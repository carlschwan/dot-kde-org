---
title: "aRts/KDE Video Roadmap Meeting"
date:    2002-01-15
authors:
  - "nstevens"
slug:    artskde-video-roadmap-meeting
comments:
  - subject: "or..."
    date: 2002-01-15
    body: "or .. Irssi, IrcII, BitchX, Xchat etc etc etc *grin*"
    author: "dmalloc"
  - subject: "Re: or..."
    date: 2002-01-15
    body: "Or mIrc.... \n\nOk I ---> []\n\n ;-)"
    author: "Shift"
  - subject: "Re: or..."
    date: 2002-01-15
    body: "Anyone using mirc will be kicked and banned. :-)"
    author: "Neil Stevens"
  - subject: "Re: or..."
    date: 2002-01-15
    body: "I doubt it - I just moved, and was stuck on a Windows machine for a week (my roommate's with a modem) while I waited for DSL.  Luckily nobody kicked or banned me in any *nix dev channel for using mIRC. \n\nMaybe you're a wee bit less open?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: or..."
    date: 2002-01-15
    body: "Maybe you are some newbie who doesn't understand a smiley?"
    author: "Erik Hensema"
  - subject: "Re: or..."
    date: 2002-01-15
    body: "Yes.  Yes, I am a newbie.  Excellent perceptive skills and cunning repartee, sir.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: or..."
    date: 2002-01-15
    body: "People like you should be shot in the head."
    author: "anonymous"
  - subject: "Re: or..."
    date: 2002-01-15
    body: "... or be forced to use mirc. :)"
    author: "ac"
  - subject: "Re: or..."
    date: 2002-01-15
    body: "Nobody should ever be shot in the head! seriously!"
    author: "cylab"
  - subject: "Re: or..."
    date: 2002-01-15
    body: ">People like you should be shot in the head.\n\nDon't tell me - you are that Israeli guy, aren't you? Thank god you probably don't know who the poster really is. Please don't bring such attitudes into this forum."
    author: "stephan"
  - subject: "Re: or..."
    date: 2002-01-15
    body: "BitchX rules !"
    author: "domi"
  - subject: "Keep talking about the Audio, please."
    date: 2002-01-15
    body: "I'm not sure if I am alone in this comment or not.  But, my experience, KDE 2.1.1 to 2.2.2 with artsd has been very disappointing to say the least.  I have a Tecra 8100 laptop 600 MHz (256M Ram) that esound and regular linux sound works great on.  Plugging in artsd, though, creates the most *nasty* choppy sound on the planet.  I moved around kernels between 2.2.x to 2.4.7 to 2.4.9 to 2.4.17 and still get the same results.  I changed all the bitrate crap, adjusted how much CPU usage and tweaked about everything except my hair before I just gave up. \n\nI also have an i810 600MHz with 256M Ram as well.  It improves the artsd server a little bit, but I still get the occasional snap,crackle,pop.  \n\nI'd like to use noatun, but XMMS is so much better.  90% of the noatun's problem is artsd, 10% is speed.\n\nPlease improve artsd!  I am an avid KDE fan and would like to be as pure as I can to help with bettering this desktop."
    author: "J"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "hm, as the bottom of the page here just said:\n\n\n\"The trick is not to dream of adding a feature, but simply to do it.\" -- Stefan Westerfeld"
    author: "Neil Stevens"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "Have you tried using the preemptive kernel patch?\n\nI've not had much of a problem with artsd as a general rule, and would only get the occansional choppyness when really stressing out the machine (Starting oracle and booting windows under VMWare at the same time)\nBut the preemptive kernel patch has removed even those problems, and I can use artsd quite happily with a response time of 40ms.\n\nI also don't use Noatun, as I prefer XMMS's playlist, but I do use the artsd output plugin for XMMs....and I play mp3s all day without problem."
    author: "Stuart Herring"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "Yes I've used preemptive.  The only thing I haven't done is move to ALSA.  I will try that when 2.4.18 comes out."
    author: "J"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "Are you using the sound drivers in the linux kernel, or drivers from the ALSA project?  In my experience, ALSA drivers are higher quality.  If you aren't using ALSA, try that.  That solved some problems I was having with aRts."
    author: "not me"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "That worked for me too, in particular for my i815\nintegrated sound card on my laptop, but I got\nimprovements also on the AC97 one on my desktop...\nALSA is definitely better..."
    author: "Andrea"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-16
    body: "Yes, reading the rest of this thread I think that many people's problems are caused by the bad sound drivers included with the Linux kernel.  Those drivers work OK for some things but aRts seems to just kill them.  I read somewhere that aRts uses unusual features of the sound drivers that most other programs don't use, which would explain why for example esound works.  Hopefully the 2.5.x kernel series in development now will make the switch to ALSA so that when 2.6.x is out Linux will finally have large numbers of high quality native sound drivers."
    author: "not me"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-04-19
    body: "In re: \"Hopefully the 2.5.x kernel series in development now will make the switch to ALSA\"\n\nALSA has been integrated to the official Linux 2.5 tree.\nThe initial merge is in patch-2.5.5-pre1, which included the 0.9.0beta9 packages"
    author: "Sven Geier"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "I used Noatun a while when 2.0 was new, but it was quite sueless for me. Any normal desktop activity caused it to skip, and it crashed too often. I tried packages of KDE (Mandrake at the time), manually compiled, and CVS releases. Now I use the Debian Woody packages, and it still isn't stable, so I'm back to xmms land. :( If I was technical enough I could submit bug reports, but nobody wants to see \"I played some music and it died\" in a report.\n\nI also don't know why it should take so long to load, i.e. several times longer than xmms, and use so much memory.\n\nI think Noatun/arts will be good when it works, and I'll keep trying it regularly, but for now I've removed it from all file associations etc."
    author: "Chakie"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "Try to increase the size of the audio buffer (in KControl/SoundServer/SoundIO)."
    author: "AC"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "Oh, that has been done. Without it I couldn't even move a window without skipping. :) There were some other settings I fiddled with too, and some helped a bit."
    author: "Chakie"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "For me It also skips when I move window ( having gcc, knode and a few konquerors running on my K6/400 128MB, especially at linking stage ). Hey people, stop complaining ! Artsd/noatrun is quite good - I find xmms worse ( I didn't try to change default settings )."
    author: "Rumcajs"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "\"manually compiled, and CVS releases\" ... \"If I was technical enough I could submit bug reports\"\n\nIf you can checkout stuf from CVS, build and install it, I think you are more than capable of a useful bug report!"
    author: "Buggy"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-16
    body: "Noatun was introduced in KDE 2.1."
    author: "Carg"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "I've tried all versions of Noatun till kde 2.2.2, but the sound is extremely choppy, and continued heavy use of resources. \nUsing xmms with artsd plugin makes xmms also produce choppy sound. The only way seems to be toi disable arts from autostarting, and then using xmms directly with oss drivers, for decent sound. \nI think as an end user, i would prefer if noatun would first play audio files atleast as well as xmms, and then we should look at video playback."
    author: "BS"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "I completelly agree! I found esd (e sound daemon) much faster than artsd with xmms, but unfortunatelly i use mandrake 8.1 and have problems to get esd running (sometimes - it depends on weather? - i get luck to start esd, but i don't know how...). it always tells me:\n---\nesd: Failed to fix mode of /tmp/.esd to 1777.\nTry -trust to force esd to start.\nesd: Esound sound daemon unable to create unix domain socket:\n/tmp/.esd/socket\nThe socket is not accessible by esd.\nExiting...\n---\n...even if i tryied everything possible to do with the =socket file in /tmp/.esd.\ni never had such problems with esd under redhat 7.1 but i'm too lazy to switch back, so i have to use that slow artsd when i want to hear few simultaneous sounds in my KDE 2.2.1 (i have got a poor amdk6/233, 128mb ram). arts works, but is really slow for me..."
    author: "luci"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "su\n<passwd>\nchown -cR <your login name>.audio /tmp/.esd/"
    author: "Ill Logik"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "thnx! it helped with esd -d /dev/sound/dsp command. but i have another problem... is it possible to run esd (artsd) available for more than just one user?\n\nluci"
    author: "luci"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-09-20
    body: "If you want ESD available globally, try this (I got it straight from the freshmeat page)\n\n1.  In your startup file (I use /etc/rc.d/rc.local -> Peanut Linux; yours may vary) add:\n      /usr/local/bin/esd &\n      sleep 1\n      /usr/local/bin/esdctl unlock\n\n2.  In /etc/profile, add:\n     export LD_PRELOAD=\"/usr/local/libesddsp.so /usr/local/lib/libesd.so\"\n\nMake sure the paths are correct...this is for my Peanut Linux setup (Slackware-derivative) using Esd 0.2.29 with default install path.\n\nRun /etc/rc.d/rc.M (or your equivalent) to restart the default runlevel and login again.  If you hear the beeps and get no errors when starting X, everything should be OK."
    author: "KBoyte"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-11-11
    body: "is there any way to start artsd in esd, like how i would start esd as\n$ artsdsp esd\n\ncan i edit some kde startscript to make arts flow to esd?"
    author: "standsolid"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-15
    body: "\"do it yourself\"\n\nuhm, if arts wants more developers it needs decent docs. last time i checked\nit wasn't so."
    author: "ik"
  - subject: "Re: Keep talking about the Audio, please."
    date: 2002-01-17
    body: "What about network audio? I've never gotten that to work, else I would use arts. I would much prefer to actually get sound in KDE *and* be able to stream audio from another system..."
    author: "richard june"
  - subject: "aRts Performance on low end machines?"
    date: 2002-01-15
    body: "My experience with arts on my machine at home(SuSE 7.3, K6-200, 96MB) hasn't been very good. While it works as expected it uses way too much CPU. Running xmms and playing an mp3 uses half of the CPU. Killing artsd and using just OSS for playback needs only 3% CPU.\n\nBecause of this it's not possible to use noatun for MPEG-Movies with sound because there is almost no CPU-power left for the movie itself. Everything is needed by artsd. So I just disabled artsd and use xmms and xine for audio/video and the performance problem has gone away.\n\nI wonder if anybody knows where this overhead comes from...and if there is a solution?!"
    author: "Stephan"
  - subject: "Re: aRts Performance on low end machines?"
    date: 2002-01-15
    body: "Without knowing Arts' performance characteristics I would blame it on the IPC/MCOP stuff and/or conversion on the server side. As xmms is not a native arts app it must send all the sound data to arts, arts will (maybe) convert it to whatever format the sournd card requires and only then they you hear that. \n\nWith a native arts app this can be done differently, you can do everything inside arts and only pass control commands (open file/start/stop/forward) from the app to arts. Then there should be no noticable difference in performance between a traditional app and an arts-based. For video the app would probably give a window id to arts and arts would put the video in the window for the app.\n\nbye..."
    author: "Tim Jansen"
  - subject: "Re: aRts Performance on low end machines?"
    date: 2002-01-15
    body: "As XMMS has an arts output plugin I consider it to be a native aRts app. Also the performance is about the same for the combination Noatun/artsd and XMMS/artsd. I would like to try Noatun with direct output to OSS or ALSA but AFAIK this is not possible(?!)."
    author: "Stephan"
  - subject: "Re: aRts Performance on low end machines?"
    date: 2002-01-15
    body: "The arts output plugin doesn't make any difference since the decoding is still done in XMMS's process, not in artsd."
    author: "Tim Jansen"
  - subject: "Re: aRts Performance on low end machines?"
    date: 2002-01-16
    body: "This doesn't make any difference.  artsd sucks up the CPU using the KDE media player noatun.  And while noatun works on my laptop, it hangs on my desktop (with a gen-u-wine SB Awe 32 audio card... can you get more standard than that?).  This, with KDE 2.2.2.\n\nEvery once in a while I try artsd, but usually I have it disabled it and just use XMMS.  I'd be more willing to mess with it if it wasn't such a CPU hog."
    author: "Sean Russell"
  - subject: "Re: aRts Performance on low end machines?"
    date: 2002-01-15
    body: "I thoought I was the only one having problems with arts...\nIt takes all my CPU (I have 1Ghz clock speed). I also use XMMS because it works better and consumes less CPU in my case.\n\nThe strange thing is that it must work on the arts developers computers... Maybe they did some configuration changes and forgot telling to other people?"
    author: "lanbo"
  - subject: "Re: aRts Performance on low end machines?"
    date: 2002-01-15
    body: "I've installed KDE 2.2.2 on several computers with several sound cards (always using OSS), and I've never had any problem using artsd....\nAnd I'm not a developer :-)"
    author: "Macolu"
  - subject: "Re: aRts Performance on low end machines?"
    date: 2002-01-16
    body: "just use bash: top"
    author: "lanbo"
  - subject: "Re: aRts Performance on low end machines?"
    date: 2002-01-15
    body: "i too used to feel that aRts was the most beta-ish software in KDE during the KDE2 run. aRts would routinely crash and it wasn't very efficient. in short, it sucked on my system here (which got me into an argument or two with Neil and Charles on IRC ;). it did get better but never got Great.\n\nthen KDE3 devel started and the first thing i noticed when i jumped back to CVS HEAD when things settled down a bit was that aRts didn't crash anymore. and it was noticeably more efficient.\n\ni'm not sure if it was the move to the GSL code or if my luck has simply changed, but i'm quite happy with aRts now. it doesn't feel / behave like it is a beta release anymore =)"
    author: "Aaron J. Seigo"
  - subject: "Re: aRts Performance on low end machines?"
    date: 2002-01-16
    body: "That's great news!\n\naRts really has been one of the weaker parts of KDE, which is a shame since it has so much potential.  The problems with sound skips and CPU usage have really held it down.  Hopefully in KDE3 aRts will mature into the multimedia framework to end all multimedia frameworks!"
    author: "not me"
  - subject: "Re: aRts Performance on low end machines?"
    date: 2002-01-16
    body: "Same goes for me. But with the TOSS driver in kde3 it doesn't take more cpu than xmms for ogg even with freereverb  or mplayer for mpeg. ~0.1% cpu for both. No CPU usage what so ever when it's idle i had to dissable the autosuspend to avoid crashes but how needs it now :)"
    author: "AC"
  - subject: "A look at what's available outside KDE"
    date: 2002-01-15
    body: "As a user, I always try to look what's available outside KDE.\n\nToday, in the Linux VIDEO arena, we have very good projects around. There's another multimedia framework \"Gstreamer\" and 2 video players that support most formats: \"mplayer\" & \"Xine\" (other projects like \"Ogle DVD player\" & \"VideoLan\" & \"AVI file\" support limited formats).\n\nHow KDE can take advantage from these available multimedia ressources ?\n\nFor good video support in KDE, we need:\n1) aRts framework to be able to stream video\n2) a good video player\n3) a set of libraries to decode (and encode ?) video streams.\n\nFor item 1, aRts will be a much better video framework due to its new core (made in collaboration with Tim Janik of BEAST project: http://beast.gtk.org/).\n\nFor item 2, two choices are possible:\n\nChoice a) To continue to use Noatun and improve its video support. Today, mplayer and Xine are the best but Mplayer got many developpers and is growing very fast. It can be a good model for Noatun to evoluate as a good Video player.\n\nChoice b) To use Noatun for audio & use another (or others) player for video.  Today, it's what happened with aKtion. We can do the same and convert Mplayer or Xine into PlayObject. This solution is fast but aRts is only used for the sound & video output. The meaning of aRts as a framework is useless then.\n\nFor item 3, two choices are possible:\n\nChoice a) To continue with \"mpeglib\" from Martin Vogt. I think it's THE weakness of aRts since it's done by one man only and is monolithic.\n\nChoice b) To accept libraries from other projects through plugins. This is the method used by Gstreamer: they re-use existing libraries by interfacing them to Gstreamer network. They don't create libraries themselves and focus on the framework instead.\n \nBoth aRts (http://www.arts-project.org/) & Gstreamer (http://gstreamer.net/) are very similar: a framework on which you can plug modules & a graphical tool is available to do it. ARts can benefit from Gstreamer by re-using its plugins (which are a lot). However, I don't know how difficult it is (maybe a common plugin API between Gstreamer & aRts ?)\n\nI hope these ideas can help. Thanks KDE multimedia developpers."
    author: "Phil Ozen"
  - subject: "Re: A look at what's available outside KDE"
    date: 2002-01-15
    body: "Hello people! Noatun doesn't decode your hax0r 31337 MP3s. aRts does. Noatun doesn't play movies, aRts does. Noatun doesn't decode. Noatun doesn't have codecs. Noatun doesn't take your CPU. Noatun doesn't skip, and last but not least, Noatun doesn't play ANYTHING! aRts does! If you want to play a Quicktime video in Noatun, you just tell aRts how to do it, recompile it, re-load it. Noatun will play it then because all Noatun does is talks to the server (aRts).\n\n_ARTSD_ uses mpeglib. This isn't about Noatun, this is about aRts. Read the post: \"aRts/KDE Video Roadmap Meeting\" NOT \"aRts/Noatun\".\n\n</rant>\n   --Nick Betcher"
    author: "Nick Betcher"
  - subject: "Re: A look at what's available outside KDE"
    date: 2002-01-15
    body: "2a) Pure players like Xine, mplayers and all the others won't get you anywhere. Well, almost, you can use them for playback of files which may be the most important use case, but not more. They usually have *very* inflexible plugin mechanisms, optimized for playback of files. Among other things, they usually lack (without wanting to criticize a specific player):\n\n- the support of encoding (think of video conferencing, video mails, video editing, tv recording)\n- the support for decoding without realtime (video editing, re-encoding)\n- the support for push mechanisms (when you have data in a file or get it from a TCP connection pulling is ok; if a codec is embedded in a multiplexer or gets data from UDP/RTP/Multicasts/whatever the situation is very different)\n- the support for filters\n\nThese players are ok as an interim solution, but in the end they will be wasted effort. The good thing is that, partially because of their popularity, many people have written codecs that can easily be rewritten for a better framework.\n\n3b) GStreamer is fine, but uses this ugly GObject framework that makes it hardly usable for most sane people :)"
    author: "Tim Jansen"
  - subject: "Re: A look at what's available outside KDE"
    date: 2002-01-15
    body: "umm.... \n\n> they usually lack (without wanting to criticize a specific player):\n> - the support of encoding (think of video conferencing, video mails, video editing, tv recording)\n> - the support for decoding without realtime (video editing, re-encoding)\n> - the support for push mechanisms (when you have data in a file or get it from a TCP connection pulling is ok; if a codec is embedded in a multiplexer or gets data from UDP/RTP/Multicasts/whatever the situation is very different)\n> - the support for filters\n\n\nmplayer's got ALL of those features..."
    author: "ealm"
  - subject: "Re: A look at what's available outside KDE"
    date: 2002-01-15
    body: ":: mplayer's got ALL of those features...\n\n    Both MPlayer and VideoLAN.  Both of which are GUI independant projects that use plugins for the entire front end.  Both of which, like aRts, are designed to work with any WM or Environment, and have a good solid base of plugins to read everything from DVDs to DivX ;-) files.  \n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: A look at what's available outside KDE"
    date: 2002-01-15
    body: "Actually mplayer does not even have a real plugin-structure, it is a huge cut&pasted hack that's illegal to distribute in binary form because of its mix of licenses. It's very good at what it does, but almost everything is hardcoded. Decoding is done in a huge switch statement, you cannot just edit a filter without editing the mplayer core and recompile it. It's completely unusable for implementing, say, a new video editing or conferencing tool."
    author: "Tim Jansen"
  - subject: "Re: A look at what's available outside KDE"
    date: 2002-01-15
    body: "> - the support of encoding (think of video conferencing, video mails, video editing, tv recording)\n\nmplayer can encode DivX using two different encoders and will sonn be able to\nencode many other formats.\nI use it every day to encode avi files from DVDs faster and with more control than FlaskMPEG\n\n> - the support for decoding without realtime (video editing, re-encoding)\n\nmplayer can decode any movie to png sequences or mpeg-PES with high quality and in batch processing\n\n> - the support for push mechanisms (when you have data in a file or get it from a TCP connection pulling is ok; if a codec is embedded in a multiplexer or gets data from UDP/RTP/Multicasts/whatever the situation is very different)\n\nI never tried, but the documentation say that mplayer can play from network streams ( i think only tcp ). \n\n> - the support for filters\n\nDo you mean output filters ?"
    author: "Andrea Albarelli"
  - subject: "Re: A look at what's available outside KDE"
    date: 2002-01-15
    body: ">> - the support for filters\n>> Do you mean output filters ?\n\nNo, putting a filter between two modules. For example you might want to increase the brightness of a video, so you connect a decoder-module to a brightness-filter and then the brightness-filter to an encoder-module. Filter's can be used for other things as well, like adding effects in a video editor or to detect motion in a video conferencing app. MPlayer doesn't have this kind of flexibility, there is no video pipeline that you can alter or even a common interface for codecs."
    author: "Tim Jansen"
  - subject: "Re: A look at what's available outside KDE"
    date: 2002-01-16
    body: "Good comments.\n\nTo address your three items:\n\n1) Yes, one of the goals of the meeting is to make sure that KDE gets what is needed, preferably through aRts.\n\n2) I consider Noatun a music player.  I wrote Kaboodle (included in KDE 3 kdemultimedia) to be the single-shot generic media player you're reaching for.  Converting an existing monolith to a Playobject would get us absolutely nowhere - it'll just be another mpeglib, as you seem to realize.\n\n3) If mpeglib gets something, we'll take it, but we don't intend to wait around.  There are projects out there that do a lot more than mpeglib does, and I think it'd be nice if the mm systems in KDE could be improved to be able to take advantage of them.\n\nI have no interest in GStreamer.  From what I've seen, it's just like aRts, only written in C, and with different levels of maturity for different parts.  The key difference between the two is that aRts has had a lot more testing in a mass install userbase, so I'm more likely to trust aRts."
    author: "Neil Stevens"
  - subject: "More on GStreamer"
    date: 2002-01-16
    body: "Unless it's possible to use GSTreamer without the glib event loop, I don't see how it can be possible at all to use GStreamer with KDE apps, as KDE apps already use the QApplication event loop."
    author: "Neil Stevens"
  - subject: "Re: More on GStreamer"
    date: 2002-01-16
    body: "Thanks Neil for your clarification.\n\nI understand better the problems of integrating Gstreamer. I also agree with you concerning Gstreamer immaturity. There's a big gap between the existing plugins (over 100 claimed) and the usable ones.\n\nTwo other important comments:\n\na) When you look at aRts web site http://www.arts-project.org/, it doesn't reflect that aRts is now a multimedia framework. Since it's the first place where potential developpers will go, this might mislead or discourage them. It might be the good time (...KDE3) to refresh the web site & find a dedicated Webmaster. This will also free Stefan Westerfeld. It's obviously better he codes aRts instead.\n\nb) In the same way, the aRts documentation seems unclear to many & needs to be upgraded. I regularly read the KDE multimedia mailing lists and many times newcomers complain of the difficulty to understand aRts due to the lack of proper documentation. In fact, only Stefan Westerfeld has the knowledge. It would be good if this knowledge is made more accessible to others.\n\nI remember that Eugene Kuznetsov (the author of AviFile )was wanting to join in December 2000 but he stopped after saying the following in the mailing list.\n\n\"I am new to this list. I've got a question about perspectives of your\narchitecture. Do you have any plans to implement streaming video in\naRts, making something that would not heavily depend on mpeglib? It\nwould make aRts more flexible and allow other developers to write\ntheir own components.\"\n(see http://lists.kde.org/?l=kde-multimedia&m=97794476604838&w=2)\n\n\"Code that does all actual work on parsing, decoding and drawing anything\nis safely hidden beyond undocumented and obscure interfaces of mpeglib.\nWhat's worse, it is unusable for anybody who does not want to completely\nintegrate into it.\"\n\"I know that I don't need mpeglib to implement Arts::PlayObject (already\ndone it in my project), but I don't like the idea of having two\nincompatible video & audio renderers, audio/video syncers, etc.\nFor sure this would require much work, you'll need to define all\nnecessary interfaces, apply heavy changes to mpeglib, but I think that\nit's worth it.\"\n(see http://lists.kde.org/?l=kde-multimedia&m=97811984816016&w=2)\n\nHis remarks are pertinent but this is long time ago. Today, it's important to make aRts more easily interfacable with others projects (such as AviFile). This might need proper documentation & aRts internal changes.\n\nI hope not to look presomptuous with my comments since I do nothing on the project. I only want to help with ideas that might interest you. Thanks again Neil for the job done."
    author: "Phil Ozen"
  - subject: "aRts room for improvement"
    date: 2002-01-20
    body: "Yes, aRts is undocumented.  Stefan lacks time for that stuff.  That's why arts-project *and* the docs are so behind.\n\nThe biggest problem I see getting a video framework going, will be getting people who know video to brave the challenge of working within aRts.  I've already had one person tell me he's unlikely to help because aRts is such an undocumented web."
    author: "Neil Stevens"
  - subject: "Re: aRts room for improvement"
    date: 2002-01-21
    body: "Yes, I agree, it's a big blocking point. To grow, a project needs new blood.\n\nObviously, the same kind of web site as \"Gstreamer.net\" wil attract a lot of newcomers. It's really well designed. As told previously, an \"aRts-project\" webmaster is top priority.\n\nI hope to read a summary of the saturday IRC on \"the dot\" or next \"Kernel cousin KDE\".\n\nThanks Neil"
    author: "Phil Ozen"
  - subject: "I still want sound with several voices"
    date: 2002-01-15
    body: "I use the ac97-Soundchip and I switched off the artsd to enjoy the lokigames-sound and use xmms.\nANd artsd isn't that good when the cpu-usage is high.\n\nBut xmms blocks all system notifications and i get them all afterwards :-/\nSo I'm quite jallous (4 sound-reasons), when I see my brother with win me gaming.\nThe artsd was the only way to play several files simultaniously.\n\nBut I'm not always using KDE, perhaps it's more a thing for the alsa-project.\n\nI just hate it, when my Mozilla hangs, until I stop xmms.\n\nI'll try kde 3 soon.\n\n\nMfG\n\nSchugy"
    author: "Christian Schuglitsch"
  - subject: "Re: I still want sound with several voices"
    date: 2002-01-15
    body: "try to use the artsd plugin for xmms. Maybe that solves the problem you have...\n\nhoink"
    author: "hoink"
  - subject: "Re: I still want sound with several voices"
    date: 2002-01-15
    body: "I nice solution to problems like this is to use a SoundBlaster Live with ALSA. The SBLive driver allows 32 processes to access the /dev/dsp file simultanously, so you don't have to suspend the sound server or use artsdsp."
    author: "Tim Jansen"
  - subject: "Re: I still want sound with several voices"
    date: 2002-01-16
    body: "I only have a via ac97-chip on  my Asus a7v 133WA or a SB 128Pci :-/\n\nI still wonder why noatun hasn't the nice features like kmp3.\n\nThat was so  comfortable when you had to rename files or make an id3-tag.\n\nPerhaps the kmedia-player will be a dvd-player with a lot of skins, dvd-menus and all other features soon."
    author: "Christian"
  - subject: "I am just a dirty end user, but....."
    date: 2002-01-16
    body: "what would be kind of neat would be a way to have a plug-in system that you could just drop into a specific folder. so even though there are Legal issues  with DVD decoding there would be a way for someone to create an aRts plug-in of Decss and anyone could just install it without too much trouble, and if any other kinds of formats come out there could be an aRts plug-in or module made for it that any newbie could install. now like I said, I am just a dirty filthy no account end-user and have no idea how to program but, I do have a love of Linux and technology and want to see linux succeed. and  I think somthing like that would be a boon to KDE, a real killer app/framework. \n\nplease don't beat me over the head, it's just an Idea (for bad or good) from a fellow Linux/KDE user. : )\n\nthank's for reading my post!"
    author: "LD"
  - subject: "Re: I am just a dirty end user, but....."
    date: 2002-01-16
    body: "Well, that's kind of the idea behind a multimedia framework:  let anyone write a plugin that can be installed with an RPM or whatever, and then all your multimedia applications can use that codec instantly.  So if someone wrote a DeCSS plugin, and you installed its RPM, then all KDE applications would automatically be able to decode CSS-encrypted DVDs.  Hopefully aRts will have the technology to do this right in KDE 3, and that's what this meeting is all about."
    author: "not me"
  - subject: "Re: I am just a dirty end user, but....."
    date: 2002-01-17
    body: "Ok, like I said I don't know much about the programming end of things I just love using Linux and if a suggestion is helpful in making Linux better and easier to use I will keep making them. I like fiddling around with Linux but I also like to have things \"just work\" too. KDE makes the \"just work\" part of Linux get better and better. so if KDE is trying to make aRts more user-friendly more power to them!"
    author: "LD"
  - subject: "movie player"
    date: 2002-01-16
    body: "I not a developer am a user so I will not comment how it should be done, I just say what I want as a user. I want kde to play movies simpel just by klicking on when. All well known format's should be supported. KDE should have it's own player well integrated with everything else in the desktop. Konq should list files with the icon of the first frame of the movie(that make's it very easy to see witch movie it is | Yes like win XP)."
    author: "Andreas"
  - subject: "Re: movie player"
    date: 2002-01-16
    body: "no like in winXP that sucks , never do this :-( , perhaps we could make the forst frame wchich has\nsome picture on it , or just start to play the movie in the icon , that would be cool. my machine can handle \n5-6 divx simulaniouly ."
    author: "hrhr"
  - subject: "Re: movie player"
    date: 2002-01-28
    body: "There is a problem with those video-formats: they are all not open source and therefore most of the players only work on i386.\nSince KDE claims to be cross plattform (KDE3 on alpha ? Any chance this time ?) implementing the Windows-Codec method used by mplayer & co ist not really a way to go, imho.\nBesides, what if the use of those dlls is getting illegal ? \n"
    author: "Milifying"
---
All aRts and KDE developers (or would-be developers) <a href="http://dot.kde.org/1011042476/">are invited</a> to an IRC meeting in order to draft a short- and long-term roadmap for the future of video in aRts and KDE.  Why? <a href="http://www.arts-project.org/">aRts</a> is a solid base, and it would be a shame not to build a good video system on top of that, taking the best from the already existing video projects.
<!--break-->
<br><br>
<table border="1">
<!-- tr><td><b>Why:</b></td><td>aRts is a solid base, and it would be a shame not to build a good video system on top of that, taking the best from the already existing video projects.</td></tr -->

<tr><td><b>When:</b></td> <Td>Saturday, January 26, 2002</td></tr>
<tr><td></td><td>21:00 GMT / 22:00 CET / 13:00 PST / 16:00 EST</td></tr>

<tr><td><b>Where:</b></td> <td>#kde-multimedia on irc.kde.org</td></tr>
<tr><td></td><td><A href="http://www.openprojects.net/">http://www.openprojects.net/</a></td></tr>

<tr><td><b>How:</b></td><td>kSirc in kdenetwork</td></tr>
<tr><td></td><td>(or <a href="http://www.kvirc.net/">KVirc</a> if you insist)</td></tr>

</table>