---
title: "Noatun Gets a Web Interface"
date:    2002-02-21
authors:
  - "rcumming"
slug:    noatun-gets-web-interface
comments:
  - subject: "wow"
    date: 2002-02-21
    body: "It's 3AM, I feel funky, and I just have to say, you guys never cease to amaze me.  One of the things that make me feel really good about OSS is kde.  The fact KDE team just keeps putting out new innovative stuff is really amazing.  All the best to KDE3, I look forward to an incredible relase."
    author: "Matus Telgarsky"
  - subject: "hey"
    date: 2002-02-21
    body: "I love your work, kde guys! Splendid!"
    author: "Dominik"
  - subject: "Security..."
    date: 2002-02-21
    body: "With recent 'innovations' like giving anybody on the Internet remote control of your desktop, the whole 'kpf' fiasco, and now, this, KDE's security makes Windows XP look like a B-grade operating system in comprison. Right now I have to have a special subnet in our organization to put the KDE boxes behind; the GNOME machines run ok with no firewall at all, but KDE needs to be protected and even a firewall is not good enough since pranksters inside my company like to log into other people's Konqueror and put up porn web pages, etc.\n\nI can see how features like kpf and nutun (or whatever the web based interface for xmms is called) like would be useful... but either disable them by default, or make a KDE port of ZoneAlarm... in case you've never used Windows, it's a program that protects your computer and auto-detects worms trying to hack in.\n\nJust my 2-cents, flames to /dev/null..."
    author: "John Harper"
  - subject: "Re: Security..."
    date: 2002-02-21
    body: "don't feed the trolls i guess."
    author: "ik"
  - subject: "Re: Security..."
    date: 2002-02-21
    body: "Ever tried GuardDog and GuideDog ????? Obviously not....\nhttp://www.simonzone.com/software/guarddog/"
    author: "Manu"
  - subject: "Re: Security..."
    date: 2002-02-21
    body: "Doesn't Redhat 7.2 have a firewall installed?"
    author: "Birger Langkjer"
  - subject: "Re: Security..."
    date: 2002-02-21
    body: "Those features are turned off by default."
    author: "Schwann"
  - subject: "Re: Security..."
    date: 2002-02-21
    body: "you silly monkey boy...\n\nokay \n1) this does not run by default brain boy.\n2) this is not a part of KDE 3.0\n3) kpf dosent run by default, you have to go through some very OBVIOUS questions to give out access.\n4) xmlrpc dosent even open by default any more.  \n5) unless you have someones password to access there computer you cannot get access to konqi.  and by that point i think you have to re-evaluate your security and employees.\n\ni know this is all probilby more than you can digest, so take a few moments and think about this.  you may also want to use your 1337 hax0r skills and run a port scan on your workstation running default KDE.  on the off chance you know what a port scan is you will find that we open NO ports save for kdm if you really configured your system wrong enough.\n\nif any of this seems strange or difficult to you please feel free to take your computer and smash it into little peices...\n\n\t-ian reinhart geiser\n\np.s. if you actually are a systems administrator your company is in bad bad shape...  unless you are talking about your \"computer\" at McDonalds, but I am pretty sure those cannot run KDE anyway...  \n\n\n "
    author: "Ian Reinhart Geiser"
  - subject: "Re: Security..."
    date: 2002-02-22
    body: "Well, I do think this post is a flame, but it disturbs me that anytime people start asking hard questions about security in KDE, the response is always flames a la \"Don't you know Unix security takes care of that, you troglodyte?\".\n\nI think we ought to start a kde-security team, which tries to go through and abuse as many KDE installations as possible, locally and via the network.  Might as well get secure BEFORE we get big.\n\nEE"
    author: "Eric E"
  - subject: "webshots?"
    date: 2002-02-21
    body: "What, Ryan, you don't want hundreds of dot.kde.org readers messing with your live flood url, like the tens of people in #kde?"
    author: "Neil Stevens"
  - subject: "[unrelated as hell]"
    date: 2002-02-21
    body: "TOPIC: file sharing!\nThis is somthing that kde could do better if you ask me. And that I want to see som e better solutions. I would like to make it very easy to share files and folders in linux. Imagne that you have one folder that you want to share with others. That should be done in kde just as easy as in windows. It should be possible to chose what type of share you want to do(smb windows shares/or unix shares nfs or something). I dont know that much of the existing sharing protocols for unix but use the most common. Anyway .. make filesharing more easy!"
    author: "Andreas"
  - subject: "Re: [unrelated as hell]"
    date: 2002-02-21
    body: "So what's wrong with http-download via kpf?"
    author: "Lenny"
  - subject: "Re: [unrelated as hell]"
    date: 2002-02-21
    body: "ignorance?\n\npersonally i like the idea of a gnutella server daemon in KDE, but really i cannot thing of an intellegent use of file shareing on the desktop.  what KPF cannot do you more than likely need a larger server for...\n\njust my 2c\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: [unrelated as hell]"
    date: 2002-02-21
    body: "\n> So what's wrong with http-download via kpf?\n\nIt is just plain braindead!\nBetter add options for configuring smb or bfs shares to\nthe properties dialog.\n\nA user does not expect to set up a webserver when sharing a folder"
    author: " me"
  - subject: "Re: [unrelated as hell]"
    date: 2002-02-21
    body: "oh and setting up a full fledged Samba server is a good idea?\n\nno, really http is the most portable and flexable protocal out there.\nthe only feature lacking in my mind is an obvious place to display the URL to access the files.\n\npersonally a small http server is the most intellegent way to go, setting up a Samba server is just plain moronic...    besides, how will i share files with my mac?\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: [unrelated as hell]"
    date: 2002-02-21
    body: "> oh and setting up a full fledged Samba server is a good idea?\n\n> no, really http is the most portable and flexable protocal out there.\n> the only feature lacking in my mind is an obvious place to display the URL\n> toaccess the files.\n\n> personally a small http server is the most intellegent way to go, setting\n> up a Samba server is just plain moronic... besides, how will i share files with\n> my mac?\n\nMaybe we should keep kpf then, but add smb and nfs as well, see next reply for more\n\n"
    author: "me"
  - subject: "Re: [unrelated as hell]"
    date: 2002-02-22
    body: "I think I fall between you and the original poster.\nSamba is a pain the butt in many ways - it is definitely not turn on and play the way a webshare is.\nHowever...........\nLike it or not, there's still a lot of benefits to having samba shares available:\n   a) lots of users and organizations rely on Windows filesharing\n   b) All Windows apps can read and write directly to samba fileshares.  I use a samba share \n   c) Windows networking has better \"resource discovery\" than http.  If we want usable file sharing over http (which I think is a good long-term direction), we need to add security and resource discovery quick.  Otherwise I think we're encouraging people to slap stuff on the web without thinking.\n\nYou might check out clarkconnect.org.  They've done a remarkable job reworking RedHat so that it sets up samba on an internal network and httpd access for an external one.  KPF could easily be reformatted to help users make dual samba/http fileshares, with the http ones available everywhere, and the samba ones available only on selected subnets, and only if samba is available.\n\nCheers,\n\nEric"
    author: "Eric E"
  - subject: "Re: [unrelated as hell]"
    date: 2002-02-21
    body: "> Better add options for configuring smb or bfs shares to the properties dialog.\n\nKDE can't become so dependent on other projects like Samba.  How is KDE supposed to know whether you have Samba installed, whether it is running, where its config files are, and which version you are using so it can write the config file properly?  What if Samba is installed but the config file is only writeable by root (the default on most distros I believe)?  What if smbd isn't started automatically when the system starts up?  This is the job of the distro, and they are doing it.  Lycoris will soon have right-click file sharing, I believe Mandrake already has it.\n\nIn the meantime, I think a web server applet is a fine way to share files.  What's wrong with serving files over http, as opposed to smb?"
    author: "not me"
  - subject: "Re: [unrelated as hell]"
    date: 2002-02-21
    body: ">> Better add options for configuring smb or bfs shares to the properties dialog.\n\n> KDE can't become so dependent on other projects like Samba.\n\nThis is true\n\n> How is KDE supposed to know whether you have Samba installed, whether\n> it is running, \n\nIt could check for the process and give a message when smbd is not running\n\n> where its\n> config files are, and which version you are using so it can write the\n> config file properly?\n\nThe config file could be set up somewhere in Kcontrol/System. I think most of\nthe time it is in etc.\n\nAFAIK the syntax for adding a share and whether it's read-ony or not etc hasn't \nchanged for aeons.\n\n\n> What if Samba is installed but the config file is\n> only writeable by root (the default on most distros I believe)?\n\nkdesu\n\n> What if smbd\n> isn't started automatically when the system starts up?\n\nAsk the user if he wants to start it + give a warning to change his setup \nif he wants to have his share still available after rebooting.\nAnd finally kdesu.\n\n\n> This is the job of\n> the distro, and they are doing it.\n\nThis is maybe true\n\n> Lycoris will soon have right-click\n> file sharing,I believe Mandrake already has it.\n\n> In the meantime, I think a web server applet is a fine way to share files.\n> What's wrong with serving files over http, as opposed to smb?\n\nIt's just strange IME.\nIt doesnt give you the same convenience than a filesystem.\n\nMaybe ftp would even be better. This can at least be browsed more nicely in konq\n and you can select multiple files at once\n"
    author: "me"
  - subject: "Re: [unrelated as hell]"
    date: 2002-02-22
    body: ">Ask the user if he wants to start it + give a warning to change his setup\n\nEvery distro does config differently.  KDE can't go messing with the system startup scripts because it doesn't know how they're laid out.  Remember, KDE runs on many systems including Linux, BSD, Solaris, and maybe someday Windows.\n\n> kdesu\n\nEntering the root password just to share some files?  That seems silly to me.\n\n> The config file could be set up somewhere in Kcontrol/System.\n\nSo KDE is going to be mucking around with your distro's setup of Samba?  How will KDE tell samba to use the new config file on system startup (remember, we can't edit the startup scripts)?  What happens when a CLI user goes to edit the smb.conf file in /etc and it doesn't work?\n\nIf Samba was installed exactly the same way on every distro and every distro used the same startup sequence, KDE could do stuff like this.  Right now though, since every distro does things differently, they have to set KDE up specifically for themselves.  And they do.  This is not the domain of the KDE project.\n\nPerhaps in the future, kpf could be upgraded to be a WebDav server, to allow more ftp-like functionality, perhaps even with ssl for security.  I think this might be the best overall solution, even better than Samba integration.  As long as security is kept in mind of course."
    author: "not me"
  - subject: "Re: [unrelated as hell]"
    date: 2002-02-24
    body: "Laurent and I have been working on adding a \"Share directory\" in the RMB popupmenu in Konqueror, for Mandrake 8.2.\nHopefully we'll be able to integrate this into KDE 3.1, i.e. if we find ways to make it enough distribution-independent.\n\nDavid.\n"
    author: "David Faure"
  - subject: "Cool"
    date: 2002-02-21
    body: "Never seen this before.\nSeems pretty cool to me."
    author: "A"
  - subject: "I don't get it."
    date: 2002-02-21
    body: "\nI don't understand what this does.  Why do I want to control noatun remotely?\nOr do you mean you can listen to the music remotely?  Nothing has mentioned\nlistening to music...only \"controlling noatun\" which I don't really find as\nexciting as listening to music.\n\nPlease explain what kind of neat things one could do with this."
    author: "Slow Poke Rodriguez"
  - subject: "Re: I don't get it."
    date: 2002-02-22
    body: "Well I can wear my wireless headphones and then with my Zaurus just browse over to my computer and change the song.  (excellent when working out for example)"
    author: "Benjamin Meyer"
  - subject: "Re: I don't get it."
    date: 2002-02-22
    body: "I'm working on Icecast support right now, which should make Flood much more useful. As Flood is now, unless you have a LAN in your house or something, it just looks pretty. :)\n\n-Ryan\n"
    author: "Ryan Cumming"
  - subject: "Re: I don't get it."
    date: 2002-02-22
    body: ">Nothing has mentioned listening to music...only \"controlling noatun\" which I don't really find as exciting as listening to music.\n\nHeh, you've got a good point.  It seems to me like Noatun development is too focused on Noatun-the-extensible-application instead of Noatun-the-program-people-use-to-play-music.  IMHO Noatun needs to focus more on being a good, useable, feature-complete player of music than having whiz-bang programmer-centric features like \"everything's a plugin!\"  It seems like a really great music player could have been produced in the time Noatun has been around, but most people still prefer XMMS.  The uber-plugin nature of Noatun seems to get in the way sometimes, like in the fact that the playlist can't be skinned, the fact that ID3 tags can't be edited directly from the playlist, and the awkward construction of the configuration dialogs.  Noatun should focus more on doing one thing and doing it well than being the super end-all one-stop-shop mega-app.\n\nSeems to me that plugins are more important for closed-source apps like Winamp anyway.  If someone wants to extend Noatun, they can darn well download the source and add a feature!  This is why I don't understand the fact that playlists and interfaces are (separate kinds of) plugins.  I can see I/O and visualization plugins becuase you might want to download/install them separately.  Even so, though, no one ever downloads and installs Noatun plugins because they are almost always developed against the CVS version of Noatun instead of the stable one.  Most people only use the Noatun plugins that come with KDE, and when they upgrade KDE they get new plugins with the new Noatun, which sort of defeats the purpose of making everything a plugin.   Also, it seems to me that if Noatun has a good DCOP interface, this plugin (or also for example the excellent Keys plugin) wouldn't even need to _be_ a plugin."
    author: "not me"
  - subject: "Re: I don't get it."
    date: 2002-02-22
    body: "Noatun does not play music! Arts plays music! Noatun is a frontend to arts. I don't get why no one understands this. I have quite a few disagreements with Neil myself, but I'm on his side on how much you people just don't seem to absorb the fact that Noatun does not play DVDs or MP3s or anything like that. It just tells aRts to do it, which handles everything that's related to audio (with the possible exception of streaming, but that's another story).\n\nThe playlist can be skinned, if someone would bother to develop a skinnable playlist. For instance, you could probably build an xmms playlist, that emulates the functionality of XMMS playlists, and can load XMMS skins. Why you'd want to do this is beyond me, but even using QT based playlists, you can just change the QT theme. And I thought you weren't concerned about anything but playing music anyways.\n\nUh, no one downloads and installs Noatun plugins because Noatun is OSS and has very few plugins at the moment, so all the good ones come with it anyways (Flood isn't one of the good ones btw, since without Icecast or something along those lines, it doesn't do much).\n\nYes, if someone wants to extend noatun, they can download it and patch, but what if the feature they want to implement conflicts with one already implemented? It would be impossible to properly, for instance, implement a file system based playlist over a hard coded flat playlist. It just couldn't be done without plugins. The whole point of plugins is they can be interchanged and modularized and make scalabilty easier, not that you can download them if you want to add something later (though this is an added benefit).\n\n*Huff*Puff* :-)"
    author: "Carbon"
  - subject: "Re: I don't get it."
    date: 2002-02-22
    body: "Noatun is the program people use to play music through aRts.  There, is that pedantic enough for you?  I don't see how the method Noatun uses to produce sound from the speakers is relevant anyways.\n\nThe problem is that the playlist is separate from the rest of the interface, unlike every other music player on the face of the planet.  So say you want a Winamp skin loader.  It can't have a Winamp style playlist unless you also make and separately activate a Winamp-skinnable playlist.  Then if you want the equalizer, you need another equalizer plugin (is this even possible?).  I think it's silly.  Why not just program the interfaces (and/or playlists) directly into Noatun?  That way its easier to program, the components can have more interaction (like ID3 editing more integrated with the playlist), and you can activate/deactivate whole interfaces at once instead of piecemeal.  Noatun could still have a modular design, so for example you could replace the playlist engine (though I think that no music player should ever need more than one playlist, but that's a different story).  Plugins are not the only way to make a modular design.  I just think the current design is TOO modular, making Noatun needlessly complex and non-integrated, for the user and the programmer."
    author: "not me"
  - subject: "Re: I don't get it."
    date: 2002-02-22
    body: "Yes, you're absolutely right! Why have a versatile media player when I could have one that's exactly like every other media player?"
    author: "Charles Samuels"
  - subject: "Re: I don't get it."
    date: 2002-02-22
    body: "Well, I'm fairly certain I'm not advocating building an inflexible media player, or a media player that's exactly like any other media player.  But don't let that stop you from making inane remarks."
    author: "not me"
  - subject: "Re: I don't get it."
    date: 2003-12-13
    body: "Its hardly inane. The fact that the playlist, equalizer, etc,. are plugins allows for the possibility of new and unique ideas in those areas to be implemented relatively painlessly. You wish to lose that functionality for the purpose of skins. While I have no hatred of skinning in and of itself, I don't know why thats so important that this unique quality of noatun should be sacraficed.\n\n-troy"
    author: "Troy"
  - subject: "Re: I don't get it."
    date: 2002-02-24
    body: "You have good points. I for one would like to use noatun as my mp3 player, as I prefer usung kde/qt apps over others. Noatun just is too complex for everyday use. I use xmms for playing music always. I tried to use noatun for a week, but it just didn't feel right tool for such a simple task. Of course noatun has a lot of potential and I hope that one day it is my player of choice. I agree with your point that developers should concentrate more on usability than making noatun extendible.\n\nFlood is a good idea though, eventhough I don't see much use for it at this moment."
    author: "samppa"
  - subject: "Bizarre Coincidence"
    date: 2002-02-22
    body: "I found this bit of news particularly amusing since yesterday (21st of February), I realeased an xmms plugin to exactly the same thing....\n\nIt does have basic icecast functionality and the layout is quite different, but the concept is the same. \n\nOdd huh?\n\nBest of luck with the ongoing development. Want to collaborate on ideas?\n\nlilo_booter\naka Charles Yates"
    author: "lilo_booter"
---
<a href="http://phalynx.dhs.org/flood/">Flood</a> is a simple web interface plugin for <A href="http://noatun.kde.org/">Noatun</a>. It allows the user to control the basic features of Noatun from anywhere on the web, using a dynamically generated XHTML/CSS interface. Flood also generates an RDF file of recently played songs readable by news tickers such as <A href="http://apps.kde.com/na/2/info/id/999">KNewsTicker</a>.
Check out the <a href="http://phalynx.dhs.org/flood/">homepage</a> for more details, the <a href="http://noatun.kde.org/news/article.php?sid=13&mode=thread&order=0">Noatun news item</a>, or just look at these webshots (<a href="http://phalynx.dhs.org/flood/webshot-1.html">1</a>, <a href="http://phalynx.dhs.org/flood/webshot-2.html">2</a>, <a href="http://phalynx.dhs.org/flood/webshot-3.html">3</a>).
<!--break-->
