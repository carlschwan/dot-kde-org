---
title: "OSnews:  Interview with Waldo Bastian"
date:    2002-07-17
authors:
  - "Dre"
slug:    osnews-interview-waldo-bastian
comments:
  - subject: "You too can help KDE - send beer!"
    date: 2002-07-17
    body: "I can't think of how many times I have had people email me and say the were not able to help develop an open source project. Waldo has put it so succinctly... send beer. ;-) I think I'll have one now.\n\nI want to take a moment to express my appreciation for Waldo as well as so many other key core developers who make so much happen. It is always fun to see such great people at work and play on KDE."
    author: "Eric Laffoon"
  - subject: "What I have liked the most:"
    date: 2002-07-17
    body: "What I have liked the most:\n\n\"I'm looking forward to the day when this new linker is widely in use because then it will become painfully clear where the linker was causing the slowness and where it is solely to blame on the application developer. At the moment it is much to easy for developers to shift the blame on the linker.\"\n\nSo true. Very interesting interview. Worth reading it!!!"
    author: "Anonymous"
  - subject: "Re: What I have liked the most:"
    date: 2002-07-17
    body: "Now for the real question: When will a \"prelinked\" KDE be the standard? Are we weeks, months, one year, several years away? I'm waiting for the day when a new Mandrake, SuSE or Red Hat comes with KDE prelinked right out of the box."
    author: "Matt"
  - subject: "Re: What I have liked the most:"
    date: 2002-07-17
    body: "\nAs soon as gcc releases their new compiler/linker.  The problem is not in KDE, it is with gcc, and only affects anybody who has used gcc to compile KDE.  Since gcc is all over the place, and by far the most common compiler used on Linux and BSD, the problem affects most users of KDE.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: What I have liked the most:"
    date: 2002-07-17
    body: "What you said is true, but the question I previously sent and would like to get an answer if possible is:\nHow much of the start time can be reduced with an improved linker?\nWill apps boot twice faster? Three times faster? 20% faster?\nI'd really like to know what are we waiting for when we hear \"Just wait until the linker is optimized and apps will boot faster\"\nThanks guys"
    author: "Anonymous"
  - subject: "Re: What I have liked the most:"
    date: 2002-07-17
    body: "I have some (old) data on http://www.suse.de/~bastian/Export/linking.txt\nBe aware that these numbers depend on the actual version of KDE/Qt (they will become slowly worse when KDE/Qt grows), the number of libs used by the application and your CPU (they will become better with faster CPU / faster memory)\n\nIn short: KEdit takes about 1.3 seconds to start and about half of that was caused by linker overhead.\n\nKeep in mind that kdeinit already reduces most of the linker overhead, so applications that currently run as \"kdeinit:\" are unlikely to see any benefit at all.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: What I have liked the most:"
    date: 2002-07-17
    body: "A \"ps -aux\" on my system shows me that almost all KDE progs are run as kdeinit.\nKonqui for instance is run as kdeinit.\nThen, after all, I understand the problem is not on the linker. Konqui still needs almost 3 secs on a 1Ghz intel to boot even if it has been previously opened.\nShould we expect any improvements, or is this speed considered good enough?\n\nThanks Waldo and KDE developers."
    author: "Anonymous"
  - subject: "Re: What I have liked the most:"
    date: 2002-07-17
    body: "Hmm, how was your Konqui compiled? I could produce times of about 1-1.5 seconds for optimized builds with my Celeron 950. \n"
    author: "Sad Eagle"
  - subject: "Re: What I have liked the most:"
    date: 2002-07-18
    body: "It was compiled by RedHat.\n\nDon't tell me that I should compile by myself when KDE project is all the time saying that configuration tools, for instance, depend on the distribution.\n\nSo I do have to get a distribution to get the config tools, but then I have to compile it by myself?\n\nOr is it just that KDE boots slower on RedHat? (and no, I'm not running all the servers (sendmail, etc) that RedHat comes with).\n\nSometimes it's a bit confussing...\n"
    author: "Anonymous"
  - subject: "Re: What I have liked the most:"
    date: 2002-07-18
    body: "Geez, this ALWAYS happens.  Someone makes a comment saying \"My Konqueror takes X seconds to start up, and KMail takes Y!\"  Then someone else posts \"Yours must be broken, mine only takes Z!\"  I think the problem is that everyone's configuration is different.  So here's a new idea:  Whenever someone posts a time for a KDE app to start, tell them to *move their .kde directory* and try it again.  This will clear all KDE settings to their default values and allow some sort of accurate measurement.  Without doing this, there can be no comparison of start times.  And if it is discovered that a certain setting makes KDE apps start much much slower, then that can be found and fixed.\n\nLike Waldo says, I really wish that GCC/Binutils would hurry up and get prelinking ready, and not because it would make KDE faster.  It actually won't.  KDE has already hacked around the problem with \"kdeinit.\"  While it will be nice to get rid of the kdeinit hack, the real benefit will be that people will no longer be able to blame the linker to hide KDE's *real* performance problems.  Then maybe we will finally see some progress."
    author: "not me"
  - subject: "Re: What I have liked the most:"
    date: 2002-07-19
    body: "> Like Waldo says, I really wish that GCC/Binutils would hurry up and get relinking ready, and not because it would make KDE faster. It actually won't. KDE has already hacked around the problem with \"kdeinit.\"\n\nRead his statement again. kdeinit links against common libraries used by KDE and thus can only help to speed up these. Not other libraries, the program's libraries or the program itself. This is where prelinking helps to improve speed."
    author: "Anonymous"
  - subject: "Re: What I have liked the most:"
    date: 2002-07-19
    body: "Standard? Without distributor patches? Current libelf, binutils and Linux 2.4 have support. Only missing piece is glibc 2.3 which is said to be still released this year (perhaps RedHat exerts pressure on glibc maintainers because of RedHat 8.0).\n\nBut I heard the current approach to modify library headers is not very clever security-wise regarding checksums (you have to re-prelink a library if any library it depends on changes). So it perhaps has to be changed to store prelink information under /var instead."
    author: "Anonymous"
  - subject: "KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "What has been smoking. Here on my 2Ghz P4 with 1GB ram I am still having some performance issues and slow loading! Would it not be a better approach for developers to sort out fundamental performance issues before adding more features? Why not concentrate on bug fixes, performance enhancements and usability issues for the next few releases. There are more than enough features in KDE to keep ppl happy for the time being."
    author: "Natra"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "I swear, somewhere there is a script, generating these things..."
    author: "jd"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "Maybe you have read the interview: one fundamental performance problem (delayed icon loading) is being addressed.\nKDE runs usable on a K6/200 128 MB, and yes, 48 MB looks a little bit insufficient.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "Not being a developer I wonder why 48 MB, which looks to be a lot, is not enough.\nIMHO a desktop enviroment shouldn't take that much as it's just a framework where the apps run.\nIt's the apps which have to use the CPU and RAM.\nI think that it's not the OS fault as I can run linux with little memory on my IPAQ and perfroms really well.\nBut I might be missing something.\nAnyway I guess having a really polished enviroment is not very easy without paied developers.\nIt's REALLY A LOT what most of them are doing for free, so thanks guys!\nCan anybody explain \"in an easy way\" why does KDE need so much memory?\n\nThanks"
    author: "Anonymous"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "I share your oppinion. I had to upgrade to 512 Mb because KDE was eating too much of my original 256 Mb of ram causing massive swapping when I was working.\n\nI do wish KDE would run lighter. Just starting konq eats 100% cpu time for 2-4 seconds on my 900MHz machine. \n\n"
    author: "heh!"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "Hey, and i planned to buy a new computer soon. It seems my PII 233MHZ with 192MB\nstill outperforms all machines out there."
    author: "Lenny"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "KDE3 runs just *fine* on this computer. I just wish it would run even better. "
    author: "heh!"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "That's just stupid.  KDE runs fine in my 128M box."
    author: "ac"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "i have 256mg and i hardly ever swap. "
    author: "Echo6"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "One advantage of KDE is that the framework provides a lot of features to applications, so that application developers can focus on their application. The  interest is that all KDE applications can benefit from these features, and that all these features (hidden in a small set of libraries: kdelib, ...) are loaded only once in memory, and then shared by all the KDE applications. This is why the memory is taken by KDE and not be an application.\n\nThese features include: standard dialogs (color chooser, file chooser, ...), component system, mimetype associations, inter applications communication (DCOP),  XML gui, handling of global and local configuration file, transparent access to many file protocols, ...\n\nMost of these features are generic. The interest is that they provide a very generic interface that can be used in many cases. They use C++ features such as template and virtual function to be generic. But this generic aspect creates more classes than you would require to implement just one particular case of this interface. The growing number of classes and virtual functions is what the linker has a problem with.\n\nBut the result is great. With KDE's file dialog, you can access local file system, ftp sites and http sites and many more. And you can preview your files, which uses two other mechanism (mimetype database and components). You can not do that with your IPAQ.\n\nBe assured that stability, speed and memory consumption are watched very closely by developers and they do their best to reduce it.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "Will, in your oppinion, KDE apps boot fast with the problem of the linker solved?\nIs really only a linker problem?\nI mean how much of the start time can be reduced with an improved linker?\nWill apps boot twice faster? Three times faster? Or 20% faster?\nI'd really like to know what are we waiting for when we hear \"Just wait until the linker is optimized and apps will boot faster\".\nThanks."
    author: "Anonymous"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-19
    body: "KDE 2 konsole takes here 1.9 sec to start, linking is almost one second.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: ">IMHO a desktop enviroment shouldn't take that much as it's just a framework where the apps run.\n> It's the apps which have to use the CPU and RAM.\n\nIf apps need the same functionality, it is a good idea to put this into a library shared by those apps.\nThis is what KDE does. It provides common functionality inside the KDE libraries.\n\nIf you watch an output of top, you'll see that most part of the memory consumption of a KDE app is shared with other processes.\nSo KDE apps have little memory overhead, common code is shared between them.\n\nIMHO it is far better to have 20 MB used by KDE and only 5 MB overhead per app than to have 10 MB per app.\n\nCheers,\nKevin\n\n\n\n"
    author: "Kevin Krammer"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "> Can anybody explain \"in an easy way\" why does KDE need so much memory?\n\nIt does an awful lot of things.\n\nAnd memory usage reporting tools (top, free, ps...) are generally not reliable.\n\nAnyway, RAM is cheap, just add some more and get over it, or run something lighter. \n\nThere's no Lost Art of Good Programming, no magic formula : features take ressources, period. We're not malloc-junkies or sloppy coders. There's always room for some optimisations, but nothing will make KDE fit into a ten year old machine."
    author: "Guillaume Laurent"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "\"There's always room for some optimisations, but nothing will make KDE fit into a ten year old machine\"\n\nThe problem is that even a 5 years old machine (pentium 133 with 16MB ?) is not enough. But on this machine, we were running Win95 with softwares like word95 or excel that were doing a good job (even if the os crashed a little bit too much).\nWhat I do not understand is why do we need today to run 2Gig machine with 512MB to do the same work?? (and sometimes, it runs slower...).\nI agree that nowadays machine are much better at handling big graphics and video but for normal office work...\n\nSomething else that I do not understand is why a simple application like the calculator take so long to start? Yeah, I know, the linker problem... but is there so many links to make on a so small application??\n\nSorry for the critics... "
    author: "Murphy"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "Win 95 GUI is more comparable to KDE 1.1.2 than KDE 3.1. KDE 1.1.2 FLIES on such a machine ;-) Win XP does not run on 5-year old machines either."
    author: "Anonymous"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "Maybe KDE 1.1.2 flies on a pentium 133. But the new versions of koffice do not run on it...\nand do the differencies between KDE 1.1.2 and 3.0.2 justify that I now need a such beast (2Gig, 512mb) to run office software ?"
    author: "murphy"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "Someone has lied to you.  You can run 3.0.2 in much less than a 2GHz, 512M machine.  Hope this helps!"
    author: "ac"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "Of course I know that, I am running it on a K6 550 with 256mb. What I mean is that with a such configuration, win98 is way faster than mandrake + kde (any version), even just to run office software."
    author: "murphy"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "Get an old version of StarOffice, or try apps like AbiWord, Gnumeric, etc.\n"
    author: "Carg"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "> Win 95 GUI is more comparable to KDE 1.1.2 than KDE 3.1. KDE 1.1.2 FLIES on such a machine ;-)\n\nSorry, but I have to disagree. Win95 is still *a lot* faster than KDE 1 on my Pentium 166 (before I upgraded my CPU) with 48 MB RAM.\nYou don't want to try KDE 1 on a machine with only 16 MB RAM!\n\nWin95 runs fine on 16 MB (but swaps a lot), but KDE 1 or GNOME 1.0 are INCREDIBLY slow on a system with 16 MB RAM. I believe it took 5 seconds to popup GNOME's main menu."
    author: "Stof"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "Win95 has very SIMPLE graphics subsystem, tightly integrated with the kernel, using all the abilities of hardware acceleration. I've run it pretty reliably on 8 MB system.\nLinux has got X Window system with plenty of abilities ( network transparency and all that) that needs a lot of resources and is slow as hell on older hardware - just because of its abilities. Add the KDE overhead to that. We ran KDE1 on Pentium Pro 200 with 32 MB ram, and it was slow as hell - till we added more RAM, up to 128 MB."
    author: "Alex"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-19
    body: "example kcalc (still KDE2 at this machine):\n\nneundorf@video:~$ ldd /opt/kde/bin/kcalc\n        kcalc.so => /opt/kde/lib/kcalc.so (0x40017000)\n        libkdeui.so.3 => /opt/kde/lib/libkdeui.so.3 (0x40041000)\n        libkdecore.so.3 => /opt/kde/lib/libkdecore.so.3 (0x40227000)\n        libkdefakes.so.3 => /opt/kde/lib/libkdefakes.so.3 (0x40355000)\n        libdl.so.2 => /lib/libdl.so.2 (0x40360000)\n        libDCOP.so.1 => /opt/kde/lib/libDCOP.so.1 (0x40364000)\n        libqt.so.2 => /usr/local/kylix2/bin/libqt.so.2 (0x40386000)\n        libpng.so.2 => /usr/lib/libpng.so.2 (0x40a38000)\n        libz.so.1 => /usr/lib/libz.so.1 (0x40a62000)\n        libjpeg.so.62 => /usr/lib/libjpeg.so.62 (0x40a71000)\n        libXext.so.6 => /usr/X11R6/lib/libXext.so.6 (0x40a90000)\n        libX11.so.6 => /usr/X11R6/lib/libX11.so.6 (0x40a9e000)\n        libSM.so.6 => /usr/X11R6/lib/libSM.so.6 (0x40b77000)\n        libICE.so.6 => /usr/X11R6/lib/libICE.so.6 (0x40b80000)\n        libstdc++-libc6.2-2.so.3 => /usr/lib/libstdc++-libc6.2-2.so.3 (0x40b97000)\n        libm.so.6 => /lib/libm.so.6 (0x40bdd000)\n        libc.so.6 => /lib/libc.so.6 (0x40bff000)\n        /lib/ld-linux.so.2 => /lib/ld-linux.so.2 (0x40000000)\n\nlibc, libm, libstdc++, ld-linux.so, ok\nlibX11 and libXext since it is a X app\nlibkdecore, libkdeui, libqt well, it is a KDE app\nlibjpeg, libpng - it supports icons'n stuff\nlibdl - KDE supports plugins\nlibDCOP - dcop supprot\nlibICE - needed for dcop\nlibSM - session management\nlibkdefakes - portability fakes\nlibz - gzip compression, I think libpng requires this\n\nSo, not much room left to improve...\n\nAlex\n\n"
    author: "aleXXX"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "I'm not a developer, but win98 needs about 64mb & windows pro2000 need 128 so I think 48mb is a small amount required. It also depends what the OS maker wants to recommend. Microsoft says you can run win95 on 4mb ram (you can but you wouldn't want to)if they were serious they would recommend at least 32.Also as you add more features to an OS it requires more memory to hold the info in a temporary place. That is why DOS doesn't require alot of ram. I hope this helps."
    author: "Jim"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "DOS can't even USE more RAM.\n\n\"640k is enough for everybody\" - Bill Gates."
    author: "Stof"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-17
    body: "On my old Pentium 233 with 48 MB RAM, KDE 2 runs fine. It just swaps too much after using it for a while.\nBut what I don't understand is why X + toolkit + desktop environment need so many memory (no I don't look at top's output, I use my feelings; my harddisk tells me that the system swaps *a lot*). Why is Win95 so fast? Win95 is an incredibly complex development platform."
    author: "Stof"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "If you didn't tell us anything about your (Linux) kernel, libs, distro, etc.\nI think you are comparing apples and oranges.\n\nSwapping is mostly a kernel (VM) issue.\nOnly the lastet 2.4.19rc1aa2 (soon 2.4.19rc2aa1) kernel has smooth VM behavior. Even in the corer cases. The -ac series comes close but with some distance.\n\n-Dieter\n\nPS Compile Qt, kdebase3, glibc and XFree86 your self and you can see it fly."
    author: "Dieter N\u00fctzel"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "You're kidding right... compile them on a Pentium 233?"
    author: "Stof"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "I did it on a pentium 150 laptop with 32 Mb RAM :)\nActually I compiled all my system (Gentoo Rules!) on it :)\n\nIt took 5 days (mainly cause I had to recompile X 3 times... kernel sources ate all the HD space)\n\nNow I use KMail on it when I'm away from my main machine. It takes like a minute + to start but then it works perfectly....\n\nAnd it handles 9Mb of Mail quite decently :)"
    author: "Claudio Bandaloukas"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "One minute swapping or linking?"
    author: "Anonymous"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "Hey win95/8 on that machine will run faster.\nNew Gnome it's said it's lighter than 1.4\nTry that one... This might be faster?\n\nMy brother is running Win2000 in a 266 with 96MB ram and works decently for email, Messenger, Explorer, and \"joe user\" stuff. Before the 64 MB upgrade I was running Win95 and it flied...\nI had to buy a bigger machine to run KDE faster and gave my bro the \"old\" machine. And my bro still laughts at me when he sees konqui in my new machine loading slower :-P But anyway, I really like KDE and enjoy using it a lot.\nI think I've spent more money on Hard than I would have saved on Licenses since I'm adicted to linux :-) It's strange but I think it's just a matter of community which sticks me to Linux/KDE\n\nGood luck!"
    author: "150 laptop?"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "NO, that's why we have \"cross platform compilation\".\n\nRegrads,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "Hi Dieter and other developers,\n\n>>PS Compile Qt, kdebase3, glibc and XFree86 your self and you can see it fly.\n\nThis is exactly the problem, only developers can do this. In order to profit from new introduced features and speedups, one has to use the latest versons of gcc, glibc, Xfree86, KDE and all programms/libraries that these programms depend upon. Upto now I have not seen a manual that explains me how to compile these programms from scratch (starting from p.e. SuSE 8.0). \nSo what I would like to see is something like\n\nInstall SuSE 8.0 with al least the following software installed:\n<list>\nNow download the following software:\n<list>\nCompile the software in this order\n<list> ./configure <options>\n\n...\n\nOther things that have to be done to have a nice KDE\n<for nice fonts see www...>\n...\n\nThe user-base of kde would be much larger and using\nLinux/KDE would get a boost, since hungry users would no longer\nhave to wait for new distributions.\n<Off-topic>\n(I would still like to support distributions, but why has no distribution a European bank-account?)\n</off-topic>\n\nI *really* hope this message will get some attention on kde-devel since I believe it's this that's holding people back. (Look at the increasing popularity of Gentoo)\n\nRegards,\n\nRichard Boom"
    author: "Richard"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "YES\nI agree with you\n\nKDE is not going to write basic system config tools as they told us to use distributions tools.\nOk. I like KDE and I'm going to use distributions as they told us.\nNow, if KDE needs to be recompiled to get speed busts, explain how to do that, please.\nI agree that I have to get a distribution, but then I don't have to use the distribution binaries if I want a speedy KDE?\nOr, even better than that: \"Please explain the distribution engineers how to compile KDE, as reading KDE developer posts, looks like distro engineers are doing a pretty bad job.\nThanks guys."
    author: "Anonymous"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-19
    body: "\nVisit these pages:\n\nhttp://developer.kde.org/source/anoncvs.html\nhttp://developer.kde.org/build/compile_cvs.html\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-19
    body: ">> Upto now I have not seen a manual that explains me how to compile these programms from scratch (starting from p.e. SuSE 8.0).\n\nHave a look at garnome.\n\nhttp://www.gnome.org/~jdub/garnome/\n\nIt was originally made to automate download, compile and installation of gnome.  You can do this with only a little more than a simple text file edit, a few 'cd's and about three 'make install's.\n\nThe maintainter (Jeff Waugh) added KDE 3.0.2 to it and you should be able to compile KDE with about this much ease all into its own directory.\n"
    author: "Jared Sulem"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-22
    body: "> I *really* hope this message will get some attention on kde-devel since I believe it's this that's holding people back.\n\nand i really hope it'll get some attention on XFree86-devel (or so), because X is a great source of problems for the Linux desktop:\n- font handling is close to a plain joke\n- simple tasks such as switching screen resolution (not zooming), are simply a nightmare (you have to reconfigure X: explain to the linux newbie to log under root, edit XF86Config, and stuff...)\n- \"advanced features\" such as 3D or XVideo are poorly implemented (i have yet to find a graphic card for which it is correct: still not implemented for Mach64, crashing X regularly for Rage128/Radeon, proprietary for NVidia - and so, not very stable). btw KDE benefits of some of these features (antialias for instance), but nobody ever talk about graphic cards when complaining about KDE.\n\nand moreover, X is so complex, that jumping in its development is harder than climbing on top of Everest. all that for what ? so-called \"network transparency\", that barely nobody needs, but induces a nightmare about not-breaking-compatibility-with-century-old-clients.\nsee what the guys from Apple did in MacOSX, simply after deciding to get rid of compatibility issues. (well, till Jaguar, their engine was slow, though).\n\ni just hope it doesn't look too much like a troll ;-)"
    author: "loopkin"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2005-03-30
    body: "How about this...\n\nKDE runs extremely slow on my 2.4Ghz, 256MB Ram.  When I switched to XFCE desktop environment, it runs EXTREMELY fast (as long as I have no KDE apps open, lol)"
    author: "wescott"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "Here are my own point of view on \"Why Linux+X+KDE is slower than Windows on the same machine ?\"\n\n1) First, it is not the fault of Linux. Every test I have seen gives the Linux kernel faster than any version of Windows and Windows NT on equivalent functions.\n\n2) Windows is faster in graphics drawing, because it is integrated in its kernel, and because its drivers are made by the constructors, so there are highly optimized.\n\n3) Windows is faster in graphics drawing, because graphics seems to have a higher priority than the other system tasks. (It is a psychological choice).\nBut maybe I'm wrong on this point (X-Windows has a higher priority too).\n\n4) X-Window is slower, because when you send it a primitive, it is marshalled, sent to a socket, unmarshalled, and then managed by the X server. It is many times slower than making a system call ! Especially when you just want drawing a line or a rectangle...\n\n5) X-Window is slower, because its drivers are sometimes, on some functions, not optimized.\n\n6) X-Window is slower, because when you want to deal with a bitmap, you must download it from the X-server to your program, process it, and then upload it to the X-server. When the bitmap is huge, the unnecessary data copy is a bottleneck. With the X-SHM extension, this process is a bit quicker, but stays not optimal.\n\n7) Graphics application under Linux may be slower, because of not using the special instructions of x86 processors (MMX, SSE, etc.). They can't because they must be compiled on other processors than the x86 ones.\n\n8) Linux is slower in launching programs because of the well-known dynamic library loader in glibc. This problem should be adressed with GCC 3.1. I hope so !\n\n9) Windows is faster in lauching programs because of many internal optimizations :\n\n- Dynamic libraries are preloaded.\n- Many dynamic libraries are preloaded at the same address for every process that use them.\n- Programs like Explorer or Office are preloaded (it is a psychological optimization !)\n\nThese optimizations are possible because, under Windows, 90% of the executed code comes from Micro$oft, and because dynamic libraries of Windows have less\nfunctionalities than the ELF format used under Linux.\n\nIt is not possible to do the same thing in a free OS like Linux. Maybe you could do these optimizations with glibc, which is used everywhere. But you couldn't go as far as Microsoft did.\n\nThe KDE guys did their best with kdeinit. With GCC 3.1, the things will be better. But I think the major bottleneck resides in X-Window.\n\nI'm optimistic. I think graphics under Linux will be faster than Windows in a few years, with a transformed X-Window, or an alternative.\n\nBe patient ! Freedom is more important than speed...\n\n"
    author: "Beno\u00eet Minisini"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "I already forgot about the gcc 3.1 sutff because, as Waldo said, this has been mostly solved by kdeinit. After all, gcc people don't have the fault of KDE loading slow. A lot of people should apologise to gcc people I think.\n\nI hope we don't blame now X if it's not their fault!"
    author: "me"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "> Windows is faster in graphics drawing, because it is integrated in its kernel,\n\nIn kernel space doesn't automatically mean faster!\n\n\n> Windows is faster in graphics drawing, because graphics seems to have a higher \n> priority than the other system tasks.\n\nSo renice -15 `pidof X` will make everything faster? I've hardly noticed any improvement in performance. And except when drawing big animations, X uses very little to almost no CPU power.\n\n\n> 4) X-Window is slower, because when you send it a primitive, it is \n> marshalled, sent to a socket, unmarshalled, and then managed by the X server.\n\nMost of the data (pixmaps) are locally transferred using shared memory.\n\n\n> 7) Graphics application under Linux may be slower, because of not using the \n> special instructions of x86 processors (MMX, SSE, etc.).\n\nAll the Linux video players I've seen (Mplayer, Xine, etc.) have *heavily* optimized using assembly. Even Gdk-Pixbuf contains MMX code.\nAnd how much difference does it really make if MMX/SSE/3Dnow is used in drawing static graphics? Windows are only drawn once every expose, and CPUs these days are 2+ Ghz.\n\n\n> They can't because they must be compiled on other processors than the x86 \n> ones.\n\nAll you have to do is\n#ifdef PENTIUM_MMX\n    // optimized assembly code\n#else\n    // portable C/C++ code\n#endif\n"
    author: "Stof"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-18
    body: "> > Windows is faster in graphics drawing, because it is integrated in its kernel,\n>\n> In kernel space doesn't automatically mean faster!\n\nNo, but it helps a lot...\n\n>\n>\n> > Windows is faster in graphics drawing, because graphics seems to have a higher\n> > priority than the other system tasks.\n>\n> So renice -15 `pidof X` will make everything faster? I've hardly noticed any improvement in performance. And except when drawing big animations, X uses very little to almost no CPU power.\n\nOn my PII-400, X has a -10 priority.\nMoving the mouse takes about 2.5% of CPU\nMoving a window with contents easily takes 30% of CPU\n\nGraphics advantage is evident under Windows. Under Windows NT, I do not know if graphics are managed by\na special kernel thread, or something equivalent.\n\n>\n>\n> > 4) X-Window is slower, because when you send it a primitive, it is\n> > marshalled, sent to a socket, unmarshalled, and then managed by the X server.\n>\n>  Most of the data (pixmaps) are locally transferred using shared memory.\n\nYes, but primitives does not use shared memory. When a window must be redraw, it send\na lot of line, rectangle, pixmap, font drawings. I think if the primitive stream sent\neach time a window is redraw was cached by the X-server, drawing speed with X could \ngreatly improve.\n\n>\n>\n> > 7) Graphics application under Linux may be slower, because of not using the\n> > special instructions of x86 processors (MMX, SSE, etc.).\n>\n>  All the Linux video players I've seen (Mplayer, Xine, etc.) have *heavily* optimized using assembly. Even Gdk-Pixbuf contains MMX code.\n>  And how much difference does it really make if MMX/SSE/3Dnow is used in drawing static graphics? Windows are only drawn once every expose, and CPUs these days are 2+ Ghz.\n>\n\nYou are completely right. I didn't say that -no- application use MMX/SSE/3Dnow. For example, reading \na DivX ;-) movie with mplayer under Linux is faster than reading the same DivX under Windows XP,\neven if you use the -same- codec (on my PII-400).\n\n>\n> > They can't because they must be compiled on other processors than the x86\n> > ones.\n>\n>  All you have to do is\n>  #ifdef PENTIUM_MMX\n>  // optimized assembly code\n>  #else\n>  // portable C/C++ code\n>  #endif\n\nYes. If you have time... And this kind of optimizations is mainly needed in video codecs, image effects\nand sound effects. Using them under other free software seems to be recent.\n\nMaybe under a fast machine (like I do not have), the bottleneck is more in\ndisk access and library loadings, and on slow machines it is in graphics\ndrawing.\n\n"
    author: "Beno\u00eet Minisini"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-19
    body: "But the slowness of kde is not only in the graphics and in the loading of applications. Try suppressing 1000 files in a directory or move 1000 files from a directory to another, under windows or in a shell under linux, it is going fast (time measures in second). Under Konqi, it needs several 10 minutes, especially to move the files.\nYou will tell me that we do not move 1000 files every day. But I do."
    author: "Murphy"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-19
    body: "I am really disturbed by all these talks.  I use KDE and love using it, the brilliance it adds to the functionality, the integrated file and web browsing... I have a PIII 667MHz with 64 MB RAM, and KDE has been working well on this machine.  Applications take some time to load, but once loaded they are fast.  Will I get  a similar performance with KDE3 which I am going to upgrade to, or is KDE3 significantly slower than KDE2.  But sure, from my experience with many desktops I have used I can tell KDE is simply the best. Windows Desktop and the whole OS are only toys compared to KDE/Linux"
    author: "Asokan"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-20
    body: "> Moving the mouse takes about 2.5% of CPU\n> Moving a window with contents easily takes 30% of CPU\n\nI really hate it when people talk about how much CPU% a process takes up.\n\nAny running process, no matter how simple, will take up 100% of CPU time for the duration it is running.\n\nplain CPU usage is one of the most pointless measurements there is.\n\n_Average_ CPU usage over X seconds where X is actually mentioned along with the CPU usage does mean something, but a CPU usage number on it's own means nothing."
    author: "Stuart Herring"
  - subject: "Re: KDE on a 166mhz PC with 48MB ram!!!"
    date: 2002-07-19
    body: "> All you have to do is\n> #ifdef PENTIUM_MMX\n> // optimized assembly code\n> #else\n> // portable C/C++ code\n> #endif\n\n\nAnd this code will not get used by most people, since none of the common binary distros requires a MMX-capable CPU on IA-32 targets. The right way to do this is with CPUID and runtime testing. "
    author: "Sad Eagle"
  - subject: "KDE Trash"
    date: 2002-07-17
    body: "A small question. Is it possible to put the trash-icon in KDE onto the panel and then use it from there with all the functionality? Even in real life, I prefer not putting the trash onto my desk."
    author: "Alex Ivarsson"
  - subject: "Re: KDE Trash"
    date: 2002-07-17
    body: "LOL. Funny off-topic :-)\nJust drag and drop it from desktop to kicker.\nIt looks to work on my system"
    author: "Anonymous"
  - subject: "Re: KDE Trash"
    date: 2002-07-18
    body: "hehe... good one :o)"
    author: "me"
  - subject: "Tounge-in-cheek"
    date: 2002-07-19
    body: "What exactly does tounge-in-cheek mean?"
    author: "Johnny Andersson"
  - subject: "Re: Tounge-in-cheek"
    date: 2002-07-19
    body: "Johnny,  \n\nhttp://www.dictionary.com/search?q=tongue-in-cheek\n\nIt means jokingly or not seriously.\n\nCheers,\nGK"
    author: "garyK"
  - subject: "Re: Tounge-in-cheek"
    date: 2002-07-20
    body: "Thanks! I even managed to spell it wrong. ;)\n"
    author: "Johnny Andersson"
  - subject: "Re: Tounge-in-cheek"
    date: 2002-10-16
    body: "help"
    author: "What does it mean>"
  - subject: "Re: Tounge-in-cheek"
    date: 2005-08-20
    body: "What kind of help do you need:\n\na) physical\nb) mental\nc) emotional\nd) all of the above"
    author: "Helper"
  - subject: "Re: Tounge-in-cheek"
    date: 2008-01-19
    body: "It's just something that bloody idiots that can't spell say when they're quipping."
    author: "Mafferz"
---
<a href="http://www.osnews.com/">OSnews</a> is running an
<a href="http://www.osnews.com/story.php?news_id=1370">interview</a>
with the dot's very own
<a href="http://www.kde.org/people/waldo.html">Waldo Bastian</a>.
Besides his long-running contributions to a large range
of the KDE libraries and other desktop infrastructure, Waldo is
well known as the KDE 2 "release dude", for helping keep the KDE
infrastructure operating, and as an inexhaustible supply
of knowledge and tips to other developers on IRC.  In his at times
serious and at times tongue-in-cheek interview,
Waldo talks about the growth of Linux, his employer
<a href="http://www.suse.com/">SuSE</a>,
<a href="http://www.gnome.org/">GNOME</a> competition and cooperation,
the role of
<a href="http://www.unitedlinux.com/">UnitedLinux</a>, and KDE's performance,
and also reveals a secret way to get
<a href="http://www.trolltech.com/">Trolltech</a>
to add requested features to Qt:  "<em>Catch a live Troll, lock it up
and feed it beer till it promises to make whatever you need.</em>"



<!--break-->
