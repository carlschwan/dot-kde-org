---
title: "People of KDE: Christian Couder"
date:    2002-01-08
authors:
  - "Inorog"
slug:    people-kde-christian-couder
comments:
  - subject: "Eiffel : higher and higher"
    date: 2002-01-08
    body: "\"on integrating Eiffel language development support into the third iteration of KDevelop\"\n\nWhaou, it's a very good idea. I love Eiffel, it's my favorite OO language (even if KDE is developing with C++ ;)."
    author: "Capitaine Igloo"
  - subject: "how about Haskell suport :("
    date: 2002-01-09
    body: "That would be a very good idea. It's my favourite functional language... \n\n[I know... stupid post, but you never know if someone is going to love the idea and do it ;) \nPlus, gtk already has haskell bindings, there has been a lot of talk about a GUI library for haskell and I'd love to see it done in Qt+KDE :)]"
    author: "Simplemind"
  - subject: "Re: how about Haskell suport :("
    date: 2002-01-09
    body: "Did Haskell support C++ bindings?"
    author: "chouimat"
  - subject: "Re: how about Haskell suport :("
    date: 2002-01-09
    body: "This should be enough,\nhttp://www.haskell.org/libraries/#interfacing\n\nThere are C bindings for Qt now..."
    author: "Simplemind"
---
<a href="http://www.kde.org/people/christian.html">Christian Couder</a> is a contributor to the <a href="http://www.kdevelop.org">KDevelop</a> project. He is working on integrating Eiffel language development support into the third iteration of KDevelop (codename: Gideon). Christian answers Tink's questions in this week's edition of the <a href="http://www.kde.org/people/people.html">People Behind KDE</a>.
<!--break-->
