---
title: "IBM developerWorks: Creating KParts components, Part 2"
date:    2002-06-21
authors:
  - "Frank"
slug:    ibm-developerworks-creating-kparts-components-part-2
comments:
  - subject: "eDonkey now has \"Creating KPart components\""
    date: 2002-06-21
    body: "Hello ppl :)\n\nCreating KParts components, Part 1 & 2 are now ready for download on eDonkey.. so you dont have to trouble with registering :)\n\nJust search for \"macavity\" and you will get nothing but KDE related stuff :)\n\n(Am i just a nice kind of guy or what ;-)\n\n/macavity"
    author: "macavity"
  - subject: "Re: eDonkey now has \"Creating KPart components\""
    date: 2002-06-22
    body: "I find the IBM developerWorks webpages very interesting for all kinds of information.\n\nThis might sound a vague argument, but you'll never know until you tried it yourself.\n\nThe registration is only minor inconvenience if you consider what you get for free in return.\n\n"
    author: "kaltan"
  - subject: "Re: eDonkey now has \"Creating KPart components\""
    date: 2002-06-22
    body: "I tend to look at *why* I'm registering.  In this case, it's pretty much to track to see if this series is popular enough to continue.  As such, I'm more than happy to register... and encourage other developers to register as well.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: eDonkey now has \"Creating KPart components\""
    date: 2002-06-22
    body: "I hope they aren't killing interest in the series due to the registration thing.  Look at LinuxToday and LWN neither of them have linked these series so far."
    author: "ac"
  - subject: "Re: eDonkey now has \"Creating KPart components\""
    date: 2002-06-24
    body: "Registration is a pain.\n\nRegistering once for one site is ok, but you're asked to register for so many things nowadays on the net, it's ridiculous. You end up having to manage several userids and passwords and for what? Non-secure content and no added value.\n\nIf you're IBM, what are the advantages of having your users sign-up anyway? You can be sure that most of the information collected is made up or redundant (I must have signed up three times over the past couple of years for the NYTimes on-line; how much fake personal info can they handle?)\n\nHave people register if you want to provide extra services to registered users (archives and what-nots...) otherwise just forget about it. It makes the web less usable."
    author: "Satisfied KDE User"
  - subject: "Re: eDonkey now has \"Creating KPart components\""
    date: 2002-06-24
    body: "I am not sure IBM developerWorks gives permission to redistribute. Did you check this?"
    author: "David Faure"
---
Take these two tutorials, if you want to learn how to use KParts components in KDE. The first in the series, <a href=http://www-105.ibm.com/developerworks/education.nsf/linux-onlinecourse-bytitle/A96500917FA1DB0786256BC000808C0F?Open&t=gr,lnxw43=Kparts1>Creating KParts components Part 1</a>, covers read-only, read-write parts and network transparency. The <i>new tutorial</i>, <A href=http://www-105.ibm.com/developerworks/education.nsf/linux-onlinecourse-bytitle/380CD6A86A7ACDBE86256BD50078C711?Open&t=gr,lnxw43=KParts2>Creating KParts components Part 2</a>, covers the KDE Trader, user interface merging, and the Part Manager. Both are well-written and will get you creating KParts components for KDE in no time at all. <i>[Ed: One-time free site registration.]</i>
<!--break-->
