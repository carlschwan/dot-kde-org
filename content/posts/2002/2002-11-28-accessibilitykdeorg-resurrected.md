---
title: "Accessibility.kde.org resurrected"
date:    2002-11-28
authors:
  - "oschmidt"
slug:    accessibilitykdeorg-resurrected
comments:
  - subject: "cooperation with gnome"
    date: 2002-11-28
    body: "Cooperation with GNOME is really cool and all, but it's important not to make such a big part of KDE dependent on GNOME, CORBA, GTK etc  despite what I read on the list about the mistaken notion that KDE would ship Mozilla or GOP and therefore need the GNOME libraries.  Terrible.\n\nThere is no reason the backend needs to depend on either desktop if properly designed.  Each desktop should be able to implement whatever is needed on top.  If the backend is badly designed it has to be redesigned.  Otherwise, I am very excited about the keeness behind this project.  KMouth sounds like it would be a blast.  It is all a very tough job.  Good luck!"
    author: "anonymous"
  - subject: "Re: cooperation with gnome"
    date: 2002-11-28
    body: "Who's talking about making something dependent on something else? Interoperability by far doesn't mean that future KDE packages ships with Gnome and vice versa, so calm down."
    author: "Datschge"
  - subject: "Re: cooperation with gnome"
    date: 2002-11-28
    body: "SUN people think its a good idea see accessiblity list."
    author: "anonymous"
  - subject: "Re: cooperation with gnome"
    date: 2002-11-28
    body: "Don't worry. We won't make KDE dependent on Gnome."
    author: "Olaf Jan Schmidt"
  - subject: "Re: cooperation with gnome"
    date: 2002-11-28
    body: "As far as I know, the dependancy is on GObject and Orbit, because that's what the AT-SPI is based on. Being the first accessiblity toolkit for Unix, the most widely used (Mozilla, OpenOffice, Gnome, Java) and the most successful, it makes sense to use it.\n\nI wonder why you post anonymously."
    author: "Philippe Fremy"
  - subject: "Re: cooperation with gnome"
    date: 2002-11-28
    body: "Just to clear things: KDE itself will not depend on GObject and Orbit. The existing AT-SPI solutions will be reached by a bridge probably depending on GObject or Orbit. This bridge won't be part of kde-core. It will be optional."
    author: "Olaf Jan Schmidt"
  - subject: "Re: cooperation with gnome"
    date: 2002-11-28
    body: "Will the bridge go two ways?  KDE screen reader will be able to read GNOME applications?"
    author: "anonymous"
  - subject: "Re: cooperation with gnome"
    date: 2002-11-28
    body: "Yes."
    author: "Philippe Fremy"
  - subject: "Re: cooperation with gnome"
    date: 2002-11-28
    body: "KDE was the first successful desktop for Linux and most widely used.  It makes sense to use it."
    author: "anonymous"
  - subject: "Re: cooperation with gnome"
    date: 2002-11-28
    body: "And this is what the KDE-accessibility is going to do, use KDE. They are also going to leverage on the existing accessibility framework that was developed by Sun. This framework contains the fruit of many man-year and more than 10 years of experience with disabilities. This is far more than the experience of any coder on the KDE-accessibility list on this topic.\n\nIf you disagree with the choice of the KDE accessibility project and want to start a project that compete with ATK or AT-SPI but based on KDE, this is perfect for me. I'll wish you good luck. I am sure that the KDE accessibility will make room for your projects if you ever finish it.\n\nHowever, if you just whine about KDE not developing its own technology and if you do not contribute anything, I suggest you just shut up. The KDE way is to do things, not to whine or talk for nothing.\n\nAnd really, I don't see why you keep posting anonymously. Are you ashamed of your identity ?\n\n\n\n"
    author: "Philippe Fremy"
  - subject: "Re: cooperation with gnome"
    date: 2002-11-29
    body: "First of all I want to take issue with your abuse of anonymous posters.  Just because someone chooses to post anonymously does not imply they have nothing important to say nor does it imply that they are ashamed of identity or the comments they've posted.  It might feel nice to call someone out because they post anonymously, but it does nothing to further your own comments nor does it make the anonymous posters comments any less valid.\n\nSecond, I don't think the parent post was 'whining' or just talking for nothing.  He merely expressed an opinion that was partially positive as well as betraying a few misgivings.  This was not an attack and does not merit a 'shut up or do something' response."
    author: "Adam Treat"
  - subject: "Re: cooperation with gnome"
    date: 2002-11-29
    body: "I am sorry. I was pissed off and I made an unjustified agressive comment. Please apology for this.\n\nHowever, regarding posting anonymously:\n- I don't like it because you don't know if you are replying to the same person or someone else\n- I don't see any reason to hide my identity when I talk\n- I find critisizing something anonymously a coward act. You should at least stand by what you say.\n- you can't even mail the author to further the discussion if you think things need clarifications\n\nI did not dismiss the criticism. [ Nothing proves the positive comment was made by the same person that did the last one (problem of anonymous poster again) by the way ] But saying that KDE was the first widely deployed desktop is a cheap argument and does not bring much to the discussion.\n\nI find natural that people question the choices made by developers when they are not obvious. And I am open to answer any question. But I don't like the people that _critisize_ developer choices when they don't know what they are talking about (typical slashdot crowd attitude). This is disrespect toward the people that actually do the work. If you don't trust developers to make the right choices, then try to do better. But if you don't code for the project and don't know exactly the issues involved, then trust the developer.\n\nFor more information about the reason to go with ATK/AT-SPI, please subscribe to kde-accessibility and read the archives. Good explanations have been given. And if anybody wants to critisize the choice being made with valid argument, let him do it on the right place, on KDE-accessbility, with an email one can actually reply to.\n\n\n\n\n\n\n\n"
    author: "Philippe Fremy"
  - subject: "Doing great"
    date: 2002-11-29
    body: "I am blind, deaf and paralized from the neck down. Please make KDE more accessable to me."
    author: "ir"
  - subject: "Info about glib"
    date: 2002-11-29
    body: "Reading the archives I get the feeling there are some misconceptions about glib\namong some of the KDE developers. glib is in many respects the bottom layer of GNOME, but it is not a GUI development library at all and has no dependencies itself that KDE don't already have. Glib provides data structure handling for C, portability wrappers, and interfaces for such runtime functionality as an event loop, threads, dynamic loading, and an object system. \n\nUsing glib to develop C programs makes sense for all C programs even non-gui ones, and I know that many of the GNU developers are considering starting to use glib in the console tools that we all expect to be part of a standard unix implementation.\n\nSo please do not think of glib as a 'GNOME' library, it is more correct to think of it as a glibc extension to make life easier for C developers.\n\nA good example of a non-gui library that has nothing to do with desktop development, using glib, is oRTP which is an implementation of the RTP protocol.  "
    author: "Christian F.K. Schaller"
---
After several weeks of technical problems,
<a href="http://accessibility.kde.org">Accessibility.kde.org</a>
is on-line again with a completely renewed and enhanced version of the web site. You can now find a new section with
<a href="http://accessibility.kde.org/reports/">Accessibility Reports</a>,
and detailed information on the KDE Accessibility Project's
<a href="http://accessibility.kde.org/events/meeting1/">first IRC meeting</a>.

<!--break-->
<p>
The KDE Accessibility Project aims at making the entirety of the K Desktop Environment usable by and as efficient as possible for disabled users of all types. The goal is a complete accessible desktop as a free alternative to the expensive commercial assistive technologies. By cooperating with other free solutions, interoperability with already existing accessibility software (e.g. GNOME applications) can be ensured.
</p>
<p>
The new web site structure reflects that a lot of new tasks are being tackled in the KDE Accessibility Project. More people have joined the project, and some keen projects have been started.
</p>
<p>
Pupeno is writing a DCOP service for text-to-speech conversion. It will support Festival, FreeTTS, the German Hadifax TTS and all text-to-speech programs that can be started by a command line. Interoperation with gnome-speech is also planned. A first client for that service, KMouth, is being developed by Gunnar.
</p>
<p>
The challenge is now to add support for an Assistive Technology Service Provider Interface (AT-SPI) to KDE. Here the KDE Accessibility Project is still in the planning and discussion phase, as every decision on that will have great influence on other areas of KDE and Qt, and on Gnome interoperability. Fortunately, both people from Trolltech and from Gnome/Sun have joined the discussions on the kde-accessibility
<a href="http://mail.kde.org/mailman/listinfo/kde-accessibility">mailing list</a>, so there is a good chance of successful cooperation.
</p>

<p>
With so many great projects being started, there is a great need for even more  contributers to the KDE Accessibility Project -- developers, and users writing Accessibility Reports. In case the word <a href="http://accessibility.kde.org/reports/">"Accessibility Report"</a> reminds you of <a href="http://usability.kde.org/activity/testing/">"Usability Report"</a>  -- that's not far off. Some parts of Accessibility are like Usability, just with a different user group in focus.
</p>

<p>
So let's all hope that KDE will soon be fully usable by all users.
</p>