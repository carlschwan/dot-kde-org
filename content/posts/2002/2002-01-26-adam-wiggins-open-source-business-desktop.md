---
title: "Adam Wiggins: Open Source on the Business Desktop"
date:    2002-01-26
authors:
  - "numanee"
slug:    adam-wiggins-open-source-business-desktop
comments:
  - subject: "Largo?"
    date: 2002-01-25
    body: "I have posted to the mailinglist and to the Enterpise-forum, but without any luck...\n\nIs there any information available regarding the Linux/KDE-deployment in Largo, Florida? I have contacted the administrator there, and he said that some of the KDE-folks are writing a white-paper on that deployment. I could use the information in my final thesis."
    author: "Janne"
  - subject: "Re: Largo?"
    date: 2002-01-25
    body: "Haven't you done even a web search?  There are several articles on the subject.  Your question is very vague, so I'm not surprised you're not getting any answers to tell you the truth."
    author: "ac"
  - subject: "Re: Largo?"
    date: 2002-01-26
    body: "I have searched on the subject. There are articles regarding it, but I would like a bit more detailed information. What kind of server/clients, numbers, software, configuration, network, cost-savings etc. etc."
    author: "Janne"
  - subject: "Re: Largo?"
    date: 2002-01-26
    body: "http://dot.kde.org/995949998/ has all the server details and so forth. It's worth noting that Largo used SCO with KDE before Linux with KDE, so you aren't going to find any Windows to Linux cost savings information there.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Largo?"
    date: 2002-01-26
    body: "Thanks for the link :)!"
    author: "Janne"
  - subject: "Re: Largo?"
    date: 2002-01-27
    body: "Glad I could help. :-)"
    author: "Timothy R. Butler"
  - subject: "No Mouse No KDE :("
    date: 2002-01-25
    body: "using KDE without mouse is a real pain. here are some examples:\n\n1. Applets can't be run\n2. Docked applications can't be activated (like noatun)\n3. panel .desktop buttons cannot be clicked\n4. konqueror views can't be switched by pressing 'Tab' key when 'show terminal emulator' is activated.\n\nI wish KDE implement complete keyboard support. it is rare that mouse fails but it happens and then kde seems a pain. but in windows 98, it is just easy to do work without using the mouse. Hope KDE 3.x will solve this issue."
    author: "Asif Ali Rizwaan"
  - subject: "Re: No Mouse No KDE :("
    date: 2002-01-25
    body: "If you have a decent distribution, all you have to do is press Shift-NumLock and then you can use your keypad to control the pointer."
    author: "ac"
  - subject: "Re: No Mouse No KDE :("
    date: 2005-03-07
    body: "Thanks a lot pal, ur a life saver :)"
    author: "Amr Elshebiny"
  - subject: "Keyboard RHS side keywords(below Num Lock) problem"
    date: 2008-08-24
    body: "Hi \n\nM problem was , while at KDE , konsole , i cant write through keyboard RHS keywords. \nFor instance , when i press 4 , prompt at keyboard moves LHS , when press 6 , then to RHS , when press 8 , moves up and so on. \nI mistakenly pressed shift + NumLock and in haste activate \"Mouse keys\"  and  later i forgot how to deactivate. I suffred for 2 days, wasted my lot to time to set to default. \nThanks buddy your single line \"shift + num lock\" reminded and overcome my suffer.\n\nGood day.\n"
    author: "Harpreet Singh"
  - subject: "Re: No Mouse No KDE :("
    date: 2002-01-25
    body: "It is rudimentary, but if you hit ALT-F12 you get keyboard emulated mouse movement.  Using the arrow keys to move (CTRL moves slower ALT moves faster) and the space bar for left clicking, you can do many of the basic things you need.\n\nI do agree though.  I would love more accels."
    author: "Paul Seamons"
  - subject: "thou know everything :)"
    date: 2002-01-26
    body: "Thanks \"ac\" and \"paul\" for the wonderful shortcut keys, you two have said. My mouse broke and I felt helpless. Thanks again for your help."
    author: "Asif Ali Rizwaan"
  - subject: "Re: No Mouse No KDE :("
    date: 2002-01-26
    body: "Mhm, why using kde without a mouse ?\nOn windows you need it because you can't shoot the gui on a server ;)\nBut on Unix you doesn't need a gui for a server. All you need is a good Web-Interface or ssh.\n\nOk, I'm happy to see Keyboard changes in the kde3 feature list but I think withous a mouse isn't a good idear.\nMany people uses the keyboard but they have a mouse. So more features could be very usefull but I think there is no need that kde will be a keyboard only Desktop.\n\nhave fun\nHAL"
    author: "hal"
  - subject: "Re: No Mouse No KDE :("
    date: 2002-01-26
    body: "Other features would be more useful? To you maybe, but this is KDE, where people hack on what they need.\n\nCheers,"
    author: "Carg"
  - subject: "Re: No Mouse No KDE :("
    date: 2002-01-26
    body: "> Mhm, why using kde without a mouse ?\n> On windows you need it because you can't shoot the gui on a server ;)\n> But on Unix you doesn't need a gui for a server. All you need is a good\n> Web-Interface or ssh.\n\nI think you miss the point here, often things can be done faster without reaching the mouse.\nset Win32 = Windows 2000 Pro\nset Winkey = buttons next to left Alt and Right Alt on 105 key KB\nset CMK = Context menu key left of RightCtrl on 105 key KB\n\nOn Win32 you can for example do things like:\n1. Winkey+Tab, Left, Right, Home, End  -  This moves focus on on taskbar tabs without switching to app before pressing Enter/Space, handy on switching tasks when taskbar is crowded.\n\n2. Winkey+Tab Tab  -  This moves selection focus to system tray and allows bring up/manipulate things in systray without touching the mouse, move with Left/Right, leftclick action with Enter, rightclick with CMK.\n\n3. Under start menu I have appz/tools/net/etc... folders which contain shortcuts to progz, now when I pop up start menu with Winkey, press A - the appz submenu pops up, press A again and first shortcut which has first letter A is selected or executed if it's the only one with this first letter.\n\n4. Desktop toolbars: instead of having 32x32 icons cluttering desktop theres one toolbar in the left side with 16x16 icons and text to the left, shortcuts on toolbar are easily rearranged by drag&drop.\n\n5. Ctrl+Leftclick on My Computer/My Documents or any other folder on toolbar pops up menu with directory structure.\n\n6. Alt+Leftclick gives properties of toolbar item.\n\n7. And what I find most handy is program called WinKey (http://www.copernic.com/winkey/). I have loads of combinations defined, most frequently used: Winkey+S - Minimize window, Winkey+W - maximize window, Winkey+Q - close window, Winkey+A - minimize all, full list shown at http://www.hot.ee/tar/img/winkey.png\n\n8. In Win32 when window is maximized borders are removed from window.\n\n9. Option to put web page as active desktop item - resizeable, lockable, hideable, rightclick->View source->modify&save&close->rightclick->refresh\n\n10. Hold Ctrl while leftclicking on task on taskbar to select multiple tasks, then rightclick on one of selected task, choose close/maximize/tile/etc.\n\n11. Use of CMK everywhere (for example while cruising in start menu press CMK, then R to get properties for selected item), use of CMK in explorer.\n\n12. Utility PicaView by ACDsystems (2.0 is crap, use 1.31 instead) to view images with CMK in explorer\n\n13. For example in Red Hat installer there's no Shift+Tab to go back to last element, arghhh frustrating...\n\n14. Many things from http://www.hot.ee/tar/tweak.html\n\nand tons of other tiny things...\n\nMy point is that with Win32 you _currently_ can do things quicker/easier without reaching for a mouse than in KDE.\n\nI do like what KDE is becoming but for me it's missing some things that I rely on when in Win32 desktop."
    author: "Tar"
  - subject: "Re: No Mouse No KDE :("
    date: 2002-01-26
    body: "I don't mean a keyboard enabled desktop is not usefull. I also use the keyboard and every day I learn more shortcuts.\nThis is very important.\nBut I think it's not needed to use KDE completely without a mouse.\n\nThis is only my way of using a desktop. If other people don't use a mouse than of course it would be nice to make kde ready for this people.\n\n\nhave fun\nHAL"
    author: "hal"
  - subject: "Re: No Mouse No KDE :("
    date: 2002-01-26
    body: "> I think you miss the point here, \nMhm, maybe you are right ;)\n\nIt would be nice if there are more keyboard features. \nBut I don't think you need a complete no mouse desktop."
    author: "hal"
  - subject: "Re: No Mouse No KDE :("
    date: 2002-01-26
    body: "Oh, yay, another list :-)\n\nWell, time for my list-like response then:\n\n1. For KDE at least, using KDE style tabbing, you can easily configure the ability to switch between two common tasks with alt+tab. Just alt+tab from task 1 to task 2, and then back to task 1, and from then on, hitting alt+tab just once will switch between these two windows. If you have a need to do this even more, just use ctrl+tab for desktop switch, which I believe has the same feature.\n\n2. In KDE and UNIX the system tray is far less used then in Windows; most Windows daemon-like things go there. However, you can achieve a simlar effect by configuring keyboard shortcuts. Many systray apps are simply shortcuts to another program (kmix, noatun young hickory, korn) and you can then use the global app shortcuts in the K Menu editor to achieve that effect, or by configuring the app in question for it's own keyboard system (Noatun has a great global control setup system). Klipper, one of my favorite systray apps, also has it's own keyboard shortcut configuration.\n\n3. I can do this faster :-) I just hit alt+f2 and type the name of the app. This dialog also has a nice completion facility.\n\n4. Just create another kicker bar to achieve the same effect. If you don't want it taking up space whle your windows are open, configure it to pop in/out automatically upon hitting the side of the screen (which Fitt's law says is extremely fast to do)\n\n5. Control leftclick does selection in KDE, which is needed for when you make icons single click instead of double click (which most do)\n\n6. Rightclick gives properties of toolbar icons too :-) (and just about everything else, as well)\n\n7. KDE can do this too. Go to Prefs/Look n' Feel/Key bindings to set various bindings to window management tasks. And you cna use the KDE menu editor to set app shortcuts.\n\n8. This is configurable in KDE. Go to Prefs/Look n' Feel/Window behavior/Advanced and turn off \"allow resize and movement of maximized windows\" to make them fill the whole screen.\n\n9. This really doesn't seem that useful to me, but I believe you can achieve the same effect by setting a Konqueror browser as the root desktop window. BTW, I tried this out on various versions of Windows, and it always made it crash quite a bit more often.\n\n10. In KDE3, you can just use dcop to achieve this same effect. Pop up alt+f2 and enter the neccessary DCOP command to the window manager. In addition, KDE's unclutter feature is very nice. And finally, this doesn't seem that great. If I want my current window maximized, I maximize it. I have no reason to want to maximize other windows until I want to use them, in which case I just maximize them then. No time saved by doing it in advance.\n\n11.  CMK? Sorry, I don't know what that is.\n\n12. Konqueror has image previews in the icons of images. In addition, it has KOffice docuemnt previews, HTML file previews, text file previews, sound previews, and a whole bunch of stuff like this as well. And you don't need 3rd party software to do it :-)\n\n13. Red Hat installer sucks. Try the Debian, MDK, or Caldera instalelrs,  they all do this iirc.\n\nBelieve me, Alt+F2 is a godsend. Yes, Windows has this too, but UNIX is better suited to this sort of thing. The nice thing about UNIX is that generally a GUI is just a frontend to a console app or library. KDE's big muscle is the libraries it provides (KHTML and other KIOslaves, KParts, DCOP, etc). This means, generally, that you can do jsut about anything from a CLI. DOS sucks majorly compared to the awesome power contained in bash or zsh or even sh. Just do alt+f2, type konsole, and off you go to doing all sorts of awesome things with just your keyboard, while still keeping your GUI around."
    author: "Carbon"
  - subject: "Re: No Mouse No KDE :("
    date: 2002-01-26
    body: "http://www.asktog.com/basics/firstPrinciples.html\n\nto learn about interesting desktop/application behavior and expectations."
    author: "Asif Ali Rizwaan"
  - subject: "Re: No Mouse No KDE :("
    date: 2002-01-26
    body: "> Just do alt+f2, type konsole, and off you go to doing all sorts\n> of awesome things with just your keyboard, while still keeping\n> your GUI around.\n\nOr use khotkeys through the menu editor to assign a special key combo specifically to launch konsole. I personally use ctrl-alt-space and using it to fire up a quick konsole has become a second nature."
    author: "Matthijs Sypkens Smit"
  - subject: "Konqueror"
    date: 2002-01-26
    body: "\"The first is speed; Konqueror is a relatively fast program, but it is slow to load, and redraw (either as a web browser or a file viewer) flickers and pops\"\n\nI have to agree with it. Konqueror does many unnecessary repaintings when loading web pages. Of course, I often get frustrated when wainting for the www pages to load in other browsers. Netscape, IE can display something like 'Loading page ... 85% of 459kB' and nothing more for an hour because they can't find out the size of some table or picture. On the other hand Konqueror refreshes the screen a few times even when displaying simple, local pages. There should be some kind of a built-in timer, to make sure that repaintings don't occur more often then, for example, every two seconds. \nKonqueror seems bloated sometimes. I think that when it loads the page it activates KIO slave, which makes my HD working loud. After a few seconds, it expires however, and when I try to load the next page it loads and activates it again, and it expires again, etc ...\nThere are also some serious design flaws in the file manager mode. When I want to to highlight some file, I click on it. When I want to open it I double-click on it, but Konqueror immediately opens the highlighted file and I don't know how to turn it off ..."
    author: "Rumcajs"
  - subject: "Re: Konqueror"
    date: 2002-01-26
    body: "> There are also some serious design flaws in the file manager mode. When I want to to highlight some file, I click on it.\n> When I want to open it I double-click on it, but Konqueror immediately opens the highlighted file and I don't know how to turn it off ...\n\nControl Center -> Peripherals -> Mouse\n\nActually it would be a design flaw if it were the other way, because with KIO there isn't a difference between a http URL and a file URl, so treating them differently by default would be wrong IMHO.\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: Konqueror"
    date: 2002-01-27
    body: "It worked. Thanks !\n\n>there isn't a difference between a http URL and a file URl\nbut there is difference between web page view, and an URL/URI list view."
    author: "Rumcajs"
  - subject: "Re: Konqueror"
    date: 2002-01-27
    body: "No there isn't. As far as KIO is concerned, a link is a link, whether it's a link on a web page or a link to a file."
    author: "Carbon"
  - subject: "Re: Konqueror"
    date: 2002-01-27
    body: "There can be link to a file on the web page and it should be opened by a single click, of course. The same link on a list view should be opened by a double click I think.  Difference is in the presentation layer and not in whether this is a www URL or a local file."
    author: "Rumcajs"
  - subject: "Re: Konqueror"
    date: 2002-01-28
    body: "\"There can be link to a file on the web page and it should be opened by a single click, of course. The same link on a list view should be opened by a double click I think. Difference is in the presentation layer and not in whether this is a www URL or a local file.\"\n\na) Why be inconsistent just because the web way didn't exist when GUIs became popular?\n\nb) You *can* select a file. Just ctrl-click on him, like you do to select the second, third and later files.\n\nHowever, what is the purpose of selecting a single file?\n\nThe only half-useful purpose I can think of is \"to presss delete and delete it\",\nand that is not really a big deal, IMHO.\n"
    author: "Roberto Alsina"
  - subject: "Re: Konqueror"
    date: 2002-02-04
    body: "a) Why be inconsistent just because the web way didn't exist when GUIs became popular?\n\nahhh, but ctrl+click on a web URL doesn't select it. So there is a differentiation."
    author: "HRJ"
  - subject: "Re: Konqueror"
    date: 2002-02-05
    body: "Well, let's fix that instead ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Konqueror"
    date: 2002-04-09
    body: "Selecting one file is also nice when you plan to copy or cut it with ctrl-c or ctrl-x.\n\nThe best thing would be if this was configurable separately for different presentations: when managing files single click would select, when browsing web opening the link."
    author: "janza"
  - subject: "Re: Konqueror"
    date: 2002-08-09
    body: "I, too, am annoyed by the single click opening files.\n\nSo I changed it in Control Panel -> peripherals -> Mouse\n\nNow the tree view is broken. Clicking on a folder in the tree view does not open the folder in the list view. Doube click also does not work\n\nBut double click does work in control panel. If i double click a tree view item it appears in the main window.\n\nThis is inconsistent.\n\n"
    author: "mike"
  - subject: "Konqueror double-click"
    date: 2004-03-08
    body: "If the double-click to open option is enabled in control Center - peripherals - mouse and konqueror still opens files on a single-click, what do I need to change?"
    author: "richard"
  - subject: "Re: Konqueror double-click"
    date: 2004-12-15
    body: "I can't find any information pertaining to this problem. Konqueror never retains the double-click preference from mouse configuration on my laptop. Anyone else?"
    author: "Josh Schneider"
  - subject: "Creating PDFs?"
    date: 2002-01-27
    body: "Maybe I missed this in the article, but...what's the preferred\nway to create a pdf these days, assuming I have KDE/KOffice\nand Star Office installed? Thanks..."
    author: "steve"
  - subject: "Sorry, found it in the second part of the article"
    date: 2002-01-27
    body: "\" the absolutely excellent KDE printing architecture (see below) allows one to print to a PDF. So, we simply print to a Postscript (.ps) file, click on that to open it in KGhostView, and then click \"Print\" and choose \"Print to PDF.\" Voila, you have a gorgeous PDF.\""
    author: "steve"
  - subject: "Distributions that don't use KDE Control Center"
    date: 2002-01-27
    body: "The author notes:\n\n\"in my opinion, all distributions should use the same scheme as Caldera OpenLinux and Redmond Linux: integrate 100% of the system configuration tools into the KDE control center.\"\n\nI would like to point out that SuSE  also integrates\nwith the KDE Control Center.  I think Red Hat and\nMandrake stand out as the only ones *not* to do \nthis.  \n\nIMHO it makes for a very unprofessional UI, and it's\nthe major reason why I no longer use them or \nrecommend them to others."
    author: "steve"
  - subject: "Re: Distributions that don't use KDE Control Cente"
    date: 2002-01-27
    body: "\"I would like to point out that SuSE also integrates\nwith the KDE Control Center. I think Red Hat and\nMandrake stand out as the only ones *not* to do\nthis.\"\n\nThis is not that strange. Mandrake's control center \nis written in GTK+. I don't now if it's possible to \nintegrate it into KDE's control center without rewriting \nthe GUI in QT...\n\n"
    author: "J\u00f6rgen Lundberg"
  - subject: "Re: Distributions that don't use KDE Control Cente"
    date: 2002-01-27
    body: "Yes, I know the technical reasons behind it.\nBut the bottom line to users is it makes for an\ninconsistant user interface.  In 2002 there's really\nnot an excuse for such amateur efforts, IMHO.\n\nI'm glad to see SuSE and Caldera getting it right\nat least."
    author: "steve"
  - subject: "Re: Distributions that don't use KDE Control Cente"
    date: 2002-01-27
    body: "Isn't it YAST, which stores all its configuration in a private file and flushing it to the \"normal\" files overwriting all previous contents, so if you use it you can't touch config files ?"
    author: "Rumcajs"
  - subject: "Re: Distributions that don't use KDE Control Cente"
    date: 2002-01-30
    body: "an MD5 checksum is added to each configfile when it is regenerated. If you modify the configfile, the MD5 checksum will be incorrect and that file will not be processed by Yast (so it leaves your modifications untouched).If you want Yast to process the configfiles again, just remove the checksum completely from the file."
    author: "Jeroen Jacobs"
---
<A href="http://www.trustcommerce.com/">TrustCommerce</a> has spent the the last 12 months or so transitioning to KDE/Linux-based desktops for general staff, with <i>"resounding success"</i>.  As Chief Software Architect, <a href="mailto:adam@trustcommerce.com">Adam Wiggins</a> has written <a href="http://people.trustcommerce.com/~adam/office.html">an excellent essay</a>  detailing the situation, requirements, issues, and the process of switchover. Apps such as <a href="http://kmail.kde.org/">KMail</a>, <a href="http://www.konqueror.org/">Konqueror</a>, and <a href="http://korganizer.kde.org/">KOrganizer</a> have been successfully deployed, along with the main KDE desktop.  Check <a href="http://people.trustcommerce.com/~adam/office2.html">Part II</a> of the essay for those details
<!--break-->
