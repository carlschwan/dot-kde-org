---
title: "Qt For The Console"
date:    2002-04-01
authors:
  - "numanee"
slug:    qt-console
comments:
  - subject: "It's a pity you didn't post this before /."
    date: 2002-04-01
    body: "Otherwise, more people might have been fooled.\n\nBTW, has anyone else heard the rumor that (and this comes from _very_ reliable sources) that the respective leaders of GNOME and KDE are planning on  a comprehensive merge and joint release of GNOME 3.0 and KDE 4.0? What they will call it, however, has not been announced.\n\n:Peter"
    author: "Peter Clark"
  - subject: "Re: It's a pity you didn't post this before /."
    date: 2002-04-01
    body: "April 1 :-)"
    author: "dr88dr88"
  - subject: "Re: It's a pity you didn't post this before /."
    date: 2002-04-02
    body: "read this....\n<p>\nKDE 4 has been announced...\n<p>\n<a href=\"http://clee.kdedevelopers.net/announce-4.0.html\">http://clee.kdedevelopers.net/announce-4.0.html</a>\n\n"
    author: "somekool"
  - subject: "Re: It's a pity you didn't post this before /."
    date: 2002-04-02
    body: "It's another april FOOLZ    :^)"
    author: "Tom"
  - subject: "Re: It's a pity you didn't post this before /."
    date: 2002-04-02
    body: "Very Funny :))"
    author: "John Sheridan"
  - subject: "COOL!!!"
    date: 2002-04-02
    body: "I love its performance!!!!! :-))))"
    author: "attila"
  - subject: "Re: It's a pity you didn't post this before /."
    date: 2002-04-02
    body: "I believe they are going to call it GNacK'D"
    author: "simon"
  - subject: "And in related news..."
    date: 2002-04-01
    body: "This morning I woke up to find that my City's most bloved symbol, the reknown 'Golden Boy' which had been moved to be recovered in gold, had been neutered.\n\nThis was a direct result of the Feminist wing of the New Democratic Party in my province - a highly socialist and wholely leftist organisation.\n\nThey were originally going to change the statue to the 'Golden Girl, but the gay rights groups cried out.\n\nSo now, in my city (Winnipeg), I now find out that our new beloved symbol is to be the 'Golden Person'.\n\nNow this is all fine and well, except that one other organisation got involved. The Cathlic Church.  They were quite upset, since they thought that this was an obvious reference to their past practice of castrating school-boys so that they wouldn't lose their nice choir voices when pubert hit.\n\nJust when I though it could get no worse, I looked at my watch to find that it was April 1st, and I was just another victim of the day of the 'poisson' (fish).\n\nHappy April Fools, everyone....\n\nTroy\ntroy(at)kde(dot)org"
    author: "Troy Unrau"
  - subject: "Accessibility win"
    date: 2002-04-01
    body: "This step will make people look deeper into what is needed to make KDE applications fully keyboard navigable.  If one good thing comes out of this, I want my KDE to be more accessible.\n"
    author: "Anonymous"
  - subject: "ttyquake"
    date: 2002-04-01
    body: "Dont be fooled, ttyquake is for real.\n"
    author: "KDE User"
  - subject: ">((((\u00b0> .oO blup !! blup !!"
    date: 2002-04-01
    body: "\\_O< coin ! coin !"
    author: "Shift"
  - subject: "/\\/\\/\u00b0~  ssssssss"
    date: 2003-06-30
    body: "     _____\n    ( \u00f4 \u00f4 )\n     |_^_|\n    8=====8   ouaf!\n     |^^^|\n     TTTTT\n\n"
    author: "JAY"
  - subject: "Dammit"
    date: 2002-04-01
    body: "Dammit I fell for it.\nI'm such a fool :)\nThe screenshots convinced me.\nBut wouldn't it be great? \nKDE for the console?\nhehe"
    author: "Fool"
  - subject: "Re: Dammit"
    date: 2002-04-01
    body: "The first KDE app to be ported should definitely be Konsole.  \nI'm badly missing its functionality on the console."
    author: "cm"
  - subject: "Re: Dammit"
    date: 2002-04-02
    body: "ever took a look at 'screen'? i think it offers exactly what you want..."
    author: "Jochem"
  - subject: "Re: Dammit"
    date: 2002-04-02
    body: "I was just kidding. I \"forgot\" the :-)\nSorry.\n\nAnd yes, I know about \"screen\". \nThanks for trying to help.:-)\n\n"
    author: "cm"
  - subject: "Berlin and GGI doe this for real"
    date: 2002-04-01
    body: " ... GGI can do ASCII Art version of X .... Berlin will rock our worlds in 10 years (I hate to think I'll still be using computers in 10 years) ..."
    author: "NameSuggesterEngine"
  - subject: "Some more hot news"
    date: 2002-04-01
    body: "http://linuxtoday.com/news_story.php3?ltsn=2002-04-01-009-26-PS"
    author: "reihal"
  - subject: "UPDATE!"
    date: 2002-04-02
    body: "Good news, folks! I got the thing to compile on win32 using cygwin ;-)"
    author: "jjan"
  - subject: "Re: UPDATE!"
    date: 2002-04-02
    body: "is it simple to compil ? \n\ni would like to have some kapps under win32\n\nplease give us more info please.\n\n"
    author: "somekool"
  - subject: "Re: UPDATE!"
    date: 2002-04-02
    body: "It's very easy to compile ...\n\nJust extract the tarball, then follow the usual procedure:\n\n./configure\nmake install"
    author: "jjan"
  - subject: "Another joke?"
    date: 2002-04-02
    body: "Is no site mature enough to post real stories today?  \"April fools\" maybe be funny to 13 year olds but not to most everyone else who is past puberty."
    author: "jmalory"
  - subject: "Re: Another joke?"
    date: 2002-04-02
    body: "not really.. it's an ageless tradition <!--, fucktard. -->"
    author: "dc"
  - subject: "Re: Another joke?"
    date: 2002-04-02
    body: "I agree with jmalory. <!-- I think this happens because at least half of KDE Team is still masturbating in the shower. I also think this stupidity of April 1st is yet another North-Americanism stupidity which we all should leave it alone. Grow up people. -->"
    author: "Reality Check"
  - subject: "Re: Another joke?"
    date: 2002-04-02
    body: "<rant-mode>\n\n>I think this happens because at least half of KDE Team is still masturbating in the shower.\n\nI'm ever so sorry that April Fools Day jokes don't fit in with your personal and very specific ideas of how we all ought to act. But, seriously, get real! April Fools Day jokes are harmless and maybe even mildly amusing, not a threat to civilization as we know it. Immature insults like the one above followed by advice to 'grow up' show nothing but a lack of debating skills on your part.\n\n>I also think this stupidity of April 1st is yet another North-Americanism stupidity which we all should leave it alone.\n\nAnd please, at least learn how to form a sentence in a language before using that language to act indignant and pompous.\n\n</rant-mode>"
    author: "Carbon"
  - subject: "Re: Another joke?"
    date: 2002-04-02
    body: "Since the really annoying part of the parent comment has been removed, Navindra, go ahead and remove my response too, if you want. It doesn't make much sense out of ranting context :-)"
    author: "Carbon"
  - subject: "Re: Another joke?"
    date: 2002-04-03
    body: "Reality Check is a notorious troll who should have been banned a long time ago."
    author: "Anonymous"
  - subject: "Re: Another joke?"
    date: 2002-04-03
    body: "Anonymous is a notorious troll who should have been banned a long time ago."
    author: "dc"
  - subject: "Re: Another joke?"
    date: 2002-04-05
    body: "masturbating in the shower is also an ageless tradition:)"
    author: "ME"
  - subject: "wow wow wow !!!"
    date: 2002-04-02
    body: "i cant wait until i can use kmail via SSH\n\nanyone have a recent success story with KDE 3 ? \n\ndoes QT-Console and KDE3 and co-habit easily ? \n\nwow !!!\n\n"
    author: "somekool"
  - subject: "Re: wow wow wow !!!"
    date: 2002-04-02
    body: "it is possible to use kmail over ssh using X"
    author: "AC"
  - subject: "TO BAD this was a joke!!"
    date: 2002-04-02
    body: "I would really like that this was true!"
    author: "Andreas"
  - subject: "Not Joke: Java AWT/Swing for console"
    date: 2002-04-02
    body: "Although this news was just a joke, there is a something truly in development to let Java AWT/Swing render well in console, CHARVA Toolkit:\n\n    http://www.pitman.co.za/projects/charva/\n\nLet's see its introduction:\n\nThe CHARVA Toolkit is a Java package for presenting a \"graphical\" user interface, composed of elements such as windows, dialogs, textfields and buttons, on a traditional character-mode ASCII terminal. It has an API based on that of the Abstract Window Toolkit (AWT) and the \"Swing\" classes (collectively known as the Java Foundation Classes). Programmers familiar with the AWT and Swing will find programming CHARVA trivial; AWT/ Swing programs can be ported easily to CHARVA, provided that they do not use GUI-specific features such as images, \"mouse events\" and \"drag and drop\".\n\nVery cool!"
    author: "jserv"
---
The toolkit we know and love has finally been ported to the text console. The <a href="http://qtconsole.nl.linux.org/">Qt-Console</a> project reports having reached the point of successfully running ports of <A href="http://qtconsole.nl.linux.org/konsole.png">Konsole</a>, <a href="http://qtconsole.nl.linux.org/kmail.png">KMail</a> and even <a href="http://qtconsole.nl.linux.org/konq.png">Konqueror</a> on the console.  The project takes KDE portability to a whole new level -- think running KDE apps on your cellphone -- and ranks right up there with the <a href="http://webpages.mr.net/bobz/ttyquake/">Textmode Quake</a> hack in terms of coolness. Just don't expect <a href="http://www.mosfet.org/liquid.html">High-Performance Liquid</a> to work.
<!--break-->
