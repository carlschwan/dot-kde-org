---
title: "Application Of The Month: KTouch"
date:    2002-04-29
authors:
  - "numanee"
slug:    application-month-ktouch
comments:
  - subject: "Formatting suggestion"
    date: 2002-04-29
    body: "This interview would be a lot more readable if the questions were bolded or indented or somehow differentiated from the answer."
    author: "Carbon"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-29
    body: "Also, it would be nice if I could access the KTouch homepage :)\n(ktouch.sourceforget.net)\n\n(Konqueror gives a sigfault when clicking on the link)\n"
    author: "rve"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-29
    body: "It works perfectly fine here...  It's a quite simple page.\n"
    author: "Navindra Umanee"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-29
    body: "AAHHH, here it crashes too:("
    author: "gogo"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-29
    body: "I also get a sigfault when clicking on the same link ;-("
    author: "Andreas Pietzowski"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-29
    body: "Have you compiled konqueror with obj-prelinking? I have the same issue here. :("
    author: "altrent"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-29
    body: "hmm, I'm having the exact same problem\nWhat version of konqueror are you using?\nMine is 3.0 from the Gentoo distribution (I was heavy on the optimisations tough).\nbtw the same thing happens to me on wilwheaton.net,\nbut those are the only sites I've ever had problems with\n"
    author: "cxvx"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-29
    body: "I'm using the TexStar rpms (KDE 3.0 stable with optimizations, without obj-prelink) for Mandrake 8.2 ( see http://www.pclinuxonline.com )\n\nThere are a few other sites that crash on me, but not many..\nKonqueror is more stable for me than Internet Exploder under Windows :) (which I'm not using anymore)\n"
    author: "rve"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-29
    body: "Huh?  What?  Aren't the questions already bolded?  I didn't do the markup, I just used what was provided, but it seems fine to me.\n\nWhat browser are you using?\n"
    author: "Navindra Umanee"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-29
    body: "The questions are bolded, as I provided them. Must be a problem with your browser. I tested it with Konqui 3.0, Netscape 4.78, Netscape 6, Opera 5.0 and Mozilla 0.94 - works fine in all these browsers."
    author: "Klaus Staerk"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-29
    body: "Doesn't look bold in my Konqueror 3.0 either..\n\nMaybe there's a small bug in Konqueror which prevents bold tags to be used within paragraph tags?\n"
    author: "rve"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-29
    body: "Huh.  Can someone suggest a fix?  The HTML file (w/out header/footer) for the interview is attached.\n"
    author: "Navindra Umanee"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-29
    body: "It's definitly a bug in Konqueror..\n\nI just browsed the KDE bug list and there have been more problems with the bold and other tags, 1 bug is still open (dated last year)\n\nI'll take a look at the KHTML source myself in a few days.\n(in a few days because I'm too busy right now to create some GIMP tutorials (I'm an artistic programmer ;))\n\nAs for a solution: maybe it can be solved using inline style tags? (CSS)\n"
    author: "rve"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-30
    body: "The bold works okay in Konqueror 3.0.1 (KDE 3.0.1) from CVS as of last week."
    author: "George"
  - subject: "Re: Formatting suggestion"
    date: 2002-04-30
    body: "That's great news!\n"
    author: "rve"
  - subject: "Wow"
    date: 2002-04-29
    body: "Wow, hey, this is a great app!\nI never checked out the kde-edutainment progs.\nI just finished trying Ktouch, and suddenly my typing classes flashed back into my memory. I used to hate those classes :(.\nOfcourse it was with a goofy old typewriter.\nBut learning how to type with Ktouch is much more fun :)\nI typed for five minutes, I couldn't stop, because it was so addictive :)\nI'll remember that when my little sister needs to learn how to type.\nIndeed the kde-edu section is underrated.\nWell it's good that there is some *fuzz* (fuzz is not the word, but I can't find the correct word, well you know what I mean) around kde-edu.\nI may not need it so very much, but if someone needs to be educated, I will tell them: \"Hey dude, get the kde-edu package.\" :)\n"
    author: "blashyrkh"
  - subject: "Re: Wow"
    date: 2002-04-29
    body: "I agree! KTouch is ruly.\n\nCongratulations on having the first on-topic post.\n"
    author: "ace"
  - subject: "Great app ..."
    date: 2002-04-29
    body: "... so simple and elegant, and so addictive!\n\nI couldn't stop typing either, such a lot of fun! And of course I had to\ngo on f f a a s s t t e e r r  a a n n d d  fa fa s s fa fa st st st er fa st er an d and fas ter fas ter and faster and faster ....\n\nAfter having tried KTouch for 10 minutes I really felt the progress.\n\nIt's a great app, thank you!"
    author: "AC"
  - subject: "Re: Great app ..."
    date: 2002-05-01
    body: "I absolutely can *not* recommend KTouch.\nHaving used /real/ typing-tutors this utility is plain silly.\n\nWhat does a /real/ typing tutor do ?\n\nFirst:  It teaches you the ten-finger system\nSecond: It contains lessons to make you type faster and faster.\n\nKTouch does not do any of these. I installed two revisions of KTouch\nand in none it would tell me *where* to put my fingers and *which* finger to use for *what* character. This is the very first thing it should do however.\n\nMuch better is GNUTypist. It is a console based application (yeah learn typing fast over an internetted ssh connection ;-D), that can read the datafiles of many different familiar applications. Alos the ones of KTouch, btw, but even here you will not find any instructions on where to put your fingers. KTouch simply does not have them. The GNUTypist lessons, however, do show this and are overall more professional."
    author: "jon"
  - subject: "Re: Great app ..."
    date: 2002-05-01
    body: "Considering that the QWERTY layout keyboard is *designed* to slow down your typing (since mechanical typewriters couldn't keep up), who says the \"10 finger\" location is best anyway?\n\nI type with 6 fingers.  A, S, D, and L, :/;, \"/' on my keyboard.  Spacebars is thumb, and Enter key is right pinky or right ring-finger depending.  Left pinky for shift, control, etc.\n\nI also type 120 wpm with 85% accuracy, and 80 wpm with 95% accuracy.\n\nI failed typing class in high school.\n\nWhy?  Because I had already been typing for 8+ years before I took the class and they wanted me to \"unlearn\" what was working well for me.\n\nNow, take the Dvorak keyboard for instance.  A much more intuitive keyboard layout, but if you try to put your 10 fingers where the \"old school\" wants you to have them and you'll end up breaking a knuckle.\n\nI say pooh!\n\nAs long as you know where the keys are, who gives a sh*t how you type, just as long as you can."
    author: "anon"
  - subject: "Re: Great app ..."
    date: 2002-05-01
    body: "The whole QWERTY thing is a myth.  There's a few pages out there that I'm too lazy to find right now that explain it all in excrutiating detail.\n\nI think there was _some_ merit to the idea that the design was meant to keep a typewriter's machinery from jamming up, but in many tests proved no slower than any other setup.  It ended up succeeding because of company/marketing reasons.\n\nhttp://www.google.com/search?hl=en&q=qwerty+myth\n\n"
    author: "Anonymous Typist"
  - subject: "Re: Great app ..."
    date: 2002-05-02
    body: "My grandfather worked for Underwood typewriters.\n\nI can assure you, the QWERTY layout is *not* a myth, and was specifically engineered that way to slow down typists and prevent the arms and hammers from jamming up.\n\nI have two Underwood typewriters in my attic.  Both can only handle typing around 50-60wpm before they jam up.\n"
    author: "anon"
  - subject: "Re: Great app ..."
    date: 2002-05-02
    body: "well, most ppl hate the 10 finger system."
    author: "10 finger"
  - subject: "Re: Wow"
    date: 2004-12-21
    body: "This app slowed me down on my typing, I can already type at 150 wpm, the only thing that I need to work on is probably, um....10 key?  I hate numbers, but that's never been a problem for me, because...well...I just don't care for numbers and I already know where all the numbers are on the key board, it comes with years of typing experience, that you learn where all the keys are on the keyboard, as a matter of a fact it only took me a minute to type all of this\n\nwhich would be at about 111 wpm, pretty fast eh? that's with out mistakes."
    author: "Dennis Blackburn"
  - subject: "Re: Wow"
    date: 2004-12-21
    body: "Plenty of mistakes:\n\n\"um...10 key?\"  What is that supposed to mean?  Maybe you mean \"10 *keys*\", with an 's'?\n\n\"key board\" You have an extra space there.\n\n\"with out\" Another extra space.\n\nOther mistakes include missing punctuation and/or inappropriate punctuation.\n\nMaybe the app is slowing you down for a reason?\n\n(Note: I'm not claiming my own post is error-free.)"
    author: "ac"
  - subject: "edu vs. science"
    date: 2002-05-02
    body: "I'm just wondering if there is a similar kde-science initiative? I still think that linux/unix system have a great market share among scientists. I'm not talking educational programs that teach you calculus or the periodic table, but rather tools that are geared especially towards a scientific audience. KLyx was a truly great program in the right spirit, but it hasn't been around since KDE 1 :( \n\nWhat I would want is: \n* a great scientific \"word processor\". A well integrated equation editor that generates nice (LaTeX quality) equations is very important as well as footnotes and multiple-file reference handling. Revision control like in KLyx would also be very nice. Candidate: Lyx GUI independent\n\n* A decent illustration app. Hopefully Kontour will do.\n\n* A professional quality plot component. This is a real problem today - when saving plots from for instance Matlab the font sizes etc are all fixed. If the image is resized in the word processor the font sizes are as well resulting in a document/book with slightly varying plot appearance. It would be awesome to have a plot component where you can change the label sizes, line widths, oline styles etc globally using normal formats/styles/whatever! (killer warning) \n\n* A numerical maths program like Matlab or Octave. Just put Octave in a KDE-window and add support for the above mentioned plot objects and ... wow! \n\n* A flow chart program (Kivio)\n\n* Perfect PDF generation from all of the above.\n\n* Anything else you can think of (but the above mentioned programs would be a more than decent start!)\n\nAm I just dreaming?"
    author: "Johan"
  - subject: "Re: edu vs. science"
    date: 2002-05-03
    body: "> KLyx was a truly great program in the right spirit, but it hasn't\n> been around since KDE 1 :(\n> [...]\n> Am I just dreaming?\n\nYes, you are. Must be some sort of nightmare. There was a KDE2 port of\nKlyx (albeit badly maintained) and there is a working verison for KDE3 in\nthe CVS. I have it running here under KDE3, yet I prefer vim+(la)tex ...  :-)\n\n"
    author: "Melchior FRANZ"
  - subject: "Re: edu vs. science"
    date: 2002-05-23
    body: "KLyX was a port of LyX to KDE.  Development always lagged behind LyX, though, and since LyX will soon be GUI-independent (i.e. available in Qt and Gtk flavours as well as the old ugly xform widgets) it should provide what you're looking for.  See http://www.lyx.org"
    author: "Robin"
  - subject: "i've enjoyed this "
    date: 2002-05-02
    body: "I Like this program! I've played all the way to level 26. Edutainment is a good name; the program is fun and I believe gives good practice. To better teach typing to beginners, rather than just provide practrice, perhaps some incorporation of proper hand position training would be nice.\n \n"
    author: "Juln"
  - subject: "KDE3 version?"
    date: 2002-05-02
    body: "Hey, I'm looking to move my Dad to Linux/KDE. All he uses Windows for is web browsing, email, and to practice typing! I don't want to give him KDE2, tho. Is there a KDE3 version anywhere, (I didn't see one)? \n\nThose keyboard buttons can use some themeing, too ;-) If there's a KDE3 version mind if I add it? Want to impress the old man with KDE :)"
    author: "Mosfet"
  - subject: "Re: KDE3 version?"
    date: 2002-05-03
    body: "You need to look for a kdeedu package for your distro (or compile it :) http://download.kde.org/stable/3.0/"
    author: "ac"
  - subject: "Re: KDE3 version?"
    date: 2002-05-03
    body: "I see. The webpage should be updated, too, tho ;-) I just looked there and didn't see it."
    author: "Mosfet"
  - subject: "Not enough layouts"
    date: 2002-05-03
    body: "There doesn't seem to be support for Dvorak. \"??????\" would be nice too, but I don't really need support for Russian. Not yet at least."
    author: "Vadim"
  - subject: "Great name..."
    date: 2002-05-08
    body: "...so what are you going to call the KDE app which changes the access time of a file? :-)"
    author: "Jon Evans"
---
As part of the May 2002 issue of "Application of the month" series on <a href="http://www.kde.de/">KDE.de</a>, <a href="mailto:staerk@kde.org">Klaus St&auml;rk</a> has interviewed <a href="mailto:havard.froiland@chello.no">H&aring;vard Fr&oslash;iland</a>, author of <a href="http://edu.kde.org/ktouch/">KTouch</a>. KTouch is part of the <a href="http://edu.kde.org/">KDE Edutainment Project</a> and provides a quick and fun way to learn the useful and impressive skill of touch typing.   See the <a href="http://ktouch.sourceforge.net/">main homepage</a> for further details and screenshots -- the interview follows.
<!--break-->
<hr><br>
<p>
<b>First of all: Congratulations for publishing the first official release of the KDE Edutainment family.
What's the current state of this KDE project concerning the initiation of new education applications ?</b>
</p>

<p>
First of all I would like to say thank you on the behalf of the hard working KDE Edutainment team. I have not
been able to follow the progress of the kdeedu package lately, and therefore I have had to get some help from
the mailing list, concerning the current status. Thanks to Anne-Marie.
</p>

<p>
We are currently preparing new programs for the KDE3.1 release: a maths plotter program, a Japanese language
learning tool, a tool to learn Spanish verbs, a Norwegian verbs learning tool, a flash card tool, and a
hangman game. We are also preparing for the KDE 3.2 release, which will have two mathematics tools, one for
primary school children, and one for young adults.
</p>

<p>
<b>How did you start the KTouch project and who else is involved in it?</b>
</p>

<p>
KTouch was started as a way of learning C++ and QT. I had previously started a couple of other projects based
on Java, which were also educational applications. These applications never made their way to the web though.
Also the spirit of open-source is great, and I wanted to contribute to it. In my opinion, to make free
software is one step in helping us to level out the differences out there.
</p>

<p>
I have had small contributions to KTouch from various people with translations, training files, keyboard
layouts, graphics and different sounds. Christian Spener was working with me for a while, helping out with
the web page and stuff. He also tried to make a qt-only application that would run on Windows. I haven't had
any major contributions to the code itself though.
</p>

<p>
<b>Do you know of any plans for CD-ROM-based CBT-programs (Computer Based Training) for the KDE Edutainment
family, e.g. some kind of multimedia application to learn a foreign language?</b>
</p>

<p>
Not yet I think. At least I have not heard of anything like that. The KDE-Edutainment project started just
8 months ago so it is still very young. The fact that KTouch and KStars existed before, allowed a strong
start for the project and a release of the kdeedu package so soon. We are not in the position yet of filling
a CD with educational KDE software! Thanks to Anne-Marie for answering this question.
</p>

<p>
<b>Do you have any more features planned for KTouch?</b>
</p>

<p>
Yes, I want to finish the statistics page. As you can see there are a couple of missing pages in it. I have
also started work on adding support for editing the training files, and have the possibility to run tests.
The test will be a page with text that you have to type as fast as possible with the least amount of errors.
Then after you are finished, it will tell you your words per minute (wpm) and how accurate your typing was.
</p>

<p>
I also want to remove the splash screen and write a touch typing tutorial to go in the help page. And while I
am at it, I also want to put all the configuration stuff in a proper KDE configuration dialog.
</p>

<p>
<b>What do you think about Open Source and what is the reason for you personally to publish KTouch under the
GPL ?</b>
</p>

<p>
I am mainly doing this for fun, and I am glad that so many people can use this application since it is GPL.
I am also using many GPL applications myself, and would like to give something back to the Open Source
community.
</p>

<p>
I like the idea that this application is meant for educational purposes, as I feel this group often has
difficulty when it comes to purchasing software, and some of the programs are certainly not cheap.
</p>

<p>
<b>Do you know other KDE developers? Do you know them just virtually or in person as well?</b>
</p>

<p>
I only know a few people very briefly, just some of the girls and boys in the kde edu project. And of course
Christian Spener who worked with me for some time.
</p>

<p>
I don't know any of these people in person though, only through e-mails or IRC.
</p>

<p>
<b>How much time do you usually spend per week working on the KDE project?</b>
</p>

<p>
It really depends, it goes up and down. In the past I have got most of it done when I have had heaps to do at
UNI. Now that I am starting work, I don't know how much time I will spend on it. But in Norway we have some
dark autumn nights with rain and wind, when some coding could be done. ;-)
</p>

<p>
<b>What's the most common Desktop Environment used on your workplace? (If not KDE, why not? :-)</b>
</p>

<p>
Well, I haven't started there yet. But I am afraid that most of the stuff is running on Windows at the moment.
There could be some changes to that though, if the word of KDE get out.
</p>

<p>
<b>Here in Germany, we had an initiative called &quot;<a href="http://www.bundestux.de">BundesTux</a>&quot;
that aims at bringing Linux to the servers and desktops of the German parliament (with quite a good success:
all Bundestag servers will run under Linux in future!). What's the situation with using free software in the
Norwegian parliament?</b>
</p>

<p>
I am not sure about this, but I know that we have a big project called &quot;Skolelinux&quot;, and
information about this can be found at <a href="http://www.skolelinux.no">www.skolelinux.no"</a>. This is a
project that aims towards making a specialized distribution for schools, with lots of different educational
applications.
</p>

<p>
<b>What's your opinion about the future of &quot;Linux on the desktop&quot;?</b>
</p>

<p>
I can't see how we can avoid it. A Linux solution running KDE is in my opinion much better than a Windows
solution. The biggest problem is Microsoft Words dominating position in the word processing market. But we
now have several fast evolving office solutions coming up, and I guess that we will see more and more
migration to free software solutions.
</p>

<p>
I also think it would have helped if we had a distribution that only focused on a graphical environment for
office workers. Making it extremely easy, and remove all the servers, developing tools, and other
applications that the average person doesn't use. Instead focus on a set of office tools, like word
processing, spread sheets, e-mail and so forth. What about a KDE distro. ;-)
</p>

<p>
<b>What's the configuration of your computer, and which Linux distribution do you use for your daily work ?</b>
</p>

<p>
At the moment I am running Mandrake 8.2 and KDE 3.0 on a 600 Athlon processor, with 128 mb memory. It runs
well, but I really need some more memory. I really like KDE 3.0, there are many great improvements and
everything looks really good. KOffice is also getting along very well.
</p>

<p>
<b>What do you do in your spare time when you're not coding for the KDE-project?</b>
</p>

<p>
My biggest interest is climbing and spending time with friends. I am also keen on bush walking and like
spending time in the mountains. Scuba diving and parachuting is also something I do from time to time. And
then of course I play around with bits of code from time to time.
</p>

<p>
<b>Thanks a lot for joining the "<a href="http://www.kde.de/appmonth">app-of-month</a>" :-)
</p>

<p>
Thanks for choosing KTouch, this was fun. ;-)
</p>

<p />
<hr />

<p />
H&aring;vard Fr&oslash;iland: <a href="mailto:havard.froiland@chello.no">havard.froiland@chello.no</a>
<br />
KTouch: <a href="http://edu.kde.org/ktouch">http://edu.kde.org/ktouch</a>
<br />
Klaus St&auml;rk: <a href="mailto:staerk@kde.org">staerk@kde.org</a>
<br />
Application of the month: <a href="http://www.kde.de/appmonth">http://www.kde.de/appmonth</a>