---
title: "KOrganizer Workshop: Outlook2Vcal"
date:    2002-10-28
authors:
  - "numanee"
slug:    korganizer-workshop-outlook2vcal
comments:
  - subject: "Good!"
    date: 2002-10-28
    body: "Well, everything that makes it easy to move from M$ to real software is good news to everyone (and especially to KDE users). Now, it would be useful if kmail could handle hotmail http mail but still KDE can replace windows' most common functions that are necessary for basic (80%) users.\n\n\nPS: is this the 1st message?"
    author: "Alx5000"
  - subject: "Re: Good!"
    date: 2002-10-28
    body: "Yes.  It already works.  Konqueror can do hotmail already and so KMail can easily do this too by embedding KHTML."
    author: "ac"
  - subject: "Re: Good!"
    date: 2002-10-29
    body: "KMail can already handle hotmail http mail via gotmail  (http://freshmeat.net/projects/gotmail/).\nSimply fetch your mail from your hotmail account with gotmail, save it in a local mbox-style mailbox (Warning: Don't save it in ~/Mail !) and then fetch the messages from this mailbox with KMail. You can even configure gotmail as precommand for a local account in KMail so that in order to fetch new mail from your hotmail account all you have to do is press the Check Mail button in KMail. Of course you can also enable interval checking for your hotmail account.\n\nThat's the beauty of Unix. Don't reinvent the wheel a thousand times but use the tools that already exist.\n\nRegard,\nIngo\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Good!"
    date: 2002-10-29
    body: "cooooool! :D"
    author: "Alx5000"
  - subject: "Re: Good!"
    date: 2003-06-24
    body: "I agree, that is one of the best updates in a long time."
    author: "JMtaner"
  - subject: "Re: Good!"
    date: 2002-11-04
    body: "Now, if only there was a way to make this whole process simple and easy for newbies...\n\nOther than, fantastic work folks!! KDE gets better every day :-)"
    author: "bagpuss_thecat"
  - subject: "sidenote"
    date: 2002-10-29
    body: "as a sidenote, the .ics files korganizer generates work really well with http://phpicalendar.sourceforge.net/"
    author: "pyre"
  - subject: "Shared calenders with outlook users..."
    date: 2002-10-29
    body: "what is more interesting is the ability for shared calenders from other Outlook XP clients can be accessed in this method with webdav and/or samba and this converter.\n\ni have played with this some when i had access to a clients system who was running this, but the more i think about it, the more it makes sense to add this native into korganiser...\n\nman i wish i had more time and a winders box to play with...\n\n-ian reinhart geiser"
    author: "ian reinhart geiser"
---
Have you ever wondered how to export your appointments from
Microsoft Outlook and re-import them in KDE's <a href="http://korganizer.kde.org/">KOrganizer</a>? The <a href="http://korganizer.kde.org/workshops/Outlook2VCal/en/html/index.html">second KOrganizer workshop</a> (<a href="http://korganizer.kde.org/workshops/Outlook2VCal/de/html/index.html">de</a>) covers  <a href="mailto:blueboy@bamafolks.com">Randy Pearson</a>'s <a href="http://korganizer.kde.org/importdata.html">Outlook2VCal</a> tool in fully illustrated detail.   For more information on the KOrganizer workshop series <a href="http://korganizer.kde.org/workshops.html">click here</a>.  Good work, Klaus!

<!--break-->
