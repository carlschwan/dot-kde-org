---
title: "KDE Icon Chatter"
date:    2002-01-27
authors:
  - "numanee"
slug:    kde-icon-chatter
comments:
  - subject: "Icons"
    date: 2002-01-26
    body: "I agree i think the current icon set needs modernized without becomeing a total mac or windows rip off. iKons are not pretty enough for me though. They to greyish and don't have enough color.\n\nCraig"
    author: "Craig"
  - subject: "I like KDE's icons"
    date: 2002-01-26
    body: "I like that the current KDE icons are not photo-realistic. They are what they are supposed to be: icons, not photos.\nThat said, there are some very nice ideas in iKons. It would be worthwhile having this as a second standard KDE icon theme.\n<p>\nIf you want to see what the 'enemy' :) is doing, here's a link to what will become the Gnome 2 default icons. They, too, have moved away from photorealism:\n <a href=\"http://primates.ximian.com/~tigert/new_stock_project/\">http://primates.ximian.com/~tigert/new_stock_project/</a><br>\nNote the slight influence KDE's icons have had on them :)"
    author: "Jon"
  - subject: "Icons, life and stuff :-)"
    date: 2002-02-01
    body: "Howdy from the \"other camp\" :)\n\nI pretty much agree that having photorealistic icons is bad for usability - unless they are very big. They work pretty okay on MacOSX because the icons are just huge. It works well for the desktop/panel/application icons on GNOME too, though I wouldnt call the style \"photorealistic\". I think it is more like \"cartoony\" or \"airbrushed\" (I use the Bezier tool in Gimp much like airbrush artists do with real world tools). But whatever people want to call it..\n\nAnyway, the \"big\" icons on Gnome have a 3D perspective look. I decided to not go with \"isometric\" perspective (45 degree angles everywhere, a good example is BeOS) because I wanted to make them more natural looking, but Tackat has been doing a very nice job using isometric angles for KDE icons. I guess it is a matter of personal preference and style. Isometric definitely gives you less jaggy edges if you do 1bit transparency versions of icons.\n\nNow, to the \"KDE influence\", noooooooo! :o) Seriously though, the current GNOME toolbar icons have a 3D perspective, but I have realized the same usability issues with them, the 24x24 pixel grid is just not enough for perspective tricks, the resolution is way too low. Even when we now have full transparency support for toolbars thanks to Gtk2. It would just look too blurry. Thus we made the decision to go with flat \"face-on\" perspective (which MacOSX also uses for toolbar icons by the way) - it makes it a lot easier to do clean, understandable icons.\n\nFor the style, I bet every designer takes influence from others. I like Tackat's work, although I personally prefer a bit less bright color palette. But I guess the biggest influence for the lighter colors has been from using Macos, mostly MacOS 9.x (for printwork, I need Illustrator to do that, and Mac is the way to go with those apps) - Apple uses a very light and \"happy\" color scheme which I like. And it looks like WinXP is also going to the same direction. Beos was like that too. I was never too fond of making my desktop look like Quake anyway .. :-)\n\nIn any case, judging from the feedback I and Jimmac got from the icons, looks like we are going to the right direction.\n\nBy the way, I found a nice link to WinXP icon style guide, they suggest people do icons first with a vector drawing app and then convert to bitmaps from that. The page also gives the winxp color palette, which is a lot like the one I did for GNOME, though it has more blue colors.\n\nhref=\"http://msdn.microsoft.com/library/?url=/library/en-us/dnwxp/html/winxpicons.asp?frame=true\n\nSometimes I wonder how much influence Microsoft takes from us.. I think I see some Gnome and KDE influence on WinXP ;-)\n\nBest wishes,\n\nTuomas\n\n"
    author: "tigert"
  - subject: "SVG versus bitmap"
    date: 2002-01-26
    body: "To me, the technical underpinnings of the icon system need more attention.  Bitmaps only make sense when you have limited range of display resolutions, but this is no longer true.  A handheld might have resolution of 50dpi while a nice display can have up to 140dpi.  The range is too large for bitmaps to work.\n<p>\nI'd prefer to see icons implemented as SVG.  This is how MacOS X works.  There has been some work to add more SVG icons to GNOME.  An initial screenshot from many months ago is here: <a href=\"http://jimmac.musichall.cz/screenshots/gorilla4.png\">http://jimmac.musichall.cz/screenshots/gorilla4.png</a>\n<p>\nWould anyone else like to see scalable icons in KDE?"
    author: "Jeffrey Baker"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-26
    body: "I agree. Gnome icons are indeed beautiful."
    author: "JK"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-26
    body: "The problem with using vector graphics, is that you have no control over how things look when you scale down.\n\nNotice that the screenshot you posted had rather large icons.  How good do you think they would look, if scaled down to 32x32, or 16x16?\n\nThe advantage of having multiple bitmaps, is that the artist can hand correct the smaller icons to still look good and be recognisable"
    author: "Stuart Herring"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-27
    body: "This is not true.  An SVG can be created to add detail as it is scaled up, and remove detail as it is scaled down.  It is very powerful, and can even be used to do things such as zoom into a fractal."
    author: "Jeffrey Baker"
  - subject: "Re: SVG versus bitmap"
    date: 2002-04-29
    body: "But does any image editor support those features? -_-"
    author: "Stof"
  - subject: "Re: SVG versus bitmap"
    date: 2002-11-25
    body: "No, but things like Adobe Illustrator manipulate SVG."
    author: "GE"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-27
    body: "I would like to!\nAnd about little icons in svg looking bad, well, I already saw some screenshots of gnome with svg icons and ultil 22*22 they look great, don't know about 16*16, but this size of icons is getting in desuse in KDE already."
    author: "Iuri Fiedoruk"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-27
    body: "> 16*16, ...icons is getting in desuse in KDE already.\n\n16x16's still see frequent use on 800x600 remote X terminals. This type setup is more common than an owner of a single-user install might think--offices, schools and even homes of resourceful users where one newer box runs KDE and old hardware provides seats.  Think LTSP: http://www.ltsp.org or City of Largo."
    author: "Paul Ahlquist"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-27
    body: "So.. in this case, why not simply using a option to user choose between bitmaps and svg?\nI want svg, some want bitmap, let's make both of them happy :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-27
    body: "Sure, just make SVGs for all the KDE icons, and code in that option, and that would be great! :-)"
    author: "Carbon"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-28
    body: "If SVG icons are used (let's hope they finally are) the method should be similar to the one used with some scalable fonts, that is: include bitmap \"snapshots\" for some concrete resolutions, for example for 16x16 and maybe for 32x32, when the SVG icon might look bad. This does NOT mean including snapshots for all icons at a particular resolution, it just means that if the author thinks the icon will be used at 16x16 and will look bad, he should include a bitmap version for that resolution.\n\nIf not, anyway, we are now using 16x16 and 32x32 icons. What's the deal with leaving the 16x16 icons and replacing the rest with SVG?"
    author: "Salva"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-27
    body: "I always create two panels, each at size TINY.\n\nThis causes me to use 16x16 icons, and that's the way I always do it on my 1024x768, because this way I still have plenty room left to place my apps."
    author: "Richard Stellingwerff"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-28
    body: "Most icons you see in KDE are displayed by the application menus and especially by the Kicker menu. These icons are displayed at 16x16.\nMost other icons are displayed in the toolbars (22x22). \nThen there are the icons on the desktop: Still most people are using 32x32 there (Of course I like 48x48 much better).\n\nSo how do you get your idea of 16x16 getting \"in desuse in KDE already\"?\n\nBTW: I haven't seen too many screenshots showing svg-icons at 22x22 - perhaps you could post a URL :-) ?\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-29
    body: "Good question, I tryied finding it again but couldn't (maybe I dreamned?) :)\nBut you can simply test it. There is a plugin for netscape on adobe.com, and you can try getting gnome .svg icons and resizing it, I'll try it for myself to see the results, but judjing by flash examples I saw already, probally it gets good.\nBut anyway, svg should be used for 32*32 or bigger icons IMHO."
    author: "Iuri Fiedoruk"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-27
    body: "How fast processor is needed to render such icons ? Some apps need to display a few dozen icons at start, so they must be rendered and stored as pixmaps somewhere."
    author: "Rumcajs"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-27
    body: "i fear it will be slooow..\nsvg is xml-based afaic, and xml parsing is slow, and offcourse\nyou need to do extra math for rendering..\nmaybe we could get around that by reviving the icon server or icon cache idea,\n\ni don't know a lot about the topic so excuse me if i'm talking nonsense."
    author: "ik"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-27
    body: "The question isn't how fast does your processor need to be, but rather how good is your programmer?  MacOS X uses SVG icons and it runs interactively on really stupid and slow CPUs like the 300MHz PowerPC 750.  You don't need bug iron, you need smart programming."
    author: "Jeffrey Baker"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-27
    body: "Are you good enough, then, I take it?\nIf so, I am sure ksvg developers would appreciate your help.\n\nMore signficantly, though, IMHO, SVG is a ridiculously complicated standard, which is an overkill \nfor icons. And in general, how often do you want to see an icon larger then 48x48? \n\n"
    author: "Sad Eagle"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-27
    body: ":: how often do you want to see an icon larger then 48x48?\n\n   When displays start leaping ahead to resolutions expressed in hundreds of DPI.  Memory is now cheap, even video memory, and the CRT is being discarded.  I have no doubt that a major factor that will hold back exponential leaps in resolution is pixel based systems.  Right now, write a new widget set and window decoration, and most things in KDE will scale - except icons.\n\n   Having said that, I'll kick in my 2 Eurocents to the thread.  Bitmaps display quickly.  They should be the default.  Extending the icon system to *support* SVG is a good thing for those who want to install icon packs that use vector icons (read: people with multi-gigahertz machines).  That also moves a final step towards being able to discard pixel units as resolution (note that it is not the final step in actually doing it, just the last system that comes to mind that needs to be fundimentally reworked).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-29
    body: "But why not use the .svg icons to -batch generate- bitmap icons\nof the required size in advance (even during installation)? (Or certainly a 'commonly used subset' of them.)  Thus one has a speed/space trade-off that can be set as required. That said, this is part of the job of an icon cache,\nso I guess that's where any effort on this cort of thing should be directed (if necessary).  Besides that, if you recall how fast early versions of Xara studio\nwere at anti-aliased transparent rendering (on Win95 that is), if the rendering\narchitecture is fast enough, this shouldn't be a problem anyhow (if it is, \nspeeding up rendering of the .svg's should become the priority).\n\nPersonally, I think .svg is the way to go with icons.  I know little about\n.svg but, is it possible for aspects of an .svg image to be parametrised (e.g.\nfor colour schemes, etc.?).\n"
    author: "John Allsup"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-28
    body: "On a 400dpi display, a 48x48 icon would be practically invisible.  You'll have a 400dpi display in fewer than 5 years.  All modern computer display systems have OVERWHELMINGLY rejected bitmapped fonts in favor of scalable vector fonts.  TrueType fonts, the popular flavor, are vector glyph descriptions each of which comes with a little program that runs inside of a virtual machine.  Why not do the same for icons?\n\nThink SVG icons would be slow?  You already draw tens of thousands of scalable vectors all over your screen all the time."
    author: "Jeffrey Baker"
  - subject: "Re: SVG versus bitmap"
    date: 2002-01-28
    body: "> You'll have a 400dpi display in fewer than 5 years.\n\nI already thought this 5 years ago. While working on KDE 2 I did some research concerning this topic on expos like CeBit. Judging from that I'm quite sure that you'll be wrong for the next 5 years, too.\n\nI don't whink that we will have resolutions like 3200x2400 in common use any time soon: think of all those myriads of webpages out there: each one would have to be redesigned (or displayed with magnified bitmaps).\n\nAnd yes: drawing svg-icons would be quite slow as you need lots of shades, alphablending, etc. Don't get me even started to talk about the limitations you run into once you try to paint some very close parallel lines!\n\nTackat"
    author: "Tackat"
  - subject: "MacOSX does not use vector icons!"
    date: 2002-01-27
    body: "MacOSX *does not* use vector icons! It *does not* use SVG icons. I don't know where this rumor started but it's absolutely not true. MacOSX icons contain normal bitmap graphics for the icons in 3 sizes, then uses scaling to make it the proper size (kindof like KDE, except all the images are in one file ;-). One of the sizes is 128x128, so it can scale down to whatever size it needs. I don't know if Gnome started the rumor but it's not true, because it's a dumb idea.\n\nVector graphics for icons is a horrible idea. You can't \"paint\" them, you have to draw it all as graphics primitives. Not only do you then have to parse a graphics control language for all the icons, but then you end up with icons that all look like Windows Meta Files. \"Painted\" icons are much higher quality, not to mention can be created in any drawing application."
    author: "Mosfet"
  - subject: "Re: MacOSX does not use vector icons!"
    date: 2002-01-27
    body: "I ment that the rasterized versions could be stored somewhere on the disk, so all frequently used icons would be always ready to display as pixmaps. If there would be no pixmap version or SVG file would have the later modification time, the pixmap should be (re)generated. However I think that it is a huge work and no advantages over the ordinary method. "
    author: "Rumcajs"
  - subject: "Re: MacOSX does not use vector icons!"
    date: 2002-01-28
    body: "What about tools like autodraw (iirc) that attempt to convert bitmaps to vectors? That way, the icon's don't look significantly different, but it's easier to do things like smooth scaling, isn't it?"
    author: "Carbon"
  - subject: "Re: MacOSX does not use vector icons!"
    date: 2002-01-28
    body: "It would be impossible to get the same quality as you can with a painted icon. How do you describe something like a photograph or photorealistic image as vectors? You can't, (well, actually you can but the result would be much worse than scaling because each pixel would be a vector point ;-) As you figured, it's not only far easier to do smoothscaling but also this provides much better results. Like I said, with vectors you end up with images like Windows Meta Files, not Photoshop ;-) Whoever spread the rumor that MacOSX uses vector icons was *wrong*. It's nonsense."
    author: "Mosfet"
  - subject: "Re: MacOSX does not use vector icons!"
    date: 2002-01-29
    body: "Hmm, isn't there any vector conversion tools that convert bitamps using higher levels of abstraction though? Things that attempt to discern high-level things like gradients and lines and curves within a bitmap, to produce a vector that was truer to the original bitmap? I'm not volunteering to do such a thing, nor attempting to say that such a thing would be a good idea, but merely asking if such a thing has been implemented or is being implemented, or is even plausible?\n\nAlso, I never said MacOS X uses vector icons. However, there are a lot of things that MacOS X doesn't do that KDE/UNIX does (such as Open Source the GUI :-)"
    author: "Carbon"
  - subject: "Re: MacOSX does not use vector icons!"
    date: 2002-02-01
    body: "You can make good looking vector icons, just look what Jakub did with the Scalable Gorilla - and he just started to do vector stuff not that long ago. \n\nOf course it is a lot more work, and the tools are different, but it is certainly possible. The style will naturally look a bit different, but if you end up with bad clipart then you have a bad artist :-)\n\nI agree with you though, that it makes no sense to render the stuff on screen as vectors, one really renders to a bitmap and caches that. It gets really slow otherwise too.\n\nTig"
    author: "tigert"
  - subject: "Re: MacOSX does not use vector icons!"
    date: 2002-02-04
    body: "I can't say about MacOSX because I'm clueless.\n\nAlas, there's something else: when you draw icons, or do any graphics,\nusing a painting program, you really do abstract, vector things, even if you\n*think* you're working pixel-oriented. Most painting program operations, such\nas fills, gradients, filtering effects are just atomic operations that you\ncould essentially do on an image scaled to any size. Imagine you draw a zig-zag\nwith a pen, and then add a few finishing touches to it, finish it wish a shadow.\nAll of your \"operations\" were abstract - they didn't necessarily need to work\non image of that particular size. Well, if you say that changing a single pixel\nis not too abstract think of this: it may be not too abstract if you look at it\nas changing (r,g,b) triad in the array of 32-bit pixels.\n\nThings change if you think of it as modifying an infinite-resolution image.\nInfinite-resolution images don't contain more information than finite-resolution\nones, it's just that the representation allows much nicer scaling. A line or\na bezier on such an image always retains its shape and never becomes jaggy\n(if you scaled a bitmap, it would), and single pixel alterations will be seen\nas gaussian peaks (for example) of given color. When you work on a 256x256 icon\nthat way, it will always render exactly same for 256x256 size, you can\neven hint it a little if you think that \"default\" rendering algorithms do\na bad job at say 128x128 size. That's what's done in font rendering. Yet, if you\nhave that crazy 210 dpi display device, and you want your icon to still be of\nthe same size on the screen, it should render just fine even it its 768x768 then.\nI'm not saying that bitmap scaling would do a bad job there, it's just that\nfor things like printing, or low-resolution displays, it's always a good thing\nto get a non-pixel-oriented representation. Bitmap programs reduce information\n- you draw a vector, you get changed pixel values. The fact that this was a\nnice vector, or a nice \"lighten\" pen sweep is lost forever in all but simple\nimages.\n\nI would say that the fact that SVG doesn't support most of this theoretical\nframework is irrelevant. It can be done, and it can be done nicely, and it's\nabout the only way to go. A decent renderer can take less than 100kb of\ncompiled code and execute perfectly fast on a 486/100 embedded system.\n\nSo, while most posters here are right that SVG is useless, it doesn't mean that\nnon-bitmap oriented icon handling is a wrong thing to do. Alas, I'm personally\nin disfavor of text-oriented formats (like SVG or XPM) for distributions, since\nthey are size-hogs - even if *after* they get loaded they consume little\nspace, they still need that much cache and buffer space in order to *get*\nloaded, and on small systems that's what does make your disk churn away\nneedlessly... All xml-like hierarchical markup documents can be represented\nvery effectively in binary form, with size benefits (without any further\ncompression, ie. w/o using bzip-lib nor zlib) for graphical (svg-like) markup\nwell in 1:10 area.\n\nCheers, Kuba"
    author: "Kuba Ober"
  - subject: "Re: MacOSX does not use vector icons!"
    date: 2003-01-22
    body: "1. Vector Icons are much easier to make. Anyone doing a lot of graphics knows that for drawing, you need a vector app. \n2. Scaling vector icons is NOT easier, it might even take more, because for every size the complete icon has to be redrawn on the screen. Imagine a osX dock, rasterizing 20 resizing vector icons all at the same time. Combining Pixels for scaling is much more efficient.  If you want to use vector icons, and scale them, then it'll probably be faster to cashe the rasterized icon (bitmapped) and then resize that one. \n3. Vector icons aren't useful. Icons are small, and then the vector icons will probably be even larger in size then bitmapped ones. \n\nSO\n\nmake them in vactor, raster them for usage. That's the way. \n\nVector Graphics are great, but unuseful for icons."
    author: "jack"
  - subject: "Re: MacOSX does not use vector icons!"
    date: 2004-03-18
    body: "Vector graphics for Icons can in fact be painted, just as well as a bitmap or raster images can. As long as the rendering engine supports gradients, all sorts of FX can be implemented. Vectors can be automatically traced out of Bitmaps which use patches of color(not necessarily on a per pixel basis) to render photographic images. The added advantage of vector? Independent resolution at any size. Make it big and it stays sharp, make it small and it's still clear.\n"
    author: "Calactyte"
  - subject: "Nonsense!!!"
    date: 2002-01-28
    body: "I don't know why you spread this nonsense here but just to keep things correct:\n<p>\nMac OS X does _NOT_ use SVG-icons.\n<p>\nThere are nice articles on arstechnica available like this one\n<p>\n<a href=\"http://arstechnica.com/reviews/1q00/macos-x-gui/macos-x-gui-6.html\">http://arstechnica.com/reviews/1q00/macos-x-gui/macos-x-gui-6.html</a>\n<p>\nwhich discuss how Mac OS X works in regard to icons. If you read throught them you might realize that the technique used in Mac OS is comparable to the one that KDE is using (in fact KDE even does it in a superior way).\nThey store each icon in several sizes (128x128, ... , 32x32, 16x16) and SCALE them down to the size needed.\nYou can even SEE this in Mac OS X if you look at the icons while dragging the scaling-slider: at some points (48x48, 32x32 e.g. ) the icons get a lot sharper!\n<p>\nConcerning Gnome: They don't use SVG's yet either. Unfortunately the SVG's done by Jimmac are not available for download yet - they exist on screenshots only.\nApart from the screenshots I haven't seen them anywhere so I don't know how slow it works. But I guess there's a good reason why the new icons they made for Gtk2/Gnome2 are still based on pixmaps ...\n<p>\nTackat"
    author: "Tackat"
  - subject: "Re: Nonsense!!!"
    date: 2002-02-01
    body: "The theme works (the screenshots are not mockups that is, but real ones) - but it is a bit slower. It is of course slower to render a vector illustration than it is to load a bitmap. It is not that bad in reality though since Nautilus is caching the stuff, so it is not getting rendered all the time.\n\nBut yea, at least I find it easier to draw bitmap icons since I havent done vectors much yet. The SVG is neat if you want to scale icons large though. I used to have this one huge trashcan on the desktop a while ago. An artist gets a lot of scratch paper you know :-)\n\nTig"
    author: "tigert"
  - subject: "Re: SVG versus bitmap"
    date: 2005-05-30
    body: "What about complete SVG background ??\nWithout real icons, but objects scaled directly in the background -the desktop-\nCoupled with javascript -like we do with xhtml... xml-\nIt could completely interactive\nFilling on the background, SVGed space to display any type of informations !\nLike RSS, xHTML... any xml compliant style !\n(using associated xmlns)\nI'm personnaly tired of the concept of icons ! :/"
    author: "Samdchire"
  - subject: "Talk about missing the point..."
    date: 2002-01-26
    body: "KDE's icons are ugly as shit. If people think this doesn't directly impact KDE's usability, well, they should just keep on using KDE. No need to drag OS X and GNOME into things."
    author: "Anonymous Gerbil"
  - subject: "GNOME vs KDE"
    date: 2002-01-27
    body: "That's true, I find GNOME ugly as hell and the Icons not just ugly but completely unusable.  I never know WHAT I am clicking on when I click on a GNOME icon.\n\nGNOME icons has zero customizatability like KDE icons.  Everybody misses this point.  KDE is so flexible, it doesn't matter what the icons look like, you can change anything.\n\nThe fact that KDE icons are so well designed and clean doesn't enter into it."
    author: "Anonymous Grebil"
  - subject: "Re: Talk about missing the point..."
    date: 2002-02-06
    body: "Gnome icons (indeed all GTK+ icons) will be customisable from 2.0+"
    author: "SteveHomer"
  - subject: "slick"
    date: 2002-01-27
    body: "<A href=\"http://www.kde-look.org/content/preview.php?file=732-1.jpg\">http://www.kde-look.org/content/preview.php?file=732-1.jpg</a>\n<p>\nWhat about these?"
    author: "JK"
  - subject: "Re: slick"
    date: 2002-01-27
    body: "Some are kind of cool but the home icon is to dark."
    author: "Craig"
  - subject: "Re: slick"
    date: 2002-01-27
    body: "instead of moaning, get off you lazy arse and create some damn icons. Add some colour to the 'home' icon, make other changes you see fit and release it to the world as a new icon set. KDE has too many moaners and not enough contributers."
    author: "Natra"
  - subject: "Re: slick"
    date: 2002-01-27
    body: "> instead of moaning, get off you lazy arse \n[...]\n> KDE has too many moaners and not enough contributers\n\nhave a look at control center -> font installer -> about\n\nBTW constructive criticism is good thing and this is what Craig did. IMHO you over-reacted a bit..."
    author: "Marco Krohn"
  - subject: "Re: slick"
    date: 2002-01-27
    body: "Umm.... I wrote the font installer, but it wasn't me who posted the previous comment. I have *no* complaints with the KDE icons..."
    author: "Craig (Drummond)"
  - subject: "Re: slick"
    date: 2002-01-28
    body: "They rock ! Good work."
    author: "Milifying"
  - subject: "did you notice .."
    date: 2002-01-27
    body: "everaldo ... Look in kde-artists mailing list ... Those icons look really cool imo \n\n-- Fab"
    author: "fab"
  - subject: "Re: did you notice .."
    date: 2002-01-27
    body: "Nice, I missed that.  Is there a web-page for the everaldo icons? Oh well, I just added a pointer to kde-artists."
    author: "Navindra Umanee"
  - subject: "Re: did you notice .."
    date: 2002-01-27
    body: "They are not everaldo's icons. Some of icons are MacOSX icons, some of them are KDE default icons, some of them are iKons folders.\nWhat everaldo did? He (just) applied one glassy effect on all of them, changed color on some of them and that's it. Yes they look nice with that effect, but I expect something more original from him, because he is talented :)"
    author: "kiki"
  - subject: "Re: did you notice .."
    date: 2002-01-27
    body: "Yes, but what he did is magic! :) Take the KMail icon for example. I've never really been very fond of the original KMail icon, but with some everaldo magic it suddenly looks very attractive. No need to create an entirely new KMail icon now. People will still recognize it (doesn't need to \"learn\" a new icon) but is still treated with a fresh new look. Lovely."
    author: "Matt"
  - subject: "Re: did you notice .."
    date: 2002-01-28
    body: "Same opinion here :-)"
    author: "Tackat"
  - subject: "Re: did you notice .."
    date: 2002-01-28
    body: "I couldn't agree more. KMail is one of my fav apps, and I like the idea of keeping the same icon but revamping it.\n\nMalc"
    author: "Malcolm"
  - subject: "Re: did you notice .."
    date: 2002-02-07
    body: "excuse me  \n  \nbut my icons are not icons of the macOs and they are not the icons default of the kde  \n  \nthey are new icons "
    author: "everaldo"
  - subject: "Re: did you notice .."
    date: 2002-01-27
    body: "Thanks for the pointer. Everaldo's icons are beautiful, me thinks."
    author: "Joe"
  - subject: "Re: did you notice .."
    date: 2002-01-27
    body: "just for everyone to see, i think what they're talking about can be seen here:\n<p>\n<a href=\"http://lists.kde.org/?l=kde-artists&m=101147705922068&q=p6\">http://lists.kde.org/?l=kde-artists&amp;m=101147705922068&amp;q=p6M</a>\n<p>\nvery impressive indeed! thanks!"
    author: "me"
  - subject: "Re: did you notice .."
    date: 2002-01-28
    body: "Cool, really cool...I like these."
    author: "hude"
  - subject: "I begin feeling like a minority"
    date: 2002-01-27
    body: "Am I the only one who likes the KDE default icons? Sure, there are some things that could be done better, but as a whole they do their job: No fancy photo-realism overloading the screen and distracting from the relevant stuff. Simple but nevertheless good-looking, functional icons. I don't know what everybody's complaining about. As it has been pointed out numerous times before, you can install your own theme, if you don'T like it.\n\nI always hear that Gnome's icons are much better than KDE's. Is it just me or why do I think it's the other way round? In fact one thing I immediately disliked about Gnome is the way it looks. KDE looks functional and tidied up. Please leave it taht way!"
    author: "Ralex"
  - subject: "Re: I begin feeling like a minority"
    date: 2002-01-27
    body: "All the complaints were about the toolbar icons, and indeed they could be  better."
    author: "Rumcajs"
  - subject: "Re: I begin feeling like a minority"
    date: 2002-01-28
    body: "your not the only one :) I personally think they are fine, but maybe i dont pay much attention to my icons. Of course that may mean that they ARE well designed since they are completely functional and the user doesnt even notice them."
    author: "870Fragmaster"
  - subject: "Re: I begin feeling like a minority"
    date: 2002-01-28
    body: "Yes, it sounds like you are !"
    author: "Rob"
  - subject: "Re: I begin feeling like a minority"
    date: 2002-01-28
    body: "You are not alone! :)) What I really like in KDE current icons, is that they are EASY TO READ... they require a minimal effort both in identifying the image, and in recognizing the associated function.\n\nMacOSX-like icons look to me like those appearently beautyful fonts with elaborated hand-written look... once you try to use them for reading a long text, you immediately look for the good, old helvetica."
    author: "Vajsravana"
  - subject: "Re: I begin feeling like a minority"
    date: 2002-01-28
    body: "I wonder, what fonts gotta do with icons? (I personally like Chicago though)"
    author: "Rajan Rishyakaran"
  - subject: "Re: I begin feeling like a minority"
    date: 2002-01-28
    body: "I like them too, though it's a psychological thing - new release - new look. I'm not sure about the complete redesigns that appear in some of the sets, but a fresh view would make the user get the feel that they have something new before they even begin to use it.\n\nSome people think eye-candy is unimportant, but Microsoft and HIFI manufacturers have become very successful at getting people to buy their products even if they are inferior to less attractive stuff.\nMalc"
    author: "Malcolm"
  - subject: "Icon Theme must be flexible !"
    date: 2002-01-27
    body: "Hi, this is the author of Klassic Icon Theme. Like towards every theme/thing, every user wants different toolbar/mime/filesystem icons. some like the standard and some want funny. The point is Even if you have 3 sets of Arrows or 3 sets of Stop/Reload button icons, you have to create 3 different icon themes with 3 index.desktop files.\n\nif KDE allows index1.desktop, index2.desktop index3.desktop and so on... then a Single theme can be much more configurable.\n\nThe iKons theme is a befitting icon theme which is much bothered by 'change arrows', and 'change stop' button and 'etc. etc.'. if KDE allows more than one 'Index.desktop' file then it would be easy for Kristof Borrey to implement both \"Professional\" and \"Home edition\" of iKons theme into a single ikons-0.6.tar.gz file.\n\nSame is with my 'klassic icon theme' where some users don't prefer 'yellow' toolbar arrows. though I have created red, green and blue arrows, I am forced to create 4 different 'klassic blue arrow', 'klassic red arrows', 'klassic green arrows' and 'klassic standard icon theme.'\n\nAny hope of having multiple index?.desktop files in a single icon folder?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Icon Theme must be flexible !"
    date: 2002-01-28
    body: "> would be easy for Kristof Borrey to implement both \"Professional\" \n> and \"Home edition\" of iKons theme into a single ikons-0.6.tar.gz file.\n\nWell if KDE-development would include such 'overconfigurable' stuff then there would be an option in KControl to change every single pixel on the screen (and millions of other options that are used by 5-6 users worldwide only).\nYou are proposing some kind of \"subthemes\" for a theme. You are proposing a solution which is not very flexible though compared to the solution which is implemented in KDE:\nIn KDE an icontheme can inherit from another icontheme. Your \"Klassic\" Icon Theme inherits from the \"Hicolor\" Icon Theme e.g.\n. This means that KDE is using icons from the \"Hicolor\" Theme if icons for certain items don't exist in \"Klassic\". As \"Klassic\" tries to look a little bit like the windows-95-icons some people might want to use some Windows-iconthemes in addition to the \"Klassic\"-theme. To accomplish this you can create an icontheme \"My Favourite Windows-icontheme\" which contains only a few icons from an MS Windows-icontheme and which inherits from \"Klassic\". \n\nIn Kristof's case I would just create an icontheme containing all the IKons for the \"Home edition\" and another icontheme which contains only those icons which are different in the \"Professional\" version. \"Professional\" would have to inherit from \"Home edition\" and \"Home edition\" would have to inherit from \"hicolor\". To accomplish the inheritance you simply edit the \"Inherits=hicolor\" line in the .desktop file of your icontheme.\n\nTackat\n\n\n"
    author: "Tackat"
  - subject: "Thanks for the Advice!"
    date: 2002-01-28
    body: "I tried to inherit klassic but i am not sure whether the inherit='klassic' means inherit contents of _folder_'klassic' or inherit content of _theme_named_klassic_. could you please advise me which one is correct 'the folder name' or 'the theme name', Thanks."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Thanks for the Advice!"
    date: 2002-01-28
    body: "It's the foldername.\n\nGreetings,\nTackat"
    author: "Anonymous"
  - subject: "GNOME icon themes"
    date: 2002-01-28
    body: "It seems to me that if all of these people like the GNOME icon themes, one of them should put together a GNOME icon theme for KDE."
    author: "csp"
  - subject: "Re: GNOME icon themes"
    date: 2002-01-28
    body: "\nI did... or at least made an attempt.  Things don't line up on a 1 to 1 relationship.  I have notes for a conversion utility, but never really came across a Gnome icon archive format (theme.org was down at the time), so that hampered my ability to progress on it.  Maybe I'll dust off the notes and take a look at Gnome 2.0 and see if things are more documented/available.\n\n    I do have a set of KDE formatted directories full of KDE named icons from Gnome CVS (which I assume to be the defaults at the time).  As I say, there's lots of missing file that just don't exist in Gnome to rename and use in a KDE icon theme.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: GNOME icon themes"
    date: 2002-01-28
    body: "I feel that you must put that gnome icon theme at kde-look.org for our friends (we the users of KDE and Gnome users) so that KDE could feel right at home for them (Gnome users). If you like I could render a helping hand."
    author: "Asif Ali Rizwaan"
  - subject: "Re: GNOME icon themes"
    date: 2002-04-29
    body: "That would be a problem. The KDE 2 panel doesn't support full alpha transparency (I don't know about KDE 3), and GNOME icons simply look ugly without transparency."
    author: "Stof"
  - subject: "iKons: Shell prompt or DOS Prompt?"
    date: 2002-01-28
    body: "Anybody know why, what I guess is the icon for konsole, has a representation of the default DOS prompt \"> _\" rather than a traditional UNIX shell prompt \"$ \"?\n\nYou only get to see \"> \" as a prompt in UNIX land in a non-shell \"command line\" application. For example a debugger or and interactive restore.\n\nIs this deliberate? Ignorance or just (bad) taste?\n"
    author: "Corba the Geek"
  - subject: "Re: iKons: Shell prompt or DOS Prompt?"
    date: 2002-01-28
    body: "Hi,\n\nof course I am aware of this problem :-) In fact one of the first versions of the konsole-icon _did_ have a \"$\" instead of the \">_\" painted on the screen.\n\n\nBut there were some problems in respect to this politically correct solution:\n\n- to paint a \"$\" you need at least 7 pixels in height to recognize it - 9 pixels would be better. On a 32x32 -icon this would mean that 25-30% of the icon would be covered by the \"\u00a7\" which would look strange. The \">_\" only requires 3 pixels in height :-)\n- In addition KDE is not targetted at the Unix geeks only: Most users probably have only used Windows before. These people don't know anything about \"$\"'s but the \">_\" looks quite similar to the DOS prompt.\n\nTackat\n "
    author: "Tackat"
  - subject: "Re: iKons: Shell prompt or DOS Prompt?"
    date: 2002-01-28
    body: "Thanks, an excellent explanation. I am happy now.\n"
    author: "Corba the Geek"
  - subject: "Re: iKons: Shell prompt or DOS Prompt?"
    date: 2002-01-28
    body: "My shell prompt currently reads \"darren@stargazer(darren)>\" so I don't think it's that unrealistic :)"
    author: "Darren Winsper"
  - subject: "Re: iKons: Shell prompt or DOS Prompt?"
    date: 2002-02-16
    body: "Because not everybody uses bash, stupid. Only Linux users do.\ncsh/tcsh standard prompt is \">\"."
    author: "KDE User"
  - subject: "Re: iKons: Shell prompt or DOS Prompt?"
    date: 2004-05-14
    body: "Please give me a way to learn me that at dos prmopt (before dos prompt)\n3 colors are showing)as (colors c:\\>) please \nthakful"
    author: "Ishfaque Khanzada"
  - subject: "Tackat is right"
    date: 2002-01-28
    body: "I couldn't agree more. It is very important for an icon to be simple clear, not aggressive (colors), without too much detail, easily distinguishable from other icons.\n\nThis doesn't mean they have to look cartoonish but it is a way to achieve this. Don't forget too that some people have difficulties to see all the colors, that some don't see icons without high contrasts, that some can't get the details because they are shortsighted. There are conflicting constraints here, but this highlights how the issue is tough. And what helps people with disabilities also helps \"normal\" people. If you are working very late on a very important thing, you will appreciate if you can grab an icon meaning in 1/10 s.\n\nBy the way, the KDE help icon is highly unusual. The other desktops I know use a question mark for the help, which looks to me like a common accepted practice. This makes it immedietaly recognisable. I fail to do that in KDE.\n"
    author: "Philippe Fremy"
  - subject: "Re: Tackat is right"
    date: 2002-01-28
    body: "> By the way, the KDE help icon is highly unusual. \n> The other desktops I know use a question mark for the help, \n\nHm, would you really expect the _answer_ of your problems behind a question mark? ;-)\n\nOf course I know that people are quite used to help-icons which show question marks from other OSes. That's why I already thought about adding a question mark layed over the life-saver.\n\nOn the other hand Gnome and Mac OS X are using this symbol now, too (and I guess the next version of Windows will do the same). so ...\n\nTackat\n\n"
    author: "Tackat"
  - subject: "The first impression"
    date: 2002-01-28
    body: "I don't think KDE icons are ugly, but they are not wonderful also.\n\nWhen people see KDE for the first time, what they will remember the most is its appearence. Usability and features will come after. So, in my opinion, it is very important that the default theme is as beatiful as possible.\n\nI've seen people that started using gnome instead of KDE just because it was more \"professional\", refering to its appearance.\n\n"
    author: "Roger"
  - subject: "Re: The first impression"
    date: 2002-01-28
    body: "> I've seen people that started using gnome instead of KDE just \n> because it was more \"professional\", refering to its appearance.\n\nI have seen people who use KDE instead of Gnome just because it\nlooks more professional.\n\nBeauty is always in the eye of the beholder. Something that looks beautiful to some people might piss off others. I know about people who switched back from Aqua/Luna (in Mac OS X/ Windows XP) because the artwork was too offensive in terms of beauty. \n\nI couldn't get my work done either if a gorgeous naked lady would sit in front of me while I try to do my job ...\n\n\n\n"
    author: "Anonymous"
  - subject: "Re: The first impression"
    date: 2002-01-28
    body: "I agree with you, but I'm talking about the default theme, or at least an included theme that is as impressive as OS X/ Windows XP. I can tell you that it is easier to make someone try something new when the appearence is nice and for the masses OS X/ Windows XP is nicer than KDE's.\n\nOf course, option is always good and that is what KDE is all about, you can always change your theme. But it lacks an appealing theme.\n"
    author: "Roger"
  - subject: "Re: The first impression"
    date: 2002-01-31
    body: "> I've seen people that started using gnome instead of KDE just because it was more \"professional\", refering to its appearance.\n\nWhat a joke... To be professional, it has to be functionnal, easy to read, without useless flashing appearance...\n\nAs many (a majority, I think), I like the KDE default icons. It is the good way, it gives a good first impression, but it is interesting to easily change all the icons, because flashing fun is also a good thing (for non professional...)\n"
    author: "Alain"
  - subject: "good and bad icons"
    date: 2002-01-28
    body: "For the most part, I think the KDE icons are functional, professional and excellent. I'm not interested in anti-aliased eye candy.\n\nHowever, I do think some of the icons look a little too \"cartoonish\". In particular, I would like the arrow icons to look smoother and cleaner. The same goes for the reload button.\n\njust my 2p."
    author: "david hj"
  - subject: "\"Yo tacket\" or \"KDE toolbar shortcomings\""
    date: 2002-01-28
    body: "If you want to prevent misinterpretation, you should LABEL THE TOOLBAR BUTTON, like GNOME does. Yes, I know KDE does have an option to display text under the icon, but this setting is not turned on by default. Keep in mind that majority of users are always going to go with the preinstalled default whatever it might be. Netscape vs. IE, need I say more? Go with the most usability-friendly default, and if some geek doesn't like it, he can turn it off. Another reason to label the toolbar button: It makes the button bigger. Why make it bigger: Fitts' Law. Fitts' Law is a principle of cognitive psychology stating that the time to access a physical target is the function of the distance to the target and its size. In the case of a toolbar button, the toolbar button with the label is bigger than one without, therefore it has a faster access time with a mouse. A really tiny toolbar button, such as the microsoft ones that a lot of KDE apps seem to imitate, have very slow access times with a mouse. \n\nTo learn more about how you can use Fitts' Law to design better user interfaces (or if you think I'm BS-ing or am arguing a matter of preference), check out this article at the site of UI guru Bruce Tognazzini\n\nhttp://www.asktog.com/columns/022DesignedToGiveFitts.html  \n\n\n"
    author: "Ilan Volow"
  - subject: "Re: \"Yo tacket\" or \"KDE toolbar shortcomings\""
    date: 2002-01-28
    body: "The problem is that for most apps, if you stick the label underneath the button, some buttons will disappear off the edge of the window. To access them you have to click the (narrow) continuation button, and then select them. from a dropdown. At this point Fitts Law goes out the window, the amount of time needed to access some buttons (or even discover their functionality) increases enormously.\n\nIncidentally, Netscape was the first to add labels under buttons (NS 3.x), and IE followed it, and then discarded it to a certain extent with IE5.56/IE6.0, where half the buttons have no labels, and the rest have labels beside them.\n\nWeird and all as it seems, we owe most of the UI enhancements of the late nineties to the rivalry between the Netscape and IE browsers."
    author: "Bryan Feeney"
  - subject: "Re: \"Yo tacket\" or \"KDE toolbar shortcomings\""
    date: 2002-01-29
    body: "Both of you are right, sort of. The excellent example is Konqueror. If you enable text under icons, some buttons will dissapear (if you only have default set of buttons on the toolbar), and that's only because viewmag+ and viewmag- icons have for long descriptive text ('Increase font size' & 'Decrease font size'). That can be easily solved by replecing text under these icons with 'Zoom (font)+' & 'Zoom- (font)'.\nThe best thing would be some kind of compromize, and that would be text beside or under the most used toolbar icons (back, forward, home, reload) or even that can be maybe customized (I don't know, maybe someone uses more cut'n'paste than back & forward :)\nOne can create toolbar icons for, for example, back and forward buttons (as Asif Ali Rizwaan created recently - thanks Asif :), so you can have 'longer' back and forward icons (22x53 pixels) while other icons are still in default 22x22 size. I use such toolbar icons and it improved browsing here.\nThese kind of icons can be easily created for all KDE applications, and that's the power of KDE."
    author: "antialias"
  - subject: "Black & White"
    date: 2002-01-29
    body: "I uploaded a black and white theme to kde-look.org. The icons can be used by\nLCD/Monochrome-displays and by visually impaired people.\n\nhttp://www.kde-look.org/content/show.php?content=777\n"
    author: "Ante Wessels"
  - subject: "Icons are very nice !"
    date: 2002-05-01
    body: "I've installed crystal icons on kde 3 and it looks very nice I find with mosfet installed too my linux is very nice, it were good I find to deliver with kde these options.\n"
    author: "Dominique Schaefer"
---
As MacOS X, and arguably, GNOME, have shown, many people love photorealistic icons despite the theoretical trade-offs with usability.  So if you've ever wondered about the philosophy behind KDE's default icons, you might want to read Torsten Rahn's <a href="http://lists.kde.org/?l=kde-artists&m=101206009910644&w=2">recent article</a> on the matter.  Tackat explains how KDE's default icons are a compromise between good usability and beauty,  using a <a href="http://lists.kde.org/?l=kde-artists&m=101206009910644&q=p3">mock up</a> of <a href="http://www.landkreis-goslar.de/SERVICE/verkehrsangelegenheiten/verkehrslenkung/verkehrszeichen/seite8.htm">traffic signs</a> to illustrate his points.  To conclude, Tackat proposes a slightly different approach for KDE 3.1: Add more photorealistic design to the application icons, while sticking to 2D for toolbars and mimetypes.  But wait, there's more.  In related news,  <A href="http://users.skynet.be/bk369046/icon.htm">Kristof Borrey</a> has <a href="http://lists.kde.org/?l=kde-artists&m=101207015827852&w=2">announced</a> the release of <a href="http://www.kde-look.org/content/show.php?content=602">iKons 0.5.5</a>.  More importantly, Kristof is now working on getting iKons 0.6 in shape for inclusion in KDE3.  For other icon activity, check out everaldo et al on the <a href="http://lists.kde.org/?l=kde-artists&r=1&b=200201&w=2">kde-artists list</a>. I should also point you to the growing number of <a href="http://www.kde-look.org/index.php?xcontentmode=iconsall">KDE icon themes</a> on <a href="http://www.kde-look.org/">KDE-Look.org</a>.
<!--break-->
