---
title: "People of KDE: Cornelius Schumacher"
date:    2002-02-04
authors:
  - "Inorog"
slug:    people-kde-cornelius-schumacher
comments:
  - subject: "ZX 81"
    date: 2002-02-04
    body: "The ZX 81 was his first computer. Mine as well. That was a box (well, rather a cigarette box). 1 KB of ram, including video ram.\n\nUwe\n"
    author: "Uwe Thiem"
  - subject: "image?"
    date: 2002-02-04
    body: "Where is the typical \"People of KDE\" image that is always used for Tink's reports?"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: image?"
    date: 2002-02-04
    body: "Yes, this article has the \"Community & Events\" icon, whereas most of the interviews have the \"Interviews\" icon. Btw, why does the interviews icon have a lion on it, anyways?"
    author: "Carbon"
  - subject: "Re: image?"
    date: 2002-02-04
    body: "Is it a lion? Hard to tell... it wears glasses and it looks more like a monkey :-)\n\nKDE people = a bunch of primates? :-)"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: image?"
    date: 2002-02-04
    body: "It sorta looks like it means something (is it the Interviewer?  is it the Interviewee?) in the interview context, but on closer look who the heck knows what it really is?  That just makes it laugh-out-loud funny to me.  :)\n\nIn case you haven't noticed, all the dot topic icons are lifted directly out of an old KDE cvs.  Original art wasn't something I particularly put much thought into for this site.  But I've always been fond of the topic icons... most especially when compared to the Gnotices ones. :-P\n\nFor that matter, our original grey logo was thrown together by Kurt in GIMP based on my own previous pathetic attempt to make one in Killu.  We stayed with that black and white thing for a long time!  Then q cured us of that, by designing the popular dot logo you see today.  :)\n"
    author: "Navindra Umanee"
  - subject: "Original Logo"
    date: 2002-02-05
    body: "\n    Is the original logo still available, preferably in a higher res format?  I kinda liked it, and seem to recall that it would have gone into wallpapers, etc. nicely.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Original Logo"
    date: 2002-02-05
    body: "<a href=\"http://www.kde.org/dot/Images/kdedotnews.old.gif\">kdedotnews.old.gif</a> is all that I have.  Maybe see if Kurt wants to whip up something high-resolution for you.  Note that the wheel is just another icon pulled out of an old KDE cvs.  Everything else is font work with the Gimp."
    author: "Navindra Umanee"
  - subject: "Re: Original Logo"
    date: 2002-02-05
    body: "Oh, don't get me wrong.  I quite liked it too.\n"
    author: "Navindra Umanee"
  - subject: "Re: image?"
    date: 2002-02-05
    body: "Well if you rip an icon out of context then that's your own problem :-)\n\nThe lion is being used for dvi-files. People who are used to TeX-files and stuff associated with it can't really avoid to meet the tex-lion or the metafont-cat somewhere in the (La)TeX-Documentation.\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: image?"
    date: 2002-02-05
    body: "Hehe, I see. Mea culpa, most certainly and intentionally! :) \n\nI'm one of the fangled TeX users who's never touched a real live TeX book, and so haven't met the mascot. Thank you info and Google!\n\nCheers,\n-N.\n"
    author: "Navindra Umanee"
  - subject: "Re: image?"
    date: 2002-02-09
    body: "Maybe I can submit my own icon? \nA little Tinkerbell fairy who sprinkles magic dust to get kde hackers to talk?\n\nJust a thought ;-)\n\n--\nTink"
    author: "Tink"
  - subject: "Re: image?"
    date: 2002-02-05
    body: "> Is it a lion? Hard to tell... it wears glasses and it looks more like a monkey :-)\n\nDamn, a monkey? Ximian bought the Dot! ;-)"
    author: "Rob Kaper"
  - subject: "Re: image?"
    date: 2002-02-04
    body: "Fixed, seems the article got filled under \"Community and Events\" rather than \"Interviews\" when I wasn't looking.  It's debatable which category it fits best under, but all the other interviews are at least now filled in the same category..."
    author: "Navindra Umanee"
  - subject: "Re: date?"
    date: 2002-02-04
    body: "Ohh, maybe its a goot idea to put a release date one every interview page, quite nice if yo read the old once a second time.\n\nBye\n\n  Thorsten\n "
    author: "Thorsten Schnebeck"
  - subject: "NEdit Plugin for KDevelop?"
    date: 2002-02-05
    body: "Hi!\n\nThe article said something about an NEdit plugin for KDevelop. Where can I find it? For I love this piece of software (if only there was a KDE port of this concept... *sigh* :))\n\n-- \nDario \"Eisfuchs\" Abatianni"
    author: "Dario Abatianni"
  - subject: "Re: NEdit Plugin for KDevelop?"
    date: 2002-02-05
    body: "It's in CVS (HEAD branch of kdevelop)."
    author: "Cornelius Schumacher"
  - subject: "Re: NEdit Plugin for KDevelop?"
    date: 2002-02-05
    body: "Thanks!\n"
    author: "Dario Abatianni"
  - subject: "Re: NEdit Plugin for KDevelop?"
    date: 2002-02-06
    body: "Sounds to good to be true. Must try it..."
    author: "Johnny Andersson"
  - subject: "Cool, but..."
    date: 2002-02-05
    body: "But when is the beta2 coming? Isn't it suposed to be here now?"
    author: "Syllten"
  - subject: "Re: Cool, but..."
    date: 2002-02-05
    body: "Mhm, maybe because mail.kde.org is down ?\nCan't ping and can't reach mailman interface.\n\nHope there will be some debian sid binarys.\nBut I don't think ther will ;)\n\nhave fun\nHAL "
    author: "hal"
  - subject: "Re: Cool, but..."
    date: 2002-02-05
    body: "actually CVS had been dead since monday along with ftp uploads and the mailing list.\nthey are down for maintance.  no, cvs is not magicly provided, VA dontes the bandwidth and server space.  \n\ni know most linux users are use to the free ride, but maby this could be a time to send cash / hardware or thanks to the people who maintain these systems.  Special thanks to those who allready contribute way more than their fair share.\n\njust my 2cents...\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Cool, but..."
    date: 2002-02-06
    body: "Ian, You spelling sucks :-) Anyway, it was nice talking to you the other day at LWCE."
    author: "Nadeem Hasan"
  - subject: "Re: Cool, but..."
    date: 2002-02-06
    body: "true.\nWasn't the KDE e.V. created to legally receive donations? What about creating\na kde e.V. website and register at paypal. Then everbody can just donate what\nthey wan't and the KDE e.V. manages this pool of money. \n\nJust my 0,02Euro"
    author: "Holger 'zecke' Freyther"
  - subject: "Re: Cool, but..."
    date: 2002-02-07
    body: "Well, it was created to enter into the Free Qt Foundation, but it expanded into accepting donations.\n\nAs for fundraising, I think it'd be best if KDE eV could raise the cash then pay for the cvs and master services directly, this way there would be real leverage for when the thing does down. The please will have to be done without snide comments about \"free rides,\" though."
    author: "Neil Stevens"
  - subject: "Re: Cool, but..."
    date: 2002-02-07
    body: "ugh, s/please/pleas/\n\nAnd about \"Free rides,\" who's writing the stupid code anyway?  I see no free ride here anyway."
    author: "Neil Stevens"
---
<a href="mailto:tink.kde.org">Tink</a>'s weekly report from the steady <a href="http://www.kde.org/people/people.html">journey through our fellowship</a> features today the author of <a href="http://devel-home.kde.org/~kandy/">Kandy</a> and the present maintainer of the highly-praised <a href="http://korganizer.kde.org/">KOrganizer</a>. We thus name <a href="http://www.kde.org/people/cornelius.html">Cornelius Schumacher</a>. He also co-authored a nice new developer tool dubbed <i>KBugBuster</i> and is a trusted member of the <a href="http://pim.kde.org">KDE-PIM</a> team. We now get the chance to learn more about him and his passions. Thank you, Cornelius. And thank you, Tink, for another interesting interview.

<!--break-->
