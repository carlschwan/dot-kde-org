---
title: "KDE-CVS-Digest for November 29, 2002"
date:    2002-11-29
authors:
  - "dkite"
slug:    kde-cvs-digest-november-29-2002
comments:
  - subject: "yay!"
    date: 2002-11-28
    body: "Thank you for your efforts!  An overview of the very heavy traffic kde-cvs list could be nothing but useful to the KDE community in general.  We will be glad to host you on the Cyberbrain.net server.  Then hopefully these 4 issues (and the last 3 KC KDEs) can be posted on LinuxToday.\n\nPS Woo, it looks like Richard Dale is back...\n"
    author: "Navindra Umanee"
  - subject: "Re: yay!"
    date: 2002-11-28
    body: "\"PS Woo, it looks like Richard Dale is back...\"\n\nThanks Navindra. Sorry I 'disappeared' - haven't had any time at all recently. I'm looking forward to doing kde stuff again..\n\nI sent an announcement to the kde-devel and kde-java lists about the java and c bindings regenerated for Qt 3.1/KDE 3.1.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: yay!"
    date: 2002-11-28
    body: "What about the Ruby bindings? Could you make them work, please?"
    author: "Me"
  - subject: "Re: yay!"
    date: 2002-11-29
    body: "Me too!\nRuby rocks.."
    author: "dah"
  - subject: "Re: yay!"
    date: 2004-01-15
    body: "say what?   why dont me and you,  heh heh  \"wink\""
    author: "derek"
  - subject: "Re: yay!"
    date: 2002-11-29
    body: "I added an -fruby option to the kalyptus bindings generator to generate the same output as SWIG would have, without needing to manually prepare loads of SWIG interfaces files. I didn't get round to finishing that though.\n\nSince then Ashley Winters has come up with the idea of smoke, where a single libsmoke.so lib can be used for any scripting language such as perl and ruby. You just need a language specific interface for ruby to that.\n\nAnd I think the Kompany might also be doing something about getting up to date ruby bindings too.\n\nSo something will probably happen with up to date ruby bindings sooner or later..\n\n-- Richard\n\n"
    author: "Richard Dale"
  - subject: "Re: yay!"
    date: 2002-11-29
    body: "I would also add that the Portable.NET project is contemplating a Ruby --> CIL compiler.  If this happens (they need some more volunteers who are interested in writing a Ruby compiler ;) you will also see Ruby bindings via the existing Qt# binding as these are really CIL bindings not just C# bindings.\n\nCheers,\n\nAdam"
    author: "Adam Treat"
  - subject: "and KC-KDE"
    date: 2002-11-28
    body: "\nWhy not do this as part of KDE Traffic ?\n\nThe thing I would like to see is cvs statistics per week:\n- 5 best comitters\n- nb of commits per projects\n\n"
    author: "Philippe Fremy"
  - subject: "Statistics"
    date: 2002-11-28
    body: "http://kde-stats.berlios.de has some old stats, not up to date anymore.\n\nIf someone could come up with an easy way to do this, please show me.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: and KC-KDE"
    date: 2002-11-28
    body: "For one thing, it's taken.\n\nSecond, the focus is different. The KC-KDE is by the developers, and seems to focus on what they are going to do. Which is important information. For example, I link to the kmail future developments in an older kc.\n\nWhat I am focussing on is the code that gets added to the repository. The code is done, and I think the strength of free software is the code and making it easy to review. Part of my purpose is to get to know the huge body of code that is kde.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: and KC-KDE"
    date: 2002-11-28
    body: "Hi Derek,\n\nYour work is very nice and useful. Could you suscribe to the kc-kde@mail.kde.org mailing list. \nThe people involved in KC KDE are discussing about which format we should give to KDE Cousin and I think we should have some coordination with you.\n\nI would like also to hear from Dot readers. What would you prefer ?\na) Pure summaries of discussion on mailing list with nothing added\nb) Information on development (from the mailing lists or outside the mailing lists) like the summary on translation in the last KC. \n\na) is exactly what Zack Brown is doing for the Kernel mailing list.\n\nb) would be more similar to the Gnome Development News.\n\na) is also a lot of work and if we don't have more contributors, we won't be able to do it on a weekly basis.\n\nCheers,\nCharles"
    author: "Charles de Miramon"
  - subject: "Re: and KC-KDE"
    date: 2002-11-28
    body: "Personnally I'd prefer a little of both:\n\nI think what Derek has done is great, but having the relevent parts of the emails quoted as in Kernel Traffic is much more convenient. I think recent kernel traffic issues have perhaps strayed a little too far to being merely direct quotes from the kernel ML, earlier issues tended to summarise the emails a little more so there was less direct quoting.\n\nHaving some general information on the development of KDE as well purely tracking the lists is also very useful. I think people are interested in the summaries, but that on their own they lack some of the authority given by quoting the relevent mails.\n\nCheers\n\nRich.\n\n"
    author: "Richard Moore"
  - subject: "Re: and KC-KDE"
    date: 2002-11-29
    body: "To make myself clearer. I would like to make for the next KC a summary on what is happening with the bindings for Perl and C#.\n\nThere are some interesting threads on the mailing lists but there are lot of points that I don't understand (I'm not a programmer). So, I thought sending private e-mails to the developpers to ask them questions and then round up their answers and the relevant e-mails on the mailing lists.\n\nThe pure KC solution, would be to use only the mailing lists and give more excerpts so people with more knowledge can figure it out even if I can't.\n\nEach solution has its pros and cons.\n\n\n"
    author: "Charles de Miramon"
  - subject: "Re: and KC-KDE"
    date: 2002-11-29
    body: "I'd be happy to answer any questions you have regarding those threads on qtcsharp and kde-perl :-)\n\nCheers,\n\nAdam"
    author: "Adam Treat"
  - subject: "Re: and KC-KDE"
    date: 2002-11-29
    body: "Yeah, and I'll answer any questions about the silly proposals I've been spouting in public where anyone can see them. Shame on me for not documenting!"
    author: "Ashley Winters"
  - subject: "Re: and KC-KDE"
    date: 2002-11-29
    body: "Yes contact the devs, the MLs are not always the pure result.\n\nAs for KOffice, some stuff is discussed on IRC and the final may not be completely clear on the ML (even if I try to post later on there as well).\n\nSo if something is unclear, just try to contact the devs. I'm happy to help here.\n\nI prefer the quotings withs some personal comments, even if some of the comments I don't like ;-) For me it's clear, this is the impression of the writer and everybody can have his own view, especially the ones who make the work for others and write articles.\n\nPhilipp"
    author: "Philipp"
  - subject: "Re: and KC-KDE"
    date: 2002-11-29
    body: "My mantra is: the one that does the work decides.\n\nDo what you can do and what you want to do. If you want to do an overview of what is going on on a list without citing the threads, then ok. Sometimes, it makes the whole thing easier than reading post after post to get the global idea.\n\nKC-KDE is low on resources. Whatever you do is good because the alternative is nothing. And do it the way you like it, Free Software is about fun.\n"
    author: "Philippe Fremy"
  - subject: "Re: and KC-KDE"
    date: 2002-11-28
    body: "GNOME Development News is pretty much empty content-wise.  Much back patting and applications but not much else."
    author: "ac"
  - subject: "Re: and KC-KDE"
    date: 2002-11-30
    body: "Sure, and you head also seems to be empty, brain-wise..\n\nCan you please stop the useless trolling on both the Gnome and KDE sites?\nThank you."
    author: "Rich"
  - subject: "Re: and KC-KDE"
    date: 2002-11-30
    body: "Do you really want to hurt me\nDo you really want to make me cry\nDo you really want to hurt me\nDo you really want to make me cry"
    author: "Christian F.K. Schaller"
  - subject: "Re: and KC-KDE"
    date: 2002-11-28
    body: "Ack! I never knew this discussion existed. KDE is HUGE. Does anybody know it all?\n\nI see that what I have tried to do came up in the discussion. Follow the cvs, comment on what is coming in. \n\nMy goals are quite simple. Glean as much useful and interesting information out of the cvs commits, comment on a few, in maybe 2 or 3 hours a week. A few threads on the devel lists are pertinent to the development process, comment on them.\n\nThe challenge is to keep it simple enough to be repeatable. There is so much stuff, and lengthy commentary could be made on literally hundreds of commits.\n\nOne thing I probably will change is instead of simply a bug reference number, use a bit of descriptive comment.\n\nRichard Moore's comment about quoting some of the list is useful. Maybe it's my preference. but I like reading threads. The most interesting part of software development is the wetware.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: and KC-KDE"
    date: 2002-11-29
    body: "\nHonestly, I like both a) and b). \n\nI am not on big KDE mailing lists with heavy traffic (kde-core, kde-devel, koffice, ...) but I like to know what is currentely being discussed and developed in KDE. What I do is that I wander through the mailing list archives.\n\nFor my, anything that help to know what is going on inside KDE is good.\n\nSo if you have time to make lengthy summary with lot of detailed references, then do it. But if you just have time for a short summary, this is still very interesting for people like me. Even a very brief monthly report noting the interesting threads on the mailing list is informative.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: and KC-KDE"
    date: 2002-11-29
    body: "I am sure you could share the infrastructure with KC-KDE. And your job can be seen as the KC-KDE of the list kde-cvs.\n\n> The KC-KDE is by the developers, and seems to focus on what they are going to do.\n\nYes, because this is usually what is discussed on a mailing-list. It doesn't mean you can't say more.\n\nKC-KDE needs more contributors. If the current way KC-KDE works doesn't reflect what you want for your summary, I am sure you can discuss with them to find a better arrangement.\n\nWith the lack of resources of KC-KDE, I would find it a pity that there are separated forces doing summary of KDE development.\n"
    author: "Philippe Fremy"
  - subject: "Re: and KC-KDE"
    date: 2002-11-29
    body: "> The thing I would like to see is cvs statistics per week:\n>  - 5 best comitters\n>  - nb of commits per projects\n\n 5 \"best\" commiters - how are you going to determine who's \"best\"? By the number of commits? Everybody receiving kde-cvs@ mails would be definitely happy to see traffic increase on this list because of some people trying to improve their stats by doing several small commits instead of one large. I don't mean anybody in particular, but there of course would be people tempted to do this. This is similar to the bugs.kde.org weekly statistics, but the consequences are different. The only way how to get better rank at bugs.kde.org is to close more bugs, even if simple ones - i.e. there's a good result of the stats, more bugs closed. For kde-cvs@, the only result would be kde-cvs@ traffic increase - not good at all.\n\n I find listing commits the way it's done in this review to be a much better way (but it of course requires a person like Derek who's brave enough to watch all the commits :), instead of simply counting them)."
    author: "L.Lunak"
  - subject: "Re: and KC-KDE"
    date: 2002-11-29
    body: "\"best\" is an inappropiate term. The five people that committed the most.\n\nFor your other argument, do you really think that KDE developers are ready to cheat in CVS commits or bug closing just for the pleasure or appearing in the top of the stats ? I don't think so. Usually, developers have more interesting stuff to care about.\n\nThe best way to close more bugs is to add many fake one at once, then close them all. Have you seen this happening ? I think you are underestimating the commitment of KDE developers. We are not here to have our names in internet but to help the project move forward.\n\nOf course commenting the commits is good. But I like to have statistics, it is always interesting.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: and KC-KDE"
    date: 2002-11-29
    body: "> \"best\" is an inappropiate term. The five people that committed the most.\nThat's quite easy. First one is the bot updating i18n stuff. Places 2-5 are for the 4 most active i18n teams. You could of course count only some kinds of people working on KDE, but which ones would that be, and would it be fair to the others? How was it ... ah yes, 'Lies, damned lies, statistics'.\n\n> ...KDE developers are ready to cheat...\nI wasn't talking about cheating. If I write some code which involves changing four source files, I usually commit them together, so that it's easier to track back the whole change later using the kde-cvs@ mail saved at lists.kde.org . But with statistics, I would be tempted to commit them separately, which would increase my number of commits. That's not cheating, is it? Even humble people like from time to time to be first. But besides lifting the developer up in the statistics, this would result only in bad things - kde-cvs@ more flooded, the change scattered over more kde-cvs@ mails. Nothing good from it, besides knowing that Joe Developer commited the most changes this week, because he changed one header file in kdelibs and had to change bazillion dependent files in whole CVS. How is this interesting?\n\nThe same way, I wasn't talking about cheating with bugs.kde.org . I meant people who simply go over the database and close/fix invalid/alreadyfixed/simple bugs. However, this has a good thing - the number of open bugs is reduced, and other developers don't have to mess with old bugreports and so on (and I'm grateful there are people who do such cleanups). That's the difference why I find bugs.kde.org statistics (at least somewhat) good and cvs statistics bad. Bugs.kde.org statistics may make some people do a better job, cvs ones can't. Some people may spend more time doing the boring bugs.kde.org stuff, but people won't code more than they already do just to get better rank in statistics."
    author: "L.Lunak"
  - subject: "Re: and KC-KDE"
    date: 2002-11-30
    body: "'5 \"best\" commiters - how are you going to determine who's \"best\"?'\n\nI got a quick hackup using J. Mallett's cvstat at http://members.shaw.ca/dkite/log.html\n\nNot a useful log of anything except to see what stats would tell us. I hacked the perl script to give me the top 5. \n\nThe high number for some developers is merging the kroupware and make_it_cool branches of kmail.\n\nIs the information useful? I don't know. I think over time you would see certain developers again and again. \n\nAnyways, it's quite easy to produce. I use the cvs log for other stuff also. If there are too many complaints, I'll drop it.\n\nderek\n"
    author: "Derek Kite"
  - subject: "Thanks for the update"
    date: 2002-11-28
    body: "I've been wondering what's holding up KDE 3.1 when the release schedule says that RC4 should be out by now, or released. As that posting in your report suggests, new tarballs will be up following the fixes of the 25th's tarballs; will this be the final 3.1?"
    author: "Paul Kucher"
  - subject: "Re: Thanks for the update"
    date: 2002-11-28
    body: "Yes I am with Paul (Kucher) on this one, what IS happening with the KDE 3.1 release. Adecision was supposed to have been made 4 days ago yet I have not seen a word about any problems or updated release schedule.\n\nHell of course I am sympathetic that most of the developers are doing this in their time and we must remember and respect that. But...they have always been punctual in the past, if not with releases then always with information on the release.\n\n\nFinally is it just me or is KDE quality slipping a little, I am concerned as long time user of kde that they (the developers) are perhaps pushing themselves too hard. Sure we all adore the eye candy, and fantastic features they bring us, but not if good old apps start to get buggy.\n\nJust a few thoughts from a still loyal and very very grateful user of KDE.\n\nCheers\n\n\"Racism ? = How so, there is just one race, The Human Race.\" anon\n\n"
    author: "Paul Sauter"
  - subject: "Re: Thanks for the update"
    date: 2002-11-28
    body: "Sorry you can't have it both ways. Late software = better quality. The delays are due to bugfixes. kdebindings required lots of late work. \n\nAs I say to my daughter all the time, \"Patience is a virtue\"  (ducking)\n\nBrowse through the cvs list and you will see huge numbers of real quality enhancing work. There are lots of discussions on how things look, but the vast majority of the code updates are dealing with how things work.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Thanks for the update"
    date: 2002-12-04
    body: "Derek,\n\nI agree with you: Late soft != better quality.\n\nBut...\n\nWhat about the project coordination? Give us a new release date.\n\nWe are totally crazy to see the new features working asp.\n\nThanks to all KDE developers."
    author: "Paulo Junqueira"
  - subject: "Re: Thanks for the update"
    date: 2002-11-29
    body: "RC4 went to packagers, with the expectation of a release a week later.  It got delayed because of more last-minute bugfixes, and an updated release candidate (RC5) went to packagers a few days ago.  My guess is hopefully early next week will be 3.1 final.\n\nBarring insanely important changes, the tarballs available to packagers will be moved to the stable tree very shortly."
    author: "Benjamin Reed"
  - subject: "Re: Thanks for the update"
    date: 2002-11-29
    body: "> Adecision was supposed to have been made 4 days ago yet I have\n> not seen a word about any problems or updated release schedule.\n\nWell, updating the release schedule is a little pointless now that the release has finished. However, there's news on kde-core-devel:\n\nhttp://lists.kde.org/?l=kde-core-devel&m=103824444618699&w=2"
    author: "Chris  Howells"
  - subject: "Re: Thanks for the update"
    date: 2002-11-29
    body: "OK Thanks Derek, Benjamin and Chris.\n\nNow that you have pointed out what the reason is I am once again satisfied. I really didn't mean to come across as impatient or annoyed by this. However I have been a little miffed because for the first time with KDE I have been suffering critical data losses through kmail. But comparing that to multitude of times when I have been so grateful for having had a reasonably upto date backup under windows (tm - registered trashmark :-)) I guess I should have been a little more patient.\n\nAnyhow with the release of 3.1 now imminent I look forward to utilising the efforts of this outstanding community of developers (and also others).\n\nFor the record as someone who runs a multi-national non-profit I really would be lost without the KDE communitys' efforts as I would not be able to afford the extortinate prices expected for such feature laden quality software. The impact of having to pay for such software would doubtlessly affect our projects massively and thereby the professionals and patients we serve.\n\n\nSo a big THANK YOU \n\nfrom ICORP\n"
    author: "Paul Sauter"
  - subject: "cool they mentioned one of my bugs :)"
    date: 2002-11-28
    body: "cool they mentioned one of my bugs :) ... if only they would do something about\n\nhttp://bugs.kde.org/show_bug.cgi?id=51061"
    author: "cbcbcb"
  - subject: "Re: cool they mentioned one of my bugs :)"
    date: 2002-11-28
    body: "Actually there were some patches that were committed to fix memory leaks in konq. No links, sorry.\n\nDerek"
    author: "Derek Kite"
  - subject: "Greetings from Winnipeg"
    date: 2002-11-28
    body: "Glad to see another Canadian into KDE ;-)"
    author: "John Hughes"
  - subject: "Re: Greetings from Winnipeg"
    date: 2002-11-29
    body: "Me too! Me too! (There are lots of us from up here in Canukistan ;)"
    author: "Sheldon"
  - subject: "Re: Greetings from Winnipeg"
    date: 2002-11-29
    body: "*chuckle* canukistan ..\n\nit would be cool to get a bunch of KDE'ers together out here in the West sometime. i've yet to actually meet another KDE hacker in person. sad but true. =/\n\ni'm madly jealous of zack and ian's pending xmas adventure in phili."
    author: "Aaron J. Seigo"
  - subject: "Re: Greetings from Winnipeg"
    date: 2002-11-30
    body: "hi guys, with a little luck I'll be in vancouver in time for christmas for a nice long stretch, so I'd be glad to catch up with other KDE folks. You have no idea how lonely it was without other KDE developers in Oz. ;)\n\n(Well not counting Martin Jones but brisbane and melb are far enough for it to feel like a different country)"
    author: "taj"
  - subject: "Improvements"
    date: 2002-11-29
    body: "The content is fine (even 2 bugfixes of me have been mentioned ;-).\n\nSo some improvements I have in mind:\nGeneral:\n- The style is a bit compressed. Please use more breaks, indentation and spaces to seperate the content a bit.\n- I like the usage of different font colors as in the cousins. How about using it here as well?\n\nBugfixes part:\n- Use a \"tab\" to align the bugnumbers\n- Seperate by module like:\n  kdelibs:\n    + khtml:   50956, 35408,...\n    + kdecore: 51026\n    ...\n  kdebase:\n  ...\n- If you post the text as well, just make the bugnumer as a link, the text doesn't need to be (just optical)\n\nNew code additions part:\n- Put in a header, what is affected (module or appname).\n- I think links to koffice or kopete HP are not necessary. The readers of cvs/interested persons of what's going on on cvs and the link disturbs a bit the optical impression (or it gives me the impression there is something new to look at).\n- The whole part is bold here (Mozilla), I asume you forgot to close the bold tag after the header.\n\nThanks anyway for your work,\n\nPhilipp"
    author: "Philipp"
  - subject: "Re: Improvements"
    date: 2002-11-30
    body: "> I like the usage of different font colors as in the cousins\nI'll be using the cousins templates, and be included in the cousins. Hopefully the formatting will be better.\n\n> Use a \"tab\" to align the bugnumbers\nGood idea\nA few comment so far seem to indicate the desire for more information on the page, less links. So for the bugfixes, I plan (here we go, show me the code) to do a ...\n\nProduct \n Component - bugnumber - comment\n\t             bugnumber - comment\n\t\t     ...\t\ngrouping the product and components. A script should be able to produce this reasonably easily.\n\n>I think links to koffice or kopete HP are not necessary\nActually, I put the links for the most part when I read about a commit to say, k3b or kexi or kopete, and I didn't know what in blazes the application was. Then I got carried away...\n\n>The whole part is bold here (Mozilla)\nReally. Konq works. (checking in mozilla). Yes, the last part. And indeed I didn't close the bold. How come Konqueror does differently? A bug?\n\nDerek"
    author: "Derek Kite"
  - subject: "AC's comment"
    date: 2002-11-30
    body: "Is it just me or is KDE CVS getting buggier to the end ?\n\nI usually re-compile KDE CVS once per week using latest sources and I found out that the general stability of it is going heavily down. With todays re-compile I got a shitload of crashes and segfaults in various places that were stable with the last compile. I know dealing with CVS will heavily lead into this siutaitons but I also know that 3.1 will come out really short and therefore I assume that it should get more stable instead of instable."
    author: "AC"
  - subject: "Re: AC's comment"
    date: 2002-11-30
    body: "How shall I put this... that harmless looking bugfix from yesterday caused some regression.\n\nOn the bright side, it is fixed already. Just update kdelibs/kdeui."
    author: "anonymous KDE developer"
  - subject: "Re: AC's comment"
    date: 2002-11-30
    body: "<i>but I also know that 3.1 will come out really short and therefore I assume that it should get more stable instead of instable.</i>\n\n3.1 has been branched, HEAD is now open. So expect it to get less stable, especially in the next months...\n\n"
    author: "AC"
  - subject: "Re: AC's comment"
    date: 2002-11-30
    body: "Ahh that may be a reason then. I still thought that HEAD is 3.1 (This explains the huge changes)."
    author: "AC"
  - subject: "very good!"
    date: 2002-12-01
    body: "I wish the fixed bugs had more of a description on the various components, vs just the bug numbers.  otherwise excellent.\n\n+10 points for putting in my email about fixing italic fonts with XFT and adjusting fonts.conf ;)"
    author: "Dave B."
  - subject: "Re: very good!"
    date: 2002-12-03
    body: "Dave:\n\nI will be changing the format of the bug list. I will have product / component then bug number and the short comment from bugs.kde.org. Should be better with more information. Seems a common request is to have more information on the page rather than having to link to it.\n\nDerek"
    author: "Derek Kite"
  - subject: "I want my kde 3.1 !!!"
    date: 2002-12-04
    body: ":-("
    author: "Anonymous....."
  - subject: "Re: I want my kde 3.1 !!!"
    date: 2002-12-04
    body: "Me too!! ;-) What's happening?"
    author: "starflight"
  - subject: "Re: I want my kde 3.1 !!!"
    date: 2002-12-05
    body: "Relax, better a stable and secure product than a quick release. There are some issues to take care of so let em do that! Anyways we got a very nice 3.0.5 to use in the mean time:)"
    author: "DiCkE"
  - subject: "Re: I want my kde 3.1 !!!"
    date: 2002-12-05
    body: "Of course, but a little bit of more info woul not be bad.... why so secretive? "
    author: "myself"
  - subject: "Re: I want my kde 3.1 !!!"
    date: 2002-12-05
    body: "I am crying, I want my kde 3.1, and I want it now!!!\n"
    author: "Kennet"
  - subject: "Re: I want my kde 3.1 !!!"
    date: 2002-12-06
    body: "You'll have to wait, according to latest news, it's going to be delayed at least until after Xmas...\nhttp://www.pclinuxonline.com/modules.php?name=News&file=article&sid=3992\n"
    author: "myself"
  - subject: "Straight from kde.kde-core-devel:"
    date: 2002-12-06
    body: "KDE 3.1: delayed\nFrom:Dirk Mueller <mueller@kde.org>\nReply-To:kde-core-devel@kde.org\nDate:Friday 06 December 2002 01:50:12 am\nGroups:kde.kde-core-devel\nno references\n\nHi, \n\nThe KDE 3.1 release has to be delayed further. Here is why. \n\nOn November 26th, we've been notified by FozZy from the \"Hackademy \nAudit Project\" about security problems in KDE. They can, after user \ninteraction, cause unwanted execution of commands with the\nprivileges of the user who runs KDE. We fixed those on the same day and \nupdated the \"hopefully final\" KDE 3.1 tarballs. Unfortunately, it was \nbecoming clear after a quick search in the KDE CVS that the \nproblematic code is repeated in many places and in many variations. \n\nYesterday, on the targetted announcement date of KDE 3.1, Waldo and I \nrealized that while we only had audited maybe 30% of the code yet, we have \nfound enough occasions for them to be a big showstopper.\n\nA short query on the packagers mailinglist showed that for the majority \nthere is no big pressure on having a KDE 3.1 to be released \naccording to the schedule. I'm considering a 3.1 with known security bugs a \nno-go anyway, even though we first thought that those are minor that the fix \ncan wait for 3.1.1, I no longer think that this is the case.\n\nWaldo, George, Lubos and I think that we can finish the audit by middle/end\nof next week. This however brings us in a bad position: its unlikely that we\nget many binary packages so short before christmas holidays, which means \nthat KDE 3.1 would go out, if released this year, probably with few or \nnone binary packages at the announcement date. \n\nSo, to sum up, we have two options:\n\na) Try to finish ASAP and try to get it out before christmas. December\n   12 could be a good tagging date.\n\nb) Take the time and schedule for a release next year. Something around\n   January 8, 2003 sounds like a good candidate (tagging date,\n   announcement rougly a week later)\n\nI neither like any of them, but I prefer to go with b), as it also allows\nfor other bugs which have been reported to be fixed. For an impression just \nhave a look at the lately steadily rising open bug count on \nhttp://bugs.kde.org/. \n\nIn any way I'll tar up and release the current 3_1_BRANCH as 3.1RC5 in a few \nhours. Many fixes for the above mentioned security problems are in there, \nbut there are still big chunks of code and patches pending for review. There \nwill be no binary packages as those which were made during the last week \nrefer to be \"KDE 3.1 final\" and are anyway not up to date. \n\nAs soon as the code review is finished we will have to release updates for \nKDE 3.0.x (and at least patches for KDE 2.x) anyway. \n\nComments, opinions, suggestions, flames welcome. \n\n\nDirk"
    author: "Johan Veenstra"
  - subject: "Re: I want my kde 3.1 !!!"
    date: 2002-12-06
    body: "You will have to wait: http://lists.kde.org/?l=kde-core-devel&m=103913196531620&w=2"
    author: "Anonymous"
  - subject: "Take a look at this!"
    date: 2002-12-04
    body: "Dunno 'bout you guys, but I LOVE this!\n\nhttp://www.kdelook.org/content/show.php?content=4080"
    author: "Janne"
  - subject: "Re: Take a look at this!"
    date: 2002-12-04
    body: "Oh, yes, the kicker alternative mockups that are floating around. I quite like the way that particular one looks - reminds me somewhat of Cloud9:ine, SharpE and similar shell replacements of Windows, and a bit of the BeOS tracker, too, but far more elegant. Would *love* to see a kicker like that. :)"
    author: "Kyro"
  - subject: "kdebase cant compile"
    date: 2002-12-05
    body: "source='crypto.cpp' object='crypto.lo' libtool=yes \\\ndepfile='.deps/crypto.Plo' tmpdepfile='.deps/crypto.TPlo' \\\ndepmode=gcc3 /bin/sh ../../admin/depcomp \\\n/bin/sh ../../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I../.. -I/opt/kde/include -I/opt/qt//include -I/usr/X11R6/include    -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -O2 -fno-exceptions -fno-check-new -DQT_CLEAN_NAMESPACE -DQT_NO_COMPAT -DQT_NO_ASCII_CAST -D_GNU_SOURCE  -c -o crypto.lo `test -f crypto.cpp || echo './'`crypto.cpp\ncrypto.cpp: In member function `void KCryptoConfig::slotVerifyCert()':\ncrypto.cpp:1382: `SSLServer' is not a member of type `KSSLCertificate'\ncrypto.cpp: In member function `void KCryptoConfig::slotYourVerify()':\ncrypto.cpp:1667: `SSLClient' is not a member of type `KSSLCertificate'\ncrypto.cpp:1669: `SMIMEEncrypt' is not a member of type `KSSLCertificate'\ncrypto.cpp:1671: `SMIMESign' is not a member of type `KSSLCertificate'\nmake[3]: *** [crypto.lo] Error 1\nmake[3]: Leaving directory `/home/user/cvs/kde31/cvs/kdebase/kcontrol/crypto'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/home/user/cvs/kde31/cvs/kdebase/kcontrol'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/home/user/cvs/kde31/cvs/kdebase'\nmake: *** [all] Error 2\n\nwhat am i doing wrong?"
    author: "user"
  - subject: "Re: kdebase cant compile"
    date: 2002-12-06
    body: "You're doing several things wrong:\n\n1) Posting this message here ;-) You can post in the mailing-lists better: lists.kde.org. It's a better place for questions like this\n2) You don't specify what version you're trying to use, so it's difficult to say why you're having that problem (at least for me)\n3) You are not using a stable version, or you are mixing libraries from a different version, according to your output, since it looks like the members of class KSSLCertificate don't corresponde to the program you are using.\n"
    author: "myself"
---
I have started doing <a href="http://members.shaw.ca/dkite/index.html">a weekly review</a> of the <a href="http://lists.kde.org/?l=kde-cvs&r=1&w=2">kde-cvs</a> updates. Check out <a href="http://members.shaw.ca/dkite/latest.html">the latest issue</a> and please comment. Any ideas that would make the information more useful is welcome. My purpose is to give links into the code, and some pertinent discussion. This hopefully will add to the body of discussion and information available to users and developers of kde. Please be easy on the server...
<!--break-->
