---
title: "KDE 3.0: A New Era In Desktop Choice"
date:    2002-04-04
authors:
  - "Dre"
slug:    kde-30-new-era-desktop-choice
comments:
  - subject: "Congrats and thanks to the KDE3.0 team"
    date: 2002-04-03
    body: "Have just downloaded and I'm going to install now - w00t! :)\n"
    author: "Joergen Ramskov"
  - subject: "Screenshot: Liquid on kde3"
    date: 2002-04-03
    body: "Looks nice"
    author: "Fake_oliver"
  - subject: "Re: Screenshot: Liquid on kde3"
    date: 2002-04-03
    body: "Thats is a fake me right? But we have the same desktop... This is getting strange! Ohh well its beautfull and you can do anything on it. Its hte most important \"Air\" not \"Send a Bill\""
    author: "Oliver"
  - subject: "Re: Screenshot: Liquid on kde3"
    date: 2002-04-04
    body: "It's no fake, the culprit who posted that owned up.  :-)\n<p>\nwww.kde.org has <a href=\"http://www.kde.org/screenshots/kde300shots.html\">some other screenshots</a> too. (the njaard link was to some outdated ones)\n\n\n"
    author: "Navindra Umanee"
  - subject: "Re: Screenshot: Liquid on kde3"
    date: 2002-04-04
    body: "That's an awesome screenshot."
    author: "Navindra Umanee"
  - subject: "Re: Screenshot: Liquid on kde3"
    date: 2002-04-04
    body: "Time to point out the original then (IIRC Oliver copied it after seeing it):\n<p>\n<a href=\"http://chrishowells.co.uk/liquid.png\">http://chrishowells.co.uk/liquid.png</a> :)"
    author: "Chris Howells"
  - subject: "Another one: liquid, xinerama, anti-aliasing"
    date: 2002-04-04
    body: "Here's another one with Noatun, Liquid and my own color scheme, transparent Konsole, and a kuickshow image window. I am using anti-aliasing with bytecode interpreter enabled, desktop backgrounds from digitalblasphemy.com, the slick icon theme, glow window decorations on two monitors, shown here with backgroud aligned so that each monitor seems to have a different background.\n\nThanks to Bero for adding Xinerama support to his rpms.\n\nRavi\n"
    author: "Ravi"
  - subject: "Re: Another one: liquid, xinerama, anti-aliasing"
    date: 2002-04-05
    body: "how many screens do you have to fit all that on it? heheh"
    author: "Oliver"
  - subject: "Re: Screenshot: Liquid on kde3"
    date: 2002-04-04
    body: "Where did you get the background picture?"
    author: "davec"
  - subject: "Re: Screenshot: Liquid on kde3"
    date: 2002-04-04
    body: "It should be in the kde-base package ready to use.\n\ntroy(at)kde(dot)org"
    author: "Troy Unrau"
  - subject: "Re: Screenshot: Liquid on kde3"
    date: 2002-04-04
    body: "It's not in kdebase or kdeartwork (at least it's not in the SuSE 7.2 rpms of those packages...)."
    author: "davec"
  - subject: "Re: Screenshot: Liquid on kde3"
    date: 2002-04-04
    body: "I wish people would give more information on how to replicate a look like their screenshots! :)\n\nCan someone please tell me which font that screenshot is using?  I assume anti-aliasing is on, and that it is using the Liquid theme.\n\nThanks.."
    author: "Craig"
  - subject: "Re: Screenshot: Liquid on kde3"
    date: 2002-04-04
    body: "Ok The \n<p>\nFont : Microsoft Verdana (fetchmsttf (suse script ported by me for debian hehe))<br>\nBackground : <a href=\"http://www.kdelook.org/content/show.php?content=1038\">http://www.kdelook.org/content/show.php?content=1038</a>\n<p>\nFor the size of the 3 menus (since i havent been using KDE for long) i hade to edit some temperarly config files but the screenshot was taken with a cvs release (rc3) little time befor the v3 officialy came out.\n<p>\nBut the most beautifull looking is Licq kudos for the developers who really worked hard along witht he kde team for such a wonderfull productivity pot.\n<p>\nI dont think i can hekp anyone else make a desktop since its so easy under kde3 a wonderfull ride. (Note even my parents use it :D)\n"
    author: "oliver"
  - subject: "Re: Screenshot: Liquid on kde3"
    date: 2002-04-04
    body: "You mean \"apt-get install msttcorefonts\" right? :) Why make life hard for yourself?"
    author: "Stuart"
  - subject: "Re: Screenshot: Liquid on kde3"
    date: 2002-04-05
    body: "heh because those are the core fonts i dont know what they include but my script must be better with all the time i spent on it :D"
    author: "Oliver"
  - subject: "Re: Screenshot: Liquid on kde3"
    date: 2002-04-05
    body: "ill also include the orginal script if someone wants it.. note the files required have to moved from /usr/bin to /usr/X11R6/bin\n"
    author: "Oliver"
  - subject: "Excellent!"
    date: 2002-04-03
    body: "Congratulations to everyone involved with KDE!  Version 3 is great.\n"
    author: "Jason"
  - subject: "mirrors"
    date: 2002-04-03
    body: "Waiting for the mirrors to update...\n\nMaybe I cheated a bit and got a jump start. I saw the announcement on CVS :-)"
    author: "Andy Goossens"
  - subject: "Nif-tay"
    date: 2002-04-04
    body: "Is it just me or are these press releases getting longer every time? :-)\n\nOh well, the develoepers deserver a little ego inflation. Keep up the good work!"
    author: "Carbon"
  - subject: "Re: Nif-tay"
    date: 2002-04-04
    body: "This is the one of the best 'press releases' I've ever seen. All informations are covered by the announcement. Kudos to Andreas. "
    author: "antialias"
  - subject: "Kongratulations and thanKs!"
    date: 2002-04-04
    body: "Now, where can I get KeramiK (without having to Kompile it)? Any chance to find a binary pacKage?"
    author: "Anonymous"
  - subject: "Re: Kongratulations and thanKs!"
    date: 2002-04-04
    body: "No, Keramik isn't out yet, it will be in KDE 3.1."
    author: "Rob Kaper"
  - subject: "Re: Kongratulations and thanKs!"
    date: 2002-04-04
    body: "I've heard of no binaries for it, as it's still in beta.  Compile away."
    author: "ThatComputerGuy"
  - subject: "Re: Kongratulations and thanKs!"
    date: 2002-04-05
    body: "And how do I compile it? I've checked out the cvs module, but there's no configure file and no makefile (just a Makefile.am)?"
    author: "Ante Skaro"
  - subject: "Re: Kongratulations and thanKs!"
    date: 2002-04-06
    body: "Add \"keramik\" to SUBDIRS in kdelibs/kstyles/Makefile.am and it will be compiled along with the rest after you did make -f Makefile.cvs && ./configure on kdelibs. "
    author: "Rob Kaper"
  - subject: "Re: Kongratulations and thanKs!"
    date: 2002-04-06
    body: "Keramik is completely removed from kdelibs in KDE_3_0_BRANCH and release tarballs."
    author: "Anonymous"
  - subject: "Re: Kongratulations and thanKs!"
    date: 2002-04-05
    body: "And how do I compile it? I've checked out the cvs module, but there's no configure file and no makefile (just a Makefile.am)?"
    author: "Ante Skaro"
  - subject: "Re: Kongratulations and thanKs!"
    date: 2002-04-04
    body: "In RH you will ;-)"
    author: "ne..."
  - subject: "Re: Kongratulations and thanKs!"
    date: 2002-04-04
    body: "Hmm, are you sure? \n\nThere is a very good reason Keramik is not part of the 3.0 release:\nIt is not finished. \n\nAnd shipping unfinished work as part of a stable release is, IMHO,\nwholy inappropriate\n\n"
    author: "Sad Eagle"
  - subject: "Re: Kongratulations and thanKs!"
    date: 2002-04-04
    body: "Well it was installed on a machine I installed the RH KDE rpms on. RH7.2.92 tho."
    author: "ne..."
  - subject: "what's keramik?"
    date: 2002-04-04
    body: "and where be its homepage? i can't find it. \n\njohnson"
    author: "johnson"
  - subject: "Re: what's keramik?"
    date: 2002-04-04
    body: "A very kool desktop style. Search www.kde-look.org for keramik to get a screenshot."
    author: "Anon"
  - subject: "Re: what's keramik?"
    date: 2002-10-17
    body: "I haven't found a homepage for it but its a really cool theme to be incorporated in the KDE 3.1 release.  If you get the betas you'll see them.  Theres tons of screenshots involved @ kde-look.org\n\nI personally would like to find out who originally made it and who the artwork is credited to.  to install Keramik you'll need to get the window decoration and the style, the icons just use whatever u want. (i like Connectiva Classic by Everaldo)"
    author: "Kal"
  - subject: "Welcome new users and thanks to NU"
    date: 2002-04-04
    body: "\nI just wanted to say to wish the people about to try the modern KDE for the first time \"Welcome!\"  I'm sure you will enjoy the experience.\n\nI also wanted to congratulate Navindra Umanee - the Dot is weathering the traffic with flying colors, just like he said it would.  A tricky thing to accomplish, and he has suceeded quite nicely.  Heh - at the moment, www.kde.org is down, but the Dot is up.  Go, Dot!\n\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Welcome new users and thanks to NU"
    date: 2002-04-04
    body: "Thanks, this article is huge at a record 52 Kb.  Someone posted a big screenshot too. :-)\n\nDre deserves a big congrats for working his ass off on the announcement itself, despite the fact that errors seem to have been introduced last minute (not his fault).\n"
    author: "Navindra Umanee"
  - subject: "KDE the Threedom Desktop"
    date: 2002-04-04
    body: "I only want to say this."
    author: "random"
  - subject: "KDE3 is great"
    date: 2002-04-04
    body: "I recently installed MDk 8.2 and was fighting with sound board and cdrom until today when I changed do kernel22 (no ext3, damn).\nBut I have to say that I loved that Mandrake made kde3 in a way you can keep kdelibs from kde 2.x, so I can still run kde 2.x apps while I can't compile them or there are no rpms.\nI'm really loving mdk 8.2 with kde3 :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE3 is great"
    date: 2002-04-04
    body: "I was worried what upgrading only KDE base and KDE libs might do to the rest of my other KDE applications on Mandrake.  Good to know I don't need to download everything!"
    author: "ac"
  - subject: "3 Cheers for KDE!!!!!"
    date: 2002-04-04
    body: "The KDE project produces some of the best software ever made, and I would just like to say thank you for all the great software they produce."
    author: "richie123"
  - subject: "Boy Howdy!"
    date: 2002-04-04
    body: "I'm pumped, I can't wait to get this bad boy off the ground!\n\nCongrats you mindbogglingly swell KDE hackers.\n\nAlmost forgot, THANKS."
    author: "Eric Nicholson"
  - subject: "Mandrake 8.2 Tip & Update"
    date: 2002-04-04
    body: "At first I was unable to access either my program menus, or my control module listing in Kcontrol.  This of course rendered KDE pretty useless. \n\nThe fix is to run rpmdrake & click save.\n\nAfter that, runs like a charm. Compiled the new liquid ... very nice.  Excellent release guys!"
    author: "Eric Nicholson"
  - subject: "Re: Mandrake 8.2 Tip & Update"
    date: 2002-04-04
    body: "What is the best way to upgrade/install KDE3.0 ? I have Mandrake 8.2 with KDE 2.2.2 now. I didnt find any README file where I downloaded the KDE3.0 rpm's which sucks a bit. :)\n\nThanks"
    author: "Emil"
  - subject: "Re: Mandrake 8.2 Tip & Update"
    date: 2002-04-19
    body: "install all the rpms...don't remember the order, but you shouldn't force any...add \"KDE3\" to the loggin manager under the control panel...."
    author: "Forge"
  - subject: "Re: Mandrake 8.2 Tip & Update"
    date: 2002-04-04
    body: "Eric,\n\nI am having the same problem. Kicker seems to be bust.\n\nCould you elaborate on your rpmdrake fix. \nIt does not have a save button.\n\nYours,\n\t-Heikki\n"
    author: "Heikki"
  - subject: "Correction, not rpmdrake of course"
    date: 2002-04-05
    body: "I'm an idiot, or it was really late, or both. \n\nAnyway, that should read:\n\nThe fix is to run _menudrake_  & click save.\n\nSorry for the confusion."
    author: "Eric Nicholson"
  - subject: "I'm seriously impressed by KDE 3.0"
    date: 2002-04-04
    body: "I haven't seen a screenshot, let alone tried it, but I'm very impressed by the professional anouncement. Everything I want to know is there, and more.\n\nJust some tidbits that struck my eye:\n\n*    KDE: The Complete Enterprise Desktop Solution\nA very comprehensive summary of what you can do with KDE as an enterprise desktop, very nice.\n\n*    KDE 2 applications will work with KDE 3 if the KDE 2 libraries are present.\nTakes away a lot of doubt about wether an upgrade is safe (as in will it brake my older apps)\n\n*  KDEPrint\nIf I read the anouncement correctly, KDEPrint is a huge step towards making the linux-desktop a reality.\n\n* Internationalization\n'49 languages' and 'And multiple font encodings and directions can be displayed in the same document' says it all.\n\n* Konqueror plug-ins\nNetscape Plug-ins(Flash, RealAudio, RealVideo), Codeweavers Plug-ins (QuickTime, ShockWave Director, Windows Media Player 6.4, Word Viewer, Excel Viewer, PowerPoint Viewer )\n\n* Word processing\n'ideal for those who need not frequently interchange MS Word documents with others, though this limitation should disappear with the release of KOffice 1.2 in mid-August'\n\n* KDevelop\nWith KDE 3.0, KDevelop has benefitted from a greatly improved Qt Designer, which now supports interactive construction of the application main windows with menus and tool bars in addition to dialogs.\n\n* Library Requirements / Options\nComprehensive set of requirements/options for those who want to compile KDE themselves and still want all possible options.\n\nand lots more, too much to summarize actually,\n\ncongrats,\n\nJohan V."
    author: "Johan Veenstra"
  - subject: "Re: I'm seriously impressed by KDE 3.0"
    date: 2002-04-04
    body: "I second that!\n\nThis has to be the most complete KDE announcement ever. I especially liked the Library Requirements section (way overdue), as now the FULL functionality of KDE can finally be realised. Especially useful for those compiling from source.\n\nWell done KDE PR Team!\n\n--J"
    author: "Jason Hanford-Smith"
  - subject: "No need to say anything buy..."
    date: 2002-04-04
    body: "... I love it!\n"
    author: "Jesper Juhl"
  - subject: "debian packages?"
    date: 2002-04-04
    body: "Anyone have the skinny on when debian packages will be available?"
    author: "victwenty"
  - subject: "Re: debian packages?"
    date: 2002-04-04
    body: "Yep, they are being done by calc, he's done kde-i18n, kdegraphics, kdeadmin, kdebindings, parts of kdelibs and kdebase. The .debs will likely not be in sid but on the ftp.kde.org site (apt-gettable) due to some linkage problems (to put it bluntly). \n\nPraise the almighty calc, and be patient until the packages are done in a few days. ;) Yours, -mcornils (aka Arondylos)"
    author: "Malte Cornils"
  - subject: "Re: debian packages?"
    date: 2002-04-05
    body: "I was a bit disappointed to find that the debian scripts are too out-of-date or just plain broken and can't generate working packages.  Building arts, and kdelibs isn't much of a problem, but kdebase doesn't build all the packages properly.  Looks like they were intended to build rc2 or something...  :("
    author: "Chad Kitching"
  - subject: "Re: debian packages?"
    date: 2002-04-06
    body: "Where will these debian-packages be found anyways (when tehy are ready)?"
    author: "Johannes Wilm"
  - subject: "Re: debian packages?"
    date: 2002-04-08
    body: "Hi\n\nIs there a projected date for Debian (Potato/Woody) packages for KDE 3?\n\nIf it is far off, is it worth trying to build the KDE 3 myself, or are there likely to be problems with GCC/GLIBC ? \n\nIn the past, when using RH, I built KDE several times without any problems, but I now use Debian (Potato and Woody) and I am aware that the versions of GCC/GLIBC used are old, but very stable."
    author: "Simon Windsor"
  - subject: "Re: debian packages?"
    date: 2002-04-08
    body: "I'd be real suprised if you see KDE3 in Potato, and woody will most likely be a long time. As you probably know, Woody is mainly frozen pending a release soon. I keep checking hoping I'll see sid packages, and the rumor was that there'd be debs floating around RSN, though not from the official sites. I can't speak to compiling KDE3 myself.\n\nHTH,\nErik"
    author: "Erik Severinghaus"
  - subject: "KDE the only desktop"
    date: 2002-04-04
    body: "first of all,\n\nthank you for the beautifull kde 3 release. this made me switch from gnome 1/2 to kde 3. i was a contributor to gnome for a couple of years now. always followed the troll and crap others mentioned like QT or KDE sucks etc. then one day. i was playing with gnome 2 installation and i said 'jesus is this gnome ? this is what we all have waiting for ? where is a gnome 2 suitable webbrowser, where is a gnome 2 suitable email client... without having to install gnome 1 and gnome 2'.. anyways thats how may days until then looked. then one day i said myself 'hey lets try kde 3 cvs... play for 5-10 mins and then remove that again' ... but then 10 mins passed, 20 mins passed..... 3 days passed after the 4th day i removed gnome from my system and realized that i was totally wrong hooking up the gnome road. the functionality in kde 3 really impressed me. the cool framework, the cool applications, the way you can use your desktop a REAL desktop. i must say that i never used a DESKTOP under linux the way i am using kde now. its stupid if people say things like 'gnome beat kde' since its not true everyone who really tried kde will realize it. to say the truth i cant wait for kde 3.1 now.. i want that keramik theme :-) so far good work kde people. you did a really cool job and thank you for openening the eyes of a ex-gnome user/contributor. i wasted my time for sooooo long with that shit. thank you."
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-04
    body: "Amen. Thanks for the insurmountable desktop."
    author: "Hude"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-04
    body: "Dude, I'm having the idea that you are a troll yourself... KDE isn't made to kill GNOME and neither is GNOME made to kill KDE. Even if KDE 3 is so great that's no reason to make stupid negative comments about GNOME.\n\n> always followed the troll and crap others\n> mentioned like QT or KDE sucks etc\n\nOh? I've always seen more anti-GNOME trolls than anti-KDE trolls on Slashdot.\nHeck, there are even more anti-Linux trolls than all the anti-GNOME/KDE trolls together!"
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-04
    body: "oh please excuse me. since i was used to gnome for all the long time i was only able to compare gnome with kde and i was speaking of myself only. no need to place me into the troll category. you are a pissed off gnome user right ? pissed because someone care to say the plain truth."
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "<anti-troll-mode>\n\nBut you are behaving like a troll, _right now_. Irregardless of the fact that you're making the common troll error of thinking KDE and GNOME are enemies, you're also (seemingly) :\n\n- Placing yourself in a position of being undebatably correct (\"pissed because someone care to say the plain truth.\").\n- Not even bothering to use proper capitalization, grammar, or even line breaks.\n- Being really sarcastic (\"oh please excuse me.\" and \"you are a pissed off gnome user right ?\").\n- Ranting and raving against the commentor instead of replying to the comment.\n\n</anti-troll-move>"
    author: "Carbon"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "Never complain about someone's grammer in a post where you use the word \"irregardless\"."
    author: "Peter Kasting"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "Irregardless is a word, though, and a valid one at that. English isn't a self-consistent language, look at 'flammable' and 'imflammable'."
    author: "Carbon"
  - subject: "Irregardless"
    date: 2002-04-06
    body: "Irregardless is a word, albeit one that any good dictionary will list only to tell you it's wrong. Kind of like 'burglarize', 'administrate' and 'object orientated'."
    author: "cosmo"
  - subject: "Re: Irregardless"
    date: 2003-04-03
    body: "I believe that in a debate that one can use this word to comeback with in rebuttal.Example... regardless of what people think \"irregardless\" is not a word. (rebuttal) Irregardless of what they think ,this word can be used against the first \"regardless\" in a debate. Contrary to thier belief. Example #2... \"Father ,regardless of what they say...I did not chop down the cherry tree!\" then the father say's \"Irregardless of what anyone say's ...I saw you chop down the cherry tree!\" "
    author: "Jeffrey Shane Linneman"
  - subject: "Re: Irregardless"
    date: 2003-05-27
    body: "Jeffrey Shane Linneman, you are wrong.  The ir- and the -less are redundant.  They constitute a double negative, which, in English, is never necessary.  \"Regarding\" (or the phrase \"with regard to\") and \"regardless\" are all you will ever need, as they succinctly express the only two possible states of existence of \"regard:\" the positive and the negative.\n\nNot only do you defend this stupid word, you argue that it is proper in debate, the setting where it is perhaps least acceptable. I don't know if you have ever won a debate, or debated at all, but using nonsensical words such as \"irregardless\" will do nothing but make you look (more) foolish.\n\nHow is one to respond to your rebuttal in a debate, \"unirregardless?\"  Followed by \"nonunirregardless?\"  At this point, I think even you can appreciate how wrong this is.  When is the addition of affixes to stop?\n\nLet's look at Example #2.  A son, apparently the young George Washington, wishes his father to ignore the testimony of unnamed third party witnesses (they) so he uses the word \"regardless,\" to indicate that his father is to consider his claim of innocence WITHOUT REGARD for \"what they say.\"  His father counters with an argument that is not dependent on the testimony of others, thus, it stands without regard to \"what anyone says.\"  In this case, he should use the word \"regardless,\" which, of course, means without regard.  Instead, Jeffrey, you give him the meaningless word \"Irregardless,\" adding to poor George's guilt the shame of his father's linguistic idiocy."
    author: "Edward Ballister"
  - subject: "Re: Irregardless"
    date: 2003-06-04
    body: "I think this debate is rediculous, and I feel that you could be spending your time doing something meaningful.  Volunteer, visit the sick, help a old lady acorss the street.  If irregardless wasn't a word then Webster would not have put it in his book.  Irregardless is a word.\n\n\n"
    author: "T Burt"
  - subject: "Re: Irregardless"
    date: 2003-06-11
    body: "Rediculous...and how does Webster spell that word?\n\nHelp a old lady, or help an old lady? ...acorss the street? Come on now, where the hell did you go to school? Or is it skool?\n\nAnd in case you did not know, you can help other people and still be smart. Who were you helping when you entered your comments? I am sure someone was being ignored by you for the moment..."
    author: "M"
  - subject: "Re: Irregardless"
    date: 2003-12-06
    body: "Irregardless is not a \"real\"word. Sorry."
    author: "Mike"
  - subject: "Re: Irregardless"
    date: 2003-06-04
    body: "I think this debate is rediculous, and I feel that you could be spending your time doing something meaningful.  Volunteer, visit the sick, help a old lady acorss the street.  If irregardless wasn't a word then Webster would not have put it in his book.  Irregardless is a word.\n\n\n"
    author: "T Burt"
  - subject: "Re: Irregardless and rEdiculous"
    date: 2003-11-12
    body: "you cannot make a fair judgement if u ur self cannot spell, this is how u spell 'ridiculous'. and if we can't all go to visit old people, or visit the sick,a debate (no matter how rIdiculous the subject) helps us to understand things better, and to apprecitae others points of view. Think again before you ruin others moments. "
    author: "alesha"
  - subject: "Re: Irregardless and rEdiculous"
    date: 2003-11-25
    body: "this is silly.  first of all, who cares about helping old people?  they're going to die irregardless of whether we help them.  in many ways the death of old people is an appropriate metaphor for this little battle of symantics and small dicks.  like old people exposed to asbestos or kryptonite, language is alive and mutates.  it's democratic in that sense--it's constantly changing, evolving, developing, and dying.  deal with it.  irregardless might sound retarded to you but it's a word and it's become synonomous with regardless.  if everyone started spelling school with a \"k\", then soon skool would become a word and an acceptable one at that. can't you see Edward the Queef? this is linguistics in action; it's happened before and will (hopefully) continue to happen.  Eddie, get over yourself and stop being such a snob.    "
    author: "JNCESQ"
  - subject: "Re: Irregardless and rEdiculous"
    date: 2003-11-25
    body: "i meant semantics and queef-fart, not symantics and queef.  my b"
    author: "JNCESQ"
  - subject: "Re: Irregardless and rEdiculous"
    date: 2006-05-10
    body: "It always amazes me, when I see this kind of rhetoric about anything, especially the elderly. THIS simply shows the ignorance and simple mindedness of the writer , in regard to life in general.\n\nIGNORANCE.......\"lacking in knowledge of either general information or a specific field\".\n\nThe IGNORANT ALWAYS \"reveal themselves in their own vernacular\".......smile\n\nGod help us all.\n*******************"
    author: "ben"
  - subject: "Re: Irregardless"
    date: 2003-12-06
    body: "Irregardless isn't a word. It is as much a word as ain't. Even if irregardless was a word most people would still be using it incorrectly. Irregardless would mean with regard however people use it in place of regardless which is pretty dumb."
    author: "Joe"
  - subject: "Re: Irregardless"
    date: 2004-03-24
    body: " a useful article on this subject may be found at \n\nhttp://www.quinion.com/words/qa/qa-irr1.htm\n\nWhat I would like to bring to peoples' attention is English is full of words with double negatives that have been used as intensifiers and not to cancel each other out.  \n\nAlthough I must concede the fact that irregardless is not YET a word which is accepted as part of the English language; it may well be soon enough. \n"
    author: "Tim"
  - subject: "Re: Irregardless"
    date: 2004-03-30
    body: "It is extremely frustrating to see people debate something that has already been defined by greater minds than those that are doing the debating.\n\nIn the future perhaps we should look in a dictionary before we say a word does not exist. Just because it is not in the one that we have on hand, does not mean it does not exist, or that it has not been recognized as a word.\n\n Found in Merriam-Webster online.\n\nOne entry found for irregardless.\n \n\nMain Entry: ir\u00b7re\u00b7gard\u00b7less \nPronunciation: \"ir-i-'g\u00e4rd-l&s\nFunction: adverb\nEtymology: probably blend of irrespective and regardless\nnonstandard : REGARDLESS\nusage Irregardless originated in dialectal American speech in the early 20th century. Its fairly widespread use in speech called it to the attention of usage commentators as early as 1927. The most frequently repeated remark about it is that \"there is no such word.\" There is such a word, however. It is still used primarily in speech, although it can be found from time to time in edited prose. Its reputation has not risen over the years, and it is still a long way from general acceptance. Use regardless instead.\n\n Find something else to do. \n"
    author: "ASH"
  - subject: "Re: Irregardless"
    date: 2005-06-04
    body: "The American Heritage\u00ae Book of English Usage. \nA Practical and Authoritative Guide to Contemporary English.  1996.\n\n \n3. Word Choice: New Uses, Common Confusion, and Constraints\n\n \n\u00a7 184. irregardless \nIrregardless is a word that many people mistakenly believe to be correct usage in formal style, when in fact it is used chiefly in nonstandard speech or casual writing. The word was coined in the United States in the early 20th century, probably from a blend of irrespective and regardless. Perhaps this is why some critics insist that there is \u0093no such word\u0094 as irregardless, a charge they would not think of leveling at a nonstandard word with a longer history, such as ain\u0092t. Since people use irregardless, it is undoubtedly a word. But it has never been accepted in Standard English and is usually changed by editors to regardless before getting into print. \n\n\n\nMy take on this subject is that I WILL NEVER USE IT.  If you are trying to appear to be a stupid person, this is one of the quickest ways to reach that goal.  \n\nIt is a not a standard English word...period.  Just because someone uses a word does not make it correct English.  Your proof here is actually doing more to prove the other side.  The last sentence of your statement shows that the dictionary advises to \"Use regardless instead\" which of course is the correct, Standard English word.  Everyone agrees it is a word, it's not a dirty sock.  But the question here is \"To use, or not to use\".  I believe the evidence clearly shows \"Not to use\". \n"
    author: "scissorfight"
  - subject: "Re: Irregardless"
    date: 2006-01-11
    body: "Irregardless, wizzle thugz tizzell me it isn'ta word, thugz often git tha word wrizzle. Stuped thugz try'n ta look important use dis word. Itz a word for shizzle, but its b-to-da-izzest not ta use this shizzay, if yo wanna b intelligizzle.\nFor reazzle."
    author: "Snoop Dogg"
  - subject: "Re: Irregardless"
    date: 2006-01-11
    body: "Irregardless, wizzle thugz tizzell me it isn'ta word, thugz often git tha word wrizzle. Stuped thugz try'n ta look important use dis word. Itz a word for shizzle, but its b-to-da-izzest not ta use this shizzay, if yo wanna b intelligizzle.\nFor reazzle."
    author: "Snoop Dogg"
  - subject: "Re: Irregardless"
    date: 2005-09-15
    body: "hmm... Computer... \n\nLet's consider someone from 1753 and ask them what the word computer means.\n\nWords are created, irregardless of what is correct, logical or even useful.\n\nTo me, irregardless adds emphasis to regardless.  A double negative in logic does in fact cancel into a positive.  However, this is language, not logic.  Logic has no meaning, it is abstract.  Language has meaning and is not the same type of abstraction.  One may refer to Latin roots to diminish this argument, but consider that \n\nInvestigate the French interest within the contents of dictionaries...  \n\nConsider this oxymoron (IMHO):\n  Military Intelligence\nDo the words means something different alone than when placed together?\n\nWell I suspect those that acknowledge the word irregardless exists and is used for \"humor\" derive this position from considering the existence of oxymorons. \n\nIn the context of an oxymoron, albeit irregardless is a single word, one can see how meaning is relevant....\n\nAt any rate... that's my opinion... (not justified as well as I would like)"
    author: "Josh"
  - subject: "Re: Irregardless"
    date: 2006-06-07
    body: "\"Ain't\" ain't a word either. If you're going to use apostrophes at all, why not use them as required? You shouldn't feel compelled to limit yourself to one. If that's the case, don't use them at all - \"aint\" is much better. You could try \"ai'n't\", but the \"a\" doesn't belong there...so why make such a futile effort? Instead, write \"isn't\". If you wish to put \"aint\" in prose, only do so when illustrating a person's speech habits (this will not reflect well on stated person). It seems that a common rebuttal to the claim \"irregardless is not a word\" goes something like: \"it's in the dictionary - look it up\". Expletives are in the dictionary too, but we don't commonly argue for our freedom to use them whenever and wherever we want. In the end, it is not about what is or is not a word, but which words you should avoid so as not to sound like a moron. Besides, who on earth is Webster? And who is he to tell me when something is widespread enough to be considered a word? If I think it sounds ridiculous, I will not use it. If you use it, I might laugh a little. Incidentally, \"burglarize\" is not a word in my vocabulary either. It's \"burgle\", back-formation or not.\n\nP.S. anyone wishing to critique my use of punctuation marks may go ahead - I don't care."
    author: "Jon"
  - subject: "Re: Irregardless"
    date: 2006-06-11
    body: "Ain't was a word.  It was a contraction for am not.  \"I ain't going to stop using the word ain't in spoken language.\"  At one time would have been correct usage.  \"'Ain't ain't a word.\"  Would never have been considered correct because ain't was never appropriately associated with \u0091is not\u0092 because the word isn't already, more appropriately, fills that usage.\n\nBack to point on the word Irregardless.  Is it a word?\n\"A unit of language that native speakers can identify; \u0093\n~ wordnet.princeton.edu/perl/webwn\n\nApparently it is a word by the very definition of a word.\n\nWhat is its meaning?  Being that it is a double negative then no meaning could ever truly be agreed upon and thus it is non standard and should NEVER be used in writing unless it is to obtain a humorous absurd effect.  Why bastardize our own language to a point where the intelligent and the ignorant can not effectively communicate?"
    author: "Festerius"
  - subject: "Re: Irregardless"
    date: 2009-01-09
    body: "I've got no recogknowlwdge of what irregardless means.  Must be time to unthaw my brain."
    author: "Anonymous"
  - subject: "Re: KDE the only desktop"
    date: 2003-04-30
    body: "To funny. God bless grammer.\n\nOn the lighter side. Hours pass by as I try to compile Gnome1.4 and Gnome2.0. My LFS box is working overtime trying to place a hand on the whole idea of trying to   \n make Both versions of G work together. On one hand I can opt for a straight Gnome2.0 with no extra Gnome packages. Just a pretty window manager. Or waste time and hard drive space by adding both versions of Gnome. I roll with option two. In the end I find out that my two window mangers dont play nice together; yet they are Gnome. I feel like a true cobler. Putting many little applications together to make some type of functional window manager with some bells and whistles. Integration whats that? Gnome skipped out on that word. Then kde3.1.1 came to mind. My modem fired up and grabbed the packages. The compiler went to town. Then when kde was all said and done the first login was attempted. A warm glow opened on my lcd. I thought for a second I could hear angles singing in the background. A tear trickled down my eye. For there in  Konqueror was a simple menu link saying send to a friend. "
    author: "Aaron"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "my apologizes then."
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "A real troll would never apologize, so you are now officially an ex-troll. Congratulations :-)"
    author: "Carbon"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "> you are a pissed off gnome user right ? pissed\n> because someone care to say the plain truth.\n\nLook, at first I take you serious. But these sentences make you really look like a troll...\n\nOK, you think that it's the truth that GNOME sucks. I disagree, and unless everybody agrees that GNOME sucks, it simply is NOT the truth.\nEven if it's the truth, it's not polite to go all out and spreading \"GNOME sucks\" messages everywhere."
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "http://dot.kde.org/1017860068/1017881855/1017968434/1017972000/1018026571/1018036569/1018037799/1018038351/\n\ngo and read this"
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "What's your point?"
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "\"Aged Person\", don't you ever give up trolling? Your old \"I used to be a GNOME contributor and have now switched to KDE\" troll is getting really tired now. What's the matter? Now that Gnotices won't print your crap anymore, you have to troll on KDE.news and Slashdot?"
    author: "Talua"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "hehe, did you see me posting things like 'kde suck' or something ? i compared gnome vs. kde and find myself saying that kde is more usable to me and that i am thankfull to those who made it. by the way you must be a lucky person to get rid of me from your gnome section. oh and not to forget. i never made any trolls of gnome iirc i was only fed up about the way it went (full controll through sun/redhat/ximian) totally commercialism, totally capitalism etc. and i am fed up about the shit people shouting out when saying 'gnome wants to be this, gnome wants to be that, gnome will be this and that' and nothing of it came true. KDE is GNOME done right and better. eat this."
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "Come on, no one believes you were ever a GNOME developer. You have a long history of trolling, under many different aliases, writing different fictitious stories about yourself and GNOME.\nCertainly, the time and effort you devote to trolling is impressive, but it would be better spent contributing to KDE in some way, rather than attacking another free-software project."
    author: "Talua"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "excuse me. its people like you that make other people look like trolls because of captious argumentations. gnome people are really pettish when it comes into these things. specially if there are people. maybe people like me that have more or less problems in explaining things correctly in a clean fine oxford english etc. i mean i was long enough with you guys to know what kind of evil assholes you people could be in certain situations so dont dump your shit on me. you are specialists in turning things, wiping other peoples eyes, itching on things that are none. etc.\n\nnow all i pointed out here is how much i like kde over gnome and i think it is nothing bad. i am speaking here from my personal opinions, my personal sight of things etc. somehow i must have done something negative to you otherwise you wont react like this now.\n\na last one, your 2 sentences above dont tell me much, specially 'long history of trolling under many different aliases' what do you mean by this ? where is the troll ? can you prove anything or are you only up to make one out of it ? but hey, if i was really that asshole you want to make me look like then in your situation i would shut the mouth and be happy to get rid of that person. instead picking the needle over and over again into that person."
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "Largo, don't waste your breath debating with a troll.\n"
    author: "ac"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "Largo/Aged Person/Whatever, your language is unique and unmistakable. Changing aliases can't disguise that.\nRemember this message from 8 months ago(one of many):\nhttp://news.gnome.org/gnome-news/998345737/998408528/index_html\nYou said you were \"a developer for a bunch of gnome projects\" and had decided to stop using GNOME. Apparently you must have taken it up again, so that you could again quit because of GNOME 2!\n"
    author: "Talua"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "Quit trolling yourself?\n\nHow do you know he is the same guy? Largo's grammer actually seems worse than this Aged Person guy.\n\nAnyways, imho, both desktop environments are great. :)\nI used kde 1.x, then gnome 1.2, then kde 2.1, then kde 2.2 (didn't care for gnome 1.4). Right now I'm using kde 3.0, but I'll definatly look at gnome2 when it's released.  I urge all trolls (Talua, aged person), and supposed trolls (largo), to do the same :)."
    author: "fault"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "I am not a troll; I am an anti-troll. :)"
    author: "Talua"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "Will you please stop that nonsense? What you say is impolite and very insulting to those who use GNOME (or both GNOME and KDE) and to GNOME developers.\nOK, so you don't like GNOME, but that's no reason to act like that.\nDon't say trollish things such as \"KDE is GNOME done right and better\", say \"I prefer to use KDE\" or something.\nOr better: don't mention anything about GNOME at all and just say how wonderful KDE is."
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "hey! it's still a free country. (at least where i am)\nI u don't like what he's saying, don't read his posts.\n\n<rantmode>\nThis is a kde site. expect to read some GNOME Sucks posts. (cause it does)\nif u don't like that, go hangout at gnotices.\n</rantmode>"
    author: "me"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "Yes, but even in a free country one does not have the right to insult others.\nAnd no, I don't expect to read some GNOME Sucks posts. Is this how you KDE guys want to behave?\nI never wanted to believe that there's a war between GNOME and KDE, I've always thought that the \"Gnome Sucks\"-people are childish trolls. But people like you really start to make me think that KDE's reason to exist is to destroy GNOME.\n\nWhat's your purpose? To tell everybody how GNOME sucks or to create a great desktop environment for Unix?"
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "now show us who the real trolls are - troll!\n\nby the way: i reviewed all my 'Largo' posts here on dot.kde.org and i was not able to find one line that said 'gnome sucks' written by me. so please shut up and stop turning things and words. you grab things out of nowhere and that is not fair. not fair for you gnome people and not fair for kde people and of course not fair for certain individuals. please stay at the facts."
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "You called it \"shit\". That's pretty clear."
    author: "Bixby"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "yes, i did say 'shit' but i never said 'gnome sucks'"
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "Oh, I stand corrected. LOL!"
    author: "Bixby"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "You got something wrong.  GNOME's reason to exist is to destroy KDE.\n\nThis is why GNOME is experiencing such a backslash today and is dying.\n\nTrolls like you keep it alive a little longer.  Why don't you go post happy happy messages on Gnotices instead of annoying everyone here?\n"
    author: "ac"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "Yeah sure, pass the blame to me.\n\nDo you want to know the fact? Subscribe to the GNOME mailing lists and ask if they're developing GNOME in order to kill KDE.\nOr subscribe to the KDE mailing lists and ask if their purpose is to kill GNOME.\n\nThe annoying ones here are you.\nKDE IS NOT MADE TO KILL GNOME AND NEITHER IS GNOME'S PURPOSE TO KILL KDE! *PERIOD*!"
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "ehm?\n\ndo you have paranoia or something? no one said that gnome was meant to kill kde. dude please go out for a walk or for a fresh fuck or something. i think sitting 24hrs/7day behind the computer makes you act like moron."
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "> no one said that gnome was meant to kill kde\n\nLet me quote ac:\n\"You got something wrong. GNOME's reason to exist is to destroy KDE.\""
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "excuse me, this is more or less a sick joke isnt it ?\n\ni mean gnome has no real framework, no real usability. basically nothing that could touch 5% of the usability kde 3 offers today. gnome is more or less a loose bunch of thrown together applications. oki i must say evolution, galeon are nice applications but they are basically STANDALONE apps. no real interprocess communication. they dont share anything.\n\nlook at kde for example. you add a bookmark to kbookmarks and it shows up in konqueror, the panel, the top panel. you enter a address in kaddress and it shows up in kmail, in your palm syncing tool and wherever. you start konqueror and _USE_ it that is you start it and really get the feeling it depends to kde.\n\nnow the problem for gnome 2 is that it will be named 'gnome 2 desktop' and not 'gnome 2 desktop environment' because it lacks of many applications. the enduser gets nothing much with gnome 2 therefore many people will stay on gnome 1 until the lack of STRONGLY needed applications are solved.\n\nthese applications are a webbrowser and a mailingclient. look the big problem that marco, yanetti, john etc. have with galeon now. they are not able to get things ported. of course they started a new branch in CVS and ported the ui to gnome 2 but the mozilla rendering engine is not yet ported to gtk2+ and the guy (blizzard) who is working on it recently got new orders from his employer to stop working on that. so basically galeon is in deep freeze in doesnt gain progress.\n\nevolution for example. it is definately a nice pim, easy to use, worth to install. but exacuse me. 90% of the times a normal user only wants to write emails so why does the user need to start 'schedule, addressbook, todolist etc' with it all the time. now evolution 1.0.3 is out and i heard from lewing, ettore, fejj (the three guys i recall working on it) that evolution wont be ported to gnome 2 until version 1.4 (and you are at 1.1 in cvs head right now)\n\nnow lets eye over to nautilus (the big eazel project) it still sucks, oki definately in gnome 2 nautilus 1.1.x is fucking fast but its as fucking unusable as it was before. i mean you start it. you see the icons popping up inside its window and you ask yoursel 'and now ?' not to mention the communication between nautilus and the current gnome framework really sucks. e.g. you cant set any backgrounds etc.\n\ni dont say that gnome generally sucks or something and i dont say that the programmers did a bad job. but why should i wait for gnome becomming at least partially as usable as kde 3 is today. i mean having a gnome desktop installed and waiting for it to get usable is a waste of time. people want to use a desktop TODAY and not 'in one day' so basically there is nothing elese and nothing that advanced as kde 3 is.\n\nwell kde 3 has everything i need. a browser, filemanager, mailer, cool framework. what do i want more ?"
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "> excuse me, this is more or less a sick joke isnt it ?\n\nNo it isn't.\n\n\n> i mean gnome has no real framework, no real usability.\n\nI don't know in what way you're thinking, but I can't possibly imagine how you could have meant that when you say \"GNOME's reason to exist is to destroy KDE.\"\n\n\nOK, so KDE is better than GNOME in some ways. In your opinion KDE is 95% better than GNOME because you think GNOME only offers 5% of what KDE has. OK, then that's your opinion.\nBut I stil fail to understand why you mention GNOME at all. What does KDE 3, being a great desktop environment, has to do with GNOME?"
    author: "Stof"
  - subject: "\"gnome exists to destroy kde\""
    date: 2002-04-06
    body: "I said that, not Largo.\n\nIf you know nothing about GNOME history then maybe you should read up on it instead of entertaining us with your uninformed opinions and \"hurt\" feelings instead of facing the truth.\n\nYou are the same dc that has been trolling this site before?\n"
    author: "ac"
  - subject: "Re: \"gnome exists to destroy kde\""
    date: 2002-04-07
    body: "OK, GNOME originally started as an alternative for KDE, because KDE was linked to QT, which wasn't GPL'ed. And you are saying that, because of the reason why GNOME started, GNOME's only pupose is to destroy KDE.\nBut that is simply not true anymore. Today, GNOME's reason isn't to provide an alternative to KDE, or to kill KDE or whatever you call it.\n\nIf GNOME's only purpose is to kill KDE, then it will suddenly disappear once KDE is gone. Don't you think that's a little unlikely?"
    author: "Stof"
  - subject: "Re: \"gnome exists to destroy kde\""
    date: 2002-04-07
    body: "GNOME is already gone, otherwise you wouldn't be here, you would be with GNOME."
    author: "ac"
  - subject: "Re: \"gnome exists to destroy kde\""
    date: 2002-04-07
    body: "So one must be using KDE just to be able to view KDE site?\n\nI'm posting this using Galeon 1.2.0 in GNOME 1.4.0 in case you don't know.\nAnd I'm running Licq (with KDE support) inside GNOME."
    author: "Stof"
  - subject: "Re: \"gnome exists to destroy kde\""
    date: 2002-04-07
    body: "If you are so happy with GNOME why do you troll a KDE site?\n"
    author: "ac"
  - subject: "Re: \"gnome exists to destroy kde\""
    date: 2002-04-07
    body: "I'm not trolling, I am asking Largo to stop trolling about GNOME.\n\nAnd I am happy about both Gnome AND kde."
    author: "Stof"
  - subject: "Re: \"gnome exists to destroy kde\""
    date: 2002-04-07
    body: "He made himself clear that he wasn't a troll, even though some of his comments sounded trollish. Everyone is entitled to voice their opinion; trolls typically voice their opinion to annoy people.\n\nThanks, drive through."
    author: "fault"
  - subject: "Re: \"gnome exists to destroy kde\""
    date: 2002-04-07
    body: "Oh yeah, he sure wasn't annoying, was he..."
    author: "Stof"
  - subject: "Re: \"gnome exists to destroy kde\""
    date: 2002-04-10
    body: "GTK is obsolete, QT is light years ahead of GTK."
    author: "andre"
  - subject: "Good Stuff, Largo"
    date: 2002-04-06
    body: "Well, I guess some good has come from this discussion afterall.\n\nThese are many interesting details of GNOME development.  It is often impossible to GNOME development news, REAL NEWS I mean not stupid publicity announcements.\n\nFor a long time I have been trying to follow GNOME development lists but ZERO real development seems to happen there.  Suddenly from your one post I konw a lot more.\n\nThanks for the info."
    author: "ac"
  - subject: "gnome is worse"
    date: 2005-11-11
    body: "gnome is worse than kde - it looks worse, and is not as easy to use and is slower. yet all linux distros should have a choice of gnome or kde"
    author: "fredintor"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "ok... i'll say it S L O W L Y\nP E O P L E  S T A T E  T H E R E  O P I N I O N S\nif u don't like it! ignore it/don't read it/close u'r browser/press the power button or whatever.\n\ni just can't see why u think saying 'GNOME sucks' is an insult.\nif it sucks (in there opinion) someone got to say it. surely u don't want the gnomes to think everything is ok with there desktop  do u?\n\nKDE Rules!!!!!\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nGNOME sucks :)"
    author: "me"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "hehe"
    author: "Spamo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "I don't have a problem with people criticising GNOME or saying it sucks (after all, this is a KDE board :) ) \n\nWhat I do have a problem with, is if someone misrepresents themselves. If they say that they are a GNOME developer, and they are not, or if they say they are a GNOME user that has just switched to KDE, and they have never been, then that makes them a troll."
    author: "Talua"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "look you whining brat. i did contribute a lot of code to certain gnome projects, either way its fixing shit etc. i think this makes me a GNOME DEVELOPER since i developed for gnome. that is if someone sits at home and wastes his time for you fucking unthankfull assholes. i for sure didnt contribute that much to name myself a GNOME _CORE_ DEVELOPER. but i never said this. so why dont you just fuck off. you dont know me and i dont know you so shut up."
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "All the changelogs for GNOME projects can be browsed in cvs.gnome.org/lxr/source.\n\nPlease tell me which project you contributed to.\n"
    author: "Carg"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "ah, i must admit you trapped me with this. but the main reason for you to ask for this is not 'what stuff i contributed to gnome' the main reason is to figure out who i really am, so you can make more sick jokes out of it. i am not willing to run into this trap no matter if i look like a fool now or not but belive me when it becomes hard then you will look like a fool. if you figure out my name one day then you can look up the changelogs ;-)\n\nso far anal carlsson, i wish you good luck to figure out. hey whats up with nautilus are the people able to dive into symlinked directories now or copy files/directories off of ftp servers ?"
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-07
    body: "You are that \"aged person\" allright. Just like him, you refuge to tell us who you are, yet you still claim that you are a GNOME developer...\nSorry, that has disqualified yourself. Say whatever you want, I won't believe any of your nonsense anymore.\n\nYes, go ahead, insult me. I know you will, just like you insult everybody else, who is trying to discuss things, for no good reason. I could care less."
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "OK, you win.\nBut I have to say that I am VERY disappointed in you KDE people's attitude..."
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "What? That's actually \"Aged Person\"? That explains everything. I can remember him very well. Thanks to people like him, Gnotices is now force to moderate comments."
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-05
    body: "yeah but this is not gnotices and that i did't wrote anthing wrong so why don't you just fuck off. no wait. please continue to reply and show everyone what kind of sick weirdo you are. i am largo and i only said how much i like kde over gnome and i am talking from my personal opinion here. if you can't stand these things then go back.\n\nbesides. the reason why gnotices went moderate is because they couldn't stand the true comments from all the people saying how much nautilus sucked. they simply can't stand negative comments so they filter the crap and today we get only 'nice' 'wonderfull' 'true' 'thankfull' comments on gnotices.\n\nnow you all come here and continue riding. my apologizes again if i hurt someone or itched his back with something. but if you all HATE me that much then why don't you simply accept that i left gnome and went to some serious desktop environment. i mean there is nothing better that could happen to you than getting rid of a troll ?. but now that you all continue the same shit HERE on dot.kde as you previously did on gnome then we all SEE who the real tolls are.\n\nthats my sentence for it. take it as is and i don't care if you give a fuck."
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "*sight*\nIt's you allright. Pretending that you're a reasonable person but in the meantime saying all kinds of insults things...\nIs it really that hard to just not to mention GNOME? That is all! Is it really that hard to only say \"KDE is great\" and nothing else?"
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "i dont know what you are up to. as written in a reply above. go out and get some friends. maybe some girls find you attractive who knows. get a fuck and calm down."
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "Will you just answer me instead of telling me your useless insults?"
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "dont you think its becomming boring now ? i mean how long do you think you want to continue this cat and mouse game ?"
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "OK, have it your way.\nBut let me tell you that at Gnotices, unlike here, people don't say negative things about the other desktop project just because they like their own. And I believe that anybody who says bad things about the competition just to show how great his own desktop environment is, is a real jerk."
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "> But let me tell you that at Gnotices, unlike here, people don't say negative things about the other\n> desktop project just because they like their own.\n\nyeah on gnotices they started to filter things thats why there are NO (and i mean no single) negative things. thats why everyone pulls their frustration on /. whenever a gnome announcement comes out."
    author: "Largo"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "So Gnotices should accept comments like \"GNOME sucks ass\" or \"Ximian is GAY\" ?"
    author: "Stof"
  - subject: "Re: KDE the only desktop"
    date: 2002-04-06
    body: "you like that right ?"
    author: "Largo"
  - subject: "Gnotices comments"
    date: 2002-04-06
    body: "GNOME is a closed community, why should it have an open board?"
    author: "ac"
  - subject: "Re: Gnotices comments"
    date: 2002-04-07
    body: "Because GNOME *should* be an open community, and an open board is the first step to achieve that?"
    author: "Stof"
  - subject: "Folder animations and filemanager tooltips"
    date: 2002-04-04
    body: "Just installed KDE 3 for the first time: Very nice job! Two questions though: Is it possible to turn off folder animation in the file manager (these jumping folders on mouse-over) and can you change the delay before file tooltips are displayed?\n\nFile tooltips seem to be pretty useful, but they can be annoying if they pop up too fast (and they do for me...) Maybe I am too slow...\n\nAgain: Way to go!\n"
    author: "Oliver Strutynski"
  - subject: "Re: Folder animations and filemanager tooltips"
    date: 2002-04-04
    body: "You can turn the animation off in kcontrol -> LooknFeel->Symbols\n-> Advanced -> Symbol Animation.\n\nYou can turn the tool tips off in kcontrol->LoooknFeel->\nStyle->Miscellanious->Activate Tooltips.\n\nI think the delay of either is not configurable by GUI."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Folder animations and filemanager tooltips"
    date: 2002-04-04
    body: "and to be complete \nyou can turn off 'File tooltips' in\nkontrol-> File Browsing-> File Manager-> Show file tips\n\nregards"
    author: "thierry"
  - subject: "THANX !!!!!!"
    date: 2002-04-04
    body: "just that !"
    author: "thanx"
  - subject: "thanks"
    date: 2002-04-04
    body: "ein dickesdankesch\u00f6n f\u00fcr die geleistete arbeit "
    author: "harald"
  - subject: "Very fine but Konqueror still crashes!"
    date: 2002-04-04
    body: "KDE3 really is a wonderful desktop ... but\nI tried one or two of the things which were not running correctly in the release canditate, i.e. read a pdf file in konqueror and then print it (this has NEVER worked in KDE1, KDE2 nor any of the beta KDE3's. Pleasant surprise: it worked.\nBut ... the release candidate did not produce a pdf file from a html page. So access the site (my favourite)http://news.bbc.co.uk, and try to create a pdf file - crash :-(\n\nRestart konquror and create the pdf file from the same html page ... OK."
    author: "Malcolm Agnew"
  - subject: "Re: Very fine but Konqueror still crashes!"
    date: 2002-04-04
    body: "now honestly.. i am a new kde user and found the help->report a bug button within some minutes after going through the menues, dont you think its better posting bugreports on bugs.kde.org than posting them here where no one really cares. or are you one of these people that want to demonstrate something to the public ? i mean its sad that some people are such fools that have nothing better to do than reporting bugs on places where they shouldnt belong."
    author: "dr. schiwago"
  - subject: "Re: Very fine but Konqueror still crashes!"
    date: 2002-04-04
    body: "\"i (sic) mean its (sic) sad that some people are such fools that have nothing better to do than reporting bugs on places where they shouldnt (sic) belong\"\n\nI was taught at school that a sentance begins with a capital letter and that an occasional apostrophe is sometimes required in the English language. But more important I was taught not to lightly call strangers \"fools\" at the drop of a hat. Perhaps it may surprise you, but I do know how to report a bug in the official way - I have often done this.\n\nThere is nothing (at least in the area of computer technology) that would please me more than that KDE/LINUX succeed in destroying the Microsoft monopoly - not just because it's free or open but because it's better.\n\nThe reason I reported this bug here (and Konqueror has crashed 3 more times on me this morning), is  that I have the sneaking fealing that fellow aficionados of KDE/LINUX often do not criticise enough on the principal, perhaps, of \"don't rock the boat\".\n"
    author: "Malcolm Agnew"
  - subject: "Re: Very fine but Konqueror still crashes!"
    date: 2002-04-04
    body: ">I was taught not to lightly call strangers \"fools\"\n\nShame you weren't taught about split infinitives ;-)\n\nIf you are going to criticise someone else's grammar, it pays to get your own correct."
    author: "Anon"
  - subject: "Re: Very fine but Konqueror still crashes!"
    date: 2002-04-04
    body: "Ouch! Old fool me :-)\n\nKDE3 really is beautiful - but kooka crashs too."
    author: "Malcolm Agnew"
  - subject: "Re: Very fine but Konqueror still crashes!"
    date: 2002-04-04
    body: "The issue of split infinitives is not clear-cut. Grammarians are divided over it. The two errors he pointed out, however, are completely uncontroversial."
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Re: Very fine but Konqueror still crashes!"
    date: 2002-04-04
    body: "\"...school that a sentance begins...\"\n\nI think you mean \"sentence\".... and I stopped reading then.\n\nGlass houses and throwing stones comes to mind."
    author: "Corba the Geek"
  - subject: "Re: Very fine but Konqueror still crashes!"
    date: 2002-04-04
    body: "I've generally got better response to bugs posted here than to bugs.kde.org, so I don't usually bother with the latter any more."
    author: "cbcbcb"
  - subject: "Jes :))"
    date: 2002-04-04
    body: "tre bonega novajxo :))"
    author: "Capit Igloo"
  - subject: "\u0091\u00e3\u00af\u00a5\u00e0! \u0084\u00ae\u00ab\u00a3\u00ae \u00a6\u00a4\u00a0\u00ab!"
    date: 2002-04-04
    body: "\u0082\u00a0\u00e3! \u009d\u00e2\u00ae \u00a2\u00ae\u00ae\u00a1\u00e9\u00a5 \u00e1\u00e3\u00af\u00a5\u00e0! \u0091\u00a0\u00ac\u00eb\u00a9 \u00aa\u00ab\u00a0\u00e1\u00e1\u00ad\u00eb\u00a9 \u00a4\u00a5\u00e1\u00aa\u00e2\u00ae\u00af! \u0091\u00aa\u00ae\u00e0\u00ae \u00a4\u00ae\u00ab\u00a6\u00ad\u00a0 \u00a2\u00eb\u00a9\u00e2\u00a8 \u00e8\u00a0\u00af\u00aa\u00a0 - \u00a2\u00ae\u00e2 \u00a8 \u00af\u00ae\u00e1\u00ac\u00ae\u00e2\u00e0\u00a8\u00ac, \u00e7\u00e2\u00ae \u00a7\u00a0 \u00e8\u00e2\u00e3\u00aa\u00a0 \u00e2\u00a0\u00aa\u00a0\u00ef!"
    author: "\u00e1\u00af\u00a5\u00ab\u00ab \u00e7\u00a5\u00aa\u00a5\u00e0"
  - subject: "KDE 3.1"
    date: 2002-04-04
    body: "Time to look forward to KDE 3.1, see what's planned: http://developer.kde.org/development-versions/kde-3.1-features.html"
    author: "Jan"
  - subject: "Re: KDE 3.1"
    date: 2002-04-05
    body: "\"Tabbed browsing support (David Faure & Doug Hanley)\"\n\nwoohoo! I can give up Mozilla for ever! :-D"
    author: "danrees"
  - subject: "Dcop issues :-("
    date: 2002-04-04
    body: "I was running rc3 - and it was great! Now I installed the final (SuSE 7.3 packages), after removing the rc3 from my box, and it won't start. throws me out with some dcop error:\n\n There was an error setting up inter-process communications for KDE. The message returned by the system was:\n\ncould not read network connection list.\n/root/.DCOPserver_boreas__0\n\nPlease check that the \"dcopserver\" program is running!\n\nNow, this box used to run rc3 just fine, and the same thing happend to a clean install of KDE3 over KDE2 on another box.\n\nany idea's?\n\ncheers,\n\nMartijn"
    author: "Martijn Dekkers"
  - subject: "Re: Dcop issues :-("
    date: 2002-04-04
    body: "The same thing happened to me with the suse 7.3 packages.\nHaven't really time to check now as I'm at work now but I'll look deeper into it tonight."
    author: "Thomas"
  - subject: "Re: Dcop issues :-("
    date: 2002-04-04
    body: "insert \n\n\"export KDEDIR=/opt/kde3\"\n\nin the /opt/kde3/bin/startkde script"
    author: "brat"
  - subject: "Re: Dcop issues :-("
    date: 2002-04-04
    body: "I had some dcop problems, here. The above solution fixed the problem !\nNo problems found during the last RC version, hopefully this is fixed in a more general way soon.\n\nPeter"
    author: "Peter"
  - subject: "Re: Dcop issues :-("
    date: 2002-04-04
    body: "Thanx for your hint. This solved the problem when i start kde as root. But it doesn't work for non-root users (something like dcop-connections rejected). I'm using Suse 7.3 with XFree86 4.2. Anyone has an idea?"
    author: "daniel"
  - subject: "Re: Dcop issues :-("
    date: 2002-04-05
    body: "Check the values of PATH, KDEDIR and KDEDIRS, maybe it gets set differently for non-root users somehow?\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Dcop issues :-("
    date: 2002-04-05
    body: "Hi Waldo\nThank you for your suggestion. I looked at the values of PATH, KDEDIR and KDEDIRS and couldn't see a difference. The only thing i noticed is that /opt/kde2/bin is in the path but not /opt/kde3/bin, the same for root and non-root.\n\nHere the detailed log of what happens when i start as non-root user (as said before, running as root goes well)\n\nXFree86 Version 4.2.0 / X Window System\n(protocol Version 11, revision 0, vendor release 6600)\nRelease Date: 23 January 2002\n\tIf the server is older than 6-12 months, or if your card is\n\tnewer than the above date, look for a newer version before\n\treporting problems.  (See http://www.XFree86.Org/)\nBuild Operating System: SuSE Linux [ELF] SuSE\nModule Loader present\nMarkers: (--) probed, (**) from config file, (==) default setting,\n         (++) from command line, (!!) notice, (II) informational,\n         (WW) warning, (EE) error, (NI) not implemented, (??) unknown.\n(==) Log file: \"/var/log/XFree86.0.log\", Time: Fri Apr  5 10:55:35 2002\n(==) Using config file: \"/etc/X11/XF86Config\"\nDCOPServer up and running.\nDCOP aborting call from 'anonymous-1841' to 'kded'\nERROR: KUniqueApplication: DCOP communication error!\nDCOP aborting call from 'anonymous-1845' to 'knotify'\nERROR: KUniqueApplication: DCOP communication error!\nkdeinit: Fatal IO error: client killed\nkdeinit: sending SIGHUP to children.\nKLauncher: Exiting on signal 1\n\n\nwaiting for X server to shut down \n\n\nkdeinit: sending SIGTERM to children.\nkdeinit: Exit.\n"
    author: "Daniel"
  - subject: "Re: Dcop issues :-("
    date: 2002-04-05
    body: "just checked - it can't be the PATH, added /opt/kde3/bin, removed /opt/kde2/bin and still get the same error runnung non-root. once it failed, a simple\n> su\nin the same directory does it: retyping\n> startx\nbrings up the beautiful KDE 3.0. so it seems to be some strange user-priv stuff, but i can't figure for which file/directory"
    author: "daniel"
  - subject: "Re: Dcop issues :-( -> Me too! =-("
    date: 2002-04-08
    body: "I'm having these problems as well. I had issues with upgrading to kde2.0 back in the day but a little fussing around got it working, but this time I can't figure it out. here's my setup:\ndistro: slack 8.0\ninstall method: packages\nwhere: /opt/kde\n\nactually, kde2.0 is in /opt/kde2\nkde3.0 is in /opt/kde3\n/opt/kde is a symlink to /opt/kde3. it used to be a symlink to /opt/kde2 before i did the install so all I really did was remove the symlink, install, move kde to kde3, and install the symlink. I've gone through the tmp folder and remove ANYTHING that looked like it might give me problems, including files starting with a . I also removed my .kde folder, and other .ICE and .DCOP files in my home directory. I've tried this as both root and a normal user and cannot get kdeinit to start up due to dcop issues. the weird thing is switching back to kde2.0 is painless (just change the symlink). when trying to get kde3.0 up I see teh splash screen, but dcop cannot be used and I get the same errors mentioned above. any ideas?:("
    author: "Will Stokes"
  - subject: "Re: Dcop issues :-("
    date: 2003-01-09
    body: "I've had the same problem after I compliled the 3.1rc6 binaries, KDE would work for root, and under VNC. After removing the user's .xftconfig file it started right up... It's something to try. Good luck"
    author: "Tony"
  - subject: "Re: Dcop issues :-( My Soloution -Fixed"
    date: 2003-02-23
    body: "Hi all I had this frustrating problem for quite some time, it involved iceauth and I keeped on getting an error message refering to sh: line1:  iceauth: command not found, or somthing similar.\n\nMy soloution was to find out which package iceauth came in, and install it. in this case it was simply xbase-clients\n\ngoodluck\naaron\n"
    author: "aaron"
  - subject: "Re: Dcop issues :-( My Soloution -Fixed"
    date: 2003-03-21
    body: "I also had an annoying problem with this. The 'iceauth' file is part of the XFree86 package which comes with Redhat. If the dcopserver program can't find iceauth, you need to first check that iceauth exists on your machine (in /usr/X11R6/bin/ normally) then set your search PATH to include this directory so dcopserver knows where to look (ie. export PATH=/usr/X11R6/bin/). That worked for me. Good Luck....\nBruce"
    author: "Bruce"
  - subject: "Re: Dcop issues :-( My Soloution -Fixed"
    date: 2007-02-09
    body: "I got the same errors, I found out it was because my /home partition was FULL.\n\n"
    author: "jonny"
  - subject: "Re: Dcop issues :-("
    date: 2002-04-04
    body: "I had this exact problem before with Redhat when updating to 2.2.x.\nI solved this problem buy download \"ALL PACKAGES\" including QT3.0.x as \nwell as other packages, not just KDE packages. The problem solved. \n:)"
    author: "Tom"
  - subject: "Re: Dcop issues :-("
    date: 2002-04-05
    body: "I had that problem for all users (root and non-root). I have tried all the suggestions which were posted here but non worked. The only solution I have found was to sym-link /opt/kde3 to /opt/kde2. This was needed despite that only /opt/kde3/libs was listed in /etc/ld.so.conf and only /opt/kde/bin was addedd to the $PATH in /etc/profile. But hey, it works so I'm happy ;-)"
    author: "JoeFromTheBush"
  - subject: "Re: Dcop issues :-("
    date: 2002-04-05
    body: "I had that problem for all users (root and non-root). I have tried all the suggestions which were posted here but non worked. The only solution I have found was to sym-link /opt/kde3 to /opt/kde2. This was needed despite that only /opt/kde3/libs was listed in /etc/ld.so.conf and only /opt/kde/bin was addedd to the $PATH in /etc/profile. But hey, it works so I'm happy ;-)"
    author: "JoeFromTheBush"
  - subject: "Re: Dcop issues :-("
    date: 2003-02-23
    body: "Try installing xbase-client\n\nif it isn't already, that was my problem."
    author: "aaron"
  - subject: "Re: Dcop issues :-("
    date: 2002-04-30
    body: "I have this same error with SuSE 8.0 clean install. Sort of frusterating with a clean install! \n\nTrying the suggestions above..."
    author: "Mattfago"
  - subject: "Re: Dcop issues :-("
    date: 2002-06-17
    body: "Hey... guys.. you need to install lib_fam. Not hard.. do a google search for libfam site:oss.gsi.com and you'll come up with it... \n\nuntar it, cd to dir, ./configure, make, make install and vuala. \n\n\n\nhope it helps. "
    author: "feeshbar"
  - subject: "Re: Dcop issues :-("
    date: 2002-06-17
    body: "Hey... guys.. you need to install lib_fam. Not hard.. do a google search for libfam site:oss.gsi.com and you'll come up with it... \n\nuntar it, cd to dir, ./configure, make, make install and vuala. \n\n\n\nhope it helps. "
    author: "feeshbar"
  - subject: "Re: Dcop issues :-("
    date: 2002-11-19
    body: "A google search doesn't find the oss site you specified.  Is there somewhere else?"
    author: "David Reinhart"
  - subject: "Dcop issues :-("
    date: 2002-04-04
    body: "I was running rc3 - and it was great! Now I installed the final (SuSE 7.3 packages), after removing the rc3 from my box, and it won't start. throws me out with some dcop error:\n\n There was an error setting up inter-process communications for KDE. The message returned by the system was:\n\ncould not read network connection list.\n/root/.DCOPserver_boreas__0\n\nPlease check that the \"dcopserver\" program is running!\n\nNow, this box used to run rc3 just fine, and the same thing happend to a clean install of KDE3 over KDE2 on another box.\n\nany idea's?\n\ncheers,\n\nMartijn"
    author: "Martijn Dekkers"
  - subject: "Re: Dcop issues :-("
    date: 2002-04-04
    body: "I am having the exact same problems. \n\nUnfortunately, the error message doen't indicate where to look for the problem. I have change the libICE.so.6 but that didn't fix it. \n\nAny help would be very much appreciated.\nThanks"
    author: "JoeFromTheBush"
  - subject: "Re: Dcop issues :-( - sorted"
    date: 2002-04-04
    body: "On the KDE mailing list (lists.kde.org) there is a mail:\n\nin /opt/kde3/bin/startkde change:\n\nunset KDEDIRS to unset KDEDIR\n\nworks for me :-)"
    author: "Martijn Dekkers"
  - subject: "Re: Dcop issues :-( - sorted"
    date: 2002-04-05
    body: "Over on usenet we're seeing the same problem - unset KDEDIR didn't work for me, and I've tried manually setting KDEDIR KDEDIRS and PATH correctly, and it still won't run.  I had RC3 installed, and did a -Uvh.\n\nAny ideas?  (I'd poke around more, but I have business deadlines, and right now I need to work, not play with a desktop.  Above anything else, KDE is a functional environment for me.  :)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Dcop issues :-("
    date: 2002-07-10
    body: "I have exactly the same problem with KDE 3.0.1 on SPARC Solaris 9 box, none of the above solution works for me.\n\nBut, when I log into failsafe mode (one of Solaris' DT login mode to start basic X), then run startkde manually, the KDE starts!\n\nVery strange. Any idea?"
    author: "Yale Yu"
  - subject: "Re: Dcop issues :-("
    date: 2002-10-06
    body: "I have exactly the same problem with KDE 3.0.1 on SPARC Solaris 2.6 box\n, none of the above solution works for me.\n\nBut, when I log into failsafe mode (one of Solaris' DT login mode to start basic X), then run startkde manually, the KDE starts!"
    author: "Girisha"
  - subject: "Re: Dcop issues :-("
    date: 2002-11-10
    body: "I too ran into the DCOP message\n\n\"could not read connection list\"\n\nrunning KDE 2.1.2 on SuSE.  I had moved /tmp to a new partition and had forgotten to set the \"sticky bit\" on /tmp."
    author: "Clem Dickey"
  - subject: "Re: Dcop issues :-("
    date: 2002-11-10
    body: "(sigh) Although the sticky bit *was* missing, DCOP was probably more unhappy with the lack of \"go+w\" permissions on /tmp.  It's late. "
    author: "Clem Dickey"
  - subject: "Re: Dcop issues :-("
    date: 2003-01-30
    body: "The answer to your problem is a known issue with Solaris and the /tmp/.ICE-unix directory used by DCOP.  If you ran kde as root initially, /tmp/.ICE-unix is not writable by anyone else.  Do a chmod 777 on it before logging out.  Then it should work fine when you log back in as anyone else.  :)  \n\nHave found that logging into KDE as root at any time will reset and cause this problem again.  Write a script or avoid logging into KDE as root.  :)\n\nMike W."
    author: "Mike W"
  - subject: "Re: Dcop issues :-("
    date: 2003-06-28
    body: "Hi,\n\nI had exactly the same problem after I was moving with my linux \nto another location. I got a new UID (500), but kept the same user name.\n\nThats why KDE could not create files with my user name in /tmp, \nbecause they were already there, but with the old UID (503).\n\nEverything worked fine for me after I removed the the old files \nlike this (503 was my old UID):\n        find /tmp -uid 503 -exec rm -rf \"{}\" \\;\n\nGood luck.\nAbrax\n"
    author: "abrax"
  - subject: "Re: Dcop issues :-("
    date: 2003-07-09
    body: "I found the following issues that resolved the .DCOPserver_??__# problem.\nIn the directory of the user under .kde/ you'll find these files:\n~socket_servername\n~tmp_servername\n\nI deleted these two files and restart kde and everything began to work as it should.\n\n"
    author: "Dale Arey"
  - subject: "Re: Dcop issues :-("
    date: 2003-12-17
    body: "I had a similar problem after having to reboot a Mandrake 9.2 machine (KDE 3.1.3)  and I fixed it by renaming the dirs /tmp/ksocket-(username) and /tmp/kde-(username) to different names. Seems that perhaps this issue is sometimes caused by DCOPserver terminating in an ungraceful fashion. "
    author: "Michael Barrett"
  - subject: "Re: Dcop issues :-( or :-)"
    date: 2004-05-15
    body: "It worked perfectly, a new /tmp/ksocket-fernando was created and now everything runs fine. The problem aroused when I closed KDE with Crtl-Alt-Bksp what probably terminated the processes ungracefully."
    author: "Fernando Gerdt"
  - subject: "Re: Dcop issues :-("
    date: 2004-03-26
    body: "I'm using RH9.0. I found the same error\n\ncould not read network connection list.\n/home/my_user/.DCOPmyserver__0\n\nPlease check that the \"dcopserver\" program is running!\n\nafter a reboot!\n\nI followed instructions 'by Dale Arey on Wednesday 09/Jul/2003, @18:02 '\nabove, and that solved the problem.\n\nthanks a lot!"
    author: "M. C. Tripp"
  - subject: "Re: Dcop issues :-("
    date: 2004-05-06
    body: "I tried changing the names of the above mentioned /tmp/ and /$user/ files with no success.\n\nRealised eventually that what Mike W said 30 Jan 03 is true in my case. Checking permissions (ls -l -a) in $user/ showed that .ICEauthority was owned by root and belonged to group root, so as a user I had no say over it's rwx. Instead of changing the permissions so that every man and his dog can write to it, I changed the user and group (chown [username].[username] .ICEauthority). Now works fine!\n\nMandrake 10.0 Official\nintel PC"
    author: "Brad Milne"
  - subject: "Re: Dcop issues :-("
    date: 2004-11-26
    body: "i had the same problem and the problem was i had my /etc/sysconfig/network parameters badly configured "
    author: "polrus"
  - subject: "Re: Dcop issues :-("
    date: 2006-04-11
    body: "hi, I have encountered the similar problem when try to login KDE.\nBut have to clarify the point before explain the phenomenon,\nthat is: I have set the default shell as \"tcsh\" for my users and root.\ndefault shell in REDHAT-7.3 is \"bash\". \nin order to start kde, the only way for me is setting default shell back.\nthen problem is gone.\nactually, I am used to work with tcsh,not familiar with bash.\nFor the more depth reason, should be solved by linux-developer.\nhope help.\n"
    author: "temp-join"
  - subject: "Re: Dcop issues :-("
    date: 2006-04-19
    body: "I solved the problem by running fsck --rebuild-tree on my disk with my home directory..."
    author: "burner"
  - subject: "Re: Dcop issues :-("
    date: 2006-08-25
    body: "The problem is related to a mismatch between the PID number you'll see in the task-manager for dcopserver and the number you'll see in line 2 in both the /root/.DCOPserver_boreas__0 -files, and the two similar files in your and  maybe other users HOME-dir. These files can safely be deleted as dcopserver creates them at boot time if they doesn't exist, and thus fixes the problem   for you. - Delete the files and reboot! "
    author: "Svein Liby"
  - subject: "Leading"
    date: 2002-04-04
    body: "Nice article in every way, but I wonder how you know that it's the \"leading\" desktop. Hard to estimate, I would think. Otherwise, brilliant article and KDE 3 looks very good."
    author: "Joe"
  - subject: "Re: Leading"
    date: 2002-04-04
    body: "do you know of anything similar on linux/unix/bsd that has the capabilities of kde ? oh please dont come up with gnome now."
    author: "dr. schiwago"
  - subject: "Re: Leading"
    date: 2002-04-04
    body: "So in other words you're guessing that KDE is \"leading\". A guess is something different than a fact. I'm not saying KDE is bad, but for the sake of credibility I think you need to avoid statements you can't prove. Just a suggestion."
    author: "Joe"
  - subject: "Re: Leading"
    date: 2002-04-04
    body: "of course noone can say for sure what is the leading desktop for unix/linux etc., but most polls indicate kde is far ahead. Polls are obviously not reliable and vary depending on where/what site they are, but i haven't seen a poll in the last two years where gnome was ahead of kde.\n\nalso, i think kde got a lot more awards (from magazines and fairs) than gnome.\n\nYou might also want to look at the number of posts in the mailing lists. Might give you an impression where development is more active. To be honest, i never compared that cause i'm quite sure i know what i'd see ;)"
    author: "me"
  - subject: "Re: Leading"
    date: 2002-04-04
    body: "ummm... why not gnome, it works well for me and many others."
    author: "me"
  - subject: "Re: Leading"
    date: 2002-04-04
    body: "And Blackbox works for me, so maybe it's the \"leading desktop\".\n\nBut of course, Blackbox isn't the leading desktop, despite its sublime elegance. KDE is."
    author: "David Johnson"
  - subject: "Re: Leading"
    date: 2002-04-05
    body: "KDE is the default desktop environment on almost every major Linux distribution except for RedHat (which, ironically, is the most popular distro, but anyway).  I think this is good justification to say that KDE is the leading Linux desktop."
    author: "Justin"
  - subject: "RedHat and KDE"
    date: 2002-04-05
    body: "Ironically most of the time I hear about desktop deployments running Red Hat...  they use KDE.\n"
    author: "ac"
  - subject: "Conflict in RH packages"
    date: 2002-04-04
    body: "Is anyone else seeing this?\n\nXconfigurator < 4.9.42-1 conflicts with hwdata-0.9-1\n\nAre people ignoring and simply --force(ing) the issue?  The conflict is only the matter of one file: /usr/X11R6/lib/X11/Cards and I assume the format hasn't changed?  \n\nApart from that, I'd just like to thank all the KDE developers and packagers(Bero in particular for the RH packages) for the best desktop(I've been running CVS on my home machine, but don't have the time/resources to compile here).  "
    author: "Rob"
  - subject: "Re: Conflict in RH packages"
    date: 2002-04-04
    body: "I got the Xconfigurator-4.10.1-1 RPM from RawHide and it solved the problem. Try rpmfind.net or one of the RH mirrors\n"
    author: "Uri Shohet"
  - subject: "Re: Conflict in RH packages"
    date: 2002-04-04
    body: "Thanks, I was considering that, but I think I'll either use --force or uninstall Xconfigurator(I don't *really* need it).  I've been bitten before using Rawhide packages..."
    author: "Rob"
  - subject: "Re: Conflict in RH packages"
    date: 2002-04-05
    body: "looks great. only problem: i cant seem to get anti-aliasing to work, even after i configure it in the control panel->look and feel->fonts.this worked well in 2.2.2.\ni deleted the old kde packages on RH 7.2 ,as per redhat instructions, and had to do a --no-deps install since there was a dependency problem with Xconfigurator."
    author: "shiva"
  - subject: "Wow! This release rocks!"
    date: 2002-04-04
    body: "It is hard to express in words how much I like the new KDE3. I am running it since yesterday when there was that premature announcement on heise.de, and I am really, really impressed. From the little time I had to test it, it seems much more stable than the KDE2.0 release. In fact, it does not feel like a .0 release at all. \n\nThere are some minor rendering bugs (I will of course post them on bugs.kde.org), but nothing drastic such as konqueror crashing. The performance is really improved. I am using a 256 meg machine, so I can not really say how fast it is on low memory machines. But on my machine konqueror is now overall the fastest browser. It outperforms mozilla on windows and even sometimes IE on windows. Other applications also come up much faster than before.\n\nI also really enjoy the new kmail. It looks much better, and (most important for me) it finally supports SMTP AUTH, so I can remove that horrible SMTP after POP script from my mail server. The PGP/GPG integration also looks nice, but I did not have time to test it yet.\n\nThe new printing system, especially in conjuntion with CUPS, is a much needed addition. Printing was always some kind of weak spot for the UNIX office. It does not affect me much, since I do not see the point of putting information on paper anyway :-), but some people just love printing out documents and web pages and reading them on paper. I guess that is what they mean when they say paperless office...\n\nSo to all the KDE developers reading this: You guys are cool. Seeing a project like KDE growing gives me back some faith in humanity.\n\nregards,\n\n    A.H.\n\np.s. is there a way to donate to the KDE project so that the donation can be written off from the income tax (in germany)?"
    author: "Androgynous Howard"
  - subject: "Re: Wow! This release rocks!"
    date: 2002-04-04
    body: "You can support KDE e.v.\n\nhttp://www.kde.de/contact/support.php\n"
    author: "anonymous"
  - subject: "Re: Wow! This release rocks!"
    date: 2002-04-04
    body: "> p.s. is there a way to donate to the KDE project so that the donation can be > written off from the income tax (in germany)?\n\nYes, see http://www.kde.org/support.html\n"
    author: "Carsten Pfeiffer"
  - subject: "Installer"
    date: 2002-04-04
    body: "I look forward to trying kde3, but that probably won't be until i upgrade do a distribution upgrade because i've wasted enough time sorting out rpm dependencies.  In gnome i use red-carpet, and that makes installing/removing/upgrading software easy, so i have gnome2 snapshots running already.  What i really want to know is if there is any similar utility planned for kde? (even just a kde channel on red-carpet would do) - otherwise i, along with most other users (excluding debian), will never upgrade kde by hand so will always be using an old version."
    author: "dave"
  - subject: "Re: Installer"
    date: 2002-04-04
    body: "What the heck dist are you using?!\nLFS?!\n\nurpmi, Yast2 AND dpackage ALL have upgraded my KDE between releases of the dists and i have never had problems.\n\nBy far Mandrakes is the most strait forward, followed by Yast2 and dpkg.  \n\nI would look into it, and you will see why no-one has ever desired to implement this.\n\n-ian reinhart geiser"
    author: "ian geiser"
  - subject: "Re: Installer"
    date: 2002-04-04
    body: "Redhat (the only distribution apart from debian that's worked relaibly and stably for me, and i've tried mandrake, suse(when they had isos available, their net installer failed miserably for me), caldera, lycoris...) - it does have it's own updater (up2date) but i very much doubt they'll put kde3 packages on there any time soon (of course i could be wrong).\nThe other reason i think it would be a good thing is so there is a standard way of doing installs/updates, you just named 3 different distros each with there own way of updating - this means that you can't tell someone how to update software with kde (or non-ximian gnome) without asking which distribution they're using.  This doesn't sound like much of a problem, but it means that people don't know how to use linux, they know how to use suse, or mandrake, or redhat or...\nI think the best way to deal with this is to incorporate standard (useful) config tools and a software installer/updater into kde and gnome (preferably with some similarities between the two)\n\nAnyway that's all getting a bit offtopic, well done on releasing kde3, i'll give it a go next time i upgrade."
    author: "dave"
  - subject: "Re: Installer"
    date: 2002-04-04
    body: "well on the off chance you actually bought software you would find both Mandrake and SuSE install very easily and work very reliably.\n\nThe only difference is that both Mandrake and SuSE seem to put more effort into makeing KDE work.  \n\nMy advice to you is to upgrade, or at least try bero's packages.  \n\nI have found you never get more than what you pay for, and for me the $60 USD to SuSE was well worth it.  In fact I am all ready to preorder my next one.  Yes as a shocker I am a Linux user and I do _pay_ for software ;)\n\n-ian reinhart geiser"
    author: "ian geiser"
  - subject: "Re: Installer"
    date: 2002-04-04
    body: "I have also paid for linux software - my most recent purchase being codewarrior for linux (they normally produce great software, at a decent price, and have been very responsive to my emails about problems i have found with the linux version).\n\nThe problem with suse linux is that the last time i tried it it crashed on me, regularly (every few hours, and no it wasn't just x) so i wanted to try a newer version, tried the network install and that failed miserably - maybe it is worth the money now, but i am a sudent so i hold onto my money tightly and i'd rather stick with a distro i know to be stable rather than pay for one that was useless last time i tried it.\nI think it is a real shame that suse have stopped free downloads of iso's, i would love to try it again, but need to be convinced that they have made a distro that works well on my computer before i part with money. i have also had unreliable installs of mandrake in the past(basically the config tools seem to start failing after a few weeks) but will download the latest version soon and see if it is any better.\nThe system i have now is redhat 7.2 with a custom built 2.4.18 kernel, and gnome2 snapdhots from red-carpet and it works pretty well, but it would be nice to find a distribution that sets everything up as nice as i have it out of the box (doesn't really matter if it's kde or gnome) i just need something that doesn't crash, doesn't grind to a halt with lots of programs open (as the default redhat kernel does because o fmy lack of ram 96M not enough these days) and a nice extra would be easy to ue setup tools.  If suse met all those criteria on my p2 266 96M ram i'd happily pay for it. If you happen to know anyone running it on low end sytems like this i'd be happy to here if it's suitable."
    author: "dave"
  - subject: "Re: Installer"
    date: 2002-04-05
    body: "I'd love a red-carpet installer and all, but the thing is that it is a nightmare implementing one (and supporting all major distros), without something like Ximian's resources. \n\nNotice that GNOME itself only officially releases source packages, just like KDE. It is only a third party (ximian), that makes it available. If a few of the KDE developers formed a company, then it might be possible, but that's rather doubtful.\n\nOTOH, why not a straight-forward port of red-carpet to qt/kde (pretty easy, red-carpet isn't that big), or a KDE channel in red-carpet (if Ximian doesn't want to, use a thirdparty server)."
    author: "fault"
  - subject: "Re: Installer"
    date: 2002-04-05
    body: "A Red Carpet type download management program for KDE would be wonderful - at least for me anyway."
    author: "Jimmy"
  - subject: "Re: Installer"
    date: 2002-04-04
    body: "Well for suse-7.3 it is possible to apt-get kde3.\nMore info at http://apt4rpm.sourceforge.net follow\nthe link marked 'Apt-rpm reps'\n"
    author: "Richard Bos"
  - subject: "Re:"
    date: 2002-04-04
    body: "Cool! A very good work guys! This will begin a new era in UNIX desktop computing.\n"
    author: "SilvereX"
  - subject: "Super ... Great ... Wooaaaooo"
    date: 2002-04-04
    body: "Everybody did a marvellous job. Congratulations!!!!!!!!"
    author: "Nassos Koyrendas"
  - subject: "Mandrake 8.2 aRts problems"
    date: 2002-04-04
    body: "Anyone had problems with aRts not starting? It complains about not being able to autodetect Input/output method or something like that...\nI'm running this on a Compaq armada m700 with a ESS Maestro 2E sound.\n"
    author: "Namar Gonzales"
  - subject: "Re: Mandrake 8.2 aRts problems"
    date: 2002-04-04
    body: "Try downloading the rpm from ftp://ftp.ibiblio.org/pub/Linux/distributions/contrib/texstar/Mandrake-8.2-i586/kde-3.0.0\n\nThis is the ftp server attached to http://pclinuxonline.com   \n\nThey often have updated or interesting files there and interesting tips and tricks for Mandrake.\n\nJustin T.\n\n"
    author: "Justin T."
  - subject: "Re: Mandrake 8.2 aRts problems"
    date: 2002-04-04
    body: "I have! The same problem with a mad16 board.\nMaybe the mdk rpms are a bit broken?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Mandrake 8.2 aRts problems"
    date: 2002-04-05
    body: "hope this helps\ngo to ftp://ftp.ibiblio.org/pub/Linux/distributions/contrib/texstar/Mandrake-8.2-i586/kde-3.0.0\nand download libarts...rpm and arts...rpm\nuse --force --nodeps in rpm to install them\nrpm --nodeps --force libarts and arts.. rpms\ni am assuming mdk8.2\nthen restart kde and it worked for me\nthose 2 rpms are updated ones\nif u want in kcontrol(kde control centre) unser sound->soundserver in the General tab uncheck and click apply then check the start aRts soundserver on kde startup and apply as root, then restart kde, should work. worked here\n\nhth\ncrazycrusoe"
    author: "crazycrusoe"
  - subject: "Re: Mandrake 8.2 aRts problems"
    date: 2002-04-05
    body: "Worked here, thank you very much.\nI already had heard about texstar rpms for mandrake but I was a redhat user until last week, and now as a happy (even that kernel 2.4 sucks in hardware support, and because that I don't have ext3) mdk 8.2 user I'm just learning how to operate this new spaceship :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Mandrake 8.2 aRts problems"
    date: 2002-09-14
    body: "these directories do not excist anymore\n\ni still have no sound, and i have a ess maestro soundcard, on a Gateway Solo 2550"
    author: "peter"
  - subject: "Re: Mandrake 8.2 aRts problems"
    date: 2004-03-04
    body: "could you send me some art peroblems, their causes and how it is solve?"
    author: "rongie isip"
  - subject: "problem with kpersonalizer"
    date: 2002-04-04
    body: "I installed KDE3 on mdk 8.0 with old kdelibs to keep old kde 2.x apps working.\nMy problem is that every time I start kde kpersonalizer starts.\nHow do I tell it I already have configured all?\nI tryied finishing it, canceling it, but always it comes back.\nProbally there is some file on .kde that should point that kpersonalizer should not start, what file is it? Does someone know?"
    author: "Iuri Fiedoruk"
  - subject: "Re: problem with kpersonalizer"
    date: 2002-04-04
    body: "Heya,\n\n~/.kde3/share/config/kpersonalizerrc needs to look like:\n\n[General]\nFirstLogin=false\n\nThen you shouldn't see this popping up anymore.\n\nJustin T."
    author: "Justin T."
  - subject: "Re: problem with kpersonalizer"
    date: 2002-04-05
    body: "So why doesn't kpersonalizer do that itself? Not very user friendly, is it?"
    author: "Einar Larsen"
  - subject: "Re: problem with kpersonalizer"
    date: 2002-04-05
    body: "Worked, thank you very much."
    author: "Iuri Fiedoruk"
  - subject: "Re: problem with kpersonalizer"
    date: 2002-04-22
    body: "Hay, Thanks for your help Justin!!!\n\nI saw their was that file there but i dint know what should be in it.\n\nThanks Again\n\nCoomsie\t:3)\n"
    author: "Coomsie"
  - subject: "Re: problem with kpersonalizer"
    date: 2002-05-12
    body: "I accidently started Kpersonalize and it started showing up at each subsequent logins. The kpersonalizerrc file was existant but empty... could this be a bug or a file access issue which is silently discarded by the software?"
    author: "Eric Thibodeau"
  - subject: "Re: problem with kpersonalizer"
    date: 2006-09-22
    body: "Thanks!  I use Kubuntu, and upgraded from KDE 3.5.2 to 3.5.4. Upon realizing I still did not have the \"extras\" (games, toys, etc.), I installed the kdebase metapackage. Kpersonalizer kept popping up on boot, and this fixed it. :-)"
    author: "William H."
  - subject: "solaris 8 pkg's?"
    date: 2002-04-04
    body: "good work guys, anyone know the schedule for the Solaris 8 binaries?\n\n"
    author: "swongy"
  - subject: "Re: solaris 8 pkg's?"
    date: 2002-04-04
    body: "I heard Slowlaris packages were coming next week... :)"
    author: "Navindra Umanee"
  - subject: "Re: solaris 8 pkg's?"
    date: 2002-04-04
    body: "Please please make it work before Gnome 2.0 comes standard with Solaris 9. "
    author: "Redingofish"
  - subject: "Re: solaris 8 pkg's?"
    date: 2002-04-05
    body: "To my knowledge KDE has always worked well with Solaris.  Sun just doesn't care.  So why would this make a difference?\n\nAre there really that many Sun desktop users?"
    author: "Navindra Umanee"
  - subject: "Re: solaris 8 pkg's?"
    date: 2002-06-13
    body: "Hi Navindra,\n\nwhere can i download Kde 3.x packages (pkg format )for solaris 7 and 8?\n\nTIA.\nAmit\n"
    author: "Amit"
  - subject: "APT repository"
    date: 2002-04-04
    body: "Is there any APT repository to get kde 3?? (RedHat 7.2)"
    author: "Marcos Tolentino"
  - subject: "Re: APT repository"
    date: 2002-04-04
    body: "For SuSE-7.3 there is, for RedHat I don't know.\nBut if there is one for RH, you should be able\nto find it at the http://apt4rpm.sourceforge.net\nwebsite.  Follow the link marked 'apt-rpm reps'\n"
    author: "Richard Bos"
  - subject: "unable to access programs menu. control center"
    date: 2002-04-05
    body: "After installing kde3 rpms on mandrake 8.1, i am unable to access the program\nmenu or the control panel..can somebody help?\n"
    author: "Pringles"
  - subject: "Re: unable to access programs menu. control center"
    date: 2002-04-05
    body: "I know that Mandrake 8.2 has menudrake that can be used to fix the problem.  I dont remember if 8.1 has the menudrake.  It it does, use menudrake to create the kmenu."
    author: "Paul"
  - subject: "Re: unable to access programs menu. control center"
    date: 2002-04-05
    body: "Thanks a lot! menudrake is available on mdk8.1.\nI have to change /opt/kde3/share/applnk-mdk-simplified to /opt/kde3/share/applnk-mdk as menudrake seems to be looking for this file.\nJust in case, somebody else have similar problems.\n\nThanks again! kde rocks!!!\n"
    author: "Pringles"
  - subject: "No SOUND with SuSE 7.3 x86 rpms - rc3 was good?"
    date: 2002-04-05
    body: "What did you get?\n\nApart from that KDE 3.0 is GREAT.\nEspecially KDEPrint.\n\nCheers,\n Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: No SOUND with SuSE 7.3 x86 rpms - rc3 was good?"
    date: 2002-04-05
    body: "I have the same problem. \n\nI use a self-compiled kernel with OSS drivers, maybe that is the problem?\nWell, I can live without KDE sound for a while.\n\nIronically arts works with xmms and arts-output plugin, so I think the SuSE package kdemultimedia3 is broken in some way."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: No SOUND with SuSE 7.3 x86 rpms - rc3 was good?"
    date: 2002-04-05
    body: "Yes, I think so.\nSame problem occurs to me...\nIf someone knows how to fix it, please tell me!\nThanx!"
    author: "Thowil"
  - subject: "Re: No SOUND with SuSE 7.3 x86 rpms - rc3 was good?"
    date: 2002-04-07
    body: "On the kde-ftp server there are some new SuSE-RPMs (arts/base/qt). I'll give them a try to see if they fix my problems..."
    author: "Schwann"
  - subject: "Re: No SOUND with SuSE 7.3 x86 rpms - rc3 was good?"
    date: 2002-04-07
    body: "That did it, as always the SuSE people fix the problems! And fast!! Thanks for the new Arts and multimedia RPM:s!!!"
    author: "DiCkE"
  - subject: "Re: No SOUND with SuSE 7.3 x86 rpms - rc3 was good?"
    date: 2002-04-05
    body: "Yes, good catch. I use the OSS driver, too 'cause I do kernel preemption (latency) beta testing and do not have the time to apply ALSA on every run...\n\nSadly -rc3 for SuSE had worked fine.\n\nSecond:\nklipper do not come up during start phase.\n\nThird:\nKMail mix languages when you send a REPLY and brake some time/date stamps.\n\nThanks,\n Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: No SOUND with SuSE 7.3 x86 rpms - rc3 was good?"
    date: 2002-04-06
    body: "I had the same problem with SuSE 7.1 rpms but after I recompiled aRts from source the problem have gone. If I remember correctly I compiled like this:\n\n./configure --prefix=/opt/kde3 --disable-debug --with-alsa\nmake\nsu root\npassword:.......\nmake install\n\nI hope that helps :)\n\nPanayiotis"
    author: "Panayiotis"
  - subject: "Re: No SOUND with SuSE 7.3 x86 rpms - rc3 was good?"
    date: 2002-04-09
    body: "The official SuSE 7.3 packages fixed my sound problems.\n\nhttp://www.suse.de/de/support/download/linuks/i386/update_for_7_3/experimental.html\nftp://ftp.suse.com/pub/suse/i386/supplementary/KDE/update_for_7.3\n\nFaster mirror:\n\nftp://ftp.gwdg.de/pub/linux/suse/ftp.suse.com/suse/i386/supplementary/KDE/update_for_7.3\n\nThanks,\n Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: No SOUND with SuSE 7.3 x86 rpms - rc3 was good?"
    date: 2002-04-10
    body: "That is good news !\n\nThank you for the links ! :)\n\nPanayiotis"
    author: "Panayiotis"
  - subject: "KOffice for KDE3? Where?"
    date: 2002-04-05
    body: "Have someone find out where the KOffice packages for KDE3 are?"
    author: "xtian0009"
  - subject: "Re: KOffice for KDE3? Where?"
    date: 2002-04-07
    body: "At http://www.koffice.org/news.phtml there is an announcement about KOffice 1.1.1 for KDE 3 being available.  It links to http://www.kde.org/announcements/announce-3.0.html which I have seen before, which says \"A KDE 3 port of the KDE office suite is available.\"  Nothing.\n\nHowever, when I jump over to the FTP mirror that I used to get KDE 3 source code, ftp://ibiblio.org/pub/packages/desktops/kde/ and I see that KOffice 1.1.1 for KDE 3 has been recently posted in ftp://ibiblio.org/pub/packages/desktops/kde/stable/koffice-1.1.1-kde3 (it wasn't there before).  I was just about to post a comment that I couldn't find this software, but now I found it.  Use the URL above or the URL or the same place on another FTP mirror."
    author: "desktop-at-nowhere"
  - subject: "Re: KOffice for KDE3? Where?"
    date: 2002-05-08
    body: "Yes, and when they find that, tell me where libkdeprint.so.0\ndependant file is so I can install the rpm.  Really aggravating\nto be hyped about installing the new koffice on kde2 and\nfinding one install dependant file.\n\n                             Don O."
    author: "don ousley"
  - subject: "any one noticed problems with splash screen ?"
    date: 2002-04-05
    body: "after the splash screen loads(loads kde), after kde is loaded the message saying ksplash as crashed ? any one experienced this one ?\n"
    author: "crazycrusoe"
  - subject: "Re: any one noticed problems with splash screen ?"
    date: 2002-04-06
    body: "thats a QT problem i heard. QT-copy from kde CVS fixes it i heard.\n"
    author: "ik"
  - subject: "problems with noatun , any one ?"
    date: 2002-04-05
    body: "noatun does not play any songs ? if i add directories to it it just never stops quering the song names though everything is already displayed in the playlist of noatun? any one had this problem as well ?"
    author: "crazycrusoe"
  - subject: "Re: problems with noatun , any one ?"
    date: 2002-04-05
    body: "IMHO it's because arts is not working...\nI have the same problem. Maybe you're using SuSE 7.3 rpms.\nEverything was fine with rc3 but now i can't make it work.\nAnyone?\n"
    author: "k-mare"
  - subject: "Re: problems with noatun , any one ?"
    date: 2002-04-06
    body: "no, i am using updated rpms by texar for mandrake 8.2 from www.pclinuxonline.com and arts is working fine with no problems after i updated arts3.. and libarts3.., kaboodle plays the files fine, but it has not got a playlist, so i gotta use noatun. n noatun doesnt wanna play\n\natached a screenshoot :-)"
    author: "crazycrusoe"
  - subject: "hola"
    date: 2004-06-16
    body: "hi"
    author: "Omar"
  - subject: "Re: problems with noatun , any one ?"
    date: 2002-04-06
    body: "I had a similar problem with SuSE 7.1 rpms but after I recompiled aRts from source the problem 's gone\n\nEnjoy our new KDE :)\n\nPanayiotis\n"
    author: "Panayiotis"
  - subject: "Thank You"
    date: 2002-04-05
    body: "Thank you for the beatiful desktop that you have created for us.\nIt's just beaituful."
    author: "HappyKdeUser"
  - subject: "Still no KDE for Darwin?  GNOME has you beat there"
    date: 2002-04-06
    body: "Gosh, I wish I could install KDE on my machine.  It's a Mac with OSX, meaning of course that it has Darwin.  The move to 3.0 represented a major opportunity for a big code re-write to permit this.  Doesn't seem like that's happened.\n\nI'm not sure if the KDE Project understands the fact by year's end there will be literally millions of UNIX computers in consumers' hands.  If even a small percentage of them download and use KDE it would be a tremendous coup.\n\nI'll tell you who already has a version of its environment ready for Darwin.  Your archrivals at GNOME.\n\nYes I know that there are \"issues\" , whatever they may be, that make KDE on Darwin a problem.  Well I thought KDE was supposed to be *the* GUI for UNIX.\n\nNot solving a problem that prevents you from being used on what is by far the most popular consumer UNIX out there seems like a boneheaded move. "
    author: "Leo"
  - subject: "Re: Still no KDE for Darwin?  GNOME has you beat there"
    date: 2002-04-06
    body: "Leo, please calm down a bit - take up yoga or something.\n\nI have two iMacs, one running SuSE Linux and the other with Mac OS X. I too would love to have KDE running on both machines. Work is underway, but at an early stage. Here is a recent discussion on the kde-devel list:\n\nOn Saturday 30 March 2002 9:10 pm, Benjamin Reed wrote:\n> Christopher Molnar [molnar@kde.org] wrote:\n> > Can anyone help me with this problem, or at least point me in the right\n> > direction.... I am getting the following errors on building arts (CVS\n> > HEAD) on mac OS X.\n>\n> If it helps any, there are a couple of us working on arts (and kde) for the\n> fink distribution.  We have arts and kdelibs building, but there are a\n> number of deeper code issues we haven't had a chance to work out yet.  If\n> you want to get together and join forces, feel free to e-mail me in private\n> and I can get you access to our current patchset.  It's not to the point\n> where we're ready to show it to anyone, but if you want to assist in\n> porting, let me know.  =)\n\n-- Richard\n"
    author: "Richard Dale"
  - subject: "Re: Still no KDE for Darwin?  GNOME has you beat t"
    date: 2002-04-07
    body: "Oh OK.  Fair enough.  I had gotten the impression that there was no interest in or work on Darwin in the KDE community.  Glad to be proven wrong.   Good luck.   Ommm........."
    author: "Leo"
  - subject: "Umm...why?"
    date: 2002-04-06
    body: "Not a flame, just curious. If you've got Aqua, why do you need another DE? \n\nUnless you wanted to run some KDE apps under Aqua, which would be a good reason. Hmm...I myself hadf thoughts of Konqueror (with future tabbed browsing) with HP Liquid under Aqua...."
    author: "Ty"
  - subject: "Re: Umm...why?"
    date: 2002-04-07
    body: "Aqua is great, no question, and in many ways better than KDE.\n\nHowever, I got interested in KDE before Aqua.  In fact, KDE was so slick and attractive that it is what finally got me interested in Unix/Linux - remember I come from the Mac computing culture in which user interface has been the primary sought-after good.\n\nIn fact I think there was a two year window or so when the combination of Unix or Linux combined with KDE was superior to Windows and to MacOS8/9, in both reliability and in attractiveness and polish of user interface.\n\nWith Luna and Aqua, I think Microsoft and Apple have leapfrogged KDE in attractiveness again, and with the 2K core and BSD Unix, Microsoft and Apple have at least equaled Linux and Intel Unix in realibility and stability, for many real-world purposes (I know  - flamebait).\n\nAnyway, I'm still interested in KDE, because I still think it looks nice, and because of my strong interest in alternatives to Microsoft, particularly in the office suite arena.  I like the fit and polish of KOffice, especially its integration with KDE.  I'm rooting for it to achieve file interoperability with MS Office.\n\nPlus, I just think being able to run KDE on my Mac would validate, in a visceral, emotional seense, its claims to be a Unix computer."
    author: "Leo"
  - subject: "Re: Still no KDE for Darwin?  GNOME has you beat t"
    date: 2002-04-07
    body: "It's not really KDE's fault. When binutils has prelink support, we can (probably) get rid of kdeinit, which would allow it to run on OSX. (alternativly, OSX could be fixed)."
    author: "fault"
  - subject: "Re: Still no KDE for Darwin?  GNOME has you beat t"
    date: 2002-04-07
    body: "Oh I see.  Well, no, I don't, at all.\n\nI didn't understand any of the references you made there.  What I gather from context and the title is that factors external the Project and outside its control have prevented KDE on Darwin, but that there are some prospects for the situation to change."
    author: "Leo"
  - subject: "Re: Still no KDE for Darwin?  GNOME has you beat t"
    date: 2005-01-27
    body: "I am running KDE on OS X as I write this.\nIt's simple enough, just read the August 04 issue of MAC Addict magazine, it steps you through the setup and install."
    author: "Hank"
  - subject: "Poor quality of SuSE rpms :("
    date: 2002-04-06
    body: "\nI found two IMO grave problems with the SuSE 7.2 i386 packages:\n\n\nNo way to get AA work.\nGoing back to an older selfcompiled qt302, AA worked.\nI use X420 upgraded by the suse 72 rpms.\n\n\nkdeaddons is missing. Is that political or did they simply forget it?\nIt was already missing in their -rc3-rpms"
    author: "me"
  - subject: "Re: Poor quality of SuSE rpms :("
    date: 2002-04-06
    body: "\nAnd I have to add the question why they don't include the qt-designer?"
    author: "me"
  - subject: "Mandrake RPMs"
    date: 2002-04-06
    body: "Feature list is really impressive, but why are mdk RPMs about twice as\nbig than others (redhat for instance)? It'll take all day to download!\nAre there more programs and stuff, or does it perhaps have something to do\nwith objprelink of something? Or are they just badly (or better) compiled?"
    author: "56k modem user"
  - subject: "Xrender?"
    date: 2002-04-06
    body: "KDE 3 looks very cool! Does it use Xrender for transparency and other rendering?"
    author: "Stof"
  - subject: "For all you speed freaks out there..."
    date: 2002-04-06
    body: "...Texstar has started rolling his Mandrake RPMs (these tend to be highly optimised, using things like objprelink). The i586 batch are out, I'd imagine the i686 ones will be there soon. The URL is http://www.pclinuxonline.com/. Even if you've downloaded them, you should still check it out, it's a great site. \n\nAs for the question above about Mandrake RPMs being twice the size of Redhat ones, it sounds like the Mandrake ones have debugging info compiled in. And for those of you who look at your /usr and shudder these days, the Mandrake RPMS install into /opt/kde3. Cool (though I could never understand why the FHS people didn't simply create a /usr/kde and a /usr/gnome to complement /usr/X11R6 - it make perfect sense given the size of the projects)."
    author: "Bryan Feeney"
  - subject: "Re: For all you speed freaks out there..."
    date: 2002-04-08
    body: "It's easy to create a soft link... So u have all in /usr/ or /usr/local/, or where u want ..."
    author: "Damien"
  - subject: "For all you speed freaks out there..."
    date: 2002-04-06
    body: "...Texstar has started rolling his Mandrake RPMs (these tend to be highly optimised, using things like objprelink). The i586 batch are out, I'd imagine the i686 ones will be there soon. The URL is http://www.pclinuxonline.com/. Even if you've downloaded them, you should still check it out, it's a great site. The links to the FTP site with the RPMs is on the middle of the left hand bar (there's two sites, ibiblio and eastwind).\n\nAs for the question above about Mandrake RPMs being twice the size of Redhat ones, it sounds like the Mandrake ones have debugging info compiled in. And for those of you who look at your /usr and shudder these days, the Mandrake RPMS install into /opt/kde3. Cool (though I could never understand why the FHS people didn't simply create a /usr/kde and a /usr/gnome to complement /usr/X11R6 - it make perfect sense given the size of the projects)."
    author: "Bryan Feeney"
  - subject: "RH 7x and libcrypto/libssl"
    date: 2002-04-06
    body: "I've downloaded the RedHat rpms for KDE3 but I get dependency errors for libcrypto.so.2 and libssl.so.2.  i've searched rpmfind.net for them, but no luck.  Can anyone tell me where to find them?\n\nI've compiled RC3 and it works just fine, very impressive, but I don't feel like spending a whole day to compile the source code for KDE3. Call me lazy, but there you have it.\n\nAny help would be greatly appreciated.\n\nThanks."
    author: "jack"
  - subject: "Re: RH 7x and libcrypto/libssl"
    date: 2002-04-07
    body: "get \"openssl*.rpm\" from your installation cd or from freshmeat."
    author: "Asif Ali Rizwaan"
  - subject: "Re: RH 7x and libcrypto/libssl"
    date: 2002-04-07
    body: "sorry that was rpmfind.net not freshmeat"
    author: "Asif Ali Rizwaan"
  - subject: "Re: RH 7x and libcrypto/libssl"
    date: 2002-04-07
    body: "Thanks, I got it, and am running KDE3.  "
    author: "jack"
  - subject: "Re: RH 7x and libcrypto/libssl"
    date: 2002-09-04
    body: "I've encounter the same problem.  By create following symbolic links\nproblem seems solved.\n\n/lib/libssl.so.2 --> /usr/local/ssl/lib/libssl.so.0.9.6 (vary base on your OpenSSL installation)\n/lib/libcrypto.so.2 --> /usr/local/ssl/lib/libcrypto.so.0.9.6 (vary base on your OpenSSL installation)\n\nThen run \n\n# ldconfig\n\nEverything backs to normal.. \n \nYou may try the same approach on your system and it should work too.\n\n"
    author: "Scott Yuan"
  - subject: "KDE 3.0 wont work under Mandrake 8.1."
    date: 2002-04-06
    body: "Guys, I've installed KDE 3.0 on a clean mandrake 8.1 system. I've installed it beside KDE 2.2.1 using the madrake 8.1 RPM's from the KDE-website. During the installation I had to force a few processed, but afted that the packagemanager indicated that the program was installed succesfull. \n\nHowever, I can't use both KDE 3.0 and 2.2.1 when logging in (graphical mode) as a normal user. When running as root I can only use 2.2.1 (so far with no problems), even if I select KDE 3.0 when logging in. \n\nNote: I am a low-technical linuxuser and most of my problems with Linux are with the installation of programs. Wether they are tarballs,RPM'S or something else. I hope the future installations get more standard and just as easy as onder Windows.\n\nAnyway, I hope to run this cool desktop very soon. Please help me to make this possible!\n"
    author: "Maarten Rommerts"
  - subject: "Re: KDE 3.0 wont work under Mandrake 8.1."
    date: 2002-04-06
    body: "Try using kde3 command instead startx to enter kde3.\nWorks fine here (mdk 8.2)"
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE 3.0 wont work under Mandrake 8.1."
    date: 2002-04-06
    body: "I've tried using the command kde3 instead of startx, but it has no result."
    author: "Maarten Rommerts"
  - subject: "Re: KDE 3.0 wont work under Mandrake 8.1."
    date: 2002-04-07
    body: "try this:\nWINDOWMANAGER=kde3 startx\nor this:\nWINDOWMANAGER=/opt/kde3/bin/startkde startx\n\nRinse"
    author: "rinse"
  - subject: "HELP! Fail with Redhat 7.2"
    date: 2002-04-07
    body: "Hi,\nI tried installing like the readme for the rh packages says:\n\n\"To remove the old packages and install the new ones, run:\n\ncd /where/you/downloaded/the/KDE3/RPMS\nrpm -e `rpm -qa |egrep ^kde`\nrpm -Uvh *rpm\"\n\nI already got dependencies errors with removing the old packages, so I choose --nodeps.\nSame with installing the new ones. I just thought \"oh well, there was a lot in the ftp-archive, it will be included\".\n\nBut now, Redhat won't let me choose KDE from the login manager.\nWhat can I do to get it started?\n"
    author: "Markus Biedermann"
  - subject: "Re: HELP! Fail with Redhat 7.2"
    date: 2002-04-07
    body: "I Mean, maybe it did not install?? I am such a newbie....never should have done this..totally lost..."
    author: "Markus Biedermann"
  - subject: "Now running without sound :("
    date: 2002-04-07
    body: "OK, it worked after i installed every single package after the other and solved the conflicts.\n\nBut I have no sound :(\nan kpackage always crashes.. :(\n\nCan someone help?"
    author: "Markus"
  - subject: "Re: Now running without sound :("
    date: 2002-04-07
    body: "Did you install arts?\n\nAnyway, report your problems following the instructions in the README file.\n"
    author: "Carg"
  - subject: "..."
    date: 2002-04-08
    body: "OK, it works. There was another conflict with arts.\n\nthx"
    author: "Markus Biedermann"
  - subject: "Problems with Mandrake 8.2"
    date: 2002-04-08
    body: "I installed KDE 3.0 on a fresh Mandrake 8.2 installation using rpm -Uvh commands. First I\ninstalled qt3, then qt3-devel (since I use it for development). Here it reported some failed \ndependencies, which I all resolved except one - this one was MySQL-devel something. I thought\nthat this failed dependency should not interfere with KDE, so I used --nodeps option to install\nqt3-devel and then other packages were installed without problems. However, when I started\nkde, all kde applications (except KOffice) have gone from the K menu, and even KWord\nand other KOffice applications failed to start (they reported some unknown simbol in\nqt library, when started from konsole). Also, some application that I am currently involved with,\nit uses hasFocus() function from QWidget, and it turned out that hasFocus() always returned\n0, no matter if the widget had focus or not. It seems like a Qt related problem, but can anyone\nhave any idea what might have caused all these problems?"
    author: "Bojan"
  - subject: "Re: Problems with Mandrake 8.2"
    date: 2002-04-08
    body: "if you install libmysql10 and libmysql10-devel packages, the qt3-devel dependence problem will go away.\n"
    author: "rover"
  - subject: "Re: Problems with Mandrake 8.2"
    date: 2002-04-08
    body: "There is no libmysql1.0-devel package in Mandrake 8.2 CDROM-s I guess. However, I suspect, that this has nothing to do with the troubles I have..."
    author: "Bojan"
  - subject: "Deleting messages from POP3 message headers"
    date: 2002-04-08
    body: "Thanks!  This has got to be considered a killer feature for an email client.  It seems it was implemented to deal with unsolicited huge downloads, but I get boat-loads of spam (*many* unsolicited messages, often small), so I use it to avoid the spam.  I also find it useful for lists that I have subscribed to, to avoid long discussion threads that I am not interested in.\n\nI regularly delete enough messages by header alone to make this extra step faster than first downloading, and then deleting.  I also prefer to screen messages before I \"allow them into\" my inbox.\n\nCan anyone tell me whether I can get the raw email address to display in the \"Sender\" column, rather than the friendlier version of the sender's name?\n\nThanks again for this feature!\n\n"
    author: "Andy Marchewka"
  - subject: "Great Stuff - Request One Additional Feature"
    date: 2002-04-09
    body: "The install of KDE 3.0 went without issue on my FreeBSD 4.5-Stable box.  I like what I see.\n\nI love the blocking of popups in Konqueror.  I was wondering if anyone has thought of adding a feature to prevent websites from altering the window size of Konqueror?  This is one of my personal pet peeves.\n\nGreat job guys..."
    author: "Carl"
  - subject: "Arabic support!"
    date: 2002-04-09
    body: "Hi guys, \n\nWell this time KDE really ROCKS :) although there are still some few bugs around (the splash screen crashes for example!!) ..\n\nbut the main issue for me now is the Arabic support .. I thought that I'll be  finally able to type in Arabic .. I tried to change the Layouts . I chose the Arabic one , and then switch the language from the Language flag on the Panel ..\nBut still I couldn't type in Arabic (I was using Kword and Kate) .. I noticed that some other similar languages such as Hebrew work fine !!!\n\nAny idea how to solve this ??????\n\noh I was about to forget :) I'm using a Pentium II (400) machine loaded with Redhat 7.2 ..\n\nthanx for You KDE people ..\n"
    author: "Faris"
  - subject: "noatun doesn't play .dat files :("
    date: 2002-04-09
    body: "Hi Guys, \nI have a problem with noatun .. it doesn't run .dat files ! .. all the movies around me have this format .. \nthis is a sad situation because everytime I want to watch a movie I need to switch to Window$ :(\n\nany help ????\n\n\n"
    author: "Faris"
  - subject: "Re: noatun doesn't play .dat files :("
    date: 2002-04-10
    body: "use codeweavers plugin and forget that lazy,unstable player"
    author: "antonito macho dominator"
  - subject: "Need help with KDM!"
    date: 2002-04-09
    body: "Hi guys. I'm having a problem with my KDE3 install on SuSE 7.2.\n\nKDM isn't working properly. I changed /etc/init.d/xdm to point to /opt/kde3. KDM will start, but it looks really dull, with a grey background 'n stuff. I can login to a 'default' and 'failsafe' session. I tried default, and it just gives me a yellow xterm. I can get into KDE3 with the startkde-command (nice!), though, but that's clumsy and not the way i want it.\n\nI've tried configuring KDM from the menu in KDE3 itself, but that doesn't seem to work/help at all. How do i configure KDM (from the command-line if necessary)? (Somehow KDM from KDE2 doesn't work well anymore, either)"
    author: "Cihl"
  - subject: "Re: Need help with KDM!"
    date: 2003-11-19
    body: "I've the same problem and I can't resolve it.\nIf you have any solution please tell me.\n\nThanks"
    author: "JB"
  - subject: "Re: Need help with KDM!"
    date: 2004-05-01
    body: "I have the same problem, if someone has any solution...\n\n"
    author: "Elfire"
  - subject: "panel freezes on slack 8.0"
    date: 2002-04-09
    body: "perhaps someone can help. i first installed kde 3.0 by compiling source, then by using the slackware binaries. in both cases, the panel (or kicker) freezes up frequenly. this has happened on both my home pc and work pc. even the clock freezes up. if i kill the kicker process and fire it back up all is well...for a while.\n\ni have successfully run KDE 2.2 & KDE 3 RC3, but no luck with the KDE 3.0 stable.\n\nany ideas?"
    author: "jhollett"
  - subject: "Won't Compile on Freebsd-4.5"
    date: 2002-04-11
    body: "when i try to compile kdelibs on freebsd 4.5-stable it gets to the ../kio/kio directory and gmake/g++ comsume all of the RAM/swap on my system.  i mean every last drop! i have 256 megs of ram and 128 megs of swap.  i assume this isn't normal?  anyone know what i can do to get it to compile?  i'm running gcc-2.95.3\n\ni've attached the outout of what happens when i run make.  not much to see though, the internal compiler error is caused when the OS forcibly kills the process\n\nNathan"
    author: "Nathan"
---
Today the KDE Project proudly <a href="http://www.kde.org/announcements/announce-3.0.html">announces the release of KDE 3.0</a> (fully mirrored <a href="http://dot.kde.org/1017860068/#pressrelease">below</a>), a release which marks a new era of choice on the desktop.  Every advance opens the door to a group of new adopters, and KDE 3 is set to tear the doors asunder.  In celebration of the release, the <a href="http://www.kdeleague.org/">KDE League</a> has posted the text <a href="http://www.kdeleague.org/kgx.php">KDE: The Complete Enterprise Desktop Solution</a>, a work-in-progress but already something useful to show those who are considering the migration to freedom.  Enough talk, time to <a href="http://dot.kde.org/1017860068/#binary">download</a>.
<!--break-->
<p>&nbsp;</p>
<a name="pressrelease"></a>
<i>[Full Announcement <a href="http://www.kde.org/announcements/announce-3.0.html">Available Here</a>]</i>