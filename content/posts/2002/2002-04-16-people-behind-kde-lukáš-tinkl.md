---
title: "People Behind KDE: Luk&aacute;&scaron; Tinkl"
date:    2002-04-16
authors:
  - "wbastian"
slug:    people-behind-kde-lukáš-tinkl
comments:
  - subject: "single guy..."
    date: 2002-04-15
    body: "I guess he means that he is not two guys :-)"
    author: "Meretrix"
  - subject: "Re: single guy..."
    date: 2002-04-16
    body: "In some languages (including mine) you're either married or single. So you can be a single guy/girl with or without a girl/boyfriend.\n\nI guess this is also the case with his language."
    author: "Carg"
  - subject: "Re: single guy..."
    date: 2002-04-17
    body: "Yes, \"single\" is often translated into Czech as \"svobodny\" meaning \"not married\"."
    author: "Lada"
  - subject: "Beer"
    date: 2002-04-16
    body: "~ Coke or Pepsi?\nCan I say beer?\n.\nLOL!! It's been way too long since I've been to Prague! Best beer in the world!"
    author: "jd"
  - subject: "Re: Beer"
    date: 2002-04-16
    body: "Of course you can say beer. You wouldn't be answering the question though."
    author: "Rob Kaper"
  - subject: "Re: Beer"
    date: 2002-04-16
    body: "Unless you count the implicit \"No preference, don't drink much coke\"."
    author: "Anon"
  - subject: "Re: Beer"
    date: 2002-04-16
    body: "I love Gambrinus !"
    author: "imm"
  - subject: "Re: Beer"
    date: 2002-04-16
    body: "\n> I love Gambrinus !\n\nGambrinus definately rocks :-)"
    author: "luxus"
  - subject: "Re: Beer"
    date: 2002-04-16
    body: "woah!!\n\ni have a pilstner urquel 6 pack in my fridge!!\nhmmm skunk beer..\n\n\n"
    author: "mistigri"
  - subject: "Re: Beer"
    date: 2002-04-17
    body: "Whoo hoo! I'm gonna be in prague in about 2 months. I am sooo looking forward to the beer. Hell, the only reason we are visiting Belgium is that I love Hoegaarden beer.  Now if I can just find a good swingers club, and some fine herbal medication in Prague, my triumverate of sin will be complete."
    author: "EvilNed"
  - subject: "jomama@jomama.jomama"
    date: 2002-04-16
    body: "Show me more!"
    author: "jomama@jomama.jomama"
  - subject: "Mosfet?"
    date: 2002-04-17
    body: "I must have missed it... Where's the Mosfet's sister part?"
    author: "AC"
  - subject: "Re: Mosfet?"
    date: 2002-04-18
    body: "No, you see, Mosfet has a reputation for cross dressing, and from the pic, so does Lukas (and I don't care if it's missing accented characters)."
    author: "Carbon"
  - subject: "Re: Mosfet?"
    date: 2002-04-18
    body: "Oh no :) This was just an XMAS party, and I don't even remember wearing that wig. You bet all the remarks about Czech beer above apply. ;)"
    author: "Lukas Tinkl"
  - subject: "Unfortunate timing"
    date: 2002-04-24
    body: "The name Lukas Tinkl is now associated with Mosfet and crossdressing in the minds of most of the KDE community. Sucks to be you :-(\n\n--\nhttp://www.gegl.org - Check out the Gimp 'E' Graphical Library"
    author: "James Paterson"
  - subject: "No news"
    date: 2002-04-24
    body: "What's happening in KDE-land since 3.0? There's no new Kernel Cousin, and no news on the dot. Or has everyone decided that GNOME 2 is perfect and given up? (j/k)"
    author: "cbcbcb"
  - subject: "Re: No news"
    date: 2002-04-24
    body: "....or everyone's staring at there very beautiful kde desktop wondering when the first crash will occur. :o)"
    author: "me"
  - subject: "Re: No news"
    date: 2002-04-24
    body: "> What's happening in KDE-land since 3.0\n\nActive development, Konqueror gained tabs and KOffice 1.2 Beta is going to be released.\n\n> There's no new Kernel Cousin, and no news on the dot.\n\nAaron has no time anymore, if no new author jumps in there will be no new Kernel Cousin KDE."
    author: "Anonymous"
  - subject: "Re: No news"
    date: 2002-04-25
    body: "I've been busy with end-of-term projects and haven't had much time to breath.  I'm a little freer at the moment, but still too exhausted to post anything right now.  I'm almost out of the burn-out phase.  You know, it's inevitable after endless nights/days of hacking.\n\nFor chrissakes guys, why don't you submit some nicely written articles so that we can just approve them?  Instead, we get lots of mail about news that *we* should write articles for...  as if.\n\nI'm still annoyed at the lack of bandwidth for the dot.  :(  Anyone want to host us?\n"
    author: "Navindra Umanee"
  - subject: "Re: No news"
    date: 2002-04-25
    body: ">>> Anyone want to host us? <<<\n\nI would if it's free :-)\n\nHow much bandwidth anyway does the dot uses a month (average?)"
    author: "Rajanr"
  - subject: "Re: No news"
    date: 2002-04-26
    body: "\n> For chrissakes guys, why don't you submit some nicely written articles so that\n> we can just approve them? \n\nBecause, as demonstrated by my previous post, I don't know anything about KDE? ;)"
    author: "cbcbcb"
  - subject: "OT"
    date: 2002-04-25
    body: "Where are the debian packages for KDE 3.0? I mean, it has got to be a month ago KDE 3.0 has been release... wonder where they go. I hate compiling my own stuff - kinda hard and irritating to uninstall later :-/"
    author: "Rajanr"
  - subject: "Re: OT"
    date: 2002-04-25
    body: "Ask Debian, not KDE. Or better read http://www.debianplanet.org or http://lists.debian.org/debian-kde/."
    author: "Anonymous"
  - subject: "Re: OT"
    date: 2002-04-25
    body: "The Debian packages are handled by Debian (just like SuSE packages are done by SuSE, RedHat's by RedHat etc.), not by KDE. The Debian maintainer in charge was busy so the whole thing got delayed, leading to numerous flamefests.\n\nI suggest you do compile it, as it's really not that hard. The correct Qt version is already in testing, so you only need kdelibs, kdebase and anything else you want. Then it's ./configure --prefix=/opt/kde; make; su -c \"make install\". To uninstall, just rm -rf /opt/kde\n \nIf you put kde into /opt/kde, it won't clash with Debian at all (Debian puts nothing in /opt) and the compile process for a release will go without any problems. If you have the few hours it takes to compile it, it's a good exercise as nobody knows yet when the debs will be ready."
    author: "cosmo"
  - subject: "Re: OT"
    date: 2002-05-05
    body: "Experimental debs are available at http://www.geniussystems.net/KDE3%20Experimental/."
    author: "Windi"
---
Ever wonder what a KDE developer looked like in a Christmas party setting? Ever wonder if <a href="http://www.mosfet.org/">Mosfet</a> had a good-looking sister? 
Hop on over to this week's interview with 
<a href="http://www.kde.org/people/lukas.html">Luk&aacute;&scaron; Tinkl</a>, 
on the 
<a href="http://www.kde.org/people/people.html">People Behind KDE</a>
page, and find out why Luk&aacute;&scaron; likes it hot and if it's possible to be 
single AND have a girlfriend.
Luk&aacute;&scaron; is co-maintainer of 
<a href="http://i18n.kde.org">kde-i18n</a>, is coding 
on 
<a href="http://www.koffice.org">KOffice</a>,
<a href="http://www.koffice.org/kpresenter/">KPresenter</a>,
<a href="http://www.koffice.org/kspread/">KSpread</a>, the image gallery plugin 
for Konqueror and, he doesn't know for sure, perhaps more.


<!--break-->
