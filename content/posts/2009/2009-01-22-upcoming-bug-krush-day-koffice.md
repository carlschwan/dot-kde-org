---
title: "Upcoming Bug Krush Day for KOffice"
date:    2009-01-22
authors:
  - "iwallin"
slug:    upcoming-bug-krush-day-koffice
comments:
  - subject: "Feature List should be updated"
    date: 2009-01-23
    body: "One huge problem that I currently see with the project is the lack of progress updates for loyal followers, personally i really think that feature plans are a good thing and the kOffice feature plan hasn't been updated in ages, there was a minor trivial edit in December after 8+ months of stagnation, if we could just have a few of the developers go through this and show the progress that has been made it would be good.\n\nhttp://wiki.koffice.org/index.php?title=KOffice2/Goals\n\nOn that same note, since KDE 4.2 will be released soon why not update its feature page: http://techbase.kde.org/Schedules/KDE4/4.2_Feature_Plan\n\nAnd on that note, its embarrasing to see all of the unfullfilled features that are in the previous schedules for kde 4.1 and 4.0\n\nIf some of the developers would take 20 minutes out of their schedule to update these pages it would be tremendously helpful.\n\n4.0 : http://developer.kde.org/development-versions/kde-4.0-features.html\n\n4.1 : http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan\n\nAnd while were on it, some of the cleanup in 4.0, 4.1 and 4.2 can be moved to the new 4.3 feature plan: http://techbase.kde.org/Schedules/KDE4/4.3_Feature_Plan "
    author: "Sebastian Alterman"
  - subject: "Re: Feature List should be updated"
    date: 2009-01-23
    body: "> i really think that feature plans are a good thing \n\nones that require hand-editting to keep up are difficult though; blogging is often just as useful if done regularly, and automated feature plans would be a godsend.\n\n> its embarrasing to see all of the unfullfilled features\n\ncompared to all the features that actually made it in? *shrug*\n\n> 4.2 can be moved to the new 4.3 feature plan:\n\nan announcement from the release team was already made for this"
    author: "Aaron Seigo"
  - subject: "Re: Feature List should be updated"
    date: 2009-01-23
    body: "does that mean you are offering to do everything you have pointed out?\n\nI hope so, I mean if you are going to point things out then you can surely help fix them"
    author: "R. J."
  - subject: "Re: Feature List should be updated"
    date: 2009-01-23
    body: "I truly wish i could, i don't know if the developers have implemented these features and just forgot to update the feature plans or if they genuinely are not done.\n\nIm not just complaining for the fun of it, if i could do it myself i would."
    author: "Sebastian Alterman"
  - subject: "Re: Feature List should be updated"
    date: 2009-01-23
    body: "In spite of the criticism in the previous replies, I think you have a point.  Except for alpha/beta and soon RC announcements, we have been bad in the KOffice project in fixing the community stuff. I will see what I can do to do a better job in the next few months here before the actual release.\n\nThere was a long thread on the koffice-devel mailing list about our lack of web contents on koffice.org, and we know that we really really need a web person to handle our web.  So if you want to make a big difference for the KOffice project, please step forward and give us a hand with this.\n\n"
    author: "Inge Wallin"
  - subject: "Re: Feature List should be updated"
    date: 2009-01-23
    body: "I would be glad to help with web related stuff, im currently a university student and my current classload is easily manageable, fire me off an email and I will gladly help.\n\nme\ndot\nclose\nat gooogles mail"
    author: "Sebastian Alterman"
  - subject: "Re: Feature List should be updated"
    date: 2009-01-23
    body: "Wonderful!  We'll do that.\n\nBut you (and anybody else who wants to help) can also come to the #koffice IRC channel on irc.freenode.org. That's where the developers hang out and discuss things."
    author: "Inge Wallin"
---
In preparation for hopefully the final beta version of KOffice 2.0, the bug squad will host a Bug Krush Day next Sunday Jan 25th from approximately 10 am CET. Everybody who has an interest in making KOffice 2.0 usable should try to be present. Especially note that you do <em>not</em> have to have any programming experience to take part of this Krush. The purpose of the exercise is to test the applications and to try to find as many bugs as possible. You can find all relevant information about the day on <a href="http://techbase.kde.org/Contribute/Bugsquad/BugDays/KOfficeDay2">the Techbase page</a>.




<!--break-->
<p>The bug day will be coordinated in the #kde-bugs channel on freenode IRC. There will be present both old timers from the Bug Squad to help out beginners and give tutorials in testing and also KOffice developers that will help with testing particular features and even fix bugs in real time.</p>

<p>This bug day is an important event, because the result will determine which parts of KOffice that will be part of the final release.</p>

<p>Some distributions have taken the trouble to provide binaries of a more up to date KOffice version than the last 2.0 beta 5. This means that some of the already reported bugs of 2.0 beta 5 are already fixed, among them a very irritating bug in KWord that makes it crash when the user just types a few letters. The detail of the binary packages can be read on the techbase page linked above.</p>

<p>So please take the time on Sunday and test one or more of your favourite KOffice applications! </p>



