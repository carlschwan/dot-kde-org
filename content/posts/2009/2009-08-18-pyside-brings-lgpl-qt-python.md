---
title: "PySide Brings LGPL Qt to Python"
date:    2009-08-18
authors:
  - "mairas"
slug:    pyside-brings-lgpl-qt-python
comments:
  - subject: "Excellent!"
    date: 2009-08-18
    body: "Wow, excellent news! I've been wondering for some time whether there might be something like this coming with Ariya porting all his recent examples to PyQt...\r\n\r\nThanks a lot, Nokia!"
    author: "thorbenk"
  - subject: "What about existing ones?"
    date: 2009-08-18
    body: "Has there been any communication with existing PyQt people? I mean it's a bit ugly overstepping into someone else toes without first trying to contact them to fix the possible issues."
    author: "tsdgeos"
  - subject: "Re: What about existing ones?"
    date: 2009-08-18
    body: "They state in the <a href=\"http://www.pyside.org/faq/\">FAQ</a> that they tried to find a solution with Riverbank first:\r\n\r\n<blockquote>Nokia\u2019s initial research into Python bindings for Qt involved speaking with Riverbank Computing, the makers of PyQt. We had several discussions with them to see if it was possible to use PyQt to achieve our goals. Unfortunately, a common agreement could not be found , so in the end we decided to proceed with PySide.</blockquote>\r\n\r\n"
    author: "piquadrat"
  - subject: "PyQt License"
    date: 2009-08-18
    body: "Edit: better answer posted while I was writing mine. :)"
    author: "Pedahzur"
  - subject: "Big day for Pythonistas"
    date: 2009-08-19
    body: "Hello there,\r\n\r\nnot that PyQt wasn't fine - in fact I wrote a few GUI frontends with it - but this day signals that Nokia has decided to take the Python bindings as something they take very serious.\r\n\r\nAnd it is. This might well expand the Qt usership to even more mobile devices applications as well as companies that do GUIs for their tools. The lack of documentation may be addressed, and potentially the API will become even more usable.\r\n\r\nYours,\r\nKay\r\n\r\n"
    author: "kayonuk"
  - subject: "The name \"PySide\"?"
    date: 2009-08-21
    body: "Am I just not getting the pun? What does the name derive from?"
    author: "majewsky"
---
The <a href="http://www.pyside.org/about/">PySide team</a> is pleased to announce the first public release of <a href="http://www.pyside.org">PySide: Python for Qt</a>. PySide is a project providing an LGPL'd set of Python bindings for the <a href="http://qt.nokia.com/">Qt framework</a>.
<!--break-->
PySide already provides a full set of <a href="http://www.pyside.org/documentation/">Qt bindings</a> as well as automated <a href="http://www.pyside.org/home-binding/binding-generator/">binding generation tools.</a> Since the whole tool set has been made available, the team expects PySide to be valuable not only to Qt software developers, but to people willing to create Python bindings to any Qt-based library, or to any C++ library in general. Although based on a different technical approach, PySide will initially be API-compatible with existing Python bindings for Qt.

PySide is still a work in progress, and some work is still required to stabilize the codebase. This being said, the team believes it is already in a usable state, especially if an occasional rough edge and unpainted surface can be tolerated. Due to practical reasons, the initial development efforts have been focused on Linux, but the team hopes people to <a href="http://www.pyside.org/get-involved/">join in</a> porting the code to other platforms and to further develop the bindings and tools.