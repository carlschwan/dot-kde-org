---
title: "Amarok 2.0.1.1 \"Magellan\" Released, Includes Security Fix"
date:    2009-01-11
authors:
  - "lpintscher"
slug:    amarok-2011-magellan-released-includes-security-fix
comments:
  - subject: "Thanks Danny!!!"
    date: 2009-01-11
    body: "Oh wait, it is not Danny in this case... :P\n\nBut nevertheless you can Digg this story if you want: \n\nhttp://digg.com/software/Magellan_Amarok_2_0_1_1_released_including_security_fix"
    author: "Jon"
  - subject: "Re: Thanks Danny!!!"
    date: 2009-01-11
    body: "You can also spread the news here:\nhttp://www.reddit.com/r/linux/comments/7oy9f/amarok_2011_magellan_released_including_security/\nhttp://www.fsdaily.com/EndUser/Magellan_Amarok_2_0_1_1_released_including_security_fix"
    author: "Jure Repinc"
  - subject: "Re: Thanks Danny!!!"
    date: 2009-01-11
    body: "what an insulting post you have made.\n\nWhen has Danny posted releases for Amarok, never, but nevertheless, yeah that makes it sound even more insulting."
    author: "Anon"
  - subject: "Re: Thanks Danny!!!"
    date: 2009-01-11
    body: "I don't see how that one was insulting, i think it was even a tribute to danny, or a running joke because for most articles on the dot the first comment is \"thanks danny\"."
    author: "Beat Wolf"
  - subject: "Re: Thanks Danny!!!"
    date: 2009-01-11
    body: "yep, it was a joke ;] I forgot about adding [sarcasm] [/sarcasm] tags ;]\n\nIt was some kind of my tribute to Danny. At least I can do this. "
    author: "Jon"
  - subject: "Re: Thanks Danny!!!"
    date: 2009-01-12
    body: "Seems some people have their sarcasm detectors broken ;). It's amazing that you have to tag each joke as such, or some angry people will not get it and complain."
    author: "slacker"
  - subject: "Features"
    date: 2009-01-12
    body: "It's great to see filtering and queuing make their return (I'm rather shocked that pretty much no players support queuing... it's such a useful yet obvious feature!), those were the two main things that made me prefer 1 over 2.\n\nReally the only other feature I miss from 1 is MusicBrainz support (which is on the todo list!), but till the devs get around to adding it I can always just use their stand alone client."
    author: "Kit"
  - subject: "Re: Features"
    date: 2009-01-12
    body: "Queuing? Actually, I never understood the reason behind this \"feature\". This function is provided by (and is the primary reason for) the playlist mechanism, and I don't know WTH duplicate it?\n\nThen again, some people abuse playlists by keeping all their music in them, treating the playlist as sort of a \"poor man's music library\". With this usage pattern, queuing can be seen as a kludge-around for the problem of a lack of playlist to use for what it was meant to be used for.\n\nI'd be glad if someone could explain to me ONE legitimate reason/usage-scenario to use queuing.\n\nOFFTOPIC:\nFor me, THE most important feature that made Amarok THE best and most comfortable player is the powerful spreadsheet-like playlist taking almost entire window area - allowing me to see all the important information about playlist entries, while at the same time conserving vertical space and allowing me to see many entries without scrolling. It was shocking to me that the dev team pushed out a release without the main feature that made Amarok Amarok!\n\nAnyway, this will be fixed by the time I go KDE 4, so I might as well cut the rant ;)."
    author: "slacker"
  - subject: "Re: Features"
    date: 2009-01-12
    body: "Well, I like to select the music I want to listen to from the collection using various meachanisms and then put that to the playlist. That playlist is never the whole collection, or even near it. Then sometimes I'd really like to listen to one specific song next and my playlist is on random mode and I really don't want to first save the current playlist (though only needed if the playlist cannot be regenerated easily by doing a simple search on the collection), then add the song I want to listen to it, and after it has started playing or has finished playing, to get that old playlist back.\n\nI think that in Amarok 2 I could usually use dynamic playlists and just add the song I want to listen to next to the place of next song and so on, though."
    author: "Larso"
  - subject: "Re: Features"
    date: 2009-01-12
    body: "Good point. With a randomly played playlist, queuing makes sense for \"that one (or more) song(s) you wanna hear next\". Thanks for insight! Though still, dynamic playlists seem much more powerful and logical than random play.\n\nPersonally, I never liked the idea of a random playlist precisely for that reason. The playlist is supposed to be a list of songs to play next. It is a playLIST, not a playSET. The dynamic playlist is an excellent concept, cleanly fixing this problem. And another reason to love Amarok! :)"
    author: "slacker"
  - subject: "Re: Features"
    date: 2009-01-12
    body: "Hi I just added 4 new albums that wanted to listen to. Even if I select random play, amarok still plays them in order. I have to \"shuffle\" the playlist by hand to hear across all albums which is kind of stupid. Not only the menu entry is without of function I have simply work to much to get a simple random play \"feature\" work in amarok 2.\n\nThere is still much work left to get Amarok 2 back to were Amarok 1.4 was. Huge playlist over the complete collection is just another issue."
    author: "Thomas"
  - subject: "Re: Features"
    date: 2009-01-13
    body: "Is Amarok's random play mode set to Album or Track based? It sounds like it's set to albums, which'd do what you say is happening (or at least one would assume... I've never tried it :P)."
    author: "Kit"
  - subject: "Re: Features"
    date: 2009-01-12
    body: ">The dynamic playlist is an excellent concept, cleanly fixing this problem.\n\nUnfortunately, dynamic playlists have always been broken for me (at least in 1.x, haven't used 2.x much yet). I pretty much always have my playlist on shuffle so queuing is very useful for me (I generally only have 70-200 tracks in the playlist out of my 1400-1800 track library)."
    author: "Kit"
  - subject: "Re: Features"
    date: 2009-01-13
    body: "In what manner are they broken for you? I use dynamic playlists often, and I didn't come across any problems."
    author: "slacker"
  - subject: "Re: Features"
    date: 2009-01-13
    body: "<BLOCKQUOTE>The playlist is supposed to be a list of songs to play next. It is a playLIST, not a playSET.</BLOCKQUOTE>\n\nWho said it had to be an ordered list? Many lists are unordered. Please don't make assumptions and stick firmly to them on a rather ambiguous language ;)"
    author: "Gigaplex"
  - subject: "Re: Features"
    date: 2009-01-13
    body: "So much for the BLOCKQUOTE tag being supported..."
    author: "Gigaplex"
  - subject: "Re: Features"
    date: 2009-01-13
    body: "That's what that little \"Preview\" button is for ;)."
    author: "slacker"
  - subject: "Re: Features"
    date: 2009-01-13
    body: "UNORDERED list? In computer science the list is ALWAYS ordered, that's what makes it a list."
    author: "slacker"
  - subject: "Re: Features"
    date: 2009-01-12
    body: "I like excel view too, but in fact new Amarok play list view is very powerfull... we only need someone that makes a excel view again (or other better view).\n\nSee this posts, I think you will happy:\nhttp://amarok.kde.org/blog/archives/810-The-Old-style-Playlist-Is-Dead,-Long-Live-The-Old-style-Playlist.html\nhttp://amarok.kde.org/blog/archives/858-From-the-Post-2.0.0-Git-Vaults,-Part-2,-The-Playlist-Evolved.html\n\n\n"
    author: "Git"
  - subject: "Re: Features"
    date: 2009-01-12
    body: "I am working on making the playlist configurable, and I am quite confident that eventually this will lead to a playlist that is vastly more powerful than the excel view alone could ever be.\n\nThe main issue with the excel view is that it is overwhelming to a lot of \"non techies\", at least many of the people I have shown Amarok 1 to. So during the development of Amarok 2, we put the foundations in place for a very powerful new playlist. In Amarok 2.0.0 we decided to create a first version of something we think is a better default for many users. Admittedly in 2.0.0 this was quite limiting and missing a number of features that users of the 1.4.x have come to expect from the playlist. In 2.1.0 however the underlying frameworks are starting to flex their muscles as the links in the parent posts show. Eventually this should lead to a playlist that is very simple by default, but extremely powerful and flexible for those who need it, catering to both casual and power users.\n\nOr put in another way, we started out implementing the simple version in so we could get 2.0.0 released, and are now working our way towards the more advanced stuff."
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: Features"
    date: 2009-01-13
    body: "Queuing is really important for me!  I usually have quite a few playlists open in my playlist section - my 10 favourite albums at the time - and sometimes i'd like to listen to some songs in a particular order!\n\nI do agree with you that i miss the spreadsheet-like playlist.  Someone on here said its confusing, but i haven't come across a single person who beleives soo.. - Title, Artist, Album, Track, length??? I don't see anything confusing about that.  I really miss the filtering tool bar at the top!  It made life soo easy when scrolling through the playlist.  Most of my friends and family use amarok on their Ubuntu boxes and they absolutely love it but are not technical by any means...\n\nWe need to get the power back in Amarok 2.  Hopefully it'll happen soon... until then I'm gonna stick with Amarok 1.4 - I mean these are the features that made Amarok as popular as it is - about a quarter of my friends switch to Linux due to Amarok alone!!!!"
    author: "Zubin Parihar"
  - subject: "Re: Features"
    date: 2009-01-13
    body: "What i'm missing in amarok 2.0 is the possibility to manually add a firefly media server to the application. Amarok 1.4 could detect them automaticly when the server can be found, and if not, you could set the ip address and port. Amarok 2.0 only has the automatic detection and leaves the user behing in cold when the firefly server fails to broadcasts its network name over avahi (which is a common problem with firefly)"
    author: "ziggo"
  - subject: "Amarok on Windows"
    date: 2009-01-12
    body: "Is there any news on Amarok on Windows?\nHaven't heard anything in a while.\n\n(and before you ask: There is some computer I'm not allowed to put Linux on, so I have no choices. At the same time, I think Amarok is to KDE, what Firefox is to Open Source. Wildly embraced by the College crowd.)"
    author: "Mike"
  - subject: "Re: Amarok on Windows"
    date: 2009-01-12
    body: "Wow my English deteriorated severely. I think I should take this as a hit that it's bedtime now...\n\n*sleep tight KDE world*"
    author: "Mike"
  - subject: "Re: Amarok on Windows"
    date: 2009-01-12
    body: "hit ==> hint\n\n"
    author: "Mike"
  - subject: "Re: Amarok on Windows"
    date: 2009-01-12
    body: "I don't know the latest news, but usually this is the best source for information:\n\nhttp://windows.kde.org\n\n\nAlso, Patrick Spendrin yesterday wrote this blog that might be interesting:\n\nhttp://saroengels.blogspot.com/2009/01/pre-release-sprint.html\n"
    author: "Mark Kretschmann"
  - subject: "Re: Amarok on Windows"
    date: 2009-01-12
    body: "It's in the windows installer. Have not tested it very compleatly, but it works for netradio with script downloaded by the getnewstuff service. Still at 2.0, but updates will surface depending on the packagers workload.  "
    author: "Morty"
  - subject: "Re: Amarok on Windows"
    date: 2009-01-13
    body: "A few weeks ago I tested Amarok 2.something on Windows (probably should have paid more attention to the version numbers), and it didn't work very well for me. It wouldn't build a collection and had a few temporary freezes. Admittidly I didn't do much investigation so it might have been a configuration error (in Amarok, Phonon, or somewhere else), or I might had been a bit too zealous in pruning packages to save disk space ;)\n\nIIRC, at the end I had around 200-300 megs of disk space used for Amarok, which isn't bad... but at the same time rather large for a single application. Hopefully with time the Windows port will mature so I'll be able to ditch Winamp on Windows (which made me quite aware of how much I love Amarok)."
    author: "Kit"
  - subject: "Visual Changelog"
    date: 2009-01-12
    body: "Abhishek Rane again created a wonderful \"Visual Changelog\" for the new release.\n\n\nCheck it out:\n\nhttp://www.abhishekrane.com/2009/01/12/magellanamarok-2011-released-screenshot-changelog/\n\n"
    author: "Mark Kretschmann"
  - subject: "Version numbering"
    date: 2009-01-12
    body: "How does the version numbering for Amarok 2 work? \n\nI thought that it would be as usual, Amarok 2.X.Y, where X is the major version number which includes new features and Y the minor version number which means bugfix release. But there's an extra number there... and this release already includes new features.\n"
    author: "RandomOne"
  - subject: "Re: Version numbering"
    date: 2009-01-13
    body: "This release was supposed to be 2.0.1 but since we had to add a security fix and packagers already got 2.0.1 we had to release 2.0.1.1 which is 2.0.1 plus the security fix."
    author: "Lydia Pintscher"
  - subject: "Re: Version numbering"
    date: 2009-01-16
    body: "And then you fixed some typos and voila - 2.0.1.1.1?\n\nSeriously, why not 2.0.2?"
    author: "kolla"
  - subject: "amarok 2 on opensuse"
    date: 2009-01-13
    body: "Nice to see a new release so soon.\nToo bad, the packages for opensuse don't work, as they require kde 4.1.9xx, while the kde repositories of opensuse don't go higher than kde 4.1.8xx"
    author: "whatever noticed"
  - subject: "Re: amarok 2 on opensuse"
    date: 2009-01-13
    body: "I'm still using amarok 1.4 on openSUSE, as there is no Dutch version available for Amarok 2.0 in the openSUSE language package, although Amarok 2.0 is in Dutch on my Mandriva system."
    author: "chriss"
  - subject: "Re: amarok 2 on opensuse"
    date: 2009-01-13
    body: "Why not use the 2.0.1.1 package which has Dutch translations?\n\n> although Amarok 2.0 is in Dutch on my Mandriva system\n\nThen they added it manually. The Amarok 2.0 release tarball did miss Dutch."
    author: "binner"
  - subject: "Re: amarok 2 on opensuse"
    date: 2009-01-13
    body: ">Why not use the 2.0.1.1 package which has Dutch translations?\n\nIt does?\nGreat!!"
    author: "chriss"
  - subject: "Re: amarok 2 on opensuse"
    date: 2009-01-13
    body: "I noticed this, too.\nThough this is OT, has anybody an idea why the - otherwise excellent!!! - openSUSE KDE4 repositories sometimes for weeks contains packages which require not yet available KDE4 base packages?\n\nConcerning Amarok2.0.1.1: I'd really  like to try it out, as I still hope that it will become usable. Reading the feature list, it looks like lot of user's complaints have at least been recognized as legitimate ;)."
    author: "Anon"
  - subject: "Re: amarok 2 on opensuse"
    date: 2009-01-13
    body: "You can install Amarok 2.0.1.1 using OpenSUSE repositories, but you have to use the \"factory\" repository instead of \"unstable\". \n\nIf you're planning on using KDE 4.2 you should migrate to factory (which now contains more recent snapshots than unstable), in less than month unstable will start having KDE 4.3 snapshots, not to mention that it won't have the RC 1 and Final of KDE 4.2"
    author: "Luis"
  - subject: "Re: amarok 2 on opensuse"
    date: 2009-01-13
    body: "Ah!! Thanks for the tip!!"
    author: "Jad"
  - subject: "Re: amarok 2 on opensuse"
    date: 2009-01-13
    body: "Nobody should use KDE:KDE4:UNSTABLE:* repositories atm: http://kdedevelopers.org/node/3825"
    author: "binner"
  - subject: "Re: amarok 2 on opensuse"
    date: 2009-01-13
    body: "Hi Stephan, \n\nYou know it would be really cool if there was a notification system associated with each repository. Have you guys ever thought about that?"
    author: "Jad"
  - subject: "local collection and controls"
    date: 2009-01-13
    body: "i won't use amarok 2 because of two things:\n\n1. local collection doesn't understand compilations. a can't get to listen to compilations and collection is overcrwoded with artists because of that.\n2. controls and progress bar are the ugliest and most unusable things in the world. amarok had much prettier controls and progress bar in alphas"
    author: "j."
  - subject: "Stop after current track not working"
    date: 2009-01-14
    body: "Does it work for you? At me (on openSUSE) there is the popup menu item but it does nothing."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Stop after current track not working"
    date: 2009-01-18
    body: "I just tried it (on Debian) and it went right on to play the next track. I suspect you have to actually place songs into the queue for it to work. Which sucks---I used to use \"Stop after current track\" all the time. Not to say Amarok 2 isn't cool, that's just one particular feature whose absence is still irritating me."
    author: "Brendon Higgins"
  - subject: "Real Download Site"
    date: 2009-02-22
    body: "Is there a real site to download a windows installer.  \r\n\r\nThe KDE windows crapstaller is not a means to release this program or any other program, just a way to make sure they never get popular under windows...  The mac version has a dmg for the installation, why is there no direct msi or exe to do the install."
    author: "mcspam44"
---
The Amarok team is pleased to announce the release of Magellan, Amarok 2.0.1.1. It includes some of the features users have been waiting for, bugfixes as well as a security fix. Filtering and searching in the playlist is possible again and track queuing as well as "stop after current track" are back. A lot of improvements have been made to MTP device handling and the scripted service API. Mac users can now enjoy Growl support.
Read more in the <a href="http://amarok.kde.org/en/releases/2.0.1.1">release announcement</a> and try it today.

<!--break-->
