---
title: "Matthias Ettrich Receives German Federal Cross of Merit"
date:    2009-11-06
authors:
  - "mirko"
slug:    matthias-ettrich-receives-german-federal-cross-merit
comments:
  - subject: "Awesome!"
    date: 2009-11-07
    body: "That is awesome on so many levels. Congratulations Matthias!"
    author: "nhnFreespirit"
  - subject: "Matthias Ettrich Receives German Federal Cross of Merit"
    date: 2009-11-07
    body: "A well deserved medal. But it is not only for the Email. To his attitude to co\u00f6perate with 'rivals' like the Gnome Desktop as well.\r\nIn 1999  I had the privelige to negotiate the co\u00f6peration of KDE and GNOME desktops with blanco card from Ettrich and the KDE-team. I went in March 1999 to the first Linux World Conference in the U.S.A.\r\nI spoke with Mr. Shuttleworth and the Gnome-Team and thanks to the full confidence I could speak openly and convince Mark Schuttleworth of the advantages to work together instead of working against each other. I must admit that also Mark Shuttleworth played a big role  (my thanks to him too) to get both groups helping each other making a better Linux Desktop and ban out personal financial gain. (Ximian for example).\r\n\r\nA Linux Pioneer from the first hour : Patrick Op de Beeck"
    author: "alug"
  - subject: "surprise"
    date: 2009-11-07
    body: "my biggest surprise is that Germany actually has such an award; says a lot about the government there that open source achievements are recognized and awarded in public.\r\n\r\nCongrats to all involved :)"
    author: "zander"
  - subject: "wow... congrats!"
    date: 2009-11-07
    body: "nowadays KDE is on millions of desktops. KDE is a pleasure to use for computer enthusiasts as well as for the average user. It is where innovations happen in an open way. Users, developers, everybody can virtually see how the project is paving the way into the (software) future. With tons of ideas and real-world projects (just to name Nepomuk, Plasma and Akonadi...) Here's now one of spearheads of technology. And there's no need for any marketing buzz (well, mostly ;-) or unfair tricks. I really like the path it all took and am looking forward to KDE 4.4!\r\n\r\n"
    author: "thomas"
  - subject: "Congratulations to Matthias"
    date: 2009-11-07
    body: "It is indeed wonderful that Germany honours one of its own open source innovators.\r\nKDE is a great desktop, and getting better and better.\r\nIt indeed helps the common good, helps the spread of knowledge and productivity as well as being great fun and entertainment.\r\nWhat more could we ask for?\r\nMay KDE go from strength to strength!\r\nCongratulations again\r\nRegards\r\nEl Zorro "
    author: "El Zorro"
  - subject: "Congratulation!"
    date: 2009-11-07
    body: "Congratulation Matthias! You have really deserve this award.\r\n\r\nWas this also in the German media, e.g. TV news? Sadly I wasn't able to watch any news yesterday."
    author: "BeS"
  - subject: "Congratulations!"
    date: 2009-11-07
    body: "well deserved :)\r\n\r\nNow I'm dying to figure out what was the answer to your only question? :)"
    author: "harry1701"
  - subject: "congratulations!"
    date: 2009-11-07
    body: "you earned it :)"
    author: "mxttie"
  - subject: "The \"Federal Cross of Merit\""
    date: 2009-11-07
    body: "The \"Federal Cross of Merit\" was awarded about 2400 times in 2007, in some other years more often. The award is too meaningless for TV news."
    author: "kleintwoers"
  - subject: "Congratulation!"
    date: 2009-11-07
    body: "That's a damn high honor - and I'm glad to see it going to a free software activist for the first time! "
    author: "ArneBab"
  - subject: "A wonderful news"
    date: 2009-11-07
    body: "I just read it on heise.de. It's so nice to read that his hard work, dedication and contribution to the best open source DE available is recognized and appreciated on such a high level.\r\n\r\nGratulation Herr Ettrich. Sie haben das mehr als verdient. Wir sind alle sehr stolz auf Sie als der Vater von KDE."
    author: "Bobby"
  - subject: "Sincere congratulations"
    date: 2009-11-07
    body: "My sincere congratulations Matthias, you made the world a better place."
    author: "FSFmember"
  - subject: "Congratulations!"
    date: 2009-11-07
    body: "I can just second all the congratulations.\r\nCongratulations! :)"
    author: "nailuj24"
  - subject: "Burying harmless comments"
    date: 2009-11-08
    body: "Sometimes I just can't understand what goes on here. For example, somebody buried harry1701 for a very harmless comment or for me expressing how happy I was to see Matthias receiving the Bundesverdienstkreuz. Oh, maybe it's because of the one German sentence that I wrote. Well KDE was founded by a German who speaks German so I think it was fitting. Apart from that I am not German but do appreciate and admire their technological skills. Or was it because I said that KDE is the best open source DE available? Well if anyone on this forum thinks otherwise then he/she is in the wrong place."
    author: "Bobby"
  - subject: "aMSN!"
    date: 2009-11-09
    body: "I re-read the original Matthias Ettrich mail that started all of this revolution, and what's surprises me is the size of his success. How many Tcl/Tk apps do you see in your 2009 Linux Desktop? I think aMSN is the only one. It has been that way, at least, for the last 5 years.\r\n\r\nAnd even aMSN is moving to Qt.\r\n\r\nDon't ever forget aMSN: without it, Tcl/Tk would have disappeared a long time ago from our desktops, and KDE would have considered its first goal achieved (just like Ubuntu #1 bug, and other dreams that, now, look like plain utopia)\r\n\r\nBTW, the second goal, the Qt standardization, will require lots of blood and a turning around from all distros. But the foundations are laid; KDE ~4.6, ~4.7 is going to be 300x what GNOME is right now, and no incremental release is going to change that."
    author: "Alejandro Nova"
  - subject: "Simple the best"
    date: 2009-11-09
    body: "\"BTW, the second goal, the Qt standardization, will require lots of blood and a turning around from all distros. But the foundations are laid; KDE ~4.6, ~4.7 is going to be 300x what GNOME is right now, and no incremental release is going to change that.\"\r\n\r\nA man after my own heart. I tried to love Gnome but it was like marrying a woman and then trying to fall in love with her. KDE on the other hand was love at first sight. I have looked at others but I stick to her because of her LOOK and FEEL. It's truely a great Desktop Environment."
    author: "Bobby"
  - subject: "congratulations, matthias!"
    date: 2009-11-09
    body: "from a former troll, hal."
    author: "BrightOrange"
  - subject: "\"How many Tcl/Tk apps do you see in your 2009 Linux Desktop?\""
    date: 2009-11-10
    body: "There is gitk. And I think that the CrossOver GUI is also written in Tcl/Tk."
    author: "majewsky"
---

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/matthias.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/matthias-small.png" width="250" height="333"/></a><br />Matthias Ettrich with his shiny new medal.</div>

Today at 4pm CET at the Center for Economics, Technology and Women’s Issues at the Berlin Senate, KDE founder <a href="http://behindkde.org/people/ettrich/">Matthias Ettrich</a> was decorated with the German Federal Cross of Merit for his contributions to Free Software. Matthias was awarded the medal in recognition of his work spurring innovation and spreading knowledge for the common good. The award was presented by state secretary Almuth Nehring-Venus. She mentioned that not only is Matthias among the younger recipients of the award but also that this is the first award ceremony where young children of the family were present. Also attending was Eberhard Gienger, member of the Bundestag for the county Matthias grew up in.

Matthias started the KDE project on October 14, 1996 with his <a href="http://www.kde.org/announcements/announcement.php">now infamous email</a>. The project grew in leaps and bounds, creating one of the most active and innovative Free Software communities today, and helped redefine the expectations users have from free software in general. Indirectly, the KDE community influenced the development of the whole Free Software ecosystem, shifting the focus from Unix as a server system to Unix as a powerful end-user desktop, pushing the boundaries until today. The advance of Free Software depends so much on the interactions between the underlying infrastructure and the desktop that we think the award is well-deserved.

The Federal Cross of Merit is both the most prestigious as well as the only general decoration awarded by the Federal Republic of Germany. It is awarded by the Federal President for outstanding achievements in the political, economic, cultural, and other fields. To Matthias, the award was such a surprise that he first considered the notification to be spam... After being decorated, his only question was: "So, now what is next?" Judging from his history, we think he'll come up with something.

Congratulations, Matthias!