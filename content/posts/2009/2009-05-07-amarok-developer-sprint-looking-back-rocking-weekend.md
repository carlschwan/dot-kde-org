---
title: "Amarok Developer Sprint - Looking Back At A Rocking Weekend"
date:    2009-05-07
authors:
  - "nightrose"
slug:    amarok-developer-sprint-looking-back-rocking-weekend
comments:
  - subject: "please more info !"
    date: 2009-05-08
    body: "Great news ! \r\nI think the Amarok team is working on the right direction, but could you please give us some more information on what changes will be done to 2.1 and 2.2 ? \r\nWhich are the \"several significant design improvements\" we\u00b4ll have to see? what are the plans that could be achieved ? and what options have been discussed at the meeting ?\r\n\r\nThanks for your great work !\r\n"
    author: "lbayle"
  - subject: "For Amarok 2.1.0 the changes"
    date: 2009-05-08
    body: "For Amarok 2.1.0 the changes are all pretty much in place (a few suggestions from the sprint have managed to sneak in though. you can see the changelog for 2.1.0 beta 1 and the upcoming 2.1.0 Beta 2 (which might become rc1 depending on whether we manage to fix a particularly nasty bug before we tag it) here: http://websvn.kde.org/*checkout*/trunk/extragear/multimedia/amarok/ChangeLog\r\n\r\n\r\nFor 2.1.1 a bunch of minor changes discussed at the sprint, which are slightly more time consuming or that require string changes (which we cannot do for 2.1.0 as we are deep in string freeze) will be implemented, along with general bugfixes. We don't have any concrete plans for 2.1.2 yet, but likely this will be a pure bugfix release.\r\n\r\nFor 2.2.0 we already have quite a few things in store, like the audiocd collection (http://amarok.kde.org/blog/archives/1001-From-the-Post-2.1.0-Git-Vaults,-Part-I-Cd-Collection.html) and many other cool features that are sitting in developers local git branches until trunk opens up for 2.2 development. For 2.2 we will also start implementing some of the larger items decided on at the sprint, many of them relating to the overall look at fell of Amarok. \r\n\r\nThe exact list of things discussed at the sprint is on our wiki, I am not going to post a direct link as it is really intended mainly for developers and not for public discussion (we will likely present some of the ideas in a format more suitable for gathering feedback later) but if your really want to find it, it should not be that hard! ;-)"
    author: "nhnFreespirit"
  - subject: "Thank you"
    date: 2009-05-10
    body: "Well done and a big thank you for all your hard work!\r\n\r\nI use Amarok every day as it's a real pleasure to use. \r\n\r\nMusic brings me joy and Amarok helps deliver that joy to my ears."
    author: "peter_dewitt"
  - subject: "Needs of the users"
    date: 2009-05-12
    body: "\"The ultimate goal of the meeting was to focus on the needs of the users and the core essentials of a music player.\"\r\n\r\nSo please fix Amarok 1.4 database import. Upgrading from a previous version (which is the bulk of all Amarok2 users I would imagine) is kinda basic. Please fix it!"
    author: "NabLa"
---
Since we began seriously working on Amarok 2 more than two years ago, Amarok has come a very long way. Looking back at screenshots of some of the very early incarnations of Amarok 2 can be almost frightening, and it is clear that Amarok 2 has changed much since then. While the Amarok 2.0 release was in no way perfect, it was more importantly a solid foundation to work on, and Amarok 2.1 is already shaping up to be a much more well rounded music manager.

Given the sheer amount of work that has gone into Amarok 2, and the pace at which development has progressed, it is only natural that not all design decisions, APIs and code are in optimal shape after 2 years. So to take a step back, take a deep breath, look at the big picture, review API's and draw up the big picture plans for the future GUI and features of Amarok 2, we descended upon the KDAB offices in Berlin for the first official Amarok developer sprint.

Topics covered at the sprint included API reviews of central parts of Amarok, discussions on PR and marketing and the big "how to improve the Amarok 2 UI and usability" discussion.

The ultimate goal of the meeting was to focus on the needs of the users and the core essentials of a music player. After 4 days at the generously offered KDAB offices, we feel the results were a resounding success. Several goals have been set for both the short and the long term. Changes in both functionality and look will be introduced in subtle increments to lead Amarok to places one could only dream of. Beginning from 2.1.0, users will begin to see the fruit of the sprint and as early as 2.2 several significant design improvements will be integrated.

This entire developer sprint, along with the resulting code, ideas, and plans could not have been achieved without the help of many contributors. For this reason we would like to send out some big thanks to:
<ul>
<li>Our users. Thank you for helping with your honest and constructive criticisms to help us reach our goal: giving you the best music player out there.</li>
<li>The usability experts. Thank you for the constructive bi-directional feedback you gave us as well as the results of your studies. These proved a great help in lining up feedback we have received and linking cause and effect to changes we have made.</li>
<li>The KDAB folks. Thank you for opening your doors and letting us descend upon your offices for our sprint.</li>
</ul>

We look forward to your reactions and feedback on the soon to be released 2.1.

<center>
<a href="http://dot.kde.org/sites/dot.kde.org/files/amarokdevsprint.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/amarokdevsprintt.jpg"></a>
</center>