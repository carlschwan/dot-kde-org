---
title: "KDE Commit-Digest for 8th February 2009"
date:    2009-02-23
authors:
  - "dannya"
slug:    kde-commit-digest-8th-february-2009
comments:
  - subject: "Nice, filelight for kde 4!"
    date: 2009-02-23
    body: "Nice to see filelight coming to kde 4. It is a great application, and one of the handful of kde 3 applications that still keep kde 3 on my desktop :)\r\n<p/>\r\nThe others I miss are kile, k3b, konversation and to a lesser extent pdfedit and hplip.\r\n<p/>\r\nAll of these seem to be close in proving a qt4/kde4 version, so kudos to all devs."
    author: "ianjo"
  - subject: "What is \"Kamala\"?"
    date: 2009-02-24
    body: "\"A Kross scripting backend, and Postgres support is added to Kamala.\"\r\n\r\nLike I said, what is Kamala? I'm asking because in Finnish \"kamala\" means \"terrible\", \"awful\" etc ;)"
    author: "Janne"
  - subject: "Re: Kamala"
    date: 2009-02-24
    body: "I plan to ask for an introduction text for an upcoming Digest.\r\n<br><br>\r\nDanny"
    author: "dannya"
  - subject: "Spanish translation..."
    date: 2009-02-24
    body: "...for this Commit is here:\r\nhttp://www.kdehispano.es/?q=content/resumen-de-los-avances-de-kde-del-8-de-febrero-de-2008\r\n\r\nAlso other Commits:\r\nhttp://www.kdehispano.es/?q=category/etiquetas/resumen-dev\r\n\r\n[Edit: I don't know how to force breaklines...]"
    author: "tunic"
  - subject: "[Edit: I don't know how to force breaklines...] "
    date: 2009-02-24
    body: "FYI You can use normal html (br and p tags)."
    author: "ianjo"
  - subject: "Impressive work, finding that"
    date: 2009-02-27
    body: "Impressive work, finding that security flaw: even more impressive is the speed at which the KDE team responded. Well done!"
    author: "madman"
  - subject: "Lightweight player"
    date: 2009-02-27
    body: "I've been installing openSUSE for some coworkers with really old computers without a whole lot of memory.  Between these older computers, and the rise of netbooks, I'm constantly looking for apps and setups that don't require a whole lot of memory.\r\n\r\nI love Amarok and I'm excited to see Amarok 2 coming along.  However, for situations where memory is a concern, it would be nice to see a simple lightweight player akin to Winamp.  A pure player could probably be whipped up as a Plasmoid with Phonon I imagine.  Heck, one probably exists somewhere (not that I've seen one in openSUSE).\r\n\r\nHowever, the ability to keep track of a collection, sort by artist/album/title, and edit tags is pretty important for many users.  I'm curious if there exists a lightweight player with a simple db backend for collections that doesn't require a full instance of MySQL, or if someone could develop a simple player.\r\n\r\nNot only are resources an issue, but on netbooks, screen resolution is also key.  So a fairly minimal interface would be key."
    author: "enderandrew"
  - subject: "MECC remakes/clones for KDE-Edu"
    date: 2009-02-27
    body: "As a father, I'm looking for educational software for my daughter.  She is only 3, so I do have some time before she really starts using the computer, but she loves playing Ktuberling with me.<p>I really loved playing the various MECC games in school as a kid growing up like Oregon Trail, Number Munchers, and Lemonade Stand.  MECC went out of business years ago.  Honestly, I'm a little shocked these classics haven't been remade, or cloned as FOSS apps.  I think there are some Lemonade Stand clones, but I think the MECC classics would make for great inspiration for KDE-Edu games.<p>MECC developed The Secret Island of Dr. Quandary, Oregon Trial, Yukon Trail, Amazon Trail, Odell Lake, Number Munchers, Lemonade Stand, Spellevator, Storybook Weaver, Freedom! and DinoPark Tycoon.  All of these would make great KDE-Edu games."
    author: "enderandrew"
  - subject: "KControl"
    date: 2009-02-28
    body: "\"Work on porting KControl\"\r\n\\o/\r\n\r\nI suppose it means the tree view in SystemSettings but even that is great \\o/"
    author: "Pliskin"
  - subject: "KControl"
    date: 2009-02-28
    body: "\"Work on porting KControl\"\r\n\\o/\r\n\r\nI suppose it means the tree view in SystemSettings but even that is great \\o/"
    author: "Pliskin"
  - subject: "No, not really"
    date: 2009-03-01
    body: "Let's not go overboard.  The issue has been discussed for 7 years or so, back in 2002 or so."
    author: "JohnFlux"
  - subject: "yep  filelight!"
    date: 2009-05-24
    body: "for ones who dont know what filelight is - its like a colorful gorgeous pie-chart, but the segments nest, allowing you to easily see which files are taking up all your space. and I love it I am glad its in KDE4.0 <span style=\"color:white\">worldclass for models cloth shopping  <a href=\"http://fashion4us.com\" style=\"color:white\">hk fashion</a> garments tops. Great many movies <a href=\"http://phimhanquoc.us\" style=\"color:white\">phimhanquoc</a> films world.</span>"
    author: "bennicehar"
---
In <a href="http://commit-digest.org/issues/2009-02-08/">this week's KDE Commit-Digest</a>: <a href="http://amarok.kde.org/">Amarok</a> 2 reached cue sheet feature parity with Amarok 1.4, a first draft of a playlist layout config editor, and various other improvements across Amarok. C# bindings for <a href="http://phonon.kde.org/">Phonon</a>. Beginnings of an effort to deliver a working debugger for <a href="http://www.kdevelop.org/">KDevelop</a> 4.0. Write support added to kio_svn. A <a href="http://kross.dipe.org/">Kross</a> scripting backend, and Postgres support is added to Kamala. An initial D-Bus interface added to parts of <a href="http://edu.kde.org/marble/">Marble</a>. Further development work on KHotKeys and the <a href="http://commit-digest.org/issues/2009-02-01/#1">LionMail</a> widget. Text-to-speech support added to the Translatoid Plasmoid, and a new <a href="http://plasma.kde.org/">Plasma</a> widget: Fortunoid (fortune quotes). Continued improvements to Sonnet and KWin. Progress on the Virtuoso backend for <a href="http://nepomuk.kde.org/">NEPOMUK</a>. Basic support for importing for SVG patterns in <a href="http://koffice.kde.org/karbon/">Karbon</a>. Work on porting KControl and <a href="http://konversation.kde.org/">Konversation</a> to KDE 4. Initial import of the "Peg-E" game, a "Google data Akonadi resource", KDE 4 versions of the SLP KIO-slave and Filelight, and "plasma2jolie". The Nuvola iconset moves back to kdeartwork from an unmaintained state. <a href="http://eigen.tuxfamily.org/">Eigen</a> 2.0.0 is tagged for release.

Also in this Digest, Michael Pyne tells the story of a recent potential security issue in KDE, and steps taken to remedy it. <a href="http://commit-digest.org/issues/2009-02-08/">Read the rest of the Digest here</a>.
<!--break-->
