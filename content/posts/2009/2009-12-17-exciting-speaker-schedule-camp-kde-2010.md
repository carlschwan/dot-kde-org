---
title: "Exciting Speaker Schedule for Camp KDE 2010"
date:    2009-12-17
authors:
  - "Justin Kirby"
slug:    exciting-speaker-schedule-camp-kde-2010
comments:
  - subject: "Frank karlitschek will announce open-desktop openrc?"
    date: 2009-12-17
    body: "Something that upset me each time I read \"Frank Karlitschek\" is to think that open-desktop is not open, even more, now kde is full integrated with this service (attica, get hot new stuff etc) so we're depending on closed-src software to get a full set of features, and even more^2 kde-core developers seems to approve it. Something with less importance is the fact that I never saw Frank running KDE on his computer.\r\n\r\nSo Frank, opensrc it and create a community around open-desktop, one of those communities you've said that you know how to create.\r\n\r\nCheers."
    author: "manag"
  - subject: "Dear anonymous poster,\nI"
    date: 2009-12-17
    body: "Dear anonymous poster,\r\n\r\nI would love to discuss this with you in person. But it seems that you want to hide your real name.\r\nThere are reasons why a part of the website is opensource and a part isn\u00b4t. Everything related attica and ghns will be AGPL soon by the way.\r\nI\u00b4m happy to explain the details to you if you ask me directly.\r\n\r\nBy the way. I\u00b4m using KDE daily for over 10 years now. And you?\r\n\r\nCheers\r\nFrank"
    author: "FrankKarlitschek"
  - subject: "For the sake of other"
    date: 2009-12-18
    body: "For the sake of other comments, I would like to state the schedule looks impressive indeed! :) Too bad I can't attend both KDE conferences (that would be a bit too much). I'll enjoy the video's after the conference, and wish you a lot of fun!"
    author: "vdboor"
  - subject: "To be honest I can understand"
    date: 2009-12-19
    body: "To be honest I can understand why his post is anonymous given the general attitude in the community when it comes to demonising any critical voices. Just look at the post's negative score. \r\n\r\nSo questioning you using KDE may have been taking it a bit to far and personalising the matter, which is unfortunate. But the concerns about the closed source nature of some important software being used by the community apparently are fair or at least not completely unfounded.\r\n\r\nCould you elaborate more on the issue here instead of leaving it for private email correspondence? Or is there some reason why this has to be kept private. \r\n\r\nThanks for all your dedication, and keep up the good work!\r\n\r\n"
    author: "jadrian"
  - subject: "\"To be honest I can"
    date: 2009-12-21
    body: "\"To be honest I can understand why his post is anonymous given the general attitude in the community when it comes to demonising any critical voices. Just look at the post's negative score.\"\r\n\r\nHave you considered that the post's negative score might be due to its rambling, hardly coherent and in many cases provably wrong attacks on Frank and others?"
    author: "jefferai"
---
The schedule for Camp KDE 2010 has just been published! The event will be taking place in sunny San Diego at the University of California, San Diego from January 15th-22nd, 2010. Registration is free and there are still some space left for attendees so <a href=https://campkde2010-registration.kde.org/>sign up</a> right away if you're interested in attending.
<!--break-->
This year's event will feature two keynote presentations. On Saturday Philip Bourne will discuss open access to data. Bourne is a well known contributor to the open-source bioinformatics field and leading advocate of open access to data. He is also the founder of <a href="http://www.scivee.tv/">SciVee</a>, which serves as a web based resource for scientists to make their research visible to their community.

Sunday's keynote will feature Frank Karlitschek who will be discussing KDE and the Cloud. Frank is a member of the KDE e.V. Board and founder of <a href="http://hive01.com/">hive01</a>, a company dedicated to building social networks.

The keynotes will be a part of 3 days of exciting presentations starting on Saturday and ending on Monday. These talks will be given by a variety of KDE community members and will cover a wide array of interesting topics. Later in the week we'll take time out to see some local San Diego sites and will round out the conference with two days jam packed with BoFs, hacking, and Qt development training. Be sure to check out the full schedule at <a href="http://camp.kde.org/schedule.html">the camp KDE website</a>.