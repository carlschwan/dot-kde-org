---
title: "Cooperation During the Gran Canaria Desktop Summit"
date:    2009-07-09
authors:
  - "jospoortvliet"
slug:    cooperation-during-gran-canaria-desktop-summit
comments:
  - subject: "CouchDB is awesome"
    date: 2009-07-10
    body: "I hope it will get used by KDE and Gnome, that would be really, well, awesome =)"
    author: "patcito"
  - subject: "Skype?"
    date: 2009-07-10
    body: "All the talkting about FOSS solutions an then use Skype? Does QuteCom (=OpenWengo) no longer work?\r\n\r\nDespite that: Great article and a great event. Be sure to spread the word about cross desktop collaboration: http://digg.com/linux_unix/Cooperation_During_the_Gran_Canaria_Desktop_Summit"
    author: "KAMiKAZOW"
  - subject: "Hmmm"
    date: 2009-07-10
    body: "I wish I wasnt seeing a laptop with VISTA installed in one of the developers machine (in photo) ;)."
    author: "BONZI200X"
  - subject: "no s"
    date: 2009-07-10
    body: "Jos, I love you, but can you please stop misspelling my name every time? :) Adam, no s. (I guess Steve might have gotten it wrong as well.) Thanks,\r\n\r\nTill"
    author: "till"
  - subject: "whoops, did it again,"
    date: 2009-07-10
    body: "whoops, did it again, sorry... I pronounce it properly but still write it the wrong way. Unfortunately I can't fix the article, the dot thinks I'm a big spammer.... :("
    author: "jospoortvliet"
  - subject: "Portability..."
    date: 2009-07-10
    body: "Well, KDE4 runs on all major operating systems, including Windows. So of course there are developer machines with Windows (and hopefully KDE4)\r\n\r\nAlex\r\n"
    author: "aneundorf"
---
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 300px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/DSCN3902.JPG" width="300" height="225" /><br />
The New Conference Location
</div>

At the Gran Canaria Desktop Summit much cross-desktop work has been done. The days we have are being used for the <a href="http://www.grancanariadesktopsummit.org/node/265">Cross Desktop Tracks</a> and during the talks there are KDE and Gnome developers mingling everywhere.  Cross desktop sessions included bug triage, metadata sharing, instant messaging and sharing personal data cross-desktop with CouchDB. Read more about the results!
<!--break-->
Having people from both desktops in one place is seen as valuable to most participants. Some of the meetings taking place have been about topics important to both desktops, like common infrastructure. A good example of this is the Freedesktop.org meeting where the current situation and the issues surrounding the process of collaboration was  discussed. Unfortunately due to the closure of the room they were in the first meeting did not last long, but it was later continued. A summary has been posted to the XDG mailing list, and the <a href="http://lists.freedesktop.org/archives/xdg/2009-July/010729.html">resulting discussion can be read</a>. In short, it seems at least the participants to the meeting agreed that a bit more structure is needed. No taking of Freedesktop.org namespaces unless at least KDE and GNOME agree, and a list of specs which have been and have not been blessed. This certainly does not mean complicated procedures or bureaucracy, just an email to the XDG mailing list to ask for a 'go' generally would work to get the process started. If the person or team proposing the standard is open to suggestions and collaboration there is no reason it would have to take long to get something accepted. Yet agreeing on some rules will make sure there will not be anymore 'standards' unsuitable for both major desktops and more work can be shared.

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 350px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/DSCN3913.JPG" width="350" height="198" /><br />
Collabora discuss Telepathy with KDE
</div>

Sharing infrastructure is clearly on the rise. As Maemo will move to Qt as its main toolkit, the other components of its infrastructure will have to work more closely with Qt. Many of those components have been cross-desktop for a while now - like D-Bus and <a href="http://avahi.org/">Avahi</a>. Others, like <a href="http://projects.gnome.org/tracker/">Tracker</a>, are in the process of removing their dependencies on GTK technology and becoming part of the Free Desktop platform layer. Since several Tracker developers are here, a meeting was set up for people working on desktop search, indexing and monitoring. People from <a href="http://nepomuk.semanticdesktop.org/">Nepomuk</a> (contextual linking), Tracker (indexing), <a href="http://strigi.sourceforge.net/">Strigi</a> (indexing) and <a href="https://launchpad.net/zeitgeist">Zeitgeist</a> (event logging) spoke about further collaboration. Zeitgeist will use Tracker for its data storage and Tracker will start using the Strigi analyzer infrastructure. The ontologies (definitions for data storage) developed for Nepomuk are already being adopted by Tracker. A new ontology will be developed for use with Zeitgeist in cooperation with the Tracker and Nepomuk developers. These developers are very enthusiastic about these collaborative developments, and rightly so!

Developers from the Open Source Consultancy company <a href="http://www.collabora.co.uk">Collabora</a> held a session with KDE developers to discuss their work in bringing the <a href="http://telepathy.freedesktop.org/">Telepathy</a> communication mechanism to KDE. Work has restarted on <a href="http://kcall.basyskom.org/">KCall</a> and more applications are expected to integrate it in future.

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 400px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/DSCN3916.JPG" width="400" height="264" /><br />
KDE Bug Squad held a joint session with GNOME's Bug Team
</div>

Cooperation was also started in the area of bug fighting. Members of the GNOME and KDE Bugsquads got together to share their secret experiences and ideas on killing these ugly animals. Discussed where things like the formal and informal policies of both teams, and the experiences in various areas. The KDE bugsquad is very good at recruitment and organisation, while the GNOME team has a good quality control and a decent process of teaching new members the ropes. Both bugsquads suffer from the same curse-and-blessing: there is a very high conversion rate of people joining the bugsquads toward development. In the KDE team some quick surveys have shown that most people joining the bugsquad were new KDE contributors. After being welcomed in the open arms of the KDE community through the bug squad they quickly moved on towards SVN accounts, starting with simple bugfixing and then moving on to maintaining full applications. But everyone moving on to KDE development, how much appreciated, leaves a gap in the bugsquad ranks. This creates a challenge for the remaining members - they quickly have to find new members, and get them to help organising the bugdays to train new recruits in the ways of bughunting. Despite the downside to this, it also creates a very interesting and fast-paced environment where people turn up, contribute a great deal (don't underestimate the incredible value of the bugreporting work!) and move on to become highly appreciated developers in our great Free Software community.

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 350px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/DSCN3914.JPG" width="350" height="269" /><br />
Couch DB talks to both Evolution and Akonadi
</div>
Steve Alexander gave a talk on CouchDB, the database being used for Ubuntu One to share everything from addresses 
 to bookmarks between computers.  He annonced that Till Adam had created an Akonadi resource for CouchDB and demonstrated the power by entering an address in Evolution then reading it in Akonadi.  Personal data can now be shared across computers and across desktops.

Smaller get-togethers which still have to bear fruit would be the talks between Maemo, KOffice and OpenOffice developers about sharing more infrastructure and some initial talks among multimedia developers. There have been cooperative efforts between GNOME and KDE marketing teams, while the KDE e.V. and GNOME Foundation boards had a meeting to talk about this conference and what would happen next year, and other possibilities of working together.

It is not only cross-desktop cooperation which is happening here. Plasma and KWin developers came together, remotely joined by Aaron Seigo over a Skype connection to discuss further integration. The remote connection could have been better, and this is definitely something we need for next Akademy so those still at home can join us. Nonetheless some ideas like moving Plasma-based animations into KWin and improving the API so more tight connection between KWin and Plasma is possible were discussed.

There also was a Kernel BoF, where kernel developer Matthew Garrett gathered some input from GNOME and KDE developers. Several developers asked for improvements on inotify, a kernel component which notifies applications of changes in files. This is useful for example to music players (which can then automatically add music to their collection if the user drops it in the music folder) or document editors (which can warn users if somebody else has made changes to a file while they are editing it). Inotify has several issues right now, one of them being performance related. Currently, you have to configure inotify for each file you want to watch, which can become a problem if you have a large collection of files to monitor. If inotify could work recursively this problem would be solved.

Related to this is the long time it takes to check if any indexed files have changed if the indexer has not been running for a while. The indexer has to crawl the whole system, checking every single file. A possible solution would be a special attribute for directories. It could indicate something in a directory tree has changed, making checking for changes much faster. This new attribute would  be possible as part of the extended attribute framework in the file systems and have a chance of making it into BRTFS. Unfortunately that means endusers will still have to wait several years before they will reap the benefits of this improvement, 'living higher on the stack' changes can take a while to trickle through.

Another interesting request which came up was related to some recent fuzz about a change in the way file systems worked on Linux. It increased the chances of losing file data for the end user if the system crashed. There is a temporary fix in place, but the developers are far from happy with that solution. A better way of solving the issue would be a new flag to be used when opening certain files, o_rewrite. It would tell the kernel to only write the changes to the file when the application closes it. Making sure the operation is atomic would also preserve extended file attributes, giving software developers an incentive to start using this new technique instead of their current workaround.

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 400px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/plasma and kwin devs surrounding remote aaron.JPG" width="400" height="300" /><br />
Plasma and KWin developers surrounding remote Aaron
</div>

The third important topic which came up at the Kernel BoF was the wish for some degree of integration between D-Bus and Linux. Currently, inter-application communication involves a lot of copying data and context switches while the applications are negotiating the exchange of knowledge and commands. Having part of this process in the kernel could have a big impact on performance, making data exchange cheaper in terms of system resources.

As usual, the above is not much more than a small selection of what is going on at the joint Akademy/GUADEC meeting here at Gran Canaria. Following every interesting BoF and talk would require some highly illegal cloning of the marketing team. But the above impression should convey at least some of the exciting things happening here to those not at the Destkop Summit, and probably even inspire our great communities to work together more closely in the future!