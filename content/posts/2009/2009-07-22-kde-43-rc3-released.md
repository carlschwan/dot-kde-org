---
title: "KDE 4.3 RC3 Released"
date:    2009-07-22
authors:
  - "sebas"
slug:    kde-43-rc3-released
comments:
  - subject: "TOn the Techbase schedule"
    date: 2009-07-22
    body: "The <a href=http://techbase.kde.org/Schedules/KDE4/4.3_Release_Schedule>Techbase schedule</a> says \"July 28th, 2009: Tag KDE 4.3 Assuming there is only one release candidate, trunk is frozen for KDE 4.3 tagging.\" It should be updated to three RCs."
    author: "gd"
  - subject: "4.3 in Good Shape"
    date: 2009-07-23
    body: "I've been testing RC2 for a couple of days now. No crashes, only a few minor issues here and there. Overall, 4.3 seems to be in a really good shape. Kudos to all developers!"
    author: "gwittenburg"
  - subject: "Yawn"
    date: 2009-07-23
    body: "There once were times when new releases of KDE spawned discussion. Lots of comments, positive, negative, fanboys and flames.\r\n\r\nNowadays reading the dot has really becoming one of the more boring aspects of internet surfing. As a KDE user and part-time bug reporter reading the dot is, I have to admit  it, not of too  much interest nowadays.\r\n\r\nNo  more  flames because  of the misplaced KDE4 release cycle, which is history nowadays, but maybe two or three nice comments to some news article. Yawn. No reason for me as an interested KDE user to read a bit longer on the dot.\r\n\r\nWas that what you all wanted when changing the dot? I've been an avid reader of the dot for years, but nowadays it is just boring...  \r\n\r\nPlease please please change the comment system again to allow the unregistered community to take part again. The  times of KDE4 flamewars are long over, please make the dot free again!!!"
    author: "Larx"
  - subject: "Dolphin"
    date: 2009-07-23
    body: "Dolphin became unusable for me with RC1. RC2 didn't fix things. It hangs for most operations, when selecting most files, moving, copying etc. Other than that KDE seems rock solid at this point. "
    author: "jadrian"
  - subject: "Anonymous"
    date: 2009-07-23
    body: "I agree with allowing anonymous comments. Not because I miss any flamewars though, just to attract more people to post. Anonymous comments should have a negative score though (below the visible threshold) and could, of course, be promoted to visibility by registered users. "
    author: "jadrian"
  - subject: "Come on, KDE!"
    date: 2009-07-23
    body: "Is that a pre-release version? It's the buggiest KDE version I've ever seen since 4.0.2 (including all minor and major versions and last release candidates of every major version)! Plasma crashes randomly, I can't connect to my wireless network anymore via network manager applet, taskbar is resising my panel without any order, sometimes, after crash plasma settings are not restored! Desktop effects are much slower than in 4.2. When browsing kickoff applications menu, it goes back one level randomly, and so on...Am I the only who has that lot of problems?"
    author: "NForce"
  - subject: "I don't have the problems you"
    date: 2009-07-23
    body: "I don't have the problems you mention with RC2. Regarding the plasma chrashes, do you have third party plasmoids? You could try to rename the ~/.kde/share/config/plasma* files and see it the crashes stop.\r\n\r\nBTW, the network management plasmoid is not yet released. It is still in playground afaik. If you have it, it means that your distribution package not yet ready software like kubuntu, which I use, but I don't have problems with the wifi."
    author: "mkraus"
  - subject: "Hm, i really don't have those"
    date: 2009-07-23
    body: "Hm, i really don't have those problems. Did you fill your bugs on bugs.kde.org?\r\n\r\nBut as i said, i really don't have any of your problems. From where did you get your kde 4.3 rc3?"
    author: "Asraniel"
  - subject: "No third party plasmoids on"
    date: 2009-07-23
    body: "No third party plasmoids on my desktop...Also, I've deleted my .kde4 folder from kde 4.2.4 before upgrade. I have to say - crashes are beggining to stop: an hour ago, plasma crashed after starting any application, but now firefox, amarok, dolphin are starting OK, if i start any other application, plasma still crashes.\r\nI know that nm-plasmoid is not released, but with kde 4.2.4 i only had problems with hidden wifi. Now i cannot connect any wifi networks"
    author: "NForce"
  - subject: "I've compiled it from"
    date: 2009-07-23
    body: "I've compiled it from official tarballs"
    author: "NForce"
  - subject: "Also, I've deleted my .kde4"
    date: 2009-07-24
    body: "Is .kde4 a typo or is your KDEHOME really .kde4?"
    author: "mkraus"
  - subject: "it is really brilliant"
    date: 2009-07-24
    body: "Hi I just compiled from tar balls and the release is brilliant. One small request: I can see that changing         \r\n\r\nOption \"AccelMethod\" \"EXA\"\r\n\r\ngives a big performance boost for radeon (open source) driver. Can you put that information in \"tooltips\" of \"Configure desktop effects\". \r\n\r\nThanks and keep the good work going.\r\nKarthik"
    author: "biophysics"
  - subject: "It's not a typo..."
    date: 2009-07-24
    body: "It's not a typo..."
    author: "NForce"
  - subject: "Giving the user more details"
    date: 2009-07-24
    body: "Giving the user more details which options he/she should choose for optimum desktop effects would generally be good. I really don't know what many of these options do.\r\n\r\nHow about a database with recommended settings for typical setups?"
    author: "Larx"
  - subject: "I think we're probably in"
    date: 2009-07-24
    body: "I think we're probably in string freeze.  Maybe report it as a wish at bugs.kde.org and it can go into 4.3.1 or similar."
    author: "troy"
  - subject: "You might actually check your"
    date: 2009-07-24
    body: "You might actually check your ram with memcheck. or other hardware. because kde 4.3 really should not be that buggy. worth a try. by the way, do you get backtraces?"
    author: "Asraniel"
  - subject: "ehh, kubuntu..."
    date: 2009-07-24
    body: "The problem is probably related to Kubuntu packages. While KDE works perfectly in Arch I have problems in Kubuntu - dolphin crashes when I want to see properties of root partition, there are missing translations (sic!) etc. Try something better, because Kubuntu it the worst \"KDE\" distro in my opinion.\r\n\r\n>I've compiled it from official tarballs\r\n\r\nOops, I noticed this after posting :) However, I still consider KDE in Kubuntu sucks a lot..."
    author: "pslomski64"
  - subject: "Boring is good"
    date: 2009-07-24
    body: "It leaves more time for getting real work done. Keeping us entertained is less important than developing a great desktop.\r\n\r\nI think another factor is the loss (for completely understandable reasons, of course) of Danny's wonderful updates. They were detailed enough that it provided a lot of material for discussion."
    author: "tangent"
  - subject: "Except that..."
    date: 2009-07-24
    body: "...Kubuntu uses ~/.kde for its KDE packages, and the poster himself has professed to use self-compiled packages. Please stop bashing Kubuntu if you have no proof, and haven't done your homework, or even read down a few comments."
    author: "JontheEchidna"
  - subject: "yep"
    date: 2009-07-24
    body: "feel free to do so! It might be a small thing, but it is a worthwhile contribution nonetheless..."
    author: "jospoortvliet"
  - subject: "Yes, he does, but I used"
    date: 2009-07-24
    body: "Yes, he does, but I used Kubuntu and the KDE in this distro is piece of bull. Like I mentioned before Dolphin crashes, there are missing translations, windows don't keep their positions etc. etc. while in Arch KDE is perfect. I have proofs, but they're already in launchpad (it seems no one's interested...). However, it seems you're a Kubuntu fanboy... It's not perfect, surprised? Btw. Kubuntu used .kde4, but this isn't important."
    author: "pslomski64"
  - subject: "Most Dolphin crash reports in"
    date: 2009-07-24
    body: "Most Dolphin crash reports in Launchpad would not be packaging problems, but rather upstream bugs. Translations have improved greatly in 9.04, and the window positioning bug has been fixed since Kubuntu 9.04. (Or, at least the bug I'm thinking you're talking about) Once again, bashing distros whenever somebody is having problems because you have had issues in the past is a bad idea. ;-)"
    author: "JontheEchidna"
  - subject: "yep... missing the commit digest"
    date: 2009-07-24
    body: "now as you mention it... I'm missing the commit-digest- It gave so much insight to the ongoing developments in a way that even non-developers could follow.\r\nOf course it was also a great source for all types of discussions (in fact mostly due to people with no expertise in programming misunderstanding some concepts)...but it was always a pleasure to read the digest and the comments..."
    author: "thomas"
  - subject: "it is possible to login using"
    date: 2009-07-24
    body: "it is possible to login using openid. allowing anonymous comments would make dot almost unreadable - finding useful posts between all the crap got too hard for me at some point. at least now stupid comment percentage has dropped significantly :)"
    author: "richlv"
  - subject: "Dolphin is fine"
    date: 2009-07-24
    body: "Ok to be fair, turns out this is not a problem with dolphin but with Nepomuk in my environment. Dolphin doesn't hang anymore when Nepomuk is off. "
    author: "jadrian"
  - subject: "I tested the newest Kubuntu"
    date: 2009-07-24
    body: "I tested the newest Kubuntu and I even installed new KDE from backports (4.2.4, because 4.3 isn't translated to my language there ;)). However, they missed translations and this is the most annoying :/ I hope they will take this into consideration, because believe or not Kubuntu is my primary distro :) \r\n\r\nRegards :)"
    author: "pslomski64"
  - subject: "I don't think so, because KDE"
    date: 2009-07-25
    body: "I don't think so, because KDE 4.2.4 (also compiled from official tarballs) was working just perfect. At the start, i was getting backtraces (saying something about qtnetwork, qtcore or so), but after few hours plasma started to crash without backtraces"
    author: "NForce"
  - subject: "Userbase"
    date: 2009-07-25
    body: "Those information can change quite frequently as drivers get updated by the vendors. Also there are a lot of different graphic cards out there, so you can't really put everything in those tool tips.\r\nKDE already hase a place to collect information of that kind, and thats a wiki called Userbase (http://userbase.kde.org). You will find an article there regarding graphics card optimization:\r\nhttp://userbase.kde.org/GPU-Performance\r\nThis article points to exactly the configuration option you mentioned.\r\nPlease consider adding additional information to improve this article."
    author: "dtritscher"
  - subject: "Just run memcheck once, won't"
    date: 2009-07-25
    body: "Just run memcheck once, won't hurt you (you already have it installed ;-) )\r\n\r\nBecause sometimes it happens that different versions of a software use different amount of ram, so one might turn just fine and the other happens to use the broken part of the ram all the time. It happend to me more than once, thats why i advice you to test with memcheck (best is just after a crash, perhaps it's also a overheating problem)"
    author: "Asraniel"
  - subject: "To EXA or not EXA ..."
    date: 2009-07-26
    body: "A Google search will also turn up warnings about using EXA with Radeon.  So, it isn't that simple."
    author: "JRT256"
  - subject: "That's how you learn from mistakes...."
    date: 2009-07-26
    body: "Problem: KDE 4.0 release causes trouble with community on the dot.\r\nSolution: Developers detach from community feedback on the dot. \r\n\r\nProblem solved, or not? Lets remember the good times and be happy it lasted so long. Just be worried if somebody does a KDE 5 in a community with disabled learning ability.\r\n\r\nYours,\r\nKay"
    author: "kayonuk"
  - subject: "I'm not so sure"
    date: 2009-07-26
    body: "You make it sound like (always) the developers were at fault for 4.0. While I won't go over this topic because it's like beating a dead horse, I would like to point out that some of the comments at the time were way more than simply offensive: of course, a lot of people thought that respect and politeness didn't matter on the Internet. \r\n\r\nThis was a way to deal with inflammatory and insulting comments. I hardly call it \"detaching from community feedback\"."
    author: "einar"
  - subject: "Not necessary"
    date: 2009-07-26
    body: "EXA for 2D acceleration is the default with current drivers. It might be that your distribution is not uptodate, but you won't have to worry with its next release."
    author: "d2kx"
  - subject: "Clarification"
    date: 2009-07-26
    body: "Hello,\r\n\r\nthe way to deal with purely inflammatory or insulting comments is to delete them or to move them out of sight. No doubt about that, just because a developer makes mistakes is no ground for personal attacks to be tolerated.\r\n\r\nBut I don't think that was it. I think developers just didn't like the feedback how much it sucked. You just need to read blogs about 4.1, 4.2 or 4.3 to hear from developers themselves how much it sucked before, but is now great, this time really.\r\n\r\nTruth is 4.3 appears to be a solid release, finally. It was a long time. Too long. And damaging to the community, just because parts of it have stopped to communicate with other parts of it.\r\n\r\nLets begin the healing.\r\n\r\nYours,\r\nKay\r\n\r\n"
    author: "kayonuk"
  - subject: "Excellent!"
    date: 2009-07-27
    body: "I always use kde as my linux desktop, and I love it.\r\n\r\nthank you! \r\n<a href=\"http://www.yukisale.com\" style=\"color:black\">Yuki</a>"
    author: "slicsir"
  - subject: "Modding Down..."
    date: 2009-07-27
    body: "Yes sure,\r\n\r\nthat definitely had to be modded down, didn't it? I was thinking that the content was not particularily bad. \r\n\r\nModeration by popularity vote can't work. I would prefer that chosen people have the ability to delete comments as they see fit according to clear rules of theirs. \r\n\r\nYours,\r\nKay"
    author: "kayonuk"
  - subject: "Some Icons"
    date: 2009-07-27
    body: "When will comes the improvement in the folders? I miss the consistency in their look. So the home-folder and root-folder looks different to the normal folder-icon - very notable in the smaller formats. This is something what I dislike totally.\r\nThe next one is the device-icon (monitor with the half cd above - plasamoid on panel): It's only in 128x128 and is only scaled by the system. It doesn't look so good.\r\nThe weather-icons are nice. But I miss them in small sizes, too. In small you can't recognize if it's raining now or only cloudy. It's too blured ...\r\nThe Media-Buttons I like them from Gnome much more. If stay with them, so I will hope for a better manual correction by hand. Right now they aren't so good. Wlan on Panel is too strange, too. Here I would replace it.\r\n\r\nHope to got here some improvements in the next versions of KDE (4.4 or 4.5). Only the detail and good work in EyeCandy can match against the fine Mac-look (prof. Icon-work).\r\n\r\nMy2cents  "
    author: "scholli"
  - subject: "KDE printing"
    date: 2009-07-27
    body: "Although I appreciate the work done on Plasma, I'd be interested to know if any progress has been made since KDE 4.2 regarding printing. In 4.2, the printing system was barely usable (Guessing what kind of printed output Konqueror would give from a webpage required for sure some metaphysical skills... :p) and lacked a lot of functionalities from the 3.5's KPrinter application.\r\n\r\nI know printing is probably way more boring to work on than fancy SVG graphics, but also a little bit more important for office uses, so any information about the status of that part of KDE 4.3?"
    author: "Lauwenmark"
  - subject: "I have to agree to that."
    date: 2009-07-28
    body: "I have to agree to that. Printing is horrible with KDE 4.2.4 (Fedora 11). E.g. I didn't manage to print a landscape PDF in landscape. It was always printed in portrait and cropped! Other things like multiple pages per slide don't work right either. (Finally I used my mothers Mac to print!) I really miss the printing system and polishedness from KDE 3.5 and kpdf. It seems to be not all KDEs fault, though, because on one point cupsd did hang with 100% CPU usage. However, Mac OS X also uses CUPS! So why it's not working as good under Linux?"
    author: "panzi"
  - subject: "Qt and printing"
    date: 2009-07-29
    body: "Printing is better in 4.3 IME, but there are still some features missing like odd/even page printing.\r\n\r\nKDE now relies on Qt for printing due to man power and general interest levels: it's a LOT of work to build and then maintain a printing system and very, very few people are both knowledgeable enough and have the desire to work on it.\r\n\r\nunfortunately for us, there was only so much input we could have in the Qt development process. the Qt developers gathered a LOT of feedback from the KDE community on printing and made a lot of improvements based on that .. but still, what developers in the KDE community that we did have who were interested in working on printing could hack right on the upstream Qt sources.\r\n\r\nnow that Qt is on gitorious and publicly developed, however, we can apply our modest manpower to the situation too! so i'm quite optimistic about how things will move from here on out: Qt can provide manpower to maintain the core infrastructure (esp cross-platform stuff) and extend the UI's capabilities, and we can provide direct effort on the features and polish points our users need.\r\n\r\nso, for instance, odd/even page printing is available on a qt branch on gitorious, thanks to John Layt. :)\r\n\r\nyou can read more about this on John Layt's blog: http://www.layt.net/john/"
    author: "Aaron Seigo"
  - subject: "Why?"
    date: 2009-07-29
    body: "I've almost stopped reading this site at all, I just follow it through rss feed.\r\n\r\nThe new layout is just terrible, with fixed width, insane line distances and lots of wasted space, so when I come here it just pisses me off...\r\nhttp://kolla.no/thedot1.png\r\nhttp://kolla.no/thedot2.png\r\n\r\nYes, I'm still on KDE3, since KDE4 for me still is just a huge pile of annoyances. I only run KDE4 on a test machine at home in hopes that it will become better, but so far I've seen nothing that indicates that. Btw - any tip on how to turn off nepomuk/strigi/wtf crap alltogether? I dont need no frigging desktop search engine, I'm actually capable of keeping track of my own data by myself."
    author: "kolla"
  - subject: "Glad to hear that at least"
    date: 2009-07-29
    body: "Glad to hear that at least something is done. :)\r\nThe printing system in KDE 3.5 was a major selling point when I explained to people why I use Linux. Nowadays printing on KDE (+Fedora Linux) is much worse than on Windows and Mac OS X.\r\n\r\nIsn't it possible to get a GSoC student to improve on the situation? Hm, I plan to be no student next summer any more, so I can't apply. (I don't think I will be fast enough, though. So who knows.) Without payment this task is really not interesting enough for me to help out, sorry. (GSoC is timed badly for austrian students anyway, because our summer break is from July to September and all the exams/due dates are in June.)"
    author: "panzi"
  - subject: "The future of the default-theme?"
    date: 2009-07-29
    body: "I saw in some screenshots that KDE 4.3 ... ist going to the side of the look-like from Bespin. If it's true so I am going to be sad - very sad. For my gust Bespin shouldn't be an example for the future default-theming ... *sorry"
    author: "scholli"
  - subject: "Modding"
    date: 2009-07-30
    body: "The problem with user moderation is that many tend to mod down comments they don't agree with and up the ones that coincide with their view point. That is NOT what moderation is for. It should be to get rid of rubbish comments (spam, trolls, off-topic,...) and make the particular interesting ones stand up. \r\n\r\nI really don't like the alternative where comments are deleted. Despite the moderation, your comment is visible. And even if it gets hidden it will still be there for all to see, just one click away. "
    author: "jadrian"
  - subject: "GSoC and motivations"
    date: 2009-07-30
    body: "\"Isn't it possible to get a GSoC student to improve on the situation?\"\r\n\r\nnow that Qt is fully open for development, i think so, yes.\r\n\r\n\"Without payment this task is really not interesting enough for me to help out\"\r\n\r\nthat's really the root of the issue here :) it's a major selling point you've used with others, you really appreciated it yourself but ... not interesting enough to help out with unless someone pays you.\r\n\r\ni'm not blaming you at all, it just demonstrates why we had to take the route we ultimately did.\r\n\r\nif Qt had not been on the road to opening up, it would have made me a lot more nervous. as it was, it was just a matter of being patient and waiting for that to happen. now, we just need focus all actually available resources on it. :)"
    author: "Aaron Seigo"
  - subject: "not that i know of"
    date: 2009-07-30
    body: "i don't know of any moves towards Bespin style in Oxygen or Air. which screenshots are these exactly?"
    author: "Aaron Seigo"
  - subject: "Semantic Desktop"
    date: 2009-07-30
    body: "I'm at office with KDE 3.5 but if my memory doesn't fail me, in 4.3 RC3 you go to Desktop Configuration, pick the Advanced Configuration tab, and you should have Desktop Search there somewhere. \r\n\r\n"
    author: "jadrian"
  - subject: "answer"
    date: 2009-07-30
    body: "I like KDE and I am observing the go-through from one level to the next. Seems like that I saw a screenshot with Bespin and I got nervous. The default (oxygen) theme is nice - maybe here and there some improvement!? But it's too nice for leave it. Sorry for my panic..."
    author: "scholli"
  - subject: "Re: Why"
    date: 2009-07-31
    body: "<quote><b>I only run KDE4 on a test machine at home in hopes that it will become better, but so far I've seen nothing that indicates that.</b></quote>\r\n\r\nWell, that is your own personal opinion. I can not move back to KDE3.5 anymore because it really does not offer features what I need. Since KDE 4.3 the last thing what kept \"away\" came implented. Every virtual desktop having own widgets and wallpaper. And the reason what force me to use KDE4 (even that I would like to use KDE3) is that applications for KDE3.5 can not be latest versions if I do not want to mix versions (what I do not).\r\n\r\nBut it is still nice that some people likes KDE 3.5. Hopefully will change someday... :)\r\n\r\n<quote><b>Btw - any tip on how to turn off nepomuk/strigi/wtf crap alltogether?</b></quote>\r\n\r\nIt should not be enabled by default. \r\n\r\nBut if you use such distribution what does such wierd config then:\r\n- Open up system settings (Configure your Desktop)\r\n- Open the Advanced tab\r\n- Click the \"Desktop Search\"\r\n- Uncheck both of the options\r\n- Click Apply and close window\r\n\r\nThe Nepomuk actually does speed up your working environment. Just add tags to files (different projects, downloads via KGet etc) and place a folderview plasmoid to desktop/panel with nepomuksearch:/tag:<tag> and you can see always those files fast, even that they would be on multiple different directories. This way I can easily manage files itself, but still access them very fast way from single place. "
    author: "Fri13"
  - subject: "upstream vs kubuntu"
    date: 2009-07-31
    body: "Well, I test kubuntu on it's every release or KDE main release and I follow kubuntus launchpad bugs and I can not find many of the bugs related to KDE on other distributions (like fedora, mandriva and opensuse). So I can say that most of the bugs are not in upstream but on kubuntu itself. I would like to recommended the kubuntu because many choose ubuntu for their first distribution. But kubuntu just push such bull to your face with KDE so I always suggest using other distribution if they want good KDE experience.\r\n\r\nAnd I have not find way to see kubuntus bugs on bugs.kde.org. It is not nice to have two database for bugs. But maybe it is good because we get many bugs what are Kubuntu only, keeped away."
    author: "Fri13"
  - subject: "Bespin looks great actually"
    date: 2009-07-31
    body: "You can make Bespin very pretty these days.\r\nhttp://kde-look.org/content/show.php/Bespin+Reborn?content=98059\r\n\r\nI think it looks great! Especially those tabs. http://img189.imageshack.us/img189/2406/snapshotd.png "
    author: "bliblibli"
  - subject: "possible typo"
    date: 2009-07-31
    body: "release notes title says \"KDE 4.3 RC3 Codenames \"Cay\"\"\r\n\r\nshouldn't that be 'codenamed' ?"
    author: "richlv"
  - subject: "answer 2"
    date: 2009-07-31
    body: "I don't want discus what is better or worst. Your imagehack-screenshot shows a better Bespin - yes it's true. But the symbols and scrollbar I can see in Dolphin bugs me already. But okay, it's a personally gust. Mhh if we talking about. I like the default Oxygen-theme more. And QT-Curve ist very nice too. Here you have the possibility to make buttons not so round and the tabs with a line above - for example.\r\n\r\nThe most important I think is that all themes KDE haves right now should stay in their own ship and leveling up with new and own ideas. Coping only with really improvements.\r\n\r\nNice to talked about a little bit.... Cheers"
    author: "scholli"
  - subject: "Kickoff"
    date: 2009-08-02
    body: "In the beginning Kickoff had all available partition listed in it's computer section, this was the KDE 3 version of Kickoff back then but that changed since KDE 4. Could somebody tell me why the partitions aren't listed in Kickoff anymore? I found it to be a nice and quite usable feature. This feature is in Lancelot but I feel more comfortable with Kickoff. \r\nCan Kickoff be configured somehow to show my partitions in the computer section?\r\nApart for that I am quite satisfied with KDE 4.3, especially by the fact that the devs are taking the time to make this release the best so far."
    author: "Bobby"
  - subject: "The perfect solution IMO"
    date: 2009-08-02
    body: "The perfect solution IMO would be Oxygen with the configurability of Qt-Curve. Bespin is nice but I really like Oxygen, however it lacks configurability and neds a little touch up here and there."
    author: "Bobby"
  - subject: "Free speech"
    date: 2009-08-02
    body: "I agree with you. Free speech should be promoted, even though it might sound negative at times I am sure that it won't kill any of the devs ;) On a serious note though, I strongly support unregistered users having the opportunity of sharing their views. Even negative comments cand be helpful sometimes."
    author: "Bobby"
  - subject: "fan :) "
    date: 2009-08-03
    body: "I really like 4.3_rc3, and I'm damn anxious for the tarballs hitting the FTP, so I can tell my Gentoo to install at once :)\r\n\r\nThank you for creating this awesome desktop! \r\n\r\nYou make KDE the Key to my favorite Digital Experience! \r\n\r\n(just to take a little stab at some really olde stuff :) )"
    author: "ArneBab"
---
Even in the hot phase up to KDE 4.3.0, there have been quite a bunch of fixes to KDE's 4.3 branch. The KDE Release Team has decided to err on the safe side and do another <a href="http://kde.org/announcements/announce-4.3-rc3.php">release candidate</a> before KDE 4.3.0 comes out. Dirk Müller has rolled tarballs of the current state of KDE 4.3 and put them up for testers, packages for some distributions are already under way. This also means that the release of KDE 4.3.0 has been postponed for one week. The new planned release date is August, 4th 2009.

One notable change since RC2 is a performance fix for Plasma's caching of rendered SVG graphics that lead to the applet being frozen for a short while after resizing it. A regression that cropped up after RC2, and which resulted in HTTP redirects being broken has been fixed in RC3 as well.

Please give this KDE 4.3 RC3 some good testing to make sure KDE 4.3.0 will be as flawless as possible.
<!--break-->
