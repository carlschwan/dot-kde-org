---
title: "Student Application Period for GSoC 2009 is Now Open!"
date:    2009-03-23
authors:
  - "jefferai"
slug:    student-application-period-gsoc-2009-now-open
comments:
  - subject: "My Thanks to Google"
    date: 2009-03-25
    body: "I really think this is an incredible program.  They don't have to do things like this, but they choose to do so."
    author: "enderandrew"
  - subject: "My Thanks to Google"
    date: 2009-03-25
    body: "I really think this is an incredible program.  They don't have to do things like this, but they choose to do so."
    author: "enderandrew"
  - subject: "Good work! Your article is an"
    date: 2009-06-02
    body: "Good work! Your article is an excellent example of why I keep comming back to read your excellent quality content that is forever updated. Thank you!\r\n<A href = \"http://www.apexprofessionalsllc.org\"> Timeshare </A>\r\n<A href = \"http://www.arcadejunkie.com\"> arcade games </A>\r\n\r\n"
    author: "apexdromnn"
---
<img src="http://dot.kde.org/sites/dot.kde.org/files/2009socwithlogo.png" alt="Google Summer of Code 2009" align="right" width="300" height="200" />

The student application period for Google Summer of Code begins at 19:00 UTC today, and closes at 19:00 UTC on April 3rd. Students should be sure to seek feedback from their mentors as early as possible, to ensure that their application is in top shape for the grading period.

Good luck to all students!
