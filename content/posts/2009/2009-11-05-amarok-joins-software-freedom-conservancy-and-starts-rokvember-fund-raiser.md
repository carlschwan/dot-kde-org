---
title: "Amarok Joins Software Freedom Conservancy and Starts Rokvember Fund Raiser"
date:    2009-11-05
authors:
  - "Mark Kretschmann"
slug:    amarok-joins-software-freedom-conservancy-and-starts-rokvember-fund-raiser
---

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/amarok_splash_small.png" width="300" height="224" /></div>
The Amarok project has joined the <a href='http://conservancy.softwarefreedom.org/'>Software Freedom Conservancy</a> (SFC). This move allows donors to give tax-deductible donations in the US, and it increases transparency in the spending of Amarok's funds. The SFC is composed of various Free and Open Source Software projects and provides member projects with administrative and financial services. This greatly helps us to be more efficient and focus on what we really do best: creating world class software. At the same time, we stay fully committed to the KDE community. The partnership with the SFC is complementary with the Amarok community's relationship to the KDE e.V., as the e.V. continues to support the Amarok team's activities in Europe and elsewhere in the world. Amarok has always been, and always will be, a part of the KDE community!

The Amarok team is aiming to raise $10,000 for the coming year and asking users to help the project by making tax-deductible donations during our <a href='http://amarok.kde.org/en/node/700'>Rokvember</a> fund raiser.

If you want to help us make Amarok even better, head over to our <a href='http://amarok.kde.org/en/node/700'>official announcement</a>, and support our fund raiser.

The Amarok team is currently developing faster than ever and we are highly motivated to deliver you the best music player possible. The upcoming <a href='http://amarok.kde.org/blog/archives/1112-Amarok-2.2.1-Were-getting-there!.html'>Amarok 2.2.1</a> release will bring our users <a href="http://amarok.kde.org/blog/archives/1115-Post-Amarok-2.2.1-Adding-some-color-to-your-life!.html">features</a> that many of you have been waiting for, and we are <a href='http://blog.jefferai.org/2009/11/03/the-collection-scanners-ultimate-speed-bump-and-cases-1137'>constantly aiming to improve the performance</a> as well.

We are looking forward to many more exciting developments and thank you for supporting us!