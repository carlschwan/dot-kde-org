---
title: "KDevelop 4 Beta 1 Available"
date:    2009-02-05
authors:
  - "apaku"
slug:    kdevelop-4-beta-1-available
comments:
  - subject: "Niftiness"
    date: 2009-02-05
    body: "In short: KDevelop4 rocks :) The PHP support sitting in playground is shaping up to kick so much backside it's not even funny, i'm using it now, pestering the two devs on it by throwing the odd backtrace at them and coming up with silly ideas for new features, and they're really supportive people. Really great stuff! :)"
    author: "leinir"
  - subject: "KDevelop "
    date: 2009-02-05
    body: "<p>I used the old versions of KDevelop and that helped me a lot getting into the development of C++ applications under Linux. Before KDevelop I tried using vim / emacs but never felt comfortable with these.</p>\r\n\r\n<p>After reading a number of blogs about KDevelop 4 at planetkde I have no doubt that the new version of KDevelop will absolutely rock! There are a number of features (in particular for C++) that made me just say \"wow!\".</p>\r\n\r\n<p>I am absolutely sure that the new release will lower the entrance barrier for developers. This will certainly help attracting new developers for KDE / KDevelop!</p>\r\n\r\n<p>Well done guys - please keep up the fantastic work!</p>"
    author: "mkrohn5"
  - subject: "kdevelop4 rocks"
    date: 2009-02-05
    body: "indeed, kdevelop4 rocks. I use it since 3.9.90 and the DUChain is incredilbe. keep up the good work."
    author: "mkraus"
  - subject: "Available on windows?"
    date: 2009-02-06
    body: "Nowadays, I am forced to do most of my development work on Windows. Is KDevelop4 available on that platform yet? I would be very happy to trade in QDevelop for KDevelop!"
    author: "andresomers"
  - subject: "Available on windows?"
    date: 2009-02-06
    body: "<p>According to this developer blog http://apaku.wordpress.com/2009/02/05/kdevelop4-beta1/</p>\r\n\r\n<p>\"The good news is: Latest trunk already compiles and opens projects, the bad news: This is not part of the beta. I\u2019m pretty confident though that the next beta will include win32 support.\"</p>\r\n"
    author: "mkrohn5"
  - subject: "Thanks for highlighting that,"
    date: 2009-05-12
    body: "Thanks for highlighting that, it did give a fairly disappointing experience when I tried the packages on kubuntu yesterday. If it\u2019s just happening on kubuntu, then there\u2019s a higher chance it\u2019ll be fixed quickly so more of us can give KDevelop4 a try.\r\n\r\nHaving followed the recent blog posts, I\u2019m looking forward to trying out interesting new features. "
    author: "chrisawalik"
  - subject: "I feel there is a kind of"
    date: 2009-05-15
    body: "I feel there is a kind of uncoordination in the way and manner that we've been handling things of this nature. Take for instant the SFD, obviuosly your impression is that the entire country I mean Nigeria have been participating. But if you ask me, I'll say it's states within the Lagos axis that probably have an insight into it. Its good this forum has provided a platform for me to meet FOSS enthusiast within my abode. One way to start is to find a way by which we can collaborate to actualize the FOSS dream in our great country and beyond."
    author: "acotas548"
  - subject: "To the author of a blog \u2026 I"
    date: 2009-05-18
    body: "To the author of a blog \u2026 I Read your blog rather recently. That it would be desirable to note \u2026 (do not think that with what that I reproach or I try to give advice) laconic enough design, anything superfluous I would tell))) your subjects are close to me, and it pleases. But why do not write the opinion on the events occurring in the world, in respect of events international for example?? I understand, that \u201cnews suffice\u201d, but sometimes it would be desirable to learn opinion of the usual person, so to say - an independent sight, to compare it to the opinion. And so \u2026 Write even more often, even more, and even more interestingly. Thanks!\r\n<A href = \"http://tummy-tuck-info.blogspot.com\">tummy tuck</A>"
    author: "prew"
---
<p>On behalf of the KDevelop team I am happy to announce that we have reached the next milestone on our way to a final release, KDevelop 4 Beta 1.  We feel that KDevelop 4, although in no way feature complete, is now usable and stable enough to get first feedback from a somewhat wider audience. Being a beta there are of course still bugs and missing functionality, but we have excellent language support for C++, integration of the CMake buildsystem, subversion, git and even starts of Qt GUI designer integration.</p>
<!--break-->
<p>We have fixed a lot of bugs since the last alpha release and there has been some improvements done especially for users who are new to KDevelop to lower the entrance barrier.  To get a first impression you can have a look at the recent <a href="http://zwabel.wordpress.com/">blog posts from David</a> and <a href="http://apaku.wordpress.com/">myself</a>.</p>

<p>For those eager to try it out, the source packages are available from the KDE mirrors. Just go to <a href="http://download.kde.org/download.php?url=unstable/kdevelop/3.9.91/src/">download.kde.org</a> and choose a mirror. There are two source packages there, kdevelop and kdevplatform. KDevPlatform needs to be built before KDevelop itself and both require KDE 4.2.0.  Kubuntu users can install kdevelop-kde4 from <a href="https://edge.launchpad.net/~kubuntu-experimental/+archive/ppa">the kubuntu-experimental archive</a>.  Mandriva users can find it with the KDE 4.2 packages.  openSUSE has packages in <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Extra-Apps/">the KDE 4 Extra-Apps archive</a>.</p>