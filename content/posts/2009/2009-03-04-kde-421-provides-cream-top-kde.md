---
title: "KDE 4.2.1 provides the \"Cream\" on top of KDE"
date:    2009-03-04
authors:
  - "sebas"
slug:    kde-421-provides-cream-top-kde
comments:
  - subject: "Congratulations!"
    date: 2009-03-04
    body: "Congratulations!"
    author: "Shade"
  - subject: "Thanks for all the fixes"
    date: 2009-03-04
    body: "additional links matching to this article; <a href=\"http://kde.org/announcements/announce-4.2.1.php\">Release Announcement</a> and <a href=\"http://kde.org/info/4.2.1.php\">Info Page</a><br>\r\n\r\np.s. the \"all SVN changes\" links in the Changelog are not correct ;-o\r\n"
    author: "sebsauer"
  - subject: "Great job!"
    date: 2009-03-04
    body: "Maybe in next version some other software could replace Gwenview. Its appearance is awful."
    author: "jigger"
  - subject: "Qt 4.5"
    date: 2009-03-04
    body: "\"If you're running this release with Qt 4.5 and you're encountering problems, please let the developers know by means of bugs.kde.org so those kinks can be worked out for everyone (including yourself).\"\r\n<p/>\r\nDoes this mean that running on Qt 4.5.0 is now supported (especially for plasma), and that there should be no problems in doing so?\r\n<p/>\r\nIn any case, nice work everyone :)"
    author: "ianjo"
  - subject: "not sure, it hasn't been fully tested yet"
    date: 2009-03-04
    body: "The truth is that we don't know. Supported or not, we want to make the experience of using KDE as good as possible, no matter which Qt version you run.\r\n\r\nThe -RC phase of Qt 4.5 has helped quite a  bit in working out kinks. I'm currently not aware of any really critical problems, but we're careful in stating that it will just work. (And what's \"just work\" for one person is \"totally broken\" for someone else, as we all know ... ;-))"
    author: "sebas"
  - subject: "Just added the link to the"
    date: 2009-03-04
    body: "Just added the link to the announcement, thanks!"
    author: "sebas"
  - subject: "Bugs what were fixed!"
    date: 2009-03-05
    body: "Ah, they got fixed the Tab completion bug on konqueror addressbar. I was getting mad with it! I do not like at all the stupid \"Opera\" way how it worked, needed to use \"arrow down\" -button to use list.\r\n\r\nAnd more speed is gained, what is nice. Altought I am not so sure because so far I have got great speed with ATI (not with NVIDIA, until 4.2 :-() \r\n\r\nAnd one bug is at least fixed on plasma. That \"Quick Access\" Widget does not crash Plasma anymore. At least on Mandriva Cooker 4.2.1 version. \r\n\r\nWell, so far great 4.2.x series. Keep it up and make 4.3 even better! So far best desktop environment IMHO (vs GNOME, Windows Vista/7 and even Mac OS X!)\r\n\r\nCan not wait 4.2.2 version to fix more bugs, what can be found etc."
    author: "Fri13"
  - subject: "qt 4.5 and kde 4.2"
    date: 2009-03-05
    body: "In Kubuntu Jaunty 9.04, KDE 4.2 is compiled with qt 4.5rc1 (and now updated to 4.5). there doesn't seem to be any apparant issue with the kde+qt4.5 combo. :)\r\n\r\nAnd congrats to all..\r\n\r\nwell written announcement, kudos to the writer :)"
    author: "fast_rizwaan"
  - subject: "qt 4.5 and kde 4.2"
    date: 2009-03-05
    body: "it depends. Jaunty also uses latest X-driver. And kde 4.2.1 + Qt 4.5 + radeon + composite on shows here sometimes heavy rectangular noise especially in  menu+window area.\r\n\r\nAnd Kubuntu seems to start again to cripple KDE packages: Konqi in Jaunty does not show the standard \"about:\" content: No home folder, no network folder. Still hope, that they stop this \"downsizing\"\r\n\r\nBut over all Jaunty alpha of Kubuntu looks quite promising.\r\n\r\nBye\r\n\r\n  Thorsten"
    author: "schnebeck"
  - subject: "Typo: \"exiting\" -> \"exciting\""
    date: 2009-03-05
    body: "Typo:\r\n\r\n\"exiting\" should be \"exciting\""
    author: "vicT"
  - subject: "Okular and .lit"
    date: 2009-03-05
    body: "Is there any chance that Okular will ever add support for .lit files?  The format was explored enough that someone wrote a script to convert .lit books to .html files, but it would be nice if Okular can just open them.\r\n\r\nhttp://bugs.kde.org/show_bug.cgi?id=158113"
    author: "enderandrew"
  - subject: "Please, don't do this"
    date: 2009-03-05
    body: "Please, don't do this. This is the announcement of a new maintenance version of KDE, so no new features will be included, so it's completely off-topic asking about new features in some application. If there is already a bug report with the feature request filled, then it's done what should be done.\r\n\r\nIf you want more users being aware of this bug report so they can vote or provide more feedback, do this in the forums or the mailing lists, not a random news entry on the dot.\r\n\r\n"
    author: "suy"
  - subject: "Only one Plasma bug fixed?"
    date: 2009-03-05
    body: "I hope this is just because developers are just lazy to write the changelog, because I can't believe a so important part of KDE (currently the whole desktop experience) received just one bug fix :)\r\n\r\nAnyway, I want to congratulate Kopete team, they fixed a lot of bugs, and some that really were annoying me, so this alone made the upgrade worth."
    author: "protoman"
  - subject: "sebas"
    date: 2009-03-05
    body: "Fixed, thanks."
    author: "sebas"
  - subject: "Mobipocket"
    date: 2009-03-05
    body: "Well, .lit support is not even planned but if you can get your books in different format then you may be interested that Okular will support mobipocket in KDE 4.3 (code is already in trunk)"
    author: "qbast"
  - subject: "Issues"
    date: 2009-03-05
    body: "Well seems like Qt 4.5.0 does have issues with filenames using Unicode characters like accentuated ones. \r\n\r\nAfter moving to Qt 4.5 with KDE 4.2.0 in openSUSE 10.3 my panel dock also stopped working properly. KDE 3 application icons are displayed correctly on the dock, but icons of KDE 4  applications are not displayed at all. The dock also does not display skype's icon. I know people for whom this is working fine though. "
    author: "jadrian"
  - subject: "This is great"
    date: 2009-03-05
    body: "with Qt-4.5.0 and kde-4.2.1 releases back to back. To bad konqueror still barfs on flash with the nsplugin viewer. Yes I am aware of the jiggles you can do with kmplayer+mplayer to get konqueror to play flash. But, erm, why make me jump through those hoops when the nsplugin viewer is already provided by kdebase? It seems like the further down the road kde4 travels, the more konqueror is relegated to the bit heap. Please don't tell me to write some code. Programmer I not, just an avid user and long for konqueror to be treated in the fashion we were lead to believe before kde4 ever came about.\r\n\r\nAnd what happened to the extragear like there was in ftp://ftp.kde.org/pub/kde/stable/4.2.0/src/extragear ."
    author: "stumbles"
  - subject: "\"all svn changes\" still 404"
    date: 2009-03-05
    body: "I suppose there are more bug fixes, or at least a bug report I made some time ago got fixed (with a backport available) but it is not listed in the changelog.\r\n\r\nI still get a 404 in any of the \"all svn changes\" urls\r\n(like http://www.kde.org/announcements/changelogs/4_2_1/kdelibs.txt or\r\nhttp://www.kde.org/announcements/changelogs/4_2_1/kdebase.txt) \r\nso not sure if it is really fixed in 4.2.1 or will only be in 4.2.2"
    author: "mootchie"
  - subject: "Flash..."
    date: 2009-03-06
    body: "There's an interesting point with flash, in that the Linux plugin is basically created to work with Gecko... And nothing else. Yup, it really is that bad - the kinds of hoops the kde devs have to jump through to get the thing working even to the level it is now are horrendous. Adobe, please wake up and smell the hummus - there's more to the internet than Firefox!"
    author: "leinir"
  - subject: "I have issues using Unicode"
    date: 2009-03-06
    body: "I have issues using Unicode characters too.. The problem is that all of my folder/file names are in Greek Languages... I can see them, but i cannot access them with the KDE 4.2.1 apps...\r\n\r\nBut there is no problem with the dock icons.\r\n\r\nKubuntu Jaunty + KDE 4.2.1 + QT 4.5"
    author: "menace82"
  - subject: "I understand"
    date: 2009-03-06
    body: "what you are saying. Frankly I think flash is a proprietary pox. But do have to give a nod to them for the release of an x86_64 version, that does run very well in a pure 64 bit environment, with Firefox of course.\r\n\r\nBare in mind my unfamiliarity of the complexities you suggest that are involved, but I can't really buy the reasoning, if only for a dollar. How is getting flash to work that much more \"horrendous\" than creating a new file manager, basically from scratch, ala Dolphin? Probably not the best example, but when it comes to konqueror, I can only fantasize just how wonderful it *could* have been had all the Dolphin efforts been put into konqueror.\r\n\r\n"
    author: "stumbles"
  - subject: "The unicode bug should be"
    date: 2009-03-06
    body: "The unicode bug should be fixed with the next round of updates that should be due in the next hour or so."
    author: "JontheEchidna"
  - subject: "debian sid"
    date: 2009-03-06
    body: "Now that Debian Unstable is again unlocked after the release of Lenny, is anyone in the know when KDE4 will migrate down from Experimental?\r\nI looked into the mailing list archives, and they wrote \"really soon now\" in the middle of february, and there has been nothing since (at least nothing I could find).\r\n\r\nBelieving in the \"really soon\" after the release of Lenny, I wanted to stay with Unstable, and now I am waiting, and waiting, and ... :-(\r\n\r\nThe current KDE looks absolutely great, and I'd really love to leave 3.5.10 behind :-)"
    author: "icekiss"
  - subject: "Flash 10"
    date: 2009-03-07
    body: "I really find such comments to be useless.  It would be nice if there was a well defined API for NS plugins, but in reality, it is Gecko<p>I realize that Flash 9 had serious issues.  However Flash 10 works OK with KDE-3.5.  Yes, the: \"nspluginviewer\" crashes occasionally, but there are probably specific bugs that need to be fixed, or the website code is at fault.  However, these sites work fine:\r\n</p><p>\r\nhttp://www.eye4u.com\r\n</p><p>\r\nhttp://ge.ecomagination.com/site/\r\n</p><p>\r\nso, I see no reason that these shouldn't be working just as well on KDE-4.2.1."
    author: "KSU257"
  - subject: "Which"
    date: 2009-03-07
    body: "comment or comments do you find useless? If if really is Gecko, then how about changing how nspluginviewer deals with flash? Just a suggestion.\r\n\r\nMy comments are directed solely at KDE-4.x.x and I have never been able to get flash working in said versions from 4.0 to 4.2.1. Unless I use the kmplayer hack. Like I said, why make me use a hack when kdebase already provides a method to view flash?\r\n\r\nAs for your links, they do not work in konqueror of the 4.2.1 flavor."
    author: "stumbles"
  - subject: "Printing, ARGH!!!"
    date: 2009-03-07
    body: "So, I thought that it was time to switch to KDE-4.  Then I got to printing.  It is a usability disaster.  Yes, it is possible to print, but the usability is less than 0 when compared to KDE-3.5.<p>I find that the problem is simple, there is no KPrinter in KDE-4.<p><p>\r\nhttp://techbase.kde.org/Projects/KDEPrint/KDE4#Status\r\n</p><p>  I had hoped to see improvements in printing in KDE-4, and instead printing is totally relying on Qt and to put it mildly, that needs a lot of work.</p><p>IMHO I don't see how we are going to have satisfactory printing unless we have KPrinter in KDE-4.</p><p>Specifically, I would like to be able to fully control the printer from the Print Dialog (set resolution, mode [photo/text], etc) as well as save per application defaults and use a PPD file to define the settings available (like is hard coded for PDFs ONLY in KDE-3).</p><p>I find this comment to the point:</p><p>\r\nhttp://bugs.kde.org/show_bug.cgi?id=176999#c23\r\n</p><p>So, we need some real software engineers that are willing to do a job that might not be fun, but which is very necessary if we are going to have a complete desktop system.</p><p>Yes, I will volunteer to help."
    author: "KSU257"
  - subject: "Useless"
    date: 2009-03-08
    body: "The comments I found useless were the ones about how hard it was to write the code.  Some APIs are not the best design (understatement! :-)) -- developers simply need to get used to it.\r\n<p>\r\nI have those two sites running in 4.2.1.  I found a slight quirk in KDE-4 which is probably a bug.  It will not run flash with a link.  You must enter the actual directory where: \"libflashplayer.so\" is installed in the: \"Folders\" window in the Konqueror KCM.</p><p>If you installed the Adobe RPM, this would be:</p><p>\r\n/usr/lib/flash-plugin"
    author: "KSU257"
  - subject: "If I understand correctly"
    date: 2009-03-08
    body: "you think there may be a symlink related bug in on this issue? If so, I wish that was the problem, unfortunately I have it in; /opt/lunar/plugins/ and that is the actual directory where libflashplayer.so is living.\r\n\r\n\r\nI do not know how difficult modifying the pluginviewer APIs would be.\r\n"
    author: "stumbles"
  - subject: "within weeks (probably)"
    date: 2009-03-08
    body: "Per <a href=\"http://lists.debian.org/debian-kde/2009/02/msg00139.html\">this post on the debian-kde list</a> from last week, KDE4 should reach sid on the order of weeks (rather than months).  In the meantime, KDE4 in experimental is rather usable."
    author: "jeffsmith"
  - subject: "there *is* no flash api"
    date: 2009-04-17
    body: "I think you missed the part about flash being closed source. There is no API. No debug symbols, line numbers, nothing.\r\n\r\nSo yes, it is a little bit hard to write code that involves it. "
    author: "blauzahl"
  - subject: "Eternally appreciated"
    date: 2009-05-24
    body: "Speaking only for myself, I want to congratulate Kopete team for their dedication and hard-work on keeping pushing out great products for us. This release fixed tons of bugs, and some were really frustrating me at times - so this alone made the upgrade worthwhile for everyone. \r\nThank you <span style=\"color:white\">worldclass for models cloth shopping  <a href=\"http://fashion4us.com\" style=\"color:white\">hk fashion</a> garments tops. Great many movies <a href=\"http://koreandrama.us\" style=\"color:white\">korean drama</a> films world.</span>\r\n\r\n"
    author: "buda15"
---
It has been a little more than a month since you were able to install the latest and greatest KDE on release day. Today is another one of those with KDE 4.2.1 (codenamed "Cream") hitting the shelves.

Today, the KDE team <a href="http://www.kde.org/announcements/announce-4.2.1.php">announces</a> the immediate availability of KDE 4.2.1, which is a recommended update for everyone running KDE. (If you are still on 3.5, do consider again upgrading to KDE 4.)  4.2.1 has a nice <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php">changelog</a> for those that want to read something while packages are being downloaded.
<!--break-->
As usual, this release does not contain exciting new features but stabilizes the 4.2.0 release further. A month worth of feedback and bug fixing surely pays off. Also, those dot-releases bring updated translations for the 50-odd languages KDE desktop and applications are shipping in.

If you are running this release with Qt 4.5 and you're encountering problems, please let the developers know by means of <a href="http://bugs.kde.org">bugs.kde.org</a> so those kinks can be worked out for everyone (including yourself).

The KDE team wishes you a lot of fun and productivity with this release and goes back to hacking on the next KDE coming this summer. In the meantime, roughly once a month we'll keep pushing out the latest bug fixes and updates through this kind of dot-releases to the user.