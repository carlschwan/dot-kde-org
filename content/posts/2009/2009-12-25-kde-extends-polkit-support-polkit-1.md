---
title: "KDE Extends Polkit Support to polkit-1"
date:    2009-12-25
authors:
  - "drf"
slug:    kde-extends-polkit-support-polkit-1
comments:
  - subject: "list?"
    date: 2009-12-26
    body: "Is there a list of system settings modules and applications that will be using PolicyKit in KDE 4.4? The Kubuntu packages still use kdesu for e.g. the log-in manager, altering apt repositories etc., while actually installing/removing applications and other things like changing the time use PolicyKit."
    author: "madman"
  - subject: "From what I've seen, the only"
    date: 2009-12-27
    body: "From what I've seen, the only things using PolicyKit as of yet are KSysguard (for killing root processes), K3b, KPackageKit, the font installer module and the date and time module."
    author: "JontheEchidna"
  - subject: "3rd party applications"
    date: 2009-12-27
    body: "RecordItNow is also using it. And also, these KDE applications are not using PolicyKit directly, but through KAuth."
    author: "einar"
  - subject: "Shaman and many other tools"
    date: 2009-12-29
    body: "Shaman and many other tools from the Chakra project (they develop Arch KDE packages) use it as well, as I found out when it started segfaulting after an upgrade ;-)"
    author: "jospoortvliet"
---
Today, the first version of polkit-qt-1 and polkit-kde-1 have been released to the public. Thanks to these tools, KDE applications now integrate nicely with the new polkit-1 with a native authentication dialog. An authorization manager, the equivalent of the Polkit Authorization System Settings module, will be included in future releases. Find out <a href="http://www.freedesktop.org/wiki/Software/PolicyKit">more about PolicyKit</a> on Freedesktop.org.

Additionally, a new (and hopefully last) release of polkit-qt is available, fixing some trivial bugs. Please note that this new release is necessary for the upcoming KDE SC 4.4.

KAuth, the new authorization framework in the KDE Libraries, is already able to take advantage of the new polkit-qt-1; however, for KDE SC 4.4, polkit-qt will remain the default choice. This is going to change for KDE SC 4.5. To learn more about PolicyKit vs Polkit, polkit-qt and polkit-qt-1 differences, please read <a href="http://drfav.wordpress.com/2009/12/22/polkit-and-kde-lets-make-the-point-of-the-situation">this blog</a>.

The tarballs can be downloaded here:

<a href="ftp://ftp.kde.org/pub/kde/stable/apps/KDE4.x/admin/polkit-qt-0.9.3.tar.bz2">polkit-qt-0.9.3</a>
<a href="ftp://ftp.kde.org/pub/kde/stable/polkit-qt-1/polkit-qt-1-0.95.1.tar.bz2">polkit-qt-1-0.95.1</a>
<a href="ftp://ftp.kde.org/pub/kde/stable/apps/KDE4.x/admin/polkit-kde-1-0.95.1.tar.bz2">polkit-kde-1-0.95.1</a>

Special credits for this release go to the Fedora KDE Desktop Team, especially to Radek Novacek, Jaroslav Reznik and Lukas Tinkl.