---
title: "KDE-Edu Education Survey"
date:    2009-11-24
authors:
  - "coles"
slug:    kde-edu-education-survey
---
The <a href="http://edu.kde.org/">KDE-Edu</a> team is looking for feedback from their users, to help drive development in directions that best suit their needs.  All users are welcome to have their say.  A short <a href="http://spreadsheets.google.com/viewform?hl=en&amp;formkey=dFNwNjJSVXA0OGwyOGNvWXFaaTF4VWc6MA">five-minute questionnaire</a> has been created, asking for three key points where you think the KDE-Edu applications could be improved, as well as to gather general feedback. We look forwards to hearing your responses.
<!--break-->
