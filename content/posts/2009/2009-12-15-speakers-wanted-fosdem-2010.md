---
title: "Speakers Wanted for FOSDEM 2010"
date:    2009-12-15
authors:
  - "jriddell"
slug:    speakers-wanted-fosdem-2010
---
<img src="http://dot.kde.org/sites/dot.kde.org/files/fosdem-2010.png" width="200" height="200" alt="FOSDEM Logo" align="right" />
<a href="http://www.fosdem.org/2010">FOSDEM</a> is the largest gathering of Free Software developers in Europe. KDE has a room for talks on the Saturday and a crossdesktop room on the Sunday shared with Gnome and XFCE. We want you to give a talk on any KDE related topic.  Do you have an interesting program which more people should be using?  Maybe you want to show off a language binding. Are you part of a distro which uses KDE?  You could share cross desktop development experiences. Take a look at the <a href="http://archive.fosdem.org/2009/schedule/devrooms/kde">2009 schedule</a> for more inspiration.

To submit a talk add it to our <a href="http://community.kde.org/Promo/Events/FOSDEM/2010">FOSDEM 2010 wiki page</a>. The deadline is January 3rd.

We are also looking for helpers on the KDE stall. Please add yourself to the wiki page if you can take a turn talking to our users.
<!--break-->
