---
title: "Release Candidate for KDE 4.3.0 Out"
date:    2009-07-01
authors:
  - "sebas"
slug:    release-candidate-kde-430-out
comments:
  - subject: "Missing duel head support"
    date: 2009-07-01
    body: "As far as I know there is still no duelhead support (2 desktop on 2 screens) in kde 4 and therefore not good enough for me. I have tried with 1 desktop on 2 screens but I did not feel right.\r\n\r\nBut I hope the rest of you enjoued the new kde while the rest of os is stuck with kde 3.5.\r\n\r\nI am looking forward to joining you "
    author: "beer"
  - subject: "duel"
    date: 2009-07-01
    body: "@beer: I'm not sure what you exactly mean, but just yesterday I was able to do something like I think you want. I hooked up an external monitor to my laptop and set it up with nvidia config tool and it let me use it as \"twinview\" which let the monitor become another desktop/activity. At least to me, it seems you can set up another desktop/activity."
    author: "eyecreate"
  - subject: "Hey, I'd like that too, but"
    date: 2009-07-01
    body: "Hey, I'd like that too, but you don't hear me constantly whinging about it.  It's a very small minority who want that set-up, and none of the plasma devs use it, they all have Xinerama set-ups which plasma does support very nicely (yay, multiple sys-trays!).  As Aaron has explained several times, if someone with that set-up wants to work with the plasma devs to make it happen, they could try work something out, the architecture is there.\r\n\r\nReally, the whingers are having to find more and more obscure corner cases why not to adopt KDE4."
    author: "odysseus"
  - subject: "Dual-Head"
    date: 2009-07-01
    body: "Isn't that supposed to be dual and not duel?\r\n\r\nAnyway I would just like to say congrats to the no.1 desktop team in the world. Guys you are fast, innovative and doing a supurb job. keep it up.\r\ni don't know if it's just me but the RC feels snappier and is more responsive than the last beta and that's on the same hardware. kwin seems to have gotten a turbo engine ;)\r\n\r\nI have something to complain about nonetheless, even at this stage it's still not possible to configure KDM. This bug is there since KDE 4.0 and continue to move from version to version without anything being done to get rid of it AFAIK. Don't get me wrong, I am really impressed by KDE 4.3 but I think that this is a bit annoying and sad at the same time. When are we going to be able to get back this feature?"
    author: "Bobby"
  - subject: "Questions"
    date: 2009-07-01
    body: "Was Korganizer ported to Akonadi? Can it now easily sync with the Google Calendar? If so, Is there a nice plasmoid for administering the calendars (kind of like Lion Mail for Mail)?"
    author: "Krul"
  - subject: "If that is true I will use"
    date: 2009-07-01
    body: "If that is true I will use kde 4.3 as soon as it is ind debian unstable. Last time I tried it (kde 4.2.4 one of the screen was permanently black). I really hope it has been fixed"
    author: "beer"
  - subject: "I tried to look into the code"
    date: 2009-07-02
    body: "I tried to look into the code where the changes should be made. The little I know of C++ is not enough to get it fixed. So I can only tell the plasma devs that I am still here and I hope that they will fix it sooner rather than later. \r\n\r\nI have a few problem with Xinerama itself eks if I start a program on screen 2 sometimes it pups op at screen 1 and the other way around. And a lot of of open source games does not work well with Xinerama. Have you tried to play glob 2 or widelands in full screen? Those games then tries to fill the whole desktop (both screens). And the only way to make sure that they only cover one screen is to only have one desktop on one screen. The last problem with the Xinerama is my screens have different highs witch then make some of the desktop will not be visible \r\n\r\nEdit:\r\nSo you are basically saying that the devs do not care about kde users and those needs :) "
    author: "beer"
  - subject: "KOrganizer was not yet ported"
    date: 2009-07-01
    body: "KOrganizer was not yet ported to Akonadi. Actually, porting has just started in a separate work branch in SVN.\r\n\r\nThere is an Akonadi resource for accessing Google Calendar in extragear.\r\nThe good news is that you can use that in KOrganizer, even though Google Calendar is an Akonadi resource and KOrganizer is not ported to Akonadi yet!\r\nYou need to set up an \"Akonadi Compatibility Resource\" in KOrganizer, then you can you all Akonadi resources in KOrganizer. Enjoy!\r\n\r\nThere is no LionMail for calendars yet, to my knowledge. However, that shouldn't be too difficult to write with the high level APIs that Plasma and Akonadi provide."
    author: "TMG"
  - subject: "Re: KDM"
    date: 2009-07-01
    body: "> it's still not possible to configure KDM\r\n\r\nWhat is missing for you? I can open the KDM control module from System Settings, and everything seems to work fine (though I only used design selection and the \"Hide the user list\" setting up to now)."
    author: "majewsky"
  - subject: "No Administrator mode possible"
    date: 2009-07-02
    body: "Everything is missing! I can't change the default picture, i cant't change the theme, the font, I can't configure anything at all. Are you sure that you are using the RC or KDE 4.2.4 with the openSuse hack? The openSuse hack gives you a pop-up dialogue so you can enter you root password and configure KDM according to your taste but this is not possible if you are using \"strait\" KDE 4 packages and it's also missing in KDE 4.3.\r\nWhat I am talking about is now no.2 on the Bugzilla most hated bug list so it's definitely a problem."
    author: "Bobby"
  - subject: "4.4 material"
    date: 2009-07-02
    body: "Now that PolicyKit has been integrated with KDE, such a thing can be implemented. That would be 4.4 material. though."
    author: "einar"
  - subject: "Or 5.0 material ;) I would"
    date: 2009-07-02
    body: "Or 5.0 material ;) I would really love to see this feature working properly in KDE 4 but I don't want to put the devs under pressure and since I can't code and don't know how difficult this implemation is then I will continue to wait and hope that it will be ready for prime time by the time openSuse 11.2 is released."
    author: "Bobby"
  - subject: "Strigi Search - OS X Spotlight"
    date: 2009-07-02
    body: "Hey Guys,\r\n\r\nDoes anyone know whats going on with Strigi?  I was hoping Strigi was gonna be like Spotlight for KDE but so far I haven't found a client for it.  I heard it was gonna be integrated into Krunner.  Does anyone one know the status for such a feature?\r\n\r\nThanks,\r\n\r\nZubin"
    author: "zparihar"
  - subject: "\"Really, the whingers are"
    date: 2009-07-02
    body: "\"Really, the whingers are having to find more and more obscure corner cases why not to adopt KDE4.\"\r\n\r\nWas that necessary?"
    author: "majewsky"
  - subject: "If it's about missing root privileges..."
    date: 2009-07-02
    body: "then \"kdesu kcmshell4 kdm\" should do the trick for you, until 4.4 brings proper PolicyKit integration with System Settings."
    author: "majewsky"
  - subject: "Simple answer"
    date: 2009-07-02
    body: "As far as I'm concerned, the main problem is lack of developers. Some GSoC students are working on improving it, and first results should already be available from the nepomuksearch KIO slave (type \"nepomuksearch:/\" into Konqueror's or Dolphin's URL bar, but do not expect too much for the moment)."
    author: "majewsky"
  - subject: "This corner cases was"
    date: 2009-07-03
    body: "This corner cases was reported as a problem back in the kde 4.0 beta fhase as a problem for some users so I do not think it is obscure. Just becouse you do not need it other people do.\r\n\r\nFor me it is a killer function that I cannot live without. Becouse if this issuse I am beginning of looking for some other DE that suits my need but so far I have not found anything as good as old kde 3.5 (or kde 4 with dual head support).\r\n\r\nIt will be an interresting day when debian stops with support kde 3.5 and if this issue is not resolved"
    author: "beer"
  - subject: "About your problems with"
    date: 2009-07-03
    body: "About your problems with Xinerama:\r\n* usually, windows of most app pop up at the same location/size where the were when last opened. Which may mean that you click the shortcut on one screen and it opens up on another, but that's still sensible behavior imo.\r\n* most open source games indeed have an issue with that setup. That's because most open source games use SDL which doesn't handle xinerama or twinview very nicely. Besides reporting that bug to the SDL ( http://bugzilla.libsdl.org/ ), it's possible to work around this problem by manually adding the correct metamodes to xorg.conf. Something like:\r\n\r\nOption \"MetaModes\" \"1024x768-1024x768 1024x768 800x600 640x480\"\r\n\r\nThis way SDL can find resolutions for a single monitor, and it will work correctly.\r\n\r\n* I have 2 monitors of different heights too... and that's absolutely not a problem.\r\n\r\nOh, and it's not that the devs do not care about the users. But with limited resource (the amount of developers), you'll have to prioritize. Obsure configurations like having a X session for each screen simply get's a lower priority."
    author: "pinda"
  - subject: "I can understand that the"
    date: 2009-07-03
    body: "I can understand that the devs have limits amout of time since they often tend to work with open source in there spare time. But the problem has been know around 1 1/2- 2 year by now. Just like the famous root/admin button in system settings. Somethings just take a bid longer before it is ready. It is like food. Good food takes long time to make. \r\n\r\nKde 3.5 was released back in november 2005 and it is so old that it is in debian Old stable. I am looking forward to kde 4 will be ready. And hwn it is it will rock hard"
    author: "beer"
  - subject: "Applications disappears from process manager pannel"
    date: 2009-07-04
    body: "Hi,\r\nafter install kde 4.3 in kubuntu 9.04 from ppa repository my applications do not appear in proccess manager pannel. Is it possible?"
    author: "sergiobh"
  - subject: "Did you read my comment at all?"
    date: 2009-07-04
    body: "All I said is that I'm uncomfortable with people flaming on those who have problems with missing features in KDE 4."
    author: "majewsky"
  - subject: "Google Calendar"
    date: 2009-07-04
    body: "Is the stuff with the google Calendar documented somewhere? I dont find the place where I can set up an \"Akonadi Compatibility Resource\"."
    author: "etienner"
  - subject: "4.3 is great!"
    date: 2009-07-04
    body: "I installed KDE 4.3 overnight in Gentoo (using the \"kde-testing\" overlay via portage 2.2: emerge -C @kde; emerge -uDN world; emerge @kde), and I like it a lot! \r\n\r\nOn the first login plasma didn't look right (the corners were still Oxygen, the rest Air), but that was fixed in the second run (plasma needs a \"report bug\" function, I think), and I needed a bit of time to get used to the Air theme, but aside from that everything seems to work - and the shadows are fast, now! \r\n\r\nNow I just wait for a Starcraft2 theme :) \r\n\r\n... and for support for the video and audio tag in Konqueror :) "
    author: "ArneBab"
  - subject: "DrKonqui!"
    date: 2009-07-04
    body: "I just reported my first Bug directly from the DrKonqui wizard, and it's great! \r\n\r\nI'd wish every GNU/Linux program would use a similarly great crash wizard. It's damn convenient reporting bugs that way! \r\n\r\nMany thanks to the dev who wrote it! "
    author: "ArneBab"
  - subject: "You can set up additional"
    date: 2009-07-05
    body: "You can set up additional calendars/resources in the system settings, under the \"Advanced\" tab, the \"KDE Resources\" module. There you can add additional resources, and you should add a \"Akonadi Compatibility Resource\" if it is not already there. In the settings of the new resource, you can add Akonadi resources as calendar sources."
    author: "TMG"
  - subject: "\"Really, the whingers are"
    date: 2009-07-05
    body: "\"Really, the whingers are having to find more and more obscure corner cases why not to adopt KDE4.\"\r\n\r\nYeah, like being able to print odd/even pages or getting correct results when evaluating simple mathematical expressions in krunner mini-commandline."
    author: "odalman"
  - subject: "I will just notice people"
    date: 2009-07-08
    body: "I will just notice people that it has now been fixed in 4.4 and the fix will maybe be in 4.3.2"
    author: "beer"
---
Today, three days before the Gran Canaria Desktop Summit <a href="http://www.grancanariadesktopsummit.org/">starts</a>, the KDE team released <a href="http://www.kde.org/announcements/announce-4.3-rc1.php">KDE 4.3.0 RC1</a>. RC1 is an early candidate for what will become KDE 4.3.0 at the end of this month.
Artwork has now also been merged, and KDE 4.3 will have a new Plasma look, sported by the new, light "Air" Plasma theme. Oxygen, the default in earlier versions of KDE 4 is still available as an option through the "Desktop Settings" dialog. Please give this version some good testing so we can iron out last bugs and make 4.3.0 a smooth release. Note that KDE 4.3.0 RC1 is not suitable for end users.
<!--break-->
