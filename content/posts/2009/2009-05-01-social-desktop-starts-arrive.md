---
title: "Social Desktop Starts to Arrive"
date:    2009-05-01
authors:
  - "FrankKarlitschek"
slug:    social-desktop-starts-arrive
comments:
  - subject: "Yay!"
    date: 2009-05-01
    body: "Congratulations on getting it ready for the 4.3 release. This is simply an amazing feature. I am really curious where the further developments will take us. There is certainly a lot of potential.\r\n\r\nCheers!"
    author: "mutlu"
  - subject: "Doesn't this overlap with the"
    date: 2009-05-01
    body: "Doesn't this overlap with the opensocial API?"
    author: "patcito"
  - subject: "facebook"
    date: 2009-05-01
    body: "Will be possible to have there my Facebook friends out of the box?"
    author: "nasty_bad_boy"
  - subject: "not really"
    date: 2009-05-01
    body: "Hi,\r\n\r\nlike described in the article we plan do a lot more than the usual \"I\u00b4m a friend of\" functionality of classic social networks. Please see this only as a first step.\r\nExamples are: Free Software Events, Knowledge Base and User Support, document sharing, location based features and more. \r\nSo this is not about integrating an existing general interest community like Facebook.\r\nWe try to create something really new and  innovative here.\r\n\r\nCheers\r\nFrank"
    author: "FrankKarlitschek"
  - subject: "OpenSocial supports much more"
    date: 2009-05-01
    body: "OpenSocial supports much more than \"I'm a friend of\" with activities, permissions, apps, events, geo-localization etc http://www.opensocial.org/Technical-Resources/opensocial-spec-v09/OpenSocial-Specification.html"
    author: "patcito"
  - subject: "like i sad before. The vision"
    date: 2009-05-02
    body: "like i sad before. The vision of combining our strong free software community with our free desktop is much more than just another social network."
    author: "FrankKarlitschek"
  - subject: "it is of course possible to"
    date: 2009-05-02
    body: "it is of course possible to build a bridge/proxy to facebook or other commercial web communities if it makes sense.\r\nBut the really powerful collaboration services will probably provided by other webservices. For example forum.kde.org or other websites run by the free software community."
    author: "FrankKarlitschek"
  - subject: "Exactly what I was gonna ask."
    date: 2009-05-04
    body: "Exactly what I was gonna ask. Unless your service can top Facebook in terms of service and use, I was wondering if your going to support it? \r\n\r\nAlso, is this only going to cater to computer uses, say knowledge base, developer talk, or will this cater to normal users who use facebook or something to talk and social with there friends? "
    author: "Dekkon"
  - subject: "Tor - anonymity network"
    date: 2009-05-04
    body: "Does this plasmoid discover your public I.P. address from within the computer, or does it show what a server tells you is your I.P address? If it's the latter, then this could cause problems for Tor users. I'm thinking that it should show your I.P. address from within your system, though (which I believe JavaScript can do...).\r\n\r\nOtherwise, I'm really looking forward to seeing this implemented! I almost can't wait another few months for 4.3!"
    author: "madman"
  - subject: "Social Filesystem"
    date: 2009-05-04
    body: "A social filesystem would be cool, where you can see your friends public files. Groups could have a public r/w directory. \r\n\r\nA bit like Dropbox (getdropbox.com) but integrated with social networks.\r\n\r\nWho should store the files? I suggest two choices, either peer to peer with full replication or server based.\r\n\r\nPeer to peer model will sync whenever two from the group is online at the same time. perhaps a torrent or rsync like protocol would do.\r\n\r\nPrivate files could be encrypted and replicated as well."
    author: "erik.martino"
  - subject: "Kde 4.2.3"
    date: 2009-05-06
    body: "Any reason why the release of kde 4.2.3 isn't covered yet?\r\n\r\nAnyway, congrats on the new release !!\r\n"
    author: "MarkHannessen"
  - subject: "other social networks"
    date: 2009-05-10
    body: "On a conceptual level, this kind of functionality could also be implemented on top of facebook, providing they provide the data in a useful form (and of course for other social networks as well).\r\n\r\nThe goal of the Open Desktop Plasmoid and the underlying library work though is to explore those features on the desktop. The goal is  not to write a desktop facebook client, and I'm not aware of anyone working on this right now. That doesn't mean that it wouldn't be welcome (it certainly is if that's what you want to work on). I'm aware that Digikam does integrate with Facebook to upload photos as another example.\r\n\r\nThe applet is using the OCS dataengine, so if one were to write a Plasma DataEngine that allows interaction with facebook, the applet could easily also be used for that. In that case, it would probably make sense to review the interface for the OCS dataengine, as it's designed on top of the OCS API, not sure in how far this would be useful for Facebook, or similar social networks that happen mostly on the web at this point.\r\n\r\nI especially find the combination of social network and geolocation services interesting in this respect."
    author: "sebas"
  - subject: "I think it is great but ..."
    date: 2009-05-17
    body: "... the simplest feature that I miss in all KDE applications is an menu item under the file menu: \"Send via email ...\"\r\n\r\nI don't know how often I would like to send a kate text file, image, snapshot, etc. out of KDE to a friend. Then I always need to first save the document and then attach it to an email.\r\n\r\nSo much effort is put into making it easier to get in touch with each other. Why not have a simple \"Send via email...\" in the KDE applications to start with?\r\n\r\nJust my 5 cents ..."
    author: "mickru"
  - subject: "tor"
    date: 2009-05-17
    body: "Wait, you expect proper locations to be put on the public web after you went through the setup of a service that is primarily meant to not expose your real location?\r\n\r\nIdeally we end up using the wifi database for even more specific local information.  This basically is a service that allows you to send the list of wifi basepoints you detect and it will match it with the database to figure out where you are from just their names and strenghts (so no login required)"
    author: "zander"
  - subject: "thats one feature "
    date: 2009-05-17
    body: "I think it would be useful to have the ability to send a file via email, for sure.\r\nI do think it needs a bit more thought though. I mean, you want send via email, your friend might want send via skype and a 3rd might want to send the file via yet another way.\r\n\r\nI think we will end up with \"Send to Contact...\" which shows all your contacts and only then will it suggest different ways of sending the file based on availability."
    author: "zander"
  - subject: "It would be nice to be able"
    date: 2009-05-18
    body: "It would be nice to be able to choose WHO sees where I am - and WHERE they see it, yes. ;)\r\n\r\n\r\nI expect it won't be that much of a problem, anyway."
    author: "madman"
---
<p>
At last year's Akademy the vision of the  Social Desktop was born and first presented to a larger audience. The concept behind the Social Desktop is to bring the power of online communities and group collaboration to desktop applications and the desktop shell itself. One of the strongest assets of the Free Software community is its worldwide community of contributors and users who belief in free software and who work hard to bring the software and solutions to the mainstream. You can find more of this conceptual background on Frank Karlitschek's <a href="http://www.open-collaboration-services.org/socialdesktop.pdf">slides</a> from his keynote at Akademy 2008.
A core idea of the Social Desktop is connecting to your peers in the community, making sharing and exchanging knowledge easier to integrate into applications and the desktop itself. One of the ideas Frank addressed was to place a widget on the desktop where users can find other KDE users in the same city or region, making it possible to connect to these people, to contact them and to collaborate.
</p>
<!--break-->
<p>
If a user is starting KDE for the first time he has questions. At the moment a lot of the support for KDE users is provided through forums and mailinglists. Users have to start up a browser and search for answers for their questions or problems. The community is relatively loosely connected, it is spread all over the web, and it is often hard to verify the usefulness and accuracy of the information found somewhere out on the web. Although is works relatively well for experienced users, beginners often get lost.
</p>
<p>
Access to a lot of user-generated information offers a great way to provide online community support. This user-generated content comes from openDesktop.org right now and there is work going on integrating the <a href="http://forum.kde.org">KDE Forum</a> as knowledge Base as well, so people can help each other via the web, and application developers transparently integrate this knowledge into applications and the desktop.
</p>


<h2>Open Collaboration Services</h2>

<p>
OpenDesktop.org is a reference implementation of the <a href="http://www.freedesktop.org/wiki/Specifications/open-collaboration-services">Open Collaboration Services (OCS) API</a>. Open Collaboration Services API allows to exchange data relevant to the Social Network between the site and clients, such as other websites and applications or widgets running locally on the user's machine or mobile device. These services are:
<ul>
    <li><strong>Person</strong> allows to retrieve data sets of other people </li>
    <li><strong>Friend</strong> is used for handling friendship relations between people. It can be used for retrieval of your own friends and those of others, to invite persons as a friend and manage these invitations </li>
    <li><strong>Message</strong> is used for exchanging messages with other users </li>
    <li><strong>Activity</strong> provides a record of activities, such as adding content. It's basically "What is going on in my social network?" </li>
    <li><strong>Content</strong> includes listing and getting for example themes and wallpapers, content that is shared on the site </li>
    <li><strong>Knowledge Base</strong> offers a way to get help about a specific topic. This can be used to integrate community support into applications </li>
</ul>

<a href="http://www.opendesktop.org">openDesktop.org</a> is also the combined portal to <a href="http://www.kde-look.org">KDE-Look.org</a> and <a href="http://www.kde-apps.org">KDE-Apps.org</a>. openDesktop is an online social network, including content sharing. You can upload and download new wallpapers, themes, but also other content for application, such as dictionaries, and of course new Plasma widgets. Purchase of offering of content will be enabled in openDesktop in the future, so app store functionality can be integrated as well.
</p>


<h2>Start of a KDE implementation</h2>
<p>
During the openSuse hack-week, Cornelius Schumacher has written a small library and a test application to interact with <a href="http://opendesktop.org">openDesktop.org</a>. This OCS client libary wraps the webservice calls and XML handling into a Qt/KDE style API. The library is built to be used asynchronous, so the interaction on the network which can cause delays. This helps keeping applications that are using the network responsive.
</p>
<p>
Sebastian Kügler has been working on integrating Open Collaboration Services into Plasma by writing a dataengine and a basic applet. Plasma applets use this dataengine to visualise data, the dataengine makes it easy to query for this data, also from applets written in JavaScript, Python and Ruby and other languages.
</p>
<p>

A screencast shows how the mechanism is used in Plasma.
<embed src="http://blip.tv/play/grwO_pg6AA" type="application/x-shockwave-flash" width="500" height="430" allowscriptaccess="always" allowfullscreen="true"></embed>
<br />
<a href="http://vizzzion.org/stuff/kde/plasma-opendesktop.ogv">Ogg/Theora Version</a>
</p>
<p>

The openDesktop Plasmoid is a first basic applet using these services on the desktop. As an applet, it sits on your desktop or dashboard and displays information related to your friends. It also allows you to find people in your vicinity. The applet using a geolocation Plasma service to find out the current location and queries people near this location through OCS. The plasmoid displays those people and allows getting in contact with them, currently in a very basic way by pointing the web browser to the right page. The dataengine and the applet aim at being released with KDE 4.3 this summer in a release early, release often fashion for people to play with the technology and give feedback for further improvements.
</p>
<p>
<img src="http://vizzzion.org/stuff/kde/plasma-opendesktop-nearby.png" />
</p>


<h2>Ideas</h2>
We invite people to work with us on the community integration of the social desktop. There are many ideas, often not hard to implement, as they can also be written in JavaScript, Python and Ruby using Plasma's strong scripting capabilities.
</p>
<p>

Show what my KDE friends are doing at the moment - People love to see what is going on in their KDE friends network, did they post a new entry on their weblog? Where are they hanging out?
More examples are

<ul>
    <li>A friend of mine posted a new blog entry</li>
    <li>A friend of mine uploaded a new application or wallpaper on KDE-Look.org</li>
    <li>A friend of mine is going to the Linux User Group meeting tomorrow.</li>
    <li>There is an update for an application I am fan of.</li>
    <li>A friend of mine committed a new feature in the KDE SVN</li>
    <li>Someone was on my profile page and offered me a KDE related job</li>
    <li>A friend of mine is now a fan of "Amarok"</li>
</ul>


Another interesting area of research is using the Semantic Desktop's concepts to build this network. According to Richard Dale, FOAF (Friend-of-a-Friend) is an interesting ontology to investigate, others are SIOC for blogging info,
SCOT for tags that are connected to semantic web ontologies, rather then just being names and DOAP as description of a project.
</p>
<p>
Using data from Akonadi to support the dataset on the social desktop, and the other way round seems like another candidate, updating the location of a contact in Akonadi based on the data from the OCS service seems like an obvious candidate for a nice improvement. In general, there are very strong connections between the local address book and people you interact with on social web services.
</p>
<p>
The ground work for an online knowledge database has already been laid by Marco Martin. He added support for Knowledge Base to the ocs dataengine, writing an applet on top of it is now relatively easy. It would be great to have a plasmoid where users can directly query an online knowledge base system. For bonus points, add support for posting questions.
</p>
<p>
So join the fun and help us putting the community into the applications and the desktop. You can find us on the <a href="https://mail.kde.org/mailman/listinfo/social-desktop">Social Desktop</a> mailing list.

<h2>Pointers</h2>
<ul>
    <li>Mailing list: social-desktop@kde.org </li>
    <li><a href="http://techbase.kde.org/Projects/Social-Desktop">Wiki</a> </li>
    <li><a href="http://www.freedesktop.org/wiki/Specifications/open-collaboration-services">OCS API specification</a> </li>
    <li><a href="http://websvn.kde.org/trunk/playground/base/attica/">Attica</a></li>
</ul>