---
title: "KDE 4.3.3 Out Now: Clockwork"
date:    2009-11-03
authors:
  - "sebas"
slug:    kde-433-out-now-clockwork
comments:
  - subject: "KDE 4.3.3"
    date: 2009-11-03
    body: "Congrats to the KDE team! Roll on KDE 4.4 ."
    author: "guhvies"
  - subject: "Kubuntu  Debs"
    date: 2009-11-03
    body: "On the Kubuntu info page the reference to http://www.kubuntu.org/news/kde-4.3.3 only return the generic news page and not details about 4.3.3."
    author: "reagle"
  - subject: "so what?"
    date: 2009-11-04
    body: "i don't think this is the place to post that. kubuntu is not kde, it is just a distro that, in addition, didn't care too much about anything alse than gnome.\r\nif you want kubuntu people to update their news, you should post in their forums."
    author: "canolucas"
  - subject: "Actually, it is. Kubuntu has"
    date: 2009-11-04
    body: "Actually, it is. Kubuntu has not released its packages for 4.3.3 yet, so KDE should not link to that page yet. I find your attitude towards Kubuntu quite hostile, in any case."
    author: "JontheEchidna"
  - subject: "kubuntu FTW !"
    date: 2009-11-04
    body: "sorry, you are right. kubuntu is kde. :)"
    author: "canolucas"
  - subject: "That's not what I said..."
    date: 2009-11-04
    body: "I never said it was. I was merely saying that it is the fault of the KDE website that it links to a nonexistent webpage. Nothing more, nothing less."
    author: "JontheEchidna"
  - subject: "i agree that the link shouldn't be there ..."
    date: 2009-11-04
    body: "i agree that the link shouldn't be there. but i don't agree that you didn't say:\r\n\r\n> Actually, it is. Kubuntu has not released its packages for 4.3.3 yet, so KDE should not link to that\r\n> page yet.\r\n> I find your attitude towards Kubuntu quite hostile, in any case.\r\n\r\nwake up. *buntu is the same distro.\r\nit is like premium, basic, starter, home, ultimate, enterprice\r\ndifferent flavours for the same sh*t\r\n"
    author: "canolucas"
  - subject: ">wake up. *buntu is the same"
    date: 2009-11-04
    body: ">wake up. *buntu is the same distro.\r\n>it is like premium, basic, starter, home, ultimate, enterprice\r\n>different flavours for the same sh*t\r\n\r\nDo you even know what you're saying? \r\n\r\nKubuntu is an official Canonical supported and funded project. It is in no way same as Ubuntu which uses Gnome as a DE. Just as openSUSE offers KDE as default, Ubuntu offers Gnome.\r\n\r\nI personally don't find _any_ KDE feature missing in Kubuntu. There is no \"basic\", \"ultimate\" etc, as each and every *buntu flavor is in itself feature complete. Nevertheless, you can always install two or more desktop environments in one Ubuntu installation.\r\n\r\nI don't see the facts on which you are basing your rants upon."
    author: "sayakb"
  - subject: "kpilot still irrevocably broken."
    date: 2009-11-04
    body: "I am annoyed with the fact that I still cannot sync my Palm Treo to kdepim. Cellphones and PDA's are important devices, and the syncing of such devices to the Linux desktop should imho have a priority over eye candy and esoteric applications. \r\n\r\nSyncing to the cloud (e.g. Google calendar) is all very well, but only for a subset of the Calendar fields, and there is no solution for the contact address database. Also, the problem of how to get your PDA synced, remains.\r\n\r\nAnd I don't want to be dependent on a third party to administrate my appointments and addresses.\r\n\r\nHans Paijmans\r\npaai@uvt.nl\r\n\r\n"
    author: "paai"
  - subject: "Congrats to the release! \nAnd"
    date: 2009-11-04
    body: "Congrats to the release! \r\n\r\nAnd I'm glad to see a \"k\" enter the release name again! :-) \r\n\r\nMany thanks for your work! "
    author: "ArneBab"
  - subject: "Anyone who has used Kubuntu"
    date: 2009-11-04
    body: "Anyone who has used Kubuntu can attest to where most of Canonicals attention goes. GNOME gets it's own theme, while KDE loads up with the default theme. (Depending on whom you ask, this might be a good thing.) There was a discussion on Distrowatch a couple of weeks ago about whether or not Kubuntu was the blue-headed stepchild of Ubuntu.\r\n\r\nNow to be fair, if a user doesn't like the way that Ubuntu does things, than I doubt they'd like the way Kubuntu does things. Other than the DE, they are the same distro.\r\n\r\nAlso, the link now works. So Kubuntu user can enjoy KDE 4.3.3."
    author: "reub2000"
  - subject: "juk fixes, yay!"
    date: 2009-11-04
    body: "juk fixes, yay!"
    author: "mxttie"
  - subject: "Mandriva RPM:s?"
    date: 2009-11-04
    body: "The Mandriva rpm:s don't seem to be where they are supposed to be? Late arrival or?"
    author: "DagNygren"
  - subject: "Build Instructions: wrong link"
    date: 2009-11-07
    body: "On the Info Page for KDE-4.3.3, the link for the 'Build instructions' points to the instructions for building TRUNK.\r\n\r\nThe correct link is:\r\n\r\nhttp://techbase.kde.org/Getting_Started/Build/KDE4.x\r\n\r\nwhich gives instruction for building either a release or an SVN BRANCH."
    author: "JRT256"
  - subject: "...or they are updating"
    date: 2009-11-09
    body: "...or they are updating Tasktop and the \"Social Desktop\" along with making the upgrade to 4.3.3? That could explain the delay.\r\n\r\nPlease, I'm in the line of waiting. Could you drop us a line, Mandriva developers?"
    author: "Alejandro Nova"
  - subject: "dark screenshot"
    date: 2009-11-12
    body: "I created a screenshot of kde 4.3.3 with some mods to turn it into a dark theme - I thought you might be interested: \r\n\r\n<a href=\"http://draketo.de/files/kde-4.3.3-screenshot-dragons-ghost.png\"><img src=\"http://draketo.de/files/kde-4.3.3-screenshot-dragons-ghost.png\" width=\"400\" alt=\"kde 4.3.3 screenshot\" /></a>\r\n\r\n<em>(simply click the image to enlarge it)</em>\r\n\r\nThis uses the Aurorae theme engine with Ghost and the Ghost plasma theme. Apart from the image on the right, everything I used is free licensed (as far as I know). \r\n\r\nIt already served me as a nice tool to show the versatility of KDE, and I thought it could serve others the same way :). \r\n\r\n<em>(I posted detailed information how to theme your KDE that way in a <a href=\"http://draketo.de/licht/freie-software/kde/kde-433\">weblog entry</a> (german) and on <a href=\"http://kde-look.org/content/show.php/KDE+4.3.3+Ghost+and+Dragons?content=115023\">kde-look</a> (english, but lacking most resource-links))</em>\r\n\r\nPS: Only in KDE 4.3.3 because Aurorae didn't work flawlessly before."
    author: "ArneBab"
---
Like the ticking of a Swiss watch, every month the KDE team brings you a new release. <a href="http://kde.org/announcements/announce-4.3.3.php">November's edition of KDE</a> is a bugfix and translation update to KDE 4.3. With the KDE 4 series picking up in popularity, we're happy to encourage even more people to give KDE 4 another spin -- or just upgrade your existing KDE to KDE 4.3.3. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. Users around the world will appreciate that KDE 4.3.3 is more completely translated. KDE 4.4 is already translated into more than 50 languages, with more to come.

KDE 4.3.3 has a number of improvements that will make your life just a little bit better. Some of KWin's effects have been smoothed and freed of visual glitches, JuK should now be more stable, KDE PIM has seen its share of improvements while in the back-rooms of KDE, the developers are working hard on porting all applications to the new Akonadi storage and cache. The <a href="http://www.kde.org/announcements/changelogs/changelog4_3_2to4_3_3.php">changelog</a> has more, if not exhaustive, lists of the improvements since KDE 4.3.2.

Meanwhile, the KDE team is plugging away at KDE 4.4 with its feature freeze drawing closer. KDE 4.4 will see its first incarnation (KDE 4.4.0) in February 2010, two weeks later than initially planned in order to make it possible to release on top of Qt 4.6. KDE 4.4 will be based on, and using the new goodness in Qt 4.6 -- think of new layouting mechanisms in QGraphicsView, improved performance, the new animation framework, and many more pearls. KDE 4.4 will deliver a staggering amount of improvements and new features on top of this: improved desktop search, better privilege escalation, remote controllable Plasma widgets, and of course more polish to the existing code. Beta 1 is only one month away so stay tuned.