---
title: "2nd Beta of KDE SC 4.4 Available"
date:    2009-12-21
authors:
  - "sebas"
slug:    2nd-beta-kde-sc-44-available
comments:
  - subject: "QTextBrowsere bug"
    date: 2009-12-21
    body: "is the \"High CPU usage in QTextBrowser\" bug fixed? It made konversation use so much CPU that I had to switch to xchat :/\r\nI know this is a Qt issue but maybe there's a patch in qt-copy or something.\r\n\r\nhttp://bugreports.qt.nokia.com/browse/QTBUG-6281\r\n\r\nedit: kde bug https://bugs.kde.org/show_bug.cgi?id=215256\r\n\r\nif you're a big user of konvy you may want to wait before upgrading to the beta."
    author: "patcito"
  - subject: "KMix icon"
    date: 2009-12-21
    body: "Please.... is it possible to change the icon for KMix?\r\n\r\nIn ControlCenter we've got the beautiful speaker icon, and in the taskbar we've got the KDE 3 'old style' icon. It looks so out of place in a KDE 4 desktop...\r\n\r\nJust a small remark. For the rest: I'm looking forward to KDE 4.4!\r\n\r\n"
    author: "H. ten Berge"
  - subject: "Not KDE3"
    date: 2009-12-21
    body: "...this is the KDE3 icon: http://i283.photobucket.com/albums/kk282/Chrysantine/KDE3-AudioFix1.png"
    author: "JontheEchidna"
  - subject: "I'm still waiting..."
    date: 2009-12-22
    body: "I'm still waiting someone to fix this plasma bug:\r\n\r\nhttps://bugs.kde.org/show_bug.cgi?id=187406\r\n\r\ntill now, it get only 800 vote. It is filed on 2009-03-17. If I was programmer, I will fix it by myself."
    author: "zayed"
  - subject: "As a workaround you can"
    date: 2009-12-22
    body: "As a workaround you can switch to a monospace font temporarily, and some users have also reported success with playing with the font size and toggling subpixel hinting."
    author: "Eike Hein"
  - subject: "Monospace considered feature"
    date: 2009-12-22
    body: "Using a Monospace font for IRC is not a workaround, but it's really the only real way to use it.\r\n\r\n\r\nHow else would you watch ASCII renderings of Santa Claus? ;)\r\n"
    author: "Mark Kretschmann"
  - subject: "pykde4 4.4 beta2 builds on Gentoo!"
    date: 2009-12-22
    body: "In this beta, pykde4 builds again! \r\n\r\nAnd it's great to hear that compositing is back! I'll reenable it once I get home. \r\n\r\nMany thanks for your great work! \r\n\r\nPS: Didn't the release schedule say \"2009-12-22\"? \r\n\r\nPPS: That's a hellish pace you're coding at. I didn't even get to test all of the first beta when the second beta came out. \"Hut ab\" (a german phrase similar to 'kudos' :) ). "
    author: "ArneBab"
  - subject: "Fonts"
    date: 2009-12-22
    body: "@Promo team:\r\nFor the next anouncement' screenshots, please, consider using another font like Droid Sans. It's something you don't pay attention while it can do big diference in the loook of the software."
    author: "javi-azuaga"
  - subject: "Wow! You guys are so"
    date: 2009-12-22
    body: "Wow! You guys are so industrious. Congrats, I have to go and get my update now :)"
    author: "Bobby"
  - subject: "i have to agree with him"
    date: 2009-12-22
    body: "I have to agree with him though. The icon in Control Center looks a lot better."
    author: "Bobby"
  - subject: "Screenshot?"
    date: 2009-12-22
    body: "Can you post a screenshot to show what KDE 4.4 would look like with Droid Sans?\r\n\r\nAnd congrats to the KDE team. I switched back to Windows a while ago, but KDE looks quite promising again. Too bad that Adobe's Creative Suite is Windows/Mac only. Linux as a desktop really grows up. I see Linux notebooks every day that suspend just as fast as macbooks do. If KDE will continue like this, I'm confident it might find it's way onto desktops if enough commercial applications will be ported to Qt."
    author: "Earthnail"
  - subject: "beta2 release date"
    date: 2009-12-22
    body: "We wanted to get beta2 out as quickly as possible, there's really not much point in waiting, when in the meantime people can test a new release (and thus not report fixed bugs more than once). Hence we released it as soon as we were comfortable with the packages."
    author: "sebas"
  - subject: "resume speeds depends on graphics driver"
    date: 2009-12-22
    body: "The speed of suspending is pretty much completely dependant on the video driver. Some drivers have to reinitialize the graphics card completely, that usually takes a couple of seconds (and makes suspend/resume feel slow). The Intel driver does this properly, as such, with those drivers, you'll get lightning-fast resumes. (On the machine I've tested it, at least.)"
    author: "sebas"
  - subject: "Fonts and branding"
    date: 2009-12-22
    body: "I'm actually not sure that using Droid Sans is a good idea.\r\n\r\n- Fonts are closely related to branding. Good examples are Apple, and Nokia -- they use standard fonts that are even -- I think -- subject to their trademark. So using a font that is part of such a branding (like Android) might not make much sense\r\n\r\n- The font used, Liberation Sans, works really well on most displays I've seen it. While you might not like it a lot (I do, the Oxygen team does), it's a very good font (technical, has the right subpixel rendering hints) and well legible.\r\n\r\nIt's still a good idea to have screenshots with more fonts available, you can just post them to FlickR, for example and they'll be found when people search screenshots. There's also a topic about this on the KDE Forums (IIRC)"
    author: "sebas"
  - subject: "Not really suitable for"
    date: 2009-12-22
    body: "Not really suitable for notifying the current volume, though. A two dimensional icon is much more suitable for that purpose."
    author: "JontheEchidna"
  - subject: "Don't bother asking for a"
    date: 2009-12-22
    body: "Don't bother asking for a screenshot, believe me, it's butt ugly. The droid fonts really suck on a large screen, made me wanna puke*. And I'm not critical of fonts, heck, my favorite has been bitstream vera sans for years. Only recently switched to liberation.\r\n\r\n*I must admit making me wanna puke ain't that hard, I'm pretty ill right now so I'm off back to bed"
    author: "jospoortvliet"
  - subject: "Are you sure it's Liberation Sans?"
    date: 2009-12-22
    body: "<blockquote>The font used, Liberation Sans, works really well</blockquote>\r\n\r\nAre you sure it's Liberation Sans in the screenshots? I normally use Deja but just switched to Liberation to have a look and it's a really nice font on my screen, but a number of things are different not least it looks a lot narrower and untidy on the screenshot. Weird :/\r\n\r\nIf it is, it might be worth tweaking the settings as it's definitely not showing off it's potential!"
    author: "dalgaro"
  - subject: "I take it back..."
    date: 2009-12-22
    body: "...it is Liberation. I managed to tweak it to look like the screenshot for better or worse! It might be worth considering DejaVa Sans which is also open source and quite light looking on the desktop."
    author: "dalgaro"
  - subject: "\"and for many users,"
    date: 2009-12-23
    body: "\"and for many users, compositing in KDE's Window Manager is back again.\"\r\nThis is for me the best part of the update.\r\nI can read what is under my konsole again!\r\n\r\nThanks a lot KDE!"
    author: "acemo"
  - subject: "It's all a matter of taste I"
    date: 2009-12-23
    body: "It's all a matter of taste I think. That#s why people should choose their individual fonts. I used to use Bitstream but then switched to DejaVu Serif. I just tried liberation and I think that it's even nicer than the two that i mentioned. It's easy to read and that's what it's all about."
    author: "Bobby"
  - subject: "KDE vs KDE SC"
    date: 2009-12-23
    body: "What's the difference between KDE SC and regular KDE?"
    author: "hensema"
  - subject: "Have a read:"
    date: 2009-12-23
    body: "Have a read: http://dot.kde.org/2009/11/24/repositioning-kde-brand"
    author: "Eike Hein"
  - subject: "good point"
    date: 2009-12-24
    body: "The Compositing problem turned out to be not that big of a problem. A blacklist check was turned (accidentally) into a whitelist check, which gave reason to improve a couple of other aspects of that piece of code. The timing though was unfortunate (no compositing in the beta for many users). Anyway, that's fixed.\r\n\r\nYou bring up one good reason to use compositing effects. I'm using a translucent background on konsole again, and indeed it's often very useful to monitor something behind it, or just to refer to some text in another window. As opposed to many depicting it as useless eyecandy, it definitely improves a couple of usecases for me."
    author: "sebas"
  - subject: "KDE4 with Droid Sans"
    date: 2009-12-24
    body: "Absolutely agree. An old post in kdeforums with screenshot:\r\n\r\nhttp://forum.kde.org/brainstorm.php#idea62208"
    author: "joethefox"
  - subject: "same for me"
    date: 2009-12-27
    body: "I also often turn a window semi-transparent to work more smoothly. \r\n\r\nIt's great for example when I write an article about another text (text in background), or rework some structured text (PDF in background, corrections in semi-transparent foreground window). \r\n\r\nDifferent from using two windows side by side it needs less screen space and saves eye movements :) "
    author: "ArneBab"
  - subject: "Desktop Grid"
    date: 2009-12-28
    body: "I was watching a video on 4.4 features and saw a desktop grid view.  My first question is, how do I activate it?  Secondly, does the desktop view show virtual desktops or does it actually show your activities?  Hopefully  it's for activities, because the current method of selecting activities (zoom out and select activity from a canvas) is kind of clumsy."
    author: "cirehawk"
  - subject: "Activate and use Grid"
    date: 2009-12-28
    body: "Start Menu - Configure Desktop (personal Settings) - General - Desktop - General - Desktop Effects - All Effects and type Grid (search function). Activate it (set a tick) if it's not activated and click apply.\r\nClick screen Edges (second at the left below Desktop Effects). Left click on one of the edges of the shown monitor (the edge that you want to use for Grid) and select Grid. Apply and you are done. Point your mouse in the selected edge and the Grid will show you the Virtual Desktops along with the given activities or Desktop Type."
    author: "Bobby"
  - subject: "You can already use it."
    date: 2009-12-29
    body: "The Desktop Grid is feature of 4.2/4.3 but the KDE Plasma-desktop 4.4 offers a Desktop Grid + Presentation -effect.\r\n\r\nAnd you can switch between activities by adding to the panel a widget to do it. You find it from 4.3. Then you do not need to zoom out/in. The KDE Software Compilation 4.5 should be focused to Nepomuk+Activities and their use. "
    author: "Fri13"
  - subject: "I prefer a verdana font, with"
    date: 2009-12-31
    body: "I prefer a verdana font, with no antialising.\r\n<a href=\"http://img707.imageshack.us/i/kde43.png/\" target=\"_blank\"><img src=\"http://img707.imageshack.us/img707/721/kde43.th.png\" border=\"0\" alt=\"Free Image Hosting at www.ImageShack.us\" /></a><br /><br /><a href=\"http://img604.imageshack.us/content.php?page=blogpost&files=img707/721/kde43.png\" title=\"QuickPost\"><img src=\"http://imageshack.us/img/butansn.png\" alt=\"QuickPost\" border=\"0\"></a> Quickpost this image to Myspace, Digg, Facebook, and others!"
    author: "juliusray"
  - subject: "Wonder how long it will stick"
    date: 2010-01-03
    body: "I wonder how long the new name for the same old thing will stick. Brand names are a powerful thing, and people won't just change to the new name just because the marketeers want them to. Especially when they just add some meaningless letters to the original name."
    author: "hensema"
  - subject: "We can't"
    date: 2010-01-08
    body: "As much as I would like to KDE as no saying in the font used (distros do) we recommend only, and we use generic font names, depending on your screen dpi I would recommend a range of settings that go from droid font with rgb inting average size 9, to liberation with full aa hinting average size 7, unfortunately i cant go to all of your screens and tweak it for you, and judging screen-shots doesn't help as each of them will look better with different settings.\r\n\r\nAny way my recommendation for most screens is average size 8 Liberation Sans with average hinting and no RGB hinting. It gives a nice sharp easy to read font.  "
    author: "pinheiro"
---
Today, KDE has <a href="http://kde.org/announcements/announce-4.4-beta2.php">released a second beta</a> version of the 4.4 series, which will debut with a stable release in February 2010. While the first beta of 4.4 has been quite a bumpy ride, most of the grave problems have been fixed. KDE-PIM should be fully operational again, and for many users, compositing in KDE's Window Manager is back again.
Meanwhile, the bug squashing frenzy is in full swing and will continue for another month until KDE 4.4 will be branched and frozen for release. Please help us test this version (you will have some time over christmas, no?), so we can make 4.4.0 and its subsequent releases ever more stable and pleasant to work with.