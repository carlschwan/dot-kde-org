---
title: "Linux Journal Reviews Kdenlive"
date:    2009-10-16
authors:
  - "troy"
slug:    linux-journal-reviews-kdenlive
comments:
  - subject: "nice"
    date: 2009-10-16
    body: "Interesting review, I didn't know KDEnlive has progressed that much. I've tried it a while ago, but it crashed all the time so I hope the next release will be usable for screencasting & editing them :D"
    author: "jospoortvliet"
  - subject: "A good and fair review"
    date: 2009-10-16
    body: "I used to struggle to use Cinelerra but now I also use Kdenlive.  I agree it's quickly become usable.  The interface is again, like KOffice's and IMO all of KDE's, Kdenlive's big advantage.\r\n\r\nI know it's been suggested elsewhere to bundle Kdenlive with Krita and Karbon, and without knowing much about much, it seems logical to me.  Kdenlive covers a very important software niche very well, and the overlap in users and usability would probably add to the popularity of all three apps."
    author: "Jerzy Bischoff"
  - subject: "Another KDE4 for Mac OS X "
    date: 2009-10-20
    body: "I just want to add that the latest release works on OS X now, available in MacPorts."
    author: "ddennedy"
  - subject: "The single biggest problem"
    date: 2009-10-22
    body: "The single biggest problem with this is that Krita and Karbon share much of the KOffice code, whereas KDEnlive doesn't share any. You don't get flake shapes in KDEnlive, for example."
    author: "madman"
---
Over at Linux Journal, Dave Phillips takes a look at the video editor <a href="http://kdenlive.org/">Kdenlive</a>. This KDE 4 application allows users to download video from camcorders or capture screen recordings, edit audio and video tracks, and more. Phillips concludes: "<em>There's a lot to like about Kdenlive, and I like it a lot. Its feature set is full enough to satisfy basic desktop video production needs, and its workflow is uncomplicated and easy to learn. However, it must be considered that I've reviewed a personal build from SVN sources.</em>" <a href="http://www.linuxjournal.com/content/kdenlive-meets-studio-dave">Read the full article</a>.
<!--break-->
