---
title: "KDE 4.2 Released"
date:    2009-01-27
authors:
  - "jriddell"
slug:    kde-42-released
comments:
  - subject: "Congratulations!"
    date: 2009-01-27
    body: "This is a great day for free software :)"
    author: "canesalato"
  - subject: "thanks!"
    date: 2009-01-27
    body: "for free software !! power to the users !"
    author: "wintersommer"
  - subject: "Good job!"
    date: 2009-01-27
    body: "Good job!\r\n\r\nI may gripe from time to time about the odd KDE related thing (Cough Toolbox, Cough Aya as default.) but this is really a fabulous release. The KDE modules have for the most part closed the gap with 3.5, the desktop / Plasma is in great shape, and the 3rd party apps have mostly arrived (I'm still lusting for a KDE 4 K3B and Kaffeine).\r\n\r\nIt's nice to know that from here on it's pretty much 'all upside'..."
    author: "Shade"
  - subject: "Aya as default?"
    date: 2009-01-27
    body: "If you do not like Aya as default desktop theme, blame your distributor. Upstream KDE ships Oxygen as default."
    author: "majewsky"
  - subject: "Well done!"
    date: 2009-01-27
    body: "Much goodies to all people (developer, translators, bug reporters, packagers, etc ,etc) involved in getting this release out the door.  It's a good release and it's nice to be able to work fulltime in this release.\r\n\r\nCongratulations kde!"
    author: "radoeka"
  - subject: "\"The Answer\""
    date: 2009-01-27
    body: "But what is the question?\r\n\r\nCongratulations to the KDE community! This is a release many have waited for, me included. <br><br>\r\n\r\nSo long KDE3, and thanks for all the fish://.<br>\r\n\r\n(With that said, I don't think I'll throw away KDE 3.5.x just yet)."
    author: "Hans"
  - subject: "Congratulations!"
    date: 2009-01-27
    body: "What a day! Really I'm happy. Thank you for this great release."
    author: "zayed"
  - subject: "the cashew"
    date: 2009-01-27
    body: "I know this has been discussed to no end, but I find it discouraging that I can not remove the cashew from my desktop. I don't use it. It gets in my way. I don't care if it disappears forever but I do want to be able to remove it without much fuss. \r\n\r\nOther than that KDE 4 has turned out great. Thanks to all the devs for your hard work."
    author: "tubasoldier"
  - subject: "Great release! Congratulations!"
    date: 2009-01-27
    body: "That is first really good release. Thanks, KDE team!\r\n\r\nAlthough I must say that exactly this release should be named KDE 4.0.\r\nPlease, don't make marketing and PR mistakes in future.  "
    author: "alukin"
  - subject: "Congrats to KDE team!!!"
    date: 2009-01-27
    body: "You almost  had me fooled in giving you the \"question\", until I remembered those lovely dolphins :D\r\n\r\nKde 4.2 is beautiful!!!!"
    author: "Autumnautist"
  - subject: "It can easily be removed,"
    date: 2009-01-27
    body: "It can easily be removed, just use this plasmoid:\r\n\r\nhttp://kde-look.org/content/show.php/I+HATE+the+Cashew?content=91009"
    author: "vld"
  - subject: "Aya as default"
    date: 2009-01-27
    body: "AFAIK Aya is the only style that obeys the color preferences of the user (while, if I'm right, the user cannot set the colors of the Oxygen style at all). Good decision from openSUSE to make Aya the default. Eye-candy is good as optional but in the default style configurability and consistency should be prefered over it."
    author: "gd"
  - subject: "Actually"
    date: 2009-01-27
    body: "Actually I'd prefer Aya AS the default. But no flame fest on the topic is required... Hey, it's KDE and easy enough to change. I'm just happy to see 'a really good' 4.2 out and most of the big 3rd party apps ported and ready.\r\n\r\nMy OP should be read as 'I gripe but this is really good' not 'this is really good but I gripe'. :)"
    author: "Shade"
  - subject: "Great!!!"
    date: 2009-01-27
    body: "hi, thanks to dev team for another great release."
    author: "NuLL3rr0r"
  - subject: "Amazing!"
    date: 2009-01-27
    body: "You can expect me to install KDE 4.2, whatever distribution I use - and to show it off to anyone that claims Linux isn't good enough!"
    author: "madman"
  - subject: "thanks"
    date: 2009-01-27
    body: "thanks"
    author: "tubasoldier"
  - subject: "Visual Guide"
    date: 2009-01-27
    body: "Spectacular work on the visual guide.  I think many will find it quite helpful.\r\n\r\nAlso, despite the fact that I am not really a fan of widgets (any implementation of), I like the concept of having a quick shortcut to one if I need one.  I don't want them on my desktop all the time, but quickly calling up pastebin, or a calendar, or a dictionary might be useful.\r\n\r\nTonight I will install the openSUSE packages and give it a try and see if I like KDE 4.x any better this time around."
    author: "enderandrew"
  - subject: "complain to your distro."
    date: 2009-01-27
    body: "complain to your distro.  OpenSuse have given people the option to have it or not have.  So should your distro."
    author: "Alien Healer"
  - subject: "Awesome!"
    date: 2009-01-27
    body: "I've been using KDE4.2 for a while now (since beta something) and I feel that 4.2 is finally good enough to replace my trusty old 3.5. I still miss a few details here and there (esp. in konq file browsing), but as a whole 4.2 beats out 3.5\r\n\r\n<p>Great work everybody!\r\n\r\n<p>Btw, is Nepomuk really slow or is it some misconfiguration? I distinctly remember some lecture where someone claimed Nepomuk (actually strigi) was way faster than all other desktop search engines, but on my OpenSuSE 11.1 Beagle behaves very nicely while nepomukdaemon eats a lot of CPU when surfing the web.\r\n"
    author: "SP"
  - subject: "OK"
    date: 2009-01-29
    body: "<br>The first post here was harsh-- At this point I think most people have accepted neither the default 'Desktop' or 'Folder View' will make the Toolbox / Cashew 'go away'. For those of us that find it annoying, it generally doesn't rise to the level of inducing a person to switch distros or DEs. At this point what grates and boggles the mind to a certain degree is that a Toolbox / Cashew optional (or free) alternative isn't shipping directly with KDE (With that guarantee of support from KDE and Distro Packagers.)</br>\r\n\r\n<br>I prefer Aya-- I can switch to Aya. Happy.</br>\r\n\r\n<br>I prefer no Toolbox / Cashew I can switch distros, go someplace and follow some process, or hope for something on 'New Stuff'. That's kind of the opposite of what many of us KDErs are used to when it comes to things that have a pretty reasonable level of demand. (It wouldn't be a 'one man and his goat' type feature.)</br>\r\n\r\n<br>There's a compounding of annoyances there, IMHO.</br>"
    author: "Shade"
  - subject: "Make 4.3 a bugfix-only release, I don't see any other solution"
    date: 2009-01-27
    body: "I wanted to show you how horrible that system notification plasmoid is but <br/>\r\nwhen I started copying something kde just got frozen or something crashed \r\n<br/>\r\nso I can only show you this:\r\n<br/><br/>\r\n-rw------- 1 test users  16M 2009-01-25 19:41 core.10280<br/>\r\n-rw------- 1 test users  23M 2009-01-25 19:47 core.10693<br/>\r\n-rw------- 1 test users  15M 2009-01-25 19:48 core.10751<br/>\r\n[...]<br/>\r\n<br/>\r\nOn the other hand, maybe is better to just show you the screenshot without<br/>\r\nthe sysnotif. plasmoid. I put the gkrellm over the cashew just I don't have<br/>\r\nto look at it, but it also has a nice feature of showing how nice kde4 is to<br/>\r\nyour CPU. Btw. there's no fonts in plasma, asegio said it's a driver<br/>\r\nproblem (radeon, open source):<br/>\r\n<br/>\r\nhttp://i42.tinypic.com/4u9f5t.png<br/>\r\n<br/>\r\nMy conclusion is that KDE4.2 is buggy, crashy, slow (could be qt4 fault), <br/>inconsistent and extremely unpolished desktop environment... so if <br/>you want\r\nit to succeed then there's only one solution: make 4.3 a bugfix-only<br/>\r\nrelease.  There are just too many bugs, but I think that six months of<br/>\r\nbugfixing could create a usable product out of kde4.<br/>\r\n<br/>\r\nBtw, you still have red/yellow items in:<br/>\r\n<br/>\r\nhttp://techbase.kde.org/index.php?title=Schedules/KDE4/4.2_Feature_Plan<br/>\r\nhttp://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Feature_Plan<br/>\r\n<br/>\r\nmaybe some of the fanboys could help here if programmers don't have time?<br/>\r\n"
    author: "vedranf"
  - subject: "Re: Awesome!"
    date: 2009-01-27
    body: "Nepomuk is not a desktop search engine, that is Strigi. If nepomukdaemon is having a high CPU load, but not strigidaemon, that smells to me like a bug in Nepomuk or one of its clients (without knowing much of the subject)."
    author: "majewsky"
  - subject: "Awesome"
    date: 2009-01-27
    body: "Using my OpenID on the dot and running pacman -R kdemod-uninstall && pacman -S kdemod-complete from chakra =)\r\n\r\nhttp://chakra-project.org/"
    author: "patcito"
  - subject: "Re: fanboys"
    date: 2009-01-27
    body: "I'd like to see how fanboys all of a sudden do the programmer's work. Programming is an art that requires long training to do it right, esp. in code like Plasma's that partially fiddles around with the Xlib directly.\r\n\r\nConcerning the feature plans: You contradict yourself. You tell us to slow down feature development, yet you moan about unfulfilled feature plans.\r\n\r\nAbout the Plasma problems: That is one problem. The other one is a driver problem (as you said yourself). We are not responsible for fixing the ISVs drivers, point (says a NVidia user).\r\n\r\nThere is a point in that Qt 4.4 is a bit slot, it has lacked optimization work in the past. But Qt performance is going to shine again in 4.5, as far as I can consider."
    author: "majewsky"
  - subject: "A great release!"
    date: 2009-01-27
    body: "He KDE-folks,\r\n\r\nthis is a really great release. It looks mostly polished and stable. I guess some of the minor probs are still there from RC1, but still I can start preparing for migration. I hope that next Kubuntu will have a better release then the last. Or is there any other cool and stable.deb-based KDE-distro out there?\r\n\r\nMark"
    author: "mark"
  - subject: "Flash?"
    date: 2009-01-27
    body: "The release announcement contains a Flash video? WTF? Adobe Flash is not Free Software, is not an open standard, and is not available on all platforms. Official KDE announcements should not be requiring proprietary Flash. At the minimum, KDE should at least provide a link to an alternate format."
    author: "Brandybuck"
  - subject: "Blip.tv has OGG as well"
    date: 2009-01-27
    body: "The video is hosted on blip.tv, which also offers a Free alternative."
    author: "einar"
  - subject: "My personal review on 4.2"
    date: 2009-01-27
    body: "Great job!  I've been using the 4.2 development releases on Kubuntu 8.10 and wrote a rant/review on my experiences <a href=\"http://randomtechoutburst.blogspot.com/2009/01/kde-42-im-tired-of-pundits-heres-my.html\">here</a>. \r\n\r\n In short: I think KDE 4.2 is ready to replace 3.5 as the primary desktop for the average user, and I'm excited to see what will be coming next.  I also attack some of the misconceptions that have grown around KDE 4 because of earlier releases, and because of people not trying to actually use KDE 4 the right way.\r\n"
    author: "CajunArson"
  - subject: "Damn, you got it all wrong ;)"
    date: 2009-01-27
    body: "> I'd like to see how fanboys all of a sudden do the programmer's work. <br/>\r\n <br/>\r\n...read more carefully, I meant they could update the feature plan. <br/>\r\n <br/>\r\n> You tell us to slow down feature development, yet you moan about <br/>\r\n>unfulfilled feature plans.\r\n <br/> <br/>\r\nAgain wrong. I'm just telling that those lists should be updated: if feature is  <br/>done then make it green, if not (re)move it.\r\n <br/>\r\n> The other one is a driver problem (as you said yourself) <br/>\r\n <br/>\r\nEven if it is a driver problem, everything else works fine with them. This  <br/>\r\nhappens with radeon drivers and to less extent with intel. And then you say  <br/>that nvidia drivers are bad. :-) <br/>\r\n\r\n <br/>\r\n> But Qt performance is going to shine again in 4.5\r\n <br/>\r\nThey told me so when 4.4 was about to be release.\r\n <br/>"
    author: "vedranf"
  - subject: "Thanks KDE developers"
    date: 2009-01-27
    body: "I love how customizable is KDE4:<BR>\r\nhttp://imagebin.ca/view/aFDl2JY.html<BR>\r\nhttp://imagebin.ca/view/GJg5hwS.html<BR>\r\nhttp://imagebin.ca/view/11KnRp.html<BR><BR>\r\n\r\nAll that I wait for 4.3 is the possibility of decently link activities with virtual desktops with a GUI and panel independency on each different activity. :-)"
    author: "xapi"
  - subject: "I agree"
    date: 2009-01-27
    body: "I would either like to see Aya get some polish to look as nice as Oxygen, or to see Oxygen respect color preferences."
    author: "enderandrew"
  - subject: "You're welcome"
    date: 2009-01-27
    body: "> ...read more carefully, I meant they could update the feature plan.\r\n\r\nWhere? Only words count that were actually written.\r\n\r\n> you say that nvidia drivers are bad.\r\n\r\nNo, I referenced that NVidia users had a hard time. ATI users are still having their hard time, as you have noticed. That is not a contradiction.\r\n\r\n>> But Qt performance is going to shine again in 4.5 \r\n> They told me so when 4.4 was about to be release. \r\n\r\nI did not hear of that, ever."
    author: "majewsky"
  - subject: "Breathe..."
    date: 2009-01-27
    body: "Make's me think of the Pink Floyd song:\r\n\r\n\r\nBreathe, breathe in the air\r\n\r\nDon't be afraid to care "
    author: "fvzwieten"
  - subject: "Re: Awesome!"
    date: 2009-01-27
    body: "Thanks for your reply\r\n\r\n<p>It's not really clear where the dividing line is between the two. For example, AFAIK I'm supposed to use nepomuksearch:/ for searching (but it doesn't seem to work in this release). Trying strigi:/ redirects me to Google (default search engine)\r\n\r\n<p>Also, KRunner has a \"Nepomuk Desktop Search Runner\" plugin but no \"Strigi Desktop Search Runner\"\r\n\r\n<p>Anyway, I just tried the old strigiclient app and it is indeed very fast, both when indexing (I had to start it manually) and when searching. It doesn't seem to update its index (in real time) though."
    author: "SP"
  - subject: "Selecting multiple windows"
    date: 2009-01-27
    body: "One feature from KDE 3.5 still seems to be missing. It is the possibility to select more than one window in the taskbar by holding ctrl. You could then e.g. show them side by side. (even Windows can do this btw.)\r\n\r\nBut still KDE 4.2 is of course a great release. It's my favorite desktop besides E17."
    author: "Maxjen"
  - subject: "How to debug a program"
    date: 2009-01-27
    body: "Maybe this is not the best place to ask it, but I hope somebody knows: can I debug the source of a program (in my case KRunner) without compiling the whole KDE from source? I am not a KDE developer but I know programming to some extent so maybe I would be able to find the location of a serious bug (screen blinking every few seconds) that few people seem to be able to confirm. (I use openSUSE 11.1.)"
    author: "gd"
  - subject: "Whole thread should have the same subject ;)"
    date: 2009-01-27
    body: "I don't have hard time with my radeon, everything works except plasma.\r\n<br /><br />\r\nhttp://www.google.com/search?num=100&hl=hr&q=kde+qt+4.4+performance\r\n<br /><br />\r\n\r\nI just want to add that I'm sorry about my first comment in which I show that one can in only one minute:<br />\r\n - make almost whole desktop black<br />\r\n - make plasma crash<br/>\r\n - make it use 100% of you CPU<br/>\r\n<br/>\r\nI shouldn't post it. We should censor screenshots that show kde in bad light<br/> especially in time when we all must be happy and celebrate the best<br/> release ever.\r\n"
    author: "vedranf"
  - subject: "It's a part"
    date: 2009-01-27
    body: "Strigi is a part of the Nepomuk now, or to be more precise, KDE now ships with strigi indexer that is a part of Nepomuk. (so, no need for the strigidaemon)"
    author: "ivan"
  - subject: "Different plasmoids on virtual desktops"
    date: 2009-01-27
    body: "How can you set that?"
    author: "gd"
  - subject: "How?"
    date: 2009-01-27
    body: "But how does one get it without Flash? The content is still application/x-shockwave-flash. Don't take my word for it, look at the page source itself."
    author: "Brandybuck"
  - subject: "A link should be accessible from blip.tv"
    date: 2009-01-27
    body: "There should be a link in the player. I admit an explicit link would be preferable.\r\n\r\nAnyway, here are the links:\r\n\r\nhttp://blip.tv/file/1650884?filename=Sebasje-WindowManagementInKDE42153.ogv\r\n\r\nhttp://blip.tv/file/1651256?filename=Sebasje-ThePlasmaDesktopShellInKDE42312.ogv"
    author: "einar"
  - subject: "100% agreed!"
    date: 2009-01-27
    body: "I have no idea why they vote you down. This is very valid. The cashew sucks. It gets in my way and it's completey useless. Yet it just *has* to be on the desktop. WHY?!!! Give us an option to hide it! Come on, it can't be that hard..."
    author: "stoked"
  - subject: "Definitely a major improvement..."
    date: 2009-01-28
    body: "... over KDE4.1, which was a major improvement over 4.0. Good job."
    author: "Michael Howell"
  - subject: "Where is..."
    date: 2009-01-28
    body: "the new weather plasmoid? Using OSB (unstable, 4.3 trunk) I can't find it :-("
    author: "augustofretes"
  - subject: "Just..."
    date: 2009-01-28
    body: "Install the debug corresponding packages from YaST."
    author: "augustofretes"
  - subject: "Re: How to debug a program"
    date: 2009-01-28
    body: "That all depends. If your distribution provides a debug package, use that (in *buntu, it would be something like kdelibs-dbg). If it lumps debug in with development, use the development package.\r\n"
    author: "Michael Howell"
  - subject: "..."
    date: 2009-01-28
    body: "> WHY?!! <br/>\r\n <br/>\r\nBecause it's a centerpiece of Aaron's vision of plasma, a graphical user <br/>interface for touchscreen users and those that didn't yet discover right mouse button.\r\n<br/><br/>\r\n> I have no idea why they vote you down.\r\n<br /><br />\r\nBecause he didn't say how happy he is and how KDE is great.\r\n"
    author: "vedranf"
  - subject: "The question is:"
    date: 2009-01-28
    body: "> But what is the question?<br/><br/>The question is: does printing work? I read before that KDE4 could not even print only odd/even pages. Does that work now?"
    author: "odalman"
  - subject: "install sesame2 backend"
    date: 2009-01-28
    body: "You are lucky you are using Opensuse so you are using the only distribution which package sesame2 the fast backend for soprano (they other distribution are using an old, slow and unmaintained backend redland)."
    author: "albert"
  - subject: "strigi disabled"
    date: 2009-01-28
    body: "by default in kde4.2 strigi is disabled if soprano backend is not sesame2 so as no distribution but opensuse are packaging sesame2, strigi and so nepomuk are useless."
    author: "albert"
  - subject: "LInk"
    date: 2009-01-28
    body: "Yes, there is a link in the player. But the player is Flash Player, so you need Flash in order to get to the link where you don't need Flash. See the problem?"
    author: "Brandybuck"
  - subject: "wrong place"
    date: 2009-01-28
    body: "I know why my comment was voted down. Now that I have had all day to think about it this is not the correct place to voice my negative opinions. This really is just an announcement about a software release.<br/>\r\nKDE 4.2 is a massive jump in amazing awesomeness compared to KDE 4.1.4. I'm loving it. I understand that there are touch screens that need some sort of configuration in the corner. As a regular user I don't need or want it. KDE *should* be about choice. And it is, completely.<br/>\r\nAfter a bit of mucking about the cashew can be removed. I just think it should be easier to do. If I offended anyone for posting my dislikes here then I would like to apologize."
    author: "tubasoldier"
  - subject: "Thanks"
    date: 2009-01-28
    body: "Great work thank you very mush"
    author: "Amine27"
  - subject: "Selecting multiple windows?"
    date: 2009-01-28
    body: "KDE3 could do that? I never knew. \r\n<p>\r\nGonna have to try that when I get home. :)"
    author: "teatime"
  - subject: "Changelog anywhere?"
    date: 2009-01-28
    body: "Previous releases had a Changelog.\r\n<p />\r\nexample:\r\nhttp://www.kde.org/announcements/changelogs/changelog4_1_3to4_1_4.php\r\n<p />\r\nIs there a changelog for the 4.2.0 releaes? For example i like to know WHAT exactly was improved in the IMAP protocol."
    author: "Robin"
  - subject: "So does Ark Linux"
    date: 2009-01-28
    body: "It isn't the only one, just the only big one -- Ark Linux packages both sesame2 and redland, defaulting to sesame2."
    author: "bero"
  - subject: "Yes"
    date: 2009-01-28
    body: "Printing works properly now. If you really wanted to know, you could have either tried it yourself, or even just read the visual guide or Wikipedia page."
    author: "drmrshdw"
  - subject: "How?"
    date: 2009-01-28
    body: "How do you select multiple windows in KDE3.5? Tried the ctrl, but it doesn't do anything... "
    author: "Vin"
  - subject: "OK, next time post the ogg links .."
    date: 2009-01-28
    body: ".. that should also shut up some french gnomes:\r\n\r\nhttp://www.figuiere.net/hub/blog/?2009/01/27/648-kde-42-mis-step\r\n<br><br>\r\n\r\nIn the future everybody will be happy ;)"
    author: "kragil"
  - subject: "I didn't know for Ark, it's"
    date: 2009-01-28
    body: "I didn't know for Ark, it's good. It's really too bad that debian, ubuntu and fedora refuse to package sesame2. So something should be done to solve this strange situation where one of the kde4 brick will never be provided (at least for Fedora) because it's use java (perhaps a mono backend could be better for them... No please don't do it, it was just a joke)."
    author: "albert"
  - subject: "re:How"
    date: 2009-01-28
    body: "hm...\r\nI don't have KDE3.5 at the moment, but I remember that I could do it. ctrl works in windows, so I thought it's the same in KDE, but I'm not 100% sure.<br /><br />\r\nMaybe it's kdemod-specific? To be honest, I only tried it once, after I found out that it can be done in windows and later tried it again in KDE4 where it didn't work."
    author: "Maxjen"
  - subject: "No MAC - Style menu bar???"
    date: 2009-01-28
    body: "No Mac-style menu bar?!?!?!\r\n\r\nWhat a shame!!! whit such a great and beautiful desktop the lack of this feature is a real shame...\r\n\r\n:(\r\n\r\nNot time for me yet to change (and please don't send links about that ugly extra software to add that menu bar... It should be native in KDE)"
    author: "jonh_tomas"
  - subject: "Congratulations"
    date: 2009-01-28
    body: "This looks like a solid release. Thanks and congratulations!"
    author: "sombragris"
  - subject: "I gave 4.2 a go from the RC,"
    date: 2009-01-28
    body: "I gave 4.2 a go from the RC, and despite a number of serious outstanding problems with my favourite apps (Kontact doesn't remember it's position, fi) it's the first 4.x that a 3.x user might actually enjoy using as it's got most of the features back."
    author: "Tom Chiverton"
  - subject: "I gave 4.2 a go from the RC,"
    date: 2009-01-28
    body: "I gave 4.2 a go from the RC, and despite a number of serious outstanding problems with my favourite apps (Kontact doesn't remember it's position, fi) it's the first 4.x that a 3.x user might actually enjoy using as it's got most of the features back."
    author: "Tom Chiverton"
  - subject: "SO"
    date: 2009-01-28
    body: "Not meant as a flame--\r\n\r\nSo you're saying that KDE users should rely on distros to provide a basic environment that has 'enough reasonable choice'? Down that road lay fragmentation, generally horrible distro 'improvements' to KDE, and a ceding of the projects claims of 'sane defaults' and to 'be responsive to the needs of users'.\r\n\r\nAs I've said previously, I prefer Aya. Out of the box I can choose Aya. I'm happy. (If I liked neither Aya or Oxyigen I could pick from one of a half dozen variations on black and grey. :) )\r\n\r\nIn the case of the Cashew / Toolbox, out of the box I have two choices neither of which address the source of my irritation (and no 1/2 dozen other variations). It's fine to have a default desktop with the toolbox, it's less fine not to include and alternative, and it's even less fine to say switch / beg your distro, go here and follow this process, hope on 'New Stuff', etc... It's one thing if there was low demand, if it was a developer's pet feature, or if it was a one man and their goat type feature, but even without scientific quantification this is clearly not the case.\r\n\r\nStatements to the contrary are pretty much missing the point... (and I say this as somebody who's relatively in the loop and capable of installing an alternative.)"
    author: "Shade"
  - subject: "How to compile debugsource packages"
    date: 2009-01-28
    body: "Thanks, I have kdebase4-workspace-denugsource installed, but it is only a pack of .cpp and .h files, without CMake files. How do I compile it?"
    author: "gd"
  - subject: "KDE 4.2"
    date: 2009-01-28
    body: "I am posting this with KDE 4.2. Startup time was very slow, but I am absolutely amazed. It is very usable and convinient for me.\r\n\r\nA few points:\r\n- Icon for the traditional menu should be different from the kickoff.\r\n- Lancelot is very nice but needs performance work and be polished.\r\n- some painting issues\r\n- Why no drag and drop adding lancelot from the desktop to a panel\r\n. locking the desktop widgets needs a usability review.\r\n- plasma quickstarter default size too small\r\n- previewer plasma default size looks strange\r\n\r\nGreat work. So far it is all rock-solid and I didn't feel it was new and just did my work. So this a brilliant indicator it feels right."
    author: "aresoma"
  - subject: "My new desktop"
    date: 2009-01-28
    body: "It took a long time downloading, but I'm finally running 4.2 as my desktop. Lots of big improvements. I'm still a heretic, because I still have love for KDE3 (and 2 and 1), but 4.2 is starting to grow on me."
    author: "Brandybuck"
  - subject: "The \"Cashew\" is redundant"
    date: 2009-01-28
    body: "I have to say that I do not understand the purpose of the \"Cashew\".  Applying logical principals of system design, it is redundant since it provides exactly the same options as right clicking the desktop.  Therefore, it should be removed; it is not needed.  Perhaps there was an original idea that required its use, but there is none now.  If something comes up that can't be implemented by right clicking the desktop, then I adding a new widget that the user can place whereever they want would seem to be a good solution."
    author: "KSU257"
  - subject: "Progress"
    date: 2009-01-28
    body: "Yes, KDE 4.2 has shown great progress.  Keep up the good work.\r\n\r\nI do still have a wish list which is basically the stuff in the Konqueror \"Tools\" menu: \r\n\r\nOpen Terminal, \r\nFind File, \r\nAuto Refresh, \r\nArchive Web Page, \r\nChange Browser Identification.\r\n\r\nTo me these are more important than the fancy new widgets.\r\n\r\nI note that \"Open Terminal\" is on the menu, but only issue is that I can't seem to find it to put on a toolbar.  And, yes, we now have \"Find File\" but it isn't a KPart which I prefer."
    author: "KSU257"
  - subject: "Discoverability"
    date: 2009-01-28
    body: "For discoverability, we should in that case probably remove the right click menu -- all options are available in other places as well. The toolbox will be easier to find for those not used to right clicking, which might be obvious for power users, but has severe problems for the not-so-power ones.\r\n\r\nAlso, for touchscreen users, those right click menues appear to be a real problem."
    author: "sebas"
  - subject: "It was the right decision"
    date: 2009-01-29
    body: "I think we have discussed this to death already. While I'm glad you like it, please spend the time needed to understand the decision we made more than a year ago.\r\n\r\nWhile it might have been painful to the users that got 4.0 without much choice, it was a clear decision for our own community, and given how the last year went for KDE, I can't say anything else than that it was a good decision."
    author: "sebas"
  - subject: "Looks Great!"
    date: 2009-01-29
    body: "I am a long time lurker and first time poster. Just wanted to say you guys have done a great job on this release and I can't wait for what is in store for the future. "
    author: "rycodge"
  - subject: "in kdereview currently"
    date: 2009-01-29
    body: "It's in the kdereview module currently, but it should have been moved to kdeplasma-addons by now already."
    author: "sebas"
  - subject: "on techbase this time"
    date: 2009-01-29
    body: "http://techbase.kde.org/Schedules/KDE4/4.2_Changelog"
    author: "sebas"
  - subject: "first time is always slow :-)"
    date: 2009-01-29
    body: "In the KDE case, the first startup is usually a bit slower since it often involves updating config files. The next logins should be a bit quicker, although there's probably quite some room for improvement still."
    author: "sebas"
  - subject: "KDE developers delivered a"
    date: 2009-01-29
    body: "KDE developers delivered a product, are they to deliver a product that will meet all people's wants and desires, no because that would be impossible.  Many people like features that you don't.  It is up to the distro's to add their little tweaks to KDE."
    author: "Alien Healer"
  - subject: "No, it does not work"
    date: 2009-01-29
    body: "He just politely asks a valid question and they bury him down. Then someone lies to him and they promote him. Excuse my language but what a bunch of idiots.\r\n"
    author: "vedranf"
  - subject: "So what about us..."
    date: 2009-01-29
    body: "who already discovered right click and don't have touchscreens? Why do we need it? Please respond. Thanks!\r\n\r\n\r\n"
    author: "vedranf"
  - subject: "thanks, but the IMAP"
    date: 2009-02-11
    body: "thanks, but the IMAP improvements are not mentioned there at all...<p />\r\nmaybe I should just dig into svn ;)\r\n"
    author: "Robin"
  - subject: "what you should do is"
    date: 2009-01-29
    body: "what you should do is complain to your distro's whose tweaks of KDE most likely caused the problems you were having.\r\n\r\nPersonally I have had none of those problems, so one has to wonder if it is KDE or more likely your computer or distro."
    author: "Alien Healer"
  - subject: "flash"
    date: 2009-01-29
    body: "at the minimum you should grow up."
    author: "Alien Healer"
  - subject: "Thanks for telling me about it!"
    date: 2009-01-29
    body: "I almost wasted time unmasking and a lot of cycles emerging KDE 4.2 based on the comment by drmrshdw. Thanks to your comment I did some searching and found that he really did lie to me (or it is indeed fixed and they just forgot to close the bug report).<br/>\r\nhttps://bugs.kde.org/show_bug.cgi?id=177437<br/><br/>\r\n\r\nI am subscribed to the report so when it is closed, I know that it might be worth installing the next KDE release. (Unless I buy a duplex printer first.)"
    author: "odalman"
  - subject: "Yeah, I agree with you.\n\nA"
    date: 2009-01-29
    body: "Yeah, I agree with you.<br>\r\n<br>\r\nA solution would be to autodetect touchscreens and the number of buttons of the pointing device, and then make intelligent asumptions based on that and the user behaviour.<br>\r\n<br>\r\nFor example:<br>\r\n<br>\r\n* There is a touchscreen: then the whole desktop configures to 1 click.<br>\r\n* There is a mouse with only 1 button: the same.<br>\r\n* There is a mouse with 2 or more buttons:<br>\r\n   ** 1 click desktop enabled by default.<br>\r\n   ** In the mouse preferences there should be an option to select 1 button or 2 buttons desktop.<br>\r\n   ** If the system detects that the user uses 2 buttons constantly, then a message pops up saying something like \"It seems that you are used to work with 2 buttons system \u00bfwould you like to nuke that stupid cashew?\"<br>\r\n<br>\r\nMaybe the idea is silly, but at least there is an option to disable the cashew.<br>\r\n<br>\r\n<br>\r\nPS: Sorry for the bad English. I wrote that in a hurry.\r\n"
    author: "DeiF"
  - subject: "heh, distro fanboy..."
    date: 2009-01-29
    body: "My distro is kde from svn, and my problems are not only mine, they are reported at bugs.kde.org and confirmed by popular vote."
    author: "vedranf"
  - subject: "Darn..."
    date: 2009-01-29
    body: "I don't have any money on my paypal account. I know I promised that I'd give some money at every release but I'm afraid you'll have to wait a bit more for your money.\r\n\r\nThanks for the release though. I hope you get more love for this release than you did for the previous ones.\r\n"
    author: "osh"
  - subject: "Congratulations!"
    date: 2009-01-29
    body: "Congratulations and a big thank you to the whole KDE team! KDE is an amazing piece of software and keeps getting better and better..."
    author: "arx"
  - subject: "You realise (in Kubuntu, at"
    date: 2009-01-29
    body: "You realise (in Kubuntu, at least) that you can get stuff in the, \"tools\" menu by installing the package, \"konqueror-plugins\", right? :D"
    author: "madman"
  - subject: "Check http://forums.kde.org/"
    date: 2009-01-29
    body: "Check http://forums.kde.org/ for tutorials on how to tweak KDE 4. :)"
    author: "madman"
  - subject: "That Argument "
    date: 2009-01-29
    body: "<br>I *LIKE* KDE 4, but that argument has been had. Pretty much everybody agrees with (relatively) 'clean interfaces', 'clean configs', and 'sane defaults'. That's why I'm very careful about saying Toolbox by default, sure... and I'm careful to exclude Developer's pet features, and 'one man and his goat features'. But to pretend that there is something less than reasonable demand is to delude yourself, and to say beg to or switch your distro, go here and do this, or hope on 'new stuff' is to miss the point...</br>\r\n\r\n\r\n<br>That's the reason why so many trolls, pathologically disgruntled ex-users, and nay-sayers have latched on to this is because 'install meta KDE' will result in something that is visible, annoying, and can't be turned off for many people. The (honestly) helpful people here will say switch / beg distro, go here and do this, or hope on 'New Stuff'. That's missing the point, and is mana to trolls. That's what you call giving them a 'nub of truth' and them getting 'years of mileage' from it. (Especially when everything else on the desktop can be changed, swapped out, or gotten rid of under 'install meta KDE' and the lack of that capacity here breaks that continuity and feels alien.)</br>\r\n\r\n\r\n<br>Then compare the alternative, say with one week of BETA for 4.3 left an announcement (Planet Post) of a fabulous new Desktop Containment shipping with KDE goes out that has a toggelable or non-existent toolbox. You can guarantee there'd be great rejoicing. Nobody would bitch and many would be happy. The trolls would say \"took you long enough\", but would be left looking like they are when they reference 4.0.0. Please note, this is not JUST to snub the trolls-- but it has that nice side effect. :) There is legitimate and largeish demand.</br>\r\n\r\n\r\n<br>I have a certain amount of hope that this Desktop Containment' will one day get shipped... But ultimately I have recourse and I'm not the one who has to field the enquiries and wear the flames. Albeit, every time I mention KDE I'll mention the toolbox as a caveat and personal annoyance (as is fair).</br>"
    author: "Shade"
  - subject: "plasma activities"
    date: 2009-01-29
    body: "wow.. thank you, KDE is finally getting usable :)\r\n<p>\r\ni'm little confused by plasma activities.. \r\n</p><p>\r\n- why I need to distinguish between programs and plasmoids?\r\n</p><p>\r\n- why another system for changing activities?\r\n</p>\r\nmultiple desktops are for \"program activities\" and plasma uses \"plasmoid activities\"? so if I'd like to browse and want to see network usage, i must change desktop (to see browser) + change plasma activity (to see usage)?\r\n\r\n\r\nwouldn't it be simpler for user to use only one mechanism?\r\n<p>thanks</p>"
    author: "mdl"
  - subject: "This would be really"
    date: 2009-01-29
    body: "This would be really inconsistent. I'd rather get rid of right-click menus completely.  Correct me if I'm wrong, but I think we only have these right-click->hover->left-click context menus because we adopted them from windows. And they are mostly redundant anyway.\r\n<br /><br />\r\nHave you ever given computer courses? I can only suggest it! \r\nYou will see how utterly stupid some of the PC handling is. I know it feels as usual as walking for us, but just think about it. You usually have no chance to see WHERE and HOW the context of the context menu is changed or what it contains. In Windows, you've got a different menu for a window title bar, the menu bar, the symbol bar, the list header, the window content, input elements, etc. Some elements just don't have a context menu at all. In Windows even the tiny little symbol on the upper left has a different menu than the rest of the title bar! It is just ridiculous!<br />\r\nAnd have you ever seen someone unused to a mouse use such a menu? It's really hard for them to switch from right-click to left-click and inbetween aiming at the 10 pixel high row of the menu is nearly impossible.\r\n<br /><br />\r\nThere are a lot of such things in PC user interfaces, don't even get me started on those draggable toolbars. I don't know how often I had to restructure those on someones MS Word. "
    author: "Andre"
  - subject: "A good release"
    date: 2009-01-29
    body: "Good evening,\r\n\r\nKDE 4.2 seems to be usable. I will install it as soon as it will be in Debian testing or stable. Now I only try it in vbox with debian experimental or UNRELESEAD packages.\r\n\r\nSome questions I have got:\r\n\r\nWill there be a function for the middle mouse button. I miss the feature to see which windows are open on which desktop.\r\n\r\nIs anyone able to add new programms to the quick-launcher?\r\n\r\n\r\nBy the way: The width for the new forum-design is a bit narrow. Konqueror doesn't even show it correctly. Despite of that it looks very nice.\r\n\r\n\r\nHolger"
    author: "Holger"
  - subject: "Duplex printing"
    date: 2009-01-29
    body: "> (Unless I buy a duplex printer first.)\r\n\r\nWhy? Duplex printing is working fine on my 4.2 since some months."
    author: "majewsky"
  - subject: "Please, do not ever make"
    date: 2009-01-29
    body: "Please, do not ever make software that tries to be overly clever when it comes to making assumptions. I want my KDE to behave coherently on all target platforms."
    author: "majewsky"
  - subject: "I'm not sure"
    date: 2009-01-29
    body: "4.x has been so disastrous that for first time in 10 years (I'm a pre KDE1 user), i've ended up using Gnome in stead. Seigos arrogant attitude towards end-users has really annoyed me.\r\nKDE4.2 is much better than previous KDE4 releases - but one year has been lost- ant KDE2.4 is still not good enough. Sound suddenly does not work for me, Dolphin generates just as much wasted screenspace as this website (50% to 2/#), the startmenu ... oh boy lets not go there.... etc etc\r\n"
    author: "janders"
  - subject: "That is not meant to be"
    date: 2009-01-29
    body: "<p>That is not meant to be compiled, you should be running the binaries from the kdebase4-workspace package. The -debugsource is used by gdb to display useful backtrace information and such.</p>\r\n\r\n<p>If you want to compile a KDE application and debug it, you will have to install the debug packages for all affected libraries (libqt4 and libkde4/kdelibs4 may be enough for the start, add third-party libs to that list as required). Then get the application's source from somewhere and compile it. (CMake should default to the Debug profile, use the -DCMAKE_RELEASE_TYPE=Debug switch to be sure.)</p>"
    author: "majewsky"
  - subject: "http://frinring.wordpress.com"
    date: 2009-01-29
    body: "http://frinring.wordpress.com/2009/01/29/mac-style-menubar-for-kde-4-and-others/"
    author: "majewsky"
  - subject: "My apology."
    date: 2009-01-30
    body: "Oh my gosh! Truly sorry for the misleading information! To clarify, the KDE 4.2 print system has improved, but does not have duplex printing.\r\n\r\nAs the bug report page said, however, if you want to give KDE 4.2 a try, but you need duplex printing, you can install and run \"printruler\", which can run as a daemon and open anything you print as pdf in any directory (default is /home/you/PDF), and give you more options such as duplex printing.\r\n\r\nSee http://www.kde-apps.org/content/show.php/printruler?content=83774 or http://code.google.com/p/printruler/source/checkout\r\n\r\nTruly hope this helps!"
    author: "drmrshdw"
  - subject: "If everything is available"
    date: 2009-01-30
    body: "If everything is available elsewhere, than a good case can be made for it except that people are probably used to right clicking."
    author: "KSU257"
  - subject: "Konqueror AddOns??"
    date: 2009-01-31
    body: "No, I have the SVN 4.2 BRANCH installed.  I found the konq-plugins in \"extragear/base/\".  Why aren't basic Tools like Auto Refresh, Download Manager, & Archive Webpage shipped with KDEBase?\r\n\r\nI also find that Open Terminal is not a Konq-Plugin but rather a function of the Dolphin Part.  This design choice has issues.  This means that the toolbar icon can only be on the toolbar incorrectly called \"Dolphin Toolbar <dolphinpart>\" (shouldn't this be \"Main Toolbar <dolphinpart>\" since it merges with the Main Toolbar?).  Same problem with Find File -- it is not a Konq-Plugin."
    author: "KSU257"
  - subject: "Personally, I've never seen"
    date: 2009-01-30
    body: "Personally, I've never seen what's wrong with the cashew: it's small enough to ignor, doesn't get in the way of open windows, doesn't get in the way of desktop icons/plasmoids, is there for if and when you need it and can even be moved. By my reconing, this is just another nit-picky thing that some KDE 3.x fanatics like to throw up as a bad thing.\r\n\r\nThis whole argument is pathetic. It's 15px by 15px of screen space, for crying out loud!"
    author: "madman"
  - subject: "PATCH exists?"
    date: 2009-01-30
    body: "This would indicate that OpenSuse has the patch.  So, why doesn't KDE ship it?"
    author: "KSU257"
  - subject: "Nitpicky maybe-- But I'm one"
    date: 2009-01-31
    body: "<br>Nitpicky maybe-- But I'm one of those sickos that's used KDE 4 daily since before it was released. While it doesn't bother you-- you can't relegate the demand for a 'meta install kde' way to remove it down to 'one man and his goat', and since the default containment's won't be modified to accommodate this, the one hope is for another containment to ship.</br>\r\n\r\n<br>All desktops suck, and I think KDE 4.2 happens to suck less than *ANY* alternative, but KDE shouldn't stand for 'Kool-aid Drinker Enlightenment'. :) One of the things I admire about the KDE community is that at some point it can take a good frank look in the mirror and see when it's missing the point, or gone too far to one extreme or another... It just takes a while sometimes, so I have tremendous hope in this case.</br>\r\n\r\n<br>edit: I don't think the desire to remove a thing conveniently is being overly rigid. The biggest selling point of KDE has been the ability to 'pick a metaphorical nit'... That little glaring bit of immutability is what can irk the most...</br>"
    author: "Shade"
  - subject: "It's a good point to make it"
    date: 2009-01-31
    body: "It's a good point to make it optional, and I'd agree - if people weren't as pathetic as to say, \"Gah, I don't like that, I CAN move it behind a panel or ignore it and see if the rest of the DE is cool, but instead I'm going to throw a fit and use GNOME!\"\r\nSomething as petty as that shouldn't be a cause to switch DEs or distros, but that's in effect what some people have been implying: \"I'm not using KDE because, despite the rate of its growth and the underlying technologies such as KIO, Akonadi, Solid or Phonon, I can't get rid of the cashew!\"\r\nKDE 4.0? yes, I can understand if you didn't like it. It was the bare minimum of what a DE requires and we're used to more then the bare minimum, but for some people it seems like a game of, \"Find some other reason to dislike KDE 4 and throw a fit!\" That's what annoys me, more then anything."
    author: "madman"
  - subject: "Error in visual guide"
    date: 2009-01-31
    body: "It should be property software instead of commercial when LGPL is mentioned.    "
    author: "origin"
  - subject: "That's odd... I've got KDE"
    date: 2009-02-01
    body: "That's odd... I've got KDE 4.2 installed (though not from source) and they're still there...\r\n\r\n<a href=\"http://www.freewebs.com/supermadman760/Konq-plugins.png\">http://www.freewebs.com/supermadman760/Konq-plugins.png</a>"
    author: "madman"
  - subject: "Programing vs Hacking"
    date: 2009-02-02
    body: "\"Programming is an art that requires long training to do it [correctly]\"\r\n\r\n+1E+99 (couldn't agree more)\r\n\r\nThis is certainly correct.\r\n\r\nI would also say that knowing a programing language is not the same as knowing how to program.  A relevant question is how much of KDE is written by trained programmers and how much of it is written by self taught hackers?"
    author: "KSU257"
  - subject: "Distros??"
    date: 2009-02-02
    body: "Yes, it appears that distros are including them.  But, they are only in ExtraGear."
    author: "KSU257"
---
It has been a full year since the beginning of the KDE 4 series and today the KDE community proudly announces the release of KDE 4.2, "The Answer". This
third feature release brings a stunning amount of new features and great stability. The KDE community is confident that KDE 4.2 is a compelling choice for the majority of end users, after previous releases targeting enthusiasts.

<img src="http://www.kde.org/img/kde42.png" width="437" height="199" />

Read <a href="http://www.kde.org/announcements/4.2/">the official announcement</a> and have a close look at what is new in <a href="http://www.kde.org/announcements/4.2/guide.php">the Visual Guide to KDE 4.2</a>.  The <a href="http://www.kde.org/info/4.2.0.php">release information page</a> gets you to the source and packages.
<!--break-->
<a href="http://www.kde.org/announcements/4.2/screenshots/desktop.png"><img alt="KDE 4.2" src="http://www.kde.org/announcements/4.2/screenshots/desktop_thumb.png"/></a>

KDE 4.2 brings better task management and new features in the Plasma workspace and KWin, and more polish to many applications. Dolphin allows for a more streamlined workflow. The improved IMAP support and new look further enhance the KMail user experience. The improvements to the KDE development platform, combined with the LGPL Qt libraries, make KDE the most appealing free development platform.