---
title: "First KMyMoney Beta Version Available for KDE 4 Platform"
date:    2009-12-28
authors:
  - "asoliverez"
slug:    first-kmymoney-beta-version-available-kde-4-platform
comments:
  - subject: "Thanks"
    date: 2009-12-29
    body: "I'm amazed at how fast this has come together. I was assuming this would be something that would take much longer. Thanks to the hard-working developers!"
    author: "tangent"
  - subject: "I'd like to test, but ..."
    date: 2009-12-29
    body: "... it crashes on startup:\r\n\r\n  #0  0x00007f279950605d in pthread_cond_timedwait@@GLIBC_2.3.2 ()   from /lib64/libpthread.so.0\r\n  #1  0x00007f279742b507 in QWaitCondition::wait () from /usr/lib64/libQtCore.so.4\r\n  #2  0x00007f27974204b1 in ?? () from /usr/lib64/libQtCore.so.4\r\n  #3  0x00007f279742a675 in ?? () from /usr/lib64/libQtCore.so.4\r\n  #4  0x00007f2799502040 in start_thread () from /lib64/libpthread.so.0\r\n\r\nCould this have something to with QT 4.6?\r\n\r\n"
    author: "Flitcraft"
  - subject: "1. This is not bugs.kde.org,"
    date: 2009-12-29
    body: "1. This is not bugs.kde.org, post your crash reports there.\r\n2. Your backtrace is useless, install debug packages:\r\n\r\nhttp://techbase.kde.org/Development/Tutorials/Debugging/How_to_create_useful_crash_reports"
    author: "majewsky"
  - subject: "build error"
    date: 2009-12-29
    body: "make fails at 97%, while \"Building CXX object kmymoney/plugins/kbanking/dialogs/CMakeFiles/kmm_kbanking_dialogs.dir/kbpickstartdate.o\"\r\n\r\nThe error seems to be with the kbanking plugin:\r\n\r\nIn file included from [...]/kmymoney/plugins/kbanking/dialogs/kbpickstartdate.cpp:27:\r\n/usr/include/qbanking/qbanking.h:41:25: error: qguardedptr.h: No such file or directory\r\n[...]\r\n\r\nI have aqbanking 4.1.8-1 installed (using Arch Linux).\r\nDo I need to install another version of it for kmymoney to build correctly?"
    author: "smls"
  - subject: "mmm"
    date: 2009-12-30
    body: "After 8 month of hard work a version gets released that crashes on start-up...\r\n\r\nYou have been warned:\r\n\"We are confident that it is stable enough for use by early adopters.\"\r\n\r\nFrankfurt is victoriously defended = about to fall in the hands of the enemy.\r\n\r\nOf course the correct path for bug mine dogs is to file bugs at bugs.kde.org"
    author: "vinum"
  - subject: "It still has some problems with Qt 4.6"
    date: 2009-12-30
    body: "It still has some problems with Qt 4.6 and KDE 4.4 beta, so that's probably the issue."
    author: "asoliverez"
  - subject: "Use AqBankink for Qt4"
    date: 2009-12-30
    body: "If you want to use AqBanking, you have to compile it for Qt4. We have to work on the install scripts to avoid installing the aqbanking plugin if another version is present."
    author: "asoliverez"
  - subject: "We use it every day, but we"
    date: 2009-12-30
    body: "We use it every day, but we need more feedback, that's why this version has been released.\r\nNow, you can either make sarcastic comments or help us. It's up to you."
    author: "asoliverez"
  - subject: "Just so you don't think it's going badly for everyone..."
    date: 2009-12-30
    body: "I'm using KDE 4.3.3 / Qt 4.5.3 on Gentoo w/o any banking plugins.  Downloaded the tar.  Ran the compile commands to do a local install.  Added the local library directory to LD_LIBRARY_PATH, and started it up.\r\n\r\nI have it pointing to a copy of my normal KMyMoney file, but all seems to be working so far.\r\n\r\nThanks for all of your hard work on the port.  Gentoo recently dropped support for KDE3 and all KDE3 dependent apps (from their normal repos anyway).  I've been looking forward to this port to KDE4 since this is the only KDE3 app that I still use."
    author: "cwoolner"
  - subject: "This is good news!"
    date: 2009-12-31
    body: "btw, any news about invoice module?  I really like to use KMyMoney for small businesses but without invoice module it just make no sense.\r\n\r\nKeep up the good work! "
    author: "Spaces"
  - subject: "run make install"
    date: 2009-12-31
    body: "It crashes because you didn't install.\r\n\r\nRun make install with admin privileges. switch to root or use sudo, whatever suits your distro."
    author: "asoliverez"
  - subject: "Not yet"
    date: 2009-12-31
    body: "Not yet. This version has the same features as 1.0.2. We will work on integrating with Kraft to provide invoicing feature in the future."
    author: "asoliverez"
  - subject: "Perfect!"
    date: 2010-01-03
    body: "The fresh installed KMyMoney 3.95 works perfectly on my openSUSE 11.1 with KDE 4.3.4 and QT 4.5.3; aqbanking works fine, too.\r\nThanks for the nice work :-)"
    author: "edison.kidshome"
  - subject: "Missing rlink maybe"
    date: 2010-01-06
    body: "From what someone else said, you need to manually add the local path to LD_LIBRARY_PATH.\r\n\r\nKDE usually gets around this problem with a bit of script magic.  I don't know how this works exactly though - anyone know how KDE manages to do this?  It would be good to fix KMyMoney."
    author: "JohnFlux"
  - subject: "Cool..."
    date: 2010-01-12
    body: "I've successfully built it now, and it looks pretty cool...\r\n\r\nHowever, since I haven't used any finance software before, I feel a little lost looking at all the things KMyMoney presents me...\r\n\r\nIs there some introduction / beginners-howto / user manual that (at least kind of) applies to this KDE4 version of KMyMoney?"
    author: "smls"
  - subject: "You might want to take a look"
    date: 2010-01-14
    body: "You might want to take a look at the manual. The chapter \"Making the most out of KMyMoney\" is a good starter and can be found online at http://kmymoney2.sourceforge.net/online-manual/makingmostof.html"
    author: "ipwizard"
  - subject: "Thanks for all of your hard"
    date: 2010-01-21
    body: "Thanks for all of your hard work on the port. Gentoo recently dropped support for KDE3 and all KDE3 dependent apps (from their normal repos anyway).\r\n____________\r\n<a href=\"http://www.edtrialpack.com/\" title=\"purchase online\">purchase online</a>"
    author: "marlena21"
---
The KMyMoney team is pleased to announce the immediate availability of version 3.95. After 8 months of hard work, this release is the first version developed upon the KDE 4 Platform. We are confident that it is stable enough for use by early adopters. We would like to get much needed feedback from the community in order to make the new KMyMoney as rock-solid as previous releases.

For this version, the focus has been on allowing it to run on the latest KDE platform while maintaining feature-parity. Subsequent releases will make better use of the new capabilities provided by this new platform. Also, significant effort has gone into making KMyMoney available on all operating systems supported by the KDE development platform.
<!--break-->
One major change which may not be visible to our end users is our move to the KDE Subversion source repository. All of the development effort was done there, and in coordination with the relevant KDE teams. We believe this move has been beneficial for both KMyMoney and the KDE community and will make integration tighter in the future.

As a direct result of this move, KMyMoney will be available in 26 languages, and its documentation will be available in 2 languages in addition to English. Also, a lot of fixes have gone into language support and documentation, to make this the most locale-friendly version of KMyMoney to date.

Going forward, there is still a significant amount of work to do in order to complete the task and leverage all the new features provided by the KDE 4 Platform. With the feedback provided to us on this version and more to follow, we will adjust the pace of development to keep KMyMoney as bug-free as possible and add exciting new features at the same time.

For installation, look at <a href="http://techbase.kde.org/Projects/KMyMoney">the installation instructions on Techbase</a>.