---
title: "Updates to Qt and Qt Creator Released Today"
date:    2009-04-23
authors:
  - "troubalex"
slug:    updates-qt-and-qt-creator-released-today
comments:
  - subject: "Anyone tried KDE 4.2.2 on Qt"
    date: 2009-04-27
    body: "Anyone tried KDE 4.2.2 on Qt 4.5.1? http://rdieter.livejournal.com/13559.html seems to indicate it's better to wait for 4.5.2 (or perhaps distributions will patch their packages)."
    author: "Haakon"
  - subject: "A patch"
    date: 2009-04-27
    body: "The next journal post the solution :\r\n\r\nhttp://rdieter.livejournal.com/13739.html\r\n\r\nThere is a patch for the issue in qt-copy. Let's wait your distribution include it or build your own Qt with that patch."
    author: "darktears"
---
<p>Earlier today, Qt Software released updates to Qt and Qt Creator. Both releases are available for download from <a href="http://qtsoftware.com/downloads" title="http://qtsoftware.com/downloads">http://qtsoftware.com/downloads</a> -- either stand-alone or as part of a new build of the Qt SDK.</p> <p>Qt 4.5.1 includes bugfixes and optimizations made since the release of Qt 4.5.0.</p> <p>Qt Creator 1.1 incorporates a good deal of user feedback gathered since the 1.0 release, and provides a broad collection of improvements and additions.</p> <p>For more details on the releases, check out the recent posts at Qt Labs at <a href="http://labs.trolltech.com" title="http://labs.trolltech.com">http://labs.trolltech.com</a>.</p> 
