---
title: "KDE Commit-Digest for 1st February 2009"
date:    2009-02-21
authors:
  - "dannya"
slug:    kde-commit-digest-1st-february-2009
comments:
  - subject: "Thanks"
    date: 2009-02-21
    body: "Thanks Danny.Its always a pleasure to read commit-digest."
    author: "emilsedgh"
  - subject: "New dot"
    date: 2009-02-21
    body: "New dot is really nice, just a few points:\r\n\r\n1)i guess most people do not prefer a fixed-layout for dot.\r\n2)dot is mostly about texts, what about a little bit bigger fonts?\r\n3)all 'By' fields are empty.\r\n4)i really miss 'dept' part of the old dot, can we get it back?\r\n\r\nActually i'd be happy to help on these issues if i know where i can get dot-specific themes and modules."
    author: "emilsedgh"
  - subject: "Re: New dot"
    date: 2009-02-21
    body: "I agree. The fixed width is a real PITA :(\r\nSince I have problems with sight (I\u00b4m not getting blind - just older), I have to use a quite large font. \r\nI have to scroll sideways, wiich is just plain crazy on a site like this (not that it was good before...) \r\n \r\nAbout the font: I use Mandriva 2008.1, Firefox with standard settings, and the text is almost unreadable."
    author: "tryfan"
  - subject: "Akonadi-based KMail in KDE 4.4"
    date: 2009-02-21
    body: "This comment is in no way intended to bash the KMail devs. KMail really rocks.\r\nHowever, KMail + Akonadi was originally planed to be part of KDE 4.0 -- released in January 2008. It's been postponed step by step with the current target being 4.4 -- to be releases in January 2010.\r\nWhen porting KMail is huge work, fine. We'll understand. But guys, please don't try to fool us with false promises."
    author: "KAMiKAZOW"
  - subject: "Try CTRL++"
    date: 2009-02-21
    body: "Try Ctrl++"
    author: "patcito"
  - subject: "I for one do not remember a"
    date: 2009-02-22
    body: "I for one do not remember a full Akonadi port to be scheduled for KDE 4.0. It is scheduled for KDE 4, which includes 4.4.\r\n\r\nAfter all, KDE-PIM is short on developers, and yet they do a great job migrating a broad user base from KResource to Akonadi."
    author: "majewsky"
  - subject: "\"3D view in Kolf-NG\""
    date: 2009-02-22
    body: "To add a note on this point: We are just getting ready to enable three-dimensional view aspects, but no objects are being rendered currently."
    author: "majewsky"
  - subject: "erm that just makes the font bigger"
    date: 2009-02-22
    body: "the problem is the lack of word wrap."
    author: "eean"
  - subject: "Akonadi stuff has been pushed back some though"
    date: 2009-02-22
    body: "."
    author: "eean"
  - subject: "with opera, you can force"
    date: 2009-02-22
    body: "with opera, you can force 'fit to width' (view -> fit to width), which will nicely format page so that you have no horizontal scrolling to perform.\r\n\r\ni don't know whether it is present in other browsers."
    author: "richlv"
  - subject: "missing parenthesis"
    date: 2009-02-22
    body: "at the end of the summary :\r\n\r\n(based on Akonadi. Read the rest of the Digest here.\r\n\r\nclosing parenthesis is missing :)"
    author: "richlv"
  - subject: "Buzz in Commit Digest"
    date: 2009-02-22
    body: "I have been wondering since quite some time what this Buzz list is, and what algorithm lays behind. The only thing I am aware of is that it never changes. Also, there are names in it I never heard of, or hardly ever read of in the commit digest of the last 18+ month. Just wondering if this is relevant at all...<br>\r\nAnyway, thanks for the digest, it's always a pleasure to read about the progress in the projects :)"
    author: "Mamarok"
  - subject: "Buzz"
    date: 2009-02-22
    body: "Have you read the text found under \"<a href=\"http://commit-digest.org/poppy/?view=buzz-explain\">Learn more about buzz...</a>\"? :)"
    author: "Hans"
  - subject: "other issues"
    date: 2009-02-22
    body: "Has anyone gotten the new dot to work in konq? I'm running svn trunk but I can't seem to log into the dot.\r\n\r\nQuite the irony if you ask me..."
    author: "_txf_"
  - subject: "Lion Mail as mini CRM System?"
    date: 2009-02-22
    body: "Hi,<br>\r\nright now we are searching in the company for a new CRM system. So far we are using an old Act! system (Windows) on a per user set-up.<br>\r\nWhen I asked what are the basic needs for a simple standard CRM System I get the answer, that its like a contact based aggregator, that let you find every mail, attachment, appointment, etc. in a timeline/history context that's related to this contact. On a second level you can also group many contacts to a company or a project.<br>\r\nWhen I get this *me* instantly thought that this is Nepomuk-Akonadi stuff. A user should not be forced to to put this stuff in a collecting database but the semantic system should take care. So when I use my KMail I can do \"right-click\" on the mail address and can generate a CRM based status page where I can create actions like \"made an appointment\", \"send file\", \"call\", \"write note\", \"write protocol\".<br>\r\nStuff like this can be a very interesting and valuable area and can put kdepim on a new level. BUT this as to be realised as multi-user system and not per Desktop or per user.<br>\r\n<br>\r\nBye<br>\r\n&nbsp;&nbsp;Thorsten "
    author: "schnebeck"
  - subject: "Re: missing parenthesis"
    date: 2009-02-22
    body: "Fixed.\r\n\r\nThanks,\r\nDanny"
    author: "dannya"
  - subject: "Re: Buzz in Commit Digest"
    date: 2009-02-22
    body: "Basically, since I only have a laptop which is not on all the time (just most of the time ;)), I was running the daily buzz generation on a server. Unfortunately, I no longer had access to the server, and so the buzz stopped being calculated.\r\n\r\nI've been meaning to look into either freshening up the data or removing it, but I hadn't had time... but i'll try and look into it!\r\n\r\nDanny"
    author: "dannya"
  - subject: "Doesn't Matter"
    date: 2009-02-22
    body: "And it doesn't matter all that much either.\r\nKontact/Kmail/etc are in very good shape in kde 4.2\r\nI use it daily both at home and at work and it's a great experience.\r\n\r\nI'd rather see them spend all the time they need on getting the akonadi integration/rewrites done right then have them rush in everything way to soon.\r\n"
    author: "MarkHannessen"
  - subject: "Re: other issues"
    date: 2009-02-22
    body: "Writing this from Konq 4.2.0, login works without problems."
    author: "majewsky"
  - subject: "Full page zoom should help"
    date: 2009-02-23
    body: "Full page zoom should help you. For me, ctrl-scrolling does a full page zoom in Firefox, MS IE 7 (yeah, at work...) and konqueror. Try it :D"
    author: "jospoortvliet"
  - subject: "Sounds like an interesting"
    date: 2009-02-23
    body: "Sounds like an interesting idea. Apparenly, similair things happened to several companies when they started to use MAC OS X with spotlight. You can stop doing dificult stuff with your documents - spotlight finds them instantaniously. The integration of Nepomuk, Strigi and Akonadi has the potential of doing what spotlight did for documents - but for far more data than just documents."
    author: "jospoortvliet"
  - subject: "Re:"
    date: 2009-05-18
    body: "When I asked what are the basic needs for a simple standard CRM System I get the answer, that its like a contact based aggregator, that let you find every mail, attachment, appointment, etc. <a href=\"http://www.infinitylogodesign.com/Web-Design/\">web design</a> | <a href=\"http://www.infinitylogodesign.com/Design/Company-Logo/\">logo design company</a> | <a href=\"http://www.infinitylogodesign.com/Logo-Design/Why-Choose-Us/\">Brand logo design</a>\r\n\r\n\r\n"
    author: "PeterWarner"
  - subject: "net dot?"
    date: 2009-05-24
    body: "I was reading comments was like stopped here. What is new dot? and how is its usage? KDE really has alot of apps and features that some i have never heard about - good to get to know more :)\r\nthanks in advance,\r\n<span style=\"color:white\">worldclass for models cloth shopping <a href=\"http://fashion4us.com\" style=\"color:white\">korean fashion</a> garments tops.</span>\r\n"
    author: "marrylane"
---
In <a href="http://commit-digest.org/issues/2009-02-01/">this week's KDE Commit-Digest</a>: Initial support for adding video and audio previewing (similar to the file dialog) to <a href="http://dolphin.kde.org/">Dolphin</a>'s metadata panel. A new "highlight window" effect for KWin-Composite. Filtering support in the "FileWatcher" Plasmoid, work on the "Welcome" Plasmoid, and initial import of "OpenBrain" and "Translatoid" <a href="http://plasma.kde.org/">Plasma</a> widgets. Experiments to make a screensaver using KGLEngine. First approach on integrating an "interactive graphs concept" in <a href="http://edu.kde.org/kalgebra/">KAlgebra</a>. Initial code for an object-oriented <a href="http://edu.kde.org/kturtle/">KTurtle</a>. Initial work on a 3D view completed in Kolf NG (a rewrite effort). Improvements in ReplayGain and bookmark support in <a href="http://amarok.kde.org/">Amarok</a> 2. Major improvements to the Facebook photo download/import KIPI plugin (used in <a href="http://www.digikam.org/">Digikam</a>, <a href="http://gwenview.sourceforge.net/">Gwenview</a>, etc). Support for LZMA decompression added to <a href="http://strigi.sourceforge.net/">Strigi</a>. More work on porting <a href="http://konversation.kde.org/">Konversation</a> to KDE 4. Work on KPackageKit and the "DynaDraw" painting mode of <a href="http://www.koffice.org/krita/">Krita</a>. Start of work on a <a href="http://www.kexi-project.org/">Kexi</a>-ODBC bridge. Fixes of many "release critical" bugs for the "Chart" shape in <a href="http://koffice.org/">KOffice</a>. Initial import of LinTV. Restart of development work on automatic language detection and switching for Sonnet. <a href="http://ktorrent.org/">KTorrent</a> 3.2 Release Candidate 1, and <a href="http://www.kdevelop.org/">KDevelop</a> 3.9.91 (first beta) are tagged for release. 

Also in this Digest, Sebastian Kügler introduces "Lion Mail", a new email information <a href="http://plasma.kde.org/">Plasmoid</a> (based on <a href="http://pim.kde.org/akonadi/">Akonadi</a>). <a href="http://commit-digest.org/issues/2009-02-01/">Read the rest of the Digest here</a>.
<!--break-->
