---
title: "KDevelop4 Beta 3 Released"
date:    2009-05-30
authors:
  - "apaku"
slug:    kdevelop4-beta-3-released
comments:
  - subject: "Exiting!"
    date: 2009-06-03
    body: "I am exited to learn about the progress in KDevelop. I am looking forward to trying it out; keep up the good work! I think KDevelop was always a good code editor, but the current incarnation seems to certainly have the potiential to keep way ahead of the alternatives like QtCreator. "
    author: "andresomers"
  - subject: "missing quanta.."
    date: 2009-06-04
    body: "Great work guys, but i do hope that quanta somehow gets revived in the process of development too.  I realize that PHP improvements have gone in to KDevelop, and I hope that the quanta --> kdevelop plan gets resolved in a way that works well for everyone (including the restless web designers like myself).\r\n\r\nStill using Quanta KDE3 here,\r\n\r\nVlad"
    author: "blantonv"
  - subject: "Yes\uff01 QtCreator"
    date: 2009-06-04
    body: "QtCreator is Splendid\uff0cwe are not care about Kdevelop longer."
    author: "obizy"
  - subject: "*we are not care about"
    date: 2009-06-04
    body: "*we are not care about grammar while trolling longer."
    author: "JontheEchidna"
---
The KDevelop team is proud to announce the third public beta of KDevelop4. This release includes some major new features, such as a new <a href="http://zwabel.wordpress.com/2009/03/13/really-rapid-c-development-with-kdevelop4">code-writing assistant</a>, a new <a href="http://www.proli.net/2009/02/28/kdevelop4s-documentation-integration/">documentation plugin</a> showing you the API docs for Qt and KDE APIs, a reworked
Mercurial plugin and a rewrite of the classbrowser plugin. On top of that we improved stability a lot, made loads of small improvements throughout and fixed as many bugs as we could.

Unfortunately we also had to let go two plugins from the KDevelop source, QMake support and Qt Designer integration. Both are currently not very usable and thus have been moved to the <a href="http://websvn.kde.org/trunk/playground/devtools/kdevelop4-extra-plugins/">KDevelop playground area</a>.

You can fetch the source packages for KDevPlatform and KDevelop from the <a href="http://download.kde.org/download.php?url=unstable/kdevelop/3.9.93/src/">KDE mirrors</a>. Be sure to download both packages.