---
title: "The Business Of Free"
date:    2009-07-16
authors:
  - "nhnFreespirit"
slug:    business-free
comments:
  - subject: "They are American"
    date: 2009-07-16
    body: "In the US the iPod and iTunes is so big that telling people to use something else didn't make any sense to them.\r\n\r\nMost Americans don't really know what an MP3 player is, they know iPods .. not much else.\r\n\r\nSo I don't really blame Palm, maybe they will rethink their strategy now or will try to emulate an iPod 100% .. "
    author: "kragil"
  - subject: "My honest opinion? It's"
    date: 2009-07-16
    body: "My honest opinion? It's impractical to have ALL software be Free software, but it's impractical to have the most useful tools (iTunes, Office, etc.) being proprietary, lest something just like this happens."
    author: "madman"
  - subject: "And to be fair, the ipod is a"
    date: 2009-07-16
    body: "And to be fair, the ipod is a well done music player,\r\nOutpacing many competitors. If you don't care to much about vendor lockin or propriety protocols/software there is hardly any reason not to get one.\r\n\r\nI think it would be very nice to have palm support amarok even though i doubt it will ever happen. A piece missing in the puzzle as well is: how would people buy populair music using amarok? ( I know you can buy music from some stores in amarok, but it does not seem to be the music most people want )\r\n"
    author: "MarkHannessen"
  - subject: "Music purchasing"
    date: 2009-07-16
    body: "A major company like Palm should have little trouble convincing Amazon to let Amarok include their mp3 store.I have heard wild rumors that a few major distros are already trying to talk Amazon into this."
    author: "nhnFreespirit"
  - subject: "Thank you for your"
    date: 2009-07-16
    body: "Thank you for your comment.\r\nI'd be very interested in it if it is going to happen. :)\r\n"
    author: "MarkHannessen"
  - subject: "What is the answer of Palm?"
    date: 2009-07-16
    body: "Out of curiosity, anybody attempted to contact Nokia and Palm on the matter?\r\n\r\nThis article is theoretical, what about trying to find the right people in those company to make it a reality?"
    author: "Ikipou"
  - subject: "If you have a contact at Palm"
    date: 2009-07-16
    body: "If you have a contact at Palm please let us know :)"
    author: "nightrose"
  - subject: "Honestly..."
    date: 2009-07-16
    body: "... I don't see the point of using AmaroK for syncing the palm. Amarok can sync music, but what about other stuff? I guess a more complete syncing solution would be cool.\r\n\r\nAnd even if it comes to syncing music, I would rather use Banshee since I think it will be more common in the future. Its userbase is growing and its development is fast. It is feature-rich and actively supported by distros like Ubuntu and openSUSE. But thats just my personal impression. Maybe Amarok is a good idea, too."
    author: "BurkeOne"
  - subject: "open standards."
    date: 2009-07-16
    body: "The important aspect is \"open standards\" and interfaces. As of money I don't see a difference GPL licensing is shareware compatible, you can even set up a charity as KDE e.V. to finance development with donations or membership fees."
    author: "vinum"
  - subject: "Can you explain? In my"
    date: 2009-07-17
    body: "Can you explain? In my opinion it will be the best thing to have everything under GPL2/3 (it's just wishful thinking right now, but apps which are the most important for me are actually under this license :>)."
    author: "pslomski64"
  - subject: "And Palm uses iTunes only to"
    date: 2009-07-17
    body: "And Palm uses iTunes only to sync music, so Amarok is the perfect replacement."
    author: "dtritscher"
  - subject: "nothing to do with the iPod"
    date: 2009-07-17
    body: "This has nothing to do with the iPod, but with iTunes. Palm wanted people to just use iTunes to fill their Pre, Apple stopped that. This wouldn't have happened with Free Software, and Amarok can easily become a viable option for all those Pre users out there.\r\n\r\nPalm gains less dependencies on one of their competitors and people win because Amarok exceeds iTunes in features and possibilities (ever tried downloading music off of your ipod? or using the same ipod on different computers? iTunes makes both impossible, Amarok can do both, easily, by default)."
    author: "sebas"
  - subject: "Amazon would be sweet"
    date: 2009-07-17
    body: "Amazon music store would be pretty sweet actually, I can definitely see myself buying stuff off it if the shop is well integrated in Amarok itself (ie one click purchase, song copied to my library straight away); bearing in mind Amarok's ipod support is pretty good (does it support ipod video yet?) I think this could be a real winner.\r\n\r\nAmazon would have nothing to lose and a lot to gain as other music players follow suit and integrate their shop as well (winamp, plugins for windows media player, that type of thing)."
    author: "NabLa"
  - subject: "In the same way, you could write..."
    date: 2009-07-17
    body: "\"I would rather use Gnome [as a desktop] since I think it will be more common in the future. Its userbase is growing and its development is fast. It is feature-rich and actively supported by distros like Ubuntu and openSUSE. But thats just my personal impression. Maybe KDE is a good idea, too.\"\r\n\r\nYou get the point? (Hint: Diversity! Diversity! Diversity! And no, I won't be doing the monkey dance.)"
    author: "majewsky"
  - subject: "Need more than music"
    date: 2009-07-18
    body: "From my point of view it is not good to have several applications for syncronising media content on a phone/media player. It is just to complicated for many users. Just these days I see my son (15) struggeling with the different pplications for media handling on openSUSE.\r\n\r\nFor ease of use such an application needs photo, movie and music capabilities. Amarok very might well be part of such an application.\r\n\r\nPreferebly such an application would convert the media(if needed) to the right format on the fly before transsfering it to the media player.\r\n\r\ncu"
    author: "birgerk"
  - subject: "I agree to music and video"
    date: 2009-07-29
    body: "but photo? There are several applications dealing with photo management and editing, the best being digikam in my opinion, and that kind of functionality just doesn't fit into a media player/synchronizer. I think for photos digikam or a similar program should be used, while amarok is fine for music and video."
    author: "blueget"
---
At the recent Gran Canaria Desktop Summit in Las Palmas, Gran Canaria, <a href="http://amarok.kde.org">Amarok</a> developers <a href="http://commonideas.blogspot.com/">Bart Cerneels</a> and <a href="http://amarok.kde.org/blog/categories/18-freespirit">Nikolaj Hald Nielsen</a> gave a <a href="http://www.grancanariadesktopsummit.org/node/157">talk</a> about how a community-developed Free Software project like Amarok can work with businesses in a way that benefited both, without compromising on the spirit or openness of the project.

One of the things that was touched upon was the recent release of the Palm Pre smartphone which relies on Apple's iTunes software for synchronising music with a computer. An interesting question asked was what would happen if Apple decided to block the Pre from using iTunes. Now, just over a week later, this is exactly what happened. <a href="http://www.gearlog.com/2009/07/breaking_apple_blocks_itunes_s.php">Apple has indeed blocked the Pre</a> from using iTunes with its latest update.
<!--break-->
Unfortunately, this is just business as usual in the world of proprietary software. In the end, Palm will surely find a way around this, but in the meantime, the users are being held hostage. Adding insult to injury, many Palm Pre owners have likely been purchasing music from iTunes to put on their new smartphones, thus becoming Apple customers as well, so in the end this move hurts Apple's music sales too. 

In the talk, the following alternative was considered: Palm could help the Amarok team get the Windows and Mac versions of Amarok polished and ship that as their synchronising client. This would yield many benefits for both parties. As far as Palm is concerned, by relying on iTunes for moving content to the Pre, they have made it clear that they do not consider the synchronisation client a differentiating feature of the device. By using a Free and Open Source application like Amarok instead, Palm would get a synchronising client that runs on Windows, Mac, and unlike iTunes, also on GNU/Linux and other Free and Unix-like platforms. Furthermore, it would run on architectures not supported by Windows or Mac, such as the upcoming breed of ARM based netbooks. They would also avoid being at the mercy of one of their competitors who can, and now has, made it impossible to access the Pre using newer versions of the iTunes software. Also, since Amarok is very actively developed, even if Palm initially has to put in quite a few man hours to get the Windows and Mac versions ready, this solution would still be much cheaper than building their own client from scratch and having to actively support it. Palm would even be free to ship a version of Amarok with custom branding if they chose to do so, something that would definitely not be possible with iTunes.

Of course, none of this is specific to Amarok and the Palm Pre. It is just an example of a collaboration that would make sense in many instances where a company is selling a hardware product and needs to bundle a supporting software solution. As long as the company is not trying to market the software as a unique defining characteristic of the hardware product, bundling existing Free and Open Source software would make economic sense, but more importantly it would also benefit the customers as proprietary lockouts by a third party (like the one Apple is currently attempting with iTunes) could not happen. Also, if several companies decided to ship the same application with their products, the added development resources on the software project would benefit all users, as bugs would get fixed more quickly and overall development would speed up.

<br><center><img src="http://dot.kde.org/sites/dot.kde.org/files/amarok_multiple_collections.png"></center>