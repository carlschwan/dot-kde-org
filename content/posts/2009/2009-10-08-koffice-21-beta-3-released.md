---
title: "KOffice 2.1 Beta 3 Released"
date:    2009-10-08
authors:
  - "jriddell"
slug:    koffice-21-beta-3-released
comments:
  - subject: "Very looking forward to this."
    date: 2009-10-09
    body: "The decisions made for overall architecture and UI have already sold me on KOffice, and now it looks like the project is going just the way it should.  I'm already using KOffice 2.0x for probably 20% or 30% of my tasks, but 2.1 looks like it could go over 50%.\r\n\r\nGreat work, and thank you to everyone.\r\n\r\nAs suggested in the release info, I'll get active on the KOffice forum for more detailed feedback."
    author: "Jerzy Bischoff"
---
<div style="margin: 1ex; padding: 1ex; border: thin solid grey; float: right">
<img src="http://dot.kde.org/sites/dot.kde.org/files/kritagentlebot-wee.png" width="250" height="185" />
"Gentlebot" made with Krita
</div>
The KOffice team is happy to <a href="http://www.koffice.org/news/koffice-2-1-beta-3-released/">announce the third beta of the upcoming KOffice 2.1</a>.  This extra beta has been added to ensure the highest quality for the final 2.1 release.  The KOffice team has worked overtime and can show a longer list of fixed bugs than ever. See <a href="http://www.koffice.org/changelogs/koffice-21beta3-changelog">the full changelog</a> for the impressive details. You can <a href="http://download.kde.org/download.php?url=unstable/koffice-2.0.82/src/koffice-2.0.82.tar.bz2">grab the source</a> or install packages available for your distribution.
<!--break-->
