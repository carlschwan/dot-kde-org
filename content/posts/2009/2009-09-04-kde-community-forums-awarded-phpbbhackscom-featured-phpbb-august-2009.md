---
title: "KDE Community Forums Awarded phpBBHacks.com Featured phpBB For August 2009"
date:    2009-09-04
authors:
  - "Justin Kirby"
slug:    kde-community-forums-awarded-phpbbhackscom-featured-phpbb-august-2009
comments:
  - subject: "He's right about the style"
    date: 2009-09-05
    body: "He's definitely right about the quality of the customisation. I hadn't even realised it was phpbb until this award came up - it's just really beautifully designed so it looks like a KDE website rather than yet another forum.\r\n\r\nMust be nice for the people behind the forums to have recognition from outside the project too."
    author: "swjarvis"
  - subject: "Yes Kde forum looks way"
    date: 2009-09-06
    body: "Yes Kde forum looks way classy. Even the brainstorm ajax enabled page is stunning. I just went and registered. Love Kde looks except that its bulky."
    author: "zenwalker"
  - subject: "Thanks!"
    date: 2009-09-06
    body: "Thank you for having me. :) Again, great work!\r\n\r\nThanks,\r\n\r\nPatrick"
    author: "Patrick"
---
<p>Over the last year the <a href="http://forum.kde.org/">KDE Community Forums</a> have served as a premier platform for the KDE community to communicate with each other. With over 13,000 registered users generating more than 15,000 topics of discussion the site is growing by the minute. A little over a month ago, the forums introduced a <a href="http://dot.kde.org/2009/06/29/new-look-and-features-kde-community-forums">new look</a> while moving over to the <a href="http://phpbb.com/">phpBB forum software</a>.  These improvements have clearly caught the attention of many people as  KDE's Community Forums were selected as the 
<a href="http://www.phpbbhacks.com/forums/congratulations-to-kde-community-forums-and-erdrron-vt75619.html">phpBBHacks.com Featured phpBB for the month of August 2009.</a></p>
<p>Read on for an interview with Patrick O'Keefe, admin and webmaster at the <a href="http://phpbbhacks.com">phpBB hacks forum</a>.</p>
<!--break-->
<p><b>Patrick, thanks for taking the time to do this online interview with us.  Can you tell us a little bit about your background and how you got into coding?</b></p>

<p>Actually, I’m not a coder. Sure, I know my way around some HTML and CSS and can fix the occasional PHP error, but that’s about it. I run a network of websites called the iFroggy Network (http://www.ifroggy.com). Mostly, I utilize freely available open source software to use on my sites. I’ve been professionally developing websites since 1998 and have been managing online communities since 2000. I’m also a writer and my book “Managing Online Forums” (http://www.managingonlineforums.com) was published by the American Management Association in 2008. It’s a practical guide to managing online forums, communities and social spaces, which is something I’m really passionate about.</p>

<p><b>It looks like you started the phpBBHacks site back in 2001?  When did you first become interested in phpBB and at what point did you dedicate a site to hacking on it?</b></p>

<p>Yes, phpBBHacks.com launched in April of 2001. I became interested in phpBB originally to run on my own sites – the first was SportsForums.net, which I still manage today. As I got to using it, I also started to customize it, install hacks and tweak things to my liking. I wanted there to be a database of hacks and customizations for phpBB and, at that time, there wasn’t one. So, I created it and have run it ever since. It is a very special site to me because of the great relationships that we enjoy with awesome phpBB hack and style authors and users of the software, which are the two audiences that we care about most.</p>

<p><b>I noticed you have over 32,000 registered users and there are more than 72,000 topics on your own forums.  That's quite a following!  How long did it take for the site to really take off?</b></p>

<p>I appreciate the kind words – it means a lot. The site was a lot of hard work and, to be honest, the creation and management of it was and, in some cases, continues to be a very trying and challenging endeavor because of the politics at work. But, we’ve always aimed simply to be a useful resource for authors and for people who run phpBB and, luckily, our hard work allowed us to get going fairly quickly. I always say that I am a marathon runner and not a sprinter (this phrase is courtesy of Sean “Diddy” Combs). The numbers are nice, but my goal is always the experience, the environment, the way the site feels.</p>

<p><b>According to your site I see the Featured phpBB Award got its start in 2002.  What originally inspired the idea?</b></p>

<p>What inspired the idea was a desire to highlight and honor one well conceived phpBB every month. It was the first award of its kind and I have enjoyed the ability to award people who do cool or interesting things with the idea of receiving nothing in return. The award is fun because it’s totally out of the blue. You can’t really nominate yourself for it. So, you’ll have no idea it’s coming… and then bam, there it is.</p>

<p><b>How did you find out about the KDE Community Forums?  Are you a KDE user?</b></p>

<p>I discovered the KDE Community Forums in my research looking for phpBB installations to consider for the award. I actually am not a KDE user. I found the community, loved the integration and the customization and made a note of it.</p>

<p><b>What was it about the KDE Community Forums that earned the award for August 2009?</b></p>

<p>It was the customization of the style, for sure. It is beautiful. I love when I see details taken care of and that was one of the things that stood out to me. Details are very important – things like edges and borders and the smallest of icons. A link color. I’m willing to forgive some details, but if a community has taken care of all the ones I can see, all the better. Details matter and they were taken care of.</p>

<p><b>Do you have any suggestions on ways the KDE Community Forums can continue to improve?</b></p>

<p>I have to say that my experience with the community, on a people and atmosphere basis, is very limited. So, it’s hard for me to offer specific advice. But, if I put my community management hat on for a second… one of the things I always say, when it comes to support and technical forums… is to always value kindness over knowledge. In other words, everyone being super smart isn’t what makes your site – what makes your site is the people being super nice. If you’re doing that, you are well on your way because character is everything. I wish you the best of luck and continued success.</p>