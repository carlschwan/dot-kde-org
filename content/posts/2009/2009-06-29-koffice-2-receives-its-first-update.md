---
title: "KOffice 2 Receives its First Update"
date:    2009-06-29
authors:
  - "jriddell"
slug:    koffice-2-receives-its-first-update
---
Today, exactly one month since the <a href="http://www.koffice.org/2009/05/koffice-200-released/">release of KOffice 2.0.0</a>, the KOffice team <a href="http://www.koffice.org/2009/06/koffice-201-released/">releases the first bugfix release in the 2.0 series</a>. This release contains no new features but lots of bugfixes for almost all of the components in KOffice 2.0. We are planning at least two more bugfix releases of 2.0 before starting the 2.1 series in October this year.

More details of the 2.0.1 release can be found in the <a href="http://www.koffice.org/2009/06/koffice-2-0-1-changelog/">changelog</a>. Source packages can be found <a href="http://download.kde.org/download.php?url=stable/koffice-2.0.1/">on one of the various mirrors</a>. This version of KOffice is translated to no less than 27 languages, one more than 2.0.0. The new language for this release is Norwegian Bokmål, one of the two written forms of Norwegian.
