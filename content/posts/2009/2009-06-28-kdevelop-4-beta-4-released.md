---
title: "KDevelop 4 Beta 4 Released"
date:    2009-06-28
authors:
  - "jriddell"
slug:    kdevelop-4-beta-4-released
comments:
  - subject: "Thx to KDev devs!"
    date: 2009-06-28
    body: "I really like how this RC brings new methodologies into KDevelop for coping with many source files, albeit I didn't have the time to test them."
    author: "majewsky"
  - subject: "Thanks for all the hard work"
    date: 2009-06-30
    body: "Thanks so much for all the hard work spent on making KDevelop 4.  I've always come back to KDevelop as my editor of choice after trying many other IDE/editors.\r\n\r\nIs there an ETA for either a MacPort or Fink Package for KDevelop?  I'd love to run it natively on my mac and up until now have not been able to compile/install it in either Fink or MacPorts.\r\n\r\nThanks again."
    author: "oilderyk"
---
On behalf of the KDevelop team I am happy to announce the Beta 4 release of KDevelop 4. This release includes some major new features, such as <a href="http://zwabel.wordpress.com/2009/06/21/kdevelop4-ui-areas-working-sets-etc/">working sets</a> (only available when building with KDE 4.3), integration of the quickopen functionality into the toolbar and a new perspective switcher (see the upper right corner of the mainwindow). We have of course also fixed again a lot of bugs, for example non-text files such as images will not crash KDevelop anymore when closing them, Valgrind execution is working again, the debugger's variable view has been fixed and a lot of crash fixes related to parsing and code-completion popups. Altogether we have managed to fix  <a href="http://bugs.kde.org/buglist.cgi?product=kdevelop&resolution=FIXED&resolution=WORKSFORME&bugidtype=include&chfield=resolution&chfieldfrom=2009-05-24&chfieldto=2009-06-24&order=Bug+Number&cmdtype=doit">30 bugs</a> in just 30 days.

I would like to point out that you will need the latest beta or the soon-to-be-released RC1 of KDE 4.3 to experience all the new features as some of them need the newer KDE libraries. Furthermore this is the first beta release that officially builds, starts and can load projects under Microsoft Windows. For that you will definetly need the latest Subversion version (or RC1) as it contains a lockup-fix for Solid which can be triggered with some combinations of Windows and Microsoft Compilers.

You can fetch the source packages for KDevPlatform and KDevelop from the <a href="http://download.kde.org/download.php?url=unstable/kdevelop/3.9.94/">KDE mirrors</a>. Make sure you download both packages and build KDevPlatform before KDevelop.  If you build yourself make sure to read and understand <a href="http://apaku.wordpress.com/2009/06/24/setting-up-environment-for-running-kdevelop4/">Setting up Environment for running KDevelop4</a>.
