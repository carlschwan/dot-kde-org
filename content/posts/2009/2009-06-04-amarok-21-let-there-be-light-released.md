---
title: "Amarok 2.1 \"Let There Be Light\" released"
date:    2009-06-04
authors:
  - "nightrose"
slug:    amarok-21-let-there-be-light-released
comments:
  - subject: "Congrats "
    date: 2009-06-04
    body: "Hi guys. \r\n\r\nFirst of all, congratulations on the new release! Seems like there are plenty of improvements. The bookmarks system seems great! Improved last.fm support is really nice too. \r\n\r\nOn the bad side, I still find it \"difficult\" to use. My most common usage is, handpick a few songs from  my collection to the playlist, and then play it. \r\n\r\n- With Amarok 1 I'd just browse the collection and each time I find a song I want, I put it in the appropriate place in the playlist. If I have n songs that means n tiny drags. \r\n\r\n- With Amarok 2 if I do the same I end up having to do n long ass drags. It's really tiring. If I use right click->append that's n selections +  plus a bunch of drags to sort them into the order I want in the end. \r\n\r\nI'm guessing this kind of pattern is very common among users. So my question is, am I missing something here? How do you guys usually do this in Amarok 2? "
    author: "jadrian"
  - subject: "Just double-clicking a song"
    date: 2009-06-04
    body: "Just double-clicking a song also adds it to your current playlist, no need to drag it all over the screen."
    author: "Zhick"
  - subject: "or just drag it onto the \"add"
    date: 2009-06-04
    body: "or just drag it onto the \"add to playlist\" drop in the center context view. It'll be the same distance as in amarok1"
    author: "blantonv"
  - subject: "Fitt's law"
    date: 2009-06-04
    body: "The old playlist was better in this regard because you could drop it anywhere in the empty area.\r\n\r\nOn the other hand, the context action system has the advantage of the target always being at the same place, while the target area in the old playlist shrunk while you added songs."
    author: "majewsky"
  - subject: "\"Let there be light\""
    date: 2009-06-04
    body: "Yet another Mike Oldfield reference? :)"
    author: "NabLa"
  - subject: "Maaaaaaaaaaaybe ;-)"
    date: 2009-06-04
    body: "Maaaaaaaaaaaybe ;-)"
    author: "nightrose"
  - subject: "Doesn't solve"
    date: 2009-06-04
    body: "Hi Zick, \r\n\r\nDouble clicking, just the like right click and append in my example, avoids the big dragging. But like I said then you still have to go and sort them, which means a bunch of drags. \r\n\r\nSo for N songs in amarok 2, you're suggesting N double-clicks (2*N clicks) + a bunch of drags. As opposed to just N tiny drags in amarok 1. "
    author: "jadrian"
  - subject: "no quite"
    date: 2009-06-04
    body: "And then you still have to go and sort them in the playlist, which involves dragging again. That's pretty much my right click+append example. "
    author: "jadrian"
  - subject: "Resize?"
    date: 2009-06-04
    body: "Dunno, tried re-sizing the side panels and context area so it's only a short drag, then restore sizes when you're done? I think you can even completely hide the context area in 4.1?"
    author: "odysseus"
  - subject: "auto tag my songs"
    date: 2009-06-04
    body: "When I select \"grouping\" on playlist layout, it automatically tags many songs with the same artist name of some random groups. Staying away from amarok for now."
    author: "patcito"
  - subject: "Playlist scrolling + moodbar + switching playlists"
    date: 2009-06-05
    body: "Hi ! Congrats for this release ! I now definitely switch to Amarok 2.x. A few remarks & suggestions though :)\r\n\r\n1) Why is scrolling in the playlist not smooth ? (on my setup at least :)\r\n\r\n2) I would really love to see the moodbar comeback :)\r\n\r\n3) Would it be possible to make the \"applets\" in the middle pane resizable ?\r\n\r\n4) There's one feature I would love, the support of multiple playlists that can be easily switch with a tab (for instance). I know there is a very powerful playlists support, but it doesn't enable to quickly switch from one playlist to another, while automatically saving the changes made to each of them. \r\n\r\nImagine you have a playlist with your favorite tracks. You load it in the playlist editor, modify it. Then save it. It'll be saved as a distinct playlist. So you have to delete the former one, rename the new one...\r\n\r\n\r\nCheers & bravo, again !"
    author: "torturedutopian"
  - subject: "Labels"
    date: 2009-06-06
    body: "I miss the labels so much. For all improved stability and speed over 1.4.10, 2.1 is still hard to use I can't find a way to set labels."
    author: "alecs1"
---
After 5 months of hard work the <a href="http://amarok.kde.org">Amarok</a> team is proud to announce the next major release, Amarok 2.1, codenamed "Let There Be Light".
<p>
Since the release of 2.0 we have gotten a lot of feedback and have already integrated some of it in 2.0.1 and 2.0.2. This new release includes a new look, many improvements, new features and brings back old favorites.
The playlist can now be customized to only show the information you care about, matching the way you use Amarok. The layout of the Context View was changed, while navigation and applet management has been made easier. We added support for Amarok URLs to make it easier to share that crazy rock song you just found on <a href="http://www.jamendo.com">Jamendo</a>. You can now bookmark track positions, allowing you to save a part of a song for later or letting you start your audio book where you left off.
</p>
<p>
What's more, these are just a few of the goodies we added. Find out more by reading the <a href="http://amarok.kde.org/en/releases/2.1">release notes, download Amarok 2.1</a> and rediscover music!</p>

<center><a href="http://dot.kde.org/sites/dot.kde.org/files/amarok21.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/amarok21t.png"></a></center>