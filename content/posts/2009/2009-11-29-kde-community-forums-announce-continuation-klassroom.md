---
title: "KDE Community Forums Announce the Continuation of Klassroom"
date:    2009-11-29
authors:
  - "einar"
slug:    kde-community-forums-announce-continuation-klassroom
---
<p>Early on in the lifetime of the <a href="http://forum.kde.org">KDE Community Forums</a>, the staff launched regularly-held courses for people willing to help KDE called <a href="http://forum.kde.org/viewtopic.php?t=16220&f=9" title="Original announcement">"Klassrooms"</a>. For each of these courses, a mentor (usually a KDE contributor, but not limited to them) guided a group of "students" towards a simple, definite goal that would improve KDE, for example fixing simple bugs in an application. However, the courses were not limited to coding: documentation, promo and other important areas were handled as well.</p>

<p>Since they were held on the forums, Klassrooms possessed an advantage over real-time courses: students could progress at their own pace and the mentor was able to oversee the development even with limited time at his/her disposal.</p>

<p>Due to the large growth of the KDE Community Forums over the last year, the staff had to focus on improving the forums themselves and so there was little or no time to hold new Klassrooms. Until today ...</p>

<p>The KDE Community Forums team is pleased to announce a <a href="http://forum.kde.org/viewtopic.php?f=4&t=84153">call for new Klassroom mentors</a>, to allow increased effort to be focussed on these courses. The existing staff are already quite busy keeping the forum up and running, so this will provide the initiative with dedicated resources.</p>
<!--break-->
<p>Becoming a mentor is easy. You need to have an idea on what topics you can hold a course on (for example, junior jobs or documentation) and have some time at your disposal.  Then, it's just a matter of reading <a href="http://forum.kde.org/viewtopic.php?f=71&t=84115">the simple guidelines</a> and sending a message to the forum administrators. A course on any activity that improves user experience or extends expertise in any area would make a welcome addition to Klassrooms. Good topics are programming (fixing small bugs in a KDE application), documentation (cleaning up documentation, improving wiki content, etc.) and promo work (web content, screencasts, various documents ...). Of course, these are just examples; any other suggestions are also welcome.</p>

If you would like to discuss your idea before committing to it, feel free to contact the forum administrators (Luca "einar77" Beltrame, Ingo "neverendingo" Malchow, Ben "bcooksley" Cooksley and Sayak "sayakb" Banerjee) on the <a href="irc://chat.freenode.net/kde-forum">#kde-forum</a> channel on the Freenode IRC network.