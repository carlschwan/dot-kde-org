---
title: "KOffice Developers At The First ODF Plugfest"
date:    2009-06-19
authors:
  - "jospoortvliet"
slug:    koffice-developers-first-odf-plugfest
comments:
  - subject: "Cool!"
    date: 2009-06-24
    body: "This is very important work - good interoperability is neccesary for ODF to spread in use!\r\n\r\nThe link to Doug Mahugh's blog post is a bit messed up, it should be this: http://blogs.msdn.com/dmahugh/archive/2009/06/16/odf-plugfest-the-hague.aspx\r\n\r\nIn regards to adoption of ODF in Scandinavia, I'm pretty sure it's progressing quite slowly here in Denmark as well. "
    author: "jramskov"
  - subject: "The officeshots.org link is"
    date: 2009-06-24
    body: "The officeshots.org link is broken, btw."
    author: "nethad"
  - subject: "thanks to you and the"
    date: 2009-06-24
    body: "thanks to you and the previous poster - I fixed all links. Bloody windows screwed up the apostrophes... That'll teach me to work on KDE stuff at work ;-)"
    author: "jospoortvliet"
---
The first ODF Plugfest was held on the 15th and 16th of June 2009 in the Royal Library in the Netherlands. The meeting was initiated by the Dutch government and the <a href="http://www.opendocsociety.org/">OpenDoc Society</a>. Jos van den Oever, brand new employee of <a href="http://kogmbh.com/">KO GmbH</a> and Sven Langkamp, proud developer, went on behalf of the KOffice team. With over forty organisations and a total of sixty representatives from businesses, public sector organizations, open source projects and research institutions, the meeting was an incredible success.

Read on for an impression of the event.
<!--break-->
Dutch minister for Foreign Trade Frank Heemskerk held an opening speech, outlining why the Dutch government pushes the usage of the three Os: Open Standards, Open Content and Open Source Software. Interoperability brings a better market and thus leads to better quality, lower prices, more innovation and vendor independence. The Netherlands wants to join the Scandinavian countries in becoming one of the leading countries in adopting ODF and Open Source software.  Unfortunately, as we learned later on, the adoption of ODF is progressing very slowly. Fabrice Mous, one of the organizing members of <a href="http://www.noiv.nl">NOIV</a> (Netherlands Open In Connection) outlined the current rather unfortunate situation. ODF adoption has a very low priority in local government throughout the Netherlands, in part because they feel they have little use for it. The work by NOIV might change this during the coming years.

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://dot.kde.org/sites/dot.kde.org/files/2009_ODF_Plugfest.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/2009_ODF_Plugfest_small.jpeg" width="400" height="171" alt="photo" /></a><br />
Group Photo! (credits Zaheda Bhorat and anonymous security person)
</div>

Further talks went into subjects such as adoption and innovation. The work by various organizations was put into the spotlight, ending with <a href="http://www.officeshots.org/">OfficeShots</a>. Officeshots offers application developers a way to test their ODF compatibility. You can upload a document and receive the output (in the form of screenshots, PDF or roundtrip ODF files) from various office suites. KOffice developers value OfficeShots as a valuable tool for improving the upcoming version 2.1. Currently, Google Docs and Microsoft Office are missing from this service, and while the attendants asked the Google and Microsoft representatives, it is not sure they will be available soon.

Microsoft’s Doug Mahugh gave a talk (using OpenOffice) explaining what Microsoft has been doing with their ODF implementation. They provided notes on the implementation and their interpretation of the specifications. During the Plugfest it was agreed among the participants to follow this example and share notes on the specific implementations. Later Doug was found talking to Sven – he was interested in the state of the KOffice installer on Windows. Apparently Redmond has had a look at KOffice… Doug’s <a href="http://blogs.msdn.com/dmahugh/archive/2009/06/16/odf-plugfest-the-hague.aspx">blogpost about the Plugfest</a> is worth a read. You can watch the rest of the talks at <a href="http://vimeo.com/tag:odfplugfest">vimeo</a>

The value of the meeting, however, lies not only in the talks, but also in the informal chatter and the hard work being put in the common test scenarios. The organisation initiated scenario-based testing, where specific parts of the ODF standard were tested by following instructions to create a document. Several bugs impeding greater interoperability between and among ODF-supporting software were uncovered and discussed, including one interoperability issue simultaneously present in KOffice, OpenOffice and Microsoft Office. In response to the positive reception from participants, a website will be developed where virtual "Plugfests" can be conducted online.

The event felt like ‘everybody’ was there, with ‘corporate dudes’ from Sun, IBM and Microsoft, to government agencies like the Munich city government, research institutes like Fraunhofer and of course members from communities like <a href="http://www.abisource.com">Abiword</a>, <a href="http://koffice.kde.org">KOffice and <a href="http://opengoo.org">OpenGOO</a>. Putting so many people with so many different perspectives and ideas together leads to an interesting atmosphere, competition and cooperation hand in hand. All in all, it turned out to be two productive and challenging days.

The KOffice team thanks Fabrice Mous and Michiel Leenaars, the OpenDoc society, the Dutch government and all the other sponsors for organizing the workshop.

Some pictures of the event can be found <a href="http://picasaweb.google.com/ODFPlugFest/ODFHagueEvent#">on Picasaweb</a> and <a href="http://www.flickr.com/photos/dougerino/tags/odfplugfest/">Flickr</a>.