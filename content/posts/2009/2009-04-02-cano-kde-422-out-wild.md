---
title: "\"Cano\" (KDE 4.2.2) Out in the Wild"
date:    2009-04-02
authors:
  - "sebas"
slug:    cano-kde-422-out-wild
comments:
  - subject: "Awesome"
    date: 2009-04-02
    body: "I must say, the list of improvements and fixes to KHTML is fantastic!  Installing my updates now!"
    author: "digimars"
  - subject: "Missing changelogs"
    date: 2009-04-02
    body: "Finally! :-)\r\n\r\nThe specific changelogs, such as http://www.kde.org/announcements/changelogs/4_2_2/kdelibs.txt, are still 404."
    author: "Haakon"
  - subject: "KHTML developer are rock !"
    date: 2009-04-02
    body: "Just I want say thank you. Keep the good hard work."
    author: "zayed"
  - subject: "To generate a subversion"
    date: 2009-04-02
    body: "To generate a subversion change log, use this command in Konsole:\r\n<code>svn log svn://anonsvn.kde.org/home/kde/branches/KDE/4.2 -r 934488:947394 &gt; kde-4.2.1-4.2.2-changelog.txt</code>\r\n\r\nTo find details about commit rNNNNNN:\r\nhttp://websvn.kde.org/?view=rev&revision=NNNNNN\r\n\r\nTo find details about bug #NNNNNN:\r\nhttp://bugs.kde.org/show_bug.cgi?id=NNNNNN\r\n\r\nEnjoy!"
    author: "christoph"
  - subject: "Thx to Kate developers!"
    date: 2009-04-02
    body: "Thanks for fixing this cursor positioning bug (I did actually not report it because I got used to it somehow)."
    author: "majewsky"
  - subject: "A W E S O M E"
    date: 2009-04-02
    body: "big thanks to everyone involved :)"
    author: "harry1701"
  - subject: "mum"
    date: 2009-04-02
    body: "Good work guys. KDE 4.2 is looking quite good. I am very curious to see what's coming next. I'm planning to move mum from windows to openSUSE 11.2 with KDE 4.3 when it is released, so I'm putting a lot of faith on you guys. Please make KDE 4.3 an amazingly polished and intuitive desktop. For my mum. Thanks!"
    author: "jadrian"
  - subject: "KMail MAPI and PST Support"
    date: 2009-04-03
    body: "Any idea on when we'll have a KMail with MAPI and PST support like the new evolution? Exchange compatibility is a big show stopper for me using Linux at work."
    author: "sheldonl"
  - subject: "Re:"
    date: 2009-04-03
    body: "+1\r\n\r\nP.S: KHTML developers rock, but they are not rocks :p"
    author: "fyanardi"
  - subject: "Flash also seams to work"
    date: 2009-04-03
    body: "It seams that flash also works again. Thanks!"
    author: "vdboor"
  - subject: "I wish that were so here, but"
    date: 2009-04-03
    body: "I wish that were so here, but nspluginviewer still barfs for me."
    author: "stumbles"
  - subject: "I just tried Konqueror out of"
    date: 2009-04-03
    body: "I just tried Konqueror out of curiosity because I have sworn by the Fox but I have to admit that Konqueror is coming on quite fine. I didn't have to do anything for flash to work and it seems to have made a good amount of improvement in speed. If the devs could give it the Fox's add-on capability then I would switch without warning ;) "
    author: "Bobby"
  - subject: "Works for me, but..."
    date: 2009-04-03
    body: "Flash works for me on FreeBSD, but not on Linux. \r\n\r\nHey Adobe! Open up Flash so we're not reliant on your flaky binary blobs!"
    author: "Brandybuck"
  - subject: "My brother fell in love with"
    date: 2009-04-03
    body: "My brother fell in love with KDE 4.2 but there were still a few problems like playing audio CDs (which didn't show up), detecting a digital camera etc, so he had to unwillingly use KDE 3.5.10 for his multimedia stuff. KDE 4.2.2 seems to have fixed the most of the issues that he was having with 4.2 but his next update will be 4.3 (when openSuse 11.2 is released) so I just hope that everything will be working smoothly by then so that he can enjoy the goodness of KDE 4. "
    author: "Bobby"
  - subject: "Flash (10.0.22.87) works fine"
    date: 2009-04-03
    body: "Flash (10.0.22.87) works fine here on Linux+Firefox, absolutely no issues there. So I am not real sure, or convinced the issue is the result of a binary blob. I *can* get konqueror to work with flash using the kmplayer dance, though it is not a very satisfactory hack. \r\n\r\nFor those who say flash works fine in konqueror, and are not using the kmplayer hack, I would like to know just where your distro installs, or symlinks the .so to."
    author: "stumbles"
  - subject: "Working Flash"
    date: 2009-04-04
    body: "On OpenSuse it's installed in /usr/lib/browser-plugins/. And for me it did not work until recently, eg I tested again after reading this tread. KDE 4.2.2 svn built from source with Qt 4.5. \r\n\r\nObviously the reason for the issues with Flash are not because of it being a binary blob. But it is the reason why it is incredibly hard for the Konqueror developers to fix.\r\n\r\nAs explained by the Konqueror developers untold times, the developers of Flash do something not entirely according to the plug-in specification(Other nsplugins does not seem to have such problems). Opera also have issues with Flash on Linux. Flash is developed and tested against Firefox on Linux, and it seems Firefox is more tolerant when it comes to this behavior. That Flash for Linux is developed with GTK+ may have something to do with this. \r\n\r\nAnyway, by being a binary blob it's close to impossible for the Konqueror developers to point out to the Flash developers what they do wrong so it can be fixed. And also makes it very hard for the Konqueror developers to create workarounds for the problem in Konqueror.\r\n"
    author: "Morty"
  - subject: "It did for me, too, but ..."
    date: 2009-04-05
    body: "I just installed KDE 4.2.2 and ran konqueror under a dedicated user.  Flash worked just fine.  Ran it under my normal user account and it didn't... so I knew that it was at least possible to run Flash on my system with KDE4.\r\n\r\nIt turns out that the reason it didn't work for my normal account was that I was using the Gtk-Qt style for gtk; since libflashplayer links to gtk, the Gtk-Qt style was getting linked in, too, and, finally, gtk-qt links to Qt3.  All well and good when your konqueror is Qt3, but causes alllll sorts of clashing with Qt4.  I switched to another gtk theme (none of which, unfortunately, is as nice as KDE's HighColor Classic) and Flash was working."
    author: "Charifir"
  - subject: "Works without hack "
    date: 2009-04-06
    body: "It's working fine on my system and without hack, however I have to add that I am using openSuse 11.1. Both Konqueror 3 and 4 are using the flash plug-in without me doing anything at all. It seems like the openSuse team has been doing quite a good job :) "
    author: "Bobby"
  - subject: "jajajajaja"
    date: 2009-04-06
    body: "jajajajaja"
    author: "alexismedina"
  - subject: "Okular on Windows Vista"
    date: 2009-04-08
    body: "I just removed Adobe Reader and updated my KDE installation on Vista.\r\n\r\nOkular is absolutely stunning, it's fast as lightning and easy to use.\r\n\r\nBye, bye Adobe Reader!\r\n\r\nMany, many thanks to Okular developers and KDE for Windows developers as well!\r\n\r\nTapio"
    author: "eleknader"
  - subject: "Kano Says \"MUN GODE\"  meaning Thank You."
    date: 2009-04-09
    body: "I wish to express our sincere and profound gratitude to all members of KDE. This is a sure pointer that Kano will take the world of FOSS by surprise. An acheivement of this nature is sure to ginger the spirit of FOSS in our enthusiastic youth. I'm glad to also inform you that we are garthering a list of FOSS enthusiast within the studentship of Bayero University. We'll keep all informed on the progress. \r\nAdditionally people have been asking me; Kano starts with a \"K\" while the \"Cano\" on KDE starts with a \"C\" can anyone explain why?\r\nThanks all the same.\r\n\r\nAuwal Tata\r\nBayero University Kano"
    author: "tatauwal"
  - subject: "Bit late...but"
    date: 2009-04-12
    body: "I've only just installed Fedora 11 Beta with KDE 4.2.1 and after past experiences of dipping a curious toe into the rapid waters that is KDE 4 I must say it is now a nice experience.\r\n\r\nMy main gripe was the speed of the desktop rendering.  It always seemed to struggle with the basics and I really thought it would never get any better.\r\n\r\nWell, I eat my words now.  Rendering is on a par with Windows.  Even Firefox seems to have had a burst of life as on GNOME it is a well known fact that it is not as fast as on Windows.\r\n\r\nMany thanks, KDE devs."
    author: "sulligogs"
---
As of today, the <a href="http://www.kde.org/announcements/announce-4.2.2.php">latest version</a> of the KDE desktop and software distribution carries the version number 4.2.2. The release, as is usual for our monthly point-releases does not have a lot of exciting new features, but makes your life just a bit easier. Many bug fixes have been backported from the trunk of KDE development. Among those fixes is a large number of improvements in KHTML, some stability work on KRunner, a long list of improvements to KMail, and many others. As they say, the devil is in the detail, so take a cup of tea and read the <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php">changelog</a> for yourself. Note that the changelog is often not complete, so be sure to upgrade and check what has been fixed in the set of programs you use.

This release contains translation updates, bug fixes and performance improvements. It should be a safe upgrade for everybody running KDE 4.2.1 or earlier versions.

The release is called "Cano", after the city in Nigeria where the <a href="http://dot.kde.org/2009/03/10/first-free-software-conference-held-nigeria">first Nigerian Free Software conference</a> was held last month.
<!--break-->
