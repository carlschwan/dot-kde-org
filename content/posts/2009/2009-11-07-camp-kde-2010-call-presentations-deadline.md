---
title: "Camp KDE 2010 Call for Presentations Deadline"
date:    2009-11-07
authors:
  - "jefferai"
slug:    camp-kde-2010-call-presentations-deadline
---
The deadline for requesting to give a presentation/talk at <a href="http://camp.kde.org">Camp KDE 2010</a> has been set for 0:00 on December 5th.

As indicated in the <a href="http://dot.kde.org/2009/10/21/camp-kde-2010-registration-open">open registration announcement</a>, if you wish to give a presentation, please indicate this preference when registering for the event. The organizers will look at the topics and handle any duplicates, and then ask for more detailed abstracts.

As the organizers plan to accommodate all that wish to present if possible, no current selection criteria has been set other than that the talk should generally be KDE-related.

There are opportunities for lightning talks and BoFs, so those that may not have enough material for a full presentation can still be accommodated.
