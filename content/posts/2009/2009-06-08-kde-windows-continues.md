---
title: "KDE On Windows Continues"
date:    2009-06-08
authors:
  - "pspendrin"
slug:    kde-windows-continues
comments:
  - subject: "Please continue the work"
    date: 2009-06-08
    body: "We all wish you the best on KDE on Windows. Your project enriches our project in many ways. It does not only broaden our target user base, but also our horizon."
    author: "majewsky"
  - subject: "Glad to hear that the project is alive"
    date: 2009-06-08
    body: "A long time ago, when deciding whether to port my application from pure Qt to KDElibs, the existence of the KDEWindows project was one of the major decision factor: I really not wanted to lose cross-platform nature of pure-Qt apps. Now KDEWindows not only allows me to show my application to my friends on their PCs but also allows some windows-only schools to use it."
    author: "vld"
  - subject: "and i continue from my side..."
    date: 2009-06-08
    body: "With digiKam and co, i continue to compile everyday all code relevant under windows and try to fix bugs step by step. I use 2 computers (one XP, one Vista) with MinGW and MSVC9 compilers.\r\n\r\nStopping KDE for windows is a non sense for me. I receive a lots of users mail, who wan t to see digiKam running properly under windows. We cannot ignore it... And progresively, digiKam run better under Windows.\r\n\r\nAlso, from my developer experience, it very interesting to compile a big program to another OS. We can found in-deep bugs, which never appear under Linux.\r\n\r\nI'm not a  big fan of M$ world, and i still to think that Linux is really a better development environment than Windows. But at least, if we can improve stability of a program, like digiKam under M$, well, i'm agree to continue...\r\n\r\nGilles Caulier"
    author: "cauliergilles"
  - subject: "Kate and so on"
    date: 2009-06-08
    body: "I think that is great but I wonder if the software installer is really the right method. First of all it is buggy and updates easily break your working application environment.\r\n\r\nAs standalone mature applications Kate and Okular and many others could become popular on the Windows platform, the KDELibs would be like VB6 runtimes. Windows user also are urgent need of a functional file manager.\r\n\r\nBut the current packaged installation method does not look very good for users. when you install programs all the apps are put in extremely slow KDE 4.x.x submenues. I don't know if that was because the environment was Vista. And many of the applications are pre-alpha stability wise.\r\n\r\nIt is merely a branding problem. 1+1 < 2\r\n\r\n"
    author: "vinum"
  - subject: "Windows?"
    date: 2009-06-09
    body: "Mod me down, hate me, burn my karma but tell me who except \u00fcber geeks needs KDE on Windows?\r\n\r\nIf it's compileable - fine, let people compile and use it. KDE with all its dependencies is still absolutely alien to the Windows platform. Window manager in Windows is irreplaceable, Windows' start menu and taskbar cannot be reliably replaced by plasma, Dolphin is not meant to browse C:/D:/E:/etc disks, and so on and so forth.\r\n\r\nPlease, use all available resources to develop KDE itself, without wasting developers time on things which only few people will try, even fewer will use and even fewer will enjoy.\r\n"
    author: "birdie"
  - subject: "I won't burn your karma"
    date: 2009-06-09
    body: "We need KDE on Windows for three reasons:\r\n\r\n1. Most businesses can't just switch to Linux. I've heard more than enough stories of workers being stucked with Windows as they're of course not allowed or able (because of special apps) to convert their boxes to Linux. KDE might provide them with a comfortable working environment to which they are used.\r\n\r\n2. Most businesses won't suddenly switch. Clear step-by-step migration paths (Windows + Office + Explorer -> Windows + OpenOffice + Konqueror -> Linux + OpenOffice + Konqueror) make it easier for the IT deciders to enter this process. (Something along the lines of \"If the users do not like Konqueror, they can still use Explorer.\") Yes, I know that Konqueror is not a good example, as many Windows users have just learned Firefox and will most probably not look into learning yet another browser.\r\n\r\n3. Having FOSS applications available on the Windows platform is crucial for attracting users.* Not many people go into the store and buy a SuSE box, but many people get single FOSS apps like OOo or Firefox because they read about it in some magazine, or some friend recommended it to them.\r\n\r\n*More users might not seem too crucial to you, but I would really desire it, because *all* hardware vendors would finally start to treat Linux equally once it has around 10% market share."
    author: "majewsky"
  - subject: "Windows! (although I wont touch it with a 10 foot pole)"
    date: 2009-06-09
    body: "\"Mod me down, hate me, burn my karma but tell me who except \u00fcber geeks needs KDE on Windows?\"\r\n\r\nNah, I'd much rather try having a civil discussion about it, but that's just me! :-)\r\n\r\nA few years ago (leading up to Akademy 2007 IIRC) we had a huge discussion on the planet about the merits of making KDE applications available on Windows. The core of my argument for doing that then was, and still is, that its really in the interest of KDE to do this because it attracts developers who would otherwise not contribute. \r\n\r\nTake Amarok for instance. The core developers spend very little time on making Amarok run on windows (I think the total amount of work I have done on this amounts to one time changing the order of some things in a CMake file as someone reported that it otherwise broke the build on Windows.) So all in all, this is not something that takes much time away from developing Amarok itself. On the other hand, the original implementation of the Last.fm service was written by a developer whose original intention was to make Amarok work better on Windows. Once he had gotten as far as he could at the time, he started, still using Windows, to hack on other stuff that benefits all users of Amarok. He did not use linux at all, and only contributed because it was possible to run and work on Amarok using Windows.\r\n\r\nSo I really think it is wrong to look at this as a zero sum game as time spent making stuff run on windows is not automatically time taken away from developing the core application. Quite contrary, making the application usable on other platforms will also attract developers who would not otherwise have worked on it. Of course there is a tipping point for this as the applications have to be working well and have a significant user base on Windows before any significant amount of developers shows up, but as my example about Amarok illustrates, people are already taking notice.\r\n\r\nAnd then there is the whole issue about philosophy. To me, Free Software is about just that, freedom. I think it would be against the spirit of that to artificially limit the platforms that our software runs on. that is for all the \"other\" guys to do, I think we are better than that! :-)"
    author: "nhnFreespirit"
  - subject: "Not the desktop"
    date: 2009-06-09
    body: "No need for hate, but it seems like you are a little confused about the scope. \r\n\r\nFirst thing to clear up, KDE are much more than just a desktop with window manager,start menu and taskbar. They are only the topping of the cake, and neither the Windows or OS X ports are about replace the native versions.\r\n\r\nAnd that is not the most important thing about KDE, it's the applications. The power of KDE are it's library and the applications made with it, and those are also interresting for the Windows platform.\r\n\r\nAnd for KDE as a whole, any developers brought in and bugs fixed by the Windows port are a net win for KDE.\r\n\r\nBTW, Dolphin handles C:/D:/E:/ just fine.\r\n"
    author: "Morty"
  - subject: "Who cares?"
    date: 2009-06-09
    body: "I have Windows installed because of educational reasons (I want to be factual when talking about Windows) but I don't care a dime aboue KDE apps running on Windows. If the whole Windows DE could be replaced by Plasma then it would be interesting but since that's not the case then it's a waste of time imo. \r\nIt's better to invest the manpower and resources to make KDE the most powerful, secure and beautiful DE on the planet than to be going through the trouble and headache to port apps to Windows. \r\nIn that case it will have a chance or replacing Windows."
    author: "Bobby"
  - subject: "sebas"
    date: 2009-06-09
    body: "Porting an application and fixing bugs in libraries that are meant to be cross-platform anyway are two different things. :)"
    author: "sebas"
  - subject: "Printing..."
    date: 2009-06-09
    body: "And if there's any Windows printing gurus out there who can lend a hand with hacking on Qt to add new features, I'd love to hear from you."
    author: "odysseus"
  - subject: "amarok, dolphin, ..."
    date: 2009-06-09
    body: "hello,\r\n\r\nreally, having all the nice kde programs available on windows is very cool. amarok, dolphin, ktorrent, kwrite, etc. and also, the educational programs are important.\r\n+1 for kde on windows for me! it's like an artist being on a smaller label with almost no air time converting to a bigger label and getting his records played on the radio..."
    author: "majorTomBelgium"
  - subject: "Printing Plans?"
    date: 2009-06-09
    body: "Hi! I wonder if you could blog at some point (if you find time) about the current state of printing in KDE? Things like what is missing; how much of this missing stuff must be restored in Qt vs KDE; general, detailed TODO lists so that people can get an idea of the scope of the problem and see which bits they personally can tackle? It would be cool to know just where we are at the moment :) Someone was asking about printing in the KDE forums just yesterday, actually ..."
    author: "SSJ_GZ"
  - subject: "1. The more business users"
    date: 2009-06-09
    body: "1. The more business users stay with Windows the less incentive they have to switch, right? :)\r\n\r\n2. KDE for Windows is not working stable enough. Qt/KDE look and feel is totally different than what native Win applications have. KDE for Windows increases Linux migration cost threefold, because users have to learn a new environment at the same time trying to cope with old Windows problems (registry, viruses, etc) and getting a new PITA by trying to work in a non-native environment which is KDE for Windows.\r\n\r\nTrust me, I've seen organizations which switched to Linux overnight and they had fewer problems than going through this intermediate hassle.\r\n\r\n3. As I've already said I have nothing except KDE on Windows, I just don't want it to get so much attention and developers time. As you try to convince me, I grow in confidence than what we really need is KDE/Qt core libraries and most popular KDE applications (like Amarok) - nothing more."
    author: "birdie"
  - subject: "That indeed makes sense, but"
    date: 2009-06-09
    body: "That indeed makes sense, but is it really necessary to port the whole KDE stack to Windows?"
    author: "birdie"
  - subject: "DE"
    date: 2009-06-09
    body: "Not the KDE-\"Desktop environment\" but the \"application space\". OO.org, Vuze, VLC, FF, TB, Audacity, Songbird, Pidgin, so many programs are today cross-platform and open source. The most users of these applications use Windows. What is missing? K3B, Kate, Dolphin, Okteta, Krita, Kolourpaint, Gwenview, Ktorrent, Okular, etc. \r\n\r\nWhat would be the outcome: \"Desktop indifference\" which means a lower migration barrier to other operating systems."
    author: "vinum"
  - subject: "\"Trust me, I've seen"
    date: 2009-06-09
    body: "\"Trust me, I've seen organizations which switched to Linux overnight and they had fewer problems than going through this intermediate hassle.\"\r\n\r\nAnd trust me, it is not always the case. For those organizations who mostly deal with Office documents or maybe doing web programming, switching to Linux can be easy.\r\n\r\nI'm one of the example of people who needs to use Windows at work, and I'm really glad KDE on Windows exists."
    author: "fyanardi"
  - subject: "For end users"
    date: 2009-06-09
    body: "Do you plan (at least in long term) to make KDE on Windows a platform for software developers to write applications and distribute them to end users? It would rock as a platform (being cross-platform and giving much functionality) but there seem to be plenty of prerequisites for that:\r\n- Most users won't care that they are installing a KDE app, and won't install KDE before, so all the libs have to be bundled.\r\n- The libs bundled with a smaller application mustn't be more than a few MBs.\r\n- But if the users installs several KDE apps, KDE libs should be installed only once to avoid consuming space and increasing download time more than once.\r\n- KDE apps take much time to start if KDE is not running. To avoid waiting several secondds for even the simplest apps, the size of the needed libraries should be minimized, and they should be left in the memory when the application exits, or even load them at Windows startup (if the user allows that).\r\n- Many common KDE settings (some which may be necessary to configure when using KDE apps) can currently be configured only from Systemsettings. As end users don't care which apps are KDE apps, they won't be able to configure these thing even if we install Systemsettings on the Control Panel automatically; the appropriate modules must be accessible from the applications if needed.\r\n- In any case where normal Windows apps use the Windows settings, the Windows settings must be used instead of a configuration option in the KDE settings."
    author: "gd"
  - subject: "This won't make me popular,"
    date: 2009-06-09
    body: "This won't make me popular, but have you forgotten who owns the Windows platform?\r\n\r\nIt's a great idea to get developers to switch to KDE devel. Being cross-platform might be the angle sought, but there are better ways to grow when you look at the long term. For example, help create that really hot new platform and you will likely gain more developers than you would when KDE is not so hot and developer resources are being spent running on Microsoft's treadmill.\r\n\r\n\"Ooops, Microsoft's update that fixed 100 'bugs' just broke many parts of KDE though it won't even be that obvious immediately except this will help KDE gain a reputation for being unstable and mercurial.. and waste even more developer time?\"\r\n\r\n\"What? KDE never really ran well on Windows ..and it was sluggish?\"\r\n\r\n\"So there aren't very many reasons for me to move to Linux since the 'few' apps that are good work decently on Windows?\"\r\n\r\nYou guys have likely done your market research. Maybe just approaching \"cross-platform\" will get enough new devs to try out Linux LiveCDs and use the real thing.\r\n\r\nIn any case, there is lot's of point-counterpoint on this thread: http://www.linuxtoday.com/developer/2009060601135OSKE .\r\n\r\nHas the KDE project considered starting a long themed series of Live distros to showcase a few apps at a time or a few features at a time or a few devel tools at a time? Make the distro a learning tool (with guided tour and learning pop-ups). Create a KDE tool to customize distros. All of these things will grab developers' attention more than a flaky port. These things are unique and their potential (esp considering the licensing and community backing) is huge. Sell what counts. Make KDE better. Don't help Monopolysoft. They won't give you an easy time while you try to beat them on their turf and rob them of their monopoly levers and profits. They are closed for a reason.\r\n\r\nThere is much work to be done to realize KDE's potential and reach developers the effective way -- by impressing the heck out of them relative to what they know. Porting to Windows is taking steps in the wrong direction: away from FOSS, away from control, away from transparency, away from a level synergistic playing field.\r\n\r\nDon't help Monopolysoft or sell Linux and KDE a little short if you can at all help it.\r\n\r\nThanks for the work you all keep doing for KDE and Linux.\r\n"
    author: "Jose_X"
  - subject: "I agree"
    date: 2009-06-09
    body: "I agree we need individual app installers (since the pan-KDE installer is too confusing for users who don't want a whole KDE distro installed), but it can't be like VB6 where every app has its own version of it since that would be a mess. So individual app installers and updaters that use an existing KDE install if it exists would be an ideal. "
    author: "eean"
  - subject: "plus"
    date: 2009-06-09
    body: "as a point of fact to the grandparent post, you can replace the start bar on Windows. :)\r\n\r\n(but this was never the main point of the KDE Windows project, because KDE is so much more then that as pointed out above)"
    author: "eean"
  - subject: "huh?"
    date: 2009-06-09
    body: "[[citation needed]] on who is calling for that."
    author: "eean"
  - subject: "KDE4 still has to catch up with KDE3 printing"
    date: 2009-06-10
    body: "KDE4 still has to catch up with KDE3 printing to let people with simplex printers upgrade, because it is not possible to print only odd or even pages. See [https://bugs.kde.org/show_bug.cgi?id=190646]. Nor is it possible to select ranges (1-5,7-12), like in KDE3."
    author: "odalman"
  - subject: "Yeah, been meaning to blog on"
    date: 2009-06-10
    body: "Yeah, been meaning to blog on the status, just life is very busy and don't even have time for much coding.  Will try do it before Akademy so we can maybe have a BoF.\r\n\r\nI actually have patches to fix the Odd/Even printing but so far on X11 only, before pushing it into Qt I need to make it cross-platform.  It also needs to be backwards compatible which I'm working on too.\r\n\r\nI have almost complete patches for ranges, but that builds on the odd/even stuff."
    author: "odysseus"
  - subject: "Didn't have to wait for too"
    date: 2009-06-10
    body: "Didn't have to wait for too long to find stories about Microsoft manipulation, in areas where they have control, in order to snuff out competition (the main example here is ODF). Ethics don't stop them.\r\n\r\nhttp://boycottnovell.com/2009/06/10/ms-discriminates-against-odf/\r\n> I have two documents, one a .odt and one a .doc. (They are the same\r\n> document, just different formats).\r\n> If I search using Vista Advanced Search by Modified Date, (they both\r\n> have the same Modified Date) ONLY the .doc file is shown.\r\n\r\nIs this a true story? It sure is characteristic of Microsoft [looking to the past years as seen through court evidence for example].\r\n\r\nhttp://www.groklaw.net/article.php?story=2009061001520015 [and http://www.robweir.com/blog/2009/06/odf-lies-and-whispers.html ]\r\n\r\n>> Now we have a closed loop. Microsoft-friendly folks rewrite Wikipedia articles, which Microsoft then picks up and serves to the world as truth. Will they correct the articles, when FUD is removed or just continue to serve up their own versions of truth?\r\n\r\nAnother discussion thread also on this KDE porting topic developed on Linux Today: http://www.linuxtoday.com/developer/2009060901335NWKE\r\n\r\n>> FOSS has a strong attraction to Linux (vs Windows) because you can participate (contribute, study, influence, etc) at many more layers. You also know that you can track changes to the software that affect yours instead of just finding out one day (maybe finding out long after the fact and without an understanding of what went wrong) that new bugs appeared in your software or it got slower after the closed source OS or key closed source libraries got updated. We know Monopolysoft's money supply and control is very much threatened by Linux. So, yes, even FOSS can be sidetracked or \"hijacked\" away from Linux when enough focus is put on this effort without it being countered properly.\r\n\r\nAnd lastly, here is one last link to a story about how well apps apparently run on Windows. See the comments, as they give a clue about how easy it is to sell that story (or illusion) that \"apps run better on Windows\" in order to help Windows and Microsoft and then cut those same apps down in size (or perhaps they were even short-changed all along) supplanting them with Monopolyware. Keep in mind, the example is a hypothetical.\r\n\r\nEthics? What ethics?\r\n\r\nhttp://www.eweekeurope.co.uk/news/ibm-goes-legal-over-microsoft-s-websphere-claims-1076\r\n\r\nThere appears to be a never-ending stream of projects and groups willing to give and give to the Windows platform. They may not survive, but who cares (right?) since another will come afterwards to take their place.\r\n\r\nPS: Might as well mention this one example of Microsoft emails: http://boycottnovell.com/2009/02/08/microsoft-evilness-galore/\r\n"
    author: "Jose_X"
  - subject: "Windows users don't need kde apps"
    date: 2009-06-10
    body: "There are so many apps available for windows so there is no need for kde applications.\r\n\r\nAmarok can't even play audio cds. K3B can't master video dvds...\r\n\r\nIs there any kde Application which uses CUDA?\r\n\r\n\r\nIf you want to program platform independent apps use java, qt (only), mono but not KDE.\r\n\r\nBy the way most users don't care about free software only in terms of free of charge."
    author: "Barristan"
  - subject: "+1"
    date: 2009-06-13
    body: "I totally agree that the current installer is suboptimal, though I understand that getting the compilation infrastructure into place is more important at the moment."
    author: "majewsky"
  - subject: "Right, although..."
    date: 2009-06-13
    body: "...there have actually been activities to make Plasma available on Windows, instead of the default UI, but this has AFAIK never been in the focus of the core KDE on Windows devs.\r\n\r\nFor example, there is a menu applet in playground/base which pops up the standard Windows start menu, which is necessary e.g. to get to the standard System Settings. This is somewhat similar to the \"Properties\" menu item in Dolphin opening the standard Windows property dialog, where you can adjust NTFS-specific access rights etc."
    author: "majewsky"
  - subject: "\"The libs bundled with a"
    date: 2009-06-13
    body: "\"The libs bundled with a smaller application mustn't be more than a few MBs.\"\r\n\r\nThat won't happen. The main advantage of the KDE platform (at least to me as developer) is the huge library stack. KDE is just too short on developers to reinvent the wheel in each and every small app."
    author: "majewsky"
  - subject: "> There are so many apps"
    date: 2009-06-13
    body: "> There are so many apps available for windows so there is no need for kde applications.\r\n\r\nRight, and there has never been any need to start new motor companies (e.g. Toyota in the 70s, or Tata nowadays), there are enough motor companies already.\r\n\r\n> Is there any kde Application which uses CUDA?\r\n\r\nIs there any Microsoft application which uses CUDA? (You see, CUDA is utterly useless for desktop apps.)\r\n\r\n> If you want to program platform independent apps use java, qt (only), mono but not KDE.\r\n\r\nArguments are out of fashion?\r\n\r\n> By the way most users don't care about free software only in terms of free of charge.\r\n\r\nWho cares about \"most users\" (except for Micro$oft)?"
    author: "majewsky"
  - subject: ">Right, and there has never"
    date: 2009-06-14
    body: ">Right, and there has never been any need to start new motor companies (e.g. Toyota in the 70s, or Tata nowadays), there are enough motor companies already.\r\n\r\nOh I didn't know that kde develops program operating systems. If you compared motor companies to companies programming operating systems it would make sense.\r\n\r\n>Is there any Microsoft application which uses CUDA? (You see, CUDA is utterly useless for desktop apps.)\r\n\r\nThere are enough applications for windows out there which use cuda. Microsoft also plans to implement cuda into their media encoder.\r\n\r\n>Who cares about \"most users\" (except for Micro$oft)?\r\n\r\nThat is the reason linux has 1% desktop market share and MS 90% and that a lot former kde users are now gnome users.\r\n"
    author: "Barristan"
  - subject: "The window manager might not"
    date: 2009-06-19
    body: "The window manager might not be replaceable, I don't know about that, but at least for the rest, you got it wrong. And what's so bad about using a different window manager (this time Window's)?\r\n\r\nThe menu and taskbar can be reliably replaced without any problems. Just replace explorer.exe as a Shell, just like GeoShell does it for example.\r\n\r\nOther projects (think cygwin) have dealt with C:... etc. disks and it really just is a display thing. Internally even Windows uses something similar to Linux for disk paths and at least with NTFS you can mount any disk as a path on another disk. So C:\\Volume2\\ can be what Windows users would call D:\\, no problem there.\r\n\r\nI would really like to see a stable KDE on Windows. Whenever I use Windows, I have to get KMail via NX, because I can't live without it!"
    author: "tharkun"
  - subject: "Why can't it be replaced? See"
    date: 2009-06-19
    body: "Why can't it be replaced? See GeoShell for example. That replaces explorer.exe as a shell, so you don't have a start button or task bar any longer. I don't see a reason why KDE on Windows could not replace explorer.exe as a shell too (apart from it being a lot of implementation work I guess). You can even replace part of the windows login mechanism (gina.dll). Some companies using Windows as an embedded OS for customer terminals have done that for ages."
    author: "tharkun"
  - subject: "But it could be modular"
    date: 2009-06-21
    body: "But it could be modular enough that only the really used pars are bundled. I don't know if this is already true but I think that KDE could be separated into much smaller libs than currently - obviously that would be much work. But you couldn't seriously write a smaller cross-platform application in KDE if it makes your users have to download tens of MBs."
    author: "gd"
---
After Christian Ehrlicher announced that he would step down from packaging and bug fixing for KDE on Windows, some articles were written which suggest that KDE on Windows is on hold now that the main developer has moved on. Even though KDE on Windows is only a small project and the loss of one developer will be felt, we are far from dead. The Windows port has not been a one-man-project and many other people are still involved. KDE on Windows will continue to be developed and packages will continue to be made.

The rumour of KDE on Windows stopping is not true and just harms our project. This clearly was not the intent of the blog post and we hope those reports will be corrected. At this point we also want to thank Christian for all the hard work he has done, for fixing bugs, making packages and of course also for making the platform accepted by KDE developers.

We hereby want to invite all interested developers and users, to come and try out KDE on Windows, help find and fix bugs and make KDE on Windows even better.
