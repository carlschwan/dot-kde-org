---
title: "Alex Spehr Interviewed About KDE and BugSquad "
date:    2009-10-07
authors:
  - "jriddell"
slug:    alex-spehr-interviewed-about-kde-and-bugsquad
comments:
  - subject: "Nice, but short"
    date: 2009-10-13
    body: "Nice interview and, but it would have been nice if it had been a little longer. Still, it's good to see Linux Pro pick this up and give the bug squad a little mention."
    author: "swjarvis"
---
Linux  Pro Magazine  has <a href="http://www.linuxpromagazine.com/Online/Blogs/ROSE-Blog-Rikki-s-Open-Source-Exchange/ROSE-Blog-Interviews-KDE-Project-s-A.-L.-Spehr?blogbox">interviewed Alex Spehr</a> about her work  on BugSquad and promoting KDE.  The interview reveals what she's doing to help  North America catch up with KDE promotion and what the most scary thing is about  working with free  software.
