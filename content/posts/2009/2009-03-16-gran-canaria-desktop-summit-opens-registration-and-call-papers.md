---
title: "Gran Canaria Desktop Summit Opens Registration and Call for Papers"
date:    2009-03-16
authors:
  - "jriddell"
slug:    gran-canaria-desktop-summit-opens-registration-and-call-papers
comments:
  - subject: "Please use this opportunity to fix notifications in FLOSS world "
    date: 2009-03-16
    body: "Hi,\r\n\r\nIt would by great if this Desktop Summit could \tcome up with ideas how to improve notification experience both in GNOME and KDE. \r\n\r\nI would love to see one improved specification of notification system which will work in Gnome and KDE. Now KDE4.2 has own implementation, GNOME has it's own, UbuntuIntrepid has it's own and it is not good for other apps. \r\n\r\nI'm involved in <a href=\"http://www.qt-apps.org/content/show.php/Kadu+Instant+Messenger?content=59508\">Kadu Instant Messenger</a> development - for IM applications notifications are crucial :> but for now there is no easy way to have good - native - notifications in both GNOME and KDE. We would love to provide good user experience for our users but for now we use our own notification system because there is no better alternative. \r\n\r\nThis is a pity and it would by great if you guys could do something with this situation :) \r\n\r\nkeep up the good work, greetings\r\n"
    author: "Piotr Pe\u0142zowski"
  - subject: "and kwallet"
    date: 2009-03-17
    body: "The top of my list of \"things that should be cross-desktop\" is encrypted password saving.\r\n\r\nThere's quite a few things like this, I suspect they will be worked on."
    author: "eean"
  - subject: "The problem with notification specs..."
    date: 2009-03-18
    body: "...is that there seems to be two very strong and diametrically opposed camps when it comes to how notifications should work. One believes that rich notifications are really great (interactive ones, such as the ones in KDE 4), while the other believes that the user should never ever interact with notifications (what Ubuntu's doing, basically). The two are so opposite and incompatible that they can't be unified, but both have valid points to be made.\r\n\r\nPersonally i'm against limiting abilities for the developer (KDE4's can be just as non-interactive as Ubuntu's if you want to), but that's just my opinion - the other camp has their points as well (i just happen to disagree with them :) )."
    author: "leinir"
  - subject: "It is not a problem..."
    date: 2009-03-18
    body: "...because specs may by designed in this way that desktop notification implementation (GNOME's , KDE's, etc)  will choose which interaction is allowed/shown and which is not. \r\n\r\nSo as a result Desktop (KDE,GNOME) will stipulate how notifications are working and overall \"feel\" of notification but applications developers will have possibility to implement one-working-everywhere-native notification. "
    author: "Piotr Pe\u0142zowski"
  - subject: "There are scenarios where"
    date: 2009-03-18
    body: "There are scenarios where that won't work. If GNOME (or any other DE) choose to only implement non-interactive parts of the spec then apps that rely on the entire spec running in those DE will either fail or have to start having multiple code paths based on the % of the spec they expect to have available to them, which kind of defeats the purpose."
    author: "borker"
  - subject: "not a difficult thing"
    date: 2009-03-19
    body: "we are already working on this, GCDS is not needed for things that trivial.\r\n\r\nthe first steps in kde were accomplished with 4.2 and we're now working on harmonizing the d-bus interfaces. the one we provide right now is based on the galago spec but with some minor modifications due to technical considerations. we are all talking with each other and should have something unified in the Not So Distant Future.\r\n\r\nnote, however, that this is only for visual notifications. gtk/gnome apps have no real concepts in their framework for anything beyond that, while kde apps have KNotify which provides log files, full featured audio notifications and more all in a centralized system.\r\n\r\nbut it's visual notifications that is the interesting point for harmonization right now between the projects and it's what we're working on.\r\n\r\nas for using your own notification system, that's a bit unfortunate. note that even once we harmonize the notification systems, you won't get native notifications on mac/win platforms. apps that use knotify should, however."
    author: "Aaron Seigo"
  - subject: "Geting the native"
    date: 2009-05-28
    body: "Geting the native natifications on Windows is probably more a priority than on <a href=\"http://freeappleimac.isgreat.org/\">iMacs</a> but its go to know your priorites are in the right areas."
    author: "Marlet"
---
<img src="http://static.kdenews.org/jr/gran-canaria-desktop-summit.png" width="290" height="131" alt="" align="right" />

This year the annual KDE community summit, Akademy, is being held in Las Palmas, Gran Canaria, Spain, from 3rd to 11th of July. It will be part of a larger event, the Gran Canaria Desktop Summit co-located with the GNOME community's annual summit, GUADEC.

Registration for the event is now open. Attendees can register for the event at the <a href="http://www.grancanariadesktopsummit.org/">Gran Canaria Desktop Summit website</a>.

Early registration is advised as this year's event is expected to be even more popular than usual.

In addition to launching registration, the <a href="http://www.grancanariadesktopsummit.org/node/74">Akademy Call for Participation</a> is now available. The timescale for submissions is short this year, so potential speakers should follow the submission instructions precisely to speed up the review and increase their chance of success.
<!--break-->
<img src="http://static.kdenews.org/jr/akademy-logo.png" width="131" height="75" alt="" align="left" />

The program committees of the Gran Canaria Desktop Summit are excited about the opportunity to have a conference with an unprecedented breadth of presentations about the Free Software Desktop, and are looking forward to your registrations and submissions.
<br clear="all" />
<h2>Akademy Call for Participation</h2>

The 2009 Gran Canaria Desktop Summit is the first event of its kind, co-locating the KDE community conference Akademy, and the GNOME Users and Developers' Conference (GUADEC) from July 3rd to July 11th 2009 in Las Palmas, Gran Canaria, Spain. This year's conference marks the first time that Akademy and GUADEC will be co-located.

Akademy 2009 also represents a community landmark, it will be the first major KDE community conference since the release of KDE 4.2, the first KDE 4 release intended for universal adoption. As such, having released a stable and mature platform, the KDE community is now in a strong position to continue innovating.

The aim of this year's Akademy is to help KDE community refocus on the key aspects of KDE 4 development. In particular this year we are asking for presentations with a particular focus on:

<ul>
<li>Beauty: One of the fundamental inherent attributes of a desktop environment is its aesthetics, in appearance, interaction design, and technology. We are seeking presentations about beauty in user Interface design, particularly organic concepts realised in applications or in Plasma applets, showcases of desktop artwork and sounds, and other examples of appealing interfaces.</li>

<li>Portability: Today "portability" is just as much about hardware as it is operating systems. Modern software may need to be run on anything from a mobile phone to a server. We are seeking presentations on KDE and operating systems, distributions, mobile KDE and KDE on netbooks.</li>

<li>Functionality: The aesthetics and portability of KDE are nothing without underlying functionality. Therefore, we are also seeking presentations on KDE applications, frameworks and services.

<li>We also welcome presentations which do not fit into one of the key areas, but illustrate interesting developments in technology or community, or give insight into innovative ideas which have the potential to shape the future.</li>
</ul>

<h3>Submission Details</h3>

The Akademy program committee invites presentation submissions from all KDE contributors: translators, artists, coders, writers, researchers and organisers. We also welcome presentations by people from projects around KDE, from industry or research partners, or from KDE-related efforts about Free Software, Free Culture and Free Content.

We are asking for four types of submissions:

<ul>
<li> 30 minute presentations about a KDE topic, optionally with an accompanying written technical paper</li>
<li> 30 minute presentations about a cross-desktop topic</li>
<li> 5 minute lightning talks</li>
</ul>

New this year, in addition to 30 minute presentations on a KDE topic, presenters will have the option of submitting a technical paper of 4 to 6 pages to be included in a digital proceedings. Evaluation and acceptance of the technical paper will be independent to the evaluation and acceptance of the related presentation.

In addition to these 30 minute presentations, the GCDS will also be hosting shared lightning talks, 5 minute presentations (with or without slides) in a contiguous time block, and we will be accepting proposals for BoFs and workgroups which will be held from Tuesday July 7th until Saturday July 11th.

Please submit abstracts of under 500 words for 30 minute presentations to akademy-talks@kde,org. Include your name, title of your presentation, a short abstract of your presentation, intent to submit a technical paper, a short bio, and a photo suitable for the web.

We also welcome informal proposals for 5 minute lightning talks. Please send your name, topic and a short description to akademy-talks@kde.org, if you are willing to hold a lightning talk in the shared program or at Akademy. All topics around the Free Software Desktop are welcome.

The deadline for submitting presentations, BoFs and lightning talks is Friday April 10th, 2009.  Speakers will be notified about acceptance or rejection of the proposal on April 24th. The full-text technical paper should be submitted by June 8th.

The program committee for Akademy 2009 is: 

<ul>
<li>Paul Adams </li>
<li>Pradeepto Bhattacharya </li>
<li>Bertjan Broeksema </li>
<li>Marijn Kruisselbrink </li>
<li>Celeste Lyn Paul</li>
<li>Cornelius Schumacher </li>
</ul>

If you have questions or comments about the call for participation, the program of the conference or anything else related to the conference, you can reach the program committee at akad<span>emy-ta</span>lks@kde.org.