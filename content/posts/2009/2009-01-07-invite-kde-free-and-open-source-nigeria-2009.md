---
title: "Invite to KDE for Free and Open Source Nigeria 2009"
date:    2009-01-07
authors:
  - "mabubakar"
slug:    invite-kde-free-and-open-source-nigeria-2009
comments:
  - subject: "I'm going"
    date: 2009-01-07
    body: "I'll be going to the conference in Kano / Sokoto. I'm really looking forward to it; the university I work at has good working relationships in Uganda and Ghana and I'm going to try to build more bridges.\n\nMustapha was an attendee at Akademy this summer, showing real courage in sitting through a cold and drizzly boat trip and speaking passionately on the topic of Free Software in Africa."
    author: "Adriaan de Groot"
  - subject: "Re: I'm going"
    date: 2009-01-09
    body: "Thanks ade :). i hope to see you in kano at the midts of KANO PEOPLE UNDER THE SUN SHAADE :)."
    author: "Mustapha Abubakar"
  - subject: "Gran Canaria Desktop Summit promotion"
    date: 2009-01-07
    body: "Since the Gran Canaria Desktop Summit event is in the Canary Islands this year, I think it will be much easier for Nigerian people to come than ever. I recommend them to fly to Madrid, Dakar, Nouakchott, El Ai\u00fan, Funchal or any big city from Marocco and then to Gran Canaria.\n\nhttp://www.grancanariadesktopsummit.org\n\nIf any representative from that country (or any other from the area) wants to come, please get in contact wih me through KDE board."
    author: "Toscalix"
  - subject: "Re: Gran Canaria Desktop Summit promotion"
    date: 2009-01-09
    body: "Hi Toscalix, Thanks for the advice on the direction we suppose to follow :).We that are coming from NIGERIA to attend the gran canaria event (GUADEMY),we have organised our self and i am given the leadership of the journey since i have experiance in traveling to conferences. I will summit the names of those that are ready for the event to KDE board on monday so that the board can submit it to you.\n\nWe will like to recieve letters of invitation so that we can start preparing for visa.\n\nThanks.\n\n\n"
    author: "Mustapha Abubakar"
  - subject: "Good Luck!"
    date: 2009-01-07
    body: "Good luck with your conference!"
    author: "David Johnson"
  - subject: "Re: Good Luck!"
    date: 2009-01-09
    body: "Thanks David:)"
    author: "Mustapha Abubakar"
  - subject: "Great!"
    date: 2009-01-08
    body: "It was a funny coincidence: I was listening to Denis Richard's album called \"Africa\" when I read this post :) Now I feel like going there ;)\n\nIn any case, it's great to hear about such event happening in Nigeria. I remember the rise of free/open source movement in Ukraine several years ago. Those were great times of enthusiasm and excitement. I'm sure Nigerians will like this too.\n"
    author: "Alexander Dymo"
  - subject: "Re: Great!"
    date: 2009-01-09
    body: "Upss!,and is not gonna be a coincidence when you will come to give us your contribution:). Hoping to see you in Nigeria. \n\nTnx"
    author: "Mustapha Abubakar"
  - subject: "Nollywood"
    date: 2009-01-09
    body: "Oh, I would like to see the KDE in a Nollywood thriller with drastic digital effects and witchcraft.\n\nhttp://fr.youtube.com/watch?v=FnSlp0ffrp0\nhttp://fr.youtube.com/watch?v=BtQn2aPJzKs\n\nhttp://www.computerworld.com/action/article.do?command=viewArticleBasic&taxonomyName=windows_and_linux_pcs&articleId=9045405&tax"
    author: "Andre"
  - subject: "Awesome news .. just what i needed"
    date: 2009-01-13
    body: "Wow am the system admin for the nelson mandela institude's university (African University of science and technology  www.aust.edu.ng ) located in abuja and i would be glad to come.. we use linux on 99% of the computers  here. and it would be cool to come share how we made it all gel. i have posted this link the mailing list of my LUG so i dont think i will be alone.. also wont mind donating please you can reach me on my email."
    author: "bigbrovar"
  - subject: "Interesting"
    date: 2009-01-22
    body: "This is a very interesting development. I will like to participate and present a paper. "
    author: "Agharinma Ehiedu RHCE (cert #: 805008887235760)"
  - subject: "Re:"
    date: 2009-05-18
    body: "From our research so far, 90% of the participants are ignorant of free and open source ideas,while the remaining have heard about the idea but do not know its values and importance. <a href=\"http://www.infinitylogodesign.com/Portfolio/industry/27/Education.htm\">Education Logo Design</a> | <a href=\"http://www.infinitylogodesign.com/\">Logo Design</a> | <a href=\"http://www.infinitylogodesign.com/Portfolio/industry/23/Communications.htm\">Communications Logo Design</a>\r\n\r\n\r\n"
    author: "PeterWarner"
---
Having met so many of you at Akademy last year, I am now glad to notify you guys that we have approval from <a href="http://www.buk.edu.ng/">Bayero University Kano Nigeria</a> to host the Free and Open Source Nigeria 2009 conference on 6th to 9th March.  We want to invite KDE contributors and users including organisations and companies who want to come and give talks or workshops during the event.  We are expecting more than 2000 participants from within and outwith the university.   We will be glad to receive guests from all over the world, especially people with vast experiences in open source.



<!--break-->
<p>70% of the attendees are going to be students from higher institutions from the northern part of Nigeria,while 2% are government representatives, 10% academicians and 17% are people that will come from outside the academic environment. Everyone will want to know what is free and open source software.</p>

<p>Due to our few resources ,the committee can only take care of our guest's food, accommodation and site visits of all those that will come from outside the country although we are going to look for a donations if we can get any.</p>

<p>We would like to release the programme of events by 19th january 2009.Therefore we will like to know those who would like to speak and give workshops so that we will speed up our arrangements in time.</p>

<p>From our research so far, 90% of the participants are ignorant of free and open source ideas,while the remaining have heard about the idea but do not know its values and importance. This is going to be the first free and open source conference in northern Nigeria, a country with more than 100 million people.</p>

<p>All interested companies who wants to advertise their open source products and services can send their banners, pamphlets, stickers, shirts or anything that can sell the idea to our community free of charge so that it can be distributed or place in all the programme halls.</p>

<p>If you are interested in coming to give a talk, workshop or just meet Nigerians interested in free software please contact me, Mustapha, at must<span>apha@hut</span>softnigeria.com.</p>

