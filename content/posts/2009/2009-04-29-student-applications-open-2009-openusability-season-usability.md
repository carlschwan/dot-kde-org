---
title: "Student Applications Open for 2009 OpenUsability Season of Usability"
date:    2009-04-29
authors:
  - "seele"
slug:    student-applications-open-2009-openusability-season-usability
---
Are you a student of design, usability, human factors, or other HCI-related field? Are you interested in working on an open source project? For 10-15 hours a week between June 1 and August 31, you could work with an experienced usability mentor on a fun and interesting design project! An added bonus is the $1000 USD internship stipend at the end of the project.

The <a href="http://www.openusability.org">OpenUsability</a> <a href="http://season.openusability.org"> Season of Usability</a> is a series of sponsored student projects to encourage students of usability, user-interface design, and interaction design to get involved with Free/Libre/Open-Source Software (FLOSS) projects. Inspired by Google’s <a href="http://code.google.com/soc/">Summer of Code™</a>, OpenUsability offers sponsored student projects. Other than the Google projects that address developers, they aim at students of usability, user-interface design, and interaction design. Students experience the interdisciplinary and collaborative development of user interface solutions in international software projects while getting into OSS development.
<!--break-->
The OpenUsability Season of Usability will be supporting 10 students to work on 10 open source projects, 2 of which are KDE-related, during the June 1 - August 31 2009 season:

<ul>
    <li>Amarok</li>
    <li>Drupal</li>
    <li>Gallery</li>
    <li>GeneMANIA</li>
    <li>GNOME</li>
    <li>Kadu</li>
    <li>KOrganizer</li>
    <li>OLM</li>
    <li>SemNotes</li>
    <li>Ubuntu</li>
</ul>

The student application period is open until May 20 2009. The Season of Usability runs from June 1 to August 31 2009.

Applications are due May 20 2009. Questions about the projects or application process may be directed to <a href="mailto:students AT openusability.org">students AT openusability.org</a>. See <a href="http://season.openusability.org/index.php/projects"> http://season.openusability.org/index.php/projects</a> for more information on the projects, student requirements, and how to apply. 

The 2009 OpenUsability Season of Usability is sponsored by <a href="http://www.google.com">Google</a> and <a href="http://www.canonical.com">Canonical</a>.