---
title: "Plasma Team Looks at the Future"
date:    2009-02-11
authors:
  - "sebas"
slug:    plasma-team-looks-future
comments:
  - subject: "Sounds like you guys had a"
    date: 2009-02-11
    body: "Sounds like you guys had a useful, productive and interesting conference. Thanks for the report. Get well soon, Sebas!"
    author: "jospoortvliet"
  - subject: "lionmail"
    date: 2009-02-11
    body: "how about just making as plasmoid that displays data from nepomuk ??\r\n\r\ni think you could already ue the folderview to filter by nepomuk:/Mails from Today , or nepomuk:/Mails that are important\r\n\r\nand so on... the nepomuk query language is much more flexible..why ont extend code a better folderview , but again and again and again and again and ... create a new app...\r\n\r\n:(#\r\n\r\n2 cent "
    author: "wintersommer"
  - subject: "Akonadi integration"
    date: 2009-02-11
    body: "This is great work. There is a killer feature I would really like to see implemented though. That is that the clock plasmoid (any of them, really) could display events data and provide alerts/alarms. Pretty much as the battery plasmoid integrates with powerdevil, the system tray integrates with the system notifications or krunner integrates with nepomuk the clock plasmoid should integrate with akonadi. \r\n\r\nMy idea is to have extended information about important events for the day on the extender, just below the calendar when you click on the clock and have the calendar highlight busy days. The clock itself could also give some hints on if there are important/special events that day.\r\n\r\n"
    author: "lacsilva-2"
  - subject: "Re: Akonadi integration"
    date: 2009-02-11
    body: "Yep, definitely.\r\n\r\nThis kind of stuff is under way as well. As the information becomes available via Akonadi, it also becomes very easy to integrate it into the desktop.\r\n\r\nHow exactly it will be presented is not quite clear yet, but the ideas you have are certainly within the range of what we're thinking about.\r\n\r\nAs a matter of fact, Davide has been working on getting information about holidays integrated into the calendar of the clock, which is one of the babysteps towards really good integration of PIM data into the desktop."
    author: "sebas"
  - subject: "A file manager-like interface doesn't cut it"
    date: 2009-02-11
    body: "Sure, you can do that quite easily with the folderview plasmoid.\r\n\r\nIt's not so much about how you retrieve the data, it's about how you <em>present</em> it, and how you let the user <em>interact</em> with it. A file-like interface is probably pretty bad in these two respects:\r\n<ul>\r\n   <li>You don't want to require users to learn a query language in order to et something as basic as new email notification </li>\r\n   <li>While Emails might be stored as files, they actually don't have so much in common. Emails have context, that is specific to them, they're usually part of one or more collection, or a folder, they're sent by someone, sent to someone else, there's content vs. metainformation, a subject, body, attachments, and so on.</li>\r\n</ul>\r\nAlso notifications do not really fit into this in a meaningful way.\r\n<p />\r\nA file manager-like interface would be very limiting (at least when I compare it with the things I have in mind for Lion Mail).\r\n<p />\r\nBesides that, Nepomuk does metadata, Akonadi has the actual data. Akonadi already has some integration of Nepomuk, and there will be more.\r\n<p />\r\nNobody keeps you from entering a Nepomuk query into the folderview and using that as your email client, or writing a Plasmoid that does this. In fact, it's also becoming very easy since we're offering the information needed in a dataengine, that you can easily use from your scripted Plasmoid."
    author: "sebas"
  - subject: "getting better ..."
    date: 2009-02-11
    body: "Thanks. My legs still looks like something unhealthy, but at least they stopped itching like crazy.\r\n\r\nFortunately, it didn't really keep me from having a productive sprint.\r\n\r\nNot impressed by the staff of that particular hotel, they didn't seem to be sorry, but did dry-clean my clothes on request (after I put a plastic cup containing some of the bugs on the reception table).\r\n\r\nMight have had something to do with other guests waiting in line ;-)\r\n\r\nNo apologies or anything proactive from their side though. :-("
    author: "sebas"
  - subject: "Like Evolution with Gnome"
    date: 2009-02-11
    body: "Like Evolution with Gnome Clock :)"
    author: "SeaJey"
  - subject: "broken links"
    date: 2009-02-11
    body: "Most links in the article point to http://dot.kde.org/2009/02/11/TODO."
    author: "patcito"
  - subject: "Distribution? "
    date: 2009-02-11
    body: "What are you guys using for a distribution? <br><br>\r\n\r\nI have yet to find a stable distribution for KDE yet? Ubuntu, I think most people know how that is, and in alot of other distros, this works, but this doesn't, and vis-versa for the other. *Ends run on sentence* \r\n<br><br> \r\nOne of my biggest concerns is my redrawing is slow, and I can see it when I open up windows, and let's not get started on effects, on my 9500 Nvidia, which I heard was supported. "
    author: "Dekkon"
  - subject: "Re: Distribution?"
    date: 2009-02-11
    body: "I'm not one of the plasma devs, but I think there isn't *one* distribution people use.\r\n\r\nI'm a big fan of openSUSE, which is quite polished and easy to setup.  \r\n\r\nThe current version offers a well-tested KDE 4.1, but you can install KDE 4.2 as add-on by adding an extra repository. For optimal experience, remove your ~/.kde4/share/config/plasma-appletsrc file after upgrading to KDE 4.2, so you get the new nicer defaults of 4.2.\r\n\r\nIf you still have slow graphics with NVidia, make sure you have the latest stable drivers installed (version 180.22). openSUSE provides this in a NVidia repository, for some distributions you need to run the NVidia installer yourself."
    author: "vdboor"
  - subject: "My question was not rather"
    date: 2009-02-12
    body: "My question was not rather objective, but rather to revise it, I would put it, what is the Distribution that works best with KDE? <br><br>\r\n\r\nEither way, I just installed OpenSuse 11.1 and without even installing the video drivers, I am completely blown away at the performance improvements, and haven't had one plasma crash. This is certainly the KDE I was expecting and was not given with Kubuntu(Not to knock on the Kubuntu devs tho :) )."
    author: "Dekkon"
  - subject: "Join the openSUSE KDE community"
    date: 2009-02-12
    body: "<p>To expand this high quality presentation of KDE, we need YOUR contributions.  There are more and more cool KDE 4 apps outside KDE SVN that can be packaged, and Plasma applets and Akonadi tools in playground that people want to see.  The openSUSE Build Service makes it possible and almost easy for anyone to package and distribute these.</p>\r\n\r\n<p>\r\nAs well as packaging, we need input and vision on what to work on next in openSUSE 11.2.  Come to the next #opensuse-kde IRC meeting at 1700UTC on Wednesday 18 February!</p>"
    author: "Bille"
  - subject: "fixed, sorry ..."
    date: 2009-02-12
    body: "We wrote the article at the airport without Internet available, so couldn't look up the link. When I posted it to the dot, I simply forgot to check for TODOs.\r\n\r\nThe links are in now, sorry for any inconvenience..."
    author: "sebas"
  - subject: "non-issue"
    date: 2009-02-12
    body: "In reality, it's a non-issue. There was hardly any talk about which distributions we use.\r\n\r\nIt may not be surprising, but everybody has different views on what an Operating System should do, some want to compile their own kernel, some want rolling releases, others simply don't want to upgrade, others are running unstable distros. I think that's actually really good since it catches problems in platform and distribution independance we might not be aware of otherwise.\r\n\r\nPersonally, I think all distros have their flaws, and I wouldn't recommend any of them, simply because I think it shouldn't matter.\r\n\r\nI don't like complaining about certain distros either since it'll get picked up in the wrong way, and often friends are working on them, and often small issues you state in public are then instrumentalised to bitch about distros.\r\n\r\nFor my, anything that works without much hassle is fine, and tend to choose things I know above new things, simply because I'm too lazy to learn new tools."
    author: "sebas"
  - subject: "Exactly, but more generic..."
    date: 2009-02-12
    body: "Exactly, but more generic..."
    author: "jospoortvliet"
  - subject: "great work"
    date: 2009-02-12
    body: "keep up the great work with plasma.\r\n\r\ni'd love to see the picture frame applet have an option in the slideshow to restrict only displaying either portrait or landscape oriented images (or both). that way i can get more control over how my desktop will look as it cycles through images in a directory. \r\n\r\ni checked out the plasma source and documentation to try to add this myself, but i can't find where the ui for the settings is created, or where about the next picture to display is selected. there doesn't seem to be much going on in frame.cpp. i'm quite puzzled..."
    author: "sanjo"
  - subject: "There's usually a .ui File in"
    date: 2009-02-12
    body: "There's usually a .ui File in the directory of the applet, you can add UI options in there. In the applet's method \"createConfigurationInterface()\" ususally), you can preset values in the form, in configAccepted() you can read the values from the submitted form (only when OK'ed or Applied), set values in the applet and write it to the config if you want.\r\n<p />\r\nIf you've more questions, you can find help on the plasma-devel kde org list, there are always helpful people around."
    author: "sebas"
  - subject: "found it"
    date: 2009-02-12
    body: "thanks, i finally found the repo for the frame applet. i got a bit confused when i was reading the documentation. i initially checked out the kdelibs and kdebase repos mentioned at:\r\n\r\nhttp://techbase.kde.org/Getting_Started/Sources/Anonymous_SVN\r\n\r\nmaybe you should add something to the plasma faq with the url of the svn repo? i haven't developed for kde before..."
    author: "sanjo"
  - subject: "Plasma FAQ is for end users"
    date: 2009-02-12
    body: "The Plasma FAQ is actually meant for end-users more than developers. I originally wrote it on Techbase as a \"temporary\" place, and once UserBase was up, I moved it there. So I think that such an information would be out of place there. \r\n\r\n<p>But a FAQ for developing would be good, indeed...</p>"
    author: "einar"
  - subject: "When it works"
    date: 2009-02-12
    body: "When the Evolution integration works that is......... However, that still doesn't work on a generic desktop level. It's still an application specific piece of integration."
    author: "segedunum"
  - subject: "Why not windows and widgets?"
    date: 2009-02-13
    body: "Here's my question.  Why not make Plasma able to contain windows or widgets from applications?  Why can't I take a playlist pane in Amarok and drag it to the desktop where it would adopt the Plasma theme and remain there?  It used to be people made windows sticky across desktops for something like this, but Plasma offers a chance to do more."
    author: "Abstractform"
  - subject: "i didn't see the one on"
    date: 2009-02-13
    body: "i didn't see the one on userbase, only on techbase at:\r\n\r\nhttp://techbase.kde.org/Projects/Plasma/FAQ"
    author: "sanjo"
  - subject: "Plasma can already embed any"
    date: 2009-02-13
    body: "Plasma can already embed any type of window, and Qt 4.5 supports arbitrary widgets in plasma. But being able to drag a random widget from a random running application, I dunno... Not sure how useful it is, and it might be pretty hard to get working from a technical POV too."
    author: "jospoortvliet"
  - subject: "I fail to see why KDE needs"
    date: 2009-02-14
    body: "I fail to see why KDE needs two window systems running in parallel.\r\n\r\nFor me, Plasma is for some small applets on my desktop, like a clock and some icons.\r\n\r\nPrograms are there to do specific tasks that require a real window that is large enough. Please don't use plasma for creating big applications.\r\nIf you want to edit a playlist, use Amarok or another normal program. Don't stuff it in an applet. Use the applet to display information about the current track being played."
    author: "tbscope"
  - subject: "useful for restricting the desktop"
    date: 2009-02-14
    body: "Funny, I have had exactly the same idea as him: being able to drag the content of a window on the desktop. I was thinking about that while my son was on my knees, wanting to play with the computer:\r\n<P>\r\nsuppose you want to temporaly restrict the use of the computer to one program only (like a little game, for your 5 years old child): open the game, drag the content window on the desktop, lock the desktop on dashboard and voila.\r\n"
    author: "zeroheure"
  - subject: "Shouldn't Plasma team be looking at the present? "
    date: 2009-02-15
    body: "Am I the only one realizing all the current flaws within Plasma, I just don't feel it has matured enough to be adding and implementing new features. \r\n<br><br>\r\nI made a video just to show the flaws. \r\n<br>\r\nhttp://www.youtube.com/watch?v=IzK1Lbt3tHw\r\n*Prob still processing*<br>\r\n<br>\r\n*First video shows more bugs, but this one explains some aswell. \r\nhttp://www.youtube.com/watch?v=dOdQxnPRxM8#\r\n<br><br>\r\nI love KDE, and I would really love to see it shine but I can't work in these conditions. Thanks Developers. \r\n\r\n"
    author: "Dekkon"
  - subject: "Just as specific"
    date: 2009-02-15
    body: "Akonadi integration, however cool it is, is of course just as specific. Sure, in principle, more applications could be build on top of it, but in practice there is only one calendar-application that does this. The one solution is developped by Gnome/Evolution engineers, the other by KDE-PIM engineers. Other then that KDE is of course kooler ;-), what is the difference? Neither are based on a standardized open protocol or API specification, I think?"
    author: "andresomers"
  - subject: "what are we looking at?"
    date: 2009-02-15
    body: "Would it be possible to tell us what it is we are looking at? The quality is quite bad (as with most youtube stuff), so it is hard to see what you are doing, and what it going wrong. So: what bugs are you demonstrating here?"
    author: "andresomers"
  - subject: "Akonadi tried to be a freedesktop.org project"
    date: 2009-02-15
    body: "Akonadi applied to be a fd.o project, but in the end it didn't really work out."
    author: "einar"
  - subject: "Sorry, did notice that. "
    date: 2009-02-16
    body: "Yes, the quality is quite bad, but I figured most people didn't want to download but I'll upload the video file here. \r\n\r\n<br><br>\r\nhttp://www.2shared.com/file/4886623/895dc2ce/out.html\r\n<br>\r\nPlasma causing fatal error when deleting plasmoids and panels. 13 Secs in video. <br> <br>Plasmoids not showing icons in panel, you'll see plasmoids in the panel but they don't show icons(shown more in second vid) but I can move them to the desktop, but when I do, it restarts plasma. <br><br>\r\n\r\nAlso, most of the screen will go completely black when resizing panel, or just changing themes say more then 5 times(I was trying themes out). 1Min and 22Sec is where this occurs. \r\n<br>\r\nJust to clarify, I love open source software and KDE, I am in no way trying to offend the developers, I am just posting this to help you guys with the current bugs present. <br><br>\r\n\r\nThanks. \r\n\r\n<br><br>\r\nsecond Vid. \r\nhttp://www.2shared.com/file/4886652/b11b649f/out.html"
    author: "Dekkon"
  - subject: "I think my idea is being"
    date: 2009-02-16
    body: "I think my idea is being misunderstood.  Of course you don't want redundant applications, and that's actually what I'm aiming at.  I'm suggesting that right now there is an artificial conceptual divide between say, Dolphin and Plasma folderviews.  Why not just drag a Dolphin pane to the desktop where it would turn into something like a folder view?  The desktop, the Plasma panels, and windows applications should not live in such separate worlds, but be interchangeable, not just in terms of libraries but interface.  And as said before, it'd be the same as making the windows sticky and underneath other windows in the scheme of older window managers.\r\n\r\n"
    author: "Abstractform"
  - subject: "sebas"
    date: 2009-02-16
    body: "Did you consider checking bugs.kde.org before going through all the hoops of creating screencasts for them? Pretty much all necessary information to be actually able to \"look at the present\" is missing, the quality of your screencasts is just too bad, it doesn't contain any information that would be helpful in debugging, or even reproducing the bug.\r\n<p />\r\nMost developers don't follow the Dot for bugreports, chances these problems are being missed is much higher when using the Dot for bugreports instead of BKO.\r\n<p />\r\n\r\nFurthermore, I see two bugs in you screencasts, one being a crash on removal (those are usually easy to fix, chances are high we already caught it). You're seriously proposing we should not concentrate on the future? How about you actually help us with those two (BKO!)? You're seriously telling me that you cannot work in these conditions? Both look like bugs you don't run into every day. (or how often do you remove all your widgets and panels?) To be honest, I think you're exaggerating quite a bit. If you're just out to make a case here, I think you failed.\r\n\r\n<p />\r\n\r\nIt's fine to complain, but please do it in a way we can actually deal with. "
    author: "sebas"
  - subject: "Hope this helps. "
    date: 2009-02-16
    body: "I'm sorry for someone who is just a user, and not a developer, I really don't know how to explain it more. \r\n<br><br>\r\nThe video was in a pretty high resolution, so I fail to see how the screencast was to fault.<br><br>\r\nThe main bugs were, when changing themes, moving plasmoids, resizing the panel, most the screen goes blank and rendered useless. I have seen this problem before on the dot, so I was sure it was in bug.kde.org. \r\n<br><br>\r\nThe second one was obviously, when deleting plasmoids, or moving plasmoids from panel, plasma causes fatal error. I was also sure this was in bugs.kde.org. \r\n<br><br>\r\nThe third bug which probably wasn't shown in the video was when using a certain theme, I think is was slim glow, when I moved plasmoids all the way to the right of the screen, it would move it self back in the middle. I think this happened when I changed resolutions cause I have two monitors I tend to hook up at different times. Could be wrong though. \r\n<br><br>\r\n\r\nProbably came off wrong with look at present not future, just a headline that failed. Either way, I am in no way saying to stop looking at the future. Everything they said in this post I can't wait to see in KDE 4.3 or later versions, I just want bug free and I will do my best to report bugs but I'm not skilled in this kind of stuff. "
    author: "Dekkon"
  - subject: "standardized open protocols"
    date: 2009-02-18
    body: "Akonadi uses open protocols like imap for it's communication. using imap in this case makes sense in a number of ways: it was designed for the usage of email type apps, it's well known and provides a way to access akonadi easily even from \"non-akonadi\" PIM apps as virtually everyone implements imap these days.\r\n<p>\r\nadditionally, one can add different storage sources to akonadi, making it a framework that can grow to encapsulate what exists out there.\r\n<p>\r\nit's a very, very different picture compared to Evolution design."
    author: "Aaron Seigo"
  - subject: "two bugs"
    date: 2009-02-18
    body: "So I see two bugs in that screencast:\r\n<p>\r\n* a crash when removing a panel after removing all items in it; a backtrace would be good to have because I can't reproduce it.\r\n<p>\r\n* the \"desktop view goes awry when resizing the panel quickly\" is something we're aware of, that is relatively recent and which i hope to have a fix for shortly.\r\n<p>\r\nto respond to original question of \"should we be looking at the present instead of the future\" i'll answer in two part.\r\n<p>\r\nfirst, let's keep some reality here, shall we? for 4.2.0 we worked from 450 open defect reports before the first beta down to well under 200 by the time of the release. those remaining reports were generally of smaller impact or else were related to multi-screen setups which either we don't support right now (dual head) or which we are hoping to have fixed in 4.2 with kephal but which need more testing before they can be closed. additionally, we spent a huge amount of dealing with the \"present expectations\" of users in developing 4.2. so your assertion is absurd in the first place as we've demonstrated more discipline and commitment to \"the present\" than you'll find in most projects out there.\r\n<p>\r\nsecond, i don't want plasma to become a dull, boring and then dead project. if the entire focus becomes \"let's work on the minutiae until that's all that's left!\", which is exactly what happens when the sort of viewpoint you are asking for is adopted, you end up with a project nobody wants to work on and then some poor sod gets stuck with maintaining what's left. oh hey, that's me with kicker in kde 3!\r\n<p>\r\nmoreover, there's a lot left in the existing design and implementation of plasma that is left to be fleshed out to expose some of the more advanced / new ideas we're aiming for, such as network relocatable/discoverable widgets and services. part of remaining competitive is pushing into new territory. part of keeping the code interesting to work on is pushing into new territory.\r\n<p>\r\ni'm not about to let plasma become a sterile project that nobody works on. you may have noticed the pace of development in 2008. there's a reason for that.\r\n<p>\r\nthere's an important balance between new work and improving existing work. the key word there is 'balance' and any approach that doesn't include such a thing is destined to fail.\r\n<p>\r\nobviously, given the time you invested in making that youtube video, you want to help us move forward, here's how you can do that most effectively given that you said you're \"just\" a user: take your bugs that aren't already reported on bugs.kde.org (the second one is already, don't know about the first without looking at a backtrace) and post them on bugs.kde.org so we can work on them.\r\n<p>\r\nnot only is that really useful for us, it's a lot easier than making youtube videos or trying to deduce what sort of development philosophy we should adopt. :)"
    author: "Aaron Seigo"
  - subject: "windows and widgets"
    date: 2009-02-18
    body: "Embedding entire windows is a non-starter for various reasons ranging from the technical (ew, xembed ;) to basic design concepts (plasma is not a window manager, it's a canvas for small tools to come together on).\r\n<p>\r\nAdapting content from applications, however, is quite within range, however. This is accomplished by creating widgets that match up to mimetypes of data dropped on the desktop.\r\n<p>\r\nYour amarok example would be trivial to achieve in 4.2 if there was an amarok plasmoid that didn't rely on amarok internals in the existing app.\r\n<p>\r\nAs a more concrete and working example, though, if you drag a torrent from ktorrent to your desktop or panel you get a plasmoid that shows you what's up with that torrent. simple, elegant, exactly what you would expect.\r\n<p>\r\nHowever, just like with Akonadi, Nepomuk, Phonon, Solid, etc ... it means writing code that takes advantage of these things. Fortunately, writing Plasma widgets isn't very hard. :)"
    author: "Aaron Seigo"
  - subject: "fixed"
    date: 2009-02-19
    body: "\"i hope to have a fix for shortly.\"\r\n<p>\r\nthe fix went in today, both to the 4.2 branch so it will be in 4.2.1 as well as to trunk for 4.3. :)"
    author: "Aaron Seigo"
  - subject: "At the very least"
    date: 2009-02-20
    body: "<p>They could make the plasmoids themselves interchangeable.\r\n</p><p>\r\nI would love to be able to through a clock into Amarok, or the wikipedia/lyrics into the desktop.\r\n</p><p>\r\nAs for moving parts of windows, <a href=\"http://polishlinux.org/apps/window-managers/metisse-you-thought-you-knew-what-3d-was/\"> Matisse </a> was a neat tech demo, but I don't think they did anything that could help others.\r\n</p>"
    author: "ddwornik"
  - subject: "impressive"
    date: 2009-05-24
    body: "woah serious enough... and so is the picture. I can see all the \"big head\"s are cranking out every litre of nerve to redefine the chaos lolz\r\n\r\nthanks guys, keep up the good work\r\n<span style=\"color:white\">worldclass for models clothes shopping  <a href=\"http://fashion4us.com\" style=\"color:white\">japanese fashion</a> garments tops.</span> "
    author: "rupesh_shah"
---
<img src="http://dot.kde.org/sites/dot.kde.org/files/tokamak.png" align="right"/>

<p>Over the last weekend, the Plasma team held their second meeting in Porto, Portugal. The meeting was organised by Akademy Award winner Nuno Pinheiro inviting the Akademy Award-winning Plasma team to his home country. The meeting was kindly hosted by the <a href="http://www.dei.isep.ipp.pt/">Departamento de Engenharia Informatica</a> (Department of Software Engineering) of the <a href="http://www.isep.ipp.pt/">Instituto Superior de Engenharia do Porto</a>. The Plasma team (and most probably many of the KDE users) are grateful for being provided this opportunity to collaborate on the desktop.</p>
<!--break-->
<img src="http://static.kdenews.org/jr/tokamuk-2.jpg" width="500" height="333" />
<p>
The purpose of Tokamak II was to design features and concepts to be worked on over the next year in Plasma and the KDE desktop. Topics covered included: animations and other new features in Qt 4.5, Social Desktop concepts, desktop search, email and calendaring integration, a new system tray implementation, Plasma for educational desktops, mediacenters and PlasMate, a content creation application. On the zeroth night, the team went out for dinner to the historic center of Porto. The first morning began with an introduction by the Plasma singer/songwriter Aaron Seigo, who asked the Plasma team to keep innovating and not fall into maintenance-only mode.</p>


<h2>Kinetic</h2>

<p>Qt developer Alexis Menard showed what are the beginnings of a new animation framework that will become part of Qt in the 4.6 timeframe. Kinetic makes it easy to add animations to applications. It also includes a new state machine framework. Kinetic will probably be offered as an add-on on top of Qt 4.5 so that developers (such as the Plasma team) can take advantage of these new features and at the same time provide feedback to the Qt team to make Kinetic rock even more. Alexis mentions that a first Kinetic version will become available in the coming weeks, depending on how quickly the Qt team can incorporate the feedback the Plasma team gave on the current version. Alexis posted a screencast of Kinetic in action on his <a href="http://labs.trolltech.com/blogs/2009/02/08/qt-kinetic-hits-plasma/">weblog</a>.</p>


<h2>The New System Tray Specification</h2>

<p>In a group session, the Plasma team has been working on improving the system tray. Two aspects are planned to be improved. The job-tracking feature that was added in KDE 4.2 will see some fixes for some interaction problems and will provide an improved display for running jobs. The icon that is currently only shown when there are running jobs will probably receive some kind of activity information, a collated display for on-going jobs.</p>
<p>
Work on a new specification for the system tray started as well. The goal is to make the system tray more accessible, more consistent in its interaction model and more flexible with respect to displaying its contents. A new implementation of this might be seen as soon as KDE 4.3. The idea is that applications register with the systray and provide icons, titles and tooltip information to display. Those systray gizmos have a status ("passive", "active" and "needs attention"). The application ("client" of the systray) receives event from the systray, those events are for example "requesting context menu at position x,y", wheel up/down event, or "activated". Systray "clients" have categories. Currently planned are the categories "System Service", "Application Status", "Communication" and "Hardware". A package update notification for example would be in the category "System Services". When it's checking for updates, it would change its status from "passive" to "active", when new packages or updates are available it would go into "need attention" and ask the user if she wants to install the updates.</p>


<h2>OpenDesktop and OpenID</h2>

<p>Work has continued to implement support for the OpenID authentication system to the OpenDesktop service. OpenDesktop is a central part of the Social Desktop concept Frank presented at Akademy in Mechelen, Belgium last year. Support for OpenID is coming up on that platform. Frank mentions <em>"Support is complete as of 5 minutes ago, but will probably not be deployed until I was able to extensively test it. Expect it to hit OpenDesktop (and along with it kde-apps.org and kde-look.org) next week."</em> </p>
<p>
Every application on kde-apps.org now has its own knowledge-base with user-generated content. This information can be queried through a webservice. Support for integrating this information in Plasma (and the KDE desktop) is coming up. Frank is also implementing support for selling content through the OpenDesktop system, tieing in nicely with the content creation application the Plasma team has just commenced working on.</p>
<p>
These efforts represent the start of efforts to bring Social Desktop concepts into the Plasma workspace shell by both OpenDesktop and Plasma developers.</p>

<h2>PlasMate</h2>

<p>Artur Souza talked about the importance of integrating scripting capabilities into Plasma, and how a content creation application would accelerate development and adoption of Plasma and in extension KDE. PlasMate is this new content-creation tool for Plasma. The idea is to make it very easy to create and publish scripted Plasmoid. PlasMate will provide a metadata editor, a script editor, a previewing component. It will take care of packaging and publishing your newly created Plasmoids. Work has commenced on the various components, and the preliminary results are looking promising. Most of the components are partly working already, after only two days of work by a group of hackers.</p>


<h2>Jolie and Services</h2>
<p>Kevin Ottens has continued his work on intergrating the functionality of <a href="http://jolie.sourceforge.net">Jolie</a> (a service concertation framework). The Jolie integration moves Plasma forward towards the ability to move Plasma services, widgets and data around on the network, and to control Plasmoids and applications across the network.</p>


<h2>Improved and New Plasmoids</h2>

<p>Blingbrother Marco Martin presented his work on the <a href="http://www.notmart.org/index.php/BlaBla/Rotating_your_adult_content">Video Plasmoid</a>, working towards bringing mediacenter-like features to Plasma. Anne-Marie Mahfouf presented new Nepomuk integration features of the Picture Frame applet, which integrates the Picture's meta-information into the Plasmoids context view. (The Picture Frame also serves as an example for Alexis and the other Trolls' / Qties' work on Kinetic you can see in Alexis' screencast.)</p>
<p>
Chani Armitage further worked on her <a href="http://chani.wordpress.com/2009/02/07/adventures-in-plasmaland-part-4/">Victory Calendar</a> Plasmoid, a small scripted Plasmoid which currently mainly serves as an example for Plasma's JavaScript support.</p>
<p>
Your editor presented three new Plasmoids to the team. The <a href="http://vizzzion.org/?blogentry=838">NetworkManager Plasmoid</a> approaches a first stable version, thanks mostly to the guys from the OpenSuse team for their work on the infrastructural bits. The NetworkManager Plasmoid will probably be ready for review by other developers within the next two or three weeks. The <a href="http://vizzzion.org/?blogentry=904">Crystal</a> Plasmoid, which is supposed to be a testing ground for new search-related concepts received some bugfixes, as well as an update to the support for MediaWiki searches based on Richard Moore's MediaWiki class (which can be found inside the Crystal sources in case you're interested in using it).</p>
<p>
Lion Mail, which is a new Plasmoid that has been started during the recent CampKDE on Jamaica (hence its name) will make Email from Akonadi accessible on the desktop. The concept of Lion Mail is to display collections of Email from Akonadi (think "Unread Emails", "Emails tagged as Important", "Emails belonging to a certain project or context") in the panel or on the desktop. It also contains a Plasmoid that displays the metadata and contents of an email on the desktop, changing the amount of displayed information based on the available size. The basics of this Plasmoid are in place now, it will probably be released with KDE 4.3 as it is based on the Akonadi PIM storage. Lion Mail feeds from the Akonadi dataengine, which has been extended to offer contact information to Plasmoids. A dataengine allows easy access also for scripted Plasmoids to all kinds of data.</p>

<p>Zanshin as a new application and framework developed by Kevin Ottens. It's based on the Getting Things Done philosophy and will provide todo and task management to the desktop.</p>



<h2>Qt 4.5</h2>

<p>During Tokamak II, some developers have upgraded their Qt version to the release candidate of 4.5. Performance seems to have improved quite a bit. The team has also found some minor rendering issues with the new Qt version. Through collaboration with the Qt team, those issues have already been addressed.</p>

<p>Some issues the Plasma team encountered with Qt 4.4 have also been resolved in the new version, the most notable being the resizing of QGraphicsLayouts which are extensively used by Plasma. In KDE 4.2 some Plasmoids contain workaround for these issues to make the Plasmoids work correctly with Qt 4.4. Most of the workarounds could be removed in KDE's trunk which is to be based on Qt 4.5.</p>

<p>It also becomes apparent that some of the workarounds KDE 4.2.0 that are required for Qt 4.4 might not behave correctly with Qt 4.5 since they rely on broken behaviour in Qt 4.4, which has now been fixed.</p>

<p>A possible solution for this would be to provide a set of patches that remove those workaround to make KDE 4.2 work well with Qt 4.5. Another possible solution would be to add conditional code pathes for Qt 4.4 and Qt 4.5. The Plasma team is currently looking at solutions for these issues.</p>

<h2>More Polishing</h2>

<p>Davide "Beard" Bettio has been integrating information about holidays into the Plasma calendar widget and along with it also created a dataengine to make this information available to other Plasmoids in an easy way. Marco Martin has applied his magic to various visual glitches in Plasma. Artur has improved the Pastebin Plasmoid (a drop area in Plasma that posts the dropped content to a "pastebin" server for easy sharing across the Net. Smaller fixes have also gone into the Microblogging widget, but also into various stock Plasma components that are shared across Plasmoids.</p>

<h2>Productive Meeting</h2>

<p>The meeting has been very productive and provided great opportunities for the Plasma team to bind closer together, to develop new ways of interacting with your data and applications and to continue working on existing features making them better, more integrated and more beautiful. Many bugs have been hunted during the meeting, not all of them in software. Your editor is still <a href="http://en.wikipedia.org/wiki/Bedbug">recovering</a> as we speak.</p>