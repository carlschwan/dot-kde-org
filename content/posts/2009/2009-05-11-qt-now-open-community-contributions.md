---
title: "Qt Now Open for Community Contributions"
date:    2009-05-11
authors:
  - "troubalex"
slug:    qt-now-open-community-contributions
comments:
  - subject: "subscribe to tasks"
    date: 2009-05-11
    body: "Can I subscribe/CC myself to tasks at http://www.qtsoftware.com/developer/task-tracker/ finally?"
    author: "testerus"
  - subject: "Alright!"
    date: 2009-05-11
    body: "Cool, guess it's time to sub those printing patches I've been working on..."
    author: "odysseus"
  - subject: "Yeha"
    date: 2009-05-11
    body: "Finally. I hope that gets you some more guys contributing. I love how Qt has changed in terms of licensing and community work."
    author: "STiAT"
  - subject: "subscribe to tasks"
    date: 2009-05-11
    body: "No, not yet. This will happen a bit later when we change the Task Tracker infrastructure."
    author: "thiago"
  - subject: "My own addition"
    date: 2009-05-11
    body: "I've been working on a QAbstractItemProxy that automatically groups items based on the value of any column. It's used in Amarok to add folder grouping to the saved playlists. It has a lot of potential uses, so I'm gonna develop it further on gitorious and hopefully it will become part of Qt one day.\r\n\r\nBart"
    author: "Stecchino"
  - subject: "Safe Languages"
    date: 2009-05-11
    body: "It is nice to see Qt available in a safer language.  While I am not all that clear about how much safer Java is is than C++, it is certainly an improvement.\r\n\r\nThere is a great letter to the editor in Communications of the ACM, May, 2009: \"Logic of Lemmings in Compiler Innovation\" regarding the lack of safe programing languages in common use.\r\n\r\nI hope to see Qt ported to other, even safer, languages.  The most popular of these is FORTRAN >= 95 (which has now become a general purpose language that even supports OOP) which is not as illogical as it sounds at first.\r\n\r\nThere is another issue which is that of the need for a good compiler.  I haven't yet tried GCJ-4.4.0, so I don't know how it is coming along.  Obviously, a good compiler is necessary for the success of any language in OSS.  And that is where FORTRAN fails.  The current GNU FORTRAN compiler needs a lot of work to become 2003 or 2008 compliant, although the current version is becoming a adequate 95 compiler for non-numeric uses -- there is also the competing G95 project.  So, perhaps Qt for FORTRAN 95 would be a good thing and finally provide a safe language for GUI applications."
    author: "KSU257"
  - subject: "jambi reference"
    date: 2009-05-11
    body: "i would suggest mentioning at least in a couple of of words what jambi is - it's a bit less popular than qt itself ;)"
    author: "richlv"
  - subject: "Re: Safe Languages"
    date: 2009-05-11
    body: "Hi KSU\r\n\r\nQt Jambi is not news. It's a product that has existed for 2 years now. Other \"safe\" languages for which Qt bindings exist are Ruby, C# and Python, probably even more.\r\n\r\nIf you want to start working on a Fortran 95 binding for Qt, talk to the KDE bindings team. I'm sure they can use the help and they may provide some hints on where to start."
    author: "thiago"
  - subject: "Compilers?"
    date: 2009-05-12
    body: "Where are the compilers for Ruby, C#, & Python.  Is there a difference between a port and language bindings?\r\n\r\nPerhaps you missed the significance of the Jambi announcement.  It is not that it is new.  What is new is that it is being converted to a community based OSS project.\r\n\r\nYes, there are other language projects for Qt.  IIUC, bindings for Pascal are being developed.  Pascal is a very safe language although it has declined in popularity."
    author: "KSU257"
  - subject: "control"
    date: 2009-05-12
    body: "I know that opening the development is in general a good idea, but I was wondering if you had in mind some sort of control of what can go in and what falls out the scope of Qt.\r\nIf everyone is able to commmit whatever he thinks is right then Qt may become bloatware soon.\r\n\r\nAlso, have you considered merging Qwt into Qt?\r\n\r\n\r\nAbout what to focus in the future... well, I have currently a necessity which could share with you:\r\nI'm trying to make a synth app. This kind of app has typically a lot (>100) of widgets on the screen at the same time. Also I use a lot of CSS gradients and SVG to mimic the appearance of these kind of apps in other systems.\r\nI've found that resizing the window (or moving a toolbar from horizontal to vertical, which in turn resizes the content also) is very slow with this setup. I know that there are some performance improvents in the making so I'm waiting this with expectation.\r\nMaybe you should use this usecase as some sort of stress test. I can provide an example ui file if you want.\r\nAlso there is a widget I'd love to see: the \"piano keyboard\" widget, which seems ubiquitous in these kind of apps. I don't have much programming/design skills or time to make a beautiful SVG piano keyboard with an arbitrary number of keys, animation of keys pressed, arbitrary resizing, etc. so I'd love to see anybody implementing this.\r\n\r\n\r\nAnyway, I want to thank the trolls for the best toolkit available.\r\nYou guys never cease to amaze me."
    author: "DeiF"
  - subject: "Yes, Please!"
    date: 2009-05-12
    body: "Please do so.  Qt-4 printing is basically broken.  I find that I can't even print with LPR.\r\n\r\nHowever, QPrint is never going to be a substitute for KPRINT (which has gone missing in KDE-4.x.y)"
    author: "JRT256"
  - subject: "Virtual MIDI keyboard"
    date: 2009-05-12
    body: "http://www.qt-apps.org/content/show.php/Virtual+MIDI+Piano+Keyboard?content=88233\r\n\r\nAnd the widget\r\nhttp://www.qt-apps.org/content/show.php?content=85050"
    author: "blergh"
  - subject: "If Java is compiled, so it Python"
    date: 2009-05-12
    body: "Hello,\r\n\r\nJava translates into bytecode which is then run in a VM. Python translates into bytecode which is then run in a VM.\r\n\r\nSo if Java counts as a compiled language, Python is one as well."
    author: "kayonuk"
  - subject: "Cool"
    date: 2009-05-12
    body: "I'll look into setting up a development environment for Qt tomorrow. I want http://www.qtsoftware.com/developer/task-tracker/index_html?method=entry&id=144577 implemented, and it seems rather easy."
    author: "majewsky"
  - subject: "Programing language"
    date: 2009-05-13
    body: "Java is a programing language.  Any true programing language can be either compiled or run on an interperter (there are various kinds and a VM is one of these).\r\n\r\nWhat makes it a \"compiled language\" is not the language itself, but rather, it is the existence of a compiler.  Now, if you had read the whole thread, you would note the mention of the GCJ compiler:\r\n\r\nhttp://en.wikipedia.org/wiki/GNU_Compiler_for_Java\r\n\r\nwhich will compile Java either to byte code to be run on a virtual machine or compile it to machine code (just like C++).  It can even compile Java classes and use them with C++ programs (sort of compiled JNI).\r\n\r\nThe same could probably be done for other languages (Python, Ruby, C#) that are usually interpreted.  It was even done for BASIC although it isn't a true programing language."
    author: "JRT256"
  - subject: "true?"
    date: 2009-05-13
    body: "You have a very strange definition of what makes a programming language \"true\"."
    author: "Tuxie"
  - subject: "Why BASIC isn't a true programing language"
    date: 2009-05-15
    body: "Your remark makes little sense.  IAC, the reasons that BASIC isn't a true programing language are technical and might not make any sense outside of a technical computer science discussion.\r\n\r\nOr do you mean the reason that Java is a true programing language?  It is just as much a programing language as C++ is."
    author: "JRT256"
---
<p>Today, we at Qt Software have <a href="http://qt.gitorious.org">opened our repositories to the public</a>. Any developer can now help guide and shape the future development of Qt by contributions of code, translations, examples and other material to Qt and Qt-related projects.  We have also published our roadmap and released Qt Jambi under the LGPL.  Read on for details.</p>
<!--break-->
<p>Since we moved to Git as our version control system in late summer 2008, the web-based Gitorious was an obvious choice for helping us manage the contributions. We have funded its founder Johan Sørensen and one of his colleagues to work on tweaks and added functionality like team support and wikis that are now available to all projects using Gitorious. We will continue to work on improvements to Gitorious together with the project in the future.</p>

<p>The <a href="http://qt.gitorious.org">new contribution site</a> will not only host Qt itself but also a number of Qt-related tools and projects including the Qt Creator IDE, Qt Jambi and various Qt Labs projects.</p>

<p>Details about the contribution model can be found <a href="http://www.qtsoftware.com/developer/the-qt-contribution-model/">on our website</a> or in <a href="http://labs.trolltech.com/blogs/2009/05/11/qt-public-repository-launched/">Tor Arne's blog posting</a>.</p>

<h3>Qt Roadmap Published on qtsoftware.com </h3>

<p>Along with the launch of the open repositories, we are publishing <a href="http://www.qtsoftware.com/developer/qt-roadmap">a roadmap</a> that gives an overview of current features in development and research projects and helps you understand Qt's future direction. Not only that, we would love you to help us shape this direction through feedback and contributions.</p>

<h3>Qt Jambi 4.5.0_01 Released Under LGPL</h3>

<p>Additionally, we have released Qt Jambi to the open repositories today. This is the final feature release which will be followed by a one-year maintenance period. We are confident that a commited group of developers will take over and make it a successful open source community project.</p>

<p>Read more about the Jambi release in <a href="http://labs.trolltech.com/blogs/2009/05/11/qt-jambi-450_01-released-contributors-wanted/">Eskil's blog posting</a>.</p>