---
title: "KDevelop Beta 6 and PHP Beta 1 Released"
date:    2009-11-04
authors:
  - "apaku"
slug:    kdevelop-beta-6-and-php-beta-1-released
comments:
  - subject: "Great IDE!"
    date: 2009-11-04
    body: "I am using the svn version for some time and am extremely happy with it. KDevelop is a great IDE which has a very good understanding of C++ and supports the workflow in various ways. For example, typing\r\n\r\nTObject x; \r\n\r\nx.\r\n\r\nshows a list of methods of x (and parent classes). There are many more ways were KDevelop makes writing C++ code easier and more fun.\r\n\r\nThanks to the KDevelop guys for this wonderful application! I hope we will see many more plugins and support for other languages in the future. Rock on!"
    author: "mkrohn5"
  - subject: "is bjam builder usable in kdevelop 4?"
    date: 2009-11-04
    body: "I'm still using kdevelop 3.5.5-0 because up to now I couldn't manage to find how to build my projects using bjam in kdevelop 4, but I would love to move to the new version as soon as possible. Could anybody tell me please if that is already possible and in such case how I could use bjam within kdevelop4?\r\n\r\nThanks in advance, and congratulations for your nice work!"
    author: "avlas"
  - subject: "not an obstacle"
    date: 2009-11-04
    body: "for me kdevelop brings alot of code-parsing related benefits that overweight the need of building my project outside kdevelop (e.g. in yakuake)."
    author: "shaforostoff"
  - subject: "Good news but..."
    date: 2009-11-04
    body: "I think that it's good news and thank you guys for your work. But I think that Kdevelop project must have more publicity and up-to-date site. Community does not know what is going on, what expect and when to expect some results. Project site is updated last week and seems better, but it was just terrible last week.\r\n\r\nI've been even thinking that Kdevelop is dead before meeting one of developers at OSDN-2009 conference :)\r\n\r\nAnd by the way, what is IDE for KDE development? :)"
    author: "alukin"
  - subject: "Is bjam builder usable in kdevelop 4?"
    date: 2009-11-04
    body: "Does it produce Makefiles ?\r\nIf yes, them it will work file you can open your project using KDevelop4."
    author: "amilcar"
  - subject: "Kdevelop project must have more publicity and up-to-date site"
    date: 2009-11-04
    body: "What website are you talking about ?\r\n\r\n> Community does not know what is going on, what expect and when to expect some results.\r\n\r\nThat info is on the FRONT PAGE of kdevelop.org\r\n\r\n> Project site is updated last week\r\nWhat site ? The front page of KDevelop.org was updated in 22 August (take a look at the bottom the page)\r\n\r\n> what is IDE for KDE development?\r\nWhere have you seen that ?\r\n\r\nSorry about all the questions, but you really got me confused.\r\n"
    author: "amilcar"
  - subject: "Any info on how Quanta + is coming?"
    date: 2009-11-04
    body: "Hello, I have heard that a KDE4 version of Quanta + will depend on the new infrastructure provided by KDevelop.  While it is clear that PHP support is already better than in Quanta 3, are there any plans to provide for a more full framework for Website Development like Quanta provided? (CSS, (X)HTML, website-specific quick buttons, etc)"
    author: "blantonv"
  - subject: "Any info on how Quanta + is coming?"
    date: 2009-11-04
    body: "Yes read the blog post on the front page of www.kdevelop.org"
    author: "amilcar"
  - subject: "I like it"
    date: 2009-11-04
    body: "I like it. I think it is currently much better than Qt creator. But who knows, maybe that will change.\r\n\r\nWhat I'd like to see, though:\r\nA context sensitive outline. Means a outline that always shows the symbols of the currently opened files. I'd like to have that opened at the same time I see the project structure (the files). So I have one window where I navigate the files and (possible on the other side of the code editor or beneath the project outline) the context outline. That's how I work in Eclipse and NetBeans.\r\n\r\nAlso I'd like to see better mercurial support. But I guess because KDE will use git this will not receive much attention. I guess I'd have to write it myself (but I don't have time for such a thing currently). However, git is fine too. Every DSCM is better than CVS/SVN."
    author: "panzi"
  - subject: "see this blog"
    date: 2009-11-04
    body: "There is a <a href=\"http://nikosams.blogspot.com/2009/11/web-development-and-kde4.html\">blog post</a> which might be interesting for you."
    author: "mkrohn5"
  - subject: "Use bugs.kde.org for requests please"
    date: 2009-11-05
    body: "Hey panzi!\r\n\r\nI concur with your in regard to the need for a outline tool view in addition to the current outline feature (it's quickopen, i.e. ctrl + alt + n, limited to the current file atm).\r\n\r\nBut please use bugs.kde.org and report a wish request for that there.\r\n\r\nWhile you are at it also create a wish request to make it possible to stack tool views on top of each other (left & right area) or next to each other (top & bottom area). For now you could put one of the tool views on the left, the other on the right.\r\n\r\nRegarding mercurial, there is a plugin for that. Did you try it?"
    author: "Milian Wolff"
  - subject: "Multiplatform"
    date: 2009-11-05
    body: "I've been using it on KDE 3.x for years. It's a pity i's not multiplatform at all. :-( :-(\r\n\r\n"
    author: "mr-echoes"
  - subject: "Indeed"
    date: 2009-11-05
    body: "Indeed, that is a real pitty.\r\nI would really like to try out KDevelop, but it still does not seem to run on Windows. I really need a cross platform IDE. I was using QDevelop before, and Qt Creator at the moment, but I'd really like to use KDevelop again. "
    author: "andresomers"
  - subject: "no, please no more features"
    date: 2009-11-05
    body: "KDevelop 4 has really awesome C++ parsing and I've been looking forward to them releasing a stable release. But the KDevelop4 release cycle has been GINORMOUS. They've been adding features for too long.\r\n\r\nSo I really like the tone of this dot post, sounds like they are going for the last mile. :)\r\n\r\nThey do have a symbol explorer, though maybe its only a drop down menu instead of a separate window. Personally I'm not a fan of the VCS features of KDevelop, seem too simple to be useful. Its mostly just a collection of menu options that trigger git commands, commands easier down on the command line. Really all I want out of an IDE is being able to highlight a function and say \"show me the history\". But... DON'T add this feature. \r\n\r\n:D :D\r\n\r\n(though seriously support development in chroots, but only because this lack of feature blocks me from using kdevelop most of the time. :P)"
    author: "eean"
  - subject: "bjam builder in kdevelop4?"
    date: 2009-11-05
    body: "There is no Makefiles in my project. what I would only need would be to add the builder pathway (where bjam resides) within the \"Project Options\".\r\n\r\nIn kdevelop3 it was located in \"Build Options\" and then indicating the \"Name of build script\" when selecting \"Other\" in \"Build Tool\". But I'm completely lost in Kdevelop4 when I try to do something similar...\r\n\r\nThanks in advance"
    author: "avlas"
  - subject: "Is bjam builder usable in kdevelop 4?"
    date: 2009-11-05
    body: "I don't have Makefiles in my projects but in kdevelop3 I have the option of choosing \"Other\" in \"Build Tool\" (Sorry, I explained that in more detail in another comment below when I should just reply to your comment here).\r\n\r\nThanks for your comment anyway!"
    author: "avlas"
  - subject: "KDevelop for Java?"
    date: 2009-11-09
    body: "There are any plans for this?\r\n\r\nI'm just so tired of eclipse an all those java-based ide's because of it's slowness."
    author: "julian"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/kdevelop.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kdevelopsmall.png" width="320" height="274" /></a><br />
KDevelop 4 in action
</div>
The KDevelop team is proud to announce the sixth beta of KDevelop4. There haven't been any major features started since beta 5 as this is mostly a bugfix release (demonstrated by <a href="http://bugs.kde.org/buglist.cgi?product=kdevelop&resolution=FIXED&resolution=WORKSFORME&bugidtype=include&chfield=resolution&chfieldfrom=2009-08-21&chfieldto=2009-11-01&order=Bug+Number&cmdtype=doit">the list of fixed bugs</a>). This also marks the end of the KDE 4.2 support. With this release KDevelop4 requires KDE 4.3 and Qt 4.5 to fully supply the GUI we want to provide our users with. It is also the last release that we're doing on behalf of ourselves, all upcoming betas will be done together with KDE 4.4. We are aiming at releasing with KDE 4.4.0 as we think that KDevelop4 is ready feature-wise and really needs to get a first release out of the door.
<!--break-->
Additionally with this beta, we are also releasing the first beta of the PHP and PHP-Documentation plugins. These two add PHP language support (including classview and code-completion) and documentation-view to KDevelop. They are currently developed in playground and are shaping up quite nicely. There is no release date set on these two, however we'd like to get these into the unstable repositories of distributions.

You can find the source packages on the <a href="http://download.kde.org/unstable/kdevelop/3.9.96/src">KDE mirrors</a> and there are <a href="http://www.kdevelop.org/mediawiki/index.php/KDevelop_4/compiling">instructions for compiling</a>. You can also check out a <a href="http://www.kdevelop.org/mediawiki/index.php/KDevelop_4/KDev3_KDev4_comparison_table">comparison of the KDE 3 and KDE 4 versions of KDevelop</a>.