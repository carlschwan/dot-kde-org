---
title: "Canonical Party Welcomes Gran Canaria Desktop Summit"
date:    2009-07-04
authors:
  - "jriddell"
slug:    canonical-party-welcomes-gran-canaria-desktop-summit
---
<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex">
<img src="http://farm3.static.flickr.com/2448/3686193748_f9006069bd_m.jpg" width="240" height="180" /><br />
Free t-shirts were popular
</div>

Tonight the Gran Canaria Desktop Summit was opened with a party sponsored by Kubuntu's very own Canonical.  Stickers, t-shirts and beer were all given out to contributors and users of KDE, Gnome and any other free software environment.  Some converts were made from the local Canary island population who were enthused by the spirit of freedom.  

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex">
<img src="http://farm3.static.flickr.com/2464/3685382565_3d6b9aba04_m.jpg" width="240" height="180" /><br />
Heavy metal and free software do mix
</div>

Conversation ranged from the essential cross desktop collaboration issues to the question of whether it ever rains in Las Palmas. 


More <a href="http://www.flickr.com/photos/jriddell/sets/72157620924123798/">photos on Flickr</a>.
<!--break-->
