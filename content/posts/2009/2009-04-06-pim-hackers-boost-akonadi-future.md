---
title: "PIM Hackers Boost Akonadi Into The Future"
date:    2009-04-06
authors:
  - "sebas"
slug:    pim-hackers-boost-akonadi-future
comments:
  - subject: "Plaxo integration and sync with Windows Mobile 6"
    date: 2009-04-06
    body: "I am relatively new on the KDE desktop and like it a lot. The addressbook looks impressive, but I am still not using it until I have found out a little more.\r\n\r\nI have gathered a lot of addresses over the years (1500+) that needs a good application. Also, I am using Plaxo and LOVE it. Plaxo takes away the need for updating a good portion of these addresses and I hope there will be a good integration with the KDE addressbook soon.\r\n\r\nPlaxo did some stupid things in the beginning (sending out requests to all your contacts unless you specifically did not want it) and it caused a lot of people to put it on their spamlist. Fortunately, they are now a lot more mature, and their system works VERY well.\r\n\r\nMy second wish is probably easier - I need to sync with Windows Mobile 6 (I have an HTC phone).\r\n\r\nThese two things would completely take away my need to run Outlook as I am not using Outlook for e-mail, just for the addressbook sync!"
    author: "Oceanwatcher"
  - subject: "Akonadi and NFS..."
    date: 2009-04-06
    body: "probably this has been discussed already, but what is the recommended way to use Kontact with your $home on an NFS-share in the future?\r\nIf I understood correctly, Akonadi stores all the PIM-data (emails, contacts, calendar) in a \"real\" db (mysql?)\r\nDoes this not limit the use of Akonadi, as you can't put a mysql db on an NFS-share?\r\n\r\nHow's the level of integration with Nepomuk? Nepomuk itself needs a db-backend (semantic RDF storage). How to get the emails into Nepomuks index? Does it need some kind of replication? Is it not overlapping functionality?"
    author: "thomas"
  - subject: "buzzword-bingo!"
    date: 2009-04-06
    body: "buzzword-bingo huh. Well, didn't that work out just fine? ;-)\r\n\r\nGood read, nice to know what is going on in Akonadi land."
    author: "jospoortvliet"
  - subject: "Akonadi and NFS..."
    date: 2009-04-06
    body: "You should give nfsv4 a try. Locking has been updated quite a bit and it supports client and server recovery after a potential crash a lot better.\r\n\r\nAkonadi and Nepomuk are to different beasts, some seperation will always be needed. Trust the devs on this one ;)\r\n\r\nHope that helps."
    author: "MarkHannessen"
  - subject: "KitchenSync"
    date: 2009-04-06
    body: "In kde 3.5 you could can use kitchensync for this. ( And i suppose you still can ) Unfortunately kitchensync does not appear to be ported to the kde4 serries just yet.\r\n\r\nI suppose we'll have to wait for akonadi support before we are going to see it back in full glory again.\r\n"
    author: "MarkHannessen"
  - subject: "Kitchensync in KDE4"
    date: 2009-04-08
    body: "Unfortunately, I am on Kubuntu 9.04 beta, so the Kitchensync solution will not work for me. But I have heard about it and I am trying to keep an eye on the development.\r\n\r\nJust wish they will add Plaxo as well :-)"
    author: "Oceanwatcher"
  - subject: "maybe you can re-think the nfs share thing"
    date: 2009-04-16
    body: "if there is a mysql db, you will be better connecting to a remote db rather than trying to use the nfs share to place the db... and if you are planning to access the db from multiple locations connected with multiple mysql servers you will have bigger problems."
    author: "tristangrimaux"
  - subject: "MySQL dependency"
    date: 2009-04-18
    body: "I'm using KMail for over a year now (coming from Evolution, then Thunderbird) and I'd wanted to use an integrated KDE program to manage my contacts. So I tried to install KAddressbook on my Debian Squeeze (KDE 4.2.2) laptop. Not so much disk space, bad batteries, long boot times etc. - you know :) Then I saw package dependencies which really devastated me... not just this Akonadi thingy (don't know why I need it, but... well... never mind), it wanted me to install a complete MySQL 5.0 server including its dependencies (exim4, mailx, bsd-mailx) and a half of a dozen further packages.\r\n\r\nProceeding to my question: Does it really require a MySQL DB server just to manage a few eMail addresses?"
    author: "Ren\u00e9 J. Pollesch"
---
This weekend the A-Team (Akonadi, not Amarok) gathered in <a href="http://www.kdab.net">KDAB</a>'s office in the heart of Berlin to push the Akonadi PIM storage database to the next level.
<br />
On Friday afternoon, after everybody arrived, the meeting started with a series of presentations to get everybody on the same page with respect to progress in various parts of Akonadi.
<!--break-->
<img src="http://dot.kde.org/sites/default/files/akonadi2009-groupphoto.jpg" />
<em>top (left to right): Tom Albers, Stephen Kelly, Frank Osterfeld, Bernhard Herzog, Ingo Klöcker, Bertjan Broeksema, Till Adam, Thomas McGuire, Kevin Krammer, Volker Krause
bottom: Andras Mantia, Keving Ottens, Sebastian Kügler, Kevin Krammer, Dmitry Ivanov
</em><br />
After the kickoff, Sebastian Kügler demonstrated <a href="http://vizzzion.org/?blogentry=908">Lion Mail</a>, which is a Plasma applet displaying emails. Lion Mail is an experimental approach to <em>leveraging the capabilities of the Akonadi PIM data store in the primary workspace interface of the KDE 4 desktop</em>. It displays collections of emails and individual emails on the desktop, dashboard and panel. After this weekend, it's possible to edit aspects of emails (such as flags) directly from the desktop shell, directly load multiple collections in an applet, a number of bugs have been fixed in the handling of multiple collections. The team also worked on making it possilbe to drop and then display remote content on Plasma. This way, it'll be possible to deal with emails and groups of emails as first class objects on the plasma desktop in a natural drag and drop way, and will also pave the way to smooth drag & drop interaction between full email clients based on Akonadi and the plasma shells.
<br />
After the presentation of real world uses of Akonadi, the discussion on how to tackle sorting, searching and filtering in Akonadi started and generated the first ideas how to <em>elevate Akonadi to the next level of a powerful and contemporary personal data cache</em>. The design of features such as virtual folders and searching is another area of development and research currently. The SPARQL query language seems to be the most promising tool for this dirty job.
<br />
After the discussion Tobias started with the initial preparations to implement that, building on top of the Nepomuk framework as laid out in the past years. Semantic concepts on top of a powerful provider for personal data look like a sensible way to make it possible for users to deal with an increasing amount of data and increasing complexity of its nature.
<br />
Stephen Kelly has been working on an abstract representation of Akonadi Items (emails, contacts, microblogs, ...) and collections of those to make it possible for application developers to display even complex data structures in easily in end-user applications. Stephen's Akonadi models provide many different types of data, using the model-view-controller pattern to create an abstraction that makes it possible to display complex heterogeneous data structures just using or creating a custom view on top of the models. During the sprint Stephen also polished his presentation for the upcoming Linux Foundation Collaboration Summit, tapping into the experience of his peers in a wholly different area.
<br />
Luckily, on Friday night, the Akonadi hackers were left untouched by the customs officers who decided to check the work permits of the personnel in the restaurant the group was dining. Apparently, hacking on Akonadi doesn't need a work permit, as opposed to gate-keeping the bathrooms. After dinner, the group headed back to the KDAB Berlin HQ to continue hacking until late in the night.
<br />
On Saturday, the early bird meeting was all about IMAP. Kévin Ottens and Andras Mantia are the brave individuals taking on this humongous task of creating an asynchronous and fast IMAP library as a replacement for the existing Akonadi IMAP resource. The guys silently hacked away for the rest of the sprint and later on Saturday were able to present the first results in the form of listing folders, including encoding funkiness of the IMAP server used for testing the code.
<br />
Tom <em><a href="http://www.mailody.net/">Mailody</a> Man</em> Albers' microblogging Akonadi resource found its way into the KDE PIM module and the core of Akonadi. The microblogging resource retrieves and caches the chit-chat from Identi.ca and Twitter and offers it in the unified Akonadi way to applications. Fetching the data happens in the background, applications get notifications when new items are posted. Shortly after, support for this kind of data hit the (experimental) Plasma Akonadi data engine, making this kind of information easily accessible from (scripted) Plasmoids. Tom also <a href="http://www.omat.nl/drupal/content/Akonadi-meeting-day-0">blogged</a> <a href="http://www.omat.nl/drupal/content/Akonadi-meeting-day-1-and-2">twice</a> from the sprint. These microblogging features are lined up for KDE 4.3 this Summer.
<br />
But behold, IMAP is not the only way to get your email into The One and Only PIM Storage! Bertjan took on the mbox format support for Akonadi, keeping in mind the inevitable use case of DVD images sent by email to your local mbox file. At the same time the guys are getting ready for the next generation of users, commonly feared as <em>"Those Who Sent Blueray Images Per Email (To Your Mobile Device)"</em>.
Thomas McGuire (the Chuck Norris of email clients) started working on support for POP3. So for the sprint the <em>Race of the Sprint</em> was all about who gets the initial folder listing done first? The results lead us to a coverage for most commonly used ways to suck email into your computer.

Bernhard Herzog, of the long-time KDE and Kolab pillar <a href="">Intevation</a> showed interesting demos monitoring automatic software packaging using the Debian format for deployment client software on end-user machines. The software provides monitoring of automatic builds of the KDE 3 and KDE 4 Enterprise branches of KDE PIM. Additionally it covers the cryptographic plumbing that make the (email) world a safer place. 
<br />
Till and Volker, a.k.a. the <em>Adam and Eve^WKrause of Akonadi</em> acted as the flesh-and-bone interface to <em>The Delphi of Akonadi</em>, patiently answering all kinds of more and less esoteric questions about <em>How it Works</em> and dealing smoothly with their virtual princess.
<br />
Ingo <em>Kandalf of KMail</em> Klöcker worked on various issues in the KDE 4 version of the best email client in the world. Among his glorious tasks the proper updating of the date in KMail's listview at midnight. Ingo's work results in an improved representation of the space-time continuum of your mailbox.
<br />
Frank Osterfeld and Dmitry Ivanov have been busy bees <em>Feeding the Princess</em> (with RSS) or in <em>buzzword-bingo</em> terms leveraging desktop-wide availability and sharing of XML feeds to raise the level of web-integration  and interoperability for home, pro-sumer and enterprise use cases. Dmitry was mostly fixing bugs and writing tests for the Akonadi RSS library and discussed with Frank the work items left to be done for an Akonadi-based Akregator. The feed guys also took a look at the Akregator wishlist at on bugs.kde.org to make sure the design can accomodate all those fancy features people requested. Frank profiled the rss-akonadi library. There is a tool that helps migrating the list of feeds and the RSS articles from Akregator to Akonadi, but it was awfully slow. Frank identified the bottlenecks and fixed at least some of them.<br />
During the sprint, various Berlin-based KDE hackers dropped by and challenged the PIM crowd in either coding or Foosball. Their presence added to an inspiring and open atmosphere.
<br />
As we speak (read: write, not 'as you read' -- for clarity's sake), the Dutch Trinity is on their way back from the <em>Capital of Currywurst</em> to the Land of Unreliable and Cursed Upon Railway Systems, looking back at a weekend full of code, receiving and spreading knowledge and face-to-face interaction with their peers. Unfortunately also at not so glorious Foosball matches.
</p>
<p>
<em>So when will all this land in KDE?</em>, the attentive reader might ask herself. Current plans of porting Kontact to using Akonadi as data store are to have contacts and calendaring in 4.3 in July, and email and feed management in 4.4 next January. Those are surely ambitious plans, but a strong team and some corporate muscle promise a good chance of achieving this task. 
</p>
<p>
The Akonadi team would like to thank both KDAB and the <a href="http://ev.kde.org">KDE e.V.</a> for hosting and supporting this meeting financially, and thus for generously making this event possible. It has been a tremendously successful meeting in terms of raw code output, transfer of knowledge, community-building and <a href="http://techbase.kde.org/Projects/PIM/Akonadi/Meeting2009-04">generating new ideas</a> for PIM and the Free Desktop.
</p>