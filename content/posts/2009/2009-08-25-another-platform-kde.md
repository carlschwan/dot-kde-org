---
title: "Another Platform for KDE"
date:    2009-08-25
authors:
  - "Stuart Jarvis"
slug:    another-platform-kde
comments:
  - subject: "+1 for ReactOS"
    date: 2009-08-25
    body: "I'm really looking forward to stable releases of ReactOS, and KDE on ReactOS. Good luck to all devs!"
    author: "majewsky"
  - subject: "I'm all for an open-source"
    date: 2009-08-26
    body: "I'm all for an open-source Windows, but ReactOS comes across to me as an interesting project at the most..."
    author: "madman"
  - subject: "Niche markets though"
    date: 2009-08-27
    body: "I wonder if anyone ever said:\r\n\r\n\"I'm all for an open-source UNIX, but Linux comes across to me as an interesting project at the most...\" ;-)\r\n\r\nSeriously though, I also don't see ReactOS becoming as popular as GNU/Linux. But ReactOS - compared to WINE for example - has the potential to work with weird old devices for which only Windows drivers exist. There are more of those than you might think knocking around in supposedly high-tech scientific institutions like the one I work in, tied to geriatric computers with old and unsupported versions of Windows. Having ReactOS, KDE (and, therefore,  fish://) on these would be wonderful. One day, maybe..."
    author: "swjarvis"
  - subject: "Touche!"
    date: 2009-08-28
    body: "Touche!"
    author: "madman"
  - subject: "I realize this has a ways to"
    date: 2009-09-01
    body: "I realize this has a ways to go with testing but enhancing KDE this way is encouraging. This is a business that works on constant improvements to make the product more and more attractive. Enhancing KDE with ReactOS will do the same thing and everyone will be better off because of it. <a href=\"http://www.bloggliv.se/casino/\" id=\"clean-url\" class=\"install\">casino</a>"
    author: "Benjamin"
---

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/kgeography.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/kgeography.jpeg" width="167" height="155" /></a>
Learn where you are
</div>

While the KDE community busied itself with preparations for the 4.3 release, KDE 4 continued to spread to new platforms with <a href="http://www.reactos.org/">ReactOS</a> user Davy Bartoloni <a href="http://www.translate.google.com/translate?u=http%3A%2F%2Fwww.reactos.org%2Fforum%2Fviewtopic.php%3Ff%3D15%26t%3D7306%26start%3D0%26sid%3D02c2071e861c8886f091462547f255d4&sl=it&tl=en">reporting</a> (machine translation of <a href="http://www.reactos.org/forum/viewtopic.php?f=15&t=7306&start=0&sid=02c2071e861c8886f091462547f255d4">original Italian</a>) some success in running KDE applications on that operating system.

ReactOS is a free software reimplementation of the Windows XP architecture. Earlier efforts existed to run KDE 3 on ReactOS using <a href="http://www.cygwin.com/">Cygwin</a>, but the latest successes use native KDE 4 Windows applications as produced by the <a href="http://windows.kde.org">KDE on Windows project</a>. Some applications like KStars, Kolourpaint, KCalc and KGeography are running already on ReactOS (see the screenshots, reproduced with the permission of Davy Bartoloni). The KDE on Windows project packages many more applications however, including Amarok, Okular and Gwenview. When both the KDE on Windows and ReactOS projects mature these applications will also become available.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/kstars.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/kstars.jpeg" width="175" height="150" /></a><br />
Waiting for the stars
</div>

ReactOS is still in alpha stage and not recommended for everyday use. Attempting to use the latest KDE Windows installer on ReactOS resulted in a blue screen of death and so this demonstration of running KDE applications required compiling them on Windows and then transferring the binaries to ReactOS. Despite such limitations, this is yet another platform beginning to gain KDE support and in future it may provide a viable free software alternative for trying out the Windows versions of KDE applications. In the meantime it brings a little KDE goodness to even more new users.