---
title: "Repositioning the KDE Brand"
date:    2009-11-24
authors:
  - "Stuart Jarvis"
slug:    repositioning-kde-brand
comments:
  - subject: "\"KDE Software Compilation\""
    date: 2009-11-24
    body: "Meh. This isn't as big a change as I was thinking it would be. That said, \"KDE Software Compilation\" makes for really awkward phrasing (at least in English). "
    author: "yokem55"
  - subject: "Wow, nice"
    date: 2009-11-24
    body: "Wow, this is quite nice, and less scary then I imaged it could be.\r\nI like the idea of positioning KDE as community, so changing the programs and modules into \"KDE something\" seams to make sense.\r\n\r\nI do have to get used to the term \"KDE Software compilation 4.4\", but that is something to get used to. Just like 'Smiths -> Lays' and 'Raider -> Twix' once seamed weird in the Dutch market. Nowadays the new names sound a lot cooler. :)"
    author: "vdboor"
  - subject: "Oh, yes, we recognize that."
    date: 2009-11-24
    body: "Oh, yes, we recognize that. However, it actually isn't really meant to be used a lot. It's mostly for the release announcement purpose. In most other cases you are talking about a specific app, the workspace or the platform. And if you need to mention the release you can say 'our latest release', call it 4.4 etc.\r\n\r\nWe didn't want to re-introduce a new brand here because frankly, it's just a bunch of apps which happen to release together. Apps which aren't part of the release schedule are just as important, and by calling the release 'software compilation' we make clear what it is (and what not)."
    author: "jospoortvliet"
  - subject: "About the Software Compilation..."
    date: 2009-11-24
    body: "You shouldn't need to use \"KDE Software Compilation\" much. It's really just there at all because we happen to release a whole load of stuff together (formerly KDE 4.3.3) and we need a name for that for release announcements and such. If someone sees your computer screen and wants to know what you're running then the answer is probably (KDE) Plasma Desktop/Netbook or possibly the name of one of the apps if that's what they're looking at.\r\n\r\nAs for the size of the change - well, we didn't want to ditch \"KDE\", but rather define it properly and strengthen it. We also didn't want to make more work for us and everyone else than was necessary. Even these changes are going to take a lot of time and effort to implement (KDE websites, About dialog in the apps, getting the press and our own community to understand)."
    author: "Stuart Jarvis"
  - subject: "How should it be abbreviated?"
    date: 2009-11-24
    body: "Currently I see I often see KDE X.X  used as an easy way to identify software versions.\r\n\r\nDudeA: What version of KSnapshot do you have?\r\nDudeB: Iunno. The one from KDE4.3.\r\n\r\nWhat do you recommend be used instead? KSC4.4? KDESC4.4? Or is KDE4.4 still acceptable in informal contexts like these?\r\n\r\n"
    author: "Parker Coates"
  - subject: "KDE Software Compilation"
    date: 2009-11-24
    body: "I think the name KDE Software Compilation X.Y.Z is kind of misconducting because *a* software compilation can contain multiple different versions (e.g. think of kdelibs 4.4, Plasma 4.3, Okular from 4.2, KOffice 2.1), even more the compilation can be platform dependent. This is kind of weird and does not really makes clear what we want to deliver (we have a software platform and applications of different versions). Also I kind of feel a bit uncomfortable if I speak about KDE as a product - I don't work for any company but what I do work on is free software. So the question is: do we really need to fall back to so much company marketing newspeak?"
    author: "pspendrin"
  - subject: "KDE SC <version>"
    date: 2009-11-24
    body: "That's what I use. I would not use KDE [version] because it would re-instate the ambiguity."
    author: "einar"
  - subject: "What about 'KDE Software Collection'?"
    date: 2009-11-24
    body: "Just that the word 'Compilation' sounds like it is something to do with compiling code, as opposed to assembling a collection of software applications and components for release. Or 'Compendium'?"
    author: "Richard Dale"
  - subject: "I agree with most of this."
    date: 2009-11-24
    body: "Good work. Even \"KDE Software Compilation\", which initially seems clunky, makes sense given the context. \"KDE Plasma Desktop\" is an attractive and sensible name.\r\n\r\nBut (you knew there was going to be a 'but'):\r\nI really don't like the \"put either KDE or a K in the application name\" policy, especially because it implicitly encourages people to do the latter to avoid having to do the former. This leads to names which are either dry, technical, and unfriendly (KImageEditor), needlessly obscure (Okular), or just plain goofy (Rekonq, Kamoso). I'm not debating application authors' right to name their applications however the hell they want, but we should have policies which encourage them to choose good ones. The K-in-the-name can be done artfully sometimes (Krita, Kate), and sometimes just sticking a K in front works well enough (KTorrent), but these seem like the exception rather than the rule. In cases where the application's KDE-ness isn't already part of the name, I think it should just be left out. That information can be conveyed through other means. Neither \"KDE Dolphin\" nor \"KFileManager\" sound very attractive, and it also exacerbates the \"so you can only use it in KDE?\" problem you guys are trying to solve. The foremost priority should be attractive and descriptive names; a 'k' in the name should only be seen as icing on the cake, and should only be done when it doesn't come at the cost of attractiveness and descriptiveness. (For the record, I also support Matthias's Ettrich's idea of adding the application's function as part of the name where it's not immediately obvious, so \"Okular Document Viewer\", \"Dolphin File Manager\" and \"Amarok Music Player\", say, while Konsole could probably just stay \"Konsole\".)\r\n\r\nObviously you guys are the ones in charge, these are just my thoughts with the hope that you will find them convincing."
    author: "illissius"
  - subject: "RE: KDE Software Compilation"
    date: 2009-11-24
    body: "\"software compilation can contain multiple different versions\"\r\n\r\nKDE SC already does contain apps with different version numbers in it; but the SC itself has a consistent numbering as a whole. this is not new.\r\n\r\n\"the compilation can be platform dependent\"\r\n\r\nKDE SC, which is what the epochal x.y.z release are, is not. or at least, the parts that are are not built for a given platform.\r\n\r\nKDE SC is not 'a' compilation, it's *the* KDE SC. :)\r\n\r\n\"This is kind of weird and does not really makes clear what we want to deliver\"\r\n\r\nas noted above already, the SC isn't something we'll be pushing as a brand. it's just a way for us to avoid saying \"KDE 4.4\". we have a \"4.4\" release, and that's certainly going to happen (as well as 4.4, 4.5, etc.). but we want to emphasize the components more with the SC fading into the background a bit as a release engineering detail (from the POV of the audiences we will be talking to) and we definitely want to distance \"KDE\" from \"that huge amount of stuff, including a desktop!, that they release\".\r\n\r\nKDE SC gets us further down that road. at some point, we may not need \"KDE SC\" at all, but for now it's a needed disambiguator. and again, it's not really a brand itself.\r\n\r\n\"Also I kind of feel a bit uncomfortable if I speak about KDE as a product - I don't work for any company but what I do work on is free software. So the question is: do we really need to fall back to so much company marketing newspeak?\"\r\n\r\ni don't agree that companies should have exclusive right to a word that describes exactly what we are producing.\r\n\r\ncarbon dioxide is a product of my respiratory system. it's not a company either. ;)\r\n\r\ntalking about \"products\" is accurate. as a bonus it is verbage people who are used to proprietary software products are used to. the point is to communicate in words that are descriptive, that we know,  that can easily be related to by others and that can be found in literature (yes, including \"how to market..\" type literature) without constantly translating.\r\n\r\nwe are a group very different from a monolithic company, something the word 'product' is not going to change in the least. by contrast, i do think that if we talked about 'management structures' with traditional monolithic company terms we would be heading in a poor direction. \r\n\r\nit's also interesting to see how this was arrived at. it was very consensus based and \"KDE\" in how it was done. it didn't happen fast (this takes time no matter what kind of organization structure you have when it is this size, really) but it did happen in line with how we, KDE, have done things and will continue to do things. at least, imho."
    author: "aseigo"
  - subject: "I always experienced KDE as a community :-)"
    date: 2009-11-24
    body: "Great, I think the general direction is good. I always experienced KDE as a community and it is good to emphasize this. What does not fit for me is the naming in some case. KDE Software Compilation does not sound really like good marketing. I do not have a better idea at the moment, but I would really to suggest to look for something else. May be start a competion for it. \r\nThe other thing is the KDE Plasma Desktop, Netbook ... It seems to long. I would suggest to shorten it to KDE Desktop, KDE Netbook. I know, Plasma is very important, but rather as a technology for programmers. Users do not need to know. Calling it will KDE Plasma Desktop is bit confusing too."
    author: "mark"
  - subject: "Yep,"
    date: 2009-11-24
    body: "good point. I like the sound of \"Collection\" as well."
    author: "Mark Kretschmann"
  - subject: "no such policy"
    date: 2009-11-24
    body: "'\"put either KDE or a K in the application name\" policy'\r\n\r\nthere is no such policy.\r\n\r\nthere was a very clear trend to do so in the past, mostly as a way to keep the namespace clear (so one of our binaries didn't conflict with one from somewhere else) but also as a way to identify. this was very pre-marketing-ourselves-very-clearly, but wasn't a horrible thing.\r\n\r\npeople who got hung up on it were .. well .. i never did understand getting distracted by something as insignificant. :)\r\n\r\nstill, in recent times names like 'plasma', 'dolphin', 'solid', 'phonon' and 'gwenview' are more common and even apps that did things like capitalize a 'k' in an odd place ('amaroK') have since normalized their names nicely.\r\n\r\nthere will continue to be 'k' names, in part because of namespacing but also in part because of culture and habit. no harm, no foul, really. it will remain up to the author(s) to name their work as they want to. sometimes a 'k' name might even make sense (KDevelop being a good example there in my mind)\r\n\r\nas for putting the full 'KDE' as a prefix, that's no different than calling something a 'Toyota Prius' or a 'Microsoft Zune'. most of the time they are referred to as a Prius or a Zune or whatever, but there are times when the umbrella brand is added for clarity or marketing purposes (or pedantry in conversation :). (in the above examples the umbrella brand is also the company's name, but that's not always the case)"
    author: "aseigo"
  - subject: "That was also proposed during"
    date: 2009-11-24
    body: "That was also proposed during the sprint. I don't remember why, but in the end there was more consensus towards \"Compilation\""
    author: "einar"
  - subject: "Yep"
    date: 2009-11-24
    body: "As Luca said, KDE SC 4.4 would be fine, similarly SC 4.4 or even just 4.4 if that's enough in the context. But you can really help us by trying to avoid \"KDE 4.4\" because that just reinforces the KDE is the software (and in particular just a desktop) thing"
    author: "Stuart Jarvis"
  - subject: "Collection, content, suite"
    date: 2009-11-24
    body: "Those were the top three from our many suggestions. In the end we felt suite was too tight (\"this is everything, other stuff is on the outside\") and collection was a bit too loose (\"just a load of stuff we threw together\"). Compilation (for example a music compilation) indicates the idea that we selected some stuff that works well together.\r\n\r\nMarketing speak over ;-)"
    author: "Stuart Jarvis"
  - subject: "ambiguity resolution"
    date: 2009-11-24
    body: "\"I always experienced KDE as a community\"\r\n\r\nsame here\r\n\r\n\"KDE Software Compilation does not sound really like good marketing\"\r\n\r\nit's not a name that will be actively marketed as a strong brand. this is quite intentional. see the above threads on this.\r\n\r\n\"The other thing is the KDE Plasma Desktop, Netbook ... It seems to long. I would suggest to shorten it to KDE Desktop, KDE Netbook.\"\r\n\r\nunfortunately we already have the \"KDE == Desktop\" thing going on, in no small part because the 'D' in 'KDE' was 'Desktop'. we are trying to create perceived separation between our workspace offerings (desktop, netbook, etc) and the app framework and individual applications KDE creates.\r\n\r\nthe reason is that far too often, even today, people assume things like \"Krita probably works only on KDE\" (we get this on the irc channels all the time, a place you'd expect people who might actually know these things to go!). of course, the sentence is broken in a few ways: Krita works great in all kinds of places, and \"KDE\" isn't just a desktop environment.\r\n\r\nto create the needed separation so that people will feel more comfortable using the KDE dev platform (to create software that runs everywhere, not only in KDE workspaces!) and KDE applications outside of a KDE workspace, we're giving our workspaces names.\r\n\r\nwe can't refer to it as 'Desktop' in public (ambiguous) so it would become \"KDE Desktop\" and too often just shortened to \"KDE\" again.\r\n\r\ngiven the historical as well as the going forward ambiguities, a name was needed. one was found. :)\r\n\r\n\"I know, Plasma is very important, but rather as a technology for programmers. Users do not need to know.\"\r\n\r\nand users need to know about KOffice, KDE or any of the other similar names? :) Plasma is an identifier, and though you may have come across it as a technology framework, it's used as an accurate disambiguator from both \"KDE\" and \"those other desktop/netbook/mobile UIs out there\".\r\n\r\n\"Calling it will KDE Plasma Desktop is bit confusing too.\"\r\n\r\nhow so?"
    author: "aseigo"
  - subject: "Also..."
    date: 2009-11-24
    body: "We really don't want you to talk of KDE as a product - KDE is the community, right?\r\n\r\nRe using the 'products' as a term for the things we produce. Well, you could just as well say KDE produces applications, workspaces and a platform if you want to avoid 'product'. But the end result of production is a product. It can sound corporate but it shouldn't be really."
    author: "Stuart Jarvis"
  - subject: "Me did, too"
    date: 2009-11-24
    body: "About \"KDE Plasma Desktop, Netbook\":\r\n\r\nActually it is \"KDE Workspaces\", which contain \"Plasma Desktop\" or \"Plasma Netbook\". KDE Desktop is exactly what was intended to be replaced ;) "
    author: "neverendingo"
  - subject: "I feel your pain..."
    date: 2009-11-24
    body: "It took me ages to get over the Marathon -> Snickers transition. Lays are Walkers over here in the UK (at least I think so - it's the same logo). At least KDE hasn't had different brands in every country..."
    author: "Stuart Jarvis"
  - subject: "Collection, *compilation*, suite"
    date: 2009-11-24
    body: "Not 'content'"
    author: "Stuart Jarvis"
  - subject: "Okay."
    date: 2009-11-24
    body: "I believe you.\r\n\r\nBut the article seems to imply the opposite:\r\n\r\n\"Especially for applications that are not well known as KDE applications and are not easily identified as such by a \"K\" prefix in their name, it is recommended to use \"KDE\" in the product name.\"\r\n\r\nSince we agree, all I suggest is to make this somewhat clearer then. :-)\r\n\r\nSo what is the actual policy here? Don't actually make e.g. \"KDE Dolphin\" be the name of the application, but use that form when talking about it if the name doesn't have a 'k' in it? Which form is going to show up in application launchers? Is it going to be different from the one used in press releases and news & reviews?\r\n\r\nAnd yeah, KDevelop was another example I thought of, but forgot to mention, of names where the K prefix actually works. I seem to notice that it tends to be the K-prefix names which consist of a single word which work well, and it's the ones with multiple words which are clunky, but I'm not sure if this works as a general rule."
    author: "illissius"
  - subject: "\"So what is the actual policy"
    date: 2009-11-24
    body: "\"So what is the actual policy here? Don't actually make e.g. \"KDE Dolphin\" be the name of the application, but use that form when talking about it if the name doesn't have a 'k' in it?\"\r\n\r\nYou got it right. If you talk about a KDE application you CAN refer to it as e.g. KDE Dolphin, but also just Dolphin, or Dolphin built on top of the KDE Platform."
    author: "neverendingo"
  - subject: "Maybe"
    date: 2009-11-24
    body: "Yeah, perhaps the text is a little ambiguous there.\r\n\r\nI see it as:\r\nKDE + App name in launchers - generally, no (not in the KDE workspaces at least or most apps will be KDE something)\r\nKDE + App name on the Dot - again, probably no (we're talking about KDE stuff)\r\nKDE + App name on some other news websites (if not in the context of talking about KDE stuff in general) it might be helpful to link the app with us\r\nKDE + Okular when your Windows using buddy asks you what that cool viewer app you're using is - yes, that would be helpful because then they might not only check out Okular but also remember it's produced by KDE and see what else we have to offer\r\n\r\nApps that have the K prefix tend to be associated with KDE anyway, in some circles, so it's probably less likely to use KDE with those."
    author: "Stuart Jarvis"
  - subject: "> The other thing is the KDE"
    date: 2009-11-25
    body: "> The other thing is the KDE Plasma Desktop, Netbook ... It seems to long. I would suggest to shorten it to KDE Desktop, KDE Netbook. I know, Plasma is very important, but rather as a technology for programmers. Users do not need to know.\r\n\r\nPlasma happens to be the name of the technology, but I think this is quite usable for marketing too.\r\n\r\n\"KDE Plasma allows you to create fluent interfaces\".\r\n\r\nthe word 'plasma' already implies this sort of, and I guess that wasn't a coincidence. What we can end up with is, getting users to demand a \"KDE Plasma\" interface for their phone/tv/mediabox and laptops. :-)"
    author: "vdboor"
  - subject: "Telling people about KDE"
    date: 2009-11-25
    body: "I just realized how much easier this is to tell about KDE to co-workers and other people.\r\n\r\nBefore I told about the Linux desktop thing, and how yes, Linux does GUI's too, and no it's not difficult, and no it even looks nice. Conversations dry up pretty quick that way.\r\n\r\nNow I can tell about how KDE is a cool bunch of people, they make quality software, and empower Linux to be great as well. :-)   ..and if those people start looking they find the same message at the Internet too. Bonus points for us. :-)"
    author: "vdboor"
  - subject: "From behind the bikeshed..."
    date: 2009-11-25
    body: "Congratulations to the KDE marketing team on producing such a well thought out and coherent rebranding plan for KDE that neatly balances logic and emotion. Perhaps the emphasis in the article is on logical consistency but ultimately brands are emotional concepts in the general \u201cmindspace\u201d that serve to short-circuit the effort of too much logical decision making in a world saturated with choice. \r\n\r\nOn that basis, it is particularly good news is that Plasma is prominent as the workspace brand. What better name for a vibrant and animated user interface? I'm sure the passion of the developers can be projected into such a brand.  So no need to be concerned that the term Plasma was going to be publicly deprecated as technological jargon. And the idea of KDE Applications is good, though inevitably the short-hand will be  \u201cKDE Apps\u201d (so why not a \u201cKDE App\u201d logo to provide a visual short-hand  to identify these in any \u201cApp store\u201d to avoid using clumsy phrases as KDE Amarok?). \r\n\r\nWhere I think logic has got the better of emotion is the term KDE Software Composition 4.4 to formally avoid terms like KDE 4.4.  The rationale for this fails to recognise that many great brands are overloaded terms covering both the product and organisation (Coca-Cola, Google, Volkswagen to mention just three) and people automatically deal with the ambiguity without a thought, its always clear from the context. But what people can't get their tongue or head round is something like the BMW Saloon Car 323. It just doesn't work.  Whilst the BMW 323 Saloon Car or the BMW 3-series Saloon Car are just fine, though the short-hand will always be the BMW 323 or BMW 3-series (and I'm appearing stupidly pedantic reminding the reader that the context here is the car not the company). So, as we all know that \u201cout there\u201d it's going to remain KDE 4.4, why not just tweak the branding to the KDE 4.4 Software Compilation  so the long-hand brand is consistent with the short-hand brand,  avoiding the need to \u201ccorrect\u201d anyone (which would seem very petty if ever done publicly)."
    author: "trevorl"
  - subject: "some background on this ..."
    date: 2009-11-25
    body: "The problem with the overloaded brand in the case of KDE is that people do not actually automatically understand the difference between a KDE app and the whole Software Compilation (not Composition.\r\n\r\nYour car analogy is off in that regard, it's not about BMW being a manufacturer and a car brand, it would actually be BMW being the standard for roadways, for tyres, for cars, basically the whole \"environment\" of the car. So you get people to avoid buying a BMW because they think they can't drive it on their Honda Streets anymore. I know, analogies can suck hard. ;) KDE has been the infrastructure (development platform) and the chrome (apps, desktop), and the relationship between them needs to be communicated clearly so it doesn't hurt adoption, especially when thinking of multi-platform use of KDE apps and dev platform.\r\n\r\nAnother aspect where this lack of distinction has hurt is the reception of KDE 4.0. While many applications have been quite good from the beginning (okular, dolphin, to name just two) people started the KDE 4.0 workspace, were disappointed by its lack of maturity and didn't make this distinction between desktop workspace and applications -- it's all \"KDE\" after all.\r\n\r\nThe re-branding of the KDE Software Compilation is there to make clear that it's really about the whole package of individual components (such as Plasma, Okular, Kontact). It also makes it easier to market those applications separately while taking advantage of the well-established and strong KDE umbrella brand.\r\n\r\n"
    author: "sebas"
  - subject: "The end of the Kirchner in-joke."
    date: 2009-11-25
    body: "Personally I always thought why the Argentinian people did not exploit more the K Desktop Environment for joking. You know: the N\u00e9stor Kirchner and Cristina Fern\u00e1ndez government has been known as the \"K Government\". Argentina spoke a lot about the \"K style\", \"K deputies\", \"K senators\", and so. What about giving those jokers a complete and truly amazingly named \"K Desktop Environment\"...? WOW! This joke really went sour when the inner circle of Kirchner and his wife began to be called \"Entorno K\" (the K Environment). So, you only have to add \"Desktop\"!\r\n\r\nNow, this is coming to an end, but it's just in time for argentinian people. Watch this if you can read Spanish: http://www.lanacion.com.ar/nota.asp?nota_id=1087203"
    author: "Alejandro Nova"
  - subject: "Compilation error..."
    date: 2009-11-25
    body: "Thanks for correcting me on Software Compilation - I'll get used to it soon.\r\n\r\nI can't argue with your logic and the intention behind it, particularly the need to create a perceptual separation between the Plasma Workspace and the KDE Apps. If the KDE Software Compilation is more an internal community release concept than a publicly marketed brand, as Aaron Seigo suggests in his very recent blog, then that task will be easier.\r\n\r\nI also agree with your point on the importance of promoting KDE Apps as multi-platform. So perhaps it would make more sense to refer to the KDE Platform as the KDE Framework (shades of Qt Developer Frameworks here) so the phrase \"multi-platform KDE App\" unambiguously refers to the underlying operating systems. It would also avoid the phrase \"multi-platform KDE Platform\" which is the sort of verbal clumsiness the marketing team are trying to eliminate."
    author: "trevorl"
  - subject: "KDE Softwore Collection"
    date: 2009-11-25
    body: "Yes this sounds good to me.\r\nI also like KDE Software Set, \r\nbut I don't like it's abreviation KDE SS.\r\n\r\nAnyway, I gess the discussion is already closed, \r\nlong life to KDE SC !"
    author: "lbayle"
  - subject: "interesting"
    date: 2009-11-25
    body: "i like it and is acurrate to say  \"KDE Software Compilation 4.4.x\" but \"KDE Software Compilation 4.4.1\" eventually become KDE SC 4.4.1 , and then again Kde 4.4.1....(daniell, dan, d).\r\n\r\nkde 4.x.y series it stay with us for a while(hope that, i dont survive another rewriteQT5?).\r\n\r\nmaybe will be Kool do something more agressive for marketing   and confussionless like \"photoshop CS series(9.0)\" \r\n\r\nKDE 4.4   -> KDE SC1  (4.4)\r\nKDE 4.5.1 -> KDE SC2.1 (4.5.1)\r\n\r\ninstead \"KDE Software Compilation 4.4.2\"\r\n\r\n"
    author: "Draconiak"
  - subject: "This is a bit silly..."
    date: 2009-11-25
    body: "I mean I can understand calling the KDE desktop and it's applications together a \"Software Compilation\"... but when talking about KDE to friends I doubt I am EVER going to go to the trouble of putting the SC on the end, particularly when referring to just the environment as opposed to including the apps as well. Do you see my point?\r\n\r\nSurely KDE Platform + KDE Plasma Desktop = \"KDE\"?\r\n\r\nAnd KDE + KDE Applications = KDE Software Compilation ?\r\n\r\nhmm o.O"
    author: "hoppipolla"
  - subject: "see, it's working already."
    date: 2009-11-25
    body: "\"when talking about KDE to friends I doubt I am EVER going to go to the trouble of putting the SC on the end, particularly when referring to just the environment as opposed to including the apps as well\"\r\n\r\nno, in fact you should be talking about the individual pieces. e.g. okular, or the plasma desktop, or the kde dev platform.\r\n\r\nif you want to refer to the \"whole chunk of stuff i got at once that contains all sorts of stuff\" then you can refer to the KDE software compilation.\r\n\r\nwe really want people to be talking about and more aware of KDE as a modular set of software suites.\r\n\r\ninteresting how terms change how we talk about things, no? :)\r\n\r\n\"Surely KDE Platform + KDE Plasma Desktop = \"KDE\"?\"\r\n\r\nnope. there's also okular and several dozen other apps that come in the SC, and many more KDE apps that don't come in the SC. this is why we're changing the name, because it's so confusing. \r\n\r\nyou evidently think \"KDE\" is a desktop environment. it's not. the desktop environment is one thing we make, but only one thing and not even the absolute central thing. the KDE team hasn't really helped people to understand that due to the communication in the past.\r\n\r\nthat's why we're changing things, and using 'KDE' exclusively for the community as an umbrella brand for everything we do."
    author: "aseigo"
  - subject: "People will call the software"
    date: 2009-11-26
    body: "People will call the software KDE forever but never mind :)\r\nBtw I think it hasn't been a problem for any open source project that the community was called the same as the software."
    author: "gd"
  - subject: "So here I am, reading"
    date: 2009-11-26
    body: "So here I am, reading comments on a KDE article. I read this comment of a Dutch guy who spells \"seems\" as \"seams\", and I think - wait, I know this guy who always does that! And it was him... Hi Diederik! o/\r\n\r\nOh man, I love how in this world you keep seeing the same great guys over and over again ;-)"
    author: "sgielen"
  - subject: "Changing how people talk"
    date: 2009-11-26
    body: "Many campaigns have tried to change how people call things and as we see in stadium renaming schemes, you can pay millions to have the stadium called one thing and people will still call it what they always did.\r\n\r\nI just dont see myself saying the whole three word name when three letters has been enough for over a decade.\r\n\r\nWill you say \"I use Mandriva 2010-KDE4.3\" or  \r\n\"I use Mandriva 2010 - KDE Plasma Desktop\" ?\r\nI dont think I ever said or wrote K Desktop Environment before although I always say GNU-Linux when talking wuth tech people to differentiate the kernel from the generic desktop name.\r\n\r\nWith the known Linux distinctions as well as the difference between Free and Gratis and open source/free software, the free software community has proven that they are clueless when it comes to these things and cant think further than their noses.\r\nFor that reason, I trust you folks know what you are doing. The 4.x demanded a leap of faith so that the future is secure for some time to come. It wasnt an easy choice but it was the right one.\r\n\r\nI understand the why you want to do it and it makes sense to some degree but asking people to change habits is hard but asking them to go from three letters to three words seems like an even harder battle.\r\n"
    author: "zeke123"
  - subject: "change it to \"KDE Software Kompilation\""
    date: 2009-11-26
    body: "then you call it KDESK :o) which sort of fits the description of what it should be...."
    author: "barsteward"
  - subject: "lol :-) Hi sjors!"
    date: 2009-11-26
    body: "lol :-) Hi sjors!"
    author: "vdboor"
  - subject: "Branding"
    date: 2009-11-26
    body: "I don't really like the concept of branding. It sounds very stupid to say, you are KDE but DE stands for nothing (oh, DE=Germany of course). \r\n\r\nAs a brand KDE developed from a community project of (potentially) coding software enthusiasts into a project clouded by artifical announcement gibberish which often clashed with reality. Branding became less language neutral. It is quite a bit self-ironic that the branding now says \"KDE is no longer software created by people, but people who create software.\"\r\n\r\n\"The expanded term \"K Desktop Environment\" has become ambiguous and obsolete, probably even misleading. Settling on \"KDE\" as a self-contained term makes it clear that we... providing...applications and platforms... on the desktop, mobile devices, and more.\" - \"It is not a limited effort to solve the problem of having a desktop GUI for Linux anymore.\"\r\n\r\nSo in other words, you give up upon the desktop and become a technology collection. Now, maybe some persons may need this to better justify their KDE involvement in a business environment. In less diplomatic terms it means: We give up on the KDE Linux desktop, mission failed.\r\n\r\nConcerning the new naming convention you probably notice that it is unsystematic. So the next step is to rename Kword as \"KDE Word\" or \"KDE Lettera or Lettera\".\r\n\r\n\"KDE applications can run independently of the KDE workspace and can freely be mixed with applications based on other platforms and toolkits.\"\r\n\r\ngets it wrong. The toolkit fetish is obsolete. If there is a branding problem then that some linux users still believe toolkit homogeinity matters for Desktop experience while on other platforms you just don't care. The real branding problem of KDE application is that they are not thought as independent. \r\n\r\nWhere is the user in all this? Originally the implicit idea was develop for a user scenario vision, and communication was characterized by interaction with users and their expectations. As developers rule (and any user is a potential future later developer or contributor to other projects which form part of the desktop experience) of course non-contributors got less rank. Now the user is completely out of focus and it is \"people who create software\". You wonder if they ever eat their own dogfood.\r\n\r\nMaybe that was the kardinal problem with the KDE4 release cycle. You develop great toolkits and platforms to be used for (later) potential purposes. But no one has a user scenario in mind to which the technology development is instrumental, the solution. Here one <a href=\"http://www.kde-look.org/content/show.php/Kde4+Mockup?content=28476\">early mockup</a> got it right. It is really about a solution to a problem \"Browse the web\", \"mail mary\", not technology and applications per se."
    author: "vinum"
  - subject: "\"kde 4.x.y series it stay"
    date: 2009-11-26
    body: "\"kde 4.x.y series it stay with us for a while(hope that, i dont survive another rewrite QT5?).\"\r\n\r\nDon't worry, any Qt5/KDE SC 5 will be a lot less painful, more like the KDE2 -> KDE3  move was.  Qt won't be changing as much, and we won't have to re-write the desktop again, it will be more like clean-up work.\r\n\r\nI can't speak for the trolls, but last I heard they have no timeframe yet for Qt5, but I suspect that once they have all the Symbian and Maemo support work completed they will want to have a major clean-up to align everything and break a few things in the process."
    author: "odysseus"
  - subject: "Just the be clear, the above"
    date: 2009-11-26
    body: "Just the be clear, the above comment is purely conjecture. There are no Qt 5 plans at this time that we know of, nor has anything real been discussed about this within KDE (not counting beer-induced planning of world domination).\r\n\r\nCheers"
    author: "troy"
  - subject: "Some points"
    date: 2009-11-26
    body: "You've covered a lot of ground (good to see you've thought about it) so I'll try and take your points one at a time:\r\n\r\n\"It sounds very stupid to say, you are KDE but DE stands for nothing\"\r\n- The K has officially stood for nothing for a long time\r\n- There are plenty of organisations that have names that used to stand for something but which have moved away from those because they no longer really represent what they do no longer do:\r\n--3M (a lot more than mining and minerals nowadays)\r\n--AT&T (beyond telephone and telegraph)\r\n--BP (allege that they are beyond petroleum ;-)\r\n--SGI (claim to be more than graphics...)\r\n- It is more stupid imho to pretend that KDE is only produces a desktop environment\r\n\r\n\"We give up on the KDE Linux desktop, mission failed\"\r\n- I don't :-)\r\n- Plasma Desktop is one of our greatest achievements and I personally prefer to have my KDE applications running in my KDE workspace\r\n- The KDE Platform is also pretty darn cool, this helps us recognise that\r\n- There are plenty of people using KDE applications because they are just the best out there (Amarok, K3B for a couple) but who don't want to use our workspaces. Separating the two helps us get this message across\r\n\r\n\"the next step is to rename Kword as \"KDE Word\" or \"KDE Lettera or Lettera\"\"\r\n- That's entirely up to the application teams\r\n- You have to balance the gain from changing a name with the loss from losing a recongnised name. For things like KWord I personally don't think a name change is worthwhile\r\n\r\n\"The toolkit fetish is obsolete. If there is a branding problem then that some linux users still believe toolkit homogeinity matters for Desktop experience while on other platforms you just don't care. The real branding problem of KDE application is that they are not thought as independent.\"\r\n- I don't really understand your point here\r\n- I agree that the problem is that KDE Applications are not thought of as being independent of the workspace - this is a big driver behind the changes we have made\r\n\r\n\"Where is the user in all this?\" (I won't quote your whole paragraph)\r\n- Hopefully they are in a position of having greater clarity about who we are and what we can offer\r\n\r\n\"Now the user is completely out of focus and it is \"people who create software\"\"\r\n- The \"people who create software\" thing is a catchy, one line summary. It is not complete. We debated who is in the KDE community and decided that really it is something that people almost define for themselves. You could be part of KDE if:\r\n-- You contribute code to anything in the KDE SC\r\n-- You contribute code to any free software app using the KDE Platform\r\n-- You contribute documentation, art, how-tos, feedback, promotion efforts, bug reports, comments on dot stories :-) There's probably a lot more\r\n\r\nRe your last paragraph:\r\n- You have to develop the underlying technology before you can use it. If you read the blogs of the people doing this I think you'll see that they have visions of how this translates in to real, usable tools\r\n- One of the things in being an open community is that we talk about things as we're doing them, partly to get more people interested in contributing so things can happen faster. We don't develop stuff in secret and then announce it in a blaze of glory.\r\n\r\n"
    author: "Stuart Jarvis"
  - subject: "Wordy."
    date: 2009-11-26
    body: "I appreciate the value of giving the community a nameable, and more clarified identity to remove the ambiguity, but this seems to be suffering from trying to say too much at once. Why not just  KDE Desktop, KDE Netbook and KDE Software? \r\n\r\nIsn't the extra 'Plasma' and 'Compilation' redundant? There is no KDE Desktop/Netbook environment not based on Plasma - it's technical jargon, like calling Mac OS X \"Mac Aqua OS X\"  (I know Plasma != Oxygen, not really the point). I consider Plasma to be a real jewel in the crown of the KDE desktop but it is just one of many components in the workspace - what relevance does it have to an end user? Why not KDE Kwin Desktop / KDE Kwin Netbook? It's like GNU/Linux all over ;)\r\n\r\nPart of making a name 'catch' is ensuring it (or an abbreviation) rolls off the tongue. It feels very wordy to refer to what has previously been just 'KDE' with 'KDE Software Compilation' (multi-syllable word's ftw!)... and the abbreviation... KDE! If I understand, the reason this is thought to not be a problem is that this name will not be actively marketed to the public - but if that is the case, what is the purpose of rebranding? \r\n\r\nI guess in short my feelings are: \"nice idea, but keep it snappy?\"\r\n\r\nNice work on looking into this however, it's good to see branding being taken seriously.\r\n\r\n"
    author: "dalgaro"
  - subject: "If I had a vote it'd be for"
    date: 2009-11-26
    body: "If I had a vote it'd be for 'KDE Software Release' ...as in the Release of Software by KDE. Then we can abbreviate KDE Release 4.4... KDE r4.4 and it still makes some amount of sense! It's funny, in the writeup and comments there is emphasis about wanting something to convey that the it's not a \"joined\" group of applications (suite too 'tight') but just software that happens to be *released* together. \r\n\r\nDid \"Release\" really not come up as an option? Kind of funny if not.\r\n\r\nIt'll be interesting to see if this sticks both inside/outside the community. I'll do my best to train myself appropriately with electro-shock treatment, and sweeties."
    author: "dalgaro"
  - subject: "The software you start when you log in"
    date: 2009-11-26
    body: "How is the set of software supposed to be called which is started when you log in in KDM? And what should we say if someone asks us what DE we use?\r\n\r\nIt is not just Plasma but it is not the whole whole software compilation either which contains many utilities which whe might not use."
    author: "gd"
  - subject: "Something new comes to mind immediately..."
    date: 2009-11-26
    body: "... all this marketing hogwash in the reasoning of the original article is horrible but, at least, they're involuntarily funny because I can now run around, screaming loudly \"KDE is people!\"... [1] ;->\r\n\r\n[1] http://en.wikipedia.org/wiki/Soylent_Green\r\n"
    author: "Philantrop"
  - subject: "Plasma Desktop"
    date: 2009-11-26
    body: "Software started when you log in: Plasma Desktop (or Plasma Netbook) I would say - sure, you probably have some other apps that start up too, but the workspace is the defining thing that you're actually logging in to.\r\n\r\nSame answer for which DE you're using.\r\n\r\nAs you note, it's not just Plasma (by which you mean the Plasma framework?) but the combination of that, KWin and a whole load of other things that I'm not technically enlightned enough to even know about. And yes, I doubt there's anyone out there who uses everything in the Software Compilation."
    author: "Stuart Jarvis"
  - subject: "Firstly, KDE Desktop is"
    date: 2009-11-26
    body: "Firstly, KDE Desktop is problematic for historical reasons - it has been used already a lot in a poorly defined way and if you want to shorten KDE Desktop, well you're going to call it KDE - you can't shorten it to Desktop because that's far too generic : \"I use Desktop\"\r\n\r\nKDE Plasma Desktop on te other hand can be shortened to \"Plasma Desktop\" and is still a unique entity. In terms of wordiness/pronunciation, Plasma Desktop is four syllables, your KDE DEsktop is five (possibly four if you pronounce KDE as kay-dee)\r\n\r\nAbout \"KDE software\" - that has to be a catch all term for any software KDE produces - otherwise you're saying something is not KDE software because it's not released in the Software Compilation?\r\n\r\nRe the Mac analogy: KDE Plasma Desktop is like saying Apple Mac OSX (well, not really, Mac and Plasma have different meanings) but both can be shortened in simiarl ways: Mac OSX, Plasma Desktop.\r\n\r\nWhy not KDE Kwin Desktop? It could have been. It could have been KDE Crystal Desktop (our codename when we were working on the structure before choosing names). We brainstormed names but there were a few considerations, one is that we felt Plasma sounds quite good and was marketable, another is that the term \"Plasma Netbook\" has already gained a bit of traction and recognition without us even trying to push it - a sign that it could be successful and it's one less thing for us to fight (this thing you've been calling Plasma Netbook - yep, that name is cool with us). We didn't aim particularly to grab a name of one of the workspace technologies for the brand.\r\n\r\nRe purpose of the rebranding: to promote things like Plasma Desktop, Plasma Netbook and the individual KDE Applications (and the Platform too). We will be pushing those. The software compilation not so much - we discussed calling it KDE Foo 4.4 where Foo is something snappy, but then people would probably latch on to that, not talk about Plasma Desktop, Plasma Netbook and the individual KDE Applications so much and we'd jsut have replaced KDE 4.3 with a new brand KDE Foo 4.4 and still be apparently marketing everything as one homogenous unit, just with a new name. That really would have been a pointless exercise.\r\n\r\nYours are all valid questions - hopefully I've answered them."
    author: "Stuart Jarvis"
  - subject: "the user"
    date: 2009-11-27
    body: "Stuart already shared many of the thoughts that sprung into my mind as well, but I'd like to add a bit to this:\r\n\r\n\"Where is the user in all this?\"\r\n\r\nWhere they've always been .. the people who use the software we write, the people many of us keep in mind while writing that software.\r\n\r\n\"Originally the implicit idea was develop for a user scenario vision, and communication was characterized by interaction with users and their expectations.\"\r\n\r\nThat hasn't really changed, and not at all with this announcement. Go look at the Plasma Netbook effort and consider how it was created from the start.\r\n\r\nI do think you are confusing \"how we are going to be using our brands\" with \"here is our marketing strategy\". The marketing and communications direction is a much bigger discussion than the definition of the top level brand names. This is more like part of the glossary than the textbook on our marketing.\r\n\r\nTo be even more clear: we aren't going to be heading around in interviews and what not trumpeting \"KDE is us!\" as if that's the important message we need to get across; we'll be communicating about the technology we create just as we always have been, but hopefully more effectively and clearly. This set of changes and definition just helps us understand where the term \"KDE\" fits into that communication. It is not a change in focus.\r\n\r\n\"Now the user is completely out of focus and it is \"people who create software\".\"\r\n\r\nWe've been people who create software for 13 years now. That's not new. The term \"KDE\" is now reserved for the community and organization and not the products and that's the only real change here. In fact, this is getting the naming into line with the reality for the last many, many years (far before 4.0 even). The idea that this reflects a change in what we've been doing \"down in the trenches\" is completely off-base.\r\n\r\n\"You wonder if they ever eat their own dogfood.\"\r\n\r\nWe do, many of us use a near-continuous (daily, weekly) build of KDE from svn, in fact. So, that question is now answered :)\r\n\r\n\"But no one has a user scenario in mind to which the technology development is instrumental, the solution. \"\r\n\r\nThankfully that isn't true, and as we continue to measure actual results over time this will become increasingly apparent.\r\n\r\n(Not sure what any of this had to do with the actual branding thing, though. :)"
    author: "aseigo"
  - subject: "not that difficult..."
    date: 2009-11-27
    body: "\"I'm logged in the (KDE) Plasma Desktop, made by KDE (a group of people making these cool things)\"\r\n\r\nI actually quite like this: \"Plasma Desktop\" springs more imagination, and things to talk about (like about how everything blends and is fluent) then when you'd just called it \"KDE\". The story quickly ends there."
    author: "vdboor"
  - subject: "My thoughts on the new branding."
    date: 2009-11-27
    body: "Hi. I am new here. My first post:\r\n\r\n\r\nIt took me a while, but I think I get it now.\r\n\r\nSo, the name of the project/community will be KDE and the main name that will be marketed for the desktop environment will be Plasma. \r\n\r\nCompare:\r\nMicrosoft Windows - KDE Plasma\r\n\r\nKDE will be the equivalent of Microsoft and Plasma will be the equivalent of Windows, except that Windows is a series of Operating Systems and Plasma is a series of 'Workspaces' (which means desktop environments).\r\n\r\nThis means that the 'GNOME vs. KDE' debate will now become the 'GNOME vs KDE Plasma' debate.\r\nA conversation could be: \"Which desktop environment do you use? GNOME or Plasma?\"\r\n\r\n\r\nI like this. I think it can be successful and I see why it is useful. (To reflect that KDE is more than a desktop environment and to explain that a lot of KDE software can be used on other desktop environments than Plasma.)\r\n\r\n\r\n\r\nWell, I have a few comments.\r\n\r\n\r\nFirst, I disagree with the following quote:\r\n\r\n\"Sometimes we use the term \"KDE project\" as a weak way to say \"KDE community\". We have grown far beyond the situation where you could refer to KDE as a \u201cproject\u201d. It is not a limited effort to solve the problem of having a desktop GUI for Linux anymore. It has evolved into a thriving community of people continuously working on creating and improving free software for end users based on specific values, ideals and goals.\"\r\n\r\nI don't see any problem with the word project. It does not automatically mean small and limited in scope. I consider the word community to be worse. To me it sounds like a user group or a group of fans, not the group of people who create the software product.\r\n\r\nSo I would suggest to keep the word 'project'. Let KDE be short for 'the KDE project' or 'the KDE community project'.\r\nThe same thing is used by 'the Fedora project' and 'the openSUSE project'. They are both community projects that produce software.\r\n\r\n\r\nSecond, I think it will not be easy to make people forget about the 'Desktop Environment' meaning in KDE. It is a well known term among the general IT public, and often abbreviated to DE. There are also products like LXDE and CDE.\r\nHowever, as I pointed out earlier, it is worth the effort, so let's try it. It will be hard work, but not impossible.\r\n\r\n\r\nThird, it can be a bit confusing to have Plasma as the main brand name for Workspaces and at the same time have Plasma as the name for one of the technologies which make up the KDE Platform. Maybe the technology could be renamed to Plasma Core, or something similar?\r\n\r\n\r\nFourth, the word Desktop is very confusing. It can mean many things.\r\n-Desktop can be used to refer to a certain type of hardware. There are also: server, laptop, notebook, netbook, nettop, smartphone, mobile phone, gaming device etc.\r\n\r\nDesktop in software has other meanings.\r\n-It can refer to the base page you get in the graphical environment of your operating system. \r\n-Desktop can also be used as shorthand for Desktop Environment, which includes most of the graphical facilities around the base page I mentioned earlier.\r\n\r\nSee also http://en.wikipedia.org/wiki/Desktop_metaphor for the second and third meaning.\r\n\r\nFor this reason I consider the name 'Plasma Desktop' very confusing. Since there is also a 'Plasma Netbook' it seems that Desktop is referring to hardware here. But the article seems to suggest it is used in the software meaning. I don't like it.\r\n\r\nIt is interesting to see how Opera Software ASA names its products.\r\nThey call their browser for desktops 'Opera'.\r\nTheir browser for the Nintendo Wii is called 'Opera for Wii'.\r\nFor mobile phones they have two products: 'Opera Mini' and 'Opera Mobile'.\r\n\r\nI would recommend a similar naming for KDE:\r\nKDE Plasma for the main desktop environment, instead of KDE Plasma Desktop.\r\nKDE Plasma Mini for the version for netbooks and other small screen devices, instead of KDE Plasma Netbook.\r\n\r\n\r\nOkay, that was it. My first comment on KDEnews. A bit longer than I expected. Good luck with the new branding!\r\n-- Peter"
    author: "Peter"
  - subject: "Yeah, we thought of that too :-)"
    date: 2009-11-27
    body: "Did I mention we made a long list? And checked it twice...\r\n\r\nKDE Software release - well, it's not the only software we release (also a lot outside the SC) so that's one problem\r\n\r\nYou also suggest the handy shortening to KDE Release 4.4 or even KDE r4.4. So... KDE Release 4.4, that's the 4.4 release of KDE, right? Why not just call it KDE 4.4 then?\r\n\r\nWe also get in to fun sentences such as \"Today KDE releases software release 4.4\" whici is a bit heavy on te word \"release\""
    author: "Stuart Jarvis"
  - subject: "[I thought I'd replied to"
    date: 2009-11-27
    body: "[I thought I'd replied to this already but it seems to have vanished.]\r\n\r\nThanks Stuart. It's clear from your reply that a lot of thought has gone into this and - as you say - there are equally valid arguments against anything I came up with. With that in mind I suspect I may just be suffering from a bout of \"I don't like change\"... It's going to take a bit of getting used to but I figure that since you've all put the effort in coming up with this, it's only fair that I put equal effort into giving it a fair go :)\r\n\r\nThanks again for all the hard work, it is appreciated!"
    author: "dalgaro"
  - subject: "To do with it"
    date: 2009-11-28
    body: "Either you don't believe in the merits of branding as I do and find it overrated or you try to take it serious.\r\n\r\nTaking it serious I noted some things\r\n- KDE = the people and community\r\n- KDE people create a universe of great basic technology for different purposes\r\n- the desktop as such is not the goal anymore, we are beyond that.\r\n\r\nThe obvious problem is a smissing solution-oriented view. That is branding gibberish, too, and meant, our customers, users etc don't care for technology, they care for solutions.\r\n\r\nGoogle Chrome OS has a user conception, the operating system is a browser, towards which it steps forward and for which it makes use of KDE technology but they could as well take Gecko. Even if it is unfinished the plan is clear. KDE4 has no user concept, it is more: here is plasma and fantastic things would be built with it, we call it the Plasma desktop. So as there is no user conception KDE4 retroconverges against KDE3.\r\n\r\nAs an example think of a city in Europe. You can totally destroy a city by warfare and citizens will naturally rebuild the city because what matters is their own vicus in mind. KDE3 is the vicus of KDE users."
    author: "vinum"
  - subject: "\"KDE is no longer software"
    date: 2009-11-28
    body: "\"KDE is no longer software created by people, but people who create software.\"\r\n\r\nIsn't that basically the same thing, but just worded differently?"
    author: "Stephen"
  - subject: "Different"
    date: 2009-11-28
    body: "Oh, there is a real difference.\r\n\r\nTake for example Adobe Photoshop.\r\n\r\nPhotoshop is software created by people.\r\nAdobe is people who create software."
    author: "Peter"
  - subject: "They're not the same case."
    date: 2009-11-28
    body: "They're not the same case. Adobe is an official entity which can decide to develop an application. But the KDE developer community is _defined_ as the people who develop the (official and unofficial) KDE software. Or is there any other definition for the new usage of the term KDE?"
    author: "gd"
  - subject: "I am afraid you missed the point"
    date: 2009-11-29
    body: "Of course the KDE community and the Adobe company are not exactly the same. The point is that both produce software.\r\n\r\nYou can read \"KDE Plasma\" or \"KDE Dolphin\" the same way you read \"Adobe Photoshop\".\r\n\r\nI was not trying to claim that Adobe is an open source developer community."
    author: "Peter"
  - subject: "Okular"
    date: 2009-11-29
    body: "Why do you call Okular's name \"needlessly obscure\"? Isn't it an incident that Eye of Gnome uses a similar metaphor?\r\n\r\nOn a nearly unrelated side-note, the name \"Okular\" (in the meaning of \"eyepiece\") looks like an implicit vote for bug 148527."
    author: "majewsky"
  - subject: "\"I don't see any problem with"
    date: 2009-11-29
    body: "\"I don't see any problem with the word project. It does not automatically mean small and limited in scope. I consider the word community to be worse. To me it sounds like a user group or a group of fans, not the group of people who create the software product.\"\r\n\r\nIf you're not comfortable with the term \"community\" for the people who create KDE software, I think that \"KDE team\" should also be fine."
    author: "majewsky"
  - subject: "They are the same case."
    date: 2009-11-29
    body: "\"Adobe is an official entity which can decide to develop an application. But the KDE developer community is _defined_ as the people who develop the (official and unofficial) KDE software.\"\r\n\r\nThe only real difference between Adobe and KDE, in this regard, is that Adobe has restrictions on who can enter their team. KDE hasn't."
    author: "majewsky"
  - subject: "I like the changes!"
    date: 2009-11-29
    body: "I like the changes a lot! \r\n\r\nI don't know if I'll tell people \"I use KDE SC 4.4\" instead of \"Look, this is KDE 4.4\". But When it becomes unclear what I'm talking about, I can now fallback to \"KDE Software Compilation 4.4\" and know that the dot says the same. \r\n\r\nI assume I'll still say \"I am a KDE user, and this is my KDE Desktop\" (show screenshot). \r\n\r\n... \r\n\r\nBut I just realized that there's a change already: Before reading your article, I'd have said \"This is my KDE\". \r\n\r\nAnd maybe I'll eventually get used to telling people \"This is my KDE Plasma Desktop\". "
    author: "ArneBab"
  - subject: "Adobe style is very bad"
    date: 2009-12-03
    body: "We should not even try to follow the path what Adobe toke with Creative Suite. They will end up to situation where they need to invent again new things. Because now they are on CS4. And few years forward they are on CS5 or CS6. Soon of that they comes to CS10 CS11 and so on.\r\n\r\nAnd right now we have clear history of releases so it is wise to follow it because it is always kept same way what was even very logical. Now we have forth generation desktop, with fourth release cycle coming. It is easy to understand the x.x.+1 updates what are bug fixes and so on.\r\n\r\nKDE SC 4.4 sounds good to me. KDE SC 4.4.1 and so on does same thing.\r\n\r\n "
    author: "Fri13"
  - subject: "Well said. It has been before"
    date: 2009-12-03
    body: "Well said. It has been before difficult to explain what KDE really is. Typically you end up to situation speaking how there are volunteer people and paid people to develop software what is \"KDE\". And then there has be difficulties to explain how some software belongs to KDE (Konqueror, KMail etc) and how some does not but they use KDE technologies and how some does not at all belong but use KDE technologies (Amarok, digiKam, Kdenlive) but same what KDE use (pure Qt apps).\r\n\r\nNow it is all easy."
    author: "Fri13"
  - subject: "Lol"
    date: 2009-12-12
    body: "Wow. Straight into the Abyss.\r\n\r\nI was thinking that KDE was moving towards end-user, not tech specialist. And now I get this: \"KDE Software Compilation\". Do KDE marketers really care about the simplicity of KDE branding?\r\n\r\n\"KDE is no longer software created by people, but people who create software.\" Great metamorphosis. KDE has always been (at least in people's minds) a desktop environment, not a group of programmers. In fact people don't care about them or the community.\r\n\r\nWhat I want to say in the nutshell: such rebranding is really going to create a mixup and strengthen people complexes about Linux unfriendliness towards them."
    author: "skybon"
  - subject: "repositioning kde brand"
    date: 2009-12-15
    body: "footer of this site mentions 'K Desktop Environment': \"\"\"KDE, \"K Desktop Environment\", \"KDE Dot News\", \"got the dot?\" and the KDE Logo are trademarks or registered trademarks of ...\"\"\""
    author: "cyrenity"
  - subject: "Keep up the good work"
    date: 2009-12-21
    body: "I'm writing this from the perspective of a very small company that sells computers with Kubuntu ready-installed and ready-to-use (www.tuxfarm.de).\r\n\r\nI come from a completely different background (journalist, author, translator, etc.) and took up Linux as a hobby. For that reason most people I around me know nothing at all about computers, except where to click to start Word, OpenOffice or Firefox.\r\n\r\nI absolutely support the repositioning of the \"brand(s)\". I was always meaning to read the new book by Jono Bacon, the Ubuntu Community Manager. My experience is that \"The Community\" (whatever it really is) is the most vital component of any internet-based (software) project today.\r\n\r\nIn practical terms, I would even go a step further. I would identify what is unique about certain programmes and push this unique characteristic with all there is.\r\n\r\nAn example: I use K3b. This is a weird enough acronym to stick. And 90% of all people want very clear advice and simple instructions along the lines of:\r\n\r\n\"Hey, how do I burn a CD on Linux?\"\r\n\"Well just click on the icon with the CD and the flames. The application's called K3b. Then click on the icon saying 'New data project'. The rest is drag&drop and automatic\".\r\n\r\nOr:\r\n\r\n\"Hey, do I need Nero for Linux? It's not that expensive.\"\r\n\"No, use K3b. It's the icon with the CD and the flames. It's easy to use, just drag&drop. It's even better than Nero, because...\" (don't know, I've never used Nero).\r\n\r\nQuite a few people use Linux around me (thanks mainly to my efforts :-) ) All they will ever know about KDE is that there's Amarok for MP3s, K3b for the CDs/DVDs and, perhaps, KMail for e-mails.\r\n\r\nYes, and that's mainly because of brands, community (me telling them; and many people using the applications) and usability. Practically everyone I know uses Firefox. They don't know what Mozilla is and they don't know what Open Source really is all about. They just know: \"internet, that's Firefox.\"\r\n\r\nIn that vein I would advocate going even further. A word like \"KPresenter\" simply doesn't really stick (all these \"K-Words\" are awful to pronounce in German). KOffice is not much better. I think animal names are generally quite good. Even better, to my mind, is if you can see what the special characteristics of an application are. As far as I understand (although I haven't tested it a lot), KWord is much better at boxy-typed layout that we know from Scribus or Adobe InDesign. So how about KDE Box Office, or BoxOffice or something like that?\r\n\r\nI don't think there really is a point having two nearly identical applications in the world of Free Software. So if KWord is different from OpenOffice Writer, how about really pushing this vital difference?\r\n\r\nThat brings me to \"boxing matches\" (e.g. Gnome vs. KDE): I think that's nonsense. I use GParted for formatting USB sticks, I use Gimp for pictures, I use Firefox a lot, because so many editing windows in content management systems don't work 100% with Konqueror, although I really like Konqueror for simple sites. I don't think there will be \"all-out winners\". There will be mainstream applications of 2-3 \"flavours\" and niche applications. Just pick and mix.\r\n\r\nI think people should be proud to be working on software that some, or even many, people want to use, that really works well and that practically anyone is able to use. For the rest, time will tell."
    author: "eViesel"
---

KDE has changed over the past 13 years. The application framework has grown, matured and gone cross-platform, as have the applications. Strong growth in our community has created an increasingly diverse and large set of high-quality applications.

In the process, KDE's identity has shifted from being simply a desktop environment to representing a global community that creates a remarkably rich body of free software targeted for use by people everywhere.

<strong><em>KDE is no longer software created by people, but people who create software.</em></strong>

To be able to communicate this clearly in our messaging, it is necessary to reposition the KDE brand so that it reflects the reality. We therefore also need distinct brands for the products we produce.

<h2>Summary</h2>

<ul>
<li>We will use simply "<strong>KDE</strong>" and retire the expansion "K Desktop Environment"</li>
<li>We will use "KDE" exclusively in two meanings:
<ul>
<li>KDE, the community, which creates free software for end users</li>
<li>As an umbrella brand for the technology created by the KDE community</li>
</ul>
</li>
<li>We will use distinct brands for the software that was previously referred to generically as “KDE”:
<ul>
<li>The KDE Workspaces will be separately referred to as <strong>"KDE Plasma Desktop"</strong> and <strong>"KDE Plasma Netbook"</strong></li>
<li>The KDE technologies used for building applications will be referred to as the <strong>"KDE Platform"</strong></li>
<li>The KDE Applications will stay as they are: <strong>"the KDE Applications"</strong></li>
<li>The product we currently have released as "KDE 4.3" is essentially a compilation of our software (Workspaces, Applications and Platform), and thus the next release will be named <strong>"KDE Software Compilation 4.4"</strong></li>
</ul>
</li>
</ul>

<h2>Details</h2>

<h3>KDE: the community</h3>
The strength of KDE is the community. It is where it all starts. It includes the KDE culture, the KDE values and the KDE mission. The community is also what ties us together and gives us an identity. This identity is KDE. So it is natural that the brand "KDE" stands first and foremost for the community.

Sometimes we use the term "KDE project" as a weak way to say "KDE community". We have grown far beyond the situation where you could refer to KDE as a “project”. It is not a limited effort to solve the problem of having a desktop GUI for Linux anymore. It has evolved into a thriving community of people continuously working on creating and improving free software for end users based on specific values, ideals and goals.

The KDE brand transports the values of the KDE community, such as freedom, technical excellence, beauty, pragmatism, portability, open standards, international collaboration, professionalism, respect, and great teamwork.

<h3>KDE: the umbrella brand</h3>
In the context of software KDE acts as an umbrella brand for the software created by the KDE community and software using the KDE platform. This includes the KDE platform itself, the KDE Software Compilation, and all other KDE applications. See it as a hierarchy, with the KDE umbrella brand on top and the other brands below it.

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:568px;"><a href="http://community.kde.org/Promo/Branding/Rebranding_KDE_v1.1.0"><img src="http://dot.kde.org/sites/dot.kde.org/files/brandmap.png" width=568 height=359 /></a><br />Graphical representation of the Brand Map</div>

<h3>Retiring the term "K Desktop Environment"</h3>
The expanded term "K Desktop Environment" has become ambiguous and obsolete, probably even misleading. Settling on "KDE" as a self-contained term makes it clear that we have made the shift from a limited set of software components to a community providing an ecosystem of free software applications and platforms for the end user on the desktop, mobile devices, and more.

The term "K Desktop Environment" can be accurately and completely replaced by "KDE Community", "KDE Platform", "KDE Applications" or specific KDE application names, depending on what is actually meant.

For the desktop itself we will have the clearer name "KDE Workspace". The huge set of KDE applications can be put under the KDE umbrella brand using more specific brands for individual applications or application suites. The confusion about what a desktop environment is and implies can be avoided by using clearer terms in this manner.

<h3>Using distinct brands</h3>
To successfully position "KDE" as the community and as an umbrella brand, we need strong identities for our range of products. A summary of these products and the new brands are set out below.

<h2>KDE Products </h2>
Software created by the KDE community is branded on its own under the umbrella brand of KDE. Use of "KDE" in the product name is optional and depending on the context. Especially for applications that are not well known as KDE applications and are not easily identified as such by a "K" prefix in their name, it is recommended to use "KDE" in the product name.

<h3>KDE Applications</h3>
KDE applications make up the breadth and richness of the KDE universe. They are solutions to specific use cases. KDE applications can run independently of the KDE workspace and can freely be mixed with applications based on other platforms and toolkits. Here are a few examples:

<ul>
<li><strong>Okular</strong> or <strong>KDE Okular</strong></li>
<li><strong>Dolphin</strong> or <strong>KDE Dolphin</strong></li>
<li><strong>Amarok</strong></li>
<li><strong>Krita</strong> or <strong>KDE Krita</strong></li>
<li><strong>KWord</strong></li>
<li><strong>KPresenter</strong></li>
<li><strong>KDevelop</strong></li>
<li><strong>KSysguard</strong></li>
<li><strong>K3B</strong></li>
</ul>

There are a few brands which are used to identify application suites built up from several applications. These can be used just like application names when referring to a set of related applications bundled under the application suite brand. Those brands are sometimes also used to refer to a sub-community. Examples include:

<ul>
<li><strong>KOffice</strong> (KWord, Krita...)</li>
<li><strong>KDE Kontact</strong> or <strong>Kontact</strong> (KMail, KOrganizer, KAddressbook...)</li>
<li><strong>KDE Edu</strong></li>
</ul>

<h3>KDE Workspace</h3>
KDE provides workspaces. These provide the environment for running and managing applications and integrate interaction of applications. The workspaces are designed as generic environment for all kinds of desktop applications, not only applications built on the KDE Platform. They integrate best with applications following the standards used by the KDE Platform. There are different flavors of the workspace to address the needs of specific groups of users or adapt to specific hardware platforms:

<ul>
<li><strong>Plasma Desktop</strong> or <strong>KDE Plasma Desktop</strong>. This is the workspace for desktop computers. It's built on the classical paradigm of a desktop environment.</li>
<li><strong>Plasma Netbook</strong> or <strong>KDE Plasma Netbook</strong>. This is the workspace for computers with a small display, e.g. Netbooks.</li>
<li>Future KDE workspaces tailored to specific devices will follow a similar naming scheme</li>
</ul>

The workspaces include many key components such as KWin, KDM, Plasma library, KSysguard and System Settings. Generally we market the specific workspaces (like Plasma Desktop), explaining they are part of the KDE Workspaces.

<h3>KDE Platform</h3>
KDE software is created on the <strong>KDE Platform</strong>, the base of libraries and services which are needed to run KDE applications. It is used by core and third-party developers to create KDE applications. Especially when including development frameworks and tools it may be referred to as <strong>KDE Development Platform</strong>. It is built on a number of Pillars (Akonadi storage framework, Solid hardware layer, Nepomuk information management, Phonon media framework, Plasma library).

<h2>The KDE Software Compilation</h2>
The KDE community does regular releases of a core set of KDE software products. These releases are called the <strong>KDE Software Compilation</strong> and tagged with a version number, e.g. "The KDE community is releasing KDE Software Compilation 4.4". Applications that make up the KDE Software Compilation may be run independently of each other and additional applications can be added without problems. These applications can be freely mixed with applications using other frameworks and run in whatever desktop environment one may choose.

<h2>Further information</h2>
The full details on KDE branding can be found in the document <a href="http://community.kde.org/Promo/Branding/Rebranding_KDE_v1.1.0">"Repositioning the KDE brand"</a> and there is also a <a href="http://community.kde.org/Promo/Branding/Map">Brand Map</a> on the KDE Community Wiki.

<h2>Implementation</h2>
The structure above has been agreed after over a year of discussion and planning. It has been discussed with the community in various places as well. We know and acknowledge it is different, and takes some getting used to. Some things might seem unnecessary - please believe we thought it through. It is time to move on from the discussion and start implementing. We want to have most of this in place for the 4.4 release of the KDE Software Compilation.

We will use the KDE brand and the other sub-brands actively in our communications, making sure to distinguish between them in a natural way, and start treating the workspace as "just another product". Over the next months we'll be increasingly transitioning to the new terminology and updating our online resources and marketing material to provide a single, coherent branding structure. Of course, as a member of the KDE community, you can help. First of all by using the new terms in your communication, updating sites, documentation and dialogs; secondly by explaining things to others.