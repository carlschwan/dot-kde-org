---
title: "KOffice 2.0 Beta 6 Released"
date:    2009-02-15
authors:
  - "jriddell"
slug:    koffice-20-beta-6-released
comments:
  - subject: ":)"
    date: 2009-02-16
    body: "Looks great, you guys have been going at betas for quite awhile now. I can't wait for the final version, it looks like it will suite my needs as only a high school student. "
    author: "Dekkon"
  - subject: "Saving Flake Shapes"
    date: 2009-02-16
    body: "I'm sure I sound like a broken record by now, but hopefully saving flake shapes in a KWord document works (at least somewhat) now.  To date it has not, which makes it kinda hard to test.  Here's hoping.  :)"
    author: "cirehawk"
  - subject: "Couldn't you at least try it"
    date: 2009-02-16
    body: "Couldn't you at least try it out <b>before</b> you complain? Or do you feel that you have to be negative anyway?"
    author: "sf"
  - subject: "Actually.  I'm not"
    date: 2009-02-16
    body: "Actually.  I'm not complaining.  I have high hopes for KOffice.  I use Mandriva and the latest updates still didn't work.  But maybe that don't have the latest snapshot."
    author: "cirehawk"
  - subject: "maybe"
    date: 2009-02-16
    body: "Well, it doesn't work for me right now, but my KOffice2 packages are 3 weeks old.\r\n\r\nCheck a screenshot of some KWord play :D\r\n\r\nhttp://imagebin.ca/view/xHzQ-7.html"
    author: "jospoortvliet"
  - subject: "Boudewijn Rempt"
    date: 2009-02-16
    body: "Saving and loading shapes works in Krita now -- I am not entirely sure whether those fixes are already in this beta (which is already a bit old, the dot story got held up a little). And Thorsten Zachmann has been really busy implementing it in KWord and KPresenter over the past two weeks. So, while we aren't done yet, we are making progress :-)"
    author: "Boudewijn Rempt"
  - subject: "Soon"
    date: 2009-02-16
    body: "We really expect to be ready for a first release candidate after beta7 -- we have identified a small number of real release blockers, and when those are fixed, we're good to go.\r\n\r\n(For a certain measure of goodness, of course. KOffice 2.0 will resemble the first release of KDE 4 in that it is mainly a milestone where we declare that our platform is stable. There will be a lot of work to make the applications be as cool as we envisioned when started out on this journey.)"
    author: "Boudewijn Rempt"
  - subject: "ps."
    date: 2009-02-16
    body: "Oh -- and what I'd forgotten to say: at Fosdem, Marijn's KOffice presentation was actually given using KPresenter from KOffice 2.0. That's quite a milestone, since it means that KPresenter could be used to create, save, load and give a presentation without any crashes :-)"
    author: "Boudewijn Rempt"
  - subject: "Latest Mandriva snapshot ..."
    date: 2009-02-16
    body: "is Beta 6 but the build has been done on Feb 1st. Either Mandriva is using IPOT, either the Koffice beta 6 is old."
    author: "TheDidouille"
  - subject: "Milestone from years ago repeated?"
    date: 2009-02-16
    body: "That sounds like the same milestone that was reached years ago when David Faure wrote that he had done a presentation in KPresenter..."
    author: "odalman"
  - subject: "The damn .doc and .docx format"
    date: 2009-02-16
    body: "I really hate these two formats, but the truth is that we actually can't work without them. I still can't make Kword to <strong>open .doc and .docx</strong> formats, even with <strong>USE=\"msword\" enabled</strong>.\r\n<em>(I'm compiling it on gentoo from KDE SVN)</em><br /><br />\r\n\r\nBut it's really good product. Congratulations, and waiting for 2.0 Final :P"
    author: "NetCutter"
  - subject: ".doc"
    date: 2009-02-16
    body: "There is a .doc filter and that should really work quite well: it got a nice bit of attention this summer of code. Support for ooxml is not planned. Fortunately, Microsoft is planning support for ODF :-)"
    author: "Boudewijn Rempt"
  - subject: "Indeed"
    date: 2009-02-16
    body: "Indeed, it is very similar. But there isn't much left of the 1.x codebase of KPresenter, so it's not weird that we have reached the same milestone."
    author: "Boudewijn Rempt"
  - subject: "Well beta6 is 10 days old,"
    date: 2009-02-16
    body: "Well beta6 is 10 days old, and packagers get beta earlier, so that explain that."
    author: "cyrille"
  - subject: "If you use gentoo ebuilds,"
    date: 2009-02-16
    body: "If you use gentoo ebuilds, apparently there is a problem with them that makes the doc filter to \"disappear\". And if you are doing \"hand\" compilation, check that you have libwv2 and if you do have it, check the cmake output for a reason why the .doc filter isn't used (as for .docx, well, we don't support it)."
    author: "cyrille"
  - subject: "Saving Flake Shapes"
    date: 2009-02-16
    body: "Thanks for the update Boudewijn.  Very good news and I'm anxiously awaiting completion of this feature.  "
    author: "cirehawk"
  - subject: "What about odf-converter-integrator"
    date: 2009-02-16
    body: "Is there any collaboration to make it possible to install the odf-converter-integrator making it possible to open and save ooxml files?\r\n\r\nhttp://katana.oooninja.com/w/odf-converter-integrator"
    author: "stig"
  - subject: "kubuntu debs available"
    date: 2009-02-16
    body: "I took the liberty of compiling koffice 2 beta 6 for (k)ubuntu 8.10 intrepid ibex, you can find them from my PPA.\r\n\r\nhttps://edge.launchpad.net/~paulo-miguel-dias/+archive/ppa\r\n\r\nPS. i also have a complete review of whats coming so far in amarok 2.1 in svn in my blog (english and portuguese versions)\r\n\r\nhttp://padoca.wordpress.com\r\n\r\nSince i can't figure out how to send news to dot.kde.org :P\r\n\r\nbest regards."
    author: "Paulo Dias"
  - subject: "Very exciting"
    date: 2009-02-18
    body: "Congrats on this milestone.\r\n\r\nNo offense to the main KDE guys, but I'm more looking forward to KOffice 2.0 than anything now.  KDE 4.1 was already good (actually great) enough for me, but I always find OOo too, I don't know, \"flimsy\"? to work comfortably with.  KOffice always just felt more compatible with the way I do things, but the promise of Flake shapes in a suite that functions according to what I call logic is what my dreams are made of.\r\n\r\nI will test for bugs and maybe try writing some docs or something, but mostly I look forward to a stable KO2 with great anticipation.\r\n\r\nKeep up the great work, from an end user and big fan."
    author: "Jerzy Bischoff"
  - subject: "RE:"
    date: 2009-05-16
    body: "It has been really busy implementing it in KWord and KPresenter over the past two weeks. (<a href=\"http://www.mustuniversity.com/\">University online</a>)\r\nAs mentioned above, the list of issues is rapidly shrinking. <a href=\"http://www.mustuniversity.com/Must/PriorLearning.asp\">Prior learning degrees</a> \r\n"
    author: "KevinSmith"
  - subject: "Re:"
    date: 2009-05-20
    body: "I've now uploaded kdelibs, kdepimlibs and kdebase from KDE 4.2.0 (more modules will follow), and also the just released koffice 2.0beta6. <a href=\"http://www.musthighschool.com/Must/DistanceLearning.asp\">Distance Diploma Courses</a> | <a href=\"http://www.musthighschool.com/Must/PriorLearning.asp\">Life Experience Diploma</a>\r\n\r\n"
    author: "KevinSmith"
---
   <p>The KOffice developers have released their sixth beta for KOffice 2.0.
   With this release we start to approach the end of the beta series and
   move towards the Release Candidates. As usual the <a href="http://www.koffice.org/announcements/changelog-2.0-beta6.php">list of changes</a>
   is rather long, but it is obvious that the really large issues are
   starting to dry up. Take a look at the <a href="http://www.koffice.org/announcements/announce-2.0beta6.php">full announcement</a> to find
   out more, or look at the changelog for the details.</p>

<!--break-->
<p>Highlights of the 2.0 beta 6 Release</p>

<p>   As mentioned above, the list of issues is rapidly shrinking. Here are
   a few highlights of this release:</p>
<ul>
<li> Merged cells work much better now in KSpread.</li>
<li> Images can be stored and loaded from files.</li>
<li> Navigation within a document works smoother.</li>
<li> There has been some work on metadata and the undo system in Krita.</li>
<li> Karbon can now save more of the SVG standard.</li>
</ul>

