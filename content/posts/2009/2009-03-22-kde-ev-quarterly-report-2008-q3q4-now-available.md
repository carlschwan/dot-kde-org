---
title: "KDE e.V. Quarterly Report 2008 Q3/Q4 Now Available"
date:    2009-03-22
authors:
  - "dannya"
slug:    kde-ev-quarterly-report-2008-q3q4-now-available
---
The <a href="http://ev.kde.org/reports/ev-quarterly-2008Q3-Q4.pdf">KDE e.V. Quarterly Report</a> is now available for July to December 2008. This document includes reports of the board and the Marketing and System Administration working groups, details of the KDE e.V. activities of the last two quarters of 2008, financial information, and future plans. All long-term KDE contributors are welcome to join the <a href="http://ev.kde.org/getinvolved/members.php">KDE e.V</a>.