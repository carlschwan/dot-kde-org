---
title: "KPhotoAlbum Competition to Create Showcase Video - Win $100"
date:    2009-05-12
authors:
  - "blackie"
slug:    kphotoalbum-competition-create-showcase-video-win-100
---
Today the KPhotoAlbum team has launched a competition to create the coolest showcase video for the new KDE 4 version of KPhotoAlbum. Besides fame and glory participant also has the chance to win $100 for the coolest video.
<!--break-->
Back in 2005 Jesper K. Pedersen (also known as blackie@kde.org to many people) created 4 small videos showing the major features of KPhotoAlbum. Back then recording such a video took a long time, and included among other things audio editing tools, fake VNC servers etc. Today, it is as simple as running recordmydesktop. Lots of feedback has been given on the four videos, and it has made the life much easier for many newcomers to KPhotoAlbum, but these days they are somewhat outdated, thus the competition to develop new ones.

Jesper says: <em>"The $100 is just my way of handing some of the money we got from donations onto other people who participate in open source with other means than code. We've previously run similar competitions for icon developers and translators. The success in open source is much more than just the code, it's the full package, including everything from translations to support. Videos help new people getting started with a particular application. This is in my view an important contribution that we see way to little of."</em>

The competition runs until June 14th, and details can be found <a href="http://www.kphotoalbum.org/news.html#item0059">on the KPhotoAlbum homepage</a>