---
title: "Camp KDE \"Be Free\" Contest Now Open"
date:    2009-11-19
authors:
  - "troy"
slug:    camp-kde-be-free-contest-now-open
comments:
  - subject: "Thanks..."
    date: 2009-11-29
    body: "to the KDE e.V. for doing this!  It sounds like a great opportunity.  I've entered... here's hoping."
    author: "ffejery"
---
Do you have a special story about how you or your organization has used KDE to break free from proprietary software?  If so, enter the Camp KDE "Be Free" Contest and tell us your story!

<a href="http://camp.kde.org/"><img src="http://amarok.kde.org/blog/uploads/campkde2010_logo.png" /></a>

Camp KDE, in conjunction with the KDE e.V., will be flying one lucky individual to Camp KDE 2010 in San Diego. Camp KDE attendees can meet the KDE community, participate in KDE talks, take free Qt developer training (normally not free), and join our hacking and planning sessions.
<!--break-->
To qualify, we (the Camp KDE event organizers) are looking for a 300 word essay which demonstrates how using KDE Software has increased your freedom to get more done with your computers.  At the event the winner will get to present a 5 minute talk about their winning submission, and will meet with KDE writers to document their story.

Send submissions to campkde-organizers@kde.org and be sure to include contact information. Contest closes December 15th. Camp KDE 2010 will be held at the University of California at San Diego (UCSD) from January 15-21st. Registration is free to all attendees, and regional travel sponsorships may be available for those that require it (sponsorship must normally be preapproved by the KDE e.V. and attendees will be reimbursed after confirmation of event participation if preapproved).

Note: while we will gladly accept contest submissions from any part of the world, those from North and South America may take priority due to the intended regional scope of Camp KDE.