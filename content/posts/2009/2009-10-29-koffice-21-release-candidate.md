---
title: "KOffice 2.1 Release Candidate"
date:    2009-10-29
authors:
  - "jriddell"
slug:    koffice-21-release-candidate
comments:
  - subject: "Wrong announcement-link"
    date: 2009-10-29
    body: "The link to the release-announcement at koffice.org is wrong. See the actual announcement at http://www.koffice.org/news/koffice-2-1-release-candidate-1-released/\r\n\r\nOh and by the way: Great work, guys and gals, I really enjoy working with this suite (though I have to admit that this is not my main line of work). So far no bugs from me.\r\n\r\ncu, tom"
    author: "TunaTom"
  - subject: "full features list ?"
    date: 2009-10-29
    body: "Where I can updated features list of 2.1 ?"
    author: "zayed"
  - subject: "We'll prepare one in time for"
    date: 2009-10-29
    body: "We'll prepare one in time for the final release, hopefully with a nice visual guide. I know Lukas Tvrdy is already working on a new features guide for Krita, but it isn't done yet."
    author: "Boudewijn Rempt"
  - subject: "Link fixed, thanks :)."
    date: 2009-10-29
    body: "Link fixed, thanks :)."
    author: "Eike Hein"
  - subject: "collaboration and document sharing"
    date: 2009-10-29
    body: " I just recently read that there's a new version of AbiWord out that includes some interesting feature for collaboration and document sharing.\r\n Is there something similar planned for KOffice ? I'm not interested in these features too much myself but it seems a bit like a must these days.\r\n I do understand that KOffice 2 was an big effort and is a step forward but I was just wondering what's the plan beyond 2.1, for the future of KOffice."
    author: "mcirsta"
  - subject: "We are working on it"
    date: 2009-10-29
    body: "This is indeed in the pipeline.\r\nThere is already a basic version management system in KWord.\r\nWe are introducing a change tracking system in 2.1. It can already track text insertion, text deletion, most text formatting. It can save and load these to/from ODF. However, there is still quite some work to do on this (mainly the accept/reject changes mechanism and UI polishing).\r\nWhen change tracking is more mature, we will move on to higher level collaboration features."
    author: "pstirnweiss"
  - subject: "VCS for change tracking?"
    date: 2009-10-30
    body: "Would it be possible to directly use a VCS for change tracking? \r\n\r\nI'd love to see for example an odt as simply the included xml files, plus a VCS directory (git, mercurial or bazaar) which tracks all changes. \r\n\r\nxml files aren't too hard to version, and all three systems already enable easy collaboration. Synchronizing would simply mean to transparently access the odt contents, pull in the changes from another document and merge them. \r\n\r\nPS: Mercurial is currently in the progress of switching the license from GPLv2 only to GPLv2 or later: http://www.nabble.com/GPLv2+-relicensing-status-update-tt26122129.html"
    author: "ArneBab"
  - subject: "No we cannot"
    date: 2009-10-30
    body: "Change tracking is part of the ODF spec. It has defined structure and tags. This also ensures that changes done with any ODF compatible office suite can be used on others (provided change tracking is supported)."
    author: "pstirnweiss"
  - subject: "good to know"
    date: 2009-10-30
    body: " Well that answers my questions and I'm happy to hear about it. When 2.1 hits final I'll give it a try but the final test will be if my mother will be able to use it :)."
    author: "mcirsta"
  - subject: "Good to know"
    date: 2009-10-30
    body: "Many thanks for the info! \r\n\r\nDo you plan to implement a \"prune the history\" option, so I can send out files without fear that people might read older data? \r\n\r\nMany people in companies simply reuse office documents, and for that they need a simple way to get rid of document history. "
    author: "ArneBab"
  - subject: "\"prune history\""
    date: 2009-10-31
    body: "As far as change tracking is concerned, there are two sides to this question:\r\n- it should be possible to accept/reject each change. This is not yet implemented but is planned. Accepting a change will make it part of the \"base\" document, rejecting will reverse the change. In both cases the change will disappear from the list of changes.\r\n- disabling the saving of changes: Provided we actually want this (isn't the first point enough), there again, two questions apply. First should saving of changes be disabled as a whole or should it be possible to disable saving of individual changes? Second, how should a \"saving disabled\" change be saved: as if it was accepted or as if it was rejected? The decision on all this has not yet been made. The design allows to implement whatever the decision will be. For the moment, all changes are saved."
    author: "pstirnweiss"
  - subject: "KOffice 2.1"
    date: 2009-11-24
    body: "Thank you for the update on KOffice 2.1. It seems that nothing works properly straight out the gate and we hope that people understand that bugs will need to be worked out on this just as in anything else. Can't wait for the final."
    author: "Henry Griffith"
---
The KOffice team is happy to <a href="http://www.koffice.org/news/koffice-2-1-release-candidate-1-released/">the release candidate of KOffice 2.1</a>. As usual, the team worked diligently to remove all release blocker bugs leading up to this candidate. See the <a href="http://www.koffice.org/news/koffice-21rc1-changelog/">full changelog</a> for the details.  This is the last chance to test before the final release of KOffice 2.1. We ask that all of our users who wish to help us make KOffice 2.1 the best it can be try out this pre-release and report any remaining bugs.