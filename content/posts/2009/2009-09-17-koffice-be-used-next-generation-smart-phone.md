---
title: "KOffice To Be Used In Next Generation Smart Phone"
date:    2009-09-17
authors:
  - "nightrose"
slug:    koffice-be-used-next-generation-smart-phone
comments:
  - subject: "great news"
    date: 2009-09-18
    body: "this is really great news, i'm a nokia power user and the major problem is the lack of applications on these phones\r\n\r\ni'm pretty sure that Koffice will provide a competitive advantage"
    author: "teodorul"
  - subject: "Nice work Koffice devs! (Esp. Thomas)"
    date: 2009-09-18
    body: "I must admit I was never a fan of the pre 2.0 incarnations of Koffice, but the 2.x series has turned out great! Superior .doc handling, speed and beauty are the wedges that will get Koffice mass appeal. The work that's already been done on this is stunning!\r\n\r\nMy thanks to all the Koffice devs that are helping build the most versatile, fast, modern and FOSS spirited office suite out there. My hat goes off to you."
    author: "Bugsbane"
  - subject: "KOffice is the logical choice"
    date: 2009-09-18
    body: "KOffice is the logical choice for smartphones: imagine trying to cut down OOo to run on it, then changing its UI for a touch screen... nightmarish!"
    author: "madman"
  - subject: "I Knew it Was Coming"
    date: 2009-09-18
    body: "Well done KOffice ladies and gents. I've always felt that there was a significant place for KOffice in the world. It's lightweight and extensible from a development point of view, unlike Open Office.\r\n\r\n<i>\"Nokia has created a customized GUI based on the Maemo 5 touch screen interface on top of the KOffice core. It has also worked on making the support for MS Office documents more mature.\"</i>\r\n\r\nI like it, and I can remember saying this somewhere in the past. What I would urge Nokia to do, and I would urge you to urge them, is it to make the work reproducible for developers by creating some sane APIs and Qt classes for working with Microsoft Office documents - as well as ODF. Make sure it's usable in Qt. <b>Don't</b> do this work as an island. Handling MS Office formats is really all that has held KOffice back over the years.\r\n\r\nIf Nokia really grasp how important development like this is then they could really clean up."
    author: "segedunum"
  - subject: "nice stuff.."
    date: 2009-09-19
    body: "now for some mobile plasma ... ;)"
    author: "eMPee584"
  - subject: "Re: I Knew it Was Coming"
    date: 2009-09-19
    body: "\"make the work reproducible for developers by creating some sane APIs and Qt classes for working with Microsoft Office documents\"\r\n\r\nAs far as I know:\r\n1. KOffice can open MSO documents through some import filters.\r\n2. KOffice devs plan to move the ODF handling to a generic library (sometimes dubbed KODF).\r\n\r\nPlease note that I am not a KOffice developer."
    author: "majewsky"
  - subject: "Plasma"
    date: 2009-09-19
    body: "Nokia has made it clear that Plasma does not suit their needs for the Maemo interface. (I've not heard any reasons, though.) Still, I'm expecting a Plasma desktop for Maemo to quickly appear after the launch of the N900."
    author: "majewsky"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/kofficemaemo.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kofficemaemot_0.png" /></a></div>
Today Nokia employee Thomas Zander announced in <a href="http://labs.trolltech.com/blogs/2009/09/17/office-viewer-for-maemo5-based-on-koffice">his blog</a> that Nokia will be using KOffice as a base for the office file viewer in Maemo 5. He also sent an <a href="http://lists.kde.org/?l=koffice&m=125319299319078&w=2">email to the KOffice mailing list</a> giving some more details about how this came to be.

"This shows that KOffice has one of the best technical foundations", says Jan Hambrecht, one of the core developers of KOffice. "It is both lightweight, flexible and very fast, which makes it perfect in embedded environments like a smart phone".

Nokia has created a customized GUI based on the Maemo 5 touch screen interface on top of the KOffice core. It has also worked on making the support for MS Office documents more mature. Thomas Zander of Qt Development Framework and <a href="http://kogmbh.com">KO GmbH</a> worked on fixing bugs and enhancing support for MS Office formats.

The first presentation of this work will be at the Maemo Summit in Amsterdam from October 9th to 11th.
<!--break-->
