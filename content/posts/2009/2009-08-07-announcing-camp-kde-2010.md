---
title: "Announcing Camp KDE 2010!"
date:    2009-08-07
authors:
  - "jefferai"
slug:    announcing-camp-kde-2010
comments:
  - subject: "Wuhu"
    date: 2009-08-08
    body: "I was so jealous about the happy guys on Camp KDE 2009 :'( i hope i will be there^^"
    author: "Vamp898"
  - subject: "Jolla!"
    date: 2009-08-10
    body: "(back y'all)"
    author: "illogic-al"
  - subject: "See you in SD :)\nUnless some"
    date: 2009-08-10
    body: "See you in SD :)\r\n\r\nUnless some exam-scheduling disaster strikes, I'll be there."
    author: "troy"
---
We are pleased to announce <a href="http://camp.kde.org">Camp KDE 2010</a>!

Camp KDE 2010 will take place at the University of California San Diego (UCSD) in La Jolla, California, USA from January 15th until January 22nd, 2010. The event is free to all participants.
<!--break-->
UCSD is both our host and a sponsor, and KDE is looking forwards to participation and attendance from the UCSD body of students and faculty.

The schedule is currently slated to include presentations, BoFs, hackathons and a day trip. We are also working to include Qt/KDE training and a career fair, and will post more details as they become available. Needless to say, we are also looking forwards to helping interested UCSD students become involved in our community and start working on KDE!

Further details and a registration system will be posted on the Camp KDE website at http://camp.kde.org as they become available. The site is currently being updated and we hope to have the registration system up within a couple of weeks. Some travel and accommodation subsidies will be available for those in need.

Anyone interested in giving a presentation or sponsoring the event should send an email to the Camp KDE Organisers (campkde-HYPHEN-organizers-AT-kde-DOT-org).

See you in San Diego!