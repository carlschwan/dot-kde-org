---
title: "LinuxQuestions.org: KDE Desktop Environment of the Year 2008"
date:    2009-02-16
authors:
  - "sebas"
slug:    linuxquestionsorg-kde-desktop-environment-year-2008
comments:
  - subject: "Desktop environment of the year!"
    date: 2009-02-16
    body: "Well deserved, though I'm much more appreciative of 4.2 than 4.0. Let's make sure someone sends a notice of the award to Linus Torvalds."
    author: "UzLinux"
  - subject: "Who cares?"
    date: 2009-02-16
    body: "Who cares what desktop Linus uses?"
    author: "Narishma"
  - subject: "Good work"
    date: 2009-02-16
    body: "good work people lets make sure next year we win again."
    author: "pinheiro"
  - subject: "wah"
    date: 2009-02-16
    body: "gedit got more votes than Kate? Seriously, that must be a joke. Oh wait, they list KWrite as a separate entry. Guess that's ok then."
    author: "Jakob Petsovits"
  - subject: "I actually switched to gedit,"
    date: 2009-02-16
    body: "I actually switched to gedit, it has much better support for textmate-like snippets and tons of great plugins. I hope kate will catchup one day."
    author: "patcito"
  - subject: "since kwrite/kate supports"
    date: 2009-02-16
    body: "since kwrite/kate supports the \"vi-input-mode\" there is imho no better graphical text-editor available out there..."
    author: "lukrop"
  - subject: "KDE 3 or KDE 4?"
    date: 2009-02-17
    body: "Lumping KDE 3 and KDE 4 together in one poll is not fair.  It creates an undeserved impression that KDE 4 has earned the accolades of being the desktop environment of the year for 2008, when it is more likely that people voted for KDE 3."
    author: "pkoshevoy"
  - subject: "updating"
    date: 2009-02-17
    body: "Yesterday, after 3 years of using linux I went back to windows, I will most likely be back, but, I wish something could be done, so updating KDE was different.  For example, if you have problems with one program, and need to update it for the bug fixes with KDE, you are left having to download the whole desktop packages you have installed, at least that is what I have to do with the distro i use.  For those of us on dial up speed, that is a 2 to 3 hour download, if not longer.  So for now, windows, where an update to fix a program is only a few mb, is far easier for dial up speed.  Hopefully in the future an update with KDE will mean simply downloading the updated files, and not all the installed KDE programs"
    author: "Alien Healer"
  - subject: "Congrats "
    date: 2009-02-17
    body: "Good job to all devs!"
    author: "Jerzy Bischoff"
  - subject: "I seriously doubt that.  4.2"
    date: 2009-02-17
    body: "I seriously doubt that.  4.2 was what people were waiting for.   (although I switched up at 4.1)"
    author: "Jerzy Bischoff"
  - subject: "OpenSuse"
    date: 2009-02-17
    body: "has incremental \"diff\"-rpms for the standard security updates (so you keep the downloads as small as possible)\r\n\r\nit does not make sense to provide an incremental update from KDE 4.1.3 to 4.2.0 though...\r\n\r\n"
    author: "thomas"
  - subject: "Indeed. Last year we lost,"
    date: 2009-02-17
    body: "Indeed. Last year we lost, due to KDE 4.0/4.1, now 4.2 has clearly convinced people that we're going in the right direction."
    author: "jospoortvliet"
  - subject: "Last year, we won"
    date: 2009-02-17
    body: "Actually, last year KDE \"won\", and by a considerably less tight margin:<br><br>\r\n\r\nhttp://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/desktop-environment-of-the-year-610190/ <br><br>\r\n\r\nI think your assessment is a little optimistic, to be honest :/\r\n\r\n"
    author: "SSJ_GZ"
  - subject: "Desktop Search? "
    date: 2009-02-18
    body: "Awhile ago I had been hearing all about Strigi and some desktop search with Akanodi, anyone know the development of this? It was one of my main features I was looking forward to in KDE 4.\r\n<br><br>\r\nSomething similar to this.\r\nhttp://kde-look.org/content/show.php/Fast+search+(like+Mac+OS+X+spotlight)?content=87140\r\n<br><br>\r\nCongrats on Win, thought I believe we won last year, and the year before that? KDE deserves it. :)"
    author: "Dekkon"
  - subject: "4.2 hasn't been out long enough"
    date: 2009-02-18
    body: "4.2.0 was just released, it hasn't had enough user exposure to earn the title of the best desktop environment for 2008. KDE 4.2 is still playing catchup to KDE 3.5 in terms of stability and usability.\r\n"
    author: "pkoshevoy"
  - subject: "yeah opensuse is what i use,"
    date: 2009-02-18
    body: "yeah opensuse is what i use, but the problems are with 4.2, which is great, there are still little niggly bugs, which need updating to try and fix.  I was spending more time updating KDE than using my computer.  I would just love to see it that when there are files that need updating, it is just those files that can be updated, instead of having to download the full KDE packages.  It would help push linux if that could be done because so many do not have broadband, or have a limited usage."
    author: "Alien Healer"
  - subject: "It wasn't technically out"
    date: 2009-02-18
    body: "It wasn't technically out yet, but I think when the poll was on people clearly saw where and how fast KDE4 was going, which gave them the confidence to keep propping it.  You have to remember that just before Mandriva 2009 even Bodnar was predicting KDE4 wouldn't be supplanting 3 until 4.5!  The growing pains at 4.0 were comfortably and rapidly moving to the past tense when the poll was on, and 4.2 really looked worth the trouble.\r\n\r\nIndeed, it was recognition of the direction the whole KDE movement was going."
    author: "Jerzy Bischoff"
  - subject: "and that matters because ...?"
    date: 2009-02-18
    body: "Let's assume you're right and people were voting thinking about KDE 3. What does it matter?\r\n\r\nHaving KDE 3 and KDE 4 as separate entries would have simply split the vote (see Kate and KWrite: same actual text editing engine, but the split landed it behind gedit) and proven approximately nothing otherwise.\r\n\r\nThe people uptight about the whole 3-and-4 thing are getting seriously tiresome. There's not other word for it. The project has moved on, 4.2 is a great release, KDE 3.5 was great in its own right ... KDE 4 isn't anywhere near to \"killing the project\" as it's pretty evident with 4.2 that the choices made are ones that working out as we hoped and intended, so the future remains bright and cheery.\r\n\r\nWell, except for the idea of having to deal with comments like yours for another year. Some days I think (in humour) that we'll end up offering some sort of closure counseling / group therapy sessions. ;)"
    author: "Aaron Seigo"
  - subject: "Are you using the"
    date: 2009-02-18
    body: "Are you using the KDE4:Factory:Desktop repo? You do not need to update it everytime a new build becomes available, the creation of new packages is in many cases just caused by some other packages being rebuilt. Only some updates are caused by patches from openSUSE devs or (once a month) by a KDE maintenance release (then with increasing revision version number, i.e. x in \"4.2.x\")."
    author: "majewsky"
  - subject: "Where it matters..."
    date: 2009-02-19
    body: "... is easy to tell:\r\n<p>\r\nIt matters where you are even interested in measuring the user satisfaction with KDE4 as opposed to KDE3. Your words only bring about the idea that you are not interested.\r\n<p>\r\nI too do look forward to the bright future, but I currently enjoy and prefer what you call the past now.\r\n\r\nYours,\r\nKay"
    author: "kayonuk"
  - subject: "erm"
    date: 2009-02-19
    body: "Who said that was the point of this poll? You can't criticize it for not having a purpose that you just made up."
    author: "eean"
  - subject: "Well..."
    date: 2009-02-20
    body: "... I didn't criticize the poll. I critized the down putting criticism of Aaron, not the poll. If you want for put discussing the merits of KDE3 vs. KDE4 near an illness, that is bad style and unwarranted. \r\n<p>\r\nObviously the poll was about desktop projects, and there is only one project. And if the KDE project decided to go the route that KDE4 has taken, I have little doubt it was in its best abilties at the time.\r\n<p>\r\nThe point is, can the KDE project learn from the past? Or will KDE5  have to repeat history. Beside the down putting style, the whole point of preventing discussion prevents the learning part for the community.\r\n<p>\r\nYours,\r\nKay"
    author: "kayonuk"
  - subject: "I think 4.2 definately worked"
    date: 2009-02-21
    body: "<p>Like Leonardo da Vinci \"was known to say\" (Thanks <a href=\"http://forum.kde.org/theblackcat-u-89.html\">TheBlackCat</a> for sharing this):</p>\r\n \r\n\r\n<p>\"Life is pretty simple: You do some stuff. Most fails. Some works. You do more of what works. If it works big, others quickly copy it. Then you do something else. The trick is the doing something else.\" </p>\r\n\r\n<p>Well, anyway, many people were happy for kde4.2 and so am I:\r\n<a href=\"http://forum.kde.org/kde-4-2-released-t-28742.html\">http://forum.kde.org/kde-4-2-released-t-28742.html</a></p>\r\n\r\n<p>KDE 4.x has worked for me. And works better the > it gets :-)</p>\r\n"
    author: "tkoski"
  - subject: "Get a better distro."
    date: 2009-02-21
    body: "Get a better distro.\r\n"
    author: "jefferai"
  - subject: "KDE rocks!  Great work devs,"
    date: 2009-02-23
    body: "KDE rocks!  Great work devs, testers, doc writers, including Aaron Seigo, keep it up!"
    author: "Jerzy Bischoff"
  - subject: "The point in KDE 4 was so"
    date: 2009-02-23
    body: "The point in KDE 4 was so that it became easier to program for the desktop... and <i>stay</i> easy. Not like 3.5. So no, I suspect the technologies in KDE 4 will live on for a good, long time - and either get updated themselves as the need arises, or simply use new technologies if these ones become outdated.\r\n\r\nI highly doubt KDE 5 will be another KDE 3 --> KDE 4."
    author: "madman"
---
<img align="right" src="http://dot.kde.org/sites/dot.kde.org/files/desktop-environment-KDE.png" width="120" height="120" alt="" />
<p>Congratulations everybody, we rock! KDE was again able to secure the precious <a href="http://www.linuxquestions.org:80/questions/linux-news-59/2008-linuxquestions.org-members-choice-award-winners-704226/">LinuxQuestions.org desktop environment of the year award</a> for 2008. The prize is a testament to the great and innovative work done on the KDE applications, desktop and in extension the KDE platform. Put another way, it is a testament to a community that based on excellence and technically sound solutions manages to be a beacon for innovation in the Free Software world.</p>
<!--break-->
<p>Similar congratulations apply to the Amarok team, which wins the medal for a smashing championship in the category "Audio Media Player Application" and K3b which wins in the category "Multimedia Utility".</p>