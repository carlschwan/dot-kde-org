---
title: "Booth, Web and Marketing Sprint"
date:    2009-11-20
authors:
  - "Stuart Jarvis"
slug:    booth-web-and-marketing-sprint
comments:
  - subject: "Wow"
    date: 2009-11-20
    body: "Wow, you've all been really busy!\r\n\r\nI'm excited about the rebranding of the desktop environment - this sounds promising (finally no more stupid 'K' jokes).\r\n\r\nThe new website sounds great, too.\r\n\r\nGood work!"
    author: "Sepp"
  - subject: "Difficult work"
    date: 2009-11-24
    body: "Sounds like a lot of quite difficult work - congrats on working it out! \r\n\r\nI'm a bit anxious/afraid about the possible replacement for the KDE desktop - partly because I already know my favorite: \"KDE is the Key to your Digital Experience\" - that's the one I already use myself :) \r\n\r\nYou already went huge steps towards improving KDEs Public Recognition, and it looks like that pace is continuing. Four years ago I dreamed to see KDE in newspapers, and that dream already came true this year! \r\n\r\nMaybe the dream to see people discuss the merits of the new KDE release just as they currently discuss the next iPod isn't that far away... \r\n\r\nPS: I only saw one little typo: \"commuity\"\r\nAnother congrats for careful text checking - normally an online text has far more typos :) \r\n\r\nPPS: And I still like kApps :) "
    author: "ArneBab"
  - subject: "Doh!"
    date: 2009-11-24
    body: "I thought we had all the typos... Fixed it now, thanks.\r\n\r\nI think you'll be able to still use \"KDE is the Key to your Digital Experience\", possibly with a little tweaking (is -> provides, maybe). I like that actually...\r\n\r\nWe're not going to ban kApps (at least, I don't think so) but we'd like to give people the option to identify with KDE without having to abuse their K button :-)\r\n\r\nAnd yes, the KDE promotion people have been doing some great work for a number of years and I'm very pleased to be part of that team now. But if it wasn't for the great products we'd have nothing worth marketing."
    author: "Stuart Jarvis"
  - subject: "Events"
    date: 2009-11-26
    body: "For many people these type of events are very helpful. They are a chance to meet the people that have an impact on your work life and your company success. Familiarity brings a sense of comfort."
    author: "Henry Griffith"
---
If you read <a href="http://planetkde.org">PlanetKDE</a>, you may have noticed that there was a combined Booth, Web and Marketing Sprint in Stuttgart last weekend. While those blog posts discussed the fun to be had in getting a load of KDE contributors together in one place, the event was really about having some important discussions and getting a lot of serious work done so that our presentation of KDE matches the quality of our products. Read on for details of the many things we got up to.
<!--break-->
<h2>The team</h2>
In these testing economic times, KDE was pleased to bring Dollars, Pounds and non-German Euros to <a href="http://en.wikipedia.org/wiki/Stuttgart">Stuttgart</a>, the home of Porsche. Unfortunately early reports indicate that there was no rise in Porsche sales arising from the event. We were simply too busy to think about cars, getting straight to work at 9am on Friday morning and working through the weekend. We were laying the foundations for a professional KDE booth presence at exhibitions, a coherent marketing strategy around the 4.4 release of the KDE Software Compilation and an easy to use and attractive website presenting the KDE community and its products.

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:500px;">
<a href="http://dot.kde.org/sites/dot.kde.org/files/grouplarge.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/groupsmall_0.jpeg" width="500" height="316" /></a>
</div>
We had attendees from Europe and North America representing the Promo, Web and Booth teams as well as several people with a foot in more than one of the teams and other parts of the KDE community. We mixed new contributors bringing enthusiasm to their first KDE event and seasoned KDE folk able to provide wisdom and experience. From left to right in the photo: (top row) Kenny, Troy, Rainer, Jos, Ingo, Daniel; (front row) Valerie, Lydia, Claudia, Justin, Martin, Eckhart, Stuart, Cornelius, Frederik, Luca, Frank.

<h2>Rebranding</h2>
One major focus of the meeting was to come to an agreement on a rebranding strategy for KDE. For some time we have been trying to refer to KDE as the community, rather than the products we create, but this has been made difficult by lacking a name for the products.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<img src="http://dot.kde.org/sites/dot.kde.org/files/PICT5727_0.JPG" width="240" height="320" /><br />
Brainstorming with Cornelius</div>
<strong>Cornelius</strong> and <strong>Troy</strong> led discussions on this. We had already discussed the key ideas over many months on the mailing lists and agreed to:

<ul>
<li>Move KDE brand from standing for the desktop to standing for the community</li>
<li>Use KDE as an umbrella brand for the technology created by the KDE community</li>
<li>Prevent the misconception that KDE applications are tied to the KDE desktop and vice versa by introducing a new brand for the desktop.</li>
<li>Phase out the term "K Desktop Environment"</li>
</ul>

Full details of the conclusions we reached will be announced shortly in a separate Dot article. The goal is to implement the new brands by the release of the KDE Software Compilation 4.4.

<h2>Identifying with KDE</h2>
Related to the rebranding efforts, we discussed how application authors can identify their applications as KDE applications (without moving back towards the gratuitous use of the letter K). This is particularly but not exclusively targeted at third party developers who may not see themselves as part of the KDE community as a whole but want acknowledge their links with KDE technology. We would like to provide a label authors can use to say that their application is using KDE technology, they feel part of the KDE community, or both. This label could be used on web pages, in the about dialog of the application, or generally in communication about the application.

After a bit of brainstorming and discussion we came up with five suggestions:

<ul>
<li>Made with KDE technology</li>
<li>Powered by KDE</li>
<li>Free Software by KDE</li>
<li>Built on the KDE platform</li>
<li>Part of the KDE family</li>
</ul>

We will gather feedback from application authors by doing a poll on kde-apps.org on these five options. Then we can decide about which one(s) to use and create some graphics, banners, HTML snippets and so on ready to be used by application authors.

<h2>KDE Handbook</h2>
Last year <a href="http://pradeepto.livejournal.com/"><strong>Pradeepto</strong></a> worked with a team in India to create a beautiful booklet for FOSS.in. Unfortunately the sources were lost. The marketing team decided a new booklet had to be developed, consistent with the new branding and of high quality, and during the meeting we created a good structure and wrote most of the content. There unfortunately still is a lot to do - it's a big booklet and we really gained an appreciation for how much work had gone into the first edition. Most pressing are the lack of screenshots and artwork right now. <strong>Jos</strong> led the work on the booklet with assistance from Stuart, Justin, Daniel, Frederik, Lydia and Claudia at the sprint and many other members of the promotion team online. Any more help is welcome!

<h2>Release announcement for 4.4</h2>
Jos arrived at the sprint with a draft 4.4 release announcement and worked mainly with Luca and Stuart on tidying up the text. We developed the main messages, wrote introductions and the main structure and then went through the whole thing again. Release announcements can surprise you - they're not that big, but every sentence has to be just perfect, which takes a lot of time and discussion and is one of those things that is done very effectively by a small group sitting face to face around a few laptops. We also contacted some developers for further information and wrote about the main features. This of course will need constant revision while the 4.4 release is getting closer...

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey">
<img src="http://dot.kde.org/sites/dot.kde.org/files/PICT5723.JPG" width="240" height="320" /><br />
Planning Camp KDE and Akademy</div>
<h2>Dot Editors' meeting</h2>
The <strong>Dot editors</strong> in attendance (Troy, Kenny, Stuart, Lydia and Jos) discussed timing and procedures around the Dot. We went through a number of quick housekeeping topics, like what to do about HTML spam and how long we should wait after an important story is posted before we post another. There have been lots of articles lately, but sometimes timing has not been perfect. This is in part due to communication and infrastructure issues, and we think we found solutions for those. We also discussed some long-term plans for helping the KDE community keep track of important happenings in the KDE codebase. If you think you would like to contribute to the Dot in any way please contact us at dot-editors@kde.org.

<h2>Akademy & Camp KDE marketing</h2>
Claudia joined the Dot editors to discuss how to work with the press at these flagship KDE events. We will develop a press kit, personally invite journalists and make sure they are taken care of during the meetings. Another topic that was discussed was how to improve the attendance and visibility of KDE's two main annual conferences, <a href="http://camp.kde.org/">Camp KDE</a> (Winter in the Americas) and <a href="http://akademy.kde.org/">Akademy</a> (Summer in Europe). This included trying to get more press to cover the events, keeping frequent but timely stories published within the KDE infrastructure, and improving local participation. The <a href="http://dot.kde.org/2009/11/19/camp-kde-be-free-contest-now-open">Camp KDE "Be Free" Contest</a> is one such example of our attempt to keep the Camp KDE news that is being posted on the Dot relevant, and improve the visibility of the event. If you are interested in going to Camp KDE or Akademy, depending on your region, please apply early for KDE e.V. funding if you face financial difficulties in attending. For people coming from outside the local areas (the Americas or Europe), the e.V. has been known to offer supplemental funding to ensure that those people in distant regions can still attend KDE events. And when you visit, tell the world about it! Blog, tweet, dent, anything to get the word out...


<h2>The KDE website</h2>
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<img src="http://dot.kde.org/sites/dot.kde.org/files/webteamsmall.jpeg" width="350" height="183" /><br />
Web team hard at work</div>
Under the able direction of <strong>Frank</strong>, the web team worked on a relaunch of www.kde.org with four main objectives:

<ul>
<li>Complement the new KDE brands in the website structure</li>
<li>Greater visual information and impact and less text on the main page</li>
<li>Show faces from and information about the community: we are KDE</li>
<li>Move lower impact pages to the wikis and make simplify www.kde.org</li>
</ul>

We are working on a prototype at the moment with new structure, content and design and <a href="http://my.opera.com/it-s/blog/"><strong>Eugene</strong></a> was able to send us some great mock-ups as early as Sunday evening. We will present ideas to a broader audience as they are implemented, but the aim is to launch the new site in February to coincide with the 4.4 release. Later we will port the other subdomains to the new structure and layout.

<h2>KDE booths</h2>
A great way for us to raise our profile and reach out to new users and contributors is to have a strong presence at all kinds of events and exhibitions. <strong>Eckhart</strong> led discussions on how to make us as effective as possible at such events. One problem at present is that the booth box is one large unit with some items (such as computers) that can make negotiating international borders difficult. A plan was developed to:

<ul>
<li>Split the booth in to three modules
<ul>
<li>display module without computers - easy to ship and get through customs etc</li>
<li>computer box - all needed in one box</li>
<li>freebies, handouts and merchandise</li>
</ul>
</li>
<li>Develop required materials such as backdrops, cloths, leaflets</li>
<li>Provide documentation for people manning the booths</li>
<li>Have a store of artwork in PDF format to allow local printing</li>
</ul>


<h2>Recruiting new contributors</h2>
As you can see from the above list, we achieved quite a lot, but there is far more to do and for that we need more contributors. Marketing the KDE community does not serve the sole purpose of merely adding additional users for the various KDE Software. The strength of KDE is the community of contributors that helps to create and improve the software we produce, as well as the various pieces of infrastructure that surround the KDE community.

The new contributors brainstorm was put together with the intention of evaluating and improving the various programs we have in place to help convert users into contributors. This could be something as simple as responding to a request for help. We identified a number of new and existing ways that we can help KDE recruit new people. These include:

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<img src="http://dot.kde.org/sites/dot.kde.org/files/PICT5715small.jpeg" width="320" height="240" /><br />
Enjoying some gluhwein</div>
<ul>
<li>Improving promotion of the bug squad events by covering them on the Dot</li>
<li>Helping to recruit mentors for the forum.kde.org Klassroom tutorials</li>
<li>Including contextual information in Dot stories about how to contribute to the software or team discussed in the article</li>
<li>Writing Dot stories that walk a user through the whole process, from outsider to insider, step-by-step</li>
<li>Improve the visibility of the Junior Jobs listing on bugs.kde.org</li>
<li>improve contributor-to-user communication when a user volunteers to help by training the existing contributors</li>
</ul>

<h2>Team building</h2>
Of course, we didn't work <em>all</em> the time. Meeting up in this way not only has benefits for more effective discussion but also in getting to know your fellow contributors. You feel more a part of the community and stronger obligations to follow up on promises if you think you might have to look someone in the eye. You also get to understand each other's personalities so that you can better interpret written communication without the accompanying body language and have a better idea of how to effectively communicate with different people. To this end, we explored a few bars and restaurants in Stuttgart and managed a quick glass of early gluhwein in Stuttgart's Schlossplatz.

<h2>Next steps - can you help?</h2>
The meeting was incredibly useful, and more importantly, energizing. We hoped to get a lot of work done - which we did, but brainstorming with so many creative people leads inevitably to an even larger number of ambitious plans... We are currently working on consolidating all the ideas into a roadmap for the future, to make coordination and cooperation easier. We are using the <a href="http://community.kde.org">KDE community wiki</a> now, which is where we'll be putting the plan up. This of course means it's easy to not only have a look at what the promo team is up to, but also pick up one or two of those tasks and join us in our effort!

<h2>A word of thanks to our sponsors and hosts</h2>
Our hosts for the sprint were <a href="http://www.konsec.com/en/"><strong>KONSEC</strong></a>, a company that provides a compatibility layer to allow organisations dependent on Microsoft Outlook to transparently switch their groupware server to <a href="http://www.kolab.org/">Kolab</a> (which of course works seemlessly with Kontact and allows an organization to migrate many of their computers to Kontact and other KDE software). KONSEC kindly provided us with two meeting rooms, internet access and a supply of caffeine to keep us all alert and working.

<strong>Martin</strong> (member of KONSEC and also of course a KDE legend) gave us an interesting presentation on KONSEC. After the terrorist attacks of 2001, countries became more aware of security and some, such as Germany, wanted to be less dependent on closed source software from other countries. This led to significant investment in both Kolab and <a href="http://userbase.kde.org/Kontact">KDE Kontact</a>. With the German government as a user the system was from the start designed to be highly scalable - able to cope with hundreds of thousands of users. However, many companies and government offices used specific Microsoft Outlook and Exchange features and were not able to migrate all their systems right away.

There are two approaches to interoperability in such a situation - make Kontact talk to Exchange or make Outlook talk to a free software server such as Kolab. The preferable option is to promote the free software server, so the <a href="http://www.konsec.com/en/products/konnektor.html">KONSEC Konnektor</a> allows Outlook clients to connect to Kolab. Once the Kolab server is set up, the organization is free to start using Kontact as a drop-in replacement for Outlook where possible. However, even where organisations do not take advantage of the Kontact features there are the cost savings and increased efficiency associated with using Kolab rather than Exchange. It also makes it easy for an employee who uses KDE software on a personal machine to use Kontact to access the company personal information server.

KONSEC is mostly sales funded and the Konnector is proprietary software as some components are licensed from third parties. The Konnector is only necessary if you want to use Outlook so is only used on proprietary operating systems to interact with proprietary software. Martin gains most of his income working for <a href="http://erfrakon.de/">ERFRAKON</a>, a fully open source consulting company.

We'd like to offer our thanks not only to Martin and KONSEC for hosting us, but also to our other event sponsors. <a href="http://opensource.region-stuttgart.de/"><strong>Opensource Region Stuttgart</strong></a> provided drinks for us on Friday evening to welcome us to the city, <strong>an anonymous sponsor</strong> provided bottled water and other soft drinks to keep us going through the meetings and <a href="http://www.hive01.com"><strong>hive01</strong></a> provided our lunch on Saturday. The <a href="http://ev.kde.org/"><strong>KDE e.V.</strong></a> provided support with travel and accommodation costs that enabled us all to attend.

Last, but by no means least, we must thank the local team: <strong>Eckhart</strong>, <strong>Frank</strong> and <strong>Frederik</strong>. They did a great job in organizing things and generally making us all feel very welcome in Stuttgart.