---
title: "Byfield \"KDE Developers are Headed in a Definite Direction\""
date:    2009-03-31
authors:
  - "sebas"
slug:    byfield-kde-developers-are-headed-definite-direction
comments:
  - subject: "It's a nice article"
    date: 2009-03-31
    body: "And probably all that could be done for the amount of money such an article pays -- but a thorough look at the differences between the way Gnome and KDE are developed really should take into account things like:\r\n\r\n* that KDE has been incrementally developed while still innovating all the way from KDE 2.0 to KDE 3.5.10 -- that's about 8 years.\r\n\r\n* the difference in proportion of volunteers and paid developers on these projects.\r\n\r\n* the difference in community activity on the dot and on footnotes.\r\n\r\n* the difference in composition of the KDE e.V. board and the Gnome board and the Gnome advisory board.\r\n\r\n* the difference in project scope: all applications in the KDE svn repository belong to KDE, while there are many applications, such as GIMP in the Gnome svn repository and Gnome bugzilla that maintain they are _not_ Gnome applications. (This difference is also clearly visible in the way Google handles the Summer of Code -- there is no way an application that is in KDE's svn will get a separate slot in the Summer of Code.)\r\n\r\n* interesting would be to check how much on planet gnome is actually about gnome instead of firefox, openoffice, gimp or vegan food, compared to planet kde. Interesting, too, is that anyone with a KDE svn account can add himself to planet kde, while planet gnome still has gatekeepers -- and many people on planet gnome are not gnome contributors. \r\n\r\n* the composition of the base libraries. Judging from planet gnome, there's been a lot of effort to remove dependencies on Gnome libraries recently.\r\n\r\nLots of scope for a lot of research leading to a really in-depth article. Or a Phd thesis, of course.\r\n\r\n(And finally, there's a simple reason for the large difference between KDE 4 and KDE 3: the change between versions of KDE tends to follow the amount of change between versions of Qt.) "
    author: "Boudewijn Rempt"
  - subject: "Re: It's a nice article"
    date: 2009-03-31
    body: "Actually, Datamation pays quite well. The restricting factors are the general audience and the allowed length.\r\n\r\nHowever, you have given me several ideas for other articles, so thank you very much.\r\n\r\n- Bruce Byfield (\"nanday\")\r\n\r\n"
    author: "nanday"
  - subject: "Nice!"
    date: 2009-04-01
    body: "Back when I was writing articles for money (before I got the tom-fool notion that writing a book would be a good idea, took a lot of time, made me lose all links with editors and never earned as much as 1/3rd Dr Dobbs article), I couldn't justify spending more than four hours on an article (eight for an Dr Dobbs article).\r\n\r\nIn any case, great that I've been able to give you some ideas for more articles. I find them very readable, generally speaking."
    author: "Boudewijn Rempt"
  - subject: "Agree with B.R."
    date: 2009-04-02
    body: "It was a very nice article, with a clear overview of what's really going on.  I know a lot of people obviously like Gnome, so I'm sure what the Gdevs are doing must be good, but Bruce nailed it: the bigger picture was always that the Kdevs were not content to merely come on par with the proprietary crew, but wanted to (bravely) completely reconsider what, in the estimation the people who were contributing their own development (i.e., themselves), a desktop environment should be.  That's the spirit for me.\r\n\r\nBy the way, people criticising how/what other people largely voluntarily contribute utterly flabbergasts me.  If they want people to program what they want programmed, I think they should specify that in the contract they're paying for."
    author: "Jerzy Bischoff"
---
Bruce Byfield <a href="http://itmanagement.earthweb.com/osrc/article.php/12068_3812616_1/GNOME-vs-KDE-Which-Has-the-Evolutionary-Advantage.htm">looks closer</a> at KDE's development process and compares it to our friendly competition at GNOME. Byfield looks at the potential of both platforms in the long run, and asks the question: <em>"... which developmental approach is likely to be most successful in the next few years?"</em> 
In the article, Bruce provides some good insight into the big picture that lies behind the move to KDE 4 technologies and how they are unfolding their potential: <em>"Reviews about KDE are not always positive, but they are about large issues and shifts in the desktop paradigm. Reading them, you cannot help but come away with the impression that KDE developers are headed in a definite direction, even if you disagree with some or all of the details."</em>
<!--break-->
