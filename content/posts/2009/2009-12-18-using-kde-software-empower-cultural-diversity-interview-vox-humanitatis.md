---
title: "Using KDE Software to Empower Cultural Diversity - an Interview With Vox Humanitatis"
date:    2009-12-18
authors:
  - "liquidat"
slug:    using-kde-software-empower-cultural-diversity-interview-vox-humanitatis
comments:
  - subject: "Link error, please fix"
    date: 2009-12-18
    body: "Great article!\r\n\r\nThe link to Ambaradan is wrong. It should be: http://www.voxhumanitatis.org/content/ambaradan-owm2-storage-engine (without the quotation marks)"
    author: "mutlu"
  - subject: "Fixed. The problem actually"
    date: 2009-12-18
    body: "Fixed. The problem actually was a missing \" ;-)"
    author: "jospoortvliet"
  - subject: "The links to"
    date: 2009-12-18
    body: "The links to http://www.voxhumanitatis.org also have problems with quotation marks, could you please fix them? "
    author: "Hans"
  - subject: "fixed\nthx :)"
    date: 2009-12-18
    body: "fixed\r\nthx :)"
    author: "nightrose"
  - subject: "excess doublequote"
    date: 2009-12-19
    body: "and currently there's also one excess doublequote :) -\r\n\"Parley is perfect for this, because we can just create the study data\" since the user interface is separated from the data.\""
    author: "richlv"
  - subject: "Wow"
    date: 2010-01-08
    body: "Now this is what I call a super helpful community!\r\n\r\nRegards,\r\nZachary Fatinez\r\n\r\n"
    author: "Zachary66"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: none"><img src="http://dot.kde.org/sites/dot.kde.org/files/vh-pms.png" width="100" height="133"/></div>
Among the broad selection of software offered in the KDE Software Compilation is <a href="http://edu.kde.org/parley/">Parley</a>, an application for vocabulary training. Recently people from <a href="http://www.voxhumanitatis.org">Vox Humanitatis</a> came in and provided a set of vocabulary data files for Parley for less known languages. This piqued our curiosity, so we did an interview with Sabine Emmy Eller, CCO of Vox Humanitatis.
<!--break-->
<b>Tell us a bit about yourself</b>

In real life I am a translator and consultant for language and international marketing. Since 1994 I have been working as a freelance translator mainly in the technical, hardware and software localisation sectors. My first experience with Open Source was around 2001, and from 2004 on I am actively involved in projects of the Wikimedia Foundation. My focus moved to less resourced cultures in 2005 which in the end led to the creation of Vox Humanitatis working with Bèrto ëd Sèra. Today I am in charge of communication as CCO of Vox Humanitatis.
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/se_limerick_2009_scale.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Sabine_small.jpg" width="200" height="250"/></a><br />Sabine Emmy Eller</div>
<b>What is Vox Humanitatis and what does it strive to accomplish?</b>

<a href="http://www.voxhumanitatis.org">Vox Humanitatis</a> is a not for profit organisation that cares about less resourced cultures. Those are cultures which do not have that much power to produce content embedded into their own culture. German for example is a powerful resourced culture because it has enough members, its own countries, a lot of content produced in that language/culture, etc. But that is not true for the smaller cultures. We are focused on developing opportunities for less resourced cultures, so that they can be perceived by their native bearers as an element of dignity and an opportunity for social promotion.

Starting from maintaining the culture by creating contents in the various languages and localizing software we establish a "virtuous circle": we reinforce cultural diversity, while at the same time spreading "knowledge about the other" and tolerance among different native communities. This even includes communities which do not have an ethnic base, like sign-language communities and the likes.

Of course the main goal of our activities is to help education in and for less resourced cultures so they can play a role again in everyday business. This also aims to improve education in many countries in the world as it is important to take culture into account when creating educational materials.

Of course one main point of our activities is helping education for and within less resourced cultures taking their usage back into everyday business and to allow for a better approach to education in many countries of the world.

Further info can be found on <a href="http://www.voxhumanitatis.org">voxhumanitatis.org</a> and in the <a href="http://www.voxhumanitatis.org/content/scope-and-practical-goals">Scope and Goals</a> section on that site.

<b>How can people support and join the efforts of Vox Humanitatis?</b>

<div style="float: left; padding: 1ex; margin: 1ex; border: none"><img src="http://dot.kde.org/sites/dot.kde.org/files/MusicalBowSmall.gif" width="120" height="89"/></div>
Good question ... there are many ways and all depends on the languages you might work on. We have professional translators who help with the translation of terminology for the mayor languages, but also students who do the easier work. Languages like Udmurt, Inari Saami etc. are mainly worked on by language enthusiasts that try to keep these languages alive. Partly they are mother tongue speakers, partly just enthusiasts. There is so much to do :-)

Currently we need help from people who could do the conversion work from the tables to kvtml, simply because this would mean that I don't have to care about it and can care more about searching for new team members that can help with some of the 7000 languages in the world.

And yes, the work for Vox Humanitatis takes time, but when you then see the first educational material for a language that has approx. 400 speakers, that is a unique feeling.

<b>What is your connection with Parley?</b>

Parley right now is used for vocabulary training, syllabification training and mathematics (multiplication). It is still not well known, but within our organisation people use it on KDE, Gnome and Windows XP without having trouble. The first "testers" are my kids who have laptops with Edubuntu 9.04. They are in third class primary school and many ideas on what to do with Parley come from their needs at school.

In the background, the actual data is stored in Google Spreadsheet, in a second stage the data is going to be maintained with <a href="http://www.voxhumanitatis.org/content/ambaradan-owm2-storage-engine">Ambaradan</a> which will allow for much easier handling and data preparation.

Parley can and will be used from primary school level to university level. Of course the available lessons will grow step by step and I hope we will have Ambaradan there soon, since it will help a lot when it comes to structuring the data for specific lessons (schools etc., depending on the books they use, need the data structured in different ways). Right now it is simply too much work to get this done properly and therefore we concentrate on just integrating terminology and translating it in as many languages as possible.

<div style="float: right; padding: 1ex; margin: 1ex; border: none"><img src="http://dot.kde.org/sites/dot.kde.org/files/Florence.png" width="100" height="119"/></div>
By the way, much of the content created can also be used for KHangman. It is relevant for us that whatever we do serves for more than "just one kind of exercise". Not all languages have many volunteers. For example, look at <a href="http://en.wikipedia.org/wiki/Inari_Sami">Inari Saami</a> which has only 400 speakers (in 2001) - and we are getting content for this language right now. Parley is perfect for this, because we can just create the study data" since the user interface is separated from the data. So: first let people learn their mother tongue and then we can go ahead and start localising software, hopefully getting help from a team of new volunteers!

<b>You said that Parley is used on Windows as well - is the cross-desktop nature of Parley important for your use case?</b>

Actually, I think it is important for the KDE community and Linux as a whole. Most people see Linux and whatever is connected to it as some kind of magic that is hard to understand, they are afraid of an OS-change. But when they use for example Parley or also KHangMan, they don't even know that they deal with something connected with Open Source and Linux. When they later find out what it really means they are astonished and realize that there is nothing strange, which is opening a door to Free Software in general.

<b>What exactly is Ambaradan, and how do you use it together with Parley?</b>

<a href="http://www.voxhumanitatis.org/content/ambaradan-owm2-storage-engine">Ambaradan</a> is aimed to be a multilingual dictionary and a data storage engine. It is still in early development and needs a bit more work before we bring it to the masses. When it comes to Parley adding the data there has various advantages: we can collaboratively edit the data without the need to care about various tables like we have it now. The same data can easily be linked and related with other data in order to form new content. And once we have our version 1.0 out people can also work offline thanks to the data distribution scheme.

<b>What is the status of KDE in your language - are the translations good? How does it compare to (proprietary/free) alternatives? Does KDE pay enough attention to languages other than English?</b>

<div style="float: left; padding: 1ex; margin: 1ex; border: none"><img src="http://dot.kde.org/sites/dot.kde.org/files/Tribal_Women.png" width="120" height="109"/></div>
There I have a problem to answer with my very own experience. Being German: all is quite fine. Other languages than the big ones: well you give people the possibility to localize into their languages and therefore it is a matter of doing it. This is often a problem. The Vox Humanitatis website features <a href="http://www.voxhumanitatis.org/content/localisation-other-way-round">an article I recently wrote</a> about "Localization the other way round" which highlights the problem.

Technically speaking, the biggest problem for small/rare languages wishing to join the Free Software and Linux train are not issues with KDE, but rather X and the cryptic procedure needed to "add a language to Linux" (compare to a few minutes to make a new keyboard layout for Microsoft). This is where the real bottlenecks are.

<b>How popular/important is KDE/Free Software in your country and how important is it inside Vox Humanitatis?</b>

For Vox Humanitatis Free Software in general is very relevant. KDE turned out to be our personal favourite, but we are not limited to it. Without free software and free content we could not do what we are doing. For many countries in the world having to pay for software and language content is simply not an option. Besides using Free computer applications we are also looking at mobile phone applications. A large share of the market will mainly deal with cheap mobile devices - computers are simply too expensive. This presents another advantage for Parley: the data created for it can be used on mobile phones, because there already is an application (<a href="http://sourceforge.net/projects/mobvoc/">MobVoc</a>) working with it.


<b>What are you missing from Parley right now?</b>

There are several points, but I would not ask the question this way: I don't use Parley for learning lessons, but to create them. So I look at it in a completely different way. We need actual users, students, to get us feedback about real-life use cases. There is only one thing that IMHO would be great: When you open a lesson it should be possible to give instructions on how to deal with it. Since Parley can be used also for grammar exercises, mathematics, yes/no questions and answers etc. It would be nice to be able to say "To answer these question please type y for yes and n for no". Or maybe "to be able to work with this exercises in an efficient way you should use the multiple choice option". Of course these instructions can become much longer though I would keep them as short as possible in order to avoid too much text to be translated.

<b>Are you in contact with the developers and if so, were you able to share ideas and feature-requests?</b>

<div style="float: right; padding: 1ex; margin: 1ex; border: none"><img src="http://dot.kde.org/sites/dot.kde.org/files/final.png" width="354" height="53"/></div>

Yes, we are in touch and we agreed to co-operate as much as possible. Of course we share ideas and talk about features that would be nice to see. Everybody really does what he/she can to get things on the way.

<b>Thanks for the interview! It is great to see our products helping others in their efforts to make the world a better place.</b>