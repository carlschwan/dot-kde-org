---
title: "Upcoming KDE SC 4.4 Gains Positive Reviews"
date:    2009-12-14
authors:
  - "jospoortvliet"
slug:    upcoming-kde-sc-44-gains-positive-reviews
comments:
  - subject: "Nice to see kde-promo mature"
    date: 2009-12-15
    body: "Indeed, it's really growing into quite a structured, self-aware and world-aware entity. This kind of \"Write-ups about KDE in the news and blogosphere\" sort of thing (and other marketing stuff) used to be just a post on <a href=\"http://troy-at-kde.livejournal.com/\">Troy's blog</a>...haha, wasn't it once called \"Media Pulse\" or something? You guys have come a long way, and have finally established yourselves as a group, and a distinct subset of KDE. No disrespect to Troy's preliminary efforts, as they were quite insightful, and did help to lay down foundations.\r\n\r\nOn a related note, whatever happened to Wade and his awesome visual mashups that were kind of like \"ln -s\" for the KDE community? I don't recall seeing any posts from him on the Planet for a while...[wanders off to google] \r\n\r\n"
    author: "maniacmusician"
  - subject: "Hard to beat"
    date: 2009-12-15
    body: "KDE 4 has obviously reached the point where it is the challenge for other desktops whether they are comercial or not. This DE is so feature rich, functional and beautiful that I am really wondering if it can get any better.\r\n\r\nI have installed and been testing the latest beta and I am so impressed that I just can't express it in words. \r\nI have only one wish for the final release: Could you guys please do a little video (maybe in ogg format) so that we can see the changes in action? The reffered video links here were very helpful. \r\nSometimes I am a bit lazy to read and I am sure that a lot like me are out there.\r\nWell done and keep up the good work guys - you are simple the best :)"
    author: "Bobby"
  - subject: "Well, at the very least, this"
    date: 2009-12-17
    body: "Well, at the very least, this should be the end of KDE 3.5. Most of the applications have been ported to KDE 4 by now, and KDE SC 4.3 is a stable and fully functional desktop."
    author: "reub2000"
  - subject: "oh yeah..."
    date: 2009-12-18
    body: "videos - count on it. We're working on that, even have contacted some ppl who seem very good at it ;-)"
    author: "jospoortvliet"
---
Over the past week many reviews of the first beta of version 4.4 of our Software Compilation have surfaced. Here are a few of them that we unearthed during some diggin' on the web.
<!--break-->
This <a href="http://polishlinux.org/kde/kde-44-dev-whats-new/">PolishLinux blog entry</a> includes screen shots of the window tab-grouping feature and various other improvements thus giving a first insight into the upcoming KDE software.

Harsh J has an <a href="http://www.harshj.com/2009/11/18/kde-4-4-desktop-an-early-preview/">'early preview'</a> of the KDE 4.4 Software Compilation. Our rebranding message hasn't gotten entirely through yet but it is an extensive review of the Plasma Desktop workspace.

Ars Technica reviewed our beta with an <a href="http://arstechnica.com/open-source/news/2009/12/hands-on-window-management-gets-better-in-kde-sc-44-beta-1.ars">article about the new window management</a>, plasma and the newcomer Blogilo.

Bruce Byfield goes into analytical mode with his excellent article on the beta titled <a href="http://itmanagement.earthweb.com/osrc/article.php/3852391/KDE-44-Beta-Incremental-Doesnt-Mean-Directionless.htm">Incremental Doesn't Mean Directionless</a>.

Apparently our release got <a href="http://digitizor.com/2009/12/07/is-tabbed-windows-going-to-be-the-next-big-thing/">digitizor.com thinking</a> about tabbed window management. Citing a few advantages and disadvantages, the discussion below the article even goes into global warming. Not sure where that came from...

And Ben Kevan blogs about the <a href="http://www.benkevan.com/blog/kde-4-4-solid-auto-mount-enhancements/">auto mount enhancements in Solid</a>. If that's your pet peeve, read it!

Finally, you can find many screencasts showing off what's new by searching for KDE (sc) 4.4 on <a href="http://blip.tv/search?q=kde+4.4">Blip.tv</a> (<a href="http://blip.tv/file/2958586/">try this one</a>), <a href="http://video.google.com/videosearch?q=kde+4.4">Google video</a>, <a href="http://www.youtube.com/results?search_query=kde+4.4">Youtube</a> (<a href="http://www.youtube.com/watch?v=WrcIUkQnYjk">like this one</a>) and many more.

Should be enough to make all of you eager to try and play with it!