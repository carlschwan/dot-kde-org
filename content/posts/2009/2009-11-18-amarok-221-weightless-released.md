---
title: "Amarok 2.2.1 \"Weightless\" released"
date:    2009-11-18
authors:
  - "nightrose"
slug:    amarok-221-weightless-released
comments:
  - subject: "Big thanks"
    date: 2009-11-18
    body: "Sometimes I have to wonder if you devs ever sleep. Man you are as industrious as the devil in hell ;) I really like how Amarok is stepping up and getting better with each release. I just wonder when Kaffeine will follow suit.\r\nOnce more thanks for another release."
    author: "Bobby"
  - subject: "Sleep?"
    date: 2009-11-19
    body: "Now that the secret is out of the bag, I might as well tell the whole story: We don't actually sleep.\r\n\r\nIt started out with a genetic mutation caused by too much CRT radiation back in the day (hacking on my AMIGA all day), then it somehow spread over to the other Amarok developers too.\r\n\r\nIt's a serious condition, but we get a lot done thanks to it. Please keep all this between you and me though. We don't want the competition to learn about it. Thanks :)\r\n"
    author: "Mark Kretschmann"
  - subject: "Great Stuff!"
    date: 2009-11-19
    body: "I love the new release as most of the annoying \"paper cuts\" that were in the previous release have been fixed, especially layout handling.\r\n\r\nBut frankly, the new toolbar design doesn't look good, and probably a lot of people will agree on this. Something needs to be done about the vast empty space, the control buttons look much too \"alone\" there in the centre. Also, the progressbar widgets aren't exactly nicely visible - they blend too much with the background for my liking.\r\n\r\nBut things are improving nevertheless, and I hope some more good stuff will come in the next release."
    author: "aditya_bhatt"
  - subject: "couldn't say it better. for"
    date: 2009-11-19
    body: "couldn't say it better. for me, the previous toolbar was much better than the current one and I wish there were an option to switch to the previous one.\r\n\r\nbeside that, (one of) the best music player available."
    author: "mkraus"
  - subject: "Toolbar"
    date: 2009-11-22
    body: "Personally, I find the \"slim\" toolbar (available from the context menu) preferable to either the old or new defaults. It has distinct buttons and minimal empty space, and makes efficient use of limited vertical screen dimensions."
    author: "nybble41"
  - subject: "Nice, but..."
    date: 2009-11-22
    body: "I like the old toolbar so much that I downgraded after upgrading, after all I am not a fan of podcasts, but for the next releases... Please guys, give us the old toolbar back :("
    author: "sonay"
  - subject: "I'm stil waiting"
    date: 2009-11-23
    body: "I like amarok 1.4 so much.\r\nThis new version ...\r\nPerhaps with some customisations.\r\n\r\nStill missing :\r\n - equalizer\r\n - simple UI\r\n - and many options (present in 1.4)\r\n\r\nOtherwise good job\r\n"
    author: "dstauret"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/amarok_221.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/amarok_221t.png" /></a></div>
Amarok version 2.2.1 has been  the released by the Amarok team. It includes a lot of improvements to podcast handling, much faster collection scanning as well as the ability to automatically update scripts.

Head over to the <a href="http://amarok.kde.org/en/releases/2.2.1">release announcement</a>, <a href="http://amarok.kde.org/wiki/Download">download it</a> and enjoy rediscovering music.
<!--break-->
