---
title: "KDE Reaches 1,000,000 Commits in its Subversion Repository"
date:    2009-07-20
authors:
  - "jefferai"
slug:    kde-reaches-1000000-commits-its-subversion-repository
comments:
  - subject: "And just before anyone calls"
    date: 2009-07-20
    body: "And just before anyone calls Amarok a bunch of posers for saying they're the first KDE project in Git: Please note that what is written here is that Amarok is the first to migrate to Git, not the first to use Git. It is well known that several KDE projects already use it (such as rekonq, gluon and more) :)\r\n\r\nWith that said: Congratulations to Ervin with his fine commit! \"r1000000 akonadi/trunk/KDE/kdepimlibs/akonadi/ (standardactionmanager.cpp standardactionmanager.h): Allow to also attach a selection model for the favorite collections. Still need to be synchronized with the collections selection model.\" - Thank you all for holding back your commits and then fire them all off as we hit r999999 ;)"
    author: "leinir"
  - subject: "Yes..."
    date: 2009-07-20
    body: "It could have read \"the first project to migrate from KDE's Subversion server to KDE's Git solution\" but as Gitorious.org is not finalized as the platform for KDE Git hosting, this may not end up being a true statement. Hence it was shortened. "
    author: "jefferai"
  - subject: "And here's our winner..."
    date: 2009-07-20
    body: "Congratulations to Kevin!\r\n\r\nhttp://lists.kde.org/?l=kde-commits&m=124811211002267&w=2"
    author: "rich"
  - subject: "Hmmm..."
    date: 2009-07-21
    body: "And on the 40th anniversary of landing on the moon, that would be \"One small commit for a Gearhead, one giant leap for Gearkind...\"\r\n\r\nSorry :-)"
    author: "odysseus"
  - subject: "Ridiculous"
    date: 2009-07-22
    body: "What are you celebrating? The number \"1000000\" in itself has absolutely no relation to the amount of code nor to its quality nor to its semantics. It is therefore a completely arbitrary milestone. Only a complete idiot would call this a milestone."
    author: "Username"
  - subject: "Marketing"
    date: 2009-07-22
    body: "It's called marketing -- gives KDE publicity."
    author: "KAMiKAZOW"
---
KDE announced today that the <a href="http://lists.kde.org/?l=kde-commits&m=124811211002267&w=2">one millionth commit</a> has been made to its Subversion-based revision control system.

<em>"This is a wonderful milestone for KDE,"</em> said Cornelius Schumacher, President of the KDE e.V. Board of Directors. <em>"It is the result of years of hard work by a large, diverse, and talented team that has come together from all over the globe to develop one of the largest and most comprehensive software products in the world."</em>

The 500,000th commit took place on January 19th, 2006, and the 750,000th commit 23 months later on December 18th, 2007. In contrast, only nineteen more months were required to reach the 1,000,000 commit milestone.
<!--break-->
<em>"The pace of development within KDE is rapid and healthy, and new developers join us at an astounding rate,"</em> remarked Schumacher. <em>"On average, each month for the past three years, more than twenty new developers have made their first commit."</em>

Schumacher attributes this growth to the strength of both KDE's products and vision. <em>"We have dedicated ourselves to innovation and building a platform well-suited to future needs. This has enabled developers to write amazing software for the platform using a variety of languages. As a result, the response from the community and press to our KDE 4.2 release has been overwhelmingly positive."</em>

Schumacher also wanted to extend thanks to the Subversion team. <em>"Subversion has proven itself to be a reliable and capable system that has served us extremely well for many years. We know of no other public repository that has reached one million commits, and we are proud to serve as a testament to the scalability of the product."</em>

The pace of commits to the Subversion repository may start to slow, however. <em>"The centralized model of Subversion has served us well and works very well for some of our contributors, such as translators. But in other areas the increasing speed of development and the availability of mature distributed version control systems has led the community to think about the current infrastructure. Many developers have found that their development style would be better suited by distributed development tools like Git, Mercurial, and Bazaar. We believe in using the right tool for the job. As such, parts of KDE development will be migrating to Git."</em>

On the same day, the Amarok project (http://amarok.kde.org) became the first KDE project to migrate to Git.