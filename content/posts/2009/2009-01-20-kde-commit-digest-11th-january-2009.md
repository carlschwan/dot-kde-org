---
title: "KDE Commit-Digest for 11th January 2009"
date:    2009-01-20
authors:
  - "dallen"
slug:    kde-commit-digest-11th-january-2009
comments:
  - subject: "Quality"
    date: 2009-01-20
    body: "Still 16 000 unresolved quality issues:\nhttp://www.englishbreakfastnetwork.org/krazy/?component=kde-4.x"
    author: "andre"
  - subject: "Re: Quality"
    date: 2009-01-20
    body: "most of those issues aren't causing defects; but yes, getting rid of them would be great.\n\nthanks to forum.kde.org's \"kourses\", we've seen a large number of patches from new contributors in this last week that fix a good number of them.\n\nyou could help too! =)"
    author: "Aaron Seigo"
  - subject: "Thanks"
    date: 2009-01-20
    body: "Thanks for the digest, and for anyone interested.  Opensuse have started to put up the early stages of 4.3 in the unstable repo"
    author: "R.J."
  - subject: "headsup"
    date: 2009-01-20
    body: "It's worth reading the linked Digest (I always do of course:) as well as the summary posted here, as there is a nice long piece from the developers about Konqueror File Management refining and also from the choqoK dev - the first Persian kde app?\n\ncheers Dansta!"
    author: "Jon"
  - subject: "Re: headsup"
    date: 2009-01-20
    body: "Considering how much pain gathering the articles causes Danny, I'm surprised he doesn't feature them more prominently.  Just a single line after the main paragraph, like:\n\n<summary of this week>\n\n<featured articles: PersonA on topicA; personB on topicB; etc>\n\n<Read the rest of the digest here!>\n\nThere's often a lot of very interesting stuff there and it seems a shame to hide it away like that."
    author: "Anon"
  - subject: "Re: headsup"
    date: 2009-01-20
    body: "Yeah, I hadn't thought of that, but it's a good idea.\nI'll see what I can do to integrate it into Digests in the near future.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: headsup"
    date: 2009-01-20
    body: "Neato, you're my hero :)"
    author: "Anon"
  - subject: "Re: headsup"
    date: 2009-01-20
    body: "Loved the Konqueror/Dolphin text, lots of awesome improvements there!\n\nIt was interesting to read about choqoK too, even though I don't use Twitter. (However, I have a really hard time remembering that name and would've preferred something like Kvitter (Kwitter?) - \"twitter\" in Swedish - but I guess that's just me. ;) It's cool with a name from ancient Persian!)"
    author: "Hans"
  - subject: "Re: headsup"
    date: 2009-01-20
    body: "yes , developer of choqoK is \"mehrdad momeny\" from iran ! he's also the developer of mdic ...\n\nmdic.sourceforge.net\n\nchoqok.ospdev.net"
    author: "Hosein-mec"
  - subject: "Thanks!"
    date: 2009-01-20
    body: "I really appreciate and enjoy these updates.  Thanks for keeping the digest going.\n\nOne small suggestion -- is there any way the updates could be broken up into smaller paragraphs?  It is kind of difficult to read when it's all one big paragraph.\n\nThanks again!"
    author: "Atomopawn"
  - subject: "Re: Thanks!"
    date: 2009-01-20
    body: "Hehe.\nWell, usually there are not so many commits and so many things that need to be covered in the summary paragraph ;)\n\nActually, the reason why it has to be kept in one single block of text is the Dot policy of only having a single paragraph of the story on the front page.\n\nDanny"
    author: "Danny Allen"
  - subject: "Danny strikes back!"
    date: 2009-01-20
    body: "Thank you very much :-) \n\nBy the way, I just love that the feature freeze is over (4.3), means that reading the dot will be interesting again :-P"
    author: "Luis"
  - subject: "kdm question"
    date: 2009-01-20
    body: "does the kdm module in systemsettings support \"administrator mode\"? in 4.2?"
    author: "me"
  - subject: "Re: kdm question"
    date: 2009-01-20
    body: "At least in 4.2RC1 a kde-su pop-up shows up in order to enter the settings."
    author: "Cyril"
  - subject: "Re: kdm question"
    date: 2009-01-20
    body: "good! thanks to the devs!"
    author: "me"
  - subject: "Damn!"
    date: 2009-01-20
    body: "Busy week - it's like reading the Release Notes for a GNOME major release ;)\n\nThanks Danny - your dedication in the face of overwhelming commit-blitzes is much appreciated :)"
    author: "Anon"
  - subject: "Re: Damn!"
    date: 2009-01-20
    body: "Oh, please stop this useless and silly GNOME bashing. GNOME and KDE are both out to make your computer more free to you. Apple and Microsoft are the ones to bash, no? If there wasn't a KDE you and I will be glad there is GNOME, so we don't have to depend on A and M.\n\nThanks. "
    author: "Hans"
  - subject: "The succes of Plasma"
    date: 2009-01-20
    body: "Reading the digest headline, there is a lot going on in Plasma. Plasma is successfull and cool and hot at the same time :-). My only concern is more and more functionality is going the plasmoid route. Is there a clear policy on what should stay as a \"normal\" app and what could/should/must be implemented as a plasmoid? The difference between a plasmoid and a app is beginning to blur in some area's. I see, for example, more and more \"file manager\" functionality going into the folder view plasmoid. Where does it stop? How will a \"normal user\" experience this. Will (s)he understand the difference? Lot's of functionality is both in plasmoids _and_ in apps. Don't get me wrong here: I love plasma and I love KDE, but I really want KDE to be succesfull. There is a limited amount of developers and I see lot's of them doing cool plasma stuff, thereby duplicating app functionality. These resources might be put to better use, allthought it is difficult to steer FOSS developers into doing things less cool, like ironing out bugs and porting efforts. Is this a genuine concern?"
    author: "Fred"
  - subject: "Re: The succes of Plasma"
    date: 2009-01-20
    body: "\"Is this a genuine concern?\"\n\nNot really - most Plasmoids are simply small-ish front-ends to backends that do the heavy lifting.  For example, FolderView, which is a very unusually large and powerful Plasmoid, is still only a few thousand lines, and a huge amount of its functionality comes from kdelibs/ Qt.  Most Plasmoids are a tiny fraction of this - for example, Marco's \"Magnifique\" and his video-playing Plasmoid represent a very small amount of code (and coding time) : Qt4+kdelibs_libplasma makes all this kind of stuff an absolute breeze."
    author: "Anon"
  - subject: "Re: The succes of Plasma"
    date: 2009-01-20
    body: "\"more and more functionality is going the plasmoid route\"\n\nlol; now the problems stem from plasma being too successful. ;)\n\n\"more and more \"file manager\" functionality going into the folder view plasmoid. Where does it stop?\"\n\nyou'll be pleased to know that we've put our foot down in a number of areas when it comes to folder view. e.g. there have been requests for:\n\n* navigating between folders in the folder view (like you do in konq or dolphin)\n* bread crumb nav at the top\n* a complex \"toolbar\" of file management functions\n\nall things that would make folderview more like dolphin, really. but we are very aware that the beauty of folderview is that it isn't dolphin. it's a simple, quick way to get at the contents of a directory. it's a jumping-off point, not a file manager in itself.\n\nthat it has so many useful features is a blessing we get from Qt and KDE libraries. but we are very careful about the scope of these components. they are meant to aid and assist in the usage of full fledged applications rather than replace them.\n\nthe rss readers are not meant to replace akregrator (but they do use the same library for fetching rss feeds! =), the calculator is not meant to become a full scientific/graphing/accounting tool, etc..\n\nwe are in the process of creating a new category of application somewhere between \"desktop shell\" and \"application\" in the process here, and so lines will appear blurry until everyone groks where those lines are."
    author: "Aaron Seigo"
  - subject: "Re: The succes of Plasma"
    date: 2009-01-20
    body: "oh, as we at file management: Then I start a longer file transfer I get in KDE 4.1.96 a notification plasmoid that disapears after some seconds. How do I get this file process notifier back e.g. to stop the file transfer?\n\nBye\n\n  Thorsten\n\nP:S.\nbig THANK YOU to and personal hero of the week is Tom Patiz who ported kdebluetooth4 to bluez4.x this week."
    author: "Thorsten Schnebeck"
  - subject: "Re: The succes of Plasma"
    date: 2009-01-20
    body: "You just need to click on the computer-like icon that appears in the systray. Then you'll get to view the transfer and choose to stop or pause it."
    author: "Luca Beltrame"
  - subject: "Re: The succes of Plasma"
    date: 2009-01-20
    body: "Ahh, thank you :-)"
    author: "Thorsten Schnebeck"
  - subject: "Re: The succes of Plasma"
    date: 2009-01-24
    body: "If this is not discoverable outside of asking on the dot I would posit that for most people it is not discoverable.  \n\nPerhaps the list of long lasting progress dialogs should minimize to said icon only when the user tells it to, but by default stay open."
    author: "MamiyaOtaru"
  - subject: "Re: The succes of Plasma"
    date: 2009-01-20
    body: "Folderview is conceptually identical to the desktop available on all other desktop environment. From what I can see, users don't confuse the desktop with the file manager.\n\nAs for the differences between plasmoids and applications, they are numerous. Plasmoids all live at the bottom of the z-order, only resize proportionally, can be locked into place, all run in the same process (iirc), can live on the panel or other specialized container, etc. As such, they would be good choice for a biff mail notifier, but not so hot for a full featured mail client."
    author: "David Johnson"
  - subject: "Re: The succes of Plasma"
    date: 2009-01-22
    body: "\"Folderview is conceptually identical to the desktop available on all other desktop environment\"\n\nit's very interesting how changing the context even just a little changes people's expectations, though. people are interesting. =)"
    author: "Aaron Seigo"
  - subject: "Nokia PIM Suite?"
    date: 2009-01-20
    body: "I see the presence of a \"/branches/work/nokia-pim-suite\" branche in the digest? Could anybody explain what that is?"
    author: "Fred"
  - subject: "Re: Nokia PIM Suite?"
    date: 2009-01-20
    body: "That's a project by a group of French students (6, I believe) for a university course, with the kdepim team as \"clients\"."
    author: "Jakob Petsovits"
  - subject: "Re: Nokia PIM Suite?"
    date: 2009-01-20
    body: "This is a really cool effort at the Paul Sabatier University, Toulouse, France.\n\nA group of students there is working on Akonadi based PIM applications suitable for Maemo devices.\n\nYou can read about last year's projects and general information about this special universtiy course in an older commit digest here http://www.commit-digest.org/issues/2007-12-23/"
    author: "Kevin Krammer"
  - subject: "Re: Nokia PIM Suite?"
    date: 2009-01-20
    body: "Great now I will have to get me one of those maemo devices :) "
    author: "nuno pinheiro"
  - subject: "Re: Nokia PIM Suite?"
    date: 2009-01-23
    body: "Hopefully we'll get someday... the thing everyone is waiting for...\n\nA KDE (and Linux-running) Nokia PC Suite! ;)"
    author: "Alejandro Nova"
  - subject: "KDE 3.6"
    date: 2009-01-20
    body: "Any news about the release of KDE 3.6??"
    author: "Marcelo Russo"
  - subject: "Re: KDE 3.6"
    date: 2009-01-20
    body: "There is no 3.6"
    author: "whatever noticed"
  - subject: "Re: KDE 3.6"
    date: 2009-01-20
    body: "what about 3.5.11?"
    author: "Marcelo Russo"
  - subject: "Re: KDE 3.6"
    date: 2009-01-20
    body: "unlikely at this point, though always possible if enough changes post 3.5.10 happen and someone oversees the release management."
    author: "Aaron Seigo"
  - subject: "Review of KDE 4,2"
    date: 2009-01-20
    body: "What is your opinion about:\n\nhttp://tuxramblings.wordpress.com/2009/01/19/piece-of-shit-linux-applications-part-2\n"
    author: "Johnnatan Schultz"
  - subject: "Re: Review of KDE 4,2"
    date: 2009-01-20
    body: "My opinion: Not worth the reading"
    author: "Birdy"
  - subject: "Re: Review of KDE 4,2"
    date: 2009-01-20
    body: "I think \"Review of KDE 4,2\" is a rather ill-fitting title for your post :)"
    author: "Anon"
  - subject: "Re: Review of KDE 4,2"
    date: 2009-01-20
    body: "I would hardly call it a \"review\", just a long rant with no or little basis, and even if it had merit, it's lost in the stream of negativity."
    author: "Luca Beltrame"
  - subject: "Re: Review of KDE 4,2"
    date: 2009-01-20
    body: "The typical \"rant blog\" politically incorrect just to get visitors. Nothing new here."
    author: "Vide"
  - subject: "Re: Review of KDE 4,2"
    date: 2009-01-20
    body: "I found the part about 4.0 release very funny :)\nThe article should not be taken seriously, even that it contains some truths, it is so biased and distorced it does not matter at all."
    author: "Iuri Fiedoruk"
  - subject: "Re: Review of KDE 4,2"
    date: 2009-01-20
    body: "Short summary of the article for those who did not care to read it: \n\nforeach (app, RandomApps)\n{\ncout << app << \" is a piece of shit\"\n}\n"
    author: "Michael"
  - subject: "Re: Review of KDE 4,2"
    date: 2009-01-20
    body: "LOL!\n\nI don't know if it's right that I actually find it funny."
    author: "Luis"
  - subject: "Re: Review of KDE 4,2"
    date: 2009-01-21
    body: "That's disingenuous to say.\n\nHe had a lot of good points, though I would say that for nearly every one of his points, you could add \"Use the older version and/or wait until the newer version comes out\".\n\nNew Amarok UI sucks. It will improve, but it sucks now.\nPidgin devs are very resistant to change. Truth.\nKDE4 was a PR disaster. Yep, check.\n\nThat's about it."
    author: "JoeSchmoe"
  - subject: "Re: Review of KDE 4,2"
    date: 2009-01-22
    body: "> New Amarok UI sucks. It will improve, but it sucks now.\n\nyou may think it sucks, a lot of us like it even now. it will certainly improve (already has in svn), but if 2.0's interface rated a \"sucks\" then i hope you're slapping that label on 90%+ of the rest of software out there. that said, rewrites of complex interfaces, esp when its a \"re-imagining\" run into two logistical problems:\n\n* people tend to be very resistant to change, and take out their non-rational response to anything different (a biological imperative, btw) on the source of that change. often if that change is actually good for them long term.\n\n* if you are replacing something highly polished and intricate, your first re-drafting will likely not be nearly as ornate. one can either sit on it indefinitely until it's \"as ornate\", but then one will never release. the way things *get* ornate is through the process of develop-release-use-feedback-repeat. in F/OSS that is not a process internal to a project.\n\n> Pidgin devs are very resistant to change. Truth.\n\ni don't know enough about the pidgin folk to make an educated statement. so.. i won't say anything. :)\n\n> KDE4 was a PR disaster. Yep, check.\n\ni don't call a couple of months of a vocal squealing minority of ill-informed individuals a \"PR disaster\". there was absolutely one misstep (clarity in the initial press release) as well as a noise blip. however if one looks at the attention it got, the continued attention it demanded and how 4.2 is shaking out as one series of PR events ....... \n\nunfortunately, people writing these kinds of drivel-infested blogs from negativeland tend to fail at being able to see pictures bigger than the end of their nose. *shrug*"
    author: "Aaron Seigo"
  - subject: "Re: Review of KDE 4,2"
    date: 2009-01-21
    body: "Well, it is quite funny to see that he first claims kde4 is a copycat of vista, and then he comes with all kinds of features that aren't available in vista :o)\n\nI also noticed in other blogs that windows7 reminds people even more of kde4. I wonder how long it will take before people accuse kde 4.3 or so being a rippoff of windows7, not realising that kde4 was already out before microsoft started with 7..."
    author: "whatever noticed"
  - subject: "Re: Review of KDE 4,2"
    date: 2009-01-23
    body: "This is clearly a rant -- it is very overblown with the intent of being humorous (and it is funny).\n\nIt is excessive and clearly over the top. But, I would suggest that there is some truth there and that the developers would be wise look behind the bombastic language and take it seriously.  "
    author: "JRT"
  - subject: "Newly created folders and files should be selected"
    date: 2009-01-20
    body: "Many thanks for all the dolphin/konqueror improvements !! I just love the filter bar :)\n\nMaybe a dev will be interested by this wish, it seems like a useful feature :\nhttps://bugs.kde.org/show_bug.cgi?id=155706\n\n\"Newly created folders and files should be selected, because it's likely the user wants to edit the file/open the folder right after he created it\"\n\nLooks cool isn't it ? :)"
    author: "DanaKil"
  - subject: "Re: Newly created folders and files should be selected"
    date: 2009-01-24
    body: "+1\n\nWant to save a file to a folder that doesn't exist yet?\n\nRight click, new folder, type name, hit enter three times.  Works in WinExplorer.  Would be nice for the KDE file dialog."
    author: "MamiyaOtaru"
  - subject: "Hey!"
    date: 2009-01-20
    body: "I was checking out the KDE 4 Unstable repos from BuildService. Are those early 4.3 snapshots? They seem like O_o\n\nI hope they are XD"
    author: "Luis"
  - subject: "Re: Hey!"
    date: 2009-01-21
    body: "Trunk is now 4.3, so any new trunk builds would be 4.3"
    author: "T. J. Brumfield"
  - subject: "Re: Hey!"
    date: 2009-01-21
    body: "Yeah, I knew it, but I was surprised they were 4.3 snapshots soo soon on OBS.\n\nMay I ask an extra question?\n\nSince KDE 4.3 is going to use Qt 4.5, Should I start using trunk+Qt4.5? Or should I wait a little bit more?"
    author: "Luis"
  - subject: "Re: Hey!"
    date: 2009-01-22
    body: "wait until we move qt-copy to 4.5 and/or 4.5 rc's start appearing."
    author: "Aaron Seigo"
  - subject: "Re: Hey!"
    date: 2009-01-22
    body: "Thank you very much Aaron :-) "
    author: "Luis"
  - subject: "Konqueror and Proxy"
    date: 2009-01-21
    body: "I cannot access https sites with konqueror via a proxy. For example accessing gmail, I get \"Connection to www.google.com is broken\".\n\nWithout proxy, its fine ... er ... although I had to try out various Browser Identities and finally \"Opera 9.x\" worked!\n\nI want to use konqueror on my work laptop, if possible, as opposed to firefox. However, broken proxy support for https is preventing me from doing so.\n\nWill this be fixed in 4.2 final?"
    author: "Ken"
  - subject: "Re: Konqueror and Proxy"
    date: 2009-01-22
    body: "i'm using it now with proxy (through ntlmaps with authentication) and it works.\n\nsometimes it displays error \"connection is broken\", but the page loads ok."
    author: "mdl"
  - subject: "Simon for President!"
    date: 2009-01-21
    body: "Simon St James is my personal Hero!\n\nAfter so long, someone is taking care of Konqueror!!!\n\nI really hope Konqueror improves further, I'd really love for it to become *the* all-round tool again.\n\nOf course there are many issues with Konqueror, but reading this digest gives me hope that Konqueror will (sometime) return full-featured, stable and fast!\n\nThanks a lot!\n"
    author: "Hannes Hauswedell"
  - subject: "Re: Simon for President!"
    date: 2009-01-22
    body: "SSJ, same here. thanks for all the patches!"
    author: "mxttie"
  - subject: "Re: Simon for President!"
    date: 2009-01-23
    body: "Thanks for the kind words, guys; however, I gather that some other fellow was sworn-in a couple of days ago so the Presidency will have to wait ;)\n\nI don't want to make any promises as, for all I know, Peter and I could be hit by buses tomorrow, but I'd really like to make Konqueror as a file manager *at least* as good as it was in KDE3 times, and will be actively requesting user feedback when I get around to writing this blog of mine :)\n\n"
    author: "SSJ"
  - subject: "uhh...  unless I'm missing"
    date: 2009-01-27
    body: "uhh...  unless I'm missing something in your post, this already exists in KDE4.  If you have a file dialog open, click on the icon in the toolbar that looks like a folder with a + sign on it.  Type folder name, hit enter, done.\r\n\r\nOr right click in the files area and click \"New Folder\", enter folder name, hit enter, done.\r\n\r\nIn Folder View plasmoid, right click on an area with no icon, highlight \"Create New\" and select \"Folder\" from submenu.  Type in folder name, hit enter, done."
    author: "bodly"
---
In <a href="http://commit-digest.org/issues/2009-01-11/">this week's KDE Commit-Digest</a>: More parts of the <a href="http://oxygen-icons.org/">Oxygen</a>-based "Air" visual identity enters KDE SVN in time for the KDE 4.2 release, including KDM background images. Better integration of the new "NetworkManager" <a href="http://plasma.kde.org/">Plasma</a> applet with KWallet. Initial work on a new "Welcome" Plasmoid. Support for more units added to the "Conversion" runner in Plasma, including "pressure", "currency", and "energy". Support for identi.ca (and laconi.ca) added to the "Twitter" Plasma data engine. Much work on choqoK, a standalone KDE Twitter application. Support for using imageshack.us as the backend for the "Pastebin" Plasma applet. More work on a "VideoPlayer" Plasmoid (based on <a href="http://phonon.kde.org/">Phonon</a>). The VLC backend for Phonon is imported into KDE SVN. New LilyPond indenter script and RPM Specification syntax highlighting files in <a href="http://kate-editor.org/">Kate</a>. Lots of long-awaited new features in KRuler. Start of a new user interface in Rocs. General improvements to visual rendering of .odt documents in <a href="http://okular.org/">Okular</a>. Lots of fixes in <a href="http://pim.kde.org/components/kpilot.php">KPilot</a> and printing in <a href="http://korganizer.kde.org/">KOrganizer</a>. Needed improvements in the LaTeX export feature of <a href="http://www.koffice.org/kspread/">KSpread</a>. Various work on <a href="http://pim.kde.org/akonadi/">Akonadi</a> and <a href="http://amarok.kde.org/">Amarok</a> 2, including an early "Bookmark" applet and changes to the way the Plasma canvas is presented in the application. An new equivalent to the old "dcopstart" command. Support for localized backends in KPackageKit, with the ability to configure the application from the system tray icon. A registry patch for registering KDE system settings modules as Control Panel elements for KDE-on-Windows. Updated browser user agents representing modern browsers in <a href="http://www.konqueror.org/">Konqueror</a>. A major refactoring to convert all backends (VNC, RDP, NX, test) to plugins in KRDC. Initial release of "Magazynier", a KSokoban remake for KDE 4. Import of a version of Konqueror with thumbnailing support. Initial import of a "<a href="http://solid.kde.org/">Solid</a> Actions" KControl module (to replace the "Media Devices" KCM from KDE 3), and telepathy-accountmanager-kwallet. <a href="http://www.koffice.org/kformula/">KFormula</a> is removed from kdelibs, in search of better maintainership and to avoid code duplication with <a href="http://koffice.org/">KOffice</a>. Amarok 2.0.1, KOffice Beta 5 (1.9.98.5), <a href="http://eigen.tuxfamily.org/">Eigen</a> 2.0 Beta 5, KDE 4.1.4, and KDE 4.2 Release Candidate 1 are tagged for release. <a href="http://commit-digest.org/issues/2009-01-11/">Read the rest of the Digest here</a>.

<!--break-->
