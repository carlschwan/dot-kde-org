---
title: "KOffice 2.0 Release Candidate 1 Released"
date:    2009-04-08
authors:
  - "jriddell"
slug:    koffice-20-release-candidate-1-released
comments:
  - subject: "Nice job"
    date: 2009-04-08
    body: "The new KOffice may not be a finished product but it really does look very good. I find it much more intuitive to use then OpenOffice.  \r\n\r\nThe start time while fast compared to OpenOffice, still seem s a bit slow, hope this can be addressed in future builds.\r\n\r\n\r\n\r\n"
    author: "littlecloud"
  - subject: "anything is better than"
    date: 2009-04-08
    body: "anything is better than OpenOffice ;-D\r\n\r\nlooks good, lot of work under the hood, but the UI really needs to be polished. really hope something like the kde4 dev way, an start point and then the things start the getting better."
    author: "El barto"
  - subject: "I'm going to pull out my wacom tonight :)"
    date: 2009-04-09
    body: "I've been following Krita 2 for some time. Its nice to see it finally getting to the finish line."
    author: "eean"
  - subject: "Release early, release often"
    date: 2009-04-09
    body: "KOffice 2.0 looks awesome. It needs polishing, what is not bad thing. There are few things on beta7 on me what keeps me away from it. Like starting KWord with two columns template and after writing something on it, save it and reopen. The columns are gone. Well, it is easy to fix for 1-2 sheets but bigger writings it is harder.\r\n\r\nOne thing what I dislike totally, is the forced linked size of open-dialog and the window itself. I liked very much the early alpha status (if I remember correctly) where the dialoge could be small and the actual application window needed size. \r\n\r\nAnd I have done new icons (SVG) for the templates but I dont know where to place those to get try them. Those 16x16px or 22x22px icons does not look good when scaled. Hopefully those are fixed on RC1 what I am currently installing.\r\n\r\nNeed to fill a bugreport about feature for the open-dialog to allow user add own groups and then own templates for those, so user ain't forced to have only \"blank documents\", \"cards and labels\" and \"Envelopes\" groups.\r\n\r\nKOffice 2.0 needs polishing on the KWord toolbars too. Like why to have two times the font type/size and zoom on toolbar and sidepanel/bottom panel.\r\n\r\nAnd I always miss the KWord configuration entry on the menu :-D\r\nEven that the menus and sidepanel are great by default. I just hope there could come configuration options if needed :-)\r\n\r\n"
    author: "Fri13"
  - subject: "New icons for templates?"
    date: 2009-04-09
    body: "Oh, please -- come by on our mailing list or irc and let's together to see whether we can get those icons in the release. We really need a nice set of good icons for the templates!\r\n\r\nAbout the startup screen: it's been this way since at least KOffice 1.5, if I remember correctly -- might be 1.4, even. I've grown to like it, especially for krita, since it lets me quickly decide what to do -- open a recent file, create a custom one, use a template.\r\n\r\nI would be great, though, if we could get the little ui glitches sorted out, like having two zoom controls and the things you mention."
    author: "Boudewijn Rempt"
  - subject: "\"About the startup screen:"
    date: 2009-04-09
    body: "\"About the startup screen: it's been this way since at least KOffice 1.5, if I remember correctly -- might be 1.4, even. I've grown to like it, especially for krita, since it lets me quickly decide what to do -- open a recent file, create a custom one, use a template.\"\r\n\r\nThose options ain't under critic, but the size. \r\n\r\nLike if you set the Krita window to fullscreen for better usage on 1920x1200px monitor. You get that sized dialog when you create new file or you reopen the krita. \r\n\r\nThere is lots of space wasted when the template, new, open and open recent dialog is linked to same size of the main window. And it makes the dialog look bad. Like on krita when you create custom image, the descreption window takes so much space that you could write almost a novell there ;-)\r\n\r\nI like the dialog, it is well designed otherwise because it allows easily to choose what you want to do. Without first needing to open application and then from menu/toolbar to open file or create new one etc. You can skip the first part totally. That is something what I would like to see more on other applications :-)"
    author: "Fri13"
  - subject: "Release number"
    date: 2009-04-11
    body: "Yes, release early and often.  You need to involve the users in the development.  But, why give it a number that indicates that it is a finished product?  This is the same error that was made with KDE-4 so I can see how that would happen.  But other FOSS projects don't make this mistake.<p>Wouldn't it be better to call it:\r\n\r\nKOffice2-0.8.0\r\n\r\nor something like that?"
    author: "KSU257"
  - subject: "Nice, but..."
    date: 2009-04-11
    body: "It looks very promising. I just tried to open my current work in ODT and noticed immediately some problems:\r\n- terrible and almost unreadable rendering of the Andron Scriptor font (I have only the free version)\r\n- footnotes turned into some huge ugly default font...?\r\n- no automatic texts (name of the document not displayed anywhere, automatic page number shows always \"2\"...)"
    author: "wanthalf"
  - subject: ".dmg ?"
    date: 2009-04-12
    body: "Do you envisage to create .dmg for KOffice under Mac OS X ?"
    author: "vida18"
  - subject: "Agreed"
    date: 2009-04-13
    body: "I would have thought that given the backlash against KDE4.0, KDE developpers would have learned that whatever the release text say is not enough and the version name is also important.\r\n\r\nPlease KOffice developpers, think seriously about naming it KOffice-preview2.0 or something like that..\r\n\r\nIt's very likely that some distribution (Fedora..) will replace KOffice1.6 with KOffice2.0 and users will complain, so please explain with the version name, the help file, etc the situation to users.."
    author: "renox"
  - subject: "Distributions"
    date: 2009-04-13
    body: "I'm quite confident that Fedora will ship KOffice as soon as possible. Fedora is a bleeding-edge distribution, and has always been. Let's hope that most distributions ship both KOffice 1.6 and 2.0."
    author: "majewsky"
  - subject: "http://bugs.kde.org"
    date: 2009-04-13
    body: "If you want your bug reports to stay completely unnoticed, this is the perfect place. Because it seems quite obvious that you want the opposite case, please visit the above URL."
    author: "majewsky"
  - subject: ".dmg ?!?"
    date: 2009-04-13
    body: "From what I read, .dmg files are disk images. What does KOffice have to do with disk images? You would probably mean some installer file, see http://mac.kde.org for details."
    author: "majewsky"
  - subject: "Mac apps rarely have"
    date: 2009-04-13
    body: "Mac apps rarely have installers, instead they usually get distributed as .app bundles within .dmg disk images."
    author: "Eike Hein"
---
<p>Today, the KOffice team has released the first, and hopefully the only, release candidate for KOffice 2.0, bringing more than three years of work to a temporary conclusion. Compared to Beta 7, this release candidate brings <a href="http://www.koffice.org/announcements/changelog-2.0-rc1.php">a multitude of bug fixes</a> and not a single new feature, as it should be!</p>

<p>See the <a href="http://www.koffice.org/announcements/changelog-2.0-rc1.php">changelog</a> for all details, the <a href="http://www.koffice.org/releases/2.0rc1-release.php">release notes</a> for download links, or the <a href="http://www.koffice.org/announcements/announce-2.0rc1.php">announcement</a> for more details.</p>
<!--break-->
<img src="http://dot.kde.org/sites/default/files/koffice2-rc-krita-wee.png" width="300" height="225" align="right" />

<p>Do note that KOffice 2.0 is <i>not</i> intended to replace KOffice 1.6 or OpenOffice for daily usage. While confident that the applications can be used for real work, we squarely aim the 2.0 release at the enthusiast, the tinkerer, the curious, the bold and the adventurous. For them, there is plenty to explore and investigate: this release contains many years of work.</p>

<p>For instance <a href="http://dot.kde.org/2006/09/05/koffice-summer-code-students-deliver-goods">Gabor Lehel's 2006 Summer of Code project, a flexible document structure docker with three views</a> finally bears fruit in this release. The design work <a href="http://www.valdyas.org/fading/index.cgi/hacking/koffice/meeting.html">we started in March 2006 in Deventer</a> now provides us with a flexible framework that at the moment already turns KOffice into the most flexible application suite in existence, you already get <a href="http://dot.kde.org/2007/08/27/pencils-down-koffice-summer-code-students?page=1">Marijn's Musical Notation Shape</a> with 2.0.  Try to imagine what will be possible!  One thing is certain: flake gives us a solid foundation to build upon for a long, long time.</p>

<p>Deciding to make a release, one in a long row of <a href="http://koffice.org/releases/2.0alpha2-release.php">alpha</a> and <a href="http://www.koffice.org/releases/2.0beta1-release.php">beta</a> releases into a release candidate is not a light decision, but while we know there are still bugs, and missing features "every" office suite provides and usability issues and other infelicities, we are confident about our decision. On to 2.0.1 and 2.1, and many, many releases after that!</p>

<img src="http://dot.kde.org/sites/default/files/koffice2-rc-kpresenter-wee.png" width="300" height="225" align="left" />
<p>Contained in this release are KWord, KPresenter, KSpread, Karbon, Krita and KPlato. Kexi and Kivio will return in later releases (there is good progress being made especially on Kexi, volunteers are welcome for Kivio!), while KChart has returned as a flexible shape, instead of an independent application. The same is intended for KFormula, but that shape will only be released in a later version.</p>

<p>There are no known release blockers in this release candidate. We hope for strong testing from the user community, and if that testing uncovers new critical bugs, we will release another release candidate.</p>

<p>One issue to note: if your distribution provides LCMS 1.17 with recent security patches, Krita will <i>not</i> be able to load files or act as a paste target, instead it will crash and die horribly.  Convince your distribution to upgrade to LCMS 1.18.</p>