---
title: "Amarok 2.1 Beta 2 Released"
date:    2009-05-14
authors:
  - "nightrose"
slug:    amarok-21-beta-2-released
comments:
  - subject: "Cool"
    date: 2009-05-14
    body: "Great news, waiting for the final release..."
    author: "Amine27"
  - subject: "Well lather me up in baby oil and call me Charley..."
    date: 2009-05-15
    body: "I do like me some fresh Amarok, I do and it's looking even sweeter with each release. I'm loving the ability to bookmark track positions in eBooks and long music pieces. The online services are nifty and the usability gets better every time.\r\n\r\nCongrat's to the A Team!\r\n\r\nNow if you'll excuse me, I have some music to listen too. :)"
    author: "Bugsbane"
  - subject: "Wonderful Magnatune Music - now with URLs!"
    date: 2009-05-15
    body: "Check this out - you can send URLs of your favourite songs to other Amarok users:\r\namarok://navigate/service/Magnatune.com/genre-artist-album/Broken%20Open\r\n\r\nThat's some pretty cool stuff right there."
    author: "madman"
  - subject: "A great improvement!"
    date: 2009-05-15
    body: "It's great to watch the good development of Amarok. I love Amarok and I love KDE. I hope that soon all the bugs removed. It's not so nice that the thing with Various Artist does not work well. Otherwise a great program!"
    author: "Martin Hasenpflug"
  - subject: "New panel..."
    date: 2009-05-15
    body: "I dont know about the non-vertical menu what got replaced Kick-Off styled menu structure. \r\n\r\nI dont like the Kick-Off application menu because it hides the root menu when user goes to submenu and clickin back is harder than usually used menu structure, what follows same idea as all the drop-down menus and context menus.\r\n\r\nIdea is nice but it just adds one click more to menu, and the usability to view where you actually are, is lost. \r\n\r\nAnd looks like there is not a change to hide the left sidebar anymore either."
    author: "Fri13"
  - subject: "100MB of memory to play an MP3?"
    date: 2009-05-15
    body: "It now only needs 100MB of memory to play an MP3 or a Shoutcast stream. This IS what I call an improvement. Very nice work everyone. My gratitude goes especially to the guys responsible for the high-level design of Amarok 2.1. Thank god it does not want more memory, like 700MB - it only needs mere 100 megabytes. Where would the world be without the natural genius sleeping in each programmer?"
    author: "Username"
  - subject: "Overall much better but still..."
    date: 2009-05-15
    body: "I love the improvements, specially the playlists management.\r\n\r\nKudos to all the developers.\r\n\r\nHowever I still can't play songs in random order.\r\nShould it be working or is it still not implemented (I just did a search on bugs.kde.org) ?"
    author: "merkurial"
  - subject: "2 questions:\n1\u00ba) When"
    date: 2009-05-15
    body: "2 questions:\r\n\r\n1\u00ba) When multiple widgets are added can be resizable? I would like decide who take more space... for exaple I would like get 3/4 parts of the column to the lyrics or more size... but until now every widget get a static size, awful when 1 of your favorite widgets has only a very little size.\r\n\r\n2\u00ba) Finally, when only 1 widget is added will take ALL the column size? Is horrible to see the Lyrics widget to get only a little part of the column..."
    author: "xapi"
  - subject: "All bugs removed?"
    date: 2009-05-15
    body: "<tt>mysql@bko> TRUNCATE TABLE bugs;</tt>\r\n<tt>Changed 186958 lines... success.</tt>\r\n\r\n;-)"
    author: "majewsky"
  - subject: "This should work. Just go to"
    date: 2009-05-15
    body: "This should work. Just go to the playlist menu, and under \"random\" select the playback mode you want. We know that it is not terribly intuitive to find the random option there, so this will be moved to somewhere near the playlist in the future.\r\n\r\nIf it does not work, then that is a bug that should be filed on bugs.kde.org"
    author: "nhnFreespirit"
  - subject: "oh yeah."
    date: 2009-05-16
    body: "Oh, it's worse. I've got just 32880 tracks, yet Amarok needs no less than 130 mb ram to work with that. And it takes at least a minute to add all the tracks to the playlist! Horrible. I mean, any audio player can handle a playlist of 50.000 tracks and search instantly in it, right?"
    author: "jospoortvliet"
  - subject: "good point"
    date: 2009-05-16
    body: "Good point. Kickoff should be removed from Amarok at once! Oh wait...\r\n\r\nThe left sidebar can be hidden like the old days, click the button of the active view and it hides. It does still show the icons themselves, not sure if that could be hidden in the old Amarok as well?"
    author: "jospoortvliet"
  - subject: "subject!"
    date: 2009-05-17
    body: "I tried Amarok 2 again a few days ago. Some things were pretty impressive (the lyrics applet; also loading the collection into the playlist instantly -- is this purely because of the SQLite->MySQL switch, or other work as well?). There's one thing left, as far as I could tell, which keeps me from using it.\r\n\r\nPressing 'next track' (when in a stopped state) automatically playing that track rather than just selecting it as current: is this an intentional change, or negotiable?\r\n"
    author: "illissius"
  - subject: "Do some estimates first"
    date: 2009-05-17
    body: "How long can it take to find the word \"Song\" in the titles of 32880 tracks on a 1GHz single-core processor using a very simple algorithm? My estimate is 5 milliseconds or less. What is your estimate?\r\n\r\nAssuming that on average the internal representation of a track has: title with 40 characters, 4/8-byte pointer to the C++ object representing its album, 4 bytes for the year, 4/8-byte pointer to the C++ object representing its genre, 4 bytes for the length in seconds, 4 bytes for the play count, rating, bitlength, etc. ... my estimate is that information per one track should be something like 100 bytes. Based on this estimate, for 32880 tracks we need some 3 MB of memory.\r\n\r\nHow many bytes do we need in order to create a sorted set of tracks for fast access (if it's needed)? For 32880 tracks, and using 4 byte pointers, I estimate it to be between 32880*4 and 32880*(4+8) bytes. That's some 130-390 KB of memory (it depends on the overhead of the data-structure in which the sorted set is stored).\r\n\r\nNotes: I am ignoring overheads which are due to malloc implementation. I am ignoring the issue of memory fragmentation."
    author: "Username"
  - subject: "100 MB? Are you sure?"
    date: 2009-05-18
    body: "Not according to this screenshot I just took: <a href=\"http://imagebin.ca/view/92gXAH.html\" target=\"_blank\">http://imagebin.ca/view/92gXAH.html</a>\r\nOr this one: <a href=\"http://imagebin.ca/view/e1Nj1-A.html\" target=\"_blank\">http://imagebin.ca/view/e1Nj1-A.html</a>\r\n\r\nIn fact, no matter how many tracks I add, in whatever format and in whatever state, Amarok seems to hang around the 20MiB area - a measly 1% of my memory, and a fairly reasonable amount considering all the widgets I have in the center bar (including Lyrics, Services and Bookmarks). In fact, just to double-check, I started streaming stuff from Magnatune: <a href=\"http://imagebin.ca/view/rTQfng.html\" target=\"_blank\">http://imagebin.ca/view/rTQfng.html</a>. *Gasp!* 28MiB? Unacceptable!"
    author: "madman"
  - subject: "One last note"
    date: 2009-05-18
    body: "Please don't complain about performance if you're not going to give us any details about your machine. It doesn't help us solve anything any quicker. \u00ac_\u00ac"
    author: "madman"
  - subject: "Not convincing"
    date: 2009-05-19
    body: "1. How many megabytes of the process \"amarok\" are swapped? Turn off all swaps using \"swapoff -a\", then post another screenshot please.\r\n\r\n2. Post another screenshot which also shows the column \"Virtual size\".\r\n\r\n3. Your statement that \"no matter how many tracks I add, ..., Amarok seems to hang around 20MiB area\" is completely insane. Have you tried to add 100000 tracks, a million, 1e100, or what?\r\n\r\n"
    author: "Username"
  - subject: "I did not complain about that"
    date: 2009-05-19
    body: "Performance and memory consumption are two different things. My complain is about memory consumption, which, according to my opinion, can/should be lowered without affecting Amarok's performance."
    author: "Username"
  - subject: "Some little bugs"
    date: 2009-05-20
    body: "When you have enabled \"Automatically scroll playlist to current track\" there are two little (in my opinion) bugs:\r\n\r\n1\u00ba When you press the Next track button, the playlist scroll to next track and you see it, but if you use key shortcut (in my casa Winkey + B), the list scroll to the previous track.\r\n\r\n2\u00ba If amarok is iconified, and you restore again, the playlist is on top\r\n\r\n\r\nThe app is great, so good work!! Thanks for made amarok so great!!!"
    author: "Fenix-TX"
  - subject: "http://bugs.kde.org"
    date: 2009-05-20
    body: "Please file your bugs at http://bugs.kde.org, devs won't read them here."
    author: "majewsky"
  - subject: "The fact that all you can"
    date: 2009-05-21
    body: "The fact that all you can think to complain about is how much memory Amarok uses - which is negligible on most modern or recent computers - goes to show that there's not much to complain about in Amarok 2. If you've got a 256MiB RAM machine, in which case 100MiB is a large portion, you shouldn't even be running KDE - that's what XFCE/Fluxbox/WindowMaker/etc are for. If you want something that uses little/no memory, go use SoX. The point of Amarok isn't to be lean by any means. If it is, it's a nice side-benefit, but that's not the point.\r\n\r\nYour statement that my statement is completely insane is... well, completely insane. If the difference in memory usage between 1 track and >100 tracks is only a few MiB, then it stands to reason that x many more tracks may well be negligible. Why don't you go and post a screenshot of the amount of memory used for a million tracks?\r\n\r\nAmarok is designed primarily for one reason: getting information relating to your music. In more basic terms, features, features, features. If you want something that plays a million songs while using <1MiB, then use Juk or SoX."
    author: "madman"
  - subject: "Can I finally get some concrete answer?"
    date: 2009-05-21
    body: "1. Memory consumption is not the only problem. There are also others. Just because I did not mention them, it does not mean that memory consumption is the only thing I care about and it does not mean I do not want Amarok to have better features.\r\n\r\n2. Nobody has answered my complaints by providing implementation reasons/examples of why Amarok uses the amount of memory it does use. All I am listening to are just non-answers to questions about memory consumption.\r\n\r\n3. As to the question \"Why don't you go and post a screenshot of the amount of memory used for a million tracks?\": It wasn't my claim that Amarok uses a sane amount of memory for an arbitrary number of tracks. It was your claim. You prove it.\r\n\r\n4. Juk-3.2.3(KDE4.2.3) has far fever features than Amarok, and its memory consumption on my machine is approx 75% of the memory consumed by Amarok. It is using more memory than 1MiB, so your \"recommendation\" that I should use it is wrong. No, I do not expect you to admit that you anticipated it to use less than 1MiB of memory. I know you are an idiot who is unable to stick to facts and numbers."
    author: "Username"
---
<p>
Another month has passed, and it's time to present the second beta release of the upcoming Amarok 2.1.
</p><p>
Besides the usual amount of bugfixes it also features some useful user interface improvements, for example the Context View has received some artistic love. More attention has been paid to detail and with the useful feedback from usability testing, tweaks have been made. From having the escape key clear the search box to standardizing the behaviour of the collection tree view to follow your KDE settings, the quirks are being ironed out. Check the <a href="http://amarok.kde.org/en/releases/2.1/beta/2">release notes</a> and grab your copy of Amarok for some testing and help us find and fix the remaining bugs before the release of Amarok 2.1.
</p>
<center><a href="http://dot.kde.org/sites/dot.kde.org/files/amarok2.0.96.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/amarok2.0.96t.png"></a></center>