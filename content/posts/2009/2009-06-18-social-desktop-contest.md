---
title: "Social Desktop Contest"
date:    2009-06-18
authors:
  - "fkarlitschek"
slug:    social-desktop-contest
comments:
  - subject: "Spelling correction"
    date: 2009-06-19
    body: "In paragraph 1 the word \"prices\" should be spelled \"prizes\"."
    author: "yman"
  - subject: "discount"
    date: 2009-06-22
    body: "They may be offering a 50% discount on the next KDE release. heh ;)"
    author: "uga"
---
Today we are launching the Social Desktop Contest. As you know the idea of the Social Desktop is to connect online webservices with desktop applications. We give away great prices to developers who help making this vision reality.

The Open Collaboration Services API has gotten many new features in the past few months and is now stable. The first features will ship with KDE 4.3, but this is only the beginning. Now that the infrastructure is in place we think that it is a good time to open up the development to more developers.

Have you already played with the idea of developing an application for the Social Desktop? Are you interested in programming a clean and easy REST API? You have already heard about the Open Collaboration Services API and wanted to join in? Now is your chance to do so and to win great prizes for your creation! We and the openDesktop.org community are searching for the best and most creative contest submission around the Social Desktop/OCS API!

The goal of the Social Desktop Contest is to foster community development and innovations around the OCS API. Prize winners will be selected by the community and a jury. Entrants are encouraged to submit entries that encourage community participation. Everyone can participate, and teams are allowed (but then they have to share their prize, too). Every contribution must be licensed under an open source license and must be built around the OCS API. Otherwise the entry will not be accepted! Every contest contribution should be uploaded to openDesktop.org (upload category: Social Desktop contest) so that the community can vote for the best content.

The contest will run until August 25th, 2009. As the Social Desktop is a cross-platform idea it is great that we have people from different projects in the jury.

Jury members are:
Aaron Seigo - KDE
Luis Villa - GNOME
Alexandro Colorado - OpenOffice.org
Frank Karlitschek – openDesktop.org

The following prizes can be won: The top winner will receive a brand new Dell Inspiron Mini 10v Netbook (running Linux of course). The owner of the entry with the second highest rating gets a new external HDD with 1TB for backup and storing data. The third and the fourth ones get an Amazon.com gift certificate for $50.00 and $30.00, respectively.

We are looking forward to your contribution! Sound interesting? Check out <a href="http://www.socialdesktop.org/contest/">the official contest site</a> to obtain more detailed information. You can also find some ideas there.

In other news, we launched version 2.0 of our <a href="http://www.openDesktop.org/knowledgebase">knowledge base system</a> on openDesktop.org today.

The questions and answers are now in the main navigation and more easily accessible. You can search for answered or unanswered questions. We have introduced different categories and a full text search. Questions and answers can be related to specific applications or independent of them. We also simplified the answer workflow. Now every registered user can click on the add answer button and answer a question. You can also upload two pictures for every question and two more pictures for the answer. The idea is to build a user generated FAQ and knowledge base to help new and inexperienced users.

All the data is accessible via the OCS REST API. Marco Martin has already written a data engine and a plasmoid to access this knowledge base directly from the desktop. The data engine will be part of KDE 4.3. Let us see what great things we can do with this infrastructure and ideas in the future.

Our new <a href="http://www.openDesktop.org/events/">event database</a> is also now accessible via the Open Collaboration Services API and has its own RSS feed. But RSS is so old-school: I think integration into the Desktop or Akonadi is the future. Does somebody volunteer to help? :-)