---
title: "KDE Commit-Digest for 4th January 2009"
date:    2009-01-07
authors:
  - "dallen"
slug:    kde-commit-digest-4th-january-2009
comments:
  - subject: "drag-and-drop unhide"
    date: 2009-01-07
    body: "Sorry, but what is drag-and-drop unhide?\n\nThanks for the digests, Danny!"
    author: "Alcal\u00e1"
  - subject: "Re: drag-and-drop unhide"
    date: 2009-01-07
    body: "Set your panel to autohide. It hides. Drag something down to where the panel should be. Before this fix, the panel would stay hidden. Now it will unhide, so you can drop something in the previewer or opener or pastebin or whathaveyou."
    author: "Anon"
  - subject: "PolicyKit Credits"
    date: 2009-01-07
    body: "Credits also go to Daniel Nicoletti for creating the text with me...\n\nBtw, nice work Danny, thanks for all the Digests! :)\n\nLukas"
    author: "Lukas Appelhans"
  - subject: "Re: PolicyKit Credits"
    date: 2009-01-07
    body: "Thanks to you both and/or whoever else has been working on this. It's just a lot nicer to have native dialogs in applications that take advantage of this (such as KPackageKit)"
    author: "Stu"
  - subject: "A Clockwork Commit"
    date: 2009-01-07
    body: "w00t! Danny <3"
    author: "Lisa Engels"
  - subject: "wrong stats bug count"
    date: 2009-01-07
    body: "Really, the closed bug count should be a bit less for me and John, but the weekly stat sometimes is not correct. The right count for the last week is something like this:\n- Leonardo Finetti (me) about 200 bug closed\n- Dario Andres is right: 128\n- John Layt about 50\n\n\n\n"
    author: "Leonardo Finetti"
  - subject: "Re: wrong stats bug count"
    date: 2009-01-07
    body: "Ah, so that's how you jumped from 150 to 1500 in an hour, hehe. :P"
    author: "Jonathan Thomas"
  - subject: "Re: wrong stats bug count"
    date: 2009-01-07
    body: "Indeed :-)\nUsually during the last month, my average closed bugs count was about 100/200 bugs per week, you know, there are a lot of dups, solved issues, bugs reported without sufficient information for understand the problem and so on...\n\nAnyway I'd like to thanks the bugsquad team which is working a lot to keep bugzilla tidy as much as possible :-)"
    author: "Leonardo Finetti"
  - subject: "Re: wrong stats bug count"
    date: 2009-01-07
    body: "We are all working hard :) (I never realized that I was in the commit-digest, what an honor :P )"
    author: "Dar\u00edo Andr\u00e9s"
  - subject: "Re: wrong stats bug count"
    date: 2009-01-07
    body: "indeed, hats off to everyone helping with bugzilla. it's awesome to see bugs.kde.org actually becoming a sensible collection of issues rather than an ever growing pile of who-knows-what."
    author: "Aaron Seigo"
  - subject: "Re: wrong stats bug count"
    date: 2009-01-19
    body: "Firstly, I love the Commit Digest for letting me keep a gradual eye on what's shaking right now.\n\nAaron -\n\nI'm incredibly impressed by the cooperation in stomping bugs by users who recognize the need to reproduce and narrow down the source as much as possible before a dev gets involved to look at code. I've seen some end-users do some great research on complex bugs--such as bugs involving unexpected behavior between KDE/Qt and non K/Q libraries--to help the KDE dev team know ahead of time more about what's going on and where to start. It's rare to see a community/end users so tightly knit with the developers.\n\nI guess my point is that I'm proud of everyone for the work they do on both sides of the fence and how comparatively little complaining goes on for an OSS project, particularly one of the magnitude of KDE."
    author: "Jonathan Harper"
  - subject: "Re: wrong stats bug count"
    date: 2009-01-07
    body: "Yeah, I've just being doing some bug cleanup in preparation for 'officially' declaring KDEPrint Unmaintained, there's only a couple of real bugfixes in there.  The majority (470) comes from my having noticed that a lot of very old bugs were only marked as Resolved and not Closed and deciding to clean them up.  Turns out, after having spammed a lot of people (sorry!) that bko considers Resolved as the equivalent of being Closed (although not everywhere), which is contrary to my 'real world' experience.  I guess our lack of a QA team means we can't rely on anyone to validate the resolution before closing them.  Perhaps we need to educate the people who opened the bug that they should be closing them once they've confirmed the resolution works.\n\nThere's a couple of things this I think should come out of this.\n\nFirstly, there's nowhere that explains the full KDE bug lifecycle, what the different status's mean, when you should change them, what are the valid state changes, etc.  Someone who knows should document that in techbase, and put a help link to there from bko.\n\nSecondly, the 'Closed Bugs' stat really needs a better definition.  We should only count each bug once, so when it gets changed to Resolved, or gets changed to Closed without having been set to Resolved first.  Perhaps we also need a second table for the top bug fixers, i.e. those who set a bug to Resolved Fixed?  \n\nFinally, to keep things really tidy, we should have a script to automatically set Resolved to Closed after say 12 months with no feedback.  Or automatically set to Closed as soon as it's set to Resolved.  Or perhaps we need to revise the entire workflow, get rid of status's like Resolved and Validated and Assigned, and just have a streamlined New -> Confirmed -> Fixing -> Closed flow.  Either way, the process needs a clean-up."
    author: "odysseus"
  - subject: "Re: wrong stats bug count"
    date: 2009-01-19
    body: "Forgive my ignorance, but does the original bug-posting user automatically get an e-mail jab in the arm to see if the fix works when the bug is set to Resolved?\n\nI'm thinking a you post it, it's your responsibility to make sure it works kinda thing. Perhaps logging into bugs.kde.org should have a \"You currently own [X] bugs that are Resolved but not Closed. Click here to view them.\" banner near the top."
    author: "Jonathan Harper"
  - subject: "Thanks"
    date: 2009-01-07
    body: "Thanks Danny,\nand nice to see the Digest in full again..."
    author: "Andrew Lowe"
  - subject: "Kde 4.1.87"
    date: 2009-01-07
    body: "I tried the new version (kde 4.1.87)...\nIt is almost perfect...\n\nIt was a big jump from 4.1.3 in opensuse..\n\nThanks..\n\nKonqueror is really stable and fine...\nIt is all responsive..."
    author: "Norwegian"
  - subject: "Re: Kde 4.1.87"
    date: 2009-01-07
    body: "Could you check if it plays nice with wordpress now (visual editor)?"
    author: "Dick"
  - subject: "Re: Kde 4.1.87"
    date: 2009-01-07
    body: "The text editor is usable, but it does not offer to activate the visual mode (at least, I did not find a possibility)."
    author: "Stefan Majewsky"
  - subject: "Re: Kde 4.1.87"
    date: 2009-01-07
    body: "I've been using the 4.2 line since it was put in the unstable repo at opensuse, many many moons ago.  It has been great watching it come along.  It is hard to see what will come from 4.3, because 4.2 is just so friggin amazing."
    author: "R. J."
  - subject: "congrats!"
    date: 2009-01-07
    body: "Hey Danny! Congratulations for catching up with the digests! Thanks for putting in the work, and don't worry about getting the extra digest-special news/reports in.  They are really wonderful when they are available to read, but having the digest come in every week is special as it is.\n\nActually, it was a pleasure to get a sense of scope and comparison of activity from the last two months all in one week.  I hadn't done much comparison of bug/commit activity before but it was pretty interesting.\n\nthanks again,\nvlad"
    author: "Vladislav"
  - subject: "KDE Partition Manager"
    date: 2009-01-07
    body: "Is KDE Partition Manager related to Qtparted ported to KDE4 or it bas been written from scratch?"
    author: "Julien"
  - subject: "Re: KDE Partition Manager"
    date: 2009-01-11
    body: "KDE Partition Manager is unrelated to QtParted and has been written from scratch."
    author: "Volker Lanz"
  - subject: "Kubuntu"
    date: 2009-01-08
    body: "At last I have a working KDE4 install (4.2 Beta 2) with a beautiful activity bar (making up for a spacer, but also looking beautiful). Two things.\n\n1. My install experience was nothing short from hell. I had to uninstall Guidance (because Guidance crashing with PowerDevil is not a beautiful experience to have), and fight kmouth to death with kubuntu-experimental repo. If you have an experimental repository, wouldn't I expect you update your packages ASAP (to fix some things, and to break others, but that is going to be expected)?. Please, do it. And bring along RC1 packages.\n\n2. Can I give kudos to everyone involved in Plasma? The Activity Bar turned an unusable feature (activities) into a powerful way to maintain a tidy desktop. I feel GNOME is going to rot, rot and rot in this machine...\n\nSo, thank you, and keep up the good work!"
    author: "Alejandro Nova"
  - subject: "Re: Kubuntu"
    date: 2009-01-10
    body: "About your first point... Blame Kubuntu, those are they package. If you want to use a the unstable KDE, I recommend you to use OpenSUSE 11.1\n\nAnd, yes, Plasma is truly good :-)"
    author: "Luis"
  - subject: "Re: Kubuntu"
    date: 2009-01-11
    body: "Keep in note: I actually know what I'm doing and know the issues I can face with experimental repos. What I don't like is the lack of daily fixes in their experimental repositories. When I add an experimental repo, I expect breakage, but I also expect that breakage to be detected and fixed in almost an hourly basis. That happened with Fedora, Arch Linux, and Gentoo, and I don't really know why Kubuntu does not follow that way of thinking if they are managing an experimental repo. So, blame on them.\n\nAnd yes, Plasma is good ;)"
    author: "Alejandro Nova"
  - subject: "Re: Kubuntu"
    date: 2009-01-10
    body: "Two replies.\n\n1. Blame kubuntu for your install problems, as that is part of distro responsibilities. Though when you use experimental repos, you should expect such problems.\n\n2. And prepare for more cool improvements in next releases, as in KDE 4 the groundwork was laid for many more powerful features. The devs only need time to flesh out the user interface part for many things that are unusable right now. The activities are only the tip of an iceberg :)."
    author: "slacker"
  - subject: "Re: Kubuntu"
    date: 2009-01-10
    body: "I'm startig to give up on Kubuntu.\n\nWhy is it that Canonical refuses to offer it the same resources as the Ubuntu distro? Do they want the distro to fail?\n\nCompetition breeds innovation, and we have to have a serious contender to opensuse, or it will get stagnant."
    author: "Mike"
  - subject: "Re: Kubuntu"
    date: 2009-01-11
    body: "Canonical are looking to hire at least one extra KDE developer:\n\nhttp://www.kdedevelopers.org/node/3720\n\nhttp://www.kdedevelopers.org/node/3805\n\nI have no idea whether anyone has applied, or whether everyone who has applied was found to be unsuitable, or even what the job involves: if you're being hired to cripple KDE so that it matches what GNOME does (e.g. implementing Mark's \"controversial\" notification system in KDE), then I imagine that most KDE developers would run a mile :) Or it could be that you're mainly given a free hand and a \"make upstream KDE as good as you can, however you can.  Here's a salary so you can quit your job and work fulltime on it!\".  \n\nMaybe jriddell could give some more details?"
    author: "Anon"
  - subject: "ark still going strong ;-)"
    date: 2009-01-08
    body: "Harald Hvaal (metellius) committed a change to /trunk/KDE/kdeutils/ark/plugins/libarchive/libarchivehandler.cpp: \n   Implementing deleting files in tar.gz/tar.bz2 files, the last remaining feature for this release\n\nOh boy. That's way cool... Does it work in the KIO slave as well?"
    author: "jospoortvliet"
  - subject: "Bug Cleanup"
    date: 2009-01-10
    body: "I think there should be more focus on removing bugs and minor glitches. We want people to use KDE 4 and dont want them to sit on KDE 3.\nNow for example, one problem was that Konqueror 4 didnt easily accept browsing behind a proxy but it seems to work in KDE 3. (One old bug). \n\nOf course there are more, but maybe more attention should be on removing bugs rather than making everything perfect right now. We dont want people to still ask the question KDE 3 vs KDE 4 in the year 2010, or do we :)"
    author: "mark"
  - subject: "Re: Bug Cleanup"
    date: 2009-01-10
    body: "I think I read somewhere that KDE 4.3 is going to be THE bugfix and feature-polishing release. Which is good, as there are many quite major issues still standing out.\n\nAnd yeah, I too hope that by 2010 there will be a KDE 4 release worth considering switching to :)."
    author: "slacker"
  - subject: "Re: Bug Cleanup"
    date: 2009-01-11
    body: "KDE 4 is worth switching right now (4.2). KDE 3.5.10 is old technology, and feels like it."
    author: "Luis"
  - subject: "To Danny:"
    date: 2009-01-10
    body: "Is there any way to track development effort over time?\n\nI'm curious if KDE development gained, or lost momentum in light of this recession. Did we gain lines of code because of the recent layoffs (by volunteers in their spare time), or did we lose valuable coders, because they are no longer paid (to code at work, or in their freetime)\n\nWhat's the general trend?\n\n"
    author: "Mike"
  - subject: "Re: To Danny:"
    date: 2009-01-11
    body: "One way is to look at http://lists.kde.org/?l=kde-commits&r=1&w=2.\n\nThe number of commits/week changes of course, but year over year it has grown considerably.\n\nDerek"
    author: "dkite"
  - subject: "Re: To Danny:"
    date: 2009-01-12
    body: "Thanks.\n\nThis isn't quite as clear cut an answer as I hoped, but I guess in the spirit of open source, it's hard to track these things accurately."
    author: "Mike"
  - subject: "Re: To Danny:"
    date: 2009-01-19
    body: "I think you're looking more for this:\n\nhttp://techbase.kde.org/Schedules\n\n=)"
    author: "Jonathan Harper"
  - subject: "Thanks Danny!!!"
    date: 2009-01-12
    body: "Thanks for your hard on the KCD!!!!!!!!!!!!!"
    author: "jorge"
---
In <a href="http://commit-digest.org/issues/2009-01-04/">this week's KDE Commit-Digest</a>: <a href="http://plasma.kde.org/">Plasma</a> panels now support "drag-and-drop unhide". More improvements for scripted Plasmoids. "Weather" Plasmoid moves into kdereview for eventual move to extragear for KDE 4.2. Lots of reworking the "HTML Validator" <a href="http://www.konqueror.org/">Konqueror</a> plugin. Start of a "BomberMan"-like game using Kapman as a base. New game themes in Bomber and KTron. Further progress on the rewrite of Kolf. Start of an effort to refactor game modules in KGoldrunner. A KIPI plugin to export photos to Facebook from KDE photo applications. Support for autoselection in the "libksane" image scanning library. Work towards automatically detecting digital cameras in <a href="http://www.digikam.org/">Digikam</a> (using <a href="http://solid.kde.org/">Solid</a>). Support for importing old feeds from the RSS plugin (from version 2.2) in <a href="http://ktorrent.org/">KTorrent</a>. Work on "Kloss" (bindings for the Lua programming language). Start of an iPod plugin for DeviceSync. Final remaining feature implemented in Ark for KDE 4.2. Initial import of KSSH4 to KDE SVN. <a href="http://commit-digest.org/issues/2009-01-04/">Read the rest of the Digest here</a>.

<!--break-->
