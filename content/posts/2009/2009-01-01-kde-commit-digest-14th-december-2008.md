---
title: "KDE Commit-Digest for 14th December 2008"
date:    2009-01-01
authors:
  - "dallen"
slug:    kde-commit-digest-14th-december-2008
comments:
  - subject: "\"i'm back\""
    date: 2009-01-01
    body: "Oh yes, you are back, better than ever. :-)\nYour work is much appreciated, thanks!"
    author: "Georgios"
  - subject: "KDE on small screen sizes"
    date: 2009-01-01
    body: "\"Continued work to make applications usable on smaller screen resolutions, this time with changes to Konqueror, KTorrent, and KMail.\"\nI don't know how this relates but I think KDE apps are often wasting too much space on the screen making them more difficult to use on small screens like 13,3'' laptops."
    author: "Florian"
  - subject: "Re: KDE on small screen sizes"
    date: 2009-01-01
    body: "Er, you don't know how this relates? Well, what you're being told here is that people are working on improving the situation with regards to using KDE applications on small screens. So that it's easier to use KDE apps on small screens.\n\nAnd, by the way, Oxygen really gives a marvelous impression of roominess, such that I, too, thought all screen space was being wasted. Until I overlaid a screenshot of Krita using Oxygen on a screenshot of Photoshop 7.0 on Windows -- and discovered that the working area was exactly the same. Cool, isn't it?\n\n(And not all KDE developers use enormous screens -- I'm using a 1024x768 resolution laptop to develop Krita on as my machine.)"
    author: "Boudewijn Rempt"
  - subject: "Re: KDE on small screen sizes"
    date: 2009-01-02
    body: "Sorry, but comparing Krita to Photoshop sounds quite stupid. It's like comparing your average family car to a stretch limo :). Then again, when a family car takes as much parking space as a stretch limo..."
    author: "slacker"
  - subject: "Re: KDE on small screen sizes"
    date: 2009-01-02
    body: "Can we stop with the stupid analogies that don't apply to software please? \nSure Photoshop can do more, but the visible UI is pretty much concerned with common tasks between the two apps, so the comparison is valid and interesting."
    author: "Leo S"
  - subject: "Re: KDE on small screen sizes"
    date: 2009-01-02
    body: "That's one thing that annoys me in Qt/KDE (especially KDE4).\n\nI'll skip the subject of the Oxygen theme, as it has been bashed here for its space-wastage enough times. No point in repeating the bash/flame/valid criticism (depending on your position :)).\n\nBut WTH can't I redock my menu bar just like any other toolbar? Even many Window$ apps are capable of that! Then, instead of having my menu and toolbar buttons in the same row, I have to waste TWO rows of screen space! This is especially annoying in apps that need only few toolbar buttons, and leave a big fat unused gray space on the right..."
    author: "slacker"
  - subject: "Re: KDE on small screen sizes"
    date: 2009-01-02
    body: "I don't have that problem because I fill up that space with more stuff.\n\nAlso, you might try vertical toolbars with small icons (only) I find that it saves space."
    author: "JRT"
  - subject: "Re: KDE on small screen sizes"
    date: 2009-01-02
    body: "you learn the shortcuts, then hide toolbar completely"
    author: "Nick Shaforostoff"
  - subject: "Re: KDE on small screen sizes"
    date: 2009-01-03
    body: "But there is space for those buttons on the screen! It's just right (or left) of the menu bar! Why waste it?"
    author: "slacker"
  - subject: "Why is this digest so late?"
    date: 2009-01-01
    body: "What's with the slowdown? If you keep it this slow, you won't manage to complete all digests for 2009 by the end of January :(.\n\nFaster, Faster Danny!\n\n;)"
    author: "slacker"
  - subject: "Re: Why is this digest so late?"
    date: 2009-01-02
    body: "We need an additional guy that does actual commit summaries not more than 80-100 words. Maybe even less, just to be really up-to-date about the most recent svn work."
    author: "mark"
---
In <a href="http://commit-digest.org/issues/2008-12-14/">this week's KDE Commit-Digest</a>: Continued work to make applications usable on smaller screen resolutions, this time with changes to <a href="http://www.konqueror.org/">Konqueror</a>, <a href="http://ktorrent.org/">KTorrent</a>, and <a href="http://kontact.kde.org/kmail/">KMail</a>. Start of a "KuickQuiz" <a href="http://plasma.kde.org/">Plasma</a> applet, with initial support for placement in the panel. More new Plasma themes. More development of the "NetworkManager" Plasma applet, including a conversion to the "extenders" paradigm. A "pseudo-working" version of QEdje Plasma wallpaper. Printing fixes in <a href="http://okular.org/">Okular</a>. A <a href="http://www.smugmug.com/">SmugMug</a> photo service exporter in kipi-plugins (used by <a href="http://www.digikam.org/">Digikam</a>, etc). Initial version of KMJ, a Mahjongg game for 4 players. Continued work on the KBugBuster and Akonadi/Maemo student projects. Significant refactored changes in the "WebGUI" plugin of KTorrent. Further improvements in the integration of KDE with PolicyKit, including policykit-kde moving to kdereview. <a href="http://eigen.tuxfamily.org/">Eigen</a> 2.0 Beta 2 and KDE 4.2 Beta 2 are tagged for release.<a href="http://commit-digest.org/issues/2008-12-14/">Read the rest of the Digest here</a>.

<!--break-->
