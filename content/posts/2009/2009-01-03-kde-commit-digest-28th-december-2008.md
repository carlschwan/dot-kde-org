---
title: "KDE Commit-Digest for 28th December 2008"
date:    2009-01-03
authors:
  - "dallen"
slug:    kde-commit-digest-28th-december-2008
comments:
  - subject: "Whooo!"
    date: 2009-01-02
    body: "We're up to date! Thank you Danny, and have a wonderful new year!"
    author: "David Johnson"
  - subject: "thank you"
    date: 2009-01-02
    body: "Danny, thank you for continuing your work for commit digests."
    author: "Oleg"
  - subject: "Danny ftw!"
    date: 2009-01-02
    body: "Hey Danny!\n\nThanks for all of your hard work! You are awesome!"
    author: "Luke Chatburn"
  - subject: "So..."
    date: 2009-01-02
    body: "Are there any idea's yet how we as a community can help Danny to make his task a lighter one? I've just checked his blog entry on the subject and there are still zero comments there. I'm not a developer so I can't exactly asses what's needed. His main problem seems to be to hunt down all the commits to find the most interesting one's. He suggests that developers mail him when they committed something noteworthy.\n\nI have an idea but I don't know if it is technically possible. This idea is to let the developer somehow tag noteworthy commits with a special \"Digest\" tag. This way, Danny could make a script to filter on that tag. Developers should by now have a general picture on what is considered Digest-worthy, considering all the already produced digests.\n\nAny other idea's?"
    author: "Fred"
  - subject: "Re: So..."
    date: 2009-01-02
    body: "\"His main problem seems to be to hunt down all the commits to find the most interesting one's.\"\n\nNo, the main problem is getting the devs to write \"feature\" articles - as you can see by the rapid rate of Digests where the feature articles have been (temporarily) dropped, \"just\" searching through the commits is not the bottleneck."
    author: "Anon"
  - subject: "Re: So..."
    date: 2009-01-02
    body: "I love the feature articles, but I don' think they need to be tied to the commit digests.  I've enjoyed the recent smaller digests as well, and they stand alone without a story to go with them.  \n\nOf course these feature articles are great, but I guess we can't expect there to be one each and every week.  I think they should be posted directly to the dot as they are written, and keep the digest in the reduced form it is now.  \n\nAnyway, great work Danny, always a pleasure reading your work."
    author: "Leo S"
  - subject: "Re: So..."
    date: 2009-01-03
    body: "I've read Danny's blog post about missing \"feature articles\" and stuff. While I really appreciate his work, I don't understand his problem in this case. I see lots of \"feature articles\" every day on http://planetkde.org/\n\nFeature article about panels in 4.2? Here: http://aseigo.blogspot.com/2008/12/hiding-panels-feature-complete-for-42.html\n\nArticles about upcoming Amarok features? http://amarok.kde.org/blog/archives/843-From-the-Post-2.0.0-Git-Vaults,-Amarok-Urls-and-Bookmarks.html and http://amarok.kde.org/blog/archives/858-From-the-Post-2.0.0-Git-Vaults,-Part-2,-The-Playlist-Evolved.html\n\nLooking back at 2008: http://aseigo.blogspot.com/2008/12/passing-between-years.html"
    author: "The Devil"
  - subject: "Re: So..."
    date: 2009-01-03
    body: "Cause they are not new? They were read and discussed already?"
    author: "anon"
  - subject: "Re: So..."
    date: 2009-01-03
    body: "What's your point? If you want up-to-date information, screw the commit-didgest completely and point your browser to http://cia.vc/stats/project/KDE and scroll down to \"Recent Messages\".\nJust as the commit-didgest is just a compilation of a week's work, it could quote two or three important blog posts of that week.\n\nIf he doesn't want to quote blog posts, fine... it's his decision -- just as it's the developers' decision to post to their own blogs instead of mailing Danny and then hoping that it gets picked up a week later."
    author: "The Devil"
  - subject: "Re: So..."
    date: 2009-01-04
    body: "or even better: http://commitfilter.kde.org/\n\n=)"
    author: "Aaron Seigo"
  - subject: "Re: So..."
    date: 2009-01-03
    body: "\"the Digest is not going to become a mirror of PlanetKDE!\"\n\nhttp://dot.kde.org/1230851783/1230892169/1230904882/1230905622/\n\nIf the Commit Digest articles are just copy'n'pasted from planetkde, then they lose a lot of their relevance."
    author: "Anon"
  - subject: "Re: So..."
    date: 2009-01-03
    body: "Devil has a point. Imho the added value of the commit digest are the commits, not the stories - those could be posted as dot-stories or blogs, and not be different. I would therefor be entirely OK if the current digests would set the tone for the future.\n\nHowever, I can imagine having the additional content makes composing the digest much more fun, and ultimately it is up to Danny. After all, he does the work."
    author: "jospoortvliet"
  - subject: "Re: So..."
    date: 2009-01-03
    body: "Well - I  DON'T like  to read all the  devs' blogs. I just don't have the time for that. Blogs have the tendency of containing too much information I'm not really interested in - the commit digest had the advantage of concentrating on real news around KDE and not the individual itches of some developper. \n\nI guess this is a hard hit against the ego of some bloggers ;) ?!?!?\n\nHowever, if Danny e.g. would just use the commit digest to link to interesting blog posts with real information about new features of KDE or nice tutorials, as some KDE4 bloggers are publishing, this would be great."
    author: "Anon"
  - subject: "Re: So..."
    date: 2009-01-04
    body: "> Well - I DON'T like to read all the devs' blogs. I just don't have the time for that. Blogs have the tendency of containing too much information I'm not really interested in - the commit digest had the advantage of concentrating on real news around KDE and not the individual itches of some developper. \n\nDo you know of Planet KDE (www.planetkde.org)? The blog posts there are 90% developing things, and the rest 10% can be sorted out by just reading the title in most cases."
    author: "Stefan Majewsky"
  - subject: "Re: So..."
    date: 2009-01-04
    body: "I guess he means that he don't want read the whole technical post about anew feature, just the commit-digest resume of that new develpoment.\n\n"
    author: "Git"
  - subject: "Re: So..."
    date: 2009-01-04
    body: "Yes. 90% of all blog entries don't deal with the news I'm interested in, but - blogs being the contemporary equivalent of diaries - with some itch the dev currently has. I'd like to have detailed description of some new KDE4 feature, written in a way that is appealing to me as an end user.\n\nIn a way, Danny is the last person who seems to be interested in conveying interesting news of the developpmnent to the end user, to many other KDE4 devs seem to be no longer interested in communicating with thesometimes critical user base :(. The  lack  of  \"featured articles\" seems to be one  more symptom of that attidtude."
    author: "Anon"
  - subject: "Re: So..."
    date: 2009-01-04
    body: "I would like to see more automated statistics as this really inspires you, for instance an EBN report for the week. Everything that is automated does not need to be manually compiled."
    author: "Andre"
  - subject: "Re: So..."
    date: 2009-01-04
    body: "I think that this could still work. Take a look at the Linux Kernel.\n\nThe git changesets already have small summaries applied. Now, if different parts of KDE have a \"leader\" who could extend these summaries every once in a while to see what is more important, it could help Danny to speed up his summaries."
    author: "mark"
  - subject: "Yay!"
    date: 2009-01-02
    body: "Good job, we are back to be up2date!!  Happy new year by the way! :D  And keep up the good work!"
    author: "fengshaun"
  - subject: "Thanks"
    date: 2009-01-02
    body: "> from the i'm-not-a-department dept\n\n:) \n\nThanks Danny. Your efforts are much appreciated."
    author: "Steve"
  - subject: "Don't stop!"
    date: 2009-01-02
    body: "Cheers Danny!  And don't let such a mundane fact like reaching the present time stop this amazing Digest marathon of yours.  It would be *awesome* if the Digests reported on KDE developments happening in the future!"
    author: "Cultural Sublimation"
  - subject: "Qt 4"
    date: 2009-01-02
    body: "I guess the day Qt switches to a public git repository (not snapshots, but a real repository) weekly digests of Qt would be possible, and I would love to read them, too. Reading KDE digests is much more interesting, but knowing what to expect from the \"next\" Qt would be very nice.\n\nFor Qt 4.5, the focus is on performance. If you now think \"Yeah, they optimized it a bit\", you are plain wrong. I tried the \"-graphicssystem raster\" switch, and IT FLIES! Even much faster than Qt 3 for sure.\n\nNow only KDE 4 needs to pick up the same path, and optimize icon loading and KWin speed. Feature-wise, KDE 4 is superior to KDE 3 already. Really, try current trunk.\n\nThanks to all involved, 2009 is going to be an awesome year, I wish all the best!\n"
    author: "christoph"
  - subject: "Re: Qt 4"
    date: 2009-01-02
    body: "I hope that because actually kde 4.1.87 sucks on my computer, is slow. I have a core2duo and an intel graphic card."
    author: "Pepe"
  - subject: "Re: Qt 4"
    date: 2009-01-03
    body: "Ok, I'm only using 4.1.85 aka Beta2 on an Intel Atom with Intel graphic card and its fast enough. Speed is not the problem, here everything is quite stable (-> back to standard KDE-3 beta release quality) and featurewise its sooo much more complete than 4.1.3. But there is one top missing (notebook) feature: kbluetooth4 + bluez-4.x. I'm still searching for a working setup for kbluetooth4 and bluez-3.\n\nBye\n\nThorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Qt 4"
    date: 2009-01-03
    body: "I use trunk, it's very slow and plasmoids crash all the time. I really hope 4.3 will be better but given the state of 4.2, I'm afraid 4.4 will be the first great and usable release, not 4.3 :/ Kopete still needs a lot of work and no VoIP or video with jabber, konversation hasn't even been ported, khtml still don't work with big sites such as digg and google apps, amarok2 new playlist sucks (that will be fixed before 4.4 though), strigi/nepomuk is still very buggy (searching by tag don't return anything here) and no app uses them, kate is still buggy and lags a lot compared to gedit or textmate, kdevelop4 hasn't been released yet, digikam still very buggy etc.\n\nSo yes, KDE 4.2 and probably 4.3 won't be The release, anyway 4.4 should have most features back, more stability and will be faster thanks to Qt4.5, that's still a full year of buggy/slow desktop to cope with :/"
    author: "kubunter"
  - subject: "Re: Qt 4"
    date: 2009-01-03
    body: "> that's still a full year of buggy/slow desktop to cope with :/\n\nWhat do you see buggy/slow about KDE 3.5?\n\n:-?"
    author: "slacker"
  - subject: "Re: Qt 4"
    date: 2009-01-03
    body: "You understand that KDE 4.2 is still in beta, right? So if anything crashes on your system, wait at least until the final release of KDE 4.2 before you say that it sucks.\nBTW: Your user name suggests that you use Kubuntu. It's a well known fact that Kubuntu ships broken KDE packages. I use the trunk packages provided by openSUSE. While they are not flawless (KDE 4.2 is beta and the SUSE guys can't do magic), my KDE 4.2 experience is by far not nearly as bad as yours."
    author: "The Devil"
  - subject: "Re: Qt 4"
    date: 2009-01-03
    body: "Whatever, I'm not going to change distros everytime KDE makes a release, also kubuntu packages for kde 3.x were great. I also heard that gentoo and mandriva package were buggy. So I blame it on KDE."
    author: "kubunter"
  - subject: "Re: Qt 4"
    date: 2009-01-03
    body: "KDE does not make any packages. If you use in Kubuntu 4.2 beta2 from\ndep http://ppa.launchpad.net/kubuntu-experimental/ubuntu intrepid main\nyou get also nearly upstream-stream quality packages. But you have to pay attention that you do not mix 4.1.2, 4.1.3 and 4.1.85 packages altogether: This leads to a crashy desktop. Its easy to overcome these problems using aptitudes problem solver.\nBut you do not get the qualitiy packages like you have as opensuse user for sure. \nIn Kubuntu you have to compile digikam 0.10pre and kdebluetooth yourself as these packages are too old or incompatible.\n\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Qt 4"
    date: 2009-01-03
    body: "First of, if one distributions release packages with bugs not existing in other distributions or when compiling from source, the distribution is the right place to put the blame not KDE. That should be rather obvious.\n\nWhen it comes to Gentoo their stability are always an issue, very depending on the specific options used when setting up the system. Getting a unstable system is easy. \n\nAs for Mandriva most sources report their packages of KDE to be excellent and among the best there is, so where you got your information from sounds rater suspect. On the other hand most reports of Kubuntu marks their packages as sub-par. Specially the KDE4 ones, but also the KDE3 ones had a generally bad reputation. The amount of distribution specific bugs also leads to suggesting the claims are true."
    author: "Morty"
  - subject: "Re: Qt 4"
    date: 2009-01-03
    body: "> the distribution is the right place to put the blame not KDE\n\nNo, if KDE makes it incredibly complex to build package so that even competent capable devs such as kubuntu's, gentoo's or else can't do it right then KDE is to blame. All packages on ubuntu are great except for KDE, same for gentoo and other distros. And all the packagers people I talked to always told me that KDE was a bitch to package, and that since KDE 3.X. I'm not saying KDE should provide packages, that the distro job, all I'm saying is they should make packagers job easier and they are obviously failing there. Blaming the distro for not getting an obviously too complex architecture that is KDE is doing it wrong."
    author: "kubunter"
  - subject: "Re: Qt 4"
    date: 2009-01-04
    body: "And again you reveal yourself as a troll. If KDE are so hard to package, the Kde-buildsystem mailing-list should have been filled with request for improvements and pleas for help by packagers, but this does not seem to be the case.\n\nAnd historically KDE has had a reputation to being rather easy to package, a evidence shown in that nearly all distributions with one or very few packagers chose KDE. And several get praise for the quality of their KDE desktops, for instance PCLinuxOS, Slackware, PC-BSD and DesktopBSD. "
    author: "Morty"
  - subject: "Re: Qt 4"
    date: 2009-01-04
    body: "> And historically KDE has had a reputation to being rather easy to package, a evidence shown in that nearly all distributions with one or very few packagers chose KDE. And several get praise for the quality of their KDE desktops, for instance PCLinuxOS, Slackware, PC-BSD and DesktopBSD.\n\nDoes it include KDE4 packages?\n\nAlso does it mean that kubuntu packagers are incompetent? or that they are not numerous enough? There are like 15 so it should be enough, if 15 people are not enough than something must be wrong with KDE cause I don't think they're all incompetent, same goes for other distros that don't get good package for KDE."
    author: "kubunter"
  - subject: "Re: Qt 4"
    date: 2009-01-07
    body: "Yes, that includes kde4. In Arch Linux, there is one guy maintaining all the KDE packages + most of the dependencies (and he only does it in his spare time), Pierre S. Then you have another guy making nightly KDE trunk snapshots, also by himself, Mark C. And then you have the kdemod project (or chakra), which consists of two core packagers for kde4 + two kde programmers (plus some packagers for kde3 and extragear programs). (All these are very well-maintained, and I can't remember hearing about any breakage they introduced.)\n\nI won't comment on why the kubuntu packages suck, but debian packaging is unnecessarily complex, imho."
    author: "archer"
  - subject: "Re: Qt 4"
    date: 2009-01-07
    body: "> Also does it mean that kubuntu packagers are incompetent?\n\nYes, their incredible incompetence is a well known matter of fact. They don't package kde, they 'pimp' it in a simply stupid way of thinking. I'm using Kubuntu for a long time, an 90% of all kde-probles can be solved just by removing the kubuntu-specific-packages (kubuntu-desktop-settings and such).\n\nOk, to be fair, half of the trash comes from ubuntu self, with their stupid translations-must-fill-into-our-stupid-system-philosophy and such."
    author: "Chaoswind"
  - subject: "Re: Qt 4"
    date: 2009-01-03
    body: "As a mandriva 2009, KDE 4.1.3 user, I certify that the desktop is very stable. In fact more stable than opensuse 11.1, which I recently installed on a different partition.\n\nAs for kubuntu, I never gave it another thought, EVER. Yes, I tried it and it is shitty. No need to go by hype. And I didn't.\n\nI am a serious linux user - I use linux at work AND home. Mandriva and opensuse get the job done. I've been doing this for years."
    author: "vm"
  - subject: "Re: Qt 4"
    date: 2009-01-04
    body: "I generally found the kubuntu packages for kde3 broken in some way or another. And whilst I really like low level ubuntu (much nicer than bloated suse), their kde packages leave a lot to be desired.\n\nNow if I could only find a decent stable debian based (or fast...without broken dns like suse) with decent kde packages I would dump opensuse too."
    author: "txf"
  - subject: "Re: Qt 4"
    date: 2009-01-07
    body: "archlinux isn't debian based, but the package manager is excellent (and extremely fast, installing 204 packages took less than 10 seconds here, when the packages were already downloaded), and the whole distro is based on the \"KISS\" principle, which means that elegance and simplicity all the way to the bottom (from the linux kernel patches to the kde packages)."
    author: "archer"
  - subject: "Re: Qt 4"
    date: 2009-01-03
    body: "digikam, amarok, konversation, and kdevelop aren't part of KDE. They have their own release schedules."
    author: "whatever noticed"
  - subject: "Re: Qt 4"
    date: 2009-01-03
    body: "Have you reported all bugs?\nBecause currently the bugfix rate is extremely high. There are really only very few crashbugs left on my computer, and i know that they come from an unpatched Qt.\n\nSo, please report the bugs, and 4.2 will be great. (well, it already is, but better is always good :-) )"
    author: "Beat Wolf"
  - subject: "Bug numbers?"
    date: 2009-01-03
    body: "How are the numbers for open bugs/wishes calculated? I've just noticed these numbers on commit digest from 21st Dec:\n\nOpen Bugs:    16362 \nOpen Wishes:    14661 \nBugs Opened:    481 in the last 7 days. \nBugs Closed:    700 in the last 7 days.\n\nversus these from\n\nOpen Bugs:    16361 \nOpen Wishes:    14661 \nBugs Opened:    487 in the last 7 days. \nBugs Closed:    707 in the last 7 days. \n\nAs far as I have noticed, the numbers are not actually right if we take into consideration dates for which the digests are composed. Is this a small mistake on Danny's part or are there no means for querying bugzilla on open bugs in the past? Maybe some kind of a cronjob would help gathering proper statistics? This would improve accuracy of digests - even if composed with delays, the data could still be collected and stored for future use.\n\nI am by no means complaining, just asking out of curiosity. I enjoy reading Danny's digests and wading through numbers.\n\n//sorry for my bad english"
    author: "Anon"
  - subject: "Re: Bug numbers?"
    date: 2009-01-03
    body: "bko (bugs.kde.org) appears to make an interesting calculation. Within the last 7 days\n\n517 bugs opened were opened, 2127 bugs were closed that *should be a drop of open bugs below 14k.\n\nthough bko reports 16287 open bugs. \n\nah, yea, and whoever finex is, respecto. same applies to john layt & others, but what you did is beyond madness : )"
    author: "Jay"
  - subject: "Re: Bug numbers?"
    date: 2009-01-03
    body: "finex closed most of the bugs as \"WONTFIX\" so this is not really fixing , but hey the database should be cleaned up right - please use the feature to autoclose bugs if the reporter does not care about the reported in 1 release cycle, to not have already resolved bugs there..."
    author: "sfdg"
  - subject: "Re: Bug numbers?"
    date: 2009-01-04
    body: "As I have said before, \"Bugs Closed\" is not a good metric, since it encourages bugs to be closed without a good resolution.  In most cases, a good resolution is fixing them.  So, I would suggest that \"Bugs Fixed\" be separated out so that we show:\n\nBugs Resolved Fixed\nBugs Resolved Other\n\na further note here that we don't actually close bugs since we have no QA department to do so.\n\nThe present figures for bugs closed have no real meaning since most of the bugs closed are KDE-3 bugs which are being closed because they are obsolete.  They should probably be actually removed after they have been marked resolved for a year."
    author: "JRT"
  - subject: "Re: Bug numbers?"
    date: 2009-01-03
    body: "My guess is that the numbers are fetched when the digest is generated. With the short time between the digests the numbers don't change much."
    author: "AC"
  - subject: "Thanks!"
    date: 2009-01-03
    body: "So you finally caught up! Great! Thanks, Danny, for your amazing work!"
    author: "Alejandro Nova"
  - subject: "The Link for Danny"
    date: 2009-01-03
    body: "http://www.picturepush.com/public/1296385\n\nwell done, sir, well done!"
    author: "Jay"
  - subject: "Knotify crashers"
    date: 2009-01-03
    body: "So when are we going to nail down showstoppers in plasma, kded, file opening freezing plasma and thus rendering the system inoperative?\n\nI leave the latest SVN in an unused mode triggering the kscreensaver and within 20 minutes I've got 2 or more knotify bug errors."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Knotify crashers"
    date: 2009-01-04
    body: "Of course, you are familiar with http://bugs.kde.org"
    author: "Stefan Majewsky"
  - subject: "Re: Knotify crashers"
    date: 2009-01-04
    body: "If the devs are not able to detect these bugs that are so obvious and so annoying to anyone who has tried 4.2, then really kde is hopeless."
    author: "kubunter"
  - subject: "Re: Knotify crashers"
    date: 2009-01-04
    body: "Indeed. Maybe you can go into the xorg lists and tell them how hopeless they are.\n\nI'm sure you will be appreciated as much there for your wisdom and perspicacity.\n\nHave you started writing your report yet?\n\nDerek\n\n"
    author: "dkite"
  - subject: "Re: Knotify crashers"
    date: 2009-01-04
    body: "\"If the devs are not able to detect these bugs that are so obvious and so annoying to anyone who has tried 4.2\"\n\ni'm sure you realize that the developers do not have every hardware or software set up available to us that you do. we do not use the software in the same way everyone else does. we don't even use the same builds most people do (we tend to use debug builds, built ourselves from source, from svn trunk/; most people use binaries from releases), etc.\n\nso it is very, very common that others run into issues we have either not seen or not noticed.\n\nif you feel that is hopelessness, then please organize a professionally staffed and stocked Q/A department to do all that work for both you and i.\n\npersonally, i'd just as soon see you spend a few minutes telling me what bugs you run into so i can either fix them or let you know they are already fixed."
    author: "Aaron Seigo"
  - subject: "Re: Knotify crashers"
    date: 2009-01-04
    body: "\"down showstoppers in plasma, kded, file opening freezing plasma and thus rendering the system inoperative?\"\n\nwhich showstoppers are these? amazingly, i can neither read your mind nor see your computer from a distance, so you'll have to help us out here.\n\nif by \"file opening\" you mean \"i click on an icon, and it freezes for several seconds before things launch\" and you are using nvidia binary drivers, then that's not a plasma bug.\n\nbut then, i really don't know what you mean exactly by \"file opening\". you could mean \"when i open a file dialog from a url requester, such as in the settings dialog in the folder view plasmoid\". that's a bug in KUrlRequestor in libkdeui and one i plan to fix in 4.3. it's not a trivial issue, unfortunately.\n\n\"2 or more knotify bug errors.\"\n\nwhich would be .... ?\n\nseriously, if you want a better 4.2, communicate with a bit of clarity. keeping your secrets to yourself doesn't help anyone, including yourself."
    author: "Aaron Seigo"
  - subject: "Was: {Knotify crashers} WGG & WPD"
    date: 2009-01-04
    body: "I have not had any crashes lately, but in direct response to what you said:\n\nIn either KFM or Dolphin, I right click on a file icon and it takes a minute for the dialog to pop up.  Yes, I realize that there is something seriously wrong.  So, I did a total reinstall of KDE TRUNK and it made no difference.\n\nDoes anyone else have this issue?  \n\nDon't know if this is a show-stopper, but for me, it means that KDE-4.2 is totally unusable except to play the games."
    author: "JRT"
  - subject: "Re: Was: {Knotify crashers} WGG & WPD"
    date: 2009-01-04
    body: "\"In either KFM or Dolphin, I right click on a file icon and it takes a minute for the dialog to pop up. Yes, I realize that there is something seriously wrong. So, I did a total reinstall of KDE TRUNK and it made no difference.\n \nDoes anyone else have this issue?\"\n\nI've not seen this, nor seen anyone else complain about it.  Is there CPU load when you're waiting, or does it look like it's waiting for a response from something and then just timing out?"
    author: "SSJ"
  - subject: "Re: Was: {Knotify crashers} WGG & WPD"
    date: 2009-01-04
    body: "This happens when an app tries to communicate with the dbus server, fails, times-out, and launches a new server.\n\nI have had that a few times, but I suspect that this is an issue with the server."
    author: "hmmm"
  - subject: "Re: Was: {Knotify crashers} WGG & WPD"
    date: 2009-01-04
    body: "Nope, only about 2 seconds on my 1.2 Athlon with a GeForce2 using KDE4.1.87 on OpenSuse 11.1\n\nTime for a new computer I think."
    author: "Damiga"
  - subject: "Re: Was: {Knotify crashers} WGG & WPD"
    date: 2009-01-05
    body: "Well, my Athlon is only 1.1 GHz so it is obviously a software problem."
    author: "JRT"
  - subject: "Re: Was: {Knotify crashers} WGG & WPD"
    date: 2009-01-07
    body: "> Does anyone else have this issue? \n\nNot a minute, but a disturbing 3-4 second-long-pause, yes. The sames goes for drag'n'drop of files, where also comes *always* those stupid plasma(?)-dialog of the notify(?)-system, which tells always how stupid it trys to read the target, which grown into a unhealty lag and make at the end every simple action into a longmere nightmare :(\n\nWay away from usable IMHO."
    author: "Chaoswind"
  - subject: "Horrible, Old, Ugly Artwork"
    date: 2009-01-04
    body: "You may have noticed that some of the artwork has been removed from KDEArtWork.\n\nThere is a problem in KDEArtWork in that a lot of it was not promptly ported to KDE-4.  I guess because either people didn't bother or because there was no agreement on what to do.\n\nI don't know what the new troika that is now running KDEArtWork plans to do, but those of you that like and use that old and ugly artwork better speak up now before it disappears."
    author: "JRT"
  - subject: "Re: Horrible, Old, Ugly Artwork"
    date: 2009-01-04
    body: "I can live without them, only use Plasma stuff ;)"
    author: "Bobby"
  - subject: "Re: Horrible, Old, Ugly Artwork"
    date: 2009-01-05
    body: "Well then you aren't someone that needs to speak up, are you?\n\nNote that if you use KDE-4 that you can't use the stuff that hasn't been ported to KDE-4 yet, can you?"
    author: "JRT"
  - subject: "Re: Horrible, Old, Ugly Artwork"
    date: 2009-01-05
    body: "You do realise they've moved it over to kdeartwork-classic?  Or that you can probably add them through GHNS???"
    author: "Odysseus"
  - subject: "Re: Horrible, Old, Ugly Artwork"
    date: 2009-01-06
    body: "Won't help much if they aren't ported.\n\nBTW, where is \"kdeartwork-classic\"; Google can't find it!"
    author: "JRT"
  - subject: "Kiosk Mode?"
    date: 2009-01-04
    body: "I know it's been said here (and I think on Aaron's blog) that the underlying configs exist for putting KDE4 into a \"Kiosk Mode\" and/or to just lock down the desktop in general, but, will a pretty GUI for doing it exist for KDE4?  The is one for KDE3, it's old and un-maintained, but it works.  Is there any future plans to include such a tool for '4, or (at this point in time) will the only way) to lock down a desktop be through hand-editing config files?\n\nThanx,\nM."
    author: "Xanadu"
  - subject: "Re: Kiosk Mode?"
    date: 2009-01-04
    body: "Put in this way: all the devs want one, but all are (far) too busy with their own projects.  If someone steps up to write one, they will be welcomed with open arms; if no one steps up to write one, then it won't get written.  It's unfortunately impossible to be more definite with volunteer-driven development :/"
    author: "Anon"
  - subject: "Re: Kiosk Mode?"
    date: 2009-01-04
    body: "Can somebody teach me to code? i have time and I would love to do something to help the community."
    author: "Bobby"
  - subject: "Re: Kiosk Mode?"
    date: 2009-01-04
    body: "I found the Qt tutorials very good. And before that any c++ tutorial on the web will give you the basics (basically, I learnt c++ by coding my own pet project and reading Stroustrup's book. But style, you can learn by looking at Qt ;) ). \n\nThis will take you a couple of weeks.\n\nThen, well... Good luck :)\n\nAnd have fun"
    author: "hmmm"
  - subject: "Re: Kiosk Mode?"
    date: 2009-01-04
    body: "He could also use PyKDE/ Korundum if he finds C++ too daunting :)"
    author: "Anon"
  - subject: "Re: Kiosk Mode?"
    date: 2009-01-04
    body: "First learn C++ (at least the basics), then take a look at http://aseigo.blogspot.com/2007/09/getting-into-kde-hacking.html (basically: Qt tutorials, KDE Techbase Getting Started and tutorials, keep the API for both Qt and KDE handy and find some easy stuff to get yourself familiar with the API's)"
    author: "Pieter Bootsma"
  - subject: "Re: Kiosk Mode?"
    date: 2009-01-05
    body: "Thanks for the tips. I have heard that C++ is very difficult to learn but so was the first step in life. Nothing good comes easily so this will be my New Year's resolution."
    author: "Bobby"
  - subject: "WAS {Kiosk Mode?} Learn to Program"
    date: 2009-01-05
    body: "Any good book can teach you to code.\n\n\"Thinking in C++\" is available free on the web.\n\nHowever, learning to write programs is not the same as learning to code and it is considerably more difficult.  It is something that you can really only learn by practice.\n\nLearning to use a GUI toolkit is also something that is not as easy as learning to code.  The Trolltech documentation for Qt is probably the place to start.  Someone suggested that I purchase a book, but I found it to be what I call an \"empty book\" -- doesn't tell you how to do anything.  \n\nThe big hurdle is KDE programing.  Although there are now more tutorials:\n\nhttp://techbase.kde.org/Development/Tutorials\n\nthey are still a little thin, and the actual API documentation is quite terse and not well indexed."
    author: "JRT"
  - subject: "Re: Kiosk Mode?"
    date: 2009-01-05
    body: "I'd strongly recommend learning Python, PyQt, and PyKDE.  It's a vastly easier to use and learn language for a complete newbie.\n\nThere's a couple of books I'd recommend, one free but one for pay.  For learning  Python in general try the free download at ttp://www.greenteapress.com/thinkpython/.  Then to learn PyQt buy http://www.qtrac.eu/pyqtbook.html .\n\nGood luck!\n\n"
    author: "Odysseus"
  - subject: "Re: Kiosk Mode?"
    date: 2009-01-06
    body: "Thank you for the reply.  I guess that's about as \"bottom line\" as one could ask for!  :-)\n\nThanks, man.\n\nM.\n"
    author: "Xanadu"
---
In <a href="http://commit-digest.org/issues/2008-12-28/">this week's KDE Commit-Digest</a>: Continued post-release polishing of <a href="http://amarok.kde.org/">Amarok</a> 2.0. Support for conversion between temperature scales in the "Weather" Plasmoid. Initial import of "System Status" <a href="http://plasma.kde.org/">Plasma</a> applet, which is renamed "System Load Viewer" (shows CPU, memory, and swap usage). Support for on-the-fly compilation of Plasma data engines with the C# language bindings. Further development of the "BookmarkSync" <a href="http://www.konqueror.org/">Konqueror</a> plugin, including additions of autofetch, autosave, and a settings dialog. More refined integration of Wikipedia "place" information in <a href="http://edu.kde.org/marble/">Marble</a>. A QtScript engine to allow creation of games using QtScript in KGLEngine. An experimental "curve brush" added to <a href="http://www.koffice.org/krita/">Krita</a>. Support for the ext4 filesystem added to PartitionManager. More reorganisation of wallpapers in KDE SVN, with a general kdeartwork module cleanup removing IceWM themes, KWorldClock maps, and unmaintained iconsets. PolicyKit-KDE moves to extragear/base. <a href="http://eigen.tuxfamily.org/">Eigen</a> 2.0 Beta 3 is tagged for release.<a href="http://commit-digest.org/issues/2008-12-28/">Read the rest of the Digest here</a>.

<!--break-->
