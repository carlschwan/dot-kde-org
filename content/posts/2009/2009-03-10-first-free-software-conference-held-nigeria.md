---
title: "First Free Software Conference Held in Nigeria"
date:    2009-03-10
authors:
  - "jriddell"
slug:    first-free-software-conference-held-nigeria
comments:
  - subject: "cool"
    date: 2009-03-10
    body: "This is incredibly cool! I really respect the organization for pulling this one off! Sounds like it was a very interesting event.\r\n\r\nYet I still have a hard time believing white guys can dance - at least, Ade and Riddell? I've got to see that with my own eyes..."
    author: "jospoortvliet"
  - subject: "Awesome"
    date: 2009-03-10
    body: "Really Great to see that FOSS and KDE also get attention in Africa."
    author: "blueget"
  - subject: "Keep up the project and make Nigeria Open S capital"
    date: 2009-03-11
    body: "Well done for KDE org for coming to Nigeria. This should be the start to bring talented skills into open source and make Nigeria a power house of Africa or the west on the open source.\r\n\r\n\r\n "
    author: "ggg1"
  - subject: "KDE in Nigeria?"
    date: 2009-03-12
    body: "Wow...guys how much publicity was done for this event? I can't seem to see it anywhere Online, Facebook or in any newspaper...\r\n\r\nThis is NOT the first FOSS conference in Nigeria. There has been numerous FOSS events in Nigeria. In Lagos and in other parts of Nigeria. Software freedom day is a popular one (http://softwarefreedomday.org/). It happens every year and Nigeria participates actively. \r\n\r\nI wonder why this was kept on a low profile and limited to Kano. If it was well publicized I'm sure the hall would be too small for attendees."
    author: "edward_nig"
  - subject: "YES! KDE Live in Kano State of Nigeria."
    date: 2009-03-16
    body: "The Internet as you know is like the ocean, Turbulence on  one part doesnt startle all fishes in the ocean. Anyway, apart from a live website solely for the conference www.foss.hutsoftnigeria.com , lots of posts were sent out on lots of KDE sites, blogs, KDE wikis and the likes. Moreover, We sent out invitations to lots and lots of computer firms in Abuja, Lagos, Kano, Kaduna and many other states of the federation and an ample amount actually graced the occassion. The Gentlemen of the Press were not left out of the event as well. \r\nI feel there is a kind of uncoordination in the way and manner that we've been handling things of this nature. Take for instant the SFD, obviuosly your impression is that the entire country I mean Nigeria have been participating. But if you ask me, I'll say it's states within the Lagos axis that probably have an insight into it. Its good this forum has provided a platform for me to meet FOSS enthusiast within my abode. One way to start is to find a way by which we can collaborate to actualize the FOSS dream in our great country and beyond. \r\n..and on a light note, I'm happy you didnt know cause I wouldnt have known who to throw out to accommodate you...joke!\r\n "
    author: "tatauwal"
  - subject: "Nigeria an Open Source Kapital"
    date: 2009-03-16
    body: "I believe that is going to come sooner than we envisage. The rate at which Bayero University students have been trooping to my office to seek for guidance on open source software is so alarming. Infact lots have signified interest in doing their thesis on open source. I say we're brazing up for lots of nteresting FOSS activities. Thumbs up for aspiring  Dudes of Centre for Information Technonology Bayero University Kano, Dudes of Hutsoft, Dudes of KDE. "
    author: "tatauwal"
  - subject: "Great all the same"
    date: 2009-03-17
    body: "Well done all the same. You can reach me through blog [dot] blueidentity [dot] com.\r\n\r\n"
    author: "edward_nig"
---
<img src="http://static.kdenews.org/jr/2009-03-nigeria-top.jpg" width="240" height="180" align="right" />
The first <a href="http://foss.hutsoftnigeria.com/">Nigerian conference on Free and Open Source Software</a> was held this week in Kano, Nigeria. The conference featured local speakers, consultants, network engineers, system administrators and academics, and international guests from KDE for three days at Bayero University of Kano. Over 500 students and professionals attended, filling the hall to capacity.
<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; float: left">
<img src="http://static.kdenews.org/jr/2009-03-nigeria-group-photo.jpg" width="300" height="225" align="left" />
Group Photo
</div>

The honourable commissioner for science and technology Dr Bashir Galadanci opened the conference on Friday morning, referring to his experiences with Free Software in Italy in the 1990s; he expressed hope for the future of Free Software in Nigeria and encouraged all those present to develop their technical skills. The director of the Centre for Information Technology, Dr. Muntari Hajara Ali, echoed these sentiments and stressed that the attendees are in the forefront of the effort to domesticate IT and giving Free and Open Source software a place in the Nigerian economy.

Not all of the scheduled speakers were able to attend the conference, visa and travel issues made some of the guests unable to reach Kano on time. Fortunately the schedule was flexible enough to accommodate this, and other speakers filled in the gaps that were formed.

<div style="border: thin solid grey; padding: 1ex; margin: 1ex; float: right">
<img src="http://static.kdenews.org/jr/2009-03-nigeria-ade-jonathan.jpg" width="300" height="225" />
Ade and Jonathan
</div>
Conferences in Nigeria are affected by power cuts just like everything else; sometimes it is necessary to shout instead of speaking into a microphone, and some talks that depended too heavily on slides were disjointed. All were appreciated by the audience, and a UPS kept the projector running through some of the outages. Topics ranged from the highly technical like Adriaan de Groot's talk on software checking tools, to the general and introductory such as Ibrahim Abubakar Dasuma's introduction to Free Software applications. These addressed the wide range of interests and abilities of the attendees. Jonathan Riddell's talks were interspersed with handing out stickers to audience members who gave correct answers to questions.

<div style="border: thin solid grey; padding: 1ex; margin: 1ex; float: left">
<img src="http://static.kdenews.org/jr/2009-03-nigeria-dancing.jpg" width="300" height="225" align="left" />
Dancing at the social event
</div>

The conference concluded with a cultural display at the Kano Museum opposite the palace of the Emir of Kano; there is photographic evidence that white men can dance if you make them.  <a href="http://www.flickr.com/photos/jriddell/sets/72157614991241443/">Photos (but no dancing)</a> shows you more.