---
title: "FOSDEM 2009: Devroom Speakers Wanted"
date:    2009-01-05
authors:
  - "bcoppens"
slug:    fosdem-2009-devroom-speakers-wanted
comments:
  - subject: "Wrong date in article"
    date: 2009-01-05
    body: "The date in the article is wrong - you're still living in the year 2008 ;-)"
    author: "eliasp"
  - subject: "Re: Wrong date in article"
    date: 2009-01-08
    body: "Yes, next year would be 2010 (not 2008)."
    author: "Erik"
  - subject: "Proposals for talks I can give"
    date: 2009-01-05
    body: "\nI want to be a speaker.\n\nHere are my proposals:\n1) Enterprise wide migration back to KDE3\n2) God gave us Debian: Debian is still on KDE3 ( A talk for religious people )\n3) A workshop for Use Case driven deployment and configuration of KDE3\n4) Necromancy in the Free Software Scene: KDE3 back from the Dead\n5) Workshop : KDE3 for Roleplaying people ( A talk for all D&D players )\n6) IA!IA! KDE3 ! Ftaghn!\n7) Workshop: How to build a community of KDE3 fans to wipe out KDE4 development\n....\n\nI can post many more proposals if you want. Just let me talk for KDE! Okay?\n\nThanks."
    author: "Jayy the Gayy"
  - subject: "Thanks for trolling"
    date: 2009-01-05
    body: "Shouldn't you be spending your time fixing and maintaining KDE3?\n\nI personally like to take my 5 minutes of well earned break to feed the tolls."
    author: "Bart Cerneels"
  - subject: "Re: Proposals for talks I can give"
    date: 2009-01-05
    body: "There also is debian with kde4 ;)\n\nhttp://pkg-kde.alioth.debian.org/kde4livecd.html"
    author: "Moo Moo!"
  - subject: "Re: Proposals for talks I can give"
    date: 2009-01-05
    body: "There's also another one\n\n8) How KDE 3 is not going anywhere. You have the source. Go to town with it if you like, just don't force others to maintain it because you want it.\nThat's the beauty in opensource, you can do it, or you can even hire people to do it for you."
    author: "IAnjo"
  - subject: "Re: Proposals for talks I can give"
    date: 2009-01-07
    body: "KDE3: the Requiem would be more appropriate... as vampires are undead and live forever.  The code is there, you are there, keep it running.\n\nThere were some big issues with KDE3, which is why there was a fundamental rewrite that took quite awhile to complete and resulted in applications still catching up to where they were.  In the meantime, just keep using whatever you want.  I know I'm still using KDE3... and perfectly happy with switching to KDE4 at some point in the future.  I have to restart aRts now and then and a few other quirks, but I'm getting my work done quite nicely.\n\nWhy all the talk about raising KDE3 from the dead?  It's still quite alive, and isn't going anywhere."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Proposals for talks I can give"
    date: 2009-01-07
    body: "[[There were some big issues with KDE3, which is why there was a fundamental rewrite]]\n\nThat's a strawman.. Of course I don't think that anybody wants to go back to KDE3's sound system, but the biggest issue of KDE4 (if we forget the KDE4.0 release naming) is Plasma and I haven't read any convincing argument why it was necessary to do a rewrite for this part..\n"
    author: "renoX"
  - subject: "Re: Proposals for talks I can give"
    date: 2009-01-13
    body: "Since Novell is the prime sponsor, maybe someone can do a talk about the need for KDE to go all Mono, all the time and the need for KDE to pay royalties for the IP it is stealing from poor proprietary companies.\n\n</trolling>"
    author: "Randall Schwartz"
---
As always, KDE will have a presence at next year's
<a href="http://www.fosdem.org/2009/">FOSDEM</a> in Belgium on 7-8 February 2008.
This is earlier in the month than usual.  We are looking for people to give talks in the KDE or cross-desktop devroom.  FOSDEM is <em>the</em> European meeting of free software developers, with over 4000
visitors, 200 lectures and lots of stalls to visit over a 2 day period. You
can come and listen to a plethora of interesting talks about anything related
to free software, and meet the people behind the nicknames.  



<!--break-->
<p>On Sunday, we will be sharing a room with the developers from Gnome and
XFCE. This means that just like last year, we are also interested in talks
that transcend free desktops in general. You can have a look at last year's
<a href="http://archive.fosdem.org/2008/schedule/tracks/kde">KDE</a> and
<a href="http://archive.fosdem.org/2008/schedule/tracks/crossdesktop">Crossdesktop</a> talks to have an idea about what kind of talk we might find interesting to
do in the devrooms.</p>

<p>For more information including accommodation, see the <a href="http://wiki.kde.org/tiki-index.php?page=FOSDEM2009">KDE at FOSDEM 2009 wiki page</a>.
If you want to give a talk, please contact Bart Coppens before January 10th.</p>

