---
title: "On Being Part of KDE"
date:    2009-12-15
authors:
  - "Stuart Jarvis"
slug:    being-part-kde
comments:
  - subject: "Where's the poll?"
    date: 2009-12-15
    body: "All the links just seem to go to the kde-apps homepage.\r\n\r\n+1 \"Powered by KDE\" btw. :)"
    author: "Bugsbane"
  - subject: "Silliness goes here"
    date: 2009-12-15
    body: "I think I need to start building packages 'Powered by [ade]'. On the KDE Platform. With KDE Technology. Free Software, too. Why doesn't the poll include an option 'Part of this nutritious breakfast'?"
    author: "adridg"
  - subject: "You can see the poll if you"
    date: 2009-12-15
    body: "You can see the poll if you scroll down (it's to the right)."
    author: "Hans"
  - subject: "Rebranding"
    date: 2009-12-16
    body: "As someone mentioned on the poll, with the rebranding a couple of the options don't make much sense...\r\n\r\n    * By the KDE Community\r\n\r\nAs KDE now means 'the community' this is redundant. Perhaps 'By KDE' but that's a little short.\r\n\r\n    * Powered by KDE\r\n\r\nThis one effectively means 'Powered by KDE (the community)' which is  nice if interpreted like that - but I suspect it isn't going to help the rebranding because of the immediate interpretation: 'Powered by KDE (the platform)'.  Perhaps 'Powered by the KDE platform' would cover that, but it's getting wordy now. \"Built on the KDE Platform\"?\r\n\r\n\r\n\r\n"
    author: "dalgaro"
  - subject: "\"you'll find the poll at the bottom of the right sidebar\""
    date: 2009-12-16
    body: "I did try and explain :-)"
    author: "Stuart Jarvis"
  - subject: "Yes..."
    date: 2009-12-16
    body: "I just replied to a couple of those types of points on the poll...\r\n\r\nRe \"Powered by KDE\" - we can help this by using appropriate people type graphics in accompanying banners/badges, perhaps. Or suggesting a link that goes to a page on kde.org explaining the community\r\n\r\nBuilt on the KDE Platform is one of the options, of course.\r\n\r\nWe'll need a couple I think - one is \"I'm part of KDE and I made this app\" the other is \"I used the KDE platform to make this app but I don't necessarily consider myself as part of KDE\""
    author: "Stuart Jarvis"
  - subject: "Thanks Ade, I'll oblige:"
    date: 2009-12-16
    body: "\"Made after drinking the KDE Kool-Aid\" ;-)"
    author: "Stuart Jarvis"
  - subject: "Powered by KDE"
    date: 2009-12-16
    body: "My immediate reaction was Powered by KDE was by far the snappiest phrase but that it weakened the KDE is a community message of the re-branding so, somewhat reluctantly, voted for Free Software from KDE.\r\n\r\nIf you Google \"Powered by Mozilla\" you will find a whole page of FAQs covering very similar ground. Mozilla clearly sees itself as a community but wanted a means for third parties to acknowledge their use of Mozilla technologies e.g. Gecko. They also wanted Mozilla to have a clear identity apart from Firefox (echoes of KDE and the Plasma Desktop).\r\n\r\nIt would seem they are prepared to somewhat overload the meaning of Mozilla to encompass both the community and its technologies. So I think if KDE marketing took a similar approach in explaining a \"Powered by KDE\" logo then it could work very well.\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n"
    author: "trevorl"
  - subject: "3rd party?"
    date: 2009-12-16
    body: "I'm thinking this post just confuses the issues more; as KDE is the group of people making the software then what does it mean to write software that uses the KDE platform?  Well, you become part of the KDE community.\r\n\r\nSo my favorite would be;\r\n<ul>I am KDE</ul>\r\n\r\nEasy to turn into a button too! ;)"
    author: "zander"
  - subject: "Powered by KDE"
    date: 2009-12-16
    body: "I really like \"Powered by KDE\", and my interpretation when I read it is \"Powered by KDE technology\", so I think it would be OK to use this definition both for developers who are part of the KDE community and for \"independent\" developers."
    author: "taedium"
  - subject: "Ideally, yes"
    date: 2009-12-19
    body: "In my ideal world, I'd agree - anyone who makes free software on the KDE Platform should be able to consider themselves a part of KDE.\r\n\r\nHowever, I don't think everyone would, at least not immediately. Imagine I know nothing about KDE, but I come across the Platform and realise it is good and so use it to make an app that I release on my website. Then I find it's on KDE-apps (because someone uploaded it) and it is described as KDE software - well, I might think \"that isn't KDE's, that's mine!\" be mightily annoyed and resolve never to use the KDE Platform again if this mysterious KDE is going to claim my apps for themselves.\r\n\r\nSo I think we need and option that says \"I used KDE technology to make this application\" in addition to \"KDE (by which I mean me) made this app\". Of course, we should always hope to convert people using the first label to people who want to use the second label by making them feel a part of KDE."
    author: "Stuart Jarvis"
---

As part of the <a href="http://dot.kde.org/2009/11/24/repositioning-kde-brand">repositioning of the KDE brand</a> we want to give authors of third party applications a phrase to describe their relationship with the KDE community. Some authors may wish to associate themselves with the KDE community, others simply to acknowledge that their application is built on the KDE Platform or uses KDE technology so it is likely that we will recommend a choice of phrases to be used to meet these differing needs.

Possible phrases were discussed at <a href="http://dot.kde.org/2009/11/20/booth-web-and-marketing-sprint">the recent marketing sprint</a> and we now have a <a href="http://www.kde-apps.org">poll of our shortlist</a> on KDE-Apps.org awaiting your vote (you'll find the poll at the bottom of the right sidebar).

In alphabetical order, the options selected are:
<ul>
<li>Built on the KDE Platform</li>
<li>By the KDE Community</li>
<li>Free Software by KDE</li>
<li>Made with KDE technology</li>
<li>Part of the KDE family</li>
<li>Powered by KDE</li>
</ul>

Please <a href="http://www.kde-apps.org">vote</a> and help us choose. If you have a idea for an alternative phrase, please feel free to suggest it in the comments below. We'll probably choose a few of the most popular phrases based on the poll but will also look through the comments and, if we see an idea we particularly like, may use that too.

We would also love to hear from volunteers willing to help produce artwork and banners to accompany the chosen phrases.