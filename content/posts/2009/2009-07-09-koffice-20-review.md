---
title: "KOffice 2.0 Review"
date:    2009-07-09
authors:
  - "troy"
slug:    koffice-20-review
comments:
  - subject: "Release Early, Release Often"
    date: 2009-07-10
    body: "I bet that some people will say that KOffice went to bad direction when they followed the KDE4 release idea.\r\n\r\nThey will \"whine\" about how KOffice 2.0 should be released as 1.9x beta or similar until it would be ready (on 2.2-2.3 version etc).\r\n\r\n"
    author: "Fri13"
  - subject: "They have a point...."
    date: 2009-07-12
    body: "Releasing beta-quality product as a \"final\" release defies logic. Why bother with version numbers if you're going to do that?"
    author: "angrykeyboarder"
---
For those following the development of KOffice, one of the first <a href="http://www.techradar.com/blogs/article/reviewed-koffice-2-0-611852">reviews of KOffice 2.0</a> has appeared at the Tech Radar website.  KOffice 2.0 is the platform release for the KOffice 2 series, and as such, is not yet appropriate for all users. From the article: "Free software is often developed with the mantra 'release early, release often'. This is a great idea, because new tools can be tested, trialled and critiqued as they're developed [...] Many of the elements in KOffice 2.0 show great promise and we look forward to testing the suite once more when it's ready."
<!--break-->
