---
title: "Akademy 2009 Technical Papers Published: Research And Innovation In The KDE Community"
date:    2009-07-21
authors:
  - "jospoortvliet"
slug:    akademy-2009-technical-papers-published-research-and-innovation-kde-community
comments:
  - subject: "I have been waiting for this"
    date: 2009-07-21
    body: "Greetings,\r\n\r\nCan't believe this is just happening, since time ago I have been working on KDE for years and wanted to get involved with KDE ;however , as my profile (more researcher than developer) didn't suit so well I wasn't sure how I was able to contribute. I think the call for papers initiative in Akademy 2009, should be a starting point for the integration of researchers on KDE and others events should also include the call in order to propose new ideas / new approaches, that can later become good contributions as the Nepomuk did. Right now I am doing research on visual hand gestures recognition technologies, and have implemented a few demos based on xstroke's algorithm. If there is a way I can contribute with KDE let me know."
    author: "jaom7"
  - subject: "*whine alert*  Videos?"
    date: 2009-07-22
    body: "I know it hasn't that long, but I really want to get the videos from GCDS.\r\n\r\nWhat is the status?"
    author: "kragil"
  - subject: "Get in touch with the KDE"
    date: 2009-07-25
    body: "Get in touch with the KDE research team. I am sure they can give you a hand in finding your way around."
    author: "nightrose"
---

Over the last few years KDE has seen increased involvement of students and university researchers. While many universities still feel uneasy about working with Free Software, the open and welcoming attitude in the KDE community has already brought several scientific research projects to life. A prime example is of course the <a href="nepomuk.semanticdesktop.org/">Nepomuk project</a>, officially finished but still very much alive within the <a href="http://nepomuk.kde.org/">Free Software</a>- and scientific communities. Furthermore, many involved contributors make use of scientific research papers while looking for inspiration to solve the more complex problems involved in writing software. The Free Software community also contributes in a practical way to science: the <a href="avogadro.openmolecules.net/">Avogadro project</a>, grown out of the KDE educational application <a href="http://edu.kde.org/kalzium">Kalzium</a> develops an advanced molecular editor designed for use in computational chemistry, molecular modeling, bioinformatics, materials science, and related areas. Last Akademy, an <a href="http://weblog.obso1337.org/2009/call-for-participation-akademy-and-the-gcds/">initiative</a> was developed by <a href="http://behindkde.org/people/celeste/">Celeste Lyn Paul</a> to bring KDE and science even closer.

We conducted a short interview with Celeste, member of the KDE e.V. board, usability specialist within KDE and Senior Interaction Architect at User­-Centered Design, Inc. We also interviewed Laura Dragan, researcher at the Digital Enterprise Research Institute and the National University of Ireland, Galway and writer of a technical paper for Akademy 2009. They explained to us what the Technical Papers are about.

<b>Starting with Celeste, can you tell us what prompted this initiative?</b>

<i>Celeste:</i> "Many of us are familiar with White Papers, and although they may be limited in what they cover or quickly go out of date because they are static, they are often well-written and thoughtful, providing an excellent reference or state of technology. Research papers are similar, but usually includes a certain level of scholarship and methodology.

Some of us academic types have been talking about a place we could publish and promote KDE research -- detailed academic and technical papers describing KDE technology in a way that the community, academia, and industry could draw on them as a resource.

I suggested the idea to have technical papers at Akademy to supplement presentations, because that is the way some of my professional conferences try to bring in interest from the academic community. Often, those papers become important work that professionals learn from and reference in their practice.

Ultimately, we hope that something like this will also show the world that KDE is serious and methodical about technology development, and not a bunch of grass-roots hackers throwing stuff together and hoping it works out.

It will also help us grow a presence as a research topic in academia, possibly opening up new avenues for collaboration, technology research and development, and funding."

<b>What do you want to accomplish with this?</b>

<i>Celeste:</i> "Primarily, the goal is as I said, to make KDE a more serious player in terms of technology. We have some awesome stuff going on in KDE. If there were technical papers (more than simply documentation; a static, historical document) written about the Plasma framework, students and academics working on similar projects could and would reference and draw from what Plasma has done. Mucho kudos if KDE was to be taken seriously like that in a stuffy academic world.

A secondary goal is to extend a way for other types of contributors, such as students and researchers, to get involved in KDE. For example, we had 3 papers from contributors who wouldn't have normally come to Akademy. 2 were student papers who worked on KDE technology during an internship, and the other was a researcher who used KDE as a proof of concept for her work. I hope that their experience has brought them closer to KDE and will encourage them to continue contributing.

Finally, open source is a popular project topic for university students. Unfortunately, many of these really great projects never make it upstream. Some of them may be too abstract to have actual committed code, or others may just think their work is not good enough. Having a place where students can publish their work in order to spread their ideas without the pressure to 'produce committable code' will help KDE get fresh ideas as well as encourage a new type of contributor (researchers) to get involved in KDE. Fresh blood and new ways of thinking".

<b>Thank you, Celeste. Laura, can you tell us to what extend Nepomuk, since the academic project is over, is still alive  in the scientific community?</b>

<i>Laura Dragan:</i> "Although the EU project is officially over since December last year, Nepomuk is still rather widely used in the scientific community. The output of the project was supposed to be most of all an architecture blueprint and a reference implementation for the Semantic Desktop, to be used .. well.. as reference .. for future research and implementations. Also after the project was over <a href="http://www.oscaf.org">OSCAF</a> was created to carry on the standardisation work that needs to be done for the Semantic Desktop.

Nepomuk is still used by researchers in their work and referenced in papers. In DERI there is still much work being done on Nepomuk related projects or on projects that started from Nepomuk."

<b>What did prompt you to send in a technical paper?</b>

<i>Laura Dragan:</i> "I had this idea of an application ontology for some time now, and Sebastian Trueg had already initiated the discussion about one, so I joined in, talked with him about ways of implementing it and I thought it would benefit from some exposure at Akademy. Presenting it there is the best way of getting instant feedback from a lot of knowledgeable people. And putting it in writing helps to clarify ideas and really makes one think about what to say and which is the best way to say it."

<b>What do you think about the initiative, and do you think there is a future for cooperation between KDE/FOSS communities and the scientific world?</b>

<i>Laura Dragan:</i> "It's a great idea to have technical papers at Akademy. This might get more researchers involved, because, as we discovered with Nepomuk, the KDE community is very receptive to new and challenging ideas. And (also the other way around) there are good ideas in research that just need dedicated people to transform them into more than a research prototype."

<b>Thank you both, for giving us some insight in the what and why behind the technical papers for Akademy 2009!</b>

The authors of papers gave a 30 minute presentation during the Akademy conference sessions at the Gran Canaria Desktop Summit. We hope that their experience has brought them closer to KDE and will encourage them to continue contributing.

Below you will find an overview of the papers presented during Akademy 2009.

<ul>
   <li>  Laura Dragan and Siegfried Handschuh. <a href="http://akademy.kde.org/conference/papers/DraganL-Akademy2009.pdf">Semantic Context Menus in KDE.</a> </li>
   <li>  Eduardo Madeira Fleury and Caio Marcelo de Oliveira Filho. <a href="http://akademy.kde.org/conference/papers/FleuryEM-Akademy2009.pdf">The Unusual Suspect: Layouts for sleeker KDE applications.</a> </li>
   <li>  Peder Osevoll Midthjell, Sveinung Dalatun, and Anders Sandven. <a href="http://akademy.kde.org/conference/papers/MidthjellPO-Akademy2009.pdf">Mobile Broadband: An Analysis and Improvement of Client-Side Connections to Mobile-Operators in KDE.</a> </li>
   <li>  Artur Duque de Souza. <a href="http://akademy.kde.org/conference/papers/SouzaAD-Akademy2009.pdf">Plasma and Netbooks: A proposal for improving Desktop interfaces on netbooks.</a> </li>
   <li>  Christian Stromme. and Helge Welde. <a href="http://akademy.kde.org/conference/papers/StrommeC-Akademy2009.pdf">Benchmarking of software for mobile devices.</a> </li>
   <li>  Knut Yrvin. <a href="http://akademy.kde.org/conference/papers/YrvinK-Akademy2009.pdf">Client alternatives in schools -- going forward.</a> </li>
</ul>
This year was the first for Akademy to accept short technical papers which describe research into new and emerging KDE-related technology. The talks were well received, and we hope the papers will prove interesting and inspiring for the community. We thank all of the authors for their contributions and hope to see more excellent submissions for next year!

The papers can be found <a href="http://akademy.kde.org/conference/papers/">on the Technical Papers page</a>.