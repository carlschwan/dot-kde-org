---
title: "openSUSE KDE Sneak Peek Lifts the Lid on Innovations"
date:    2009-11-03
authors:
  - "jriddell"
slug:    opensuse-kde-sneak-peek-lifts-lid-innovations
comments:
  - subject: "mainstream distributions..."
    date: 2009-11-03
    body: "> openSUSE has been the only mainstream distribution to allow the parallel installation of KDE 3 and KDE 4\r\n\r\nSo they don't count Gentoo as mainstream... \r\n\r\nOtherwise a nice article! \r\n\r\nI hope much of the KDE integration for Firefox goes upstream! "
    author: "ArneBab"
  - subject: "The firefox intergration"
    date: 2009-11-03
    body: "The firefox intergration looks neat. I'll have to check it out when openSUSE 11.2 is released. Firefox has a lot more features Konqueror, and gecko renders more pages correctly than khtml."
    author: "reub2000"
  - subject: "Artwork"
    date: 2009-11-03
    body: "I really liked the KDE-4.3-consistent artwork used for this release. I'm really looking forward for the final release."
    author: "Krul"
  - subject: "Personally I find the shade"
    date: 2009-11-04
    body: "Personally I find the shade of green for the wallpaper repulsing, and my favorite color is green. :x\r\nBut then again it's just a default wallpaper."
    author: "JontheEchidna"
  - subject: "OpenSuse Themes"
    date: 2009-11-04
    body: "OpenSuse is IMO one of the best (if not the best) distros around. It's innovative, flexible, stable, easy to use and fits both the needs of advance users and Linux new comers but themes seem to be a problem for the openSuse guys. I am yet to see a \"wow\" theme in opensuse. The theme is not bad and better than those in the past openSuse releases but it's definitely not the \"capture my eyes\" theme. Maybe the KDE guys can show them how to do it in the next release ;)"
    author: "Bobby"
  - subject: "Ehm? Did you read the"
    date: 2009-11-04
    body: "Ehm? Did you read the article? The KDE guys did show us how to do the themes. The wallpaper, Plasma theme, splash, KDM were done in cooperation with upstream. Besides, I may be old-fashioned, but I thought that themes should not capture the eyes, at least as long as one wants to actually do something other than stare at the screen.\r\n\r\n\r\n"
    author: "Seli"
  - subject: "OpenOffice.org KDE 4 Integration"
    date: 2009-11-06
    body: "I've tested out the RC1 & 2 and there is some very nice OpenOffice.org & KDE4 integration as well.  The look and feel is not of GTK or even that of KDE3 but of KDE4 with dialog boxes, theme, etc. that make it look like it is a part of KDE4.  I think openSUSE 11.2 is going to be a real solid release and a good representation of the KDE4 desktop!"
    author: "asimonelli"
---
Over at <a href="http://news.opensuse.org">openSUSE News</a>, the new <a href="http://news.opensuse.org/2009/10/27/sneak-peeks-at-opensuse-11-2-kde-4-3-experience-with-lubos-lunak/">KDE features in openSUSE 11.2 come under the spotlight</a> before its release in a few days.  KWin stalwart and openSUSE hacker <a href="http://www.kdedevelopers.org/blog/280">Luboš Luňák</a> bares his soul.  See how KDE is presented as the default desktop on openSUSE, and find out how the openSUSE team made Firefox fit in on a KDE desktop.