---
title: "The Open-PC Project Announced at GCDS 09"
date:    2009-07-11
authors:
  - "jospoortvliet"
slug:    open-pc-project-announced-gcds-09
comments:
  - subject: "dead links"
    date: 2009-07-12
    body: "2 dead links: instead of open-pc.com is open-pc-com. \r\nanyway good project, keep an eye on it"
    author: "Regenwald"
  - subject: "Fixed"
    date: 2009-07-12
    body: "Thanks for spotting, fixed now."
    author: "rich"
  - subject: "not quite fixed yet, the link"
    date: 2009-07-12
    body: "not quite fixed yet, the link is now open-pc.om"
    author: "anarky"
  - subject: "Won't be a success"
    date: 2009-07-12
    body: "Getting a PC that is 100% FOSS compatible PC is very easy these days: Just get an Intel-based one and you're done.\r\n"
    author: "KAMiKAZOW"
  - subject: "Great initiative!"
    date: 2009-07-12
    body: "I applaud for this initiative! I feel this could really improve the Free Software image, and make room for more innovation/posibilities to bring the Free Desktop to the masses.\r\n\r\nLooking forward to the release :)"
    author: "vdboor"
  - subject: "DEs"
    date: 2009-07-12
    body: "Oh no!\r\nI wanted to fill out the survey, but I've not a clue which DE I would rather see on the desktop - KDE or Gnome (Personally, I think these two are superior when it comes to usability, so...). See, I really like KDE with it's, \"Hey, watch this!\" factor (interactive wallpapers, Plasma in general and Nepomuk mainly, but there are other things as well), but then getting Firefox, Thunderbird and other, \"familiar\" applications to look pretty in it is a pain and there are still a few tiny niggles in 4.3 that might give Free software a bad name (for example, if it came with KDE but Nepomuk didn't work properly because this/that/the other wasn't installed). Similarly, GNOME is stable as a mountain, responsive and well-integrated with Compiz effects, but if KDE is a diamond to look at then GNOME is cardboard and the settings menu is about as organized as a riot...\r\n\r\nHelp! >.<"
    author: "madman"
  - subject: "If you do this please"
    date: 2009-07-12
    body: "If you do this please encourage FOSS. 100% FOSS like the YEELOONG http://www.lemote.com/english/index.html.\r\n\r\nIntel is no solution. Their new graphics chips only work with closed source drivers and they suck.\r\n\r\n"
    author: "kragil"
  - subject: "fixed now :)\nthanks"
    date: 2009-07-13
    body: "fixed now :)\r\nthanks"
    author: "nightrose"
  - subject: "Browsers"
    date: 2009-07-13
    body: "I find firefox a bit on the heavy side, and I'd prefer a KDE desktop therefore a KDE or at least Qt browser would integrate better. But then again both Arora and Rekonq still have a long way to go. So I did answer Rekonq to stress the desire for a KDE browser to be deployed with it, but I very much that it will be consumer ready for an initial stage, which is supposed to start in a couple of months. "
    author: "jadrian"
  - subject: "openbios"
    date: 2009-07-13
    body: "Would be nice if we could have an open-source bios"
    author: "Joop"
  - subject: "OK, so I finished. Yeah, I"
    date: 2009-07-13
    body: "OK, so I finished. Yeah, I put Arora as the default browser, because I'm probably the only one here that still enjoys using Konqueror. :p"
    author: "madman"
  - subject: "Not true anymore"
    date: 2009-07-13
    body: "Your statement is no longer true. Check out the new GMA 500 from Intel http://en.wikipedia.org/wiki/Intel_GMA\r\n\r\nThis is really bad news. From several blogs, I have taken the information that it is from a branch of Intel that doesn't really know about Linux yet.\r\n\r\nYours,\r\nKay"
    author: "kayonuk"
  - subject: "Took the survey"
    date: 2009-07-13
    body: "... and felt almost ridiculed. It might be only there to distract from the empty state of things otherwise. I mean, why would it matter to ask software questions like these. \r\n\r\nI would suppose it was a given that the machine should work with either desktop, otherwise it doesn't give the user the freedom, simple as that.\r\n\r\nYours,\r\nKay\r\n\r\n"
    author: "kayonuk"
  - subject: "Connection"
    date: 2009-07-13
    body: "I don't see the connection"
    author: "jadrian"
  - subject: "Don't underestimate the power"
    date: 2009-07-13
    body: "Don't underestimate the power of first impressions ;)"
    author: "madman"
  - subject: "Well, I chose KDE as the"
    date: 2009-07-13
    body: "Well, I chose KDE as the desktop, but Konqueror still experiences some crashes left, right and centre, so I chose the faster, more web-standard-compatible and more integrated look-'n'-feel wise Arora instead of Firefox."
    author: "madman"
  - subject: "Not true"
    date: 2009-07-14
    body: "My Intel sound card (82801H, ICH8 family) does not work with out-of-the-box drivers, though I can get most functionality to work by playing around with the options of the snd_hda_intel.ko"
    author: "majewsky"
  - subject: "Re: Took the survey"
    date: 2009-07-14
    body: "Surely the OpenPC will not deny you to choose another DE, but it needs to define some default. End users are puzzled when they're presented Linux for the first time and are asked to choose between KDE and Gnome, and XFCE, and LXDE, and ... you get the message?"
    author: "majewsky"
  - subject: "Re: OK, so I finished. Yeah, I"
    date: 2009-07-14
    body: "\"I'm probably the only one here that still enjoys using Konqueror. :p\"\r\n\r\nI do, too. (Still I get your argument.)"
    author: "majewsky"
  - subject: "Oh, and Fluxbox. Don't forget"
    date: 2009-07-14
    body: "Oh, and Fluxbox. Don't forget about Fluxbox. XD"
    author: "madman"
  - subject: "CoreBoot most definitely. "
    date: 2009-07-16
    body: "I think this is the most important part. I think anyone can get a Linux compatible PC, but last time I tried to get with CoreBoot (formerly LinuxBIOS), I had a lot of trouble (though I admit it's been a while). CoreBoot would move the openPC automatically to the front of the line for consideration of my next PC purchase."
    author: "aidosl"
  - subject: "Suggestion for OpenPC (OPC)"
    date: 2009-07-14
    body: "This suggestion should have been posted on OpenPC, unfortunately the cite is not ready for posting yet.\r\n\r\nI read the postings under this thread, it seems like most of them are preferences and sound bites that don't help much. I am not involved with OpenPC, but I suggest that most people submit their own suggestions detailing what they thought would be a guideline the OpenPC group can benefit from to create their own complete guideline unifying best ideas. Here is mine.\r\n\r\nOK, I always knew that OEMs will not support FOSS as long as MS, in one way or another, has them under its control. This control gets even more vicious as a result of MS having great influence on PC hardware manufacturers.\r\n\r\nTwo years ago, I suggested and argued on multiple cites that, in order to get OEMs to support FOSS based computer, we have to solve the chicken and the egg problem by breaking the cycle of, OEMs wouldn't offer FOSS based computers unless there is demand, and demand for FOSS based computers can't be created unless OEMs start supporting & advertising. \r\n\r\nThere have been many commercial & non-commercial efforts to accomplish that, but, for one reason or another, some failed and others barely made a noticeable progress. Commercial efforts had pretty good start, but MS was able to persuade those who started their business with Linux (eg. Asus, etc.) to switch to supporting Windows instead. Companies whose business model is dependent on Windows, MS leveraged its monopoly power to stop or slow down their efforts to a point where such efforts became in-effective (wishy-washy eg. HP & Dell), and consequently forcing them to concentrate on Windows. Efforts from companies with business model independent from MS, like Zareason, have been making good progress but not flourishing like they should.\r\n\r\nOpenPC (OPC) is a great idea project, but what could we learn from previous experiences to make this endeavor successful? Here are a few guidelines that I thought would a good start.\r\n1)OPC would have a much better chance of succeeding if it wasn't a pure FOSS project, rather, it should be a FOSS project with a touch of business aspect.\r\n2)Seek the support of FOSS supporting entities. Google, IBM, Intel, Canonical, Oracle/Sun are highly appropriate to seek support from. Novell/Ximian, HP, Dell, etc... should be avoided. Asus and Lenavo shouldn't be considered.\r\n3)Selecting a distribution should be the choice of the consumer but with a limit to top 5 distributions. LinuxMint would be top selection.\r\n4)Both KDE & Gnome should both be available by default.\r\n5)Packaged default applications should be the most productive & popular ones. OpenOffice, FireFox, Gimp, Inkscape, K3B,  Kopete & Gaim, Kmail-Thunderbird (not Evolution), Audacity, KDEnLive, Amarok & VLC & Mplayer, Games, Ogg & all codecs (Mono should be totally excluded). VirtualBox is essential for user to install other OS. Etc...\r\n6)End user support should be included at least for the first 6-9 month. Support be either free of included with the system cost. It should consists and furnished by both volunteers & paid staff. \r\n7)Hardware offered should for for standard and power end users. Here is a recommended configuration for standard end users, which would be the majority of consumers.\r\n1.M350 Universal Mini-ITX enclosure [$40]\r\nhttp://www.mini-box.com/M350-universal-mini-itx-enclosure\r\n2.picoPSU-120, 120w output, 12v input DC-DC Power Supply [$40]\r\nhttp://www.mini-box.com/s.nl;jsessionid=0a010b421f43d4ac412f3f614db8926eb33f2fe92813.e3eSc38TaNqNe34Pa38Ta3aLa3n0?it=A&id=417&sc=8&category=13\r\n3.Zotac IONITX-D-E Atom N330 Dual Core WiFi ITX Intel Motherboard [$170]\r\nhttp://www.amazon.com/gp/product/B002BA5IHM?ie=UTF8&tag=ln01-20&linkCode=as2&camp=1789&creative=390957&creativeASIN=B002BA5IHM\r\n4.4GB 800MHz DDR2 Non-ECC CL5 DIMM (Kit of 2) [$48]\r\nhttp://shop.kingston.com/partsinfo.asp?ktcpartno=KVR800D2N5K2/4G\r\n4.1 Seagate Barracuda 7200.10 160GB Hard Drive - 8MB, SATA-300, OEM [$45]\r\nhttp://www.amazon.com/Seagate-Barracuda-7200-10-160GB-Drive/dp/B000Q8E0H0/ref=sr_1_9?ie=UTF8&s=electronics&qid=1247357257&sr=8-9\r\n\r\n\tThat is a decent state of the art computer with good resources for roughly about $300US . The only issue is convincing the consumer that a computer doesn't have to be physically large to be powerful. The enclosure has the capacity for a 2nd 2.5\u201d internal drive. An external DVD\u2013RW USB based can be purchased if needed.\r\n\r\n\r\n"
    author: "Abe"
---
<div style="float: right; border: none; padding: 1ex; margin: 1ex; width: 240px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/open-pc_0.jpg" width="240" height="74" /><br />
</div>
During the Gran Canaria Desktop Summit Frank Karlitschek announced the <a href="http://www.open-pc.com">open-pc</a> initiative. The aim of this ambitious project is to cooperatively design a Free Software based computer by and for the community. Read on for more information about this initiative from the team.
<!--break-->
The project was initiated in response to the lack of quality in the Free Software-based hardware solutions currently on the market. As many reviewers and end-users have stated, the pre-installed software used by hardware vendors generated a bad image for Free Software with potentially interested end-users. Much of the software was buggy and not widely tested and device drivers were often unstable, non-free or not available at all.

Not delivering a complete Free Desktop experience also led to dissatisfaction inside the community. Many vendors developed a custom set of desktop components without community input or support. The result was often not only a re-inventing of many wheels and little advancement for Free Software but also had severe limitations and unsatisfactory performance, consistency and stability.

We are sure that we, as a community, can pool our resources and creativity and avoid these common mistakes. The goal of the open-pc project is to develop a powerful, high quality Free Software based PC in an open fashion. We believe that opening up the development of a personal computer, setting our own goals and boundaries and expanding on the knowledge of thousands will result in a superior product. Everyone is invited to take part in the preparations: currently there is an online poll at <a href="http://www.open-pc.com">open-pc.com</a> for the first product's hardware details (What form factors? Netbook or nettop? Optical drives?) and software additions (e.g. which preinstalled browser?).

A final price of $300 to $400 is our current goal, assembly and international shipment included. As an unbeatable advantage we promise to deliver compatible hard- and software, a preinstalled AppStore client and integrated online services for community support.

A large hardware vendor has already signaled its cooperation and more corporate partners are welcome. We can be contacted through the official web site <a href="http://www.open-pc.com">open-pc.com</a>. First computer deliveries are planed for autumn 2009.

The online poll can be found <a href="http://open-pc.com/survey">here</a> and the open-pc presentation <a href="http://open-pc.com/open-pc-gcds.pdf">here</a>.