---
title: "Second KOffice Developer Sprint 2009 Kickoff"
date:    2009-11-29
authors:
  - "jospoortvliet"
slug:    second-koffice-developer-sprint-2009-kickoff
comments:
  - subject: "Thanks to Jarek for the"
    date: 2009-11-29
    body: "Thanks to Jarek for the images that look good, other images are my own ;-)"
    author: "jospoortvliet"
  - subject: "nice"
    date: 2009-12-20
    body: "Thanks to Jarek for the images that look good, other images are my own ;-)<a href=\"http://www.yarisma.biz/\"><span style=\"color: #ffffff;\">yarisma</span></a> <a href=\"http://www.zomoyun.com/\"><span style=\"color: #ffffff;\">oyun</span></a> <a href=\"http://www.parite.net/\"><span style=\"color: #ffffff;\">forex</span></a> <a href=\"http://www.muzikler.org/sitemap.php\"><span style=\"color: #ffffff;\">mp3 dinle</span></a>"
    author: "spiderbote"
---
In Oslo, Norway, the second KOffice developer sprint this year has started. The KOffice developers must be getting used to seeing each other regularly - besides the two sprints there were many other meetings and events with a handful of KOffice developers present. However, their 'own' sprints still are special - dedicated to some team building, designing and hard work in a cooperative and positive atmosphere.
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/inge discussing agenda.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/inge discussing agenda - small.jpg" width="300" height="168"/></a><br />Inge assembles the agenda</div>

<h2>Topics</h2> 

The <a href="http://wiki.koffice.org/index.php?title=Meetings/End_2009_meeting">Agenda on the wiki</a> contains some info on the goals of this meeting. The topics could be split up in three distinct areas: for the developers, the users and the wider world.

<h3>The wider world</h3>
KOffice is quickly becoming a player in the world of commercial software development, and this means that besides the interested individuals, larger teams and organisations are getting involved in development. The inherent and incidental differences between what the companies and the community want have to be balanced, discussed, and agreed upon. This is relatively new for the KOffice community. For a long time they could decide among themselves what to do, keeping only the users in mind. Now, commercial interests have a role to play. It is important to keep the community close to the commercial work and vice versa, solve potential conflicts, communicate openly, and involve both parties in decision making. This is a challenge. Other teams within the KDE community have been going through this process, shown it is doable - but it can be a rocky road. The KOffice team wants to discuss this, gather opinions, and be prepared.

<h3>Development</h3> 

The developers, meanwhile, have something else to discuss as well: the KOffice libraries. Now external parties are picking them up, essentially realizing the ambitious 2.0 plans, it is time to have a close look at them. Since the 2.0 release the libraries have been largely ignored - lots of work was spent on finishing the user interface and bringing back the many features requested by users. However currently there are no guarantees in the area of source and even binary compatibility. This means using the KOffice libraries is much harder for external parties. If the KOffice team really wants to fulfill on their promise to create a generic ODF library for Qt/KDE, to be a framework for all kinds of document editing applications and become an important infrastructural part of for example a prominent phone stack, their libraries and especially their API needs to be top-notch. There will be presentations on library design, metadata and other technical topics, including a move to the Git sourcecode management tool.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/food.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/food_small.jpg" width="368" height="224"/></a><br />Thursday night food</div>

<h3>Next release</h3>

The last but not least important topic for the meeting this weekend is the upcoming 2.2 release. The team needs to discuss the release schedule, and plan on getting the release ready for end users. A clear list of tasks which have to be accomplished and bugs which have to be solved is crucial to guarantee the quality of the upcoming release. Furthermore, the future KOffice releases will be discussed. What will the release schedule for the coming versions be, what roadmap is realistic yet challenging, where does the team want to be in a few years from now?

<h2>The first day</h2>

The location for the meeting is the former Trolltech office in Oslo - now fully Nokia branded. Starting time was 9:00, with an opening from Inge which he used to discuss and decide upon the agenda. After that the first topic, Git migration, started. The final decision was to move to Git as soon as possible, but there are some issues to work out. Basically converting KOffice to Git has been done and it is ready to Gitorious. However, some subversion branches still need to be converted, a couple of scripts have to be written and of course the whole team needs Gitorious accounts. The next topic was the release schedule for 2.2 and onwards. Possible schedules like a 4 month schedule, always-summer-in-trunk and other schemes, in part made possible thanks to the planned Git migration, were discussed. 

<h3>Nokia working with KOffice team</h3>

After the technical discussions, Suresh Chande, Product Manager at Nokia gave a presentation on what Nokia is doing with KOffice. Nokia has developed a testset of 750 special test documents which will be released soon for testing KOffice and other OpenDocument compatible office suites. Furthermore, Nokia testers have been testing KOffice and created a list of 600 high quality bugreports they want to share with the community and fix together. Besides Suresh, visiting Maemo developers Lassi and Mani introduced themselves to the team and joined the discussion, collecting feedback from the KOffice team on various things like coordinating development and planning on KOffice features for the next version. One decision made was to put the Fremantle user interface (in the Maemo version currently on the N900) in the KOffice source repository. This GUI will be released by Nokia soon.

Another bit of news was that Suresh will provide 5 N900s to KOffice developers, and the team has to decide who gets one.

<h3>More talks</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/tabletennis.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/tabletennis_small.jpeg" width="400" height="266"/></a><br />Table tennis during lunch</div>
After a lengthy discussion about decision making and arbitration, it was time for Olivier Goffhart. Olivier, Kopete developer, KDE libs developer and Nokia Qt Development Frameworks employee gave a talk about developing libraries, diving deeper into technology. Why would you put something in a library? Olivier considers consistency as one of the most important reasons for that. He further went through various other issues like designing easy to use API's, maintaining compatibility et cetera. The talk ended in a lengthy discussion about libraries and code. This topic was clearly not done yet, so once lunch was done the developers continued on this track. After a few hours and some cookies brought by Thomas Zander the team had settled for relatively quiet hacking until it was time for food.

<h3>Ending the day</h3>

While the day is not over as this is being written, plan was to have dinner together - Pizza always results in a good time. Then some will surely want beer, while others might prefer sleep. And the Krita team has planned a meetup at the hotel to finish some discussions. Tomorrow is another day!

<h2>Conclusion</h2>

As you see, the KOffice development team faces some serious long and short term decisions. A face to face meeting is often the best place to decide upon such crucial issues and the team seems to be excited to get going. This morning has been spend on laying out the plans for the weekend, and your author must say they seem challenging. It is clear that KOffice however is in capable hands. The attending team combines a strong vision and great development and design skills with a cooperative spirit and an incredible work ethic.