---
title: "Krita 2.0: a Host of New Features"
date:    2009-02-09
authors:
  - "unknow"
slug:    krita-20-host-new-features
comments:
  - subject: "dpi"
    date: 2009-02-09
    body: "Shouldn't the scan and screen dpi be the same for the displayed image to be the same physical size as the original ? If not, what's the math to get screen dpi vs scan dpi ?"
    author: "moltonel"
  - subject: "Suppose we want to scale 300"
    date: 2009-02-09
    body: "Suppose we want to scale 300 dpi to 100 dpi while ensuring that one inch on the paper is still one inch on screen. So we know:\r\n<ul>\r\n <li>One paper inch = 300 picture pixels</li>\r\n <li>One monitor inch = 100 pixels</li>\r\n</ul>\r\nTherefore 1 pixel = 3 picture pixels, which means that the image has to be scaled by 1/3 when painting it on screen. You can see that the scaling ratio is simply the screen resolution divided by the scan resolution (which is available through Exif tags).\r\n"
    author: "majewsky"
  - subject: "dpi"
    date: 2009-02-09
    body: "Well, I always get confused when touching that code myself... But it isn't that hard, actually: a pixel on screen has a particular size, say there 1/100th of an inch. Once that's known, and once you know how many pixels in the image fit in an inch, say 300, you can do the conversion: 100 pixels on screen make an inch, 300 pixels in the image man an inch, so you have have at 100% 3 image pixels in a screen pixel."
    author: "Boudewijn Rempt"
  - subject: "Digg it!"
    date: 2009-02-09
    body: "Please digg it!\r\nhttp://digg.com/software/Photoshop_Killer"
    author: "Dread Knight"
  - subject: "I wonder if you realize that"
    date: 2009-02-10
    body: "I wonder if you realize that we do have buttons for digg, reedit and the like now? Why are you still opening a second topic, hurting the default one? Just because you think \"Photoshop Killer\" is a better title?"
    author: "danimo"
  - subject: "not the dot story"
    date: 2009-02-10
    body: "actually it is not this dot story. it was submitted a few days ago when boud blogged about the new features"
    author: "mkraus"
  - subject: "Krita from mac.kde.org? NO NO NO NO NO."
    date: 2009-02-11
    body: "Krita (or koffice for that matter) are not currently in any disk images available from mac.kde.org.\r\nKrita is available from source (compile it, and dependencies yourself) or macports (have a build system compile it for you) via koffice2-devel. I really wish someone would check with us first before giving out erroneous info. "
    author: "illogic-al"
  - subject: "sorry ;/"
    date: 2009-02-11
    body: "I didn't checked this when writing this article :( my fault. I've checked Windows port where it is available, and I assumed the same is for mac ;/ sorry... "
    author: "Piotr Pe\u0142zowski"
  - subject: "updated"
    date: 2009-02-11
    body: "story updated"
    author: "jriddell"
  - subject: "user"
    date: 2009-02-20
    body: "I've been a fanatical GIMP user, but desiring to stay with Qt, and an app with a \"k\" in its name, I thought I'd play around with Krita and so far -- it's been great.  The layout should make all the /photoshop/tards happy and the features are keeping me quite happy.  I can't wait for it to evolve even further.  Great work, Krita team."
    author: "klaatu"
  - subject: "dpi is for print, not for screen"
    date: 2009-02-22
    body: "This is where a lot of people go wrong.\r\n\r\nDPI is for print. Measure your screen. Do the math for the resolution you have right now. Then change the resolution of the screen. You just changed the resolution of the paper and the picture changed size without changing the dpi setting.\r\n\r\nHere is the easy solution: Forget about all that has to do with dpi and screen. It does not apply. Sure, many screens, especially LCD has an optimal resolution. But the resolution can still be changed.\r\n\r\nHere is a good scenario.\r\n\r\nYou have a picture that is 600x400 pixels. One version is 3000000 dpi. The other is 5 dpi. They will both be the same size on the web. And that is exactly how it should be. If you print it, however, well - that is a different story.\r\n\r\nBTW - the filesizes will also be the same. Or at least extremely close :-)\r\n\r\nWhy does it not apply? The application do not know how big the monitor really is. There are too many unknown factors.\r\n\r\nAdding anything like this to the application makes it really look like an amateur application. Photoshop have it correct. DPI is firmly placed in the section for print."
    author: "Oceanwatcher"
---
<p>Boudewijn Rempt <a href="http://www.valdyas.org/fading/index.cgi/hacking/krita-progress-29-01-2009.html?seemore=y">has summarised</a> results of development for the next version of Krita, the painting and image editing application for KOffice. Krita 2.0 will contain a host of new features, some of which are unique in the free software world. Below Piotr introduces some of the new features which will be available in this release.</p>
<!--break-->
<a href="http://static.kdenews.org/jr/krita-2.0.png"><img src="http://static.kdenews.org/jr/krita-2.0-wee.png" width="300" height="214" style="border: thin solid grey; padding: 1ex; margin: 1ex; float: right;" /></a>

<p><strong>Ported</strong>: Krita now runs on Mac OS X and on Windows. Windows and Mac OS X ports are now available. They are easy to get using the <a href="http://windows.kde.org/">Windows installer</a> and <a href="http://mac.kde.org/">Mac OS X macports (via koffice2-devel)</a>.</p>

<p><strong>Vector layers</strong>: Krita 1.6 made it possible to embed a KOffice KPart as a layer into an image. This makes it possible to have a complete KSpread or KWord document as a layer. In KOffice 2.0, KParts have been replaced by <a href="http://dot.kde.org/2006/06/05/koffice-20-vision">Flake Shapes</a>, as an end result Krita 2.0 has vector layers where you can add any combination of KOffice shapes. Examples of KOffice shapes are vector drawings, text objects or even <a href="http://dot.kde.org/2007/08/27/pencils-down-koffice-summer-code-students">musical notation objects</a>. These shapes are then rendered at the image resolution and blended with your pixel layers.   The same technique is used for vector selections.</p>

<p><strong>More brush engines</strong>: Krita is the only paint application that has pluggable brush engines.  Thanks to Lukas Tvrdy Krita has got some new engines like: </p>
<ul>
<li>Sumi-e brush: a generic hairy brush simulation </li>
<li>Chalk: a very acceptable simulation of dry media</li>
<li>Dynadraw: Inspired by <a href="http://www.graficaobscura.com/dyna/index.html">Paul Haberli's Dynadraw</a></li>
</ul>

<p>Expect more interesting brush engines to appear over the next year.</p>

<p><strong>Image resolution</strong>: In 2.0, Krita keeps track of resolution and can always display the image at the correct size on screen. That means that if you have scanned an image at 300dpi and have a 96dpi screen, your image will at 100% zoom be as big physically as on the paper you scanned it from.</p>

<p><strong>Real colour mixing</strong>: A mixer canvas as in Corel Painter, but one where mixing the colours would give real-life results. This feature was added by <a href="http://dot.kde.org/2007/08/27/pencils-down-koffice-summer-code-students">Emanuele Tamponi during Summer of Code 2007</a>. This feature may by postponed to 2.1, please test this feature if you plan to use it.</p>

<p><strong>Clone layers</strong>: A clone layer copies part of another layer and inserts it somewhere else in the layer stack. You can then add new effect masks to the copy, for instance to create a blur effect. This feature seems to work but extensive user testing would be appreciated.</p>

<p><strong>OpenGL canvas with on-canvas preview of gradients</strong>: If your computer supports OpenGL, you can use the OpenGL canvas. This is faster and uses less memory than the ordinary canvas. For introduction to other new features and answers why development of Krita 2.0 is taking so long please <a href="http://www.valdyas.org/fading/index.cgi/hacking/krita-progress-29-01-2009.html?seemore=y">see this article.</a>  </p>

<p>Krita now needs a lot of testing to shape up 2.0 release and make it rock solid, so please give it a try and test it extensively. Also, the Krita project is in need of people who would like to help with updating <a href="http://docs.kde.org/development/en/koffice/krita/">The Krita Handbook.</a> As you see a lot of new features are now available and the manual needs some love, this is a great opportunity for non-coders to contribute and work together to improve Krita and KOffice.</p>

<p>Three years of work on Krita 2.0 has resulted in much better code, extensive set of unit tests and better separation of concerns.  Furthermore a better design for many data structures and better user interface. Krita developers hope that 2.0 release would be a great base for future development, the code is better and it is easier to get involved in coding for those who would like to improve this cross-platform painting and image editing application for KOffice.</p>