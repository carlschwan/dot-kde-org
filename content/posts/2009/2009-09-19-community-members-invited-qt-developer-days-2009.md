---
title: "Community Members Invited To Qt Developer Days 2009"
date:    2009-09-19
authors:
  - "leinir"
slug:    community-members-invited-qt-developer-days-2009
---
<p>The last few years has seen the company formerly known as Trolltech open their arms to one of the largest parts of their supporting community, KDE, in a new way: By offering a few members of the KDE community free admittance to the Qt Developer Days conference. This year is no different, and they have invited a number of people to attend this year's conferences. Yes, that's plural: There are two conferences. One from the 12th to 14th of October in Munich, Germany and one from the 2nd to the 4th of November in San Francisco, USA.</p>

<center>
<object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/_b3QEdJ3EzQ&hl=en&fs=1&"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/_b3QEdJ3EzQ&hl=en&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object><br />
Highlights from Qt Developer Days 2008, by QtStudios
</center>

<h2>The Next Level</h2>
<p>Over the summer holidays, Knut Yrvin, Community Manager for Qt approached a couple of members of the community and suggested organising further events in connection with Nokia's own events. As such, two teams started working on concepts for sprints - one for each side of the big pond.</p>

<p>The first of the sprints, in Munich, has been given a fairly untraditional topic - not only is it about games, which is maybe not so untraditional, focusing on the game creation framework named Gluon, which is currently in development. It is also about the creation of a visual tool designed to create games, inspired in part by Unity3D and tentatively named Gluon Creator. What that means is that this sprint will be aimed at creating an entire new application from scratch, to fill a void in the open source world. The discussions about this has already begun on the KDE Games mailing list, and on <a href="http://amarok.kde.org/blog/archives/1098-The-Future-of-Game-Development-in-KDE.html">the initial blog entry announcing the creation of the project</a>.</p>

<p>The second sprint's topic has yet to be decided, but the organisers, A. &quot;blauzahl&quot; Spehr and <a href="mailto:greeneg@REMOVETHISkde.org">G. &quot;greeneg&quot; Greene</a>, are open for participants contacting them to work out topics for the sprint - in fact, they encourage you to do so!</p>

<h2>You're Up!</h2>
<p>The sprint in Munich has attracted 12 people, but that does not cover all the passes Nokia has made available. The rest of the passes will be distributed to members of the KDE community by the KDE e.V. - what that means is that some of you, as active, coding members of the KDE community, may be in for a treat in the very near future.</p>

<p>The sprint organisers in San Francisco need you to contact them as soon as absolutely possible if you wish to attend. This is to make sure that the accommodation can be worked out. When you contact them, please include an estimate for your travel costs, so a budget can be created.</p>