---
title: "Camp KDE Talks Part Two"
date:    2009-02-04
authors:
  - "jospoortvliet"
slug:    camp-kde-talks-part-two
comments:
  - subject: "More is coming!"
    date: 2009-02-04
    body: "Hi all,\r\n\r\nsorry it took so long to continue the series of articles. The new dot interfered a bit with my schedule... A few more articles are coming, I'll try to get them online in a reasonable time period."
    author: "jospoortvliet"
  - subject: "Videos"
    date: 2009-02-04
    body: "Any news on videos of the talks?\r\nI always watch a bunch of them, trying to pick up tips from the best :)"
    author: "ianjo"
  - subject: "Videos"
    date: 2009-02-05
    body: "I sent the final videos to Jeff for remastering 2 or 3 days ago.  He had completed the others speeches and was waiting for the final 2 from me."
    author: "wolson"
  - subject: "5th picture is broken"
    date: 2009-02-05
    body: "hi,\r\ni just wanted to watch the pictures in big size. the 5th picture seems to be broken (at least for me, 404 :) )\r\n\r\ngreets"
    author: "Regenwald"
  - subject: "I see. I'll try to fix it"
    date: 2009-02-06
    body: "I see. I'll try to fix it when I get home (next week, can't fix it at FOSDEM...)"
    author: "jospoortvliet"
  - subject: "fixed."
    date: 2009-02-11
    body: "fixed."
    author: "jospoortvliet"
---

Sunday again had talks about a large range of topics. The day started a bit late, but Guillermo Amaral really made up for it by providing us with a funny and interesting talk about the opportunities for the Business use of KDE in Mexico. He pointed out how important it is to handle cultural differences well, continuing the theme set by Adriaan and Till.

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jos/campkde/Guillermo.jpg"><img src="http://static.kdenews.org/jos/campkde/Guillermo_thumb.jpeg" width="400" height="266" alt="photo" /></a><br />
Guillermo talks to us
</div>

Guillermo was followed by Leo Franchi who spoke about <a href="http://amarok.kde.org">Amarok</a> and the challenges the Amarok developers are facing. The choices they have made in the interface have been heavily influenced by the (limitations of) technology. Much thinking is going into the interface, and these thoughts are interesting to hear.

Orville Bennett spoke about the the work being put in <a href="http://mac.kde.org">KDE-on-Mac</a>. It's a fairly small team of 4 or 5 people, but just like the KDE-on-Windows team they manage to provide pretty decent KDE software packages to Apple OS X users. When Orville's talk was over, Bill Hoffman from Kitware mentioned how CPACK could help, and Orville joined him at his table. Let's see what happens.

As lunch was a bit late, some distribution developers - Gökmen Göksel from <a href="http://www.pardus.org.tr/eng/index.html">Pardus</a> and Marcus D. Hanwell from the <a href="http://www.gentoo.org">Gentoo</a> community - continued with a talk about the cooperation between their distributions and the KDE community. Marcus explained the challenges Gentoo faces when packaging KDE software. Marcus discussed the pros and cons of splitting modules. Marcus then moved to inter-distribution cooperation - is it possible to share patchsets and bugfixes more effectively? There are techbase pages but not many distributions currently use them. They both agreed that support of legacy users still wanting KDE 3.5 updates is an issue with no easy answers.

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jos/campkde/marcusgukmen.jpg"><img src="http://static.kdenews.org/jos/campkde/marcusgukmen_thumb.jpeg" width="400" height="266" alt="photo" /></a><br />
Marcus and Gökmen
</div>

Gökmen took Marcus' place. Pardus, a government funded Turkish distribution, has recently been working hard to get closer to the KDE community. Gökmen explains the history behind Pardus asking: "what does a distribution need?" Pardus has pretty much written a distribution from scratch. Much of the administration tools are built with Python, and the GUI applications make use of the python bindings for KDE and Qt. The reasons for that are simple - the available tools didn't fit with the ideas they had about managing a distribution. Currently all tools are based on PyQt3 and PyKDE3. There is a major porting effort going on right now, but there is still a lot to do. Gökmen demonstrated several of the tools developed by Pardus. A few of them, like networking and power management, have seen strong integration in KDE. The Pardus developers are discussing how to take advantage of that - it is one of the reasons they sent two people to this conference.

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jos/campkde/work late.jpg"><img src="http://static.kdenews.org/jos/campkde/work late_thumb.jpeg" width="400" height="266" alt="photo" /></a><br />
Working in Jamaica at night
</div>

Marcus continued talking about interaction with upstream, outlining the ways Gentoo currently communicates with KDE, and asking for input and ideas for improvement on how distributions can work with KDE. Gökmen mentioned how hard it can be to decide what patches might be accepted upstream. He knows about the kde-devel but you still have to figure out what to send for review. This was discussed at length, and will be the topic of more discussions over the coming weeks.

After another good Jamaican lunch, Zack Rusin gave us a lecture about computer graphics. He explained the difference between CPUs and GPUs. GPUs are essentially large groups of small and simple CPUs that execute a relatively small number of different instructions on a large amount of data. By design this is much more efficient than what general purpose CPUs do - speeding up a single data-and-instruction-stream is much more difficult. According to Moore's law, the number of transistors you can put on a chip doubles approximately every 18 months. Not all of these transistors translate to more speed. In the case of CPUs, only 1/3 of these are used for executing commands. The other 2/3rd are needed to feed the CPU with instructions in a timely fashion. For example, a good chunk of it is used for caches and branch predictors to overcome the speed difference between RAM memory and the CPU core. GPUs suffer from that issue to a much lesser extent. Most of the new transistors are used for new execution units, and thus a similar sized GPU is many times more powerful than a CPU for floating point operations.

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jos/campkde/zack during lecture.jpg"><img src="http://static.kdenews.org/jos/campkde/zack during lecture_thumb.jpeg" width="400" height="266" alt="photo" /></a><br />
Zack with audience
</div>

Zack went through the various efforts currently going on to use the enormous power of GPUs for more generic computing. The most promising approach in his opinion is OpenCL, although it does not currently have a working implementation. When OpenCL usage grows, GPUs will have an increasing impact on performance. As OpenCL matures and Free Software implementations arrive, we need to prepare for greater concurrency in our applications and library stack. Zack further spoke about the Gallium3D effort he is involved with, making driver development easy. This will contribute to faster and better graphics on Linux - something we will all appreciate.

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 200px">
<a href="http://static.kdenews.org/jos/campkde/zack not during lecture.jpg"><img src="http://static.kdenews.org/jos/campkde/zack not during lecture_thumb.jpeg" width="200" height="299" alt="photo" /></a><br />
Zack without audience
</div>

Concluding, Zack urges us to start focussing on parallelism - we need it to keep our advantage over the competition. Yes, it is difficult, and there is a large challenge for Qt Software to lower the barrier and make it easier for developers to take advantage of parallelism. QtConcurrent is already a big step forward, and more should come soon.

One interesting question was concerning the full OpenGL based rendering of Qt/KDE applications. According to Zack and Thomas Zander the upcoming Qt 4.5, to be released in March, can enable OpenGL rendering on a per application basis. This feature is still experimental, but future versions will of course improve on it.

Next was a talk by Mauricio Piacentini and Marcus D. Hanwell. Mauricio started by describing the improvements in the games and educational applications brought by the rewrites for KDE 4.0. The community is growing and very healthy. The work on KBrunch is an interesting example of cooperation between KDE and a major Brazillian university. Several people from the university came to Akademy 2008 and together with some KDE contributors came up with a good plan to bring KBrunch forward. Students from the University of Touluse are working on Kapman. All these things would have been much harder without the infrastructure brought by the KDE 4 series. Marcus introduced his work on Avogadro, a powerful visualisation library and tool. It started in Kalzium, where recently a 3D engine was added for viewing molecules. Kalzium contributors met Avogadro developers online and began collaborating. Marcus joined the project as a summer of code project, and started to work on Kalzium, Avogadro and OpenBabel. He ported Kalzium to use the Avogadro library using the plugin infrastructure as the base for Kalzium's viewer.

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jos/campkde/ade leo Orville.jpg"><img src="http://static.kdenews.org/jos/campkde/ade leo Orville_thumb.jpeg" width="400" height="266" alt="photo" /></a><br />
Adriaan, Leo and Orville
</div>

Marcus explained the purpose of Avogadro, and went into some detail concerning its architecture and the relationship with Kalzium. There is still close collaboration between Kalzium and Avogadro. Kalzium in KDE 4.1 added editing functionality, but 4.2 didn't introduce many new features. There is a lot waiting to go into what will be the 4.3 release. Marcus told us the that the development team is looking for OpenGL advice, somebody with experience with Kross and user interface experts. They need help to improve both Kalzium and Avogadro and make it easier for children and students to explore the world of molecules. Luckily there is feedback coming from the classrooms, and on several occasions improvements were prompted by requests from teachers and lecturers. There is even a commercial company starting to take advantage of Avagadro technology. Interest in Avogadro grows. Marcus has already given several talks at big conferences in the US and the UK. The research group he works for at the University of Pittsburgh uses Avogadro in its research and is able to push Avogadro forward along with other developers from around the world.

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jos/campkde/pardus developers and Davide.jpeg"><img src="http://static.kdenews.org/jos/campkde/pardus developers and Davide_thumb.jpeg" width="400" height="266" alt="photo" /></a><br />
Both Pardus developers with rum and Davide in the middle (who's code was NOT compiling)
</div>

Mauricio took the microphone again, going further into the goals of the KDE edu team. They want a closer relationship with teachers and other users of their software. The team is on the right track but there are still many ways in which communication could be improved. Another important step which has been made is teaching a new generation of contributors. Not just younger developers currently joining the team, but interestingly enough both speakers' wives are expecting new, potential KDE contributors to arrive soon...

During the questions an interesting notion came up. As there are now thousands of small games to be found on the web, the KDE games team has been thinking about the future. They have been abstracting functionality from the individual games into libraries, and are moving towards as many generic components as possible. It would be interesting to pursue the goal of allowing users to quickly and easily create their own games using KDE Games tools.


The closing talk was by Jeff Mitchell who spoke about the use of Free Software in Enterprises. Basically, he explained why open source coders need companies, and why companies need open source coders. Often, being a Free Software developer gives you an advantage in finding a job - and you need a job to eat, go to conferences in sunny places and buy legal digital music to listen to at these conferences. Companies are starting to see the advantages of FOSS developers. They often have a lot of experience - not by having written code for 20 years, but by having had the opportunity to be responsible for others code; have a say in application design and generally learn from the best. A deeper understanding of code is a result of the way of working in Free Software. Your code is reviewed by many people, you are allowed to experiment, you have to read, understand and repair other peoples code and make sure others can fix yours. Open source coders are used to working in various environments, they have to be adaptable and open-minded. They try different linux distributions or other OS's, and get bug reports from various environments. They have to communicate with others who will take their arguments based on merits - after all, hierarchy plays only a minor role in Free Software. In online communication, and the regular face to face meetings a FOSS developer has to be realistic, open minded, persistent, robust and honest. Others will criticize their code, sometimes harshly, and they must cope with that. 

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jos/campkde/jeff in sea.jpg"><img src="http://static.kdenews.org/jos/campkde/jeff in sea_thumb.jpeg" width="400" height="266" alt="photo" /></a><br />
Jeff (not during his closing speech)
</div>

All these social and technical skills are very attractive to enterprises. Jeff provided various examples of issues with proprietary project which would be very unlikely if Free Software developers were involved. Even an inexperienced FOSS developer has real, tangible insights to offer to projects going around in many companies. Interestingly enough, one of our sponsors ended up talking to one of the attendants, discussing a possible job offer. Clearly meetings like this offer more than just time for coding or discussing code.

Jeff offered another insight at the end of two days full of interesting insights - we learn a lot of things here that can make a difference. All that while making a difference to the world, as Wade proved in his opening talk. The subsequent days were spent talking about what we have learned and applying it to real-world code.