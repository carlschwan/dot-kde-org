---
title: "Ongoing Oxygen Icons Usability Survey: KDevelop"
date:    2009-08-03
authors:
  - "troy"
slug:    ongoing-oxygen-icons-usability-survey-kdevelop
comments:
  - subject: "Hard survey"
    date: 2009-08-04
    body: "Okay, just did this survey myself, and even being a coder, this was a really hard survey compared to the last one - I had to think a lot more, and I'm still not happy with all of the decisions.  Wow.\r\n\r\nOkay, good luck with this one folks."
    author: "troy"
  - subject: "Hard survey"
    date: 2009-08-04
    body: "Yeah this one is not trivial at all and honestly I'm not expecting brilliant results as the ones we had last time, but that doesn't make it any less interesting, in fact its the other way around IMO.\r\nWe will have to probably make a few more test around this batch of icons."
    author: "pinheiro"
  - subject: "bug in survey text"
    date: 2009-08-04
    body: "In the last page of the german survey there are visible (broken?) p tags and there is the untranslated text \"Have fun!\"."
    author: "panzi"
  - subject: "Sorry"
    date: 2009-08-04
    body: "OK this is a WIP next time we will do that better thanks for spoting it."
    author: "pinheiro"
  - subject: "Hard to choose"
    date: 2009-08-04
    body: "I also had problems choosing the icons, none of them really jumped out as saying what they were. It might be better to go with abstract coloured/shaped shiny blobs, than an icon that seems to strongly represent another action/object/idea.\r\n\r\nFrom KOffice:\r\nnew/add anything has a mini-icon of a white cross in a green circle on an icon of the thing\r\nopen file has an open blue folder with a icon of a thing poking out (I'm not happy with this one)\r\n\r\nSuggestions:\r\n\r\nObjects:\r\nFunction/Method - an italic \"f(x)\" or button or 'action'\r\nVariable/Attribute - an italic x or text-box or slider\r\nDefine/Constant - a capital X or label or italic x with padlock\r\nProject - (tricky there are lots of different ones) a folder or a folder of a window/panel, a program script, a folder, etc.\r\nEmpty Project - like empty glass folder, light grey program script/text file, window/panel...\r\nTemplate Project - half full glass, half blue half grey \r\nFavourite - yellow star\r\n\r\nAction mini-icons:\r\nAdd/New - white plus sign in green circle or green +\r\nDelete - white minus sign in red circle or red X\r\nUpdate/Open - while hand-finger on amber circle or amber O\r\nClose - kiss goodbye X or save's floppy disk\r\nFind/Jump-to - binoculars\r\nZoom/Details - magnifying glass\r\nImport - white arrow into object container\r\nExport - white arrow out of object container\r\nWizard - magic wand or lightning\r\n\r\nQuick-open - open file-folder\r\nProject (Development) - folder of text doc, window/panel (with edit pencil)\r\nProject (Development) Template - half full folder\r\nFunction - \"f(x)\"\r\nJump to definition - binoculars or |< (previous track) mold/die/stamp/parent"
    author: "Purple-Bobby"
  - subject: "typo"
    date: 2009-08-04
    body: "There's a little typo on the first page: The page says \"Runing\" where it should say \"Running\" :) \r\n\r\nNow that I got my first feedback out: \r\n\r\nI really like these surveys - they are a very easy way for people to become active, at least a little bit. So they are a great way to gain entry - and I can also do them when I'm very short of time. \r\n\r\nMany thanks! "
    author: "ArneBab"
  - subject: "I felt lost !"
    date: 2009-08-04
    body: "Sorry, but for most of the questions I was absolutely unable to choose an Icon: no one would fit !"
    author: "lbayle"
  - subject: "."
    date: 2009-08-05
    body: "Also, \"excelent\" should have two \"l\".\r\n\r\nI know that KDE 3 is no longer really supported, but I found it a little sad that I had to fill in this survey with firefox as it wouldn't work in konqueror (kde 3.5.10 - clicking on the icons didn't have any effect). It's not as if a survey like that requires highly advanced javascript tricks...\r\n\r\nAlso, instead of just putting the icon name I think it may have been better to write a short sentence saying what they should do. For example I had no idea what the \"project development\" icon is supposed to do."
    author: "tendays"
  - subject: "\"project development\""
    date: 2009-08-14
    body: "<i>Also, instead of just putting the icon name I think it may have been better to write a short sentence saying what they should do. For example I had no idea what the \"project development\" icon is supposed to do.<i>\r\n\r\nsame problem here.."
    author: "mxttie"
---
<div style="float: right; border: none; padding: 1ex; margin: 1ex; width: 48px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/code-context.png" width="48" height="48" /><br />
</div>
Every few weeks <a href="http://pinheiro-kde.blogspot.com/">Nuno Pinheiro</a> and the KDE <a href="http://www.oxygen-icons.org/?page_id=16">Oxygen Icons team</a> are publishing a new usability survey online to get feedback from users on the look and feel of icons.  In particular, the Oxygen team is looking for feedback from individuals that have had no exposure to KDE, so if you are at home or at work, poke your friends and family and have them complete the survey, or simply take the survey yourself.  The <a href="http://oxygen-icons.org/survey/">current survey</a> is on icons for <a href="http://www.kdevelop.org/">KDevelop 4</a> which is a major rewrite of KDevelop for KDE 4.  So if you have a moment, grab someone and complete the <a href="http://oxygen-icons.org/survey/">KDevelop Icons Survey</a> now.
<!--break-->
