---
title: "KDE Voted Free Software Project of the Year"
date:    2009-01-20
authors:
  - "sjarvis"
slug:    kde-voted-free-software-project-year
comments:
  - subject: "Congratulations!"
    date: 2009-01-20
    body: "Congratulations! Personally I think KDE 4.2 is the best KDE I ever used."
    author: "Michael"
  - subject: "Re: Congratulations!"
    date: 2009-01-20
    body: "+1 think so 2"
    author: "nuno pinheiro"
  - subject: "Re: Congratulations!"
    date: 2009-01-20
    body: "Me too. Even being still RC quality it clearly surpasses 4.1, let alone 4.0. I don't miss 3.5 anymore."
    author: "NabLa"
  - subject: "Re: Congratulations!"
    date: 2009-01-20
    body: "Yep, I switched one machine at beta 1 and liked the improvements so much that by beta 2 they were all switched over :-)"
    author: "Stuart Jarvis"
  - subject: "Re: Congratulations!"
    date: 2009-01-20
    body: "Been running it from trunk for a few months and it's never been unstalbe in my opinion. Can't wait for final ;)\nKudos!"
    author: "JavierBere"
  - subject: "Re: Congratulations!"
    date: 2009-01-20
    body: "I have it on my play-laptop, and there are e.g. only 3 bugs in e.g. Akregator that make it feel very unfinished:\n\na) CTRL-W does nothing when hit the first time. The second time it tells me to check the double keybinding of CRTL-W. After checking everything, I know it's not like that. Bug exists.\nb) CTRL++ does not work. Bug exists.\nc) Settings for sort order and displayed items is not saved. By default it sorts by title, not by date?! Bug exists.\n\nAnd that is only _one_ application with only the 3 features of it that are somewhat essential to using the software.\n\nMe, I am waiting for 4.3. I know, Richard rightfully said once something similar to \"It's going to be ready sooner if you help\". But probably I am allowed to point out, that 4.2 is still only a waypoint.\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Re: Congratulations!"
    date: 2009-01-21
    body: "Way to post in the completely wrong place about that sort of stuff mate :D"
    author: "zicada"
  - subject: "Re: Congratulations!"
    date: 2009-01-23
    body: "I think the comment was on the same level of discussion as \"yay kde4.2 is the best kde, i haven't encountered any bugs yet\".\n\nWe really appreciate the hard work done so far and would die for our beloved devs and give their all our fortune if need be, but by all means it does not mean that kde4 is perfect now. If your daily usage pattern requires those 3 features, they really start to bug after a while. "
    author: "also a debian user"
  - subject: "Well-deserved"
    date: 2009-01-20
    body: "Congrats to the many who did the work!"
    author: "T"
  - subject: "All sing along..."
    date: 2009-01-20
    body: "Weeeee aaaaaare thee chaaaampioooons\n\n:-))"
    author: "Fred"
  - subject: "pdf file only suitable for acrobat reader?"
    date: 2009-01-20
    body: "I can only open the pdf file with the awards in acrobat reader. Okular and other opensource viewers only show half of the content.\nDo others have the same problem?"
    author: "whatever noticed"
  - subject: "Re: pdf file only suitable for acrobat reader?"
    date: 2009-01-20
    body: "Yes"
    author: "goran"
  - subject: "Re: pdf file only suitable for acrobat reader?"
    date: 2009-01-20
    body: "I think it's working fine here, in both kpdf and okular.\n\nI have Okular 0.7.3 / KDE 4.1.3 / libpoppler 0.8.2 / openSUSE 11.0 ."
    author: "IAnjo"
  - subject: "Re: pdf file only suitable for acrobat reader?"
    date: 2009-01-20
    body: "Ah, I noticed this when writing the article but then forgot (since I had paper copy too), just checked and it's an issue in both okular and evince on Fedora with okular 0.7.90 on KDE 4.1.96 (4.2 RC1) from kde-redhat and poppler 0.8.7 - as both evince and okular are affected I'm thinking poppler may be the issue\n\nThere's already a bug against poppler: http://bugs.freedesktop.org/show_bug.cgi?id=19660 submitted by Riddell - I suggest if you have something useful to add do it there."
    author: "Stuart Jarvis"
  - subject: "Re: pdf file only suitable for acrobat reader?"
    date: 2009-01-20
    body: "I'm sure I'm not the only one that sees the irony here."
    author: "NabLa"
  - subject: "Re: pdf file only suitable for acrobat reader?"
    date: 2009-01-20
    body: "I just assumed it was a problem with the site---knowing it's a problem with poppler is quite hilarious, in fact!\n\nI propose next year's awards be made available only in Silverlight format.\n"
    author: "T"
  - subject: "Re: pdf file only suitable for acrobat reader?"
    date: 2009-01-20
    body: "Ironically, the file was produced by GPL Ghostscript, according to the file info\\."
    author: "Boudewijn Rempt"
  - subject: "Re: pdf file only suitable for acrobat reader?"
    date: 2009-01-20
    body: "To be fair, I don't think the PDF is really meant for public consumption - Aaron obtained the link to the PDF so that he could link to it from his blog post on this a few days ago. LXF probably intend that people mostly only see the printed copy (since they sell that) so depending on what they use internally this may not be an issue for them.\n\nAlso, there are reports (I've seen a couple now but haven't been able to check myself) that it works in older versions of poppler-based viewers so it may be a problem with a newer version of poppler rather than the PDF being bad itself.\n\nBut yeah - I do see the irony ;-)"
    author: "Stuart Jarvis"
  - subject: "Re: pdf file only suitable for acrobat reader?"
    date: 2009-01-20
    body: "You can get the link to the pdf document by clicking on Archives (PDF) at the left menu of the main page"
    author: "guerra"
  - subject: "Re: pdf file only suitable for acrobat reader?"
    date: 2009-01-20
    body: "Heh, yeah you're right - I never noticed that."
    author: "Stuart Jarvis"
  - subject: "Re: pdf file only suitable for acrobat reader?"
    date: 2009-01-21
    body: "I have the same problem, but I I just used pdftops and the \nresulting postscript is perfectly readable in okular."
    author: "John Varouhakis"
  - subject: "Re: pdf file only suitable for acrobat reader?"
    date: 2009-01-21
    body: "Works fine here with current svn build.\n\nDerek"
    author: "dkite"
  - subject: "Re: pdf file only suitable for acrobat reader?"
    date: 2009-01-22
    body: "acrobat? this should work\n\n$ gs download.pdf\n"
    author: "mdl"
  - subject: "Obligatory Mystery Men Reference"
    date: 2009-01-20
    body: "We are number one! All others are number two or below!"
    author: "David Johnson"
  - subject: "Re: Obligatory Mystery Men Reference"
    date: 2009-01-20
    body: "but ... below \"two\" ... there's again \"one\" ? !"
    author: "Thomas"
  - subject: "Re: Obligatory Mystery Men Reference"
    date: 2009-01-21
    body: "\"If you do not master your Ben Stiller movie quotes...\"\n\"Then your Ben Stiller movie quotes will become your master? That is what you were going to say, right?\"\n\"...Perhaps not.\""
    author: "diskinetic"
  - subject: "Re: Obligatory Mystery Men Reference"
    date: 2009-01-21
    body: "\"Not...  nessecarily....\""
    author: "Oscar"
  - subject: "Congratulations"
    date: 2009-01-21
    body: "I agree KDE is the best project (that I've used)"
    author: "Javier"
  - subject: "congratulations!"
    date: 2009-01-21
    body: "you are doing a great and important job.\nkeep on going!"
    author: "yuval aviel"
  - subject: "Mr"
    date: 2009-01-21
    body: "HURRRRRRRRYYYYYYY,Congrats to all KDE USERS AND DEVELOPERS :)."
    author: "Mustapha Abubakar"
---
<A HREF="http://www.linuxformat.co.uk/">Linux Format</A> magazine has unveiled its annual <A HREF="http://www.linuxformat.co.uk/pdfs/download.php?PDF=LXF115.awards.pdf">Reader Awards</A> (PDF) for 2008 and KDE won a 'landslide' victory in the category of Free Software Project of the year in recognition of the 'incredible' work done with KDE 4. Amarok, Qt, Konqueror and the KDE-based Asus Eee PC were also recognised in the awards. Read on for more details of the KDE related successes.

<!--break-->
<P>As noted in the Linux Format article, it takes a 'lot of hard work, some serious code fu and above all a commitment to the free software ideals' to succeed in the Reader Awards. The winners are chosen in an open process in which readers both determine the shortlist through nominations and the ultimate winners. KDE can be proud to be named the best Free Software Project - a wide category encompassing any project producing free software, including the likes of Compiz (runners up in this category) and giants of free software such as the Mozilla Foundation and Apache projects.</P>
<P>The hard work and successes of the Amarok and Konqueror teams were also recognised as they each came second in their respective software categories - notably Amarok was named the second placed desktop application overall, beaten only by Firefox. The Asus Eee PC which uses a custom Xandros desktop with KDE technologies won the accolade for best geek gadget of the year, beating the iPhone into second place. Trolltech (now Qt Software) was awarded second place in the embedded Linux category for Qt, rounding off a strong showing in this year's awards for KDE and related projects.</P>
