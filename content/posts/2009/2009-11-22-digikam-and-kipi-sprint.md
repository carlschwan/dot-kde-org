---
title: "digiKam and Kipi sprint"
date:    2009-11-22
authors:
  - "Marcel Wiesweg"
slug:    digikam-and-kipi-sprint
comments:
  - subject: "Tagging"
    date: 2009-11-22
    body: "I'm liking the idea of tagging parts of images. Mediawiki export would also be great - Facebook export already makes getting pics on to Facebook a lot nicer."
    author: "Stuart Jarvis"
  - subject: "Face reckognition :)))"
    date: 2009-11-22
    body: "\"With Michael G. Hansen, Gilles talked about face recognition and tagging of regions in an image.\"\r\n\r\nYou are talking about it. Hopefully we will see it in the future in digiKam, maybe even Gwenview... This one of several features I really miss in digiKam.\r\n"
    author: "StefanT"
  - subject: "NIce work guys..."
    date: 2009-11-23
    body: "My two sisters love the Facebook uploader and steal my linux laptop solely for the purpose, so full windows support is going to be awesome for me :-)  Looking forward to the face tagging too."
    author: "odysseus"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/dks1.jpg" /></div>The developers of digiKam and the Kipi project came together in Essen, Germany on November 13-15 for the second coding sprint for KDE photography applications. With digiKam preparing for the 1.0 release shortly before Christmas, plans were discussed and work began already in feature branches for the following release. A lot of work was put into polishing Windows support, with collaborative testing and bug fixing. The developers of Kamoso took the opportunity to bring Kipi support to their application. A lot of discussion was centered around a future architecture for Kipi plugins for syncing with web services and how Akonadi could help in this context.

digiKam developer Gilles Caulier hacked with Kåre Särs on the Acquire Images Kipi plugin and found some problems with the libksane Twain implementation. Working on cross-platform support, he reported a lot of problems with digiKam on Windows to Patrick Spendrin, who immediately went to fix them, shared his experience to compile digiKam under MinGW and MSVC, and tested compilation under Mac OS X on Kåre's borrowed MacBook. Some problems in the liblqr library are waiting to be fixed.

In discussions with Luka Renko, Alex Fiestas and Aleix Pol, plans for a common codebase for all import/export Kipi plugins were drafted. The Kipi interface was extended with a new way to export a whole collection using default settings and not starting a dialog.

Gilles and Marcel Wiesweg shared initial thoughts about future image versioning support in digiKam and wishes to support grouping of pictures. Gilles and Andi Clemens discussed how to refactor all the preview widgets in digiKam, which are still based on Qt3 support classes and wait for a pure Qt4 port. Features were reviewed and a paper created in SVN.

With Michael G. Hansen, Gilles talked about face recognition and tagging of regions in an image. Here as well a proposal paper was created listing leading ideas. Improvements in the camera interface and supporting KDE notification for downloads were discussed.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/dks2.jpg" /></div>When not busy with coding and discussing, he tried to walk around inconspicuously with his camera to take photos of working hackers, but not only to personal pleasure. Gilles used these photos to perform in-depth testing of digiKam's RAW file import tool and reveal some problems. At last, Gilles started to write a new HDR creator plugin based on Qtpfsgui, but code is still under construction.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/dks3.jpg" /></div>Alex and Aleix, Kamoso developers, adapted the Kipi Plugins architecture to Kamoso. It was changed to provide better communication between the plugins and the host application. Done with that, they ported Kamoso to use Kipi. That way Kamoso will be able to take advantage of the whole architecture (and hopefully benefit from the work of more people in the future). Also they started the port of the YouTube plugin to Kipi so at some point in the near future it will be possible to use it with the rest of the Kipi applications.

Besides the usual amount of bug hunting and fixing, Kåre, who is the author of libksane, moved all the scan operations to separate threads to improve the scanning performance and to improve the responsiveness of the GUI. At the same time he cleaned up the logic a bit to make it easier to follow and implemented small GUI enhancements.

Holger Foerster, working on MySQL support for digiKam, brought his feature branch up to date again and, working with Marcel, ensured it is usable as well with the traditional SQLite database. MySQL support marks the start for a multi user digiKam application and other features in the future.

Functionality was tested and bugs fixed. For data conversion between the supported databases, development of a dialog was started where the user can copy the data from one database to another.

Guillaume Paumier's main goal was to start working on an export plugin to Wikimedia Commons. By analyzing how the existing Facebook KIPI plugin was made, he was able to create the basis of the communication layer between the plugin and a MediaWiki site, even though not having done any C++/Qt development before. While the current work focuses on a specific wiki (Wikimedia Commons), it could be later extended to a more general MediaWiki library for Qt.

Tom Albers, Akonadi and Mailody developer, joined to give a talk and first-hand advice about Akonadi. He worked on an Akonadi resource for images and tried to convince the participating developers about utilizing Akonadi within Digikam. At the end of the meeting, the resource was able to succesfully and recursively list images in a folder and show their thumbnails.

Johannes Wienke started porting the remaining substantial bits of code in digiKam still based on Q3Support view classes to the Qt model-view concept. At the sprint he started with the left side bar widgets. Porting the views there, and later the ones for the right side bar and other widgets, includes structural improvements and code simplifications.

Luka started refactoring the KIPI Facebook Import/Export dialog by integrating all progress dialogs into the main dialog. This work will continue in the direction of a common UI look and feel for all import/export dialogs in KIPI. Guillaume's new Wikimedia plugin will be based on the new UI design agreed on during the sprint. Luka also dived into Akonadi resources and KFacebook library: the plan is to extend them to provide virtual albums for Facebook through Akonadi.

Michael worked on two areas: geolocation and region tagging. Michael discussed plans for a more generic geolocation-widget with Marcel and talked about nice-to-have features for better Marble integration with SaroEngels. As already said above, he discussed plans for tagging certain regions of an image as known from popular web services, and how to store such information in image metadata.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/dks4.jpg" /></div>Andi set up a KDE build environment for Windows to finally be able to hunt bugs on the Microsoft platform as well, then took a look at some Windows bugs with Patrick. This took some time, so unfortunately time was getting short for talking with Gilles and working on the new image editor layout and widgets planned for digiKam post-1.0. He fixed some bugs in AdvancedRename utility to make it work under Windows, appearing pretty much broken on MS in the current state.

Marcel organized and hosted the whole event and is now happy that no severe complications arised (only Gilles being rejected to take his intended flight, but arriving the next morning, and Pieter Edelman missing the event due to flu), and happy that everyone hopefully enjoyed the meeting. On Friday and Sunday he was concerned with picking up and bringing people from and to the airport and train station. When started with coding, he worked with Johannes to point to all the code locations needing a model-view port, with Holger to round up the work on MySQL support, and discussed with Michael about geolocation in digiKam, its present problems and ideas for the future.

The image metadata library in kdegraphics, libkexiv2, got one long-standing important bug fix requiring extensive changes, some cleanup, and support for accessing embedded previews for KDE 4.4.

All event photos are available on <a href="http://www.flickr.com/groups/kde-imaging-codingsprint/pool">Flickr</a>.