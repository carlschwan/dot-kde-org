---
title: "Kubuntu Tutorials Day will Get You into KDE Programming"
date:    2009-06-26
authors:
  - "jriddell"
slug:    kubuntu-tutorials-day-will-get-you-kde-programming
---
Time for another <a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">Kubuntu Tutorials Day</a>.  Learn about KDE and Kubuntu development in a helpful atmosphere next Monday from 19:00UTC in the #kubuntu-devel IRC freenode channel.  We have a lineup of exciting speakers on a range of topics.  Read on for the timetable
<!--break-->
<img src="http://people.ubuntu.com/~jriddell/kubuntu-tutorials-day-2009.png" width="472" height="148" />

The timetable looks like this:

19:00UTC <strong>The next six months with Kubuntu</strong> with Roderick Greening 
20:00UTC <strong>Getting into Ruby</strong> with Harald Sitter 
21:00UTC <strong>Packaging and Merging with the Ninjas</strong> with Jonathan Riddell 
22:00UTC <strong>Artwork The composition of an icon</strong> with Ken Wimer
23:00UTC <strong>Amarok scripting</strong> with Sven Krohlas
end of talks onwards, <strong>Kubuntu Q & A Ask us anything you want to know