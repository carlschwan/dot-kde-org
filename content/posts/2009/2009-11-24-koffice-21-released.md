---
title: "KOffice 2.1 Released"
date:    2009-11-24
authors:
  - "ingwa"
slug:    koffice-21-released
comments:
  - subject: "Karbon vs. Inkscape?"
    date: 2009-11-25
    body: "Anyone know how the new Karbon compares to Inkscape? I guess the version numbers (2.1 vs 0.47) tell all that much.\r\n\r\nI only need really basic SVG drawing."
    author: "kragil"
  - subject: "Then it should really be"
    date: 2009-11-25
    body: "Then it should really be fine, in fact I think it exceeds that requirement even. The handling IMHO is a lot nicer than Inkscape's in many ways; the UI is less dense and many tools can be handled more intuitively than in Inkscake by way of on-canvas controls. There's a lot of nice attention to detail in there, e.g. in the snapping options or the gradient tool. It's really a great app in v2.1, I'm very impressed."
    author: "Eike Hein"
  - subject: "Yep, try Karbon if you're used to KDE software"
    date: 2009-11-25
    body: "I used it for the first time (seriously at least) a couple of weeks ago to make some simple instrument schematics and I found it easier to use than I've found Inkscape in the past - probably just because it follows more the the KDE software interface conventions and so felt a bit more familiar.\r\n\r\nInkscape's a great app too, and maybe superior for complex stuff, but for my (simple) use-cases Karbon is just fine."
    author: "Stuart Jarvis"
  - subject: "Great article by the way"
    date: 2009-11-25
    body: "You did a really nice job with this, Inge. The sections are really well set out, the status and intended audience is made clear early on and the key points are highlighted in the text."
    author: "Stuart Jarvis"
  - subject: "Thank you"
    date: 2009-11-25
    body: "It felt good afterwards, and I had some good help proofreading (thanks Chani et al)."
    author: "ingwa"
  - subject: "Thanks"
    date: 2009-11-26
    body: "Thank you for the answers, I will try it once .debs are available.\r\n\r\nBTW: Will Braindump be part of KOffice?\r\n( http://cyrilleberger.blogspot.com/2009/05/braindump.html )"
    author: "kragil"
  - subject: "Never judge by version"
    date: 2009-11-26
    body: "Never judge by version numbers in the FOSS world, they can be very misleading as everyone has their own numbering scheme."
    author: "odysseus"
  - subject: "Braindump"
    date: 2009-11-26
    body: "Not on the short term, it will be released seperatelly. But I do intend to make a beta release, sometime soon."
    author: "cyrille"
  - subject: "Krita Realistic Color Mixing"
    date: 2009-11-26
    body: "Are the results of the realistic color mixing GSoC project now integrated in Krita?"
    author: "gd"
  - subject: "optimize"
    date: 2009-11-27
    body: "I just tried the new version of Krita on my desktop, and compared to the gimp, it takes a lot longer to see the effects of a levels adjustment to show up. Heres hoping that we can see an effort to optimize the code for 2.2."
    author: "reub2000"
---
The KOffice team is very happy to <a href="http://www.koffice.org/news/koffice-2-1-released/">announce version 2.1.0 of KOffice</a>, 6 months after the platform release 2.0.0.  This release brings a number of new features as well as general improvements in the maturity of the individual applications. Importing of documents have also been given an overhaul.

The advantages of the clean and well-structured codebase have started to show. Despite a relatively limited developer group, there are a large number of improvements over 2.0. During the development of 2.1, it was also announced that KOffice is going to be <a title="KOffice used in Maemo 5" href="http://www.koffice.org/news/office-viewer-for-maemo5-based-on-koffice/">used in the Nokia n900 smartphones based on Maemo Linux</a>.

<h2>The State of KOffice 2.1</h2>
This release is a marked improvement of almost all parts of KOffice compared to 2.0. Version 2.0 was labeled a "platform release", which was meant as a first preview of the framework and new UI paradigm. In version 2.1, most applications and components have improved significantly <strong>but should still only be used by early adopters and probably not as the primary worktool</strong>.

The exceptions are the graphics applications: Krita, the picture editor / paint application, and Karbon, the vector editor application. Nuno Pinheiro, the designer of the Oxygen KDE theme and icon set says: "<em>I have tested Karbon and it is definitely usable for real work even if lacks a few advanced SVG features</em>". What Krita is capable of can be seen at the <a title="Krita gallery" href="http://forum.kde.org/viewforum.php?f=138">Gallery</a> in the <a title="Krita forums" href="http://forum.kde.org/viewforum.php?f=136&amp;sid=b2ea76c8b90813dba346008acfc044d8">Krita forums</a>.

<a href="http://dot.kde.org/sites/dot.kde.org/files/karbon-oxygen-icon_0.png"><img class="size-medium wp-image-724" title="karbon-oxygen-icon" src="http://dot.kde.org/sites/dot.kde.org/files/karbon-oxygen-icon-490x368_0.png" alt="Karbon 2.1 editing the oxygen Webcam icon" width="490" height="368" /></a>

<h2>Targeted Audience</h2>
Our goal for this release is to show the public the current state of KOffice and create a platform for further improvements.  This release is mainly aimed at developers, testers and early adopters.  <strong>It is not aimed at end users, and we do not recommend Linux distributions to package it as the default office suite yet.</strong>


It is noteworthy that KOffice 2.1 still does not have all the features that KOffice 1.6 had, even if it in some ways greatly exceeds KOffice 1.6.  These features will return in the upcoming version  2.2, in most cases better implemented and more efficient. Also, not all applications which were part of KOffice 1.6 made it into KOffice 2.1.  The missing applications will return in 2.2 or possibly 2.3.
<h2>Applications and Components</h2>
The following applications and plugins are part of KOffice 2.1:
<h3>General</h3>
All parts of KOffice, including the underlying libraries have received many bugfixes and improvements. Special attention has been paid to support for the OpenDocument Format, ODF. One example of that is <a title="Bugfixing of lists in KWord" href="http://www.koffice.org/background/bugfixing-lists/">handling of lists</a>. To ensure the quality of the ODF implementation, the KOffice team has participated in ODF interoperability tests called the <a title="KOffice at the ODF plugfest" href="http://ingwa2.blogspot.com/2009/11/koffice-at-odf-plugfest-and-ooocon-2009.html">ODF Plugfest</a>. For a more detailed list, refer to the announcements and changelogs for the alpha and beta versions of KOffice 2.1.
<h3>KWord - Word processor</h3>
KWord has a new implementation of tables. The tables are more advanced than the ones in version 1.6: They can be larger than one page and support nested tables in tables and they support a number of formatting properties. The drawback is that the table contents can be edited, but the tables themselves will only become editable in the next version.

Another large new feature in KWord is change tracking. Changes in the document can be tracked and are displayed color coded. Insertions, deletions and formatting changes have different colors. Still lacking is a dialog to select individual changes and make them permanent.
<h3>KSpread - Spreadsheet calculator</h3>
There are no big new features in KSpread, but a fair number of bugfixes.
<h3>KPresenter - Presentation manager</h3>
KPresenter has a few new features and many bug fixes. Some of the new features are an infinite canvas and slide numbers.
<h3>KPlato - Project management software</h3>
KPlato has been improved a lot, and has support for many different manipulations of resources, currencies and work types.  It also has support for offline work using a small utility called KPlatoWork. KPlato was featured in the <a title="Announcement for KOffice 2.1RC1" href="http://www.koffice.org/news/koffice-2-1-release-candidate-1-released/">release announcement for KOffice 2.1 RC1</a>.
<h3>Karbon - Vector graphics editor</h3>
Karbon 2.1 has a new framework for SVG filter effect plugins. There are also a few filters implemented: offset, blur and merge. These can be combined to create a drop shadow effect.

Also new in 2.1 is an import filter for PDF files.
<h3>Krita - Raster graphics editor</h3>
Krita has new brushes, among them a spray brush and a deform brush, along with a 3D visualization of brushes. It also has a new infinite canvas feature and a dyna tool that can simulate a pressure sensitive input device by using the speed of the mouse.
<h3>Plugins</h3>
The <strong>Chart</strong> plugin (or so called <em>shape</em>) has received a number of bugfixes but no new features.

The <strong>Formula</strong> shape is now back in KOffice 2.1 after being absent in 2.0.  It is not very advanced at the moment.

The <strong>Picture</strong> shape has seen many optimizations.
<h3>Import and Export</h3>
Import and export of foreign file formats have been given an overhaul, and especially the import filters for Microsoft PowerPoint (.ppt) and Word (.doc) have been much improved. This is a result of the work done for the Nokia n900 Office viewers.

The desktop database creator Kexi was scheduled for inclusion in 2.1, but has not been finished. Right now we hope to be able to release it in version 2.2. Kivio, the flowchart editor, is currently without maintainer and it is not certain when or if it will be released.
<h2>About KOffice 2</h2>
KOffice 2 is a much more flexible application suite than KOffice 1 ever was.  The integration between the components is much stronger, with the revolutionary Flake Shapes as the central concept.  A Flake Shape can be as simple as a square or a circle or as complex as a chart or a music score.

With Flake, any KOffice application can handle any shape. For instance, KWord can embed bitmap graphics, Krita can embed vector graphics and Karbon can embed charts.  This flexibility does not only give KOffice unprecedented integration, but also allows new applications to be created very easily.  Such applications can e.g. target special user groups like kids or certain professions.


<h3>Unified Look and Feel</h3>
All the applications of KOffice have a new GUI layout better suited to todays wider screens.  The GUI consists of a workspace and a sidebar where tools can dock.  Any tool can be ripped off to create its own window and later be redocked for full flexibility.  The tool layout is of course saved and reused the next time that KOffice is started.
<h3>Platform Independence</h3>
KOffice is available on Linux (with any desktop), Windows and Macintosh.  Solaris will follow shortly and we expect builds for other Unix versions to become available soon after the final release.  It is possible that the release of binaries for Windows and Macintosh will occur after some time if other packages that KOffice depend on need more time.

Since KOffice builds on Qt and the KDE libraries, all applications integrate well with the respective platforms and will take on the native look and feel.

<h3>Native Support for OpenDocument</h3>
The OASIS OpenDocument Format (ODF) is the ISO standard for office document interchange.  ODF has been selected as the national standard for many countries around the world, and continues to grow stronger every month.

KOffice uses the OpenDocument Format as its native format. This will guarantee interoperability with many other Office packages such as OpenOffice.org and MS Office.  The KOffice team has representatives on the OASIS technical committee for ODF and has been a strong participant in the process of shaping ODF since its inception.