---
title: "KOffice 2.1 Beta 2 Released"
date:    2009-09-16
authors:
  - "ingwa"
slug:    koffice-21-beta-2-released
comments:
  - subject: "just to note that:"
    date: 2009-09-16
    body: "<ul><li>\"the full changelog\" link points to a \"Error 404 - Not Found\".</li>\r\n<li>Seems that there has been a doubled copy-paste somewhere, because the sentence \"Something that is not obvious [...]\" and the paragraph \"As was mentioned in the release notes for beta 1 [...]\" are present twice.</li>\r\n</ul>\r\ncheers!"
    author: "111bits"
  - subject: "fixed ;-)"
    date: 2009-09-16
    body: "fixed ;-)"
    author: "jospoortvliet"
  - subject: "Awesome"
    date: 2009-09-16
    body: "Looking forward to 2.1.  I already use 2.01 and love the interface, but it is a bit sparse in some areas and I sometimes have to revert to OOo.  e.g., For Karbon14, I notice it doesn't seem to import or otherwise deal with multipage ODG files from OOo.\r\n\r\nNevertheless the potential of KOffice 2 is awesome.  Keep up the great work."
    author: "Jerzy Bischoff"
  - subject: "Impressive"
    date: 2009-09-16
    body: "The changelog indeed reveals an impressive amount of improvement!\r\n\r\nAs a Kubuntu Jaunty user, I encountered unmet dependencies when I tried to install it from the beta PPA. Does one have to have KDE 4.3 installed? If so, I'll wait until Karmic."
    author: "tangent"
  - subject: "Good to the there is progress"
    date: 2009-09-16
    body: "... I am really happy to see that Koffice is making its way and hopefully soon to be able to replace ressource-consuming OpenOffice.Org for many KDE-users.\r\nI am tempted to give it a try, but last time I did there were many problems with the OpenDocument-Format. Changing the office-suite is not an easy decision to make. It would be helpful to have a kind of feature comparism-list for KOffice and OpenOffice.org, especially in terms of import/export-functionalities of .doc, docx and rtf and the differences (that should not exist) between OpenDocument-implementation.\r\nThe redering engine (do not know how to call it otherwise) of KWord looks really beautiful and beats everything I have seen in OpenOffice and MsOffice.\r\nWhat kind of disturbs are these experiments you seem to be making with the user-interface. Of course it is necessary to try new approaches. But on first start, Kword looks more like a dtp-programme. I don't see any use for me and therefore I would like to get rid of that (right side) toolbar-stuff. Is this possible? I could not find away. May be there should be the possible to switch easily between a traditional and an experimental user interface in Koffice."
    author: "mark"
  - subject: "Configuring dockers"
    date: 2009-09-17
    body: "KWord does follow some DTP ideas (like using frames) so having an interface that reminds you of that is not surprising. Obviously you can question whether it's the best user interface for your personal usage.\r\n\r\nYou can get rid of most of the stuff on the right hand side by going to Settings > Dockers and unchecking the boxes. That leaves just one with the selection tools, which you can then move elsewhere (just grab the top of it and drag) and resize - although it seems you can't get rid of it completely.\r\n\r\nA feature comparison would be nice."
    author: "swjarvis"
  - subject: "Text flow around shapes"
    date: 2009-09-17
    body: "Maybe it is now possible to have text flow around a shape on _both_ sides."
    author: "axeloi"
  - subject: "MS Doc support"
    date: 2009-09-17
    body: "Thomas Zander has blogged about the source of some of the improved MS doc support here: http://www.koffice.org/news/office-viewer-for-maemo5-based-on-koffice/\r\n\r\nSo, Nokia has stepped up with some contributions in this area which is great and it's also exactly what the KOffice devs were saying when people were complaining about MS doc support: if someone wants to add it, they can. And now here we are...\r\n\r\nKudos KOffice devs for making such a great framework and set of applications that they attract commercial support by the allure of their quality alone!"
    author: "borker"
  - subject: "Re: Good to the there is progress"
    date: 2009-09-18
    body: "The interface of KOffice is the natural result of displays becoming wider and wider, while the paper format is still the same. With tools ordered at the left and right, you can inspect a maximum of information on your sheet. (Compare screenshots of KOffice with OOo or MSO if you don't see what I mean. If you want to view many content at once in OOo or MSO, there is much unused space on the left and right.)"
    author: "majewsky"
---
The KOffice team today announced the second beta of the upcoming 2.1 release. The KOffice community has now switched from adding new features to only fix the remaining bugs, and that is obvious from this release. The first beta of 2.1 was released without any fanfares, but it marked the transition into the bugfixing stage. We now think it's time to let the users start to participate in the process.  You can see the progress in <a title="Changelog 2.1 Beta 1" href="http://www.koffice.org/changelogs/koffice-21beta2-changelog">the full changelog</a>.

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 400px">
<a href="http://dot.kde.org/sites/dot.kde.org/files/karbonfilters.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/karbonfilters-wee.png" width="400" height="289" /></a><br />
Karbon
</div>

Something that is not obvious from the changelog is that there has been much activity in the MS office import filters, especially for MS Word and Powerpoint.  Many new formatting features have been implemented in both these filters. We expect KOffice 2.1 to be better at reading MS file formats than any previous KOffice version.

As was mentioned in the release notes for beta 1, the 2.1 release will contain new features in most applications, especially KWord, KPresenter and Krita. It also features the come back of the mathematical formula shape.

Since this release includes so many bugfixes, we encourage users that want to help with the process to try it out and report remaining bugs.  As usual we are especially interested in bugs that cause data loss or incompatibilities with other office suites such as OpenOffice.org.

For more detailed information see the <a href="http://www.koffice.org/news/koffice-2-1-beta-2-released/">announcement</a>.