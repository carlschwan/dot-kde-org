---
title: "What I Did On My Summer Holiday"
date:    2009-09-26
authors:
  - "nightrose"
slug:    what-i-did-my-summer-holiday
comments:
  - subject: "Impressive !"
    date: 2009-09-27
    body: "Thank you to all the students! A lot of awesome stuff there.\r\n\r\nWe should also thank Google that really improve free software over the years with its GSOC!"
    author: "gaboo"
  - subject: "Yes, great work! Thanks to"
    date: 2009-09-27
    body: "Yes, great work! Thanks to All!\r\n\r\nOne comment: I think the thanks to google could receive a bit more prominence.\r\n\r\nI really liked the \"Thanks To\" tab shown on Plasmamediacenter, and I think it would be appropriate to add something like 2009 Google Summer of Code."
    author: "jfd5xte"
  - subject: "On Kolf 2"
    date: 2009-09-27
    body: "Now that it it mentioned here, I (as the lead developer) would like to repeat my previous blog post on this topic: Kolf 2.0 won't be ready for KDE 4.4, because the subjects on which Zeng has worked have proven to be much more complex than we thought at first. Also, other parts of Kolf need much more polish, or still need to be written."
    author: "majewsky"
  - subject: "Brilliant!"
    date: 2009-09-27
    body: "Glad to see that programs are starting to take advantage of the new frameworks - that's what I was really waiting on!\r\n\r\n'Vyacheslav Tokarev had to concentrate on fixing things first in his project WYSIWYG (What You See Is What You Get) support in khtml: \"I was able to improve caret positioning code, selection, edit commands. It's very technical and I had to do a lot of debugging to find the correct fixes. From now (in my khtml branch) WYSIWYG editors (with implemented commands) mostly work well and very importantly without crashes. Though I didn't end up implementing all the missing commands (only 2). It's important too to have the rich text editing features, but I had to put all my efforts on the stabilization and the improvement of the existing framework. (e.g. refactoring the code). As for the future plans I'm gonna merge editing branch to the trunk in upcoming days and will work on that later.\" Many websites use WYSIWYG editors, and thanks to the work of Vyacheslav these will work in newer versions of Konqueror.'\r\n\r\nExcellent! This is all I've still got Firefox for. Can't wait to use Google Docs in Konqueror. :D"
    author: "madman"
  - subject: "wow"
    date: 2009-09-28
    body: "I am always impressed by GSOC participants.  Great work to all of you ! and I do hope that you all will continue being a part of the project."
    author: "blantonv"
  - subject: "*impressed*"
    date: 2009-09-28
    body: "The results of this years SoC look awesome! \r\n\r\nMany thanks to all of you for making KDE even better! \r\n\r\nI look forward to KDE 4.4! "
    author: "ArneBab"
  - subject: "Why thanks to"
    date: 2009-09-28
    body: "Why, what does make Google so special they should receive more prominence? \r\n\r\nAfter all, they only sponsor a few developers a very limited time. As oposed to other commercial enteties sponsoring developers full time for the whole year, most of wich have far less financial resources than Google. "
    author: "Morty"
  - subject: "Re: Why thanks to"
    date: 2009-09-28
    body: "\"Why, what does make Google so special they should receive more prominence?\"\r\n\r\nThe Summer of Code is about sponsoring students, many of whom are new contributers to KDE. That is different from full time established developers being sponsored all year round.\r\n\r\nPersonally I'm pretty excited to read about all the awesome work that the students have done, and I think a lot of other people are too. Sorry to hear that you're not."
    author: "Richard Dale"
  - subject: "Pleas don't missread"
    date: 2009-09-29
    body: "As lots of others I'm also exited about the awesome work done. But I prefer praise the ones doing the job, the students and their mentors. Rather than a multi billion dollar comapny throwing some spare change around."
    author: "Morty"
---
<img src="http://dot.kde.org/sites/dot.kde.org/files/2009soclogo.gif" width="300" height="200" align="right" />

<a href="http://socghop.appspot.com/org/home/google/gsoc2009/kde">Google Summer of Code</a> has again been a huge success for KDE this year. 37 out of 38 projects were finished successfully. Much of the work done during these projects is already merged into trunk and will be available for the users with the KDE 4.4 release in January 2010. Thanks to all students and mentors for their great work! Below you will find a short interview with each of the students, asking them about the cool things they have been working on for the past few months.
<!--break-->
Alessandro Sivieri about his project: "The aim of the project was to create a semantic load and save dialog system, an alternative to the current one. The idea is to allow users to specify meta information, tags and relations about their files while saving them, instead of just a location and a filename. This will enable you to find your files later on. The dialog system has been created and now my mentor Sebastian Trueg and I are working on integrating it into kdelibs. The user will be able to switch between the current file save dialog and the new one, making it easy to try it. This work is still in progress and of course I will blog about it as soon as it will be testable. It has been great working with the KDE community in these three months, and I hope to enjoy it more and more in the future." Find out more in <a href="http://www.chimera-bellerofonte.eu/tag/gsoc">Alessandro's blog</a>.

<a href="http://dot.kde.org/sites/dot.kde.org/files/alesandrosivieri.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/alesandrosivieri_1.png"></a>

Martin Sandsmark worked on Phonon: "During this summer I implemented support in Phonon (the KDE multimedia framework used in Amarok, JuK and DragonPlayer, amongst others) for exporting audio data. This in turn enables applications that use Phonon to analyse the audio and for example display pretty visualisations or calculate the BPM." So hooray for pretty visualisations in Amarok soon!

<a href="http://dot.kde.org/sites/dot.kde.org/files/martinsansmark.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/martinsansmark_1.png"></a>

Chani Armitage brought back a KDE 3 feature by implementing plugins for mouse clicks on the desktop background like window list, application list, etc. Read more about it in <a href="http://chani.wordpress.com/">her blog</a>.

<a href="http://dot.kde.org/sites/dot.kde.org/files/chaniarmitage.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/chaniarmitage_1.png"></a>

<a href="http://algebraicallyclosed.blogspot.com/">Jeremias Epperlein</a> has helped the KOffice team: "I have been working on the formula or equation editor of KOffice under the mentorship of Alfredo Beaumont. KOffice is based on the Flake framework, where each content element type is represented as a plugin, called Flake shape. I implemented editing for the formula shape used for representing mathematical equations. This includes cursor movement, mouse handling, selection and editing support. You can now insert and remove variables, numbers, operators, fractions, roots, sub- and superscripts, under- and overscripts, tables and so on. All this works with proper undo/redo. You can manipulate tables as well as load and save the resulting MathML documents." Thanks to the hard work of Alfredo, KOffice will have state-of-the-art MathML formula support!

<a href="http://dot.kde.org/sites/dot.kde.org/files/jeremiasepperlein.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/jeremiasepperlein_1.png"></a>

Christopher Ing helped improve Step, KDE's physics simulator. "I implemented a smoothed particle hydrodynamics algorithm in KDE-Edu's Step for fast fluid simulation. This will allow science educators to quantitatively measure and qualitatively observe interesting properties of fluids." Physics students will love this - the improvements to Step will make it easy to visualise and experiment with complex fluid physics. Go and read his <a href="http://www.jacksofscience.com/physics/three-aspects-of-a-great-fluid-simulation/">personal Summer of Code conclusion</a>.

<a href="http://dot.kde.org/sites/dot.kde.org/files/christophering_1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/christophering.png"></a>

Constantin Berzan had fun with the KDE PIM team: "This summer I worked on two bits that will be part of the underlying architecture for KMail 2, namely a new Akonadi mail dispatcher agent and a new mail composing library based on KMime. The KDE PIM team is absolutely awesome and you can expect amazing things in the years to come (-;."

Marwan Hilmi built a KDE based game client for the Thousand Parsec 4X game framework: "Over the summer, I've created a game client, which integrates with a Thousand Parsec libtpproto-cpp protocol library, and can connect to game servers and display basic game information. Progress was made in providing Parsek with full gameplay functionality." Marwan wrote about his progress in <a href="http://www.marwanhilmi.com">his blog</a>.

<a href="http://awainzin-foss.blogspot.com/">Alejandro Wainzinger</a> worked on finishing up media device support on Amarok: "This meant bringing back playlists, beefing up the warnings/errors on actions related to devices, stale/orphaned entry cleanup support for iPods, and supporting Universal Mass Storage devices. A lot of code had to be de-sqlified to make this possible, which has had good side-effects on other parts of Amarok.  It also ended up in a very large media device refactor that will hopefully make it easier for other devices to make a comeback if anyone is willing to take up the job." This is his second round of GSoC and he is now a core member of the Amarok team.

<img src="http://dot.kde.org/sites/dot.kde.org/files/alejandrowainzinger.png">

Alessandro Diaferia worked together with the Plasma team on a media center: "My GSoC project has come to its end but my effort into MediaCenter won't stop now, I'll keep working on it trying to make it a valid KDE piece of software. My big thank you goes to my mentor Marco Martin who helped me a lot in my path through this huge project and of course to the KDE community who made this possible."

<a href="http://dot.kde.org/sites/dot.kde.org/files/allesandrodiaferia.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/allesandrodiaferia_1.png"></a>

Ana Cecília Martins Barbosa joined the Plasma crew to do a New Widgets Explorer: "It basically consisted of a re-design of the current one. UX concepts and techniques were applied to lead us to the best result. These concepts will still be evaluated, but the success of the project can be seen with a quick comparison of the old and new Widgets Explorer." The code was integrated into KDE during the last <a href="http://dot.kde.org/2009/09/08/third-plasma-summit-lifts-kde-desktop-higher-grounds">Plasma sprint</a> in the Swiss Alps. Her work will make it a lot easier to manage new Plasma applets in KDE 4.4!

<a href="http://dot.kde.org/sites/dot.kde.org/files/anaceciliamartins.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/anaceciliamartins_1.png"></a>

Arno Rehn was working on the new generator for SMOKE (library which wraps C++ classes and methods and makes them available to other programming language) together with the KDE Bindings team. "It's based on the KDevelop4 C++ parser and finished by now. The code is in trunk, but it's not yet used. Nokia has done pretty much the same with their PySide bindings and libapiextractor that they developed for it. Maybe we can merge the two projects so that they use a common basis." <a href="http://www.arnorehn.de/">His blog</a> has more details.

Bastian Holst worked on a weather plugin and some other plugins for Marble. This included work on Marble itself, for example on the plugin dialog and the backend of all online services like Wikipedia and photo plugin. He made displaying items in Marble easier and his work will let Marble introduce many new features in future.

<a href="http://dot.kde.org/sites/dot.kde.org/files/bastianholst.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/bastianholst_1.png"></a>

<a href="http://lukast.mediablog.sk">Lukas Tvrdy</a> brings three important features to Krita 2.1: infinite canvas, OpenGL tool overlay and 3D brush visualisation. "Infinite canvas, implementation of the canvas from KOffice libraries where the mouse and tablet events and mainly strokes are not cut by the border of the canvas. So you can start the stroke outside of the canvas. You also can paint outside of the canvas, because internally the layers in Krita are infinite. OpenGL tool overlay is e.g. line you see when you use line tool. It is the preview of the line, which is going to be painted. If you enable OpenGL canvas, the tool overlays will be painted with XOR operation using OpenGL. 3D brush visualisation is something handy when you own a tablet. You don't have to watch your hands, you can see the 3D brush on the screen."

<a href="http://dot.kde.org/sites/dot.kde.org/files/lukastvrdy.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/lukastvrdy_1.png"></a>

Diego Casella worked on PlasMate (an editor for plasma widgets, data engines and themes) and mainly on its TimeLine: "The TimeLine is designed to provide for the beginning developer a simple and easy to use interface to manage his or her project history. Internally this is done by using a git repo; however, since the developer doesn't need to know this implementation detail, the usual terms like commit and branch are replace by the more comfortable "SavePoint" and "Section" terms. The commands available are reduced, compared with git, in order to keep it simple and easy to understand; however they are still effective to accomplish common operation such as creating a savepoint based on the current project status, or rolling back to a previous savepoint, delete it, or creating multiple sections, switching between sections and merging them." For further info and screenshots, take a look at his <a href="http://polentino911.wordpress.com">blog</a>.

Dmitry Kazakov worked on Krita's tile engine: "I had two aims in my project. First was to rewrite tiles-subsystem to allow swapping and compressing of unneeded tile data of Krita. The second was to implement a mipmapping for faster zooming. Both of them are in trunk, but both are in testing state as mipmapping made me revise very-very much code of Krita." The result of this work will make Krita much snappier when handling large images.

<a href="http://blog.edulix.es/">Eduardo Robles Elvira</a> tells us: "My GSoC was about reimplementing Konqueror Bookmarks system with new KDE technology that was not available at the time, mainly Akonadi and Nepomuk. I rewrote the base resource where Bookmarks are stored, then the bookmarks organizer, the bookmarks menu and the location bar. This touches the heart of Konqueror and means writing a lot of lines of code, a work which not yet ready to land in Konqueror trunk but hopefully will end up there sooner than later."

Basic tables support for KWord was implemented by <a href="http://estan.dose.se/">Elvis Stansvik</a>. KWord now has basic support for laying out ODF tables. Features that were implemented include table width, relative width, margin and background color, column width and relative width, cell padding, borders and background color as well as column and row spanning. These features will be available in KOffice 2.1.

<a href="http://dot.kde.org/sites/dot.kde.org/files/elvisstansvik.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/elvisstansvik_1.png"></a>

George Kiagiadakis wrote KCall, a voip client based on the telepathy framework: "KCall currently supports audio and video chat over Jabber, and although there are still a few bugs, it is usable for making calls. However, I don't consider it ready for general use, mostly because of usability issues and some other bugs/issues in the underlying components of telepathy." He will continue working on it, and you can read more about KCall in <a href="http://gkiagia.wordpress.com/">his blog</a>.

<a href="http://dot.kde.org/sites/dot.kde.org/files/georgekiagiadakis.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/georgekiagiadakis_1.png"></a>

<a href="http://www.kashthealien.wordpress.com">Kashyap Puranik</a> created the calculator widget for Kalzium. "It currently has 4 calculators: molecular mass calulator, gas calculator (calculates temperature, volume, pressure etc), concentration calculator (calculates solution concentration) and a nuclear calculator (nuclear degradation calculation). I have also created a Plasma widgets for the same." Again an addition for KDE-Edu users which will be appreciated in schools around the world!

<a href="http://dot.kde.org/sites/dot.kde.org/files/kashyappuranik.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kashyappuranik_1.png"></a>

Ahmmahfuz Rubel joined the KDEGames team to implement a Mancala game. "I implemented a human to computer playable Mancala game. Currently, it supports 7 Mancala games from different regions. There are 3 different levels of difficulty. In the future, I am planning to provide support for online play and an easy way for custom game making."

<a href="http://dot.kde.org/sites/dot.kde.org/files/ahmmahfuzrubel.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/ahmmahfuzrubel_1.png"></a>

Matthias Fuchs worked on KGet and thanks to him it now supports multisource-downloading. This means you can add multiple mirrors to a transfer. Transfers can be verified and repaired, renamed and moved on the filesystem while downloading. Checksums for a download can be automatically searched for on the internet and the Metalinkcreator makes it possible to create your own metalinks.

<a href="http://dot.kde.org/sites/dot.kde.org/files/matthiasfuchs.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/matthiasfuchs_1.png"></a>

Prakash Mohan created an Observation Planner for KStars, which would make the life of any astronomer easy in planning, scheduling and executing observation sessions. The planner has features like "auto add visible objects tonight to list", "download images from the web" and a nice way of storing your observation logs. For more details read on in <a href="http://praksh.wordpress.com">his blog</a>.

<a href="http://dot.kde.org/sites/dot.kde.org/files/prakashmohan.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/prakashmohan_1.png"></a>

Kaushik Saurabh tells us: "I have worked on Kopete's history plugin so that it saves its chats in Akonadi server and on TelepathyWatcher, a telepathy client, that logs text chats done by any chat client using telepathyprotocol. Though everything works, I am still working on it and trying to improve it." His work will make searching through your chat logs much easier.

<a href="http://dot.kde.org/sites/dot.kde.org/files/kaushiksaurabh.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/kaushiksaurabh_1.jpeg"></a>

Sandro Andrade started a project which aimed to implement better approaches for program comprehension and developer interaction with software artifacts in KDevelop by means of code visualization mechanisms: "For that purpose, I introduced features for control flow graphs and polymetric visualization, implemented as part of an extensible framework that will facilitate the future development of new visualization paradigms (treemaps, class/package dependency, dynamic, evolution etc)." Developers have already expressed excitement over these technologies, which visualize the design of an application, making it easier to keep a high level overview of how complex applications work.

<a href="http://dot.kde.org/sites/dot.kde.org/files/sandroandrade.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/sandroandrade_1.png"></a>

Sascha Peilicke was working on SyncML support for Akonadi (KDEPIM) during this summer to make it possible to sync with mobile devices for example. More details can be found in <a href="http://saschpe.wordpress.com">his blog</a>.

Rob Scheepmaker implemented the ability to publish/access plasma widgets over the network: "Widgets can be announced on the local network through zeroconf and when accessing them, the entire plasma package will be send over in case of scripted widgets. All data engines and services will act on the remote machine instead of the local machine completely transparent to the widget. You can now for example access a published nowplaying widget to remotely control Amarok from another computer without any change in the nowplaying widget, or any of the data engines/services. Of course security plays an important part in this project: all messages are signed, and the shell can decide which machines are allowed access to certain services, use the bluetooth like password pairing mechanism (enter the same password on both machines), or simply not allow remote access at all."

<a href="http://dot.kde.org/sites/dot.kde.org/files/robscheepmaker.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/robscheepmaker_1.png"></a>

Szymon Tomasz Stefanek joined the Akonadi crowd to implement a filtering framework. The project focused on creating the new filtering system for the Akonadi platform. Filtering is what you actually use to discard spam, move the messages from your boss to a special folder, play a nice sound when a mail from your fiancée comes in etc. Most of the work is at API level and only the top is visible in the UI. A screenshot of what is probably going to be the next KMail advanced filter editor:

<a href="http://dot.kde.org/sites/dot.kde.org/files/szymonstefanek.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/szymonstefanek_1.png"></a>

Sven Krohlas implemented a framework for unit tests in Amarok including first tests. This will help us identify problems, especially regressions, early and hopefully in time for releases. In time, Amarok will become a more stable application, and it also eases development of features as it warns the developer in advance of regressions his code introduces.

Téo Mrnjavac, who already worked with the Amarok team as part of Season of KDE last summer, refactored much of the playlist backend code, implemented multilevel playlist sorting, implemented a nice breadcrumb interface for multilevel playlist sorting and shuffle, implemented configurable grouping and made all this easily accessible through Amarok URL bookmarks. Users will be happy to see those much requested features back in Amarok 2.2.

<a href="http://dot.kde.org/sites/dot.kde.org/files/teomrnjavac.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/teomrnjavac_1.png"></a>

Vyacheslav Tokarev had to concentrate on fixing things first in his project WYSIWYG (What You See Is What You Get) support in khtml: "I was able to improve caret positioning code, selection, edit commands. It's very technical and I had to do a lot of debugging to find the correct fixes. From now (in my khtml branch) WYSIWYG editors (with implemented commands) mostly work well and very importantly without crashes. Though I didn't end up implementing all the missing commands (only 2). It's important too to have the rich text editing features, but I had to put all my efforts on the stabilization and the improvement of the existing framework. (e.g. refactoring the code). As for the future plans I'm gonna merge editing branch to the trunk in upcoming days and will work on that later." Many websites use WYSIWYG editors, and thanks to the work of Vyacheslav these will work in newer versions of Konqueror.

Zeng Huan's main target was making a landscape object for Kolf2 (backdrop image for 2d view and an object for 3d view). He also created a texture blender and heightmap editor, these two tools are both used for editing the landscape object. Kolf2, the successor of the popular golf simulator in KDE 3, will thus be an even better application than its predecessor!

<a href="http://dot.kde.org/sites/dot.kde.org/files/zenghuan.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/zenghuan_1.png"></a>


Adam Kidder worked on a nice UI for searching for your files with the nepomuksearch:/ kioslave. He is working on getting it integrated into Dolphin for KDE 4.4. Finding the files you want is going to get a lot easier. You can read about his progress in <a href="http://www.thekidder.com/">his blog</a>.

<a href="http://www.gigabytes.it">Nicola Gigante</a>'s work has focused on the integration of the new authorization library with the rest of KDE. First of all, two new classes have been added to KDEUI, that automatically handles authorization. If you need to execute a privileged action when the user clicks on a button you can use those. The next step was the integration of the authorization library with the KDE configuration modules API. "Specifically, I’ve added some trivial APIs to the KCModule class to allow a module to declare that its save() method requires authorization to be executed. This way, System Settings and kcmshell can now handle authorization of the module’s save action."

<a href="http://realate.blogspot.com">Andrew Manson</a> worked with the Marble crew to implement a nice Open Street Map annotation framework that makes the life of mappers all around the world easier with Marble.

<a href="http://tiagosh.wordpress.com">Thiago Herrmann</a> improved the MSN support in Kopete by adding various features like ink support and support for voice clips, proxies and emoticons. He also improved the contact list. He merged his work into trunk already so MSN users can look forward to nice improvements in KDE 4.4.

<a href="http://www.jmata.co.cc">Jorge de la Vega</a> joined the kwin folks to work on window tabbing: "The windows can be grouped and the decoration chooses how to display the contents of the group, it can be as tabs or with a list or any widget the decoration decides. In a group the windows can be switched by a keyboard shortcut or by the decoration like clicking in a tab. There are also some options to manage the group in the window pop up menu. I've implemented tabbing in Oxygen and it allows to drag the tabs between groups, or to ungroup a window by dragging the tab somewhere else, changing the order of the tabs. I also wrote a decoration for testing it and I'm writing a simple decoration." Be prepared for some more kwin goodies once this is polished and merged into trunk.

<b>Conclusion</b>

As the above overview shows, the students have done an incredible job this year. The students have gained valuable experience in working online with a great community, improving their coding skills and having fun. We, as a community, gained valuable contributions in terms of code - but more importantly, we welcomed new friends. We hope the students had as much fun as we had working with them, and maybe in time it will be them guiding new students in the welcoming arms of our great community!

Thanks of course also go to Google for making this possible once again.