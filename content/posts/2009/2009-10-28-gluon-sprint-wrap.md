---
title: "Gluon Sprint Wrap-Up"
date:    2009-10-28
authors:
  - "leinir"
slug:    gluon-sprint-wrap
comments:
  - subject: "Excited"
    date: 2009-10-28
    body: "I'm super excited about this project, and I hope it doesn't fizzle. Unity3D is a fantastic product, but not open source. I've wanted to make an open source game for a while using something like it, but never found anything suitable.\r\n\r\nHopefully we can do python scripting in Gluon... Wheee!"
    author: "troy"
  - subject: "We're doing everything we can"
    date: 2009-10-28
    body: "We're doing everything we can to avoid it fizzling :) As for python scripting, well, we're trying to figure out which scripting engine(s) to use :) Trick is, we can experiment with it without actually having to worry about impacting the rest, because of the way the whole system's built :) The way scripting is done is by creating a Component which takes care of interpreting scripts, which exist as Assets in the project. Best part is: Since your gamelogic sits in these interpreted scripts, there's no compiling or anything to get them to run on different platforms, once the player runs there... :)"
    author: "leinir"
  - subject: "What kind of tools to use gluon?"
    date: 2009-10-29
    body: "Gluon seems very cool.\r\n\r\nDo you plan to also write tools for designer (like the tools of Adobe for Flash) or the whole interface has to be created in code?"
    author: "Ikipou"
  - subject: "Yay! Scripting bindings? "
    date: 2009-10-30
    body: "Gluon sounds great! \r\n\r\nDo you plan to include bindings to scripting languages? \r\n(I ask, because I wrote a fullfledged RPG rules-backend library in Python - currently mostly used for small commandline games - and Gluon sounds like a good way to publish graphical games developed with it)"
    author: "ArneBab"
  - subject: "Well... i kind of thought"
    date: 2009-10-30
    body: "Well... i kind of thought that much was obvious from the vision document ;) We do indeed plan on (and are working on) a tool, named Gluon Creator :)"
    author: "leinir"
  - subject: "All game logic will be"
    date: 2009-11-03
    body: "All game logic will be written using one or another scripting language - either through Kross or through QtScript :) If you want to help with this, i know leifeld is happy to get any help :)"
    author: "leinir"
---
 
On October 9 through 11, a dozen developers gathered in the Nokia Offices in Munich to bring the vision outlined in <a href="http://amarok.kde.org/blog/archives/1098-The-Future-of-Game-Development-in-KDE.html">Dan Leinir's blog about the "Future of Game Development in KDE"</a> to life. After three days of hard work, the developers joined hundreds of other Qt developers for the Qt Developer Days 2009 (thanks to free tickets provided by Nokia).

The purpose of this article is two-fold; first and foremost it is to describe the happenings at and results of the Gluon developer sprint. Secondly, however, it also aims to explain in further depth what Gluon actually is, so the casual observer is not left out. In the age-old tradition of defining something before using it, the second part shall happen first.

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:500px;">
<a href="http://dot.kde.org/sites/dot.kde.org/files/group.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/group_0.jpg" width="500" height="287" /></a>
Gluon Sprint attendees
</div>
<h2>Gluon</h2>

Many have been asking "So, what exactly is this Gluon thing?" Well, that's really where it becomes a little complicated. Gluon is not a single piece of software, but rather a collection of software and a community to go with it.

<div style="clear:both; padding: 1ex; border: none; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:500px;">
<a href="http://dot.kde.org/sites/dot.kde.org/files/Gluon-flow.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/Gluon-flow_1.png" width="500" height="154" /></a></div>

The info-graphic above shows how Gluon is a way for game creators to create games, distribute them to their players and get feedback on their game directly - all using a single set of tools. The game developer comes up with the idea, which they then use Gluon Creator to develop into a game. From Gluon Creator the game is published to the world, through OpenDesktop.org, and Gluon Player then downloads those games. The players of the games can then talk with each other about the games, and give feedback to the game developers about the games they create.

The software itself is thus separated into three distinct parts:
<ul>
<li>A set of libraries (currently <a href="http://gluon.tuxfamily.org/wiki/index.php?title=The_Gluon_Vision#The_Libraries">KGL, KAL and KCL</a>, probably with more to come)
<li>Gluon Creator
<li>Gluon Client
</ul>

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/gluonrocks.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/gluonrocks_0.png" width="320" height="240" /></a><br />
Gluon rocks
</div>
The libraries contain common functionality that can be used by Gluon applications, exposing for example the audio playing capabilities of OpenAL in a Qt-friendly library, KAL.

Gluon Creator is the tool inside which games are created. It is a way of putting together building blocks that make up the game, named GameObjects and Components, in an efficient, easy and consistent manner. Furthermore it allows for easy distribution of the games to potential players through the next generation of GHNS publishing technologies, namely the FreeDesktop.org specification <a href="http://www.freedesktop.org/wiki/Specifications/open-collaboration-services">Open Collaboration Services</a>, of which <a href="http://opendesktop.org/">OpenDesktop.org</a> is an implementation.

When dealing with Free Software the community is very important. Without the community a project is really just a potentially interesting academic exercise. In comparison Gluon Client is not just a way of consuming the games (which are downloaded via the OCS system). It allows gamers to report back to the creators and provide feedback through ratings, donations, comments and so on. Gluon Client also allows players to interact directly with one another by allowing them to chat, submit high scores and share screenshots as well as other game-specific content.

<h3>History</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/gluon-active.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/gluon-active_0.jpg" width="320" height="200" /></a><br />
Gluon activity: 9th place on Gitorious.net during the sprint</div>
Gluon was conceived when the project's creator, Sacha Schutz, looked around the internet and saw how popular casual games based on Flash were. He saw the need for something which would make it possible to create similar games in a simple manner using technologies unrestricted by the closed world of proprietary software.

He worked on it more or less alone for just under a year until going public with the idea. He did not want to go out to the world with something which looked like vapourware; rather he wanted to have something to show first. When he finally showed off the work he had done to a small group of people at the Gran Canaria Desktop Summit, what he had were the libraries and some examples showing off the possibilities of those. These possibilities included the ability to handle input in a manner unprecedented in Qt software through KCL, a simple way of managing discrete objects of data in an OpenGL viewport through KGL, and a way of handling sound playback through KAL. What he lacked at the time was a more coherent vision for the project.

This is where the enthusiasm shown by Sacha inspired others from the community. They decided the project seemed sound, and that Sacha's initial project vision of creating a Flash-killer designed for creating the style of games he had seen was an interesting point of departure for a cool new Free Software project.

After this first presentation a vision slowly started to take shape, and finally during August, things started coming together as the team began work on a more game-centric set of libraries and concepts: the GameObject Hierarchy and the Gluon Game Project concept. The next step, then, was the developer sprint...

<h2>The Sprint</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/office.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/office_0.jpg" width="320" height="240" /></a><br />
A pleasant working environment stimulated progress
</div>

The Gluon developers' sprint came about as the result of an idea of Knut Yrvin, community manager at Qt Frameworks Division at Nokia. During GCDS he was approached by yours truly regarding the community tickets for Qt Developer Days in previous years. He suggested Nokia could host a sprint leading up to the conference, and asked that a purpose be defined for the sprint. So, on the 8th of September, the first announcement of the sprint was launched, by way of a blog entry with the somewhat provocative and desperately hype-tainted title <a href="http://amarok.kde.org/blog/archives/1098-The-Future-of-Game-Development-in-KDE.html">The Future of Game Development in KDE</a>.

This gathered some amount of interest from a good selection of community members - those interested in using the finished tools, but also those interested in creating those tools. After only two weeks, twelve people were found who wanted (and had time) to take a week out of their calendar in the start of October and attend both the sprint and the conference.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/presentation.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/presentation_0.jpg" width="320" height="240" /></a><br />
Sharing ideas at the sprint
</div>
So, on the 9th - 11th of October, the participants converged at the Nokia offices in Munich, ready to work on the future of game development in KDE as the blog entry had indicated. During the weekend, a number of presentations were held, describing other tools that each of the participants had experience with (such as Unity3D, VirTools, Blender Game Engine and others). Many an active discussion was held, on topics such as input handling and sound management, as well as 2D versus 3D. Much brainstorming was conducted on topics ranging from the Game Project concept to the distribution model to the user interface of the Gluon Creator tool.

Last but not least, there was much socializing. This is a vital part of any developer sprint, as it means that the participants get to know each other in a way that is not possible simply using the normal channels of communication such as IRC and mailing lists. Most of you reading this article will be aware of what problems that can occur in those channels - most prevalent of course the misunderstandings brought on by the lack of emotion and body language in text-only media.

<h3>Tangible Achievements</h3>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/smnallgroup.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/smnallgroup_0.jpg" width="320" height="240" /></a><br />
Discussing Gluon
</div>
However, during the sprint not only these arguably somewhat fluffy goals were reached. More tangible results also appeared. After returning home from the sprint, the participants sent emails to yours truly with information on what they had conceived over the course of the sprint, both in terms of code  and otherwise. Below is a summary of these.

<strong>Luciano &quot;mikelima&quot; Montanaro</strong> highlighted that other than that he &quot;learned from you all about the Gluon components and the high profile objective that is to be achieved&quot; he had &quot;dusted up my svgoptimizer program&quot; and that he was &quot;adding features to it.&quot; Furthermore he hopes that &quot;it could be useful in the Gluon Creator, as part of the final packaging steps -- or to KDE in general, since SVG graphics is used in so many places.&quot;

<strong>Sacha "DrIDK" Schutz</strong> was more direct about his coding achievements:

<blockquote>
What I have done:

New class KGLPainter which help you to draw openGL primitif using VertexBuffer Array. For example you can code like this:

<pre>
void QGLWidget::paintGL()
{
KGLPainter painter;
painter.setColor(Qt::red);
painter.setMode(GL_FILL);
painter.drawRect(rect);
}
</pre>
</blockquote>
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/blok.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/blok_0.png" width="320" height="178" /></a><br />
The Blok game in action
</div>
<blockquote>
New class to add text as Item by generating an openGL texture: KGLTextItem * text = new KGLTextItem("gluon rocks")

Improve KGLFx: Now we have 5 shaders Fx:
<ul>
<li>KGLBlurFx
<li>KGLMosaicFx
<li>KGLPosterizeFx
<li>KGLLightFx
<li>KGLGrayScaleFx.
</ul>

But We are waiting for QGraphicsEffects to have a deeper look at it.

Improve KGLView: Sandroandrade and me have worked on the KGLView and camera. I tried to replace KGLView by KGLLib::GLWidget and use the rivo library to use QGLProxyWidget. And it works great!!
</blockquote>
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/blokeditor.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/blokeditor_0.png" width="320" height="178" /></a><br />
The Blok editor
</div>

The same went for <strong>Sandro Andrade</strong>, who wrote the following:

<blockquote>
Implemented features during Gluon Sprint:

<ul>
<li>Enhancements in KGLView: openGL camera parameters were configured in order to meet screen resolutions. This allows for really going fullscreen, in contrast to the squared GLView previously used in Gluon. Fixed a bug in mapping from view to scene by using OpenGL's gluUnProject() function.
<li>Enhancements in Blok Editor: grid functionalities were implemented, levels were reconstructed, and level files now include background and ground textures.
<li>Enhancements in Blok: use of new level files. Fixed a bug in sound playing.
<li>Initial work for embedded widget support in KGLView: Using a QGraphicsView with a QGLWidget as viewport allows for the use of embedded widget for displaying score, lifes, and etc.
</ul>

Current work:

<ul>
<li>Enhancements in Blok Editor to include physics and sound properties.
<li>Refactoring of KGLView to ensure low coupling and separated concerns.
</ul>
</blockquote>

<strong>Kim Jung Nissen</strong> and <strong>Morten Justesen</strong> both worked hard most of the weekend on heavily re-factoring the KCL library which, until that point, had been tied too closely to the X11 and EVDEV based implementation. First work happened on the whiteboard and later it moved onto the computer, where KCL was ripped apart and slowly put back together again. This process is still on-going, but is looking to reach a conclusion very soon (potentially around the same time as this article is published).

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/gluon-creator-begins.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/gluon-creator-begins_0.jpeg" width="320" height="208" /></a><br />
Beginnings of the Gluon Creator
</div>

<strong>Dirk Leifeld</strong> spent most of his time hacking away at QtScript and Kross, the two scripting systems currently being investigated for use for the scripted components in Gluon. This work will allow programmers to extend games created in Gluon with custom scripted code. The reason for this happening in this manner is that Gluon, unlike many other game development frameworks, is designed to run interpreted rather than compiled. Only the components supplied with the framework itself are compiled code, while the game logic provided by the game writers is written in scripting languages that are interpreted at runtime. The reason for this is simple: Publishing a game becomes simply the distribution of a game project. When you look at the infographic at the top of the article here, it becomes apparent just why this is important.

<strong>Arjen Hiemstra</strong> and <strong>Dan Leinir Turthra Jensen</strong> worked on the Gluon Definition Language and the parser for it. They furthermore made a start on the Gluon Creator, which began its life at the sprint as an example application for testing out the GameObject Hierarchy model that Arjen wrote. See also <a href="http://blog.heimr.nl/node/3">his blog entry</a> about the progress.

Our thirteenth participant, <strong>Harald Fernengel</strong>, who graciously offered to assist us with finding hacking space for everyone, decided to also take part in the sprint. This resulted in a lot of work from him on getting Blok to run on MacOS X. Nearing the end of Sunday, he had the game running. Without sound, mind you, but the game was running. Brilliant achievement there!

<h2>Conclusion</h2>

All in all, the Gluon Developer Sprint was a huge success, which boosted the already active development of Gluon into a state of overdrive: during the weekend the Gluon team ended up in the top ten most active teams on gitorious.net. On top of this, all the participants reaped the benefits of a developer sprint as mentioned above. As Luciano puts it: &quot;Finally, I have to say I really had a great time with the Gluon team, and it's been a pleasure to be with you during the event. And the Trolls have been great hosts, of course.&quot;

It was great to see the interest that Nokia put in us on another level - during Sunday we had a visit by <strong>Daniel Kihlberg</strong>, who is the head of Qt Service, Marketing and Sales at Nokia, who put many interesting questions in front of us about non-technical matters. This highlighted the need for Gluon to have a proper, solid vision which is actually written down. As a direct result of his visit, a document was started on the Gluon wiki named <a href="http://gluon.tuxfamily.org/wiki/index.php?title=The_Gluon_Vision">The Gluon Vision</a>, in which is described on a higher, non-technical level what Gluon actually is. While this document is not yet completed, it is a good step on the way to something solid we can show to the world.

On a personal note, I must echo Luciano's sentiment: the Trolls at Nokia did an amazing job in hosting this sprint for us, and many thanks must go to both them and the KDE e.V. for sponsorship. It would not have been possible without you.