---
title: "KDE Community Invited to FOSS Nigeria 2010"
date:    2009-12-27
authors:
  - "jospoortvliet"
slug:    kde-community-invited-foss-nigeria-2010
comments:
  - subject: "KDE Developers Should Go"
    date: 2009-12-27
    body: "If you are an internationally minded KDE developer I highly recommend you go.  You'll need to be prepared to give talks about free software (ranging from general introductions to highly technical).  In return you'll get great hosting, great food and a fun adventure.  e.V. should be able to help with the travel costs.\r\n"
    author: "jriddell"
  - subject: "Bad Typo!"
    date: 2009-12-27
    body: "\"Kano will threat you well\" should be \"treat you well\""
    author: "odysseus"
  - subject: "Aye, bad typo"
    date: 2009-12-27
    body: "That is a little unfortunate one right now, yeah. Let me echo JRiddell's comments: Kano was a wonderful place last year, very friendly and happy, and I'm looking forward to going there again."
    author: "adridg"
  - subject: "whoops. Fixed it..."
    date: 2009-12-27
    body: "whoops. Fixed it..."
    author: "jospoortvliet"
  - subject: "the image says march 2009 instead of 2010"
    date: 2009-12-28
    body: "the image-flier says march 2009 instead of 2010, please fix it"
    author: "slashdevdsp"
  - subject: "More Promotion and awareness"
    date: 2009-12-28
    body: "KDE and Linux/open source should be prometed on a much wider scope in not-so-developed countries (I hate the word Third World) like those in Africa, South America and the Caribbean if growth is the goal of the open source community.\r\n\r\nThe most developed countries seem to be clinging to Microsoft and it's \"products\" for better or worse so we have to foster these other countries that are not in the position to feed the cash cow and where a lot of people don't even know what a computer really is.\r\nThe best people to introduce and teach Linux and open source are those who are not already contaminaed by MS, who have open minds.\r\nThe countries that I mentioned are oases when it comes to such people. Fertile soil for the growth of Linux and Open Source Software so let's expand our wings and spread the good news :) \r\nI am sure that Nigeria will welcome you guys with open arms and that you will enjoy one of the best hospitality available. "
    author: "Bobby"
  - subject: "I very much agree.  Having"
    date: 2009-12-28
    body: "I very much agree.  Having lived for a period in South Africa I'm very aware of the impact FOSS and IT in general could have on the lives of people by empowering them and opening opportunities previously denied.  I'd love to go to the conference, but I doubt I could teach enough to justify the expense.  If anyone does plan to go, please do think hard about what you can contribute, don't just view it as a junket."
    author: "odysseus"
  - subject: "Looks like the banner has"
    date: 2009-12-29
    body: "Looks like the banner has been taken from the 2009 promo material. The 2009 dates given there differ greatly from the 2010 dates mentioned in the article."
    author: "majewsky"
  - subject: "fixed"
    date: 2009-12-30
    body: "fixed"
    author: "jriddell"
  - subject: "Deleted comments"
    date: 2009-12-30
    body: "I've deleted a couple of comments which make fun of 419 scams.  This is a common prejudice about Nigeria (with good reason) the prevalence of which is off-putting for people who might otherwise go to this conference.  It is something we are trying to overcome with this event.\r\n"
    author: "jriddell"
  - subject: "Our Appreciation"
    date: 2010-01-14
    body: "Hi all,\r\n\r\nI am very happy with response i have recieved so far from the  people that lived outside my country (Nigeria). Especially those that really wanted to attend our second international conference on Free and Open Source software in Nigeria.\r\n\r\nWe are ready to host many people from FOSS Community across the globe.\r\n\r\nWe do appreciate many response and assistance from all of you guys.\r\n\r\nPlease,if you have any question regarding the conference don't hesitate to contact us.\r\n\r\n\r\nThanks.\r\n\r\nMustapha Abubakar,\r\nOrganiser-FOSS Nigeria 2010 "
    author: "mabubakar"
  - subject: "My country Nigeria"
    date: 2010-01-14
    body: "Thanks J,the issue of scam is one of our major problem when we try to tell people that they can come and join us in our FOSS campaign in Nigeria.\r\n\r\nBut people are now understanding our situation and we hope many poeple will help us in spreading the FOSS ideology in Nigeria.\r\n\r\n"
    author: "mabubakar"
  - subject: "Your Participation will Count"
    date: 2010-01-14
    body: "Hi odysseus,\r\n\r\nDo not ever under estimate the impact of knowledge that you deliver at the conference,i  diffidently knew that both Adrian and Jonathan that have attended the last year's conference have change many people regarding the free and open source software.\r\n\r\nImaging addressing 500 participants in one hall,how many people do you think will benefit and from there you have change them.\r\n\r\nI think coming to the conference is worth spending for those that can afford or can have a sponsorship.\r\n\r\nAs part of our contribution and support for our international invited guest, we will give cover the expenses of accommodation,feeding,transport and any other request that will make our guess feel ease and happy during his/there stay in Nigeria.\r\n\r\nPlease fell free to join us in our conference, your participation will count.\r\n"
    author: "mabubakar"
---
A few days ago The Dot received an invite for the KDE community to FOSS Nigeria. FOSS Nigeria 2010 will be the second Free Software conference in Nigeria, following the successful event last year (<a href="http://dot.kde.org/2009/03/10/first-free-software-conference-held-nigeria">as reported on The Dot</a>). Again, Free Software developers and community members from around the world, but of course especially those from Africa and Nigeria, are invited for a 3-day conference in Kano at the Bayero University Kano.

<img src="http://dot.kde.org/sites/dot.kde.org/files/FOSSBanner.jpg" width="569" height="108" alt="FOSS Nigeria Logo" />
<!--break-->
<i>Dear All,</i>

<i><a href="http://fossnigeria.org">Free and Open Source Software Foundation Nigeria</a> in collaboration with The Centre for Information Technology at <a href="http://www.buk.edu.ng/">Bayero University Kano</a> and <a href="http://hutsoftnigeria.com">HUTSOFT Nigeria Limited</a> are officially inviting all KDE contributors and users including organisations and companies who want to come and give talks at our second international conference on Free and Open Source Software Nigeria 2010 (FOSS Nigeria 2010 ).</i> 

<i>The conference is scheduled to take place from 5th to 7th March 2010 at Mambayya House, Centre for Democratic Research, Bayero University Kano, Kano State, Nigeria.</i> 

<i>Conference registration will be open on 1st January 2010 through the official <a href="http://fossnigeria.org">FOSS Nigeria website</a>.</i> 

<i>The call for papers is now open and will accepting talks until 20th February 2010.</i> 

<i>Conference papers may cover all aspects of free and open source software projects. </i> 

<i>For paper submissions, request for invitation letters or any other enquiry please write to the following e-mail addresses: conferen<span>ces@foss</span>nigeria.org, mu<span>stapha@foss</span>nigeria.org  and iadas<span>uma@foss</span>nigeria.org </i> 

<i>All interested companies and organisations who wish to donate or advertise their open source products and services can send their banners, pamphlets, stickers, shirts or anything that can sell the idea to our community free of charge so that it can be distributed during the conference. We are still looking for donations to help us improve this conference further.</i> 

<i>All foreign invited attendees can attend the conference free of charge. In addition, as last year, food, accommodation throughout the stay in Nigeria and sight seeing will be sponsored by the organisation as well.</i> 

<i>Last year we received reports of people who would have wished to attend the conference but were unsure about visiting Nigeria. We understand there are many negative stories about our country, but we can assure you Kano will treat you well.</i> 

<i>Adriaan de Groot (Vice President KDE e.V) and Jonathan Riddell (Kubuntu Developer) are two KDE community members who attended the FOSS Nigeria 2009 conference in March 2009. They can answer any question that you may have regarding the country and the conference, feel free to contact them.</i> 

<i>FOSS Nigeria 2009 Conference communique and reports can be found <a href="http://fossnigeria.org/node/5">on our website</a>.</i>

<i>FOSS Nigeria 2009 Pictures can be found on <a href="http://www.flickr.com/photos/jriddell/sets/72157614991241443/">the Flickr account from Jonathan Riddell</a>

<i>Hope to see you all at FOSS Nigeria 2010.</i>

<i>Mustapha Abubakar,
Organiser-FOSS Nigeria 2010.</i>