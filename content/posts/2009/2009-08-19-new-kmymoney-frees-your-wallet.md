---
title: "New KMyMoney Frees Your Wallet"
date:    2009-08-19
authors:
  - "asoliverez"
slug:    new-kmymoney-frees-your-wallet
comments:
  - subject: "Wow!!"
    date: 2009-08-20
    body: "kmymoney sure looks hot!!\r\n\r\nCan't wait until packman has the 1.0 version online, so i can finally do my personal accounting on my computer again :)\r\n"
    author: "whatever noticed"
  - subject: "Horrible theme"
    date: 2009-08-20
    body: "KMyMoney is great, but that screenshot is awfully UGLY! It's disgusting. Why are you using theme like from 1999? It's 2009 for God's sake! Use Oxygen for screenshots (or at least Bespin or some other modern styles)."
    author: "mikmik"
  - subject: "Oh, sorry, my bad! I didn't"
    date: 2009-08-20
    body: "Oh, sorry, my bad! I didn't read it till the end. It's still KDE 3.x version. Ok, then at least use default Plastik or even better Domino (or something similarly modern) for screenshots. Not this ugly Keramik (or what it is)."
    author: "mikmik"
  - subject: "Default KDE look used"
    date: 2009-08-20
    body: "We used the default look for the screenshot, to keep consistent.\r\nIf you want to see KMM in a modern look, you can check my blog, where I have screenshots using the Tragedy Plasma in KDE4.\r\n"
    author: "asoliverez"
  - subject: "It's not default look"
    date: 2009-08-21
    body: "It's not default look. Default look in KDE 3.5.x is Plastik!"
    author: "mikmik"
  - subject: "\"the nascent KDE Finance group\""
    date: 2009-08-21
    body: "Great to see this software field being covered by KDE applications. I'd like to use a real financing application for observing my personal account balance, incomes and expenses (this data is currently hosted in a huge OOo table).\r\n\r\nHowever, I could not invest any time yet to check all these applications in detail (i.e., KMyMoney, Skrooge and Kraft). What is the difference between them, and what is best for my use case (see above)?"
    author: "majewsky"
  - subject: "Strange wording"
    date: 2009-08-21
    body: "\"The support of PGP encryption for the KMyMoney files has been improved [...] so no one can access your financial records.\"\r\n\r\nWrite-only memory? ;-)"
    author: "majewsky"
  - subject: "My bad. It should say \"no one"
    date: 2009-08-21
    body: "My bad. It should say \"no one else\". But hey, write-only memory is safe, even if not practical. =)"
    author: "asoliverez"
  - subject: "opensuse 11.1 packages"
    date: 2009-08-21
    body: "I took packman's spec file and made this for opensuse 11.1:\r\nhttp://software.opensuse.org/ymp/home:eean/openSUSE_11.1/kmymoney.ymp\r\nand for 11.0:\r\nhttp://software.opensuse.org/ymp/home:eean/openSUSE_11.0/kmymoney.ymp\r\n\r\nWatch the packman repo for when they update though... these packages work fine but its probably better to get some more professional packages when they're available. :)"
    author: "eean"
  - subject: "Congratulations"
    date: 2009-08-23
    body: "Great work KMyMoney team!  I use your software already, and I too consider it a very important piece of KDE.  Thanks!"
    author: "Jerzy Bischoff"
  - subject: "I wish it were for KDE4"
    date: 2009-08-24
    body: "I really appreciate the effort done by the developers and volunteers. I just feel sorry that it took them longer than planned to reach the release status. The situation now is that we have a KDE3 application at the same time that KDE4 has reached, with 4.3, a de-facto status for KDE users.\r\n\r\nI have settled on KDE4 and I'm not looking back. I'd like to use KMyMoney, but having to pull all KDE3 libs is as unattractive as mixing with gnome applications.\r\n\r\nI really hope that the team manages to get a KDE4 port out soon. The application looks great and I'm sure that once the port is out, a lot more people will be willing to give it a spin.\r\n\r\nOne final question: what's with the numbering convention dude? It's called kmymoney \"2\", but then version 1? I find this rather confusing.\r\n\r\nAnyway, thanks for the effort put into this release and looking forward for the KDE4 port.\r\n\r\nBatistuta"
    author: "batistuta"
  - subject: "KMyMoney or Skrooge"
    date: 2009-08-25
    body: "Kraft is not what you're looking for, it is for generating invoices. KMyMoney or Skrooge are designed for your use case, so one of them should fit your needs. I won't advise, though, I'm obviously biaised ;-)\r\n\r\nGuillaume"
    author: "gdebure"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/kmm-snap0.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kmm-snap0t.png" /></a>
KMyMoney 1.0</div>The <a href="http://kmymoney2.sourceforge.net">KMyMoney</a> development team is pleased to announce a major step forward for what has been described as "the BEST personal finance manager for FREE users". KMyMoney 1.0 has been released. With over 3 years of development, this new stable release has many new features and a refreshed user interface.

You don't know where your money is going? Trying to get a hold of your household budget? Have an investment portfolio and don't know how much money you earned or lost? You have money in many currencies and it's hard to count how much you actually have? KMyMoney can help you. KMyMoney strives to be the easiest open source personal finance manager to use, especially for the non-technical user. Based on tried and tested double entry accounting principles KMyMoney ensures that your finances are kept in correct order. Although many of the features can be found in other similar applications, KMyMoney strives to present an individual and unique view of your finances. Following the KDE's way of "sensible defaults with powerful configuration", KMyMoney offers a default configuration and templates to start managing your finances with minimum hassle, with the possibility to customize it to your liking.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/kmm-snap1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kmm-snap1t.png" /></a>
Keep track of where your money is going easily.
</div>
As a new user of KMyMoney you can start with entering your account information and keep track of your income and expenses, synchronize with your online banking and have an accurate view of your current finance situation. You can also keep an eye on your investments, if you have them, tracking their price through online quotes. As you feel more comfortable, and ready to get a grip of your finances, you can create a budget, compare it to your actual expenses, and even try to get a peek into the future with the forecast feature. All this and more can be done with this new version of KMyMoney.

Since our latest stable release, 0.8.9, a lot of effort has been put in by the developers and the community to add new features and test them to ensure a rock-solid release. Over 2 years of development have resulted in the addition of budgets, a forecast feature, many new reports, report charts, a complete redesign of the import feature, which allows for a much easier migration from other application and a swifter synchronization with online banking. The support of PGP encryption for the KMyMoney files has been improved too, including the option to have multiple keys for a single file, so no one can access your financial records. The summary view has been revamped to show more and more useful information, allowing you to have an overview of your financial situation at a glance. Also, there are now translations for 22 languages, though not all of them are as complete as we would like. We have users wherever KDE3 is installed. That results not only in a greater quality application, but also in one that can be customized to fit the needs of a wide range of users. In between all that work, we have fixed a lot of bugs and little annoyances to make this the best KMyMoney release ever.

Concerning the future, the porting of KMyMoney to KDE 4 is already underway. Following our usual conservative approach, the first release for KDE4 will be very similar to this one, feature wise. Subsequent releases will take full advantage of the new platform. Also, the coordination work with other KDE4 financial applications under the umbrella of the nascent KDE Finance group opens the door to other exciting possibilities.

For more detailed information about the features included in this release, please check the <a href="http://kmymoney2.sourceforge.net/release-1.0.0.html">release notes</a>.