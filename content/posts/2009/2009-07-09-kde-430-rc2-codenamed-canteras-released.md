---
title: "KDE 4.3.0 RC2 Codenamed \"Canteras\" Released"
date:    2009-07-09
authors:
  - "sebas"
slug:    kde-430-rc2-codenamed-canteras-released
comments:
  - subject: "No comments anymore"
    date: 2009-07-09
    body: "Hello everybody,\r\n\r\nthere are rarely ever now any comments anymore to stories here and certainly not the large numbers of old days. I do miss that.\r\n\r\nCould you consider to allow anonoymous comments again. I believe most people just won't bother to create and account or to actually log in, esp. when there was nobody posting anything before.\r\n\r\nComments are part of a healthy community. Or do you expect that to now happen on another site?\r\n\r\nYours,\r\nKay"
    author: "kayonuk"
  - subject: "+"
    date: 2009-07-09
    body: "I agree. There were often really informative comments here and they are missing now"
    author: "mark"
  - subject: "Part of it is possibly that"
    date: 2009-07-09
    body: "Part of it is possibly that dot readership levels tend to fluctuate quite a bit, depending on what's going on, and until Akademy kicked off, new articles had almost dried up since the digest has been MIA. We're working on a few things, but personally (rather than officially), I'm not sure anonymous comments is the way to go (again|yet).\r\n\r\nThanks for the feedback though, and if things continue as they are, it might change in the future.\r\n\r\nCheers"
    author: "troy"
  - subject: "I actually agree. Just make"
    date: 2009-07-09
    body: "I actually agree. Just make the modding feature a registered users only feature and we are fine. The heated debates of 4.0 should be over anyway"
    author: "Asraniel"
  - subject: "Trolls"
    date: 2009-07-09
    body: "It's not about heated debates, as a heated debate are positive. The problem are the trolling, which unfortunately most of the KDE 4.0 debates drowned in. It's useless and only harmful for the community and demotivating for the developers.\r\n\r\nAnd we even got a good proof today that the problem has not gone away, just look at the troll in the comments for of the KDE e.V article.\r\n"
    author: "Morty"
  - subject: "Klipper regression"
    date: 2009-07-09
    body: "Hey KDE 4.3 looks really good. One regression from 4.2 to 4.3 which should be fixed is the problems concerning klipper \r\nhttps://bugs.kde.org/show_bug.cgi?id=194844"
    author: "mark"
  - subject: "moderation"
    date: 2009-07-09
    body: "Since comments are moderated, anonymous ones could start with a low score and therefore not visible by default. Quite a few forums work like that."
    author: "jadrian"
  - subject: "Raw changelog?"
    date: 2009-07-10
    body: "Where can I find it?\r\n\r\nI don't really want to run svn2cl on KDE svn servers."
    author: "birdie"
  - subject: "I fully support this idea.\nAs"
    date: 2009-07-10
    body: "I fully support this idea.\r\n\r\nAs for spammers, there's a recaptcha module for drupal which is used here."
    author: "birdie"
  - subject: "I do not need subject!"
    date: 2009-07-10
    body: "I don't understand why the parent comment has been buried ... but I'm gonna add more: I miss the old .kde\r\n\r\nIt was so beautiful and so dissimilar to everything else."
    author: "birdie"
  - subject: "No thanks!"
    date: 2009-07-10
    body: "The comments now are _much_ more meaningfull and usefull now.\r\n\r\nEarlier the dot was a depressing read with plenty of negative spam. I do not want to see this happening again."
    author: "Jo \u00d8iongen"
  - subject: "reCHAPTA is not working"
    date: 2009-07-10
    body: "Was just luck I found my logon credentials. Is a bit weird that one cannot contact the webadmin to tell him that the reCHAPTA is not working..."
    author: "Jo \u00d8iongen"
  - subject: "KDE 4.3 Looking Sweet"
    date: 2009-07-11
    body: "Im loving it. I know its an RC, but it is working great \"seems\" much more responsive on my older machine. Definitely digging the new KRunner. Keep up the awesome work KDE Devs!"
    author: "tomdavidson"
  - subject: "Just not as exciting anymore..."
    date: 2009-07-11
    body: "Hello Troy,\r\n\r\nyou see, there were release announcements of betas and release candidates that were barely getting any comments at all. The KOffice stories used to receive many encouraging comments, no more now.\r\n\r\nIn the past, you could bet any day, that the comments would be in the hundreds for even a minor KDE release. And I think it will fall even lower as more and more frequently, questions or suggestions are left unanswered.\r\n\r\nDo the article writers feel motivated when there is barely any feedback?\r\n\r\nYours,\r\nKay\r\n\r\nBTW: The digest is very missed surely, but I understand that nobody can sustain this for as long as it was. \r\n"
    author: "kayonuk"
  - subject: "Full Picture"
    date: 2009-07-11
    body: "Hello Jo,\r\n\r\nsee, the negative comments are part of the picture. The KDE 4.0 development has let down some people. Why should it be forbidden to discuss it. \r\n\r\nAnd it is not now. Here I say it: The ALT-F2 behaviour of KDE 4.2 was still crap, only 4.3 will be good. The developers have let substantial parts of the user community down. And then they didn't want to hear it for the extended time it took to fix it.\r\n\r\nYours,\r\nKay"
    author: "kayonuk"
  - subject: "Difference"
    date: 2009-07-11
    body: "It's a huge difference between negative comments and the trolling and negative spam plaguing the dot previously. And all that spam and trolling did not bring anything meaningful, only drowning the comments in negative spite and even personal insult of the developers.\r\n\r\nComments that are negative are still more than welcome as always, as long as they are meaningful and useful. The whole picture are wanted and well received, but useless spite and negativity are not. "
    author: "Morty"
  - subject: "Second RC?"
    date: 2009-07-11
    body: "According to the schedule for 4.3 a second release candidate was not planned. Have some critical bugs been found on the first release candidate?\r\n\r\nRegards,\r\n\r\nRub\u00e9n"
    author: "ruben3d"
  - subject: "No difference"
    date: 2009-07-12
    body: "You see, right now there are no positive and no negative comments on most of the stories. Just no comments.\r\n\r\nAnd about negativity, and insults, and so on: I comment on a site where the admins can move things below treshold and use that power wisely. People can post anonymously, but will find that if they step over boundaries, their post and whole threads will become invisible.\r\n\r\nI found that to work nicely. Comments are plenty, and people feel comfortable with the fact that sometimes comments are just not yet removed, but that if you post within the lines, you can have a free and inspiring exchange.\r\n\r\nYours,\r\nKay"
    author: "kayonuk"
  - subject: "Probably mostly the taskbar"
    date: 2009-07-12
    body: "Probably mostly the taskbar crash, there where so many bugreports that probably all bug triagers went crazy;)"
    author: "Asraniel"
  - subject: "I second this. Now that we"
    date: 2009-07-13
    body: "I second this. Now that we have a comment moderation system, the anonymous trolls that plagued the Dot before will be taken care of. I think a low barrier to comment is a good thing."
    author: "Haakon"
  - subject: "Thank you for the answer,"
    date: 2009-07-13
    body: "Thank you for the answer, Asraniel. Let's hope nothing goes wrong with this new release candidate so we can enjoy the final product at the end of this month!"
    author: "ruben3d"
  - subject: "There were flame wares and a"
    date: 2009-07-14
    body: "There were flame wars and a lot of harsh critics at times but I believe that all that helped the development of KDE 4 in some ways. If everybody was praising KDE 4 then I am sure that it wouldn't be as good as it it presently. \r\nI also agree to what you said that people should be able to have their say without having to get registered. "
    author: "Bobby"
  - subject: "Negative comments"
    date: 2009-07-14
    body: "Yes, negative comments are a part of the picture. As long as they are constructive. During and after the release of 4.0 there were so many negative comments from people refusing to see what 4.0 was and was meant to be. This opened the flood gates for trolling and I think all this was very counter productive and demotivating for all people involved with KDE. \r\n\r\nHaving users creating a logon account to comment will, in my view, improve this. Thus making the comments much more useful and consequently helping the further improvement of KDE much more."
    author: "Jo \u00d8iongen"
  - subject: "Mod me down for the truth"
    date: 2009-07-15
    body: "It's really sad to see that there are still so many norrow minded people who can't tolerate others for saying their opinion or even speaking the truth.\r\nI love KDE, it's the best Linux DE for me and surpasses even the comercial ones in some ways but KDE is (still) not perfect, otherwise it wouldn't be growing.\r\n\r\nKDE 4.3 is very good but it's still not where I personally would like it to be. Some things are better than I could ever expect but there are other things that still nerve me like KDM still not having any administrator mode - since 4.0. I would prefer a KDE 4 that is very fast and that everything works perfectly than one with a lot of new features and a few essential things still missing.\r\nI appreciate the developers effort and I admire their skills and hard work but these things are necessity, I know that I can work around these things but is that the way that it should be? "
    author: "Bobby"
  - subject: "In addition to capcha"
    date: 2009-07-20
    body: "A friend of mine, who was plagued by SPAM bots in his forum comments, came up with a very simple way to defeat most bots. All he did was include a hidden field in his forms with a known name and known value (like; hidden field named the_answer and pre-set value=42). Then, when someone posts he checks if A) the field is present and B) whether it has the correct pre-set value.\r\nThis is more effective than you would think. Most SPAM bots (as in almost all) do not submit hidden fields in forms and when they do they often just leave them empty.\r\nSo sure, use a capcha, but also include a hidden field and just dump the post in the bit bucket if the hidden field is not set or does not contain the expected value... It's really quite effective...\r\n"
    author: "juhl"
---
The KDE Team has <a href="http://kde.org/announcements/announce-4.3-rc2.php">released</a> another release candidate for KDE called <em>"Canteras"</em>. It contains only few changes compared to RC1 which suggests that the 4.3 is stabilizing and shaping up well for the 4.3.0 release on 28th of July. KDE 4.3.0 will be followed up by a series of monthly bugfix and translation updates. Testers are asked to <a href="http://bugs.kde.org">report</a> bugs in this release so 4.3.0 becomes a release as smooth as possible. <br />

KDE's Release Team has already opened KDE SVN's trunk for development to allow for new features to be developed during the currently ongoing <a href="http://www.grancanariadesktopsummit.org">Gran Canaria Desktop Summit</a>. Fixes will be backported to the KDE 4.3 branch to stabilize the desktop further. After KDE 4.3, the next release, KDE 4.4 will be released in January 2010.
<!--break-->
