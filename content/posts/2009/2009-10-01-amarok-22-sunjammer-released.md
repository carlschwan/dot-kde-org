---
title: "Amarok 2.2 \"Sunjammer\" released"
date:    2009-10-01
authors:
  - "nightrose"
slug:    amarok-22-sunjammer-released
comments:
  - subject: "Audio CD!"
    date: 2009-10-01
    body: "I especially like the new Audio CD playing functionality. Thanks for that!"
    author: "sebas"
  - subject: "Just for you"
    date: 2009-10-01
    body: "We added that just for you ;-)"
    author: "nightrose"
  - subject: "Not just that, but it only"
    date: 2009-10-01
    body: "Not just that, but it only works in Sebas's computer. Talk about true devotion!\r\n\r\n\r\nPS:\r\nIf you want the feature too, use the secret keyboard code CTRL-X, then S-E-B-A-S.\r\n"
    author: "Mark Kretschmann"
  - subject: "That's ok, sebas is probably"
    date: 2009-10-01
    body: "That's ok, sebas is probably the only person left who still buy audio CDs xD"
    author: "patcito"
  - subject: "hahahahahaaah, my opinion"
    date: 2009-10-02
    body: "hahahahahaaah, my opinion it's the same.\r\n\r\nyet exist person that buy audio CD, haahah"
    author: "serroba"
  - subject: "Sunjammer"
    date: 2009-10-02
    body: "It's good to see the update. The external mysql database feature is awesome. Going to try it right now. <em class=\"timeEntry_control\"><a href=\"http://www.handyblog.org\" id=\"clean-url\" class=\"install\">Online-Casino</a></em>"
    author: "Benjamin Barnett"
  - subject: "Dont like v2"
    date: 2009-10-02
    body: "I think interface is very confusing, and I couldnt find option to show \"player only\" like in amarok 1.4.\r\nBig screen repulses me from using it."
    author: "beli0135"
  - subject: "Alternative fix"
    date: 2009-10-02
    body: "There is no \"show player only\" feature in V2.\r\n\r\nWhat you can do however, is installing a plasmoid which controls your media player. This has about the same effect."
    author: "vdboor"
  - subject: "I will not use Amarok until..."
    date: 2009-10-02
    body: "... It can play my vinyl collection."
    author: "burp"
  - subject: "and..."
    date: 2009-10-02
    body: "...skip between songs like a cd!"
    author: "timcarr89"
  - subject: ".. and wash my dishes. I"
    date: 2009-10-03
    body: ".. and wash my dishes. I can't believe it still can't do that.\r\n\r\nBut despite of course being unwilling to use Amarok until these crucial features have been added, I did have a look and now I'm addicted. Darn Amarok developers, this app is just too cool not to use :(\r\n\r\nThe rediscover your music thing is just TOO true!"
    author: "jospoortvliet"
  - subject: "It works"
    date: 2009-10-03
    body: "It really works. I am now playing Bob Marley's Rebel Music. The problem is that it's not intuitive. You have to browse to local music and there you will see the songs on the CD which you can drag and drop to the playing list, a bit awkward. The devs should add a play cd module or something similar so that it can't be missed. One click on the module and your CD plays. That's how it should work on a modern desktop."
    author: "Bobby"
  - subject: "A great release"
    date: 2009-10-03
    body: "I thought \"Amarok is back!\" when I started Amarok today. It's the first 2.x release that I really like. It looks great and (almost) everthing works but there is still some room for improvement. I personally would wish a faster start up, the cover download is not working for mp3 (but it works fine for CDs) and like I wrote, the play cd function should be more intuitive. Apart from that I would say well done, Amarok is back on track."
    author: "Bobby"
  - subject: "Congrats! Now Spotify support plz!!! :) If it's possible..."
    date: 2009-10-03
    body: "Even though Spotify works pretty well through wine. Some form of a plugin for Amarok would well rock. Would it be possible to add Spotify support through despotify (free software cmd line implementation) or libspotify? Something like that would fit perfectly to the \"Internet\" menu :)\r\n\r\n10\u20ac/month and unlimited access to 95% of the music out there (320kbps OGG stream) is not a bad deal at all and of course it's legal. I have personally turned from skeptic to believer. It's just so much more convenient and the experience is just so seamless. I think that kind of services are the future of music industry. Great quality, Reasonable pricing and it just works."
    author: "bliblibli"
  - subject: "Performance"
    date: 2009-10-03
    body: "Im very disappointed of Amarok 2 performance. It starts up very slowly. Amarok 1.4 started in about 4-5 seconds first time, and any of Amarok 2 series starts in about 11-12 seconds. That's a big regret to me, because i want to listen my music without having to wait 11 seconds. Please improve start up performance in Amarok 2.3. Thank you."
    author: "Nece228"
  - subject: "Performance"
    date: 2009-10-03
    body: "I also mentioned that among a few other things. Sadyly enough it's not only Amarok but also KDE 4 itself that I personally find to start up waaayv too slowly. \r\nThe most desktop users today are very impatient, that's why they buy powerful Laptops and PCs which they want to perform. I wish that the KDE devs would learn to take better advantage of the hardware, that we would get a lot more SPEED on the desktop. The beauty is there, the look and feel but performance needs a lot of improvement. It can't be that newer and \"better\" apps are much slower than the older ones. That's not the aim is it?"
    author: "Bobby"
  - subject: "I don't know what's"
    date: 2009-10-03
    body: "I don't know what's specifically wrong on your system, but this long startup time is just not normal. It takes about 2 seconds here.\r\n\r\nIf you send us a debug log (\"amarok --debug --nofork\") we'll be happy to look at it.\r\n"
    author: "Mark Kretschmann"
  - subject: "Expect that I cannot keep it"
    date: 2009-10-03
    body: "Expect that I cannot keep it above the other windows, which I always did with the player window in 1.4."
    author: "gd"
  - subject: "Missing features"
    date: 2009-10-04
    body: "Have they added labels back in? I understand that you can make Amarok 2.2 look mostly like 1.4 (which would be what I want - sorry, I want to devote my screen real estate to a compact playlist, not a bunch of useless shiny metadata), but it's not worth even trying until they have support for the music library features that I use, including labels."
    author: "marcan"
  - subject: "Where do I find this file? I"
    date: 2009-10-04
    body: "Where do I find this file? I have searched the Amarok directory but it's not there."
    author: "Bobby"
  - subject: "Sometimes slows down system"
    date: 2009-10-04
    body: "On my system (Eee PC 901 with openSUSE 11.1 and KDE 4.3.1) Amarok (both 2.1 and 2.2) sometimes (once a day or so) slows down my system to nearly unusable. These times it generates extremely high load (like 10 or 20) while its own CPU usage is usually still between 10% and 20% or even lower. My system becomes usable again only if I kill Amarok.\r\n\r\nIs there anyone who experienced this?"
    author: "gd"
  - subject: "The log"
    date: 2009-10-04
    body: "Here's the log when i run amarok --debug --nofork from terminal:\r\nhttp://pastebin.com/m1206b55\r\nAnyway, thanks for your care"
    author: "Nece228"
  - subject: "won't start on opensuse 11.0 :("
    date: 2009-10-05
    body: "Well, i tried it on opensuse 11.0, and it failed:\r\n\r\n amarok --debug --nofork\r\namarok: symbol lookup error: /usr/lib/libamaroklib.so.1: undefined symbol: _ZTIN6TagLib3MP44FileE\r\n\r\ndunno if my system is to blame or that the opensuse buildservice made an error..\r\n\r\n[edit]: \r\nFixed it by upgrading taglib manually. For some reason, upgrading amarok didn't upgrade tablib to its newest version in the buildservice"
    author: "whatever noticed"
  - subject: "Re:  Expect that I cannot keep it"
    date: 2009-10-05
    body: "True..\r\n\r\nThere are some workarounds:\r\n * drag the nowplaying plasmoid to the taskbar.\r\n * Run it with the 'plasmoidviewer' tool: \r\n  Alt+F2 -> type \"plasmoidviewer nowplaying\" -> enter.\r\n\r\nIn the terminal, you can try the following for more info:\r\n * plasmoidviewer --help\r\n * plasmoidviewer --list\r\n(which you can type as plasm&lt;tab&gt;&lt;tab&gt;o&lt;tab&gt;)"
    author: "vdboor"
  - subject: "If someone is up for coding"
    date: 2009-10-05
    body: "If someone is up for coding that I'm sure we can talk about it :) Just get in touch with us on our devel mailing list."
    author: "nightrose"
  - subject: "No Way!"
    date: 2009-10-05
    body: "No way! Lots of serous audiophiles or even just people who take their music very seriously still buy exclusively on CD's.  Until there are lossless digital audio formats some people will always want a physical copy.  Others like the safety of having copies if something happens to their digital collection.  That being said I even though I want to have CD's of everything in my collection I would still like to have copies for my iPod or to set up long party playlists.  I also sometimes use my laptop to DJ and like to be able to quickly pop in my own or someone else's CD's to compliment a mix.  I will be trying this feature out for sure because Amarok is an awesome project."
    author: "crazykiwi"
  - subject: "\"Until there are lossless"
    date: 2009-10-06
    body: "\"Until there are lossless digital audio formats some people will always want a physical copy.\"\r\n\r\nThere are lossless digital audio formats (e.g. FLAC). The problem is that those files are bigger (of course), but bring no benefit to most customers (I for one am not able to tell FLAC/OGV/MP3 apart). If many customers would know about and demand lossless audio files, they would be distributed, and there would be really no need for CDs anymore."
    author: "majewsky"
  - subject: "Howto"
    date: 2009-10-06
    body: "Say \"amarok --debug --nofork > ~/amarok.log\" on the console (when Amarok is not running). This will produce \"amarok.log\" in your home directory."
    author: "majewsky"
  - subject: "Certainly not"
    date: 2009-10-11
    body: "Audiobooks typically come on CD and are not downloadable. \r\n\r\nMy first experience after upgrading Kubuntu and thus to Amarok 2: \"How the hell do I play CDs?\" I thought people were kidding when I was told I had to install some other program (kscd). "
    author: "Monika"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/amarok2.2_1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/amarok2.2_1_1.png" /></a><br />
Amarok 2.2</div>
The Amarok team is excited to announce the release of Amarok 2.2. In three and a half months, Amarok has made a huge leap forward, gaining many new features and a lot of old features from 1.4 have returned.

Amarok 2.2 brings back support for sorting and shuffling the playlist, for an external MySQL database as well as for playing audio CDs to name a few. It brings a new video and photo applet to show media related to the current song. The layout can be modified to suit your needs thanks to dock widgets and the sidebar has been changed to be easier to navigate with bread crumbs.

Read more about all the new exciting features in the <a href="http://amarok.kde.org/en/releases/2.2">release announcement</a>.
<!--break-->
