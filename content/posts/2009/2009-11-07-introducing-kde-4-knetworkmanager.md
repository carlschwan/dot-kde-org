---
title: "Introducing KDE 4 KNetworkManager"
date:    2009-11-07
authors:
  - "Bille"
slug:    introducing-kde-4-knetworkmanager
comments:
  - subject: "Why extragear?"
    date: 2009-11-07
    body: "Isnt kdebase the right place? I dont like the fact that all cool stuff are moving away from KDE/.\r\n\r\nEdit: I forgot to mention that I've been using it from months ago and it always just worked for me. Good job."
    author: "emilsedgh"
  - subject: "Why extragear?"
    date: 2009-11-07
    body: "Quite simple I would say, for several reasons.\r\n\r\nIt still need work, both stability and feature wise. So tying it to KDEs 6 month cycle does not make sense. If they do that, it should enter feature freeze about now, like the rest of KDE 4.4. And no changes would go in except bug fixes, locking the current feature set until KDE 4.5.\r\n\r\nIt depends on 3rd party software, Network Manger, that do not follow KDE release schedules. With current KDE release schedule, it will in worst case lead to new versions of Network Manger to be unsupported for 7 months(release cycle + freeze).\r\n\r\nStaying in Extragear give the opportunity to have more frequent (feature) releases and the possibility to adopt to upstream Network Manger releases. "
    author: "Morty"
  - subject: "wicd would have been a better choice. (n/t)"
    date: 2009-11-07
    body: "Never have liked networkmanager and never will."
    author: "stumbles"
  - subject: "wicd"
    date: 2009-11-07
    body: "Actually yes, I recently tried wicd and it is, for the networks that I have had to use, incomparably more robust than even the GNOME NetworkManager applet (nm-applet), and its UI makes the most sense too and is really good at asking you only the bare minimum questions (connect by only knowing the passphrase). Anyway, just wanted to say that I was glad to read above that the KDE-networkmanager developers intend to support wicd as a backend, and I also hope that they can borrow good ideas from the wicd UI."
    author: "Bjacob"
  - subject: "Cool, but still not for me"
    date: 2009-11-07
    body: "Wake me up when KNetworkManager can import .pcf files for my VPN connections. Until then I stay with nm-applet."
    author: "KAMiKAZOW"
  - subject: "and wicd can be compiled without the gui stuff."
    date: 2009-11-07
    body: "ya and you already have it built in (so to speak with kdebase4-workspace) as solid_wicd.so. "
    author: "stumbles"
  - subject: "need to learn with nm-applet"
    date: 2009-11-07
    body: "I was using knetworkmanager in the beginning (since it was the no-so-good plasma applet in openSuSE) and I think it should be as easy to use as nm-applet for GSM connections. \r\nI have a modem and cant connect with knetworkmanager even if I properly configure everything by hand.\r\nWith nm-applet I just follow a little wizard and everything is OK. easy;"
    author: "philippe.hac"
  - subject: "Does mobile broad band work?"
    date: 2009-11-08
    body: "Last time I checked knetworkmanager mobile broadband was not working. Has anything changed?"
    author: "jithin1987"
  - subject: "nice article, welcome kde extention, some unfriendly comments"
    date: 2009-11-08
    body: "last thing first:\r\nwhy so many negative comments on this article?\r\n\r\ni like the article! it is a nice write-up that gives some insights into an interesting part of a modern and mobile \"desktop\". it also tries to explain a bit what went wrong (why there _was_ no proper default solution for wireless on KDE).\r\n\r\nmuch of the negativity seems aimed at the network manager which is in very welcome tool: we need a good solution for this that integrates with the rest of KDE. and it just had an amazing redesign. i love the way it shines on kubuntu 9.10!\r\n\r\nso be a bit grateful ok? it's free remember!\r\nand hurray for open innfovation (reflecting on the last paragraph of the article)."
    author: "cies"
  - subject: "Good work, Will. Thanks"
    date: 2009-11-08
    body: "Good work, Will. Thanks :)\r\n\r\n<br/>\r\n<p>\r\n(The auto-repeating feature for the topic here is kinda funky, and <a href='http://uncyclopedia.wikia.com/wiki/Redundancy'>redundant</a>. Also, the auto-repeating feature for the topic here is kinda funky, and <a href='http://uncyclopedia.wikia.com/wiki/Redundancy'>redundant</a>. )\r\n</p>\r\n"
    author: "Mark Kretschmann"
  - subject: "Thanks"
    date: 2009-11-08
    body: "Yes, thank you Will and others for this important part of KDE. And also for the nice article."
    author: "janhalasa"
  - subject: "More than one thinks"
    date: 2009-11-08
    body: "I just found that PPTP was supported natively with the current KNetworkManager. Very neat! Thanks!"
    author: "gwittenburg"
  - subject: "Ability to run scripts on Connect"
    date: 2009-11-09
    body: "I have a script that automatically reconfigures a local HTTP proxy, so that I don't have to reconfigure all proxy settings in all the applications when working on a different network. (In case you are interested, here it is: http://www.yeap.de/blog2.0/archives/212-Road-Warriors-Proxy.html).\r\n\r\nWith the old KDE 3 knetworkmanager it was possible to configure commands to run on certain actions with the notifications framework. The KDE 4 Network Manager does not offer this possibility, so I had to switch to wicd.\r\n\r\nSo it would be great, if notifications could be made available for knetworkmanager 4 again for feature parity with the old version."
    author: "Yaba"
  - subject: "usability issues"
    date: 2009-11-09
    body: "Nice job guys, KNetworkManager is definitely getting better. But there are quite important issues: \r\n\r\n<ul>\r\n<li>there is no visible sign, that network (wifi) is not connected (there could be different icon)</li>\r\n<li>in list of available wifi networks is missing information about strength of signal, encryption, etc. </li>\r\n<li>there are no information about progress in connecting to network (well it's hard to find out what's going on) </li>\r\n<li>no information why wasn't connection successful</li>\r\n</ul>\r\n"
    author: "tombart"
  - subject: "Agreed"
    date: 2009-11-09
    body: "It's early days and what is there already is great, but some indication of whether a network is encrypted would be great - you go somewhere new, you want to quickly see what open networks are available, not waste time trawling through private encrypted networks."
    author: "swjarvis"
  - subject: "Your questions answered."
    date: 2009-11-09
    body: "1) I am currently discussing more detailed status icons with the Oxygen artists\r\n\r\n2) These features were completed too late for Kubuntu but you can get it with openSUSE 11.2\r\n\r\n3) Also being handled as part of 1) - there is a detailed status change knotification you can enable, but this is a bit annoying because there is no way in KNotification to re-use an existing notification dialog by changing the text yet\r\n\r\n4) The detailed status change notification also gives a reason (but be aware that NetworkManager itself often does not give useful status change information.\r\n\r\n\r\n"
    author: "Bille"
  - subject: "solid_wicd"
    date: 2009-11-09
    body: "Does solid_wicd actually do something in 4.3.x ? In fresh Kubuntu 9.10 installation I had an option for selecting wicd backend in Solid config, but it had no (visible) effect. Wicd was installed, NetworkManager removed.\r\n\r\nAnd I must say that I'm also one who prefers wicd over (K)NetworkManager, atleast for wireless and for my hw and network setup."
    author: "margusl"
  - subject: "Negative Commenters"
    date: 2009-11-09
    body: "I thought that both, negative and positive comments, were appreciated.\r\nSome of us did just show our point of view, like about using wicd (what will happen in future) and about knetworkmanager being hard to use when compared to nm-applet.\r\nThe developers can then look at what people are looking for and improve the software.\r\nI appreciate the work but, as it does not work for me at all, it's just that."
    author: "philippe.hac"
  - subject: "sounds great.\nOne thing i"
    date: 2009-11-09
    body: "sounds great.\r\nOne thing i hope will come one day are error messages. Currently when i connect for example to a network (wifi or other), and it fails, there is no error message. I don't know what failed (connection to the wifi, getting a ip etc), it just does not work. Would be nice if there was a way to get some more information."
    author: "Asraniel"
  - subject: "I loathe network"
    date: 2009-11-09
    body: "I loathe network manager.\r\n\r\nUnless there is a reliable way to make connections in the command line then it is worthless. Reliable meaning simple commands.\r\n\r\nWicd is nice and it works well. Mandriva's connection tools are far better than network manager. \r\n\r\nIts interesting to see reliable network management tools that existed before network manager, in the case of drakconnect, be completely ignored. "
    author: "tubasoldier"
  - subject: "Wicd"
    date: 2009-11-09
    body: "Yes and no. Yes as your environment reacts to connections detected by Wicd, and applications querying network information from Solid will get that from Wicd.\r\n\r\n\r\nNo as the Wicd backend for the NM applet is not ready yet. Unfortunately due to lack of time on my side it got delayed (as I am also working on polkit, which is a requirement for NM, and all that stuff that follows). I have already something fairly working, namely a KCM port of the wicd configuration (http://gitorious.org/wicdkde/kcmwicd) and I hope I will get the wicd backend done after my exams, in december, hoping nothing gets in my way this time. Just watch out Planet KDE for news.\r\n"
    author: "drf"
  - subject: "Good to see progress!"
    date: 2009-11-09
    body: "Thanks to Will and the Team! It looks like soon we'll have the coolest NetworkManager out there. :-) "
    author: "FreeMinded"
  - subject: "wicd"
    date: 2009-11-10
    body: "Thanks for clarifying drf!\r\nLast night when I first discovered wicdkde from gitorious and saw that last commit was from mid-september, I wasn't quite sure what to conclude from this. \r\n\r\nYour work is really apriciated, but I'm sure you allready knew that [:\r\n\r\n"
    author: "margusl"
  - subject: "Awesome"
    date: 2009-11-10
    body: "Thanks for the additional info"
    author: "swjarvis"
  - subject: "Must"
    date: 2009-11-10
    body: "I must say that Mandrivas network tools are great, but I do not like because it is written as GTK+. So it does not fit to KDE4 well. Mandriva should write all their tools as backends and then offer frontends with GTK+ and Qt. Currently the drak tools for networking are great even from CLI. Mayby the best."
    author: "Fri13"
  - subject: "I do not know what version"
    date: 2009-11-10
    body: "I do not know what version the OpenSUSE 11.2 use, but it has the security icons and link quality information. "
    author: "Fri13"
  - subject: "Mobile connections"
    date: 2009-11-10
    body: "I'm impressed by the new network manager! The applet works fine and all my connections are really stable. With the new package delivered with my current Ubuntu (Karmic) I'm thinking of tethering.\r\n\r\nI have a Nokia E51. I see several signs of possible mobile connections, but can't manage to get it working. Is it possible to provide some more information about this topic? Wired and wireless connections are pretty straight forward and even vpn isn't hard to configure. But I can't find out how to complete a mobile connection.\r\n\r\nThanks for having this information shared, I hope more articles about the network manager possibilities are written in the near future :)"
    author: "JSluiman"
  - subject: "thanks"
    date: 2009-11-11
    body: "Glad to hear that. Is this going to be in update or we have to wait till next Kubuntu 10.04? \r\n\r\nOther thing, when AP is down (electricity blackout) KNetworkManager keeps asking for password (although it was established connection and now the AP is no longer available), just error message (disconnected) would be enough. \r\n\r\nI love the possibility of choice, but in most cases user doesn't have a clue what kind of crazy settings should be used for a wifi connection. So a very simple dialog with meaningfully detected parameters would be nice. And on some \"advanced\" card others nice possibilities for special cases.  \r\n\r\nRegardless these objections I appreciate your work, you made a huge step in development of KNM."
    author: "tombart"
  - subject: "Repeated password dialogs"
    date: 2009-11-11
    body: "Repeated password dialogs when AP is down:  KNM can't influence NM's behaviour in this respect.  If NM thinks the AP is there and tries to connect, it's NM's problem.  \r\n\r\nSimple settings: Yes, a simple mode with default settings except for password is on the plan.  We had to get the full settings working first before we can do this though."
    author: "Bille"
  - subject: "Vpn"
    date: 2009-11-11
    body: "Is KNetworkManager going to handle VPNs as well? An historical problem with KDE has always been the lack of a user-friendly VPN client. I know, I know, there's Kvpn but we're talking \"user-friendly\" here or, if you prefer, \"noob-friendly\". I admit that my last try with Kvpn dates back a few months but, in my experience, you need to know connection parameters in detail, too much in detail (not to mention all the \"you need to install this or that daemon beforehand\" error messages) for a successful connection. I have never succeeded, and google tells me that I'm definetely not alone. With other OSes (guess which two?) I've never ever had any serious problem. Will KNetworkMagager fix this?\r\n\r\nThanks\r\nHect"
    author: "spz001"
  - subject: "VPN support"
    date: 2009-11-11
    body: "KnetworkManager, via the KDE Network Management stack, already supports VPNs via plugins - OpenVPN and VPNC are working already and PPTP just needs bugfixing.  Eckhard Woerner has written a couple of custom VPN plugins as well.  \r\n\r\nThe daemon requirements exist but *should* be solved by your distribution's dependency resolution systems.  I know that at least on openSUSE, as soon as you install any of the VPN plugins, the NM plugins and any other daemons it uses are pulled in.  If this doesn't work for you, complain loudly to your distro.\r\n\r\nBut the most important part of your post is how to make VPN setup user-friendly, and this is the hardest thing to do.  VPN setups are not simple.  Our best chance is to make a config file importer - the GNOME applet already has this for OpenVPN and VPNC.  It's not hard to do either, just some text file bashing.  Other than that we can make VPNs more user friendly by giving good (not cryptic) error messages when things go wrong.  I am working on improving the notifications in KNM generally today."
    author: "Bille"
  - subject: "Mobile connections"
    date: 2009-11-11
    body: "In principle they are supported if paired using some bluetooth tool and the bdaddr of the phone is set in the connection, but I don't have a phone capable of this myself so it's probably going to be up to someone else to beat the bugs out of this one.\r\n\r\nI have a PCMCIA 3G dongle which I test with, and which sort of works with NM 0.7.1.  NM 0.8pre is said to work more reliably with Mobile.  \r\n\r\nMy next actions on Mobile are going to be integrating a configuration assistant with provider database and supporting the ModemManager interface for things like cellular signal strength and network selection."
    author: "Bille"
  - subject: "Error messages and notifications..."
    date: 2009-11-11
    body: "... are today's special on my hacking menu. svn up!"
    author: "Bille"
  - subject: "NetworkManager"
    date: 2009-11-11
    body: "Although you have gone to great detail describing this and what it is capable of doing, it seems as if there is much more work to be done. We look forward to further updates now and in the future. <a href=\"http://188.165.58.192\" id=\"clean-url\" class=\"install\">casino en ligne</a>"
    author: "George Buchanan"
  - subject: "VPN support"
    date: 2009-11-12
    body: "It's nice to hear that PPTP will work fine in KNM! Then I will throw out nm-applet at once. Well, only mobile broadband support would be issue for me.\r\nBy the way, do you know that many providers from ex-USSR countries provide Internet to end users by VPN? So, we need bugfixing! :)\r\nThank you in advance, guys!"
    author: "amokk"
  - subject: "Many thanks! "
    date: 2009-11-12
    body: "Your work looks great - and seeing what is about to come is even better! \r\n\r\nCan't wait for KDE 4.4 :) \r\n(Seems I'll install a beta KDE again ;) )\r\n\r\nMany thanks for your work! "
    author: "ArneBab"
  - subject: "Nice article"
    date: 2009-11-12
    body: "Thank you for the article, Will! \r\n\r\nIt is nice to read and interesting from front to back - and the images help making it feel easy to understand, though you talk about quite deep issues. \r\n\r\nThe insight into development processes and the future directions made it especially interesting for me. The last part also set a nice political statement, I think. It hit the \"distributions are a part of what makes free software different\" argument which I also found in a Gentoo blog a week ago (with which I agree). \r\n\r\nAnd I really can't name a part I missed :) "
    author: "ArneBab"
  - subject: "Improving"
    date: 2009-11-12
    body: "This is the first version of KNetworkManager where all my usecases are implemented. ;)\r\nBefore I had to resort to nm-applet, but since the one included in openSuSE 11.2 I'm happy.\r\nConnection to my home wired and wireless net works, and also 2 nets from my University, with VPN included.\r\nOnce nice feature to have (but I can live without it) would be to automatically enable VPN in some of the connections I have (but not all of them)."
    author: "DeiF"
  - subject: "VPN support"
    date: 2009-11-12
    body: "I think that a good part of \"user-friendly\" is provide good defaults. A good client should provide a few common case templates to use as a base for your own profiles. If I need to connect to a Windows Vpn (one hell of a common case) I should tinker with domain names and such, maybe more but definetly not with handshakes or that kind of stuff. I totally agree with you about the good error messages though.\r\n\r\nHect"
    author: "spz001"
---
One of the few utility programs that are used every day on mobile devices is a wireless networking tool, but somehow this is one of the last applications to appear for KDE 4. With the autumn 2009 crop of Linux distributions, a usable client for the widely used <a href="http://projects.gnome.org/NetworkManager/">NetworkManager</a> system finally makes its debut.
<!--break-->
<h2>Why so long?</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/graphic.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/graphic_1.png" width="299" height="209"/></a><br />D-Bus connects the NetworkManager Server and Client</div>

So why did a native KDE 4 version of what is basically a system tray icon take three and a half KDE 4 releases to complete?  There are several reasons. The original NetworkManager client was a GNOME applet and was developed in tandem with the server. Because of this, the NetworkManager interfaces for client developers were only <a href="http://live.gnome.org/NetworkManagerConfiguration">partially documented</a>. The NetworkManager system consists of a system daemon that receives network settings from a pair of settings services, one of which is run by the first local logged in user. This service supplies configuration information by placing Connection objects on the system bus. A client application sends commands to the service to activate these connections. One of the first tasks, therefore, was to take the D-Bus interface documentation system from Telepathy and apply it to the <a href="http://projects.gnome.org/NetworkManager/developers/spec.html">NetworkManager D-Bus interface</a>. This was contributed to the upstream NetworkManager project, enabling up-to-date HTML documentation to be produced at build time so that other client authors can work without reading the NetworkManager C sources. Another reason was that the existing <a href="http://en.opensuse.org/KNetworkManager">KDE 3 KNetworkManager</a> needed many improvements to its user interface to fit in with other KDE 4 apps. It used the Qt3 D-Bus bindings, so this layer had to be completely rewritten as well. The current KDE 4 code shares only a few core algorithms with its predecessor. Finally, the KDE 3 KNetworkManager clients for NM 0.6 and 0.7 were developed entirely in-house at SUSE and never merged into KDE main modules, so few developers joined in and most of the work was done when necessary by SUSE engineers.

<h2>A new beginning, and a false dawn</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><img src="http://dot.kde.org/sites/dot.kde.org/files/popup2.png" width="314" height="355"/></a><br />The KNetworkManager menu</div>

<a href="http://www.kdedevelopers.org/blog/77">Will Stephenson</a> of the openSUSE KDE Team took over responsibility for the KDE 4 NetworkManager client in 2007 from another team and began coding the base D-Bus layers using the new Qt4 D-Bus bindings. At the <a href="http://dot.kde.org/2008/01/20/second-day-kde-40-release-event">KDE 4 Launch Event in Mountain View in January 2008</a>, he met with Celeste Paul, Christopher Blauvelt, Sebastian Kügler and others to develop a concept for a user-friendly yet powerful NM client. It was to be designed as a native Plasma Network Management applet for KDE 4.1, and shipped on several distributions. But this client had more than its share of flaws, mostly because the team spent much of its time dealing with layout problems in the very new QGraphicsView classes, instead of ensuring that NetworkManager features worked, and the design had grown naturally over time, resulting in an amorphous structure that was hard to work with. Then in May 2009, the KDE eV offered to sponsor a <a href="http://www.kdedevelopers.org/node/3975">coding sprint</a> <a href="http://www.kdedevelopers.org/node/3977">in Oslo</a>, where seven developers met and produced a workable design to go forward with. Throughout Summer 2009 this was refined, implemented and tested. The problems with the previous implementation led the team to produce a strongly layered design so that a simpler System Tray applet could be written first, running as an independent process so that any bugs could not crash Plasma, with most of the code in libraries that a subsequent Plasma applet can build on.

<h2>What KNetworkManager offers</h2>

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/ipv4basic.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/ipv4basic_1.png" width="259" height="299"/></a><br />Basic configuration</div>

So what does the new KNetworkManager offer? At the least, a NetworkManager client provides user controllable networking. A system daemon receives configuration and commands from user programs. This is what the default nm-applet tool for GNOME offers. But since we were creating a new client from whole cloth, and with the experience of two KDE 3 clients and nm-applet to work from, we decided to improve on existing designs to make it as usable as possible. For example, where there are many wireless networks available, showing all the networks all the time in the applet's menu results in a screen-filling popup, making it hard to spot the network you wish to use. Instead, the applet only shows networks that you have used before out of those that are present. To connect to another network, or a previously used hidden network, a dialog provides a scrollable complete list, which can be easily filtered with a search line. Even though KNetworkManager is not built with the fashionable QGraphicsView used by Plasma for its UI, Qt allows a lightweight yet attractive interface, with menu items that display wireless strength and detailed security information, while other designs <a href="http://blogs.gnome.org/dcbw/files/2009/05/netmgr_05.png">are still mockups</a>. You can connect to networks with a single click in the popup menu, and its configuration dialog plugs into the standard KDE System Settings tool, making it easy to find there or in the popup menu.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/maintooltip.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/maintooltip_1.png" width="300" height="249"/></a><br />The system tray applet with tooltip</div>

But that's not all we managed to fit in. Due to the modular structure of the code, we've already been able to attract new developers like Paul Marchouk to add polish and enhancements while the core was being completed. Features such as setting custom icons for a connection, configurable tooltips for both the hardcore networking specialist and the first time user, a choice between one icon for everything, or an icon per network interface (like in Windows), and a configuration UI for IP addresses that is 'better than OS X' have come from new KDE contributors. Other tweaks like storing passwords and other secrets securely or forcing them to be entered every time please administrators, and a Wireless Scan display lets you see what access points are in the neighbourhood.

<h2>But what about Plasma?</h2>

Some readers will be asking about the Plasma applet now. While this isn't being released yet, it hasn't stood still. The Oslo sprint design splits the user interface from the configuration UIs and the D-Bus services that do most of the work, so 90% of the KNetworkManager code will be used directly by the Network Management Plasma applet. Meanwhile, Sebastian Kügler is continuing to polish the code, and the layout issues have been fixed in Qt. We expect that the Plasma applet will be released as part of KDE 4.4.

<h2>Future directions</h2>

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/itemtooltip.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/itemtooltip_1.png" width="257" height="300"/></a><br />Detailed information on a network</div>
While we hope that users of <a href="http://en.opensuse.org/OpenSUSE_11.2">openSUSE 11.2</a>, <a href="http://www.kubuntu.org/news/9.10-release">Kubuntu 9.10</a> and <a href="http://fedoraproject.org/get-fedora-kde">Fedora 12</a> will agree that KDE 4 KNetworkManager is useful for daily wired and wireless networking, and is mostly functional for 3G mobile broadband connections, there is still a lot to be done. The number of possible networking configurations mean there are still known and undiscovered bugs, which are being fixed now. A set of final icons are being prepared by the Oxygen team. Improvements for KDE 4.4 will include a minimal UI when creating new connections, showing only the password fields necessary, and better input verification to produce connections that work first time. Mobile Broadband will be improved by the inclusion of a database of ISPs, and support for the ModemManager interface will deliver cellular signal strength. IPv6 configuration should be available soon, and it will also be possible to create and edit system-wide connections so networking is up before users log in. And by passing information about connection usage to Nepomuk, queries such as 'Which files did I download at the office yesterday?' become possible.  Finally, we hope that backends for other network management stacks such as <a href="http://wicd.sourceforge.net/">wicd</a>, <a href="http://connman.net/">ConnMan</a>, and the in-house tools used by <a href="http://wiki.mandriva.com/en/Category:EasyWifi">Mandriva</a> and <a href="http://websvn.pardus.org.tr/trunk/kde/network-manager/?root=uludag">Pardus</a> will be written, so that a KDE user will feel at home no matter which distro he/she picks up.

<h2>Distributions working for KDE, not the other way round</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/multipleicons.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/multipleicons_1.png" width="299" height="237"/></a><br />Extensive configuration settings</div>
Most of the the KDE 4 Network Management stack was developed by Will Stephenson as a Novell employee, but it is important to point out that this is an open and Free development, with and for every other distribution and end user to benefit from. Throughout the development we have liaised with Fedora and Kubuntu and exchanged bug reports, experiences, patches and even icons.  The source code is <a href="http://websvn.kde.org/trunk/playground/base/plasma/applets/networkmanager">in KDE SVN</a>, and will shortly move to Extragear, and developer documentation will be available <a href="http://techbase.kde.org/Projects/Network_Management#KDE_4_Architecture">at TechBase</a>. We hope that this serves as a model for other cross-distribution cooperation to benefit all of KDE.