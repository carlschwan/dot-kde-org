---
title: "Growth Metrics for KDE Contributors"
date:    2009-07-14
authors:
  - "troy"
slug:    growth-metrics-kde-contributors
comments:
  - subject: "Releases?"
    date: 2009-07-14
    body: "Would be interesting to see some lines marking the major releases on the graphs, for example to see how .0 versions affected the attraction of developers."
    author: "odysseus"
  - subject: "Estimates"
    date: 2009-07-14
    body: "It can be somewhat estimated.\r\n\r\nJuly 1998 - 1.0\r\nOctober 2000 - 2.0\r\nApril 2002 - 3.0\r\nNovember 2005 - 3.5\r\nJanuary 2008 - 4.0\r\n\r\nWithout doing some sort of signal analysis on the graph, the easiest thing to notice is that the total number of contributors started to plateau about a year after the release of 3.0, as the platform was apparently starting to stagnate.  \"Regular growth\" resumes in the lead-up to 4.0.  (Also interesting is that the rate of new developers does not seem to be significantly affected, which may imply we lost some longer-term developers during that time.)\r\n\r\nAlso interesting to note is that the numbers become \"noisier\" over time.  This is likely due to some periodic signals that have been introduced into KDE's development cycle that weren't there originally.  For example, Akademy now happens once per year (and we're adding even more periodic conferences), while in the early days, such a conference didn't exist.  Google's Summer of Code brings in new coders, but is only significant during summers. KDE 4.x introduced a 6-month release cycle. These events tend to spike the data in unique ways and make it a little noisier.\r\n\r\nPersonally, I'd love to see some similar numbers from Gnome after 3.0 is released to see if the same trends are present (period of stagnating development, period of increased activity...) Perhaps this will produce some sort of grander theory to cycles within the open source development processes. *ponders*"
    author: "troy"
  - subject: "Improve the tutorial?"
    date: 2009-07-15
    body: "Speaking of newcomers, it could be nice to update the introductory tutorial: http://techbase.kde.org/Getting_Started/Build/KDE4\r\n\r\nI was not able to get KDE working by following it strictly, it should include the build of all kdesupport to avoid any problem of dependency."
    author: "Ikipou"
  - subject: "It's an ascending trend, i"
    date: 2009-07-15
    body: "It's an ascending trend, i suppose this is good thing :)."
    author: "ultimatev"
  - subject: "Wiki"
    date: 2009-07-15
    body: "Since techbase is a wiki, there is nothing stopping you from documenting your experience right there where it belongs. That would be a great way to contribute to KDE too!\r\n\r\nSpeaking of contributors: I guess the stats in these graphs only refer to contributors in the sense of coders who submit their work through the revision control system? I think that should be more clear, as there are obviously other ways one can contribute to the KDE project."
    author: "andresomers"
  - subject: "RCS only"
    date: 2009-07-16
    body: "Yeah, it is specific to those those that are stored on the SVN server, but within KDE, there are a lot of things running through this server.  Docs, translations, many KDE websites, artwork, code..."
    author: "troy"
---

In 1996 when KDE was <a href="http://www.kde.org/announcements/announcement.php">first announced</a>, it had only a handful of developers and the project could manage the source code without using a revision control system. More and more developers have begun to contribute to KDE over the years, and while there has been some attrition, the total number of active developers working on KDE has been steadily growing.  

In order to get a pulse from the current developer community, Simon St. James and Arthur Schiwon produced and plotted two basic metrics that show the continued growth within the KDE community.

<div style="border: thin solid grey; padding: 1ex; width: 800px; text-align: center; margin-left: auto; margin-right: auto">
<img src="http://dot.kde.org/sites/dot.kde.org/files/active-contributors-800.png" width="800" height="377" /><br />
Active KDE Contributers 1997-2009
</div>

The first chart, above, shows the total number of active KDE contributers in any given month over a 12-year period. While there is some ebb and flow, its quite obvious that the activity surrounding KDE is ever increasing. This is further reinforced by the chart below, showing the number of new contributers each month over the same period.  

<div style="border: thin solid grey; padding: 1ex; width: 800px; text-align: center; margin-left: auto; margin-right: auto">
<img src="http://dot.kde.org/sites/dot.kde.org/files/new-contributors-800.png" width="800" height="377" /><br />
New KDE Contributers 1997-2009
</div>

The second chart shows more fluctuations due to a smaller scale.  It also shows that, short-period fluctuations notwithstanding, KDE has been steadily recruiting more contributors over time.

In fact, while the earliest KDE quickly outpaced a world without a revision control system, the current KDE is attempting to adjust to new tools and techniques that will help it scale well into the future. With the probable <a href="http://amarok.kde.org/blog/archives/1070-More-Info-on-Gitorious.org.html">conversion to decentralized revision control</a> (pending some testing), the era of being able to track the size of the KDE developer community by commit logs alone may be coming to an end.

If you are interested in contributing to KDE in any way, visit <a href="http://techbase.kde.org/Welcome_to_KDE_TechBase">Techbase</a> to see how to get started. If you get stuck, or would prefer not to code, try the KDE <a href="http://forum.kde.org/">Forums</a>, <a href="http://www.kde.org/mailinglists/">mailing lists</a>, or one of our <a href="http://userbase.kde.org/IRC_Channels">irc channels</a> for additional help. We also highly recommend joining a bug day organised by the  <a href="http://techbase.kde.org/Contribute/Bugsquad">bugsquad</a>. Finding and reporting bugs is one of the easiest and least demanding ways to contribute to and learn more about KDE and it is highly valuable to our developers.