---
title: "Computerworld: OpenChange, KDE bring Exchange Compatibility to Linux"
date:    2009-01-31
authors:
  - "jriddell"
slug:    computerworld-openchange-kde-bring-exchange-compatibility-linux
comments:
  - subject: "Sweet!"
    date: 2009-01-31
    body: "This is what I've been hoping for in the last 3 years. Yeah, we all know that Exchange is bad for the soul, but there are a lot of people like me who are forced to use it. If this will allow me access to the GAL, shared calendars, public folders, free/busy, and also correct daylight savings I am sold! (Kmail/Oorganizer does some of this to some degree, but it's been flakey and very buggy).\r\n\r\nWay to go guys, this thankful soul appreciates your work."
    author: "sarathmenon"
  - subject: "Wow"
    date: 2009-01-31
    body: "This is amazing! When I first read about akonadi a bit over a year ago, I realized that its is an exceptionally good concept, allowing for a whole new level of integration. But implementing the OpenExchange protocols beyond mail is something that could make akonadi a huge success. I am really impressed by the PIM team. Thanks to all of you for your excellent work!"
    author: "mutlu"
  - subject: "Great!"
    date: 2009-01-31
    body: "This is great news! In our company this is one of the main reasons we don't succeed very well in a move from Windows to Linux. Maybe for us geeks this is a no-brainer, but for office users Exchange/Outlook is their primary tool and suggesting to get another PIM suite is simply out of the question.<BR><BR>\r\n\r\nI've been trying for the last 6 months to use Evolution on a daily basis but it's... not my thing. I find it a very annoying application that crashes often.\r\n\r\nSo KDEPIM with Exchange calendaring would be great! "
    author: "H. ten Berge"
  - subject: "You ain't the only one"
    date: 2009-01-31
    body: "You ain't the only one thinking Evolution is unstable. It's also heavy on resources, and at least on the exchange connector, cumbersome and clumsy. I'd much rather use KMail, or Thunderbird for what's worth."
    author: "NabLa"
  - subject: "mee too"
    date: 2009-02-01
    body: "I am waiting for this too. It would be great for some webdevs who are using linux and kde for work and still need a virtaul machine running the M$ to access the Exchange for their companies pim. \r\nThis would be very cool to see in 4.3\r\n\r\ncheers \r\nblendo from http://www.kde4.de"
    author: "kde4.de"
  - subject: "Very important feature"
    date: 2009-02-01
    body: "This is the main reason stated by people resisting Linux as their office computer OS in our organization. Thank you very much."
    author: "yzg"
  - subject: "Good work!"
    date: 2009-02-02
    body: "Good work Brad!\r\n\r\nYet another small step to world domination. :-D\r\n\r\n'am starting to wonder about FD.o thought."
    author: "KSU257"
  - subject: "its good for knowledge given"
    date: 2009-06-11
    body: "its good for knowledge given us..."
    author: "jacus"
---
<a href="http://www.computerworld.com.au/article/274883/openchange_kde_bring_exchange_compatibility_linux">Computerworld covers progress in OpenChange</a>, the Microsoft Exchange server and client replacement.  He looks at integration work being done in Akonadi and how that will arrive in KDE 4.3.  <em>"Speaking at the Linux.conf.au conference in Hobart last week, Canberra-based OpenChange and KDE developer Brad Hards said 'We're working with the Microsoft Exchange protocols and work it mostly does.  Mail is easy, the stuff which is hard is the address book, calendar, free-busy lists, and notes. These are a big deal in the enterprise.'"</em>
