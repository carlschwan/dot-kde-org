---
title: "Konqueror BugDay Starting Saturday, June 20th"
date:    2009-06-18
authors:
  - "blauzahl"
slug:    konqueror-bugday-starting-saturday-june-20th
comments:
  - subject: "Oh, baby!"
    date: 2009-06-18
    body: "Why bugday?\r\n\r\nI have a lot of bugs/feature requests/regressions on my watch list even without it!\r\n\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=122599\">Bug 122599</a>\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=153599\">Bug 153599</a>\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=153602\">Bug 153602</a>\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=154095\">Bug 154095</a>\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=154098\">Bug 154098</a>\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=166241\">Bug 166241</a>\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=166687\">Bug 166687</a>\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=171256\">Bug 171256</a>\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=178419\">Bug 178419</a>\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=185052\">Bug 185052</a>\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=185100\">Bug 185100</a>\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=189904\">Bug 189904</a>\r\n<a href=\"https://bugs.kde.org/show_bug.cgi?id=195466\">Bug 195466</a>\r\n\r\n:)"
    author: "birdie"
  - subject: "I do not know about you but I"
    date: 2009-06-18
    body: "I do not know about you but I think there are some KDE users who cannot code and would like to help KDE. Clean up old bug reports/verify that they are still valid and write test cases does help the developers"
    author: "beer"
  - subject: "Have you bothered to check"
    date: 2009-06-18
    body: "Have you bothered to check the bug reports I've mentioned? ;)\r\n\r\nSome of them are clearly identified as bugs (with testcases available), some of them are identified as missing features, some of them even prevent users from browsing the Internet (like broken redirects which cause Konqueror to request pages via :80 notation and that breaks a lot of web sites and KWallet integration)."
    author: "birdie"
  - subject: "yes I have looked at them but"
    date: 2009-06-18
    body: "yes I have looked at them but I am not a konq developer. I am also waiting for some bugs in plasma to get fixed (missing duel head/twin view support) and a missing admin button in system settings annoying it is.\r\nThe point with those Bug days is to help the devs with eks making make test cases and se if the bugs stil is present in recent revision of trunk and maybe find a dublication. During this helps the devs so they do not have to do it and hopefully get more time to correct the rest of the bugs"
    author: "beer"
  - subject: "Cool!"
    date: 2009-06-19
    body: "Keep up the great work, despite attention seeking trolls, Konq rules my machine."
    author: "Jerzy Bischoff"
  - subject: "Oh, yes, let's bury our heads"
    date: 2009-06-19
    body: "Oh, yes, let's bury our heads in the sand and pretend that everything's fine, but in my case I prefer to file bug reports and help the community.\r\n\r\nStrange that everyone tries to mod me down for my critical messages (not trolling in any way). ;) but I don't care - I do a lot more good than anonymous users who are only capable of hitting \"bury\" :) :) :)"
    author: "birdie"
  - subject: "Agreed"
    date: 2009-06-19
    body: "I agree, you've taken the time to fill out quite a number of bug reports. Yay for you! That's a great contribution.\r\n\r\nI think the criticism was triggered by your \"Why bugday?\" comment: it should be obvious that it's very helpful for the developers to have someone else first go through and pick out the still-valid bugs, because it saves them quite a lot of time. Implying otherwise just annoyed people.\r\n\r\nBut I'm sure your entire intent was merely to advertise the bugs you wish to see fixed (because they affect you personally). A more productive and certain path is to participate in the bugday, of course.\r\n"
    author: "tangent"
  - subject: "My own words in my own mouth"
    date: 2009-06-19
    body: "Actually I'm not pretending everything is fine, thanks.  As a regular end-user, everything IS fine on all the web pages I use in the distro I use.  What's more the UI fits me better than any alternative.  I love Konqueror under the sand, over the sand, and near the sand.  Even without sand.\r\n\r\nThanks for finding and reporting bugs through appropriate channels and taking advantage of bug day as a time when several volunteers rally to help make both our lives better.  They do a great job."
    author: "Jerzy Bischoff"
  - subject: "Fanboyz and Promote/Bury don't work!!"
    date: 2009-06-22
    body: "It appears that there are self appointed internet police that immediately bury any comments that are not positive and seem to promote the replies to them.\r\n\r\nThis makes discussion impossible.  Just look at the current comments for this item.\r\n\r\nSo, I suggest that although the ratings could be kept -- no point in that really since they do not represent actual public opinion -- either the deletion of comments should be eliminated or the number of negative votes needed to do so should be greatly increased."
    author: "KSU257"
  - subject: "Kind of true"
    date: 2009-06-23
    body: "You have a point that people don't know how to use the moderation system. You should bury trolls and moderate very interesting comments up. You should not bury a comment just because you don't agree with it!\r\n\r\nThat said, comments are not being deleted are they? If you click on them you can see the content. "
    author: "jadrian"
  - subject: "Re:Fanboyz and Promote/Bury don't work!!"
    date: 2009-06-23
    body: "> It appears that there are self appointed internet police that immediately bury any comments that are not positive and seem to promote the replies to them.\r\n\r\nI'm uncertain whether hooray-type comments should be voted up. But they increase the developers' motivation, which is arguably a Good Thing.\r\n\r\nOn the other hand, I object when it comes to burying comments. Most buried comments I saw were buried for a good reason, because they have not read the news or were just ranting pointlessly.\r\n\r\nFor example, birdie said: \"Why bugday? I have a lot of bugs/feature requests/regressions on my watch list even without it!\" This comment was correctly buried, because it shows that he has not read past the first two paragraphs of the article. Otherwise he would have known that Bugday is not about searching for yet more bugs (which Konqueror has plenty of, sadfully, because it is a big project). Bugday means triaging: sorting out invalid or resolved bugs and adding test cases to existing bug reports. This makes bugfixing much easier for the developers because they find the important bug reports quicker and have more information to work on them more efficiently.\r\n\r\nBirdie then continues sarcastically: \"Oh, yes, let's bury our heads in the sand and pretend that everything's fine, but in my case I prefer to file bug reports and help the community.\" Bug reports help certainly, but it is quite small-minded to rant on everybody who wants to thank the developers for their efforts, just because some bugs have not yet ironed out, which possibly only exist in their particular configurations. (I generalize, as I did not have the time to read through all bug reports that Birdie mentioned.)"
    author: "majewsky"
  - subject: "Yet another bug?"
    date: 2009-06-24
    body: "<q>That said, comments are not being deleted are they? If you click on them you can see the content.</q>\r\n<p>\r\nI note with a certain irony here that if you are using Konqueror 4.2.92 that you can't do that because it doesn't work -- another new bug I presume? :-D</p>"
    author: "James Tyrer"
  - subject: "> I'm uncertain whether"
    date: 2009-06-25
    body: "> I'm uncertain whether hooray-type comments should be voted up. But they increase the developers' motivation, which is arguably a Good Thing.\r\n\r\nExactly! Keep up the good work, guys! https is just for geeks, not ordinary users like me!\r\nYou guys rule! W00t w00t! ;)"
    author: "kolla"
  - subject: "Works in KDE 3.5.10, but only if server reachable"
    date: 2009-06-27
    body: "It works in KDE 3.5.10, but if the Internet connection broke, clicking on the header will not show the comment, nor an error message. It will just silently fail and do nothing, unlike if you type an URL in the address bar and press enter, in which case you do get an error message."
    author: "odalman"
  - subject: "> Exactly! Keep up the good"
    date: 2009-06-30
    body: "> Exactly! Keep up the good work, guys! https is just for geeks, not ordinary users like me! You guys rule! W00t w00t! ;)\r\n\r\nThis is exactly what I mean. You do not contribute anything to the discussion (as in: we all know that Konqueror has problems). The only thing you do is driving potential contributors away through your harmful behavior."
    author: "majewsky"
  - subject: "Desired behavior"
    date: 2009-06-30
    body: "This is actually a bug of the JavaScript code that dot.kde.org uses. The behavior you describe (silent fail of AJAX operations) is actually the common way to implement it, and not adopting this behavior might lead to myriads of senseless error messages on other sites."
    author: "majewsky"
---
As we get ready for the 4.3 release, it's time to have another BugDay! We are going back to basics this time, and doing Konqueror. 

All you need to join is yourself and a recent copy of KDE (trunk is best, but you can also use a Neon nightly or a snapshot from a distribution). You do not need to know how to program! We will teach you all you need to know. This is a fun and easy way to get involved with the KDE community! Working with BugSquad is a great way to discover KDE, especially if you would like to help contribute, but aren't sure where to begin.

We'll be doing various things:
<ul>
<li>Our standard bug triage (sorting through bugs, verifying and checking them)</li>
<li>Writing JavaScript/CSS test cases</li>
<li>Checking various important websites to see how well they work</li>
</ul>

All of these items will help developers more than you might imagine!

Again, you don't need to know how to program, although if you know JavaScript/CSS, then you can help with test cases. 

Where? On #kde-bugs on irc.freenode.net
When? June 20th, UTC till our last timezone goes to sleep.