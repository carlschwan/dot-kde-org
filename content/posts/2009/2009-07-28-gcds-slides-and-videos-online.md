---
title: "GCDS Slides and Videos Online"
date:    2009-07-28
authors:
  - "jriddell"
slug:    gcds-slides-and-videos-online
comments:
  - subject: "Keynotes MIA?"
    date: 2009-07-28
    body: "Robert Lefkowitz, Walter Bender and RMS did speak, didn't they?"
    author: "kragil"
  - subject: "Technical Problems"
    date: 2009-07-28
    body: "Unfortunately due to some technical problems not all talks were successfully recorded. Those available are all of the ones that did."
    author: "sealne"
  - subject: "Bummer, but thanks! I am"
    date: 2009-07-28
    body: "Bummer, but thanks! I am grateful for every video I get. "
    author: "kragil"
  - subject: "Where I can download"
    date: 2009-07-28
    body: "Where I can download it?\r\n\r\nChris\r\n[SPAM REMOVED]"
    author: "burev1982"
  - subject: "Here:"
    date: 2009-07-29
    body: "Here: http://www.geeksoc.org/gcds/\r\n\r\nP.S. Thanks for putting up all these talks. I have watched about five so far and they are really excellent! :)"
    author: "mutlu"
  - subject: "Conference videos"
    date: 2009-07-29
    body: "Thanks! Some great talks in there.\r\n\r\nAlthough the quality of the production is quite good on these videos (we can see the screen and the speaker at the same time, for example), getting this process right from start to finish is quite difficult and not always completely successful in free software conferences. \r\n\r\nA number of times I have thought about whether it would be useful to write some documentation about how to record, edit and distribute conference videos to assist people who haven't done it before (and with free software conferences often being hosted in different cities and therefore by a different group of volunteers, this is quite a common occurrence). It could cover techniques, dos and don'ts, recommended free software tools, etc. I'd love to do this, and I don't mind writing documentation; the only snag being that whilst I do have some ideas about what works and what doesn't in the result, I don't actually have any concrete experience in this area. Perhaps I could start something and then others could help to complete it?"
    author: "bluelightning"
  - subject: "Go ahead..."
    date: 2009-07-29
    body: "... it certainly won't harm. And I love watching those videos, as it gives an entirely perfect idea.\r\n\r\nI absolutely loved the Social Desktop Talk (and the guy holding it appeared to be very social :-). \r\n\r\nBefore seening that I thought it's only Vaporware, and probably it is very much, but now I understand that it will change our position in the market dramatically as well as our support interaction and that there is somebody who is not just after announcement clicks, but trying to establish a Free Social Network.\r\n\r\nYours,\r\nKay\r\n\r\n"
    author: "kayonuk"
  - subject: "what license"
    date: 2009-07-30
    body: "I want to know what license the videos are under; I wanted to repost some of them on blip.tv.  Does anybody know if this is permitted or what I would have to do to get the videos reposted.  "
    author: "redsteakraw"
  - subject: "It says in license.txt:"
    date: 2009-07-30
    body: "\"All videos are licensed as: http://creativecommons.org/licenses/by-sa/3.0/ \"\r\n\r\nHope that helps. :)"
    author: "Hans"
  - subject: "Thanks so much for these! "
    date: 2009-07-30
    body: ":)"
    author: "peller"
  - subject: "Power management"
    date: 2009-07-30
    body: "Matthew Garrett basically tore all the GUI devs a new one ;) .. I liked it."
    author: "kragil"
  - subject: "I can help"
    date: 2009-07-31
    body: "I do have a little experience with video processing free software tools. I can help a bit bluelightning. Maybe it would be good to start this on the OpenDesktop.org platform so more people would be able to join and contribute as well.\r\n\r\n"
    author: "jaom7"
  - subject: "Great!"
    date: 2009-08-03
    body: "Great stuff. I've started making a few notes, but what I think we really need is a wiki somewhere so that we can collaboratively work on the document. I can't see that Opendesktop.org has a wiki function, so maybe somewhere else would be more appropriate? \r\n\r\nIn fact, maybe if there was a larger wiki talking generally about how to run a conference, this could be a section within it. Perhaps that's more something for the future though, initially the document could be anywhere with a wiki."
    author: "bluelightning"
  - subject: "thanks for the"
    date: 2009-08-13
    body: "thanks for the slides/vids! \r\nnote on the bluetooth talk (Bastien Nocera): audio only comes through the left channel."
    author: "mxttie"
  - subject: "thanks geeksoc"
    date: 2009-08-14
    body: "for providing so much bandwidth! it was really a pleasure downloading the vids when you only have to wait a few seconds :)"
    author: "mxttie"
---
The available slides and videos from GCDS are now available for download.  Grab the <a href="http://people.canonical.com/~jriddell/gcds-presentations-2009/">slides</a> to catch up on over 40 of the best talks, and <a href="http://www.geeksoc.org/gcds/">get the videos</a> to over 50 enlightening presentation.  Thanks to <a href="http://www.geeksoc.org/">GeekSoc</a> for hosting and thanks to the team from KDE who manned the cameras.
