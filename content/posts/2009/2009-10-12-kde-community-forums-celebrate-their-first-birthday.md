---
title: "KDE Community Forums Celebrate Their First Birthday"
date:    2009-10-12
authors:
  - "neverendingo"
slug:    kde-community-forums-celebrate-their-first-birthday
comments:
  - subject: "Which tool to draw timeline?"
    date: 2009-10-13
    body: "Which tool did you use to draw this nice timeline?"
    author: "unormal"
  - subject: "Re: Which tool to draw timeline"
    date: 2009-10-13
    body: "Actually it was just inkscape. Thanks to Mogger for providing it :)"
    author: "neverendingo"
  - subject: "Source"
    date: 2009-10-13
    body: "If you want the source (svg), just poke me on:\r\n<img src=\"http://hanswchen.wordpress.com/files/2009/07/email.png\" width=\"120\" height=\"18\" />"
    author: "Hans"
---
<div>
<img src="http://dot.kde.org/sites/dot.kde.org/files/cake.png" style="width: 193px; height: 85px; float: right; margin-left: 1em; margin-right: 0px;">
<div><a href="http://www.kde.org/announcements/forum.php" >Exactly one year ago</a>, on October 12th, the <a href="http://forum.kde.org">KDE Community Forums</a> were founded. It was about time to give users a place for discussion and support beside the mailing lists, which were mainly used by developers and other contributors. A lot of time has passed since then, and the forums have grown into a healthy community, contributing to the KDE landscape as a whole. Here people can ask KDE-related questions, help other users, find useful information, or just discuss whatever comes to mind.</div>
<!--break-->
</div>
The KDE Community Forums started as a two-person endeavor, but members from various backgrounds in the KDE community soon joined: as of today, the staff includes four administrators, five global moderators and a plethora of forum moderators. At the same time, the forums have acted as a foundation for new initiatives, the most famous one being the KDE Brainstorm. In Brainstorm forum members can suggest improvements to KDE, as well as vote on and discuss other users' ideas. Other, similar activities have included the Klassroom (week-long tutorial sessions) and the recently launched KDialogue. The forums evolved to accomodate the needs of their users, getting a brand new look and more features.

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:500px;">
<a href="http://dot.kde.org/sites/dot.kde.org/files/timeline.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/timeline_thumb.png" /></a>
A timeline showing major events in the history of the KDE Community Forums
</div>
<div>
Right from the start there were already some application-specific forums, for example for <a href="http://forum.kde.org/viewforum.php?f=69">KMyMoney</a>. Later a special forum for <a href="http://forum.kde.org/viewforum.php?f=74">Eigen</a> was also added on request. A big milestone was the merge with the largest existing KDE application forum at that point, the <a href="http://forum.kde.org/viewforum.php?f=127">Amarok forum</a>. Other applications (such as <a href="http://forum.kde.org/viewforum.php?f=96">KOffice</a> and <a href="http://forum.kde.org/viewforum.php?f=136">Krita</a>) also found their home in the forums. These are all moderated by their respective developers, with support from the forum team.
</div>
As of today, the forums have 18397 registered members and no less than 71001 posts in 15246 topics.<br>
What will the future bring for the KDE Community Forums? There is still room for expansion, and the team welcomes any constructive feedback. You can leave a message in the <a href="http://forum.kde.org/viewforum.php?f=9">Feedback &amp; Questions forum</a> or talk to us directly on IRC in <a href="irc://irc.kde.org/kde-forum">#kde-forum</a> on Freenode.

So, happy birthday to the forums. We hope you enjoy them as much as we do!