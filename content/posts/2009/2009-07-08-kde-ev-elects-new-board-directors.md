---
title: "KDE e.V. Elects New Board of Directors"
date:    2009-07-08
authors:
  - "sebas"
slug:    kde-ev-elects-new-board-directors
comments:
  - subject: "Other board members ?"
    date: 2009-07-08
    body: "Sorry, stupid question, who are the other (\"old\") board members ?\r\n\r\nAlex\r\n"
    author: "aneundorf"
  - subject: "Yes..."
    date: 2009-07-09
    body: "Aaron Seigo is out, this are great news, his arrogance as president was the main reason a stopped using KDE an switched to GNOME."
    author: "woootan"
  - subject: "Right..."
    date: 2009-07-09
    body: "So, you stop using a KDE, not because KDE isn't good, but because you don't like somebody who is president of a foundation that does little more than secure some legal aspects of the KDE project? That sounds pretty insane to me. The e.V. has nothing to do with the actual contents or workings of KDE; the members of the board are just well known and trusted members of the KDE community. And yes, Aaron is such a member, at least IMHO. \r\n\r\nAaron, thanks for your work for the e.V., and KDE in general!"
    author: "andresomers"
  - subject: "You have no idea what great"
    date: 2009-07-09
    body: "You have no idea what great things Aaron did and will continue to do for KDE, do you?"
    author: "nightrose"
  - subject: "Cornelius, Sebas and Ade."
    date: 2009-07-09
    body: "Cornelius, Sebas and Ade."
    author: "nightrose"
  - subject: "Apart from trolling and"
    date: 2009-07-09
    body: "Apart from trolling and giving a bad name to KDE, tell me all those \"Amazing\" things he has done."
    author: "woootan"
  - subject: "You are the one who is"
    date: 2009-07-09
    body: "You are the one who is trolling ..."
    author: "fyanardi"
  - subject: "Parallel Universe"
    date: 2009-07-11
    body: "Eh, yes... because Gnome is the home for concerned citizens, from Guadec:\r\nhttp://opensourcetogo.blogspot.com/2009/07/emailing-richard-stallman.html\r\n;-)\r\n\r\nWhile it is entertaining to watch that I am glad that I have never seen communication like this from the KDE developer blog community. Somehow they always manage to focus on running code and fixing our problems, and avoid public confrontation. Fortunately, you don't have to get in touch or agree with developers cultures or listen to the developers, just use the software as it suits you. Which does not hinder you to be more respectful to Aaron Seigo. Association management is bureaucratic work, you need to consider the public benefit status and so on, an office like this is a heavy burden. Who takes the burden needs to be treated like a hero. "
    author: "vinum"
---
<a href="http://ev.kde.org">KDE e.V.</a>'s Annual General Meeting was held today during the <a href="http://www.grancanariadesktopsummit.org">Gran Canaria Desktop Summit</a> in Las Palmas de Gran Canaria, Spain. The KDE e.V. is the association that provides representation, support and governance to the KDE community. After former board member Klaas Freitag and KDE e.V. president Aaron Seigo stepped down and vice president Adriaan de Groot's term ended, three open positions had to be filled. Adriaan de Groot was re-elected as vice president of KDE e.V. and two new board members have been elected.

Celeste Lyn Paul, the leader of KDE's usability efforts, and Frank Karlitschek, the man behind KDE community web services such as KDE-Look.org and KDE-Apps.org. Karlitschek is also known for his efforts to introduce the Social Desktop to KDE. The new president of the KDE e.V. is Cornelius Schumacher, and Frank takes over the role of vice president and treasurer from Cornelius.
<!--break-->
Cornelius Schumacher says <em>"I'm honored and proud to serve KDE e.V. in the board and as president. It is an organisation driven by the community and the strength and growth of this community is a promise for the future of the Free Desktop and Free Software in general."</em> Celeste Lyn Paul adds, <em>"as a member of the board I will focus on supporting design and usability efforts in KDE. These have been an essential part of the KDE 4 success story and we will continue in this direction."</em> Frank Karlitschek notes <em>"KDE, while offering great technology has not yet deployed its full potential in the market place. My personal passion is to drive this further and enable the community to deliver on the potential of the KDE platform."</em>