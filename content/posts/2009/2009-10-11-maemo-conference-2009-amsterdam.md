---
title: "Maemo Conference 2009 in Amsterdam"
date:    2009-10-11
authors:
  - "jospoortvliet"
slug:    maemo-conference-2009-amsterdam
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/KDE people.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE people_small.jpg" width="200" height="112"/></a><br />
KDE people @ Maemo</div>
Last weekend Amsterdam was visited by hackers from various Free Software communities and companies from around the world. Brought together by the Maemo Summit in the WesterGasFabriek, they gave and attended talks about topics like the Maemo applications, user interface components, the underlying infrastructure and of course the future of Maemo. Read on for a short impression of this conference.
<!--break-->
Since the decision to move to Qt for the UI in the next release of Maemo, Harmattan, the KDE community has gotten more involved with Maemo. Due to the use of many frameworks initially developed within the Gnome community, Maemo presents an interesting mix of technologies which has the potential to infuse new ideas in the Free Desktop. The Gran Canaria Desktop Summit this year already made clear how both communities can work together and how the whole can be more than the parts. 

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:400px;">
<a href="http://dot.kde.org/sites/dot.kde.org/files/dave neary.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/dave neary_small.jpg" width="400" height="224" /></a>
Dave Neary talking about documentation in the main hall
</div>

<div style="float:right; padding: 1ex; margin: 1ex; border: thin solid grey;">
<a href="http://dot.kde.org/sites/dot.kde.org/files/drinks.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/drinks_small.jpg" width="300" height="168"/></a><br />
Cocktails from Skype!
</div>

There are people here from various KDE projects like <a href="http://amarok.kde.org">Amarok</a> and <a href="http://koffice.kde.org">KOffice</a> but also KDE related companies like <a href="http://www.kdab.com/">KDAB</a> and <a href="http://www.basyskom.de/index.pl/enhome">BasysKom</a>. They mingle with the Maemo community, and the location is perfect for that. In the community area, but also in the main ("N900") hall you can find besides the usual tables and chairs beanbags, plenty of power outlets, wifi and plenty of food and drinks. The Westergasfabriek is certainly a very interesting location, especially the main hall - the photos below certainly don't do justice to the industrial environment.  We're being taken care off very well, that's for sure.


Of course Nokia made sure there would be enough to do - we had a party on every evening, with various amounts of free drinks and food. And Amsterdam has been a great host city - living up to its reputation. All in all, this conference is a nice start for bringing the KDE community and technology to the Maemo platform. Expect a few more articles to come out soon!