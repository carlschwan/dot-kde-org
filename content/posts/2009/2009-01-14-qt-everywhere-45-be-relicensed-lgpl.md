---
title: "Qt Everywhere: 4.5 To Be Relicensed As LGPL"
date:    2009-01-14
authors:
  - "jriddell"
slug:    qt-everywhere-45-be-relicensed-lgpl
comments:
  - subject: "Sweet!"
    date: 2009-01-14
    body: "Sweet! I was unsure about what the long term implications of Nokia buying Trolltech would be at the time when it was first announced, but this is certainly a very positive outcome!"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: Sweet!"
    date: 2009-01-14
    body: "There is available FAQ what this change really means for developers/companies:\nhttp://www.qtsoftware.com/about/licensing/frequently-asked-questions"
    author: "patpi"
  - subject: "Re: Sweet!"
    date: 2009-01-15
    body: "Yes, this is is the optimal license solution! A very open toolkit that allows open and closed applications. I been waiting/hoping for this for years. Now (soon) there is no reason not to use Qt :-) This killed any interest I had in learning gtk..."
    author: "I'm off to learn Qt"
  - subject: "Great news!"
    date: 2009-01-14
    body: "Thanks a lot Nokia!\n\nI'm very excited about this news, Qt is such a great toolkit that everyone should use it :-)\n\nCongratulations!"
    author: "Thorben"
  - subject: "Re: Great news!"
    date: 2009-01-14
    body: "Awesome news! I always understood the reason for Qt's previous licencing policies and never imagined this day would come. Congratulations to Nokia and Qt Software for their investment in the Free Software community."
    author: "Paul Eggleton"
  - subject: "whoa"
    date: 2009-01-14
    body: "This is big. This is big. Dude! (!!)"
    author: "Jakob Petsovits"
  - subject: "THIS IS HUGE!"
    date: 2009-01-14
    body: "This is really huge announcement. It is hard to predict real impact of this decision at the moment but effects of licensing under LGPL and GPL3 will be HUGE for all KDE. I think this really has a great chance to \"lead to a veritable explosion of Qt and KDE adoption.\"\n\nnow KDE has everything important:\n- open source licence\n- possibility to use KDE technology also to gain some money if needed\n- cross platform capabilities\n- flexible technology from KDE4\n- and as always great ideas and contributors\n\nnext step really could be world domination. Now only lets convince downstream to use KDE technology as base for desktop development. Everywhere, as base for desktop oriented distributions - I'm looking at you Canonical - , as base for netbooks, as base for enterprises (KDE PIM everywhere ;] ) , etc\n\nI think those news are great ones!     \n\n\n\n\n\n "
    author: "patpi"
  - subject: "Re: THIS IS HUGE!"
    date: 2009-01-14
    body: "I understand what you mean by \"now KDE has everything important\". Nevertheless, I want to clarify that KDE had all of the points you mentioned already since Qt was released under the GPL on all platforms. The Kolab Konsortium very successfully earns money by extending Kontact with features needed by their customers since more than 6 years. So the \"possibility to use KDE technology also to gain some money if needed\" exists since a long time already.\n\nThe only thing that is new (and that you most likely meant) is that starting with Qt 4.5 and KDE 4.3. one can develop proprietary, closed source software using Qt and KDE without the need to buy a commercial license for Qt."
    author: "mahoutsukai"
  - subject: "Re: THIS IS HUGE!"
    date: 2009-01-17
    body: "The real benefit for Linux users is that, as others have said, now competition can be solely on technical merits.\n\nThat means that integrators no longer have \"We can't develop our closed-source corporate offerings in it cost-effectively\" as a justification for dismissing Qt and KDE desktops in favor of GTK+ and GNOME/Xfce desktops."
    author: "Stephan"
  - subject: "Re: THIS IS HUGE!"
    date: 2009-01-14
    body: "The LGPL makes a bigger impact in the commercial/proprietary scene, and not as much for KDE. That's because Qt was already Free Software (GPLv3+exceptions). The biggest impact for us, I think, is that it will silence those few critics that claimed GPL+exceptions was not free enough. Now Qt can be compared to GTK+ and other toolkits on the basis of technical merit, instead of mere licensing.\n\nHere's a good overview of the situation: http://www.ics.com/files/docs/Qt_LGPL.pdf"
    author: "Brandybuck"
  - subject: "But..."
    date: 2009-01-14
    body: "But will this mean Nokia will reduce the number of developers working on QT in the hope there will be other people coming up to work on for free?\nEspecially in hard times like this: no revenue -> less support from upper management -> less people...\n\nA \"commitment\" from upper management on anything is usually not worth the pixels showing it on the screen...\n\nI hope it will turn out fine...\n(Sorry for being a bit negative -> but it wouldn't be the first time)"
    author: "Norbert"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "I don't think you realise just how profitable Nokia is - they probably spend more on lunches for executives than they do on Qt :)"
    author: "Anon"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "We are actually working actively to hire more developers here at Qt Software."
    author: "Aron Kozak"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "Just out of interest - do you know if Nokia is planning to sponsor some more developers to work on KDE itself? It's a great advert for Qt and, considering how far it's come on a shoestring budget, and addition of even 3 or 4 full-time developers could give a huge return on investment."
    author: "Anon"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "\"It's a great advert for Qt\"\n\nNO - Since KDE4 it is exactly the opposite.\n"
    author: "KDETroll"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "Then all the more reason for Nokia/Qt to spend some money making it better :)."
    author: "slacker"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "We in Qt Software beg to differ: http://labs.trolltech.com/blogs/2008/12/04/how-kde-4-is-blocking-qt-45/\n\nWe respect your opinion, but ours is that KDE 4 is a great showcase. So, we will act accordingly."
    author: "Thiago Macieira"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "\"So, we will act accordingly.\"\n\nI'm tempted to interpret this as a \"yes\" to my original question ;)"
    author: "Anon"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "No, Nokia will *increase* the number of Qt developers:\n\nhttp://labs.trolltech.com/blogs/2009/01/14/nokia-to-license-qt-under-lgpl/\n\nNokia realizes that a good software platform is key to the success of the telephones and Qt is their choice of platform.\n\nQt Creator is another initiative Nokia has taken to make it easy for developers to start developing for the Nokia platform. The goal is that software created like that will run on Windows, Linux, Mac *and* Nokia phones.\n"
    author: "Jos"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "I hope they really realize that. There are so many mobilephones that are great. Great if you look at the hardware but if you look at the software many suck so hard that you wonder how they made it to the market.\nMy mobilephone e.g. can do so _much_ but when it comes to navigating the menus it is slow as hell and sometimes I encounter bugs like shutting it down though there is enough battery power left etc.\n\nA good foundation one can reuse and thus improve and something that would be really used would change a lot.\n\nImo most people do not want to only look at a phone but also use it, I hope that Nokia will improve the \"use\" part with this move. :)"
    author: "mat69"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "Actually, the situation hasn't really changed for commercial (paying) developers. Although it opens up options for them as well, the commercial Qt license says you cannot use the open source (LGPL or GPL) version of Qt to start developing a commercial program, and then switch to the commercial license.\n\nSo that means commercial developers still *need* to pay for a commercial license if they don't want to be \"stuck\" with the LGPL or GPL licenses for their programs."
    author: "HelloWorld"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "True, but on the other hand, there's not much to be \"stuck\" about with the LGPL. It offers most freedoms that even proprietary developers would require, you just can't modify Qt itself without releasing those changes into the wild.\n\nBut that's about the only thing where you're \"stuck\" with the LGPL, there's now little reason to buy a license unless you need the commercial support services. I would be interested in the reasons that you consider important enough so that developers \"still *need*\" to buy a license."
    author: "Jakob Petsovits"
  - subject: "It depends. LGPL got restrictions"
    date: 2009-01-14
    body: "When creating a device that will include LGPL and proprietary components, you need to ensure that you comply with the following restrictions in the LGPL:\n\n-- Modifications made to the library itself must be licensed under the terms of the LGPL \n\n-- A proprietary application that is a \u0093work that uses the library\u0094 needs to be dynamically linked with the library in order to avoid becoming a combined work that results in a licensing incompatibility\n\n-- Users need to be aware that an executable might be a combined work with the library unless a suitable shared library mechanism is used (i.e., one that, at runtime, uses a copy of the library already present on the user\u0092s computer system (rather than copying library functions into the executable), or one that will operate properly with a modified version of the library (if the user installs one as long as the modified version is interface compatible with the version that the original was made with)."
    author: "Knut Yrvin"
  - subject: "Re: It depends. LGPL got restrictions"
    date: 2009-01-16
    body: "Yes, this license incompatibility is a serious issue.  IMHO, the FSF has a point, but in some cases they are shooting themselves in both feet, and it is hurting Linux.  \n\nClearly, the nVidia drivers are illegal under any type of GPL and there are issues with printers where there are proprietary parts of the driver that can not be LGPLd or GPLd.  Then there is the ZFS which Linux can't use in Kernel space.\n\nBasically, I can see why linking to non-free software should be forbidden in general, but why should the LGPL and GPL forbid linking to other libraries which are free and open -- free and open software being any software where you can get the source code for free.\n\nThen there need to be exceptions.  How should the FSF licenses handle things like the Epson printer drivers.  They are free and you can download the source code, but it comes with binary libraries.  If the binary code is available free, that isn't as free as if you could get the source code (it isn't open), but free is free and the FSF license being pickey about minor points is not advancing the cause of FOSS.  There should be exceptions in the LGPL & GPL for other types of free software.  Whether or not the software is free (as in beer), is more important than the details of the license.  That is my $0.02."
    author: "JRT"
  - subject: "Re: It depends. LGPL got restrictions"
    date: 2009-01-17
    body: "Even without being as much a zealot as RMS, I can still understand the problem with that. There's no guarantee that code will remain ABI compatible as systems march onward. Windows provides that guarantee to some extent, but a big part of Linux's architectural strength comes from an assumption that old APIs and ABIs can be phased out and new ones phased in.\n\nGPLing the kernel, for example, ensures that anything legally distributed as a part of it will have source code that can be adapted to new hardware platforms or to satisfy new requirements. The kernel's API is probably one of the least stable APIs on a linux system, but even more typical stuff changes. For example, the ABI for libstdc++ changed a couple of years ago and now, only a handful of apps require the old version of libstdc++ to be installed."
    author: "Stephan"
  - subject: "Re: It depends. LGPL got restrictions"
    date: 2009-01-18
    body: "I don't see your point.  Obviously it wouldn't apply to software that was open but not GPL since the source code would be freely available.  This is the main issue is whether the GPL should forbid linking to other types of open software. \n\nThere are serious issues with the ABI of the nVidia Kernel drivers and having a GPL Kernel doesn't solve them.\n\nI suggested that there be a singular exception that GPL or LGPL code be allowed to be combined with binary libraries.  Since there is no alternative in the case of printer drivers, the problems are simply something which we would have to live with.  In this case, the problem you mention is very unlikely to occur since the ABI (to either GhostScript or GIMPPrint) is in the GPL part of the code."
    author: "JRT"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "PLease explain to me what (as long as I don't want to alter the Qt libs) is the difference to me as a closed-source developer between the LGPL and commercial licences in effect.  As I read it I can distribute my code completely closed if I use either of those licenses?"
    author: "Curious"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "The difference is a certain amount of money and a good deal of support."
    author: "boudewijn"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "Obviously this is not the place for legal advice. You should read the LGPL or have a lawyer interpret it for you, so you don't expose your business to unnecessary risk.\n\nThe LGPL is more liberal than the GPL, sure, but it's still a far cry from \"free, free, free\". There are still obligations to fulfill, limitations, etc. The most mentioned one is the relinking clause.\n\nIn the end, we expect to a certain switch from commercial, closed-source applications to the LGPL, closed-source model. And we expect there to be creation of Qt applications directly into the LGPL/closed-source model (the \"shareware / freeware market\" that we previously didn't address)"
    author: "Thiago Macieira"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "If all you do is dynamically link with Qt, then you can use LGPL Qt for your proprietary projects. But it doesn't apply to static linkage, modifications to the library itself, and some other cases. Also, Qt Extended (Qtopia) remains GPL. I think most embedded developers will stick with the commercial license, as well as a few application developers."
    author: "Brandybuck"
  - subject: "Re: But..."
    date: 2009-01-15
    body: "> Also, Qt Extended (Qtopia) remains GPL.\n\nFTFA:\n\n\"Nokia today announced that its Qt cross-platform User Interface (UI) and application framework for desktop and embedded platforms will be available under the Lesser General Public License (LGPL) version 2.1 license from the release of Qt 4.5, scheduled for March 2009.\""
    author: "kubunter"
  - subject: "Re: But..."
    date: 2009-01-15
    body: "Qt != Qt Extended\n\nGo read the FAQ: \"Does the licensing change apply to Qt Extended?\nNo, the licensing change does not apply to Qt Extended.\" My interpretation is that Qt (included Qt/Embedded (previously known as QtopiaCore)) will be LGPL, but that Qtopia Platform (Qt Extended) will remain Dual/GPL.\n"
    author: "Brandybuck"
  - subject: "Re: But..."
    date: 2009-01-18
    body: "Yeah, well... but who cares about that POS?\nSeriously, Qtopia only makes me depressed."
    author: "Kolla"
  - subject: "iPhone killer"
    date: 2009-01-14
    body: "What I've been waiting for is for Nokia to release a killer piece of hardware to showcase Qt and KDE as a fully-featured mobile computing platform.\n\nI keep drooling over the n810 tablet, but it doesn't have enough memory.  The n96 is big, clunky, and doesn't have the resolution necessary to run decent apps (not to mention really expensive, and runs Symbian)\n\nIf Nokia were to unleash a new smart phone and/or tablet with a high resolution OLED screen and decent memory (like the rumored G2 specs, and the rumored iPhone HD specs) and showcased KDE 4 and Qt running on top of it, I think Nokia could get back in the game."
    author: "T. J. Brumfield"
  - subject: "Re: iPhone killer"
    date: 2009-01-18
    body: "If Nokia wants to get back in the game, they should stop dicking around and deliver phones with operating system that actually works well (as in stable), are easy to set up and use (not that damn inconsistant and confusing menu hell of S60v3), quick and responsive UI, rotate between apps using single tap/key input, dont crash because of third party apps etc. etc.\n\nI dont see KDE4 solving anything here, it's way too bug ridden to be of any use on a phone, and plasma would eat out the battery. "
    author: "Kolla"
  - subject: "Re: iPhone killer"
    date: 2009-01-18
    body: "Yet KDE4 runs on the Nokia n810 with minimal memory."
    author: "T. J. Brumfield"
  - subject: "Re: iPhone killer"
    date: 2009-01-18
    body: "\"Runs\" is being rather charitable, I think: \"hobbles\" might be more accurate."
    author: "Anon"
  - subject: "Re: iPhone killer"
    date: 2009-01-20
    body: "Most of KDE wouldn't really be useful on a phone, but Plasma would be. And saying it would drain your battery - hah! If I run powertop on my laptop, I see firefox, firefox and firefox. And I run plasma with lots of applets, compositing kwin and a bunch of KDE apps. Numbers are even better: \nWakeups per second:\nFirefox: 139 + 14.9 (shows up twice)\nKWin 9.4\nPlasma 4.2\n\nwhen I close firefox, my powerusage drops by 3 watt (acer aspire one)...\n\nI doubt using plasma on mobile devices and things like the N810 would INCREASE poweruse. I actually expect it to go down."
    author: "jospoortvliet"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "Nokia/Trolltech is busily hiring new engineers. Their goal is \"Qt Everywhere\", to make it the expected infrastructure for software development. You don't get there with fewer developers. The engineering focus will still be there.\n"
    author: "Brandybuck"
  - subject: "Re: But..."
    date: 2009-01-14
    body: "Cool, thanks for all your answers..."
    author: "Norbert"
  - subject: "Re: But..."
    date: 2009-01-15
    body: "Hard times? Nokia paid 100 million Euros for Trolltech. Why? So they could give it away? So they could rake in license fees?\n\nI saw the new head of Qt software speak at last years KDE conference and I don't think I could have been more impressed that someone could be honest, intelligent and totally familiar with the business aspects while being sincerely interested in being a good part of the community. However the true test of a buyout is to see how the employees deal with it. On that count I can only say if I wanted to work full time on software I'd be hitting them up. \n\nWhat is a commitment worth? See for yourself. No sane company says one thing, does another and expects to stay at the top of their industry. Nokia has made it to the top of several industries and as an 100 year plus company they didn't start with cell phones.\n\nAs to their reasons for buying Qt. Clearly it fits in their strategy for having the best tools available and for advancing a technology climate where they are riding the crest of the wave and not following the pack. This move is EXTREMELY logical for them. "
    author: "Eric Laffoon"
  - subject: "Great"
    date: 2009-01-14
    body: "I think this solves the license issues for a Qt-plugin for SWT and wxWidgets. This means we could probably see a few more well integrated applications on the KDE desktop. Yeah!"
    author: "kdefan"
  - subject: "Re: Great"
    date: 2009-01-14
    body: "Oh, wow --- is that THAT why wxWidgets never supported Qt?  I see :)\n\nAnyway, yes, this is great news.  Not only will it make Qt and KDE more attractive to corporate users, it might boost development for both, encourage more cooperation and code sharing with GNOME, etc.  Great stuff."
    author: "Lee"
  - subject: "Re: Great"
    date: 2009-01-14
    body: "There's also that Qt already is a C++ API and much nicer than Wx to boot. It's like wrapping a faberge egg with an old newspaper."
    author: "Roberto Alsina"
  - subject: "Re: Great"
    date: 2009-01-14
    body: "With a LGPL Qt, there's absolutely no reason to use wxWindows that I can think of, other than just refusing to learn something new."
    author: "Brandybuck"
  - subject: "Re: Great"
    date: 2009-01-14
    body: "QtCore being GPL was also a problem for system-level libraries, like e.g. libdbus, libz, libpng etc.\nE.g. if libdbus would have used QtCore, no closed source application could have used it without buying commercial Qt licenses.\nQt 4.5 being LGPL solves this issue :-)\n\nThis is really great news ! :-)\n\nAlex\n"
    author: "Alex"
  - subject: "Re: Great"
    date: 2009-01-14
    body: "libdbus doesn't use Qt due to the license, given that it doesn't even use glib. Its just designed to be a very low-dependency system library."
    author: "Ian Monroe"
  - subject: "Re: Great"
    date: 2009-01-14
    body: "Yes, it was just some example.\n\nAlex\n"
    author: "Alex"
  - subject: "Re: Great"
    date: 2009-01-14
    body: "> is that THAT why wxWidgets never supported Qt?\n\nNo.\nMore likely because nobody with good knowledge of both wanted to spend time on this for very little benefit.\n\nSame for SWT.\n\nA common misconception though, usually comes up when Qt licence changes or is discussed"
    author: "Kevin Krammer"
  - subject: "Re: Great"
    date: 2009-01-14
    body: "\"No\"? Wow, that answer surely sounds well documented.\n\"More likely nobody wanted..?\" I see, you don't have the faintest clue, or live in refusal of the fact. But don't you worry, it's over now.\n\nI once read in wxWidget's webpage that the reason there could be no Qt version was that it was a way around Trolltech's barrier against proprietary software that used Qt without paying the license, and that as such it could make them incur in Trolltech's legal rage.\n\nNow wxWidgets can have a Qt version with no legal ifs. We'll see if they bother."
    author: "Lobotomik"
  - subject: "Re: Great"
    date: 2009-01-15
    body: "Here is a well documented one: I once had interest in doing it and noone in Wx objected on license grounds. How could they? Wx links to proprietary stuff in windows (MFC?)!\n\nThere has never been a licensing problem about a WxQt."
    author: "Roberto Alsina"
  - subject: "Re: Great"
    date: 2009-01-17
    body: "It's not about linking to proprietary stuff, it's that Qt 4.4.x and below are available under two licenses:\n\n1. GPL (Anything which links to it, no matter how indirectly, must also be GPLed)\n2. Commercial (Requires one paid license per developer per platform)\n\nMFC doesn't have that kind of licensing structure."
    author: "Stephan"
  - subject: "Wow"
    date: 2009-01-14
    body: "This is exactly the kind of thing that makes one go Wow. Impressive."
    author: "Tomas Mecir"
  - subject: "both both"
    date: 2009-01-14
    body: "\"this marks the first time that both Qt and kdelibs will both be available\"\n\ni'm not sure, but that double 'both' seems to be redundant a bit redundant ;)"
    author: "richlv"
  - subject: "in the news"
    date: 2009-01-14
    body: "There's already a Heise News article about the decission, including reactions from Mark Shuttleworth and a Second Life developer:\n\nhttp://www.heise-online.co.uk/news/Qt-Software-U-turns-offers-an-LGPL-licence-option-for-Qt-4-5--/112414\n\nDigg:\n\nhttp://digg.com/programming/Qt_4_5_To_Be_Relicensed_As_LGPL\n"
    author: "mistermaulwurf"
  - subject: "Shuttleworth"
    date: 2009-01-14
    body: "Hasn't Shuttleworth said in the past he'd like to see Gnome being built on Qt in the future?"
    author: "T. J. Brumfield"
  - subject: "Re: Shuttleworth"
    date: 2009-01-14
    body: "\"Shuttleworth: Well, I think it would be perfectly possible to deliver the values of GNOME on top of Qt. There are licensing issues, GNOME is very much built on the LGPL, allowing companies to build their own products on a free software system, giving them some freedom and flexibility in their choice of licensing. That's very frankly been a huge drive for the adoption of GNOME by corporate ISVs.\n\nWhether we'll be able to have the FSF excited about something, have GNOME excited about something, have Nokia excited about something which makes life better for developers - that's gonna be the interesting challenge for me. I'd like to see both desktops focusing on a common infrastructure. And we've already seen that, a lot of the Freedesktop initiatives have been embraced by both projects - HAL, d-bus for instance.\n\nThis also applies to other software projects, if you name your project g-something or k-something your are articulating a very specific user experience. Projects should really look to the whole Linux desktop and see how they can appeal to both sides.\"\n\nhttp://derstandard.at/?url=/?id=3413801"
    author: "Anon"
  - subject: "Re: Shuttleworth"
    date: 2009-01-14
    body: "A Qt based gnome would be great!\nKDE can only profit from it. Competition - especially from a desktop environment using the same toolkit - can bring the quality of KDE to a new level."
    author: "Hardgnome"
  - subject: "Re: Shuttleworth"
    date: 2009-01-14
    body: "It would make it easier for KDE and Gnome to share new breakthroughs in core underlying technology.  Imagine if KDE and Gnome both utilized things like Solid and Phonon?  What if they standardized on one underlying VFS/Kioslave technology, and yet allowed each desktop to provide their own UIs for it?  The same goes for Kwallet, keyring, etc, etc. etc."
    author: "T. J. Brumfield"
  - subject: "Re: Shuttleworth"
    date: 2009-01-14
    body: "I absolutely agree!\n\nGnome and KDE should be based on the same core libraries.\n\nThe actuall desktop environment can be completly different and follow it's own philosophie. There would even be the possibility to have even more desktop environments, like for example some intresting usability research projects - __but all besed on the same core technology__.\n\n"
    author: "Hardgnome"
  - subject: "Gnomies are stunned!"
    date: 2009-01-15
    body: "\"Gnome and KDE should be based on the same core libraries.\"\n\nDream on...\n\nDid you notice the deafening silence that's currently benumbing the Gnome camp?? \n\nI think it is safe to assume that it's not because they're busy discussing how and where to start their \"porting to Qt-4.5\" work. They must still be crawling on the floor, searching their dropped jaws.\n\nIt is noticeable how, after 24 hours, not a single entry over at http://planet.gnome.org/ did comment about the Nokia license change for Qt-4.5. Neither positive nor negative....\n"
    author: "kobserver"
  - subject: "Re: Gnomies are stunned!"
    date: 2009-01-15
    body: "I don't recall a huge amount of bloggage from KDE developers when GNOME announced 3.0.  Most likely explanation for both occurrences: nobody particularly cares.  Your whole post smacks of wishful thinking, to be honest."
    author: "Anon"
  - subject: "Re: Gnomies are stunned!"
    date: 2009-01-20
    body: "Hmmm. It migt also be that Gnome 3.0 would have less of an possible influence on KDE and the KDE software stack than this could have on Gnome (if they want to). Gnome 3 isn't very ambitious and won't bring much new - it's just a much needed fixing of the issues they are having. Qt being LGPL on the other hand could bring them the opportunity of a huge improvement in their infrastructure and a revitalization of their development process (easier, faster toolkit).\n\nThen again, you're probably just right, they don't see this as a serious option (despite technical advantages) and they don't really care..."
    author: "jospoortvliet"
  - subject: "Re: Shuttleworth"
    date: 2009-01-14
    body: "Sorry to double-post, but perhaps a better example would be notifications.\n\nUbuntu has some new ideas for how notifications should be handled.\n\nIdeally, every Linux app should able to use a standard API/call for notifications, and both Gnome and KDE should be able to pick up on this and handle notifications in an appropriate manner.\n\nSystem trays apps also fall in this category. Gnome and KDE apps behave differently, when both should just provide a common call, and then let the Gnome or KDE session handle that call in the manner appropriate for that environment.\n\nMaybe I'm ignorant, but that sounds like the way to go to me."
    author: "T. J. Brumfield"
  - subject: "Re: Shuttleworth"
    date: 2009-01-15
    body: "\"every Linux app should able to use a standard API/call for notifications\"\n\nthere's a D-Bus interface for visual notifications; we implemented a slight variation of it in 4.2 and hope to have a mutually agreed upon version in 4.3\n\n\"System trays apps also fall in this category.\"\n\nthis is a rather different sort of issue, really. in any case, we laid the foundation in 4.2 for fixing it properly, though, along the lines of what you wrote really. this is another 4.3 goal for plasma.\n\nhaving one toolkit beneath both desktops would make this all slightly easier to accomplish, but only slightly. as long as there are also more than one possible desktop shell, it's still tricky."
    author: "Aaron Seigo"
  - subject: "Re: Shuttleworth"
    date: 2009-01-15
    body: "Sounds good!  Thanks for letting us know. "
    author: "T. J. Brumfield"
  - subject: "Truly shocking news!"
    date: 2009-01-14
    body: "I'm speechless. This was a totally unexpected move! This is so awesome I'm having a hard time finding words for it! Thank you, Nokia! If it was still 2008, this would have been the best news of 2008. :)"
    author: "Tuxie"
  - subject: "Thanks!"
    date: 2009-01-14
    body: "This means that I finally can use Qt on my job too! BIG THANKS NOKIA!\n\nI'm so grateful that now I'll buy a Nokia phone :)"
    author: "Paulo Cesar"
  - subject: "Re: Thanks!"
    date: 2009-01-14
    body: "I was undecided, but now my next phone is definitely going to be a Nokia! (the N97 perhaps?)"
    author: "Nicholas"
  - subject: "Re: Thanks!"
    date: 2009-01-14
    body: "Yeah, my next phone will be from Nokia as well..."
    author: "Thorben"
  - subject: "Re: Thanks!"
    date: 2009-01-14
    body: "So will mine, but then again, it's a company phone :-)"
    author: "Thiago Macieira"
  - subject: "Re: Thanks!"
    date: 2009-01-14
    body: "My next phone *is* a Nokia. In a rush of enlightenment, I somehow anticipated this move and bought an E51 one month ago.\n\n...yeah, sure, it's a nice device too."
    author: "Jakob Petsovits"
  - subject: "This is good news?"
    date: 2009-01-14
    body: "I am the only one who doesn't think this is a good idea?\n\nThe GPL ensures that software using it is Free as well. The LGPL gives all the features away for free to proprietary software. [1]\n\nWhile dual-licensing is already a debatable action, it still leads to wider adoption of free software licenses and at least provides its developers with funds by proprietary developers.\nLGPLing removes both of these benefits. qt-development does not depend on these funds right now so that probably isnt an issue - as long as NOKIA stays commmited.\nBut the first is surely an issue. We will see many more smaller proprietary apps using Qt and KDE in future. \n\n[1] http://www.gnu.org/licenses/why-not-lgpl.html\n\n"
    author: "Hannes"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "Not everybody is a GNUTard!"
    author: "Richard"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "Perhaps not, but everyone IS better at discussing contentious issues intelligently without resorting to stupid namecalling than you are."
    author: "CF"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "If you are a free software zealot, maybe.\n\nBut may I remind you that the official GNU desktop is based on GTK, which is licensed under the LGPL?\n\nIf it's good enough for them, it's good enough for me."
    author: "donald"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "I have also thought about this. The why-not-lgpl document specifically says:\n\n\"Using the ordinary GPL is not advantageous for every library. There are reasons that can make it better to use the Lesser GPL in certain cases. The most common case is when a free library's features are readily available for proprietary software through other alternative libraries. In that case, the library cannot give free software any particular advantage, so it is better to use the Lesser GPL for that library.\"\n\nThis case is true for Qt since there are alternative libraries, e.g. GTK.\n"
    author: "mahoutsukai"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "You are quoting from the FSF as it is the bible.\nIt's just a rant from a bitter man..."
    author: "Richard"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "No, it's a _reasoned_ essay (still waiting for your reasons) about an expert on the matter. The fact that you disagree with him, or that you consider him a \"bitter man\" doesn't mean you are right. You could be, or you could not. Of course if someone is quoting him is because he disagrees with you (and of course I do too)."
    author: "Anon"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "Rarely do I read comments from Stallworth and find any of them to be reasoned.  Stallworth, for all his great contributions, is a zealot."
    author: "T. J. Brumfield"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "Stallman.  Richard Stallman.  I am Googling Shuttleworth in another tab and typed Stallworth.  That is great."
    author: "T. J. Brumfield"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "You appear to be thinking of Slugworth - Wonka's competitor - and thinking like Slugworth, too, if labelling Stallman a zealot and considering his writings without reason is the thrust of your message. In fact, Stallman drives his critics up the wall precisely because his writing is typically well thought out, logically consistent, and because he is more often than not shown to be right on whatever issue is being discussed, long after the squeals of \"zealot\" have died down and those doing the squealing have gone off to admire the next shiny gadget, failing still to get the big picture."
    author: "The Badger"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "Stallman is not logically consistent.  Wikipedia used to have an early quote of his when he said he would never in good conscience ever agree to a software license.\n\nHe contends that freedom is only freedom so long as it consists of agreeing to a growing list of his preferred restrictions.  Fewer restrictions is less free, and a different set of shackles is certainly not free.\n\nHe maintains that he isn't about ego, yet he fights people calling Linux, well Linux.  He insists on calling it GNU/Linux, and drew a few mascots that feature a large, pronounce GNU mascot, and a much smaller Linux mascot.\n\nWe don't call it KDE/Linux, or Qt/Linux.  We don't call it Adobe/Windows because it features some Adobe software.\n\nStallman also has said several times in the past that all software should be free in cost, and that it is a crime to charge for software, but later changed his tune and said programmers can be paid.\n\nStallman also thinks that hardware companies shouldn't have the right to protect their hardware.\n\nHe also said cloud computing was beyond idiotic.\n\nI can't recall the last time I saw him make a single logical statement that he hasn't contradicted with another statement.\n\nFurthermore, insisting that if I don't agree with Stallman, then I must be blind (without making any logical argument yourself) sure sounds like zealotry to me."
    author: "T. J. Brumfield"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "Wrong on so many accounts.  Stallman has mentioned that drivers for graphics cards have some reason to remain proprietary when open sourcing the code would hurt them in business.  Stallman has worked as a consultant for Intel.\n\nHe is and has always been very consistent in his views, and he has tried to convince people through reason.  You may think that he's wacky if you don't agree with him,...well, ok, he is kind of wacky, but the GPL is what made Linux successful.  I'm not sure if you've booted into a FreeBSD system, or even OpenSolaris, or whatever that runs X and has a gnome/kde desktop, but it's hard to tell when Linux isn't there.  It would be much harder to have the same result if you took away all the GNU software."
    author: "Bob"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "It isn't a matter of agreeing or disagreeing.  I'm a proponent of OSS.  If you look hard enough, you'll see him contradict himself regularly.  Furthermore, he lectures how companies like Google are evil for not supporting OSS enough.  I'm not sure he understands who his allies are in the FOSS world.\n\nAnd frankly, the argument that a growing list of restrictions is the very definition of freedom is far from logical.  He blasted Ubuntu's attempt at a 100% free distro because it wasn't free enough.\n\nHe blasts open OSS licenses that he didn't write, because if they aren't his specific restrictions, then they don't fit the definition of freedom.\n\nHe blasts Firefox for allowing proprietary extensions, when Firefox is a program that opens many people's eyes to FOSS.\n\nHe blasts Microsoft when they open documents and standards for not doing enough.\n\nI can't recall the last time I saw Stallman issue a statement, \"I really like this group.  I think they're doing some good work, and I endorse it\" unless he is talking about the FSF and himself."
    author: "T. J. Brumfield"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "This \"growing list of restrictions\" that you keep bringing up is actually a bit misleading. I assume you are referring to the GPLv3 compared to GPLv2, but most of the additional \"restrictions\" are more like clarifications or closures of loopholes that people were using to get around the intent of the GPLv2.\n\nThe new sections that are more than just clarifications are mostly reactions to issues that weren't around fifteen years earlier."
    author: "AC"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "According to him Subversion and OpenBSD are not free\nhttp://marc.info/?l=openbsd-misc&w=2&r=1&s=Hypocrite&q=b\nhttp://fitz.blogspot.com/2007/07/stallman-shoots-free-software-movement.html\n"
    author: "anonymous"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "I think you're a bit unclear in your thinking. See, Stallman has never been unclear as to WHAT freedoms he values most; he calls them \"the four freedoms\" and publishes them prominently on the FSF website.\n\nWhen people, organizations, and companies grant these four freedoms, he does praise them. When they fail to do so, and insist on using terms like \"free\" or \"open\" anyway, he criticizes them (like your examples of Firefox and Microsoft). It's super-hyper-ultra consistent, to the chagrin of people who cannot think as clearly and precisely as he does.\n\nSometimes, such people--apparently including you--look at the road to those \"four freedoms\" as \"restrictive.\" In fact, the \"restrictions\" the FSF imposes still preserve those four freedoms; they only \"restrict\" people from infringing on those four freedoms for other people. It's akin to saying the thirteenth amendment to the US Constitution \"restricted\" people from owning slaves, and therefore made the country \"less free.\"\n\nIt's true only for utterly worthless definitions of the term \"free.\" It exposes a grave misunderstanding and muddled thinking on the part of such a speaker/writer. In fact, it's just fundamentally wrong."
    author: "CF"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "Firefox clears his list of the four freedoms, and yet he blasts them anyway.\n\nEither you are 100% free, and you stop people from using proprietary extensions with your software or you aren't free at all.\n\nAnd his version of freedom is a series of restrictions.  Less restrictive licenses like BSD aren't free in his book.\n\nHe can't see the forest for the trees.  He doesn't understand how other people an companies are furthering the FOSS movement, so he attacks them rather than works with them.\n\nAnd frankly his zealotry turns off people, and hurts the FOSS movement.  The GPL is a dirty word in many corporations.  Even though we quietly use some FOSS software where I work, I was told explicitly by the suits that it is company policy not to install or use any GPL software, because they heard from somewhere that it is dangerous, and that the GPL stands against anything commercial."
    author: "T. J. Brumfield"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "And you, being--what exactly are you, here? Sincerely misinformed but educatable? A willfully-ignorant idiot? Dishonest out of maliciousness? You SHOULD already know that the suits, and your company policy, are factually incorrect and the FSF has refuted and debunked such idiocy for over a decade now.\n\nIf you're sincerely misinformed but educatable, you should go read the FSF website for the real scoop.\n\nIf you're a willfully-ignorant idiot, then there's nothing I can do for YOU, but I will point that out here so no one else ascribes more than entertainment value to your opinion.\n\nIf you're dishonest out of maliciousness, stop it. Here, too, I will point and loudly proclaim, \"T. J. BRUMFIELD IS A BIG FAT LIAR! Everything he says about GNU/FSF vs. BSD is specifically designed to mislead! He's running a misinformation campaign to undermine all your hard work and progress toward free software!\"\n\nYou write, again, \"his version of freedom is a series of restrictions.\" I have already refuted that in my previous post: the four freedoms he champions, he does not restrict. He only seeks to restrict those who would infringe on those four freedoms of others. Again, I must repeat that this is like saying the thirteenth amendment to the US Constitution \"is a series of restrictions\" because it restricts people from owning slaves. Well, DUH, yeah, obviously it does, but only someone really, really stupid would think it results in a net loss of liberty.\n\nYou're convincing me you are such a person, because now you have ignored my refutation and gone back to reciting your articles of faith by rote. You have no intelligent refutation of how the four freedoms of the FSF are defended. You're just like some religious idiot sitting in a pew reciting John 3:16 over and over to try to keep yourself convinced.\n\nYou write, \"...licenses like BSD aren't free in his book,\" which of course, to those of us who've read the FSF website, is patently, stupidly wrong. He lists the BSD license among those of true free-software licenses. It's not a COPYLEFT license, but then, you know that difference. Right? RIGHT?\n\nChant away, there zealot-boy. I'll keep pointing out your continual, repetitious, factually-incorrect posts as examples of how NOT to be. Thanks for serving as such a useful example."
    author: "CF"
  - subject: "Re: This is good news?"
    date: 2009-01-16
    body: "You have firmly entrenched yourself in flames and trolling here.  I hope someone takes care of this post."
    author: "T. J. Brumfield"
  - subject: "Re: This is good news?"
    date: 2009-01-16
    body: "But not your straight-up lie that flatly contradicts what the FSF openly publishes about BSD licenses? You want that to be allowed to stand, unchallenged?"
    author: "Dave L."
  - subject: "Re: This is good news?"
    date: 2009-01-16
    body: "No, anyone with Google can find Stallman blasting BSD on numerous occasions.  \n\nCF's post is centered almost entirely on hyperbolic personal attacks with no basis in reality.  It flies into tangents on slavery and religion.\n\nI have no desire to entertain such arguments.\n\nAnd for the record, I pity the fool that tries to argue with me using Constitutional similes."
    author: "T. J. Brumfield"
  - subject: "Re: This is good news?"
    date: 2009-01-16
    body: "\"Pity the fool?\" What are you, Mister T? I didn't see you arguing effectively there; in fact, calling us \"fools\" is essentially stooping to exactly that level.\n\nIf you believe Stallman's four freedoms should be adjoined by a fifth (the freedom to take away other people's four freedoms) then you need to argue effectively that doing so will result in greater freedom for everybody. This is impossible, which is why the FSF's official position, and the anti-slavery amendment, result in more people having more freedoms.\n\nNow that's two of us arguing with you using constitutional similies, and you've responded by doing nothing more than hand-waving, name-calling, and ignoring the point to hope that someone will step in and simply delete the posts that dare to disagree with you.\n\nThanks for showing us the level of quality and defensibility of your opinion."
    author: "Dave L."
  - subject: "Re: This is good news?"
    date: 2009-01-16
    body: "So if I understand you correctly:\n\n* In addition to being factually incorrect about the FSF's judgment of BSD licenses,\n\n* In addition to using factually incorrect policies imposed by ignorant workplace managers,\n\n* In addition to believing the oxymoronical saw that the freedom to TAKE AWAY others' freedoms results in being MORE FREE (go fig),\n\n* In addition to falling back to repeating yourself like a liturgy without addressing refutations when challenged...\n\nYou now have to resort to cowardly begging others to delete comments that challenge your opinion? How embarrassing! If I was you, I would have shut up a long time ago and hoped that the obscurity of this message thread would hide my ignorance from anyone who wasn't a participant in it!"
    author: "CF"
  - subject: "Re: This is good news?"
    date: 2009-01-16
    body: "So if I understand you correctly:\n\n* In addition to being factually incorrect about the FSF's judgment of BSD licenses,\n\n* In addition to using factually incorrect policies imposed by ignorant workplace managers,\n\n* In addition to believing the oxymoronical saw that the freedom to TAKE AWAY others' freedoms results in being MORE FREE (go fig),\n\n* In addition to falling back to repeating yourself like a liturgy without addressing refutations when challenged...\n\nYou now have to resort to cowardly begging others to delete comments that challenge your opinion? How embarrassing! If I was you, I would have shut up a long time ago and hoped that the obscurity of this message thread would hide my ignorance from anyone who wasn't a participant in it!"
    author: "CF"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "\"He contends that freedom is only freedom so long as it consists of agreeing to a growing list of his preferred restrictions. Fewer restrictions is less free, and a different set of shackles is certainly not free.\"\n\nOne can argue about freedom from a number of perspectives, but Stallman has chosen his spot and stayed there. This doesn't make his view any less valid, even if some people would rather have more freedom for themselves and less freedom for, say, the end-user.\n\nIndeed, Stallman's view of freedom is arguably a lot more balanced than those who seem to think that such privileges are just for developers, not users. Such people should read up on notions of freedom - Wikipedia is a good-enough primer - before trying to impress us with \"amateur hour\" arguments about the supposed disadvantage-free merits of permissive licensing.\n\n\"Furthermore, insisting that if I don't agree with Stallman, then I must be blind (without making any logical argument yourself) sure sounds like zealotry to me.\"\n\nI noted that people liberally using the term \"zealot\" didn't get the \"big picture\": quite a different assertion to one which would claim that you are blind. Given such a feeble attempt at misrepresenting what I've written, can we really expect any more from you than the mere impression that you just don't agree with Stallman? Again, Stallman's policies (as opposed to random exchanges captured from discussions) stand up to a lot more scrutiny than the ridicule such policies elicit from his detractors."
    author: "The Badger"
  - subject: "Re: This is good news?"
    date: 2009-01-16
    body: "The argument against Stallman's position can be reduced to its essence!  This issue has nothing to do with copy left -- I have nothing against copy left as it applies to software which you have written and others have modified.  The problem come with the restrictions on linking.\n\nSuppose that I have a piece of GPL software which I have written and I want (or need) to link it to a standard library.  The source code for this library is available free and I am allowed to distribute either unmodified or modified copies of it to anyone as long as I include a copy of the license.\n\nNow, the question is, under the GPL, am I allowed to link this library to my GPL software and distribute copies of both the GPL software and the library together on the same disk?"
    author: "JRT"
  - subject: "Re: This is good news?"
    date: 2009-01-20
    body: "\"Suppose that I have a piece of GPL software which I have written and I want (or need) to link it to a standard library. The source code for this library is available free and I am allowed to distribute either unmodified or modified copies of it to anyone as long as I include a copy of the license.\"\n\nSo this \"standard library\" (although you presumably mean \"popular\" or \"widespread\") is available under a permissive licence...\n\n\"Now, the question is, under the GPL, am I allowed to link this library to my GPL software and distribute copies of both the GPL software and the library together on the same disk?\"\n\nProvided that the licence of the library is GPL-compatible, yes. If not, see this FAQ response:\n\nhttp://www.fsf.org/licensing/licenses/gpl-faq.html#GPLIncompatibleLibs\n\nCompatibility is discussed here:\n\nhttp://www.fsf.org/licensing/licenses/gpl-faq.html#WhatIsCompatible\n\nSee here for some related material:\n\nhttp://www.softwarefreedom.org/resources/2007/gpl-non-gpl-collaboration.html"
    author: "The Badger"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "Richard? The Richard?? Is that you just being sarcastic??\n\n:D"
    author: "J"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "I was thinking the same thing first, but then i realised quickly 2 things.\n\nGnome does not have that problem.\nSomebody writing closed source software for linux, probably won't make a open source app anyway. If yes, kindly asking probably solves the problem."
    author: "Beat Wolf"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "> I am the only one who doesn't think this is a good idea?\n\nPretty much."
    author: "Vee"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "> We will see many more smaller proprietary apps using Qt and KDE in future. \n\nAnd that is good news. Sometimes, there is no choice and you have to use/write non-free software. Now, you have a fair chance that this software will at least integrate well with your KDE desktop. And become more convenient, as a side effect of using the excellent Qt framework (think of the bletcherous GTK open-file dialog!).\n\nOr am I the only one who gets annoyed at the GTK interfaces of some apps (e.g. the NVidia driver configuration applet) standing out in KDE (both in terms of look and (un)usability)?\n\nI won't even talk about the issues of license freedom that GPL-hating open source pundits (including BSD guys) would gladly pound into your head with their trusty rugged clue-by-fours :)."
    author: "slacker"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "> And that is good news. Sometimes, there is no choice and you have to use/write non-free software. \n\nHow exactly do you define \"no choice\"? Someone is forcing you to write non-fre software? Maybe you should call the police ;)\nNo, seriously. I dont run around telling people which software to use, I don't even think that its bad to install a single proprietary driver on you mom's laptop, but I am decidedly against developing non-free software. You *do* have a choice.\nI am also gainst buying hardware that would require you to use non-free-software or donating time/resources/money to the cause of improving non-free-software beit by features or look'n'feel. \n\n> I won't even talk about the issues of license freedom that GPL-hating open source pundits (including BSD guys) would gladly pound into your head with their trusty rugged clue-by-fours :).\n\nI use BSD almost exclusively, I don't hate the GPL. It makes no difference for BSD-licensed software, since the BSDL is compatible with both GPL and LGPL. Also there was an additional GPL-Exception for BSD-licensed code in Qt."
    author: "Hannes"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "> Someone is forcing you to write non-fre software?\n\nIn my own time - hell no ;). At work - it's an entirely different matter.\n\n> I use BSD almost exclusively, I don't hate the GPL.\n\nI was talking about BSD _LICENSE_ zealots here. You misunderstood me :)."
    author: "slacker"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "Idealism is lame."
    author: "Max Howell"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "What's wrong with idealism? Shortsightedness and stubbornness on the other side..."
    author: "Sergio Pistone"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "Actually, idealism can be bad when it results in unnecessary eiher-or thinking, or it starts to make a  bad image on the ideas you are trying to support.\n\nHowever, lack of idealism can also be a bad idea. Idealism is what often makes the world better. And, of course, F/L/OSS wouldn't be what it is today without Stallman's work both coding and promoting.\n\np.s. are we, perhaps, related ;)\n"
    author: "Michael \"Idealism\" Howell"
  - subject: "Re: This is good news?"
    date: 2009-01-14
    body: "GNU's licensing recommendations are directed at you, the reader. License your own software however you want, but allow Nokia and its customers the same freedom. How other people use Qt does not affect your use of Qt."
    author: "Brandybuck"
  - subject: "QT-proprietary stuff can become QT-GPL"
    date: 2009-01-14
    body: "A lot of GPL'd software starts out life as proprietary.  Look at OOo, Mozilla, Java.  And there are more.\n\nHaving a great library like QT available as LGPL will mean that more proprietary software will be QT-based.  And when some of those projects go FOSS, they'll be FOSS QT, not some other package.\n\nHad QT been LGPL (and OS/X cross-platform) many years ago, Firefox, OOo and Java might use QT for their GUIs today."
    author: "littlenoodles"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "My personal preference is GPL over LGPL. It is absolutely unbelievable to see people attacking Stallman here. As someone involved with free software for nearly 10 years and managing projects and watching companies let me give you my take. When I first read Stallman's ideas in the 1990s I thought it was a lovely hippie pipe dream. Now for reality... If you're reading this you probably are interested in the Linux kernel and associated libraries and KDE on top of it. Read up. There was no need for a Linux kernel except that the hurd kernel being developed by GNU wasn't cutting it. Who cares. There was BSD which was solid stable and free. If you don't know the BSD license look for it in the TCP/IP stack in Windows NT or newer. (LGPL offers something between BSD and GPL) Why then is is the Linux kernel so popular? Individuals and companies felt more secure with the protections that their work would not end up being forked for private projects. Linux was not better than BSD for years. And I should point out Linux is just a kernel. It is compiled with the GNU compiler and and linked to GNU libraries. Hello? That's Stallman's baby and why he suggests you call it what it really is, GNU/Linux. Linux would never have overtaken BSD without the GNU GPL and GNU software. \n\nSo if you are hostile to the GPL and Richard Stallman I suggest you either check your history or go back and kiss up to Stallman's former contemporary, Bill Gates. For years it looks like Gates was a visionary, but now it looks like Stallman had the vision that persisted. Having said that I don't worship the guy nor do I think he's the most fun geek to meet at a cocktail party. I just recognize that whatever your opinion of him is you have to give him his due.\n\nSo am I against Qt being LGPL's? No. Idealistically I like the GPL and the dual license makes rational sense. Pragmatically the LGPL option is killer. I say this for one reason. Even an idealist likes to listen to MP3s and watch DVDs. Idealists also have to process documents. In a perfect world new programs and technologies come to us without proprietary hooks. In a nightmare world nothing is accessible without approval by a gate keeper. In our world we allow the two ideas to compete because we know open standards, freedom and quality software has an edge and can attract content and development. By compromising our ideals we can introduce a viral element into the mix that favors our world view and fosters development of our software. We want more people drinking out koolaid because that's how we infect them to work with us.\n\nAgain, the stated goal here for Qt is to make it 10 times as big, and in effect this is the first time ever that commercial development on KDE makes any real sense. Let me point out I'm talking KDE, not Qt which I feel always made sense if there was a profitable model. Imagine having 10 times as many developers on KDE. I get excited about that.\n\nWhere does this make a difference? GNOME on Qt? It's funny but they have swung from we're GPL and C to we're LGPL and Mono. It will be interesting to see if there is another argument. I'm not a fan of Gtk but I don't wish it dead. Where this will make a difference is in programs like OOo where they take community contributions and then make additions and package them into a commercial version. Projects like Firefox or other programs running on Linux for media and such may show up running Qt. \n\nI may prefer the GPL to the LGPL idealistically, but I prefer a more wide spread adoption of my software tools and a larger developer base of our associated projects in practice. There can be no doubt the acquisition of Qt by Nokia has turned out to be a huge boon to KDE users."
    author: "Eric Laffoon"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "Thanks for taking the time :)\n\nI am not sure whether I understand you completely though. \n> Again, the stated goal here for Qt is to make it 10 times as big,\n[...]\n> I may prefer the GPL to the LGPL idealistically, but I prefer a more wide spread adoption of my software tools and a larger developer base of our associated projects in practice.\n\nTo me \"adoption of software\" in itself has no ethical value. It is only benificiary to \"the people\", as a whole, if it also leads to more freedom. There are situations where permissive licensing can be the right decision, e.g.  concerning open standards or free alternatives to non-open-standards, but giving Qt \"away for free to proprietary developers\" will primarily improve non-free software.\nThis means that the free software community is losing an advantage and proprietary developers are gaining one.\n\n\n> and in effect this is the first time ever that commercial development on KDE makes any real sense.\n\ncommercial as in proprietary? Who would benefit from that? How long before everybody starts finding excuses why this and that proprietary application should be considired part of kde/qt/${gnulinuxdistro}? \"It fits in so well\" \"It's just this one module\" \"my friends have it\"...\n"
    author: "Hannes"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "\"This means that the free software community is losing an advantage and proprietary developers are gaining one.\"\n\nThe only thing the free software community loses is exclusivity. Good riddance, in my opinion. It doesn't bother me in the least that other people might be using the same software that I am."
    author: "David Johnson"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "Absolutely agree. I like the GPL since it forces payment of some kind. And prevents the type of situation where free software developers are free workers for someone else.\n\nHowever, this is interesting. Nokia seems to be attempting to build a platform. Cross platform, cross hardware, cross business plan. A place for everyone. And much larger than Nokia/QtSoftware.\n\nWhat is very interesting, and probably bigger news than the license change is the announced opening up of the repository.\n\nMaybe all those KDE developers they hired are convincing when they say how an open and free development system produces amazing results. Maybe the decision makers have been listening to what makes a free software community tick. Nokia seems to be attempting to unleash something instead of exploiting.\n\nAnd if they manage to unleash what I think, they will benefit monetarily. And we will benefit immensely.\n\nI've been reading of instances in previous economic downturns where game changing businesses were established. Time will tell, but this is going to be interesting to watch.\n\nDerek"
    author: "dkite"
  - subject: "Re: This is good news?"
    date: 2009-01-15
    body: "Complitely agree. I'm more and more shocked about how little Freedom is considered among GNU/Linux users, expecially Ubuntu ones that scream \"I want Skype included\", install proprietary video drivers, etc..\nAlso about how few understand that GPL \"restrictions\" are the golden rules to keep software (and USERS) Free.\n"
    author: "marki"
  - subject: "Re: This is good news?"
    date: 2009-01-16
    body: "Restricting users from the freedom of choosing the apps or drivers they want to use isn't keeping users free.\n\nProtecting source-code so that the software is always free is a reasonable goal.\n\nRestricting choice from the end-users in the so called name of freedom isn't."
    author: "T. J. Brumfield"
  - subject: "Great!"
    date: 2009-01-14
    body: "The comercial license is what prevented me for doing my GUI work with QT on my personal projects, speciall since I find QT a great C++ library, now this is no longer an issue.\n\nThanks for the license change,\nPaulo"
    author: "Paulo Pinto"
  - subject: "Would this mean the coming of a Qt based GNOME?"
    date: 2009-01-14
    body: "Do you know what this means?\n\nI see a Qt 4.5-based GNOME in the horizon... This move has shattered the only serious obstacle impeding that. If KDE and GNOME agree in the next GUADEC/aKademy, and you have a LGPL licensed Qt 4.5, this is coming, sirs. Maybe not now, but in a 2-3 years frame from now.\n\nThe unification of the Linux desktop around Qt is coming."
    author: "Alejandro Nova"
  - subject: "Re: Would this mean the coming of a Qt based GNOME?"
    date: 2009-01-14
    body: "Since a large proportion of GNOME developers hate C++ (and Qt itself, for reasons that often have little to do with its license), I don't see this as being very likely.  Additionally, it's unlikely that GNU would throw its weight behind a project where a really core part - the toolkit - had a commercial company as its copyright holder, \"poison pill\" or no.\n\nEven more - what would a Qt-based GNOME look like? They may as well go the whole hog and make use of kdelibs.  In which case, if they want to write a desktop, they might as well use libplasma, which would basically boil down to \"GNOME will be a special case of Plasma\".\n\nI really don't see this happening, to be honest - GNOME is a big project and is GTK through-and-through, so the only way it could switch to Qt is effectively to die completely and re-write itself using KDE technologies, in which case it would simply end up being a small specialisation of KDE."
    author: "Anon"
  - subject: "Re: Would this mean the coming of a Qt based GNOME?"
    date: 2009-01-14
    body: "I've long said that Gnome as a project has a differing philosophy than KDE.  That's fine.  A Gnome built upon Qt wouldn't look like KDE, or operate like KDE.  They have very different opinions on how a UI should be designed, and how apps should work.\n\nThat being said, the idea behind the Free Desktop project is to unify some core concepts and technologies between desktops.  I think building upon Qt would further that goal.\n\nGnome can have a Clearlooks engine, put their Cancel buttons in weird places, and have a desktop operate how they want.  More common libraries are a good thing.\n\nI think the C++ issue will sadly be a deal-breaker, but I really would love to see gnomelibs built upon Qt regardless, especially given the unclear direction of Gnome 3.0.\n\nThe current plan is to clean up and deprecate portions of GTK+ moving forward to refactor it while still preserving compatibility.  Yet other devs are calling for breaking of APIs and a major refactoring.  I don't think the Gnome devs want to go through the KDE 4.0 process because of the negative reaction it provoked.  That being said, they either hobble along on a poorly written GTK platform that was never designed to carry a desktop platform, majorly rewrite GTK, or jump over to Qt.\n\nHobbling along is not the best decision for the future.  The two remaining options are to redesign GTK, and that takes time, as well as fighting over that design.  Moving to Qt means they can start on rewriting gnomelibs tomorrow as opposed to first designing the new GTK."
    author: "T. J. Brumfield"
  - subject: "Re: Would this mean the coming of a Qt based GNOME?"
    date: 2009-01-14
    body: "That's even more silly then people asking Amarok to be ported to gtk. :D\n\nGnome/Gtk and KDE/Qt are technologically incompatible at that sort of level."
    author: "Ian Monroe"
  - subject: "Re: Would this mean the coming of a Qt based GNOME?"
    date: 2009-01-14
    body: "Next thing you'll expect KDE to not only port from QT 3 to QT 4, but to rewrite their core libraries and many of their apps largely from scratch.  That will never happen."
    author: "T. J. Brumfield"
  - subject: "I know your being sarcastic"
    date: 2009-01-14
    body: "Really not much of kdelibs was rewritten from scratch at all in the KDE3->KDE4 transition.\n\nBesides that was KDE, Gnome is far more conservative. They're going to release something called \"Gnome 3\", where they will basically just remove their deprecated functions."
    author: "Ian Monroe"
  - subject: "Re: I know your being sarcastic"
    date: 2009-01-14
    body: "I haven't run a diff between the two versions of kdelibs, and frankly I wouldn't know what I was looking at anyway.\n\nI based my comment on Aaron's comments that kdelibs was completely rewritten.\n\nI did see the plans for Gnome 3, but I've also seen dissenting comments from various Gnome devs who weren't invited to that conversation and feel that GTK needs a major rewrite, not just a compatibility release with some deprecation."
    author: "T. J. Brumfield"
  - subject: "Re: I know your being sarcastic"
    date: 2009-01-14
    body: "\"I based my comment on Aaron's comments that kdelibs was completely rewritten.\"\n\nWhoa - where? "
    author: "Anon"
  - subject: "Re: I know your being sarcastic"
    date: 2009-01-14
    body: "I believe I saw those comments in response to people calling for KDE forks, and people wondering why not all the KDE 3 features immediately ported over to KDE 4.  He insisted that they didn't just port KDE 3 to QT 4, but many of the core underlying libraries of KDE were completely rewritten.\n\nI know for one I can point to Plasma being a part of the new kdelibs as a huge difference.\n\nPerhaps Aaron can come in and clarify, but I know I've seen those comments."
    author: "T. J. Brumfield"
  - subject: "maybe he said core apps"
    date: 2009-01-14
    body: "Plasma is hardly core, its the successor of something that was in kdebase.\n\nAnd Aaron isn't a higher authority. :)"
    author: "Ian Monroe"
  - subject: "Re: maybe he said core apps"
    date: 2009-01-14
    body: "I'd contend that Aaron is a higher authority on his statements."
    author: "T. J. Brumfield"
  - subject: "Re: maybe he said core apps"
    date: 2009-01-15
    body: "only when i'm in an airplane. or on a very tall ladder.\n\n;-P"
    author: "Aaron Seigo"
  - subject: "Re: maybe he said core apps"
    date: 2009-01-15
    body: "Yeah, for that the ladder must be very tall indeed :-P\n\nAlex\n"
    author: "Alex"
  - subject: "Re: I know your being sarcastic"
    date: 2009-01-15
    body: "i don't believe i ever said kdelibs was rewritten =)\n\nthe desktop shell has been rewritten (kdesktop+kicker -> plasma), and that meant libkicker and some classes in libkdeui bit the dust as well.\n\nwe did add a lot of stuff to kdelibs: solid, phonon, threadweaver, the paged dialog classes, sonnet, the model/view based file listings, etc..\n\nand we did do a massive amount of moving stuff around to make the library better segretated and more sensible.\n\nand we did do a massive amount of API cleansing, and in some cases that resulted in huge changes in API approach.\n\nbut through all of that, kdelibs wasn't actually rewritten. it was refactored and augmeneted.\n\nthat effort was gigantic as it was; a rewrite of the rest of the stuff that already works would have been insane.\n\ni don't think it's overly analogous to what's in GNOME, however. they are busy removing dependencies on things like libgnomeui and slowly moving everything into Gtk+ or below.\n\nat the end of it, which will probably take a few more years, they'll probably end up with a bunch of applications on top of one library: gtk+. various apps will use different libs for application specific things (e.g. reading tags out of oggs/mp3s, etc).\n\nonce they are 'there' i have no idea what they'll do next. i could hazard a few sensible guesses, but they'd only be WAGs really so i won't bother. =) one of the GNOME devs could do a much better job of that than me anyways."
    author: "Aaron Seigo"
  - subject: "Re: I know your being sarcastic"
    date: 2009-01-15
    body: "What you just described sounded like a rewrite to me, and thusly if I read something previous to that extent, I no doubt interpreted that as a rewrite."
    author: "T. J. Brumfield"
  - subject: "Re: I know your being sarcastic"
    date: 2009-01-15
    body: "The word \"rewrite\" implies something was started from zero again. This is only true for the desktop and panel as Aaron pointed out."
    author: "Anon"
  - subject: "Re: Would this mean the coming of a Qt based GNOME?"
    date: 2009-01-14
    body: "We saw how difficult was to port KDE from Qt 3 to Qt 4. Porting a desktop from GTK to Qt would be 5 times or so more difficult."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Would this mean the coming of a Qt based GNOME?"
    date: 2009-01-14
    body: "Yep - and GNOME is hardly an active project with many contributors, compared to KDE:\n\nhttp://cia.vc/"
    author: "Anon"
  - subject: "Re: Would this mean the coming of a Qt based GNOME?"
    date: 2009-01-14
    body: "Gnome is due for a major port/upgrade anyways if you listen to all the rumblings.  If you're going to do a refactoring, you might as well discuss what to build around.  Gnome seems to be building more and more on Mono (more and more Gnome packages depend on Mono every month it seems).  I for one would prefer to see a Qt-centric Gnome rather than a Mono-centric Gnome.  But that is just me."
    author: "T. J. Brumfield"
  - subject: "I guess we can expect a useable desktop soon!"
    date: 2009-01-14
    body: "This is great news!\n\nI really have never thought this will happen! Qt is so much better and faster than GTK(+). Unfortunately it's expensive license has spoiled a lot of shareware developers away.\n\nIt will give the community the possibility to build a commercial friendly desktop environment for linux with a state of the art toolkit!\n\nLet's just hope there will be strong and experienced projects leaders who avoid horrible design mistakes done in KDE4 (like using threads for desktop applets instead of processes).\n\nWith good marketing (like not using childish names like K*****, over hyping pre-alpha releases, etc.) this could be another step stone for Linux on the desktop. Professional cross platform apps are finally possible - Linux with the current market share (below 1%) on the desktop can only benefit from it.\n\n"
    author: "KDETroll"
  - subject: "Re: I guess we can expect a useable desktop soon!"
    date: 2009-01-14
    body: "\"horrible design mistakes done in KDE4 (like using threads for desktop applets instead of processes).\"\n\nAgreed.\n\n\"childish names like K*****\"\n\nStrongly disagreed.  KHexEdit > Okteta.  I can look at the former and know what it does.  Apple also disagrees with you, except in their choice of letter.  iTunes and iPhoto communicate what they are for.  Amarok does not.  KPDF does, Okular does not.  I very literally hate this unfounded bashing of k**** names."
    author: "MamiyaOtaru"
  - subject: "Re: I guess we can expect a useable desktop soon!"
    date: 2009-01-15
    body: "> Agreed.\n\nyou do realize you just agreed to something that was factually incorrect, right? =)"
    author: "Aaron Seigo"
  - subject: "Re: I guess we can expect a useable desktop soon!"
    date: 2009-01-15
    body: "Actually, I refactored all the Plasma code to use threads instead of processes. But it was a mistake, so I never committed it. So, he's actually agreeing with something which is factually correct. Possibly. Depending on whether I'm telling the truth or not..."
    author: "Johhny Awkward"
  - subject: "Re: I guess we can expect a useable desktop soon!"
    date: 2009-01-15
    body: "\"like using threads for desktop applets instead of processes\"\n\nwhatever gave you the idea we're using threads for desktop applets? (because, well, we're not.) i'd also love to hear your solution for out of process rendering and interaction that doesn't result in insanity. remember, these aren't tabs in a window, but interacting objects.\n\ni do have the inkling of moving the dataengines out of process at some point, however."
    author: "Aaron Seigo"
  - subject: "Yes but..."
    date: 2009-01-14
    body: "For the short term Qt going LGPL is ending a very long troll, on the long term Qt software is becoming a cost center for Nokia. \n\nThe quality of Qt was also reliant on the paying customers that would not pay if the documentation was bad, or cross-platform was shaky and had a wide range of interests from the desktop to the embedded market.\n\nNow there is only one client, Nokia, with little interest on the desktop. If the going gets tough they will cut Qt software staff or community-source it. Qt will certainly work very well on Nokia platforms but it will be up to the community to make it work elsewhere. There is a clear risk in the long term of an 'androidification' of Qt. \n\nIt will also make it hard for the small Qt ecosystem to bill clients for extra software or services. If you can write commercial software with Qt for free why pay for quality python bindings or extra testing tools ?\n\nThe fact that Gtk is LGPL was not a real blessing for the library. Red Hat has reduced its investment in it, different groups of payed developpers have pushed the library in orthogonal ways...  "
    author: "cmiramon"
  - subject: "Re: Yes but..."
    date: 2009-01-14
    body: "Mod parent up! Also, Im all for a wider adoption of KDE, but 10x bigger?  Lets hope this growth can be managed in a sane way..."
    author: "Anon"
  - subject: "Re: Yes but..."
    date: 2009-01-14
    body: "I don't think the LGPL'ing of Qt will hurt the ecosystem. In fact, I think the opposite will happen.\n\nThere will be more people evaluating Qt and using in their projects. In the past, either they made it opensource (in which case, the ecosystem was of little interest anyways), or they acquired a license. And to justify the expense of the license, they really needed to evaluate it and have a solid business plan.\n\nNow, we may get a surge of short-on-cash companies who will jump onto Qt and may require the services of partners to complete their work.\n\nBesides, testing tools? Hell yeah, every one should use them, regardless of whether they're paying a license to Nokia or not."
    author: "Thiago Macieira"
  - subject: "Re: Yes but..."
    date: 2009-01-14
    body: "> For the short term Qt going LGPL is ending a very long troll, on the long term Qt software is becoming a cost center for Nokia. \n> Now there is only one client, Nokia, with little interest on the desktop. If the going gets tough they will cut Qt software staff or community-source it.\n> Qt will certainly work very well on Nokia platforms but it will be up to the community to make it work elsewhere. There is a clear risk in the long term of\n> an 'androidification' of Qt.\n\n\nIf eveyone codes with Qt API's, how is Nokia going to loose? :-/ They've opened themselves for a much larger pool of developers. Instead of coding on Android, Java or .Net you can to Qt all the way now.\n\nAll those applications can be migrated easily to Nokia phones. Getting people to use your API is important (MS all thought us that, and this could beat them at that same playing field :-p). Killing the desktop would kill the stream of applications which can be easily ported, so I wouldn't be too pessimistic about it."
    author: "Diederik van der Boor"
  - subject: "Re: Yes but..."
    date: 2009-01-14
    body: "As a Qt developer for a Nokia partner, let me chime in here: Putting Qt under the LGPL is awesome news for me! The only way this news could have gotten better is if they had used the BSD or MIT license instead :-)\n\nThe business model for Trolltech/Nokia has now shifted from selling licenses to selling professional services. That's where I am. The more commercial software firms there are using Qt, the more demand there is for me. I'm not a pollyanna, and expect some rocky situations as the ecosphere rebalances itself, but in the long term this is awesome news.\n\nFor more perspective on this move from the services side, see this whitepaper: http://www.ics.com/files/docs/Qt_LGPL.pdf"
    author: "Brandybuck"
  - subject: "Re: Yes but..."
    date: 2009-01-14
    body: "LGPL license for Qt will make the situation more similar with the GPL license of the Linux kernel.\n\nA lot of companies develop for Linux as a platform for their commercial applications and/or special needs.\n\nThere are hundreds of full time developers of the Linux kernel.\n\n- Will it be a need for paid developers? Yes! Someone needs to get the job done.\n- Will it be a need for a commercial support and services company? More than ever. Different users have different needs. They need someone to help them and to develop for their needs.\n\nMy guess is that OpenOffice.org and Firefox will be ported to Qt. LGPL Qt will also open up a lot of possibilities for open source Java.\n\nA LGPL Qt version of OpenOffice.org will be a huge step.\n\nI am a Gnome user since the start of Gnome. Why? The Qt license is the major reason. I also agree with the philosophy of the Gnome Desktop. A Gnome desktop integrated with Qt would be great."
    author: "Martin"
  - subject: "Re: Yes but..."
    date: 2009-01-15
    body: "> Now there is only one client, Nokia\n\nyou analysis hinges on this point. unfortunately for the analysis, this point is incorrect.\n\nQt Software will continue to sell commercial licenses and provide professional support. the goal is to actually increase the number of clients, not decrease it to one (Nokia) while ignoring the other users of Qt.\n\nif it was indeed a Nokia-for-itself play, i'd agree with some of what you said.\n\n\"If you can write commercial software with Qt for free why pay for quality python bindings or extra testing tools ?\"\n\nbecause the Python bindings have value, as do the testing tools. in fact, with more people using Qt at lower cost, there are going to be more people using Qt and less money taken out of their budget in the process. this means a bigger market with more money per consumer to spend on things like Python bindings or testing tools.\n\ni actually expect the ecosystem of add-ons and professional services to grow as a result.\n"
    author: "Aaron Seigo"
  - subject: "Re: Yes but..."
    date: 2009-01-15
    body: ">Qt Software will continue to sell commercial licenses and provide professional >support. the goal is to actually increase the number of clients, not decrease >it to one (Nokia) while ignoring the other users of Qt.\n\nI hope you are right but I doubt it. The idea that Open Source will support itself by selling services is a mantra we keep repeating. But, we have always overestimated the amount of services people are ready to pay if they have a product for free.\n\nNow that Qt is LGPL, it makes it possible for other companies to offer support. These companies will not bear the cost of developping Qt and will be able to cut the prices. We saw that for Linux distributions. Suse missed the M\u00fcnchen municipality market, Mandriva missed the French National Assembly because the \"serious\" distributions were undercut in the tenders by cheap competitors that were just rebranding Debian.\n\n  "
    author: "cmiramon"
  - subject: "Re: Yes but..."
    date: 2009-01-15
    body: "In the corporate world, rarely do suits seem to trust anything free.  I just tried convincing two VPs that we should move to Untangle from expensive proprietary network appliances where we are paying for the device, and subscription fees.  Even though we are in cost-cutting mode, they just didn't trust someone that was free.\n\nThe only version of Linux the suits are happy with is Red Hat, because we pay for support on it, even though we've never once called Red Hat.  When I suggested that we could use CentOS for free, immediately people became skeptical."
    author: "T. J. Brumfield"
  - subject: "Re: Yes but..."
    date: 2009-01-16
    body: "> support itself by selling services \n\nup until now Qt Software has been selling the right to build hermetically sealed proprietary wares in combination with technical / professional support.\n\nthat isn't changing going forward.\n\nwhat they are doing is trading some of their current market for market segments they haven't been able to tap into _at all_. by growing the overall market at a  lower conversion rate they hope to achieve two things: greater adoption of Qt (strategically good for Nokia) and numerically more licenses sold (operationally good for Qt Software division)\n\n> it makes it possible for other companies to offer support\n\nthat's been true for years, and in fact many companies do just that.\n\n> These companies will not bear the cost of developping Qt\n\nthose companies don't bear that cost now, either.\n\n"
    author: "Aaron Seigo"
  - subject: "Why not version 3?"
    date: 2009-01-14
    body: "Why LGPL 2.1, and not 3 ?"
    author: "Capit. Igloo"
  - subject: "Re: Why not version 3?"
    date: 2009-01-14
    body: "First LGPL v2.1 is license compatible with all other copyleft licenses from Free Software Foundation. Please take a look at the license matrix from FSF: http://gplv3.fsf.org/dd3-faq\n\nThis ensures there is no license incompatibility when making free software with Qt LGPL v2.1.  \n\nSecond as a first step we have selected LGPL version 2.1 as this version best fits our purposes and we are most comfortable with at this point in time. We will continue to evaluate the adoption, use and legal interpretation of LGPL version 3 by the community and may use version 3 for future releases.\n\n"
    author: "Knut Yrvin"
  - subject: "Re: Why not version 3?"
    date: 2009-01-14
    body: "Any chance getting it to do v2 or later?\n\nAlso, especially now that you're not requiring copyright assignment, you need to ensure that Qt can be upgraded to later versions even if some of the contributors don't like the new license (say, LGPL v4). Probably you'll be doing something like the KDE e.V.'s license voucher anyways, but make sure you don't lock yourself into a specific version of a license."
    author: "Jakob Petsovits"
  - subject: "Re: Why not version 3?"
    date: 2009-01-14
    body: "Luckily section 3 in LGPL v2.1 allows re-licensing to future GPL versions (e.g GPL version n=2, n+1; n-> infinity). Not all know this part of the LGPL. Continuing releasing Qt as GPL v3 is for convenience, not leaving a perception that Nokia is revoking that version.\n\nLGPL v2.1 section 3 says: \n\nYou may opt to apply the terms of the ordinary GNU General Public License instead of this License to a given copy of the Library. To do this, you must alter all the notices that refer to this License, so that they refer to the ordinary GNU General Public License, version 2, instead of to this License. (If a newer version than version 2 of the ordinary GNU General Public License has appeared, then you can specify that version instead if you wish.) Do not make any other change in these notices.\n\nOnce this change is made in a given copy, it is irreversible for that copy, so the ordinary GNU General Public License applies to all subsequent copies and derivative works made from that copy.\n\nThis option is useful when you wish to copy part of the code of the Library into a program that is not a library. \n\nSource: http://www.gnu.org/licenses/lgpl-2.1.html"
    author: "Knut Yrvin"
  - subject: "Re: Why not version 3?"
    date: 2009-01-14
    body: "That's actually new to me and a major coolness.\n\nAt first I thought that it would still be insufficient as the \"any later version\" clause is still required for the LGPL itself, but in fact all the compatibility issues with free software requiring any version of the GPL are solved by this section, and for proprietary applications based on LGPL software, it should not make a lot of difference. Neat."
    author: "Jakob Petsovits"
  - subject: "Oh my f*****g gosh!!!"
    date: 2009-01-14
    body: "Now that the argument that KDE-is-based-on-a-commercial-library-and-so-does-not-pass-our-zealot-philosophy is no longer valid, I wonder what the KDE detractors would now say. I'd bet they didn't even expect this happening, another point against KDE wiped off their list, ha!\n\nI can't wait for articles comparing KDE over GNOME based purely on their technical merits and not based on the license of their underlying toolkits.\n\nWe should express our thanks by supporting Nokia products!\n\n"
    author: "HappyQtUser"
  - subject: "Re: Oh my f*****g gosh!!!"
    date: 2009-01-14
    body: "\"Now that the argument that KDE-is-based-on-a-commercial-library-and-so-does-not-pass-our-zealot-philosophy is no longer valid, I wonder what the KDE detractors would now say.\"\n\nMy guess? \"KDE is built on a toolkit whose copyright is held by a commercial company that hates OGG and open web standards, rather than the glorious Free Software Foundation.\"\n\nThere's also the standard \"KDE is bloated/ cluttered/ buggy/ crashy / looks over functionality (KDE4 onwards)/ everything begins with K/ the only software project in history to have a dodgy x.0.0 release\", etc.  People who truly hate KDE will always be able to find things to criticise, whether their criticisms are valid or not."
    author: "Anon"
  - subject: "Gtk has commercial backing as well"
    date: 2009-01-14
    body: "Like Sun develops it accessibility framework, Redhat works on this and that. Not sure what percentage of Gtk developers are paid, but I think its fairly substantial.\n\nThis fact won't stop trolls though, you're right. :)"
    author: "Ian Monroe"
  - subject: "Re: Oh my f*****g gosh!!!"
    date: 2009-01-15
    body: "personally, i'm hoping it doesn't devolve into a Qt vs Gtk, KDE vs GNOME argument. \n\ndiscussion is great, arguing sucks.\n\ni'd rather sit back and be happy about this news for what it is, the rest of the toolkit world out of mind. =)"
    author: "Aaron Seigo"
  - subject: "Re: Oh my f*****g gosh!!!"
    date: 2009-01-15
    body: "I believe the point was, this will hopefully end some of the toolkit bickering by removing the argument that Qt is less than free.  (Is that like less than fresh?)"
    author: "T. J. Brumfield"
  - subject: "LGPL, C++ and closed source software?"
    date: 2009-01-14
    body: "It is actually possible to develop closed source software with a LGPL'ed version of Qt 4.5? The LGPL 2.1 states:\n\n5. A program that contains no derivative of any portion of the Library, but is designed to work with the Library by being compiled or linked with it, is called a \"work that uses the Library\". Such a work, in isolation, is not a derivative work of the Library, and therefore falls outside the scope of this License.\n\nHowever, linking a \"work that uses the Library\" with the Library creates an executable that is a derivative of the Library (because it contains portions of the Library), rather than a \"work that uses the library\". The executable is therefore covered by this License. Section 6 states terms for distribution of such executables.\n\nWhen a \"work that uses the Library\" uses material from a header file that is part of the Library, the object code for the work may be a derivative work of the Library even though the source code is not. Whether this is true is especially significant if the work can be linked without the Library, or if the work is itself a library. The threshold for this to be true is not precisely defined by law.\n\nIf such an object file uses only numerical parameters, data structure layouts and accessors, and small macros and small inline functions (ten lines or less in length), then the use of the object file is unrestricted, regardless of whether it is legally a derivative work. (Executables containing this object code plus portions of the Library will still fall under Section 6.)\n\nOtherwise, if the work is a derivative of the Library, you may distribute the object code for the work under the terms of Section 6. Any executables containing that work also fall under Section 6, whether or not they are linked directly with the Library itself.\n\nI did not look at the Qt sourcecode, but usually templates and macros are defined in header files and if you include a header file which contains e.g. a template definitions which is longer then 10 lines of code, you have to release your code under LGPL as stated above. I think that is why people usually use GPL + runtime exception. What do you think about that?"
    author: "Michael Thaler"
  - subject: "Re: LGPL, C++ and closed source software?"
    date: 2009-01-14
    body: "I forgot this. Here is an example from libstdc++: http://gcc.gnu.org/onlinedocs/libstdc++/manual/bk01pt01ch01s02.html\n\n The source code is distributed under the GNU General Public License version 2, with the so-called \u0093Runtime Exception\u0094 as follows (or see any header or implementation file):\n\n\n      As a special exception, you may use this file as part of a free software\n      library without restriction.  Specifically, if other files instantiate\n      templates or use macros or inline functions from this file, or you compile\n      this file and link it with other files to produce an executable, this\n      file does not by itself cause the resulting executable to be covered by\n      the GNU General Public License.  This exception does not however\n      invalidate any other reasons why the executable file might be covered by\n      the GNU General Public License.\n    \n\nHopefully that text is self-explanatory. If it isn't, you need to speak to your lawyer, or the Free Software Foundation. "
    author: "Michael Thaler"
  - subject: "A: Dynamic linking"
    date: 2009-01-14
    body: "Legal at Nokia has worked really hard on what is meant by \u0093suitable shared library mechanism\u0094 and \"work that uses the Library\". The recommendation is to do dynamic linking since copyright laws may vary between different countries. \n\nQ: What is a \u0093suitable shared library mechanism\u0094?\n\nOne that uses at runtime a copy of the library already present on the user\u0092s computer system (rather than copying library functions into the executable) \n\n-- This is dynamic linking!\n\nOne that will operate properly with a modified version of the library (if the user installs one as long as the modified version is interface compatible with the version that the original was made with).\n\nQ: I\u0092m deploying on a device \u0096 what does the LGPL mean for me?\n\nWhen creating a device that will include LGPL and proprietary components, you need to ensure that you comply with the following restrictions in the LGPL:\n\n-- Modifications made to the library itself must be licensed under the \nterms of the LGPL \n\n-- A proprietary application that is a \u0093work that uses the library\u0094 needs to be dynamically linked with the library in order to avoid becoming a combined work that results in a licensing incompatibility\n\n-- Users need to be aware that an executable might be a combined work with the library unless a suitable shared library mechanism is used (i.e., one that, at runtime, uses a copy of the library already present on the user\u0092s computer system (rather than copying library functions into the executable), or one that will operate properly with a modified version of the library (if the user installs one as long as the modified version is interface compatible with the version that the original was made with).\n\nAt the end. Since this is complicated and copyright law may vary between different countries, please consult your own legal advisors regarding this issue.\n"
    author: "Knut Yrvin"
  - subject: "Nokias response to this question"
    date: 2009-01-14
    body: "Read the following from Nokia:\nhttp://www.qtsoftware.com/about/licensing/frequently-asked-questions#what-versions-of-qt\n\n** Can I transition my commercial Qt project to LGPL and how?\nCustomers who have developed applications with Qt will be able to dynamically link their proprietary applications to the LGPL licensed Qt library. Additionally, as customers own the modifications they make to Qt under the commercial license, they may relicense such modifications under the LGPL if they wish.\n\n** Why would I want to buy a commercial license? What is the difference?\nThe commercial Qt license includes email support, access to upgrades and allows you to develop fully closed source software. The LGPL carries some restrictions regarding the ability for users to relink libraries and other restrictions that may impose architectural requirements that some organizations might not be comfortable with.\n\n"
    author: "Jens"
  - subject: "Re: Nokias response to this question"
    date: 2009-01-14
    body: "Thanks. I think it is clear that Nokia wants to allow proprietary applications to link to the LGPL licensed Qt library. However, to develop such an application you need to include the Qt header files and the LGPL 2.1 states (as far as I understand) that you must release your application under LGPL if you include header files which contain a non-trivial amount of code (functions with more then 10 lines of code etc.). C++ templates are defined in header files, thus you will include header files with functions that contain more then 10 lines of code if you link to Qt. I just pointed that out because someone mentioned that in a German forum and I thought maybe the Nokia people overlooked that (however, seems not very likely to me). If so, it would probably be a good idea to add an exception to the license like libstdc++ does.\n\nJust to make it clear: I am very happy that Nokia released Qt under LGPL. I do not want to critisize Nokia in any way. (I have never used Qt for closed source software, only for open source stuff, so personally I don't care)."
    author: "Michael Thaler"
  - subject: "Re: Nokias response to this question"
    date: 2009-01-14
    body: "No, if a library is LGPL (not GPL), you can use it without opening up your own code. Yes, that means you can include the headers (otherwise you won't be able to use the library, right?)\n\nOnly if it's GPL you will have to open up your own code."
    author: "HelloWorld"
  - subject: "Re: Nokias response to this question"
    date: 2009-01-14
    body: "http://gcc.gnu.org/onlinedocs/libstdc++/faq.html#faq.license\n\n\"The LGPL requires that users be able to replace the LGPL code with a modified version; this is trivial if the library in question is a C shared library. But there's no way to make that work with C++, where much of the library consists of inline functions and templates, which are expanded inside the code that uses the library. So to allow people to replace the library code, someone using the library would have to distribute their own source, rendering the LGPL equivalent to the GPL.\""
    author: "Hans"
  - subject: "Re: Nokias response to this question"
    date: 2009-01-14
    body: "Except that it does work in C++! Take a Qt application binary, that links with libQtCore.so.4.2.0. Now replace libQtCore.so.4.2.0 with libQtCore.so.4.4.3. The app still runs!!! Version 4.4.3 is most certainly a modified version of 4.2.0. Yet the app runs. This is because Trolltech designed Qt to be forwardly binary compatible."
    author: "Brandybuck"
  - subject: "Re: Nokias response to this question"
    date: 2009-01-14
    body: "No it wont work. The code for QList<MyType> is in the Qt Application binary, not in libQtCore. To replace that one needs to recompile the application and thus have the source code. Nokia needs to add the appropriate exceptions to the LGPL, which they may or may not do."
    author: "Hans2"
  - subject: "Re: Nokias response to this question"
    date: 2009-01-15
    body: "What does your legal department say? Nokia's says it's okay to link with LGPL from non-LGPL application."
    author: "Brandybuck"
  - subject: "Re: LGPL, C++ and closed source software?"
    date: 2009-01-14
    body: "Qt will still be available under the GPL with exceptions.\n\nBut regardless, the licensor's intentions carry legal weight. Nokia intends for Qt to be dynamically linked to from C++ applications, for its base classes to be subclassed, and it's template containers to be used, without \"infection\" by the library's license."
    author: "Brandybuck"
  - subject: ":^D"
    date: 2009-01-14
    body: "Woohoo! Now that is some extremely good news I would never expect! I wish more such pleasant surprises happen :).\n\nSeriously, this remedies the most important problem there was with Qt. Now, with THE best toolkit out there being available for free to people objecting to the GPL - I see GTK fall and fall in popularity in not-so distant future :).\n\nNow the only thing that I find Qt lacks is bindings for the C language. But maybe I'm too greedy ;)."
    author: "slacker"
  - subject: "Excellent news "
    date: 2009-01-14
    body: "Lets now hope they port all their phone sync software to QT so it can be multiplatform and i could fully sync to Kontact.  If that happens then i'll also be buying a Nokia smartphone"
    author: "Ian"
  - subject: "Re: Excellent news "
    date: 2009-01-15
    body: "the biggest problem with the PC Suite as i understand it is not so much the use of Qt, which it is already doing, but accessing all the platform specific APIs outside of Qt to do what it does. so there's work yet left to be one there. hopefully it will get done, though that's not yet a given."
    author: "Aaron Seigo"
  - subject: "Re: Excellent news "
    date: 2009-01-15
    body: "Thanks for the update.  If they do sort it out, it will open up another large sector in their battle with RIM etc and put them in the driving seat for smartphones.  I do love my blackberry so i tolerate having to sync etc it via my girlfriends PC but I would defect to Nokia once the PC suite is available."
    author: "Ian"
  - subject: "Re: Excellent news "
    date: 2009-01-18
    body: "I sure hope they'll port their stuff to opensync plugins. At least, Nokia should start opening up to opensync. IIRC, Michael Bell (the syncml-plugin-master) said there's noone at nokia answering questions about nokia-phones acting up when syncml-syncing.\n\nNokia's PC-Suite could simply be a opensync-frontend. And in KDE, it would be kitchensync.\n\nNow how sweet would that be?"
    author: "ben"
  - subject: "A real breakthrough!"
    date: 2009-01-14
    body: "This is the best news about Qt of the past ten years!\n\nA software galore based on Qt is coming!\n\nViva, Nokia!\n\nLet's celebrate that!"
    author: "Artem S. Tashkinov"
  - subject: "Re: A real breakthrough!"
    date: 2009-01-14
    body: "yes, viva nokia, thanks, what a news to begin 2009 with.\n\n\nthannnnnnnnnnnnnnnnnks Nokia"
    author: "mimoune djouallah"
  - subject: "Great News"
    date: 2009-01-14
    body: "This is awesome news.\nI wish it happened 1.5 years ago.\nWell done Nokia, this move will win you many friends in developer's community!\nNow GNOME and GTK's existence is a big question mark, though :(\n"
    author: "sergeant"
  - subject: "Re: Great News"
    date: 2009-01-14
    body: "\"Now GNOME and GTK's existence is a big question mark, though :(\"\n\nI'm guessing that distros such as Redhat will continue to rally around GNOME (they seem to be almost actively antagonistic towards KDE, even regardless of licensing), so I don't see GNOME/ GTK disappearing any time soon :)"
    author: "Anon"
  - subject: "Re: Great News"
    date: 2009-01-14
    body: "\"I'm guessing that distros such as Redhat will continue to rally around GNOME (they seem to be almost actively antagonistic towards KDE, even regardless of licensing)\"\n\nActually, they haven't had the opportunity to act \"regardless of licensing\" yet.\n"
    author: "Vee"
  - subject: "Re: Great News"
    date: 2009-01-14
    body: "I read a comment from a SUSE guy once who said that while he liked KDE, he understood why distros like SLED and Red Hat went with Gnome.  In an enterprise roll-out, you want everyone to have the same desktop, and you want them to be simple.  Instead of offering up the choice of KDE and Gnome, some people just want choices.  They want to install Linux and get a default desktop.  Personally, I think KDE is better for people moving from Windows, but plenty of people feel that Gnome is simpler and easier to learn.\n\nThe Fedora guys seem to really dig KDE 4, so I wouldn't say Red Hat on the whole is antagonistic towards KDE."
    author: "T. J. Brumfield"
  - subject: "Re: Great News"
    date: 2009-01-20
    body: "I never understood why you would roll out Gnome on a corporate desktop. The slight usability advantage of Gnome over KDE is gone after you've used either desktop for about a week full-time, and after that the optimized workflow you can have in KDE makes a big differencen in terms of productivity. I'd rather have my employees take a week more to get used to something and be more productive afterwards than having the shortest time possible to get used to it. After all, productivity matters - if it didn't we could all continue to use dos 5.0 and WP 5.1 or something..."
    author: "jospoortvliet"
  - subject: "Re: Great News"
    date: 2009-01-24
    body: "\"I read a comment from a SUSE guy once who said that while he liked KDE, he understood why distros like SLED and Red Hat went with Gnome. In an enterprise roll-out, you want everyone to have the same desktop, and you want them to be simple. Instead of offering up the choice of KDE and Gnome, some people just want choices.\"\n\nIt doesn't explain on what basis you would pick Gnome, and some 'easy to use' benchmark is particularly feeble. On a corporate desktop you need the technology and you need the applicatiins ultimately otherwise there is no hope whatsoever, and it seems as though 'easy to use' has become a standard excuse for not having them. Gnome has proved itself incapable of delivering on that front and the LGPLing of Qt just highlights that even further."
    author: "Segedunum"
  - subject: "Re: Great News"
    date: 2009-01-14
    body: "Of course, this won't happen overnight, but even Redhat has become pretty flexible lately.\nGTK+, being such an awful toolkit, had its image badly spoiled by Mono.\nTheir other desperate attempts with \"easier\" bindings didn't work so well (gtkmm, java-gnome etc.).\n\nI think even die-hard redhat and ubuntu developers will have to have another look at straight pros/cons comparison of both toolkits and we know already what the outcome will be :).\n\nThe hype about web applications and cloud computing makes this announcement a little bit less exciting, still very valuable."
    author: "sergeant"
  - subject: "Re: Great News"
    date: 2009-01-14
    body: "I disagree with the mono statement myself.  Mono was the one thing that GTK had going for it.  With Mono, you at least had a toolkit with a full set of libraries under a nice license (there was some controversy here for a while).\n\nGTK by itself is so limited - how do you write networking code?  What is the state of the CORBA stuff?  Where are the tutorials on how to write a simple app that integrated with GNOME?  At least with Mono, there is direction to the project.\n\n"
    author: "Chris Parker"
  - subject: "Re: Great News"
    date: 2009-01-14
    body: ">actively antagonistic\n\nIsn't that practically an oxymoron? :P\n\n>so I don't see GNOME/ GTK disappearing any time soon :)\n\nWhich, IMO, is a very good thing. For years KDE and Gnome have been moving closer and closer to being able to create a consistent look and feel (the shared icon spec, qt's gtk widget scheme, gtk-qt) as well as compatibile interfaces (eg, dbus) so using Gnome apps in a KDE environment (or KDE apps in a Gnome environment) is much less intrusive than it once was.\n\nAs Gnome and KDE consist of different people with different philosophies, and generally approach things from different directions initial but more and more frequently come up with a common solution thats superior to either of the original implementations (think D-Cop and... cobra?, then DBus replacing both after a time; KHTML/WebKit is looking like a similar example that has an interesting origin).\n\nThis (LPGL Qt) is definitely very exciting news for KDE (and Gnome), especially when combined with a combined aKademy/GUADEC... I foresee a very interesting few years coming up!"
    author: "Kit"
  - subject: "Re: Great News"
    date: 2009-01-15
    body: "An antagonist is, put simply, a \"bad guy\". I think you're thinking of a different word.\n"
    author: "Michael \"English\" Howell"
  - subject: "OT - Flat Forty?"
    date: 2009-01-14
    body: "Are there any plans for the Flat Forty to return? It makes reading fast-moving discussions far more pleasant, and I've been missing it a greal deal :)"
    author: "Anon"
  - subject: "Re: OT - Flat Forty?"
    date: 2009-01-14
    body: "+1"
    author: "Binky"
  - subject: "Bindings too?"
    date: 2009-01-14
    body: "Will all the bindings, like PyQt, be LGPL too?"
    author: "Mork"
  - subject: "Re: Bindings too?"
    date: 2009-01-14
    body: "AKAIK PyQt is commercial. I don't think they'll go LGPL, too."
    author: "knue"
  - subject: "Re: Bindings too?"
    date: 2009-01-14
    body: "It was commercial during Qt3 phase (i.e years ago). Currently, it has the dual license. I assume they will follow suit and go LGPL (staying GPL would be a a PR suicide and cause reimplementation)."
    author: "Ville"
  - subject: "Re: Bindings too?"
    date: 2009-01-15
    body: "No, it has always(Nearly, not sure if the the Qt 1 was only GPL) been dual licensed GPL and commercial."
    author: "Morty"
  - subject: "their choice"
    date: 2009-01-14
    body: "QtJambi is going to be LGPL though."
    author: "Ian Monroe"
  - subject: "Re: Bindings too?"
    date: 2009-01-14
    body: "I can answer for C# and Ruby.\n\nI'm not sure how the Ruby source code in QtRuby that the bindings depend on, can be 'dynamically linked' with a commercial application as per the requirements of the LGPL. Maybe we would have to add a 'Ruby exception' or something to make sure that QtRuby could be used for commercial in house development. The LGPL seems to really only consider compiled languages. But if making QtRuby LGPL will make it more popular, then it seems a good idea to change the license from GPL to LGPL.\n\nC# is compiled into dynamically linked .dlls which seem to be the sort of thing the LGPL license has in mind. I think we will change the Qyoto license to LGPL to make it more competitive with GTK# which is LGPL. We haven't had a discussion on kdebindings on what to do yet, but I would be surprised if we don't end up LGPL'ing Qyoto.\n"
    author: "Richard Dale"
  - subject: "Thanks Nokia!"
    date: 2009-01-14
    body: "Truly great news! Not the least because we now finally will not hear more from people complaining about the license :D"
    author: "Joergen Ramskov"
  - subject: "Re: Thanks Nokia!"
    date: 2009-01-15
    body: "Why the hell does it use the fascist LGPL, and not the Truly Free BSD? I guess I'm switching to Enlightenment. It's too bad that I am forced to abandon such a great peace of software like KDE/Qt only because it is restricted by a fascist un-free license.\n\n:P ;) xD"
    author: "slacker"
  - subject: "Nokia smartphone market accepts reality"
    date: 2009-01-14
    body: "Either they open up their licensing scheme to compete against Android and the iPhone or they slowly see their market share erode and phone hardware sales diminish.\n\nWith the Palm Pre being a potential, they made this move in hopes they can get a large pool of FOSS devs to keep them competitive. Smart move, but reactive and not pro-active."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Nokia smartphone market accepts reality"
    date: 2009-01-15
    body: "\"but reactive and not pro-active.\"\n\nagreed, but Qt licensing has always been this way, really. it's a result of being very careful and conservative with licensing, and understandably so. eventually they get there, and that's what ultimately matters."
    author: "Aaron Seigo"
  - subject: "Re: Nokia smartphone market accepts reality"
    date: 2009-01-15
    body: "\"With the Palm Pre being a potential, they made this move in hopes they can get a large pool of FOSS devs to keep them competitive. Smart move, but reactive and not pro-active.\"\n\nPre was announced last week. I hardly think this is a knee-jerk reaction to that. Decisions like this simply don't happen that quickly within multi-billion dollar global organizations.\n\nFrankly, I suspect that Nokia had intended to do this from the beginning, the acquisition certainly wasn't about the potential for licensing fees as incremental revenue, not at a time when Nokia is chopping off business units as part of an overall restructuring to re-focus efforts strictly on their mobility platforms and service delivery.  I would imagine it just took time to work out the legal mumbo jumbo before they could actually do this.\n\niPhone was probably a wake-up call, and RIM's continued dominance in the enterprise space despite the hundreds of millions that Nokia has previously invested in, and ultimately failed at, trying to build a portfolio of proprietary enterprise apps and services around their mobile platforms, was another driver for this new direction.  \n\nI think Nokia has a solid strategy here, and it goes far beyond trying to make KDE overtake Gnome, though the KDE team and users will certainly benefit. I suspect things are going to become very interesting over the next couple of years..."
    author: "elsewhere"
  - subject: "kde svn does not compile qt 4.5 snapshot 20090113"
    date: 2009-01-14
    body: "kde svn trunk from yesterday Jan 13 2009 does not compile with qt 4.5 snapshot 20090113 with gcc version 4.2.4.  It used to but no longer.  It has been like this for a few days.  I was wondering if I should even file a bug since qt 4.5 is not in KDE's SVN repository yet.\n\nIt stops at kfilewidget:\n\nIn file included from /home/k/kdesvn/kdelibs/kfile/kfilewidget.cpp:2602:\n/home/k/kdesvn/kdelibs/kfile/kfilewidget.cpp: In member function 'virtual void KFileWidget::slotOk()':\n/home/k/kdesvn/kdelibs/kfile/kfilewidget.cpp:771: warning: 'statJob' may be used uninitialized in this function\nLinking CXX shared library ../lib/libkdeinit4_kbuildsycoca4.so\n[ 64%] Built target kdeinit_kbuildsycoca4\nLinking CXX shared library ../lib/libkfile.so\n[ 64%] Built target kfile\nmake: *** [all] Error 2"
    author: "charlie"
  - subject: "generally don't file compile problems as bugs"
    date: 2009-01-14
    body: "And you didn't post the actual error message."
    author: "Ian Monroe"
  - subject: "License still questionable"
    date: 2009-01-14
    body: "I am not sure this is big news. This is what GNU says about the LGPL:\n\nhttp://gcc.gnu.org/onlinedocs/libstdc++/faq.html#faq.license\n\n\"The LGPL requires that users be able to replace the LGPL code with a modified version; this is trivial if the library in question is a C shared library. But there's no way to make that work with C++, where much of the library consists of inline functions and templates, which are expanded inside the code that uses the library. So to allow people to replace the library code, someone using the library would have to distribute their own source, rendering the LGPL equivalent to the GPL.\"\n\nNormally, C++ libraries that are licensed under the LGPL add an extension to address the problems stated above. Only if Nokia does so and adds an extension we have big news. If its the pure LGPL, not much will change.\n"
    author: "Hans2"
  - subject: "Re: License still questionable"
    date: 2009-01-14
    body: "I don't think Nokia did this just to take a piss at people who get excited about this kind of stuff. Surely, all shortcomings like this will be quickly fixed if necessary.\n\nThat is, your pessimism is probably premature."
    author: "Ville"
  - subject: "Re: License still questionable"
    date: 2009-01-14
    body: "Once Nokia has released Qt 4.5 and published the license we will see if the high hopes of many people will actually come true. \n\nFrom the Qt FAQ:\n\n\"The LGPL carries some restrictions regarding the ability for users to relink libraries and other restrictions that may impose architectural requirements that some organizations might not be comfortable with.\"\n\nIt is not clear what this actually translates to. It is definately a very strange way to tell people that they can use Qt for free to build closed source applications.\n\nFrom the Qt FAQ:\n\n\"The commercial Qt license includes email support, access to upgrades and allows you to develop fully closed source software.\"\n\nWhat is fully closed software? Does this mean without a commercial license I can only develop partially closed software? What part must be open and what part can be closed? \n"
    author: "Hans2"
  - subject: "Re: License still questionable"
    date: 2009-01-14
    body: "\"What is fully closed software? Does this mean without a commercial license I can only develop partially closed software?\"\n\nSome people may want to modify Qt itself (proprietary framebuffer stuff?). They would need to get the commercial license. Likewise, if you don't have / don't want a dynamic linker (realistic these days?), you would need commercial license."
    author: "Ville"
  - subject: "Re: License still questionable"
    date: 2009-01-14
    body: "Thats one way to interpret it.\n\nWe need a statement like this from Nokia. I hope for the same like you, but so far its LGPL 2.1 with the consequences explained by GNU, which means very little will change compared to the GPL."
    author: "Hans2"
  - subject: "Re: License still questionable"
    date: 2009-01-14
    body: "That's probably why LGPL 2.1 is used (note the version number).\n\nSee terms 5 and 6:\nhttp://www.gnu.org/licenses/lgpl-2.1.html\n\n"
    author: "HelloWorld"
  - subject: "Re: License still questionable"
    date: 2009-01-14
    body: "Hmm.. right, there seems to be an issue with templates though:\n\nhttp://lab.obsethryl.eu/content/lgpl-21-qt-45-and-c-templates\n\nThat's interesting. So without an added section to the license, you're not allowed to use templated Qt classes..."
    author: "HelloWorld"
  - subject: "Re: License still questionable"
    date: 2009-01-14
    body: "Neither can you use macros like in Qt's implementation of signals and slots and QObject."
    author: "Hans2"
  - subject: "Re: License still questionable"
    date: 2009-01-14
    body: "Hans2,\n\ncheck the macro definitions: \"slots\" and \"emit\"  expand to nothing, \"signals\" is simply \"protected\", and \"SIGNAL\" and \"SLOT\" expand to what YOU put in, with a '0' or '1' added. While I would love to have the copyright on the empty set, or on 0s and 1s used in the context of computers, I doubt this is enforcable :-)\n\nPlease read the licensing FAQ, it is clearly Qt Software's intention to enable closed source software development with Qt, otherwise the change to the LGPL wouldn't make sense at all. The vast majority of Qt's functionality can be linked against, should there be legal issues with the few template tool classes, or a bunch of mostly trivial inline functions (the most complicated ones are probably in QString, and even those are trivial), I'm positive these issues will get addressed and fixed..\n\n\n"
    author: "Matthias Ettrich"
  - subject: "Re: License still questionable"
    date: 2009-01-14
    body: "This is good news. Still, I do not find the licensing FAQ very clear at all! You should really consider adding the exception to the LGPL about C++ templates. And while you are at it, why not extending the exception to allow static linking too? \nThe pure LGPL is not ideal to enable closed software development with a modern C++ library. All meaningful C++ libraries with LGPL-like licenses that I know of did this."
    author: "Hans2"
  - subject: "Re: License still questionable"
    date: 2009-01-15
    body: "Do not get your legal opinions from engineers on forums. Go talk to a real lawyer. In the meantime, stop spoiling the party for everyone else."
    author: "Brandybuck"
  - subject: "Re: License still questionable"
    date: 2009-01-15
    body: "These concerns are not \"opinions from engineers\" but the official interpretation by the FSF of one of their licenses. So far LGPL 2.1 combined with C++ templates, macos and inline functions has the same effect as the GPL."
    author: "Hans2"
  - subject: "Re: License still questionable"
    date: 2009-01-15
    body: "Good that GDK/GTK don't have any macros :-D\n\nSome people on the dot voiced the hope that the licensing discussions could stop now, how naive. I'm not surprised, there always was a lex trolltech/kde, and a set of community rules for everybody else.  The demand to allow static linking is bold at best, what's next, public domain? Would you want to write code that others fork without giving back? If so, I'm afraid, then you speak for a small minority of the free software developer community. \n\nEither way, Brandybuck is completely right, talk to Nokia's legal department, as you won't have Nokia lawyers reading this forum, and you won't have former Trolltech engineers changing licensing terms. An email send to the standard support address will get forwarded to the right people.\n\nMay the party continue for the rest of us.\n\n\n"
    author: "Matthias Ettrich"
  - subject: "Re: License still questionable"
    date: 2009-01-15
    body: "Weird isn't it? I had expected a bit of bitching about C++ being evil, about moc being unclean, about Qt not using Cairo or things. But this same came completely out of the left field. I mean... Are people really daft enough to think that moving Qt to LGPL is a sneaky move to underhandedly force people to GPL their code?"
    author: "Boudewijn"
  - subject: "Re: License still questionable"
    date: 2009-01-15
    body: "People simply raise a point Nokia did overlook or judged its relevance differently than people who will be bound to this license. No one is questioning Nokia's intention, people just feel better when intention and legal wording match. (L)GPL with linking exception would solve this."
    author: "Carlo"
  - subject: "Re: License still questionable"
    date: 2009-01-16
    body: "... and where are the people saying this about Gtk-mm? exactly."
    author: "Aaron Seigo"
  - subject: "Re: License still questionable"
    date: 2009-01-16
    body: "It's not that seldom that license incompatibilities go unnoticed or are ignored - even for a log time, as SGI's license used for OpenGL code highlights\u00b9. Worse than having licensing issues is ignoring them. I don't know about Gtk-mm and don't care either, but if it's the same problem, they should address it.\n\n\n[1] http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=211765"
    author: "Carlo"
  - subject: "Re: License still questionable"
    date: 2009-01-15
    body: "Sorry Matthias, but this is not about spoiling the party or bitching about Trolltech or Nokia or KDE. It is pretty clear that Nokia wants to allow commercial software developers to develop closed source applications using the LGPL version of Qt. I think this is great for KDE because it makes KDE more interessting for commercial software developers that cannot / do not want to develop open source applications.\n\nHowever, the chosen license (LGPL 2.1) is problematic because it does not allow you to include header files that include macros, inline functions or templates with more then ten lines of code. I think especially templates are problematic because they are used in lots of places. If people on the dot realize the problems with LGPL 2.1 and C++ libraries, commercial software developers will realize that, too and they might not use Qt because of this. This cannot be in the interest of Nokia. There are plenty of other libraries that are licensed unter LGPL 2.1 and that do include macros, templates and inline functions. But just because other people chose questionable licenses for their libraries does not mean QtSoftware/Nokia should do the same.\n\nThis also has nothing to do with allowing static linking or public domain. I posted the problems with C++ libraries and LGPL 2.1 because I really like KDE, I think Qt is a great software library and I think it is best to address this problem before Qt 4.5 is released (in case there is not already an exception in header files including templates). I also posted this because I thought some people from QtSoftware (like you, Matthias) might read this and talk to the people that are responsible for license issues at QtSoftware/Nokia. I am sorry if some people feel this is trolling. And I am also a bit disappointed that people assume that I (or the other posters on this subject) think \"that moving Qt to LGPL is a sneaky move to underhandedly force people to GPL their code\".\n\nMatthias if you think the concerns mentioned here about LGLP 2.1 and C++ headers including templates, inline functions or macros are valid, could you please mention that to someone from the QtSoftware/Nokia legal department?"
    author: "Michael"
  - subject: "Re: License still questionable"
    date: 2009-01-15
    body: "As per the link someone else posted before you posted this (http://dot.kde.org/1231920504/1231996715/1232014659/), people at Nokia legal have been made aware of the issue and are resolving it. Already. As they would have done if someone had contacted Qt Software directly, instead of posting their concerns all over the internet, making it seem as if they are really more interested in pissing on the parade than getting their concerns resolved."
    author: "Boudewijn Rempt"
  - subject: "Re: License still questionable"
    date: 2009-01-15
    body: "I thought this was an open, honest and fair discussion based on facts without insulting anyone or blaming Qt Software. Open Source means that concerns are expressed in public and not on the answering machine of Nokia lawyers."
    author: "Hans2"
  - subject: "Re: License still questionable"
    date: 2009-01-16
    body: "that's like saying the best way to get a bug fixed in Qt is to talk to the legal department.\n\nyou talk to the contextually correct people.\n\nas for LGPL issues, i'm impressed and dismayed that only when *Qt* goes LGPL that this all comes up. i hope and expect you to go around to every major LGPL'd library and correct them too.\n\nthere's a real double standard at play. not one that i think is necessarily intentional .. it's just, culturally, fair ball to hold Qt under a scanning tunneling electron microscope while the rest of the world gets free passes.\n\na little fair play would be welcome, and a little pragmatism would too (referring to the issue of expecting legal issues to get sorted on a technical interest forum.)"
    author: "Aaron Seigo"
  - subject: "Re: License still questionable"
    date: 2009-01-15
    body: "> As per the link someone else posted before you posted this \n> (http://dot.kde.org/1231920504/1231996715/1232014659/), \n> people at Nokia legal have been made aware of the issue and are resolving it.\n\nSorry, I only read this after I wrote my last post.\n\n> As they would have done if someone had contacted Qt Software directly, \n> instead of posting their concerns all over the internet, making it seem \n> as if they are really more interested in pissing on the parade than getting\n> their concerns resolved.\n\nI have already explained that I posted this on the dot because I thought someone from QtSoftware/Nokia (like Matthias) might read this and contact the responsible persons at Nokia. I would have sent these concerns directly to QtSoftware/Nokia's legal department if I would have known an email address. But I do not know and I don't think that posting to some offical address would be a good idea because it is not clear that the email will ever be forwarded to the correct person.\n\nThere already were discussions about the use of LGPL 2.1 and C++ on a German linux forum. There actually were people assuming that QtSoftware/Nokia do not want to allow people to develop closed source software with the LGPLed version of Qt. That's why I brought this up here. Because I do not want these false assumptions to spread. Because I think things like that should be stopped as soon as possible and that can only be done by making it clear that this is not what QtSoftware/Nokia wants and stating this in the actual license. The best way to stop licensing FUD is to make it impossible to create lincensing FUD in the first place by fixing the license. \n\nI included my email address in my first post. You could just have asked me why I am posting this instead of just assuming I (and other posters) are trolls that wants to spoil the party. And since Nokia's legal department is aware of this now and tries to resolve it, my (and other people's) concern with LGPL 2.1 and C++ libraries seem to be valid and it might even have been a good thing to post this here. But I really think I said enough about this now, so there will be no further comments about this from me."
    author: "Michael"
  - subject: "Re: License still questionable"
    date: 2009-01-16
    body: "> someone from QtSoftware/Nokia (like Matthias) might read this\n...\n> But I do not know and I don't think that posting to some offical address would \n> be a good idea because it is not clear that the email will ever be forwarded to \n> the correct person.\n\nMatthias is not a lawyer (neither am i, of course).\nthe message has been passed on to the right people.\nthere are people whose job it is to look after such things (passing concerns to the right people).\n\n> The best way to stop licensing FUD is to make it impossible to create \n> lincensing FUD in the first place by fixing the license. \n\nagreed. which is what will happen. i still think it's fascinating that it comes out around Qt instead of any of the other LGPL'd frameworks that carry bulks of inlined code with them.\n\nmaybe our community just cares more.\nmaybe our community is more paranoid about FUD being spread because of past events, resulting in us gnawing our own feet off.\n\n> concern with LGPL 2.1 and C++ libraries\n\nit's not unique to C++ libraries, and some others have taken legal steps to fix it previously. so there is precedent. there are also far more who haven't.\n\nin the meantime, it would be very nice to be able to have just one \"huzzah! cool stuff!\" day at some point without someone coming to the party with a wet blanket. it's really disheartening at times putting up with that every. single. time.\n\ni really don't blame Matthias when he shows some bitterness because after a while it gets old and one starts to feel that those other people just don't deserve what you're giving to them.\n\nthere's some really deep matters to reflect upon here, and i hope people do so."
    author: "Aaron Seigo"
  - subject: "Re: License still questionable"
    date: 2009-01-17
    body: "Aaron,\n\n\"i still think it's fascinating that it comes out around Qt instead of any of the other LGPL'd frameworks that carry bulks of inlined code with them.\"\n\nAll meaningful C++ LGPL-like licensed libraries have added an exception for inline code and static-linking. See wxWidgets, fltk, fox, gtkmm, gnu-stdc++.\n\n\"Matthias ...shows some bitterness...\"\nIf this licensing issue comes up here, it is not a reason to to be bitter. It shows that Qt is in the focus of many people and that the people who bring it up here and now don't have bad intentions. Much more damage could be done if the issue had been brought up *after* the license change had been made. \n\nYes, there are pure LGPL 2.1 C++ libraries out there, but they do not have the user base as the toolkits mentioned above or Qt. "
    author: "Hans2"
  - subject: "Re: License still questionable"
    date: 2009-01-17
    body: "http://www.gtkmm.org/license.shtml does not mention an exception for gtkmm."
    author: "Boudewijn Rempt"
  - subject: "Re: License still questionable"
    date: 2009-01-19
    body: "That is because the LGPL version 3 specifically adds this text:\n\n<start quote>\n3. Object Code Incorporating Material from Library Header Files.\n\nThe object code form of an Application may incorporate material from a header file that is part of the Library. You may convey such object code under terms of your choice, provided that, if the incorporated material is not limited to numerical parameters, data structure layouts and accessors, or small macros, inline functions and templates (ten or fewer lines in length), you do both of the following:\n\n    * a) Give prominent notice with each copy of the object code that the Library is used in it and that the Library and its use are covered by this License.\n    * b) Accompany the object code with a copy of the GNU GPL and this license document.\n<end quote>\n"
    author: "slougi"
  - subject: "Re: License still questionable"
    date: 2009-01-15
    body: "Thanks for clarifying. However, I really think you should add an exception like libstdc++ does:\n\nAs a special exception, you may use this file as part of a free software\n       library without restriction.  Specifically, if other files instantiate\n       templates or use macros or inline functions from this file, or you compile\n       this file and link it with other files to produce an executable, this\n       file does not by itself cause the resulting executable to be covered by\n       the GNU General Public License.  This exception does not however\n       invalidate any other reasons why the executable file might be covered by\n       the GNU General Public License.\n\nOtherwise commercial developers would have to be very careful not to use any header files that contain templates (or include other header files that include templates). Basically that would render the LGPL Qt edition useless for commercial developers. I doubt that commercial developers take this risk. I think it is not enough that QtSoftware gives permission in a FAQ that closed source software may link to the LGPL edition of Qt. It has to be allowed by the license itself."
    author: "Markus"
  - subject: "Re: License still questionable"
    date: 2009-01-15
    body: "So you got lucky this time. Q_OBJECT is a bit longer, but you are still lucky.\n\nUsing the LGPL 2.1 also means that developers are restricted to use C++ language features only in a way that does not conflict with the LGPL 2.1. I doubt a developer will want to check his/her code every time if it complies with the LGPL.\n\nC++ is not very attractive without templates. And while macros are certainly overused I would not generaly exclude the possibility to write a longer macro."
    author: "kme"
  - subject: "Re: License still questionable"
    date: 2009-01-14
    body: "When I read that Qt 4.5 will be licensed as LGPL this morning, I was really happy because I think this will give KDE a huge boost. Unfortuately back then I did not know that LGPL 2.1 is basically the same as GPL for C++ libraries. If Nokia is really adding LGPL 2.1 without exceptions nothing changes and Qt / KDE will still not be an option for many commercial software companies.\n\nI really like KDE (I even was a KDE developer a couple of years ago) and it is sad to see that all major distributions are using Gnome as their default desktop now (except maybe Novell, but Gnome is also their default for their enterprise products). Making Qt available for free to commercial software developers would maybe help to reverse this trend. And it would also help to bring more developers to Qt which would make Qt a better product. And that would also benefit Nokia which wants to use Qt for their own products.\n\nKDE 4.2 is really great and I don't think KDE as a project will go away anytime soon, but in the long run KDE will get less significant because non of the major players (Redhat, Sun, Ubuntu) really supports KDE anymore. I really hope Nokia does the right thing and make Qt available for free to commercial developers but from all I read so far I doubt it. And then all the hype generated by \"Qt will be LGPL\" will have the opposite effect because people will be quite disappointed when they realize that LGPL 2.1 for a C++ library basically means GPL."
    author: "Hans"
  - subject: "Re: License still questionable"
    date: 2009-01-15
    body: "I would consider Novell and Nokia to be major players."
    author: "Brandybuck"
  - subject: "Re: License still questionable"
    date: 2009-01-16
    body: "remind me never to invite you to my birthday party. you'd be the person showing up to remind me that i'm older than i've been, which means i'm closer to my death than ever before.\n\n> really supports KDE anymore\n\nis that we nailed that 50 million + student roll out this past year?\nis that why Red Flag, #1 desktop Linux retailer in the world, sold 4 million retail units of KDE+Linux on Dell/HP hardware last year?\nor how Mandriva has done several massive roll outs in the last couple years? (they are almost exclusively OEM deals these days)\nor the EEE PC?\nor ...\n\nbut you say Sun is a major player, which makes me laugh. a major player in what? they aren't even a major player in the server market anymore. they are a well known name, but hardly a client side player.\n\nRed Hat? they still have an expressed _non_ interest in the desktop space.\n\nas for Ubuntu, they are popular in the enthusiast crowd but are still making their way into a toe hold in the \"Real World\". let's come back to them in a couple years and see how it pans out.\n\n> but from all I read so far I doubt it\n\nso you read that this is exactly their plan, and based on that you determine based that it isn't their plan. hu-wah? *double take*\n"
    author: "Aaron Seigo"
  - subject: "Re: License still questionable"
    date: 2009-01-14
    body: "When I read that Qt 4.5 will be licensed as LGPL this morning, I was really happy because I think this will give KDE a huge boost. Unfortuately back then I did not know that LGPL 2.1 is basically the same as GPL for C++ libraries. If Nokia is really adding LGPL 2.1 without exceptions nothing changes and Qt / KDE will still not be an option for many commercial software companies.\n\nI really like KDE (I even was a KDE developer a couple of years ago) and it is sad to see that all major distributions are using Gnome as their default desktop now (except maybe Novell, but Gnome is also their default for their enterprise products). Making Qt available for free to commercial software developers would maybe help to reverse this trend. And it would also help to bring more developers to Qt which would make Qt a better product. And that would also benefit Nokia which wants to use Qt for their own products.\n\nKDE 4.2 is really great and I don't think KDE as a project will go away anytime soon, but in the long run KDE will get less significant because non of the major players (Redhat, Sun, Ubuntu) really supports KDE anymore. I really hope Nokia does the right thing and make Qt available for free to commercial developers but from all I read so far I doubt it. And then all the hype generated by \"Qt will be LGPL\" will have the opposite effect because people will be quite disappointed when they realize that LGPL 2.1 for a C++ library basically means GPL."
    author: "Hans"
  - subject: "LGPL 2.1, Qt 4.5 and C++ templates"
    date: 2009-01-15
    body: "\"LGPL 2.1, Qt 4.5 and C++ templates\":\nhttp://lab.obsethryl.eu/content/lgpl-21-qt-45-and-c-templates\n\n\"There is an old moral here; for what C++ templates are concerned to the point where third party code is making use of the library and instantiating itself such templates, the GPL and LGPLv2.1 licenses are in effect equivalent in GPL behaviour of the template - based code, while LGPLv3 corrects this slight but mephistophelian oversight on behalf of the LGPLv2.1. This has been discussed over and over by several developers and you have the FSF being clear about it as in the libstdc++ case and the corrections made for LGPLv3.\"\n\nThis issue needs to be worked out before the license change can be a success."
    author: "Martin"
  - subject: "Re: LGPL 2.1, Qt 4.5 and C++ templates"
    date: 2009-01-15
    body: "See comment by Marius here:\n\nhttp://labs.trolltech.com/blogs/2009/01/14/nokia-to-license-qt-under-lgpl/#comments\n\n\"Legal has been made aware of the issue, and are working on an exception for the LGPL 2.1, to fully enable commercial development towards the LGPL library with all its templates and inline functions, as was the intention.\"\n\nSo stop worrying and keep celebrating :-)"
    author: "Thorben"
  - subject: "The world is filled with love!"
    date: 2009-01-15
    body: "WONDERFUL! I LOVE THIS WORLD WITH Qt4.5! I LOVE PROGRAMMING I LOVE EVERYTHING AND EVERYONE INCLUDING YOU!"
    author: "Magi Su"
  - subject: "Re: The world is filled with love!"
    date: 2009-01-15
    body: "Great :-)"
    author: "Axl"
  - subject: "Thanks Nokia!"
    date: 2009-01-15
    body: "I bet so many geeks are peeing in their pants because of this news :D :D\n\nRegarding GTK+ and GNOME, I hope that they can coexist well with Qt/KDE and even they can cooperate more. Competition is good, this news might encourage wxWidgets and GTK+ guys to work harder to polish their libraries. And people do have preferences (like prefer C over C++ for GUI programming, or prefer more lightweight library like FLTK)\n\nFrom my POV, I prefer GPL, but considering the huge impact of LGPL-ed Qt, I also support this movement ;)"
    author: "fred"
  - subject: "KBasic to become part of KDE?"
    date: 2009-01-15
    body: "Has anyone seen the recent (last Monday?) announcement of KBasic 1.87 ? (KBasic is a Qt-based implemenation of the Basic programming language, and it is cross-platform, and it can be used to easily port Windows Basic programs over to Linux, because the language syntax is the same...)\n\nSee here: http://www.kbasic.com/forum/viewtopic.php?f=5&t=261\n\nThe developer claims there that he now wants KBasic to become part of the KDE project."
    author: "kobserver"
  - subject: "Re: KBasic to become part of KDE?"
    date: 2009-01-15
    body: "\"it can be used to easily port Windows Basic programs over to Linux, because the language syntax is the same...\"\nnice! :)"
    author: "Koko"
  - subject: "KBasic useless"
    date: 2009-01-15
    body: "Too bad KBasic is effectively proprietary software. Have you tried compiling their \"source codes\"?"
    author: "Luke"
  - subject: "Current KBasic is not proprietary, it's GPL3"
    date: 2009-01-15
    body: "No, I've not compiled their source code.\n\nDid you some research before spreading FUD?\n\nAccording to this source: http://www.kbasic.com/doku.php?id=kbasic_s_license  KBasic is (dual) licensed (also) under the GPL3. I have no reason to doubt the contents of that website and swap it against your unfounded \"But it's proprietary!\" assertion."
    author: "kobserver"
  - subject: "Re: KBasic useless"
    date: 2009-01-16
    body: "His software code was GPL open source but because he was the only developer he turned it into a proprietary model. If it becomes part of the KDE project it will be in the playground to maturate and it will be possible to compile it."
    author: "Andre"
  - subject: "Re: KBasic useless"
    date: 2009-01-17
    body: "Whatever the past....  Looks like _NOW_ it's GPL3."
    author: "anon"
  - subject: "Qt/Embedded ?"
    date: 2009-01-15
    body: "Hi,\n\ndoes this also include Qt/embedded (or what the current name was) ?\nIOW are the per-device royalties also gone now and the LGPL version can be used for closed source apps ?\n\nWhat about Qt/Extended ?\n\nAlex\n"
    author: "Alex"
  - subject: "Re: Qt/Embedded ?"
    date: 2009-01-15
    body: "Yes, the LGPL will be valid for all platforms:\n\nDesktop/Server:\n* X11 (Linux, Solaris, HP-UX, IRIX, AIX, ...)\n* Windows (XP, Vista, 2003, ME/98)\n* Mac OS X (PPC, Intel)\n\nMobile/Embedded:\n* Embedded Linux\n* Windows CE\n* Symbian (S60) is planned\n\n"
    author: "Olaf Jan Schmidt"
  - subject: "Re: Qt/Embedded ?"
    date: 2009-01-15
    body: "But I guess not for the additional applications part of Qt/Extended ?\n\nAlex\n"
    author: "Alex"
  - subject: "Re: Qt/Embedded ?"
    date: 2009-01-15
    body: "http://www.qtsoftware.com/products/device-creation/qt-extended\n"
    author: "Alex"
  - subject: "Re: Qt/Embedded ?"
    date: 2009-01-15
    body: "http://www.qtsoftware.com/about/licensing/frequently-asked-questions#does-the-licensing-change\n\nDoes the licensing change apply to Qt Extended?\nNo, the licensing change does not apply to Qt Extended."
    author: "Thorben"
  - subject: "Re: Qt/Embedded ?"
    date: 2009-01-15
    body: "IRIX is no longer supported as of Qt 4.5.\n\nWe support, as Unix platforms:\n - MacOS X (X11 and not)\n - Linux\n - Solaris\n - HP-UX / HP-UXi\n - AIX\n\nThe BSDs are not directly supported by us, but they are known to work and supported by the community. We apply fixes sent to us."
    author: "Thiago Macieira"
  - subject: "Nokia should talk to Adobe!"
    date: 2009-01-15
    body: "Nokia should talk to Adobe!There are no limitations now to port CS to Linux. "
    author: "origin"
  - subject: "Re: Nokia should talk to Adobe!"
    date: 2009-01-15
    body: "Does all CS is developped using QT?\nI'd be indeed a lot interested in a Linux After Effect port."
    author: "Richard Van Den Boom"
  - subject: "Re: Nokia should talk to Adobe!"
    date: 2009-01-15
    body: "I doubt that the cost of a few commercial Qt licenses was the problem for Adobe to port software to Linux."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: Nokia should talk to Adobe!"
    date: 2009-01-16
    body: "I'm more thinking in the line open development process and assurance that your products don't depend on other company which LGPL makes possible for Adobe.     "
    author: "origin"
  - subject: "Thanks Guys!"
    date: 2009-01-15
    body: "I'll definitely be using QT a lot more now ;)\n"
    author: "Mark Hannessen"
  - subject: "Remove all redundancy between Qt and Glib"
    date: 2009-01-16
    body: "The ideal way to merge KDE and GNOME is to build Qt on top of Glib such that there is absolutely no redundancy between the two (ie. ASCII conversion, File access, etc). This way Qt apps won't incur a memory penalty if Qt is compiled with Glib support.\n\nQtCore should rely on Glib to do the low-level stuff. Unfortunately this will mean sacrifices on all sides (ie. GTK will need to die), but eventually will lead to a unified Free Desktop."
    author: "Stan"
  - subject: "Re: Remove all redundancy between Qt and Glib"
    date: 2009-01-16
    body: "> won't incur a memory penalty if Qt is compiled with Glib support.\n\ndo you have #s on the size of this memory penalty?\n"
    author: "Aaron Seigo"
  - subject: "Re: Remove all redundancy between Qt and Glib"
    date: 2009-01-17
    body: "I don't see the point in dealing with the whole glib. The only \"interesting\" glib functionality (from Qt's perspective) is the mainloop.\n\nPerhaps glib mainloop should be made a separate module (\"linux mainloop\"), and move all toolkits over to use that..."
    author: "Ville"
  - subject: "Qt + LGPL analysis"
    date: 2009-01-16
    body: "A complete analysis of Qt+LGPL can be found here\n\n   http://www.ics.com/files/docs/Qt_LGPL.pdf\n\nIt seems to be a professional analysis of someone who knows what he is talking about.\n\nGreetings\nJens"
    author: "Jens"
  - subject: "Thief! Robber! Give me back my money!"
    date: 2009-01-16
    body: "I've paid BIG$$ a year ago for a commercial license, so I don't need to show everyone my $prec$ious code, and now I can do the same without giving away a single buck!\n\nCutthroats!\n"
    author: "Pokerface"
  - subject: "Cmp. Ubuntu Brainstorm"
    date: 2009-01-16
    body: "http://brainstorm.ubuntu.com/idea/9833/"
    author: "Andre"
  - subject: "Qt and GTK+"
    date: 2009-01-16
    body: "I think it is impossible to have one toolkit like it is impossible to have only one distro, but since both toolkits will be licenced under LGPL now, i would like to see them share features/ideas, talk to each other and have some standarts. As a user, i want GTK+ and Qt applications to have the same functionality under Gnome and KDE (for example: look and feel, file open dialogs, ability to use drag and drop, which sometimes does not work). It would be great. But it seems that (sadly) toolkit developers have no interest in that (Qt will have a GTK style in 4.5, but there is no response from GTK+ developers) and don't see anything happening soon."
    author: "Zolookas"
  - subject: "Misconception about the press release"
    date: 2009-01-17
    body: "I think most people misunderstand the press info.  Adding LGPL as a license option allow developers to use Qt for commercial purposes without having to pay a license for Qt.  Now you only pay for the support.  That will bring more developers to the platform, and probably more programs for Linux, and that's good news.\n\nWhat means for Gnome:   probably nothing. While they are talking about breaking ABI compatibility with 3.0, is not the same as switching a toolkit.  To clear a concept to non programmers:\n    A. Breaking API/ABI compatibility means that they are free to add new library calls and change old ones (arguments, return values, etc) so that an older application will likely not work unless a programmer fix the calls.\n    B. Rewriting means throwing everything to the basket and start from scratch.  And thats not what Gnome/Gtk developers have been talking about.\n\nIn short, if they are not rewriting Gtk, less they are going to switch the framework.\n\n"
    author: "Mario Vazquez"
---
Nokia <a href="http://www.qtsoftware.com/about/news/lgpl-license-option-added-to-qt">has announced</a> that starting with version 4.5, <a href="http://www.qtsoftware.com">Qt</a> will be available under the LGPL 2.1.  From the announcement,
<blockquote>
<em>The move to LGPL licensing will provide open source and commercial developers with more permissive licensing than GPL and so increase flexibility for developers. In addition, Qt source code repositories will be made publicly available and will encourage contributions from desktop and embedded developer communities. With these changes, developers will be able to actively drive the evolution of the Qt framework.</em>
</blockquote>
This exciting change, made with consultation of the <a href="http://www.kde.org/whatiskde/kdefreeqtfoundation.php">
KDE Free Qt Foundation</a>, should encourage KDE and Qt use among commercial and proprietary developers and makes the philosophy of "Qt Everywhere" complete.
<!--break-->
<p>Kai Öistämö, Executive Vice President of Devices at Nokia expands,</p>
<blockquote>
"<em>By opening Qt&#8217;s licensing model and encouraging more contributions, Qt users will have more of a stake in the development of Qt, which will in turn encourage wider adoption.</em>"
</blockquote>

<p>The change in licensing for Qt is happening under the mantra "Qt 
Everywhere" and is a step to remove any and all possible blocking objections 
for not using Qt. Nokia explains further,</p>
<blockquote>
Qt will be available under the LGPL version 2.1 with the upcoming Qt 4.5 release. Qt will also continue to be licensed under commercial license terms, as well as the GPL, version 3.0.
</blockquote>

<p>Nokia will also be opening up the development of Qt. A clear path for the 
extension of external contributions is currently being built, including 
publicly available source code repositories and access to the same level of 
support, independent of the license.</p>

<p>The simple fact behind this is that Nokia is less reliant on the 
income from Qt licensing than Trolltech (now Qt Software) was, and this has 
given Qt Software more room to approach the market with more permissive 
licensing strategies in order to increase adoption of the Qt toolkit.  
This is part of the 10x growth target announced at last year's Akademy
to achieve ten times as many developers and ten times as many free software community users.  
Or as KDE and Qt developer Thiago Maciera put it "<em>we want KDE to be ten times as big</em>".</p>

<p>While kdelibs has always been available under the LGPL, this marks the first 
time that both Qt and kdelibs will both be available under the LGPL.
This will help make the licensing of KDE's libraries more permissive, flexible 
and coherent.  KDE's licensing is summarised on <a 
href="http://techbase.kde.org/index.php?title=Policies/Licensing_Policy">TechBase</a>, 
and can be described as <em>"LGPL or equivalent for libraries, GPL otherwise"</em>.</p>

<p>Meanwhile, Nokia reiterates their commitment to a commercially viable and, 
technology-wise, best-of-breed toolkit. In fact, the performance and  
functionality improvements that can be seen in the upcoming Qt 4.5 release are 
impressive. Running KDE 4.2 with Qt 4.5 is already being tested by several 
engineers inside Qt software, which has resulted in a number of bug fixes in 
both KDE and Qt. Independent of the licensing changes, the KDE release team 
plans to update the version of Qt in KDE's development tree (qt-copy) shortly 
to snapshots of Qt 4.5 which is due to be released in March.</p>

<p>All-in-all today's news means tremendous things for Free and Open Source software. The 
possibility of extending the reach of all of our work is exciting in and of 
itself, and this announcement could lead to a veritable explosion of Qt and 
KDE adoption.</p>