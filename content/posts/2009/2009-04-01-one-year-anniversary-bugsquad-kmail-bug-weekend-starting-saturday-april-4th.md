---
title: "One Year Anniversary of BugSquad: KMail Bug Weekend Starting Saturday, April 4th"
date:    2009-04-01
authors:
  - "jriddell"
slug:    one-year-anniversary-bugsquad-kmail-bug-weekend-starting-saturday-april-4th
comments:
  - subject: "I'd love to."
    date: 2009-04-01
    body: "If at all possible I'll try to join this effort. Hopefully there's something I can do.\r\n\r\nOn a related note, while speaking about KMail, are there any plans to connect it to OpenChange? http://www.openchange.org/ \r\n\r\nIt would seem that they're getting closer and closer and from what I gather the latest Ubuntu already uses this to get Evolution to talk to MS-Exchange.\r\n"
    author: "osh"
  - subject: "Re: I'd love to."
    date: 2009-04-01
    body: "There's an OpenChange akonadi resource that originated from a gsoc project last year. Unfortunately it couldn't be fully merged into KDE because there were still some things lacking on the OpenChange API. Still it's around and will hopefully be ready soon.\r\n\r\nHope to see you around on saturday :)"
    author: "lemma"
---
One year ago, on April 6th, <a href="http://techbase.kde.org/Contribute/Bugsquad">BugSquad</a> had the first BugDay. Since then, an astounding 93 people have participated in one.  For many, it was their first active involvement in the KDE community. Join us as we celebrate in style!

How? By having a triage session, of course! Starting Saturday, April 4th, we will go <a href="http://techbase.kde.org/Contribute/Bugsquad/BugDays/KMailDay4">through the bugs in KMail</a>. We will continue through to Sunday, but with us, the party never stops, so drop by any time.

Where? On #kde-bugs on irc.freenode.net
<!--break-->
All you need to join is yourself and a copy of KDE's KMail 4.2.x, if you are not sure how to install it, join earlier in the week and we can help you get set up. You do not need to know how to program, we will teach you what you need to
know. This is a fun and easy way to get involved with the KDE community!
