---
title: "Vibrant Community Propels KDE Forward at Akademy 2009"
date:    2009-07-14
authors:
  - "rich"
slug:    vibrant-community-propels-kde-forward-akademy-2009
comments:
  - subject: "Wish I was there"
    date: 2009-07-14
    body: "All I can say is, \"I wish I could have been there.\" :)\r\n\r\nOh well, there'll be more events, so more chances will arise.\r\n\r\nCongrats to the organizing team on a successful conference."
    author: "troy"
  - subject: "recordings?"
    date: 2009-07-14
    body: "All the reports from this summit sound really great... Will there be video or audio recordings available from the presentations?"
    author: "BeS"
  - subject: "video recordings to come"
    date: 2009-07-15
    body: "Yes as far as I know, all talks have been taped. It usually takes some time until the recordings can be made available. Most people are rather exhausted after Akademy ..."
    author: "sebas"
  - subject: "Nepomuk"
    date: 2009-07-15
    body: "What I would like to see is the option of adding tags to your files in the save dialog. It doesn't make sense to first save your files and then use dolphin to browse to the desired file and then do the tagging. I'm thinking in something as firefox save bookmark dialog, you specify the name of your file, the location, and its tags."
    author: "Krul"
  - subject: "Report as a bug/wish please"
    date: 2009-07-15
    body: "Could you report this, IMHO excellent, idea in the bugtracker? This way, it won't get lost. "
    author: "andresomers"
  - subject: "It is already being worked on"
    date: 2009-07-16
    body: "It is already being worked on as part of GSoC ;-)"
    author: "nightrose"
  - subject: "Gitorious"
    date: 2009-07-16
    body: "\"...roadmap of necessary actions for the KDE development process to move to Gitorious, a distributed version control system based on Git. An exact timeframe could not be give, although the move might even be made before KDE 4.4 in January 2010, or shortly thereafter.\"\r\n\r\nWhere can I find more information on this one? Techbase has a page about Gitorious for KDE, but there has not been any activity for one year."
    author: "majewsky"
  - subject: "Check the kde-scm-interest mailing list"
    date: 2009-07-16
    body: "You can find the minutes of the BoF at http://kde.markmail.org/message/u6eqfjece7fibfyo ."
    author: "mahoutsukai"
---
Akademy 2009 was held as part of the <a href="http://www.grancanariadesktopsummit.org">Gran Canaria Desktop Summit</a> in Las Palmas de Gran Canaria last week. During the week the KDE community attended conference presentations and was engaged in meetings, informal discussions and of course a great deal of coding. This article provides an overview of what's happening in the KDE universe and what can be expected for the future.<br />

The KDE development platform gains support by Maemo's announcement to include Qt as supported platform in its next release. The semantic desktop gained traction with wider adoption of its underlying infrastructure. The Plasma team showed their upcoming netbook interface; a first version is expected to be available in the first quarter of next year. Cornelius Schumacher reports that KDE e.V., the organization behind KDE, is healthily growing and new projects to grow KDE's developer base and industry adoption are being spawned. A shift to next generation code hosting using the version control system git and its web interface Gitorious was announced, creating a way for more people to contribute to KDE in the future.
<div style="border: thin solid grey; padding: 1ex; width: 500px; text-align: center; margin-left: auto; margin-right: auto">
<img src="http://dot.kde.org/sites/dot.kde.org/files/gcds-opening.jpg" width="500" height="238" /><br />
Gran Canaria Desktop Summit
</div>
<!--break-->
<h2>Community Healthy and Growing</h2>
<p>

In his report from the KDE e.V. Board, Cornelius Schumacher summarizes KDE e.V.'s (the association to support KDE) current state. Schumacher, who has been KDE e.V.'s treasurer in the last years, was elected during this year's general assembly as president, following up Aaron Seigo. Schumacher tells us <em>"While we have vastly increased our support of KDE, we are also able to create a situation in which KDE e.V. stands firm, financially and as an organization. We have sponsored 18 developer sprints last year, and were able to provide financial and organizational support for many more. The budget for this has already been secured. Developer sprints are a great way to make bigger steps more quickly for many projects in KDE. By bringing together creative minds, we have both an immediate effect on the development, but also a long-term effect resulting in stronger bounds within the community."</em>
KDE's chief usability expert Celeste Lyn Paul and Frank Karlitschek, the  brain behind the Social desktop were elected as new KDE e.V. Board Members. Celeste was also <a href="http://dot.kde.org/2009/07/06/akademy-awards-2009">awarded this year's Akademy Award</a> in the category of "non-application". David Faure was awarded for his long-time contribution to KDE. Peter Penz won the "Application" category for his work on the Dolphin file manager.<p />
During Akademy, KDE and OpenBossa developer Artur Souza <a href="http://dot.kde.org/2009/07/10/qt-labs-america-and-other-akademy-talks-sessions">announced</a> the <strong>Qt/KDE Research Lab in South America</strong>. The lab is a concerted effort to bring Qt and KDE development into universities with the aim of growing the developer base. South America's Qt/KDE Research labs are modelled after the <a href="http://www.grancanariadesktopsummit.org/node/133">successful project</a> in Toulouse, France which has been run by Kevin Ottens for a few years already.<p />
Frank Karlitschek <a href="http://dot.kde.org/2009/07/11/open-pc-project-announced-gcds-09">announced</a> the <a href="http://www.open-pc.com">Open-PC</a> project, a community-based effort in in collaboration with hardware vendors to provide a high-quality Free Desktop experience, shaped and fully integrated for specific computers.
<br />

</p>

<h2>Semantic Desktop Gaining Traction</h2>
<p>
One of the areas that will see an increased activity by means of focused developer meetings is the Semantic Desktop. The semantic desktop is a multi-year project of KDE, and while the first results can already be seen by a powerful file search engine and a desktop-wide tagging and rating system, the full potential of this new technology has not yet been realized.<br />
During his keynote at Akademy, Sebastian Kügler announced an increased focus of moving KDE towards the Semantic Desktop. "KDE e.V. will sponsor a series of developer meetings that focus on integrating these features into the desktop. We invite teams inside KDE to think about how their software can benefit from the semantic framework introduced in KDE 4. The semantic desktop has the potential of being a game-changer for the Free Desktop, as it provides a way to model the user's data closer to how the human brain does it. It will move the computer's user interface one step closer to the user."<br />
Sebastian Trüg, who works on Nepomuk, the underlying semantic infrastructure in the KDE platform has agreed to provide guidance and help when implementing semantic features in more applications.<br />
Nepomuk developer Laura Dragan <a href="http://www.grancanariadesktopsummit.org/node/147">presented</a> her research work on <strong>semantic context menus</strong> in KDE. These context menus provide other object that are semantically close to the object in question.
<p />
Jos van den Oever, the main developer of Strigi, announced that Strigi's underlying indexing libraries will be <strong>adopted by the Tracker</strong> project as well. Strigi and its libraries libstream and libstreamanalyzer are used to index files and metadata inside Nepomuk.
A new feature, that has just before Akademy been committed to what is becoming KDE 4.4 now records the origin of files downloaded and copied by applications using the KDE KIO library, the standard, network-transparant transfer mechanism.
</p>

<h2>Development Platform Thrives</h2>
<p>
Sharing indexing technologies and <a href="http://dot.kde.org/2009/07/09/cooperation-during-gran-canaria-desktop-summit">collaboration in other technical areas</a> is only one of the positive aspects of a shared conference. KDE and GNOME developers have also taken the opportunity of holding their conferences in the same place to sit together and work on ways to improve the standardization process of shared specifications.A proposal for a specification process for freedesktop.org has been made and got basic consensus.
<p />
During the first day of cross-desktop keynotes, Quim Gil, who works for Nokia, announced the switch of Maemo to Qt. A <strong>Qt-based Maemo</strong> will offer easier integration with KDE technologies on mobile devices, so the announcement has been received positively by many people inside KDE. The underlying software stack, most of which is shared between KDE and 
GNOME, will remain relatively unchanged.
<div style="border: thin solid grey; padding: 1ex; width: 500px; text-align: center; margin-left: auto; margin-right: auto">
<img src="http://dot.kde.org/sites/dot.kde.org/files/maemo-qt.jpg" width="500" height="333" /><br />
Gran Canaria Desktop Summit
</div>
<p />
A Birds-of-a-Feather session on Wednesday resulted in a clear roadmap of necessary actions for the KDE development process to <strong>move to Gitorious</strong>, a distributed version control system based on Git. An exact timeframe could not be give, although the move might even be made before KDE 4.4 in January 2010, or shortly thereafter. Gitorious provides a nice user interface on top of Git. Gitorious developers who were present in Gran Canaria have offered their help to provide a powerful development collaboration system for KDE. One of the focal points of the team is to provide clear communication and documentation about this move, so it all happens with as little as possible disruption to developers as possible.
</p>

<h2>Improving the User Experience</h2>
<p>
During Akademy, the Plasma and KWin developers sat together and worked on topics such as <strong>deeper integration with the KWin window manager</strong>, a special user interface for <strong>Netbooks</strong> and deeper integration with the Web. On the last days of Akademy, the Plasma team was able to show a first version of a plugin that will integrate data from Wikipedia into the netbook's user interface. This KRunner plugin will also be available for the "normal" desktop, thanks to Plasma's modular architecture. A alpha version of the new Plasma Netbook shell is planned for Q3 2009, a first release might be ready as soon as Q1 2010. The design phase and a <a href="http://www.youtube.com/watch?v=iULB1zE7EJE">screencast of a rough prototype</a> show good progress. The Plasma team believes that introducing a specialized user interface for netbooks makes these devices more useful as device-specific limitations such as screenspace, but also the more limited use cases compared to "normal desktops" have been taken into account when designing the new shell. Another focus point is deeper integration of online data sources sources and interaction models (The Web) into the user experience.<br />
A series of session has spawned new concepts of handling virtual desktops and visualizing Plasma's Zooming User Interface, and making both of these complement each other better. The Plasma and KWin teams' next in-person meeting is planned for August/September in Switzerland.

<div style="border: thin solid grey; padding: 1ex; width: 500px; text-align: center; margin-left: auto; margin-right: auto">
<a href="http://www.youtube.com/watch?v=iULB1zE7EJE">
<img src="http://dot.kde.org/sites/dot.kde.org/files/netbook-sal.png" width="500" height="258" /></a><br />
<a href="http://www.youtube.com/watch?v=iULB1zE7EJE">Wikipedia search integrated into the Plasma netbook shell</a>
</div>

<p />
Nuno Pinheiro, KDE's lead artist, provided insight in the design process of <strong>usable icons</strong>. Pinheiro <a href="http://pinheiro-kde.blogspot.com/2009/07/take-test.html">invites</a> everyone to take part in the regular <a href="http://www.oxygen-icons.org/survey/">icon test surveys</a> that will be conducted to improve the clarity and usability of these central visual elements of the workspace and applications.
</p>


<h2>Akademy concludes...</h2>
<p>
Akademy plays an important role in talking about where KDE and its software is going. There have been many discussions about the direction KDE can be taking with respect to the web experience, more details on this will be announced as they materialize in the coming months. This year's Akademy was the first one to be co-hosted with GUADEC, GNOME's flagship conference, and the cross-desktop sessions have proven to provide good ways to share and interact between the two major Free Desktops. During Akademy, KDE has also issued a <a href="http://kde.org/announcements/announce-4.3-rc2.php">second release candidate of KDE 4.3</a>, the new version that will bring improvements all over the desktop, applications and development platform. KDE 4.3.0 will be released on 28th of July.<p />
Special thanks go out to the conference host, the <a href="http://portal.grancanaria.com/portal/ficha_consejeria.px?codcontenido=1329">Cabildo de Gran Canaria</a> and sponsors such as <a href="http://www.qtsoftware.com">Qt</a>, <a href="http://www.maemo.org">Maemo</a>, <a href="http://www.google.com">Google</a> and <a href="http://www.novell.com">Novell</a>.<br />
The conference would not have been possible without the local team headed by Agustín Benito Bethencourt and of course the brave KDE people: Kenny Duffus and KDE e.V.'s Claudia Rauch. It is these and many other helping that have made this successful and important event possible. </p>