---
title: "Camp KDE Takes off in Jamaica"
date:    2009-01-19
authors:
  - "jpoortvliet"
slug:    camp-kde-takes-jamaica
comments:
  - subject: "weather"
    date: 2009-01-19
    body: "yeah, it is very thrilling to watch the photos, sitting here in Kiev and watching the snow silently shining out the window ;)"
    author: "Nick Shaforostoff"
  - subject: "What type of cookies ?"
    date: 2009-01-19
    body: "I wonder what ingredients were in the cookies ??\n\nI for one would be very happy to find out that my favorite desktop developers like to 'chill out' together,,,,\n\nWell you were in the best place !"
    author: "morgan"
  - subject: "Re: What type of cookies ?"
    date: 2009-01-19
    body: "Is Ade open sourced with his software but closed source with his recipe?  We'll find out.  \n\nThey may be similar to the chocolate chip cookies he made after breaking into the hotel kitchen at the KDE Release Event.  But worth the risk - they were delicious.\n\nI hope he hasn't made them yet; they went to get ingredients in town yesterday but I haven't heard the buzz of the results (or alternatively seen people shipped off to the hospital)."
    author: "Wade"
  - subject: "Re: What type of cookies ?"
    date: 2009-01-20
    body: "He broke into my room to make them this time. :D\n\nPeanut butter, oatmeal, and I think chocolate chip. \"C is for Cookie\" is now the official song. \n\nRandom camp notes:\nI'll note that it is disturbing how much you can do while in a bar on the beach if you have a laptop. Or an n810. :D\n"
    author: "Blauzahl"
  - subject: "Re: What type of cookies ?"
    date: 2009-01-20
    body: "There were peanut butter cookies and oatmeal chocolate-chip cookies. The peanut butter ones are based off a recipe from the Children's Cottage Cookie Cookbook, an early-80s fundraiser for a children's shelter in Calgary. My interpretation of the recipe is this: a lump of butter, a mound of sugar, some vanilla, an egg, half a jar of peanut butter, a heap of self-raising flour and a pinch of salt. I'm still working on a conversion table of that to metric."
    author: "Adriaan de Groot"
  - subject: "Re: What type of cookies ?"
    date: 2009-01-20
    body: "Ok, what Morgan was so slyly alluding and that no one seemed to pick up was what is in the cookies besidest sugar or vanilla.\nMaybe Jamaica's best know export: marijuana?\n\nMagic brownies/cookies is usually what people refer to in case like these.\nGoing to Jamaica and not having pot is like going to Germany and not having beer.\n\nI on the other hand would like to know why you chose Negril over the other much nicer beaches in Jamaica. Seems the only thing they have in Negril that they have nowehre else on the island is the 'everything goes' resorts like Hedonism.\n\nOn the other hand, Id rather not know....\n\nThe image of you guys smoking baseball sized joints is a lot more pleasant than the ones that would come up if I had to combine the thoughts \"KDE geeks\" and \"Hedonism\".\n\nLast thing we want is any of you to catch some social disease.\nLike Mono.\n \n"
    author: "Lyle Howard Seave"
  - subject: "disgusting"
    date: 2009-01-19
    body: "Troll Removed [moderator]"
    author: "Anon"
  - subject: "Re: disgusting"
    date: 2009-01-19
    body: "No desktop has a \"conscious\" - furniture is generally reckoned to be non-sentient.\n\n- Another Anon"
    author: "Anon"
  - subject: "birthday"
    date: 2009-01-19
    body: "I really feel bad about managing to forget to write about the beautiful songs we sang for the two silly guys who came here despite their birthday: Gokman and Orville. Wade DID blog about it: http://wadejolson.wordpress.com/2009/01/18/camp-kde-2009-day-2/"
    author: "jospoortvliet"
  - subject: "tale"
    date: 2009-01-20
    body: "hehe, it's like a fairy tale :) thanks for sharing"
    author: "mxttie"
  - subject: "Re: tale"
    date: 2009-01-21
    body: "As a Jamaican I am honor that you guys appeared to have enjoyed you stay. Nuff Respect!"
    author: "J"
  - subject: "Wicked, wicked"
    date: 2009-01-22
    body: "Jungle is massive"
    author: "Scriabin"
---
In a warm Jamaica around thirty KDE developers have gathered for the first Camp KDE. The following article is an impression of the first days of this event, a short summary of what is going on here. Read on for the full report!





<!--break-->
<p>
In a warm Jamaica some thirty KDE developers have gathered for the first Camp KDE. The healthy growth of the KDE community created the need for a combined North/South American meeting. The release event showed it is possible. It has been a year since the KDE 4.0 release event by San Fransisco, and we see many of the faces we saw back then at Google headquarters. 
</p>

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 300px">
<a href="http://static.kdenews.org/jr/campkde/where-we-work.jpg"><img src="http://static.kdenews.org/jr/campkde/wee-where-we-work.jpg" width="300" height="225" alt="photo" /></a><br />
Little taste of where we are
</div>

<p>
Last year has been an exciting journey for the KDE community. The 4.0 release was made to some joy and some criticism. But the release increased activity and resulted in a fast growing number of contributors and code commits. Thus the 4.1 released around Akademy brought many improvements. Now it is the upcoming release which aims for the general public. And again the American community event is in the sweet spot, we will be releasing the KDE 4.2 software suite a couple of days after we leave the golden island of Jamaica. Of course this meeting is not only about the 4.2 release, far from it. It is about continuing what we started last year. We will continue to grow and create great software. And have fun while doing it.
</p>

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 300px">
<a href="http://static.kdenews.org/jr/campkde/hotel-area.jpg"><img src="http://static.kdenews.org/jr/campkde/wee-hotel-area.jpg" width="300" height="225" alt="photo" /></a><br />
A look at the hotel
</div>

<p>
The location for this event is amazing. Most of us have traveled a long way, and arriving in Jamaica after such a journey is very rewarding. It is a beautiful country and even late at night, it is warm and we can sit outside. Many arrived rather late, and met the others at the beach. Many went for a swim and there was a KDE Plasma meeting held in the Caribbean sea - the water is lovely.
</p>

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 300px">
<a href="http://static.kdenews.org/jr/campkde/wet-geeks.jpg"><img src="http://static.kdenews.org/jr/campkde/wee-wet-geeks.jpg" width="300" height="225" alt="photo" /></a><br />
wet developers
</div>

<p>
As usual at KDE events there is a large variety of developers. There are PIM people, Edu developers, Plasma developers. We have them in every colour and taste. There are some Pardus developers looking for bonding with the KDE community. Of course, besides enjoying each others company, food and the weather, work is in progress already. There is wireless and power everywhere, even at the beach. As is often the case the heavy network usage proved too much for the network, but after persistent efforts of the organisers we got connected again. After being introduced to several kinds of fruit we never knew existed by a great Jamaican breakfast  we started to move to the conference room.
</p>
<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 300px">
<a href="http://static.kdenews.org/jr/campkde/working.jpg"><img src="http://static.kdenews.org/jr/campkde/wee-working.jpg" width="300" height="225" alt="photo" /></a><br />
Working developers
</div>

<p>
Wade introduced us to the conference, outlining the reasons for this meeting. Then Winston Wellington, the owner of <a href="http://travellersresorts.com">Travellers Beach Resort</a> and founder of the <a href="http://neetja.com">Negril Education Environmental Trust</a> told us how results of our work are affecting this place. This organisation is dedicated to educating the Jamaican people by bringing cheap computers and books to them. This year alone they opened several libraries with thousands of books and over fourty computers. As Wade concludes, it is often easy to forget what happens with the software we develop. We worry about text drawing, widgets and memory usage - here the results are applied and used to help improve the life of people. After Wade's introduction several developers gave a talk. Those will be covered in a separate article available in the coming days.
</p>

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 300px">
<a href="http://static.kdenews.org/jr/campkde/conference-area.jpg"><img src="http://static.kdenews.org/jr/campkde/wee-conference-area.jpg" width="300" height="225" alt="photo" /></a><br />
Conference area
</div>

<p>
When the last talk was over a big group of contributors went to town under supervision of our "security team" (Roger Pixley and Dmitri Dawkins). After a visit to the supermarket for drinks we had a portion of the famous Jamaican <i>Jerk Chicken</i> alongside the road. Eating chicken, sitting on benches between the Caribbean sea on one side and the road with a reggae party going on on the other side. That is how life is around here.
</p>
<p>
Returning at the hotel, one group went to the beach and sat on the benches just before the hotel (2 metres from the sea). Accompanied by various kinds of Jamaican rum, juice, good company and a warm sea, the evening was great.
</p>
<p>
A second group went out for pizza but was less lucky. A local pizza place had a hard time serving the whole group. So it took a while to get food, but in the end they joined the others on the beach with their belly full.
</p>
<p>
Sunday again offered talks about various topics. During and between talks developers went downstairs to the beach for a swim and we also had a great lunch.
</p>

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 300px">
<a href="http://static.kdenews.org/jr/campkde/preparation-for-evening.jpg"><img src="http://static.kdenews.org/jr/campkde/wee-preparation-for-evening.jpg" width="300" height="225" alt="photo" /></a><br />
Supplies for a perfect Sunday evening in Jamaica (yes, Ade will bake cookies)
</div>

<p>While this article is being finished your author is looking out over the beach, watching the community enjoy the waves and have a good time. This must be one of the most enjoyable KDE meetings ever... You can count on more articles, blogs, inspiration and of course code resulting from this event!
</p>




