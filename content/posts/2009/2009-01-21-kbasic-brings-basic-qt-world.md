---
title: "KBasic Brings BASIC to Qt World"
date:    2009-01-21
authors:
  - "bnoetscher"
slug:    kbasic-brings-basic-qt-world
comments:
  - subject: "Not a KDE app..."
    date: 2009-01-21
    body: "... but still a nice and welcomed addition to the free software world.\n\nI started programming with BASIC on a Laser 210 computer when I was 10, and maybe I can \"convert\" one of my students to Linux; he is 14 and currently writing VB.NET apps on Windows.\n\nWho else had his first programming experiences with BASIC?\n"
    author: "christoph"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-21
    body: "The first language I learned was QuickBASIC, in grade 6, and while I'm not really a fan of BASIC anymore, I still have fond memories of it :P"
    author: "Jeffery"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-21
    body: "Same for me, of course along with the games Nibbles and Gorillas! :-) That was really funny :-)"
    author: "dh"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-21
    body: "Basic V2 (Commodore 64), AmigaBASIC, GFA-Basic (atari ST and Amiga), STOS (Atari ST), Amos (Amiga). Does that count..."
    author: "Jonas"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-21
    body: "Good stuff! I had all kinds of fun with Amos back in the day. :-)"
    author: "Nikolaj Hald Nielsen"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-21
    body: "Hmm, I don't know if it counts as an \"experience\", but my first contact with programming was when I got my hands on a book about BASIC when I was 5. Didn't have a computer to try it out, but nevertheless I was thrilled :). The first \"programming\" \"language\" I really wrote in was DOS batch files (later including NDOS), followed by QBasic and Pascal."
    author: "slacker"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-21
    body: "My first programming experience was with TI-BASIC on the TI-85..."
    author: "momo"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-21
    body: "Same here, but on the TI-84+\n\nI wrote pong for it and it was CRAZY slow.  I'd love to be able to write apps for it in C/++ and compile them and put them on the calc instead of having to choose between BASIC and ASM.."
    author: "ethana2"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-21
    body: "Sinclair Basic on a ZX-Spectrum 16. Fun times. It was a loaner, and I persuaded my parents to buy the infinitely cooler 48k speccy because I demonstrated filling up the available memory with basic code."
    author: "Boudewijn Rempt"
  - subject: "Who else had his first programming experiences wit"
    date: 2009-01-21
    body: "I had. My first computer was ZX Spectrum 48)"
    author: "Dark Amateur"
  - subject: "Re: Who else had his first programming experiences wit"
    date: 2009-01-22
    body: "Mine too. Sir Clive Sinclair is a hero. =)\n\nI fondly remember games like \"Jet Set Willy\", \"Underworld\", \"Sabre Wulf\" and \"Elite\". Apart from programming dead simple things like text adventures and the occational drawing app. The draw app was controlled by a joystick and could only draw lines. I think that was assembly code though as it was lightning fast. So fast that a single tap on the joystick would send it out to the screen-edge and from there it would only draw a \"box\" following the screen-edge. Not really useful.\nMost of the code there was probably borrowed from magazines though."
    author: "Oscar"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-21
    body: "When I was very small I used to draw pictures using the cursor keys and the graphics symbols you got by SHIFTing keys on a Commodore PET.  I started programming when I wanted to save my pictures and copied the REM statements I saw in one of my dad's programs, thinking it meant REMember.  Typing \"REM (-:\" into the basic interpreter was fine but it didn't save my graphical masterpieces.  Fortunately my dad came along and explained line numbers, ?\"(-:\" and DSAVE \"WILLART\" and I was off like a shot."
    author: "Will Stephenson"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-22
    body: "Nope. I still can't understand BASIC to this day. I'm glad. It always takes me a while to understand what's going on when I have to clean up someone ugly BASIC code."
    author: "winter"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-22
    body: "Basic was the first language I learned with my Epson QX-10 many centuries ago. I was like 12 or so... My mother gave me a book called \"1000 games for basic\" or something like that.\n Who else remembers ELIZA?"
    author: "eamner"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-22
    body: "In 1978 (or was it '79) I had an Atari 2600 VCS and bought the BASIC programming cartridge and keyboard controllers.  I learned the whole language and exhausted all the coding possibilities of the limited 2600 environment in about a week.   That convinced me I'd like to get into computers\n \nLater in high school we had BASIC classes with Apple IIs.   It was another year until I could afford my own computer -- An Atari 800 with an enormous 48K, and BASIC.  I couldn't afford a floppy drive, so only had the 410 tape player.  Later came the enhanced and faster BASIC XL.\n\nBy the time the Amiga appeared I was doing C and assembly.  I've been coding in C for unix (and unix-like) systems for over 20 years now.  I still miss writing games in Basic and 6502 assembly on the Atari 800."
    author: "Ken Jennings"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-23
    body: "I started with Basic MSX on Yamaha."
    author: "Ilya"
  - subject: "Re: Not a KDE app..."
    date: 2009-01-24
    body: "My first program language is GVBASIC which is similar to QBASIC but not worked in PC when I was 14. Since I've loved linux I never use any software belong to Microsoft."
    author: "WarGrey"
  - subject: "BASIC"
    date: 2009-01-24
    body: "I started off with GW-BASIC on my Dad's Olivetti when I was about 8 years old, and I got hooked on programming :-)"
    author: "Claire"
  - subject: "Yeah!"
    date: 2009-01-21
    body: "Yeah! The greatest abomination of a programming language in existence now makes its way into KDE!\n\nHow is the progress on KCobol going?"
    author: "slacker"
  - subject: "Re: Yeah!"
    date: 2009-01-21
    body: ">Yeah! The greatest abomination of a programming language in existence now makes its way into KDE!\n\nNot even close.  KDE has already been introduced to the worst language ever made: C++.\n\n:P"
    author: "Ronald"
  - subject: "Re: Yeah!"
    date: 2009-01-21
    body: "C++ is the worst compiled language ever... except for all the others!"
    author: "David Johnson"
  - subject: "Re: Yeah!"
    date: 2009-01-21
    body: "Try using Haskell or Ocaml.  You'll curse all the years you have been using C++...\n"
    author: "Functional guy"
  - subject: "Re: Yeah!"
    date: 2009-01-21
    body: "At least it puts bread on the table."
    author: "David Johnson"
  - subject: "Re: Yeah!"
    date: 2009-01-22
    body: "Actually I have used both. And I curse Haskell beyond anything else... I don't know if I would rather use BASIC, but it is possible :). Cool paradigm spoiled by \"academic purity\" getting in your way all the time.\n\nOCaml on the other hand is quite nice, and for some applications the functional paradigm makes a hell of a difference. Still, there is a substantial performance difference when compared to C/C++. And I find wasting processing power impolite towards the end users. After all, they have paid for it. Also, there are some subtle scalability problems that can bite you with larger projects.\n\nAll in all, I will stick with C when I have the choice :). I'm not a great fan of C++, either."
    author: "slacker"
  - subject: "Re: Yeah!"
    date: 2009-01-23
    body: "Yes regarding haskell \"academic purity\" does hurt sometimes, but I honestly hope that most of these pains are just the symptom that theory isn't quite there yet, and some improvement will be possible while still sticking to the principles on which the language is founded. These are what makes it so beautiful when it works. \n\nThere's lots of ideas in the hair. Many of these would make haskell easier to use right now. However that's not the goal of the language, you don't want a good enough solution for the present, you want a very good solution for the future... so it takes time. \n\nAll that said, I honestly think that the language is already at the point where for many tasks, it can be an excellent choice. "
    author: "Jad"
  - subject: "Re: Yeah!"
    date: 2009-01-21
    body: "You probably mean the \"Kobol\" sf project? Last time I checked they had a new punchcard plasmoid and several skins for that."
    author: "Deral Pern"
  - subject: "Re: Yeah!"
    date: 2009-01-21
    body: "Yes, all this interactive BASIC stuff is a bit new for me. The jCquard JavaScript library is where it's at for that cool Plasmoid:\n\nhttp://www.outstandingelephant.com/jcquard/\n\nOf course when I started programming we couldn't afford punchcards and had to cut up old cornflake packets instead. Didn't have any computers either, had to do it all in our heads. You were lucky.."
    author: "Richard Dale"
  - subject: "Re: Yeah!"
    date: 2009-01-23
    body: "+1E+99\n\nYes, BASIC is probably the worst computer language.  Notice that I said computer language since it is one of those languages that is not really compileable and is thus not a *programing* language.  Not to mention that it is obsolete.\n\nIt is unfortunate that obsolete computer (and programing) languages are perpetuated just because people know them.  But, that is the way with C as well.  \n\nWhat is BASIC (Beginners All-purpose Symbolic Instruction Code) really?  It is a pidgin based on FORTRAN IV that was modified so that it it was easier to interpert specifically designed for timesharing systems.  Don't know what a timesharing system is?  As I already said, very obsolete. :-D\n\nCould someone please design a modern procedural language to replace it?"
    author: "JRT"
  - subject: "MDI Interface"
    date: 2009-01-21
    body: "According to the screenshots, this uses an MDI interface. Is this now accepted in KDE applications?"
    author: "Brian Pritchett"
  - subject: "Re: MDI Interface"
    date: 2009-01-21
    body: "I hope not XD"
    author: "Luis"
  - subject: "Re: MDI Interface"
    date: 2009-01-21
    body: "Is this important? Either use it or not...\nIt's not for end users but for developers and developers probably can live with that.\nI wouldn't mind actually, although it feels a bit old - I'd prefer the Tab interfaces (IntelliJ interface) as well...\nCan't think of any other application that has an MDI interface..."
    author: "Peter"
  - subject: "Re: MDI Interface"
    date: 2009-01-21
    body: "> Is this important?\n\nOf course the UI paradigm an app follows is important.\n\n> It's not for end users but for developers and developers probably can live with that.\n\nSo you say developers can live with crappy interfaces? I object. If I use an IDE and not emacs/vi and the like, and want a streamlined and effective interface, one that classic MDI doesn't provide (not to mention that it doesn't work with multi-display setups).\n\n(Disclaimer: that's all general remarks without judgement about kbasic)"
    author: "Hanno"
  - subject: "why are people complaining ?"
    date: 2009-01-21
    body: " I saw some of you are already begging to complain about this and that and ... What I really don't understand is why. Don't like basic ? Fine , don't use this , there are still a number of way to write apps in qt besides this. Don't like how this program looks ? Again , use another IDE, there are plenty to choose from.\n It seems to me that there are people here that like to whine about stuff. It's free software , it's open source. Don't like it, don't use it or get the code and make it better. It's not like somebody is asking you for your money for a crappy piece of software , unlike what some other companies are doing.\n Oh and it's nice having the possibility of writing code in yet another programming language. I remember having a PC with a Z80 clone CPU that kind of had  the OS running basic. I did some basic programming in it. Also tried some simple stuff in VB. And let's not forget it's basic that made MS possible, it's what the company started with :). However they seemed to push C# now rather than VB.NET ."
    author: "Marius"
  - subject: "Gambas <-> KBasic ?"
    date: 2009-01-21
    body: "How does it compare to Gambas?\n\nhttp://gambas.sourceforge.net/\n\nI once did a small project with gambas and I must say it's a very nice, stable and feature-rich environment."
    author: "Thomas"
  - subject: "Re: Gambas <-> KBasic ?"
    date: 2009-01-21
    body: "Gambas isn't compatible with VB6, nor does it have the ambition to become compatible.\n\nThe professional edition is probably bought the most by small businesses that want to port their old VB6 apps to a modern platform without switching to vb.net."
    author: "Paul"
  - subject: "Re: Gambas <-> KBasic ?"
    date: 2009-01-23
    body: "KBaasic currently supports Qt4 while Gambas does not. But Gambas has packaging abilities which KBasic does not have."
    author: "Ilya"
  - subject: "Compiling only in professional (paid) edition???"
    date: 2009-01-21
    body: "Note that the compile to executables feature is available ONLY in the professional (PAID) feature..."
    author: "Trapanator"
  - subject: "Re: Compiling only in professional (paid) edition???"
    date: 2009-01-23
    body: "This is under Windows. Under Linux you can easily compile executables with free version."
    author: "Ilya"
  - subject: "Kbasic"
    date: 2009-01-21
    body: "Well, actually there are simple languages as Javascript or Python that serve the same objective as Visual Basic 6: they reduce complexity hells for desktop uses.\n\nIt is merely psychological as a barrier.\n\nI am convinced that C++ or C are not designed for desktop applications. In theory you can use all the powerful features but in reality you run into a bug and memory management hell. Shuttleworht call to use Javascript for the Desktop is up to the point."
    author: "andre"
  - subject: "Re: Kbasic"
    date: 2009-01-21
    body: "I still don't understand why Python hasn't displaced VB on Windows. Perhaps it's the NEBM syndrome (not endorsed by Microsoft).\n\nAs for C++, it's very appropriate for the desktop. But unlike many scripting language, is unforgiving of bad programming practices. In terms of memory management, just remember that everything you allocate you need to deallocate. Destructors do much of this work for you, object hierarchies like Qt's QObject do a lot more. You can even use a full blown garbage collector. But ultimately it's your responsibility as a developer to look after your own code."
    author: "David Johnson"
  - subject: "Re: Kbasic"
    date: 2009-01-21
    body: "Yes, and it happens all the time that Developers make mistakes. A programming language should not provide for that. C++ is not unforgiving of bad programming style, it makes bad programming style possible.\n\nIt is like real construction workers, they don't like to wear a helmet. Programming languages were not created for memory management."
    author: "andre"
  - subject: "Re: Kbasic"
    date: 2009-01-21
    body: "Hate to break the news to you, but bad programming most certainly exists outside of C++."
    author: "David Johnson"
  - subject: "Re: Kbasic"
    date: 2009-01-23
    body: "BASIC is probably preferred over Python because it is a procedural language."
    author: "JRT"
  - subject: "Re: Kbasic"
    date: 2009-01-22
    body: "When you learn how a computer works and when you start to write assembly or when some one stresses to you the importance of memory management first thing, you tend not to forget it. So I don't really see what the deal is with C++ and all the whinners. It tends to be a very fast compiled language. Fortunately, some of us aren't wasteful enough to write desktop suites in Python or BASIC or Javascript. Oh, and then there are garbage collectors. How nice.\n\nBTW, in game programming, you'd be hard pressed to used something else. There has to be a really strong reason other than performance not to use C or C++. I've seen the Java and Python engines. Most of them just have a scripting interface, which is nice for fast prototyping."
    author: "winter"
  - subject: "Re: Kbasic"
    date: 2009-01-23
    body: "I'm not sure if you have noticed, but many games (FPS too) are \"written\" in Python, Ruby or Lua. Basically a C++ engine which is controlled via a \"scripting\" interface on top. This is same architecture that you use when writing desktop utilities or applications in Python, BASIC or Javascript. The engine in this case is typically a libraries like Qt and/or a bunch of other C/C++ libraries. It is a very smart and effective way of working. I don't see what is so \"wasteful\" about it.\n\nAs for garbage collection, it is probably the single most important language feature out there for improving programmer productivity. Just MHO.\n\n--\nSimon\n"
    author: "Simon Edwards"
  - subject: "64 bits?"
    date: 2009-01-21
    body: "Only works in 32 bits?"
    author: "Vagner"
  - subject: "THE WORST programing language of ALL time!"
    date: 2009-01-21
    body: "Worthless programing language based on THE WORST programing language of ALL time!\nAHahAHHAha!\nWTF? Why would anyone use it?"
    author: "kojot"
  - subject: "Good god, I could become a programmer again!"
    date: 2009-01-22
    body: "Good god, I could become a programmer again! :-(\n\nI wrote COBOL for Honeywell mainframes, and I also became reasonably proficient  with Bill Gates' BASIC on my Radio Shack TRS-80 microcomputer. I was likewise able to use QBASIC on my first IBM PC clone (back in MSDOS days). BASIC somehow disappeared about the time that Windows 95 appeared, and I have missed it ever since."
    author: "Kenyon Karl"
  - subject: "Re: Good god, I could become a programmer again!"
    date: 2009-01-24
    body: "You might want to think about that. We wrote Kommander for point and click novice programmers. Having used many languages I once bought a cross platform Basic package that ran on OS/2. I quickly realized I was spending all my time with the reference book open looking up functions. Then I tried VX-REXX and realized I was productive with a natural language computer language. Remember Basic was never standardized and codified. If you need Basic it is because you have many lines of Basic already, and then you must consider compatibility.\n\nOver the years I used Perl and PHP on the web and then got a job on a project where I had to work with ASP, which is essentially MS Basic. I thought at first the programmer had been whacked, but the more I tried to write good code and fix things the more I realized that ASP inherently made you write bad code, had lousy reporting and was just syntactically annoying. I refused to do any more of it. Having had to learn C++ and work with Qt to work on Kommander I was once again constantly working with my nose in the docs. However the elegance and power of C++ along with the remarkable framework of Qt made it a joy.\n\nWhen I was young I thought bagpipes were cool. You don't hear them in contemporary or classical music. A little goes a long way and a lot makes me crazy. If you used to play bagpipes I'd recommend you got back into music by learning piano or guitar. Likewise I think I would pick nearly any language except scheme over Basic if I was going to get back into programming. This project is expressly useful and good because it empowers legacy code bases, but when it comes to programming legacy languages != fun."
    author: "Eric Laffoon"
  - subject: "Good for them."
    date: 2009-01-22
    body: "Any new piece of free open source software is welcomed.\n\nI learned BASIC when I was about 9, but I quickly moved onto Assembly since Basic was too slow then on my Commodore 128. A few years later I started programming in Pascal, then a whole bunch of other languages (including obviously C++). Honestly I have no desire to ever program in Basic again (someone used the word abomination?). It is highly unlikely that KBasic would ever be downloaded to my computer.\n\nBut KBasic clearly gives a great advantage to whoever has some old code in VB, wants to reuse it, and for some reason gambas doesn't do it. That might bring more software into Linux and that is clearly positive for everyone."
    author: "Luisito"
  - subject: "Not GPL-licensed!"
    date: 2009-01-22
    body: "> It follows the old Qt licencing of being GPL licenced for Free Software and\n> commercially sold for proprietary software.\n\nThen where's the source code?\n\nAll I see on their download page is a .bin installer and a statement that it is \"The Linux\u00ae version is free of charge for open source (GPL 3) Software.\" That's only free as in beer. It's the Kylix licensing model (yuck!), not the Trolltech one."
    author: "Kevin Kofler"
  - subject: "Re: Not GPL-licensed!"
    date: 2009-01-22
    body: "Actually, I have to correct myself, the source code is there:\nhttp://www.kbasic.com/doku.php?id=source_codes\n\nBut why is this not linked from the download page? Why does the download page only talk about free as in beer?"
    author: "Kevin Kofler"
  - subject: "Re: Not GPL-licensed!"
    date: 2009-01-22
    body: "PS: It's extremely hard to find the source code from the kbasic.com front page, it's not linked from Download, Community, About nor Description (that's the order in which I checked them). You have to actually go to Manual, then scroll down to \"Open Source\" (which comes below a long list of links and there's no in-page link) and there you find the link. Please make the source code more visible if you want to be recognized as a Free Software project! I'd suggest mentioning the GPL license where it talks about \"free of charge\" in the Download page and adding a link to the source code right there."
    author: "Kevin Kofler"
  - subject: "Re: Not GPL-licensed!"
    date: 2009-01-22
    body: "The GPL doesn't require the source code to be available for everyone, but only for those who purchased the software."
    author: "whatever noticed"
  - subject: "Re: Not GPL-licensed!"
    date: 2009-01-22
    body: "No, only if you include the source when shipping the binary.\n\nOtherwise you have to \"include a written offer to make it available to any third party\"\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Not GPL-licensed!"
    date: 2009-01-22
    body: "Since when is the GPL \"Free as in beer\"? The FSF are the ones that made up that whole \"free as in speech not free as in beer\" schtick."
    author: "Roberto Alsina"
  - subject: "Re: Not GPL-licensed!"
    date: 2009-01-22
    body: "The thing is, nowhere on the download page does it actually say that it's GPL. It only offers a binary and tells you in the description it's free as in beer (\"free of charge\") if you develop GPL software. It is actually GPLed (see my correction), so it is actually Free as in speech, but unfortunately the download page doesn't make that obvious at all, you have to know where it's buried."
    author: "Kevin Kofler"
  - subject: "Re: Not GPL-licensed!"
    date: 2009-01-22
    body: "The source appears to be here:\n\nhttp://www.kbasic.com/doku.php?id=source_codes\n\nThere are directions, and a link to http://www.kbasic.com/kbasic_linux_sourcecode.tar.gz (V1.87)\n\n\nLinux\n\nThe following package contains three projects for KDevelop with complete source codes:\n\n    *kbc - the compiler (only C++ program without Qt usage)\n    *kbide - the IDE (with Qt usage)\n    *kbrun - the runtime and intepreter (with Qt usage)\n\nAvailable under the terms of the GNU Public License as published by the Free Software Foundation (Version 3): \n"
    author: "Ken Jennings"
  - subject: "Re: Not GPL-licensed!"
    date: 2009-01-23
    body: "I know, I noticed after writing my comment. :-( See my correction.\n\nStill, it should be written on the download page."
    author: "Kevin Kofler"
---
<a href="http://kbasic.com/">KBasic</a> is a new programming language similar to Visual Basic. It combines the best features of those tools and comes with built-in backward support for those tools as it is 100% syntax compatible to VB and QBasic.  It is written with Qt making it entirely cross platform.  The <a href="http://kbasic.com/download.html">Full Version Professional Edition</a> is available to download for KDE now.  It follows the old Qt licencing of being GPL licenced for Free Software and commercially sold for proprietary software.




<!--break-->
<p>KBasic is an open source project backed by years of continual development.  Versions are available for KDE, Windows and Mac. It allows developers with an installed base of VB applications to start developing for a mixed Windows, Mac OS X and Linux environment.  It is made up of a compiler, an interpreter and an integrated development environment.  It is about 15 MB source codes in C++ and about 1000 source code files.</p>

<p>People around the world join KBasic - inspired by the idea to make software available for everybody: a programming language that is easy to use, and a development platform that is stable, reliable and available at a low price.</p>

<p>We communicate by different means, most of them on the Internet. The <a href="http://www.kbasic.com/forum/index.php">KBasic community</a> rests on dedicated volunteers to further improve our programming language and development platform in a number of different ways. Whatever your skills, there are lots of places to start contributing.</p>

<p>The project is under active development and has a vibrant community. Take a look at <a href="http://www.kbasic.com/doku.php?id=manuals#contribute">ways to contribute</a>.</p>

<p>KBasic supports calling functions of C/C++ library files, enabling you to mix C/C++ and BASIC code.  KBasic uses SQLite as an embedded database engine, which creates database files locally on your machine without the need of a database server.  Libraries are availalble for data/time, i18n, RTF, web browser.</p>

<p>KBasic Software is a small software company with headquarters in Frankfurt am Main in Germany. KBasic Professional is its flagship product, the multi-platform BASIC programming language and environment.</p>


