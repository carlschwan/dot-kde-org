---
title: "KDE4 Demonstrates Choice Is Not A Usability Problem"
date:    2009-10-22
authors:
  - "jospoortvliet"
slug:    kde4-demonstrates-choice-not-usability-problem
comments:
  - subject: "Frying the big fish"
    date: 2009-10-22
    body: "This is a great article, thanks Daniel for the submission.\r\n\r\nI'd just like to preemptively point out that the initial link is to a KDE vs. Gnome style article, which while not that controversial, is a topic that the KDE team wants to be clear about. KDE an Gnome are both exciting open source projects with our own design goals in mind. We live in the shadow of much larger competitors, who have over 95% of the desktop computing market share, and regardless of whether converts choose KDE or Gnome, we're happy, as it grows the open source desktop community. (Or LXDE, Xfce, E17...)\r\n\r\nSo while it's nice to be receiving third party feedback about positive design choices that KDE has made, keep in mind that we have some really big fish to fry, and there's a hell of a lot of work to be done, in both camps, before we can reel 'em in.\r\n\r\nCheers"
    author: "troy"
  - subject: "KDE4 Demonstrates Choice Is Not A Usability Problem"
    date: 2009-10-22
    body: "This is going to get modded to oblivion:\r\n\r\nKDE4 Demonstrates Choice Is Not A Usability Problem Except For the Cashew on a Stock KDE Desktop Where It Is Indeed a Problem. :)\r\n\r\nI love KDE, but..."
    author: "Shade"
  - subject: "Usability"
    date: 2009-10-22
    body: "So how are the Cashew and stock desktop a usability problem? \r\n\r\nI have newer seen any explentation showing either to be a usability problem, so I'm rather curoius. Have seen lots of incoherent rants, but never a demostration of how they can be usability problems. Perhaps someone have outlined it an bugreport to the Plasma developers, so maybe they can enlighten the rest of us.\r\n\r\n "
    author: "Morty"
  - subject: "A feature you don't use doesn't necessarily mean bad usability"
    date: 2009-10-23
    body: "I have a desk (one of those old wooden ones with an actual physical desktop) and a drawer underneath on one side.\r\n\r\nIt used to be my Dad's desk, the drawer was there, over on one side, and he quite liked it to store pens and bits of paper - he's not so much in to computers and still writes on paper quite a lot and that little drawer made some tools he wanted to use close at hand.\r\n\r\nThen it became my desk. I use a PC more than pen and paper and the fact is I don't use that drawer, over under the side of my desktop, but it doesn't get in my way and I've never really felt the need to comment on it (until now).\r\n\r\nOverall it's a pretty usable desk. It has one item over on one side that I don't use, but that doesn't stop me using the desk. And I appreciate that the part I don't use might be useful to someone else."
    author: "swjarvis"
  - subject: "Summary"
    date: 2009-10-23
    body: "The entire article is nice retorics, but lets sum it up: \r\n\r\nKonq was great, too great so it was confusing for noobs. Enters Dolphin. KDE4 is just like KDE3 only with folder views (20+ year old concept, innovative?), and usefull panels are replaced with simplistic plasma fluff. And there's now also context sensitive drag and drop. (another \"novelty\" - back in mid 90ies I could even add modifiers, drop image while pressing j - voila, jpeg).\r\n\r\nThere, done.\r\n\r\n\"Design your own desktop\", well so far it cannot even mimic my KDE3 desktop in any practical way, let alone let me \"design\" a desktop that truly works how I want. \r\n\r\nOh, and the nutty thing is in the way, it's located where I useally press for switching to next desktop when using mouse. And those plasma \"oparation bars\" are really pissing me off, it doesnt help that they're huge and pop up on random side of plasma widgets, just because the mouse pointer accidently pass over them. And nepomuk - for people who dont know how to organize their files? How to kill off enterprise desktops with networked filesystems? Excellent. I could go on."
    author: "kolla"
  - subject: "Thank you for the laugh!"
    date: 2009-10-23
    body: "Was that article meant to be published on April 1st? Because, frankly, associating KDE and usability must be some king of joke, right ?\r\n\r\nLet's just consider the examples one by one.\r\nHow can you consider that bothering the user with a pop-up menu <i>every single time</i> he drags a file is a positive thing ? You're just increasing the time needed to accomplish a simple action. Other file managers don't ask. They try to guess what the user wants to do and 9 times out of ten they're right. When the guess is not right, on Windows you can drag the file with a right-click to have the same pop-up menu as in Dolphin. And on a Mac, you can use Cmd+ drag to always move the file instead of letting the computer guess. Now, that's usability: the computer does the boring work and the user can just go on with what he was doing.\r\n\r\n\r\nThe same goes for the toolbar configuration dialog. The way it is presented to the user, it's painfully obvious that the developer sees the toolbar as a list of buttons and separators. The problem is that, as a user, I don't. I just see buttons side by side. With this dialog, I have to choose the buttons I want, apply, see if it's ok, change again if it's not, apply again... Being able to drag and drop buttons where I want them in the toolbar with a live preview of what the toolbar will look like when I'm finished is far simpler (and most of the time faster). Of course, it would be more difficult for the developer.\r\n\r\nThis article really illustrates how disconnected from the real-world kde developers are. I have no doubts that things like Nepomuk and Folder view can be useful. But if you cannot get basic things right nobody will put up with your software long enough to care about advanced features. The problem as I see it is that kde developers see everything as binary choices with simple answers instead of considering complex answers to real-worlds problems."
    author: "rcachou"
  - subject: "> How can you consider that"
    date: 2009-10-23
    body: "> How can you consider that bothering the user with a pop-up menu every single time he drags a file is a positive thing ?\r\n\r\nYes, this is a very positive thing ! For example, windows requires me to remember whether source and a destination are on the same filesystem. This is a purely technical thing and should be hidden from the user. \r\n\r\nMoving a file is quite serious action which could lead to, for example, deleting a file believing it was only a copy. This action should never rely on me remembering some technical detail about filesystems."
    author: "vld"
  - subject: "sebas"
    date: 2009-10-23
    body: "I've actually come to find this mechanism of \"present a popupmenu on drop\" very convenient: I know exactly what's going to happen, it's easy to cancel it, and the popup comes up very close to the mouse, so little I need to move.\r\n\r\nI don't quite agree with your assessment of the simple choices. Especially in the case of hover interfaces, but also if you look at Plasma's configuration, you'll see that it's not actually the case.\r\n\r\nFunny thing is, you complain about binary choices and in the next second paint the world as black and white as you possibly could, that doesn't quite match, IMO."
    author: "sebas"
  - subject: "Quite the oposite"
    date: 2009-10-23
    body: "\"This article really illustrates how disconnected from the real-world kde developers are.\"\r\n\r\nIt does actually show quite the oposite, it shows how the real-world appreciate the work and choices of the KDE developers. \r\n\r\nSince you obviously missed it, this article was written by a real-world user not a KDE developer."
    author: "Morty"
  - subject: "I did not miss it. But since"
    date: 2009-10-23
    body: "I did not miss it. But since the article is reprinted here, it is endorsed by the KDE developers. And the fact that they're so proud of their developer-oriented UI which systematically tries to complicate users' life with useless choices shows that they are disconnected from the real-world.\r\n\r\nI don't think kde sucks. It's great for control-freak geeks who like to customize every pixel of their screen. However, those people are a tiny minority and it seems to me that when they gathered and write a desktop environment that suits their needs, they forgot about the rest of the world."
    author: "rcachou"
  - subject: "It may be \"endorsed\" because"
    date: 2009-10-23
    body: "It may be \"endorsed\" because the article expresses points that they wanted to convey. Since it was written by a non-developer, an end user who never even got into programming and who in recent years became quite lazy when it comes to tinkering with the inner workings of a system, I think it qualifies as an affirmation of the KDE4 approach as not merely dev-centric.\r\n\r\nIt's worth noting I've been using GNOME primarily for most of the last few years and up to recently. Ever since KDE4 came out however I've been more and more interested in it and waited for it to become more stable. I think KDE4.3 in Karmic might just do it.\r\n\r\nAnd why have I used GNOME? Because it is simple, easy and feels very polished. I wouldn't switch to KDE4 if I didn't perceive significant improvements in those terms on it as well.\r\n\r\nAs for control freak customizers being a tiny minority, even if that's true the whole point I tried to convey in the article is that choice is added without complicating it for those who don't want to opt for certain options. \r\n\r\nAnd as some have already said, KDE4 defaults are becoming more and more polished and reasonable for most people. In short, more choice is not a problem. It's not meant to complicate anything for anyone. That's the whole point. Even if KDE4 isn't quite there yet, that seems to be the destination.\r\n\r\nIn the end you might as well get GNOME level simplicity without losing the additional choice and power if you want it, but without hiding it in such a way to be accessible only to geeks willing to plow through cryptic gconf registry."
    author: "memenode"
  - subject: "Folder View"
    date: 2009-10-24
    body: "I really like the folder view.  I have two of them on my DeskTop.  It would be nice if the configuration of them didn't change every time I started KDE, but that is a minor issue.\r\n\r\nHowever, the author stated: 'You're moving a folder and get the option to create a \"folder view\"'.  I have to admit that I have no idea what he is talking about."
    author: "JRT256"
  - subject: "If you drag a directory from,"
    date: 2009-10-24
    body: "If you drag a directory from, say, Dolphin, to the desktop, you get asked if you want an icon (shortcut) or a folder view."
    author: "einar"
  - subject: "\"the whole point I tried to"
    date: 2009-10-24
    body: "\"the whole point I tried to convey in the article is that choice is added without complicating it for those who don't want to opt for certain options\"\r\n\r\nI see the point but your examples show exactly the opposite. Popping-up a menu to ask what I want to do every time I drag a file feels very invasive to me. Especially as there is no way to \"teach\" the file manager what I want to do so that it does not ask the next time.\r\n\r\nOn the other hand, in Windows, the menu pops-up only when you drag a file using the right mouse button. This way, power users have the options they want. And users who prefer simple things are not bothered by options they do not need."
    author: "rcachou"
  - subject: "Bueller? Bueller?"
    date: 2009-10-25
    body: "Bueller? Bueller?\r\n\r\nedit: maybe this was a reference that missed its mark... it was meant to note that despite amount of whining about the cashew that goes on, when the OP asks what the problem is, no one can come forward with anything.\r\n\r\nFor those who don't know the bueller reference, its from this film: http://www.imdb.com/title/tt0091042/"
    author: "borker"
  - subject: "BTW the ctrl, shift and alt"
    date: 2009-10-24
    body: "BTW the ctrl, shift and alt shortcuts work in KDE as well - it's just that for normal users (who don't know those) it's easier to choose what to do with a file instead of having the OS guess wrong 1 out of every 10 times they copy something. That's not consistent and makes life harder on the user.\r\n\r\nAnyway, these examples might not be the ones I would pick when describing great solutions to usability issues the KDE community has come up with, but they are what an honest outsider considered great. And, more importantly, he saw what we were doing: trying to find solutions where we could have our cake and eat it too. Something the competition not always does, or not even tries to do. That's why I asked him to do this article."
    author: "jospoortvliet"
  - subject: "That's a matter of what a"
    date: 2009-10-25
    body: "That's a matter of what a default set up is. I suppose it's decided that offering such a menu makes the default more usable. When it comes to defaults you simply can't please everyone. You may complain about that menu and someone else might complain if it wasn't there (such as me for instance) and see a predefined drag and drop behavior as intrusive in a similar sense.\r\n\r\nThe best you can do when it comes to defaults is offer something most people will like, something reasonable, but then allow deviations that can be made without the existence of such a choice ruining the default experience.\r\n\r\nThough for this reason I would agree that an option to turn it off should probably exist somewhere. Perhaps it could be added as a tick box in the \"Context Menu\" tab of \"General\" section of the Dolphin preferences dialog. Maybe devs should consider it.\r\n\r\nKDE4 is not perfect, but I think the approach taken and the direction are good. It's not yet at the destination however."
    author: "memenode"
  - subject: "> Especially as there is no"
    date: 2009-10-25
    body: "> Especially as there is no way to \"teach\" the file manager what I want to do so that it does not ask the next time.\r\n\r\nJust hold down the ctrl-key while dragiing to force a copy without popup - or the alt-key to force symlinking. "
    author: "ArneBab"
  - subject: "Some ranting"
    date: 2009-10-25
    body: "KDE usability is not perfect but on the right path. Hands down: how many times do we receive emails with just a *lnk file attached cause windows users can not even see what they're doing when draggin' droppin files from the desktop to their email ;-)?\r\n\r\nAnd now for some ranting: (hint: I did already file several bug-reports)\r\nThe desktop is fine, but it is lacking behind when it comes to everyday-tools. E.g. there's still no (stable) working networkmanager-plasmoid (since 1 1/2 years now?). Well the svn version does not even compile... For bluetooth there's kbluetooth, but it's not working (incompatibility due to api changes in libbluez). No progress since abaout a year or so? A lot of the kio slaves are still not working properly (e.g. webdav...).... and there's no (working) webcam utility for kde either... \r\nGood that there're stable and reliable gnome-alternatives out there... but I'd prefer a coherent desktop.\r\n\r\n"
    author: "thomas"
  - subject: "Precisely Why I Am A KDE User Now"
    date: 2009-10-25
    body: "This is precisely why I am a KDE user now for the past year.  While KDE3 users loved to be able to configure their desktop to the n'th degree, I personally found it overwhelming and took comfort in GNOME simplicity.\r\n\r\nHowever, after I became familiar with GNOME, I found it frustrating that some simple changes were only accessible in gconf-editor or completely unavailable.  KDE4 changed all of that, and I find it to be a perfect balance between simplicity and power and will have a hard time using anything else!  Nepomuk is also exciting to me and can't wait to see how it evolves in KDE4."
    author: "asimonelli"
  - subject: "you can remove the cashew"
    date: 2009-10-25
    body: "Just set the desktop setting a different view  type and you are no longer bothered with the cashew"
    author: "whatever noticed"
  - subject: "disable nepomuk"
    date: 2009-10-25
    body: "you can disable nepomuk in the systemsettings of kde."
    author: "whatever noticed"
  - subject: "other file managers don't guess"
    date: 2009-10-25
    body: "Other file managers like windows explorer, don't guess what the user wants, it simply makes a choice for you, based on where you are dragging the file to.\r\nUnexperienced users don't know on what conditions explorer moves, copies or links  the file, so for them it is like a gamble: will the file actually get dragged to the other directory, or is windows going to create a link?\r\n"
    author: "whatever noticed"
  - subject: "you also cannot teach explorer what to do"
    date: 2009-10-25
    body: "On Windows, you also cannot teach explorer to always copy, move or link a file to the location you dragged it to.\r\n"
    author: "whatever noticed"
  - subject: "easy way to create folderviews"
    date: 2009-10-25
    body: "if you would like to add a third folderview, you don't need to add a new widget and configure it to show the desired directory, simply drag the directory to the desktop and select 'folder view' in the popup menu"
    author: "whatever noticed"
  - subject: "I wouldn't call that ranting :-)"
    date: 2009-10-26
    body: "It's ok to make criticisms, particularly when you back them up and (I haven't checked but I'll believe you) make bug reports. I think most of what you say is pretty fair.\r\n\r\nOne thing - knetworkmananger (not the plasmoid, but the kde4 port of the kde3 application) does appear to be working now - at least it's operational in prereleases of Fedora 12. It still needs a bit of love and a plasmoid that can live on the desktop, panel, wherever is still the better option for the long term I think."
    author: "swjarvis"
  - subject: "\"Hands down: how many times"
    date: 2009-10-26
    body: "\"Hands down: how many times do we receive emails with just a *lnk file attached cause windows users can not even see what they're doing when draggin' droppin files from the desktop to their email ;-)?\"\r\n\r\nNever.\r\n\r\n(SCNR)"
    author: "majewsky"
  - subject: "I see, but don't"
    date: 2009-10-27
    body: "That sounds like a useful and logical feature.\r\n\r\nNow what I need to figure out is why it doesn't work on my system. :-(\r\n\r\nThanks for the help."
    author: "JRT256"
  - subject: "webcam"
    date: 2009-10-27
    body: "> and there's no (working) webcam utility for kde either...\r\n\r\nDoes kdenlive work for you? \r\n\r\n-> http://www.kdenlive.org/\r\n\r\nIt isn't perfect for dv (I switched to using kino when I ran into errors), but it's great for video editing! "
    author: "ArneBab"
  - subject: "KNetworkManager4 != KNetworkManager3"
    date: 2009-10-27
    body: "The only thing both apps share is the name.  KNetworkManager4 is built on top of the same Network Management libraries used by the plasmoid."
    author: "Bille"
  - subject: "you have to unlock the"
    date: 2009-10-27
    body: "you have to unlock the desktop first"
    author: "mkraus"
  - subject: "I stand corrected "
    date: 2009-10-28
    body: "Title says it all really :-) Either way - it's a KDE app for managing my networks, nicely integrated (KWallet etc) so I'm pretty happy."
    author: "swjarvis"
  - subject: "Lock has some issues"
    date: 2009-10-29
    body: "Yes, that might be obvious, but it isn't to this user.\r\n\r\nI think that the Lock/Unlock Widgets function needs some work.  Perhaps two levels of locking would be an improvement."
    author: "JRT256"
  - subject: "Agree"
    date: 2009-10-31
    body: "I agree with you. Windows has a very dirty habit of moving a file when you drag and drop it without asking what you want to do. KDE gives you the option to move, copy or link the file, which makes a lot of sense.\r\nIt was a bit funny for me when I started using KDE in the beginning but now I can't imaging working without this feature.\r\n\r\nHave you ever marked all files in a particular directory in windows and then try to hold and move them all with the mouse but your hand slip? What does Windows do? Windows creates a copy of all those files in the same directory! Can you imagine having thousands of photos double in a given diectory?\r\nThat wouldn't happened on KDE that easily."
    author: "Bobby"
  - subject: "I don't know why the cashew"
    date: 2009-10-31
    body: "I don't know why the cashew should be a problem to anyone. I mean, it's in the corner up there, covered by open windows and if you don't click on it nothing happens.\r\nI personally find it very handy and it's unique. KDE is the only DE with such a nice nut ;)"
    author: "Bobby"
  - subject: "KDE4 Demonstrates Choice Is Not A Usability Problem"
    date: 2009-11-01
    body: "If an additional method can be added without making the other available methods any less effective and easy to use then there is no good reason not to, from the user experience perspective. This is a hugh problem because things that we had and worked far better have become a usability problem.Usability issue because they are gone . The only choice we have sucks and there are things we never get used to or want to . classic menu was removed the setting menu . Common sense should have been there to know it was ridiculous to remove them. Konq has had so may features removed but they are in the dophin and not all of us like dolphin. I like that swiss army knife. Tree has lost a lot of the functions it had in the right click , delete,rename... . gwenview instead of the html embedded . So drag drop is gone and you spend hrs trying to figure out how to get the better way back(html emmbedded) . Some new things are ok , But there are new ones that just aren't quick and efficient  .But the old ones are, it just doesn't make any sence to remove things that work better . lets get rid of the wheel its old eh? Anyway there are lots of things that should be there for us that liked it and still giving us the chioce and not making things less effective. As far as the peanut(looked like a tear drop to me) its ok. But the right click function is far superior. takes less time and less hastles to move the icons,widgets and add apps to the kicker(now panel) than it does with the peanut. My 3 fav apps(there are more ) but the ones I use every day are still frustrating after using it for a while here. konsole , took me hrs to find out how to get rid of the ctrl+page up/down to use in mecedit . the konsole is supposed to be to use the apps like you would from console(cli). Always having to restart it to save anything . MC(midnight commander) is such a bright white when you first open it. I had some people in #kde check it out and agreed . thats still not fixed and its been like that for quite a few months. But the pretty blue around the tab was more important. Looks really have become the major part now over usablity.Lots of features that were there making konsole easy to use are gone too. anyway I'm really disappointing that great apps are just cut down so bad they are frustrating and not enjoyable to use now. Other than konq file manager mode is still better than dolphin. Even mc is better than dolphin.\r\nThe last few weeks I've seen quite few people in #kde saying that since konq file manager mode and other apps aren't as good . they are just staying with kde3. I wish they would add the request but so far what I see as answer to people is disappointing. It more like we don't care how many people want it . Its not going to happen:(. Its hard enough figuring out how to put in a request too. that system is too darn complicated for most so they just walk away. I really wish too at first start there was the option to except defaults or custom. I really don't believe there are people that would love that option. It should have all the common old ways to edit . Took me days to get it to get back to not even half of whats gone. \r\n"
    author: "oneforall"
  - subject: "impression"
    date: 2009-11-01
    body: "I personally don't miss any functionality from the KDE 3 times - some ppl say the do but they can rarely say what exactly it is they are missing. Could you be more specific? cuz I'm under the impression developers are willing to help out but they don't know any better than that we're there by now."
    author: "jospoortvliet"
  - subject: "KDE 4.3 is what KDE should be"
    date: 2009-11-01
    body: "I have to admit, with my experiences with KDE 4.1 and 4.2, I was contemplating switching to GNOME. The bugs and quirks and visual problems were really painful. If I didn't have a hunch that the underlying technology was great, I wouldn't have stuck with it. It all came back to the last time I tried to write a GTK app... yuck! Qt is so much more developer friendly...\r\n\r\nI'm so very happy, however, with KDE 4.3. It has a few rough edges that will no doubt be polished in the future, but it's really quite excellent. Folder view turned from something clunky to changing the way I work. Dolphin took a little getting used to, but I really, really like it now. It's certainly better in every measurable way to MacOS's Finder. I say it's about as good as Windows 7's explorer. KWin is finally up to snuff with its predecessors, and moreso. \r\n\r\nThe only thing I actually hate: the new K menu. I use the traditional one, but it seems old and clunky. The new one is unusable -- even with Windows Vista/7 start menu is better. The only feature from the new is the search -- if I could have traditional + search box with some modern skinning, it would be amazing."
    author: "AaronTraas"
  - subject: "impression"
    date: 2009-11-02
    body: "well I did lay out some of them there ,which is a small chunk.\r\nthe tree in konq , missing right click fictions delete,rename,show hidden files. Yes you can split the window ot open another tab to do the rename of that folder but if you can't see how that is extremely annoying to do . Then you are totally lost and can stick wiht taking the long route . But I do not want you to be the cause of the rest of use that know there was and still should be a quick, short and better way to do it. Its the same as the peanut its fine to keep it But there was and still should be the org old quick and better functioning way to do it too. Go look at kde3 the developers should know all the right functions in both and many more of the apps. If I have to list then then yes I'll put it in kvm(qemu) to get all the names. I used them but never really bothered with the names. Kong file manger mode the Home.desktop is there in /usr/shared/applications/kde/ but has the line to stop it from showing in the kickermenu . Took me a while to find out why it wasint showing. thats crazy . Not evey one wants to use dolphin. I like that swiss army knife style of konq its very handy. I don't want that annoying put in a web url and have it open in konq window. I like the tabs for both web and file manger and the split windows and the tree. Konsole has a lot of the right functions gone too. rename its one off the top of my head. I liked it because of my depth perception with only 1 eye. going to the menu to do it I've changed the wrong one too many times , so I like the right click on the tab . There are many option there too . I forget the names. The saying things are made to make it easier is getting me to the point of I wish they would quit saying that. There are things that did but are removed . Konsole the right click on the tabs, hust under the File>new Window it had some of the shell to auto open midnight commander etc . The settings is horrible because it never applies. It  saves but you have to restart konsole . the default color is way to bright for mc and when you look at the color scheme its just the wrong color period(white).Plus at first start up there should be the chioce of excepting the defaults or to custom with all the old style .Why, very simple it makes it easier to know where they are compare to going and finding out the setting menu has changed and now it has the option to change it back to classic. But that took a long long time for it to be brought back. the things that are coming back in good common sense prove they should never have been dropped in the first place(choice). Some people like to take the long way every day , some of us don't when there always was a quicker more efficient way to do it. Forcing  people to use a slow method for a short time till its ported over is ok but sucks. But when it looks like its going to be permanent that is in no way good at all. All in all there are missing features that looks got preference over functionality. Thats not good in any way shape or form. Other than a look that needs a fix because it not showing up right. But I mean for example the tabs in konsole got the blue highlight and any other feature is left out in the cold. Fuctions(features) not new ones but old ones should be done first not the looks. The new panel(kicker) has a nice look but the clock having to go to the setting just to change time and format is not a pleasant way at all. Again its looks that wining over important features. This isn't a new feature to have to wait till kde 4 5 etc . This is one that should have been consider long before pretty highlights etc. Plus in konq picking gwen to be default is a bad idea. It should be the html . Then people can chose quickshow or gwenview. That what I had to do in kde3 and its better and more sensible that way. Why because they are both good in there own way, but its a matter of preference. the html is nice default style and genwview is an enhanced option type. But me I prefer the html because gwenview you can't grab the picture etc and drag drop. The right click functions is very limited. Plus I like quickshow better full screen quick esc to get out of it etc . As you see I'm saying neither of those should be default but options. because some people like quickshow better and the other way round. But they are both the optional style and should be an option not as default. It was thought and said that more people like the new menu from the big OLD K. But since it took along time for it to come back and the settings menu  etc Its proof that it was bad judgment to think every one would want all of the new stuff. Any way I hope all these important features are brought back soon or kde4 for me is a real disappointment.  right now I'm going to have to look at compiling konsole/konq kde3 and use it in kde4 but thats really going to be a pain. not deps etc just might not work either. syncing is one thing ..\r\n"
    author: "oneforall"
  - subject: "KDE 4.3 isn't what KDE should be"
    date: 2009-11-02
    body: "yeah the new k menu is really nice looking . But it is so slow and some thing you can and never want to get used too period:). The hover over of the classic is what makes it so much quicker to move a round in. and yeah I'd say the search and nice skin would be nice. But also the right click on an app and have it open the menu editor like kde3  would be agreat and I'm sure those that love the new one would like that too and really all the right click features are once again more important than new looks being addedto polish it off . Those can wait till kde4 -5 no :) . Use is always more important\r\n"
    author: "oneforall"
  - subject: "the only reason I stepped"
    date: 2009-11-04
    body: "the only reason I stepped into kde is cos konqueror - I aint found so far a more productive desktop experience that this - if I was KDE ceo I would buld all upon konqui and throw everithing else down the drain. only flaw is to not have a decent web browsing so I got to go with firefox - dolphins should be stay in the sea IMHO"
    author: "eevo"
  - subject: "I see the point but your"
    date: 2009-11-04
    body: "<blockquote>\r\nI see the point but your examples show exactly the opposite. Popping-up a menu to ask what I want to do every time I drag a file feels very invasive to me. Especially as there is no way to \"teach\" the file manager what I want to do so that it does not ask the next time.\r\n</blockquote>\r\nCheck the screen-shot again. Every single pop-up tells you what to do to avoid it. You already know one of the answer from your experience with Mac, and this popup tells you the others."
    author: "carewolf"
---
<em>A few days ago we found a nice <a href="http://www.nuxified.org/blog/kde4-overtaking-gnome-terms-usability">blog post</a> on the usability approach taken by the KDE community for the KDE 4 series. We have contacted the author to see if he was interested in doing a guest article for the dot expanding on his blog post. So without further ado, I present a writing by <a href="http://nuxified.org">Daniel Memenode</a>, web publisher and designer.</em>


KDE always stood out as a desktop environment that doesn't shy away from giving you lots of options and features. It is no wonder that one of its flagship products, Konqueror, was often compared to <a href="http://vivapinkfloyd.blogspot.com/2008/06/konqueror-359-review-great-swiss-knife.html">a swiss army knife</a>. It could be used as a file manager for both local and remote files, an image viewer and a fairly powerful web browser shifting from one role to the other as needed.

This however came with one downside in that it increased the perceived complexity of the desktop environment and increased the learning curve of a new user. KDE 4 was expected, among other things, to come with innovations which would possibly resolve this issue and I think it has already made some significant strides in that direction.

On face value KDE 4 didn't fundamentally change the <em>typical</em> layout of the desktop. There is still a main panel at the bottom with a K-Menu on one side of it, system tray on the other and task manager in between. The biggest visible change in defaults is that desktop icons now reside in a folder view widget and that the K-Menu is completely different. However behind the scenes everything changed and the nature of these basic desktop elements changed as well. They all became configurable plasma widgets thus actually providing the user even more flexibility than before.

This could mean that KDE 4 could be even more complex and come with an even greater learning curve than KDE 3, but that doesn't seem to have happened. Instead, in the experience of many including myself as someone who typically used GNOME exactly for its simplicity, KDE actually became at least a little simpler and more streamlined, and I think it is further evolving in that respect.

KDE 4 demonstrates the effort by the KDE community to make peace between these two seemingly mutually exclusive characters: power and flexibility (lots of options to choose from) versus simplicity and ease of use. The idea is to offer the user lots of choice and power over the way the desktop behaves without this feeling like a burden. It is to bring up the extra options when the user is actually looking for them and when they actually make sense - offering them in a way that doesn't add to the perceived complexity and doesn't ruin the usability of the default experience.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<img src="http://dot.kde.org/sites/dot.kde.org/files/contextmenu.png" width="303" height="213"/><br />
The context menu when dropping a text file on the desktop
</div>

One nice example of the way KDE did this even in KDE 3 is the contextual menu that appears when you drag and drop files or folders. It asks you if you want to move, copy, make a link or cancel the whole operation. In KDE 4.3 there are additional options added to this menu that depend on what kind of file you're dragging. For instance, if it's a picture and you're dragging it to the desktop, it lets you choose to make it into a desktop wallpaper. If it's a text file it offers the choice of making a sticky note widget from it. If you're dragging a folder it allows you to make an entire folder into a folder view widget.

This last example is especially interesting because this is where a whole new feature, one that new users may be unfamiliar with, is revealed to the user when it actually makes sense and may be quite useful. You're moving a folder and get the option to create a "folder view". Choose it and voila, folder view begins to make sense and you're that much closer to knowing what exactly a "folder view" option in a desktop settings dialog (available from a desktop right-click menu) does refer to. In fact, it may even become clear to the more tech savvy and experienced among us that this is what all desktops with icons including Windows, Mac OS and GNOME actually are; nothing but views of content of specific folders (like /home/user/Desktop).

Another example is a dialog for configuring toolbars in many KDE4 applications (Dolphin, Kate, Konqueror, Dragon Player etc.) which uses a relatively simple and logical interface that in one place allows the user to add from a pretty big list of possible toolbar buttons and position them wherever wanted by either dragging and dropping or using the arrows. It offers two methods for doing the same thing without one disrupting the other. All that's missing is the ability to drag and drop the buttons directly to the toolbar which on one hand would seem redundant, but on the other would provide the user a third choice without disrupting the existing two.

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:623px;">
<img src="http://dot.kde.org/sites/dot.kde.org/files/toolbars.png" width="623" height="434" />
Editing toolbars like it works in any KDE application
</div>


This demonstrates that choice of ways of doing things doesn't have to come at the expense of usability. If an additional method can be added without making the other available methods any less effective and easy to use then there is no good reason not to, from the user experience perspective.

This also stands true in the example of "+" and "-" emblems on files and folders in KDE4 which allow selecting and deselecting them without holding ctrl or drawing a selection rectangle. This too actually <em>adds</em> another way of selecting files and folders without disrupting an existing one, thus serving people of both preferences at the same time. Furthermore, this particular feature actually aids usability, especially if the desktop is configured to use a convenient single-click launch for files and folders where single click to select would also launch the file or folder. "+" and "-" emblems get around that problem.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<img src="http://dot.kde.org/sites/dot.kde.org/files/select-emblem.png" width="315" height="120"/><br />
Select emblem on hover
</div>

There are other examples that illustrate the principle of offering flexibility without impairing ease of use. But perhaps most important ones involve the reinvention of primary desktop workspace elements as plasma widgets. This almost turns KDE4 users into designers of their own perfect desktop, should they want to be that. Everything is exchangeable and mobile. You don't need to have a typical bottom or top panel. You can mix and mash desktop elements in any way you like them. You can even have multiple arrangements at the same time by taking advantage of desktop activities.

Is this extra amount of choice really impairing the usability of those who may want to simply stick to the familiar defaults? No, because by making all workspace elements into a widget they have all been put on the same fundamental level where each can be manipulated in the same basic ways.

It's not like you have to go into some sort of an editor or a configuration dialog with a slew of options presented via textual descriptions you don't necessarily understand in order to completely transform your desktop (which actually reminds me of Compiz settings manager and even worse, gconf). Instead you literally take the elements by their handles and move them around. You manipulate them almost in the same manner you would arrange items on your real life desktop. And if you need finer tweaks you can open the relevant settings dialog right from what you're editing. 

This is why I am beginning to appreciate the persistence of the KDE project at giving users great power and flexibility. I no longer believe that having lots of choice necessarily means a harder to use desktop. I think KDE 4 is well on its way to proving that point. In a sense it seems to show that good usability isn't necessarily about creating an interface that is "perfect" and "usable" for everyone with a single set of options and a single "mindset", but rather about providing a desktop that can do nearly everything anyone wants it to do, but do it only IF the user actually chooses so without impairing the experience of those who don't make that choice. 

In other words, the desktop morphs to the subjective preferences and personality of its user rather than trying to assume what most users want. If usability is to some extent in the eyes of the beholder, as I think it is, then such a desktop adapts to that reality and becomes a perfectly usable desktop not by the definition of a particular group of experts, but by the definition of the user standing in front of it.

If that's the vision behind KDE 4, as it seems to be, then I think the potential is limitless. Of course, literal achievement of such a potential may be an unreachable ideal, at least with current technologies, but KDE 4 is making significant steps forward in that direction. Just proving that choice is not necessarily an usability problem is a big accomplishment in itself.

<b><em>ABOUT THE AUTHOR:</em></b>

Daniel Memenode is an alias of a web publisher, designer and main content producer behind a FOSS community site <a href="http://www.nuxified.org/">Nuxified.org</a> and other sites such as <a href="http://libervis.com">Libervis.com</a> and <a href="http://doubleplushuman.com">DoublePlusHuman.com</a>, projects which are typically made to promote a particular set of good ideas, from Free Software and responsible use of technology to individual and social evolution (namely, promotion of self improvement and <a href="http://en.wikipedia.org/wiki/Voluntaryist">voluntaryism</a>).