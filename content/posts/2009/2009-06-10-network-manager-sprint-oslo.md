---
title: "Network Manager Sprint In Oslo"
date:    2009-06-10
authors:
  - "Frederik"
slug:    network-manager-sprint-oslo
comments:
  - subject: "time frame for wicd support "
    date: 2009-06-13
    body: "Hi,\r\nFirst, this is great that you're working on this, it's a real weakness in KDE for the moment.Is there any expectation as to when support for the wicd backend in the networkmanager plasmoid might be ready (in between KDE 4.3 and 4.4, or after 4.4)? Thanks a lot."
    author: "sk8glad"
  - subject: "Wicd support"
    date: 2009-06-16
    body: "Even if there is not a timeframe for the applet release, it should be there within the first release or shortly after."
    author: "drf"
---
A small but intense code sprint took place in Oslo last weekend. Peder Osevoll Midthjell, Sveinung Dalatun and Anders Sandven, who work on mobile broadband connections for Linux as their thesis project, met with Darío Freddi, Will Stephenson and Frederik Gladhorn of KDE. Knut Yrvin spent his weekend with us to make us feel comfortable at Qt Software.

<center>
<a href="http://dot.kde.org/sites/dot.kde.org/files/NetworkManagerSprint.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/NetworkManagerSprintt.jpg"</a>
</center>

Starting on the afternoon of June 5th, intensive discussions were held, first regarding the students' project, then about the structure of the Network Manager plasmoid. It became clear quickly that a more abstracted approach for the Network Manager backend is desirable, also to make the applet simpler. This lead to large restructuring after careful planning and a coding frenzy on the following two days.

Many thanks go to Will, who organized the meeting and the KDE e.V. for sponsoring the event. We would also like to thank Qt Software for providing us with a nice office and pizza!

We had a great time in Oslo, including lunch with Thiago, Simon and Olivier. Olivier also took us bar-hopping on Friday evening and to Viegeland Park on Sunday.