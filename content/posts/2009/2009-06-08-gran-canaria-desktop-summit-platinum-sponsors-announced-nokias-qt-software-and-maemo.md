---
title: "Gran Canaria Desktop Summit Platinum Sponsors Announced: Nokia's Qt Software and Maemo"
date:    2009-06-08
authors:
  - "sebas"
slug:    gran-canaria-desktop-summit-platinum-sponsors-announced-nokias-qt-software-and-maemo
---
<div style="float:right">
<a href="http://www.qtsoftware.com">
<img src="http://dot.kde.org/sites/dot.kde.org/files/sponsors_qt.png" />
</a>
</div>
<p>
The KDE and GNOME communities are happy to announce the Platinum sponsors of the upcoming <a href="http://grancanariadesktopsummit.org/">Gran Canaria Desktop Summit</a>. Nokia's <a href="http://www.qtsoftware.com">Qt Software</a> and <a href="http://www.maemo.org">Maemo</a> will be the main sponsors of the event, which will be held from 3rd to 11th of July 2009 in Las Palmas on Gran Canaria, Spain.
</p>
<!--break-->
<p>
<em>"Nokia's Qt Software and Maemo have both had significant involvement with the KDE and Gnome communities. The contributions these communities have made to us is extremely valuable and we wanted to show our support by sponsoring the Gran Canaria Desktop Summit,"</em> said Aron Kozak, Nokia, Qt Software's head of web and community. <em>"We are looking forward to this event and to spend more time meeting and speaking with people there."</em></p>

<div  style="float:left">
<a href="http://www.maemo.org">
<img src="http://dot.kde.org/sites/dot.kde.org/files/sponsors_maemo.png" />
</a>
</div>
<p>The Gran Canaria Desktop Summit is a co-hosted event around the Free Desktop, featuring Akademy and GUADEC, KDE's and GNOME's yearly flagship conferences. The programme is being finalised at present, with much of it already confirmed, see <a href="http://grancanariadesktopsummit.org/node/10">here</a>. 
The KDE and GNOME communities are very grateful for Nokia's support of the event, and are looking forward to productive sessions in the respective conference tracks and the cross-desktop sessions that will provide a strong foundation for improved collaboration across these Free software projects.
</p>