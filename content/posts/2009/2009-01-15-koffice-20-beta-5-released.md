---
title: " KOffice 2.0 Beta 5 Released"
date:    2009-01-15
authors:
  - "iwallin"
slug:    koffice-20-beta-5-released
comments:
  - subject: "odf-lib"
    date: 2009-01-14
    body: "hi there everybody!\na loong time ago, i read in the \"road to kde 4\"-articles (nice name btw) about a odf-library. is this peace of software already existing? so that it could be shared between open source-projects/kde-components?\nthanks!"
    author: "anonymaus"
  - subject: "Re: odf-lib"
    date: 2009-01-15
    body: "The code exists since we used in koffice, but it's not ready yet to be shared between other open source projects."
    author: "Cyrille Berger"
  - subject: "Screenshot?"
    date: 2009-01-15
    body: "Can anybody tell me why the screenshot shows a windows of OpenOffice-Draw? I mean the one in the back not the one in the front."
    author: "Timmy"
  - subject: "Re: Screenshot?"
    date: 2009-01-15
    body: "I think it represents the improved interoperability between the two office suites thanks to the big ODF improvements in KOffice."
    author: "Yortx"
  - subject: "Re: Screenshot?"
    date: 2009-01-15
    body: "i hope it does.  I just tried Kspread2 on a ODS file that had been converted from XPS and it got most of it correct, a few cells lost its \"number\" format and became \"generic\" in Kspread2 and got weird values displayed.  Once i reformatted the cells to \"number\" it was fine.\nI, as a normal user, tried to open an XLS file (owner root:root - i forgot to chown it first) by mistake in Kspread2 and it just sat there and didn't warn me that either it was the wrong format or had the wrong permissions. I had to terminate Kpread2 to get out of that one. \n\nI looking forward to getting rid of my dependence on OO. "
    author: "Ian"
  - subject: "Re: Screenshot?"
    date: 2009-01-16
    body: "This is important stuff to bug report.  Can you share the spreadsheet or is it secret?  If so, would it be possible to anonymize by e.g. changing the text cells?  We really really want to get this right."
    author: "Inge Wallin"
  - subject: "Re: Screenshot?"
    date: 2009-01-16
    body: "I'll try to remove any personal stuff from it and get it posted.  I'll also retest the sequence to make sure that it can be reproduced."
    author: "Ian"
  - subject: "Re: Screenshot?"
    date: 2009-01-16
    body: "I've submitted a bug report (#18098), with sample attachments, for the formatting issue. \nThe opening of the XLS issue seems to have gone away, i think i was just impatient as i'm using KDE4 with a nvidia board and its still slooooow."
    author: "Ian"
  - subject: "Re: Screenshot?"
    date: 2009-01-17
    body: "if you run the (yet) beta driver , it's fine. just be patient, or install 180.xx"
    author: "sfdg"
  - subject: "Re: Screenshot?"
    date: 2009-01-15
    body: "because it is the better product and looks far more mature than Koffice ?"
    author: "anon"
  - subject: "Re: Screenshot?"
    date: 2009-01-15
    body: "yawn"
    author: "cm"
  - subject: "Re: Screenshot?"
    date: 2009-01-16
    body: "Yawn? Truth hurts, eh?"
    author: "Marc J. Driftmeyer"
  - subject: "Re: Screenshot?"
    date: 2009-01-16
    body: "No, just trolls like you are boring."
    author: "Jason Beesen"
  - subject: "Re: Screenshot?"
    date: 2009-01-15
    body: "Yeah, until you change your theme and get a harsh reminder that it's written in frigging JAVA."
    author: "ethana2"
  - subject: "Re: Screenshot?"
    date: 2009-01-19
    body: "It's not. But ignorance reigns supreme."
    author: "donald"
  - subject: "Re: Screenshot?"
    date: 2009-01-15
    body: "Actually it's Impress and not Draw, and the other application in the screenshots is KPresenter.  The point is to show interoperability between KOffice (Kpresenter in this case) and OpenOffice.org through the OpenDocument file format."
    author: "Inge Wallin"
  - subject: "Re: Screenshot?"
    date: 2009-01-15
    body: "Oops, my bad.  It is indeed Draw and Karbon, not Impress and Kpresenter.  But the point remains."
    author: "Inge Wallin"
  - subject: "i know, it's a bad request"
    date: 2009-01-15
    body: "but it would be nice to have in the future a good support also for the office formats.... some people still send files in .doc or stuff like that (and obviously i reply with an .odt)... but if koffice want to be quite popular it should consider this thing (ovbiously after the other more important stuff like a good/perfect support of the open document format)"
    author: "jackie"
  - subject: "Re: i know, it's a bad request"
    date: 2009-01-15
    body: "KOffice already has support for .doc and the other MS Office formats.  And actually it's better than most people think.  Try it!\n\nThat said, I'm not 100% sure what the status of the MSO filters is for 2.0.0."
    author: "Inge Wallin"
  - subject: "Re: i know, it's a bad request"
    date: 2009-01-15
    body: "The excel filters have just gotten a big boost yesterday. The .doc filter is quite good already, it's kword that cannot yet display everything kword can. Using koconverter to convert .doc to .odt gives quite nice results, as can be verified with OOWriter."
    author: "Boudewijn"
  - subject: "Re: i know, it's a bad request"
    date: 2009-01-15
    body: "The excel filters have just gotten a big boost yesterday. The .doc filter is quite good already, it's kword that cannot yet display everything kword can. Using koconverter to convert .doc to .odt gives quite nice results, as can be verified with OOWriter."
    author: "Boudewijn"
  - subject: "Re: i know, it's a bad request"
    date: 2009-01-18
    body: "Does it save to MSoffice formats though?"
    author: "Jussi"
  - subject: "beta 5 on Windows?"
    date: 2009-01-15
    body: "At the moment I only have a Ubuntu 8.10 and Windows Vista dual boot. Because I don't want to 'pollute' (don't get me wrong, I will certainly switch from GNOME to KDE 4 once it's a bit more mature) my Ubuntu installation with KOffice and all it's dependencies on Qt and KDE, I'd like to test on Windows.\n\nHowever, last time I checked with the Windows installer, KOffice beta 4 wasn't available, merely an older beta AFAIK. When will beta 5 be packaged for Windows, or is it already available?\n\nBy the way, with beta 6 being the last beta mentioned in the release plan \u0096 http://wiki.koffice.org/index.php?title=Schedules/KOffice/2.0/Release_Plan \u0096 that means there will be more beta's if necessary? Once all bugs for beta releases are fixed, a release candidate will be released? So it can take a few months before we see the final release?"
    author: "Alexander van Loon"
  - subject: "Re: beta 5 on Windows?"
    date: 2009-01-15
    body: "I don't know about the windows release -- that's for someone else to answer. But yes, if we are not satisfied that we've reached a release candidate stage, we will release more beta releases than are planned right now."
    author: "Boudewijn Rempt"
  - subject: "Re: beta 5 on Windows?"
    date: 2009-01-15
    body: "Ok, the lowdown on the windows builds is that Patrick Spendrin no longer has a windows build machine. That's why we haven't got packages right now -- which is a pity, but it also offers an admirable point for someone else to step in and start helping out!"
    author: "Boudewijn Rempt"
  - subject: "KOffice 2.0 stable?"
    date: 2009-01-15
    body: "I hope KOffice 2.0 will be really stable as KDE 4.2 is now. I think KOffice 2.0 final can be released also next year if this can produce something very stable and usable. I'm a great fan of KWord and KPresenter and I'd like to see a good release also if this means other 12 months :) .\nI will try to use this version to report bugs to fix in time for the 2.0 final stable release."
    author: "Giovanni Venturi"
  - subject: "Re: KOffice 2.0 stable?"
    date: 2009-01-15
    body: "I *suspect* that 2.0 will be very similar to KDE 4.0 and Amarok 2.0 - i.e. not very full-featured or stable releases pushed out to get more testing and also to afford some psychological relief by ending the feature-freeze so that the devs don't feel stifled.  As with KDE 4.0 and Amarok 2.0, I then would expect rapid progress in fixing bugs and restoring features."
    author: "Anon"
  - subject: "Re: KOffice 2.0 stable?"
    date: 2009-01-15
    body: "2.0 won't be the end of the road for KOffice, that's for sure. And there will be regressions from 1.6 -- sometimes really important regressions, like a complete lack of table support. But we are committed to making 2.0 as stable a release as we can within its set of features. And yes, it'll be relief to finally have it released and be able to start working on 2.1 again :-)\n\nBut we do need the help of all you guys in the quest for stability. No matter how stupid and obvious you might think a crash or a bug is: the fact that it's in there means we didn't know about it, so please report it! We need people to test KOffice before we release the final, otherwise we _will_ release  a KOffice with lots of bugs."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice 2.0 stable?"
    date: 2009-01-15
    body: "I just experienced a nice bug: KWord crashes when you press \"Save\"... ;-) (seems to depend on whether the .odt was changed by OOo before or not)\n\nA bug report will be today or tomorrow."
    author: "Cyril"
  - subject: "Re: KOffice 2.0 stable?"
    date: 2009-01-15
    body: "Thanks!"
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice 2.0 stable?"
    date: 2009-01-15
    body: "Yes the dot article saying that kword got all its crash fixed, is for all *reported* crash, it's as much a display of progress than an encouragement to test things, and report new issues, so that the devs can make 2.0 rock :)"
    author: "Cyrille Berger"
  - subject: "Re: KOffice 2.0 stable?"
    date: 2009-01-16
    body: "Yep, I have the same issue.  Anytime I attempt to save a KWord document, it crashes.  However, since I am running Mandriva cooker I hesitated to say anything until I ruled out a packaging problem.  Since you have the issue as well, it seems to be a KWord issue.  Of course my main issue that I always bring up is not being able to save flake shapes within a KWord document.  That very well could be resolved, but since I can't save a document at all now there is no way of knowing."
    author: "cirehawk"
  - subject: "Re: KOffice 2.0 stable?"
    date: 2009-01-16
    body: "The bug is already reported and fixed: see http://bugs.kde.org/show_bug.cgi?id=180238"
    author: "Zagge"
  - subject: "Re: KOffice 2.0 stable?"
    date: 2009-01-16
    body: "The incredible bug fixing rate within the KDE4 project(s) IMHO shows what a powerful new framework KDE4 is.\nSoon stability and feature equivalence to KDE3 will be reached, and then KDE4 will just beat the s..t out of other DEs (including M$ & MA\u00a2. ;-)"
    author: "Cyril"
  - subject: "Re: KOffice 2.0 stable?"
    date: 2009-01-15
    body: "i think that the total lack of table support is a bad thing which will cause some/a lot of people don't use koffice util that is fixed :(\nbtw, will koffice adopt the same release cicle of 6 months as kde has ? (for the releases after the 2.0)"
    author: "jackie"
  - subject: "ODF compatibility in KOffice"
    date: 2009-01-16
    body: "OpenOffice is my only office suite, and I use it for both general and academic work, always saving in the ODF format. I too would like not to depend on OpenOffice, and have a \"native\" Linux suite. But if you want to work with ODF, OpenOffice (and derivatives) is the only choice. I will only consider KOffice when its ODF implementation is 100% compatible with that of OpenOffice."
    author: "Card"
  - subject: "Re: ODF compatibility in KOffice"
    date: 2009-01-16
    body: "ODF compatibility and OpenOffice compatibility isn't always the same. For example Karbon can save perfectly valid odf files, that OpenOffice isn't able to load correctly.\n\nBut the good news is that KOffice and OpenOffice are getting more and more compatible, as both teams are solving odf problems in their implementations. You will see significant improvements with the 3.x releases of OOo and the 2.x releases of KOffice."
    author: "Sven Langkamp"
  - subject: "Future of office"
    date: 2009-01-16
    body: "I have great respect for these developments. I just wonder whether the vision of cloud application space might not be true. If so then the Desktop application space is becoming irrelevant.\n\nI tried Adobe Buzzword, beta quality software, export does not work well, and I am amazed by the user interface ideas these guys had.\n\nThe new KOffice is nice but is it production ready as e.g OO.org? In fact it never has been. How can you make KOffice run as a Cloud application? "
    author: "Andre"
  - subject: "Re: Future of office"
    date: 2009-01-16
    body: "Didn't someone do a proof of concept of a plasmoid browser plugin?\n\nWho knows?  Maybe a few years from now, KOffice 3.0 is a plasmoid.\n\nI doubt it though.  I think KOffice is aimed at a different crowd."
    author: "T. J. Brumfield"
  - subject: "File extension FODF"
    date: 2009-01-16
    body: "Just a query as why kspread2 uses an extension of FODF instead of ODF if using ODF format when saving a spreadsheet. I was assuming that ODF format would ave defaulted to the same extension to show that it can be opened by any ODF compliant program - will this not be confusing to the ordinarty user? "
    author: "Ian"
  - subject: "Re: File extension FODF"
    date: 2009-01-16
    body: "That sounds vaguely familiar... I had the same problem this summer: .fodt is one of the valid extensions for odt documents and added by OpenOffice.  I discussed this with David Faure at the time on #koffice:\n\n\n01.07.2008-15:30 < dfaure> it has to be a code bug, unless you can really find fodt in /usr/share/mime or kdedir/share/mime, but I doubt that\n01.07.2008-15:31 < boud> actually, yes\n01.07.2008-15:31 < boud> boud@valdiesalie:/usr/share/mime> grep -R fodt *\n01.07.2008-15:31 < boud> globs:application/vnd.oasis.opendocument.text:*.fodt\n01.07.2008-15:31 < boud> packages/openoffice.xml:    <glob pattern=\"*.fodt\" />\n...\n\n1.07.2008-15:33 < dfaure> it's just an _extra_ pattern for this mimetype. odt files are still associated with the mimetype too.\n01.07.2008-15:34 < dfaure> it's just that the file dialog has to pick _one_ extension as the default one, and there's no real support for that anymore with shared-mime-info;\n01.07.2008-15:34 < dfaure> no replacement for the old X-KDE-NativeMimeType property.\n01.07.2008-15:34 < dfaure> so it takes the first extension from the list, so the bug is that fodt is first. I'll have a look at that.\n...\n01.07.2008-15:41 < dfaure> (hmm I see, update-mime-database uses a hash internally so the order isn't kept)\n...\n01.07.2008-15:47 < dfaure> ok, no fix possible with current shared-mime-info; I emailed the xdg list. I knew this problem would arise, just didn't have a use case for it up to now\n01.07.2008-15:48 < boud> cool :-)\n\nAfter that, I failed to ping David Faure again about this issue."
    author: "Boudewijn Rempt"
  - subject: "Re: File extension FODF"
    date: 2009-01-16
    body: "Of course, I probably _should_ have created a bug :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: File extension FODF"
    date: 2009-01-16
    body: "David actually _did_ contact the xdg list about this issue.\nhttp://lists.freedesktop.org/archives/xdg/2008-July/009732.html"
    author: "cloose"
  - subject: "Re: File extension FODF"
    date: 2009-01-16
    body: "It is an option actually saying \"Automatically select filename extension (.fods)\" at the bottom of the \"Save As\" dialog, so it seems correct. and it say similar on the kword2 \"Save As\" i.e. \"Automatically select filename extension (.fodt)\"\nSeems like a strange choice of extension guaranteed to add confusion.  \n\nAs these messages are hard-coded on the dialogs (i think) it doesn;t appear to match David's thoughts."
    author: "Ian"
  - subject: "Re: File extension FODF"
    date: 2009-01-16
    body: "Well, no, the messages really aren't hard-coded, but constructed out of the available patterns. If you, for example, grep through the KOffice source code, you won't find a single instance of .fodt in source or ui files."
    author: "Boudewijn Rempt"
  - subject: "Re: File extension FODF"
    date: 2009-01-16
    body: "Ok, thanks.  Hopefully it will sort itself out."
    author: "ian"
  - subject: "Re: File extension FODF"
    date: 2009-01-16
    body: "I think we'd better add a bug report about it, though. Will you or shall I?"
    author: "Boudewijn Rempt"
  - subject: "Re: File extension FODF"
    date: 2009-01-19
    body: "Done.... Bug 181271"
    author: "Ian"
  - subject: "is karbon as good as inkscape?"
    date: 2009-01-17
    body: "hi, is karbon a good replacement for inkscape or not yet? (for simple stuff, nothing complicated)"
    author: "arthur"
  - subject: "Re: is karbon as good as inkscape?"
    date: 2009-01-19
    body: "Karbon has an excelent curve editing tool in 2.0, which is on par feature wise with inkscape. Of course, Karbon misses a lot of the other feature of inkscape, the biggest thing it currently miss are filters (especially the blur one ;) )."
    author: "Cyrille Berger"
  - subject: "Great..."
    date: 2009-01-17
    body: "From what I've seen, and used, it looks like its coming along nicely. :)\n\nChange the name, and I can certainly guarantee I will advertise and how it off, but until then, nope. I'll save the embarrassment of explaing why I use a computer named KDE, or as I call, Kiddy Desktop Environment, and Kiddy Office. \n\nLol, sorry, I'm picky about these things. =p"
    author: "Jeremy"
---
Moving towards the 2.0 release with almost monthly beta releases, the KOffice team has once more honoured its promise to bring out beta releases of KOffice until the time is right for a release candidate. So today we bring you this beta with many, many improvements across the board. Incremental as it is, this beta is an important step towards a final release. So here it is: <a href="http://www.koffice.org/announcements/announce-2.0beta5.php">full
announcement</a> and <a href="http://www.koffice.org/announcements/changelog-2.0-beta5.php">changelog</a>.
<!--break-->
<h2>Highlights of the 2.0 beta 5 Release</h2>
<p> The developers have been working very hard fixing bugs and removing incompatibilities and other misfeatures. This can be seen in the <a href="http://www.koffice.org/announcements/changelog-2.0-beta5.php">rather large list of changes</a>. Here are some of the highlights of this release: </p>

<h3>Significant Progress with OpenDocument Saving and Loading</h3>
<p> Much of the work on this beta has been concentrated in making loading and saving OpenDocument files more complete. A lot of effort has been put into making KOffice interoperable with OpenOffice.org, since that is the most important OpenDocument editor today. </p>

<div style="border: thin solid grey; width: 666px;">
<img src="http://static.kdenews.org/jr/koffice-oo.png" width="666" height="367" alt="" /><br />
KOffice loading a document from OpenOffice.
</div>

<h3>All Reported Crash Bugs in KWord Fixed</h3>
 KWord has received extra attention that removed all known crash bugs. We want to emphasize that at this point it is extremely important for users to test all components of KOffice and report all bugs, especially crash bugs.</h3>

<p>Together with the Bugsquad team, a Krush day is going to be organised on Sunday 25th
                  January 2009, to help find new bugs before the final release.</p>