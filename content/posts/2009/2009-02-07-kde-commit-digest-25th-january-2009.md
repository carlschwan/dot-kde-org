---
title: "KDE Commit-Digest for 25th January 2009"
date:    2009-02-07
authors:
  - "dannya"
slug:    kde-commit-digest-25th-january-2009
comments:
  - subject: "ksysguard - feedback requested!"
    date: 2009-02-07
    body: "Hi all,\r\n  I want to make ksysguard, the task manager thing, as good as I can :-)\r\n\r\n  I'm thinking of adding scripting support to the process list thing.  There is a huge amount of information that can be gathered and shown for a process.  Open files, detailed memory usage, pixmaps owned, etc etc.\r\n  The idea would be that people can easily write their own javascript programs and send them to me, allowing me to add the best ones to the next release.\r\n\r\n  Anyway, I want your thoughts on this.  Would this be useful to you?\r\n\r\n  (I want to design it in a way that it uses no resources (cpu or memory) until actually used.  So no worries about bloat slowing things down)\r\n"
    author: "JohnFlux"
  - subject: "Great"
    date: 2009-02-07
    body: "Would be really nice to have for some quick and dirty debugging.  \r\n\r\nHowever ksysguard in 4.2 has a couple regressions that would be nice to have fixed too..  For example this bug makes it not nearly as nice looking as in KDE 4.1:  http://bugs.kde.org/show_bug.cgi?id=181070\r\n\r\nThanks for the cool tool though."
    author: "leos"
  - subject: "So? Don't ask permission,"
    date: 2009-02-07
    body: "So? Don't ask permission, give it a go :)"
    author: "Carutsu"
  - subject: "Make it use kross so that"
    date: 2009-02-07
    body: "Make it use kross so that people can write scripts in python, ruby, javascripts and even extend the ksysguard UI, and also make it use GHNS :) Thanks for the nice work."
    author: "patcito"
  - subject: "ghns"
    date: 2009-02-07
    body: "it already supports GHNS."
    author: "emilsedgh"
  - subject: "KContact dialog design"
    date: 2009-02-07
    body: "It is nice to hear that the contact details dialog has been redesign. However, as a Netbook user I can only shiver when looking at the screenshot: the dialog is far to tall. Please, dear developers, don't forget us Netbook users with small screen dimensions. Not everyone happens to have or wants screens with vertical dimensions in the 2000's."
    author: "plaude"
  - subject: "To be fair, that dialog is"
    date: 2009-02-07
    body: "To be fair, that dialog is only 567pixels high including windeco.  Should fit on most 2nd gen netbook screens.  With a more compact style it might even fit the original Eeepc"
    author: "leos"
  - subject: "To be fairer..."
    date: 2009-02-07
    body: "To be fairer, this leaves a whooping 33 pixels. Whow, impressive! To be even more fairer, this fair analysis thus tells Netbook users to either make their task list as low as possible or to hide it. Otherwise, the most important Ok and Cancel buttons will be clipped by the task list. I'm not sure if every Netbook user with a \"second generation\" screen will regard this as perfect usabilty."
    author: "plaude"
  - subject: "Re: Great"
    date: 2009-02-07
    body: "That bug is due to an old configuration file, rather than a regression as such.  The solution would seem to be to add some sort version number so that it doesn't try to load the old settings."
    author: "JohnFlux"
  - subject: "Bug fixed"
    date: 2009-02-07
    body: "Okay now fixed.  I added a version number to prevent loading old configurations."
    author: "JohnFlux"
  - subject: "It would be very nice to see"
    date: 2009-02-07
    body: "<p>It would be very nice to see X memory usage statistics for each process (like xrestop).</p>\r\n\r\n<p>Another idea is to support some kind of plugins for sensors. For example, currently ksysguard can't monitor temperature of NVidia graphic card (it exports it's temperature using X protocol extension handled on client side by libXNVCtrl.so). And it would be absolutly cool to see a plasmoid version of ksysguard monitors - like the old kicker plugin but with all cool things of plasma.</p>\r\n\r\n<p>And one more point. I like to thank you very much for your work on ksysguard - its already the best task manager that I've seen on all platforms !</p>\r\n"
    author: "vld"
  - subject: "Kapman -> Granatier"
    date: 2009-02-07
    body: "Actually, kapman is still kapman, but the mainwindow of granatier now shows granatier instead of kapman. I forgot to do it when I copied the source to use it for granatier, so Burkhard L\u00fcck changed it."
    author: "mkraus"
  - subject: "Move data to \"personal\" / \"business\" tabs?"
    date: 2009-02-07
    body: "<p>First of all thanks a lot Danny for another great commit digest! (btw: the \"By:\" is empty for a number of articles on dot.kde.org)</p>\r\n\r\n<p>Work on the addressbook and KDEPIM in general is great and very much appreciated! However, the KContact dialog (shown in the screenshot) is in my opinion a bit inconsistent. On the one hand, there are tabs for \"personal\" and \"business\". On the other hand the \"phones\" part of the widget shows \"home\", \"work\", \"mobile\". Would it not make sense to transfer these entries in the respective tabs \"personal\" / \"business\"?</p>\r\n\r\n<p>Additionally, there should be a possibility to have a private and a business email address (actually many of my colleagues also have two mobile numbers office / home).</p>\r\n\r\n<p>Moving these parts to the other tabs might also allow for making the dialog a bit smaller!? </p>\r\n\r\n<p>Thanks and keep up the great work!</p>"
    author: "mkrohn5"
  - subject: "Re: It would be very nice to see"
    date: 2009-02-07
    body: "> It would be very nice to see X memory usage statistics for each process (like xrestop).\r\n\r\n\r\nThere was actually a release that had this.  But it proved to be too slow to get the information, so I had to remove it.  I want to try to add this back in, implemented in a way to try not to slow everything down.\r\nBtw, I found (and fixed) some bugs in xrestop because of that work :-)\r\n\r\n\r\n> Another idea is to support some kind of plugins for sensors. For example, currently ksysguard can't monitor temperature of NVidia graphic card (it exports it's temperature using X protocol extension handled on client side by libXNVCtrl.so).\r\n\r\nWould be nice indeed.  I'm still not really sure how exactly. I'm thinking to allow the 'sensor' to be a javascript command.\r\n\r\n>  And it would be absolutly cool to see a plasmoid version of ksysguard monitors - like the old kicker plugin but with all cool things of plasma.\r\n\r\nThere's already several plasmoids that use the ksysguard plasma engine.  I use the cpu monitor one myself :-)\r\n\r\n\r\n"
    author: "JohnFlux"
  - subject: "> It would be very nice to"
    date: 2009-02-07
    body: "<p> > It would be very nice to see X memory usage statistics for each process (like xrestop). There was actually a release that had this. But it proved to be too slow to get the information, so I had to remove it. I want to try to add this back in, implemented in a way to try not to slow everything down. Btw, I found (and fixed) some bugs in xrestop because of that work :-) </p>\r\n\r\n<p>Hm, may be it can be implemented as separate thread that fills values when they become available ? i.e. initially column showing X resources is empty and then gradually get's filled with values. Of course it will also imply lower update frequency, but IMHO it shouldn't be a problem.</p>\r\n\r\n<p> > Would be nice indeed. I'm still not really sure how exactly.I'm \r\n> thinking to allow the 'sensor' to be a javascript command. </p>\r\n\r\n<p>What about implementing querying of several ksysguardd-like processes ? The implementing a new sensor will require creating new executable plus some desktop fill for ksysguard to find it. IMHO, if equipped with simple library to encapsulate ksysguardd communication interface it would be very convenient solution.</p>\r\n\r\n<p> > There's already several plasmoids that use the ksysguard plasma engine. I use the cpu monitor one myself :-) </p>\r\n\r\n<p>Didn't know that. Thanks for a hint - I'll try it (and especially plasma engine itself :)</p>"
    author: "vld"
  - subject: "Show graphs by default"
    date: 2009-02-07
    body: "It would be nice if ksysguard would have tabs and show the process-list (as it does already now), and additionally several graphs with one or more of the following:<br><br>\r\n\r\n- cpu usage<br>\r\n\r\n- system load<br>\r\n\r\n- memory + swap usage<br>\r\n\r\n- network bandwidth<br>\r\n\r\n- logged users<br>\r\n\r\n- some system stats as uptime, load avg, number of processes<br>"
    author: "yglodt@yahoo.com"
  - subject: "And one more idea. A button"
    date: 2009-02-07
    body: "And one more idea. A button in \"system activity\" (the one embedded into krunner) to launch full ksysguard interface. It would not only be very convenient, but also will greatly improve discoverability for new users."
    author: "vld"
  - subject: "Awesome :)  Thanks"
    date: 2009-02-07
    body: "Awesome :)  Thanks"
    author: "leos"
  - subject: "."
    date: 2009-02-07
    body: ">> To be fairer, this leaves a whooping 33 pixels.<br/><br/>\r\n\r\nThe goal is to fit on the screen, not leave room for you to paint a picture in the remaining space.<br/><br/>\r\n\r\n>> thus tells Netbook users to either make their task list as low as possible or to hide it.<br/><br/>\r\n\r\nPretty much a no-brainer when your screen is that small.  \r\nAnd I already mentioned if you use a different style rather than Oxygen you should be able to save lots of space.  "
    author: "leos"
  - subject: "tote"
    date: 2009-02-07
    body: "hi,\r\ntote sounds interesting. are there somwhere more information about this program?? what does it do excatly?\r\n\r\n"
    author: "Regenwald"
  - subject: "Process explorer"
    date: 2009-02-08
    body: "I'd recommend looking at Process Explorer for inspiration. This is a very useful 'task manager' replacement for Windows, and it gives a lot of information about processes.\r\n\r\nMy wish request is here: https://bugs.kde.org/show_bug.cgi?id=127728\r\n\r\nI love to hear what you think about this!"
    author: "vdboor"
  - subject: "Development has started"
    date: 2009-02-08
    body: "Development has started recently, so it does not do much right now (draw strokes/text, move/save/load them). I had hoped tablet support would come in basket, but the port of basket is not done.\r\n"
    author: "christoph"
  - subject: "CPU temp sensor"
    date: 2009-02-09
    body: "A plasmoid that can be embedded on the panel that displays CPU's temp would be awesome. Using KSensors is a bit of a pain, I'd like to avoid using any KDE3 apps at all :)"
    author: "NabLa"
  - subject: "Just drop the temp plasmoid"
    date: 2009-02-09
    body: "Just drop the temp plasmoid on the panel ;-)"
    author: "jospoortvliet"
  - subject: "I don't understand.\nDoesn't"
    date: 2009-02-09
    body: "I don't understand.\r\n\r\nDoesn't it do that already? There are tabs and you can have graphs on them..."
    author: "jospoortvliet"
  - subject: "Tote Development"
    date: 2009-02-09
    body: "The goal for Tote is to have a simple and usable notebook for tablet pc users. For now that means a canvas on which one can place pen marks, text and images. Think of a KDE version of Xournal, but more focused on taking notes that treats pen, text, images on the same level.\r\n\r\n\r\nAs the other replier mentioned, Tote is still in a very experimental state. It will take a large amount of work to get it to where it can replace my notebook."
    author: "astromme"
  - subject: "Hmmm. would it be efficient"
    date: 2009-02-10
    body: "Hmmm. would it be efficient to build such a tool upon the KOffice 2 infrastructure?"
    author: "jospoortvliet"
  - subject: "Sounds like KWord to me."
    date: 2009-02-11
    body: "I think you should take a look at KOffice2 where most of these things already work or are in progress of making work.\r\nThe default settings for things like the caligraphy tool and the default template may be changed as well as the default set of plugins (dockers, tools) you want to enable may need to be changed.\r\n\r\nBut thats a days work, not really comparible to reimplementing all this yourself..."
    author: "zander"
  - subject: "Reply"
    date: 2009-05-19
    body: "I already mentioned if you use a different style rather than Oxygen you should be able to save lots of space.<br>\r\n<a href=\"http://www.thecoursework.co.uk/\">Course Work</a> | <a href=\"http://www.thedissertation.co.uk/\">Dissertation Help</a> | <a href=\"http://www.theonlinethesis.co.uk/\">Theses</a>"
    author: "jennifergarner75"
---
In <a href="http://commit-digest.org/issues/2009-01-25/">this week's KDE Commit-Digest</a>: New Plasmoids: "LionMail", "Magnifying Glass", "Spacer", and the Javascript-based "Victory Calendar". Further development on the "VideoPlayer", "Remember the Milk" and "Welcome" Plasmoids. Configuration dialog for turning off notifications in the <a href="http://plasma.kde.org/">Plasma</a> panel via the system tray. Initial import of a simplified desktop based on Plasma (top and bottom panels) for educational settings. KDialog uses Plasma notifications (via D-Bus) if possible (keeping KPassivePopup as the fallback option). A configurable tray icon, and transparency support in KRuler. Various improvements in KSysGuard and <a href="http://amarok.kde.org/">Amarok</a> 2. "Dynadraw" painting is added to <a href="http://www.koffice.org/krita/">Krita</a>. The "Birthday" resource in <a href="http://pim.kde.org/">KDE-PIM</a> is ported from KResource to <a href="http://pim.kde.org/akonadi/">Akonadi</a>, and a database browsing tool for the internal Akonadi database appears. Ability to exclude private/confidential events from printouts in <a href="http://korganizer.kde.org/">KOrganizer</a>. Support for sending files over the Skype protocol in <a href="http://kopete.kde.org/">Kopete</a>. The Kepas tool can now send KNotes. Support for sending and receiving direct messages, and using secure HTTPS connections in <a href="http://commit-digest.org/issues/2009-01-11/#2">choqoK</a>. First simple version of a bookmarks tool added to Okteta. Start of work on a pure Qt version of KCachegrind: QCacheGrind. Support for matching "simple expressions" in <a href="http://edu.kde.org/kalgebra/">KAlgebra</a>. Various unfinished features temporarily removed from <a href="http://edu.kde.org/parley/">Parley</a> for the KDE 4.2 release. The Kapman game is renamed "Granatier". Initial import of "Tote" (a tablet-based notebook application), and "Kamala" (astrology software). The milestone "Release 1" of the PIM suite student project is tagged. KDE 4.2.0 begins to be tagged in preparation for release.<br><br>Also in this Digest, Tobias Koenig introduces a rewrite of <a href="http://www.kde.org.uk/apps/kaddressbook/">KAddressBook</a>, KContactManager. <a href="http://commit-digest.org/issues/2009-01-25/">Read the rest of the Digest here</a>.
<!--break-->
