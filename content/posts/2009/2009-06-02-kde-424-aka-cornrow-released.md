---
title: "KDE 4.2.4 a.k.a. CornRow Released"
date:    2009-06-02
authors:
  - "sebas"
slug:    kde-424-aka-cornrow-released
comments:
  - subject: "Hmm, its been a day now since"
    date: 2009-06-05
    body: "Hmm, its been a day now since compiling this from source and it has to be my imagination. Koqueror seems, feels faster than 4.2.3. "
    author: "stumbles"
  - subject: "Thanks for the update"
    date: 2009-06-05
    body: "Thank you guys for taking time out to update KDE 4.2 even though you are working so hard to make KDE 4.3 as perfect as can be.\r\n\r\nIt seems like the more perfect KDE 4 gets is the less people comment, kinda funny."
    author: "Bobby"
  - subject: "Other OS's?"
    date: 2009-06-09
    body: "What other OS's there is getting packages soon like Linux? FreeBSD? Windows? SunOS?\r\n\r\nI would like to try KDE 4.2 with SunOS but packages seems to be little late. "
    author: "Fri13"
---
The KDE Release Machine seems unstoppable these days! <a href="http://kde.org/announcements/announce-4.2.4.php">Today brings you</a> KDE 4.2.4, the monthly update to the 4.2 series of KDE. KDE 4.2.4 is the recommended update for all those using KDE 4.2, or rather anything in the KDE 4 series. Those that stayed away from KDE 4 until now might give it a whirl as well to see if KDE 4 is up to their tasks.
As it currently looks like the KDE team wants to fully concentrate on rock-stabilizing the 4.3.0 release that's shaping up nicely in KDE's Subversion trunk, so likely there won't be a KDE 4.2.5. If no grave problems, such as security issues arise, KDE 4.2.4 is the last update to the KDE 4.2 series.
Packages for your favorite Operating System will as usually arrive shortly, as we made the sources available to the packagers a bit earlier already.

