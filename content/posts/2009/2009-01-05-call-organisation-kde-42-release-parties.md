---
title: "Call for Organisation of KDE 4.2 Release Parties"
date:    2009-01-05
authors:
  - "jrepinc"
slug:    call-organisation-kde-42-release-parties
comments:
  - subject: "spelling mistake"
    date: 2009-01-05
    body: "s/stabalisation/stabilisation"
    author: "Anon kde fan"
  - subject: "Re: spelling mistake"
    date: 2009-01-05
    body: "Any one deciding to throw a party 'down under'?"
    author: "slashdevdsp"
  - subject: "Re: spelling mistake"
    date: 2009-01-05
    body: "What about Jamaica? I will be there on the 27th, would love to celebrate the 4.2 release."
    author: "Bobby"
  - subject: "First beta?"
    date: 2009-01-05
    body: "> stabalisation and feature completement\n\nIn other words - you're claiming that on Jan 27 we'll have the first beta-quality KDE4 release? Actually I didn't think KDE4 would leave alpha-stage before 4.3 :). Cool!"
    author: "slacker"
  - subject: "Re: First beta?"
    date: 2009-01-05
    body: "> Actually I didn't think KDE4 would leave alpha-stage before 4.3\n\nI'd say don't hold your breath for it, 4.4 or probably 4.5 will the first stable/full featured release. Right now, there are still too many outstanding issues, but it's usable though I wouldn't install it or tell friends to install it, they would get a very bad impression of KDE. I just tell them to stay with 3.5 or Gnome for now."
    author: "kubunter"
  - subject: "Re: First beta?"
    date: 2009-01-05
    body: "I have KDE4 on a desktop, and Windows Vista on a laptop.  If MS can call that gold, then we can call KDE4.2 beta.  And I still prefer KDE4.2 because it is more stable than Vista, even when the underlying OS is beta."
    author: "bigpicture"
  - subject: "Re: First beta?"
    date: 2009-01-05
    body: "4.2 beta2 really rocks. In my impression, it goes further than KDE 3.5, although not completely as stable as 3.5 (how could it be!).\n\n4.1 was quite usable for me, but from 4.2 I'm all in for the 4th generation the K Desktop Environment."
    author: "Pascal"
  - subject: "Re: First beta?"
    date: 2009-01-05
    body: "I agree.\n\n4.0 was a disaster, but 4.1 is good enough for daily use.  Now I can not work without KDE 4.1, and never think of jumping back to KDE 3.5.x.\n\nGo KDE, I believe that you can make it. :-)"
    author: "Franklin"
  - subject: "4.x is terrific!"
    date: 2009-01-05
    body: "Just thought I'd throw in my 2 cents instead of sit back silently like usual -- I agree! Too much needless negativity on here lately, and not enough congratulations or at least constructive criticism.\n\n4.x has made superb progress, I use 4.1 now and love it, and flinch at the thought of going back to 3.5 -- not that 3.5 is bad, it was a joy to use, but it looks somewhat \"antique\" now, once you've used KDE4. It's a much smoother experience in general; the under-the-hood work that went in I think shows itself in all of the little settings and toolbars and even look-n-feel, things you can't really point out on command but you can tell when they're not there. That's how I feel with 3.5 now, it's just not quite right anymore. 4 is my home. It's both usable and beautiful.\n\nI tried the 4.2 betas on OpenSUSE, sure a little buggy here and there but already leaps and bounds above 4.1, and those little nitpicks sound like they'll be ironed out in the official release. And, I am quite sure future releases will only improve exponentially! Particularly now that other apps like Amarok and KOffice are reaching their new iterations (which I've used those betas too and like what I see), it's all coming together into a terrific package that looks and feels very modern and very streamlined.\n\nCongratulations to everyone that has worked on KDE in any manner, you've done wonderful work!"
    author: "Positive Guy"
  - subject: "Re: 4.x is terrific!"
    date: 2009-01-05
    body: "and to mention i run's on windows an mac too !!!!\n\nthis is so great !"
    author: "cross plattform!"
  - subject: "Re: 4.x is terrific!"
    date: 2009-01-05
    body: "Kde4 is rocking hard, with lots of bugs fixed, and the desktop shell plasma is a dream come true\nSome screenies \nhttp://img183.imageshack.us/img183/8224/snapshot3yz5.jpg\nhttp://img223.imageshack.us/img223/151/snapshot2ew5.jpg\n:)\n\nps: screenies contain bugs, found them yet? :)"
    author: "slashdevdsp"
  - subject: "Re: 4.x is terrific!"
    date: 2009-01-05
    body: "Totally forgot to mention the rate at which booogs are fixed, one word - amazing :) It is starting to shape into a solid release"
    author: "slashdevdsp"
  - subject: "Re: 4.x is terrific!"
    date: 2009-01-05
    body: "ps: those screenshots were used to highlight some bugs, i.e., used in my bug reports -  they are already fixed or being worked on :)"
    author: "slashdevdsp"
  - subject: "Re: 4.x is terrific!"
    date: 2009-01-05
    body: "I also agree!\n\nWith KDE 4.2, KDE is not only rocking, but starts shining!\n\nThe development and stabilization speed is incredible!\n\nCheers!"
    author: "Neil"
  - subject: "Re: 4.x is terrific!"
    date: 2009-01-06
    body: "My girl friend started using KDE 4.1 with OpenSUSE 11.1. There are no visible performance problems (on a one year old all-Intel notebook), no crashes until now (except kwin when using OpenGL screensavers) and it looks just awesome :-)\n\nAlex\n"
    author: "Alex"
  - subject: "Re: 4.x is terrific!"
    date: 2009-01-06
    body: "I see in your screenshot the return of the ksysguard and other applet for the taskbar.  I really missed those in 4.1.\n\nIs 4.2 going to be optimized any better?  I found 4.1 on Fedora10 a bit slow, though relatively stable."
    author: "MGL"
  - subject: "Re: 4.x is terrific!"
    date: 2009-01-06
    body: "well the first applet (multiple bars is cpu/mem/network - meters) is in kdereview - this one is for kde4.3, but you can get it from here: http://websvn.kde.org/trunk/kdereview/plasma\nthe other separate meters are the cpu usage and network usage plasmoids moved to the panel :)"
    author: "slashdevdsp"
  - subject: "Re: First beta?"
    date: 2009-01-05
    body: "4.0 wasn't a disaster, it was a beta release to get people porting their applications to 4.0.  The only problem with 4 was that too many people did not understand this, and took it up expecting it to be a fully featured, fully working system.\n\nI've been using KDE 4 since the beta's of 4, and have yet to understand all the negativity about it, except that maybe some of the people complaining just like to be negative."
    author: "R. J."
  - subject: "Re: First beta?"
    date: 2009-01-05
    body: "I could not agree more.\n\nOne lesson learned from the KDE4 release is that when (or if :-) ) KDE5 is to be released then the naming of the first \"platform\" release should make it clear that it's not intended for everyone.\n\ncya /"
    author: "Birger"
  - subject: "Re: First beta?"
    date: 2009-01-06
    body: "I could not disagree more.\n\n\"Beta\" release means reasonable feature-completeness and stability level. 4.0 was an ALPHA release. We're only getting to beta quality right now. It wasn't a disaster - it was only an inappropriately named developer preview version showing great promise, and also annoying a hell of a lot of users that got misled by the version tag.\n\nThe only disaster I see here is at the marketing - not technical - level."
    author: "slacker"
  - subject: "Re: First beta?"
    date: 2009-01-11
    body: "I agree.  Although things didn't go as quickly as expected -- do they ever? (Murphy is always on the job).  It looks like the 4.2 release will be a good solid public Beta.  Why the releases were not properly labled is a great mystery to me and it is unfortunate that the developers were criticized because of this when the only legitimate criticism was that it was taking too long.  Please, nobody take offense at that, users aways want it yesterday -- for users, everything takes too long.\n\nIt looks like 4.2 has some new features that I really like, and it is still missing some little ones that I use every day in 3.5.  But, I will consider it to be the public Beta and try to make any comments in that context."
    author: "JRT"
  - subject: "Re: First beta?"
    date: 2009-01-05
    body: "I agree with you. There are a few missing bits and pieces like Kaffeine 4 isn't finished yet and doesn't have a Kaffeine icon (not blaming the KDE guys since Kaffeine is a different project). The same applies to a few other programmes and the hardware notifier/solid whatever isn't quite fit as yet but some things are further that 3.5x, it has features that 3.5x doesn't have and it's getting better everyday.\n"
    author: "Bobby"
  - subject: "Re: First beta?"
    date: 2009-01-05
    body: "Well, I really miss a KDE4 Rosegarden, with Solid and Phonon support. And a Qt4 Scribus too. The latter is well under way, the former... is in alpha. But we must wait..."
    author: "Alejandro Nova"
  - subject: "Re: First beta?"
    date: 2009-01-05
    body: "Oh man, let's open a forum bashing.kde.org where all those KDE4-haters may whine as much as they want...\n\nKDE 4.1.3 and 4.2beta2 work like a charm on my machines, I had no plasma crashes since 4.0.4 (except some driver issues), and the bug fixing rate is incredible (2 days ago I reported a panel bug, it was fixed in a heartbeat).\n\nIt's the same as with amarok 2: The devs make a totally new powerful code, but when presented many people complain that it has not yet all the features the old version has. They don't see that the new applications may run natively on other os (I love using dolphin and amarok on win) and that in their x.0 releases they are just a _base_ on which one can implement new an better features - take a look at the upcoming amarok 2.1, that'll be a killer app!"
    author: "Cyril"
  - subject: "Re: First beta?"
    date: 2009-01-05
    body: "I would say, let's open a forum to bash those who bash KDE 4 :D \nThere are still issues yes but I can't understand people who call 4 an Alpha. Where are we? Why can't they see the progress and improvements?\nit's really hard to please all the people all the time."
    author: "Bobby"
  - subject: "Re: First beta?"
    date: 2009-01-05
    body: "That's usually because people have their pet feature that doesn't work (or that works differently). Yesterday my sister's boyfriend was complaining you couldn't move icons from one panel to the other in KDE 4.2 and I showed him the solution in 5 minutes (also triggering a bug, which I have already reported to BKO). "
    author: "Luca Beltrame"
  - subject: "Re: First beta?"
    date: 2009-01-11
    body: "So you're saying it takes 5 minutes to move an icon from one panel to another? And that is supposed to be a good thing? Why cant drag-and-drop work like it used to?"
    author: "Kolla"
  - subject: "Re: First beta?"
    date: 2009-01-12
    body: "You have to remember, people used KDE 3.5 because they LIKED the features that it had, and the interaction with the desktop components. That's why it garnered a userbase in the first place.\n\nChanging user interface conventions, failing to emphasize feature parity, and making GUI changes that are not justified by real scientific usability testing is a good way to alienate those users who like KDE for the features and GUI: in other words, a good way to alienate ALL users.\n\nI'm glad your experience with your sister's boyfriend showed you that five minutes to perform a simple task, and then discovering a bug in the process, is unacceptable for anything other than alpha-level software. Perhaps you'll be willing to understand the critical point of view from now on."
    author: "CF"
  - subject: "Re: First beta?"
    date: 2009-01-06
    body: "And did either I or parent do any \"bashing\" of KDE? I love the progress being made - I am simply saying it is *not ready yet*. I am looking forward to seeing the first release-quality KDE4 desktop (which I hope will happen this year). I only object to the version naming policy - where clearly unfinished alpha-quality builds get a stable version number.\n\nSeriously, some people get overly touchy after the bash-athon that ensued after the first KDE4 release - with people spouting nonsense about \"going Gnome\", or similar. Calm down! ;)"
    author: "slacker"
  - subject: "Re: First beta?"
    date: 2009-01-07
    body: "I've been using Linux & KDE for 8 years now.\nMaybe it's because I use it for usual work (I wouldn't say I'm a power user), but since 4.1 the new KDE fits my demands very well, being nearly as stable as 3.5, at the same time it makes so much fun that I started exploring Linux more thoroughly then ever before.\nIn my opinion asserting that 4.2 has alpha quality ist already kind of bashing. Yes, it's beta, but I use it nearly every day on my lesser used pc, and it works just like a charm.\nSo, sorry if my reaction was a bit harshe, but let's not get carried away by that KDE4-must-have-everything-after-1-year-which-KDE3-had-after-5-years."
    author: "Cyril"
  - subject: "Re: First beta?"
    date: 2009-01-08
    body: "I never said 4.2 is (going to be) alpha quality. I've been saying that I EXPECTED it to be alpha quality like 4.1, and am very happy that, judging by what I've seen and heard, it looks like it will achieve what I consider beta quality. There was no irony in that.\n\nAnd I'm not saying that KDE 4 must have everything. I'm only saying that a release that purports being final SHOULD NOT feel severely underpowered when compared to previous releases. Yeah, so maybe I am a power-user pampered by the has-it-all-and-more KDE 3.5. Maybe casual users do not run into the overwhelming majority of problems, bugs and un-features I am annoyed at. But given the severity of some of them, I think that \"finality\" should really start AGAIN to mean something."
    author: "slacker"
  - subject: "Re: First beta?"
    date: 2009-01-06
    body: "And did I say something different? I said I am looking forward to 4.2 being the first BETA quality release - there is still some distance to cover to stable :)."
    author: "slacker"
  - subject: "Re: First beta?"
    date: 2009-01-06
    body: "define beta and stable, please"
    author: "hias"
  - subject: "Re: First beta?"
    date: 2009-01-07
    body: "Maybe I'll start from the end :).\n\n\"stable\":\n1. Feature-complete - usable, no missing (but advertised) features, should not feel severely inferior in features when compared to previous stable versions.\n2. Stable (as the name suggests ;)). Hangs or crashes very rare, rare enough not to annoy the users too much ;). OK to use in production environment.\n\n\"beta\":\n1. Reasonably feature-complete - no major features lacking to the following stable release (some may be unpolished, but should be usable). May have some minor features lacking or not usable (yet).\n2. Reasonably stable - may hang or crash from time to time, or have many annoying bugs - but should be OK for daily use by people not afraid to run into some occasional (and sometimes often) breakage (and then report it - after all this is what beta releases are for!). If an application crashes or corrupts data or screws up things in some other way often enough to make it practically unusable, it is NOT beta quality yet.\n3. Expect many bugs - this goes without saying about betas ;)\n\n\"alpha\":\nDevelopment build not yet meeting the above criteria (but showing progress towards them)\n\nNow while KDE 4.0 did meet beta stability criterion, and KDE 4.1 got stable enough for a final release, they both don't meet the feature criterion. While the libraries may be fairly complete, the desktop itself still has many issues, and, well, it feels IMHO simply primitive and poor when compared to KDE 3.5. Some flashy visual effects won't hide lack of features. There are also many other, quite major issues like the obnoxious \"no root mode in system settings\" problem. And many annoying bugs also. I did not try out KDE 4.2 betas myself, so I don't have any basis to talk about them - though I do have high hopes for 4.2.\n\nAll in all, I very much like the progress I am seeing with KDE4. It is definitely going the right direction, and at an amazing pace. Still, the road in that direction is long (rewriting the entire desktop from scratch is no easy task). I very much hope that KDE 4.2 will achieve beta quality, and become a viable (though not nearly on par in features and usability) alternative to 3.5. While I don't think that by 4.3 it will be mature enough to be called \"stable\", I do have hopes that it will be enough for me to switch to it. Till that time, I'll keep my marvelous KDE 3.5 as my primary desktop :).\n\nAnd no, I don't remember any Micro$oft software deserving to be called \"stable\"."
    author: "slacker"
  - subject: "Re: First beta?"
    date: 2009-01-05
    body: "\"In other words - you're claiming that on Jan 27 we'll have the first beta-quality KDE4 release?\"\n\nBeta quality according to whose criteria? Alas, you don't get to set those criteria yourself. The developers do."
    author: "Segedunum"
  - subject: "Re: First beta?"
    date: 2009-01-06
    body: "Since when did beta mean stable?  Heck a .0 *final* release is rarely stable in any world."
    author: "T. J. Brumfield"
  - subject: "Huh since when is a stable release a beta or alpha"
    date: 2009-01-05
    body: "Only thing i would like to say, KDE4.2 is ready for it, applications are shifting nice up, and its getting really stable, yeah maby some problems, but i am sure they will get fixed.\n\nAnd some problems are distro related, yeah Kubuntu please do a good job on 9.04 and fix the Intel GMA bug please, that will make KDE4 run even better.\n\nOh i hope Jos or someone else will organise a party for Belgium, Netherland, i am from The Netherland but i spend much time in Belgium."
    author: "Wesley Velroij"
  - subject: "My $0.02"
    date: 2009-01-05
    body: "I'm still waiting for KWin with effects not to be as slow as molasses.  I'm also waiting for Oxygen to de-uglify.  They have made strides in the right direction, but menus are still terrible with too much extra space and strange coloration.  I guess I can use QtCurve in the meantime (after all, who still uses Plastik on 3.5?).  Also, console is missing some features, like the ability to set the size of the window to, say, 80x24 and also have it stay.  It's just completely gone that option.  Kinda Gnome-like thinking if you ask me."
    author: "Anonymous Asshole"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "Konsole resizing was broken a bit for me after updating to KDE 4.1.2, but the ability to save window size was fixed in KDE 4.1.3. In short, the konsole issue was a bug, not a deliberate removal of a feature. :)"
    author: "Jonathan Thomas"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "I don't know what type of hardware/video card you are using but I am using an Intel GMA 3100\non the desktop and a GMA 855 on the laptop and I haven't seen any sign of slowness. The only problem that I have is that the effects (not KWin) crash without a warning sometimes.\n\nConcerning the Oxygen Theme, yes there is room for improvement but in all honesty, I haven't seen a better KDE 4 Theme until now - not even Bespin, but maybe I have a weird taste like I have already pointed out."
    author: "Bobby"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "I have an ATI Radeon X300, which works fine with any compositing manager except KWin's.  And not just fine, I mean really good, flawless.  On my Gentoo install, I can use kompmgr and the performance is almost on par with what you get with no compositing manager, yet I get the effects with very little CPU usage.  On Ubuntu, compiz works great and is about as fast as Ubuntu will let it be (given that Ubuntu apparently has applied CFLAGS or something that cause GUI performance to be slower -- I actually measured with gtkperf and x11perf and the performance is notably slower than what I have on Gentoo).  Yet KWin4 with effects in enabled is a bear.  Moving windows around is extremely choppy.  Shadows don't quite work.  It crashes.  It's a terrible experience.  Apparently nobody else has my video card because it hasn't been fixed in the months I've been testing it.  I guess I should file a bug report, but honestly, I don't see why it shouldn't work given that compiz and kompmgr work just fine on the same machine with the same settings."
    author: "Anonymous Asshole"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "> I guess I should file a bug report, but honestly, I don't see why it shouldn't work given that compiz and kompmgr work just fine on the same machine with the same settings.\n\nPlease file a bug report if something is broken! :-) The story at http://dot.kde.org/1225989053/1226032356/1226052540/1226061674/1226062853/ tells us the following funny anecdote:\n\n\"\"\nI'm reminded of the time when everyone was complaining about the fact that Digg was clearly broken in Khtml, and no one actually bothered to report it because it was so \"obviously\" broken. What they didn't realise, of course, is that none of the Khtml devs (quite sensibly) ever visit Digg! :)\n\"\""
    author: "Diederik van der Boor"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "I've filed half a dozen bug reports relating to rendering or effects in KDE 4.x, and every one has been closed as \"WONTFIX\", \"WORKSFOR ME\" or \"INVALID\". The typical reasons given were \"it's not our bug\" and \"it must be your video card\"."
    author: "David Johnson"
  - subject: "Re: My $0.02"
    date: 2009-01-06
    body: "Could even be that the developers are right in those cases :-)"
    author: "Morty"
  - subject: "Re: My $0.02"
    date: 2009-01-06
    body: "Regardless of whose fault it is, a bug is still a bug. Especially when the bug ONLY happens in KDE4. KDE developers can't fix X.org bugs, but they should do more than just close the bug. Perhaps offer a workaround? Perhaps forward the information off to the project in question? Perhaps direct the user to the other bug database so they can vote on it?"
    author: "David Johnson"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "> Apparently nobody else has my video card because it hasn't been fixed in the months I've been testing it\n\nOr they do, but their configuration works fine. Please understand the issue could be caused by several layers of the software stack, up to xorg.conf settings or (in case of Gentoo) different CFLAGS."
    author: "Diederik van der Boor"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "I have an ATI Radeon X300 RV370 card and I do not have the same issues you state here. Of course this particular card absolutely sucks! Specially if you have the SE (Standard Edition) version. Regardless, you did not mention whether or not you are using the ATI binary drivers (fglrx) or the open source ones (radeon) so no one can help you out. \n\nPerhaps mentioning the bug report number you opened might prompt others who have the same card as you and encountered the same problems to help you out ???"
    author: "DA"
  - subject: "Re: My $0.02"
    date: 2009-01-06
    body: "I have the same card and my mistake was to try to get the closed-source fglrx driver to work properly with KDE4. I tried all xorg.conf options to no avail. Choppy movement of windows etc. Very bad. Since I've switched to the open source \"radeon\" driver I haven't had any problems except some kind of video garbage showing for an instant when a new window or menu was about to appear which was supposedly caused due to broken Ubuntu xorg packages. Since I've upgraded those packages the problem went away (Google for \"video garbage kde4\").\n"
    author: "Michael"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "Yeah, I made a comment about sluggishness of Kwin before. However, I now realise it's plasma - I think... If I move a window around with another window in the background it's fast and responsive. If it touches plasma, it sloooooooes downs. Maybe we need a way to turn off plasma or have a very light, no shadow or transparency theme for plasma. That way those with black listed VGA cards or slow ones can still use KDE4.\n\nWould that fix it? If so, I may just go and make one. What do you guys think?\n\nOh and anyone want to part in Sapporo? I'm game. If we could get dragon suits from &#12489;&#12531;&#12461;&#12507;&#12483;&#12486;&#12451; we could ski in those! Yay! The freaks!"
    author: "winter"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "Son, if the devs don't know where you problem is then they can't fix it. I am 101.1% sure that it's not Plasma because Plasma is one of the most stable parts of KDE 4 in the meantime. Maybe it's Xorg, or nVidia or badly compiled packages, wrong/bad settings. It can be a 1000 things but I doubt that it's KWin or Plasma."
    author: "Bobby"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "I am about 72% sure you are a fool, about 96% sure you are somewhat litterate, and about 65% sure your reading comprehension was not doing so well when you posted that or you didn't read my post... Father Bob."
    author: "winter"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "You could be 0.1% right son."
    author: "Bobby"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "\"I now realise it's plasma\"\n\nPossibly, but I'm doubtful.\n\nThe most likely reason is that the graphics driver sucks at painting the sorts of things Plasma draws. (Yes, vague and non-technical description ...)\n\nA less likely reason is that kwin is painting the desktop too much (turn on Kwin's repaint plugin to see).\n\nA less likely reason is that Plasma is painting the whole desktop constantly on window changes (QT_FLUSH_PAINT=1 plasma)\n\nIt could also be that something is happening like the pixmap cache is being overrun and Plasma is re-rendering the background constantly instead of using a cached copy.\n\nOh, if the behaviour only shows with desktop effects on, then it's not the last two and less ikely to be the lsecond one."
    author: "Aaron Seigo"
  - subject: "Re: My $0.02"
    date: 2009-01-06
    body: "I can't turn on desktop effects on this particular notebook. I think graphics chip is blacklisted. Reason I think so is, I can play OpenGL games. DRI = yes. But, Kwin says:\n\"Failed to activate desktop effects using the given configuration options. Settings will be reverted...\"\n\nI know there is probably no acceleration going on this machine for Kwin. So drawing simple things like shadows and transparency is probably quite heavy for Plasma. This is just my guess. It just seemed to make sense because there is no lag when I move around a window in front of a maximized window.\n\nBy the way, I don't think it is a bug or unstable, as Bobby said. It's very stable and hasn't crashed on this machine since BETA1. It's very nice. I think it's just that it needs acceleration. So that's why I thought if I didn't have to load Plasma or make a theme that doesn't need any acceleration, it would be more fast and smooth. I am in a rather fortunate position. Rather than fix the driver for this card, perhaps Plasma or Kwin or whatever it is, should be able to work with no DRI. Linux doesn't always get great hardware vendor support. So maybe the software could be flexible enough to handle these bad situations."
    author: "winter"
  - subject: "Re: My $0.02"
    date: 2009-01-06
    body: "it's really not about the plasma theme, but about plasma using what's called an argb visual. you could hack the source (plasmapp.cpp in particular) and remove that bit and see if it helps you any.\n\n\"Rather than fix the driver for this card, perhaps Plasma or Kwin or whatever it is, should be able to work with no DRI.\"\n\nit does work. but if you have craptastic drivers you're going to have poor results; the option is to do what we've done the last 10 years and be boring, limited and do nothing special because *other people's* code isn't great.\n\nthere is hardware and there are drivers that work well, and they are getting better specifically because we are pushing on them.\n\nand seriously, unless we want to repeat the wonderful strategy of CDE with all its glorious results, it's critical to keep current and pushing forward. i happen to want more than 100 people using our stuff in 10 years. =)\n\n\"Linux doesn't always get great hardware vendor support.\"\n\nwhich is why if you're using Linux you need to be picky about the hardware you purchase. =)\n\n\"So maybe the software could be flexible enough to handle these bad situations.\"\n\nit works and looks ok while doing so; that was a lot of work in and of itself. i often find myself wishing we had the luxuries of Apple and could just say, \"screw the crap, we're only using *this* hardware because it actually works.\" oh well, reality is more complicated than that."
    author: "Aaron Seigo"
  - subject: "Re: My $0.02"
    date: 2009-01-06
    body: "I had the exact same problem. Everything in trunk worked fine one day, and then the next I would get that \"failed to activate\" message. Turned out that I need to tweak my xorg.conf yet again, this time with an undocumented setting. It feels like I'm back in 1996 trying to calculate modelines based on conflicting information found on usenet. No, it's not KDE's fault, but it doesn't lessen the frustration any."
    author: "David Johnson"
  - subject: "Re: My $0.02"
    date: 2009-01-07
    body: "Aaron, I agree. If it's as you say, then so be it. I would not want great development hindered because someone blacklisted my functioning opensource driver. (Just a guess as I heard others saying the same thing.) I did purchase my hardware for Linux over 3 years ago. I made sure I had a graphics chip with dedicated RAM and the opensource ATI driver works well with Xorg - just not Kwin.\n\nDavid, can you share some insight on what you did to get it to work? I felt very much the same way - 1996; Redhat; modules; \"Look Mom, windows and mouse and sound! Printing!\" I was actually kind of surprised that the settings in xorg.conf were so sparse. I hadn't looked at it in ages. Everything was detected automatically. So I really have nowhere to start. :/"
    author: "frozen"
  - subject: "Re: My $0.02"
    date: 2009-01-07
    body: "Posting from a work computer. This computer needs a cookie reset. Post above name = winter"
    author: "winter"
  - subject: "Re: My $0.02"
    date: 2009-01-05
    body: "Anonymous wrote: \"after all, who still uses Plastik on 3.5\".\n\nI do! If I wanted a shiny desktop I would Armor-All(tm) my monitor!"
    author: "David Johnson"
  - subject: "Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "\n\nOkay,\n\nI have really enough of KDE4. \n\nHere and now I announce a fork of KDE3 to get back the further development of\na desktop environment which was before the big stop of the development of this branch a near perfect DE for GNU/Linux.\n\nThere are many people who have the same opinion. \n\nI want back the further perfectisation of my favourite DE. \nAnd for this I will act against the current KDE project - because THEY are\ncausing me do this -.\n\nYou can make parties all days long. We will see, which branch will end more \nlikened.\n\nOkay? Thanks.\n\n"
    author: "Jayy the Gayy"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "it's a free world ... might be interesting to see how this plays out though i think you might be more blaaaaaaaaa than action. either way, it might be challenging for you to keep up with the pace we see in kde4 but then again let's see how it works out. also, maybe some people agree with you but i kind of doubt they will all be active coders but hey ... why not.\n\ni personally like kde4 very much and follow its development rather closely; kde4.2 will definitely be a great step forward so congratulations to all developers! "
    author: "andrea"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "oh yeah, please do it and also please make a dot.kde3.org and a commit-diggest3.org and take slacker and kubunter with you. there would be so many people appreciating it."
    author: "hias"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "LOL, I think he deserves a little winding up doesn't he? Where is TJ? I guess TJ would join him too.\nI can imagine him coming back in 6 months and asking for a Qt3 fork, which of course he can also have."
    author: "Bobby"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-06
    body: "I appreciate the slander, but I've never called for a fork.\n\nI wouldn't mind seeing people continue to polish off KDE 3, apply bug fixes, etc.  Maybe even apply various patches in BKO that still aren't committed for some crazy reason.\n\nIf someone wants to do that, great.  However, I don't expect anyone to seriously develop or maintain a fork.  Nor would I ever suggest it.  It isn't realistic or beneficial.\n\nI really don't care for this black-and-white divided nature here on the Dot.  Either you blindly praise all aspects of KDE as being perfect, or if you criticize anything you are a cancer and a troll.\n\nThe developers don't have to listen to me.  They don't have to cater to me.  I've never suggested they have to.  But I am free to express my opinions and state that I don't like things.  I tried the latest 4.2 snapshot release on openSUSE 11.1, and I still can't stand using a KDE 4 desktop.  Bugs and issues that have been reported since the 4.0 betas (constant plasma crashes, the panel jumbling the layout) still seem to be prevalent.\n\nAt every turn I look for something to discover what I want just isn't there.  I see forward progress on the KDE 4 front, but I just can't bring myself to use it.  Software should enable me as a user, not stymie me.  I am able to do everything I want in an expedient manner with KDE 3.  I'm grateful the various KDE devs over the years freely contributed their time to make a product I enjoy so much.\n\nBut the moment I load a KDE 4 app, I find my entire desktop noticeably slowed, even with the latest Nvidia drivers, and even with composite effects turned off.  The moment any KDE 4 app loads on my wife's laptop, her webcam stops working.  It is a known issue with a bug reported on Novell's website since the 11.0 launch, but it still isn't clear if it is a distro problem, or an upstream problem, or if it will be resolved.\n\nI really loved many of the early Oxygen concepts and mock-ups, but the final product really leaves me wanting.\n\nMany of the apps themselves are just now starting to come to speed (like Ark).  Again, I see progress, but there is plenty of core functionality that has been missing this past year."
    author: "T. J. Brumfield"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-06
    body: "> if you criticize anything you are a cancer and a troll.\n\nno, if you are a cancer and a troll you are a cancer and a troll. saying things like \"but it's just how i feel\" doesn't justify half the stupid things i've read in the last 18 months. i really don't remember what you yourself have written/said (couldn't be bothered to keep a running tally on every person =), so you could be completely blameless in this regard. but yeah .. it's unfortunate because people who are complete dicks ruin it for pretty much everyone at some point.\n\n> panel jumbling the layout\n\nseveral different causes of that were caught and fixed by this point.\n\n> constant plasma crashes\n\nif you are experiencing constant plasma crashes, you either have a crap built of Qt (there are several crash fixes in qt-copy) or a jumble of crud from previous installs.\n\n> I find my entire desktop noticeably slowed\n\nyour *entire* desktop? if so, then something is using resources. cpu, ram or both.\n\n> The moment any KDE 4 app loads on my wife's laptop, her webcam stops working.\n\ncould be whatever backend Phonon is activating on that system?\n\n> but there is plenty of core functionality that has been missing this past year.\n\nand i'm sure that people such as yourself will be here to remind us of that for the next year, as well. makes, how did you put it, \"freely contribut[ing our] time to make a product [you] enjoy so much\" feel a little less enjoyable. kde 3.5 wasn't born in a day, and neither will kde4. being pestered during the entire development process, when none of us are taking away your beloved 3.5, is a bit of a PITA."
    author: "Aaron Seigo"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-06
    body: "Soory Aron, but my webb-camera (Logitedh Quick 9000 pro fusion) work NOT too ...like T.J's wife... \nvideo and video= findf NOT from /dev-directory... (only maybe 1 from 20 try) \nit means, I can't phone videophone with Skype... \n\nOpenSuse 11 and OpenSuse 11.1 failed both! \n"
    author: "Jake"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-06
    body: "So they do work in for example ICEWM and Gnome and not in KDE 4.1.x?\n\nThat must be phonon. See if starting KDE apps disables the thing. See if starting a KDE video or audio app disables it. In other words, figure out WHAT exactly causes it, then post a bugreport so the people at Qt software & KDE can fix it in time for 4.2..."
    author: "jospoortvliet"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "I proudly use Kubuntu on my notebooks. I went from Gentoo to Kubuntu. Oh what? You want me to use SuSE? It was a good distro when it was a German distro. It was the only distro I ever bought. But now the MS loving Novell has it. Oh woe is me. So Kubuntu it is. And it's great. It works fine. It's never been better.\n\nKubutnu is a distro built around a KDE desktop that uses Debian packages and package system. Is there an alternative? If there is, I'd like to hear of it. Otherwise, talk to the hand."
    author: "winter"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "I didn't say kubuntu, but kubunter. if you read the dot the last weeks, you would have noticed that he is one of the people who constantly bash about kde and dannys commit diggest.\n\nbtw, I use kubuntu too, because of aptitude and being able to upgrade without to reinstall (from dapper to intrepid and I made each upgrade) but I'm running trunk and one reason is the messed up localization of intrepid because they use this *piep* rosetta and overwriting a 100% german translation from upstream with a mixture of german and english (try to convince somebody to move from windows to linux if it doesn't even have a consistent language within the applications). a few weeks ago I read on planet.ubuntu.com that the rosetta team gave upstream translations a higher priority so I hope that it's getting better in jaunty."
    author: "hias"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-06
    body: "I see. Sorry. I thought that was jibe directed towards Kubuntu and it's users. I just wanted to defend it. It does get quite a bit of flack. I haven't read from a user named kubunter. So my bad. Yes, Kubuntu is cool. ;)"
    author: "winter"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "If that's your only reason for not using openSuse then that's a really weak one because openSuse doesn't belong to Novell. Suse Linux and openSuse are not the same. Get the facts and come again. \nI don't have a problem with you using KUbuntu because Linux is Linux in my eyes but I don't condone people putting down other Linux distros for no rational reasons at all"
    author: "Bobby"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-06
    body: "Whilst I use opensuse, I have in the past used Kubuntu. Suse's kde packages rock, but I cannot say the same about the rest,mostly lower level stuff. In my experience debian based systems just work better and faster. With opensuse I just can't get it going as nicely and I always have to fix something when I install fresh (or upgrade)."
    author: "txf"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-06
    body: "Either large ego or perhaps cognitive abilities should be examined."
    author: "winter"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "too many happy new year drinks? I'm kidding but I think that you're not understand very well what's happening around you...\nHappy new year"
    author: "joethefox"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "So... Got a source repository already? Show us some activity, baby!"
    author: "Boudewijn Rempt"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "After you have used the 4.2 final/trunk, the 3.5 series feels soo old generation."
    author: "slashdevdsp"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "Good luck porting all KDE3 to Qt4 you and your alter egos alone.\n\nIf what you want is continue the maintenance of the KDE3 branch in bugfix mode only, you will be more than welcome to do so in the KDE repository, because some distros will be packaging it, and some users will still use applications from there."
    author: "Anon"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "You're welcome to KDE3 and your forks, personally I adore KDE 4.2 beta2. Having taken the time to actually USE it, - it would be very hard to go back to 3.5.x now. \n\nLets see how much action there is behind your big words. I think none. "
    author: "Maarte"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "\"We will see, which branch will end more likened.\"\n\nI'm sure the \"Jayy the Gayy\" branch of KDE3 will be very popular."
    author: "Anon"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "You can probably even continue to develop kde3 in the normal kde svn repertory (there is no feature freeze for kde3 in svn, is there?)\n\nSo, just apply for a svn account and good luck :-) (i will be using kde4, but everybody should be able to develop whatever he wants)"
    author: "Beat Wolf"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "You are free liike a bird in a tree. This is open source man so you can fetch the codes and have a lot of fun ;) As for me, I am too old and lazy for that type of adventure so I will continue to use KDE 4.whatever until all the KDE 3 apps are ported."
    author: "Bobby"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "Troll, oh, Troll, why is it your desktop has a kidney on it?\n\nYou are outperforming. I feel sorry for the real trolls you have now put into the shadow and stealing all their bread."
    author: "Hans"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "You should really do this as a separate dot article, though you'll need to give a few more details, such as whether you are going to use a branch or an external repository, etc.\n\nIf you are using a different infrastructure you might want to coordinate with the KDE infrastructure sysadmins so you can get all KDE3 related bugs and wishes exported from bugs.kde.org and imported into your bug tracker.\n\nAdditionally provide information about your developer mailinglists, so that current KDE developers can subscribe and provide technical help. After all they are the ones with best knowledge of the KDE3 code base.\n"
    author: "Kevin Krammer"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "Desktop Environments stopped striving for consistency, ease-of-use and perfection when NeXTStep/Openstep stopped being released.\n\nYou want perfection? Drum up a few million and hire Keith Ohlfs to build a team to do it."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "Or hope for \u00c9toil\u00e9 -> http://etoileos.com/"
    author: "Hans"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "I think you're really loosing people of the same opinion, no offense.\n\nI liked KDE 3.5 too, but I - for myself - do not look back at all. KDE4.2svn is stable, fast and highly usable. \n\ncheers"
    author: "Neil"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "I had my problems with the release policy of the 4.x series. Well, that's history, I don't feel any need to discuss that any longer and it's time to get back to normal business. 4.2 will be a great release IMHO.\n\nTherefore, being one of the critics, I feel a strong urge to distance myself from such childish announcements of a KDE fork.\n\nPlease, when I read comments like this, but alos Blog entries like Mark Kretschmanns from today:\n\nhttp://amarok.kde.org/blog/archives/862-KDE-Trolls,-eat-this.html\n\nI ask myself: Do we really need another round of aggressive flame wars? And why now? The debate about the release policy is over and I thought things were cooling down?!\n\n"
    author: "furanku"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "you loose\nkde 4 rocks\n"
    author: "---"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "Will you create a website? A foundation? Will you maintain Qt3 for us? Marvellous!\n\nNow, how about a nice, hot, steaming cup of reality?"
    author: "Alejandro Nova"
  - subject: "Re: Announcing a new development line of KDE3"
    date: 2009-01-05
    body: "Nice, finally somebody who steps up and puts all his power into maintaining and bugfixing the stable KDE 3.5.x series :-)\n\nYou're welcome.\n\nAlex\n"
    author: "Alex"
  - subject: "4.1.3 User Survey..."
    date: 2009-01-05
    body: "My small non-representative, non-technical audience survey has achieved a 100% \"gosh we love it, can we have some...\",  I've never heard anyone say that about any other Linux based GUI, all of which have been admired for their competence.\n\nFor the wider \"we want our stuff to say something about us\", KDE 4.1+ is a total winner. I'm holding them off until 4.2, but I've got a small queue of people waiting for it, who are now receptive to the wider Free Software arguments. \n\nThis is excluding the Mac user, who remembers Linux when she was at University 8ish years ago... :)\n\nYes it's had a few problems, but the latest openSUSE RPMs on a recent \u00a324 6200 and the beta 180.17, sing on otherwise 5 year old hardware.\n\nAnd the top left corner of Plasma is really useful for a disorganised, \"where's that bl..dy app?\" person like me.\n\nIf someone wants to take 3.5 further, well that's the other beauty of Free Software, and I will be grateful if they achieve something.  The Bazaar has always achieved better outcomes than the Cathedral as several Cathedral builders have demonstrated by changing their attitude."
    author: "Gerry"
  - subject: "Nice one Aaron"
    date: 2009-01-05
    body: "\"Aaron Seigo will give a talk, food and drinks afterwards.\"\n\nWhat are you doing in Switzerland before you give a talk, food and drink afterwards?  Thats very generous of you being a speaker, waiter and barman at the same event.  Value for money.... :o)\n\n"
    author: "Ian"
  - subject: "Re: Nice one Aaron"
    date: 2009-01-05
    body: "LOL, it really reads like this! talk, food and drinks! But even without that I'd really love to come!\n\nCheers!"
    author: "Neil"
  - subject: "Re: Nice one Aaron"
    date: 2009-01-05
    body: "Speaking at the Student Summit for Sustainability.\n\nand yes, it does sound like i'm cooking for a lot of people there doesn't it. hm. maybe i should bring my pots and pans."
    author: "Aaron Seigo"
  - subject: "Kde 4.2.1 and above"
    date: 2009-01-06
    body: "My experience with KDE 4.2 beta is very positive, it shows a lot of polish and subtle details all of which make the Linux Desktop a serious contender when it comes to look and feel.  \n\nFor my use (and my wifes), I hope KDE 4.2.x will focus on speed improvements, especially in booting up. \n\nThere is one other area I would like to see improved upon in future releases of KDE and that is for use on small low resolution screens.  I put KDE 4.2 Beta on my Lenova Netbook with a 10\" screen, and it is rather difficult to use as many of the dialogs are to big to fit on the screen.  There seems to be a hardcoded values for spacing and margins that are not appropriate for small screens.\n\nAll in all I really like KDE 4 and look forward to 4.2's release.\n\n\n\n\n\n  "
    author: "David "
  - subject: "Re: Kde 4.2.1 and above"
    date: 2009-01-06
    body: "have you posted screenshots? and submitted bugs for those dialogs?"
    author: "slashdevdsp"
  - subject: "Re: Kde 4.2.1 and above"
    date: 2009-01-06
    body: "Either way, small screens have been a focus for this release and there have been several improvements in this area."
    author: "jospoortvliet"
  - subject: "freetype for hinted fonts?"
    date: 2009-01-06
    body: "Will this release include detection (and utilisation) of freetype with font hinting (if enabled?)"
    author: "RussH"
  - subject: "Re: freetype for hinted fonts?"
    date: 2009-01-06
    body: "You'll have to wait for KDE 2.1 for that."
    author: "Boudewijn Rempt"
  - subject: "Re: freetype for hinted fonts?"
    date: 2009-01-06
    body: "LOL\n\nAnother question then. Does it detect mindreaders yet? I've bought one, and Vista worked out of the box with it! It detected my hamster only thinks about running, food and sleeping."
    author: "jospoortvliet"
  - subject: "Re: freetype for hinted fonts?"
    date: 2009-01-07
    body: "When KDE4 looks kick-ass we can all praise it for being the sexiest desktop on the planet. Right now it's doing allot of re-engineering and that takes (much appreciated) effort. If we simple desktop users can contribute even if it's only by posting feature requests etc, then surely it needs to be welcomed - if the KDE4 project is to move forward?\n"
    author: "RussH"
  - subject: "Re: freetype for hinted fonts?"
    date: 2009-01-07
    body: "I'm not entirely sure, but I guess the poster before me wanted to say that it was there since KDE 2.1.\nif you mean the Systemsettings->Appearance->Fonts->Use anti-aliasing ... Configure... ->Hinting style feature, then it's there at least since KDE 3.4"
    author: "hias"
  - subject: "Re: freetype for hinted fonts?"
    date: 2009-01-07
    body: "May be the original poster is talking about QT4 (prior to 4.5) not using the proper hinting style (so the fonts looks not so go, or different compared to KDE3)\n.\nAs you can see in this article ( http://labs.trolltech.com/blogs/2008/09/01/subpixel-antialiasing-on-x11/ ) this was improved for Qt4.5 :)"
    author: "Dar\u00edo Andr\u00e9s"
  - subject: "Release Event at Fosdem"
    date: 2009-01-07
    body: "IS there release event for 4.2 planned at FOSDEM 2009 ?"
    author: "pvandewyngaerde"
  - subject: "Will google host another release party this year?"
    date: 2009-01-08
    body: "That would be really cool.\n\nEspecially with a video/podcast keynote we can all watch and blog about.\n\nThis created a firestorm of blog posts last year, should be even better this year.\n\n"
    author: "Mike"
  - subject: "Don't count your checkins before they've crashed"
    date: 2009-01-08
    body: "As they don't say in the commercial airline business, all this alleged, archived and arguably anachronistic evangelism might scare some people. However, using a flaky v4.1.3 (and having just discovered I can now more easily and more accurately record my desktop than I ever previously could), I've not so very much to grumble about.\n\nIt's seemed a long time coming, maybe, but only because ability to contribute and learn to code at the same time has been more uphill-struggle than easy-does-it. The inertia of KDE helps, but between making excuses and being distracted I don't feel well-positioned to contribute. Maybe when stabler things (4.2/3)are further downstream (?) in the distributions and I set up focused ISO files can be laid out really easily with KDE4, it's sources and a development-oriented setup, then perhaps I can just roll into the sandbox with and start burying kippers.\n\nDon't mind me."
    author: "Andy \"And Holy War\" Warhol"
  - subject: "Beta Quality?"
    date: 2009-01-09
    body: "http://www.englishbreakfastnetwork.org/krazy/?component=kde-4.x\n\n21002 issues!\n\nAnd these are not even the real bugs as e.g.\nhttps://bugs.kde.org/show_bug.cgi?id=162485"
    author: "Andre"
  - subject: "Re: Beta Quality?"
    date: 2009-01-09
    body: "\"And these are not even the real bugs as e.g.\"\n\nYes, they are no bugs. They are in many cases just warnings regarding the style, i.e. if there is code which might potentially lead to problems:\nhttp://www.englishbreakfastnetwork.org/krazy/reports/kde-4.x/kdelibs/kdecore/index.html\n\nAlex\n"
    author: "Alex"
  - subject: "Re: Beta Quality?"
    date: 2009-01-09
    body: "Yes, trivial warnings, matters of policy, easy to fix. But 22000? In other words there is a severe lack of maintenance and capacity. And then I come to \"real bugs\" that are hard to fix as compared to the krazy warnings and my example was one that makes Konqueror unusable for most of us. Despite that according to Murphy's law things go wrong that can go wrong.\n\nBut sure you all can say KDE 4.2 is \"ready\", just a bit more. Nobody told users that it would take years to complete the KDE4 transition, this was not how it was planned. Neither KDE3 nor KDE4 are fully stable by 2009 standards. They still tend to break and this is because of unresolved bugs and those small quality problems.\n\nWill there be a Krazy sprint before the release or will KDE4.2 released with all these unresolved issues? Not to mention incomplete translations even for major languages?"
    author: "Andre"
  - subject: "Re: Beta Quality?"
    date: 2009-01-22
    body: "Years?\n\nAs in... plural?"
    author: "Anonymous"
  - subject: "KDE and SSL/TLS - still no revocation support?"
    date: 2009-01-12
    body: "Does the KDE4 crypto setup support revocation list handling yet, or OCSP? It's quite difficult to take KDE seriously, as long as this is not in place - it ruins the the security model that SSL/TLS offers, when you leave out such vital functionality. For what it's worth, it's not there in KDE3 either."
    author: "Kolla"
---
On January 27, a year after the release of the first KDE 4 version, KDE 4.2 "The Answer" will be released. This release will feature stabilisation and feature completion and is likely to be taken up by a wide audience of users.  To celebrate the important event in KDE's history with our fans all around the world we would like to invite our community members to organise a release party. It is all up to the organising teams to make it just the way you like it. There could be presentations, workshops, maybe some translation marathons, a hacking contest, just some socialising fun with a drink or two. It can be short and sweet or a whole-day event. Do not be afraid to invite some local press to the event to get the word about KDE out there into the wide world.  If you decide to throw a party, or know about one, do add it to the <a href="http://wiki.kde.org/tiki-index.php?page=KDE%204.2%20Release%20Party">list of party locations</a>.
<!--break-->
