---
title: "NLnet Foundation Sponsors Lokalize"
date:    2009-01-30
authors:
  - "jriddell"
slug:    nlnet-foundation-sponsors-lokalize
comments:
  - subject: "Congrats and thanks"
    date: 2009-01-31
    body: "Congratulations on getting the fundings to further develop Lokalize. It already is a very nice translation tool and it is nice to see it will get help for further improvements. Thank you Nick! BTW, do I see in the screenshot a new way of adding the tags just by clicking the button to insert them into the translation? It will make this quite a bit more discoverable feature as Ctrl+T was quite hidden and it was easy to overlook."
    author: "JLP"
  - subject: "Yes"
    date: 2009-02-01
    body: "Yes, I thought about it too, and included this in my ToDo list (I even know already how to implement this)"
    author: "shaforostoff"
---
<p>
<a href="http://nlnet.nl/">The Dutch NLnet Foundation</a>, aiming to stimulate open network research and development and more general to promote the exchange of electronic information, has decided to financially support the <a href="http://userbase.kde.org/Lokalize">Lokalize</a> project of KDE.</p>

<p>Previously NLnet, alongside sponsoring of a number KDE projects and activities, <a href="http://mail.kde.org/pipermail/dot-stories/2008-April/001357.html">helped to develop ODF support in KOffice</a>.  This sponsorship is to support another open standard, <a  href="http://en.wikipedia.org/wiki/XLIFF">XLIFF</a>, in Lokalize.</p>
<!--break-->
<a href="http://static.kdenews.org/jr/lokalize-xliff-inline-markup.png"><img src="http://static.kdenews.org/jr/lokalize-xliff-inline-markup-wee.png" width="300" height="240" alt="" style="border: thin solid grey; padding: 1ex; margin: 1ex; float: right" /></a>
<p>
During the KDE 4.3 development cycle, Nick Shaforostoff, the original author and maintainer of Lokalize will work on XLIFF editing support and integrating <a href="http://translate.sourceforge.net/">Translate-Toolkit</a>'s odf2xliff (also funded by NLNet) and also other converters to make Lokalize a general user-friendly translation tool for freelance translators.
</p>

<p>Nick has already implemented basic open/save functionality as well as displaying and editing in-line mark-up.  You can track the progress of the project via <a href="http://shaforostoff.blogspot.com/">Nick's blog</a> (aggregated on <a href="http://planetkde.org">PlanetKDE</a>).