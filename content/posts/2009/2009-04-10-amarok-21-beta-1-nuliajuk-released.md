---
title: "Amarok 2.1 Beta 1 \"Nuliajuk\" Released"
date:    2009-04-10
authors:
  - "nightrose"
slug:    amarok-21-beta-1-nuliajuk-released
comments:
  - subject: "This seems to be the SVN"
    date: 2009-04-10
    body: "This seems to be the SVN version I'm currently using, because a svn up didn't change anything. I was really hoping that podcasts would be fixed, but they aren't. Amarok just forgets that it has *any* podcast episode downloaded so you *always* stream them. Is it really that hard to fix this bug? I'm certain I did report this bug a long time ago, but I can't find my bugreport on bugs.kde.org right now (however, I never find anything there)."
    author: "panzi"
  - subject: "podcast migration tool?"
    date: 2009-04-11
    body: "the reason why i havent moved to amark2 is because i am too lazy to hunt down all my podcast links all over again ..is there any way of migrating my podcast list to amarok2?"
    author: "mtz"
  - subject: "Dockable widgets ?"
    date: 2009-04-12
    body: "I wave a great wish:\r\nWould it be possible to make all the GUI parts dockable, so that we could drag&drop them just where we want them to be (like eclipse does) ?\r\n\r\nthat way we could:\r\n- put the play/stop buttons back under the playlist.\r\n- remove the context view which IMHO is NOT the most important part of a music player.\r\n- just customize it the way we use Amarok, everyone wants it different, why did you freeze the layout ?!\r\n\r\nWicked would be to be able to save/download the layouts (or views or whatever it should be called).\r\n\r\nThanks for your great work."
    author: "lbayle"
  - subject: "Podcast fixes"
    date: 2009-04-12
    body: "That should be fixed now. Update your SVN and try it out."
    author: "Stecchino"
  - subject: "Not yet, but that would"
    date: 2009-04-12
    body: "Not yet, but that would indeed be useful to have.\r\nIt should be implemented in script since that will allow people on the stable branch of Amarok 2 to also migrate their subscriptions.\r\nI'm hoping someone will write this. My lack of time is the reason why the podcasting is in the sorry state it is."
    author: "Stecchino"
  - subject: "I really like about amarok"
    date: 2009-04-13
    body: "I really like about amarok that the developers usually react quickly to reported bugs. However, I DID update and it didn't fix anything for me (using revision 953166). However, just for debuggings shake I'll redownload one of the podcast episodes and see if the problem only exists with episodes that where downloaded before the new revision. This will take quite some while, because the podcast episodes of the feeds I've subscribed are all at least 2 hours long."
    author: "panzi"
  - subject: "+1\nOr at least make widgets"
    date: 2009-04-13
    body: "+1\r\nOr at least make widgets resizeable and add a scrollbar to the context view."
    author: "panzi"
  - subject: "Ok, newly downloaded episodes"
    date: 2009-04-13
    body: "Ok, newly downloaded episodes seem to be remembered. However, they still get the file:// prefix, which causes amarok to think it's a stream and you cannot read or even edit the id3 tags. Pity that the old episodes aren't detected as downloaded."
    author: "panzi"
  - subject: "Dockable"
    date: 2009-04-14
    body: "It could be nice... Right now Amarok2 works great on me because I have hided the plasmoid area. But the playbuttons still does not fit well to typical KDE4 desktop. Dont know why... \r\n\r\nI wish there would be easy way to change buttons skins. It is great that style follows KDE4 style but buttons etc are always same :-(\r\n\r\nI dont say Amarok2 is bad or anything such. All application just can be better and it is hard part to find how ;-)"
    author: "Fri13"
  - subject: "Equalizer"
    date: 2009-04-18
    body: "When Amarok will have a graphic equalizer?"
    author: "dsemblano"
  - subject: "When it's supported by"
    date: 2009-04-22
    body: "When it's supported by Phonon, I do believe."
    author: "madman"
---
<div align="right" style="float: right;">
<div align="center"><img style="float: none;" vspace="15" hspace="15" border="0" align="right" src="http://dot.kde.org/sites/dot.kde.org/files/amarok2.0.90t.png" alt="Amarok 2.1 Beta 1" /></div></div>Amarok 2.1 Beta 1 is out in the wild! Featuring one of the longest ChangeLogs in Amarok history, this beta release showcases what is possible when building on the strong technical foundations that have been laid with Amarok 2.0. Nearly all parts of Amarok have received attention, and while not a final release, it is already very usable and quite stable. Hop on over to <a href="http://amarok.kde.org">amarok.kde.org</a> to read the <a href="http://amarok.kde.org/en/releases/2.1/beta/1">full release announcement</a> including the epic sized ChangeLog.
<!--break-->
