---
title: "First KDialogue Is Now Open"
date:    2009-09-28
authors:
  - "einar"
slug:    first-kdialogue-now-open
---
<p>Today, the KDE Community Forums, in collaboration with <a href="http://behindkde.org">"People Behind KDE"</a>, have launched a new initiative to give the community an opportunity to get to know each other a bit closer: <a href="http://forum.kde.org/viewforum.php?f=135">KDialogue</a>.</p>

<p>KDialogue is, in short, a way to ask one of the community members about their personal and KDE related life. At fixed intervals, a KDE contributor will be asked to participate in a dialog. The community will propose questions in a special forum set up at the KDE Community Forums, and everyone can vote on questions they would like to see answered (in a similar vein, although simplified, to the KDE Brainstorm).</p>

<p>Voting will stay open until one week before the dialog, and at that point the contributor will be asked to answer the top-voted questions. His or her answers will then be published on the <a href="http://behindkde.org">behindkde.org</a> web page.</p>

<p>The first KDE contributor to be under the spotlight is <a href="http://forum.kde.org/viewtopic.php?f=135&t=82458">Jonathan Riddell</a>. Many know Jonathan for being one of the founding members of the Kubuntu project, where he currently acts as head of the development team and package maintainer. However, his involvement in KDE goes much deeper than 'just' working on a KDE-flavored distribution.</p>

<p>So here is your chance to ask Jonathan any question you want. <a href="http://forum.kde.org/viewtopic.php?f=135&t=82458">Head over to KDialogue</a> and be creative!</p>