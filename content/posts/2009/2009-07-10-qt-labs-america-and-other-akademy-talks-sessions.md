---
title: "Qt Labs America and Other Akademy Talks & Sessions"
date:    2009-07-10
authors:
  - "jriddell"
slug:    qt-labs-america-and-other-akademy-talks-sessions
comments:
  - subject: "still no s"
    date: 2009-07-10
    body: "And one more story with my name misspelled. :) Adam, no s."
    author: "till"
  - subject: "Sorry, Till. Fixed :)"
    date: 2009-07-10
    body: "Sorry, Till. Fixed :)"
    author: "nightrose"
  - subject: "Translations"
    date: 2009-07-10
    body: "http://transifex.net/ is a great translation tool for upstream AND downstream. It will also have a nice web frontend and it works with every SCM. \r\nMaybe KDE could look into using that with the git transition? (Just a suggestion .. I'm not really a translator.)"
    author: "kragil"
  - subject: "can you fix the other one"
    date: 2009-07-10
    body: "can you fix the other one too, I can't do it cuz the dot thinks I'm a big spammer ;-)"
    author: "jospoortvliet"
  - subject: "I'm a student of Sao Paulo"
    date: 2009-07-11
    body: "I'm a student of Sao Paulo University from Brazil and I'm interested in get involved. Please invite me."
    author: "traysh"
  - subject: "Join the appropriate"
    date: 2009-07-11
    body: "Join the appropriate mailinglist and request it there, this really isn't the place for that..."
    author: "jospoortvliet"
  - subject: "help this person out?"
    date: 2009-07-11
    body: "Jos, or anyone else who knows...could you leave some additional info for the person interested in helping?  Clearly they don't know where to start. What mailing lists are appropriate for this latin american initiative?   Where can he find them to sign up?  Lets not turn people away at the door ;)"
    author: "neomantra"
  - subject: "Thank you. What is the"
    date: 2009-07-12
    body: "Thank you. What is the appropriate mailing list?"
    author: "traysh"
  - subject: "kde list info"
    date: 2009-07-13
    body: "Hey, so I found this site: https://mail.kde.org/mailman/listinfo.\r\n\r\nIt lists all the KDE mailing lists.  I dont' see anything that looks like it would be geared specifically about this Latin American project so you might just want to try the general KDE list here and ask if anyone has more details: https://mail.kde.org/mailman/listinfo/kde"
    author: "neomantra"
  - subject: "Information about how to help in Brazil"
    date: 2009-07-13
    body: "Hi, and welcome. For information on how to help KDE efforts in Brazil, see\r\n\r\nhttp://br.kde.org/\r\n\r\nand more specifically\r\n\r\nhttp://br.kde.org/index.php?title=Venha_fazer_parte_da_equipe_KDE_4\r\n\r\nWe have a kde-br mailing list, follow the links above to subscribe and join our discussion (in portuguese)"
    author: "Mauricio Piacentini"
  - subject: "How to help in Brazil 2"
    date: 2009-07-13
    body: "Please, join KDE brasil community:\r\n\r\nhttp://br.kde.org/\r\n\r\nand more specifically\r\n\r\nhttp://br.kde.org/index.php?title=Venha_fazer_parte_da_equipe_KDE_4\r\n\r\nSoon we'll post on Brazilian mailing list instructions about Qt Labs America an also on planet KDE. You can also contact me by email or irc #kde-brasil . It's important that you contact me so I can add your university in our plans.\r\n\r\nCheers,"
    author: "MoRpHeUz"
  - subject: "Thank you very much!"
    date: 2009-07-16
    body: "Thank you very much!"
    author: "traysh"
---
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 240px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/qt-labs.jpg" width="240" height="181" /><br />
Qt Labs Americas being announced
</div>
Akademy is continuing here in Gran Canaria with many talks, BoFs and announcments.

One of the big announcements was for Qt Labs America. The team at OpenBossa are working with Qt Software to promote Qt development in Latin America, starting with Brazil. They want to find students to work on KDE as a means to learning development, similar to the methods tried by the university in Toulouse. They will sponsor KDE developer sprints, and are looking for KDE teams to invite out to Brazil.
<!--break-->
Sebastian Kügler gave a keynote on the Momentum of KDE. He talked about the exciting technologues we have such as the semantic desktop and the social desktop projects. He did not hide away from the many challenges we have too such as improving the web experience in KDE. Our community is great he concluded and we should all be happy to be working with such motivated people.

Several talks were given on making money with free software. Till Adam discussed the experiences of KDAB in commercially supporting Kontact.  They do a lot of work for various governments on Kontact and other applications.  Most of this is in Germany where there is a law requiring the use of Free Software when possible. Many government departments like to spend money they have because otherwise they will not get that money again next year, and KDAB are happy to help them do that. They have problems convincing Kontact users that they will need ongoing maintenance and not just one off setup help, but a bigger problem is that they have hired a lot of the KDE PIM developers so there are now fewer left to do maintenance outside of KDAB.

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 240px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/bof.jpg" width="240" height="180" /><br />
A full BoF
</div>
The KDE Usability Project has been convincing developers to consider usability and design from the start of the development process. Celeste Lyn Paul gave a talk on how to start the design process giving the example of Plasma. Napkin drawings with too many details not useful, be careful not to put too much detail into initial mockups, but use that as the starting point to build on. Testing with users is important too.

Qt Software has recently changed to Git. Simon Hausmann talked about their old CVS setup which was visible to the outside world and that when they changed to Perforce the repository was mostly hidden away which hurt development from the community. Now with Git development is not just visible but activly open to everyone. They have been collaborating with Gitorous developers on the platform. He said Qt Software can help KDE get features it needs into Gitrorous, and using that will take some burden off infrastructure on our side. On the Wednesday there was a BoF to discuss what needs to be done to move KDE to using Git and a long list of transition that would be needed including moving everyone to ssh, removing svn:externals, splitting the modules into separate applications. Amarok will be the first test case for moving to Git which will probably happen as soon as possible. No decision has been taken on whether the rest of the archive would move in one big transition or piece by piece. It was noted that translators would still use SVN for some time to come.

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 240px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/hacklab.jpg" width="240" height="180" /><br />
The hacklabs are full of busy developers
</div>
David Faure is the efficient process master and spoke on using GDB in preference to KDebug, he pointed the audience towards the GDB macros in kdesdk. In a further productivity talk Kevin Ottens emphasised the Getting Things Done philosophy. He talked on the inbox 0 approach to handling mail and demoed Zanshin, an application he is working on for To Do list management.

Pardus held a BoF to show off their COMAR tools which configure a range of system options from network to services and power management. There was discussion about which parts can be included in KDE and whether any of it could be shared with other distros.

Mike Arthur introduced CPack, a package builder that every KDE developer has on their system but does not know about. He advocated using CPack to create Windows and Mac installers for KDE applications which have the simplicity to download and install that users of those platforms are used to. There was discussion on the merits of including all the dependencies in one package and how updates could be done. CPack does work on Linux but he said the quality of packages was low compared to those made by distributions.