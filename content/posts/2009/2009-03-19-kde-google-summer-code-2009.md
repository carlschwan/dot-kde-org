---
title: "KDE in Google Summer of Code 2009"
date:    2009-03-19
authors:
  - "jefferai"
slug:    kde-google-summer-code-2009
comments:
  - subject: "Articles about each project, before and after?"
    date: 2009-03-20
    body: "It would be neat to see articles on each project, listing their goals and background and hopes for the summer, and then companion articles in the Fall."
    author: "JabberWokky"
  - subject: "Blah blah"
    date: 2009-03-25
    body: "I quote:\r\n\"This will give KDE another opportunity to achieve the massive forward momentum\"\r\nWow that's heavy, but silly. KDE4 is losing popularity, compared to Gnome.\r\nIMHO KDE should do more about integration with other systems like a Windows Network, with for example comprehensive and complete wizards guiding you. "
    author: "stefbon"
  - subject: "Windows Network integration"
    date: 2009-03-25
    body: "That would need developers who have Windows Networks to test with. I think that KDE does not have them. Could you do it?"
    author: "odalman"
---
<img src="http://dot.kde.org/sites/dot.kde.org/files/2009socwithlogo.png" alt="Google Summer of Code 2009" align="right" width="300" height="200" />

This summer KDE will once again be participating in Google Summer of Code!  This will give KDE another opportunity to achieve the massive forward momentum and influx of new developers that has been the hallmark of each Summer of Code.
<!--break-->
The administrators and mentors are looking forward to seeing students' proposals. Many prominent programs and features in today's KDE (such as <a href="http://okular.kde.org">Okular</a>) started their life as Summer of Code projects, and this year is not expected to be an exception. Students should be sure to check out the <a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2009/Ideas">GSoC 2009 TechBase page</a> for ideas, and are of course welcome to submit their own. Any questions about the program should be directed to kde-soc-mentor@kde.org, and students looking for mentors or wanting feedback on their ideas or proposals should contact the devel mailing list, kde-devel@kde.org.

Mentors must sign up on the <a href="http://socghop.appspot.com">Appspot page</a>, and students will be able to start applying on March 23rd. A full timeline <a href="http://socghop.appspot.com/document/show/program/google/gsoc2009/timeline">is available</a> as well.

All participants (students and mentors alike) are encouraged to join #kde-soc on Freenode.

Good luck to prospective students!