---
title: "New Qt Community"
date:    2009-04-01
authors:
  - "troubalex"
slug:    new-qt-community
comments:
  - subject: "Color."
    date: 2009-04-01
    body: "No offense, but.. why is everything pink? is a little weird that color for a Qt website, looks like some teenager blog. Everything else it's cool.."
    author: "alexismedina"
  - subject: "My assumption is that is what"
    date: 2009-04-01
    body: "My assumption is that is what makes it \"cute\". I'm expecting to see ponies next."
    author: "Brandybuck"
  - subject: "Things are never as bad as they seem"
    date: 2009-04-01
    body: "Things are never as bad as they seem. Especially on April 1st :).\r\n\r\nI can't believe they actually got you ;).\r\n"
    author: "Michael Howell"
  - subject: "Amazed (and reading through planet Ubuntu)"
    date: 2009-04-01
    body: "Wow! ubuntuforms.org did the same thing, except that Qt did it better :).\r\n"
    author: "Michael Howell"
---
<p>Today, the Qt Marketing Team has launched a new website to engage the community in a different way: <a href="http://www.qtoverload.com">QtOverload.com.</a> This site is dedicated to unusual Qt development and will mirror the diversity of the ecosystem surrounding Qt.</p>

<p>Qt Software directly asks for submissions by their readers and hopes for a broad spectrum of contributions ranging from mockups to actual applications that make use of Qt technology.</p>

<p>If you have anything you would like to contribute yourself, please, send an email to submissions [at] qtoverload [dot] com.</p>
