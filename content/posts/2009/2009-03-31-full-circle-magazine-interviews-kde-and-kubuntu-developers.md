---
title: "Full Circle Magazine Interviews KDE and Kubuntu Developers"
date:    2009-03-31
authors:
  - "jriddell"
slug:    full-circle-magazine-interviews-kde-and-kubuntu-developers
---
<a href="http://fullcirclemagazine.org/">Full Circle Magazine</a>  has <a href="http://dl.fullcirclemagazine.org/issue23_en.pdf">a new issue out (4MB PDF)</a> with a couple of KDE themed interviews.  Lydia Pintscher, Amarok community manager and Kubuntu Council member, is interviewed about how she got into Linux, her role in the community, the fate of KDE women and even how she fell in love.  Elite Kubuntu packaging ninja, Steve Stalcup talks about how he got into packaging, where to get help and what makes him amazed.
