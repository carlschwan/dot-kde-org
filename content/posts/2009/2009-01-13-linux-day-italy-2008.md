---
title: "Linux Day Italy 2008"
date:    2009-01-13
authors:
  - "gventuri"
slug:    linux-day-italy-2008
---
A little bit in late to release this article, but important to let know KDE community about our involvement to spread KDE to Italian people.  Last October, three members of <a href="http://www.kde-it.org/">KDE Italia</a> gave talks in three different Italian cities. <a href="http://www.hcsslug.org/?SEC=eventi&amp;SUB=Linux%20DAY%202008&amp;id=12">Daniele Costarella in Salerno</a>, <a href="http://linuxday.plugs.it/">Salvatore Brigaglia in Sassari</a> and <a href="http://www.montellug.it/wiki/index.php/LinuxDay2008#Conferenze_proposte">Diego Rondini in Castelfranco Veneto (TV)</a>.


<!--break-->
<h3>University of Salerno, Italy</h3>
<p>
The Linux Day begins with a day in advance here in Salerno. Despite the inevitable unforeseen, the HCSSLug's boys managed to organise a beautiful Linux Day. A large student participation, some of whom are new to the "strange" world of Free Software, ensured the success of the event. After a morning passed together with the very pleasant organisers to work on the final preparations, in front of the <a href="http://www.hcsslug.org/?SEC=video">Neapolis TV troupe</a>, an informative show broadcast by the national TV, the conference about Free Software begins. The first on stage is Carmine De Rosa. I sit and assist with his introduction to Free Software. The concept of Freedom is emphasized also during the following presentations, the first one about FOSS projects and second about the distribution (what they are and how they born). The discussions devoted to Linux and its history make room for another topic that in recent years has made itself heard, "virtualisation" and in particular the security aspect that follow. At this point we have given to those present a 10 minutes break. But, 10 min are not been enough to answer all the questions they asked: "How do I install a Linux distribution?", "Where can I ask support?" and similar questions. We were happy to provide the answer. After the break it is my turn. With the slides that I prepared, I illustrated an example of a desktop environment, KDE as example :P, focusing on the pillars and all that makes KDE 4 a state-of-the-art system and ready to take a challenge with the well-known proprietary systems. I hope to have transmitted at least a bit of the potential of an environment such as KDE. After about 30 minutes of presentation (I believe 30 min were enough to divert the most of the new users and some users of other desktops), I returned to sit. A comparison between the free and proprietary formats (and consequences), a very good demonstration of Blender and some minutes on the vector graphics concluded the event, or at least the part reserved to the talks. I remained beyond the conclusion to answer the thousand questions from enthusiastic users and people who wanted to live in this new free world.  Thanks to HCSSlug for giving me the opportunity to take part in the event. A big greeting to all participants, many of whom will surely read this short article. See you at the next Linux Day!</p>

<p>Daniele Costarella</p>

<h3>Sassari, Italy</h3>

<p>
The was my first time for many experiences. First time on a Linux Day, first time surrounded by kind free software fellows, first time to have a speech in public about Free Software. Is it enough? I was a bit scared, but my mission was most important: talking about KDE 4 (I will not write here what I said. My slides are downloadable on the KDE Italia website and are in Italian)!</p>

<p>They gave me half an hour, which I thought was really a lot of time but, when the organisation-guy looked at me tipping with his finger on his wristwatch I realised that time flew!</p>

<p>It was in general a really pleasant day with Giuseppe Maxia, MySQL's community lead, talking about how he did business with Linux and Open Source in general, a lot of Ubuntu and openSUSE CDs, public administration and instruction chiefs present, a not-so-young couple asking about how to install Linux and, in general, talks here and there about what I care so much! Unforgettable!
</p>

<p>Salvatore Brigaglia<p>


