---
title: "KOffice 2.0.0 Released"
date:    2009-05-28
authors:
  - "ingwa"
slug:    koffice-200-released
comments:
  - subject: "Excellent"
    date: 2009-05-28
    body: "Congratulations on this release, I'm really looking forward to a wonderful KOffice 2.x series!\r\n\r\nI'd also like to think you for the very honest description of development state and target audience, a lesson well learned from the KDE 4.0 release...\r\n\r\nCheers!"
    author: "thorbenk"
  - subject: "Congratulations!"
    date: 2009-05-28
    body: "Congratulations on getting KOffice 2 out! "
    author: "einar"
  - subject: "Does it support MS office compatibility now?"
    date: 2009-05-28
    body: "I used Koffice 2 in beta stages and liked its simplicity and style alot. But it did not open MS office documents, which unfortunately many of my contacts,friends still use. That alone turned me off.. I hope its included now. \r\nThanks "
    author: "indishark"
  - subject: "Congratulations!"
    date: 2009-05-28
    body: "I hope you all have a fun time now that feature-freeze is lifted and you can enjoy reaping the benefits of the frameworks you've created :)"
    author: "SSJ_GZ"
  - subject: "Congrats!"
    date: 2009-05-28
    body: "Congrats on a job well done. Hope you can now add all the missing parts including that scarce resource - developers!"
    author: "guhvies"
  - subject: "Congrats!"
    date: 2009-05-28
    body: "Congrats on this milestone!"
    author: "girish"
  - subject: "MS Compatibility"
    date: 2009-05-28
    body: "No, KOffice does not have an MS Office filter and I'm fairly certain they are not planning one.  As was stated, ODF is the file format KOffice has been built around.  So they only solutions for MS compatibility are to either use Open Office, or find an ODF filter for MS Office and save to the ODF format."
    author: "cirehawk"
  - subject: "Question About Dockers"
    date: 2009-05-28
    body: "I too offer congratulations on the 2.0 release.  I hope the developer's \"KDE4\" like strategy pays off and we begin to see rapid improvements (which is indeed happening with KDE4).  I do have a question about dockers though.  Whenever I start a KOffice app initially, the dockers are positioned similar to the above screenshot.  However, whenever I select an object in one of the dockers (a flake shape for instance), the size of the docker changes to take up the majority of the docker space.  Is there a way to force the docker size to remain the same?"
    author: "cirehawk"
  - subject: "Well, actually"
    date: 2009-05-28
    body: "We do have import/export filters for many of the binary MS Office file formats -- .doc, .xls, .rtf. They're not perfect, but the .doc import filter got improved a lot during the past summer of code.\r\n\r\nWhether we'll ever have ooxml filters depends on a couple of things: whether Microsoft will ever support ooxml (they don't right now), whether someone will get interested in it and whether anyone will ever really use that file format."
    author: "Boudewijn Rempt"
  - subject: "known"
    date: 2009-05-28
    body: "The dockers resize according to their content. Some dockers' design might need tweaking to a smaller size. I'm sure the devs are working on this kind of stuff, but as usual patches would be appreciated ;-)"
    author: "jospoortvliet"
  - subject: "Yay :)"
    date: 2009-05-29
    body: "Big, fat congratulations. Your stuff is one of the prides of our developer community."
    author: "Eike Hein"
  - subject: "Gratulation"
    date: 2009-05-29
    body: "Absolutely great. KOffice is not yet another Office B-production but an experimental lab for the future. This is a clear advantage of the open source model where developers can take the time to get it right and build an inspiring platform for the future.\r\n\r\nI just noticed that the icons of the application are incomplete. These small visual glitsches tend to undermine the experience of users and will built a groundswell in interest to see them fixed.\r\n"
    author: "vinum"
  - subject: "MSOffice support"
    date: 2009-05-29
    body: "At work, this is critical -- at least until ODF file format becomes more prevalent for business documents.\r\n\r\nI could not open any of my word documents at work. I could open spreadsheet but formulas didn't show up.\r\n\r\nI know this is a developer-only release. So, this is probably a feedback for devs rather than a rant at KOffice!"
    author: "kanwar.plaha"
  - subject: "Soo..."
    date: 2009-05-29
    body: "Soo.... are the kspread formulas compatible with openoffice ODF or microsoft ODF.. :p\r\n\r\nhehe, just kiddin.\r\nNice jobs getting koffice 2.0 out you guys.\r\n\r\nCongrats.\r\n"
    author: "MarkHannessen"
  - subject: "Boudewijn"
    date: 2009-05-29
    body: "Well, we take the MS ODF formula namespace into account at least; kspread isn't surprised by those. Whether the formula's work, I don't know."
    author: "Boudewijn Rempt"
  - subject: "Great"
    date: 2009-05-29
    body: "Although i have never had a look at the source code of KOffice, i have the feeling that this is the office suite with the cleanest and most flexible code base, because it is written in an object-oriented programming language with reuse of components as a top priority. Also it uses the KDE libraries and Qt which also make the impression of being designed very well.\r\n\r\nCongratulations to the developers! This is great work!\r\n\r\nOne remark about the first screenshot showing a kspread example: eeerr, isn't this the way you shouldn't do it? The whole invoice letter is written with kspread. Wouldn't it be better writing the letter with kword and embedding the tables as shapes of kspread? I would have expected that developers use the features they have implemented and praise :-)"
    author: "Dschey"
  - subject: "Damnit. "
    date: 2009-05-29
    body: "You're depleting my Paypal account. If you continue releasing quality software like this I'll have to put more money into it YET AGAIN.\r\n\r\n*sigh*\r\n\r\nThanks for the software. I love you guys and gals for it."
    author: "osh"
  - subject: "Well, actually.."
    date: 2009-05-29
    body: "Tables are not yet implemented in KWord, because of lack of time."
    author: "majewsky"
  - subject: "lol"
    date: 2009-05-30
    body: "Thanks, to you and all the others that commented.\r\nosh; pizzas and coffee will always be appreciated ;)"
    author: "zander"
  - subject: "KOffice 2.0.0 Released"
    date: 2009-06-27
    body: "Well, actually, KOffice do have support for file formats like .doc and .xls --\r\nit's the .docx and .xlsx file formats for which we don't have support. But\r\nthen, Microsoft doesn't support ooxml either, and they have started to\r\nsupport ODF... \r\n-------------------------------------------------------------\r\n<a href=\"http://www.omnistarmailer.com\">email marketing software</a>,\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\r\n<a href=\"http://www.top-tourist-destinations.com/\">Top Travel Destinations\t\t\t\t\t\t\t\r\n</a>\t\t\t\t\t\t\t\r\n"
    author: "dim333"
---
<img src="http://dot.kde.org/sites/dot.kde.org/files/koffice.png" width="123" height="137" alt="koffice logo" align="right" />
The KOffice team is extremely pleased to finally <a href="http://www.koffice.org/2009/05/koffice-200-released/">announce version 2.0.0 of KOffice</a>. This release marks the end of more than 3 years of work to port KOffice to Qt 4 and the KDE 4 libraries and, in some cases, totally rewrite the engine of the KOffice applications.

Over the last few years, the KOffice team has reworked the framework into an agile and flexible codebase.  Our intention was to increase integration between the components of KOffice, decrease duplication of functionality and ease maintenance and development of new features. Furthermore, new approaches to UI design and interacting with the user have been implemented to support the new capabilities.

This is the first release -- we call it a "Platform Release" -- of a long series, much like KDE 4.0 laid the groundwork for what is now a fully mature desktop environment. The developers have so far concentrated on creating a flexible and powerful foundation that we can build on for a long time.

<center>
<a href="http://dot.kde.org/sites/dot.kde.org/files/kspread.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kspreadt.png"></a>
</center>
<!--break-->
<h2>Targeted Audience</h2>
Our goal for now is to release a first preview of what we have accomplished. This release is mainly aimed at developers, testers and early adopters. It is not aimed at end users, and we do not recommend Linux distributions to package it as the default office suite yet. KOffice 2.0 will be useful for some users, but since it is the first release in a long series it is likely to contain bugs and incompatibilities.

It is noteworthy that KOffice 2.0 does not have all the features that KOffice 1.6 had. These features will return in the upcoming versions 2.1 and 2.2, in most cases better implemented and more efficient. Also, not all applications which were part of KOffice 1.6 made it into KOffice 2.0. The missing applications will return in 2.1 or possible 2.2.

<h2>Components</h2>
The release team has decided that the following applications are mature enough to be part of 2.0:
<ul>
<li>KWord - Word processor</li>
<li>KSpread - Spreadsheet calculator</li>
<li>KPresenter - Presentation manager</li>
<li>KPlato - Project management software</li>
<li>Karbon - Vector graphics editor</li>
<li>Krita - Raster graphics editor</li>
</ul>

The chart application KChart is available as a shape plugin, which means that charts are available in all the KOffice applications in an integrated manner. The desktop database creator Kexi and the formula shape are aimed to be available in version 2.1. Kivio, the flowchart editor, is currently without maintainer and it is not certain when or if it will be released.

<h2>Highlights of KOffice 2</h2>
KOffice 2 is a much more flexible application suite than KOffice 1 ever was. The integration between the components is much stronger, with the revolutionary Flake Shapes as the central concept. A Flake Shape can be as simple as a square or a circle or as complex as a chart or a music score. With Flake, any KOffice application can handle any shape. For instance, KWord can embed bitmap graphics, Krita can embed vector graphics and Karbon can embed charts. This flexibility does not only give KOffice unprecedented integration, but also allows new applications to be created very easily. Such applications can e.g. target special user groups like kids or certain professions.

<h3>Unified Look and Feel</h3>
All the applications of KOffice has a new GUI layout better suited to today's wider screens. The GUI consists of a workspace and a sidebar where tools can dock. Any tool can be ripped off to create its own window and later be redocked fo full flexibility. The users setup preferences are of course saved and reused the next time that KOffice is started.

<h3>Native Support for OpenDocument</h3>
The OASIS OpenDocument Format (ODF) is the ISO standard for office document interchange. ODF has been selected as the national standard for many countries around the world, and continues to grow stronger every month. KOffice uses the OpenDocument Format as its native format. This will guarantee interoperability with many other Office packages such as OpenOffice.org and MS Office. The KOffice team has representatives on the OASIS technical committee for ODF and has been a strong participant in the process of shaping ODF since
its inception.

<h3>Platform Independence</h3>
All of KOffice is available on Linux with KDE or GNOME, Windows and Macintosh. Solaris will follow shortly and we expect builds for other Unix versions to become available soon after the final release. It is possible that the release of binaries for Windows and Macintosh will occur after some time if other packages that KOffice depend on need more time.

Since KOffice builds on Qt and the KDE libraries, all applications integrate well with the respective platforms and will take on the native look and feel.

<center>
<a href="http://dot.kde.org/sites/dot.kde.org/files/kpresenter.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kpresentert.png"></a>
</center>

<h2>New Website</h2>
At the same time as the KOffice team releases its new version, there is also a new website designed by Alexandra Leisse. It features a new look, and is built on Wordpress. We hope that it will attract more people who are not developers so that we can build up a library of templates, add-ons, and user-generated documentation and tips-and-tricks.