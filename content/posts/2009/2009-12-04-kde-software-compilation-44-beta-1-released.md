---
title: "KDE Software Compilation 4.4 Beta 1 Released"
date:    2009-12-04
authors:
  - "sebas"
slug:    kde-software-compilation-44-beta-1-released
comments:
  - subject: "Nice!"
    date: 2009-12-04
    body: "I've been looking forward to tabbed windows appearing in kwin since I migrated away from pekwm to KDE3 a few years ago.\r\n\r\n\r\nIs KSsl finally fixed and allows for user-imported CA/Certs by now?"
    author: "colo"
  - subject: "KDE 4.4 Beta1 released"
    date: 2009-12-04
    body: "on www.kde.org it says that KDE 4.4 Beta1 is released. Shouldn't it be KDE SC 4.4 Beta1?"
    author: "mkraus"
  - subject: "Look&Feel Polish Kudos!"
    date: 2009-12-04
    body: "I must say to all involved in the graphics design of the 4.4 series: NOW we're talking. KDE is actually starting to look professional and mature. \r\n\r\nI guess most of all congrats to Mr Pinheiro! But all the rest as well. \r\n\r\nAnd finally the system settings have stopped sucking visually.\r\n\r\nGreat!!"
    author: "Miman"
  - subject: "kssl"
    date: 2009-12-04
    body: "you can use kleopatra to manage your s/mime certs"
    author: "lfranchi"
  - subject: "Nepomuk & Virtuoso"
    date: 2009-12-04
    body: "I hope that with 4.4 Nepomuk is finally ready for prime time and really usable for end users.\r\n\r\nWill Soprano work with Virtuoso 6? Currently it only supports v5.\r\nThe is a Debian package in the works for v6 and it would be awesome to have a fully working Nepomuk out of the Box on Debian Sid!"
    author: "Sepp"
  - subject: "indeed ..."
    date: 2009-12-04
    body: "Good catch, I've fixed it."
    author: "sebas"
  - subject: "Dear $DIETY: Thankyou and Please Let Kubuntu Have Nepomuk..."
    date: 2009-12-04
    body: "Congratulations team KDE! This sounds so very awesome.\r\n\r\n* The new apps sound awesome.\r\n* The animations sound awesome.\r\n* The automounting volumes make me cry terars of joy (Amarok sees my >whole< music collection! Bliss!)\r\n\r\nWhat would be so very, very awesome from the Kubuntu kids would be being able to use the turbo speed nepomuk, dolphins search interface and all the other semantic goodies without needing to know why it's never worked out of the box in Kubuntu, access the command line and manually fiddling around with symlinking the missing /usr/lib/libjvm.so.\r\n\r\nI know you're very busy $DIETY, running the world and all, but it would make so many of us mere mortals so very, very happy if just ticking \"Enable strigi desktop file indexer\" worked all by itself. It was nominated for Jaunty. It was nominated for Karmic. Hoping that Lucid's \"The One\".\r\n\r\nDo this for me, and I'll be good this year... I promise!\r\n\r\nLove, thanks and gratitude to all the hard working KDE/Kubuntu peeps. You're all my hero(in)es.\r\nBugsbane"
    author: "Bugsbane"
  - subject: "Exactly this.\nKDE SC 4.4 is"
    date: 2009-12-05
    body: "Exactly this.\r\n\r\nKDE SC 4.4 is serious business."
    author: "d2kx"
  - subject: "I already hear my Computer tremble :) "
    date: 2009-12-08
    body: "In fear of having to compile the beta, and in excitement of having me stare at it in awe once more. \r\n\r\nThe changes sound great, and I'm especially anxious to try the integrated version tracking in dolphin! \r\n\r\nMany thanks to all of you! \r\n\r\nAnd special thanks to the great people who do the bugtracking triage! You're connecting casual users to the developers, and that's far too easy to underestimate. "
    author: "ArneBab"
---
Today the KDE team <a href="http://www.kde.org/announcements/announce-4.4-beta1.php">makes available the first beta release</a> of what will become KDE Software Compilation 4.4. KDE SC 4.4 Beta 1 is the first step in the official 4.4 development release cycle, which will be completed in February 2010 with the release of KDE SC 4.4.0.

Central goals for the KDE SC 4.4 release include the introduction of a variety of mobile, online and social networking features, new interface effects and tabbing/snapping in window management and of course many new and improved applications, games and workspace widgets.
<!--break-->
<h3>Short overview</h3>

There is a new smart automounter for removable storage. The device notifier now offers actions when a USB stick or DVD is inserted directly in its popup instead of a traditional window. The same technique makes it easier to add widgets to the workspace by showing widgets to add in a panel toolbox, and system-related Plasma widgets such as the battery monitor or network management now appear in the system tray instead of elsewhere on the panel. KRunner moves to the top screen edge when activated, allowing more search results on smaller displays, and has gained a wealth of new plugins including wiki searching, bookmark searching and contacts search.

Subtle yet efficient animations are now present throughout the user interface, in menus, window decorations and in applications, making using KDE software a smoother and more harmonious experience. Windows can be easily tiled to take up half the screen, or can be grouped as tabs within a single window.

Dolphin, the default file manager, gets a powerful search interface and supports further KDE software development by allowing interaction with version control systems. Digital photographers using Gwenview will enjoy a new import tool and better thumbnailing.

All applications will benefit from a new TimeLine virtual filesystem, giving access to recently used items. This is powered by a new high-performance semantic storage backend. KMail gets a number of workflow improvements, including an email archiving feature, tag searching and less intrusive error messages.  The KGet developers have added support for digital signature checking and downloading files from multiple sources. New or entirely reworked applications will see the light of day in this release. Palapeli is a puzzle game which lets you do jigsaw puzzles on your computer. You can also create and share your own puzzles. Cantor is an easy and intuitive interface to powerful statistical and scientific software (R, SAGE, and Maxima). Blogilo is a new, easy to use application to write and post blogs.

<h3>Time Line</h3>

Over the next months, the KDE community will finish and stabilize the improvements to the KDE software compilation. In about 2 weeks a second beta will appear and the first release candidate is expected to appear in the first week of January. The second candidate will be released in the fourth week of January, with February 9th the targeted final release date.