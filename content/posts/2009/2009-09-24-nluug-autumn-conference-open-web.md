---
title: "NLUUG Autumn Conference - The Open Web"
date:    2009-09-24
authors:
  - "jospoortvliet"
slug:    nluug-autumn-conference-open-web
---
On October 29 the dutch <a href="http://www.nluug.nl/">NLUUG</a> will organise a conference about 'The Open Web'. In 18 talks and one keynote we hope to give you the best from a wide range of topics. Things you can expect are cool stuff you can do with HTML5, integrating geoinformation in applications with Geoclue, comet, the social desktop (integrating information from web services and all contacts into applications and your desktop) and much more.

There will be quite some talks related to KDE. Accidental? We don't think so. It shows that KDE is on the forefront of integrating open web technologies into the desktop. It also means that KDE is exciting and cool.

The complete program and details about registration can be found on the <a href="http://www.nluug.nl/activiteiten/events/nj09/">conference website</a>.
<!--break-->
