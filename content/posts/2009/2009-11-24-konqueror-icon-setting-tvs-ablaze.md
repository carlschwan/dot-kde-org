---
title: "Konqueror Icon Setting TVs Ablaze"
date:    2009-11-24
authors:
  - "Eike Hein"
slug:    konqueror-icon-setting-tvs-ablaze
comments:
  - subject: "Pure Awesome"
    date: 2009-11-24
    body: "I'm a big fan of the show, so that really made my day :)\r\n<br>\r\n(Some claim I'm starting to resemble Sheldon more and more. This worries me a bit...)\r\n"
    author: "Mark Kretschmann"
  - subject: "I don't see that. Sheldon has"
    date: 2009-11-24
    body: "I don't see that. Sheldon has much more hair! ;-)"
    author: "Eike Hein"
  - subject: "I do!"
    date: 2009-11-24
    body: "You didn't here him laugh recently... "
    author: "Mamarok"
  - subject: "nicotina"
    date: 2009-11-24
    body: "I've seen KDE in Nicotina (http://www.imdb.com/title/tt0337930/). "
    author: "abrown969"
  - subject: "As previously reported on"
    date: 2009-11-24
    body: "As <a href=\"http://dot.kde.org/2008/09/10/kde-congratulates-cerns-large-hadron-collider\">previously reported</a> on dot, LHC control uses KDE and, um, that was on TV.\r\n\r\nPS <a href=\"http://i38.tinypic.com/2q37yh5.jpg\">this</a>was in the comments of the link page above, classic.\r\n\r\npps I never seem to be able to log in with blogspot openID \"error:Invalid AuthRequest: 768: Invalid value for openid.ns field: http://openid.net/signon/1.0\", something to do with drupal not liking blogger apparently, anyone know what to do about this?\r\n\r\n"
    author: "maninalift"
  - subject: "Alias"
    date: 2009-11-24
    body: "Here's the one from alias\r\n\r\n<a href=\"http://i158.photobucket.com/albums/t120/wdawn/alias-kde.png\" target=\"_blank\"><img src=\"http://i158.photobucket.com/albums/t120/wdawn/th_alias-kde.png\" border=\"0\" alt=\"Photobucket\"></a>"
    author: "ivan"
  - subject: "Caps of that seem to have"
    date: 2009-11-24
    body: "Caps of that seem to have found their way onto KDE-Look.org: http://www.kde-look.org/content/show.php/KDE+in+%22Nicotina%22+%28Movie%29?content=60213"
    author: "Eike Hein"
  - subject: "One DE to rule them all?"
    date: 2009-11-24
    body: "And for those who dont already know, KDE was in use by creators of LOTR. Here is the dot story: \r\nhttp://dot.kde.org/2003/11/29/lord-rings-wields-our-precious\r\n\r\nToo bad those links [to screenshots] now point to wrong place."
    author: "emilsedgh"
  - subject: "The Wayback Machine at"
    date: 2009-11-24
    body: "The Wayback Machine at <a href=\"http://www.archive.org/\">archive.org</a> still has them:\r\n\r\nhttp://web.archive.org/web/20070208165528/http://static.kdenews.org/mirrors/thetwotowers/gollum_success.png\r\nhttp://web.archive.org/web/20070320223203/http://static.kdenews.org/mirrors/thetwotowers/kde_gollum1.png\r\nhttp://web.archive.org/web/20070208165507/http://static.kdenews.org/mirrors/thetwotowers/kde_gollum2.png"
    author: "Eike Hein"
  - subject: "Want that shirt!"
    date: 2009-11-24
    body: "Nice!!! But where can you get that shirt from? =:)"
    author: "vanRijn"
  - subject: "Maybe one day on"
    date: 2009-11-24
    body: "Maybe one day on http://www.sheldonsshirts.com/ :D"
    author: "Eike Hein"
  - subject: "There's a fun bit of synergy"
    date: 2009-11-24
    body: "There's a fun bit of synergy there considering the Big Bang Theory character wearing the Konqueror t-shirt is an experimental physicist, and the LHC is the ultimate physics experiment. Totally makes sense."
    author: "Eike Hein"
  - subject: "KDE in media"
    date: 2009-11-25
    body: "If KDE is good enough for popular media, its good enough for me!\r\n\r\n:-)"
    author: "kanwar.plaha"
  - subject: "Fan of dexter"
    date: 2009-11-25
    body: "I must of missed the Dexter one, can someone tell me where and when "
    author: "ziggyfish"
  - subject: "Ha, found the link finally:"
    date: 2009-11-25
    body: "Ha, finally found the link: http://aseigo.blogspot.com/2006/11/kde-on-tv.html\r\n\r\nWill add it to the article."
    author: "Eike Hein"
  - subject: "Free and Open Source in Movies/TV Shows"
    date: 2009-11-28
    body: "A blog about :\r\nhttp://fosstv.wordpress.com"
    author: "gouchi"
  - subject: "Nifty :) *adds to Akregator*"
    date: 2009-11-28
    body: "Nifty :) *adds to Akregator*"
    author: "Eike Hein"
  - subject: "KMail"
    date: 2009-12-01
    body: "Aaaand the first showing of KDE 4 on TV:\r\nhttp://www.youtube.com/watch?v=AcRH61T2tOQ"
    author: "jospoortvliet"
---

CBS' geektastic sitcom sensation "<a href="http://www.cbs.com/primetime/big_bang_theory/">The Big Bang Theory</a>" has joined a long list of KDE television outings with main character Leonard Hofstadter wearing Konqueror on his chest in last night's episode, "The Vengeance Formulation":

<div style="padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:640px;"><img src="http://dot.kde.org/sites/dot.kde.org/files/tbbt_konq1.png" width="640" height="515" /></div>

<div style="padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:640px;"><img src="http://dot.kde.org/sites/dot.kde.org/files/tbbt_kong3.png" width="640" height="547" /></div>

KDE software has previously been spotted on "<a href="http://dot.kde.org/2003/10/31/kde-television-show-24">24</a>" (FOX), "Alias" (ABC), "<a href="http://aseigo.blogspot.com/2006/11/kde-on-tv.html">Dexter</a>" (Showtime), "<a href="http://www.eikehein.com/kde/heroes/">Heroes</a>" (NBC), "<a href="http://pinheiro-kde.blogspot.com/2009/04/oxygen-on-house-md.html">House, M.D.</a>" (FOX) and "Shark" (also on CBS). If you've seen it anywhere else, chime in below!