---
title: "Camp KDE 2010 Registration Open!"
date:    2009-10-21
authors:
  - "jefferai"
slug:    camp-kde-2010-registration-open
---
Registration for Camp KDE 2010 has officially opened! Check out the beautiful new web site at http://camp.kde.org and click on the Registration link to sign up. (Many thanks to Eugene Trounev for the graphics and Eugene and Leo Franchi for the site design).

Camp KDE 2010 will take place in sunny San Diego, CA on the campus of the University of California, San Diego. Both KDE and UCSD have high hopes that all attendees, whether from KDE, UCSD or the public will find the conference fun, informative, productive and warm (average January high of 18C/66F)!
<!--break-->
Lodging will be at a local hostel right on the beach and a short public transportation ride from the venue. More lodging information is on the registration page.

The schedule is as follows:

<ul>
<li>Fri, Jan 15: Arrive
<li>Sat, Jan 16: Presentations
<li>Sun, Jan 17: Presentations
<li>Mon, Jan 18: Training/Birds of Feathers/Hacking
<li>Tue, Jan 19: Day Trip
<li>Wed, Jan 20: Hacking/BoFs
<li>Thu, Jan 21: Hacking
<li>Fri, Jan 22: Leave
</ul>

Presenters are needed, so if you're interested in presenting, be sure to indicate during the registration process that you'd like to present and on what topic.

Limited subsidies are available for those that need help attending; indicate this during the registration process.

As always, the Camp KDE organizers are very thankful to the sponsors, which are currently <a href="http://www.collabora.co.uk">Collabora</a>, <a href="http://code.google.com/opensource/">Google</a>, <a href="http://ixsystems.com">iXsystems</a>, <a href="http://www.kdab.com">KDAB</a>, <a href="http://ev.kde.org">KDE e.V.</a>, <a href="http://qt.nokia.com">Nokia/Qt</a> and <a href="http://www.ucsd.edu">UCSD</a>. Be sure to check them out and thank them for their contributions.

The Camp KDE organizers are looking forward to seeing you there!