---
title: "KDE Commit-Digest for 21st December 2008"
date:    2009-01-02
authors:
  - "dallen"
slug:    kde-commit-digest-21st-december-2008
comments:
  - subject: "OMG"
    date: 2009-01-02
    body: "OMG... Danny, you're on fire! ;)"
    author: "Carlos Licea"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "Don't take god's name in vain.\n---\nSo when is Qt 4.5 going to be included in the svn repository?"
    author: "charlie"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "Which God? Oh so many and so little time to ponder."
    author: "Marc J. Driftmeyer"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "God's name is \"Danny?\""
    author: "David Johnson"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "It must be! There is no other explanation.\n"
    author: "Mark Hannessen"
  - subject: "Re: OMG"
    date: 2009-01-03
    body: "> So when is Qt 4.5 going to be included in the svn repository?\n\nWhen it's done."
    author: "Stefan Majewsky"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "Put him out you fools, put him out!\nDanny!  Stop, drop, and roll!"
    author: "ethana2"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "Is this a sign of the KDE community?\n\nDanny has failed in this job constantly, not once or twice, but for a very long time now he has been unable to deliver the commits in a timely manner.  When he posts commits that are months old, the fools act like he is doing a good job.  Danny is on fire?  No, It's just that he got so far behind, yet again, which is constantly does, that now he is having to produce very weak commits, which is typically the way he does these, weak.\n\nIt is unacceptable that he is allowed to get so far behind.  It's time Danny, even if you fight it, that you understand you are not able to do this job, you are failing the KDE community by continuing to hold on to this job when you are unable to do it at a timely matter.  Now step aside and let someone who can do this in a timely manner take over.  You have become the thorn in the side of KDE, and you have become a liability.\n\nI don't go to the news stand and buy a three month old paper to read the news.  And people despite the few who kiss up to your slackness, don't come to the dot to read commits that are months old.  Once or twice it could be forgiven but you constantly fall behind and don't have time.  It's time you step aside and let someone else who does have time.\n\nPlease Danny step down, and if you don't then Aaron, please find someone who can deliver these at a timely manner and replace Danny. I'm sorry Danny, but your commits have become not only a joke, but they are now causing division amongst the kde community, you are doing far more damage than you will ever know."
    author: "Anon"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "This is a different Anon, but I just had to mention that this post was, if you'll pardon my language, the dumbest shit I've ever read on the Dot.  It's simply mind-boggling.  I was going to start picking it apart piece-by-piece, but the stupidity was so densely layered that I simply got lost in it all.  Years from now, scholars will still be trying to ferret out all the hidden nuggets of cluelessness, and I suspect it will produce more Theses than Ulysses and Finnegan's Wake combined.\n\nAnd, because I understand the mind of the true dullard quite well (it is not, after all, that hard to emulate): this has nothing to do with me being a KDE or Danny fanboy or whatever absurdity you've instantly dreamt up to convince yourself that you are correct, and everything to do with the fact that you are, objectively, a moron."
    author: "Anon"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "\n\"This is a different Anon, but I just had to mention that this post was, if you'll pardon my language, the dumbest shit I've ever read on the Dot. It's simply mind-boggling.\"\n\ni fail to see how the post is \"dumbest shit\" ..he has made a valid observation and came up with a possible solution ..his language could have been a little bit less  strong but this statements are valid ones ..\n\nif hs is too busy to publish in a timely manner, why not delegate some of the work to somebody else with more time? ..i am not in favor of having him completely removed from the post but ..why not have other people help him out with more consuming aspects of the publications? have others offer to help him out and he refused? ..is there room for others to chip in when he is too busy with other things ..\n\nThis person would be a troll if he says the same thing over and over and over again on each publication but i do not see his statement as trolling ...he made an observation and came up with a possible solution ..\n\n"
    author: "ink"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "Rubbish.\nThis is not a \"job\", nobody is payed or given the task by some authority to do that. Danny decided to do it by himself and if he did not delegate it to someone else, that may be (think about it) because noone actually stood up to give any sort of help (I suppose neither you nor the gruesome poster above did). There's no solution provided in his post, just the useless speech of someone who have probably never done something repeatedly, week after week, on his spare time for others.\nHow is it difficult for you guys to understand that probably nobody will do it if Danny steps down. \nThe worst line of all, IMO, is the one claiming that the \"Digest being late is causing division amongst the kde community\". The only thing doing that is the poisonous comments of spoiled brats, who obviously have no sens whatsoever of a community spirit."
    author: "Richard Van Den Boom"
  - subject: "Re: OMG"
    date: 2009-01-03
    body: "And it's always someone or somebody who absolutely must do it. It's newer \"I will do it\", funny that. "
    author: "Morty"
  - subject: "Re: OMG"
    date: 2009-01-07
    body: "As long as he spams at a community-site, it IS a job.\nMaking a job, does'nt mean you get paid with money."
    author: "Chaoswind"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "You say it is unacceptable that he is allowed to get so far behind.\n\nFrom my point of view, whats unacceptable is that people allow themself to make such comments. Danny is doing an amazing job.\n\nDon't missunderstand me, I think constructive criticism is improtant, but when this becomes an attack on persons, it is just no longer in the best interst of this project(KDE).\n\n\n\", but they are now causing division amongst th kde community, you are doing far more damage than you will ever know.\"\n\nHave you ever thought about it: Danny is not the one making the division while the people who don't appreciate his work is.\n\n\nWhy is it that the people in the community always have to bawl at someone, first (when I started using and following KDE) it was KDE4 and it's developer. Now when KDE4 is shaping up even better than people would imagine, one have to bawl at something else, and Danny is the choice?"
    author: "Christian"
  - subject: "Re: OMG"
    date: 2009-01-07
    body: "> Danny is doing an amazing job.\n\nI ever wonder about this: what job is he doing? Where is the worth of it? Where is the worth of an old news?"
    author: "Chaoswind"
  - subject: "Re: OMG"
    date: 2009-01-09
    body: "So because it's old, it shouldn't be published at all?  Because tghat's what would happen.  I don't check SVN commits.  The digest is how I find out what is going on.  I could find out about it a few weeks late, or never.  I'm fine with a delay.  You'd better be too, unless you're going to be doign a better job yourself."
    author: "MamiyaOtaru"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "\"you are doing far more damage than you will ever know.\"\n\nNo, you are.\n\nCan we please have those posts removed?"
    author: "Carsten Niehaus"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "Well,\n\nI totally disagree with Anon position, but.... why we should remove that post? I mean... I think that we are a great community and I hope that most of us are able to distinguish between shit and chocolate ;)\nSo, please, don't invoke censorship when is more than enough simply answer and isolate Anon's thought with our opinion."
    author: "Iudo"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "I could delete it, but I won't.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "Right decision Danny, keeping the Anon guy's post is good idea for everyone to see his/her stupidity for not seeing the value of giving your time even though you don't have much of it to spare. And you are doing the best any one can. People should be gracious for volunteering your time and efforts, and from what most of us see, the vast majority are truly appreciative.\n\nKeep up the good work and don't be bothered by such individuals. They are not worth it.\n"
    author: "Abe"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "You're setting a troll-standard hard to surpass and on the second day of the year!\nCongratulations! "
    author: "Lisa Engels"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "> It is unacceptable that he is allowed to get so far behind.\n\nDear Sir,\n\nIf you consider the quality of Danny's work \"unacceptable\", you are of course entitled to get a full refund of the money you paid him to do it.\n\nWith kind regards,\nHendric"
    author: "Hendric"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "I won't address every point. but:\n- \"failed in this job constantly, not once or twice, but for a very long time now\"\nNope, i've only fell behind twice in the 143 weeks i've been doing this (that's since April 2006): once in the summer, and this winter. No Digests were skipped, and I think i'm allowed to take a rare break.\n\n- \"produce very weak commits, which is typically the way he does these, weak\"\nAnd these are the same Digests as usual, only without the introduction pieces. Hardly \"weak\". If you don't like the standard of my usual work, feel free to stop reading. ~40,000 satisfied readers will remain.\n\n- \"thorn in the side of KDE, and you have become a liability\"\nIf I didn't step up to resurrect and continue the Digest, there would have been a lot less coverage whilst KDE 4 was in development, and I doubt there would be a Digest today.\n\n- \"step aside and let someone who can do this in a timely manner take over\"\nAnd who would you suggest? This is not the ego speaking, but I don't know anyone in KDE who I could transfer the Digest responsibilities to (actually, I wouldn't even want to impose this sentence on my friends!).\n\n- \"if you don't then Aaron, please find someone who can deliver these at a timely manner and replace Danny\"\nThat's not the way things work around here: Aaron is not my manager or employer.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "Don't feed the trolls, honeybunny <3"
    author: "Lisa Engels"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "Do it better or shut up. Thank you."
    author: "The Devil"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "And the winner is Danny! Anon, you have failed. Please die."
    author: "winter"
  - subject: "Re: OMG"
    date: 2009-01-02
    body: "I think you'd have a hard time finding a single person who actively works on the KDE project (that is they actually contributes) something that doesn't appreciate the work that Danny puts in and understands when he falls behind.\n\nAnd anyway, what makes you think that Aaron had any power to force Danny to step down and push someone else into the position? KDE is a meritocracy, not an authoritarian dictatorship."
    author: "Matt Williams"
  - subject: "Re: OMG"
    date: 2009-01-03
    body: "Do you really expect anyone to answer to your bullshit, or do you want them to give a serious reply? Sorry, but you are less than a lame duck, you have no understanding of KDE, you have no understanding of how it works, and I definitevely think (and I would be VERY SURPRISED if you had anything more to say than a troll (a real, not the most venerated ones)) that you should STFU! You are ridiculous, at best, though I still can't feel to even laugh about your retardedness... Just... if YOU don't contribute anything, keep your mouth shut about anything else! Everyone can criticise, but not in the harmous way you do!"
    author: "Me"
  - subject: "Implementation rush"
    date: 2009-01-02
    body: "Wow!\n\nI'm impressed how fast the features are coming (back) to KDE4 now.\nMaybe the rewrite (e.g. of Amarok) wasn't so bad after all.."
    author: "Ulrich"
  - subject: "Re: Implementation rush"
    date: 2009-01-02
    body: "Yep, what you're seeing here is the strong technical foundations of KDE 4 and its apps finally coming into full effect.\n\nFrom an Amarok developer perspective, many users were puzzled by our decision to do an almost complete rewrite of Amarok, instead of just porting Amarok 1.x to KDE 4. But in the coming months people will see that we can now progress at an amazing speed, thanks to the much better program design.\n\nAs for Amarok, we are about to release 2.0.1 very soon, and although it's just a minor release, it is going to introduce a whole bunch of new features. And this only took us a few weeks to make!\n\nAlso, we even have parts of 2.1 already finished. This is possible thanks to new development tools like Git, which made us much more productive.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Implementation rush"
    date: 2009-01-02
    body: "I am one of those formerly disgruntled  Amarok1-users who was, to put it bluntly, completely p**ed after trying out Amarok2 ;). Now, after reading  the  commit digest and the devs' blogs, I am really once again looking forward to Amarok 2.1. If all the planned features will be implemented, Amarok 2.1 will be the time to switch  from Amarok 1.4. Thank you!\n\nIt has to be stated, however, that the (very much appreciated) planned features for 2.1 in some cases seem to be a (welcome) reversal of the devs' rigid views on how the user is supposed to use Amarok. Now even the \"old-style-playlist\" is going to reappear (yippee!), a feature which was only weeks ago an absolute no-go according to the devs' comments. \n\nSo please don't put it as if the fact that some Amarok users were \"puzzled\" would  be their own fault!\n\nHowever, I  like to see that KDE4 (and corresponding apps) devs are listening to user feedback. Keep up the good work!"
    author: "Anon"
  - subject: "Re: Implementation rush"
    date: 2009-01-02
    body: "No so fast :) If the Excel playlist is going to make a comeback is still very uncertain. If I can help it: NO.\n\nEven we developers sometimes have disagreements and discussions about the direction of the project, and I represent the \"hardcore\" side: I like to say \"No\" more often than \"Yes\". I hate feature creep and sloppy design.\n\n\nAs the founder of Amarok I don't want my name associated with anything resembling this:\n\nhttp://mark.kollide.net/homer_dreamcar.gif\n"
    author: "Mark Kretschmann"
  - subject: "Re: Implementation rush"
    date: 2009-01-02
    body: "Well it always depends on your personal point of view, whether some feature  is \"feature creep\" or \"basic necessity\". Obviously e.g. you have other priorities then me - which is only normal ;) (for me the \"plasma-fication\" looked more like feature creep). \n\nHowever, considering the controversies after the Amarok2-launch, I'd consider it advisable to appease the \"old\" Amarok fans with stuff like \"Excel playlist\" (why is this sloppy design, BTW?) and so on. \n\nAs this is already proposed by some of your co-developpers and initial coding is being done - with overwhelming positive user feedback, as I might note ;) - why not integrate it?\n\nAnyway, that's just my opinion - as  Amarok2 in its current incarnation is somehow unusable for me, I applaud all moves to make it usable for me once again ;). \n\nYou have one initial success: The vision of a re-integration of my favorite features of Amarok1 already made me deinstall \"songbird\" which I  had  decided to test (aaaaaarg :(((( )."
    author: "Anon"
  - subject: "Re: Implementation rush"
    date: 2009-01-03
    body: "What do you mean by \"controversies after the Amarok2-launch\", do you mean this small but vocal minority on dot.kde.org, mailinglists and forums.\n\nI'm happy with Amarok2 playlist, so you want see my on amarok mailinglist. This playlist has rough edges but it should stay like it is in amarok2 and by a little bit immproved. "
    author: "Jon"
  - subject: "Re: Implementation rush"
    date: 2009-01-03
    body: "So, how do you know it's a \"small minority\"?\nAs far as I can see the mentioned feedback is our only point of reference here, and it indicates more than just a minority feeling uncomfortable with the new playlist.\n\nA little OT, but just to see it mentioned somewhere: The suggestion to use JuK is less than optimal for anyone who wants to listen to online radio stations (that is a lot of people, I'd guess). Not to mention all the other advanced features of Amarok..."
    author: "onety-three"
  - subject: "Re: Implementation rush"
    date: 2009-01-04
    body: "I deeply respect developers work.\n\nAround me there are a lot of people that are not very happy with Amarok 2.0, me included. We understand that changes sometimes need to pass throuhg hard times, so we are happy about the Amarok 2.x development. \n\nSo, althought I don't know every Amarok user, I think it's not a silly thing to say there's certain controversy, or sane discussion if you preffer someting more polite.\n\nFinally, I think taht would be a good idea to get back the \"excel playlist\". What worries me is the oppostion to do this, because I feel that developpers (or those who manage Amarok 2.x direction) have changed some philosphy of Amarok since 1.4.x. For me \"excel playlist\" is esential, because it's easier to manage the playlist than actual view (where you see lees data), specially for live plasylist as above user said.\n\nAnyway I'll accept whatever Amarok developpers make, because I know they do it for fun, so it's stupid to say \"hey, stop doing things as you like and do thinhs as I like!\". But I'll love if they think about bring back that \"excel playlist\" and not be so eyecandy-widget-centered .. (I mean, I feel there is less usability and more eyecandy).\n\nI may be wrong, and I may unknow something important that wuold change my mind... but taking into account all what I know that's my opinion.\n\nAnyway thanks for your work!\n\n\n\n\n\n\n\n\n"
    author: "Git"
  - subject: "Re: Implementation rush"
    date: 2009-01-04
    body: "I forgot to mention that when I say bring back the excel plasylist I mean that or add functionality to catch up the excel playlist funcionality.\n\nAs I can read here: \nhttp://amarok.kde.org/blog/archives/810-The-Old-style-Playlist-Is-Dead,-Long-Live-The-Old-style-Playlist.html\nthe new playlist could tdo this, and really overcome old excel playlist... but it needs to be developped. I hope there are plans to do this... :)\n\nBy the way, I can't use \"a\" html tag...\n\n\n\n\n"
    author: "Git"
  - subject: "Thank you (again!)"
    date: 2009-01-02
    body: "Thank you Danny ! I think you deserve another akademy award for your work :)\n\njust a reaction to your recent blog post : \n\nIf writing publication with 'featured stories' is too time consuming (or frustrating), I think these \"shorter editions\" are completely OK.\n\nActually I'd prefer you completely drop the 'featured stories' instead of knowing that you may step down because you're tired of chasing people...\nThe 'Featured stories' could be done by other peoples on the dot as a different kind of articles.\n\nSo, again, thank you Mister Allen, and be sure that a lot of people appreciate your work :)"
    author: "shamaz"
  - subject: "Re: Thank you (again!)"
    date: 2009-01-02
    body: "I disagree!\n\nI really liked the 'featured stories', as these in many cases pointed out features of KDE4 I wouldn't have noticed otherwise. They were good starting points for \"exploring\" KDE4 and related apps. (Yes, if I had the time to follow the developpers' posts, I wouldn't have needed Danny's stories...) \n\nHowever, I also see that writing those 'featured stories' is  a rather time consuming job :-/. Maybe it would be good to make a split - the \"pure\" commit digest maintained by Danny, and a loose series of 'featured stories' by various other authors?\n\nAnd BTW: I'm very thankful to Danny, however I consider the obligatory \"thank you Danny\"-posts on the Dot a bit boring. BUT: Currently there seem to be a few posters who, on the opposite, seem to consider \"Danny-bashing\" fashionable. That is something that really, really sucks big time! Please ignore those pricks!"
    author: "Anon"
  - subject: "Re: Thank you (again!)"
    date: 2009-01-02
    body: "\"And BTW: I'm very thankful to Danny, however I consider the obligatory \"thank you Danny\"-posts on the Dot a bit boring.\"\n\nThey aren't \"obligatory\" - people post thanks because they are genuinely appreciative of Danny's work."
    author: "Anon"
  - subject: "Re: Thank you (again!)"
    date: 2009-01-02
    body: "I liked them too, but if developers aren't writing them, they don't exist - the Digest is not going to become a mirror of PlanetKDE!\n\nThey'll return on the January Digests, but as I explained in my blog, the current usage is unsustainable (and I haven't seen any real change in pro-active sending of content yet!).\n\nDanny\n\np.s. I'm not some fragile flower who needs obligatory \"thank you Danny\" posts to keep me out of therapy (though they sure are nice!)"
    author: "Danny Allen"
  - subject: "Re: Thank you (again!)"
    date: 2009-01-02
    body: "I did not want to say that I'm not thankful to you - I am! However, if  the first twenty posts after each commit digest entry  are nothing more than \"thank  you Danny\", it is somehow boring ;). However, this way it is definitely better than those strange \"Danny step down\"-postings which are occuring currently :(.\n\nI would strongly encourage the devs to contribute more 'featured stories' for your digest. It's the best way to advertise their own work. Maybe I'm a bit old-fashioned, but I don't want to follow all blogs (and  there is lot of nonsense written in the blogs, like in a diary) in order to be updated about KDE. I'd like to have some nice to read, to-the-point articles about 'featured new features' of my favorite desktop environment."
    author: "Anon"
  - subject: "Re: Thank you (again!)"
    date: 2009-01-02
    body: "After having read all the recent comments, I must add that I'm really impressed by your perseverance. Continuing to offer us these publications despite all these (undeserved and trollish) criticisms is a great proof of your patience :o.\n"
    author: "shamaz"
  - subject: "Happy New Year!!!"
    date: 2009-01-02
    body: "Happy New Year to everyone!\n\nI just wish to thank all KDE developers for all their work. 2008 has been a really exciting year. It has been great to have the opportunity of watching a desktop shell being built in front of our own eyes. We have had everything this year, a new desktop shell, supporters, people against it, trolls, fanboys, lots of bugs, and then lots of bugs being squashed, missing features that were later reimplemented and controvertial features that were discussed for months.\n\n2009 looks like an exciting year too, with the big KDE apps joining the KDE 4 party we will witness most of the completion of the KDE 3 -> KDE 4 transition this year. An exciting time for begin a KDE user indeed.\n\nI wish you peace, love and a great 2009 for all of you"
    author: "Raul"
---
In <a href="http://commit-digest.org/issues/2008-12-21/">this week's KDE Commit-Digest</a>: Restoration of basic queue management functionality, support for basic searching and filtering the playlist, work on device handling, and better integration of the Mac OSX "Growl" notification system in <a href="http://amarok.kde.org/">Amarok</a> 2.0. The <a href="http://plasma.kde.org/">Plasma</a> "Pager" now accepts taskbar entry drops, support for dragging applets between panels, and improvements in setting keyboard shortcuts for Plasmoids. Import of a "BookmarkSync" plugin for <a href="http://www.konqueror.org/">Konqueror</a>, allowing storage and retrieval of bookmarks on multiple computers from a central location. Support for full content zoom (instead of text-only zoom) when using <a href="http://webkit.org/">WebKit</a> with Qt 4.5, with plugins (eg. Flash) starting to work. Support for nested "for loops" in <a href="http://edu.kde.org/kturtle/">KTurtle</a>. More work on the rewrite of Kolf. A basic prototype of a MuPDF-based backend (for PDF documents) in <a href="http://okular.org/">Okular</a>. More work on the Windows Live Messenger protocol in <a href="http://kopete.kde.org/">Kopete</a>. Support for running programs as other users (using kdesudo) in KDevPlatform (used in <a href="http://www.kdevelop.org/">KDevelop</a> 4, etc). A new game in development, "FruitWars", is added to KDE SVN. Reorganisation of wallpapers (including the addition of "Air", the default for KDE 4.2) in KDE SVN.<a href="http://commit-digest.org/issues/2008-12-21/">Read the rest of the Digest here</a>.

<!--break-->
