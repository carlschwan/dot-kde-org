---
title: "KDE 4.2 Release Parties All Over The World"
date:    2009-02-04
authors:
  - "nightrose"
slug:    kde-42-release-parties-all-over-world
comments:
  - subject: "Congrats and thanks to everyone!"
    date: 2009-02-04
    body: "First I have to thank all of you who answered the call to organize the parties and all people who came to parties to celebrate this awesome release of KDE. And it is great to see Slovenia doing so good again. Looks like a lot of people here in Slovenia know the importance of free and open source software and like it (don't forget that Firefox is used by more than 50% of people here). Thanks again to all!"
    author: "JLP"
  - subject: "Great."
    date: 2009-02-05
    body: "Also, I've managed to give 4.2 a try now and it's wonderful. I love it. It's beautiful and does everything I need (except connect to MS-Exchange). \r\n\r\nI failed to donate money at the release but I hope you'll accept them now instead. Not much but hey, it's bad times..."
    author: "osh"
---
<p>
Last week, the KDE 4.2 release was not only received well in several reviews, but also celebrated by contributors and users on a global scale. A number of <a href="http://wiki.kde.org/tiki-index.php?page=KDE 4.2 Release Party">release parties</a> were organised by enthusiasts, often with surprisingly high numbers of participants.
</p>
<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; float: right; width: 300px">
<a href="http://websvn.kde.org/*checkout*/trunk/dot.kde.org/lydia/42releaseparty.jpg?revision=921223"><img src="http://websvn.kde.org/*checkout*/trunk/dot.kde.org/lydia/42releaseparty_thumb.jpg?revision=921223" /></a><br />
KDE 4.2 release party in São Paulo
</div>
<p>
In Asia, the OpenSUSE community invited to the party in <a href="http://andi.opensuse-id.org/2009/02/02/bandung-opensuse-111-and-kde-42-release-party/">Bandung, Indonesia</a> which included several presentations.</p><p>South America saw several events in major cities such as <a href="http://gallery.atpic.com/24867">Buenos Aires</a>, <a href="http://www.jasonnfedora.eti.br/index.php?option=com_content&task=view&id=70&Itemid=36">Salvador</a> and <a href="http://websvn.kde.org/*checkout*/trunk/dot.kde.org/lydia/42releaseparty.jpg?revision=921223">São Paulo</a>.</p>
<p>North American KDEers got together in the <a href="http://weblog.obso1337.org/2009/washington-dc-kde-42-release-party/">Washington DC area</a> at Piratz Tavern with some familiar faces from the local LUG and Ubuntu Loco communities and also several new faces. Piratz and ninjas alike had a good time drinking grog, singing pirate songs, and passing around laptops with the latest and greatest.</p>
<p>Many events were hosted in Europe, the biggest one taking place in <a href="http://jlp.holodeck1.com/blog/2009/01/28/kde-42-release-party-in-slovenia-a-big-success/">Ljubljana, Slovenia</a>. Not far from there, the FSFE occupied the Metalab in <a href="http://wiki.fsfe.org/FellowshipGroup/Vienna/KDE_4_2_Release_Party">Vienna, Austria</a>. Both <a href="http://sslug.netslave.dk/da/galleri/view/3">Denmark</a> and <a href="http://jespersaur.com/drupal/node/44">Norway</a> had parties in their respective capitals, one of them with a slightly higher number of Trolls. In Spain, the folks in <a href="http://flickr.com/photos/r0uzic/3241121232/">Barcelona</a> met up. Germany had a bunch of parties as well, for example in <a href="http://www.kdedevelopers.org/node/3871">Stuttgart</a>, also co-located with an FSFE meeting and attracting about 40 people and presentations on e.g. the new Amarok. In <a href="http://www.kuarepoti-dju.net/blog/2009-02-03-kde42frankfurt/">Frankfurt</a>, people met close to the KDE e.V. office with a mix of current and potential future contributors.
</p>
<p>
Additional <a href="http://zecke.blogspot.com/2009/02/thank-you-for-kde42.html">blog</a> <a href="http://kdedevelopers.org/node/3870">entries</a> and <a href="http://www.kiberpipa.org/modules/gallery/album432">photo</a> galleries have been published recently and this is expected to continue for additional locations not already included in the paragraph above. As a special bonus for Italian users, there will be <a href="http://www.kde-it.org/news.php?extend.296">one more party</a> in Naples this coming Saturday!
</p>