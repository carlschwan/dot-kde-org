---
title: "KOffice 2009 Sprint In Berlin"
date:    2009-06-13
authors:
  - "Boudewijn Rempt"
slug:    koffice-2009-sprint-berlin
comments:
  - subject: "Other notable topics"
    date: 2009-06-14
    body: "We had a lot of technical talks about stuff that was too complex to discuss on IRC.  For example; we managed to simplify some API in flake after realizing a new feature was actually ideal to be merged with an existing one removing one virtual method.\r\n\r\nOther things that were useful was a refactoring of the kotext undo/redo code to work together with scripting and with change tracking.  Introducing the new guy to loads of KOffice concepts, and just plain hacking on those things that its useful to ask other experienced Qt devs to help with."
    author: "zander"
  - subject: "Congratulations"
    date: 2009-06-14
    body: "As a KDE user in times past, looking to return to the fold soon, it's great to see how much progress has been made in my absence. KOffice 2.0 looks incredible - I can't wait to play with it! Congratulations to all the K* developers out there, you're producing some stunning work."
    author: "smuj"
  - subject: "Keep up the good work! We really need you guys."
    date: 2009-06-14
    body: "Nice one. I half expected the word company in the picture to be spelled with a K. The KOffice release has not yet appeared on Ubuntu's updates. In fact, neither has KDE 4.2.3 even. I know about the PPAs, but I'd rather get them through official channels. Is the port for Mac and Windows available? Honestly, I'd use KOffice in a heartbeat. OpenOffice is getting a bit too bloated and slow for my liking. Its one thing working on a document at home, but quite another when you're waiting for about 15 seconds for a spreadsheet to start up at work and a colleague is standing there patiently. While I really do support the ODF format and use it wherever I can, I think there's no reason the application itself needs to be so slow. "
    author: "carleeto"
  - subject: "Ubuntu update policies"
    date: 2009-06-14
    body: "\"The KOffice release has not yet appeared on Ubuntu's updates. In fact, neither has KDE 4.2.3 even. I know about the PPAs, but I'd rather get them through official channels.\"\r\n\r\nThis will not happen. The whole point of a stable release is that the versions do not change after the release. (The opposite is the rolling release.) And the whole point of PPAs is to allow people to mix newer versions into a stable release where they want to. (There are surely enough people who just want to keep KOffice 1.6 for now.)"
    author: "majewsky"
  - subject: "Librarification"
    date: 2009-06-15
    body: "\"This means that we will, in all probability, be able to offer libraries for low-level ODF handling, text handling and color management that can be used outside KOffice applications.\"\r\n\r\nExcellent - share the love :)\r\n\r\nAnd congrats to Jos, too - hopefully he'll be the first of many!"
    author: "SSJ_GZ"
  - subject: "What???"
    date: 2009-06-16
    body: "Commenter stated:\r\n\r\n\"Its one thing working on a document at home, but quite another when you're waiting for about 15 seconds for a spreadsheet to start up at work and a colleague is standing there patiently.\"\r\n\r\nWhat???  I have a PIII computer 1 ghz processor 768 mb ram running OpenOffice 3 (and of course, Linux).  My computer will load an openoffice document usually in just about 10 seconds.  Sometimes up to 15, but never more.  I attribute this to the computer speed (or slowness), but it is much better than OO2, and MUCH better than OpenOffice on Windows.\r\n\r\nSaying this, I prefer KOffice in general, but for spreadsheets OO works for me, so I use it.\r\n\r\nD.\r\n\r\nPS - Thanks for all your hard efforts on KOffice.  If I was a millionaire, I'd support the project by hiring one or 2 full time staff, but I'm not, so I need to just use it and report bugs when I can."
    author: "Dulwithe"
---
<p>Last weekend &mdash; it seems like yesterday and like a year ago at the same time &mdash; the KOffice team came to Berlin for the first post 2.0 sprint. Graciously hosted by KDAB and smoothly organized by Alexandra Leisse, this sprint was one of the most productive sprints ever for KOffice. Not only because there were many developers attending, among them three out of four of our KOffice Summer of Code students, but also because everyone was filled to the brim with joy and relief about having release 2.0 and eager to forge forwards to 2.1.</p>
<!--break-->
<p>We had chosen a slightly different setup from our other sprints, with presentations and discussions on Saturday, followed by BoF sessions on Sunday. There were a couple of themes.</p>

<a href="http://dot.kde.org/sites/dot.kde.org/files/KOffice_Sprint_2009_group_photob.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/KOffice_Sprint_2009_group_photo.jpg" style="float:right;border: 1px solid; padding: 0.5em;margin: 0.5 em 0em 0.5em 0.5em;"/></a>

<p>The first was collaborative editing, which is really hot now with Google Wave riding the hype cycle &mdash; Pierre Stirnweiss gave a presentation on this topic that was characterized by an almost academic depth and a huge stack of printed research papers. He argued that ODF's change tracking definition is definitely lacking a lot of important features, so that is something we will try to work on within the ODF standards committee.</p>

<p>Boudewijn presented his work on reorganizing the KOffice libraries. We had intended 2.0 to be a stable platform to develop against, but some projects, like Cyrille Berger's Braindump, need more flexibility than we can currently offer. This means that we will, in all probability, be able to offer libraries for low-level ODF handling, text handling and color management that can be used outside KOffice applications.</p>

<p>Then KOfficeSource GmbH introduced their first full-time employee: Jos van den Oever. KOffice being a trademark of KDE e.V., the name the KOfficeSource team had originally selected, had to be changed, and they unveiled the new name, KO GmbH today. This name obliquely refers to the common prefix of the KOffice library class, so it's not totally unconnected either.</p>

<p>Originally we had feared we had so much work to do that we'd only have dinner by midnight on Saturday, but after deciding to try to have the next sprint in Oslo in October or November, more or less concurrent with the release of KOffice 2.1, we went out to one of our favourite Kreuzberg haunts for a really nice dinner, sponsored again by KDAB.</p>

<p>Sunday was for hacking, and your correspondent must admit that he was totally absorbed by the Krita BoF and hasn't got a clue what the other people had been talking about: we had four Krita hackers sitting on the comfortable KDAB couches and we got through an amazing number of issues, chief of which were ways in which we can improve Krita's performance.</p>

<p>Sprints like these are important in so many ways: it introduces new hackers to our community, it's a way of getting work done and decisions made, it offers a chance to get to know the other people's areas of expertise. Thanks again to KDAB for hosting and feeding us, to the KDE e.V. for paying for travel and lodging, to Alexandra for herding the cats and also to Cyrille Berger, for having been the release dude for countless alpha's, beta's, rc's and the 2.0 release.</p>