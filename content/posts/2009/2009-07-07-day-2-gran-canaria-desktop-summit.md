---
title: "Day 2 at Gran Canaria Desktop Summit"
date:    2009-07-07
authors:
  - "jriddell"
slug:    day-2-gran-canaria-desktop-summit
comments:
  - subject: "I am still looking forward to"
    date: 2009-07-08
    body: "I am still looking forward to I can see the videos of those events :)"
    author: "beer"
  - subject: "closed science"
    date: 2009-07-08
    body: "The closed nature of science as mentioned by Glyn Moody is actually quite important, and somewhat scary as you start thinking more and more about it. Anyone that works with FOSS is familiar with both the technical, ideological, and philosophical problems that come with completely proprietary software -- but these problems seem petty and pedantic in comparison to their counterparts in the science community. We're talking about the discovery and dissemination of knowledge. \r\n\r\nIt's understandable that most people never see or think about this; it only starts becoming a problem once you are trying to gain access to information that you need. However, it again becomes obscured through the fact that most people get access to the information through an educational or academic institution of some kind who in turn is paying to get access to that knowledge. \r\n\r\nIt is prohibitively expensive for a person or small group to be able to gain access to knowledge relevant to their field of study, since this would almost always involve multiple subscriptions. It's no wonder that people on average are getting less aware of, and less able to understand scientific advancements; they can only ever afford to get the bastardized version of it from the mainstream media, with exaggerated implications and falsehoods. Wikipedia is a nice middle ground between scientific jargon and the media's versions, but it's usually 2 years or more behind in terms of new advancements.\r\n\r\nAnd this is only the closed <b>dissemination</b> of the knowledge; there's a whole other side to the coin when it comes to the closed nature of scientific exploration. Tons of proprietary tools using obsolete standards that won't evolve until money is thrown at them...and other things that I don't want to go on another tirade about.  \r\n\r\nPS. Really looking forward to grabbing the videos of this year's talks, especially the Glyn Moody and Plasma-netbook ones."
    author: "maniacmusician"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<img src="http://dot.kde.org/sites/dot.kde.org/files/DSCN3785.JPG" width="300" height="209" /><br />
The conference auditorium
</div>

The Desktop Summit is continuing with talks in the Cross Desktop tracks and the start of the Akademy tracks.  Between the talks developers can be spotted huddled in groups discussing everything from problems with their code to building community.  The tracks covered metadata, community, infrastructure and multimedia.    Read on for some of the talks.
<!--break-->
In the infrastructure track Matthew Garrett spoke in his trademark determined style about the problems of power management and showed the future of the power management user interface with only two sliders, for brightness and idle time, everything else should be sorted out by the system not the user. ConnMan, the network setup tool from Intel, was demonstrated.  He assured the crowed that they did not dislike Network Manager but it did not fit into their needs on Moblin.  ConnMan is intended to be easy to make network connections with.  Okular developer Albert Astals Cid spoke about the Poppler project, including an impressive number of applications where it gets used.  Poppler does not cover the full PDF spec, which is very large, but it is gaining features such as multimedia and javascript support.

Jos van den Oever talked about the libraries he has created, libstreams and libstreamanalyser which can be used to extract information from files, and is intended for desktop search application.  At the end of the talk Jos announced that Gnome desktop search tool Tracker was going to start using these libraries from Strigi, plus the ontologies are being shared along with Nepomuk, showing the power of cross desktop collaboration.  

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey">
<img src="http://dot.kde.org/sites/dot.kde.org/files/DSCN3790.JPG" width="300" height="181" /><br />
A spontanious BoF between talks
</div>
Alexandra Leisse showed pretty, funny and motivating pictures to emphasise the motivation needed in a community.  She discussed her methods to deal with communities and the communication channels you can use as well as the importance of keeping diversity in the community.

Akonadi was introduced by Will Stephenson, showing the technology for developers.  It was well attended with interest from Tracker developers.  He emphasised that Akonadi was entirely desktop independent, and later in the day some KDE developers were spotted discussing creating a basic GTK application for Akonadi to prove it.  Kevin Krammer created an Akonadi resorce during the course of his talk which made user accounts on the computer available to Akonadi as addressbook entries, this can immediately be used in Kontact or any other Akonadi application.  Finally in the metadata track Will Thompson presented his profiling tool for D-Bus so developers can see how many calls are being sent on the bus and where from.  He discussed common mistakes made by developers using D-Bus which makes their interfaces an impedence to the speed of applications.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<img src="http://dot.kde.org/sites/dot.kde.org/files/IMG_2562.JPG" width="200" height="211" /><br />
<a href="http://opendotdotdot.blogspot.com/">Glyn Moody</a>
</div>

After the required midday siesta the joint conference talks split and Akademy started.  We enjoyed a well recieved keynote from journalist and author Glyn Moody on Why Hackers Will Save the World.  He gave a review of free culture in general, including Project Gutenberg, which only had 10 books in 1991 but thanks free software had over 1000 a decade later.  The increasingly closed nature of science was a worry, he said, to the commons of world knowledge.  He talked about his book Rebel Code interviewing over 50 free software developers and activists and how that related to his following book Digital Code of Life about genetics. Linux had saved the knowledge of the world during the race to decode the human genome.  The government funded Human Genome Project was losing ground to commercial rivals who would patent genes in any juristiction that would let them, but a Linux cluster let them get the lead and save the knowledge of our genes for free use.  Finally he said that the world's limited physical resources (including oil but also every other substance mined from the earth) will need to be shared following a similar commons model as created by the free software community.

The applications track covered the two main IDEs used by KDE developers.  Aleix Pol talked about KDevelop 4 and the active community which has gained momentum recently to bring it to a forthcoming 4.0 release.  Daniel Molkentin spoke on Qt Creator, a project which was worked on internally at Qt Software before being released as their first open development project.  He showed off the slick UI and code centred approach (press escape and you always get back to your code).  Qt Creator is making bi-monthly releases and is now included in every major distribution.

In the community track Frank Karlitschek covered ideas to integrate into KDE the social desktop API which he has implemented on his OpenDesktop.org site.  For example help in the print dialogue to find people with the same printer model.  He showed the user and friends Plasma applet which will be included in KDE 4.3.  The Knowledgebase module will be in KDE 4.4.

Kevin Ottens talked about his three years of collaboration with the University of Toulouse.  Each year students work on KDE projects.  He talked on the guarantee he has to give to teachers to make them confident that students can work on KDE.  He hoped that others would implement the same idea in other universities but warned that there are some problems to be watched out for.  He emphasised the important aspect of getting results which work from the student projects so they get the free software bug and continue to contribute.  

Thomas McGuire covered the problems with the current KMail design and how it came to be in the shape it currently is.  He said that Akonadi will fix all the problems and porting is currently underway, it won't be in 4.4 but a prototype should be available since then.  He emphasised that stability is important for a mail application because losing a users personal data is a problem so they are being conservative about when to release it.

Torsten Rahn demonstrated Marble and showed off the Wikipedia, Flickr and weather plugins developed by students.  There is an annotations plugin to add your own notes, and he showed the ability to measure distances which follow the curvature of the earth.  He talked about creating a GeoGraphicsView which has a coordinate system suitable for geographic applications.  The long terms wishlist for Marble includes an OpenStreetMap editor so you can anotate the streets shown in Marble.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey; width: 300px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/DSCN3817.JPG" width="300" height="168" /><br />
The Nokia social event got everyone talking and dancing together
</div>

On Sunday night Nokia threw a party at Club Sotavento, close to the harbour. It was a well appreciated event with drinks and food provided for free and enjoyed by everyone. The beer helped the mingling of KDE and Gnome contributors, as there were many talking to each other, and towards the end of the night dancing too.