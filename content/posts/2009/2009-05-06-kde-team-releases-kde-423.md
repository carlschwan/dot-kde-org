---
title: "KDE Team Releases KDE 4.2.3"
date:    2009-05-06
authors:
  - "sebas"
slug:    kde-team-releases-kde-423
comments:
  - subject: "fantastic job guys!!"
    date: 2009-05-06
    body: "You guys have already blown my mind with 4.2.2.!! Looking forward to 4.3 :)"
    author: "indishark"
  - subject: "RSS feed borked..."
    date: 2009-05-06
    body: "Hmmm the dot RSS feed seems to be broken.\r\n\r\nDoes this release include the fix for the memory leak aseigo posted?\r\nhttp://aseigo.blogspot.com/2009/04/plasma-and-memory-usage.html"
    author: "mootchie"
  - subject: "Statistics..."
    date: 2009-05-06
    body: "Some statistics for the changes in the KDE 4.2 branch between 4.2.2 and 4.2.3:\r\n\r\n* 482 commits made by 80 committers\r\n* 136 bugs fixed\r\n* 1055 file modifications in 854 unique files\r\n* 22 files added, 29 files deleted\r\n\r\nNote that these statistics are made using simple grep commands, and are only a rough indication. There are actually more contributors. For example, all those commits done by the translation teams only show up as \"scripty\" in the change logs. For the bug numbers, I only counted lines starting with \"BUG\" or \"CCBUG\".\r\n\r\nRead the full SVN change log online:\r\nhttp://pastebin.com/m685aa418\r\n\r\nDownload the full SVN change log:\r\nhttp://pastebin.com/pastebin.php?dl=m685aa418\r\n\r\nEnjoy!\r\n"
    author: "christoph"
  - subject: "Cuagmire"
    date: 2009-05-06
    body: "*giggly giggly*\r\n\r\n(sorry, couldn't hold myself :D)"
    author: "ianjo"
  - subject: "scripty and translators"
    date: 2009-05-06
    body: "Actually scripty commits are just merging translators work over .desktop files, translators do commit with their own user account, just that translations are on another subtree (stable/l10n-kde4), so you are not counting them at all."
    author: "tsdgeos"
  - subject: "Improving a good thing"
    date: 2009-05-07
    body: "I have to really say thanks and respect to you guys for the good work that you are doing. KDE 4.2 was good enough but you continue to improve it in spite of the fact that you are working so hard on KDE 4.3. Speaking of KDE 4.3, I just can't imagine what the release will be like because it's already very functional and as solid as a rock - excellent job!"
    author: "Bobby"
  - subject: "Well done KHTML and kdepim developers"
    date: 2009-05-07
    body: "I would thank you because you are care about bug-reports and you are working in the bug-fixing very hard. I know bug-fixing is not sexy as create something new. I really appreciate your work. \r\n\r\nBy the way the new feature in KHTML is amazing (i.e CSS3 Web Fonts )."
    author: "zayed"
  - subject: "Well said."
    date: 2009-05-07
    body: "The best argument against dropping KHTML in favor of WebKit is its steady development, which allows me today to use Konqueror on all sites that are relevant to me. Thank you for that, KHTML devs!"
    author: "majewsky"
  - subject: "Typo on http://www.kde.org/announcements/"
    date: 2009-05-08
    body: "It's \"KDE Community Ships Third Update....\" instead of Second update..."
    author: "rajputrajat"
---
<p>In our tradition of doing monthly service releases, today the KDE community <a href="http://kde.org/announcements/announce-4.2.3.php">presents the release</a> of 4.2.3, the third update to the KDE 4.2 series and just as cool as ever. This service update brings you bugfixes, performance improvements and updated translations, but no new features in order to minimize the risk of regressions. KDE 4.2.3 is a recommended upgrade for everybody currently running KDE 4.2.2 or earlier. The details of this release can be found in the <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php">changelog</a>.</p>

<p>The KDE release team will evaluate if changes in the 4.2 branch over the next months will warrant a KDE 4.2.4. The next feature release will be KDE 4.3 end of July and will most likely blow your mind. The feature freeze for KDE 4.3 has happened earlier this week and the KDE team will now stabilise this development version for another two month for you to enjoy a well-working, good looking, feature-rich and usable desktop.</p>