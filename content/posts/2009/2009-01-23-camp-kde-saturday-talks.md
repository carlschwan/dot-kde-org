---
title: "Camp KDE Saturday Talks"
date:    2009-01-23
authors:
  - "jpoortvliet"
slug:    camp-kde-saturday-talks
comments:
  - subject: "Akonadi"
    date: 2009-01-23
    body: "\"Akonadi is on its way to becoming a FreeDesktop.org project\"\n\nI thought that this effort had recently suffered something of a setback.  What's the inside word, here, PIMsters? :)\n\n\"The Kolab Konsortium is starting work on a prototype for the next generation of the KDE/Kontact Kolab client, which will be based on Akonadi. KDAB and Intevation have secured funding for this effort that will allow the team to progress Akonadi and KDEPIM overall in close collaboration with the wider KDE community throughout 2009.\"\n\nThis, however, is unreservedly good news - thankyou very much,  KDAB and Intevation - you've been a huge help to KDE :)"
    author: "Anon"
  - subject: "Re: Akonadi"
    date: 2009-01-23
    body: "Akonadi was rejected by FDO because there was no Qt implementation. A fact which apparently matters less when the proposed implementation is based on another toolkit.\n\nI personally feel that this is a lost opportunity for FDO from getting less and less relevant due to being a politicial instrument for others in the market, rather than serving useful value for us as a desktop project.\n\nWhen I see decisions like this, I'm wondering how much energy we should spend on FDO anymore (we did put a lot into it!), and how we can justify not spending this time and energy on creating the best platform in the world. \n\nClearly, FDO could work much, much better for us. Lately though, it has gone a long way from an asset to becoming a liability."
    author: "sebas"
  - subject: "Re: Akonadi"
    date: 2009-01-23
    body: "yes full ack,sebas !"
    author: "ch"
  - subject: "Re: Akonadi"
    date: 2009-01-23
    body: "I have always been wondering why KDE devs even care about FDO. In its current form, it's completely useless. I mean, it kind of ignores KDE/QT, so why bother? FDO is just a non-democratic waste of time..."
    author: "Marcel"
  - subject: "Cdash"
    date: 2009-01-24
    body: "What is the special advantage of CDash? Is it just for visualisation of builds?"
    author: "andre"
---
The program for the first two days at the Camp KDE meeting in Jamaica provided the attendants with a series of talks. In time, these will be available in video from but for now we have these short summaries of the talks. Read on for details.






<!--break-->
<h3>
Saturday
</h3>
<p>
Wade Olsen introduced us to the conference, outlining the reasons for this meeting. He spoke about the growth of the community, the large travel costs for contributors outside Europe to Akademy and the success of the Release Party at Google's place. Then Winston Wellington, the owner of <a href="http://travellersresorts.com">Travellers Beach Resort</a> and founder of the <a href="http://neetja.com">Negril Education Environmental Trust</a> told us how results of our work are affecting this place. This organisation is dedicated to educating the Jamaican people by bringing cheap computers and books to them. This year alone they opened several libraries with thousands of books and over fourty computers. As Wade concludes, it is often easy to forget what happens with the software we develop. We worry about text drawing, widgets and memory usage, here the results are applied and used to help improve the life of people. 
</p>
<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex;">
<a href="http://static.kdenews.org/jr/campkde/ade.jpg"><img src="http://static.kdenews.org/jr/campkde/wee-ade.jpg" width="300" height="200" /></a>
</div>

<p>
Adriaan de Groot, wearing a kurta, introduced us to a talk by Pradeepto about diversity in KDE. In the absence of Pradeepto, Ade and <b>Till Adam</b> replaced him. Moving through pictures of KDE PIM meetings Till showed changes in the KDE demographics from young, white male European students volunteering to work on KDE to a growing diversity of people from all over the world. This Jamaican meeting, the FOSS.in meeting and the upcoming <a href="http://dot.kde.org/1231333013/">Free Software event in Nigeria</a> are clear signs of the ongoing changes. Ade and Till detail the issues in our communication with people in other cultures and provide examples of the importance of their contributions. The presentation ends with an acknowledgement of the huge amount of effort and work Pradeepto has put into building a KDE community in Asia. He has taught us how we were effectively chasing away people from any other demographic than our own and how we can do better.
</p>
<p>
A very interesting product which came from the FOSS.in meeting is The KDE Handbook. This little book covers many aspects of KDE. From who we are and what we do to details of our development infrastructure, the way we work, release schedules, the technology we developed and the various KDE application suites. The marketing people showed interest in this, and will most likely try to work with the Indian community on keeping this handbook up-to-date.
</p>
<p>
Till Adam then gave a presentation about what is happening in PIM country. The work going on on Akonadi and the KDE PIM applications is very interesting. Akonadi is on its way to becoming a FreeDesktop.org project, and its libraries are used by more and more applications. Work on the applications like Akregator and KOrganizer is progressing very well and there was some good news. The Kolab Konsortium is starting work on a prototype for the next generation of the KDE/Kontact Kolab client, which will be based on Akonadi. KDAB and Intevation have secured funding for this effort that will allow the team to progress Akonadi and KDEPIM overall in close collaboration with the wider KDE community throughout 2009.
</p>
<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex;">
<a href="http://static.kdenews.org/jr/campkde/till_akonadi.jpg"><img src="http://static.kdenews.org/jr/campkde/wee-till_akonadi.jpg" width="300" height="200" /></a>
</div>

<p>
Next we listened to Sebastian Kügler talking about Bringing the Free Desktop Software to the Mobile World. A world with leaner hardware, smaller screens and different input tools. The work KDE is doing in the area of resolution independence, flexible and scalable layouts and new widgets is pushing ahead. There are many boundaries to cross, and Sebas details several of them, but also mentions how we are tackling these issues one by one. In many cases we do so in novel ways, bringing the desktop closer to the mobile space. The tight integration of power and network management introduced in KDE 4.2 is a good example of solutions important for mobile devices. Sebas discusses several things developers should keep in mind when writing applications targeting a variety of devices.
</p>
<p>
KDE-on-Windows was introduced and demonstrated by Hölger Schröder. He showed Amarok, Kate, Konqueror and various other applications working pretty well. The installer is easy to use and the integration in Windows is reasonably good. Even plasma works, and the biggest issue right now is stability. The team is looking for people willing to test and send a few patches to get the KDE-on-Windows effort to the next level.
</p>
<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex;">
<a href="http://static.kdenews.org/jr/campkde/holger.jpg"><img src="http://static.kdenews.org/jr/campkde/wee-holger.jpg" width="300" height="451" /></a>
</div>
<p>
The last talk of Saturday was by Bill Hoffman from <a href="http://www.kitware.com">Kitware</a>, about CMake and other building, packaging and testing tools. He introduced his company and the work they are doing. He explained how the roots of Kitware's quality software process lie in the 6-sigma efforts of General Electric. The goal of the process is to make errors more visible so they can be fixed and thus improve the quality of software. They have also developed and supported a powerful visualisation toolkit (VTK), a segmentation and registration toolkit  (ITK), as well as the CMake family of build, test and package tools which are available to anyone under the BSD license. They are under contract by both governmental agencies and companies for consultancy of their products. Bill detailed the various products Kitware has and what they do. In KDE we currently use CMake and to a lesser extent CTest. Bill also pointed us to several other interesting and open technologies we could have a look at.
</p>
<p>
In later conversations Bill, who has been active in FOSS for over 18 years, told us how well the Free Software business model has worked out for Kitware. Initially the goal of the company was to provide support for VTK and ITK and build proprietary vertical stacks upon them. But it turned out the development of these free libraries brought in more business than the proprietary solutions have, and now almost all of their products are available under the BSD. In the past year, after many years of indirect support as parts of other efforts, the CMake project has begun to generate direct consulting income. This ability to generate consulting income can be directly linked to the adoption of CMake as a build tool by KDE.  In one example, a fortune 500 company has begun the process of porting a large amount of internal software to CMake with Kitware as a partner in the effort. The fortune 500 company cited the use of CMake by KDE as one of main reasons for their discovery of CMake and Kitware.
</p>
<p>
Thanks to Casey Link for the photos!
</p>




