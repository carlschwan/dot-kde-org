---
title: "Nokia Sponsors KOffice Development for Mobile Devices"
date:    2009-10-13
authors:
  - "jospoortvliet"
slug:    nokia-sponsors-koffice-development-mobile-devices
comments:
  - subject: "Very interesting interview"
    date: 2009-10-14
    body: "This was a very interesting interview. This is a great opportunity for the Koffice team. \r\n\r\nI know that it's old news now, but I remember when Nokia bought trolltech a lot of people where worried about the future of Qt. It seems to me that Nokia has done a great job supporting the community and at the end I think it has been a good thing for KDE. I hope that in the future this kind of involvement between Nokia and KDE becomes more and more common.  "
    author: "Krul"
  - subject: "True"
    date: 2009-10-14
    body: "Yeah, there have been a few issues with communication and a change in priorities perhaps (I'm thinking Phonon, Qt-Multimedia etc) but there are bound to be changes and frictions as new people get involved, some of whom probably aren't familiar with how free software works.\r\n\r\nBuilding on the KOffice project and putting code back in to the main project SVN is a good sign and it seems like there has been some good communication with the KOffice devs - contrast that perhaps with the initial development of WebKit by Apple where (afaik) the KHTML devs were not kept informed - of course Apple were perfectly entitled to develop in that way but working together seems to make sense where possible."
    author: "swjarvis"
  - subject: "Sounds promising"
    date: 2009-10-15
    body: "I really enjoy seeing companies adopt more and more free software, and having a mobile office suite based on KOffice is another great step into that direction. \r\n\r\nMany thanks for the nice interview! \r\n\r\nFor me it also illustrates, that working with the community isn't always as easy as people who are already in the community might think - how do you get into a community, if you didn't start out in it and don#t have much time for \"idle\" chatting? (the type of chatting which doesn't yield immediate results in code but gets you in contact and can take the whole night)"
    author: "ArneBab"
  - subject: "Yep, working with a community"
    date: 2009-10-18
    body: "Yep, working with a community is definitely difficult sometimes. Even if you've been doing it for years."
    author: "jospoortvliet"
---
At the Maemo Conference in Amsterdam <a href="http://sureshchande.blogspot.com/">Suresh Chande</a> announced that Nokia has contracted <a href="http://kogmbh.com/">KO GmbH</a> to <a href="http://www.koffice.org/news/nokia-announces-ms-office-2007-import-filters-for-koffice/"> write a mobile office viewer using the KOffice libraries</a>. The presentation by Suresh was given with the Nokia N900 smartphone, using the new Office Viewer.

The improvements in KOffice have largely been in the libraries, on top of which a Maemo-specific GUI was written. KOffice became faster and more stable, and the various file import filters have been greatly improved. This includes the beginnings of MS Office 2007 import support. Thanks to this work the KOffice document viewer for Maemo will be able to properly read files created with a wider range of office applications, and all other users of KOffice 2.x will benefit.
<!--break-->
We had a short chat with Suresh who told us about the work on KOffice.
<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/Suresh.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Suresh_small.jpeg" width="400" height="224"/></a><br />
Suresh talking about KOffice on Maemo 5
</div>
<b>Hi Suresh, can you introduce yourself to the KDE community?</b>

Hi, I am Suresh Chande, born in Bangalore, India. After my study in Melbourne (Australia) I have spent 12 years at Nokia Research in Finland. Since 1 year I have been involved in the Maemo project and currently I am Technical Product Manager for the FRE Office (Fremantle or Maemo 5) suite in Maemo. I have never really been involved in FOSS communities myself but of course we have used Free Software for our research projects.

<b>You went though the various aspects of Mobile Office during your presentation. Can you elaborate a bit on this, e.g. why is this so important?</b>

Well, my presentation was called "mobile office". As my boss once said, office applications are the pen and paper of the future. Just as essential for the average citizen. These days, we type more than we write and we need office tools everywhere. You see laptops getting more popular - you can take them with you to the office, while traveling or to school. We intend to make office even more mobile with the N900 and the Maemo platform. Imagine combining this with a mobile projector - you have 2 devices which fit in your pocket yet you can give a full presentation!

Furthermore, this will also help eradicate the need for the dead trees you are using even now for this interview. You can just make quick notes on the go, on this device. Or finish your document in the train. Or practice your presentation while traveling. This is incredibly powerful!

<b>So in your presentation you mentioned this started after Marijn Kruisselbrink ported KOffice to Maemo at Akademy. How did it progress from there?</b>

Several things came together. After Marijn demonstrated KOffice on the N810 we knew it was possible. Later I had a chat with Thomas Zander who pointed me in this direction, and with KO GmbH there the pieces fell together.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/mobile future.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/mobile future_small.jpeg" width="400" height="224"/></a><br />
The future of presentations: all you need in your pocket.
</div>

Currently we have two parts in this project - one developing the Maemo UI on top of the KOffice libraries at Nokia, the other improving the KOffice libraries themselves, directly in KDE's subversion. However, we want to see office on Maemo mature more quickly, and we realize we need the community for that. So we will get our UI in the hands of the people as well - it will be published with Git within a couple of weeks.

The team working on the KOffice libraries focused on getting KOffice ready as a backend for the document viewer. This meant performance and stability work, but also features like improving the import filters for various file formats. And MS Office 2007 compatibility was completely missing, so we decided to do something about that. The KOffice community was interested in it but it was low on their priority list. So we have introduced the basics, there still is a lot to do. We'll continue to work on it, and we will be at the next KOffice sprint to discuss progress and other things.

<b>So how is working with the community going?</b>

We have mostly been working with some core community members, but personally I'd like to get involved with the wider community as well. This will certainly happen, as we are increasing our involvement with KOffice and opening up our own work. We want to take advantage of the expert knowledge within the community and we want to work with them to find the best way of improving KOffice. We need to be more open for that, create a relationship and an understanding between our team and the KOffice community.

<b>You showed us a demo video of KOffice on Maemo, can we find it online?</b>

<center><object width="445" height="364"><param name="movie" value="http://www.youtube.com/v/6AgJBEo4DD4&hl=en&fs=1&color1=0x006699&color2=0x54abd6&border=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/6AgJBEo4DD4&hl=en&fs=1&color1=0x006699&color2=0x54abd6&border=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="445" height="364"></embed></object></center>


<b>Do you have anything else to say to the KDE community?</b>

Well, yes. This application will at some point be unleashed on the millions who will hopefully buy Maemo-based devices over the next year. It needs to be ready for the end user and create a good impression. So any kind of testing and feedback is greatly appreciated! Of course, code contributions are very welcome as well. We will publish the application for Maemo in a couple of weeks in the Maemo's Extras or Testing repositories, and the code will be in Git. Of course the core KOffice code has been in SVN all along, so any contributions there help both us and KOffice.

<b>Thank you for the interview.</b>

My pleasure.