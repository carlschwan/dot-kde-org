---
title: "Akademy 2010 Dates Announced"
date:    2009-10-06
authors:
  - "sealne"
slug:    akademy-2010-dates-announced
comments:
  - subject: "Dates not that bad"
    date: 2009-10-06
    body: "The news item feels a bit negative about the dates that were chosen: it's true that we can never satisfy everyone. For my family, two weeks later would have been better, but I hear that all the bars are closed then, so the social aspect of the conference would suffer. Two weeks earlier and you're still in the middle of exams (for some universities in some countries)."
    author: "adridg"
  - subject: "Some notes on Tampere itself"
    date: 2009-10-06
    body: "The city itself is fairly compact. Well, that is that the whole Tampere economic region is about 60km wide, but the part that is relevant for Akademy is 2km by 1km, so it's all walkable in a half hour or less. It's fall now, so temperatures of 5 degrees make that refreshing -- in summer, 20 degrees warmer will make it really nice (unless it rains). Streets are wide, well provisioned for pedestrians, and there's a variety of pedestrian paths as well.\r\n\r\nThe city centre is on one side of the river; a wide selection of pubs is available, and restaurants are all over the side streets. Japanese, Thai, Spanish, kebab -- that's the stuff I've seen advertised, mostly, and we've been looking for vegan-compatible places (hence the emphasis on Eastern cooking). This too is where we've been looking at hotels; the one we had now was about 20 minutes walk from the proposed venue, but there are lots of locations closer as well. My room would sleep a family of four, or two or three individuals who don't mind being  a bit close. We have asked the organizing team to investigate cheap hostel-type accomodation as well as slightly more fancy hotels, so that there is something for everyone. "
    author: "adridg"
  - subject: "Some hints & links about Tampere"
    date: 2009-10-09
    body: "The bars being closed refers to the midsummer feast, a national holiday that leaves typically all towns and cities empty, not just bars. During midsummer, or juhannus in Finnish, Finns go to their summer cottages or some other places in the countryside to enjoy the summer. See http://en.wikipedia.org/wiki/Midsummer#Finland\r\n\r\nThe English pages of the city of Tampere are under a renovation but you can still see the content from the old site: http://www.tampere.fi/english_under_construction.html\r\n\r\nTampere has a very well working public transportation system. Going places by bus is both cheap and fast. A ticket valid for travelling for one hour (changing bus is possible) costs 2,50\u20ac. http://www.tampere.fi/tkl/english.html\r\n\r\nYou are all most welcome to Tampere and Finland! Feel free to ask questions - We'll try to answer them.\r\n\r\nElias Aarnio\r\nCOSS"
    author: "elias_a"
---
Last week Adriaan de Groot, Claudia Rauch and Kenny Duffus visited Tampere, Finland representing KDE. This gave a chance to meet face to face with members of the local team and talk about next summer's Akademy 2010 conference.

The representatives visited the Openmind Conference as part of the trip, which gave them a chance to find out about FLOSS in Finland and to meet and talk with  government representatives and people from local companies. The conference was an interesting event with more of a focus on FLOSS in business than development. At Openmind Adriaan gave a talk on software licensing issues for the <a href="http://fsfe.org/projects/ftf/">Free Software Foundation Europe's Freedom Task Force</a>.

Together with the local team possible dates for the conference were discussed. After taking in to account local constraints the dates of <strong>Sat 3rd - Sat 10th July</strong> were decided to be most appropriate, with Friday the 2nd July being the main day for arriving. As always it is unfortunate that no matter what dates the event is held on not everyone from around the world will be able to attend, but we still expect all to cross these dates in their agendas, if only to keep up with what is going on from IRC, blogs and other online media.

The programme committee will be announcing a call for talk submissions soon and more details about Akademy will be announced as they are confirmed. If you plan to attend and will need a visa please speak to the Akademy Team (akademy-team @ kde.org) as soon as possible so that the process can be started.