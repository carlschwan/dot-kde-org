---
title: "Krita Team Seeking Sponsorship to Take Krita to Next Level "
date:    2009-12-02
authors:
  - "jospoortvliet"
slug:    krita-team-seeking-sponsorship-take-krita-next-level
comments:
  - subject: "Donated a rather feeble \u20ac10."
    date: 2009-12-02
    body: "Donated a rather feeble \u20ac10. I'd give more, but I'm a bit tight on cash.\r\n\r\nAnything that can put KOffice on OpenSUSE/Kubuntu/Other KDE Distro's default installs, I consider a good thing!"
    author: "madman"
  - subject: "Madman -Thankyou!"
    date: 2009-12-02
    body: "Every donation makes a hero in my books, and 10 euros definitely helps. What makes a bigger difference to Krita is *whether* people support Lukas rather than how much (although multi-million euro donations happily accepted ;-)\r\n\r\nThanks once again for choosing to support Krita dna open source art creation. :)"
    author: "Bugsbane"
  - subject: "It's not!"
    date: 2009-12-02
    body: "It's not feeble, it's awesome!"
    author: "Boudewijn Rempt"
  - subject: "Fundraising"
    date: 2009-12-02
    body: "I remember the fundraising campaigns for Quanta Plus... It is not an easy job, but you are doing great. Good luck with it, and make a Krita that rocks."
    author: "andras"
  - subject: "Donate for Krita to KDE e.V."
    date: 2009-12-02
    body: "I will donate EUR 20 (with pleasure!). However, I do not want to use Paypal. Is it possible to donate for Krita via KDE e.V.? Moreover, donating to KDE e.V. would allow me to donate more (as donations are tax deductable).\r\n"
    author: "mkrohn5"
  - subject: "Not through the e.V."
    date: 2009-12-02
    body: "Through the e.V. is not possible, because this drive is about getting the money to pay someone to do work, instead of things like travel or hardware. If you mail me (boud@valdyas.org), we might be able to work something out nevertheless."
    author: "Boudewijn Rempt"
  - subject: "Nice saying"
    date: 2009-12-03
    body: "Albert Schweitzer said: The little that you can do is a lot. \r\n\r\nI am giving 10 Euro too.\r\n\r\nYours,\r\nKay"
    author: "kayonuk"
  - subject: "split from koffice"
    date: 2009-12-03
    body: "If the problem is trying to release a stable version every 4 months, then I think the thing to do is to is too split off from the KOffice suite. \r\n\r\nAlso, I want to see koffice to succeed. And this is not because I want a cheap program, because I just grudgingly purchased a student edition of photoshop recently. No I want something that will work on my platform of choice, and without having to e-mail a picture of my student ID to get my CD key.\r\n\r\nYes, KOffice has some very nice features. The GIMP has been missing adjustment layers, a feature that photoshop implemented a while ago and that I consider indispensable. Every time that I see that a new version of KOffice has been released, I immediately try out Krita hopping to see some speed improvements, but I've been mostly disappointed. "
    author: "reub2000"
  - subject: "huh?"
    date: 2009-12-03
    body: "Why do you think the 4-month schedule is a problem for Krita? I certainly didn't get that impression so the article shouldn't give it either ;-)\r\n\r\nYes, the speed is a big problem, as noted - and the first thing Lukas will work on."
    author: "jospoortvliet"
  - subject: "that's the idea"
    date: 2009-12-03
    body: "Improving the performance of Krita is the first goal of this project: we do recognize that Krita is too slow for many operations and have already done some research to find the bottlenecks. You can read all about it in the action plan for Lukas: http://wiki.koffice.org/index.php?title=Krita/ActionPlan."
    author: "Boudewijn Rempt"
  - subject: "First, krita team does need to present a plan"
    date: 2009-12-03
    body: "One of the advantages of Krita over Gimp was it's simpler and nicer UI. But I've been using Krita 2.1, and man, it is horrible!\r\n\r\n- selecting region is buggy as hell (you can't select pixels that are on the edges of the canvas) and the selection looks very strange\r\n\r\n- inserting text is ilogic, why there is not a button in the tools for text?\r\n\r\n- I can't figure out how to configure the eraser to clear just ONE pixel at time, and I want it erased 100%, not being more clear/translucit and having to click button 5 times to fully clear it. (if possible, the configure tool UI is a mess, because I could not find it after a lot of tries)\r\n\r\n- why showing a export dialog every single time I save a png? Once for session is good enought.\r\n\r\n- tool configuration aren't saved. Why should I configure the pencil brush to just draw one solid pixel every single time I open a new krita window?\r\n\r\nThat said, I hope Krita have a plan to address those issues, if true, then I'm willing to donate. But today, I don't see a plan to fix it's UI. SHould I post all those issues on bugs.kde.org or are you aware of that?\r\n\r\nSo, is there a plan for Krita future? People needs to see what they'll get if they put their money in :)"
    author: "protoman"
  - subject: "Yes, we have a plan"
    date: 2009-12-03
    body: "Yes, we have a plan. A fairly detailed one even. Lukas' action plan is here: http://wiki.koffice.org/index.php?title=Krita/ActionPlan. It has two phases: performance work and usability work. We've already received enough donations to start doing the performance work starting in January (Lukas first needs to finish his exams), and we are starting to get there for the usability work.\r\n\r\nThe general plan for 2.2 is at http://wiki.koffice.org/index.php?title=Krita/Roadmap22.\r\n\r\nHowever, this doesn't answer most of the points you make above. Some I agree upon, some I'm afraid we'll have to agree to disagree on.\r\n\r\n* selections: Sven Langkamp is working on this, so that should indeed be improved.\r\n\r\n* location of the text tool/shape. This is the result of a long discussion in KOffice as a whole. We don't have one text tool as such: we have text shapes that can be manipulation with a number of tools. For instance, the standard text shape has an editing tool and a paragraph settings tool. Then there is the artistic text tool that enables you to set type on a path. We chose to make all shapes available through a shape selector instead of putting them all as creation tools in the toolbox, because the toolbox quickly became very crowded. I'm afraid that this is settled for now, pending some future KOffice-wide usability and interaction review.\r\n\r\n* pixel eraser. It's a general issue with painting tools in Krita that they are meant for painting, not for changing the value of single pixels. There is a bug for the pencil brush engine to make neat, precise, aliased lines. Together with the \"erase\" blending mode, this should do what you want, when it is fixed. See bug https://bugs.kde.org/show_bug.cgi?id=148207.\r\n\r\n* showing the export dialog. Well, in general, the work flow we aim at is for someone working on a single image for a longer period of time, saving it as .kra often, and only in the end exporting it. Krita isn't a photo manager like Digikam. Still, it's a valid point -- so we'll have to think about it.\r\n\r\n* tool configurations not being saved. That's actually my fault. Back in 2003, Krita had some rudimentary code to save tool configurations to the settings file, but that didn't work, and I kept getting confused by gimp remembering my settings when I started it again, so I concluded it was a bad idea and removed the non-working code. There is a bright side here, though, because we're working on making it easy to save and restore brush settings, which would certainly meet the requirements you state above: you will be able to define a one-pixel hard eraser \"preset\", save it and retrieve it from a combobox. It will work a bit like Corel Painter's brush selection works now.\r\n\r\nHope that clears up your questions!\r\n"
    author: "Boudewijn Rempt"
  - subject: "Clearly there's demand"
    date: 2009-12-03
    body: "Well more than three quarters of your financial target in four days is great going! Clearly the community is right behind this project.\r\n\r\nIts great to see us coming together to provide resources where they're needed!"
    author: "samtuke"
  - subject: "selection"
    date: 2009-12-03
    body: "Btw, the particular point you made about selecting pixels on the edge of the canvas, if you can reproduce that reliably, could you add a bug for that to bugzilla? I cannot reproduce that myself.g"
    author: "Boudewijn Rempt"
  - subject: "good work"
    date: 2009-12-03
    body: "i never used krita, but i gave money nevertheless :)\r\n\r\n(not because the app is not good, but i like the cause...)\r\n\r\nkeep up the good work!"
    author: "wintersommer"
  - subject: "Well, if the linked plan is"
    date: 2009-12-03
    body: "Well, if the linked plan is not enough, then perhaps I can offer a few words of reassurance.  I've been hanging around KDE for almost 10 years now, and Boud is someone I'd trust 100%.  If he says it's good, that's good enough for me to pledge my \u20ac10.\r\n\r\nThe KOffice team have past experience with this sort of sponsorship, for example through KO and work on KWord and the ODF filters, so they know how to make it work."
    author: "odysseus"
  - subject: "Should be done more often"
    date: 2009-12-03
    body: "Users are clearly willing to pay for good features. Maybe more KDE devs should seek such sponsoring. \r\n\r\n- Advance the semantic desktop. \r\n- Expose BtrFS features in KDE.\r\n- ...\r\nI think the list goes on and on.\r\n\r\nMaybe that is a new business opportunity for KDAB. I know the numbers have to be much higher, but still. For really popular KDE features the community would be willing to easily spend 10K I guess."
    author: "kragil"
  - subject: "Good Idea"
    date: 2009-12-04
    body: "I think it's a very good idea to raise money to pay Luk\u00e1\u0161 to improve Krita.\r\n\r\nI don't really want to use KOffice (I like OpenOffice more), but Krita is an exception. It's the best. Gimp is much too hard to use.\r\n\r\nI am a bit out of money so I can't donate much, but it looks like by tomorrow you will have enough money raised anyway."
    author: "Monika"
  - subject: "Willing to do similar thing for other projects"
    date: 2009-12-04
    body: "Performance, stability and usability issues are not always the most fun things to work on, but someone has to do it.\r\n\r\nI'm not a Krita user, but with my rather smallish donation I want show that I support the cause, and that I'm willing to do it for other projects as well. If there's a noticeable improvement in e.g. performance, it's worth every cent.\r\n\r\n(Just don't do it too often, a poor student like me also needs food. ;)"
    author: "Hans"
  - subject: "Finished!"
    date: 2009-12-04
    body: "And with \u20ac40 left over to spend on champaign... ;)"
    author: "madman"
  - subject: "It obviously worked this time"
    date: 2009-12-04
    body: "It obviously worked this time with a clear plan, a resource in place a known set of goals etc etc. It would be interesting to see if this model however would continue to work if it became widespread. \r\n\r\nI could imagine a scenario in which 100 different ideas were put forward for sponsorship, each attracting a few donations here and there but none of them reaching the critical mass needed to actually allow someone to dedicate substantial chunk of their life to a given piece of work...\r\n\r\nThis might be one of those practices that needs to be infrequent to remain effective (I'm not trying to come off as cheap here, I already donated $30 ;)\r\n\r\nedit: a thought just occurred, what might work is to have a regularly spaced donation drive, say every 3 months for example; people could submit proposals, which could be voted on. The winning proposals could then be subject of the next round of donations. \r\n\r\nProposals would have to be cleared by the project maintainer (no point coding up something that won't be merged) and should be of the standard set by the Krita team, with clear goals, an available and willing coder etc."
    author: "borker"
  - subject: "Cool huh...\nIf ppl continue"
    date: 2009-12-04
    body: "Cool huh...\r\n\r\nIf ppl continue to donate, the Krita team can keep Lukas working for another few months!"
    author: "jospoortvliet"
  - subject: "Well, it yes helped to solve"
    date: 2009-12-04
    body: "Well, it yes helped to solve my questions, and I am kind of amazed that you have a plan! 90% of open-source programs out there (and that includes kde and plasma) do not have a clear vision of what they want, but you do!\r\n\r\nCongratulations and keep the great work :)"
    author: "protoman"
  - subject: "he"
    date: 2009-12-04
    body: "if we can get a rate of \u20ac1000 a month we can keep him employed forever. :)\r\n\r\n"
    author: "wintersommer"
  - subject: "It seems like a lot of the"
    date: 2009-12-05
    body: "It seems like a lot of the work is going into brushes. The slowness that I was talking about was the preview for effects like levels or curves. Also, if I hear of anything interesting during the time when Lukas is hacking away, then I might have to build from subversion."
    author: "reub2000"
  - subject: "Tried before"
    date: 2009-12-08
    body: "There have been (and probably still are) schemes like this before, where you could pledge an amount of money to have certain issues addressed or features implemented in KDE software. I'm pretty sure there was something like that for KDE-PIM. Unfortunately, it did not work. At least not for me. \r\n\r\nI think that in general, things like this could work, but you probably should not expect to have it raise enough to work full time on it. I am happy to see that this time around, it seems to work out pretty good! I think that also has to do with the clear focus and the publicity around it.\r\n\r\nI'm sure it also feels very good for the developers behind Krita to see this work out so well! It must be very satisfactory to see that in a matter of days, the target was met. That does mean the community appreciates your project, I'd say.\r\n\r\nedit: this should have been a reply to Thu, 2009/12/03 - 9:32pm \u2014 kragil: \"Should be done more often\"."
    author: "andresomers"
  - subject: "that is really nice.i will"
    date: 2009-12-21
    body: "that is really nice.i will try to contribute too <a href=\"http://steamcleanersblog.blogspot.com/\"></a>"
    author: "itcoll"
---
At the KOffice meeting in Oslo the Krita team had a meeting to put the finishing touches to an ambitious plan. On hearing about it the Dot managed to lure two of the Krita developers in a separate room to question them and find out what was going on. Nobody got seriously hurt in the process.

We spoke with Krita developers Boudewijn Rempt and Lukáš Tvrdý.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href=" http://dot.kde.org/sites/dot.kde.org/files/krita-enki-sumi-coolness.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/krita-enki-sumi-coolness_small.jpg" width="400" height="237"/></a><br />Variety of Sumi-e brushes by Lukáš</div>

<h2>User Experience</h2>

<b>Jos</b>: I sensed some excitement from the Krita developers, what do you guys have in store?

<b>Boudewijn</b>: Well, we realized while working on Krita 2.1 that it was getting to that point where an end user could basically work with it. So we were looking for the next challenge.

<b>Lukáš</b>: It was about that time when I got in contact with <a href="http://www.davidrevoy.com/">David Revoy</a>. He's the concept artist who has been working with the Blender team on Project Durian: their latest open source movie project. I asked him for his opinions on Krita to get some feedback from a professional. I like for people to use my applications, and David has plenty of experience with various tools like the Gimp and MyPaint. His opinions seemed very valuable to me for making Krita ready for actual users.

<b>Jos</b>: And? Did he have experience with Krita?

<b>Lukáš</b>: Yes, actually, he had tried it a while ago. It didn't work for him, even though he liked our interface a lot. The main issue was not really the lack of great features and tools but the slowness of the painting.

<b>Boudewijn</b>: But he was willing to provide us with feedback on the issues he bumped into. So that's when we decided we should put a strong focus on getting Krita ready for him. If he can work comfortably with Krita, so will many other users, both casual users and professionals...

<h2>Challenge</h2>

<b>Lukáš</b>: That was a challenging goal, but also very exciting. I had been working on a bunch of cool brushes, using the tablet I've been loaned after <a href="http://dot.kde.org/2007/05/28/full-tilt-krita">a previous fundraiser</a> and I thought I could shift my focus a bit.

<b>Boudewijn</b>: Yes, but frankly, the issues David bumped into would need more than a few casual hours a week to fix them properly. The speed issues for example need deep changes in the code. This could take a long time especially with Krita and KOffice being a moving target all the time.

<b>Lukáš</b>: So I mentioned to the team that since I'll have a slow period the first three months of the year I might be able to spend a serious amount of time on this. In 3 months I could certainly fix the performance and usability issues David encountered.

<h2>Pledge</h2>

<b>Boudewijn</b>: But of course Lukáš would have to find a job to be able to pay his bills - after all, he's a student. So we decided it was worth it to try and <a href="http://krita.org/index.php&option=com_content&id=20">raise money</a> to let Lukáš work on Krita full-time for three months!

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href=" http://dot.kde.org/sites/dot.kde.org/files/krita-spray-enki-tree.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/krita-spray-enki-tree_small.jpg" width="400" height="233"/></a><br />Krita Art</div>

<b>Jos</b>: So that's the plan - raise money and get Lukáš to work on Krita full-time?

<b>Boudewijn</b>: exactly. Cyrille Berger will go and meet David Revoy next week to talk about the issues in Krita. David has agreed to help us get Krita to the point where a professional like him can use it comfortably. It'd be really cool if for example the Blender artists can use Krita for their next piece of art!

<b>Lukáš</b>: This is only possible because we've been focussing on fixing bugs and improving stability in KOffice so much lately... By restraining ourself to that instead of working on new cool features, we've created something which is usable. But this brings out the performance issues and missing functionality.

<h2>Lots of work ahead</h2>

<b>Boudewijn</b>: With David's help we have made a specific list of tasks to be accomplished, with a timeline. We'll of course run the results by David every now and then. Lukáš will know exactly what he's supposed to do and in 40-80 hours a week he can get them done.

<b>Lukáš</b>: wait, 80 hours? We didn't talk about...

<b>Boudewijn</b>: Don't worry, if you don't manage to finish a target during the week, the weekend has another 48 hours!

<b>Lukáš</b>: Well, hum. Anyway, we have a realistic plan. We'll start on the performance issues, the fixes for that should make it into the KOffice 2.3 release. Then 4 months later, the 2.4 release will contain the usability and feature improvements - all of this should be in the hands of the users around the end of August or September.

<b>Jos</b>: Next year?!?

<b>Boudewijn</b>: Hence the 80 hours.

<b>Lukáš</b>: It is doable. I'm not alone, the other Krita developers will of course continue working on various other improvements as well.

<b>Boudewijn</b>: And we know what Lukáš can do, he's very good. He has shown that in the earlier summer of code projects and his work with the tablet support, the new brushes, 3D cursors and the infinite canvas. You can look up the code if you like.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href=" http://dot.kde.org/sites/dot.kde.org/files/krita_spray_bitmap.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/krita_spray_bitmap_small.jpg" width="400" height="235"/></a><br />Bitmap Spray (<a href="http://lukast.mediablog.sk/log/?p=121">more here</a>)</div>

<b>Jos</b>: Sounds good. So where does the money come from? I've seen Lukáš eat...

<b>Boudewijn</b>: Hehe... Well he lives in the Slovak Republic. Life's not that expensive over there. We aim to raise about 2000-3000 euros. And the Krita team has decided to put a bit of personal money in as well. If we manage to raise more money, it'll be used for sponsoring Lukáš for a longer period.

<b>Jos</b>: So this is really a team plan?

<b>Boudewijn</b>: Yes, absolutely, everybody's on board here. We all see this as a great opportunity to raise the bar for Krita and move to the next level. For us it is about enabling a team member to work on a specific set of improvements full time to the benefit of the whole product. The money raising will probably have to be an ongoing effort but I think the community will help us out here.

<b>Lukáš</b>: yeah, everyone really wants to do this. The other team members can can continue to work on the cool things they have been working on, I'll be doing this as if it's a job - do the boring optimizing and usability work. It'll still be a lot more fun than getting a random student job around here for three months...

<b>Boudewijn</b>: And it's good for his resume, I suppose.

<h2>Why donate?</h2>

<b>Jos</b>: So, sum it up for us, why should the community support the Krita team with this plan?

<b>Lukáš</b>: If you want to use krita on a professional level!

<b>Boudewijn</b>: It is cheaper than a licence of Corel Painter. Not that I would object to any donations the size of a Corel Painter license...

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href=" http://dot.kde.org/sites/dot.kde.org/files/P1080102.JPG"><img src="http://dot.kde.org/sites/dot.kde.org/files/P1080102_small.jpg" width="454" height="303"/></a><br />Will code for food!</div>

<b>Boudewijn</b>: And it can fill in a gap in the Free Software world. MyPaint is here for quick sketching, and Gimp is real good at image manipulation but we don't have a good digital painting appliation with all the features artists need. We have been working on this many years and we're close - this is the chance for the big leap forwards. If you care about this, a donation will make a big difference. We can do this!

<b>Jos</b>: Sounds like a plan. Thanks for the interview and of course for the work on KOffice and Krita!

<i>You've all read it, go on to the Donations page and help make Free Software provide a real alternative for digital artists! Be sure to notify others and spread the word...</i>

<a href="http://krita.org/index.php&option=com_content&id=20">Read more on the Krita site</a>.

<a href='http://www.pledgie.com/campaigns/7221'><img alt='Help raise Krita to the next level and make a donation at www.pledgie.com !' src='http://www.pledgie.com/campaigns/7221.png?skin_name=chrome' border='0' /></a>
<br />