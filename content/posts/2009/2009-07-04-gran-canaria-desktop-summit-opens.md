---
title: "Gran Canaria Desktop Summit Opens"
date:    2009-07-04
authors:
  - "jospoortvliet"
slug:    gran-canaria-desktop-summit-opens
comments:
  - subject: "Are ther some videos from"
    date: 2009-07-04
    body: "Are ther some videos from those talks?"
    author: "beer"
  - subject: "Nice"
    date: 2009-07-05
    body: "As they say, \"videos or it didn't happen\" :)\r\n\r\nAs for Maemo, is it really a surprising announcement? It was all over the grapevine for quite some time now.\r\n\r\nCheerio and thanks for making us that are not there not left out :)"
    author: "ivan"
  - subject: "Video recording was going on,"
    date: 2009-07-05
    body: "Video recording was going on, unfortunately the first three keynotes were not recorded. Luckily there are notes up there (also thanks for Sebas for his contribution in that area).\r\n\r\nAbout the surprise, well, we knew it was going to happen since previous akademy at least, but now it's official ;-)\r\n\r\nAs one of the interviewies said, Nokia has chosen the best technology..."
    author: "jospoortvliet"
  - subject: "The location is the 'Alfredo Kraus Auditorium'"
    date: 2009-07-05
    body: "and not 'Albert Kraus Auditorium'. Alfredo Kraus was a famous Canarian opera singer.."
    author: "Richard Dale"
  - subject: ":S"
    date: 2009-07-05
    body: "Indeed. And it's a shame google didn't do the same with chrome :S"
    author: "jadrian"
  - subject: "Why no webcam?"
    date: 2009-07-05
    body: "I don't need a superduper edited video + slides. Just plug some embedded webcam into a router and you are nearly done.\r\n\r\nI still don't get why big events like the GCDS don't offer something like this :("
    author: "kragil"
  - subject: "chrome and gtk...."
    date: 2009-07-06
    body: "apparently google built chrome with gtk because thats what the developers were familar with. \r\n\r\nthey've left it open so it could be rebuild around qt if desired\r\n\r\nhttp://linux.slashdot.org/story/09/05/30/1740205/Harsh-Words-From-Google-On-Linux-Development\r\n\r\n"
    author: "thirtytwobit"
  - subject: "See, that really gets me:"
    date: 2009-07-07
    body: "See, that really gets me: they complain about all the different available toolkits and how much of a nightmare it is for developing on Linux to make a program look consistent all-round, but they used Qt for Google Earth - so why not for Chrome?"
    author: "madman"
---
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex;">
<img src="http://farm3.static.flickr.com/2538/3688167692_6fa08877ea_m.jpg" width="240" height="180" /><br />
Robert Lefkowitz's Keynote
</div>
Today the Gran Canaria Desktop Summit has started, bringing KDE and Gnome developers together in the biggest conference of its type.  It is situated on the beautiful Atlantic island of Gran Canaria and housed in the spectacular Alfredo Kraus Auditorium which dominates the skyline of Las Palmas, capital of Gran Canaria.  The conference was opened by a series of talks from various people in the Canaries local government and the organisation. After that the keynotes started with star speakers and impressive announcements including an Open PC developed by the community and Maemo switching to Qt. Read on for an impression of the GCDS!
<!--break-->
Yesterday (Friday) from 16:00 onwards registration for the GCDS opened. Hundreds of community members entered the foyer, received their badge and the sponsored items. Everyone now has a big green <a href="http://www.qtsoftware.com">Qt</a> towel, a <a href="http://www.google.com">Google</a> sports drink bottle and a <a href="http://www.nokia.com">Nokia</a> mini SD card reader. Importantly we received coupons for morning coffee and free ice cream sponsored by <a href="http://www.intel.com">Intel</a>.

After the registration closed, a <a href="http://dot.kde.org/2009/07/04/canonical-party-welcomes-gran-canaria-desktop-summit">Canonical-sponsored party</a> began. Hundreds of attendants gathered on the large area behind the conference building.  The beach is just outside the auditorium, and during the day is full of people enjoying the heat. Many of us have been walking along the beach, enjoying the view, having some food there and even having a swim. At night, we had beer. There was supposed to be free beer from 9 to 11 but we obviously did not drink it fast enough and it lasted until after midnight. Still many were unhappy when the beer stopped flowing - luckily the bar continued to be open.

Next morning at 10 the conference started. It was opened by a group of government and local officials and representatives of KDE and Gnome:
<ul style="margin-left: 1em">
<li>Fernando Navarro, Cardoso Counselor for the government of innovation, employment and human resources for the city of Las Palmas in Gran Canaria</li>
<li>Jose Regidor, Rector for the University of Las Palmas in Gran Canaria</li>
<li>D. Juan Junquera Temprano, Director of the State Department of IT Society and Telecommunication</li>
<li>D. Roberto Mereno Díaz, Advisor to the government for tourism, technological innovation and international commerce for Gran Canaria.</li>
<li>Adriaan de Groot, Vice Chairman of KDE e.V. Board</li>
<li>Behdad Esfahbod, President of the board of directors for Gnome</li>
</ul>
Each welcomed us and thanked the many participants for the work they have been and will be doing. After these short introductions we moved on to the keynotes.

The first keynote of the day was held by engineer and speaker Robert Lefkowitz. Robert drew a wider picture of how software, and especially Free Software fits into our culture. Robert's main point was probably that Free Software is best viewed as literature, with the side effect of it also being able to get work done. This has important implications for the view we have on software patents and copyright.

Walter Bender, executive director of Sugar Labs, spoke about the Sugar children interface and why they started to work on Sugar. He spoke about sharing, cooperation and the plans and wishes the Sugar community had for this summit. Bender related the development of technologies such as Sugar to the learning process of children. His talk was directed at Free Software developers and asked them to join the group hacking on Sugar. His presentation was "programmed" in a Sugar learning software environment. At the end of Bender's talk, he quickly demonstrated a small application he wrote using this system which paints graphical artifacts on the screen.

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex;">
<img src="http://farm3.static.flickr.com/2454/3688174206_e6aafe3716_m.jpg" width="240" height="180" /><br />
Free Software Love
</div>

Third was Richard Stallman, founder of the Free Software Foundation. He spoke about what Free Software is (Software that respect the users freedom and the social solidarity of the user's community). Proprietary software keeps users divided and helpless (they are not allowed to share and they do not have source code so they can not control it). Free software is defined by 4 freedoms: freedom 0 gives the user the freedom to run the software where and whenever he or she wishes. Freedom 1 allows users to study source code & change it. Freedom 2 is the freedom to share the product. Freedom 3 is the freedom to distribute the improvements and changes to anyone who wants them. Thanks to these freedoms, Free Software is under control of its users, both individually and collectively. It is developed democratically, which is what it is all about.

Stallman continues to talk about the history of KDE and Gnome, the two Free Desktops who are at this Summit. Back in 1997, KDE used Qt, a semi-proprietary toolkit which was free of cost to use with Free Software. Stallman and the Free Software foundation considered this a danger to the freedom of the users, and started two projects to do something about it. One was project Harmony who's goal was to rewrite Qt as Free Software. The second project was Gnome, a replacement for KDE. This had the intended effect, and in 2000 Qt was released as Free Software by the company behind it. Richard continues to talk about the current dangers to Free Software and freedom. Sometimes you have to use  what is not the best thing technically, so he argues.

Richard finally mentions how the Spanish government is planning to hand out computers with Windows to school children. Even worse, these computers use Digital Restrictions Management for books distributed on these computers. According to Richard this plan is evil. Teaching children that taking away their freedom is good seems like a bad move by the government.

After a short introduction to the Church of Emacs, Stallman auctioned a Gnu to the audience for benefit of the Free Software Foundation.

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex;">
<img src="http://farm3.static.flickr.com/2557/3687366643_223da146f5_m.jpg" width="240" height="180" /><br />
Quim Gil's Keynote
</div>

After a break, one more keynote was held by Quim Gil from the Maemo community. He made a very surprising announcement. Showing an architecture diagram with the current stack for Maemo with Qt in the community supported area. Then a button was pushed, and Qt moved to the foundation stack and the old toolkit moved into the community support area. In a later interview, Quim Gil and Aron Kozak from Nokia expanded on the plans. The upcoming release of Maemo, Maemo 5, will be based on GTK. It is currently being stabilised and finished. The following release, codenamed Harmatan, will be build around Qt. Nokia wants to offer third party developers a common platform to build upon on both Symbian and Maemo, attracting more contributions and applications. For KDE, this move will mean a huge potential userbase, and the Nokia engineers have been watching KDE technology closely for opportunities for closer integration and cooperation.

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex;">
<img src="http://farm3.static.flickr.com/2609/3687383971_3e75a110d9_m.jpg" width="240" height="180" /><br />
A Spanish Band Played Us Out
</div>

The rest of the afternoon was filled with 5 minute cross-desktop lightning talks.  Frank Karlitschek from <a href="http://www.opendesktop.org">Open Desktop</a> announced <a href="http://open-pc.com/">Open-PC</a>, a project to create "A Linux PC from the community for the community".  He said that they currently have one hardware manufacturer as a partner to build the laptops.  The website currently has a questionnaire to take suggestions on the software to include.

Matthew Paul Thomas, usability man from Canonical, gave an amusing talk highlighting common usability problems in applications.  Top of his list was applications with a silly name, first impressions count.  Alex Spehr gave two talks.  First covering KDE's BugSquad, which she highlighted as an excellent way for contributors to get into KDE.  She also talked about the improvements in DrKonqi and how this had improved the quality of submitted bug report.  Laura Dragan gave talks on her SemNotes application which uses Nepomuk to create linked notes, and the Konduit visual RDF application.

Videos of most of the talks should be available for download in the next day or so.  <a href="http://www.flickr.com/photos/jriddell/sets/72157620955618284/">Photos by Jonathan Riddell</a>.