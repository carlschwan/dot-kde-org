---
title: "Akademy Awards 2009"
date:    2009-07-06
authors:
  - "jriddell"
slug:    akademy-awards-2009
comments:
  - subject: "minor typo"
    date: 2009-07-07
    body: "\"The conference was going to make a big different to the attitudes of people to Free Software in the Canary Islands he said.\" - difference;\r\nalso, i'd put a comma before \"he said\", unless the sentence is rewritten.\r\n\r\nand is that really aseigo in the first picture ? another image change ? :)\r\n\r\n"
    author: "richlv"
  - subject: "re: minor typo"
    date: 2009-07-07
    body: "\"and is that really aseigo in the first picture ? another image change ? :)\"\r\n\r\nLooks more like sebas to me, though I've never seen either of them \"in the flesh\", as it were :)"
    author: "SSJ_GZ"
  - subject: "Is Jonathan drunk or joking :) ?"
    date: 2009-07-07
    body: "I'm not a KDE member, but I still know the difference between Aseigo und Sebas :)"
    author: "kragil"
  - subject: "nice to see that some of the"
    date: 2009-07-07
    body: "nice to see that some of the awards goes to non coders/devs (to the usability team). "
    author: "beer"
  - subject: "Augustin's award"
    date: 2009-07-07
    body: "What award did Augustin Benito Bethencour get, and what for?"
    author: "IndigoJo"
  - subject: "Re: Augustin's award"
    date: 2009-07-07
    body: "He is (one of ?) the main organizer of GCDS/Akademy.\r\n\r\nAlex\r\n"
    author: "aneundorf"
  - subject: "Congratulations !"
    date: 2009-07-07
    body: "Congratulation to all three, the jury again made a very good choice ! :-)\r\n\r\nFinally David receives the award, it was about time :-)\r\n\r\nAlex\r\n"
    author: "aneundorf"
---
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 400px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/DSCN3857-wee.JPG" width="400" height="300" /><br />
Award Winners David Faure, Celeste Lyn Paul and Peter Penz accepted by Sebastian Trueg
</div>
The Akademy Awards for 2009 have been announced, celebrating the best of KDE contributors.  As always the winners are chosen by the winners from the previous year.  Read on for the winners.
<!--break-->
<br clear="all" />
<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 300px;">
<img src="http://dot.kde.org/sites/dot.kde.org/files/aaron.jpg" width="300" height="359" /><br />
Aaron Seigo, as played by Sebas, one of last year's winners presenting an award
</div>

<h3>Peter Penz</h3>

Peter Penz is the developer of Dolphin and won the award for best application category.  Unfortunatly Peter was not at Akademy so his award was accepted by Sebastian Trueg on his behalf.
<br clear="all" />
<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 373px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/DSCN3841_0.JPG" width="373" height="305" /><br />
David Faure accepts his prize
</div>

<h3>David Faure</h3>

David Faure is one of KDE's longest serving developers.  He won the Jury Selection category for greatest service to KDE.  

<h3>Celeste Lyn Paul</h3>

Celeste is a member of KDE's Usability team and a contributor to OpenUsability.org.  She accepted the award for greatest non-application contribution to KDE.  She said she was pleased at the community's acknowledgement of the work of the usability team and how it proved that improvements were noticed and appreciated.

<br clear="all" />

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex">
<img src="http://dot.kde.org/sites/dot.kde.org/files/DSCN3850_0.JPG" width="332" height="281" /><br />
Augustin
</div>
<h3>Augustin Benito Bethencour</h3>

The final award was given to the Akademy organiser Agustin.  In his acceptance speach he said he accepted the award on behalf of the whole organising team.  The conference was going to make a big difference to the attitudes of people to Free Software in the Canary Islands he said.