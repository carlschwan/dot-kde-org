---
title: "openSUSE KDE Community Week Brings Distro And KDE Closer"
date:    2009-05-18
authors:
  - "Bille"
slug:    opensuse-kde-community-week-brings-distro-and-kde-closer
comments:
  - subject: "Nice :)"
    date: 2009-05-18
    body: "Nice work, I'm looking forward to the results of these meetings :)"
    author: "vdboor"
  - subject: "Syntax error?"
    date: 2009-05-18
    body: "\"The week began with a lively chat session discussing the Andrew Stromme and Marco Martin from the Plasma dev team paid #opensuse-kde a visit\".\r\n\r\nThere seems to be a syntax error, and the link on \"Andrew Stromme\" is quite weird."
    author: "majewsky"
  - subject: "Fixed!"
    date: 2009-05-18
    body: "Thanks!"
    author: "danimo"
  - subject: "suse"
    date: 2009-05-18
    body: "As a user I always was very passionate about SuSE, but then Novell took over and steered the distribution into a totally different direction. OpenSuse seems to contain the better spirit.\r\n\r\nWhat I appreciate in particular is the OpenSuse weekly newsletter and that the OpenSuse community actually cares for KDE and its stability: https://features.opensuse.org/305783\r\n\r\nEBN-wise KDE PIM always looks like a little code quality stepchild as the result of the fast progress\r\nhttp://www.englishbreakfastnetwork.org/krazy/?component=kde-4.x\r\n\r\nWhich date is scheduled for the conference?\r\n"
    author: "vinum"
  - subject: "Broken link"
    date: 2009-05-19
    body: "#opensuse-kde and let us know how the migration to >>>Akonadi<< is...\r\nLink is broken\r\n\r\nAlso, where could I find aconadi-console, Mailody etc packages? :)\r\n\r\np.s. openSUSE 11.1"
    author: "lukas"
  - subject: "Kiosk Tool?"
    date: 2009-05-20
    body: "In reference to the \"things involving KDE systems administrators\" (sorry I can't copy the exact text - Opera on a Blackberry doesn't do that), does that mean we'll eventually get a Kiosk Tool for the 4.* series?  I can honestly say that I didn't use the 3.* much, but it was nice/handy to simply know it was there.\r\n\r\nM."
    author: "Xanadu"
  - subject: "Consider more RC's before Kde 4.3.0"
    date: 2009-05-20
    body: "Consider more Release Candidates in Kde 4.3 development\r\nFor kde , they should consider a longer release cicle, for example making RC2 RC3 and so until gets solid rock, and then RTM or something like, that.\r\n\r\nMaking only one RC1 will cause KDE 4.3.0 not be solid rock system as many of us expect.\r\n\r\nAnyway Kde 4.3 beta 1 is in right direction, = D"
    author: "marcomelo"
  - subject: "Chameleon"
    date: 2009-05-21
    body: "That lizard is a Chameleon!\r\n\r\n"
    author: "JRT256"
  - subject: "They are in the KDE"
    date: 2009-05-21
    body: "They are in the KDE repositories. (Mailod\u00ed is in playground.) Find packages at http://packages.opensuse-community.org ."
    author: "gd"
  - subject: "Re: Consider more RC's"
    date: 2009-05-21
    body: "You do not need to post your comment on KDE 4.3's release schedule in multiple posts, but anyway I'll copy my answer for reference:\r\n\r\nIt's absolutely impossible to kill all bugs, because, for example, developers lack resources to reproduce many bugs that depend on configuration.\r\n\r\nAlso, some bugs emerge from programs being in a transition state to a better solution (one example being System Settings which did not yet get Administrator Mode because they want to do it the right way with PolicyKit).\r\n\r\nA similar argument applies for some bugs and also some crashes: Developers might already know how to fix them, but they would possibly introduce even more bugs through a hacky solution.\r\n\r\nSo there can be numerous reasons why a bug does not get fixed in the first place. The last and most important reason is lack of manpower, but we cannot solve that. There is a saying that \"assigning more workers to a delayed project delays it further\", which is quite true: New developers have to find their way into the program, which may consume the time of the old devs who have to explain things to them, or the new devs might break stuff without noticing, thereby introducing even more bugs.\r\n\r\nAs you see, there is no way to get bug-free software with just a longer release cycle. Our release cycles already consist of one third feature freeze. Even more could be counterproductive, as it lowers the motivation of the developers significantly. I hope you understand this reasoning."
    author: "majewsky"
  - subject: "it's already there"
    date: 2009-05-21
    body: "KDE 4.3 Beta 1 is already rock solid. I have been using 4.3 for about 6 weeks now and have been having very few minor issues with it. it's really the best in the 4-series to date."
    author: "Bobby"
  - subject: "openSuse and KDE "
    date: 2009-05-21
    body: "I have tried quite a lot of \"KDE\" distros but I always come back to openSuse which in fact delivers the best and most polished KDE. It's really nice to see that the openSuse guys are working so close with the KDE guy to make a very good distro even better and give end user the ultimate KDE experience and enjoyment."
    author: "Bobby"
  - subject: "opensuse sucks as a name"
    date: 2009-05-22
    body: "opensuse sucks as a name though."
    author: "patcito"
  - subject: "Quality Assurance"
    date: 2009-05-22
    body: "Sure im not talking abaout eliminating all bugs, but bringing more Quality Assurance to software , if only an extra RC is made im sure KDE will satisfy stability needs."
    author: "marcomelo"
  - subject: "Reporting bugs"
    date: 2009-05-22
    body: "Since when and what bugs it is reasonable to report?\r\n\r\nReporting all bug (or when user thinks this is a bug, but its just a lack of feature etc) will flood bugs.kde with pointless bugs. \r\n\r\nThe same is for apps, that are in development, and bug fixes are not yet in repos, or just in devs heads \r\n"
    author: "lukas"
  - subject: "Dead Straw Man"
    date: 2009-05-23
    body: "There is some validity to what you stated.  However, the general impression one gets from it is that it is a laundry list of excuses for poor quality.  Yes, these are valid points, but they should not be used as excuses for buggy software.\r\n\r\nIt is true that more RC versions are, strictly speaking, neither necessary nor sufficient to produce more stable and bug free software.  However, I have to say that with our project spread all over the globe that having more RC version would give us more chance to find and fix bugs.  This contrasts with what I think is the current policy of kicking the release out of the door on the scheduled date, even if it isn't really ready.\r\n\r\nThen your (Straw Man) conclusion: \"As you see, there is no way to get bug-free software with just a longer release cycle\" does not follow from your premises and is a nonsequitur to the  posting you answered.  This is the falicy of the extnded argument.  The request was for better quality and you argue that making it perfect is not possible.  \r\n\r\nThis was not about bug free software.  What is wanted is rock solid (stable) software, rather than the buggy and unstable software that the KDE-4 project has produced so far.  Yes, there will always be minor bugs in \".0\" release, but instability and serious design and coding errors should be fixed before a release.\r\n\r\nI would tend to agree that a longer feature freeze wouldn't help.  What would help is a switch to a process that does not have a feature freeze.  I must say that I do not understand the motovation of the developers.  My motovation is to to the job well.  I would hope that the developers have the same motovation.  If so, what is needed are improvements to our development process."
    author: "KSU257"
  - subject: "Much improved"
    date: 2009-05-25
    body: "Don't know if I would say rock solid.  But, the desktop on  current SVN TRUNK is much improved compared with KDE-4.2.3.  And I see that other bugs have been fixed too.\r\n\r\nIf 4.0 was this good, there would have been no gripes.\r\n\r\nGood work (finally)."
    author: "KSU257"
  - subject: "I hope OpenSUSE 11.2 be"
    date: 2009-05-25
    body: "I hope OpenSUSE 11.2 be lighter than 11, KDE4 in 11.1 is feeels sloooow (on a core 2 duo 2.5 gb ram)"
    author: "marcomelo"
  - subject: "I don't know if it's"
    date: 2009-05-27
    body: "I don't know if it's openSuse's or KDE's fault but that's also the biggest problem that I am having even on my machine with 4 gig of RAM. We need a lot more speed here - like Gnome for example. "
    author: "Bobby"
  - subject: "For a Beta yes. I have never"
    date: 2009-05-27
    body: "For a Beta yes. I have never had a more stable Beta upto now.\r\n\r\nI think that 4.0 would take a lot more time to get where 4.3 is if it hadn't been released at the time that it was because it's the testing and reporting of that made it possible.I can't imaging that so many excited users would participating on using and testing KDE 4.0 if it would still be there as a Beta.\r\nApart from the confusion and misunderstanding around KDE 4.0, I think that the devs made the right decision on getting it out at that time even thoug it wasn't ready for the normal public.\r\nAnyway, all's well that ends well ;)"
    author: "Bobby"
  - subject: "EBN"
    date: 2009-05-28
    body: "http://www.englishbreakfastnetwork.org/krazy/?component=kde-4.x\r\n\r\n?\r\n\r\nIn the end stability can be improved by more automated testing and more trained developers. If the problem is lack of developers the solution may be found on another level.\r\n\r\nWith KDE 4.0 the problem was exactly that early adoptors had to wait too long to get a release of the KDE 4 platform. With even more delay you destroy your user community and raise expectations.\r\n\r\n"
    author: "vinum"
  - subject: "It is all in the name"
    date: 2009-06-02
    body: "I certainly agree that KDE-4.0.0 should have been released.  The only mistake was what to call what was obviously the first public Beta release.  The choice of the name has caused a lot of confusion and bad publicity for the KDE project.  And now I see that KOffice is going to make the same serious error."
    author: "KSU257"
---
The first <a href="http://en.opensuse.org/CommunityWeek">openSUSE Community Week</a> took place on the 11-17 May 2009 and as an important part of the distribution, the <a href="http://en.opensuse.org/KDE">geeko-loving KDE community</a> were actively involved.  

Throughout the week we were busy packaging, triaging bugs and introducing new community members to these skills.  The releases of <a href="http://dot.kde.org/2009/05/13/kde-43-beta-1-signals-beginning-bug-hunting-season">KDE 4.3 Beta 1</a> and <a href="http://dot.kde.org/2009/05/14/amarok-21-beta-2-released">Amarok 2.1 Beta 2</a> kept the team busy fixing last minute compilation errors and replacing 4.2.3 in the <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Desktop/openSUSE_11.1/">KDE:KDE4:Factory:Desktop</a> repository with 4.3 Beta 1 as we move towards openSUSE 11.2.  <a href="http://download.opensuse.org/repositories/KDE:/42/openSUSE_11.1/">KDE 4.2</a> continues to be available in the KDE:42 repository so if you haven't switched yet, we expect you want to help make 4.3 excellent!
<!--break-->
The week began with a lively chat session discussing the <a href="http://plasma.kde.org">Plasma desktop</a>. <a href="http://blog.chatonka.com/">Andrew Stromme</a> and <a href="http://www.notmart.org/">Marco Martin</a> from the Plasma dev team paid #opensuse-kde a visit and gave the community a look at what is coming in KDE 4.3.  The improved system tray and notifications system got everyone excited, and the channel was surprised to learn about the expanding folders in the Folder View widget that will make powerful desktop file management possible.  Marco introduced the ability to link Plasma Activities to virtual desktops, which the openSUSE community thought was a great idea to make Activities accessible and discoverable to the majority of users.  Questions were also asked about Plasmate, the planned Plasma widget creator tool, and ways to distribute scripted plasmoids to make user-created Plasma content as ubiquitous as browser extensions.

On Tuesday we discussed issues facing KDE system adminstrators and had a question and answer session on the internals of KDE.  Topics included how to lock down Plasma, how to ensure the right Phonon backends are installed on large KDE installations and how to setup and theme KDM.

Things hotted up again on Wednesday with the KDE PIM session.  Thomas McGuire and <a href="http://www.kdedevelopers.org/blog/83">Kevin Krammer</a>, two hyperactive KDE PIM developers, made the short journey from #kontact to #opensuse-kde and let us know how the migration to <a href="http://akonadi.kde.org">Akonadi</a> is going.  As usual, good things seem to be know they are worth the wait and take a bit longer than planned, but the participants were very happy to hear that when it comes to crucial PIM data, nothing is being rushed and stability and reliability are paramount.  The news that 4.3 KDEPIM apps contain numerous bug fixes and stability measures was well received.  KContactManager, the Akonadi-based eventual replacement for KAddressBook, was presented and used as an example of how Akonadi will give performance and footprint advantages over every other PIM system out there.  Exciting possibilities to integrate with online services were illustrated with the Google Data and Remember The Milk resources.  Mailody, the purely Akonadi-based KDE mail client and star of extragear, also got a mention, as well as KJots, the note-taking tool, and everyone was happy to hear that development continues on a KDE 4 port of Basket.

As SUSE has opened up and become <a href="http://www.opensuse.org">openSUSE</a> over the last couple of years, the team has adopted a pragmatic bug policy so that bugs which are definitely not specific to openSUSE are moved upstream to bugs.kde.org.  This is in everybody's best interest since the bugs end up where there are developers most able to fix them quickly, and our expert team of bug triagers improve the quality of the reported bugs by filtering out packaging issues, broken patches, reports against old versions and reports caused by underlying system problems.  Keeping bugs hanging around distro bug trackers for months where maintainers' limited resources mean they get limited attention is frustrating for users who take the time to make reports.  Luboš Luňák led two intensive bug-squashing days on Thursday and Friday which resulted in a large number of bugs being fixed and closed.

Overall the week was pretty intense but productive and the activity level in the #opensuse-kde IRC channel was noticeably higher than normal.  We look forward to having another Community Week soon, and also to KDE playing a big part in the openSUSE Conference planned for later in the year.