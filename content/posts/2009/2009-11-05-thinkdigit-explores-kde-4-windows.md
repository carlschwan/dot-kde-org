---
title: "ThinkDigit Explores KDE 4 on Windows"
date:    2009-11-05
authors:
  - "Stuart Jarvis"
slug:    thinkdigit-explores-kde-4-windows
---
Kshitij Sobti has written a nice article over at <a href="http://thinkdigit.com">ThinkDigit.com</a> about installing KDE 4 applications on Windows. He takes a look at the installation process and reviews some key applications such as Dolphin and Okular, praising Dolphin's "simple yet rich interface" and suggesting that "Okular can serve as a good replacement for Acrobat Reader". Kopete, Akregator and Kate are also featured. <a href="http://www.thinkdigit.com/Features/Getting-KDE4-on-Windows_3550.html">Read the full article</a>.
<!--break-->
