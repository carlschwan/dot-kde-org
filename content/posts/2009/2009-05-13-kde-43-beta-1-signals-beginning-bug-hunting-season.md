---
title: "KDE 4.3 Beta 1 Signals Beginning of Bug Hunting Season"
date:    2009-05-13
authors:
  - "sebas"
slug:    kde-43-beta-1-signals-beginning-bug-hunting-season
comments:
  - subject: "A quick question"
    date: 2009-05-13
    body: "Am I correct in noticing that this beta release doesn't ship kdebindings? (Since I have trouble getting PyKDE4 to compile myself...)"
    author: "einar"
  - subject: "You're correct. We (that's"
    date: 2009-05-13
    body: "You're correct. We (that's Dirk) had trouble building the bindings module and decided to skip it in this beta. It'll be included in 4.3 of course."
    author: "sebas"
  - subject: "Thanks for the answer. \nWhile"
    date: 2009-05-13
    body: "Thanks for the answer. \r\n\r\nWhile we're at it, the link \"the new system tray\" referred when talking about KGpg refers to a non-existent page kde.org/announcements/TODO. "
    author: "einar"
  - subject: "Just fixed that one, well"
    date: 2009-05-13
    body: "Just fixed that one, well spot!"
    author: "sebas"
  - subject: "Shadow colour"
    date: 2009-05-13
    body: "Cool! I'm looking forward to KDE 4.3. Will I be able to change to a black window shadow colour there? That would be a killer feature."
    author: "Tuxie"
  - subject: "yup"
    date: 2009-05-13
    body: "While I don't see how this is a killer feature, it's possible to change the shadow's color. Go to the settings of the shadows effect, there you can find this option."
    author: "sebas"
  - subject: "Zarin just noted on IRC that"
    date: 2009-05-13
    body: "Zarin just noted on IRC that this color setting is for something different, so sorry ..."
    author: "sebas"
  - subject: "Whooooaa"
    date: 2009-05-13
    body: "> New Tree Mode in System Settings \r\n\r\nNice, we waited for 3 revision to get actually usable navigation in systemsettings/kcontrol. Now, how long we will have to wait to get all the options inside actually working? (bug #168480, ...)\r\n\r\n> A fully themable look taking advantage ...\r\n\r\nGreat. Now to change a single color in UI shell (plasma) one need to create a whole new theme. Compare that to the easy yet powerful color kcmodule where one can easily change every color of selected style. Sad.\r\n\r\nRegards!"
    author: "vedranf"
  - subject: ">bug #168480\nI'm working on"
    date: 2009-05-13
    body: ">bug #168480\r\n\r\nI'm working on this as we speak, oddly enough. "
    author: "SSJ_GZ"
  - subject: "Thanks for your work!\nWhat I"
    date: 2009-05-13
    body: "Thanks for your work!\r\n\r\nWhat I don't like is the mentality: \"ship it and hope nobody will notice that it's broken\".  And then claim we have the best DE around.\r\n\r\nInstead, if devs want KDE to succeed, they should remove the UI, comment out broken code and publicly announce that this broken and will stay broken until someone steps in and fixes the problem.\r\n"
    author: "vedranf"
  - subject: "Beginning of Bug Hunting Season ... really?"
    date: 2009-05-13
    body: "Sebastian,\r\n\r\nWith all my respect to OSS, KDE developers and to you personally, I have to say that something is terribly wrong with KDE development model.\r\n\r\nThere are over a hundred severe bugs filed in KDE's bugzilla (with thousands(!) of duplicates), there are wishes that have earned over 100 points and you say \"Bug Hunting Season\" in KDE 4.3?\r\n\r\nTake for example Konqueror. It has no firefox3/ie8/opera10 like location bar. It lacks any sane history management (group by sites or by date with no intervals like today, yesterday, last week, last year; no ability to search in history; etc). It has no good ad blocking (no subscriptions, no separation between subscribed list and your own entries). In KDE4 Konqueror still opens text documents in external KWrite application instead of its own window, it has no progressive PNG/JPG showing (Gwen KIO part cannot do that right now). It has no ability to export/import own/other browsers settings, passwords or history. It has absolutely incomplete and bad KWallet usage (user/password are URL/PATH/ dependent), no ability to store more than one user/password for a single form... I can go on infinitely.\r\n\r\nThis is not a rant.\r\n\r\nI just want KDE to be really usable and I don't care much about all that fancy features meant to compete with Windows or/and MacOS.\r\n\r\nRight now KDE and its base components are barely usable. That's very sad. However I'm still glad that you are years ahead of Gnome in every regard.\r\n\r\nPlease, take care of old sores - eliminate long standing bugs. Raise funds and fund developers who can solve long standing bugs.\r\n\r\nThank you."
    author: "birdie"
  - subject: "Duel head support?"
    date: 2009-05-13
    body: "I would just hear if  kde 4.3 has support for 2 x session on 2 screens eka bug 156475,\r\nIt is the last thing that keeps me away from using kde 4"
    author: "beer"
  - subject: "cut the flamebait"
    date: 2009-05-13
    body: "> ship it and hope nobody will notice that it's broken\r\n\r\nin the case of the toolbars, chances are simply no one noticed. it's one of those settings that you tend to set once and then forget about from then on, and our current defaults are pretty good for most people which lowers the odds of noticing this even more.\r\n\r\nwhat we lack is a team going around and exhaustively testing features for regressions. if you look at the number of features in system settings, for instance, you'll start to get a feel for the enormity of the task.\r\n\r\nin this case it's been reported, someone is working on it, the system works given time."
    author: "Aaron Seigo"
  - subject: "corrections"
    date: 2009-05-13
    body: "> we waited for 3 revision to get actually usable navigation in \r\n> systemsettings/kcontrol.\r\n\r\ni will not quibble with you what you consider to be more usable for you. for the majority of the users we talked to and/or watched use kcontrol live over the course of many years, the tree view simply isn't very friendly.\r\n\r\nsome people have gotten the idea that KDE is exclusively for them and their group of friends. it isn't.\r\n\r\nnow, what's cool about the new system settings is that it does have something for both, it's faster (just check out the start up time on it), and it has activate developers.\r\n\r\ni think that effort deserves more from you than a flippant, negative remark seeing as you are obviously one of the primary beneficiaries. \r\n\r\nto compare/contrast: i don't use the tree view and yet now i'm saddled with it as an option, should i complain about your \"obvious lack of taste\"? of course not.\r\n\r\n> Now to change a single color in UI shell (plasma) one need to \r\n> create a whole new theme.\r\n\r\nthis is no more true than this is: \"to change a single color in KDE, you have to do a whole new widget theme\".\r\n\r\nfirst, plasma themes may opt to be colourized according to the desktop colour scheme.\r\n\r\nsecond, there's a control panel to mix and match items between themes if you wish to customize them that way. definitely a power user toy, but if you consider the colour scheme dialog \"easy\" i'm sure you'll get along just fine with that too."
    author: "Aaron Seigo"
  - subject: "Dual head support?"
    date: 2009-05-13
    body: "Sorry to break the news but it does not. \r\n\r\nDual head is a feature I personally have no way of testing, which none of our devs use (some do use Xinerama, however) and which is used by a vanishing small number of people.\r\n\r\nThere absolutely is a certain segment of our user base who would very much like to see this happen. I'm sure it eventually will, but perhaps all the dual-headers out there should try and come up with a way to convince someone to actually implement it.\r\n\r\nDon't sit and wait for others.\r\n\r\nIt's not all dour news though: the plasma application global options in kde 4.3 are now moved out into their own config file which should make implementing this a bit easier."
    author: "Aaron Seigo"
  - subject: "*cough*"
    date: 2009-05-13
    body: "> and you say \"Bug Hunting Season\" in KDE 4.3?\r\n\r\nYes we do. In fact, that's why we have such periods: to force a focus off of feature development and what not and focus on improving stability, performance and behaviour.\r\n\r\n> Take for example Konqueror. \r\n\r\nI agree with you about Konqueror: it is severely lacking in developer effort on the as-a-web-browser front.\r\n\r\n> This is not a rant.\r\n\r\nI beg to differ. Ranting is not necessarily a bad thing, but let's call it what it is :)\r\n\r\n> Please, take care of old sores - eliminate long standing bugs.\r\n\r\nWe are, there's just a lot to do; you can help us go faster :)"
    author: "Aaron Seigo"
  - subject: "Re: *cough*"
    date: 2009-05-13
    body: "I strongly believe KDE should start earning hard money (ads on all KDE related sites, t-shirts with KDE art style, \"Donate Here\" all over the place, etc) to hire several more full time developers.\r\n\r\nYou do insane amount of work but to make KDE feature-rich and bug-free you need more <b>dedicated</b> developers, not those <i>spare-time</i> guys who are helping you today and may disappear in Valhalla tomorrow. KDE's progress is doubly unbelievable since there are over five thousands full-time developers working on Microsoft/Windows 7 alone."
    author: "birdie"
  - subject: "Hi\n> It has no"
    date: 2009-05-13
    body: "Hi\r\n\r\n> It has no firefox3/ie8/opera10 like location bar\r\n\r\nIts getting developed as a google summer of code project which you can follow here: http://edulix.wordpress.com/\r\n\r\n>  It lacks any sane history management\r\n\r\nActually i'd be happy to inform you that this has been added.In 4.3 its placed in 'go' menu.you can try it out.It has both site and date sorting.\r\n\r\n> It has no good ad blocking\r\n\r\nthis one always worked good for me.\r\n\r\n> Konqueror still opens text documents in external KWrite application instead of its own window\r\n\r\nGo to Settings->Configure konqueror->File associations, select 'text' and check the 'open in embedded viewer' option.\r\n\r\n>  It has absolutely incomplete and bad KWallet usage\r\n\r\nI agree that it can be smarter than it is, but it has been imroved for 4.3.At least the ui is now a lot better.\r\n\r\nKDE is one of the most active FOSS projects.The development happens really fast and features and volunteers are 'flowing' into it.The switch to 4 series was a huge job and all those pillars are now starting to get intergrated.You can already see how far kde has become since two years ago when 4.0 was released.\r\n\r\nPlease do not blame kde's development model.Its just the matter of time."
    author: "emilsedgh"
  - subject: "..."
    date: 2009-05-13
    body: "\"in the case of the toolbars, chances are simply no one noticed. it's one of those settings that you tend to set once and then forget about from then on, and our current defaults are pretty good for most people which lowers the odds of noticing this even more.\"\r\n\r\nBut in this case someone noticed and reported the problem months before even 4.0 was out (http://bugs.kde.org/show_bug.cgi?id=152266). I talked about the problem almost every time there was an article on dot about new KDE release. Also there is a lot duplicates of this bug and is often voted for. This is not a single case, there are really lot of them. And btw. on smaller screens like laptops defaults are too big.\r\n\r\nTo me it seems like devs are not focused on core bugs, but instead on making nice screenshots to please users like the one here who commented:\r\n\r\n\"Will I be able to change to a black window shadow colour there? That would be a killer feature.\"\r\n"
    author: "vedranf"
  - subject: "Re:"
    date: 2009-05-13
    body: "<i>> this one always worked good for me.</i>\r\n\r\nOnce again: no subscriptions, no separation between (now missing) subscriptions and user generated rules.\r\n\r\n<i>> Go to Settings->Configure konqueror->File associations, select 'text' and check the 'open in embedded viewer' option.</i>\r\n\r\nI suppose most people prefer that behavior to be a default.\r\n\r\n<i>> Please do not blame kde's development model.Its just the matter of time.</i>\r\n\r\nOr maybe not since there are a lot of unresolved bugs filed at the beginning of this millennium, i.e. up to 6 years ago.\r\n\r\nI wasn't clear: Kde's development model is just fine - the real problem is that there are not enough qualified and dedicated developers."
    author: "birdie"
  - subject: "Simply no!"
    date: 2009-05-13
    body: "It's not an advantage to hire developers. If you have 10 full-time developers, there is also a risk of one or two quitting their job and moving to another corp. This risk is much more real and fearing than one of 100 spare-time devs vanishing (which is actually happening quite often in such a big project as KDE is).\r\n\r\nAlso, full-time developers would divide the community into two groups, which is certainly very bad. (See also the discussion on whether to introduce a bounty system in Bugzilla.)"
    author: "majewsky"
  - subject: "Um... no."
    date: 2009-05-13
    body: "> I suppose most people prefer that behavior to be a default.\r\n\r\nNormal people expect a webbrowser to be different from a text editor. This is the advantage of Konqueror: It just works for normal users, and is a swiss army knife to the power user."
    author: "majewsky"
  - subject: "Multi headed display use case: DTV transition"
    date: 2009-05-14
    body: "With the impending all-out indroduction of digital and HDTV broadcasting in Europe, the use of multi headed displays in media-center PCs and set-top-devices (recorders etc.) will likely become more interesting in just a few months from now.\r\n\r\n1st display: \r\nHDMI display device\r\n\r\n2nd display: \r\nPC Screen, case-integrated touchscreen, projector, ..."
    author: "RamsdenSource"
  - subject: "There is 2 methods I can"
    date: 2009-05-14
    body: "There is 2 methods I can convince people to make it (at least to my knowledge). One is filling in bug reports at bugs.kde.org and then vote for them whitch is already done by me.\r\n\r\nIf this does not work I know only another way and that is throughing money at the problem whitch I cannot affort now (I am a student)\r\n\r\nAnd maybe the duel-head users are vanishing becouse of it is missing in kde 4. And they prefer using kde 4 without duel head instead of kde 3.5 with duel head.\r\n\r\nI have tried to use Xinerama but it did not feel right (specielt that part when I have to tab trough all programes instead of just those on one of the Desktop. )\r\n\r\nSo the button line is I am still using kde 3.5 and will do so in min another release cyckle and hope that someone would fix it\r\n\r\n"
    author: "beer"
  - subject: "More developers would indeed"
    date: 2009-05-14
    body: "More developers would indeed be good. But then again, wouldn't it always? If you have ideas on how to get more developers in KDE, let us know...\r\n\r\nWe are working a bit on the money front, btw, not sure if adds on the websites and selling t-shirts would help a lot but we have some ideas."
    author: "jospoortvliet"
  - subject: "Raptor"
    date: 2009-05-14
    body: "It would be great to have more people help out in Raptor menu. I think that kickoff is a bad implementation, and doesn't glue well with kde4. Please give more attention to the (raptor) menu."
    author: "Autumnautist"
  - subject: "I have this setup with KDE4"
    date: 2009-05-14
    body: "I have this setup with KDE4 and Xinerama (1 HDMI + 1 Touchscreen) and it is working very nice.\r\n\r\nI'm not sure what the benefit would be, to use dual head setup, instead of Xinerama."
    author: "fabian"
  - subject: "Cite  Aaron Seigo:\"Dual head"
    date: 2009-05-14
    body: "Cite  Aaron Seigo:\"Dual head is a feature I personally have no way of testing\" \r\nIf it is only a mather of another screen before you would, or another dev is going to implement it I would gladly donate one of my screen to that person"
    author: "beer"
  - subject: "There is 3 reason for me to"
    date: 2009-05-14
    body: "There is 3 reason for me to use duel head instead of Xinerama:\r\n\r\n1: when I tab between the programs I prefer to see those I am actual working with. They are place on on screen/desktop and I have a few programs on the 2. desktop that I only whant to watch but not interfere with my work/tabbing souch as konversation, pidgin and ktorrent\r\n\r\n2: becouse of I have 2 screen with 2 defferent screen size I cannot see the whole of my Desktop if using Xinerama becouse screen nr 2 has a lower number  pixel in hight that my primary screen\r\n\r\n3: The last problem is not with Xinerama itself. A few program that I use get confused in full screen. Eks glob2 tries to cover the whole desktop but is only shown on the screen it was launced"
    author: "beer"
  - subject: "..."
    date: 2009-05-14
    body: "\"i will not quibble with you what you consider to be more usable for you. for the majority of the users we talked to and/or watched use kcontrol live over the course of many years, the tree view simply isn't very friendly.\"\r\n\r\nYeah, make it dumb-friendly so even dumb users can use it. But note that this will repel non-dumb users. Is that what you want? AFAIK systemsettings was designed by kubuntu to ease the switch from winxp and osx. That's all, nothing about friendliness.\r\n\r\n\"to compare/contrast: i don't use the tree view and yet now i'm saddled with it as an option, should i complain about your \"obvious lack of taste\"? of course not.\"\r\n\r\nThis isn't a matter of taste, it's simply matter of NUMBER of clicks needed to browse from option to option. In some of my previous post here on dot I counted number of clicks needed to change some options. Using systemsettings that number is double the dumber of clicks needed when using tree in kcontrol. How is that friendly?\r\n\r\n\"first, plasma themes may opt to be colourized according to the desktop colour scheme.\"\r\n\r\nSay I like oxygen but I want it to match my red-ish desktop color settings and reduce opacity a bit. How am I supposed to do that?\r\n"
    author: "vedranf"
  - subject: "module for hardware events?"
    date: 2009-05-14
    body: "Hello,\r\n\r\nI've read in the anounsement that there is new module to configure actions triggered by hardware events. Where can I find it?\r\n\r\nFuther, on the website, about howto compile this version, the new package kdelibs-experimental is not mentioned anywhere. It should.\r\n\r\nStef Bon"
    author: "sbon"
  - subject: "over five thousands"
    date: 2009-05-14
    body: "> over five thousands full-time developers working on Microsoft/Windows 7\r\n\r\nno.. it's not nearly that number... (only counting developers)\r\n\r\nin fact, even the single project \"Linux Kernel\" nowadays has about the same no of people working on it as the whole \"windows division\" inside MS...\r\nIf you take into account, that we divide up into different projects, like kernel, basic libs, gnu utils, X, KDE or gnome and so on.. and if you sum up all the people working on these parts.... well you get the picture..."
    author: "thomas"
  - subject: "You can"
    date: 2009-05-14
    body: "You can it from shadow settings now. But the active window shadow is tied to window decoration color. You need to go to appearance > Colors > Colors (Tab) > Active Titlebar and set that color wanted. \r\n\r\nI am hoping that 4.3 allows easily change active shadow color as well, from same place. It is not nice to be forced to have same active shadow color as the window decoration is. And these two configs should be on the same place."
    author: "Fri13"
  - subject: "Congratulations!"
    date: 2009-05-15
    body: "I just wanted to thank everyone on team KDE for the amazing work that's taken place since 4.0 was released. 4.2 was a particularly impressive improvement for me and set the bar high for 4.3. Now I'm excited about the integration and visibility of Nepomuk / Strigi / Desktop search and many of the other shiny news toys promised.\r\n\r\nI just with those Kubuntu packages would hurry up and come down the line so I can try this! Heh, maybe I'll just go and install Karmic on my old laptop instead... :D\r\n\r\nAnyway, I've been highly impressed by both the forward looking decisions in KDE with the pillars, and the rapid improvement that we've seen. Remember, the people who hate *any* change, while whining today, will eventually be the ones who will be proclaiming that everyone should be jumping from KDE 3.5 to 4 (once they're used to it! ;-)\r\n\r\nGreat job KDE community! 4.3 development looks like it's rocking hard!"
    author: "Bugsbane"
  - subject: "KDE and online security, only half way"
    date: 2009-05-15
    body: "Last I checked, the TLS/SSL module (or whatever it is called) totally lacked support for CRL or OCSP, so any revocation of certtificates, even manual, goes by totally ignored by KDE. I have not seen any changelog indicating that anything is done of yet. Will there ever be any focus on fixing this rather severe problem?"
    author: "kolla"
  - subject: "TQM needed"
    date: 2009-05-15
    body: "\"what we lack is a team going around and exhaustively testing features for regressions. if you look at the number of features in system settings, for instance, you'll start to get a feel for the enormity of the task.\"\r\n\r\nFirst I note that your are correct that regression testing is needed.  To bad that there isn't any way in KBO to tag bug reports as regressions.\r\n\r\nActually we have such a team.  They are the users.  However, if developers and fanboys keep dissing users for complaining about problems and reporting bugs at KBO, we are going to have less of these valuable workers.\r\n\r\nHowever, this is not the best way to do things.  This outdated quality control method has been replaced by TQM in most industries.  Perhaps software development is in denial."
    author: "KSU257"
  - subject: "Actually..."
    date: 2009-05-15
    body: "I discovered that you CAN change the shadow colour: System Settings --> Desktop --> Kwin Effects --> All Effects --> Shadow --> \"Force default shadow for decorated windows\". Then you can set the shadow using this dialogue, and it applies to both the active and inactive windows."
    author: "madman"
  - subject: "Re:"
    date: 2009-05-16
    body: "\"\"\"\r\nAFAIK systemsettings was designed by kubuntu to ease the switch from winxp and osx. That's all, nothing about friendliness.\r\n\"\"\"\r\n\r\nSystem settings was first done by someone as an alternative to kcontrol. Later, Kubuntu picked it up to replace the rather unfriendly kcontrol. SS was bug fixed and IIRC it was one of KDE's usability experts El who did the research to determine what the categories and organisation should be. SS was then taken up in KDE 4. It was *all* about improving the usability of the configuration settings.\r\n\r\n\"\"\"\r\nThis isn't a matter of taste, it's simply matter of NUMBER of clicks...\r\n\"\"\"\r\n\r\nIt is not a matter of clicks. It is a matter of normal people being able to use their computer; something which the old kcontrol was poor at.\r\n"
    author: "Sime"
  - subject: "And that's only 4.3..."
    date: 2009-05-16
    body: "Let's see how 4.4 will be, when the GSoC students have finished their projects and merged them into trunk. Awesomebar for Konqueror, a new widget explorer and network transparency in Plasma, 3D mode in Step, etc. etc. I just can't wait for it."
    author: "majewsky"
  - subject: ">There is a new tree view"
    date: 2009-05-17
    body: ">There is a new tree view mode in System Settings\r\n\r\nWhat's about admin mode (bug #151669) in System Settings? It's one of really important defects. It would be nice if you focus on functionality, not on visual effects."
    author: "generatorglukoff"
  - subject: "KDE 4.3"
    date: 2009-05-18
    body: "I am deeply impressed by the progress made! However, stability is still a problem. I counted three unexpected application crashes so far. I sincerely hope that special attention will be put on improvements of the stability. So I can only recommend you to release more often, also with full debug information enabled.\r\n\r\nIn addition to that there are still many trivial quality issues recorded in EBN\r\nhttp://www.englishbreakfastnetwork.org/\r\n\r\nProbably KDE needs a more highlevel automated testsuite to track the maturity.\r\n\r\nWhere do I find information on how to build the development version from trunk?"
    author: "vinum"
  - subject: "Easy-to-learn ain't same as Easy-to-use."
    date: 2009-05-19
    body: "[quote]It is not a matter of clicks. It is a matter of normal people being able to use their computer; something which the old kcontrol was poor at.[/quote]\r\n\r\nIt was matter how the _new users_ was be able to change settings easily. Not how _normal people_ or _advanced_ people.\r\n\r\nThere is almost every time two different lines.\r\n\r\n1) Easy to learn\r\n2) Easy to use\r\n\r\nSoftware can be easy to learn, but difficult to use.\r\nSoftware can be difficult to learn, but easy to use.\r\n\r\nThe midway is always hardest to find, usually impossible.\r\nThe only way to get over the basic problem, is to make options to tweak settings and allow easy-to-learn UI, be tweaked to easy-to-use UI. \r\n\r\nMost important part is, that the UI/Settings are by _default_ in the easy-to-learn -mode. The advanced and power users know how to tweak UI and settings how they like it, so they wont be in their way doing jobs. But new users do not know it, and they do not even know what they want, yet!\r\n\r\nSo when blockin the advanced/power users possibilities to tweak the settings/UI, the application actually turns out to be worse than what was the original meaning of usability.\r\n\r\nWe can find in the usability tests so many times how new users see things, but many times in the tests is forgotten the advanced and power users who does things in different ways and the study goes wrong right away.\r\n\r\nExample, the great feature of the KDE4 applications like Konqueror, is that user can tweak it's toolbars how she/he pleases. Problem on the KDE3 was that by default, the konqueror toolbars were full of buttons for every function what power users needed. Konqueror did not gain popularity among new users because of it, but was popular among power users. \r\n\r\nNow on KDE4, the toolbar was set _by default_ to easy mode, what made konqueror gain popularity among new users, but among power users as well, because they could tweak the toolbars how they wanted. So both were happy.\r\n\r\nWhat was the only thing what mattered there? Options, tweaks and other features what many KDE developers now starts to blaim as disease of usability. But they make error there, because the error was simple as wrong _default_ settings. \r\n\r\nDragonplayer or Kontact are one of kinds what has usability problems in basic levels. They do not allow hide the menu on Ctrl+M. You can hide toolbar or even other important panels, but not menu. Even the normal user would never use the menu but just toolbar, it is not possible to hide menu. Developers gives excuses about how normal users would hide the menu by mistake and would not get it back etc.\r\n\r\nThat is easy to get over. 1) add function to hide menu by shortcut. 2) place the shortcut by default unassigned.\r\n\r\nThen those who want to have menu hided, can go to shortcut functions and assign ctrl+M there and then use the feature. If someone actually does such thing that assigns a shortcut for that actions and then does not know how to get it back, you can not protect her/him from anykind mistakes by removing the hide menu capabilities. Developers only makes all other users to suffer from bad default settings again, by protecting only those few who would make the mistake in the first place.\r\n\r\n"
    author: "Fri13"
  - subject: "[quote]Once again: no"
    date: 2009-05-19
    body: "[quote]Once again: no subscriptions, no separation between (now missing) subscriptions and user generated rules.[/quote]\r\n\r\nWell, I do not need those because I use same filtersets as I use on Firefox on Windows systems. \r\n\r\nI just import the AdBlock lists and I am good to go. No need to add other filters or manage those. \r\n\r\nI am happy about that but I think that import feature need to be easier, by making easy link to set it.\r\n\r\nOr then make easy to block wanted addresses by right clickin ad (even flash etc) and selecting \"block address\" and allow user to tweak the address by adding asterisks or joker marks."
    author: "Fri13"
  - subject: "Akonadi really in beta state?"
    date: 2009-05-19
    body: "Currently testing KDE 4.3 Beta 1 and recent upstreams.\r\nIf I look at Akonadi, there seems to be rather a heavy development in progress than a feature freeze. The internal Akonadi PROTOCOL version changed meanwhile from 10 over 11 to 12. Doesn't feature freeze concern Akonadi or didn't I get the message what feature freeze means?"
    author: "rkrell"
  - subject: "Kubuntu packages"
    date: 2009-05-20
    body: "FYI Kubuntu packages http://www.kubuntu.org/node/81 are utterly broken. There's a conflict between kdebase-data (IIRC) and kde-icons-oxygen and the upgrade leaves you with a broken KDE setup.\r\n\r\nEDIT: They've changed the post, now it talks only about 4.3b1 being in Karmic. No more Jaunty. I hope they'll put it back... I'd like to give beta1 feedback to KDE devs without having to pass through the entire OS beta trial.\r\n"
    author: "Vide"
  - subject: "Feature Freeze"
    date: 2009-05-20
    body: "Feature freeze means that developers are not allowed to add features to the feature plan (exceptions can be discussed).\r\n\r\nAfter the feature freeze, which was about 3 weeks ago, developers finalize the features they have added to the feature plan. This may include bug fixing, and of course changing implementation details (such as a protocol version) if it helps fixing bugs.\r\n\r\nTo find bugs very early, builds of KDE are released shortly after the feature freeze. Bugfixing continues in the branches after trunk opens again for new features.\r\n\r\nThis means that 4.3.0 will contain bugs, the release will only be delayed on \"show stopper\" bugs. The speed of bug fixing largely depends on how many developers can help, and how much time developers can spend.\r\n\r\nRegarding crashes, I should note that it IMMENSELY helps developers if the crashes are reported with FULL debug information (including source file line numbers) for both KDE and Qt libraries. I have seen several crash reports for the KRandR KControl module, but was never able to see why it crashed, until someone posted the full backtrace. Fixing it was then a job of a few hours. So much for people that say that bugs are open for years...\r\n\r\nWhile crashes are probably the worst bugs, please note that developers are also willing to fix minor UI glitches, like color, font, or layout problems, so please report these, too. Do not wait for others to report them, even if you think that it is an \"obvious bug\"."
    author: "christoph"
  - subject: "Don't worry about the issues"
    date: 2009-05-20
    body: "Don't worry about the issues reported on the EBN. Those are in almost all cases no critical bugs and won't cause user-visible grief. The number of issues found on the EBN is only a very weak indicator for code quality.\r\n\r\nI agree that more automated testing would be a good thing though.\r\n\r\nYou can find information how to build KDE's development version on TechBase, it's very easy to find from the frontpage."
    author: "sebas"
  - subject: "You find it in ..."
    date: 2009-05-20
    body: "System Settings -> Advanced -> Device Actions"
    author: "sebas"
  - subject: "oh oh"
    date: 2009-05-20
    body: "Ok, so here's an email you could write to Ben:\r\n\r\n\"Dear Ben,\r\n\r\nI think it's cool that you spent your free time on improving system settings. You didn't concentrate on the right thing though since I'm missing the administrator button. It would have been good if you didn't start to rewrite sytem settings at all but just went ahead and fixed bugs like the one I'm quoting. Please do that next time you want to contribute to KDE.\r\n\r\nYours,\r\n\r\ngeneratorlukoff\"\r\n\r\nI hope you get the irony here.\r\n\r\nOn a more serious note, we managed to get support for PolicyKit in, so we can now tackle this lack of privilege elevation in the right way.\r\n\r\nMeanwhile, distros have been working around this in a way that's less than optimal. We did not merge these patches because we want it to be solved in the right way. No users harmed, though."
    author: "sebas"
  - subject: "Akonadi not concerned"
    date: 2009-05-20
    body: "You're right that Akonadi is not concerned by the feature freeze. Akonadi is in kdesupport, and as such kind of a third party package. (It also doesn't have any KDE dependencies.)\r\n\r\nIn this situation, it's even very handy that development on Akonadi can go on at full pace. It's a huge amount of work to port all KDE PIM components to Akonadi, and to get it ready in time for KDE 4.4. That's what's being worked on.\r\n\r\nOf course the Akonadi glue libraries that are contained in kdepimlibs will remain backwards compatible, no matter what."
    author: "sebas"
  - subject: "Consider more Release Candidates in Kde 4.3 development"
    date: 2009-05-20
    body: "For kde , they should consider a longer release cicle, for example making RC2 RC3 and so until gets solid rock, and then RTM or something like, that.\r\n\r\nMaking only one RC1 will cause KDE 4.3.0 not be solid rock system as many of us expect. \r\n\r\nAnyway Kde 4.3 beta 1 is in right direction, = D"
    author: "marcomelo"
  - subject: "Re: Consider more Release Candidates"
    date: 2009-05-21
    body: "Did you read what Christoph said above? (His post title was \"Feature Freeze\", unfortunately the new dot does not create links to comments.) It's absolutely impossible to kill all bugs, because, for example, developers lack resources to reproduce many bugs that depend on configuration.\r\n\r\nAlso, some bugs emerge from programs being in a transition state to a better solution (one example being System Settings which did not yet get Administrator Mode because they want to do it the right way with PolicyKit).\r\n\r\nA similar argument applies for some bugs and also some crashes: Developers might already know how to fix them, but they would possibly introduce even more bugs through a hacky solution.\r\n\r\nSo there can be numerous reasons why a bug does not get fixed in the first place. The last and most important reason is lack of manpower, but we cannot solve that. There is a saying that \"assigning more workers to a delayed project delays it further\", which is quite true: New developers have to find their way into the program, which may consume the time of the old devs who have to explain things to them, or the new devs might break stuff without noticing, thereby introducing even more bugs.\r\n\r\nAs you see, there is no way to get bug-free software with just a longer release cycle. Our release cycles already consist of one third feature freeze. Even more could be counterproductive, as it lowers the motivation of the developers significantly. I hope you understand this reasoning."
    author: "majewsky"
  - subject: "Project Neon"
    date: 2009-05-26
    body: "For (K)(X)(ED)Ubuntu use this repo:\r\ndeb http://ppa.launchpad.net/project-neon/ubuntu jaunty main\r\n\r\nThen you can install KDE 4.3 alongside KDE 4.2.\r\n(Install the kde-nightly package or something)."
    author: "ikkefc3"
  - subject: "Bug Hunting?"
    date: 2009-06-10
    body: "Yes, hunt bugs if you want to, but don't waste your time filing a bug report.\r\n\r\nBecause, it is quite probable that a developer will immediately post a lame excuse for why it isn't a bug.  The latest variation on this is claiming that things that are obviously bugs are \"Wish List\" items.\r\n\r\nSee:\r\n\r\nhttps://bugs.kde.org/show_bug.cgi?id=195837#c1\r\n\r\nI think that this can be restated as I (AJS) didn't design this correctly so your are just wishing that it had been done correctly.\r\n\r\nPerhaps the bugs are all wishes. :-D\r\n\r\nActually, whether this was a design error or a coding error, it is still a bug."
    author: "KSU257"
---
The first beta version of the new and shiny KDE 4.3 <a href="http://kde.org/announcements/announce-4.3-beta1.php">has been released</a>. The list of new features and improvements is long and impressive as ever. There is a new tree view mode in System Settings, many cool new features, the desktop search becomes more visible to the user, and yet more polishing in user interfaces all over the place make using KDE more fun. Interesting infrastructural bits are the integration of geolocation services into Plasma and Marble, a couple of nice improvements for KAlarm, the notification icon for your calendar. Apropos, the calendar in the clock now shows you holidays. More work down there in the panel brings the option to have the panel cover windows, to accurately align your panel using the new spacer. While KDE 4.3 already looks nice and stable, the KDE team wants to shake out as many bugs as possible before KDE 4.3.0 will be released in late July.
<br />
Get in gear ready for test-driving KDE 4.3 and help us ironing out kinks for everybody's KDE pleasure.
