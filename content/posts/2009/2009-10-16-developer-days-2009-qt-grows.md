---
title: "Developer Days 2009: Qt Grows"
date:    2009-10-16
authors:
  - "sebas"
slug:    developer-days-2009-qt-grows
comments:
  - subject: "Harmattan release date"
    date: 2009-10-16
    body: "<p>Hi. The article says the following:</p>\r\n\r\n<blockquote>\r\nMaemo 6 (Harmattan) is already planned for the fourth quarter of this year.\r\n</blockquote>\r\n\r\n<p>I suppose this is a typo or something, since the N900 will appear pretty soon, but already in the last 3 months (so 4th quarter) of this year, the same that Qt 4.6, which probably will be stable by the beggining of the next year.</p>\r\n\r\n<p>Is there an expected release date for Harmattan?</p>"
    author: "suy"
  - subject: "It's not a typo, I understood"
    date: 2009-10-16
    body: "It's not a typo, I understood during the Maemo summit (and nobody corrected me during devdays) that it'll be out in Q4 2009 (which indeed would be really quick!). The report on LWN suggests 2010. Does someone know what's right?"
    author: "sebas"
  - subject: "\"the end of the widgets as we know them today\""
    date: 2009-10-16
    body: "If this really kicks off, and standard widgets are fully obsoleted by Graphics View stuff in Qt, does that mean that we have to make KDE 5 even before Qt 5 appears (which is, afaik, not going to happen any time soon)? After all, GUI is quite a big part of what we're doing here."
    author: "majewsky"
  - subject: "Yes and no"
    date: 2009-10-16
    body: "Yes and no. You heard right, the Maemo 6 SDK (or a pre-release thereof, or at least the UI libraries; I don't know, I wasn't at the Summit) will be made available later this year. The device will not.\r\n\r\nSee http://www.qt.gitorious.org/maemo-6-ui-framework. The point is that you should start seeing more activity (rather than the initial code dump) over there."
    author: "thiago"
  - subject: "Obsolescence and obsolescence"
    date: 2009-10-16
    body: "Standard widgets will become obsolete because the new ones will just be a lot more powerful and easier to use.\r\n\r\nBut no one said anything about deprecating them. We still have a few ideas how to breathe some more life into them. But we're not hiding anything. Check out the developer blogs."
    author: "thiago"
  - subject: "Wrong Qt version"
    date: 2009-10-16
    body: "\"This will further increase with Qt 4.7 which will bring support for a number of new platforms, new APIs and vastly improved performance in some areas. The new platforms Qt 4.7 will officially support are Windows 7, Apple's Snow Leopard, QNX, VxWorks and the port to Nokia's Symbian-driven smartphone OS S60.\"\r\n\r\nI think it is about Qt 4.6, not 4.7"
    author: "serzholino"
  - subject: "You are right"
    date: 2009-10-16
    body: "Indeed, fixed. Thanks for reporting."
    author: "danimo"
  - subject: "QNX and VxWorks"
    date: 2009-10-17
    body: "... are community supported, so no commercial support will be given for those two platform ports."
    author: "harry1701"
  - subject: "sw vs hw"
    date: 2009-10-17
    body: "Thanks Thiago, I can trust my own ears and memory again :-)\r\nThe N900+1 will bring features such as multitouch and a capacitive touchscreen, but Maemo 6 won't require those features to work. This means that Maemo 6 can be released before the next version of the device is out, and that it'll run fine on the N900 as well.\r\nMy uncertainty was merely about the software."
    author: "sebas"
  - subject: "QNX and WxWorks"
    date: 2009-10-18
    body: "...are community supported only until they gain a user base where it makes economic and strategic sense for Nokia to add commercial support for the platform ports."
    author: "Morty"
---
Last week, Munich saw the 2009 Edition of the Qt Developer Days. Qt Developer Days is a Qt-focused software conference which is held yearly in Europe and the U.S. The American edition will be held at the start of November in San Francisco. 700 attendants and more than 70 Trolls made this edition the biggest Developer Days to date. Qt Development Frameworks had invited a group of KDE developers to the conference, more well-known heads from the KDE world were sent by their respective employers. The days brought training sessions around Qt and many interesting presentations ranging from higher level topics such as the future roadmap for Qt to topics related to Qt programming with techniques and technologies such as the Model/View Framework, QGraphicsView, WebKit, multithreading and many more. Read on for a more detailed report of what has been revealed and talked about during the three-day conference in Munich's Hilton Park hotel.
<!--break-->
The first day was kicked off by KDE and KDAB hackers Till Adam and Mirko Boehm who gave an introduction to programming with Qt. Basic concepts such as signal/slots, QLayouts and widgets passed by. At the end, the audience was well-prepared for more in-depth topics such as multithreading using Qt's tools, QWebKit, model/view programming and QGraphicsView. For the seasoned KDE hackers, not everything revealed new knowledge. Nevertheless, reiterating over those rather basic topics helped to prepare for the Qt certification which Qt Development Frameworks has introduced this year. Qt kindly sponsored certification for KDE hackers, who took the opportunity to prove their expertise. Results are not known yet, most of those your editor talked to were rather confident after the exam. Results will only become available in the coming weeks though. Let's wish all those who took the exam (this includes your editor) good luck.

During his presentation, Qt Development Frameworks' Sebastian Nyström gave some background on the state of the art in the Qt world. Nyström told that the plans to grow the Qt developer base 10-fold are well underway. Adoption has grown to 250% of last year's size, an exponential pattern can be observed. This will further increase with Qt 4.6 which will bring support for a number of new platforms, new APIs and vastly improved performance in some areas. The new platforms Qt 4.6 will officially support are Windows 7, Apple's Snow Leopard, QNX, VxWorks and the port to Nokia's Symbian-driven smartphone OS S60. For developers, Nokia is working on the Qt SDK which is built around tools such as Qt Designer and Qt Creator. This SDK will be complemented by the "Qt Developer Network". The adoption of the LGPL license for Qt and opening of the development process is, according to Nyström, also showing the envisioned effect. The Qt developers have already been able to merge well over 400 contributions from conributors outside of Nokia, Qt Development Frameworks.

KDE hacker extraordinaire Lars Knoll told the audience more about the roadmap for the coming year. In contrast to Qt 4.6 (which is due to be released in December), Qt 4.7 will contain only little new features and focus more on stability and performance. Nokia plans to increase the development budget by 50%.

In his presentation about the Qt Markup Language (QML), the declarative UI technology that will be part of Qt 4.7 (despite Lars' announcement to do "less features" in this version), KDE founder Matthias Ettrich, who is heading the Qt department in Berlin, Germany announced the end of the widgets as we know them today. Static, widget-based UIs will in the future morph into object / state relationships that allow for more fluid, rich and attractive UIs.

QML and the declarative UI concept is part of the Kinetic project in Qt, which will start to materialize in the upcoming Qt 4.6 already. The new APIs developed as part of Kinetic in Qt 4.6 are QGraphicsEffect, a framework that makes it easy to apply visual effects to visual elements, the new animation API (which is already being integrated in KDE's Plasma shell as we speak), an underlying State-Machine framework and support for gestures and multitouch in Qt. A beta version of Qt 4.6 has been released during the DevDays.

In the hallway track, your editor met CMake's Bill Hoffman who attended the conference to evaluate -- together with certain Qt developers -- replacing QMake with CMake in the future. This step would surely be welcomed by many KDE developers since it streamlines both layers even more. It will be interesting to see what the future brings on this front.

During the conference, we spotted many of the new N900 Maemo based devices. Developers were largely happy about this new piece of hardware, and were excited to see good progress on this front. With Maemo having announced official support for Qt on Maemo 5, the Qt ecosystem showed great interest in this new platform. Less than 24 hours after getting their hands on it, the KDE team attending the <a href="http://dot.kde.org/2009/10/11/maemo-conference-2009-amsterdam">Maemo summit</a> which was held last weekend in Amsterdam, the hackers around Marijn Kruisselbrink showed <a href="http://dot.kde.org/2009/10/12/free-n900">Plasma running on the N900</a>. Maemo 6 (Harmattan) is already planned for the fourth quarter of this year, leaving the impression that Maemo's transition to Qt is progressing quickly, opening exciting opportunities for Qt developers to dive into mobile development and open and hackable phones. Lucky guy of the conference was definitely KDE's Sandro Andrade who won two contests in a row. Sandro will be going home with an N900 and a new T-shirt. Daniel Kihlberg who handed over the prizes suggested Sandro should play lotto that same day.

Apparently, videos of many of the presentations held during DevDays in Munich will become available shortly on <a href="http://qt.nokia.com">Qt's web pages</a>.