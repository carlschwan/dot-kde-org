---
title: "Stallman, Bender, Lefkowitz and Pavelek To Hold Keynotes at Gran Canaria Desktop Summit"
date:    2009-06-11
authors:
  - "nightrose"
slug:    stallman-bender-lefkowitz-and-pavelek-hold-keynotes-gran-canaria-desktop-summit
comments:
  - subject: "You make me jealous"
    date: 2009-06-11
    body: "I really would like to see all of you in person again, but lectures continue until the mid of July.\r\n\r\nAside that, kudos to the government of Gran Canaria. I would really like our government here in Germany to be more like that. But don't worry, I'm working on that, I've elected the Pirate Party. ;-)\r\n\r\nEDIT: I nearly forgot this one: FIRST COMMENT!!!!11111oneoneone *scnr*"
    author: "majewsky"
  - subject: "I really hope this will work"
    date: 2009-06-11
    body: "I really hope KDE and Gnome can work together more closely. Even if it means to break a few eggs in the process. \r\nSadly I think most of the reaching out to the other project will again have to be done by KDE (because they are not so \"corporate\", I hope you know what I mean).\r\n\r\nBut the perfect outcome would be if both projects would swallow their pride and work on a great free desktop.\r\n\r\nA still young KDE4 and a up and coming Gnome 3 offer a great chance for more collaboration. (Just to be a bit more specific, maybe work on a common HIG etc.)\r\n"
    author: "kragil"
  - subject: "Travel Agency..."
    date: 2009-06-12
    body: "Anyone else having issues with the official travel agency and accommodation bookings?  I've yet to hear back from them after several days, is that a normal delay in peoples experience?"
    author: "odysseus"
---
The GNOME Foundation and KDE e.V. are excited to <a href="http://www.grancanariadesktopsummit.org/node/239">announce the keynotes</a> for the first ever co-located Akademy and GUADEC, over 100 talks as well as BOFs, keynote sessions, lightning talks and many opportunities to meet other developers and begin collaborating between projects.
Current confirmed keynotes are:
<ul>
<li><b>Richard Stallman</b>, Free Software Foundation</li>
<li><b>Walter Bender</b>, Executive Director, Sugar Labs</li>
<li><b>Robert Lefkowitz</b>, Distinguished Engineer of the ACM</li>
<li><b>Jakub Pavelek</b>, Nokia</li>
</ul>
<!--break-->
The keynotes were chosen to inspire conference attendees and to encourage collaboration. Over the past few years, GNOME and KDE have been cooperating in order to make choices and application development easier for end users, distributors and ISVs. An increased amount of technology is shared between the desktops, making cross-desktop application integration easier. By holding their annual developer flagship events in the same location, KDE and GNOME will foster cooperation and discussion between their developer communities. In addition many other desktop technology groups have expressed interest in attending and we expect to have participants from many technology projects that touch on the open source desktop and mobile space.

The Desktop Summit welcomes all users and contributors to the Desktop Summit and expects attendees from the GNOME and KDE communities as well as related projects and companies that use desktop technologies.


The Desktop Summit will take place July 3-11, 2009 in the Alfredo Kraus Auditorium and the Palacio de Congresos in Las Palmas, Gran Canaria. The venue has facilities for 1000+ people to attend keynotes and multiple simultaneous tracks with plenty of room for an exhibition area and hacking space.

Free Software is highly valued on the Canary Islands. Hence, the Gran Canaria Desktop Summit 2009 is supported by the Cabildo de Gran Canaria, the local government, that has provided much logistical and financial support.

Registration is free and we look forward to meeting desktop enthusiasts at the conference. You can register at <a href="http://www.grancanariadesktopsummit.org/node/30">grancanariadesktopsummit.org</a>.