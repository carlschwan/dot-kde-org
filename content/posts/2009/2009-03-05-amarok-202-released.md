---
title: "Amarok 2.0.2 Released"
date:    2009-03-05
authors:
  - "nightrose"
slug:    amarok-202-released
comments:
  - subject: "Thanks"
    date: 2009-03-05
    body: "Thanks for the great release.\r\n\r\nAny idea when 2.1 will be released?\r\n\r\nAlso what's with the reference to Mike Oldfield in the code name? :P"
    author: "patcito"
  - subject: ":) You're welcome.\nYes we"
    date: 2009-03-05
    body: ":) You're welcome.\r\n\r\nYes we have a plan for 2.1 but it is not set in stone. Beta 1 is not too far away.\r\n\r\nWe love Mike Oldfield. Or why do you think Amarok is named Amarok? :D"
    author: "nightrose"
  - subject: "Do tell."
    date: 2009-03-05
    body: "Never thought about it, and don't get the connection with Oldfield."
    author: "stumbles"
  - subject: "http://lmgtfy.com/?q=amarok+o"
    date: 2009-03-05
    body: "http://lmgtfy.com/?q=amarok+oldfield :-P"
    author: "Robin"
  - subject: "I've only used it for a few"
    date: 2009-03-06
    body: "I've only used it for a few hours but it seems to have fixed some problems I had with random incorrect tagging in 2.0.1.1. Amarok is shaping up nicely, I look forward to 2.1."
    author: "hconnellan"
  - subject: "Gaa"
    date: 2009-03-06
    body: ".._. .._ _._. _._ _ .._. .._. ._. _..."
    author: "NabLa"
  - subject: "Amarok collection import"
    date: 2009-03-06
    body: "Importing the collection from Amarok 1.x is somewhat important (big deal for me). Never worked properly on previous A2 releases, I find it disappointing it hasn't really been fixed yet.\r\n\r\nNot so clear I'll move to A2 in the near future :("
    author: "NabLa"
  - subject: "will be audio CD support in v2.1?"
    date: 2009-03-06
    body: "A little question about future v2.1: Will return audio CD support?\r\n\r\nUntil we get that support I can't migrate all computers of the house from the 1.x to version 2.x :-("
    author: "xapi"
  - subject: "Yep"
    date: 2009-03-06
    body: "Yeah, eventually AudioCD support might return.\r\n\r\nMore important to us is easy and well integrated automatic CD ripping, but technically ripping is pretty much the same as playing - just that you are throwing the data away after playing in one case, while you are keeping it in the other.\r\n"
    author: "Mark Kretschmann"
  - subject: "its pretty much completely different :D"
    date: 2009-03-07
    body: "Playing sounds is quite different from saving it.\r\n\r\nThat said I think it'd be pretty trivial to enable cd playing."
    author: "eean"
  - subject: "One suggestion"
    date: 2009-03-07
    body: "It's looking really slick -- congrats to all Amarok devs!\r\n\r\nThere's only one tweak I would make: in the VCR-like buttons, the backwards/forwards icon should be slightly curved in order to follow the contour of the overlayed of the pause/stop buttons, respectively.\r\n"
    author: "Cultural Sublimation"
  - subject: "MySQL"
    date: 2009-03-14
    body: "Hello,\r\n\r\nOne little Question: Will return MySQL support to Amarok 2.1?"
    author: "Max Steel"
  - subject: "Yes / No"
    date: 2009-03-15
    body: "In fact Amarok 2 _only_ supports MySQL. But I suppose you mean the server version, as opposed to the Embedded version we are currently using.\r\n\r\nConcerning this, no, it's not yet in SVN. And we're in feature freeze (soft freeze though), so it's unclear if this is still going to happen in time for 2.1.\r\n"
    author: "Mark Kretschmann"
  - subject: "why the gaga?"
    date: 2009-05-24
    body: "Aren't you happy with the new release and all? I thought every one would hihi... enjoy -:)\r\n<span style=\"color:white\">worldclass for models clothing shopping  <a href=\"http://fashion4us.com\" style=\"color:white\">hong kong fashion</a> garments tops.</span>"
    author: "peterpoboski"
---
The Amarok team is proud to announce the release of Amarok 2.0.2, codenamed "Only Time Will Tell". It is a maintenance release that fixes some critical bugs while we are working hard on the next major version. A list of all changes can be found in the <a href="http://amarok.kde.org/en/releases/2.0.2">release announcement</a>.<br>
<a href="http://dot.kde.org/sites/default/files/amarok2.0.2.png"><img src="http://dot.kde.org/sites/default/files/amarok2.0.2t.png"></a>