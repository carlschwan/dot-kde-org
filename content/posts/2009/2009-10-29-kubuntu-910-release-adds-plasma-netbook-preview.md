---
title: "Kubuntu 9.10 Release Adds Plasma Netbook Preview"
date:    2009-10-29
authors:
  - "nightrose"
slug:    kubuntu-910-release-adds-plasma-netbook-preview
comments:
  - subject: "Wooh!"
    date: 2009-10-29
    body: "i am so looking forward to seeing this thing on the Touchbook! That device was just plain MADE for plasma-netbook! :)"
    author: "leinir"
  - subject: "Kbuntu First Class?"
    date: 2009-10-29
    body: "Is the Kbuntu release better than the prior ones? It seems that SUSE has the best KDE distro. I used to try Kbuntu, but it never worked. I don't know if this was KDE's problem or the distros. \r\n\r\nI'm also tempted to wait for the next KDE release. What do you advise for someone who isn't in a hurry?\r\n"
    author: "Scorp1us"
  - subject: "this is a tech preview, so if"
    date: 2009-10-29
    body: "this is a tech preview, so if you are not in a hurry, don't install it. while it certainly is nice, it's more of a testcase for the 10.04 release (the plasma netbook interface isn't even officialy released yet, but will be with 4.4 which will be in 10.04)"
    author: "Asraniel"
  - subject: "Possible confusion"
    date: 2009-10-30
    body: "I'm not sure the original poster was specifically interested in the plasma netbook mode, or in Kubuntu/KDE more generally.\r\n\r\nI run Kubuntu 9.04 (I haven't upgraded to 9.10 yet, and won't for a little bit yet), but I have have installed OpenSUSE 11.2 milestone 7 on another machine (and I should say, not one I use on a daily basis). There are indeed some nice things about the SUSE version that I enjoy. But, I'd also say that many of the remaining bugs that annoy me personally are upstream bugs, and thus common to both distros. Maybe I'm just not very picky, but in general I don't see the reason for widespread Kubuntu-bashing by comparison to other distros.\r\n"
    author: "tangent"
  - subject: "Small screen Kubuntu will help to improve visual appearance"
    date: 2009-10-30
    body: "9.10 feels much better than the last release but still it is a bit unpolished. What makes Kubuntu suffer compared to other distributions and Linux Desktop Environments are pretty small visual glitches and attention to details. Many of them could be caught by automated testing. It is a fantastic idea to customize Kubuntu for smaller screens. This will help to overcome the current immaturity and get KDE better and better. For instance: \r\n\r\nA. When you take the KUbuntu App \"about\" dialogue it reads: \"License: Gnu General Public License Version\" where 2 is cut away. The About dialogue says: \"About Konqueror, Konqueror, Version 4.3.2 (KDE 4.3.2) Using KDE 4.3.2 (KDE 4.3.2)\"\r\n\r\nB. Default Wallpaper is AIR which is just not good enough anymore. Creates a pretty negative impression and the circles visually overlay with the startup.\r\n\r\nC. Missing Icons, even in Konqueror: Tools > Minitools\r\n\r\nD. Crash cows like the installation of nonplasma widgets are deactivated. But not the selection dialogues, with a single option.\r\n\r\nE. Default width settings are always a problem for KDE applications. For \"places\" in Dolphin it does not scale. Or for Akregator where the allocated space for title is far too small. \r\nhttp://bayimg.com/naEEaaACK\r\nIt seems to be a problem which occurs with KDE applications all the time, so it is toolkit related. There is probably no good default algorithm to makes such list rows fit. Of course just an issue for first time use.\r\n\r\nF. Strange empty space http://bayimg.com/naeeiAacK "
    author: "vinum"
  - subject: "I gave it a shot"
    date: 2009-10-30
    body: "I was a kubuntu user for about a year. Jaunty changed that really quick. \r\nI've given this release a shot and it looks pretty decent. At least the wireless works, mostly.\r\n\r\nI didn't have it up for more than five minutes before plasma crashed on me. The latest Mandriva 2010 RC has been more stable on my hardware than this bad karma.\r\n\r\nThanks to the developers anyways. Having KDE available and getting more coverage in the gnome-centric Ubuntu crowd has been a good thing."
    author: "tubasoldier"
  - subject: "Those are all issues with KDE"
    date: 2009-10-30
    body: "Those are all issues with KDE itself, aside from F. (F. is that big so that larger directories can be listed. It's really a matter of preference there)"
    author: "JontheEchidna"
  - subject: "Yes, Kubuntu 9.10 rocks"
    date: 2009-10-31
    body: "I assume that you mean Kubuntu 9.10 in general. If so, then I can assure you: It's an excellent release.\r\n\r\nThis highlights the fantastic work being done by Jonathan Riddell and the other contributors of Kubuntu. And all this is despite the low priority given to it by Canonical. It is pretty amazing what the Kubuntu team achieves with their rather limited resources :)\r\n\r\nAll that said, I'm wishing that Kubuntu will get more resources allocated by Canonical in the future. KDE is technical excellence, and now it's also excellence in usability. There is no reason to favor the competition any more.\r\n"
    author: "Mark Kretschmann"
---
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: left" />
<img src="http://dot.kde.org/sites/dot.kde.org/files/screenshot-newspaper-wee.png" width="350" height="219" />
</div>

KDE-based distribution Kubuntu has <a href="http://kubuntu.org/news/9.10-release">released version 9.10</a> which adds a new variant showcasing the up and coming Plasma Netbook setup. The release also adds OpenOffice KDE 4 integration and extra installer beauty thanks to artwork from KDE's Oxygen team.
<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right" />
<a href="http://www.flickr.com/photos/freeflying/4036438792/in/set-72157622515083587/"><img src="http://dot.kde.org/sites/dot.kde.org/files/arm.jpg" width="160" height="240" /></a>
Plasma Netbook on ARM board
</div>

The new <a href="https://wiki.kubuntu.org/Kubuntu/Netbook">Kubuntu Netbook Edition</a> is intended for computers with small screens, limited processing power and no CD drive. This initial release is a technical preview to show the progress being made by Plasma developers on this exciting new use case for KDE. Plasma developer Artur de Souza said on the release: "We've been working with the the Kubuntu developers to make KDE ready for a Netbook platform. Not everything is implemented yet, but it clearly shows the direction we are working in." The release is available for common i386 processors, but is also available in experimental form for ARM processors (pictured).

On Kubuntu 9.10 generally, developer Jonathan Riddell said: "This release is a notable improvement in quality over our previous KDE 4-based releases. Thanks to the hard work of KDE developers, vital elements such as network connection management are now much more reliable."

One small touch for KDE developers, a Qt SDK meta-package has been added to make it easy to install development tools for the world's best GUI toolkit.

Grab your copy now <a href="http://www.kubuntu.org/getkubuntu">from the download page</a> (best to use BitTorrent at this busy time) or if your connection is too slow for downloading CDs you can <a href="https://shipit.kubuntu.org/">order a CD free from ShipIt</a>.