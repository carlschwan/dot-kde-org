---
title: "New Look And Features For KDE Community Forums"
date:    2009-06-29
authors:
  - "einar"
slug:    new-look-and-features-kde-community-forums
comments:
  - subject: "Link"
    date: 2009-06-29
    body: "We seem to be missing <a href=\"http://forum.kde.org\">a link to the forum itself</a> :-)"
    author: "Haakon"
  - subject: "Good point, added!"
    date: 2009-06-29
    body: "Good point, added!"
    author: "danimo"
  - subject: "and brainstorm.forum.kde.org"
    date: 2009-06-29
    body: "and brainstorm.forum.kde.org isn't working any more as well.\r\n\r\nthe page looks really amazing. the forum was a really great idea."
    author: "chris"
  - subject: "Great job guys"
    date: 2009-06-29
    body: "New UI looks fantabulous...great job"
    author: "indishark"
  - subject: "and brainstorm.forum.kde.org"
    date: 2009-06-29
    body: "This is fixed now, address is working again."
    author: "neverendingo"
  - subject: "Can we have a mobile-devices layout?"
    date: 2009-06-30
    body: "The new design has a fixed-with layout which makes it impossible to use on a mobile device (since I need to scroll for each line). I'm getting used that KDE websites consistently fail on that part, but at least I can bring it up again ;)"
    author: "zander"
  - subject: "Good point! Will suggest it"
    date: 2009-06-30
    body: "Good point! Will suggest it to the rest of the team."
    author: "einar"
  - subject: "Forum software"
    date: 2009-07-01
    body: "Hello,\r\n\r\nwhen forum was launched last october you have been very enthusiasic about MyBB.\r\nWhy did you change the forum software?\r\n\r\nCheers,\r\nMark\r\n\r\nBtw: Pretty nice style\r\n"
    author: "Mark Ziegler"
  - subject: "Lovely"
    date: 2009-07-01
    body: "Very very smart design :)\r\n\r\nThere's one thing though, the list of topics can do with a bit of love. The most important bit of it are the topic names of course because that's what you use to identify threads *and* access them as they're also links. They're not highlighted in any way other than a blue font.\r\n\r\nI also think the grey box at the top is far too big for the content it has (at least when not logged in).\r\n\r\nI have done some css tweaks to illustrate my point:\r\n\r\n<ul>\r\n<li><strong>Original:</strong> http://img43.imageshack.us/img43/1499/originalb.png</li>\r\n<li><strong>Modified:</strong> http://img43.imageshack.us/img43/4840/modified.png</li>\r\n</ul>\r\n\r\nBodge stylish css:\r\n<blockquote><font face=\"courier\" size=\"1\">@namespace url(http://www.w3.org/1999/xhtml);\r\n@-moz-document domain(\"forum.kde.org\") {\r\na.topictitle { font-weight: bold !important; }\r\nul.linklist li {display: inline; margin-right: 2em !important;}\r\nul.linklist li a {font-weight: bold !important; }\r\n.linklist {position: relative; float: right;}\r\n.linklist {position: relative; float: right;}\r\n.panel-mid { min-height: 3em; }\r\n}</font></blockquote>\r\n\r\nOf course it would need proper styles but hope you catch what I mean :)\r\n\r\n"
    author: "NabLa"
  - subject: "Team change"
    date: 2009-07-01
    body: "The main reason is the change in the team. When the forums started there were just Ingo and Rob (which later retired from admining). \r\n\r\nNow we're a lot more, so needs and workflows changed: there was also a theme change planned so we took the opportunity to switch to a system most of the team was more comfortable with."
    author: "einar"
  - subject: "Thanks"
    date: 2009-07-01
    body: "... for the suggestion. Indeed it looks nice, and i will integrate it into the next rollout.\r\nAbout bold topictitles, not sure, though. That is used to highlight updated or new topics."
    author: "neverendingo"
  - subject: "Good point... maybe can be"
    date: 2009-07-02
    body: "Good point... maybe can be had both ways? how about bold for topic titles, and a different (darker?) row colour for new/updated topics? "
    author: "NabLa"
---
During this weekend (and after almost 24 hours of work), the <a href="http://forum.kde.org/">KDE Community Forums</a> have undergone a complete overhaul, resulting in a much improved look and additional features. The first and most user-visible change is the new forum theme. It is heavily inspired by "Air", the default Plasma theme in the soon to be released KDE 4.3.

<a href="http://dot.kde.org/sites/dot.kde.org/files/newforum.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/newforum_resized.png" style="float:right" /></a>

Also, the board is now powered by the <a href="http://phpbb.com">phpBB forum software</a> and has gained a number of extra features, including but not limited to:
<ul>
<li>Improved KDE Brainstorm section, with a new user interface;</li>
<li>A "friends connection" system that enables people to mark other users as
friends and easily interact with them;</li>
<li>Improved user reputation system;</li>
<li>Topic tagging;</li>
</ul>

The changes are quite numerous and deep, so some bugs may have slipped through our tests. The whole KDE Community is encouraged to visit the forums and <a href="http://forum.kde.org/viewtopic.php?f=9&t=62056">report issues</a>.

As a final note, thanks go to the people who made this possible: the KDE Community Forums administrators (Ben Cooksley, Ingo Malchow, Sayak Banerjee), the phpBB developers Chris Smith, Ashley Pinner and Nils Adermann for their continued assistance and support and to the entire phpBB team.
<!--break-->
