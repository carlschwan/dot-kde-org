---
title: "Team Sets Seventh Beta of KDevelop4 Loose"
date:    2009-12-16
authors:
  - "apaku"
slug:    team-sets-seventh-beta-kdevelop4-loose
comments:
  - subject: "Nice birthday present :D"
    date: 2009-12-16
    body: "I'd never have guessed you remember my birthday! Thanks!\r\n\r\nKDevelop ROCKs, everyone who codes C++ should try it. Sure, it still crashes occasionally, but the stuff ctrl-space can do for you is worth it alone, and then there is the \"show usage\", mouse hover and other goodies. \r\n\r\n"
    author: "esben"
  - subject: "Agreed - Great Work!"
    date: 2009-12-16
    body: "I've been using since long before it was halfway stable, just because of the sheer awesomeness of it.  (For my purposes,) it blows any other FOSS IDE that I've tried out of the water, and a lot of its features show up commercial offerings too.  Fully recommended for any C/C++ coder (and its other supported languages are probably good too, but I haven't tried them.)"
    author: "ffejery"
  - subject: "Reverse Debugging"
    date: 2009-12-17
    body: "Will KDevelop support the newest features of GDB, specifically reverse debugging?"
    author: "HandyGandy"
  - subject: "Documentation problems"
    date: 2009-12-17
    body: "This might be a bit off-topic but I need to rant:\r\nI have tried many times using kdevelop but always end up giving up because I don't know how to use it and can't find documentation for it :-/\r\n\r\n(Latest attempt was on kdevelop 3.9.95, I never tried version 4.X, hence the possible off-topicness of this post)\r\n\r\nTrying the \"Kdevelop handbook\" option from the \"help\" menu item shows an error message and googling this error message returns a bug report saying that \"it's normal, no one has written that documentation\".\r\n\r\nI tried following four year old tutorials on http://docs.kde.org/kde3/en/kdevelop/kdevelop/ but they most likely refer to four year old versions of kdevelop and had to give up when they referred to menu items, options, gadgets, that no longer exist, and to which I couldn't find any equivalent even after going through every single menu.\r\n\r\nThere's that \"documentation\" sidebar that's supposed to show informations about APIs but I couldn't get beyond the \"there's no documentation selected yet\", and here again the tutorial referred to \"update config\" or \"update index\" buttons that do not exist in my copy of kdevelop.\r\n\r\nSorry.\r\n\r\nBTW is there supposed to be a difference between \"Filtered HTML\" and \"Full HTML\"? Their description is character-for-character identical."
    author: "tendays"
  - subject: "Great Work"
    date: 2009-12-17
    body: "I tried Kdevelop4 some weeks ago and I was impressed about the provided functionality and the way it is integrated in the user interface!\r\nKeep up the great work.\r\n"
    author: "infopipe"
  - subject: "Out of curiosity, what kind"
    date: 2009-12-17
    body: "Out of curiosity, what kind of documentation are you expecting?\r\n- getting started with coding?\r\n- API documentation?\r\n- kdevelop user interface explainations?\r\n- how to's for specific tasks (creating a project / add file / build project / etc..)"
    author: "vdboor"
  - subject: "Re"
    date: 2009-12-17
    body: "The last three, basically, with increasing importance.\r\n\r\nThat (outdated) kdevelop handbook I looked at has a good layout and would cover the last two elements:\r\nIt starts with \"Getting Started with KDevelop \u2014 a Guided Tour\" which is basically an ordered sequence of howtos (so I can get started with a hello-world project that compiles, and understand all steps involved in that).\r\nThen it proceeds with a reference of kdevelop's user interface for stuff that isn't covered in a \"guided tour\" but should be documented somewhere nevertheless.\r\n\r\nThen, API documentations, IOW a working \"documentation\" pane that I can use to search for (kde) classes, or know what parameters this or that method takes, along with their explanations.\r\nThis part is less crucial because googling class names tends to return documentation. At worst I can go through the header files.\r\n\r\nThere are tons of generic tutorials and books on coding in C/C++ already so the first point is fine (and that wouldn't be the job of kdevelop people anyway)."
    author: "tendays"
  - subject: "\"Latest attempt was on"
    date: 2009-12-18
    body: "\"Latest attempt was on kdevelop 3.9.95, I never tried version 4.X\"\r\n\r\nActually, 3.9.95 is 4.0 Beta 5. The number is smaller than 4.0 because the versions have to be ordered, and 4.0 is the final version."
    author: "majewsky"
  - subject: "!"
    date: 2009-12-19
    body: "Ah this explains some of my problems!\r\n\r\nApparently I got hit by this: http://apaku.wordpress.com/2009/11/10/dont-install-ubuntu-9-10-if-you-want-a-stable-kdevelop/\r\n\r\nI'll probably compile kdevelop3 as advised on that post, or wait for kdevelop4 to become stable (and well-documented ;-) )\r\n\r\nThanks."
    author: "tendays"
---
The KDevelop team is proud to announce the seventh beta of KDevelop4. At the same time we're a bit sad as this beta also marks our drop-out of the KDE SC 4.4 release cycle. We feel that we didn't manage to get the needed features for the 4.4.0 release working properly and that we'll need a longer freeze period than what is available in the release cycle. We're now concentrating on getting the existing features shaped up and ready for release as well as fixing as many of the bugs as we can. No new features will be introduced into KDevelop anymore until the first release, which is currently aimed at end of March.
<!--break-->
This beta mainly brings stability improvements (see the list of <a href="http://bugs.kde.org/buglist.cgi?product=kdevelop&resolution=FIXED&resolution=WORKSFORME&bugidtype=include&chfield=resolution&chfieldfrom=2009-11-01&chfieldto=2009-12-11&order=Bug+Number&cmdtype=doit">fixed bugs</a>) over the last one and a much improved session management allowing you to easily switch between different kinds of projects. This is already used by the developers working on the PHP plugin so they can switch between working on the plugin and doing their actual PHP work.

This leads us to the PHP and PHP documentation plugins, which again are accompanying this KDevelop4 beta. The second beta of kdevelop-php and kdevelop-phpdocs is also a mostly an incremental improvement over the first beta. Apart from fixing various bugs, the parsing performance for PHP files got greatly improved, making parsing big files like the internal one for php functions a lot faster.

You can find the source packages on the <a href="http://download.kde.org/unstable/kdevelop/3.9.97/src">KDE mirrors</a> and there are <a href="http://www.kdevelop.org/mediawiki/index.php/KDevelop_4/compiling">instructions</a> for compiling. You can also check out a <a href="http://www.kdevelop.org/mediawiki/index.php/KDevelop_4/KDev3_KDev4_comparison_table">comparison of the KDE 3 and KDE 4 versions of KDevelop</a>.

Please make sure the md5sum of the downloaded source packages match the following list as there was a last-minute update on the kdevplatform tarball:
bfe385bc471d4d9abf4e989ca203db86  kdevplatform-0.9.97.tar.bz2
260b4eae5962c1e57941ebbb64b42bb8  kdevelop-3.9.97.tar.bz2
edc84d426f7a1492995ac4fcde39992f  kdevelop-php-beta2.tar.bz2
2ea2bea4c0b3642db155c916af6689f3  kdevelop-phpdocs-beta2.tar.bz2