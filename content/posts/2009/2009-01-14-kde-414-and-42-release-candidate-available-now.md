---
title: "KDE 4.1.4 and 4.2 Release Candidate Available Now"
date:    2009-01-14
authors:
  - "sk\u00fcgler"
slug:    kde-414-and-42-release-candidate-available-now
comments:
  - subject: "Go!"
    date: 2009-01-14
    body: "KDE 4.2 is my home now, and I'm not planning to look back. Thank you!"
    author: "Alejandro Nova"
  - subject: "suse repos"
    date: 2009-01-14
    body: "drat! no suse packages yet...\n\nThe suse repos are usually so punctual. I'm surprised packages aren't in yet. I thought tagging was done early so that packages would be ready when released."
    author: "txf"
  - subject: "Re: suse repos"
    date: 2009-01-14
    body: "check KDE:KDE4:Factory:Desktop repositories, I guess they are in there."
    author: "Burke"
  - subject: "Re: suse repos"
    date: 2009-01-14
    body: "Yeah, we screwed Beineri on that one, unfortunately. We wanted to put out -rc rather quickly to give it more time for testing. (Some days ago meantime, we said we'd put it on the shelf until Thursday, but it was just easier to put it up quickly.)\n\nWork on OpenSuse packages is under way though, as I've heard."
    author: "sebas"
  - subject: "Re: suse repos"
    date: 2009-01-14
    body: "Two reasons:\n\n- Despite the planning on the release-team list for the last week to have the 4.1.4 and 4.2rc1 releases NOT coincide but have 4.2rc on Thursday, the release was suddenly on Tuesday evening pushed forward two days to Tuesday night (and final 4.2rc1 release tarballs were available on Tuesday only too).\n\n- The Build Service started to suffer heavily from its own success yesterday: the schedule server is regularly running out of memory (\"only\" 8GB RAM available atm to calculate $WORLD's dependencies) and is starting new build jobs only very delayed. This could not be fixed on Tuesday anymore.\n\n\n"
    author: "binner"
  - subject: "Re: suse repos"
    date: 2009-01-14
    body: "Well, I'm grateful for all the work the OpenSUSE team does, so waiting means nothing.\n"
    author: "R. J."
  - subject: "Re: suse repos"
    date: 2009-01-14
    body: "Yes, indeed! And the build service is so very, very useful in itself, too."
    author: "Boudewijn Rempt"
  - subject: "Re: suse repos"
    date: 2009-01-14
    body: "will there be a supported or semi-supported way to use 4.2 on opensuse 11.1 installation ?\nthanks for the work on kde, i was surprised how usable 11.1 with kde4 actually was ;)"
    author: "richlv"
  - subject: "Re: suse repos"
    date: 2009-01-14
    body: "opensuse-kde@opensuse.org and #opensuse-kde on FreeNode give you as much real support as the KDE 4.1.4 packages released with 11.1 have.\n\nSince we're continually developing the next version of openSUSE, it makes sense to get people using our packages of the newest releases of KDE so they get bugtested early."
    author: "Will Stephenson"
  - subject: "Re: suse repos"
    date: 2009-01-14
    body: "Although I really like the fact that the openSUSE guys have provided KDE 4.2 in the repos, I've got just one single request:\n\nPlease, please, pleasssse provide KDE with it's default theming. KDE 4.2 does look so much better with default Oxygen than the current Aya theme.\n\nI can imagine openSUSE would want to brand the desktop. That's not a problem. I can live with the lizard as the menu button, but the Aya theme is just plain ugly.\n\nRegards Harry"
    author: "harry"
  - subject: "Re: suse repos"
    date: 2009-01-14
    body: "I agree, and the same goes for Mandriva, although their theme looks a little bit better than the openSUSE one.\n\nI wouldn't mind using Oxygen with some green-ish tints to it or something, but the default openSUSE/Mandriva is downright ugly compared to default KDE."
    author: "unbob"
  - subject: "Re: suse repos"
    date: 2009-01-14
    body: "I agree...Anyway I have to add that aya improved a lot in kde 4.2...it's really better to my eyes...in opensuse 11.1 I'm a little embarassed to show the default installation of kde4.1 because it's really ugly. :)\nNot so anymore with 4.2 packages and the new aya...\nThat said I REALLY hope that openSUSE switches to kde 4.2 upstream plasma theme...in an ideal world i'd like a coherent visual experience with upstream from poweron to poweroff, exept openSUSE brands (kmenu, opensuse logo in a OXYGEN styled bootsplash and so on) "
    author: "Giovanni Masucci"
  - subject: "Re: suse repos"
    date: 2009-01-14
    body: "Last time I checked, Oxygen doesn't recognize the system's color settings though.  It would be nice to have a well designed theme that looked sharp, and yet also respected system color settings."
    author: "T. J. Brumfield"
  - subject: "Re: suse repos"
    date: 2009-01-18
    body: "A default theme must either obey system color settings or the user should be able to change its colors. A theme with fixed colors that cannot be changed shouldn't be the default because users will be pissed off (they could be changed even in Windows 95 after all). OTOH it may be cool as optional.\n\nI don't think Aya is ugly (or uglier than KDE 3 or classic Windows or default GNOME), it is just not \"cool\" but more \"traditional\". And in KDE the consistency and configurability should be prefered over eye-candy, although unfortunately we can see the opposite in some places in KDE 4."
    author: "Gr\u00f3sz D\u00e1niel"
  - subject: "Re: suse repos"
    date: 2009-01-15
    body: "Packages for 11.0 and 11.1 are meanwhile available."
    author: "binner"
  - subject: "Kubuntu Jaunty Packages"
    date: 2009-01-14
    body: "The Kubuntu Jaunty packages are being built as we speak. \nhttp://launchpad.net/+builds\n\nHopefully they will be done in a few hours. I'm on beta2 and am quite excited for the RC."
    author: "Andrew Stromme"
  - subject: "Re: Kubuntu Jaunty Packages"
    date: 2009-01-14
    body: "I think the only ones we are waiting on now are the amd64 packages. (The amd64 servers are being a bit slow today...)"
    author: "Jonathan Thomas"
  - subject: "Re: Kubuntu Jaunty Packages"
    date: 2009-01-14
    body: "This worries me a lot:\namd64 28 builds waiting in queue\ncrested\tAUTO Idle\nyellow \tAUTO Idle\n\nI have 30+ kde 4.2 packages being marked to remove because they all depend on kdebase-runtime which still isn't bumped to 4.1.96. And both amd64 machines are idle?"
    author: "Martin"
  - subject: "Re: Kubuntu Jaunty Packages"
    date: 2009-01-14
    body: "thanks to anyone making this happen ( :"
    author: "jay"
  - subject: "Re: Kubuntu Jaunty Packages"
    date: 2009-01-15
    body: "There's a conflict between kscreensaver-xsaver and kscreensaver-xsaver-extras that prevent one of them to be installed (a file belonging to both packages), other than that installed and working fine :)"
    author: "NabLa"
  - subject: "Re: Kubuntu Jaunty Packages"
    date: 2009-01-15
    body: "Sorry, I forgot to mention, this is on 8.10 packages."
    author: "NabLa"
  - subject: "KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "\nThe KDE 4.2 will be the state what persons who are not familiar with computers can use as desktop.\n\nThere is great new features and most important thing... more features ;-)\n\nI currently have a 4.1.96 version (>Beta2 <RC1 ?) and I just like it. Only few things are missing for me... unique wallpaper for every virtual desktop, Mac OS X kind menubar top of screen (Xbar from bespin menu and there is test plasmoid on playground) and few small things there and here... \n\nNothing \"important\" missing what was case on 4.0 version, why it got so bad welcome from older users who did not read first it is for development... I have used KDE4 since first betas and I would say... I like how KDE4 has evolved in last 3 years.... to mature desktop what will shame all others desktop on *nix systems or other than *nix OS's (actually there is only a Windows NT ;-) ) \n\nDo not count me as bad messenger, but I am already looking forward to 4.3... If it will get such great features and changed than 4.1 -> 4.2... It will rock more.\n\nIf Aaron Seigo saw this in his dreams when the muse was bringing the Plasma idea to his brains... and the community could share that idea and actually make it happend (and build it even better ;-) ) It was very badly explained to normal users what to wait ;-)\n\n"
    author: "Fri13"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "> unique wallpaper for every virtual desktop\n\nYou can get that by putting a line perVirtualDesktopViews=true in your plasmarc. It's an experimental feature we've implemented that doesn't have a configuration UI yet.\n"
    author: "sebas"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "To what location it should be placed? To first [Containments][101] or just end of file? I really would like to test this ;-)"
    author: "Fri13"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "You seem to be looking at plasma-appletsrc, while it's in plasmarc.\n\nTry the [PlasmaViews] group. (I didn't use this yet, so I'm relying on 3rd party information here ;-)"
    author: "sebas"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "kdebase/workspace/plasma/shells/desktop/kcfg/plasma-shell-desktop.kcfg implies that it's the [General] group. (Create it if it doesn't exist.) Unfortunately, the only effect that I can see is that the background image is now indeed no longer shared by the desktops. The other desktops, however, are empty and I didn't yet find a way to add other backgrounds there.\n\nThis is the last major reason why I hadn't yet switched to KDE4. (Well, and the fact that I don't know yet if I can use konsole in a sane way -- full size without decoration, but not in \"fullscreen\" mode. I still want to be able to use the autohide panel and switch desktops while I'm working with it.)"
    author: "Melchior FRANZ"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "\"Well, and the fact that I don't know yet if I can use konsole in a sane way -- full size without decoration, but not in \"fullscreen\" mode.\"\n\nYou can. I maximised the konsole window, disabled the border via Alt+F3 and then set konsole never show tab-bar. Then I enabled the auto-hide to panel and I got \"fullscreen\" konsole with panel coming top of it when moving mouse bottom of screen etc... needed to made a profile for it ;-)\n\nIt is nice to get the TTY feeling when having all great functions from konsole.. and KWin effects ;-)"
    author: "Fri13"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "Ahh, thanks. I had used a different method in KDE3 with dcop commands, so that I only had to type \"fs\" in a konsole window and it would do the necessary stuff. Under KDE4 dbus seems rather limited: I could figure out how to maximize the window (qdbus org.kde.konsole-$pid /konsole/MainWindow_1 com.trolltech.Qt.QWidget.showMaximized), but not how to remove/restore the decoration. Of course, doing it by hand is better than nothing."
    author: "Melchior FRANZ"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "\nMayby you could do it totally automatic.\n\nSet Konsole profile the tabs etc and then set the profile to have own unique name on it what the window is called. Then make the KWin profile for that unique name what you gave to profile. Store both profiles and add a konsole profile-manager plasmoid to desktop what launch the wanted \"fullscreen\" profile, what gets set as correctly with KWin ;-)"
    author: "Fri13"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "I had tried that, but this works only on newly created windows, which isn't enough. I'd really prefer a complete set of DBUS commands for KWin. One can't do much with what's available now."
    author: "Melchior FRANZ"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "File a wishlist, giving a complete list of those you require and the affect they should have."
    author: "Anon"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-15
    body: "Err ... KWin operations have a defined effect, so there's not much to describe. But meanwhile I've added the missing DBus methods myself. So for me it's just one script command in a konsole, and it goes full-size without decoration and menubar, or back to normal. /me happy now.  :-)  (Yeah, I'll submit it.)"
    author: "Melchior FRANZ"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-15
    body: "https://bugs.kde.org/show_bug.cgi?id=180855"
    author: "Melchior FRANZ"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-16
    body: "Speaking of being proactive :)"
    author: "NabLa"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-15
    body: "about plasmarc -> [General] -> perVirtualDesktopViews=true\n\nI also had to adjust the respective [containment]s manually, setting desktop and screen to subsequent numbers, and added [Containments][*][Wallpaper][image] entries (the latter of which was probably not necessary). Now it works!  :-)"
    author: "Melchior FRANZ"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "> unique wallpaper for every virtual desktop\n\nYou can get that by putting a line perVirtualDesktopViews=true in your plasmarc. It's an experimental feature we've implemented that doesn't have a configuration UI yet.\n"
    author: "sebas"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "that's REALLY nice. :)\nThanks for the tip and congratulations to all kde developers for this great release! 4.2 is going to rock."
    author: "WOW, that's REALLY great :)"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "\"but I am already looking forward to 4.3'\n\nyeah, so am i =P\n\n\"If Aaron Seigo saw this in his dreams when the muse was bringing the Plasma idea to his brains...\"\n\nhonestly, we're about half way there. there are a few more fundamental pieces missing, but we're getting there at tremendous speed.\n\nthe ability to send widgets across the network between machines, timelining, social networking, a design studio and a few other bits and knobbles ... nothing a few more lines of code can't cure ;)\n\n\" and the community could share that idea and actually make it happend (and build it even better ;-) )\"\n\nthat's been the joy of plasma: other people have happened along, looked at it, \"got\" it and then dove in and made it better than i ever could have alone. our community is beyond great."
    author: "Aaron Seigo"
  - subject: "Re: KDE 4.2 Rocks!"
    date: 2009-01-14
    body: "So this would be possible http://kde-look.org/content/show.php/record-plasmoid?content=97428 for someone to make right now or does it has features what still is missing on current plasma?\n\nI heard on #kde someone saying (actually readed, because IRC does not transmit the sound... yet) that Phonon layer only support playback but not recording.\n\nI have trying to find nice and esay application to allow user to do video journal with their laptops (webcam is almost 100% integrated top of the screen) or sound journals. Because I did not find one good application for that, I tought what if someone with skills could do that as plasmoid if plasma just would support it. So, there it is hanging waiting someone.\n\nThe Plasma idea is still \"missing\" from some people but soon when they understand that plasma is not just the widgets and plasmoids... it is the desktop itself. They will got it. For me it looks that KDE4 really is braking the old habits of desktop usage. "
    author: "Fri13"
  - subject: "Bug in folderview?"
    date: 2009-01-14
    body: "Please could someone, especially with an intel graphic card, test this?\nWith desktop effects on, and folderview as a desktop background tra to move icons on the desktop...especially selecting them with the rubberband...in my kde 4.1.87 installation this lead to 100% cpu usage taken by kwin/xorg. Then everything become dog slow until moving is finished...sometimes kwin even kills composite because it was taking too many resources...moving icons on the desktop should not be so cpu expansive and this wasn't happening in kde 4.2 beta2.\nI don't know if this is fixed in 4.2rc1 and I cannot test this, since I'm not with a 4.2rc installation at the moment...if somebody conferm  that the bug still exists I'll be happy to report it in bugs.kde.org\n"
    author: "Giovanni"
  - subject: "Re: Bug in folderview?"
    date: 2009-01-14
    body: "I've test this on my dell inspiron latop with intel graphic card, effects enabled. When I move icons selected with rubberband... it's a bit slow but it works. xorg & plasma 14% cpu."
    author: "RG"
  - subject: "Re: Bug in folderview?"
    date: 2009-01-14
    body: "that's probably because inspiron has a better cpu than my netbook atom.\nI'll report the bug, but I'm happy it's not such a serious one for people with a fast cpu. :)"
    author: "Giovanni"
  - subject: "Re: Bug in folderview?"
    date: 2009-01-14
    body: "https://bugs.kde.org/show_bug.cgi?id=168924\nreported :)"
    author: "Giovanni"
  - subject: ":("
    date: 2009-01-14
    body: "My Printer( brother HL 2150N ) no print ebooks :(. In Kde beta 2, it does."
    author: "Anonimous"
  - subject: "Fast, faster, RC !!!"
    date: 2009-01-14
    body: "WOW! The RC is incredibly fast now! Thank you very much, thats great! It all works fine now!"
    author: "SONA"
  - subject: "Is it so good?"
    date: 2009-01-14
    body: "In my previous dot comments I wrote about bugs, missing features,\nquestionable plasma functionality. Although none of stuff I mentioned were fixed or\nexplained and my KDE still looks like this:\nhttp://dot.kde.org/1229613785/1229633767/1229634780/1229638970/1229704539/kde42b2.jpeg\nI won't talk about it, but rather about how good KDE4.2 looks.\n\nLook at the attached screenshot, in hardware kcm in systemsettings, it shows only a few lines of information and there are so many empty space that content doesn't fit in, so vertical\nscrollbar is required. \nOn the other hand, in Kate there's no content at all\nbut we have dead scrollbars. Probably devs want to show us how great they\nlook.\nWhy is there so much empty space around menubar, toolbar, statusbar?\nLook at the kate's edit menu. Why is there so much empty space on the left, right\nand between the items? Some of us don't have 1900x1200.\nThere's a image loaded in okular but you maybe didn't noticed it because you\ncan barely see where interface (toolbar, sidebar) ends and content starts.\nKate does that better.\nFinally look at the highlight color, it's yellow, and in plasma taskbar it's\nblue. Inconsistent, looks very unprofessional.\n\nStill not there...\n\nRegards!\n"
    author: "vf"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "4.2 is awesome!\n\nbtw.. I also hate the empty space around menubar and another problem is the column width! A lot of programs doesn't have a useful column width (e.g. akregator) IMHO a no go. "
    author: "Dude"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "Yes, since Oxygen was released I complain about that, and this is the reason why I use KDE 3 on my Asus eeePC 701 :-(\nBut this will not be solved just by changin the theme to something smaller, all programs should focus into using less space, otherwise KDE4 will be used only in bug LCD displays (my computer @home does it)."
    author: "Iuri Fiedoruk"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "I actually find KDE 4.2 quite usable on my Eee PC 900."
    author: "Luca Beltrame"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "Imagine then how much more usable would it be if they would remove all that dead space and unneeded scrollbars. It would be as if you had 1-2 inches bigger screen. Or don't imagine, try something else and see how much more content fits in the screen.\n\n"
    author: "vf"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "The fact is, I'm happy with KDE 4.2 even on my Eee, and there's no compelling reason to switch (I don't like GNOME and now I can't stand 3.5.x anymore)."
    author: "Luca Beltrame"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "Sad thing is that some people will accept everything no matter the quality of the product. It doesn't have nothing to do with KDE (some users even like (old) gtk2 file dialogs!!), I see it everywhere. Others call them fan(atic)s. :-)\n\n"
    author: "vf"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "Call it whatever you want. I've actually increased my productivity with a proper use of ZUI, Plasma and activities, I'm enjoying also the other KDE applications I use often either for leisure or work (Dolphin, kopete, kontact, krdc, Amarok 2, kate, gwenview, digikam KDE4 beta...). The only serious non-KDE app I use is (g)vim.\n\nP.S.: I've been using KDE since the 1 era."
    author: "Luca Beltrame"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "\"increased my productivity with a proper use of ZUI, Plasma and activities\"\n\nHow? Use case example?\n\nOn my desktop computer plasma and whole KDE4 is very slow, like when you play game with old gfx. card and  have only few fps so with KDE4 it's more UI waiting then actually being productive. So event if they fix most bugs and add missing features I would be less productive.\n\n\"P.S.: I've been using KDE since the 1 era.\"\n\nSeems that you didn't use much of old KDE features. There are some people that use them, people who wrote script KDE to do things faster. I cannot do that in KDE4 thus I'm less productive."
    author: "vf"
  - subject: "Re: Is it so good?"
    date: 2009-01-15
    body: "Your computer is irrelevant for the world. I mean, I can't run KDE 4.2 in the computer I easily run e17.\n\nI WILL COMPLAIN!\n\nCome on, boy."
    author: "Luis"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "Oxygen just freed up the space now we need to take control of that space.\nwe need you and more people to go thrugh all the aplication Ui's and make it shine, its not an esay task couse of the huge numbes of use cases out there and all the implications change brings.\n   \"Finally look at the highlight color, it's yellow, and in plasma taskbar it's blue. Inconsistent, looks very unprofessional.\"\nNo it is not yellow its blue :) just like plasma."
    author: "Nuno Pinheiro"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "It's easy, just reduce empty space between all widgets (buttons, *bars, menubars). Maybe some people like it as it is, but for us with smaller screens I would like to see oxygen-compact, like this:\n\nhttp://martin.ankerl.com/2007/11/04/clearlooks-compact-gnome-theme/\n\n\"No it is not yellow its blue :) just like plasma.\"\n\nSo plasma now follows color scheme of oxygen style? That's new to me, I didn't read nothing about it. When was that feature added? Must be very recently.\n"
    author: "vf"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "You should be a bit patient! That last post was only Dec 19, when 4.2 was well on its way to finalization. One doesn't make intrusive changes so late in the game.\n\nI'm an open source developer, but I'm not a KDE developer, and in any case I can't speak for how other people plan to spend their time :-). But I'd guess that the usability team might appreciate your help (you seem to be thinking carefully about usability), and that if you volunteered you might indeed see some of your suggestions implemented for 4.3. Just keep in mind the fact that there is a definite rhythm to releases, that \"issues\" are more often due to a lack of time than any nefarious intentions of the \"devs,\" that it takes _much_ (much!) longer to fix issues than it does to point them out, and that a small amount of patience will make the process less frustrating for everyone.\n\nEveryone knows there are still problems---that's why a KDE 4.3 is planned, after all---but the developers, packagers, artists, documenters, translators, media people, and everyone else who has contributed to KDE4 should be justifiably proud of how far they've come. Get ready to party!\n"
    author: "T"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "I'm patient. I voted for some KDE4 bugs well before even 4.0 was out. They are still open.\n\n\"if you volunteered you might indeed see some of your suggestions implemented for 4.3\"\nI commented a lot about plasma for more than a year...I was ignored. I opened a bugreport about huge menus, it got closed as WONTFIX.\n\nI'm also going to party... because debian still has 3.5.  ;-)\n"
    author: "vf"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "one method is to \"change the show text below icons\" to \"show text alongside icons\", to conserve vertical space :)"
    author: "slashdevdsp"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "Good that you put smiley. :-)\n\nI could also set in kco^H^Hsystemsettings that all apps use small icons in toolbars. Oops, I can't. That's broken since KDE4 technical preview 1. :-)\n"
    author: "vf"
  - subject: "Re: Is it so good?"
    date: 2009-01-15
    body: "Which bug report number?"
    author: "Stefan Majewsky"
  - subject: "Re: Is it so good?"
    date: 2009-01-15
    body: "You mean numberS (as this is frequently reported). You can find the number of the first bugreport (back in 2007) here:\n\nhttp://dot.kde.org/1229613785/1229633767/1229634780/1229638970/1229704539/1229772543/\n\nYou actually commented on my post which included the bugreport # few weeks ago. :-)\n"
    author: "vf"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "I hope you can contribute with issues like this early and help in getting rid of them :)"
    author: "slashdevdsp"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "I'm not going to report bugs about obvious issues. If devs cannot spot them, then they have some bigger problems than those issues."
    author: "vf"
  - subject: "Re: Is it so good?"
    date: 2009-01-15
    body: "well, do you remember that some time ago people complained that comments on digg were broken, but no one filed a bug report because it was so obvious. do you know what, no developer visited digg, so it didn't matter how visible it was because no developer ever saw it.\n\nwhat if the developer use a very high resolution? then the empty space would look very small. so if you really want it to get solved, then you have to point the developers to the problem."
    author: "hias"
  - subject: "Re: Is it so good?"
    date: 2009-01-15
    body: "Well, it's not necessarily a common issue.  People like you and me like things borderless and with no spacing.  I use a 1px maximum border on everything and typically a zero pixel border and a frame based window manager.  As a result, casual users typically have a hard time finding something on the screen even when I'm pointing it out to them.  It seems that the white space we both dislike is necessary to organize screen elements and focus attention for many people.\n\nI'm likely even more aggressive than you regarding removing white space (I don't even tolerate *any* window decoration and just get rid of windows entirely), but I do think that I'm decidedly not who should be catered to if people have a hard time finding words and icons I'm physically pointing to with my finger while describing them.  \n\nI also don't think my \"Super-W Ctrl-A Backspace Ctrl-ESC S Enter kcd\" to jump to the directory open in the Konqueror tab to the left in the running Konsole session is appropriate for most people either, although it rolls off my fingers and I'd hate to give it up.  That said, I'm happy that KDE allows me to configure my system so I can do it that way.  I'd imagine some aggressively compact styles will appear as options for those KDE4 users like you and I who want to render their applications that way.  I'd just hate to see them as the default."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "Why some people keep saying KDE is not ready because of the widgets style? If you do not like Oxygen or think Oxygen takes too much screen space to use it, just change it. There are QTCurve, plastique, bespin and some more. The widget style itself is not an issue to stop using KDE, since there are some more available, just choose one that fits your necessity. \n\nI think Oxygen is indeed in a very good share now. Because of the following:\n\n* KDE 4 is only one year old;\n* There is a few number of coders working in Oxygen now;\n* Coding a widget style can be a VERY VERY difficult process (Believe it, I tried it myself). You have to practically draw everything you see on the widget in C++. \n* Oxygen is indeed BEAUTIFUL right now. Has its issues, but it will be solved as time goes by.\n\nI thing these \"problems\" in Oxygen will be solved, but it DOES take time. Coding a style, once more, is not an easy task nowadays. \n\nSo please be patient of simply change the theme."
    author: "SVG Crazy"
  - subject: "Re: Is it so good?"
    date: 2009-01-14
    body: "Actually most of issues I reported in my comment will still be present even if you change style. Just try it yourself.\n"
    author: "vf"
  - subject: "Re: Is it so good?"
    date: 2009-01-15
    body: "then this almost certainly an issue in qt..."
    author: "txf"
  - subject: "Re: Is it so good?"
    date: 2009-01-15
    body: "Some of them yes, but some are not (e.g. unneeded scrollbars in kate)."
    author: "vf"
  - subject: "Re: Is it so good?"
    date: 2009-01-16
    body: "I also think that there are some layout issues. I filed some bugs for them."
    author: "knue"
  - subject: "Known Bugs are good Bugs :-0"
    date: 2009-01-14
    body: "Nice development. Sad that no (for me) important known bug is fixed yet :("
    author: "Chaoswind"
  - subject: "kde rocking hard"
    date: 2009-01-14
    body: "Kde 4.2 is rocking hard, I am impressed :) Thanks for all the hard work you guys\nMy my kde 4.2 screenshot gallery below: http://www.imagebam.com/gallery/c2b1855e780164f08450069f8c6024c2/\n\nDon't wet the keyboard guys :p"
    author: "slashdevdsp"
  - subject: "Re: kde rocking hard"
    date: 2009-01-15
    body: "I've posted my own at http://www.linuxchillan.cl.\n\nCoincidently, I've listened all day to a pop song called \"Aire\" that would say something like this, in english.\n\n\"Air... I dreamt for just a moment that I... was... air... Oxygen, nitrogen and argon... without a definite form...\"\n\nPerfect song for the Air wallpaper and the Oxygen theme, I think ;)"
    author: "Alejandro Nova"
  - subject: "fedora"
    date: 2009-01-14
    body: "how can i try it on fedora 10?"
    author: "mdl"
  - subject: "Re: fedora"
    date: 2009-01-14
    body: "http://kde-redhat.sourceforge.net/ \n\nQuite easily"
    author: "plupla"
  - subject: "simply AMAZING"
    date: 2009-01-14
    body: "This release is AWESOME. I don't have words to describe the tremendous advancement there has been done in many apps and the desktop in general. Have you seen kmail? It is... WOOOWWWW And kopete's notifications look very \"sweet\"\n\nAnd a lot of bugfixes! The key bindings seem to work much much better ( they actually work ;) ), akregator no longer \"forgets\" the columns arrangements, kopete does not eat 80% of my cpu cycles anymore as neither does strigi nor nepomuk. \n\nMore advances... finally dolphin keeps thumbnailing on the background while looking at images in a folder, this is somethings I've been missing for a long time!\n\nWell, I can be here writing endlessly! I was happy with KDE 4.1 but this gets over my expectations on KDE 4.2!\n\nWell done KDE devs, *tons* of thanks from a very happy user from Barcelona!"
    author: "Damnshock"
  - subject: "Great"
    date: 2009-01-14
    body: "Thanks to all the developers, this is indeed very nice. Just installed it on my Dell Mini 9 and it works great. The Animations are much smoother than with 4.1. So many little things have improved, really great. "
    author: "tpwm"
  - subject: "4.3"
    date: 2009-01-14
    body: "Out of interest, I prefer using the unstable branch from OpenSUSE, it was rather interesting watching KDE 4.2 coming together.  Does anyone know when 4.3 will start?"
    author: "R. J."
  - subject: "Re: 4.3"
    date: 2009-01-14
    body: "8 days ago ;)"
    author: "Anon"
  - subject: "Re: 4.3"
    date: 2009-01-14
    body: "thanks for your reply, i guess it hasn't made the opensuse unstable respo yet\n\n"
    author: "R. J."
  - subject: "Re: 4.3"
    date: 2009-01-15
    body: "they are probably going to wait till after 4.2 before they start updating unstable"
    author: "txf"
  - subject: "Bloat vs Stability"
    date: 2009-01-14
    body: "I am curious as to when KDE project leaders plan to release a stable version which can be used by the general public.\n\nA whole range of bugs which have been present since pre4.0 are still there.\n\nI've filed a few bug reports after updating to 4.1.96 (KDE 4.2RC) already. Some of the bugs I were going to report were, as expected, already reported. That so many of them were reported long, long ago really is quite sad.\n\nIsn't it time to perhaps get a stable version which can actually be used out in the near future? Will KDE 4.3, scheduled this summer, be another pile of more eye-candy, more bloat and more new bugs without any old bugs being closed - like KDE 4.2 RC?\n\nI use GNOME as my desktop now. Mainly because I have two monitors on two seperate graphics cards - which works in KDE 3.x, fluxbox, ion, metacity and EVERY OTHER WM. But not KDE 4. Didn't work in 4.0, still doesn't in 4.2. Just way to much other very basic functionality.."
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Re: Bloat vs Stability"
    date: 2009-01-14
    body: "http://apaku.wordpress.com/2009/01/14/jit-bugfixing/"
    author: "Paul Eggleton"
  - subject: "Re: Bloat vs Stability"
    date: 2009-01-14
    body: "Are you talking about two seperate X sessions, one per screen?  I've got that going with 4.1.  Or the same session spanning two cards/monitors?  That one I haven't tried yet."
    author: "Anonymous"
  - subject: "Re: Bloat vs Stability"
    date: 2009-01-15
    body: "One X session with screen 0 and 1."
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Re: Bloat vs Stability"
    date: 2009-01-16
    body: "I had that working perfectly well on 4.0.5.  Not needed it on 4.1.x (only have one monitor now)."
    author: "Phil"
  - subject: "Re: Bloat vs Stability"
    date: 2009-01-15
    body: "Do you have any idea of how many bugs have been fixed for 4.2?\n\nCould you point us to the bugreports for those very important remaining bugs? (appart from the fixed multi screen bug)"
    author: "Beat Wolf"
  - subject: "Looks good ..."
    date: 2009-01-14
    body: "I am still using KDE3 as my main desktop. I have just installed the Kubuntu packages in my parallel install. This looks very good. It is the first installation which appears to be very stable from the beginning. If that first impression continues, I will switch. \nTwo questions:\n- Is there anything about data and settings migration from KDE3.5 to KDE4? So far I did not stumble on that topic. I can not believe that this is totally without problems.\n- Is there a possibility to have different content on both screens in a dual monitor setup?"
    author: "Mark"
  - subject: "Konqueror is lighting fast to start up!"
    date: 2009-01-14
    body: "I just installed RC1. My big surprise is how fast Konqueror starts up, a big Wow!!. Now I am thinking seriously about using Konqueror as web browser instead Firefox. Browing speed is also much more faster. \n\nI am very impressed. I also tried Kmail, which is also lot better. \n\n4.2 is indeed faster than 3.5.9 installed on the same machine.\n\nGreat jobs! I can't wait to put the final release of 4.2 on."
    author: "Simon"
  - subject: "Re: Konqueror is lighting fast to start up!"
    date: 2009-01-14
    body: "I've thought of using Konqueror, but for me the reason i moved from opera, years ago, to firefox was all the addon's.  Is there any addons for konqueror? "
    author: "R. J."
  - subject: "Re: Konqueror is lighting fast to start up!"
    date: 2009-01-15
    body: "Some of the addon functionality are default in Konqueror, but yes there are addons too. They are called plugins/extettions, they and the interface they use have existet since before there was a Firefox."
    author: "Morty"
  - subject: "Re: Konqueror is lighting fast to start up!"
    date: 2009-01-16
    body: "what would be better would be if you could firefox addons in it.  \n\nI thought the plugins were rather weak, which is why I continue to use firefox"
    author: "R.J."
  - subject: "Great...but still buggy..."
    date: 2009-01-15
    body: "KDE 4.2 is great and I finally switched over and am quite happy. But there are still bugs. I came across even more of them than in Beta2:\n\n- no more font size setting in the Plasma Digital clock in the panel? why?\n- local time 00:00? I had to choose a particular time zone or else the time did not update at all...\n- trying to set color scheme for desktop theme crashed the settings and they crash now whenever I choose the whole desktop theme settings module again\n- the Kate session selection applet still cannot handle sessions with non-ascii characters, but when I start kate \"without arguments\" and choose the session from its own selection dialogue, it works well\n(- I won't speak about some desktop effects (Desktop Sphere, Blur, etc.) they probably just don't work on my bloody Nforce FX Go5650 with only 64MB RAM)\n(- oh, and of course the bloody un(re)movable useless cashew-ghost, that always pushes my gkrellm out of the upper right corner (when in dock mode) after logging in, even if I use \"ihatethecashew\" applet to make it disappear... well, you can customize \"everything\" in KDE, but this spook will probably stay there even when you switch over to Gnome...? even when I turn off the computer? ...sorry, I am probably just getting a new KDE nightmare... nightcashew... scary!)\n\nThere will surely be more of them than what I noticed immediately. But - since I have heared some opinions - I am not sure anymore, what is supposed to be a \"feature\" and what is a real bug."
    author: "wanthalf"
  - subject: "Re: Great...but still buggy..."
    date: 2009-01-15
    body: "some of these are distro related.  \n\nI have no problem with time in opensuse, and hey, no cashew either, thank you opensuse\n\nchanging colour scheme didn't crash for me in opensuse."
    author: "anon"
  - subject: "Re: Great...but still buggy..."
    date: 2009-01-15
    body: "That 00:00 bug has been mentioned once. Somebody said it was a fixed translation issue.\nCan you report a bug with the backtrace of your crash? with debuging symbols please. thank you\nDesktop effects should work with your graphics card, if you have the proper driver installed.\nThe cashew can be dragged to another location if that helps. If the cashew is disturbing another application, like your gkrellm (don't know what that is), you should report a bug, with screenshots of the problem if possible."
    author: "Beat Wolf"
  - subject: "Re: Great...but still buggy..."
    date: 2009-01-15
    body: "I agree that the cashew is an abomination and the usability of it is even worse: If the bloody thing is so darn important that it has to be there all of the time then why are the menus - if you finally are pushed over the edge and try to interact with the things offered - popping up behind windows that happen to be in close proximity? \nBut that is just a small annoyance (I have a 2 line patch that removes the cashew once and forall - I have a monitor with dynamic contrast adjustment and the cashew botches the contrast for one area of my screen - but I can't hope to get that in place as the main developer is so fond of the thing)...\nThere are important bugs though that make using plasma and kwin a nightmare: \n#172615: Utility windows broken - yepp, they truly are!\n#177025: In GIMP default install not all windows are picked up by taskbar and alt-tab switcher. This makes the default install of GIMP unusable but for the developer this is working for him and he (un)duly closes this bug! It's mainly another facet of #172615 but those two together hinder any real use of the GIMP unless you customize the window behaviour - I dare a beginner in KDE and GIMP to get that to work without help from someone who has a clue what's going on there - and there are not many that have such a clue.\n#177319: Group popup grabs the mouse and will not allow switching to another window as long as you stay within the confines of the taskbar. This is supposed to be a bug that Qt-Software is aware of. Well I have searched the bug tracker there high and low and I can't find a single reference that might be related - so the bug persists but it is deferred (and closed as such) to the upstream developers without any reference what priority or status this has there!\nThe annoying thing is: You run into blocks if you report bugs in plasma. In other areas of KDE you report bugs and get decent responses from the developers and they ask you if you still can confirm if the bug has been squashed for good.\n\n"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Great...but still buggy..."
    date: 2009-01-15
    body: "Could you file a bug report for changing the desktop color scheme, please?  I haven't encountered this problem and didn't see a bug report for this.\n\nThanks"
    author: "Jamboarder"
  - subject: "Re: Great...but still buggy..."
    date: 2009-01-15
    body: "Thanks to everybody. Yes, the problems are probably merely distribution-related. Maybe the colortheme too:\n$ kcmshell4 desktopthemedetails\nQFSFileEngine::open: No file name specified\nObject::disconnect: Unexpected null parameter\nKCrash: Application 'kcmshell4' crashing...\nBut still it should not crash because of this. (Sorry, I do not have debug info in my binaries by default, but if neccessary, I can recompile... Maybe this will be enough to find the problem?)\n\nAnd yes, there are more annoying problems - like kmail deleting all my account settings from time to time (probably during logout, I guess).\n\nI use Nvidia binary drivers, 173.14.15 at the moment. Quite slow (especially rolling yakuake up and down or resizing windows, but quite OK for the rest of things), but the more complex effects probably trigger the old out-of-memory bug (black screen), which has never been resolved really _completely_ by Nvidia, if I understand it well. Anyway playing video through Xv is also not really flawless when compositing is on, but switching fullscreen and back a few times usually helps to make it work (show, at least - in addition, the playing gets sometimes teary and complains about lack of resources with some movies/codecs). Should I expect something better from my hardware/drivers?\n\nOh, the bloody cashew is movable, at least! Thanks, I somehow did not notice! Yes, it helps to move it to the other corner. Well, gkrellm just always started in the corner (in its old position), but when the cashew got activated a second later (even when using 'ihatethecashew' plugin) it moved about 60px away from the screen edge. (gkrellm is an old GTK+ system monitor, still more compact and comprehensive than any KDE/plasma monitor I've seen until now, unfortunately)\n\n"
    author: "wanthalf"
  - subject: "Re: Great...but still buggy..."
    date: 2009-01-15
    body: "You should consider using a newer nvidia driver, they are waaay faster for kde4.\n\nFor that cashew bug, please make a bugreport with screenshot."
    author: "Beat Wolf"
  - subject: "Re: Great...but still buggy..."
    date: 2009-01-16
    body: "Unfortunately, there is no newer driver for my card, as far as I know. Even this one is still a \"beta\". Drivers from the 177 and 180 series do not support chips older than GeForce 6. Or did I miss something?"
    author: "wanthalf"
  - subject: "Re: Great...but still buggy..."
    date: 2009-01-16
    body: "Actually the Desktop Cube works very well, so I don't know, what is the problem with the Sphere, Cylinder and Blur effects."
    author: "wanthalf"
  - subject: "Re: Great...but still buggy..."
    date: 2009-01-17
    body: "Oops, the \"bloody cashew\" is probably innocent in this case. Gkrellm still \"jumps away\" even though the cashew is sitting in the other corner. Then I have no more idea."
    author: "wanthalf"
  - subject: "Re: Great...but still buggy..."
    date: 2009-01-17
    body: "Probably the desktop layout algorithm, then - I know that's caused me some annoyance before."
    author: "Anon"
  - subject: "Re: Great...but still buggy..."
    date: 2009-01-18
    body: "you should still submit a bug with screenshot, if it's a bug in kde4 it can be fixed, if not, the bugreport will be closed and you can submit it in the Gkrellm bugtracker"
    author: "Beat Wolf"
  - subject: "In the end..."
    date: 2009-01-16
    body: "We were all waiting for a usable (read \"ok, 3.5 can be dropped\") version? 4.2 will be what we were waiting for. For those like me who are weekly compiling from svn since the days 4.0 was at his beginning (almost 2 years ago), it's absolutely clear the amount and the quality of work done by all the developers. Now, if I want a crash (with my desktop usage), I really have to go hunt it.\n\nThere are show-stoppers for someone (dual monitor, I guess is an example), but I think the vast majority of the things reported here are quite \"normal bugs\" that don't make KDE4 usage *impossible*. Maybe \"harder\", but not impossible. Looks weird here and there? Ok, but this won't stop you (or me) from using it *daily*. I agree for example with the amount of space often wasted blank, but ok, I can live with it.\n\nSeeing the development quality and pace, I'm confident that great things will be done. But.\n\nMy but is about (like pointed from others in this thread) not in KDE as a whole, but on single apps. For example, single plasmoids. A dumb example is the disk usage plasmoid: its minimum size contraints are definitely too big (I have 6/10 fs mounted) to be left on the screen. Or the annotation plugin, way too \"rough edge\" to be for common usage.\n\nAnother example, invest in integrating Nepomuk/Strigi in each every app KDE base ships (have users understood the value of Nepomuk/Strigi?) so that its potential comes out.\n\nNow that KDE4.2 is such a beautiful thing, please invest on it before stepping into 4.3 (that means, take more care of what exist before creating). Please, forward patches to trunk instead of backporting to branch (I you know what I mean)."
    author: "SR"
  - subject: "boost 1.37.0\nincompatibilites with kdesdk4 rc2?"
    date: 2009-01-17
    body: "The build fails on some boost issues;\n\n[ 67%] Building CXX object poxml/CMakeFiles/swappo.dir/swappo_automoc.o\nIn file included from /usr/src/kdesdk-4.1.96/umbrello/umbrello/activitywidget.h:16,\n                 from /usr/src/kdesdk-4.1.96/umbrello/umbrello/dialogs/activitydialog.cpp:17:\n/opt/lunar/qt/4/include/Qt3Support/q3canvas.h:551: warning: 'virtual void Q3CanvasPolygonalItem::draw(QPainter&)' was hid\n/usr/src/kdesdk-4.1.96/umbrello/umbrello/umlwidget.h:231: warning:   by 'virtual void UMLWidget::draw(QPainter&, int, int\n[ 67%] [ 67%] Building CXX object umbrello/umbrello/CMakeFiles/umbrello.dir/dialogs/activitypage.o\nBuilding CXX object poxml/CMakeFiles/swappo.dir/GettextLexer.o\n[ 67%] Building CXX object kompare/komparepart/CMakeFiles/komparepart.dir/komparesplitter.o\n[ 67%] Building CXX object poxml/CMakeFiles/swappo.dir/GettextParser.o\n\n\nDo these types of bugs get filed at http://bugs.kde.org ?"
    author: "Taurnil"
  - subject: "Still Buggy!"
    date: 2009-01-17
    body: "I'm using http://rdieter.fedorapeople.org/torrents/trash/f10-kde-4.1.96-2.torrent to evaluate KDE 4.2 in VirtualBox. Plasma keeps crashing. Sometimes some kind of grey plasma window covers almost the whole desktop, preventing me to click anything on the desktop. And it never remembers which widgets I removed from the desktop. So every time I relogin I get all the widgets I ever added. Urgh. Thats still really buggy. Almost unusable. Gah, I don't want to switch to a buggy system, but Fedora does not provide security patches to Fedora 8 anymore!"
    author: "panzi"
  - subject: "Please tell me"
    date: 2009-01-17
    body: "Have they fixed the start menu so that it at last starts fitting the rest of the theme? That blocker level bug has been around from the earliest 4.x development versions :("
    author: "Anonymous bastard"
  - subject: "Re: Please tell me"
    date: 2009-01-17
    body: "The menue changes it's appearance with the theme.\n\nFunny, I never payed attention to that. Everybody seems to have his own little blocker. ;-)"
    author: "Cyril"
  - subject: "thank you :-)"
    date: 2009-01-17
    body: "KDE 4.2 is my home now !"
    author: "Nadav Kavalerchik"
  - subject: "cashew"
    date: 2009-01-18
    body: "I know that long debates have happened as a result of what has become known as the cashew. I'm one user who thinks its rather ugly. Will there ever be an option to remove this from the desktop or do I have to live with it? It is one of the visual aspects of KDE4.X that I absolutely loathe. I just want it off my desktop."
    author: "Clifford"
  - subject: "Re: cashew"
    date: 2009-01-18
    body: "http://www.kde-look.org/content/show.php/I+HATE+the+Cashew?content=91009"
    author: "Cyril"
  - subject: "Re: cashew"
    date: 2009-01-19
    body: "As Tom Patzig writes in http://bugs.kde.org/show_bug.cgi?id=172267 :\n\n\"After a hard last week, I released kdebluetooth4-0.3 today.\n\n\"This version is no feature release. Kdebluetooth4 provides now full bluez4 support. Therefore a lot of work and api change had to be done in solid-bluetooth.\n\n\"Means: kdebluetooth4-0.3 needs the latest solid-bluetooth (kdebase4-workspace) stuff to compile and run. I've patched the openSUSE kdebase4-workspace packages from KDE4.1 and KDE4.2 to support the latest solid-bluetooth changes. If someone needs these patches for other distro packages, let me know. Kdebluetooth4-0.3 also needs the latest bluez and obex-data-server package.\""
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: cashew"
    date: 2009-01-19
    body: "(And that'll teach *me* to browse the dot with more than one tab... obviously this reply wasn't intended on this thread.  My fault entirely)."
    author: "Evan \"JabberWokky\" E."
  - subject: "Business ready?"
    date: 2009-01-18
    body: "Two questions that affect professional users:\n\nDoes the multimonitor bug yield to those who plug external monitor or projector into the external VGA port, or is it just multi-gfxcard systems that are affected?\n\nAnd, does KDE4 crypto (SSL/TLS) support CRLs or OCSP yet? It's very annoying that KDE apps happily continue to put up SSL connections to fraudent services that use revoked certificates. It's in fact a major security flaw in KDE."
    author: "Kolla"
  - subject: "No hope for real replacement of 3.5"
    date: 2009-01-18
    body: "Well, progress is quite impressive but I have no hope that 4.2 release will be really usable as 3.5. Developers are eager to make visual effects but real bugs not fixed for eons. For example, I often can not make my notebook go to suspend because stupid button in this dialogue works strange way."
    author: "Alex"
  - subject: "Re: No hope for real replacement of 3.5"
    date: 2009-01-22
    body: "suspend/resume is usually handled by the OS not the desktop"
    author: "Rioting_pacifist"
  - subject: "Please fix Bluetooth-Support!"
    date: 2009-01-18
    body: "Proper Bluetooth-Support ist essential. Please make it bluez4-compatible. KDE4.2 without it would unfortunately be a No-Go for me.\n\nhttp://bugs.kde.org/show_bug.cgi?id=172267"
    author: "Bluez"
  - subject: "Re: Please fix Bluetooth-Support!"
    date: 2009-01-19
    body: "As Tom Patzig writes in http://bugs.kde.org/show_bug.cgi?id=172267 :\n\n\"After a hard last week, I released kdebluetooth4-0.3 today.\n\n\"This version is no feature release. Kdebluetooth4 provides now full bluez4 support. Therefore a lot of work and api change had to be done in solid-bluetooth.\n\n\"Means: kdebluetooth4-0.3 needs the latest solid-bluetooth (kdebase4-workspace) stuff to compile and run. I've patched the openSUSE kdebase4-workspace packages from KDE4.1 and KDE4.2 to support the latest solid-bluetooth changes. If someone needs these patches for other distro packages, let me know. Kdebluetooth4-0.3 also needs the latest bluez and obex-data-server package.\""
    author: "Evan \"JabberWokky\" E."
---
The KDE community has made available two new releases of the KDE desktop and applications today.  <a href="http://kde.org/announcements/announce-4.1.4.php">KDE 4.1.4</a> is the latest update for the KDE 4.1 series. It contains many bugfixes, mainly in the e-mail and PIM suite Kontact and the document viewer Okular.  <a href="http://kde.org/announcements/announce-4.2-rc.php">KDE 4.2 RC</a> is the release candidate of KDE 4.2, also bringing new features and thousands of bug fixes to the KDE desktop and applications.  KDE 4.1.4 is the last planned update to the KDE 4.1 series and stabilises the 4.1 platform further. It is a recommended update for everyone running KDE 4.1.3 or earlier.





<!--break-->
<p>KDE 4.2.0 will be released on January 27th and is considered a significant improvement over the KDE 4.1 series. The <a href="http://kde.org/announcements/announce-4.2-beta1.php">beta</a> <a href="http://kde.org/announcements/announce-4.2-beta2.php">announcements</a> have all the details.  Enjoy our releases, and don't have your head explode when choosing which version to upgrade to.</p>




