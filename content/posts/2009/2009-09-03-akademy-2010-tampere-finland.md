---
title: "Akademy 2010 in Tampere, Finland"
date:    2009-09-03
authors:
  - "sebas"
slug:    akademy-2010-tampere-finland
comments:
  - subject: "Sorry, no midnight sun in Tammerfors"
    date: 2009-09-04
    body: "Sorry to disappoint you, but there is no midnight sun in Tammerfors, which is only at 61.5\u00b0 N. Midnight sun begins at the arctic circle at 66.56\u00b0 N, where it lasts 1 night (the midsummer night). The further north, the longer it lasts (half the year at the north pole)."
    author: "odalman"
  - subject: "Sun"
    date: 2009-09-06
    body: "Well, officially you need to go over the city of Oulu to see the sun stays always on sky. But even under the Tampere, there does not come dark. The Sun goes down for 15-20 minutes just under littlebit under horisont, but still shines everything nicely.\r\n\r\nFor those who lives on mid-european (france, germany) or on same level, for them it is still something to see.\r\n\r\nIn short, Tervetuloa tampereelle (Welcome to tampere)"
    author: "Fri13"
  - subject: "Daylight Savings Time"
    date: 2009-09-07
    body: "Well, if you account for daylight-savings time, there might indeed by sun at mignight, even if there isn't 24h sun."
    author: "carewolf"
---
<p>
The KDE community is proud to announce the location of next year's Akademy: Tampere, Finland. Akademy is the yearly world conference held by the KDE community to celebrate the Free Software desktop and work towards the future of KDE. 
</p>
<p>
 After a successful Akademy 2009 on the Canary Islands, as part of the Gran Canaria Desktop Summit, Akademy heads north to the birthplace of Linux where contributors will enjoy the midnight sun as they spend a week to present, plan and participate in the development of KDE software.
</p>
<!--break-->
<p> 
 Akademy 2010 will be organized by COSS, the Finnish Centre for Open Source Solutions in cooperation with KDE e.V. <em>"We were happy to meet Mr. Ilkka Lehtinen at GCDS,"</em> says Sebastian Kügler, board member of KDE e.V., <em>"and we spent some time discussing what really goes into Akademy to make it such a vibrant event."</em> The conference is planned for late June or early July, and announcements will be made when the date is finalized and contributors to the Free Software desktop can start to make their summer plans. 
 A call for papers and participation is expected in the dark days of the year. Volunteers for the conference itself will be sought in the spring. Mr. Ilkka Lehtinen will lead the local team, Kenny Duffus will lead the KDE team and Claudia Rauch, Business Manager of KDE e.V., will be the central point of contact and coordinate with the local team. 
</p>
<p>
 Cornelius Schumacher, president of KDE e.V., says of the coming conference, <em>"it will be great to show our technology in new areas and to reach out to the business community and Finland in particular with this Akademy. The release of KDE 4.5 is scheduled around Akademy in 2010 and it will be a good moment to look back at the road from KDE 4.0 to stability, beauty and elegance."</em>
</P