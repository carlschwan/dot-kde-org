---
title: "KDE 4.3.0 Beta 2 Out, Codenamed \"Crumping\""
date:    2009-06-10
authors:
  - "sebas"
slug:    kde-430-beta-2-out-codenamed-crumping
comments:
  - subject: "Debian packages"
    date: 2009-06-10
    body: "Looking forward to some debian experimental packages of this release :)"
    author: "ianjo"
  - subject: "It's great!"
    date: 2009-06-10
    body: "I am runing it with Kubuntu packages and most of my issues I have reported with Beta 1 have been fixed. Only one crash so far. With KDE4.3 it seems really ready for use. \r\nThings which still need improvement for 4.3.\r\n- networkmanagement e.g. mobile \"broadband\" and vpn-support\r\n- bugs for a small icon setting enviroment need to be fixed\r\n- Klipper actions are partly brocken since Beta 1\r\n\r\nWishes for the future:\r\n- Konqui seems to need some love.\r\n- Activities are in an early stage, need improvement\r\n- Dual monitor support with different desktops\r\n- Mail tabs in Kmail and a mode for only showing folders with unread mail\r\n- Akregator having a similar folder view then Kmail.\r\n"
    author: "mark"
  - subject: "Socks v5 support"
    date: 2009-06-10
    body: "I hope there will be socks v5 support!"
    author: "hlavki"
  - subject: "Crash reporting tool"
    date: 2009-06-10
    body: "Hey, the new crash reporting tool is really wonderful!! I've just installed beta2 on my Kubuntu box and I have experienced a KMail crash (but I were already experiencing them with KDE 4.2 as well) and the new report tool pop out... it's almost perfect!! I have successfully reported the crash at the first try, I didn't have  any doubt at any time about what I was doing... the UI is really well thought and an example for new (and old) KDE application about what a good UI is. Great job!\r\n\r\n(the worst part being that this tool in the long run should be not used almost at all, cause we don't want crashes in KDE ;)"
    author: "Vide"
  - subject: "KDE Brainstorm"
    date: 2009-06-10
    body: "The most effective way how to offer new ideas and features:\r\nUse the \"KDE Brainstorm\" section, see http://dot.kde.org/2009/03/20/kde-brainstorm-get-your-ideas-kde"
    author: "rkrell"
  - subject: "akregator"
    date: 2009-06-10
    body: "Hi,\r\n\r\nThere is one question I have about testing.\r\n\r\nFor the very obvious bugs, doing an extra installation based testing is possible. But from what I've experienced, most of the good bugs are only seen when you start using the app on a daily basis.\r\nLike, I could have installed KDE 4.x in a chroot from day 1 and just periodically used it for \"testing purpose\". But that doesn't give much bugs. Especially for apps like Kmail/Konqueror, you need to be using them on a regular basis.\r\nBut then, that costs. I had to spend a lot many times with broken apps during the initial KDE 4.x releases.\r\n\r\nHow do you guys test ? Do the \"real\" testers always run trunk ?\r\n\r\n\r\nAnd yes, I was wondering if Google Reader synchronization in Akregator was going to make into 4.3 ? I see the option for adding an account, but then no option to request for a sync. Is it done for 4.3 ?"
    author: "rickysarraf"
  - subject: "Oxygen"
    date: 2009-06-11
    body: "So, when the new oxygen tabs style will be implemented? \r\nThis one: http://pinheiro-kde.blogspot.com/2009/05/oxygen-is-back-to-work.html"
    author: "alexismedina"
  - subject: "Re: Oxygen"
    date: 2009-06-11
    body: "It has been in trunk since few weeks ago, so I suppose it is also in 4.3 beta 2."
    author: "fyanardi"
  - subject: "Embedded images in Kmail"
    date: 2009-06-11
    body: "This is my most-wanted-feature in Kmail!! I have already posted it on the kde brainstorm website as well.\r\n\r\nIt can be done (embedding images) in the composer windows, so when can we see this in the signature?"
    author: "kanwar.plaha"
  - subject: "testing"
    date: 2009-06-11
    body: "Surely I cannot speak for all developers, but I for one switch to trunk snapshots (from openSUSE's unstable repos) around the release of the first Beta of a 4.x release (i.e. version 4.(x-1).80). By then, the obvious bugs are ironed out. My most important motivation is not testing (though that also is important), but the new features I'm seeing on Planet."
    author: "majewsky"
  - subject: "Re: Embedded images in Kmail"
    date: 2009-06-11
    body: "I thought I've read about this feature being implemented some months ago. Could somebody confirm this who writes/recieves HTML mails regularly?"
    author: "majewsky"
  - subject: "I do the same thing"
    date: 2009-06-12
    body: "I usually switch one of my boxen to the beta when it comes out, and run nightlies thereafter, although I don't have as much time as I'd like to report bugs.  I must say, I'm a sucker for pre-release software, and reading the Planet just feeds that hunger for the newest features, whatever the cost. :P  That being said, I was impressed with the stability of 4.3.0b1 - there were some graphical issues, but it ran better overally than 4.2.3 did."
    author: "ffejery"
  - subject: "KDE 4.3 beta2 rocks"
    date: 2009-06-12
    body: "Just wonder why the Kubuntu packages for a beta are not debug enabled. Two crashes so far, one reproducable when you select Mandelbrot as a desktop wallpager background. No idea if it is the broken Kubuntu packages or KDE beta2: 3170 segmentation fault 11, reproducible. One minor theming bug I was unable to reproduce: drop-down menues of Dolphin in wonton colours and just the drop-down menues. When I put a plasma panel above the folderview desktop and select autohide, the panel is not visible otherwise it is on top of apps. Folderview content should ideally flow around the plasma widgets. etc. All this is very much minor polishing stuff. Lancelot is great design, should be a bit faster when launched. The black box welcome html screens of Konqueror and Kmail are still extremely ugly.\r\n\r\nWith beta2 of KDE 4.3 we find a clear vision of KDE developers which is ready for KDE enthusiasts and just needs a bit of polishing, speed optimization and elimination of the few remaining bugs. I am impressed how quickly all the common objections against KDE4 melt away and make way for the promising future on the base of that renovated architecture.\r\n\r\nLooking forward to KDE 4.3 final!\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n"
    author: "vinum"
  - subject: "debug symbols"
    date: 2009-06-12
    body: "To get debugging support, install the -dbg package for the corresponding KDE module. (E.g. you'd probably want kdeplasma-addons-dbg to debug the wallpaper plugins, or kdeutils-dbg for ark, etc)"
    author: "JontheEchidna"
  - subject: "crash report"
    date: 2009-06-15
    body: "yes, the new crash report handler is very cool. It is really easy to file bugs now. very nice!"
    author: "majorTomBelgium"
  - subject: "What do testers run?"
    date: 2009-06-18
    body: "For developers, it tends to be reasonably easy to run trunk. Although people won't svn up and recompile everything constantly, just what they happen to be working on. \r\n\r\nSome of BugSquad constantly does go and keep a very current installation via nightly rebuilds. Others use trunk snapshot packaged builds, such as the Neon nightlys or various distro builds. \r\n\r\nDo you *have* to run trunk? Some bugs are on both trunk and branch. But for some programs like plasma, if you are reporting bugs from branch, odds are it was fixed long ago.\r\n\r\nIt depends... Remember that KDE gets many commits daily. There are still bugs in branch that are also in trunk though! So keep reporting. "
    author: "blauzahl"
---
<p>The KDE release train continues to roll, delivering <a href="http://kde.org/announcements/announce-4.3-beta2.php">another beta release of KDE 4.3</a> to you today. The effort has shifted towards increasing stability and adding polish to the codebase so our users will find a well-working KDE 4.3.0 on their desktops when it is released in late July.</p>

<p>Over the last month, 2991 bugs have been closed and 3008 new ones opened. This indicates a huge turnover of bugs and by extension means: we care, yes we do. So help us with another round of quality improvements of KDE 4.3 and you will be able to reap the benefits yourself. Install the beta (though only for testing, we explicitly warn against production use), find bugs, report them on bugs.kde.org or better yet: fix them and send us patches!</p>
<p>Come help <a href="http://techbase.kde.org/Contribute/Bugsquad">BugSquad</a> as it kicks into high gear -- the next BugDay is June 20th, although people are always around to teach on irc.freenode.net in #kde-bugs.</p>