---
title: "KDE Community and Apliki Cooperate on Understandable Icons"
date:    2009-11-28
authors:
  - "jospoortvliet"
slug:    kde-community-and-apliki-cooperate-understandable-icons
---

The 4.0 release of the KDE software compilation marked a major milestone for the KDE community. While the underlying development platform has seen a modernization to better work with increased demand of applications, the community also saw a shift in its development methods. Interaction design has become much more important, and hence the need to collect feedback from the user in a structured manner. Ultimately, this leads to more understandable user interfaces and simpler handling of the underlying complexity of modern computers and portable devices. <a href="http://pinheiro-kde.blogspot.com/">Nuno Pinheiro</a>, a well-known artist and icon designer in the KDE community and engineering psychologist <a href="http://www.opensource-usability-labs.com/">Björn Balazs from the Open Source Usability Labs</a> and director for analysis, design and testing at <a href="http://www.apliki.de">Apliki</a> decided they wanted to help with this.

<h2>The Icon Usability Test</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: none"><a href="http://www.apliki.de"><img src="http://dot.kde.org/sites/dot.kde.org/files/signet_apliki_200px.jpg" width="200" height="109"/></a></div>Nuno and Björn met for the first time in person at an Amarok developer meeting in Berlin. They discovered they both had interest in usability and design and decided to work together. <a href="http://www.apliki.de">Apliki</a>, Germany's first Psychological IT-Consultancy, has been using psychological methods and tools to test and improve usability in a variety of Free Software and proprietary applications. They have developed the <a href="http://www.usability-methods.com/">Icon Usability Test</a>, a web-based test designed to quickly assess the usability of an icon by user survey. Icon usability is often an issue overlooked during development, yet icons play a pivotal role in quickly guiding users through the interface. The Icon Usability Test is therefore designed to assess the responses of users themselves to determine the quality and understandability of icons.

<h2>First Test</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: none"><img src="http://dot.kde.org/sites/dot.kde.org/files/180px-Oxygen-front.svg_.png" width="180" height="180"/></div>Nuno and Björn decided the huge number of icons in the KDE <a href="http://oxygen.kde.org">Oxygen</a> theme could benefit from the usability test. The first Icon Usability Test has already been completed and more than 7000 community members participated. Björn comments: "We were overwhelmed by this input. And we only need a minimum of 30 participants for a scientifically accurate assessment of any icon. Of course, more participants help a lot in comparing different target groups". He suspects that "so many people participated because it only takes about 4 minutes for the participant to assess the quality of an icon."

<h2>More Testing</h2>

Further Icon Usability Tests for Oxygen Icons will take place on a biweekly base. Nuno: "with the Icon Usability Test, we not only receive feedback on the understandability of the icons, but also learn about cultural differences in their interpretation". Based on these tests, Nuno wants to explore the development localized icons to help KDE software adapt better to the user. Apliki has kindly offered to run the regular icon tests, asking users for their cooperation through a soon to be aggregated blog on <a href="http://planet.kde.org">Planet KDE</a>.

<a href="http://www.apliki.de">Apliki</a> offers their tools for free use by Free Software communities and university researchers. Commercial and non-commercial users can get in contact with Apliki using data on the website or emailing them <a href="mailto:info@apliki.com">here</a>.

One last comment from Björn: "Thanks to the community and our web-based Icon Usability Test, the days for incomprehensible icons are counted!" 

It is good to see the KDE community engage in cooperative relationships with parties outside the community, seeking mutual benefit. We welcome collaboration for any individual or organization which believes in improving the quality of our software and helps us realize our goal of getting the best Free Software anywhere!