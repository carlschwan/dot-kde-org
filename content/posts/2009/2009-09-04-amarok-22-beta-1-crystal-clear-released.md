---
title: "Amarok 2.2 Beta 1 \"Crystal Clear\" Released"
date:    2009-09-04
authors:
  - "nightrose"
slug:    amarok-22-beta-1-crystal-clear-released
comments:
  - subject: "is there a schedule plan with"
    date: 2009-09-05
    body: "is there a schedule plan with an estimated release date for 2.2? can't wait :p"
    author: "patcito"
  - subject: "playback slider"
    date: 2009-09-05
    body: "Hrm, I'm not sure I like the new playback slider. It's not clear exactly what part of the circle is the current position; is it the center? left edge? right?\r\n\r\nOne of the things I liked about the old one was that it was clearly exactly which pixel you were at, which makes seeking feel more precise."
    author: "davidben"
  - subject: "only miss."
    date: 2009-09-05
    body: "im testing amarok from svn and only miss:\r\n\r\n-use the new tray, like kmess-svn\r\n-use plasma tooltips for the tray\r\n\r\nis really working nice."
    author: "El barto"
  - subject: "schedule"
    date: 2009-09-05
    body: "see amarok-devel@kde.org for a discussion. most likely not too long to wait, depends on how many betas we have.\r\n\r\nleo"
    author: "lfranchi"
  - subject: "Nice Progress"
    date: 2009-09-05
    body: "Nice to see the work and improvement going into Amarok. Kudos to the devs especially for listening to the users. It's the only way to write a killa app these days ;)\r\n\r\nOn another note, does anyone here knows what's going on in the kaffeine side of the Universe? The development of kaffeine seems to be moving on so slowly and quietly. \r\nIt would be so nice to see a KDE 4 version of kaffeine that is as good as Amarok is at this stage. Even K3b (kde 4 version) seems to have made some good progress."
    author: "Bobby"
  - subject: "It was released earlier this"
    date: 2009-09-05
    body: "It was released earlier this month iirc. Checkout http://kaffeine.sf.net"
    author: "emilsedgh"
  - subject: "New Progress Slider"
    date: 2009-09-05
    body: "Yep, we are aware of these issues. The current slider widget is experimental, a work in progress. We're tossing around a few ideas for a new toolbar look.\r\n\r\nWe'll make sure to fix these issues before the final release.\r\n\r\nAnd hey, suggestions for the toolbar's look and layout are welcome and encouraged :)\r\n"
    author: "Mark Kretschmann"
  - subject: "Thanks for the suggestions,"
    date: 2009-09-05
    body: "Thanks for the suggestions, we'll look into it.\r\n"
    author: "Mark Kretschmann"
  - subject: "Normal volume bar for \"Main toolbar NG\" puhleeeze :)"
    date: 2009-09-05
    body: "I hope you can reconsider putting a norman volume bar in the new \"Main toolbar NG\" like in the original \"main toolbar\". There's definitely space fo it. I think there're a lot people who access volume quite frequently and that extra click is surprisingly annoying when accessing volume control.\r\n\r\nOther than that I can't find anything to complain it just ROCKS! :)"
    author: "bliblibli"
  - subject: "work-in-progress!"
    date: 2009-09-05
    body: "As I had written in another comment, the toolbar is still work-in-progress. We're still experimenting with different layouts.\r\n\r\nThanks for the feedback though :)\r\n"
    author: "Mark Kretschmann"
  - subject: "Precompiled packages"
    date: 2009-09-06
    body: "What all distributions has precompiled packages?\r\n\r\nI am searching mandriva cooker packages (Sophie told there is not (yet) this version) so I could not need to wait compile this on netbook. :-)\r\n\r\nAll what I found was this page: http://amarok.kde.org/wiki/Download:Mandriva\r\n\r\nAnd it does not help at all.\r\n\r\nAnd the Neon project (http://amarok.kde.org/wiki/User:Apachelogger/Project_Neon) should get supporting other distributions as well (Debian (goes for ubuntu well and others), OpenSUSE, Mandriva and Fedora)\r\n\r\nOther way, can not wait to get hands dirty for testing and reporting!"
    author: "Fri13"
  - subject: "Progress slider suggestions"
    date: 2009-09-06
    body: "well, if you want more accuracy, you can try a cross-hairs (like an X, where the centre pixel is clear), but it is a bit OCD :)\r\n\r\nWhat I suggest is to copy the time position tooltip from youtube (the bit that hovers over the cursor when seeking); i find it very good as the length of the slider is fixed in space but represents different times according to the length of the song.\r\n\r\nKeep it up! :)\r\n\r\nvespas"
    author: "vespas"
  - subject: "Don't agree"
    date: 2009-09-06
    body: "I think the new design with the volume bar not being embedded is much better. I never change the bar in amarok. I already have Kmix and my multimedia-buttons on the keyboard. \r\nHowever the new volume icon doesn't seem to have the same style as the rest of the amarok icons/buttons"
    author: "Fyzix"
  - subject: "We're in a feature freeze"
    date: 2009-09-07
    body: "We're in a feature freeze now, so that sort of thing won't be added for 2.2. \r\n\r\n2.3 will probably use the new tray like I expect most KDE apps will start using it. :) It'd have to be conditional since its a new feature in kdelibs and we try to run on older KDEs when we can.."
    author: "eean"
  - subject: "MusicBrainz"
    date: 2009-09-07
    body: "Is there any way to restore or fix music tags like amarok 1.4 had?"
    author: "moritzmh"
  - subject: "Fallback"
    date: 2009-09-07
    body: "As far as I know the class already takes cares of that - if it doesn't detect support for the new dbus-based system tray, it'll fall back to the old implementation."
    author: "Hans"
  - subject: "But that still requires"
    date: 2009-09-09
    body: "But that still requires compiling against the libraries for the new implementation, which older KDEs (<4.3) wouldn't have. The fallback is mainly for applications that don't support the new spec."
    author: "JontheEchidna"
  - subject: "great despite some crashes"
    date: 2009-09-16
    body: "I like the new look a lot - despite some crashes it's very nice to use. \r\n\r\n@Amarok team: Congrats and many thanks for your good work! "
    author: "ArneBab"
---
The Amarok team is getting ready for the release of Amarok 2.2 and is proud to release the first beta version of Amarok 2.2.

Work was put into all parts of Amarok to bring back old features like playlist sorting and new ones like a video and photo applet and the ability to rearrange parts of the program to make the layout fit your way of rediscovering music.

To find out more please read the <a href="http://amarok.kde.org/en/releases/2.2/beta/1">release notes</a> and this <a href="http://padoca.wordpress.com/2009/09/01/amarok-2-2-reloaded-revamped-rethinked-reeverything/">excellent user review</a>.

Please help us polish this for final release in about a month by testing, reporting bugs and sending patches. Most of all though enjoy rocking with a much improved Amarok!

<div style="border: thin solid grey; padding: 1ex; width: 360px; text-align: center; margin-left: auto; margin-right: auto">
<a href="http://dot.kde.org/sites/dot.kde.org/files/Amarok2.2beta1.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/Amarok2.2beta1t.png" /></a><br />
Amarok 2.2 Beta 1
</div>
<br>
<!--break-->
