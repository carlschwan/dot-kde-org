---
title: "KDE Commit-Digest for 15th February 2009"
date:    2009-03-07
authors:
  - "dannya"
slug:    kde-commit-digest-15th-february-2009
comments:
  - subject: "5 lines of Javascript! That"
    date: 2009-03-08
    body: "5 lines of Javascript! That is AMAZING!\r\nJust imagine: web developers creating fully-functional software/\"webware\" with little or no learning curve. I've yet to see anyone else achieve this. Well done KDE devs!"
    author: "madman"
  - subject: "Digikam & Gwenview"
    date: 2009-03-08
    body: "Currently I'm using Dolphin and Gwenview and lots of folders for most of my image management in KDE, but after a visit to a friend I noticed Digikam is much more powerful and easy to use. Last time I tried (some years ago) it was unreliable and crash prone and I somehow never got around to test it again. Back home I instantly tried it and it is almost exactly what I need. However I have trouble with photos which are managed by PicStor, a commercial photo management program I need for my work and use via wine. PicStor creates a ridiculously large tif index file in each folder which makes Digikam take a very long time to scan this folder and use up a lot of memory. I had to raise the values in limits.conf to prevent it from getting killed. And Digikam ignores all photos which are tagged or edited in PicStor. PicStor adds .0000 to the files and .0001 etc. for each edited version. I inserted the 0000 on the config page but Digikam only shows the original photo. In Dolphin and Gwenview I can see all photos and their versions.\r\nHow can I make Digikam to show all subsequent photos too? And how can I prevent it from reading the index file?"
    author: "AJ"
  - subject: "Just amazed by the speed of innovation :)"
    date: 2009-03-08
    body: "Just can't wait for KDE4.3! \r\n\r\nEdit: I'll read the Digest _before_  asking questions next time *blushing*"
    author: "Jo \u00d8iongen"
  - subject: "Re: Digikam & Gwenview"
    date: 2009-03-08
    body: "This is one of the best examples of use case which should by submitted to Digikam's bugzilla.  \r\nMaybe Digikam developers do not know anything about this issue. So let them now on bugzilla. \r\n\r\nregards"
    author: "Piotr Pe\u0142zowski"
  - subject: "Amarok developers ... remember, we're the experts!"
    date: 2009-03-08
    body: "From the post\r\n... and also some for usability reasons (such as the tabular playlist design - remember, we're the experts!).\r\n\r\nOh please don't try to convince that everybody are fools and just don't get it. There are 2 features that Amarok users mostly hate in 2.x series:\r\n1. Large central area with context information.  \r\n2. New play list.\r\n\r\nI remember history of both these new \"features\". Initial motivation of placing context info at the center was that Amarok developers believe that this context related stuff is really a central thing that distinguishes Amarok from other player. But all users ask is to place playlist next to the collection browser so that it was easier to drag items to playlist. \r\n\r\nThe mock up for new playlist was introduced long time before any real usability expert worked on Amarok 2. And current playlist is almost exact copy of that mockup. A lot of usability experts say that it is more natural for people to understand information if it is in the table. All usability experts say that that it is easier for human eyes to accept information by lines. There is no real usability reason to have current playlist. Not even saying about having some \"context\" space between collection and play list.\r\n\r\nIn free software world developers make all decisions because they really write code. That's fair. And that's the only reason for current Amarok 2.x UI. There are no real usability reasons for this.\r\n\r\nI am ok with statement that \"We write Amarok so we decide how it looks\". Why can't you just say this. But I really don't like when 10 people say to few hundreds of people that \"we are the experts and we know better\". To say this you need to be a real expert. If someone don't write any code for Amarok it doesn't means he is not an expert. Or he doesn't know what he is talking about. I am senior software developer. But I don't want to start writing code for Amarok only for purpose so my voice as well as voice of large number of other people was heard.\r\n\r\nHow many times amarok users must say something so that amarok developers at last started to listen???"
    author: "4bugzilla"
  - subject: "Joke?"
    date: 2009-03-08
    body: "perhaps it was more ment like a joke...\r\n\r\nBesides, like the guy said, the ones who do like the new playlist are the ones who don't complain. Plus, you can get the old view if you want, so I don't really see the problem."
    author: "terracotta"
  - subject: "Software Engineering (Development)"
    date: 2009-03-08
    body: "<q>In free software world developers make all decisions because they really write code.</q><p>There is an interesting article in the current (03/2009 P.24) <i>Communications of the ACM</i> that asks the big question: Is Software Engineering [really] Engineering?</p><p>Whether or not it is really engineering, developers need to realize that software development is a lot more than writing the code.  Writing the actual code should be only a small part of software development.  If it is engineering, then analyzing the problem and designing the solution are the important parts of the project.  Writing the code is only implementing the solution.</p><p>To make my point clear: <b>design is what is important.</b>  You need to develop a good design <u>before</u> you write the code.</p><p>This mantra of FOSS has come to mean that those that write the code do the system analysis and project design.  Why should it be that way?  Does knowing a programing language automatically make someone good at analysis and design?  I don't think so.  "
    author: "KSU257"
  - subject: "software engineering"
    date: 2009-03-08
    body: "<i>To make my point clear: design is what is important. You need to develop a good design before you write the code.</i><p>You should really read up on <a href=\"http://en.wikipedia.org/wiki/Agile_software_development\">agile software development</a>. It's really big in the general softwre development world now, and is basically based on the observation that big up-front designs seldom are any good. We've been doing <a href=\"http://en.wikipedia.org/wiki/Big_Design_Up_Front\">Big Up-Front Designs</a> for a few decades now, and if there is anything software projects are generally know for, it's failing. They're very often delivered late, above budget and non-working.<p>\r\n\r\nAgile development tries to correct this by producing functioning code early, and improving and extending the code base over several iterations, all in close cooperation with the user/customer. Notice how this sounds like the \"release early, release often\" mantra of open source.<p>\r\n\r\nOne cornerstone of agile methods is to constantly asking the user \"does this work? is this what you want?\". Whether or not the amarok project is doing that, I have no idea... ;)\r\n"
    author: "torkjel"
  - subject: "users are not designers"
    date: 2009-03-08
    body: "@KSU257,\r\nOf course that design is what is important, and developers are not designers by itself.  But users are not designers either. Especially advanced FLOSS users which are the vocal minority in FLOSS world. Pro users have a bad tendency of adding (or suggesting this) more and more widgets, buttons and checboxes without thinking about overall design ;)\r\n\r\nSo in this perspective developers are the ones which have more to say about their application. They know what they want to achieve. AFAIR Amarok developers during 2.0 development clearly stated what is their vision, and 2.0 is really good implementation of this vision. \r\n\r\nNot agreeing to this vision is completely different topic. \r\n\r\nAlso lack of usability experts willing to work with both developers and end users is completely different topic."
    author: "Piotr Pe\u0142zowski"
  - subject: "Usability experts and Amarok..."
    date: 2009-03-08
    body: "<em>\"Also lack of usability experts willing to work with both developers and end users is completely different topic.\"<em>\r\n\r\nJust FYI: We have _two_ notable usability people working closely with us: Dan Leinir Turthra Jensen, and Celeste Lyn Paul. The names might ring a bell.\r\n\r\nWe take usability very seriously, better believe that. We've made mistakes in the past, and we don't plan to repeat them. And we are continuously working very hard on improving in this area; it's a \"priority thing\" for us.\r\n"
    author: "Mark Kretschmann"
  - subject: "Mark, when writing last"
    date: 2009-03-08
    body: "Mark, when writing last sentence I was thinking generally about FLOSS world. I could write it more clearly...\r\n\r\nI'm really glad that usability people are working with amarok devs :)"
    author: "Piotr Pe\u0142zowski"
  - subject: "as an amarok 1.4 user, i"
    date: 2009-03-08
    body: "as an amarok 1.4 user, i still feel that dragging from collection to playlist is unnecessary more complex and unintuitive in v2.\r\nwould be interesting to see percentage of users who would choose to put playlist besides collection if that was possible ;)"
    author: "richlv"
  - subject: "I feel that too. Thought that"
    date: 2009-03-09
    body: "I feel that too. Thought that maybe it was just a \"bad habit\" from using amarok 1.4 and have been giving it a chance but it stills feel kind of weird. \r\n\r\nPart of it is simply how the distance makes it more weird in my mind. We are doing something on the far left to affect what's on the far right, with that big whole in the middle. Where should I focus my attention. \r\n\r\nBut then there is the dragging itself too. Sure if you just want to append you got alternatives. But what if you want to put songs from your collection in a particular place in your playlist. How do you do that without dragging it all the way from one side to the other?"
    author: "jadrian"
  - subject: "Bugzilla"
    date: 2009-03-09
    body: "I haven't yet received my bugs.kde.org login details. It is possible and sadly very probable my providers filter again might have dropped it. This wouldn't be the first time.\r\nWhy is there no unified kde login? Except the svn maybe...\r\nOn the Digikam note i found a workaround to keep the memory consumption reasonable: I simply let a script traverse all folders and set the index files to 000. I have added to the script to start PicStor to restore them to 600. But I have not yet found a way to tell Digikam to accept non raw photos which have extensions other than jpg or 0000. This is rather confusing as Dolphin and Gwenview display and do accept them. Maybe pebkac but more i suspect the factory 4.2 rpms may be the reason. I'll try to build rc2 0.10 myself to see if works better."
    author: "AJ"
  - subject: "And as an amarok user from 0.4 days..."
    date: 2009-03-09
    body: "(yes, 0.4, not 1.4)\r\n... I can say that I really DO like Amarok 2 more than 1.x. The UI is simpler, clearer and more focused on one thing: the context. What's the point in having a giant playlist occuping a width of ~1200px? When you're listening to music, probably you have amarok minimized and you're doing another task. When Amarok is maximezed and focussing your attention, what's the point in staring at a giant playlist? Are you messing up the playlist every moment? Or perhaps you just create it once (when starting your listening session) and maybe modify something here and there from time to time?\r\n\r\nI mean, the context view is not perfect (layouting could be enhanced/fixed for example.. yes arrows that control the \"activities\", I'm pointing my finger at you!) but it without ANY doubt the way to go.\r\n\r\nAmarok 1.4 is dead. Hurray for Amarok 1.4"
    author: "Vide"
  - subject: "Are you in the '90's?"
    date: 2009-03-09
    body: "Ok, I'm not saying that a good design isn't important (it IS important) but as others already said.. do you know agile dvelopment? Release early, release often has proven to be quite successful in the FLOSS world (do you know that software called \"Linux\"?). Moreover freesoftware has always and always will have a clear hacker attitude. To do boring software development there are old school companies which pay their employees to do just that: lot of boring development overhead."
    author: "Vide"
  - subject: "Thanks.."
    date: 2009-03-09
    body: "Thank you for this thoughtful and constructive comment. As for the context view - you are right with your criticism - and we have been working hard to improve it for the upcoming Amarok 2.1.\r\n\r\nPlease check out <a href='http://padoca.wordpress.com/2009/02/15/amarok-21-back-to-the-future-english-version/'>this informative blog</a> for an overview of Amarok 2.1 features.\r\n"
    author: "Mark Kretschmann"
  - subject: "Issues with Amarok playlists"
    date: 2009-03-09
    body: "I'm using a regularly compiled SVN snapshot of KDE and extragear, and for about two months now, My playlists in Amarok are quite broken : showing me lots of weird new playlists that seem to point to some m3u files in my music dirs, not allowing to save new playlists, I even cannot create dirs anymore.\r\nIs that something normal considering the development state of Amarok or is that something should be reported?"
    author: "Richard Van Den Boom"
  - subject: "if it shows up as a new user"
    date: 2009-03-09
    body: "if it shows up as a new user (or after removing config files and database - back them up first!) you should report it as an bug..."
    author: "jospoortvliet"
  - subject: "No, I am in the 2K's"
    date: 2009-03-09
    body: "I know about eXtreme Programing and I think that it is a good idea. \"Agile Software Development\" appears to be similar.  XP requires that you \"Release early, release often\"\r\n<p>\r\nHowever, these good ideas are not an excuse for  writing code without designing it first (hacking) and the poor design that is the usual result.  XP and ASD do not mean that we should discard the traditional engineering cycle (analyze, design, implement), rather they require that the cycle is done many times on a small part of the project rather than the inefficient old school method of doing it once for the whole project.</p><p>Is the KDE project actually meeting the goals stated in the ASD manifesto?</p><p>\r\nhttp://agilemanifesto.org/\r\n</p><p>\r\nWorking software?\r\n</p><p>\r\nCustomer collaboration?\r\n</p><p>\r\nIf not, perhaps we need to refine our development methods.\r\n"
    author: "KSU257"
  - subject: "Useless  boast"
    date: 2009-03-10
    body: "\"Write &lt;amazing thing&gt; in &lt;very few&gt; lines of code!!\" claims are always pointless. They always boil down to something like:\r\n\r\n#include &lt;web_browser.h&gt;\r\n\r\nint main()\r\n{\r\n&nbsp;&nbsp;Browser b;\r\n&nbsp;&nbsp;b.run();\r\n&nbsp;&nbsp;return 0;\r\n}\r\n\r\nOMG! I just wrote a web browser in 2 lines of code!!!"
    author: "Tim"
  - subject: "but useful .."
    date: 2009-03-10
    body: "in this case, however, it's not just a proof of concept that's useless but something that allows people do something both useful and commonly desired: embed media (audio, video..) into a widget with extreme ease. it's not at all, therefore, like your web browser example which is, indeed, typically what these sorts of things are. this is an actually useful step that lowers the bar to creating actually useful things. :)"
    author: "Aaron Seigo"
  - subject: "great one"
    date: 2009-05-24
    body: "As a developer - consultant myself, I would love such feature. But I am still waiting.... please hurry up -:) \r\n<span style=\"color:white\">worldclass for models clothes shopping  <a href=\"http://fashion4us.com\" style=\"color:white\">hong kong fashion</a> garments tops.</span>"
    author: "peterpoboski"
---
In <a href="http://commit-digest.org/issues/2009-02-15/">this week's KDE Commit-Digest</a>: An example <a href="http://phonon.kde.org/">Phonon</a>-based video player in 5 lines of javascript code. Beginnings of a D-Bus interface for the <a href="http://plasma.kde.org/">Plasma</a> "System Tray" daemon. Start of support for retrieving contacts from <a href="http://pim.kde.org/akonadi/">Akonadi</a> through the Plasma DataEngine. Improvements in the "Converter" and "System Monitor" Plasmoids. Continued development of multi-language spellchecking support in Sonnet. Support for "hybrid suspension" in <a href="http://solid.kde.org/">Solid</a>. Continued development of <a href="http://amarok.kde.org/">Amarok</a> 2, including filtering by date. Further work on LinTV. Support for recordings added to <a href="http://kaffeine.sourceforge.net/">Kaffeine</a>. A new puzzle table texture selector in Palapeli. Consistent keyboard shortcuts added to all KIPI import/export plugins (used by <a href="http://www.digikam.org/">Digikam</a>, <a href="http://gwenview.sourceforge.net/">Gwenview</a>, etc). Hover buttons to rotate left and right added to KRuler. Foundations for a microblog resource for Twitter and Identi.ca. Initial 7z plugin added to <a href="http://utils.kde.org/projects/ark">Ark</a>. Various SVG import improvements in <a href="http://koffice.kde.org/karbon/">Karbon</a>. Get Hot New Stuff (linked to kde-look.org) support added to deKorator. Various polishing in KWin-Composite. Search capabilities (matching System Settings) added to the KDE 4 port of KControl. The "Calendar" Plasma DataEngine moves to kdebase. KTron moves from playground/games to kdereview. Import of KALEngine to playground/games. Initial import of Blazer, a "simple desktop shell for use with thin clients and virtualized remote desktops", and "a KIO-slave listing the network from a device centric point of view" to KDE SVN.

Also in this Digest, Seb Ruiz writes about the reception of Amarok 2 and current and future developments. <a href="http://commit-digest.org/issues/2009-02-15/">Read the rest of the Digest here</a>.
<!--break-->
