---
title: "New KDE Buzz"
date:    2009-08-04
authors:
  - "Bille"
slug:    new-kde-buzz
comments:
  - subject: "Integration"
    date: 2009-08-04
    body: "While I'm happy this service exists, I'm wondering about KDE's pending focus problems :P\r\n\r\nActually, it would be nice if some of these new KDE sites and services could be inter-connected in some way.  For example, why can't buzz be integrated into planetkde? This would require less websites to check, and reduce fracturing the readership?\r\n\r\nActually, this goes for many other KDE website/services as well..."
    author: "troy"
  - subject: "very different..."
    date: 2009-08-04
    body: "Well Buzz and Planet are two very different things. I wouldn't want to mash them together.\r\nBuzz is showing opinions, comments, rants, images, videos and so on from people that are not necessarily (most likely not) part of the KDE community. It's a showcase of what people all around the world are thinking of KDE.\r\nPlanet on the other hand shows blogs and identi.ca/twitter from contributors to KDE. It's a showcase of what the KDE project is working on."
    author: "nightrose"
  - subject: "Performance"
    date: 2009-08-05
    body: "Hello,\r\n\r\nnice new site.\r\nI use ff and opera (latest versions) and the page gives my CPU a high load.\r\nDo others have the same behaviour?\r\n\r\nBtw. I get lost in our websites as well. Too many of them are around. jm2c.\r\n\r\nCheers,\r\nMark"
    author: "Mark Ziegler"
  - subject: "Same here"
    date: 2009-08-05
    body: "Got the same issue here, Firefox 3.5 mentioned a script was running too long."
    author: "vdboor"
  - subject: "Hi,\nI got the same problems"
    date: 2009-08-05
    body: "Hi,\r\n\r\nI got the same problems with firefox 3.0.x and 3.5.x...\r\n\r\nI also tried konqueror, and after a little bit of a hang - it runs smoothely there.\r\n\r\n\r\nCheers,\r\nGregor"
    author: "polysem"
  - subject: "Yes, in Seamonkey it is so"
    date: 2009-08-06
    body: "Yes, in Seamonkey it is so slow you can hardly use it."
    author: "stlpaul"
  - subject: "Same here"
    date: 2009-08-07
    body: "same problem here, with all my browsers"
    author: "rickychiumiento"
  - subject: "Browsers..."
    date: 2009-08-08
    body: "No problem at all here with arora... workin real smooth, haven't tried any\r\nother tho."
    author: "TheRob"
---
While you wait for the KDE 4.3 gates to open, you may be interested in our new <a href="http://buzz.kde.org">buzz.kde.org</a> site, using an experimental "LifeStream" tracking KDE on identi.ca and Twitter, Picasaweb, Flickr and Youtube. Check out buzz.kde.org for the stream for who's saying what about the hottest Free Desktop release this year!