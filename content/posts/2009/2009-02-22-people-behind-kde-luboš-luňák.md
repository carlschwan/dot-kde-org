---
title: "People Behind KDE: Lubo\u0161 Lu\u0148\u00e1k "
date:    2009-02-22
authors:
  - "jriddell"
slug:    people-behind-kde-luboš-luňák
comments:
  - subject: "Great interview"
    date: 2009-02-22
    body: "Actually a very interesting and enjoyable interview -- I think the more flexible format is a lot better for these kind of developer interviews. "
    author: "apokryphos"
  - subject: "+1"
    date: 2009-02-22
    body: "That was the first interview of People behind KDE that made me laugh out loudly. The best part was the one about bus numbers. Also very good was Lubos' comments on KHotKeys code readability. I just checked out the sources of KHotKeys before the 4.2 rewrite and found that comment: \"that main reason for existence of this class is the fact that I was very curious if overloading operator, ( = comma ) really works ( it does, but not exactly as I expected :( )\""
    author: "majewsky"
  - subject: "encoding in the main page"
    date: 2009-02-23
    body: "something's fishy with Lubo\u0161's name encoding on the kde.org frontpage (news section) - i couldn't figure out which encoding is used there, it's garbled in all i tried :)"
    author: "richlv"
  - subject: "Date: 1st September 2008"
    date: 2009-02-23
    body: "WTF? Bit late, isn't it?"
    author: "KAMiKAZOW"
  - subject: "Re: Date: 1st September 2008"
    date: 2009-02-23
    body: "It's an interview with a developer about their past and present involvement in KDE, and their personality and life story... so it's hardly time-sensitive, and it was updated before publication.\r\n<br><br>\r\nDanny"
    author: "dannya"
  - subject: "Hmmm"
    date: 2009-02-23
    body: "Curious that his desktop looks a lot like Gnome's :P"
    author: "NabLa"
  - subject: "Yes and no."
    date: 2009-03-06
    body: "Picking a date to put on this one is a bit hard. The pre-aKademy bits were done in August. The coda was done September-October. Exams in December delayed everything, and by then it actually was a bit out of date and inaccurate in places. So it got tweaked a bit, and put through the wringer again. Then I sat on it and wondered just what date *to* put on it. :D\r\n\r\nThis interview was interesting: Since it was my first one with a non-native English speaker, I wanted to make sure that what he said actually made sense and was what he wanted it to be. So there was an extra round or two of editing.  \r\n\r\nIt was also my first interview with someone not anywhere near my timezone. At the time, our schedules didn't overlap very well. I asked two questions a day!"
    author: "blauzahl"
  - subject: "great interview"
    date: 2009-03-14
    body: "informative and entertaining.  it's really cool to see behind-the-scenes of my favourite desktop environment."
    author: "klaatu"
---
In the latest <a href="http://www.behindkde.org/">People Behind KDE</a> interview, we cover someone who has been with us for a long time. Alternately known as "Evil Genius" or "Rock Star with an International Fan Club". He still has his plushy lion, but has to now rectify his work on KWin with a dislike of fancy graphics and reveals the story behind his blue hair appearance. These are the publishable parts from our interview with our current star of People Behind KDE: <a href="http://behindkde.org/people/seli/">Luboš Luňák</a>.
<!--break-->
