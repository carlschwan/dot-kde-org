---
title: "KDE 4.3.0 Released: Caizen"
date:    2009-08-04
authors:
  - "sebas"
slug:    kde-430-released-caizen
comments:
  - subject: "Thank you!"
    date: 2009-08-04
    body: "Thank you to all KDE developers and all others involved (translators, artists, usability experts, ...) in making this release as awesome as it is! I have been using it since the betas and am highly impressed. It surely is the best free desktop release I have ever seen. If you would only not tease us with all those reports about the progress toward 4.4... :P\r\n\r\nSo, again, thank you to all! :)"
    author: "mutlu"
  - subject: "It's amazing!"
    date: 2009-08-04
    body: "Thank you very much indeed and Congrats to you all! I love it! Though thanks to your merciless teasing now I can't wait for 4.4 :D"
    author: "bliblibli"
  - subject: "Congratulations"
    date: 2009-08-04
    body: "Thanks KDE team for this great release"
    author: "Amine27"
  - subject: "Well done..."
    date: 2009-08-04
    body: "... you good and faithful developers. Thanks for another great release, for your good work, your invested energy, time and patience which I think all KDE users appreciate greatly.\r\nI am using 4.3 on openSuse 11.2 M4 and it feels really great, it feels even better than it  does on openSuse 11.1. I tell you, Vista 2 aka Win 7 is going to get cold feet ;)\r\nBig thanks to the KDE team."
    author: "Bobby"
  - subject: "updating...."
    date: 2009-08-04
    body: "updating from KDE 4.3 RC 3.... cant wait to have it :D"
    author: "jaom7"
  - subject: "Yes, we're like that. And"
    date: 2009-08-04
    body: "Yes, we're like that. And we're not even sorry!!! MUHAHAHA\r\n\r\n\r\n(be careful: the screencast was made with trunk. It is barely visible to the naked eye, if at all, but on an unconscious level it will tease you into installing KDE 4.4 beta as soon as it is out)"
    author: "jospoortvliet"
  - subject: "How to mount?"
    date: 2009-08-04
    body: "How to mount an external disk in KDE4?\r\n\r\nI already have one disk plugged in and now I plugged another one in.\r\nClicking on device notifier plasmoid it shows:\r\n\r\n- Volume (ext3)\r\n- Volume (ext3)\r\n\r\nI know that version 4.3 means it's still \"alpha quality\", but I would expect\r\nto show at least size and /dev/name so one can see some difference between\r\ntwo disk and know which one to mount.  OK, move one, I'll mount them both.\r\nRight click - nothing, right click again - nothing.  Strange.  OK.  Left\r\nclick...  I think it tries to open dolphin but fails, of course, since I\r\ndon't use dolphin and don't have it installed.  Also, I don't want to open\r\nthat device, JUST to mount it so I can save some files to it from\r\napplication that's ALREADY OPEN. Simple task, but not so simple if you are\r\nusing KDE. Back to konsole and mount(8). We have bling bling, that's what's\r\nimportant!\r\n\r\nI haven't seen so much \"innovation\" since I started using GUIs (Windows 3.1\r\ntime). Congrats people! Fanboys, you know the drill. ;)\r\n"
    author: "vedranf"
  - subject: "Blah blah blah"
    date: 2009-08-04
    body: "Well, very nice ideas you have to improve the current behavior. So, go there and code it!"
    author: "traysh"
  - subject: "It was all fine..."
    date: 2009-08-04
    body: "...in KDE3, but they had to innovate. ;)"
    author: "vedranf"
  - subject: "Caizen tte nani"
    date: 2009-08-04
    body: "I believe the correct romanization would be \"kaizen\". Or is it a witty reversal on the k-pun? ;-)\r\n\r\nThat said - KDE ha ichiban dayo; ganbatte, minna.\r\n\r\n"
    author: "lucke"
  - subject: "yes, how to mount?!"
    date: 2009-08-04
    body: "...so I went to the console, but did not know which /dev/... actually to mount and how many partitions there are... so I found out, but it told me I have to be root to mount it... damn, I should have never left my Windows 3.1!\r\n\r\nWell, seriously: this was the first thing I got crazy about with KDE4, and 4.3 is actually the first version to improve it. But I have to admit, that the fact that I have to add manually(!) an \"empty action\" (i.e. a \"do-nothing-action\"!) to do a \"do-mount-only\"... does not really make me very comfortable as a user.\r\n(Well, at least that was the RC version... is it better with the final release?)"
    author: "wanthalf"
  - subject: "KDE is making fun of itself a"
    date: 2009-08-05
    body: "KDE is making fun of itself a little with the release names.  Since we traditionally made everything start with a K, we're doing it backwards sometimes - maybe to restore 'carma' or something :P  Check some of the more recent release names for things in the KDE world, you'll find: \"The Answer\" for 4.2, also a bit of an inside joke."
    author: "troy"
  - subject: "Fantastic"
    date: 2009-08-05
    body: "I've used it a few times and love it. It seems a bit snappier to me than 4.2"
    author: "tubasoldier"
  - subject: "Good job !"
    date: 2009-08-05
    body: "Thanks for this release again, may the force be with you in future one.\r\n\r\nI'm updating now ..."
    author: "Zenithar"
  - subject: "Great stuff!"
    date: 2009-08-05
    body: "If this is like the RCs that I have been using on my machine then it will be a very nice, polished release indeed. Great work and thanks to all contributors!"
    author: "bluelightning"
  - subject: "More changes?"
    date: 2009-08-05
    body: "I'm running KDE 4.3 since the betas and like it very much.\r\n\r\nUnfortunately, while everything was fine up to RC3, the final yesterday broke Nepomuk (on two machines, I had to delete my index and start it again) and, a more serious issue, the NetworkManager applet: I lost my wireless connection and can't bring it up again (just nothing is shown in the menue, although in the settings I can scan for networks). The now rocking plasmoid stopped working, too.\r\n\r\nI know, this thread is not for bug reports, nevertheless I want to ask how come that from RC3 to final there were such changes causing problems, I thought there should be nothing else but bug fixing.\r\n\r\nAnyone else having these problems? It could be a problem with the opensuse packages, too."
    author: "Kirilo"
  - subject: "Congrats!"
    date: 2009-08-05
    body: "Looks like KDE4 is progressing quite nicely! I'm looking forward to installing it as soon as I have some time...\r\n\r\nThe screencast is quite nice. One question though: What's the deal with rotating the widgets? There must be some thoughts about where that is useful, I just haven't seen any yet and can't think of any either? As an example, the screencast shows it's possible to rotate the calendar, but I couldn't imagine I would ever want to do that. If I wanted to rotate anything, it would be the whole desktop, not just the calendar widget, so why is the option available on the calendar widget? It will likely only irritate the user because he/she will click on it by mistake.\r\n\r\nWell, I could see one thing that could be interesting to rotate and that would be a video player widget - sometimes people turn their camcorder which of course means the video will display wrongly when played on the computer."
    author: "jramskov"
  - subject: "KDE3 --vs-- KDE 4.3 aka Windows 3.1"
    date: 2009-08-05
    body: "Well so far I'm less than impressed, but in all fairness KDE 4.x *IS* improving, but still has a long ways to go. To date no one has given a justifiable reason for the 180 on the desktop. I still do not know why I can't put icons on the desktop proper as well as \"containers\". It would be a vast improvement if KDE would simply splice both the good features of the KDE 3.x series with the new ideas in KDE 4.x series. Right now KDE is re-inventing the wheel. If you look at both Windows 3.1 and KDE 4.x they are almost identical in their approach. Probably what ticks me off more than KDE was the force fed diet of KDE 4.0 by the Fedora Project. Indeed Fedora has gone in the wrong direction by putting stuff not ready for prime time and making it the \"default\". More than anything else their rather hasty decisions have prejudiced me against potentially good technologies, such as KDE 4.x. into their distro. To be sure KDE still has a lot of work to do to bring it up to the quality of the KDE 3.5 series. But at the end of the day the user is left with an sub-par GUI that is almost identical to Windows 3.1, though M$ did it better. Fair is fair we have to wait till all the apps are \"converted\" to the \"new\" system of KDE 4.x which differs little from Windows 3.1. we understand that. The question is WHY when we already had an excellent desktop with KDE 3.5x. If something is not broke why \"fix\" it? To be sure Windows 3.1 -- and KDE 4.x -- had/have good idea using \"containers\" -- or whatever you want to call them, but it is the exact same idea. KDE 3.x also had good ideas that have been trashed to make room for the \"innovative\" -- NOT!!! -- ideas of KDE 4.x. It would be wonderful to see a marriage of both the standard desktop of the KDE 3.x series with the \"container\" ideas of the KDE 4.x aka Windows 3.1, series. In the current format -- KDE 4.2 -- It is still way too confusing to use: lost containers, lost locations, etc, etc, etc., still KDE 4.2 is a VAST improvement over KDE 4.0. Please do us all a favor  and shoot -- metaphorically speaking -- the project manager of the FEDORA PROJECT: More than anything, or any one, else he did more damage to your project could have. He is single handedly destroying an excellent distro by incorporating a bunch of crapware and making it the default. I'll be looking forward to KDE 4.3, and all future versions of KDE 4.x I most sincerely hope that it will someday reach the quality of the KDE 3.5x series. The marriage of the standard desktop along with the \"container\" ideas of KDE 4.x would go a long ways to creating a universal and flexible desktop that the USER can set up to THEIR desire. I sincerely hope that will be one of your incorporated improvements in future releases of the KDE 4.x series. "
    author: "azbobcat"
  - subject: "Use KDE 4 knetworkmanager for NetworkManager"
    date: 2009-08-05
    body: "Use the knetworkmanager client if you are using the plasmoid.\r\n\r\nThere is however a bug in knetworkmanager that I fixed yesterday with the default config - wireless and cellular interfaces are not shown in the tray icon.  \r\n\r\nThe workaround is easy, open the config, go to Other Settings and drag all the interface types so they are below 'Icon 1'.  "
    author: "Bille"
  - subject: "You made my day :-)"
    date: 2009-08-05
    body: "\"The workaround is easy, open the config, go to Other Settings and drag all the interface types so they are below 'Icon 1'.\"\r\n\r\nThat did it, thanks a lot!\r\n\r\nAnd after re-indexing Nepomuk seems to be fine again, too. I just have to take a look at the NowRocking config in order to be happy again. ;-)"
    author: "Kirilo"
  - subject: "how to mount?"
    date: 2009-08-05
    body: "i think you should try a different linux distribution.\r\nIn my case, any drive i plug in is automaticly mounted. I don't need to do any extra clicks or commands.\r\n"
    author: "whatever noticed"
  - subject: "KDE3 --vs-- KDE 4.3 aka Windows 3.1"
    date: 2009-08-05
    body: "It is quite funny to see you comparing kde 4.3 with windows 3,1, as they both have close to nothing in common.\r\nThis is like comparing windows 7 with windows 3, by pointing out they both are using windows to show different applications at the same time on the screen..\r\n"
    author: "whatever noticed"
  - subject: "rotating image"
    date: 2009-08-05
    body: "rotating is also useful for images, though I must admit that I seldom use it myself. \r\n\r\nI wouldn't like to see it go away, though. For heavy customization it still looks very nice - maybe I want _my_ desktop to have a folder-view rotated to the left on teh bottom left side, mascerading as some game interface :) \r\n\r\nI'd love to see a KDE Starcraft lookalike someday ;) \r\n\r\nOverall I've been using KDE4.3 since rc1 and I love it! Also I liked reading the news post. Nice work on getting to the codename :) \r\nAnd the screencast is really well done - damn interesting to watch. \r\n\r\nMany thanks to all the great KDE developers, documenters, and advocates who made 4.3 possible! \r\n"
    author: "ArneBab"
  - subject: "Why should I listen to..."
    date: 2009-08-05
    body: "someone who suggests changing whole distribution to get automounting? You probably buy a new car when you want to change CD player in it?\r\n\r\nAnd btw., I don't want automounting. It reminds me of reasons why I don't use windows.\r\n"
    author: "vedranf"
  - subject: "automount"
    date: 2009-08-05
    body: "\r\n\"someone who suggests changing whole distribution to get automounting? \"\r\n\r\nYup, if your distribution can't do the job, it is time to change to something else.\r\nBut in stead you blame the desktop for not doing something the operating system below should do.\r\n\r\n\r\n\"You probably buy a new car when you want to change CD player in it?\"\r\n\r\nNope, but i do buy a new car if my current one doesn't have climate control and i like to have that feature. \r\nI'm not blaming the cd player manufacturer for not providing buttons to adjust the climate control.\r\n\r\n\" don't want automounting. \"\r\n\r\nThen i surprises me that you aren't able to mount the drive manually.\r\nI would expect you would have that knowledge or be able to find an application that handles the manual mounting for you.\r\n\r\n\"It reminds me of reasons why I don't use windows.\"\r\n\r\nCould be me, but if you plugin a mass storage device, doesn't that mean that you want to use it?\r\n\r\n"
    author: "whatever noticed"
  - subject: "you're a few releases behind apparently?"
    date: 2009-08-05
    body: "> The marriage of the standard desktop along with the \"container\" ideas \r\n\r\nit's called Folderview and you can switch your whole desktop to it, giving you what you refer to as a \"standard\" desktop, by right clicking on your desktop and going to Desktop Settings. it's the first setting right at the top.\r\n\r\nthis has been there since 4.2 and you could do it manually (with some caveats) in 4.1.\r\n\r\n> of KDE 4.x would go a long ways to creating a universal and flexible \r\n> desktop that the USER can set up to THEIR desire.\r\n\r\nas you can see, we've already done that. but we've gone one better and made it so that your definition of what users desire isn't forced on everyone else either."
    author: "Aaron Seigo"
  - subject: "shenanigans"
    date: 2009-08-05
    body: "i call shenanigans. let's see:\r\n\r\n\"but I would expect to show at least size and /dev/name so one can see some difference between two disk and know which one to mount\"\r\n\r\ni just plugged in a removable disk and i get \"698.6 GiB  External Hard Drive\" in: the device popup, kickoff's Computer tab, dolphin and the file open/save dialog. so you do get size. as for /dev/name, that's a technical detail and not even available on all OSes we support.\r\n\r\n\"Right click - nothing, right click again - nothing. Strange.\"\r\n\r\nright click? c'mon. you mouse over it and it tells you either \"open in file manager\" or \"2 actions available\". (personally, i'd like to see those actions available directly via a left-clickable menu, but that's another story). the natural action is to left click on it to launch, just like _every other icon in KDE_.\r\n\r\n\"think it tries to open dolphin but fails, of course, since I don't use dolphin and don't have it installed\"\r\n\r\nthat's probably a bug, though given the rest of your comment i'd want to check locally first to confirm your assertions. i assume you don't have konqueror installed either? If so, i have to say that you're the first person i've run into who is running plasma without a KDE file manager _somewhere_\r\n\r\n\"JUST to mount it so I can save some files to it from application that's ALREADY OPEN\"\r\n\r\nin any KDE4 application, you'll see it listed in the side bar in the file save dialog. and yes, even before it's mounted.\r\n\r\nif you're using an application that doesn't support that functionality, i suggest finding a new application to do that task because that is a farily basic function for today's modern applications.\r\n\r\n\"We have bling bling, that's what's important!\"\r\n\r\nit seems what we really have is someone who is running around and trying to out-think the system instead of just using it. you plug in a drive, an interface is there and you RIGHT click it first? you're running a desktop shell without any file managers available or configured? etc...\r\n\r\nso yes, i call shenanigans on your entire posting. the jabs at the end of it don't lend any credibility either.\r\n\r\n"
    author: "Aaron Seigo"
  - subject: "rotation"
    date: 2009-08-05
    body: "\"What's the deal with rotating the widgets?\"\r\n\r\na number of people actually enjoy being able to rotate widgets on their screen, esp ones showing photos or what not. but that wasn't the primary reason i had in mind.\r\n\r\nthink about a screen that is laying flat on a table or other horizontal surface. position yourself and a couple other people randomly around it. it becomes natural to orient different widgets towards who ever is currently using it.\r\n\r\nproblem with adding rotation as a feature only for those use cases is that it then never gets tested (or, probably, even written) by anyone because so few people have such hardware today.\r\n\r\nthen again, i do have an internet tablet. they are likely to grow in usage.\r\n\r\nin the meantime, people can make pretty sets of photos on their desktop and plasma is ready for the near future should that include flat-mounted screens. :)"
    author: "Aaron Seigo"
  - subject: "Congratulations !"
    date: 2009-08-05
    body: "Dear KDE-team,\r\nKDE is becoming a great DE again! I have been working with the RCs and Betas and it has been mostly real fun. I am now very positive and optimistic about the things to come and I am really looking forward especially to fruits of Akonadi. I hope there will be a lot of it in 4.4. \r\nI just forwarded this announcement to my Facebook feed. While doing that I was rereading that announcement and was thinking about all my non-tech-savy friends. I think many of them would not understand part of the tech-bable. Since I now I really believe KDE4 is really for the average user, I would to suggest that you might publish two different types of announcements: One for the geek crowd and one for the rest. "
    author: "mark"
  - subject: "Great Work!!!"
    date: 2009-08-05
    body: "Great work and my congratulations to all the people behind KDE. All of you are into something good"
    author: "jaom7"
  - subject: "thanks"
    date: 2009-08-05
    body: "It also worked for me.. thanks"
    author: "jaom7"
  - subject: "Tralala"
    date: 2009-08-05
    body: "This is free software, but free speech it is not.  My account got blocked by the fanboys. So, if you don't have positive experience with KDE you are not allowed to talk about it. This will be my last comment here.\r\n\r\n> i just plugged in a removable disk and i get \"698.6 GiB External Hard Drive\" \r\n\r\nWhy would I lie? Ok, seeing is believing: \r\n\r\nhttp://img205.imageshack.us/i/mediadevices.png/\r\n\r\nAs you can see, no size is shown.\r\n\r\n> as for /dev/name, that's a technical detail\r\n\r\nSo, hide technical details from them because KDE users are stupid? ;)\r\n\r\n> right click? c'mon. you mouse over it and it tells you either \"open in file \r\n> manager\" or \"2 actions available\". (personally, i'd like to see those actions \r\n> available directly via a left-clickable menu, but that's another story). the \r\n> natural action is to left click on it to launch, just like _every other icon in \r\n\r\nWell, I use left click to launch and right click to get options. I'm used to that since early 90s. Now KDE invented \"no right click\", \"no options\", \"no mount\".\r\n\r\n>i assume you don't have konqueror installed either? If so, i have to say that > you're the first person i've run into who is running plasma without a KDE \r\n> file manager _somewhere_\r\n\r\nNo, I don't have konq. Konq is dolphin in KDE4. No difference between them. And dolphin, like nautilus or win. explorer sucks and is useless for me. I use krusader or konsole. I'm sure that I'm not a first such person. I just want to use my computer, not to put it on the wall and show it to my friends.\r\n\r\n>in any KDE4 application, you'll see it listed in the side bar in the file save \r\n>dialog. and yes, even before it's mounted.\r\n>if you're using an application that doesn't support that functionality, i >suggest finding a new application to do that task because that is a farily >basic function for today's modern applications.\r\n\r\nI'm not a child or a office secretary. I actually do something, just like you. Today, application you are talking about is MATLAB and there is no alternatives to it. Tomorrow, there will be some other application. So, you are suggesting me to stop using MATLAB because it doesn't allow me to mount media?! Mounting media is not MATLAB's job, but job of a desktop shell. And that desktop shell doesn't have some essential features like manual media mounting. \r\n\r\n> it seems what we really have is someone who is running around and trying \r\n> to out-think the system instead of just using it. you plug in a drive, an \r\n> interface is there and you RIGHT click it first? you're running a desktop \r\n> shell without any file managers available or configured?\r\n\r\nIt is not hard to out-think KDE. Just try to mount external device. In KDE3 I used media applet for kicker which offered a menu with actions (including mount). Right click on a device seemed obvious for me to get similar menu now. But... innovation.\r\n\r\n"
    author: "vedranf2"
  - subject: "+1 on that!"
    date: 2009-08-05
    body: "4.3 is a beauty! :)"
    author: "gwittenburg"
  - subject: "Don't be silly..."
    date: 2009-08-05
    body: "> Yup, if your distribution can't do the job, it is time to change to something \r\n> else. But in stead you blame the desktop for not doing something the \r\n> operating system below should do.\r\n\r\n?\r\n\r\nYou don't understand the basics: Distribution is just a collection of software. Enabling auto-mounting on my or any other distro is just a matter of installing and configuring appropriate software (with apt-get, rpm, emerge, whatever).\r\n"
    author: "vedranf2"
  - subject: "This is a bug that has been known since KDE4.0 beta"
    date: 2009-08-05
    body: "I consider your complaint very justified. \r\n\r\nThis bug (or changes to the current setup) has been reported and a fix requested many times. Still the most glaring regression for me personally compared to KDE-3.5\r\n\r\nAll of these bug reports are for basically the same issue:\r\n\r\nhttps://bugs.kde.org/show_bug.cgi?id=164053\r\nhttps://bugs.kde.org/show_bug.cgi?id=175611\r\nhttps://bugs.kde.org/show_bug.cgi?id=174612\r\nhttps://bugs.kde.org/show_bug.cgi?id=187054\r\nhttps://bugs.kde.org/show_bug.cgi?id=196624\r\n\r\n\r\n(I will merge these later.)\r\n\r\nUnfortunately, noone in the KDE core team felt like fixing this since, even though there appears to be a solution outside the tree here:\r\nhttp://www.kde-look.org/content/show.php/New+Device+Notifier+with+Automount?content=91517\r\n\r\nI did not manage to install it and do not know if it still works at all for KDE-4.3"
    author: "moritzmh"
  - subject: "Me feeling stupid"
    date: 2009-08-05
    body: "My distro does all of that for me. Pop what ever click and get auto mounted since 4.1 btw.\r\nActually I don't even know its mounted or not \"it just works\" TM. "
    author: "pinheiro"
  - subject: "..."
    date: 2009-08-05
    body: "I said that I won't be adding new comments, but I can't help myself. Sorry fanboys.\r\n\r\n> I consider your complaint very justified.\r\n\r\nActually, After Aaron's comment I see now that it wasn't justified. I just didn't think much. Adding more features introduces clutter and more options would confuse users, and we don't want that, don't we. We have to be careful with our users as giving them additional possibilities and useful options could force them to use GNOME. Also, enabling mounting without dolphin would break our design, vision and introduce unnecessary complexities in the code. Our motto is one \"dolphin per desktop\"  and \"you don't need the right click: with KDE you always click wrong\".\r\n\r\nNow, add analog clock plasmoid and resize it to the 1/4 of your desktop. Try to rotate it!! You see, you see... how it's funny. Now show it to your friends and you will most definitely be the koolest kid around like me! With useful features like this one, you will soon forget all about mounting and other techno babble. \r\n"
    author: "vedranf2"
  - subject: "you are just trolling"
    date: 2009-08-05
    body: "It is clear that you don't have any clue about what Aaron was talking about, nor are you interested in solving whatever problem you are experiencing.\r\nYou are just looking for a place to rant."
    author: "whatever noticed"
  - subject: "But, but,..."
    date: 2009-08-05
    body: "I could also install some automounting software or use pmount from console. Then install some keybindings daemon to bind multi key shortcuts to pmount commands. Then some printing software, then... What's the purpose of this DE then if I have to install tens of additions apps to complement its (KDE4) shortcomings? This is KDE's job and KDE compiled for svn should be able to do that without additional magic.\r\n\r\n\r\n"
    author: "vedranf2"
  - subject: "kinda odd"
    date: 2009-08-05
    body: "it is kinda odd that you know how to enable/disable automounting, but as soon as you have disabled it, you are clueless about how to mount the device manually. \r\nThat makes me wonder why you want to mount the device manually, and wonder why you are unable to install software that can do it for you.\r\n"
    author: "whatever noticed"
  - subject: "but, but, but.."
    date: 2009-08-05
    body: "you first crippled your distribution by disabling automounting, and now you expect the desktop to fix this for you by default?"
    author: "whatever noticed"
  - subject: "that is something different"
    date: 2009-08-05
    body: "What you are talking about is automounting devices, while he doesn't want them to be automounted at all. He likes to mount them manually."
    author: "whatever noticed"
  - subject: "Mounting just work here, I"
    date: 2009-08-05
    body: "Mounting just work here, I didn't need to install any additional software (kubuntu and archlinux). If you're compiling the whole kde from scratch you should expect to do some manual work, sounds logical to me... that's why distros exist."
    author: "patcito"
  - subject: "whine whine whine"
    date: 2009-08-06
    body: "darn, you're getting boring. Why don't you go and install that device notifier replacement thing from kde-look.org:\r\nhttp://www.kde-look.org/content/show.php/New+Device+Notifier+with+Automount?content=91517\r\n\r\nYou're so smart you can do that for sure. And maybe, if the guy writing the device notifier replacement wants to, that functionality can make it into KDE by default. Meanwhile, the fact nobody put that functionality in Plasma YET means not many ppl need it - and the minority who do will have to use KDE-LOOK or write something themselves. That's why the devs made it possible to easily write plugins in a gazillion languages for plasma, you know..."
    author: "jospoortvliet"
  - subject: "not a bad idea!"
    date: 2009-08-06
    body: "Not at all. I did try to make the announcement a bit easier to understand for non-techies, with the introduction to the workspace/app suites/dev platform and such, but it's still technical. If you are willing to 'translate' it for next time into something reasonably easy for newbies, please join kde-promo@kde.org ;-)\r\n\r\nI simply don't have time for that too..."
    author: "jospoortvliet"
  - subject: "..."
    date: 2009-08-06
    body: "> If you're compiling the whole kde from scratch you should expect to do some \r\n> manual work\r\n\r\nYes, I'm willing to do simple basic operations like manual mounting my devices. Can you show me how to do that in KDE4?\r\n\r\nI will be so kind to show you how it was done in KDE3:\r\n\r\nhttp://img44.imageshack.us/i/kde3mount.png/\r\n\r\nBut no more. Dictator said it's too complicated! I guess they do usability tests with monkeys?\r\n"
    author: "vedranf2"
  - subject: "Work-arounds"
    date: 2009-08-06
    body: "I can also manually mount by clicking on the device notification widget - that is not the point.\r\n\r\nI know I can add udev scripts to do anything in linux. However, KDE-3.5 allowed my to graphically configure a device to auto-mount. This is missing in KDE-4 so far. \r\n\r\nWhy do you defend the lack of this feature in KDE? Why do you think Debian is crippled?"
    author: "moritzmh"
  - subject: "Sure, first click on the"
    date: 2009-08-06
    body: "Sure, first click on the Device Notifier applet in the panel, there you should get the list of devices as you showed on your picture (I don't get size of devices either, like you btw). Then click on one of the device that is on the list, that will mount the device and open dolphin. To unmount a device, repeat first step, then hover the device you want to unmount, an eject icon will appear, click on it and it will unmount it. Works here (kubuntu, archlinux) and on my friends opensuse and gentoo."
    author: "patcito"
  - subject: "Your full of baloney..."
    date: 2009-08-06
    body: "Your full of baloney... maybe. Personally I have not seen anything you have posted so far to \"get you banned\". Or I must have missed the post. In any case, I have complained here many times about the shoddy shape MNHO thinks konqueror is in; especially with flash (minus the kmplayer jiggle) and no one has banned me yet. So I either missed your banning post, or you might want some mustard with that baloney."
    author: "stumbles"
  - subject: "..."
    date: 2009-08-06
    body: "> darn, you're getting boring. Why don't you go and install that device notifier \r\n> replacement thing from kde-look.org\r\n\r\nI would rather have as default something done right from the scratch than having to collect pieces all around the web which are intended to fix those shortcomings.\r\n\r\n> Meanwhile, the fact nobody put that functionality in Plasma YET means not \r\n> many ppl need it \r\n\r\nSomeone shown us 5 bug reports about this.\r\n\r\n(Actually ppl don't need KDE, only internet tablet with fullscreen web browser)"
    author: "vedranf2"
  - subject: "Great for you!"
    date: 2009-08-06
    body: "Now back to my first comment which started this discussion. Read it 10 times if needed.\r\n\r\n\r\n"
    author: "vedranf2"
  - subject: "You asked how I mount, I"
    date: 2009-08-06
    body: "You asked how I mount, I answered you, too lazy to go look at your first comment."
    author: "patcito"
  - subject: "Tasty baloney"
    date: 2009-08-06
    body: "Why would I lie and create a new user?\r\n\r\nhttp://img199.imageshack.us/i/bannedpwi.jpg/\r\n\r\nI also gave a lot of negative comments about KDE4 in the past, since 4.0. And nobody banned me up until now.\r\n\r\n> So I either missed your banning post\r\n\r\nIn every post I behave heretically towards the church of KDE. That cannot be tolerated. Everybody must be happy and fully satisfied when new KDE release arrives.\r\n"
    author: "vedranf2"
  - subject: "So true :) It looks great,"
    date: 2009-08-06
    body: "So true :) It looks great, performs great and is a pleasure to work with."
    author: "d2kx"
  - subject: "trolling"
    date: 2009-08-06
    body: "the problem is that you aren't criticizing the desktop, but just trolling. \r\n"
    author: "whatever noticed"
  - subject: "RE: workarounds"
    date: 2009-08-06
    body: "\"I can also manually mount by clicking on the device notification widget\"\r\n\r\nSo manual mounting does work in kde4.\r\n\r\nAccording to vedranf, manual mounting doesn't work.\r\n\r\n\"  Why do you defend the lack of this feature in KDE? \"\r\n\r\nI'm not defending that.\r\nEveryone knows that featurewise, kde4 is behind kde3. But trolling about it like vedranf does doesn't help at all. I won't feel urged to implement this with his aditude. Also, feature requests should be done on bugs.kde.org.\r\n\r\n\"Why do you think Debian is crippled?\"\r\n\r\nIt turned out that vedranf crippled his distribution himself. So i stand corrected: Debian isn't crippled, only the installation of vedranf is.\r\nVedranf also isn't looking for a solution.\r\nSeveral people have attempted to help out, but he refuses. He expects this to work by default, not after installing additional software or changing his configuration.\r\nThat is kinda silly.\r\n"
    author: "whatever noticed"
  - subject: "Release announcement"
    date: 2009-08-06
    body: "I am a geek, but for what it's worth, I thought the release announcement was pure genius."
    author: "tangent"
  - subject: "Better default fonts"
    date: 2009-08-06
    body: "Congratulations for the release!\r\n\r\nI haven't tried it yet, but according to the screenshots and the screencast it looks much cleaner than KDE 4.2. Better spacing in the taskbar, more fine tuned widget theme and no unproportional spacings in the default KWin windecs anymore... I like it! The icons were great from 4.0 on anyway.\r\n\r\nImho the appearance is a very important aspect to gain more users. For the most users it's simply the first impression they get from Linux.\r\nIn a time where Windows (from which most new users will come) doesn't look like some ugly Teletubbie/Fisherprice combination anymore this is more important than a few years ago, too. Nice to see a lot of improvements with KDE 4.3 there.\r\n\r\nBut what's still very ugly are the fonts. Compared to Vista's/Win7's Segoe or OSX's Lucida Grande with their optimization for the font rendering of their OS it looks very dated.\r\n\r\nI know, I know... it's a distribution thing, it's patent thing, it's a X.org thing, you have thousands of characters with Unicode... and there are people who hate antialiasing and there are the ones who love ist. It's quite complicated.\r\n\r\nBut I'd find it cool if the KDE team could come up with own recommended default fonts that look modern and beautiful. And ship it with the default packages. Helvetica, DejaVu, Bitstream Vera etc. look quite dated nowadays. Some nice narrow font like Frutiger or Lucida would look much more modern and would fit the modern look of the Plasma themes and the icons much better.\r\n\r\nUnfortunately I don't have anywhere near enough sparetime to do something like that. And I know that good screen fonts take a huge amount of time to create. But at the moment it's a single elements that makes the whole nice desktop look much worse than it deserves. And the fonts are always present, much more present than e.g. icons.\r\n\r\nJust look at the screenshots from the announcement. Beautiful icon artwork, a modern turquoise default wallpaper, a nearly borderless widget theme, little shadows everywhere, a very stylish clock... and a font that looks like it's from Windows 95 14 years ago. The KDE appearance deserves much better."
    author: "Stefan Kopic"
  - subject: "Had to test it here"
    date: 2009-08-06
    body: "Really don't get his problem, ok so i insert a device here and the device notifier tells me about it.  (It does not mount it) if i click it it just works (opens in dolphin mounted) to unmount it I can click the device notifier and tell it to by clicking an \"eject like\" icon. My wife as no idea what mount is yet she operates this devices without any problem. So really I have no idea what his problem his maybe it was my distro that this for me dunno. but maybe he should be asking his distro to do that for him since mine did it for me.\r\nBut maybe I'm stupid and I don't get the problem.  "
    author: "pinheiro"
  - subject: "fonts"
    date: 2009-08-06
    body: "Making a free font is something that takes huge huge amount of work and truly expert skis, I use liberation sans from the same company that made the segoe UI fonts and the android fonts, and full hinting one, (the ones you can see in the first screnshot) now depending on what you have in your windows and your personal preferences there are things we can do in Linux and things we can't (patent issues). Personaly I really like liberations sans.   "
    author: "pinheiro"
  - subject: "(Auto)mount"
    date: 2009-08-06
    body: "What you describe is just the normal device notifier plasmoid. The alleged one from kde-look.org does nothing else than mount the inserted device without having you to click (and it does show multiple option, if there are, within the notification window, not in a seperate pop-up).\r\nThis might be useful if you have an application which isn't able to mount something (eg. if you want to save a website from within a browser on a usb device without opening (and closing) Dolphin first), but this is certainly not a showstopper."
    author: "Kirilo"
  - subject: "The Answer"
    date: 2009-08-06
    body: "Thank you for all the fish :P"
    author: "pinheiro"
  - subject: "targets"
    date: 2009-08-06
    body: "> http://img205.imageshack.us/i/mediadevices.png/\r\n\r\nright, so the labels on you disk are really poor. if you name every file on disk with a similarly bad choice of label, you'll get the same kind of problems for files in the fs too.\r\n\r\n> So, hide technical details from them because KDE users are stupid? ;)\r\n\r\nno, not because people are stupid but because it should not be necessary to know what a C++ pointer is to use a C++ application, to use an absurd but accurate analogy. similarly, it shouldn't be necessary to know what /dev/sda1 is to use a drive. if you need to know that information _from the device notifier_ then something is wrong.\r\n\r\nnow, if you want to know device names, use the hard disk component from the system information widget. that is designed for technical information display. different purpose, different target audiences, different information and display. aka \"design\".\r\n\r\nwith the design philosophy you are trying to espouse, we'd have no tools that are approachable for average users and a bunch of really gnarly to use bits that few people would want to.\r\n\r\n> Konq is dolphin in KDE4. No difference between them.\r\n\r\nboth statements are incorrect, btw.\r\n\r\n> I use krusader or konsole.\r\n\r\nso set up krusader as your default file manager. or go into system settings and add/modify a device action. \r\n\r\nif open up krunner (alt+f2) and type \"device\", that control panel is actually the first result i see, and i can quite easily add a manual mount command there.\r\n\r\n(btw, i just noticed a bug in that panel: it doesn't let me modify actions that are installed system-wide; i've reported it to the maintainer)\r\n\r\n> you are suggesting me to stop using MATLAB because it doesn't allow \r\n> me to mount media?!\r\n\r\ni'm suggesting that matlab, and other apps like it, have some very serious and fundamental shortcomings. i have no interest in hobbling our interfaces for other people not saddled with such applications. matlab could be (should be!) using one of the great toolkits out there that handle all this for them.\r\n\r\n> And that desktop shell doesn't have some essential features like \r\n> manual media mounting. \r\n\r\nwell, no. you didn't add such an action to the Device Actions configuration, and we don't have such an option by default because it's not useful to most people.\r\n\r\n> Right click on a device seemed obvious for me to\r\n> get similar menu now.\r\n\r\nso you trained yourself using a tool which you found useful but which either didn't spend much time thinking about usability or had a different target audience, and now you have some habits that don't align nicely with the rest of our user base.\r\n\r\nthe device notifier widget uses standard left-click driven interaction. that shouldn't be surprising.\r\n\r\n> But... innovation.\r\n\r\nplease stop the ridiculous trolling / flamebait comments. i don't need/want to deal with sarcasm at the end of every one of your comments. it's unnecessarily frustrating when all i'm trying to do is have a conversation with you. or is this how you talk to people in person?"
    author: "Aaron Seigo"
  - subject: "..."
    date: 2009-08-06
    body: "OK, I will repeat everything for people that don't want to read.\r\n\r\n> Really don't get his problem, ok so i insert a device here and the device\r\n> notifier tells me about it. (It does not mount it) if i click it it just works \r\n> (opens in dolphin mounted)\r\n\r\nMy devices are inserted before starting KDE4. If I click device notifier, I get:\r\n\r\nhttp://img205.imageshack.us/i/mediadevices.png/\r\n\r\nso I don't know where to click because there are no difference between two disks shown. I'm not laying. Please click on the link.\r\n\r\nMounting fails because there is no dolphin here. I don't use it, so I don't have it installed. I don't want this device to be open by file manager at all. I just want to save some files from an app that is already open. If dolphin starts I would close it immediately. Requiring huge application to be installed and run every time just to mount device is beyond stupidity.\r\n"
    author: "vedranf2"
  - subject: "everybody?"
    date: 2009-08-06
    body: "> Everyone knows that featurewise, kde4 is behind kde3.\r\n\r\nthere are features in kde3 that aren't in kde4.\r\n\r\nsome of those features never will be in kde4 because they are poor features.\r\n\r\nsome of those features will be in kde4 at some point because they are good features but nobody has gotten yet to impleenting them.\r\n\r\nthere are features in kde4 that are not in kde3. \r\n\r\nwith near certainty none of those features will ever appear in kde3, because nobody is working on it further.\r\n\r\nat this point, the number of useful features in kde4 that are not in kde3 outnumber by quite a margin the useful features in kde3 that are not in kde4."
    author: "Aaron Seigo"
  - subject: "system settings"
    date: 2009-08-06
    body: "> Can you show me how to do that in KDE4?\r\n\r\n0. go into the System Settings.\r\n1. go to the Advanced Tab\r\n2. open Device Actions\r\n3. add your desired action\r\n\r\nor:\r\n\r\n0. Alt+f2\r\n1. type \"device action\" then enter\r\n2. add your desired action\r\n\r\n(personally, i find that dialog a bit to complex .. it could have the same features with a nicer UI)\r\n\r\n> Dictator said it's too complicated!\r\n> I guess they do usability tests with monkeys?\r\n\r\nagain with the insults. *shakes head*"
    author: "Aaron Seigo"
  - subject: "or.."
    date: 2009-08-06
    body: "> Requiring huge application to be installed and run every time just to \r\n> mount device is beyond stupidity.\r\n\r\n* dolphin isn't a huge application\r\n* yes, we expect you to have a file manager for integration purposes\r\n* you can modify the settings in system settings to not use a file manager for mounting\r\n* it's only 'stupidity' for you, but it's actually the workflow most people need; they don't tend to open a disk \"just because\", they want to work with it. breaking it into a series of steps where the first one,  \"mount it\", is a technical detail that gets them (from a user POV) no closer to their goal is non-optimal"
    author: "Aaron Seigo"
  - subject: "..."
    date: 2009-08-06
    body: "> right, so the labels on you disk are really poor. \r\n\r\nYes, I haven't label it. I've only 2 such disk with different sizes so I never needed labels. KDE3 show me sizes and I know which is which:\r\n\r\nhttp://img44.imageshack.us/i/kde3mount.png/\r\n\r\n> similarly, it shouldn't be necessary to know what /dev/sda1 is to use a drive.\r\n\r\nI didn't say that. Just put it there (it's small, 9 letters) as info. People who knew what that is will appreciate it. Those who don't will ignore it. Some of them will maybe look it up in google and start learning. Imagine that, LEARNING. Maybe even become KDE devs in the future.\r\n\r\n> now, if you want to know device names, use the hard disk component from \r\n\r\nYeah, but I don't want to go there every time I want to mount a device.\r\n\r\n>> Konq is dolphin in KDE4. No difference between them.\r\n> both statements are incorrect, btw.\r\n\r\nKonq. uses dolphin Kpart. That's end of the story.\r\n\r\n> so set up krusader as your default file manager. or go into system settings \r\n> and add/modify a device action. \r\n\r\nOK, but I don't want file manager at all to mount device. Those two must be separated. Provide \"open in file manager\" and \"mount\". Those who don't know what is \"mount\" will use \"open\". Others will appreciate \"mount\". Why is this so hard to understand? Why assume that every KDE4 user is totally ignorant?\r\n\r\n> i'm suggesting that matlab, and other apps like it, have some very serious \r\n> and fundamental shortcomings. i have no interest in hobbling our \r\n> interfaces for other people not saddled with such applications\r\n\r\nThere are hundreds of such applications. Especially those for people that do something with their computers (scientists, engineers, audio/video processing people, 3D graphics people,...). So, basically what you are saying is that KDE4 is not designed for those productive people, but for others, like children. That's good to know.\r\n\r\n> so you trained yourself using a tool which you found useful but which either \r\n> didn't spend much time thinking about usability or had a different target \r\n\r\nYes, I didn't spend much time thinking about its usability because it was usable. Now, the situation is different and what I have now is not usable at all and I think all the time about usability, unfortunately.\r\n\r\n> or is this how you talk to people in person\r\n\r\nI rarely encounter people just don't want to understand.\r\n"
    author: "vedranf2"
  - subject: "I don't think this is"
    date: 2009-08-06
    body: "I don't think this is entirely true. An easy and quick way to just mount a device is something I would probably use too.\r\n\r\nRight now I'm using Kubuntu Jaunty with KDE 3.5. I haven't used 4.x much yet, so I don't know exactly how it works there.\r\n\r\nAlthough I do think that from KDE's PoV KDE development should mainly focus on usability applications that are actually built on KDE libraries and provide all the good KDE integration, I myself do have to use apps regularly that do not fit in.\r\n\r\nRight now when I put a CD in my drive or connect an external disk, I get a pop-up which asks me what I want to do. (Open in filemanager, listen to the songs, etc.) In this pop-up, I would probably click the \"Just mount\" option most of the time if it would be there. Right now I click the \"Open in filemanager\" option and then just close the filemanager. Other times I cancel the dialog and use the right-click & mount option on the desktop icon.\r\n\r\nApplications that I use, but do not include KDE/mount integration are Mathematica, Opera, Java based apps, such as Maple, Dosbox and games that need a cd to play (both Linux and Wine based games).\r\n\r\nSo for me a \"Just mount\" option would be quite handy. Letting KDE automatically mount anything as soon as it's connected may be even better. Although I do not always need that either. When I play a game in ePSXe for example, the game cd doesn't have to be mounted.\r\n\r\nI agree that the existing workflow of immediate access to a filesystem from a filemanager is something that should not be changed. Adding an additional option to just mount a device after it's inserted could also be handy though."
    author: "Z_God"
  - subject: "You may have a point..."
    date: 2009-08-06
    body: "... that \"Volume\" as a label is not all that informative if no label has been set, IMHO. It would be good to make that a bit more informative, if possible. Adding the size could help, just like things like manufacturer etc. if available. \r\n\r\nOtherwise, I am amazed that you get much response at all. The way you are \"discussing\" by constantly trying to bash the people who are working hard to build a system you can use for free is not all that conductive to a productive exchange of ideas. It takes away from any valid point you may have, I think."
    author: "andresomers"
  - subject: "..."
    date: 2009-08-06
    body: "> Otherwise, I am amazed that you get much response at all. The way you are \r\n> \"discussing\" by constantly trying to bash the people who are working hard to \r\n\r\nI started commenting and reporting bugs about KDE4 even before 4.0 was released. Most of them were closed as wontfix of ignored just because they didn't fit in their vision. After I saw that all the valid complaints (not only mine) are getting completely ignored I started to be sarcastic because I know nothing will be changed.\r\n"
    author: "vedranf2"
  - subject: "..."
    date: 2009-08-06
    body: "> I don't think this is entirely true. An easy and quick way to just mount a \r\n> device is something I would probably use too.\r\n\r\nI'm crazy and you do not exist. ;)\r\n\r\n> Adding an additional option to just mount a device after it's inserted could \r\n> also be handy though\r\n\r\nThis is KDE4. Adding an option is not an option.\r\n"
    author: "vedranf2"
  - subject: "I'm a scientist"
    date: 2009-08-06
    body: "I use KDE daily for my work either directly (with applications like rkward) or as a shell for other programs, and I see no hindrance: KDE4 is perfectly usable for me.\r\n\r\nBut the more you go on, the more I think you're just trolling."
    author: "einar"
  - subject: "..."
    date: 2009-08-06
    body: "> dolphin isn't a huge application\r\n\r\nCompared to standard unix mount(8) that I have usually used to mount, it is huge and slow.\r\n\r\n> yes, we expect you to have a file manager for integration purposes\r\n\r\nOK, but do you also expect it for mounting purposes?\r\n\r\n> you can modify the settings in system settings to not use a file manager \r\n> for mounting\r\n\r\nI see now. But adding new action is all but user friendly. What to put in command field? Should I use mount or pmount? What will be the mountpoint? Also, this opens unnecessary dialog slowing the whole process. It's better to have a small menu, like in that KDE3 applet.\r\n\r\n> it's only 'stupidity' for you, but it's actually the workflow most people need; \r\n> they don't tend to open a disk \"just because\", they want to work with it\r\n\r\nWell, I'm an engineer. My friends are engineers, scientists, sysadmins, etc. This is stupidity for all of us, and our workflow is ... well I already told you. We can only conclude that KDE4 is not designed for us (but could be for both types of users) and we'll have to stay on KDE3 and eventually move on to something better.\r\n"
    author: "vedranf2"
  - subject: "As it has already been posted above..."
    date: 2009-08-06
    body: "http://www.kde-look.org/content/show.php/Device+Manager?content=106051\r\n\r\nThis plasmoid is your  friend. :-)"
    author: "Kirilo"
  - subject: "> But the more you go on, the"
    date: 2009-08-06
    body: "> But the more you go on, the more I think you're just trolling.\r\n\r\nI constantly repeat the same thing. So if I was trolling, I was trolling from the start. Also, more then one person confirmed my experience, and btw., some people showed me plasmoids designed to fix obvious KDE defects I'm talking about:\r\n\r\nhttp://www.kde-look.org/content/show.php/Device+Manager?content=106051\r\nhttp://www.kde-look.org/content/show.php/New+Device+Notifier+with+Automount?content=91517\r\n\r\nSo I'm not imagining things, problem exists, multiple bugs are reported. multiple software fixes for the are available and are extremely popular (90% good). Story ends.\r\n\r\n"
    author: "vedranf2"
  - subject: "Yes, that looks nice. I'm"
    date: 2009-08-06
    body: "Yes, that looks nice. I'm just not fine with having to find and install a lot of additional software to get the basic functionality.\r\n\r\nAlso, it's rated 90% good. So when Aaron says that majority of users like the default workflow (mount with dolphin), he is just talking, excuse my language, bullshit.\r\n\r\n"
    author: "vedranf2"
  - subject: "A better way to act"
    date: 2009-08-06
    body: "Well, why don't you act productively and ask if those people are willing to merge their work into KDE?\r\nYou know, developers aren't omniscient."
    author: "einar"
  - subject: "Aaaaargh!"
    date: 2009-08-06
    body: "Please follow the whole discussion. Exactly THIS is the main problem! Lead developer is satisfied with current situation, thinks that this (mount with dolphin) is workflow for the majority of userbase, doesn't want to change a thing and it's impossible to persuade him to change his mind. This is why I started giving sarcastic comments.\r\n"
    author: "vedranf2"
  - subject: "dolphin, right-click and constructive criticism"
    date: 2009-08-06
    body: "> Konq. uses dolphin Kpart. That's end of the story.\r\n\r\nDolphin KPart evolved from Konqueror. That's the story. \r\n\r\nDolphin uses khtml for showing the filesystem, which is part of Konqueror. \r\n\r\nMore exactly: Dolphin is a UI which uses the Konqueror backend. That's why it was possible for one person to start it as a great new tool. \r\n\r\nNow it got far enough that's it's the default filemanager in KDE4. \r\n\r\nThere's one thing where I agree, though: I expect right-click to show me options and left click to execute the default option. Even current MacOSX programs use right-click (or \"control-click\", which is just another name for \"I only have one button, but I need button two\"). \r\n\r\nBesides: If you don't like getting bad reactions from all sides and don't want to stop people from actually implementing your suggestions (even when they come from others later on), you should start to critizice constructively, and the first step towards that is to write nice and friendly messages. \r\n\r\nAttacking people doesn't serve your cause. It just makes them harden in their stance, not to follow your way, even though they would otherwise begin to think different later on. In German: \"Du verh\u00e4rtest ihre Meinung gegen deine Idee\". "
    author: "ArneBab"
  - subject: "I see now. But adding new"
    date: 2009-08-06
    body: "<i>I see now. But adding new action is all but user friendly. What to put in command field? Should I use mount or pmount? What will be the mountpoint?\r\n</i>\r\n\r\nOn my system, both mount and pmount work for me, but my user is member of the right groups so pmount is probably the better option.\r\n\r\npmount %d %f \r\n\r\nshould do the trick for the command line, furthermore, I have \r\n\r\n\"The device must be of the type StorageVolume\"\r\n\"The devices property StorageVolume.ignored must equal false\"\r\n\r\nfor the parameters, which seems to be the (rather liberal) default.\r\nThe mountpoint is the one suggested by the system (hal/solid?) e.g. usually something along the lines of /media/label or /media/disk etc. .\r\n\r\nSide remark: \r\n\r\nMy personal workflow also benefits from a \"mount without opening filemanager\" option, because I often start applications from the command line + one file from a removable media (e.g. something like \r\n\r\n1.) mount usb pen drive\r\n2.) okteta /media/disk/largedatafile.bin &\r\n\r\n). I would suggest submitting the inclusion of such a minimal mounting option to brainstorm.forum.kde.org. There is already an idea for a popup - less modification / version of the device notifier, which probably meets your requirements of being less cumbersome to use.\r\n\r\nFinally, I would like to point out the obvious: If you enter a discussion with a certain amount of cynism and sarcasm because you are certain not to be successful with your request, you are, in fact, heading full speed into \"self-fulfilling prophecy land\".\r\n\r\nHth\r\nMartin \r\n\r\nEDIT: Wording"
    author: "setecastronomy"
  - subject: "..."
    date: 2009-08-07
    body: "> Dolphin KPart evolved from Konqueror. That's the story. \r\n\r\nDoesn't matter. Important thing is today konqueror uses dolphin kpart and doesn't have any feature that isn't already present in dolphin.\r\n\r\n> Besides: If you don't like getting bad reactions from all sides and don't \r\n> want to stop people from actually implementing your suggestions (even \r\n> when they come from others later on), you should start to critizice \r\n> constructively, and the first step towards that is to write nice and friendly \r\n> messages.\r\n\r\nSince 4.0 I wrote nice and friendly messages. Response from devs was always one of the:\r\n\r\na) it will introduce unnecessary complexities in the code\r\nb) it will create more clutter\r\nc) it will confuse users\r\nd) it will break our design/vision\r\n \r\nand usually fanboys would say that current situation is best possible. Now I just tend to be sarcastic and don't expect anything.\r\n\r\n\r\n"
    author: "vedranf2"
  - subject: "Well..."
    date: 2009-08-07
    body: "Nice post. You seem to understand everything I said and I agree with everything you said.\r\n\r\nCheers,\r\n\r\nVedran\r\n"
    author: "vedranf2"
  - subject: "why aargh?"
    date: 2009-08-07
    body: "The fact Aaron is satisfied with the current situation doesn't mean that the device notifier will never change into something that fits your needs more.\r\nIt isn't as if Aaron has to give his green light to every change and/or new feature that gets added into kde.\r\n\r\nAlso: i wonder why you don't want to automount external drives.\r\non my opensuse system, whenever i plugin a drive, opensuse mounts it in a subdirectory in /media.\r\nLike aaron stated, any kde application will get the drive visibl in the open file dialog, while non kde applications that doen't use the open file dialog of kde, nor one with similar features, can access the drive via /media/\r\n\r\nYou stated that it isn't Matlab's job to mount the drive you want to use. But is other applications, like k3b, kword, openoffice, gwenview, etc are capable doing so, why can't matlab?"
    author: "whatever noticed"
  - subject: "Yes, everybody :o)"
    date: 2009-08-07
    body: "But that isn't exactly what i meant.\r\nI think, featurewise, if you would count every featere in kde 3.5 and 4.3, the latter probably has more than the former.\r\n\r\nBut for users, it is the features they were used to using that counts. And untill kde 4.3, any kde user that i have met knew some features that they used in kde 3.5, which weren't available in kde4. I think kde4.3 will be the first release that makes users forget about 3.5\r\n"
    author: "whatever noticed"
  - subject: "so you want to mount without dolphin opening"
    date: 2009-08-07
    body: "so you want to mount without dolphin opening\r\n\r\nAaron told you how to do that with the current device notfier.\r\nSimply go to systemsetting and change the behavior of the device notifier when you click on a drive in it.\r\n\r\nWhy don't you follow his advise?"
    author: "whatever noticed"
  - subject: "On opensuse, i usually use"
    date: 2009-08-07
    body: "On opensuse, i usually use 'cancel' in the dialog.\r\nThe device gets mounted anyway. \r\n"
    author: "whatever noticed"
  - subject: "\"OK, but do you also expect"
    date: 2009-08-07
    body: "\"OK, but do you also expect it for mounting purposes?\"\r\n\r\nThe mounting itself isn't done by dolphin.\r\n\r\nI wonder why your system doesn't use automount, and why you don't want to mount drives automaticly when they are detected by the system."
    author: "whatever noticed"
  - subject: "Not excused"
    date: 2009-08-07
    body: ">So when Aaron says that majority of users like the default workflow (mount with dolphin), he is just talking, excuse my language, bullshit.\r\n\r\nNo, your language is not excused anymore. Start saying what you have to say in a constructive way, or shut up please. By the time you have become one of the most constructive and productive KDE hackers, we will allow you to talk \"bullshit\" too."
    author: "andresomers"
  - subject: "Doh!"
    date: 2009-08-07
    body: "And if he even bother too look, it has been downloaded a pitfull of 1124 times. Compared to the 7 digit of KDE users out there, Aaron is totally right saying that the majority of users like the default workflow. So yes, the vedranf caracter is talking bullshit.\r\n"
    author: "Morty"
  - subject: "Well, I have gotten rather"
    date: 2009-08-07
    body: "Well, I have gotten rather partial to the liberation fonts and use those for everything. I guess it is a matter of personal taste but I do think they look better than the ones you mentioned."
    author: "stumbles"
  - subject: "Holly crap! Has anyone gone"
    date: 2009-08-07
    body: "Holly crap! Has anyone gone into System Settings > Paths, and tried changing the Downloads path from /home/someuser to /where/I/want/downloads/to/go ? It moved my whole frickin Home directory. I forget exactly what the warning said before I clicked ok, non the less I was not expecting that to happen. That's just wrong."
    author: "stumbles"
  - subject: "can only say THANKS"
    date: 2009-08-07
    body: "to the whole KDE team: I can only say thanks for this impressive release.\r\n\r\nWhat I find so impressive about this release is that the team has confirmed what their *vision* was. When they started with KDE4.0, arguably a release, they took an incredibly amount of heat from their most loyal people.\r\n\r\nDespite this, the team was pragmatic and kept going towards what their goal. And now, with this release, they can loudly say \"here people, this is what we meant\". I still remember Aaron politely answering to flaming comments about the first plasma version. I find  him an example of social skills, how he handled that and kept going with the project. Some people would have given up. Instead, he (and others) accepted constructive criticism, let the trolling slip through, and get going forward.\r\n\r\nThe release is terrific. The amount of work is tremendous. I want to thank *everyone* who contributed to this, regardless of whether that was the whole Kwin or a single patch.\r\n\r\nYou guys have all my respect not only for the results that we see now, but also for what you had to go through since the KDE4.0 release.\r\n\r\nThanks!"
    author: "batistuta"
  - subject: "oops :o)"
    date: 2009-08-07
    body: "Well, next time you will read the message boxes :o)\r\n\r\nBut you are right, the module should be aware that some directories just shouldn't be moved.\r\n\r\nI could not find the module you are talking about. Can you give more details?\r\n\r\nAlso, consider filing a bug report. \r\n"
    author: "whatever noticed"
  - subject: "Did not work for me"
    date: 2009-08-07
    body: "The fact that the current behaviour has been reported as bug or wish 12 times shows that there is a large number of people who think the current behaviour could be improved.\r\n\r\nI am one of them - I love KDE-4.3 and would like to see it further improved. The current behaviour does not make KDE-4.3 unusable, but it is a nuisance.\r\n\r\nAlso I tried to install the new notifier and it never worked for me (did not try very hard, though)"
    author: "moritzmh"
  - subject: "The new comments system is actually working"
    date: 2009-08-08
    body: "Hi there,\r\n\r\nI hereby retract my statements that the comments system is not working. I think the amount of comments would be higher without it, but we are building up and big releases like this one are going to contribute to it.\r\n\r\nSo not all is lost after all. Some releases more and it will be like it was.\r\n\r\nYours,\r\nKay"
    author: "kayonuk"
  - subject: "Thanks. This looks very nice"
    date: 2009-08-08
    body: "Thanks. This looks very nice indeed. I hope it will be listed in the Plasmoid list if it is not installed by default :)"
    author: "Z_God"
  - subject: "Re: ..."
    date: 2009-08-08
    body: "99% of us are fine with the current situation because, even if we do only need to save a file on that disk, we can use the places view from the application's file dialog. The real bug is that file dialogs are not shared between GTK and Qt, so GTK applications in a KDE environment can not use the places view, and vice versa.\r\n\r\nOf course, we could add a \"Simply mount\" option to the device notifier in no time if we wanted. But for KDE 4, we are not just adding options anywhere, but we think about how functionality can be added without adding bloat to the interface."
    author: "majewsky"
  - subject: "\"Since 4.0 I wrote nice and"
    date: 2009-08-08
    body: "\"Since 4.0 I wrote nice and friendly messages. Response from devs was always one of the: a) it will introduce unnecessary complexities in the code b) it will create more clutter c) it will confuse users d) it will break our design/vision and usually fanboys would say that current situation is best possible. Now I just tend to be sarcastic and don't expect anything.\"\r\n\r\na), b) and c) are totally true in this case. See what I (and others) explained earlier, a \"Simply mount\" option is not necessary (though you can still create this option on your desktop, as Aaron pointed out).\r\n\r\nd) is the hard reality of software. Whenever you try to make a product that pleases anybody, in the end you'll please nobody. A good product consists of clear design decisions.\r\n\r\nA nice example is programming languages: There is a saying that the only good programming languages have been designed by one or at most two people. Examples are C and C++: While many programmers hate these languages, they have been around for 40 years (or 30 years, respectively) which is ages in computer science, and still are used excessively."
    author: "majewsky"
  - subject: "\"(eg. if you want to save a"
    date: 2009-08-08
    body: "\"(eg. if you want to save a website from within a browser on a usb device without opening (and closing) Dolphin first)\"\r\n\r\nThis again shows the real problem underneath the bashing here. There would have been exactly zero discussion on silly \"simply mount\" options in device notifier if every widget toolkit would just use the KDE file dialog if run from KDE. This kind of integration is _not_ rocket science."
    author: "majewsky"
  - subject: "\"1.) mount usb pen drive\n2.)"
    date: 2009-08-08
    body: "\"1.) mount usb pen drive\r\n2.) okteta /media/disk/largedatafile.bin &\"\r\n\r\nIsn't it easier to open Okteta and navigate to the largedatafile.bin through the places pane in the file dialog, which mounts the pen drive if it is not mounted yet?"
    author: "majewsky"
  - subject: "Re: Fonts"
    date: 2009-08-08
    body: "\"But what's still very ugly are the fonts. Compared to Vista's/Win7's Segoe or OSX's Lucida Grande with their optimization for the font rendering of their OS it looks very dated.\"\r\n\r\nAs hard as it may sound: This is not KDE's problem. All we do is to provide an interface to select fonts.\r\n\r\nIf the default fonts bother you, ask the distributors.\r\n\r\nIf the font rendering bothers you, ask the Qt developers.\r\n\r\nYou have a valid point, though: We should pay attention to the fonts when creating official screenshots and stuff."
    author: "majewsky"
  - subject: "Re: The new comments system is actually working"
    date: 2009-08-08
    body: "\"I hereby retract my statements that the comments system is not working. I think the amount of comments would be higher without it, but we are building up and big releases like this one are going to contribute to it.\"\r\n\r\n+1\r\n\r\nBTW I think that the real reason for comment numbers going down is the missing commit digests."
    author: "majewsky"
  - subject: "Hmmm"
    date: 2009-08-08
    body: "There is only one thing to say about this release.\r\n\r\nThe new \"Air\" Theme is damm awesome and sexy =)\r\n\r\nHope that KOffice will be ready to use as soon as possible."
    author: "Vamp898"
  - subject: "AFAIK the System Settings are"
    date: 2009-08-08
    body: "AFAIK the System Settings are provided by kdebase-workspace if that is the module your talking about. "
    author: "stumbles"
  - subject: "No, i meant which"
    date: 2009-08-08
    body: "No, i meant which module/section in systemsettings allows one to change the documents directory.\r\n\r\nI couldn't find it."
    author: "whatever noticed"
  - subject: "Utter BS - KDE 4.3.0 is far"
    date: 2009-08-09
    body: "Utter BS - KDE 4.3.0 is far away from being anywhere near a desktop users can set up to their desires, and forcing your definition of what users desire on everyone else is _exactly_ what is going on with KDE4."
    author: "kolla"
  - subject: "Maybe if you read the"
    date: 2009-08-09
    body: "Maybe if you read the original complaint once more, you will realise that this was not the problem... sheesh."
    author: "kolla"
  - subject: "Anyone who have complaints"
    date: 2009-08-09
    body: "Anyone who have complaints about KDE4 is a troll here, appearantly."
    author: "kolla"
  - subject: "So what do _you_ do if you"
    date: 2009-08-09
    body: "So what do _you_ do if you want to avoid the automounting to take place? For exmple in the case of a bad disk you want to run read-only tests on, or some other fragile media you want full control over?"
    author: "kolla"
  - subject: "Yeah"
    date: 2009-08-09
    body: "Yeah, if I was a monkey, I'd be pissed off now."
    author: "kolla"
  - subject: "Damn those so called most users!"
    date: 2009-08-09
    body: "\" it's only 'stupidity' for you, but it's actually the workflow most people need\"\r\n\r\nUtter BS again. You have absolutely _no_ properly done user tests to back up that claim with. Yes, it is so easy to put the blame of all kinds of stupid design decisions on the so called \"most users\", but no more I tell you, no more. It's like Fox News' \"some people say\", it's just a wild assumption. Do \"most users\" really care one way or the other? The problems become evident when people who _do_ care speak up, right? \r\n\r\nSeriously, Aaron, your attitudes alone are now driving me away from KDE, even KDE3 now has a foul taste to it - I feel bad for having supported a desktop so much led by what I now understand is a leader of the marching morons. Shame."
    author: "kolla"
  - subject: "\"Isn't it easier to open"
    date: 2009-08-09
    body: "\"Isn't it easier to open Okteta and navigate to largedatafile.bin trough the places pane in the file dialog, which mounts the pen drive if it is not mounted yet?\"\r\n\r\nNot for me :-)\r\n\r\nI'm fully aware that I'm not a statistically representative part of the KDE audience, e.g. I have a strange workflow, established in the course of over a decade on working with *nix systems, that works perfectly for me but seems to be overly complex or awkward for anybody else.\r\n\r\nThis is not to say that I don't change my workflow if new features arise that I find useful, as I'm for example an enthusiastic user of the krunner calculator or the [ctrl]-[i] filter option in dolphin. It's just that sometimes old patterns are so hard to overcome that I don't even try to do it any more, at least not consciously (e.g. launching applications with documents as file parameters from krunner / command line instead of using the open file dialog and the places side menu, for example).\r\n\r\nTherefore, I appreciate that the KDE devs have provided tools that even odd balls like me can have their little pet features (e.g. the ability to have a \"mount only\" option). Furthermore, it seems clear that for those Operating systems out there that do automatic mounts seamlessly in the background - which seems to be nowadays the major \"user friendly\" distros - having such an option by default is neither intuitive nor addresses an especially pressing need. Distros that default to non-automounting behaviour (like the ones I choose, oddly enough) can provide such an option for their traget audience and once the kiosk tools are back in full swing (which may be already the case, since I had the last look at them at the time around the 4.2.0 release), it should also be possible to maintain such options for larger installation bases (say a whole department of odd ball engineers or scientists).\r\n\r\nRegards"
    author: "setecastronomy"
  - subject: "Re: No, i meant which"
    date: 2009-08-09
    body: "It's under \"Personal information\". If you can't find that, either, invoke \"kcmshell4 desktoppath\" from your console."
    author: "majewsky"
  - subject: "nope"
    date: 2009-08-10
    body: "with comments like \" I know that version 4.3 means it's still \"alpha quality\"\"\r\n\r\nyou are indeed trolling.\r\n"
    author: "whatever noticed"
  - subject: "i don't do such operations in"
    date: 2009-08-10
    body: "i don't do such operations in a full blown desktop environment.\r\n"
    author: "whatever noticed"
  - subject: "kde is opensource.\nif it"
    date: 2009-08-10
    body: "kde is opensource.\r\nif it doesn't behave to your pleasing, you can change its behavior or search for someone who will make the changes for you.\r\n\r\nIn this discussion, aaron has pointed out that you can manually mount devices using the device notifier; in the sidebar of dolphin, and in the side bar of the file::open dialog of kde. You can also change the behavior of the device notifier to make it not opening a filemanager when you click on a device you want to mount.\r\nIf you want to  distinguish different drives by givint them a unique drive name.\r\n\r\nIf all that isn't enough for you, please go ahead and change the software to your pleasings or find someone who can do it for you.\r\n"
    author: "whatever noticed"
  - subject: "Thanks!"
    date: 2009-08-10
    body: "Thanks, i found it!!"
    author: "whatever noticed"
  - subject: "Why not?"
    date: 2009-08-11
    body: "Do you suggest that full blown desktop environments are unfit for such... \"advanced\" tasks?"
    author: "kolla"
  - subject: "Indeed.\nFixing damaged drives"
    date: 2009-08-11
    body: "Indeed.\r\nFixing damaged drives isn't something your average user does, so not something a full blown desktop computer takes into account.\r\nSo while you find it usefull to add several options to the device manager of kde that will deal with carefully handling broken devices. i do not. \r\n\r\nFixing such devices in Linux often requires the use of command line applications and such. So why not startup a system that is dedicated to such tasks, in stead of ripping out automount of a desktop system, to suit people that sometimes have the need to plugin broken devices that shouldn't be mounted.\r\n"
    author: "whatever noticed"
  - subject: "Fully blown..."
    date: 2009-08-16
    body: "Fully blown in your face, I'd say.\r\n\r\nYour answer nicely illustrates why I struggle to see KDE4 as anything but a toy system whos developers have no other vision than to atract morons. It just shows how immature the entire \"desktop on linux\" is, instead of creating something that is actually _better_ for _everyone_, you just badly mimic what is worst on certain commercial systems. Why?\r\n\r\nAnyone got a tip for a desktop that is actually not aimed at morons?"
    author: "kolla"
  - subject: "nope.\nYou could try windows"
    date: 2009-08-17
    body: "nope.\r\nYou could try windows or mac os X, but both have automounting too, so you run into the same cave heat if you want to access damaged drives (which is apperently something you do on a dayly base, which makes me wonder why you don't take better care of your hardware).\r\n\r\nlinux on the other hand allows you to disable automount, and with the device manager of kde you can mount devices manually.\r\n\r\nBut that is something you prefer to forget, as you cannot use that in your trolling at this article.\r\n"
    author: "whatever noticed"
  - subject: "But it is."
    date: 2009-08-17
    body: "But it is.\r\n"
    author: "whatever noticed"
  - subject: "Why?"
    date: 2009-08-17
    body: "Why would I try windows or OSX? Cave heat? What on earth are you on about?\r\n\r\nThe \"broken devices\" are typically USB storage, and not even mine. These kinds of operations are part of my work, and the parent bonehead want me to have a dedicated machine for this particular task. I wonder what other machines I'd need round for various particular tasks in his view."
    author: "kolla"
  - subject: "Not by a longshot"
    date: 2009-08-17
    body: "As long as KDE4 cannot duplicate even the simplest things from my KDE3 desktop (hue on background image, panel widgets that stay put where I place them, horisontally scrolling rss feed reader in panel, ways to script desktop settings), there's no chance at all that I, as a user, will forget KDE3. The fact that KDE4 is picky about hardware, sometimes annoyingly slow, full of annoying misfeatures one cannot get rid of or turn off, still very unstable and ridden with bugs (too much to list up here)... and also even look bad (konsole session menues and more) despite all the annoying bling - makes KDE3 unforgetable in comparison."
    author: "kolla"
  - subject: "\"Why would I try window\"\nThey"
    date: 2009-08-17
    body: "\"Why would I try window\"\r\n\r\nThey have a 80% userbase, so they must be doing something right. Or are you stating that windows and macos users are morons because they are using a system dat automounts their devices?\r\n\r\nIf your job is repairing broken devices, i would expect that you are skilled enough to use a Linux system with manual mounting and aren't dependend on a default device notifier to do the job for you.\r\n\r\nBut i guess that skilled people are becoming rare in the ICT business, and that people nowadays completely rely on automated GUI software to do the job for them.\r\n"
    author: "whatever noticed"
  - subject: "Still sounds to me like you"
    date: 2009-08-17
    body: "Still sounds to me like you are using a crippled linux distribution...\r\n"
    author: "whatever noticed"
  - subject: "You're really good"
    date: 2009-08-17
    body: "You're really good at twisting things around, arent you - you're now arguing that I should use the exact opposite tools of what I want and need.\r\n\r\nNo, I'm not against automounting per se, and there are systems where automounting works beatifully - linux is not among them, with all its different variants of automounting. My job is not repairing broken devices, but every now and then I do get a USB stick, or a disk or whatever on my desk that people hope I can pull data from. Working from command line is what I'm used to, but that has more to do with the continuing lack of functional desktops than anything else. And it's not about GUI, it's about the DESKTOP.\r\n\r\nI would love to see an alternative KDE4 desktop, with more traditional focus than this \"let's revolutionate how people use computers\" that KDE4 team aimed for (ironically the result is akin to a bad windows copy, nothing revolutionary)"
    author: "kolla"
  - subject: "What different variants of"
    date: 2009-08-17
    body: "What different variants of automounting?\r\nAFAIK automounting is done by the linux kernel, together with HAL/DBUS, something that works on every modern full blown linux distribution. The days of automount and supermount are over. It works quite simple: plugin something mountable, linux notices it and mounts it, creating a directory in /media where you can access it. \r\nDesktops like KDE can pick up the device notification and offer you a gui that pops up on the desktop or in application (side bars in file managers and file::open/save dialogs), providing a convenient way to access the device. \r\n\r\nI sometimes also get a broken mass storage device to recover, but i don't do it using my desktop system,, but with a live cd, containing the right tools for it, including descriptions on how to proceed. \r\nApperently, that is something you aren't used to, which is most likely a legacy of your windows computing knowledge, which doesn't (legally) provide you the convenience of using cd versions of the OS, aimed at a specific task. \r\n\r\nAs for your Windows Rant: it is funny to see that people like you consider kde4 a windows copy, as things like plasma have nothing in common with windows. \r\n\r\nBut if you are looking for another desktop experience: try gnome, try xfce, try lxde. Try windows, Try macos X. Try Windowmaker..\r\n\r\nOr do you believe they are aimed at morons too?\r\n\r\nIf so, why don't you just start your own project, attrack other non morons and show the world how a desktop should be done.\r\n"
    author: "whatever noticed"
  - subject: "of course it is his view"
    date: 2009-08-17
    body: "He is one of the designers of the desktop, so what else would you expect?\r\n"
    author: "whatever noticed"
  - subject: "strong disaffirmation."
    date: 2009-08-18
    body: "<blockquote>right, so the labels on you disk are really poor. if you name every file on disk with a similarly bad choice of label, you'll get the same kind of problems for files in the fs too.\r\n<blockquote>So, hide technical details from them because KDE users are stupid? ;)</blockquote>\r\nno, not because people are stupid but because it should not be necessary to know what a C++ pointer is to use a C++ application, to use an absurd but accurate analogy.</blockquote>\r\nThat analogy sucks. 'C++ pointer' is to 'C++ application' not what 'device node' is to 'mount point icon'. Anyways.\r\n<blockquote>similarly, it shouldn't be necessary to know what /dev/sda1 is to use a drive.</blockquote>\r\nWell, most probably it should not be <em>necessary</em>, but clearly it is an important bit of information that should be provided if one has the desire.\r\n<blockquote>if you need to know that information _from the device notifier_ then something is wrong.</blockquote>\r\nOhh is it. The kernel detects a new device and adds it *somewhere* into the /dev tree. Clearly, getting that information via dmesg|tail is the only right way to tell where, right?\r\nBesides, if <em>you</em> start telling me which usage of computers is right and which is wrong, <strong>then<strong> something is wrong.\r\n<blockquote>now, if you want to know device names, use the hard disk component from the system information widget.</blockquote>\r\nOh yeah - that is a so much more convenient and straight to the point way of finding out about the freshly added device node... Nevermind i could not find anything to deliver that information when it is relevant, despite opening the full kinfocenter (which is architected for a different task: getting all information)-\r\n\r\n<blockquote>different purpose, different target audiences, different information and display. aka \"design\". with the design philosophy you are trying to espouse, we'd have no tools that are approachable for average users and a bunch of really gnarly to use bits that few people would want to.</blockquote>\r\nThis mentality (rip everything out that joe average user would not care about) is so what i despise most about KDE4 (although mostly contained in plasma and dolphin - which makes it no less frustrating). Clearly this is easier than creating a DE which provides everything applicable at ALL in a certain context in a way that makes sense, i.e. makes it accessible by quality self descriptive UIs and info tooltips, and gives an option to slim down the interface (i.e. hide stuff only advanced users might fully grok). BTW this last bit of ripping advanced stuff out really should be something the distributions should handle anyways...\r\nBut instead, it is done the other way round. Useful features are not exposed on the default UI because they are considered 'distracting' and presumably do not suffice the 80/20 rule. This results in many features never used by people who, in the end, would consider them very beneficial to their computer work.\r\n\r\n<blockquote><blockquote>And that desktop shell doesn't have some essential features like manual media mounting.</blockquote>\r\nwell, no. you didn't add such an action to the Device Actions configuration, and we don't have such an option by default because it's not useful to most people.</blockquote>\r\nSee above. So judging by this statement, KDE is not anymore aiming at the power users AT ALL because they are less than 20 percent. No wonder the abundant experience of positive surprise which accompanied the daily work with KDE3 has left the building.\r\n\r\n<blockquote><blockquote>Right click on a device seemed obvious for me to get similar menu now.</blockquote>\r\nso you trained yourself using a tool which you found useful but which either didn't spend much time thinking about usability or</blockquote>\r\nYeah, right clicking an icon to get offered a context menu with relevant actions is so far off here.\r\nWhat do you think how many people confronted with that stupid applet tried to do exactly that?\r\n<blockquote>had a different target audience,</blockquote>\r\n(people with more than one mouse button?)\r\n<blockquote>and now you have some habits that don't align nicely with the rest of our user base.</blockquote>\r\nThe rest of <em>Aaron Seigo's wishlist user base</em>, that is.\r\nBefore you appeared on the KDE project, never was there such a statement to be heard.\r\n\r\n<blockquote>the device notifier widget uses standard left-click driven interaction. that shouldn't be surprising.</blockquote>\r\nWell no, the lack of right click context sensitive actions is.\r\n\r\n<blockquote>i don't need/want to deal with sarcasm at the end of every one of your comments. it's unnecessarily frustrating when all i'm trying</blockquote>\r\nOh please, don't get all soppy here. Criticism is not out there for your amusement. Often times, it's not even unjustified.\r\nHow about fscking that 80/20 principle that crippled down GNOME so much and strive for a higher feature density <strong>*and*</strong> accessibility instead? Cutting down on UI items can always be done somewhere <strong>downstream</strong>."
    author: "eMPee584"
  - subject: "ACK."
    date: 2009-08-18
    body: "I see your problem, i understand your reasoning, i agree with your conclusion. KDE4 took a turn into a direction i never even feared it would. Power user's favorite pet has turned into something that looks like a mix of windows, osx and gnome on the surface. Why?\r\nBecause some people who fanatically promote the 80/20 rule mentality have risen to the top of the commit list because of their engagement. Well fine, Plasma and solid and everything are nice frameworks, and surely noone wants them to be ripped out again. But STILL, what was possible with kicker in 3.5 - not possible with plasma panels. The system notification thing comes nowhere close to the cool mockups that circulated before 4.0, and browsing a huge folder with dolphin takes several times as long as <strike>both old konqueror's kpart and</strike> windows explorer accessing it via virtualbox's shared folder feature.\r\nA cool kicker spinoff named slicker - although the website states\r\n<blockquote>... looking for Slicker - 'the kicker'-thingie? ... it has metamorphed into Plasma</blockquote> five years down the road, plasma still can't do <a href=\"http://www.slicker.org/setup/modules/gallery/fop/images/slicklarge.jpg\"><span style=\"font-size:2em;font-weight:bold\">it</span></a>.\r\nAnd adding a folderview applet to the desktop and rotating it - it's not even anti-aliased and looks ugly as some creature from planet fnord."
    author: "eMPee584"
  - subject: "You dont think I have tried?"
    date: 2009-08-21
    body: "I have tried the lot of desktops for linux, and the only one being close to comfortable was KDE3. \r\n\r\nThey have different aims, Gnome is aimed at the casual user, xfce is more of a CDE clone, lxde is lightweight win95 clone. Windowmaker is hardly a desktop, it's just a window manager inspired by NeXTStep/GNUStep. MacOSX is too rigid for me, costs money, only runs on certain hardware and is a bitch to maintain (I do have a machine with OSX on). Windows is just useless, only runs on certain hardware and lacks just about everything.\r\n\r\nThe idea that I should boot a live CD to fix disks is just utterly hilarious, there is absolutely no reason why I should not be able to do this from a desktop computer, especially one that is \"full blown\". You actually tell me I have to close down all the apps I have running, interrupt all the tasks I'm doing, just to mess around with \"maybe broken\" disk? I'm sorry, that's just insane. And Windows legacy - I dont have any windows legacy, I come from a different place.\r\n\r\nKDE4 is a windows clone in that it doesnt allow me to control my own workflow, it tries to dictate how I shall do things in stead of offering me options so I can decide for myself. And plasma har absolutely nothing to do with this, but plasma ads a heck lot of frustration due to it still being immature and unstable, but most of all because there are no alternatives. It does add alot of \"bling\" though, just like Vista.\r\n\r\nOfcourse mounting is done by the kernel, all HAL does is detect the device and letting the rest of the system know via dbus. What actually tells the kernel to do the mounting varies alot. IMO it is _perfectly_ reasonable that the DESKTOP running on the console tells the kernel to do the mounting, _AT USERS WILL_.\r\n\r\nAs for starting yet another desktop project - I think it makes a heck lot more sense to fix KD4, the question is whether it is fixable. Technically it is, easily. But socially? Any critics on Plasma for example is being ignored or ridiculed, plasma is presented as the manna of KDE4 (yet I have not seen it offer anything even slightly impressive) If someone wrote a new kicker for KDE4, not using plasma - would it even be accepted?"
    author: "kolla"
  - subject: "Sounds to me you are stupid"
    date: 2009-08-21
    body: "Care to point out one single thing in the list I mentioned that is distro related?\r\n\r\nFor the record, distros used are gentoo, ubuntu/kubuntu and the openSUSE live CD, and hardware used are multiple PCs, laptops, eeepc, eeebox and an old iBook. And the problems are the same no matter what distro or hardware used. The problems I describe are only related to KDE4."
    author: "kolla"
  - subject: "\". You actually tell me I"
    date: 2009-08-21
    body: "\". You actually tell me I have to close down all the apps I have running, interrupt all the tasks I'm doing, just to mess around with \"maybe broken\" disk?\"\r\n\r\nSounds to me that you aren't taking your task too seriously.\r\nWhich indicates to me that this whole story about you fixing disks is balloney..\r\n\r\n\r\n \"KDE4 is a windows clone in that it doesnt allow me to control my own workflow, it tries to dictate how I shall do things in stead of offering me options so I can decide for myself.\"\r\n\r\nHmm, you are talking shit on this too.\r\nYou claim to have no Windows experience, yet you seem to know how it works. \r\nAnd kde4 is a clone of windows because both don't work as you like them too?\r\nThat is just plane silly.\r\n\r\n\" What actually tells the kernel to do the mounting varies alot.\"\r\n\r\nAnd you cannot choose any variation that fits your needs?\r\n\r\n\" IMO it is _perfectly_ reasonable that the DESKTOP running on the console tells the kernel to do the mounting, _AT USERS WILL_.\"\r\n\r\nAnd that is what kde4 is capable of.\r\nBut the problem with users like vedranf and you is that they aren't capable of performing the manual mounting.\r\n\r\n\r\n\r\n\"I think it makes a heck lot more sense to fix KD4\"\r\n\r\nAnd what are you doing to fix kde4?\r\nRants like these are counter productive.\r\n\r\n\" If someone wrote a new kicker for KDE4, not using plasma - would it even be accepted?\"\r\n\r\nWhy do people always want to put everything into 1 software package?\r\nIf someone ports kicker to kde4, it is quite easy to install it as seperate package into kde4.\r\nIt is even that easy that vedranf doesn't even realize that his pet mounting applet in kde3 is a third party extension of kicker. \r\n\r\nAnd your macos/windowmaker/windows/xfce rant was rather funny.\r\nSo you dont want to pay for software, so windows can only run on 1 platform (but you don't complain about that with macos), and apperently, you aren't able to install additional software in windows either..\r\n"
    author: "whatever noticed"
  - subject: "Well, apperently it is user"
    date: 2009-08-21
    body: "Well, apperently it is user related.\r\n"
    author: "whatever noticed"
  - subject: "You are incredible"
    date: 2009-08-22
    body: "So, since I dont want to interrupt my other tasks and work to look at disks that might be broken, I'm not taking my tasks seriously - and because I said I dont have windows legacy, you think I'm saying I dont have any windows experience? Amazing logic. \r\n\r\nIt's not just that neither windows nor KDE4 dont do what I want, if you had been paying attention, you'd notice that I specified what they share that I dont like.\r\n\r\nAnd yes, I so far I have not managed to get KDE4 to do mounting the way I prefer it, and I am perfectly capable of doing manual mounting (but I suspect you wouldnt know how to mount partitions from devices that no longer have partition table intact, huh?) - but that's not the point here (for crying out loud.)\r\n\r\nI'm glad you enjoyed my rant. No, I dont I dont see the point of paying for cripled software when I can get that for free (that is sarcasm, in case you wonder), and I did point out that _both_ macos and windows only run on certain hardware. Maybe you should read more carefully? What makes you think I cnnot install software on Windows, and what the heck is the relevance anyhow?\r\n\r\nAnd lastly, the source code for the media applet for kicker that has been the topic for the last few dozens of messages in this very enlightening thread is not a third party applet, it is right there in the kdebase tarball, under kdebase-3.5.10/kicker/applets/media.\r\n\r\nHow long will you keep this going btw?"
    author: "kolla"
  - subject: "people have given several"
    date: 2009-08-23
    body: "people have given several options to mount devices in kde.\r\nWhy isn't there any option that suits your need?\r\n\r\nIt should be simple: the device pops up somewhere, you click on it if you want to open it, and it gets mounted in the process.\r\n\r\nAbout broken devices: they usually don't get mounted automaticly, so i don't see any reason why you want to disable automount.\r\n\r\nthe problem with linux is that while kde offers a nice way to access external drives when you want to open/close files, not every linux application is using it. This makes accessing external drives a pain for some people.\r\nBut in stead of contacting the developers of such applications to fix this situation, users turn to KDE to come with workarounds to address the short comings of third party applications.\r\n\r\nThe kicker applet for media devices that is being discussed over here isn't part of kde3 at all: it is a third party addon for kicker.\r\nA similar third party applet is available for plasma panel.\r\nIt makes me wonder why kde3 users have no problem installing that non kde applet for kicker to do the job, but demand kde4 to contain such an applet by default."
    author: "whatever noticed"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px">
<a href="http://kde.org/announcements/4.3/"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde430-inspired.png" width="540" height="226" /></a>
</div>
<a href="http://www.kde.org/announcements/4.3/">KDE 4.3.0</a> is out, and it is a great release. It is unlikely that any one specific thing will strike the user as the most noticeable improvement; rather, the overall user experience of KDE has improved greatly in KDE 4.3.0. The release's codename, Caizen, is a Japanese philosophy that focuses on continuous improvement throughout all aspects of life. That has been the goal of the KDE team for 4.3.0: polish, polish, polish. The statistics from <a href="http://bugs.kde.org">the bug tracker</a> speak for themselves: 10,000 bugs have been fixed. In addition, close to 63,000 changes were checked in by a little under 700 contributors. That is not to say that the KDE team did not add a large number of new features: 2000 feature requests were implemented in the past 6 months, meaning that any user's pet feature might well be among the improvements KDE 4.3.0 brings.
<!--break-->
The changes in KDE 4.3 are largely in three categories: the desktop workspace, the applications, and the development platform.
<ul>
    <li>
        Plasma, KDE's <strong>desktop interface</strong> and KWin, KDE's window and compositing manager are now working more closely together. Desktop effects and the Plasma shell now share their themes, and it is also possible to have separate sets of Plasma widgets and wallpapers on each virtual desktop. The new Air theme makes a visual difference. It is much lighter than the Oxygen theme, which is still available through the Desktop Settings dialog.
    </li>
    <li>
        On the <strong>applications</strong> front, KDE 3 users will like the new tree view in System Settings, which more closely resembles KDE 3's KControl. Dolphin, KDE's file manager, now show previews of images contained in a directory as an overlay for the directory icon. Hovering over it enables a slideshow of the images in the directory the icon represents.
    </li>
    <li>
        KDE's <strong>development platform</strong> has become more stable, more performant, leaner and at the same time more complete. For Plasma applet developers, there is now a geolocation plugin that makes it possible to query the current location. This is used in the OpenDesktop applet to show people near you. The new KDE PolicyKit library provides a mechanism for applications to authorise certain actions based on profiles. A KDE-style API makes it secure and easy to temporarily elevate privileges for an application.
    </li>
</ul>
</p>
<p>
Of course, those are just a few examples of the changes in KDE 4.3.0. All of the seemingly small changes add up to make a wonderful difference to our users.
</p>
<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/kde430.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/kde430_thumb.png" width="540" height="337" /></a><br />
The KDE 4.3.0 Desktop
</div>
<p>
The KDE 4.3.0 release will be followed by the usual monthly bugfix and translation updates. The next feature release, KDE 4.4.0, will see the light of day in January 2010.  For those among our readers who enjoy microblogging, we have set up an experimental "LifeStream" tracking the "!kde" tag on Identi.ca and Twitter. Check out <a href="http://buzz.kde.org">buzz.kde.org</a> for the stream. Jos Poortvliet, who also wrote the excellent release announcement, has created a <a href="http://jospoortvliet.blip.tv/file/2428228/">screencast</a> (<a href="http://blip.tv/file/get/Jospoortvliet-KDE43DesktopWorkspaceDemo820.ogv">Ogg Theora</a>, <a href="http://people.canonical.com/~jriddell/kde-4.3/">HTML 5</a>) that gives an overview of the versatility and coolness of KDE 4.3.0. As always, be sure to check out the <a href="http://www.kde.org/announcements/4.3/">release announcement</a> for more details.
</p>