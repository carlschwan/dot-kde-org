---
title: "KDE Windows Team Releases Applications from Software Compilation 4.3.4"
date:    2009-12-08
authors:
  - "Stuart Jarvis"
slug:    kde-windows-team-releases-applications-software-compilation-434
comments:
  - subject: "KOffice"
    date: 2009-12-08
    body: "I had a look at KOffice 2.1 and it is seriously promising, I definitly like what's happening with it. I can see myself replacing my existing Office suite with KOffice with the next major release (2.2)."
    author: "d2kx"
  - subject: "Pandering to Windows"
    date: 2009-12-09
    body: "If KDE is going down this road, I may find it humane enough not to continue with KDE. Somehow it just feels wrong to provide KDE in this fashion. I don't feel half as bad with distributions of http://www.andlinux.org/ or http://www.topologilinux.com/ or the like that actually run Linux on Windows as that is far more functional than just replacing the desktop window manager. It's still MS Windows.\r\n\r\nMake Microsoft publish Windows under GPL, maybe others and I'll stick around."
    author: "bmw"
  - subject: "\"It's still MS Windows\"... but..."
    date: 2009-12-09
    body: "Some people are forced to use Windows, whether through their jobs, or that *one* app that doesn't have a good Open Source alternative.  Realistically, Microsoft is never going to GPL Windows, but why not give those that have to use it the choice of using high quality FOSS software as much as they can?  Furthermore, if Windows users try apps from the KDE SC, maybe they'll be more inclined to try Linux/*BSD later.  Windows isn't going away any time soon, and including Windows users is hardly \"pandering\" to the OS/company itself."
    author: "ffejery"
  - subject: "Benefits and costs"
    date: 2009-12-09
    body: "Personally, using Firefox on Windows changed my view on Free Software - that was what made me realise that Free Software could actually be better than the proprietary competition and made me give Linux a chance. I do think having KDE applications on Windows is a net benefit for free software. I also believe that this outweighs the cost of making our software available to people not using free platforms. Overall I believe we're more likely to get people moving to free systems by seeing how great our software is rather than saying they can't use our software (that they don't even know about) until they switch.\r\n\r\nThe KDE Windows team has already addressed this - please see <a href=\"http://techbase.kde.org/Projects/KDE_on_Windows/FAQ#Reasons_for_KDE_on_Windows\">their FAQ</a>"
    author: "Stuart Jarvis"
  - subject: "Interesting..."
    date: 2009-12-09
    body: "What's your current office suite? Are you on Windows or Linux? (or something else?)"
    author: "Stuart Jarvis"
  - subject: "> If KDE is going down this"
    date: 2009-12-09
    body: "> If KDE is going down this road ...\r\n\r\nYou're thinking on the assumption that all of KDE is doing a concerted effort to \"pander to Windows\", when in reality, it is just a team who is doing this active work (a team that is relatively smaller compared to those who are working on KDE libraries and applications, regardless the platform\". In some cases, a developer doesn't even need to make any deliberate modification to his application to make it run on Windows. If the application doesn't have any Linux/Unix-only code or dependency, then there won't be anything to stop it from being cross-platform. It's partly a side effect of Qt and KDElibs being cross-platform already (and kdelibs  was already partly cross-platform a long time ago IIRC).\r\n\r\nAnd as the comments below say, it is less about \"pandering to Windows\" and more about creating quality Free Software (which has never been limited to Linux only) that happens to be also cross-platform, to be used by everyone, no matter his situation. IMHO this is just giving users one of the freedoms guaranteed by the Free Software philosophy: the freedom to use the software for whatever purpose. Even if it means using it on Windows. (Note: My interpretion, not FSF's)\r\n\r\nOf course, if you look at KDE development more closely, the tools and processes, you will see that KDE is still largely Unix-based/friendly. But I think what is more important is that it is still Free Software. :)"
    author: "Jucato"
  - subject: "I agree with ffejery. KDE on"
    date: 2009-12-09
    body: "I agree with ffejery. KDE on Windows has its own audiences:\r\n1. People who need to work on Windows but still love KDE applications (like me, at home I use Linux 100% but my jobs requires Windows).\r\n2. People who are new to Free Software (and Linux in general), who are hesitant to migrate to Linux (or BSD, or any other open source OS) entirely for various reasons. Introducing them to KDE applications on their native environment will give them better view of what free software can offer in terms of quality.\r\n3. And people who just want to use good software no matter what OS is being used and whether the OS is free or non free."
    author: "fyanardi"
  - subject: "Nothing special"
    date: 2009-12-09
    body: "I am mostly on Linux. I am using OpenOffice.org at the moment."
    author: "d2kx"
  - subject: "I really like KDE on Windows"
    date: 2009-12-09
    body: "OK, I don't need to work on it (99% of my time I'm in Linux), but when I'm sitting in front of XP I like to see some beautiful, functional programs. :-)\r\n\r\nUnfortunately, 4.3.4 broke something, I get a Qt error message, let's look on bko for a report...\r\n\r\nAnyhow, keep up your good work, KDE-on-Win-guys!"
    author: "Kirilo"
  - subject: "Offline install"
    date: 2009-12-09
    body: "Is there a way to download all packages and install offline? My work-computer is not connected to the Internet."
    author: "andreak"
  - subject: "Yes"
    date: 2009-12-09
    body: "Right after startup of the installer you can choose \"install from the internet\",  \"install from local directory\" and \"download only\"."
    author: "Kirilo"
  - subject: "What happened to Amarok packages?"
    date: 2009-12-09
    body: "Just noticed that there haven't been any updates in ages on windows front. Is there any particular reason for that other than lack of time?"
    author: "bliblibli"
  - subject: "First let me say that I'm"
    date: 2009-12-09
    body: "First let me say that I'm using only Linux or other free-as-in-speech platforms privately and that I'm helping developing KDE even though I don't have much time for it.\r\n\r\nSo why do we still need Windows? For some strange reasons there exist complete markets for specialized Software that runs well under Linux but could not be bought for it. Sounds strange? Why should someone make the effort to keep Linux compatibility but not officially providing that platform? Well, the Linux version could be bought and it gets bought. But only from very huge companies and for very much money. Why? There is absolutely no market for the standard version on Linux. Any little marketing-effort for Linux would cost much more than it could bring in.\r\n\r\nAnd for as long as this is the situation Linux will be only a toy to many companies (for example those that provide services for earlier described software). And if we want to change that we need to provide them with good cross-platform open-source Software to make it possible for them to change step by step.\r\n\r\nNot everything in the world gets done by volunteers. If it would be so I wouldn't have to work on proprietary projects to make my living. Somebody would provide me with shelter and food and I could spend all my time making the world a better one.\r\n\r\nAnd if you want to know how I could tell you all this for sure, well it's because I have to fight with that problem every day. (For example there is no better IDE than KDevelop but before it is 100% integrated into Windows I am not allowed to use it at work.)\r\n\r\nRegards\r\n"
    author: "JoselB"
  - subject: "KDE 3.4.3 on Windows"
    date: 2009-12-09
    body: "Since the using of opensource on Windows became possible, I really do it by myself. With Visual Studio 2008 and all the packages needed to build KDE 4, Qt 4.6 and others. Yeah, it is a long process with many moments of trouble. But yesterday i've done it, the last word. Thanks KDE Team for this. It's a great challenge, challenge to use the best. However, I'm not a Windows fan, but still have a good experience. For the first time, began from 4.3.80, but it's to heavy to do that, so many troubles, besides not every requirements may build by Visual Studio 2008, it's a pity. "
    author: "eshimsichi"
  - subject: "Windows integration"
    date: 2009-12-10
    body: "> .... than KDevelop but before it is 100% integrated into Windows I am not allowed to use it at work.\r\n\r\nWhat does this mean exactly ? \r\n"
    author: "rhabacker"
  - subject: "RE: KDE 3.4.3?"
    date: 2009-12-10
    body: "Surely you mean KDE 4.3.4?"
    author: "majewsky"
  - subject: "yeah, sure! KDE4.3.4 with"
    date: 2009-12-10
    body: "yeah, sure! KDE4.3.4 with different VC2008 may build stuff. Over 1.5 GB."
    author: "eshimsichi"
  - subject: "Cross-plattform"
    date: 2009-12-11
    body: "Cross-plattform often means better and more stable code. Other compilers, other architecture... KDE everywhere."
    author: "vinum"
  - subject: "kdebindings"
    date: 2009-12-13
    body: "the only one that cannot be compiled in any cases by VC2008."
    author: "eshimsichi"
---
Following shortly after the <a href="http://dot.kde.org/2009/12/01/kde-software-compilation-434-released-codename-cold">release of KDE SC 4.3.4</a> for Linux and similar platforms, the <a href="http://windows.kde.org/">KDE Windows team</a> is pleased to announce the release of the latest KDE software for the Windows platform.
<!--break-->
Highlights of the release include a Qt 4.6 package and KOffice 2.1.0 (however, in line with upstream, KOffice 2.1.0 is primarily intended for testing rather than production use). Konversation 1.2.1 and KTorrent 3.3.1 are also included. Older packages from the SC 4.3 series should be binary compatible, allowing you to choose which packages you would like to upgrade to the latest versions.

You can install KDE applications on your Windows platform using the <a href="http://www.winkde.org/pub/kde/ports/win32/installer/kdewin-installer-gui-latest.exe">latest installer</a> (Windows executable). Please remember that KDE software on Windows is still a work in progress, so applications can be unsuitable for day to day use at the moment.