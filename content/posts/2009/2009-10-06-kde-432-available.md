---
title: "KDE 4.3.2 Available"
date:    2009-10-06
authors:
  - "sebas"
slug:    kde-432-available
comments:
  - subject: "KDE Four Live"
    date: 2009-10-06
    body: "An easy way to give a quick test to KDE 4.3.2 is using <a href=\"http://home.kde.org/~binner/kde-four-live/\">KDE Four Live</a>. I don't know if it's linked from any of the important page, but I haven't seen it, and I'm quite happy that Stephan Binner has updated the ISO to the latest KDE. KUDOS to him!"
    author: "suy"
  - subject: "Solaris packaging started as well"
    date: 2009-10-06
    body: "The KDE4-Solaris project has branched off its specfile repository as well (solaris.bionicmutton.org/hg/kde4-specs-432/) to keep up-to-date (but I admit, that's a pretty obscure niche for now)."
    author: "adridg"
  - subject: "Information"
    date: 2009-10-06
    body: "Nice to know that SunOS got already newest packages, even when using OpenSolaris."
    author: "Fri13"
  - subject: "Great"
    date: 2009-10-07
    body: "The updates already hit the Arch Linux repos, downloading them as I type this. Great work KDE devs, the development pace of KDE never ceases to amaze me!"
    author: "bralkein"
---
The KDE community today proudly <a href="http://www.kde.org/announcements/announce-4.3.2.php">announces the immediate availability of KDE 4.3.2</a>. As with any minor release, there are no new features but a strong concentration on further polishing the 4.3 series, which has been widely received as a release suitable for end users of all sorts.

KDE 4.3.2 brings a nice number of bug fixes including crashers. UI issues have been ruled out in KMail, and KWin effects have become more stable. There is also a good number of fixes in KDE's core libraries, which are beneficial to all applications using KDE libraries. More information can as usual be found in the <a href="http://www.kde.org/announcements/changelogs/changelog4_3_1to4_3_2.php">changelog</a>. Most distributions will have updated packages available shortly, so do not hesitate to update your KDE. KDE 4.3.2 is a recommended to everybody using KDE 4.
<!--break-->
