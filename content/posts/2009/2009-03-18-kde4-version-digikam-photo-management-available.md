---
title: "KDE4 Version of Digikam Photo Management Available"
date:    2009-03-18
authors:
  - "sebas"
slug:    kde4-version-digikam-photo-management-available
comments:
  - subject: "Congratulations and thank you"
    date: 2009-03-18
    body: "A big congratulations for a job well done.  Thank you for providing such useful tools, I use it almost every day."
    author: "rdieter"
  - subject: "Yes!"
    date: 2009-03-18
    body: "God, hallelujah!! Been waiting for quite a while :)"
    author: "NabLa"
  - subject: "Promo Image"
    date: 2009-03-18
    body: "While I don't use Digikam myself (don't care much about photography), I hear it's an excellent application, so congrats from me :)\r\n\r\nBut on another note, I love your promo image in this article. May I ask how you made it?\r\n\r\n\r\nCheers, Mark.\r\n"
    author: "Mark Kretschmann"
  - subject: "borrowed"
    date: 2009-03-18
    body: "I've borrowed the image from Gilles' blog on digikam.org, so he's the one who should take credits (I guess)."
    author: "sebas"
  - subject: "Screenie"
    date: 2009-03-19
    body: "it could be made by Screenie\r\nhttp://code.google.com/p/screenie/"
    author: "Georges"
  - subject: "SVG + inkscape"
    date: 2009-03-19
    body: "I'm not the author of this promo page. It's done by a very good Finnish designer named <b>Risto Saukonp\u00e4\u00e4</b>. I know that he use SGV and Inkscape."
    author: "cauliergilles"
  - subject: "Made me happy"
    date: 2009-03-19
    body: "I use digikam for all my photo-needs and will have a look at the windows version too for my friends.\r\n\r\nSent a token amount of money their way as a little thank-you. \r\nBy posting that here I hope that someone else thinks about doing the same. "
    author: "osh"
  - subject: "What about printing?"
    date: 2009-03-19
    body: "I use KDE for all tasks discussed above but I must print photos in Windows. Are there some improvements in area of cropping, auto-fitting to paper, and so on related to printing?"
    author: "alukin"
  - subject: "work in progress..."
    date: 2009-03-19
    body: "Angelo Naselli work in new Print Assistant plugin. Code is already available in svn from kipi-plugins project, but not it's not yet complete.\r\n\r\nI recommend to checkout code, compile, test, and report to bugzilla.\r\n\r\nGilles Caulier"
    author: "cauliergilles"
  - subject: "Cheers bro, I didn't know"
    date: 2009-03-19
    body: "Cheers bro, I didn't know about this program. Spooky you posted it exactly the day I needed something like it :)"
    author: "NabLa"
  - subject: "Wow!"
    date: 2009-03-20
    body: "Between porting to KDE4, the new features, and the unbelievably-long list of bugfixes, I am just flabbergasted at how much has been achieved. Has digikam somehow been able to fund a whole army of fulltime developers? Or is just raw talent :-)?\r\n\r\nI haven't been able to test it yet, but from the changelog alone it is clear that this will be well worth the wait (and more). Thanks!\r\n"
    author: "tangent"
  - subject: "A little team"
    date: 2009-03-20
    body: "In digiKam, we are 3 active lead developers. We receive also some important patches from contributors.\r\n\r\nProject management use KDE bugzilla. We trying to organize bugs reports by priority order. Listing all closed bugs is a way to follow works in progress...\r\n\r\nFrom Kipi-plugins, there are others developers too. We contribute a lots to this project.\r\n\r\nThere are also some shared library used by digiKam and Kipi-plugins that we manage (libkexiv2, libkdcraw, libkipi) or contribute (Exiv2, and LibRaw).\r\n\r\nTo resume, yes, we are very busy. This is why, all contributions are welcome...\r\n\r\nGilles Caulier"
    author: "cauliergilles"
  - subject: "Ace ! Now I can upgrade"
    date: 2009-03-20
    body: "Ace ! Now I can upgrade gwenview :-)"
    author: "Tom Chiverton"
  - subject: "GREAT news!"
    date: 2009-03-21
    body: "All I can say is, YAY!!  I've missed Digikam since upgrading to KDE 4.x and am glad it's available again.  Good job!"
    author: "Xenix1985"
  - subject: "Thank You"
    date: 2009-03-24
    body: "Hello,\r\n\r\nThank you for a beautiful program.\r\n\r\n\r\n\r\n"
    author: "carabao"
  - subject: "Basic tools"
    date: 2009-03-31
    body: "I made promo from digiKam (with two digiKam color themes and KDE4 style \"bespin\") with KSnapshot and few small effects with the Inkscape (like the digiKam logo with digiKam slogan the Box on the middle), while the box is photographed and edited by colors and markings and then all edited on the GIMP and showFoto. All managed with digiKam. \r\n\r\nI tough first to use Screenie but it has little limited usage, so making that by hand was easier to get wanted result (like the Marble map area etc). \r\n\r\n \r\nI have two others coming for 0.10.0 release what can be founded later from digiKam flicker! site like the others."
    author: "Fri13"
  - subject: "well done"
    date: 2009-03-30
    body: "i think there are some amazing improvements there like the connection to facebook, flickr, picasa etc. really smooth."
    author: "benvantende"
  - subject: "be honest"
    date: 2009-04-06
    body: "Come on, Gilles, be honest. You guys have a bunch of robots typing away somewhere in a basement, right? Considering the excellent communication and project management (extensive list of bugs fixed, blogs etc) you wouldn't have time for that much coding ;-)"
    author: "jospoortvliet"
  - subject: "The program amazingly"
    date: 2009-06-09
    body: "The program amazingly flexible, it can be adjusted quickly in the necessary direction: to bring accents on houses, managers, to a disposition - and all it to keep for further use in the necessary situation. And, that the most interesting, you should not pass from one window in another, all settles down on one panel and you work in several <A href = \"http://www.expressdelivery.biz\">generic viagra</A> modes simultaneously and the necessary information receive in very convenient kind, and at own discretion can work with a tabular variant, the dynamic schedule or zodiac circle practically at once. "
    author: "bolnoy"
---
Photographers in the Free world rejoice! On behalf of the Digikam developer team, Gilles Caullier has <a href="http://www.digikam.org/drupal/node/434">announced</a>  the first KDE 4 release of Digikam, the photo management application.

<img src="http://dot.kde.org/sites/dot.kde.org/files/digikam_0.10.0.jpg" alt="" align="right" height="301" width="500">

Digikam is a full-fledged tool that covers the whole digital photography workflow, from downloading the pictures off of your camera (and in the new version even capturing photos from inside digikam) to sorting, editing, rating and tagging (including geolocation and editing of raw photos in the new version) and of course also publishing your photos. Digikam now also allows for importing images from Social Media such as Facebook and SmugMug. Export plugins makes it easy to publish your photos to sites such as Picasa, FlickR, 23hq, Facebook and SmugMug. For the more traditional paper version of photos, there is the scan option that lets you import and manage all your old photos from Digikam as well.
Light Table allows you to compare pictures side by side and the powerful Image Editor lets you touch up your photos, correct colors, white balance and contrast and remove red eyes. It also offers more advanced features such as ICC profile-based color management and lens vignetting correction.
<!--break-->
The new KDE4-based version of Digikam comes only one day after the release of <a href="http://www.digikam.org/drupal/node/432">Digikam 0.9.5 for KDE 3</a> and the release of <a href="http://www.digikam.org/drupal/node/433">Kipi-plugins 0.20.0</a>, the plugins that provide image processing, import and export functionality to Digikam and other KDE graphics applications. There is also a Windows version of Digikam available via <a href="http://windows.kde.org">windows.kde.org</a>. Digikam is Free Software, licensed under the GPL and can be obtained free of charge.
<p />
This brand new version of Digikam does not only come in new Oxygen-clothes, it also gained a host of new features, some of the more impressive are:

<ul>

    <li>
        Geotagging of photos with GPS coordinates
    </li>
    <li>
        UI improvements for using Digikam on netbooks
    </li>
    <li>
        Editing metadata of RAW files
    </li>
    <li>
        The camera interface has been redesigned and a capture tool has been added
    </li>
    <li>
        New photo search tools: based on location, similar images, a sketch
    </li>
    <li>
        Duplicate photo search
    </li>
    <li>
        Automatic correcting of lens aberration
    </li>
    <li>
        New ratio crop composition guide based on Diagonal Rules
    </li>
    <li>
        Faster loading of thumbnails using multithreading    </li>
</ul>
For a more comprehensive list, visit the changelog available via <a href="http://www.digikam.org/drupal/node/434">digikam.org</a>
Not only has Digikam received many new features. This combined release also squashes more then 500 bugs and surely makes it a compelling offering for both for beginner and more demanding digital image processing.