---
title: "Qt Software Releases LGPL'ed Qt 4.5 and Creator 1.0, Provides SDK"
date:    2009-03-03
authors:
  - "danimo"
slug:    qt-software-releases-lgpled-qt-45-and-creator-10-provides-sdk
comments:
  - subject: "Welcome to the world of tomorrow!"
    date: 2009-03-03
    body: "Obligatory Futurama quote in the subject, and thanks to you Qt guys :)\r\n\r\nBeen looking forward to this release, especially for the rendering performance improvements.\r\n<br><br>\r\n\r\nCheers! :)\r\n"
    author: "Mark Kretschmann"
  - subject: "KDE issue"
    date: 2009-03-03
    body: "http://www.tuxradar.com/content/linux-format-free-download-24-hours-only"
    author: "kragil"
  - subject: "what about qt-extended?"
    date: 2009-03-03
    body: "I see on http://www.qtsoftware.com/about/news/qt-software-discontinues-qt-extended/ that it's been discontinued. Has it been replaced by qt-embedded for linux? And what about the symbian version? (yeah, I'm curious :p)."
    author: "patcito"
  - subject: "I hope KDE will get better and faster on Netbooks"
    date: 2009-03-03
    body: "And I really hope it will get more space efficient and all those big fixed size windows go away."
    author: "kragil"
  - subject: "And I"
    date: 2009-03-03
    body: "really hope you will stop posting off-topic squibs."
    author: "Boudewijn Rempt"
  - subject: "?"
    date: 2009-03-04
    body: "Why OT?\r\n\r\nMaybe if the toolkit would support resizing windows on small screens it might help.."
    author: "kragil"
  - subject: "A very significant step"
    date: 2009-03-04
    body: "This lowers the barrier of entering the Qt technolgy significantly. Do you remember Delphi or C++Builder? After installing the program you dragged a few components to the form, pressed the compile button  and voila - you had your first compiled application. In contrast I found it quite messy to persuade Visual Studio to build Qt applications. Now we have the Borland feeling again without compiler vendor lock in, plus cross platform. \r\nMy guess: In a few years Qt will be the dominating API for application development on all platforms. \r\n\r\n"
    author: "bitarbeit"
  - subject: "Jonathan Thomas"
    date: 2009-03-04
    body: "No, that has nothing to do with the toolkit. It has everything to do with how the User Interface was designed."
    author: "JontheEchidna"
  - subject: "qt-extended"
    date: 2009-03-04
    body: "Qt Embedded is the old Qtopia-Core. Qt Extended is the old Qtopia. Qt/Embedded still exists, and is the same as ever (except that it's finally in sync with other Qt's).\r\n\r\n<p>Everyone cursed Qtopia, it was a huge marketing mistake. People would get excited by the promises, buy it, and then find out they had to do an extreme amount of hacking to get it to run on their hardware. What was supposed to be a convenient bundle of stuff for targeted embedded audiences (phone and home) ended up being more trouble than it was worth. Most customers ended up just using Qt/Embedded and doing their own integration.\r\n\r\n<p>So there isn't a huge loss by dropping extended.\r\n\r\n<p><em>just my opinion, of course..."
    author: "Brandybuck"
  - subject: "Selected Qt Extended features will be migrated to Qt X-platform"
    date: 2009-03-04
    body: "Basically three things are happening. \r\n<p>\r\nFirst selected features from Qt Extended will be re-factored and migrated into Qt, making it cross platform. There are several APIs as Blutetooth, IP telephony and input devices like touchscreens. Those will be available not only for Linux Embedded, but also on S60, Win CE and the different desktop systems. Things that will not be migrated into Qt are some legacy code which have little future value. This changes will not happen over night. It will take time. \r\n<p>\r\nThe added APIs in Qt will make it more easy making exiting KDE applications, reaching out to more devices with interesting input methods and connectivity. For those who may got concern that their Qt Extended/Qtopia applications will not run, they should know that most Qt Extended applications runs on Qt functionality. So it will not be a traumatic transition.\r\n<p>\r\nSecondly there will be added more unit tests. Relevant test/demo  applications will be cross platform. The plan is to also continue the \"select a device profile\", which let developers build a example stack of applications for different targets. This was Linux only. The plan is to make the \"choose a profile\" system cross platform, improving the quality of the migrated APIs. \r\n<p>\r\nThird we are opening up for external contributions by opening up Qt repository, using Git and Gitorious. We will not ask for copyright assignment. Instead we will ask contributors to grant Qt Software rights to reuse and improve the code as a part of Qt. When opening up, we are making communication more transparent, increasing our effort with community code camps and developer gatherings. \r\n\r\n<p>\r\nTwo articles describes the transition we are in: \r\n<p>\r\n<li> About the opening of the <a href=\"http://lwn.net/Articles/315066/\">Qt repository in LWN</a></li>\r\n\r\n<li>About future plans with Qt, a <a href=\"http://www.linuxdevices.com/news/NS4824952673.html\"> Interview with Benoit Schillings in LinuxDevices</a></li>\r\n<p>\r\n<b>\r\nPlease don't hesitate to ask more questions. \r\n</b>\r\n<p>\r\nBest regards\r\n<p>\r\n<p>\r\nKnut Yrvin<p>\r\nOpen Source Community Manager <p>\r\nQt Software, Nokia"
    author: "yrvin"
  - subject: "Market Position"
    date: 2009-03-04
    body: "I think if Trolltech had provided an IDE from the beginning, the software world would have been vastly different than it is today.  I think it is now too late for Qt to rule.<p>\r\n\r\nTrolltech's dependence on license revenue, and the lack of a dedicated Qt IDE, hurt Qt's market potential enormously, but Nokia is making all the right moves for Qt to gain back a lot of lost ground.\r\n"
    author: "Tony OBryan"
  - subject: "Kde 4.2.1"
    date: 2009-03-04
    body: "Will kde 4.2.1 compile with qt 4.5?"
    author: "Picander"
  - subject: "Compile? Yes. Be absolutely"
    date: 2009-03-04
    body: "Compile? Yes. Be absolutely regression-free? No.<br />\r\nThere are some graphical glitches in Plasma, and a few of the applications are a bit crashier than usual. (Kolourpaint, Kapman)"
    author: "JontheEchidna"
  - subject: "Too late?"
    date: 2009-03-06
    body: "i think the too late statement might be a little harsh - this is the long term, do remember, Qt has all the power required - the only other alternatives are Java/Swing and .NET, and neither of those are as comprehensive as Qt... With this release, the new license and the SDK, there really is no technologically based reason for not choosing Qt for application development - the only reasons are either personal or management-made IT decisions (and dog knows those happen all the time, but... at least now there are very valid and hard-hitting arguments for those opposed to them ;) )."
    author: "leinir"
  - subject: "It might make a difference in"
    date: 2009-03-06
    body: "It might make a difference in performance, but that's about it. Fixing the dialogs is simple (mostly a point-and-click job), it doesn't require many skills so if you really want it to happen - help. I guestimate one can fix the worst ones within a couple of weeks doing it in spare time (say an hour a day on average)"
    author: "jospoortvliet"
---
<a href="http://www.qtsoftware.com/products">Qt Software</a> has made <a href="http://www.qtsoftware.com/products">Qt 4.5</a> and the <a href="http://www.qtsoftware.com/products/developer-tools">Qt Creator 1.0</a> IDE <a href="http://www.qtsoftware.com/downloads">available for download</a>.  For the first time, <a href="http://www.qtsoftware.com/downloads">Qt SDK</a> is available, a convenient bundle of the two.  Qt Software will be opening up their development process by making it easier for people to contribute patches and add-ons in the forthcoming weeks. Both Qt and Creator now come with the an LGPL licensing option along with the current GPL and commercial licenses. </p>
<p>Qt 4.5 brings <a href="http://www.qtsoftware.com/products/whats-new-in-qt">many new features</a> such as a new pluggable graphics system, ODF support and an updated <a href="http://www.webkit.org">WebKit</a> component which features HTML 5, Netscape-Plugin (e.g. Flash) support and the <a href="http://trac.webkit.org/wiki/SquirrelFish">SquirrelFish</a> JavaScript engine. Find out more in the <a href="http://labs.qtsoftware.com/blogs/2009/03/03/qt-45-hits-the-virtual-shelves/">Qt release blog</a>, the <a href="http://labs.trolltech.com/blogs/2009/03/03/qt-creator-10-is-out/">Qt Creator release blog</a> and feel free to join <a href="irc://irc.freenode.org#qt-labs">the new #qt-labs IRC channel</a> on Freenode.
<!--break-->
