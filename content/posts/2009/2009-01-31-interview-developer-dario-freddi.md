---
title: "Interview with Developer Dario Freddi"
date:    2009-01-31
authors:
  - "jriddell"
slug:    interview-developer-dario-freddi
comments:
  - subject: "Awesome!"
    date: 2009-02-01
    body: "Awesome interview! Doing a good job representin' the Chakra supermodels! ;D"
    author: "sandsmark"
  - subject: "chakra is awesome"
    date: 2009-02-01
    body: "I'm using it right now and it's just pure awesomeness. By the way, did I mention how awesome chakra is? :)"
    author: "patcito"
  - subject: "wicd Solid backend"
    date: 2009-02-01
    body: "A great idea indeed!\r\nI use it since it has entered official Slackware and like it a lot.\r\nBut a better integration with KDE would be really wonderful.\r\nThanks for the interview and the work done on all these parts of KDE, it's really quite good."
    author: "Richard Van Den Boom"
  - subject: "Good interview :)"
    date: 2009-02-01
    body: "Now that was a really good and refreshing interview. Fun to read, and informative. The way we like it!\r\n\r\n\r\nThanks Dario + Riddell :)\r\n"
    author: "Mark Kretschmann"
  - subject: "...and KDE Italia?"
    date: 2009-02-01
    body: "As Riddel said in the starting notes, this interview comes from <a href=\"http://www.kde-it.org/\">KDE Italia</a>. We made the interview in December, if can read Italian see <a href=\"http://www.kde-it.org/news.php?extend.285.4\">here</a>. We have to say thank you to Diego Rondini known as Panda :) . He is the original interviewer. Anyway thanks to Riddel to have posted and reviewed this article.<br>\r\n<br>\r\nGiovanni Venturi"
    author: "slacky"
  - subject: "...yes"
    date: 2009-02-01
    body: "Yes, thanks to KDE Italia too, of course.\r\n"
    author: "Mark Kretschmann"
  - subject: "Solid-wicd"
    date: 2009-02-05
    body: "Dario,\r\nIn your solid-wicd stuff, did you target 1.5.x or 1.6.x (which is still in development)?  Come see us in #wicd again...\r\n-RW"
    author: "rworkman"
  - subject: "2010 KDE Calendar"
    date: 2009-02-09
    body: "I'd like to suggest a 2010 KDE Calendar project to raise money for KDE development.  Perhaps Dario would volunteer. ;)"
    author: "BeowulfSchaeffer"
  - subject: "gotta love the caption that"
    date: 2009-02-16
    body: "gotta love the caption that goes with the pic =)\r\n\r\nnice interview"
    author: "mxttie"
  - subject: "Good work! Your article is an"
    date: 2009-06-12
    body: "Good work! Your article is an excellent example of why I keep comming back to read your excellent quality content that is forever updated. Thank you!\r\nmy blog:  <A href = \"http://www.konferensblogg.com\">konferenser</A>"
    author: "3zak"
---
<p>It is with great pleasure that we publish this interview with Dario Freddi, the developer known as drf in the KDE community. For those who do not know, Dario dedicates his time to many aspects of KDE 4; PowerDevil for example, the power manager that has debuted in KDE 4.2, is the result of his hard work. Other projects which he contributes are Arch Linux and the Chakra Project, DeviceSync and PolicyKit-KDE! You can find much interesting information in his blog and in the various links here and there in this interview which comes from KDE Italia from last December.</p>
<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; float: right; width: 200px">
<img src="http://static.kdenews.org/jr/dario-freddi.jpg" width="200" height="530" /><br />
Like all KDE developers, Dario is both cool and good looking.
</div>

<p><strong>Hi Dario! Thank you for taking the time to answer to our questions! Can you briefly
introduce yourself to our readers?</strong></p>

<p>Dario: Ok! My name is Dario, I'm 20 years old, I study at the "Politecnico di Milano". Among my hobbies there is music, to which I dedicate a lot of time. Then come sports (basketball in particular), friends, girls, and so on. I started programming years ago with PHP and I actively contribute to KDE since roughly a year ago.</p>

<p><strong>Humm, music... do you also play an instrument?</strong></p>

<p>Dario: I play the guitar and the piano, but at the moment I'm mostly dedicating myself to singing.</p>

<p><strong>So do you play in a band?</strong></p>

<p>Dario: Yes, since 2 years.</p>

<p><strong>Interesting! Tell us the name of the band and if you already reached international popularity.</strong></p>

<p>Dario: We haven't got a name because, for some external reasons, we have been forced to change the name and we still haven't got ideas. We experienced a moment of popularity, we took part in several events in Milan and a couple of festivals out door, among which the Music Village in Calabria.</p>

<p><strong>Can I ask you how did you got in touch with Linux and why?</strong></p>

<p>Dario: Well, it's a difficult question, but I was expecting it... :) Mostly because I'm curious. I think I'm with Linux since 4 or 5 years, I started with Mandriva and gave up just after, subsequently, on the second try I managed to make the feat (installing Mepis, if I remember exactly) and from then on I didn't ever step out of Linux.</p>

<p><strong>You are collaborating with ArchLinux's KDEmod aren't you?</strong></p>

<p>Dario: Sure! Infact it is because of KDEmod that I made the move to KDE, maybe without it I wouldn't ever have joined the "big K" programming crew.</p>

<p><strong>So I should ask what brought you to choose Arch and KDE?</strong></p>

<p>Dario: Let's start with the simpler answer: the one for Arch. In my opinion Arch is a distribution you can only love or hate: in my case it has been love at first sight! Arch, if used the right way, is the distro that needs the less maintenance; thanks to its simplicity it is extremely performing and fast; and it is a rolling release. That's all I need from a distro.
It's a pain that some developers doesn't behave and that I had some discussions with them.</p>

<p>Regarding KDE, I must admit that I had used GNOME peacefully since more than one year ago. But KDE 4 totally stroke me, with its innovative ideas it has won my heart. With KDE 3 I didn't ever had any interest in it, but with KDE 4 I found my world because it can satisfy all my needs.</p>

<p><strong>Observing your "conversion on the way to Damascus" I ask you what you like of the other Desktop Managers and if you feel that KDE is missing something in respect to them.</strong></p>

<p>Dario: Bearing in mind that the only ones I have actively used for some time are Enlightenment E17 and GNOME, I won't express much on E17. I tried it some time ago, and still today I give it the credit of pushing me to learn the command line, because it was in an early stage of developement at that time. I would like to give it a try today, especially because of the good things they are doing (we'll be able to create plasmoids with QEdje in KDE 4.2), and I'm sure I'll do it when I'll
have time.</p>

<p>Regarding GNOME, it's a difficult question, but the answer is, honestly, that I think that KDE doesn't miss anything that GNOME has. On the contrary, I think that the idea that "GNOME is more usable" is partly wrong because, in this area, KDE is gaining ground. KDE has improved a lot regarding usability and rationalisation of the user interfaces, which was some time ago
GNOME's strong point. We should give credit of this change to the organisation of the community, where there are people working exclusively on these aspects. You can tell me I'm coloured, but at the time being (and I'm talking about KDE 4.2), KDE doesn't have anything to envy GNOME (after all, if I have been converted and I didn't get back there should be a reason! :D).</p>

<p><strong>And what about the programs? What's your favourite KDE application excluding yours? Which ones, in your opinion, need more love?</strong></p>

<p>Dario: There are plenty I like! Amarok 2, I love it.  The same for KMail. The KDE 4 version of K3B isn't ready, I'm feeling bad seeing it in this state... it is in need of a huge amount of love!</p>

<p><strong>You're not the only one thinking that way; could it be beacause Sebastian Tr&uuml;g  (developer of K3B and Nepomuk) has become a father and the remaining time he dedicates to Nepomuk?</strong></p>

<p>Dario: Bingo.</p>

<p><strong>Ok, now it's time to talk about your work. Can you tell us a little about PowerDevil? For example how the idea started, what's the state as of now and, if present, some gossip on future plans for this application.</strong></p>

<p>Dario: Well, the idea came to my mind when I took a look at the general state of the power management stack in Linux. There was nothing integrated, power efficient, and not intrusive. Though KPowersave was a good project it was a stand-alone application, and for a basic component of the system as power management is, that's not a good choice. PowerDevil
started to be integrated, efficient and not intrusive. Indeed it is a daemon (KDED module) that starts automatically, configurable through SystemSettings and manageable through Plasma. This is possible thanks to a set of libraries that occupies much less than KPowerSave, both on disk usage and on memory.</p>

<p>The project is now stable and mature. Thanks to the use of XSync it is probably the most efficient power manager in Linux, because it doesn't poll the system. It will be shipped in KDE 4.2 in kdebase-workspace.</p>

<p>For the future we will continue to work together with the Solid team for desktop integration. Even now all KDE applications can ask to deactivate automatic suspension with a single line of code, Kickoff [the current default menu in KDE 4] uses the PowerDevil settings to activate suspension. In future PowerDevil could become a real control centre for power management, using it applications could know how and how much they can use system resources.</p>

<p><strong>Would you like to talk about DeviceSync or on your work about the PolicyKit integration, or to talk about some ideas you already have in your mind?</strong></p>

<p>Dario: Ok, I'll do a couple of resumes... regarding DeviceSync, the work is going on while we're talking, Alessandro [Alessandro Diaferia, a KDE developer] is helping me with the GUI right now. The project aims to provide a universal manager for multimedia devices like iPods and similar, and to integrate it into the system through a KIO slave (soon on these screens). Support for MTP devices is already working, the application is in playground/utils at the moment, obviously in preprealpha.</p>

<p><strong>Great scoop for KDE Italia! :)</p>

<p>Dario: PolicyKit-KDE is almost ready, but we still need to squash some minor bugs. The biggest work now will be making the PolicyKit API, which is a nightmare, accessible to the KDE developers. We're starting to plan this work and I'll blog about it as soon as we have defined our plans more clearly. [Editors note: PolicyKit-KDE now works and is available in extragear.]</p>

<p>Another scoop: for KDE 4.3 I hope to get ready a Solid Network backend based on Wicd . In this way all the Wicd users on KDE will not be forced anymore to use the GTK GUI, but will be able to use the official KDE applet for network management.</p>

<p><strong>I've been hassling you for more or less an hour now, I think it's time to jump to the simplest question and then we can stop so you can go on developing 4.2!!</strong></p>

<p><strong>Do you like charts? Are you willing to answers the classical test "favourite song", "favourite film", "favourite beer"?</strong></p>

<p>Dario: Ok, I'll try :D</p>

<p>Well, the one about the favourite song is almost impossible. I can only list favourite artists: Thrice and Ataris (especially the latest 3 albums). Regarding songs that I like at the moment I would say: Follow and Feel of Saosin and Chapter III - Nostalgic Mannerism of The fall of Troy.</p>

<p>Regarding the favourite film, I'm sorry but I'm not passionate about cinema.</p>

<p>On the contrary beer is much more a field of interest for me! Blonde: triple karmeliet; wheat beer: kaputziner; amber ale: st. bernardus.</p>

<p>Before we end, some advertising?</p>

<p>Chakra is a live CD that we of the KDEMod team are in progress of making. It's made of Arch Linux + KDEmod4 + the several tools that made us famous like Shaman and Arxin, and it has a mouth-watering installer... the biggest part of the work comes from me, Lukas Appelhans (of KGet fame) and the rest of the KDEmod team. Videos and screenshots here <a href="http://chakra-project.org/">on the website</a>.</p>

<p><strong>Would you like to greet relatives and friends that are watching you from home? :)</strong></p>

<p>Dario: Yes, hi mum!</p>

<p><strong>Thank you very much for the interview you and I wish you good work and good study.</strong></p>