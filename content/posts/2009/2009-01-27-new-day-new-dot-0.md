---
title: "A New Day, A New Dot "
date:    2009-01-27
authors:
  - "CitizenKane"
slug:    new-day-new-dot-0
comments:
  - subject: "Looks great!"
    date: 2009-01-27
    body: "Nice new look"
    author: "JackieBrown"
  - subject: "congrats!"
    date: 2009-01-27
    body: "<strong>well done...!</strong>\r\nThe new look is great! I really liked the simplicity of the old dot layout though it was really starting to show its age.\r\n\r\nYou've done a fantastic job by not \"overdoing\" it. The new layout is good looking while not to fancy and still simplistic."
    author: "thomas"
  - subject: "Looks great"
    date: 2009-01-27
    body: "And the new features are a given too :-)"
    author: "augustofretes"
  - subject: "Simply beautiful!"
    date: 2009-01-27
    body: "Simply beautiful!Contrats to the web dev/designers!"
    author: "Autumnautist"
  - subject: "Great Layout"
    date: 2009-01-27
    body: "Congratulations to the team"
    author: "FrankKarlitschek"
  - subject: "At last, it's there!"
    date: 2009-01-27
    body: "Thanks to Kyle and to everyone who made this happen."
    author: "einar"
  - subject: "Thumbs Up!"
    date: 2009-01-27
    body: "Great Design... the redesign was really badly needed.\r\n\r\n<br><br>\r\nVery well done.<br>\r\nDaniel"
    author: "Daniel"
  - subject: "OH MY GOD!"
    date: 2009-01-27
    body: "What happened here?\r\nTime Warp from 2000 to 2009? ;)\r\n\r\nLook really great, guys! :)\r\n\r\n"
    author: "Robin"
  - subject: "A huge improvement"
    date: 2009-01-27
    body: "This is certainly a big ( and needed ) improvement.\r\nThough,I have two minor remarks:\r\n<ul>\r\n<li> I don't like the styling of the primary links </li>\r\n<li> the sidebar is quite wide, wich makes the article quite small. Certainly when images are used. Would you consider to not show them on the article page ( that's easy to do with drupal )? </li>\r\n</ul>\r\n\r\nBut I repeat: these are minor remarks. Your work is outstanding!"
    author: "jolos"
  - subject: "Good design"
    date: 2009-01-27
    body: "Really nice design, a breath of fresh air.\r\n\r\nSome comments however from Konqui on KDE 4 Trunk:\r\n\r\nBoth the subject field in comments and email address field in register + edit account are too long and go behind the kde apps box.\r\n\r\nThis post has no author or category.\r\n\r\nAnd the button to search is mushed up between search text box and the page design.\r\n\r\nOtherwise, an excellent perfect design. Thanks to all those who made this happen."
    author: "bcooksley"
  - subject: "Awesome"
    date: 2009-01-27
    body: "Good going. It looks great."
    author: "anoop.chargotra"
  - subject: "Looks very nice indeed. I"
    date: 2009-01-27
    body: "<p>Looks very nice indeed. I just hope the account requirement doesn't deter people from posting (especially cricisms, since there is the danger of mobbing).</p>\r\n\r\n<p>Some minor rendering glitches I see on FF3/MacOS (plus a couple of things I think have still room for improvement)</p>\r\n\r\n<ul>\r\n<li>the KDE.NEWS text at the top is quite blurry</li>\r\n\r\n<li>the rounded corner at the right bottom of the search bar seems to be missing two or three pixels</li>\r\n\r\n<li>the \"more\" links in the sidebar boxes could use some right padding</li>\r\n\r\n<li>the list bullets in the account sidebar are different and indented differently than the ones in the other boxes</li>\r\n\r\n<li>the KDE logo on the top right seems quite small. Also: Is it just me or is there some trapezoid distortion (top seems smaller than bottom)? Or is this some sort of 3d effect?</li>\r\n\r\n<li>the \"By: \" line for this article is empty</li>\r\n\r\n<li>the links are a bit too colorful for my taste: blue on :link, green on :hover and purple on :visited is a bit too much, maybe.</li>\r\n\r\n<li>the helptext says that lines and paragraphs break automatically, but this is not the case, at least not in the preview: All I see is one blob of text</li>\r\n</ul>"
    author: "sonic"
  - subject: "Nice work!"
    date: 2009-01-27
    body: "Looking good and seems to work well too. \r\n\r\nThank you all who have used their time providing this :-)\r\n\r\n"
    author: "tkoski"
  - subject: "Well done, but..."
    date: 2009-01-27
    body: "Technically it seems awesome, like the OpenID support (even though at least gmail doesn't seem to authenticate).\r\n\r\nHowever, the layout is definitely not quite right for me. I'd prefer a layout that spans the full page, either multi-column newspaper-style (yep, looks ugly but is actually less tiresome to read) or just regular single column."
    author: "radtoo"
  - subject: "This new look is CRAP"
    date: 2009-01-27
    body: "We are in 2009 and many web pages are unable to adapt to the view area of my browser? This is ridiculous, it's insane.\r\n<br>\r\n<br>\r\nWhy are you calling this \"an improvement\": The actual text of the comment now barely takes 40% of the width of my browser's window. The other 60% is just some crap and blank space."
    author: "Username"
  - subject: "congratulations"
    date: 2009-01-27
    body: "Very nice, thanks!"
    author: "pulp"
  - subject: "Go dot!"
    date: 2009-01-27
    body: "A new dot for a new kde desktop, seems fitting :)\r\n<p/>\r\nI agree with some of the comments here that the layout could still use some tweaking, but great work so far, the dot is definitely better this way!"
    author: "ianjo"
  - subject: "Go Go Gadget dot"
    date: 2009-01-27
    body: "Looks great. Good work. Kudos on the openid login working (after the second try) ;-)"
    author: "illogic-al"
  - subject: "I believe"
    date: 2009-01-27
    body: "what you were trying to say is:\r\n\r\nPlease sir, I want w i d e r. \r\n\r\nMe? I like it as is."
    author: "illogic-al"
  - subject: "negative vote by tone /language"
    date: 2009-01-27
    body: "Don't be stupid and try to write your criticism in a constructive way... your comment is crap!"
    author: "Autumnautist"
  - subject: "Self-adjusting"
    date: 2009-01-27
    body: "No, I wanted to say: Please sir, I want it to be automatically wider when necessary.\r\n<br>\r\n<br>\r\nFor the record, I don't care what you personally like."
    author: "Username"
  - subject: "Dude!"
    date: 2009-01-27
    body: "That's great. I was so afraid i could miss the release article (that will surely come on the dot ?) when you were down all morning long (in my TZ).\r\n\r\n\r\nNow you're not only back but looking better than ever (although I would appreciate if the text was a bit wider. I guess it's because of all the small devices that are all the rage? )\r\n\r\n\r\nAnyway - double celebration today: for the dot and for 4.2 !\r\n\r\n\r\nCongratulations, Dudes and Dudesses."
    author: "TunaTom"
  - subject: "Congratulations!"
    date: 2009-01-27
    body: "Congratulations on the new \"release\" of the Dot. These are much needed changes, IMHO, specially the moderation/rating system.\r\n\r\nThere's one peculiar thing I'd like to request though. Could you please reverse the order of comments from oldest at the top to most recent at the bottom? The way it is now is quite... unusual."
    author: "Jucato"
  - subject: "Level"
    date: 2009-01-27
    body: "Anybody want to see what a response to a 9-th level comment looks like? Let's try it - at least it will be something constructive, even demonstrative!"
    author: "Username"
  - subject: "\"Could you please reverse the"
    date: 2009-01-27
    body: "\"Could you please reverse the order of comments from oldest at the top to most recent at the bottom? The way it is now is quite... unusual.\"\r\n\r\n+1 from me :)\r\n\r\nAlso, is there a chance that the Flat Forty could make a return? The \"x new comments\" isn't quite the same as it is per-article and also you have to manually chase up the new comments as they are likely to be spread over the comments section.\r\n\r\nApart from that, this all looks much more modern and pleasing :)"
    author: "SSJ_GZ"
  - subject: "Next level"
    date: 2009-01-27
    body: "This is the 4th level of the famous \"The 10-th level response test\". See you next level."
    author: "Username"
  - subject: "Next level"
    date: 2009-01-27
    body: "This is the 5th level of the famous \"The 10-th level response test\". See you next level."
    author: "Username"
  - subject: "Next level"
    date: 2009-01-27
    body: "This is the 6th level of the famous \"The 10-th level response test\". See you next level."
    author: "Username"
  - subject: "Next level"
    date: 2009-01-27
    body: "This is the 7th level of the famous \"The 10-th level response test\". See you next level."
    author: "Username"
  - subject: "Next level"
    date: 2009-01-27
    body: "This is the 8th level of the famous \"The 10-th level response test\". See you next level."
    author: "Username"
  - subject: "Next level"
    date: 2009-01-27
    body: "This is the 9th level of the famous \"The 10-th level response test\". See you next level."
    author: "Username"
  - subject: "Next level"
    date: 2009-01-27
    body: "This is the final level of the famous \"The 10-th level response test\"."
    author: "Username"
  - subject: "Actually, the site adapts to"
    date: 2009-01-27
    body: "<p>Actually, the site adapts to the view area of your browser, the main text column just stays the same size, which is actually a good thing imo: text with lines longer then about 100 characters become a pain to read. Ideally you don't have to move your eyes much from the left to right. That's the reason newspapers have such small column. Or the default LaTeX template has such big margins on the left and the right. Actually most books and magazines have lines shorter then 100 characters.</p>\r\n\r\n<p>This is also one of the main reasons why wide screen monitors on a computer are such a terrible idea. Unless you can rotate them 90 degrees of course.</p>\r\n\r\n<p>Oh, and thanks to everybody involved in this new look, fresh and slick, just the way I like it. :)</p>"
    author: "pinda"
  - subject: "Overall great, but some annoyances"
    date: 2009-01-27
    body: "The new look and functionality is great but I agree with some comments here:<br>\r\n<br>\r\n* The text is too narrow. I have had enough of \"Optimized for X resolution\" webs. Please make the layout expand to the browser width.<br>\r\n<br>\r\n* I like to see oldest comments first.<br>\r\n<br>\r\n* Login to post? You have to be kidding me.<br>\r\n<br>\r\n* \"Lines and paragraphs break automatically\". It's not working for me. I had to insert manually some \"< br >\" tags."
    author: "DeiF"
  - subject: "Well"
    date: 2009-01-27
    body: "1. I agree with you about those 100 characters. Though I fail to see how it invalidates my idea in general.\r\n<br>\r\n<br>\r\n2. Hmm, I do not think you realize what \"age\" you are living in: this is the age of notebooks. Now explain these to me: Would a notebook with a screen 10:16 (instead of 16:10) be sitting stable on your desk? Where do you put the keyboard on such a notebook?  Or, just maybe, wide screen monitors on a computer might not be such a terrible idea at all.\r\n<br>\r\n<br>\r\n3. Your sentence \"site adapts to the view area of your browser, the main text column just stays the same size\" seems self-contradictory to me."
    author: "Username"
  - subject: "Y\ne\na\nh\n,\n \nl\ni\nn\ne\ns\n \n1"
    date: 2009-01-27
    body: "Y<br>\r\ne<br>\r\na<br>\r\nh<br>\r\n,<br>\r\n <br>\r\nl<br>\r\ni<br>\r\nn<br>\r\ne<br>\r\ns<br>\r\n <br>\r\n1<br>\r\n <br>\r\nc<br>\r\nh<br>\r\na<br>\r\nr<br>\r\na<br>\r\nc<br>\r\nt<br>\r\ne<br>\r\nr<br>\r\n <br>\r\nw<br>\r\ni<br>\r\nd<br>\r\ne<br>\r\n <br>\r\na<br>\r\nr<br>\r\ne<br>\r\n <br>\r\ne<br>\r\nv<br>\r\ne<br>\r\nn<br>\r\n <br>\r\nb<br>\r\ne<br>\r\nt<br>\r\nt<br>\r\ne<br>\r\nr<br>\r\n.<br>\r\n<br>\r\nPlease, stop this nonsense. Make the text adapt to the full width."
    author: "DeiF"
  - subject: "Configurability"
    date: 2009-01-27
    body: "I like the order of comments from the most recent to the oldest. The way it is now is quite... good.\r\n<br>\r\n<br>\r\nQuestion: How to please users if they have opposing opinions?\r\n<br>\r\nAnswer: Make the look of this site be, to some extent, configurable on a per-user basis."
    author: "Username"
  - subject: "One more rendering glitch"
    date: 2009-01-27
    body: "I have to add that, on a 4.2 RC1 Konqi, the Search button (which looks like it should just be a button with an arrow on it) also carries the caption \"earc\" (should most probably be \"Search\", but the space is not sufficient). Please fix."
    author: "majewsky"
  - subject: "Mail subscription"
    date: 2009-01-27
    body: "I wonder if the e-mail subscription functionality went away, or if I just can't find it, or if it's coming back anyways. Nevermind that, the new dot looks awesome, and should be pretty more or less future proof by running on Drupal.\r\n\r\n(Also, have you considered writing up a case study for the drupal.org front page? *That* might bring even more interest in the dot in particular and KDE in general :)"
    author: "Jakob Petsovits"
  - subject: "Very good, some improvements could be made"
    date: 2009-01-27
    body: "Firstly, congratulations to the new site!\r\n\r\nHowever, there are a few things that should be changed: I hate having to register in order to be able to post comments, and I find ist absolutely unneccessary. Why not let non-registered users post while they have to solve some captcha, and keep registering as option if you don't want to solve the captcha everytime?\r\n\r\nThere are also a few rendering issues on Firefox 3, and PLEASE, can you at least provide an alternative css file that enables us to view the dot in full width? The fixed layout is really annoying on my 22\" widescreen."
    author: "blueget"
  - subject: "bug"
    date: 2009-01-27
    body: "I just noticed that the \"Lines and paragraphs break automatically\" feature doesn't work as it should. Say, I'm making a paragraph break here [...]\r\n\r\n[...] and start again two lines below that, but my comment doesn't break. Any idea what may be causing that? Just trying out with &lt;p&gt;...\r\n\r\n<p>Blah. Yup, that works. It doesn't say that I'm allowed to use &lt;p&gt;, though. There's a mismatch in input formats and input format help somehow.</p>"
    author: "Jakob Petsovits"
  - subject: "News URL doesn't work"
    date: 2009-01-27
    body: "The news.kde.org URL doesn't send you to this site anymore.  Can you fix it?"
    author: "Luigiwalser"
  - subject: "Login to post? You have to be"
    date: 2009-01-27
    body: "<i>Login to post? You have to be kidding me.\"</i>\r\n<br/>\r\nNo.\r\n<br/>\r\n<br/>\r\n<i>\"Lines and paragraphs break automatically\". It's not working for me. I had to insert manually some \"< br >\" tags.</i>\r\n<br/>\r\nYes, that seems to be broken indeed.\r\n<br/><br/>\r\nText-width and comment-ordering could be settings. Kyle?<br/>\r\n"
    author: "danimo"
  - subject: "Cosmetic issues in the comments"
    date: 2009-01-27
    body: "The way the comments are displayed is really confusing.  Some are indented and some are not, you can't tell what's a reply to what, and the newer messages are on top o_O"
    author: "Luigiwalser"
  - subject: "Yeah, I totally agree with"
    date: 2009-01-27
    body: "Yeah, I totally agree with you. I tried to use my Google account to log in, but that did not work. The column with comments is too small now: I like to be able to *use* the space that my monitor offers. Furthermore, there is no way to get back from your account details to the dot itself and the planet KDE listing is way to short. It is also annoying that you are *forced* to preview your comment."
    author: "andresomers"
  - subject: "RE: This new look is CRAP"
    date: 2009-01-27
    body: "I agree and promoted you up.\r\n\r\nI just loaded the site and the first thing on my mind was: why's the text all compacted in the middle?  Less RSS feeds too.  And the worst new \"feature\" is self-moderated news comments.\r\n\r\nIMO the older mostly text web site was superior for the readers."
    author: "Ronald"
  - subject: "\"The fixed layout is really"
    date: 2009-01-27
    body: "\"The fixed layout is really annoying on my 22\" widescreen. \"\r\n<br>\r\n\r\nI don't really understand this argument. Why not make the browser-window smaller? Especially if you have a large screen, you could run several windows side-by-side.\r\n\r\nI can understand your frustration if you have a huge screen and you run your apps maximized, but surely no-one does that these days?"
    author: "Janne"
  - subject: "Fonts"
    date: 2009-01-27
    body: "The cosmetic issues in the comments seem to have been fixed since I posted that message.  Thanks!\r\n\r\n<P>The default font is really small though, and even if you use the Enlarge Font button in Konq (which really helps) the column stays pretty narrow."
    author: "Luigiwalser"
  - subject: "Agreed. OpenID support is"
    date: 2009-01-27
    body: "<p>Agreed. OpenID support is cool. Good stuff.</p>\r\n\r\n<p>And I don't like the fixed width layout either. Please let me use my screen in the way I prefer! I can resize the window myself, thanks.</p>\r\n\r\n<p>Oh, and most recent at the bottom is what I expect too. Having most-recent-new-post above feels unintuitive, particularly since most-recent-post-in-a-thread is below.</p>"
    author: "teatime"
  - subject: "Nice work"
    date: 2009-01-27
    body: "Good job, I like the new look!"
    author: "tcoopman"
  - subject: "Nice Job"
    date: 2009-01-27
    body: "Great looking new site.\r\n\r\nWell done to all those involved."
    author: "TheGreatGonzo"
  - subject: "Expanding comments"
    date: 2009-01-27
    body: "First of all, great work! Although I liked the old Dot, the new and improved site is much appreciated.\r\n\r\nHowever, there is one thing that bugs me: expanding comments (clicking on comments with score < 0) makes Firefox freeze for a couple of seconds. Anyone else who has this problem?\r\n\r\nMaybe it's an indication that I should update my browser, still running Firefox 2.0.0.12.\r\n\r\nAlso, an \"Add new comment\" link at the bottom would be nice."
    author: "Hans"
  - subject: "Thanks..."
    date: 2009-01-27
    body: "Great news, thanks!"
    author: "nethad"
  - subject: "Half of the screen is wasted!"
    date: 2009-01-27
    body: "Erm, why waste half of the screen? So much white and all those boxes on the right side...uhhh. I don't like it much, sorry. :\\"
    author: "stoked"
  - subject: "cool"
    date: 2009-01-27
    body: "looking nice! it was also a nice experience, that for once, my openid account was accepted (most sites don't accept myopenid.com)\r\n\r\n\r\nfor the full width lovers: maybe add an alternative stylesheet or a button like there is on websvn? although imho, it doesn't make much sense to have full width here..\r\n\r\n\r\nlast but not least: drupal ftw! :)\r\n\r\nedit: line breaks don't work"
    author: "mxttie"
  - subject: "Final?"
    date: 2009-01-27
    body: "Let's see..."
    author: "stoked"
  - subject: "Awesome!"
    date: 2009-01-27
    body: "Hey I've been waiting impatiently for the new dot and now I need to say that is cool!\r\n\r\nPs: if it is possible to hide comments, why not its replies too?"
    author: "xbullethammer"
  - subject: "Well"
    date: 2009-01-27
    body: "I think most people will still prefer the *original* version of the famous \"The 10-th level response test\", rather than your reprint."
    author: "Username"
  - subject: "Re: Expanding comments"
    date: 2009-01-27
    body: "I also have a problem with Firefox freezing for a few seconds, although I am using 3.0.5, and I get the following message:<br><br>\r\n\r\n<b>A script on this page may be busy, or it may have stopped responding. You can stop the script now, or you can continue to see if the script will complete.<br><br>\r\n\r\nScript: http://dot.kde.org/sites/default/files/js/c4b6ac7b4beaf295f91f75dfe1649020.js:13</b><br><br>\r\n\r\n--------------------------------------------<br><br>\r\n\r\nAlso a couple of other things:<br>\r\n- I agree that horizontal space could be used more effectively; I have a 15 inch non-widescreen monitor and even on that there is a lot of space on the sides.<br>\r\n- The old Dot used to link to more articles from PlanetKDE on the side, I can't remember exactly how many but definitely more than five (which is how many are currently shown). I know this is a minor issue because I could just go to PlanetKDE to see older posts but I really like just seeing the headings on the side, it makes it much easier to decide what to read after a quick glance. Eight to ten seems like a good number to me...<br>\r\n- Where it says \"More information about formatting options\" when you go to comment there is a link, but it doesn't tell you which HTML elements you can use (if any). That's why I just used plain text in this comment with a few feeble attempts at formatting (eg. I could use list elements or blockquote elements if they are available...) <b>EDIT:</b> I've since discovered that you have to put in a br tag to get a line break, just pressing enter doesn't work... and the \"b\" (bold) tag works (sorry, I know its deprecated but creating a full tag with style attributes takes a lot more typing).<br>\r\n- Dunno if it's been fixed now, but the first time I visited the new Dot (Firefox 3.0.5) the CSS stylesheet wasn't loaded (although now I have visited a few other pages including the front page again and that's working fine. Still, it's not a good look if that happens to someone visiting the Dot for the first time).<br>\r\n-------------------------------------------<br><br>\r\n\r\nBut having said all that, I really like the look of the new Dot and agree that there is a need for comment moderation of some form. Congratulations to everyone involved, and seeing this is the \"point zero\" release it can only get better!<br><br>\r\n\r\nThank you!"
    author: "sf"
  - subject: "You can't change your browser window?"
    date: 2009-01-27
    body: "If you have problems with the readability of the text, what about resizing your browser window?  Its not that hard.\r\n\r\nI'm quite annoyed that 75% of my screen is white and I see 5 words per line.  And I can't do anything about it since the site decides for me which amount of pixels work for me :("
    author: "zander"
  - subject: "Great look!"
    date: 2009-01-27
    body: "Great, great, great! Now the site really matches a modern KDE4 desktop :)"
    author: "Peter de Sowaro"
  - subject: "half?"
    date: 2009-01-27
    body: "its more like 75% on my widescreen (which I think is common nowadays).\r\n\r\nI'll add my vote to this. I don't understand why the dot jumps on the bandwagon of having fixed-width layouts. It makes things really bad on mobile devices (which tend to have loads of pixels per character and thus only a couple of words per line now).\r\n\r\nCan we please have a layout that is not optimized for beauty primarily?\r\nI think people tend to be very capable of resizing their browser windows if they don't want really wide text.  Has worked for me for the last 15 years ;)"
    author: "zander"
  - subject: "Nice"
    date: 2009-01-27
    body: "Looks great, and it's only going to get better from here. Awesome job Kyle!"
    author: "Jess"
  - subject: "You don't understand the argument?"
    date: 2009-01-27
    body: "Maybe it was too complex for you to understand?\r\n\r\nOK, here you go: FIXED LAYOUT SUCKS! The content has to adapt to the resolution of the browser window, not vice versa."
    author: "blueget"
  - subject: "The layout"
    date: 2009-01-27
    body: "<p>If the layout spans the full page, lines will be too long. Normally, for a proper legibility, lines with no more than 80-100 characters are a must. With the actual layout lines are about 90 characters, with is nice.</p>\r\n\r\n<p>There was a problem, however, with the old dot design that was very unpleasant: when somebody posted a long link, the width of the page was too big, and the uncomfortable horizontal scroll appeared. I hope that this doesn't happen anymore, either because \"overflow: auto\" is used in comment's block, or because basic HTML is allowed (Drupal will do the filtering, although it's a security concern).</p>\r\n\r\n"
    author: "suy"
  - subject: "Thank you"
    date: 2009-01-27
    body: "kudos to all of you. It's really good."
    author: "zayed"
  - subject: "No prev/next story link?"
    date: 2009-01-27
    body: "I like the new look, but when looking at a specific article I can't seem to find the good ol' \"previous story\"/\"next story\" links anymore. Would appreciate it if you could put them back in."
    author: "oblomov"
  - subject: "Cool"
    date: 2009-01-27
    body: "It is simply cool!"
    author: "smihael"
  - subject: "So so"
    date: 2009-01-27
    body: "So so\r\n\r\nI completely agree with many other posters: fixed with layout sucks! I'm perfectly capable of resizing my browser window myself, should I feel the need to.<p>\r\n\r\nAlso, what's with the line spacing? Every line looks like a new paragraph and it completely ruins the flow. Setting the line spacing property is completely unnecessary. Why not force the font size too while you're at it?<p>\r\n\r\nUnfortunately the OpenID login doesn't work, it fails with \r\n<pre>\r\nwarning: fread() [function.fread]: SSL: fatal protocol error in /srv/www/dot/includes/common.inc on line 501.\r\n</pre>\r\nPlease fix.\r\n"
    author: "SP"
  - subject: "I agree"
    date: 2009-01-27
    body: "Congrats to migrating to Drupal and for creating one of the best <strong>free</strong> desktop environments!\r\n\r\nI agree with the statement <blockquote><em>Also, have you considered writing up a case study for the drupal.org front page?</em></blockquote>\r\n\r\nIt would be nice to see a wrap up of the dot.kde.org case study website in drupal.org."
    author: "Vasilis"
  - subject: "Finally!"
    date: 2009-01-27
    body: "The Dot was way overdue for a facelift, and it looks good. The comment threads could perhaps be a bit wider since now they are a narrow strip in the middle of the screen. I love that I can use OpenID."
    author: "Haakon"
  - subject: "looks great"
    date: 2009-01-27
    body: "The new dot looks great. I was pleasantly surprised when I came here to read the release announcement for KDE 4.2\r\n<br/>\r\nI understand how much time it can take to put together a really nice looking dynamic website. Thanks for all your hard work."
    author: "tubasoldier"
  - subject: "Simply impressive!"
    date: 2009-01-27
    body: "I had to double-check the URL when I saw this brilliant new layout. Simply beautiful! It's consistent with the KDE 4.2 style, and yet still unique. An excellent job!"
    author: "madman"
  - subject: "ajax comments"
    date: 2009-01-27
    body: "Congrats for the new dot, it looks great. As a suggestion, it would be great to be able to post comments with ajax so that I don't need to load 4 pages (posting, previewing, saving, getting back to the article)."
    author: "patcito"
  - subject: "Feature requests"
    date: 2009-01-27
    body: "<p>I appreciate the much work you put in porting the site and I don't post this pack of request with expecting that they should all be done soon. Just ideas for improvements.\r\n</p><p>\r\nI think the following options would be useful (some of which I saw on other Drupal based sites):<br />\r\n- Mark new comments<br />\r\n- Set the number of comments on a page<br />\r\n- Set that I don't want to hide comments with low scores. I would even make this default: unpopular comments may still hold truth.<br />\r\n- Alternative variable-width stylesheet like on planetkde.org. Making both options available would stop the debate about which is the better.<br />\r\n- Tracker where I can see whether there are new comments in the topics to which I have posted a comment.<br />\r\n- Save button without preview. For simple text-only comments it is unneccesary and annoying to have to see Preview first, especially that the comment can be edited later if something is wrong.<br />\r\n- Permalink at the comments (that will jump to that comment)\r\n</p><p>\r\nBy the way, it seems to me that jump to comment (e.g. after posting it) does not (always) work.\r\n</p><p>\r\nSorry if I'm lame and just couldn't find these settings.</p>"
    author: "gd"
  - subject: "Woo Hoo!"
    date: 2009-01-27
    body: "<p>Looks brilliant guys, congrats and thanks for all the hard work.  Shiny shiny :-)\r\n\r\n<p>To the whingers, Drupal supports a whole lot of user configurable options such as choosing your own theme, so it is entirely possible that a variable width version of the theme could be made available for you to choose in your profile.  I have little doubt one will be done when the guys have the time, but it is a brand new site and they have limited time.  \r\n\r\n<p>BUT how about asking for it nicely???  Kiddies, yelling FIXED WIDTH SUX YOU LOSERS won't get you what you want in a grown-ups world, you'll just get ignored.\r\n\r\n<p>And as for requiring log-in to comment, the hope is it will reduce the level of immature bashing going on around here, but from this response it appears we may have been optimistic in that."
    author: "odysseus"
  - subject: "About time. "
    date: 2009-01-27
    body: "You don't know how long I have been waiting and expecting this to happen. <br>\r\n<br>\r\n\r\nThanks Web Developers/Artists, it looks great and well needed addition to the KDE Website. "
    author: "Dekkon"
  - subject: "Comment formatting"
    date: 2009-01-27
    body: "Nice looking site, but when I make a comment, my line breaks are forgotten. The word \"Also\" in the next line is prefixed with 2 CR's.\r\n\r\nAlso, there are almost no formatting options."
    author: "fvzwieten"
  - subject: "Where to report problems?"
    date: 2009-01-27
    body: "No, sorry, not everyone enjoys the Dot as much as you enjoyed remaking it (at least assuming you didn't actually hate remaking it). The layout is rather broken for me. Where is one supposed to send bugreports about the Dot?\r\n"
    author: "Lubo\u0161 Lu\u0148\u00e1k"
  - subject: "Yet another commenter"
    date: 2009-01-27
    body: "I have a widescreen laptop, and would rather not have 75% of the screen wasted (fixed width layout). If it weren't for that, it would be truely perfect; good job.\r\n"
    author: "Michael Howell"
  - subject: "-"
    date: 2009-01-27
    body: "There\u2019s also a rule about line lengths for text.  It\u2019s unwise to make areas with lots of text too wide.\r\n\r\nA great solution would be to have an alternative version with bigger text and site graphics (zoom just doesn\u2019t do it any justice)."
    author: "ralesk"
  - subject: "Good job"
    date: 2009-01-27
    body: "I like the new look a lot, although I\u2019d totally love a wider-bigger version for easier reading.  I think we\u2019ve grown out of the 800px wide era a while ago.\r\n\r\nI hope you\u2019ll bring back the \u201cdepartment\u201d tags, I always enjoyed reading them behind the poster\u2019s name."
    author: "ralesk"
  - subject: "The layout"
    date: 2009-01-28
    body: "> If the layout spans the full page, lines will be too long.<br/><br/>\r\n\r\nIf the layout spans the full page, it will be <b>configurable</b> by the user (as long as he has a window manager installed). Now there is only a narrow column in the middle of the screen, occupying \u00bc - 1/3 of the width. The rest is white. And that sentence of yours is all that fits on a line. It is only 59 characters, not 80 - 100 as you claim.<br/><br/><br/>\r\n"
    author: "odalman"
  - subject: "Text-width and comment-ordering"
    date: 2009-01-28
    body: "Yes, text-width should be easily configurable with the window manager. Comment ordering should be a setting in the user account."
    author: "odalman"
  - subject: "Breathtaking!"
    date: 2009-01-28
    body: "<p>Do I need to say more?\r\n\r\n<p>However I don't believe we shouldn't let anonymous users express themselves - sometimes they share very deep thoughts and have no time to register. And with /. like system of commenting annoying comments can be easily buried in Hell.\r\n\r\n<p>So, let anonymous users in but oblige them to answer a CAPTCHA (or your old puzzle)."
    author: "birdie"
  - subject: "The design is broken in many places"
    date: 2009-01-28
    body: "<p>Konqueror 3.5.10\r\n\r\n<p>http://imageshare.web.id/images/mxg17xd8xjy24ejulsa.png\r\n\r\n<p>Log in user OpenID lacks any image.\r\n\r\n<p>Search button text doesn't fit in.\r\n\r\n<p>51 comments | 51 new comments (instead of \"Comment this\").\r\n\r\n<p>Right boxes lack modern glossy/shiny style ;)\r\n\r\n<p>Comments page should be redesigned to occupy 100% of page with (right column isn't needed at all at the comments page - and that space is not used right now).\r\n\r\n<p>Please, decreases spacing between articles and page elements.\r\n\r\n<p>Articles paragraph padding should be set to \"justify\"."
    author: "birdie"
  - subject: "Elastic layout"
    date: 2009-01-28
    body: "<p>A lot of people here complain about the fixed layout. Why don't you use the \"elastic\" (NOT fluid) layout instead?</p>\r\n\r\n<p>\r\nSee:\r\n<br />\r\n http://www.onenaught.com/posts/9/fixed-fluid-or-elastic-width-layouts#toc-benefits-of-elastic-layout-em-based-units\r\n<br />\r\n\r\nhttp://www.456bereastreet.com/archive/200504/fixed_or_fluid_width_elastic/\r\n</p>"
    author: "Vin"
  - subject: "Too narrow for replies"
    date: 2009-01-28
    body: "Each reply gets aligned right a bit more related to it's parent, so, after a few replies, you end up with comments with a word per line ... that is just bogus.  Please, make use of all screen estate.\r\n"
    author: "pedro.alves"
  - subject: "Where can I set the threshold?"
    date: 2009-01-28
    body: "I would actually love to read comments that are not praising KDE as well, by default if possible."
    author: "testerus"
  - subject: "All comments on one page?"
    date: 2009-01-28
    body: "The old dot had the feature to display up to 1000 comments on one page. This was quite useful, e.g. if you are searching for a post or looking for posts that cover a specific topic."
    author: "testerus"
  - subject: "Great Job!"
    date: 2009-01-28
    body: "This is excellent work and this from a hard grader. :-)\r\n\r\nI love the edit feature.  I always screw up my posts and find the mistake later.  Now I don't have to look like a complete idiot. :-)\r\n\r\nOne thing, the comments do not go all the way to the right edge of my screen.  Is this a function of the screen resolution (mine is 1024 wide)?  Or, does it need to be adjusted.  IAC, comments are only using about 2/3rds of the space available on my screen."
    author: "KSU257"
  - subject: "Good to have you back ^^\nI"
    date: 2009-01-28
    body: "Good to have you back ^^\r\nI love your new look and feel.<br/>\r\nhttp://blog.snyke.net"
    author: "Snyke"
  - subject: "Wonderful New dot"
    date: 2009-01-29
    body: "The new dot looks amazing great work guys."
    author: "lawaladekunle"
  - subject: "small typo"
    date: 2009-02-02
    body: "Thought you might want to correct this, given that you were thanking them in this post:\r\n\r\nOSUO<b>U</b>L -->  OSUO<b>S</b>L"
    author: "maniacmusician"
---
Hello everyone!  I would like to welcome you to the new and improved KDE Dot News.  As I am sure many of you know, although the old KDE Dot News has served the KDE community very well, it was beginning to show its age.  Because of that a number of people including myself took it upon ourselves to modernise the Dot.
<!--break-->
The new Dot also has a number of exciting new features including:

<ul>
<li>A comment moderation system similar to Slashdot</li>
<li>The ability to translate stories (and interface elements) into multiple languages</li>
<li>It streamlines story submission and the editing process</li>
<li>A sexy new look</li>
</ul>

As part of this, it is now going to be required to get an account to comment.  While we were somewhat hesitant to do this we feel that having an account will help encourage a greater dialogue among users on the Dot.  Since signing up to get an account can be a pain, you can already login if you have an OpenID.  If not I encourage you to sign up for an account now.


Importantly, the new Dot is being based on the open source content management system Drupal. This will make continued maintenance of the Dot easier and will also allow us to roll out cool features at a faster pace.  Additionally, every story and comment that has ever been written on the Dot has been imported into Drupal.


I would like to thank Philip Tyrer and Nuno Pinheiro for the awesome new design for the Dot. I would also like Daniel Molkentin for helping export the stories from the old Dot and Dirk Mueller for taking the time to troubleshoot server issues.  Many thanks go to <a href="http://osuosl.org/">OSUOUL</a> who hosted our server very reliably for many years. Lastly, thanks to everyone in the KDE community for their support throughout the entire design and implementation process.


We hope everyone enjoys the Dot as much as we enjoyed remaking it.


- Kyle Cunningham