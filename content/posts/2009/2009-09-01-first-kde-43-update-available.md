---
title: "First KDE 4.3 Update Available"
date:    2009-09-01
authors:
  - "jriddell"
slug:    first-kde-43-update-available
comments:
  - subject: "Congrats team"
    date: 2009-09-01
    body: "And classy move on the dedication.  The KDE community is a pretty awesome thing."
    author: "troy"
  - subject: "About Kmail bug fixes"
    date: 2009-09-01
    body: "Am I the only person suffering this tedious bug:\r\n  https://bugs.kde.org/show_bug.cgi?id=196949\r\n?\r\n\r\nBasically, since the new feature to \"group\" messages not just by threads, but also by date, sender and so, ordering mails just by threads (without \"groupping\") is really *slow* and it's performed *each* time I click in the folder (a local maildir with ~ 13000 messages). So each time I click my inbox folder, it takes ~30 seconds ordering all the mails again.\r\n\r\nI insist on the fact that I *don't* use the new \"groupping\" feature, but just the classic threading order (which worked perfectly in KDE3 and KDE4 until \"groupping\" was added).\r\n\r\nAm I the only? This bug is killing my patience and it's the first time I'm thinking about leaving Kmail and using Gmail web interface (which is faster and better searching for mails).\r\n\r\nAnyhow congratulations to KDE developers for this release."
    author: "ibc"
  - subject: "fish"
    date: 2009-09-01
    body: "\"Support for transferring files through SSH using KIO::Fish has been fixed\"\r\n\r\nExcellent!"
    author: "vinum"
  - subject: "KMail features"
    date: 2009-09-01
    body: "And I would be very happy to see KMail (whole Kontact) to support Ctrl+M function. KMail has bloated user interface and worse user experience because the forced menu what is not needed all the time (only mayby few times a year).\r\n\r\nThat is just a wish what would fit to dragonplayer and many other applications as well. ;-)"
    author: "Fri13"
  - subject: "?"
    date: 2009-09-01
    body: "What is a ctrl+m function?"
    author: "whatever noticed"
  - subject: "Ctrl+M hiddes/shows the meun"
    date: 2009-09-02
    body: "Ctrl+M hiddes/shows the meun bar (at last in most of the KDE applications, but not in Kmail)."
    author: "ibc"
  - subject: "Control-M and hiding menus (and an ad for how to help ;)"
    date: 2009-09-02
    body: "There is a wishlist item for that in bugzilla under the general \"kde\" component. \r\n\r\nI know that Akregator implemented it, so I suspect interested parties could go and look at that commit, and use that to come up with a patch to apply to their favorite application. \r\n\r\nBTW, Dragonplayer seems to be without a maintainer right now, so if you're interested in it, go ask the last one. You should be able to get a patch review on the kde-devel mailing list.\r\n\r\nI wonder if KMail has had a UI check run on it? I would certainly like it to work better with small screens, and I keep meaning to grab tcmguire on irc again to ask him about that. :) (Who you would go to for patch review for KMail ;) (I believe somewhere in techbase is some very through work of Celeste's writing about how to run a UI review on an app, if anyone is interested in doing something for KMail. Or anything else. Her blog would have links.) \r\n\r\nExcept I don't actually think Control-M is the best way to deal with this sort of thing. It isn't something you would guess, or that would be obvious. This is something the netbook project will hopefully come up with a good answer for (if they haven't already). But I hope it isn't usable just on a low-powered machine, but also on a blinged-out, high-powered small laptop. I should probably go find that project and look at it. Maybe posting here will let me cheat and have someone post a link for me. :)"
    author: "blauzahl"
  - subject: "Take a look at my patch"
    date: 2009-09-02
    body: "Take a look at my patch here:\r\nhttp://forum.kde.org/viewtopic.php?f=17&t=76641&sid=0d0595ef307b1c66b205f92d3d3fed9a\r\n\r\nEDIT: I wa replying to ctrl+m post, but the login did not remembered that apprently, dossy about that."
    author: "Elv13"
  - subject: "dragon player"
    date: 2009-09-02
    body: "I'm the Dragon Player maintainer, I just rather hate bugzilla. *hides*\r\n\r\nCTRL-M always seemed like an anti-feature to me. Its easy for people to trigger it without knowing what their doing, and of course hard to get it back. And in the special case of Dragon Player there's full screen mode already so its less needed."
    author: "eean"
  - subject: "Those bugs currently are sent"
    date: 2009-09-02
    body: "Those bugs currently are sent to the unassigned-bugs mailing list, would you like them to go to your email? Or is there a dragon mailing list? \r\n\r\nhttps://bugs.kde.org/component-report.cgi?product=dragonplayer will give you a nice overview of them, btw.\r\n\r\n(Map control-m to full screen? Does that go against usability?)"
    author: "blauzahl"
  - subject: "eeeeean!"
    date: 2009-09-02
    body: "Please look 2 comments below this thread, somebody posted a patch ;-)"
    author: "jospoortvliet"
  - subject: "It is nice that someone has"
    date: 2009-09-02
    body: "It is nice that someone has done global function for such. But i think Alt is not good idea for such function. Even that IE or Firefox with addon has Alt as shortcut, it is very easy to press by accident. The Ctrl+M does not happend so easily. And is still possible to press by old people and new computer users.\r\n\r\n(I continue on the thread on up.)"
    author: "Fri13"
  - subject: "The usability problem here is"
    date: 2009-09-02
    body: "The usability problem here is that GUI should be clean as possible for old (all) people. Only to show features what they need. They are not powerusers or users who change their behavior every other week. But they have lots of problems with UI what is cluttered with options. Too many icons, menu, buttons, panels and so on. And their computer usage changes to work. And if there happends one small change, they can be totally lost because they can not find what the change has be. \r\n\r\nMany defends leaving the Ctrl+M out by way it would be too easy to press. Well, old people has enough problems to press any shortcut (copy, cut, paste, bold, underline, all, to addressbar), heck, even the function keys are very difficult to use. There has be some questions to get it back. \r\n\r\nWell, if we would like to do it then protected that no one would accidental press it. Lets disable the Ctrl+M feature by default. So only way to hide menu would go to menu. Or add it only to context menu where it could be enabled or disabled (but clutter there..).\r\nMake the globalshortcut to the system settings what would allow users to enable it if wanted. Other wise it would not be used or even come up to menus? I have seen many times problems by users dragging the toolbar out of application or on to the different order than by default it is. Because by default KDE does not lock it's toolbars but keeps them open. \r\nI have not seen than once someone pressing Ctrl+M accidental. Reason was that she needed to learn surf web with keyboard only, so Ctrl+, and Ctrl+. were just next to the button. But She liked the menu be hided and got it right away that function was somewhere near those buttons. \r\n\r\nCtrl+Alt+M is too difficult shortcut for old people (young and new users can do it easily). They have lots of problems pressing multiple buttons same time. It is harder to remember. And this is not just for Ctrl+M function, but all shortcuts as well. Learning to use Ctrl+A Ctrl+c/Ctrl+x and Ctrl+V shortcuts is hard. So changing it to Global function (Ctrl+Alt+M) is not possible, because users should be possible get it back if needed easily.\r\n\r\nAnd if applications use different shortcuts too much, like Ctrl+m is on many (if not on most) applications feature, it makes harder to use them. Dolphin, Konqueror, Kopete, Ktorrent, Konsole, digiKam, Kate, Gwenview, KGet, Okular and many others allow user to hide the menu with Ctrl+M. Many of those are popular applications what uses the function. Then the mentioned Kolorpaint use Ctrl+M something else. What is already against then the current feature.\r\n\r\nBecause Ctrl+M is not global function for all KDE apps, it makes problems. People actually exepts that all applications has this feature. But it is not.\r\n\r\nCtrl+M could not be changed to something else. It would brake the usability with current users and the \"Ctrl Menu\" fits for english (like Ctrl+P for printing).\r\n\r\nWe should get this discussion to (Kmail) usability postlist up again."
    author: "Fri13"
  - subject: "\"And in the special case of"
    date: 2009-09-02
    body: "\"And in the special case of Dragon Player there's full screen mode already so its less needed.\"\r\n\r\nFullscreen is not a option for case. It will hide all other windows. It does not allow user to maintaint simple GUI with easily clicked UI. \r\n\r\nThe dragonplayer current UI is littlebit against logic (sorry, my opinion ;))\r\n\r\nExample, when opening dragonplayer without media. It shows\r\n\r\nPlay Media, Play File, Play Disk options. Fullscreen and volume as position slider are disabled. Next is list of last played media. Easy and bretty forward. \r\n\r\nUser clicks media open. The middle view change to video. User has menu, toolbar and bar on the bottom (what I can not now remember what it was called). \r\n\r\nUser has all needed functions for basic usage on the toolbar. \"Play Media\" \"Play/Pause\", position slider, Fullscreen and volume.\r\n\r\nFrom menu I can hide the toolbar, where is one very important information, the slider. The play/Stop buttons are still possible to get from \"File\" menu. But most used functions can be hided what are on toolbar.\r\n\r\nUser can not get minimalistic, simple and clean player what would allow them to get just player window on the screen with controls. They are forced to have menu (what has video, sound, channels and other controls what are not needed all the time), but they have possiblity to hide the only panel what has the position slider?\r\nWhy not just have option for Ctrl+M and to hide the bottom bar as well? Place the shortcut disabled by defaul?\r\n\r\nJust a picture of the idea.\r\nhttp://img134.imageshack.us/img134/1848/conh.th.jpg\r\n\r\nIMHO Dragonplayer would be very clean, simple and even powerfull then. Not forcing user to have cluttered UI. "
    author: "Fri13"
  - subject: "Condolences"
    date: 2009-09-02
    body: "Indeed. I can't imagine what it must be like for them. Michael, heartfelt condolences!\r\n"
    author: "tangent"
  - subject: "Ctrl+M"
    date: 2009-09-02
    body: "I did hide the menu ones in the beginning, And i search the Internet to get a solution to get is back. So I agree that this option can be confusing sometimes. But now I'm using it because I like a clean interface. I have some ideas maybe. \r\n\r\n1. gave a pop-up that says, You pushed Ctrl+m, this will hide the menu bar, to get is back press Ctrl+m again. (a do not show again option) and allow of cancel buttons.\r\n2. When the menu bar is hiding, maybe a option to temporary get the menu back, when hovering the title-bar for 2 seconds or something like that.\r\n\r\nAnd another idea (completely of-topic i know). I have a idea of a Plasmoids option, For example Amarok. In the past there was a mini player option, a small window with basic functions, it start and stops whey you start and close the program. \r\n\r\nNow we have Plasmoids like playwolf etc. The thing is, the keep running even after closing Amarok, so they are always \"in your face\". I would see a option that the Plasmoids can start when the program starts and stops (and disappears from the desktop) when the program is closed. for programs like Kget and Ktorrent this option will be handy i think. Because I like the Plasmoid option of Ktorrent but don't use it because I don't want it on my desktop all the time.\r\n\r\nPs. I use XBar now for \"hiding the menu\" and i love it."
    author: "dummyxl"
  - subject: "1. gave a pop-up that says,"
    date: 2009-09-02
    body: "<blockquote><i>1. gave a pop-up that says, You pushed Ctrl+m, this will hide the menu bar, to get is back press Ctrl+m again. (a do not show again option) and allow of cancel buttons.\r\n2. When the menu bar is hiding, maybe a option to temporary get the menu back, when hovering the title-bar for 2 seconds or something like that.</i></blockquote>\r\n\r\n+2"
    author: "mxttie"
  - subject: "and then?"
    date: 2009-09-02
    body: "Because if you hide the menu bar... then what? How do you get it back? You need a secret code.\r\n\r\nYou remove the toolbar, you can just use the menu to get it back. Its pretty clear what to do, even if eg your sister did it when using Dragon Player previously. \r\n\r\nPersonally I either watch a video or not. So if I'm out of full screen mode its because I just started or I need to do something like change the subtitle. Or because I'm not even watching it, and its paused. So I don't see the problem.\r\n\r\nRegardless, even if you can educate me on the use-case for a less cluttered Dragon Player, the logic above prevents a ctrl-m feature from ever being in Dragon Player (and makes me question its existence anywhere else)."
    author: "eean"
  - subject: "ah yea"
    date: 2009-09-02
    body: "I've pinged you on IRC with our new email address. Thanks. :)\r\n\r\nControl-m to full screen sounds like a fine idea to me. But maybe its a bit evil, yea."
    author: "eean"
  - subject: "classy move on the"
    date: 2009-09-16
    body: "classy move on the dedication. The KDE community is a pretty awesome thing.\r\n---\r\n<a href=\"http://www.4insure.net\">4insure</a>\r\n"
    author: "auto-insurance"
  - subject: "kmail 4.3.1 extremely slow start"
    date: 2009-09-28
    body: "In reference to previous comments I suffer from this bug\r\n\r\nhttps://bugs.kde.org/show_bug.cgi?id=196949\r\n\r\nas well and think of stopping to use kmail now after eight years of use:\r\nI have about 10.000 emails in my main imap folder (Favorite) and about\r\nthree times that much in imap-folders of previous years. With\r\nthe new grouping feature kmail 4.3.1. cannot be used on my dual-core amd\r\nfor about 5 minutes (!). During that time it pretty much uses up all desktop resources. \r\nI was forced to use kmail 4.3.1  since kmail kde-3.5.10\r\ncrashes in my gentoo box  (kde 4.3.1 jointly installed with kde-3.5.10, amd64 unstable) every time upon sending emails via smtp. "
    author: "rand"
---
KDE has <a href="http://kde.org/announcements/announce-4.3.1.php">released the first update in its 4.3 series</a>.  Bugs have been fixed and translations made more complete.  4.3.1 includes a new Croatian translation.  KMail and KWin have both recieved a lot of fixes and a crash when editing toolbars was solved making this an important update for all.  The <a href="http://kde.org/info/4.3.1.php">release info page</a> has the links for source downloads and information on the distro packages which are currently available.  

We are dedicating this release to the memory of Emma Hope Pyne, baby daughter of KDE developer Michael Pyne.  Emma sadly died last week and our thoughts are with Michael and his family.
