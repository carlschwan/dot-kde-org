---
title: "KDE Commit-Digest for 22nd February 2009"
date:    2009-03-18
authors:
  - "dannya"
slug:    kde-commit-digest-22nd-february-2009
comments:
  - subject: "Thanks danny"
    date: 2009-03-18
    body: "As always, very nice to read the digest.\r\nAlso, good luck with your work :)"
    author: "ianjo"
  - subject: "wahouuuu google sync"
    date: 2009-03-18
    body: "next step calendar and we can share kalendar + kontact with my android phone.\r\n"
    author: "laradji"
  - subject: "K3b!"
    date: 2009-03-18
    body: "Good to hear of progress being made on K3b!  This should be good under KDE 4..."
    author: "phil_"
  - subject: "Great That Things Are Progressing"
    date: 2009-03-19
    body: "But... Don't you think that its kinda sad that I can't login using to a site dedicated to promoting KDE and relaying news about KDE with Konqueror.\r\n\r\nThe web browser is a central application on any modern desktop and Konqueror simply is broken in so many many ways. If I wanted to use gtk based apps I would be using gnome or xfce. But I love KDE and I am really frustrated by Konqueror and the requirement to use Firefox and consequently the gnome plugins that plugin directly.\r\n\r\nPlease put the appropriate focus on KDE/qt based browser. Even it means simply wrapping around gecko and utilizing the KDE's recorded file associations.\r\n\r\nPlease.\r\n"
    author: "eliwap"
  - subject: "Re: Where is the problem?"
    date: 2009-03-19
    body: "Which site are you meaning? If you mean this site, I'm logged in with Konqueror 4.2.1 without any problems."
    author: "majewsky"
  - subject: "Thumbs up for Akonadi"
    date: 2009-03-19
    body: "The google contacts integration is really great!!!\r\nSeems like the framework is progressing nicely... compliments guys ;-)\r\n"
    author: "enricoros"
  - subject: "I agree"
    date: 2009-03-19
    body: "I agree with you. KDE deserve a better browser. Right not I can not use Konqueror to Login to any website. It's a bug. \r\n\r\nI can not use konqueror to chat or writing a rich text format message in GMail."
    author: "zayed"
  - subject: "I am waiting for that too!"
    date: 2009-03-19
    body: "I am waiting for that too!"
    author: "Elv13"
  - subject: "And others..."
    date: 2009-03-20
    body: "And also with the upcoming Palm Pre, and likely other future phones.  I'm not a fan of \"cloud computing\", as I like to have my data on my machines, but cloud syncing makes a heck of a lot of sense and appears to be where tech is going.  These kinds of things really allow KDE to fit into part of the next emerging workflow."
    author: "JabberWokky"
  - subject: "Syncing"
    date: 2009-03-20
    body: "Agreed. It's not really practical for me to insist on being connected to the internet at all moments, so my laptop is the primary repository of my data. So I am very excited about syncing data, particularly calendar data as it will be a major improvement simply in allowing my family to coordinate all of our complicated schedules.\r\n\r\nSince I'm not doing the coding myself, I will wait patiently for this feature. But given the rate at which KDE is improving I bet I won't be waiting long...\r\n\r\nKudos to the developers!\r\n"
    author: "tangent"
  - subject: "nice digest ,is there"
    date: 2009-05-14
    body: "nice digest ,is there anything new about this.\r\n\r\n\r\n\r\n--------------------\r\n<a href=\"http://voucherfrenzy.co.uk/discount-codes/\">voucher codes</a>"
    author: "michalraise"
---
In <a href="http://commit-digest.org/issues/2009-02-22/">this week's KDE Commit-Digest</a>: Experimentation with recording presentations in <a href="http://okular.org/">Okular</a>. Mobipocket format support added to Okular, <a href="http://strigi.sourceforge.net/">Strigi</a>, and the thumbnailer. Ability to configure gestures in the "Hotkeys" KControl module. Start of a metadata editor and other general work in Plasmate. Support for multiple collections, and HTML emails in the LionMail <a href="http://plasma.kde.org/">Plasma</a> widget. A "maintenance" tab, with reorganised status displays and operations added to the folder properties dialog in <a href="http://kontact.kde.org/kmail/">KMail</a>. Initial check-in of Qt QObject - GTK GObject bridge. Support for C# events added to the C# bindings. A simple plugin using WebKit for <a href="http://kommander.kdewebdev.org/">Kommander</a> 4. Rudimentary printing support implemented in <a href="http://www.caffeinated.me.uk/kompare/">Kompare</a>. Charts tables added to Postgres backend in Kamala. More development work in KMJ and the rewrite of Kolf. Zooming, and seeking during video playback now working in <a href="http://kphotoalbum.org/">KPhotoAlbum</a>. Support for changing brightness, contrast, hue, and saturation in LinTV. More work on effects in KWin-Composite. Support for letting the user change KDE mimetype icons. Save report support in the DrKonqi dialog. Improved tab behaviour, and a prototype "Konsole" KPart-based plugin (to provide SSH/Telnet support) in KRDC. SnaKe is renamed KSnake. Initial import of Kopier and Tellico to KDE SVN.

Also in this Digest, Boudewijn Rempt provides an update covering recent developments in Krita, and Adenilson Cavalcanti Da Silva writes an update on the Google Contacts Akonadi Resource. <a href="http://commit-digest.org/issues/2009-02-22/">Read the rest of the Digest here</a>.
<!--break-->
