---
title: "Counting The Days To Akademy 2009"
date:    2009-07-02
authors:
  - "cschumacher"
slug:    counting-days-akademy-2009
comments:
  - subject: "It is really a great"
    date: 2009-07-04
    body: "It is really a great achievement for KDE organization to go Nigeria. It is a starting point for them to make Nigeria more powerful in Africa or in the west by collecting talented skills through open source. \r\n<p>Sophie;<br><a href=\"http://www.k9stud.com\">.</a><a href=\"http://www.k9stud.com/viewalldogs.aspx\">.</a></p>\r\n"
    author: "sophie87"
---
It is that time of the year again. Akademy 2009 is about to begin in only a few days. Held in the <a href="http://www.auditorio-alfredokraus.com/webe/aak.htm">Alfredo Kraus Auditorium</a> close to the beautiful beaches of <a href="http://www.openstreetmap.org/?lat=28.1235&lon=-15.423&zoom=12&layers=B000FTF">Las Palmas de Gran Canaria</a>, this event is shaping up to be a special conference.
<!--break-->
There is an <a href="http://akademy2009.kde.org/conference/program.php">excellent program</a> in place, featuring tracks about Beauty, Business, Community, Methods and Applications. As special highlights there will be keynotes by Glyn Moody talking about how <a href="http://akademy2009.kde.org/conference/presentation/149.php">hackers will save the world</a> and Sebastian Kügler, showing the <a href="http://akademy2009.kde.org/conference/presentation/150.php">Momentum of KDE</a>. Akademy this year is a part of the <a href="http://grancanariadesktopsummit.org">Gran Canaria Desktop Summit</a>, which provides more keynotes, a series of lightning talks, a full cross-desktop track, and the entire program of <a href="http://www.grancanariadesktopsummit.org/node/165">GUADEC</a> for an unprecedented richness of talks about free desktop topics at the same event.

Following the three days of talks and keynotes there will be the general assembly of KDE e.V.. At the same time the more informal part of Akademy starts, consisting of <a href="http://en.wikipedia.org/wiki/Birds_of_a_Feather_(computing)">BoF sessions</a>, intense hacking and plenty of room for inspiring discussions, community building and having some fun. BoF sessions can be initiated by anyone and are planned in the Wiki. Go to the <a href="http://wiki.grancanariadesktopsummit.org/mediawiki/index.php/Akademy_BoFs">Akademy BoF session overview</a> and add your own session there.

For details about <a href="http://akademy2009.kde.org">Akademy 2009</a>, the <a href="http://www.grancanariadesktopsummit.org/node/7">location</a>, the <a href="http://www.grancanariadesktopsummit.org/node/10">schedule</a> and <a href="http://wiki.grancanariadesktopsummit.org/mediawiki/index.php/Main_Page">latest organizational details</a>, see the <a href="http://grancanariadesktopsummit.org">main event site</a>.

See you all on Gran Canaria for an exciting and productive desktop summit.