---
title: "NLUUG Conference on Open Web "
date:    2009-11-01
authors:
  - "jospoortvliet"
slug:    nluug-conference-open-web
comments:
  - subject: "OpenDesktop spreading"
    date: 2009-11-03
    body: "Really nice to see third parties picking up the OpenDesktop API. With the likes of Gluon in the earlier article also leveraging this it has the potential to become very interesting.\r\n\r\nI have beer mug jealousy :-)"
    author: "swjarvis"
---
On October 29th the NLUUG held their second conference this year (the first, held in the spring, focused on file systems). With over 200 visitors and talks by 19 speakers, all prominent in their respective fields, this conference was of particularly high quality. This is surely emphasized by the location and surroundings and the excellent organization. Read on for a short impression on the conference, which was attended by several KDE community members.

For those who don't know, NLUUG is the association of (professional) Open Source and Open Standards users in the Netherlands. Since the late seventies, the NLUUG has brought together the community of systems administrators, programmers, researchers and IP network professionals. The primary goal of the NLUUG is to extend the application of, and knowledge about, open systems and UNIX.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href=" http://dot.kde.org/sites/dot.kde.org/files/steven.jpeg">
<img src="http://dot.kde.org/sites/dot.kde.org/files/steven_small.jpeg" width="400" height="224"/></a><br />
Steven Pemberton during the keynote
</div>

Since 1985 the NLUUG has organized conferences which included many notable highlights, such as the first talk by Linus Torvalds outside of Finland (1993), the first technical Java conference in the Netherlands (1997) and the bi-yearly <a href="http://www.sane.nl/">SANE</a> conferences (1998-2006). Topics in the past have included file systems and storage and virtualization and next year's topic will be system administration (<a href="http://www.nluug.nl/activiteiten/events/vj10/cfp-en.html">call for abstracts & talks</a>). This conference was on The Open Web and started with a keynote about 'Walled Gardens' by <a href="http://homepages.cwi.nl/~steven/">Steven Pemberton</a>. Steven, a researcher at CWI in Amsterdam (Center for Mathematics and Information Technology), has been involved with the web since the 80's.

He spoke about the dangers the web faces. One of the major problems he sees is what he calls 'Walled Gardens' - the lack of connections between various (social) networking sites. You can read more about this topic in the upcoming interview with Steven. Other talks included legal and usability issues with websites and presentations of various technologies like <a href="http://geoclue.freedesktop.org/">GeoClue</a>, <a href="http://en.wikipedia.org/wiki/Comet_(programming)">Comet</a>, <a href="http://ampache.org/">Ampache</a> and <a href="http://www.gnu.org/software/gnash/">Gnash</a>. Abstracts can be found on the <a href="http://www.nluug.nl/activiteiten/events/nj09/index.html">NLUUG site</a> and papers on the various topcis are expected to appear soon.

The conference also presented three talks by KDE community members. Sebastian Kügler spoke about Freeing the Web from the Browser, Frank Karlitschek discussed the Social Desktop and Alexandra Leisse was planning to give a 'Community 101' course but was unable to attend. Your humble writer suddenly had to fill in for her, which was quite a challenge considering he never had seen her talk before...

<h2>Freeing the Web from the Browser</h2>

Sebastian's talk about Freeing the Web from the Browser started with a short recap of how the web currently works. Being designed for desktops or laptops with keyboard and mouse, the web offers challenges on mobile devices with the variety of input devices which come with those.

The Qt and KDE based technologies employed by Project Silk attempt to alleviate these issues and allow web technologies to work better on a large variety of devices like netbooks, phones and TV's and be usable with remote controls, styluses or touch devices. The goal of Project Silk is 'deep integration of online content and communication into the user experience'. More specific:
<ul>
<li>The web as a first-class citizen on the desktop</li>
<li>A better UI for the web</li>
<li>Integration of online content & services in applications</li>
</ul>
Further, Silk attempts to fix accessability and availability of data on the web.

It should be noted that Silk is NOT a new technology or a specific group of people - it is an effort to coordinate various efforts in the web area. Currently, results include Wikipedia search in Plasma, the Selkie standalone webapp, the webslice plasmoid, a video dataengine, a web thumbnailer and integration of Google search.

So what makes a tool or application Silk? Sebas presented the 4-point Silkyness scale (not to be confused with the sillyness scale):
<ul>
<li>Native UI for web content</li>
<li>Integrates data from the web</li>
<li>Web data is cached so the app works off-line</li>
<li>Usable on a wide range of devices (small screen, different input devices etc)</li>
</ul>

Each property is worth one point, and having them all makes your application fully Silky.

<div style="clear:both; padding: 1ex; border: thin solid grey; margin-left:auto; margin-right:auto; margin-top:3ex; margin-bottom:3ex; width:400px;">
<a href="http://dot.kde.org/sites/dot.kde.org/files/KDE ppl.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/KDE ppl_small.jpeg" width="400" height="224" /></a>
KDE at NLUUG
</div>

The Silk applications get their data from various online services. Those should of course be as free as possible - have open specifications, free implementations, good privacy and data integrity protection. But of course plugins can be written for services which do not fully serve the user. Of course the Social Desktop initiative, working on Free webservices and API's provides an excellent complement to Silk technology, giving users more control over the service and their data and a better UI.

Sebas proceeded to demo a few things like Webslice, which embeds an element of a website in a widget. He also showed a webapp-to-desktop tool with extensive integration between the web application and the desktop like notifications and page-specific buttons on the toolbar.

<h2>The Social Desktop Initiative</h2>

Later that day Frank Karlitschek spoke about the <a href="http://www.socialdesktop.org/">Social Desktop</a>. He started by explaining the reasons behind the Social Desktop initiative. The goal is to create something unique and compelling the non-free desktops can't follow, building upon our main strength: the community. The Social Desktop brings cloud computing concepts to the desktop. Users will be able to find each other, contribute and help KDE innovate. Besides the deep integration in the desktop the main difference between other similar developments is openness. The team does not want to create a new Facebook, locking users into one initiative, but to build something where you can own your own data using open protocols and file formats.

Frank demoed the current Social Desktop tools, beginning with the Social Desktop widget which made its appearance in KDE 4.3. This allows you to connect to other people, and thanks to the integration with geo-information, shows you KDE events nearby or see who uses KDE in your neighborhood.

After this he moved on to the upcoming knowledge base widget which helps users to help each other. This is also integrated in the documentation tools in KDE. Usual documentation covers the basics for many users, but not the more advanced things - nor previously unconsidered issues which might arise. Having online knowledge base access built-in will help users find solutions for which they otherwise would have to search on the web by themselves - something not every user finds easy. The tools can even take your hardware into account, showing other users who have the same hardware. More cool features can be found in the about dialog in KDE applications where you can see more personal details on the developers and become a fan of an application (which lets you receive updates on new versions).

Further integration is going on in various applications using the KNX/GetHotNewStuff framework. This framework introduced in KDE over 5 years ago allows users to download 'stuff' right from the application they are working in. Think about things like wallpapers, scripts for Amarok, new stars in KStars or new languages and exercises in Parley. Recent improvements include uploading, downloading, rating and commenting, making the sharing of your creativity much easier.

But also KDevelop, Plasmate and Qt Creator are getting involved so a developer can write an application and share it with the world with just a few mouseclicks. C++ applications get uploaded to the excellent <a href="http://build.opensuse.org/">openSUSE buildservice</a> (which creates binaries for many platforms, including Windows and Mac in the future) and scripted applications become immediately available through OpenDesktop everywhere.

After the demonstration Frank moved on to some more technical details on the infrastructure which is used like the OpenDesktop API. Currently Frederik Gladhorn, Eckhart Wörner and Frank Karlitschek are working on integrating all this functionality into the KDE libraries. They are creating an easy to use API so applications do not have to worry about handling authentication and different service providers.

The presentation ended with a few interesting titbits:

<ul>
<li>All shown features will be in KDE 4.4!</li>
<li>Maemo.org will be an SocialDesktop API provider!!! (using the Open Collaboration Services)</li> 
<li>Gwibber, the GNOME microblogging app supports the SocialDesktop features.</li>
<li>The forum.kde.org team is working on knowledge base support, bringing all the knowledge from the forums to the desktop!</li>
<li>And various open groupware projects are talking with the OpenDesktop team to support the API.</li>
</ul>

<h2>Community building</h2>

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href="http://dot.kde.org/sites/dot.kde.org/files/beermugs.jpg">
<img src="http://dot.kde.org/sites/dot.kde.org/files/beermugs_small.jpeg" width="400" height="225"/></a><br />
Every visitor received a beer mug
</div>

There also was a presentation on Community Building using slides from Alexandra Leisse (who, as mentioned above, was unable to make it to the conference) presented by yours truly. Fortunately, according to Frank (who has seen Alexandra's presentation a few times) it was not unlike her own talk. As part of a vibrant and fast growing community like KDE we often take a nice and open atmosphere for granted - but it takes hard work to build and maintain such a community. It is important to value the whole range of contributions - not just coding but also promotional input, documentation, bug reporting and those helping out others. An often overlooked topic is also the value of personal contact, either by meeting or just keeping each other up to date on what is going on in real life. Building personal relationships is a key part in building an engaging community, even if that sometimes leads to emotional outbursts. It is important to remember those are a sign that people care - and handling them in a proper, respectful and confidential manner is key to strengthening the community.

After the last talk there was time to put some of the above recommendations into practice and have a few beers together. There was also handing out of prizes and some fun. At the end, attendees agreed the topics were interesting and, as cloud computing is an area where the FOSS world lags behind a bit, the discussions about ownership of data, privacy and security will hopefully result in more awareness on the issues with the current 'Walled Gardens'.