---
title: "The Social Desktop Winners"
date:    2009-10-09
authors:
  - "fkarlitschek"
slug:    social-desktop-winners
comments:
  - subject: "Congratulations!"
    date: 2009-10-09
    body: "Congrats to \"our\" Teo, and to the rest as well :)\r\n\r\nAnd thanks to Frank for organizing this!\r\n"
    author: "Mark Kretschmann"
  - subject: "interesting"
    date: 2009-10-09
    body: "the two first places are for kde ideas (sure, they can be copied for gnome too\", interesting since it was a colaboration between kde and gnome (and oo.org and open desktop)"
    author: "Asraniel"
  - subject: "Re: Congratulations!"
    date: 2009-10-09
    body: "BTW, when will the Extended About Dialog replace the old KAboutDialog in kdelibs?"
    author: "majewsky"
  - subject: "As soon as possible"
    date: 2009-10-10
    body: "That's not trivial, it should be nicely integrated into kdelibs without breaking binary compatibility. Right now anybody can just copy over the code if he wishes to use it. For kdelibs, I'd say no sooner than 4.5."
    author: "teo"
  - subject: "Might have been as simple as:"
    date: 2009-10-10
    body: "Might have been as simple as: KDE is easier to adapt to new technologies ;)"
    author: "madman"
  - subject: "Happy to have participated"
    date: 2009-10-13
    body: "I am very happy and proud to have participated as a judge on this contest. I hope we can have more of this and get the social desktop concept running within the FLOSS community as a whole."
    author: "JZA"
---
Today we are announcing the winners of the <a href="http://www.socialdesktop.org/contest/">Social Desktop Contest</a>. The Social Desktop Contest was launched in June with the goal to bring Web 2.0 ideas and our user and developer community closer to the desktop and foster community development and innovations around the <a href="http://www.opendesktop.org/news/index.php/OCS+API+1.3+released?id=325&name=OCS+API+1.3+released">OCS API</a>.

There have been many new ideas and innovations coming from the community in the Social Desktop area and we received a large number of really good submissions. This made it obviously hard for the jury, as you can have only so many winners.

After combining the community votes with the votes from the jury we have 4 winners. We are especially trilled that the winning submission of the contest, the "ExtendedAboutDialog for KDE apps" is not only fully implemented and working, it already ships with Amarok 2.2! So you probably already have it on your hard drive...

So without further delay, we present the winners of the contest:

<b>1st place</b> - "<a href="http://opendesktop.org/content/show.php?content=110614">ExtendedAboutDialog for KDE apps</a>" by Téo Mrnjavac
This is an enhanced KAboutDialog which gets developers' data from openDesktop.org. It enables direct interaction with the development team and can be used in any KDE application.

<i>Téo Mrnjavac has won a Dell Inspiron Mini 10v netbook with Ubuntu.</i>


<b>2nd place</b> - "<a href="http://opendesktop.org/content/show.php?content=110513">Knowledge base widget</a>" by Marco Martin
This is a plasmoid which lets users query an online knowledge base for support without the need to visit a forum or subscribe to a mailinglist. This widget will be part of KDE 4.4.

<i>Marco Martin won an external 1TB hard disk.</i>


<b>3th place</b> - "<a href="http://opendesktop.org/content/show.php?content=107669">libopengdesktop</a>" by Guido Roberto
A simple Glib-oriented library which easy access to Open Collaboration Services providers. Still under heavy development, it will be useful for other people which want to bring the Social Desktop on Gnome and XFCE platforms.

<i>Guido Roberto won an Amazon.com Gift Coupon worth $50.00.</i>


<b>4th place</b> - "<a href="http://opendesktop.org/content/show.php?content=108958">PyContent</a>" by Ni2c2k
A plasma widget written in Python to show the newest contents from specific content categories from a content provider.

<i>Ni2c2k has won an Amazon.com Gift Coupon worth $30.00.</i>

<b>Congratulations to all the winners from the jury members!</b>

- <a href="http://aseigo.blogspot.com">Aaron Seigo</a> from the <a href="http://www.kde.org">KDE</a> community
- <a href="http://tieguy.org/">Luis Villa</a> from the <a href="http://www.gnome.org">GNOME</a> project
- <a href="http://www.alexandrocolorado.com/wordpress/">Alexandro Colorado</a> from <a href="http://www.openoffice.org/">OpenOffice.org</a>
- <a href="http://karlitschek.de/">Frank Karlitschek</a> from <a href="http://www.opendesktop.org">openDesktop.org</a>
