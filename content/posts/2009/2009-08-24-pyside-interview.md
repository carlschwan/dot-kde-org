---
title: "On the PySide - interview"
date:    2009-08-24
authors:
  - "jospoortvliet"
slug:    pyside-interview
comments:
  - subject: "Mixed response"
    date: 2009-08-24
    body: "I don't want to take away from Riverbank's offering, as I've been a fan of their python bindings for quite a while, however I think PySide will sink their product.  It's too bad that no middle ground was found as Riverbank has really been a boon to Qt and KDE over the years.\r\n\r\nI will, of course, be moving to the PySide bindings if/when KDE bindings exist for licensing reasons, but in the meantime, PyQt is not dead.\r\n\r\nCheers"
    author: "troy"
  - subject: "GPL Python modules"
    date: 2009-08-24
    body: "Hi there,\r\n\r\nI somehow didn't think that \"import PyQt\" with the old bindings would force my code unter GPL, does it?\r\n\r\nYours,\r\nKay"
    author: "kayonuk"
  - subject: "PySide shared library sizes"
    date: 2009-08-24
    body: "Currently the total size of the PySide libs for all the Qt modueles is 30 Mb. For just QtCore and QtGui they are 22.5 Mb. These are really high figures, and about twice the size of the existing PyQt libs. They are five or six (!) times larger than the Smoke libraries, which weigh in at just over 5 Mb for all the Qt modules. The Smoke libraries can be used by Ruby, C#, Perl, Common Lisp and PHP, not just a single language too. The large size is caused by the heavy use of C++ templates in Boost::Python. \r\n\r\nQt alone has about 500 classes, whereas the current KDE bindings like Python and Ruby wrap over 1500 classes, which would give a combined shared library size of 90 Mb or so assuming the same size per class as Qt. So as PySide stands, I would personally consider that these figures are just too high.\r\n\r\nThere is a hack in the generate code of doing '#define protected public', to allow protected methods to be called. This certainly won't work on Windows. Fixing it properly will require extra code to be generated to subclass each class with protected methods, and add a public method that calls a corresponding method in the class to be wrapped. This will add some extra code obviously."
    author: "Richard Dale"
  - subject: "I'd tend to agree that using"
    date: 2009-08-24
    body: "I'd tend to agree that using Boost::Python is not a good solution for large bindings. The #define will also as you say cause problems. That said, now the type system files exist I'd guess that addressing these will be something that can be done incrementally.\r\n"
    author: "rich"
  - subject: "About the size and the ugly hack"
    date: 2009-08-25
    body: "The bindings size is due to <strong>heavy</strong> usage of templates in boost::python, sometimes a nonsense template usage IMHO.\r\n\r\nWe avoided to use various boost::python features and wrote our own code because the boost::python feature just caused more code bloat (see how we deal with virtual methods and how boost::python do it for example).\r\n\r\nThe hack about protected/public also was made to try to reduce the library size, and yes, you are right, the hack doesn't work on Windows. But is easy to write code to avoid this ugly hack."
    author: "hugopl"
  - subject: "Qt-Maemo-GTK?"
    date: 2009-08-25
    body: "I'm confused, is Maemo not GTK-based? How do these things fit together?"
    author: "mhermans"
  - subject: "Maemo WAS GTK based, it's"
    date: 2009-08-25
    body: "Maemo WAS GTK based, it's moving to Qt."
    author: "jospoortvliet"
  - subject: "All the more reason to look"
    date: 2009-08-25
    body: "All the more reason to look forward to Nokia's N900, supposedly to be released this year :-)"
    author: "Haakon"
  - subject: "The N900 will be based upon"
    date: 2009-08-25
    body: "The N900 will be based upon the current, GTK based MAEMO stack. Only the one after that will be Qt based, and I think it'll take a while before KDE tech will make it to the phones.\r\n\r\n(personally I think we should focus on integrating KDElibs functionality in Qt as much as possible so any KDE app runs on Qt)"
    author: "jospoortvliet"
  - subject: "Guitar Hero World Tour"
    date: 2009-08-25
    body: "So you OpenBossa devs spend their time playing Guitar Hero World Tour. Have you also tried Frets on Fire?"
    author: "majewsky"
  - subject: "what we need to do"
    date: 2009-08-25
    body: "Is find a company that's a Qt license holder to send Qt Software a requirement that Solid would solve.\r\n\r\n(this was supposed to be a reply to Jos's N900 message)"
    author: "eean"
  - subject: "dunno about that"
    date: 2009-08-27
    body: "For closed source projects I suppose so (if thats even possible...), but rdale makes a good point regarding memory usage. Richard is obviously biased, but he also knows what he's talking about. :)\r\n\r\nI'm glad that they did apparently talk to Riverbank and just didn't forget about them (what I was worried of when I first read the announcement). "
    author: "eean"
  - subject: "yes"
    date: 2009-08-27
    body: "Yes. If you use a GPL library, then your code has to be GPL (something more permissive might be allowed but IANAL)."
    author: "eean"
---
Recently the dot carried an article about <a href="http://dot.kde.org/2009/08/18/pyside-brings-lgpl-qt-python">the first public release of PySide</a>, LGPL python bindings to Qt. We conducted a short interview with one of the people behind <a href="http://www.pyside.org/">PySide</a>, Nokia employee <a href="http://mairas.net/wiki/Main_Page">Matti Airas</a asking a few questions about this initiative.
<!--break-->
<b>Hi Matti! Can you tell us a bit about yourself?</b>

Sure! I'm having the time of my life being able to work in <a href="http://www.nokia.com">Nokia</a>'s <a href="http://maemo.org">Maemo</a> division, surrounded with top-notch talents and a can-do atmosphere. :-) I've been working with the PySide project from the very beginning and am now acting as a "liaison officer" between Nokia and INdT, helping them with the PyMaemo and PySide efforts. I've been a Linux user since, what, 1997 and have contributed to various FOSS projects. I did a PhD on speech processing (go figure!) before joining the Nokia Python team.

<b>When did the PySide project start?</b>

We started the project late last year after examining the alternatives. Since making of the generator system and the basic Qt framework functionality was a non-trivial task, we deemed the project was only now eady to be publicised.

<b>What prompted this project?</b>

We wanted to enable the community to be able to develop Qt software on the Maemo platform with Python and not limit their licensing options. Since this was not possible using the existing Python bindings for Qt, we decided to roll our own. Naturally, it was clear to us from day one that the project could only fly if it was available not only for Maemo but other mainstream platforms as well, and was open-sourced as soon as possible. That's where we are at the moment.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey">
<a href="http://dot.kde.org/sites/dot.kde.org/files/pyside-screenshot-2.png"><img src="http://dot.kde.org/sites/dot.kde.org/files/pyside-screenshot-2.png" width="320" height="240" /></a><br />
Some Python examples running on PySide
</div>

We naturally contacted Riverbank, the creators of the current, GPL licensed Python bindings, during the planning phase as well as later on to see if there would be a possibility to find a common solution to the licensing issue. However, these were private meetings and I hope you understand that we can't go into more details publicly.

<b>Who are working on PySide?</b>

So far the the code has been written by the <a href="http://openbossa.indt.org">OpenBossa</a> developers working at <a href="http://www.indt.org/institutional/index.php">INdT</a> and funded by Nokia. The team will keep on working on the project but would love to see a genuine community project evolve around our contributions.

<b>INdT, OpenBossa?</b>

iINdT is a Brazilian research institute funded by Nokia and specialising in mobile technologies. The OpenBossa Labs is their GNU/Linux development team. The PySide team of OpenBossa has been hacking on FOSS projects such as the Linux, development tools, PyMaemo and PyGtk for a long time before starting the PySide project. Nowadays they excel at playing Guitar Hero World Tour at the Expert level and on their spare time do awesome things with Python and Qt. (Personally, I think they're some of the most talented FOSS hackers I have been able to work with.)

<b>How can people get involved?

We depend on outside contributors on the success of the project. The code is available in <a href="http://qt.gitorious.org/pyside">our Git repository</a> . Information on the mailing list, irc channels and Bugzilla is available at <a href="http://www.pyside.org">the project website</a>.

We would very much like to see active community members take the lead on specific portions of the project such as creating KDE bindings, porting the code from Linux to other platforms, and so on. Contact us on the mailing list if you're interested.

Also, we're happy to receive other contributions ranging from for example scripts and website improvements to major new features in the core codebase.  :-)

<b>What are the plans for the future?</b>

At the moment, we are working hard to iron the last kinks out so that we can make the first stable release. As we also target mobile devices, one of our prime goals is to reduce the memory consumption quite a bit.

Although we're not working within the Qt team, we plan to track their releases quite closely so that the new Qt features will be available for Python as quickly as possible.

One of the bigger things is redesigning some portions of the currently PyQt-compatible API to be more "Pythonic" in nature. As this might require breaking the PyQt-compatibility, the API changes need to be designed very carefully. We intend to adopt a Python Enhancement Proposal (PEP) like process to involve the community in the API design.

Finally, we're obviously aiming to have Python 3 support as well, but we don't yet have specific plans for that. This is one major area where community contributions would be welcome.

<b>Will these Python Qt bindings suffice for PyKDE usage?</b>

Unfortunately, not at the moment. The current PyKDE binding use SIP (the same binding technology as PyQt), and the binding technologies can't be mixed. However, it would be possible to generate the PyKDE bindings using the generator tools offered by the PySide project, but whether that will be done depends on the kde-bindings team.

<b>How have the responses from the community been so far?</b>

The initial reaction has been quite astonishing. We have received a huge amount of publicity, and the comments have been almost exclusively positive. The site has gotten more than a fair amount of traffic, and already on the first day the project was packaged for two new Linux distros and the PyQt examples were ported for PySide. We definitely feel we're doing something right here!