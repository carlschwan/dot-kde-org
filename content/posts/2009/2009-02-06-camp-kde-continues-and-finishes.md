---
title: "Camp KDE Continues And Finishes"
date:    2009-02-06
authors:
  - "jospoortvliet"
slug:    camp-kde-continues-and-finishes
---

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jos/campkde/model-view lecture.png"><img src="http://static.kdenews.org/jos/campkde/model-view lecture_thumb.jpeg" width="400" height="266" alt="photo" /></a><br />
Our models doing their thing
</div>

<a href="http://camp.kde.org/">Camp KDE</a>, the KDE community event of North and South America, has finished. Similar to the European KDE meeting, Akademy, the first two days were based around a series of talks on various topics. After that we moved towards <a href="http://en.wikipedia.org/wiki/Birds_of_a_Feather_%28computing%29">BOF</a> sessions, local discussions and programming. We had a trip to the Appleton Estate, visited Rick's café and had a lot of fun. The following article details some of the things that kept us busy.

KDAB and Nokia sponsored Till Adam to give a course in advanced Qt development using the official training material from Qt Software. KDAB is a Qt Software partner and has been providing official Qt training since Trolltech was a small start-up.

KDE developers proposed topics they had an interest in learning about. Till then collected the materials and prepared his course. Starting with generic Model/View programming, Ade and Sebas were used as "model" and "view". Later on, Jeff Michell and Leo Franchi joined, and they played a funny "I want the data from row 2 column 1" game. Who said coding was boring?

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 200px">
<a href="http://static.kdenews.org/jos/campkde/ade-cookies.jpg"><img src="http://static.kdenews.org/jos/campkde/ade-cookies_thumb.jpeg" width="200" height="150" alt="photo" /></a><br />
Cookies from Ade
</div>

Like Sunday, Mrs. Whyte from Sweet Spice provided us with an excellent lunch consisting of fruit, stewed beef, chicken, curried goat, banana bread, rice and vegetable soup. Eating this from the balcony, with a view over the beach and the sea, you can imagine it was hard for some to resist the urge to swim. After the lunch and swim, Till continued his course. He had given everybody some exercises, and discussed the results. More talk and homework followed, culminating in a solid learning experience. You could almost see the developers get smarter.

Around 4 the discussions came to a grinding halt as Adriaan and Romain brought cookies. Everybody jumped up, grabbed some and enjoyed them. Ade knows how to keep us motivated. One might wonder why he brought a whip to Akademy and baked cookies at both American events, though. Maybe he is prompted by some cultural difference between both sides of the ocean?

In the evening it became a bit windy (which didn't keep anyone from swimming). Some went into town for food, while others walked along the beach, or ate at the hotel restaurant. As with previous days there were lots of real Jamaican goodies, and it was a beautiful night of writing code, talking, and just hanging out.

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jos/campkde/till_towel.jpeg"><img src="http://static.kdenews.org/jos/campkde/till_towel_thumb.jpeg" width="400" height="266" alt="photo" /></a><br />
Our teacher also had a swim
</div>

Tuesday and Wednesday were a lot like Monday - Till taught some more while many developers also spent their time around the beach, swimming and coding. We had a group photo in the morning, a great lunch at noon, and in the evening everyone went out in various directions for food. Wednesday afternoon a group of developers went to <a href="http://www.rickscafejamaica.com/">Rick's café</a> and the lighthouse at the far west end of the island. At Rick's café some brave men jumped from a huge cliff - Jeff and Gökmen dared to, the others proved to be too chicken. We had dinner there, visited the lighthouse at the far end of the Island and went back to the hotel.

On Thursday 16 developers were squeezed in a small bus. Apparently 2 more people were supposed to join - we couldn't imagine how they would fit, but we later passed another van which proved us wrong. At least twice as many people in a smaller van - no problem, mon, this is Jamaica!

It was quite a long ride, but enjoyable – as soon as we got used to the driving. Our driver turned out to be on the same racing team as Dmitri, one of the Camp KDE organizers. Well, it did show. As <a href= “http://wadejolson.wordpress.com/2009/01/27/camp-kde-2009-final-two-days/ ”>Wade</a> mentioned – it took some getting used to.

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jos/campkde/donkeys.jpg"><img src="http://static.kdenews.org/jos/campkde/donkeys_thumb.jpeg" width="400" height="299" alt="photo" /></a><br />
These two donkeys were used to get the juice from the sugarcanes
</div>

After half an hour, Dmitri and our driver Granville had to ask somebody in a small local place where the Appleton rum factory was. Unfortunately she pointed us in the wrong direction, but as Roger noted, the supermarket there was probably the place <em>she</em> got her rum... We quickly went back and moved on through the beauty that is Jamaica and arrived at the Appleton Estate. We had an interesting tour. We tasted some stuff on the way to becoming rum - first the juice from sugarcanes, provided by Chani and Ade who played donkey for a while. Then we tried the first wet stage of the process, which is sugar and molasses before they are separated, it looked rather like donkey sh*t but tasted pretty OK. And finally we got a taste of the real deal, in its many varieties, and many brought some back to the hotel. To be consumed later that night, of course... We had a decent lunch, took some more pictures, and left.

On the way back we refreshed ourselves with coconuts from a street vendor just outside of Bamboo Holland. We also passed the Sweet Spice restaurant where our lunches were prepared, on the outskirts of Negril. The evening was spent, as usual, with a mix of swimming, coding and overall, enjoying Jamaica.

Besides the many blogs on <a href="http://planet.kde.org">the Planet</a> you can find links to more pictures of what happened in Jamaica aggregated on <a href="http://techbase.kde.org/Events/CampKDE/2009">TechBase</a>.

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 200px">
<img src="http://static.kdenews.org/jos/campkde/not-yet-rum.jpg" width="200" height="150" alt="photo" /><br />
Rum when it's not yet rum
</div>

<h2>Results</h2>

Now the first Camp KDE meeting has ended about two weeks ago, and it is time to count our losses. Or actually, our wins. The event has certainly had a great deal of influence on the community in North- and South America. But the effects of the ideas discussed and the conclusions reached will ripple out through the wider community.

The community has always been the strength of KDE. After all, KDE <em>is</em> the community. The open and relaxed atmosphere, the enthusiasm and mutual cooperation towards a common goal, these are strong signs of a healthy community. An event like this brings these values out in the open, and shows everyone at the conference, but also those back at home, what it is to be a great community. Of course, the relaxation had much to do with the location as well. I'm obviously talking about the weather and beach here - it's stunningly beautiful. All day you could find people just staring away, enjoying the sight of sun, beach, palm trees and the sea. Only to regain consciousness when their compilation was finished...

<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jos/campkde/coco-nuts.jpg"><img src="http://static.kdenews.org/jos/campkde/coco-nuts_thumb.jpeg" width="400" height="300" alt="photo" /></a><br />
Coconuts! Oh boy, did we struggle...
</div>

Despite the tempting water, and giving in to that temptation, work has been done. Not just in terms of code. Listening in to the various groups of developers sitting around you could hear sometimes heated discussions about the design of the applications, frameworks and libraries. Here are some of the more tangible results which came from this meeting.

<ul>
<li> The two Pardus developers Gökmen and Gökcen got a lot of inspiration and worked on further integration between KDE and their distribution. Gökmen wrote a Plasma widget which connected to the native Pardus network management tool. Even the Plasma developers were impressed when they saw the work with Plasma being done in Pardus. But the Turkish developers are also contemplating cooperation with the KDE network manager Plasma integration efforts.</li>
<li> A sudden scream from Sebas marked the point that Plasma showed data from Akonadi for the first time. He had been working on that for a couple of days, with help and inspiration from Till.</li>
<li> While drinking rum Holger Schröder convinced <a href="http://kdab.com/">KDAB</a> emplyee Romain Pokrzywka to join the kde-on-windows team. We're expecting packages and/or code from him soon...</li>
<li> Wednesday Ade brought cookies again - oatmeal and M&M's. Pretty good!</li>
<li> Talks about a next Camp KDE have begun - currently, it seems Mexico and Brazil are the contenders.</li>
</ul>
<div style="float: right; border: thin solid grey; margin: 1ex; padding: 1ex; width: 400px">
<a href="http://static.kdenews.org/jos/campkde/group-foto.jpg"><img src="http://static.kdenews.org/jos/campkde/group-foto_thumb.jpeg" width="400" height="266" alt="photo" /></a><br />
Some had left already, but here you go - a bunch of KDE people!
</div>
<ul>
<li> The Jamaicans have decided there is a need for a Jamaican/Carribbean local KDE group. Guillermo thinks Mexico needs one, too.</li>
<li> The <a href="http://neetja.com">Negril Educational Environmental Trust</a> will soon receive another 300 computers, and is working with some KDE people in Jamaica to load them with Linux and of course KDE.</li>
<li> And there was the news about the work on the next generation KDE/Kontact Kolab client, based on Akonadi we mentioned in <a href="http://dot.kde.org/2009/01/23/camp-kde-saturday-talks">a previous article</a>.</li>
</ul>

In the end the conclusion can only be that we had a very successful Camp KDE. It has been said many times during the conference: "this needs to happen again next year!"

On Friday, after a lot of hugging, most developers went home, back to often cold and much darker places. But they will surely bring a sunny, warm, Jamaican attitude and a lot of great memories with them. I am sure this event will leave all of us inspired for a long time. Jamaica is awesome and has set the bar for future events to try and beat!