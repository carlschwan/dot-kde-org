---
title: "KOffice 2.0 Beta 7 Released"
date:    2009-03-05
authors:
  - "ingwa"
slug:    koffice-20-beta-7-released
comments:
  - subject: "Looking Good"
    date: 2009-03-06
    body: "This is starting to look good. :) \r\n\r\nCan't wait for final release, anything to get me away from OpenOffice would be great. Much Appreciated. Please don't disappoint, many people don't like OpenOffice and this could be there answer. \r\n"
    author: "Dekkon"
  - subject: "don't expect KOffice 2.0 to"
    date: 2009-03-06
    body: "don't expect KOffice 2.0 to replace OpenOffice. It doesn't strive to, and won't be able to. The goal is to have it stable so ppl can test it, and to have it's infrastructure in place so ppl can start contributing plugins and other content."
    author: "jospoortvliet"
  - subject: "Few features"
    date: 2009-03-06
    body: "I am missing few features what are important in visual/usability view. Currently (beta6) KWord includes \"Display mode\", but it does not show anything. I hope there comes modes what allow this kind view...\r\n\r\nhttp://kde-apps.org/content/preview.php?preview=2&id=32145&file1=32145-1.jpg&file2=32145-2.jpg&file3=&name=Kword:+sheet+of+paper+shadow+[Mockup]\r\n\r\nIt should have big space between edges and other sheets, so all sheets would look like they would be on desk. All other office programs can do this but KWord and I have waited this few years :-D\r\n\r\nAnd I am waiting that Mandriva Coover would get beta7 on it. And I am too waiting that I can replace OpenOffice with KOffice. Because this has much better UI than OO or even MS Office 2007. Just so marvelous UI! Kudos for that!"
    author: "Fri13"
  - subject: "display mode"
    date: 2009-03-06
    body: "You talk about two different things in your comment. First is the display mode sub-menu.  In KOffice1 we had 3 display modes, which we plan to add in KOffice2, but not in 2.0.\r\nI'll remove the menu for now...\r\n\r\nThe second thing is a certain display mode. When I follow the link all I see is that the page is centered. Which is something KWord has had for various years now. Even in my KWord1.6 thats what it looks like, although in 1.x you need to change zoom to 100% to get that look. In 2.0 it will look like that even in the page-width zoom mode.\r\n\r\nMaybe I'm missing the point, but as far as I know the feature you want is already there."
    author: "zander"
  - subject: "\"Tight\" vs \"Natural\", a new default view."
    date: 2009-03-07
    body: "Yes, the sheet is already center of the view, what I suggested for 1.x series and it got implented. But still there is not enough space between sheets, what makes it harder to try to look how they would look alone printed, when you have graphic on top and bottom of sheet. There is only couple millimeters space and for eye, it is almost one long sheet when zoomed out littlebit.\r\n\r\nhttp://img129.imageshack.us/img129/3250/kword.jpg\r\n\r\nYou can notice the difference on between left (current) and right (suggested). \r\n\r\nI would suggest that there would be this new for view (if possible?), other as \"tight\" (current) and this as \"natural\" (suggested, default), so you can select wanted view for netbooks and for larger monitors etc. Not for 2.0, but at least for later ones. \r\n\r\nThe shadow is not a must, but it would just pop-up the sheet when having white/light background on KWord. The shadow should be smooth, and not just one colored, so it might not be so easy to implent for \"natural\" view. I believe that is harder to do.\r\n\r\nThese are just few things what can make the user to see application as \"richer\" than other, when it is pleasing the eye visually as well. \r\n\r\nMayby later the space could be configured from options, but it should be set so that space between first sheet and the top of edge is same as space between two sheets. \r\n\r\n"
    author: "Fri13"
  - subject: "Then why don't we wait until"
    date: 2009-03-07
    body: "Then why don't we wait until KOffice is stable and tested before we release as so? \r\n\r\nWhats with open-source developers and there \"release buggy, and release often\", it's ridiculous in my opinion. You don't have a have a official release on there to have people test and make plugins, this is exactly what happened with KDE 4.0. \r\n\r\nSorry if I'm a little harsh, but in my opinion its not the way to go. \r\n\r\n*Start flames here. :("
    author: "Dekkon"
  - subject: "Otherwise it's \"release"
    date: 2009-03-07
    body: "Otherwise it's \"release bugfree, release never\".\r\nIt's a simple matter of a handful of developer(s) not being able to find all the bugs that a userbase of much more people can.\r\n\r\nWithout users you cannot tell if you application has bugs beyond what you encounter. Without software releases you cannot have users who can give you bug reports. Otherwise the software just goes stagnant since nobody's using it.\r\n\r\nPlus you could also just not use the software until it reaches your required level of stability. If they were following your \"release only when not so buggy\" you would even know about it until that point anyway, right?\r\n\r\nPlus, the KOffice crew (and the Digikam crew for that matter) have given KOffice2 an extensive alpha/beta period spanning over several months. There has been a good deal of time dedicated to Quality Assurance, so things shouldn't be too horrible. Though since its a first release of a new codebase it can't be entirely bugfree."
    author: "JontheEchidna"
  - subject: "What's stopping me from using koffice 2.0 beta"
    date: 2009-03-07
    body: "Don't mind, but the UGLY looking fonts (all fonts) in Koffice applications (kword, kspread, etc.) are blurred and more of an displeasure/discomfort to use kword, etc. hence visually annoying.\r\n\r\nIf fonts in koffice 2.0 betas were to look normal as in all kde 4.2.1 applications, desktop, openoffice, etc. then i'm glad to use and report bugs if found.\r\n\r\nbtw, is there a way to fix the ugliness of the fonts in kword?"
    author: "fast_rizwaan"
  - subject: "Speaking only for myself, I'm"
    date: 2009-03-07
    body: "Speaking only for myself, I'm more comfortable with KOffice 2 Beta UIs than I am with OOo.  I really like the work you guys are doing.  I will gladly give up on some features in the spreadsheet and some M$ compatibility in exchange for a smooth and logical UI.  I simply like working with software that I like working with, so for me, replacing OOo will be automatic with a certain level of KOffice 2 crash resistance.\r\n\r\nBTW, I absolutely think the KOffice team is on the right track with their dev priorities and plans though.  I fully support but moreover appreciate the team and roadmap.  I simply look forward to 2.0 as something basically usable, even if feature or plug-in sparse.\r\n\r\nPlease, keep up the great work."
    author: "Jerzy Bischoff"
  - subject: "fonts"
    date: 2009-03-07
    body: "You could use a different distro that sets up the fonts like you want to. I suspect its mostly about anti aliasing settings in freetype. Which is part of your system, not KOffice."
    author: "zander"
  - subject: "yea I've always been a fan of the KWord"
    date: 2009-03-07
    body: "UI. It makes sense to keep things desktop-publishing like even for a document writer. \r\n\r\nAnd Krita is just a lot of fun. :) And obviously doesn't have an equal in the OpenOffice suite."
    author: "eean"
  - subject: "Can grab it on OS X too"
    date: 2009-03-07
    body: "Updated macports to beta 7  <a href=\"http://trac.macports.org/changeset/47839\">just now</a>\r\nEnjoy. "
    author: "illogic-al"
  - subject: "fonts"
    date: 2009-03-08
    body: "Please have a look at font issue, i compiled koffice 2.0beta7 from sources and with freetype 2 Bytecode interpreter enabled.\r\n\r\nPlease observe the text in kate is nicer than kword. thanks\r\n\r\nattachment:\r\nhttp://img105.imageshack.us/img105/7959/fontbugkword20beta7.jpg"
    author: "fast_rizwaan"
  - subject: "Textflow"
    date: 2009-03-09
    body: "Is it possible to make the text flow _around_ a shape, on both sides?"
    author: "axeloi"
  - subject: "Because this attitude leads"
    date: 2009-03-09
    body: "Because this attitude leads to where Enlightenment 0.17 is now: Endless beta without any chance to ever become stable, stretched developer capacities because 0.16 has to be maintained at the same time, and NO USERS."
    author: "KAMiKAZOW"
  - subject: "Awesome. Is this going to"
    date: 2009-03-09
    body: "Awesome. Is this going to make it into a kde4 mac package? I'm thinking http://mac.kde.org ? I have mac ports installed but I have kde 4.1.2 installed via packages, so I'm wondering if this will install? I was advised not to install kde 4.2 via macports because it might conflict with the packages. Would it be problematic to install koffice via macports even though I have kde from packages?\r\n\r\nKeep up the good work koffice team! I'll try it out on ubuntu until I can get my mac packages sorted.\r\n\r\nCheers\r\n\r\njosh\r\nhttp://tucson-labs.com"
    author: "yoshu"
  - subject: "Ehm, doesn't it do that by"
    date: 2009-03-10
    body: "Ehm, doesn't it do that by default? Should be possible, there are text-flow-settings."
    author: "jospoortvliet"
  - subject: "Can't do it... there's only a"
    date: 2009-03-10
    body: "Can't do it... there's only a left or right side option. No both side option..."
    author: "axeloi"
  - subject: "you're right, this is clearly"
    date: 2009-03-11
    body: "you're right, this is clearly a missing feature. Well, that'll probably be something for KOffice 2.1..."
    author: "jospoortvliet"
  - subject: "Great"
    date: 2009-03-12
    body: "Just compiled it and it seems to be shaping up rather nicely ;). Congratulations!\r\nI was actually intending to make Mandriva 2009.0 packages, but my file list was outdated :(. However, the next release will be packaged by me (I hope).\r\n\r\nI still experience major compatibility issues with OOo (in .odt using a page end in kword just throws various lines randomly throughout the doc in OOo). I'm not sure who's fault that is.\r\n\r\nBut in the end: Great concept"
    author: "Typhoner"
---
The KOffice developers have released their seventh beta for KOffice 2.0. This release may be the last of the many betas. A decision on whether there will be another beta or if the next version will be the first Release Candidates will be made next week.

The <a href="http://www.koffice.org/announcements/changelog-2.0-beta7.php">list of changes</a> is longer than ever. For this release we have concentrated on crashes, data loss bugs and ODF saving and loading. Take a look at the <a href="http://www.koffice.org/announcements/announce-2.0beta7.php">full announcement</a> to find out more, or look at the <a href="http://www.koffice.org/announcements/changelog-2.0-beta7.php">changelog</a> for the details.
<!--break-->
Here are a few highlights of this release:<ul>
<li>Crashes fixed in all applications</li>
<li>Fix loading and saving of shapes in the text shape (KWord)</li>
<li>Fixed an important cell edit bug in KSpread</li>
<li>Use the format of a master page when using a layout in KPresenter</li>
<li>Many bugs fixed in the chart shape</li>
<li>Also many bugs fixed in Krita</li>
<li>Improved support for SVG in Karbon</li>
</ul>

Remember that now it is more important than ever to test it and report bugs. If no new severe bugs are found, the next version of KOffice 2.0 will be a release candidate.

<center><a href="http://dot.kde.org/sites/default/files/koffice20beta7.png"><img src="http://dot.kde.org/sites/default/files/koffice20beta7t.png"></a></center>