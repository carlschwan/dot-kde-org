---
title: "KDE Software Compilation 4.3.4 Released: Codename \"Cold\""
date:    2009-12-01
authors:
  - "sebas"
slug:    kde-software-compilation-434-released-codename-cold
comments:
  - subject: "KDE SC 4.4 Beta 1"
    date: 2009-12-01
    body: "Techbase says it's coming today. I assume it's delayed by two days?"
    author: "d2kx"
  - subject: "About plasma cache bug"
    date: 2009-12-01
    body: "Ive readed that, now, when this bug is resolved, plasma becomes much faster. Is this true? Will i see the performance boost? And in what areas i should see performance improvements?\r\nThank you"
    author: "Nece228"
  - subject: "IIRC"
    date: 2009-12-01
    body: "The beta 1 has been delayed some days because of buildsystem issues."
    author: "majewsky"
  - subject: "Missing tarballs"
    date: 2009-12-01
    body: "Is it only me or is the tarballs for the 4.3.4 release missing?"
    author: "tnyblom"
  - subject: "nope ..."
    date: 2009-12-02
    body: "Something went wrong syncing the mirrors, dirk had to kick the sync process once more, but all should be in place now."
    author: "sebas"
  - subject: "The changes sound great -"
    date: 2009-12-02
    body: "The changes sound great - many thanks for the update! \r\n\r\nWhat I kinda missed is having the short list of improvements here in the dot article, so we can comment them directly. \r\n"
    author: "ArneBab"
---
KDE SC 4.3.4's codename "Cold" refers to the feature freeze, KDE's development tree is currently in. This Thursday will see the first beta of KDE SC 4.4, bringing significant improvements to the KDE development platform, the Plasma desktop and the individual applications shipped with KDE SC 4.4.
The current release is one of those monthly releases the KDE team ships to provide the users with bugfixes to the stable series. KDE SC 4.3.4 will most likely be the last release of the 4.3 series as KDE has now entered the stabilization period for 4.4, which is to be released in early February 2010. All hands are needed on deck to make KDE SC 4.4 a stable successor to the 4.3 series. Head over to the <a href="http://www.kde.org/announcements/announce-4.3.4.php">official announcement</a> or the (probably incomplete) <a href="http://www.kde.org/announcements/changelogs/changelog4_3_3to4_3_4.php">changelog</a> for further information.