---
title: "Impression of Gran Canaria Desktop Summit "
date:    2009-07-10
authors:
  - "jospoortvliet"
slug:    impression-gran-canaria-desktop-summit
comments:
  - subject: "where does it end?"
    date: 2009-07-14
    body: ">Many developers have therefore expressed interest in having more Free >Software communities joining us next year, such as LXDE, XFCE and >Android. \r\n\r\nThrow in the Moblin folks, E17 and then how could you refuse if the folks from Workbench,OpenWindows,Ambient,Mezzo, CDE,ROX,IRIX,Project Looking Glass,XFast,\u00c9toil\u00e9 and UDE  as well and you got a A LOT OF PEOPLE.\r\n\r\nIts not a KDE developers summit anymore, its a free desktop summit.\r\n\r\nDont get me wrong, I dont think these two events are mutually exclusive and I think the goals of Gran Canaria are laudable but the slippery slope of having 2,4,6 different desktops is where do you draw the line?\r\n\r\n"
    author: "zeke123"
---
Many of you who did not come to Akademy might be wondering how this meeting compares to the previous years' conferences. After all, aside from being in a sunny location, there are many different animals around here besides the usual trolls and dragons. Gnomes might be small, but they are noticable and they certainly make for interesting company. Read on for an impresson of Akademy at the Gran Canaria Desktop Summit.

<div style="border: thin solid grey; padding: 1ex; width: 429px; text-align: center; margin-left: auto; margin-right: auto">
<a href="http://people.ubuntu.com/~jriddell/akademy/akademy-2009-group-photo.html"><img src="http://dot.kde.org/sites/dot.kde.org/files/akademy-2009-group-photo-wee.jpg" width="429" height="181" /></a><br />
<a href="http://people.ubuntu.com/~jriddell/akademy/akademy-2009-group-photo.html">The Akademy 2009 Group Photo</a>
</div>
<!--break-->
In some ways, Akademy here at the Gran Canaria Desktop Summit is like any of the previous Akademy meetings. It is far less intimate than the many smaller developer sprints, but does present an interesting opportunity to talk to people outside of your usual team. This can solve issues you usually would have a hard time even finding, and can present interesting new venues for cooperation. It is a great way to meet many of the friends you made online or in previous meetings, have beer together or talk about the tougher issues which are too complicated or sensitive to be discussed online. The only (little) disadvantage of the cross-desktop meeting might be the slight loss of the sense of being here with a large group of people from the same project. At Akademy you usually bump into people at random and you can ask them what cool KDE thing they are working on. Now you have to adjust your question a little, to ask for Free Desktop technologies.  There are tensions too admittedly, not everyone was appreciative of our primary sponsor, but the conference has acted as a real focus to break down such attitudes.

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 1ex; width: 260px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/teamwork.jpg" width="260" height="180" /><br />
Cross-desktop meeting.
</div>

There is a serious plus in being at <em>the</em> desktop conference of the year. The <a href="http://dot.kde.org/2009/07/09/cooperation-during-gran-canaria-desktop-summit">article</a> about cross-desktop collaboration might already have hinted at some of the advantages of this joint meeting. Many of the get-togethers and talks about collaboration have yet to bear fruit - only time will tell which ones will turn up interesting results and which ones will fade away. The seeds have been planted, and there is room for more. Most developers you speak with here will either like the joint meeting because of the opportunities they see or not mind it much because it does not really touch them. Negative voices are few and far between, at least on the KDE side, and even those who do not value the cross-desktop cooperation see advantages in this larger meeting.

One of those advantages is the number of teams and talks about topics you usually do not encounter at Akademy. Maemo would most likely have been at Akademy next year, but the new approaches to user interfaces by the Moblin team or the work of the Zeitgeist and GNOME shell developers offers interesting perspectives to graphical and technical design. Many developers have therefore expressed interest in having more Free Software communities joining us next year, such as <a href="http://www.lxde.org">LXDE</a>, <a href="http://www.xfce.org">XFCE</a> and <a href="http://code.google.com/android">Android</a>. There is a strong feeling of being at a historical event, possibly even a turning point in the development of the Free Desktop. We had many more parties than usual at Akademy... four of them might actually be a bit too much to handle for some. It also cuts in the time for intimate dinners which are often a great way of getting to know people. Quite a few developers did not even attend the last party on Wednesday, tired as they were after all those days of hard work, or still busy with hacking and writing code.

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 200px">
<img src="http://dot.kde.org/sites/dot.kde.org/files/voting.JPG" width="200" height="260" /><br />
The voting at the KDE e.V. meeting.
</div>

Of course this is really an Akademy meeting. We discuss here topics we need to do to drive our community and products further. Yes, this includes the work by Adriaan and his motivational whip, but also talks about infrastructure (<a href="http://amarok.kde.org/blog/archives/1070-More-Info-on-Gitorious.org.html">Jeff's blog</a> about the Git discussion is worth a read), marketing, bug-squashing and community work. There is a lot of coding going on, and we can expect to surpass the <b>one million commits to the KDE SVN server</b> during this week! We also had Aaron Seigo join us remotely - getting up early to be able to say 'hi'. He 'attended' a Plasma/KWin BoF and was even present in a KDE hacking room for a short while.

The Spanish KDE and GNOME communities are also proving to be very engaged when it comes to Free Software. Many members of the Spanish community have traveled to Gran Canaria to join this meeting and there is a full track of talks in Spanish. This is not only proof of the value of a joint conference but also of the dedication and strength of the local FOSS community. While the conference was pretty much over on Friday, on Saturday morning you could still find Spanish KDE and GNOME community members attending talks!

For a first try, this meeting certainly does a great job at bringing people together. And suggestions for improvements are floating around. The joint Free Desktop tracks sure were interesting, but having a joint BoF track is seen as very valuable as well.  Videos are due online early next week.