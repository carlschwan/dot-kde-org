---
title: "KDE Commit-Digest for 18th January 2009"
date:    2009-01-30
authors:
  - "dannya"
slug:    kde-commit-digest-18th-january-2009
comments:
  - subject: "Keep up the good work!"
    date: 2009-01-30
    body: "So let me be the first to say.. Keep up the good work! =)"
    author: "edulix"
  - subject: "Git"
    date: 2009-01-30
    body: "Some months ago, there were talks about moving the repository from Subversion to Git. What is going on there? I personally think that's a good idea, it would probably allow greater flexibility for all contributors."
    author: "SP"
  - subject: "Git"
    date: 2009-01-30
    body: "AFAIK there are just a few people intrested but the amount of work needs to be done for this is huge.so the answer is lack of manpower in this area.but many projects are using git-svn.(i repeat that was just AFAIK)\r\n"
    author: "emilsedgh"
  - subject: "inline images in kmail"
    date: 2009-01-30
    body: "... are finally there ! Thank you very much :)"
    author: "moltonel"
  - subject: "Jippii!"
    date: 2009-01-30
    body: "Yes, that might be feature what I have waited most arrive to KMail. Mayby since when I started to use KDE daily on my own computer (3.0.1)\r\n\r\nHopefully we can create soon as good looking HTML emails as on Apple's Mail (or was it iMail... who cares if it is not KMail ;-)"
    author: "Fri13"
  - subject: "Uh! Maybe we can, but that"
    date: 2009-01-30
    body: "Uh! Maybe we can, but that doesn't mean we should: I highly dislike the, \"templates for morons\" way iSomethings make things for users. iWeb is already useless for what I want, and KDEnlive is easily on par with iMovie."
    author: "madman"
  - subject: "I love the digest"
    date: 2009-01-30
    body: "I love the digest-- Thank you very much."
    author: "Shade"
  - subject: "next move.."
    date: 2009-01-31
    body: "The next move will be really cool if kmail can reply to HTML emails with the original HTML format. That's the only feature that i'm missing in Kmail comparing to Thunderbird.. "
    author: "alexismedina"
  - subject: "possible problems with git"
    date: 2009-02-02
    body: "if i'm not mistaken, git also requires either creating a subrepo for every project, or for every interested person to check out everything, as it does not support partial subtree checkouts (using svn terminology here).\r\nfor such a huge repository as kde it seems like a serious drawback.\r\nwhile i like the idea behind the git a lot, there's also binary file changes bloating checkout sizes - something that projects like linux kernel cares little about, but kde has more of images, sounds etc, that could increase checkout size noticeably over time."
    author: "richlv"
  - subject: "Kudos to KMail developers!!"
    date: 2009-02-03
    body: "KMail starting from 4.2 is gaining steam again, and with KAddressBook they are a wonderful PIM solution! Great work!"
    author: "Vide"
  - subject: "Ok..."
    date: 2009-02-06
    body: "You're living a fantasy... KDenlive is no where near iMovie '09.\r\n\r\nThat said, KDenlive is an amazing project, and it's improving fast.\r\n\r\nPlus, there is people who prefer to be call \"Moron\" by those like you than learning how to do everything manually (and they're right soo...).\r\n\r\nI clicked directly on \"login\" in the comment I tried to anwser to, yet, once I logged, it send me to the message composer but in another level, ugly bug XD :-("
    author: "augustofretes"
---
In <a href="http://commit-digest.org/issues/2009-01-18/">this week's KDE Commit-Digest</a>: A new "Crystal Desktop Search" Plasmoid, allowing searching through <a href="http://nepomuk.kde.org/">NEPOMUK</a> indexes (and MediaWiki-based websites). Support for "grep-like behaviour" in the "FileWatcher" <a href="http://plasma.kde.org/">Plasma</a> applet, and support for custom server addresses (aka. backend locations) for the "Pastebin" applet. Further developments in the "System Load Viewer" (which moves to kdereview for KDE 4.3) and "Video Player" applets. An option to show Plasma panels during active KWin-Composite window switching effects, and a configuration dialog for changing the animation duration for the "Magic Lamp" effect. Work on Replay Gain support, and "basic playlist sorting" in <a href="http://amarok.kde.org/">Amarok</a> 2. Inline image support for HTML messages in <a href="http://kontact.kde.org/kmail/">KMail</a>. Porting to <a href="http://pim.kde.org/akonadi/">Akonadi</a> in <a href="http://www.mailody.net/">Mailody</a>. First version of a new KContactManager editor. Various fixes in <a href="http://pim.kde.org/components/kpilot.php">KPilot</a>. Continued development in <a href="http://www.kdevelop.org/">KDevelop</a> and KNetworkManager. Support for rectangular and elliptical field of view, and comet magnitudes in <a href="http://edu.kde.org/kstars/">KStars</a>, with optional integration with XPlanet for displaying planets. Support for the "Sonic Screwdriver" in the Killbots game. Ability to import Pidgin chat logs into <a href="http://kopete.kde.org/">Kopete</a>. A brand new spray brush for <a href="http://www.koffice.org/krita/">Krita</a>. <a href="http://commit-digest.org/issues/2009-01-18/">Read the rest of the Digest here</a>.
<!--break-->
