---
title: "Free Developer Sprint for North American KDE GSoC 2009 Students!"
date:    2009-04-20
authors:
  - "jefferai"
slug:    free-developer-sprint-north-american-kde-gsoc-2009-students
comments:
  - subject: "Awesome"
    date: 2009-04-20
    body: "I almost wish I was a student still :-)"
    author: "Haakon"
  - subject: "Good luck..."
    date: 2009-04-20
    body: "... to all the students who applied and I hope you'll have a great weekend!"
    author: "troubalex"
  - subject: "To be young again..."
    date: 2009-04-20
    body: "Ah, to be a young student again. No cares, no worries, no need to sleep..."
    author: "Brandybuck"
  - subject: "Stability is valuable, though."
    date: 2009-04-20
    body: "Ahh, if only, if only....I need to eat and sleep, I have field work for this class, a paper for that class, I have to integrate a physics library for another class, I should also study for the exam for that one class, I have to find a new job because my old one went under, my family hasn't heard from me in weeks, the list goes on.  And to top it all off, because I never get a break from the essential things, \"non-essentials\" like bonding with the community and refining my proposal were never able to happen (much to my chagrin).  So I'm worried sick over here, man! :)\r\n\r\nA \"status quo\" that has me working a flat nine-to-fiver would really give me so much more time to accomplish things in the day. :/"
    author: "Wyatt"
  - subject: "agreed :)"
    date: 2009-04-20
    body: "I think Brandybuck was being a bit ironic (\"no need for sleep\"), but yea I'm not envious of university students at all. :)"
    author: "eean"
---
<a href="http://qtsoftware.com">Qt Software</a> and <a href=http://ev.kde.org/"">KDE e.V.</a> are happy to report that they are sponsoring a developer sprint for all North American students accepted into Google Summer of Code 2009 to work on KDE.  The event will be completely free for all accepted students, with round-trip flights, lodging, and some meals fully reimbursed; all students that applied for GSoC 2009 are invited to attend, although those not accepted into the program will have to ask <a href=http://ev.kde.org/"">KDE e.V.</a> to be reimbursed for travel and lodging expenses.
<!--break-->
The focus of the sprint will be integrating the students into the community, introducing them to each other and to longer-term members of KDE, giving them some quickstart training with Qt and KDE, helping them get their developer environments set up; helping them get started on their projects and hopefully just having a good time.

The location will be Boston, Massachusetts, USA.  The exact time is not yet determined (it will take place over a weekend and will depend upon the best-fit dates for the accepted students), but we hope that each and every one of the accepted students will be able to attend, and will try to schedule it as early in the summer as possible.

Accepted students for GSoC 2009 are going to be announced later today: good luck to everyone!