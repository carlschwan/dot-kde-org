---
title: "KOffice Meeting Day Two"
date:    2009-12-02
authors:
  - "jospoortvliet"
slug:    koffice-meeting-day-two
comments:
  - subject: "food"
    date: 2009-12-01
    body: "Anybody complaining about the fact 2/3 of the images are about food: first of all, I like food. We all do, we wouldn't eat it three times a day if we didn't. Second, didn't have any better pics ;-)"
    author: "jospoortvliet"
  - subject: "High speed"
    date: 2009-12-02
    body: "Wow, I was reading the stuff about metadata in ODF 1.2 and thinking that would be cool once it gets implemented, one day, and then read \"Within 50 days this work, which is sponsored by NLNet, will be implemented in KOffice\"."
    author: "Stuart Jarvis"
  - subject: "MS Office Support"
    date: 2009-12-02
    body: "Installed 2.1 in Kubuntu this morning to check how well it can read MS Office documents and sad to say, the entire document looked whacky. \r\n\r\nIs there a roadmap to support MS Office files on par with OO.org (or better)?\r\n\r\nThanks."
    author: "kanwar.plaha"
  - subject: "Getting better"
    date: 2009-12-02
    body: "KOffice seems to be following the path of KDE4. 2.0 was not usable, 2.1 is semi-usable, but not really. 2.2 will be ok, 2.3 will start making it awesome, and by 2.4 it will probably be surpassing OOo. :)"
    author: "cabrey"
  - subject: "patience..."
    date: 2009-12-02
    body: "Most of the improvements in that area will be merged over the coming months so 2.2 will most likely be a lot better."
    author: "jospoortvliet"
  - subject: "4 months?"
    date: 2009-12-02
    body: "Note that the KO developers are going to try a 4-month schedule, so it might be a release more than that if it would take the same amount of time ;-)"
    author: "jospoortvliet"
  - subject: "Have to keep reminding myself"
    date: 2009-12-02
    body: "There's only 7 developers working on the main of KOffice. When I read this stuff, I tend to forget that there's not a massive company behind KOffice (and strictly speaking, Nokia is only working on the MS import filter). I can't wait for it to be included in KDE-centric distributions by default - I expect development to pick up considerably then.\r\n\r\nAnyway, good luck - and thanks for not just abandoning KOffice for OpenOffice, like so many people suggest you should :D"
    author: "madman"
  - subject: "Copy/paste"
    date: 2009-12-02
    body: "I suppose that copy/paste can be used to transfer arbitrary contents between different KOffice instances, but does copy/paste also work as expected between KO and OOo?"
    author: "majewsky"
  - subject: "Copy/paste"
    date: 2009-12-03
    body: "That's a very good question.  Unfortunately copy/pasting snippets that include formatting, meta data and maybe more is not defined in the specification.  \r\n\r\nThe copy/paste spec allows you to offer your data in several formats at the same time.  We will start by creating a KOffice specific format (meaning that we don't have buy-in from other vendors) and at the same time work with others to create a public specification.  If it turns out that the koffice specific one and the public one are not the same, then we will also offer the cut/paste in the new format.  The same goes for drag&drop of course."
    author: "ingwa"
  - subject: "The development on KOffice is"
    date: 2009-12-03
    body: "The development on KOffice is great, that project is way too important to fail in my opinion. It's time to replace Firefox, OpenOffice and the GIMP in KDE distributions, because you can do so much more with KDE/Qt if it's actually used well."
    author: "d2kx"
---

New day, new KOffice work. The team met for lunch at 8 and arrived in the office around 9. After Inge opened the day again everybody took a while to check mail and news. Then it was time for work.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href=" http://dot.kde.org/sites/dot.kde.org/files/morningfood_0.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/morningfood_small_0.jpg" width="400" height="326"/></a><br />Breakfast<br /><i>images courtesy of Jaroslaw Staniek<i></div>

<h2>Starting with Metadata</h2>

Starting at 10 o'clock Jos van den Oever gave a talk about using metadata in ODF 1.2 and how it could be used in KDE and KOffice applications. He introduced the <a href="http://nepomuk.kde.org/">Social Semantic Desktop</a> to the team, going through the basic architecture. After Nepomuk and KDE technology he went through what ODF has in terms of Metadata support. Luckily the new ODF 1.2 standard builds strongly on existing standards and formats, and unlike the 1.0 standard it supports an arbitrary amount of metadata through RDF. <a href="http://kogmbh.com">KO GmbH</a> has a team with <a href="http://monkeyiq.blogspot.com">Ben Martin</a> and Tobias Hintze working to get metadata support in KOffice. More specifically they are enabling KWord to read and write metadata during loading and saving of 1.2 documents. They aim to to do it properly so even copy-pasting of text preserves metadata.

Bringing them together, ODF and RDF support makes it possible to store any type of data in your document, making office smarter. Combine some scripting with metadata and you can for example make a document which supports several languages, and upon opening it will show your local language. Not having to generate separate documents from a server but having it in one single document makes a lot of sense for companies who want to archive their documents.

<div style="float: left; padding: 1ex; margin: 1ex; border: thin solid grey"><a href=" http://dot.kde.org/sites/dot.kde.org/files/troll_office_0.jpeg"><img src="http://dot.kde.org/sites/dot.kde.org/files/troll_office_small_0.jpeg" width="400" height="224"/></a><br />Empty, lonely office space</div>

As said, clever documents keep their metadata, and the same goes for parts of the text. Governments for example often copy around pieces of law, basically quoting it in various other policy documents. If those pieces of text could retain their original metadata they could point back to the original law. Upon opening the document scripts could help update the text or warn the user the law has been changed between the writing of the document and the reading of the opened document.

This technology can make documents far more powerful - small databases in themselves (in ODF 1.2, each ODF document is a so-called triple store, forming the basis of a semantic network). Within 50 days this work, which is sponsored by <a href="http://www.nlnet.nl/">NLNet</a>, will be implemented in KOffice and the developers can start experimenting with it and explore the posibilities.

<h2>End User Readiness</h2>

The second and last main subject of the sprint is End User Readiness. Inge led the discussion on this. Important here was that currently there is no clear agreement on the definition of End User Readiness. It was agreed that for all applications a persona, a virtual target user, will be defined. The team will focus on getting the application ready for that persona. As soon as it is, it is End User Ready. Of course the initial personas will have pretty moderate requirements. Newer versions will then target more advanced personas, organically moving up to professional users.

It took quite a while to create personas for all the applications. To speed the process up it was decided to try and use real people for the personas. These people will of course have to be willing to help by complaining as long as the application does not fulfill their requirements.

<div style="float: right; padding: 1ex; margin: 1ex; border: thin solid grey"><a href=" http://dot.kde.org/sites/dot.kde.org/files/eveningfood_0.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/eveningfood_small_0.jpg" width="400" height="267"/></a><br />Dinner time</div>

The team does not only rely on these personas and their own ideas and opinions. Support from the KDE usability and open usability teams has been promised, and Subash will try and see if Nokia can help out as well.

<h2>Finishing up</h2>

After the group photo the team went back to relatively quiet hacking and small discussions (leading to, among other things, <a href="http://wiki.koffice.org/index.php?title=Schedules/KOffice/2.2/Release_Plan">this release plan</a>).

Many team members had to leave quite early - after lunch the office quickly went quiet. The few still there continued working and developing a bit until the office had to be closed. As Thomas Zander, who helped Alexandra Leisse take care of the visitors, noted: "I must say this meeting really surpassed my expectations. We discussed more topics and even came up with several new plans and ideas. The previous meetings were still pre-2.0, and often focusing on past problems. Now we have our eyes on the horizon, and things look much brighter".

This seems a good note to end this article with.