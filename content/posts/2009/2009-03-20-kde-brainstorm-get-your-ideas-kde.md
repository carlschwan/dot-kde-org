---
title: "KDE Brainstorm: Get Your Ideas Into KDE!"
date:    2009-03-20
authors:
  - "einar"
slug:    kde-brainstorm-get-your-ideas-kde
comments:
  - subject: "kmail"
    date: 2009-03-21
    body: "The inability to cut and paste using html in kmail make it a NO-NO for me. If that was fixed I would use kontacy and the rest of the packlage, however as it stands NO.\r\n\r\nBob Coulter"
    author: "bob@coulter.org"
  - subject: "vmware server 2"
    date: 2009-03-21
    body: "Konqueror appears to be a good browser until I attempted to install the plugin that would allow me to use it with vmware server  2. Works good with firefox.\r\n\r\nBob Coulter"
    author: "bob@coulter.org"
  - subject: "which inability? As a *nix"
    date: 2009-03-21
    body: "which inability? As a *nix user (I suppose you are one) you should be accustomed with cut'n'paste by selecting the text and pressing the middle mouse button where you wan't to have it.\r\n\r\nCheers"
    author: "longh"
  - subject: "kmail"
    date: 2009-03-21
    body: "True on the middle mouse for text, however using thunderbird or Windows(ugh) i can also do graphics..\r\nBob"
    author: "bob@coulter.org"
  - subject: "Cool"
    date: 2009-03-21
    body: "Wow!\r\n\r\nThat's a really cool idea! I'm loving it ;)\r\n\r\nCheers"
    author: "twolfy"
  - subject: "That's a bug ..."
    date: 2009-03-21
    body: "Hi Bob,\r\n\r\nThe Dot is not the best place to report bugs like that, this way, developers will read it and are able to do something about it. Comments on the Dot are maybe a way to express your discomfort, but it doesn't help anything in the way of getting the bug you report fixed. The Dot is just to volatile for that. Besides this, your report is missing essential information to be even remotely able to fix it.\r\n<p />\r\nhttp://bugs.kde.org for bugs please."
    author: "sebas"
  - subject: "A critique of Brainstorm section"
    date: 2009-03-22
    body: "Really nice section but:\r\n - you cannot see the current score of an idea until you open the thread\r\n - you cannot order by votes (the place for those icons like thumbs up could be used for that number)\r\n - voting is flawed: imagine if some idea gets 200 votes up and 200 votes down this will result in a 0 vote count which is unfair as such an idea should probably be more of an option. Instead of voting there could be a tagging system, so that you could tag ideas for instance as:\r\n\"uber awesome\"\r\n\"I like it\" (current vote up)\r\n\"interesting, but low priority\"\r\n\"Completely crap\" (vote down)\r\nan then show how many for each tag...\r\n - tagging for different sections of KDE should also be interesting so you can browse for ideas just for Plasma, KWin, etc.\r\n"
    author: "mootchie"
  - subject: "Agreed..."
    date: 2009-03-22
    body: "... and this is something we are working on. The brainstorm forum will get a different thread view than other forums.\r\n\r\nAlso, we plan to do a tag (cloud) plugin to give every thread useful tags, independant of the brainstorm area.\r\n"
    author: "neverendingo"
  - subject: "Ubuntu's Brainstorm?"
    date: 2009-03-23
    body: "Great thing. I just think that this is a lot of work for you guys. Isn't the Ubuntu Brainstorm site available for other Projects? It's open source and it has everything a good Brainstorm needs.\r\n\r\nAnyway, rock on dudes !"
    author: "olingerc"
  - subject: "Re: Ubuntu's Brainstorm?"
    date: 2009-03-23
    body: "See <a href=\"http://forum.kde.org/brainstorm-forum-idea-sort-by-votes-t-39104.html#pid55210\">this post</a> and neverendingo's answer below."
    author: "Hans"
  - subject: "About time."
    date: 2009-03-24
    body: "I've been asking for a Official Community forum for awhile now, I'm really glad they implemented it. \r\n\r\n+1 KDE Team. "
    author: "Dekkon"
  - subject: "How to score"
    date: 2009-04-02
    body: "IMHO, score = good - bad, is not a good idea.  First you have to consider what the votes mean.  If people voting good mean that they would like the feature then > 40% should be considered a good score.  IAC, the score should be in percent good of the total votes."
    author: "KSU257"
---
KDE is about the community, rather  than the product. It is not all about the code: there are many other ways in which people can be part of KDE, and a very simple way is to connect with other people.

In an effort to bridge the gap between users and developers, the <a href="http://forum.kde.org">KDE Community Forums</a> have launched a new initiative to coordinate feature requests. A new <a href="http://forum.kde.org/kde-brainstorm-f-83.html">"Brainstorm" section</a> has been created in the KDE Community Forums: users are encouraged to post requests there.
<!--break-->
Once a wish has been filed, it is reviewed by the forum staff to filter out duplicates, spam, and the like. Upon approval, other forum users can approve or disapprove the idea. Every few months, the highest-voted features will then be submitted to the developers as feature enhancement requests. Of course, there will be no guarantee a feature will get implemented, but proposals made this way will be more focused, reducing the noise for both developers and users and leading to a better relationship between the two worlds.

This is a first public test run. In order to provide the best experience possible, we encourage everyone to come to the forums, make use of the system, and leave feedback. Do not forget that KDE Brainstorm is meant to accept suggestions on new features only: crashes or unexpected program behaviors are bugs and should be reported to KDE's Bugzilla instead.

If you feel creative, this is the time to show your skill to the world. <a href="http://forum.kde.org/kde-brainstorm-f-83.html">The Brainstorm forum</a> is waiting for you!