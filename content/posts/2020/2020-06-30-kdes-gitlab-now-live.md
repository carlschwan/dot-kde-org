---
title: "KDE's GitLab is now Live"
date:    2020-06-30
authors:
  - "aniqakhokhar"
slug:    kdes-gitlab-now-live
---
<!--
<figure class="topImage">
    <a href="/sites/dot.kde.org/files/GitLab_Logo_with_KDE_peoplex1000.png" data-toggle="lightbox">
        <img src="/sites/dot.kde.org/files/GitLab_Logo_with_KDE_peoplex1000.png" style="display: block;  margin-left: auto; margin-right: auto; width: 60%; height: auto;" alt="Plasma 5.19" />
    </a>
</figure>
-->

<figure style="float: right; padding: 1ex; margin: 1ex; width: 400px">
    <a href="/sites/dot.kde.org/files/GitLab_Logo_with_KDE_peoplex1000.png" data-toggle="lightbox">
        <img class="img-fluid" src="/sites/dot.kde.org/files/GitLab_Logo_with_KDE_peoplex1000.png" />
    </a>
</figure> 

After our final decision to adopt <a href="https://about.gitlab.com/">GitLab</a> in November 2019, KDE started the work of tackling the many challenges that come with moving a whole development platform for a large open source community. KDE has now officially completed Phase One of the adoption and contributors have begun to use GitLab on a daily basis.

<h3>Why did we migrate to GitLab?</h3>

By switching to GitLab we will be offering our community one of the most popular and latest, fully-featured, actively developed, and supported DevOps platforms in existence today. This will contribute to boosting collaboration and productivity, and making our workflow more transparent and accessible to everyone who wants to contribute.

<h3>How will it benefit the wider community?</h3>

By using a platform offering an interface that most open source developers are nowadays familiar with, we will be lowering the bar for new contributors to join us, and providing a way for our community to continue to grow even faster in the coming years.

GitLab will also help us to achieve goals like "Consistency", as it will help our community members have a single solution to their needs. Now, we will be able to host and review code, manage projects/issues, communicate, collaborate, and develop software/applications on a single platform.

By adopting GitLab as a platform, we will be adding stability to our framework, as we will count on the support of GitLab as a company. GitLab, Inc. has nearly a decade of experience behind it, releases new versions on a regular basis and, apart from its in-house team, counts on an active community of third party contributors. This guarantees that our new development platform will be updated and maintained throughout the years.

KDE has migrated to GitLab and our instance is now live. Start discovering projects, groups and code on by visiting <a href="https://invent.kde.org">invent.kde.org</a>.

You can read more about this migration on <a href="https://about.gitlab.com/blog/2020/06/29/welcome-kde/">GitLab's blog</a>.
<!--break-->