---
title: "Don't miss Akademy 2020 \u2014 This Year KDE is going Online!"
date:    2020-04-15
authors:
  - "sealne"
slug:    dont-miss-akademy-2020-going-online
---
<div style="text-align: center;"><img src="/sites/dot.kde.org/files/collage_logo.jpg" alt=""  style="max-width:100%" /></div>
<p>
<br />
</p>
The KDE Community will be hosting <a href="https://akademy.kde.org/2020">Akademy 2020</a> online between <strong>Friday 4th and Friday 11th September</strong>.

The conference is expected to draw hundreds of attendees from the global <a href="https://kde.org/">KDE Community</a>. Participants will showcase, discuss and plan the future of the Community and its technology. Members from the broader Free and Open Source Software community, local organizations and software companies will also attend.

<h2>Akademy 2020 Program</h2>

Akademy 2020 will begin with virtual training sessions on Friday 4 September. This will be followed by a number of talk sessions held on Saturday 5 Sept. and Sunday 6 Sept. The remaining 5 days will be filled with workshops and Birds of a Feather (BoFs).

<h2>A Different Akademy</h2>

Due to the unusual circumstances we are living through and the need to keep the KDE Community healthy, thriving, and safe, the Akademy Team have decided to host Akademy 2020 online. During the program organization period for this year's activities, we took into consideration multiple timezones to ensure that, regardless of physical location, every member of the KDE Community can participate in as many conference activities as they like.

Despite not being able to meet in person this year, KDE members will be able to reach an even wider audience and more people will be able to attend and watch the live talks, learn about the workings of the technology and the Community by participating in Q&As and panels.

Registrations and Call for Papers will be opening soon!
<!--break-->