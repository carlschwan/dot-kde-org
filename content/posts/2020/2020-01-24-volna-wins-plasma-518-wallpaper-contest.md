---
title: "Volna Wins Plasma 5.18 Wallpaper Contest"
date:    2020-01-24
authors:
  - "sealne"
slug:    volna-wins-plasma-518-wallpaper-contest
---
<div style="text-align: center;"><img src="/sites/dot.kde.org/files/Volna-desktop_1500.jpg" alt="" width="800"></div>
<p>

</p>
<p><b><a href="https://forum.kde.org/viewtopic.php?f=313&amp;t=164095">Volna</a> by Nikita Babin wins KDE's 2nd Wallpaper contest. Volna will be upcoming Plasma 5.18's default wallpaper.</b></p>
<p>Congratulations to Nikita for the win. Nikita will receive the Grand prize, a <a href="https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Notebooks/10-14-inch/TUXEDO-InfinityBook-Pro-14-v5.tuxedo">TUXEDO Infinity Book 14</a> featuring an i7 Intel processor and an all-day battery with a 12-hours capacity.</p>
<p>We would like to extend our congratulations also to the artists that made the finals, specifically (and in no particular order): metalbender and the spacey  <a href="https://forum.kde.org/viewtopic.php?f=313&amp;t=164105">Milky Way</a> wallpaper; CaceK, who created the dramatic <a href="https://forum.kde.org/viewtopic.php?f=313&amp;t=163999">Breach / Crystaline</a>; Luwx submitted the cool looking <a href="https://forum.kde.org/viewtopic.php?f=313&amp;t=164168">Iridescent Shell</a>;  <a href="https://forum.kde.org/viewtopic.php?f=313&amp;t=163942">The Grand Canyon</a> was designed by kevintee; and the winner of the Plasma 5.16 wallpaper competition, Santiago Cezar, also made it to the finals with <a href="https://forum.kde.org/viewtopic.php?f=313&amp;t=163800">Vera</a>. They will each receive a package containing a KDE-branded baseball cap, a plush Tux, KDE stickers, a frozen glass coffee mug and more goodies.</p>
<p>We saw many high-quality entries in this contest and it has been difficult to select six finalists and even harder to choose a winner. We are incredibly proud of the great community that decided to contribute in making Plasma a great desktop and we hope that the artists who joined the competition, even if they didn't win, will become regular contributors to <a href="https://community.kde.org/Get_Involved/design">the Visual Design Group</a> and help make Plasma even better. </p>
<p>The 5.18 wallpaper contest ends today, but if you still want to try and win some amazing prizes, don't forget about the other two contests that are currently ongoing: the <a href="https://community.kde.org/Promo/Plasma_5.18_Video_Competition">Plasma Video Contest</a>, the winner of which will receive a PC with a powerful Intel core i7, 16GB of RAM, 250GB NVMe SSD, 2TB HDD and an Nvidia GTX1050Ti video card as the prize; and the <a href="https://forum.kde.org/viewforum.php?f=315">Applications Video Contest</a>, which has a PC featuring an Intel core i3, 16GB of RAM and 250GB SSD as the prize.</p>
<p>We would also like to thank <a href="https://www.tuxedocomputers.com/">TUXEDO</a> for sponsoring the competition and for donating all the prizes.</p>
<!--break-->