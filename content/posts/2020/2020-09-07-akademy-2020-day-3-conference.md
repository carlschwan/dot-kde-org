---
title: "Akademy 2020 Day 3 - Conference"
date:    2020-09-07
authors:
  - "Paul Brown"
slug:    akademy-2020-day-3-conference
---
Day 2 of the conference stretch of Akademy (day 3 of the overall event) kicked off with a heavy-duty programming courtesy of Ivan Čukić who talked about <I>C++17 and 20 Goodies</I>. Most KDE applications are developed using C++ so Ivan covered the new features that C++17 and 20 bring and how they could be combined with each other.

Something more user-centric was going on in Room 2, where Marco Martin and Aditya Mehra were <I>Showcasing Plasma Bigscreen</I>, KDE's interface for large smart TVs. Marco and Aditya took attendees through the various features and the technology that powers Bigscreen, such as Plasma, Kirigami, and KDE Frameworks with a touch of Mycroft's open-source voice assistance platform. You can <a href="https://plasma-bigscreen.org/">try Bigscreen now</a> by burning it to a micro SD card and loading it into a Raspberry Pi 4  hooked up to your TV.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/main%20menu.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/main%20menu.jpg" />
    </a>
   <figcaption>Plasma Bigscreen melds Plasma, Mycroft and a Raspberry Pi to make any TV a smart TV.</a></figcaption>
</figure> 

In the next slot, the audience in Room 1 was subject to another talk about programming languages, in this case, <I>Rust from a KDE Perspective</I>. In this talk, Méven Car explained what Rust could offer to the KDE developer community and the features that made it a unique programming language.

<figure class="float-md-right w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/Janayugam.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/Janayugam.jpg" />
    </a>
   <figcaption>Janayugam: The Indian daily newspaper that took the leap to Free Software.</a></figcaption>
</figure> 

In Room 2, we learned about a success story of KDE in the Real World™: back in October 2019, the staff of Janayugom, a local daily newspaper in Kerala with 100,000 readers, decided to move their publication to Free Software. In his presentation on <I>Free Software, Press Freedom & KDE</I>, Ambady Anand S., a sysadmin involved in the move, told us the story of how the migration went.

Later, back in Room 1, Andreas Cord-Landwehr introduced a different way of developing in his talk <I>Test It! – Unit testing for lazy developers</I>. Andreas proposed developers turn things on its head and prepare tests for the code before actually writing the code. He argued that code written to pass tests was leaner and more focused. Andreas also discussed why automated tests are important for projects and strategies on how to design them.

Meanwhile, in Room 2, Timothée Giet was <I>Celebrating 20 Years of GCompris</I>, as well as the fact that the universally acclaimed classic FLOSS toolset for teachers is nearing version 1.0. It only took two decades! Seriously though, Timothée showed us some of the new activities which are coming to GCompris, such as new counting and arithmetic games, and a fun-looking electric circuits simulator.

Next up in Room 1, David Faure told us about <I>KIO: A Story of Young and Old Jobs</I>. The presentation aimed at application developers and KIO contributors gave an overview of the job mechanism as it is used in KIO, laid out the jobs added in the last two months and explained the concept of "delegates" which are used to solve the inverse dependency problem.

In Room 2, Marta Rybczynska told us about her <I>Year in KDE from Outside</I>. During her year "away," she analyzed media reporting on KDE and tracked what news sites, blogs, and podcasts chose to focus on when they talked about KDE.

Next up in Room 1 Daniel Vrátil talked about <I>Static Code Analysis with Gitlab CI</I>. Daniel showed the benefits of using static analysis tools and linters to automatically check code quality, and explained how to configure GitLab CI to run those tools automatically on each pull request or as a part of regular builds.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/1200px-Gcompris_0.61_screenshot.png" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/1200px-Gcompris_0.61_screenshot.png" />
    </a>
   <figcaption>GCompris, the world's favorite educational software for children, celebrated its 20th anniversary at Akademy 2020.</a></figcaption>
</figure> 

In Room 2, Leinir and Andrew Shoben literally couldn't hide their excitement while presenting <I>KDE Wags Your Tail</I>, in which they explained how to control animatronic tails and ears using software based on free software and KDE's Kirigami framework.

<figure class="float-md-right w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/photo_2020-09-06_13-20-22.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/photo_2020-09-06_13-20-22.jpg" />
    </a>
   <figcaption>The news of the availability of KDE-controlled tails caused sensation among a certain demographic.</a></figcaption>
</figure> 

The first set of the day's talks wound up with three 10-minute fast track talks in which Bhushan Shah talked about his experience with and gave advice on online sprints; Adriaan de Groot explained the Fiduciary License Agreement, a tool that the KDE community uses to manage licensing in the long-term; and Kai Uwe Broulik revealed some of the less obvious tips and tricks to get the most out of KDE's Plasma desktop.

After a four-hour recess, the KDE Community got together again to listen to the event sponsors.
KDE displayed deep gratitude to Canonical, KDAB, MBition, openSUSE, GitLab, Froglogic, Collabora, CodeThink, FELGO, DorotaC.eu, The Qt Company, Pine64 and Tuxedo for their generosity thanks to which Akademy is made possible.

Next up in Room 1, Dimitris Kardarakos talked about <I>Creating a Convergent Application Following the KDE Human Interface Guidelines</I>. Dimitris introduced attendees to the primary components of Kirigami and showed how an application can look equally good on the desktop and mobile. Using the Calindori calendar app as an example, Dimitris aimed to inspire attendees to create their own Kirigami applications.

Meanwhile, in Room 2, Nicolas Fella explained in <I>Konquering the Droids</I> that, to stay relevant, KDE needed to expand from its traditional desktop space and into the mobile world. He argued the most realistic and quickest way to do that was to create apps for the Android ecosystem, and explained the porting process.

Aleix Pol took to Room 1's virtual stage again in <I>Getting into KWin and Wayland</I> to explain how contributors could get involved with the development of KDE's Wayland experience.

<figure class="float-md-left w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/18_Luis.png" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/18_Luis.png" />
    </a>
   <figcaption>Doctor Luis Falcón explains how they ported GNUHealth to mobile thanks to Python, Qt and KDE.</a></figcaption>
</figure> 

In Room 2, Doctor Luis Falcón  told us about <I>MyGNUHealth: GNU Health Goes Mobile with KDE and Kirigami</I>. GNU Health (GH) is a Libre Health and Hospital Information System which has been deployed in many countries around the globe, from small clinics to very large, national public health implementations. MyGNUHealth is the GH's Personal Health Record application that integrates with the GNU Health Federation and is focused on mobile devices. Dr. Falcón told us about what led him to choose KDE's Kirigami framework to develop MyGNUHealth and the technical insights gained by the community behind the project.

Following these two talks, Neal Gompa presented <I>Fedora KDE</I> in  Room 1, explained how it started and what makes it special within Fedora; while at the same time in Room 2, Aniqa Khokhar introduced us to <I>Change Management</I> and helped us learn how to accept changes and newcomers in KDE.

A bit later in Room 1, Volker Krause showed how, by <I>Using Wikidata and OpenStreetMap</I> data, it was possible to make applications smarter. Volker went into depth and explained how those two data sets are structured, how they can be accessed, how to comply with their licenses, and how developers can make use of them for their apps.

In Room 2 Catharina Maracke spoke to attendees about <I>Open Source Compliance</I>. Catharina told us about how in today's complex world of OSS license compliance, it is very important to know the basics of copyright and licensing structures as well as some of the relevant tips and tricks for the most common OSS licenses.

<figure class="float-md-right w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/EhQBo_XWoAU7Mdy.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/EhQBo_XWoAU7Mdy.jpg" />
    </a>
   <figcaption>Aniqa Khokhar discusses change and how to overcome the anxiety it generates.</a></figcaption>
</figure> 

A little later in Room 1, David Edmundson and Henri Chain co-hosted a talk on  <I>Next Generation Application Management</I> and explained how using cgroups, developers could contribute to making everything amazing.

And, speaking of amazing, in Room 2, Massimo Stella told us about Kdenlive's <I>Journey to Being a Leading Open Source Video Editor</I>. He showed us a video of Kdenlive's new capabilities and provided a live demo of the new features of KDE's powerful video editing software.

The regular talks wrapped up with two technical talks aimed at developers: In Room 1 Carson Black  talked about <I>API Design and QML</I> and in Room 2 Benjamin Port explained how Plasma's System Setting have recently all been ported to KConfigXT and the advantages gained from the move in <I>System Settings: Behind the Scenes</I>.

To finish off the day, we had a star keynote presentation delivered by Nuritzi Sanchez, Senior Open Source Program Manager at GitLab and prior President and Chairperson of the GNOME Foundation. In her presentation, <I>Open Source Resilience and Growth: Creating Communities that Thrive</I>, Nuritizi  talked about some of the initiatives that KDE is involved in that help it become a more resilient community, while at the same time pointing out areas of opportunity. She also explored topics around building more diverse and inclusive communities, including collaborative communication, and ideas for outreach.

As usual, YouTube recorded the streams which allows you to enjoy the conference in case you missed anything, albeit in a big blobby, unformatted form:

<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/y59AOI4Vwvg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/OnDnspPWn4w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/y59AOI4Vwvg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/UVzZA1_OKV4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>


We will post edited and cut versions of the talks to PeerTube and YouTube soon. Watch this space!

From Monday to Thursday, Akademy attendees will participate in Bird of a Feather (BoF) meetings, private reunions, and hackathons.

We'll be back with more talks on Friday, September 11, when we will hear from students working on KDE projects and some of KDE's most active veteran contributors.
<!--break-->