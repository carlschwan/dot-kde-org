---
title: "Nuritzi Sanchez and Lydia Pintscher"
date:    2020-08-22
authors:
  - "unknow"
slug:    nuritzi-sanchez-and-lydia-pintscher
---
By Allyson Alexandrou

<figure class="float-md-right w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/photo_2020-08-03_20-59-39.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/photo_2020-08-03_20-59-39.jpg" />
    </a>
   <figcaption>Nuritzi Sánchez</a></figcaption>
</figure> 

<I>Nuritzi Sanchez is the Senior Open Source Project Manager at GitLab Inc. Previously, Nuritzi has worked at Endless and was the President and Chairperson at GNOME. KDE is excited to have Nuritzi as a keynote speaker at this year's Akademy to talk about Collaborative Communication in the Open Source community.</I>

<I>Lydia Pintscher is KDE e.V.'s current Vice President and Wikimedia Deutschland e.V.'s Product Manager for Wikidata. Lydia is a free software and free culture enthusiast who was the President of the e.V. in years past. As the current Vice President of KDE e.V. you will see her talking about updates on KDE's goals during Akademy.</I>

<I>Today we are lucky to have both of these well accomplished women sit down to take a moment to talk about their lives, accomplishments, visions for the future, and how we can continue to uplift young women in the technology and open source sector.</I>

<span style="color:#1b1;font-weight:bold">Lydia:</span> Hey :)

<span style="color:#f55;font-weight:bold">Allyson:</span> Hi there! 

<span style="color:#55f;font-weight:bold">Nuritzi:</span> HI everyone!

<span style="color:#f55;font-weight:bold">Allyson:</span> Thank you for being here. Shall we get started?

<span style="color:#55f;font-weight:bold">Nuritzi:</span> Sure, happy to be here :)  And nice to see you all 

<span style="color:#1b1;font-weight:bold">Lydia:</span> Let's do this!

<figure class="float-md-right w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/Lidya_interview.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/Lidya_interview.jpg" />
    </a>
   <figcaption>Lydia Pintscher</a></figcaption>
</figure> 

<span style="color:#f55;font-weight:bold">Allyson:</span> First Question: Every superhero has a defining moment in their origin story: Bruce Wayne fell into a cave full of bats when he was a young boy, Jessica Jones was exposed to radioactive material in a traffic accident... What was your defining moment? What happened that put you on course to where you are today? Were you a child? In high school? At college? 

<span style="color:#1b1;font-weight:bold">Lydia:</span> Haha. There were a few defining moments I'd say but one that definitely changed the course of my career was when a friend and fellow student in college told me to come and join the booth for one of KDE's projects at a big Linux expo in Berlin. I was contributing to the project for a while here and there already but didn't really feel like I'm a part of it. 

Anyway - he wouldn't accept a no and excuses like "I wouldn't know what to tell people who ask questions about the program." I went there and didn't know what I was doing for the first day but then it was smooth sailing and I met the best people who welcomed me with open arms. Been with KDE ever since and have no regrets!

<span style="color:#f55;font-weight:bold">Allyson:</span> That is amazing Lydia! Sounds like a very significant moment that pushed you into the world of KDE quite quickly. And Nuritzi, what about you?

<span style="color:#55f;font-weight:bold">Nuritzi:</span> For me, I think there are several defining moments -- I can't narrow down to just one. The first is when I was really young and would visit Mexico with my parents since they're from there and a lot of my family is still there. I developed a desire to do something to help level the playing field for more people around the world, not just in the US, and I was able to see a kind of poverty that I didn't see in Silicon Valley (to be clear, it was a different kind. Poverty in the Silicon Valley still exists). Throughout my career, I've explored ways of doing that and since I am from the Silicon Valley, I think tech seemed a pretty obvious choice for how I could try to influence worldwide change. 

Another moment for me was when I was in the 8th grade and a teacher encouraged me to go to a leadership summit. It taught me to have a leadership mindset, and that being a leader was really possible. 

<figure class="float-md-left w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/endless.JPG" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/endless.JPG" />
    </a>
   <figcaption>At Endless, Nuritiz worked to take computers to areas in Latin America where there is no Internet.</a></figcaption>
</figure> 

Lastly, for open source, I came across it because of the work I was doing at Endless. We created computers for users without internet and did a lot of research in Latin America, and I started to learn about the Linux app ecosystem and the world of GNOME. For me, contributing to open source became a way to work towards the social change goals I developed really early on. 

I agree with you Lydia -- that warm reception of people welcoming me with open arms into GNOME was a defining reason of why I joined. The social aspect of contributing to projects is equally important as the technical aspect, in my opinion.

<span style="color:#f55;font-weight:bold">Allyson:</span> Nuritzi, That sounds incredible as well. 

And I can most definitely relate to you both with the warm welcome received in the open source communities.

We touched on this a bit already, but I would like to follow up just a bit more. You both have been extremely successful in the technology industry (namely open source of course), while Lydia studied computer programming at uni, Nuritzi you were in international relations.

Nuritzi, How do you think this has affected your mindset and work you have done in your career so far?

And Lydia, what sorts of influences did you have to choose to study computer programming rather than something else?

<span style="color:#1b1;font-weight:bold">Lydia:</span> Working with technology and computers in particular has always been interesting and a lot of fun for me. I think that's something I have from my mom. (Thanks, mom!) Being able to make a piece of code or text do what you want is incredibly empowering, starting with something simple like messing with the html of a website many many years ago. 

I am dismayed at the world moving more and more into a world where technology is something that's given to you and you take it as it is as opposed to something you tinker with. This feeling of being able to change the world around you is something many many more people should have but sadly don't. That's why I'm spending so much of my professional and volunteer time on opening up software and data so that more people can tinker. If my life had turned out differently I'd probably become a vet (thanks, dad!) or a lawyer. Not sure how I feel about that today ;-)

<span style="color:#55f;font-weight:bold">Nuritzi:</span> I actually studied International Relations and Psychology in undergrad, and I'm really glad to be bringing that perspective into FOSS! Because of my academic background, and my personal interests, I have a natural leaning towards thinking about the experience of products, communities, etc. through a cultural and people-focused lens. 

Open source is just as much about social challenges as it is about technical challenges, and so I've found many ways to jump right in and help, even though I'm not a programmer. In fact, there's so much to do in those areas, that I've become passionate about getting more people into open source that are from diverse backgrounds. 

Even beyond what's become more obvious (e.g. user experience design, translations, engagement, and documentation). For example, people who are interested in Sales or Business Development might be great fundraisers, and fundraising is something that communities really need! I'm kind of mad that I didn't know about open source sooner. At Stanford University, where I studied, I didn't hear once about open source -- and I think it's a shame. There's a lot of opportunity to get awesome students involved in open source from psych, international relations, economics, medical fields -- everyone! 

<span style="color:#f55;font-weight:bold">Allyson:</span> Thats pretty interesting! I very much admire the want/drive/need to give to others that you both have touched on already. 

That's a very fair point you made Nuritzi. American universities in general do not tend to use open source software. And it is a pity because there are many students at American universities who could really use the experience and exposure that would come with an internship. Or even just working on open source projects as many students do in the KDE community

<span style="color:#55f;font-weight:bold">Nuritzi:</span> Agreed!

<span style="color:#1b1;font-weight:bold">Lydia:</span> Yeah all around the world really. Contributing to an open source project for a while gives you so much experience and helps you grow, especially as people in KDE and other projects are willing to take you along and support you along the way.

<figure class="float-md-right w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/gnome_swag.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/gnome_swag.jpg" />
    </a>
   <figcaption>It was not only the swag that convinced Nuritzi to become an active GNOME contributor.</a></figcaption>
</figure> 

<span style="color:#f55;font-weight:bold">Allyson:</span> I absolutely think that the welcoming vibes that are common in open source communities could benefit students and younger professionals so much!

<span style="color:#55f;font-weight:bold">Nuritzi:</span> Yes, and an important aspect of getting students in is building relationships and supporting their teachers. Because a lot of teachers are the ones who encourage students to try new things, or help guide them in person. 

When speaking at an all woman's college in Oakland, California, a couple of years ago, the feedback I received from the teachers is that they felt disconnected from the open source communities and would be eager to be more plugged in and get their students involved

I think we often focus on the students only and skip teachers, so would love to see more of that relationship-building in the future

<span style="color:#f55;font-weight:bold">Allyson:</span> ^^ Absolutely! Teachers can make such an impact on the lives of their students. It is a special bond that is created with amazing teachers or mentors and their students

To keep on moving, are there any particular projects (volunteering or business opportunity) that you both are most proud of? And are there any current projects you are working on that you are particularly excited about?

<span style="color:#1b1;font-weight:bold">Lydia:</span> I think for me that would be KDE's vision and Goals and having brought that to fruition. KDE was becoming so large, diverse and splintered that it was very hard for us as a community to understand what we're really all about and what drives us. 

Redefining KDE's vision and the resulting Goals processes helped us grow together again and rally behind a shared vision for the future that we are all working towards: A world in which everyone has control over their digital life and enjoys freedom and privacy. It always makes me happy to see what impact this had on the KDE community over the past few years and what amazing new people joined us because of it.

<span style="color:#55f;font-weight:bold">Nuritzi:</span> I think I'm relatively new to open source compared to many members in the GNOME and KDE communities. I started actively contributing to GNOME back in 2015, and less than a year later I was encouraged to run for the Board of Directors and was voted in as President and Chairperson, so I'm pretty proud of that. 

The things that I mentioned before in my approach to open source community building and leadership is really what I wanted to share with the GNOME community and I'm very thankful for how receptive they were to that perspective and how encouraging they were to let me contribute in that way. During my time at the GNOME Foundation we hired an Executive Director after several years of not having one, attracted donations of over $1 million, and saw many people join us or re-join us. We also started a more active collaboration with KDE, which I'm particularly proud of. 

I think there's a lot that we can collaborate on and I'm of the bridge-making mindset! We launched the Linux App Summit along with KDE because of our closer collaboration. And I want to add that I was able to do that only because of the mentors I had within the community, the kind encouragement of some individuals, and the positive environment GNOME has.

<span style="color:#1b1;font-weight:bold">Lydia:</span> True! I think it has served both projects well.

<span style="color:#f55;font-weight:bold">Allyson:</span> Wow! 

Do either of you see any new collaborations coming up? More specifically, any sort of collaboration to help get more young women into open source?

<span style="color:#1b1;font-weight:bold">Lydia:</span> Nothing particularly exciting right now unfortunately.

<span style="color:#f55;font-weight:bold">Allyson:</span> Do you think this could be a possibility in the near future?

<figure class="float-md-left w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/LAS2019_1000.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/LAS2019_1000.jpg" />
    </a>
   <figcaption>Nuritzi (1st row, 3rd from left) and Aleix Pol (2nd row, 4th from left) with GNOME and KDE community members at LAS 2019.</a></figcaption>
</figure> 

<span style="color:#55f;font-weight:bold">Nuritzi:</span> To answer your other question, I'm still involved in organizing the <a href="https://linuxappsummit.org/">Linux App Summit</a> this year, although I'm more in a support role this time around. Aleix is doing a great job at leading it this year! 

I'm also involved in <a href="https://chaoss.community/">CHAOSS</a>, which is a community centered around metrics and measuring the health of open source communities. There was a lot of structural change within the GNOME Foundation that we created due to all the awesome stuff happening, and I really think that the next stage of our community's maturity is to measure how we are doing and iterate in order to improve. 

My goal when joining the GNOME Foundation was to help get it into a more strategic mindset and not just surviving but growing and thriving. Data is crucial for that. So I'm really excited to be working on that initiative (along with some KDE people, I might add)! 

<span style="color:#1b1;font-weight:bold">Lydia:</span> Absolutely. I'd love to, for example, make it possible for KDE to take part in initiatives like Outreachy again to give women a good path to contributing to KDE and open source at large.

<span style="color:#55f;font-weight:bold">Nuritzi:</span> I think there's room for collaboration on those metrics, and it's something that's already underway. I also think that we're collaborating more in the Linux app space, which is crucial for both of our orgs. I would love to see that expanded. 

Also, I think that there's room for collaborating on things like onboarding initiatives and making more inclusive environments. While each org may have their own spin on it, I think sharing in those efforts would be beneficial since our orgs are likely to need to learn about similar things / might have similar ideas 

<span style="color:#1b1;font-weight:bold">Lydia:</span> Yeah that's definitely something we should closely collaborate on.

<span style="color:#f55;font-weight:bold">Allyson:</span> Very cool! This is beginning to sound like an interesting upcoming opportunity for open source communities.

<span style="color:#55f;font-weight:bold">Nuritzi:</span> Honestly, I think there's a lot of room for collaborating more on a TON of the social challenges our open source orgs face -- and with orgs beyond that! 

I'm also part of another group called FOSS Responders that formed to help orgs that are experiencing financial difficulty because of COVID and I was able to meet people from Drupal, Python, GitHub, Indeed, Spotify, etc... 

I think it's important for us to join other communities and create bridges because SO MUCH of what they were also involved with seemed super relevant to GNOME and KDE! Yep! I agree :)  

Unfortunately, I haven't been as involved in GNOME since I started a new job at GitLab and am trying to ramp up / make lots of awesome improvements for the open source world there. But this kind of stuff is where my heart lies, and I'm happy to be part of initiatives where we collaborate on newcomer experiences and the like! :)  

<span style="color:#1b1;font-weight:bold">Lydia:</span> I think that's a common challenge - so many important and worthwhile things to do and only so much time to do them.

<span style="color:#55f;font-weight:bold">Nuritzi:</span> But yeah, Lydia -- I'd love to connect about all of this afterward and start connecting some more dots :)  

<span style="color:#1b1;font-weight:bold">Lydia:</span> That's one of the things that's hard but you need to learn in open source: prioritize and spend your time on the things that matter the most 

<span style="color:#f55;font-weight:bold">Allyson:</span> Absolutely! This is all very encouraging to hear.

<span style="color:#55f;font-weight:bold">Nuritzi:</span> Yeppp definitely. I actually experienced a bit of burnout and so right now I'm in the process of achieving a more balanced approach at contributing. And yes, I 100% agree with that, Lydia

<span style="color:#f55;font-weight:bold">Allyson:</span> Are there any other tips you both would pass along to young women (or younger people in general) about getting started in open source? Was there anything you learned that could be useful to those still gaining experience? 

Lydia, prioritizing and task/time management are so crucial. It is good to see you point that out

<span style="color:#1b1;font-weight:bold">Lydia:</span> There are so many things, some of them we put together in <a href="http://open-advice.org/">Open Advice</a>. 

Looking back today I would say the best opportunities opened up for me when I stopped saying no to projects/opportunities a mentor offered me simply because I felt I wasn't up to the task. They offered it for a reason after all and they were right that I was capable of it. The world is holding you back enough. You don't need to help it in addition.

<span style="color:#55f;font-weight:bold">Nuritzi:</span> A lot of people are talking about this in the open source world, but mentorship is really key. So finding those people that you can go to when you have questions, or can help guide you is really important. In order to do that, it's important to take advantage of social events that the community has and ways that the community has set up to create 1:1 relationships. 

In this COVID era, having a video call can go a long way. Don't hesitate to reach out to people and see if they'd be willing to chat and share their experience with you. I also suggest trying to develop the leadership mindset. If you're willing to take on responsibility, then responsibility usually comes your way in an open source community. You need to show that your reliable and there for the medium-term though in order to really get plugged in and making a difference. I'd say the usual time is 6+ months, and then to consider staying really active for at least 2-3 years. It's hard because it's volunteer-only, so make sure that you pay attention to your mental health and that you prioritize your well-being before anything else! 

Being part of a community like GNOME and KDE is so rewarding though -- professionally and personally! For me, the volunteer time was definitely worth it and the experience has enriched my life is so many ways!  

Oooo, Lydia, I didn't know about Open Advice! I'll definitely be checking that out and sharing :)  

<span style="color:#1b1;font-weight:bold">Lydia:</span> :)

<span style="color:#55f;font-weight:bold">Nuritzi:</span> It's nice to see some familiar faces there!

<span style="color:#1b1;font-weight:bold">Lydia:</span> And I completely agree. If you have a mentor or sponsor who supports you that's worth so much. That also means that it is important for people like us to lift up the next generation. If you're in a position where you can be a sponsor for a person: do it! It'll make such a difference for them.

<span style="color:#55f;font-weight:bold">Nuritzi:</span> Yes. 10000000000% agree to that!

<span style="color:#f55;font-weight:bold">Allyson:</span> Great points! 

Alright my last BIG question for you both is... <B>drum roll please</B> ...

<span style="color:#1b1;font-weight:bold">Lydia:</span> :D

<span style="color:#f55;font-weight:bold">Allyson:</span> Do you both have any upcoming works that we should keep an eye out for? For example, any podcasts or books?

<span style="color:#1b1;font-weight:bold">Lydia:</span> I'm spending a ton of my time working on Wikidata, a knowledge base and sister project of Wikipedia. It's an incredible source of general purpose data and I'd love to see many more open source projects make use of it. One in KDE that already does it is KItinerary for airport data.

<span style="color:#55f;font-weight:bold">Nuritzi:</span> I don't have anything fancy planned, but I'm really excited about a speech I've started to give about collaborative communication. It's a topic that I think is super relevant not only in open source communities, but in life. I've had feedback from people who said it's been really useful as we go over things like cultural differences (and potential conflict that could arise because of it), active listening, giving AND receiving feedback, and some other of my favorite communication hacks. I have a version of it on YouTube somewhere (just search for my name "Nuritzi Sanchez" and "Communication Hacks"), and I'll be giving the speech live (and in some cases an expanded version), at some upcoming events. I encourage you to check it out! 

<span style="color:#f55;font-weight:bold">Allyson:</span> Nuritzi we can't wait to hear about this at Akademy as well! 

<span style="color:#55f;font-weight:bold">Nuritzi:</span> :)  :)  :)  

<span style="color:#f55;font-weight:bold">Allyson:</span> Lydia, KItinerary sounds quite interesting. I will have to go look it up. 

Thank you both so much for taking time out of your busy schedules to have this chat!

<span style="color:#1b1;font-weight:bold">Lydia:</span> Thanks for having us!

<span style="color:#55f;font-weight:bold">Nuritzi:</span> Thanks for organizing this -- it was fun!

<span style="color:#f55;font-weight:bold">Allyson:</span> I can't wait to *virtually* see you both next month!

<span style="color:#55f;font-weight:bold">Nuritzi:</span> Same here! Looking forward to it 

<span style="color:#f55;font-weight:bold">Allyson:</span> In the mean time, stay safe and healthy. Lydia have a wonderful evening & Nuritzi enjoy the rest of your day!

<span style="color:#55f;font-weight:bold">Nuritzi:</span> Thank you! Bye, all! :)  

<span style="color:#1b1;font-weight:bold">Lydia:</span> Thank you! Bye! :)

<I>Nuritzi will be delivering the keynote speech <a href="https://conf.kde.org/en/akademy2020/public/events/235">"Open Source Resilience and Growth: Creating Communities that Thrive"</a> on September 6 at <a href="https://akademy.kde.org/2020">Akademy 2020</a>.</I>

<I><a href="https://akademy.kde.org/2020/register">Register now</a> and don't miss it!</I>
<!--break-->