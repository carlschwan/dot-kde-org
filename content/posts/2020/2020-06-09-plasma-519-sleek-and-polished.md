---
title: "Plasma 5.19 - Sleek and Polished"
date:    2020-06-09
authors:
  - "Paul Brown"
slug:    plasma-519-sleek-and-polished
---
<figure class="topImage">
    <a href="/sites/dot.kde.org/files/laptop_p5.19.png" data-toggle="lightbox">
        <img src="/sites/dot.kde.org/files/laptop_p5.19.png" style="width: 100%; height: auto;" alt="Plasma 5.19" />
    </a>
</figure>

<b><a href="https://kde.org/announcements/plasma-5.19.0">Plasma 5.19 is out!</a></b> If we gave alliterative names to Plasma releases, this one could be "Polished Plasma". The effort developers have put into squashing bugs and removing annoying papercuts has been immense.

<video width="750" controls="1" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.19/Thumbnail.png">
  <source src="https://cdn.kde.org/promo/Announcements/Plasma/5.19/Video.webm" type="video/webm">
  <source src="https://cdn.kde.org/promo/Announcements/Plasma/5.19/Video.mp4" type="video/mp4">
Your browser does not support the video tag.
<a href="https://cdn.kde.org/promo/Announcements/Plasma/5.19/Video.webm">Plasma 5.19 video</a>
</video> 

In this release, we have prioritized making Plasma more consistent, correcting and unifying designs of widgets and desktop elements; worked on giving you more control over your desktop by adding configuration options to the System Settings; and improved usability, making Plasma and its components easier to use and an overall more pleasurable experience.

Read <a href="https://kde.org/announcements/plasma-5.19.0">the full announcement</a> to discover all the new features and improvements of Plasma 5.19
<!--break-->