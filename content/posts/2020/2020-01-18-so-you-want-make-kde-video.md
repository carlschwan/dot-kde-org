---
title: "So you want to make a KDE video..."
date:    2020-01-18
authors:
  - "Paul Brown"
slug:    so-you-want-make-kde-video
comments:
  - subject: "jingle cringe"
    date: 2020-01-18
    body: "<p>Likewise. I can never make it beyond three seconds of any mawkish, glib glockenspiel-ukelele combo soundtrack without going hulk.</p>"
    author: "pierre4l"
---
<div style="text-align: center;"><img src="/sites/dot.kde.org/files/female-4126134_1280.jpg" alt="" width="800" /></div>
<p><br /></p>
<b><a href="https://community.kde.org/Promo/Plasma_5.18_Video_Competition#Help_and_Feedback">KDE is running a competition</a> in search of the next great promotional video for KDE's Plasma desktop and KDE's applications.</b>

The prizes are two fantastic <a href="https://www.tuxedocomputers.com/">TUXEDO</a> computers, one per category, which will undoubtedly boost your film rendering capacity. There are also 12 goodie packages for runner-ups, and who doesn't need more Linux shirts, caps and stickers?

Although we have already received some interesting entries, we feel it may be time to help video artists out there with ideas from the judges themselves.

Below, Julian Schraner, Ivana Isadora Devčić, and Paul Brown from the Promo team and Farid Abdelnour from the Kdenlive team give their views on what a KDE promotional video should look like, where to find resources, and which pitfalls may hurt your film if you fall for them.

<h2>Julian</h2>

I have five simple recommendations for participants:

<ol>
<li>Avoid videos that contain only screencasts</li>
<li>Break the mold, be creative</li>
<li>Use motion graphics techniques (have a look at animation nodes)</li>
<li>Choose a good catchy song and work on that editing</li>
<li>Make it short, make sure there is something happening in every single frame. Cut out lulls -- they're boring</li>
<figure class="text-center img-fluid"><img class="img-fluid text-center" sizes="(min-width: 35em) 1200px, 100vw" src=/sites/dot.kde.org/files/bored-768x432.jpg alt></figure>
</ol>

<h2>Ivana</h2>

Here's my list of things I would recommend doing, as well as what to avoid:

<ul>
<li>No slideshows, please! Don't make the video look like someone animated a PowerPoint presentation with zoomed-in screenshots sliding into view and fading out. That just looks cheap and low-effort.</li>
<li>Don't overdo it with memes and attempts at humor. As amusing as it may be to you and your 3 friends, things like that do not always translate well across cultures and generations. It's OK to add a cheeky moment if it's appropriate with the general theme of your video, but trying to make the entire video "funny" might send the message that you think Plasma is a joke.</li>

<figure class="text-center img-fluid"><img class="img-fluid text-center" sizes="(min-width: 35em) 1200px, 100vw" src=/sites/dot.kde.org/files/confusedmacgyvermeme-5ab8392eae9ab8003778609b.PNG alt></figure>

<li>It's OK to include real, actual people in the video. Anyone can easily make a screencast, but it takes effort - and shows that you made an effort! - to film people using Plasma. Including clips of people using Plasma can make a much stronger impact than just showing the desktop, even if the clips are short.</li>
<li>This is very, very basic and Captain Obvious-style, but: background music can have this cool thing called volume. Playing with volume can add depth and variety to your video. Make the background music louder in some parts and softer in others, depending on the effect you're trying to achieve. If it's always the same, the entire video can easily seem monotonous, even if it's only a minute long.</li>
<li>If possible, don't use computer-generated voices for voice-overs in the video (that is, if you're going to have any at all). 98% of them sound really fake and distract from the actual content, or just make the video sound boring. Even if you don't have a particularly radio-friendly voice, it will still sound more natural and normal if you record yourself (or try asking someone to help).</li>
<li>Great videos tell a story; they're not just a semi-connected list of clips with background music tacked on. Try to develop a little story that sends a specific message instead of just showing random Plasma features. It's also very easy to fall into the trap of comparing Plasma to something else and disparaging other DEs, so please try to avoid that.</li>
<li>Personally, I do not want to see "wobbly windows" in any of the videos. Plasma is more than just KWin effects; we're looking for videos that will make people think "wow, this could be really useful to me, I wanna try it", not "haha that's a cool gimmick, I bet I can move the mouse fast enough to break it".</li>
</ul>

Another important thing - if the video does have any narration/voice-over, I would ask that you include subtitles (at least as a separate file, if hard-coding them is a bad practice). Accessibility is a must!

<h2>Paul</h2>

I've split my recommendations into 3 parts: the footage you can use, the music (and other sounds), and the editing; that is, the raw materials and how you put them together.

<h3>Footage</h3>

Like Ivana, I would personally like to see some live action mixed in with the screencasts. You can grab your phone, and go and film something you can then work into the story. Or you can also take some public domain footage from the <a href="https://archive.org/details/movies">Internet Archive</a>.

If you look at Real Life<sup>TM</sup> advertisements, they don't only show the product, they also show people doing stuff with the product. Most of our videos are already a bunch of screencasts chained together. I want to see something different.

That said, when you do insert screencast footage, make sure you show something interesting happening in the applications. Don't show an empty folder in Dolphin, a blank square in Krita, or a boring, text-only document in Okular. Make sure there's some color in there and that you can see the user doing something.

As for footage of people, try to avoid stock footage, especially those clips that show smartly dressed white people looking at a computer screen, pointing and smiling. It's like the stock photos of women laughing at salads: you see that kind of stuff everywhere and it looks soooo fake. It is nearly as bad as stock music. Nearly...

<div style="text-align: center;"><img src="/sites/dot.kde.org/files/salads.jpg" alt="" width="600" /><br />"My salad is hilarious!"</div>
<p><br /></p>

Another thing to take into account is that you have to be careful with copyrights! Anything that has not been published before 1924 and doesn't have an explicit free license or isn't explicitly in the public domain, is under a regular copyright and may not be used without the authors' or the copyright holders' express written permission. Also, make sure the person distributing the work is the owner of the work. Don't blindly trust a random Youtuber!

TIP: Apart from the <a href="https://archive.org/">Internet Archive</a>, many public institutions publish footage for free on the Internet. One such organization that comes to mind is <a href="https://www.nasa.gov/multimedia/videogallery/index.html">NASA</a>.

<h3>Music</h3>

The best way to make me switch off a video after a few seconds is by choosing the "wrong" music. Ironically, "the wrong music" is often that heartless, bland music used in nearly every single corporate video. I swear I would prefer to have hot wax poured into my ears rather than listen to another hokey, fake-quirky ukulele theme in my life.

<div style="text-align: center;"><img src="/sites/dot.kde.org/files/Smashed_Uke.jpg" alt="" width="600" /><br />The best kind of ukelele.</div>
<p><br /></p>

Conversely, sometimes what may seem the wrong music is in fact the right music. Stuff that you can't imagine going together, goes together. Salsa for Spectacle, flamenco for Falkon and power-pop for Plasma? Why not?

Again, be careful with copyrighted stuff. The same thing that applies to footage, applies to music. Besides, just because Johann Sebastian Bach died 300 years ago, doesn't mean that you can use that Von Karajan recording of his Cantatas: the recording itself will be copyrighted.

Most artists on <a href="https://www.jamendo.com/start">Jamendo</a> release their music under a liberal license as long it is not used for commercial purposes. And making a video for a competition organized by a non-profit that will not generate any money for you or for the non-profit IS NOT a commercial purpose. Also check out <a href="https://freemusicarchive.org/music">Free Music Archive</a> and especially <a href="https://freemusicarchive.org/music/Juanitos">Juanitos</a>: They make great, fun retro pseudo-ethnic music and distribute it under very generous terms.

On the other hand, go easy with <a href="https://incompetech.com/">Incompetech</a> -- his music can be lovely, but it is EVERYWHERE. If you must use one of his tracks, make sure you poke around and go for his less obvious stuff.

TIP: If you need a voice-over but are not confident that your own voice will sound okay, try <a href="https://www.fiverr.com/categories/music-audio/voice-overs?source=hplo_search_tag&pos=1&name=voice-overs">Fiverr</a>. For a few dollars, you can have a professional actor read out your script.

<h3>Editing</h3>

Avoid canned transitions that come as standard with your video-editing software (Kdenlive, right?). They may impress your parents, but everybody else thinks they're lazy.

Related to the above, avoid excessively flashy transitions, even if they are original. I am personally guilty of using animations to transition from one scene to another, but if they are not part of the story, they can draw attention to themselves and away from the real content, distracting the viewer. Most of the time, nothing beats a good clean cut.

The same goes for effects: don't overdo them! Effects are great to confer atmosphere, but if they are not doing that, they are just distracting from the story, and more often than not, make the action harder to follow. Here's looking at all those "editors" that place a screencast into the picture of a monitor! Yes, I get it: you do masks. So does everybody.

TIP: If you do need effects for your video, don't limit yourself to the catalogue supplied by your editing software. Check out things like Blender and Natron -- these programs are used a lot for effects and filters for a reason.

<h2>Farid</h2>

Apart from everything mentioned above, I would encourage participants to look into using <a href="https://www.reddit.com/r/MotionDesign/">motion graphic techniques</a> rather than sticking to simple transitions when possible. For that, you can use tools like Blender's Animation Nodes addon.

Something that is often overlooked is the matter of typography. Choose your fonts wisely! If you have text on the screen, avoid the default bland fonts. Go to <a href="https://www.fontsquirrel.com/">Font Squirrel</a> or <a href="https://fonts.google.com/">Google Fonts</a> for a great selection of free and open types to choose from.

<div style="text-align: center;"><img src="/sites/dot.kde.org/files/1fde3_COMIC-SANS.jpg" alt="" width="600" /><br />Same goes for video titles.</div>
<p><br /></p>

If you need footage, check out <a href=" https://www.pexels.com/videos/">Pexels</a> it contains a lot of high quality clips for free.

<h2>About KDE's Video Contest</h2>

KDE is looking for talented videographers and filmmakers that will help promote our free software through the medium of video. There are two categories in this contest: in the Plasma category we want participants to create a video that will promote KDE's <a href="https://kde.org/plasma-desktop">Plasma desktop</a> to the world. The Applications category is to help promote one or several <a href="https://kde.org/applications/">KDE applications</a>.

<strong>Submissions must be sent in before the 20th of February 2020.</strong> To find out more, check out the <a href="https://community.kde.org/Promo/Plasma_5.18_Video_Competition">full rules</a>.
<!--break-->