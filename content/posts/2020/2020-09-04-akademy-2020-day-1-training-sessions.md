---
title: "Akademy 2020 Day 1 - Training Sessions"
date:    2020-09-04
authors:
  - "Paul Brown"
slug:    akademy-2020-day-1-training-sessions
comments:
  - subject: "Very Good Qt Multithreading Training Session"
    date: 2020-09-04
    body: "I learned a lot :) Thanks David"
    author: "carlschwan"
---
Akademy kicked off today with training sessions on several KDE-related topics.

In the morning, Nuno Pinheiro from KDAB conducted a class on <I>UI/UX Design in QML for Desktop</I>. This online workshop contained practical exercises on dos, don'ts, and integration; and  tips on outside of the box UI design. The session started out with attendees relaying to Nuno the projects they were working on and how they hoped the lesson would help them.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/photo_2020-09-04_11-29-33.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/photo_2020-09-04_11-29-33.jpg" />
    </a>
   <figcaption>Nuno compares QML to a Sergio Leone movie.</a></figcaption>
</figure> 

Meanwhile, in room 2,  Milian Wolff, another Senior Software Engineer at KDAB, taught us about <I>Debugging and Profiling on Linux</I>. This training was at a slightly higher level and required some knowledge and experience with Qt and C++ as well as basic understanding of multithreaded programming. The session covered the most essential debugging and profiling tools on Linux and attendees learned how to use the tools and how to interpret the results.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/Trainingsday1.png" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/Trainingsday1.png" />
    </a>
   <figcaption>Milian demonstrates profiling techniques.</a></figcaption>
</figure> 

In room 3, Michael Friedrich, a Developer Evangelist at GitLab, told us how to <C>Speed Up Your Development Workflows with GitLab</I> in his best practice workshop. Michael took us through our first steps in GitLab with project management (issues, boards, labels, templates, etc.) and combined it with development workflows. We learned how to start our first merge request to solve an issue and got ideas on branching, code reviews, approval processes and added automated CI test feedback in MRs.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/EhD3pbaUMAErkLv.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/EhD3pbaUMAErkLv.jpg" />
    </a>
   <figcaption>Michael shows how GitLab can speed up your development workflow.</a></figcaption>
</figure> 

In the afternoon, Albert Astals Cid conducted a session called <I>Introduction to QML</I> in room 1. Albert taught us how to compose fluid user interfaces using the QML language and we also learned how to hook the QML side up to the business logic in C++.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/QML_Albert.png" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/QML_Albert.png" />
    </a>
   <figcaption>Albert gives a masterclass on how to develop apps using QML.</a></figcaption>
</figure> 

In room 2, David Faure told us about <I>Multithreading in Qt</I>, which is essential for developers who want to create fast and responsive applications on computers, phones, and embedded devices with an increasing number of cores. 

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/MultithreadingQt.png" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/MultithreadingQt.png" />
    </a>
   <figcaption>David tackles the tricky matter of multithreading in Qt.</a></figcaption>
</figure>

Finally, for something quite different, Dr. Carlee Hawkins talked about  <I>Implicit Bias</I> in room 3. In this session, we were asked to consider our own biases regarding race, gender, age, etc. We explored how researchers understand biases and learned how to mitigate the influence of our own biases on our thoughts.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/Bias.png" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/Bias.png" />
    </a>
   <figcaption>Carlee helps us identify and compensate for our implicit biases.</a></figcaption>
</figure>

The advice offered by Dr. Hawkins will help us make KDE more welcoming and diverse for everybody.
<!--break-->