---
title: "Welcoming our Google Summer of Code Students for 2020"
date:    2020-05-09
authors:
  - "unknow"
slug:    welcoming-our-google-summer-code-students-2020
---
by Akhil K Gangadharan and Valorie Zimmerman

<div style="text-align: center;"><img src="/sites/dot.kde.org/files/IMG_20170725_101726.jpg" alt=""  style="max-width:100%" /></div>
<p>
<br />
</p>

<b>The KDE Community welcomes our students for <a href="https://summerofcode.withgoogle.com/">Google Summer Of Code</a> 2020!</b>

We are so grateful to the GSoC program for offering this opportunity to the KDE Community and our students. By the end of the summer, we hope that each of these students will be a confident KDE Developer, happy with their summer of work, and looking forward to supporting their code and newfound friends far into the future.

<a href="https://krita.org/">Krita</a> is KDE’s professional free and open source painting program. The Krita team will mentor four students this year: L. E. Segovia will work on adding dynamic fill layers, Saurabh Kumar will implement a storyboard feature, Sharaf Zaman will bring SVG Mesh Gradients to Krita and Ashwin Dhakaita will integrate the <a href="http://mypaint.org/">MyPaint</a> brush engine. 

<a href="https://gcompris.net/index-en.html">GCompris</a> is a high quality educational software suite which includes a large number of activities for children aged 2 to 10. This year GCompris  will have two students with Deepak Kumar adding multiple datasets to several activities and Shubham Mishra will complete the multiple dataset task.
 
<a href="https://www.digikam.org/">digiKam</a> is KDE’s professional photo management software. This year digiKam will be mentoring two students: Nghia Duong will bring DNN based face recognition improvements to the app and R Kartik will make improvements to the face management workflow.

<a href="https://edu.kde.org/cantor/">Cantor</a>, which lets you use your favorite mathematical applications from within a nice KDE-integrated worksheet interface, will mentor Nikita Sirgienko. Nikita will extend the usability and feature set of Cantor and Shubham will be working on integrated documentation in Cantor.

<a href="https://kirogi.org/">Kirogi</a>, a ground control application for drones, will have Kitae Kim improve MAVLink integration. Aniket Kumar will work with <a href="https://kdeconnect.kde.org/">KDE Connect</a>, which enables communication between all your devices, to improve MMS support to the SMS client.  Paritosh Sharma will implement the Qt3D backend for <a href="https://edu.kde.org/kstars/">Kstars</a>, KDE’s astronomy software.

Anuj Bansal will work on replacing OpenLDAP based authentication with OAuth2 in KDE Websites. Jean Lima Andrade will add support to text annotation in <a href="https://community.kde.org/Incubator/Projects/marK">marK</a>, a machine learning dataset annotation tool. Prasun Kumar will  work with <a href="https://kmymoney.org/">KMyMoney</a>, a personal finance manager, to integrate SQLite format support for bank data. Davide Briani will work to bring <a href="http://en.wikitolearn.org/">WikiToLearn 2.0</a>  (collaborative textbooks) to life. For <a href="https://www.qt.io/">Qt</a>, Agisilaos Kounelis will Port QtQuickControls Calendar component to QtQuickControls2 module.

Also, Sashmitha Raghav will work to bring basic subtitling support to <a href="https://kdenlive.org">Kdenlive</a>, KDE’s video editor; <a href="https://edu.kde.org/rocs/">ROCS</a>, a Graph Theory IDE,  will have Dilson Guimarães improving graph visualization capabilities; Shashwat Jolly will to bring EteSync sync backend to <a href="https://userbase.kde.org/Akonadi">Akonadi</a>, a database to store, index and retrieve personal information. 

Detailed reports from our students will follow later this summer as the program progresses. We wish all of our students and mentors a safe, productive and a successful summer. 
<!--break-->