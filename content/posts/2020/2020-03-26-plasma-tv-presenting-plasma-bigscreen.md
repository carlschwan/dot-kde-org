---
title: "Plasma on TV: Presenting Plasma Bigscreen"
date:    2020-03-26
authors:
  - "Paul Brown"
slug:    plasma-tv-presenting-plasma-bigscreen
comments:
  - subject: "beta release for intel cpu "
    date: 2020-03-30
    body: "<p>This is a wonderful project! Wondering the plan to release it for intel cpu, so we can replace regular plasma kde-neon on mini-pc with intel cpu with this.</p>"
    author: "Rahul"
  - subject: "This is amazing"
    date: 2020-04-06
    body: "<p>After switching from win 10 to Ubuntu on my htpc, I found that it was no perfect open source solution for the living room. I'm happy to hear somebody is working on an opensource distro for the living room.</p>"
    author: "Angel"
---
<div style="text-align: center;"><img src="/sites/dot.kde.org/files/main%20menu.jpg" alt=""  style="max-width:100%" /></div>
<p align="center">
Plasma Bigscreen main menu.
</p>
<p>
<br />
</p>

<b><a href="https://plasma-bigscreen.org/">Plasma Bigscreen</a> is KDE's user interface for big TV screens.</b>

Plasma Bigscreen powers the interface on a Single Board Computer and uses the Mycroft AI voice assistant to provide a Smart TV platform. Plasma Bigscreen will deliver not only media-rich applications, but also traditional desktop applications redesigned to fit the Bigscreen experience.

<h2>Advantages of Plasma Bigscreen</h2>
<ul>
<li>
<B>Free (as in Freedom) and Open Source:</B> One of the most important goals of this project is to hand control over to the people and the industry so they can build and power smart devices without the limits of other closed TV environments. Plasma Bigscreen is completely Free and Open Source and gives everyone the freedom to use, acquire, change and redistribute the software how they see fit. It also gives people the freedom to create, innovate and improve on top of the Plasma Bigscreen and share their work with the world.
</li>
<li>
<B>Innovative:</B> Plasma Bigscreen transforms the traditional plasma workspace experience into something that is controlled with a regular TV remote control. This is new territory for KDE interface designers and requires a new thinking of how to layout applications and how to make it easy for people to interact with Plasma from their couches.
</li>
<li>
<B>Voice Control:</B> Talking of interacting from the couch, voice control provides users with the ultimate comfort when it comes to TV viewing. But most big brands not only do not safeguard the privacy of their customers, but actively harvest their conversations even when they are not sending instructions to their TV sets. We use Mycroft's Open Source voice assistant to solve this problem.

For the current beta img, the team connects to Mycroft's Home server, which by default uses Google's STT (Speech to text) which sends anonymized utterances to Google. This, of course, is not ideal, but being Open Source, you can switch out the back end and use whatever you want, even self-hosted systems like Mozilla Deepspeech. Or you can de-activate voice recognition altogether. Your choice.

With Mycroft AI, the Bigscreen team intend to give users all the comfort of a smart voice controlled assistant with the advantages of the control over you privacy you can only achieve with Open Source software. 
</li>
<li>
<B>Easy to Expand:</B> Mycroft's AI uses what are called "skills". Skills allow the assistant to learn about and perform different tasks. A weather skill, for example, lets Mycroft know about the weather and tell you what the day is going to be like; a cooking skill retrieves recipes and instructions and you can then ask Mycroft to help you make a delicious meal. There are already many skills in Mycroft's library and Mycroft AI's graphical framework for skills is built on top of <a href="https://www.qt.io/">Qt</a> and <a href="https://kde.org/products/kirigami/">Kirigami</a>, two mature development frameworks. This allows third-party developers to use Python and QML to <a href="https://mycroft-ai.gitbook.io/docs/skill-development/introduction">develop rich voice skills</a> for the platform, which means features on KDE Bigscreen will multiply and provide even more functionalities to viewers.

<p>
<br />
</p>
<div style="text-align: center;"><img src="/sites/dot.kde.org/files/audio%20kcm.jpg" alt=""  style="max-width:100%" /></div>
<p align="center">
Simple settings make Bigscreen easy to tweak.
</p>
<p>
<br />
</p>

</li>
<li>
<B>Community Supported:</B> Plasma Bigscreen was created and is being maintained by KDE developers. KDE is one of the oldest, largest Free Software communities in existence and builds and maintains literally hundreds of projects, spanning from a full-featured desktop environments and development frameworks, to  educational software and creativity apps. With the support of KDE, Plasma Bigscreen will develop quickly and grow to have as many features as users require.
</li>
</ul>

<h2>Coming to a Screen Near You</h2>

The upcoming beta release for Plasma Bigscreen is already working on the Raspberry Pi 4. It's targeted to run on a TV screen, but will also work fine on a regular monitor.

The interface is largely designed to be easy to use with a remote control. There is experimental support for HDMI-CEC in the beta image, so anyone with a TV that supports HDMI-CEC can choose to use their TV remotes.

<p>
<br />
</p>
<div style="text-align: center;"><img src="/sites/dot.kde.org/files/youtube.jpg" alt=""  style="max-width:100%" /></div>
<p align="center">
The YouTube app.
</p>
<p>
<br />
</p>

As one of the key features of Plasma Bigscreen is Mycroft's voice-controlled applications/skills, it's recommended to use a USB/Bluetooth remote with a microphone to try it out. Some recommended generic USB remotes are the WeChip <a href="https://www.amazon.com/WeChip-G20-Control-Wireless-Sensing/dp/B07P322VRP">G20</a> / <a href="http://www.wechipbox.com/wechip-w2-voice-remote-control-with-touch-pad-24g-wireless-p-602.html">W2</a> remote controls. It can also be used with a keyboard / mouse and any USB microphone.

For a more in-depth look at Plasma Bigscreen, check out <a href="https://notmart.org/blog/2020/03/plasma-bigscreen/">Marco Marin's</a> and <a href="https://aiix.tk/dev/plasma-bigscreen-a-dive-into-mycroft-skills-voice-applications-more/">Aditya Mehra's</a> write ups on this new project.
<!--break-->