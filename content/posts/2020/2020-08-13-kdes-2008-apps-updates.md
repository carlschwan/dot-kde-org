---
title: "KDE's 20.08 Apps Updates"
date:    2020-08-13
authors:
  - "Paul Brown"
slug:    kdes-2008-apps-updates
---
<b><a href="https://kde.org/announcements/releases/2020-08-apps-update/">The updates to KDE apps released today</a> are many, contain a wide array of changes, and cover an impressive number of applications.</b>

<a href="https://kde.org/applications/en/system/org.kde.dolphin">Dolphin</a>, KDE's file explorer, for example, adds previews for more types of files and improvements to the way long names are summarized, allowing you to better see what each file is or does. Dolphin also improves the way you can reach files and directories on remote machines, making working from home a much smoother experience. It also remembers the location you were viewing the last time you closed it, making it easier to pick up from where you left off.

<figure class="topImage">
    <a href="/sites/dot.kde.org/files/dolphin_preview.png" data-toggle="lightbox">
        <img src="/sites/dot.kde.org/files/dolphin_preview.png" style="width: 100%; height: auto;" alt="Cover" />
    </a>
</figure>

For those of you into photography, KDE's professional photo management application, <a href="https://kde.org/applications/en/graphics/org.kde.digikam">digiKam</a> has just released its <a href="https://www.digikam.org/news/2020-07-19-7.0.0_release_announcement/">version 7.0.0</a>. The highlight here is the smart face recognition feature that uses deep-learning to match faces to names and even recognizes pets.

If it is the night sky you like photographing, you must try the new version of <a href="https://kde.org/applications/en/education/org.kde.kstars">KStars</a>. Apart from letting you explore the Universe and identify stars from your desktop and mobile phone, new features include more ways to calibrate your telescope and get the perfect shot of heavenly bodies.

<figure class="topImage">
    <a href="/sites/dot.kde.org/files/kstars-main-screen.jpg" data-toggle="lightbox">
        <img src="/sites/dot.kde.org/files/kstars-main-screen.jpg" style="width: 100%; height: auto;" alt="Cover" />
    </a>
</figure>

And there's much more: KDE's terminal emulators <a href="https://kde.org/applications/en/system/org.kde.konsole">Konsole</a> and <a href="https://kde.org/applications/en/system/org.kde.yakuake">Yakuake</a>; <a href="https://kde.org/applications/en/multimedia/org.kde.elisa">Elisa</a>, the music player that looks great both on your desktop and phone; the text editor <a href="https://kde.org/applications/en/utilities/org.kde.kate">Kate</a>; KDE's image viewer <a href="https://kde.org/applications/en/graphics/org.kde.gwenview">Gwenview</a>; and literally <a href="https://kde.org/applications/">dozens of other applications</a> are all getting updated with new features, bugfixes and improved interfaces to help you become more productive and making the time you spend with KDE software more pleasurable and fun.

Check out the <a href="https://kde.org/announcements/releases/2020-08-apps-update/">full announcement</a> for all the goodies that lay in store for you.
<!--break-->