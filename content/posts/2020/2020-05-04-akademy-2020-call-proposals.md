---
title: "Akademy 2020 \u2014 Call for Proposals"
date:    2020-05-04
authors:
  - "sealne"
slug:    akademy-2020-call-proposals
comments:
  - subject: "FRIENDS!"
    date: 2020-05-04
    body: "<p>That picture banner made me think ... I miss a hug from most/all of these people. I guess it will have to be virtual hugs for now.</p>"
    author: "Sune"
---
<div style="text-align: center;"><img src="/sites/dot.kde.org/files/slices.jpg" alt=""  style="max-width:100%" /></div>
<p>
<br />
</p>

<a href="https://akademy.kde.org/2020">Akademy 2020</a> is getting closer and the KDE Community is warming up for its biggest yearly event. If you are working on topics relevant to KDE, this is your chance to present your work and ideas to the community at large.

<strong>Akademy 2020 will take place online from Friday the 4th to Friday the 11th of September 2020</strong>. Training sessions will be held on Friday the 4th of September and the talks will be held on Saturday the 5th and Sunday the 6th of September. The rest of the week (Monday - Friday) will be Birds-of-a-Feather meetings (BoFs), unconference sessions and workshops.

If you think you have something interesting to present, tell us about it. If you know of someone else who should present, encourage them to do so too.

Talk proposals on topics relevant to the KDE Community and technology are:

<ul>
  <li>Topics related to KDE's current <a href="https://community.kde.org/Goals">Community Goals</a>:</li>
  <ul>
    <li>Consistency</li>
    <li>All About the Apps</li>
    <li>Wayland</li>
  </ul>
  <li>KDE In Action: use cases of KDE technology in real life, be it on mobile, desktop deployments, embedded, and so on</li>
  <li>Overview of what is going on in the various areas of the KDE community</li>
  <li>Collaboration between KDE and other Free Software projects</li>
  <li>Release, packaging, and distribution of software by KDE</li>
  <li>Increasing our reach through efforts such as accessibility, promotion, translation and localization</li>
  <li>Improving our governance and processes, community building</li>
</ul>

Don't let this list restrict your ideas though! You can submit a proposal even if it doesn't fit in this list of topics as long as it is relevant to KDE. To get an idea of talks that were accepted previously, check out the program from previous years: <a href="https://conf.kde.org/en/akademy2019/public/events">2019</a>, <a href="https://conf.kde.org/en/akademy2018/public/events">2018</a>, and <a href="https://conf.kde.org/en/akademy2017/public/events">2017</a>.

Full details can be found in the <a href="https://akademy.kde.org/2020/cfp">Call for Proposals</a>.

<strong>The deadline for submissions is Sunday 14th June 2020 23:59 UTC.</strong>
<!--break-->