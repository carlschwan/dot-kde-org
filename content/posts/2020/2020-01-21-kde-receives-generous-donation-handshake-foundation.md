---
title: "KDE Receives Generous Donation from the Handshake Foundation"
date:    2020-01-21
authors:
  - "Paul Brown"
slug:    kde-receives-generous-donation-handshake-foundation
---
<div style="text-align: center;"><img src="/sites/dot.kde.org/files/hnadshake_1500.jpg" alt="" width="800" /></div>
<p>
<br />
</p>
<b>We are excited to announce that KDE e.V. has received a donation of 880,000 <a href="https://handshake.org/how-it-works">HNS coins</a> (roughly 79,000 euros) from <a href="https://handshake.org/">the Handshake Foundation</a>.</b> 

This is the not the first time Handshake has made a substantial donation to the KDE Community. Back in 2018 Handshake donated approximately USD 300,000 to KDE which was used to finance projects and fund activities.

"The Handshake Naming System is a child of the Open Source Community", says Andrew Lee from the Handshake Foundation. "Just like Handshake, KDE has championed privacy and freedom since the beginning and has paved the way forward in creating usable tools made for the masses.

"Personally, I've used KDE software since the early 2000s, and I've seen it grow and flourish. I think, many people today would be surprised to hear that Apple Safari, for example, was based originally on Konqueror, a web browser created by the KDE Community. The Handshake Naming System is proud to be able to make a donation to the KDE team. It is our way of showing appreciation for KDE, as much of the development in the Open Source world would not have been possible without it."

KDE would like to thank the Handshake Foundation for their continued generosity and the support they offer to FLOSS communities across the spectrum. This contribution will help KDE continue with its commitment to create Free Software for everyone, finance events and sponsor community members. 

You can help KDE too! All you need to do is <a href="https://community.kde.org/Get_Involved">join the Community</a> and be part of our mission to help people maintain their privacy and their control over their digital lives with Free Software.
---
Photo by <a href="https://unsplash.com/@cytonn_photography">Cytonn Photography</a> on <a href="https://unsplash.com/s/photos/handshake?utm_source=unsplash">Unsplash</a>.
<!--break-->