---
title: "Videos From KDE Talks at FOSDEM"
date:    2020-02-12
authors:
  - "jriddell"
slug:    videos-kde-talks-fosdem
---
<div style="text-align: center;"><img src="/sites/dot.kde.org/files/20200202_141558_narrow_1500.jpg" alt="" width="800" /></div>
<p>
<br />
</p>

FOSDEM is one of the world's largest Free software conferences and was held last weekend in Brussels.  There were <a href="https://fosdem.org/2020/schedule/">hundreds of talks</a> and videos are available for most of them.  KDE had a stall there and KDE contributors delivered three talks spread between different devrooms.

<h2><a href="https://fosdem.org/2020/schedule/event/om_qml/">Rendering QML to make videos in Kdenlive</a></h2>
<b>With Akhil Gangadharan Kurungadathil</b>

How QML, a language prominently used for designing UI, could be used to create title video clips containing text and/or images. The videos can then be rendered and composited over other videos in the video-editing process. Kdenlive's Google Summer of Code 2019 project tried to achieve this and is still under active development.

QML is used primarily for UI development in Qt Applications. It provides an easy way of designing and creating interactive, clean and a modern UI. Kdenlive is a popular non-linear open-source video editor and it currently makes use of XML to describe title clips -- clips which contain text or images used to composite over videos. XML requires more processing in the backend as one needs to explicitly write code for, say an animation of the text. Using QML eases this restriction, making the backend more robust and maintainable as rendering in QML makes use of a dedicated Qt Scene Graph. Kdenlive's Google Summer of Code 2019 student Akhil Gangadharan Kurungadathil tried to achieve this by creating a new rendering backend library and a new MLT QML producer which is still under active development. Owing to the dedicated scene graph while rendering, this could also possibly lead to greater overall performance.

  <video preload="none" controls="controls">
    <source src="https://video.fosdem.org/2020/UB2.147/om_qml.webm" type='video/webm; codecs="vp9, opus"' />
  </video>
  
<h2><a href="https://fosdem.org/2020/schedule/event/kde_on_freebsd/">KDE on FreeBSD</a></h2>
<b>With Adriaan de Groot</b>

The state of KDE (the Plasma desktop and applications) on FreeBSD, what works, what needs better support lower in the stack. How do we get rid of HAL?

  <video preload="none" controls="controls">
    <source src="https://video.fosdem.org/2020/AW1.121/kde_on_freebsd.webm" type='video/webm; codecs="vp9, opus"' />
  </video>
  
<h2>  <a href="https://fosdem.org/2020/schedule/event/kde_itinerary/">KDE Itinerary</a></h2>
<b>With Volker Krause</b>

A privacy by design travel assistant.
    
  <video preload="none" controls="controls">
    <source src="https://video.fosdem.org/2020/H.2215/kde_itinerary.webm" type='video/webm; codecs="vp9, opus"' />
  </video>
<!--break-->