---
title: "Gina H\u00e4u\u00dfge and OctoPrint"
date:    2020-08-15
authors:
  - "Paul Brown"
slug:    gina-häußge-and-octoprint
---
<figure class="float-md-right w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/gina_printer_1000_0.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/gina_printer_1000_0.jpg" />
    </a>
   <figcaption>Gina Häußge and printer.</a></figcaption>
</figure> 

<I>Gina Häußge's biggest claim to fame may be <a href="https://octoprint.org/">OctoPrint</a>, the most popular software for managing 3D printers from small, single board computers -- like the Raspberry Pi, Odroid, etc..

<I>OctoPrint is currently Gina's main occupation, but it wasn't always like that. In this Interview she talks about how a tech-loving child nearly got eaten up by the software industry, but thankfully managed to escape and transform her FLOSS passion project into her day job (plus a whole bunch of other nerdy stuff).</I>

<I>Enjoy.</I>

<span style="color:#55f;font-weight:bold">Gina Häußge:</span> Hello there!

<span style="color:#f55;font-weight:bold">Paul Brown:</span> Hello Gina! You are early!

<span style="color:#55f;font-weight:bold">Gina:</span> Well, this gives me a nicer start to the day than having to field endless support requests along the lines of "it's broken, pls fix kthxbai".

<span style="color:#f55;font-weight:bold">Paul:</span> A good place to start, because I understand you just released a new version of OctoPrint. How did that go?

<span style="color:#55f;font-weight:bold">Gina:</span> It's still a bit tricky to say at the moment. I pushed 1.4.1 on Tuesday and rollout looked good, apart from the usual issues experienced by people due to hiccups in their environment. Then I got three bug reports, one somewhat critical and thus on Thursday, I released hotfix 1.4.2. And since then I've been monitoring the rollout of that.

So far it looks like a good update, and that makes me happy obviously. Was quite the weird time developing this the past few months with Corona dominating all our lives one way or the other.

<span style="color:#f55;font-weight:bold">Paul:</span> Two topics in there we will catch up with later, but first let's get into how you got here. Because most superheroes have an origin story: Batman fell into a cave full of bats, Carol Danvers had an accident with an alien device and became Captain Marvel... What is yours? What happened that put you on the road to become the (let's not mince words here) massive nerd you currently are?

<span style="color:#55f;font-weight:bold">Gina:</span> I guess I've just always been that way. I grew up in the 80s in Germany, and I can't remember a time when I was NOT drawn to technical stuff, wanting to know how things tick, wanting to build.

My parents thankfully encouraged that and provided a steady supply of fun stuff to learn and try and build with, like electronics kits, chemistry kits, a microscope, tons of Lego and so on. Not given for a girl in the 80s, so I'm really grateful to them.

At some point when I was maybe six or seven, we got an old computer from my uncle. I wanted to play on this thing obviously, but my dad took me aside and told me "this is a tool to work with, not a toy. I can show you something cool to do with it though". And then he introduced me to BASIC and programming. I was pretty much instantly hooked.

<span style="color:#f55;font-weight:bold">Paul:</span> Ah! The Defining Moment<span style="vertical-align: super; font-size: smaller;">TM</span>!

<span style="color:#55f;font-weight:bold">Gina:</span> It was a bit like playing/getting creative with Lego, but without the bricks running out and the ability to create your own bricks if need be ^^.

And yeah, definitely a defining moment. I continued exploring the world of programming while growing up, went from BASIC to Pascal and then C and C++, and quite early figured out what I wanted to do in life (once I learned that "Software Engineer" existed as a job).

After graduating from school, I went to university to study Computer Science, got my degree there, then after a brief stop in academics, I switched into the industry and started my life as a professional Software Engineer. Apparently, I was fairly good at that, meaning I got promoted... further and further away from the code that I loved shaping so much.

<span style="color:#f55;font-weight:bold">Paul:</span> That seems to be a trend: good developers get promoted up into management.

<span style="color:#55f;font-weight:bold">Gina:</span> It's a horrible trend. I really wish there were more career options for good developers. I think of myself as a good dev, but I really abhor management duties.  Can do that in a pinch, but I don't enjoy it. So yeah... that was not so fun.

So back in November 2012, I bought myself a 3D printer after having circled around the idea of getting one ever since seeing them in action at <a href="https://har2009.org/">Hacking At Random 2009</a> (Dutch hacker festival). Built it, set it up, had a ton of fun in doing so.

<span style="color:#f55;font-weight:bold">Paul:</span> Were you still in the industry back then?

<span style="color:#55f;font-weight:bold">Gina:</span> Yeah... I was still living a life as a "Technical Architect" at a somewhat larger IT consulting and custom solution development company. Team manager and architect by day, nerd by night.

<span style="color:#f55;font-weight:bold">Paul:</span> So... 3D printer = "Defining Moment - Part II"?

<figure class="float-md-left w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/battlestation.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/battlestation.jpg" />
    </a>
   <figcaption>Gina's current battlestation.</a></figcaption>
</figure> 

<span style="color:#55f;font-weight:bold">Gina:</span> I guess so :D. Christmas came around and with it, a 2 week vacation. I figured I might as well scratch the by that point somewhat urgent itch of getting my hands on some code again and wrote a web interface for my 3D printer, mostly from scratch.

I had no idea how printers worked, but I just decided to figure that out along the way and use a communication layer from another project in the meantime. And thus, what is today known as OctoPrint was born.

I publicly posted about what I was working on December 25th 2012 as far as I remember. Just a small post in the Google+ 3D printing community. Threw my work up on Github a couple days later after it did everything I wanted it to do for now. I figured "Ok, that's done now" and went back to work a couple days after New Year's.

And then the messages started. People from all over the world were suddenly getting in touch with me about how what I had built was what they'd been looking for so long now, and that it was almost exactly what they needed, but could I maybe add this little thing or make it work with that particular printer?

I suddenly had a full grown Open Source project on my hands. I did it next to my (quite stressful I might add) day job. Even talked to my boss and got him to reduce my contract to 4 days a week (at reduced pay of course) to be able to dedicate one day per week to what was now called OctoPrint.

But the project grew and grew and ate more and more of my time. No evenings, no weekends, no vacation without taking care of OctoPrint. Sometime in early 2014, I realized that this was not sustainable that way. I was constantly tired, my day job was more and more unfulfilling, what was supposed to be just a pet project more and more demanding, but also actually helping people out there contrary to what I got paid for (or at least it felt that way).

So, when I got an offer by a technology company that also was dipping its toes into 3D printing to get employed by them to work full time on OctoPrint while keeping full control and ownership of it, I took that opportunity.

<span style="color:#f55;font-weight:bold">Paul:</span> Oh! That was lucky!

<span style="color:#55f;font-weight:bold">Gina:</span> Indeed it was, and I'm still really happy that I could do that jump. I quit my old day job and started my life as a full time Open Source developer in August 2014.

<span style="color:#f55;font-weight:bold">Paul:</span> Well, not lucky. Based on merits, obviously.

<span style="color:#55f;font-weight:bold">Gina:</span> I guess it was a combination of luck and being able to display what I could do if given the chance. Full time meant a whole different pace and way bigger sub projects that I could now take on. So, I did. OctoPrint continued to grow and its community with it. It was sometimes a bit lonely working from home, but it also had its big advantages. No interruptions ^^.

Well, apart from the mailman dropping off packages for me or the neighbours.

<span style="color:#f55;font-weight:bold">Paul:</span> Printer parts?

<figure class="float-md-right w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/printers_0.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/printers_0.jpg" />
    </a>
   <figcaption>Part of Gina's collection of 3D printers.</a></figcaption>
</figure> 


<span style="color:#55f;font-weight:bold">Gina:</span> Printer parts, drones, electronic components ^^. And of course, a stream of test equipment. Printer electronics, Raspberry Pis and other SBCs, as that's the primary platform for OctoPrint to run on. So, every time a new version came out, I had to get one to test the builds against.

Life was all in all good.

But then, in April 2016, there was a big complication. The company that had hired me had to downsize some departments for financial reasons. That meant they also had to pull the plug on our arrangement.

<span style="color:#f55;font-weight:bold">Paul:</span> Oh no! What happened?

<span style="color:#55f;font-weight:bold">Gina:</span> Well, I found myself in the situation that I had this huge Open Source project on my hands that really was not a thing you could still do on the side, but I also had no funding. I could just have said, "well, okay then" and gotten myself a regular job again. No risk, steady paycheck. But not as exciting either.

So, I did what just two or so years prior I had laughed off and decided to go self-employed. I figured I would probably kick myself for the rest of my life if I didn't even TRY to see if I could maybe continue working on OctoPrint full time funded through crowdfunding. So, I went for that. Set up a Patreon campaign, pushed out a "Call to Action" on all social feeds of the project and hoped for the best.

<span style="color:#f55;font-weight:bold">Paul:</span> I am on the edge of my seat.

<span style="color:#55f;font-weight:bold">Gina:</span> To this day it still amazes me that this actually worked. People put their money where their mouth is and actually started supporting me. So did one or two companies. After two months I was back at a sustainable level of income.

<span style="color:#f55;font-weight:bold">Paul:</span> What a roller-coaster. But it makes sense that 3D printer manufacturers would support your work.

<span style="color:#55f;font-weight:bold">Gina:</span> And since then it has just... worked. The only reason why OctoPrint has seen constant maintenance, improvements and growth over the past four years is its awesome community that helps fund it.

<span style="color:#f55;font-weight:bold">Paul:</span> Not to get to deep into monetary details here, but would you say that most income comes from community members then?

<span style="color:#55f;font-weight:bold">Gina:</span> Oh, definitely. I also have some ad revenue and income from kit resales through partners. But the major chunk is and always has been community donations, which absolutely amazes me.

<span style="color:#f55;font-weight:bold">Paul:</span> OctoPrint also has a healthy plugin ecosystem.

<span style="color:#55f;font-weight:bold">Gina:</span> Yes, and I'm really happy about that!

<span style="color:#f55;font-weight:bold">Paul:</span> Many of the plugins are provided by community members. How about the core? Do you get many contributions to that?

<figure class="float-md-left w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/Darth.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/Darth.jpg" />
    </a>
   <figcaption>Darth Häußge finds your lack of faith in the financial viability of passion-driven open source projects disturbing.</a></figcaption>
</figure> 

<span style="color:#55f;font-weight:bold">Gina:</span> Sadly no. Most of the core development is still done by me alone. I might have partially caused that by introducing the plugin system back in 2015.

That was an intentional decision though, as that way people who want features can implement them but also maintain them. It's not a case of "here is a piece of code that does this and that against this piece of obscure hardware. Have fun keeping it working from now on. Cheers!".

Because THAT isn't really manageable and would have quickly made OctoPrint become a giant piece of hard-to-maintain and impossible-to-test code. I didn't want that, not only for my sake, but also for the sake of its users.

No one wants to install a giant piece of bloatware that does everything but cook coffee, when all you want is to stream files to a 3D printer. So, people can install core OctoPrint and then add and mix and match whatever plugins they want.

<span style="color:#f55;font-weight:bold">Paul:</span> What about a team? Wouldn't you like to have, I don't know, two or three stable colleagues to work with?

<span style="color:#55f;font-weight:bold">Gina:</span> Oh, definitely! But the funding for that isn't there. I have some people who are more or less regular contributors, help me with ticket triage and support and such. But sadly, I've not been able to attract a core team of maintainers.

I sometimes think that might also be due to the audience of OctoPrint. In the beginning, it was mostly used by developers who also happened to have a 3D printer. 3D printers were still expensive and niche, and you needed to know your tech and be prepared to tinker with things to get them to work properly.

These days 3D printers are sold in hardware stores for less than 500 bucks and the people who use them range from miniature hobbyists to RC fans to HAM radio operators, retired engineers who want to print stuff for their model trains, school children...

I think there's not that big of an overlap between people who know how to write code and people who own a 3D printer as there used to be. That means it's a bit tricky to find people with the knowledge and enthusiasm to help beyond "I found this bug, here's a fix, bye".

<span style="color:#f55;font-weight:bold">Paul:</span> But still, it is risky to have only one developer. For the project, I mean. The bus factor and all that...

<span style="color:#55f;font-weight:bold">Gina:</span> I totally agree. It's one of the things I worry about. But I don't really know how to change it other than trying to recruit from the pool of plugin developers here and there ;). Maybe I should leave in more bugs so people feel motivated to help :P.

<span style="color:#f55;font-weight:bold">Paul:</span> That would be just evil. Let's move on to happier topics: your recent release. In the latest, say, two or three releases, what do you think is the biggest change? What things will users most notice?

<span style="color:#55f;font-weight:bold">Gina:</span> That would probably be the granular permission system introduced in 1.4.0. See, before that release, OctoPrint only knew two classes of users. Regular users and admins. In 1.4.0 that changed. There's now a broad range of permissions assigned to all the functionality built into OctoPrint, and users can create their own groups that e.g. allow printer control but no access to the time-lapse functionality, or - since a bug fix in 1.4.1 - create, read-only users that can look but not touch, stuff like that.

This was a feature requested again and again over the years but a behemoth to implement. Thankfully, I got a huge contribution for that from a long term on-and-off contributor and, after some intense refactoring and cursing here and there, we managed to get this implemented in tandem.

<span style="color:#f55;font-weight:bold">Paul:</span> So you can have people watching the print progress, for example, but cannot mess it up by pressing the wrong button?

<span style="color:#55f;font-weight:bold">Gina:</span> Yeah, exactly.

<span style="color:#f55;font-weight:bold">Paul:</span> I must update.

<figure class="float-md-right w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/bouldering.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/bouldering.jpg" />
    </a>
   <figcaption>[Insert metaphor here for Gina's journey from software industry drone to independent FLOSS project leader.]</a></figcaption>
</figure> 

<span style="color:#55f;font-weight:bold">Gina:</span>The other big thing that people probably don't notice, but will notice as OctoPrint continues to simply work, is that from 1.4.0 OctoPrint is also compatible with Python 3. OctoPrint's backend is written in Python and, up until 1.4.0, it only supported Python 2. That version of Python, however, has been deemed end-of-life as of January 1st of 2020 and that meant that OctoPrint had to be ported to Python 3.

Sadly, the two are incompatible here and there, which made it a huge undertaking. But I thankfully also got some help.

<span style="color:#f55;font-weight:bold">Paul:</span> What about the Next Big Thing<span style="vertical-align: super; font-size: smaller;">TM</span>? What does the future hold for OctoPrint users?

<span style="color:#55f;font-weight:bold">Gina:</span> One big thing I'm currently laying the groundwork for by after-hours learning is a new user interface.

<span style="color:#f55;font-weight:bold">Paul:</span> Oooh! That sounds nice!

<span style="color:#55f;font-weight:bold">Gina:</span> Currently OctoPrint still runs a, by now, outdated version of Bootstrap, the UI isn't responsive, it's a bit messy on mobile devices and overall, it just looks aged.

The thing is, I can't just rip it out and build a new one, because there are a ton of third party plugins out there in the ecosystem that rely on it and that would no longer work if it was just removed suddenly.

Hence, I introduced the ability to add alternative user interfaces through plugins two years ago and I am currently catching up on current technologies for browser-based user interfaces to make a technological decision. I am currently working through online courses for both React and VueJS.

The idea will then be to create the new UI as a bundled plugin next to the existing one, offer extension hooks for the existing plugins so that they can hook into the new and old interfaces, and then, once there's some good amount of support, make the new one the primary, but keep the old in a bit longer.

Updates like this get tricky when you have to keep the health of a vibrant plugin ecosystem in mind :)

<span style="color:#f55;font-weight:bold">Paul:</span> You probably realize that about half your userbase will complain when you make the change, right?

<span style="color:#55f;font-weight:bold">Gina:</span> Obviously. But if I didn't push new features or improvements whenever someone complained I would have stopped working on OctoPrint back in 2013 :P.

<span style="color:#f55;font-weight:bold">Paul:</span> Heh heh! Damn right

<span style="color:#55f;font-weight:bold">Gina:</span> People always complain. They even complain when you tell them "Hey, your printer's firmware is known to have a flaw that could mean it can burn down your house, MAYBE you should look into that ASAP".

<span style="color:#f55;font-weight:bold">Paul:</span> OctoPrint reminds me of this ALL THE TIME!

<span style="color:#55f;font-weight:bold">Gina:</span> You should really fix your firmware then; it's not doing that for fun ;).

<span style="color:#f55;font-weight:bold">Paul:</span> What other stuff are you into? You seem like the kind of person who would have plenty of other creative hobbies and interests.

<span style="color:#55f;font-weight:bold">Gina:</span> Just too little time for them XD .

<span style="color:#f55;font-weight:bold">Paul:</span> Have you picked up anything new during lockdown?
 
<span style="color:#55f;font-weight:bold">Gina:</span> First of all I'm a passionate video gamer. I'm currently working my way slowly but steadily through Witcher 3, still have a bunch of XCOM expansions to work through, and there are also some nice new things coming out this year.

Last year I got into pen & paper RPGs and bouldering, but sadly both got somehow nuked by lockdown due to social distancing mandates.

<span style="color:#f55;font-weight:bold">Paul:</span> Bouldering?

<span style="color:#55f;font-weight:bold">Gina:</span> Yeah, rock-climbing. No rope, 5 - 6 m height tops, crash pad at the bottom. I have a bit of a fear of heights and sit too much, so when a friend from university asked if I wanted to join him in trying that out, I was "heck yeah" - a chance to combat a fear, get in shape AND solve problems? Count me in

<span style="color:#f55;font-weight:bold">Paul:</span> Ah!

<span style="color:#55f;font-weight:bold">Gina:</span> I also tinker with electronics, just built myself my first custom mechanical macropad in fact.

<span style="color:#f55;font-weight:bold">Paul:</span> What is that?

<span style="color:#55f;font-weight:bold">Gina:</span> Think a stand-alone numpad for your keyboard, with mechanical switches and programmable.

<span style="color:#f55;font-weight:bold">Paul:</span> Any other coding projects?

<figure class="float-md-left w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/test_rigx1000.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/test_rigx1000.jpg" />
    </a>
   <figcaption>Gina's OctoPrint test rig.</a></figcaption>
</figure> 

<span style="color:#55f;font-weight:bold">Gina:</span> Currently not. But I recently did a bit of an extracurricular task related to OctoPrint and built myself a test-rig to help with automating my pre-release tests. That involved playing with a laser cutter, coding up some automation tasks in a tool for that and some enclosure design and such and was a TON of fun, plus it now saves me literally hours per release.
 
I have two Pis sitting here just for update tests and on each update, I will flash them to various versions of the OctoPi image (a Raspberry Pi distribution for OctoPrint), the provision that, get it up to a base version and then test whether the update from that version to the new release or release candidate works as it should and if everything looks alright after. Having to flash the SD cards for that manually, doing provisioning and all that for a whole test matrix on each release was horribly time consuming. So, I automated that.

<a href="https://octoprint.org/blog/2020/07/29/automating-octoprints-release-tests/">I wrote a post about that on the OctoBlog with some pictures</a>.

I also somewhat recently built myself a hardware button for controlling my Sonos system (volume control, pause, forward, back), that involved soldering, mechanical design and some programming. I used <a href="https://github.com/foosel/SonosRemote">an ESP8266 and MicroPython to interface with that and now can control stuff with it</a>.

Then I pimped a scooter for getting around 36c3, with LED effects and speedometer and so on.

I also have home automation as a hobby, so there are sensors throughout my flat and I sometimes spend way too much time on automating stuff in NodeRED just to save me some time elsewhere. I no longer forget to open the dishwasher after it has run for example, because my home automation now detects when it's done through power consumption and sends me a push notification on my phone. I do small projects like that here and there for fun.

Oh... and sometimes I bake bread ^^.

<span style="color:#f55;font-weight:bold">Paul:</span> Bake bread... Is this a lockdown thing?

<span style="color:#55f;font-weight:bold">Gina:</span> In my case it started in early 2019 and I was a bit annoyed when I couldn't find any yeast and flour anymore due to everyone ELSE starting with that too during the lockdown. But in fact, I wasn't able to pick anything new up during lockdown, as I was too busy not drowning in support requests.

<span style="color:#f55;font-weight:bold">Paul:</span> Oh! Are people printing more during lockdown?

<span style="color:#55f;font-weight:bold">Gina:</span> The thing with lockdown was everyone apparently had a ton of more time all of a sudden to use their 3D printers. And with a huge demand for PPE (personal protection equipment) and not enough availability, a lot of people jumped in and helped printing face shields and such.

So that meant a HUGE increase in prints, more people running into problems that needed help and such. The number of OctoPrint instances I see via the anonymous usage tracking spiked. So did the hours of prints per day. I spent April and May under a ton of stress due to that. Was drowning in mails, tickets, forum posts and so on. It was a bit insane.

It got to a point where I was actually somewhat seeing red whenever I saw some post scroll past me on Twitter along the lines of "now that you have so much free time on your hands, why not learn something new" XD.

<span style="color:#f55;font-weight:bold">Paul:</span> Again it sounds like you need a team! Apart from becoming a stable developer, how else can people help?

<span style="color:#55f;font-weight:bold">Gina:</span> Helping out others on the forums is always good! So is helping the Release Candidates! The current release has once again shipped with some bugs that could have been detected if people with less mainstream kind of setups or workflows would have participated in the month long RC phase.

What we need there is diversity: the more variant in environments the better. I cannot test against every possible hardware and workflow combination under the sun, but if we get a couple hundred RC testers together for each release, and they also take the time to properly report issues, with logs and such, that can ensure that stuff works fine

<span style="color:#f55;font-weight:bold">Paul:</span> Also donations? What is your Patreon page?

<span style="color:#55f;font-weight:bold">Gina:</span> I no longer have just a Patreon page, there's a multitude of ways now to support my work financially: <a href="https://octoprint.org/support-octoprint/">Patreon, PayPal, Github Sponsors</a> to name a few.

<span style="color:#f55;font-weight:bold">Paul:</span> So fascinating. We could go on for hours, but I think I should let you get back to your hobbies. And OctoPrint, of course!

<span style="color:#55f;font-weight:bold">Gina:</span> It's a work day, so it's OctoPrint only for the next couple of hours ^^.

<span style="color:#f55;font-weight:bold">Paul:</span> Thank you so much for your time.

<span style="color:#55f;font-weight:bold">Gina:</span> Thank you for the opportunity to share my journey :)

<span style="color:#f55;font-weight:bold">Paul:</span> We are all looking forward to your talk at Akademy! See you there!

<span style="color:#55f;font-weight:bold">Gina:</span> See you :)

<I>Gina will be delivering the keynote speech <a href="https://conf.kde.org/en/akademy2020/public/events/234">"Adventures in Open Source Development"</a> on September 5 at <a href="https://akademy.kde.org/2020">Akademy 2020</a>.</I>

<I><a href="https://akademy.kde.org/2020/register">Register now</a> and don't miss it!</I>

<div style="text-align: center;"><img src="/sites/dot.kde.org/files/IMAG0242.jpg" alt="" class="img-fluid"/></div>
<!--break-->