---
title: "KDE Video Competition Winners"
date:    2020-02-25
authors:
  - "unknow"
slug:    kde-video-competition-winners
---
By Niccolo Venerandi

<div style="text-align: center;"><img src="/sites/dot.kde.org/files/oscar-3679610_1280.jpg" alt="" width="800" /></div>
<p>
<br />
</p>

<b>On the 20th of February, our first video contest finished and winners were decided by a panel of judges.</b>

This was the first time we run a video contest and we were really excited to see how much the community got involved, the quality of the videos and the onboarding effect that this contest would have.

All the submitted videos show great effort on behalf of the creators and it was extremely difficult to select the winner -- at one point there was even a tie! But, at last, we were able to select a winner and finalists for each category.

Without further ado, let's dive into the results:

<h2>Plasma Video Contest</h2>

The winner of the Plasma contest is Skye Fentras with their video "Plasma 2020". Congratulations! Skye will get a fantastic <a href="https://www.tuxedocomputers.com/">TUXEDO</a> gaming PC, featuring a powerful Intel core i7, 16GB of RAM, 250GB NVMe SSD, 2TB HDD and an Nvidia GTX1050Ti video card.

Check out Skye's video below:

<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/2RyE0kZYdD0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>
<p align="center">
<b>Skye Fentras: "Plasma 2020"</b>
</p>

The three finalists for the Plasma category are:

<ul>
<li>KonqiDragon with his video "Plasma 5.18 Promo"</li>
<li>Nayam Amarshe with "This is Plasma"</li>
<li>Kubee for "This is Plasma"</li>
</ul>

All finalists will receive a package with a KDE baseball cap, a plush Tux, and more.

See their videos below:

<p align="center">
<iframe src="https://player.vimeo.com/video/387880472"; width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
</p>
<p align="center">
<b>KonqiDragon: "Plasma 5.18 Promo</b>
</p>

<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/XsnWZU9gWLA"; frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>
<p align="center">
<b>Nayam Amarshe: "This is Plasma"</b>
</p>

<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/PFQrsQcmHEw"; frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>
<p align="center">
<b>Kubee: "This is Plasma"</b>
</p>

<h2>Apps Video Contest</h2>

The second category of this contest was for videos showcasing KDE applications. The winner of this category is KonqiDragon, with their stunning "KDE applications Promo" video. KonqiDragon wins a TUXEDO InfinityBox, featuring an Intel core i3, 16GB of RAM and 250GB of SSD.

See the winning video below:

<p align="center">
<iframe src="https://player.vimeo.com/video/387804286"; width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
</p>
<p align="center">
<b>KonqiDragon: "KDE Applications Promo"</b> 
</p>

The two finalists for this category are:

<ul>
<li>Katia with "Dolphin File Manager" and "Meet KDE Applications"</li>
<li>Tauheedelahee with "KDE Plasma Applications Promotional Video"</li>
</ul>

Again, they will both receive a package with a KDE baseball cap, a plush Tux, and more.

See their videos here:

<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/U0uWQ0eh5Sc"; frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>
<p align="center">
<b>Katia: "Dolphin File Manager"</b>
</p>

<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/LN8BrMXFnNs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>
<p align="center">
<b>Katia: "Meet KDE Applications"</b>
</p>

<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/f9E3fcOrEzU"; frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>
<p align="center">
<b>Tauheedelahee: "KDE Plasma Applications Promotional Video"</b>
</p>

We'd like to thank <a href="https://www.tuxedocomputers.com/">TUXEDO Computers</a> for helping make all this happen. TUXEDO Computers have been incredibly generous providing prizes to both winners and finalists of the contest. We would also like to thank all participants and invite you all to carry on making videos promoting KDE!
<!--break-->