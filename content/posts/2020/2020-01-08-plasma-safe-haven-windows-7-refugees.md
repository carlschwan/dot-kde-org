---
title: "Plasma: A Safe Haven for Windows 7 Refugees"
date:    2020-01-08
authors:
  - "Paul Brown"
slug:    plasma-safe-haven-windows-7-refugees
comments:
  - subject: "Win 7 Theme as customization option?"
    date: 2020-01-08
    body: "<p>Are there any plans to gather up all the elements used by Dominic Hayes&nbsp; to create the Windows 7 theme as a one-click customization option? That would be wonderful.</p>"
    author: "Jason Evangelho"
  - subject: "Win 7 Theme as customization option?"
    date: 2020-01-08
    body: "<p>The Plasma Customization Saver (https://store.kde.org/p/1298955) widget seems like a good step towards this.&nbsp; You can export and import tar files that have all the desktop settings with a single click in the system tray.</p><p>&nbsp;</p><p>Unfortunately, I\"ve rarely seen reference to it and it's hard to find because the author isn't consistent about the name.&nbsp; It's called the above in the Plasma Widgets download page, but when it's installed, it shows up a PlasmaConfigSaver.</p><p>&nbsp;</p><p>But the concept is good and to have a widget you can just click and load new or saved configs would be a big step if it were merged into plasma settings directly.</p>"
    author: "Ian K"
  - subject: "Yes please!!!"
    date: 2020-01-08
    body: "<p>I came here looking for the link to download this theme as well - please make it an easy 1-click in Plasma and I think it'd be a pretty easy sell!</p>"
    author: "tradesmanhelix"
  - subject: "Browsers /Apps"
    date: 2020-01-08
    body: "<p>What browser will it run?, and wiuld we still have the option to install apps if so what will it install excutables &nbsp;or will it require someting like wine?</p>"
    author: "Byron"
  - subject: "windows 7 theme"
    date: 2020-01-08
    body: "<p>one of the main things i noticed coming from windows to linux when i first did was the mouse.&nbsp; May I suggest a windows cursor pack as part of this theme if it becomes something official?</p>"
    author: "Max Stevens"
  - subject: "Need help to upgrade "
    date: 2020-01-09
    body: "<p>Hi guy's</p><p>I need help to up grade my Dell desktop, as I can't afford to pay for windows 10. Also need my laptop do as well asus x501this got screwed up after downloading a fix it from Microsoft it needs to be wiped completely then a fill reinstall or upgrade&nbsp; both are windows 7. I'm getting desperate&nbsp;</p>"
    author: "Lee Beck"
  - subject: "Zorin OS Lite"
    date: 2020-01-09
    body: "<p>For computers that can't run the KDE desktop well, would Zorin OS Lite be the best user-friendly lightweight distro now? My grandma's using my old laptop, a 2006 Dell Latitude with a dead GPU, with Zorin OS Lite installed. She's incredibly computer illiterate, the last OS she knew was Windows 98 and the last web browser she knew well was Internet Explorer 6, and she's been almost totally unable to learn subsequent versions despite using them every day. I put a Firefox icon and a shortcut to her email (of course it's AOL) in the middle of her desktop, and sometimes she'd tell me that someone hacked her email when she accidentally opened a new tab. I put Zorin Lite on it once Windows 7 got unusably slow, and now she's using her computer as naturally as I do mine, and it's running better than new. I have noticed some annoying Windows 10-like schizophrenic settings menus, but it's sure a lot prettier than LXDE, if not quite as lightweight. Unless you have &lt;1GB of RAM, Lubuntu and Slackware/Puppy aren't even in the running anymore. I did try KDE Plasma, but it was so slow that even the mouse was moving at 1-2fps, ditto even more glittery distros like Ubuntu or Elementary OS.</p>"
    author: "Sean K"
  - subject: "Good job, but this article is misguided."
    date: 2020-01-10
    body: "<p>I agree that Microsoft's move to end support for Windows 7 is bad, not only for the reasons mentioned above, but also for the implied hardware upgrade that many will be forced to undergo.</p><p>&nbsp;</p><p>I also agree that getting people to use Linux is a worthy cause, anything to end Microsoft's dominance in the the consumer OS market and introduce healthy competition is welcome. Apple did a great job on this front, but lately they've been moving in the wrong direction by making their hardware near unaffordable for most.</p><p>&nbsp;</p><p>I do not believe that this KDE theme is a viable refuge for Windows 7 users. People that still use Windows 7 are either resisting change due to the unknown, or have some software compatibility issue with Windows 10. Neither of which are solved by going to Linux. Users that can't handle the change from 7 to 10, surely will are far less likely to make a change to linux, and those with software compatibility issues almost certainly will not find support for their legacy or bespoke systems in Linux.</p><p>&nbsp;</p><p>A Linux theme is simply does not cut it as a \"refuge for Windows 7 users\".</p>"
    author: "Matthys"
  - subject: "Of course! The way forward is"
    date: 2020-01-11
    body: "Of course! The way forward is to go here: https://kde.org/distributions and pick a distribution that comes with Plasma. Maybe pick one that you can burn to a USB stick and run from there. Like that, before you install anything onto your hard disk, you will be able to see if everything works to your satisfaction.\r\n\r\nI personally like Manjaro, but Neon, Kubuntu, and openSUSE are all good options.\r\n\r\nOnce you are happy, you can proceed to install and make Linux a permanent resident on your computer. All of the above distributions give you a guided process through the installation and there is plenty documentation on each of the distributions' websites that explain what to do.\r\n\r\nWe also have a \"welcome\" room on Matrix: https://webchat.kde.org/#/room/#kde-welcome:kde.org\r\n\r\nThere, you can ask seasoned members of the community for help and they will guide you live through the process of getting up and running."
    author: "Paul Brown"
  - subject: "Re: windows 7 theme"
    date: 2020-01-11
    body: "Sure. There are already quite a few packages with sets of cursors you can download for Plasma, so it may already be included somewhere. If you could point to a graphic where we can see the design you have in mind, that would help us identify it."
    author: "Paul Brown"
  - subject: "Re: Browsers /Apps"
    date: 2020-01-11
    body: "Most browsers work on Linux and Plasma: Chrome, Firefox, Chromium, Vivaldi, Brave, Opera, etc. There are also some Linux exclusive browsers, one of which is Falkon, created and designed be KDE volunteers.\r\n\r\nAs for apps, what apps do you need? Often times people coming from Windows try and run for the exact same apps they had on Windows. This can be counterproductive, as, even if the do run (with WINE, for example), there are probably equivalent apps made to run natively on Linux that will be faster and more efficient. For example, instead of trying to  run Word, or Excel, or any other Microsoft Office application, I would advise you first try LibreOffice, as it will probably come with all the office apps you need.\r\n\r\nBut, again, if you tell us what apps you need, we will be able to advise with alternatives or confirm whether you need WINE or not."
    author: "Paul Brown"
---
<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/Screenshot_20200108_105524.png"><img src="/sites/dot.kde.org/files/Screenshot_20200108_105524.png" width="800" /></a><br /><figcaption>A fully functional Plasma desktop with a Windows 7 theme.</figcaption></figure>
</div>

<b>Microsoft will stop providing updates for Windows 7 on January 14 2020.</b>

There won't be any more patches that correct bugs or even dangerous vulnerabilities. This will leave Windows 7 users exposed to all sorts of bad stuff. But that is not a huge concern for Microsoft. With this move, Redmond hopes to encourage users to upgrade to Windows 10.

But why should we care? Maybe because Windows currently holds 77% of the global desktop market share (all Linux desktops combined hold less than 2%). Of that 77%, nearly 30% belongs to Windows 7. That is nearly a billion people still holding on to Windows 7 because they are resisting the move to Windows 10. Apart from the natural human resistance to change, Windows 10 has earned a bad rap as an operating system that will <a href="https://www.howtogeek.com/224616/30-ways-windows-10-phones-home/">gladly leak your data back to Microsoft</a> and <a href="https://www.theverge.com/2017/3/17/14956540/microsoft-windows-10-ads-taskbar-file-explorer">lace your desktop with intrusive advertisements</a> as a matter of course.

Helping people regain control over their systems and protecting their data is precisely what Free Software communities do best, making this the perfect opportunity to help Windows 7 users upgrade to something much better: To the <a href="https://kde.org/plasma-desktop">Plasma desktop</a>!

<h3>How you can help</h3>

We need you to help convince Windows 7 users to move to the Plasma desktop. We have set up a <a href="https://phabricator.kde.org/T12444">task where we are brainstorming ideas, advice and resources</a>. You can contribute your thoughts too. Get your <a href="https://identity.kde.org">KDE Identity</a> today and join the conversation.

You can also <a href="https://webchat.kde.org/#/room/#kde-promo:kde.org">join the Promo team live on Matrix</a> and help us run this campaign.

Or fly solo! Talk to your friends, family, classmates and colleagues. Even if you convince just one person to make the transition to <i>any</i> Linux-based system, you will have done something valuable and helped the FLOSS movement.

<hr />

<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/TJzfaqRLfpY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

The Windows 7-like theme shown above was put together (from many parts created by many generous contributors) by Dominic Hayes, creator of <a href="https://ferenos.weebly.com/">Feren OS</a>, a cool-looking Ubuntu-based Linux distro aimed squarely at end users. Check it out!

Dominic used the following elements to re-create the look and feel of the desktop:

<B>Plasma Theme:</B> <a href="https://www.pling.com/p/998614">Seven Black</a>
<B>Window Decorations:</B> Seven Black
<B>Application Style:</B> gtk2
<B>GTK Theme:</B> Windows Se7en by Elbullazul
<B>Icons:</B> Darkine
<B>Colours:</B> Breeze Light
<B>Cursors:</B> DMZ White
<B>Splash Screen:</B> Feren OS
<B>Panel:</B> 38 height
<B>Widgets:</B> Default Apps Menu, I-O Task Manager, Stock System Tray, Feren Calendar or Event Calendar, Win7 Show Desktop

<!--break-->