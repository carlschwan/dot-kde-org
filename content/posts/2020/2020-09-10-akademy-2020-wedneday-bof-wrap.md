---
title: "Akademy 2020 - Wedneday BoF Wrap-up"
date:    2020-09-10
authors:
  - "sealne"
slug:    akademy-2020-wedneday-bof-wrap
---
<a href="https://community.kde.org/Akademy/2020/Wedneday">Wedneday</a> continued the Akademy 2020 BoFs, meetings, group sessions and hacking. There is a wrap-up session at the end of the day so that what happened in the different rooms can be shared with everyone including those not present.

Watch Wednesday's wrap-up session in the video below

<p align="center">
<video width="750" controls="1">
  <source src="https://files.kde.org/akademy/2020/wednesday_bof_wrapup.webm" type="video/webm">
Your browser does not support the video tag.
<a href="https://files.kde.org/akademy/2020/wednesday_bof_wrapup.webm">Tuesday BoF Wrap-up video</a>
</video> 
</p>

<!--break-->