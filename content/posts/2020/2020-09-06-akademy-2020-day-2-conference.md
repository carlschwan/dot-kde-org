---
title: "Akademy 2020 Day 2 - Conference"
date:    2020-09-06
authors:
  - "Paul Brown"
slug:    akademy-2020-day-2-conference
---
Written by Blumen Herzenschein, David C. and Paul Brown

The first day of Akademy talks were varied and interesting, covering a wide range of topics, from managing project goals and technical advances in Qt and KDE technologies, to Open Source in dentistry and Linux in automobiles.

Aleix Pol, President of KDE, kicked off the day at 8:50 UTC sharp by playing a video made by Bhavisha Dhruve and Skye Fentras welcoming everybody to the event.

<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/v07r6_6E8TQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

After acknowledging the very special circumstances of this year's Akademy, Aleix introduced the first keynote speaker Gina Häußge.

Gina is the creator and maintainer of <a href="https://octoprint.org/">OctoPrint</a>, a highly successful and feature-rich system for controlling your 3D printer over a web interface. Gina used her time to reveal the good and not so good things about becoming an independent Open Source maintainer. She talked about the sense of freedom and purpose  gained through Open Source work, but also the downsides of monetary instability and frequently feeling on her own despite working for hundreds, maybe thousands of users. Despite these disadvantages, she happily admitted that she would do it all over again, that the sensation of helping others and the fulfillment she experienced made up for all the darker patches.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/02_Gina_schematic.png" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/02_Gina_schematic.png" />
    </a>
   <figcaption>Gina's talk, summarized in graphic form by Kevin Ottens.</a></figcaption>
</figure> 

After that it was time for another veteran Open Source contributor: Jonathan Riddell talked about his steering of one of KDE's current Community-wide goals: <I>It's All about the Apps</I>, the project in which KDE community members work to promote and distribute KDE applications on their own merits, beyond their link to KDE's Plasma desktop. Jonathan gave us the motivations behind proposing the goal and its evolution since it was officially announced in Akademy 2019.

<figure class="float-md-right w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/Jon2.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/Jon2.jpg" />
    </a>
   <figcaption>Jonathan Riddell speaks of the progress made in All About the Apps goal.</a></figcaption>
</figure> 

Likewise, Niccolo Venerandi talked about the <I>Consistency</I> goal. This goal seeks to unify the look and feel of Plasma and all KDE apps to provide a coherent experience to users. Niccolo pointed out that Plasma does not have serious consistency problems, but different approaches to design in apps that sometimes lead to a bewildering array of looks and behaviors. Niccolo then showed us the future of KDE applications and, frankly, it looks amazing.

The presentations covering individual goals wound up with Méven Car talking about <I>Wayland</I>. It is no secret that the ride of porting KDE software and technologies to Wayland, the replacement for our venerable X window system, is being a bumpy one. That is why the KDE Community decided to make   Wayland a priority. The Wayland goal is a big task requiring updates to multiple components and forcing to refactor KDE's whole display stack. But as Méven explained, the community has made significant progress since Akademy 2019.

Following the presentation of individual KDE goals, Niccolo Venerandi, Méven Car, Jonathan Riddell, Lydia Pintscher and Adam Szopa got together for a round table that tackled how the first year of their goals went and what they learned along the way.

Following the round table, Andreas Cord-Landwehr used a ten-minute fast track slot to talk about <I>SPDX</I>, a system for better license statements. In the talk we learned that SPDX identifiers are an important step towards enabling automatic tooling for checking license statements. Andreas explained the advantages of using license statements and how simple it is to apply them. He also gave a short overview of what has already happened inside the KDE Frameworks and where contributors could help to support the conversion to SPDX.

Then Shawn Rutledge covered <I>Editing Markdown with QTextDocument</I> in another 10-minute talk. Shawn added markdown support in Qt 5.14 as a first-class format and as an alternative to the limited subset of HTML that QTextDocument had traditionally used. During the talk he demoed WYSIWYG editors written with widgets and with Qt Quick.

In the final fast track talk before lunch, Carl Schwan expounded on <I>How to Create a Good Promotional Website for your Project</I>. Carl has been the main developer behind the overhaul of many of KDE's main sites, including kde.org. During the talk, Carl presented the KDE Jekyll theme, the motivation behind the project, and briefly explained how it could be used to create a KDE website. He also showed some counter-examples, examples of poorly designed websites and how they could be improved to make the projects more attractive to potential users.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/09_KDE.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/09_KDE.jpg" />
    </a>
   <figcaption>Akademics enjoy some downtime between the morning and evening talks.</a></figcaption>
</figure> 

In the afternoon, things started with a presentation from the KDE e.V. Board and reports from the  Working Groups. The Board told the attendees all about the things they had done over the year since the last Akademy. Highlights included expanding the number of paid employees from three to five, the migration to GitLab, and the funding of more support for community members. The Board followed up with details on the activities of the different working groups, although some of their presentations had to be moved to the end of the day due to time constraints.

The full text of the <a href="https://ev.kde.org/reports/ev-2019/">KDE Annual Report 2019</a> is available for you to read at your leisure.

Then we launched back into the talks proper with the <I>Input Handling Update</I>, again by Shawn Rutledge. In this talk, Shawn talked about what's coming up and the several goals for input events in Qt 6.

<figure class="float-md-left w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/12_Cornelius_KFQF.png" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/12_Cornelius_KFQF.png" />
    </a>
   <figcaption>Cornelius Schumacher tells the story of the KDE Free Qt Foundation.</a></figcaption>
</figure> 

Meanwhile, in Room 2 Cornelius Schumacher was talking about the <I>KDE Free Qt Foundation</I>. Established in 1998, the KDE Free Qt Foundation was founded to keep the Qt toolkit free for KDE and all other free software projects. The Foundation has held steady during the more than two decades of sometimes turbulent times Qt and KDE have gone through together. Cornelius told the story of how this worked.

And speaking of The Qt Company... A little later, back in Room 1, Richard Moe Gustavsen, talked about <I>Native Desktop Styling Support for Qt Quick Controls 2</I> and the ongoing work at The Qt Company to support writing desktop applications using Qt Quick Controls 2.

At the same time, in Room 2, Aleix Pol was talking about <I>KDE's Products</I> and how to visualise their relationship with users. In the talk, Aleix introduced a framework to help developers make sure the Free Software community and its users are best taken care of.

In the next slot, Patrick Pereira presented in Room 1 <I>QML Rapid Prototyping -- Developing tools to improve QML prototypes and development</I>. In his talk, Patrick talked about how QML prototyping is something that all developers do, and how it can be achieved more efficiently. He used two projects as examples: QHot (a hot reload for nested QML files) and QML Online (an online QML editor created with WebAssembly) to help explain how to bring down the development time and learning curve for QML.

<figure class="float-md-right w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/EhKyJiuWsAE4b4Y.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/EhKyJiuWsAE4b4Y.jpg" />
    </a>
   <figcaption>Jonah Thelin delivers a talk about Linux in cars from inside his car.</a></figcaption>
</figure> 

In Room 2, Johan Thelin introduced his talk <I>Linux in Cars - So What?</I> from, get this, inside his car. Literally. Johan talked about why cars are still also using so much proprietary software you would be hard pushed to find the Open Source bits, even though they may be using Linux deep down. He also talked about what needed to be addressed to improve the situation and how KDE software could work for those use cases.

Following this batch of regular talks, there were another three 10-minute fast track presentations.

In <I>Flatpak, Flathub and KDE: A Quick Summary</I>, Albert Astals Cid introduced the audience to Flatpak, explained what Flathub was and how KDE interacted with both of them.

Then Nicolás Alvarez spoke of <I>Improving KDE Server Infrastructure</I>, the formation of the Sysadmin Working Group, and told attendees how the Sysadmin team was making KDE servers more manageable by reducing "technical debt", moving manual tasks into scripts, improving documentation, and making more things testable locally before putting them on the real servers.

In the last fast track of the day, David Edmundson gave tips on <I>How to Win an Argument with a Maintainer</I>, having partaken in and witnessed hundreds of discussions on Bugzilla and Phabricator that then turned into arguments that yielded angry stalemates. He shared with the audience the methods he had seen work to achieve happy mediums and warned against attitudes that escalated situations into miserable experiences for everybody.

Next came one of the more surprising presentations of the day, delivered by Tej Shah, a Doctor of Medicine in Dentistry from the US. Tej talked about his project <I>Clear.Dental</I> and his attempt to move dentistry to Open Source using the power of Linux, Qt, and KDE. He reviewed the state of Dental software (which is pretty dire), the problem with the current software available, and how Clear.Dental could contribute to solve it.

At the same time, in Room 2, Camilo Higuita was talking about his own passion project: <I>Maui</I> and gave the audience a rundown of all the updates on the group of apps, services, libraries, and UI (User Interface) frameworks Maui provides to produce attractive-looking applications.

In the next session, Rohan Garg gave attendees a lesson in <I>Linux Graphics 101</I> in which he explained how the growing popularity of ARM devices has led to platform architectures with quirkier graphics hardware. He talked about the basics of how the Linux graphics stack works and the history behind how we've come to the current Gallium design in Mesa.

Finally, Google Summer of Code participant Amy Spark showcased how she <I>Integrated Hollywood Open Source with KDE Applications</I> by porting a Disney Animation technology to Krita. SeExpr gives Krita artists access to procedurally generated texturing, allowing for fine surface details, lighting effects, overlays, and more to be added at the push of a button. As a scripting language, it gives creators the flexibility needed to ensure perfect results every time by tailoring the algorithm to their needs. Amy had to overcome many technical barriers during the porting process, as SeExpr was originally built to run only on a very specific proprietary stack.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/photo_2020-09-05_23-48-43.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/photo_2020-09-05_23-48-43.jpg" />
    </a>
   <figcaption>David Revoy shows an example of how SeExpr can be used in an image from "Pepper and Carrot".</a></figcaption>
</figure> 

In case you missed it, the today's talks are already available online in three blocks — one for the morning and two for each of the rooms used in the afternoon. We have also recorded all the talks and you will be able to watch each talk separately on KDE's available video platforms soon.

<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/_172B5er4P4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>
<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/inJ4BblVm0o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>
<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/FSTcqlmGR30" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

Tomorrow, we will again be streaming via BigBlueButton, directly from our servers, and through YouTube.
<!--break-->