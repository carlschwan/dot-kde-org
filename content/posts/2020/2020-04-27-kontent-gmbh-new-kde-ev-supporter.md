---
title: "KONTENT GmbH is a New KDE e.V. Supporter"
date:    2020-04-27
authors:
  - "jriddell"
slug:    kontent-gmbh-new-kde-ev-supporter
---
<figure style="width: 190px; float: right; padding: 1ex; margin: 1ex; border: 0px solid grey;"><img src="/sites/dot.kde.org/files/kontent_shadow.png" /><br /></figure> 



<a href="https://ev.kde.org/">KDE e.V.</a> is very happy to welcome <a href="https://www.kontent.com/">KONTENT GmbH</a> as one of our supporting members.

<blockquote>
We from KONTENT are Linux enthusiasts since our beginning, back in the 90s. So KDE was at anytime a well known, high quality brand for us. It was just a matter of time that we would get in closer contact to the community. This moment has come now, and we are very proud of it. Maybe in the future we can not just help in financial matters, but also in an inspiring way. 

Uli Klinkhammer, CEO of KONTENT GmbH 
</blockquote>

<a href="https://ev.kde.org/supporting-members/">Supporting memberships</a> are very important because they help make KDE
sustainable. If you would like to become a supporter as well, you can find more information on our website.
<!--break-->


