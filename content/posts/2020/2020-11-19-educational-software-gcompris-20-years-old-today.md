---
title: "Educational Software GCompris is 20 Years Old Today"
date:    2020-11-19
authors:
  - "Paul Brown"
slug:    educational-software-gcompris-20-years-old-today
---
<a href="https://gcompris.net/">GCompris</a> is a popular collection of educational and fun activities for children from 2 to 10 years old.  GCompris has become popular with teachers, parents, and, most importantly, kids from around the world and offers an ever-growing list of activities -- more than 150 at the last count. These activities have been translated to over 20 languages and cover a wide range of topics, from basic numeracy and literacy, to history, art, geography and technology.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/gcompris.png" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/gcompris.png" />
    </a>
   <figcaption align="center">GCompris offers children between the ages of 2 and 10 more than 150 fun educational activities.</a></figcaption>
</figure> 

The newest version of GCompris also incorporates a feature that teachers and parents alike will find useful: GCompris 1.0 lets educators select the level of the activities according to the proficiency of each child. For example, in an activity that lets children practice numbers, you can select what numbers they can learn, leaving higher and more difficult numbers for a later stage. An activity for practicing the time lets you choose whether the child will practice full hours, half hours, quarters of an hour, minutes, and so on. And in an activity where the aim is to figure out the change when buying things for Tux, the penguin, you can choose the maximum amount of money the child will play with.

We have built the activities to follow the principles of "nothing succeeds like success" and that children, when learning, should be challenged, but not made to feel threatened. Thus, GCompris congratulates, but does not reprimand; all the characters the child interacts with are friendly and supportive; activities are brightly colored, contain encouraging voices and play upbeat, but soothing music.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/levels.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/levels.jpg" />
    </a>
   <figcaption align="center">GCompris now lets you select the level of some activities according to the child's proficiency.</a></figcaption>
</figure> 

The hardware requirements for running GCompris are extremely low and it will run fine on older computers or low-powered machines, like the Raspberry Pi. This saves you and your school from having to invest in new and expensive equipment and it is also eco-friendly, as it reduces the amount of technological waste that is produced when you have to renew computers to adapt to more and more power-hungry software. GCompris works on Windows, Android and GNU/Linux computers, and on desktop machines, laptops, tablets and phones.

<p align="center">
<video width="750" controls="1">
  <source src="https://cdn.kde.org/promo/GCompris/GC1-LearnAdditionsSubtractions_music.mp4" type="video/mp4">
Your browser does not support the video tag.
<a href="https://cdn.kde.org/promo/GCompris/GC1-LearnAdditionsSubtractions_music.mp4">Practicing additions and substractions with GCompris. </a>
</video> 
</p>

GCompris is built, maintained and regularly updated by the KDE Community and is Free and Open Source Software. It is distributed free of charge and requires neither subscriptions nor asks for personal details. GCompris displays no advertising and the creators have no commercial interest whatsoever. Any donations are pooled back into the development of the software.

<p align="center">
<video width="750" controls="1">
  <source src="https://cdn.kde.org/promo/GCompris/GC1-AnalogElectricity_music.mp4" type="video/mp4">
Your browser does not support the video tag.
<a href="https://cdn.kde.org/promo/GCompris/GC1-AnalogElectricity_music.mp4">Learning about electrical circuits with GCompris.</a>
</video> 
</p>

Seeking to engage more professional educators and parents, we are working on several projects parallel to our software and have recently opened <a href="https://forum.kde.org/viewforum.php?f=316">a forum for teachers and parents</a> and <a href="https://webchat.kde.org/#/room/#gcompris-teachers:kde.org">a chat room</a> where users and creators can talk live to each other, suggest changes, share tips on how to use GCompris in the classroom or at home, and find out upcoming features and activities being added to GCompris.

Apart from increasing the number and variety of activities, for example, an upcoming feature is a complete dashboard that will provide teachers with better control of how pupils interact with GCompris. We are also working with teachers and contributors from different countries to compile a "Cookbook" of GCompris recipes that will help you use GCompris in different contexts. Another area where we are working with contributors is on translations: if you can help us translate GCompris into your language (with your voice!), we want to hear from you! Your help and ideas are all welcome.

Visit our <a href="https://forum.kde.org/viewforum.php?f=316">forum</a> and <a href="https://webchat.kde.org/#/room/#gcompris-teachers:kde.org">chat</a> and tell us how you use GCompris and we will share it with the world.

<hr />

<a href="https://kde.org">KDE</a> is a community of volunteers that creates a wide range of software products, like the <a href="https://kde.org/plasma-desktop/">Plasma desktop</a>, the <a href="https://krita.org/en/">Krita painting program</a>, the <a href="https://kdenlive.org/">Kdenlive video editor</a>, the <a href="https://gcompris.net">GCompris educational suite of activities and games</a>, as well as dozens of other high-quality <a href="https://gcompris.net">applications and utilities</a>. Among them, KDE develops and maintains several educational programs for children and young adults.

All KDE's products are Free and Open Source Software and can be downloaded, used and shared without charge or limitations.
<!--break-->