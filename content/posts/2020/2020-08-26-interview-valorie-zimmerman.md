---
title: "Interview with Valorie Zimmerman"
date:    2020-08-26
authors:
  - "unknow"
slug:    interview-valorie-zimmerman
---
By Bhavisha Dhruve

<figure class="float-md-right w-100 p-2" style="max-width: 400px">
    <a href="/sites/dot.kde.org/files/Image47.jpeg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/Image47.jpeg" />
    </a>
   <figcaption>Valorie Zimmerman</a></figcaption>
</figure> 

<i>Valorie Zimmerman is a veteran member of the KDE community. In her decade-long tenure, she has worked on making the open source community easier to access for newcomers; especially women and members of the LGBTQ community. As a council member for Kubuntu, she plays a vital role in ensuring that the Kubuntu project aligns with the goals of the Ubuntu community. In her free time, she volunteers in her chapter of the LinuxChix, Ubuntu-Women, and of course KDE.</i>

<i>In September, Valorie will be one of the keynote speakers at Akademy 2020 and agreed to talk to us about her work at KDE and the importance of diversity in the open source community.</i>

<span style="color:#f55;font-weight:bold">Bhavisha:</span> Hi Valorie, how are you?

<span style="color:#55f;font-weight:bold">Valorie:</span> I'm good, thank you.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> Great, let's start by getting to know you a bit. Why don't you tell us a bit about yourself?

<span style="color:#55f;font-weight:bold">Valorie:</span> I'm a geeky parent, grandmother, and lover of history, research, writing, humans, and the rest of the natural world. A lot of my time is spent in genealogy research and working with <a href="https://skcgs.groups.io/g/Society/topics?p=recentpostdate%2Fsticky,,,20,2,0,0&previd=1590792338996915682&nextid=1589766512294981965&prev=1">my local genealogy society</a>. I lead the Publicity team and will be standing for Vice President in September. I'm looking forward to reaching out to other genealogy and history organizations and creating joint projects with some of them. Right now, we're researching the Black miners of Franklin, a local mining ghost town. Many local people do not realize how many people of color used to live in this part of the county.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> Conducting research into a deserted mining town sounds like a story I need to hear.

<span style="color:#55f;font-weight:bold">Valorie:</span>  I live about half a mile outside of an old mining town, Black Diamond. Franklin was a town just 2 miles away. Now it’s a ghost town. There is a very cool local history museum there, created out of the old train station. A bunch of the old-timers started it years ago, and the local people have donated their money, time, and relics. My neighbor across the street is very active in it; her family has lived and worked in the town for over 100 years. Anyways, we began talking about doing genealogy (right before COVID-19 hit here) and she eventually thought about this project that the museum wanted to do. So, she wrote to the genealogy society asking if anyone was interested in helping out.

I thought it sounded fascinating, as did another researcher so we dived right in, finding records and other resources, including newspapers from 130 years ago. There was a coal miners' strike, and the mine owners went to the south of the US to recruit black men to come to break the strike -- but they didn't tell them that part until they reached the mine. You should know that the US has been very racially divided -- it's really a caste system so this was explosive and there was some violence, eventually though there was peace, but there are almost no people of color living in this part of the county, so we would like to bring this old history to light, and we hope to involve the Northwest African American Museum as well to oversee the project. If people are interested, <a href="https://skcgs.groups.io/g/Black-Heritage-Franklin">they can check it out here</a>. 

<span style="color:#f55;font-weight:bold">Bhavisha:</span> It is fascinating you mention genealogy; we are actually seeing the beginning of a whole new industry intersecting genomics with technology with the advent of modern day genetic tracing. Have you tried one of the genetic marker tests yourself?

<span style="color:#55f;font-weight:bold">Valorie:</span>  Oh yes, I'm on all the sites and have done 4 different tests and had my father do two of them. It's a very geeky way to research even more geeky than genealogy with records.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> I have been meaning to do a few myself once this pandemic subsides, I noticed that you actually majored in Anthropology and Women's Studies. What led you to the open source community and KDE?

<span style="color:#55f;font-weight:bold">Valorie:</span> I heard about free software many years ago, soon after we got our first computer, a Coleco ADAM. I didn't know much about how to use the computer so I did the only logical thing: started a user group! We all learned together; my husband and I along with our children and the group members. That was years of fun but it turned out that 240kb was not enough memory! By that time we had an early Windows computer and I got a Mac to do some design work. Eventually my oldest son told me that Linux was ready for the desktop, and put Mandrake on my first laptop. I loved it, and although it was a dual-boot, I never bothered to login to Windows after that. After Mandrake went away, I ended up on Kubuntu where I've been ever since. The LinuxChix helped me a lot in the change from Windows to Linux.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> <i>For our younger readers, Mandrake was the precursor to the current OpenMandriva Linux distribution which uses a lot of products from the KDE ecosystem.</i> You mentioned LinuxChix helped you a lot when you were first getting started with Unix systems, can you tell us a bit more about them?

<span style="color:#55f;font-weight:bold">Valorie:</span> The women I found in Linuxchix, UbuntuWomen, and KDE have been real leaders for me as well as some long time friends. I met both Lydia and Myriam there, among many others. They both urged me to get involved in KDE and Kubuntu and in particular, Amarok (since I was already using it). My studies in anthropology and women's studies are still useful to me in everything I do.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> Computer Science is becoming more accessible than ever before, and there is a growing number of students who are considering college to be unnecessary, do you believe that college, no matter your major, is still worth it?

<span style="color:#55f;font-weight:bold">Valorie:</span> As I often tell students, nothing is ever wasted in your studies, they give you an analytic framework, it's not about learning facts; it's about learning how to learn everything. So this all continues to be useful. I learned how to see structure and not just appearance. One of the reasons KDE is so great is that the governance structure continues to grow and is made up of those who do work and care, not just those who pay.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> I agree, our community at KDE is quite special. When you started here was it difficult to find your footing in such a vast organization?

<span style="color:#55f;font-weight:bold">Valorie:</span> Hmmm, unsure. I keep making new friends, but the long-time connections give stability for sure and I hope that people trust me enough to call me out when I need it and I try to do the same for them.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> You are currently a council member for Kubuntu, can you tell us a bit about the Kubuntu project and your role as council member?

<span style="color:#55f;font-weight:bold">Valorie:</span> Kubuntu brings KDE software to Ubuntu. We think we have the most beautiful, powerful and friendly experience for our users. We have a packaging CI set up which helps our main developers quite a bit and some other tools for the rest of us to help with promo and testing. We mostly meet up at conferences such as LinuxFestNW, that is very fun, oh, and SeaGL - which by the way everyone should submit a talk to! It's online this year so everyone can join.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> That's amazing, I want to pivot here to something that I know is very near and dear to your heart. You have done some incredible work in helping the KDE community become more accessible to coming generations of open source contributors. What made you focus on this particular issue?

<span style="color:#55f;font-weight:bold">Valorie:</span> As I began helping answer questions in more and more KDE IRC channels, I began to notice some pain points. I tried to help fix those or find those who could help. I'm very happy that it is now a wide-spread effort, we need new people because they have the superpower of <i>being</i> new; they can see pain points that we folks who have been around for a while can no longer see. We know the workarounds, so we need people immediately empowered to bring those to our attention, so we can work together to make things easier.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/GSoC_BoF.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/GSoC_BoF.jpg" />
    </a>
   <figcaption>Valorie runs the GSoC BoF during Akademy 2019.</a></figcaption>
</figure> 

<span style="color:#f55;font-weight:bold">Bhavisha:</span> That's true, you have done some publication in this space as well, <a href="https://dot.kde.org/2012/02/05/kde-development-%E2%80%93-beginners-guide">The Beginner's Guide to KDE Development</a>, tell us what prompted you to write this, and what were some key takeaways you wanted your readers to get from reading this.

<span style="color:#55f;font-weight:bold">Valorie:</span> Better documentation is particularly important to getting women involved. Most women do not have much free time, they want to read the docs and dive in, ours... still suck. The Beginner’s Guide was great when we wrote it but it is so out of date. Google was doing a book sprint to follow up on that last sentence, it would be great to get that Beginner’s Guide into Sphinx or just Git with markdown, so that it can be more easily updated. 

I was also invited to join the writing team by Lydia, as part of a book-writing sprint sponsored by Google and some former GSoC students who wanted to write the book. I'm an editor and a non-coding developer, which was perfect for a book for beginners. I think we did good work, but the book needs updating! I want beginners to be able to dive in and do what they love right away.

The Frameworks Cookbook was a huge challenge, but the fact that I'm a non-coder and that we created the book in Randa, Switzerland while other sprints were going on, again turned out to be a plus. I was able to wheedle very clear English out of the developers and make the book readable. The fact that we had horrible internet connectivity is why we ended up committing it to git, which ended up being perfect! Now each framework maintainer can keep their own section up-to-date.

That task taught me that you don't have to know what you're doing to dive in. You will learn what you need as you go, or find people who know how to do what you don't. That is the magic of working in teams, and the magic of community.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> Do you recall the very first project you worked on?

<span style="color:#55f;font-weight:bold">Valorie:</span> Oh yes: Amarok! I took the user docs and put them onto the userBase wiki, which I thought everyone did. But we were the first. Also that was the year we first participated in GCI, Google Code-in for 12-18 year-olds. I made a task for each page we lacked for the user manual, soooo many tasks, so many great kids who loved music. It was so fun.

<span style="color:#f55;font-weight:bold">Bhavisha:</span>  Do you think the Community Working Groups (CWG) make contributing more accessible or communication better?

<span style="color:#55f;font-weight:bold">Valorie:</span> Hmmm... Not more accessible, really. It's more about keeping teams working better and restoring communication where it has broken down. My role has varied: I've actually visited one person just to listen to their side of a bitter fight, I've had to advocate kicking out a long-time member who would not stop hurting others, the really hard cases are when two friends fight, that hurts.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> Yeah that is sad, but it is interesting to know what CWG is about, could you share some details about your volunteer work on the administration team for GSoC (Google Summer of Code)?

<span style="color:#55f;font-weight:bold">Valorie:</span> What I like about GSoC is that I can help both the mentors and the students. It’s usually a lot of fun, reviewing proposals, reminding students about deadlines, etc. Though we need a bigger team, I do have a rough idea about why most people don't want to be part of it.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> Oh, why so, do you mind elaborating?

<span style="color:#55f;font-weight:bold">Valorie:</span> People don't like conflict and dealing with strong emotions, especially angry people. We have even had some hard times such as when one of our students was basically imprisoned in Kashmir last year. That was scary, but India’s Prime Minister Mr. Narendra Modi finally lifted the ban and he was free to finish the project.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> I am glad he could finish up the project. You are planning to give talk on “No Burnout”, something that has become very common in today’s culture. Can you give us a quick summary about your Talk?

<span style="color:#55f;font-weight:bold">Valorie:</span> I wish to enlighten about this subject. How can you recognize it in yourself and others, and what to do about it, by sharing others' experiences on it. I've given it before, I hope those who need it will be able to attend.

<span style="color:#f55;font-weight:bold">Bhavisha:</span> We look forward to hearing more about it. I want to thank you so much for taking the time. It has been such a pleasure talking to you and we are looking forward to seeing you at Akademy 2020.

<span style="color:#55f;font-weight:bold">Valorie:</span> See you at Akademy!

<i>Valorie will be delivering the Keynote speech <a href="https://conf.kde.org/en/akademy2020/public/events/221">"No Burnout"</a> on September 11 at <a href="https://akademy.kde.org/2020">Akademy 2020</a>.</i>

<i><a href="https://akademy.kde.org/2020/register">Register now</a> and don't miss it!</i>
<!--break-->