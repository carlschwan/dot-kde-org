---
title: "Akademy 2020 Day 7 - The Last Day"
date:    2020-09-12
authors:
  - "Paul Brown"
slug:    akademy-2020-day-7-last-day
---
It was a fine day in Onlineland and Akademy attendees were in a festive mood, not least because they were ready to celebrate the successful migration of KDE to GitLab. Although a titanic effort, the move is already paying off, as GitLab offers an easier and more flexible platform for developers and users to get their work done and shared.

Ben Cooksley, sysadmin extraordinaire, Bhushan Shah, Plasma Mobile's main developer, Community veterans like David Edmundson and Lydia Pintscher, and many others shared their experiences of how the migration has improved the way they worked.

GitLab was also represented in the party with Nuritzi Sanchez, Senior Open Source Program Manager at GitLab, attending.

<h2>BoFs</h2>

Then there were several interesting BoFs throughout the day covering, in typical KDE fashion, a very wide range of topics.

Aniqa Khokhar and Allyson Alexandrou hosted a meeting on <I>The KDE Network</I>, KDE's initiative to start and support grassroots organizations in different parts of the world. Cornelius Schumacher told us about Blue Angel, an official label from the German government that is awarded to eco-friendly products. As KDE has already proven to have a low carbon footprint and helps recycle old machines, Cornelius thinks the Community should work harder to become even greener and get recognized for our efforts. Carl Schwan managed a meeting on what KDE should do to improve the online documentation for developers, and David Edmundson met with community members interested in pushing the development of Qt Wayland forward.

<h2>Talks</h2>

<figure class="float-md-left w-100 mr-md-4" style="max-width: 400px"><a href="/sites/dot.kde.org/files/05_kevin.png" data-toggle="lightbox" class="position-relative"><img class="img-fluid" src="/sites/dot.kde.org/files/05_kevin.png" /></a><figcaption>Kevin Ottens shares advice on how to avoid forgetting things.</a></figcaption></figure> 

Later in the day, we attended the first batch of student presentations. Mentoring students is an essential part of KDE's mission, as they can often receive through events such as Google Summer of Code and Season of KDE, valuable experience and get started in contributing to Free Software.

First up was Kartik Ramesh who worked on facial recognition in digiKam. If you have been following the latest releases of <a href="https://www.digikam.org/">digiKam</a>, KDE's professional photograph management software, you will be aware of how face recognition has changed and improved over the last few versions. Kartik worked on the front end interface to make it friendly and usable.

Deepak Kumar, on the other hand, worked on multiple datasets for <a href="https://www.gcompris.net/">GCompris</a>, the activity-packed educational software for children. He added a tutorial screen to the <I>Odd & Even</I> game and also new datasets to (read "made more exercises for") the <I>Clock</I> game, <I>Balance Scales</I>, and more.

Sharaf Zaman SVG worked on mesh gradients for <a href="https://krita.org">Krita</a>, KDE's application for painters, thus improving Krita's support for SVG images; and Sashmita Raghav improved the timeline clip color palette for <a href="https://kdenlive.org">Kdenlive</a>, KDE's video editor.

It was then time for Kevin Ottens to warn us about <I>Lost Knowledge in KDE</I>. He explained how it is possible to lose knowledge over time because people leave the Community, advances in technology go undocumented and then forgotten, and software gets deleted. Kevin finished his talk by speaking of ways to avoid losing this information in organizations like KDE.

Kevin's talk was followed by another batch of student presentations in which Sashwat Jolly talked about incorporating an <a href="https://www.etesync.com/">EteSync</a> agent into Akonadi, Kontact's backend for storage indexing and retrieval of users' personal information. EteSync is a free software service you can self-host and that provides an end-to-end encrypted, and privacy-respecting sync for contacts, calendars and tasks.

Then Shivam Balikondwar spoke of how he added file backends for the <a href="https://kde.org/applications/en/rocs">ROCS IDE</a> and how he added KML files to the list of types of files ROCS could parse.

Meanwhile, Paritosh Sharma worked on bringing 3D to <a href="https://edu.kde.org/kstars/">KStars</a> by incorporating Qt3D into the stargazing app.

Finally, Anaj Bansal explained how he worked on improving KDE's web infrastructure and helped port kde.org to Hugo.

After this batch of students' presentations, it was time for another conference talk, and Nate Graham told us about his <I>Visions of the Future</I>. Nate, among other things, wanted us to envision a world in which KDE Plasma gets shipped by default on every PC, phone, and tablet on the planet (and possibly off it too). It is worth pointing out that Nate had already presented a talk called "Konquering the world -- A 7 step plan to KDE world domination" at Akademy 2018. We may be detecting a trend here...

<figure class="float-md-right w-100 p-2" style="max-width: 400px"><a href="/sites/dot.kde.org/files/08_saurabh.png" data-toggle="lightbox" class="position-relative"><img class="img-fluid" src="/sites/dot.kde.org/files/08_saurabh.png" /></a><figcaption>Saurabh Kumar implemented a storyboard editor in Krita.</a></figcaption></figure> 

After a glimpse into tomorrow, quite appropriately the next generation of KDE contributors took again to the stage and shared their work. There was a theme here too, because Saurabh Kumar told us how he implemented a storyboard editor as a docker for Krita; L.E. Segovia spoke of their work with dynamic fill layers using SeExpr, again for Krita; and Ashwin Dhakaita told us how he had managed to integrate MyPaint brushes... into Krita.

The only discordant note came from Kitae Kim, who spoke of how he improved MAVLink integration in <a href="https://kirogi.org/">Kirogi</a>, KDE's ground control software for drones.

The final presentation of the day and the very last of Akademy 2020 was delivered by KDE veteran Valorie Zimmerman. Valorie told us how to avoid burnout and advised us on how to recognize the signs, gave practical advice on what steps we could take to not let it affect us and then opened the floor to questions and stories from other Community members.

An appropriately heart-warming, feel-good final talk.

<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/_VlgkuPvK0o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

<h2>Akademy awards</h2>

Then it was the moment to celebrate individual achievements with the traditional Akademy Awards. 

Presented by last year's winners, Volker Krause, Nate Graham and Marco Martin, the award to Best Application went to Bhushan Shah for creating a new platform, Plasma Mobile, on which new applications could thrive. The prize to Best Non-Application was given to Carl Schwan for his work of revamping KDE's websites; and the special Jury Award went to Luigi Toscano for his work on localization.

Finally, the jury awarded a special Organization Prize to the Akademy Team made up by Kenny Coyle, Kenny Duffus, Allyson Alexandrou and Bhavisha Dhruve, for their work organizing such a very special event.

Aleix Pol, President of KDE e.V., delivered the final words of the event and pronounced closed what has been an amazing edition of Akademy in so many different ways.

<figure class="w-100 img-fluid "><a href="/sites/dot.kde.org/files/index.jpg" data-toggle="lightbox" class="position-relative"><img class="img-fluid" src="/sites/dot.kde.org/files/index.jpg" /></a></figure> 

<h3>Akademy will be back again in 2021! Don't miss it!</h3>
<!--break-->