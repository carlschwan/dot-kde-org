---
title: "Akademy 2020 - Monday BoF Wrap-up"
date:    2020-09-08
authors:
  - "sealne"
slug:    akademy-2020-monday-bof-wrap
---
<a href="https://community.kde.org/Akademy/2020/Monday">Monday</a> was the first day of Akademy 2020 BoFs, group sessions and hacking. There is a wrap-up session at the end of the day so that what happened in the different rooms can be shared with everyone including those not present.

Watch Monday's wrap-up session in the video below

<p align="center">
<video width="750" controls="1">
  <source src="https://files.kde.org/akademy/2020/monday_bof_wrapup.webm" type="video/webm">
Your browser does not support the video tag.
<a href="https://files.kde.org/akademy/2020/monday_bof_wrapup.webm">Monday BoF Wrap-up video</a>
</video> 
</p>

<!--break-->