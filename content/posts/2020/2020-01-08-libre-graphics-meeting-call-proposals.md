---
title: "Libre Graphics Meeting Call for Proposals"
date:    2020-01-08
authors:
  - "jriddell"
slug:    libre-graphics-meeting-call-proposals
---
<img src="/sites/dot.kde.org/files/lgm2.png" width="800" height="458" />

The <a href="https://libregraphicsmeeting.org">Libre Graphics Meeting</a> (LGM) is the annual international convention for the discussion and development of free and open source graphics software.

This year it will happen in Rennes, France, from May 26th to 29th. We are welcoming all relevant projects to submit a proposal for a talk and/or a workshop. We already expect Krita and Kdenlive teams to be present. The Krita sprint will be held after the meeting and Kdenlive are planning to have a sprint around that time too. It would be awesome to also see some people from Plasma team working on graphics tablet support and color management, or any other topic of interest for developers and users of graphics creation application.

LGM are now asking for talks, workshops, BoF meetings and lightning talks for the conference. Please don't be shy and <a href="https://libregraphicsmeeting.org/2020/en/submission.html">submit your proposal</a>.

KDE e.V. has agreed to support the event by providing travel support to KDE contributors. If you are interested, make sure to file your <a href="https://reimbursements.kde.org/">reimbursement request</a> before January 31st.

<img src="/sites/dot.kde.org/files/lgm3.jpeg" width="800" height="532" />
LGM 2019
<!--break-->
