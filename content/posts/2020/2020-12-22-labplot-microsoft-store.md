---
title: "Labplot on Microsoft Store"
date:    2020-12-22
authors:
  - "jriddell"
slug:    labplot-microsoft-store
---
<i><a href="https://labplot.kde.org/">LabPlot</a> can now be installed on <a href="https://www.microsoft.com/en-us/p/labplot/9ngxfc68925l">Windows computers from the Microsoft store</a>.  We asked some questions to Stefan Gerlach who got it into the store about how this was done.</i>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/LabPlot_on_Windows.JPG" width="600" /><br /><figcaption>LabPlot on Windows from the Microsoft Store</figcaption></figure> 

<span style="color:#f55;font-weight:bold">Jonathan Riddell:</span> Can you tell us what LabPlot does?

<span style="color:#55f;font-weight:bold">Stefan Gerlach:</span> LabPlot is a desktop application for interactive visualization and analysis of scientific data. We try to provide an alternative to commercial products like OriginLab Origin, SigmaPlot or IgorPro, but also use modern desktop features. There are some free applications with more or less overlapping goals like SciDAVis and kst and we collaborate with them.

LabPlot is a multi-platform KDE application. The current code base, named LabPlot2, started in 2006 when rewriting the old version LabPlot 1.6. Our main development platform is Linux, but most of our users are on Windows, so we are working hard to make everything work there too.

<span style="color:#f55;font-weight:bold">Jonathan:</span> What about <a href="https://cantor.kde.org/">Cantor</a>?

<span style="color:#55f;font-weight:bold">Stefan:</span> Cantor is basically a frontend to several (mathematical) applications like Python, Octave, Sage or Julia with a nice worksheet interface.

Cantor-Worksheets can be used in LabPlot to do calculations and show the results. Two nice examples can be found at <a href="https://labplot.kde.org/gallery">the Labplot gallery</a>. I'm not a main developer of Cantor, but mainly work on porting it to Windows and macOS to make it available for LabPlot.

<span style="color:#f55;font-weight:bold">Jonathan:</span> How was the Windows build of LabPlot made?

<span style="color:#55f;font-weight:bold">Stefan:</span> We have been using the <a href="https://binary-factory.kde.org">Binary Factory</a> to build nightly and release builds for Windows for some time now. Before that we had a virtual machine running <a href="https://community.kde.org/Craft">Windows with Craft</a> installed to make our own packages. It was not easy to get everything built correctly on the Binary Factory, but, looking back, it was worth the effort.

<span style="color:#f55;font-weight:bold">Jonathan:</span> What sort of QA have you done on the Windows build of LabPlot?

<span style="color:#55f;font-weight:bold">Stefan:</span> Besides several unit tests that we have, most of the QA we do now is using new features on all platforms. We also get user responses from nightly builds when things are not working as expected. This helped a lot to find regressions and problems especially on Windows.

<span style="color:#f55;font-weight:bold">Jonathan:</span> How easy was it to get access to the KDE account on the Microsoft Store?

<span style="color:#55f;font-weight:bold">Stefan:</span> Very easy. I just had to open a sysadmin ticket :-)

I don't think that every application developer needs access to the KDE account. If a Windows package is well prepared and tested, the submit process can be easily done by any developer on the KDE Partner Center.

<span style="color:#f55;font-weight:bold">Jonathan:</span> What sort of process did you have to go through to get it into the Microsoft Store?

<span style="color:#55f;font-weight:bold">Stefan:</span> To get LabPlot in the Microsoft Store I used the excellent <a href="https://kate-editor.org/post/2019/2019-11-03-windows-store-submission-guide/">submission guide</a>. Before that I followed the corresponding <a href="https://phabricator.kde.org/T9575">Phabricator ticket</a> for requirements and prepared the Windows package on the Binary Factory. It took some time to do it the first time, but other application developers can surely find help if needed.

<span style="color:#f55;font-weight:bold">Jonathan:</span> LabPlot is available at no cost, did you consider charging for it the same as Krita does?

<span style="color:#55f;font-weight:bold">Stefan:</span> We are happy to provide a free software application for anyone to use. Any donation right now goes to KDE and we think this is well earned for providing such a great framework and infrastructure for developers.

We already talked internally about whether we should collect money for hiring developers but we decided that it probably won't pay off. We know that our target group is rather small and we don't have so many users as more popular KDE applications :-)

<span style="color:#f55;font-weight:bold">Jonathan:</span> Do you have a process to keep the LabPlot version on the store up to date?  Have you considered how you would handle security updates for example?

<span style="color:#55f;font-weight:bold">Stefan:</span> Updating all packages is part of our release plan as far as we can support it. It normally takes a few days after the source is published but we are only a small team :-)

Security is normally not an issue for LabPlot. As far as i can remember there were never any security problems in our code. But in case there are any, we would fix it as soon as possible and update all packages.

<span style="color:#f55;font-weight:bold">Jonathan:</span> Have you looked at other platforms and stores for LabPlot?

<span style="color:#55f;font-weight:bold">Stefan:</span> Sure. Our third major platform is macOS, which is not as popular among our users, but it is gaining more and more popularity. Improving LabPlot on macOS with the help of our users is something we constantly work on.

With the latest release we started to look at more ways to make LabPlot available.

Besides for the Microsoft Store, we also created a flatpak with the help of the Binary Factory and added the latest release on <a href="https://flathub.org/apps/details/org.kde.labplot2">Flathub</a>.

There is also a FreeBSD build on build.kde.org so we can make sure that it at least compiles on other Unix-like platforms. Besides that, I'm not aware of anyone using LabPlot on anything else than Linux, Windows or macOS. But this should already be sufficient for most users :-)
We would be happy to use more stores and platforms like for ARM architectures, as well as AppImage or Apple Store when KDE has better support for it.

<span style="color:#f55;font-weight:bold">Jonathan:</span> Thanks Stefan!
<!--break-->