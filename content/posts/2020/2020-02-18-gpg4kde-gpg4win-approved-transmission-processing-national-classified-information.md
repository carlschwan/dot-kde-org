---
title: "Gpg4KDE & GPG4win Approved for Transmission & Processing of National Classified Information"
date:    2020-02-18
authors:
  - "Paul Brown"
slug:    gpg4kde-gpg4win-approved-transmission-processing-national-classified-information
comments:
  - subject: "Beauty!"
    date: 2020-02-24
    body: "<p>Nice! A win for the community!</p>"
    author: "dps"
---
<div style="text-align: center;"><img src="/sites/dot.kde.org/files/kmail_kleopatra.jpg" alt="" width="800" /></div>
<p>
<br />
</p>

Something that may have slipped you by: Back in November, <b>the German Federal Office for Information Security <a href="https://gnupg.com/20200107-freigabe-vs-nfd.html">approved Gpg4KDE and Gpg4win for the transmission and processing of national classified information</a></b>.

Gpg4KDE is the encryption system that you use each time you encrypt and sign messages in <a href="https://kontact.kde.org/components/kmail.html">KMail</a>. <a href="https://www.gpg4win.org/">Gpg4win</a>, used for encrypting and signing emails on Windows, is built upon KDE's certificate manager <a href="https://kde.org/applications/utilities/org.kde.kleopatra">Kleopatra</a>. The German Government has now ranked both secure enough to be used when transmitting messages with <i>VS-ONLY FOR SERVICE USE (VS-NfD)</i>,  <i>EU RESTRICTED</i> and <i>NATO RESTRICTED</i> levels of confidentiality.

In view of the recent <a href="https://www.washingtonpost.com/graphics/2020/world/national-security/cia-crypto-encryption-machines-espionage/">Rubicon/Crypto AG/CIA scandal</a>, this is further evidence that FLOSS encryption technology is the only reliable encryption technology.
<!--break-->