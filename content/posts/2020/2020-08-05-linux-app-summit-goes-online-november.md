---
title: "Linux App Summit Goes Online in November"
date:    2020-08-05
authors:
  - "Paul Brown"
slug:    linux-app-summit-goes-online-november
---
<figure class="topImage">
        <img src="/sites/dot.kde.org/files/2020LAS-Social-3.png" class="img-fluid w-100" alt="LAS Banner" />
</figure>

Once again, KDE and GNOME are teaming up to bring you THE conference for people interested in establishing Linux as a great end-user platform. At the Linux App Summit we work on making app creation for users easy and worthwhile.

Since travel is complicated nowadays, we decided to make LAS 2020 a virtual conference. The event will open Thursday, 12th November and we'll wrap up on Saturday, 14th November. Our goal is to engage people in multiple time zones and make the content available online after the conclusion.

The Call for Talks is now open! Please take a look at the <a href="https://linuxappsummit.org/cfp/">suggested topics</a> and send in your ideas. We encourage new speakers, so don’t hesitate to submit a proposal!

Save the date on your calendar and we look forward to seeing you at LAS 2020!

<a href="https://linuxappsummit.org">Learn more about LAS 2020</a>.

<h3>Important Dates:</h3>

<ul>
<li>Call for Papers opens: Today!</li>
<li>Call for Papers closes: September 15th</li>
<li>Speakers announced: October 1st</li>
<li>Conference: November 12th to November 14th</li>
</ul>
<!--break-->