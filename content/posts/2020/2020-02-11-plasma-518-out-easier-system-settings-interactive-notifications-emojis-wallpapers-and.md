---
title: "Plasma 5.18 is out: easier system settings, interactive notifications, emojis, wallpapers and more"
date:    2020-02-11
authors:
  - "Paul Brown"
slug:    plasma-518-out-easier-system-settings-interactive-notifications-emojis-wallpapers-and
---
<div style="text-align: center;"><img src="/sites/dot.kde.org/files/global_themes.png" alt="" width="800" /></div>
<p>
<br />
</p>

<b>A brand new version of <a href="https://kde.org/announcements/plasma-5.18.0.php">the Plasma desktop</a> is now available.</b>

In Plasma 5.18 you will find neat new features that make notifications clearer, settings more streamlined and the overall look more attractive. Plasma 5.18 is easier and more fun to use, while at the same time allowing you to be more productive when it is time to work.

<p align="center">
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5" frameborder="0" allowfullscreen></iframe>
<p align="center">

Apart from all the cool new stuff, Plasma 5.18 also comes with an LTS status. LTS stands for "Long Term Support". This means 5.18 will be updated and maintained by KDE contributors for the next two years (regular versions are maintained for 4 months). If you are thinking of updating or migrating your school, company or organization to Plasma, this version is your best bet, as you get the most stable version of Plasma *and* all the new features too.

Check out the <a href="https://kde.org/announcements/plasma-5.18.0.php">Official announcement</a> for more details and the <a href="https://kde.org/announcements/plasma-5.17.5-5.18.0-changelog.php">full changelog</a> for every single change that went into this version. 
<!--break-->