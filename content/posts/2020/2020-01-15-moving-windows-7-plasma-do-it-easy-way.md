---
title: "Moving from Windows 7 to Plasma? Do it the Easy Way!"
date:    2020-01-15
authors:
  - "davidc"
slug:    moving-windows-7-plasma-do-it-easy-way
comments:
  - subject: "Theme name"
    date: 2020-01-15
    body: "<p>What's the theme used in this screenshot? I can't seem to find such a good Windows 10-like theme.</p>"
    author: "Anonymous"
  - subject: "Theme name"
    date: 2020-01-15
    body: "<p>Hi! I used https://github.com/fauzie811/plasma-theme-modern10light</p>"
    author: "niccolove"
  - subject: "Here is it"
    date: 2020-01-16
    body: "<p><strong style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">Plasma Theme:</strong><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">&nbsp;</span><a style=\"color: #0068c6; text-decoration-line: none; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\" href=\"https://www.pling.com/p/998614\">Seven Black</a><br style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\" /><strong style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">Window Decorations:</strong><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">&nbsp;Seven Black</span><br style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\" /><strong style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">Application Style:</strong><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">&nbsp;gtk2</span><br style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\" /><strong style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">GTK Theme:</strong><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">&nbsp;Windows Se7en by Elbullazul</span><br style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\" /><strong style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">Icons:</strong><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">&nbsp;Darkine</span><br style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\" /><strong style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">Colours:</strong><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">&nbsp;Breeze Light</span><br style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\" /><strong style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">Cursors:</strong><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">&nbsp;DMZ White</span><br style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\" /><strong style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">Splash Screen:</strong><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">&nbsp;Feren OS</span><br style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\" /><strong style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">Panel:</strong><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">&nbsp;38 height</span><br style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\" /><strong style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">Widgets:</strong><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">&nbsp;Default Apps Menu, I-O Task Manager, Stock System Tray, Feren Calendar or Event Calendar, Win7 Show Desktop</span></p>"
    author: "Rahim"
  - subject: "https://www.youtube.com/watch"
    date: 2020-01-16
    body: "<p><a href=\"https://www.youtube.com/watch?v=5td3I3IKtEs\">https://www.youtube.com/watch?v=5td3I3IKtEs</a></p>"
    author: "Alexandr"
---
<!--<div style="text-align: center;"><a href="/sites/dot.kde.org/files/plasma10.png"><img src="/sites/dot.kde.org/files/plasma10.png" alt="" width="800" /></a><br />Plasma desktop with a Windows 10 layout.</div>-->
<div style="text-align: center;"><a href="/sites/dot.kde.org/files/Screenshot_20200115_000427.jpg"><img src="/sites/dot.kde.org/files/Screenshot_20200115_000427.jpg" alt="" width="800" /></a><br />Plasma desktop with a Windows 10-like theme.</div>
<p><br /></p>
<b>Today Microsoft has left Windows 7 users behind.<b>

Redmond will no longer provide updates for the 2009 operating system. This puts almost a billion people in the difficult situation of facing increased security risks alongside a slow decline in software availability.

Folks who reject Microsoft’s forced updates are already opting to regain control over their systems by switching to the friendly and full-featured <a href="https://kde.org/plasma-desktop">Plasma desktop</a>, built on a design philosophy which centers freedom and respect for its users. Recent buzz about the <a href="https://dot.kde.org/2020/01/08/plasma-safe-haven-windows-7-refugees">possibilities of Plasma</a> has brought a lot of fresh faces on board, and now they are trying to navigate a new operating system that has its differences from Windows.

If you’re one of those people, you’re probably wondering where you can find experienced users to help you get settled in.

<h2>How to make the jump with ease</h2>

Luckily, there is a wealth of resources available for those new to Plasma and the Linux world.

The best place to talk live with Plasma users, ask questions, and get to know the KDE community is the <a href="https://webchat.kde.org/#/room/#kde-welcome:kde.org">KDE Welcome room on our webchat</a> or on Matrix. If you want to discuss Plasma and comment on the latest KDE news with other users, find <a href="https://www.reddit.com/r/kde/">r/KDE on Reddit</a> or check out the official <a href="https://forum.kde.org/">KDE forums</a>.

<a href="https://askubuntu.com"/>AskUbuntu.com</a> is the largest dedicated tech support site in the Linux world, and an invaluable resource for anyone using KDE Neon or Kubuntu. Much of the info available here even translates well to other Linux flavors. Other places for specific support questions include <a href="https://www.reddit.com/r/linux4noobs">r/Linux4Noobs</a> and <a href="https://www.reddit.com/r/linuxquestions">r/LinuxQuestions</a> on Reddit. Talking of which, another great resource is the <a href="https://www.linuxquestions.org/">Linux Questions</a> forums.

Once you have a little bit of experience under your belt, if you run into trouble with a specific system component, you can always resort to the <a href="https://wiki.archlinux.org/">Arch Linux wiki</a>, an in-depth hub of documentation which is often useful to users of any Linux system.

Everyone in the KDE community is familiar with the hurdles new users face when making the jump to Plasma and the Free Software world. Don’t hesitate to take advantage of KDE's welcoming community who will help you feel right at home in Plasma and make sure you get the most out of your newly upgraded system.
<!--break-->