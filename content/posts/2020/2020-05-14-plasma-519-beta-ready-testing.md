---
title: "Plasma 5.19 Beta Ready for Testing"
date:    2020-05-14
authors:
  - "jriddell"
slug:    plasma-519-beta-ready-testing
---
    <figure class="topImage">
        <a href="https://www.kde.org/announcements/plasma-5.19/plasma-5.19-2.png" data-toggle="lightbox">
            <img src="https://www.kde.org/announcements/plasma-5.19/plasma-5.19-2.png" height="450" width="800" style="width: 100%; max-width: 800px; height: auto;" alt="Plasma 5.19 Beta" />
        </a>
        <figcaption>KDE Plasma 5.19 Beta</figcaption>
    </figure>

    <p>Thursday, 14 May 2020.</p>

    <p>It's time to test the beta release for <a href="https://kde.org/announcements/plasma-5.18.90">Plasma 5.19</a>!</p>

    <p>In this release, we have prioritized making Plasma more consistent, correcting and unifying designs of widgets and desktop elements; worked on giving you more control over your desktop by adding configuration options to the System Settings; and improved usability, making Plasma and its components easier to use and an overall more pleasurable experience.</p>

    <p>Read on to discover all the new features and improvements of Plasma 5.19…</p>

    <h2 id="desktop">Plasma Desktop and Widgets</h2>

    <figure class="float-right text-right">
        <a href="https://www.kde.org/announcements/plasma-5.19/sensors.png" data-toggle="lightbox">
            <img src="https://www.kde.org/announcements/plasma-5.19/sensors.png" class="border-0 img-fluid" width="242"  alt="Rewritten System Monitor Widgets" />
        </a>
        <figcaption>Rewritten System Monitor Widgets</figcaption>
        <br />
        <a href="https://www.kde.org/announcements/plasma-5.19/system-tray-applets.png" data-toggle="lightbox">
            <img src="https://www.kde.org/announcements/plasma-5.19/system-tray-applets.png" class="border-0 img-fluid" width="242"  alt="Consistent System Tray Applets" />
        </a>
        <figcaption>Consistent System Tray Applets</figcaption>
        <br />
        <a href="https://www.kde.org/announcements/plasma-5.19/user-avatars.png" data-toggle="lightbox">
            <img src="https://www.kde.org/announcements/plasma-5.19/user-avatars.png" class="border-0 img-fluid" width="242"  alt="Completely New User Avatars" />
        </a>
        <figcaption>Completely New User Avatars</figcaption>
        <br />
        <a href="https://www.kde.org/announcements/plasma-5.19/multi-device-switching-ui.png" data-toggle="lightbox">
            <img src="https://www.kde.org/announcements/plasma-5.19/multi-device-switching-ui.png" class="border-0 img-fluid" width="242"  alt="More consistent appearance for switching the current audio device" />
        </a>
        <figcaption>More consistent appearance for switching the current audio device</figcaption>
    </figure>

    <ul>
<li>We have improved the panel spacer so that it can automatically center widgets</li>
<li>The System Monitor widgets have been rewritten from scratch</li>
<li>Plasma now has a consistent design and header area in system tray applets as well as notifications</li>
<li>We have refreshed the look of the media playback applet in the System Tray and of Task Manager tooltips</li>
<li>There are completely new photographic avatars to choose from</li>
<li>You can now see the name of the creator of a desktop wallpaper when you go to pick one</li>
<li>Sticky notes get usability improvements</li>
<li>You now have more control over the visibility of volume OSDs during certain situations</li>
<li>GTK 3 applications immediately apply a newly selected color scheme and GTK 2 applications no longer have broken colors</li>
<li>We have increased the default fixed-width font size from 9 to 10</li>
<li>More consistent appearance with less ugly UI for switching the current audio device</li>
    </ul>

    <br clear="all" />

    <h2 id="systemsettings">System Settings</h2>

    <figure class="float-right text-right">
        <video width="350" height="240" autoplay="true" loop>
            <source src="https://www.kde.org/announcements/plasma-5.19/krunner.webm" type="video/webm" />
        </video>
        <figcaption>Full System Settings App Is Now Launching</figcaption>
        <br />
        <a href="https://www.kde.org/announcements/plasma-5.19/redesigned-kcms.png" data-toggle="lightbox">
            <img src="https://www.kde.org/announcements/plasma-5.19/redesigned-kcms.png" class="border-0 img-fluid" width="350"  alt="Redesigned Settings Pages" />
        </a>
        <figcaption>Redesigned Settings Pages</figcaption>
    </figure>

    <ul>
<li>Default Applications, Online Accounts, Global Shortcuts, KWin Rules and Background Services settings pages have all been overhauled</li>
<li>When launching System Settings modules from within KRunner or the application launcher, the complete System Settings application launches on the page you asked for</li>
<li>The Display settings page now shows the aspect ratio for each available screen resolution</li>
<li>You now have more granular control over Plasma's animation speed</li>
<li>We have added configurable file indexing for individual directories and you can now disable indexing for hidden files</li>
<li>There is now an option that lets you configure the mouse and touchpad scroll speed under Wayland</li>
<li>We have made lots of small improvements to the font configuration</li>
    </ul>

    <br clear="all" />

    <h2 id="kinfocenter">Info Center</h2>

    <figure class="float-right text-right">
        <a href="https://www.kde.org/announcements/plasma-5.19/kinfocenter.png" data-toggle="lightbox">
            <img src="https://www.kde.org/announcements/plasma-5.19/kinfocenter.png" class="border-0 img-fluid" width="350"  alt="Redesigned Info Center" />
        </a>
        <figcaption>Redesigned Info Center</figcaption>
    </figure>

    <ul>
<li>The Info Center application has been redesigned with a look and feel that is consistent with the System Settings</li>
<li>It is now possible to see information about your graphics hardware</li>
    </ul>

    <br clear="all" />

    <h2 id="kwin">KWin Window Manager</h2>

    <figure class="float-right text-right">
        <a href="https://www.kde.org/announcements/plasma-5.19/recoloured-icons.png" data-toggle="lightbox">
            <img src="https://www.kde.org/announcements/plasma-5.19/recoloured-icons.png" class="border-0 img-fluid" width="249" alt="Icon Recoloring in the Titlebar" />
        </a>
        <figcaption>Icon Recoloring in the Titlebar</figcaption>
    </figure>

    <ul>
<li>The new subsurface clipping for Wayland greatly reduces the flickering in many applications</li>
<li>Icons in titlebars are now recolored to fit the color scheme instead of sometimes being hard to see</li>
<li>And in Wayland screen rotation now works for tablets and convertable laptops</li>
    </ul>

    <br clear="all" />

    <h2 id="discover">Discover</h2>

    <figure class="float-right text-right">
        <a href="https://www.kde.org/announcements/plasma-5.19/discover.png" data-toggle="lightbox">
            <img src="https://www.kde.org/announcements/plasma-5.19/discover.png" class="border-0 img-fluid" width="350"  alt="Flatpak Repository Removal in Discover" />
        </a>
        <figcaption>Flatpak Repository Removal in Discover</figcaption>
    </figure>

    <ul>
<li>Flatpak repositories in use are easier to remove now</li>
<li>Discover displays the application version for reviews</li>
<li>Discover improved its visual and usability consistency</li>
    </ul>

    <br clear="all" />

    <h2 id="ksysguard">KSysGuard</h2>
<!--
    <figure class="float-right text-right">
        <a href="https://www.kde.org/announcements/plasma-5.19/ksysguard.png" data-toggle="lightbox">
            <img src="https://www.kde.org/announcements/plasma-5.19/ksysguard.png" class="border-0 img-fluid" width="350"  alt="More than 12 CPU cores in KSysGuard" />
        </a>
        <figcaption>More than 12 CPU cores in KSysGuard</figcaption>
    </figure>
-->
    <ul>
<li>Our system monitor KSysGuard has gained support for systems with more than 12 CPU cores</li>
    </ul>

    <br clear="all" />
    
    <a href="https://www.kde.org/announcements/plasma-5.18.5-5.18.90-changelog.php">Full Plasma 5.19 Beta changelog</a>
<!--break-->
