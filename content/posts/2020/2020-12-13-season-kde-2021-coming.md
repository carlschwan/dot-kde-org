---
title: "Season of KDE 2021 is coming"
date:    2020-12-13
authors:
  - "Paul Brown"
slug:    season-kde-2021-coming
---
Since 2013, the KDE Student Programs has been running <a href="https://season.kde.org">Season of KDE</a>. Season of KDE is a program similar to, but not quite the same as, Google Summer of Code. It offers an opportunity for everyone (not just students) to participate in both coding and non-coding projects that benefit the KDE ecosystem. In the past few years, SoK participants have not only created new application features, but have also developed the KDE Continuous Integration System, statistical reports for developers, a web framework, ported KDE applications, created documentation, and contributed to KDE with lots and lots of other tasks.

<figure class="float-md-center w-100 p-2" style="max-width: 100%">
    <a href="/sites/dot.kde.org/files/Group_Photo.jpg" data-toggle="lightbox" class="position-relative">
        <img class="img-fluid" src="/sites/dot.kde.org/files/Group_Photo.jpg" />
    </a>
   <figcaption align="center">Our friendly KDE mentors will guide you and help you achieve your goals.</a></figcaption>
</figure> 

<a href="https://season.kde.org">The Season of KDE 2021 timeline is now online</a> and the season starts now and you have until January 4 to find a project that interests you. This year the coding period will be longer, as we found that the 3 weeks full-time coding period from earlier editions was too stressful for the students and for the mentors. Season of KDE is meant to be a fun event to participate in, so this year, you will have more time to complete your task and you can have a more flexible timeline.

There is already a list of proposed projects available in the <a href="https://community.kde.org/SoK/Ideas/2021">wiki</a>. You have little less than a month to find a project that interests you and your mentor. Your goal is to get noticed by the mentors by, for example, sending Merge Requests to their projects, sending high-quality bug reports, or simply by starting to interact with them. Remember that many KDE developers have a life beyond KDE and won't respond immediately. Also, it is recommended you contact the mentors in the public channel so that if they can't respond, someone else can.

Once you find a project and a mentor, you can submit a proposal for a project at the <a href="https://season.kde.org">Season of KDE site</a>. Note that you will need a <a href="https://identity.kde.org">KDE Identity</a> account to register. We have some guidelines for proposals on the wiki. Your mentor will review your idea and, if nothing goes wrong, your proposal will be accepted on the 11th of January.

If you have any questions, you can ask in the <a href="https://webchat.kde.org/#/room/#freenode_#kde-soc:matrix.org">#kde-soc</a> channel.
<!--break-->