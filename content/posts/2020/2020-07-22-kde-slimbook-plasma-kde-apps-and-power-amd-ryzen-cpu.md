---
title: "KDE Slimbook: Plasma, KDE Apps and the Power of the AMD Ryzen CPU"
date:    2020-07-22
authors:
  - "Paul Brown"
slug:    kde-slimbook-plasma-kde-apps-and-power-amd-ryzen-cpu
---
<figure class="topImage">
    <a href="/sites/dot.kde.org/files/banner1.jpg" data-toggle="lightbox">
        <img src="/sites/dot.kde.org/files/banner1.jpg" style="width: 100%; height: auto;" alt="Banner" />
    </a>
</figure>

<b>Today Slimbook and KDE launch the new <a href="https://kde.slimbook.es">KDE Slimbook</a>.</b>

The third generation of this popular ultrabook comes in a stylish sleek magnesium alloy case that is less than 20 mms thick, but packs under the hood a powerful AMD Ryzen 7 4800 H processor with 8 cores and 16 threads. On top of that runs KDE's Plasma desktop, complete with a wide range of preinstalled, ready-to-use Open Source utilities and apps.

Both things combined make the KDE Slimbook a one-of-a-kind machine ready for casual, everyday use, gaming and entertainment; design work, animation, and 3D rendering; as well as hardcore software development.

The KDE Slimbook can fit up to 64 GBs of DDR4 RAM in its two memory sockets, and has three USB ports, a USB-C port, an HDMI socket, a RJ45 for wired network connections, as well as support for the new Wifi 6 standard.

<figure class="topImage">
    <a href="/sites/dot.kde.org/files/photo_2020-07-20_21-10-47.jpg" data-toggle="lightbox">
        <img src="/sites/dot.kde.org/files/photo_2020-07-20_21-10-47.jpg" style="width: 100%; height: auto;" alt="Cover" />
    </a>
</figure>

It comes in two sizes: the 14-inch screen version weighs 1.07 kg, and the 15.6-inch version weighs 1.49 kg. The screens themselves are Full HD IPS LED and cover 100% the sRGB range, making colors more accurate and life-like, something that designers and photographers will appreciate.

Despite its slim shell, the AMD processor and Plasma software deliver enough power to allow you to deploy a full home office with all the productivity and communications software you need. You can also comfortably browse the web and manage social media, play games, watch videos and listen to music. If you are the creative type, the Ryzen 4800 H CPU is well-equipped to let you express your artistic self, be it with painting apps like <a href="https://krita.org">Krita</a>, 3D design programs like <a href="https://www.blender.org/">Blender</a> and <a href="https://www.freecadweb.org/">FreeCAD</a>, or video-editing software like <a href="https://kdenlive.org">Kdenlive</a>.

<figure class="topImage">
    <a href="/sites/dot.kde.org/files/IMG_0759_e.png" data-toggle="lightbox">
        <img src="/sites/dot.kde.org/files/IMG_0759_e.png" style="width: 100%; height: auto;" alt="Side by side" />
    </a>
</figure>

If you are into software development, you are in luck too: KDE provides all the tools you need to code and supports your favorite languages and environments. Meanwhile, Slimbook's hardware is ideal for CPU-intensive tasks and will substantially shorten your build times.

Pricing for the KDE Slimbook starts at approximately € 899 for the 14'' version and at   € 929 for the 15.6'', making it more affordable than most similarly-powered laptops. Besides, when you order a KDE Slimbook, you will also be contributing to KDE, as the Slimbook company actively supports and sponsors KDE and donates part of the proceedings back into the Community.

Find out more from the <a href="https://kde.slimbook.es">KDE Slimbook page</a>.
<!--break-->