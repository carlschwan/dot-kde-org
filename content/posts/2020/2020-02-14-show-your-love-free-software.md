---
title: "Show Your Love for Free Software"
date:    2020-02-14
authors:
  - "jriddell"
slug:    show-your-love-free-software
---
<img src="/sites/dot.kde.org/files/ILoveFS-2020image1.png" width="627" height="150" />

In recent decades, Free and open source software (FOSS) has increasingly been the enabling factor for advances in areas we probably aren’t even aware of. If software is still spreading around the world, FOSS had already spread through the software world. All of that is only possible because of striving communities that push solutions forward with an amazing flow of continuous passion and love for nice technology, open knowledge, and supporting people. KDE is not any different - we have all been involved in such a lovely addiction for 23 years.

Today, February 14th 2020, The Free Software Foundation Europe calls everyone to express their gratitude to all FOSS contributors around the world with the eleventh annual “I Love Free Software” campaign. It’s a day when we focus on drawing everyone’s attention to the amazing work done by thousands of FOSS contributors from many communities, most of them voluntarily dedicating their spare time to create high-quality software technology readily and openly available to everyone.

What about you? Have you or your company/university been using Free software lately? Have you already thought about contributing back to that amazing FOSS community that creates the applications you use daily? It’s certainly a very rewarding and inspiring experience, with a lot of contributions made possible by people from different backgrounds. 

For now, how about enjoying this day by telling us why you love Free software, or expressing your gratitude to the people behind those projects? Use #ilovefs and #ilovekde tags on your social media to share that picture of you with your favorite Free software T-shirt, or show off all those amazing stickers on your laptop’s lid. Organize a meet-up, submit a bug fix or documentation improvement, make a donation, or just hang out with some friends to celebrate! Everything counts when letting the world know how grateful and passionate we are about Free software.

<img src="/sites/dot.kde.org/files/ILoveFS-2020image2.jpg" width="750" height="272" />

As nearly all large and experienced Free software communities, KDE is made out of people who strongly believe that enabling everyone to take control of their digital lives can really make a difference. We love being part of this, and that love unfolds into many different actions. We develop almost 200 applications translated to 76 languages plus a full-fledged modern desktop environment (KDE Plasma). We run many contribution sprints around the world in addition to our main yearly gathering (Akademy), where we renew our energy by meeting old and new friends in person. What a lovely way to enjoy our lives! 

Today, we invite you to join us in this vibrant flow of gratitude by sharing your FOSS love stories on social media. Use #ilovefs and #ilovekde tags in your posts, so we can find them more easily. We would love to hear about your favorite KDE things.

            <a class="shareFacebook" href="https://www.facebook.com/kde/" rel="nofollow">KDE on Facebook</a>
            <a class="shareTwitter" href="https://twitter.com/kdecommunity" rel="nofollow">KDE on Twitter</a>
            <a class="shareDiaspora" href="https://joindiaspora.com/people/9c3d1a454919ef06" rel="nofollow">KDE on Diaspora</a>
            <a class="shareMastodon" href="https://mastodon.technology/@kde" rel="me nofollow">KDE on Mastodon</a>
            <a class="shareLinkedIn" href="https://www.linkedin.com/company/29561/" rel="nofollow">KDE on LinkedIn</a>
            <a class="shareReddit" href="https://www.reddit.com/r/kde/" rel="nofollow">KDE on Reddit</a>

<!--break-->
