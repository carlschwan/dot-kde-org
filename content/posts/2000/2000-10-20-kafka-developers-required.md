---
title: "Kafka: Developers Required"
date:    2000-10-20
authors:
  - "jbacon"
slug:    kafka-developers-required
comments:
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "Great idea! As a web developer I miss such a application a lot under linux. \nOne suggestion:Maybe you could exchange some code with Quanta which already provides a nice non-WYSIWYG solution for webdesign?\n\ncheers for the great idea\nfranz"
    author: "franzbe"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "We are going to try and make Kafka as extensible as possible with a good plugin system so developers can create new and interesting plugins. this could include a Quanta plugin.\n\nI have used Quanta, and I am sure we will learn and no doubt use code or learn from it in other ways.\n\nKeep the suggestions coming, and get involved if you can. ;-)"
    author: "Jono Bacon"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "<p>When we first started Quanta some people with KDE asked us why we did not first approach the developer of Webmaker. Actually we talked with him but it did not look like we could make a match as we were looking to do more quicker.</p>\n</p>As far as a plug in goes it seems to make more sense to go the other way around. We could easily plug a graphical editor into Quanta in fact we have talked about it and are considering it. The fact that we have an extensive and well thought out document parsing engine should be obvious if you load a PHP page or pop open the struct tab and navigate a big page. I realize that kafka is still very early on but wysiwyg seems to be pretty much it's pull right now. I'm not upset that we have not been contacted about putting a graphical tool in Quanta and working as a team. In fact, as with Webmaker, there may not be a match there. Still it looks pragmatic to explore should there be an interest.</p>\n<p>Quanta is also being designed to be modular and extensible. In fact you can already customize any tag dialog with XML and in a few days you will be able to customize any tool bar. The Document Structure Navigation is also nothing short of revolutionary. True it is not wysiwyg but in our minds the web is not exactly wysiwyg either and we have yet to see a graphical tool that actually did exactly what we wanted and didn't create difficulties with our control of the end product.</p>\n<p>However it seems to us the optimum web development tool would have to marry the two ideas of editing the source and graphically laying out. That would mean professional developers could still coax out that last 10-15% that makes it shine. In our opinion a purely graphical tool (we know kafka is not) is a novice tool and while we want to make it easier for novices we want to make it dynamite for professional web developers. After all, that is what we are and how and why we build Quanta. our objective is to give the user total control... and we are a bit further along. ;)</p>\n<p> We wish the kafka team the best. They certainly have done an excellent job with thier preliminary docs and organization. If they want to open a dialog to exchange ideas or perhaps some collaboration we are very much open to talking. It seems our objectives are both to produce the best perhaps with some difference in focus, but perhaps a fair amount of similarity?</p>\n<p>Our objective is to see KDE have the best tools whoever produces it. We openly invite the kafka developers to dialog with us it they want. We have a development team in place as well as substantial funtionality they will need to assemble and we really like what they are doing. Perhaps there is a complementary match to advance our objectives or at least some information exchange that would benefit the community</p>"
    author: "Eric Laffoon"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "To both the Kafka and Quanta teams I'd suggest that you take a look at the Namo Web Editor for Windows (www.namo.com). In my opinion that's one of the best WYSIWYG editors around (many of its users agree with me, unfortunately it's still widely unknown). You might get some good ideas from it concerning the user interface and functionality.\n\nPersonally I'd really like to see WYSIWIYG in Quanta.\n\nRegards,\nJoerg"
    author: "Joerg Benscheidt"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-21
    body: "If Kafka turns out anything like the Namo WedEditor for Windows, I'll finally be able to delete windows on my computer!"
    author: "Chris Aakre"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "Thanks Eric for these kind and supportive words.\n\nOur intention for the Kafka project is to basically provide a soloution for the many web designers who prefer or rely upon a WYSIWYG tool. we realise that many developers prefer to use a direct source based editing approach, and we respect that these are two different needs.\n\nI feel that we can supplement our seperate projects in the way you suggested, and this can be done with crosslinking between the applications and some code reuse.\n\nThe great thing about KDE development is that competition is friendly. :-)\n\nI look forward to opening and contributing to some discussions between the two projects, and I more than welcome  you to get in touch with me if you wish to open some dialog."
    author: "Jono Bacon"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "How about making Kafka a part and allowing a user to use it embedded in quanta instead of the preview. Just an idea."
    author: "rsv"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "we are actually developing Kafka so it as flexible as possible, and we are currently developing a KHTMLEditor class to handle the WYSIWYG requirement. This should make embedding it in other applications such as Quanta fairly straightforward."
    author: "Jono Bacon"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-21
    body: "I got the idea when I was designing a site. As a newbie who was handling this for the first time, I used Dreamweaver on windows initially (shareware version that 'expired' in 30 days.) I then switched to quanta, found out a large number of redundant font tags and could improve on the pages. I would use this approach, first designing the site in WYSIWYG, then opening it in source and editing it. It saves time.\nQuanta could have, instead of a preview, a WYSIWYG component which would allow one to edit the page, and simultaneously see the changes in the code in quanta. Similarly any changes done in the source would be seen in the WYSIWYG view. This would allow one to use both modes simultaneously, making it a unique app. \nThe WYSIWYG part could have other uses too. like the kmail composer, as an option."
    author: "rsv"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "we are actually developing Kafka so it as flexible as possible, and we are currently developing a KHTMLEditor class to handle the WYSIWYG requirement. This should make embedding it in other applications such as Quanta fairly straightforward."
    author: "Jono Bacon"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "Isn't Mozilla Composer a good source of inspiration too?"
    author: "Al_trent"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "We are going to try and make Kafka as extensible as possible with a good plugin system so developers can create new and interesting plugins. this could include a Quanta plugin.\n\nI have used Quanta, and I am sure we will learn and no doubt use code or learn from it in other ways.\n\nKeep the suggestions coming, and get involved if you can. ;-)"
    author: "Jono Bacon"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "never mind ;-)\n\nI would really like to help. I'm no experienced c++ programmer (just starting) and also don't know much about KDE2 but I could do some translation (german). \nSo I will subscribe to the mailing list. Hope I can also help with my experience as web developer.\n\ncheers\nFranz"
    author: "franzbe"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "Oops...sorry for being a bit over eager with the post button."
    author: "Jono Bacon"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "Hello all, and Jono, I am 13 and I want to learn <code>C++</code> to help KDE... Any suggestions on how to learn? How did everybody else learn? \n<p>\nThanks!"
    author: "Jason Katz-Brown"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "Well, if you don't know how to program, I wouldn't recommand you to start with C++, but with a simpler language such as Python. (look at his webpage www.python.org, the docs and tutorial are great).\nIf you already are an 'experienced' programmer with some language and want to learn C++/KDE, try out the C++ Annotations book (http://www.icce.rug.nl/docs/cplusplus/cplusplus.html) and the Qt tutorial (with every Qt distrib). \n\nJust my 2 cents..."
    author: "Al_trent"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "Hi,\nI think a good side is www.kdevelop.org -\na lot of information and links to tutorials.\ntry it..."
    author: "Martin K."
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "Buy the Stroustrup-book."
    author: "Lenny"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "<i>Buy the Stroustrup-book.</i>\n<br><br>\nDo you want to scare new developers away? This is certainly the last book to recommend to <i>learn</i> C++."
    author: "gis"
  - subject: "Learning C++"
    date: 2000-10-20
    body: "Hi!\n\n<p>You can get a really nice Book called \"Thinking in C++\" by Bruce Eckel in html. Download it for free <a href=\"http://www.mindview.net\">here</a>.\n<p>Have fun!\n<p>Franz"
    author: "franzbe"
  - subject: "Re: Learning C++"
    date: 2001-09-23
    body: "please teach me about c++, that program useful for me in college\ni'm so glad if you can tell me more detail about that"
    author: "indra"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "i found this funny tag in the screenshot section of the kafka-site:\n\n<TD background=../index_files/stripes.gif colSpan=3><IMG alt=\"\" height=1    src=\"E:\\kafkasite\\index_files\\pixel(1).gif\" width=1>"
    author: "mic"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "Oops. Looks like an old bit of code from our previous page. I will fix it."
    author: "Jono Bacon"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "A wonderful idea.. being code-stupid I have always tended to use StarOffice (ok,nok, stop laughing) as the only WYSIWYG web tool available so far, skippling the Netscape composer that I don't even want to comment upon. \nToo bad I am not a programmer... but I am one of the tens of thousands who will be forever grateful for such a piece of soft.\nMy question however would be related to what extend the soft will allow project management. Usually anyone would think in terms of a whole project, such as a serie of pages devoted to the same subject (smth that Composer has missed completely). While Dreamweaver allowed some of it, the emphasis on design tricks dont really make it suitable for anything real big.\nAnyways, good, good news! thanks guyz!"
    author: "Divine"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-20
    body: "We aim to implement a complete project management system in Kafka. Remember that any suggestions are greatfully recieved on the mailing list. ;-)"
    author: "Jono Bacon"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-21
    body: "I've used KDevelop to write the HTML, PHP, Python, and C++ portions of www.merchantempires.net.  If anyone liked Trade Wars, come check out my game.\n\nAll I want in a tool is a nice tree control for viewing and editing files quickly.  Autosave is also nice.\n\nDoes anyone know of good PHP/HTML editors that compare well with the what I can already do in Kdevelop?"
    author: "Anonymous"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-21
    body: "<p>\ncheck out quanta\n</p>\n<p>\n<a href=\"http://quanta.sourceforge.net/\">http://quanta.sourceforge.net/</a>\n</p>\n<p>\nit might help.\n</p>\n<p>\n-Chris\n</p>"
    author: "Chris Lee"
  - subject: "Re: Kafka: Developers Required"
    date: 2000-10-21
    body: "I've used KDevelop to write the HTML, PHP, Python, and C++ portions of www.merchantempires.net.  If anyone liked Trade Wars, come check out my game.\n\nAll I want in a tool is a nice tree control for viewing and editing files quickly.  Autosave is also nice.\n\nDoes anyone know of good PHP/HTML editors that compare well with the what I can already do in Kdevelop?"
    author: "Anonymous"
---
The open source WYSIWYG web editor project <a href="http://www.jonobacon.co.uk/kafka/">Kafka</a> is looking for interested developers. We are looking for coders, documentation writers, testers etc. Anyone and everyone is invited to get involved.

You can find out more <a href="http://www.jonobacon.co.uk/kafka/">here</a>.


<!--break-->
