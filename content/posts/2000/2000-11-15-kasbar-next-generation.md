---
title: "Kasbar - The Next Generation"
date:    2000-11-15
authors:
  - "rmoore"
slug:    kasbar-next-generation
comments:
  - subject: "Nice"
    date: 2000-11-15
    body: "It sounds really cool, but I thought it might be cool to be able to completely control the size of the bar, like horizontally and vertically, so you could resize the bars to whatever dimensions you want, or put four quater-size bars at once instead of one huge one.\nThanks!"
    author: "ralian"
  - subject: "Re: Nice"
    date: 2000-11-15
    body: "Take a look at the screenshots I posted a link to in my comment below. As you'll see you have some degree of flexibility already. I'm not sure that the effort required to support multiple bars is worth it - it would need fairly major changes and would probably need the extension API to change too.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "Wow! transparency would be really really cool, and a quick picture of windows would be neat. And having it be external would be very great. Same with shade indicator. Keep up good work.\n<p>\nI need to get less lazy and compile from cvs."
    author: "Jason"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "<p>Richard, as usual you are spot on! I've got to shake up my desktop and got get the cvs again. Keep up the good work.</p>"
    author: "Eric Laffoon"
  - subject: "On transparency in KDE"
    date: 2000-11-15
    body: "I' ve had this problem with transparency since i first compiled from CVS.<br>\nThe first time i run konsole after i login it never comes up with the transparent background. I have to move the window to force it to work. It comes up transparent from there on. I reported this bug a long time ago but it never got fixed.<br><br>\nAlso, if i use multiple background mode, the wallpaper changes but the konsole doesn't update their windows until i move them. They should update to reflect the new wallpaper."
    author: "Storm"
  - subject: "Re: On transparency in KDE"
    date: 2000-11-16
    body: "This would require constantly polling the background, something most people don't think is worth it. Get E-term if cool transparency is so important to you."
    author: "Adrian Kubala"
  - subject: "Re: On transparency in KDE"
    date: 2000-11-17
    body: "instead of polling,why not implement a listener,you see,the user install a new background,one of kde's daemon (i don't know which daemon,maybe sycoca ??) get informed and the daemon tell kasbar to update its background.\n\nAlain"
    author: "Alain Toussaint"
  - subject: "Re: On transparency in KDE"
    date: 2003-09-03
    body: "That means that they have to use a java like Listeners. But, if an application is busy or dead you might lock the entire desktop waiting for the application to respond. To avoid this they should implement some poolong techniques... that's going to be time and processor consuming."
    author: "TI"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "Sounds great, but what I don't see covered here is a way to hide the kasbar in the same way as kicker does (arrows on both or either sides).\n\nAnd what I really,really,really would love to see is to have those great WindowMaker applets inside it. I think this will be possible, because I've allready seen the wmmatrix applet somewhere in a screenshot of the extension panels, but i'm not sure this means it wil be possible in the kasbar as well."
    author: "Netizen51"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "I guess that autohide etc. will be part of the extension framework itself once the API is complete. Your other point about WM applets is interesting, but I don't think these should be inside Kasbar itself. Instead they should be in an extension panel that is set to be stretched rather than full length, which would give much the same effect.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "How about enabling autohide on the Kasbar?  I miss this from KDE1.  \n<BR>\nThis would of course be optional.."
    author: "KDE fan"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "Anyone tried the BeOS taskbar? There is one feature there I really miss in both KDE and Windows. There is only one button for each application on the taskbar no matter how many instances or windows there are of that app. To access the individual windows you just click the button and a menu pops up (similar to the start menu). Not sure if this can be done but I guess that dcop should I theory make this possible.\nWhen there is only one instance of an app the taskbar should work like a normal taskbar (no pop-up menu)."
    author: "Erik Engheim"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "I never used BeOS, but the feature you describe is very nice!! I always have alot of windows opened and now I sometimes have trouble to find the right to raise, with al those buttons on my taskbar. Creating jus one button for every application in stead for every window would make this a lot easier!!!\n\nRinse"
    author: "rinse"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "Cool. I like it. Would be sooo useful when running Gimp. \n\nIf this feature is user configurable and application specific it would be even better. I think that I would like one top level button for each Netscape instance..."
    author: "Johan"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "A feature similar to this has been around for years on Acorn Risc OS - when an application was started it's icon would appear on the taskbar, then you would click on it to create a new document or click on it with the menu button (middle on on the 3 button mouse) to call up a menu with a list of currently open documents on it.\n\nI'd like to see this feature implemented, it's something I've wanted for a while but I've but up with Win95 style taskbar icons because they do the trick (albeit not as good IMHO)"
    author: "Andrew Coles"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "Interesting, I have been hearing a lot about RiscOS lately. Especially when I mentioned features I thought was unique to Nexstep. Pitty I have never tried RiscOS before. The first and only time I read about Acorn Computers was in a computer magazine from 89-90 (Byte or commputer world I think). Are Acorn computers still in production/development?"
    author: "Erik Engheim"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-16
    body: "Acorn computers are pretty much dead. I think they are into set-top boxes now... haven't heard much in ages. You can still buy the Risc OS though: <A HREF=\"www.riscos.co.uk\">www.riscos.co.uk</A>"
    author: "ac"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-16
    body: "Well... I've read some Win Whistler announce, and they said that feature will be implemented in it.\n\nI too agree it's a nice feature..."
    author: "Marko Rosic"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "Anyone tried the BeOS taskbar? There is one feature there I really miss in both KDE and Windows. There is only one button for each application on the taskbar no matter how many instances or windows there are of that app. To access the individual windows you just click the button and a menu pops up (similar to the start menu). Not sure if this can be done but I guess that dcop should I theory make this possible.\nWhen there is only one instance of an app the taskbar should work like a normal taskbar (no pop-up menu)."
    author: "Erik Engheim"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-16
    body: "I remember seeing a suggestion similar to this after people saw the feature in Whistler.  The argument against it was that this goes against the idea of a \"document centric\" desktop.  Still, I think it is an idea worth exploring."
    author: "Doug Welzel"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "What I'd also like to see is on option to <b>HIDE an application</b> from the taskbar. For example I prefer xosview running on my machine in all windows, it is never iconized and is always in the same place.<p>\nTherefore I do not need taskbar/kasbar to manage it ( unless maybe for whatever reason it has been \niconized )."
    author: "con"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "I think that's a great idea.. You could for example still access the hidden items by going to a \"Hidden\" submenu on the RMB-menu, and from there either un-hide them or select them.\n\nOr perhaps enable the user to configure kasbar not to show entries for certain apps.. endless possibilities ;) Too bad my coding skills don't suffice, or I'd certainly implement this myself :)"
    author: "Haakon Nilsen"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "IIRC You can already do this by launching xosview with kstart. I'm not sure, but you should check it out.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "Yes, something like systray in Windows."
    author: "Anders"
  - subject: "kasbar and xmms"
    date: 2000-11-15
    body: "Please consider xmms. The window menu does not contain the standard items\nlike minimize, maximize, move to desktop. These things are only available when\nright clicking in the standard taskbar."
    author: "Wolfgang Rohdewald"
  - subject: "Re: kasbar and xmms"
    date: 2000-11-15
    body: "The default shortcut for getting that menu is Alt+F3.  Open XMMS and give it focus, then press Alt+F3 and the operations menu should pop up :)"
    author: "Christian A Str\u00f8mmen"
  - subject: "Re: kasbar and xmms"
    date: 2000-11-15
    body: "Thank you for the tip. But this is no help for \nmost users who just point and click and would never find out about ALT+F3"
    author: "Wolfgang Rohdewald"
  - subject: "Re: kasbar and xmms"
    date: 2000-11-15
    body: "It is sure KDE won't be able to provide perfect fixes for all non-KDE compliant apps existing out there. Apps have to do their job by themselves."
    author: "Inorog"
  - subject: "Re: kasbar and xmms"
    date: 2000-11-16
    body: "The point is, when you access the window menu through the taskbar, it <i>will</i> be the correct menu, not XMMS's right-click menu. This is nothing but an improvement."
    author: "Adrian Kubala"
  - subject: "Re: kasbar and xmms"
    date: 2000-11-18
    body: "That's one of the many reasons that I and a fair number of people use Linux.  I'd like to think that most people using Linux don't just \"point and click.\"  Don't make me get into \"back in the day\" stories.  :-)"
    author: "Scott Wheeler"
  - subject: "Re: kasbar and xmms"
    date: 2000-11-15
    body: "Don't worry, it works fine - you get the full menu.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "the desktop panel"
    date: 2000-11-15
    body: "right now they display only position and size of the windows and mark the focused window. I wonder what thumbnails would look like in there. Maybe too small?"
    author: "Wolfgang Rohdewald"
  - subject: "Re: the desktop panel"
    date: 2000-11-15
    body: "The thumbnail would be external. I've put a mockup of what I mean on my web page, there are also a few more screenshots of Kasbar in different configurations. Take a look at <a href=\"http://www.ipso-facto.demon.co.uk/kde/kasbar-tng.html\">http://www.ipso-facto.demon.co.uk/kde/kasbar-tng.html</a>.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: the desktop panel"
    date: 2000-11-15
    body: "E's iconbox has this feature. It's actually clear enough to recognice the apps, and it's quite useful."
    author: "Rikard Anglerud"
  - subject: "Re: the desktop panel"
    date: 2000-11-15
    body: "E's iconbox has this feature. It's actually clear enough to recognice the apps, and it's quite useful. However, it doesn't seem to be what he ment by the thumbnails."
    author: "Rikard Anglerud"
  - subject: "Re: the desktop panel"
    date: 2000-11-15
    body: "E's iconbox has this feature. It's actually clear enough to recognice the apps, and it's quite useful. However, it doesn't seem to be what he ment by the thumbnails."
    author: "Rikard Anglerud"
  - subject: "The thumbnail..."
    date: 2000-11-15
    body: "The thumbnail can be a very good way of finding a window in a busy desktop.<p>\n\n&lt;Day Dream&gt;<p>\n\nA small thumbnail can render it useless but a big one can take too much space...<br>\nHow about a zooming mechanism? Can it be easily done?<p>\n\n&lt;Day Dream&gt;<p>\n\nThanks"
    author: "Gon\u00e7alo"
  - subject: "Re: The thumbnail..."
    date: 2000-11-16
    body: "<p>Something like the \"Genie\" effect of MacOS X ?<br>\nIt ought to be optionnal if it were to exist. For performance reasons...</p>"
    author: "GeZ"
  - subject: "Re: The thumbnail..."
    date: 2000-11-16
    body: "Look at the way Enlightenment's pager works.  the thumbnail snapshot gets updated in near-realtime and when you hover the pointer over it, it pops up (similar to a tooltip) a zoomed-in version of the snapshot.  Really cool!"
    author: "me"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "Is it possible to download it?"
    author: "Zeljko Vukman"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "It is now - I committed my latest code to the CVS last night. The configuration dialog is still missing, but the most of the features are working.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "Thanks Richard,\n\nKasbar is really great. I'm looking forward to\nnext releases.\nGreat work. BTW, the new releases of KDE2 has\nimproved stability & speed. \n\nRegards,"
    author: "Zeljko Vukman"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-16
    body: "Sorry to ask,\nwhat release are you referring to?\nIs there new release of KDE2 ?\nI have KDE2 released in Mandrake 7.2. It's working great, but I notice there are some annoying bugs such as crash during changing background image."
    author: "jamal"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-16
    body: "Thats not a bug in KDE. Changing wallpapers works fine. Perhaps your libpng is broken? I've heard that broken libpng has caused a great deal of crashes and downgrading to previous version has helped a lot.\n\nAnyway, he's probably referring to cvs-snapshots, i've compiled few packages from snapshots and those are generally working quite well.\n\n-- \n    Janne Kylli\u00f6 at V e l t o x . o r g"
    author: "jannek"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-17
    body: "Yes, I'm refering to the snapshots (or anoncvs). The code is very new, and relies on the extension API which was not present in 2.0. You'll either have to build it yourself or wait for 2.1. Note that 2.1 is not too far away.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-17
    body: "There are new packages of KDE2 for Mandrake 7.2\nwith Kasbar and External taskbar etc. They can be downloaded here:\n\nftp://nebsllc.com/pub/KDE_UPDATE\n\nRegards,"
    author: "Zeljko Vukman"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "When I discovered kasbar in one of the beta releases, I was really impressed and wanted it as a replacement for the standard taskbar. Sadly I found out, that it did not have enough features. Reading about your great work makes me really looking forward to a new release of it. Your ideas are damn kewl :)"
    author: "kR0ll"
  - subject: "Mosfet Rules"
    date: 2000-11-15
    body: "<I>Kasbar was originally written by Mosfet as a kicker applet</I><br><br>\nMosfet need to update his pages more often."
    author: "Kde user"
  - subject: "Re: Mosfet Rules"
    date: 2000-11-15
    body: "One of the main advertised purposes of mosfet.org was to provide \"development news and information for the NEXT generation of KDE, KDE2.0\". Now that KDE2 is here and this news page is up, the need for Mosfet to keep his page updated is probably less now.\n\nAlthough if he wants to keep his reputation as KDE's \"Rasterman\" he needs to figure out a new angle quick. Richard Moore and Rik Hemsley are going to upstage him in the \"cool, man\" dept.:-). \n\nMaybe he's coding in secret, preparing the Next Big Cool Thing (tm) to spring upon us at just the right time. More likely, he just still has a hangover from his 26th birthday:-)\n\nSamawi"
    author: "Samawi"
  - subject: "Re: Mosfet Rules"
    date: 2000-11-15
    body: "I agree.\nCome on Mosfet, post something, your fans are waiting!"
    author: "reihal"
  - subject: "Re: Mosfet Rules"
    date: 2000-11-16
    body: "Amen to that.  I was looking forward to the\nnew \"diary\" format of www.mosfet.org, but it's been quite a while since an update.  Reading developer diaries sure beats press releases.  Of course, there's always mailing lists to keep up with mosfet's work :)\n\nAdam"
    author: "Adam"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "This question may or may not pertain to kasbar. I just would like to know if there is a mechanism in kde2 that can notify you of a window changing via kasbar or the taskbar. \n\nExample: In windows I have an IRC client connected and minimized. When some one says something the button on the taskbar flashes.\n\nIs this a feature of the App, Kde2, or Kasbar/taskbar?\n\nIf it is Kasbar/taskbar it would be nice if you implemented it.\n\nIt is so annoying to have ksirc minimized and not be able to know when anything goes on in the channel without maximizing it."
    author: "Sigil"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "Kwrite shows changed files by placing an additional disk icon in the taskbar. Maybe ksirc could use the same mechanism?"
    author: "Maarten ter Huurne"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "That's actually supposed to be a standard feature of KDE 2 apps. It was added fairly late though, so it's done by everything yet. It is done via the ENT protocol, so it can be supported by other WMs etc. It is definitely on my TODO list for Kasbar (though the visual appearance will be different). If KSirc can use it too then that's great.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "Hi!\n\nSomething I would really like to have is the following (whereby I'm not sure what exactly Kicker, Kasbar, Taskbar and so on are):\n\nI have a desktop preview in the taskbar, which is really cool. But I often want to move a window onto another desktop. I need to switch to that desktop, rmb on the taskbar entry, and send it to the new desktop. Or, make the window sticky, move to the new desktop and unstickyfy it again. This sucks. What I would like is to be able to drag a window from one desktop to another within the preview."
    author: "Anonymous Coward"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "The pager applet is responsible for that. You can send in your wish using the bug report system. Use \"Application: minipagerapplet\" and \"Severity: wishlist\"."
    author: "Maarten ter Huurne"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "> What I would like is to be able to drag a window from one desktop to another within the preview.\n\"kpager\" does exactly what you want. (kpager in KDE2 is much more stable than the old version.) One bad thing: kpager needs some space. Sure, the minipager should also support window draging..."
    author: "Dirk Manske"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-15
    body: "You can already do this, just right-click on the\ntitlebar and select \"To Desktop\"."
    author: "David Simon"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-16
    body: "Here's a suggestion, something that I have disliked about the taskbar ever since KDE 1.  It doesn't really show the minimised state of a window very clearly - the (parentheses) around the name don't stand out very well, and also are a bit \"technical\"ish.  It would be better if the state was shown in some other way (different color, font, etc) - fully configurable of course!\n\nMy preferred configuration would be:\n\n the window with focus:  button depressed, bold text\n\n other open windows: normal text\n\n minimised windows: grey (half-bright) text\n\nbut other people of course may have other ideas..."
    author: "Jonathan Marten"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-16
    body: "Hallo everybody!\n\nI'd realy like to use this Program, because I used the verion that was given to me with the beta version of KDE and I can't life without it anymore! ;-)\n\nBut now I've installed the Final release of KDE 2 and was shockt because there was no kasbar anymore :-(\n\nI now tried to find the new version of kasbar but I didn't find it!\n\nCan anybody tell me where I can find a link to a download verion of kasbar (and, what would be very very important, a good howto for the installation, because I'm a stupid newbi ;-)\n\nWith the very best reguards\nSven"
    author: "Sven Kempf"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-16
    body: "I'd like a feature where one can drag any running app from kasbar/taskbar to the desktop / menu-editor and instantly having a shortcut this way. That would be very cool, I think."
    author: "Klas Kalass"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-16
    body: "A neat option would be to show tasks which are NOT on the current desktop as ICONS only, so that they are less important than the tasks on the current desktop."
    author: "Martin Fick"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-17
    body: "I don't understand - surely this is what it already does? or do you mean icons with no title?\n<p>\nI've recently (yesterday) added the option to limit the display to windows on the current desktop, but it is only an option (and is off by default).\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-17
    body: "Sorry, I didn't look at your screenshots until after I posted that message.  When I did, I realized that kasbar seems different than the regular KDE 2 taskbar; I can't tell from your shots whether my suggestion will still apply.  \n\nI am particularly talking about when a taskbar is used in horizontal mode (all the screenshots are vertical).  Does kasbar do horizontal like the normal taskbar does?\n\nWith tho normal taskbar, applications are displayed with an icon on the left and some text on the right.  When you have a lot of applications, the text for each app is reduced drastically and is no longer very usefull.  I imagine that this is the reason that the option to only show the apps on the current desktop is offered.\n\nI feel that a \"nice\" cross between showing only the current desktop's apps and all the apps (in horizontal mode), would be to show the current desktop's apps along with some text while only displaying the icons of the other apps.\n\nTo help illustrate my idea, run the KDE2 normal taskbar with a bunch of apps and drag it on the panel.  While dragging, it will shrink all\nthe apps so that mainly the icon is showing.  In my scheme, this is kind of what non current desktop apps would look like."
    author: "Martin Fick"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-16
    body: "How about allowing the location of applications on the taskbar to be moved around by dragging and dropping.  Not the location of the apps themselves, just their order within their desktop section of the taskbar."
    author: "Martin Fick"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-17
    body: "That's not a bad idea. It's difficult to see how it should work though - what happens if you quit and restart an app for example. I'll think about it, but don't expect to see it any time soon.\n<p>\nIf you have a clearer idea than I do of how this should work then let me know.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-17
    body: "I wasn't thinking about a permanent thing for an apllication, it should simply be for running apps.  I don't always start apps in the order that I wouls like them in the taskbar, therefor rearranging the current running apps would be helpful.  If you quit an app and restart it, it is a new app and should act as it always does, go to the end of the list (so to speak).  The taskbar should not remember where it was."
    author: "Martin Fick"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-16
    body: "Please include \"Minimize all windows\" or \"Iconify all windows\" as one of the options when you right-click on the Kasbar itself.\n\nLook forward to using the new and improved Kasbar :-)"
    author: "Thorarinn"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-17
    body: "That's tricky because the window menu is generated by kwin rather than kasbar (see the original article). I did however think that an optional half-width icon offering this might be useful. Would that do?\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-17
    body: "Will there ever be a way to get the KDE2 taskbar laid out exactly the same way it can be laid out in KDE 1.1.2? Right now, I have found that having the taskbar accross the top of the screen and the toolbar down the left is the most convenient. But, I've found no way to duplicate this layout in KDE2 and won't switch until I do."
    author: "Andrew"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-17
    body: "It's in the latest CVS implemented using the same panel extension system used by Kasbar. It is not part of the 2.0 release. It will be available in 2.1\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-18
    body: "I have a couple of friends who have the exact same sentiments about the integrated panel and taskbar we now call \"kicker.\"  They do not want to switch to KDE2 until they can have their old layout back with a separated taskbar.  I personally like the integration but I've come across many that don't.  Someone said that KDE2.1 remedies this problem.  When is KDE2.1 due out?"
    author: "erotus"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-17
    body: "This is GAY!!!  KDE suX and WinD0z3 ru1Z!"
    author: "Scotty"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-17
    body: "Please consider some visual clues that shows on wich destop an app is running. (ala KDE 1.1.2). Also, bring back the \"onto current desktop\" feature if possible. I miss thoses 2 features badly."
    author: "J-C Dumas"
  - subject: "Improved window tile management?"
    date: 2000-11-18
    body: "Here's a thought that I don't think any desktop has implemented well. <BR><BR> How about a feature where you can tile a <I>specific</I> number of apps either vertically, horizontally, or equally.<BR><BR>\n\nIn Windows, when you right click on the taskbar you have the ability to apply those tiling commands to ALL the running apps. (Hardly ever used, if at all)<BR><BR>\nWhat I believe would be better is an implementation such as this:<BR>\nRight click on Kaskbar and we have submenus (along with the other menu items) titled Vertical, Horizontal, and Equal tiling.<BR><BR>\nWithin those sub menus you have labels saying \"Choose 2, 3, \"etc, up to the number of apps on the Kasbar (this number shouldn't grow too big)<BR><BR>\nAfter issuing a command like that, you simply click on the names of the apps in the kasbar of those apps you want tiled. (similar in feel to the kill command for X windows)<BR><BR>\nIf the idea seems stupid, don't flame me.  I'd love to have a feature like that and I am not aware of any implementation of the same kind.  There's many times when I wanted to tile vertically one specific web page and a document to fill up the view of my desktop without having to minimizing the other 5 running web browsers blocking the way."
    author: "Melvin Quintos"
  - subject: "Re: Improved window tile management?"
    date: 2004-02-14
    body: "I believe that tiling windows in windows XP works on the apps with opoen windows only. the other running aps are left untiled if they are minimised when you use the tile option.\n"
    author: "AltKey"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2000-11-18
    body: "what about skipping windows?\ni mean if i have started xmms -for example- i do not want to see its button in the taskbar since i have gkrellm to control it. kstart seems to have a parameter --skiptaskbar but it does not seem to work for me. this is a feature i'd really like to see. or am i to stupid to find this functionality in kde2!\nsorry for my bad english."
    author: "anonymous"
  - subject: "Re: Kasbar - The Next Generation"
    date: 2001-02-07
    body: "I was messing around with enlightenment for a while and found the ICON BOX very useful. A feature like snap-shotting the windows in the moment of minimizing is very usefull and not just a graphical gimmick. If you are doing so please check if there is a chance to define the size of Kasbar (maybe drag it bigger) thus also the size of the snapshotted windows-\"icons\". If they are too small one can not recognize the application on display with high resolutions ...\n\nKasbar is great! Keep up your work!"
    author: "Michael H. Raus"
---
I'm in the process of improving the <a href="http://dot.kde.org/974226730/kasbar-transparent.png">Kasbar</a> taskbar replacement, and as there 
  seems to be a fair degree of interest I thought I'd outline what I've done so 
  far and my plans. My first aim (which I have nearly achieved) was to ensure 
  that Kasbar had all the features of the standard taskbar, but I have quite a 
  bit more in mind...

<!--break-->
<p>Kasbar was originally written by Mosfet as a kicker applet, and has always 
  had a pretty cool look. Shortly before the release of 2.0 support for external 
  applets was removed because the API sucked, which unfortunately meant that Kasbar 
  would no longer work. Mathias Elter's recent work adding a panel extension system 
  to kicker meant that the time was ripe for Kasbar to return, so over the weekend 
  I ported the code to the new API.</p>
<p>My first port was just that: a direct port of the original Kasbar applet code, 
  but once I had it working properly I began to add some new features. So far 
  I've added the following:</p>
<ul>
  <li>Improved left mouse click behaviour<br>
    <i><font size="-1">If you click on the icon for the active window then Kasbar 
    will now minimise it, clicking on inactive or minimised windows activates 
    them as before. The new behaviour is consistent with the standard taskbar.</font></i></li>
  <li>Window menu<br>
    <i><font size="-1">You can now access the window menu by right-clicking on 
    the windows icon. This is done by sending a DCOP message to KWin, so you get 
    the complete set of options unlike the standard taskbar which only offers 
    a subset of the available actions.</font></i></li>
  <li>Transparency<br>
    <font size="-1"><i>I like the current Kasbar look, but I thought that a transparent 
    Kasbar would look <b>very</b> cool. Adding this option to Kasbar was trivial 
    because kdelibs has a KRootPixmap class which does all the hard work. In addition 
    to simple transparency you also have the option to tint the background to 
    a specified colour. The code for this is working, but won't be committed to 
    the cvs for a few days because I have some more work to do to make it configurable.</i></font></li>
</ul>
<p>Of course, I have some more features planned, I expect to have the following 
  working in the near future:</p>
<ul>
  <li>Shade indicator<br>
    <font size="-1"><i>This should indicate that a window is shaded in a way that 
    is consistent with the existing icons for minimised and normal windows. I 
    have wirtten the code for this, but unfortunately this indicator does not 
    work for some reason.</i></font></li>
  <li>Startup notification<br>
    <font size="-1"><i>Kasbar should support startup notification in the same 
    way as the standard taskbar. I've already started making changes to the implementation 
    to support this, but there is still more work to be done.</i></font></li>
  <li>Tooltips<br>
    <i><font size="-1">I plan to add a tooltip displaying the complete window 
    title. At the moment you can only see a small part of the title and the only 
    way to see the rest is to look at the window itself.</font></i></li>
  <li>Window thumbnails<br>
    <font size="-1"><i>I'd like to add a new feature offering thumbnails of minimised 
    windows. The idea would be to grab the image of the window from the screen 
    immediately before it is minimised then display a thumbnail of the image in 
    some tooltip-like way. There are some issues with this but I think it would 
    be pretty cool. It would of course be optional.</i></font></li>
</ul>
<p>I'm very interested to hear what people think of my plans, so I'll be monitoring 
  the comments posted here. If you have a feature request for Kasbar then now 
  is the time to let me know.</p>
