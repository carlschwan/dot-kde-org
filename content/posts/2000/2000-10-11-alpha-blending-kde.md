---
title: "Alpha Blending for KDE?"
date:    2000-10-11
authors:
  - "numanee"
slug:    alpha-blending-kde
comments:
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-10
    body: "That looks really cool. The KDE 1.X series wasn't very visually attractive. KDE 2 is so much better and keeps getting better all the time. Totally awsome stuff, developers keep it up."
    author: "tweaker"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "<a href=\"ftp://ftp.kde.org/pub/kde/Incoming/gnome.png\">ftp://ftp.kde.org/pub/kde/Incoming/gnome.png</a>"
    author: "ac"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "<i>\nftp://ftp.kde.org/pub/kde/Incoming/gnome.png</i><p>\nIs this supposed to illustrate something?  I can't see any Alpha blending in this screenshot anywhere; certainly not in the wharf/menu area.<p>\nAm I just missing the point?<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "Look left in the folder icon, and above in the K of the KDE-icon. It is certainly blending, my friend."
    author: "David"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "Doubtful.  Only mentioned to illustrate how far and flexible KDE has come."
    author: "ac"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "<i>Look left in the folder icon, and above in the K of the KDE-icon. It is certainly blending, my friend.</i><p>\nIn the original one listed in the article, yes.  However, if you look at my message, I was talking about the one that was posted here: ftp://ftp.kde.org/pub/kde/Incoming/gnome.png.  The K menu does not look at all like it is blending, and the shadow on the KWrite icon is black and harshly aliased.<p>\n--<BR>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-12
    body: "You could look in the background, maybe"
    author: "Aubanel"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-10
    body: "Very nice...  its neat little stuff like that that puts KDE on top.  KDE 2.0 is visually looking very, very impressive.  At this rate, you can put MS to shame (if you haven't already) by 2.1 or 2.2.  Any chance in this making it into 2.0, or is it definitely waiting till 2.1?  It would be a nice last minute addition if you ask me...."
    author: "dingodonkey"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "It's way too late for 2.0. In addition as the mail says it needs the icons to use the alpha channel in order for it to be useful.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "there was always hopin' :)\n\nRegardless, 2.0 is lookin good."
    author: "dingodonkey"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-12
    body: "> In addition as the mail says it needs the icons to use the alpha channel in order for it to be useful.\n\nThey already support the alpha-channel.\n\nTackat"
    author: "Tackat"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "How about making the background of Kicker transparent, showing the desktop background?"
    author: "Peter Duus"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "How about using it in konsole!!?"
    author: "David"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "hmm....  i love that idea\n\nbeing able to select a pic to use (and adjustable transparency would be GREAT) as the background on the konsole would be good....  or even just transparent flat dull colors\n\nwho knows"
    author: "dingodonkey"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "Maybe TrollTech should incorporate this into QT?\n\nMaybe I'm stupid(I have no experience with graphical programming), but what makes it \"impossible\" to implement alpha-channel support in the X11-version of QT when you can do stuff like Rik(and Rasterman) did anyway? I know X doesn't have an alpha-channel, but how is alpha-in-icons possible then? Think of it - if we could have transparent windows(at least those made with QT) that would be very cool:-)\nIf someone could explain this, I would be - Enlightened:-)\n\n--\nAndreas"
    author: "Andreas Joseph Krogh"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-12
    body: "X11 gives you access to your window of memory for your application, which is an RGB surface(sometimes greyscale or palletized).  You can put whatever colors you want in that window, but there is *NO* support for alpha transparency.  However, if you blend two pictures(icons or whatever) and then put it in the window that is just fine. That is what he has done.  Since X11 doesn't give you alpha transparency in the actual window, you cant make a see through window(Right now you are saying that I am full of crap because Eterm does).  Some programs use a hack to do this by getting the actual background image(I'm not sure how they go about this), and blend it with their window contents which make their window look transparent.\n\nI am not much of an X programmer so whatever I said here take with a grain of salt."
    author: "Matt Newell"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-13
    body: "IIRC, Konsole can do the transparent background thing too, but one can tell that it isn't real transparency; try moving the Konsole so that it partially overlaps another window. The other window won't show through (at least it didn't the last time I had the transparency turned on)."
    author: "jliechty"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-16
    body: "The reason it isn't currently possible (without hacking on the X server) is because of the fundamental way windowing systems work.  When a window is obscured, the window that is obscured doesn't get asked to repaint its contents.  This saves a huge amount of time as you are only drawing what is required.  The problem with alpha blending is that the X server would have to understand that if the obscuring window was using alpha blending that the obscured windows should be sent repaint events anyway.  There are a lot of design assumptions in X about obscured windows like in this example.  Getting true alpha blending to work is not a problem at the Qt/KDE level in other words, it needs to be addressed at the X server level to be done correctly and efficiently.  It would make for an interesting project. :)"
    author: "Steven Brown"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "I think the term 'sexy' springs to mind. Too bad it won't make it into the release =("
    author: "MichaelM"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "Ok this post is a little off topic but... Ok, its practically impossible to buy a PC graphics card these days 'without' bit-bliting alpha blending etc.  So much of KDE/Qt could be made to fly if there was a good was to hit the hardware.  3D is a superset of 2D, OpenGL is a mature HW accelerated standard with all the functionality a 2D GUI could ever need + more.  Hardware cards can accelerate even things like blended bitmap scaling.  Just the straight forward X approach seems non-sensical these days.  I dont know what the best engineering answer is and I not bashing X, just this standard is showing its age now.  I realise you guys are working within the limitations of X and doing a very good job.  Sorry thats my little rant over and i'm sure its all been said before."
    author: "N8"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "I guess embedded Qt will make this possible. Using the frame buffer without X. Am I right?"
    author: "reihal"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-11
    body: "X is getting an alpha channel too... the RENDER extension. Debian is including it in their builds of X4, presumably it will go in wholesale soon."
    author: "Kevin Puetz"
  - subject: "Re: Alpha Blending for KDE?"
    date: 2000-10-19
    body: "a little bit off topic but ...<BR>\n<BR>\nA very interesting project for a future X replacement is <A HREF=\"http://berlin.sourceforge.net\">berlin</A>.<BR>\n<BR>\nit's far from being usable though and there is no QT for it: -("
    author: "Andreas Erhart"
  - subject: "FYI"
    date: 2000-10-11
    body: "http://www.xfree86.org/~keithp/render/\n\nIt's coming for X as well.. :)"
    author: "AC"
  - subject: "Re: FYI"
    date: 2000-10-11
    body: "Can that be accelerated by an OpenGL/GLX backend ?"
    author: "AC"
  - subject: "Re: FYI"
    date: 2000-10-11
    body: "no.\n\nit is(or at least can be) accelerated in hardware, but in my understanding of how it works, OpenGL is entirely unsuitable because it doesn't guarentee you an exact rendering."
    author: "Kevin Puetz"
  - subject: "Aqua, anyone?"
    date: 2000-10-12
    body: "<p>I'm sure you've all seen screenshots (or had the pleasure to use) of Mac OS X. It still has a lot of work to go, but if there's any one word that describes it, that's \"beautiful.\" One of the major reasons for this is the way everything is alpha-blended, transparent, etc, etc. Thanks to this idea, now KDE2 can leap beyond other X Window System apps, and approach this new ideal. Excellent!</p>\n\n<p>By the way, I ported Qt-2.2.0 to Darwin, but KDE still won't compile. I guess Stan Shebs (Apple's compiler guy) still hasn't fixed that C++ bug... oh well</p>"
    author: "Robert Thompson"
---
From <a href="http://lists.kde.org/?l=kde-core-devel&m=97114710116004&w=2">Rik Hemsley</a>: <i>"I hacked KIconLoader to allow me to get a QImage instead of a QPixmap, which was quite easy, because it uses QImage internally and only converts to QPixmap just before returning a processed pic. I also wrote a <b>very fast</b> blend function that takes 2 QImages and blends the first over the second, honouring the alpha channel. I couldn't find anything in Qt or KDE to do this, so I had to hand-roll it. In tests on my Celeron 600 it can blend 65 million pixels (about 1/2 of which have alpha != 0) in 5 seconds. That's about 5ms for a 256x256 image, so approximately 0.17ms for our largest icons (48x48). <a href="http://lists.kde.org/?l=kde-core-devel&m=97114710116004&w=2">[...]</a> Anyway, enough talk. I'm tired and I'm sure a <a href="http://www.geoid.clara.net/kicker_alpha_test.png">screenshot</a> is in order. ;) Apologies for the hideous background tile, but I needed something to illustrate the point."</i> Good stuff for KDE 2.1.

<!--break-->
