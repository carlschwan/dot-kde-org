---
title: "Advances in Alpha-Blending"
date:    2000-12-01
authors:
  - "Dre"
slug:    advances-alpha-blending
comments:
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "My word! That's absolutely gorgeous!<p>\nOff to hit the CVS, who's coming?"
    author: "MichaelM"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "Sheesh, I've been there for months.\n\nI wish that they would make fully alpha blended windows and pixmaps a standard part of X. This would be very nice."
    author: "Matt Newell"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "I see no need for this feature - except making KDE's code slower and bigger. Maybe not this one feature alone, but as we say in germany: \"Kleinvieh macht auch Mist\""
    author: "Johann Lermer"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "<i>\"Kleinvieh macht auch Mist\"</i><p>Or, as babelfish says:\n<p>\n\"Flock makes also muck\"<p>\nCould be get a better translation?  (I'm guessing KISS... Keep It Simple [Silly|Stupid])  And if it can be turned off, it's great.  PDAs are already pushing 300 Mhz, and normal office machines are often above 500 Mhz machines.  As long as it can be turned off for those with lesser machines (of course, my Mom uses a 386sx with 8 MB - there *is* a limit) I'd rather have a DE that provides useful, pleasant visual cues.<p>\nNow if only those .htm files were rendered thumbnails... \n<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "<I>\"Kleinvieh macht auch Mist\"</I><BR>\nIn my Dictionary it says:<BR>\n<I>\"Manny a mickle makes a muckle\"</I><P>\nSounds much better to me :-)"
    author: "Worf"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-04
    body: "From My understanding, \"Kleinvieh macht auch Mist\" means\n\"Small farm animals poo as well as other things\"\n\n<a href=\"http://www.crosswinds.net/~cucurucho\">El Cucurucho</a>"
    author: "El Cucurucho"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "You're right, there is no real *need* for this in\nKDE. It is not essential. In fact, none of the eye-candy is essential. Frankly, the whole *G*\nin GUI is non-essential. One can build very\nusable UIs text-only. On the other hand, why\nnot take advantage of the power modern computers\ncome with? Beauty might not be essential - but\nstill is beautiful. ;-)"
    author: "Uwe"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "Hi Uwe,\n\nI wouldn't go so far and say GUIs are useless. In fact, they can make work faster and easier than any text based interface (well, not always, that's why we still have the console...). But to me it seems, that KDE is so nice and has everything it needs, that now the programmers add features just for the featuers' sake (some people call it 'eyecandy' - I say: candys are bad for your teeth!). And we all know to where it leads: just have a look at MS Office! \nAh, and talking about modern computers: I confess, I have an old, outdated and slow machine from the ancient times when the hobbbits.. okok, it's 5 years old, just recently reinforced by a 400MHz K6 and a new harddrive, and I simply do not want to spend my money for a new 'Deep Thought' just to make KDE working - I need all I earn to buy diapers for my little son."
    author: "Johann Lermer"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "That's why the alpha blending is disabled by default. It will only \"waste\" CPU cicles when you enable it manually . Currently, you have to edit your $KDEHOME/share/config/kdeglobals file and add alphaBlending=true to the [General] section, but  there'll be (of course) a way to configure it from KControl."
    author: "Antonio Larrosa"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "That is not partly true.\n\nWhile it will probably take half an extra second to render 100 of those icons. The new Render extension for XFree86 that will probably be in next version will do Alpha blending stuff in hardware (http://xfree86.org/~keithp/render/) Then\nall X will have it and Qt/KDE too.\n\nSince it will move the rendering code from software directly to your graphic card. You will not have any penelty for doing this stuff.\n(Some newer cards like the ATI Radeon even do alpha cursor (seen in Win2000) in hardware.)\n\n\n/Sam"
    author: "Sam"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "Speaking of the Render extension, does anyone know what progress has been made with it? I've been checking the URL mentioned, but it hasn't changed - not surprising if this is just one person doing it. Maybe Shawn needs to start the Xompany..."
    author: "Simon"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "<i>\"The two big pieces remaining in the extension are polygons and image transformations\"</i>\n\nSome drivers (I believe Tseng, S3 Virge, Matrox,..) have the render extension done (or partly done).\n\nI believe I read somewhere that there was a 40 factor speed up between hw & sw rendering :)"
    author: "Sam"
  - subject: "No Need for this? (was:Advances in Alpha-Blending)"
    date: 2000-12-01
    body: "<p>On Friday December 01, @12:34AM, Johann Lermer wrote:</p>\n<p>\n> I see no need for this feature - except making<br>\n> KDE's code slower and bigger.\n</p>\n<p>You are absolutely right!</p>\n<p>It is a waste of both time and disk space!</p>\n<p>But why stop here?</p>\n<p><b>Think further, Johann! </b> Imagine what you could save by reducing the system even more:</p>\n<ul>\n<li><p><b>No Icons at all!</b><br>\nPlain Text is fairly enough. It is understandable and fast.</p>\n<li><p><b>No time-consuming and difficult Mouse handling!</b><br>\nJust hit one of those fabulous [Fnn] keys that can easily be combined with [Shift], [Ctrl], [Alt] and/or <u>combinations</u> of those.</p>\n<li><p><b>No graphical User Interface!</b><br>\nYour goals can be achieves faster by using the shell, right?</p>\n<li><p><b>NO COMPUTER!</b>\nWhy not use your good, rock-solid TYPE WRITER.<br>\nIt was good enough for your father, why isn't it good enough for you?</p></ul>\n<p>Ok, let us stop kidding. :-)</p>\n<p>In my opinion this nice feature in not only <i>nice</i> (something that might be worth thinking about, since all of us happen to be <i>human</i> beings - not just 'efficiency machines') but I am convinced it will also be a very usefull feature quite soon.</p>\n<p>This is not the first time that a feature is added because people just <i>like</i> to have it and soon afterwards the manifold ways how to benefit by using this feature become clear...</p>\n<p>I am sure, Alpha-blending is absolutely worth being happy about!</p>\n<p>Kindest Regards,<br>\nKarl-Heinz Zimmer - http://home.snafu.de/khz<br>\n-- <br>\n\"Why do we have to hide from the police, Daddy?\" \"Because we use vi, son. They use emacs.\"<br>\nDave Fischer, 1995/06/19<BR></p>"
    author: "Karl-Heinz Zimmer"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "Yes, you are right. There is no need to make the box of your computer transparent with nice colors and sell them and get real rich either. You could just have a boring one."
    author: "Lenny"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "I can see this being extreemly useful for coders like me. Because you can see the first few lines of text of each file, you can quickly identify a files purpose, especially in large code bases with 100 or more .cpp files in each dir, some with simmilar names. Although this can be alievated by paying careful attention to file names, GUIs are supposed to make work easier, and the names of the files certainly dont affect the resulting binary."
    author: "Carbon"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-04
    body: "IMHO, all these extra features should be modular options.\n\nMy idea is this: To make sure that KDE doesn't bloat up to much (memory wise) and freak out all our processing power while creating one simple alpha blend, all unnessesary features (\"GUI enhancements\") are to be in modules that can be dynamicly loaded and unloaded, so that unused features don't take up memory.\nAll of these extras should of course be as optimized as possible... I suggest carrying on the UI stuff, making it look _REALY_ cool (and by golly, It allready is to cool to be true!), but have all the coolness detachable from the main system so that people on pentiums can still live.\n\nGOOD WORK! Viva KDE ;)"
    author: "Sm\u00e1ri P. McCarthy"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-05
    body: "I totally disagree with you.  I've been waiting for this for a long time.\n\nJohn"
    author: "John"
  - subject: "Blending disabled icons"
    date: 2000-12-01
    body: "Will disabled icons be alpha-blended with the background to get a nicer semi-transparent effect? Currently, semi-transparency is implemented by displaying half the pixels, but displaying at half alpha would look much better. Take a look at the disabled icons in the toolbar of Konqueror in the screenshot."
    author: "Maarten ter Huurne"
  - subject: "Re: Blending disabled icons"
    date: 2000-12-02
    body: "A nice idea, but an interesting addition would be to remove the colour and reduce the contrast as well, making the effect much more obvious.\n\nAdam Foster"
    author: "Adam Foster"
  - subject: "Re: Blending disabled icons"
    date: 2000-12-03
    body: "KDE2 is already able to remove color from disabled icons (\"to gray\" and \"desaturate\" effects), it's in the control center under \"Look & Feel / Icons / Effects\". Decreasing contrast is a good idea, which can probably be implemented without too much trouble.\n\nI tried using \"to gray\", which works well for colorful icons, but obviously fails for icons that already contain mostly gray."
    author: "Maarten ter Huurne"
  - subject: "Text file preview."
    date: 2000-12-01
    body: "The whole text-file thumbnail preview thing has a certain gee-whiz coolness to it, but I gotta say I think it is a step backwards from a UI standpoint.\n\nAt first glance, all of the text files look alike - I can't distinguish between .txt, .cpp, .c , .h etc. The text included is completely useless unless I study each one close enough to identify the language (perl, C++, whatever). In the screenshot above, the original icons are still there, but now they are semi-transparent(looks very cool btw) and I can't really see them unless I look hard.\n\nI think I'll stick with my boring icons that give strong immediate cues about what the file types are."
    author: "JC"
  - subject: "Re: Text file preview."
    date: 2000-12-01
    body: "I sort of agree with you that it \n  * Probably takes too much time to render\n  * Is \"a step backwards from a UI standpoint\"\n\nBut it is cool anyway. What I would like to see instead is a thumbnail popping up when you have the mouse over the filename. Then you can have the detailed file list and, lets say, a special column icon that represents a preview. If you have the mouse over there you will get this alpha blended preview...\n\nComments"
    author: "Sam"
  - subject: "Re: Text file preview."
    date: 2000-12-01
    body: "It works so fast that we thought about generating it on the fly (which might be faster) than using cached versions of the generated image. Is that fast enough?\nYour suggestion is almost useless though. The advantage of the preview is that you can recognize the document *without* having to click anywhere (Doing this on mouseover would be extremly annoying given the number of files you cross \"accidently\" - so it's not a viable option either). Therefore your eyes can scan a bit more than the usual information very fast while scrolling through the directory.\nI see no real advantage of your proposal as I could also click on the document, view it, and click back from the embedded view in almost the same amount of time. Also your method would destroy the layout.\n\nGreetings,\nTackat"
    author: "Torsten Rahn"
  - subject: "Re: Text file preview."
    date: 2000-12-01
    body: "Your suggestion is especially usefull\nif you replace your mouse with a grafix tableau.\nAccidentially popping views then become less annoying 'cause of the direct pointing character of those devices:\nSimply hover over that icon, and after a (configurable) timeout that transparent preview appears. Removing the pen collapses the preview.\nSimply as is.\n\nJens"
    author: "Jens"
  - subject: "Re: Text file preview."
    date: 2000-12-01
    body: "As you mentioned correctly you have to wait for the timeouts .. and that is a waste of time.\nThe current solution is simply better: you see everything at once.If I need to wait for timeouts or if I need to click to look explicitely at the document then all the advantage of the whole thing is lost!\nImagine I would want to search for a document and forgot the filename. With your method you'll happily move over every single icon, wait for the preview to pop up and move further until you moved over every single stupid icon. With the current method you only have to scan with your eyes over through the dirs -- no hands ..."
    author: "Torsten Rahn"
  - subject: "Re: Text file preview."
    date: 2000-12-01
    body: "As you mentioned correctly you have to wait for the timeouts .. and that is a waste of time.\nThe current solution is simply better: you see everything at once.If I need to wait for timeouts or if I need to click to look explicitely at the document then all the advantage of the whole thing is lost!\nImagine I would want to search for a document and forgot the filename. With your method you'll happily move over every single icon, wait for the preview to pop up and move further until you moved over every single stupid icon. With the current method you only have to scan with your eyes through the dirs -- no hands ..."
    author: "Torsten Rahn"
  - subject: "Re: Text file preview."
    date: 2000-12-01
    body: "Ah, you got me wrong here; sure, to want a preview in general and therefore to wait for the timeout is silly. \nBut as an intermediate between no preview at all and total preview, which reduces the amount of files per screen (due to its space requirements) a tooltip-like preview as a hint to the lost user seems very reasonable to me.\nHowever, all pros and cons of tooltips take place here.\n\nCiao,\nJens"
    author: "Jens"
  - subject: "Re: Text file preview."
    date: 2000-12-02
    body: "I sympathize with the view that the current implementation obscures the mimetype icon, but prefer that over a pop-up.  OTOH when you are in preview mode, the mimetype gets obscured (well, completely replaced) for images as well.  Perhaps one slight improvement would be to make the mimetype icon in the text-preview mode stronger (i.e., emphasize it more).  That would block some of the text more, but the mimetype icon is smaller than the text preview window.  Also, one could switch to the smallest mimetype icon (22x22?) to get less of the blocking.\n"
    author: "Dre"
  - subject: "Re: Text file preview."
    date: 2000-12-02
    body: "In Windows 98 file manager there is view with two tabs: in one there are icons and when you point to one of the icons with mouse then in second tab you will see file type, date, size etc. there. We could have same and also this there."
    author: "Rivo Laks"
  - subject: "Re: Text file preview."
    date: 2000-12-05
    body: "You basically already have this since you can split the screen and have different views of the same directory in two different panes.  I'm not sure if that's what you mean since I don't use Win98 and therefore may be misinterpretting you, if so, I apologize."
    author: "Martin Fick"
  - subject: "Re: Text file preview."
    date: 2000-12-01
    body: "You are completely wrong there :)\n\nAlthough I was one of the people who helped implementing the textpreview I was almost shocked to see how well it worked. I recently browsed the home-dir of a friend (with his permission of course ...) and I was extremly amazing how much more information this textpreview-feature offered over the usual view. Basically I knew within two minutes what he did over the last four weeks.\n\nThe textpreview is also not important for actually *reading* the text (although you can read the upper part of it which contains very often keywords which make you know what the document is about). It is important for recognizing the layout! If you create a document yourself you will be able to recognize it by looking at the layout -- even if you can't read the text inside.\n\nThe semitransparent icons are used to determine the mimetype quickly. So you *can* distinguish .txt, .cpp etc. by looking at the watermark-icon. You are right though that it is currently a bit too transparent to be comfortably visible.\nAnd after all it's possible to switch this feature off.\n\nGreetings,\nTackat"
    author: "Torsten Rahn"
  - subject: "Re: Text file preview."
    date: 2000-12-01
    body: "I'm not sure how wrong he is, personally...\n\nI find the watermarks nearly impossible to read quickly, in the screenshot.\n\nPerhaps the level of transparency should be configurable?"
    author: "Neil Stevens"
  - subject: "Re: Text file preview."
    date: 2000-12-01
    body: "Reread the second-last sentence in my reply ...\n\n\"You are right though that it is currently a bit too transparent to be comfortably visible.\""
    author: "Torsten Rahn"
  - subject: "Re: Text file preview."
    date: 2000-12-01
    body: "\"Reread the second-last sentence in my reply ...\"\n\n<never-ending-recursion>\nOk, and what was your second-last sentence ? :P\n</never-ending-recursion>"
    author: "Lenny"
  - subject: "oops.. sorry"
    date: 2000-12-02
    body: "How did I miss that?"
    author: "Neil Stevens"
  - subject: "Re: Text file preview."
    date: 2000-12-01
    body: "I will happily be proved wrong when I get to use it for myself.\n\nLooking forward to the next release.\n\nJC"
    author: "JC"
  - subject: "Re: Text file preview."
    date: 2000-12-01
    body: "I c what u meen. And I hope the gods (read: KDE programmers) do so too. There will probably be some config option that will let the Icon stand out from the Preview by Precentage or someething. (KDE usualy don't spare the options :)<br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: Text file preview."
    date: 2000-12-02
    body: "I don't think there's any doubt we'll allow you to set the amount of blending. For one thing, the amount the blend stands out is partially dependent on the monitor you use (eg. I use an LCD). It is definitely something that can benefit from user configuration.\n<p>\nOn the idea of text file previews in general, I have a similar view to Tackat - I thought it would be pretty but useless - I was wrong. I find that it is amazingly useful in practice. The only problem I have is that far too many source files include loads of copyright headers first, which make the previews look the same.I can't speak for the other developers, but I'm moving mine to the end of the files as the preview feature is too good to waste.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Text file preview."
    date: 2000-12-02
    body: "<i>I don't think there's any doubt we'll allow you to set the amount of blending. For one thing, the amount the blend stands out is partially dependent on the monitor you use (eg. I use an LCD). It is definitely something that can benefit from user configuration.</i>\n<br><br>\nAgreed. Now you can add an entry into your konquerorrc:<br><br>\n[FMSettings]<br>\nTextpreviewIconOpacity=70<br><br>\n\nPossible values are 0-255 (larger values = more opacity of the icon).<br><br>\n\n<i>The only problem I have is that far too many source files include loads of copyright headers first, which make the previews look the same.I can't speak for the other developers, but I'm moving mine to the end of the files as the preview feature is too good to waste.</i><br><br>\n\nGood idea, will do the same :)"
    author: "gis"
  - subject: "Re: Text file preview."
    date: 2000-12-04
    body: "It would be nice for .c/.cpp files if the preview was moved to the main() as standard ;)<br><br> /kidcat"
    author: "kidcat"
  - subject: "Alpha blending in PNG images?"
    date: 2000-12-01
    body: "While the alpha blending for icons is very nice, do you plan to support the alpha channel of PNG images in Konqueror?\nWith KDE2.0, Konqueror uses only partially the alpha channel. I would like a complete support.\n\nThanks."
    author: "Federico Cozzi"
  - subject: "Alpha blending in images"
    date: 2001-05-14
    body: "i hope that you could help me gather enough information about alpha blending bmp images. thanx."
    author: "rose alonzo"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "Actually i think these are exellent features because it makes the desktop attractive to windows users because when they see a console they panic but when they have an attractive GUI they feel like they're home."
    author: "Hilbert Mostert"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "EXCATLY! I say, keep pouring in eyecandy and cunning UI-tricks so the M$ users will droooool until they can't resist trying it out anymore.. and <b>bingo</b>, a new KDE'r is born :)<br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "I have no idea if this comment is sarcastic or not. Although, I will say I agree with you if you are not being sarcastic, simply because a good looking desktop is important. Why? Because although tooth candy is bad for teeth, eye candy makes a desktop more attractive, and useful simply because when it's attractive, it becomes more of a work of art. Of course, this should never be more important then functionality (if it is, you end up with things like iMacs, all looks, very little substance). The great thing about alpha blending is that it is useful. You can, with a well designed alpha blended interface, see two things at once. For instance, you can see text in front of an image without having to do screwey things with fonts, and see the description for an image at the same time as seeing the image itself (transparent tooltips would be great too, this is how Office should do its annoying little paperclip, so that it doesnt come in front of your work to interrupt and remind you of how helpful it is [of course, the assistant is a bad idea anyways])."
    author: "Carbon"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "it was not sarcastic at all :) And yes ur right, AB is a good thing that can be usefull and not just good looking. <br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "it was not sarcastic at all :) And yes ur right, AB is a good thing that can be usefull and not just good looking. <br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "it was not sarcastic at all :) And yes ur right, AB is a good thing that can be usefull and not just good looking. <br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "DOOOOOH! This is a bad example of what NOT to do :)"
    author: "kidcat"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "Cool!\n\nNow i'd like to see the thumbnail of the HTML document instead of his source :o)\n\n[i'm half kidding: could be put as an option for people with <i>_fast_<i> machines]"
    author: "emmanuel"
  - subject: "Menus and toolbars"
    date: 2000-12-01
    body: "Completely of topic, but:<br>\nWill Qt-3 or KDE-2.1 allow menubars and toolbars to be combined on the same bar to say. Today you can have more than one toolbar vertically connected....<br><br>Just like IE and Win2000 has it."
    author: "Sam"
  - subject: "Re: Menus and toolbars"
    date: 2000-12-02
    body: "Errr, it already does this?"
    author: "Ill Logik"
  - subject: "Re: Menus and toolbars"
    date: 2000-12-04
    body: "Noops.<br><br>\nThere is no way to put a menubar together with a toolbar. Ie: <br>\n_________________________________________<br>\n\u00a6[File][Edit]|  \u00a6[Toolbar Icon] [Icon]<br>\n------------------------------------------<br>\n<br>That is not supported. There is pretty much dead space between the right most menu (usually help) and the right border of the screen.\n\n<br><br>/Sam"
    author: "Sam"
  - subject: "Re: Menus and toolbars"
    date: 2000-12-05
    body: "Yeah, I checked toolbars, but didn't check menu bars....  my bad......."
    author: "Ill Logik"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "This is great. Unlike a lot of people I find many text utilities usefull and I'm not scared about the commandline but I do like a nice looking desktop and I like cool features even though they might not be terribly usefull. I think a lot of people would agree on that. I remember when I first booted up Linux (RedHat 4.2 I think) I though yuck this looks really amaturish. I didn't matter if you could to productive things with the thing. It didn't look proffesional and I think that has scared off a lot of people earlier.\n\nAfter looking at the Quick Time movies of Mac OS X I'm almost ready to waste a lot of money to get a Mac and play with that wonderful looking OS (yeah I know it's stupid but the OS just looks so good). I hope we will be able to make KDE look like Mac OS X some day. But before we can do that I think we got to get rid of X."
    author: "Erik Engheim"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-04
    body: "Why is that stupid?  I just did exactly that - a 450MHz dual processor G4.  I can now run OSX, OS9, and Linux.  The machine is fast, it looks superb, and it feels so much better than any PC I've played with.  Not a waste of money at all."
    author: "curmi"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-05
    body: "I agree with the other poster, it's not stupid. I feel that working with the computer should be a pleasant and productive experience, whether GUI or CLI is your preference, or you always switch between both a lot :-)\n\nThat's why my WinBox is skinned to the gills, thanks to the folks at litestep.net and skinz.org. They hardly resemble that boring ol interface from u-know-where"
    author: "jazz"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "Why don't you folks just use GNOME and stop wasting your time?"
    author: "Inspector Todd"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "Why don't you just troll somewhere else?"
    author: "Jacek"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "Why do YOU waste your time writing trollish postings?\n\nTo give you a real answer: for my personal needs KDE is better: better to program (C++), nicer and more consistent GUI and better apps.\n\nLast but not least the signal / noise ratio is \nexcellent and the KDE community is very friendly and helpful.\n\nSo what is the result of your flame? One more reason to stay with the best desktop: KDE!"
    author: "Marco Krohn"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "No, see, I like my desktop *not* crashing...  If you're gonna make a stupid statement, it might as well be, \"Why don't you folks just use Windows and stop wasting your time?\""
    author: "Ill Logik"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "I just rebuilt from CVS, but cannot see the new effect. Do I need to grad any new icons, and if so, from where?\n\nThanks,\nD"
    author: "Anonymous Custard"
  - subject: "Re: Advances in Alpha-Blending\u00a0\u00c7\u00b4@X\u00bc(\u00c0_),\u00e5\u00b4@\u00a3-\u0088\u00e4\u0088\u00e4\u0088\u00e4\u00d0\u00a6$\u00f8z)`"
    date: 2000-12-01
    body: "Quote from the orig announcement:\n\nIf you want to test it, go and edit manually your\n$KDEHOME/share/config/kdeglobals file and add a\nalphaBlending=true\nentry to the [General] section.\n\nCheers!ar_\u0081 \u00fd\u00ec@ \u00fd\u00ec@\u00f8\u008c.hQP\u009c%q\u0088\u00ef,\u00fc\u00ec@iled View0Name@\u00b0\u009b%XpW\u00a0W\u0090(\u00fb\u00ec@ B\u00b4@\u00ff\u00ff\u00ff\u00ff\u00af\u00b8\u00f0\u009b%\u00f0,\u00e0 <@\u00b9\u00e8#(X\u00fc\u00ec@`\u00da,@\u00c0<@(\u009c%(\u00f8\u008c.p\u009b%\u0098\u00a6\u0081\u00c8\u00fd+ \u00fc\u00ec@\u00e7\u00b4@\u00a8'\u00b8\u00fb\u00ec@naller@@\u0080\u009b%X\u00b0\u00fb\u00ec@h\u00a0WT\u00ad"
    author: "Zander\u00e8!DD\u00b4@"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "did you put alphaBlending=true in the [General] section of your kdeglobals file?"
    author: "Tick"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "Yes\n\nI have\n.\n.\n.\nName_8=\nNumber=8\n[General]\nalphaBlending=true\nbackground=192,192,192\nbuttonBackground=217,217,217\nbuttonForeground=0,0,0\n.\n.\n.\n\nin my ~/.kde/share/config/kdeglobals\n\nI also checked my CVS sources, and it's definitely the new version."
    author: "Anonymous Custard"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "I take it all back.\n\nI didn't realise that only the 48px icons had made use of the alpha channels.\n\n:)"
    author: "Anonymous Custard"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "I see, you guys make gigantic steps forward. Alphablending, wow!! These are the things revolutions are made of.\n\nAK"
    author: "Albert Knox"
  - subject: "Kind of on this subject"
    date: 2000-12-01
    body: "Speaking of icons and alpha channels, I've been having a problem that no one else seems to have seen.  Anyone know about this?\n\nUsing recent CVS, starting about three weeks ago, kicker icons look fine during the zooming. However, if I turn off the zooming, the icons are all greyed out. Going to the control panel for icon effects shows no effects active. If I apply an effect, it works but the icon is still greyed out.\n\nThis is after reinstalling from scratch with fresh CVS and a fresh .~/kde. Any ideas?"
    author: "Otter"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-01
    body: "Anto\u00f1ito es un maestro, el Malague\u00f1o m\u00e1s apa\u00f1ado que he visto. Adem\u00e1s le tengo mucha envidia porque vive m\u00e1s cerca de mi ni\u00f1a (ella est\u00e1 en Sevilla y yo en Barcelona).\n\nYa me gustar\u00eda a mi poder tener tus conocimientos para poder contribuir a KDE. Ahora que mi editorial ha cerrado mi revista podr\u00eda meterme de lleno en ello, tendr\u00e9 que conformarme con la traducci\u00f3n."
    author: "Manuel Rom\u00e1n"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "C++/programacion no es tan dificil muchacho. ponte a jugar con eso, y en dos o tres meses tu eres un experto."
    author: "krazyC"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "bablefish says:<br>\nC++/programacion is not so dificil boy ponte to play with that, and in two or three months your you are an expert"
    author: "kidcat"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: ":)"
    author: "krazyC"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "bablefish says:<br>\nAntonito is a teacher, the grasped Malagueno more than I have seen. In addition I have much to him envies because it lives more near my girl (she is in Seville and I in Barcelona). It would already please me to my power to have your knowledge to be able to contribute to KDE. Now that my editorial has closed my magazine could put to me completely in it, I will have to conform to me to the translation"
    author: "kidcat"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "Thanks.\n\nSee you other day."
    author: "Manuel Rom\u00e1n"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-04
    body: "Cashondos...."
    author: "Otro esp\u00e1nis"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "Alpha blending? Do you have seen www.berlin-consortium.org ?"
    author: "McLoud"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "Berlin-consortium ? Have you ever seen a Berlin-app running on a Cray redirected to your 486 ?"
    author: "Lenny"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-04
    body: "No, and I don't want to do it. Instead, I guess it will run better on my Athlon 600 redirected to my P200 doing it's good job on true alpha blending, instead of doing icon overlapping on a static background, thing that started with win95.\n\n(ps. sorry my very bad english)"
    author: "McLooud"
  - subject: "Berlin != X"
    date: 2001-07-20
    body: "What a stupid idea! With berlin's architecture all the alpha blending happens in the (display-)server, so your 486 would do it.\n\nThe idea is that clients create graphics in the serverprocess. So that knows everything it needs to redraw, change alpha transparency of windows, move the windows around, ... all without _any_ communication with the server. Once a UI is set up only chnages in the client's state are transmitted over the network. That way we use very little bandwidth.\n\nRegards,\nTobias"
    author: "Tobias Hunger"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "I think this is great and a step forward. If you don't like it and you feel it slows you down, I am sure that there will be an option to turn it off. Keep up the good work guys!"
    author: "CockneyRebel"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "I think this is a great feature, as long as i have the option to disable it. But what about font smothing and colored mouse cursors?"
    author: "heroo"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-02
    body: "I want font smoothing too! It looks great on OSs that have it - such as Mac OS X. Doesn't X handle the fonts? Or can we use FreeType or something?"
    author: "jpmfan"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-03
    body: "Sorry, font smoothing would have to in X, which it isn't (yet)."
    author: "David"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-05
    body: "Not necessarily.  Nautilus does font and icon smoothing, but as of the second test release it is fairly slow.  But from what I understand, when it is in X, it will be universal, which will be sweet :)"
    author: "Caleb Land"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-04
    body: "It is closer than you think: \n<A HRef=http://lists.kde.org/?l=kde-devel&m=97591561811342&w=2Target=_new>read this thread from the mailing lists.</A>\n\n\nIf you have a magnifier on your system, you can see how the text is anti-aliased in the screenshot."
    author: "reihal"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-04
    body: "It is closer than you think: \n<A HRef=http://lists.kde.org/?l=kde-devel&m=97591561811342&w=2Target=_new>read this thread from the mailing lists.</A>\n\n\nIf you have a magnifier on your system, you can see how the text is anti-aliased in the screenshot."
    author: "reihal"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-03
    body: "OK here is what i have to say in case u care:\n\nthis is one step towards a windows-like gui and when i say windows-like im mean a gui stuffed with crap nobody needs. stuff like this here is only slowing down the system - increasing kde-code - could make system unstable \n\nwhen will we have a mouse-shadow ??? \n\nhi to everybody \n\ndaniel"
    author: "Daniel Schlieder"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-03
    body: "Daniel you are so right! KDE still misses a lot of nice features. I don't want to say that this alpha blending isn't nice to look at, but it doesn't make the desktop more usable.\nFirst of all make the desktop more stable! There are so many bugs left on the new KDE2. I think that KDE has no change as a multimedia operating system, but has a lot of users in the scientific world. These users don't need alpha blending, but need a fast and stable desktop.\nI would like to see features like burning a CD without starting an external program (modul in konqueror? - just drag and drop on a CD-symbol?), better tools for programming (keep developing on kdevelop!), to combine kmail,korganizer,kab and some others to something like 'outlook'.\n\nBut first of all stability comes first and then the 'cool looking'!"
    author: "Steffen"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-03
    body: "But, the thing that you have to remember is;\nDifferent people do different things, some work on the stability some work on the candy, so should the people on candy wait for the others, or should the do more work?  Seriously, that's what makes KDE so cool, lots, and LOTS of cool crap that can be turned on...   It's not like they'll force you to use this...  And I for one want a mouse with a shadow...   ;-p"
    author: "Ill Logik"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-04
    body: "I think you could probably attract more new people to KDE by including all these fairly pointless but  pretty features like alpha blending, shadow mouse pointers, anti-aliased text and so on, as these people would probably come from a Windows environment which is full of rubbish that no-one needs.\n\nIn regards to code bloat from these new features, please bear in mind it's KDE we're talking about which is fairly modern in terms of the technologies it uses (apart from X :-)) unlike Windows 95/98/SE/Me/2K/Whistler which is based on MS-DOS and Windows 3.1.\n\nAs long as people who don't want them have the option to turn them off, I don't really mind, so long as it doesn't divert resources from putting in all those really cool features..."
    author: "jpmfan"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2002-07-19
    body: "You can do this if you have an Nvidia based card.  I can give you the config. I for one really like the eye candy.  I can't wait for 3d powered version of KDE like the new OSX release."
    author: "Michael McLaughlin"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-04
    body: "So what ?<p>\nGfx cards get faster and faster.<br>\nMemory (let's hope!) tends to fall in price ,ost of the time.<br>\nCPU power grows rapidly.<br><p>\nWindows has some nice features. Only a fanatic can say it's completly crap.\n(I use AmigaOS btw. (you could name <em>me</em> a fanatic, hehe), neither Windows\nnor Linux, but soon will)<p>\nAnd guess what: My mouse-pointer HAS a shadow and it looks absolutly cool.\nI don't know how it is on Linux, but I guess the mouse-pointer is a bitmap.\nHere it is. And it has a shadow drawn in.<p>\nWhat definitly would be cool would be an option to position the lightsource\non your desktop: The shine/shadow elements of the GUI would be rendered \naccording to that. And connect that with the daytime and you have a sun\nmoving on your desktop. That would be cool. Hehe.\n<p><p>\n\nDon't get me wrong !<br>\nI like thin and stable systems. I LOVE the commandline, I really like passing series of commands through a pipe, I often prefer it rather than firing up a full blown GUI.\nI am not a GUInatic. I like both worlds at their maximum for maximum joy.\nAnd....I am sure that you can switch this off. (Well, not the bugs, though.)"
    author: "jock"
  - subject: "to the developers and all who are interested ;)"
    date: 2000-12-04
    body: "maybe this is the right moment to discuss something, i lately noticed.\ni switched to icewm as my desktop of choice.\nthis is not a big deal. \ni still like to use konqueror, kmail, knode, etc.\nthese are great programs.\nbut i noticed, that if i start any kde app, several kdeinit processes are started.\ni guess these are all the processes to provide enough of a kde environment, that those apps are able to run.\ni don't like this really much, but i can understand that it is necessary now.\nnevertheless, they should atleast quit, when they are not needed anymore.\ni hope that you will think more of other desktop environments soon.\nthe power of choice is the power of unix.\nif you want to become a unix standard desktop, you shouldn't ignore the rules.\nthis is not a rule, just a friendly reminder ;)\ni used to like kde a lot and it sure has a great potential when working together with other applications and not ignoring them."
    author: "Spark"
  - subject: "Re: to the developers and all who are interested ;)"
    date: 2000-12-04
    body: "I think on thursday a counter for KDE programs was implemented (KDE 2.1 CVS Head). Thus kdeinit and all the other background processes will quit when the last KDE app is gone.\n<p>So this has already been implemented :-)"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: to the developers and all who are interested ;)"
    date: 2000-12-05
    body: "oh... WTG! :)"
    author: "Spark"
  - subject: "Good stuff, but not really usefullI [\"\u0098\u0091:\u00ff\u00ff\u00ff\u00ff\u00ff\u00ff\u00ff\u00ff@\u00c8\u00e2"
    date: 2000-12-04
    body: "Hi all,\n\nI think it's really nice to look at it.\nBut as you can read in the other statements\nit only getting the kde core bigger.\n\nI'd think it's okay to make it optional.\n\nBut really really more usefule would be other thinks like plugins for konqueror some peoples\nreally need I think, e.g. a CVS integration such as TortoiseCVS it is.\n\nYou know there are alot of developers making the code of kde, so it would be really a good idea to\nhave an CVS integration in konqueror. It's really easier.\n\nRegards,\nChristian Parpart (mailto:cparpart@surakware.com)\nSurakWare\nhttp://www.surakware.com\u00d0T:\u00c0P:i\u00e8\u00a0+\u00df6\u00c0P:.8/a`[\u00b4@\u00d0T:I\u00806,\u00d0\u00e2\u00ea@ ]\u00c8\u00e2\u00ea@(0\u00cc\u00d1!A\u00d0\u008c'T:\u00c8\u00e2\u00ea@i\u00e8\u00a0+ \u00e16T:XP:(\u00e16.975`[\u00b4@\u00d0\u008c'gFor\u00806,\u00d0\u00e2\u00ea@\u00c0@\u00cc\u00d1!A\u00b8\u00df6\u00e06\u00c8\u00e2\u00ea@)`\u00cd:\u00e8\u00e2\u00ea@\u00d0\u00d0\u00e2\u00ea@@\u00cc\u00d1!A8T:\u0099p\u00c0X\u00e4\u00ea@\u0089`\u00a2 H\u00e4\u00ea@\u00c2*n!A8\u00c2*\u00fcm!A`\u00c2*\u00ecm!A\u00d8\u00d6*\u00dcm!ApS:\u00ccm!A\u0091H\u00b62`w%\u00e06\u00e2\u00ea@`[\u00b4@\u0098\u00e06\u00ff\u00ff\u00ff\u00ff\u00d8\u00e2\u00ea@9\u00b0\u00f66\u00f8\u00e2\u00ea@\u00b0\u00e06\u00a0!\u0098x+\u00e0\u00e2\u00ea@\u00b1\u00806,\u00d0\u00e2\u00ea@\u00c8\u00cc\u00d1!A\u00e8\u00e06@n3\u00d0\u00d0\u00e2\u00ea@\u00f00\u00cc\u00d1!A\u00d0\u00e06T:i\u00e8\u00a0+X;T:\u00df6`[\u00b4@\u00d0\u00e06\u00806,\u00d0\u00e2\u00ea@x@\u00cc\u00d1!A\u00e0\u00ea@\u00dc6\u00dc6\u00c8S:p\u00e26\u00c8\u00e2\u00ea@\u00c02\u00d0\u00e3\u00ea@\u00e46\u00e0S:{5\u00e9@\u00a1!\u00a8\u00e3\u00ea@\u00ff\u00ff\u00ff\u00ff \u00e26\u00c1\u00d0P7\u0080\u00e3\u00ea@\u00ff\u00ff\u00ff\u00ff\u00e26\u0099\u0098\u00ce5X\u00e3\u00ea@(q\u00bf20\u00e3\u00ea@\u00c2*n!A8\u00c2*\u00fcm!A`\u00c2*\u00ecm!A\u00dd6\u00dcm!Ax\u00ad5\u00ccm!A9\u00a8M78\u00b1&p\u00e26\u00a8*\u00ff\u00ff\u00ff\u00ff\u00c80\u00ea@0("
    author: "Christian Parpartn;charset=utf-88"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-04
    body: "Great stuff!!!\n\nBut what about the overall GEOMETRY Control of KDE2 packages? \n\nIt aint no good if it just looks good because it has to also function as a usable desktop too!\n\nMaybe you need some \"Alpha Bent Feet Icons\" and do a re-name to \"Knome\"?"
    author: "James Alan Brown"
  - subject: "Good stuff, but usefull?"
    date: 2000-12-04
    body: "<h3>Hi all,</h3>\n\nthis feature looks really very nice. But as you have read above, it's not really usefull. <br/>\n<i>Btw. the shadow cursor like the one from m$win looks nice too.</i><br/><br/>\n\nBut I think the real missing features (for developers) aren't implemented yet.<br/><br/>\n\nSo, what do you think about an integration of CVS access for the konqueror, such as TortoiseCVS for InternetExplorer it is. These is something required from all developers (developing for kde, gnome or all sourceforge projects) <i> (otherwise an standalone K-CVS-app) </i><br/><br/>\n\nWhat do you think?<br/><br/>\n\nRegards,<br/>\nChristian Parpart \n<a href=\"mailto:cparpart@surakware.com\">\ncparpart@surakware.com</a><br/>\nSurakWare<br/>\n<a href=\"http://www.surakware.com\">\nwww.surakware.com\n</a>"
    author: "Christian Parpart"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-04
    body: "Text preview: Theoretically a very useful feature, but the way its done above is not good enough. First, as mentioned several times above, the icon is more important than the content and should thus be displayed stronger. Maybe in the top left corner going over the edge of the white paper, leaving a nice alpha-blended drop shadow on paper as well as background (with different displacements to show that the paper is closer to the icon...)? \n\nAlpha channel for icons: Cool, alpha channel should really be standard when dealing with any kind of graphics. \n\n<p>What would be even cooler is <b>antialiased text</b>. This might be a problem due to stone age X architecture - but if it could be done it would be soo great. It's getting sort of embarrasing not having antialiasing yet... Looks Win3.1-isch to MS users. ."
    author: "Johan"
  - subject: "Font Smoothing/Anti-Aliasing"
    date: 2000-12-04
    body: "I too would like anti-aliasing. I've seen it working on an OS called Acorn RISC OS ages ago. It need to be done at the X protocol level - but don't despair, I think it's on the way: I've just found this link:\n\nhttp://www.xfree86.org/~keithp/render/\n\nSmall fonts are much more readable when smoothed using anti-aliasing."
    author: "Adam"
  - subject: "Re: Font Smoothing/Anti-Aliasing"
    date: 2000-12-05
    body: "yes, but the plan to include antialiasing in X is years old :(\nwhy does it take so long?\nit looks like it would be already working fine on the screenshots. whats the problem? does anyone know more?"
    author: "Spark"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-05
    body: "This article has been Slashdotted!\n<A HREF=\"http://slashdot.org/article.pl?sid=00/12/04/1633247&mode=thread&threshold=-1\">http://slashdot.org/article.pl?sid=00/12/04/1633247&mode=thread&threshold=-1</A>\n\nYou won't believe how many trolls and flamebaits you'll find there!"
    author: "LOL"
  - subject: "Re: Advances in Alpha-Blending"
    date: 2000-12-05
    body: "I had a look at this, just to see... not very pleasant reading. Didn't have much to do with KDE either, it just seemed to be a forum for people to have a go at GNU, Linuxm, homosexuals and so on...\n\nFortunately, this site is a lot more grown up and 99% on topic..."
    author: "jpmfan"
  - subject: "How about this stuff?"
    date: 2000-12-05
    body: "pretty cool screenshots on\n<a href=\"http://www.jcraft.com/weirdx/screenshots.html\">WeirdX</a>."
    author: "Mike"
  - subject: "Windows envy!"
    date: 2000-12-05
    body: "I just thought everyone would like to know that windows developers are <B>already</B> envious:\n<BR><BR>\n<A href=\"http://www.codeproject.com/lounge.asp?select=13911&fr=1#xx13911xx\">Codeproject Post</a>\n<BR><BR>\nYou saw it here first folks!  =P\n<BR><BR>\n-pos"
    author: "pos"
  - subject: "Re: Windows envy!"
    date: 2000-12-06
    body: "I've said it before and I'll say it again:\n\nSince we all like to point out how stupid Windows is, Windows developers being envious is almost a sign you've made a mistake, not an advance :)\n\nAlso, from a usability point of view, I concurr with the text file contents comments. Icons are far more useful. After 15 years of recognizing files by their icons and names, its a step backward to try to identify files by the first 10 lines. Alpha blending is far more useful for terminals, disabled icons, cursors (I personally think the whole cursor should be blended ~ 80% with what's behind it.)\n\nAt any rate, its also a huge CPU suck until X supports alpha blending at a lower level, isn't it?"
    author: "KraftBoy"
---
Icon alpha-blending has taken some big steps forward today.  First, <A HREF="mailto:antlarr at arrakis.es">Antonio Larrosa</A> introduced alpha blending to the HEAD branch in CVS.  Here's a <A HREF="ftp://ftp.kde.com/pub/dot/img/alpha-2.png">screenshot</A> showing one icon blended into another.  A few hours later, <A HREF="mailto:carpdjih at cetus.zrz.TU-Berlin.DE">Carsten Pfeiffer</A> shot back with this tasty <A HREF="ftp://ftp.kde.com/pub/dot/img/preview.png">screenshot</A> of Konqueror previewing text files with the mimetype icon blended in.  Oh, I can't wait for 2.1 -- time to hit <A HREF="http://www.kde.org/anoncvs.html">CVS</A>!  (screenshots below)


<!--break-->
<BR><P>Thought I'd put the screenshots right inline for ya'll:</P><BR>
 <IMG SRC="ftp://ftp.kde.com/pub/dot/img/alpha-2.png" WIDTH=130 HEIGHT=505 ALT="Desktop Icons Blended" HSPACE=10 VSPACE=10 ALIGN="top"> <IMG SRC="ftp://ftp.kde.com/pub/dot/img/preview.png" WIDTH=722 HEIGHT=768 ALT="Konqueror Preview Blending" HSPACE=10 VSPACE=10 ALIGN="top">

