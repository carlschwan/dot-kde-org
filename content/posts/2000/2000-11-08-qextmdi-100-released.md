---
title: "QextMDI-1.0.0 released."
date:    2000-11-08
authors:
  - "fbrettschneider"
slug:    qextmdi-100-released
comments:
  - subject: "Re: QextMDI-1.0.0 released."
    date: 2000-11-07
    body: "Please note, that KDE does NOT use MDI, it uses SDI. Everyone should have a look in the UI styleguide, where the reasons are explained."
    author: "Xiang"
  - subject: "Re: QextMDI-1.0.0 released."
    date: 2000-11-07
    body: "MDI means that one application can deal with many documents at the same time. QextMDI provides several different types of visualization of such an document architecture. If you like to have all views of your application as toplevel windows (like e.g. in gimp), you simply switch to Toplevel mode.\n\nF@lk"
    author: "F@lk"
  - subject: "Re: QextMDI-1.0.0 released."
    date: 2000-11-08
    body: "Having a good working MDI manager is execllent. There\nis always some designs/environments that will benefit\nby using this document/display model (regardless\nwhat people say :)\n\nThe standard QWorkSpace is ok for basic tasks but is \na bit (read: too) limited once one need to fine tune the\nbehavior."
    author: "Espen Sand"
  - subject: "Yes!"
    date: 2000-11-08
    body: "I second this motion. While I understand what another poster said about just using it to manage multiple documents, I hope developers don't go and do the Wrong Thing.\n\nI have a feeling that the KDE UI guidelines/style guide etc. needs to be pimped a hell of a lot more to developers - I bet a lot of them haven't even read it. Consistency between ALL KDE apps (unless when it's definitely not the right thing to do) is a target to aim for. UI must not be neglected!\n\nHmm..Well, that's my mini-rant for the day ;)"
    author: "Matt"
  - subject: "Re: Yes!"
    date: 2000-11-08
    body: "Well, i'm only an old KDE user. I think MDI is a good think. I don't like when my windows move around my desktop without a way to reorganize them automatically according to specifyed rules. Opera (pre)runs with MDI and it sounds great. Use Gimp or Xterm and you get flooded with top-level windows you can't organize/find. MDI will not mess kicker with a lot of pseudo-applications. Basic KDE-applications don't follow SDI: the KDE-Konsole uses a sort of MDI and it's usefull. Konqueror uses frames. QextMDi offers just more freedom. The only question is : does it use more ressources? I would even like windows (inside a MDI system or on the desktop) being able to glue (and communicate) each other (a la Opera), producing a kind of big framed-window (or an easy/poor sort of embedding/viewing system).\nI wonder if/why it is difficult to give Kdesktop the possibility to nicely show all windows of a given desktop (not like the very limited actual \"unclutter/cascade\" option).\nFinally KDE2 looks great! I'm impressed."
    author: "xavier"
  - subject: "Re: Yes!"
    date: 2000-11-08
    body: "If you really like keeping windows in only one place, then use the session taskbar system of konsole. Basically, MDI is a bad thing because:\n1. You can't move the windows for different docs to different desktops\n2. It's a pain in the arse to manage extrememly large amounts of documents\n\nAbout the only thing that's appropriate for MDI is UI database work, where each MDI main window is a database, and it includes sub windows for tables, queries, and such, but I can't think of anything other then that. Besides, anything that isn't really simple ought to be done with Apache/PHP(or perl or python)/Mysql(or msql or postgresql) instead, it's much more versatile that way."
    author: "David Simon"
  - subject: "Re: Yes!"
    date: 2000-11-09
    body: "You haven't understood QextMDI. Otherwise you wouldn't argument with your 1. item. With QextMDI it _is_ possible to move MDI view windows to other desktops. What you have in mind is QWorkspace where an MDI view can only live within the mainframe window.\nConcerning to your 2. argument: I see no advantage in using SDI for the same number of documents. You overload your common desktop in the same way. Additionally, application-foreign windows mix you up."
    author: "F@lk"
  - subject: "Re: Yes!"
    date: 2000-11-10
    body: "I wasn't aware of this feature of QextMDI. Also, what do you mean by application foriegn-windows? If it isn't part of the app, or at least supportive of DCOP or KParts, wouldn't it be incompatible anyways?\n\nMy main problem with MDI is confusing concepts, that would mess up newbies. I'm not arguing that it isn't powerful enough, rather, that it isn't user-friendly enough. I can see it, like I said, being useful in situations where a newbie wouldn't be anyways."
    author: "David Simon"
  - subject: "Re: Yes!"
    date: 2000-11-10
    body: "> application foriegn-windows\nI mean they are among your windows in case of using many SDI windows on the KDE desktop."
    author: "F@lk"
  - subject: "Re: Yes!"
    date: 2001-07-12
    body: "I like MDI feature of Opera alot. I understand that most of the users may not need MDI (for most of the apps), but heavy surfers like me are in desperate need of MDI. It not only organizes your browser windows inside a separate space (thus it doesn't clutter the taskbar space giving more space to other apps), but it also gives the facility to save the surfing session. I like this feature of opera alot. If somehow opera crashes, when its run again it gives the option to restore the session from where it crashed and because of this feature I use opera though opera is still not very stable and crashes a lot, but I can always restore the session.\n\nHaving said that I like Konqueror alot too and hope that someone would come up with an MDI Konqueror based browser (maybe using KTHML as rendering engine)."
    author: "Ghulam Sarwar"
---
It's a Qt-extension widget set for MDI that is an alternative to QWorkspace. Compared to QWorkspace, your application is configurable in several modes - MDI like QWorkspace and Unix-like with all windows as toplevel. You can compile QextMDI as a KDE2 extension library, it then bases on KMainWindow and kdeui.so. You can decorate the MDI views with Win9x, KDE1 and default-KDE2 windowmanager style. Take a look <a href="http://www.geocities.com/gigafalk/qextmdi.htm">here</a> for further info (including <a href="http://www.geocities.com/gigafalk/qextmdi/pics_en.html">screenshots</a>).

<!--break-->
