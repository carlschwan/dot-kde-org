---
title: "KDE Documentation needs your help!"
date:    2000-11-02
authors:
  - "mmcbride"
slug:    kde-documentation-needs-your-help
comments:
  - subject: "Re: KDE Documentation needs your help!"
    date: 2000-11-02
    body: "Agreed. Documentation does need work :)\n\nDave\n\nP.S.: Konqueror rocks! :)"
    author: "David B. Harris"
  - subject: "Re: KDE Documentation needs your help!"
    date: 2000-11-02
    body: "I've wanted to volunteer in this arena for quite some time. The only thing holding me back is I haven't found too many resources for documentation writers. I've started writing some documentation using RoboHelp HTML (yes, I know it's a windows program but it's also the best as far as I'm concerned when it comes to HTML documentation). Only thing is, I figured it wouldn't be accepted by the KDE community so I scrapped it.\n\nFrankly, I don't know SGML and really don't have a desire to learn and kwrite is too ... well ... let's just say for DTP I think it's great, for Word Processing I think it's too cumbersome. So, my tools are StarOffice, kedit, or vi.\n\nAny suggestions?"
    author: "George Ellenburg"
  - subject: "Re: KDE Documentation needs your help!"
    date: 2000-11-02
    body: "An emacs-based package for writing SGML can be found at http://www.oasis-open.org/cover/tei-PSGML-SK.html, and probably elsewhere. It works on Windows; I have used it with some success."
    author: "Winter Laite"
  - subject: "Re: KDE Documentation needs your help!"
    date: 2000-11-03
    body: "You should not let SGML knowledge (or lack there of) stop you from helping.  Many of KDE's documentation authors write their documentation in ASCII text.  \n\nOne of our reviewers, who knows a lot about docbook, and KDE documentation can convert that documentation to docbook very easily.\n\nSo please reconsider.  We are happy to recieve good quality documentation (either docbook or text).  The content is the hard part.  We will help you with the docbook (or just do it for you if you don't want to learn it)."
    author: "Mike McBride"
  - subject: "Live Documentation?"
    date: 2000-11-02
    body: "Why not have an on-line (that is pulled off-line for initial distribution) set of docs that can be annontated...<p>\nI'm picturing something like the FAQs on SourceForge, but with editorial control over what becomes cannon, with the unedited user comments at the bottom of the documentation a la PHP or MySQL's site.<p>\nThat way, the documentation becomes a living document of the community that uses it.<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Live Documentation?"
    date: 2000-11-02
    body: "> That way, the documentation becomes a living \n> document of the community that uses it.\n\nAlthough it may be a good idea, you must remember that not all the people in the world speak English. The KDE documentation is (being) translated into many other languages, which I fear wouldn't be possible the way you suggested...\n\nRegards,\nLukas Tinkl\nCzech KDE team"
    author: "Lukas Tinkl"
  - subject: "Re: Live Documentation?"
    date: 2000-11-02
    body: "<i>which I fear wouldn't be possible the way you suggested...</i><p>\nActually, having the ability for a normal user to be able to contribute translations while using the program would (in my eyes) make it far *easier* to translate the help. <p>\nSomebody using an obscure dialect of a regional language might not be available to formally join the KDE effort, but if it's simply a matter of clicking a \"Help Translate\" button and typing a translation actually *in* the help window, and thus posting it to the global help documentation, I would imagine that everybody would add their own effort.<p>Again, some sort of editorial control would be in place for review purposes, but I would imagine that opening the help up to the maximum extent possible would be a Good Thing.<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
---
The KDE Team needs your help with writing documentation for all of the great applications in KDE!  This is your opportunity to help KDE become useful to the  users of KDE applications at any level. You don't need to be a professional writer, and you don't even need to know everything about a program. For more information, please visit us at the <A HREF="http://i18n.kde.org/teams/en/index.html">Editorial Team website</A>, or contact <A HREF="mailto:mpmcbride7@yahoo.com">Mike McBride</A> if you are interested.