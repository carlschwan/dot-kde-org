---
title: "Python Bindings and Scripting for KDE Updated"
date:    2000-10-21
authors:
  - "sgordon"
slug:    python-bindings-and-scripting-kde-updated
comments:
  - subject: "Re: Python Bindings and Scripting for KDE Updated"
    date: 2000-10-22
    body: "Very cool!  Are there already apps written that use PyQt or PyKDE?  How long till a port to KDE 2.0?"
    author: "KDE User"
  - subject: "Re: Python Bindings and Scripting for KDE Updated"
    date: 2000-10-22
    body: "PyQt and PyKDE have been around for some time, and yes they are very cool :).  They are the basis of VeePee and our new Python debugger called Eric.  I know a lot of people use them, but I'm not sure exactly what all is out there.  The port to KDE 2.0 will be trivial and should follow the official 2.0 release by just a few days."
    author: "Shawn Gordon"
  - subject: "Re: Python Bindings and Scripting for KDE Updated"
    date: 2000-10-22
    body: "Wow, that trivial?  Heh.  SIP must be very cool indeed.  Keep up the good work!"
    author: "KDE User"
  - subject: "Re: Python Bindings and Scripting for KDE Updated"
    date: 2000-10-22
    body: "Do you have any idea if it would be possible to modify sip to generate JNI bindings for Java?\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Python Bindings and Scripting for KDE Updated"
    date: 2000-10-22
    body: "It could be, but we are swamped with other projects.  SIP is open source, so if you want to give it a try, go for it, if you wait for us, it could be a while."
    author: "Shawn Gordon"
  - subject: "Re: Python Bindings and Scripting for KDE Updated"
    date: 2000-10-22
    body: "I'll give it a try if I have time (like you I'm swamped with stuff to do). The main issue I guess would be how to handle signals and slots in Java. I've thought of some solutions:\n<ul>\n<li>using reflection\n<li>using some native code\n<li>mapping them to events and listener interfaces\n</ul>\n\nBut I don't know how cleanly these would fit with the way sip works."
    author: "Richard Moore"
  - subject: "Re: Python Bindings and Scripting for KDE Updated"
    date: 2000-10-22
    body: "I've been using PyQt and PyKDE for ages - at\nmy website (http://www.valdyas.org/python) there\nare lots of projects I've started (some even\nfinished) with PyQt/KDE for KDE 1.1.2.\n\nI'm at two minds about using PyKDE now: PyQt has become cross-platform since version 2.0. However, I think it will be quite easy to write apps that use PyKDE (once it has been released for KDE 2.0), that degrade nicely to PyQt only when only that's\navailable. Python is so wonderfully dynamic.\n\nI've said it before: I've never had so much fun as when working with Python and Qt. Clean design, good documentation, instant impressive results, full feature-set..."
    author: "Boudewijn Rempt"
  - subject: "Re: Python Bindings and Scripting for KDE Updated"
    date: 2000-10-23
    body: "Hey it all looks great and all, but I can't help but dislike the name. You do know there here in England, that the word 'pee' mans to erm... well, to urinate?"
    author: "SJK"
  - subject: "Re: Python Bindings and Scripting for KDE Updated"
    date: 2000-10-23
    body: "Means the same here in the states, and VeePee was done by our only British employee.  It was actually called Visual Python originally, but we had some naming conflicts and had to change it.  VeePee was the first thing that came up, we will probably change it again at some point, but part of the problem is that the letters VP are all through the code, and we didn't want to make a mass change of that either.  We are open to suggestions :)."
    author: "Shawn Gordon"
  - subject: "Random new VP name generator"
    date: 2000-10-23
    body: "I have a way to change this little problem: the on-line VP name generator:  ;)\n\nChoose a V-word, then a P-word from the list below:\n\nVagrant       Pachyderm\nVanguard      Pen\nVampire       Piranha\nVegetal       Plasma\nVelocious     Pneumatic\nVenimous      Pocket\nViolet        Poison\nVolcanic      Poltergeist\nVolt          Problem\nVulgar        Python\n\nFeel free to add more words to this list."
    author: "GeZ"
  - subject: "Re: Random new VP name generator"
    date: 2000-10-23
    body: "I could go for Vegetal Plasma :)"
    author: "Shawn Gordon"
---
theKompany.com is pleased to announce the release of <a href="http://www.thekompany.com/projects/vp/">VeePee</a> v1.0 and <a href="http://www.thekompany.com/projects/pykde/">SIP/PyQt/PyKDE</a> version 2.1.  VeePee is the Python-based scripting environment for KDE, and SIP/PyQt/PyKDE are the Python bindings for Qt and KDE.  These updates are to support Python 2.0 as well as numerous feature additions and some bug fixes.  The VeePee page now contains <a href="http://www.thekompany.com/projects/vp/screenshots.php3?dhtml_ok=1">screenshots</a> for your enjoyment.



<!--break-->
