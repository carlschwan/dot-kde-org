---
title: "kde.themes.org Needs Your Help!"
date:    2000-12-10
authors:
  - "kdeFan"
slug:    kdethemesorg-needs-your-help
comments:
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-10
    body: ">Have you noticed that kde.themes.org has not been as active as everything else in the kde world? \n\nDefinitely!  I was looking forward to getting some REALLY cool themes for KDE 2.0.  There's so much potential!  With the new theming improvements, KDE can be made to look like anything you want.  Unfortunately, I haven't been able to find any good themes at all.  Hopefully kde.themes.org will become more active in the future.  BTW, I haven't tried the GTK theme importer.  Does it work well?"
    author: "not me"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-10
    body: "I wonder if it's just a matter of updating the site or if it's also true that nobody is creating themes? I think I'll try my hand at porting a few themes to kde as soon as I get some time off for the holidays. I'd create my own, but I'm not artistically gifted. :( Hopefully there is some good stuff out there that has an open license.\n<p>\nI haven't tried the GTK theme importer either, but it's said to be faster under kde that in a GTK environment! Personally, I'd rather use a native, coded theme than a pixmap theme though.\nmosfet.org is a good place to find kde2 theming info/tutorials.\n<p>\n-kdeFan"
    author: "kdeFan"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-10
    body: "not me><I>Unfortunately, I haven't been able to find any good themes at all.</I><P>\n\nkdeFan><I>I wonder if it's just a matter of updating the site or if it's also true that nobody is creating themes?</I><P>\n\nI think there is one single reason for this: The lack of a theme manager in 2.0.x.<P>\n\nFortunately this will change with 2.1.x, where the theme manager will reappear."
    author: "Matt"
  - subject: "Another reason"
    date: 2000-12-10
    body: ">  I think there is one single reason for this: The lack of a theme manager in 2.0.x\n\nAnother reason : You can change so many thinks \nwith kcontrol (color, size of icon, saturation, gamma, ...)\n\nI'm less interest in themes for this reason."
    author: "thil"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-10
    body: "Perhaps Mosfet can tell us something about the Theme  Manager and stuff.\nLong time since there was a update to www.mosfet.org, is he too busy coding?"
    author: "reihal"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-10
    body: "I think there was a Widget Designer on Mosfet's website earlier. Anyone knows if I'm right or am I dreaming?"
    author: "reihal"
  - subject: "I've made one theme..."
    date: 2000-12-10
    body: "Just to show something... I will soon submit to kde.themes.org...\n<p>\nTis attached.BTW, any comments?? :)\n<p>\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: I've made one theme..."
    date: 2000-12-10
    body: "Great work!\nA little bit more colour would be nice.\n\n<br>Klaus"
    author: "Klaus"
  - subject: "Re: I've made one theme..."
    date: 2000-12-12
    body: "Well..\n\nI kinda LIKE it ;D"
    author: "dys"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-10
    body: "He, he..\nTheme manager is already part of cvs. Go and compile it!\nHere is screenshot how it looks."
    author: "Zeljko Vukman"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-10
    body: "Great Stuff!\nwill it also handle winowdecortion and will it be ready for 2.1?<br>\nAnother question: is mosfet still an active KDE-developer? I couldn' find anything in the mailinglists newer than one month posted by him.\n<br>Klaus"
    author: "Klaus"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-10
    body: "Dude, the decoration of your windows rocks!\nwhere did you get it?"
    author: "Remenic"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-10
    body: "It's the new default decoration (the one called \"KDE2\") that's in CVS."
    author: "Maarten ter Huurne"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-10
    body: "is there any theme manager rpm available yet ?"
    author: "Massimo TRENTO"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-11
    body: "Looks great!\n\nDo you know if there's any planned support to make it easy to download/browse themes from kde.themes.org? That would be a *REALLY* nice feature to have. 2  things which would be nice:\n\nBrowse new themes (since the last time your were there)\n\nInstall the theme (automatically downloads, installs it)\n\n\nRich"
    author: "Richard Sheldon"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-10
    body: "Regarding Mosfet, yes I think he's busy coding. Because this is what he promised (he used the word \"promise\" himself) for 2.1:<P>\n\n\"Extended color schemes, including the KControl module, will be ready. I\ngot all the widgets for KControl working and just need to do \"HotSpot\"\nstuff. This is *really* cool - it's cool even with regular styles like\n\"CDE\" being able to select different colors for scrollbar buttons vs.\nsliders, etc... :) Two new highcolor styles will be ready - a Marble one\nthat follows color schemes and a \"MegaGradient\" one. KWM themes will be\nsupported by default, but like I said there is no theme installer. A\ndedicated Aqua plugin is in the works. Pixie needs to be fixed up and\nbrought back in, because it kicks ass and honestly I think it's going to\nbe the most useful thing I've done. OTOH the widget theme engine is\nhalf-rewritten and I need to finish that as well... This is why I am no\nlonger really paying attention to widget theme engine bugs - the new one\nis completely different.\"<P>\n\nIs all that in the CVS already?"
    author: "Matt"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-10
    body: "Hmmm ...\n\nmore than a year ago I started coding my own KDE2 style, but haven't finished it yet - mainly because I'm not a programmer and my C++ knowledge is minimal. Still, I hope to finish it sometime in the future, though I don't hope that everybody will like it - it's pretty minimalistic and aimed to be functional and not eycandy. But I hope that thoese who like Motif or NeXT look & feel will enjoy it (if I ever finish it ;-)"
    author: "fura"
  - subject: "What happened to the BeOS-like window decorations?"
    date: 2000-12-11
    body: "I remember seeing screenshots of KDE2 on mosfet.org. He had true BeOS-like WM decorations up there.  What happened?  It's all different withthe BII thing in KDE2.  I want my close in the far left!!"
    author: "Whiner"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-11
    body: "I was not using KDE as much before, but since KDE II released, it come my desktop. Anyway, I love to help KDE with both building theme and updating the site. But I truely have not idea about how to creat theme. Looking after or maintain the website maybe ok .. \n\nreply me if you want my help ...\nfrom your BIG FAN"
    author: "TOm"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-11
    body: "I don't think the people who are asking for help are reading this site (or they probably would have submitted their request for help). Perhaps you would like to offer your assistance to them and let them decide if you have the necessary skills? You can find their email addresses in the article.\n<p>\nAs for theming, mosfet.org has a kde2 theming tutorial. I don't know how good it is because I haven't read most of it yet. You can look at it <a href=\"http://www.mosfet.org/widgettheme-tutorial/\">here</a> if you're interested.\n<p>\nGood Luck,<br>\nkdeFan"
    author: "kdeFan"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2000-12-12
    body: "Hiya,\n\nA bare bones theme manager is currently in KDE CVS and will be released with 2.1. It currently only handles colors, backgrounds and icons but more\nwill be added including KWM style window decorations.\n\nYou can also use it as a quick way to create a theme from your current configuration. \n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2001-08-19
    body: "i dont know whats wrong with kde.themes.org but common sense would say if the server isnt up, maybe they cant aford the server.\n\nwhat i do know is i own a web site hosting / developement company, www.2GProductions.com\n\nif kde.themes.org needs a new home, id like to offer a account to them for free.\n\ncontact me if i can help\n\n-Mario"
    author: "mario"
  - subject: "Re: kde.themes.org Needs Your Help!"
    date: 2001-08-19
    body: "i dont know whats wrong with kde.themes.org but common sense would say if the server isnt up, maybe they cant aford the server.\n\nwhat i do know is i own a web site hosting / developement company, www.2GProductions.com\n\nif kde.themes.org needs a new home, id like to offer a account to them for free.\n\ncontact me if i can help\n\n-Mario"
    author: "mario"
---
Have you noticed that kde.themes.org has not been as active as everything else in the kde world? Apparently, they <a href="http://www.themes.org/php/comments.phtml?forum=news.1.86">need some help</a> to maintain the site. If you have "intimate knowledge of the KDE theme system" then you may be the person for the job. If you're interested, email <a href="mailto:cjr@themes.org">cjr</a> and <a href="mailto:kfc@themes.org">ElCoronel</a>.



<!--break-->
