---
title: "KDE2 Release Delayed One Week"
date:    2000-10-09
authors:
  - "numanee"
slug:    kde2-release-delayed-one-week
comments:
  - subject: "Strong move by the KDE team"
    date: 2000-10-09
    body: "Good descition by the KDE team to delay the release date instead of releasing unfinished software."
    author: "Mats Eriksson"
  - subject: "Re: Strong move by the KDE team"
    date: 2000-10-10
    body: "I guess there will be a lot of reviews in computer mags covering KDE2, esp. after all this gnome foundation stories, many of them will read like a contest between them. A buggy release *now* will do bad harm to KDEs reputation.\n\nAnd for future releases: Spread the beta releases\nwider, CVS is a good thing for active devolpers, but there should be an easier way for interested users to get a preview. Maybe there should be something like a monthly \"Linux for real man\" CD\ncontaining the latest snapshots of the kernel, KDE, gnome, gimp, ... but that's another topic.\n\nFrank"
    author: "Frank Hellmuth"
  - subject: "Re: Strong move by the KDE team"
    date: 2000-10-11
    body: "Indeed. Have been following KDE snapshots for more that 6 month (almost daily) and recent snaphots are worse than those released roughly around 12 September. I guess this depends on which apps you are using but khtml is not going entirely forward right now. It's fast but fonts are all f...ed up.\n\n/jarek"
    author: "Agree"
  - subject: "but does it run on a pentium?"
    date: 2000-10-09
    body: "KDE2 was ready for me, personally, when I got the latest debs working just fine on my P120 w/ 32M of RAM + crappy harddrive.  ;) I use it as my everyday desktop on that machine now.  Previously I had been switching back and forth between KDE 1.1.2 and Fvwm2. \n\nIt's handy having such a machine around...  makes a good benchmark.  hristo doichvev, you listening? Didn't get your email address...  :)\n\n(I have been using KDE2 for months on my Celery 400 w/ 64M, of course.)\n\nCheers,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: but does it run on a pentium?"
    date: 2000-10-09
    body: "You mean KDE2 has reasonable performance with 32Mb?\n\nI'm typing this on a 64Mb machine using KDE 1.94 (i.e. not RC1) and it is not all that fast.\n\nAt home KDE2 runs just great with >128Mb memory, but with this 64 Mb machine I can't but wonder who would run KDE2 with even less memory.\n\nCheers, Martijn"
    author: "Martijn Klingens"
  - subject: "Re: but does it run on a pentium?"
    date: 2000-10-09
    body: "I'm running KDE 2 (since August 31) as my primary Desktop Enviroment. It's good. It runs fairly well on 32 MB of RAM, at least on my PowerMac 4400/200. Some apps are still sluggish, but it's apparent it's getting better, as everytime I get new debs of it, apps keep launching faster, and in general it runs smoother. While having tones of RAM is cool, it's not at all neccessary to run KDE 2.\n\nExcept for slower app launch times (which are improving all the time), KDE 2 is as fast as KDE 1.1.2 on this machine."
    author: "AArthur"
  - subject: "Re: but does it run on a pentium?"
    date: 2000-10-09
    body: "Yes, it does.  It's a 8-bit color crappy video card too.  It takes a while initially to load (about the same as KDE 1.1.2) but once loaded I get reasonable performance and speed (about the same or better than KDE 1.1.2).  Enough to show off the various styles and themes and Konqueror.  ;)  I do have swap, though.  About 10-14M of it is used initially, if I recall correctly.  What's cool is that with konsole, I don't need tons of xterms/rxvts open anymore.\n\nI've been running KDE2 for months on my Celery 400 w/64M and I'm a bit puzzled about your impressions of slowness.  How does it compare to KDE 1.1.2?  Hmmm, I should benchmark the two.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: but does it run on a pentium?"
    date: 2000-10-10
    body: "Maybe it's because I'm running my server apps on my 'old' machine, so the slugginess may be my own fault.\nBut even if I close them for a while it's no fun to have multiple web pages open or some KOffice documents on a 64Mb box.\n\nAnyway, I haven't seen Linux with X perform even close to Windows 9x on 64Mb machines. Personally I don't care, because Linux is more stable, but many novice users don't. Also, it's the novices who are the majority of the 64Mb users and the novices at who KDE2 is targeted.\nI can only hope that the RCx and the final versions of KDE2 are better in that respect as my 1.94 version.\n\nCheers, Martijn"
    author: "Martijn Klingens"
  - subject: "Re: but does it run on a pentium?"
    date: 2000-10-09
    body: "Well, I'm running it on a P200 w64Mb RAM, and it feels significantly slower than 1.1.2."
    author: "Rikard Anglerud"
  - subject: "Re: but does it run on a pentium?"
    date: 2000-10-10
    body: "It was a real sloth on my Toshiba laptop. P100 w/32RAM.  EVERYTHING took a long time to load and screen redraws were painful. Of course that was due more to a low performance graphics card. I ended up taking it off and going back to 1.1.2."
    author: "Lawnman"
  - subject: "Re: but does it run on a pentium?"
    date: 2000-10-10
    body: "If you want to to have reasonable perfomance, you _must_ compile Qt with exception handling disabled (it's enabled by default). Add -fno-exceptions flag to CXXFLAGS."
    author: "fura"
  - subject: "Qt-Exceptions, was: Re: but does it run on a pe..."
    date: 2000-10-10
    body: "Is disableing the Qt-exceptions really a good idea? IMHO the exception-handling was bulit into Qt for some reason. May turning off the exceptions affect stability?"
    author: "Thrax"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a pe..."
    date: 2000-10-10
    body: "It has nothing to do with stability, but reduces memory usage a _lot_ (think about megabytes per single runing application).\n\nThat was the reason, why KDE 1.x.x is known as slow and bloated - developers didn't care a lot, and distributors were stupid enough to build both Qt and all of the KDE with exception handling enabled which made KDE unusable on boxes with less that 64MB of RAM. Cleverly built KDE1 runs great with 32MB of RAM ( actually I used KDE1 a lot on 486/24Mb box - worked perfectly). But you won't get correctly precompiled packages anyware - the only way is to grab the source and build it yourself.\n\nThe same applies to KDE2 - if you want to feel the real power of KDE2 - build it (and Qt) from the source, and do it correctly."
    author: "fura"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a p"
    date: 2000-10-10
    body: "Anyone knows if the Mandrake packages (KDE and Qt) are compiled with exceptions disabled?\n\nIf not, how do I compile KDE and Qt with exceptions disabled?"
    author: "Mats Eriksson"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a p"
    date: 2000-10-10
    body: "KDE2 is built with exception handling disabled by default (luckily), only khtml library, which uses exceptions is built with enabled exception handling, at least in theory. But I had problems when building shanpshots, and had to disable exception handling \"by force\" (on Bourne shell):\n\nCXXFLAGS='-O2 -fno-exceptions' ./configure\n\nTalking about Qt - it's very easy to see if Qt is compiled corectly simply checking the file size of the library. On my FreeBSD/i386 box file size of libqt.so.2.2.0 is ~5MB (thats with libpng and other stuff built in IIRC). If the size is more like 10MB, than you need to rebuild Qt (these numbers are for i386, libqt on Alphas should be much bigger ;-) ). The difference is ~5MB and most of these 5MB is _unshared_ and _unused_ code. So, with every KDE app runing you'll waste ~5MB of memory. If you run 10 applications, you waste 50MB.\n\nTo fix this nonsense,\nopen $QTDIR/configs/linux-g++-shared (or freebsd-g++-shared or whatever you use) with text editor and add \"-fno-exceptions\" to SYSCONF-CXXFLAGS. Then ./configure and make. \n\nThat's very easy and gives huge usability boost. \nRebuilt Qt should be even binary compatible (no need to rebuild apps depending on it)."
    author: "fura"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a p"
    date: 2000-10-10
    body: "This really gave my box(pp200 96m) a boast.\nBefore kde2 were slow unresponsive, kinda useless.\n\ntook 10 - 15 seconds to start kword now i takes 2.\nI Compiled with pgcc-2.95.2\nand added flags -mpentiumpro -fno-execptions to qt."
    author: "Leif"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a p"
    date: 2000-10-10
    body: "Sounds interesting. Does the KDE team have any response to this?"
    author: "Phator Que"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a p"
    date: 2000-10-11
    body: "Still waiting..."
    author: "Phator Que"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a p"
    date: 2000-10-12
    body: "I heard that Qt 2.2.2 will come with exceptions disabled."
    author: "David Faure"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a p"
    date: 2000-10-12
    body: "<p>\"<i>On my FreeBSD/i386 box file size of libqt.so.2.2.0 is ~5MB (thats with libpng and other stuff built in IIRC).</i>\"</p>\n<p>Great, that means <a href=\"http://www.slackware.com\">Slackware</a> builds the libraries correctly.</p>\n<tt>-rwxr-xr-x   1 root     root      5737500 Jun  4 13:25 libqt.so.2.1.1*</tt>\n<p>That's from the qt-2.1.1 package in the contrib dir for Slackware.  I'm assuming the 1.4.4 with KDE1 is also built correctly, as KDE 1 in Slackware has always been nice and fast for me.  I guess <b>some</b> distributions do get it right, because they care about speed and stability. :-)</p>"
    author: "Inoshiro"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a p"
    date: 2000-10-13
    body: "Anyone else find -O2 on xml/qxml.cpp made the compiler think for three minutes then finally die with an assembler error?  I compiled that one module by hand without -O2; hopefully that won't be a big hit.  (LinuxPPC 2000 + Helix GNOME 1.2, G4/400)"
    author: "rhd"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a pe..."
    date: 2000-10-10
    body: "That explains a lot to me!\n\nI don't like to compile each and every program I use, so I normally stick with binary rpm or tar files when they are available.\n\nI guess that's the reason that both KDE 1 and 2 run sluggish on low-memory configurations...\n\nThanks, Martijn"
    author: "Martijn Klingens"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a pe..."
    date: 2000-10-12
    body: "In a C++ program the usual way of handling error conditions is through exceptions. Unless KDE is written in a very unusual way, no exceptions means no error handling, just crashes.<P>\n\nIf KDE doesn't use exceptions to handle errors, what does it use?"
    author: "Michael S Czeiszperger"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a p"
    date: 2000-10-14
    body: "Exceptions tend to create quite a performance hit, both in code size and speed.\n<P>\nTake a look at this <A href=\"http://www.javaworld.com/javaworld/jw-04-1997/optimize/Benchmark_Results.html\">Java benchmark</A> for example. Good, taken your point that Java isn't C++, I guess it says <I>something</I> about exceptions (those guys at Sun aren't that stupid, right?)\n<P>\nSo I guess it must have to do with the idea of exceptions, not only with a crappy implementation.\n<P>\nOther methods to handle problems exist, like for example those often used in plain C. Examples are: returning error codes, solving the problem at the place where it occurs (not always possible) or setting global error flags. Whatever you like..."
    author: "Wilke Havinga"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a p"
    date: 2000-10-16
    body: "C++ extensions when they are thrown are slower than just normal code, which is why you just use them for errors. In my 12 years of using profilers with C++, I've never seen a speed increase just by turning off exceptions.\n\nThe way you're supposed to deal with exceptions and performance, is to not use them critical performance areas. Since the events that trigger exceptions are supposed to be \"exceptional\", this isn't problem. Exceptions are only supposed to be thrown when there's an error, so there isn't a performance problem.\n\nAll this being said, the reason I posted was that if the program is written as a modern C++ program, it definitely will use exceptions for error handling, so what happens to error handling when you turn off extensions?"
    author: "Michael Czeiszperger"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a p"
    date: 2000-10-24
    body: "Actually, \"new\" can throw an exception if it runs out of memory. Many people consider that \"new\" can be called in a performance critical section."
    author: "S. Loisel"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a p"
    date: 2000-10-24
    body: "\"Performance-critical\" sections are usually segments of code that are being called repeatedly. One of the first things anyone should look at in code like this is whether or not they are allocating any new memory. When possible, a really optimized piece of code will not allocate any new memory, but re-use existing memory."
    author: "Foogle"
  - subject: "Re: Qt-Exceptions, was: Re: but does it run on a pe..."
    date: 2004-03-27
    body: "i got error while executing make command in qt\n\n for ex : \n   qmake simple-dialog.pro\n   \n\n   then \n\n\n    make command\n\n   it is giving error as\n\n   undefined reference to 'main'\n\nplease let me know the solution of the above problem..\n\nthank u"
    author: "anand"
  - subject: "Re: but does it run on a pentium?"
    date: 2000-10-11
    body: "THANK YOU!!\n\nI was already thinking that KDE2 is nifty, but with the 25% - 50% speed boost that I got by following your post, I'm awestruck. On my old PII-300, it makes a world of difference. Now it really is better _and_ faster than KDE1 (and others).\n\nMaybe this should be included in the build instructions on kde's website. I've also seen a lot of speed complaints on comp.windows.x.kde. Maybe a post there would be a good idea, if someone hasn't already done so (haven't checked in a while).\n\nThanks again,\nDave"
    author: "d721970"
  - subject: "Educating package makers"
    date: 2000-10-11
    body: "This thread is very interesting. I tried to compile\nKDE 2 myself but since my environment is so complex\nit just didn't work. So I installed the RPM's.\n\nIt works fine on my computer but not great (I have\na duel PIII 500 with 256mb and Matrox G400).\n\nMy point is... Shouldn't the KDE PR team offer tips\nfor distribution vendors on how to build the package\ncorrectly or possibly make the release builds into\nthe defaults?"
    author: "Shai"
  - subject: "Re: Educating package makers"
    date: 2000-10-11
    body: "...and get some information from the KDE team how the current packages are built..."
    author: "Phator Que"
  - subject: "Re: KDE2 Release Delayed One Week"
    date: 2000-10-09
    body: "Great!  :)\n\nKDE2 is not finished yet, there's still a lot of bugs that needs to be removed... :)\n\nThis was a smart decision!"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: KDE2 Release Delayed One Week"
    date: 2000-10-09
    body: "I agree 100% on the decision to hold it back a week. It's stable for me, but I'd rather be safe than sorry. One of the things I love about KDE is the \"when it's done\" attitude, an embarrasing but in KDE2 would be really bad..."
    author: "jorge"
  - subject: "Re: KDE2 Release Delayed One Week"
    date: 2000-10-09
    body: "I really appreciate this decision -- I am currently using the acutal CVS version\nand it is really great and stable. However, some of the functionality is flawed.\nWhen you use it on a daily basis, you get used to it and feel that it is \"acceptable\",\nbut in fact -- it isn\u00b4t. I think this is a clever decision not because actually KDE2 \nis not stable, but just because it is so stable that every single error has the potential\nto spoil the impression.\n<p>\nSo I encourage every developer to finish their great work and fix the last bugs...\n<p>\n... looking forward to the release,\n<p>\nSven\n<p>\nPS: Never mind... have a look at the kernel... they are delayed, too, and for good\nreasons as well. Thank god we're not in the corporate world and don't have no\ndeadlines!"
    author: "Sven Niedner"
  - subject: "Re: KDE2 Release Delayed One Week"
    date: 2000-10-09
    body: "Well, KDE 2.0 is not the end of the line whatever happens.  KDE2 will only be delayed for real show stoppers.  No release can be perfect, no matter what users think or expect but things can only get better from there.  ;)  \n\nHere's to more incremental releases, more early and more often and not another repeat of the KDE1->KDE2 transition phase.  \n\nJust MHO.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: KDE2 Release Delayed One Week"
    date: 2000-10-10
    body: "OK -- on the one hand, yes. But when there\nare GUI elements that do not function as\npromised, and when certain features do not\nwork reliably, we shouldn't even call it \na \".0\"-release, but a beta. I have strong\nfeeling about keeping up the quality standards\nof free software, where a beta is often more\nstable than an off-the-shelf commercial \nproduct. <b>The KDE release should try not\nto ruin this reputation.</b><p>\n(I admit that a complete GUI has more \npossibilities for errors going undetected\nthan, say, a compiler or LaTeX, and it is\na relatively new project.)\nSo, my opinion: Fix the last bugs, IMHO there\nare very few which are really important, and\nthen draw a line and ship. (However, drawing\na line becomes more difficult on larger \nproject - this was my remark on the Linux kernel.)"
    author: "Sven Niedner"
  - subject: "Re: KDE2 Release Delayed One Week"
    date: 2000-10-09
    body: "i agree that decision too.\n\nBut is there any stress test application to crash proof the api and functionality ?\n\ni mean a tool like in Winblows that make random calls to kde's api and log errors."
    author: "Massimo"
  - subject: "Re: KDE2 Release Delayed One Week"
    date: 2000-10-10
    body: "\"But is there any stress test application to crash proof the api and functionality ?\"\n\nYeah, lots and lots of users. :-)"
    author: "J. J. Ramsey"
  - subject: "Re: KDE2 Release Delayed One Week"
    date: 2000-10-10
    body: "Sorry asking, but what are those tools and where I can get 'em?"
    author: "Reza Prime"
  - subject: "There are plenty of test tools on Unix"
    date: 2000-10-10
    body: "but I bet they haven't been used too extensively. test frameworks like deja-gnu are old but for KDE probably some perl or pythong scripts could exercise things a bit :-)\n\nI'd like to see KDE thrown throug purify and other commercial profiling tools etc. too.\n\nAs for the users being the testing tool - there haven't been enough of them. **Especially** for KOffice which has no documentation (the idea I guess is you just use MS Office and then switch over to the less stable and buggy KOffice and use the same \"instinctive approach\" or something). Sorry KOffice is a great project and all that and maybe in 5 years **if people use it** it will be solid and low on bugs and maybe even have good searchable manual etc.\n\nThe problem is not very many people are going to use it so many bugs will remain ... Emacs got so stable because a ton of motivated programmers used it alot. I'm not sure KOffice will get the same treatment (look at the gimp a 5 year old project barely out of verion 1.0 and not too polished).\n\nThe main thing KDE has going for it is Qt which is a *fantastic* and well documented piece of technology - kudos to Troll (too bad for me it's C++ which I hate :-P perhaps Troll could take over GNUStep or GTK now?). KDE is the best test of Qt there is around !!"
    author: "Abs"
  - subject: "Re: KDE2 Release Delayed One Week"
    date: 2000-10-09
    body: "GO FURTHER KDE TEAM!!!!!!!!!\n\nIt's a great decision. Now, is very stable, but in some cases.... KDE 2 is a little crazy."
    author: "Manuel Rom\u00e1n"
  - subject: "Gee what a lot of precise \"bug reports\""
    date: 2000-10-10
    body: "If the comments on this page are any indication I hope you people aren't filing bug reports. What exactly is \"a little crazy\", or \"not finished\" or \"GUI elements that don't work\"? Where? \n\nIf you *do* file bug reports try to be a little more accurate and precise."
    author: "AC"
  - subject: "Wrong URL"
    date: 2000-10-10
    body: "It should be something under:\n\nhttp://ftp.de.kde.org/pub/KDE/unstable/distribution/"
    author: "AC"
  - subject: "Re: Wrong URL"
    date: 2000-10-10
    body: "Thanks, we'll fix it once there's an actual URL / public release.  Packagers working on the binaries, as I understand it.\n\n-N."
    author: "Navindra Umanee"
  - subject: "FreeBSD Packages?"
    date: 2000-10-10
    body: "I have noticed that my FreeBSD 1.94 machine has some bugs that the Mandrake 1.94 didn't have, does anyone know if there are newer binary packages for FreeBSD to test?\n\nI've sent in a couple of bugs wrt 1.94 on BSD, and really want to help to make 2.0 run well on BSD...\n\n(Alternatively, does anyone know how to make the 1.94 port work with newer builds?)\n\nThanks,\n\nBen"
    author: "benmhall"
  - subject: "Re: KDE2 Release Delayed One Week"
    date: 2000-10-10
    body: "<B>Thank god.</B>\n<P>I can sleep well now ;)"
    author: "Sm\u00e1ri P. McCartyh"
  - subject: "Slooooooow"
    date: 2000-10-10
    body: "KDE 2 (including apps) runs *much* slower than KDE 1 or Gnome 1.2 on my machine (Pentium 166 MMX with 48 MB RAM)\n\nI think it's a bit too integrated ;-)\nAll that DCOP stuff will only slow down things, and I really don't see why every KDE 2 app use DCOP.\nIsn't it just a communication protocol like CORBA or something?\nWhy does an editor have to communicate with other programs?\n\nBTW is there any way to disable the one-click-to-do-it-all \"feature\" ala Winblows 98?\nI hate it."
    author: "Anonymous"
  - subject: "Re: Slooooooow"
    date: 2000-10-10
    body: "Please, read another thread about exception handling code."
    author: "fura"
  - subject: "Re: Slooooooow"
    date: 2000-10-10
    body: "It doesn't, apps only use DCOP when communicating with other components. If you think DCOP is slow, have you tried programming with a CORBA ORB(probably not when making the statement above)? DCOP is _REALLY_ light-weighted.\n\n--\nAndreas"
    author: "Andreas Joseph Krogh"
  - subject: "Not true"
    date: 2000-10-11
    body: "Gnome's Bonobo components here are *much* faster than DCOP.\nORBit (Gnome's CORBA implementation) is also much faster than DCOP.\nTo my experience, they are 2 times faster than DCOP.\n\nCORBA is not slow.\nOr at least, the Gnome implementation is not slow."
    author: "Anonymous"
  - subject: "Re: Slooooooow"
    date: 2000-10-10
    body: "Hey, you're right!\n\nI tried to run KDE2 as my all-day-working environment. It is stable enough for that task, but it is SO SLOW on my machine (P200MMX with 48 megs of RAM). On my machine in the office with 128 Megs and 450Mhz it's still really slower than KDE1 and GNOME. Is there any clue to improve speed or do i have to invest some money into RAM?\n\ncu\nManuel"
    author: "Manuel Zamora"
  - subject: "Re: Slooooooow"
    date: 2000-10-11
    body: "Hmm, one thing that makes the KDE2 beta's slow is the debugging code... Does your version of KDE2 use include debugging??\n\nRinse"
    author: "Rinse"
  - subject: "Re: Slooooooow"
    date: 2000-10-11
    body: "I am not a coder --- I used the official binaries.\n\nDo they include debug-code? How do I \"strip\" the debug-code from the sources? Is there any ./configure - option?\n\nManuel"
    author: "Manuel Zamora"
  - subject: "Get rid of debug code!"
    date: 2000-10-11
    body: "\"./configure --disable-debug\" is a must!  You might also want --enable-final!"
    author: "ac"
  - subject: "Re: Get rid of debug code!"
    date: 2003-04-29
    body: "this debuging thing is really on my nerves and sometimes it kick me off the internet while I was in a middle of something.Somethimes it Ican't do nothing about it that I need to get off the internet.I want this to stop as soon asI can find out a way.please help me!\n"
    author: "hanh"
  - subject: "Re: Get rid of debug code!"
    date: 2006-01-15
    body: "i got told that i had to reformat my computer but i dont no hot to do i am only 15 years old"
    author: "anthony william glean zielke"
  - subject: "Re: Slooooooow"
    date: 2000-10-11
    body: "If you don't want to recompile KDE, try a 'strip'\nType \"man strip\" to know more about it.\nOh, and be carreful, do not use it when KDE runs as it could crashs KDE, ... and have some very bad consequences on the whole system.\nYou can use this command to strip all the a.out and elf binaries (Imagine the glibc, before strip : 87MB, and after strip : 16MB :))) but DO IT ONLY FROM A RESCUE DISK WHERE EVERYTHINK IS LOADED IN A RAMDISK AND NOTHING FROM THE SYSTEM TO STRIP WAS LOADED!!!! I had bad experiences, because I didnt do it :(("
    author: "Thierry GRAUSS"
  - subject: "Re: Slooooooow"
    date: 2000-10-11
    body: "You don't need to invest into RAM. Simply you should not get precompiled binary packages (as packagers are plain stupid), but grab the source of Qt and KDE2 and build it yourself with exception handling disabled. Please, read the thread about Qt exceptions, to find instructions how to do a correct build ."
    author: "fura"
  - subject: "Re: Slooooooow"
    date: 2000-10-11
    body: "Thanx! Will try that.\n\nManuel"
    author: "Manuel Zamora"
  - subject: "KDE2.0-rc2 rpms for redhat 7.0"
    date: 2000-10-11
    body: "OK, I have been trying for days to compile the\ncurrent snapshots on redhat 7.0 only to find that the distribution gcc is a \"development\" version and that I should use gcc 2.95.2. But this gcc won't compile the c++ libraries because it is \"incompatible\" with the glibc shipped with redhat 7.0 (2.1.94 as I write). \n\nHow did you manage to compile the 2.0r2.rpm's for redhat-7.0?\n\nI am intrigued."
    author: "edscott wilson garcia"
---
Due to the popular sentiment that KDE 2.0 is not quite ready for prime time, Matthias Elter, the release coordinator, has <a href="http://lists.kde.org/?l=kde-core-devel&m=97109574800041&w=2">announced</a> that a second release candidate is being prepared.  RC2 will undergo scrutiny and testing by developers and packagers alike so that those final showstoppers can be found and squashed.  KDE 2.0 final tarballs have been delayed one week and are now scheduled to be released to packagers on the 16th, with a public release of the packages on October 23rd. <b>Update 10/9 3:15 PM</b> by <b><a href="mailto:pour@kde.com">D</a></b>: We've received word from Matthias that the RC2 tarballs will be released tomorrow, Oct. 10, on the KDE ftp servers, most likely <A HREF="http://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/">here</A>.  Stay tuned -- we'll give you the details when we get them.












<!--break-->
