---
title: "Interview with Matthias Ettrich"
date:    2000-09-14
authors:
  - "numanee"
slug:    interview-matthias-ettrich
---
In this first edition of "The People Behind KDE", we get to learn a little more about KDE founder <a href="http://www.kde.org/people/matthias.html">Matthias Ettrich</a>.

<!--break-->
