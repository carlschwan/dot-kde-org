---
title: "Linux Online: Interview with Shawn Gordon"
date:    2000-10-15
authors:
  - "numanee"
slug:    linux-online-interview-shawn-gordon
comments:
  - subject: "Re: Linux Online: Interview with Shawn Gordon"
    date: 2000-10-15
    body: "Shawn Gordon is a wonderful, beautiful person."
    author: "John Kintree"
  - subject: "Re: Linux Online: Interview with Shawn Gordon"
    date: 2000-10-16
    body: "I think what theKompany is doing is a wonderful thing. I hope all KDE developers include their efforts in current and future projects - this is a wonderful example of how KDE can create marketshare for corporate IT houses, and still give back to the community.\n\nAs far as I know, doesn't Powerplant include the full KDE distribution? And will have an update agent similar to Helix Update?"
    author: "Eron Lloyd"
  - subject: "Re: Linux Online: Interview with Shawn Gordon"
    date: 2000-10-16
    body: "First off, thanks for the kind words, they are appreciated.\n\nPowerPlant does include a full KDE compliment, but it's from 1.92.  By subscribing to the PowerPlant Developer Network you get updates on anything in the package at anytime, as well as new stuff.  I haven't looked at the Helix update, but I've read about it, and there is some overlap of concept."
    author: "Shawn Gordon"
  - subject: "Re: Linux Online: Interview with Shawn Gordon"
    date: 2000-10-16
    body: "PowerPlant includes two of the KDE 2 beta versions (the ones available at the time we released PP) as well as a KDE cvs snapshot.\n<p>Also, PP comes with it's own RPM installer  - Magnum ][ ( there's also a \"Magnum I\" on the PP CD). The nice thing about the install is that it has both a GUI (Qt-based) and a TUI (TVision-based), so you don't need X to use Magnum.\n<p>And BTW, you may see some screenshots of Magnum in action at <a href=\"http://www.thekompany.com/products/powerplant/magnum/\">http://www.thekompany.com/products/powerplant/magnum/</a> .\n<p>I'm working right now on the Magnum ][ Update, which will feature RPM updates via the Internet (ftp and http), better and nicer UI, RPM repository management, and many, many other small improvements.\n<p><p>\nxxL"
    author: "Catalin Climov"
  - subject: "Re: Linux Online: Interview with Shawn Gordon"
    date: 2000-10-16
    body: "So do you think a new product could be in the works for \"end users\"? Like a KDE desktop with applications and utilities? That could employ Magnum][Update to manage upgrades, etc. (very similar to other well-known desktop environments). I see this as a Good Think for KDE and theKompany, as well as anyone else that would be part of it. Especially for end users, I'm sure a company that provides an easy way to get KDE and update it would give it an even better reason as the choice for desktop users. Some people don't need the wealth of tools PowerPlant provides, but wouldn't mind paying $25-30 for a good KDE desktop CD, maybe with a copy of Opera web browser included (hint, hint), KOffice, and theKompany applications like Kivio and Magellan. But I guess you already have plans for your next products ;)"
    author: "Eron Lloyd"
---
<a href="http://www.linux.org/">LinuxOnline</a> is currently featuring an interesting <a href="http://www.linux.org/people/gordon.html">interview</a> with Shawn Gordon of <a href="http://theKompany.com/">theKompany.com</a>. 

<i>"Everyone I've talked to is amazed that we are able to make this work. theKompany is     spread across 11 time zones at the moment. We have people in England, Germany, Italy, Russia and Romania, with people in the Ukraine and Thailand we are looking to hire shortly. Overall it works very well, but we do have enclaves of people in Moscow and Iasi Romania, we are going to open an office in Iasi soon for the 5 people that will be working there. The advantages are a lower overhead cost for me. It's not like we intentionally looked at other countries, we just looked for good people, and that's just where they lived."</i>

<!--break-->
