---
title: "Kaim preview release"
date:    2000-09-28
authors:
  - "bmeyer"
slug:    kaim-preview-release
comments:
  - subject: "Re: Kaim preview release"
    date: 2000-09-28
    body: "A couple other things to mention:\n\n<BR>1. We are looking for packagers. I'm gonna try my hand at debs, but if anyone wants to help with rpms, or debs, or anything else, it would be awesome. \n\n<BR>2. If anyone experiences a bug where Kaim will not connect but instead hangs on \"waiting for authorization\" **please** report it. I seem to be the only one experiencing it, and I'd like to know that I'm not completely crazy. :)\n\n<BR>Thanks\n<BR>Erik Severinghaus\n<BR>ezs@email.unc.edu\n<BR>kaim developer (off and on) :)"
    author: "Erik Severinghaus"
  - subject: "Re: Kaim preview release"
    date: 2000-09-28
    body: "I have been working on the RH 6.2 rpms for KDE 2, so I can make rpms for rh62 if you would like. \n\nI am going to release new KDE2 rpms from the latest CVS sources soon.  These rpms will require openssl >= 0.95a which will be uploaded to ftp.kde.org also."
    author: "Robert Williams"
  - subject: "Re: Kaim preview release"
    date: 2000-09-29
    body: "Does anyone know how kaim compares to kit?"
    author: "Trebor A. Rude"
  - subject: "Re: Kaim preview release"
    date: 2000-09-30
    body: "I can't help but respond...\n\nWhether Kaim or Kit suits your needs depends on your tastes in UI.  Kaim is a lot like your typical AIM client, while Kit is nothing like that at all.  Kit's main window borrows ideas from kicq, and the chat window is my own \"design.\"\n\nI personally can't stand the typical AIM interface.  That's why Kit exists in the first place.  But, if you find yourself annoyed at Kit, try Kaim.   You might like it, now that it's getting stable."
    author: "Neil Stevens"
---
The preview release for Kaim has just been posted up on the on the <A HREF="http://www.csh.rit.edu/~benjamin/benjamin/works/kaim/">Kaim homepage</A>.
   Kaim is an AOL instant messenger.  Along with having many other <a href="http://www.csh.rit.edu/~benjamin/benjamin/works/kaim/info.html">features</a>, Kaim's main jewel is its <a href="http://www.csh.rit.edu/~benjamin/benjamin/works/kaim/screenshots.html">chat window</a> which is fully configurable, functional, and filled with a ton of features.  After 6 months in the works the Kaim team is happy to announce this release.





<!--break-->
