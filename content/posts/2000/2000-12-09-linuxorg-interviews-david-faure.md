---
title: "Linux.org interviews David Faure"
date:    2000-12-09
authors:
  - "rmoore"
slug:    linuxorg-interviews-david-faure
---
<b>Linux.org</b> has interviewed <i>David Faure</i>. David talks about KDE 2, Konqueror and the future of KDE and Linux. This is a <a href="http://www.linux.org/people/faure.html">nice reading</a>.


<!--break-->
