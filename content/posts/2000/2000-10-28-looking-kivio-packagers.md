---
title: "Looking for Kivio Packagers"
date:    2000-10-28
authors:
  - "sgordon"
slug:    looking-kivio-packagers
comments:
  - subject: "Re: Looking for Kivio Packagers"
    date: 2000-10-27
    body: "Source code is already great! Thanks...\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Looking for Kivio Packagers"
    date: 2000-10-28
    body: "The binary-only release really bugged me...\n\nThanks not only for the source, but for Slackware support."
    author: "Neil Stevens"
  - subject: "Re: Looking for Kivio Packagers"
    date: 2000-10-28
    body: "We've had slackware up for a week now, but it was binary.\n\nI will be curious if all the people that insisted we open the source understand that open or free is meant as in speech, not as in beer.  I think a lot of people have started to think they are entitled to whatever they want at no cost, and not even RMS thinks that is true.\n\nI plan to open source many of our offerings, and the reaction from some members of the community demanding that we give them the source was rather disturbing.  I obviously can't support a company that produces quality products without making money somehow.  I hope that that the community will support us as we are supporting them."
    author: "Shawn Gordon"
  - subject: "Re: Looking for Kivio Packagers"
    date: 2000-10-28
    body: "Look at it from a customer perspective:\n\nGiven a choice between a completely open package, which gives me total control, and a proprietary package, which gives my vendor control (especially if UCITA passes), which should I choose?\n\nAll else being equal, the open package will win, because the custmer is in control.  That's why I insist on open source for anything I rely upon.  Low sticker prices are just a beneficial side-effect."
    author: "Neil Stevens"
  - subject: "Re: Looking for Kivio Packagers"
    date: 2000-10-28
    body: "I think another issue that got community members riled specifically with kivia was that there was no mention of kivio's licensing terms anywhere on its page.  You were giving away a free binary of the package, but you didn't mention if it was eventually intended to be open sourced or not.  \n\nI think it \"frightens and confuses\" many free (as in speech) software advocates when you give away binaries of a linux app for free, since you're not benefitting on either end.  Knowing licensing terms, open or not, for the software they use is important to many community members. I think many \"insisted\" (as you say) that you open the source because, to them, it makes no sense to give away a linux app without freeing its source.  \n\nIf, instead of just giving away the binary, you had advertised it as a free demo of the upcoming, closed-source full version (available for a reasonable fee), you wouldn't have had such an outcry.  Likewise, if you had claimed that you were releasing this as a free binary, with the intent of releasing the source in the near future after internal issues had been cleared, most would have waited with no complaint.  No claims to either (or any) licensing scenario only caused frustration.\n\nPersonally, I'm all for you setting aside as many as your products as you want to sell as closed-source binaries.  I'd just prefer to know which licensing strategy you're taking with an app up front.  The kivio pages offered no such information (and still doesn't, at least not that I can find on the site)."
    author: "ac"
  - subject: "Re: Looking for Kivio Packagers"
    date: 2000-10-28
    body: "Valid points.  We originally had a GPL notice up on the web page, but we weren't sure if that's what we wanted, so we pulled it down.  Unfortunatly we don't have as many employees as we would like to have to handle everything and we tend to focus on the programming.  This was probably a good learning experience, and things should go smoother in the future.\n\nWe are updating the web pages over the next couple of days to coincide with the next release, there will be a short tutorial up as well.  Thank you for your input."
    author: "Shawn Gordon"
  - subject: "Re: Looking for Kivio Packagers"
    date: 2000-10-28
    body: "Shawn -- if there's one thing the KDE world should have learned by now, it's that ambiguities and questions about licensing create PR nightmares that will affect mindshare for years to come. \n\nI respect that you guys are more interested in coding than in marketing and licenses. I'm much more impressed by that than by companies that are more interested in pandering to 19 year old loudmouths on Slashdot than in making software. But I'd strongly urge you to decide what policy you want to make and work out the details before you release, maybe getting Bruce Perens or RMS to sign off on your plan, instead of setting off yet another flamefest.\n\nI know you have good intentions. But that didn't help the KDE project when things got ugly and it won't help you.\n\nOh, and I volunteer to take a shot at a LinuxPPC package ;-)"
    author: "Otter"
  - subject: "Re: Looking for Kivio Packagers"
    date: 2000-10-28
    body: "Shawn,\n\nNobody \"insisted\" that you open the source. I remember some people asked you for licensing\nclarifications.\n\nBTW, you have the right to keep your source private if you choose. If you release a well-supported, useful app for Linux for which there is no free alternative, people will pay for it. You should not feel pressured to open up the source as long as you are not in violation of any licenses. \n\nGood luck with your Kompany.\n\nMagnus."
    author: "Magnus Pym"
  - subject: "Re: Looking for Kivio Packagers"
    date: 2000-10-29
    body: "You didn't see the private emails I got :), some people got very rude and insistant, it's a little disheartening and it almost made me change my mind and keep it closed, but all the good words from the other people kept us going."
    author: "Shawn Gordon"
  - subject: "Mandrake is covered"
    date: 2000-10-28
    body: "Just to let you all know, Chris Molnar from Mandrake has volunteered to take care of the Mandrake packages, so everyone that was requesting them, you are now covered :)."
    author: "Shawn Gordon"
  - subject: "Re: Looking for Kivio Packagers"
    date: 2000-10-28
    body: "If Ivan will not find time for packaging Kivio, I will try to package it myself."
    author: "Hasso Tepper"
  - subject: "Re: Looking for Kivio Packagers"
    date: 2000-10-28
    body: "Doh, I'll try to package Kivio for _Debian_ of course. :))"
    author: "Hasso Tepper"
  - subject: "Re: Looking for Kivio Packagers"
    date: 2000-10-28
    body: "If you are not a Debian maintainer, I'd be willing to sponsor your package."
    author: "Jaldhar H. Vyas"
  - subject: "If you are volunterring to package"
    date: 2000-10-29
    body: "Please write to us at support@thekompany.com so we make sure we have everyone.\n\nthanks,\nshawn"
    author: "Shawn Gordon"
  - subject: "Debian is now covered"
    date: 2000-10-29
    body: "Jaldhar Vyas has graciously volunteered to take care of the DEB packages for us, and even said he would help beyond just Kivio.  Thank you so much for your support Jaldhar."
    author: "Shawn Gordon"
---
We will be releasing an update of <a href="http://www.thekompany.com/projects/kivio/index.php3?dhtml_ok=1">Kivio</a> early next week that also includes the source.  We have been getting a lot of requests for packages for systems that we just don't have access to.  We will be creating RPM's for RH 6.2 and 7.0 as well as a DEB by using Alien (DEB packagers are welcome).  We will also supply tarballs for Slackware 7.1.  If you want to see support for other environments, then please write to us at <a href="mailto:support@thekompany.com">support@thekompany.com</a> and let us know so we can make them available to everyone.  Please note that support for unofficial packages is left up to the packager.
<!--break-->
