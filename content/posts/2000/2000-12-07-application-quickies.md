---
title: "Application Quickies"
date:    2000-12-07
authors:
  - "Dre"
slug:    application-quickies
comments:
  - subject: "Re: Application Quickies"
    date: 2000-12-07
    body: "I'd want to thank Hans Dijkema for his excellent work with KmailCvt, and the support he gave me when i decided to get rid of the dreaded OE! :))\nThanks again, Hans!"
    author: "Andrea Cascio"
  - subject: "Re: Application Quickies"
    date: 2000-12-07
    body: "I'd like to thank Mr. Gordon and theKompany.com for their contributions. I wish them well in their future endeavours!"
    author: "kdeFan"
---
It's time for a bag of quickies focusing on application news.
<A HREF="mailto:shawn@thekompany.com">Shawn Gordon</A> informs
us that <A HREF="http://www.thekompany.com/">theKompany.com</A> has moved
<A HREF="http://dot.kde.org/973614369/">Kivio</A> to KDE CVS as part of the
<A HREF="http://koffice.kde.org/">KOffice</A> package.  This is a great
addition to the KOffice suite.  theKompany also recently released
version 2 of
<A HREF="http://www.thekompany.com/projects/kdestudio/">KDEStudio</A>, an
integrated development environment for KDE.
<A HREF="mailto:h.dijkema@hum.org">Hans Dijkema</A> tells
us that <A HREF="http://www.hum.org/kmailcvt.html">KMailCvt</A> is now
part of the <A HREF="http://apps.kde.com/infofr.php?id=464">kdenetwork</A>
package.  Finally,
<A HREF="http://www.csh.rit.edu/~benjamin/benjamin/works/kaim/">Kaim</A> 0.50
has been released, the first stable release of the Instant Messenger client.

<!--break-->
