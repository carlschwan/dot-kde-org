---
title: "The Other Media Player"
date:    2000-09-30
authors:
  - "csamuels"
slug:    other-media-player
comments:
  - subject: "Re: The Other Media Player"
    date: 2000-09-30
    body: "Finally, more details on this mysterious media player!  Cool.  I see it has been removed from kdemultimedia.  Can't it make KDE 2.0.1 or does a lot remain to be done?\n\nCheers,\nNavin.\n\nPS We're still wondering about the name...  :)"
    author: "Navindra Umanee"
  - subject: "Re: The Other Media Player"
    date: 2000-09-30
    body: "Yes! The mysterious media player :)\n\nWell, Yes, a lot remains to be done: Effect support, skin loading and Visualization.\n\nIt'd be nice to release the current copy with 2.0.1, but that version cannot add translations, much less any new programs.  But for 2.1, it will rock.\n\nOh, and the name? Well, there's a whole story to it, and I tend to just leave it as mystery :)"
    author: "Charles Samuels"
  - subject: "Re: The Other Media Player"
    date: 2000-09-30
    body: "Stable ? Usable ?\nLast time I tried to use this thing, it dumped core every time. \nI still can't understand, why do you want to compete with XMMS, whose mpg123 player library is pretty useable, stable, fast and GPLed ?\nOr you just want to get rid of libgtk.so.1.2 ?\nReinventing the wheel is probably interesting task, but mostly useless from global point of view..."
    author: "Someone from the Net"
  - subject: "Re: The Other Media Player"
    date: 2000-09-30
    body: "XMMS may be nice, but as you have said, it's not a true KDE app, and it duplicates functionality (which wastes memory and creates more overheead), with KDE 2. KDE 2, has native Mp3 support built into aRts, so using a third party decoder would be silly. \n\nSecond, XMMS is not nearly as modular as noauton, and as you said uses gtk+. These means when you start XMMS, you have to load a bunch of stuff you really don't need (including artsdsp esd if you want to play music in XMMS the same time as KDE). \n\nXMMS certainly has a few KDE specfic features, like supporting xdnd in the song list (along with KDE 1 dnd). But it's not a real KDE app."
    author: "AArthur"
  - subject: "Re: The Other Media Player"
    date: 2000-10-01
    body: "I had the same stability problem until I updated my out-of-date kdemultimedia.  I think arts just segfaults if a bogus/no mp3 arts plugin is found.\n\nBoth kaiman and noatun work great now."
    author: "ac"
  - subject: "Cool"
    date: 2000-09-30
    body: "This thing looks pretty cool!\nI've got to have it when it's finished."
    author: "A user"
  - subject: "Re: The Other Media Player"
    date: 2000-09-30
    body: "I guess variety and competition is good, but I have to say this project has a lot to do to make itself so insanely great that it will actually overcome the inertia of wanting to just stay with XMMS.  I am extremely happy with XMMS, its architecture, stability, and plugin availability (I have a plugin that uses my keyboard lights (caps, num and scroll locks) as a meter -- wicked cool :), one that accelerates mp3 decoding by utilizing 3dNow! multitudes of effects, visualization, input, general and output plugins and many skins -- this is a lot to duplicate).  Obviously this project has moved so far along now that it has its own character, and thus it shouldn't be snuffed out... but IMHO the ideal KDE ultimate multimedia system would merely be an effort to liberate XMMS from its toolkit dependancies thus allowing for a KDE or gtk+ native player... but I think this is even unnecessary because as long as I can drag files from konqueror to XMMS and have it work ... I'm happy *shrug*."
    author: "the_1000th_Monkey"
  - subject: "i18n"
    date: 2000-09-30
    body: "What's with the Norwegian website translation? Seems like babelfish.altavista.com or something ;)"
    author: "Haakon Nilsen"
  - subject: "Re: The Other Media Player"
    date: 2000-09-30
    body: "Ahh, excuse me, but I really don't understand one thing..\n\nIf KDE 2.0 supports MP3 playing built in, and we got KAction to play some Video formats (mpeg 1 etc) - then why do we need this?\n\nI mean - sure, it's nice - but I already seen couple of those MP3 players with skin selections. Nothing new with this one..\n\nWhat I really hope is that someone will add MPEG-2 playing capability to the KDE 2.0 itself..\n\nHetz"
    author: "Hetz"
  - subject: "Why does the open source community waste so much e"
    date: 2000-09-30
    body: "Why do open source programmers insist on wasting their time by producing fifteen competing versions of the same thing? Xmms is the best media player there is, and it really doesn't make sense to write another one from scratch.\n\nIt's not like the closed source world where there are reasons for producing hundreds of apps that do the same thing; here collaboration is essential.\n\nThis player will have to be compatible with all of XMMS' plugins and skins or will fail. In addition, it would be nice if it reused as much of the XMMS code as possible (or collaborated with that project); this way people don't waste as much time and improvements can be made to both packages."
    author: "Matthew Brealey"
  - subject: "Re: Why does the open source community waste so much e"
    date: 2000-09-30
    body: "How is XMMS the best player available?  As far as I can tell, it plays media formats, just like any other media player.  Nothing especially great about that.  However, it is ugly.  You can only use rectangular skins with xmms (well, there's that kjofol plugin that acts as a remote; that's not handy).  Noatun and Kaiman both support shaped skins.  Plus the obvious fact that xmms isn't a KDE app.  Some of us prefer KDE apps, especially over gtk ones."
    author: "Chris"
  - subject: "Re: The Other Media Player"
    date: 2000-09-30
    body: "What about Kaiman? It is part of Kde2, it is stable, it is skinable... Why duplicating programs?"
    author: "Zeljko Vukman"
  - subject: "Re: The Other Media Player"
    date: 2000-09-30
    body: "Yes, this is a lot of duplicated effort, but here I go trying to explain myself:\n<br>\n<ul>\n<li> I want to dump gtk+.so, GTK programs should have nothing to do with _my_ desktop.\n<li> I've hated the skin format ever since my days of windows.  But it's the only media player I know that supports effects and such.\n<li> Yes, it does have a long way to go, but the thing is totally modular-- the simple QT-ish user-interface that you see is a .so, and is in no way required to be loaded. (and you can even load another one at the same time)\n<li> For how extensive XMMS, I have even longer to go, with supporting plugins left-and-right; but you also forget that the vast majority of users don't want a VU for their keyboard LEDs and a 640x480 rectangle of dancing pixels.  Most are like me, they want to play their music, maybe add some special effects, and get on with their work (in my case, coding noatun :)\n<li> Kaiman already is stable and has skin support... but not much else.\n<ul>\n<br>\n(oh, and the Norwegian translation shows you how well I know it ;)"
    author: "Charles Samuels"
  - subject: "Waste of Time"
    date: 2000-09-30
    body: "<P>\nThe current version of KDE on my system (1.1.2) already has not one, but two MP3 players.  It also has a \"media player\" that as far as I can tell can only play .WAV files, and \"aktion\", which can play AVI/QT/MPEG files.\n\n<P>\nWhy do we need <strong>another</strong> media player?  Especially one that calls itself a \"media\" player, but only plays MP3 files?  Why can't you take one of the other media apps for KDE and build on it or mold it to your vision?\n</P>\n\n<P>Is it our destiny to have a dozen MP3 players, ten IRC chat clients, and five instant messengers?  Can't someone work on a tool that KDE needs, but <em>doesn't already have</em>?\n</P>"
    author: "Michael Portuesi"
  - subject: "Re: Waste of Time"
    date: 2000-09-30
    body: "Not a good point.\n\nIf you see the need of an at this time absent application, start to code it;\n \nhe wan\u00b4ts to code a media-player, that this time is limited to mp3s and may be enhanced via plugins to support other formats. \nIt\u00b4s his choice, not a matter of the need of anybody, and btw: I like this\neven if kaiman has a similar appraoch (i don\u00b4t know).\n\nBoth kaiman and noatun crash on my system (final-beta), but I like the possibility to choose between different apps, so I await both to be finished/half-way-working.\nAnd I like it to get my desktop gtk-free ;-)\n\nTom"
    author: "Thomas Regner"
  - subject: "Re: Waste of Time"
    date: 2000-09-30
    body: "<p>I really have to take some mild offense to this comment and others like it. Come on people let's think about it.</p>\n<ul>\n<li>This is free open source software... complaining about that seems obtuse.\n<li>The author is writing this for their own reasons and one of them is not the standard universal motivator, to receive piles of money for it. So wanting them to do something else seems to totally miss the point of why they are doing anything!\n<li>Anyone who want's to can write a program for whatever reason they want and they should announce it for those interested. It really is a sad statement that there are so many detracting statements over an announcement.\n<li>Diverse programs offer both personal choice and competition as an impetus to improve.\n</ul>\n<p>I know that we would all like to have every wonderful piece of software for Linux now, but when you consider how long Linux and KDE have been around the progress has been remarkable.\n</p>\n<p>What I hear is a lot of people saying the wish other people would just up and get together to produce the perfect (for that user) environment. Each of these projects was taken up to meet what the author saw as a need, or to gain experience in their area of interest.\n</p>\n<p>What we need is not more programmers doing what somebody else wants, but more somebody elses taking some time and getting involved programming.</p>\n<p>Just my opinion... BTW I <b>love</b> KDE-dot-News!!!</p>"
    author: "Eric Laffoon"
  - subject: "Re: Waste of Time"
    date: 2000-10-01
    body: "<P>\nI suppose you are justified in taking offense to my first message - it was intentionally written in an inflammatory manner.\n</P>\n\n<P>\nPerhaps I shouldn't have picked on this individual project, but this isn't the first time I've seen redundant projects spring forth from the minds of creative and intelligent people.\n</P>\n\n<P>\nIt's one thing to do something that's been done before if it is going to result in some new benefit that the existing implementations don't already have.  Many other people have noted that XMMS is a really good package, and will be tough to beat.\n</P>\n\n<P>\nI understand fully (and support) the goal of having KDE-based programs, and not having to depend upon GTK apps.  But does that require starting a new project entirely from scratch, as opposed to building upon the XMMS infrastructure?  Where do you draw the line between standing on the shoulders of others, versus reinventing the wheel?\n</P>\n\n<P>\nIn response to the \"if you see a need, then start coding to fill it\" claim: that is a legitimate (and very justified) response to people like myself who do possess the programming and technical skills necessary to start coding a KDE application.  But most people do not possess these skills.  Not everyone was born a Picasso.  They'd still like to use KDE to balance their checkbook, though.\n</P>"
    author: "Michael Portuesi"
  - subject: "Re: Waste of Time"
    date: 2000-10-01
    body: "I think you're missing the point.\n<P>\nAs noble as it is to write a program \"for the good of the community\", not everyone spends their time in front of the compiler pursuing such ideals.  Sometimes, it's purely for the fun, or the experience, or to \"scratch an itch\".\n<P>\nWhen somebody does this, I am happy to know that it has been released under the GPL, so that the code is available for others to use IF they choose to.  The goal is not \"never reinvent the wheel\", but to have the code there so you don't <B>have</B> to."
    author: "Cameron Tudball"
  - subject: "Re: Waste of Time"
    date: 2000-10-03
    body: "If a non-programmer has a good suggestion for an app they feel is missing from Linux / KDE or whatever let them shout it from the rooftops and hopefully a few will agree and write it.\n\nThey could even offer to help with the myriad of non-programming tasks which help a project become a successful project.\n\nThere is a lot of difference between suggesting a good idea for a new, different app and critisising the development of a media player though.\n\nThat last paragraph sounds so obvious now, doesn't it? :o)"
    author: "Michael"
  - subject: "Re: Waste of Time"
    date: 2000-10-01
    body: "Noatun (and Kaiman) already play mp3, Ogg Vorbis, and MPEG video.  There may also be some MPEG2 support.  Vorbis doesn't work out of the box yet, but I, myself, play a lot of these Vorbis files (and may I add, they're quite a bit nicer than mp3).\n\nI am doing it for my own purposes.  Remember that the only player currently to support plugins and all is XMMS (and FreeAmp?), neither of which are KDE or QT based.\n\nAll KDE players are very limited.\n\nAbout Noatun (and Kaiman) crashing on certain people's systems on start?  aRts is still in development, I can't promise that it will work on non intel-linux systems.\n\nIt can promise it will work on: FreeBSD/intel and Linux/Intel.\n\nUnfortunately, the MP3 decoder code (mpeglib) doesn't even work on FreeBSD for now.  I do not write this code, I solely work on Noatun (not aRts or mpeglib).  I'm hoping that it will be far more portable by the time KDE2.1 (And noatun) are released, but I can at least take comfort that a majority of my users will be using Linux/Intel (not that I don't hope that all other users are blessed with noatun, but I don't have the resources to fix this stuff, unless you volunteer another system, or fix it :)\n\n\nOk, let's explain \"Noatun\" further:\n\nNoatun, as has been stated, is a place in the old Nordic religion.\n\nBut first, please be aware that my nickname on IRC is Njaard (as spelled Nj\u00e5rd in at least norsk), and frequently spelled Njord (And in english, pronounced Nyord).  Nj\u00e5rd is the nordic god of wind and sea.\n\nSo, in conclusion, Noatun was Nj\u00e5rd's home :)"
    author: "Charles Samuels"
  - subject: "Re: Waste of Time"
    date: 2000-10-07
    body: "Hello everybody,\n\nreading the above comments I could not resist to add my on 2 cents worth.\n\nHaven't we heard all these comments over and over again, and ---  I am sorry, but that's how it seems to me --- usually from people who don't code themselves. Why would anybody want to write another editor, we have emacs and vi. Why would anybody write another text processing system, we have TeX. Why would anybody write another window manager, we have twm (or whatever was there before that).\n\nI am sure none of the programs that seem to be just plain copies of another one are really exactly the same. The more people think about a problem, the more usefull ideas are likely to come up and nobody will deny that this is to the benfit of all of us.\n\nSo go on copying, cheers, Tobias"
    author: "Tobias Vancura"
  - subject: "Re: The Other Media Player"
    date: 2000-10-01
    body: "noatun, is a place in old nordic religion, and the\nname of a supermarket here in iceland (which is actually named after the place in Reykjavik where the first store opened). The name has an acuted o and u in Icelandic: N\u00f3at\u00fan.\n\nJust for fun :)"
    author: "Sm\u00e1ri P. McCarthy"
  - subject: "Re: The Other Media Player"
    date: 2000-10-01
    body: "How long after the skin loader is released will someone release a skin based on the N\u00f3at\u00fan logo? :-)"
    author: "Neil Stevens"
  - subject: "Re: The Other Media Player"
    date: 2000-10-01
    body: "Bah.. the Noatun logo isn't very cool. <a href=\"http://www.noatun.is\">See for yourself</a> :)\n<p>\nAlso, there's a place in Finnmark, Norway called <a href=\"http://home.online.no/~kollstr/noatun/\">Noatun</a>.\n"
    author: "Haakon Nilsen"
  - subject: "Re: The Other Media Player"
    date: 2000-10-01
    body: "I can understand where some people are coming from on the ''redundancy'' issue. However, as mentioned before the reason for the 10s of thousands of apps out there for Linux based systems vary as much as the people who write them. To ''scratch an itch,'' fill a need, or perhaps just curiosity and then share it with the rest of the world; for personal gratification, prestige, and the wanting to share.\n\nWhat I would like to see is a highly ''integrated'' media player that can play the gamut of file types automagically (sure you're looking at bloat) but integrated with KDE would give it a smaller memory footprint. The ability to play mp3, wav, mpg, mov, asf, avi, etc would be a big plus. And extremely pluggable as well for those of us who like eye candy :). One more reason to get friends to switch from that ''other'' operating system when they see the kewl kapabilities of KDE on Linux. Hats off to the programmers!"
    author: "Joseph Nicholson"
  - subject: "Re: The Other Media Player"
    date: 2000-10-02
    body: "<p>\n<quote>What I would like to see is a highly ''integrated'' media player that can play the gamut of file types automagically</quote>\n</P>\n\n<p>Yes, this is what I would like to see, too.  I'm glad to see that noatun is going to try to cover the spectrum of media file formats, and isn't aiming to be another \"look ma, I've got skins\" MP3 player.  If it truly can become the \"one-stop-shop\" that KDE users can turn to for playing back media files, then redundancy be damned - I'll take it.</P>"
    author: "Michael Portuesi"
  - subject: "Re: The Other Media Player"
    date: 2000-10-02
    body: "<p>It only seems to support audio. Big deal, there are a zillion players for audio out there. I'd like to see a <i>real</i> media player which supports both audio and video, including streaming protocols. Offcourse fully plugin-driven, so it can contain decoders for every format out there.\n\n<p>Besides the obvious audio formats like wav, mp3 and au, I'd also like to see a good player for mpg, avi, dvd-video (yeah, I know) and maybe even realvideo/audio (Real can release their plugin as binary format)."
    author: "Erik Hensema"
  - subject: "Re: The Other Media Player"
    date: 2000-10-03
    body: "What about .mpg's, .avi's and .mov's?\n\nI sure am tired of rebooting to Windoze in order to view most video clips.\n\nI see now that this project isn't intended to cover these file formats, even though I got excited when I read about the 'new media player for KDE 2.1'.\n\nIs there a project to cover this?  This is perhaps the very last thing that forces me to keep Windoze around.\n\nCheers,\n\nMatt"
    author: "Matt Thompson"
  - subject: "Re: The Other Media Player"
    date: 2000-10-03
    body: "MPG is supported\n\nAnd I'de be surprised if AVI or MOV is supported because the certain companies that produce those things don't believe in fair competition or standards."
    author: "Charles Samuels"
  - subject: "StarCinema: Multi-Movie  Media Player for Windows"
    date: 2001-11-19
    body: "StarCinema ia an advanced Multi-Movie Media Player that has unique features such as Multi-Movie playback, slow motion, reverse slow motion, frame inspector, movie browser, cinema explorer, media lists, etc."
    author: "Rodolfo A. Frino"
  - subject: "Re: StarCinema: Multi-Movie  Media Player for Windows"
    date: 2001-11-19
    body: "And what does this have to do with KDE?"
    author: "Ranger Rick"
---
Information, <a href="http://noatun.derkarl.org/screenshots.phtml">screenshots</a>, and <a href="http://noatun.derkarl.org/download.phtml">source</a> for noatun, a new media player, developed in the hope for release as part of KDE2.1 is available at <a href="http://noatun.derkarl.org/">noatun.derkarl.org</a>. Noatun will be more than capable of competing head-to-head with XMMS, and is already stable, and usable.  Comments and help are, as always, welcomed.





<!--break-->
