---
title: "People Behind KDE: Claudiu Costin"
date:    2000-10-23
authors:
  - "numanee"
slug:    people-behind-kde-claudiu-costin
comments:
  - subject: "Shouldn't it be Klaudiu Kostin ?"
    date: 2000-10-23
    body: "???"
    author: "Albert Knox"
  - subject: "Re: Shouldn't it be Albert Cnox ?"
    date: 2000-10-23
    body: "!!!"
    author: "reihal"
  - subject: "Re: Shouldn't it be Albert Cnox ?"
    date: 2000-10-23
    body: "= Claudiu Costin"
    author: "caatje"
---
This week's edition of <a href="http://www.kde.org/people/people.html">The People Behind KDE</a> features an interview with <a href="http://www.kde.org/people/claudiu.html">Claudiu Costin</a> of the Romanian translation team.  <i>"In March 1999, I obtained KDE-1.1.1 and I hugely liked it, especially the idea that I had the code and I was able to change it to my will. When I saw how easy I could modify visible strings thus being able to do translations, I started to translate KPackage 1.2. Toivo Pedaste encouraged me a lot and so the translation of documentation followed."</i>  Thanks goes to <a href="mailto:tink@kde.org">Tink</a> as usual for conducting the interview.





<!--break-->
