---
title: "Trolltech releases localization tool"
date:    2000-10-25
authors:
  - "Hetz"
slug:    trolltech-releases-localization-tool
comments:
  - subject: "KBabel"
    date: 2000-10-25
    body: "Does Linguist have anything in it that KBabel might want to integrate?\n\nAnything that can help the translators work more efficiently is very good for KDE."
    author: "Neil Stevens"
  - subject: "Re: Trolltech releases localization tool"
    date: 2000-10-25
    body: "wow! this tool is rather terrific.\n\ni'm not using tmake .pro files for the project i'm working on, rather GNU ./configure && make, but it is STILL a breeze to use!\n\nthank YOU trolls!\n\nbtw, i've attatched a small screenshot =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Trolltech releases localization tool"
    date: 2000-10-25
    body: "er.. attatched a screenshot *smacks forehead*"
    author: "Aaron J. Seigo"
  - subject: "Re: Trolltech releases localization tool"
    date: 2000-10-25
    body: "Very cool!  Thanks for the screenshot."
    author: "ac"
  - subject: "Re: Trolltech releases localization tool"
    date: 2000-10-25
    body: "How does this differ from KTranslator/GTranslator and GNU Gettext?"
    author: "Anonymous"
  - subject: "Re: Trolltech releases localization tool"
    date: 2000-10-25
    body: "Is this actually useful for KDE ? It seems to use different format for i18n texts than KDE."
    author: "L.Lunak"
  - subject: "Re: Trolltech releases localization tool"
    date: 2000-10-27
    body: "Can anybody reply to the last question?\nIs Linguist really useful for KDE?"
    author: "Dmitry Samoyloff"
  - subject: "Re: Trolltech releases localization tool"
    date: 2000-10-27
    body: "Better ask at kde-i18n-doc@kde.org, I suppose.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Trolltech releases localization tool"
    date: 2000-10-29
    body: "For the time being, the Linguist is of no real use to KDE translation. It's based on another file format and featurewise it is seriously underpowered compared to <a HREF=\"http://i18n.kde.org/tools/kbabel\">KBabel</a>.\n\n<p>Regards,\n\n<p>Thomas"
    author: "Thomas Diehl"
---
Trolltech has released a preview version of Qt Linguist - an application translation system under the open source BSD license.

<i>"Qt Linguist, localization tool, allows users to seamlessly convert Qt-based programs from one language to another, simply and intelligently. Qt Linguist helps with the translation of all visible text in a program, to and from any language supported by Unicode and the target platforms. A key feature in Qt Linguist is a specialized editing tool, with many features to help the translators be more productive and get better results."</i>  

You can read the press release <a href="http://www.trolltech.com/company/announce/linguistpre.html">here</a> and you can download it <a href="ftp://ftp.trolltech.com/qt/pre-releases/">here</a>. Could someone provide some screenshots please?



<!--break-->
