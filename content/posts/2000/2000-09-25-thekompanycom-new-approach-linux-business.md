---
title: "TheKompany.com: A New Approach to Linux Business"
date:    2000-09-25
authors:
  - "hnilsen"
slug:    thekompanycom-new-approach-linux-business
comments:
  - subject: "Cool!"
    date: 2000-09-25
    body: "Another good one from Dennis E. Powel..   This certainly explains theKompany.com better than ever before. They are certainly doing cool stuff for KDE.\n\nGo theKompany!!"
    author: "KDE User"
  - subject: "Re: TheKompany.com: A New Approach to Linux Business"
    date: 2000-09-25
    body: "Hmm. I feel a bit skeptical about all this. Hows he paying his programmers? $20US/hour works out quite steep for a Kompany with little or no sales.\nStill, I think the basic philosophy of the idea is sound - this is just the sort of commercial support Linux and KDE especially needs (given the lack of support from Sun et al)"
    author: "Ezz"
  - subject: "Re: TheKompany.com: A New Approach to Linux Business"
    date: 2000-09-26
    body: "While I am very pleased with the story overall, there are a couple of items I wish were left out.  I was joking about salary rates in different countries, and it got posted like this, and it's not accurate.  We pay a very fair wage for the countries we are in (we span 11 time zones), every country has a different salary that is fair.\n\nSo far the company has been self funded from me.  We have other things we do than products, we do web site work, specific project work, and some consulting.  This all helps to make money, but we would certainly like to sell more of our PowerPlant product (sales have been growing steadily).  Hopefully people will support us by buying our products, so we can support them by adding more hands."
    author: "Shawn Gordon"
  - subject: "Re: TheKompany.com: A New Approach to Linux Business"
    date: 2000-09-28
    body: "Ok Shawn.\nWhy don't you try for some Venture Cap?\nWHy not make a bit of noise, let people know you're doing great things? I'm sure that extra financial backing would enable you to budget for marketing etc which (I assume) you can't do so much at the moment, which, in turn, would boost sales.\nI also can't help but notice that Slashdot has ignored the Kompany, whereas they welcomed HelixCode and Eazel with open arms. I think theKompany really needs to raise it's profile where possible.\nStill, as I say, I like the basic philosophy and kudos for doing something!"
    author: "Ezz"
  - subject: "Re: TheKompany.com: A New Approach to Linux Business"
    date: 2000-09-28
    body: "We are looking at some financing options right now, I just don't want to give up the company for a chunk of working capital.  If we can accelerate sales of PowerPlant, and with our new products coming out in the next couple months, we might be able to keep going.\n\nPR is tough, a lot of people have been ignoring us.  Loki is our distributor, and /. loves them, and Loki still hasn't been able to get /. to say anything.  We've tried, users have tried, but I guess there is too many neat things with Lego's going on to notice us.  We run PR pieces as often as makes sense for what we are doing.\n\nThere will be some product reviews in Linux Journal and Linux Magazine later this year, and Nick Petrely is supposed to interview us soon.  Also got a call from the Discovery Channel to do an interview soon.  We are also looking for a VP of Marketing to really get things kick started.\n\nThanks for your suggestions and kind words"
    author: "Shawn Gordon"
  - subject: "Re: TheKompany.com: A New Approach to Linux Business"
    date: 2000-09-25
    body: "This is maybe the closest thing to Helix Code in the KDE camp, with the fundamental difference that it's not driven by a KDE founder/core developer, which I think is Helix's downside.\n\nThe Kompany is working on really cool key software for KDE, the KDE/Qt bidings are a much needed piece of software, and it seems that Kivio and Magellan will be formidable competitors for Dia (or whatever Open Office comes with) and Evolution (de Icaza's brainchild and favorite son).\n\nWay of go guys..."
    author: "Carlos Miguel"
  - subject: "Re: TheKompany.com: A New Approach to Linux Business"
    date: 2000-09-26
    body: "I had the chance to exchange a few e-mails at the \nbeginning of this month with Mr. gordon, IMHO he's\na very decent man, I like what that company stands for and will offer my services as soon as\nI'm done working on my current project.\nFo any of you people that like Linux app. dev.\ntake a look at PowerPlant, I'm certain you will be impressed.\n\nCheers"
    author: "Steven Hebert"
  - subject: "Re: TheKompany.com: A New Approach to Linux Business"
    date: 2000-09-29
    body: "I found this article to be very inspiring in  more ways than one. One reason is I like the vision of theKompany. Linux needs more companies like theKompany doing quality software for the masses."
    author: "David Trotz"
---
<a href="http://www.linuxplanet.com/">LinuxPlanet</a> carries an <a href="http://www.linuxplanet.com/linuxplanet/reports/2362/1/">interview</a> with Shawn Gordon of <a href="http://www.thekompany.com/">theKompany</a>. It's all about their business model and their really cool projects like Magellan (coming soon to a FTP server near you!), Kivio and KDB. Really interesting.