---
title: "People Behind KDE: Reginald Stadlbauer"
date:    2000-10-31
authors:
  - "numanee"
slug:    people-behind-kde-reginald-stadlbauer
comments:
  - subject: "Reginald Hanson?"
    date: 2000-10-30
    body: "Anyone else think Reggie looks like the oldest Hanson brother? Or maybe the kid on Third Rock From The Sun?"
    author: "Otter"
  - subject: "Re: Reginald Hanson?"
    date: 2000-10-31
    body: "Definitely the 3rd Rock guy, I'd swear they were long lost brothers!  :)"
    author: "Chris Bordeman"
  - subject: "Not quite answered question"
    date: 2000-10-30
    body: "So who's the leader of his favorite band.  Who is his favorite band?  I would not care, if it weren't for the tease."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Not quite answered question"
    date: 2000-10-31
    body: "Well, if I had a guess, I'd say he talks about Kurt Cobain/Nirvana.\n\nBut who knows..."
    author: "tix64"
  - subject: "Re: Not quite answered question"
    date: 2000-11-07
    body: "I don't know, Kurt Cobain at a BBQ? what a depressing thought! ( you'd have to keep pulling him off the grill and hiding the lighter fluid just in case he tried to drink it.) \n\nI don't mean to offend anyone, it's just,  if I'm not mistaken he was allways depressed and trying to kill himself."
    author: "bender"
  - subject: "Re: People Behind KDE: Reginald Stadlbauer"
    date: 2000-10-31
    body: "Hey Reggie!\n\nI probably don't live too far from you if you're located near somewhere near Troll Tech. I'm at \"Frogner\" in Oslo. Mail me if you'd like to have a beer sometime:-)\n\n--\nAndreas Joseph Krogh <andreak@nettverk.no>"
    author: "Andreas Joseph Krogh"
  - subject: "Kalle Dalheimer"
    date: 2000-10-31
    body: "Does anyone here know if Kalle Dalheimer still is affiliated with the KDE team? And what is he doing right now?"
    author: "Mats Eriksson"
  - subject: "Re: Kalle Dalheimer"
    date: 2000-10-31
    body: "http://www.kde.org/gallery/#dalheimer"
    author: "reihal"
  - subject: "Re: People Behind KDE: Reginald Stadlbauer"
    date: 2000-11-04
    body: "I just want to say <I>thanks, Reggie</I>, for KWord and KPresenter.  KOffice has got to be the boldest part of KDE.  It's quite impressive to know that you agreed to set aside work on KPresenter to fill the need for KWord, and did such an incredible job with it.  Despite some small nit-picks, it's unbelievably full-featured and well thought out.  Definitely the cornerstone of KOffice."
    author: "Andy Marchewka"
---
This week, in <a href="http://www.kde.org/people/people.html">The People Behind KDE</a>, Tink interviews <a href="http://www.kde.org/people/reggie.html">Reginald Stadlbauer</a>, hacker extraordinaire originally behind such things as <a href="http://www.koffice.org/kpresenter/">KPresenter</a> and <a href="http://www.koffice.org/kword/">KWord</a>. <i>"I posted to the kde lists, that I'll write a
powerpoint clone. As I was new to C++ and Qt/KDE, nobody took me serious (as some people told me later :-)
But after some time I had something which was awfull code but worked somehow. So I got a CVS account
(although coolo was not sure at that time if this is good idea, as he told me later :-) and got more and more
involved into KOffice and KDE."</i>

<!--break-->
