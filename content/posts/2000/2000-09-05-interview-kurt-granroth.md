---
title: "Interview with Kurt Granroth"
date:    2000-09-05
authors:
  - "numanee"
slug:    interview-kurt-granroth
---
ITworld.com is currently running an interesting <a href="http://forums.itworld.com/webx?128@237.WHPoaYqLciI%5E1@.ee6c983">web interview</a> by Joe Barr with core KDE developer, Kurt Granroth.  Kurt talks about KDE2 vs KDE1, KOffice 1.0, KMail, and much more.
<!--break-->
