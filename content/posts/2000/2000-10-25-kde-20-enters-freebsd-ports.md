---
title: "KDE 2.0 Enters FreeBSD Ports"
date:    2000-10-25
authors:
  - "grussell"
slug:    kde-20-enters-freebsd-ports
comments:
  - subject: "Re: KDE 2.0 Enters FreeBSD Ports"
    date: 2000-10-25
    body: "Great! Compiling right as i type.. :)\n\n(damn what a long time it takes!)"
    author: "Per Wigren"
  - subject: "Re: KDE 2.0 Enters FreeBSD Ports"
    date: 2000-10-25
    body: "That dumb guy at Slashdot who said KDE's source is more portable than Gnome's would feel embaressed now..."
    author: "Anonymous"
  - subject: "Re: KDE 2.0 Enters FreeBSD Ports"
    date: 2000-10-25
    body: "<p>Why? You can ( and I have ) compile the source tarballs as released by KDE, unpatched.\n<p>\nThe compile problem is a GNU libtool bug. HAND, advocating the quality of GNU software, such as GNOME and libtool"
    author: "George Russell"
  - subject: "Re: KDE 2.0 Enters FreeBSD Ports"
    date: 2000-10-26
    body: "me too! i can compile unpatched from the tarballs, without any problem after figuring out the correct LIBS environment to set ;)"
    author: "nuzrin"
  - subject: "Re: Finally! Woohoo!"
    date: 2000-10-25
    body: "I've been checking every day for qt-2.2.1 since it was announced. And I've been checking twice daily for kde-2 since Monday. I checked last night and neither qt221 nor kde2 were there. I was starting to think that the maintainer was on vacation...\n\nNow it's here! But... but... I have class tonight and can't build it :-( Maybe I'll use a sick day tomorrow from work :-)"
    author: "David Johnson"
  - subject: "Re: KDE 2.0 Enters FreeBSD Ports"
    date: 2000-10-26
    body: "precompiled packages available for FreeBSD 4.1?\njust want to save some time compiling everything :-)"
    author: "mr. daemon"
  - subject: "Re: KDE 2.0 Enters FreeBSD Ports"
    date: 2000-10-27
    body: "Hi.\n\nThe whole of ports gets built weekly.  Judging by the timestamp from last week's build of 1.94, 2.0 ought to hit the packages directory either Friday or Saturday.\n\nThe link should be:\n\nftp://ftp.freebsd.org/pub/FreeBSD/FreeBSD-stable/packages/kde\n\nwhen it gets there.  You'll likely want to re-build qt2.2.1 (which is in: ftp://ftp.freebsd.org/pub/FreeBSD/FreeBSD-stable/packages/x11-toolkits)\nas the binary package won't have gif support compiled in.\n\nCheers,\n\nBen"
    author: "benmhall"
  - subject: "Re: KDE 2.0 Enters FreeBSD Ports"
    date: 2000-10-28
    body: "just found out that 2.0 binaries are at ftp://ftp.FreeBSD.org/pub/FreeBSD/ports/local-distfiles/will/\n\nin the process of downloading :-)\nthanks"
    author: "mr. daemon"
---
KDE 2.0 final is <a href="http://lists.kde.org/?l=kde-devel&m=97244603313822&w=2">now available</a> from the ports tree. Use <tt>cvsup</tt> to sync your ports tree with the master site, and then enjoy KDE 2 with
<tt>cd /usr/ports/x11/kde2 ; make install</tt>
as root. Finally, a KDE more recent than 1.94. Only thing that seems missing is kdepim. Hopefully this also fixes the compilation problem leading to an unusable khtml on FreeBSD. See 
<a href="http://freshports.org/">FreshPorts</a> for more details. Enjoy. ;-)

<!--break-->
