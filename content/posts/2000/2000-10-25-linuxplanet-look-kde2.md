---
title: "LinuxPlanet: A Look at KDE2"
date:    2000-10-25
authors:
  - "numanee"
slug:    linuxplanet-look-kde2
comments:
  - subject: "Nice article"
    date: 2000-10-25
    body: "that made me want to install KDE2.0 right away.\n\nNow, I'm just waiting for Bero to fix that RH 7.0 thingie, so it won't overwrite my KDE 1.2 settings.\n\nBero wrote something on Slashdot three days ago, but I haven't seen any remarks lately.\n\nAny news on that front?"
    author: "Torben Skolde"
  - subject: "Re: LinuxPlanet: A Look at KDE2"
    date: 2000-10-25
    body: "Did they really gave their own opinion about KDE?\nKDE 2.0, the final release, is in my experience still an unstable beast.\nHalf of the apps are either not working or are just a port of the bad-designed KDE 1 apps.\n\nThis is not flame, just my experience and opinion.\nKPaint is probably the worst KDE app ever."
    author: "Anonymous"
  - subject: "I agree, just too many annoying bugs."
    date: 2000-10-25
    body: "Not to take anything away from the KDE guys, this is a wonderful achievement and I love them. However, they have squandered somewhat the moral \nhigh ground that they had over the Gnome guys \nfor their terrible 1.0 release. I have been using\nKDE2.0 for a day now and have come across several\ninfuriating annoyances and quite a few \"show stoppers\". I'm surprised that they were not picked\nup in the testing. Anyway, here are some that I found (purely in the spirit of constructive criticism). I would appreciate any workarounds.\n</p>\n\n1) Kmail. Sometimes gets mail from the server and drops it into the ether. Thankfully I tested it before switching my mailbox over.\n           Sorting does not work properly. \n</p>\n\n2) The Kpanel is terribly buggy. I cannot add any applications to the panel. The icon appears on the panel, but when I click on it, I\n           simply get an error message. </p>\n\n3) Konqueror crashes reliably under many circumstances. Clicking on a postscript file displays it correctly once. If I click \"back\" and\n           click on the file again, crash! Also, the browser has a problem with accepting URLs. After a while, it refuses to take new URLs and keeps\n           going back to the old page. For some reason, Mozilla M18 has the exact same bug! </p>\n\n4) I hate to say this, but Koffice is unuseably buggy. Sometimes, it crashes while doing common operations. (Creating a table of\n           contents). It corrupts its own file so that when you try to read it, it crashes. The math fonts look terrible when printed. </p>\n\n5) Knode has not crashed on me so far, but there are so many small things that do not work right that I gave up on it. </p>\n\nKDE developers, here's hoping that 2.01 will get here quickly! And that brings me to another point:\nwhy don't the KDE team do incremental releases\nthe way gnome guys do? Fix a bug in an app, say Kmail.  It should be released in a form that does\nnot necessitate upgrading the whole of KDE. This is one area where Gnome has a significant  advantage. </p>\n\nMagnus."
    author: "Magnus Pym"
  - subject: "Re: I DON'T agree, just too many annoying bugs."
    date: 2000-10-25
    body: "Why didn't you test KDE 2 in the release candidate stage then? To take the final and say it's wrong doesn't cut it for anyone, not even you...\n\n<p>Please: If you write HTML code then write correct code. An ending tag like &lt;/p> does imply there is a corresponding opening tag...\n</p>\n\nThat's the reason your post doesn't render correctly under konqueror. By the way I just tested the opening of ps-files in this konqueror several times and as you see it didn't core dump or do anything stupid...\n\n\nregards\nKarl G\u00fcnter W\u00fcnsch\n(posted with konqueror)"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: I DON'T agree, just too many annoying bugs."
    date: 2000-10-25
    body: "<I>Why didn't you test KDE 2 in the release candidate stage then? To take the final and say it's wrong doesn't cut it for anyone, not even you...</I>\n\n<P>(Raises hand) Maybe Magnus didn't, but I did. </P> \n\n<P>I tested KDE 1.91 through to the final release...and you know what?  I could see comments like this comming.  When the release date only shifted by a week, and it still had substantial showstoppers, I expected that the 2.0 release would not be stable enough to recommend.  KDE 2.0 is interesting -- as a preview.  The beta and pre-releases were exceedingly difficult to compile and install, thus cutting into my ability to comment on it at all...let alone contribute.</P>\n\n<P>Take this as someone who doesn't have a clue, or is not paying attention, or is just stupid if you want...I don't expect much different. </P>\n\n<P>The typical process I'd follow to test KDE went something like this; wipe dedicated KDE 2.x test system clean, check directions, read mailing list, grab source or RPMs and compile/install, encounter showstopper, check mailing list, add/update supporting files, wipe out KDE 1.x and 1.9x, run Gnome to verify X is OK, recompile from source, compile and install, other showstopper, wipe out test system and reinstall OS, told 'use CVS', used CVS (scant directions), recompile and install, encounter other showstopper, told 'RedHat has it out for KDE', update more supporting files, told 'obviously you don't know what you're doing', ....  Maybe I don't, maybe I didn't have the right libraries and the right environment settings, but you can't fault me for trying!</P>\n\n<P>At one point early in the 1.9x series when I had a partially working test system, I offered to give some time creating KOffice templates (open job #10?), but nobody seemed interested in the offer.  Maybe they knew how much of a moron I was.  Either way, last time I checked the job was still unclaimed.</P>\n\n<P>I'm about to wipe out my CVS copy again and give it another try...maybe 2.0+ will be better?  Can you blame me for being unenthusiastic at this point?  Probably..."
    author: "Andy Longton"
  - subject: "Re: I DON'T agree, just too many annoying bugs."
    date: 2000-10-25
    body: "Andy, what does it say when a lot of people find KDE2 stable and very usable, and you don't?  Perhaps could it be something you're doing wrong?  Like maybe you could have a corrupt .kde from all the betas and alphas you have been testing?\n\nOr else you have a vastly different idea of what a \"showstopper\" is.  Every little bug is not a showstopper.  Crashes and instability or dangerous behavior are."
    author: "KDE User"
  - subject: "Re: I DON'T agree, just too many annoying bugs."
    date: 2000-10-26
    body: "KDE User, read the ... well ... KDE User mailing list.  Archives if you have to.  In the past few days, the advice has ranged from 'compile from source' to redhat/mandrake/SuSE 'are doing something wrong'.  Yet, the directions aren't consistant, and change as new people report different problems.  Ignore the evidence of others if you want.\n\n<P>Showstopper: A defect that makes a system unusable as designed where there are no user-level workarounds.  (If you want other definitions, I'll consult my VV&T notes...though they are substantially the same.)\n\n<P>Examples: Taskbar, Control Center, and Konq. dying.  KDE 1.x apps not working or not working at all or as usual.  Failure of KDE 2.x to load completely.\n\n<P>I'll use KDE 2.0, but I'm not under any illusions that it's high quality software at this time.  It has promise...it's just not there yet. (But, why listen to me.  I'm obviously a moron or a troll.)"
    author: "Andy Longton"
  - subject: "Re: I DON'T agree, just too many annoying bugs."
    date: 2002-05-17
    body: "With a name like Andy, you most certainly must be a troll.\n\nI realize this thread is probably dead, since October was 8 months ago and KDE 3 is now out.  However, I hope Andy is notified of this response and reads it.\n\nI represent a computer company in Washington state...no I'm not Bill Gates.  We have installed KDE2 properly and have kept close watch over our production machine.  KDE2 does crash and is not a finished product.  In fact, I hesitate to call Linux and Linux-based software products.  Only if the software is closed source would I consider it a product.  Otherwise I would call them tools.\n\nWe would be repeating the sins of our fathers if we called our software finished while it is obviously not.  Afterall, that is what Microsoft has always done.\n\nKDE3 has not yet crashed on us, but I expect it too at least once in the next three months.  I hope I am proven wrong.\n\nQuirks in the placement of objects on the screen is usually not a showstopper.  however, if it prohibits your use of the software then it is!  Buttons not responding to activity is typically an issue of patience or dire need of ungrades, but I have seen programs in Linux exhibit this behavior authentically.\n\nWe could go on and on.  KDE2 is a good desktop.  It is better than Gnome in the respect of not having Nautilus integrated into the file browsing and every other creepy corner.  Nautilus is a resource hog.  Also, Gnome has a dark feel...trolls must prefer gnomes...they taste good.  KDE has a light and cheerful look.  I have had more programs fail to launch in Gnome than I have in KDE.  The light feel and the ability to launch most programs are the two sole reasons we use KDE on every computer at our business.  I think we'll stick with it.\n\nNashledanou!"
    author: "General REC"
  - subject: "Re: I agree, just too many annoying bugs."
    date: 2000-10-25
    body: "Which version of KDE do you use? \n\nI tested the kpanel (2) and the konqueror (3) and did the same things you described without any problems. What is you problem with KNode? I use it for more than 8 weeks and it works perfect for me. I am compiling from CVS but this will not make much difference to the final relase.\n\nKOffice has indeed several bugs but every week things getting better. And comparing with M$ Office and StarOffice I like KOffice much more (it is faster and the others have bugs as well).\n\nI agree with you critics about the upgrading mechanism of KDE. But if I remember correctly the KDE developers attack this problem and will update their packages more often.\n\nciao, Marco"
    author: "Marco Krohn"
  - subject: "Re: I agree, just too many annoying bugs."
    date: 2000-10-25
    body: "<br>> Which version of KDE do you use?\n\n<p>Sorry, you wrote that you use KDE2.0, my fault.</p>"
    author: "Marco Krohn"
  - subject: "Re: I agree, just too many annoying bugs."
    date: 2000-10-25
    body: "<p>\nHi,\n</p>\n\n<p> \nSorry for the bad HTML formatting on the previous post.Are you saying that you cannot reproduce these bugs with your setup?\n</p>\n<p> I have a Mandrake 7.1 system. I have compiled\nKDE2 myself from source and put it in /opt.</p>\n<p>\nTry the following:\n</p>\n<p>\nK-Menu-> Panel Menu -> Add -> Application -> Internet -> Netscape.\n</p>\n<p>\nThe netscape icon appears on the panel. Now click on it. I get a window with the following message:\n</p>\n<p>\nThe desktop entry file \n/home/magnus/.kde2/share/applnk/Internet/Netscape.desktop has no type=...entry. \n</p>\n<p>\nAs for Konqueror, are you saying that you have no\nproblems with new URLs being accepted? Well, it is probably a setup-specific bug then. What happens is that after visiting a few sites, Konqueror does not accept any more URLs and keeps going back to the last site visited. But I do not know why it happens for me and not you. </p>\n\n<p> I checked your claim about PS files. I made a mistake. The crashing depends on the specific PS file being opened. It looks like ps files made from dvips are handled without problem. I have a \npostscript file that was I unfortunately cannot share (proprietary info) that appears to have been\ncreated with some wierd app. I can open the file\nwith ghostview without problems. I can even open\nit with kghostview from within Konqueror once. If I try to do it again in the same session, it crashes. If I can reproduce this with some non-propritary document, I will send in a bug report on this. </p>\n\n<p> For Kmail: I tested this and am able to observe cases when it obtained messages in the server and did not put the message into my inbox. I'm not sure about how to generate a test case for this, but I assure you that it does happen. Also, try sorting the messages in a folder based on the date. Every once in a while, the sorting goes haywire. </p>\n\n<p>Magnus </p>"
    author: "Magnus Pym"
  - subject: "Re: I agree, just too many annoying bugs."
    date: 2000-10-25
    body: "As much as I used to like Mandrake, since 7.0 it's been exceedingly buggy. I've had similar problems with KDE2 pre-releases on Mandrake 7.1 that no one else seemed to have. \n\nAs much as I love Mandrake's install and configuration utils, I'm moving to Debian as soon as Woody is officially released."
    author: "Aaron Traas"
  - subject: "Re: I agree, just too many annoying bugs."
    date: 2000-10-25
    body: "I have to agree about Mandrake. After having quite a few problems myself with 7.0 ( release versions of many apps wouldn't compile properly, or wouldnt run quite right once they did) which, from everything I could find out, I shouldn't have been having, I moved back to RedHat 6.2, and have hardly had a problem, since.\nI'm currently running a CVS version of KDE 2.0 without a snag, when I could barely get it to compile on Mandrake 7.0 ( yes tried with exact same tar balls, just recently moved to using CVS )"
    author: "Eddy"
  - subject: "Re: I agree, just too many annoying bugs."
    date: 2000-10-25
    body: "well I use the current CVS - so maybe some things have been fixed since KDE2, but I did not encounter such problems with pre-KDE2 versions\n\nadding Netsacpe this way works fine for me\n\nNo problems with konqueror accepting URLs - even working for hours. There seems to be a problem with redirects and going BACK.\n\nas for PS-files you might have problems with encrypted files.\n\nas for kmail - I receive many mails per day which I fetch from my POP3 server - for many month I did not encounter  a loss of mails. \n\nas for sorting - did you check if the header shows \"Date of arrival\" - then it's sorted by the date of arrival but shows the actual date\n\nhope that helps\n\nferdinand"
    author: "Ferdinand Gassauer"
  - subject: "Re: I agree, just too many annoying bugs."
    date: 2000-10-25
    body: "<p>I am really not getting this. I have mandrake 7.1 with KDE 2 compiled in the /opt/kde2 directory and it has been practically perfect for me for about a month. I have the final release in now and it's great.</p>\n<p>Obviously I cannot duplicate the bugs mentioned here. My question is did you compile QT too? I found that with the qt 2.2.1 rpm from Mandrake that it did not behave as expected and compiled qt myself to be sure. Additionally I found on at least one occasion during the beta process for some reason some residual files from when I moved qt giving me problems. I wiped the kde 2 directory and reinstalled.</p>\n<p>Finally I have posted at <a href=\"http://virtualartisans.com/linux/mandrake/\">virtualartisans.com/linux/mandrake/</a> my instructions for compiling KDE 2 on Mandrake 7.1. I can think of one other potential issue. The latest cooker seems to have gcc 2.96 for some reason. I would have thought something would have been learned from Red Hat's mistake. You should be compiling with gcc 2.95.2 preferably. (2.96 is a beta with C++ problems)</p>\n<p>KDE 2 may be a hair short of flawless and still not have every conceivable application and feature however it has ceded nothing to any other desktop in terms of reliability or function. If you are having a bad experience it is not really rational to assume everyone else has somehow received hyper-functional code. ;-)</P"
    author: "Eric Laffoon"
  - subject: "Some improvement if you wipe out .kde2 and restart"
    date: 2000-10-26
    body: "<p>Guys,</p>\n\n<p>Based on suggestions I received, I deleted\nmy .kde2 directory and restarted KDE2. There\nseems to be substantial improvement to the overall stability. Some of the Kmail and Knode\nproblems have gone away, and the panel is \nworking mostly OK now. The only problem is\nthat I cannot get the taskbar to disengage\nfrom the panel. </p<\n\n<p>Maybe the baggage that remained in .kde2 from the\nbetas caused some of the problems. I guess this\nshould be added to the KDE2 HOWTO somewhere? </p>\n\n<p>Magnus.</p>"
    author: "Magnus Pym"
  - subject: "Re: Some improvement if you wipe out .kde2 and res"
    date: 2000-10-26
    body: "Woohoo!  Good to hear.\n\nUnfortunately, the taskbar does not \"disengage\" in KDE2.  Some code was there, but the proper framework was not.  It was decided that this feature would have to wait for KDE 2.1.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: I agree, just too many annoying bugs."
    date: 2000-10-27
    body: "I have to agree as well... Mandrake 7 and I've been using KDE 2 from CVS for around 3 months, no more problems than the known bugs.  must admit, my mandrake is a very minimal install just the basics needed for a linux system."
    author: "David"
  - subject: "Re: I agree, just too many annoying bugs."
    date: 2000-10-26
    body: "I built KDE 2.0 from source on a RedHat 6.2 box\nthat I use at work.  I tried what you said above\nand when I clicked on the new Netscape icon on the panel I got... a Netscape window pretty much\nthe way you would expect.\n\nI've been running various KDE 2.0 betas since\nBeta1 came out and I've never experienced the\nkind of URL cycling problem you describe with\nKonqueror.  (I have experienced it on Windows\nwith MSIE, though.  Weird.)\n\nI have experienced some crash problems with viewing PS files, but I think the problem is\nwith the PS viewer, not with Konqueror.  So far\nevery such crash I've had has been fully reproducible in the stand-alone PS viewer app.\nFrankly, I would agree that that one isn't quite\nup to snuff; OTOH it isn't really any different\nfrom the PS viewer in KDE 1.1.2.\n\nIn general, I'm quite impressed with KDE 2.0.  I think Konqueror's web browsing is really good.  I know of a few pages that have rendering glitches (but which are still perfectly usable). The main issues I have with it are Java and JavaScript related (some pages that use JavaScript for form submission don't work at all).  But Konqueror has replaced Netscape as my\nprimary browser; I just keep Netscape around to\nhandle the few pages that Konqueror still won't.\n\nMoreover, as a platform for further development\nKDE 2.0 kicks butt.  I expect to see a flood of\npowerful, high-quality apps coming out for this\nenvironment in the relatively near future.\n\nKyle Haight"
    author: "Kyle Haight"
  - subject: "Re: I agree, just too many annoying bugs."
    date: 2000-10-26
    body: "I tried this (using knode instead of nutscrape).  Worked fine.  SOunds like you have some lone kde config file somewhere that is acting up.  When I installed KDE 2.0 (compiled from source on Mandrake 7.1 without a KDE 1.1.2 install) I removed my old KDE 2.0 RC2 install and completely reinstalled it.\n\nEverything works beautifully so far.  And I can't wait for Mandrake 7.2\n\nOh yeah, posted using Konqueror...\n\nTim"
    author: "Tim_F"
  - subject: "KMail just fine"
    date: 2000-10-25
    body: "KMail has never dropped stuff in the ether for me! What are you talking about?"
    author: "David Simon"
  - subject: "Re: I agree, just too many annoying bugs."
    date: 2000-10-26
    body: "I installed KDE 2 final yesterday on my SuSE 7.0 and was very impressed. There is major progress im comparison to the last beta i had installed before (1.93)\n\nBut, nevertheless, there are some annoying bugs. Several applications crash without any obvious cause. Especially koffice seems rather unstable to me. \n\nBut the thing I'm most unhappy about is that i can't get working neither kscd nor kmidi. kscd simply doesn't recognize the cd, though it's no problem to play it with Xmcd, and kmidi crashes while starting...\n\nIs there anyone who had similar problems and found a solution for them?\n\nGreetings\n\nChristoph"
    author: "Christoph Maurer"
  - subject: "Re: I agree, just too many annoying bugs."
    date: 2000-10-27
    body: "<i>1) Kmail. Sometimes gets mail from the server and drops it into the ether.</i>\n\nIf you are using a pop server and can reproduce this bug then give me a mail and I'll try to help you debug the problem. (It doesn't have to happen on demand, even being able to reproduce it once every few days is enough).\n\nIf you use a local account you should ensure file locking is set up correctly as described in http://lists.kde.org/?l=kmail&m=97163225926136&w=2\n\n\nIt does sound as if KDE did not install correctly for you. I don't experience any of the problems you do with the apps I use. I have received somewhat similar complaints from people whose KDE installs have only partially succeeded. (But these people have not mentioned any mail losing bugs)"
    author: "Don Sanders"
  - subject: "Re: LinuxPlanet: A Look at KDE2"
    date: 2000-10-25
    body: "<br>> This is not flame, just my experience and opinion.\n\n<p>Sorry, but it really sounds like a flame.\nWriting as anonymous and give just general statements like: \"is an unstable beast\" is typical for flamers.</p>\n\n<p>No insult intended, I just want to explain why one can think it is meant as a flame.</p>\n\n<p>Ok, try to give us some examples of things that are unstable. Which apps are bad-designed or bad ported? Or even better write bug reports to bugs.kde.org. This will help us all to make kde a better place ;-)</p>\n\n<p>ciao, Marco</p>"
    author: "Marco Krohn"
  - subject: "Re: LinuxPlanet: A Look at KDE2"
    date: 2000-10-25
    body: "<I>&gt; Writing as anonymous and give just general statements like: \"is an unstable beast\" is typical for flamers.</I>\n\nI also wrote \"in my experience\"..."
    author: "Anonymous"
  - subject: "Re: LinuxPlanet: A Look at KDE2"
    date: 2000-10-25
    body: "<I> &gt; Ok, try to give us some examples of things that are unstable.</I><BR>\n\n* Konqueror crash before I see the main window (beta 2 is much more stable).\n* Same story for KOffice.\n* KMail still doesn't have full thread support.\n* I still can't configure the file dialog to do double-click-to-open-a-file (I hate the one-click-to-do-it-all \"feature\" ala Win98).\n* No TAB completion in the file dialog like GTK+/Gnome.\n* Focus system is terrible IMHO.\n* The apps load much slower than KDE 1 or Gnome apps. Mainly because of DCOP (yes I know everybody claim it's fast, but it just doesn't feel like fast to me).\n* Some apps still looks ugly (like KEdit/KWrite).\n* No floating toolbars like in KDE 1 and Gnome.\n* Panel is terribily unstable. I can't even add an applet! I lost my taskbar and I can't get it back!\n* Al some more things I can't remember right now...\n\nYes I'm using KDE 2.\nAnd yes both QT and KDE are compiled without exceptions."
    author: "Anonymous"
  - subject: "Re: LinuxPlanet: A Look at KDE2"
    date: 2000-10-25
    body: "> Konqueror crash before I see the main window \n\nObviously an install issue. Do you think they'd release KDE2.0 if Konqueror had that problem? :)\n\n>Focus system is terrible IMHO\n\nSo change it to whatever scheme suits  you. Look in the control center.\n\n>Some apps still looks ugly (like KEdit/KWrite)\n\nHow's that? Have you tried changing style/theme/icon theme?\n\n>No floating toolbars like in KDE 1 and Gnome.\n\nThis I think will be back in KDE 2.1."
    author: "Haakon Nilsen"
  - subject: "Caught you"
    date: 2000-10-25
    body: "You are obviously trolling since there is no KPaint in KDE2, is there?  The paint app is pixie which Mosfet pulled at the last minute."
    author: "KDE User"
  - subject: "No, caught YOU!"
    date: 2000-10-25
    body: "Download kdeaddutils.rpm and see it for yourself.\nNow what were you saying about no KPaint in KDE2?\n\nAnd Pixie is not an image editor, but an image VIEWER!\nSo who's trolling here now?"
    author: "Anonymous"
  - subject: "Re: No, caught YOU!"
    date: 2000-10-25
    body: "<p><i>Download kdeaddutils.rpm and see it for yourself.\nNow what were you saying about no KPaint in KDE2?</i></p>\n<p>Freaking confused people!!! kpaint is so not part of KDE 2 it is not even funny. You will find it in the <b>nonbeta</b> package and it has always been a seperate compile. The last work done on it was something like a year ago!</p>\n<p>If your distro has included non release KDE programs in the package to have more stuff avaialble great. But... it is not part of the KDE 2 release. It is in a package of <b>unstable</b> applications available for people to bitch about.</p>\n<p>That may not be trolling... but it is certainly getting to the bottom of the barrel.</p>\n<p>I would say if you are looking this hard for reasons to dislike it then don't use it! Nobody here who enjoys the functionality and stability of KDE really cares about kpaint and if they did they would work on getting developers for it. </p>"
    author: "Eric Laffoon"
  - subject: "Re: No, caught YOU!"
    date: 2000-10-26
    body: "For your information: Pixie is also an image editor. It has more than 20 filters you can aply\nto your images. You have obviously never tried to use them."
    author: "Zeljko Vukman"
  - subject: "So what?"
    date: 2000-10-27
    body: "So Pixie has filters.\nIt still isn't an image editor!\nCan you draw lines with Pixie?\nOr ovals? Or rectangles?\nNo you can't! So Pixie is not an image editor!"
    author: "Anonymous"
  - subject: "Re: So what?"
    date: 2000-10-27
    body: "No, when you change allready created images - you edit them (for example by applaying filters). When you draw lines, ovals, rectangles - you create images. Pixie is not an image creator, but it is an editor."
    author: "Zeljko Vukman"
  - subject: "Re: LinuxPlanet: A Look at KDE2"
    date: 2000-11-22
    body: "i installed kde 1.9 and it was a mess\na screwed my desktop......\n\nthen ........\n\ni trashed all the kde packages\nand installed kde 2.0\nWorks fine for me.... no problem at all\n\nworked first time .... impressive !!!\n\nY"
    author: "yan"
  - subject: "Re: LinuxPlanet: A Look at KDE2"
    date: 2000-11-22
    body: "yeah everybody seems to have problem with 2.0\nwell if you upgraded your old kde 1.x.x\nand you did not trash all old /lib....\nor backed them up dont expect the impossible\ni first tried without cleaning my /lib\nscrewed everything was fu**ed up kfm & kconqueror\nwas running at the time ????????????\n\ni then backed up all kde's  /lib\n\nand installed all kde2 packages\nfrom my home directory at the bash prompt\nby doing.......>\n\nrpm --nodeps --force -i *rpm\n\nI have no problem Yet and installed kde2\nfor a month now and my pc as been rebooted \nlike 2 time max."
    author: "yan"
  - subject: "Footprint"
    date: 2000-10-25
    body: "Is there anyone out there willing to post their \"top\" output? I would like to get some idea of KDE2's footprint before trying it on my 64MB machine. Thanks."
    author: "Chris"
  - subject: "Re: Footprint"
    date: 2000-10-25
    body: "Y'll be pleased ;-)\n\nThe data  collected come from a 64MB Pentium MMX 200MHz and XFree 4.\n\nwith  SuSE 7.0 KDE2 rpms the minimal memory usage after startup is about 30MB\n\nafter opening kword,kspread,kmail,knode,kpm, konqueror webbrowser and konqueror filemanager, and kcontrol the \"momory used\" is 40MB.\n\nhope that this convices you\n\nferdinand"
    author: "Ferdinand Gassauer"
  - subject: "Does SuSE enable exceptions in QT"
    date: 2000-10-25
    body: "You will gain about 2MB RAM with -no-exceptions\nThe file size is ~5.5MB without and ~7.5MB with them for libqt"
    author: "anon"
  - subject: "Re: Footprint"
    date: 2000-10-30
    body: "how on earth can you run it on a 200mhz machine?  i am compiling kde + qt from source (slackware 7.1) on a 450mhz machine w/96mb of ram and konqueror is intolerably slow.  which pisses me off because i like its UI a lot and want to use it as my main browser!\n\nwhat am i doing wrong - or, what could i do to increase performance?  i compiled QT with no exceptions already...  are there flags to kde to make it fast and small?\n\nhelp!!\nthanks,\nsteve"
    author: "steve havelka"
  - subject: "compile QT without exception handling !"
    date: 2000-10-26
    body: "I have a 64MO laptop.\nAnd observe a better speed and memory usage (around 30%) after the compilation of QT \nwithout exception. \nI don't see any difference in stability.\nThe way to do it :\n- download the QT source\n- open $QTDIR/configs/linux-g++-shared (or freebsd-g++-shared or whatever you use) with text editor \n- add \"-fno-exceptions\" to SYSCONF-CXXFLAGS.\n- then ./configure and make.\nThat's all.\nYour QT libqt.so.2.2.x is ~ 5MO."
    author: "thil"
  - subject: "Re: Footprint"
    date: 2002-06-06
    body: "Well, I have kde2 running (as part of the Peanut distro) on a 150MHz system with only 68k memory.  Loading takes about 25-30 sec.\n\nIt is noticeably slower than the kde-1 running on a similarly configured 120MHz system. But there's no other way I can run koffice, so I live with the slow performance. "
    author: "Ken"
  - subject: "Is focus follows mouse indeed broken?"
    date: 2000-10-25
    body: "The article mentions that menus don't work with focus follows mouse. Is that indeed the case? I was thinking about upgrading within the next couple of days, but this problem would certainly make me wait for the next release, as I use focus follows mouse all day.\n\nClaus"
    author: "Claus Wilke"
  - subject: "Re: Is focus follows mouse indeed broken?"
    date: 2000-10-25
    body: "Not on SuSE 7.0 \n\nit works fine with Xfree 336 and 4.0.0 right out of the box\n\nferdinand"
    author: "Ferdinand Gassauer"
  - subject: "Re: Is focus follows mouse indeed broken?"
    date: 2000-10-25
    body: "Great. That's the setup I wanted to try.\n\nClaus"
    author: "Claus Wilke"
  - subject: "You know you are being trolled when..."
    date: 2000-10-25
    body: "<a href=\"http://linuxtoday/news_story.php3?ltsn=2000-10-25-003-21-NW-CY-KE&tbovrmode=1#talkback_area\">LinuxToday</a> has better comments than you do."
    author: "KDE User"
  - subject: "HELP..viewing jpeg/gif"
    date: 2000-10-26
    body: "Hi all,\n\nI don't why I can view a jpeg and gif file on my KDE2. I can't select a jpeg file as my bacground image either. \nI think have compiled QT and all the KDE2 packages  correctly.\nI specified flags for QT :\n./configure -shared -sm -gif -system-zlib -system-libpng -system-jpeg\nThe order KDE2 package compiled:\nKDESupport-2.0\nKDElibs-2.0\nKDEBase-2.0\nKDEAdmin\netc\nAny body can help me? I now there must be something wrong since for other people its working, but I just can't find it.\n\n\nJamal"
    author: "jamal"
  - subject: "Re: HELP..viewing jpeg/gif"
    date: 2000-10-27
    body: "Don't use:\n\n-system-libpng -system-jpeg\n\nif they are not installed, maybe they are not. Have you installed kdegraphics???"
    author: "David"
  - subject: "Re: HELP..viewing jpeg/gif"
    date: 2000-10-28
    body: "How do I detect that my RH 6.1 has library jpeg and png that Qt using. What version of these libraries that works with QT-2.2.1?<br>\nI have compiled and installed all the 13 packages including kdegraphics-2.0, with order : <br>\n1. QT-2.2.1 <br>\n2. kdesupport <br>\n3. kdelibs<br>\n4. kdebase<br>\netc....<br>\nMy question:<bt>\nif I don't specify flags:<br>\n-system-libpng -system-jpeg <br>\nIs Qt will directly use its jpeg and png library?<b>\nThanks..."
    author: "jamal"
---
<a href="http://www.linuxplanet.com/">LinuxPlanet</a> is the first one out with a <a href="http://www.linuxplanet.com/linuxplanet/reviews/2531/1/">closer look at KDE 2.0</a>. <i>"For years we've many of us made excuses for our Linux desktops. They did what we wanted, and what we wanted that they didn't do we learned to live without in order to take advantage of the robustness of the underlying operating system. But with KDE2 we no longer have to apologize for our desktops. Even with that annoying menu bug, KDE2 is the best desktop I've ever used on any platform."</i>  The bug that Dennis refers to was fixed a while back, and thankfully does not occur in the default KDE2 config. <b>Update 10/26 9:20 AM</b> by <b><a href="mailto:navindra@kde.org">N</a></b>: This bug is actually <b><i>not</i></b> present in the final KDE 2.0, it was found and squashed before the final release.



<!--break-->
