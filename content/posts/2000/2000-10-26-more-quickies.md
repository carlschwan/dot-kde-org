---
title: "More Quickies"
date:    2000-10-26
authors:
  - "numanee"
slug:    more-quickies
---
<a href="http://bestlinux.net">BestLinux</a> have announced  <a href="http://bestlinux.net/en/news/07.shtml">Release 3</a> of their distribution, proudly featuring KDE 2.0.
<a href="http://www.newsforge.com/">NewsForge</a> offers some <a href="http://www.newsforge.com/article.pl?sid=00/10/25/0223211">enthusiastic commentary</a> on KDE2. ZDNet picked up the press release <a href="http://www.zdnet.com/zdnn/stories/news/0,4586,2644170,00.html">as well</a>, although with a few harmless inaccuracies.  <a href="mailto:Victor@KSourcerer.org">Victor Röder</a> informed us that <a href="http://www.ksourcerer.org/">KSourcerer.org</a> is up again, courtesy <a href="http://www.pingworks.de/">Pingworks IT Consulting</a>.  <a href="mailto:multivac @ fcmail . com">Neil Stevens</a> wrote us with a simple message: Instead of flooding channel #kde on irc.kde.org with support requests, please use #kde-users instead.  The less noise on #kde, the better for the developers and in the end everyone wins.



<!--break-->
