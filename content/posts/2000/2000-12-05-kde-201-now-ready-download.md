---
title: "KDE 2.0.1, Now Ready for Download"
date:    2000-12-05
authors:
  - "Dre"
slug:    kde-201-now-ready-download
comments:
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "I have some issue to download \"qt-x11-2.2.2.tar.gz\" from \"ftp://ftp.trolltech.com/qt/source\". Would it be possible to place a copy of this file in \"2.0.1/distribution/tar/generic/src/\" like previous 2.0.0 release?\n\n    Thanks"
    author: "Nicolas"
  - subject: "Re: KDE 2.0.1"
    date: 2000-12-05
    body: "Please"
    author: "Jim Ermis"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "Ok, now I have it. And... thanks for KDE team. Champagne for all of you!"
    author: "Nicolas"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "Quick Q for you ppl already running 2.0.1:<br>\nIs it possible to drag and drop links in konqueror now? ie - drag a link from a page to another window, that loads it. It's pretty much the only feature from netscape 4.x/Mozilla that i miss.\n<p>\n-henrik"
    author: "Henrik"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "i'm not using 2.0.1 but a version from CVS of about a week ago and yes, DND links works just fine..."
    author: "Aaron Seigo"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "Really, I just grabbed the Mandrake 2.0.1 RPMs and\nI couldn't drag html links into another window.\n\ndoug"
    author: "Doug Keller"
  - subject: "Are you using the right wm?"
    date: 2000-12-06
    body: "Could it have to do with the wm? Some don't support KDE's DND..."
    author: "Juju"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "He said he was using CVS, not 2.0.1."
    author: "ac"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "Really, I just grabbed the Mandrake 2.0.1 RPMs and\nI couldn't drag html links into another window.\n\ndoug"
    author: "Doug Keller"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "Really?  I can't find Mandrake RPMs, and am d/ling RedHat 6.x src.rpm's..."
    author: "Ill Logik"
  - subject: "Kdebase"
    date: 2000-12-05
    body: "I installed Suse 7.0 rpms - works fine though but while installing kdebase-2*.rpm there is a failed dep called \"icons\".\nI simply cant find a package which makes me think that it could fulfill this dep. So which one is it?"
    author: "predator 1710 of Valor"
  - subject: "Re: Kdebase"
    date: 2000-12-05
    body: "Your CD package contains this package. But you can also install with rpm -Uvh --nodeps kdebase*. It will work also usually.\n\nbye\nadrian"
    author: "Adrian"
  - subject: "Re: Kdebase"
    date: 2000-12-05
    body: "Your CD package contains this package. But you can also install with rpm -Uvh --nodeps kdebase*. It will work also usually.\n\nbye\nadrian"
    author: "Adrian"
  - subject: "Re: Kdebase"
    date: 2000-12-05
    body: "Your CD package contains this package. But you can also install with rpm -Uvh --nodeps kdebase*. It will work also usually.\n\nbye\nadrian"
    author: "Adrian"
  - subject: "Re: Kdebase"
    date: 2000-12-06
    body: "What is \"my CD package\"? Simply say it s package \nxxx.rpm on suse 7.0 or you can d/l it at \nhttp://.... which would be better"
    author: "predator 1710 of Valor"
  - subject: "Re: Kdebase"
    date: 2000-12-06
    body: "By testing the deps (rpm -Uhv --test *.rpm) I also encountered this unsatisfied one (SuSE 6.4). But I've found a icons.rpm on rpmfind.net, which satisfies it! It should also exist on the SuSE CD in xwm1 folder, because rpmfind structure is similar to Suse's own. Or download it simply,\nhere is the adress of the package on rpmfind:\n\nftp://rpmfind.net:21/linux/SuSE-Linux/i386/6.4/suse/xwm1/icons.rpm"
    author: "Adrian"
  - subject: "Re: Kdebase"
    date: 2000-12-06
    body: "thankee-say."
    author: "predator 1710 of Valor"
  - subject: "Congrats!"
    date: 2000-12-05
    body: "Congratulations on yet annother triumph! KDE is _THE_ desktop environment, and I for one am drooling over the idea of it...\nA few ideas:\n  Add the window decoration thingymagjig to KControl... I don't know wether it's happening everywhere, but in KDE 2.0 on my box it was missing..\n  Anyways and anyhow, I'm realy liking what's happening, and look forward to seeing the further development of this beauty.\n  Rock on!!"
    author: "Sm\u00e1ri P. McCarthy"
  - subject: "Re: kcontrol"
    date: 2000-12-05
    body: "I'm gonna like those kcontrol bugfixes.\nAfter trying KDE2 for a while I had this feeling kcontrol became more stable after a week, on which it crashes every time no matter what user or what config files exist.\n\nAfter I used Kicker for a while and got _used_ to it, I really felt in love with it :-)"
    author: "dys"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "Argh, the error message I keep getting upon trying to kick off startkde look beautiful.  \n\n\"Cannot find octet stream/binary\"\n\"No MIME types loaded!\"\n\nI know it's just missing something.\nI uninstalled all of my previous KDE stuff with rpm -e, installed QT 2.2.1, kdesupport, kdelibs, kdebase and then the rest.with rpm -ivh.  I'm booting into run level 3, and kicking it off with a 'startx' with /usr/local/kde/bin/startkde in my .xinitrc . Can't think of anything I missed. Ideas?"
    author: "Brendan"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "I'm not sure if this will help , but I seem to remember that it sometimes makes a difference when you\nrm -rf ~/.kde and ~/.kderc too."
    author: "wim bakker"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Hi,\nI also have the following lines in my .xinitrc\n\nexport KDEDIR=<KDEDIR>\nexport QTDIR=<QTDIR>\nexport PATH=<KDEBINDIR>:$PATH\nstartkde\n\nThe first two are not really neccessary but the missing PATH could cause a problem. If this wasn't it, I have no idea either ;)\n\nGood Luck,\n\nRichard"
    author: "Richard Stevens"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "I had this problem with Caldera and KDE2.0. The fix was as follows:\n\ncd /opt/kde/lib\nrm libmimelib.so\nrm libmimelib.so.1\nln -s /usr/lib/libmimelib.1.0.1 libmimelib.so\nln -s /usr/lib/libmimelib.1.0.1 libmimelib.so.1\n\nIn Caldera, KDE installs under the /opt directory. You may prefer to rename the libraries instead of removing them. Likewise, check for libmimelib.1.0.1 .\n\nChuck"
    author: "Charles"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "Oops, make that\n\nlibmimelib.so.1.0.1\n\nChuck"
    author: "Charles"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "Way to introduce new depenendencies into a BugFix release, guys.  I'm tripping out a little that I'd be expected to upgrate libstdc++, and libc for a minor-fix release.  These dependencies should have been held-off 'til KDE2.1."
    author: "James"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "Using more recent base libs is always a good idea, since the bug fixes in them often apply to huge amounts of apps! Also, unlike in Windoze, updates to very base libraries are free! Thanks to progs like mandrake update and urpmi, things like this are even easier! I love doing stuff like \"ln -s /usr/bin/myupdatescript.sh /etc/cron.monthly\""
    author: "Carbon"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "In most cases, I would agree with you.  In this case, however, To install 2.0.1, I would have to upgrade my entire system.\n\nLet me illustrate my point.\n\nTo install the new KDE Libs, I need the new QT libs.  Cool.  This is not a problem.  To install the QT libs, however, I have to upgrade libc (To glibc 2.2), and libstdc++.  These are not libraries users should be expected to update.  This upgrade will break a number of other applications.  Fine, I can recompile.  The real kicker in it all, is that to upgrade to glibc2.2, I need the GCC 2.96 development snapshot.  Blegh.\n\n- James"
    author: "James"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "That seems wrong, I have a RH 6.2 system (VALinux box) with glibc 2.1.3 and QT 2.2.1 (which I compiled from the Source RPM) and I didn't have any problems.  I had several KDE 1.9x releases and the 2.0 final installed.  I just upgrade to 2.0.1 by \"rpm -Uhv --replacefiles k*\" in the directory I downloaded the RPMS to.  It worked perfectly.\n\nWhat distro/lib versions do you have that would prevent you from installing QT and KDE?"
    author: "raven667"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "I could say the same about latest versions of linux2.4testX....\n\tyou can't compile 2.4-test12preX since you need latest version of util-linux -> can't install later RPM verisons of util-linux on a rh6-0,1, 2 system since it depends on Glibc 2.2 and can't compile late versions of util-linux either...\nWho says you can get off the Windows upgrade death march by switching to Linux? I don't see it."
    author: "ac"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Why would you want to update to glibc 2.2? Glibc 2.1.2/3 should be sufficient. I updated my system quite a while ago without any trouble. I was using a SuSE 6.0 based system with glibc 2.0.7 and egcs compiler for quite a while until I discovered a bug in libstdc++ (iostreams not working) and I had to upgrade. I grabbed the sources for gcc 2.95.2, glibc 2.1.2, and the latest libstdc++, read the documentation and started compiling. The installation was not a problem (you have to be careful with glibc installation, of course :-) and the system worked smoothly ever since.\n\nRegards, Stefan."
    author: "Stefan Hellwig"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "<P>\nLooks like you are using something compiled for a different system (an older major version of the distribution, maybe?).  Also please note that the KDE team does not compile the release, this is done by the various distributions.\n</P>"
    author: "Dre"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "If upgrading large amounts of progs is a big problem, then it isn't really that much trouble to simply wait three or four weeks for a new distro to come out with updated libs, and use that! After all, you can buy almost any distro at www.cheapbytes.com for under $20 w/ S&H!"
    author: "David Simon"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "I just finished compiling qt-2.2 and all of kde2.01 from the source tarballs (with the exception of the games and koffice, just downloaded those) without any problem. I am using gcc-2.95.2 and glibc-2.1.3. It seems that the dependency problems are due to the builder of the rpm's and not kde2.01 itself."
    author: "Greg"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "Thanks\n\nI love you, I love ALL KDE TEAM!!!!!!!!"
    author: "Manuel Rom\u00e1n"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "\u00bfA todos, juntos? :-)"
    author: "Luis Digital"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "\u00a1\u00a1\u00a1\u00a1HOLA!!!!!\nCuanto tiempo. Hace mucho que no tengo el placer de comunicarme contigo. Y s\u00ed, a todos, y sobre todo a ti y GNU_LEO."
    author: "Manuel Rom\u00e1n"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Ejem, maricon\u00e1s las precisas :))))"
    author: "anonimous"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "No te entiendo.\nI don't understand you."
    author: "Manuel Rom\u00e1n"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "Yeah, and kudos to the GNOME, Enlightenment, WindowMaker, Sawfish, AfterStep, BlackBox, and other DEs/WMs!"
    author: "Carbon"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-05
    body: "too, but I love specialy KDE Team. Because KDE is orgasmic for me."
    author: "Manuel Rom\u00e1n"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "But where are debian packages ???"
    author: "benoit"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "http://kde.tdyc.com"
    author: "Hasso Tepper"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Add this line to /etc/apt/sources.list\n\ndeb http://kde.tdyc.com/ potato main crypto optional qt1apps\n\nHave fun"
    author: "Mike Goodman"
  - subject: "Floating Panels?"
    date: 2000-12-06
    body: "Hi, thanks for the great release..\nOne Question:\nI got used to the floating panels, esp. the Taskbar, from the CVS.\nNow I've installed the rpms, and: the panels have gone...\nWhat a pity.\nOr am I doing anything wrong? At least, there is no Add->Extension in the kicker-menu at all.\n\nDD"
    author: "ddd"
  - subject: "Re: Floating Panels?"
    date: 2000-12-06
    body: "Yes, I'd like to make the same \"complaint\".  The extensions were great.  On average I have about 10-20 windows opened and having a child panel dedicated mainly to taskbar was a real blessing.\n\nI tried to patch up the new 2.0.1 code with older CVS source - all in vain (broken library dependencies).\n\nOther than that, KDE 2.x rocks!!!"
    author: "Peter"
  - subject: "Re: Floating Panels?"
    date: 2000-12-06
    body: "The CVS version that you got is 2.1 branch and not 2.0.1.. 2.0.1 only has bug-fixes.. what u were looking at is future 2.1! \n\nI have it installed and loving the extensions etc.. go back to CVS ;)"
    author: "sarang"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Congratulations! It's great to see a followup so quickly to KDE 2.0.\n\nTwo quick questions about installing KDE 2.01 on a Red Hat 6.2 system using RPMs.\n\n1. To install 2.01 and remove 1.12 in one step, is \"rpm -Uvh [rpm_name]\" all I need to do?\n\n2. The install README says that it is preferable to use qt-2.2.2 but the 2.01 RPMs posted today contain qt-2.2.1. Using RPMFind, I've located Red Hat's qt-2.2.2 RPMs, so I think I'm all set. Just curious: why wouldn't the RPMs be prepared with 2.2.2 (which was evidently packaged on Nov 17)?\n\nMany thanks.\n\nMichael O'Henly"
    author: "Michael O'Henly"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "There's the texture example in the qt-2.2.2 examples directory I couldn't compile/make. I removed the $QTDIR/examples/texture directory and proceeded fine, any clues...still looking at the code!"
    author: "Charles Kibue"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Still waiting for Caldera rpms.  I hope it fixes some bugs in knode and kmail that I've been having."
    author: "Jim"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "<i>I hope it fixes some bugs in knode and kmail that I've been having.</i><br>\n<br>\nDon't have to high expectations, this release is mainly for documentation and translations. Only some critical bugs are fixes. Have a look at the changelog."
    author: "Michael Haeckel"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Two things:\n-where are the slackware packages? i converted teh rpms to tgz format with rpm2tgz but this leads me to my next question:\n\n-kde2.0 installed to /opt/kde2 when I used teh pacakges or compiled from scractch and set KDEDIR to /opt/kde2. But when I tried installing these new packages nothing gets installe ther, but rather fills up stuff in /usr/*. I REALLY REALLY liked having kde all in the /opt/kde2 dir since it was so EASY to upgrade (mv /opt/kde2 /opt/kde2.old) and then install. PLEASE, can somebody help me find out how I can install all these new bugfixes into /opt/kde2? :)"
    author: "Will Stokes"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "The Slackware packages should show up soon in slackware-current. Or you could build them from scratch if you're patient.\n\nBut converting an RPM to tgz is not always the best thing to do. As you've already seen, Redhat/Mandrake install under /usr while Slackware installs under /opt. You will also miss the /etc/profile.d/kde.sh file meant for Slackware.\n\nYou could also take a look at package central (www.linuxmafia.org)"
    author: "David Johnson"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Congrats! I think KDE is goning the right way.\nA small question:\n\nDoes someone know about the system how the icons at the Desktop are arranged when you \"Arrange icons\"? I have lots of Directorys and files on my desktop an everything is mixed up. It woud be great if KDE had a sorting mechanism that someone understands. E.G. sorting by name or by filetype. Also an auto-sort function would be great.\n\nT.E."
    author: "Thomas Eller"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "This message has been Slashdotted as well!\n<A HREF=\"http://slashdot.org/article.pl?sid=00/12/06/001253&mode=thread&threshold=-1\">http://slashdot.org/article.pl?sid=00/12/06/001253&mode=thread&threshold=-1</A>\n\nYes, you'll find a lot of trolls and flamebaits this time too!"
    author: "Anonymous"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Question - why has KDE 2.0.1 not included RPMs for all of the locale settings? Am I correct in assuming that we can still use the 2.0 packages for locales that are missing?\n\nApart from that, KDE 2 is an vast improvement over KDE 1.x, and light-years ahead of Gnome IMHO."
    author: "Alan Hughes"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "There ARE rpms containig the localizations - more and better localizations and docs is one of the MAIN reasons KDE 2.0.1 exists so it'd be just stupid not to include it... :-?"
    author: "Gunter Ohrner"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "But WHERE are they? I'm using the RH 7.0 RPMs from ftp.kde.org; as an example the 2.0 release included an rpm for a \"British\" locale. Its certainly not there in 2.0.1 - I've check 2 minutes ago."
    author: "Alan Hughes"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "Right, the only translations available for RH7 seem to be in this directory:\nftp://ftp.kde.org/pub/kde/stable/2.0.1/distribution/rpm/RedHat/7.0/noarch\nand the British translation is NOT one of them. This is very strange as I'm using the rpms for SuSE 7.0 and there are at least 30 different translations available... Maybe you should contact the pakagers who created the RH rpms. (Another alternative would be to switch to SuSE... <g>)"
    author: "Gunter Ohrner"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "Thanks, but RH suites me just fine at the moment <g>. KDE2.0.1 also seems to be OK with the 2.0 locales, so I'll just leave it as that for the time being. I think I will fire an e-mail to the packagers as soon as I have time."
    author: "Alan Hughes"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "<h1>KDE Too popular??</h1> \n<br>\nthe <b>CVSUP</b> is <b>down </b>:-(((\n<br>\nI have tried for a several days to connect to the CVSUP server, I either get a timeout or \n\"Access limit reached\"\n<br>\nIs the CVS server working ok? \nShould I switch to CVS instead?\n<br>\nErlend"
    author: "Erlend Boe"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "try max.tat.physik.uni-tuebingen.de"
    author: "Zank Frappa"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Thanks!\nThat works like a dream :-)"
    author: "Erlend Boe"
  - subject: "STILL no taskbar outside the panel ?!"
    date: 2000-12-06
    body: "Great work, 2.0.1 was finished quickly, but I have to say I'm very disappointed ! THE showstopper is still not fixed ! When will it be possible to run the taskbar outside the panel ? Why is Kasbar not downloadable (or if it is, where ? Searching through the whole web only gives me notes that it's avaible via CVS, but because of the firewall here at work I'm not able to connect to CVS).\nMany people now have complained about this, and it is absolutely neccessary to run the taskbar outside the panel...\nI'd be very happy if someone could tell me how to fix this, because it's simply very ugly and contraproductive to have this way too small taskbar in the panel.\nC'ya,\n StonedBones"
    author: "StonedBones"
  - subject: "Re: STILL no taskbar outside the panel ?!"
    date: 2000-12-06
    body: "You are talking about KDE 2.1 features. 2.0.1 is a bugfix only release. I'm sorry but you will have to wait for the first KDE 2.1 beta which will be released before christmas.\n\nBye,\nMatthias"
    author: "Matthias Elter"
  - subject: "Re: STILL no taskbar outside the panel ?!"
    date: 2000-12-06
    body: "At least these are concrete infos ;-)\nThnx\n\tStonedBones"
    author: "StonedBones"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "KDE2.0.1, Now Ready for Download\nsalim akyol"
    author: "salim akyol"
  - subject: "Patches? (Re: KDE 2.0.1, Now Ready for Download)"
    date: 2000-12-06
    body: "As the distro is so huge by now: Are there patches available to patch the 2.0 tarballs to 2.0.1, thus saving a lot of download time? So far, I've only seen tarballs.\n\nCheerio,\n\nThomas"
    author: "Thomas"
  - subject: "Re: Patches?"
    date: 2000-12-09
    body: "Yeah!\n\nPlease could someone make diffs 2.0-2.0.1 - I get the stuff via analogue telephone line, i.e. about 2,5 hours of download for all of kde - that's too much for bug fixes and documentation improvements.\nThis wouldn't be much work for some friendly kde-developer.\n\nThanks!\n\nRobert"
    author: "Robert"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "I'm experiencing some trouble with kde-2.0(.1).\nSystem is RH7.0 with updates applied on a Athlon-based computer, with ibm java sdk-1.3.\n*Konqueror:java often stops working (applets aren't executed anymore) and with side effect that the url field doesn't react on keyboardinput anymore. So konqueror has to be killed and restarted to be usable again.\n*Arts:the multimediaplayer doesn't play mp3's (SB-PCI128)Though when I start kde it plays the startup-wav very nicely , xmms does work properly \nstill.\nAre these problems related to RH7.0 being a bit experimental or is the athlon not quite compatible enough or should I use another sdk??"
    author: "wim bakker"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Hi, I work with SuSE 7.0 and an Athlon with SB-PCI128 too. I'm perfectly able to play mp3 with KDE2 MultiMediaPlayer (Kaiman) so I guess it should be a distribution problem. Don't know about Java."
    author: "Andrea Cascio"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "I had some Problems with my SB PCI 128 too (AMD Duron System). I am using SuSE 7.0. My problem was that my PCI 128 did not have the usual ES1371 Chip, but the newer ES5880. So the hardware detection recognized the right card (PCI 128) but with the ES1371 (didn't work well). I simply downloaded an update of alsaconfig from the SuSE support database and the whole thing worked. Good luck."
    author: "daemond"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2001-02-14
    body: "i had a problem regading with my multimedia player i'm using win95. i can't play my vcd, but there's no problem in my cd-rom when i'm playing it it appears \"install a driver that support this type of file. now what i'm supposed to do with this kind of problem. thanx"
    author: "Edagrdo Aguilar"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Focus-follows-mind won't have the desired effect in many cases I'm afraid, maybe a focus-follows-nose\noption together with the automagic patch will do it ."
    author: "wim bakker"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-06
    body: "Has anyone else had the following problem...\n\nAfter installing KDE2 and using kdm to login, all of my console shells did not have their path set correctly.  I \"solved\" this by simply forcing each console shell to be a login shell(an extra parameter to the console usually).  \n\nNow with kde2.0.1, the situation has got worse :-(  Now, no path is being set correctly and I am given the failsafe option of just an xterm when X starts.  It fails to find any of my session types.  \n\nAny suggestions appreciated!"
    author: "Rob"
  - subject: "KDE SUCKS"
    date: 2000-12-06
    body: "KDE SUCKS KDE SUCKS KDE SUCKS\nNAAANANAANAAAAANAA lol :)"
    author: "Flamer"
  - subject: "Re: KDE SUCKS"
    date: 2002-09-30
    body: "lol"
    author: "beavis"
  - subject: "Re: KDE SUCKS"
    date: 2002-12-10
    body: "it really SUCKS! kinda m$ on linux boxes - but much slower!!! use blackbox! kde is for kids"
    author: "asdfafsd"
  - subject: "Re: KDE SUCKS"
    date: 2005-03-05
    body: "KDE sux because of DCOP which can be the root of all your KDE problems.\n"
    author: "gazza_11"
  - subject: "Re: KDE SUCKS"
    date: 2005-04-13
    body: "hi,\n\nmy preference is to work in console, with gnu screen.\n\nbye.\n~a"
    author: "a"
  - subject: "Re: KDE SUCKS"
    date: 2005-12-16
    body: "so what?"
    author: "hi"
  - subject: "Re: KDE SUCKS"
    date: 2006-10-21
    body: "To use an real-life example here: My father-in-law is a big fan of Saab cars. \nHe's a card-carrying member of local Saab-club, and he's constantly telling \nme how great and reliable Saabs are. And while he's doing that, he totally \noverlooks the fact that his Saab has been in the shop two times over the past \ntwo months, how it sometimes spontaneously turns the turn-signals on and how \nthe handbrake doesn't work properly. To him, those problems are irrelevant \nbecause he's in love with the car.\n\nIn a way, same thing would apply here. If I tested KDE with a mindset that \neverything is absolutely fabulous, minor problems might get overlooked.\n\nIf I really pushed my in-law to mention 10 bad things in his Saab, he propably \ncould notice and mention required number of flaws. But if he just keeps on \nthinking that his car is the best thing since sliced bread, those flaws would \ngo overlooked. THIS is what I'm talking about here. \n\nNote: I'm _NOT_ claiming that KDE sucks (well, all software sucks, some just \nsuck less than others ;)). Far, far from it. It's my desktop of choice and I \nuse it every day. That said, I think it could be even better, and it is my \nintention to nitpick my way through KDE. And it helps there if I make the \nconscious decision to find the flaws in KDE. To some, some of the flaws that \nI might discover are so minor that they are not worth worrying about. But \nagain: if I make it my life's mission to find those flaws, then I will find \nthem.\n\nThis isn't about how I feel about KDE. That do I really think that it sucks or \nnot. It's about the mentality. If I test KDE while thinking \"KDE sucks\", it \ndoesn't have to mean that I really think that KDE sucks. I'm having problems \nputting my thoughts in to words here...\n\n> ah, dualism. =) it's either kick ass or it sucks, but it can't be both at\n> the same time?\n\nSure it can :). Make no mistake: I love KDE. What I'm talking about here is \nthe _mentality_ of testing KDE, not the actual feelings towards the desktop.\n\nI do not have an \"attitude\". I do not think that KDE sucks, overall. I talked \nabout mentality, not actual feelings towards the desktop.\n\nAgain: I'm not saying that KDE sucks, and I apologize if it sounded like that. \nWhat I talked about is a way of testing KDE. If I test KDE thinking that \n\"this thing sucks\", it desn't really mean that I think KDE sucks, it's just a \npreconditioned way of thinking to find any flaws in the system. If I start \ntesting KDE and I already think that \"KDE is the best thing since sliced \nbread!\", then I might overlook many flaws and errors in the desktop."
    author: "kde vs gn0me"
  - subject: "KDE 2 Rocks!"
    date: 2000-12-06
    body: "I've been a heavy Helixcode Gnome user for the past six months, but I've got to say, KDE 2 is awesome! Konquerer is especially impressive -- it shows the doubters that, yes Virginia, an excellent web browser can be produced by a small team quickly without reinventing the operating system wheel (read: Giant green monster which knocks over tall buildings of otherwise productive programmers).<p>\n\nOne suggestion: It'd be nice if, upon first run, KDE would automagically, optionally, import the Gnome menu items. Then I'd have the best of both worlds.<p>\n\nCongrats, y'all, and thanks for sticking it out in the face of the Gnome winds. You've proven that GUI competition is good -- really good -- for everyone."
    author: "Steve Freitas"
  - subject: "Congratulation to KDE team!"
    date: 2000-12-06
    body: "Congratulation to KDE team for the 2.0.1 release! I've been using Mandrake 7.2 with KDE2, and it's been great - especially Konqueror. I really like it, and I think Konqi is much better than Netscape 4.7x and Mozilla!\n<BR><BR>\nI'm one of Gnome Foundation members, and although I only speak for myself (not for all Gnome coders), I really wish that KDE and Gnome can be unified  step-by-step.\n<BR><BR>\nMaybe it can be started little by little. Currently Gnome uses /usr/share/gnome/apps for storing its menu entries, while KDE uses /usr/share/applnk. Both KDE and Gnome use the same \".desktop\" file format for its menu entry. I hope that maybe someday Gnome and KDE can use a global menu instead of seperated menu.\n<BR><BR>\nGood luck on the success! :-)"
    author: "Prana"
  - subject: "Re: Congratulation to KDE team!"
    date: 2000-12-07
    body: "yes, please do!! :)\nit can't be that difficult.\ni wish everyone would think the way you do."
    author: "Spark"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "Good job guys!! Thanks for fixing the menu stuff, looks great. Keep up the good work!!"
    author: "Gutter"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "Just FYI, when I went to KDE 2.0.1 from 2.0, I suddenly needed libGLU, which is apparently a set of OpenGL utilities. I tried re-installing XFree86 4.0.1, even re-downloading the source and compiling it, but that didn't install libGLU. Finally, I downloaded mesa's OpenGL-compatible 3D implementation (http://www.mesa3d.org/), which installed libGLU. Either my 2.0 install was incorrect and yet somehow still ran, or this library wasn't required until 2.0.1. In general, as I've gone from KDE 1.x to 2.0 to 2.0.1, it seems as though these kinds of dependencies are not as clearly described as I'd like to see them. It'd be nice to have a README that lists all the known dependencies in order to install KDE. It seems as though the only dependency ever listed is QT, whereas there are quite a few more packages required than that."
    author: "Chris Chen"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "Did you compile QT library with -no-opengl option in ./configure (see install-source page)?\n./configure -sm -gif -system-jpeg -no-opengl"
    author: "Nicolas"
  - subject: "just a quick question..."
    date: 2000-12-07
    body: "... why are kde releases always bundled with all possible kde applications?\ni don't see why i need a new version of ktuberling or even knode. i didn't see any changes in most of the programs i just downloaded. wouldn't it be a better idea to release only a new version of kdebase, kdelibs, etc?\ni don't see why you have to bundle the DE with the applications. even microsoft isn't so strict.\ni want a new version of ktuberling if there is a new release of ktuberling and i want a new version of kde if there is a new release of kde.\ni don't even use kde. i use icewm so single releases of konqueror of kmail would be much more exciting for me."
    author: "Spark"
  - subject: "Re: just a quick question..."
    date: 2000-12-07
    body: "argh.. if someone could delete this and the next one please... sorry :)"
    author: "Spark"
  - subject: "just a quick question..."
    date: 2000-12-07
    body: "... why are kde releases always bundled with all possible kde applications?\ni don't see why i need a new version of ktuberling or even knode. i didn't see any changes in most of the programs i just downloaded. wouldn't it be a better idea to release only a new version of kdebase, kdelibs, etc?\ni don't see why you have to bundle the DE with the applications. even microsoft isn't so strict.\ni want a new version of ktuberling if there is a new release of ktuberling and i want a new version of kde if there is a new release of kde.\ni don't even use kde. i use icewm so single releases of konqueror of kmail would be much more exciting for me."
    author: "Spark"
  - subject: "just a quick question..."
    date: 2000-12-07
    body: "... why are kde releases always bundled with all possible kde applications?\ni don't see why i need a new version of ktuberling or even knode. i didn't see any changes in most of the programs i just downloaded. wouldn't it be a better idea to release only a new version of kdebase, kdelibs, etc?\ni don't see why you have to bundle the DE with the applications. even microsoft isn't so strict.\ni want a new version of ktuberling if there is a new release of ktuberling and i want a new version of kde if there is a new release of kde.\ni don't even use kde. i use icewm so single releases of konqueror of kmail would be much more exciting for me."
    author: "Spark"
  - subject: "Re: just a quick question..."
    date: 2000-12-07
    body: "It's really strange.\nSome people are always complaining about the dependencies (so they flame Gnome) and other people are always complaining about no dependencies!"
    author: "Anonymous"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "It's great and it's beautiful as well!\n\nGreat Konqueror is finally able to understand some - albeit - very limited javascript and CSS. It is still not a replacement to Netscape unfortunately although some reactions go there.\n\nOne annoying <b>problem</b> encountered sofar (after 1 hour): half the screensavers don't work anymore because the parameters past to them from kcontrol are incorrect. If you try to select one that might work, you end up with multiple screensavers running in the background or a screensaver not allowing you back on (thank god there are still virtual terminals from slaying them)."
    author: "Rob Olsthoorn"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "It's great and it's beautiful as well!\n\nGreat Konqueror is finally able to understand some - albeit - very limited javascript and CSS. It is still not a replacement to Netscape unfortunately although some reactions go there.\n\nOne annoying <b>problem</b> encountered sofar (after 1 hour): half the screensavers don't work anymore because the parameters past to them from kcontrol are incorrect. If you try to select one that might work, you end up with multiple screensavers running in the background or a screensaver not allowing you back on (thank god there are still virtual terminals from slaying them)."
    author: "Rob Olsthoorn"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "I downloaded and tried the kde2.\nI overall like it very much, however,\nI found some annoying features:\nThe file selector is much worse than the one that\ncame with kde 1: The worse thing is that file names get truncated, and that renders the \nfile selector almost useless. I managed to display also other information along file names\nwith kde1. Is there a way to change the widget behaviour?\nThe other annoying thing is that the panel does\nnot work too well when in the left corner and\nwith mac-like menus.\n\nUh, also the Window manager does not support\nthe activate-and-pass-click option anymore on\nbutton 1.\n\nIs there a solution to all of the above, or is\nsomething planned for the next releases?"
    author: "Luciano Montanaro"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-07
    body: "I downloaded and tried the kde2.\nI overall like it very much, however,\nI found some annoying features:\nThe file selector is much worse than the one that\ncame with kde 1: The worse thing is that file names get truncated, and that renders the \nfile selector almost useless. I managed to display also other information along file names\nwith kde1. Is there a way to change the widget behaviour?\nThe other annoying thing is that the panel does\nnot work too well when in the left corner and\nwith mac-like menus.\n\nUh, also the Window manager does not support\nthe activate-and-pass-click option anymore on\nbutton 1.\n\nIs there a solution to all of the above, or is\nsomething planned for the next releases?"
    author: "Luciano Montanaro"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-08
    body: "Hi,\n  You must include KDE 2.0.1 for Mandrake 7.2 to download, because the packages in Mandrake Cooker aren't compatible , dependencies problems (RPM 4.0 Libstdc,etc)are painfull to resolve,please put the Mandrake 7.2 RPMs in you FTP site.\nThanks."
    author: "Daniel"
  - subject: "Big icons?"
    date: 2000-12-08
    body: "How do you get doublesize icons with KDE2? I really only am stiking with GNOME for that feature. It means a lot to me, though, since I run at 1024x768! Does anyone  know where in the configuration/kontrol this is?"
    author: "Marc_E"
  - subject: "Re: Big icons?"
    date: 2000-12-13
    body: "Just go into the control center, click 'Look n Feel, click Icons. Then, you can increase the amount of detail the icons are drawn with by choosing 16, 32, or 48, and use pixel-doubling. 48 w/ pixel doubling created icons so easily visible and huge, using a maximized konqi window at 1024x768, i could only view 24 icons at once! Of course, you don't have to make them that large. :)\n\nAlways glad to be of service"
    author: "KDEer"
  - subject: "Re: KDE 2.0.1, Now Ready for Download"
    date: 2000-12-10
    body: "Hi,\n\nKDE 2 seems to be running alot faster in BSD.  Alot more responsive than in Linux.  Any plans for KDE 2.0.1 for FreeBSD?"
    author: "Now a FreeBSD user."
  - subject: "Please make diffs"
    date: 2000-12-11
    body: "I just downloaded KDE 2 (which is, without a doubt, the best Linux desktop) a couple weeks ago over my extremely slow connection and now I need to upgrade. I really can't download everything all over again, but I need the fixes. So could someone please make some diffs and help out all of us without fast connections?\n\nThanks."
    author: "Bill Gates"
---
The KDE Team has just <A HREF="http://www.kde.org/announcements/announce-2.0.1.html">announced</A> the release of KDE 2.0.1.  While the release is primarily for updated translations and documentation and for the addition of Japanese as a supported language, it also fixes many of the very annoying bugs in 2.0.  A <A HREF="http://www.kde.org/announcements/changelog2_0to2_0_1.html">summary of the fixes</A> and a <A HREF="http://www.kde.org/info/2.0.1.html">KDE 2.0.1 Info Page</A> are also available.  As always, enjoy!

<!--break-->
