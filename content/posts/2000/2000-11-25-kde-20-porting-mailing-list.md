---
title: "KDE 2.0 porting mailing list"
date:    2000-11-25
authors:
  - "melter"
slug:    kde-20-porting-mailing-list
comments:
  - subject: "Re: KDE 2.0 porting mailing list"
    date: 2000-11-25
    body: "Good work, but why can't some of the great developers create a Utility or something like that which automatically convert KDE1.1.x source to KDE2.0 source. Why millions of developers of KDE1.1.x do the editing of the source? If only few good KDE1.1.x and KDE2.0 developers get together to create a utility which converts KDE1.1x source to KDE2.0, things could become better and that would be easy for the legacy applications  (of KDE1.1.x) to be available for KDE2. \n\nBest wishes,\nRizwaan."
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE 2.0 porting mailing list"
    date: 2000-11-25
    body: "Have you tried to create one?\n\nDo you know any other example (and I mean a project of this size), where someone did something like this?"
    author: "Marko Samastur"
  - subject: "Re: KDE 2.0 porting mailing list"
    date: 2000-11-26
    body: "John Birch of KDevelop created a script that automatically converts source code from KDE-1.x to KDE-2.0 as good as it is possible. You cannot convert without doing things manually but it is a start...\n...Get the latest CVS source tree of the upcoming KDevelop-1.3 (www.kdevelop.org) and look for file  kdevelop/kdevelop/tools/one2two.\n\nCiao,\n  F@lk"
    author: "F@lk"
  - subject: "Re: KDE 2.0 porting mailing list"
    date: 2000-11-26
    body: "Considering how much work the developers are doing right now, I don't think it's possible. \n\nAnyway, let's have developers that think. Not developers that have their code written for them by other developers :)"
    author: "Chris Aakre"
  - subject: "Re: KDE 2.0 porting mailing list"
    date: 2000-11-27
    body: "Yes and while were are on the subject Why doesn't\nsomeone port my old DOS program to Win2K. )\nThe problems of porting are not trivial.... they \ntake some analysis of the program; i.e. Can I use this \"new\" feature or should I use the old function/feature. (OOPS this is no longer supported) What should I do? (let the program/script handle it?) I think not.\n\nSometimes a script/program can't handle w/o user intervention is the so-called what should I do's. As good as AI is (IMHO not very) you still need the eyes and fingers of a \"real\" person to make such decisions and new let me repeat NEW features\nthat a script can't know about because you wrote the script/program before it had any knowledge of the new features.  \n\nAnd If any one can come up with a program/script to \"automagically\" update source code PLEASE E-Mail me your company details as I would be willing to bet the farm. )"
    author: "Maniac"
  - subject: "FSF is evil!"
    date: 2000-11-26
    body: "GNU is communism!\nRMS is a jerk!"
    author: "Anonymous"
  - subject: "Re: FSF is evil!"
    date: 2000-11-26
    body: "You are a TROLL, and nobody cares."
    author: "Lenny"
  - subject: "Re: FSF is evil!"
    date: 2000-11-26
    body: "If you don't care, then why are you replying?\nThis shows your lack of intellegence!"
    author: "Anonymous"
  - subject: "Re: FSF is evil!"
    date: 2000-11-26
    body: "And why are you Anonymous ?"
    author: "Djiwhy"
  - subject: "Re: FSF is evil!"
    date: 2000-11-27
    body: "Why is GNU communism? As far as I know, communism is a pretty unusable ideology, and GNU a fine project in which a lot of very usable tools and documentation is produced. If you're calling it just communism, you have to explain yourself. Why is FSF evil? It follows (and sets) a lot of standards, and provides the world with usable programs. What is RMS? (I just don't know ;) And why is this guy, who calls himself \"anonymous\" (strange name) a TROLL??? (I think he's not a real troll, and I think Lenny means something else.)\nWhat has this strange meaningless discussion to do with porting?"
    author: "Matthieu"
  - subject: "Re: FSF is evil!"
    date: 2000-11-27
    body: "He must be from Slashdot!"
    author: "Anonymous"
  - subject: "Re: FSF is evil!"
    date: 2000-11-28
    body: "What's wrong with communism?"
    author: "someone"
  - subject: "Re: FSF is evil!"
    date: 2004-12-16
    body: "FSF is evil because they are about taking away my freedom.\n\nSay I want to code an open-source syntax checker for GCC.  Will they add hooks so that my syntax checker can read GCC's internal state?  Absolutely not.  Would you like to know why?  Because they are afraid that people will be able to write a proprietary link in GCC's chain that optimizes my code in some way that GCC can't.  Do I want my code optimized?  Maybe, maybe not.  I at least want the freedom to optimize my code if I feel like it.\n\nFSF is freedom?  No.\n\nFreedom is the ability to peacefully request a redress of greviences.  Freedom is about my ability to make a profit if I feel like it, and donate my effort when I feel like it.  Freedom is (more or less) everything that doesn't enroach on anyone else's freedom (vague, I know).  Software under an X11-type license is freedom.  Software under GNU's license is communism.\n\nWe've never had a communist state.  It turns totalitarian too quick for communism to ever be stable.  Communism is where everything is held in \"common\" (hence the name) by everyone, and where everyone is of equal rank and social class.  While this is great in theory, it falls apart, because people are greedy by nature.\n\nPersonally, I think that anyone should be able to write whatever software they want, and there should be some kind of user rating system that indicates whether it is 'safe' or not.  This is not how it happens, however.  To submit an unsafe ebuild to the gentoo portage tree requires you to prove that you're able to write kernel patches, as well as a \"mentor\" system and a 30-day waiting period.\n\nFree software is about going to any length to prevent people from closing the software, which goes against my definition of \"free\".  If someone hands me a free lumber pile, I have every right to build a fine house out of that lumber pile without explaining to the person who 'gave' me the lumber the intricate details of the plans for my house.  \n\nThis from gentoo's docs: \"If legal action is required, it is very beneficial if the code that is being used improperly is owned by as few entities as possible. \"  That is the dumbest argument I have ever heard.  If legal action is requried, you want the software in the *most* people's hands, so it's harder for Them to sue 50 entities than 1 entity.  \"This is a benefit because all owners of the code in question must be a party in any legal action. \"  Are you kidding me?  You are going to TAKE a legal action against someone else?  Since WHEN should open source companies be TAKING legal action??  Isn't that what you're trying to get away from, or do you secretly LIKE to be like microsoft?  Or how about this: \"If your contributions are or are expected to be significant, we may allow your organization to retain the copyright of your submitted works,\" so WHAT NOW?  They offer EXCEPTIONS FROM THE RULES FOR LARGE COMPANIES?  \n\n/me chokes\n\nThis isn't the Free Software Foundation.  This is the Free Software Monopoly.  All your code are belong to us.  You are on the way to destruction."
    author: "BaseDwarf"
  - subject: "Re: FSF is evil!"
    date: 2007-05-27
    body: "The FSF is evil in the way that they say one thing and do another. Freedom is the freedom to do what you want with your code, so that you make the world a better place. GNU prevents that by forcing you to avoid any code covered by it. I do not use Linux anymore because of this mess. If I wanted to be limited in my \"Openness\" then I would use the GNU, but I do not. The GNU prevents the author from allowing closed source companies from useing their technology. The problem with this is that those companies won't lend you their code either. So, do you want a big company like Microsoft to let you go on coding with half their kernal because you coded the other half? Or do you want to be forced into litigation hell?"
    author: "Anon"
  - subject: "Down with gnu!"
    date: 2000-11-27
    body: "Down with openness! Up with MS qualifications! Long live VBA certified engineers! The GPL will be the bane of our childrens lives! RMS is a slothenly weirdo!\n\nAlso - I have found that the kde2 debs that are for potato and you download off tdyc.com are waaay quicker than the debs that are distributed with woody.\n\nI'm guessing they're distributing binaries with debug support built in.\n\nThis is really bad news as it's going to give a few peeps a very bad first impression of kde2. \n\nBtw: Kde2 potato style debs with a libqt no-exceptions build FLIES on my 486 66 with 32 megs ram (router)."
    author: "anonymous"
  - subject: "Re: KDE 2.0 porting mailing list"
    date: 2000-11-27
    body: "This one's from Slashdot too."
    author: "Anonymous"
  - subject: "Re: KDE 2.0 porting mailing list"
    date: 2000-11-29
    body: "i would say gnu is much more like a socialism, where everyting is everybodies and everyone is equal ( socialism is the base for communism, without the strong military and whatnot)"
    author: "James"
---
A new KDE 2 porting mailing list has been set up to help KDE 1 application authors port their software to the new APIs. Please subscribe if you are interested in porting your KDE 1 application to KDE 2 or if you want to help others with porting their applications. To subscribe visit <A HREF="http://master.kde.org/mailman/listinfo/kde2-porting">http://master.kde.org/mailman/listinfo/kde2-porting</A> or send a mail with "subscribe your-email-address" in the subject of the message to <A HREF="mailto:kde2-porting-request@kde.org">kde2-porting-request@kde.org</A>.  A KDE2 porting FAQ based on questions submitted to this mailing list will hopefully be available soon, too.


<!--break-->
