---
title: "The KDE League, Explained"
date:    2000-11-22
authors:
  - "Dre"
slug:    kde-league-explained
comments:
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-23
    body: "I have been a user of KDe since 1.0beta, and a developer for about the past year or so.\n\nOne thing I have noticed about KDE developers and users alike is the friendly, warm nature towards new users and developers.\n\nI can remember a number of times goign into #kde and posting to kde-devel about simple introductory questions to KDE development, and I remember getting an amzingly quick and encouraging response from the developers.\n\nThese are the reasons I am a KDE user and developer, and do not believe for one second any league or organisation will change that. I know from discussing and hanging out with many of my good friends in KDe that they would *NOT* let any finincial or otherwise organisation screw KDE.\n\nI trust the KDe League and I think it will bring some great things to KDE - mainly publicity and impact on new users, but we can keep the developers weaving the magic that makes KDE so popular.\n\nIf some of the less informed members of the critique assume that the KDE League is a bad thing, lets just ignore them and carry on doing what makes KDE great - producing a great desktop envirenment and applications, and providing a nice friendly platform for potential users/developers."
    author: "Jono Bacon"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-23
    body: "On the contrary!\nIf anything, the KDE League will cause the warm welcome we're getting familiar and accustomed to to go away.. that would be a disaster!\nI've been a KDE user since 1.0beta, and I've been translating it (in small dosages I might add) for about two years, off and on.... one of the things that stunned me was that KDE, more than any other open source movement, worked hard on trying to make people feel comfortable in their new chair :)\n... now during this time, I have seen gigantic companys like RedHat fall into the gaps of their marketing devision... (we all remember 6.1!)... I wouldn't like to see KDE do the same plunge.."
    author: "Sm\u00e1ri P. McCarthy"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-23
    body: "How can creating a marketing side of KDE that *doesn't* affect KDE development be a bad thing?\n\nThis can only be a good thing by bringing KDE to more desktops and bring more users and developers with it.\n\nPlease tell the rational behind your negative feeling?"
    author: "Jono"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-23
    body: "<P>\nThanks for the great explanation, Chris!  Unfortunately it seems that regardless of what we say about the League the media is determined to\n\"spin\" the League into a confrontation with GNOME. Take, as a fairly\ntypical example, this recent ZDNet <A HREF=\"http://www.zdnet.com/enterprise/stories/main/0,10228,2657287,00.html\">article</A>.\nI posted a talk-back to it which has not yet appeared, so I will repost\nit here.\n</P>\n<HR SIZE=1 WIDTH=\"70%\">\n<P>\nThe KDE League has made it abundantly clear that our intent is not to harm GNOME.  We have repeated this consistently in interviews, in press releases, in our <A HREF=\"http://www.kdeleague.org/bylaws.html#article_ii\">Mission Statement</A>, in answering questions and in other public statements.  Instead, the KDE League exists so that all users and developers -- and not just those in the Linux community -- will know about a great choice in the desktop market.\n</P>\n<P>\nThe backers of the KDE League are quite simply supporting an Open Source project.  The fact that many, if not most, KDE League members are also members of the GNOME Foundation underscores that these members are not trying to have one project dominate the other.  What most are trying to do is support Open Source projects that offer their customers a choice.  Yes, it's all about <EM>choice</EM>.  So the media is getting it backwards -- these organizations were not formed to destroy one project or the other; quite to the contrary, they were formed to support both projects to help provide <EM>all</EM> developers and users with <EM>choice</EM>.\n</P>\n<P>\nNot to pick on this article in particular, but the second paragraph states that\n\"(Red Hat, VA Linux and Sun are behind GNOME, while SuSE, Caldera, MandrakeSoft, Corel, Siemens and Fujitsu are behind KDE.)\".  If you compare the founding member lists of the <A HREF=\"http://www.kdeleague.org/members.html\">KDE League</A> and the <A HREF=\"\">GNOME Foundation</A>, except for companies that focus specifically on one of the desktops (like Trolltech and KDE.com for KDE and Helix Code and Eazel for GNOME), the membership largely overlaps:  Compaq, Hewlett-Packard,\nIBM and TurboLinux (as well as Borland and Mandrakesoft) are members of both organizations.\n</P>\n<P>\nIn observing this, the author writes, \"Such a play makes no sense; each camp seeks to make its project the definitive Linux desktop\" and then goes on to write fof the supposed \"war\" between KDE and GNOME.  In case, as it seems, he is writing about the League, the statement is not correct.  It is the League's mission to make KDE <EM><STRONG>a</STRONG></EM>, not <EM><STRONG>the</STRONG></EM>, desktop standard.  Moreover, KDE is not limited to Linux systems.  Maybe it takes some out-of-the-box thinking, but isn't it imaginable that users can actually have a choice over a desktop, much as they do over virtually all other products they use?  Of course there\nis room for multiple desktop standards, just like there are different standards\nfor computer hard drives.  The KDE League exists to promote KDE so it can compete on its merits with other desktops, particularly proprietary ones.\n</P>\n<P>\nIt is fundamental precept of Western (and at this juncture most of the world's) economic systems that choice -- and hence competition -- is good.  For some reason, perhaps due to the recent domination of the PC software industry by one company, this truism seems to have been lost on many people.  With respect to desktops they can think only in terms of the Highlanders:  \"There can be only one\".  But quite to the contrary, no one desktop can or should satisfy all users.  There is no intent to make KDE \"defeat\" GNOME; the League simply wants a level playing field with other desktops -- particularly proprietary variations that have huge marketing budgets and extensive marketing campaigns -- so that developers and users can make an informed choice rather than having to settle on some default imposed by a mega-corporation.\n</P>\n<P>\nSo truly speaking it is not the existence of the KDE League and the GNOME Foundation which \"intensif[y] the confrontation and makes coexistence that much harder to achieve\".\nIt is the fact that the media fixates on a non-existent \"war\" between KDE and GNOME that ferments these unfortunate consequences.  Members of the KDE League understand that no one desktop is right for everyone, just like no one hard drive is right for everyone.  Hardware vendors do not start a \"war\" by advertising and offering their customers the choice of SCSI and IDE hard drives and software vendors do not start a \"war\" by advertising and offering their customers the choice of Linux and Windows; similarly, nothing about supporting both KDE and GNOME is inconsistent, confrontational or counterproductive.\n</P>\n<P>\nIn short, the members of the League and the Foundation are supporting two Open Source projects that offer users and developers excellent and free (as in speech) choices.  And that, my friends, is a very great thing.\n</P> \n<P>\nAndreas Pour<BR>\nChairman<BR>\nKDE League\n\n\n\n"
    author: "Andreas Pour"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-24
    body: "The original raison d'etre of GNOME was to make war on KDE. While RMS has made some public statements effecting reconciliation between the two camps, it is a fact that war sells more ad space than peace. It is also true that ZDNet and many other mainstream tech news outlets are themselves complicit in the wintel duopoly and therefore find that covering the war serves their editorial interests, even if the war is only still being waged by an infinitesimally small minority of extremist wackos on the GNOME side. <br><br>\nIf GNOME is really serious about reconciliation (I am assuming that KDE is, of course:-), perhaps official cooperation for the purpose of interoperability would help to stem the flow of media disinformation on the subject. By official, I'm thinking some kind of joint working group whose members are commissioned by the League and the Foundation. This joint working group would not be authorized to dictate policy to either side, but it would be a formal venue for the discussion of interoperability.<br><br>\nFailing this, I think a formal declaration of the cease-fire is absolutely necessary if we are to have any hope of shutting up the media warmongers. Such a declaration would have to contain a clear statement that members of either group who are found to be creating strife or encouraging the belief in a war between KDE and GNOME will be excluded or otherwise censured.<br><br>\nI know the media won't cover this any more than they covered RMS's statements of a few months ago. Even so, if news of a cooperative effort or  a formal policy of non-aggression were prominently and permanently displayed on the major KDE and GNOME websites, it would go a long way toward demonstrating to the public that the war is over and peace has broken out. I realize this is a lot to ask, but both camps have a lot to gain by making their mutual respect known to all."
    author: "Joe Forbes"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-25
    body: "There's already a mailing list for issues of interoperability with KDE and GNOME developers. Those, who don't consider this enough of a statement from both sides, will never be satisfied. Even if you add additional management structure or whatever it is you are proposing.\n\nMaking a statement about cease-fire doesn't make more sense either. First, for the reason already stated. Second, it also would mean there WAS a war going on and AFAIK, this simply isn't true. There were outbursts from both sides but by large the big mouths were zealots from both sides. Just as english soccer clubs can't do much about behavior of their fans, KDE (and GNOME) can't do more about behavior of their users.\n\nAnd it's hard to exclude someone from voluntary project. What would you like to do? Ban their code? Censor their messages? It's hard for me to find a better way to destroy this two projects than this.\n\nI'm sorry if this is going to sound rude, but get real. Not only won't your proposal help a bit with the media, it will just give conspiracy wacos more fuel (you know, \"where's smoke, there's fire\" sort of thing).\n\nI appreciate your intention, I really do. However, I think it's a very bad solution what you are proposing."
    author: "Marko Samastur"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-25
    body: "You're probably right that the media just won't pay any attention if they haven't already. They have their anti-Free Software axe to grind, and don't show any signs of quitting. The statement about a cease-fire doesn't have to use martial terminology. I am only using those terms because those are the terms being used by the media. A simple statement that {KDE | GNOME} acknowledges the accomplishments of the other and that both are dedicated to the common goal of making Linux easier to use would be sufficient.<br><br>\nIf you think there wasn't a war going on, you have missed a lot of GNOME's history. They were started by RMS with the specific aim of destroying the KDE because it linked to the closed-source (but free of charge) Qt libraries. Now that Qt-Unix has been released under an Open Source license, RMS has made an ostentatious display of calling for an end to the hostilities. War is perhaps a confusing word for it, because it was waged primarily by one side for pseudo-religious reasons; it might be more appropriate to call it a jihad.<br><br>\nIt's good that there is a mailing list for interoperability. However, a mailing list does not a working group make. If I were going to develop an app for KDE that I wanted to work nicely with GNOME, there would be no central place to look for information, etc., etc. I'm not suggesting an additional layer of management (ick!), but a simple website and a small, possibly self-appointed, group made up of developers from each side who are experts on designing for interoperability between the two desktop environments. It could be started by interested participants in the mailing list and could be as simple as a log of the messages sent to the list and a search feature.<br><br>\nOn reflection, I agree that exclusion of contributors to a voluntary project would be problematic in the extreme. It was an emotional suggestion and I was wrong to make it. However, censure of some kind may have its place. Even if it is just an informal policy of sending e-mails to the offenders to let them know they are not helping matters by stirring up trouble, that might help to convince them that nobody else wants to participate in this internecine conflict."
    author: "Joe Forbes"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-23
    body: "In fact, I was mildly shocked when I first heard of the Gnome Foundation. In my opinion KDE is an excellent piece of work, based on an equally excellent framework, and I just couldn't understand why so many big companies seemed to think different or ignore it. To see most of those also join the KDE League is very reassuring and just fair. Thanks to the people behind the scenes for this initiative. I think you did it the right way."
    author: "Alexander Kuit"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-23
    body: "<p>1st let me say that it has been a pleasure for me reading Chris' understandable and good explanation of the KDE League.</p>\n<p>Unfortunately Andreas Pour seems to be absolutely right when criticizing the 'media' here:</p>\n<p>\n> Unfortunately it seems that regardless of what we say about the League the media is determined<br>\n> to \"spin\" the League into a confrontation with GNOME.</p>\nMight it be that /some/ journalists just don't realize their writing being both <u>incorrect</u> and <u>harmful</u>?</p>\n<p>Harmful to the Free Software community, to all the people writing and/or supporting free software!</p>\n<p>The winner of such articles will not be KDE nor GNOME but those people telling others that Free Software is not reliable.</p>\nA few weeks ago Byron Sonne posted his opinion on this to osOpinion, that IMO clarifies the issue very well: <a href=\"http://home.snafu.de/khz/Sonne/A_contra_B.html\"><img src=\"http://home.snafu.de/khz/Sonne/espagnol.png\" border=0></a> <a href=\"http://home.snafu.de/khz/Sonne/A_vs_B.html\"><img src=\"http://home.snafu.de/khz/Sonne/english.png\" border=0></a></p>\n<p>Karl-Heinz<br>\n-- <br>\n\n\"Why do we have to hide from the police, Daddy?\" \"Because we use vi, son. They use emacs.\"<br>\nDave Fischer, 1995/06/19"
    author: "Karl-Heinz Zimmer"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-24
    body: "<i>Might it be that /some/ journalists just don't realize their writing being both incorrect and harmful?</i><br><br>\nDoubtful, considering the following statement from the same post by Andreas:<br><br>\n<i>I posted a talk-back to it which has not yet appeared, so I will repost it here.</i><br><br>\nAt the risk of repeating myself, war sells more ad space than peace. (There I went, I repeated myself. I guess that's what happens when you take risks) Furthermore, I think the advertisers on ZDNet and other mainstream tech news outlets, many of whom are commercial software vendors, don't want Linux to do well because they don't want to have to compete with Free Software. Even if they are not exerting pressure, it's clear that the news outlets know which side of the bread their butter is on. I mean, it's almost like they have all seen the mutating penguin ad and decided they could curry favor with Redmond by bringing their editorial policy in line with it.<br><br>\nThere are a few things that could be done about this:\n<ol>\n<li>Supporters of both groups should flood the news outlets with articulate, level-headed, extremely negative feedback whenever they pull this nonsense.</li>\n<li>More Linux companies should advertise with those news outlets who cover KDE- and GNOME- related news without mentioning the non-existent war.</li>\n<li>Those Linux companies who do advertise with these outlets should make it clear that they don't appreciate this kind of malicious misinformation.</li>\n</ol>\nMost important of all, those who hope for KDE and GNOME to succeed must take this issue seriously. If enough of the potential users believe the war exists, it may actually break out again through simple inertia: \"I like {KDE | GNOME}, there is a war between KDE and GNOME, therefore I must hate {GNOME | KDE} and do anything I can to make things difficult for them.\" Such a scenario must be prevented if at all possible."
    author: "Joe Forbes"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-23
    body: "Thanx for this explanation.\nfor me, you are on the right way. go ahead!\ni am promoting kde on my personal way: a few days ago, i installed linux/kde2 on a friends machine. explained 5-10 minutes how to log in and she is doing here internet stuff without porblems.\n\nnext thing is to install linux/kde on all computers in our training-centers. teaching linux/kde will be a great fun!\n\ngunnar"
    author: "Gunnar Johannesmeyer"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-23
    body: "Will someone please post more detailed information about the supposed \"studies\" that indicate KDE owns 70% of the Linux desktop market.  I have seen no such \"studies.\""
    author: "Jamin P. Gray"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-23
    body: "Actually this number doesnt matter if you read the above and it was a mistake to mention it."
    author: "Lenny"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-23
    body: "As if anyone really thinks that KDE had conceived of this idea before the GNOME Foundation was announced, but just \"forgot\" to work on it whilst KDE 2.0 was being developed. Yeah, right, and the moon is made of cheese. Green cheese at that.\n\nThe idea of promoting KDE to a non-Linux audience is a worthwhile one, but surely it would be better for Linux as a whole to do this, and not for separate organisations to promote their own devisive agendas. Linux has already suffered in the eyes of corporations for being too fragmented, these people want standards, not multiple and incompatible \"choices\". Having two separate Foundations or Leagues or whatever is just going to exacerbate the situation.\n\nWe need a standard for the desktop, but it's not going to happen whilst KDE and GNOME are busy engaged in a pissing contest over control of the Linux desktop. Time to grow up people, before Microsoft eat your 5% market share for breakfast."
    author: "anti-kupolu"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-23
    body: "So we have learned to copy&paste from <a href=\"http://slashdot.org/comments.pl?sid=00/11/22/1850237&cid=7\">slashdot</a>. How...impressive."
    author: "ac"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-23
    body: "So we have learned how to be ignorant. How impressive."
    author: "kupolu"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-24
    body: "A few questions: What exactly do you mean by \"grow up\"? Are you suggesting that one group or the other should disband? Do you think GNOME will voluntarily break up the GNOME Foundation? If they don't, should KDE unilaterally close down the League? What is so wrong about having a separate PR group for each project, anyway?<br><br>\nThe desktop environment is for most users the most important and easily recognizable component of their system. If KDE thought up the idea of a creating a modern (i.e., not CDE) free Unix desktop environment, that's great, and it was that event that caused RMS to bring GNOME into existence. If GNOME thought up the idea of organizing to better get the word out to the masses, it seems to me that that idea should be viewed as a contribution to KDE's PR strategy.<br><br>\nUnless and until the distros line up each on one side or the other (Heaven Forbid!), it will be critical to have a focused marketing message based around each of the desktop environments if we expect to make any headway against Microsoft. Really, insofar as the success of either KDE or GNOME will increase the Linux install base, it will contribute to the success of the other, provided of course that neither side renews hostilities by creating incompatibilities to prevent the curious user from trying out the other desktop environment.<br><br>\nThe issue of fragmentation can be addressed without disbanding the League or Foundation:\n<ol>\n<li>Make each product highly configurable, so that they can be made to look and act like each other. (Oh, wait, this is already largely the case. Don't you just love Linux?)</li>\n<li>Do everything possible to make programs from each environment interoperable with the other.</li>\n<li>Trumpet these accomplishments and the mutual respect of each group for the other as loudly as humanly possible.</li>\n</ol>\nI think you have been reading too much of the dishonest and malicious news coverage of the war if you see a pissing contest going on. Either that, or you are paying too much attention to the worst elements on both sides.<br><br>\nTry to look on the bright side. If both sides have PR armies, that doesn't necessarily mean they are fighting each other. All of the countries involved on both sides in WW's I and II have armed forces of some kind, but that hasn't caused any outbreak of hostilities between them. (With the obvious exception of the trouble in Yugoslavia, but that's an isolated example and all the major European players in WW's I and II came in on the same side, despite their differences.) This is because they realize it wouldn't serve any legitimate purpose to start another war.<br><br>\nPrecisely the same thing is going on now between KDE and GNOME. Disbanding the KDE League and the GNOME Foundation would only make both projects more vulnerable to continued domination by their mutual enemies. This would hurt both and all of their allies in the Free Software community. It is for this reason that so many companies have joined both the League and the Foundation. They have thought long and hard, you can be sure, about the implications of having PR groups for both desktop environments, and have come to the conclusion that the existence of these groups is good for everyone in the Free Software community. Maybe they're just a bunch of idiots for joining these groups, but if that's the case, we're probably doomed anyway."
    author: "Joe Forbes"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-24
    body: "There's no point in trying to reason with him.\nHe's from Slashdot, he posts rants, flames and stupid comments for fun."
    author: "Anonymous"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-24
    body: "I read Slashdot pretty regularly, too, so careful with the anti-Slashdot comments, please. Not everyone on Slashdot is a jerk. It may be that I can't reason with anti-kupolu, and I appreciate the heads up. However, if you need to tell people he's a troll then it's important to respond to his statements so that those who do not yet know his reputation will not be swayed by his arguments.<br><br>\nComplacency in the face of reasonable-sounding but wrong-headed views does nothing to stop the spread of those wrong-headed views. We need only look at the outcome of the CP/M vs DOS and Unix vs Windows debates to demonstrate how important this is.<br><br>\nNow, if he were to make comments about Natalie Portman and hot grits, or if his posts were littered with obscenity, then we could safely ignore him. As it is, I think an effort should be made to answer his viewpoint with an articulate arguments that address any reasonable-sounding points he might make."
    author: "Joe Forbes"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-24
    body: "Oops. \"...an articulate arguments...\" Unfortunate, that. Sorry."
    author: "Joe Forbes"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-25
    body: "You're missing the whole point, actually a couple of whole points. First, you speak as if this whole desktop thing is a matter of survival for Linux. Linux survived and even prospered without corporate backing in the past, and I believe it will continue to do so regardless of the existence or lack thereof. Those of us who have been using Linux for a long time had to get used to the fact early on that the corporations who had even heard of it thought of it as a toy that would never amount to anything. That was ok. We were having fun. Most of us are still having fun. Which brings me to another point. If you read the announcement, you'll find it's about the KDE developers having fun. I'll bet they're not sitting in front of the monitor generating stomach acid over whether Corporation X likes them or not. We Linux folks are convinced we have a better way than Windows. We are convinced that if corporations look at what we've got, they'll think it's a better way too. We're more than happy to welcome them aboard. When you say that \"these people want standards, not multiple and incompatible 'choices'\", it makes me wonder just whose playbook you're reading out of. Sounds like something Ballmer would say. The Linux desktop world is not a zero-sum game. As long as Gnome folks are having fun with Gnome, Gnome will survive and continue to provide them with fun. The same goes for the KDE folks. For those who see it as a pissing contest, well, maybe it's because they're the ones producing the piss. And as for growing up, those of us who have realize that that the real world is not a game of Doom. You don't have to kill everybody else to have fun yourself."
    author: "Rod Wright"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-25
    body: "While I agree that the parent poster sees a pissing contest where there is none, I feel I must differ with you on one point. The desktop thing is not a matter of survival. It is part of a strategy for expansion out of the server market that Linux is currently doing so well in. If we want Linux to continue to be a marginal player *in the desktop market*, the status quo is fine. If we want Linux to push Windows off of more corporate desktops, something must be done to combat the charges that Linux is too fragmented and contains a lot of mutually incompatible products. Anything that makes easier the IT director's job of convincing the pointy-haired bosses that Linux is a safe bet will help the cause of increasing the Linux install base."
    author: "Joe Forbes"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-28
    body: "I don't think Linux as a whole will ever be a completely mutually compatible collection of products. There are just too many people who want too many different things from it. If the goal is to replace Windows on the corporate desktop, then maybe the only thing to do is take Linux away from the hobbyists, put it under some single controlling authority, and appoint a committee to decide how it will look and work. In which case it becomes just another Windows. There is as much chance of getting all the Linux folks one one sheet of paper for the pointy-haired boss as there is of getting them all to cut their hair and put on a shirt and tie to look respectable for him. If you want a cathedral, don't ask the bazaar goers to build it for you. The key really is not uniformity, which will never happen, but interoperability, which is entirely achievable. What businesses don't want is somebody e-mailing a spreadsheet to somebody else in the organization who can't read it because they're not running the same operating system, or because the Gnome spreadsheet app has a certain file format while the KDE spreadsheet app has a different, incompatible one. The e-mail got there successfully, so we know interoperability is possible. What needs standardization is not the desktop, but the ways in which information is exchanged. Let everybody see the same information, but let them choose what desktop they want to see it on."
    author: "Rod Wright"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-28
    body: "Certainly let them choose. I don't think that anyone's choices should be limited. Interoperability is definitely more important than consistency. Granted all of that, it is still critical that Linux do everything it can to take advantage of its flexibility by being able to emulate the user's expected behavior and by not requiring they become experts on finding and editing config files, etc., etc.<br><br>\nWhile it may be wrong-headed of the first poster on this thread to suggest a single standard, it seems to me narrow-minded to deny his concerns and just say that everything is OK. If Linux is to increase its install base, it will need to do something about the top-level poster's concerns."
    author: "Joe Forbes"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-24
    body: "If you take an even wider view, there are now about 6 billion people on this planet. Those using desktops are a minority (maybe 5% of world population) compared with those using mobile phones (maybe 10% of world population) or those using phones at all (maybe 25% of world population). There is still plenty of scope for growth, assuming world economic growth can continue, and assuming that people will want and need devices of some type to communicate and compute with.\n<br>\nThis may explain why is there is so much recent interest in embedded Linux and other embedded software.\n<br>\nSee also (eg.) <A href=\"http://www.ac.com/ecommerce/mcommerce_trends.html\">Marketplace Trends in mCommerce</A>"
    author: "Paul C. Leopardi"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-24
    body: "More to the point, see <A href=\"http://www.trolltech.com/company/announce/ericsson.html\">this announcement</A>\n<br>"
    author: "Paul C. Leopardi"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-24
    body: "KEEP GOING! :)<br>\nThere is one thing, though, that need some attention. The \"soon-ex-M$-users\" often get very frustrated when I talk about Linux and KDE, because they are so used to a system that merges the base and the GUI. I often have meny listners at my school, because they don't trust M$ anymore and think something alternative is something good (typical Danish mindset :), but they get totaly cross-eyed when i start to explain what a GIU is. I get questions such as, \"but isn't the GUI just the computer?\" (sigh). <br><br> This has let me to the idea, that KDE might need it's own official \"KDE-linux\". It would help all the newbies alot, if there were a desktop orented (DTO?) Linux around. I imaginge a simgle CD distro, with basically no servers and only the best of the best of apps. A very ('xcuse me) M$-like distro where configuration is 100% done inside KDE. In other words: if it hasn't got at frontend, the user won't need it. <br><br> I know this is a huge task, but hey... so was KDE 2.0! If anyone has knowledge/skills in assembling a distro I would very much like to have a mail so I can start reading the right stuff.<br><br> Rome wasn't biuld in one day :)<br><br> /kidcat"
    author: "kidcat"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-24
    body: "An official KDE-linux might strain relations with the existing distros, which would be bad. It would also take valuable resources away from current and future core KDE projects. I have to say that I think any distribution that doesn't offer packages for both desktop environments is probably a bad thing for KDE in that it might lead to a renewal of the GNOME jihad against KDE.<br><br>\nAll that having been said, your concerns are certainly valid. However, I think a better approach with non-Linux users might be to skip the discussion of GUI vs OS vs distro. Just say that they can get KDE from all kinds of different places, but that they are all KDE and they are all full-featured, easy to use, and easy to configure.<br><br>\nI understand that Mandrake is a very Windows-like distribution in that they are making efforts to provide the seamless user experience you describe in every desktop environment they package for their distribution. I am sure that many other distros are making the same effort. I suggest that the KDE team leave the creation of distributions to the distributors and concentrate on making KDE the best it can be. (Cut-and-paste, anyone?) The modularity and limitless choices of the Linux world are its greatest strengths; let us not muddy the waters with a KDE version of Linux."
    author: "Joe Forbes"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-25
    body: "I get your points... But I'm not done whith the KDE-Linux idea however :) Maby I'll just cal it \"Office-Linux\" and still base it on KDE... that would just piss of all the GNOME fans, and not the other distros. The reason I posted my idea here is because KDE has a very wellworking machinery. And I hoped that everybody wouldt scream, \"why didn't we think of that\" ;). <br><br> The only thing important to me is killing M$, and I find it sad that bureaucracy and politics has poisoned the opensouce community ;....( I think we should all gang up on M$, cut it's throat <b> and then</b> let the thousands of  flavours flourish :) <br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-25
    body: "<i>...piss [off] all the GNOME fans...</i><br><br>\nI think upsetting the GNOME fans is precisely what we should work to avoid. Why do you want to stir up that particular hornet's nest? Have you read the letter at the top of this discussion, or any of the other people's comments?<br><br>\nI don't mean to be harsh, but the theme here is cooperation with the GNOMEs, not the renewal of hostilities with them. The GNOMEs have historically been a lot better at getting media attention, whether because they are based in the States, or because they are generally more loudmouthed, or because they were organized by RMS. Upsetting them will just cause them to start up the anti-KDE PR machine again. I don't think the leadership of KDE want that to happen.<br><br>\nWhat attracted me to KDE in the first place was that they seemed more interested in the product than in PR. The GNOMEs, in contrast, seemed a little to ideologically centered for my taste and a bit full of themselves. While I agree that tweaking them might have an emotional appeal, on further reflection I think you'll see that it will not serve the true interests of the KDE project.<br><br>\nBesides, if you create a KDE-based distro, even if it is not \"official\", with the collateral purpose of upsetting the GNOMEs, how does that help KDE and GNOME get together and gang up on Microsoft? Bureaucracy and politics are part of being a mature community, unfortunately, perhaps, but also unavoidably. The only way we can keep politics and beauracracy out of the Open Source community altogether would be to stay on the periphery, which is not really in keeping with the whole idea of expanding the Linux install base."
    author: "Joe Forbes"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-25
    body: "I don't want to piss of<b>f</b> the GNOME, ohh no I don't. On the contrary. But as long as there is two horses, I'll have to bet on one of them. And since KDE2 is the easier choise... As I have understood it GNOME is mainly used by the people who still bitch about calling Linux GNUIX. IMHO people who use (badly generalized) Debian/GNOME are the kind that flame newbies to hell on a good day, which is obviously a good thing for M$. I speak of own bad experinces. Now if-<b>if-<i>if</b></i> GNOME and KDE got together and bouild \"K-GNOME-DE\" I would clap my little paws :) (That would be so good!). But until then the lamers <b>need</b> a Linux/\"DTE\" whith pre-configured office+everything that does not install +2GB if you choose Standard Workstation. It would be a bad thing if this was not possible without starting a civil war over it.<br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-25
    body: "Agreed that GNU/Linux is a ridiculous and unwieldy way of referring to Linux, and that those who insist on doing so are a bit silly. Two things, though: Debian is now offering packages for KDE2 in their unstable branch. The only reason they are in the unstable branch is that they were not included in the first release of the current Debian distro. They will be stable in the next major release of Debian. And I would find it easier to swallow the KDE-based single-cd very-few-installation-choices distribution thing if there were a companion distro that used GNOME, or at least packages designed or compatible with the KDE-based distro so that those who wanted to could try out GNOME.<br><br>\nBTW, the \"[off]\" was not meant as a dig at your English. I must say your English is a great deal better than my Danish. You Europeans have it all over on us Americans when it comes to languages."
    author: "Joe Forbes"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-25
    body: "It seems that we agree on most points here :) And I am sure that the GNOME's arent lazy, so if dot.kde.org announced that linux.kde.org was comming up they wouldt probably have something equal (or better?) up in say... 3 days :). And if both projects build on the same \"skeleton\" it would be a huge step towards full inter-compatibillity. And yes, package design should make it easy to install GNOME on \"KDE-Linux\"... just as anything else should be easy to install. (<b>I</b> think RPMs would do). The goal here is not to make KDE win but to make M$ looooose :). I think it would be a <i>great</i> thing if there were both a GNOME-Linux and a KDE-Linux, which each showed their respective strengths. And it would be even greater if you had one installed and inserted the other cd.. and viol\u00e1 both DT's on the same machine. But as an element of advertizing it would be easier to the lamer if he picked two cds of the shelf, looked on the backsides... and put the one he thought looked the coolest in the basket.<br><br>I think that the \"strategy\"-folks from both KDE and GNOME should get together and drink a bunch of beers whilest discussing a stradegy that resembled the above (beerdrinking has let to <i>a lot</i> of concensus in Denmark :).<br><br>This has been a very fruitfull discussion to me.. thx. And no offence taken on the typ\u00f4 :)<br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-26
    body: "We do seem to agree on most things, at least on this subject;-)<br><br>\nTo do a quick-and-dirty version of what you describe, all one would have to do is modify the graphical installer to a mainstream distro (e.g. RedHat) a bit to remove the screen where one chooses the install type (in RedHat, KDE Workstation, GNOME Workstation, Server, Custom etc.) and use KDE in one version and GNOME in another. The next thing would be to make different boxes on which the desktop environment included was the main theme of the design. The distribution (Kidcat, I imagine) would just be a tiny logo in the corner, the rest of the graphics would be KDE or GNOME.<br><br>\nThis would make it easy for someone to pick the distribution they wanted based on their choice of desktop environment, which is the easiest way to choose for newbies. Of course, to do it right would take a bit more work. I can envision a whole suite of distribution disks, like KDE Workstation for Windows users, KDE Server for Windows users, KDE Workstation for Mac users, KDE Server for Mac users, GNOME Workstation for Windows users, etc. Each version could have a pre-configured desktop environment with UI settings and a combination of applications chosen to match as closely as possible the configurations and applications on the other OSen. Switching from one to another would be as easy as picking up the appropriate package from the internet and installing it. In this way, the Linux community can capitalize on all of its much-vaunted flexibility.<br><br>\nUnfortunately, my kung fu is not currently even good enough to get the quick-and-dirty version done, but I will certainly be the first to tell anyone who listens to run out and buy their chosen flavor of Kidcat Linux if you ever get around to doing this. I might even be persuaded to read the Distribution-HOWTO and work on this with you if you wouldn't mind having a somewhat retarded and distracted (and yes, occasionally inebriated) partner on the project. I have some boxen and much disk space for testing with, so if nothing else, I'd be glad to test installers. I suppose I could also work on desktop themes for emulation of Windows and Mac (or for that matter GNOME look-and-feel in KDE and vice versa). Drop me a line if you get started on this and think there's something I could do to help."
    author: "Joe Forbes"
  - subject: "KDE Linux"
    date: 2005-08-12
    body: "In general, i agree with the entire first article. There _should_ be a kde based distro made from kde only. This is what i would use. And as for the GNOME discent, why not simply make a GNOME based distro as well (or at least propose the idea of an entirely kde based distro and a seperate completely gnome based distro)\n\nOr even if a third party (non kde-dev) makes this distro. I would be the first on the list for it.\n\nThe only thing i dont agree with is the lack of command line tools. After all it is unix - there needs to be some cl tools (most notably gcc at least) and many of the graphical tools require command line tools as backends.\n\n"
    author: "nick"
  - subject: "Re: KDE Linux"
    date: 2005-08-12
    body: "Are you aware that you have replied to a very old news article?  \nDon't expect too many people to read this. \n\nOn topic, you may want to take a look at the two distributions Ubuntu (GNOME-centric) and Kubuntu (KDE-centric) that both get a lot of hype lately...\n\nHere's the kubuntu homepage: http://www.kubuntu.org/\n\n"
    author: "cm"
  - subject: "If the tables turned..."
    date: 2000-11-24
    body: "<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\n<html>\n<head>\n   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n   <meta name=\"GENERATOR\" content=\"Mozilla/4.73C-CCK-MCD {C-UDP; EBM-APPLE} (Macintosh; U; PPC) [Netscape]\">\n   <title>Untitled</title>\n</head>\n<body>\n&nbsp;&nbsp;&nbsp;&nbsp; I believe the formation of the KDE Leage\nwas nessessary.\n<br>&nbsp;&nbsp;&nbsp;&nbsp; But, the KDE League has to remember&nbsp;\nthat if the tables were turned, and KDE had the minority share of the Linux\ndesktop market (~20 - 30%), it would be a whole different story. KDE would\nall of a sudden be on the defensive.\n<p>&nbsp;&nbsp;&nbsp;&nbsp; By the way, i got my info from recent polls\non themes.org that <b><font color=\"#EF1F1D\">clearly</font></b> show KDE\nahead.\n</body>\n</html>"
    author: "Mac+"
  - subject: "Re: If the tables turned..."
    date: 2000-11-24
    body: "Polls are not always correct.\nI've also seen many sites that shows Gnome as a clear winner.\nPolls can't really be trusted as a reliable source."
    author: "Anonymous"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-25
    body: "I like the choices of desktop I have.\nAt home, more than one user uses my computer. Why should those other users be locked into my choice of interface.  In fact, I often log in as different users for different purposes.  When I want to play a game, for example, I log in with a nice simple icewm.  I'm not gonna be publishing any web pages or writing any letters while I'm busy capturing the flag.  (And thanks to kdm, this is very easy to do.)  I think the \"benefits of choice\" are what KDE/Gnome/other/Linux users should argue in a huge way to Win/Mac users.  I wouldn't have it any other way, now that I have choice."
    author: "Scotty Orr"
  - subject: "Re: The KDE League, Explained"
    date: 2000-11-28
    body: "KDE sucks!\nGNOME rules!\nKDE is just a lame windows ripoff"
    author: "gnome-user"
---
<A HREF="mailto:cs at suse.kde">Chris Schlaeger</A>, one of the two core KDE developers most active in the formation of the <A HREF="http://www.kdeleague.org/">KDE League</A>, has written a tome explaining the motivation behind creating the League and why it was done in secret.  What it all boils down to:  have fun and spread the code!

<!--break-->
<BR>
<P>
Hi developers, translators, doc-writers and artists!
</P>
<P>
I just returned from Las Vegas  where we announced the creation of the
KDE League. Since  I was involved in setting it up  and was present at
the  initial board meeting  as well  as at  the press  conference, I'm
probably a good candidate to tell  you a little bit more about the KDE
League and it's purposes.
</P>
<P>
Since the creation  of the KDE Project in October 1996  we have made 5
official releases. With each release we found more users but also more
people  who are willing  to devote  their time  to improve  KDE.  This
growing popularity assured us that we were doing the right thing.
</P>
<P>
Moreover, the technology  behind KDE 2.0 would not  have been possible
without all those  excellent folks that joined the  project after they
discovered KDE 1.x.  The same is happening again. Since KDE 2.0 is out
we have new people getting interested in the project on a daily basis.
Many  of  them  will probably  be  key  contributors  to the  KDE  3.x
technology.
</P>
<P>
Why  are we  creating  KDE?  Simply for  the  fun of  it!  We spend  a
significant amount  of our time on  KDE simply because it  is fun. One
can write  a simple text editor  alone, but for  a network transparent
desktop  you need  many programmers.  The  more bright  people we  can
attract to the project the  more interesting stuff we can achieve. And
that's the reason  why it is important for us to  spread the word. The
more contributors the more challenges  can be met, the better KDE will
become and the more fun we will have.
</P>
<P>
So  promoting KDE is  almost as  important as  working on  KDE. Recent
studies show that  KDE is used on more the 70%  of all Linux desktops.
We could fight  for those remaining 30% but given  that Linux has less
than 5% of the overall desktop  market we should rather target the 95%
of desktop users than compete with our friends from the GNOME project.
Just converting  5% of Windows users  will get us more  KDE users than
converting all  GNOME users.  But  those users know little  about Open
Source, Linux  or KDE and posting  to some mailing  lists won't change
this.  To address  those users  we have  to communicate  through other
channels that we have little experience with.
</P>
<P>
This was the  motivation for us to create  a professional organization
that would  promote KDE to  those people. So  we started to work  on a
concept early this year but  then got distracted again. Working on KDE
2.0 was just so much more fun. When the GNOME Foundation was announced
it was a bit  of a wakeup call again. It was  not the GNOME Foundation
itself that bothered  me, but the fact that  many people believed that
KDE was doomed  because it had no commercial  backing.  Many companies
have been supporting KDE for years now,  so I felt it was time to make
some noise about it. So Kurt  Granroth, Andreas Pour and I resumed the
work on  the KDE League. Since  we all already had  full-time jobs, it
was  quite a  challenge  for us  to  establish contacts  with so  many
companies. We set COMDEX Fall 2000 as a deadline.
</P>
<P>
Obviously some of our contacts were as busy as we are, and we were not
able to get  a final answer from all the companies  that we have asked
in time for the announcement.  Since we felt that the group was strong
enough to justify  an announcement we went ahead  anyway. VA Linux and
Red Hat are  two of these companies who for  various reasons are still
considering joining the League.
</P>
<P>
In all announcements concerning the  KDE League we have emphasized the
fact  that  it is  purely  a  promotional  organization.  It  is  very
important to  bear this in mind.  We have taken  every precaution that
the League will not get involved in development issues. As I mentioned
earlier  we are  working on  KDE for  the joy  of it.  If you  are not
actively  involved in the  project this  may sound  absurd to  you.  A
bunch  of freaks  cannot create  a  desktop system,  a full-blown  web
browser  and  an object  model  simply  because  they have  fun  doing
so. Yet, KDE is living proof that this actually works.
</P>
<P>
Anything  that can  spoil the  fun  is potentially  dangerous for  the
health of the project. We do not want a steering committee or advisory
board that tells us what to do and what not to do. We also do not want
to have  people in the team that  work on KDE because  their boss told
them to do so.  They lack the  enthusiasm that we have and try to push
their corporate agenda. This causes friction and frustration that must
be avoided. Corporate developers  can certainly join and contribute if
they  get satisfaction  from  doing so,  just  like the  20  - 30  KDE
developers who have been hired by KDE friendly companies.
</P>
<P>
The KDE League  is a not-for-profit corporation controlled  by a board
of directors and an executive committee. Each member is represented in
the  board  of directors  and  KDE  representatives  hold 50%  of  the
votes. The  executive committee consists of 3  people (currently Eirik
Eng, Andreas Pour and me) that  control the daily business and have to
make decisions unanimously.
</P>
<P>
The  KDE  League has  a  budget of  around  US$120.000  for the  first
year. This is  ridiculously small by any corporate  standard given the
size of our project. From this  money we will hire a PR firm, organize
KDE  participation on  up  to 6  major  Linux shows,  pay a  part-time
employee and  some other expenses.  So  we do not intend  to annoy our
users with huge  marketing efforts. Instead we will  still rely on our
technical merits  and the  help of the  many volunteers out  there. We
simply are trying to reach a different kind of user that does not read
geek sites and mailing lists.
</P>
<P>
You may have noticed that KDE does not depend on any kind of cash flow
to survive. The  contributions from developers that are  hired to work
on  KDE are  spread  all over  our  core libraries.  If  any of  these
companies would discontinue their KDE support from one day to another,
it would hurt  us a bit, but it  would not be a disaster.  We will try
very hard  to protect our core  technology from any  kind of financial
dependency. Certainly the KDE League does not violate this guideline.
</P>
<P>
A  few people  have complained  that the  creation of  the  KDE League
wasn't discussed on the mailing lists. Forming a group such as the KDE
League  is a  very sensitive  process. One  has to  take all  kinds of
political dependencies into account  and pull some diplomatic strings.
Acting in a  corporate atmosphere does not match  our open development
process very well. Since the KDE League does not affect development we
believed  it was  acceptable to  act without  making much  noise about
it. We hope you will understand  this. Let me assure you that this was
an exception and  that we will continue our  open development process,
just like we have done very successfully in the past.
</P>
<P>
As you can see, the creation of the KDE League does not affect many of
us. If you have been promoting KDE in various ways, you are welcome to
continue  your efforts.   Your  support  has been  and  still is  very
valuable. We  will see how the  League can support KDE  User Groups in
the future.
</P>
<P>
I hope this will answer some  of the questions you might have. You can
find     more    information     about    the     KDE     League    at
http://www.kdeleague.org/.
</P>
<P>
Chris
</P>
<P>
-- KDE 2.0: Conquer your Desktop!  <A HREF="http://www.kde.org/">http://www.kde.org</A>
</P>
