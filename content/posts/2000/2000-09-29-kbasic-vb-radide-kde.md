---
title: "KBasic: A VB-Like Rad/IDE For KDE"
date:    2000-09-29
authors:
  - "t0m_dR"
slug:    kbasic-vb-radide-kde
comments:
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-29
    body: "I heard that GNOME has or is working on a VB implementation for xls/doc compatibility.  Can you use any of that work?"
    author: "anonymous"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-29
    body: "Seems like you've used many German words for identifiers etc. in the kbasic code. Is this really necessary?\n\nOther than that, I'm taking my first compiler course and kbasic is very interesting as it's always nice to see something in \"real life\". Good going! :)"
    author: "Haakon Nilsen"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-29
    body: "I was curious so I downloaded the source.  I couldn't find one German word in it."
    author: "anonymous"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "Look closer, in the parser code (bnf.cpp). Some comments and some method names are in German (\"TokensatzAktuell\", \"GrammatiksatzAktuell\"...). Anyway it's not really a problem since it really doesn't occur often (and I speak some German anyway ;), so it's really nit-picking. Guess I over-reacted :)"
    author: "Haakon Nilsen"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "Wait... uh... oh, I see. I must have been asleep the day God announced that everything to do with KDE or any programming in all the world shall be done in English.\n\nHow arrogant.\n\nAmericans need to learn how deal with the concept that they aren't the center of universe."
    author: "Xtian"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "I agree every programing lanuage and tool should be done in different languages.  That way I will be 10x more productive since I am an expert in every language in the world.  My point is that things need to be standardized.  I don't care if it is English or Swahili."
    author: "Jack Crack"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "I agree that Americans are not center of the universe. In fact, I'm Norwegian. That doesn't mean I write my open source code in Norwegian. Doing that would make it harder for non-Norwegians to get into the code. And when they decide to add some code using words in their own language, that would further the confusion... So we obviously need to agree on one language. We could pick a truly international language like Esperanto, but unfortunately I don't speak Esperanto. If you'd like, you can go with British English, since the Brits don't believe they're center of the universe (guess the fall of their Empire taught them a thing or two (sorry, I couldn't resist ;)).\n\nHowever, this is completely off-topic :)"
    author: "Haakon Nilsen"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-10-01
    body: "Yea, lots of things like that are based on british rather than american. Well, we got the meridian for time at least, so were the centre of time on this planet =).\n\nEven though our empire collapsed, we still know were the centre of everything, just us english tend to be very modest about it ;)."
    author: "Nick Ives"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-10-02
    body: "Americans did not decide our language was to be the defacto standard for software. Saying so only shows a persons' smallminded animosity. The reason so much software is in English (Note English NOT American!) is because such a large portion of the Computer Using world speaks english! The U.S., Canada, The U.K. and Australia. Supply and demand Pal, If China has 50% of its populace online, we'd all be speaking Chinese!"
    author: "Daedalous"
  - subject: "Cooporate with Gnome"
    date: 2000-09-29
    body: "The Gnome team is working on Gnome Basic.\nWouldn't it be better to work together with them instead of reinventing the wheel?"
    author: "Anonymous"
  - subject: "Re: Cooporate with Gnome"
    date: 2000-09-30
    body: "i'd like to see kde co-operate and work /with/ gnome instead of poking fun at them for a change.  if not on the language, at least with the api that the language uses to interface into things."
    author: "ac"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "I do not see any needs for another IDE for KDE.<br>\nWhy won't you guys cooperate with the KDevelop Team and incorporate a good GUI Designer and KBasic language output for that thing? (you can still \"invent\" your KBasic language then)<br>\nSo much work is being done over and over and over again, but not many things reach high release numbers on Linux, especially user applications.<br><br>Please put your energy in KDevelop, don't reinvent the wheel."
    author: "Heiko Stoermer"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "Well, I suspect that they will use a lot of Kdevelop's code. Then again, Kdevelop2 is nowhere near finished yet, so they might have to really mess about with the code in order to get it to work properly."
    author: "ac"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "I totally agree about reusing KDevelop or KStudio,\nI also think QtDesigner should be used as the common GUI Designer, so that it's XML output could be translated to any language implementation...or better: why don't use an approach similar to Glade, so GUI definition could be loaded at run-time and modified without recompiling."
    author: "Carlos Miguel"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "I'd love to see this.  Visual Basic 5 is the <b>only</b> thing I miss from Windows."
    author: "anonymous"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "VB/VBScript etc is the thing i hate MOST about windows ;)"
    author: "anon"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "Yes indeed. Urgh."
    author: "anonymous"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "They could do worse than pick up the better features of REALbasic. Its optional event-based class inheritance model is *inspired*.\n\nA better VB than VB. Hmmm."
    author: "Guyren G Howe"
  - subject: "Basic ?"
    date: 2000-09-30
    body: "This is going to sound like a troll, but it isn't meant as one. It's just my humble opinion. And this is a opinion forum after all.\nI think KBasic and GNOME Basic are very bad ideas. Mostly because the worst programming memories I have are tied to Visual Basic, after which these will no doubt be modeled.\nAt first I was shocked someone would actually want to write this, but after viewing the member list I understood. Not a single member of this project has experience with Delphi. If they did, they'd be writing KDelphi.\nI know it sounds like favoritism. But my eyes really opened when I switched from Basic to Delphi. Before the switch, I thought Basic was cool, and nice to program in. After switching I couldn't understand how I stuck with the horrid thing so long. When compared to the inctricate complexity of C++ (especially Visual C++) Visual Basic might look like a good idea, but ...\nMy problem is not with them writing it, however. It's a free world (mostly). My problem is that this is obviously targeted at existing Visual Basic programmers. And most of those programmers also use VBA for every MS app out there, so they will be demanding Visual Basic integration with applications (like GNOME Basic is doing right now).\nThe last thing I'd want to see happen is that I'd have to resort to Basic (*spit* *gag*) to program macro's in linux office apps. And it seems to go that way more and more. It's not because MS marketing chose to use Basic as a back-end that we do too, and I hope we don't.\nI know it all seems distant future, and alarmist ramblings, but I do believe the odds are in favour of Visual Basic infiltrating the linux office environment. You can't teach an old dog new tricks, and there are a lot of old Windows dogs looking at linux, especially with the Windows gui-clone KDE (no offense meant here, people's preferences differ). And as we all know, majority wins, even if it doesn't know what it's talking about (eg presidential elections)."
    author: "Joeri Sebrechts"
  - subject: "Re: Basic ?"
    date: 2000-09-30
    body: "> If they did, they'd be writing KDelphi.\n\nThere are 2 flavours of this already in the works: Kylix (will be shipping this year from Borland) and FreePascal, a GPL'd clone that works as a command line compiler, IDE coming later. \n\nI could not agree more with Joeri Sebrechts. VB is *not* something to be emulated. IMHO it's not something to be touched with a bargepole. Please someone tell me that this is not meant as as a VB-wannabee. Who needs that?\n\nYes, I am a Delphi advocate. I tell everyone how good it is. I am a Delphi advocate *because* it rocks, not vice versa."
    author: "StrawberryFrog"
  - subject: "Re: Basic ?"
    date: 2000-09-30
    body: "Writing a VBA clone for Linux *is* a good idea. You can then leverage the thousands of developers who already know VB/VBA to write corporate productivity apps. VBA is all about speed of development and a short learning curve, even a crap programmer like me can knock up an app in an afternoon to solve some basic business problem and save $$$ over doing it manually."
    author: "barnaclebarnes"
  - subject: "Re: Basic ?"
    date: 2000-09-30
    body: "You can use a VB clone for the old bait&amp;switch.  Lure them over to the Linux side with the knowledge that they don't have to forget their VB training, then show them a REAL language.\n<P>\n(I'm presuming, here, that the various office replacements will give users a choice of plugin language to use)."
    author: "Stephen Samuel"
  - subject: "Re: Basic ?"
    date: 2000-10-01
    body: "I would have to agree with Joeri Sebrechts on this one. VB is a piece of crap, why would anyone want such a thing on Linux.\n\nKDelphi, now that would be interesting. Borland has already started to do a comercial version, but yes a free one would be better yet.\n\nWhat about http://www.freepascal.org/ \nCould this be used as a starting point for a KDelphi?\n\nLater,\nKJD"
    author: "Kyle Dansie"
  - subject: "Re: Basic ?"
    date: 2001-11-04
    body: "Actually, though I'm a longtime linux pro and advocate, I have recently been using VBA and Access to make some pretty nifty software for work. When it gets to the abstraction level such as VBA, all of the languages tend to blur into an OO mishmash.\n<P>Having said that, I think that Python should be the defacto scripting language for linux office apps. As far as interpreted languages go, it is pretty hard to beat Python. Imagine the fantastic things you could do with a python module and a database... easy to use Tk for quick dialog boxes and such, really easy dynamic arrays, hashes, fast polymorphic variables, good file I/O capability, advanced math modules, all the rich library modules which are just part of the regular Python distribution.\n<P>The office app would have to provide its own Tk-like functions to its modules. In any case I think some type of programmability is a good thing for the gnome applications. It is a useful addition to the software and really enhances its usability. Just look at what it did for Emacs :-)"
    author: "Dougster"
  - subject: "Boa Constructor, Visual TCL, Glade, KDevelop, ..."
    date: 2000-09-30
    body: "Why reinvent the wheel and put <b>corners</b> on it just because Microsoft does? If the idea is to attract Microsoft programmers, why not do a translator, along the lines of the <b>excellent</b> <a href=\"http://asp2php.naken.cc/\">ASP2PHP</a>, and choose a semi-decent language as the base language instead. BTW, I will personally thump anyone who takes that literally and makes PHP the default language for KDE. Stretching BASIC into object-land and RAD is a bad idea. Leave it where it belongs: on TRS-80s and Ohio SuperBoards.\n<p>Does anyone remember those huge American cars (<i>``Yank tanks,'' we call them in Oz</i>) with ginormous fins spreading out from their rear ends? I recall a MAD Magazine cartoon of one driving along a street knifing pieces out of the scenery with the fin tips. That bizarre trend started because a single <b>small</b> car sported <b>vertical</b> fins for stability at speed. The car started out popular, so other designers copied the <b>look</b> to try elbowing into that market. Naurally, the <b>horizontal</b> fins they added are <b>un</b>stable at speed!\n<p>We're doing that again here. Pull out! Pull out! We're gonna hit..."
    author: "Leon Brooks"
  - subject: "Re: Boa Constructor, Visual TCL, Glade, KDevelop,"
    date: 2000-09-30
    body: "I could not agree more.  I think the focus should be on what would be most beneficial for KDE and not necessarily dupe MS, or try to get MS developers.  There is already visual python\n<a href=\"http://www.thekompany.com/projects/vp/\"> http://www.thekompany.com/projects/vp/</a> that looks very promising.  Why not focus on that instead of fragmenting shit?"
    author: "Dimi Shahbaz"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "Well, like many others, BASIC was the first programming language I learned. It sucked 20 years ago, and VB sucks no less today.  There exists a fairly complete and mature set of Python bindings for Qt/KDE (<A HREF=\"http://www.thekompany.com/projects/pykde/\">www.thekompany.com/projects/pykde/</A>).  Perhaps the effort would be better spent developing a nice IDE for PyKDE?"
    author: "Timbo"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-10-03
    body: "If you like Python, you may want to use the\nPyKDE IDE \"Vipyl\". Look at http://vipyl.sourceforge.net !"
    author: "Chris"
  - subject: "Why not find out what people want first?"
    date: 2000-09-30
    body: "I am reading down the list of comments here and noticing that there are very few which express the view that the project as it stands now is a good idea.  Given the fact that you are making something you hope the developer community will adopt, wouldn't it make sense to ask them what they really want, then define the approach which will be taken?  I've seen suggestions here to use: Java, Delphi, Python, and a plea to coordinate with the Gnome people.  Perhaps you should consider a poll of your development community to find out if how they would like to see you approach this."
    author: "Blackjax"
  - subject: "Re: Why not find out what people want first?"
    date: 2000-10-01
    body: "Those most vocal are responding.\nI like the idea, I like BASIC, VB isn't as nice, C sucks, pascal is good.\nThe developers want this to go through because they probably want to use it.\nThat seems like a pretty good and standard reason to me.\n\nFran\n:):):)"
    author: "Fran"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "You know, I see a lot of arrogant replies to this thread as to why there shouldn't be a BASIC for KDE.   You're sounding a lot like the Microsoft guys who don't think there should be a Linux, or the Sun guys who don't think there should be a Linux.\n\nWhat you're talking about is another programming language CHOICE for Linux, that is a GOOD THING, not a bad thing.\n\nS=op what it's BASIC?  There are millions of Visual Basic developers out there in corporate jobs using it who would love to target Linux, but can't, because Linux development can only be done by the \"in\" crowd, e.g. C/C++ developers.\n\nC/C++/Java developers my laugh at Visual Basic, that's OK, but VB isn't a slouch, either.  Put me up against a C/C++/Java developer to write an OOish order entry app, and I bet I get get mine finished before yours even compiles.  Some people like to get the job done using a tool their comfortable with.\n\nIf we can create a KBASIC that is easy to use, with constructs and keywords that don't leave people out of the Linux loop.\n\nMongolo\nC++/Java/Visual Basic developer."
    author: "Mongolo"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "Have you ever seen/tried Delphi?\nIf you can't do anything in Delphi that you can in VB in the same time or less, I'd be surprised.\n\nDelphi has all the power of C/C++ and almost all the simplicity of BASIC, with more read-ability than either.\n\nYou really should try it out, and as Borland/Inprise happens to have a 60 day free trial available for download, it's fairly easy to start out with =)"
    author: "Trevor"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-10-04
    body: "<i>Have you ever seen/tried Delphi? </i><p>\nAlso Borland is getting closer to finishing Kylix, their Delphi/C++ builder on Linux project, so that anything written with (is it the CLX cross platform class set) will simply recompile in both windows AND linux with little to no modification.<p>\nSnazzy!"
    author: "Tsujigiri"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2004-07-02
    body: "Well, there are still people who prefer Visual Basic-like language to Delphi. I have done some bigger projects in Delphi and written some littler programs in Kylix and of course there numerous things that are better in Delphi and Kylix than in VB, but I do not believe that it is not possible to give to BASIC the power of Delphi and also maintain (partial?) compatibility with Visual Basic. I personally would like something Visual Basic .NET-like more than Delphi. And it seems to be simpler to learn BASIC than Pascal (I do not realize, why and maybe this is only my experience with people) and for me the modern BASIC looks better than Pascal. For GUI programming and for plugins BASIC would be my true preference (even if I like old-style text-mode applications and use to write these in C/C++)."
    author: "Still do not like Delphi as much as VB."
  - subject: "So take the bits that work!"
    date: 2000-10-01
    body: "Go for the RAD/IDE parts, burn the VB. That's what <a href=\"http://boa-constructor.sourceforge.net/\">Boa Constructor</a> is. John Sowa's post below pretty much sums VB itself up as it is, but why not translate?\n<p><a href=\"http://asp2php.naken.cc/\">ASP2PHP</a> already has a top-notch VB parser in it, and I'm sure that making it spit out a Python subset or whatever would be a heck of a lot easier than coping with VB's deficiencies for the rest of our collective lives!"
    author: "Leon Brooks"
  - subject: "M$ developers know that VB sucks"
    date: 2000-09-30
    body: "The Microsoft developers themselves admit that VB is a total disaster:  it was never designed, and it evolved with unplanned patches installed by programmers who didn't coordinate their work with COM or with other modules that use VB as a scripting language.\n\nVB and VBA run many billions of dollars worth of application code, and it is the single biggest reason for the almost universal use of M$ Office.\n\nThe only way that any office suite will be able to compete with M$ office is to run current VBA scripts without any change."
    author: "John Sowa"
  - subject: "Re: M$ developers know that VB sucks"
    date: 2000-09-30
    body: "And also be subject to the viruses that spread via this scripting language! That is my ONLY real concern for things. Whatever anyone does - don't make things like mail clients that use VBS...PLEASE!!!"
    author: "Steve Wilson"
  - subject: "Re: M$ developers know that VB sucks"
    date: 2000-09-30
    body: "It's not having VB scripting that allows end uses to easily run malicous code -- it's because the file extensions are associated to the interpreter engine by default.  Users could just as easily click on an .exe file and run it.\n\nVBS viruses are not spread due to any fault in software (well, that's arguable)... it's sheer human stupidity that causes it."
    author: "Justin Buist"
  - subject: "VBS viruses really *are* spread by flawed software"
    date: 2000-10-01
    body: "<i>VBS viruses are not spread due to any fault in software (well, that's arguable)...</i>\n<p>It certainly is! VB is conceptually descended from a whole line of BASICs without terms like ``multi-user'' or ``network'' or even ``resources'' in their concept spaces. The fact that some of these have been (kind of) crudely bolted on afterwards is no help to the gestalt of the language. When you program in VBS land, you are programming with a hyper-extended version of dear old single-user, network-free MBASIC. Dear as in expensive, which is how it often ends up.\n<p>As networking and timesharing have been strapped on to Windows 9[58] (and age has seriously compromised the plastic cable-ties they used), so it can be strapped on to VB - but unlike (respectively) Unix or Python, it can never approach the clean, strong elegance of being born networking and timesharing. It will always be ``nuevo riche'' and illegitimate.\n<p>Trust me, we <b>do not</b> want to adopt this!"
    author: "Leon Brooks"
  - subject: "Re: VBS viruses really *are* spread by flawed soft"
    date: 2005-12-01
    body: "perl too"
    author: "george rogers"
  - subject: "Shades of CoBOL! Why not translate?"
    date: 2000-10-01
    body: "<i>VB and VBA run many billions of dollars worth of application code, and it is the single biggest reason for the almost universal use of M$ Office. The only way that any office suite will be able to compete with M$ office is to run current VBA scripts without any change.</i>\n<p>How gloomy. And doesn't that sound like CoBOL used to, once upon a time? I reckon a son of <a href=\"http://asp2php.naken.cc/\">ASP2PHP</a> would be a much better idea. It would cope with a great many existing projects (it does very well with VBS-based .ASP pages) and also break us loose from the millstone which is Visual BASIC."
    author: "Leon Brooks"
  - subject: "Re: M$ developers know that VB sucks"
    date: 2003-12-12
    body: "It comes down to this: dumb people.  They are everywhere.  But the last place you want them is in an IS/IT department.  Yet, languages like COBOL and VB allow dumb people to get jobs in this very department.  I guess the only thing sadder is a company of dumb people making a crappy product in COBOL/VB that a dumber group of dumb people needs to support (and obviously can't because the code is dumb).\n\nIf you want to stop wasting money (on smart people like me that charge outrageous rates for fixing dumb code), please keep dumb people and dumb technologies out of your department."
    author: "no"
  - subject: "Re: M$ developers know that VB sucks"
    date: 2005-12-01
    body: "1! it teaches you bad habits;\n2! its a pached up basic witch is a dumb languige;\n3! no lexicle closhures, no regexs, and worst of all propriatary;\n4! perl has all theas fetures free and the languige is not for retards"
    author: "george rogers"
  - subject: "Re: M$ developers know that VB sucks"
    date: 2006-10-29
    body: "Ppls ppls just because VB can't do powerful stuffs does not mean it sucks... I love VB and it makes me so sad when ppls curse at it... i can make pretty decent programs out of VB... i made a lot of programs for my school and i make happy birth day cards to my good friends with VB... As programers we should know how to live with other programs around us ^^ so yea my point is lets not curse at VB... thank you so much wish you all luck on your programings! \n"
    author: "unknown"
  - subject: "Re: M$ developers know that VB sucks"
    date: 2006-11-02
    body: "We all know that this programming language sucks but when it's what the crappy schools and teachers hand to you you can't do much but cower and learn how to use it and then make sure you don't do all the stupid crap that the obvious idiots are doing.\n\nAlso this language can be as fun or as strong as any other and it does a lot more than what those earlier naysayers were saying in the beginning. This laguage has become very  powerful and it does way more than you naysayers give it credit for...\n\nAlso if your stoopid enough to deal with a Microsoft product then you have to put up with it and take your lumps or better yet go learn another 'BETTER' language if you can even find one...\n\nI've been using it for longer than this particular thread has been around and what they say at the beginning of the thread is/was true at the time but we have adapted so things are much better now. So, if your gonna say no to learning VB just because of the first few naysayers well that is your loss and that just too bad for YOU....\n\nnuff said..."
    author: "Mr. Underhill"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "Personally, I'd rather not see a VB implementation for Linux. I love Delphi, C++ and PHP a lot, and although I do not know much about Java and Python, I think these two are great languages as well. VB (or Basic in general) is not a good language in my opinion.\n\nHowever, that doesn't mean there shouldn't be a VB version for Linux. It's said before: CHOICE is the keyword here and for the majority of Windows people VB is the only language they know about. Just offer them a language they know, as it DOES do the job for many simple apps.\n\nIt's just Basic's tendency to promote 'quick and dirty' work that makes many experienced programmers hate the Basic language AND that makes inexperienced programmers LOVE the language.\n\nThere are just a few notes I want to add here:\n\n1) NEVER use a single language as the only scripting language in other apps. There should be a KPart like interface to plugin litterally any programming language into e.g. KOffice. That would be a feature that would bring KOffice way beyong MS best selling product\n\n2) PLEASE share as much code as possible with the Gnome Basic and the KDevelop guys. Reinventing code is nice if the old code is fundamentally wrong, but it also uses lots of resources that could otherwise be used much more efficiently.\n\n3) Continue the work for all Basic people out there (but do not expect ME to use it on a regular basis)"
    author: "Martijn Klingens"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "Get off it you sanctimonious naysayers. Believe it or not, there are some of us out here who have the capacity to create useful (even inspired) code who don't have the time to tangle with the malodorous intricacies of C++ and other gnome-inspired languages. If you don't like Basic (for theological or practical reasons), stay away. Those of us who value our time ... and who aren't bound by the peer group politics of turf-guarding (viz the IBM era)... often prefer it."
    author: "Concatenatus"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-10-01
    body: "40% of the cost of software is maintanance. THose of us who value that time DON'T use Basic."
    author: "cbcbcb"
  - subject: "A source code converter would be better."
    date: 2000-09-30
    body: "<BR>\nIt seems to me that Microsoft put efficient tools into Visual Basic because the company wants to make sure it doesn't deliver a good programming tool. It isn't Basic that people like, it is the tools available in VB. \n<BR><BR>\nVisual Basic is written in C and C++, I understand. Microsoft wants to make sure that its customers are not able to become good enough programmers to compete with its own products. That's why it delivers efficient tools for a ridiculously bad language.\n<BR><BR>\nMost people don't seem to realize that the limitations of Microsoft products are deliberate. Windows 98 crashes because there is a deliberate limitation (of 128 K memory, I understand) in GDI and User resources. Exceed those limitations, and the operating system crashes without warning. Deliberate limitations like this are the action of a monopoly wanting to insure that the customers always have a reason to upgrade.\n<BR><BR>\n(Run Microsoft's own Resource Meter software in Windows 98 to see the limitations in action.)\n<BR><BR>\nIt is a challenge for open source software creators not to get involved with Microsoft's destructiveness. Possibly what is really needed here is a converter to take source code from a VB program and turn it into source code in a sensible language. \n<BR><BR>\nIf the conversion could go in both directions, the need to write in VB would completely disappear. This is the desired effect. The present plan, of providing a VB-like language in open source software has the negative effect of encouraging VB programming.\n<BR>"
    author: "Michael Jennings"
  - subject: "Re: A source code converter would be better."
    date: 2001-08-07
    body: "I want to know is there any possibility of convertion of vb source to say c/c++ ?"
    author: "Rishi"
  - subject: "Re: A source code converter would be better."
    date: 2002-04-04
    body: "there is a C++ to VB code converter and visa versa. the only down side is that its quite pricy for me. here is the link http://www.metamorphic.com/html/vb_to_c___0.html"
    author: "Quonic"
  - subject: "Re: A source code converter would be better."
    date: 2002-06-04
    body: "Is there a c++ to .txt code converter, or does anyone have one? if so id like to know."
    author: "Mr. Anxious"
  - subject: "Re: A source code converter would be better."
    date: 2002-06-15
    body: "lol u dumbass thats vb to c++ not c++ to vb"
    author: "satan"
  - subject: "Re: A source code converter would be better."
    date: 2004-07-19
    body: "huh im a master at c++ and i suck at vb and i dont wanna take the time to convert my vb code into c++ so is there a vb to c++ code converter?"
    author: "Dave"
  - subject: "Re: A source code converter would be better."
    date: 2007-03-05
    body: "C++ to .Txt? Just rename the files."
    author: "Cyprus"
  - subject: "Re: A source code converter would be better."
    date: 2004-02-13
    body: "hi\ni have a program in c++ and want to convert it to vb\ncan u help me in this matter?\nthanks alot\nbye\n"
    author: "akbar samadi"
  - subject: "Re: A source code converter would be better."
    date: 2004-04-13
    body: "Are there any free VB to C++ source code converters?"
    author: "Ben"
  - subject: "hay"
    date: 2005-01-29
    body: "well there a lot Questions on this forum and no a lot of answers.............http://www.kdiggins.cjb.net/ this site has a vb to C converter but i realy dont see y one would want to convert c++/c to VB anyways?\u00bf?\u00bf?\u00bf?\u00bf?\u00bf"
    author: "MEE"
  - subject: "Re: A source code converter would be better."
    date: 2002-08-13
    body: "Anybody can help me to develop the coding using Visual BAsic as listed below:\n\n1. How to do the code \" REply email\" in Visual Basic \n\nRegards\nUngku"
    author: "ungku abu bakar ungku othman"
  - subject: "Re: A source code converter would be better."
    date: 2002-10-01
    body: "How can you say that Visual Basic is limited? It does what it was made for...\n\nIt was created to make applications for use within the windows environment and give people access to features of windows that are otherwise inaccessible...\n\nNow you're going to argue the toss and say \"C++ does this too, so whats the point in Visual Basic\"\n\nWell my friend, Visual Basic is a lot faster than C++ when it comes to development time...meaning you can whip up a windows application within minutes that delivers the functionality that is needed...\n\nC++ takes far too long to create applications that you may only need for a limited amount of time...\n\nDon't get me wrong, I use a wide range of languages to create programs for various functions...but I do not judge the languages on what they cant do...I judge them for what they can do...\n\nAnd to finish off. You are so wrong about Microsoft products...if you do a clean install of Windows and install nothing into the OS...its perfectly stable, no errors, nothing...\n\nYou have to remember Windows has to be compatible with a stupidly massive amount of hardware, its virtually impossible to create a perfect OS that has no issues with anything...\n\nNo mentioning Linux here either...the reason that Linux is rock solid, is because theres pretty much no software that has been written specifically for Linux...only ported software...\n\n-End of Rant-"
    author: "Peter G"
  - subject: "Re: A source code converter would be better."
    date: 2002-10-02
    body: "I agree. Visual Basic has improved significantly in the past few years. It's a great tool for getting things done. However, I think that Visual C#.NET will probably replace much of VisualBasic usage (except for established VB apps), as it's a nicer language and as easy as VB.NET."
    author: "fault"
  - subject: "Re: A source code converter would be better."
    date: 2004-03-10
    body: "Last time i checked, Linux software has been ported to windows... not the other way around. some progs are ofcourse ported, but 90% of that again has been totally rewritten. Try researching some before you make a statement...."
    author: "Zaph"
  - subject: "Re: A source code converter would be better."
    date: 2003-12-17
    body: "comparing vb to basic shows your knowlege of either is pretty much zero.\n\nwhat a twat"
    author: "Duncan"
  - subject: "Re: i want to have vb project to a jpg file"
    date: 2004-11-18
    body: "can u help me out. i have to make a ID card S/w where i will have data entry and DB connectivity. backen is access. front end can be anything...ASP or anything. but the rule is i want the output into a jpg file from which i will have colored printout of the page from a shop. most probably, i want to have the IDs of different students will be displayed on A3 size page (18x12 in) then that page will be converted to jpg file.... can u pleeeeeeeeeeeaaaaaaaaaseeeee help me im in an urgent need. thank u \n\nGod bless You,"
    author: "paul"
  - subject: "Re: A source code converter would be better."
    date: 2005-08-12
    body: "Some ppl are really stupid. Michael here for one."
    author: "Andrew"
  - subject: "Re: A source code converter would be better."
    date: 2005-12-28
    body: "wow 5 years later and you people still pi#s me off. fact of the matter is vb is 80% faster then c++ in development time. \"development time\" thats were the money is saving \"development time\", and any real windows functioning is done thru the \"WINDOWS API\" might be a big word, sorry. any \"REAL\" coder can pseudo code so language is irelevent fast UI in VB with a solid backend in C++.\n\nlol, linux is a great operating system, but dont make it look bad by being a ja#ka## about not liking windows. alot of people want a nice install, and others want a stable os. in closing its my feelings that a windows guru has just as much os control as a linux guru does, and they both have to much spite about it."
    author: "blabla"
  - subject: "Re: A source code converter would be better."
    date: 2006-01-17
    body: "Michael Jennings is a, know-nothing, talking head (as is evident from the latest .NET developments) \"AS I UNDERSTAND\", LOL you don't understand crap buddy."
    author: "Richard Saunders"
  - subject: "Re: A source code converter would be better."
    date: 2006-10-10
    body: "Strange, I could have sworn I clicked on the KDE link instead of the 4chan link..."
    author: "Anonymous "
  - subject: "VB is useless as a public program compiler."
    date: 2006-12-24
    body: "One would want to convert VB into C++ because the .net framework is needed to run most user made VB programs. Most people would lose interest in a 400kb program that has a 23mb framework requirment.\n\nI need a way to make my programs publicly useable, which means I'll probably have to learn some complicated and time consuming language like C++.\n\nThe price of that convertor is over $100, which compells me all the more to look for a good crack. e.e\n\nMicrosoft needs to GTFO their high horse and create a Visual Basic-eske program which makes program creation simple while allowing the creator to publish the program and have it run on PC's like a regular application.\n\n\n\n"
    author: "Fullmetalfox"
  - subject: "Re: A source code converter would be better."
    date: 2007-03-07
    body: "Downloaded software code for converting j2me code to pseudo code."
    author: "apsar"
  - subject: "Re: A source code converter would be better."
    date: 2007-08-08
    body: "I did try out a limited VB to C++ converter some time back to try to convert one of my programs to C++ but that converter was limited to converting just the form as I recall but last night I did try out Instant C++ (VB Edition): VB to C++ Converter Demo & this is for VB .NET so it did nothing for me since I run Win98 so I can not run Visual Studio .NET unless I change over to another operating system such as Win XP, 2000, NT, Vista..., if I installed Linux I bet I could run it or if I installed the operating system ReactOS www.reactos.org which is a open source a ground-up implementation of a Microsoft Windows operating system, I am no C++ programmer & even run into problems compiling programs others wrote, my Hello World compiled in all the C++ compiler I tried out so far, about all it is good for is for beginners to try out compilers...\n\n#include <stdio.h>\nint main()\n{\n        printf(\"Hello, World\\n\");\n        return 0;\n}\n\nBTW, Hello World =8^)"
    author: "FileGod"
  - subject: "egregious spelling/translation error in story  :-/"
    date: 2000-09-30
    body: ">>The KBasic language is a pretentious project....\nVery cool, but you should change \"pretentious\" to something else... it sounds like you're saying \"we are working on a project which is arrogant\" which sounds odd, and slightly unflatterring. :)\nWell congrats and good luck on your effort folks"
    author: "7thGradeEnglishTeacher"
  - subject: "Re: egregious spelling/translation error in story  :-/"
    date: 2000-09-30
    body: "I disagree. Perhaps the author chose a word that was contrary to their opinion but i think that a negative word was specifically chosen to indicate the problem aspect of it taking a while to complete the project and to be humble about that problem aspect (especially with the popular dislike of Mozilla style bloat). By using a negative word like \"pretentious\", that makes it more difficult for other people to criticize them later for their ambitiousness."
    author: "Jay S. Lazlo"
  - subject: "Any KBasic people here?"
    date: 2000-09-30
    body: "Hi,<p>\n\n  there have been a lot of interesting questions e.g. regarding code sharing with GNOME. Is somebody of the KBasic Team here to comment? Speak with us.<p>\n\ncheers,<br>\n-A"
    author: "Atanasius"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "I think KBasic is a good idea, allthough I see\nthat many people don't think so. I hope they reuse\nthe KDevelop code allthough I know that reuse it \nnot allways as easy as it seems.\n\nPersonally I think Basic as a language stinks, hell I'm not really sure you could call it a language. I will probably never use KBasic. But hey that's not the point. The point is that Linux\nis all about choice (at least thats what everybody is talking about) and if somebody wants\nto program basic in Linux, let them.\n\nPascal is a much better language than Basic, but still nothing beats smalltalk. Maby we should make\na KSmalltalk."
    author: "Erik"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-09-30
    body: "Looks like a translation problem with the word \"pretentious\" there.  The word has a negative connotation, implying not only arrogance, but that it couldn't live up to it (similar to \"vaunted\").\n\nI think the word you're looking for is \"ambitious\""
    author: "Chuck"
  - subject: "Phoenix Object Basic"
    date: 2000-09-30
    body: "<a href=\"http://www.janus-software.com/phoenix_body.html\">Phoenix Object Basic</a> is a Visual Basic-like RAD for Linux. It's shareware so far, but they talk of GPLing it eventually."
    author: "Haakon Nilsen"
  - subject: "Re: Phoenix Object Basic"
    date: 2001-09-04
    body: "A true Object oriented basic that preserves the easy of use of VB4.0-6.0, and is far more speedy from other bytecode compiled langs, such java. I'm wondering the impact that it will have on windows systems, given the mess that MS will create with VB.NET."
    author: "K C"
  - subject: "Re: Phoenix Object Basic"
    date: 2004-10-21
    body: "ok fine\nhow to create reports in it.\ni am using phoenix object basic on linux platform\ni designed the form and saved. Then i try to open the same\nbut the form does not display the controls in the same order.\nIt was displayed like one control is place over another.\nI dont know how to create data source name in linux.\nMy Control pallet is having only few controls.\nhow to add more controls to control pallet.\nplz reply soon\nregards & thanks\n"
    author: "Amarnath varma"
  - subject: "The problem with VB"
    date: 2000-10-01
    body: "The problem with VB until version 6.0 is that it doesn't go the full distance with Object Oriented Programming. Unlike Delphi or Power Builder. M$ has released a spec for version 7 which looks like it goes the distance. Because of VB not going the distance in the past it has turned out a generation of VB programmers who don't understand how OOP realy works.\nI don't mind a KBasic as long as it properly does OOP and doesn't just copy VB 6 or below.\nBut how about a \"visual\"-Python?"
    author: "James Clarke (Remove NO SP AM from Email)"
  - subject: "Re: KBasic: A VB-Like Rad/IDE For KDE"
    date: 2000-10-01
    body: "In my opinion, such a IDE sould look like this:\na) an big API (including an GUI API, very alike the\none from Delphi), coded in C or C++ for performance\nb) an IDE that can be modular enough to support different languages: Basic, pascal, C, C++, phyton, perl (why not?)"
    author: "vitor"
  - subject: "I totally agree!"
    date: 2000-10-01
    body: "It is a complete mystery to me why such a system doesn't already exist. Both the Borland range of tools and the MS development tools share a lot of code base, so I guess it should be possible to have them integrate into one IDE.\n\nI can't wait for such an IDE, so if someone has created one for KDE, please let me know!"
    author: "Martijn Klingens"
  - subject: "Get Real: VB isn't that bad"
    date: 2000-10-09
    body: "Hi guys,\n\nLet's all get realistic and stop bashing VB.. it *is* good in doing the limited things it's supposed to do, and does it rather well. \nLanguage purists may insist that inheritance. etc are missing.. but as a VB programmer, i've mostly never felt the need for it, and there's always a workaround.\n\nSince then I've switched completely to java, and I feel that *both* are good, for the correct projects."
    author: "Balaji Srinivasan"
  - subject: "Linux can help VB Grow"
    date: 2003-02-20
    body: "VB6 is not just a RAD... its more than that... it can become MORE than MS actually intented it to be if all of us stop critisizing and start re-inventing the wheel where it doesnt really work our way...\n\nI mean why not keep the good parts of VB and throw away the bad ones. For example. Let KBasic or GnomeBasic or the next VB-clone for Linux use inline assembly... the ability to create standalone binaries... work out inefficiencies and/or intentional bugs in the language as it exists now, make it parse strings and stream files faster than it already can :)\nand I am sure that VB can evolve into something that lots of people can use regardless of what they think of it.\n\nProgramming Languages are tools we use to make programs... they do not define us as programmers. They are meerely tools for the job we so much love. So ... lets start brainstorming on how to make one of our tools ... BETTER  :)"
    author: "Dimitris Anoyatis"
---
<a href="http://www.kbasic.de/">KBasic</a> is a programming language which brings a complete BASIC to KDE. It will include a great IDE with a form designer and a complete binding to KDE like controls (CommandButton, TextBox, ComboBox, Image, Html) and the other KDE features.  Read on for more.




<!--break-->
<p>
Now the first steps are the development of the form designer and the designing of the KBasic language (EBNF-Grammar, Lexical Scanner, Grammar Parser, BASIC-Functions and Interpreter). In later future it will also support SQL database access via a KSql-Plugin. The big aim is to have a free and good programming tool. 
<p>
The KBasic language is a pretentious project, so we will need a long time to make it complete. We think in the summer of 2001 the first end user version will be released. 
<p>
The KBasic website is at <a href="http://www.kbasic.de/">http://kbasic.de/.</a> Any help is very welcome. 
<p>
<i>The KBasic Team</i>



