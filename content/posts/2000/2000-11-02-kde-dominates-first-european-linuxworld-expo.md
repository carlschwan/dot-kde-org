---
title: "KDE dominates first European LinuxWorld Expo!"
date:    2000-11-02
authors:
  - "numanee"
slug:    kde-dominates-first-european-linuxworld-expo
comments:
  - subject: "Re: KDE dominates first European LinuxWorld Expo!"
    date: 2000-11-02
    body: "Linus gave Konqi a great big kup!\nAll well deserved, kongratulations from me too!"
    author: "reihal"
---
Read all about it in this <a href="http://www.linuxworld.com/linuxworld/lw-2000-10/lw-10-europroducts.html">feature</a> currently running on LinuxWorld.  As we had previously <a href="http://dot.kde.org/970766758/">reported</a>, KDE also took the Linux Community Award 2000 at that event -- LinuxWorld have now made available a RealPlayer <a href="http://193.159.249.12/ramgen/feedback/messen/linuxworld2000/05/04.smi">streaming video</a> of the ceremony.  And last, but not least, LinuxWorld talks to our own Matthias Elter in this nice little <a href="http://www.linuxworld.com/linuxworld/lw-2000-10/lw-10-kde.html">interview</a>.  Very cool!


<!--break-->
