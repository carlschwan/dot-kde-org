---
title: "Tool support for porting KDE 1.1.2 apps to KDE 2.0"
date:    2000-10-25
authors:
  - "amichail"
slug:    tool-support-porting-kde-112-apps-kde-20
comments:
  - subject: "Re: Tool support for porting KDE 1.1.2 apps to KDE"
    date: 2000-10-25
    body: "Could you give an explanation for dummies as to how exactly this helps porting an app from KDE 1.1.2 to KDE2?  It seems rather involved and technical.  :)\n\nWouldn't it be easier to read the porting guide?  \n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Tool support for porting KDE 1.1.2 apps to KDE"
    date: 2000-10-25
    body: "You can view this as a complement to the porting\nguide.  It is comprehensive and actually provides\nreal-life code directly from the standard KDE\napplications to demonstrate the porting process.\n<P>\nThe table shown initially shows the % of applications that use a particular library class in V1 and V2.  The usage deltas in this table show you classes that tend to no longer be used (in which case you have a large negative delta) or are now used more often than before (in which case you have a large positive delta). You can click on the column headings to sort the column in the table.\n<P>\nYou can also click on a library class to see its \"reuse patterns\". That is, other functions/classes that tend to be used when this library class is used. Again, there are usage deltas for these rules also. For example, if you click on the patterns link for KApplication, you will notice from the usage deltas that you are now expected to use  KCmdLineArgs and KAboutData whenever you use\nKApplication and you should no longer use getKApplication() and getConfig().\n<P>\nEqually important as these patterns is the access\nto the application source code which demonstrates\nthem.  For example, if you click\non KCmdLineArgs in the patterns page for\nKApplication, you will see all application classes\nthat contain KApplication. If they also contain\nKCmdLineArgs, they \"support\" the pattern. If \nthey do not contain KCmdLineArgs, they \"detract\"\nfrom the pattern. Clicking on an application class\nlet's you see the source code in both versions\nso that you can see by example how people have\nupgraded their applications from v1 to v2.\n<P>\nYou can also access the code directly from the main table through the code link.\n<P>\nSee the documentation for more details.  BTW,\nthe two usage deltas should give you roughly\nsimilar information. I just use different techniques for each one.  In a future version,\nthey may be combined into one delta.\n<P>\nAmir"
    author: "Amir Michail"
  - subject: "Re: Tool support for porting KDE 1.1.2 apps to KDE"
    date: 2000-10-25
    body: "<i>For example, if you click on the patterns link for\n                 KApplication, you will notice from the usage deltas that you are now expected to use\n                 KCmdLineArgs and KAboutData whenever you use KApplication and you should no\n                 longer use getKApplication() and getConfig().</i>\n<p>\nThanks for the explanation!\n<p>\nCheers,<br>\nNavin."
    author: "Navindra Umanee"
---
The CodeWeb tool, now GPL'ed, provides guidance in porting applications from KDE 1.1.2
to KDE 2.  It works by data mining the
differences in core library usage among 
applications that come in the main distribution.
In addition, CodeWeb continues to demonstrate
typical library usage much like
KDE tutorials using "reuse patterns" and real-life code as examples of those patterns.  See the <A HREF="http://codeweb.sourceforge.net">CodeWeb</A> page at SourceForge for a demo and documentation.