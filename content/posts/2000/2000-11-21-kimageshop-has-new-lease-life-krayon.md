---
title: "Kimageshop Has New Lease on Life as Krayon"
date:    2000-11-21
authors:
  - "Dre"
slug:    kimageshop-has-new-lease-life-krayon
comments:
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "Krayon looks fantastic... well done!  Just downloading the CVS now to take a better look at it.\n<br>\n<br>It's about time the GIMP got a serious competitor - it's the only non-KDE app I use regularly, and for all the hype about it's power (and it is powerful), its interface sucks badly (common problem with them GTK+ apps ;)\n<br><br>\nWhat the GIMP does have though is an immense number of quality plugins already, many of which could be a PITA to port to Krayon due to their UI relying on GTK+.  So, my number one killer feature request for Krayon (aside from all the regular stuff like kick-ass rendering in both RGB and CMYK modes :) is foreign plugin support, which realistically probably means GIMP plugins.  This has some problems, the fact it means linking against GTK+ as well, and having to keep up with a plugin API that's not under your control... but think of all the juicy plugins that suddenly become available.  I think it's worth a shot.  Remember that the nspluginviewer that Konqueror uses for netscape plugins links against Lesstif, so there is a precedent in KDE for linking against other widget sets to gain new features.\n<br><br>Also, if you're stuck for ideas for your high-quality rendering widget, as I think somebody pointed out on the mailing lists, Raph Levien's <a href=\"http://www.levien.com/libart/\">libart</a> is worth checking out as it seems to be the single most advanced 2d rendering code I've seen anywhere, not just in the free software world.  Right now it's mostly powering GNOME apps but I don't see any reason why it couldn't be used... AFAIK it's entirely stand-alone, and given that KDE's licensing problems seem to be settled I don't see why there could be any objections...  I know KDE has tried to avoid using non-KDE (L)GPL'd code in the past due to these licensing difficulties, but now that's settled it'd be nice to see some used - there's an awful lot of very good and very useful free code out there that would add major value to KDE.\n<br>\nRasterman's <a href=\"http://www.us.rasterman.com/imlib.html\">imlib2</a> also might be worth a look for ideas, although it's not nearly as ambitious as libart.\n<br><br>\nAnyway, that's my tuppence... keep up the good work! :)"
    author: "marm"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "Nothing to add to this great comment. Thanks marm!"
    author: "Fritz Wepper"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "I'll take a look at the links you provided especially for libart.  A lot of Krayon's rendering code seems to be similar to Gimp already - use of tiles, etc.  There's a way to access the data for use with other libs, though it took me a while to find it in what was there when I started.\n\nIt's possible to have foreign plugins as well as native plugins which use Qt/Kde and which operate directly on the image in native format.  For these foreign plugins shared memory or pipes would need to be used - this would be less of a problem for those things that just do things to the image without the user having to interact than for those that require a visible user interface which uses Gtk.\n\nMore promising and much easier to use are the image magick plugins already used by Pixie. Maybe there are not as many of these, but they are very compatible and already have been proven to work with Kde in Pixie, with examples of how to do this.  These libs (minimagick) really should be moved to kdesupport or kdelibs, though, so a dependency on kdegraphics isn't created for a koffice app.  Overall, image magick is probabally just as good as any other rendering code and is well supported and widely used.   There's a lot more image magic stuff that is not currently in kde's version that can  be added.\n\nI have no idea how krayon compares in rendering speed with similar paint programs.  Briefly, I tried using QPainter and QImage/QPixmap code in place of Krayon's native image code which stores the actual data by layers and channels, usually 32 bit.  The Qt stuff was a little faster, but it won't do as it only was 16 bit (whatever your display supports) and lacked channels and layers, and translating back involves even more overhead. You can access the scanline data fairly easily, though, in terms of what QImage uses with Krayon's native image format - for pasting in images and so forth.  It's possible for krayon to use threads for updating large images quickly.  That's not hooked in but it's doable - later.\n\nThis app was started while there were still possible licensing issues with using Gimp code and formats.  However, Krayon uses xml file format like all other koffice apps and appends the binary image data in separate files and then it's all compacted into one file.  Compatibility with other koffice apps and ways of doing things has to come first. While gimp's format can be imported, and is similar, I don't see how both Krayon and Gimp can use the same format natively.\n\nthanks - John"
    author: "John Califf"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "FYI\n1- Gimp interface does not sucks. You just don't like it. I do like it, as it's oriented towards speed and efficiency, which lacks to many KDE apps, because of the menus that are so useless and complicated to use for a single action.\n2- Imlib2 may introduce licensing problems, as Raster's apps are licenced under BSD or X style licences"
    author: "a"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "It seems Imlib2 is under X license which is fully GPL compatible, so no problems there."
    author: "b"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "<i>as it's oriented towards speed and efficiency</i>\n<p>\nFor me it's vice versa, GIMP slows me down. But isn't it cool ? Everyone can use what he prefers."
    author: "Lenny"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "Gimp's interface is flawed.  Granted if you use it enough you will learn how to use it, but for anyone else there are many steep learning curves.  Many things in Gimp are implimented from a programmers problem and not a user end problem.  This is a major major GUI design error.  Countless times when I don't know what to do in gimp I just try to think of how I would impliment it and then I can get it to work.  So Gimp's interface may not suck, but it is wrong.  If you don't believe me pick up any gui design book."
    author: "Benjamin Meyer"
  - subject: "Krayon User Interface"
    date: 2000-11-21
    body: "Most of the comments here on Krayon/GIMP interfaces totally miss the point. It's not a question of whether GTK or KDE are prettier!  What matters how easy it is to access the functionality of the program itself.  Writing specific interface suggestions would be far more productive than repeating bland generalizations.\n\nGIMP has a very functional and efficient scripting interface, but the GUI interface sucks for once-off quick and dirty operations that need to be performed frequently.  \n\nSpecifically: \n1) there aren't keyboard shortcuts for many common operations \n2) the keyboard shortcuts aren't universally accesible because of the bizarre document interface model.  Try accesing the File menu when an image has focus!\n3) the menu system is only accesible via mouse clicks.  There is no way to navigate through it using only the keyboard, unless you 'tear' the menu off and alt-tab between it and various images.  Even with this arcane little trick, the user is dramatically slowed down.\n4) if you have more than a few images open the screen becomes incredibly cluttered and it is difficult to navigate between them and the toolbar.  Compare this to Adobe Photoshop's persistent toolbars and window menu.\n5) most of the toolbars and dialogues waste an inordinate ammount of screen space.  They're simply far larger than they need to be.  Compare them to their equivalents in photoshop (e.g. The interface for controlling layers) \n...\n\nIn short, please don't use GIMP as a model for Krayon's GUI!  Photoshop is the winner in this department, hands down.  It would make a lot of sense to simply copy the tried-and-loved GUI from photoshop and borrow ideas from GIMP's excellent script-fu programmer's interface and plugin design.  \n\nIn my ideal world I would be using an open-soure image tool that pairs a clone of photoshop's GUI (with exactly the same keyboard shortcuts) with a coherent scripting interface that exposes ALL the functionality to both the command line and a macro interface inside the GUI.\n\nGood luck.  I wish I had more time and experience to help with the coding itself."
    author: "Tavis Rudd"
  - subject: "Gimp keyboard shortcuts"
    date: 2000-11-22
    body: "I agree with most of your point, except what you say about the keyboard shorcuts. Gimp's keyboard shortcut system is the best I have seen in any application. You can change the shortcuts dynamically - just right-click once to bring up the menu, then as you hover your mouse over the options just press the key combination that you want, and it will change right then and there.\n\nRgading your gripe about Photoshop shortcuts, I also remember in earlier versions of The Gimp (I assume it's still there now, but I just go through and manually change it myself when I install, these days) there was a config file called psusers or something which contained the same keyboard shortcuts as in Photoshop, which you could then copy over your normal configs.\n\nHowever, it would be nice in Krayon if it could use the global menu shortcuts configs as found in KDE2. I love that feature!"
    author: "Matt"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-23
    body: "From what I know, Gimp's interface is going to be redesigned eventually so even its developers think it sucks. Personally, I found the Krayon screenshot rather obscure. There seems to be no clarity with its interface. Does anyone have a screenshot where it looks more normal?"
    author: "Bart Szyszka"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "Gimp's GUI is not ugly. You just don't prefer that look.\nTo me, it's exactly the opposite.\nI find QT apps more ugly than GTK+ apps."
    author: "Anonymous"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "I second this."
    author: "oliv"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-23
    body: "You're weird then. GTK+ has to be the most ripped off hacked gui I've seen in my life. *Anything* looks better than GTK+. Look at the arrows at the bottom of scrollbars, they're friggin pixellated gray arrows. It's ugly! Give me a KDE 2.0 app anyday!\n\n--\nkup"
    author: "kupolu"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-23
    body: "You're weird then. QT has to be the most ripped off hacked gui I've seen in my life. *Anything* looks better than QT. Look at the arrows at the bottom of scrollbars, they're friggin pixellated black arrows. It's ugly! Give me a Gnome app anyday!"
    author: "Anonymous"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-23
    body: "Can't you at least respect other peoples' choises and preferences?"
    author: "anti-kupolu"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-25
    body: "Did I saw we should eliminate gtk+? I was just voicing my opinion on the gtk+ look."
    author: "kupolu"
  - subject: "BeOSs Gobe Productive used GIMP filters/plugins"
    date: 2000-11-21
    body: "Hey there,\n\nI seem to remember Gobe Productive (BeOS's Office Suite) using Gimp plugins.  This would seem to suggest that you can get Gimp plugins without using GTK.\n\nI think Easel, a free (beer) drawing app for Be also used Gimp plugins.  While neither of these apps were Open Source, there may be some helpful source around from these programs that could point people in the right direction.  Or, I could be way off and may mislead people...\n\n(oh yeah, for my take on things, Gimp is simply awesome.  While this interface is a little confusing at first, once you get used to it it is quite straigtforward.  Kinda like Photoshop.)\n\nCheers,\n\nBen"
    author: "Ben Hall"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "unfortunately \"compile kimageshop\" is OFF by default in the current CVS\ncu\nferdinand"
    author: "ferdinand gassauer"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "Looks great! Cluttered windows and the missing menu bar are the disturbing points of the gimp.<br>\n<i>Whats exactly the cause not to use the gimp's file format?</i><br> It would be great if GPL apps could share their document formats. Opensource programmers do not need to force the users of their software to stick with it, because they don't need to sell the next version.\n<br>\n--<br>\nMfG,<br>\nChris"
    author: "Christian Mayrhuber"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "Whatever happened to Kimp? There was one, wasn't it?\nOf course, in those days there were some \"diskussion\" about licenses but that doesn't apply now."
    author: "reihal"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "AFAIK Kimp was merely a patch to Gimp that Matthias Ettrich sent to Gimp maintainers."
    author: "ac"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "<p>&gt;<i>Whats exactly the cause not to use the gimp's file format?</i>\n<br>The cause is explained in John's post. Basically, KOffice files are tarballs with an XML file and some binary data files inside. All KOffice apps must conform.</p>"
    author: "GeZ"
  - subject: "Re: Kimageshop Has New Lease on Life as Krayon"
    date: 2000-11-21
    body: "I see...<br>\nkoffice policy :-)<br>\nIt has at least one advantage: There are a lot of free parsers for xml."
    author: "Christian Mayrhuber"
  - subject: "KIS file format"
    date: 2000-11-21
    body: "Has anyone addressed that there is a graphic file already in existance (and *very* active use) that uses the .KIS extension?  It would seem that now would be a good time to change the file extension to something that would not collide with another set of programs.<P>\nFor those who don't know, .kis files are multilayer graphic files with embedded scripting.  Although they can be used for things like simple games and simple presentations, the vast majority of them are KIS Paper Dolls, generally Anime or Manga characters.  They are very, very popular, and can be found all over the net, and people trade them at Anime conventions here in the US.  They range from Hello Kitty to X-Rated characters.<p>\nApplications that view KIS files are available for almost every platform, including Win, Lin, Mac, Playstation (yes), Amiga, etc.  I believe the format is French, although the name is based on the japanese work kisekae, meaning \"clothes changing\".<p>\nI wrote some of the core people about this many months ago... I figured now would be a good time to bring it up again."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KIS file format"
    date: 2000-11-21
    body: "what about kyn instead ?"
    author: "mm"
  - subject: "I thought that Krayon was alive and well..."
    date: 2000-11-21
    body: "Didn't theKompany plan on adopting Krayon officially as part of their desktop application suite? It's listed on their Projects page. With the quality that their current apps are producing (Kivio, PyKDE/PyQt, etc.), I bet they have big plans for this as well. I hope they decide to use Python for the scripting, instead of Gimp-perl (ugh..) ;-). I'm sure Shawn Gorden is around... so please, tell us more, Shawn!!"
    author: "Eron Lloyd"
  - subject: "Re: I thought that Krayon was alive and well..."
    date: 2000-11-21
    body: "TheKompany has it listed on their projects tab, but the link goes to KImageShop. I thought I had heard the same thing though. Anyone know anything?\n\nErik"
    author: "Erik Severinghaus"
  - subject: "Re: I thought that Krayon was alive and well..."
    date: 2000-11-21
    body: "I'm always lurking around :).  Here is the deal with Krayon.  We wanted to see it done, and it was stalled, so we solicited new names for the project, and someone suggested Krayon and everyone liked it, so that was it.  Then I had one of my programmers working on it, he got a number of the bugs fixed and then I moved him on something else for awhile.  Our support of Krayon is similar to KWord, but less formal.  Given that John has jumped in so vigorously, we should probably take it off our project page.\n\nWhat we are working on now specifically for Krayon is creating a kio_slave out of the gPhoto libraries.  I would love to see a Konqueror plug-in that let you browse your digital camera contents :).  Our intention is to help out on pieces of KOffice and KDE where we can, but KWord is the only formal committment outside of our own applications.\n\nBTW - we have been working with John a bit, and he is an excellent programmer, I hope we can work even more together in the future."
    author: "Shawn Gordon"
  - subject: "KDE and command line support"
    date: 2000-11-21
    body: "You know, I just got an idea when you mentioned the browsing of a digital camera through Konqueror.  Would there be any possible way to implement Konqueror's browsing capability in the shell?  Perhaps a kiomount or something? =)\n\nAlso useful on this subject of command line support would be something similar to the Windows \"start\" command.  Basically a program which launches files from the shell using the program associated with that type of file.\n\nbash$ start file.html\nbash$ start ..\n\nwould both launch Konqueror\n\nbash$ start file.mp3\n\nwould launch xmms\netc.\n\nJust some thoughts!\n\n-Justin"
    author: "Justin"
  - subject: "Re: KDE and command line support"
    date: 2000-11-22
    body: "already works, do:\nalias start 'kfmclient openURL'\nand you can do the things you mentioned."
    author: "Simon"
  - subject: "Re: KDE and command line support"
    date: 2000-11-22
    body: "simple.... make a /usr/sbin/start sript that takes one or more arguments after konqueror, since it takes path as an argument :)\n\n/kidcat"
    author: "kidcat"
  - subject: "Just a little word"
    date: 2000-11-22
    body: "Just a little word to congrats you for the choice of \"Krayon\". It's far cuter (kuter ?) than KImageShop. Although it could be a nice name for a Ray-Tracing apps also (K Ray On) ;\u00b7)"
    author: "GeZ"
  - subject: "Why not just use the new Gimp 2.0 backend?"
    date: 2000-11-23
    body: "The new gimp 2.0 backend is in production.  The GUI is totally seperate from the imaging and effects/scripting engine and KDE can have their own front end put their own functionality and wrappers for using it as an editor in other programs.  They would have the same functionality because this set of libs is totally GTK free.  \n\nLook in cvs.gimp.org and look around in gimp2 and gegl (the actual imaging system) it is going to be totally redesigned to take and use CYMK and 16-bit color channels with an advanced undo system with branches and many other cool ideas.  Look at the documentation.\n\nMakes me pee my pants as a graphic designer to have this much power."
    author: "Emmanuel Mwangi"
  - subject: "Re: Why not just use the new Gimp 2.0 backend?"
    date: 2000-11-30
    body: "hmmm.... so there will be a Kimp after all, interesting...."
    author: "reihal"
  - subject: "Re: Why not just use the new Gimp 2.0 backend?"
    date: 2003-02-26
    body: "Are there any plans and people willing to work on a Kimp? I hope so."
    author: "Anonymous"
  - subject: "Re: Why not just use the new Gimp 2.0 backend?"
    date: 2003-02-26
    body: "Of course.. there have *always* been plans and people willing to work on a Kimp, but it's really not possible until the GIMP folks act on the long awaited promises and seperated gimp from gtk :)\n\n(which seems to be happening now :))"
    author: "fault"
  - subject: "Re: Why not just use the new Gimp 2.0 backend?"
    date: 2003-02-27
    body: "Cool, can't wait for the Kimp then......, it will sure be the highlight of my day when it is released. Will it become part of Koffice you think?"
    author: "Anonymous"
---
<A HREF="mailto:jcaliff@compuzone.net">John Califf</A> wrote in to tell us that, "<EM>Krayon (also known as Kimageshop) is alive and well and under active development now.</EM>"  Read below about the status of this important <A HREF="http://koffice.kde.org/">KOffice</A> component.  Has anyone used it lately?  What features are most important to add/get working before Krayon is distributed with KOffice?






<!--break-->
&nbsp;<BR>
<p>
Krayon is a component of <A HREF="http://koffice.kde.org/">KOffice</A> for editing bitmapped graphics, with the goal of being KDE's equivalent of the <A HREF="http://www.gimp.org/">Gimp</A>.  Due to other commitments by the original developers, Krayon has stagnated for almost a year, but I have now taken over its active development.
</p>
<p>
The latest cvs version, avaliable in the KOffice <A HREF="http://www.kde.org/anoncvs.html">cvs</A>, is quite stable and somewhat usable.  You can load and save in the native .kis format, do some simple editing with the brush tool,\ and import from or export to any of the standard image formats supported by <A HREF="http://www.trolltech.com/">Qt</A>.  Some of the other tools also work - zoom, moving layers and other layer operations, but most tools are still incomplete. You can edit any number of images at one time using the tabbed interface, without having to make a mess all over your desktop with numerous windows and dialogs and menus.
</p>
<p>
I'm uncertain of when Krayon will be ready for release with the rest of KOffice.  It's mainly a matter of deciding how complete it needs to be in terms of features for inclusion.  With this kind of app there is no end to features that can be added.  It already has an interface for tools and plugins that can be reused as new tools and plugins are developed.  My estimate is that Krayon will be "stable" and "complete" enough by early next year.
</p>
<p>
It's well worth your trouble to compile this app and play around with it now.  I hope that within a month or two more tools will be usable.  Of course all these tools are custom made and can't use Qt painter routines - it's similar to Gimp in that way vis-a-vis <A HREF="http://www.gtk.org/">Gtk</A>.  The user inferface is quite nice.
</p>
<p>
Krayon desparately needs a documentation writer.  No translations yet, just the basic docbook stuff.  I've received some unexpected but very welcome help from another KOffice developer with patches to code and expect help from others also.  If you have some time to work with Krayon and document its capabilities, please contact me or the KImageshp mailing list.
</P>
<P>
That list has been reactivated and all development patches should be posted there for comment. Discussions should also take place there.  Previously I'd been using the KOffice lists. 
</p>
<p>
More information about Krayon and other KDE graphics apps is <A HREF="http://users.compuzone.net/jcaliff/software.html">available on my web site</A>.
You'll find some screenshots of several KDE graphics apps (including Krayon) and general non-technical descriptions.  Quite an attractive site, oriented towards non-techies. Please visit.
</p>
<p>
Cheers,  John
</p>





