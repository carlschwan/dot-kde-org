---
title: "Qt, now also GPL!"
date:    2000-09-05
authors:
  - "numanee"
slug:    qt-now-also-gpl
---
Trolltech has <a href="http://www.trolltech.com/company/announce/gpl.html">announced</a> that Qt 2.2 will be released under the GPL!  Read what LinuxPlanet/LinuxToday has to say about this <a href="http://linuxtoday.com/news_story.php3?ltsn=2000-09-04-002-21-PS-BZ-KE">here</a>, ZDNet also <a href="http://www.zdnet.com/enterprise/stories/main/0,10228,2623592,00.html">reports</a>, and an editorial by Matthias Ettrich and Eirik Eng <a href="http://linuxtoday/news_story.php3?ltsn=2000-09-04-013-20-OS-LF-KE">here</a>.



<!--break-->
<p>
Trolltech definitely deserves a big thanks on this.  This move should squash the license flamewars, will lead to better code sharing (Kimp or KEmacs, anyone?), and will improve KDE acceptance in general.  As a user of Debian, personally I can't wait for KDE inclusion in the dist.


