---
title: "Free KDE 2.0 Development Book Now Available"
date:    2000-11-10
authors:
  - "dsweet"
slug:    free-kde-20-development-book-now-available
comments:
  - subject: "Re: Free KDE 2.0 Development Book Now Available"
    date: 2000-11-10
    body: "Great!\nhad a first look, looks good...\n\nbut might be that this announcement triggered the /. effect? or is that site always so slow?"
    author: "Mathias H."
  - subject: "Re: Free KDE 2.0 Development Book Now Available"
    date: 2000-11-10
    body: "It's not always so slow ;) it's just that the initial announcement of the book has given my web host provider more traffic than they've ever seen. \nThey're also in the process of doubling their bandwidth capacity, so it should perk up.\n\n<P>\nDave"
    author: "David Sweet"
  - subject: "Re: Free KDE 2.0 Development Book Now Available"
    date: 2000-11-10
    body: "Will there be paper printed copy of this anywhere?"
    author: "Benjamin Meyer"
  - subject: "Re: Free KDE 2.0 Development Book Now Available"
    date: 2000-11-10
    body: "It will be available next week in \"a store near you\".  You can preorder online right now at all of the usual places where you'd buy this kind of book.  (In fact, you can be really nice and order via the link on the book's web site if you want. ;)\n\n<P>\nDave<BR>\n<A HREF=\"http://www.andamooka.org/~dsweet\">http://www.andamooka.org/~dsweet</A>"
    author: "David Sweet"
  - subject: "Wow, Andamooka is damn slow !!!"
    date: 2000-11-10
    body: "I like the concept of having user discussion within the book. So I would prefer this version instead of the paper one. \n\nBut the webside is such slow, and even got several server errors. I think this host is simply not capable of serving such an ammount of requests."
    author: "Andre"
  - subject: "Ouchh, is that site slow!!!!"
    date: 2000-11-10
    body: "Downloading this thing is as impossible as it was to download the KDE2.0 sources just after the announcement.\nAre there any mirrors (not of KDE, of the book)??"
    author: "grovel"
  - subject: "Re: Ouchh, is that site slow!!!!"
    date: 2000-11-10
    body: "There are a few places that are in the\nprocess of putting up mirrors.  They'll\nall be listed at <A HREF=\"http://www.andamooka.org/files.shtml\">http://www.andamooka.org/files.shtml</A> and\nthere'll be a note on the <a HREF=\"http://kde20development.andamooka.org\">books's home page</A>  saying where to go.\n<P>\nDave"
    author: "David Sweet"
  - subject: "Posted to Slashdot.org"
    date: 2000-11-12
    body: "Theres publicity for you. I posted this to www.slashdot.org and it got accepted! Check it out <a href=\"http://slashdot.org/article.pl?sid=00/11/11/160215&mode=nested\">here</a>.  <p>Go KDE!<p>\n--\nChris Aakre"
    author: "kupolu"
  - subject: "Re: Posted to Slashdot.org"
    date: 2000-11-12
    body: "O no! It will be slashdotted forever!"
    author: "reihal"
  - subject: "Re: Posted to Slashdot.org"
    date: 2000-11-12
    body: "Luckily the book has been mirrored at a few places\nand Andamooka is back up to speed.\n<P>\nI'm happy to see that there's been a lot of\nannotation/discussion of the text going on in the past few days.  If you're reading the book,\nyou should probably check out this annotation\nb/c some good stuff (errata, etc.) has come up.\n<P>\nDave"
    author: "David Sweet"
  - subject: "Re: Offline annotations?"
    date: 2000-11-13
    body: "<p>Will the annotations be available for download as well? I think it's a good idea to at least assemble the important issues, so that those of us who don't have permanent Internet access can read them, too. Perhaps an <em>&quot;Appendix D&quot;</em>?</p>\n\n<p>I'm downloading the HTML version right now, and have already ordered the paper version&nbsp;;-)</p>"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: Offline annotations?"
    date: 2000-11-13
    body: "I'm working on it.  It'll be something like this:\n\t- download whole book or just a chapter (or maybe even section)\n\t- with or without annotation (the annotation will be mixed in with the text like it is online)\n\t- choice of moderation-score threshold for annotation\n\t- choice to include \"just my annotation\" or all of the community annotation\n\t- choice of plaintext, HTML, PDF, or (when I get to it) openEBook formats\n\nIf you have any other suggestions, I'd love to hear them.\n\nOf course, I'll also probably need more bandwidth to handle this.\n\nDave"
    author: "David Sweet"
  - subject: "Re: Offline annotations?"
    date: 2000-11-14
    body: "<p>Sounds great!</p>\n\n<p>However, it would also be nice if the annotations could be downloaded separately, so that I can print them and keep them with the (paper) book.</p>"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: Free KDE 2.0 Development Book Now Available"
    date: 2000-11-13
    body: "Does the paper version of the book include the APIs?\n\nI've got the HTML version and it's very impressive. Great job guys..."
    author: "jpmfan"
  - subject: "Re: Free KDE 2.0 Development Book Now Available"
    date: 2000-11-14
    body: "No, there's no API reference in the print verion.\nIt was just HUGE and I figured it would be pointless to print it.  Considering that it seemed like it would be <B>so</B> much easier to navigate it online than in a book and that people don't generally read the API (like they do a chapter, i.e. from start to finish) it was best to leave the API out.\n<P>\nAnother plus is -- I can't verify this -- but I think it lowered the price of the book by $10.\n<P>\nDave"
    author: "David Sweet"
  - subject: "Re: Free KDE 2.0 Development Book Now Available"
    date: 2000-11-14
    body: "What about Making printed book consists of KDE API.I myself prefer to have printed document rather than online html files. I know KDE API will still changes but sometimes in the future I think it will be helpful that KDE API is well documented in form of books. \nOne good thing about Windows is its API is very well documented."
    author: "jamal"
  - subject: "Re: Free KDE 2.0 Development Book Now Available"
    date: 2000-11-14
    body: "If I can't get it in a book, is there anywhere where you can download it in a tarball or similar? I would prefer to have my own local copy than having to rely on a slow V90 modem... I'd like to start programming in KDE but it would be easier to have my own APIs on disc somewhere than having to dial up my internet connection everytime I forget what the parameters for such and such a method are."
    author: "jpmfan"
  - subject: "Re: Free KDE 2.0 Development Book Now Available"
    date: 2000-11-15
    body: "<p>I've downloaded the API reference from <a href=\"http://www.mcp.com/sams/display_sams.cfm?item=0672318911&itemhdr=Downloads&itemloc=downloads\">here</a>.</p>"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "I read this book"
    date: 2000-11-14
    body: "I've read this book and it's very good!  I am quite sure this book will give rise to a lot of new KDE2 developers.\n\nCongrats to the authors!"
    author: "Anonymous"
---
KDE 2.0 Development, a new book being published under the 
Open Publication License is now available online, in full, for free, at 
<a href="http://kde20development.andamooka.org/">http://kde20development.andamooka.org/</a>.

The site also holds all of the source code from the examples in the book,  
<a href="http://www.andamooka.org/kde20devel/Files/kde20develhtml.tgz">downloadable HTML</a> and <a href="http://www.andamooka.org/kde20devel/Files/PDF">PDF</a> versions, an annotated online-only <a href="http://www.andamooka.org/reader.pl?pgid=kde20develAppendixBindex">abridged KDE API 
reference</a> (Appendix B), and an annotated version of the <a href="http://www.andamooka.org/reader.pl?pgid=kde20develkdedevelhowtoindex">KDE Developer's HOWTO</a>.
<!--break-->
<p>
The host site is part of <a href="http://www.andamooka.org/">Andamooka</a> which runs web software that lets visitors 
annotate and discuss each section of the book and view the annotation along 
with the original text.  This site will serve as an "open support" system for 
readers of the book.
<p>
The book is written for C++ programmers with or without previous KDE coding 
experience.  It covers introductory KDE/Qt programming, KDE UI design, 
KParts, DCOP, aRts, XML GUI, KDevelop, licensing issues, CVS, code and 
end-user documentation, and more.  
<p>
Many chapters are written by contributing authors who have helped design 
and/or implement the systems about which they wrote.  These authors are: Kurt 
Granroth (also technical editor), Cristian Tibirna, David Faure, Espen Sand, 
Stefan Westerfeld, Ralf Nolden, Daniel Marjamaki, and Charles Bar-Joseph, and 
a forward was written by Matthias Ettrich.
<p>
Since the book is released under the Open Publication License, it may be 
modified and redistributed online, which means that the book can be 
maintained (fixed, updated, expanded etc.) in the style of a free software 
project.  In this spirit, volunteer translation of the book into five other 
languages has already begun.  I welcome any other feedback or contributions 
to the maintainance and development of the book.  Everything that is  
incorporated into official versions of the book will be released under the 
Open Publication License electronically and for free.
