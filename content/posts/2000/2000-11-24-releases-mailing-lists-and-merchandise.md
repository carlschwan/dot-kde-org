---
title: "On Releases, Mailing Lists and Merchandise"
date:    2000-11-24
authors:
  - "Dre"
slug:    releases-mailing-lists-and-merchandise
comments:
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-24
    body: "I hope that in KDE2.0.1 can support Chinese input and display no need any patch."
    author: "younker"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-24
    body: "That's a good idea, and that is important.\nIs there any way to embed xcin?\nAnd about display,\nI think it would be great if we can choose fonts\naccording to different encodings-- like netscape 6."
    author: "Kevin Chen"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-24
    body: "Agreed. Chinese input and display would be an important addition to KDE2.x\n\nJSC"
    author: "Jethro Cramp"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-25
    body: "If you have patches (for Chinese support or for anything else) that you would like to see included\nin KDE 2.0.1 you will have to send them to the mailinglist or one of the KDE developers _NOW_.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-25
    body: "Hello, I'm a java developer and I'm currently working in a Linux Box with KDE 1.1.2. I'm using JBuilder on Linux with a java virtual machine 1.3, it works very well but I have a trouble when I try to work on KDE2, the clipboard doesn't work!!!, I can copy and paste between KDE2 applications and JBuilder. I'd like to know what happend, please can someone explain me what happen?? \n\nThanks"
    author: "alexia"
  - subject: "Just a question on KDE2 / kernel2.4"
    date: 2000-11-25
    body: "I have just a question concerning KDE2 and the coming soon 2.4 linux kernel. Does this new kernel have an impact on KDE2 implementation? A recent article in linuxtoday gives a list of the new features of this kernel. They are talking of modification in device management. Do you know if it may impact KDE components like Control Center / Information. There are also other modifications like system functions API, but I assume there is also a backward compatibility with the current (?).\n\n    Thanks and Regards"
    author: "Nicolas"
  - subject: "Re: Just a question on KDE2 / kernel2.4"
    date: 2000-11-25
    body: "Hi,\n\nI'm not too deep in the 2.4, but as long as the old structures under /proc are still there, it should not affect the information bits in kcontrol... And as no app (known to me...) calls the kernel directly, it should have no impact...\n(Corrections accepted)"
    author: "Mathias Homann"
  - subject: "Re: Just a question on KDE2 / kernel2.4"
    date: 2000-11-25
    body: "I have been using the 2.4test kernel for months and kde has always worked just great so I wouldn't worry about it. I'm sure that many of the KDE developers also use 2.4test kernels from time to time for various reasons.\n\nMatt Newell"
    author: "Matt Newell"
  - subject: "Re: Just a question on KDE2 / kernel2.4"
    date: 2000-11-27
    body: "One really broken thing I have met (Currently\nI'm using kernel 2.4.0-test10 with devfs \nmounted and devfsd running) is kpm:\n\nIt's showing nonsense instead uf UIDs.\n\nSo I'm currently running gtop instead (it doesn't have similar problem)"
    author: "Andris"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-25
    body: "I appreciate the mention of the CafePress site I setup to help raise a bit of money for KDE. I even understand the reluctance to believe that I intend to donate all the profits (I do. <a href=\"http://www.cafepress.com/\">CafePress</a> makes it brain-dead simple to setup this sort of thing), but was the \"caveat emptor\" comment <i>really</i> necessary ??\n<p>If an individual is concerned about being stuck with a product they dislike, they shouldn't be. CafePress offers a very generous <a href=\"http://www.cafepress.com/cp/home/help/customerfaq.jsp#break\">return policy</a>. As far as how trustworthy I am, well...I guess there's no way for anyone who doesn't know me to be sure. I was just trying to think of someway to support KDE since I'm not a developer (or at least not a good enough developer to matter ;)"
    author: "David Huff"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-26
    body: "Am I missing something here? It seemed to me that the article wasn't insulting your trustworthiness at all. Ah well. In any case, I want you to know that KDE merchandise is always cool. The only thing we need now is KDE deoderant (smells dragon fresh!) :-)"
    author: "David Simon"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-26
    body: "How about the Konqi mouthwash, makes your breath like fire."
    author: "reihal"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-26
    body: "Well, <i>caveat emptor</i> is a Latin phrase that basically translates to \"let the buyer beware\" ;) And while it's always good advice, it just struck me the wrong way. After all, the point is that the more sales the site generates, the more money gets donated to the KDE project. Seems a bit counter-productive to potentially put off people with such a comment. If anyone has questions/concerns about this, please feel free to use my email addr to contact me (and as I mentioned, CafePress has a good return policy in case you don't like what you get). \n<p>On a more positive note, I recv'd a shirt and mug that I ordered for myself before I publicized the site, and the production quality is quite good, IMHO :) Don't let the thumbnail images on some of the items give you a bad impression, but click on the link to see the image in a larger size..."
    author: "David Huff"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-26
    body: "GNU is communism!\nRMS is a jerk!"
    author: "Anonymous"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-26
    body: "Yeah, so?"
    author: "Johan"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-27
    body: "I'm sure he's very happy that you're following his  fine example...\n\nLong live RMS! Long live GNU! Long live KDE!"
    author: "jpmfan"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-27
    body: "Long live little ignorant me!\nMe sucks, so me talk nosense!"
    author: "jpmfan"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-28
    body: "Oh look, my computer has managed to post a message to dot.kde.org all on its own. It managed to unravel my phone extension, plug the modem into the right socket, turn itself on and dial the web site and post a very funny, original and grammatically correct message on its own. Now that's impressive...."
    author: "anonymous"
  - subject: "Re: On Releases, Mailing Lists and Merchandise"
    date: 2000-11-28
    body: "Grow up, you two..."
    author: "anonymous"
---
<A HREF="mailto:david at mandrakesoft.com">David Faure</A>, the new KDE release
coordinator, has set forth the release schedule for KDE 2.0.1, a bugfix and translation release, and KDE 2.1.  KDE 2.0.1 should be available on December 4, and
the first KDE 2.1 beta two weeks later on December 18 (in time to test it out over the Holidays!).  <A HREF="mailto:jaldhar at debian.org">Jaldhar</A> told us about a new <A HREF="http://www.debian.org/">Debian</A> mailing list for Debian KDE users and package maintainers; subscribe at <A HREF="mailto:debian-kde-request@lists.debian.org"> debian-kde-request@lists.debian.org</a> with the word "subscribe" in the body of the message.  <A HREF="mailto:david at dhuff.org">David Huff</A> points to a new section at CafePress.com where you can buy KDE <A HREF="http://www.cafepress.com/kde">shirts, mugs and mousepads</A>, with all profits promised to the KDE project. Caveat emptor.


<!--break-->
