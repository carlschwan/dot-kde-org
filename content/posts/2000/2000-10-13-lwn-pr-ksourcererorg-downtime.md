---
title: "LWN on PR, KSourcerer.org downtime"
date:    2000-10-13
authors:
  - "numanee"
slug:    lwn-pr-ksourcererorg-downtime
comments:
  - subject: "Re: LWN on PR, KSourcerer\u00a9org downtime"
    date: 2000-10-14
    body: "I'm sure there are lots of people \u00a5myself included\u00a4 that would be willing to mirror the page for a while if need be, until the switch\u00a9"
    author: "Colin Davis"
---
The <a href="http://lwn.net/2000/1012/">latest edition</a> of LWN has a nice editorial on PR in free software projects.  Worth a read.  <i>"An often-heard sentiment among KDE developers is that they may have a better desktop, but that the project has taken a number of hits on the public relations front. This idea was made more explicit this week with <a href="http://dot.kde.org/971269290/">this KDE Dot News editorial</a> on how to improve KDE's public image."</i> Item 2: We've received word from <a href="mailto:Victor_RoederNOSPAM@GMX.de">Victor Röder</a> that <a href="http://www.ksourcerer.org/">KSourcerer.org</a>, a cool developer website dedicated to KDE, has been down lately and will remain unavailable until late October/early November when the switch to a new service provider is complete.