---
title: "KDE Dot News goes live!"
date:    2000-09-21
authors:
  - "numanee"
slug:    kde-dot-news-goes-live
comments:
  - subject: "Keep you site clean"
    date: 2000-09-21
    body: "Congratulations on the idea and its implementation. This site is a worthwhile addition to the existing newsgroups, as you can drop by\nat any time and check whats up.\n\nIt is delighting to see such a clean, fresh and\nplain layout, compared to such a monster of a html\npage as slashdot.\n\nkeep up the good work\n--\nis it me or is the world spinning?"
    author: "Matthias Lange"
  - subject: "Re: Keep you site clean"
    date: 2000-09-21
    body: "Hee hee hee.  Making \"Plain Text\" the default posting mode was definitely something I was planning to do.  It seems to have disappeared from my TODO list what with all commotion.  ;)\n\nThanks for your kind comments!\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Keep you site clean"
    date: 2000-09-21
    body: "Congratulations on the idea and its implementation. This site is a worthwhile addition to the existing newsgroups, as you can drop by\nat any time and check whats up.\n\nIt is delighting to see such a clean, fresh and\nplain layout, compared to such a monster of a html\npage as slashdot.\n\nkeep up the good work\n--\nis it me or is the world spinning?"
    author: "Matthias Lange"
  - subject: "Re: KDE Dot News goes live!"
    date: 2000-09-21
    body: "Just a little addition: the site is also accessible through the <a href=\"http://www.kdenews.org/\">kdenews.org</a> URL, easier to remember I think ;-)\n"
    author: "raphinou"
  - subject: "Re: KDE Dot News goes live!"
    date: 2000-09-21
    body: "Well, this is exactly that kind of site i was looking for since months and hoped to find in kde.com ;-)\n\nPlease feel yourself bookmarked by myself on my company machine (win f***ing dows) as well as at home on my beloved Red Hat 6.2 :-))\n\nKeep the faith, I will keep my KDE... ;-)"
    author: "((c3k:::"
  - subject: "Re: KDE Dot News goes live!"
    date: 2005-07-18
    body: "Hi there, I've just installed SUSE 9.3 in my toshiba notebook, and after the installation i noticed that the KDE interface does not start anymore (sorry, i forgot to tell that before the SUSE 9.3 I was runnig SUSE 9.0 on the same computer and everithing was working fine). When I start the environment in SAFE mode he works fine, but with the normal login nothing happens. Does anybody knows why ?\n\nThanks in advance,\n\nRaphael"
    author: "Raphael"
  - subject: "Re: KDE Dot News goes live!"
    date: 2005-07-18
    body: "Replying to a five years old message surely won't gain you the answer you look for... as for your SUSE stuff SUSE does have extensive community mailing list where you should ask instead."
    author: "ac"
  - subject: "Re: KDE Dot News goes live!"
    date: 2000-09-21
    body: "Congratulations from me, too. I'm looking forward to see lots of interesting discussions here. KDE Dot News will certainly server as a good forum for user and developers.\n\nHarri.\n\nP.S. This more than a pure test of the posting mechanism :)"
    author: "Harri Porten"
  - subject: "Great!!!"
    date: 2000-09-21
    body: "<P>Thanks a lot to everyone who contributed to making this happen! This is definitely a site I have been waiting for, and will visit often.</P>\n<P>Cheers.</P>"
    author: "Vad"
  - subject: "Those copycat Gnomers..."
    date: 2000-09-22
    body: "KDE Dot News is only a few days old, and already they've copied it!"
    author: "Alfhiem"
  - subject: "Re: Those copycat Gnomers..."
    date: 2000-09-22
    body: "Oh yes, I was totally inspired by that site.  I had always liked it very much and I was impressed by how useful the concept turned out to be.  Where's the harm in that?\n\nI did try to avoid some of their mistakes though.  For example, you don't see any annoying redirections here and you don't have Netscape or Konqi trying to connect to NNTP when you type in a short URL.  Although you *can* use http://news.kde.org/ if you want to.  ;-)\n\nCheers,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "More Community"
    date: 2000-09-24
    body: "I think it's cool that I can now browse\n          through ongoing comments and discussions\n          on the progress and thoughts of KDE. As soon as I got into\n          Linux (SuSE flavor) and found KDE I stopped with KDE as\n          it was the cleanest, most organized, and easy to use out of\n          all the other GUI's. I tried Gnome, but it was too awkward\n          for me. I'm totally looking forward to KDE 2.0 and have\n          been for probably 1 to 2 years now. I CAN'T WAIT!!!"
    author: "Brandon Kelton"
---
The <a href="http://www.kde.org/">KDE</a> Desktop Environment project is pleased to announce the launch of <a href="http://dot.kde.org/">KDE Dot News</a>, a news and discussion site dedicated to KDE and supported by the KDE community.  KDE Dot News is the response to a large and growing demand for a KDE-specific news site.

We hope you will like what you find here, including KDE development and user news, discussions, feature articles and more. The only catch is that we ask that you help us out by actually submitting articles. Read on for a few more details.

<!--break-->
<p>
<h3>Manifesto</h3>
<p>
The aim of this site is to help foster the growth of the KDE community by providing developers, users, and other parties a convenient means of making the community aware of relevant developments, as well as providing a means for public input for the general advancement of KDE.
<p>
Future ideas and plans for the site include exploiting the full potential of the backend, such as: providing different frontends, setting up translated versions of the site, providing news for the main KDE page, generating weekly summaries, and providing different sub-sections as is found useful.
<p>
<h3>Technology</h3>
<p>
KDE Dot News is based on a slightly hacked Squishdot 0.7.x, Zope 2.2.x, and Python 1.5.x, with an Apache proxy thrown into the equation.  The site is hosted, courtesy <a href="http://www.kde.com/">KDE.com</a>, on a beefy Linux server.
<p>
<h3>Credits</h3>
Thanks go to everyone involved including: Raphael Bauduin for writing
several articles, Kurt Granroth for the site logo, Andreas Pour for
the web hosting and for hours of technical and admin level support,
Navindra Umanee for hours spent installing, customizing, testing and
debugging the site, and Nadia D'Alfonso, Serge Arsenault, Victor Calzado, Matthias Elter, David Faure, Serena Fenton, Arnt Gulbrandsen, Martin Konold, Ulrich Schreiner, Casey Allen Shobe, Bill Soudan, Cristian Tibirna, and Robert Williams for their input, suggestions,
admin or article contributions, and, last but
not least, the <a href="http://www.kde.org/artist/index.html">KDE
artists</a> for all the icons and art that were shamelessly "borrowed"
from KDE CVS.
<p>
Comments or suggestions? Click on the Reply link below!  Otherwise, we can be reached at <a href="mailto:dot@kdenews.org">dot@kdenews.org</a> and bug reports on the beta site can be sent to <a href="mailto:bugs@kdenews.org">bugs@kdenews.org</a>.
