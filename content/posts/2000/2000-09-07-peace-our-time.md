---
title: "Peace in our time?"
date:    2000-09-07
authors:
  - "numanee"
slug:    peace-our-time
---
Dennis E. Powell at LinuxPlanet has a new <a href="http://www.linuxplanet.com/linuxplanet/opinions/2276/1/">article</a> out.  This time he writes about the recent events in KDE history, and also includes a bit about the guys from Helix Code.  It's all good.


<!--break-->
