---
title: "KDE Games Center"
date:    2000-09-21
authors:
  - "raphinou"
slug:    kde-games-center
---
Surfing the official KDE website, I found the budding <a href="http://www.kde.org/kdegames/">KDE Games Center</a> where you can find a list of games based on KDE (and not only those included in KDE). There are also several links to developer resources such as AI, network programming,...
If you develop a game for KDE, then please contact <a href="martin@heni-online.de">Martin Heni</a> with the info.   













<!--break-->
