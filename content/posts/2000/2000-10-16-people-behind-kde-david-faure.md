---
title: "People Behind KDE: David Faure"
date:    2000-10-16
authors:
  - "rwilliams"
slug:    people-behind-kde-david-faure
comments:
  - subject: "Re: People Behind KDE: David Faure"
    date: 2000-10-16
    body: "<I>I don't sing in the shower. Does everyone else really do, as implies the question? I play the piano though</I> \n<br>\n<br>\na. How on earth did you manage to fit your piano there?<br>\nb. Doesn't it get wet?"
    author: "ac"
  - subject: "Piano in the shower ?"
    date: 2000-10-16
    body: "Come on the man's a programer.  How could he not have the absolute latest technology at his disposal ?"
    author: "Borg"
  - subject: "Re: Piano in the shower ?"
    date: 2000-10-16
    body: "Nah, the man can't speak English properly.\n\nI meant that I don't sing usually, but if I play the piano BEFORE the shower, then it puts songs\nin my head :)\n\nBut I like the idea of a waterproof piano, yeah,\nthat'd be cool !"
    author: "David Faure"
  - subject: "Re: People Behind KDE: David Faure"
    date: 2000-10-16
    body: "ROTFL!"
    author: "ac"
---
This week's installment of <A HREF="http://www.kde.org/people/people.html">The People Behind KDE</A>, interviews <A HREF="http://www.kde.org/people/david.html">David Faure</A> of Konqueror fame.  David has done some amazing programming for KDE and continues to impress. What is David's current role in KDE? 
"<I>Doing the dirty job :). Fixing bugs everywhere I can, helping users and developers that have problems. I also work on some Mandrake-specific KDE stuff. Oh, and when I have time after all this, I work on improving konqueror and I plan to do more on koffice in the future.</I>"


<!--break-->
