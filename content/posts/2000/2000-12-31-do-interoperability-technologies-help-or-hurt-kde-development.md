---
title: "Do Interoperability Technologies Help or Hurt KDE Development?"
date:    2000-12-31
authors:
  - "Dre"
slug:    do-interoperability-technologies-help-or-hurt-kde-development
comments:
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "It will hurt KDE development.  This is exactly why Micro$oft does what they do.  They know that people will develop for the largest possible audience.  As such they make it as easy as possible to port your code from another system.  Ie. Porting from Mac has tons of assistants and wizards to help get your code to work on M$ OSes.  \n\nThe better approach would be to allow KDE apps run in GNOME land but not the other way around.  That way people see the best that KDE has to offer and then \"lure\" them over to KDE.\n\nIf you look at (yes I hate to bring them up again but they have gotten where they have because they have been smart) M$.  They license their technology very limitedly to Unix to basically encourage folks to come over to M$.  Not for M$ developers to go over to Unix.  This kind of mentality needs to be brought in for the KDE official approach.  \n\nNow the great thing about Open Source and this kind of development environment is that someone else will go and make these amazing products such as XParts & GTK widget containers.  However these should not be \"officially\" supported apps.  That way its not something that developers heavily depend on.  They then know also that support as KDE is developed is entirely up to that third party and compatibility is not a priority for the KDE core development team.\n\nAll in all everything here has been fantastic work.  I don't want to discourage it but we have to be prudent about what is done for the greatest good while not impeading anyone else."
    author: "iguy"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "Yes, but the big difference between the Windows/M$/etc. world and the Free Software world is this: MS is out to compete with and destroy their competitors in order to further their commercial success. Those are their goals. However I believe (idealistically, I guess) that the goals of free software developers are much closer to \"make the world a better place by writing good software for everyone, that is free (speech/beer)\".\n\nSo in light of what KDE is doing, I think they're doing the right thing. Sure it may or may not have negative effects on their mindshare, but is that really important? If they're improving computing under Linux/BSD/* in any way, then in my opinion it's great. We don't need KDE vs Gnome flamewars. We don't need crazy competition that devolves to the extent of what you see in the commercial software world.\n\nIt's actually very refreshing to see the developer who did this, take a step back and realise that free software development is about more than 'my project is better than yours and we don't want anyone to use yours', but to actually look at the bigger picture and work towards something positive for everyone."
    author: "Matt"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "When Open Source people mention \"world domination\" as a goal, don't take it too seriously. As I see it, the goal is not to have KDE installed on as many desktops as possible, but to make KDE the best desktop possible both for the users and from a technical point of view. So whether or not interoperability technologies help KDE to dominate the Unix desktop is not important.\n\nMicrosoft's tactics may be a commercial success, but it hasn't made them popular, especially among Open Source developers. I wouldn't recommend the way Microsoft treats their competitors as a rolemodel to anyone..."
    author: "Maarten ter Huurne"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-05
    body: "Yes, but on the other hand helix code tries to dominate\nthe unix desktop by declaring gnome as the\nstandard desktop. And, because, in my view, kde is technically superior, why should the kde crowd not\ntry to turn the thing round and use some tactics\nto make kde the standard desktop ?"
    author: "Thomas"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "I'm sorry to say, but you've got a lousy alternative.\n\nFrom a business perspective, Microsoft's attitudes and actions are pure gold. They're very good at what they do - killing the competition.\n\nHowever, when you say things like \"we should let KDE apps run under GNOME, but not the other way around\", what are you doing?!? Everyone who wants to use a full KDE desktop is immediately locked into KDE-only apps. That's absurd. I mean, no offense, I like KDE and all, but KMail just doesn't cut it. It's close, but I use a GTK+-based app(Sylpheed) which has just the features I need, and no more. Now, you'd want me not be able to run this under KDE? How does that help me?\n\nNo, to suggest such a thing means that instead of getting ahead by being good, you'll get ahead by making the other guys bad. That's unacceptable, and whenever I see that attitude around me, I make sure to change it. Real Quick(tm).\n\nDave"
    author: "David B. Harris"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-02
    body: "I can't agree with you in any point. It is not a problem for KDE when there is a better compatibilty with the Gnome Desktop. Perhaps it will bring some people to use gtk instead of QT but where is the problem? The Unix Desktop will never be accepted if there there are two incompatible desktops. But if you can use the same apllications in any desktop system you can decide which to use and don't have any disadvantages because there is one little program that is only availabl for the other desktop system.<br>\nThis should be the futury: There are two compatible desktop systems and everybody can desides which looks better for him but this decision will not bring any disadvantages! This will beat Micro$oft because the only advantage for beginners is that there is only one deskop system. But I prefere the freedom to choose my system."
    author: "Heiko"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-03
    body: "I don't think any of the c-coders, writing gtk apps, could (or would like to) handle QT without relearning almost everything... This makes it hard to get the gnome/gtk coders to start writing pure kde programs, but with the QGtk classes more programs, that was limited to gnome, will now run as native kde apps and I think that helps kde to be a better and wider desktop.."
    author: "Martin Andersson"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-05
    body: "??? but the qt/kde libraries are much easier\nto work with than gtk. So what ?"
    author: "Thomas"
  - subject: "It will NOT hurt KDE development"
    date: 2001-01-04
    body: "I don't think this type of stuff can hurt KDE development.\nLooking at the APIs of Qt and gtk, I can't see any developer switching from Qt and gtk by his own choice.\n\nHaving GNOME land embedding KDE applications would IMO not be a very good idea for KDE's spreading (though it would be a good idea in general, our enemy is/should be Microsoft, not GNOME) - if people can just run KDE applications natively, where's the reason to switch?\n\nThis is nowhere near the M$ tricks because the limitations are missing."
    author: "Bero"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "<i> why a Linux developer would now develop for KDE when they can write for Gtk and have the application work both under KDE and GNOME. \n</i><br>\n\nbecause he thinks the kde framework is better?\nthis is open source development, and the kde team and the gnome team are willing to reach the same goal. No need to be afraid of each other...\n<br>\nby the way, gnome apps already run in kde, but now they can be embedded in kde: that brings more tools to kde users, so this is a Good Thing(tm)."
    author: "emmanuel"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "I agree with you :) It shocked me when I first looked at KDE2 source. Amazing - I could *read* it! :)\n\n*THAT'S* why I'll be programming for KDE2. The widget set is all right(nothing amazing), speed is acceptable(not as fast as GTK+ 1.2.x, though), but damnit, I can actually code for it! :)\n\nDave"
    author: "David B. Harris"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "Not to sound trollish, but people will develop for QT because it is a superior C++ OO toolkit. I don't even want to *know* how ugly GTK apps look embedded in QT programs. \n\n--\nkup"
    author: "kupolu"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-02
    body: "they would indeed look pretty ugly.  the GTK+ apps would look beautiful on their own, but if you try and embed them in an ugly QT app, Lord!"
    author: "backer"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-05
    body: "Did you ever look into qt ? Doesn't seem so.\ngtk uses similar technologies as Xt/Motif, which is from the 80's. I started to look into gtk first, but when i compared it with qt, i didn't bother with gtk any minute longer. Not more than a disturbance to me."
    author: "Thomas"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-03
    body: "begin_vent();\n First off,\n  1.) Asthetically speaking; GTK+ (IMHO) is far  more cosmetic then QT.\n  2.) QT is only a superior C++ OO Toolkit over GTK+ because GTK+ is done with C.\n\nSecond,\nWhat's with that \"iguy\" character and his (\"The better approach would be to allow KDE apps run in GNOME land but not the other way around. That way people see the best that KDE has to offer and then \"lure\" them over to KDE.\" and \"yes I hate to bring them (them == MS) up again but they have gotten where they have because they have been smart\" END_QUOTES) comments? \n\nNow, not to sound like a prick here or anything, but are all KDE users gnubies now or what? Open Source isn't about capitalism, it's not about strong arming the competition, open source is a collaboration of developers from all over the world, not market share.\n\nThat's my 2 cents anyway. \nThanks for reading  ;P  \nCSM. \nkernel@nexus-gen.net"
    author: "Carey McLelland"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-04
    body: "<i>are all KDE users gnubies</i>\n<p>\nA large portion of us, yes. KDE makes *nix fun and easy, after all. I think \nyou're right that people often forget about the community concept of free \nsoftware because they're not used to it, but please try to be gentle with \n\"gnubies\". A little time and some experience with these new ideas are required \nbefore they sink in. \n<p>\nNow for my 2 cents on the topic... I think this new ability will be good for \nkde. Most free software developers code for enjoyment and those who choose kde \ndo so because the kde/qt style of coding appeals to them. Also, for those new \nto the scene and trying to choose a platform, this could make kde more \nattractive because it means that developers who code for kde will have a huge \nnumber of components to reuse, including gtk ones. And of course, gnome will \ncontinue to be a fine choice as well. Those who ultimately choose gnome may \neven be encouraged to play with kde, knowing that they can reuse stuff they've \nalready written. The experience they gain from doing so could bring more cooperation to the environments. So really, I see this as having the potential to benefit a \nlot of people without hurting anybody, which is great!\n<p>\n-kdeFan"
    author: "kdeFan"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-04
    body: "I am gentle with gnubies. :) \nEveryone is a gnubie at some point in time, right? But come on now, anyone who states that one open source project should somehow have an advantage over another, at the other projects expense; should know better.. I mean, if I wanted to run software developed by bigots, I sure as hell wouldn't have started using the open sourced *nix's in the first place. \n\nI meant no harm with the gnubie comment, I was just pointing out the obvious, and I hope iguy reads this and realizes that that's not what \"open source\" stands for, and if he does read it, and he understands it, then my job is done. ;]\n\nCheers all! Happy New Millenia"
    author: "Carey McLelland"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-04
    body: "Fair enough. Happy new millenium to you too!"
    author: "kdeFan"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "I don't really think this is the issue at hand between Gnome and KDE.  Seeing as IBM, Solaris, Easle, Helix Code, are all companies that are supporting Gnome.  The issue that Star Office will be ported as a Gnome office tool.  This is really a shame because KDE is hands down the better looking of the two.\n\nWhatever the case, if someone decides to code for GTK it should run in KDE, and vice versa.  If a user decides to use KDE or Gnome, the user should be able to install a base RPM or DEB that would allow them to run the other desktop's applications.  KDE apps should run in Gnome, and Gnome in KDE."
    author: "R. Masci"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "It's unfortunate, but I'm pretty sure that Star Office won't be ported to a native Linux toolkit(like Enlightenment's, or GTK+/GNOME, or QT/KDE). Apparently, from what I've heard, they'll be porting it to Mozilla's XUL.\n\nYup, you heard right. A slow Office app, with a slow toolkit, in a slow graphical environment(sorry, Xfree guys! I appreciate all the hard work you do, but it's not as fast as some of the alternative - but really, thank you so much! :).\n\nYucky, eh?\n\nDave"
    author: "David B. Harris"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "Actually, almost an equal number of companies have joined the KDE League, some are even in both! Company backing probably won't affect it very much, these companies are using this as a way to get involved in Open Source, so that in 5 years they can say \"Hey, eveyone look, we were part of developing this!\". Besides, Solaris (er, I assume you mean Sun Microsystems) has a nasty habit of making software that is good, but rather clunky. Besides, KDE has theKompany. Also, KDE apps already run in GNOME, and vice versa. The problem is that GNOME apps dont use DCOP, and KDE apps dont use CORBA, thus rather limiting the interoperability of GNOME and KDE apps."
    author: "Carbon"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "KDE interoperability technologies just continue the tradition of showing how inferior other technologies are compared to KDE.  Qt has shown this with Motif and Windows, GTK+/GNOME is logically the next."
    author: "ac"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "Gnome/GTK+ isn't that inferior.\nIn fact, the current releases of Gnome/GTK+ are in some ways still superior (although not all).\nAnd the development version can really match KDE/QT."
    author: "Anonymous"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-05
    body: "Nope, it can not. gtk tries to solve an object-oriented problem area (ui programming) with a procedural language (C) instead of an object oriented language (C++). This may work for small programs, but the experiences with Motif showed pretty well that it doesn't work well for any bigger project. Believe, i've programmed Motif for 8 years. And C++ Wrappers around wanna-be object-oriented C-based frameworks showed to be quite crappy.\nThat's why qt/kde programs will remain faster and more reliably than gtk programs at all times."
    author: "Thomas"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "Nice said, but what is it what makes QT/KDE so supperior? I always read how supperior KDE is to GNOME, but I never see any evidence. What killerapplication can you offer. What technologie do you have I would not like to miss. Could you please explain your stupid rant with some facts? Otherwise people will easily realize that you are just talking nonsense.\n<P>\nJacob R. Kreutzfeld"
    author: "Jacob R. Kreutzfeld"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "Are you a developer? Have you looked at the source for Qt/KDE or GTK+/GNOME apps? I am, and I have. Qt/KDE is a lot more advanced/modern from a developers perspective. GTK is messy, and hard to understand, as they are typically written in C using odd OO models. Qt is the nicest GUI dev kit I've ever used. Plus, KParts is way cleaner, leaner, and just easier to understand than Bonobo/CORBA or OLE. What about KIOslaves? Konqueror is the anything browser, and can literally browse any service, protocol, or device if someone takes the time to write an IO slave.\n\nAre you even using KDE 2? If so, you should notice some huge improvements over GNOME. For one thing, it's fast. It starts up quicker than KDE 1.1 or any version of GNOME. It also has a much smaller memory footprint. An how about Konqueror for a killer app? It's by far the best browser for the Linux platform. Once they improve Java/JavaScript support, I will remove Netscape and never look back. How about KOffice? KWord, although a bit young and lacking a few features, loads up in little more time than it takes me to load a terminal. It also does an admirable job reading those accursed Word documents. What does GNOME have? StarOffice? Give me a break! SO takes about 2+ minutes to load on my 600mhz PIII. It also likes to fill the whole desktop, acting like a window manager of its own."
    author: "Aaron Traas"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "I strongly disagree with you.\nI have written tons of GTK+ and Gnome programs, and GTK+/Gnome definitely isn't a big mess.\nIt's a very good environment to program in, and certainly not difficult to understand."
    author: "Anonymous"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "<p>I have to agree with Aaron since I did the same.\nWhile Gnome was my primary desktop (I did not like the look of KDE1).\nI had a look at KDE /Gnome and I came to the conclusion that KDE/Qt\nis from a programmers point of view a lot easier. (And yes, I had a\nlook at gtk-- which is incomplete and not very well documented).\nSo while GTK+ might be easy for you, you should at least have a look\nat KDE/Qt (assuming you know some C++) and probably you will find\nout that KDE/Qt is easier to program :-)</p>\n\n<br>Happy new year, Marco"
    author: "Marco Krohn"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "I have looked a few times at KDE/QT source code.\nThey look a little cleaner than C code, but that doesn't make GTK+/Gnome a complete mess.\nHave you ever looked at gnome-libs' source code?\nOne of the cleanest code I've ever seen!"
    author: "ac"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "I bet you have never written a line of GTK+/Gnome code and never seen any Bonobo/ORBit code."
    author: "Anonymous"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "So what? Nautilus can do the same.\nKIO and kioslaves? I bet you've never heard of gnome-vfs and it's modules.\n\nKDE 2 is not faster than KDE 1. It's even slower.\nGnome and KDE 1 are waaay faster than KDE 2.\nSmaller memory footprint? Yeah right, in your dreams.\nKWord? Ha! Pratically unusable.\nYou might lauch at StarOffice/OpenOffice, but you can't deny the fact that it's better and going to be *much* better!\nSO takes about 45 seconds to load on my P166.\nKOffice takes about 1 minute.\nAnd OpenOffice is going to be *much* faster!\n\nKDE might be better in some cases, but definitely not far superior!\nIf you compare KDE's cvs version with Gnome's cvs version, you'll see that Gnome is a pretty good match."
    author: "ac"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "<br>> And OpenOffice is going to be *much* faster!\n<p>Don't want to disturb your dreams, but concerning\nOpenOffice there was an article in c't\n(an excellent German computer magazine) saying that</p>\n<ul><li>a lot of code is not very well documented</li>\n<li>most of the comments are written in german</li>\n<li>there is a general lack of interest</li>\n<li>the whole package has about 10000 of files and 9 Million LOC</li>\n</ul>\n\n<p>And even if you are not a programmer you should know that\nmillions of line of code with almost no (readable) documentation\nis no fun to code. So please wait for your OpenOffice but\ndon't be disappointed if they need more than 2-3 years to\nbreak it in smaller pieces. In the same time KOffice as well as\nthe (old?) office package are more stable and faster than\nthe star office package. IMHO the gnome developers should\nforget the OpenOffice stuff and continue coding the good old\noffice package which looks more promising than the Sun stuff.</p>\n\n<br>Happy new year, Marco"
    author: "Marco Krohn"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "Well, I've written programs for GTK+/GNOME and it was a lot of fun. The API is neither messy nor hard to understand. It would be nice if there were full C++ bindings for GNOME, but that does not imply technological inferiority.<BR>\nAlso could you tell me, what you did to make KDE2 start faster that GNOME1.2? On my machine GNOME still starts faster than KDE2. By the way a fully bonoboized GNOME app needs about 5 secs on my P233 but KOffice apps need about 20 secs. Both are tested without any GNOME or KDE libraries loaded before. So how do you see, that KDE2 starts faster than GNOME. Still I don't see, what speed's got to do with superiority.<BR>\nDo you seriously want to compare Gnumeric with KSpread, Abiword with KWord, Evolution with KMail or Dia with ? (GIMP). You would terribly loose. I haven't seen any KApplication witch appeared to be so usefull, that I needed it. However I use lots of GNOME apps every day and wouldn't like to miss them.\n<P>\nI think you must be dreaming that KOffice is a usefull tool, KDE is faster than GNOME etc.\n<P>\nJacob R. Kreutzfeld"
    author: "Jacob R. Kreutzfeld"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "Ummm... how can you compare a bonobo app to Koffice? You should state exactly what app you are referring to when you compare it to Koffice. For me (on a P233), Konqueror loads up in around 5 seconds. Loading up a new Konqi window is less than a second. Also, what koffice app took 20 seconds to load, surely you are doing something wrong there? I would say it takes about 10 seconds to get a koffice part up. But like you say, load speed is not amazingly important, although it does make people happy if their apps load in less than a minute.\n\nApart from Gnumeric and Evolution, the apps you mentioned are barely GNOME apps. I mean, Abiword? It doesn't use bonobo. All the koffice programs allow embedding. It works too. Seamlessly. Just because you don't find Koffice a good tool, doesn't mean others don't. I think Kword is useful (though buggy), Kspread is useful and getting a lot better very quickly, Kmail is not part of koffice but all the same is great mail app (Aethera (Magellan) is the one you should be comparing with Evolution), kivio is an awesome diagramming program, Kpresenter is excellent (best presentation app I have seen that's opensource), Krayon is making excellent progress etc. YOU must be dreaming that koffice is not useful."
    author: "ac"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-02
    body: "Well, I'm using the packages from unstable debian. If I start KDE first, then KOffice apps indead need about 5-10 secs to start. If I don't start KDE then it takes about 20 secs.<P>\nAbiword doesn't use bonobo jet but it will very soon. Besides that is Abiword regarded as full GNOME-app if you compile it with the GNOME-flag. The point about bonobo is, that there is no stable release jet. As soon as a stable bonobo is out you will see lots of bonobo support everywhere. Many apps already use it.<BR>\nAnyway KOffice can hardly be regarded as stable (I mean stable). KOffice apps crash left and right. Most GNOME-office apps are more stable than KOffice apps even though thereis no stable release jet.<P>\nJacob R. Kreutzfeld"
    author: "Jacob R. Kreutzfeld"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "<p>I belive that technologies like Kparts and QGtkWidget can hurt comercially, but as it is, neither KDE or GNOME/GTK are comercial.  I think it's great to have this sort of functionality, it just allows more things to be available.</p>\n<p>I primarily use GNOME, but I always keep up to date my KDE packages, and I reasearch and try out every technology I find interesting, in both KDE,and GNOME.</p>\n<p>The KDE vs. GNOME wars should be nonexistant - Having more than one enviornment is a good thing, it allows choice for a user.  And interoperability between the two only makes it better.</p>  \n<p>Both KDE and GNOME copy off each other in some areas, but that's good- if it works far better than anything else, is there a good reason NOT to copy it? I will always support both KDE and GNOME in everything they do, because choice is good.  Interoperability is better. </p>\n<p>One thing that I would like to see happen between both projects is the component technology. I think having both enviornments share the component technolgy would make things alot better for sharing code, etc.  When I heard about the rumor of KDE adopting Bonobo as their component technology, I was very excited to hear that, unfortunately it wasn't true, but I really wish it would have happened.  Bonobo makes sense for both envirnments because it is not toolkit dependant (as far as KParts, I have read that it does depend on KDE libraries-- correct me if I'm wrong), and would only be good for both.</p>\n<p>Gnome has support for KDE's \"task tray\", although not perfect, it allows a GNOME user to use a KDE application in a GNOME enviornment without trouble.  Why would a GNOME user want to run a KDE application, why not just use the GNOME equivalent?  Easy, the KDE program may work better for that specific users needs.  In my case before I found out about GNOME's rp3 dialer, I always used kppp, because it worked better than anything GNOME offered. Even today while I use rp3, there are many things I would like to see implemented in it that kppp has always had.</p>\n<p>These are my thoughts (and more) on the subject. Please don't turn this into another KDE vs. GNOME war, as this was not my intention.  I only wanted to explain why interoperability is a good thing between KDE and GNOME</p>\n<p>-Brandon</p>"
    author: "Brandon"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "I strongly disagree, because it is a one-way street.  While the developement of XParts and other methods to allow \"foreign\" embedding is a strength of Kde, there is no evidence that Gnome has any interest whatsoever of providing ways to embed kde components in Gnome.  Even if Kde had adopted Corba instead of Kparts, Gnome would make interoperability more difficult than it need be.\n\nGnome most certainly is a commercial project.  Helix, RedHat, and Eazel are entirely commercial, regardless of GPL.  Kde has some commercial influence, but has a good mix of commercial and non-commercial participants at this time. In Miguel's latest interview, he proclaimed that \"volunteers\" can't handle a major project like Gnome's Evolution and that he prefers commercial management to the use of volunteers in Gnome development and promotion. \n\nStill, with GPL, we can use code and concepts from Gnome and Gtk in our own endeavours.  Recently I borrowed some really useful code from GPaint, and likewise Gnome and Gtk developers have borrowd from Kde (as in the gtk html component which was mostly copied form kthml version 1.x).  But this does not mean that the Gnome project, as a project,  has any interest whatsoever in reciprocating by making it easier to use Kde components in Gnome, aside from providing a place for the Kde menu in the Gnome Panel.\n\nXParts is useful for a lot more than embedding Gnome components.  Much also remains to be explored with Java and Kde, for example. \n\nAfter time and time again giving the Gnome the benefit of the doubt, I've conculued that commercial interests (to recover the investment of time and capital in ventures like Helix and RedHat Labs and Eazel) totally drive the Gnome project. Kde is more laid back, and more free to experiment and explore.  Kde's primary commercial benefactor, Trolltech, is now well established and probably relies less on venture captial and more on a positive flow of revenue. I really do feel that the Gnome organization regards Kde, not MS, as its primary competitor, and that the Gnome organization regards it as a life or death struggle.  \n\nThis doesn't mean that efforts like XParts are in vain.  They will prove very useful in the long run.  But Gnome will cooperate only when it has no other alternative.  This seems unlikely so long as Gnome and related commercial endeavours spinning off of Gnome which really direct Gnome's development like the tail wagging the dog remain on life support from major players like Sun, IBM and AOL."
    author: "John Califf"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "A Gnome equalivant of QGtkWidget can't be done, because Gnome is written in C.\nBut Gnome's Bonobo architecture is capable of embedding KParts component, and it will happen in the future.\n\nGnome is *NOT* a commercial project!\nWhile HelixCode and Eazel are commercial, they produce Free/open source software.\nAnd if you still claim that Gnome is a commercial project, how does that make KDE not commercial too?\nQT is created by TrollTech and KWord and KDEStudio by theKompany, both are commercial companies.\nAnd who cares if it's commercial or not?\nThe people working at those companies are humans too, aren't they?\n\n\n\"Kde's primary commercial benefactor, Trolltech, is now well established and probably relies less on venture captial and more on a positive flow of revenue.\"\n\nAnd what makes you think HelixCode, Eazel and RedHat aren't?\nYou are just trying to spread FUD, that's why.\n\n\n\"Gnome organization regards Kde, not MS, as its primary competitor, and that the Gnome organization regards it as a life or death struggle.\"\n\nYeah right. You keep beleving that kind of things, I won't stop you.\n\n\n\"But Gnome will cooperate only when it has no other alternative.\"\n\nWhat about Mozilla? Gnome can use GtkHTML too you know."
    author: "Anonymous"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "Well, Gnome isn\u00b4t a commercial one but HelixCode is ;-) and in opposition there\u00b4s no KDE \"for the poor\" and a KDE \"for those who pay\", here there\u00b4s only one KDE and that is free and not developed by a company.\n<p>\nAs far as theKompany goes, KWord ins\u00b4t developed by them (AFAIK) and KDEStudio is not an official part of the KDE distribution while KDevelop is.\n<p>\nNow, when it comes to the commercial extend, I do care. But there is a difference for me in a commercial desktop environment providing everything for daily work and a desktop environment that does the same but is completetly independent. \n<p>\nThird party apps that are written to earn money nowadays have to support one or the other desktop to integrate smoother into the user\u00b4s envirnoment and *both*, KDE and GNOME allow commercial development of applications and they will arise sooner or later although we try to provide something free for all needs.\n<p>\nHappy new year,\n<p>\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "The software stays GPL'ed.\nSo anyone can grab Nautilus/Evolution/RedCarpet/whatever's source code and develop it further, even when HelixCode/Eazel doesn't support them anymore."
    author: "Anonymous"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "> Well, Gnome isn\u00b4t a commercial one but HelixCode > is ;-) and in opposition there\u00b4s no KDE \"for the\n> poor\" and a KDE for those who pay\", here there\u00b4s > only one KDE and that is free and not developed\n> by a company. \n\nDude, what shit are you smoking?\nHelixGNOME is free.\nIf you want to pay you can have it sent to you on a CD. If you don't want to pay, you download it, just like anything else. All developments made to Helix GNOME are put into the main GNOME source tree if they make sense (like it doesn't make sense to put the logos into the source tree.)\n\nStop talking crap about stuff you don't know."
    author: "Me"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-02
    body: "I would like to clarify.  theKompany did not write KWord and doesn't sell KWord, we simply made a committment to help support 2 developers working on KWord for 2 years as part of our committment to KDE.  \n\nKDEStudio is a great tool, we are have been responsible for it for almost a year now, but it is not a commercial product, it is strictly a free open source project.\n\nAnd yes, we are human :)"
    author: "Shawn Gordon"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-03
    body: "While it is true that the various GNOME companies do not have an interest in playing nicely with KDE, there is nothing stopping J. Random KDE or GNOME Developer from writing a compatability module. That's the whole point of free software, remember?"
    author: "Anonymous Coward"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-03
    body: "Who would want to use GNOME with KDE2 out?  A retard?"
    author: "I am a fuckin' commie!"
  - subject: "Java applets run on even more systems"
    date: 2001-01-01
    body: "If the number of supported systems for an embeddable component is the only concern, a Java applet would be the ideal solution. Not only KDE and GNOME support it, but also plain X11, Windows, Mac etc. Ofcourse there are also downsides like the overhead of the virtual machine (especially startup time and memory consumption) and a widget toolkit that is either limited (AWT) or not responsive enough (Swing).\n\nMy point is that the reasons for developing embeddable components for KDE are the same reasons for developing for KDE in general. Many developers like the way KDE and Qt work and will prefer to write KDE components even if it means their components won't run under GNOME (yet?) without porting."
    author: "Maarten ter Huurne"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "My point was never to say that we should stop this kind of development.  However as a suggestion as an intelligent use of resources was my primary intent. (Really need to get more sleep)\n<p>\nThe real issue, in my mind, as others have stated is the fact that this is Open Source product.  The other developments such as XParts & QGtkWidget are fantastic.  However until GNOME and KDE decide willingly to \"get along\", exterting energy by the core KDE team to work on this cooperation is a major waste of time, resources and skills in my book.  \n<p>  \n(Because I mention GNOME here this is not a flame fest against GNOME.  I use both GNOME & KDE on a regular basis.)\nThe other point is everyone kept mentioning that this is Open Source, not commerical.   Actually it is now.  With the GNOME Foundation, including Sun and 1/2 a dozen other commerical companies, GNOME does have significant amount of commercial intrests at heart now.  Granted KDE has commercial intrests also.  But from an Engineering & Political standpoint I see it very difficult to justify making something such as these two tools part of the core development.  <p>\nFor this kind of effort to succeed there needs to be a political agreement between the two core development teams.  A standard of some kind has to be agreed upon, be that official standard or verbal standard.  Without this, the development done can be \"contaiminated\" and \"corrupted\" by either core team.  What I mean by this is on version 2.1.1.1 KDE you can embed Gtk Widgets.   However Gtk v 1.2.3 comes out two minutes later.  Now KDE which claims to be able to embed and use GTK widgets suddenly can't with all new versions that use the latest greatest GTK widget.  I as dumb user comes along and tries to use program that claims KDE compatibility but it uses the latest GTK widget.  I dump KDE (not knowing any better) and go to GNOME.  However in my case GNOME is too complicated and I decide to dump Linux all together.  This is not what anyone wants.  <p>\nOkay.. off my little horse.  I've dealt with this issue on other developments in my history.  Everytime its been the same thing.  If there isn't a political agreement between the two core teams doing that kind of cross development is doomed to various forms of failure.  However <b> third parties can succeed</b>.  That is my point.  Let the third parties do what they want.  Just don't make it something that the core development team needs to concern themselves with right now."
    author: "iguy"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "Oh no, not more Gnome Foundation FUD...\n\nGNOME IS *NOT* COMMERCIAL!\nTHE GNOME FOUNDATION IS *NOT* COMMERCIAL!\nTHOSE COMANIES HAVE NO POWER OVER THE FOUNDATION AT ALL!\n\nAnd if you don't believe me, read the Gnome Foundation FAQ:\nhttp://www.gnome.org/faqs/gnome-foundation-faq/"
    author: "Anonymous"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-01
    body: "Gnome trying to break KDE's interoperability?\nThat'll never happen!"
    author: "TrollKiller"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "It depends on how you look at it.  In my mind, you shouldn't be asking whether or not it hurts KDE, but rather, whether or not it hurts the KDE user.  Sure, you could easily \"help\" KDE by making KDE apps work in GNOME, but not vice versa, although I think that would only hurt KDE.  By doing that, people would be more obligated to use GNOME because it runs everything (most everything), while KDE is still only holding 50%.  I think what KDE needs to do is worry more about trying to support other stuff, like GTK, than trying to become more popular.  With advanced GTK support built-in to KDE, you could easily swing the tides.  The goal should not be to bring developers to KDE, but to bring KDE to developers."
    author: "dingodonkey"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "I believe that this is a good thing.  how could giving your project more functionality hurt it? on a personal dirty end user level I feel more inclined to use KDE because they are trying to give the users all the funtionality that they can pack into the desktop.  KDE is an impressive Desktop and doing this will just expand it's appeal to a broader audience, which in turn will cause developers to program with the K desktop in mind."
    author: "L.D."
  - subject: "oh no!"
    date: 2001-01-01
    body: "Looks like this is starting to be a flamewar about Gnome again.\nEverybody is whining about how KDE/QT is superior and how commercial and bad Gnome is (while it is not).\nGet a life will ya! You don't even know what's really going on!"
    author: "Anonymous"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "It may hurt commercially, but who cares?\nKDE will live on, and so will Gnome!\nWe aren't power-hungry marketing-weenies, trying to pull as much users/developers to our side, are we?\nBoth Gnome and KDE's goals are to create the best desktop and development environment."
    author: "Anonymous"
  - subject: "NO SLASHDOT!!!!!!"
    date: 2001-01-01
    body: "PLEEEAAAASE don't Slashdot this article!!!!\nThere's already enough flamewars here and I don't want more!\nEspecially because /. is full of *ignorant* trolls!"
    author: "Anonymous"
  - subject: "Re: NO SLASHDOT!!!!!!"
    date: 2001-01-01
    body: "Please behave yourself too ;-)"
    author: "GlowStars"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "I'm surprised (and kind of disappointed) that dot.kde.org picked up this article. Anyway...<BR>\n\nPersonally I think it's good for KDE. But that's not really the point. This is Open Source, ladies and gentlemens. If we take QGtkWidget for example: There wasn't some KDE focus group somewhere that sat down and discussed \"hmm, KDE really needs the possibility to include gtk widgets\". No, a guy (in this case bero at redhat) wanted to create QGtkWidget for whatever reason, and so he did. Fine. That's the nature of Open Source -- if you want to create something for it, feel free."
    author: "Matt"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-02
    body: "Amen to that."
    author: "caatje"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "I believe it's good to have interoperability.\nUsers should be free about which desktop to\nuse and programmers should be free about which\ndesktop architecture they prefer to program for.\n\nPersonally, I think most opensource developers\n\"who do it for the hobby\" will never choose to\ndevelop on an architecture that they don't feel\ncomfortable with or isn't their taste. So if a\ndeveloper decides to program primarily for KDE\nor primarily for Gnome, you can't stop him / her.\n\nWhat you can do for the users is to make Gnome\nprograms run well on KDE and make KDE programs\nrun well on Gnome, so the users have more good\napplications to choose from on both desktops.\n\nBest regards,\n\nEric"
    author: "Eric Veltman"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "Since most Gnome and KDE apps are still developed by hackers in their sparetime, XParts and QGtkWidget wont hurt KDE, as nothing in the hackers eyes have changed. The reason why people code for KDE is because they like it. They don't care about market share and all that crap. Coding for Qt/KDE is still easier and more fun than coding for Gtk/Gnome and that's what matters.\n\nAll in all this mostly good news for KDE not for Gnome. This gives more options and possibilities for Qt/KDE developers as well as KDE users."
    author: "Erik Engheim"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "\"Do Interoperability Technologies Help or Hurt KDE Development?\"\n    It HELPS, of course!\n\nThis is in fact what we can consider as a MAJOR FEATURE of KDE environment.\n\nJust have a look at XPart example: this does not only mean your are able to play with the HTML\nengine your want. This means without recompiling Konqui, you are able to upgrade its web browser\nfunctionaltity.\nSo what? So if KHTML had a deep problem (let's say is no more upgraded, or have a\ndeep architectural defficiency), Konqui (and all KDE apps using KHTML) are not dead anymore because\nit's easy for them to replace KHTML by any other Gnome component.\n\nMore globaly, this means Interoperability Technologies is very important for KDE, not from a\ncomponent but more from an application point of view. Those who are developping applications would\nfind KDE a better environment because if needed, it would be possible for them to easily use,\nreplace or upgrade each component they use by any other better KDE or Gnome component offering the\nsame functionnality.\n\n    Happy new year,\n\n        Nicolas"
    author: "Nicolas"
  - subject: "OS2/windows"
    date: 2001-01-01
    body: "The same problem happened about OS2 wich was able to use windows 3.1 programs, resulting in more programs and users for windows due to the fact that OS2 users would anyway be able to use windows user's work"
    author: "renaud"
  - subject: "Re: OS2/windows"
    date: 2001-01-01
    body: "That is completely different! This is the open source world!\nKDE apps runs fine in Gnome and vice versa.\nWe're talking about some widgets here.\n\nFreeBSD is able to run Linux binaries.\nThen why isn't FreeBSD dead yet?\nThink about it."
    author: "ac"
  - subject: "Re: OS2/windows"
    date: 2001-01-04
    body: "The real problem about OS2 was its lack of win95-compatibility. We had to use the new versions of the standard-software-packages (like Corel Draw and Office). Many users prefered the 0S/2-Desktop, but they were forced to use Windows95 because of OS/2s missing compatibility."
    author: "wmueskens"
  - subject: "Stop This HATRED!"
    date: 2001-01-01
    body: "This is really stupid of you to have hatred toward other programmers work! How could you people be so cruel? Instead of helping each other we tend to fight...why? \n\nRemember \"Cooperate We Survive, Fight we Die!\".\n\nHatred is not the Answer..."
    author: "Asif Ali Rizwaan"
  - subject: "PR Gag"
    date: 2001-01-01
    body: "Hey Folks, do you relly think that this PR Gag about Bonobo-components used in KDE-apps has some relevance?<BR>\nDoes anybody know which features can be used at all? How do you thing Bonobo-Components can be used in KDE-apps at all.<P>\nHow would you for instance use an evolution-calendar component in a KDE app which doesn't know about the evolution-API? Or a red carped-component?<BR>\nDoes anybody of you have the slightest idea of what componentisation is about and how it is used? How can you think that GNOME-components can really be used in a KDE-app?<P>\nI think you should all go home and do your homework. Read some bonobo-code etc... Making a quick hack to embed a bonobo-component indo a KDE app and use it as PR is one thing. Actually use a bonobo-component is another.<P>\nWake up!!!!!<P>\nJacob R. Kreutzfeld"
    author: "Jacob R. Kreutzfeld"
  - subject: "Re: PR Gag"
    date: 2001-01-01
    body: "They wrap a Bonobo component using an XPart \"component\" with it's own API."
    author: "TrollKiller"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-01
    body: "I am really tired of this media attitude that GNOME and KDE are competing! Listen up : KDE AND GNOME DO NOT COMPETE! The whole concept of trying to get people to use KDE instead of GNOME is stupid in this sort of system. Why? Because we have the same goal! Our goal is : \"Make Open Source Software userfriendly and powerful\". The only reason there are 2 projects in the first place is partially because of liscensing problems, but  mostly because of programmer preference! Interoperability technologies help because they encourage more programmers and users to use Open Source! Also, these sort of interoperability technologies might eventually lead to a project merge, which would be great for everyone! Great for the users because now they have twice the developers working hard on their software, and great for the developers because they can use either of 2 APIs and still have a result thats compatible with developers who prefer the other API! I fail to see any sort of downside here, other then hightened memory and CPU requirements."
    author: "Carbon"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-02
    body: "GNOME is a comercial desktop, even though it's \nunder the GPL, the main GNOME mantainer own's \na company that sells GNOME.  He's more interested\nin money than GNOME, not only that, GNOME is\ninferior and slow, the only reason people use\nit is because of RedHat, they want people to \nuse GNOME so that they don't know how to use \nKDE, and will continue to buy RedHat with GNOME.  This is why so many big companies chose GNOME, because they know they can influnce development with money.  \nIt wouldn't suprise me if in 2 years GNOME\nonly runs on RedHat and Solaris.  This would kill\nlinux also - GNOME is a piece of crap is completely unusable and is horribly designed, probably beyond repair.  RedHat and Solaris dont care because they're pushing for server market share.  I wouldn't be suprised if RedHat chose GNOME to push linux off of the desktop and get more opensource developers working on server aspects of linux.  This won't just hurt linux it'll eventaully kill it off entirely.  Linux \nis about free software, and the GNOME people are trying to force people to buy it.  The only way\nto get people to use linux is to kill GNOME off entirely, it's what's kept it unusable for so long.  This is KDE vrs GNOME because GNOME will eventaully destroy linux if noone does anything.  RedHat and the other companies don't care as long as they can get a few $s out of it."
    author: "_"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-02
    body: "Dang! Seeing posts like this on /. is rare. But you, you are really funny! I don't know what you smoked, but you did make me laugh! Thanks!"
    author: ""
  - subject: "GNOME companies totally commited to free software"
    date: 2001-01-02
    body: "Unlike the companies involved with KDE, the main GNOME companies are committed to only releasing free software. It was only the growing success of GNOME that forced Trolltech to first open source the unix version of QT, and then to GPL it. The Kompany, Caldera, etc sell proprietary software. Not that there is anything necessarily wrong with this, but I find the attitude of the GNOME companies much more admirable (and certainly much more advantageous to the free software movement.) In fact, the GNOME project as a whole seems much more idealistic than that of KDE. Certainly, in the past, they've taken a stand on principles, rather than doing that which is merely convenient. This seems to be a consistent differance between the two projects."
    author: "Nexus"
  - subject: "Re: GNOME companies totally commited to free software"
    date: 2001-01-02
    body: "Have you looked at the business model that Helixcode and Eazel have?  I have, and I find it amazing that at least Helixcode is projecting $120 Million in revenue per year starting this year based on subscription services.  Advertising has to be part of this model, how many people have to be paying them $5 per month to make that kind of money?  At least with my company we are doing valuable open source work that everyone can take advantage of, we have a model for making money that we are clear about and we do our best to work with the community to make sure that what we are doing is acceptable.  What piece of proprietary software do we, or Caldera sell?  As far as I know Caldera doesn't, and neither do we.  I love people talking about theKompany, but I do wish you would have your facts straight.  If you have a question about what we are doing, you are always free to ask, I respond to every piece of email I receive."
    author: "Shawn Gordon"
  - subject: "Re: GNOME companies totally commited to free software"
    date: 2001-01-03
    body: "Caldera recently bought out SCO's unix business. This is what they said about why they wouldn't open source it:<BR> <I>\"Ownership [of code] is not a bad thing; it's actually a good thing,\" said Love. \"It protects [code] quality.\"</I><BR> \nCompare this with Red Hat's acquisitions.(Cygnus etc)<BR> <I> \"Every line of code that we write, we put back as open-source software,\" McNamara (general manager of Red Hat's enterprise business unit) said. \"We believe the operating system should be part of the public infrastructure and not the account of one company.\"</I><BR> As for theKompany, you are keeping most of the stencils for Kivio proprietary, and from your past criticism of the Eazel/Helix business model I assumed you felt it necessary to keep some things proprietry to be successful. My comments weren't, however, intended as a criticism of theKompany, merely as a rebuttal of the absurd statement that the GNOME people were only in it for the money.\nThe business model used by Helix/Eazel is still unproven; it may not be successful, though I hope it will be. Still, it's a noble effort. <P> Of course, as a company heavily involved in free software, I wish you success, even if one day you may have to change the fourth letter of your company's name.  ;)"
    author: "Nexus"
  - subject: "Hello troll!"
    date: 2001-01-02
    body: "Just came back from Slashdot?\n\nAnd how do you know Miguel is more interested in making money?\nDo you know him personally?"
    author: "TrollKiller"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-02
    body: "Boihh, how does it feel to be controlled by paranoia? I knew that smoking crack is bad for you mental state, but I didn't expect to be that terrible. <BR>\nPoor guy, I'm affraid there is no help for you anymore, so go and bang your head.<P>\nJacob R. Kreutzfeld"
    author: "Jacob R. Kreutzfeld"
  - subject: "Re: Do Interoperability Technologies Help or Hurt"
    date: 2001-01-12
    body: "But what about Motif. It\u00b4s possible to use this on most linux computers. Perhaps that is the best alternative.\nI have just downloaded it myself and will soon find out if it\u00b4s good for the average homeuser.\n\nregards!"
    author: "roger"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-02
    body: "On on Sunday December 31, @11:46AM, Dre asked:\n<p>\n> In a recent story at Kuro5hin.org, the author considers the implications of some recent<br>\n> developments announced on the dot -- namely, XParts and QGtkWidget. These projects<br>\n> permit non-KDE programs -- particularly Gtk applications -- to be used within KDE<br>\n> applications. The author opines that these projects will end up hurting KDE development.<br>\n<p>\nIn my opinion the only way this <i>would</i> hurt KDE development <i>if</i> it included the need to make the code structure worse - something I cannot see at all.<p>\nThe only way to harm the KDE project is making KDE worse, <b>not</b> making KDE even more open-minded and able to cooperate with other projects.<p>\n> In particular, the author wonders why a Linux developer would now develop for KDE when they<br>\n> can write for Gtk and have the application work both under KDE and GNOME.<br>\n<p>\nShouldn't this be the decision of the developer herself/himself?<p>\nThere is no way to prevent a developer from choosing FLTK or GNOME or whatever they like - and I am glad about this!<p>\nAnybody contributing to Free Software should have the right to choose his/her favorite toolkit/desktop environment/window manager... right?<p>\nIf the chance to use Bonobo components from insite KDE results in a developer deciding to code Bonobo components now, what it wrong then?<p>\nThere is an increasing number of some hundreds <b>HIGHLY</b> motivated developers loving Qt and KDE - continuously contributing many good ideas and a lot of very good code.<br>\nI see no desperate need to prevent anybody from prefering another project: KDE is by far the #1 desktop today and continues to find skilled friends throughout the world, more and more and more.<br>\nNo need to handcuff somebody who might be attracted by 'the others'.<p>\n\n> What do you think, will these projects encourage Gtk development at the expense of KDE/Qt development?<br>\n<p>\nPerhaps a few people, perhaps. Perhaps nobody at all, perhaps...<p>\nPlease consider that for somebody being attracted by the idea of converting from Qt and KDE development to GNOME development there must be meet <b>one very important requirement:</b><p>\nA skilled software developer would only decide to start GTK development and forget about Qt in case she or her is convinced to like GTK more than Qt.<p>\n:-D<p>\nConsidering this simple question those of you <u>knowing both</u> toolkits (as I do) will surely admit there really is no need for any handcuffs. ;)<p>\n\n\n> Even if so, does it nevertheless make KDE stronger as a competitor against proprietary<br>\n> desktops?<br>\n<p>\n\nKDE is that much appreciated because of its superior quality, its many skilled contributors, its great developer-documentation, easy to use toolkit...<p>\nBeing more open will be another advantage for KDE: since there is no magic in the way Bonobo components can be plugged now. Everything is open and IMO very fair. This cannot be compared to idiotic ways a 'pseudo marked-leader' in the proprietary software world would act - nothing about hiding interfaces or surpressing open standards - the contrary is true: KDEs new ability supports interoperation at its best and both the GNOME and the KDE team should be happy about this.<p>\nKarl-Heinz &nbsp; <a href=\"http://home.snafu.de/khz\">Keep coding...</a><br>"
    author: "Karl-Heinz Zimmer"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-02
    body: "Hi there,\n\nI think interoperability is one step in the right direction. I really dislike the gnome/KDE flame wars. Personally I believe that the two projects joining their powers and developing together the *perfect* environment would be best but I guess the barriers for that are to big. Either side wouldn't see enough of their code and development in the combined thing. Plus competition might not be that bad, since both sides put a lot of effort in the development to try to be better than the other. \n\nPersonally I prefere KDE2 over gnome. But I guess that's just because when I compared the two gnome was in a very early stage and just looking like a childrens toy with huge icons. This is not the case anymore I know but I got used to KDE and thats the main point. It's the same with distributions. In my early days I tried Slackware, Debian, DLD, Suse and Redhat not in that particular order but RedHat was the last and because distributions got better every increase in version number it would be unfair to compare an older distribution with another newer. Since some years I only use RedHat because I got to know their setup and got used to it. I think that's all there is. You get used to something get to like it, get to know it and then you just don't want to change. \n\nI admit, I've never written a line of GTK/Gnome code but I started to develop with QT and KDE just because it's KDE I'm using. Both QT and KDE-Libs are easy to learn and extremely powerfull. In addition to that they are very good looking (my oppinion). \n\nInteroperability would bring more apps to each of the both sides as long as either side would provide such a support. This can only be a good thing. Not perhaps for one side or the other but for Linux as a whole. People will have the ability to choose and get all the apps of both sides. Developpers can chose the environment they like and provide apps for both sides. Since I believe Linux will never be theoperating system for the dumb user who isn't interested in what's going on within the system you will always have a userbase that is at least a little skilled and at least as skilled enough to be able to use both app-types at the same time. And if you had let's say drag and drop or other things working cross environment wouldn't this be just very nice?\n\nSo putting it all in one phrase, I'd say go interoperability!\n\nGreetings and happy new year,\n\nRichard"
    author: "Richard Stevens"
  - subject: "We use computers for applications first."
    date: 2001-01-02
    body: "Use the better KDE GUI and run that Gnome application you must use.\n\nWouldn't you love to use Linux, but run Microsoft Word or Excel ??? Same thing."
    author: "KDE2 Man"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-03
    body: "...or use the better Gnome GUI and run KDE programs you must use."
    author: "ac"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-03
    body: "ac wrote:\n<p>\n> ...or use the better Gnome GUI and run KDE programs you must use.\n<p>\nDear aonymous, I got an even better idea:\n<p>\n<i>Why not send all trolls to jail and continue coding in peace?</i>\n<p>\nKarl-Heinz"
    author: "Karl-Heinz Zimmer"
  - subject: "Do Interoperability Tech.s Help or Hurt KDE use?"
    date: 2001-01-03
    body: "Do Interoperability Technologies Help or Hurt KDE use? would be a much better question, with a simpler answer: They help!\n\nCome on people, what do you want? To rule the world? Yeah, that would be nice, but I just want good apps as a user and a possibilities as a developer and interoperability just helps on both counts.\n\nInteroperability is a Good Thing."
    author: "Luis Coelho"
  - subject: "World domination?"
    date: 2001-01-04
    body: "Exactly!\nI don't even think, that it would be \"nice\" to rule the world. Who needs this? The developers, to be proud? This could be the case for some individuals. But from a users POV? No...\nWhy do i care about what other people are using??\nI want the best thing for ME. I used to love KDE but recently i switched to IceWM and fall in love with it. Than i had to realise, how stupid it is to use Gnome or KDE apps without having one of the desktops running. So i avoided them as much as possible. Why should i use kcalc, if i can use xcalc? Well, kcalc looks better, but wouldn't a Qt calc or a Gtk calc look better two? I can see that newbies could like to have all their apps look the same way, that's why i think it is a good idea to make KDE Frontends for apps. But i don't like that a lot of KDE applications are reinventing the wheel, just to have something less functional, bigger, slower, desktop dependent.\nThat's why i support anything that's not Gnome nor KDE dependent. Qt is a great Toolkit, so why are there almost no Qt apps but a whole bunch of KDE apps? I honestly think that recreating all important apps for a desktop environment is the wrong way. They should rather try to cover existing applications. Imagine a simple Qt calculator. If this Qt calculator would now use a Qt RC file to set a unique theme for all Qt applications (this could be configurable in kontrol-center), what would be the disadvantage of this calculator to the KDE calculator?\nOf course, there are a lot of cases where the KDE libraries are usefull (KOffice for example).\nSo better embedding of other applications into the KDE desktop would be a great thing. I would be glad if everyone would help to make alle applications as independent as possible, so that we can remain the power of choice.\nI still feel bad about kparts and bonobo. Wasn't it possible to stick together and write something desktop independent?\nAnother thing is the KDE system tray. It makes KDE applications feel really bad in other windowmanagers if they use the system tray! But this is not only KDE's fault, others are doing the same (gnome...). I can't understand it. Of course it's more trouble to create something compatible, a kind of standard, so that application programmers can support ANY system tray, not only the one of KDE or gnome, but wouldn't it be worse it?\nThe KDE2 system tray isn't even compatible with KDE1 apps. D'OH!\n\nThis wasn't intended to be a flame, i just hope that some people will keep this in mind when they develop new concepts and that hopefully everyone stays away from thoughts like \"how can we force the people to use our software\" as the parent article implied...\n\nSpark\n\n\nDisclaimer: I feel that my english wasn't the best and i had to guess a few times. I hope everything was understandable. Well.. and i always forgot to write capital I instead of i. ;)"
    author: "Spark"
  - subject: "Re: World domination?"
    date: 2001-01-04
    body: "Wow! You're the 1st person who consider KDE apps \"bloat\"!"
    author: "ac"
  - subject: "Re: World domination?"
    date: 2001-01-04
    body: "But it's true. Especially if you don't use the KDE Environment. They are very well designed and most of them make sense. But the KDE libs are still bloat for a calculator. ;) Qt itself does almost the same, but without the overhead."
    author: "Spark"
  - subject: "Re: World domination?"
    date: 2005-07-15
    body: "Believe it or not, but I also switch from KDE to IceWM. For some reason I couldn't get KDE to compile, IceWM on the other hand was up and running in no time. Also, for the very seem reason as mention above, I don't use anything that is desktop depenent, it just doesn't look as good without the desktop environment."
    author: "Dave Kok"
  - subject: "Re: World domination?"
    date: 2005-07-16
    body: "I hope you are aware that 1) KDE is a *D*esktop *E*nvironment while IceWM is a *W*indow *M*anager (KDE's window manager is Kwin) and 2) you are replying to a four years old flaimbait."
    author: "ac"
  - subject: "I was under the impression that..."
    date: 2001-01-04
    body: ".. KDE doesn't really want to crush Gnome, but take market share from Windows/Mac.\nWhat is the point of fighting over a 3-5% market share, when you can go after the other 95-97%?\n\nBoth KDE and Gnome should have the policy of helping interoperability(both ways).\nTo be frank, I use both KDE and Gnome, and I really don't care which ends up bigger, as long as they together have a big impact in the market. \n\nIf KDEs goal was to crush Gnome, I would stop using KDE.. fortunately I think both camps think bigger (and more noble).\n\nA merging of the two projects are close to impossible, but interoperability is possible, and should not be hurt by fear of loosing the battle."
    author: "Gaute Lindkvist"
  - subject: "KDE vs. GNOME?!"
    date: 2001-01-04
    body: "Why does this war exists? I'm tired of listenning KDE fans saying <a href=http://www.kde.org>KDE</a>/<a href=http://www.trolltech.com/>Qt</a> programming is easier, KDE2 is faster, Konqueror is the best browser for Linux, ... while on the other side GNOME fans say <a href=http://www.gtk.org>GTK</a>/<a href=http://www.gnome.org>Gnome</a>/ libs are more powerfull, GNOME is faster, Nautilus and Evolution are better and so on. I think programs from one side sould run on the other side so that when people could benefit from both projects. The ideal was that both projects could merge into one and end this wasting of resources in competition between them after all both share the same enemy M$"
    author: "Paulo Jorge"
  - subject: "What about childish us and them debates?"
    date: 2001-01-05
    body: "I think this thread does more damage.\n\nFace it, kde isn't something you can or should personify.\n\nIt doesn't have opinions, it can't be harmed.\n\nIts nothing \"official\" nor important.\n\nLike gnome it is just a pile of source code.\n\nOn the other hand, the people using, developing, promoting and debating it are (on the whole) the ones who have opinions and who may consider themselves affected by what happens to kde.\n\nPerhaps the question they really meant is \"will it harm me?\" because it certainly won't harm kde whether you embed gtk or not. Maybe you should question  their fears?"
    author: "Michael"
  - subject: "Re: Do Interoperability Technologies Help or Hurt KDE Developmen"
    date: 2001-01-08
    body: "The replies to this can be simply summarized: way too many people have their political, ethical, or other senses bothered by the fact that somebody sat down and instead of doing mouthwork did codework.\n\nCreativity is about being free to express yourself.\n\nOpen Source and Free Software is about being free to use whatever you want and need to accomplish your task.\n\nFreedom of Speech is about being free to say whatever hell you want to.\n\nCommon Sense is about using the freedom productively. Get a cold shower, pals.\n\nIf discussions like this will be dominated by the people who don't have a clue what they're talking about, prospective open-source developers will eventually get scared off... I'm starting to get scared-off, as nobody knows what kind of the war the code I'll develop may start."
    author: "Kuba Ober"
---
In a <A HREF="http://www.kuro5hin.org/?op=displaystory;sid=2000/12/26/01728/315">recent story</A> at <A HREF="http://www.kuro5hin.org/">Kuro5hin.org</A>, the author considers the implications of some recent developments announced on the dot -- namely, <A HREF="http://dot.kde.org/977406679/">XParts</A> and <A HREF="http://dot.kde.org/977615897/">QGtkWidget</A>.  These projects permit non-KDE programs -- particularly <A HREF="http://www.gtk.org/">Gtk</A> applications -- to be used within KDE applications.  The author opines that these projects will end up hurting KDE development.  In particular, the author wonders why a Linux developer would now develop for KDE when they can write for Gtk and have the application work both under KDE and GNOME.  What do you think, will these projects encourage Gtk development at the expense of KDE/Qt development?  Even if so, does it nevertheless make KDE stronger as a competitor against proprietary desktops?  Is it important for KDE developers to create projects that would make KDE apps work inside GNOME apps (such as a KParts container that works with Bonobo) or with Gtk widgets (essentially a GtkQtWidget)?



<!--break-->
