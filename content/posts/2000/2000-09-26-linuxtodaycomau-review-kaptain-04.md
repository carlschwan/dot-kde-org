---
title: "LinuxToday.com.au: Review of Kaptain 0.4"
date:    2000-09-26
authors:
  - "numanee"
slug:    linuxtodaycomau-review-kaptain-04
comments:
  - subject: "Re: LinuxToday.com.au: Review of Kaptain 0.4"
    date: 2000-09-26
    body: "Could this App act as a SETUP tool????\nI think on a OpenSource Setup-script - hosted on SourceForge....\n\nPossible????"
    author: "Mike"
  - subject: "Re: LinuxToday.com.au: Review of Kaptain 0.4"
    date: 2000-09-26
    body: "Shouldn't this be using XML?  Doesn't KDE already have some XML -> GUI rendering capability that could be leveraged?  (or even XUL...)"
    author: "Matt Agler"
  - subject: "Re: LinuxToday.com.au: Review of Kaptain 0.4"
    date: 2000-09-27
    body: "Good question although XML isn't the solution to everything and I don't think XMLGUI does all of this either.  I think XMLGUI is only for toolbar and menu merging. What might be done is a dynamic parser for Troll's .ui dialog file format.  That's XML if I'm not mistaken."
    author: "KDE User"
  - subject: "Re: LinuxToday.com.au: Review of Kaptain 0.4"
    date: 2000-09-27
    body: "If all of KDE's windows and dialogs were using XML, there would be some kind of standard, which would definitely be preferable. But since this is by far not the case right now, it only makes little sense to use XML.\n\nAn advantage of XML is that the interface can be 'drawn' with special tools instead of just entering it as ascii in a config file and that the use of gimmicks like a bitmap image is easier. I think it's worth the required effort, but I don't it's required for a first version"
    author: "Martijn Klingens"
  - subject: "Re: LinuxToday.com.au: Review of Kaptain 0.4"
    date: 2000-09-27
    body: "What they oughta do is make kaptain be able to save a set of defaults (maybe it does, haven;t played with it yet..), and the you can run a command-line with a filename like:\n\nkaptain tar.kaptn kernel.tar.gz\n\nand it will use the defaults applied to that file.. Then we can can mime types that act on those files..."
    author: "garion"
---
<a href="http://www.linuxtoday.com.au/">LinuxToday.com.au </a> is running a <a href="http://www.linuxtoday.com.au/r/article/jsp/sid/131347">review</a> of <ahttp://www.hszk.bme.hu/~tz124/kaptain/ href="http://www.hszk.bme.hu/~tz124/kaptain/">Kaptain</a>, the universal graphical front-end we <a href="http://dot.kde.org/969795302/">mentioned</a> a few articles back. They like it a lot.  <i>"Sound ground breaking? It sure is. Kaptain uses .kaptn files, which are simple text scripts edited in any way you choose. The script 'describes the possible arguments for a command line program'. Kaptain then interprets this script, and puts up into your X-windows display a simple Qt-based window with all of your options detailed."</i>  Kaptain can be built with KDE2 support as well.

<!--break-->
