---
title: "KDE2 to be included in Debian GNU/Linux"
date:    2000-09-07
authors:
  - "raphinou"
slug:    kde2-be-included-debian-gnulinux
---
In an <a href="http://www.linuxplanet.com/linuxplanet/opinions/2281/1/">article </a>posted on <a href="http://www.linuxplanet.com">LinuxPlanet</a>, Wichert Ackerman states that Debian will include KDE versions based on GPL'ed Qt. It seems everybody seems happy with Qt under GPL ;-)






<!--break-->
