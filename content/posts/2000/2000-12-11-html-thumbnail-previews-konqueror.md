---
title: "HTML Thumbnail Previews in Konqueror"
date:    2000-12-11
authors:
  - "Dre"
slug:    html-thumbnail-previews-konqueror
comments:
  - subject: "Re: HMTL Thumbnail Previews in Konqueror"
    date: 2000-12-11
    body: "Cool ! How is it comparable as the same feature in Eazel's Nautilus in GNOME ?"
    author: "Ariya Hidayat"
  - subject: "Re: HMTL Thumbnail Previews in Konqueror"
    date: 2000-12-11
    body: "&lt;stupid-questions-deserve-stupid-answers&gt;\nLike a warm frog.\n&lt;/stupid-questions-deserve-stupid-answers&gt;"
    author: "Lenny"
  - subject: "Re: HMTL Thumbnail Previews in Konqueror"
    date: 2000-12-11
    body: "This is really cool. The best would be the ability to resize individual icons in an easy way. For example, if I wanted to see just a little more of a certain document (or in this case, see a little more detail), I could put the mouse over it, hold the right( or middle ) button down, and drag. This goes for those alpha-blended previews we saw last week, too. Nautilus has something similar with it's document preview, but you have to right click, choose resize, and then resize the icon. A fast way to do it could be very helpful."
    author: "bob"
  - subject: "Re: HMTL Thumbnail Previews in Konqueror"
    date: 2000-12-12
    body: "I dont think it would be possible, because KDE makes previews in a directory (.pics for exemple).\nSince the preview is calculed previewsly, you shouldn't be able to modify it that way...\nIt s not a preview then. I don't know this\n(sorry for my english)"
    author: "gravis"
  - subject: "Re: HMTL Thumbnail Previews in Konqueror"
    date: 2000-12-11
    body: "Another thing that would be cool is if we could set text preview for all but HTML documents..."
    author: "fred"
  - subject: "Re: HMTL Thumbnail Previews in Konqueror"
    date: 2000-12-11
    body: "Text preview has already been implemented by Carsten about two weeks ago. If HTML preview is off, it also handles HTML files, otherwise it is used exactly for \"all but HTML documents...\", as long as they are some sort text files. If you enable alpha-blending, it also blends in the normal icon for the file so you can still quickly see if it's a C source, a header, just a plain text file...\nOf course you need a recent snapshot or a CVS checkout of KDE for it unless you want to wait for the upcoming 2.1 Beta."
    author: "Malte"
  - subject: "Performance hit"
    date: 2000-12-11
    body: "So how fast is this? Will the display of the previews cause a delay in useage of a folder with many .htm's, or will there be some kind of placeholder icon that gets replaced as each page is rendered so that we don't have to wait? I'd really like to try this, but I'm wondering how my K6-2 400 would handle it..."
    author: "Joe KDE"
  - subject: "Re: Performance hit"
    date: 2000-12-11
    body: "On my PII 500 it takes about 1-2 secs for an average HTML page, if a thumbnail didn't finish rendering after 5 secs, it is cancelled.\nAll this happens in the background so you can use Konqueror in the meantime of course. It surely takes some cycles so the file manager might be a little less responsive, but still it's not blocked.\nAlso, if the directory is writable, the thumbnails are cached in .pics (just as image previews are already), so next time the HTML pages need not be parsed and re-rendered again.\nHope this helps."
    author: "Malte"
  - subject: "Re: Performance hit"
    date: 2000-12-11
    body: "I know you're probably sick of all those \"how about this and that\", but too me it seems like the way Windows handles it might be a solution(not _the_ solution). Have the preview generated in its own \"view\", split the treeview in two, and have the bottom view display the preview of the GIF/HTML/C-source file when the icon is clicked/marked, with its filename as its title. This might be what many users will prefer, but of cource people with dual P-IV will have all their icons in preview-mode.\n\nJust $0.02, and it would not be so hard to implement now that preview allready exists.\n\n--\nAndreas Joseph Krogh <andreak@nettverk.no>"
    author: "Andreas Joseph Krogh"
  - subject: "Re: Performance hit"
    date: 2000-12-12
    body: "Normally in Konqueror, once you click on a file it gets displayed in your active view. I'd like a feature where the display can be in a dedicated view, so I can browse and view my files more quickly.\nThat could be a good solution, too."
    author: "Klas Kalass"
  - subject: "Dedicated view (was Re: Performance hit)"
    date: 2000-12-12
    body: "<i>Normally in Konqueror, once you click on a file it gets displayed in your active view. I'd like a feature where the display can be in a dedicated view, so I can browse and view my files more quickly.</i>\n\n<p>Here's what you do:\n<ul><li>Browse to the directory in question</li>\n<li>Split into two panes</li>\n<li>Link the two panes: <i>View->Link view</i> or click on the checkbox in the lower right-hand corner of the the new view</li>\n<li>Activate the pane which will <b>not</b> be your dedicated view (e.g. by clicking somewhere in the pane other than on an icon)</li>\n<li><i>View->Lock to current location</i></li></ul>\n\nNow when you click on a file in the active pane, it will be displayed in the other pane.  Of course, then you can't descend into directories this way unles the view is set to 'tree' mode.\n\nChris"
    author: "Chris Kuhi"
  - subject: "Re: Dedicated view (was Re: Performance hit)"
    date: 2000-12-12
    body: "<p>I agree with Klas, I really miss the opportunity to dedicate a view for previews.\n<p> When for example programming, I often want to view source files for reference, and going through the trouble of unlocking/moving/locking each time I change the dir is cumbersome, and so is finding my way in a large directory structure in tree mode.\n<p>SO if anyone have an idear of how to implement the feature, hurrae!\n<p>-anders"
    author: "Anders Lund"
  - subject: "Re: Dedicated view (was Re: Performance hit)"
    date: 2000-12-13
    body: "Thanks a lot for the trick.\nThat already helps. As gis pointed out, something like a \"dedicated\" view is planed, so I'm a very happy man!"
    author: "Klas Kalass"
  - subject: "Re: Performance hit"
    date: 2000-12-12
    body: "Such a view, that can handle all kinds of previews and more is already planned. It probably won't be in 2.1, tho."
    author: "gis"
  - subject: "does it need a new version of konqueror ?"
    date: 2000-12-11
    body: "Or is it a plugin ?\n\nCan I use it now ?"
    author: "Henk"
  - subject: "Re: does it need a new version of konqueror ?"
    date: 2000-12-11
    body: "It's in The CVS, so you can use it now, personally, I'm gonna' wait for the 2.1 beta 1 next monday..........       ;-)"
    author: "Ill Logik"
  - subject: "Re: does it need a new version of konqueror ?"
    date: 2000-12-12
    body: "The current solution requires a new konqueror, but Malte made it \"plugin-ready\", so that with 2.1 you can add preview-plugins for all kinds of files without upgrading konqueror."
    author: "gis"
  - subject: "History Tree"
    date: 2000-12-11
    body: "Maybe I have grossly overlooked some option, but this is the first time that I have seen the History tree in konqueror. Could someone tell me how to do this? Is this a new feature only in CVS? Could bookmarks be implemented like this as well?\n\nThanks,\nDan"
    author: "Dan Christensen"
  - subject: "Re: History Tree"
    date: 2000-12-11
    body: "Seems pretty like a cool idea. But really,\nshouldn't those trees be configurable?  Afterall,\nwhy not make allow any folder to be 'rooted' there, kinda like dropping a folder on the panel."
    author: "Martin Fick"
  - subject: "Re: History Tree"
    date: 2000-12-11
    body: "Select \"Go\" from the menu, and then select \"Directory Tree\", you can then add and modify anything you want in the directory tree.. Check the attached screenshot to see how I've made it :)\n\nThe preview stuff is great. What's really cool is that I don't even notice that this is going on, and konqueror doesn't slow down AT ALL(!!) when using all of these previews.  And after it's done once, it pops up in less then 1 second when re-entering the directory.  But a preview window below would be really cool too, so when you click on a file a larger preview appears in the window below. Also, check out my Napster shortcut in \"Network\". It's a shortcut to the napster io-slave, and when clicking that icon I get the search html page up and can search for mp3's on Napster.\n\nAlso note that the icons I've choosen are medium, using larger icons will give you larger previews (and smaller ->).\n\nAnother cool thing that's been added, and that you can see in the screenshot, is that you can select to have either the 5 most frequent used or the 5 most recently used applications on top of the kmenu.  This makes getting to those often used programs even easier.\n\nThere's also been added a smaller kpanel (now you have: tiny, small, normal, large), this is great for us that wants as much desktop space as possible.\n\nIf anybody wants more screenshots of new features, then please say so and I'll make more and send them :)"
    author: "Christian A Str\u00f8mmen"
  - subject: "Re: History Tree"
    date: 2000-12-12
    body: "<p>\nWhat exactly do your history item point to? I can't find my web browsing history, and the cache files are not ecactly inspiring to look at..?\n<p>\n-anders"
    author: "Anders Lund"
  - subject: "Re: History Tree"
    date: 2000-12-12
    body: "The browsing history is another new feature of Konqueror in 2.1 (or CVS). I can't see it in his screenshot either, so I'm wondering what he meant."
    author: "gis"
  - subject: "Re: History Tree"
    date: 2000-12-13
    body: "Well this let me to another thing:<br>\nThe configuration option for the directory tree is nice, but it is a shame that it only accepts directories!<p> Say I wan't to have a link to my PIM data there, like my calendar and address book - korganizer works completely fine inside konqueror, and it probably wouldn't be that hard to define a view for .kab files...<p>But it dosen't work. Actually confusing, since Knoqueror includes the new->link to location option when I go to Go->Directory tree.<p>\nIt seems that the directory tree can only accept directories or http/ftp urls, whereas a link to file:/home/anders/anders.vcs makes it complaint that it expects a directory. (kde 2.01)\n<p>\n-anders"
    author: "Anders Lund"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-11
    body: "Konqueror is already looking absolutely fabulous. I think that it should be called the \"Universal File Viewer\" because that seems to be the direction it is heading in. The only thing it needs is to get rid of KHTML (which frankly is not really all that good, especially at JavaScript) and use Gecko or something similar. That would allow me to finally delete the powerful but slow and klunky Mozilla!"
    author: "Carbon"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-11
    body: "Huh? Get rid of KHTML???\n\nWhy should they get rid of KHTML - it doesn't do JavaScript that good right now, but otherwise it supports most of the w3 standards you can find, and I bet it's JavaScript support will be much better soon.\n\nFurthermore it's damn fast - I've not seen a faster browser than Konqueror (KHTML).\n\nBesides that, I totally agree with you - Konqueror rocks - it seems like nearly every day (or at least once every week since KDE2 was released), some new cool functions have been to Konqueror. \n\nIt's pretty amazing - I can't imagine what Konqueror *can't* do a year from now...well maybe it can't make me a nice cup of coffee, but else :-)"
    author: "Pointwood"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-12
    body: "I really just don't like KHTML. The reason : it's too good. I know this sounds stupid, but KHTML works really well when it's handed clean code, when it dies is when it runs into a badly designed site. Technically, it supports all the JS calls, but only if they are done to spec. Unfortunately, almost 95% of the sites on the net are either done by idiots who use badly designed 5 year old WYSIWYG HTML editors on their iMacs, or WinNT/Novell fanatics. That's why gecko is so good : it's based off spec, but because that makes sense because most sites are off spec. \n\nI suppose that it would also make sense to fix KHTML to be able to deal with stuff like that. But the reason I reccomend Gecko is because it's the most advanced (with ability to view the largest amount of sites), and the spirit of open-source is based on finding the best available, and improving that. \n\nOr, perhaps we could have a project merge, or a Gecko/QT port? Kecgo or GecKo sounds nice. \nAnyone who wants to see a demonstration of KHTML's excpectations that sites be spec, go to www.littleguitars.com, (that address might be wrong), and watch as Konqueror sucks proc cycles until you are forced to Ctrl-Alt-Bksp.\n\nPlease don't take this as an anti-KDE flame. Just because I don't like a particular kde project doesnt mean i dont like kde."
    author: "David Simon"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-12
    body: "According to <a href=\"http://dot.kde.org/969516460/\">this post</a> on this site, Corel is working on a Qt port of Gecko - if that is true, your will soon be able to use Gecko...\n<br><br>\nBut I still haven't seen any documentation on why  KHTML isn't good - sure it maybe fails to render www.littleguitars.com (I haven't got access to  Linux/KDE2 from where I am now), but as I said before, JS isn't that good supported in KHTML yet, or that is what I've heard and the above site contains broken JS code. But please come with some more facts? I've tried it with numerous sites, and one of the only sites I've seen it doesn't render properly is www.zdnet.com."
    author: "Joergen Ramskov"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-12
    body: "http://cw2001.vtk.be\nhttp://www.vtk.be\n\nNetscape renders them correctly."
    author: "MarkoNo5"
  - subject: "ZDnet *does* render correctly in Konqi"
    date: 2000-12-13
    body: "You just have to change your User Agent to IE...\nReally, it works.\n\n-Justin"
    author: "Justin"
  - subject: "Re: ZDnet *does* render correctly in Konqi"
    date: 2000-12-15
    body: "Hi there,\n\nhmm, maybe a stupid question, but how do I do that? Could not find the apropriate setting yet.\n\nThanx,\n\nRichard"
    author: "Richard Stevens"
  - subject: "Re: ZDnet *does* render correctly in Konqi"
    date: 2000-12-18
    body: "I'm not running KDE right now, so forgive me if this isn't quite the correct spot...\n\nThis is somewhere in Konqi's control panels, as a text field. It's not a checkbox or anything...it's a user-agent checkfield. Right now, it probably says something like Mozilla 4.0 (compatible; Konqueror 1.0). Internet Explorer's string is something like Mozilla 4.0 (compatible; Microsoft Internet Explorer 5.0)."
    author: "Scott Lamb"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-13
    body: "Try out\n\nwww.merckmedco.com (even if you don't have an account, any entry in the login box complains that the entry was empty)\n\nwww.ebay.com (unpredictable crashes upon doing a search in the keyword box)\n\napps.kde.com (a memory leak problem upon loading the view that has many frames [of course, IE has this problem as well]).\n\nI'm not saying that we should abandon KHTML! As I said before, i'm not dissing it, I'm just saying that we need to realize that it isn't compatible with certain sites. These problems must be fixed!\n\nAlso, perhaps some of it is not neccessairly related to KHTML. Perhaps it's a problem with distros. In any case, we really need to either fix KHTML or replace it before Konqueror can become fully usable as a web browser. It's already a totally awesome file manager (whups windows explorer into many tiny sized chunks), but it needs to be improved.\n\nP.S. This is a great example of what is so cool about Open-Source, and in particular, KDE. A great big debate is being started over this issue, and in a large company that uses a totally proprietary development mode, these sort of problems simply get stashed in someones 'in' box, often to be ignored."
    author: "David Simon"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-13
    body: "Sorry, but Netscape 6 that is based on your favorite HTML engine crashed after 30 minutes of use after installation (first run). And now I can't run this \"browser\" again. Even after reinstallation.\nAnd I don't want to speak about its speed and rendering of pages and visual interface - this is pre-pre-pre-alpha version in fact!!! I didn't ever see so many problems!\nSo, sorry, but really, the place for all this \"Gecko\" is in trash. From my point of view two years of development produced only a big, huge piece of shit and nothing more :-(((\nYes, konqueror not always working properly. But it is possible to say this about _any_ browser, even IE. In most cases konqueror working really good. Even with JS. Of course, it crashes often on one of my favorite websites ixbt.stack.net, but I'm really shure that this will improve. Because the konqueror becomes better every day. And I can't say this about other such projects."
    author: "Alexei Dets"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-15
    body: "Ok, first of all, this borders on flaming. Second, I already stated that I think Netscape 6 and (to a lesser degree) Mozilla stink. The rendering engine is what I think we need to either port or borrow code from. The part that dies and slows down the system are the basically unrelated to web browsing composer, email system, news browser, chat engine, stock and news quote checking, and other misc. stinkiness. \n\nAlso, I'm not advocating against konqueror! This is open source : what I'm suggesting is ways in which we might improve it! You seem to take my message as a suggestion that we should ditch konqy! Nothing could be further from the truth."
    author: "David Simon"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-13
    body: "<p>Konqueror is modular. I think it was designed so that even if some functionality (like khtml) is not fully working yet, it does work where it can.</p>\n<p>khtml is just a part, a lightweight (better designed, that is) simile to activex document/control for you windo$e oriented. khtml lives its own life. kthml it's not the main element of konqueror as far as I can tell (I'm not developing kde). In fact (again, kde developers correct me please), konqueror doesn't vitally depend on khtml. You can make it browse any document for which a kpart exists. Say if you feel like implementing .rtf renderer, just write a part. You don't need to even touch konqueror, yet konqueror will use your part to render .rtf files when you configure filetypes right.</p>\n<p>khtml will improve, and if anybody cares to implement gecko renderer as a kpart, he/she would be probably better off starting with qtscape/qtzilla (get it from trolls site). It MIGHT be nice to have two html parts of different implementations... but what for if khtml is going to be more foolproof than IE anyway in a year or two.</p>\n<br>\n<p>khtml is important, but not most important, and konqueror doesn't equal khtml ;-)</p>\n<p>Kuba</p>"
    author: "Kuba Ober"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-15
    body: "Actually, I believe kpart is a koffice term, it's a dcop part (or some such term) for a konqueror plugin. But other then that, I totally agree."
    author: "David Simon"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-16
    body: "Isn't it kpart for general things; kopart for koffice?"
    author: "ac"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-17
    body: "I'm not entirely certain. If someone can provide a link to a resource about what exactly a kpart is, that would be great."
    author: "David Simon"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-12
    body: "What's so great about Gecko? It's slower than KHTML and the pages Konqueror chokes on are usually not handled very well by Mozilla either."
    author: "Zank Frappa"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-12
    body: "Gecko renders better than khtml, even if khtml is a close second and is far better than all the others (except gecko).\nAs an example (and just an example), with CSS, you can redefine bullets of the UL LI tags, using images. Unfortunately, Gecko aligns then to the top of the text (like IE5, btw) while bullets should be aligned with the baseline of the text. Gecko does this fine."
    author: "oliv"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-12
    body: "Well, maybe Gecko is a web developers dream, but in many ways KHTML is superior.\n\nE.g. often when loading a large,complex page A Gecko-based browser processes all content and then displays it all at once. Konqueror OTOH starts rendering the page immediatly, adjusting the page dynamically. This is much smoother, especially if you're using a 56k connection."
    author: "Zank Frappa"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-12
    body: "You should try Gecko, because it is the faster rendering engine for html available. I do not say that khtml say has to be disposed. I just say that gecko faster and more accurate for standards.\nReally, I have just loaded a few slashdot pages and then resized the window\nEverything is done the same way you describe it for khtml. The display is changed dynamically when elements are received (for instance the gif ads appear when they are loaded, and the page display is then redrawn for them)."
    author: "oliv"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2002-09-06
    body: "too bad it is CORRECT to wait for the whole thing to come in\notherwize relatively sized elments will look messed up"
    author: "yo"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-13
    body: "Mostly, the slowdowns are not a gecko thing, but rather a problem with the browser frame around the rendering engine. Netscape 6 and Mozilla both use Gecko, and they are slow because not only do they do web page rendering, but also:\n<UL>\n<LI>Email reading and composing\n<LI>News-browsing\n<LI>Constant updates with \"My Sidebar\" to display such information as \"What's related\", \"tinderbox\", and stock quotes\n<LI>Web site WYSIWYG composing\n<LI>Chatting (Netscape with AOL, Mozilla with Chatzilla)\n</UL>\nAlso, all these things are actually internal in the binary, as opposed to Konqueror's system of dynamically loading plugins."
    author: "David Simon"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-12
    body: "Huh ? Is this the way open-source works ? It isnt perfect right now, so trash it ? Think again."
    author: "Lenny"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-13
    body: "That isn't what I said. I said, the way open source works now is to take whatever is the best option available and improve it. The point of open-source is to avoid wasted programmer time by needlessly copying code that already exists in someone else's program. At the moment, Gecko is capable of viewing more sites better then KHTML is (at least to my knowledge, I haven't being keeping current with the latest CVS). This isn't meant as a dis to KHTML, since we aren't in competetion anyways."
    author: "David Simon"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-13
    body: "<i>whatever is the best option available and improve it.</i><br>\nYes, that's khtml. Just think of signals/slots, is gecko based on Qt ? So we lose all the advantages and gain what ?"
    author: "Lenny"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-15
    body: "I totally agree. You see, I said the best option. It's up to the <u>developers</u> to decide that. Although I'm doing a little coding, konqueror is outside my forte by a large degree! Seriously though, if the people who do konqueror would rather use khtml, then fine, they can! Signals and slots seem a little redundant for a html processor, which is mostly built on a parser system, but that's just me. I trust the konqueror team to do this properly, even if they decide to use the lynx html rendering engine, because their fabulous work so far has me drooling!"
    author: "David Simon"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-16
    body: "I perfectly agree. Konquorer is very shoddy in\nrendering java script. It takes too much time and\neven then does not do it correctly and sometimes\ndoes not even do it!!! Even Netscape is pretty much\nfaster than konqueror in rendering java script\nThis is something which should be very urgently\ndone. This can really harm people who are planning to switch to kde.\nTo view a javascript page, you will have to separately start\nnetscape."
    author: "lxlingan"
  - subject: "Uhh I think DAV is a bit more important"
    date: 2000-12-12
    body: "Not to diss the developer since any additional work is great but in the big picture this is kind of a trivial addition. After all thumbnail previews have been a feature of xv since prehistoric time ....\n<p>\nAdding a DAV IO slave so that Konqui will work with mod_dav enhanced apache web servers (and with apache 2.0 which includes DAV) is a bit more important task and perhaps more involved. But given the API of Konqui/KDE and the wealth of info and libraries and info at webdav.org I can't for the life of me figure out why this hasn't happened. A knowledgeable Konqui developer could likely add DAV support in a day.\n<p>\nI guess the problem is that not many developper's ever use DAV enabled web servers or clients (IE 5's \"Web Folders\" are the only popular implementation in use). Many users probably view DAV as a \"web developer\" issue.\n<p>\nBut DAV is **not** just about web editing or \"replacing frontpage\" (it would be worthy if that was all it was): it's about version control, locking and seamless collaboration using HTTP. In fact Greg Stein joked that DAV may become the \"mother of all protocols\" given its extensive features (see http://www.webdav.org).\n<p>\nDecent DAV support in a client can create possibilities for shared applications like calendars, mailbox files, etc. etc. all over the web. DAV is a HUGE \"under the radar\" (like linux used to be) project - a standard for which support will be necessary in the future. The sooner it gets included and debugged by users the better.\n<p>\nBTW Gnome's new desktop filemanager thingie supports it, so it when it finally gets out of alpha there will be a working proof of concept for how to support DAV in a filemanager style client."
    author: "couard anonyme"
  - subject: "Re: Uhh I think DAV is a bit more important"
    date: 2000-12-12
    body: "Uhh what does xv have to do with this?  Xv is for images this is web pages and will be a very important convenience tool for web developpers. That said I agree about DAV - it's wonderful. Go Live, Photoshop etc are all starting to support it. \n\nBut I think instead of having DAV \"in Konqui\" it should be \"in KDE\" so that every KDE based application inherits it as just another option in the File Open/Save dialogs ... That way Koffice etc would all become \"DAV aware\" (which MS claims future versions of MSOffice will be).\n\nOne thing I thought of re HTML previews was how it'd be great if (with links properly relavitized) you could edit a style sheet in a directory and then refresh and see all the thumbnails update!!\n\nA decent style sheet editor would be a great thing -- apps like Go Live and Cold Fusion's editor provide a really easy to use UI for style sheet editing and adding elements (CF's lets you choose elements to include and edit in a style sheet grouped by what version of each browser supports from CSS)."
    author: "AC"
  - subject: "Or even"
    date: 2000-12-12
    body: "Heh replying to myself ... \n\nOn the stylesheet editor the idea was really to suggest that maybe it would be cool if Konqui's directory views themselves could be controlled by  style sheets editable via a config panel that doubled as a CSS editor."
    author: "AC"
  - subject: "Re: Uhh I think DAV is a bit more important"
    date: 2000-12-12
    body: "<p><I>But I think instead of having DAV \"in Konqui\" it should be \"in KDE\" so that every KDE based application inherits it as just another option in the File Open/Save dialogs ... That way Koffice etc would all become \"DAV aware\" (which MS claims future versions of MSOffice will be).<I></P>\nWell that would be the natural way to implement it - as a new addition to KIO. All the I/O Konqy supports (ftp, smb, NFS, etc.) uses KIO. When a new \"plugin\" is added to KIO, it is available to all KDE2 applications instantly.<br><br>\nTake a look <a href=\"http://www.kde.org/info/overview.html\">here</a> under \"KIO Network Transparency\", where it is mentioned."
    author: "Pointwood"
  - subject: "Re: Uhh I think DAV is a bit more important"
    date: 2000-12-12
    body: "<a href=\"http://quanta.sourceforge.net/\">Quanta Plus</a> (An excellent HTML/PHP/etc editor for KDE2) has a pretty decent stylesheet editor built in. I haven't looked at it enough to know whether it can sort by browser like you say, but this probably wouldn't be too difficult to add.<br><br>\nSomeone has already responded about how KIOslaves work, so I won't repeat that (other than to say it's an amazingly awesome feature ;)."
    author: "Matt"
  - subject: "Re: Uhh I think DAV is a bit more important"
    date: 2000-12-12
    body: "DAV is certainly interesting and on the radar. We are just waiting for a volunteer :-) *hint*\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Uhh I think DAV is a bit more important"
    date: 2000-12-12
    body: "I think Dawit is already restructuring kio_http to  make it extensible for example to support DAV. So if anyone's interested in helping, please mail kfm-devel@kde.org or drop Dawit a mail."
    author: "gis"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-12
    body: "I think it's time for a preview plugin architecture in Konqueror. This way any new plugin engine will not entail another menu item etc... and you could download plugins (imagine a C++ plugin...). I am not sure about how this should be handled at the kde level but I think that it should not be even at the konqueror level but rather at the mime level. Each mime type could have a preview plugin associated with it out of a library of plugins. You will be able to go in Konqueror, select which mimes which have plugins associated with them should be previewed (subset) and be done with it. Besides, doing it in the mime level will enable other applications to use plugins. (imagine a File/Open box which previews files also...).\n\nIn any case - very good work by the KDE team again.\n\n\t\t\t\tMark."
    author: "Mark Veltzer"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-13
    body: "A plugin system like that is already prepared by the current implementation, but the 2.1 Beta 1 release is too close to do it before that.\nAlthough it is not yet using plugins, any application can make use of the preview generator already as the preview generation is done by a KIO Slave (thumbnail:/) which makes it very flexible and easy to use. And yes, the selection of which generator to use (image / text / HTML currently) is done by MIME type."
    author: "Malte"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-13
    body: "Just what I wanted to hear.... sweeeet talk in my ear.<br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-12
    body: "This may be a bit off the start of the topic (my apoligies) but it sounds like someone reading here might be able to help me.  When I try to preview a text file (i.e. a ChangeLog) Konqueror complains of a missing import filter and gives this error \"Could not import file of type text/english\" and thus will not open it.  If I choose Kwrite or Kedit from the view menu it will open the file there but if I try to use it in Kword I recieve the same error.  I compiled from source and have dug around in there looking for a solutiob but to no avail.  Can someone please shed a light on this.  Thanks in advance.\n-Adam"
    author: "Adam Black"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-12
    body: "I really like these \"eye candies\" like thumbnail and alphablending but I'm generally using the detailed view, so my eyes dont get any candies. I thing you should first concentrate on the functionality. What about:\n1) right-click on the \"back-button\" and choose \"new window\"\n2) drag an icon to the taskbar, holding over an application, application comes to the foreground, drop the icon on the application\n3) more consistency with the drag-and-drop (ala os2)\n\nPlease dont think i don't like Kde! I belive in you. (sorry for my english)"
    author: "heroo"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-13
    body: "Your point 2) is an inherently bad UI design, invented by MS. I agree that KDE should copy the *good* parts from Windows, but why the bad parts ;)"
    author: "Haakon Nilsen"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-15
    body: "Maybe! But is there a faster way to get a file from konqueror to kwrite when konqueror is maximized? (dont want to pop up menus to copy/paste the file, dont want to un-maximize konqueror,...).\n\nheroo"
    author: "heroo"
  - subject: "Re: The little details missing in KDE 2.0"
    date: 2000-12-15
    body: "KDE 2.0 is probably the most impressive piece of open source soft, in years. However, I miss a few things from NT.\n\nI think KDE should support customizable CTRL ALT <key> shortcuts to launch applications. For example, CTRL ALT 'K' should have the same effect as pressing the Konqueror icon in the panel.\n\nAlso, the KDE Task Manager and the KDE System Guard should be merged in one. I don't see the need for two different programs.\nEvery time I press CTRL ESC, an additional System Guard appears. I understand this is not a bug, but I would like to have only one of them at a time."
    author: "zhir"
  - subject: "Khotkeys"
    date: 2000-12-16
    body: "I think, what you want is a program to define hotkeys (I want that, too (for xmms)). I hear that there will be a Khotkey-program in Kde2.1 as there was one in Kde1.xx, so dont worry. -heroo"
    author: "heroo"
  - subject: "Re: The little details missing in KDE 2.0"
    date: 2000-12-19
    body: "whats the problem? you can bind \nthe keys anyway you want..."
    author: "noselasd"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-18
    body: "Why dont the konqueror folks do what they do in windows(IExplorer)?\n(just because its windows doesnt mean it dont got any good ideas!!)\n\nWhere a pane on the left is simply a HTML, \nallowing you to write simple HTML displaying some \ninfo on the dir your in, or based on the\nfile type you currently selected..\nSo, clicking a image file, a HTML shows to the left,\ncontaining some javascript, a plug-in able to \nrender thumbnails...\nor selecting a video file, a diffrent html shows \nto the left, with a plug-in(activex) beeing able\nshow the video in about 100,100 pixels..)\nAnd, well since its HTML (actually DHTML) its\nhighly customizeable, where you can have \nglobal views, per file-type or directory view..\nand using your favourite image previewer,\nhtml / text previewer or whatever your DHTML skills\ncan do...\n\njust a thought... a brilliant idea i'd say.."
    author: "noselasd"
  - subject: "Konquerer & Thumbnail"
    date: 2000-12-18
    body: "While reading this I got an idea for large thumbails too:\n\nIf you click in gimp (in the pattern or brush selection) on an element which is larger gimp display it fully.\n\nSomething similar for the image/text/html thumbnails would be great.\n\nIf you click with the Middle Mouse Button on the thumbnail konquerer show it 4x4(/5x5/3x3 or whatever) larger."
    author: "Masato"
  - subject: "Re: HTML Thumbnail Previews in Konqueror"
    date: 2000-12-19
    body: "I really hate having extra files added to my data storage volume. Any chance of an option to force all previews to be stored in the html cache? I assume this will be done by default if the volume is locked ?? but I would like it to happen always.\n\nBTW. Why does the Backspace forward delete. That is illogical. We have a Delete key for forward deletion. An option in the key bindings control to chose BS behavior would be very much better than the suggested hack. While we're at it, a checkbox for turning Numlock on by default would be nice. I know this is an X issue, but again, a checkbox is easier than fiddling with X config files."
    author: "Glenn Alexander"
  - subject: "Relative URLs make Thumbnails Useless"
    date: 2000-12-19
    body: "Hello,\n\nFirst off I'd like to thank the KDE team for advancing an innovation far beyond that of Microsoft's.   Microsoft did have a feature to save a Web page as graphics (say, jpg or png).   Contrast this to KDE's HTML preview -- \n\n1)   Konqeuror does not convert relative URLs to absolute URLs.  This means that any picture or text which is included into the HTML and is encoded with a relative url file will not show up in the thumbnail.\n\nsuggestion:   convert relative paths in the HTML file to absolute ones.   It's is a simple fix.\n\n2)  Creating the thumbnails is slow.  Why not create the thumbnail as you save the HTML from the start? \nIf I click 'Location->Save',  a thumbnail should immediately be created\n\nsuggestion:  save a thumbnail at the same time that one File->Save's a Web page.   Again, a simple fix. \n\n3)   I cannot say that MS has this (but then again, I'm used to KDE innovating past MS):   When selecting bookmarks,  all we're looking at is text.  If I am keeping links to non-English homepages,  I may want instead to select the bookmark based on a thumbnail catalogue.  In this case, I'd open the bookmark directory and look from there (doube-clicking would launch Konqueror with that URL).  But we don't *have* bookmark directories, we have bookmark XML files.  \n\nsuggestion:  Perhaps a \"thumbnail=\" entry should be added to a <bookmark> entry to denote the location of the thumbnail.   Have a mode to select bookmark where a thumbnail catalogue is shown.  THIS in total would be useful.\n\n\nRoey"
    author: "Roey Katz"
---
Anonymous wrote in with the news that <A HREF="mailto:starosti at zedat.fu-berlin.de">Malte</A> has implemented thumbnail previews of HTML documents in Konqueror. Some screenshots: <A HREF= "http://derkarl.org/ftp/pub/incoming/malte1.png">one</A> and <A HREF="http://derkarl.org/ftp/pub/incoming/malte2.png">two</A>.  And, yes, folks, it looks to be configurable!

<!--break-->
