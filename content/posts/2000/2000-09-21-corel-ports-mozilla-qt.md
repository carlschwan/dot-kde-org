---
title: "Corel ports Mozilla to Qt"
date:    2000-09-21
authors:
  - "numanee"
slug:    corel-ports-mozilla-qt
comments:
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "I just recently read that mozilla will be\nbinning GTK right after the First Official release in favour of pure xlib calls.\n\nThe reason was, that mozilla rarely uses gtk\nas it creates its own user interface.\n\nWhat benefits would mozilla gain by using QT\ninstead?\n\nMatthias\n--\nno signature today"
    author: "Matthias Lange"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "Making sure that people have Qt and KDE installed ;o)"
    author: "M_at"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "I think it's not the QT library that is the big deal, but the KParts thing. Having Mozilla as a Konqueror embedded component would be really great (in my opinion), something which is obviously hard to achieve (if possible at all) using GTK.\n\nIt would let you choose between two browsers within one single user interface\n\nMartijn"
    author: "Martijn Klingens"
  - subject: "kpart thing"
    date: 2000-09-21
    body: "wouldn't it be more appropriate to give access to gecko, the HTML rendering engine, by a kpart, and not the whole mozilla app? Or am I missing a point here?"
    author: "raphinou"
  - subject: "Re: kpart thing"
    date: 2000-09-21
    body: "there was a slashdot-article a few month ago about gnome and kde sharing a component model. in the discussion, somebody (sorry, i dont remember) said: \"hey, lets make a 4-way bridge between windows(com/dcom), mozilla(xpcom), kde(kparts/dcop) and gnome(bonobo). I think he got a \"funny\" rating.\n\nthis idea is bothering me from this day. isnt it possible to make such a thing ? take for example a gnome component. what does it take to accomplish this task.\n\nin my understanding, there has to be a parser that generates a kparts/dcop interface from the bonobo-idl and a wrapper-app(possibly generated, too), that maps the bonobo components functionality to the generated kparts interface. this wrapper app acts like a bonobo enabled app to the component and as a kparts/dcop component to the kparts using application and the dcop-server. \n\ni cant really estimate the complexity of such a parser/generator nor the difficulties based on the different designs, but in theory it sounds feasible.\n\nif i am not totally off the ground, such a system could be used for xpcom components and even other component systems. think for example of java-beans inside konqueror..."
    author: "Mathias Henze"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "The great benefit is the internal support of Qt in Mozilla. This means that programmers can e.g. embed the Gecko engine into Qt programs. Or someone could produce a version of Mozilla that replaces these incredible xml-rendered GUI elements with fast Qt."
    author: "Heiko Stoermer"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "Corel would not be replacing just a tiny bit of GTK for a tiny bit of Qt, but dumping a tiny bit of GTK and a whole mass of Xlib in favor a Qt."
    author: "David Johnson"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "This is really good, more Kparts Komponents!\nNow if only someone will port Sun's OpenOffice to Kparts (when it gets released)...."
    author: "t0m_dR"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "Well, it looks like it won't be the same guys!!!\n\nThey will want to sell Corel Office 2000..."
    author: "The SFinX"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "You're right, but for corel I would be happy if  at least they did a port of their (commercial) office suite to Qt and Kparts..."
    author: "t0m_dR"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "Better idea, take any of the good stuff out of OpenOffice and put it into KOffice. \n\nErik"
    author: "Erik Severinghaus"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "That would be cool. A OpenOffice/KOffice hybrid. \n\nI imagine that any such effort might be more complicated then it is worth, so probably just using the OpenOffice code as a guide would be better."
    author: "Ian M"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "Well , *IF* OpenOffice becomes Kparts compatible then stuff from one office will work on the other anyway!"
    author: "t0m_dR"
  - subject: "Where is it ?"
    date: 2000-09-21
    body: "The post says it's at opensource.corel.com \nHowever, That site dosn't mention it at all.  \n\nDo I have to grab the huge CVS tree to get just that one application ?\n\nMemo to Corel... How about posting a tarball of the source and maybe a Linux binary built against KDE-2.0 ?"
    author: "Forge"
  - subject: "Re: Where is it ?"
    date: 2000-09-21
    body: "The post doesn't say it's at opensource.corel.com. Ming was answering my question about where I could find the SMB implementation done by Corel (the mail thread is actually about SMB slaves).\n\nI don't know where you could find it however :)"
    author: "Jelmer Feenstra"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "Wouldn't it be a better idea to make the Geckho Engine a KPart, so that Konqueror users get to choose which component they wish to render their HTML with?\n(I'd choose Konqueror!)"
    author: "Coptain Cack"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-22
    body: "That's what Corel's doing- they're making Gecko a KPart. BTW, technically, you'd choose KHTML, not Konqueror; you'd still be using Konqueror, but with Gecko instead of KHTML. Hope that helps."
    author: "Chris Lee"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2004-10-09
    body: "a heve a problem in the pc hardvare end cd its not vorking,aj delet the file cofig system,have jur ken help my,thenkju from vehbi."
    author: "vehbi"
  - subject: "Re: Corel ports Mozilla to Qt"
    date: 2000-09-21
    body: "First Post!  Sorry, forgot this isn't Slashdot.\n\nIt will be interesting when gecko becomes an embeddable Kpart.  Then a fair comparison of its rendering engine with khtml can be made. I bet khtml uses less memory, important to us with low-end machines.\n\nThis news site is great.  If this post goes through and passes the lameness filter I'll be very surprised. Testing now...."
    author: "John Califf"
---
Buried under hundreds of emails on the Konqueror mailing list, was this little <a href="http://lists.kde.org/?l=kde-kfm-devel&m=96869745925340&w=2">gem</a> from Ming Poon of Corel. Apparently, Corel has been working for months on a Qt port of Mozilla. The results are reportedly impressive with QtMozilla turning out to be more stable than the official Linux GTK version.  Corel plans to port QtMozilla to KParts so it won't be long before you can embed even that in Konqueror.  Honorable mention goes to Roberto Alsina who had begun his own Mozilla port the weekend before.

<!--break-->
