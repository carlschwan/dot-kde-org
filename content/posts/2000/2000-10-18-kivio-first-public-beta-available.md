---
title: "Kivio First Public Beta Available"
date:    2000-10-18
authors:
  - "sgordon"
slug:    kivio-first-public-beta-available
comments:
  - subject: "Re: Kivio first public beta available"
    date: 2000-10-18
    body: "Looks very nice, Shawn!  Congratulations on this first release!  I look forward to installing and using it on my distribution soon (Debian Slink).\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Kivio first public beta available"
    date: 2000-10-18
    body: "<p>This is just awesome. theKompany really, really is on point. I have been patiently waiting for a quality diagramming application for Linux for quite some time. GNOME's Dia program is coming along, but lacks MANY vital elements. And Kivio has Python scripting!!! The first thing I will do is use it to model my Zope objects ;).</p>\n\n<p>One thing I miss dearly from Windoze is something called a \"Mind-mapping\" application, which is a very flexible way of sketching out ideas using a diagram, with links, arrows, and symbols/shapes. I'm a very visual thinker and mind-mapping really aides in the thought process. One excellent example was the\n<a href=\"http://www.inspiration.com\">Inspiration</a> diagram software, from. Not only could you sketch diagrams, but you could dynamically view them as textual outlines as well. I begged and begged them to port it Linux ;)</p>\n\n<p>I wonder what theKompany will come up with next! I just ordered a copy of PowerPlant today. I was thinking, too, that wouldn't it be a good idea to create a KDE distribution (the standard desktop with some enhancements, applications like Kivio, Magellan, and KOffice) and provide an update service to it through Magnum (theKompany installer/updater)? This way, non-developers could get a quality package of desktop applications, and this would put KDE on par with HelixCode's strategy, which is VERY appealing to new users that want an easy way to setup a desktop environment and applications.</p>\n\n<p>Just food for thought. Anyways, three cheers to theKompany AND the KDE team for such excellent, excellent work!</p>\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Kivio first public beta available"
    date: 2000-10-18
    body: "I think that Eron Lloyd and I are not the only ones that have thought of a KDE-distribution. Bare-bones Linux plus KDE and KDE-applications in a special KDE-distribution will happen sooner or later, I am sure, the sooner the better.\n\ntheKompany may very well be the ones that does it."
    author: "reihal"
  - subject: "Re: Kivio first public beta available"
    date: 2000-10-20
    body: "Wait-- why would it need a bare-bones Linux distro?\n<p>\nI like Helix's idea: If you use RPM, the install downloads and installs rpms transparently; if dpkg is used, then it downloads and installs debs transparently.\n<p>\nIn my opinion, a Linux distro isn't needed... it wouldn't be a great on anyways.\n<p>\nKeep up the Superb Work!"
    author: "Jason Katz-Brown"
  - subject: "Re: Kivio first public beta available"
    date: 2000-10-19
    body: "Actually, KOffice is a part of the default KDE desktop, it isn't a theKompany thing, except for their help on Kword. I personally am waiting for some news on Krayon, right now the link just leads to the koffice.kde.org/killustrator page, which hasn't been updated in ages. I'm tired of GIMP layering crashing!"
    author: "David Simon"
  - subject: "Re: Kivio first public beta available"
    date: 2000-10-19
    body: "In the case of KWord and Krayon, those really aren't mine to take over, which is why we simply point to the web sites.  Our developer support on those projects has been slim at this point, but there are others working on them more now, so that helps everyone.\n<br>\nI agree with you about the web pages, we had some discussion of updating them, and I offered my web designer, which was agreed to, I just needed some stuff first, but everyone got too busy with KDE 2.  We definitely need to get the web site updated soon, I'm glad your note reminded me of that."
    author: "Shawn Gordon"
  - subject: "Re: Kivio first public beta available"
    date: 2000-10-24
    body: "Hi,\nregarding a mind mapping application: months ago I started to write one, which is already more or less usable. You can find it at http://mymap.sourceforge.net . \nHowever: development has totally stalled since I'm in state exams right now. So I don't even know if it still compiles with KDE final, as I haven't tried for weeks. However, there's a JAVA mind mapping program, too, by Joerg Mueller: http://freemind.sourceforge.net . It's coming along nice, and we decided to agree on a common XML file format (although mymap doesn't adhere to that yet). I will eventually continue on mymap and do a rewrite in 2001, when I have time :)"
    author: "Jost Schenck"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-18
    body: "Thank you both for the kind words :).  This release of Kivio does *not* have the Python scripting active yet, we are working on it right now.  We are also hoping to put a DEB of Kivio up by the end of the week.  Things got a bit hectic at the last minute, but we really wanted to get this out to people.  Thanks for all the support :)"
    author: "Shawn Gordon"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "Any chance of source for at least the final release?  ;-) I don't doubt that the debs will be for Debian Potato and those probably won't work on my machine.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "I am very eager to use an application like Kivio. Professionally, I build dynamic websites using an opensource application server written in java and configured with xml. It would be wonderful if I could define entities and relations (and a couple of attributes specific to the application server I use) and convert that information to xml definitions of data structures that can be read by the application server.<br>\nJust wondering: how does Kivio's functionality relate to kuml?"
    author: "Case Roole"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "We have some very interesting ideas ahead for Kivio such as integration with databases via KDE-DB to generate ERD's and other fun things, it's all just a matter of time and resources to get them out.  Keep watching, there should be more features quickly :)."
    author: "Shawn Gordon"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "stuart@pulse:~> kiviopart\n\nkiviopart: error in loading shared libraries: /opt/kde/lib/libkiviopart.so: undefined symbol: hasGroup__C7KConfigRC8QCString\n\n:(\n\nOh well...I am only using 1.94, so I guess I'll have to update to RC2 afterall...or wait a little longer for KDE 2.0 and the second beta....\n\nBut anyway....from the screen shots this looks like something I'll be using a lot..\n\nKeep up the good work!"
    author: "Stuart Herring"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "I'm having similar issues, I haven't been able to run Kivio. I'm using RH6.2 + KDE RC2.\n\nReally sad thing, though it looks very nice on the screenshots."
    author: "Carlos Miguel"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "This is the error message:\n\nkiviopart: error in loading shared libraries: /usr/lib/libkiviopart.so: undefine\nd symbol: create__11KLibFactoryP7QObjectPCcT2RC11QStringList"
    author: "Carlos Miguel"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "We did make an update to the rpm early today which solved the problem for most people, but you have to be on the latest Qt, and the KDE2 RC2 to be totally compatible, which it sounds like you are.  Try pulling down the RPM again just to make sure that the one you got was correct.  When KDE2 final comes out, we will all be on the same libraries and it should be ok at that point."
    author: "Shawn Gordon"
  - subject: "A similar error message..."
    date: 2000-10-19
    body: "<p>I have tried both packages (yesterdays and today), and I still get the same error:</p>\n\n<pre>kiviopart: error in loading shared libraries: kivio.so: cannot opened shared object file: No such file or directory.</pre>\n\n<p>Could this be a PATH error? I checked the installation with KPackage - everything seems to be there.</p>\n\n<p>Darn. Guess I'll have to wait until next week ;)</p>\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: A similar error message..."
    date: 2000-10-19
    body: "It looks like your KDE2 libraries are binary incompatible with the versions used to build Kivio.\n\nCould you please send me the output of the following commands:\n\nldd /usr/bin/kiviopart\nldd /usr/lib/libkiviopart.so\n\nplease send them to support@thekompany.com so we don't clutter up this discussion forum.  This is true for anyone having this problem.\n\nthanks,\nshawn"
    author: "Shawn Gordon"
  - subject: "Re: A similar error message..."
    date: 2000-10-19
    body: "Where do you have KDE installed?\nBy default KDE rpm's for RH6.2 use /usr, and Kivio rpm is set to use /usr/local\n\nUninstall it and try again using \n\nrpm -ivh --prefix=/usr <Kivio rpm>\n\n(replace <Kivio rpm> with the actual rpm file name)"
    author: "Carlos Miguel"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-20
    body: "The version I see on your ftp server looks like it is still an old version. (2630990   Oct 18 19:21   kivio-0.9.0-1tkc.i386.rpm)\n\nI'm running RH7.0 with the RC2 kde (qt-2.2.1-3)\n\nThe error I get is...\n\nganesh% kiviopart 2>&1 | c++filt\nkiviopart: error while loading shared libraries: /usr/lib/libkiviopart.so: undefined symbol: KLibFactory::create(QObject *, char const *, char const *, QStringList const &)"
    author: "Brian Wallis"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-20
    body: "If you can please direct support questions to support@thekompany.com we can better address the different issues.  We are putting up a few different RPM's and DEB this week (DEB should be up now).  Once KDE 2 final is released, it should solve many of the issues.  We are trying to accomodate as many people as possible in the meantime.  Thanks"
    author: "Shawn Gordon"
  - subject: "I Got the same error.Try the tarball worked for me"
    date: 2000-10-23
    body: "-||-"
    author: "AC"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "Is this/will this be an open-source project?  I'm not complaining if it isn't, but licensing terms aren't available on the website, at least not to the page linked to."
    author: "ac"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "I was wondering about this also.  I didn't see any source packages for Kivio.  Do they plan on selling select apps with their cd set?\n<p>\nI get the uneasy feeling that their in house projects are going to be binary only packages.\n<p>\nHope I'm wrong.\n<p>\nSimon"
    author: "Simon"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "Well we ended up in a last minute rush to get Kivio out to meet our internal deadline.  It will be open source, however we haven't decided on the license yet that works best for us over the long term and we're having our lawyers review them.  Because of that, we only released the binary for now until we get it figured out.\n\nI do hope you've noticed all the other projects that we are doing that are totally free open and available such as VeePee, PyQt/PyKDE, KDE-DB, Kugar and Korelib.  This should be more than enough compared to our commercial undertakings to show our support of OSS and open licensing.  Our Python stuff particularly is totally free and isn't even hampered by the GPL.\n\nRemember, we do have a business to run so there has to be some things we make money on like PowerPlant so we can keep working on the things I just mentioned."
    author: "Shawn Gordon"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "Shawn,\n<p>\nThanks for the reply and the reassurance.  I'm all for you guys making money off your projects; further proof that OSS works.\n<p>\nI'll have to take a look at some of the other projects on the site.\n<p>\nOn a side note have you guys considered packaging up KDE with your installer similar to what the Helix Code people have done?  It would be great for people who don't want/know how to compile source or download dozens of packages and install.\n<P>\nThanks and Congrats.\n<p>\nSimon"
    author: "Simon"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "Hi,\n\nDon't take me wrong, but even Qt went GPL...\nI don't think Kivio will be well received as a KOffice member if they don't share the same license.\n\nHow about a dual license?"
    author: "Carlos Miguel"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "to reply to Simon and Carlos.  Unlike many other companies, we try to not pre-announce things way in advance.  We started on our update technology a year ago with a mind towards things like what Helix eventually did and RedHat is doing.  Great minds think alike? :), maybe, but we are very focused on KDE and we have some very exciting things planned with other prominent members of the KDE community.  Hang on and see what happens shortly after the release of KDE 2, I think you will all be pleasently surprised.\n\nAs to the license issue that Carlos mentions.  It's really a matter of just having the legal people make a solid determination for it, a dual license might be the way to go, but I'm a developer and not an attorney, so I'm just waiting on an educated opinion.  Don't worry, it should all be cleared up by the next release, in the mean time try to enjoy the software if you can :)."
    author: "Shawn Gordon"
  - subject: "Only for programmers?"
    date: 2000-10-19
    body: "Waow, I am really impressed. Looks like a nice productivity tool which will make lots of people's work easier.\nMy only problem is the following... Mabe I am an exception here, but I am just a \"plain normal user\" who knows only in theory what Python and C++ is. So I am a person who tried to use Linux in non-programming fields (I do comparative education research)... will there be any tools in there suitable for \"normal people\"? It would be graet to have several stencil bars with more general elements than just programming =)\nCan't wait to see the final product =)"
    author: "Divine"
  - subject: "Re: Only for programmers?"
    date: 2000-10-19
    body: "The beauty of the Python based scripting available with VeePee is that it's a layer above Python and is meant for the power user.  Just as it's not required to use VBA with Microsoft products, it isn't necessary to use VeePee in our products, but it's there for you if you want to use it.  I just want to emphasize that it is designed for normal people, at least as much as a scripting environment can be."
    author: "Shawn Gordon"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "Hummm.... can't seem to find the source and the rpm is x86 only...."
    author: "AC"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "You hadn't time to read the above, did you ?"
    author: "Lenny"
  - subject: "Don't be pressured to open-source it."
    date: 2000-10-19
    body: "Hi Shawn,\n\nGreat work. I hope you will have great luck with your product. I  know that I will buy it when it is finished.\n\nI'm a big fan of the GPL. I am also a fan of closed-source development using LGPL libraries. I think commercial firms should be encouraged to port their stuff to linux using the LGPL libraries. As far as I know, all KDE libraries are LGPL (except QT, which you will have to pay for, I guess). \n\nAs your product is not a library, there is no reason why you should feel pressured to open source it. I hope you guys make lots of money with your project.\n\nMagnus."
    author: "Magnus Pym"
  - subject: "Re: Don't be pressured to open-source it."
    date: 2000-10-19
    body: "me man. Where are you coming from?"
    author: "Anonymous"
  - subject: "Re: Don't be pressured to open-source it."
    date: 2000-10-20
    body: "Hehe, a slow day at Microsoft, or what? ;)"
    author: "Haakon Nilsen"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-19
    body: "ok, very nice program... but I dont see the sources. Is this open source ?\n\n  I dont use RH so I need to recompile it...\n\n  thx"
    author: "Romero"
  - subject: "Re: KDE Requirements for Kivio?"
    date: 2000-10-19
    body: "I like what I see and would like to try this program.  Question is: I don't use the KDE desktop environment; does anyone know if this program requires the KDE environment to run, or just QT and KDE core libraries?  I don't mind installing some KDE libraries, but I'm not sure I want to change my desktop environment right now."
    author: "Dave Rosky"
  - subject: "Re: KDE Requirements for Kivio?"
    date: 2000-10-19
    body: "You will need most kde libs:\nkdesupport,\nkdelibs,\nkdelibs-sound,\nkdebase,\nkoffice.\n\nDid I forget some?"
    author: "Al_trent"
  - subject: "Re: KDE Requirements for Kivio?"
    date: 2000-10-19
    body: "Hmm, I dont't think you need to install KOffice to use Kivio. Just kdelibs and QT should be enough.\nMake sure you install the latest version of KDE (KDE 2.0 RC2)"
    author: "rinse"
  - subject: "Answers from the RPM"
    date: 2000-10-19
    body: "OK, for fun I downloaded the RPM and here are the unresolved deps:\n\nerror: failed dependencies:\n        libDCOP.so.1 is needed by kivio-0.9.0-1tkc\n        libkdecore.so.3 is needed by kivio-0.9.0-1tkc\n        libkdesu.so.1 is needed by kivio-0.9.0-1tkc\n        libkdeui.so.3 is needed by kivio-0.9.0-1tkc\n        libkfile.so.3 is needed by kivio-0.9.0-1tkc\n        libkio.so.3 is needed by kivio-0.9.0-1tkc\n        libkofficecore.so.1 is needed by kivio-0.9.0-1tkc\n        libkofficeui.so.1 is needed by kivio-0.9.0-1tkc\n        libkoml.so.1 is needed by kivio-0.9.0-1tkc\n        libkparts.so.1 is needed by kivio-0.9.0-1tkc\n        libkstore.so.1 is needed by kivio-0.9.0-1tkc\n        libksycoca.so.3 is needed by kivio-0.9.0-1tkc\n        libqt.so.2 is needed by kivio-0.9.0-1tkc\n\nIt looks like it needs quite a bit more than just QT and the kde core library.  In particular, it looks like it *does* need Koffice, at least the core libraries anyway.\n\nIf this app matures, I think it will definitely fill a void in UNIX apps and could become really popular. It seems like kind of a shame to make a nice app like this so heavily tied to a particular desktop environment - sort of goes against the general Linux philosophy (that of choice).  Suggestion for the developers:  when the source is released, include a compile option to shed the KDE/Koffice integration portions so fewer extra libraries will be required for non-KDE users, who won't benefit from those portions anyway."
    author: "Dave Rosky"
  - subject: "Re: Answers from the RPM"
    date: 2000-10-19
    body: "Dave, our focus as a company is heavily tied to KDE for the most part, and we are very happy with the level of KDE and KOffice integration we've been able to achieve and even more is planned.  Not to sound flippant, but the idea of choice for Linux goes many directions.  We choose to support KDE, you choose not to use KDE, you can also choose to not use Kivio because it uses KDE.  If someone requires us to make our products and projects a particular way, then it removes our choice from the equation.  Is one persons choice more important than anothers?"
    author: "Shawn Gordon"
  - subject: "Re: Answers from the RPM"
    date: 2000-10-20
    body: "Shawn (and David),\n\nI think I might have been misunderstood.  Although I didn't actually intend to start a long thread, I would like to clarify myself.  Also, lest people think this is a Gnome vs. KDE thing, it is not - I actually use XFce most of the time because I have some older, smaller systems and I like simplicity.  \n\nFirstly, I think application integration is great.  When you started this project and you wanted to have integration with other productivity applications, you obviously had to make a choice, and I think Koffice and KDE was a very good choice as it is more mature and functional than other open source office suites.\n\nMy point is simply that your application looks to be very useful and may fill a void that will make it attractive to many users of UNIX and Linux, even as a standalone application. KDE/Koffice integration is fine, but if you make it possible for your program to run on non-KDE systems without the installation of a lot of additional software, you will probably have more users in the end and advance the use of UNIX in general as a productivity tool.\n\nOne should definitely *not* feel compelled to avoid Kivio simply because it uses KDE -- that would be rather silly and sophomoric.  If Kivio is an awesome application in its own right (as it appears it may become), I will want to use it regardless of  whether it integrates tightly or loosely with my particular office applications.\n\nIt seems it should be possible, even at runtime, to determine if certain libraries are not available and just disable the integration functions that require those libraries.  As an example, these days many Windows applications have high levels of integration with MS Office, but it is certainly possible to install and run these (binary) applications without having Office on your system, you just loose the integration functionality.  I'm not familiar with the intricacies of KDE programming, but my question is basically why is this kind of behavior not possible to implement with an application like Kivio?\n\nMaybe I'm making a mountain out of a mole hill - I'm sure that if I like Kivio enough, I'll probably go ahead and install all of the extra stuff; however,  I would certainly prefer not to.  True, disk space is cheap, but nobody wants to use it inefficiently, and a full installation of the items required by the RPM is not small.\n\nRegards"
    author: "Dave Rosky"
  - subject: "Don't be cheap."
    date: 2000-10-24
    body: "Disk space for all of KDE2 (including KOffice) amounts to about 75 U$S cents. \n\nMaking Shawn's life harder for even 3 minutes costs more than that. So, be a pal, pay the 3 quarters, install the stuff, and we can all be happy ;-)"
    author: "Anon. Advocate."
  - subject: "Tables Turned?"
    date: 2000-10-20
    body: "Here's a excerpt from the KImageShop web page:\n\n\"KDE Image Manipulation. There have been renewed talks of future cooperation between KDE and Gimp developers, both on the gimp-developers list as well as on the #gimp channel. Most seem to agree that the Gimp should evolve towards a toolkit-agnostic architecture, but there has as yet not been unanimous agreement on a collaboration. \"\n\nSo here we have a case of KDE app developers wishing that an existing mature, useful application was more more toolkit agnostic because it would have made their efforts much easier.\n\nGimp, while perhaps not perfect, is a relatively mature open source image manipulation program.  KImageShop is a brand new, from-the-ground-up effort.  One of the great benefits of open source development should be the efficiency you get from reuse of existing code.  It would be great if KImageShop could borrow code from Gimp and ImageMagick - this is the way open source should work.  But the problem is that the underlying Gimp architecture is apparently too heavily tied to the GTK toolkit, making resue in a KDE application difficult.\n\nWould making Gimp more toolkit-agnostic be an undue burden on the developers and limit their choice too much?  Maybe at this point it would, but it probably would have been much easier to do had it been thought of at the beginning of the project.\n\n-D"
    author: "Dave Rosky"
  - subject: "Re: Answers from the RPM"
    date: 2000-10-19
    body: "You do not need to use kde to use kivio, you just need to install it! Hard drive space is cheap, and you dont need all of kde, just the main libs, and accessory libs, like koffice. P.S. I think it would be hard for theKompany to add an option not to add KDE integration, because KDE comes with, among other things, a couple of extra widgets, which would be hard to work without. Also, I expect GNOME's office suite as well as OpenOffice to come up with a program simillar to this very soon. Competetion is good for the industry, if a great Open Source enviroment like the Linux/BSD community can be called that."
    author: "David Simon"
  - subject: "Re: Answers from the RPM"
    date: 2000-10-20
    body: "David,\n\nLet me make a counter example:  Gimp is a very popular GTK application.  If you want to use Gimp, you must have GTK on your system.  No big deal.  But suppose you also had to download and install (and maintain) a dozen or more of the Gnome RPMs and keep them all current?  As a non-Gnome user, you might do it if you like Gimp enough, but you probably wouldn't be to happy about it.  If and when Gimp becomes bonoboized, I hope they do it in a way that will avoid non-Gnome users from having to install and maintain so many additional packages that they don't otherwise need.\n\nHere's another rub:  If someone is responsible for deploying applications in a working environment with several machines, as I am, they now have to track and maintain not one or two, but a dozen or more extra packages that aren't used for anything else.  Yes, I have scripts that to updates on multiple machines, but it's extra tracking work that I could do without.\n\nIt seems that it should be technically possible to have it both ways -- to detect whether KDE is present or whether the program is stand-alone, and provide more or less integration functionality depending on the result.  If this can be done, the application will have a lot more general appeal."
    author: "Dave Rosky"
  - subject: "Re: Answers from the RPM"
    date: 2000-10-21
    body: "Your main argument seems to be that GNOME (and similar packages) are difficult to maintain to their latest version. Not so.\n\nurpmi, when given the name of an rpm, automatically goes on the net, grabs the latest version of the package AND grabs all the packages that it requires. And, if you don't upgrade GIMP except occasionally (and you often don't want to update unless there is a feature or bug-fix added you might want to use, or if it is a security issue), there is no need to do that very often!\n\nAll you have to do to make it update is put in a cron job on each machine, going 'urpmi gimp', or something similar, every 6 months or so, or just do it manually! Also, GIMP does not require a dozen or more GNOME packages, all it needs are the base libraries (for instance, you dont need any of the utilities or games or editors or ...)\n\nPlus, every time GIMP is updated, that doesn't neccesarily mean that you need to get a new GNOME as well!"
    author: "David Simon"
  - subject: "Re: Answers from the RPM"
    date: 2000-10-21
    body: "\"urpmi, when given the name of an rpm,\n   automatically goes on the net, grabs \n   the latest version of the package AND \n   grabs all the packages that it requires.\"\n\nThanks for that info.  I didn't know about it, and I'll look into it.\n\n   \"Also, GIMP does not require a dozen or more\n   GNOME packages, all it needs are the base\n   libraries.\n\nActually that's exactly my point.  In fact, last time I installed GIMP, it didn't need *any* GNOME libraries, it just needed the GTK libraries.  Now, the last version of GIMP I installed was not componentized, so that may change.  I expect that I need to install QT to use a QT application.\n\nLike I mentioned in one of the other posts,  the extra package installation and maintenence is not really that big of a problem for me.  In my case, I'm probably making a mountain out of a mole hill on that one.\n\nThe issue is really more one of software design and who the target audience is.  I beleive that theKompany has potentially created a program that could be of appeal to an audience much larger than just KDE and Koffice users.  It may also appeal to non-technical users.  If you want users of other desktop environments (non-technical users, in particular) to be able to easily use your program, you don't want them to have to be maintaining lots of extra libraries and packages.  This is one of the things that creates entry barriers to average users adopting Linux.  Windows developers tend to take into account ease of installation and use for average users, Linux developers often do not.  If we keep assuming all of our users are highly technical, then Linux will not grow beyond the technical community.\n\nThere are many examples, particulary from the Windows world, of applications that integrate tightly with other packages (generally MS Office), but are able to run stand-alone without the user having to install several pieces of Office.  It might take a little extra coding effort, but it should be possible to do this with Linux applications as well."
    author: "Dave Rosky"
  - subject: "Re: Answers from the RPM"
    date: 2000-10-20
    body: "<i>\nerror: failed dependencies:<br>\n libDCOP.so.1 is needed by kivio-0.9.0-1tkc<br>\n libkdecore.so.3 is needed by kivio-0.9.0-1tkc<br>\n libkdesu.so.1 is needed by kivio-0.9.0-1tkc<br>\n libkdeui.so.3 is needed by kivio-0.9.0-1tkc<br>\n libkfile.so.3 is needed by kivio-0.9.0-1tkc<br>\n libkio.so.3 is needed by kivio-0.9.0-1tkc<br>\n libkofficecore.so.1 is needed by <br>kivio-0.9.0-1tkc<br>\n libkofficeui.so.1 is needed by kivio-0.9.0-1tkc<br>\n libkoml.so.1 is needed by kivio-0.9.0-1tkc<br>\n libkparts.so.1 is needed by kivio-0.9.0-1tkc<br>\n libkstore.so.1 is needed by kivio-0.9.0-1tkc<br>\n libksycoca.so.3 is needed by kivio-0.9.0-1tkc<br>\n libqt.so.2 is needed by kivio-0.9.0-1tk</i>\n<br><br>\nall this is just Qt, the KDE libraries and the KOffice libraries. You need to install Qt and kdelibs to run any KDE program anyway. For this program, you additionally need the KOffice libs, but don't worry, those are not that big."
    author: "gis"
  - subject: "Mac app like this allows \"export to web site\""
    date: 2000-10-20
    body: "Sort of brainstorming and diagramming app. In one mode (called \"web site brainstorm\" or something) you could attach files and images to a node (making them \"compound nodes\") and then link that node to another (or link one element of a node to another node). Then when exported to HTML a page would be made of each node with appropriate links from/to other nodes/pages, images, etc.\n\nSupposedly this WWW mode was a toss in expected to be not useful for much beyond \"roughing up websites\" but it became a great way to actually design a web site."
    author: "AC"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-20
    body: "Congratulations! The screenshots show a great interface ... it seems as if you have moved to feature parity with Dia already. Wow.\n<p>\nI have a question: once the diagram is up on the page, is it static? I mean, suppose you want to display the status of your servers using a Kivio diagram (with appropiate VeePee code behind it) and one of them started going up and down could the Kivio diagram show that dynamic behavior (a color change)?. Doesn't have to be real time ...\n<p>\nAnd another thing. Doesn't this help out the folks working on project planning tools?"
    author: "Allen"
  - subject: "Re: Kivio First Public Beta Available"
    date: 2000-10-20
    body: "Currently they are static, we have some very interesting plans on making Kivio interactive with a number of different environments, some of which are sure to surprise you :).  Good things come to those who wait :)."
    author: "Shawn Gordon"
  - subject: "Wow what an original design"
    date: 2000-10-21
    body: "KDE: Kind of like Windows only flakier."
    author: "Frank Rizzo"
  - subject: "Can it read/write Visio ?"
    date: 2000-10-23
    body: "This is fantastic.  I've just recently had to start using Visio on MS Windows for the first time, to document a solution I've just built for a customer.  It's great to see that a Linux solution to fill this niche is on the way.\n\nWhat's the file format?   Will it be able to read/write Visio stencil & template files?\n\nJohn"
    author: "John"
  - subject: "Re: Can it read/write Visio ?"
    date: 2000-10-23
    body: "The file format is XML.  Visio filters are on the \"to do\" list, but they are last on the list, we need a fully functional product first.  So you will see them, I just can't say when."
    author: "Shawn Gordon"
---
<a href="http://www.theKompany.com/">theKompany.com</a> is pleased to announce the first public beta of <a href="http://www.thekompany.com/projects/kivio/">Kivio</a>, our diagramming and flowcharting tool for Linux/KDE. 

Kivio is the first and most complete diagramming tool for KDE. Kivio has been integrated into KOffice through the use of KParts. This technology preview release is the first public beta of Kivio. There is a core set of functionality and features to start getting feedback from the Linux/KDE community. 

A second beta will be released within a day of the final KDE 2 release to ensure compatibility and release some new features. You can find out more and download Kivio <a href="http://www.thekompany.com/projects/kivio/">here</a>. <i>[Ed: Nice <a href="http://www.thekompany.com/projects/kivio/screenshots.php3">screenshots</a> too.  Only RPM available at this time.]</i>










<!--break-->
