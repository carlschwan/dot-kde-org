---
title: "German-Sponsored KOffice Meeting Report"
date:    2000-10-01
authors:
  - "Dre"
slug:    german-sponsored-koffice-meeting-report
comments:
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-01
    body: "ok!!!!\n\nI love KOffice. Now is a Beta, but I use this for my personal use."
    author: "Manuel Rom\u00e1n"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-01
    body: "Can't wail to get rid of MS Office on my machine. As soon as the MS Word import filter can import tables and images nicely StarOffice and Word are coming off my machine.\n<p>\nThanks Guys, and keep up the great work"
    author: "Rick"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-01
    body: "Give up.  KOffice sux!  StarOffice 6.0 will be out and much more functional than any other office package.  You only need wait <2 weeks for it.  It is a real MS Office killer, not a wanna be like Krap Office."
    author: "ME"
  - subject: "Re: StarOffice"
    date: 2000-10-01
    body: "StarOffice is bloatware"
    author: "jf"
  - subject: "Re: StarOffice"
    date: 2000-10-01
    body: "Gee.  Now that it will be GPL'd, any bloat will likey be evaluated and removed.  Make sense?  I think so.  How much would you care to wager that it will become the premier cross-platform office suite?"
    author: "ME"
  - subject: "Re: StarOffice"
    date: 2000-10-01
    body: "<i>Now that it will be GPL'd, any bloat will likey be evaluated and removed. Make sense? I think so.</i>\n<p>\nKOffice is GPL too and very nicely integrated into KDE2, SO isnt. KOffice makes sense ? I think so.\n"
    author: "Lenny"
  - subject: "Re: StarOffice"
    date: 2000-10-01
    body: "I dont use KDE all the time. Choice. I don't want an office suite tied to a desktop. That's what we are stuck with now in M$-land.  Can you use M$ Office in Linux, Solaris...  No.  It is tied to Winders.  It is crap.  StarOffice may have some faults, but it is not tied directly to any platform, much less a particular desktop on a platform."
    author: "ME"
  - subject: "Re: StarOffice"
    date: 2000-10-02
    body: "StarOffice will be tied to gnome eventually. Anyway, you say you want choice... well you have choice. KOffice, StarOffice, Applix. StarOffice isn't all bad, you are right. It has decent filters. But I suspect they will not be GPL'd :( The rest of StarOffice is OK at best. We have the chance with KOffice to make a real good job of an office suite; to get StarOffice so that it is fast, stable etc it'll take a long time. Think mozilla, only StarOffice is bigger and more complicated."
    author: "ac"
  - subject: "Re: StarOffice"
    date: 2000-10-01
    body: "Don't you know that cross-platform comes at a price?\nWhile I won't deny the advantages of cross-platform software, platform specific applications are generally more straightforward to code and mantain, furthermore, cross-platform ability depends on leveling layers that don't add to performance.\nDon't expect S.O. to be bonoboified and integrated to a Desktop Framework any time soon...who knows at what level of functionality will the S.O. code be released,...just remember Mozilla."
    author: "Carlos"
  - subject: "Re: StarOffice"
    date: 2000-10-01
    body: "Yes, it does come at a price.  Think about the benefit of open formats and the ability to work effectively with M$ formats, though.  How long before M$ will be forced to support StarOffice formats in it's products?  Not long.  KOffice?  Much longer.  This gives the world a choice of suites to use and be productive.  There is no way that my users could be productive with KOffice for the forseeable future, but they could with StarOffice.  Not only that, but the better import filters in StarOffice provide a direct path away from M$ bloat on ANY platform.  Get users using SO on M$ boxen and soon I will be able to start switching OS's to Linux and they wont know or care.  Tell me you can do that with Koffice.  You can't!  I see that these people need something to do, so contribute to a real M$ Killer and leave the toys behind.  No troll, just looking at it from a logical business perspective. :-)"
    author: "ME"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-01
    body: "Please go directly to alt.troll. Thanks!"
    author: "mk"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-01
    body: "Whatever, you turd.  If you can't participate in a reasonable discussion without calling someone a troll because you don't like what they say, then butt out."
    author: "ME"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-02
    body: "You call this a reasonable discussion?\n\n> KOffice sux!\n> It is a real MS Office killer, not a wanna be like Krap Office.\n\nMind your language, idiot. Fuck you!"
    author: "malte"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-02
    body: "I guess you have never used SO then? It is one of the *worst* pieces of software I have ever used. SO version 6 is coming out? If it's anything like the previous versions it will be crap. Bloated and crap. And when it's moved over to gnome... geez, it'll suck even more. Thankfully we have Koffice. Much nicer."
    author: "ME sux"
  - subject: "You will wait a lot longer than 2 weeks."
    date: 2000-10-02
    body: "According to comments from Sun employees on  openoffice.org, the version of code that sun will relase on the 13^th of october will be alpha at best. What they have done is to rip out all the third party software in the current staroffice codebase and made sure that it still compiles. The target release date for Staroffice 6.0 is at least 6 months away, and not before the release of Gnome 2.0.\n\nMagnus."
    author: "Magnus Pym"
  - subject: "Re: You will wait a lot longer than 2 weeks."
    date: 2000-10-05
    body: "...and not much less to compile this beast. According to www.openorifice.org, it takes approximately 20 hours for a full build on a PIII with 256 MB RAM running Linux. 3MB spare HD space is recommended also.\n\nCan anyone spare a few GB for a hungry gnome?"
    author: "Francis Sedgemore-Schulthess"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-03
    body: "I don't know whether KOffice sux or not.  I have used StarOffice 5.1, and I discarded it because it was truly awful.  It has a pretty face, but many of those decorations are just stubs.  The newsreader is non-functional alpha-code, but I could not figure out how to use the word processor without having that non-functional news reader loaded along with it.  That desktop thing is an awful nuisense when you already have the KDE or GNOME desktop."
    author: "Arcturas"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-01
    body: "what is this with you???\nwhy flame about a free software project.\nfree software has always been about choice.\nI can choose between linux/bsd or between\nkde/gnome/enlightenment... or between emacs/vi.\n\nso many people also want to choose their office suite. some people will choose koffice, some staroffice, some gnumeric/abiword and some will\nstay with emacs/latex.\n\nI think koffice is a great effort by great programmers. if you do not like it, do not use\nit, but don't flame.\n\nin the end, everybody will profit because one can\nchoose between many great software packages"
    author: "markus jais"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-01
    body: "No flame.  Like I said in another post, we are up against M$.  They are a force and will be for some time to come.  In order to combat that, we should align ourselves and pooll the efforts behind the best and most mature code there is and right now that is StarOffice.  That makes the community stronger in providing choices.  Choice doesn't necessarily mean that I need or want 15 different word processors or spreadsheet apps to choose from!  I want to get the best there is and not have to do research on the damn things just to figure out if it has the features that I want.  Pooling the effort behind a M$ killer gives an immediate choice that none of the others provide.  Pooling the effort puts more features in right off, provided that it is done in such a way that the features are not adding to bloat.  Yes, the code base would increase, but that doesn't have to mean bloat.  If properly componentized, features could be installed based on need easily and not increase memory footprint.  Sensible, eh?"
    author: "ME"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-01
    body: "Just read the postings about StarOffice on Slashdot etc. and you will find out that a lot of people don't like StarOffice at all. There is not much difference between StarOffice and M$ office: both are huge packages, overloaded with functions nobody wants and full of bugs.\n\nThink about it: there are A LOT of people who like KOffice because it is not like StarOffice/M$ Office. IMHO the fact that StarOffice is now GPLed will not change that at all, at least not within the next two years. If you know something about programming you would know that it is not that easy to break something down in smaller pieces. All the bla bla about StarOffice now GPLed is IMHO bloatware: we will have to wait a loooong time before StarOffice is leaner at better than the old one ... and meanwhile KOffice is already there. Think about it.\n\nciao, Marco"
    author: "mk"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-01
    body: "I understand ME's point of view regarding the pooling of resources to provide a solid alternative to M$ products, a true \"choice\" in the *real* world.  Imagine how quickly we could produce high quality office products and rid ourselves of the M$ stranglehold if all of our talented minds and energies were channeled toward a common goal (management issues aside :-).  In this respect I agree with ME completely, but I won't get into the argument of whether that common goal ought to be StarOffice or KOffice.  (Heck, I'd throw a vote in for gnumeric and abiword!  I've always thought the concept of a \"suite\" of products was silly.  I'd rather pick and choose the best available in each category from any vendor.  Let the desktop people impress with me with killer integration API's to allow the best of the best to integrate if they so choose.)\n\nI can also see point that ME alludes to regarding the transition from M$ technologies.  It isn't going to happen overnight.  No one will want to be retrained.  No will want to discover 'nix on their desk one morning when they left good ol' Winders the night before.  I think ME is trying to say that in his world he can ease his users onto StarOffice on Winders in one step, and then (after a comfort level is reached) take the next step and run StarOffice on 'nix.  Not a bad approach towards transition.\n\nSo I hear what ME is saying, and the points others have made are completely valid as well.  And absolutely none of these comments detract from the good work that the KOffice folks are doing.  I guess the bottom line is that we are all _right_ in different ways.  I only encourage each of us to keep looking far into the future, trying to see the big picture, and focusing on the greatest benefit to our fellow man.  It's just too easy to get caught up in personal successes and egos only to let the Grand Ambition fade.  I personally feel that was the M$ went.\n\n--LaSarde--"
    author: "LaSarde"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-02
    body: "<P><EM>In order to\n       combat that, we should align ourselves and pooll the efforts behind the best and most mature code there is and right now\n       that is StarOffice. </EM>\n</P>\n<P>Do you have any idea what the StarOffice code looks like?  Can you assure it's not a convoluted codebase that would take months of learning to even begin to make constructive contributions (something virtually no open source developer will do).  Do you know how the API calls are tied into the code -- as a separate layer or sprinked throughout?\n</P>\n<P>If you have these facts, I'd like to hear the answers, as I don't know.  If you don't, try to refrain from making statements when you don't know what you're talking about.\n</P>"
    author: "Direct"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-02
    body: "It's great to hear that there will be a real alternative to StarOffice on the Linux platform.\nMany of the replies here seem to be more concerned about the effort which is being put into StarOffice at the moment. I think that it will probably be quite a good tool, very much a MS Office replacement, but it will be so compatible with MS Office that it is also quite interesting to have another tool (KOffice) which has a different view-point and different user interactions. This is called competition, and users are the ones who will benifit most from this as new forms of interaction can lead to improvements in UIs in general.<B><P>\nHowever I do hope that both StarOffice, Abiword and KOffice really try to work together on MSOffice\nimport/export functionality so that complete MS documents can be displayed/parsed properly by ALL of the GPLed office tools."
    author: "CP"
  - subject: "Why is the print quality bad in Koffice?"
    date: 2000-10-02
    body: "In the version of Koffice that came with kde-1.94, I found that the quality of printed equations is too bad for words. The equations look OK on the screen, but when printed, the characters look awful. They look like boldface fixed-width characters, oddly placed and disproportionate. Does anyone in the know have comments on whether this is due to user error, or because printing support is not quite there yet?\n<p>\n\nThanks,\n<p>\nMagnus."
    author: "Magnus Pym"
  - subject: "Re: Why is the print quality bad in Koffice?"
    date: 2000-10-04
    body: "I don't know for sure, but it sounds like a problem with true-type-fonts. Seems like it uses scaled fonts. They get ugly when you print them a bit larger than they are really: they don't get larger without loosing quality.\n\nJonathan Brugge"
    author: "Jonathan Brugge"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-02
    body: "I have read the comments above and I must admit that I am pretty surprised to see that most have missed the point. LaSarde mentioned the following as a comment to ME's rambling:<br><br>\n\n<strong>\"Imagine how quickly we could produce high quality office products and rid ourselves of the M$ stranglehold if all of our talented minds and energies were channeled toward a common goal\"</strong><br><br>\n\nMy question is, what would the result be? We would probably end up with another M$ wannabe that everyone would learn to hate. That is a great way to solve the problem :-(<br><br>\n\nIn my opinion, only CP has hit close to the truth and that is that the fight should be to try and get a unified format for saving documents, be it XML or something else. In that way we can use whatever product that is out there to create that document. Only in that way will be benefit from choice. I have absolutely no interest in being forced to use Staroffice no more than I like the idea of having to use M$ Word because it is \"standard\".<br><br>\n\nThe mindless chat about <strong>\"Koffice Sux\"</strong> or <strong>\"StarOffice is bloatware\"</strong> is counterproductive and should be left out of the GPL/Linux way of life."
    author: "DTT"
  - subject: "Re: German-Sponsored KOffice Meeting -- Report"
    date: 2000-10-05
    body: "I've installed the final Beta of KDE 2.0, KOffice included.  I've tried KWord, KPresenter and KSpread and my fist conclusions are:\n\n - KWord and KSpread are very useful tools that can compete with the MS counterparts, although a lot of small improvements in the near future are necessary\n\n - KPresenter should not be called a alpha version.  Compared to Powerpoint it lacks roughly all features that makes the difference between putting some text frames on a slide and a real presentation program.    I'll tried to remake an existing Powerpoint presentation (19 slides) using KPresenter and it took me more than 3 times as long.\n\nMy understanding is that most programmers uses text processing and a spreadsheet but hardly  make presentations.  As such, the presentation package lags really behind in functionality and user friendliness.  The fact that in the report about KOffice hackers meeting nothing was said about KPresenter is another indication in this direction.\n\nI hope that a few of the hackers spend some extra time on KPresenter.  Without that, the chances that KOffice will be success are severly limited"
    author: "Ruben Decrop"
---
The German Ministry of Education and Research (Bundesministerium f&uuml;r
Bildung und Forschung, BMB+F) recently sponsored a meeting of 16 KOffice
developers in Erlangen, Germany by paying their travel and lodging costs.
The meeting was held from September 23 - 25 in connection with the
Linux-Kongre&szlig;, a technically oriented Linux congress that is jointly
organized by German Unix Users Group (GUUG) and LiVe (Linux-Verband,
a non-profit organisation that fosters commercial use of Linux in
Central Europe).  Many of the
developers attended and two filed reports on the successful event.
And what do hackers do at a hacker conference, you might ask?  Read
more below to find out . . . .





<!--break-->
<p>
<A HREF="shaheedhaque@hotmail.com">Shaheed Haque</A> and
<A HREF="mailto:zander@earthling.net">Thomas Zander</A> filed this report
from the event:
</p>
<BLOCKQUOTE>
<P>
Erlangen was the first opportunity for the many of the new KOffice
developers to meet each other, as well as to meet some of the founding
fathers.  Erlangen proved a fertile breading ground for ideas, and
the resulting energy yielded good progress in several areas.  However,
given that the KDE head branch is currently in a feature freeze in
anticipation of the release of KDE 2.0 on October 16, many of the ideas
will have to wait until the freeze is lifted at the end of October.
At that point many ideas will percolate into the codebase.
</P>
<P>
First, KWord frame handling was improved and floating frame support
was added.  Tables generally work much better
now.  Even tables with variable-width columns are now handled when
imported from MS Word<SUP>TM</SUP>, as such imported tables use the new
floating frame support.
</P> 
<P>
Second, the MS Word filter now supports basic text styles such as bold,
italics, etc., in body text.  The filter also has now successfully imported
at least one embedded picture from one MS Word document (!!), but more work
is needed to handle the range of different Microsoft<SUP>TM</SUP>
embedding techniques. Finally, the architecture for enabling both import
from, and export to, MS Word has been implemented . . . all volunteers
welcome!
</P>
<P>
Third, the MS Excel<SUP>TM</SUP> import filter continued to evolve.
In particular, a lot of head scratching resulted in
<A HREF="mailto:percy@linuxfreak.com">Percy Leonhardt</A> adding support
for large tables, and extending support for various border options.
</P>
<P>
Fourth, the current KWord implementation will benefit from a
Rich Text widget under development by our
<A HREF="http://www.trolltech.com/">Trolltech</A> friends for Qt 3.0.
KWord will include beta versions of this widget when it reaches that
stage, which will greatly enhance our ability to deliver enhancements
to KWord in the future. It also helps that Thomas has taken on the
mantle of the KWord maintainer!
</P>
<P>
Finally, KSpread benefitted from serious improvements to i18n support. As
a side effect, there is a growing understanding of the interaction between
charsets, fonts, keyboards and file formats. We think we have identified
a complete solution to the fact that Unicode fonts are not yet universally
available . . . but more analysis is needed to implement it.
</P>
</BLOCKQUOTE>

