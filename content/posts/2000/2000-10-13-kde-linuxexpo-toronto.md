---
title: "KDE at LinuxExpo Toronto"
date:    2000-10-13
authors:
  - "wes"
slug:    kde-linuxexpo-toronto
comments:
  - subject: "Re: KDE at LinuxExpo Toronto"
    date: 2000-10-15
    body: "Shit!!!! My bride join more with KDE than with me( mmmm.... but if my \"instrument\" is big... she prefer me).\n\nLucky KDE"
    author: "Manuel Rom\u00e1n"
---
Cristian Tibirna <a href='http://lists.kde.org/?l=kde-devel&m=97140991118071&w=2'>writes</a>, <i>"David Faure and I will be present in the name of KDE at the LinuxExpo in 
Toronto from 30 October to 1 November 2000. [...] If you will be in the Toronto area in this period and you will be available 
for helping us present KDE in the booth kindly lended to us by the organizers 
or you want to meet us, let me know ASAP."</i>  More information on LinuxExpo Toronto can be found at <a href="http://www.linuxexpocanada.com/EN/">http://www.linuxexpocanada.com/EN/</a>.  If you're in the area and have some time to spare, consider making an appearance and giving them a hand!

<!--break-->
