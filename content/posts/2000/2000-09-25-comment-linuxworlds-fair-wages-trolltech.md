---
title: "Comment on LinuxWorld's Fair wages for Trolltech"
date:    2000-09-25
authors:
  - "numanee"
slug:    comment-linuxworlds-fair-wages-trolltech
comments:
  - subject: "Guess Trolltech's just been making a quiet living"
    date: 2000-09-25
    body: "It seems like what the article is saying is that Trolltech has been quietly making a living off of Qt already, and that any business it makes from any popularity of KDE is gravy.\n\nCool."
    author: "J. J. Ramsey"
  - subject: "Re: Comment on LinuxWorld's Fair wages for Trolltech"
    date: 2000-09-25
    body: "What I would like to know is why Mandrakes pre and post install tools are written using GTK when they're sponsoring a KDE developer (Mosfet).\n\nI've had a look at SuSE 7 recently, and their tools are written using QT. The fact that they rock is secondary to the fact that they're much better looking than Mandrakes.\nHmm....."
    author: "Coptain Cack"
  - subject: "Re: Comment on LinuxWorld's Fair wages for Trolltech"
    date: 2000-09-26
    body: "Who cares? Install tools should be written in something small and simple like FLTK ... the full GUI can be statically compiled into the app and still end up smaller than linked qt gtk kde gnome apps ..."
    author: "no one special"
  - subject: "Re: Comment on LinuxWorld's Fair wages for Trolltech"
    date: 2000-09-26
    body: "It's the POST-install tools that I'm making the pooint about. Most distributions now have a seamless and powerful install. Post install, when I fire up KDE, I expect to be able to re-configure my hardware (as I can with Windoze) using GUI tools which look and feel like KDE. That's my point.\nTo date, only Corel and SuSE offer me this.\nMandrakes post install looks and feels exceedingly clunky."
    author: "Coptain Cack"
  - subject: "Re: Comment on LinuxWorld's Fair wages for Trolltech"
    date: 2000-09-26
    body: "Oh my god ... it may just be that Trolltech enjoys success already and that making a good solid well doc'ed open product provides all the money and recognition they need. Maybe they're one of those simple little companies with a great workspace (I see hardwood floors and a sauna) and really don't want to take over the universe.\n\nI would like to work in their mail room and fall in love with a beautiful Norwegian XC ski goddess."
    author: "A nordic skiing fanatic ..."
  - subject: "Momentum?"
    date: 2000-09-26
    body: "Bah.\n\nI'd say that KDE, theKompany's plans, Borland's Kylix, Opera's browser, and Loki's apps is a fair amount of \"momentum\" for a company and toolkit to have.\n\nThat doesn't even count whatever announcements have been made involving Qt/Embedded.\n\nI don't see the Trolls starving anytime soon."
    author: "Neil Stevens"
---
Nicholas Petreley likes KDE2, he loves Konqueror, and he certainly doesn't hold back his praise for Qt and Trolltech.  In this <a href="http://www.linuxworld.com/linuxworld/lw-2000-09/lw-09-penguin_4.html">article</a> however, he worries about Trolltech's business model and isn't certain the numbers add up.  Personally, I wouldn't worry. Read on for why.




















<!--break-->
<p>
<a href="http://www.trolltech.com/">Trolltech</a> is obviously doing great with Qt, judging by the <a href="http://www.linuxtoday.com/news_story.php3?ltsn=2000-09-04-013-20-OS-LF-KE">huge growth</a> of the company and the number of quality hackers behind it. And Trolltech is doing all the right things with Qt/Embedded and the such -- as proof, yet another significant strategic alliance was recently <a href="http://alllinuxdevices.com/news_story.php3?ltsn=2000-09-22-002-03-PS">announced</a>, targeting no less than the Asian wireless market.   No, I'm not worried for Trolltech.  
<p>
KDE certainly does not lack corporate backing either.  SuSE, Caldera, Mandrake, Corel and countless other Linux distributors are all doing cool stuff for KDE and everything to back it.  There's Trolltech...  Then we have product companies such as theKompany.com and MieTerra/KDE.com quietly working on or funding cool projects of their own.  
<p>
Some of them just don't appear to shout loud enough, instead they content themselves with dropping subtle hints on the mailing lists.  <a href="http://www.thekompany.com/">theKompany.com</a>, for example, is doing some amazingly cool things behind the scenes (trust me), but has Nick Petreley heard of them?  Has anyone else?  Had anyone heard of <a href="http://linux.corel.com/">Corel</a>'s Qt Mozilla before I published the "leaked" info?  We need a little marketing effort here, folks. Observe and learn from our sister project.  Whatever.  Just get on with it!  ;)
<p>
As an aside, Nick seems to imply that KDE was founded by Trolltech.  That isn't true -- KDE was initially conceived and largely implemented by independent free software hackers.  Only later did Trolltech hire some of them.  And who wouldn't.



















