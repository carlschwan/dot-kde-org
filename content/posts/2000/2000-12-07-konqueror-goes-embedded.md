---
title: "Konqueror Goes Embedded!"
date:    2000-12-07
authors:
  - "Dre"
slug:    konqueror-goes-embedded
comments:
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-07
    body: "I think that this is an awsome idea, and the fact that its finished is making KDE get closer to every computer. One thing I am concerned about is the fact that this is the only program that has been ported to QT/Embedded from the KDE project. Point your browsers to the online CVS for KDE and you will notice that in kdenox (KDE NO X) Konqueror is the only thing there. I do understand that this is a step in the right direction and I hope that others soon to follow. Tronical: mabey you should write a howto or some notes on how you ported it, many would follow you, and that is what we need.\n\n    Thanks,\n       Error"
    author: "Error403"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-07
    body: "I guess everyone is waiting for Matthias Ettrich to initialize the development on KDE-NOX. History tells that this usually happens in form of an inconsiderable-looking USENET-posting. *hint* *hint*\n\nGreetings,\nTorsten Rahn"
    author: "Torsten Rahn"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-07
    body: "This is great news about Konqi, and could be the start of a full por of KDE to Qt/E.\n\nas for Mattias Ettrich giving the thumbs up, does he really need to say so? It would be great for him to start the ball rolling, but surely anyone could port any part of KDE (including kdelibs, kdebase etc) in theory?\n\nI think a full port of KDE away from X would be great, and I have been told by some coders that there is not a lot of KDE which relies on war X libs, and most requires purely Qt.\n\nCan anyone comment on the likelyhood of this? There seems to be a demand for it. :-)"
    author: "Jono Bacon"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-07
    body: "Jono, you got me -- I forgot the smiley. So here it is:\n\n:^)))"
    author: "Torsten Rahn"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-07
    body: "Hehe...smileys are good. ;-)"
    author: "Jono Bacon"
  - subject: "Video Card support etc., without X"
    date: 2000-12-07
    body: "How would KDE support video cards without X. Is there a way of just using those parts of X that pertain to video cards? What about the new anti-aliasing features? \n\nCan KDE really just canniblize X to create a lean, mean, Windows-eating machine?\n\nIf any of the developers could give the brodest outline of what it would take to create KDE-NOX, that would be great to read!\n\nSamawi"
    author: "Samawi"
  - subject: "Re: Video Card support etc., without X"
    date: 2000-12-07
    body: "I wonder if it would be possible to just use the video card drivers from Xfree 4.  Since they are now nice and separate from the server I would think that would be possible.  Of course you could only run KDE apps, but I think it would be really nice for trying to create a low-overhead GUI for things like kiosks and what not."
    author: "dirty"
  - subject: "Re: Video Card support etc., without X"
    date: 2000-12-07
    body: "Not really. You see, KDE wouldn't be making their own personal version of display drivers, rather, it would be using the internal framebuffer drivers that are loaded as part of the kernel. I'm not certain if FBDEV yet supports acceleration, but it runs in SVGA mode just fine."
    author: "Carbon"
  - subject: "Re: Video Card support etc., without X"
    date: 2000-12-08
    body: "the new kernel has its own accellerated drivers for a number of chipsets as well as a perfectly useful frame buffer. there ought to be some way to use kde right on top of the kernel -- which is, after all, already in a graphics mode.\n\nanybody know what modifications would be needed to bring this about?"
    author: "dep"
  - subject: "Re: Video Card support etc., without X"
    date: 2001-05-10
    body: "I haven't worked on KDE development much so don't know about the kdelibs and other libraries in KDE and there dependency on X.\n\nHowever yestarday I was looking into qt/embedded(qte), based on my preliminary analysis there is a kernel directory within qte/src, the qgfx*.* files should give anyone a rough idea of how qte provides a flexible graphics layer.\n\nRoughly:\n1) One could use the Framebuffer driver in Linux kernel for their target to work with qte because qte already as support for Linux framebuffer.\n\nOR\n\n2) One could implement ones own graphic drivers by inheriting from qScreen for the framebuffer part and from qGfxRaster for the Drawing primitives."
    author: "HanishKVC"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-08
    body: "KDE-NOX would be nice, except that by abandoning X, we lose the use of all X applications!\n\nSure, GNOME may not have much now, but what if they come up with some killer app later?\n\nOh, wait.  Even if GNOME did come up with one, I probably wouldn't install the GNOME libs anyway.  Never mind. :-)"
    author: "Neil Stevens"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-07
    body: "\"compiling them in a new environment, an environment without X11 and without kdelibs.\"\n\ndoes this mean, that konqueror compiles without the kdelibs?\nhow does it work?\ni can't believe it's so simple to make konqueror a non-kde application? (but it would be great :))\nwhere can i find more about it?"
    author: "Spark"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-07
    body: "This could be also great for old computers with low memory."
    author: "Henning"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-09
    body: "That's true. Handheld computers shouldn't be the only target group for ports of KDE stuff to Qt/embedded. Linux/Qt/embedded seems to be the perfect platform for thin clients. Install the browser and a couple of small-footprint client programs on the local machine and use http or XML-RPC or whatever to just communicate the data, not the graphics ..."
    author: "Joachim Werner"
  - subject: "Re: GUI suggestion"
    date: 2000-12-07
    body: "Here's a small GUI suggestion:\n<P>\nGet rid of the scroll bar on the right.  Since real estate is so tight on PDAs, it might be wiser to have the two scroll bars next to each other on the same horizontal line.\n<P>\nEg:\n<P>\n[<]===[>] [^]===[^]\n<P>\nThe second [^] would be the down arrow.\n<P>\nAnother further improvement would be to create custom scroll bars that are about 1/2 or 2/3 the width of the default ones, in order to squeeze out more screen space.\n<P>\nJust my 2c.\n<P>\n-Niek"
    author: "NJS"
  - subject: "Re: GUI suggestion"
    date: 2000-12-07
    body: "Excellent idea!\n\nSo one would loose a bit of height but get more\nwidth?\n\nOh, and your idea of making the scrollbars thinner\nis definitely great! I'll see if that is possible.\n\nThanks for the comments!\n\nBye,\n Simon"
    author: "Simon"
  - subject: "Re: GUI suggestion"
    date: 2000-12-07
    body: "<p>As a user of Psion palmtops for some years, I see some things that can be improved. Symbian (Psion/EPOC developers) has published an excellent style guide: <a href=\"http://www.epocworld.com/techlib/documentation/ER5/OPL/sysdoc/techpaper/styleguide/styleguide.html\">EIKON Application Style Guide</a>. This document sums up <em>many</em> years of experience in GUI design on small devices.</p>\n\n<p>I'd say the vertical scrollbar is the most important to keep, as it would be hard to quickly jump up and down without it. A more important change is to move the arrow buttons to one end (or both ends) of the scrollbar (like in some KDE themes). This makes it much easier to operate with the pen (most devices will probably have a touch screen). There can be a similar button pair for horizontal scrolling, but without the scroll bar.</p>\n\n<p>Some things are unnecessary to keep on the screen. The menu should be hidden, accessible by a button (has worked nicely for ten years on Psion PDAs). Toolbars should be simple (like on the screenshots you've shown). All buttons should also be large enough to operate with a finger.</p>\n\n<p>Regards,\n<br />&nbsp;Gaute Hvoslef Kvalnes</p>"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: GUI suggestion"
    date: 2000-12-07
    body: "Get rid of the scrollbars completely, use the hand cursor instead, as have been used on Macs for ages.\n The Windows image-viewer ACDsee uses the hand cursor exclusively and have no scrollbars at all!\n\nThis should be an option in KDE as well."
    author: "reihal"
  - subject: "Re: GUI suggestion"
    date: 2000-12-07
    body: "Ah, excellent idea. Hand cursor == small gameboy like joypad thingy, right?\n\nHmm, I wonder if Qt supports it? :-}\n\nHmmm, I'll probably just make it an option in the menus to turn off the scrollbars (combined with the idea someone else here brought up: make the menubar a toggable item, not visible all the time)\n\nBye,\n Simon"
    author: "Simon"
  - subject: "Re: GUI suggestion"
    date: 2000-12-07
    body: "This probably needs a hack of Qt.\n\nAnother idea is to make scroll- status- and tool-bars auto-hide, like the taskbar in Windows and the Panel and taskbar in KDE. They pop-up when you place the cursor at the edge of the screen.\n\nThe hand-cursor is used in all versions of Acrobat Reader as well, maybe it was old Aldus that started it. This could be applied to all apps, both regular KDE and embedded, you just shove the contents of a window or frame around while holding down a mouse-buttton."
    author: "reihal"
  - subject: "Re: GUI suggestion"
    date: 2000-12-08
    body: "the \"hand-cursor\" would make it impossible to drag-select text for cut& paste.\n\nThumbs down!"
    author: "Ross Campbell"
  - subject: "Re: GUI suggestion"
    date: 2000-12-08
    body: "It would be selectable, don't worry."
    author: "reihal"
  - subject: "Re: GUI suggestion"
    date: 2000-12-07
    body: "The hand cursor is not really like a gameboy pad if I understand correctly.\n\nThe hand cursor is what you get when you hold the spacebar down on adobe products like illustrator, acrobat and photoshop. The idea would be that once you touch the screen you take mouse coordinates and pan the page you are viewing based on how you move the mouse. The part of the page you click on stays under the pen tip.\n\nI believe AvantGo's web browser for the palm works this way.\n\n-pos"
    author: "pos"
  - subject: "Re: GUI suggestion"
    date: 2000-12-11
    body: "Yes, AvantGO works this way, and it is quite functional. However,\nit is difficult to scroll down many pages at once (\"I want to get to the bottom of this damn thing!\").\n\nHaving the option of hidding the scroll bars is quite useful,\nespecially if combined with some sort of \"scrolling device\",\nlike the \"Up-Down\" buttons on Palms (is there support for such thing in Qt-embedded?).\n\nJoystick-like devices are very good when you *need* diagonal movement (ex. games). On a PDA I'd rather have 2 rolling-devices\n(I don't know how you call it in english, you know like the\nold style buttons used in radios to manage the volume).\nOne at the top of the device (I could use it with my thumb) to scroll horizontally and one at the left side (where the pointer usually is) to scroll vertically...\n\nPanayotis."
    author: "Panayotis Vryonis"
  - subject: "Re: GUI suggestion"
    date: 2006-01-21
    body: "What KDE needs is a more general configurability of things like scroll bars.\nFor example, they get a bit thin on a 21\" monitor or larger. Fishing for the small target area with a mouse can be a pain.  Seeing it well can be a pain. Which a lot of people will experience as the grow older, as I am finding out.  And for some who have motor-coordination problems, being able to double a scroll bar's size would be useful.  Being able to set colors would be good too, for those with vision problems.  Generally speaking all around configurablity should be an aim in future KDE systems.  As technology goes on we will have Linux on stuff from phones to PDA to 10\" sub-portables to 32\" screens and 42\" home media centers, capable of monitor Display.  One might envision future KDE systems with a set of preset configuations from PDA to large monitors, to widescreen laptops etc.  Pick one which gets you in the ball park and then configure anything not quite right.  A large screen might be set with wider scroll bars.  It would be nice to have it sort of like Opera where you left click an item and pull up a configuration dialogue.  Hit a scroll bar and set it 2 x normal size and make it light blue.  And it would be nice to have profiles for more than one user on a machine. A big plus in a business setting. Or a one computer family situation. It would make it easer for somebody with several machines to coordinate uniform setups across machines. "
    author: "William C. Barwell"
  - subject: "Re: GUI suggestion"
    date: 2000-12-07
    body: "To go a bit further Simon,\n<BR><BR>\nThe location toolbar is not stricly necessary in this form factor either. My guess is that usability studies would show that pda's aren't used for browsing so much as they are for checking known (and thus bookmarked) sites.\n<BR><BR>\nThe status bar can show the current location just as well as the url text box, therefore making it somewhat redundant. A toolbar button that brings up the url entry would be sufficient in the case that the user would want to navagate to a new site.\n<BR><BR>\nOne could even go as far as maing the status bar only a toolbar button that brings up a tooltip on mouse over. (pen over?) What about a fullscreen mode?<BR><BR>\noverall, great work!\n<BR><BR>\n-pos"
    author: "pos"
  - subject: "Re: GUI suggestion"
    date: 2000-12-07
    body: "I would go even further and say that the title in the titlebar is not really necessary, and could be replaced by what is now the status bar/current status bar. Or at least on mouse-overs ?"
    author: "cph"
  - subject: "Re: GUI suggestion"
    date: 2000-12-07
    body: "<p>Oh, there is a titlebar? Totally unnecessary as a window shouldn't be resizable at all. The current application is always maximized on a PDA (except for dialog boxes).</p>\n\n<p>And mouse-over effects are not possible on a PDA, as you most often don't have a mouse but a touchscreen.</p>\n\n<p>Regards,\n<br />&nbsp;Gaute Hvoslef Kvalnes</p>"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: GUI suggestion"
    date: 2000-12-07
    body: "The titlebar I can't remove that easy, because it's what the Qt windowing system provides (well, I probably could remove it, but I'll better leave that to Qt ;-)\n\nYou are definitely right on the statusbar issue. I will probably remove it then and implement \"pen-overs\" as tooltips (so that when you hover over a link the url gets displayed as tooltip, not in the statusbar.\n\nBye,\n Simon"
    author: "Simon"
  - subject: "Re: GUI suggestion"
    date: 2000-12-07
    body: "<p>&quot;Pen-overs&quot; are <em>impossible</em> - the touchscreen doesn't know where the pen is until you tap it. What you describe is implemented as follows in Psion Web: Tap once, link is displayed in a corner of the screen. Tap again, link is followed. These corner messages are Psion's variant of status bars - another great way of saving screen space.</p>\n\n<p>Regards,\n<br />&nbsp;Gaute Hvoslef Kvalnes</p>"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-07
    body: "well.. i planned to write this posting with konq/embedded but unfortunetly it hangs everytime i click on \"add\" :)\nbut other forums work great.\njust wanted to say i really like it!\ni use it with x11, though.\nit's the fastest browser i have ever seen (in startup and in rendering) and has even a loweder memory footprint than opera.\nthis just needs some minor tweaks to become my new desktop browser of choice.\nthis could really be a killer application, especially for all those non-kde users out there!\ni really really hope this won't be embedding only :) it's even great for the desktop.\nthis is EXACTLY what i was waiting for!"
    author: "Spark"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-07
    body: "> unfortunetly it hangs everytime i click on \"add\" \n\nThat is a bug in current CVS. Why didn't you post your screenshot?\n\nhttp://home.wtal.de/borgmann/konq.png"
    author: "anon"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-07
    body: "thx, whoever you are :)\n\ni posted the link in my first two tries and than forgot it ;)"
    author: "Spark"
  - subject: "Whoever s/he is, s/he uses phony e-mail"
    date: 2000-12-07
    body: "Anon used my e-mail address, and so I got e-mail confirmation of this reply.  I have no idea what you people are talking about, but good luck with it."
    author: "The real Onan"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-08
    body: "So etwas hab ich auch schon gesucht, gibt es schon fertige bin's?"
    author: "Max"
  - subject: "QT/E compiling troubles"
    date: 2000-12-07
    body: "I actually never mentioned this, even though I always wanted to try out kdenox. I'm having trouble recompiling QT/Embedded (at the moment I don't have the error) with differen compilers (gcc-2.95.2 and egcs-2.96).\nAnything I should be aware of?\nI mean I think I have all the requistes (fb set up properly..)\n\nTack."
    author: "Anonymous"
  - subject: "Less is more"
    date: 2000-12-08
    body: "On a small screen it's very important to make as much space available for the viewing pane, by mininising the interface. The interface can be minimised by makeing thnigs smaller, hiding them until needed or getting rid of them all together.\n\nAs an example of the latter, get rid of the tile button. The screen is too small to support overlapping windows.\n\nAs suggested by others, the scroll bars should be half the size or less, if they are to be static, or a pop up option. \n\nThe URL location box should be hidden, probably brought up with a button on the button bar. The title bar probably should be removed or made optional.\nThe button bar should be shrunk a little. The Words File and Bookmarks should be replaced by buttons so that all the buttons can fit on one row (including shut and minimise). Actually, you could do without minimise.\nOn a PDA, the menu bar is usually hidden, and brought up with a hardware menu button.\n\nIf you have never used a palm pilot, you should try to get a hold of one, at least for a few weeks. This is the best example of a minimalist design that I know of - it's amazing. I read that the developers actually measured how many pen taps/strokes were needed for every possible action that could be performed in their program suite and tried to minimise this without making the interface ambiguous or confusing.\n\nChris"
    author: "Chris Wise"
  - subject: "Re: Less is more"
    date: 2000-12-10
    body: "Yes this is some top advice IMO.  I aggree that the text menus should be replaced with small icons and could be identified via tooltips.  An interface similar to Internet Exploiters fullscreen mode would be good, ie a nav bar the optionally autohide navbar, and a stop button that pops up only durning network activity would be a good one same with the forward/back, if you cant go forward, why have a button.."
    author: "Amibug"
  - subject: "Re: Less is more"
    date: 2006-05-30
    body: "Are you refering to InternetExploiters.com?"
    author: "Joe H"
  - subject: "rotate 90 degrees?"
    date: 2000-12-08
    body: "Does anyone else think the browser should\nbe wider than it is long?  I noticed the\ndemo QT/Embed apps could roate themselves - \ncan Konqi do that?"
    author: "steve"
  - subject: "Re: rotate 90 degrees?"
    date: 2000-12-08
    body: "konqueror -display Transformed:Rot90:0"
    author: "Martin Jones"
  - subject: "Re: rotate 90 degrees?"
    date: 2003-03-22
    body: "  I have a horizontal picture on my digital display in documents.  How do I upright this picture so it is correct vetically?"
    author: "Alpeachm"
  - subject: "Re: rotate 90 degrees?"
    date: 2006-02-22
    body: "U have to have a picture editor then open the picture with it. There should be options to rotate the picture and just select that. Once you exit the picture make sure you save the changes."
    author: "Anthony F"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-08
    body: "Excellent- I've been waiting for an easy way of fitting Konqueror onto my linux distribution \"JAILBAIT\" specifically for the I-Opener... and I had not yet heard of QT-Embedded until I stumbled across this...\n\nThank you, Simon! I'm really happy to hear about projects like this!\n\n-Jeff"
    author: "Jeff Baitis"
  - subject: "Koffice Embedded?"
    date: 2000-12-08
    body: "Great!, now if only we had Kword and Kspread embedded , we would REALLY be cooking!"
    author: "t0m_dR"
  - subject: "Re: Koffice Embedded?"
    date: 2000-12-08
    body: "You are soooo right!"
    author: "anonymous"
  - subject: "https"
    date: 2000-12-08
    body: "how do i enable https?\nif i want to reach a secured site it says something like \"The protocol https is not supported\". do i have to use some special compiler flags do enable ssl?\nbtw it's working for me with \"real\" konqueror, so all necessary libraries should be installed."
    author: "Spark"
  - subject: "Re: https"
    date: 2002-11-04
    body: "from http://lists.debian.org/debian-kde/2001/debian-kde-200109/msg00098.html\n\n<<\n> I get the message:\n> \"The protocol https is not supported.\"\n>\n> I think there might have been some message about crypto, perhaps\n> mentioning that I needed to do something else to get it, when\n> I installed KDE, but I don't recall.\n\nYou probably need kdebase-crypto and kdelibs-crypto.  You might need to add a \nnon/US entry in your apt sources to get those.\n>>"
    author: "meyou"
  - subject: "Re: https"
    date: 2006-11-10
    body: "HI...\n\ni'm also facing the similar problem.when i browser in the konqueror/embedded web browser. i got the error \"the protocol https no supported\".can you say what do you mean by \"You might need to add a non/US entry in your apt sources to get those\".Should i give any option while building inorder to activae it...? or what should i do inorder to get that working...?\n\nthank in advance..\n\nsaravanan"
    author: "saravanan"
  - subject: "Re: Konqueror Goes Embedded! (GUI suggestions)"
    date: 2000-12-09
    body: "Very cool!\n\nRegarding the GUI thing: It seems that the main problem is that websites just are not \"320x240\" and also not \"240*320\" as your screenshots (or an iPac) would display them.\n\nI support the other suggestions (rotating, minimizing the widget size etc.) and add a new one that might be a bit harder to acomplish:\n\nWhat about a \"zoom\" mode? A lot of links can still be guessed at half the size, so this would give us almost the same usability as a full 640x480 screen ..."
    author: "Joachim Werner"
  - subject: "320x240 GUI"
    date: 2000-12-11
    body: "Hi,\n<p>As someone said it's not much to do about websites. Allowing users to pick unreadable tiny fonts and scale images to 50% might make some people happy. Removing all status, url and iconbars should get some screen estate - but I'm sure Konqueror allows this already.</p>\n\n<p>I tried KDE 1.0 on my WinCE can using VNC. It worked wonderfully with menus and window, especially with a tiny font in the shell.\nJust one thing made it unusable.</p>\n\n<p>Opening a file dialog crammed a 400x400 window on the screen and it became impossible to reach the \"OK\" button. Are there any solution for these problems in KDE 2.0? I've heard about tiny icons - which would be great, but are there any way to resize or scroll dialogs to view all the window?\n(For example linuxconf are unusable even in 640x400 because of a large unresizable window!)\nI'd love to put XMMS on top on my stereo...\n</p>"
    author: "Patrik Carlsson"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-14
    body: "<b>Some problem on Strong ARM platform</b>\n      I compiled konqueror/embedded for Assabet (Strong ARM's platform) used cross-compiler,\n   but have some problems.\n      1. First I compiled QT/embedded and set configure to static library enable, then compiled\n   konqueror/embedded and set configure to enable static, disable shared. When configuring\n   the environment I found it check X library (Xext, X11), why it do this. It found my PC (x86)\n   /usr/X11R6/lib/*, but I used cross-compiler for Strong ARM. When configure finish and make\n   completed, I copied all files to my Strong ARM platform then running, I got a error message\n   \"Segment fault\".\n      2. Now I rebuild QT/embedded and set configure to shared library, then compiled konqueror/\n   embedded and used default setting to make shared library. It always check X library, when make\n   completed and ready to run. I got a error message following.\n      BUG IN DYNAMIC LINKER ld.so: ../sysdeps/arm/dl-machine.h: 488\n      elf_machine_lazy_rel: Assertion ((reloc->r_info) & 0xff) == 22 failed!\n   Why? Is the X library error? (Link X is not make sense)\n      Could you help me, thank you for your help.\n\n\nBest Regards,\nLion Nung"
    author: "Lion Nung"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2000-12-14
    body: "Thank you for the time you spent on your article.\nThe reason I am reponding is because I am looking for information and an opinion on an idea.\nThis small type of linux is something I would like to do on a older 486 laptop or pc(Almost like the $200 web terminals you see at Compusa). I would not be doing it for profit, I donate time for orignizations that use 486 systems.  With this type of small os the user gets email and browse the web(all in GUI).  A very simple terminal.  This embedded technology seems like a good way for a simple low footprint, easy to install, easy on the resouces OS? That even 486 could run fine?\nAnyway that is the idea, an opinion would be appreciate if you think it makes sense.  The other thing is some help with my research so far www.trolltech.com and their QT for X11 in something I am keeping in mind and this KDE browser.\nThanks again for your time,\nAlfred Nutile"
    author: "Alfred Nutile"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2001-01-10
    body: "I have compiled Konqueror to a Tynux-Box which is a Qt/Embedded capable device. I think Konqueror has some dependency of X11, so I have modified some configurations and hand-compiled some parts. But it works. \n Tynux-Box has a Intel StrongARM SA1110 and 320x240 8bpp LCD and I have compiled with Tynux's Cross Compiler.\n Have a fun with a attached real snapshot."
    author: "HeeGoo Han"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2001-02-16
    body: "If you could post what you did to get such an excellent result, I would be very grateful.\n\nMy unit is also a StrongARM SA1100, but with 640x480 8bpp LCD.  All of the QT/Embedded sample applications work perfectly.\n\nIf I load kde.org, the right scroll bar shows up, then goes away as the page continues to load.  The blue box below the linuxworld logo doesn't show up, and the \"KDE is a powerful open source desktop...\" page doesn't show up.\n\nI've tried QT Embedded 2.2.3 and 2.2.4, and konq-embedded-0.1 and a current snapshot from 2/14.  \n\nA picture is worth 1000 words, so I've included a picture.  Any help would be most welcome.\n\nThanks,\nRich Dulabahn\nrich@accelent.com"
    author: "Rich Dulabahn"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2001-02-16
    body: "If you could post what you did to get such an excellent result, I would be very grateful.\n\nMy unit is also a StrongARM SA1100, but with 640x480 8bpp LCD.  All of the QT/Embedded sample applications work perfectly.\n\nIf I load kde.org, the right scroll bar shows up, then goes away as the page continues to load.  The blue box below the linuxworld logo doesn't show up, and the \"KDE is a powerful open source desktop...\" page doesn't show up.\n\nI've tried QT Embedded 2.2.3 and 2.2.4, and konq-embedded-0.1 and a current snapshot from 2/14.  \n\nA picture is worth 1000 words, so I've included a picture.  Any help would be most welcome.\n\nThanks,\nRich Dulabahn\nrich@accelent.com"
    author: "Rich Dulabahn"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2001-02-21
    body: "I installed konqueror on the IPAQ. At first, I got the same problem with you.. Scrollbars disappeared.\nI found that the problem was executing 'konq' directly instead of 'konqueror' script file.\nWhen I executed 'konqueror', the program died with seg fault, but scrollbars were still there.\nMy solution was copying the whole directory from flash rom to ramfs. \nMy guess is there are some routine which requires to write a file, and it crashes when it fails to write on a cramfs file system.\nI'm going to inspect the source code and find the directory which should be moved to ramfs.\n\nRegards.\nOsok"
    author: "Osok Song"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2001-02-22
    body: "Thanks for the tip.  The platform I'm using is pretty close to an ipaq, but has an IDE hard drive attached, so I have no flash or rom type file systems.  I tried what you suggested and get an identical segfault.  Looks like this when you load a page:\n\nSegmentation fault\ndebug: SlaveBase: read() error! exiting!\n\nIs this the error you are seeing as well?\nRich"
    author: "Rich Dulabahn"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2001-02-22
    body: "Almost as soon as I posted my last message, I found the problem and fixed it.  Here's what worked.  I grabbed the konqueror cramfs for the iPAQ to study, and went into the /konqe directory.  It held the konq binary, the konqueror script, and a directory called share.  I tarred the share folder, and copied it to my source tree, in konqueror-embedded-snapshot/konq-embed/src.  Then, I ran konqueror with this command:  ./konqueror -qws and everything worked fine.  I've included the tarfile to save time.  Hope this helps."
    author: "Rich Dulabahn"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2001-02-07
    body: "Hmmm, what about that neat shaded mouse cursor? Wouldn't that be nice to have on desktop QT? I have actually seen people use that in arguemnts of WinME vs KDE."
    author: "Carbon"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2001-02-07
    body: "Hmmm, what about that neat shaded mouse cursor? Wouldn't that be nice to have on desktop QT? I have actually seen people use that in arguemnts of WinME vs KDE."
    author: "Carbon"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2001-03-14
    body: "damn, instead of replying i posted a new article. oh well but now i should be right.\n\nI have a problem with konquer over framebuffer for i386. I downloaed the latest cvs snapshot (but tried the v 0.1 too) and compiled it. I could run the browser, but all pages containing tables are screwed up. I read around konquer works almost perfectly over the framebuffer, so i think i'm missing something.\n\nAnyone had same problems? Any clue about a solution?\n\n(i've used qt/e 2.2.4 to compiled the browser)\n\nthank you for your help."
    author: "Cristian Prevedello"
  - subject: "Re: Konqueror Goes Embedded!"
    date: 2001-04-03
    body: "Yup I had the same problem.  See my post above about the \"share\" directory.  That should fix it.\n\nRich"
    author: "Rich Dulabahn"
  - subject: "where can i get some documents"
    date: 2001-10-30
    body: "Hi,\nwhere can i get some documents about konqueror about qt/embedded?\nMay I directly cut off kdelibs."
    author: "Yang Zhiwen"
  - subject: "Not able to download"
    date: 2003-07-09
    body: "When i tried downloading the tar file i am getting an error message as given below\n\nThe requested URL /~hausmann/konqueror-embedded-0.1.tar.gz was not found on this server\n\nCan anyone give the link from where i can download the file.\n\nThanks\n"
    author: "Erwin"
  - subject: "Re: Not able to download"
    date: 2003-07-09
    body: "Be creative and search http://devel-home.kde.org/~hausmann/ ! Ancient 0.1 is in Attic/ and newer versions are in snapshots/"
    author: "Anonymous"
  - subject: "Building konqueror on qt/embedded"
    date: 2004-11-17
    body: "Hi all,\n \nI have built konqueror on Qt/embedded but when i run\nthe konqueror executable i am getting the error like\n \n$./konqueror -qws\n \nthe error is \n \ncan't open frame buffer device /dev/fb0..\n \nDoes any body had this problem earlier and if you have resolved it please\nlet me know the solution.\n \nRegards\nSatish"
    author: "sathish"
  - subject: "Re: Building konqueror on qt/embedded"
    date: 2006-02-21
    body: "Dear sathish,\n\nI am developing user interface for a ultrasound scanner.I am using qt/embedded-2.3.10 version.I had cross compiled it for my target system and the application is working fine on the target.\n\nRight now i am looking to cross compile konqurer/embedded for my target system which has a arm processor,could u plz guide me how to do it.\n\nI am enclosing the error msg plz go through it.\n\nRegards,\ngunna vishnu"
    author: "gunna vishnu"
  - subject: "Re: Building konqueror on qt/embedded"
    date: 2006-04-29
    body: "Hi,\nThe issue is that you are missing to tell the path of the libz.so library.\nThis should be given by '--with-extra-libs=/<path to your libz.so>' wne running the configure script.\nExample:\n\n$./configure --target=arm-linux --with-extra-libs=/home/project/zlib-1.1.4\n"
    author: "tharmarajan"
  - subject: "Qtopia desktop support for konqueror"
    date: 2006-08-07
    body: "Hi.....\n\ni am using Blackfin DSP processor(BF537 ezkit) connected with SHARP LCD LQ34 series.i need to port knoqueror embedded into it. i had compiled qt/Embedded and konqueror3 embedded web browser. my doubt is whether should i compile Qtopia desktop environment also for viewing the konqueror web browser output in the LCD or compiling Qt/embedded and konqueror alone is enough...?can anyone give me suggestion or solutions for this...?\n\n"
    author: "saravanan"
  - subject: "I need a konqueror embedded browser based on QT4 a"
    date: 2009-01-22
    body: "HI hausmann,\n    I am looking for a latest Konqueror embedded browser for my \nEmbedded system which is running debain embedded Linux kernel 2.6.11 which is a non X11 system.\nLatest KDE available in market is version 4.1 so I want a Konqueror browser\nBased on  KDE4 libraries and Qt embedded 4.4.0.is it possible to integrate \nKDELIBS and KDEBASE components to develop a Konqueror 4 embedded browser.\nFrom ROAD edition, I able to get konqueror/kdenox based no Qt3 not for latest Qt4 version.\nPlease help me on this.\n\n\nRegards\nSenthil kumar R  \n"
    author: "Senthil kumar Raju"
---
Our friend <A HREF="mailto:hausmann at kde.org">Simon Hausmann</A> wrote in with some exciting news about KDE's excellent browser <A HREF="http://www.konqueror.org/">Konqueror</A>:  it's been ported to <A HREF="http://www.trolltech.com/company/announce/embeddeddl.html">Qt/Embedded</A>.  He says:  <EM>"I just uploaded a first version of a port of the
Konqueror webbrowser to Qt/Embedded, Konqueror/Embedded.  The project aims to build a feature complete version of the webbrowsing
component of Konqueror for Qt/Embedded, including support for SSL, Cookies,
Javascript, non-blocking IO, HTML4/CSS, etc."</EM>  Way to go, Simon!  Simon has only been able to test this port on a virtual framebuffer -- if you can donate a Qt/Embedded-enabled palmtop device for Simon to perfect his work, please contact him!




<!--break-->
<p><STRONG>Simon wrote:</STRONG></p>
<p>
I just uploaded a first version of Konqueror/Embedded, a port of the
Konqueror webbrowser to Qt/Embedded, to
<a href="http://devel-home.kde.org/~hausmann/konqueror-embedded-0.1.tar.gz">http://devel-home.kde.org/~hausmann/konqueror-embedded-0.1.tar.gz</A>.  The project
aims to build a feature-complete version of the webbrowsing
component of Konqueror for Qt/Embedded, including support for SSL, cookies,
JavaScript, non-blocking I/O, HTML4/CSS, etc. Well, basically almost everything
the "big brother" can do, with the main difference that it consists of only
one static, small binary (plus some small data files). The status of development is that it is almost feature complete (http
authentication caching is missing). There are a few bugs left and some
cosmetic changes in the GUI.
</p>
<P>
Here are some screenshots:
</p>
<CENTER>
<IMG SRC="http://master.kde.org/~hausmann/konqe1.png" WIDTH=244 HEIGHT=349 VSPACE=8 HSPACE=8>
<br>
<IMG SRC="http://master.kde.org/~hausmann/konqe2.png" WIDTH=244 HEIGHT=349 VSPACE=8 HSPACE=8>
<br>
<IMG SRC="http://master.kde.org/~hausmann/konqe3.png"
WIDTH=244 HEIGHT=349 VSPACE=8 HSPACE=8>
<br>
<IMG SRC="http://master.kde.org/~hausmann/konqe4.png" WIDTH=244 HEIGHT=349 VSPACE=8 HSPACE=8>
</CENTER>
<P>
Depending on the compiler/binutils used the size of a stripped binary is somewhere between
2.1 and 2.8 megabytes.
</P>
<p>
Please note that this is <EM>not</EM> a fork of the original Konqueror. The portis realized by copying the unmodified original sources (this is in fact
the very first step in the build process) and compiling them in a new
environment, an environment without X11 and without kdelibs. This means that
Konqueror/Embedded always automatically gains from the latest bugfixes
and improvements of the KHTML rendering engine, the HTTP KIO slave and
other sources by using/compiling the original sources directly!
</p>
<p>
The software compiles and runs on Qt/Embedded as well as on Qt/X11. The
support for Qt/Embedded has only been tested in the virtual framebuffer on a
PC, because I don't have the hardware for "real life" testing.  So if anyone has a Qt/Embedded capable device and is looking for a
webbrowser please drop me a mail :-). I would love to know if this
software really works :-).
(I'd be happy to help out with any compilation/run-time problems, as far
as that's possible remotely)
</p>
<p>
In general I'd be happy about any feedback, especially with regard to the GUI
(for small resolutions like 320x240).
</p>
Bye,<br>
&nbsp;Simon



