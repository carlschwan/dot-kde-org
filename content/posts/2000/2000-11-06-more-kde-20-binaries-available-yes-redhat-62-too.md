---
title: "More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
date:    2000-11-06
authors:
  - "Dre"
slug:    more-kde-20-binaries-available-yes-redhat-62-too
comments:
  - subject: "Ow ? Where ?"
    date: 2000-11-06
    body: "Is it me or aren't there any kde 2.0 binaries following the link (\"here\") ?\n\nBesides that, the link mentioned in the list of kde 2.0 binaries available seems dead."
    author: "Jelmer Feenstra"
  - subject: "Re: Ow ? Where ?"
    date: 2000-11-06
    body: "Darn right, the binaries are not there.  Is somebody going to fix this problem, or were they vapour to begin with?"
    author: "Karl Schroeder"
  - subject: "Re: Ow ? Where ?"
    date: 2000-11-07
    body: "Vapour? They programmed an entire desktop system. I could be wrong, but I think you have never even coded a 'Hello world' program."
    author: "David"
  - subject: "Re: Ow ? Where ?"
    date: 2000-11-06
    body: "For some reason, they linked to Sourceforge.  The official KDE.org ftp site has them."
    author: "Kevin Breit"
  - subject: "Slackware available as well"
    date: 2000-11-06
    body: "Slackware binaries are available as well under slackware-current."
    author: "David Johnson"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-06
    body: "Anyone got binaries for Solaris ?\nOr know where I can get them ?\n\nThanks\nCP"
    author: "cph"
  - subject: "Solaris binaries"
    date: 2000-11-06
    body: "I was in touch with Luc I. Suryo, who made the binaries for KDE 1.1.2 and Qt-1.44, and he told me that he should try to have the binaries for KDE2 and Qt 2.2.1 done by the 2nd or 3rd week of November."
    author: "Mats Eriksson"
  - subject: "Re: Solaris binaries"
    date: 2000-11-07
    body: "I am working directly with Luc Suryo to get the solaris binaries out....I have actually fixed all of the bugs in the Solaris binaries and am pkging the x86 ones up now. They are ready to be shipped off to ftp. They should be available via ftp.kde.org or ftp.patriots.org in the next day or so."
    author: "dcranford"
  - subject: "Re: Solaris sources and binaries"
    date: 2000-11-07
    body: "I am wondering if you would be kind enough to look into the following comments regarding compilation of kde on Solaris 8.\nI have tried to compile kde-2 on Ultra 10\nrunning Solaris 8.  I had 3 problems, 2 I fixed\nmyself but can not do the third.\n1)  kdenetwork-2.0\nThere is a problem with /var/utmp.  Solaris 8 \nuses /var/utmpx not utmp.  I made a link utmp\nto utmpx, then it compiled fine.  But there may be \na better way.\n\n2) kdemultimedia-2.0\nThere is a problem in kmidi/TIMIDITY. The file \nmakelinks contains links 'ln -sf' to files in \nparent directory. But Solaris 8 does not like the\nlink options 'ln -sf'.  But it works with the \nmodified options:  'ln -f -s' .  It compiled fine.\n \n3)  koffice-2.0.  I still have a problem compiling.  After running make for some time, I get the following error:\n------------------\n\nattached\n\n------------\n\nThank you very much."
    author: "Chris Papachristou"
  - subject: "Re: Solaris binaries"
    date: 2000-11-15
    body: "Hi,\n\nI've seen binaries for Solaris 8 are available on ftp.kde.org. Unfortunatly I'm using Solaris 2.6. Any chance there will be one day binaries for this version ?\nI've actually tried to compile the sources but got too many problems and gave up..."
    author: "Lionel Graff"
  - subject: "Re: Solaris binaries"
    date: 2001-11-06
    body: "Really ?\nI tried to find them in pub/kde/stable/2.2\nI only found the bin.tar.bz2 files ?\nI dunno how to install them\n\nIs there a pkg release somewhere for 2.2 or 2.2.1 ?\n\nThanks in advance\n\nVincent"
    author: "Vincent Croquette"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-07
    body: "I second that question, I run SunOS 5.6 on a SUN Ultra 250 and have tried to installe KDE2.0 from source, using GNU g++.<br>\nI managed to comple QT-2.2.1 and libz (threw libz into the QT lib/include-dirs, simply and ugly ;)<br>\nBut the kdelibs compilation stops with errors, trying to delete an instantiation of a template class (see below), so I am just sitting here now waiting for someone to pick up where I stopped.<br>\n<i>\nkcompletionbase.cpp: In method `KCompletionBase::~KCompletionBase()':<br>\nkcompletionbase.cpp:46: type `class QGuardedPtr<KCompletion>' argument given to `delete', expected pointer<br>\nmake[3]: *** [kcompletionbase.lo] Error 1</i>"
    author: "jimbo"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-06
    body: "I never got the konqueror to browse web pages...\n(rh6.2 x86)\nIt complained something about /usr/lib/klibssl or something like that."
    author: "Patrik Wallstr\u00f6m"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-06
    body: "You need to install openssl (and any of it's depencies). Konqueror needs that to view secure sites. You might be able to get a version that doesn't require openssl, such versions exist for Debian GNU/Linux."
    author: "AArthur"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-06
    body: "Lies!  Lies!  The rh6.2 binaries are not where that link shuffles you.  Only a couple of SRPMS, but no binaries.\n\nlies!  lies!\n\n--arcade"
    author: "arcade"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-07
    body: "Your attitude is pathetic. Remember that these people are providing a whole desktop system for free. For F R E E !"
    author: "David"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-07
    body: "Don't try to convience him.\nHe's a Slashdotter, so that kind of attitude is normal to him."
    author: "Anonymous"
  - subject: "KDE 2.0 Binaries, Mandrake version and distro prob"
    date: 2000-11-06
    body: "<p>If there is any fault with KDE 2 it is that it is not so easy for newbies to install. The mandrake RPMs are a good example. Simply trying to install them on Mandrake 7.1 or older is an exercise in patience getting all the dependencies, very likely breaking X and other things with the new initscripts and upon completion Konqueror will not work right nor much else. In fact in theory you have remove all old KDE 1 rpms which are not all conveniently named starting with a k.</p>\n<p>I have installed from source and found it not to be to difficult at all, plus everything works, kde1 is still 100% and I have a pristeen install instead of one that is distro-ized with different menus, icons and splash screens.. I know of no one from the Mandrake newsgroup proclaiming total victory installing from RPM.</p>\n<p>It would be great if KDE had more assistance in packaging etc... however I think this misses the point. It really is up to distros to package. They do the RPMs. They make all the different ways to start X and customize all the tweaks. While it would be great to have a Helix like install I thihk it is impractical and too time consuming for KDE developers to have to think about packaging.</p>\n<p>I thought about packaging for older versions of Mandrake however with the Quanta editor most of the problems we encountered were from RPM installs. I realize that even if I have unlimited bandwidth on my server I can't justify the email requests for help. Only a distro can afford that and guess what? They want to sell new boxes... or you can download the CDs for free.</p>\n<p>I have created a help page for mandrake users for getting exactly the prep files they need and walking them through the steps of an install including even how to get the new kdm on line. I think we have to realize that RPMs are a double edge sword. They are faster to install but they remove choices. They are also nightmarish to make and have such a complex variety for point numbers and distros (on the level of KDE 2) as well as back end service issues. We should expect RPMs pretty much just from distros and generally only supporting thier new releases, at least on the complexity level of a project like KDE 2.</p>\n<p>If you would like to look at my Mandrake help page it is <a href=\"http://virtualartisans.com/linux/mandrake/\">here</a>."
    author: "Eric Laffoon"
  - subject: "Re: KDE 2.0 Binaries, Mandrake version and distro prob"
    date: 2000-11-07
    body: "It is a real shame that Mandrake is not releasing rpms for their \"old\" products (7.0 and 7.1), and only for the brand-new 7.2\n\nLike you, I ended up spending a lot of time upgrading my system (after looking at your helpful page :-) ), and a lot of time downloading the source. \nLuckily, the compile went flawlessly, and i now have a very stable KDE2 \n\nHowever, I'm sure that there are many Mandrake users out there that are very happy with their 7.0 or 7.1 (like me), but unable to spend a day (thats how long it takes with ISDN) to download and install the upgrades and the KDE2 source\n\nIf only I was less curious.. then I would have been able to save a lot of time by waiting a bit and instead buying (again!!) yet another Mandrake CD"
    author: "Erlend Boe"
  - subject: "Re: KDE 2.0 Binaries, Mandrake version and distro prob"
    date: 2000-11-07
    body: "Thanks for this. I recently (<A href=\"http://lists.kde.org/?l=kde-user&m=97340634729870&w=2\">here</A>) tried to answer a question on installing KDE 2.0 on Mandrake 7.1 and found it hard. Some of the required dependencies seemed to be unavailable as Mandrake RPMs.\n<br>\nMy answer was based on <A href=\"http://lists.kde.org/?l=kde-user&m=97299020422157&w=2\">\"Be careful:...\"</A> which I based on what I could see in RPMFIND, Cooker and other sites."
    author: "Paul C. Leopardi"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-06
    body: "I had problems starting konqueror and everythig else with KDE2, because I am stage with Mandrake where I don't think what I am doing. :-)\n\nAnyhow, I got installed every packages except kdebase with rpm -ivh *.rmp. I downloaded rpm's from ftp.kde.org. Kdebase was version kde 1.2. I made upgrade from downloaded iso of 7.2 from version 7.1. I leave you guess what went wrong, I even thougth to made some report to mandrake. ;-) Below are notes which I wrote down, when I tryed to install downloaded  version from ftp.kde.org. Oh, I had kde2-beta in /opt/kde2 :-) \n\nkdebase                     ###############################unpacking of archive\nfailed on file /usr/share/apps/kdm/pics/users: cpio: unlink\n \nfile /usr/bin/khotkeys from install of kdebase-2.0-1mdk conflicts with file from package khotkeys-1.0.3-3mdk\nfile /usr/bin/kdesu from install of kdebase-2.0-1mdk conflicts with file from package kdesu-0.98-13mdk\nfile /usr/bin/kdesud from install of kdebase-2.0-1mdk conflicts with file from package kdesu-0.98-13mdk\n \nat least:\n \nwarning: /etc/X11/wmsession.d/01KDE created as /etc/X11/wmsession.d/01KDE.rpmnewwarning: /etc/menu-methods/kde created as /etc/menu-methods/kde.rpmnew\nwarning: /etc/pam.d/kde created as /etc/pam.d/kde.rpmnew\nwarning: /etc/profile.d/kde.csh created as /etc/profile.d/kde.csh.rpmnew\nwarning: /etc/profile.d/kde.sh created as /etc/profile.d/kde.sh.rpmnew\nkdebase                     ##################################################"
    author: "anon"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6"
    date: 2000-11-07
    body: "For a start, it looks like the rpm you downloaded is corrupted, try downloading it again or from a different server (mirror)\nSecondly you should install all the binaries with rpm -Uvh to remove the old KDE 1.1.2 packages (U for upgrade) \nAs for the conflicts, It will porbably be ok to force the installation agains the kdesu package by doing rpm -Uvh --nodeps kdebase-.....\nIt will overwrite the 3 files from kdesu-0,98 but if Mandrake have been consistent (I would imagine so :-))  then the replacement shouldn't matter. Either that or just uninstall kdesu-0.98 first but there may be other packages that depend on it. \nGood Luck"
    author: "stephen harker"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-07
    body: "Hi,\n\nRemove or rename the /usr/share/apps/kdm/pics/users directory, this will allow the RPM to install what it needs. It wants to make a symlink, and can't. Try forcing the update on the rest of them, it works. I do agree though, the packaging could have been better :-) At least, leaving KDE 1.1.2 there would have been a nice option for those who didn't want to jump in too deep.\n\nRegards,\n\nLester"
    author: "Lester Barrows"
  - subject: "How to install kde2 on old Mandrake"
    date: 2000-11-07
    body: "Mandrake is actually very easy.\n\nBasically you should:\n\nboot a gnome or failsafe X session\nuninstall ALL of kde1 (try rpm -qa |grep kde) kdebase, kdesupport, kdegames, etc. Plus uninstall of its apps (e.g., kpacman)\n\nThen uninstall qt & qt2 and any of its apps (probably won't be many non-kde qt apps; nethack is one, however), because the new qts will be incompatible.\n\nThen download new qt and qt2 rpms from Mandrake, plus all the basic kde rpms (koffice, kdebase, kdesupport, etc.), plus any other qt/kde apps you want (e.g., kpacman).\n\nIn addition you'll need to upgrade mandrake_desk and menu; you also might need to upgrade other things; e.g., menudrake. There might be related dependencies; e.g., rpm 4, but these should be ok.\n\nrpm -U *should* work the same rpm -e and then rpm -i, but for several reasons (e.g., the new kde1-compat rpm, which won't work too well with the old packages otherwise), I'd recommend the rpm -e approach.\n\n\nBut whatever you do, don't rpm -i, which will try to install *both* versions"
    author: "Mike Hunt"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-06
    body: "is there any 'howto' document for upgrading from KDE1.1.2 to KDE2.0?"
    author: "mathi"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-07
    body: "I would also like to know...\nIs it enough to do rpm -Uvh packages ?\nOr are there post-installation procedures ?\nthanks. Martin"
    author: "Martin"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-07
    body: "This is only the beginning of an answer:\n<br>\nIn <A href=\"http://lists.kde.org/?l=kde-user&m=97299020422157&w=2\">\"Be careful:...\"</A> I showed that RPMs for each distribution assume a different directory structure and also make different assumptions about whether to keep KDE 1.1.2, install over it, or re-install it as a compatibility library.\n<br>\nIn this case, and for other reasons, the post-installation steps will vary with your distribution and version.\n<br>\nI am looking for the right post-installation steps for SuSE 7.0. I would like to get rid of all obsolete references to /opt/kde in files under /etc and /sbin. Some of these references should be changed to /opt/kde2, but only in some files and not others. These post-installation steps do not apply to Red Hat or Mandrake because they use a different directory structure.\n<br>\nAlso, I installed from the SuSE RPMs at KDE and not those at SuSE, and I'm not sure if there are any differences besides the SuSE installation shell script which exists at SuSE but not at KDE.\n<br>\nSee also <A href=\"http://dot.kde.org/973526972/973588466/posting_html\">this posting.</A>"
    author: "Paul C. Leopardi"
  - subject: "Compilation options??"
    date: 2000-11-06
    body: "I'm posting using the RedHat 6.2 version of Konqueror right now. Does anyone know if RedHat used the faster KDE compilation options, such as -fno-exceptions and -O2? How can I find out?"
    author: "Simon Arthur"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-06
    body: "Good news to some who got disappointed:\nftp://ftp.kde.org/pub/kde/stable/2.0/distribution/rpm/RedHat/rh6.2/i386/\nreally includes binary rpms.\nAlso Dre may sigh for relief, he will not be killed :), but he should update the links."
    author: "Eeli Kaikkonen"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-07
    body: "The rpms for RedHat 7.0 is not so good. \nFirst, the libmng and qt-2.2.1 is missing;\nSecond, even after downgrade the libpng, some\nkde program like kcontrol still keep crash/give\ncore dump when you try to close it after play\nwith it for a while. Anyone knows what's wrong?"
    author: "trelos"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-07
    body: "See <A href=\"http://lists.kde.org/?l=kde-user&m=97299020422157&w=2\">\"Be careful...\"</A> for some hints. I do not use Red Hat, so I have not been able to test it for accuracy, but I have submitted it for inclusion in the KDE FAQ."
    author: "Paul C. Leopardi"
  - subject: "Konqueror Strangeness"
    date: 2000-11-07
    body: "RH 6.2 install went smoothly - thanks! Only problem is that Konqueror refuses to display web pages unless (and this is the odd bit) it is started from a terminal. Starting it from the panel, a desktop link, or even right-clicking an html file and say Open With... konqueror, gives a \"Could not create view for text/html\" error. Any ideas?"
    author: "Uri Zarfaty"
  - subject: "Re: Konqueror Strangeness"
    date: 2000-11-07
    body: "The installation did not go smoothly for me and I ended up removing KDE 1 to install KDE2\n\nNow everything seems to work except Help, Konqueror and kMail....\n\nI get the same html/text error on opening web pages.\n\nDoes anyone know how to fix these problems?"
    author: "Stuart Tippins"
  - subject: "Re: Konqueror Strangeness"
    date: 2000-11-08
    body: "By installing OpenSSL 0.9.6 binary RPM...\n\nIf one of the KDE FTP maintainer will allow me to upload - I will upload tommorow a Redhat 6.2 RPM which I made...\n\nHetz"
    author: "Hetz"
  - subject: "Re: Konqueror Strangeness"
    date: 2003-01-17
    body: "\"Could not create view for text/html\",  in Konqueror.\n\nI had a similar problem and message, in spite of having made Konqueror the primary HTML reader.   Other accounts on the same computer worked fine, so I compared the settings:  by changing the configuration for the html file-type in the \"Embedding\" tag, and putting \"KHTML\" as the only thing in the \"Services preference order\" it all worked.\n\n"
    author: "Nathaniel Taylor"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6"
    date: 2000-11-07
    body: "Hello,\nCan anyone tell me where to find the PowerPC RPM's?? the link ( ftp://ftp.kde.org/pub/kde/stable/2.0/distribution/rpm/ppc-glibc21/ ) leads to and empty folder and it has been leading to that empty folder for weeks now. Can anyone tell me which, if any, server has the PPC binaries? how much longer will Mac users have to wait to experience KDE 2?\n\nthanks,\nappleweek@mindspring.com"
    author: "MacPlus"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6"
    date: 2000-11-07
    body: "Hello,\nCan anyone tell me where to find the PowerPC RPM's?? the link ( ftp://ftp.kde.org/pub/kde/stable/2.0/distribution/rpm/ppc-glibc21/ ) leads to and empty folder and it has been leading to that empty folder for weeks now. Can anyone tell me which, if any, server has the PPC binaries? how much longer will Mac users have to wait to experience KDE 2?\n\nthanks,\nappleweek@mindspring.com"
    author: "MacPlus"
  - subject: "RRunning KDE2 apps in WindowMaker"
    date: 2000-11-07
    body: "I am trying to run KMail and a few other apps without running KDE2 and I get a bunch of errors, and then all of the toolbars and menus are missing from the app.\n\nThe errors are QObject::connect errors to KMMainWin and many others\n\nIs there a secret I am missing?"
    author: "Frustrated"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-07
    body: "This mirror at least has got them:\nftp://ftp.na.kde.org/pub/kde/stable/2.0/distribution/rpm/RedHat/rh6.2"
    author: "Uwe"
  - subject: "Re: More KDE 2.0 Binaries Available: SuSE site"
    date: 2000-11-07
    body: "SuSE has KDE 2.0 download information including FTP sites and URLs at <A href=\"\nhttp://www.suse.com/us/support/download/kde2/index.html\">http://www.suse.com/us/support/download/kde2/index.html</A> and instructions given in a FAQ at <A href=\"http://www.suse.com/us/support/download/kde2/faq.html\">http://www.suse.com/us/support/download/kde2/faq.html</A>\n<br>\nThe SuSE RPMs at <A href=\"ftp://ftp.suse.com/pub/suse/\">SuSE</A> have a different directory structure and come with an installation script which is not in the equivalent SuSE directories at <A href=\"ftp://ftp.kde.org/pub/kde/stable/2.0/distribution/rpm/SuSE/\">KDE</A>\n<br>\nThe SuSE home page links to a press release at <A href=\"http://www.suse.com/us/suse/news/PressReleases/kde2.html\">http://www.suse.com/us/suse/news/PressReleases/kde2.html</A>\n<br>\nI do not work for SuSE but am running SuSE 7.0 at home, with KDE 2.0 installed and running, but missing some post-installation steps."
    author: "Paul C. Leopardi"
  - subject: "Re: More KDE 2.0 Binaries Available (Yes, RedHat 6.2 too)"
    date: 2000-11-07
    body: "http://bestlinux.net/cgi-bin/ftp.pl?dir=pretest/current/KDE2\nBest Linux is a Red Hat variant from Finland.\nSee http://bestlinux.net/en/"
    author: "Eeli Kaikkonen"
  - subject: "RedHat 6.2 RPMs"
    date: 2000-11-07
    body: "Well, I've yet to get the RedHat 6.2 i386 RPMs to work on my RH 6.2 system. Basically, the problem is that, after running startkde in my .xinitrc, I get:\n<p><tt> KDesktop error dialog:                                                           \nUnable to create io-slave: klauncher said: Unknown protocol 'file'             \n                                                                              \n<br>Both KDesktop and Kicker error dialogs:                                            \n                                                                               \n<br> * Could not fine mime type aplication/octet-stream                            \n <br>* No mime types installed </tt>\n<p>I submitted a bug report and David Faure responded quite fast. After checking some (to him :) obvious things, he found \"kdeinit: Already running.\" in my .xsession-errors file. This led him to reply:\n<p><i>\"Hmm, that's the dcopserver problem again.\nI remember it has something to do with gcc 2.96 - I wonder if your packages were compiled with it ? This should go back to whoever compiled your packages, it's not a KDE bug per se.\"</i>\n<p>The RPMs were downloaded from ftp.kde.org. The RPM info on them said they were built on porky.redhat.com. Anyone know how to get word to whomever built these at RedHat ?\n<p>BTW, many thanks to David Faure for his polite and speedy responses :)"
    author: "David Huff"
  - subject: "where is libGL.so and libGLU.so in rh6.1?"
    date: 2000-11-08
    body: "I'm running rh 6.1 and when I try to install the rpm for qt2.2.1 it complains about my open gl... i have mesa installed and actually the libgl.so.1 is on my hdd... what's the deal? can someone help me out or point me to the right package?\n\nThanks\nWill."
    author: "Will"
  - subject: "Caldera KDE binaries"
    date: 2002-09-17
    body: "I can't seem to find a link anywhere (that works) to the kde 2.* binaries for Caldera edesktop 2.4\n\nAny suggestions?"
    author: "Paul"
---
Several of you have written in to note that Red Hat 6.2 rpms for KDE 2 are now available <A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/RedHat/rh6.2/">here</A>.  A full list of KDE 2.0 binaries for the various distributions/versions is below -- if you know of any more please add a comment.  Remember, folks, these rpms are not built by the KDE Team.  <STRONG>Update: 11/06 10:16 PM</STRONG>.  It appears the mirrors have <EM>still</EM> not picked up the rpms -- here is a <A HREF="ftp://ftp.kde.org/pub/kde/stable/2.0/distribution/rpm/RedHat/rh6.2/">direct link</A> to the KDE ftp site.



<!--break-->
<P>
Here is a list of the KDE 2.0 binaries available as of todayt:
</P>
<P>
<UL>
<LI>
<A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/COL-2.4/">Caldera OpenLinux 2.4</A> for i386
</LI>

<LI>
<A HREF="http://ftp.kde.org/stable/2.0/distribution/tar/True64/">Compaq Tru64 Systems</A>
</LI> 
<LI>
<A HREF="http://ftp.kde.org/stable/2.0/distribution/deb/dists/potato/">Debian GNU/Linux 2.2 (potato)</A> for <A HREF="http://ftp.kde.org/stable/2.0/distribution/deb/dists/potato/binary-i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.0/distribution/deb/dists/potato/binary-sparc/">sparc</A>
</LI>
<LI>
<A HREF="http://ftp.kde.org/stable/2.0/distribution/deb/dists/woody/binary-i386/">Debian GNU/Linux Devel (woody)</A> for i386
</LI>
<LI>
<A HREF="http://www.freebsd.org/ports/kde.html">FreeBSD</A>
</LI>
<LI>
<A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/Mandrake/">Linux Mandrake 7.2</A> for i586 (guessing on the version here, Mandrake does not say)
</LI>
<LI>
<A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/ppc-glibc21/RPMS/">LinuxPPC (glibc 2.1)</A> for ppc
</LI>
<LI>
<A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/RedHat/rh7/">RedHat Linux 7.0</A> for <A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/RedHat/rh7/i386/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/RedHat/rh7/alpha">alpha</A> and <A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/RedHat/rh7/sparc/">sparc</A>
</LI>
<LI>
<A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/RedHat/rh6.2/i386/">RedHat Linux 6.2</A> for i386
</LI>
<LI>
<A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/">SuSE Linux 7.0</A> for <A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/7.0-i386">i386</A>, <A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/7.0-ppc/">ppc</A> and <A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/7.0-sparc/">sparc</A>
</LI>
<LI>
<A HREF="http://ftp.kde.org/stable/2.0/distribution/rpm/SuSE/6.4-i386/">SuSE Linux 6.4</A> for i386
</LI>
</UL>
</P>





