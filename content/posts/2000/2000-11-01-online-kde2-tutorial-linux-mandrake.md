---
title: "Online KDE2 Tutorial from Linux-Mandrake"
date:    2000-11-01
authors:
  - "plavigna"
slug:    online-kde2-tutorial-linux-mandrake
comments:
  - subject: "Ha!"
    date: 2000-11-02
    body: "Ha!\n\nMy Mandrake 7.2 installation worked smooth!!! NO, NO, NO help needed! No more hassle with RH7 and XFree86,TrueType,startx,KDE2,etc,etc!\n\nWow! KDE2 is SO great!"
    author: "Torben Skolde"
  - subject: "Re: Ha!"
    date: 2000-11-05
    body: "Ha yourself; I'm dying to get KDE2 working but I share your woes re: RH7 and kde2.  Whaaa.\nHere's hoping I get to share the joy soon..."
    author: "Rubin"
  - subject: "Re: Ha!"
    date: 2000-11-05
    body: "What's your problem with RH7.0 and KDE2?"
    author: "Ravindra"
  - subject: "Re: Online KDE2 Tutorial from Linux-Mandrake"
    date: 2000-11-02
    body: "ok, maybe this should not be here but i dont give a damn! kde is nice and all but i find it reminding me to much of winblows, i like enlightenment, i swiched over to it on mandrake 7.2. it runs fine other then one small problem there is no way to tell it to make the menus... is this just a problem with my system or is it a fu bar....? thanks for listening to me wine.\n\nlina"
    author: "Smooth install but whats up with E?!"
  - subject: "Re: Online KDE2 Tutorial from Linux-Mandrake"
    date: 2000-11-03
    body: "<i>but i find it reminding me to much of winblows</i>\n\nRegarding to kde's ability of customization i assume a cow would remind you of windows too."
    author: "Lenny"
  - subject: "Re: Online KDE2 Tutorial from Linux-Mandrake"
    date: 2000-11-05
    body: "Although I haven't tried KDE2/Mandrake7.2 yet (just about to), I figure this hasn't changed...\n\nThe reason you can't edit the enlightenment menus is that that must be done by hand.  KDE doesn't edit enlightenment menus, only desktop environment menus.  Enlightenment 16.3 menus, however, are very easy to learn and edit.  Start browsing through your .enlightenment file, and you'll soon start to see the relation of the files to your pop-up menus.  If you have further questions, please IM me online (AIM, fuzzyping).\n\n-J."
    author: "fuzzyping"
  - subject: "A quote from the tutorial..."
    date: 2000-11-04
    body: "<TT><I>\"Konqueror is the name of KDE2's new file manager. Konqueror supports drag 'n drop just like other operating systems you may be used to.\"</I>\n<BR>\n-From page 40 of the online tutorial.</TT>\n<BR><BR>\nI wonder what \"other operating systems\" they mean. Certainly not MacOS...\n<BR><BR><BR>\nI can't wait 'til I get Linux-Mandrake 7.2!"
    author: "The Khan"
  - subject: "A quote from the tutorial..."
    date: 2000-11-04
    body: "<BLOCKQUOTE><I>\"Konqueror is the name of KDE2's new file manager. Konqueror supports drag 'n drop just like other operating systems you may be used to.\"</I></BLOCKQUOTE>\n<TT>-From page 40 of the online tutorial.</TT>\n<BR><BR><BR>\nI wonder what \"other operating systems\" they mean. Certainly not MacOS...\n<BR><BR><BR>\nI can't wait 'til I get Linux-Mandrake 7.2!"
    author: "The Khan"
  - subject: "Whoops!"
    date: 2000-11-04
    body: "I hate double posts. Sorry."
    author: "The Khan"
---
To help celebrate the release of KDE2 and Linux-Mandrake 7.2 (which now offers KDE2 as the default desktop), a new set of pages have been posted on the <a href="http://www.linux-mandrake.com/">Mandrake website</a> which offer a beginner's tutorial of the <a href="http://www.linux-mandrake.com/en/demos/Tutorial/">KDE2 desktop</a>.
Hope you stop by, your comments and suggestions are always <a href="mailto:phil@mandrakesoft.com">welcome</a>.



<!--break-->
