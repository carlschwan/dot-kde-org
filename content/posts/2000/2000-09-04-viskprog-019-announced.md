---
title: "VisKProg 0.1.9 announced"
date:    2000-09-04
authors:
  - "numanee"
slug:    viskprog-019-announced
---
A new release of <a href="http://www.viskprog.org">VisKProg</a>, a visual programming language for KDE, was recently announced.  Here are some sample <a href="http://www.viskprog.org/screen/screen018.html">screenshots</a>.  Seems pretty interesting, hope they port it to KDE 2.0 soon!

<!--break-->
