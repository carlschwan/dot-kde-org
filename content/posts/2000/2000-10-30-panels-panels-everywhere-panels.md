---
title: "Panels, Panels, Everywhere Panels"
date:    2000-10-30
authors:
  - "Dre"
slug:    panels-panels-everywhere-panels
comments:
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-30
    body: "Just a note to say I'll be porting KasBar to the new framework over the next few days. Once I've done that I plan to add support for displaying the window menu and for the startup notifier."
    author: "Richard Moore"
  - subject: "Possible to get rid of the panel?"
    date: 2000-10-30
    body: "What's the easiest way to disable the panel completely? I don't use it, and would prefer to just not run it. I keep it hidden, but that's obviously not an ideal solution.\n\n<p>I'd like to see a checkbox at the very top of the panel preferences labeled \"show panel\" that would control whether the panel is displayed at all."
    author: "M. Brubeck"
  - subject: "Re: Possible to get rid of the panel?"
    date: 2000-10-30
    body: "it's an app, it's called kicker, don't start it (ie, comment it out of your startkde script)"
    author: "Kevin Puetz"
  - subject: "Re: Possible to get rid of the panel?"
    date: 2000-10-30
    body: "btw, i'm not using the desktop (only to click the drakconf button but i could use the menulink ;)) so i was thinking of not loading the desktop. can this cause any probs with any app?"
    author: "Spark"
  - subject: "Re: Possible to get rid of the panel?"
    date: 2000-10-31
    body: "it shouldn't cause problems if you have all the KDE2 libs that app needs... \n\nhowever, you will probably notice slower loading times as things like dcop will need to start up... you can start the dcopserver prior to launching your window manager to see some speed up apparently..."
    author: "Aaron J. Seigo"
  - subject: "Great!"
    date: 2000-10-30
    body: "this is exactly what i was waiting for. I just love the way it was arranged in KDE 1.x"
    author: "renaud"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-30
    body: "The lead article is slightly inaccurate in that these changes were planned for a while, but of course we were in freeze. ;)\n\nWhat Matthias has now implemented is way more generic and powerful than an implementation of an external taskbar!\n\nCheers,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-30
    body: "mmm... i prefer the new kde2 task-bar. but i\nrespect this fast response-time :-), how long\nhad people to wait for microsoft implementing\nsuch a new feature? 2 years?\n\nthe taskbar was the thing i most disliked with\nthe old kde and i love the new one implemented\nin the panel. \n\ngreat work, kde btw. \na hello & many thanx to all kde-developers out\nthere.\n\nbye\n<-harald\n\np.s.: written with konqueror ;-)"
    author: "harald"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-30
    body: "Agreed.  KDE1's separate taskbar uses too much vertical screen room for my tastes - I greatly prefer the KDE2 default.  But it's nice to see that KDE's still all about choice.\n\n(Hey, KDE devs, how about restoring the ability to bind keys to desktop up/down/left/right like KDE1?  That's one thing I *do* miss a lot)."
    author: "Ian Schmidt"
  - subject: "Being able to \"stretch\" kicker"
    date: 2000-10-30
    body: "Hi.\nWhile I very much like and appriciate kicker, however, it would be nice if one coud stretch it so that it gets \"multiple rows\" like the windows startmenu. Sometimes(allways) you have so many windows open you want kicker to be able to display them all a little bigger. Its a nice feature of the windows startmenu/taskbar which I think many would like in kicker too.\n\n--\nAndreas Joseph krogh"
    author: "Andreas Joseph Krogh"
  - subject: "Re: Being able to \"stretch\" kicker"
    date: 2000-10-31
    body: "What I would like even more would be a taskbar that has a button per application not per window. Pressing the button would give a menu with all windows of that app (like its done in BeOS), maybe depending on the mouse button used (e.g. first mouse button would bring a random window to the front (useful for one-window-apps) while second mouse button would open the window menu and third mouse button would open the application menu). This would not only save space but would make it easier to find a particular window. At least IMHO :-)\n\nThat said, I want to add that KDE is really a great piece of work and I want to thank all of you developers, documenters, translators, organisators and what have you for making it!"
    author: "Thorsten Seitz"
  - subject: "Re: Being able to \"stretch\" kicker"
    date: 2000-10-31
    body: "<i>What I would like even more would be a taskbar that has a button per application not per window.[...]</i><br><br>\nWell, the good thing about the kicker framework is, that the parts are completely interchangeable. You can easily write an own taskbar (it's really easy, all the stuff about window-management is in the libs) and use that instead of the the shipped one."
    author: "gis"
  - subject: "Getting used to a taskbar"
    date: 2000-10-30
    body: "Wow, this is great,\n\nI used the taskbar on top in KDE 1.1.2,\nand in the betas there was kasbar, which was\nuseful too. But in the final kde2 it is really\nhard to get used to the current taskbar.\n\nI am looking forward to this extension,\n\nThanks\n\nMatthias\n-- \nNo, Dun Laoghaire is pronounced differently!"
    author: "Matthias Lange"
  - subject: "Re: Getting used to a taskbar"
    date: 2000-10-31
    body: "> No, Dun Laoghaire is pronounced differently!\n\nWhat's someone called Matthias doing in Ireland?"
    author: "Nobody"
  - subject: "Re: Getting used to a taskbar"
    date: 2000-10-31
    body: "<p>&gt; <i>What's someone called Matthias doing in Ireland?</i></p>\n<p>Vacation, I guess...</p>"
    author: "GeZ"
  - subject: "Re: Getting used to a taskbar"
    date: 2000-10-31
    body: "<offtopic>\nActually, I studied in Ireland last year.\n\nOn Thursday I will be over there again,\nso this sentence just came to my mind.\n\nSlainte\n\nMatthias\n</offtopic>"
    author: "Matthias Lange"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-30
    body: "I'm not sure I understand these screenshots... \n\nWhat I --really-- want out of kicker is the ability to have it be less than full screen in width... it really really really really sucks on 4 monitor xinerama to have a panel all the way across 4 monitors!!!!"
    author: "Joe Dorita"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-31
    body: "<i>the ability to have it be less than full screen in width</i><p>\nThat's one of the more common requests in the KDE General mailing list.  I'm using BlackBox as a Window Manager for that reason (partially - I also was using early betas where the window manager was crashing).<p>\nI've got a while bunch of tweaks to make KDE apps work fine in multihead configurations - like fixing the \"Konqueror opens huge when links in KMail are clicked\", but Kicker still stinks on a multihead config.<p>\nOh, and try a vertical config if your monitors are laid out horizontally.<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Vertical taskbar"
    date: 2000-10-31
    body: "I always used the old (1.1.2) vertical taskbar to <B>protect some space for apps</B>, which should be visible. In my case, I run an ICQ client, which gets not lapped over from new starting apps. It was also possible to maximize the other apps, without extending all over the screen.<br><br> I really miss this protected area and I hope it will be back with the externel taskbar.<br><br>\n\n\nBTW: <B>Congratulation to the programmers of KDE 2</B>, its even more impressive than the old one. Especially Konquerer is fast (except when he delete files (?!))\n<br><br>\nAndre\u00b4"
    author: "Andre"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-31
    body: "That's AWESOME! where do we get it?"
    author: "tannhaus"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-31
    body: "Looks great, flexibility all around. But what i've seen from the screenshots, it's not possible to hide these extra panels away (Don't know about autohide but I don't see any arrows on either side). More panels, ok, but you must be able to get rid of them fast! (AFAIK, Gnome has this feature and windows has this feature as well)\n\nKeep up the good work!!"
    author: "Netizen51"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-31
    body: "Nice idea with the panels. But I have some problems to add more menus to the panel. With KDE 1 it was no problem to add a complete submenu from the K-Menu to the panel. With KDE 2, i find no way to do this. Maybe its my error and someone can help me?"
    author: "Bernd"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-31
    body: "Just drag the menu you want to the panel. IIRC you need to drag the icon not the text.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-31
    body: "It's called the Kicker, not panel ;-)"
    author: "fura"
  - subject: "Finally"
    date: 2000-10-31
    body: "Finally <b>KDE (kicker)</b> has the feature, <b>GNOME</b> has since May that year. Another nice thing would be <i>moving</i> the windows in the dekstop preview, like GNOME has! \nAnyway, nice work, go on..."
    author: "Christian Meyer"
  - subject: "Re: Finally"
    date: 2000-10-31
    body: "This is more powerful than the GNOME feature.  You can add any extension you can think of to kicker, it's not just about more panels.  As for moving the windows, use the pager.  Alt-F2 kpager.  Even KDE1 had that. Thanks for the FUD anyway."
    author: "KDE User"
  - subject: "Re: Finally"
    date: 2000-11-05
    body: "Thats not FUD. I am having using multiples panels, applets (extensions), a very long time in GNOME. Im sure KDE developers got THE IDEA from GNOME implementation.\n\nLook at my Desktop in order to verify (a very big screenshot attached)."
    author: "Miguel E"
  - subject: "Re: Finally"
    date: 2000-11-05
    body: "I dont know what happened with attachment :-/"
    author: "Miguel E"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-31
    body: "Hi \nThanks for a nice and fast KDE 2.0, I run it in Mandrake 7.2.\n\nI am an old man and use large fonts on my 19\" tube. Therefor I use many desktops with usually only one program in anyone of them. I find it irritating that all my programs starts in the upper left corner and COMPLETELY COVERS\tmy icons.\n\nHow do I change the starting position for a new program on a new desktop, something like Windowmanager.\n\nregards\nguran"
    author: "g\u00f6ran remberg"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-31
    body: "Grrr, this is a step in the wrong direction. We need to make Kicker *smaller*, not take up more space. Kicker is huge, and doesn't have to be. We should spend time making kicker more space efficient instead of making more of them..."
    author: "Chris Aakre"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-11-01
    body: "??? What do you mean? Kicker can already be configured to only use a minimal amount of space with the 'tiny' size. Combined with embedding the task list as is the default in KDE 2 kicker is very space efficient.\n<p>\nIf you have a genuine issue then please give some more detail - we aren't mind readers."
    author: "Richard Moore"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-10-31
    body: "I got a problem shading and unshading windows, everytime I unshade a window it gets 1 pixel larger, why ??\n\nI noticed a crash Window$ has too, when kicker restarts the trayicon isn't updating :)\n\nAnyway, kicker rocks!! and the screenshots I saw even more."
    author: "Mikey Machuidel"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2000-11-03
    body: "KDE2 kicker is just awesome although panel extensions are even cooler.  The taskbar applet is very good too. I like the way that you can select all applications or only the current desktop.  Now if there was a third option:  to display the current desktop as full entries and other desktops as small icons only (like when you drag the applet), that would be the best option!"
    author: "Martin Fick"
  - subject: "Re: Panels, Panels, Everywhere Panels"
    date: 2001-01-10
    body: "Nice screenshots but I still dind't understand how to add a child panel.\nIs it possible at all while working with KDE2.0.1 ?"
    author: "Tina"
  - subject: "Removing child panels and extensions."
    date: 2001-05-26
    body: "Well I finally installed KDE2 and it looks fantastic. Kicker kicks ass.\n\nMy problem is this: In a fit of insanity one night I decided to see how many child panels I could create, now I have no idea how to get rid of them. I tried editing kickerrc, but that just stopped it starting up...\n\nany ideas?"
    author: "Geoff Thornburrow"
---
It seems many users have not been overly thrilled with the way the task
bars are arranged in the KDE 2 desktop.  As is typical, the KDE developers have quickly responded.
<A HREF="mailto:meNOSPAMns.caldera.de">Matthias Elter</A> wrote
in to tell us he has made some nice modifications.  Details -- and
screenshots -- below.













<!--break-->
<p>
Matthias writes:
</p>
<blockquote>
I have implemented the kicker extension framework I've been talking about.
It comes with two demo extensions, an external taskbar and a child panel
extension.<br><br>
 
Similar to panel applets you can add a panel extension from the panel's
RMB menu with "Add->Extension->Something". 
<br><br>
 
The <a href="http://master.kde.org/~me/panel/external_taskbar_bottom.jpg">external taskbar</a> is a simple external taskbar as requested by many
people. I'm going to improve the vertical mode to make it use only as
much space as required.<br><br>
 
The child panels are ... child panels. Add as many as you need. Adding a child panel with a taskbar applet
is a second way to get an external taskbar for example. I'm going
to improve them to work in a "corner-only" mode where they wont use the
full width respective height. See
the screenshots for a <A HREF="http://master.kde.org/~me/panel/panel_on_the_left_child_panel_on_top.jpg">child panel to the top right</A>, <A HREF="http://master.kde.org/~me/panel/panel_on_bottom_plus_small_child_panel.jpg">a child panel above</A>,
and <A HREF="http://master.kde.org/~me/panel/child_panel_on_top.jpg">a child panel on the top</A>.
<br><br>
 
Panel extensions similar to panel applets can be run internal (shared libs)
or via a proxy process. The child panel extension is an exception to this
rule, it works internal only.<br><br>
 
This is work in progress but stable enought o be used for daily work and
for <A HREF="http://www.mosfet.org/">Mosfet's</A> kasbar to be ported to.<br><br>
</blockquote>






