---
title: "Kivio beta 2 now available"
date:    2000-11-07
authors:
  - "sgordon"
slug:    kivio-beta-2-now-available
comments:
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Wow a lame rip-off of visio that isn't even visio compatable. Real original design here guys."
    author: "Frank Rizzo"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Glad you like it Frank.  Visio has proven itself to be a compentant and popular program, why wouldn't we use it as a template?  We are going much further with our integration than Visio ever went.  We don't read Visio files yet because we haven't gotten to it.  This is a beta release, so we haven't done everything yet."
    author: "Shawn Gordon"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Are you joking around? TheKompany has to be the *coolest* for developing awesome applications for KDE, then releasing it under the GPL. I don't know about the above post, but I sure appreciate all your guys hard work.\n\nKeep it up!\n\n--\nChris Aakre"
    author: "Chris Aakre"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-12-07
    body: "But a Visio clone is exactly what a huge number of us need, purely because the Visio name/brand has become synonymous with the type/style of document it produces, and that's what IT and Business Management relate to.  They don't ask for a vector graphics diagram, they ask for a \"visio diagram\" !!\n\nJust like Hoover for example, where people use the word hoover in place of Dyson and don't even realise the mistake; because the word \"hoover\" is synonymous with \"vaccuum cleaner\"!\n\nIf I can give my boss a Visio diagram, he's happy.  The fact that I have produced it in Kivio and then output the drawing in Visio format is irrelevant.  Ok, I know Kivio can't do this yet, but I'm just outlining a conceptual example.\n\nThere are advantages to do original work .. capitalizing on existing mind share isn't one of them.\n\nJohn"
    author: "John"
  - subject: "Re: Kivio beta 2 now available"
    date: 2006-05-21
    body: "Frank, please don't make yourself sound like a wally.  Have you any idea what the enormous challenges are of working with Visio's proprietary format?  It will take a not insignificant amount of work to reverse engineer the format, and a not insignificant amount of legal minefield ballet to get away with it.  That fact that these guys even have plans to take them on are praise-worthy enough.\n\nKivio is not a finished app.  Remember that.  And if you really want Visio support so desperately, I'm sure the Kivio project will welcome your contribution, so start coding.\n"
    author: "stevem"
  - subject: "KDE is doomed!"
    date: 2000-11-08
    body: "Oh great, now theKompany is suppost to \"help\" KDE?\nYou can't trust them, they are pure evil!\nThey are only at it for the money!\n\nJust great. Now Matthias is dropping his work and let KDE be a commercial product!"
    author: "Anonymous"
  - subject: "Re: KDE is doomed!"
    date: 2000-11-08
    body: "Well you've always got to wonder about someone that is anonymous.  In what way might we be evil?  We are writing software no one else is writing, we are providing tools to OSS that no one else is providing.  We also plan to make money on some of our projects.  How is that evil?  Even RMS is of the opinion that free software doesn't not mean no cost.  We work very closely with many people in KDE, and everyone has been happy with the arrangement as far as we know.\n\nThe wonderful thing about OSS is that you have a choice.  You can use what we create, or you can choose something else that satisfies your needs."
    author: "Shawn Gordon"
  - subject: "Re: KDE is doomed!"
    date: 2000-11-08
    body: "<p><i>We also plan to make money on some of our projects. How is that evil?</i></p>\n<p>When company after company, including Real, Microsoft, and AOL, has used the proprietary software business model in ways that harm consumers, do you blame people for being suspicious of you?</p>\n<p>When you follow this business model of selling software as a product (for whatever future proprietary projects you have in mind/in development), the burden is on you to prove that proprietary software is worth our time.  Again and again, software developers have used their exclusive control over the source code as leverage over their customers.  Why will you be different?  What guarantees do we have?</p>"
    author: "Neil Stevens"
  - subject: "Re: KDE is doomed!"
    date: 2000-11-08
    body: "I am curious Neil, do you think OSS means that the source should be available or that the software should be free?  If no one ever makes money, then eventually the system will start to degrade.  Money has to come from somewhere.  In the case of Kivio we have produced what I think is an excellent application in a very short development cycle.  Each of our products has a different pricing model.  The pricing model for Kivio will be announced in a Linuxplanet interview/review of Kivio later this week.  I don't want to make a big production of it, but be assured that Kivio is GPL and will remain free as in beer.  We are working on making it a standard piece of the KOffice package.  Hopefully people will support us so that we can continue to bring quality software to everyone."
    author: "Shawn Gordon"
  - subject: "Re: KDE is doomed!"
    date: 2000-11-08
    body: "Yes, I understand very clearly that making money is important to the thekompany venture, and I also do distinguish clearly between the price of software, and the control over software.\n\nI don't care if a piece of software is free of charge, or if it costs $1000 dollars, as long as the customer has control over his destiny.\n\nWhat I find interesting is that you keep focusing on money.  If you read Stallman's writings, money is not an issue to him.  Freedom (i.e. the lack of outside control) is an issue to him.\n\nShawn, when you keep talking about how you have to make money \"somehow,\" I have to look at your plans to make closed software releases with a critical eye.  What about thekompany should make me trust closed software releases from it?  What guarantee do I have that \"somehow\" won't include trojan code that violates my privacy, or perversion of standard protocols, or subtle incompatibilities with less-favored operating system distributions, or even annoying advertising?\n\nThat's all I Want to know.  If you can make a clear argument to answer that, my criticisms of your plans to make proprietary software releases will cease.  It's that simple.\n\nJust because you release some good software GPL, it doesn't mean I'm going to just go along with proprietary groupware servers, or closed tools for RAD, or whatnot."
    author: "Neil Stevens"
  - subject: "Re: KDE is doomed!"
    date: 2000-11-09
    body: "Neil there is nothing I can or need to say to guarantee my plans for anything that I do.  Our plans evolve as the market changes and as our understanding changes.\n\nThis does *not* change the fact that whatever we have released to date under a GPL will always be available in that state for you to do whatever you want.  If for some bizarre reason we changed the license, as we can, but I don't know why we would, you would still have that code out there.\n\nI don't need to justify or make excuses about items we might do that are closed or proprietary.  You are worried about your rights, what about my rights to my intellectual property that we spent large sums of money to develop?  Who will protect my right if I release it totally open and instead of paying for it, people just take it.  Do they have a right to do that?  Should they have a right?  \n\nI agree with making the source available for people so they can continue to improve it or modify it to suite their needs, but any work they do will be trivial compared to what we did.  \n\nI focus on money because I have a business to run and salaries to pay.  I get resumes from people every day wanting to work for me, I can't pay them if I don't have money.\n\nAs a company we expend a tremendous amount of time and money on projects that are for everyone, we are giving back to the community because it's the right thing to do.\n\nAgain you have the choice.  You can use what we create, or you can find something else to use."
    author: "Shawn Gordon"
  - subject: "Re: KDE is doomed!"
    date: 2000-11-10
    body: "You won't even promise to follow open protocols.\n\nThank you; come again."
    author: "Neil Stevens"
  - subject: "Re: KDE is doomed!"
    date: 2000-11-10
    body: "Thank you; come again? Ok.\n\nOpen protocols.  Hmm... let me see.  Flowcharting protocols, flowcharting protocols... nope, I do not see any flowcharting protocols.\n\nTo what are you referring to Neil?\n\nI'm the original author of the program, and throughout the course of it's development, I've never seen any sort of protocol that I'm supposed to abide by.\n\nAre you afraid I'm going to change the X protocol or something?  Are you afraid I'm going to embed some HTML widget inside it, change the bold tag to italics, and call it HTML2?\n\nI'm apologize if this sounds sarcastic, but if you do not like the software, or do not want to use it, then don't.\n\n-dave"
    author: "Dave Marotti"
  - subject: "Re: KDE is doomed!"
    date: 2000-11-11
    body: "I don't see your problem Neil. The Kompany programmed something - well everybody has the right to write a program. You don't have to use it, doe you? Furthermore they give it to us under the GPL. You still don't have to use it. It could be, that they develop more cool software. If you don't liek to, don't use it. So, what is your problem? You don't have to give them your money. Besides, where do you get your money from? Don't you get paid for your work? Why mustn't this people get paid for their work?\n\ncu"
    author: "Andreas Gross"
  - subject: "Re: KDE is doomed!"
    date: 2000-11-10
    body: "You make no sense,\nKivio was released under the GPL, even though theKompany has the right to change it's license down the road, the source that has already been released can be taken back or restricted, damn, you can even fork it if you want!\n\nReading Linuxplanet's article, theKompany plans making money from Kivio by selling specialized stencils, but nothing keeps you from reverse engeneering the stencil's format and create your own stencils or a tool to do it, you have Kivio's source code, so wouldn't be that hard either."
    author: "Charles"
  - subject: "Re: KDE is doomed!"
    date: 2000-11-08
    body: "Oh great, now the trolls have found their way to down here too. Okay, who told about this site on slashdot? Time to fess up."
    author: "piko"
  - subject: "Re: KDE is doomed!"
    date: 2000-11-08
    body: "This is a ridiculous remark. Support for KDE, be it commercial or not, is always helpful. Hey, Anonymous, if you don't want to buy it, just leave it!"
    author: "Alexander Kuit"
  - subject: "Re: KDE is doomed!"
    date: 2000-11-08
    body: "So now Gnome is flamed down by Slashdotters it's KDE's turn.\nInteresting tactic....\nWho's next? Xfce?"
    author: "Anonymous"
  - subject: "Flow Networks that execute code."
    date: 2000-11-08
    body: "Will kivio support creating networks of objects where you can define inputs and outputs for the various boxes.  For example, having an \"input box\" that loads an image, and various \"transformation boxes\" that are given the image as input and does various transformations.  Then at the end of the network one could have a \"viewer\" object that shows the image.\n\nI ask because I saw that kivio was supposed to be scriptable, and there are lots of programs that does the above, but usually they are not very strong in the stencil department."
    author: "Anonymous"
  - subject: "Re: Flow Networks that execute code."
    date: 2000-11-08
    body: "We are working on the scripting implementation right now.  It will probably be possible to do what you are describing, but without all the pieces in place yet, I can't guarantee anything :)"
    author: "Shawn Gordon"
  - subject: "Re: Flow Networks that execute code."
    date: 2000-11-08
    body: "Hello Anon,\n\nThis should be possible.  Like Shawn said, I can't guarantee this, but as we get the scripting pieces in, I don't see why something like this couldn't be implemented.\n\n\n-dave"
    author: "Dave Marotti"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Don't listen to the trolls, Shawn, it looks great. So it's now going to be GPL?"
    author: "SJK"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Thanks for the support :).  Actually we finally got all the GPL stuff resolved and properly applied this time around."
    author: "Shawn Gordon"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Great stuff. I just installed the beta on Mandrake 7.2  and it works very well. Kivio will be the perfect to diagram drawing and as a replacement for Visio user.\nI personally support Kompany even if it is going to release its products as commercial softwares."
    author: "jamal"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Why would we want yet another *bloated* piece of commercial junk?\nWho would wanna download 100 MB of KDE packages just to use it?\nThey should remove the KDE dependency!"
    author: "Gerbango the Chimp"
  - subject: "Who are these assholes?"
    date: 2000-11-08
    body: "Wow, lots of worms are crawling out of the\nwoodwork today. If you don't like KDE, why\nare you in this forum, oh great one? \n\nMagnus."
    author: "Magnus Pym"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Now wait a sec, you do not need 100MB of KDE packages! If all you want is Kivio, all you need is\nQT\nKDESUPPORT\nKDELIBS\nKDEBASE\n\nand thats it (although I think no programmer's life is complete without SpaceDuel)\n\nThis comes to under 20MB! And besides, hard drive space is incredibly cheap! What was it, 20GB IDE hard drive for $175 or somewhere around there?"
    author: "David Simon"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-10
    body: "The above post was not in any way connected to reality anyway. So, I wouldn't even spend the time to answer him..."
    author: "Anonymous Coward"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-10
    body: "u just did! :)"
    author: ""
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-10
    body: "Yeah, whatever Mr.Troll. If you really want to make a point say something like:\n\n\"Actually, the price of a 20 GB HD is really \nsosososo, heres the url of where you can buy it from that price. The above argument doesn't really make much sense.\"\n\nAlthough I disagree, i have seen HDs priced that way, if you want to make a point, make it in a civisiled way, using arguments with REAL STATISTICS, and put your name. Otherwise, you're just another troll.\n\nAnd if you're argument about me being \"not in any way connected to reality\" is not about the the hard drive, it's instead about the size of the packages, then you're quite right, I forgot you need the KOffice package for KParts, so it's actually around 25MB. Sorry about that."
    author: "David Simon"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-10
    body: "Yeah, whatever Mr.Troll. If you really want to make a point say something like:\n\n\"Actually, the price of a 20 GB HD is really \nsosososo, heres the url of where you can buy it from that price. The above argument doesn't really make much sense.\"\n\nAlthough I disagree, i have seen HDs priced that way, if you want to make a point, make it in a civisiled way, using arguments with REAL STATISTICS, and put your name. Otherwise, you're just another troll.\n\nAnd if you're argument about me being \"not in any way connected to reality\" is not about the the hard drive, it's instead about the size of the packages, then you're quite right, I forgot you need the KOffice package for KParts, so it's actually around 25MB. Sorry about that."
    author: "David Simon"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-10
    body: "Yeah, whatever Mr.Troll. If you really want to make a point say something like:\n\n\"Actually, the price of a 20 GB HD is really \nsosososo, heres the url of where you can buy it from that price. The above argument doesn't really make much sense.\"\n\nAlthough I disagree, i have seen HDs priced that way, if you want to make a point, make it in a civisiled way, using arguments with REAL STATISTICS, and put your name. Otherwise, you're just another troll.\n\nAnd if you're argument about me being \"not in any way connected to reality\" is not about the the hard drive, it's instead about the size of the packages, then you're quite right, I forgot you need the KOffice package for KParts, so it's actually around 25MB. Sorry about that."
    author: "David Simon"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Oh great, first everybody complains about Gnome dependencies and now KDE dependencies.\nWhat's next? Remove GTK/QT dependency and move to Xlib only.\nThen remove X dependency and implement your own graphics engine?"
    author: "Anonymous"
  - subject: "Excellent Work, and Thanks for the GPL"
    date: 2000-11-08
    body: "Personally, I think Kivio has great potential.  I look forward to things like EPS output and anti-aliased canvas :)\n\n\nCheers!"
    author: "Matt Benjamin"
  - subject: "Re: Excellent Work, and Thanks for the GPL"
    date: 2000-11-08
    body: "Hello Matt,\n\nI've looked into creating an anti-aliased canvas.  It's not on the current todo list simply because there is alot of work needed to implement it (as far as I can tell).\n\nI've looked at libart as a means to do this, but it doesn't appear to have text support.  Or am I wrong?\n\nIf it's doable, I'd love to add it because anti-aliased canvases look MUCH nicer than the normal ones.\n\n-dave"
    author: "Dave Marotti"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Shawn,\n\nKivio looks great. I downloaded the RPM's and played with it over the weekend and was very impressed. Thank you for donating this piece of software to our community. When you have a final version, I will probably buy it from your company to return the favor.\n\nI do have a question though. I tried to compile the source for Kivio but got completely lost. I'm used to ./configure..make..make install but I couldn't compile Kivio in this manner. It looks like you built the system with KDE Studio, which I haven't had a chance to try yet. Is that the only way to compile the product or am I missing something silly?\n\nThanks again!\n\nDavid"
    author: "David Joham"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "its like the CVS compiles...\njust use make -f Makefile.cvs previous\nto your normal configure routine...\n\nAlex"
    author: "Alexander Kellett"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-10
    body: "Here's the best part: you won't have to buy it! It's officialy GPL now, meaning that you can get source code (and therefore the program itself) for free! Thanks for this awesome donation to the Open Source community, theKompany!!!"
    author: "David Simon"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Hi!\n<BR>\nShawn Gordon, this software impresses me! The UI looks finished, it hasn't crashed on me once in an hour, and starts up very quickly. Personally, I've never doubted your fine intentions for the KDE project, and those trolls above don't have a fscking clue what they're talking about! Thank you very much!\n<BR><BR>\n   ben\n<BR><BR>\nP.S: Where are the credits, who exactly coded this? (Help->About doesn't say too much...)"
    author: "Ben Adler"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Hi Ben,\n\nThanks for your kind words and support, and if you do run into trouble, make sure to email us at support@thekompany.com, or even if you have suggestions.  We have a list of things we are working through already, maybe we should post them so people know and don't replicate work.\n\nWe are working on the About box, there are some things we need to resolve when we put it in there.  For simple clarification, Kivio is a derivitive work of Queesio by Dave Marotti.  We worked closely with Dave to create Kivio and the primary architect from our group was Max Judin, the author of KDE Studio.  Both guys have done a tremendous job."
    author: "Shawn Gordon"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-08
    body: "Max is responsible for alot of the awesome widgets you see, like the color chooser on the toolbar, the spinboxes, and the draggable stencil docks.\n\nI mostly do the behind the scenes work.\n\nThe about box wasn't really a high priority compared to the functionality of the product.\n\n-dave"
    author: "Dave Marotti"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-10
    body: "Hi,\nBefore the GPL announcement, I was going to ask Shawn if theKompany will integrate this cool new widgets to the rest of KOffice, specially the color chooser. Is this going to happen?"
    author: "Charles"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-10
    body: "Funny you should ask, we were just having a conversation about this yesterday.  Max did a fantastic job on the color chooser widget, and a lot of people seem to like it.  We just need to coordinate it and make sure that the stuff we release is production quality so people don't suffer :).  You should see the stuff showing up in  the very near future."
    author: "Shawn Gordon"
  - subject: "Re: Kivio beta 2 now available"
    date: 2000-11-10
    body: "That's a great news Shawn, I'd love to see the look and feel of all KOffice apps unified -- which isn't really the case nowadays. \nI must say I like very much the custom widgets you created.\n\nBye,\nLukas [lukas@kde.org]\nCzech KDE team"
    author: "Lukas Tinkl"
  - subject: "Can I publish Kivio's Diagram in the web"
    date: 2007-02-12
    body: "\nI'm have one problem, in my job the people use VISIO to draw the diagram and transform in pdf to publish in the web, but we want to publish direct the program and when i see in the browser( NOT IE) i can see the shape's properties.\n\nCan i do this with kivio ???"
    author: "gilmar"
---
The second public beta of <a href="http://www.thekompany.com/projects/kivio/">Kivio</a> is now available from <a href="http://www.thekompany.com">theKompany.com</a>.  Most major distributions are  available and so is the source.  License is now officially GPL.


<!--break-->
<p>
<a href="http://www.thekompany.com">theKompany.com</a> is proud to announce the second beta release of <a href="http://www.thekompany.com/projects/kivio/">Kivio</a> - our premiere flowcharting tool.
<p>The 0.9.1 release features:<br>
- Connector Tool<br>
- Text Tool<br>
- Bug Fixes<br>
- Connections are now saved<br>
- Some small memory optimizations for shapes<br>
- GPL and full source code :)<br>
- binaries and support for more distributions
<p>You can dowload the source code from here: <a href="ftp://thekompany.com/pub/kivio/src/kivio-0.9.1.tar.gz">ftp://thekompany.com/pub/kivio/src/kivio-0.9.1.tar.gz</a>
and <a href="ftp://thekompany.com/pub/kivio/">binary packages</a> for your favorite distro from here:<br>
Mandrake 7.2: <a href="ftp://thekompany.com/pub/kivio/Mandrake-7.2/">ftp://thekompany.com/pub/kivio/Mandrake-7.2/</a><br>
Mandrake Cooker: <a href="ftp://thekompany.com/pub/kivio/Mandrake-Cooker/">ftp://thekompany.com/pub/kivio/Mandrake-Cooker/</a><br>
Slackware 7.1: <a href="ftp://thekompany.com/pub/kivio/Slackware-7.1/">ftp://thekompany.com/pub/kivio/Slackware-7.1/</a><br>
RedHat 6.1: <a href="ftp://thekompany.com/pub/kivio/RedHat-6.1/">ftp://thekompany.com/pub/kivio/RedHat-6.1/</a><br>
RedHat 7.0: <a href="ftp://thekompany.com/pub/kivio/RedHat-7.0/">ftp://thekompany.com/pub/kivio/RedHat-7.0/</a><br>
SuSE 7.0: <a href="ftp://thekompany.com/pub/kivio/SuSE-7.0/">ftp://thekompany.com/pub/kivio/SuSE-7.0/</a><br>
Debian Potato: <a href="ftp://thekompany.com/pub/kivio/Debian-Potato/">ftp://thekompany.com/pub/kivio/Debian-Potato/</a><br>
Debian Woody: <a href="ftp://thekompany.com/pub/kivio/Debian-Woody/">ftp://thekompany.com/pub/kivio/Debian-Woody/</a><br>

<p>Here is the list of volunteers who helped us packaging this release:<br>
<a href="mailto:jaldhar@debian.org">Jaldhar H. Vyas &lt;jaldhar@debian.org&gt;</a>: Debian<br>
<a href="mailto:molnarc@mandrakesoft.com">Christopher Molnar &lt;molnarc@mandrakesoft.com&gt;</a>: Mandrake 7.2 and Cooker<br>
<a href="mailto:koch@kde.org">Michael Koch &lt;koch@kde.org&gt;</a>: SuSE 7.0<br>
A big "Thank you!" to these people for helping us out.
<p>Enjoy Kivio and stay tuned for <a href="http://www.thekompany.com/products/">more cool products</a> from your <a href="http://www.thekompany.com">favorite KDE software developer</a> :).

