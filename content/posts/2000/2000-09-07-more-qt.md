---
title: "More on Qt"
date:    2000-09-07
authors:
  - "numanee"
slug:    more-qt
---
Trolltech and Qt have been getting some good press and support lately. The latest good news includes a strategic alliance with Loki to develop Linux applications.  The LinuxToday article is <a href="http://linuxtoday.com/news_story.php3?ltsn=2000-09-06-007-20-PR">here</a> and the full announce <a href="http://linuxpr.com/releases/2497.html">here</a>. KDE is also potentially a big winner because it is quite easy for application developers to provide a KDE-enhanced version of a Qt app.  Trolltech themselves have designed <a href="http://www.trolltech.com/products/qt/designer/sshots.html">Qt Designer</a> in this manner and have some clever ideas to offer in this department.  In other Qt news, the much talked about Qt 2.2, including of course Qt Designer, has been <a href="http://www.trolltech.com/company/announce/qt-220.html">released</a>.



<!--break-->
