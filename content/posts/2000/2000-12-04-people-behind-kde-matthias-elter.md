---
title: "People Behind KDE: Matthias Elter"
date:    2000-12-04
authors:
  - "wes"
slug:    people-behind-kde-matthias-elter
comments:
  - subject: "But.."
    date: 2000-12-04
    body: "I thought that face belonged to Matthias Ettrich.. at least <a href=\"http://www.kde.org/gallery/index.html#ettrich\">here</a> it does :)"
    author: "ac"
  - subject: "Re: People Behind KDE: Matthias Elter"
    date: 2000-12-04
    body: "Hey cool, I attended the same school in Erlenbach like Matthias did... what an honour :-)"
    author: "Oliver Eichhorn"
  - subject: "Where is Daniel M. Duley (aka Mosfet)"
    date: 2000-12-04
    body: "When will we see something with Daniel M. Duley (aka Mosfet).\n<br>\nWhat happend to his web page www.mosfet.org?"
    author: "KDE USER"
  - subject: "Re: Where is Daniel M. Duley (aka Mosfet)"
    date: 2000-12-04
    body: "He is scheduled:\n\nhttp://www.kde.org/people/people.html"
    author: "Anon"
  - subject: "Re: Where is Daniel M. Duley (aka Mosfet)"
    date: 2000-12-04
    body: "I asked Daniel for the interview 2 months ago but up till now he didn't reply. I guess he doesn't have time or just doesn't like to have his face on a webpage ;-)"
    author: "Tink"
  - subject: "Re: People Behind KDE: Matthias Elter"
    date: 2000-12-04
    body: "Why is there no sched about Thorsten Rahn, one of our mi graphic designers?"
    author: "predator 1710 of Valor"
  - subject: "Re: People Behind KDE: Matthias Elter"
    date: 2000-12-04
    body: "Same story as Daniel, I contacted Torsten a couple of times, a few months ago, but got no reply. To me that's a sign people aren't interested. I'm will contact some other designers though."
    author: "Tink"
  - subject: "Re: People Behind KDE: Matthias Elter"
    date: 2000-12-05
    body: "I'll try to type a few lines before xmas ...\n\nGreetings,\nTackat"
    author: "Torsten Rahn"
  - subject: "Re: People Behind KDE: Matthias Elter"
    date: 2000-12-05
    body: "Tink, it says on the fram \"Last Week\" but that is bad grammar and implies you are a superperson that can interview 10 people in a week ;)\n<p>\nOtherwise, cool. Realeasing KDE2 is a \"boring job?\" Hm, looked more like \"hectic\" to me :)\n<p>\nJason"
    author: "Jason"
  - subject: "Re: People Behind KDE: Matthias Elter"
    date: 2000-12-05
    body: "Well, I am of course a 'superperson' ;-0. But since it is considered bad taste to show that so obvious, I will change the button ;-)\n\nThanks for the compliments though!\n\n--\nTink"
    author: "Tink"
---
In the latest installment of <a href='http://www.kde.org/people/people.html'>The People Behind KDE</a>, <a href="mailto:tink@kde.org">Tink</a> brings us an interview with <a href='http://www.kde.org/people/matt.e.html'>Matthias Elter</a>, release coordinator for KDE2.0 and <a href='http://www.kdenews.org/972916165/'>everyone's favorite kicker hacker</a>.  <i>"I got involved with KDE after reading the "call for help" c't article on KDE AlphaI written by Kalle in 1997. It seems like a large number of todays core developers got attracted by that article. Thanks, Kalle!"</i>

<!--break-->
