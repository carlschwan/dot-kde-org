---
title: "Three's Company"
date:    2000-10-08
authors:
  - "numanee"
slug:    threes-company
comments:
  - subject: "Re: Three's Company"
    date: 2000-10-08
    body: "I downloaded that opera browser too, but it just kept crashing. Opera need to take a look at Konqueror's code to see how to make a stable browser - oh! IMHO of course :)"
    author: "InfoSec"
  - subject: "Re: Three's Company"
    date: 2000-10-08
    body: "Didn't crash while I was using it for what it's worth.  Didn't use it for all that long though (slow X connection).  I'll definitely be giving it another test drive.\n\n-N.\n"
    author: "Navindra Umanee"
  - subject: "Re: Three's Company"
    date: 2000-10-08
    body: "They should really copy code from Konqueror.\nThen we'll have another GPL'ed browser ;-)"
    author: "Anonymous"
  - subject: "Re: Three's Company"
    date: 2000-10-08
    body: "It might be QClippboard - I red somewhere that there was a bug in the X11-version which could cause a crash when dealing with the clippboard.\n\n--\nAndreas"
    author: "Andreas Joseph Krogh"
  - subject: "Re: Three's Company"
    date: 2000-10-08
    body: "I tried the new Opera and was very impressed.  It's very smooth and very fast with lots of features - has a unique feel and the design and layout is better than any other browser that I've used.  It's much nicer than what I remember Opera for Windows being like a year or so ago.  The tabbed interface/mdi design really shows Qt off well.  I hope they don't revert to a sdi design just for unix users who are used to single document applicatons.\n\nI was most impressed that the download is only 1 meg. and the decompressed binary only 3 megs.  Memory use is also very low, much lower than Mozilla and lower than Kde + Konqueror.\n\nHate to admit this, but I found Opera for Linux to be a lot more fun to use and more intuitive than Konqueror.  Of course Konqueror is now very stable.  Opera does crash after a while on page switching but the bugs will be fixed.  It seems very close to release quality or at least official beta quality.\n\nKde should take a look at the gui design for Opera and consider some of those ideas in the future.  Everything just seems so much cleaner and more attractive (even though Kde2 is looking very good itself) using the same Qt toolkit Kde uses. Kde could use some artistic/ui design help, especially with alternate icon designs.  Maybe I am missing something here, but with all the talented artists and designers in Europe at least a few would be happy to help Kde for free, just to advertise their own designs.\n\nToo bad that Opera costs $35 American and that the current (free) alpha version is not quite stable enough for daily use."
    author: "John"
  - subject: "Re: Three's Company"
    date: 2000-10-08
    body: "Its running quite decent over here, with occasionally a crash. Friends of my, who tested Opera ass wel, noticed that it runs decent on Debian, but not on SuSE, or RedHat....\n\nRinse"
    author: "Rinse"
  - subject: "Re: Three's Company"
    date: 2000-10-08
    body: "It runs very well on my SuSE 6.3 with QT 2.2.0\ninstalled - but needs some improvements of cource."
    author: "Erich"
  - subject: "Re: Three's Company"
    date: 2000-10-10
    body: "Runs superbly under Debian Woody.\nwhoohoo.\n\nOne thing I don't get: IE and Opera icons on the buttonbar are greyed out by default, and then onmouseover the light up. Isn't this counter-intuitive? and non-standard?\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Three's Company"
    date: 2000-10-10
    body: "It, Opera Version - [4.0b1], runs well enough on Caldera 2.4 too. My primary Linux machine, Linux From Scratch (http://www.linuxfromscratch.org/), runs both KDE2 and Opera superbly.\n\nAMD Athlon 600, 128MB RAM\nLinux 2.2.17\ngcc version 2.95.2 19991024 (release)\nglibc 2.1.3\nqt 2.2.0\n\nAs for the icons and such... They say this is one of the primary areas they need to fix up proper. It still has bugs for sure in this area, and you cannot save all preferences between sessions, but it is getting better. Now that they have released the first beta version, I'll be keeping up with the releases to test them out. I couldn't run any of the Alpha releases. They segfaulted all over the place.\n\nI used Opera (3.62) religiously when I had windoze 95 way back when... I'll gladly buy version 4 for Linux. I do wish it was open source though. I'm not a programmer, but whatever they use to make their rendering engine work IMHO renders pages better than anything else. I was always fond of this little browser, as opposed to IE or Netscape on windoze.\n\nWhat they have changed between version 4 &amp; 3.6x is definitely going to be stellar and much better than it was before.\n\nI like Konqueror though, now that it can do CSS and such. Konqueror does have a couple of rendering oddities, but I can't complain since I'm no programmer... ;-)\n\nIt'll be a hard choice for me Konqi .vs Opera. I'll settle for both though..! :) Shoot, KFM on 1.1.2 is great too. I still use it!\n\nJeffrey"
    author: "Jeffrey Neitzel"
---
Three quick news items for the sake of completeness and because they were submitted: (1) Trolltech has <a href="http://www.trolltech.com/developer/changes/2.2.1.html">released</a> Qt 2.2.1, a bugfix release with minor enhancements.  KDE 2.0 will be based on this one.  (2) Version 0.0.2 of KDB, the database APIs being developed by theKompany, is <a href="http://www.thekompany.com/projects/kdb/">out</a>.  There seem to be quite a few improvements for a 0.0.1 increment, including a new KControl module.  (3) A new Opera browser is <a href="http://www.opera.com/linux/index.html">out</a>, based on Qt 2.2.x.  I downloaded a remarkably small Debian package for a no-hassle install, and I must say it's really looking good now. Does a decent job of rendering KDE Dot News.  Here's to hoping for future KDE integration -- the more browsers the merrier.




<!--break-->
