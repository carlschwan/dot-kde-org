---
title: "People Behind KDE:  Rik Hemsley"
date:    2000-11-07
authors:
  - "Dre"
slug:    people-behind-kde-rik-hemsley
comments:
  - subject: "Re: People Behind KDE:  Rik Hemsley"
    date: 2000-11-07
    body: "Empath? Where can I read more about it?<br><br>\n\nWhat about Aethera (earlier called Magellan)?<br>\ntheKompany is working on it, and the first beta should be made available this month.<br><br>\n\nMore information <a href=\"http://www.thekompany.com/projects/aethera/index.php3?dhtml_ok=0\">here</a>.<br><br>\n\nI can't find anything about which license it will use, so maybe that's the reason for two seperate products?"
    author: "Joergen Ramskov"
  - subject: "Re: People Behind KDE:  Rik Hemsley"
    date: 2000-11-07
    body: "It will be a GPL license for the client application."
    author: "Shawn Gordon"
  - subject: "Re: People Behind KDE:  Rik Hemsley"
    date: 2000-11-07
    body: "Thanks for the info - which made me more curious ;-)<br><br>\nWhat license will be used for the server? Or that is maybe not decided yet?\n<br><br>\nWhat about the first public beta is it still scheduled to be released in november?"
    author: "Joergen Ramskov"
  - subject: "Re: People Behind KDE:  Rik Hemsley"
    date: 2000-11-07
    body: "The server is a closed source commercial product, we have to make money somewhere :).  It is designed to allow small to medium companies, or even large and huge companies, able to implement groupware and workflow control for a much lower cost than Notes or Exchange with a lower cost of ownership as well.  We are still working on all the details in terms of what the server will handle, but coding has already started.\n<br>\nRight now we are still anticipating that the first Aethera public beta will still be in November.  I've had to pull a couple of developers to finish some other projects, so we got slightly delayed."
    author: "Shawn Gordon"
  - subject: "Re: People Behind KDE:  Rik Hemsley"
    date: 2000-11-07
    body: "> I can't find anything about which license it<br>\n> will use, so maybe that's the reason for two<br>\n> seperate products?\n<br><br>\nThree separate products. Magellan is not dead.\nhttp://www.geocrawler.com/archives/3/3184/2000/11/0/4620343/"
    author: "Hasso Tepper"
  - subject: "GPL is bad!"
    date: 2000-11-07
    body: "Am I the only one who knows that the GPL is *viral*?\nLook at all the restrictions!\nKDE made a mistake by using the GPL.\nThey should have used the BSD license instead!\n\nCompare the BSD license with socialism:\n\"You can have this. And you can do whatever you want.\"\nCompare the GPL with communism.\n\"You can't have this. You can only do what we tell you.\"\n\nAnybody who doesn't agree is ignorant to the reality.\n\n(actually I'm only quoting some posts of some stupid Slashdot poster)"
    author: "Anonymous"
  - subject: "Re: GPL is bad!"
    date: 2000-11-07
    body: "Why do you quote things which are plainly stupid and wrong ?"
    author: "Lenny"
  - subject: "Re: GPL is bad!"
    date: 2000-11-07
    body: "Give your comments to those stupid Slashdotters, not me."
    author: "Anonymous"
  - subject: "Re: GPL is bad!"
    date: 2000-11-08
    body: "You're the one who chose to post the comments in here. Take responsibility for your own actions!"
    author: "Another anonymous"
  - subject: "Re: GPL is bad!"
    date: 2005-03-03
    body: "Yes gpl is really bad.. and has a lot of restriction\nBsd is the best!\nCeers\n\n\nOpen sould be really open.. better closed source then.."
    author: "none"
  - subject: "Re: GPL is bad!"
    date: 2006-01-17
    body: "Troll...."
    author: "Anonymous"
  - subject: "Re: People Behind KDE:  Rik Hemsley"
    date: 2000-11-07
    body: "I actually found this quite inspiring: it shows people that they can go (almost) straight from menial job to being a productive KDE programmer - most people think they have to have 10 years C++ to be any help."
    author: "Mike Hunt"
  - subject: "Re: People Behind KDE:  Rik Hemsley"
    date: 2000-11-07
    body: "well this kid is probably very smart."
    author: "krazyC"
  - subject: "Re: People Behind KDE:  Rik Hemsley"
    date: 2000-11-07
    body: "Probably. But I'm sure a lot of KDE users are of this level of intellect (degree from good university), but just think of C++ as one of those things they wish they'd got around to learning."
    author: "Mike Hunt"
  - subject: "Re: People Behind KDE:  Rik Hemsley"
    date: 2000-11-07
    body: "true. i see what you mean."
    author: "krazyC"
  - subject: "Re: People Behind KDE:  Rik Hemsley"
    date: 2000-11-07
    body: "very inspiring indeed.\n\ni'm a pascal (delphi) programmer, but right now i'm studying c++ to write my first c++ (kde) app."
    author: "caatje"
  - subject: "Re: People Behind KDE:  Rik Hemsley"
    date: 2000-11-07
    body: "Hi!\nDo you remember what the joke about gzip was? ;-)\n\nF@lk"
    author: "F@lk"
---
This week, in <A HREF="http://www.kde.org/people/people.html">The People Behind
KDE</A>, <A HREF="mailto:tink@kde.org">Tink</A> interviews <A HREF="http://www.kde.org/people/rik.html">Rik Hemsley</A>, who is actively working on Empath, a
groupware project much like Outlook for the KDE PIM project.  In fact, his claim to fame is that "<EM>people seem to know me as the Empath man, but it's not even released yet</EM>."

<!--break-->
