---
title: "State of the aRts"
date:    2000-09-22
authors:
  - "numanee"
slug:    state-arts
comments:
  - subject: "Re: State of the aRts"
    date: 2000-09-22
    body: "Hmmm ... knotify never worked for me on FreeBSD.\nArts also never works when started from 'startkde' script (seems to work when started by hand though)."
    author: "fura"
  - subject: "Re: State of the aRts"
    date: 2000-09-27
    body: "aRts is definitely a great piece of software.\nIMO most people do not know the capabilities it\nhas under the hood. \n<p>\nMaybe we (Kde) should have some\ndecent demos that run out of the box to show what\nit can do. I remember the presentation I saw.\nThese effects that you can plugin are great: echo,\nmixing, etc. Definitely something that makes it\nspecial."
    author: "Stefan Taferner"
  - subject: "Re: State of the aRts"
    date: 2000-09-27
    body: "Yeah, I bet!  The \"analog realtime synthersizer\" part always trips me up though, never know what to make of that.  His site is really technical and sometimes it's hard to know what he's talking about.  ;-)"
    author: "KDE User"
---
Stefan Westerfeld has a neat <a href="http://space.twc.de/~stefan/kde/status.html">document</a> up summarizing the status of <a href="http://www.arts-project.org/">aRts</a> in the KDE 2.0 final beta preview.  Most of the work of the past year has been focussed on integrating aRts into KDE.  The result of those efforts is a new multimedia middleware known as <a href="http://space.twc.de/~stefan/kde/arts-mcop-doc">MCOP</a>, a brand new notification system, a brand new optimized sound server, kaiman, Brahms, and more.