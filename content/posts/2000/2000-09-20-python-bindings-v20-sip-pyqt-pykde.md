---
title: "Python bindings v2.0: SIP, PyQt, PyKDE"
date:    2000-09-20
authors:
  - "numanee"
slug:    python-bindings-v20-sip-pyqt-pykde
comments:
  - subject: "Re: Python bindings v2.0: SIP, PyQt, PyKDE"
    date: 2000-09-22
    body: "<P>This news brings me to a question I have had on my mind for a while now. KDE developers: why is it that so little importance seems given to bindings in the release cycle?</P>\n<P>I am sure that many developers strongly approve developing language bindings for KDE. But the release plans do not reflect this. There are no formal plans for any bindings for KDE. No release will be delayed if no-one does them. kde-core-devel does not mobilize to make them happen. They are treated as an optional add-on, a non-core application.</P>\n<P>Counter-example: Recently, it was discovered that the KDE news reader was out of shape. A long thread ensued on kde-core-devel about possible replacements. Everyone seemed to agree that a good one was needed in the regular KDE distribution, and several people were willing to go out of their way to make sure one was found/coded. I have never seen any discussion of the sort regarding KDE bindings, for Python or otherwise.</P>\n\n<P>I am a beginning programmer who has only learned Python, so this concerns me personnally. I am now coding something for which I will write a front-end. Question: What do I want to tell my potential users?</P>\n\n<OL>\n  <LI><I>\"Go get these snapshot KDE bindings. The KDE project does not support them, but they work really well for me and compile without a hitch on my box. The download is just under foo megs. You can find install instructions for the bindings on the website.\"</I></LI>\n  <LI><I>\"Just grab and run! You need the Gnome bindings, but if you have Gnome, you probably have them already. They are part of the standard gnome packages, and ship with Helix and most distributions.\"</LI></I>\n</OL>\n\n<P>If the bindings exist, shouldn't they ship with KDE? (Maybe plan for inclusion in KDE 2.0.1?) I would really prefer to code for KDE, and I am sure that I am not alone facing this problem.  Thanks for your attention! :-)</P>"
    author: "Vad"
  - subject: "Re: Python bindings v2.0: SIP, PyQt, PyKDE"
    date: 2000-09-22
    body: "I basically agree with you. It seems bindings never were a priority for the KDE project. Wouldn't it be good to give a little more importance to this? Or is this considered a side project?"
    author: "ticon"
  - subject: "Re: Python bindings v2.0: SIP, PyQt, PyKDE"
    date: 2000-09-22
    body: "Last time I checked PyKDE didn't support KDE 2.0.\nSo how could a python binding be considered in\nthe release cycle of KDE 2.0?"
    author: "Bernd Gehrmann"
---
Phil Thompson <a href="http://lists.kde.org/?l=kde-announce&m=96894384008196&w=2">announced</a> updated Python bindings for Qt 2.2.0, including pyuic which compiles Qt Designer files to PyQt code, and even Windows support.  The bindings for KDE1 have been updated as well, although a port to KDE2 is still in the works.  Project website <a href="http://www.thekompany.com/projects/pykde/">here</a>.



<!--break-->
