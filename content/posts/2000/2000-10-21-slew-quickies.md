---
title: "Slew of Quickies"
date:    2000-10-21
authors:
  - "numanee"
slug:    slew-quickies
comments:
  - subject: "Re: Slew of Quickies"
    date: 2000-10-21
    body: "I've brought up the KControl layout many times in the lists and e-mails with developers, but no one seemed to be interested in fixing it, even when I provided a nice layout for it... \n\n/me needs to learn C++"
    author: "Chris Aakre"
  - subject: "Re: Slew of Quickies"
    date: 2000-10-21
    body: "What layout did you propose?  I'm so annoyed at this myself, but haven't had time to properly figure out a layout.\n\nJust yesterday I was pulling out my hair trying to find the option to turn off KDE colors for non-KDE apps.  Turned out it's under Preferences->Look&Feel->Themes->Style. ;)\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Slew of Quickies"
    date: 2000-10-22
    body: "Hmm, I'll have to look around to see where I saved it. I'll e-mail you with the design I proposed, and if I can't find it, I'll just re-make it."
    author: "Chris Aakre"
  - subject: "Re: Slew of Quickies"
    date: 2000-10-23
    body: "Not to respond to my old message :D But heres my proposed KControl attached to this message (It's kind of long)"
    author: "Chris Aakre"
  - subject: "Re: Slew of Quickies"
    date: 2000-10-23
    body: "Yeahyeahyeah!!!\nThis is exactly what KDE needs!  I like the idea of putting the control center inside Konqueror.  I also really like the idea that the control center should be expanded to be able to configure more stuff outside of KDE, like internet connections (or how about a panel that configures kernel modules or X settings?).  I know it's a big project to do that sort of stuff, but it would go a LONG way towards making Linux as easy to use as Windows.\n   Even OS- and hardware-specific stuff could be made into control panels.  We could program it so only the control panels that are relevant to the specific configuration of the computer are shown.  If Linux is really going to replace Windows, this is the kind of stuff that needs to happen.  In my opinion, the biggest thing keeping users away from Linux is the lack of one single, graphical interface that can control most aspects of the system (like the Windows or Mac Control Panel).\n\nIf you program it, they will come (from Windows and GNOME)!"
    author: "James D"
  - subject: "Re: Slew of Quickies - suggestion"
    date: 2000-10-26
    body: "Well this problem could be delt with.\nThe indexing of keywords has to be extended\nIt's just a pitty, that searching for colors does not find \"Style\".\n\nSo my suggestion - improve the CONTENT of the great search facility.\n\nNothing against a \"better\" designed tree, but a novice will NEVER understand the best designed tree, because he/she does not understand the meening of the words."
    author: "Ferdinand Gassauer"
  - subject: "Re: Slew of Quickies"
    date: 2000-10-21
    body: "Well thank you for the support.  I was the one who wrote the KDE2 and Konqy reviews.  I have complained about this, but I get the 'It'll be fixed in 2.1' answer.  Lets cross our fingers or contribute :)"
    author: "Kevin Breit"
  - subject: "PW Manager for Konquerer?"
    date: 2000-10-21
    body: "Does anyone know if Konquerer includes any password management features?\n\nIf not, is there a decent standalone PW manager for KDE2? I currently use kpasman, but it pales in comparison to Gnome's Figaro...."
    author: "JC"
  - subject: "Re: PW Manager for Konquerer?"
    date: 2000-10-21
    body: "No, it doesn't.  It's a feature which has taken a lot of flack on the lists due to insecurity problems.  I am not sure about a 3rd party one, maybe one should be written."
    author: "Kevin Breit"
  - subject: "Re: PW Manager for Konquerer?"
    date: 2000-10-21
    body: "I can imagine the security concerns...\n\nMy web passwords just seem to multiply like rabbits though. I've probably got 15-20."
    author: "JC"
---
VoteZone is running an interesting <a href="http://www.votezone.com/messages/messages.asp?MID=62177">review of KDE2</a> and this <a href="http://www.votezone.com/messages/messages.asp?MID=61705">comment on Konqueror</a>.  The review is fairly positive and has several good points, including the criticism levelled at the current Control Center layout. 

LinuxNews.com has an <a href="http://www.linuxnews.com/stories.php?story=481">interview with David Sweet</a>, who talks about his experiences with KDE and the Open Publication of the <a href="http://developer.kde.org/documentation/tutorials/kparts/">upcoming</a> KDE 2.0 book.

<a href="mailto:renai@student.unsw.edu.au">Renai Lemay</a> submitted this <a href="http://www.linuxtoday.com.au/r/article/jsp/sid/263188">link</a> from over at Linuxtoday.com.au.  It is primarily an article about StarOffice, but mentions KOffice in a favorable light.  This week's LWN has <a href="http://lwn.net/2000/1019/">an excellent analysis</a> as well.
Finally, <a href="mailto:david-at-usermode-dot-org">David Johnson</a> submitted this <A HREF="http://linuxtoday.com/news_story.php3?ltsn=2000-10-20-014-04-OP-DT-SW">link</A> to Raph's Open Letter over at LinuxToday concerning GNOME/KDE politics and Ghostscript.  David is interested in opinions from KDE users and developers regarding this matter.

<!--break-->
