---
title: "KDE Documentation and Localization Meeting"
date:    2000-09-29
authors:
  - "Dre"
slug:    kde-documentation-and-localization-meeting
---
<A HREF="mailto:ebisch@cybercable.tm.fr">Eric Bischoff</A> today filed a <A HREF="http://www.kde.com/maillists/show.php?li=kde-devel&v=list&m=315117">detailed report</A> on the successful conclusion of "the first meeting of the KDE translation and documentation teams".  Some excerpts from the report are included below.



<!--break-->
As Eric explains:

<BLOCKQUOTE>
<P>The purpose
       of the KDE Documentation and Localization Workshop was to give people
       involved in KDE's documentation and translation an opportunity to:</P>
<UL>
<LI>meet</LI>
<LI>define policies and priorities,</LI>
<LI>coordinate work, and</LI>
<LI>solve technical issues.</LI>
</UL>

<P>This success demonstrates that there are strong energies willing to
       bring Unix Operating Systems and Free Software to the citizen with a
       documented, easy-to-use graphical interface, in their own language. This
       is an approach that proprietary operating systems are unable to
       sustain consistently.  Now that computers have come to everyday life,
       this is putting a threat on cultural independance of many countries with
       respect to American languages. On the contrary, all the people attending
       the workshop are committed to making computers - even using powerful
       Operating Systems like Linux - more easy to use for everyone.</P>

<P> . . . </P>
<P>
The KDE project (http://www.kde.org) represents more than 50 languages
       by now . . . .</P>
<P>Persons having collaborated every day for years, either on a voluntary
       or on a professional basis, were happy to meet personnaly for the first
       time. The ambiance was nice and productive.

</P>
<P>
Covered areas of work included:
</P>
<UL>
<LI>applications translation,</LI>
<LI>documentation writing,</LI>
<LI>documentation translation,</LI>
<LI>web servers translation, and </LI>
<LI>coordination with other projects.</LI>
</UL>
<P>       All the subjects of the agenda have been examined (with the exception of
       the demonstration of the software tools that each team uses in prevision
       of a future integration). A survey of the current situation has been
       established and we reached a consensus on the short and long term plans
       for the future.
</P>
</BLOCKQUOTE>


