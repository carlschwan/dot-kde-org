---
title: "Quickies:  Release Schedules; Magellan Milestone1 Released; KDE Threading; KDE and Schools; Konqueror Review"
date:    2000-11-27
authors:
  - "Dre"
slug:    quickies-release-schedules-magellan-milestone1-released-kde-threading-kde-and-schools
comments:
  - subject: "PR hype applied here ..."
    date: 2000-11-27
    body: "is there any changelog for future version 2.1 to let people drool about ?"
    author: "AC"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-27
    body: "The changelog is still being written, but you can read the latest version direct from the CVS by using the web interface. Click the 'download' link on the page below to view the latest text.\n<p>\n<a href=\"http://webcvs.kde.org/cgi-bin/cvsweb.cgi/www/announcements/changelog2_0to2_1.html\">changelog2_0to2_1</a>"
    author: "Richard Moore"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-27
    body: "For now...\n\n<h2>New in KDE 2.1</h2>\nThis page tries to present as much as possible of the problem\ncorrections and feature additions that occurred in KDE between the 2.0\nand 2.1 releases.\n\n<h3>Kicker</h3>\n<ul>\n  <li>Support for icon zooming. When this feature is enabled the icons in the \n    panel zoom to a larger size when the mouse passes over them.</li>\n  <li>New panel extension API</li>\n  <li>External taskbar re-added</li>\n  <li>Support for sub-panels. These sub-panels can contain buttons, applets etc. \n    as usual, they can also be a different size to the main panel if you want.</li>\n  <li>Support for WindowMaker dock applets</li>\n  <li>Kasbar included. Now uses the extension API and supports transparency, window \n    menu, and window thumbnails.</li>\n</ul>\n<h3>Konqueror</h3>\n<ul>\n  <li>Improved file name completion</li>\n  <li>In-place renaming of files</li>\n  <li>Image thumbnail feature extended to support text files as well.</li>\n  <li>Improved enabling/disabling of menu items, and location bar not being filled on startup</li>\n  <li>Bookmarks are now stored as XML (old-style bookmarks are automatically imported)</li>\n  <li>New bookmark editor</li>\n  <li>Improved support for window profiles: these now have dynamically assigned \n    keyboard shortcuts, and view linking is now correctly saved in the profile.</li>\n  <li>Toolbar settings are now saved automatically</li>\n  <li>Devices can now appear in the directory tree and will be mounted on demand</li>\n  <li>Automatically chooses web browsing profile when opened on HTML files (even \n    if they're local)</li>\n  <li>The properties dialog now supports multiple files, and even allows to apply\n    permission changes recursively</li>\n  <li>Many improvements in the list views (speed and features)</li>\n  <li>Persistent history</li>\n  <li>Drag and drop improvements (drop onto a web page, drag the location label...)</li>\n</ul>\n<h3>KHTML</h3>\n<ul>\n  <li> Java support improved - better security, now uses the Java 1.2 URLClassLoader \n    and friends to download applets. Note that the code now requires at least \n    JDK 1.2 (or compatible).</li>\n  <li>New 'transitional mode' used when parsing old HTML, allows greater compatibility \n    with badly-formed pages etc. without causing a performance hit for pages that \n    use HTML correctly. This change is invisible to users.</li>\n  <li>Many other fixes</li>\n</ul>\n<h3>KIO</h3>\n<ul>\n  <li>Proxy auto-configuration implemented</li>\n  <li>Support for proxies that need authentication</li>\n</ul>\n<h3>Icons</h3>\n<ul>\n  <li>The standard icons are now 34x34 instead of 32x32 to allow the use of the \n    alpha channel for shadows.</li>\n</ul>"
    author: "GeZ"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-27
    body: "One feature that I really miss in KDE and this feature is available on all GTK based apps is the auto key assignments:\n\nYou click on the menu, point your mouse to an option, click set of keys (example - ALT Q) - and that's it - it's assigned for good. You can also remove it easily..\n\nAny chance that KDE will have this feature?"
    author: "Hetz Ben-Hamo"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-27
    body: "<p><em>One feature that I really miss in KDE and this feature is available on all GTK based apps is the auto key assignments:\n\n                  You click on the menu, point your mouse to an option, click set of keys (example - ALT Q) - and that's it - it's assigned for good. You can also remove\n                  it easily..\n\n                  Any chance that KDE will have this feature?</em></p>\n<p>\nThis was discussed quite a bit and discarded as being undesirable from a HUI perspective.  Why, you may ask?  Because programs can then have inconsistent key strokes to perform the same task, it was argued.  For example, if you change app A to use Ctrl-P for pasting, but in app B it's printing, well, it gets confusing.  So in KDE it's all done globally in the control panel.</P>\n<p>Of course you can make the argument that the GUI should not enforce consistency, or that the mechanism you describe should be a short-cut to the global change.</P>"
    author: "Dre"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-27
    body: ">Devices can now appear in the directory tree and will be mounted on demand\n\nHell Yeah!  But will drives be easily accessible with the \"File Open\" dialog?\n\n\n>The properties dialog now supports multiple files, and even allows to apply permission changes recursively\n\nHell Yeah!  I've been wanting recursive permission changes for ever!  There is one thing.  You should be able to apply recursive permission changes to \"All subfolders\" as well(ignoring files).  This is because you need the \"Enter\" permission on folders, but you don't necessarily want to make all the files executable.  Currently, all my mp3z are marked executable for this reason.\n\nPerhaps something like this(similar to Webmin):\nApply changes to:\n(Drop down box)\nThis Directory Only\nThis Directory & it's files\nAll Subdirectories\nAll Subdirectories & files\n\nPlease!!!"
    author: "Henry Stanaland"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-28
    body: "chmod -R -x *.mp3"
    author: "GeZ"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-28
    body: "heh heh\nnever neglect your console :)"
    author: "dingodonkey"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-28
    body: "Of course I know how to do that.  But this isn't about me.  This is about Joe Sixpack who shouldn't have to use the command line to use his operating system to its fullest."
    author: "Henry Stanaland"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-28
    body: "> >Devices can now appear in the directory treea nd will be mounted on demand\n\n> Hell Yeah! But will drives be easily accessible with the \"File Open\" dialog?\n\nYou can't mount something from the filedialog, sorry. But why not use an automounter for that? I have cdrom, cdr and my jaz-drive all automounted, so I just need to point the filebrowser to /misc/jaz to mount it, for example."
    author: "gis"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-28
    body: "Well, I use supermount, so it is that easy.  But it's just annoying when you are in /home/me/Projects/Programs/ and you have to click, \"Up, Up, Up, Up, /mnt, /mnt/zip\" just to save to a zip.  \n\nWhy can't there be some kind of link that sends me to /mnt/zip automatically?  And if it doesn't have supermount or automount, why can't you just right-click and mount like on the desktop?"
    author: "Henry Stanaland"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-29
    body: "Right-click > Send To  is a very useful feature in Windows. You can put anything in the Send To-folder, like the desktop of another networked computer."
    author: "reihal"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-28
    body: "<p> Another thing that has been added is a possibility to save images that are linked to directly from HTML pages. Excellent for saving screenshots or porn :-)\n<p>What is missing though is showing the pictures during transmission. kview seems towait until the pircutre has been completely loaded. This gives konqueror an undeserved slow feeling during browsing. It is even more amaziong because pictures embedded in HTML are shown during load.\n<P> will this be fixed?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: PR hype applied here ..."
    date: 2000-11-28
    body: "On the same lines, is it possible to view images like gqview on gnome?. I mean, click an image icon on the left pane (file manager view) and view it fully on the right pane? One could click on an icon or thumbnail after navigating to it and view it on the right. Currently, if an icon is clicked, the picture loads in the same window or pane, and it is troublesome to go back to view another (or drag and drop). As I understand it, this could be done with some script. This could be very useful for viewing large number of images.   \nAnother \"problem\" I have noted is concerning transparent images, especially circuit diagrams and technical figures obtained from the net. Click on a thumbnail, and the image opens in a window, with a dark grey background. The background should be white, so that the image can be viewed (it gets camoflaged and hardly visible). I attempted to change this setting but could not find out how."
    author: "rsv"
  - subject: "How about symlinks in konqueror?"
    date: 2000-11-30
    body: "An important feature i didn't found in konqueror is the ability to create and modify symbolic links (just like the way it is done in mc).\nWill this be added in a new version?"
    author: "Renaud"
  - subject: "Re: How about symlinks in konqueror?"
    date: 2000-12-05
    body: "If you drag a file to a new location, you have the choice to symlink, move or copy. A symlink is supposed to point to somewhere. So a \"create symlink\" menuitem is pointless. \n<p>\nIf you still want that:\nIt should be possible to get this \"feature\" by mere configuration of the context menu."
    author: "Rudi"
  - subject: "Re: How about symlinks in konqueror?"
    date: 2000-12-05
    body: "OK, but how about editing existing symlink?"
    author: "renaud"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-27
    body: "This is by far some of the best I've seen conserning PIM's! Finally Evolution's got som heavy competition:-)\nThe KDE-league should really sponsor these kind of projects, which are essencial for desktop-users. I've yet to see a good \"all-in-one-mail-and-global-address-list+contacts\" replacement for Outlook.\nKmail kan compete with Oultook on stability, and security, but not - unfortunetly - on functionalily. I hope Magellan is a bit more MT-designed than Kmail tho...\n\nAnyway - greate work, keep it up!\n\n\n--\nAndreas Joseph Krogh <andreak@nettverk.no>"
    author: "Andreas Joseph Krogh"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-12-03
    body: "I hope it's not going to be all in one! This is one of the things I hate about outlook, my poor old Celeron 450 (or so) is completely paralysed while outlook starts up...\n\nCan we please have lots of little bits? OO and all that, if KOffice can do it, then why not Magellan?\n<BR>\nThink of the advantages:\n<BR>\n<UL>\n<LI>\n1. If I don't like one component I could replace it...\n<LI>\n2. If I don't need one component why install it?\n<LI>\n3. Faster startup\n</UL>\nHoping my prayers have been answered already,\n<BR>\nFriso"
    author: "Friso"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-27
    body: "Magellan looks very nice.\nA question? There is nothing in changelog about\nKOffice. Has KWord been improved lately? It is\ntotally useless because of a nasty bug\nthat has been reported to bugs.kde.org. (fonts\ndissapear on the right side of the page).\nI would like to see KDE2 more stable than it is now.\nBTW, I use Mandrake 7.2."
    author: "Zeljko Vukman"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "The changelog as it stands, only lists the parts of KDE I've been monitoring and some additions David has made. The idea is that developers of the other apps to be included in the release will add the changes for their own apps to the log via CVS.\n<p>\nI don't know what has changed in KWord, so it isn't in the list, but I think that they have been fairly minor in this release so far. At the moment I think 2.1 is much more about improving the core functionality of the desktop, with only some of the application having major updates."
    author: "Richard Moore"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-27
    body: "Hopefully work is done between the Magellan project and KDE to provide a standard contact database, since there are wayy to many. KMail can use it's own, it can use Kab, etc... and then add another one for magellan? I hate re-doing all my contacts.<p>\n\n--\nChris"
    author: "Chris Aakre"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "I AGREE STRONGLY!!!!\nSEVERAL DATABASE FOR CONTACTS IS BAD!\n\nPLEASE ONE DB IN WHICH ALL INFO GO!\nAND OF COURSE THE FEATURE TO BUILD GROUPS FROM\nCONTACT ENTRIES.\n\nAND FOR CHRISTMAS A INTERFACE TO STORE \nALL DATA IN A RDBMS!\n\nBYE Thomas"
    author: "Thomas"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "I didn't see anything posted about speed improvements.  For me windows runs quicker than kde at the moment.  I was hoping speed improvements would be a major factor in 2.1.\n\nIs this being looked at?"
    author: "Shortfella"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "Well, it certainly lists speed improvements in the Konq list views. Which areas are you talking about?\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "Speed improvements for kde on the whole.  Loading all programs seem much slower than under kde1.\n\nIt also has a slower feel than gnome at the moment and doesn't seem as responsive.\n\nTelling people that linux is way better than windows doesn't seem right at the moment for anyone that wishes a good,fast complete desktop."
    author: "Shortfella"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "Funny, I have the opposite experience.\n\nThese last few days I was playing with GNOME to see how far did they get since I last work with it. I used Helixcode's GNOME with all updates and I tried Nautilus as well.\n\nI played on Celeron 400 with 128 MB and GNOME just wasn't as responsive as KDE2 on my machine (which is PII 350 with 128 MB RAM). Using Nautilus got even worse, because beta2 of it is unbelievably memory hungry. When I tried to open Eazel services, it ate 87 MB of RAM.\n\nAnyway, I'm going off here. What I want to say is that I don't share your experience. I'm quite sure others don't share mine with GNOME. On my machine, KDE2 generally isn't slower than KDE1 though some things are. For example, changing background picture when I change desktop takes quite some time, but doesn't bother me much because application windows are ready much sooner so it doesn't really interfere with my work.\n\nIt's not possible to tell if one desktop is too slow relying only on one man experience. However, I do agree that Linux desktops these days seem to need just as powerful hardware as Windows."
    author: "Marko Samastur"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "<p>Have you compiled yourself KDE 2, or just installed a precompiled archive ?<br>\nIn the latter case, it is possible KDE2 wasn't compiled with the -fno-exception switch. Exceptions are not used by KDE, and results in <strong>unused</strong> and <strong>unshared</strong> code that slow down program loading, and bloat memory usage uselessly.</p>"
    author: "GeZ"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "KDE is not Linux"
    author: "dingodonkey"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-29
    body: "I share your experience, that apps need much more time AND cpu-usage when being loaded. I use a pre-compiled version, so doing it myself is next.\nCould somebody who has it at hand post a short list of compiler-options for optimized speed (there was s.th. like --no-opengl, wasn't it?), please?"
    author: "Oliver"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-29
    body: "I also have feeling KDE2 being slower than KDE1.\nFirst, starting the whole thing (restoring all programs) takes eternity. Second, logging out and saving all takes an another eternity. And Konqueror is very slow. I tried Netscape 4.73, new Opera and Konqueror side by side with many different www-pages. Opera is the fastest, Netscape is second and Konqueror is definitely the slowest. Additionally, Konqueror doesn't remember the vertical position of the page like Opera. It is very annoying when I start reading a re-loaded page and it suddenly moves vertically. Opera starts with the right position right away. Much is to be done."
    author: "Eeli Kaikkonen"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-29
    body: "Strange, for me Konqueror is clearly the fastest, followed by Opera and Netscape. Something I haven't seen mentioned anywhere is the dynamic rendering that Konqueror uses, where the pages are displayed right away and refitted as more of the page is processed. Opera and Netscape often process more or less the whole page and then displays it. Not to mention Operas font handling that really sucked on my computer."
    author: "Zank Frappa"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-29
    body: "All these responses about KDE2 being slow strike me as very strange. I'd been using an updated RH6 for the last year, until I switched to Mandrake 7.2 a few weeks ago. I didn't like KDE1, so I'd always used (helix)gnome and sometimes windowmaker. I'd always been really annoyed about how slow and unresponsive things were, especially when doing things like playing MP3s. This is on a 96Mb/K6-2 450. Just changing virtual desktops under gnome would take 1 or 2 seconds to redraw the screen. Windows has always been much faster and more responsive on my machine.\n\nHowever with MDK7.2 and KDE2, I've been extremely impressed. The interface just flies for me (I think this is a GTK+ vs QT issue) and Konqueror is the same speed as, if not faster  than IE5 in windows on the same machine (which is very fast).\n\nI think there are a lot of configuration problems going around, as I've noticed a large divide between people running KDE2 fast and well, and people who think it's slow and buggy. For me, it's better than anything else I've used (BeOS, Windows, MacOS, Gnome)."
    author: "Matt"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-29
    body: "I think KDE2 is super fast.  It sucks up a lot of memory though.  I can't play Unreal Tournament or run Quickbooks Pro unless I'm in Gnome(or have only been logged into KDE a few minutes).\n\nBut it's still superfast.  Konqueror is superfast.  Try resizing Netscape and it takes 5 seconds to come back...Konqueror is instand(like Explorer).  The problem I have is that Konqueror has a 10 minutes lifespan before it crashes."
    author: "Henry Stanaland"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-30
    body: "<p>I have heard many comments that to speed up kde & qt , use the --fno-exceptions option. I used it to compile kde2 & qt. I have also heard that the exceptions result in a lot of unused objects and memory use. I have also noticed that kHTML and some other programs compile with the --fexceptions flag by default. I edited some makefiles to remove --fexceptions but it did not compile (could have missed some makefiles). </p><p>What is the use of exceptions then? If it is useless then this option should be removed to speed up the program. Just curious.</p>"
    author: "rsv"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-30
    body: "I haven't actually compiled without exceptions myself, but here's what I understand: <p>\n\nQt itself doesn't need exceptions, and the *majority* (not all!) of the kde libraries don't use them either.  Therefore, it's safe to turn them off with -fno-exceptions.  BUT, some libraries DO use them, and these libraries explicitly turn on exceptions with -fexceptions (for those of you who compile with -fno-exceptions, I guess).  You shouldn't be taking out the -fexceptions flag in the Makefiles!"
    author: "wes"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-30
    body: "So by this I take it that you're supposed to use the -fno-exeptions flag in the ./configure statement.\n\nI've just compiled kde2 from scratch, and other than there being no documentation to tell you which order to compile the tar files (compile support before libs took me as a bit of a surprise) and kde2 seems to be not much different to kde1 as far as speed goes. However Kmail is increadibly slow!\n\nIs there any documentation on compiling kicking around, and if so can it be put into the ftp site near the distributions section?\n\nFYI I've redhat 6.1 on a cirix?sp? 333 (266MHz) with 128Mb Ram.\n\nWill\n  --"
    author: "will"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-30
    body: "I just installed Mandrake 7.2. My experience is that KDE2 uses a lot more memory than KDE1. For my machine Celeron 333 w/ 64M SDRAM, KDE1 is faster than KDE2, I think. But I just increased my memory to 192M. I think KDE2 is fine now."
    author: "Yufeng Han"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-30
    body: "On my Mandrake 7.2 upgraded to kde2 final, the thing i found very slow is the logout. It take up to 10 or sometimes 15 seconds to exit the desktop.\nIs there anything to do (modify the configuration for example, perhaps recompile from source)?"
    author: "Renaud"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-12-01
    body: "My experience is that KDE2 is faster than windows and KDE1. Konqueror is definitly faster than Netscape on my computer. Nothing beats BeOS though, but I don't think that is possible.\n1.Because BeOS uses a smaller amount of features.\n2.BeOS doesn't use X which seems to be very slow.\nand so on.\n\nAnyway I think that people with small amounts of Ram will find KDE1 to be faster than KDE2. I have experienced that windows have been faster than KDE on earlier installs, but now I'm mostly anoyed about how slow windows is to browse large directory structures for instance."
    author: "Erik Engheim"
  - subject: "Re: Threded code vs SMP?"
    date: 2000-12-02
    body: "I read this thing about thred support in Qt. I have a dual celly 500. Will a threded KDE make it run more like a 1000? I meen, the more small proceses the better performance, rigt? Has this got <i>anything</i> to do with threding? <br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-12-02
    body: "I used to use Linux Mandrake 7.2 on my laptop (128M sdram, 500Mhz Mendocino Celeron, 4.8 G UDMA 33, 15.1\" TFT XGA display, 8M ATI Rage LT Pro, 24x CDROM).  Well, my personal experience is that KDE2 is alot slower than KDE 1.  I've tried a custom compiled version and the Mandrake 7.2 version but both still much slower than KDE 7.2.  It takes forever to open any applications.  In KDE 1, opening, say, kedit is a snap.  In KDE 2, opening kedit takes twice as long!  Also, I think the default MP3 Player that comes with KDE 2 sucks!  It takes forever to load from HDD and uses much more CPU power.  Why bother using Arts Sound when one can have much better performance and little usage by using soundcards native driver, say Maestro Driver supported by Linux 2.4 for my card!\n\nAnyways, Linux in general, whether it's due to X or Linux kernel or bloated KDE 2... was definitely slower than Windows.  But I hate Windows.  I think it's a trash OS to be honest!  So, I'm using FreeBSD.  Unfortunately, FreeBSD 4.2 doesn't come with KDE 2 yet.  Hopefully, FreeBSD 5.0 comes with KDE 2.1 or something better for KDE 2 is simply too slow!\n\nKonqueror is definitely slower than netscape.  It renders so much slower than netscape!  I think netscape still rules.  But then, netscape 4.6 also suck too!  Netscape 4.6 is unbelievably slow!\n\nIt would be better if KDE 2 used smaller memory!  I'd rather switch my desktop manager than to buy another 128M just for KDE!"
    author: "Now a FreeBSD user!"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-12-02
    body: "You were right with most of what you said (kde2 being slow even if you have 224 MB RAM in your box etc) except one thing: I can see kde2-packs on their ftp-Server ftp://ftp.freebsd.org/pub/FreeBSD/ports/i386/packages-4.2-release/kde.\n\nCheers."
    author: "Oliver Immich"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "Is anything going to happen regarding \"GEOMETRY\" As in being able to save your maximized settings or are we all going to have to Open and Click the maximize button every time we want to use anything??\n\n-geomery does no longer funtion on KDE2 links\"\n\nRegards,\n\nJames"
    author: "James Alan Brown"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "In KDE 2.0 it is spelt --geometry. You can use --help-kde to get a list of all the standard command line options for KDE apps.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "I think this is the second time you submit this kind of complaint. Both times with not enough details so that we understand what you mean.\n<p>KDE applications have default profiles that are stored in the configuration and are used for each new instance of the app. This applies for example to Konqueror, except that this particular app is able to handle <i>many</i> such profiles. If you want to get a particular profile as a default (like a maximized size for your konqueror windows) you *need* to save them (use \"Save Options\" before quitting your application).\n<p>On the other hand, quitting a whole KDE section while having applications open (and maximized for example) will bring them to you in exactly the same shape <b>if you choose to save the session at logout</b>.\n<p>Anyways, maybe I understand you wrong, so please provide more details."
    author: "Inorog"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-29
    body: "He got confused and he didn't look in Window (on menu bar) where he can Save View Profile Web Browsing or File Managment. That's because Save settings from Settings (on menu bar) has been removed, and people are not (yet) used to it."
    author: "Zeljko Vukman"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "Ok let my try yet again to make this quite clear to all:\n\nGeometry or to be able pass the parameter -geometry was one of the best features with KDE1/1.1.2 that it seems has not been continued with KDE2.  \n\nI am not talking about Konqueror and its method of saving its defaults!\n\nI am questioning the fact that with this new KDE2 firstly you can no longer pass the - geometry parameter on your kdelink files or can you as it seems save maximized settings in most other programs to fit your desktop. Now you have this half view of any package you open, in order to run it in full screen each time you open it up, you have to maximize it via the buttons. (Is it really that odd to want to open say a word processor in full screen view from default)! \n\nDo you all really think its looks good or is useful having programs that open half way off the screen on your desktop.  And do we need to run another windows manager in order to control geometry as is the case with Gnome?\n\nRegards,\nJames"
    author: "James Alan Brown"
  - subject: "Re: Quickies:  Release Schedules; Magellan Milestone1 Released; "
    date: 2000-11-28
    body: "Well today is the 28th and I am wondering where I can grab a copy of it, I would like to pack it, or try anyway.\n\nBen"
    author: "ben"
  - subject: "FSF is evil!"
    date: 2000-11-28
    body: "So he's yet another RMS lover isn't he?\nJust like the rest of the KDE and Gnome camp.\nWhen do you people realize that GNU is bad?\nThe viral GPL for example, it stands for communism and is restrictive as hell.\nI can't even control my software, how is that freedom?\nAnd RMS, a greater jerk is does not exists.\nHe and his Free Shit Foundation are only looking for power and wants to own everything.\nLook at FSF's site, and read it for yourself: \"Why software shouldn't have owners\".\nAnd unlike the free licenses such as BSD, the GPL is like communism: \"you can't have this, it belongs to us\". Freedom my ass!\n\n<RANT>\nANYBODY DENYING THIS IS A LOOSER AND IGNORANT TO THE REALITY!\nIF YOU WANT TO SHOW YOUR LACK OF INTELLIGENCE, DENY THIS AND YOU'LL BE KILLED BY GOD!\n!@#$%&*^%$&*@!&(*%$@!\n</RANT>"
    author: "Slashdotter"
  - subject: "Re: FSF is evil!"
    date: 2000-11-30
    body: "Looks like people posting insults never put their name."
    author: "Renaud"
  - subject: "Bloatness, be gone!"
    date: 2000-11-28
    body: "Why would we want another *BLOATED* piece of junk?\nAnd why the hell are you using shared libs?\nThat's the worst idea ever!\nEvery piece of software needs libkde.so and hundres of others!\nWHEN ARE YOU GOING TO REMOVE THE BLOATNESS!?!?!?!?"
    author: "Slashdotter"
  - subject: "Re: Bloatness, be gone!"
    date: 2000-11-28
    body: "And what exactly would you do? Link in all libraries statically? Now THAT would be bloat.\n\nWhat do you think shared libraries are good for?"
    author: "Jeremy M. Jansary"
  - subject: "Re: Bloatness, be gone!"
    date: 2006-11-16
    body: "I am so depressed about my boloatness.  3 years ago I had a really series car accident which took me 9 months in hospital, and with the steriods I was given in order to continue to live I gained almost 30 kilos.  Before the accident I was 56 kilos and now I am 84 kilos!  All my life I give extra attention for my figure, that is I never eat oils, butters and sugars and always cook without using any oils.  Now I am more eating salads, vegetables and fruits but I can't loose weight.  What I have is bloatness, since I had to take all rings even from my fingers since even my fingers are really bloated."
    author: "Anna Carabott"
  - subject: "Re: Quickies:  Release Schedules..."
    date: 2000-12-04
    body: "I'd say david faure's release schedule for 2.0.1\nis nothing less than a wild guess.... It's Monday, the Announcement should be out, the files should be out since friday, and guess what can be found on www.kde.org and/or ftp.kde.org... nil."
    author: "Mathias H."
  - subject: "Re: Quickies:  Release Schedules..."
    date: 2000-12-04
    body: "and what a wild guess it was... Monday evening, 20:45 local time (MET), and even the release schedule hasn't been updated..."
    author: "Mathias Homann"
---
The <A HREF="mailto:faure at kde.org">KDE Release Dude</A> has posted updated release schedules for <A HREF="http://developer.kde.org/development-versions/kde-2.0.1-release-plan.html">KDE 2.0.1</A>, a bug-fix and translation release due out next week, and <A HREF="http://developer.kde.org/development-versions/kde-2.1-release-plan.html">KDE 2.1</A>, which will provide new functionality and improvements in many areas of KDE.  The first beta of KDE 2.1 is due out in three weeks.  <A HREF="traeholt@email.dk">Niels Tr&aelig;holt Frnack</A> wrote in to tell us that <A HREF="http://zamolxe.csis.ul.ie/about.html">KAlliance</A> has released Milestone 1 of <A HREF="http://zamolxe.csis.ul.ie/download.html">Magellan</A>, a very promising PIM client (the website seems to be a bit broken); some mouth-watering (but large) screenshots are available <A HREF="http://zamolxe.csis.ul.ie/screenshots.html">here</A>. 
<A HREF="mailto:rik at kde.org">Rik Hemsley</A> has put up an <A HREF="http://www.geoid.clara.net/rik/kde_mt.html">article</a> explaining how one can use multithreading in a KDE or Qt program and why the KDE libs don't use threading.  <A HREF="mailto:molkentin at kde.org">Daniel Molkentin</A> wrote in that <A HREF="http://master.kde.org/mailman/listinfo/kde-edu">KDE-EDU mailing list</A>, which aims to help people that want to bring KDE into schools or to develop educational software, has been reopened.  Finally, <A HREF="http://www.bsdtoday.com/">BSD Today</A>
has publishd a quite favorable <A HREF="http://www.bsdtoday.com/2000/November/Features339.html">review</A> of our favorite browser, <A HREF="http://www.konqueror.org/">Konqueror</A>.






<!--break-->
