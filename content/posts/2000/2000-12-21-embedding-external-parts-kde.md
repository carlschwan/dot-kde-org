---
title: "Embedding external parts into KDE"
date:    2000-12-21
authors:
  - "hporten"
slug:    embedding-external-parts-kde
comments:
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "it's funny how kde gets accused of copying windows, when bonobo is pretty much an interface by interface copy of OLE. it shows great vision that kde was able to move away from CORBA when it didn't work into this beauty of a component system. if that's not enough, kparts/dcop has already shown itself capable of evolving while already providing a strong base functionality.\nnow that's what i call innovation."
    author: "krazyC"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "Their is a reason GNOME has decided to base bonobo on OLE.  OLE is a DAMN good component model.  Sure it's complicated, but it's the best out there."
    author: "John"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "i never said it wasn't. i was alluding to the fact that kde's architecture is innovative. \ni'm no expert, but i think kparts will keep evolving as need arises, and meanwhile it works."
    author: "krazyC"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "Sounds really great. There has been some similar projects going on (providing simplier interface to Mozilla), but it seems that this makes those efforts less meaningful.\n\nHowever, I just have to use this space to say this. As I'm personally running an old P200 64Mb machine, I would appreciate a lot more improvements to the speed and memory usage of KDE (and X) - rather than having all the time great new additions and configuration options. Of course optimization isn't so fun as creating something new, but it would be great if someone from the developement team could become a bit more ascetic and start removing unneeded stuff and cutting the memory and processor usage of KDE. Though Konqueror is much faster than any other browser in Linux, it is still a lot slower than any browser in Windows - including Netscape - and starting even some small games in KDE takes unbelievably long time."
    author: "Tonttu Torvinen"
  - subject: "Re: I would appreciate a lot more improvements..."
    date: 2000-12-21
    body: "> \"I would appreciate a lot more improvements to the speed and memory usage of KDE (and X)\"\n\nI think that the KDE team is also working on it : just see KDE1 and KDE2.\nMoreover, if you think KDE has too much fonctionality, you can use blackbox or IceWM. Konqueror can be use without KDE so you can keep this mervelous browser :)"
    author: "Shift"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "Agree with the above. I'm running a 600 celeron O/C'd to 800 with 128 meg of ram. Performance is no where near what it should be and win ME on this machine really blow KDE/Linux out of the water.\n\nOf course KDE is more than useable but I'm running basically a 800 Mhz PIII. This machine is also setup very well, I used mandrake 7.2 for ONLY the base libs like glibc and the init scrips, basically just enough to compile with and it's very lean. Xfree 4.0.2, QT 2.2.3 (qt-copy inc xft) & 2 day old CVS of KDE.\n\nI realize that optimization isn't as fun as hacking something new but if KDE 2.1 is released with this level of performance it will drive many users on low end machine a way.\n\nOne of the main fingers pointed at Windows for years by the Linux supporters is that windows has a lot of bloat, KDE seems to be following the same path.\n\nGranted a P200 64Mb machine is not exactly state of the art but I do feel that KDE should perform to an useable level on such a system."
    author: "David"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "Hey guys, I run KDE2 on a Pentium 133 with 81MB RAM. You can work quite reasonable with it (better than KDE1 but a bit slower than Win). However, I would very very much like any speed/size improvement I could get!\n\nEspecially, browsing the web (or local directories) is much slower in KDE2 than in windows. Paintshop Pro for example can show me the thumbnails of a directory instantaneously (all images are painted at the same time), whereas Konqy renders every single image one after the other. The same on some www picture sites: Netscape/IE start to build up all images in parallel, Konqy starts the second image only after image one is rendered completly. Is this a bug? Easy to fix?\n\nHowever, KDE2 is much faster than KDE1 or Netscape6 (which is unusable on this machine).\n\nThanks to the KDE Team!!\nChris"
    author: "Christian Naeger"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "I 100% agree that performance is an issue with KDE. KDE2 is a lot slower than using Windows, and I assume we can direct part of the blame to the fact that XFree86 adds an extra layer of bloat. We do have some possible soloutions to this problem, but these soloutions need volunteers to execute them.\n\n1. It seems that much of the problem with KDE performance is down to slow code. If KDE developers were educated more in how to write more efficient coe, this could reduce this problem. A soloution to this could be a section on developer.kde.org dedicated to performance giving tutorials and tips and tricks on writing efficient code.\n\n2. XFree86 is a considerable hog IMHO when it comes down to performance. Although the XFree86 team are doing a fantastic job, reliance on a slow layer below KDE is a natural performance hit. The soloution to this could be a splintered off XFree86 customised to KDE, or developing KDE for the Framebuffer. I have heard that the Framebuffer can be quite slow at higher resoloutions, but I have no idea of the development of the Framebuffer.\n\nKDE running slowly on a 200mHz Pentium is IMHO not acceptable for the user. Performance is boring, but although we can all identify the problem, we need firm volunteers to solve the issue.\n\nOne other idea would be to arrange a tuning week. In this week the CVS could be frozen and only performance tuning commits could be made. I believe this could prove to be a benifit for KDE."
    author: "Jono Bacon"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "<I>\n1. It seems that much of the problem with KDE performance is down to slow code. If KDE developers were educated more in how to write more efficient coe, this could reduce this problem. A soloution to this could be a section on developer.kde.org dedicated to performance giving tutorials and tips and tricks on writing efficient code.\n</I>\n<P>\nHuh ?  We do optimize as needed, but optimization is the last step in coding.  Never ever start optimizing before features are completed unless you are locking for headaches down the road.  However, there were several obvious optimizations done with respect to loading config files and loading already used librabries before KDE 2.0 release.  \n\n<P>\nComparisons with Windows is also IMHO moot since we have no integration into any kernel period. We also do not load every DLL this side of the Mississipi to boast performance.  Well that is not entirely right since we do similar thingsby utilizing kdeinit so that every applications will not attempt to load and re-loacate already loaded libraries, but still not to the extent it is done in Windows.  Anyways IMO neither KDE nor X cannot afford to do things that Windows can for portability reasons, but I personally feel amuzed when people generally accuse developers of not optimizing or not knowing how to optimize before really understanding what issues are involved !! There are many things one needs to consider before going willy nilly into optimazations.  I am sure they will be forth coming in future releases of KDE...\n<P>\nDown the line one thing that might improve speed is multi-threading.  When Qt gets to be muti-threaded then we can do the same thing to KDE and this might indeed boost performace a great deal specially with GUI blocking problems which tends to be the thing most people notice and associate with performance problems...\n<P>\nBTW, on my machine a bare bones installation of Windows 95 was about the same speed my KDE 2.x setup above with all debuging enabled...\n<P>\nRegards,<BR>\nDawit A."
    author: "Dawit A."
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "<p>Good answer, except for the multithreading part.\n\n<p>Multithreading doesn't improve speed, it improves <b>apparent</b> speed, which is quite different.\n\n<p>Multithreading is actually <em>less efficient</em>. On the surface, doing many things at once like a multithreaded program without using real threads seems more complicated, but real multithreaded programming has so many hidden pitfalls and debugging problems that you would probably have found the increased difficulty of singlethreaded code not that great.\n\n<p>The <a href=\"http://oss.sgi.com/projects/apache/\">Accelerating Apache</a> project for example, has managed to make Apache 1.3 up to <em>ten times faster</em> and Apache 2.0 up to <em>four times faster</em> on the SPECweb96 benchmark. A major part of this was ditching threads for their <a href=\"http://oss.sgi.com/projects/state-threads/\">State Thread</a> package, which simulates multithreaded behavior without actually using threads (similar to <a href=\"http://www.gnu.org/software/pth/\">GNU Pth</a>, by the way)."
    author: "Pierre Phaneuf"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "Well, I'm not sure what everyone is talking about with respect to speed problems either. I used to dual-boot win98 and kde 2.0 (on linux) on this machine (until windows stopped booting about a week ago), a plain PII-300 with 128mb, and I have found the speed to be very similar. Speed isn't the only usability concern, and I much prefer kde... and I would even if it was slower.\n<p>\nPerhaps people are using badly built binaries?? First when I tried a kde 2 beta (binaries), I thought that it was really slow but after compiling qt and kde myself, with exceptions disabled, etc., I found kde 2 to be blazingly fast (especially considering what it actually does). When I say blazingly fast I mean it's as fast (sometimes faster, sometimes not quite as fast) as windows while being much more comfortable and configurable.\n<p>\nAnyway, thanks kde developers!"
    author: "KdeFan"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "Okay, to be more precise on the performance thing and to avoid misinterpretation of what could be called fast and what could be called slow in kde2: The launching of apps itself is it which decreased my enthusiasm about kde2. I absolutely agree to what people said above, that it become quite boring to wait for an standard app like kmail or korganizer to come up. I even cannot make the binaries responsible for that since I went through three kde2-releases after final came out and all were the same slow block of software. If I should be wrong, I may ask, why didn't the kde-team give advice to the distributors to do their job 'properly'."
    author: "Oliver Immich"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "<i>If I should be wrong, I may ask, why didn't the kde-team give advice to the distributors to do their job properly.</i>\n<p>\nI think that may be where the real problem is. Maybe the packagers just aren't building optimally (not to complain - their efforts are appreciated). I know that I thought kde2 was slow until I found the thread here on the dot about how to compile kde (and qt in particular) correctly. It has been speedy since then. I used gnome for a week or so, and it wasn't any faster than my kde2 desktop.\n<p>\n Aside from that I can only assume that the speed issue is more subjective than anything, because I just can't relate to your experience. For me, kmail opens quickly (certainly quicker than Outlook did) and I never really feel that I'm waiting for it. Anyway, I hope it gets worked out."
    author: "kdeFan"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "i remember what my former boss told me once, \"you can never ever please everyone, not even if you sweat blood!\".\n\nhere is what i have to say: if you dont like kde then dont use it, it's that simple, u have no right to complain because ur using it free of charge.  kde folks are working hard to give us a nice desktop, we should give them a big thanks first even if some of us find it unuseably right now(tho, i doubt it), just wait, think about just how long kde existed, 1996, its only been 4 years, \n\nu can give constructive criticism rather than complaining, if you have a 5 year old processor, blame it for the slowness, not your 2-month old software. and stop comparing kde to windows, its a very different world.\n\ni also cant imagine people wasting their time arguing about kde icons, its ugly, gnome icons are better, very unpleasant to they eye etc, stupid people."
    author: "japy"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "Reality check..\n\nThis argument does not hold water. If you don't like it, and you don't have the skills to improve it, then the best you can do is attempt to raise the issue. That's how things improve.\n\nSaying \"If you don't like it, don't use it\" is a rather ignorant argument. It's apparent that these people like KDE, but they want to see it improve. They are making good points about the direction KDE needs to go.\n\nWindows and KDE are in competition, and thus comparing the end user experience of KDE with Windows is a valid comparison. We all want KDE and the various platforms it supports to gain wider acceptance, and that will require it to surpass windows in functionality, speed, and ease of use."
    author: "Nick"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-23
    body: ">If you don't like it, and you don't have the >skills to improve it, then the best you can do >is attempt to raise the issue.\n\njust like what i said, deliver constructive criticism, not simply complain about it and make it appear kde is totaly useless.\n\n>Saying \"If you don't like it, don't use it\" is >a rather ignorant argument.\n\nwhat would you feel when your favorite desktop is undersiege?  saying \"i used to run kde 1.1.2on my prehistoric computer and it works lightning fast, while suddenly when i install kde 2 it crawls\" is what i believe a rather ignorant argument.\n\nyes we all want kde to improve, but theres a proper place for this, and thats the bug tracking system.  im sorry if i sounded so rude, i recognize that this is a disccusion and respect everyones opinion, but people wanting to switch to kde and wants to learn more first, might interpret all of this as sign that kde is not a worthy alternative.  \n\nbesides this is dot.kde.org, AFAIK, this site gives out good news and information about the beauty of kde, we are suppose to thank the developers here,  its very irritating just after you have read a nice news about kde, then people will flood in their misseries and griefs etc. (should'nt it be all addressed to complaints.kde.org?)\n\n>Windows and KDE are in competition, and thus >comparing the end user experience of KDE with >Windows is a valid comparison.\n\ni did not say they aren't, i said kde is totally different to windows, when it comes to design, windows is the operating system itself, which makes it more integrated with its compononents, on the other hand kde runs on top of x, which in turn runs on top of linux.  windows can play tricks it wants with regards to resources, but kde can do only what it is permitted by x and linux respectively.  \n\nbut of course, both are desktop, and that is where we can compare windows with kde: windows has intallshield, kde has ./configure, make, make install, kde has virtual desktops, windows doesn't, kde users are cute, windows user's aren't (ha ha ha) and many more.  you see, we can only compare what we can see and feel on the outside, on the inside its totally different.  and that is why kde's performance cannot be directly compared to windows, (not of course if we have a microsoft windows desktop environment for x!)\n\nbut lets simply agree on one thing nick, before we start a \"words war\", lets just be happy kde users!\n\nmay the force be with you all!"
    author: "japy"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "I totally agree.<br>\n<br>\nMore over, Speed of kde2 is the same as win98 on my system.<br>\n<br>\nThe only slow thing is XFree86 V4.01 (Frame buffer is realy slow)<br>\n<br>\nOn thing to improve performace (if you own a very very very old obsolete computer ;-) ) is to choose a simple styles just like windows (without pixmaps).<br>\n<br>\nFunctionnality is very important (IMHO), because it is a race to functionnality. If you quit the race, then peoples moves to the concurents.<br>\n<br>\nI've heard the same things between MUI and BGUI under Amiga (MUI is to KDE what BGUI is to GNOME)\nPeople using 7MHz 68000 expected high drawing speed with fancy bitmaps (they said that it was slower than the standard system (plain colors) and thus, that that was inacceptable! )<br>\nMaybe peoples could ask for high speed on 80386SX16 VGA systems?<br>\n<br>\nI think this attitude is stupid.<br>\n<br>\n<U>The most important thing is an API that allow high developpement speed. That is the most important thing.</U><br>\n<br> at KDE2: This model (C++, Qt, And mode) allowed koffice in one year and lots of applications in few weeks."
    author: "Olivier LAHAYE"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-23
    body: "<i>The only slow thing is XFree86 V4.01 (Frame buffer is realy slow)<i><p>\nNot getting involved in the argument, but I actually thought X 4.0.1 was rather zippy (especially compared with X 3.3.X), but then again, I'm not using Frame Buffer"
    author: "MichaelM"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-23
    body: "My comment was meant to be 'constructive critisism'. Critisism can always be taken as complaining - it just depends on the point of view and the attitude. \n\nSure I currently have quite an old and slow computer, but I see no reason why I should spent a lot of money to upgrade it when I can do most of the things I need to do with this P200 64Mb (I can guarantee that I can imagine hundreds of better ways to spend my money). I'm not interested in having always the latest new gadgets, but what I want to have is to be able to do the things I need to do in the most comfortable way. Even though Win95 runs a lot faster than Linux&KDE, I've noticed KDE to be the best environment for me in the most cases. As the speed is the biggest problem here, I would like to see also this part to be improved. \n\nSorry to say, but what you said doesn't sound very 'open'. Rather than openly discussing the problems people are having with KDE, you would like us to shut our mouths, be quiet and pretend to be fully satisfied - while there still is lot to improve."
    author: "Tonttu Torvinen"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-24
    body: "sorry people!"
    author: "japy"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-24
    body: "my girl went away, so I was said I nervous"
    author: "japy"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-25
    body: "No problem, I was just trying to be constructively critical also to what you said.. ;)"
    author: "Tonttu Torvinen"
  - subject: "Re: Embedding external parts into KDE"
    date: 2003-11-19
    body: "On a P200/160mb mem  I can run XPpro with some services, Mandrake/KDE keeps on swapping back forth on the HD."
    author: "Geert"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-26
    body: "Curiously ha have pretty acptable performance on my ancient P166 with 48Mb.\nSure it is slower in some aspects than windows 98 on the same machine but it is worth it!"
    author: "Joao Cardoso"
  - subject: "Re: Embedding external parts into KDE"
    date: 2001-01-22
    body: "I have also found that KDE and linux was no speed demon compared to windows, and I know to a certain extent why windows seems fast.  However, I have also discovered that there are reasons why linux and kde is slower than it should be.  I am no expert on software and operating systems, but here are a few of the things I found result in slow performance:\n\n1)32 bit disk read/write access is not enabled, nor is direct memory access.(more detail on this at www.frankenlinux.org)\n\n2)the kernel is not customized, as it is the distribution's default kernel install.\n\n3)The current constraints of linux ( for example, reiserfs is supposed to speed up performance dramatically). \n\n4)The x server is a problem for memory usage and rendering speed.\n\nDespite all that, though, it would be nice to be able to have a fast desktop, and if anything listed above can be eliminated, or any further improvements to kde made, that would be great."
    author: "linuxfan"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "This is big news for the Linux desktops and the Linux community.  Anyone want to submit this story to Slashdot?"
    author: "ac"
  - subject: "Embedded bugs"
    date: 2000-12-21
    body: "..and you can be sure that the screenshot is showing Mozilla embedded in Konqueror because it contains three egregious Mozilla-specific rendering bugs!"
    author: "Otter"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "Wow! That's just amazing!\n\nit seems to me KDE is accomplishing something almost of this magnitude every two weeks or so. I'm writing this with antialiased fonts in all my Qt apps, something that most people thought impossible in X a year ago (ok, keithp of X fame has a lot more to do with that than KDE, but patching Qt to use antialiased fonts didnt take long)\n\nLets see - what other amazing things has KDE released since/with 2.0:\n* Konqueror/khtml - Probably the best and fastest linux browser of today. It renders almost everything correctly. Only IE and Netscape work with more.\n* Konqueror - Kickass file browser. \n* KParts/DCOP - As good as microsofts component technologies (whatever they're called this week)\n* Themes - Widget themes are fast and look good.\n* KOffice - Far from being finished, it's already very useable.\n\nand now this.. all i can say is wow! (again) :)\n\nHow will the gnomes and the redmondites ever manage to keep up with the trolls? :)"
    author: "Henrik"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "I don't want to flame, but rather alleviate ignorance here.\n\nGnome has:\n* Galeon - Kickass web browser using GtkMozEmbed\n* Nautilus - already fully usable and able to give Konqueror more than a run for its money\n* Bonobo - a lot more sophisticated embedding framework than KParts (minus the recent X-Parts extension) or OLE2 for that matter\n* Themes - on the GTK level and soon Gnome-wide\n* Gnome Office - AbiWord and Gnumeric simply kick ass. And AbiWord is about to get a KDE port too.\n\nI won't even go into how WinME compares, because you'll dislike that even more... KDE2 kicks some major ass. But I wouldn't discount Gnome or Windows as easily as you do."
    author: "Dom"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "<blockquote>\nI won't even go into how WinME compares, because you'll dislike that even more... KDE2 kicks some major ass. But I wouldn't discount Gnome or Windows as easily as you do.\n</blockquote>\n<p>\nGnome, Windows and KDE each have strong points, and there's a whole lot of cool code in Gnome. My intention wasn't to bash Gnome (or Windows for that matter - I agree that it probably has the best UI of the bunch today).\nThe point I was trying to make is that the <i>rate of progress</i> of KDE is absolutly astounding.. \n<p>\n\n-henrik"
    author: "Henrik"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "Obviously I'm getting slightly off-topic here, but now that you brought up the gnome and redmondite falling behind thing, I suppose there's no harm in sharing my opinions.\n\nSomething that I really liked about Helix Gnome when I tried it, was the easy installation method. Download one installation binary, select the programs/packages you want, and voil\u00e1. Now that's something I'd really like to see in the next major KDE release. \n\nI'm not sure whether it's possible to keep the packages up-to-date with Helix Gnome, but Windows Update seems work just fine for me at least. But it wouldn't be too hard to add it to the installation program, making KDE extremely easy to install and maintain, making it superior to Gnome and Windows in this sense too."
    author: "Ergo"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "If anything changes about KDE installation, I\ndo hope that the installation \"backend\" doesn't\nchange. I like the current feature of most Linux\ndistributions which is called \"package manager\".\nThe package manager ( RPM or whatever ) should do\nthe work. I wouldn't like to see every suite of\nprograms implementing its own package manager,\nbecause then it only becomes harder to manage\nthe software installed on your Linux system."
    author: "Eric Veltman"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "I agree. I think installation via a packet manager ist th best way. I personally had some troubles with the Helix insatller on SuSE. KDE rpm installation was never a problem for me."
    author: "Bernd"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "How about using some kind of apt-frontend? \nApt works great, its clearly the most advanced package manager available. Besides, since apt is now able to handle rpm's there should be nothing to stop us from standarizing on it. Doing so would also stop us from duplicating effort, in creating yet another package management solution."
    author: "baco"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "HelixGnome for Debian is an apt frontend.  The HelixInstaller is generic enough that it would easily work with KDE packages.  A port to QT would not be difficult."
    author: "John"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "HelixGnome for Debian is an apt frontend.  The HelixInstaller is generic enough that it would easily work with KDE packages.  A port to QT would not be difficult."
    author: "John"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "apt-get kdelibs3  (of course, you'd haveta be using debian) :-p   \n\nErik"
    author: "Erik"
  - subject: "Install KDE2 on debian"
    date: 2000-12-22
    body: "First of all you must run woody.\n<p>\nThen, # apt-get install task-kde\n<p>\nThen, you're done ;)\n<p>\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "This sounds great.  Although I'm not a programmer, I can see how this opens a whole new world to KDE. I am impressed with design of KDE2, which seems to make it very easy to expand KDE far beyond it's built-in functionality.\n\nI have recently installed CUPS as my print system. I also installed QTCUPS, which provides a wonderful print dialog.  However, I am not able to configure KDE2 to take advantage of the QTCUPS.  I guess the printing system in KDE2 is hard-coded to the basic lpr system.  Does this announcement mean that it should be easy for someone to integrate QTCUPS into the KDE printing system?\n\nAlso, while I love all the new cool stuff happening with KDE2, I hope the developers add the ability to remember window size and screen position between sessions automatically in the next update.  I know I can manually save window size (in konqueror), but not screen position.\n\nAt any rate, thanks KDE team for all the wonderful work you are doing.  Your continued innovations are giving the linux community less and less to criticize about KDE, and giving the other computing environments less to criticize about linux in general.\n\nKUDOS!!\n\nPaul....."
    author: "Paul Hawthorne"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "I'd really like to find more information about how KDE prints. I also have cups installed on one machine but KDE doesn't use it. Konqy only lets me print ot a file, however a cups test print works perfectly.\n\nAlso what's the story with all the printer drivers in kdesupport? I have a feeling that KDE does have some sort of print system built in but I have no idea how it works!"
    author: "David"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "It's a class called kprint."
    author: "KDEer"
  - subject: "Whitepaper?"
    date: 2000-12-21
    body: "The whitepaper (at http://trolls.troll.no/~lars/xparts/doc/xparts.html) has been slashdotted. \n\nDoes anyone have a mirror?"
    author: "KuriousGeorge"
  - subject: "Re: Whitepaper?"
    date: 2000-12-21
    body: "It's available, just really slow.  If you're patient, you'll get it."
    author: "ac"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "Does this mean I can use KHTML in Nautilus?"
    author: "Anonymous"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "No. It means that you can embed componenets from nautilus in konq e.g., but not vice versa."
    author: "J\u00f6rgen Lundberg"
  - subject: "Huh?"
    date: 2000-12-22
    body: "But Bonobo is toolkit-independent too."
    author: "Anonymous"
  - subject: "Re: Huh?"
    date: 2000-12-23
    body: "But bonobo isn't as advanced as KParts. Go ask the Gnome developers to try to catch up to KDE development :)\n\n--\nChris"
    author: "kupolu"
  - subject: "Re: Huh?"
    date: 2000-12-23
    body: "How do you know that's true?\nKParts is intended to be *simple* (read developer.kde.org)\nBonobo is based on CORBA, which is very powerful and complex.\nSo Bonobo can embed KDE apps as well."
    author: "Anonymous"
  - subject: "Re: Huh?"
    date: 2000-12-24
    body: "Slowly."
    author: "kupolu"
  - subject: "KParts + Fixes = XParts (aiming to be Bonobo)"
    date: 2000-12-21
    body: "As far as I know, KParts never gave much attention to other toolkits or architectures.\nNow this has been recognizes as a major goal.\n\nCONGRATULATIONS!!!\n\nI just don't like the way it's been presented. I mean, as if it was the pioneer technology for doing that... Or treating other toolkits with some inferiority! \n\n<I>\"...Mozilla's (which currently utilizes GTK)...</I> It sounds as if it's a bad thing... BUT IS T??? \n\nMaybe the text should say clarify people that KParts (or XParts) NOW can do the same thing other component models such as Bonobo already do."
    author: "Cleber Rodrigues"
  - subject: "Re: KParts + Fixes = XParts (aiming to be Bonobo)"
    date: 2000-12-21
    body: "A lot of bark, but now bite?\n\nWhere are the cool apps actually using Bonobo?"
    author: "Peter Putzer"
  - subject: "Re: KParts + Fixes = XParts (aiming to be Bonobo)"
    date: 2000-12-22
    body: "Let me see: Evolution, Nautilus, Gnumeric, Gnome-DB, and almost every Gnome Application that would make sense providing a component for re-use.\n<br>\nAnd actually, what does it have to do with 'cool apps' ???"
    author: "Cleber Rodrigues"
  - subject: "Re: KParts + Fixes = XParts (aiming to be Bonobo)"
    date: 2000-12-22
    body: "Soon you'll be able to add AbiWord to that list..."
    author: "Dom"
  - subject: "Re: KParts + Fixes = XParts (aiming to be Bonobo)"
    date: 2000-12-22
    body: "None of these applications is stable yet. For Gnumeric there is even a text that mentions that one shouldn't file bugreports if one compiles with Bonobo (because it is still way too broken)."
    author: "anon"
  - subject: "Re: KParts + Fixes = XParts (aiming to be Bonobo)"
    date: 2000-12-22
    body: "I'd argue against your statement. I have a fairly large role in the AbiWord project and am an ex Gnumeric hacker (amongst other things). From what I've seen, KSpread can't hold a candle to Gnumeric (though I could be wrong here). KPresenter is pretty darn cool. KWord is a damn fine app that I want to study more. A lot could be gained from Abi and KWord people working together.\n\nAFAIK, Jody (the Gnumeric maintainer) just doesn't want to have to deal with bugreports not related to gnumeric, but rather Bonobo. This is just \"boiler-plate\" text that he attaches to each release announcement. If you'd try a Gnumeric release that was bonobo-enabled, you'd see exactly what I mean.\n\nJust because an Application doesn't have a \"1\" as its major build number doesn't mean that it's not stable. It's just under heavy development. When Gnome 1.4 comes out shortly, you'll see what I mean.\n\nKeep up the good work on KDE, guys."
    author: "Dom"
  - subject: "WRONG!"
    date: 2000-12-22
    body: "WRONG ANSWER DUDE!\nGnumeric is *far* superiour to KSpread!"
    author: "TrollKiller"
  - subject: "Re: WRONG!"
    date: 2000-12-22
    body: "Dude, that's what \"can't hold a candle to\" means. Did you even bother to read his post?"
    author: "Anon"
  - subject: "Re: WRONG!"
    date: 2000-12-22
    body: "I was replying to your topic, not Dom's."
    author: "TrollKiller"
  - subject: "Re: KParts + Fixes = XParts (aiming to be Bonobo)"
    date: 2001-01-05
    body: "Hmm, strange. So bonobo can talk to kparts ? Or use any other truely object-oriented toolkit (i mean C++ based) ? How comes ?"
    author: "Thomas"
  - subject: "We are KDE Of BORG"
    date: 2000-12-21
    body: "That\u00b4s great!\n\nThe following scene comes to my mind:\n\n[On the bridge of the starship Nautilus]\n\nCrewman: Look, Captain, we\u00b4re being pursued!\nCaptain: Full Speed Ahead\n\n\"WE ARE KDE OF BORG! PREPARE TO BE ASSIMILATED\"\n\nCaptain: Evasive Maneuvers, Fire Mozilla Grenades!\nCrewman: Sir, the grenades simply attach to the\nship, no damage done.\n\nCaptain: Oh, we better give up then...\n\n:-)"
    author: "Matthias Lange"
  - subject: "Re: We are KDE Of BORG"
    date: 2000-12-22
    body: "hehehe"
    author: "reihal"
  - subject: "Re: We are KDE Of BORG"
    date: 2000-12-22
    body: "Assimilation is a *good* thing when everyone plays nicely and shares."
    author: "egkamp"
  - subject: "Re: We are KDE Of BORG"
    date: 2000-12-22
    body: "if i would get the same feelings when i look onto the kde-desktop as when i look at 7of9, it would be great! ;)\n\ncya. markus"
    author: "Markus Kohls"
  - subject: "We are Gnome of Q"
    date: 2000-12-22
    body: "TrollKiller\nTo protect the Internet from ignorant trolls.\n------------------\n\n\"We are KDE of Borg. You will be assimilated. Resistence if futile.\"\n\nGnome of Q: \"Oh no, not those suckers again...\"\n\n\"Lower your weapons and...\"\n(Gnome of Q snaps in his fingers)\n\n***KABOOM!***\n\nGnome of Q: \"Yet another KDE of Borg ship gone. Heh heh.\"\n\n>:->\n\n------------\nTrollKiller\nTo protect the Internet from ignorant trolls."
    author: "TrollKiller"
  - subject: "Re: We are Gnome of Q"
    date: 2000-12-22
    body: "What a lame reply"
    author: "TrollKillerKiller"
  - subject: "Re: We are Gnome of Q"
    date: 2000-12-22
    body: "What a lame comment."
    author: "TrollKillerKillerKiller"
  - subject: "Re: We are Gnome of Q"
    date: 2000-12-28
    body: "Geee....\nIf you just want to be a troll and making such wasted comment you have accomplished it. Finally, you've found dot.kde.org."
    author: "jam_kim"
  - subject: "Re: We are Gnome of Q"
    date: 2000-12-22
    body: ">> \"Lower your weapons and...\"\n>> (Gnome of Q snaps in his fingers)\n\n>> ***KABOOM!***\n\n>> Gnome of Q: \"Yet another KDE of Borg ship >>gone. Heh heh.\"\n\nGnome of Q: Caption.... Caption.... we....\n\nG Killer: hehehehe... they can't even recognize there own ships...\n\n:)"
    author: "G Killer"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-21
    body: "It's impressive that not a single line of code was modified in Konqueror to do this.  Something can certainly be said for the architecture of KDE2.\n\nHowever,  I fail to see how this is in anyway revolutionary.  It's been done dozens of times before in many different toolkits.\n\nThe article, and comments above, and most ntoably the Slashdot article, suggest that XParts/KParts are now able to embed next to everything - Bonobo Components, OpenOffice, etc.  This is simply not the case.  Embedding the GtkMozEmbed component is a very special case.  This component was designed as a GTK widget.  QT can already use GTK Widgets.  So the effect shown is different from Galeon, Nautilus, etc only in so far as it required no modification to the Konqueror code.\n\nThis is all illustrated in the whitepaper linked to above.\n\nI do not mean to troll, I simply want to set many of the posters here straight."
    author: "Jebediah Smith"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "<p><i>I do not mean to troll, I simply want to set many of the posters here straight.</i>\n</p>\n<p>I supposed it would be more impressive if you could figure out how to post once instead of four times.</p>"
    author: "Eric Laffoon"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "I don't think he did, it's a bug, it have happened  many times before."
    author: "reihal"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "I don't think he did, it's a bug, it have happened  many times before."
    author: "reihal"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "KMozilla does not require Qt's ability to utilize GTK widgets. Instead, it reparents an X-Window with XEMBED. This works on a wide range of toolkits. \n\nBut your are right. There's indeed nothing revolutionary with the approach, and I don't believe the web page or the whitepaper claim this. It is basically a demonstration on what we TOLD people for a long time now: KParts is not a dead end. It's powerful enough to support other component models. The fact that we use fast inprocess components with a KDE API doesn't mean, we cannot utilize other models. The fact that we dropped CORBA doesn't mean users suffer from less interoperability.\n\nBut not everybody is a developer and not everybody understands the technical issues involved. So people simply didn't believe us. \"KDE does no longer use CORBA\" was percieved as \"GNOME components will never be usable in KDE\". The only \"revolutionary\" thing is, that we demonstrated what technical people knew for a long time.\n\nNow, KMozilla is special, because it (or rather GtkMozEmbed) isn's a GNOME or Bonobo component. But the approach we've chosen is indepentent from that. We are confident that we will be able to provide a generic BonoboXPartHost that is able to serve arbitrary Bonobo components as KParts. This is just a bride, it doesn't require changing either Bonobo or KParts.\n\nA similar thing was done with Java and Konqueror previously, or the Netscape Plugin support."
    author: "Matthias Ettrich"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "I didn't mean to suggest that the webpage and/or whitepaper suggested that it was revolutionary.  I was trying to set some of the posters here - and on slashdot - straight.\n\nIf this same technology could be extended to use Bonobo, I have either read the whitepaper wrong, or you are much smarter than I:)\n\nProbably the latter.\n\nWhile this certainly shows off KParts' well-designed architecture, I'm unsure if this would be practical.\n\nWhen loading a Bonobo component, you would have all the overhead of Bonobo (oafd, ORBit, etc) along with KParts' overhead.  How would this effect performance?\n\nA better idea, IMNSHO, would be a concerted effort to bring a standard COM to *N*x/BSD.\n\nPlease, don't anyone reply with \"We have a standard COM....CORBA.\""
    author: "Jebediah Smith"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-23
    body: "<p>On Friday December 22, 2000, Jebediah Smith wrote:\n<p>> While this certainly shows off KParts' well-designed architecture,<br>\n> I'm unsure if this would be practical.\n<p>> When loading a Bonobo component, you would have all the<br>\n> overhead of Bonobo (oafd, ORBit, etc) along with KParts' overhead.<br>\n> How would this effect performance?\n<p>\nJebediah, please don't get me wrong, but this does not sound very dramatically to me.<br>\nThe KParts component model was designed to be effective and easy to implement while at the same time working really FAST.\n<p>There is not very much in looking for 'overhead of KParts' - trust me: the time consumed by KParts can be ignored when you have to deal with something that time-consuming as CORBA.\n<p>I do *not* intend to start a flame-war against CORBA but I thought it might be good to mention that this possibility of embedding external parts into KDE is a great thing: Just be happy and <b>forget</b> about the *tiny* amount of overhead that might be caused by using the KParts model.\n<p><i>Karl-Heinz</i>\n<p><A href=\"http://home.snafu.de/khz/\">http://home.snafu.de/khz/</A>"
    author: "Karl-Heinz Zimmer"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-23
    body: "You're certainly right.  The point of the post was that embedding Bonobo comonents as a KPart would require that oafd, ORBit, et al., be loaded.  The added overhead isn't coming from KParts - It's coming from Bonobo."
    author: "Jebediah Smith"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-22
    body: "<p>Does a right click on the KMozilla component in Konqueror bring up a menu? The last time I saw it, neither Galeon nor Nautilus could do it. If a standard menu does not come on a right click (with save as.. copy link etc), it is better to stick to KHTML</p>"
    author: "rsv"
  - subject: "IDEA! (embeding Gimp into Krayon)"
    date: 2000-12-22
    body: "Well, what about using this technology to embed Gimp into Krayon (the former KImageShop)!!. That way you'll have all the power that Gimp already has without the crappy UI!"
    author: "t0m_dR"
  - subject: "Re: IDEA! (embeding Gimp into Krayon)"
    date: 2000-12-22
    body: "Make a Kparts of the GIMP libraries."
    author: "reihal"
  - subject: "Re: IDEA! (embeding Gimp into Krayon)"
    date: 2000-12-22
    body: "A kde developer told me a kimp was made, a port of gimp to qt, but that gimp devs got mad so it was never realeased... hmmmmmm...\n<p>\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: IDEA! (embeding Gimp into Krayon)"
    date: 2000-12-22
    body: "ummm,,,,isn't it GPL? So who cares if they were Mad!. Anyway I think that was in the pre GPL era of the QT toolkit. I Think they were Mad 'bout the licence..."
    author: "t0m_dR"
  - subject: "Re: IDEA! (embeding Gimp into Krayon)"
    date: 2000-12-23
    body: "I think this comes down to a lie.\n\nI just don't believe such a port exists."
    author: "Cleber Rodrigues"
  - subject: "Re: IDEA! (embeding Gimp into Krayon)"
    date: 2000-12-23
    body: "Such a port did indeed exist. It was shown to some developers (I think even Miguel Icaza saw it) on Linux-Kongress and on Linuxtag in 1998."
    author: "anon"
  - subject: "Re: IDEA! (embeding Gimp into Krayon)"
    date: 2000-12-24
    body: "sure it did, if you search for \"Kimp, Linux , Kde\" keywors you'll find some (OLD) material about it..."
    author: "t0m_dR"
  - subject: "Re: Embedding external parts into KDE"
    date: 2000-12-28
    body: "Something I keep thinking of is this: KParts seems to me (a Linux programmer and a VB programmer) to be much more like local-only ActiveX EXEs and ActiveX Controls. (To my knowledge KParts doesn't do networking). Bonobo/Corba, OTOH, is made for networking, and using it locally would have a performance hit vs. KParts (probably not much of one, but it's probably still there). Why not have both? I don't want my real-Cool-Grid widget running over a network, but putting business logic into a component on a shared server somewhere makes lots of sense to me..."
    author: "ChrisWiegand"
  - subject: "Embedding IE running in Wine in Konqi!!!"
    date: 2003-05-28
    body: "IE is like the fastest browser ever, and then embed in xparts in konquerer... totally fast... oh my god... Then embed VM ware in konqi in xparts and run mozilla in the konqi in the vmware in the konqi, totally the slowest thing ever lol. "
    author: "TROLL PATROL"
---
Up to now, the KPart component model was limited to embedding in-process parts. <a href="http://trolls.troll.no/~lars/xparts/">XParts</a> is an extension written by Matthias Ettrich, Simon Hausmann and Lars Knoll to extend kparts and make it possible to embed out-of-process components. The <a href="http://trolls.troll.no/~lars/xparts/doc/xparts.html">approach chosen</a> is toolkit independent, as can be seen by their choice to embed Mozilla. Read on for the full announcement and details.














<!--break-->
    <h2>KDE takes a massive leap by providing interoperability with major Unix/Linux software toolkits and applications --
including Mozilla.</h2>
<p>
Linux and Unix developers, can now easily make KDE components --
no matter what toolkits and software they decide to develop with. 
Recent Unix/Linux desktop applications are composed of small
components that could be utilized in several applications or wherever needed. KDE is now the first desktop system to be able to effectively integrate and provide transparent interoperability
with such software -- whether written in the KDE native component model (KParts), plain X Windows, or GTK. 
<p>
As a first step, users can now choose to use either KDE's native
sophisticated HTML widget or Mozilla's (which currently utilizes GTK) inside the KDE browser, Konqueror. This easily puts Konqueror in the lead when it
comes to browsing the World Wide Web, since the user now gets to select which HTML renderer works best for him/her while still using Konqueror and
having all the same advantages of the popular KDE user interface and technology. 
<p>
This is only a first step. Other possibilities include providing transparent access to OpenOffice components within KOffice, and embedding other Bonobo components, such as the various Nautilus components, inside, say, Konqueror... The goal is to provide the most powerful desktop for users by allowing them to pick and choose whatever software
they like while still in the familiar and comfortable KDE environment. KDE is close to closing the schism within the Linux desktop environments by
being the first project to allow users to utilize all the software written for different user interfaces within the KDE environment with unparalleled
integration. 
<p>
Also, people writing standalone applications that do not utilize any desktop technology can easily integrate with our environment in ways previously
impossible. 
<p>
The KDE project would like to thank Lars Knoll, Simon Hausmann, and Matthias Ettrich for their coding genius in making this happen. 
<p>
Links:
<p>
<a href="http://trolls.troll.no/~lars/xparts/">Overview including screenshots and installation instructions</a>
<p>
<a href="http://trolls.troll.no/~lars/xparts/doc/xparts.html">Whitepaper</a>













