---
title: "KDE wins Linux Community Award 2000"
date:    2000-10-06
authors:
  - "wes"
slug:    kde-wins-linux-community-award-2000
comments:
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-10-05
    body: "it's great!!!!\nThe people, the world, know what is the best."
    author: "Manuel Rom\u00e1n"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-12-20
    body: "yeah.. REAL the BEST Window Manager :))))))\n\nEveryone knows this"
    author: "HITMAN"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-10-05
    body: "Congratulations! Well deserved!\n\n(I wonder what Linus is smiling at on the screen there...)"
    author: "reihal"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-10-06
    body: "Excuse me for sounding so ignorant, but which one is Linus? I've never seen a picture of him before, so I don't know what he looks like."
    author: "jliechty"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-10-06
    body: "The man in the middle (on the computer) is Ralf Nolden (KDevelop Team),left from him stands Linux Thorvalds and on the right side Martin Konold(KDE)."
    author: "Sandy Meier"
  - subject: "Re: Fat guy in grean shirt."
    date: 2000-10-06
    body: "Title says it all.  Linus is the fat guy in the grean shirt.  \n\nLooking at that picture you have to ask; \"Who says computers arn't bad for your eys ?\""
    author: "Forge"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-10-06
    body: "GOOD question and thanks for asking it coz I didn't dare to (beeing a newbie you're supposed to find it all in the FAQ's and the HOWTO's)"
    author: "Jeroen M"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-10-06
    body: "Well, it was this picture I was referring to.\n\n\n<A Href=http://master.kde.org/~me/lwe/-016.jpg Target=_new>Click here</ A>"
    author: "reihal"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-10-05
    body: "Congratulations to KDE team. They deserve it."
    author: "Zeljko Vukman"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-10-05
    body: "And you didn't even release KDE2.\n\nMeine herzlichsten Gl\u00fcckw\u00fcnsche!"
    author: "Moritz Moeller-Herrmann"
  - subject: "Congratuation!1"
    date: 2000-10-06
    body: "Good news for KDE developer and user alike.\nThey really deserved it.\nBut, I just wonder whether the award presented because this Linux Community exhibition was held in Germany rather than in US.\nWell, we all now that many linux users in US is Gnome fanatics.\n\n\nJamal"
    author: "Jamal"
  - subject: "Re: Congratuation!"
    date: 2000-10-06
    body: "<br>> But, I just wonder whether the award presented because this Linux\n<br>> Community exhibition was held in Germany rather than in US.\n<br>> Well, we all now that many linux users in US is Gnome fanatics.\n\n<p>I don't think so, just read: <a href=\"http://www.kde.org/announcements/lwe_08_2000.BW.press-release.html\">\nKDE Desktop Is Show Favorite at LinuxWorld Expo</a>. This one was held in San Jose, California. Personally I don't think that there is much difference between US and Germany but I can't offer any numbers.</p>\n\n<p>KDE you did a GREAT job and you are the reason that I have switched from Windows to LINUX (I even gave up the IMHO best compiler in the world: Visual Age C++) about a year ago and I am now using kdevelop (which is great too). Just want to say THANK YOU for this excellent desktop! Great job!!!</p>"
    author: "Marco Krohn"
  - subject: "Re: Congratuation!"
    date: 2000-10-09
    body: "\"we all now that many linux users in US is Gnome fanatics\"\n\nwell, I'm American and I use KDE.  I know many Americans who use KDE.  I think you are refering to the Gnome foundation and the decision by major vendors to choose Gnome as the standard unix desktop.  I don't agree with their decision.\n\nI will always use KDE as long is it relatively bug free and continues to improve.  I congratulate the KDE team on a job well done.  There are many of us who are anxiously awaiting the official release of KDE 2.0.  Go KDE!"
    author: "erotus"
  - subject: "Re: Congratuation!"
    date: 2000-11-07
    body: "The beauty of it is we don't have to agree with their decision because we have a choice.  Kudos to the KDE developpment team for their hard work! You guys deserve the recognition"
    author: "zeek"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-10-08
    body: "Congratulations from asia user :)\nThanks for your excellent work,kders!"
    author: "Kafa"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-12-09
    body: "Congrats !!! to the KDE team keep up the good work, you deserved it right."
    author: "Abdul Rahim"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-12-10
    body: "You guys have done an excellent job!!! I love KDE2!! Excellent job guys!"
    author: "Bill"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-12-19
    body: "Congratulations! KDE is the BEST !!!!!!!!!!!!"
    author: "Panos Polychronis"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-12-19
    body: "Congratulations! KDE is the BEST !!!!!!!!!!!!"
    author: "Panos Polychronis"
  - subject: "Wowwww"
    date: 2000-12-19
    body: "I start using Kde 1 week ago and i soon like using it!!\n\nI hope i can be like Kders to make Kde better than ever.\n\nCiao"
    author: "Matteo"
  - subject: "Wowwww"
    date: 2000-12-19
    body: "I start using Kde 1 week ago and i soon like using it!!\n\nI hope i can be like Kders to make Kde better than ever.\n\nCiao"
    author: "Matteo"
  - subject: "Re: KDE wins Linux Community Award 2000"
    date: 2000-12-27
    body: "I am reply from work; but as soon as I reach home I'm gonna install linux on my Pentium II PC.  Congrats to KDE team on winning the Linux Community Award 2000.  Wishing you guys success year 2001! \\:-))\n What's the best open source C++ compiler available for Corel Linux Lite? Merci! Gracias! Thanks!\n\nPlease note I am a novice to Linux."
    author: "Allister"
---
Matthias Elter <a href='http://lists.kde.org/?l=kde-devel&m=97075580324120&w=2'>announced</a> today that KDE has won the Linux Community Award 2000 at LWE in Frankfurt/Germany!  Congratulations to all the KDE developers on a job well done and an <a href="http://master.kde.org/~me/lwe/-008.jpg">award</a> well deserved.  Pictures are available <a href='http://master.kde.org/~me/lwe'>here</a>, including <a href='http://master.kde.org/~me/lwe/-018.jpg'>a nice shot</a> of the KDE team with none other than Linus himself.


<!--break-->
