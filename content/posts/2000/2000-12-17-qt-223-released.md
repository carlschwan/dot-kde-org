---
title: "Qt 2.2.3 Released"
date:    2000-12-17
authors:
  - "melter"
slug:    qt-223-released
comments:
  - subject: "Keith's AA stuff in there?"
    date: 2000-12-17
    body: "Is Keith P's Anti-Aliasing stuff in there? The announcement doesn't say anything about it. \n\nAnyone know when we can expect it to be in there?"
    author: "Joe Dorita"
  - subject: "Re: Keith's AA stuff in there?"
    date: 2000-12-17
    body: "AFAIK, the AA text code is in the latest version of Xfree86 (CVS). The only modification that was made to QT to enable this, is to call the AA text display functions in X, rather than the normal ones. The Anti-Aliasing code is not in QT itself.\n\nOf course all of this is just my own educated guess."
    author: "Matt"
  - subject: "Re: Keith's AA stuff in there?"
    date: 2000-12-17
    body: "It's in qt-copy though."
    author: "ac"
  - subject: "Re: Keith's AA stuff in there?"
    date: 2000-12-17
    body: "I don't know when it might be in the official\nQT, but you can get Keith's patch here:\nhttp://keithp.com/~keithp/download/qt-2.2.3.diff.gz"
    author: "SteveH"
  - subject: "Re: Keith's AA stuff in there?"
    date: 2000-12-17
    body: "Hey thanks. Grabbed it, complied it. It messes up a few things, but it's pretty neat :)"
    author: "Joe Dorita"
  - subject: "Re: Keith's AA stuff in there?"
    date: 2000-12-18
    body: "Keith's patch is now in qt-copy on CVS. I upgraded mine over the weekend using CVSup, along with XFree86 4.0 (also using CVSup) and anti-aliased fonts work perfectly. There were a few problems with 4.0.2 and QT/KDE before but they seem to have been fixed now (e.g. konsole). Well done Keith and Trolltech!"
    author: "jpmfan"
  - subject: "Not quite perfect"
    date: 2000-12-19
    body: "It works well, but it crashes some apps (try chosing a font in Licq.) It also isn't Xinerama-ified yet, so only one of your heads will display text AT ALL :)\n\nI've exchanged e-mail with Keith and he said that he's planning on Xinerama-ifying it soon."
    author: "Joe Dorita"
  - subject: "Re: Keith's AA stuff in there?"
    date: 2000-12-19
    body: "Hey, these Patches are pretty neat. But, I compiled my own version of qt2.2.3, and how can I get this patch to work ... I downloaded it, but executing, neither in the QTDIR nor in any directory else, leads to any output I could use. What to do?"
    author: "Thorsten"
  - subject: "Re: Keith's AA stuff in there?"
    date: 2000-12-24
    body: "The pach is qziped so first gzip -d it\n\nthen copy it to you QTDIR (the one where you built qt)\n\nnow type:\npatch -p0 < qt-2.2.3.diff\nyou should get this output\npatching file configs/linux-g++-shared\npatching file configs/linux-g++-shared-debug\npatching file configs/linux-g++-static\npatching file configs/linux-g++-static-debug\npatching file src/kernel/qapplication_x11.cpp\npatching file src/kernel/qfont_x11.cpp\npatching file src/kernel/qfontdatabase.cpp\npatching file src/kernel/qpainter_x11.cpp\npatching file src/kernel/qpixmap_x11.cpp\npatching file src/kernel/qt_x11.h\npatching file src/kernel/qwidget_x11.cpp\n\nstep 3 recompile\n\nhappy hacking"
    author: "red5"
  - subject: "Re: Keith's AA stuff in there?"
    date: 2000-12-24
    body: "Thank you very much ... I will try it due the free days ... Happy X-mas, Thorsten"
    author: "Thorsten"
  - subject: "Re: Qt 2.2.3 Released"
    date: 2000-12-18
    body: "will XFree86 4.0.2 include the AA text or do we have to wait?"
    author: "Charles"
  - subject: "Re: Qt 2.2.3 Released"
    date: 2000-12-18
    body: "It will be included...\n<br><br>... if your graphic card supports the extension that is...<br>Some cardholders like the ATI ones will probably be a bit disapointed :("
    author: "Sam"
  - subject: "Re: Qt 2.2.3 Released"
    date: 2000-12-18
    body: "I'm sick of always buying the cards that don't get supported. Which cards will this be in? Which cards are best for Xfree86-4?"
    author: "David Johnson"
  - subject: "Re: Qt 2.2.3 Released"
    date: 2000-12-19
    body: "Go to the XFree86 website, I believe there's a list of which are compatible and which are not.\n\nsite is http://xfree86.org"
    author: "Locohijo"
  - subject: "Re: Qt 2.2.3 Released"
    date: 2000-12-20
    body: "It is just minor work getting it supported for software fallback (not super fast rendering then). So when 4.0.3 is out, Qt, Gtk, and probably a lot of other things have been prepared for the renderer<br>In short, you won't miss to much.<br><br>/Sam"
    author: "Sam"
  - subject: "Re: Qt 2.2.3 Released"
    date: 2000-12-19
    body: "No problems with the ATI Rage128 Mobility ;)"
    author: "MK"
  - subject: "Re: Qt 2.2.3 Released"
    date: 2000-12-19
    body: "Why can't AA be supported whether or not the card has support for it?  It can be done in software (I modified the X-Windows driver for Microwindows to do alpha -- AA just requires alpha and the math is simple."
    author: "Erik Hill"
  - subject: "Re: Qt 2.2.3 Released"
    date: 2000-12-20
    body: "It will for most cards. The small thing has just to be enabled for software fallback. Will _most_ likely be in 4.0.3"
    author: "Sam"
  - subject: "AA"
    date: 2000-12-18
    body: "What is AA anyway? Could someone give \npointers to good tutorials?"
    author: "Netpig"
  - subject: "Re: AA = Anti-aliasing"
    date: 2000-12-18
    body: "Have a look to \nhttp://www.widearea.co.uk/designer/anti.html"
    author: "thil"
  - subject: "Broken OpenGL on nvidia driver?"
    date: 2000-12-19
    body: "We couldn't get OpenGL to work on the nvidia driver because of Mesa dependencies in the source code. Has this been fixed?"
    author: "S. Loisel"
---
Our friends at <A HREF="http://www.trolltech.com/">TrollTech</A> have announced Qt 2.2.3. Qt 2.2.3 is a bugfix release. It keeps both forward and backward compatibility (source and binary) with Qt 2.2.2. For details, read the <A HREF="http://www.trolltech.com/company/announce/qt-223.html">announcement</A> and see the change <A HREF="http://www.trolltech.com/developer/changes/2.2.3.html">summary</A>.

<!--break-->
