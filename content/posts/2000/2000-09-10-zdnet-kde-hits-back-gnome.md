---
title: "ZDNet: KDE hits back at GNOME"
date:    2000-09-10
authors:
  - "numanee"
slug:    zdnet-kde-hits-back-gnome
---
Readers have been writing in that ZDNet is currently <a href="http://www.zdnet.com/zdnn/stories/news/0,4586,2626017,00.html">reporting</a> on a rumoured KDE League that will go head-to-head with the GNOME Foundation.  LinuxToday has picked up the <a href="http://linuxtoday/news_story.php3?ltsn=2000-09-08-019-04-PS-GN-KE">story</a>, including a link to a <a href="http://lists.kde.org/?l=kde-devel&m=96844930413378&w=2">statement</a> by Kurt Granroth.  Well, it's nice to see that the press is so interested in KDE that they'll even report rumours or "the skinny" if they think they have it. ;-)







<!--break-->
