---
title: "Qt, Kivio, Kafka, and more..."
date:    2000-11-14
authors:
  - "numanee"
slug:    qt-kivio-kafka-and-more
comments:
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-13
    body: "hummm....\nam I the only one who really love X.\nrunning programs from diffrent computers displaying it on one, for example Running koffice from my old\ngood Sparc Station lx with my beloved keyboard it's a joy, you can even play asteroids on it :)"
    author: "AC"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-13
    body: "I think most Linuxers only have one computer, a PC. X isn't really useful for us.\nA faster system with a smaller footprint would be useful, though."
    author: "reihal"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-14
    body: "I dont' think so. I use ssh and x to run apps from home at college and vice versa. That, and across the network in teh apartment at home, it's invaluable."
    author: "Rikard Anglerud"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-14
    body: "Even for this, X is poorly designed and architected. It's decade-old kludge. No debate. Go support Berlin."
    author: "Adrian Kubala"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-14
    body: "Then go and support.\nBerlin is not free after all."
    author: "fura"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-14
    body: "it is not?\nquote from berlin-consortium.org:\n It is developed entirely by volunteers on the internet, using free software, and released under the GNU Library General Public License.\n\nthat's free enough for me"
    author: "P. Mauritz"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2002-05-09
    body: "I dare say you are a retard!\nThe xwindow system has thousands of benifits over\nmicrosofts windows toolkit. The xwindow toolkit has\nbeen redefined over 100 times in the past 20 years.\nand is actualy doing more work with less resources \nthan ever before.. Winblows still adds eyecandy yearly\nat the cost of resources.. Fact is nothing can beat linux\nand the xwindow toolkit for depth, functionality, ease of use,\nand overhead..  Go back to school and learn somthing this time!\nWildman"
    author: "wildman"
  - subject: "Re: Qt/Embedded"
    date: 2000-11-13
    body: "well,i have a use for QT without X,my main box (in other words,the box i'm stuck with it) has been crawling under the load of X,i must admit that my Xserver isn't anywhere near optimized (stock debian xserver-svga which have driver for every graphic card under the sun) but i don't have the disk space to recompile X so each time i use X,i must wait about 2 or 3 minute when it start up and i use an average of 10 to 15 MB of swap (my main memory is totaly maxed out),i'm currently buying a new box but i'll have it in about 5 month.\n\n486/40\n16 MB of ram\n270 MB hard drive\n\nat least,i have Cable modem and a new Asus 50x CD drive.\n\nAlain"
    author: "Alain Toussaint"
  - subject: "Re: Qt/Embedded"
    date: 2000-11-19
    body: "The place I would like to see Qt embedded used is in the graphical installers of some linux distros(such as Mandrake or esp. Redhat). Currently the installation programs load X to display graphics. I doubt that the network transparent features of X are ever useful in a temporary installation program. Instead they could use Qt/Embedded to display better graphics and get the installation done, without the slow interface that X provides (Redhat's graphical install really is horrible. I prefer the Ncurses interface rather than curse the graphical install). First impressions do count."
    author: "rsv"
  - subject: "Qt/Embedded"
    date: 2000-11-13
    body: "This looks really cool, and I was just about ready to install this on my Cassiopeia E-100 when I took a look at the README. Apparently, I could install it and it would work, but due to the large size of the kernel (twice as big as the kernel for x86), a great deal of thrashing would occur. Qt blames gcc, saying that the compiler produces such a large kernel binary. I'm wondering if:\n<UL>\n<LI>anyone is working on this or</LI>\n<LI>it would be possible for someone to compile the kernel with a more efficient compiler and release the binary of the (hopefully smaller) kernel</LI>\n</UL>\n\nI'd really like to try this out so any information is greatly appreciated. Thanks."
    author: "bob"
  - subject: "Re: Qt/Embedded"
    date: 2000-11-14
    body: "Not just the kernel.  Anything (including Qt/Embedded and QPE) compiled with the mips compiler tools from linux-vr.org is about twice the size of x86 and arm.\n<p>\nThere's a new package for the Cassiopeia now that uses a smaller RAM disk and is useable on the E-100.\n<p>\nGet it from the link at the bottom of <a href=http://www.trolltech.com/products/qt/embedded/qpe.html>http://www.trolltech.com/products/qt/embedded/qpe.html</a>"
    author: "Martin Jones"
  - subject: "Re: Qt/Embedded"
    date: 2000-11-14
    body: "Thanks, I just ordered a flash card so I could try it out."
    author: "bob"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-14
    body: "Well, KWin and Kicker would be kool on a palmtop or any embedded Unix system... but... will it be legal to do so? I'm not sure. You see, KWin, Kicker and several other KDE applications are under the BSD license, but Qt/Embedded does not have the *dual* GPL/QPL license like Qt/X11 has. I could be wrong here, but I don't think it's allowed to link to a GPL library from anything other than a GPL application."
    author: "David Johnson"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-14
    body: "In this particuliar case, noone can deny the \"system library\" status of Qt embedded. It is allowed to link a GPL program to any library, whatever its license, as long as it's a system library -- in extenso something you have already installed on your system and you really need to have your system working."
    author: "GeZ"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-14
    body: "KWin and some other KDE tools are under X-Style licenses. According to the FSF, those are compatible with the GPL. Hey, otherwise Debian wouldn't include a sincle X11 application ;)"
    author: "Matthias Ettrich"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-14
    body: "Why the X license?\nAre Kicker and KWin written by ignorant Slashdotters who never read the GPL and really think GPL stands for communism?"
    author: "Anonymous"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-14
    body: "And may I ask, what does choosing The Most Compatible Free Software License for your program have to do with not reading GPL or having preconceptions about it's political connections?"
    author: "Another one"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-15
    body: "You can also just say \"because the X license is the most compatble one\" you know.\nI'm only asking why they chose the X license."
    author: "Anonymous"
  - subject: "Re: Qt, Kivio, Kafka, and more..."
    date: 2000-11-15
    body: "<I>You can also just say \"because the X license is the most compatble one\" you know</I><br>\nWell actually I can't, because I don't know the exact reasons (not being one of the developers). I just got ticked of by the assertion that you by default do not understand (L)GPL or hold some sort of grudge against RMS, if you choose some other free license for your project."
    author: "Another one"
  - subject: "RedHat 7"
    date: 2000-11-14
    body: "I read that there were some issues regarding QT 2.2.1, and RedHat 7. Can anyone tell me if these issues are fixed in this release?\nTa."
    author: "MichaelM"
  - subject: "About Kylix's code"
    date: 2000-11-14
    body: "Anybody notice the news about Kylix source will be opened to GNOME foundation ( \n<a href=\"http://www.zdnet.com/eweek/stories/general/0,11011,2652581,00.html\">look here</a>)? <br>\nI don't know why they do this, considering Kylix is written with Qt. So if Borland will ever release the code for Kylix, shouldn't be to KDE team instead of GNOME foundation? After all KDE is written using QT.<br>\nMy Questions:\nIs Kylix will only support GNOME application development?\nIs this mean KDE losed another PR war?"
    author: "jamal"
  - subject: "Re: About Kylix's code"
    date: 2000-11-14
    body: "<p>As far as I can understand the problem:</p>\n<ol>\n<li>Borland has a pro-KDE image (works with TrollTech on VCLX, attempt of merging with \"Korel\")</li>\n<li>GNOME is a nice buzzword these days in the USA</li>\n<li>due to its \"pro-KDE\" image, Borland was having an \"anti-GNOME\" image</li>\n<li>Borland was willing to give support for both desktop</li>\n</ol>\n<p>So they decided to make an announcement where they will talk about GNOME.</p>\n<p>Kylix is a project made of several part: the component library (CLX, formerly VCL, I will call it VCLX because there's already a CLX library), the compiler, and the IDE. The former will be (perhaps) open-sourced, ie available to the GNOME foundation but also to the rest of the world, including KDE developers. Both two laters will stay proprietary.</p>"
    author: "GeZ"
  - subject: "Trouble running Kivio"
    date: 2000-11-14
    body: "I installed the Mandrake RPM for Kivio on my LM7.2 system, but when I try to run kiviopart I get this message:\n\nkoffice (lib kofficecore): ERROR: Couldn't find the native MimeType in kivio's desktop file. Check your installation !\n\nI tried changing the x-kivio.desktop file in many ways, but to no effect. What's going on?"
    author: "Haakon Nilsen"
  - subject: "Re: Trouble running Kivio"
    date: 2000-11-14
    body: "Run kbuildsycoca manualy from terminal. Does it help?"
    author: "Hasso Tepper"
  - subject: "Re: Trouble running Kivio"
    date: 2000-11-14
    body: "It does for me (SuSE rpm).\nOle"
    author: "Ole"
  - subject: "Re: Trouble running Kivio"
    date: 2000-11-14
    body: "No sorry, it doesn't help... Same error, but thanks anyway. Anyone else who succeeded to run Kivio on Mandrake 7.2?"
    author: "Haakon Nilsen"
  - subject: "Re: Trouble running Kivio"
    date: 2000-11-14
    body: "For me it works! really do not know how fine :-). On mdk 7.2 with kde 2.\n\nMauro"
    author: "Mauro"
  - subject: "Re: Trouble running Kivio"
    date: 2000-11-15
    body: "I have Kivio working very well in Mandrake 7.2.\nI don't have any problem whatsoever after downloading and installing Mandrake's Kivio RPM."
    author: "jamal"
  - subject: "Re: Trouble running Kivio"
    date: 2001-04-12
    body: "I had exactly the same problem on Mandrake 7.2. The installation wasn't out of the box though - I had built qt and kde 2.1 from the source. I tried installing the kivio rpm, and it segfaulted on startup, so I got the source. It built fine, but gave the error you described. I installed the rpm again and hey presto - it works! There must be something missing from the source distribution."
    author: "Christopher Kelly"
  - subject: "Will Smith"
    date: 2000-11-14
    body: "Which Will Smith was that ?\n\n*I*, the rapper / acctor or some other guy with the same name ?  You see I have no recolection of making sch a submision :)"
    author: "Will Smith"
  - subject: "why are you doing these stupid sunmaries..."
    date: 2000-11-15
    body: "and not running items on each of these articles like you used to do? These summaries are worthless. This site was a lot better when there were more descriptions of the articles. Let's face it -- the outside world does a better job of covering KDE than kde news does (most of the original articles here ar little more than dog food or regurgitated newsgroup postings), and I find it useful to see how kde is being covered by the rest of the web."
    author: "George Tirebiter"
---
<a href="mailto:kudling@kde.org">Lenny</a> wrote in to point us to this interesting 
<a href="http://zez.org/article/articleview/2/">article</a> on writing games with QCanvas. Nice, easy, and includes example source code.  All six pages can be loaded in the printer-friendly version <a href="http://zez.org/article/articleprint/2/">here</a>. In Qt/Embedded news, Trolltech has <a href="http://www.linuxdevices.com/news/NS6133695536.html">announced</a> version 2.2.2, and even more exciting a new <a href="http://www.trolltech.com/products/qt/embedded/qpe.html">Qt Palmtop Environment</a> available for download under the GPL.  It will be interesting to see if a  KDE-enhanced version is feasible -- imagine running KDE on a Linux PC without X. For more Qt/Embedded buzz, also see this <a href="http://www.linuxworld.com.au/news.php3?nid=322&tid=2">article</a> on Lineo and Trolltech.

In other news, Will Smith himself wrote in to point us to this positive <a href="http://www.linuxplanet.com/linuxplanet/reviews/2614/1/">review</a> of Kivio over at LinuxPlanet. 

<a href="mailto:renai@student.unsw.edu.au">Renai Lemay</a> wrote in to point us to this <a href="http://www.linuxtoday.com.au/r/article/jsp/sid/579910">article</a> discussing KDE, GNOME and the media.

Lastly, <a href="mailto:jono@kde.org">Jono Bacon</a> wrote in to point us to this <a href="http://www.jonobacon.co.uk/kafka/rfc/rfc1.htm">Request For Comments</a> on the proposed Kafka WYSIWYG HTML editor.  Developers send your Kafka comments <a href="mailto:kde-kafka@master.kde.org">here</a>.

<!--break-->
