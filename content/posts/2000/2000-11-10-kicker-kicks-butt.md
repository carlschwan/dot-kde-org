---
title: "Kicker Kicks Butt"
date:    2000-11-10
authors:
  - "Dre"
slug:    kicker-kicks-butt
comments:
  - subject: "Re: Kicker Kicks Butt"
    date: 2000-11-10
    body: "One of the greatest things holding me back\nfrom going to KDE was the docklets in WindowMaker.\nThis is what I wanted in KDE more than anything,\nbefore I even asked its being worked on.\nDoes this mean the WindowMaker applets (protocols)\nwill be adopted by KDE?<br>\nKDE just got bigger and better.\nThe whole NeXT docklet idea is something worth\nhaving in all window managers/desktops.<br>\nGreat news!\n<br>\nAn another note does anyone know the status\nof WindowMaker window manager with the KDE-2\ndesktop?"
    author: "Dale"
  - subject: "Re: Kicker Kicks Butt"
    date: 2000-11-12
    body: "Is it possible to embed GNOME panel\napplets, too? This would be awsome!!"
    author: "Thed"
  - subject: "More co-operation"
    date: 2000-11-13
    body: "It would be nice to see more co-operation between the kde and GNOME teams in other things too besides dock applets! Why not co-operate instead of competing?"
    author: "Tomas"
  - subject: "Re: More co-operation"
    date: 2000-11-14
    body: "I bet they were saying this during the Reformation too. \"C'mon, we're all Christians!\"\n<p>Don't underestimate the power of stubbornness."
    author: "Adrian Kubala"
---
<A HREF="mailto:m_elter@t-online.de">Matthias Elter</A> wrote to the KDE core devel mailing list about his progress in getting KDE to play nice with Window Maker and Afterstep "applets", and included some <A HREF="http://master.kde.org/~me/dockbar.jpg">eyecandy</A> to boot.  His message follows below.
<!--break-->
<blockquote>
I have hacked a kicker extension that detects and embeds dock apps. Dock apps like Window Maker or Afterstep "applets" set a standard XWMHints hint (IconWindowHint) which is recognized by the extension. Save and restore is not implemented yet and the layouting is not perfect. I will fix both soon.
<br><br>
Eye candy: <a href="http://master.kde.org/~me/dockbar.jpg">http://master.kde.org/~me/dockbar.jpg</a>
<br><br>
Have fun,<br>
Matthias
</blockquote>