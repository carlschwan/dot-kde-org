---
title: "Improving KDE Public Relations"
date:    2000-10-11
authors:
  - "Samawi"
slug:    improving-kde-public-relations
comments:
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "I don't necessarily agree that embracing\nGNOME's style is the way to go.  Personally\nif I want something that looks good I'll run\nE with KDE.  \n<P>\nHowever, you're right that KDE needs to draw\nupon its strengths.  Looks at what kinds of \namazing things KDE has now:\n<P><UL>\n   <LI>Network and filesystem transparency \n       in the form of kioslaves (RIO baby!)\n       integrated into a kick ass filemanager.\n   <LI>A full blown office suite that almost\n       no one has seen any working examples\n       of because the koffice team is even\n       more inept at PR. (C'mon, we get\n       deluged in PR crap about some program\n       called Evolution that is still pre-alpha.)\n   <LI>New theme engines, new themes, new \n       graphics. \n</UL>\n<P>\nMost other projects flood the linux news sites\nwith weekly updates, weekly screenshots, RFP's,\nand nonsensical PR bs.  I admire KDE for not\nannouncing every little crapplet as major\ninnovation, but there's a lot of amazing stuff\nin the new release that no one knows anything\nabout (yes folks, you can browse smb..)"
    author: "vinn"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "To show the KDE io_slaves, I suggest to prepare\na series of screenshot showing the same konqi accessing all possible filesystems (including rio). \n\nAnd for developers, just tell that one\nonly needs to code a simple io_slave to add support for new protocols (and add a link to the io_slave html library and to examples).\n\n\nKonqueror as a web browser should of course have a dedicated paragraph, with all the features it includes. Perhaps, a detailed comparison to Mozilla would be interesting, especially since the rendering is faster than gecko. IRC, some kind of java support is disabled for security reasons. Perhaps it should be mentionned too.\n\n\nKde is criticized for not using Corba, so we should defintely have lot of examples ready to demonstrate DCOP and kpart. Simon Haussman once showed in response to an article what one can do with DCOP. This is the kind of thing that developers love and that will make them turn to kde. So have examples of that and of more powerful interactions ready for the developers to try.\n\n\nMention that one can embed an editor in kde with kpart. For the moment, there is only kwrite, but vim should be there one day (I'm working on it), and qfte too. This means that a user will be able to always edit with its favorite editor.\n\nMention that khtml is also an embedable componnent, that could even be replaced by gecko as Corel suggested.\n\n\nOk, I realised these are too much developers things.\n\nUser things are ... (thinking) ...\nYes, unicode.\n- Show a whole desktop in russian, in japanese, in chinese, etc. \n- Do we have applications that are unique ? Something better than a mail client and a ftp client ? \n- The programs most used by 'simple' users should have a word: Irc, icq and aim, mail, organizer."
    author: "Philippe"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "This is so true. Konqueror has to be the best browser for linux to date, yet no one knows about it. It's ok to hype a product if theres so much to hype about."
    author: "Chris Aakre"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "Uhm vinn,\n\nI'm afraid that SMB browsing isn't as complete and functional as you would like it to be. It has quite some irritating bugs left, which are not very likely to be removed on a short notice, because the library (libsmb++) that is being used for the smb functionality isn't really updated anymore (the author has no internet access for quite a while now, and isn't on a smb network anymore).\n\nSo unless you tell me all smb slave problems have been fixed, I suggest not to go shouting around that smb browsing is fully functional..."
    author: "Jelmer Feenstra"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "<P>\nI didn't say it worked well did I?  ;)"
    author: "vinn"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "No you're right.\n\n*sigh*\n\n:)"
    author: "Jelmer Feenstra"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-13
    body: "I'm a professional writer and I've done a fair bit of computer journalism in my time. I'm also sympathetic to linux and KDE in particular. It's fun and konqueror is really very impressive. almost my favourite browser on any platform.\n\nBut the rest is nothing like ready for the consumer market, especially Koffice. The first thing missing is interoperability. Like it or not, MS Office is the world's standard. If your applications can't read and write its formats they are about as much use as 1994 Mosaic would be on today's web: much less use than Lynx. \n\nBeyond the lack of filters there are other problems. Kword, which is, for obvious reasons, what I have looked at most, lacks all sorts of elementary features for a professional word processor -- select/delete words, lines, or sentences, for one example. Macros, for another. The ability to transpose letters. A degree of configuration so that everything can be made to work in familiar ways when you change from something else.\n\nKspread doesn't seem to have a \"find\" function. This stuff is not even Beta. It's alpha software, as far as I can see. It is not going to persuade any serious user to change over. Don't boast about an office suite when it compares to MS office roughly as dos 3 compared to Unix.\n\nOne final thought. If you want to do something which would really make KDE stand out from the competition, get the help finished and consistent. The basic framework is all there and excellent but hardly any of it is complete and lots of the help files still start with a section on where to get hold of the software. If I hadn't got that far, I wouldn't be reading the help file, would I?"
    author: "Andrew Brown"
  - subject: "gnome look"
    date: 2000-10-11
    body: "How to emulate gnome look in 2 steps :-)\nMake a gnome icon theme.\nCode a style to look like Sawfish and use the QT\nGTK style for the widgets.\n\nThen scrap the icons, hack the style to use some\nnice looking widgets. (Sawfish's window \ndecoration looks good :-)"
    author: "Morty"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "The other desktop enviroment seems to me less easy to use, and that is the bigger issue for non windozes.  But for some reason KDE has no the \"media\" that the others have.  May be they are better at that may be not, but I belive KDE2 will be a bigger player in the near future than KDE1 has been.  Anyway PR are always a good investment unless it affect the real work."
    author: "Fernando Delgado"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "I think KDE has need a little more PR, but not that much, we are ( or i'm ;_) ) not a American who always blow of the higest tree ;-)\n\nI think KDE2 will make it anyway by its own, Ok little PR is OK, but not say things you cannot make true.\n\nKDE1 was also a success, but most people used it but did not say they did !\n\ni realy like KDE2 and i'm rewriting KXicq to KDE2!\n\nbut please let KDE2 be what it is and not more and sure, not less !"
    author: "Herwin Jan Steehouwer"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "I agree that showing that you can look like the other dt is not the way to go. What will sell in America is the features that you will bring to the market. To be honest, even mentioning or begging comparison with the competition I take as petty, and sort of a pathetic, and bitter response. Lay out what they're getting in their package. Don't use the other guy as a (low) benchmark."
    author: "matthew"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "I agree that KDE has generally lacked good PR, but yet according to some surveys it is still the most populate user environment out there.  \n\nThis clearly indicates that whether the competition has a better approach to PR or not - people like using KDE.  It is this quiet confidence that attracted me to KDE in the first place.\n\nHowever having said that, how do you attract future Linux users to KDE?\n\nThe KDE PR has to be at multiple levels and geared towards specific audiences.\n\n** A PR effort is required to attract third party application developers to KDE.  Applications, commercial or otherwise, are critical to the future viability of KDE.\n\n** A PR effort is required to attract corporate/business users to KDE.  A focus on productivity, ease-of-use and applications.\n\n** A PR effort is required to attract \"everyday\" end users to KDE.  Again ease-of-use and applications.\n\nA focussed approach will allow KDE to effectively reach the right audience with the right message.\n\nFor example:\n\nA PR effort based on technical superiority or politics (\"we have a better component model that the competition...\", \"we have a superior theme engine...\", \"we are GPL compliant !\", etc) will miss the point when you are trying to attract novice end-users to KDE.  KDE must be made relevant to their day-to-day computing needs - \"You can use KOffice to do...\".\n\nAn overall PR strategy is required, but specific individuals or groups must be responsible for PR endeavors geared towards specific audiences.\n\nAnother area may be to relook the design of the KDE home page as it is generally the first contact that new users have with the KDE community.\n\nMaybe the design should combine a more marketing orientated look-n-feel with the technical information that is currently available.\n"
    author: "Antonie Fourie"
  - subject: "The main reason I prefer KDE"
    date: 2000-10-11
    body: "Although I enjoy KDE and Gnome nearly equally the same. Personally, I find the main reasons why I prefer KDE is it's sloppy focus.\n\nIn the KDE Control Center, under 'Window Behavior'/'Properties' I use 'Focus follows mouse' and 'Click Raise' as Focus policy.\nPreviously in fvwm and afterstep this was known as sloppy focus. I think this is a key GUI functionality that is not publicized enough. This option provides me with what I'll refer to as a 'reference window'. I can be either editing or running a script in one window, while referencing the contents of another (perhaps very large) window which can be overlapping my window. This allows me to compare output which may be used for troubleshooting, running test scripts, or simply formulating an email response. What's great about this is that everything stays in place. I'm not clicking between windows to activate them, nor is a window being raised automatically which could obscure my view. (Gnome has a 'sloppy focus' feature but it doesn't provide the same functionality)\n\nI utilize this method regularly since I find it most efficient. Furthermore, I think that many who would use this feature might never want to have it any other way.\n\n\nBrian Fisher"
    author: "Brian Fisher"
  - subject: "Re: The main reason I prefer KDE"
    date: 2000-10-11
    body: "Sawfish (Gnome's default window manager) can be configured easily to do exactly this."
    author: "Jamin P. Gray"
  - subject: "Re: The main reason I prefer KDE"
    date: 2000-10-12
    body: "Sawfish comes with this behavior by default. I personally don't like it but try out Sawfish for the first time and thats what you get. You also can get this feature from enlightenment ( and probably others that I havn't tried)."
    author: "Kevin Vandersloot"
  - subject: "Re: The main reason I prefer KDE"
    date: 2000-10-14
    body: "As others have noted, sawfish and E both have this behaviour available. I use WindowMaker, and it also does this, by default I think, but if not by selection, and does it well."
    author: "Arker"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "I think it is missing a daily or weekly summary/digest of the KDE world, including digests from the devel lists as well as application reviews, presentation of projects and announcements of releases. .kde is already fulfilling some of these items.\n\nRegarding themes, why should I settle for an imitation when I can get the real thing? I've always preffered the(fast) non-intrusive themes to the baroque stuff. Simplicity, speed and productivity should be the major 'selling' points.\nI like to look at enlightenment, but i prefer to use kwm for example.\n\nJust my personal contribution."
    author: "JS"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "The weekly summary idea is actually in the works.  Our plans are to provide a weekly summary of the kdenews.org news items, with some additional content thrown in (insightful user comments perhaps, extra development tidbits that don't belong on the main page, CVS stats, etc).  Look for it soon!"
    author: "Bill Soudan"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "My 2p on that:\n\nseen that already:\noracle vs sybase, Ms vs apple.\nthe technically superior ( assumed sybase, apple here) were so sure that \"IT will shows by itself\", that they did not do so much about PR. and also, they were targeting brother-technicians, ie initiated, not \"normal people\", or corporates.\n\nthe competition, reversely, knowing that they cannot compete on the technical side, pushed on Pr and marketing. they didnt talked to the technicians, they talked to their boss. oracle and Ms were targeting corporate heads, and the PR was targeted at them.\n\nwe all know what the result was for these two head-to-head.\n\nbut probably techies prefer doing nicer things than  lying, lobbying, and managment talking...\n\nwe all know that corporate talks bs makes no sense, ... except for corporates..."
    author: "Luc taesch"
  - subject: "Re: Improving KDE Public Relations"
    date: 2001-09-10
    body: "All communications regarding Public Relations must be addressed to me in future. \nThanks"
    author: "Shashi Thapar"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "Wouldn't it be good to link the KDE annoucement to a general review of KDE2 and its features? Aren't there people ready to write a piece about a specific part of KDE2? Here a things I think about:\nKOffice\nKonqueror file manager\nKonqueror Browser\nKmail\nControl Center Themes\nControl Center Localization\nControl Center Personalization\nKDE sound system (Arts)\nKDE Multimedia features\nKDE2 Games\nKDE2 Utils\n+ several developers features\nkio\nkparts\netc, etc\n\nAs you see, there's a lot of work. Are there people interested in writing such a review? I'm ready to participate actively!\n\nRaph"
    author: "Raphael Bauduin"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "I think KDE's PR should be done by the users, leaveing the devlopers free to give us some great code.\nI agree that many projects become bogged down with PR, and therefore the product is not as good as it could be.\nSo, my fellow users, lets get to work doing PR for KDE, and leave the developers to their work.\n\nJoshua"
    author: "Joshua Kuebler"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "The idea looks good.. it's true that probably only the Enlightenment team is more secretive than the KDE group =) But at the same time it's up to the numerous fans to help as well...\nPersonally what I would do first and foremost is to improve the www.kde.org site... prove me wrong, but even the frontpage (is Linux ready for the desktop?\") didn't change for the last 1.5 years... and that's THE starting page where anyone interested in something new looks for info. Persoanlly, I have seen MS Windows themes pages which are much more appealing (don't get me wrong, it's \"constructive critisism\").\nSolution? The KDE developers don't want and don't have the time to write flashy homepages instead of workin' on code (especially now =))) So why don't we get together 10-20 people helping them? Ok, I can't write coe and don't know where Perl is, but I can write a review of the KDE appz that I use... and if everyone does the same, we'll have plenty of reviews/tips&tricks/howtos etc to constructively advertize KDE.\nAnyone interested, email me!\n"
    author: "Divine"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-14
    body: "The web-team has been working for the last months with a new look and feel for the web-page, and the way it looks now, we'll be able to launch the new page when we ship kde2.  :)"
    author: "Christian A Str\u00f8mmen"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "Something I hear a lot is 'kde looks just like windows'.  I think shipping a couple of hardcore, over the top, purposefully pure eye candy themes would help out.  I dont really see any in the 1.94 release (Even the marble ones look too useful, not enough hacker like :).  Also, if SMB really works we should get the word out.  And finally showcasing konqueror wouldn't hurt I dont think, but dont talk about the javascript...\n\nI would showcase kword but it cant really compete with SO for importing word files."
    author: "Spud"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "Personally, I don't see why a GNOME look and feel is a problem, nor do I understand how it's stooping to the competition.  You're highlighting KDE2's configurability; what better way is there to illustrate this than to make it look almost exactly like another desktop?  It's not like you're hard-coding the GNOME look into KDE2.  It's an option, and a good one at that.  If a user does not want it, he or she does not have to use it.\n\nAdd the GNOME libraries to the system, and there will be virtually no reason anyone could claim that KDE2 can't do for them what GNOME can.\n\n(Note: I don't have any real biases either way with regard to KDE or GNOME.  I'm just responding to the responders who say that making KDE2 look like GNOME -- even temporarilly -- is stooping.  I strongly disagree.)"
    author: "Erik"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "I find that the argument of: \"I know KDE is more stable and has more features but I still think GNOME looks better!\" is something that I hear extremely often!\n\nI don't understand why they think so since GNOME looks awful to me but there you go. So because of this, I think it would not hurt KDE at all of we actually did show some thing like \"You like that look? Oh, no prob, click here and here and here and presto! - but wait, there's more!\".\n\nIt's not stooping in any way. At least I don't think it is. It's silencing silly comments before they even get a chance to start. After all, the other project has all it needs in terms of PR what with large commercial interests backing it - we don't need to let them win a simple battle we could not lose...right?"
    author: "Mukhsein"
  - subject: "Steering the debate"
    date: 2000-10-11
    body: "> After all, the other project has all it needs in > terms of PR what with large commercial\n> interests backing it - we don't need to let them > win a simple battle we could not lose...right?\n\nThat's EXACTLY the point. Although many or most of us don't like the competition's look, by providing an optional compatible style we can actually steer the debate away from style issues to more substantive things. Let's remove this red herring once and for all. Yes, this is known already amongst all KDE developers but the word has to get down to the masses at large. The \"look and feel straw man\" is one of the last distractions put forward by many loud voices on the other side. Let us remove this distraction once and for all. It should not take much work. KDE has nothing at all to lose in this regard and everything to gain.\n\nLet's not be reactionary; let's steer the debate!\n\nSamawi"
    author: "Samawi"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "I completely agree! I am new to the whole linux thing and wanted to just spend (really not to much) some time on testing KDE or GNome. There is lots of talk which one is best, but if you are a newbie like me you really don't know whom to trust, so I decided more or less emotionally. The look of an desktop is then of great importance and if KDE can look whatever desktop you like, it is an advantage (one of many!)"
    author: "Frank"
  - subject: "Nasty nasty nasty"
    date: 2000-10-11
    body: "Please!  Put a warning before that screenshot link or something!  I almost puked!\n\nGnome is so ugly, badly designed, and its icons are terrible!  The new Gaim has taken on Gnomish icons, so I had to set my prefs to show text and not icons now, just because they're so ugly (and they're not intuitive at all, KDE's are)."
    author: "David Walser"
  - subject: "Re: Nasty nasty nasty"
    date: 2000-10-11
    body: "This is all a matter of preference.  I personally use Gnome for a lot of reasons, aesthetics being one of them.  What I found interesting about the screenshot is that it didn't really look like Gnome. it had a few of the same icons, but that's about it.  Sure you probably could make gnome look like that, but by default it looks rather different.  But one thing I will agree with, the screenshot is ugly.  :)  I much prefer my Gnome setup...it's beautiful..."
    author: "Jamin P. Gray"
  - subject: "Re: Nasty nasty nasty"
    date: 2000-10-11
    body: "> What I found interesting about the screenshot is that it didn't really look like Gnome.\n\nOh well --- I put that icon-theme together in something like 15 minutes to test certain aspects of our icon-engine. So my major goal was not to \"emulate\" a perfect gnome. Still I'm sure that one could ask a gnome-coder to recreate konquerors widgets in gtk while using gtk's icons and themes. If one would do a screenshot of that and say it would demo KDE's themability people like you would still reply the same bullshit.\n\nTackat"
    author: "Tackat"
  - subject: "Re: Nasty nasty nasty"
    date: 2000-10-12
    body: "Gaim????\n\nDo you use some of the features Gaim has that Kit lacks or something?\n\nTake a look at \"Kit\", in kdenetwork, if you haven't already."
    author: "Neil Stevens"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "I've thought of this before, and have meant to start an effort which in my mind would be called Kameleon, which would create packages to make KDE2 mimic other desktops through the use of icons, color schemes, widget themes, and kicker applets.  With these things all being completely configurable, it's totally feasable to morph the default KDE desktop into an almost exact copy of any desktop I can think of.  It would a great draw (IMHO) if KDE could easily be made to look like an enviroment that new users might already be familiar with (Windoze, MacOS), and I agree it would be great PR to say, \"you like the competition's look? then fine, have it!\""
    author: "the_1000th_Monkey"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "Great idea! It should also apply to keyboard shortcuts, single-click/double-click. There is also the Mac-Menubar, Focuspolicy, panel/taskbar mode, system sounds, button order in dialogs, ...\n\nKameleon is a great name for that, please go ahead and start that project!"
    author: "ac"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "the kde team creates software; gnome issues press releases on what it is getting ready to start doing about the process of beginning a new alliance that will ultimately look into the idea of creating software...<P>\nin example, kde now has a quite workable software suite...gnome has gnumeric...it did have abiword, but per stallman's recent interview, has abandoned that to whoever wants to continue playing with it...instead it now, or soon, will have star office, a klunky old dog that not even sun was able to turn into an attractive product...but putting the gnome footprint onto star office doesn't integrate it into gnome, that's a one to three year process...in short, at now, gnome has only vapor, promise, and press release for an office suite...<P>those who use computers will use kde...those who only talk about how good things are gonna be someday will continue talking about gnome...kinda like george, lenny, and the rabbits..."
    author: "frank"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "Wow, it really is like George, Lenny and the rabbits (I like that book)-- I found AbiWord pretty weird, and KWord to be superb, especially with KParts. And I don't like StarOffice that much. \n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "Hi there,\n\nI DO agree with quite everything said in the article - I do hear a lot about Evolution-Pre-Beta-29,234 with 2 lines of changed code (just to name an example). \nBut it was, half a year ago, hard to get any news about KDE issues. That is one point which IMHO *has* to be changed AFAP. \n\nAnother point, which has been already mentioned, that if you *can* give KDE2 quite every look you want, why don't you include some popular ones in the standard distro? If there's someone, who likes the gnome look (btw. I don't ;), give it to him. Also include some windoze-themes for some crazy guys, who like that style of computing. \n\nKDE2 really *is* something very cool, so why don't promote it? \n\nIf someone want's to organize a PR-Team (or if the existing needs help) I am there for you. \n\ncheers!\nManuel"
    author: "Manuel Zamora"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "One big disadvantage KDE has on the PR front is the lack of a celebrity front man. Miguel de Icaza serves as the public face of Gnome, while KDE is a software collection that periodically emerges from somewhere in Germany. The result is that KDE is viewed as a faceless corporate product while Gnome is perceived as a project of the hacker community -- despite the fact that Gnome is much more driven by corporations (Red Hat, Sun, Eazel, Helix Code) than KDE is. \n\nBSD suffers from a similar issue. Linux is viewed as the project of Linus and his hacker buddies even though it's vastly more corporate-driven than the BSDs.\n\nOne of the reasons I enjoy contributing to KDE is the complete lack of ego and empire building. It's unfortunate that it works to the project's disadvantage on the PR front."
    author: "Otter"
  - subject: "Yes."
    date: 2000-10-11
    body: "Yes, the lack of ego and the honesty of KDE team is a great advantage.\nThe community is broader.\nThere is no disilusion.\n\nKDE is more like DEBIAN. \nJust real and good code. \nJust thankful users.\n\nLong life !"
    author: "thil"
  - subject: "NO!"
    date: 2000-10-15
    body: "No!\n\nIguana's PR efforts are notoriously succesful. Without his lobbying efforts Gnome would have been dead by now.\n\nThe Big-Iron support he managed to get was a killer, and I dislike him for that... ;)\n\nHis machismo works wonderfully in the Californian sun, where the cool and eurocrat temper doesn't work.\n\nIf KDE doesn't have a self-proclaimed diva then that is too, too bad. It may be hard to swallow such personalities, but they are needed in the US, where competence and technological arguments don't sell.\n\nThe anti-hero Alan Cox is the closest Europe has come up with (apart from Linus), but he is allied with Gnome, so he is out of the question.\n\nPerhaps Kalle Dalheimer? He is a talented guy, with a sense for style, and not too shy.\n\nKalle, will you be our poster guy?"
    author: "Arnold"
  - subject: "KDE can't look just like the competition"
    date: 2000-10-11
    body: "KDE's panel must go all the way across the screen. That sucks for people using Xinerama. This is the only reason I don't use KDE. \n\nI think if you wish to improve your image, you should let your code stand on it's own (which it does, for the most part), rather than bringing your competition into it."
    author: "Jooky"
  - subject: "Re: KDE can't look just like the competition"
    date: 2000-10-11
    body: "<i>KDE's panel must go all the way across the screen. That sucks for people using Xinerama. This is the only reason I don't use KDE</i><p>\nThat's one reason I don't use KWin and Kicker.  I use BlackBox instead, but run all the other nice KDE2 programs, including KControl, KWrite, Konqueror, etc.<p>\nWith a 3200x1200x24 screen size, KWin and Kicker *do* start to break down, but many other window managers work just fine with KDE2.<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE can't look just like the competition"
    date: 2000-10-12
    body: "<i>That's one reason I don't use KWin and Kicker. I use BlackBox instead, but run all the other nice KDE2 programs, including KControl, KWrite, Konqueror, etc</i><br>\n<br>\nThis is why I use the gnome panel, as I don't want it to go all the way across, I want it in the right had corner taking up only the room it needs, I don't use the task list app, I only use something like that for iconified apps, and I have that in E.<br>\n\nBesides the panel that is almost the only thing I use of gnome, the rest are just system utils.<br>\n<br>\nGordon.<br>"
    author: "Gordon Heydon"
  - subject: "Re: KDE can't look just like the competition"
    date: 2004-04-27
    body: "I do want my kicker to go all teh way accross.  And now it doesn't because of comments like this.  Argh.  Why don't they make it optional."
    author: "Ron"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "When i first started using linux i used kde, then i switched to gnome. i still think kde is great, but i always here the kde team talking about its technical superiority. What parts of kde is superior technicaly, ive never seen any examples.\ncould someone please list some."
    author: "josh mcgee"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "kfm"
    author: "Odair"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "kfm is superior in features because it can also do web browsing.\nBut gmc is superior to kfm in file management in general."
    author: "Anonymous"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-13
    body: "You can't be serious? gmc was clumsy piece of crap that segfaulted all the time.\n\nKfm isn't such a great filemanager, but it was the best one available on linux and it worked and worked well."
    author: "jannek"
  - subject: "gmc vs. kfm"
    date: 2000-10-13
    body: "IMHOP, gmc is far better. It doesn't segfault for me and I use it every day. Kfm is totally annoying, not only for wanting to be more than a file manager (displaying HTML inline is a bug not a feature so far as I am concerned - gmc will hand off the HTML to whatever browser you prefer) but also for the fact that it insists on being a \"desktop manager\" (my root window is NOT a \"desktop\" and I don't want it to be) and refuses to exit cleanly. Gmc at least will exit cleanly and not require an explicit kill. I'm told konquerer solves this problem at least - I'll see in a few days I expect, when the actual release version of KDE 2 comes out I'll be downloading it."
    author: "Arker"
  - subject: "Re: gmc vs. kfm"
    date: 2000-10-13
    body: "Do you know what ? The car that i owned 6 years ago was very rusty and sometimes didnt start."
    author: "Lenny"
  - subject: "kfm -w dude."
    date: 2000-10-13
    body: "Well, just that."
    author: "Anon. Advocate."
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "kfm sucks as bad a gmc, thats why both fms are being replaced. i still havent heard a good example."
    author: "josh mcgee"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "\"little lesson on marketing\"\n1) select your target group\n1.1) freeks\n1.2) windozese\n1.3) novices\n\n2) tell them THEIR benefits of using your product (this has usualy nothing to to with the great technical capabilities of KDE)\n\n2.1) much more technical stuff (dcop, io, mcop, artsd, technical details on integration.)\n\n2.2) differences to windoze (stability, speed, cost...) - someone mentioned the well know comparison lists.\n\n2.3) IMHO most difficult target group. What apps are provided, and what the user can do (and what not!!! still a considerable incompatibility to MS-world)  avoid all abrevations like css,ssh.... \n\nSo IMHO KDE needs (at least?) three specific PR-lines.\n\nGeneral: \n*) IT's free, no copyright, no cost. (together with linux as operating system)\n*) fast development\n*) fast bugfixes\n*) roughly 50 languages supported\n\n\n\nSpecific:\n*) Games\n*) Internet\n**)WWW-browsing (except many javascript-pages :-(\n**) Chat\n**) Mail\n*) Addressbook\n*) KOrganizer\n*) News\n*) Graphics\n**) Pixie\n**) Faxvier\n**) PS/PDF (except encrypted?)\n*) Office-Suite \n**) KWord\n**) KWrite\n**) KPresenter\n**) KChart\n**) KLyx (?)\n*) KPilot\n*Multimedia\n*) Configuration options\n\nForget (yes!) about all the great utilities - no one (almost ;-) will install KDE2 only because of its utilities.\n\n(Most) users want to\n*) get their job done \nor\n*) just play\n\ncu\nferdinand\n"
    author: "Ferdinand Gassauer"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "**) PS/PDF (except encrypted?)<br>\n<p>\nYou can look at encrypted PS/PDF with ghostview based viewers like kghostview. It was just switched off due to FUCKING american crypto laws and equally bad patents.\n\n<p>\nLook at <A href=\"http://www.ozemail.com.au/~geoffk/pdfencrypt/\">pdfencrypt/</A>\n<P>\nReally simple.\n\nAlso xpdf 0.91 (superior anyways) opens them.\n"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "Thanks!\nSo exactly things like this should be handled carefuly in PR and INSTALL instructions\ncu\nferdinand"
    author: "Ferdinand Gassauer"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "Whether or not it is stooping depends entirely upon how it is presented.  If you are discussing customizability, and mention it is so customizable that you can make it even look like the \"competition\" and show a pic to prove it, that is good.  But if you go off saying that you've incorporated features that allow you to mimic the style and feel of another interface, that comes across as sleezy.  Don't include the libraries, icons, or anything out of gnome as i've seen suggested.  The widget set, and maybe the fundamental images that make up the environment would be ok, but beyond that, it does look bad to a certain extent, depending mostly on the person using it.  I know I would have no problem with it, while other people I know would.  Also, include tons and tons and tons of themes :)"
    author: "dingodonkey"
  - subject: "Re: Improving KDE PR -- public perceptions matter!"
    date: 2000-10-11
    body: "<p>\nI'm one of those... um... Debian users, and I've been giving KDE a work out since it was added to the official mirrors.  I like KDE, have always liked it since using it with for example Mandrake or the Corel version :^) on some systems I've installed for others.  I'm dying to see Konqueror get to the point where it's my default browser (at this point, the Debian packages haven't stabilized yet).\n</p>\n<p>\nPublicity is *very* much needed for KDE.  Gnome has Red Hat to stump for it, and now The Foundation.  I think the \"figurehead\" comment is a very astute one.  KDE has no de facto figurehead like Linus or Miguel.  Such a central person really makes PR much easier and more effective.\n</p>\n<p>\nBut my main comment is something that jumped out at me right away when I read this article.  Please don't take this as a flame, I surely don't mean it as one.  But if any \"official KDE\" people say in a public forum that \"KDE is technically superior to the other desktop environments,\" that statement is likely to backfire.\n</p>\n<p>\nIt's one thing to be proud of the work you've done or to show off all the good things it can do.  But it's quite another to say this in the wrong way, where it can easily be taken as a boast.  Boasting can be very detrimental to PR because it turns lots of people off.  If this gets taken as \"We're better than all the rest of you,\" it's a very elitist message.\n</p>\n<p>\nThere is a strong need to be inclusionary.  So far KDE has very much been so.  Just make sure that a few misspoken words don't confuse people into thinking KDE is a snobbish, unfriendly project.\nJust my two cents...\n</p>\n<p>\n<pre>\n\t\t\tClemmitt Sigler\n</pre>\n</p>"
    author: "Clemmitt Sigler"
  - subject: "Re: Improving KDE PR -- public perceptions matter!"
    date: 2000-10-12
    body: "<em>But if any \"official KDE\" people say in a public forum that \"KDE is technically superior to the other desktop environments,\" that statement is likely to backfire.</em>\n\nFunny, but that's what the GNOME developers say all the time. How come they never get any flack for it?\n\nThe way I figure it, the more the other side howls the more KDE must be on the right track."
    author: "David Johnson"
  - subject: "Re: Improving KDE PR -- public perceptions matter!"
    date: 2000-10-12
    body: "<pre>\n> Funny, but that's what the GNOME developers say\n> all the time. How come they never get any\n> flack for it?\n</pre>\n<p>\nFrom what I've read the Gnome developers get plenty of flack from KDE bigots, and vice versa.  It's a two-way street, and all that energy is wasted in the flames.  No?\n</p>\n<pre>\n> The way I figure it, the more the other side\n> howls the more KDE must be on the right track.\n</pre>\n<p>\nIf you support the logic that two wrongs make a right, yes.  I don't think that logic is valid myself.\n</p>"
    author: "Clemmitt Sigler"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "I first used KDE in 1996 because a friend of mine told me about it (it was still an alpha version then if I recall correctly).\n\nFor great PR, there needs to be a PR team in each country promoting the product so that each campaign can be tailored for specific markets.\n\nThis need not be an expensive operation, but dates and events (not to mention websites) need to be co-ordinated.\n\nFeedback on KDE (opinions, discussions on various BBS etc) needs to be monitored by a central committee, so that the developers can be informed about what the users are asking for or appreciating.\n\nI would suggest something like http://www.mozillazine.org, which is a useful site taking feedback on all aspects of Mozilla and providing and 'unofficial' voice on developments.\n\nI hope this helps.\n\nCheers,\nMatthew\n"
    author: "Matthew Trump"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-11
    body: "Well, I generally think that the PR about KDE should be much more improved. We did a lot during the year on exhibitions all over Germany and we\u00b4re still doing, but the net could use some more especially after Qt going GPL. <P>For me personally, it\u00b4s not only KDevelop where I\u00b4m programming but the success of KDE as a whole where I only see KDevelop disconnected from the train when it comes to the KDE core developer usage (no claim to make KDevelop the choice for everyone here ;-)and our own time frame as we started much later than KDE did. <P>But I think we\u00b4re doing such a great job by what we get to know from attendants at the expos - and we could improve that a lot with more speeches, more news send out to the media, people placed publically with their success story to participate on KDE - in opposition to only having one face for KDE as there is none. The current interview series \"people behind KDE\" is a good start for that IMHO. \n<P>My experience: In my speeches on Expos, I\u00b4m thinking more in terms of \"selling\" our stuff to the people although it doesn\u00b4t cost them anything. But they are hungry enough to get it if you actually *show* what KDE does than wait for them asking what it can do for them.\n<P>\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "Concerning the image:\n\nftp://ftp.kde.org/pub/kde/Incoming/gnome.png. \n\nI did the icontheme to create this screenshot in about 15 minutes to test certain features of KDE2's new powerful icon-engine. Therefore it was *NOT* my major goal to create a perfect copy of Gnome (although it *IS* possible to create exact Gnome-look&feel using KDE2). I prefer the original look&feel of KDE2 though:\n\nftp://derkarl.org/pub/incoming/kde2shot21092000b.png\n\nSo I'm not really interested in improving the gnome-icon-theme. If somebody else wants to do it ... start here:\n\nftp://derkarl.org/pub/incoming/theme-gnome-0.3.tar.gz\n\nI would be happier though if people would create more icon-themes in addition.\n\nWithin the upcoming two months I'll work on new better application-icons for KDE2 (Mimetype-icons look quite perfect to me now), better folder-icons and Hicolor-versions of the small 16x16-icons (the current ones are based on a 40-color-palette and might suck in the eyes of some people).\nKDE2 already does support lots of features to customize KDE2's look down to your needs -- windowmanager-styles, widgetthemes (including support of gtk-themes which are being displayed faster than using gtk itself), iconeffects and iconthemes. The iconengine supports a lot of weird stuff. Also in the next two months alphatransparency for icons will probably be implemented. This would be the last feature missin g to create a decent original (for-private-usage-only-of-course) MacOSX-icon-theme (scalable icons are already supported by KDE2.0) :-)\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "I suggest that we buy national domains (with kde prefix) all over the world. For example, I could buy www.kde.dk, and mirror kde.org site, and translate it to danish. It would be much easier for Danes to access, and to read kde news. I can easily find some people in Danmark who would like to share expensies connected with this. \nSecond, The Danish Ministry of Technology is\nvery interested in replacing Microsoft in all\npublic computers (great news for Linux). I am going to burn some cd's (with KDE2 when it is released) and send to that ministry and all\nministers in danish government."
    author: "Zeljko Vukman"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "Firstly, I think it's great that KDE is starting to recognize the need to be pro-active in the PR dept.  I also like the idea that you can push real stuff not vapourware.\n\nLet's be real.  You don't have the M$ PR budget.  You are not the default desktop on the largest distro, SuSE is a good second thou.  You will probably never be the hackers choice and nor should you try to be.\n\nAll is not lost :-)   \n\nYou need to find large companies willing to commit to the KDE product.  Hp, Compaq etc companies who have hardware, may have Linux, but companies that need office software and are willing to run KDE to get it.\n\nThe bullet points below are just some ideas to present your message.\n\no     One central repository for all K stuff, this site is perfect for that.  News should be published here first.  Links to any mention of K stuff on the web be it /. or the London Times.  Links to all K projects.  Make sure the links are working, if not don't display them.\n\no    One contact on each project for PR contact, doesn't have to be a developer, just someone who knows what's occuring and is able to talk to non-techie people.  This might be the hardest role to fill on OS projects.\n\no     Make sure you have many recent screenshots easily available on the websites, how long did it take for the KDE2 screenshots to be available, too damn long!\n\no    _Monthly_ Digests of all KDE features/apps, at least KDE/KOFFICE/KDEVELOP I'm a developer and I had no idea Kdevelop existed until I started hunting around.  Once the digests are up and running, make sure they get posted to the Linux news sites.  Non Linux people will only be interested in big news, not the small stuff.\n\no    You have a big release coming up, is this going to be reported in the mainstream IT press?  Have you made sure all the distros are aware?  Who is going to be the first distro to announce they're shipping KDE2?\n\no    When Koffice is ready make sure the mainstream IT press is aware.  Get yourself on the office software reviews, be compared to M$, Lotus, StarOffice, Applixware etc.  \n\no    Likewise get Konqeuror in the browser reviews.\n\no    A box running KOffice with some connected Xterms is all that most small offices need, can you find a partner for this?\n\no    Link to all articles about KDE, especially bad ones.  In fact highlight the bad reviews and give either a refutation or a date when the problem will be resolved.  Do stuff that M$ can't or will not.\n\no    Find reference sites, who is using K stuff?  How much, why etc.  Not just single users but commercial sites, publish their details.  the cost benefit of KDE/KOFFICE in a large company is huge.  But they need to know you're here."
    author: "Usquebaugh"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "Let's be real. KDE have no PR budget!;)"
    author: "AC"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "<P>You have some nice ideas, but they are, AFAICT, total vaporware.  Which of your ideas are you planning on implementing?\n</P>\n<P>Face it, nobody's going to follow your instructions.  Things only get improved in an open-source project if someone does it.  Why don't you help, pick one of your list items and implement it?  Maybe you can do the monthly digest?\n</P>"
    author: "Usquebaugh who?"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "Of course they're vapourware the original poster was looking for new ideas!\n\nHow do I go about implementing my ideas?  Most of them require the use of this website.  I can submit links but that's no good if there isn't someone putting the links up and co-ordinating the submissions.\n\nI cannot be the PR contact as I have neither the people skills or the backing of the dev team.  Likewise, I have no role on the dev team so how do I do the digest?\n\n3rd party liasons, oh like they're going to listen to me.\n\nThe only way I can help is with questions and comments to mainstream press.  Well gee I'm already doing that, not just for KDE.  But I'm not part of the team so I sure can't speak for them.\n\nThe whole focus of my ideas was that you need to foster favourable PR."
    author: "Usquebaugh"
  - subject: "Hackers choice"
    date: 2000-10-13
    body: ">> You will probably never be the hackers choice and nor should you try to be.\n\nWhy? I consider myself as hacker (I event get money for hacking) and I use KDE as my major Desktop, although I also use GNOME and would install xfce on slower machines."
    author: "Wolfgang Winkler"
  - subject: "Re: Hackers choice"
    date: 2000-10-13
    body: "The dev team has a limited amount of resources.\n\nSo the hacker market is  small if vocal.  As you confirm it's not a loyal market.  \n\nSpend the time making KDE  more appealing to the enterprise clients.  If a market is created for KDE developers then more people will develop for KDE."
    author: "Usquebaugh"
  - subject: "Re: Hackers choice"
    date: 2003-10-04
    body: "hello , l just wanna know u more"
    author: "mark"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "<p>I think the reason KDE is having so much trouble is simple.  KDE is *not* vastly superior to GNOME.  It may be a little better in some respects, but it's not overwhelmingly better.  Most things KDE can do, GNOME can do too.  And if not right now, GNOME will do them in the near future.  So the feature fight is a moot point.\n<p>\nI think concentrating on features misses the point.  This struggle is about mind-share.  It's not about who has the better office suit or smoother interface.  Those things are irrelevant, because they are near-term advantages.  The long term advantages come from massive mind share.  Once you have hordes of developers supporting you, you will get everything, even if initially you didn't have some feature or whatnot.  Plus, even if you lack some features, what will count in the end is over-all support, which is again mind share.\n<p>\nPeople gathered around Gnome, not because it's technically superior, but because 1) it was good enough and 2) it was in a good strategic position that was open for future growth.  It's funny how important GPL is.  What's funny is that even though GPL was invented to protect the consumer, it has an excellent side-effect of protecting big corporations too!  GPL is even *more* important to a big corp, than to a small developer.  A big corp needs assurances that it won't lose competitive advantage, and that the space won't get fragmented.  And GPL does that.  PR that.\n<p>\nNow that we're down to mind share, the issue is simple.  KDE's license used to be simply unfair and dangerous to most developers out there.  WHO wants to develop some software, if there is even a little chance of it become closed later on?  Who wants to work hard in order for someone else to later close the software and benefit from your work?  Very few.  Sure, there is BSD.  But why is BSD not winning the mind-share?  It's the license.  LICENSE MATTERS.  No matter how technically superior the product is, if the license is unattractive to developers, it will lose in the free software world (in the corprorate world it doesn't have to lose if you have a significant financial clout).\n<p>\nGnome may not be the best (altho it's darn good), but it has *succeeded* in attracting a lot more developers, and it will keep succeeding.  Gnome from the get-go had an attractive license.  If ppl contribute some code to Gnome, they have a peace of mind that no one can take advantage of them.  This has been a huge factor in gathering a huge mass of developers around it, and that in turn creates a huge inertia/momentum.  And we all know how critical this inertia is.  Look at Microsoft.  It has been providing inferior products for EVER, but it still dominates.  That's market inertia at work.\n<p>\nKDE2 now has a better license, but it has lost critical momentum.  Many developers have already commited to Gnome.  Why should they switch, when they are in majority?  There is safety in numbers.  Even if KDE2 is 2x better, that's not a compelling reason to switch.\n<p>\nSince Linux is *not* a *desktop* OS, customers right now do *not* influence the desktop market.  It is the developers.  If/when linux becomes a desktop OS, then the casual customer will have an actual impact.  Then the developer will think, \"well Gnome has a bigger following and is safer now, but KDE2 has the customer base, and since I want to sell my product to customers, I want the biggest market...\" and so on, and then KDE2 might have a chance.  But even then, it will be at a disadvantage, because Gnome will have a head start with the broad support.\n<p>\nI think KDE2 could do better by being a closed-source product.  You either open your source all the way, right away, or you better close the source and start charging money for it.  There is no middle ground that will succeed in a major way (like gain majority acceptance).\n<p>\nI don't think KDE users have anything to worry about though.  I think KDE2 is here to stay for a long time.  It's not going to take over the world.  No way.  And it's not going to go away either.  I think it will be a stalemate, like Emacs, X-Emacs.  I little bit of positive noise (PR) can help KDE gain desktop consumers.  I doubt they'll gain more developers than Gnome though.  However, in the very far future, the desktop end-user might bring developers back to KDE, although not everyone.  Mostly for-profit people, I think."
    author: "Aeoo"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "\"KDE's license used to be simply unfair and dangerous to most developers out there. WHO wants to develop some software, if there is even a little chance of it become closed later on? Who wants to work hard in order for someone else to later close the software and benefit from your work?\"\n\nKDE's licensing problems were, AFAIK, never open vs. closed. KDE itself was GPL'd. The problem was the issue of possible license incompatabilities between KDE and Qt. \n\n\"But why is BSD not winning the mind-share? It's the license. LICENSE MATTERS. No matter how technically superior the product is, if the license is unattractive to developers, it will lose in the free software world (in the corprorate world it doesn't have to lose if you have a significant financial clout).\"\n\nThe popularity of Linux as opposed to the *BSDs has probably more to do with the leadership and culture of the two projects, as well as the circumstances. i386 BSD was intended as a serious project, and like the free software projects of the time, including GNU, it was developed in a cathedral fashion. From what I can tell, Linus didn't think of Linux as a big deal, and he initially released Linux with the attitude that it was a fun thing that was starting to get good that he just thought he'd like to share. In effect, he invited people to hack on Linux, give feedback, and join him in some serious fun, and it snowballed from there.\n\nI would say that KDE's licensing did make for some rocky PR, but not the way that you're thinking."
    author: "J. J. Ramsey"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "<p> Gnome may not be the best (altho it's darn good), but it has *succeeded* in attracting a lot more developers, [...]</p>\n\nPlease explain me, why do you think GNOME has attracted more developers. Or even better try to give some hard numbers. \n\nFrom my point of view GNOME will not attract much C++ coders since KDE is by far superior on this field and the GNOME folks concentrate too much on C. In fact (AFAIK) there are a lot more people who like C++ much more than C. The C++ support for GNOME is IMHO not very good (friendly speaking). I had a look at KDE & GNOME before I decided to use KDE and I am very lucky with this decision, especially after reading this slashdot \n<a href=\n\"http://slashdot.org/articles/00/08/10/123221.shtml\">\narticle</a>\n\n<p>ciao, Marco</p>\n"
    author: "Marco Krohn"
  - subject: "Re: Improving KDE Public Relations [the URL]"
    date: 2000-10-12
    body: "[the missing URL]\n\n... after reading this slashdot <a href=\"http://slashdot.org/articles/00/08/10/123221.shtml\">article</a>\n\nciao, Marco"
    author: "Marco Krohn"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "You are the best example for the bad shape of kde-marketing:<br>\n- kde was ALWAYS GPLed<br>\n- maybe more people code for gnome. So there must exist muchmchmuch more and cooler applications fro gnome than for kde... but wait, where are they ? can it be that coding large projects in C instead of C++ is a waste of time and energy ? Just maybe ?<br>\n- lost critical momentum ? Linux desktop share is about 3%. The game just started. And i dont think gnome will prefered by the most windows and mac-users<br>\n- gnome has a huge support ? gnome is supported by red hat, but kde is supported by red hat, mandrake, caldera, suse... where are your numbers ?"
    author: "Lenny"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "1. gnome apps can be written in alot more languages than just c and c++\n\n2. gnome has not been around has long has kde, kde had about a year ahead of gnome to begin with\n\n3. gnome has support from red hat, helix-code, eazel, sun, and many many more, apparently someone doesnt read the news and cant count."
    author: "josh mcgee"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-13
    body: "Oh cool, Helix adds icons, Eazel adds a filemanager. Kde has both of them. So what ? Do also consider cygnus supporting Gnome because of gcc ?"
    author: "Lenny"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-13
    body: "Oh cool. KDE artists adds icons, KDE developers add file manager.\nGnome has both of them. So what?"
    author: "Anonymous"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-13
    body: "You're right. KDE doesnt use big mouth words, that it has a huge corporate backing even for the simplest things."
    author: "Lenny"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-14
    body: "So companies adds features to Gnome. So what?\nThey are humans too! What they make is GPL'ed too!"
    author: "Anonymous"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-14
    body: "You guys have theKompany helping development in KWord and other software, TrollTech for the toolkit, and various distributors.\nLooks pretty much similar to Gnome's relations with companies."
    author: "Anonymous"
  - subject: "The myth of KDE being older"
    date: 2000-10-13
    body: "KDE is a whooping 7 months or so older than GNOME.\n\nThat is, KDE is about to be 4 years old, while GNOME is 3.5!\n\nNow, some GNOMer will tell you that KDE got a bigger headstart because we had a more mature toolkit. Well, if the toolkit made all that difference, GNOME should have cloned Qt, not KDE. Don't say we get an advantage because you choose the wrong path ;-)\n\nOther than that: Ok, we DID (and do) have a better toolkit. But we did develop our own html widget, while GNOME ported ours. We started a file manager from scratch, while GNOME used mc (which was a very bad idea, apparently). We coded our own WM, while GNOME used E... which apparently turned out to be another bad idea, according to the enthusiasts of sawfish.\n\nSo, the pattern is that GNOME claims KDE has a lead because... KDE made better choices in things that saved KDE work. But then again, there are lots of things KDE did do that GNOME didn't. So not even that is true.\n\nIn short: KDE has a lead because KDE is better planned, and has a more solid choice of foundation blocks. And that difference is enough to make KDE be ahead even doing MORE than GNOME is.\n\nGNOMers, quit whining, and start designing, coding, and engineering. You need it."
    author: "Anon. Advocate."
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "Like many people I've tried both KDE and Gnome. I like KDE better because:\n\n1. It's more stable\n2. It's technically better - and is in C++ :-)\n3. It's further along in development\n4. Looks better (controls don't take up so much       room etc - in the default themes at least)\n5. I think KDE 2.0 is just the start...\n\nMaking the most of these current things is important, but I'd also like to see some better PR for some of the upcoming things. Some of the projects coming out of http://www.thekompany.com/home/\nlook *really* cool. Imagine being able to browse an sql database from within Konqueror (kio_sql) I'd use that every day. Most people haven't heard about all that stuff, and it's coming...soon.\n\nHaving said this, I think the one area where GNOME has the lead is in availability. helixcode.com has made it *stupidly* simple to not only download and install  GNOME, but to keep it up 2 date! Not just the core stuff, but lots of utilities as well. I think KDE needs to have something similar.\n\nThere's so much new stuff happening around KDE, so many new projects, so many useful apps that it's impossible for the average KDE user (somebody who works in the KDE environment, but doesn't want to spend hours each week maintaining and upgrading it) to keep up2date.\n\nAnyway, just my $0.02\n\nRich"
    author: "Richard Sheldon"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-14
    body: "Actually, the thing which really got me about KDE was the fact it's actually decently fast! This is what worries me about all this talk of transparent icons implemented in software, etc..."
    author: "Nemo Torontonis"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "Showing that KDE2 can look like GNOME isn't a bad idea, but it definitly shouldn't be a prime PR focus (just an example from many of KDE2's flexibility) and PLEASE use a screenshot where it actually does look like GNOME. The link above looked like some bastardized hybrid of KDE2, GNOME, WindowMaker, and BeOS. You'd especially have to get the colors and window decorations down. GNOME does not have bright blue and teal colors. They're more unsaturated and subdoed."
    author: "Bart Szyszka"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "I use kde because it doesn't crash like to competition. I'm not wooed by PR bs.\n\nLoren Brookes"
    author: "Loren Brookes"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "My humble suggestion:\nMake the initial experience completely positive.\nWith software, especially this type of software, negative experiences spread by word-of-mouth vastly amplified and accelerated by mailing lists, newsgroups, IRC and media.\nThis means that if KDE 2.0 is being targeted at novices** it should:\n<br>1. Be astonishingly easy to install in a variety of circumstances. The RPMs and binaries help a bit here, but foolproof and instructions are also needed for each distribution. Distributors can help by packaging KDE 2.0 doco as well as code with their distribution. SuSE is an example of this for KDE 1.1.X.\n<br>2. Be foolproof and bulletproof, with no hidden gotchas and blowups. The betas and bugfixing helped here. But be aware that KDE 2.0 will be released with non-critical bugs. See the buglist.\n<br>3. Come with friendly support. Distributors can also help here. Mailing lists, newsgroups and IRC will also help. We (experienced users) need some self-restraint. Novice users (and hopefully ther will be many) don't yet know all the rules of etiquette (RTFM, etc.) so we MUST be patient and gentle. Otherwise they will tell the rest of the world about their negative experience.\n\n(**eg. people who don't recompile their kernel twice before breakfast)"
    author: "Paul Leopardi"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "One way of doing better PR is stopping the stupid\nkicks at the GNOME project. It seems that ever since the GNOME Foundation announcement the primary concern of KDE is to badmouth GNOME.\nFor instance the sentence: <I>despite its technical superiority among various desktop environments</I>, should instead be written like:<I>despite having a very good technical foundation</I><P>\nI mean how many news postings on Gnotices have you people seen which tries to promote GNOME by giving small kicks at KDE (answer: none)<P>\nOf course if it is the goal of the KDE project to start a new flamewar between the projects this is a good start, hey, maybe we soon see Gnotices postings saying stuff like:<I>GNOME the only desktop solution with developers competent enough to make a working CORBA component solution</I> or <I>unlike some other slow projects the GNOME project didn't need two years to implement xdnd.</I><P>\nThat didn't sound nice did it?<P>\nPlease start to focus on what's good about KDE instead of trying to badmouth the competition."
    author: "Edward Bloom"
  - subject: "Mee too"
    date: 2000-10-12
    body: "Of all the very constructive proposals in this discussion, this is the one I agree with the most. Kicking the competition to promote yourself is not only immature and shooting yourself in the foot, but as far as commercials go, some countries/states even prohibit such practices.\n\nWhile many companies sponsor KDE coders, it seems some sponsor money should go into hiring real, educated, experienced and professional PR people to help out the existing team..."
    author: "Haakon Nilsen"
  - subject: "Re: Mee too"
    date: 2000-10-12
    body: "<p>\nI heartily \"third\" this post expressing concerns about the attitude taken towards all other desktop environments.  (Please see my earlier post.)\n</p>\n<p>\nWhen I read this editorial, that statement (\"technical superiority\") jumped right out at me and colored my whole opinion of the article.  The reason I tried KDE when it was *finally* added to Debian (congrats to TrollTech, you've done an excellent job!) was because I thought it had a good feel and good apps (Konqueror), and also because KDE spent their time coding, not bragging or forming foundations.  My understanding of KDE was that it \"just worked.\"\n</p>\n<p>\nBut if the attitude of KDE developers is that it \"just works better than all other desktops,\" I myself can probably poke some holes in that assertion.  I know a bit about KDE and Gnome, enough to know what their relative merits are, and both have advantages and disadvantages.  (And let's not forget xfce.)\n</p>\n<p>\nI guess what I'm saying is that those who live in glass houses shouldn't throw stones.  If you don't want to encourage Gnome bigots to point out your glaring holes (and *every* project has glaring holes), then just shut up and keep coding!\nPR is one thing.  Saying you're better than everybody else is quite another.  This is a lesson well learned -- pride goeth before a fall :^)\n</p>\n<pre>\n\t\t\tClemmitt Sigler\n</pre>"
    author: "Clemmitt Sigler"
  - subject: "Re: But not Mee"
    date: 2000-10-13
    body: "The writer has the freedom to express his point of view of KDE's \"technical superiority\". I have that freedom too, and so does everybody else.\nThe bit about \"point out your glaring holes \" is called \"a bug-report\" and is a Good Thing."
    author: "reihal"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "KDE team made two major mistakes with KDE1:\n\n1. Shiped KDE with Windows style by default. It was terrible mistake, because:\n\na) main KDE target - linux users hate windows\n\nb) KDE1 in windows style looks extremely ugly, because of ugly toolbars. The guy who implemented that code deserves to be shot.\nOn the other hand KDE1 in Motif style looks great.\n\n2. KDE project never cared about the quality of binaries distributed. All linux distributions and FreeBSD shiped binaries built with exceptions handling enabled, making KDE1 bloated and slow to unusability.\n\nThe first problem was solved using new default KDE2 style. The second problem will plaque KDE2 installations all over the world, until the end of the days, or until packagers will learn to compile Qt and KDE2 correctly."
    author: "fura"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: ">All linux distributions and FreeBSD shiped binaries built with exceptions handling enabled\n\nouah! thought about that once when i compiled it myself, but i thought that suse and mandrake, who have feet in the projects, would know that...\n\nis it the case for kde 2 too ? (say cooker ?)"
    author: "Luc taesch"
  - subject: "Windows-style has more readable checkboes"
    date: 2000-10-13
    body: "There's a real advantage to the Windows widget style. My mom has no depth perception. That means that things that are supposed to look 3D look flat to her. Well, my dad had KDE up on his machine, Mom was using it, and she had trouble reading the checkboxes, and no wonder, too. The difference between a unchecked and a checked Motif checkbox is just some shading. For most people, this isn't a problem because the shading makes it look like something is popping in or out, but for Mom, the color difference is a subtle change. Checkboxes are one thing Windows widgets got right."
    author: "J. J. Ramsey"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "I agree that KDE has visability problems on two fronts:\n\n1)  Being seen outside the KDE project world\n2)  Having the perception of looking like windows\n\nSimply put, the KDE project reminds me of the old GNU Linux argument which Stallman has lost - Linux is \"Linux\" and \"GNU\" is nowhere, simply because Linux is visible and \"GNU\" is not - even with \"GN\"OME and the GPL.\nThe KDE project is invisible to most people outside of the community development level of things. Unfourtanately, PR makes the world go round and the fact that GNOME can come from nowhere and be not only level with,  but edging ahead of, KDE is testament to the immense need for mindshare.\nMany KDE advocates say that the technically best project will win out. This simply isn't true. Computing history is littered with the corpses of products which depended on technical excellence alone to capture market and mindshare. If technical excellence alone were enough, we'd all be using Macs, or NeXTStep, or whatever, not all these MS products.\nSo, KDE does need evangelism. It needs to manage \"corporate relations\" from a central location - some kind of KDE announce website perhaps - this is what KDE.com should be, but isn't. A place where all the lastest press releases (yes, press releases), news, screenshots and advocacy come together along with links to KDE software.\nAlso, if an organisation like HelixCode, except KDE based were formed so that corporates such as Sun et al have a visible entity to deal with, matters would be helped further.\n\nAs to the \"KDE looks like Windows\" line, I don't think it does. However, KDE does *not* look sufficiently different from windows for ignorant potential users to appreciate that it is different and offers a whole lot more. A huge part of GNOMES success is its eyecandy. Sun must have jumped at the chance of replacing the graphically challenged CDE with something that looks like GNOME. \nAnd then there's the old CORBA argument - again I follow the KDE line - CORBA is too heavy for simple pluggable GUI components, but KDEs success with KPArts and especially DCOP isn't being preached to the unconverted loudly enough. CORBA is, after all, an industry standard.\n\nAnyway, just my 2 pence worth."
    author: "Ezz"
  - subject: "Image Mirror?"
    date: 2000-10-12
    body: "The gnome.png image seems to be gone from the ftp. Anyone still have it in their cache or something?<p> \n\n<i>I have an IQ of 6,000, which is the same as 12,000 PE teachers</i> - Holly, Red Dwarf"
    author: "Holly"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "KDE has improved it's PR as of recently, but it still has a ways to go.\n<P>\nMaking the QT toolkit GPL has totally blown that entire argument out of the water (finally). Also, there is finally a good news site about the latest KDE happenings (http://dot.kde.org).\n<P>\nNow, here's some <B>improvements</b> I can think of.\n<P>\nFirst, really push the differences between GNOME and KDE. With KDE2, apps can easily talk amongst one another via DCOP. GNOME is still developing this and won't be ready at least 1/2 a year. KDE has a working Office suite (http://koffice.kde.org). GNOME is planning to port StarOffice which will take way too long, IMHO.\n<P>\nSecond, if you really want to push KDE, make apps for the end user. Koffice is a great example. Not too many techies get into things like this, but it is very functional for the end user. What about multimedia. This is one of the few areas where Micro$oft still reigns. With things like ARTS and the new media players being developed, it's very possible to surpass that other OS.\n<P>\nThird, prove how easy it is to develop for KDE vs GNOME. Although there will be C bigots who will never convert, the advantages of using C++ in development time and code reusibility are mind boggling. I've always thought this was the <B>major</B> difference between GNOME and KDE. C++ allows quicker development...<B>PERIOD!</B>\n<P>\nFourth, really, really, really promote the multi-language support. If I remember correctly, this is still in development for GNOME 2.0 (Pengo comes to mind). If so, we won't see it for another year or so. Hah. Doesn't 50+ languages exceed any other user environment???\n<P>\nIn conclusion, KDE has a great track record in its releases. They've been mostly on time and stable. Long live KDE!"
    author: "Scott L. Patterson"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "What you're saying is not entirely true.\n\nGnome is already quite mature.\nThe current release is 1.2\nYes, 1.0 is unstable, but 1.2 is a *lot* faster and more stable.\n\nAnd that DCOP thing... as far as I know it will only slow down apps because every KDE app initialize a DCOP socket or whatever it is.\nGnome programs use CORBA to communicate.\nMany people say CORBA is slow, but it isn't, at least the Gnome implementation (ORBit) isn't.\nIt is quite fast on my old Pentium 166 with 48 MB, and it's really faster than DCOP (when initializing).\n\nThere is already an office suite for Gnome.\nIt's also quite usuable.\nKOffice has more features than Gnome Office, but Gnome Office is more stable.\nThe only exception is Gnumeric, which has more features than KSpread and is more stable.\nSo I end up using KWord for word processing (AbiWord sucks) and Gnumeric for tables and such.\n\nYou're saying that it's easier to developer for KDE than Gnome.\nIt's only partially true.\nGnome uses the GTK+ toolkit, which is written in C yet *object oriented* like C++!\nSo Gnome also use code reusability.\nAnd it's *very* easy to develop in Gnome, even if it has a C API.\nAnd both Gnome and GTK+ have C++ bindings.\n\nC++ allows quicker development?\nWhen using Glib C development is just as fast :-)\n\nAnd Gnome already supports internatiolization and localization.\nPango is just a new internatiolization engine (right?).\n\nConclusion: KDE and Gnome both have their strengths and weakness.\nThe KDE panel for example is still not as powerful as Gnome's."
    author: "Anonymous"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "<br>> You're saying that it's easier to developer for KDE than Gnome.\n<br>> It's only partially true.\n<br>> Gnome uses the GTK+ toolkit, which is written in C yet *object oriented* like C++!</p>\n\nAt least for me it was much easier to develop with QT/KDE then with gtk--. A couple of month ago I had a look at both and found QT/KDE _much_ better documented. The documentation of the classes is for gtk-- in many cases not more worth than the header file--compare this with QT/KDE!\n\nciao, Marco"
    author: "Marco Krohn"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "GTK-- is an exception. I think GTK+ itself is better than GTK--"
    author: "Anonymous"
  - subject: "Documentation of QT/GTK was: Improving KDE Public"
    date: 2000-10-12
    body: "<p>> GTK-- is an exception. I think GTK+ itself is better than GTK--</p>\n\n<p>I just had a look at the gtk+ documentation and must say that it looks incomplete as well e.g. I found no documetation for GtkWidget. Nevertheless it looks much better then the gtk-- docs but there is still a big difference to the QT/KDE docs.</p>\n\n<p>If you want to see what I mean please have a look at:</p>\n\n<br><a href=\"http://developer.gnome.org/doc/API/gtk/\">The GTK docs</a>\n<br><a href=\"http://doc.trolltech.com\">The QT docs</a>\n\n<p>and compare any two classes e.g. GtkImage and QImage.</p>\n\n<p>Personally I recommend QT/KDE everybody who wants to use C++ since the gtk-- is very incomplete and it makes (IMHO) no sense to use C if you can use a wonderfull C++ toolkit. I know at least 3 other people who think the same and who are now coding with QT/KDE. Just my two cents...</p>\n\nciao, Marco"
    author: "Marco Krohn"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "Rebuttal time...\n\nThe majority of GNOME apps do NOT use CORBA. Perhaps by choice, but mostly because it's not mature. Look at how often the Bonobo stuff changes in Evolution! DCOP on the other hand, is well established and available now. Plugging one app into another (Konqueror) is very easy.\n\nThere is no Office suite for GNOME. It's just a mish-mash of unrelated programs. There is no interactivity amongst the programs like Koffice offers (put a spreadsheet into your document in GNOME...yeah right). If there is an office suite, why is GNOME supporting the StarOffice port. Isn't their existing suite full-featured enough. BTW, what's in this suite? Abiword, Gnumeric, ???. Where't the rest? KDE definitely has the advantage here!\n\nI strongly believe development in KDE is faster than in GNOME from my experiences in both toolkits. I suppose this varies depending on the programmer's background, so, we can't really debate this issue.\n\nI agree both desktops have strengths and weaknesses. By no means, do I want one to prevail as they provide motivation for one to out-do the other.\n\nPeace!!"
    author: "Scott L. Patterson"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "You're wrong. A lot of Gnome apps use CORBA.\nThe Gnome panel use CORBA for it's applets, the Control Center use CORBA, gmc use CORBA, gtm use it, Galeon use it...\n\nBonobo is still in development, but it is quite usuable now.\nDCOP is already available, because AFAIK it's started before Bonobo.\n\nThere is a Gnome office suite.\nIt just don't look like an integrated suite.\n(ok it depends on how you look at it)\nMost of the programs are not released yet.\nAnd there is no interactitivy, because Bonobo is not released yet, so they can't use embedded components like KDE does.\nThey support StarOffice, because they can use their code.\nStarOffice's code can benefit both Gnome and KDE and speed up development.\n\nGnome Office contains AbiWord, Gnumeric, Gimp, Achtung, maybe Evolution and Sodipodi, and probably some others as well.\n\nTime is the issiue here.\nKDE is older than Gnome.\nGnome is always one version numer behind KDE.\n\nPeace to C and the ++."
    author: "Anonymous"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-13
    body: "It really really bugs me that GNOME hijacked the gimp and said it's ours!  Gimp long before gnome and probably before KDE too.  If the gimp is part of Gnome it's also part of KDE and fvwm for that matter.\n\nThen there's gphoto! I'll admit that a couple of the developers for gphoto are well into gnome but it was never built as part of Gnome.\n\nOh yes, Gnumeric.  Never intended to be part of Gnome.\n\nOh and StarOffice, I wish they would stop stealing my applications and telling me they are gnome.\n\nAnd the one we've all been waiting for?????  Mozilla.  If Mozilla gets good then you can be sure it'll be a Gnome app.\n\nIt even stole Abiword and Enlightenment for a while. \n\nHas the Gnome project developed anything itself?\n\nRegards"
    author: "Mark"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-14
    body: "So what's your point?\nGood programmers create good code.\nBetter programmers *borrow* code.\nIsn't this what the GPL is all about?\n\nThe Gimp was not part of Gnome, but it is part of the GNU project.\nGnome is also part of the GNU project, and they have every right to make The Gimp part of Gnome.\n\nGnumeric *is* intended to be part of Gnome.\nOther may not; so what?\nTheir authors agreed to let their software be part of Gnome, what's wrong with that?\n\nWhy reinvent the wheel if it already exists?\nDoing that will only result to further disintegration in the software world, and wasting time."
    author: "Anonymous"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-14
    body: "So what's your point?\nGood programmers create good code.\nBetter programmers *borrow* code.\nIsn't this what the GPL is all about?\n\nThe Gimp was not part of Gnome, but it is part of the GNU project.\nGnome is also part of the GNU project, and they have every right to make The Gimp part of Gnome.\n\nGnumeric *is* intended to be part of Gnome.\nOther may not; so what?\nTheir authors agreed to let their software be part of Gnome, what's wrong with that?\n\nWhy reinvent the wheel if it already exists?\nDoing that will only result to further disintegration in the software world, and wasting time."
    author: "Anonymous"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-13
    body: "KDE is about 7 months older than GNOME. When GNOME started,\nthey had the big advantage that they could take lots of the ideas\nand infrastructure we developed from scratch for KDE ( code \norganization, desktop files, later the HTML engine).\n\nDCOP on the other hand is a lot younger than Bonobo. I would have \nto check, but I'd  say I wrote it at least one year after I heared Miguel \ntalking about Bonobo the very first time."
    author: "Matthias Ettrich"
  - subject: "Re: A Minor Rebut"
    date: 2000-10-12
    body: "Just a minor rebuttal, since my major ones have already been addressed...\n\n\"Gnome uses the GTK+ toolkit, which is written in C yet *object oriented* like C++!\"\n\nThe big problem here is that this OO-ness is not native to C. Users of GTK+ have to learn a completely new convention and style of writing C. This is not trivial. I'm sure it's easier to learn this new convention than learning C++, but unless every C project adheres to the same OO convention, you're going to have to learn half a dozen ways to do the same thing.\n\n\"Who needs a truck, when I can put a motor on my bicycle and go anywhere they can...\""
    author: "David Johnson"
  - subject: "Re: A Minor Rebut"
    date: 2000-10-12
    body: "The GTK+ object system is not so complicated.\nAt least using it isn't complicated.\n\nBut all OO in Gnome are using the GTK+ object system, so they are all compatible.\nAnd I haven't seen any OO program yet that is written in C and doesn't use the GTK+ object system."
    author: "Anonymous"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-12
    body: "I don't understand why PR is important to KDE. KDE has a large number of users and developers. KDE2 is fast, stable, beautiful, customizable, and functional (and easy as hell to write code for). There are those who disagree, but so what?\n\nIn my office, only the developers and systems guys use Linux. One of my co-workers and I use KDE, 3 people use GNOME (1 only because it's default in RedHat), 2 use FVWM2, 2 use TWM, one uses OLWM, and one uses a really old one that I don't remember the name of. Why should I convince them to all use KDE? The beauty of Linux/*NIX is choice. We can all use what we want. Let's just keep writing good software, for the sake of writing good software, and people will use it."
    author: "Aaron Traas"
  - subject: "Bashing Gnome"
    date: 2000-10-12
    body: "Why does KDE have to improve his RP record?\nKDE is accepted and prefered by more people.\nIf you regularly read Slashdot, you will notice that there are more flamewars about Gnome than KDE.\n\nI hope this is not an attempt to bash Gnome.\nOn the day the Gnome Foundation was announced the KDE site was full of FUD about Gnome, while I didn't find any FUD article at Gnotices.\n\nBefore you say something about the Gnome Foundation, I have to say something first.\nThose companies don't have any power at all!\nAll they can do is provide suggestions and developers.\nNo commercialization, no world domination, no you-have-to-pay-for-everything."
    author: "Anonymous"
  - subject: "Re: Bashing Gnome"
    date: 2000-10-13
    body: "\"Those companies\" provide acceptability and mindshare. Commercial acceptability and mindshare usually means the exclusion of any alternatives - look at MS Windows for an example of this.<br>\nThat's why KDE needs to be more visible. It's a mindshare war, not a flame war, a war for the hearts, minds and desktops of all the people who are now, and will in the future, migrate to Linux.<br>"
    author: "Ezz"
  - subject: "\"Legacy\" widget sets"
    date: 2000-10-13
    body: "Another dumb dig at GNOME/GTK+ is calling the importer for GTK+ themes the *Legacy* theme importer. Considering that GTK+ currently continues to be developed and is used rather widely, referring to it as \"legacy\" real makes no sense. It seems to me that the \"legacy\" bit is more of a dig at GTK+ than anything else."
    author: "J. J. Ramsey"
  - subject: "No naughty pics on KDE site!"
    date: 2000-10-13
    body: "If you check the KDE 2 screenshots page, you will notice that the wallpaper in many of the screenshots features a topless model.\n\nNow while this may not bother most geeks, it will completely freak a lot of corporate types. I know a lot of poeple in jobs where they could be fired, or even prosecuted for sexual harassment, for customizing their desktop in this manner.\n\nI also remember UK computer manufacturer Mesh taking serious flak from readers for using models in their magazine advertising.\n\nIts bad PR. Fix it."
    author: "Boss Rabbit"
  - subject: "Re: No naughty pics on KDE site!"
    date: 2000-10-13
    body: "Yes, we will take action and fire this guy.... but wait.... there is no company called \"kde\". This screenshots are made by individuals, users. Oh my god. Nobody will switch to a desktop whose users may like such pictures, so they might stay to the virgin windows desktop."
    author: "Lenny"
  - subject: "Re: No naughty pics on KDE site!"
    date: 2000-10-13
    body: "I understand you are trolling, and I won't comment that. However, to avoid any misunderstandings: the \"naughty\" screenshot may cause prude \"suits\" to turn their backs on KDE since it gives them the wrong impression. www.kde.org is the definite official source for KDE information, and if it doesn't look corporate, it lessens KDEs odds in the corporate market (which is LARGE)."
    author: "Haakon Nilsen"
  - subject: "Re: No naughty pics on KDE site!"
    date: 2000-10-14
    body: "I have to reply to this 'cause one of these \"naughty\" screenshots is mine. Screenshot with Kde2 (Konqueror in acton & Kaiman) and a\nbackground picture of beautiful Laetitia. This picture is as much art as any other \"fine art\"\npicture (should I start to name all great sculptors or painters who created a masterpieces of art using naked models of men and women?).\nAs ignorant as you are, you are not able to make distinction between art-photograph of a naked body and porno. I have never seen a pornographic screenshot on kde.themes.org site.\nSo, stop bullshiting. Kde is not for impotent people. For people who have not seen the screenshot I've attached it to this message.\nEnjoy."
    author: "Zeljko Vukman"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-14
    body: "I don't think the community needs PR structures.\nThe free SW movement should just get things done and forget the crappy \"Where do you wan to go today?\" stuff. KDE rocks because it looks cool, works fast and konqueror doesn't crash as often as netscape  on my celeron 300 machine (which is for today's standard close to scrap). I don't use linux because of Red Hat's female testimonials or Linus looking like Ricky Martin.\nThis stuff requires big money that only some big corporate kingdom can provide. If we accept it, we won't be able to complain when sometime in the future the CEOs of such corporations will ask us to do this that and the other the way THEY please... that day we'll just become \"linuxerfs\" and have pity of ourselves...\n\nJust keep coding and listen to the userbase (newsgroups, mail-lists, IRC channels, you name it) not the PR makeup!"
    author: "curious.corn"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-16
    body: "Please note my specialist area is Quality Management.\n\nMicrosoft has proved that technical merit has little bearing on final outcomes of success.\nIndeed PR is important \"combined\" with satisfying client needs.\nCost, Features, Reliability, Conformance, Serviceability and Aesthetics are subconsciously examined by end users and ultimately \"their\" priorities measured are used in a decision to use or not use a particular product or service.\n\nIf KDE, Gnome and other developers understand this then they are well on their way to success.\n\nAnd finally I read /. and am NOT a nay-sayer about KDE.  KDE is my DT of choice and its by far the best one out there.\n\nThanks to all who made it available to the world.\n\nSteve"
    author: "steve"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-16
    body: "it's quite easy. Put more tutorial on KDE applications, enviorments, KDE programing... Be humble and work more for the end users. That is it."
    author: "Ben"
  - subject: "Re: Improving KDE Public Relations"
    date: 2000-10-18
    body: "See my comments on packaging, PR and customer relationship management <A href=\"http://dot.kde.org/971680096/971796213/971813027/971828478/\">here.</A>"
    author: "Paul Leopardi"
---
There is a general consensus that the KDE project, despite its technical superiority among various desktop environments, has had a poor PR record, especially in North America. Now that the release has been delayed a week or so, let's take this opportunity on <a href="http://dot.kde.org/971269290/">dot.kde.org</a> to present and share ideas that will help the KDE PR and marketing efforts. Just to get us started, here's one idea which I mentioned to <a href="http://www.mosfet.org/">Mosfet</a>:





<!--break-->
<p>
I just looked at <a href="ftp://ftp.kde.org/pub/kde/Incoming/gnome.png">ftp://ftp.kde.org/pub/kde/Incoming/gnome.png</a>. (This is actually what reminded me of KDE's poor public relations and advertising record compared to the competition.) If KDE can be configured to
look virtually exactly like the competition, why not advertise that fact when KDE2 is released? Those companies or organizations who have invested in the look and feel of the competition can consider the technical superiority of KDE
without worrying about style issues. And the naysayers on /. etc. who talk about how the competition is prettier can be silenced before they even start. When the KDE2 release is officially announced, the folks at /. etc. should have <i>immediate</i> links to screenshots showing off style compatibility with the competition.
<p>
Look, now that KDE is totally (L)GPL compatible (ok, for some of us there was never a <i>real</i>
problem to begin with but let's not even go there), why not include a couple of themes using the competition's icons, etc. (minus the foot) in the standard KDE distribution? This will make it easier for users to get that look if they want and then they focus on the technical merits or demerits. If KDE can include non-standard applications in the distribution, then why not distribute some non-standard
styles as well?
<p>
If this has already been discussed somewhere, then I missed it. Any way, given all of the work that has gone into KDE2, not to mention the great configurability that has been developed, it should be advertised <i>LOUDLY</i> and decisively immediately upon release (links to screenshots, etc). The excuse that  "Well, uhh, I chose &lt;insert name of competition here&gt; because it looks cooler man" can and should be put to rest once and for all.
<p>
The above is meant to provoke ideas and general discussion about KDE PR. The release delay gives the PR team (who are they anyway?) an extra week to get things together. After all of the experience of the past, we have no excuse to not get it right this time. There may never be as crucial an advertising moment for KDE as with this KDE 2.0 release.
<p>
Thank goodness they dropped the KDE2 1.x idea, or we'd be in <i>REAAAL</i> trouble.
<p>
Samawi
















