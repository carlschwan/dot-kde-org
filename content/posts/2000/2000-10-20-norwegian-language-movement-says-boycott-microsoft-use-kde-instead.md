---
title: "Norwegian language movement says: \"Boycott Microsoft - use KDE instead\""
date:    2000-10-20
authors:
  - "gkvalnes"
slug:    norwegian-language-movement-says-boycott-microsoft-use-kde-instead
comments:
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-20
    body: "Er det ikke realativt uinteressant hva bygdefanatikere mener om datasystemer? Det nermeste de kommer en pc er vel uansett skurtreskeren...\n\nJoey"
    author: "Joey"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-20
    body: "Nobody asked you to care. The affected people care."
    author: "AC"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-31
    body: "No-one is affected by this, Neo-Norwegian is an artificial language spoken by no-one. Quite costly and hugely unpopular."
    author: "Anders Moe"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-11-01
    body: "Norway has two official written languages, Norwegian Bokm\u00e5l and Norwegian Nynorsk. Norwegian Bokm\u00e5l is the majority language, Norwegian Nynorsk the minority language. Norwegian Nynorsk is used by 15-16 per cent of the pupils in elementary and lower secondary school and by 10-15 per cent of the adult population. One fourth of approx. 200 newspapers are edited in Norwegian Nynorsk, most of them small papers. Their circulation amounts to about 6 per cent of the total newspaper circulation in Norway. About 10 per cent of the Norwegian newspapers, among them some large regional newspapers, are edited in both languages, e.g. some of the articles written by the jounrlists are in Norwegian Nynorsk, some in Norwegian Bokm\u00e5l. About 10 per cent of the books published in Norway are in Norwegian Nynorsk, this goes for fiction as well as non-fiction.\n\nBoth Norwegian Bokm\u00e5l and Norwegian Nynorsk are \"constructed languages\", as are all modern written languages. \n\nThe localisation of KDE into Norwegian Nynorsk is an important event for the Norwegian language movement and an important contribution to linguistic democracy and linguistic pluralism in Norway.\n\nYou will find more information about the Norwegian language situation here: http://www.sprakrad.no/engelsk.htm\n\nGaute Hvoslef Kvalnes has done a remarkable job!\n\nJon Grepstad\nthe Norwegian Language Council"
    author: "Jon Grepstad"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-11-01
    body: "\"Both Norwegian Bokm\u00e5l and Norwegian Nynorsk are \"constructed languages\", as are all modern written languages.\"\n\nThis is the stock response of the Neo-Norwegian movement when told their language is a construct, spoken by none, written by few, liked by fewer; it doesn't work so well here- I'm almost sure the word I used was \"artificial\", and there is indeed a real difference between politically and ideologically planned languages, such as Neo-Norwegian, and languages that are developed by their users. \n\nIt's also interesting to remember that in the heyday of Neo-Norwegian, one of the chief reasons Neo-Norwegian was great was its absolute naturalness- nothing cultured or aristocratic about it; \"Mother's own tongue!\" It wasn't. A barrage of spelling changes set about to make Neo-Norwegian understandable to \"mother\" by making it more similar to the Danish she knew, a process in which the Neo-Norwegian language lost all its special features without increasing legibility or diminishing the hatred between the groups who believe in the various Norwegian spellings."
    author: "Anders Moe"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-11-01
    body: "> Neo-Norwegian is an artificial language\n> spoken by no-one.\n\nOf course; all written languages are \"artificial\". Norwegian Nynorsk is a *written* language. When Norwegians speak, they use their dialects, which can be very different (or not) from the written Nynorsk and Bokm\u00e5l. Nynorsk and Bokm\u00e5l is only *spoken* in news broadcasts.\n\nThere's a very good article at http://library.thinkquest.org/18802/norwlang.htm about the Norwegian language:\n\n\u0093Dialects are used in school (by teachers and students), on TV, on the radio.  \n\nTo show how the dialects can vary, here are some ways to say \u0091I\u0092 in Norwegian dialects (the Norwegian spelling has been kept): \u0091Jeg\u0092 (standard Dano-Norwegian), \u0091Eg\u0092 (standard New-Norwegian), \u0091Je\u0092, \u0091I\u0092, \u0091\u00c6\u0092 and \u0091E\u0092. That\u0092s six different ways of just saying \u0091I\u0092!\u0094"
    author: "Karl Ove Hufthammer"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-20
    body: "<I>Er det ikke realativt uinteressant hva bygdefanatikere mener om datasystemer? Det <B><U>nermeste</U></B> de kommer en pc er vel uansett skurtreskeren...</I>\n<br><br>\nDe fleste b\u00f8nder kan stave riktig."
    author: "Tommy"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-21
    body: "Actually we should also concentrate on making KDE accessible to blind (language/sound/braille) or other handicapped people. It`s no longer just a matter of how kewl we can make it(although every addition is surely welcome), it is that already. Now let`s concentrate and focus upon making it available to all.\n\nMikael Helbo Kj\u00e6r (a dane not a norseman)"
    author: "Icebear"
  - subject: "What's the difference?"
    date: 2000-10-20
    body: "I've noticed the two Norwegian translations for KDE but didn't know what the difference was. I assume they're regional variants? How different are they in practice?"
    author: "Otter"
  - subject: "Re: What's the difference?"
    date: 2000-10-20
    body: "Norway has two official written \"languages\" (both being equally Norwegian). They're not very different in practice (both are plain easy to understand for any Norwegian), but Bokm\u00e5l is seen more often than Nynorsk (having little to do with regioning).\n\nGaute is the Nynorsk translator, and IMO he deserves a lot of credit for the effort :) It's a powerful display of how free software can fill needs that corporations and even governments aren't willing to take care of."
    author: "Haakon Nilsen"
  - subject: "Re: What's the difference?"
    date: 2000-10-20
    body: "<p>The difference is historical. For 400 years, until 1814, Norway was a part of Denmark. The only written language during this period was Danish. The old Norse language didn't survive (Icelandic, for example, has evolved much less, and remains quite close to the old &quot;viking&quot; language).</p>\n\n<p>When Norway no longer was Danish, the need for a Norwegian language arose. Two directions emerged - either &quot;Norwegianise&quot; the Danish language, or create a language from scratch, based on Norwegian dialects.</p>\n\n<p>Simply put, <em>Bokm&aring;l</em> (&quot;Book language&quot;) is evolved from Danish, while <em>Nynorsk</em> (&quot;New Norwegian&quot;) is based on spoken Norwegian dialects. Since 1870 or so, both languages has had equal official status as Norwegian languages. They are mutually understandable, and pupils learn both in school.</p>\n\n<p>Today, Bokm&aring;l is used by the majority of Norwegians. Nynorsk is mostly used in the western parts of Norway, but nation-wide television, radio, etc. uses both.</p>"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: What's the difference?"
    date: 2000-10-21
    body: "Ha! Sort of like the GNOME and KDE of Norwegian languages. :) I bet you have flamewars between the supporters of the two, just like we have too! Anyway, thanks for the interesting historical background."
    author: "Alfhiem"
  - subject: "Re: What's the difference?"
    date: 2000-10-21
    body: "<I>Ha! Sort of like the GNOME and KDE of Norwegian languages. :) <B>I bet you have flamewars between the supporters of the two, just like we have too!</B> Anyway, thanks for the interesting historical background</I><BR><BR>\nNever happend :-)"
    author: "Tommy"
  - subject: "Re: What's the difference?"
    date: 2000-10-23
    body: "<I>I bet you have flamewars between the supporters of the two, just like we have too! </I>\n\nYes.  :-)\n\nBut the norwegian vs farmer-language(nynorsk) is a bit more bitter than gnome vs kde, i think.  (Read: I don't want it to be compulsory to be able to write both \"nynorsk\" and \"bokm\u00e5l\" in norway, but it is, and that sucks)."
    author: "arcade"
  - subject: "Re: What's the difference?"
    date: 2000-10-23
    body: "No you suck"
    author: "Tommy"
  - subject: "Re: What's the difference?"
    date: 2000-10-21
    body: "<p>\n<b>1814</b><br>\nJan 14. Denmark signs the peace-treaty of Kiel, and gives Norway to Sweden. <br> \nNov 4. King Karl II (King Karl XIII of Sweden) and Karl Johan (Crown Prince of Sweden) is formally elected King and Crown Prince of Norway  \n<p>\n<b>1905</b>\nThe union with Sweden falls apart and Norway becomes an independent kingdom. The Danish prince Karl becomes king Haakon VII of Norway."
    author: "Anders Andersson"
  - subject: "Heh sounds like \"joual\" in Quebec"
    date: 2000-10-22
    body: "Some people (esp separatists at one point in the past) - think \"joual\" is more \"authentic\" than namby-pamby French.  But (dons flamesuit) really it's just poorly pronounced French with bad grammar and a lot of anglicisms thrown in :-) heh heh ... (BTW \"joual\" was the cool Quebecois way of pronouncing \"cheval\" - har har)\n\nThankfully joual appears to be on the decline and a sort of \"mid atlantic\" standard sort of French is accepted as every bit as \"authentic\" in Quebec (there's even immigrants who speak French now). As an *Anglo* who speaks French the number of times I got asked whether I was from Europe (because of my less slangy sounding accent) used to irritate me - now it makes me laugh."
    author: "couard anonyme"
  - subject: "Re: What's the difference?"
    date: 2000-10-31
    body: "No actual \"need\" arose, it's purely national romanticism; Danish being even closer to \"bokmaal\" than Neo-Norwegian is, and all three readily understandable to everyone.\nBut Danes don't buy Norwegian books, and that's why the Norwegian language can only survive on government subsidies. It won't be missed. We've had a painful hundred years of pointless messing about with the spelling."
    author: "Anders Moe"
  - subject: "Re: What's the difference? (not very much)"
    date: 2000-10-21
    body: "The difference isn't much bigger than the difference between American English and British English. And that is probably the reason why there is a great oppostion in Norwegian schools against learning Nynorsk. The reason why it is still around is that some believe that it is important for norwegian cultural inheritance.\n\nThe nynorsk people are probably going to flame me for this."
    author: "Erik"
  - subject: "Re: What's the difference? (not very much)"
    date: 2000-10-22
    body: "<b>Nynorsken</b> er eit vidunderlig spr\u00e5k.\n<br>\nLengje leve Ivar Aasen\n<br><br>\n<b>New norwegian</b> is A wonderfull language\n<br>\nLong live Ivar Aasen"
    author: "Tommy"
  - subject: "Re: What's the difference? (not very much)"
    date: 2000-10-23
    body: "True, the difference is not great.\n\nAs a Swede I can inform our non-Scandianvian readers that the high form of Swedish and Norwegian may be more similar to each other than some spoken dialects in each country.\n\nIn fact, some dialect words found in southern Sweden, can also be found in western Norway, yet lost in both the Stockholm and Oslo areas.\n\nThe Danish influence in Norway and on _written_ Norwegian is probably the main reason why Swedish and Norwegian differ at all today. Had Sweden historically dominated Norway the same way as Denmark (or the other way around, of course), Norwegian would most probable have been regarded as one of many dialects.\n\n[This could have been true, for example through the historically only legal heir to Sweden (incl current Finland), Norway, and Denmark (incl Iceland, Greenland, Faroe Islands, and Shetland Islands). That was the prince born in the 1400-hundreds by the crown prince of and crown princess. As his great(?) paternal grandfather also was the king of Sweden made him legally eligible for the Swedish crown as well. Unfortunately his father died and his mother became Queen Margareta of Norway of Denmark. Then he himself died at the age of 14, I believe, in Sk\u00e5ne (then part of Denmark). Subsequently, Margaretha gained power in Sweden, but never on legal grounds, and she was formally never recognized as Queen of Sweden.\n\nTHAT would have been the superpower of Europe."
    author: "weiuds kfj"
  - subject: "Re: What's the difference? (not very much)"
    date: 2000-10-23
    body: "<p><i>Norwegian would most probable have been regarded as one of many dialects.</i></p>\n\n<p><i>[This could have been true, for example through the historically only legal heir to Sweden (incl current Finland) ...</i></p>\n\n<p>Sorry to be a little bit pedantic, but while the poulations of Denmark, Norway, Sweden and Iceland come from similar Norse roots, Finns don't. I know this wasn't the meaning of your comment - but like I say I'm being pedantic!</p>\n\n<p>A large minority of Finnish citizens come from Swedish origins, thanks to Swedish overlordship, but true Finns are from different racial stock. This is reflected in the Finnish language, which is unlike any of the Norse languages. In fact the only languages closely related to Finnish are Estonian and various Karelian dialects. In the dim and distant past (around the time of the migration westwards of the Ural basin tribes), Finns had a similar dialect to Hungarians, but the similarity faded into obscurity centuries ago.</p>\n\n<p>Other dialects related to Finnish include Ingrian(sp?) but I understand the last true Ingrian speakers disappeared earlier this century. Most had already disappeared during the construction of St. Petersburg on Ingrian land many years before, or migrated to the Karelian Isthmus where they were absorbed into the Finnish population.</p>"
    author: "Chris"
  - subject: "Re: What's the difference? (not very much)"
    date: 2000-10-23
    body: "<I>In fact the only languages closely related to Finnish are Estonian and various Karelian dialects.</I>\n\nIf you check more carefully you will find a dozen languages (each with several dialects) more close related to Finnish and Estonian than either of these to Hungarian. Included in this branch you will also find the Sami language. These languages are, however, subdued by Russian along the northern interior of Russia and Siberia.\n\nWhat makes Finnish and Estonian particular, is that these two areas became the neutral zone between Russia and the Western powers and therefore received a country of their own. Ethnic identity for the fenno-ugrians is a lot tougher to the East.\n\nOn the topic, I wonder how a Kurdish KDE will flourish in Turkey? Or, a Basque in Spain, or a Catalan in France, or Friesean in the Netherlands, etc.\n\nThe rebellian power of KDE may be too strong for some governments to tolerate?! Just a thought."
    author: "weipruhfuds"
  - subject: "Re: What's the difference? (not very much)"
    date: 2003-02-26
    body: "You wrote:\n\n\"On the topic, I wonder how a Kurdish KDE will flourish in Turkey? Or, a Basque\nin Spain, or a Catalan in France, or Friesean in the Netherlands, etc.\"\n\nI doubt the Dutch government would have a problem with a Friesian language version, as most other things are in Friesian too, including radio, TV, newspapers and even www.google.com\n\n\n"
    author: "Alex Koster"
  - subject: "Re: What's the difference? (not very much)"
    date: 2000-10-23
    body: "There is no one-to-one relationship with language and race in Finland. Finnish spaking finn has the very same genes as swedish speaking finn. \n\nFinland was part of Sweden up to 1809, so official language was swedish and swedish was spoken in \"big\" towns like Turku and Helsinki where goverment had it's officials. Therefor people living in those towns had to speak Swedish to communicate. Swedish speaking upperclass was still mostly finnish, they just had to adapt speaking swedish."
    author: "jannek"
  - subject: "Re: What's the difference? (not very much)"
    date: 2000-10-23
    body: "\"Finnish spaking finn has the very same genes as swedish speaking finn.\"\n\nand\n\n\"Swedish speaking upperclass was still mostly finnish, they just had to adapt speaking swedish.\"\n\nOoops... Those were a bold, but unfortunately untrue statements. Svante P\u00e4\u00e4bo (Estonian guy, raised in Sweden, and professor in Germany) claimed the opposite... That the Finnish/Estonian \"stock\" had become so Indo-european by intermarriage with surrounding Germanic and Slavic groups so there are very few genetic markers left to trace a person as from Finland/Estonia. The language had remained intact but the genetics not.\n\nNonetheless, the Swedish dictionary used by Microsoft doesn't come from Sweden, but from Finland! Apparently, Finns know Swedish grammar and computer dictionaries better than Swedes."
    author: "weipruhfuds"
  - subject: "Re: MS spelling checker"
    date: 2000-10-23
    body: "<p><i>Nonetheless, the Swedish dictionary used by Microsoft doesn't come from Sweden, but from Finland! Apparently, Finns know Swedish grammar and computer dictionaries better than Swedes.</i></p>\n\n<p>This is also the case with the Norwegian dictionaries. These new dictionaries (MS Office 2000) have caused many laughs in Norwegian offices. The spelling checker tries to be smart and accept compound words that are not in the dictionary. The result can be seen at <a href=\"http://www.pcworld.no/pcworld/harry/\">Harry Hurt</a>'s page in the Norwegian magazine PC World. Those who know Norwegian can just browse the archive - there are lots of Word 2000 entries!</p>\n\n<p>Anyway, the new spell checker might be better, since it accepts more compound words than before. The risk of writing something wrong that gets accepted anyway is quite low.</p>\n\n<p>Note for non-Scandinavians: The Scandinavian languages (and others, I'm sure) compose words differently from English. In English, you can have <em>web site</em>, which becomes <em>nettstad</em> in Nynorsk. Needless to say, the amount of compound words is quite large - no spell checker can include them all.</p>"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: MS spelling checker"
    date: 2000-10-24
    body: "The same with compound-words is in german.\nanyway: for example there is also a little difference between Germany-german and Austrian-German (I'm from Austria, btw.). so, why don't make a special Austrian KDE?"
    author: "christian"
  - subject: "Re: MS spelling checker"
    date: 2001-03-23
    body: "Can you get the MS Swedish Dictionary from US resalers? If so, where from?\nThanks, tack"
    author: "dave"
  - subject: "Re: What's the difference? (not very much)"
    date: 2000-10-23
    body: "I'm not sure I understand your reasoning. Why do you think this prince, had he lived, would have been more succesfull at keeping the union together than his mother? Please bear in mind that such things as legality and fairness didn't always play the decisive role in medival powerpolitics. Also Margareta was the Scandinavian Elizabeth.\n\nBirger"
    author: "Birger"
  - subject: "Re: What's the difference? (not very much)"
    date: 2000-10-23
    body: "Simply because Swedes never accepted Margareta as their queen. To become the king or queen of Sweden at that time you had to have Swedish ancestry, which she didn't.\n\nThe legal alternative at the time was Albrecht von Mecklenburg who had already been thrown out, and the mother of the dead heir seemed better option at that time, despite not being done the legally correct way; a minor detail which may have helped at the time was that she was half-way to Stockholm with her army to re-inforce negotiations ;)\n\nMy overall point, however, is that Norwegian, Swedish, and Danish are so similar that by any language's standard they would be dialects and not separate languages."
    author: "weipruhfuds"
  - subject: "Re: What's the difference? (not very much)"
    date: 2005-09-13
    body: "I like your overall point, it is a pity how many stupid and useless nationalism is alive today"
    author: "muzaraque"
  - subject: "Re: What's the difference? (not very much)"
    date: 2000-10-23
    body: "Sorry,\n\nThe text\n\n\"That was the prince born in the 1400-hundreds by the crown prince of and crown princess.\"\n\nshould have been\n\n\"That was the prince born in the 1400-hundreds by the crown prince of Norway and crown princess of Denmark.\""
    author: "weipruhfuds"
  - subject: "Re: What's the difference? (not very much)"
    date: 2006-09-12
    body: "are u talking about greenland and iceland"
    author: "suzzi"
  - subject: "Re: Norwegian language movement says: \"Boycott Mic"
    date: 2000-10-20
    body: "Great article - very eye opening.  Thank you for translating it.  I hope KDE/Linux's support for languages gets better and better."
    author: "KDE User"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-20
    body: "Let all to join in Microsoft Boycott!!!!!!\n:)))))))))\nBulgarian open source community supports the suggestion!"
    author: "Alexander Marinov"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-21
    body: "Ok, first step: dont use hotmail."
    author: "Lenny"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-21
    body: "right in his face :)"
    author: "AC"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-20
    body: "Small correction.\n\nThe Icelandic government did not pay for the translation. It entered into an agreement though that it would actively eliminate the use of pirated copies of Microsoft software in government institutions and encorage the use of them in educational institutions. Only Win98 was covered by the contract.\n\nThe funny thing though is that they managed to screw up networking in Win98 while translating it (how that is possible god only knows) so nobody uses it on LANs in schools. Its just not possible :)"
    author: "Hrafnkell"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-20
    body: "<p>Thanks for the correction. From what I've heard, the Icelandic deal with Microsoft was so strict that the Norwegian government wanted to avoid the same situation. I just assumed Iceland had to pay&nbsp;;-)</p>"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-20
    body: "Have always hated nynorsk as a written language, but this almost makes up for it!\n\nI really believe they have a good case here.  :)"
    author: "Christian A Str\u00f8mmen"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-23
    body: "The rest of the country hate people from Oslo, they have a dialect from hell."
    author: "Knut Hamsun"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-20
    body: "Yeah, let the world boycott Micr$oft :)\n\n(actually I prefer Gnome but I would never wind this fight here 8)"
    author: "Anonymous"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-21
    body: "Let's boycott Gnome!"
    author: "Anonymous coward"
  - subject: "stupid posts"
    date: 2000-10-21
    body: "hmmm....\nHas anybody asked you for advice ?\nI don't think so..."
    author: "Somebody"
  - subject: "Re: stupid posts"
    date: 2000-10-21
    body: "Loosen up would ya! It\u0092s called humour, and its good to have a laugh once in a while."
    author: "Just Some Guy"
  - subject: "What about a latin translation ?"
    date: 2000-10-21
    body: "I dont know latin myself, but i think that would be very cool. No commercial vendor could ever afford that.<p>\n\nDont know though, if there are untranslatable words (internet, file manager,...)."
    author: "Lenny"
  - subject: "Re: What about a latin translation ?"
    date: 2000-10-21
    body: "That would be cool! Any priests around?\n\n--\nAndreas Joseph Krogh <andreak@nettverk.no>"
    author: "Andreas Joseph Krogh"
  - subject: "Re: What about a latin translation ?"
    date: 2000-10-21
    body: "I think a franconian translation is more important :-)"
    author: "Michael H\u00e4ckel"
  - subject: "Re: What about a latin translation ?"
    date: 2000-10-21
    body: "I'm supporting this very interesting option. We could avoid this pretty complicated German which every Franconian pupil has to learn. *g*"
    author: "Christian Selig"
  - subject: "Re: What about a latin translation ?"
    date: 2000-10-22
    body: "Excellent idea. This would force the clergy-men in Rome to use latin from the beginning instead of italian. l"
    author: "Mats LG Rosen"
  - subject: "Re: What about a latin translation ?"
    date: 2000-10-22
    body: "> Dont know though, if there are untranslatable \n> words (internet, file manager,...)\n\nFile Manager should be translatable.  The Romans must have had papyrus files, and would be required to manage them.  I guess you would take the Italian word for Internet."
    author: "Jonathan Bryce"
  - subject: "Re: What about a latin translation ?"
    date: 2000-10-23
    body: "The \"Italian word for Internet\" is...\n...Internet!\n\nI'm one of the KDE Italian translators. I can tell you that Italian computer language is very similar to English - lots of words go untranslated.\nOnly IBM OS/2 and - to a lesser extent - Apple tried to translate technical jargon into Italian.\nIn fact, using Italian OS/2 is quite funny....\n\nOn the other hand Latin translates much more words than Italian. They even have words for \"television\", \"telephone\" and so on...\n\nSpeaking about \"strange\" languages: you can try KDE in Esperanto. The Esperanto team is one of the best translator teams."
    author: "Federico Cozzi"
  - subject: "Re: What about a latin translation ?"
    date: 2003-07-04
    body: "First of all, Download QuickLAtin...it helps\n\nSecond of all, \"Internet\" IS ALREADY a latin word...it means \"Web connection\". an \"Interstate\" highway connects states, and an \"Internet\" connects servers, or nets (networks). That's all the internet is...a string of different servers, and the \"Web\" or \"net\" or \"internet\" is literally the wires connecting those networks. "
    author: "tom Duff"
  - subject: "Re: What about a latin translation ?"
    date: 2000-10-23
    body: "Easy!\n\nInternettus, filus managrus etc :)"
    author: "Per Wigren"
  - subject: "Re: What about a latin translation ?"
    date: 2000-10-23
    body: "Fuckus internettus"
    author: "Knut Hamsun"
  - subject: "Re: What about a latin translation ?"
    date: 2004-04-20
    body: "i need a latin 2 english translator"
    author: "joey"
  - subject: "Re: What about a latin translation ?"
    date: 2002-09-14
    body: "I need help with Latin translations, too. Any priest willing to help would be sincerely appreciated."
    author: "I.R. Ravenwood"
  - subject: "Re: What about a latin translation ?"
    date: 2003-01-28
    body: "Y'know, there is this guy in the vatican who is specifically paid to come up with new latin words, so the Pope can still say his sermons in Latin. Afterall, we wouldn't want the pope to not be able to tell us that we shouldn't drive drunk with guns, now do we?\n\n      Caity"
    author: "Caity"
  - subject: "In 1920s Brazil and Portugal worked it out ..."
    date: 2000-10-22
    body: "along with reps from Portuguese speaking african colonies (one of whom was African - i.e. not a colonist).\n\nThey sat down went over the dictionary and standarized spelling (and some grammar) ... since then Radio and TV have have made the pronunciation mutually more comprehensible and a sort of \"international Portuguese\" evolved (same thing for Europeaan, North American and Caribean Spanish and French - for some reason U.S. Americans have trouble understanding other forms of English let alone other languages, but for other linguistic groups TV and Radio have helped to *stop* language divergence).  \n\nGiven other historical developments the language tension in Norway seems monumentally silly.  Nynorsk is more of a cultural purity thing than anything else (and ironically seems more like a created language than Bokmal does now), and Bokmal works *everywhere* in Norway. When I lived there it took me several months to even notice the difference (flame away).  I wasn't interested in learning the subtleties of two dialects (especially since in Oslo I could practically get by in English anyway).\n\nHebrew had to be *reinvented* from scratch 50 years ago - it too was mostly a \"fake\" or \"book\" language at the time but with Jews from all over the world coming to Palestine the choice was Yiddish or Hebrew - even though probably the majority of the European origin population spoke Yiddish the locals spoke Hebrew and it was the most effective and strangely a \"new\" language for a new country (less German sounding maybe). It was hard work but they *decided* and got on with it. Norwegians should be glad more of the world *doesn't* know about their silly little language battle."
    author: "AC"
  - subject: "Re: In 1920s Brazil and Portugal worked it out ..."
    date: 2000-10-22
    body: "I blame Denamark for extermenating our \"real\" language.<br>Bomb Denamark!.\n<br><br>\nThey have ugly royals, so do we."
    author: "Tommy"
  - subject: "Re: In 1920s Brazil and Portugal worked it out ..."
    date: 2000-10-31
    body: "That never actually happened, although it's commonly taught in Norwegian schools. (And Danish schools; national romanticism works both ways.)\nWe don't really know how the Danish written language came to be fixed, but national borders were different then. The king's writers could have been Swedish, Norwegian or Danish by today's strict standards. \n\nMost of this period, Danish wasn't much used at all; the army was commanded in German, the church and all learned men used Latin; in the 18th century, Copenhagen had a French-language newspaper; Danish didn't really come into its own as a written language before Ludwig Holberg (born in Bergen, though he could have cared less about his Norwegianness- national romanticism wasn't invented yet) wrote some plays and essays in the language. However, in one play, he lapses into Dutch for comical effect, and although he had a royal privilege for Danish plays, he lost the competition against a German troupe.\nPeople just didn't feel very strongly about which language they belonged to in those days-."
    author: "Anders Moe"
  - subject: "History of Modern Hebrew"
    date: 2000-10-23
    body: "Well modern Hebrew is more than 50 years old, more like 100 or so, it was recreated in the early years of the Zionist movement as the language of Israel. It should be noted that while many of the Jews coming from Northern Europe spoke Yiddish, the Jews from around the Medeteranian spoke Laddino (Jewish Spanish) and those from other places spoke Arabic and Farsi etc. \n\nHebrew is the only example of a dead language being brought back to life."
    author: "Zachary Kessin"
  - subject: "Re: History of Modern Hebrew"
    date: 2002-12-16
    body: "Hi Zachary,\n\nI wouldn't say hebrew was brought back to life, simply because\nit never died! It was the language spoken by hundreds of \nthousands of jews in their religious services, or when\nreciting their daily prayers to G-d.\nObviously there was the need to create thousands of new words\nto address the new uses of the language."
    author: "Carlos"
  - subject: "Re: History of Modern Hebrew"
    date: 2004-01-13
    body: "I think that what Zachary intended to say was that Hebrew was not used as a commonly spoken language in times past and hence, it was \"brought back to life\" as a daily language in the past century.  Indeed it was used in religious services/daily prayers throughout the millenium.  Nevertheless, it was Aramaic that was used as the commonly spoken language as a lingua franca in the Near East and then Arabic.\nI find it particularly amazing that even though \"thousands of new words [were created] to address the new uses of the language,\" Hebrew still maintains its Semitic qualities, such as the trilateral root. "
    author: "EunJoo"
  - subject: "Re: History of Modern Hebrew"
    date: 2004-04-21
    body: "Ok. But its not just a matter of creating new words. I still don't understand how, even all these people ( the citizens of Israel) came to adopt a language new to them as their mother language, for all everyday uses, and started thinking in it, speaking to their children etc, if they originally spoke other languages- which is quite ddifferent from using a language only for certain puposes, such as ritual. It's a historical gap I have here ( and many friends as well). I'd be grateful for any comment or indication.\nKatie"
    author: "Katie"
  - subject: "Re: History of Modern Hebrew"
    date: 2005-02-12
    body: "That's interesting, Katie.  I had a Jewish father, born in the UK but parents from Warsaw, and an English mother but fortunately they stayed together and I had the benefit of both cultures and was, and am still, in touch with both sides of the family.  My cousin David Reed emigrated to Israel with his parents when a small boy and I was amazed how quickly he became fluent in Ivrit.  I still remember his calling our pet budgerigar 'yelled tov' (pretty boy!) on a visit back to England.  He married an Israeli whose family escaped from Breslau (Wroclav) but is now in Australia where my brother also lives - he is a Professor at the University of New England in NSW.  Incidentally, I have always thought the rule that a Jew inherits via his mother an absolute rule of convenience for primitive ancestral conditions.  It is common sense that it does not matter from which side Jewish ancestry is derived - the effect is the same on the person!"
    author: "Phillip Sorensen"
  - subject: "Re: In 1920s Brazil and Portugal worked it out ..."
    date: 2000-11-01
    body: "I would be glad if Norway could have fixed this issues in the 1920s too. I had to learn Bokm\u00e5l in school and found out as an adult how bad it really is compared with Nynorsk. So than I decided to change in an age of 21. You seams to mean that bokm\u00e5l is good enough (language of choice). And I wonder why should I choose a language which is worse? Nynorsk has the norweegian logic, Bokm\u00e5l the danish. (Nothing bad about the danes.) Why should I write in another language than the natural. \n\nSince the portugeese, spanish and many more dosn't have this option it might be understandable that you call it a silly language battle. And as far as I know did you spend the most time in Oslo where the wast majority use bokm\u00e5l. Not a good referance if you ask me. Anyway I think you have got something wrong when you brush aside Nynorsk as just a cultural purity thing. Also that you may use bokm\u00e5l everywhere dosn't mean anything else than that we do understand bokm\u00e5l as well as all the many hundreds of dialects used in Norway daily. Bokm\u00e5l and Nynorsk are for me our written languages, and of them are nynorsk the closest to the majority of the people. The thing that most people learn bokm\u00e5l in school must only be understood as a result of bad luck.\n\nYes we want more computer programs in Nynorsk and less silly jokes about our language battle. It's anyway a sharming battle run by pencils and keyboards, so if you need some tip's about silly battles, look around... :-) HKNy"
    author: "HKNy"
  - subject: "Japanese Localisation. (Maybe off topic)"
    date: 2000-10-22
    body: "I'm having trouble getting Japanese localisation working in KDE 1.1.2.\nI can read Japanese web pages in Netscape and I can read Japanese text in KTerm. But I can get the KDE to display Japanese Text all I get is garbage. Can anyone help me?\nJames"
    author: "James Clarke (Remove NO SP AM from Email)"
  - subject: "Re: Japanese Localisation. (Maybe off topic)"
    date: 2004-08-09
    body: "Did you solve this problem. Please let me know your solution. Im having the same problem. :)"
    author: "Jeramie Maratas"
  - subject: "Re: Japanese Localisation. (Maybe off topic)"
    date: 2005-11-08
    body: "SOLVED!!! (or at least for me)\n\nI had this very same problem with Kde 3.3 on slackware 10.1 while atempting to set language locale to japanese, or any other non-roman character language, the only thing I got on screen was blank squares. I downloaded the last kde-i18n-ja available by the time from kde.org (kde-i18n-ja-3.4.2.tar.bz2), once I installed the package and reboot the system everything was ok."
    author: "Freddy Diaz"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-25
    body: "I agree that one of the more interesting things of free software is the possibilities that offers to traslate it to any language.\n\nKde is a very good example of this. But it can be better.\n\nI am compiling the kde-i18n package. A huge one with a lot of languages, but I don't need all of them. In fact, I suppouse that majority of machines don't need more than two or three languages. \n\nIt would be a nice thing to be able to install only a subset of the languages, in an easy way.\n\nI am using Redhat 7.0, and I have not found this possibility.\n\nAny way thanks for your work.\n Ramon"
    author: "Ramon Flores"
  - subject: "Re: Norwegian language movement says: \"Boycott Microsoft - use K"
    date: 2000-10-25
    body: "<p>Where did you get the source? At <a href=\"http://ftp.sunet.se/pub/kde/stable/2.0/distribution/tar/generic/src/\">http://ftp.sunet.se/pub/kde/stable/2.0/distribution/tar/generic/src/</a> (the main KDE ftp is busy, this is a Swedish mirror) there are separate packages for all languages.</p>\n\n<p>I use SuSE, with nice RPMs available for each language. KDE doesn't control how the distributions organise their packages, though.</p>"
    author: "Gaute Hvoslef Kvalnes"
---
The organization Norsk Målungdom, which works with promoting the Norwegian (Nynorsk) language, asks Norwegian schools to boycott Microsoft products and use KDE instead. MS Windows and Office only exist in the Norwegian (Bokmål) language, but Norwegian law states that pupils have the right to books and equipment in their own language.
<!--break-->
<p>So far, Microsoft has claimed that a translation to Nynorsk will be too expensive. Virtually all schools in Norway use MS software, and that means all applications are in Bokm&aring;l.</p>

<p>Since KDE now provides both Bokm&aring;l and Nynorsk, <a href="http://www.nynorsk.no">Norsk Målungdom</a> wants schools to use KDE instead of MS Windows. This can put pressure on Microsoft to create a Nynorsk version, but more importantly, it can spread Linux and KDE to more users.</p>

<p>Iceland (250 000 people) has got Windows translated, but the Icelandic government had to pay a lot for it. Norwegian governments are so far not willing to subsidise Nynorsk Windows translations the same way. There are twice as many Nynorsk users than Icelanders.</p>

<p>&quot;Small&quot; languages really have their chance with free software. Anybody can translate it, and earning money is unimportant. Even non-technical people can understand the benefits of open-source software when they see the results of translation - a result commercial companies never can achieve. One can't complain about the price, either&nbsp;;-)</p>

<p><a href="http://www.dagsavisen.no/utdanning/2000/10/518757.shtml">This article</a> (Norwegian) appears today in the Norwegian paper Dagsavisen.</p>





