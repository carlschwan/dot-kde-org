---
title: "People of KDE: Luigi Genoni"
date:    2000-12-16
authors:
  - "Inorog"
slug:    people-kde-luigi-genoni
comments:
  - subject: "Re: People of KDE: Luigi Genoni"
    date: 2000-12-16
    body: "This is what I think missing in KDE: \"Writing Administration Stuff for Administrators\". You know, we have Kandalf only for showing tips, and not for helping in other stuff like \"NFS Server/Client setup\", \"Samba Client/Server setup\", \"System Administration\" etc. \n\nIf you have used Corel Linux, you will appreciate the samba server wizard. There must be KDE based full system and network administration tools/wizards and applications available, so that the user/administrator have ease of use and which  will make KDE THE best Choice for all...\n\nYours truly,\nRizwaan."
    author: "Asif Ali Rizwaan"
  - subject: "Re: People of KDE: Luigi Genoni"
    date: 2000-12-16
    body: "<P><I>You know, we have Kandalf only for showing tips, and not for helping in other stuff like \"NFS Server/Client setup\", \"Samba Client/Server setup\", \"System Administration\" etc.</I></P>\n<P>I agree on this. Some useful tools that I would like to see are a KDE based Apache configurator, and a similar thing for Squid.</P>"
    author: "jliechty"
  - subject: "Re: People of KDE: Luigi Genoni"
    date: 2000-12-17
    body: "Kand-ALF shows tips? Since when? I can't remember that Kand-ALF has shown me any tips yet.\n\n*grin* *grin*\n\nTackat\n\n.. testing with this posting if one can post here again using konqueror ...."
    author: "Torsten Rahn"
  - subject: "Re: People of KDE: Luigi Genoni"
    date: 2000-12-17
    body: "We've all seen how well GNOME and KDE can work together in the previous article:)  HelixCode has already spent a significant amount of time setting up the Helix Setup Tools, Which do all of the things you mentioned and more.  \n\nThe GUI is completely seperated from the backend.  The backend basically calls Perl scripts to perform the actual configuration changes.  It would be a trivial port to QT:)\n\n - James"
    author: "James Morton"
  - subject: "Kde 2.1 beta is out!!!"
    date: 2000-12-17
    body: "2.1 is out, it's on the ftp."
    author: "Kaufman"
  - subject: "Favourite quote/saying"
    date: 2000-12-18
    body: "<p><i>\"Unix is the most user friendly system I know, the point is the it is really selective about who is indeed its friend.\"</i></p>\n<p>That is for sure a beautiful piece of <a href=\"http://www.everything2.com/index.pl?node_id=496022\">Scenglish</a>.</p>"
    author: "Anonymous Demoscener"
---
<a href="http://www.kde.org/people/luigi.html">Luigi Genoni</a> is this week's guest in the <a href="http://www.kde.org/people/people.html">People</a> section in the KDE website. With this new interview <a href="mailto:tink@kde.org">Tink</a> offers us a refreshing postcard from the Italian Alps. Luigi is interested in developing for KDE from the point of view of a system administrator. And he also gives us a hint about the joy of celebrating Christmas in long lasting Italy.

<!--break-->
