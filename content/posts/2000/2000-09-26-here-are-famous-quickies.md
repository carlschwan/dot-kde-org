---
title: "Here are the famous quickies!"
date:    2000-09-26
authors:
  - "raphinou"
slug:    here-are-famous-quickies
---
<a href="http://www.linuxnews.com/">LinuxNews.com</a> is running a <a href="http://www.linuxnews.com/stories.php?story=288">story</a> about <a href="http://www.thekompany.com/projects/kugar/">Kugar</a>, a reporting app we <a href="http://kdenews.org/969571500/">mentioned</a> earlier. There is also an <a href="http://alllinuxdevices.com/news_story.php3?ltsn=2000-09-25-001-03-NW-HH-WL">article</a> on <a href="http://alllinuxdevices.com/">All Linux Devices</a> about Qt/Embedded mentioning  that  some KDE apps are being ported to embedded devices.  We also received mention of a very interesting FTP client based on KDE 2: <a href="http://home.bip.net/bjorn.sahlstrom/kbear/index.html">KBear</a> reached its <a href="http://home.bip.net/bjorn.sahlstrom/kbear/pub/kbear-beta3.tar.gz">third beta</a>.