---
title: "The KDE Games Center Gets Busy"
date:    2000-09-28
authors:
  - "Dre"
slug:    kde-games-center-gets-busy
comments:
  - subject: "Re: The KDE Games Center Gets Busy"
    date: 2000-09-29
    body: "very good"
    author: "Manuel Rom\u00e1n"
---
Last week we <A HREF="http://dot.kde.org/969483421/">reported</A> on the launch of the KDE Games Center.  The site designers continue to build the site and it looks to want to satisfy both gamers and developers.  Yesterday they added a <A HREF="http://www.kde.org/kdegames/index.html">news section</A> and today launched a <A HREF="http://www.kde.org/kdegames/top10.html">"Top 10 KDE Games"</A> section.  Bravo, <A HREF="mailto:martin@heni-online.de">Martin</A> and <A HREF="mailto:b_mann@gmx.de">Andreas</A>!  If you're interested in games (and who isn't), maybe you can help out -- a nice "Help Wanted" list can be found <A HREF="http://www.kde.org/kdegames/index.html">here</A>.

<!--break-->
