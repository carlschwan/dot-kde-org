---
title: "KDE.org:  A New Site for a New KDE"
date:    2000-11-01
authors:
  - "Dre"
slug:    kdeorg-new-site-new-kde
comments:
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-10-31
    body: "Hum, Shouldn't it be 'Konquer your desktop' intead of conquer??\n<p>\nIt would make sense... heh.\n<p>\nOther than that , super superb! PHP was the way to go. Good job.\n<p>Jason"
    author: "Jason Katz-Brown"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-10-31
    body: "This new site looks great and navigation is a lot better. You've done a super job, guys!\n\nThe only thing I found not working is \"Search\" in the home page, it produces \"Page not found\" message."
    author: "Oleg Noskov"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-10-31
    body: "The link is wrong, it should be to search.php instead of search.html.  It's being fixed, might take a while for the CVS update though."
    author: "Dre"
  - subject: "Actually..."
    date: 2000-10-31
    body: "Should've just used Zope, since it's used right here already ;-)."
    author: "Eron Lloyd"
  - subject: "Re: Actually..."
    date: 2000-10-31
    body: "Well, we're hosted on completely different servers.  Plus, IMHO, Zope is less practical for KDE.org, for various reasons.  Kurt did certainly consider using Zope, btw.\n\nCheers,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: Actually..."
    date: 2000-11-01
    body: "Less practical? I'd be interested (as a Zope developer) to know why you (or Kurt) feel that way. No flaming - just feedback. Thanks."
    author: "Eron Lloyd"
  - subject: "Re: Actually..."
    date: 2000-11-01
    body: "Heh, at the risk of putting in my foot in my mouth... \n<p>\nWell, first, let me say the more I look at the Zope, the more I'm amazed.  There's a huge number of third party apps and community support out there, updating the system is a breeze (just drop in a Hotfix, restart, and you're done), the ability to rollback changes (though it doesn't always seem to work) is wonderful.  Almost everything can be minutely customized and there's a solution to almost every problem.\n<p>\nOne problem: KDE has a long history of storing in and updating the website from CVS.  Multiple people can work on the website and keep track of it this way.  Zope, by default, however uses some sort of  proprietary DB (still OpenSource, of course) that you can't directly access,  and so one would have to use the Web interface or the rather clunky FTP interface to update it.  The Zope FTP interface is not fail-safe either and doesn't support many of the usual FTP features. A comprehensive script would have to be written to handle the situation.  Would it even work?  Probably not, but maybe you can correct me.\n<p>\nAssuming that KDE dropped CVS and started the Zope Web interface instead, personally I don't see this working too well with multiple people trying to update and maintain the site.\n<p>\nSecond, mirror sites...  but we've got a similar problem with PHP.  Then again I'd imagine, PHP would be less of a problem than Zope for mirror sites, as PHP is pretty common out there.  It's almost the defacto Linux web-scripting platform.\n<p>\nThird, I didn't feel comfortable advocating Zope instead of PHP to Kurt, because after all this time, I still haven't learnt it properly and I still have trouble figuring things out.  I'm not generally dense, but I had trouble figuring out some of the Zope syntax/API without reading lots of  seemingly inadequate documentation (which sometimes don't help if you don't know Zope basics :). \n<p>\nOn the other hand, although I don't know PHP, Andreas Pour has constantly impressed me with how much and how quickly he can accomplish something in it.  I get the impression that PHP has a less steep learning curve than Zope and is easier to grasp for someone who just wants to get down to hacking.  I'm sure if it came to it Andreas could offer Kurt a PHP solution faster than I could figure out one in Zope.  :)  \n<p>\nIn short, more people around knew PHP and less of us knew Zope.  No doubt Zope would have been a great solution too, but the implementation would have probably required a little more effort and possibly outside help (although Kurt may certainly be better at grasping Zope than me). ;)\n<p>\nCheers,<br>\nNavin.\n\n"
    author: "Navindra Umanee"
  - subject: "Re: Actually..."
    date: 2000-11-01
    body: "Navindra answered this pretty well.. but here is the short (and 100% accurate since I made the decision) reason:\n\n1. Zope natively uses an internal database to store the pages.  I wanted to keep the pages in CVS.  I know it's possible to skip the database.. but that brought me to the second point..\n\n2. I am not very familiar with either Zope or PHP.. but I know a LOT of people in the KDE community that know PHP and don't know of any beside Navindra that know anything about Zope.  If I have a PHP question, I can get it answered very quickly.  If I had a Zope question, I wouldn't have known where to start.\n\nSo in the end, in spirit with the KDE style of development, I chose the solution that worked for me at the time... and it was PHP."
    author: "Kurt Granroth"
  - subject: "Points well taken"
    date: 2000-11-01
    body: "*nod* ... and thats fine. The site DOES look well, regardless. Maybe we can get an IOSlave for WebDAV and XML-RPC/SOAP for KDE, and make accessing the Object Database somewhat easier for KDE Users (I envision managing Zope from Konqueror). Well, at any rate, use what ya know, be it PHP or whatever - just keep up the excellent work!\n\nEron"
    author: "Eron Lloyd"
  - subject: "Am I the only one?...."
    date: 2000-10-31
    body: "Or is the Emperor wearing no clothes? This new \nsite seems really, well, boring and bland, to say the least (like the series finale of Seinfeld). Very functional and sophisticated under the hood, but borrrring.\n\nOh well, there's no accounting for taste....\n\n> There was a lot of discussion and debating that > went on, and the result is a consensus          > project. \n\nI guess this is what happens when you design by committee:-)\n\nStill, congratulations on the new site!\nSamawi."
    author: "Samawi"
  - subject: "Re: Am I the only one?...."
    date: 2000-11-01
    body: "Well, sure we could go for something much more flashy with lots of fancy graphics etc. However, we decided a long time ago that we wanted a web site that was quick to load, and that if there was choice between looking exciting and being a no-nonsense way to get to the information you wanted then we would go for the latter. The new design aims to provide a modern look and feel without sacrificing the accessibility of the information.\n<p>\nA lot of sites today are very nice to look at, but are a nightmare to use if you actually want to find a piece of information, we don't want the KDE web site to be numbered amongst them. I think the new design provides a good balance between the two. If people have suggestions for how we can make the site better (bearing in mind the above) then we'd be happy to hear what they have to say."
    author: "Richard Moore"
  - subject: "Re: Am I the only one?...."
    date: 2000-11-01
    body: "Why can't you use the old style, like this site does? It's looks are much better.\nThe new style is too noisy."
    author: "reihal"
  - subject: "Re: Am I the only one?...."
    date: 2002-07-09
    body: "Please DO remember that the big audience (and the decision makers!) expect a clean, sophisticated website. The site is probably their first introduction to KDE and therefore it *must* look slick and clean. To be honest, I am not too satisfied with the current site. It just cannot compete with the 'company' sites out there and that is a potential loss for us all. Again: you never get the second chance for a first impression..."
    author: "Marko Faas"
  - subject: "Re: Am I the only one?...."
    date: 2000-11-01
    body: "No, I totally agree with you.\nMake it dynamic but keep the old look."
    author: "Coco"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-10-31
    body: "It's about time! *sigh*  Nearly any major player, whether it be open source or commercial, has a decent website.  I have always been embarrassed to show newbies that site so I always pointed them to the comercialized kde.com."
    author: "hevyd"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-01
    body: "You found the old KDE.org embarassing!?\n\nKDE.com is embarassing. It looks and feel awful.\nThe new KDE.org may be technically great, being dynamic etc, but it doesn't look as polished as the old version.\n\nSorry."
    author: "Coco"
  - subject: "Screenshots"
    date: 2000-10-31
    body: "Why do you begin the \"screenshots\" section with screenshots of KDE 1.x? People who come to www.kde.org want to see what's new, not what's old :-)\nAlso some screenshots are JPG, some are PNG. JPG format is not suited well for screenshots so please don't use it since compatability with old browsers is not your concern anyway."
    author: "Pavel Roskin"
  - subject: "Re: Screenshots"
    date: 2000-11-01
    body: "You make good points, I agree this should be fixed. Any help (as always) would be much appreciated."
    author: "Richard Moore"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-01
    body: "Hi,\n\nI quite like the new design, both from a usability and aesthetical POV. The more blueish color looks better than the old violet one as well. My only tiny complaint is, that it could be a little bit lighter and more colorful. Maybe a few lighter and darker shades of that blue here and there?"
    author: "gis"
  - subject: "Dates for announcements"
    date: 2000-11-01
    body: "The new site looks great! Clean and not cluttered; I like that. I would humbly suggest that you put a date on the announcements, though. The KDE 2.0 announcement says, \"The KDE Team today announced...\"\n\nOf course, clicking thru will reveal that \"today\" is actually Oct. 23, but it's a bit confusing :) Anyway, congrats on the new site!"
    author: "tq123"
  - subject: "Missing a \"works best\" statement :)"
    date: 2000-11-01
    body: "Great new site. It's funny but often people judge the life in a project by how often the web page is updated :) Now with the PHP code hopefully it'll be close to second to slashdot or freshmeat :)\n\nAnyway, I just thought it's missing a \"best viewed with Konqueror\" logo or something (which I'm using right now btw :))"
    author: "loom"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-01
    body: "Great job, guys!  I am so happy about the changes!  We have always beaten GNOME with the software, and finally, we have beaten them with our website.  I especially like the \"Applications\" section, so I can stay up to date on the latest and greatest software for KDE.  Kongratulations!  I was just wondering if the other KDE sites (such as developer.kde.org) will follow the same design as this."
    author: "Steve Hunt"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-01
    body: "Yes, I really like the new look especially the top banner with Konqui the dragon showing things off.  It loads quickly too. \n\nI'm sure the site will evolve more quickly now with the new framework in place, in response to comments by visitors.  A little more in the way of fancy graphics might be useful, maybe in the context of stories.  Thank God you got rid of the notebook image and the animated gif.  \n\nThere are two gripes, though. \n\n1.  Centering.  Since you are using a fixed width page it should be centered in the browser. Currently everything is flush with the left edge and the title banner doesn't line up with the tables below.  This is how it looks in the current Konqueror.  Not quite right.\n\n2. apps.kde.com.   If you are going to link to this site, please get the webmaster there to fix the site. It's horrible. Yes, it does have the apps, but hangs up if you are using a proxy.  Doesn't eveyone use junkbuster or something?  The default view with 4 (count em) frames puts a strain on any browser, including Konqueror.  Switching to the no-frames view usually crashes or hangs up Konqueror and Netscape, placing a recursive view of the entire page in the cetner frame, at least for me, and clicking on app links in the right hand frame often does the same.  \n\nI feel that Kde should have its own apps listing anyway and not rely on this commercial site. To me, it's a confict of interests. If you insist on using it for apps annoucements please at least make it usable for those of us who value our privacy and refuse to accept cookies or turn off our filtering proxies.\n\nJohn"
    author: "John Califf"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-01
    body: "> 2. apps.kde.com. If you are going to link to this site, please get the\n> webmaster there to fix the site. It's horrible.\n \nHi, I hope we can solve your problems.\n \n> Yes, it does have the\n> apps, but hangs up if you are using a proxy.\n \nThat shouldn't happen, I myself often use a proxy to access the site.\nIf somehow you are in the position where it tries to serve compressed\nHTML to speed up downloading, many proxies are buggy and can't handle\nit.  However, the site has a mechanism to detect this and it should\nnot happen a second time.  It would probably be more helpful if the\nbuggy proxies were fixed, though.\n \n> Doesn't eveyone use\n> junkbuster or something? The default view with 4 (count em) frames puts\n> a strain on any browser, including Konqueror. Switching to the\n> no-frames view usually crashes or hangs up Konqueror and Netscape,\n \nI don't have that problem.  Which Konqueror do you use and which Netscape\ndo you use?  I've been to the site thousands of times and my browser has\nnever crashed.\n \nMay I suggest the text option for you?  It's quite a bit lighter.\n \n> placing a recursive view of the entire page in the cetner frame, at least\n> for me, and clicking on app links in the right hand frame often does\n> the same\n \nThat's a bug in your browser, hopefully that will get fixed.  Once\nthe 2.0.1 version is out we can start thinking about dealing with\nkhtml quirks to make things work better, until now it's been improving\ntoo quickly to make it worth it.\n\n \n> I feel that Kde should have its own apps listing anyway and not rely\n> on this commercial site. To me, it's a confict of interests. If you\n> insist on using it for apps annoucements please at least make it usable\n> for those of us who value our privacy and refuse to accept cookies or turn\n> off our filtering proxies.\n \nThere is no need to accept cookies to use the site and there is no\nneed to turn off filtering proxies.  It uses HTML just like every other\nsite, though if your browser says it accepts gzip'd data it may actually try to send it, since it can't know there is a buggy proxy in the way.  If you have any particular problems that can't be resolved with\na reload please write me, we are always working to make the site better."
    author: "Dre"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-01
    body: "I'll second your comments on apps.kde.com .\n\nPerhaps the intension with apps.kde.com was to make it easy to navigate, but the 4 (four!) frames just clutters the screen. It would be much better with a unframed version of the app-index as the front page.\n\nAlso the listings of the software is too terse. To get a grip about what a software package is about you need to enter the page for that package. Look at freshmeat how to make a informative summary of a package."
    author: "Claus Rasmussen"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-02
    body: "<P><EM>Perhaps the intension with apps.kde.com was to make it easy to navigate, but the 4 (four!) frames just clutters the screen. It would be much better with a unframed version of the app-index as the  front page.</EM>\n</P>\n<P>\nYou can bookmark it that way, if you like, just use<A HREF=\"http://apps.kde.com/indexnf.php\">http://apps.kde.com/indexnf.php</A> as your entry point.  Perhaps the kde.org front page can have a sub-link for the no-frames option.\n</P>\n<P>\n<EM>\n\n      Also the listings of the software is too terse. To get a grip about what a software package is about you need to enter the page for that package. Look at freshmeat how to\n      make a informative summary of a package.</EM>\n</P>\n<P>\nThe only thing freshmeat does is add a \"changes\" paragraph.  IMHO that does not give you a better feel for the app but insted clutters the page.  What apps.kde.com provides is the app's icon and the app's documentation, if it comes in the package; though you have to click to view the documentation, I find this information much more helpful in deciding if an app is for me.  Which is why we use frames, it makes these clicks extremely fast (including to the screenshots, also not found at Freshmeat, and to the full ChangeLog).\n</P>\n<P>\nTo get back to the Changes paragraph, the way they do that is force developers to provide that information.  Once developers start contributing more we'll be happy to include that information :-). We actually refer to it as a \"Version Comment\".\n</P>"
    author: "Dre"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-02
    body: "<I>You can bookmark it that way, if you like, just usehttp://apps.kde.com/indexnf.php as your entry point. Perhaps the kde.org front page can have a sub-link for the no-frames option.</I>\n<P>\nYou're right. The non-frames version is <B>much</B> better. I would suggest that you make the non-frame version the default link.\n<P>\n<I>The only thing freshmeat does is add a \"changes\" paragraph. IMHO that does not give you a better feel for the app but insted clutters the page.</I>\n<P>\nI think the \"changes\" paragraph is fine, but YMMV. But it was not the \"changes\" paragraph I \nwas referring to. It was the descriptions of the packages. I know - by now - that its not your fault, but the developers. But take a lot at this random description taken from the first item at the freshmeat page to see how I think it should be done:\n<P>\n<I>\"SkinCat.pl is a Perl script that will decrypt and look up just about anything you can scan with a :Cue:Cat. If the barcode is an ISBN, it will print out the title and author, as listed at amazon.com. It also does lookups on DCNV's database. SkinCat does not send your CueCat serial number anywhere. It is very useful for command-line lookups, and can be used as part of a database-entry system\".\n<I>\n<P>\n...and compare it to this (extreme) example from apps.kde.com:\n<P>\n<I>\na radio card tool\n</I>\n<P>\nOh, and I almost forgot to tell you: I <B>do</B> like the new layout of www.kde.org. Congrats."
    author: "Claus Rasmussen"
  - subject: "Re: KDE.org:  bad rendering on netscape on windows"
    date: 2000-11-01
    body: "Looks alot better than the old site. However when I load it with Netscape on windows it does not render correctly. The text is too far to the left on top of the blue border area. Its about one capital letter too far left. Other than that it looks quite nice."
    author: "mike"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-01
    body: "Looks very good. It has similar functional look as KDE desktop and loads quickly. Very nice indeed."
    author: "jannek"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-01
    body: "Some minor points. First, the color selection on the top links (FAQ | Download | Screenshots | news) when they been clicked on in less then optimal. Red or whatever the color is does *not* work well blue. The links are almost invisible. <P>Second, the top \"cell\" containing Conquer your... is not aligned with the rest of the page. It starts at position (0,0), hence it look out of place.<P>\nIt looks *great* other than those small gripes."
    author: "J\u00f6rgen Lundberg"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-01
    body: "<P><BLOCKQUOTE><I>Some minor points. First, the color selection on the top links (FAQ | Download | Screenshots | news) when they been clicked on in less then optimal. Red or whatever the color is does *not* work well blue. The links are almost invisible.</I></BLOCKQUOTE></P>\n<P>It also seems that some of these links don't render properly in Internet Explorer. Only FAQ, Download, and Contact Us show up, whereas the others are invisible.</P>"
    author: "jliechty"
  - subject: "Quick to load, yet looking better"
    date: 2000-11-01
    body: "\"However, we decided a long time ago that we wanted a web site that was quick to load, and that if there was choice between looking exciting and being a no-nonsense way to get to the information you wanted then we would go for the latter. \"\n\nYou are quite right about that! I always liked the free bsd ( http://www.freebsd.org ) site for those qualities. However, I think the bsd site looks very good while the new KDE site is a bit dense. I like the free bsd colors better, but that is a question of taste - KDE choose blue, ok. \n\nSome points:\n- The new site could use some more white. The three blocks that start with KDE is... fill up the space. I keep it simpel, I compare with the bsd site: there the blocks have headers: What is ... Cutting edge ... , etc. Headers make for good reading as have been discovered ages ago :-)  People do not read web pages, they scan them (As surveys show.). So give them headers... It solves the problem the site is dense, it makes the info more readable. \n\n - Gis already remarked it could be more colorful. Yes, the blue links on a blue background look quite grey, dull.  It is also hard for visually impaired people. This should be changed, if only for accessability.\n\n - The box with the links at the left side takes 1/3 of the width. It does make the links prominent, very good. On the other hand, a 1/3 - 2/3 balance looks dull, not exciting. Changing colors in the links box makes the links better readable, so the width can be brought down. It does not take heavy graphics to make a page look more exciting. \n\nWhile the free bsd and KDE sites seem to have the same goal (quick to load, no nonsense) the bsd one is very beautiful, the new KDE one is not attractive visually. More close to home: the KDE's iconfactory ( http://www.kde.org/artist/introduction.html ) , while using almost the same style is much better too... While the bsd site is almost provocative in it's use of colors, the iconfactory is within the \"corporate look\" KDE uses. \n\nGlad you've got the internals right! :-)\n\nAnte"
    author: "ante"
  - subject: "feedback using bugs.kde.org"
    date: 2000-11-01
    body: "BTW, you can use <a href=\"http://bugs.kde.org\">bugs.kde.org</a> to report bugs against our web page, too. The module (if you'\nre using email) is called \"www\". Actually this wasn't used very much in the past, but I like the idea to handle web bugs just as software bugs."
    author: "Daniel Naber"
  - subject: "The dinosaur must go ... it makes no sense"
    date: 2000-11-01
    body: "Put it somewhere on the site maybe but in the header?? This is silly.  At least with the old laptop image an average joe/jane could tell WTF the site is about!\n\n\nAnd why all the hype about \"powerful\" this and that? The page in the best tradition of free software should simply state the honest facts. Let the users/reviewers decide how powerful it is."
    author: "couard anonyme"
  - subject: "Re: The dinosaur must go ... it makes no sense"
    date: 2000-11-02
    body: "<p>I agree with you. I think I liked the old design better. The new design is boring, ugly and has no apparent structure. I'm soo sick and tired of boxes like the one on the new page, and the top logo looks like a banner.</p>\n\n<p>As you say, the old page had a natural \"entry-point\" - <b>the laptop</b>. The new one is just an ugly mix of boxes and banners (hrm) that doesn't give me a clue of where I should start reading.</p>"
    author: "Johan"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-01
    body: "The KDE web site was a fairly good design but it had not changed for so long that it seemed bland compared to many other Linux project sites.  \n\nThe new look is wonderful. Kudos to everyone that worked on it."
    author: "David Rugge"
  - subject: "Bad grammar and phrasing and an unclear message"
    date: 2000-11-01
    body: "The three paragraphs at the top do not sound right to a native English speaker (the third is meaningless to the point of being embarassing). I know they are the same as with previous version of the site but I complained about them too ;-) Here's my rewrite (note the changed order which makes more sense):\n\n<blockquote>\n<p><b>KDE</b> is a Open Source graphical desktop environment for Unix workstations. It combines ease of use and contemporary graphical design with the stability of the Unix operating system.\n\n<p><b>KDE</b> provides a solid foundation for a growing number of applications. KDE includes a high quality development framework for Unix that makes developing software easier and faster, from simple applications to complex office suites and word processors.\n\n<p>\n<b>KDE</b> is an Internet project and is truly open in every sense. Development takes place on the Internet and is discussed on our mailing lists, USENET news groups, and IRC channels to which we invite and welcome everyone.\n\n</blockquote>\n\n,p>There might be mistakes here too, but anything is an improvement over the current poorly worded version."
    author: "couard anonyme"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-01
    body: "I like it, with one minor exception. The links at the top of the page are hard to read once you've already visited them (well, in IE anyways). Click on the FAQ, then go back to kde.org and look at the link."
    author: "bob"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-02
    body: "I note your heavy use of tables as an HTML block device on your site as opposed to using <p> or <div> blocks. Tables are used as margins rather than using cascading style sheets to set margins.\nIs this because Konqueror IS NOT YET CSS2 COMPLIANT?\n\n(now if I could only get KMail for KDE2 2.0 to stop crashing on startup)"
    author: "Bryan Cebuliak"
  - subject: "Re: KDE.org:  A New Site for a New KDE"
    date: 2000-11-02
    body: "Just one gripe, at least on my system your\nuse in the kde.css file of,\n\ntd {font-family: verdana,geneva,arial,helvetica,sans-serif;\n    font-size: small;}\n\nresults in a hard to read font being used\n(I think arial)... Just leaving it to helvetica\nmake the page a lot more readable!"
    author: "Adrian Bool"
  - subject: "New News"
    date: 2000-11-02
    body: "<p>I regularly check the KDE page to read the news. At first I was delighted to see that the news section is now in view with a \"normal\" browser window - no more scrolling to get to the point!</p>\n\n<p><b>But</b> - in the old version I could click into the news list from then first page and then read ALL the news with just the help of the scrollbar. Now I have to go forth and back between news-view and first page. And if there are more than five new items I also have to press \"more\" and get yet another view of the news, this time listing something like the first paragraph of the latest news (?).</p>\n\n<p>I would really like a system that works like the old one - click in first page news list sends me to the right anchor in an all-news-full-text page (or all-news-since-30-days). <i>If I want to comment something I won't bother having to click twice, but when I simply want to read the news it's too much work as it is now!</i></p>"
    author: "Johan"
  - subject: "Re: New News"
    date: 2000-11-02
    body: "Well, we could add anchor hooks to the dot.kde.org frontpage and tweak rdfplus so that the KDE site points to anchors on the frontpage, but I'm not sure it's necessary or if we should even do that. I think you'll get used to the new system soon enough.  \n\nOr else we could have an enhanced version of http://dot.kde.org/articles that would also display the article text.  30 days is quite a lot though.\n\n-N.\n"
    author: "Navindra Umanee"
---
Hallelujah!  The <A HREF="http://www.kde.org/">KDE site</A> has quite a new
look!  It's been completely redesigned and I like it, thank you very much!
But judge for yourself...  Is the new layout better?  Is
the new art better?  Are things easier to find?  We want to know what you think!  I asked <A HREF="mailto:granrothNOSPAMkde.org">Kurt
Granroth</A>, the KDE core developer behind the changes, a few questions you might have as well.  Read what he had to say below.




<!--break-->
<P>
I conducted a brief interview with <A HREF="mailto:granrothNOSPAMkde.org">Kurt Granroth</A>, the force behind the new <A HREF="http://www.kde.org/">KDE.org</A>
design.  Here is what he had to say:
</P>
<BLOCKQUOTE>
<P>
<EM>Dre</EM>.  <STRONG>  What motivated the changes to the design?</STRONG>
</P>
<P>
<EM>Kurt</EM>.  We had two principal objectives in mind.  First, navigating the
old
site was a bit difficult.  There were too many links on the front
page and things were not organized as well as they should be.  Second, we
wanted to switch to dynamic page generation.  As it stood it was quite
difficult to update pages, as we did not use templates.  Now we are using
<A HREF="http://www.php.net/">PHP</A> and templates so maintaining the
site will be much easier.
</P>
<P>
<EM>Dre</EM>.  <STRONG>  It looks like the look-and-feel has changed as well.  How did that come about?</STRONG>
</P>
<P>
<EM>Kurt</EM>.  The site design had not changed in several years.  KDE has changed a
lot in that time and we felt it was time to get a more modern look for the site.</P>
<EM>Dre</EM>.  <STRONG>  How long has this redesign been in the works?</STRONG>
</P>
<P>
<EM>Kurt</EM>.  The idea to redesign has been floating about for quite
some time, but all active development has occurred in just the last
couple months.
</P>
<EM>Dre</EM>.  <STRONG>  So how did the new design get implemented?</STRONG>
</P>
<P>
<EM>Kurt</EM>.  Most of the PHP coding, artwork, and layout was done by me,
but it was a highly interactive process.  We set up a mailing list two
months ago for people interested in the new site.  There was a lot of
discussion and debating that went on, and the result is a consensus
project.
</P>
<EM>Dre</EM>.  <STRONG>  What about mirror sites?  How do they keep being mirrors?</STRONG>
</P>
<P>
<EM>Kurt</EM>. The entire site is still available from CVS in the www
module, though it's mostly PHP pages now.  All the PHP code is
available under an Open Source license, though we haven't decided on
the exact license yet.  For mirrors to be a "true" mirror they will
need to PHP-enable their site and mirror the PHP code.  The settings
that are required to use the pages will be available soon.
</P>
<EM>Dre</EM>.  <STRONG>  What about mirrors that cannot use PHP?  Can you make a static version of the site available?</STRONG>
</P>
<P>
<EM>Kurt</EM>.  We'll consider this.  Perhaps we can create a tarball
of the site nightly and let mirrors update their site with that.
</P>
<EM>Dre</EM>.  <STRONG>  What about other KDE application sites that used the KDE site
design as a template.  Are they able to use this new design as well?</STRONG>
</P>
<P>
<EM>Kurt</EM>.  Absolutely.  As I mentioned, the PHP code is freely
available from CVS.  Many of the KDE applications are hosted on
<A HREF="http://www.sourceforge.net/">SourceForge</A>, which supports PHP so
updating those pages <EM>should</EM> be fairly painless.
Otherwise they can download pages from the KDE.org site and modify the static
HTML to taste.
</P>
<EM>Dre</EM>.  <STRONG>  So how is the server handling the increased load?</STRONG>
</P>
<P>
<EM>Kurt</EM>.  Great so far!  We are very happy with PHP.  Plus the
front page is only regenerated every 30 minutes and served statically.
</P>
</BLOCKQUOTE>
<P>
There you have it.  Thanks, Kurt, I love the new design!



