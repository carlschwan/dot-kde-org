---
title: "People behind KDE: Stephan Kulow"
date:    2000-09-25
authors:
  - "numanee"
slug:    people-behind-kde-stephan-kulow
comments:
  - subject: "Re: People behind KDE: Stephan Kulow"
    date: 2000-09-25
    body: "Thanks for your tireless work, Stephan.  I wish you well with your diploma thesis!"
    author: "KDE User"
  - subject: "Re: People behind KDE: Stephan Kulow"
    date: 2000-09-27
    body: "Keep up the good work Stephan, and i wish you luck with your exams...\nAnmar"
    author: "Anmar"
---
<a href="http://www.kde.org/people/people.html">Tink</a> is back, this time with an <a href="http://www.kde.org/people/stephan.html">interview</a> of the venerable Stephan Kulow.  Stephan has been with the KDE project, way back when, so we get a free history lesson too.  Favorite quote: <i>"But for me a KDE without kpat is no KDE."</i>