---
title: "theKompany.com releases Kugar, a business reporting tool for KDE"
date:    2000-09-22
authors:
  - "rwilliams"
slug:    thekompanycom-releases-kugar-business-reporting-tool-kde
---
<A HREF="http://www.theKompany.com">theKompany.com</A> announced a new reporting application for KDE that works by merging application data and a template to create a final report.  It can run standalone or as a KPart (which means it can be embedded in other KDE applications, such as KWord or Konqueror).  Impressive stuff.
"<I>Kugar is a business report generation and viewer tool for KDE. Kugar takes application generated data and merges it with a template to produce a high quality report that can be displayed or printed. Both data and templates are specified in XML ... <a href="http://www.konqueror.org">Konqueror</a> can be used to view formatted reports simply by clicking on the XML data file.</I>"
<a href="http://www.thekompany.com/projects/kugar">Kugar homepage</A>



<!--break-->
