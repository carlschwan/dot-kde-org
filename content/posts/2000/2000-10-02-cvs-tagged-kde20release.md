---
title: "CVS tagged KDE_2_0_RELEASE"
date:    2000-10-02
authors:
  - "Dre"
slug:    cvs-tagged-kde20release
comments:
  - subject: "Please O please O please don't"
    date: 2000-10-02
    body: "rush on anyone's account. Stick to your guns and do it right! You all remember what happened to Gnome 1.0 dispite its higher political and \"mindshare\" standing. They still have not completely lived it down. If KDE does the same thing, then trust me, the critics will make even more noise. \n\nI am sure the developers know all of this already but I don't want to see KDE lose any more mindshare. If it takes another month or more to really justify the 2.0 version number, then take it. A few extra weeks won't hurt anyone.\n\nOf course, if things are really as they should be then that's absolutely wonderful! I look forward to the release."
    author: "Samawi"
  - subject: "Re: Please O please O please don't"
    date: 2000-10-04
    body: "They could wait forever, and things would get better, but nobody wants to do that. The fact is KDE 2 is way overdue, and it's now decently stable to ship.\n\nDon't get me wrong, KDE 2, has it's share of problems and missing features, that could be fixed in a few months. But why delay the whole release over some silly problems? In general KDE 2 is quite good right now, and I believe it comes a time, that they have to say, time to ship it.\n\nYes, nobody is saying KDE 2 is perfect. I could name several area of KDE 2 that could use work. First, Konqueror is far from perfect, I wouldn't use it for day to day browsing, but for basic stuff it's okay. Konqueror not being perfect isn't a showstopper really, as people can always use other browser (like Netscape which I am currently using). Second, KDE 2 lacks a theme manager kcm ala KDE 1.1.2, that makes installing themes easy. And lets' not forget that there is no kcm for changing the window manager border yet.\n\nMost of those minor problems, will probably be fixed in KDE 2.1. That's fine with me, I think the most important thing is to get a solid release out the door, and just work around the minor kinks, and get them patched up in future versions."
    author: "AArthur"
  - subject: "Re: Please O please O please don't"
    date: 2000-10-06
    body: "hey! konqueror is vary good for browsing the web. It has java support added to it so the web experiance is vastly better than kfm and in my oppinion it looks far better than netscape. the only thing they do need to do to it is to embed the kmail client and the news group reader into it so then we can have a full and robust one-stop interface for all our computing needs. and if they do that netscape will surely die."
    author: "JDP"
  - subject: "Mandrake RPMs"
    date: 2000-10-02
    body: "I see cooker (Mandrake-devel distro) has new KDE2 RPMs as of today. They've called them \"1.99\" (Mandrake 7.2 is right around the corner, and will ship with a pre-final KDE2, sadly). If you run Mandrake, now is the time to test KDE2 and report any serious bugs."
    author: "Haakon Nilsen"
  - subject: "uh-oh"
    date: 2000-10-03
    body: "I've downloaded and installed those \"1.99\" RPMs, and I'm experiencing many of the same problems and bugs as I was before.  (Java doesn't work well, cursors disappear mysteriously, etc.)  None of them are really showstoppers (except for the fact that KPackage utterly fails to work), but taken together, they really detract from KDE2 (which is otherwise great!).  If it were my decision, I'd certainly wait a couple more weeks.  Are other people having problems with bugs like me?  Or did I install KDE wrong or something (I hope)?  Please post your experiences if you've upgraded to the newest version of KDE2."
    author: "James"
  - subject: "Re: uh-oh"
    date: 2000-10-03
    body: "I built the latest (1.94) version from sources on FreeBSD 3.4 and I had a lot of problem with it.\n\tFirst, Konqueror crashes even in relatively simple situations. Next, for some reason, KDE refuses to shutdown so I need to kill it explicitly in order to end the session. The last thing I want to mention is that keyboard switcher doesn't work properly and I think that IS a REAL SHOWSTOPPER for people whose native language isn't english. There were some non-critical compiling errors on my system but they are very simple to fix, so I hope it is not a problem.\n\tDespite of that, I would like to say KDE-2 now looks very good and much more stable comparing to the older versions. Thank you for your work and good luck!\n\nPS. Sorry for my terrible english :)"
    author: "Lev V. CHIRIN"
  - subject: "Re: uh-oh"
    date: 2000-10-03
    body: "There is something wrong with your installation.\nMy 1.94 works almost perfectly, Konqueror hasn't\ncrashed at all etc. Works fine IMHO."
    author: "jannek"
  - subject: "Re: uh-oh"
    date: 2000-10-03
    body: "Did you follow instalation instructions for FreeBSD?\n\nI run following command to configure (on /bin/sh) :\n\nCXXFLAGS='-O3 -fno-exceptions' \\ LDFLAGS='-Wl,-export-dynamic' ./configure ... (other options)\n\nKonqy never crashes for me ;-)"
    author: "fura"
  - subject: "Re: uh-oh"
    date: 2000-10-03
    body: "Of course I've missed those instructions! :)\nI'll try to rebuild KDE.\n\nThank you!"
    author: "Lev V. CHIRIN"
  - subject: "Re: uh-oh"
    date: 2000-10-03
    body: "I've had similar experiences with the beta5\nunder Caldera 2.4.\n\nJava doesn't work too well and javascript still\nmisses some features. The html rendering is not \nalways perfect either, but that's a technicality\nI'm sure. However, other than that, I think KDE2\nis pretty much ready. Kmail, korganizer, konsole,\nkwrite are more stable and much more mature \nthan in the latest stable KDE (1.1.2?). Kicker,\nkwin, kdesktop are performing okay: they\neven restart automagically when (if?) they crash.\n\nKppp, kghostview, kpackage and knotes I find are \nnot quite there yet: I wonder if anyone is \nmaintaining them? Some bugs in kghostview haven't\nbeen fixed in more than 1 year already. \n\nOverall however, I feel it's time KDE2 gets\nreleased. There's always gonna be some apps that \nare a bit more buggy than others.."
    author: "Bernard Parent"
  - subject: "Looks like they may not be rushing things too much"
    date: 2000-10-02
    body: "\"We might delay the release for a RC2 in case any serious problems to be found that can't be fixed until Monday 09/10. In this case the schedule will be delayed for another week.\"\n\nIt looks like they've got somewhat of a safety net with the RC2. Good.\n\nWonder what they consider a showstopper, though? Does it have to be a bug that makes things crash, or does a really dumb misfeature like the \"Type:\" field in kmenuedit count?"
    author: "J. J. Ramsey"
  - subject: "Re: CVS tagged KDE_2_0_RELEASE"
    date: 2000-10-04
    body: "Anyone know of tarball releases?  I've looked and been unable to find.  Maybe I'm just lazy."
    author: "Wreck"
  - subject: "Re: CVS tagged KDE_2_0_RELEASE"
    date: 2000-10-04
    body: "wow, sorry for the doublepost/wasted posts.  I'm not european.. \"RC1 tarballs on 9/10\" to me, meant \"September 9th.\"  I'm such a silly american."
    author: "Wreck"
  - subject: "Re: CVS tagged KDE_2_0_RELEASE"
    date: 2000-10-10
    body: "You certainly are a silly American if you can somehow get September 9th out of 9/10    ;)"
    author: "Matt"
  - subject: "Re: CVS tagged KDE_2_0_RELEASE"
    date: 2000-10-10
    body: "er, Sept 10th.  or something.  i need sleep. fasljkdaskldjflaksdjfasdfjks."
    author: "Wreck"
  - subject: "Re: CVS tagged KDE_2_0_RELEASE"
    date: 2000-10-05
    body: "Over on the KDE User mailing list, at http://lists.kde.org/?t=97047438100002&w=2&r=1\nI have been struggling to understand what the final release will look like on my SuSE 7.0 directory structure. Some questions:\n<br>(1) Is KDE 2.0 \"final release\" meant to replace or to coexist with KDE 1.1.2 ?\nand a related question:\n<br>(2) Is the answer to (1) up to each distribution, so eg. for Red Hat, KDE 2.0 replaces 1.1.2 immediately, but for SuSE, KDE 2.0 is separate from 1.1.2 and they can coexist indefinitely?\n<br>(3) Should KDE 1.1.X apps run using KDE 2.0 libraries without recompilation?\n<br>(4) If KDE 1.1.X apps are not ready for KDE 2.0 and if KDE 2.0 is meant to replace KDE 1.1.2 and not coexist with it, will the KDE 1.1.2 apps break, get wiped out or just run?"
    author: "Paul C. Leopardi"
  - subject: "Re: CVS tagged KDE_2_0_RELEASE"
    date: 2000-10-05
    body: "1) If can coexist with the older versions of kdelibs and qt, although ideally it's meant to replace it.\n\n2) It could be up to the distros own preferences, but I think most will allow qt 1.x and kdelibs 1.x to be installed on a system also with qt 2.x, kdelibs 2.x and the rest of KDE 2.\n\n3) No. In most cases it will require updated versions of these apps, as many of the library calls have changed. That said, most KDE 1.x apps will run perfectly under KDE 2 (looking and acting like KDE 1.x apps).\n\n4) Assuming you leave KDE 1.1.2 libs and Qt 1.4.5 installed, when installing KDE 2.0, your KDE 1.1.2 apps will continue to run fine. As said above, they won't get any of the benfits of KDE 2 (such as widget themeing), but most should run with no problems."
    author: "AArthur"
  - subject: "Re: CVS tagged KDE_2_0_RELEASE"
    date: 2000-10-06
    body: "There seem to be a number of differences of opinion on how to install KDE 2.0 on different distributions. I would dare to go as far as to call this \"confusion.\"\n\nI would dearly like each packager for each distribution to spell out clearly and simply:\n1. How to install KDE 2.0 for their distribution\n2. What happens to KDE 1.1.X libraries and applications\nFor 1. there are a number of cases: (a) KDE 1.1.X already installed (b) KDE 1.9X already installed (c) no version of KDE already installed.\nThere are also dependencies andother \"gotchas\" that I'm sure naive users would rather not learn through bitter experience.\n\nAn example of the variation in installations follows. I doubt that the following is what the SuSE packager intends all SuSE users to do, since it would mean that the on the next upgrade of SuSE the installation of KDE RPMs would probably break. Yet at this point I just don't know, because the documentation I want, need and am asking for is still missing:\n\nEugene Tyurin <eugene_tyurin@yahoo.com>\n05/10/00 10:52 AM\n \nTo: Paul C. Leopardi@Andersen Consulting, suse-linux-e@suse.com\nSubject: Re: [SLE] Re: kde 2 final & kde 1.x apps\n\nHi,\n\nI am not a SuSE employee, but  I've installed KDE 2.0FinalBeta on my 6.4\nmachine, and here's my take:\n\n1) Your old  KDE 1.x is in  /opt/kde - move the  directory to /opt/kde1.\nKDE2 will be  installed by RPM to /opt/kde2 -  symbolically link that to\n/opt/kde.\n\n2)  Edit /opt/kde2/bin/startkde  to adjust  KDE1 directory  structure to\npoint to  /opt/kde1 instead of /opt/kde  - you'll need this  for KDELIBS\nwhich apparently matters only to KDE1 applications.\n\n3)  Now  you  can  edit  /opt/kde2/share/config/kdmrc  to  your  heart's\ncontent.  Keep in mind  that NoUsers list used to be  separated by ';' -\nnow it's separated by ','.\n\n4) Welcome to the working KDE2 setup where all of your KDE1 applications\n(kpilot in my case) still work. ;^)\n\n--\nET."
    author: "Paul C. Leopardi"
  - subject: "Re: CVS tagged KDE_2_0_RELEASE"
    date: 2000-10-05
    body: "Knapster 0.9 for 1.1.2 runs well with my KDE 2.0 RC1,\nusing SuSE 6.4. binaries."
    author: "Christoph Monig"
---
Matthias Elter, the KDE 2.0 release coordinator, has officially <A HREF="http://www.kde.com/maillists/show.php?li=kde-core-devel&m=317175">announced</A> that KDE has been tagged for the 2.0 release in CVS!  Things are moving on schedule for an Oct. 16 release date.  Matthias noted that the KDE 2.0 release will be based on the forthcoming <A HREF="http://www.trolltech.com/">Qt</A><SUP><SMALL>TM</SMALL></SUP>-2.2.1.  Got my compiler all ready and rearin' to go!


<!--break-->
