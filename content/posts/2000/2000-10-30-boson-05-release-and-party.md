---
title: "Boson 0.5 Release and Party"
date:    2000-10-30
authors:
  - "badler"
slug:    boson-05-release-and-party
comments:
  - subject: "Re: Boson 0.5 Release and Party"
    date: 2000-10-30
    body: "I am really impressed. The game is already playable, it only needs a lot of small features\nto be played by real players. So the next version should be the right one. And the graphism are excellent, congratulation to the boson team."
    author: "Philippe Fremy"
  - subject: "Re: Boson 0.5 Release and Party"
    date: 2000-10-31
    body: "Gosh, I compiled from source -- no pains. Then I found another person to play who had a server set up -- no pains. And then the actual game is <b>sooo</b> cool. Except, one can't farm oil. That is the only really screwed up. And ships are really good at killing their own kind :)\n<p>Jason"
    author: "Jason Katz-Brown"
---
On Monday, October the 30th, the real-time, multiplayer strategy game <a href="http://boson.eu.org/">Boson</a>
will be released in its newest version, 0.5. Although the game's playability
is still quite limited, this release represents a major milestone since it now is based on QT 2.2.1 & KDE 2.0 and features many
enhancements plus new graphics. The Boson Team hereby invites you to our IRC release party on the
date mentioned above at around <B>1800 GMT</B> on channel <B>#boson</B> in the Open Projects
IRC network (i.e. <B>irc.kde.org</B>). If you'd like to learn more about the project or how to help out or simply watch the celebrations, this is the place to drop by!  Click <a href="http://boson.eu.org/screenshots.html">here</a> for some Boson eyecandy.