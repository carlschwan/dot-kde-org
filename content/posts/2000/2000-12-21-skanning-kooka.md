---
title: "Skanning with Kooka"
date:    2000-12-21
authors:
  - "Dre"
slug:    skanning-kooka
comments:
  - subject: "Looks nice"
    date: 2000-12-21
    body: "Cool, now if only I didn't have a paralell scanner : ("
    author: "Ash"
  - subject: "Re: Looks nice"
    date: 2000-12-21
    body: "But some parallel scanners are supported by SANE, so they shoud work with Kooka<br> Have a look at http://www.buzzard.org.uk/jonathan/scanners.html"
    author: "Andrea Cascio"
  - subject: "Re: Skanning with Kooka"
    date: 2000-12-21
    body: "Looks great!  Will it be integrated into the KParts framework?  How cool would it be to be in KWord and have a button to scan in a document and have it OCR'd and then opened in KWord for correction and editing?  All without leaving KWord!\n\nKeep up the great work! I look forward to hearing more about this project... Now, maybe if I could just find the $ to get a scanner :)"
    author: "Tom Philpot"
  - subject: "Re: Skanning with Kooka"
    date: 2000-12-22
    body: "How about a system in kio? <BR>\nscanner://1 would result in a directory with two files \n(for the scanner #1)\n<ul>\n<li>A file containing the scanned file as a graphic\n<li>A file containing the scanned file as ASCII text\nThat way you could open it with ANY kde app!\n<BR><BR>\nAlso, how about OCR support in other\nplaces. Say, download an image online, run it\nthrough a KDE prog, and get text."
    author: "KDEer"
  - subject: "Re: Skanning with Kooka"
    date: 2000-12-23
    body: "There were discussions about that already. I dont like the idea to have just one image in a directory scanner:/1, because one image is not enough. Should it always be the last scanned image or should it be scanned on the fly ?\nYou mostly need more than one try to get a cool scan result. Thats why I favour a scan application more than a kind of filesystem.\n<P>\nKooka saves your scan results automatically into a 'special' directory. Future releases will be able to save descriptions like date, caption etc. beside the images.\n<P> \nI would like the idea that scanner:/1 leads you to that directory where you are able to manage your image pool. And that's hopefully fairly easy, because that should only be just a symlink to Kooka's image save dir or a bookmark ;-)"
    author: "Klaas Freitag"
  - subject: "Re: Skanning with Kooka"
    date: 2000-12-21
    body: "Hi. Looks really cool, I like it. 2 hours ago, I just finished my university project on Handwriting OCR. As far as I have seen on the web page, gocr, the OCR engine, does not use neural networks but a more classic approach. I would like to dig into the source but I just started my thesis -- so little time :-(\nChris"
    author: "Christian Naeger"
  - subject: "Re: Skanning with Kooka"
    date: 2000-12-27
    body: "This description sounds indeed very promising.\nA special point of interest for me lies in image storage. Kooka seems to be prepared to handle extensive scan jobs. (By the way: are automatic document feeders supported?). <p>\nAutomatic generation of file names can be a good aid, but another option would make it even better: \nSemi-automatic embedding of metadata. There are a few approaches of handling metadata availabe right now:\n<ul>\n<li>IPTC-Codes, embedded directly into some types (e.g. jpeg, tiff) of image-files, see <a href=\"http://www.cepic.org/iptc.htm\">http://www.cepic.org/iptc.htm</a>. IPTC is very polular right now in professional image management systems.\n<li>(PHOTO-)RDF, wich <b>can</b> also be embedded, see <a href=\"http://www.w3.org/TR/photo-rdf/\">http://www.w3.org/TR/photo-rdf/</a>\n</ul>\nIt would be really a relief for document storage and retrieval, if the handlig of metadata would available in KDE-Applications. Kooka could mark the start, pixie, konqueror etc. to follow."
    author: "Bernhard Vornefeld"
  - subject: "Re: Skanning with Kooka"
    date: 2000-12-27
    body: "Thank you very much for the links.<P> Yes, I tried to design kooka to handle mass scanning with approbiate scanners. SANE offers drivers for Fujitsu and Bell and Howell by now, maybe that improves ?<P>ADF-Support depends on SANE. If a SANE driver supports ADF, kooka also should. There is already something coded (see massscandialog.cpp), but not yet finished and tested, because I do not have an ADF (yet).\n<P>\nAutomatic file name generation is just a starting point. The interface to the object, which stores the images, was designed as 'slim' as possible to allow the implementation of storage objects as required: Handling XML, database connections or whatever. Barcode and/or form recoginition for metadata generation should also be possible."
    author: "Klaas Freitag"
  - subject: "Re: Skanning with Kooka"
    date: 2000-12-29
    body: "I have been looking for this for a while. Thanks.\nAny chance that you may include an interface to the HANDWRITTEN recognition system from NIST."
    author: "Hannes Kruger"
  - subject: "Re: Skanning with Kooka"
    date: 2000-12-29
    body: "Hannes, I dont know what NIST is, but handwriting recognition sounds very interesting to me. Is there any open source software existing already?"
    author: "Klaas Freitag"
  - subject: "Re: Skanning with Kooka"
    date: 2001-01-01
    body: "The NIST Public Domain OCR System Release 2.1 is at:\n\nhttp://www.itl.nist.gov/iaui/894.03/doc/doc.html\n\nIt recognizes handprint characters only (in german: Druckbuchstaben). Perhaps it can be included in the Recognition engine. Chris"
    author: "Christian Naeger"
  - subject: "Re: Skanning with Kooka"
    date: 2001-04-24
    body: "This is just what I'm waiting for.  I'm writing a little KDE app that requires scanner support.  Will Kooka work as a Kparts plugin type thing?  I need to be able to call it to import the image into my app.  \n\nDoes Kooka have auto selection of the media being scanned?  (if you insert a 4x5 picture, will it autoselect the picture at the edges, or do you have to do that manually?)"
    author: "Jay Austad"
  - subject: "Tif scanning in Kooka"
    date: 2004-11-17
    body: "I was just wondering about why the ability to save as a tif file hasn't been included in kooka ?\nYou can scan in binary/b&w mode but what format can it be saved in as a b&w image file ?\n\nThe scanimage command has the option to output as tif.\n\nA great job has been done on the app, with a lot of progress.\n\nG4 compressed tif files are small and can be at 600dpi, quite easily.  I \nused to work for a company supporting professional printing packages that work \nwith the G4 tifs so because of size and quality.  A 600dpi uncompressed \nbinary tif, can be 4.2M, a greyscale tif is 34M so when a G4 compressed tif \nis around 90K, it is a huge difference in size.  G3 compression reduces it \nto about 200K. A 600dpi pnm file is also 34M, binary is 4.1M, Jpeg greyscale \nis 3.2M, binary is about 1.3M.  A long way from 90K, so unless for license \nrestrictions, it seems silly to me, not to be able to save as a G4 \ncompressed tif from xscanimage or kooka, hence the script.\n\nI found a way for this script to work with whichever sane scan device you \nmay have, though not perhaps the most beautiful, it works and I adapted it \nto output to G4 compressed pdf to.  2 scripts which I saved to my /usr/bin \ndirectory.\n\n---------------------------------------------\n\ntifscan.sh\n\n#!/bin/sh\n\nusage()\n{\necho \"Usage: tifscan {nameofimage.tif}\"\n}\n\n#test to see if a filename has been entered\nif [ $# -lt 1 ] ; then usage ; exit 1 ; fi\nname=$1\n\n#Read output of help command to get scanner device name\nscanner=`scanimage --help | tail --lines=1 `\n\necho Now scanning your A4 document on $scanner\n\n#scan the A4 binary(b&w) file uncompressed at 300dpi to temporary file\nscanimage -d $scanner --mode binary --resolution 300 --quick-format A4 \n--format tiff >temp-$name\n\n#Use tif utility to convert the temporary binary tif to a G4 compressed tif \nand then delete the temporary file\ntiffcp -c g4 temp-$name $name\nrm -f temp-$name\n\n# display resulting G4 tiff file\nkfax $name\n\n----------------------------------------------\n\npdfscan.sh\n\n#!/bin/sh\n\nusage()\n{\necho \"Usage: pdfscan {nameofimage.pdf}\"\n}\n\n#test to see if a filename has been entered\nif [ $# -lt 1 ] ; then usage ; exit 1 ; fi\nname=$1\n\n#Read output of help command to get scanner device name\nscanner=`scanimage --help | tail --lines=1 `\n\necho Now scanning your A4 document on $scanner\n\n#scan the A4 binary(b&w) file uncompressed at 300dpi to temporary file\nscanimage -d $scanner --mode binary --resolution 300 --quick-format A4 \n--format tiff >temp-$name.tif\n\n#convert to pdf with G4 compression\ntiff2pdf temp-$name.tif -p A4 -q G4 -o $name\n\n#display pdf\nkghostview $name\n\n----------------------------------------------\nChris\n\n"
    author: "Chris"
  - subject: "Re: Tif scanning in Kooka"
    date: 2004-12-06
    body: "Problem: \nThe script didn't work with the 2.4 kernel, because of the way USB detects the scanner.\nIn 2.4 kernel 3 devices are listed for my 1 scanner\n\nepson:/dev/usb/scanner0 epson:/dev/usbscanner0 epson:/dev/usbscanner\n\nwhereas under 2.6 only one scanner is listed.\n\nResolution:\n\nSo swap the line which says :\nscanner=`scanimage --help | tail --lines=1 `\n\nwith\n\nscanner=`scanimage --help | sed -e 's/ /\\n/g' | tail --lines=1 `\n\nor even better\n\nscanner='scanimage -f %d | sed -e 's/0/\\n/g' | tail --lines=1 '\n\nwhich should work on 2.4 or 2.6 kernel, plus hopefully others\n\nRegards\n\nChris\n\n"
    author: "Christopher Booth"
  - subject: "Re: Tif scanning in Kooka"
    date: 2005-11-28
    body: "Respected Sir,\nI am Dr.V.Shivakumar Sharma writing to you from India, Karnataka State, Mysore City.  I saw your site, found it highly intersting and valuable information also.  \nPlease furnish me some details for my personal usage:\n1. I need a software to compress my researched pdf results, they are occupying a lot of    \n    space.\n2. I am facing a lot of problems for keeping my pictures in the tiff formats also. they are also occupying a huge amount of space.\n\nCan u please let me know the solution for the above problems and hence the reduce the size for the pdfs and the tiffs and help me Sir.\n\nHope to do a healthy and longstanding longstanding business relationship with Professionals lilke You Sir.\n\nDr.V.shivakumar Sharma\nDirect: +919845120010"
    author: "Dr.V.shivakumar Sharma"
  - subject: "Re: Tif scanning in Kooka"
    date: 2006-03-22
    body: "Okay - I have built some improvements on this script, turning it into a small bit of perl.\n\n--------------------------------\n#!/bin/perl\n# pdfscan, adapted from post by Christopher Booth, 2004\n# Adapted by Danny Staple, 2006\nuse Term::ReadKey;\n\nsub usage()\n{\n  print <<USAGETEXT\nUsage: pdfscan {nameofimage.pdf}\\n\npdfscan will use the default scanner (in a single scanner set up)\nand scan to a PDF file. Warning - these are big memory operations!\nUSAGETEXT\n;\n  exit();\n}\n\nsub user_has_more()\n{\n  print \"More to scan (y/n)?\\n\";\n  my $key;\n  do {\n    ReadMode 'cbreak';\n    $key = ReadKey(0);\n    ReadMode 'normal';\n    if($key eq 'y' or $key eq 'Y') {\n      return 1;\n    }\n  } while ($key ne 'n' and $key ne 'N');\n  \n  return 0;\n}\n\nmy $outputname = $ARGV[0] or usage();\n\n#Read output of help command to get scanner device name\nmy $scannerdevice =`scanimage --help | tail --lines=1 `;\nchomp($scannerdevice);\n\nprint \"Now scanning your A4 document on $scannerdevice\\n\";\n\n#scan the A4 file uncompressed at 300dpi \n#--quick-format A4 - Note we are creating a temp file until we can find a way\n#to get tiff2pdf to take standard input\n\nmy $count = 0;\nmy $cpargs=\"\";\ndo {\n  print \"scanning....\\n\";\n  $count ++;\n  my $imagedata = `scanimage -d $scannerdevice --mode Color --resolution 300 --format tiff >temp-$outputname-$count.tiff`;\n  $cpargs = \"$cpargs temp-$outputname-$count.tiff\"\n\n} while(user_has_more() == 1);\n\nprint \"stitching...\\n\";\n`tiffcp $cpargs temp-$outputname-all.tiff`;\n\n#Look at multiple page scans - either from a preset parameter, or an interactive prompt, using the tiffcp command\n\n#Convert to a B&W tiff as well, and pass through ocr. Filter out non-dictionary \n\nprint \"outputting\\n\";\n#convert to pdf with jpeg compression - pass in our image stream\nprint `tiff2pdf temp-$outputname-all.tiff -j -p A4 -o $outputname`;\n\nfor my $i (1..$count) {\n  unlink(\"$temp-$outputname-$i.tif\");\n}\nunlink(\"temp-$outputname-all.tiff\") or print (\"Failed to remove output file\\n\");\n#display pdf\n#kghostview $name\n--------------------------------------------\nI am sure it could still be done in sh, and there are comments with stuff I may do later. If there is no objection from Chris, I may pop this onto berlios as an open source project. I have planss for this - meanwhile, it is now the core of my document scanning.\n\nDanny\nhttp://orionrobots.co.uk"
    author: "Danny Staple"
  - subject: "Re: Tif scanning in Kooka"
    date: 2006-03-22
    body: "How annoying - the posting system removed all my indenting. And there was me thinking posters just neglected it...\n\nThere is a bug there - the $temp on the unlink in the loop should actually just be temp, no dollar sign.\n\nDanny"
    author: "Danny Staple"
---
<A HREF="mailto:torsten at kde.org">Torsten Rahn</A> is ebullient about a
KDE program for scanning using
<A HREF="http://www.mostang.com/sane/">SANE</A> called
<A HREF="http://apps.kde.com/nfinfo.php?vid=1673">Kooka</A>.
<EM>"This is a real
nice productivity-app proving that it's easy to create extremely useful
apps for KDE 2 with relatively little work."</EM>
Details below.


<!--break-->
<P>
<H4>Kooka</H4>
</P>
<P>
Kooka is a raster image scan program for the KDE2 system.
</P>
<PRE>
   PLEASE READ THE FILE "WARNING" FIRST !
   Using kooka may damage your hardware !
</PRE>
<P>
It uses the <A HREF="http://www.mostang.com/sane/">SANE libraries</A> and
the the <A HREF="http://apps.kde.com/nfinfo.php?vid=1672">KScan library</A>,
 which is a KDE 2 module providing scanner access.
</P>
<P>
KScan and Kooka are under construction. Don't expect everything to work
fine. If you want to help, send patches to freitag@suse.de.
</P>
<P>
<H4>Screenshots</H4>
</P>
<P>
The best way to describe an app is often with screenshots:
<UL>
<LI><A HREF="ftp://ftp.kde.com/pub/screenshots/Kooka/kooka1.png">Previewing
a scanned document</A>;</LI>
<LI><A HREF="ftp://ftp.kde.com/pub/screenshots/Kooka/kooka2.png">OCR
configuration dialog</A>; and</LI>
<LI><A HREF="ftp://ftp.kde.com/pub/screenshots/Kooka/kooka3.png">OCR
results</A>.</LI>
</UL>
</P>
<P>
<H4>Features</H4>
</P>
<P>
Kookas main features are:
</P>
<UL>
<LI>SANE</LI>
<UL>
<LI>Scanner support using SANE. Kooka <EM>does not</EM> support all
features that SANE and its backends offer. It takes a small subset of the
available options.</LI>
<LI>Kooka offers a GUI to change the most important scanner options like
resolution, mode, threshold etc. These options are generated on the fly,
depending on the scanner capabilities.</LI>
<LI>Kooka offers a preview-function and the user can select the scan area
interactively.</LI>
</UL>
<LI>Image storage</LI>
<UL>
<LI>Kooka provides an assitant to save your acquired images.</LI>
<LI>Filenames are generated automatically to support multiple scans.</LI>
<LI>Kooka manages the scanned images in a tree view where the user can
delete and export images.</LI>
</UL>
<LI>Image Viewing</LI>
<UL>
<LI>Scanned images can be viewed by clicking them in the tree view.</LI>
<LI>The viewer has a zoom function.</LI>
</UL>
<LI>OCR: Kooka supports Joerg Schulenburg's
<A HREF="http://jocr.sourceforge.net/">gocr</A>, an open source
program for optical character recognition. Kooka starts the OCR program
and displays its output. Best results with bw-images scanned with ~150 dpi.</LI></UL>
</P>
<HR WIDTH="90%" SIZE=1>
<P>
Kooka is being maintained by <A HREF="mailto:freitag at suse.de">Klaas
Freitag</A> and can be found in kdenonbeta together with KScan.
</P>

