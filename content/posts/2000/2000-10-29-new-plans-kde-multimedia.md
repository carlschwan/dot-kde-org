---
title: "New Plans for KDE Multimedia"
date:    2000-10-29
authors:
  - "numanee"
slug:    new-plans-kde-multimedia
comments:
  - subject: "Re: New Plans for KDE Multimedia"
    date: 2000-10-29
    body: "Does this mean there will be a media player for KDE similar to Microsoft's Media Player or RealPlayer?"
    author: "Anonymous"
  - subject: "Re: New Plans for KDE Multimedia"
    date: 2000-10-29
    body: "It would be great to have a high performance Multimedia player in Linux! audio and video.\n\nI remember when I would be playing an mp3 and I would get multiple icq messages during the song after the song was over all of the icq sound events would play one after the other, it was terrible, at least it doesn't do that anymore."
    author: "L.D."
  - subject: "Re: New Plans for KDE Multimedia"
    date: 2000-10-30
    body: "What about esound?"
    author: "Anonimous"
  - subject: "Re: New Plans for KDE Multimedia"
    date: 2000-10-30
    body: "esound doesn't hold a candle for arts. Well, except for the performance part, but optimization is always one of the last step in a project.  Esound is very minimalistic, so they it achieved that state rather quickly :)"
    author: "Charles Samuels"
  - subject: "Re: New Plans for KDE Multimedia"
    date: 2000-10-29
    body: "<p>It would be nice if KDE 2 also had an mpeg player that worked as well as zzplayer!</p>\n\n<p>As for the rest, I may have to get involved. I'm thinking of the A/D boards bringing in the mics and instruments, striping the MIDI, laying down the tracks, automixing and burning the CD. Look for it on mp3... <i>Old guy scortches subwoofer with KDE</i> on a new open sound license ;)</p>"
    author: "Eric Laffoon"
  - subject: "Re: New Plans for KDE Multimedia"
    date: 2000-10-29
    body: "What about the great avilib available at euro.ru/~divx which uses wine code to run DivX and other codecs available under windows. Would there be any legal issues with this since there seems to be some reverse engineering involved?\nEven Quicktime should be able to be incorporated this way. What I'd like to see is a player being able to handle all sorts of media and with a interface consistent with the KDE look. I have been thinking about writing something like this myself, I really don't care about skins and stuff."
    author: "tritone"
  - subject: "Re: New Plans for KDE Multimedia"
    date: 2000-10-29
    body: "<i>What about the great avilib available at euro.ru/~divx which uses wine code to run DivX and other codecs available under windows.</i>\n<br><br>\nInstall the full <a href=\"http://mpeglib.sourceforge.net\">mpeglib</a>\nand kaiman and noatun will be able to play DivX!"
    author: "gis"
  - subject: "A standard multimedia infrastructure..."
    date: 2000-10-29
    body: "Fact: Multimedia is currently a major weakness for Linux (and most Unices).\n\nNeither KDE or GNOME have made much progress on integrating real multimedia support into their desktop environments. Surely, this would be a good time to make use of the KDE-GNOME discussion list? A standard multimedia architecture would save an awful lot of duplicated work. It would mean that CODECs would only need to be written once.\n"
    author: "Julian Regel"
  - subject: "Re: A standard multimedia infrastructure..."
    date: 2000-10-29
    body: "i agree"
    author: "AC"
  - subject: "Re: A standard multimedia infrastructure..."
    date: 2000-10-30
    body: "The gnomes have been cooperating with Stefan Westerfeld.  I'm not sure how much, but they will use aRts in some way or another.  See our mailing lists.\n\nWith the gnome's help, arts will become better quickly"
    author: "Charles Samuels"
  - subject: "Re: A standard multimedia infrastructure..."
    date: 2000-10-30
    body: "So you read the article and came to the same conclusion as the author ?\n<P>\nCool.\n<P>\nThe AC above read your post and gave a response worthy of AOL ?  \nNot cool."
    author: "Forge"
  - subject: "Some CPU optimization is needed"
    date: 2000-10-30
    body: "Comeon, how can you be taken serious as a player, if kaiman/artsd (or noatun) use 7% of the CPU power of my Athlon, while xmms does not even show in xosview (<1%)\n\nThis is one point I would like to see improved, the handling for the most common case: One sound file is being played.\n\nAnother thing I would like to see is a GUI plugin for xmms skins and a crossfade effect for song changes. Should be quite easy to do with artsd."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Some CPU optimization is needed"
    date: 2000-10-30
    body: "CPU optimization is a concern, we'll get there, but aRts is still maturing.  Give us time :)\n\nWinamp/xmms skins on the other hand, well, there is a person already writing a skin loader for noatun:\n\nhttp://noatun.derkarl.org/shots/winamp.png\n\nIt's not released to the public because it's not complete enough (the Kjofol skin loader took at least a month before I was confident enough to put in the cvs)"
    author: "Charles Samuels"
  - subject: "Re: New Plans for KDE Multimedia"
    date: 2000-10-30
    body: "I'd like to know whether the ogg / vorbis \neffort (http://www.ogg.org) would fit in here, and how. \n<br><br>\nRegards,\ncm."
    author: "Christian Mueller"
  - subject: "Re: New Plans for KDE Multimedia"
    date: 2000-10-30
    body: "OggVorbis support seems to be completely stable and working.  All of us like OggVorbis very much in fact.\n\nBTW, it doesn't work off the CVS, you'll need to install libvorbis and the \"kmpg_vorbis\" plugin for it to work.  there's a readme in the source somewhere (for mpeglib_artsplug)"
    author: "Charles Samuels"
  - subject: "Re: New Plans for KDE Multimedia"
    date: 2000-10-31
    body: "Does this mean there will be a media player for KDE similar to Microsoft's Media Player or RealPlayer?"
    author: "Anonymous"
  - subject: "Re: New Plans for KDE Multimedia"
    date: 2000-10-31
    body: "Does this mean there will be a media player for KDE similar to Microsoft's Media Player or RealPlayer?"
    author: "Anonymous"
  - subject: "ID3 tags with different languages/charsets"
    date: 2000-10-31
    body: "I have a small and stupid problem, that is probably not worth any work, but perhaps someone will have a simple and good idea :-).\nI have MP3s with ID3 tags in different encodings (ISO8859-1 and 2). It would be nice to see something like unicode here as well, but 30 chars are not enough. Of corse, the new revisions of ID3 do not have such problems, but what with the old files? Will I have to re-burn my CD\u00b4s again with new ID3 tags in order to get correct characters in the future?"
    author: "Pavel Vondricka"
  - subject: "We really need esound + arts integration"
    date: 2000-11-04
    body: "What we really need is a esound compatibility for arts (or the other way around it, it does not matter for me)\n\nThere are many nice games or programs like xmms which have to fight with arts to support sound on kde2. But there is already esound which provides one standard. Either gnome or kde people have to take care to integrate both together. The first environment providing integration will have better chances to be the environment of choice for at least me in future.. I am currently switching between both environments and like them both very much!!"
    author: "Dietmar Schnabel"
---
Stefan Westerfeld has <a href="http://lists.kde.org/?l=kde-multimedia&m=97276750721961&w=2">posted</a> a first draft of cool new plans for the new multimedia architecture for KDE2, based on aRts (the "analog realtime synthesizer"). Plans include merging the two existing media players (noatun and kaiman), new media types, infrastructure improvements, improved MIDI and more.  The full draft (edited by our own <A HREF="mailto:pour@mieterra.com">Dre</A>) is attached below.




<!--break-->
<h2>KDE2.1 Multimedia Plan (draft)</h2>
<p>
KDE 2.0 was quite a step for multimedia on the UNIX&reg; desktop. aRts, which has been integrated into KDE 2 over the past year, provides an always running soundserver and service-type media-playing. The notification system, backends and the
media player were completely rewritten.  As a result users have a more powerful platform
for multimedia tasks.
</p>
<p>
Still, there remains a lot to be done. This document tries to provide a
planning and discussion base for coordinating "the way to go". The focus
lies on what can be achieved for KDE 2.1.
</p>
<p>
Discussion by the community is highly welcome. Also, please consider contributing to further development.
<p>
This draft has spontaneously been designed on IRC, by
</p>

<ul>
<li>Charles Samuels
<li>Stefan Schimanski
<li>Stefan Westerfeld
</ul>

<h3>Overview</h3>

<ol>
<li>The KDE Media Players
<li>New media types
<li>Organization & Documentation
<li>Midi/Sequencer
<li>Not in 2.1
</ol>

<h3>1. The KDE Media Players</h3>

<h4>1.1 Merging noatun and kaiman</h4>
<p>
Kaiman and noatun are two seperate media players available
for KDE2.0.  Kaiman was shipped with 2.0, and noatun is available via CVS. The plan
for 2.1 is to combine these players into one media player, the official "KDE Media Player".
</p>
<p>
Technically, the KDE Media Player will be based on noatun's plugin architecture.  Kaiman would be
just one player making use of the noatun technology.
</p>
<h4>1.2 Pluggable effects</h4>
<p>
aRts allows much more than vanilla playback: filters can be used to affect the sound. Currently, this feature has been mostly unavailable
in media player(s), mostly for two reasons:
</p>
<ol>
<li>there were few effects; and
<li>the binding of a GUI to effects was not yet implemented.
</ol>
<p>
The fix for (1) seems obvious: write more effects. One starting point could
be porting the <A HREF="http://home.onet.co.uk/~jzracc/plugins/index.htm">FreeVerb</A> code to the aRts architecture.
</p>
<p>
The fix for (2) is less obvious. The problem is forumlating a method for GUI objects (which run in the player process) to connect to the
effects (which run in the sound server process). The clean way to do this
is to extend MCOP (the inter-process communication architecture used by aRts) to provide a signals & slots technology.
</p>
<p>
An example best illustrates this point.  Say for instance there is a GUISlider aRts object, which
runs in the player process space, and a ExtraStereo effects plugin, which runs in
the aRtsd process space. The idea is to be able to write:
</p>
<blockquote><tt>
  connect(slider,"value_changed",effect,"depth");
</tt></blockquote>
<p>
and thereby cause a connection between the slider and the effect. Conversely, the same mechanism can also be used to connect output GUI elements to
effects.
</p>
<p>
Besides replacing the need to poll, this approach also allows the GUI to have a flexible deisgn, using the same modular approach that artsbuilder
currently provides for sound.
</p>
<h4>1.3 Moving the tag reading code</h4>
<p>
The ID3 tag reading code, which provides information such as title and author from .mp3 files, is currently a noatun plugin.  It should be made an aRts components to make it available to all applications using aRts.
</p>

<h3>2. New media types</h3>

<h4>2.1 mikmod</h4>
<p>
Work to make an aRts PlayObject for mikmod files is currently progressing.  This
would allow the KDE Media Player to play .xm, .s3m, .mod and other digital audio codecs.  The clear advantage is that the component would be available to all other apps which know how to talk to aRts.
</p>
<h4>2.2 MIDI via timidity</h4>
<p>
Another great project would be to make a timidity plugin for aRts. Timidity is a
package which renders MIDI notes to audio output using sampling. Currently kmidi is the KDE MIDI player. Converting kmidi to an aRts PlayObject would again
provide the user with a much more consistent experience and provide the playback capabilities to all applications aware of aRts.
</p>

<h3>3. Organization & Documentation</h3>
<p>
<h4>3.1 KDE multimedia webpage</h4>
<p>
KDE multimedia development needs to be more visible. There is no official
coordination point where people can see the development process and design overviews. To address this shortcoming, a KDE multimedia webpage (how does http://multimedia.kde.org sound?)
should be created. 
</p>
<p>
Charles has volunteered to take care of this.
</p>
<h4>3.2 aRts documentation</h4>
<p>
Fortunately the multimedia chapter of the <A HREF="http://dot.kde.org/972566642/">KDE 2.0 Development book</A> should
become available to the public RSN. While this is a significant piece of
good developer documentation and a great start, more work
needs to go into a consistent documentation on http://www.arts-project.org,
for both developers and users.
</p>
<h4>3.3 kdelibs/arts -> arts</h4>
<p>
Packaging aRts seperately is probably a good idea. aRts itself by design does not
depend on Qt or KDE. People outside the KDE world should
still be able to use aRts as a sound server or for music tasks.
</p>
<p>
If - for instance - GNOME would start using aRts, having aRts mixed in the
CVS and in packaging with KDE is probably a bad idea.
</p>

<h3>4. Midi/Sequencer</h3>

<h4>4.1 Usability issues</h4>
<p>
aRts in the CVS already provides MIDI realtime synthesis. You can use midisend
to play instruments in artsbuilder. You can also combine it with <A HREF="http://lienhard.desy.de/mackag/homepages/jan/Brahms/">Brahms</A> or
artstracker to compose songs. The problem is that if you are not
a rocket scientist or don't study the code or collected READMEs for a while, you
will probably not manage to do it. That has to change.
</p>
<p>
A good start would be providing an instrument manager, where you can
graphically assign which MIDI channel should get which instruments, without
bothering with the details.
</p>

<h4>4.2 Interoperability</h4>
<p>
There are at least three sequencer-like programs which actively want to talk
to aRts:
</p>
<ul>
 <li>Brahms, which has been in the CVS for some time now.  Jan, it's author, is
working on a new version right now - some aRts to MIDI support is already in place.
<li>Anthem is in its infancy.  Its author Pete definitely wants to have
audio stuff like hd recording, and probably aRts to MIDI support should
be added as well.
 <li>ArtsTracker is an experimental small tracker in kmusic.
</ul>
<p>
Especially the areas
</p>
<ul>
 <li>connecting to aRts
 <li>finding (virtual) MIDI channels and synthesis instruments
 <li>assigning instruments to channels
 <li>sending timestamped events
</ul>
<p>
need to be standarized more for MIDI. Other interesting endeavors include:
</p>
<ul>
 <li>harddisk recording; and
 <li>effect processing
</ul>
<p>
Most of this only needs a small amount of code in aRts and the applications,
the problem is finding a sophisticated standard.
</p>

<h3>5. Not in 2.1</h3>
<h4>5.1 Media interfaces</h4>
<p>
aRts supports streams between objects. Besides the typed (float) audio streams,
it also supports untyped byte streams. These streams need to be extended to support notions of:
</p>
<ul>
 <li>which encoding does the stream have?
 <li>what is the size of the frames?
 <li>which streams (audio/video) belong together?
</ul>
<p>
Addressing these issues and others would make a good step towards further modularizing audio/video
codecs. It would also allow things like connecting from a decoder to a
renderer without knowing which conversions need to be done to make them
the same format (a connection manager could find a suitable chain of filters
to plug in there). Also http and other forms of streaming could be supported.
</p>
<p>
Not in 2.1 however.
</p>

