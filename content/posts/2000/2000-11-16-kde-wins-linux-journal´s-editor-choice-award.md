---
title: "KDE wins Linux Journal\u00b4s Editor Choice Award"
date:    2000-11-16
authors:
  - "Inorog"
slug:    kde-wins-linux-journal´s-editor-choice-award
comments:
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "Congrats K Team!!!!\n\nWho needs a PR League, just keep piling on the awards..  :-)\n<br>\n<br>\n\n\n\n-jorge<br>\nhttp://www.whiprush.org"
    author: "jorge o. castro"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "[sigh]\n\nThese are getting boring.  We seem to get one every other day."
    author: "Charles Samuels"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "Let's not forget what KDE means for millions of people. If KDE wants to keep its position it has to maintain as stable and professionally designed as before. This means getting those last(many) bugs in KDE-2.0.0 squashed. This means focusing on design, user-interface, modularity first and maybe \"making it look cool\" second. Is it f.ex. possible today to make KDE-2.0 apps talk to CORBA components, EJB's etc...?\n\n--\nAndreas Joseph krogh <andreak@nettverk.no>"
    author: "Andreas Joseph Krogh"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "Congrats n' all, but KDE 2 is still riddled with bugs, Konqueror especially has major problems. It was my hope that these would be largely ironed out in 2.01, however, par a discussion with some kde developers yesterday, it seems that this will not be the case. Indeed it was suggested that the remaining bugs will not be resolved for some time.\n\nI must admit to being somewhat puzzled. Why was kde2 'released' when it was clearly not ready to come out of beta. KDE is not a commercial entity and as such 'in theory' should not have to concern itself with beating a 'competitor' to the market place."
    author: "Ross Evans"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "<p>&gt<i>Why was kde2 'released' when it was clearly not ready to come out of beta ?</i><br>\nBecause there was just to many people who couldn't wait any longer ! KDE  was already too much delayed.<br>\nFurthermore, there's not so many bugs. Are you trolling ?</p>"
    author: "GeZ"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "I don't think he is trolling. From time to time I hear complaints that konqui or kde2 in general is very unstable. Probably kind of misconfiguration? I can assure you that I use KDE2 for more than 3 months and konqui runs very stable (max. 1 crash per week)."
    author: "Marco Krohn"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "I think you have right. Most stability problem reported by KDE users on KDE2 could come from configuration.\n\nMy personal experience is:\n- Using pre-compiled rpms of KDE gave me lots of problems. That was on Beta3, but I'm sure most of my problems was coming more from precompiled than beta status.\n- Compiling myself KDE2.0.0 sources, I have now a very stable release and I can use this desktop as my default without any problem. All apps are ok, especially Konqui.\n\nI think KDE should have a look at it, as it could give a bad image of unstability while KDE2 is really a great desktop."
    author: "Nicolas"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: ">>I can assure you that I use KDE2 for more than 3 months and konqui runs very stable (max. 1 crash per week).\n\nThat is unfortunately not _very_ stable. That is reasonable stable. But then again.. we cant complain since we didn't pay for it ;)\n\nTo Ross Evants:\nI don't care whatever there a bugs in there or not. It was important that 2.0 came out, since bugs are found (and fixed) much faster if it becomes a public release. Not saying that alpha/beta/RC versions should be released as stable just to make people find all the bugs. But when the bug-level has reached er sane level there is no good reason why that (huge) part of the community who can't (like me) figure out how to compile from CVS shouldn't help finding the rest of the 'pitty' bugs. After all 10.000 developers can't find all the bugs as fast as 1.000.000 users... right?\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "I have been using the Debian's version of KDE2 since the day the packages came out.\n\nI can tell you one thing: There's little I can't blame on Debian's packaging (and since these are _supposed_ to be unstable, this is not meant as criticizing the .deb packager), or on the 'feature-not-implemented' syndrome (say, Konqueror doesn't support this-or-that).\n\nIn the last months (including the self compiled KDE2 before the .debs) KDE2 has, au contraire, impressed me with the overall stability it has. I still use the catchy phrase \"I reboot windows more often than I reinstall linux\" to lure new people to linux. Using a 2.4.0-test11-pre5, on an 'unstable' distro, with, until recently, phase2 .debs for X4 has proven that things _ARE_ working great, _IFF_ you are using a distro that knows what packaging means. (Sorry to all {other-distro} lovers)\n\nIn this case, I really feel the RPMs are to blame, or the RPM-packager, or maybe even the underlying distro (RH7 anyone?) for the overall stability of KDE.\n\nConclusion: Our main competitor out there (Windows) is _much_ worse than we are at stability.\n\nKDE2 performs well when packaged correctly (or self-compiled, from what I know), and is even faster (on my 133mhz computer) than the other *NIX desktop.\n\nBefore blaming the KDE guys for unstable software, look deeper to see if someone isn't really the cause of the screw-up..."
    author: "Christian Lavoie"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "To me most of the stability problems I experienced were linked to two causes:\n\na) I definately had some old configurations from the beta-versions around. After I got rid of them stability improved.\nb) gcc > 2.95.2. Because of the better diagnostics gcc 2.96/2.97 give I always had this beast around for compiling and on my first try compiling the final KDE 2.0 sources I managed to compile them (mostly) with that experimental compiler. No wonder that it didn't work. \n\nI since recompiled with gcc 2.95.2 and since have only seen one abnormal program termination coming from kmail which was being fed an ugly (microsoftish encoded) thing, I couldn't make heads or tail of...\n\nregards"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "I think that it has a number of features which needed to get onto users desktops and have apps developed for them (QT 2, for example, or the much-improved Konqueror). If they'd waited until everything was stable before encouraging wide-spread release, development of applications and themes for KDE 2 would have lagged behind; as it is, at least the features are /there/, even if they don't work perfectly, and applications can be developed for broad use NOW."
    author: "Adrian Kubala"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-19
    body: "You're right, but I believe the developers when they say a release was necessary. However, I think it should be made clear to new KDE2 users that the current KDE 2.0.0 still contains bugs. Let's be honest, putting KDE 2.0.0 in the \"stable\" directory on the ftp servers is somewhat confusing and may piss off potential new KDE2 users. We don't want that, do we ??"
    author: "Jasper"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "I am impressed with KDE 2.0. It kicks butt!!! To all the trollers, \"Go back to MS BS\". Konqueror is intuitively thought out and works well. Thanks for 2.0 KDE coders.\n\nRich Deeming"
    author: "Rich Deeming"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "I've had to abandon KDE2 due to its apparent extreme instability.  I tend to blame it on Linux-Mandrake rushing out their 7.2 version in time for xmas though.\n\nNonetheless, I haven't had to return to \"MS BS\" (though I still have to use it sometimes at work.)  Instead I typed \n\nlynx -source http://go-gnome.com | sh\n\nand now I run Helix GNOME without a hitch.  I do like KDE's underlying architecture better though, so I hope both Mandrake and the KDE team resolve these issues.  (And let me start up two KDE sessions at once so I can use my machine over VNC, dammit!)  Uh, sorry for that outburst, I've been pretty frustrated lately.\n\nRob"
    author: "raindog"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "Have you actually tried building KDE from scratch? Most of the problems I have been hearing are coming from one of two distros, and yours is one of them. I can't help but suspect the package. I do see some annoyances here and there, but no grave or critical problems. Konq has crashed only once in three weeks, and the same site that crashed it hung Netscape.\n\nIf you don't want to rebuild from scratch, try a package from a different distro.\n\nps.s Your lynx \"solution\" sounds eerily reminiscent of some Slashdot posts from the time of GNOME-1.0, where it was common to see \"I fixed GNOME by installing KDE\"."
    author: "David Johnson"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "I hadn't thought about trying another distro's packages (and neither would a typical end user, in all probability) - but I may just give it a try just because I'm desperate to use kmail. ;)\n\nAnd yeah, the irony of KDE being suddenly less stable than GNOME isn't lost on me.  I agree that it's most likely Mandrake's packages causing the problem - they really rushed it out for the holidays."
    author: "raindog"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "I know that a few people seem to have problems with KDE 2 in Mdk 7.2 but mostly they are niggling problems due to a number of little customizations Mdk did. Of course the worst problems have been from people trying to add the 7.2 rpms to earlier distros. That has been a disaster writing two programs in the same space.\n\nI compiled KDE 2 on Mdk 7.1 and I can't remember how many weeks my desktop has been continuously running or the last time konqueror crashed. (I'm pretty sure it was in the beta) KDE 2 continues to be rock stable.\n\nRegarding running Helix... I have not been a GNOME fan due to it's mass instability that seems to have finally been cured with it's 1.2 release. I loaded the Mdk 7.2 beta on a machine and had a look and while it appeared very solid it did not have  a lot of the functionality of KDE 2. Further, the interface decisions on a number of programs I ran were infuriatingly unintuitive.\n\nWhat I noticed first though was, in my subjective opinion the depressing browns, and monkeys everywhere was a look that turned me off. Of course that is the really puzzling thing. The 7.2 beta I loaded had Helix-GNOME installed... so I'm trying to figure out why someone would say they had to install it on 7.2 when it was already there and selectable at log in???\n\nBTW my friends did not like the KDE 2 implimentation in Mdk 7.2 and they did an install from source in the /opt directory. It seems that anyone who is comforable running Lynx is not put off by \"./configure && make\"."
    author: "Eric Laffoon"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "Actually, while I grew up with the command line myself, that particular lynx command is just the Helix GNOME's web site's single instruction for installing HG.  They tell you to get a root prompt and copy and paste that line, which to me still seems kind of hard for ex-Windows users.\n\nSee, I'm now trying to evaluate Linux desktops with an eye for installing them on non-technical people's desktops -- since at least half a dozen of that kind of person have asked me about Linux recently -- so I'm doing my best to avoid doing things they wouldn't do.  (Obviously the exception is when I write my little applets, but even then I've been trying to write in such a way that people using it won't have to compile anything.  And of course when my machine blows up, as it did when LM7.2 decided to install XFree86 4.0.1 after I'd asked it not to.)\n\nI found a lot of things confusing about HG, but they were similar things to KDE2's changes from KDE1 - how themes are handled, keyboard shortcuts, window manager differences, etc.  I agree the default theme (Sweetpill Eggplant or something like that) is kind of odd.  I eventually settled on a theme that mimics the QNX Photon interface, and it's very easy on the eyes.  (Tried to import it into KDE2 and the theme importer crashed.)\n\nAnd at least in the LM7.2-final ISO's I downloaded, there is no Helix GNOME, just GNOME 1.2 with a bunch of Helix apps and themes tacked on.  You get a Linux Mandrake splash screen, there's no helix-update, etc.  But I installed using the so-called developer option, so who knows, maybe it just decided I didn't need HG.\n\nAt any rate, I realize I'll get a much more stable installation if I build KDE from scratch.  But I can't tell end users -- like my parents -- to build their KDE from scratch, so I'm not going to either.  This means KDE is held hostage to the whims of the individual distros' packagers, but frankly, it needs to be that way if it's going to be at all useful to Windows refugees (and let's not pretend we don't need them.)\n\nKDE could really benefit from an easy installer/updater like Helix has, especially if the KDE one didn't require any command line interaction at all.  I thought I heard Shawn Gordon's people were working on such a thing but that was quite a while ago.\n\nRob"
    author: "raindog"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-17
    body: "Hi,\n\nI'm working for a company which you'll hear lots of it on next month (specially Redhat users which we target our product to - other distro's will follow - it's market share and it's not my decision)\n\nI have worked with BERO from Redhat Germany to make the RPM's for Redhat 6.2 (if you want to see which company I am - type: rpm -qip on the kdegraphics rpm), and I must say that the current RPM's which are avaialable on KDE FTP site for Redhat (most of them are Bero's work) are rock solid! He managed somehow to compile QT and KDELibs with Netscape plugin support without requiring motif even!\n\nSo yes, some packages sucks on other distro's (specially the RPM's which are on Mandrake 7.2), but I suggest for you to look if your Linux distribution got more updated.\n\nYou can check this by looking at the RPM filename - the last number before the .i386 is the release number of that packages. example: kdebase-2.0.0-7.i386.rpm means this rpm is on the 7th version of compiling - which means - many bugs were removed from the package.\n\nEnjoy KDE 2- and to all KDE Developers - keep up the good work!\n\nHetz"
    author: "Hetz"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "Kde sucks. Windows has a much better interfact.\n\nSo much easier to use...."
    author: "Anonymus"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "Interfact? Is that some new great MS invention?"
    author: "Zank Frappa"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-16
    body: "Yes, MS can't come up with a decent GUI for Windoze so they have formulated a new word that's almost the same as interface for the almost-GUI of Windows.\n\nWindows is plain ugly. Horrid 16 color 32x32 icons and nasty fonts. The window furniture (buttons, radio icons, etc) are crap. I could design better ones using Paintbrush. And where's the consistency between applications?\n\nBTW, if KDE \"sucks\", why are you visiting its web site. If you are Bill Gates shouldn't you be visiting Microsoft's website and brown-nosing Gates instead? KDE may be a little buggy (my current installation doesn't even load the desktop (lots of segfaults) - that's what comes from using CVSUP) but it's free and you can't complain. Perhaps you could try writing a non-sucks desktop environment so we can all benefit and stop using all this rubbish that sucks like KDE.\n\nKDE rocks. Keep up the good work guys. It's the best set of programs I've ever used.\n\n(P.S. signing yourself as anonymous doesn't give your negative comments much credence.)"
    author: "jpmfan"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-17
    body: "Ha ha. You just got to go to control panel and select 'use full color icons'. \n\nAnd while you're at it, you might want to put font smoothing on too.\n\nNot gonna see KDE getting that any time now are we?"
    author: "Anonymus"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-17
    body: "I don't see Windows having infinite numbers of panels floating around, do you?\nI don't see Windows have tear-off menus (OK this is a GTK+ thing), do you?\nI don't see powerful themes like window decorations or pixmap backgrounds in Windows, do you?\nI don't see network-transparency in Windows IO system, do you?\nI don't see any stability in Windows, do you?"
    author: "Anonymous"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-17
    body: "Font smoothing? In Windoze 95/98/2000/NT? Yeh, right. Full color icons? Yeh right (again). Obviously you must have a super-duper version of Windows that doesn't actually \"suck\". We could all wait till Whistler comes out in 2198 and then we could all have these marvellous features.\n\nFonts and colors are all very nice but they aren't as important as the underlying OS architecture. Windows is still based on DOS. KDE has some great stuff behind it like kparts and so on. And it's free.\n\n(BTW, it's \"anonymous\", not \"anonymus\". Bill Gates may spell it with only one \"o\" but the rest of us just standard English spelling)."
    author: "jpmfan"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-18
    body: "I bet this guy is now scared of us.\nYou stupid coward!"
    author: "Anonymous"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-17
    body: "<p>Ho ho, I love idiot trollers that can't even spell.\n<p>Grab a dictionary, idiot.  \n<p>While we're on the subject of the Windows interface, it's interesting how Bill & Co basically robbed it from OS/2 (with little bits of MacOS duct-taped here and there).  And even then, they only copied the interface on the surface, missing all the genius that made the Workplace Shell so good.\n<p>The Windows interface is NOT an example of good interface design..it's a hodge-podge mess of ideas superficially culled from all over the place.\n<p>Consider the Start button...does anyone find it convenient to use?  OS/2's desktop-oriented metaphore is much more effective than wading through a stupid menu-maze.  I don't use the K/Foot menu in KDE/GNOME either...both are stupid relics from a Windows era that should be abandoned (in my opinion).  Let's come up with something new, instead!\n<p>However, KDE has a very powerful desktop that allows me to use the desktop-oriented metaphore exclusively...I've not had near as much success with GNOME in this respect (GMC is absolutely wretched at desktop management).  KFM was great for KDE 1.X, and kdesktop is even better."
    author: "Radagast"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-17
    body: "\"I don't use the K/Foot menu in KDE/GNOME either\"\n\nThen use tear-off menus like in Gnome.\nOr use panel lauchers."
    author: "Anonymous"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2003-01-15
    body: "Haha! Yes, the workspace Shell! I have worked with it only once, and there is exactly one thing I remember very good. From the Color Settings, you could Drag a color and Drop it on a button, then only That button got the color and this was even saved. This is soooo great!\n\nYou could paint every OK button you saw Green, and every Cancel-button red. But if you wanted to paint the OK button int he Delete File dialog red, it was also possible!"
    author: "Daan"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-18
    body: "I'll bet this guy lives in Palm Beach County, too."
    author: "Chris Bordeman"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2002-09-30
    body: "Yeah, Windows rules. Long live Bill Gates."
    author: "Mir"
  - subject: "Can Linux users change a website?"
    date: 2000-11-16
    body: "I think someone (not me) should write a letter to the poor guys at the Japanese company Kuken. Their website (www.kuken.com) is so offensive. Poor merry men."
    author: "Torbj\u00f6rn Skolde"
  - subject: "Re: Can Linux users change a website?"
    date: 2000-11-18
    body: "Oh, you dirty norwegians We Swedes think this is the perfect word for the male genitalia."
    author: "A Swede"
  - subject: "Re: KDE wins Linux Journal\u00b4s Editor Choice Award"
    date: 2000-11-17
    body: "yawn...\nThis is getting very boring, actually.\nAlways this award stuff."
    author: "jamal"
---
Linux Journal´s editorial Board <a href="http://www2.linuxjournal.com/advertising/Press/2000ec_winners.html">announced recently the list of winners</a> for the second annual LJ´s Editor Choice Award. KDE wins the <b>Best Desktop Environment</b> category. Notice that this is different from LJ´s Reader Choice Award that KDE won earlier this year.
Congratulations to the KDE community and many many thanks to Linux Journal, for the high quality tribune they produce for us and for this new award.


<!--break-->
