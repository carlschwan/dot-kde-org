---
title: "KOrganizer Feature Update"
date:    2000-10-04
authors:
  - "numanee"
slug:    korganizer-feature-update
comments:
  - subject: "Just one more thing"
    date: 2000-10-04
    body: "Now think about that:\n\nWhat, if I had an additional module in php,perl,whatever\nthat would allow me to modify my calendar on the\nweb, and synchronise the data back to my palm...\n\nWow, thats what I call a good idea.\n\nWhat do you think?"
    author: "Matthias Lange"
  - subject: "Re: Just one more thing"
    date: 2000-10-04
    body: "Maybe a syncronization tool withg exinting internet calendar, such as yahoo, would be very usefull aswell.\n\ncheers and congratulations on the good work done\n\n\nLuli"
    author: "Luiz Masag\u00e3o"
  - subject: "Re: KOrganizer Feature Update"
    date: 2000-10-04
    body: "How hard would it be to extend KOrganizer to allow other KOrg users to see your schedule and book meetings etc (based on security/permissions)?\n<p>\nOr would it be easier to develop a KOrganizer server?\n<p>\nIt would be a useful extension, sort of a \"Groupware\" feature.\n<p>\nRick"
    author: "Rick"
  - subject: "Re: KOrganizer Feature Update"
    date: 2000-10-04
    body: "Actually, that's what Magellan will do. I hope\nthat there will be a project merge between\nMagellan and Korganizer, that would be great."
    author: "David Simon"
  - subject: "Re: KOrganizer Feature Update"
    date: 2001-10-30
    body: "as far as usability and workability is concerned we should reach \na solution which is at least as good as the msfuck exchange thing. \n\nwe need a good groupware server to which the userspace kde-pim-clients \ncan connect. on the userspace client side we need a good and generic \ninterface so we can build serial connects to whatever handheld we want.\n\nonly if there is a good kde groupware solution (not just for the private\ndomain) we will manage to be a serious competitor in the professional \nfield."
    author: "Markus Heller"
  - subject: "Re: KOrganizer Feature Update"
    date: 2000-10-04
    body: "I think the most important thing to think about is the upcoming redundancy as soon as Magellan is released.\n\nI am no coder, but since Magellan is supposed to have a background engine doing all the important database stuff (Magellan), and a frontend (Arkteon, I think ?!?), there probably won't be much that can be merged.\n\nSince both projects are aimed at KDE, it makes me a little sad seeing that so great programmers all doing their own things. But well, I guess thats the way it has to be with OpenSource :o) Choice rocks!\n\nAnd luckily, both use standards like vCards..."
    author: "KernelPanic"
  - subject: "Re: KOrganizer Feature Update"
    date: 2000-10-04
    body: "The last news item on the Magellan site is dated 5/5.  The Kompany did mention that they were working on it but I doubt that we will see anything for a few months.  Hope Magellan is released soon, can't wait to play with it."
    author: "Rick"
  - subject: "Re: KOrganizer Feature Update"
    date: 2000-10-07
    body: "I have been following the Magellan development pretty closely for a rather long time now. I admit that you can only be disappointed by the amount of news the developers publish...\n\nBut they (theKompany) *SAID* there would be a technology preview release in October..."
    author: "KernelPanic"
---
Curious about what KOrganizer 2.0 has to offer?  <a href="mailto:schumacher@kde.org">Cornelius Schumacher</a> has just the thing, with a brand new <a href="http://devel-home.kde.org/~korganiz/korganizer2.html">feature update</a> including <a href="http://devel-home.kde.org/~korganiz/screenshots/main.gif">shots</a> and all.  Amongst other new features, KOrganizer now supports hierarchical To-Do lists, web export and archives. Future plans include group scheduling and synchronization with PDAs.



<!--break-->
