---
title: "DCOP bindings for Java"
date:    2000-10-13
authors:
  - "numanee"
slug:    dcop-bindings-java
comments:
  - subject: "Re: DCOP bindings for Java"
    date: 2000-10-13
    body: "This sounds really interesting (javadcop). I'll have to try it once I find the time to install this on my box. If something useful comes out of it, I'll let you know ...\n\ncu\n\nD\n\n\"Real programmers don't comment their code.\nIt was hard to write, it should be hard to understand.\""
    author: "Dominik Niedermeier"
  - subject: "DCOP and SOAP"
    date: 2000-10-13
    body: "Will there ever be such a thing as kSOAP?"
    author: "Antonie Fourie"
  - subject: "Re: DCOP and SOAP"
    date: 2000-10-13
    body: "Yes probably. There is already kxmlrpc which supports the similar XMLRPC protocol. SOAP is somewhat more complicated, but it will probably be added when someone has time.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: DCOP and SOAP"
    date: 2000-10-14
    body: "Absolutely!  I wrote kxmlrpcd back before SOAP was announced (XMLRPC is a precursor to SOAP) and never got around to updating it.  I'll be working on ksoapd (or whatever) after 2.0."
    author: "Kurt Granroth"
  - subject: "Bad for WORA (Re: DCOP bindings for Java)"
    date: 2000-10-15
    body: "What's the point? If I wanted to write an app with KDE support, I would write a native KDE app, in C++, etc. The whole point of Java is to write cross-platform programs, which are not tied to any particular operating system. Don't get me wrong, I like KDE, but lets not add 'non-WORA' extensions to Java."
    author: "Slava Pestov"
  - subject: "Re: Bad for WORA (Re: DCOP bindings for Java)"
    date: 2000-10-16
    body: "Java is a nice language, making it work well with KDE is the Right Thing (tm) to do.  This only adds to the power of Java, not removes."
    author: "ac"
  - subject: "Re: Bad for WORA (Re: DCOP bindings for Java)"
    date: 2000-10-20
    body: "Because you should have the opertunity to to whatever\nyou'd like.\nAnd if you think component architecture, you can be sure that you once need to communicate with\nan already existing component....!"
    author: "Nils Olav Sel\u00e5sdal"
  - subject: "Should have called it ROBOCOP"
    date: 2000-10-17
    body: "Instead of DCOP, they should called it ROBOCOP as in Realtime OBject Oriented COP!"
    author: "ac"
---
Matthias Hoelzer-Kluepfel recently <a href="http://lists.kde.org/?l=kde-devel&m=97111194307948&w=2">announced</a> Java bindings for <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/dcop/HOWTO?rev=1.16&content-type=text/x-cvsweb-markup">DCOP</a>.  In other words, Java can now communicate with and control KDE2 applications.  A welcome addition to the existing C bindings for DCOP, originally used by the long gone kmapnotify. Then, of course, there's <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebase/kxmlrpc/README?rev=1.7&content-type=text/x-cvsweb-markup">kxmlrpc</a> which provides a bridge to DCOP for anything that can speak HTTP and XML (eg, control DCOP-speaking apps from a <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebase/kxmlrpc/test/testxmlrpc.sh?rev=1.4&content-type=text/x-cvsweb-markup">shell script</a> or <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebase/kxmlrpc/test/testxmlrpc.py?rev=1.7&content-type=text/x-cvsweb-markup">python</a>).  And as if all that wasn't enough, there's the command-line or point-n-shoot DCOP <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebase/kdcop/README?rev=1.1&content-type=text/x-cvsweb-markup">browser/executor</a> that just goes to show how far, flexible and mature DCOP has become in a short matter of time.  Simplicity pays off in the end.




<!--break-->
