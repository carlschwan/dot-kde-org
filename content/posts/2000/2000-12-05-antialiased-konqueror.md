---
title: "Antialiased Konqueror!"
date:    2000-12-05
authors:
  - "wes"
slug:    antialiased-konqueror
comments:
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "oooh... pretty..."
    author: "greg"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "No, VERY pretty....."
    author: "Ill Logik"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "ehhmm.. I hate to say this, but it's even prettyer than VERY pretty. It's, uhh.. FLABERGHASTING!"
    author: "Sm\u00e1ri P. McCarthy"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "Thats something I have been wishing for.  Finally text will be readable in X.\n yippy :>"
    author: "MacHack"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "Very nice indeed, but what is still missing is a majorly updated printing system with full WYSIWYG.\n\nWhy cant\u00b4t we have something like one directory where you put any Truetype-, Type1-, CID-, etc. fonts, which are then used for both displaying and printing - without having to hack together any Fontmap and fonts.dir files.\n\nYes, I know there are ways to generate them automatically, but these auto-generation is not exactly always working as it should do.\nEspecially when you have a large number of fonts you have to spend hours adjusting Fontmap, fonts.dir, etc. so that at least printing from Qt-based-programs works roughly as it should.\n\nThen of course there is the problem that there are many fonts (e.g. asian TTF fonts) which X11 can display but Ghostscript cannot print, at least not without patches.\n\nIf we want to beat Windoze on the desktop, this whole font problem needs to be solved."
    author: "jtux"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "This sort of thing is already partially solved in Manrake 7.2. Using CUPS, it is capable of rendering fonts for printing pretty well, and FontDrake allows easy font installation, and even importation of Windoze fonts."
    author: "Carbon"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "pretty, keep on the good work."
    author: "heroo"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "Am I the only one who finds antialiasing overrated?\nMy fonts look great in KDE2/XFree4.01/Mandrake (as good as in Windows IMHO) and I doubt I will notice any significant difference. (Perhaps my monitor is blurry ;-))"
    author: "Zank Frappa"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "Zank Frappa? Maybe you got some mudshark on your screen? :-O\n\nIt's great that you're happy... but many of us pretty much don't feel like resting until people bust out laughing and slapping thier knee when they see windoze run proclaiming they have a real desktop on thier system. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "Are you sure you're not just reading octal fourty? Get your eyes checked willja..."
    author: "Sm\u00e1ri P. McCarthy"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-06
    body: "Yes. I think that's the short version. \"Yes, yes you are in fact the only person who \"finds\" it overrated\" - that would be the full version.\nFew people will put up with crunchy, craggy, broken-up looking letterforms and other screen elements.\n\nMaybe your truetpye fonts look \"OK\" as in passable considering it's UNIX, but your diagonals and curves in any drawing tool still look like absolute shit.\n\nAntialiasing in general use a very good thing; cars with pneumatic tires are preferable to solid rubber or wooden wheels although strictly speaking a wheel vehicle need NOT be equipped with inflatable products from Michelin or Goodyear, still you will find few people riding in carts and wagons when faster, smoother riding transportation is available. So too with OS's that spurn what the common folk think is a comfortable interface."
    author: "ac"
  - subject: "Killer App?"
    date: 2000-12-05
    body: "OK, this isn't the killer app which will make Linux/KDE the number 1 desktop, but it is a very big move in the direction of taking users away from Windows.\n\nWhen I am showing people how cool Linux looks with KDE, things go great, until we go surfing the Internet.  I hit a web site such as BBC News, and the font rendering is abysmal.  Having cool anti-aliased fonts will make Konqueror the browser of choice for many Linuxers (RIP Mozilla)"
    author: "Gareth Williams"
  - subject: "Re: Killer App?"
    date: 2000-12-05
    body: "Notice the person that wrote the Konquie patch - Keith Packard, of XFree fame.  This is not a KDE 'killer-app'.  In fact, this has little to do with KDE.  The screenshot is a proof-of-concept.  This code is going into the XFree86 Core source tree.  Hence, Mozilla, Gnome and every other X-enabled app will take equal advantage of the scw33tness.\n\nHave a nice day.\n\n- James"
    author: "James"
  - subject: "Re: Killer App?"
    date: 2000-12-06
    body: "I think that Konqueror is still a bit far from Mozilla, because Konqueror renders some pages horribly, due to extreme lack of CSS compliance. Mozilla is now even better than IE with CSS"
    author: "Jo\u00e3o Marcus"
  - subject: "Re: Killer App?"
    date: 2000-12-06
    body: "Yes, but I can live with substandard page rendering, if I can read the page, and have gone on to something else before Mozilla even loads.\n\nAnd besides, I've never noticed anything substandard about Konquerors page rendering."
    author: "Gareth Williams"
  - subject: "Re: Killer App?"
    date: 2000-12-07
    body: "I'm so sorry to have people like you in the Linux Community. Why the heck would we want to take away windows users? what can they do for us? WHY do we need to take them away, you fscking tell me that dude.\n\nIf all windows users would move to linux, then linux will probably shapeshift into a PURE windows CLONE. no more shell, no more full control, just a sweet GUI. and that's not what Linux is all about...\n\nAnd the arrogance, thinking you can take them away... GOD! This is _NOT_ a race!"
    author: "Remenic"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "Looks pretty cool, but am I the only one which thinks the \"w\" looks a bit \"fuzzy\"? <br>\nTake a look under \"Family\" in the menu to see what I meen. Maybe it's just because of the way the \"w\" looks, that it is a bit \"fuzzy\".\n<br><br>\nIt's not something that bothers me...I *really* would like to have this inkluded in KDE 2.1 :-)"
    author: "Pointwood"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "> inkluded ? \n<br>\nIs this the way we'll have to spell it as soon \nas kcc, the KNU Kompiler komes out? \n<br>\n#inklude \"konfig.h\"\n<br>\n;-)"
    author: "Anonymous Coward III."
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "What a great idea! Then the gnomes can produce their own gompiler with ingludes to get some competition going ;-)"
    author: "Zank Frappa"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "Please see my other comment - it's just a spelling error..."
    author: "Pointwood"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "LOL :)"
    author: "Macka"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "I guess what you really meant is:\n\n... with ingludes to get some gompetition going ;-)"
    author: "Jeremy M. Jancsary"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "You're an flaming AC, but I'll answer it anyway. It's actually a my spelling that's bad!\n<br><br>\nYou see, I'm from Denmark and here we spell include with a \"k\", like this: \"inkludere\".\n<br><br>\nIt has nothing to do with KDE or that most KDE apps is is called \"KSomething\"!<br><br>\n\nAnd please stop \"trolling\", what good is it for? - you just sound like a stupid 14 year old kid with nothing better to do..."
    author: "Pointwood"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "He was just having a bit of light hearted fun.  Take a happy pill.   That, and the gnome follow up had me in stitches :)\n\nMacka"
    author: "Macka"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "Oh dammit...is it monday? :-)\n<br><br>\nI overlooked the smiley, sorry about that \"Anonymous Coward III\".\n<br><br>\nGreetings\nPointwood (quietly eating a happy pill :-) )"
    author: "Pointwood"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "Actually, that isn't trolling, its just that hackers have a tendency to dig puns. It's irrestiable when it involves their current project as well!\n\nFor instance, here is trolling (I don't really mean this, it's just an example):\n\n<trollscript>\nKDE Sucks! Windoze Rules! Shut up all you ***** ****ing *****ers! HAHAHA! I AM GOD! HAHAA! U ALL SUCK! I AM L33T! shut up all you idiots! Windoze rules, and you all stink, so why dont you go **** yourselves!\n</trollscript>\n\nNote the low self-esteem and the merciless slander without the presentation of any evidence whatsoever. Now this is trolling.\n\nThat was trollling, and I've even seen worse! So please, don't mistake just standard hacker punning and joking for trolling!\n\nP.S. I said it once, and I'll say it again. The above trolling is just an example! I don't mean it! I support KDE and linux. I've even done a little coding to help support it."
    author: "Carbon"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-06
    body: "A png may contribute to / magnify the blurriness of an image depending on how it's made. Not as badly as your garden variety jpeg, of course."
    author: "ac"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-06
    body: "This screenshot is better.\nhttp://xfree86.org/~keithp/render/session.html"
    author: "Zeljko Vukman"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "Will this also bring antialiasing to KPresenter?\nThat would be a big step towards real usability of that program."
    author: "sune"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "I have babbled about this feature since I saw X the first time, and now (a long time after) it is here.... YEEEESSSS! Now talking about Qt.. Is this meant to be implemented in a way that ALL text can be antialiased? Like also Konsole? (I would like to c clear 10pix)<br><br> As someone else said, \"... in KDE 2.1 ????\". Well.. I hope so.. but with 13 days to beta realese it doesn't sound plausible... or does it? Anyway, this is worth waiting for 2.2 over, since this is exactly the kinda things that make M$ users ENVY us freeminded and independent beings, whith sooo cooool looking DT's.<br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "I have babbled about this feature since I saw X the first time, and now (a long time after) it is here.... YEEEESSSS! Now talking about Qt.. Is this meant to be implemented in a way that ALL text can be antialiased? Like also Konsole? (I would like to c clear 10pix)<br><br> As someone else said, \"... in KDE 2.1 ????\". Well.. I hope so.. but with 13 days to beta realese it doesn't sound plausible... or does it? Anyway, this is worth waiting for 2.2 over, since this is exactly the kinda things that make M$ users ENVY us freeminded and independent beings, whith sooo cooool looking DT's.<br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "SHIT, I DID IT AGAIN!!! SHAME ON ME!!! DO NOT FOLLOW THIS EXAMPLE!!!"
    author: "kidcat"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "I don't think it's you, it must be some bug."
    author: "reihal"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "All talk about hideous looking fonts in X makes me wonder if all these people actually are referring to the scalable Type1 and TrueType fonts, or if they're using non-scalable bitmap fonts that look real blocky when used for big text.\nFor me there's no huge difference between the same TrueType font used with X and with Windows. Sure, it's smoother under Windows with antialiasing, but not *that* much.\nCorrect me if I'm wrong, but I find it hard to believe that antialiasing would make a bitmap font look great in X."
    author: "Zank Frappa"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "I agree.  If one spends the time (only about 30 minutes) to bring over all the MS TrueType fonts into KDE, it looks wonderful.  The Antialiasing adds a little, but the real improvement is going from bitmap to TrueType... I think <b>that</b> should be stressed."
    author: "Darrell Esau"
  - subject: "I Love you Keith"
    date: 2000-12-05
    body: "<I>Keith Packard, member of the XFree86 Core Team and author of the highly anticipated Render extension for X, posted an exciting screenshot Sunday to kde-devel: Konqueror displaying kde.org using antialiased text!.</I>\n<BR><BR>\nWonderfull. \n<BR>I love you Keith."
    author: "KDE USER"
  - subject: "OFF TOPIC - ANNOUNCEMENTS"
    date: 2000-12-05
    body: "Why are all KDE-Apps first announced at \nfreshmeat and hours later on apps.kde?\n\nLike TODAY KDE 2.0.1 HAS BEEN released. Noted on freshmeat at 05.45 cet.\n\nNow it s 7h later and there s not the idea of someone mentioning that on a kde site. this is weird.."
    author: "predator1710"
  - subject: "Re: OFF TOPIC - ANNOUNCEMENTS"
    date: 2000-12-06
    body: "Duhhh...<br>Perhaps to let other ftp sites mirror ftp.kde.org :)<br><br>Otherwise it will take two days before anyone can download anything and the server will be down. There are probably thousands of users out there who look at the ftp.kde.org and when they notice anything the fire off a mail to freshmeat..."
    author: "Sam"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "3 CHEERS FOR THE KONQUEROR TEAM!<BR><BR>\n<P>I had almost given up on fonts ever looking good in linux, but once again the KDE folks are on it.</P>\n<P>I'm loving KDE2 and can't wait to see the antialiasing and alpha blending in 2.1!</P>\n\n<P>I'd like to buy you guys a beer, you earned it!</P>"
    author: "David Talbot"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "Great!\nThe ony problem its that the rest of XFREE does not supported for antialliasing"
    author: "OcTO_VIII"
  - subject: "It's about time...."
    date: 2000-12-05
    body: "I've been waiting a long time for this.\nFinally X is capable of producing readable text.\n\nA big thanks to everybody involved....and keep up the good work."
    author: "Binge"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "I'm not impressed. My fonts in Konqueror looks much better. See screenshot."
    author: "Zeljko Vukman"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-05
    body: "What's wrong with theese fonts in Konqueror?"
    author: "Zeljko Vukman"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-06
    body: "The theme you are using is a case for an eye examination ;)"
    author: "Sam"
  - subject: "Re: Antialiased Konqueror!"
    date: 2006-10-02
    body: "omar atoui ossama"
    author: "omar atoui"
  - subject: "Re: Antialiased Konqueror! -- with tt fonts"
    date: 2000-12-05
    body: "OK, if we're comparing length and width anyway, her's konqueror on freebsd with true type fonts enabled on X 3.y using xfstt as font server..."
    author: "Danny"
  - subject: "Re: Antialiased Konqueror! -- with tt fonts"
    date: 2000-12-06
    body: "Really nice, so where can I find documentations for install true type fonts on my linuxbox like you?! :-)\n\nBye!"
    author: "Miky"
  - subject: "Re: Antialiased Konqueror! -- with tt fonts"
    date: 2000-12-06
    body: "Download and install the xfstt package for your distro and type:\n$>man xfstt\n\nfollow the man page and you have ttf."
    author: "loserboy"
  - subject: "Re: Antialiased Konqueror! -- with tt fonts"
    date: 2000-12-06
    body: "Thanks a lot! :-) I will download xfstt and install true type font as son as possible! :D"
    author: "Miky"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-06
    body: "Nice to see anti-aliased fonts come to *nix. Those of you who have used Acorn Archimedes RISC OS machines will remember Acorn had this going way back in 1988..."
    author: "Soruk"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-06
    body: "I still have a RiscPC, and an a3000 before that (although I run linux on my x86)\n\nThis is a VERY welcome addition - I used Acorns for years before seeing a windows machine, and my eyes hurt as they followed every jagged pixel :o)\n\nIts the sort of thing that you dont notice so much until its gone."
    author: "Funky_Peanut"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-06
    body: "I've still got my RISC-PC and A3010. The anti-aliasing on these beats anything I've ever seen, including Mac OS X, Windoze Whistler and the Render extension to X (very welcome, though). It even looks good on a 640x256 screen in 8bpp..."
    author: "jpmfan"
  - subject: "Re: Antialiased Konqueror!"
    date: 2000-12-07
    body: "Here's a pic and some info on a fully antialiased KDE: <BR><A href=http://devel-home.kde.org/~granroth/anti-aliased-kde.html>http://devel-home.kde.org/~granroth/anti-aliased-kde.html</A>"
    author: "Zank Frappa"
---
<a href='mailto:keithp@keithp.com'>Keith Packard</a>, member of the XFree86 Core Team and author of the highly anticipated Render extension for X, posted an exciting screenshot Sunday to kde-devel: <a href='http://keithp.com/~keithp/aa/'>Konqueror displaying kde.org using antialiased text!</a>  Unfortunately the code is not ready for general use, but Keith <a href='http://lists.kde.org/?l=kde-devel&m=97592329525177&w=2'>offered a rough patch</a> for Qt to set things in motion.  Matthias Ettrich (along with many other enthusiastic core developers) expressed interest, and <a href='http://lists.kde.org/?l=kde-devel&m=97595594108180&w=2'>mentioned</a> Keith's code may be included as soon as Qt version 2.2.3.  You can read the full discussion on kde-devel, starting with Keith's initial announcement, <a href='http://lists.kde.org/?l=kde-devel&m=97591561811342&w=2'>here</a>.


<!--break-->
