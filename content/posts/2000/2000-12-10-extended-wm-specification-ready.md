---
title: "Extended WM Specification ready"
date:    2000-12-10
authors:
  - "Inorog"
slug:    extended-wm-specification-ready
comments:
  - subject: "Re: Extended WM Specification ready"
    date: 2000-12-10
    body: "Call me stupid... but does this mean I can use KWin in Gnome?"
    author: "Anonymous"
  - subject: "Re: Extended WM Specification ready"
    date: 2000-12-10
    body: "Once GNOME is brought up to spec (no idea as to the status of this), yes!"
    author: "wes"
  - subject: "Re: Extended WM Specification ready"
    date: 2000-12-10
    body: "Congratulations!  It's great to see developers from the KDE and Gnome projects working together on something.  This will benefit the users greatly.  Thank you."
    author: "Jamin P. Gray"
  - subject: "Re: Extended WM Specification ready"
    date: 2000-12-10
    body: "I agree with you! Thanks to the developers."
    author: "Stefan"
  - subject: "Re: Extended WM Specification ready"
    date: 2000-12-10
    body: "From a user's perspective I don't care if my\napplications are KDE or Gnome applications so\nthis is great news.\n<P>\nBack in the early days of computing there used\nto be the Athena Widgets and then toolkit after\ntoolkit followed but all the applications could work\ntogether thanks to the ICCCM (well, at least\nthose applications that would care about the\nICCCM :-).\n<P>\nBut would this make Unix/X11 a success? No,\nnot really. And I could understand why. Each\napplication had a different file selection dialog\nand moreover each application had a different\nlook and feel of the scroll bar or menus\nor buttons!\n<P>\n<B>So my wish would be, that you'd continue this\nwork with a style guide for X11 toolkits.</B> The\nusers want every application to behave like\nthe other and I believe this is necessary.\n<P>\nIf you can reach this goal then there isn't much\nleft from the KDE/Gnome war (or whatever it is).\nThen anybody can program in C/C++/Python/Perl/Java\nor whatever he likes and use whichever toolkit\nhe likes. And the user wouldn't even see the\ndifference. He would just choose the application\nhe likes the best.\n<P>\nPlease get me right. I don't mean that things\nshouldn't be configurable, but the standard\nlook and feel of KDE and Gnome should be\nthe same. And I beg you to not underestimate the\nneed for a common look and feel!\n<P>\nThis is what made the Mac (and also Windows)\na success and Unix a failure (on the desktop).\nI often read that having alternatives to choose from is a\ngood thing. But what about the company that wants\nto develop an application for all Linux users\n(and we need those applications)? If we force\nthis company to choose only one half of the\nLinux users it's possible that the company\ndecides to not do it at all."
    author: "Michael Gengenbach"
  - subject: "Re: Extended WM Specification ready"
    date: 2000-12-10
    body: "I really agree with you. My dream is that the user can't tell the difference between a Gnome and a KDE program. This would be great because then these two desktops would minimize the number of missing applications. Every desktops has a few great applications but also some applications that are still missing (but maybe the other desktop has this missing application already).\nI know that this is easier to say then to accomplish. But why not dream a little bit?\nI think the problem with the \"wasted\" memory will be no problem in the future. There will be so much memory in the computers of the future (128 MB of RAM and more) that two loaded libaries will be not a big problem. \nThe user of a QT-program (under windows) can't also tell that the program is not using the \"normal\" windows libary. Why shouldn't this be possible under linux?\nKDE-developer keep up doing this great job!"
    author: "Steffen"
  - subject: "Re: Extended WM Specification ready"
    date: 2000-12-10
    body: "Ok. I'm gonna sound like I'm bashing GNOME, but bare with me, I'm not. Not voluntarily, anyway. Not wanting to spread more gas on a dying fire, but here is what I think/feel:\n\nThe idea is a wonderful one. Unfortunately, it's (right now) completely impossible. The Qt graphics capabilities are simple a few years ahead of GTK's. GTK's pixmap theme handling is at best laughable. I'm on 133mhz computer, and can rest assure that GTK's themes are as simplistic as they can get. It's slow, ugly, and unusable.\n\nOk, my computer is anything but bleeding edge. OTOH, just writing bad code and bad applications because the hardware can survive it, is plain stupid. Simply because 800mhz are out there does _NOT_ mean that a simplistic widget set should be brain-dead and require heavy load to draw simple themes. Especially when there are much better implementations out there.\n\nAlso, Qt supports UNICODE, and GTK does not. (Yet, I hear 2.0 is supposed to). Qt is a few years in advanced in many, many points to GTK. (Theu se of the programming language for example... )\n\nThe morale is that for application to simply LOOK similar, KDE would have to forbid its developers from using the whole of Qt's power, for user to sacrifice the advanced theming KDE/Qt can provide, simply to allow GTK apps to catch up.\n\nFor applications  to behave similarly, GNOME would have to implement network transparency, a usable (non-mozilla) HTML widget, and thousands of other things KDE has had since 2.0, and in some (non-rare)  cases since before that. (For example, a sane pixmap handling code. GTK's gdk-pixbuf is still not up to par wth Qt 1.4 series' QPixmap+QPainter classes)\n\nThe differences between gnome and KDE are less and less philosophical, and more and more technical. GNOME lags behind in almost all cases of usability (user-wise) and usability (developer-wise: GDK vs QPainter? There are no comparisons... same for a thousand other details)\n\nThe same kind of arguments could probably be made in favor of some parts of GNOME (bonobo vs KParts come to mind, even though I favor KParts personally), and the GNOME guys would tell you that GNOME would need to be 'inferiorized' in order to let KDE catch up.\n\nThere would probably need some lowest common denominator in app development that would bring both desktops years back, not to sayy completely inferior to other OS'es desktops environment.\n\nIt's impossible. (Un)Fortunately."
    author: "Christian Lavoie"
  - subject: "Re: Extended WM Specification ready"
    date: 2000-12-11
    body: "Without diving into the details of this, I'll just say that you are wrong on a number of technical points there. Overall, GTK and Qt are essentially comparable in terms of features and technology.\nI could point out the specific areas in which one or the other is ahead, but none of those areas amount to \"a few years,\" all of them are relatively small issues, and there are advantages and disadvantages to each toolkit.\nIf you make a spectrum of toolkits (including win32, Swing, Tk, etc.) then GTK and Qt are closer to each other than they are to any other toolkit."
    author: "Havoc Pennington"
  - subject: "Re: Extended WM Specification ready"
    date: 2000-12-11
    body: "Concerning Unicode, is full Unicode input into the text boxes possible on QT 2.2 today? And how about Bidirectional text? Afraid not ...\n\nThat's stuff GTK+ 1.3 does and so QT 3.0 is expected to."
    author: "Ilya"
  - subject: "Can someone help me"
    date: 2000-12-11
    body: "I know this is off topic but I cant find help anywhere.  When I try to install KDE 2.0.1 I get \nlibkmid.so.0 is needed by kdebase-2.0.1-1.  I have a RH7 system with NO KDE1 GNOME only.  I searched everywhere and cant find anything.  Please help me."
    author: "Robert"
  - subject: "Re: Can someone help me"
    date: 2000-12-11
    body: "Ahh, fun with rpms! I've given up on them and started compiling almost everything (a real pain). I'm going to have to try an apt-based system... it's supposed to be easier.\n\nAnyway, are you installing the rpms in the right order? I think it goes kdesupport, then kdelibs, then kdebase, etc. (Check kde's site for instructions). I think if you just \"rpm -ivh *rpm\" then rpm will work out the order itself.\n<p>\nBTW, if you get it installed and find kde slow or unstable, it's probably not kde but the packages that you installed. In that case, compiling (qt especially) makes a lot of difference.\n<p>\nHope that helps,<br>\nkdeFan"
    author: "kdeFan"
  - subject: "Re: Can someone help me"
    date: 2000-12-11
    body: "I think kmid is part of the kdelibs package, so it's possible you're not installing/compiling the packages in the correct order. Here's the order for compiling, which should hold true for installation as well\n\nQT\nkdesupport\nkdelibs\n[other kde libs, order not significant)\n\nI got this from \n\nhttp://www.kde.org/install-source.html\n\nHope this helps a bit.\n\nRich"
    author: "Richar Sheldon"
  - subject: "Re: Can someone help me"
    date: 2000-12-11
    body: "I had the same problem with libkmid.so.0.\n\nTake a look on the rpmfind.net pages.  I did a quick and dirty, and found this:\n\nhttp://rpmfind.net/linux/rpm2html/search.php?query=libkmid.so.0\n\nCame up with 50 rpms for libkmid.  You can also look on the home page for libkmid:\n\nhttp://www.arrakis.es/~rlarrosa/libkmid.html"
    author: "Nick Ryberg"
  - subject: "Re: Can someone help me"
    date: 2000-12-11
    body: "I found out how to to so it but thanks for the help \nit ends up more than kdebase qt2.2.2 and kdelibs are needed to run kde2, I needed konqueror and the multimedia packages as well.  ."
    author: "Robert"
  - subject: "Re: Extended WM Specification ready"
    date: 2000-12-12
    body: "Speaking of Window Managers, I've been working on a new KWin theme which is based on, and closely resembles the KWin 2.1 default theme in the KDE2.1 snapshots. It has been optimised to reduce flicker, esp. when resizing. To see a preview, point your browsers at:\n\n<a href=\"http://gallium.home.dhs.org/\">http://gallium.home.dhs.org/</a>"
    author: "Gallium"
---
Encouraging news come from the <a href="http://www.freedesktop.org">X Desktop Group</a>. <i>Havoc Pennington</i>, founder of the group and free software visionary, <a href="http://www.freedesktop.org/news/wmspec-announce.html">announced</a> the release of the version 1.0 of the <a href="http://www.freedesktop.org/standards/wm-spec.html">Extended Window Manager Specification</a>.
<!--break-->
<p><i>Matthias Ettrich</i> of KDE and <i>Hugh Bradley</i> of Trolltech fruitfully participated to the work of this project. Thanks to them, KDE provides a first, proof of concept, full implementation of the spec. Chances are you're using their work on this topic right now, as the window manager of KDE-2, KWin, is 100% compliant.
<p>The release of this specification is an important event. There is now a real chance that applications will be offered with the same functionality at the level of the interface with the windowing system, no matter which desktop environment the user employs.
<p>Many thanks from the KDE project to all the participants in the WMSpec creation.