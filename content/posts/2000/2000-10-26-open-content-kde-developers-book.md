---
title: "Open Content KDE Developer's Book"
date:    2000-10-26
authors:
  - "dsweet"
slug:    open-content-kde-developers-book
comments:
  - subject: "Re: Open Content KDE Developer's Book"
    date: 2000-10-26
    body: "I ordered it a month ago from Amazon, do you know a date when it is out?"
    author: "reihal"
  - subject: "Re: Open Content KDE Developer's Book"
    date: 2000-10-26
    body: "It is being printed right now.  It should\nbe shipped in a few weeks.\n\nDave"
    author: "David Sweet"
  - subject: "Re: Open Content KDE Developer's Book"
    date: 2000-10-26
    body: "KParts chapter looks *great*!  Keep up the good work."
    author: "KDE User"
  - subject: "Table of Contents?"
    date: 2000-10-26
    body: "Is there a Table of Contents yet?  I'm interested if there is a kio_slave chapter. (Also just curious what else is covered, although I'll probably get it regardless).\n<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "KDE 1.1.2 ?"
    date: 2000-10-27
    body: "I'm sorry for posting totally offtopic here ...\n<BR>\nWhere are the KDE 1.1.2 sources ?\nThey have been removed from the FTP site and it's server .... ARGH !<BR>\n\nI need them ! \nThomas"
    author: "Thomas"
  - subject: "Re: KDE 1.1.2 ?"
    date: 2000-10-27
    body: "Alltough it has been some time since I have downloaded\nthat version this it should be possible to use cvs with\nthe correct TAG (KDE_1_1_2_RELEASE)"
    author: "Espen Sand"
---
A book called <a HREF="http://www.andamooka.org/kde20devel/"><EM>KDE 2.0
Development</EM></A> will be available online and in print starting in
early November.  Until then you can find prerelease versions of <A
HREF="http://www.andamooka.org/kde20devel/chapter13.shtml">a KParts
chapter</A> by David Faure and an <A
HREF="http://www.andamooka.org/kde20devel/AppendixB">abridged KDE API
reference</A> on <A HREF="http://www.andamooka.org">Andamooka</A>.
 The book will be published under the <a
HREF="http://www.opencontent.org">Open Publication License</A>.

<!--break-->
<p>
<em>KDE 2.0 Development</em> was written by me and several people
from the KDE community:
<UL>
<LI>Kurt Granroth
<LI>Cristian Tibirna 
<LI>David Faure 
<LI>Espen Sand 
<LI>Stefan Westerfeld 
<LI>Ralf Nolden 
<LI>Daniel Marjama"ki 
<LI>Charles Bar-Joseph 
<LI>with a forward by Matthias Ettrich
</UL>
We hope that this will help current KDE developers learn about the
new KDE 2.0 technologies/APIs as well as encourage new developers to
try working with KDE.

<P> 
The entire book will be available for online and offline reading and
will be subject to section-by-section, moderated, community
annotation.  This should help to keep the information current and
accurate while still keeping it organized.

<P>
Finally, translations of the book have begun: 
<A HREF="mailto:jcornavi@club-internet.fr">Joelle Cornavin</A> (French),
<a HREF="mailto:pkiatisevi@student.ei.uni-stuttgart.de">Pattara Kiatisevi</A> (Thai), and <A HREF="mailto:goddard@brfree.com.br">André Goddard Rosa</A> (Portuguese).  Please contact <A HREF="mailto:dsweet@andamooka.org">me</A> or one of them if you are interested in helping with translation.
