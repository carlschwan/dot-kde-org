---
title: "Miscellaneous KDE eyecandy"
date:    2000-10-04
authors:
  - "numanee"
slug:    miscellaneous-kde-eyecandy
comments:
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "Hey I like that Mosfet theme, much better than the default KDE2 one."
    author: "David Walser"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "Yeah, I like it too, although it's too bad that the X went back to the right.  But what can you do, at least we got the funky icon back.  I hope this style is eventually committed!  (I like it better than ModSystem)\n\nThe current default has already grown on me though, but it still truncates the lowers in the title text.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "Hey, what about setting the size of the font to 11 instead of 12? Look at KDE Control Center/Look&Feel/Fonts....\n\nMichael"
    author: "Michael Brade"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "It's a pretty theme, but I think the default KDE theme is a better \"default\" theme. This is primarily because it's simple, bare-bones, and looks a good bit like the old kwm style, but a bit touched up. Has that \"classic\" appeal, and is just simple. It won't be at all confusing to newbies, and people who actually want it to look prettier (like me) can take the 10 seconds to change it.\n\n--Aaron"
    author: "Aaron Traas"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "Wow!  That looks exactly like the GNOME login sequences.\n<p>\nEven the touch of using a sea shore.\n<p>\nNice touch."
    author: "Anonymous Coward"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2002-04-01
    body: "no it doesn't. if it looked like the gnome login process, then it would be like a mac. since that's what they made the gnome one look like."
    author: "lamer"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "My vote is for Sreshth Kumar's yet 'another' splash screen. It looks the most professional."
    author: "Geter"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "Oh, no! Yet another windoz-like suks! ;-) I think that a first screen is best! It is really cool, yeah!"
    author: "Dmitry"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "Note that these aren't really up for vote, and that there's already a pretty good splash screen in KDE2.  I thought the above were worth a mention anyway.\n\nWho knows though, the current splash might always change before the final release (the artists always tend to surprise us :).\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "How do I create my own splash screen ??\n\nWhere can I get other splash screens once kde2.0 final is out ?\n\nThanks\n\nChris, aka bunty on IRC"
    author: "Christien Bunting"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "Good question.  No doubt some distributors will come out with their own splash screens, come to think of it.  We could use some themability out of the box here.  ;)\n\nAnyway, look at the source in kdebase/ksplash/.  It doesn't seem too hard.  For starters it may be as easy as replacing the images with your own versions.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "It is - replacing the picture works, sort of. I've ran for a while with a sketch I made myself. The only issue is the strip of buttons underneath.\n\nNot that I see the splash all that often - there are not many memory leaks anymore, and I often leave KDE 2 just running."
    author: "Boudewijn Rempt"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "I think that the current KDE2 splash screen is\nprofessionally designed, and that is one of the best designed things in KDE2. Anyway, this is a matter of taste, and KDE2 gives possibility to change splash screen if one does not like it.\nHelix-Gnome has a nice splash screen, and that\nkind of design represents Mexican fiesta, happiness & visual creativity. \nKDE2 splash screen simbolize German precision and\ntechnical superiority (which KDE2 compared to Gnome really is).\nI would like to see more consistency in KDE2 icon design. KDE2 has some extremly beautiful icons, and some extremly ugly ones (one example for ugly one is default \"home\" icon). KDE has allways had\nthat kind of design which I call \"streamlined, clean & non-disturbing design\", and it should stay with it. KDE2 should not copy Gnome design\nand surely not add flowers, birds, clouds & seasides to its visuality."
    author: "Zeljko Vukman"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "Well said. I think now that the standard is set and KDE2.0 is ready for primetime we'll see a section of the community devoted to the graphical design of it. The Graphical part wasn't priority as we can see. Get the Functionality then all the Nice stuff. Unlike the gnome regime which devoted most of it's efforts into \"look and feal\" in the earlier development period.\n\n\nGo KDE!!!"
    author: "Christien Bunting"
  - subject: "Splash"
    date: 2004-05-13
    body: "i would like to change the splah of KDE 3.1. but idont kwon how. tks "
    author: "juanS"
  - subject: "Re: Splash"
    date: 2004-05-13
    body: "Upgrading to KDE 3.2 changes it."
    author: "Anonymous"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "honestly, comparing to GNOME splash screen...\nyou folks need to learn more photography technique."
    author: "acc"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "As mentioned above, these splashscreens are proposal/sketches from users, not members of the kde-core-team. And i doubt very much that they will sneak into cvs... just consider copyright issues..."
    author: "Lenny"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "IMHO, KDE borders and Icons *do* need to be upgraded. GNOME gets its distinctive look from it's icons and the window dressings. The actual GTK widgets look, IMO, terrible.\n\nIf you start a GNOME app in KDE, you can see this.\nHowever, start a KDE app in GNOME, and you see the crispness of the QT widgets.\n\nWhat KDE needs, is some work in the artwork department - nice rendered hi-color icons and decent window dressings. This is all that gives GNOME it's unnique look - and all that makes people insist on refering to KDE as a Windows clone."
    author: "Ezz"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "Cool, i'm eagerly awaiting your contributions."
    author: "Lenny"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-04
    body: "Not a half bad post.  However, I'm going to say what you've probably heard already \"If you want it, do it yourself\".  I contributed two icons to KDE2 (the cookie icons).  I found it to be much much harder than I expected it to be.  As a newbie, it took 6 weeks to do those two icons.  Torsten can push them out in a day or three, but he still has most of the KDE icons to do, so he doesn't have time to worry about going in and doing each icon again.  As someone who has done icons, I feel that I am qualified to say...\"If you want it, do it yourself\"."
    author: "Kevin Breit"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-05
    body: "Hey Can U tell me how to go about making icons or splash screens I mean what format is required I am eager to do it. I have had some experience with Q3 textures.."
    author: "Pramod"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-05
    body: "Look at artist.kde.org (icon-factory)."
    author: "Lenny"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-05
    body: "I'm no graphic artist, unfourtanately, or I would have created my own icons etc ages ago.\nBut, surely, there are/is a single person/group of people responsible for the \"look\" side of the the KDE interface, and it's for them to take on board comments such as the one I made above.\nEven if I were a graphic artist and I did create my own set of Icons, what would the chances be of them being accepted as the \"official\" set of KDE icons?\nThe most I could do, if I had the ability, was to create my own theme containing my icons a route many people have taken already. But said icons still are not the defaults distributed with KDE as they are with GNOME.\nAll I can do is contribute an observation and a recommendation which I have done.\n\nAs an (not entirley un-related) aside, I notice that the kde artist site hasn't been updated for a long time. \nWhat is the process, beyond rising issues in this forum, for giving input to the direction of KDE generally and the Artist stuff particularly?"
    author: "Ezz"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-05
    body: "<i>As an (not entirley un-related) aside, I notice that the kde artist site hasn't been updated for a long time.</i>\n\nWhat do you consider most important: marketing or doing the work ?\n\n<i>What is the process, beyond rising issues in this forum, for giving input to the direction of KDE generally and the Artist stuff particularly?</i>\n\nPlease keep in mind that kde doesnt lack of people _talking_ about what should be done. The optimal process is: mail your icons/graphics to icons@kde.org or torsten@kde.org. If its good it will be in cvs within a jiffy."
    author: "Lenny"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-05
    body: "Hmm.\nI was alluding to the KDE artist site as a central focus for those interested in advancing the look of KDE. Seems difficult to contribute to an invisable effort.\n\nThanks for telling what to do with any artwork. The point being, it wasn't obvious.\n\nAny graphic artists take note: You've been told.\n:)"
    author: "Ezz"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-05
    body: "Yes, you are right. I hope we have time after 2.0 to drag things more into the public."
    author: "Lenny"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-06
    body: "> IMHO, KDE borders and Icons *do* need to be upgraded.\n\nWe are upgrading them all the time. Until recently all icons were -- for technical reasons based on almost the same palette Windows is using. For the recent three months we have been moving to more pastel-colored icons though. This process will take some time as we have quite a lot of icons.\n\nA major difference between KDE and Gnome is that KDE is using many more pixmaps than Gnome does.\nIf you look into kdelibs only, you'll discover that there are about 700 icons in there which we have to take care of -- the whole KDE has probably something around 1200 icons!\n\nFurthermore if you want to compare KDE with Gnome you should be aware of the fact that Gnome uses 48x48 icons by default while KDE uses 32x32 by default. If you switch KDE to 48x48-icons you get of course much \"better\" results:\n\nftp://derkarl.org/pub/incoming/kde2shot21092000b.png\n\n(where better means in my opinion also compared to gnome-icons).\n\nGnome-icons are not being rendered. They are being *painted* by Tigert. Tigert's icons are quite flashy and cool and are of course quite nice artwork. But in terms of usability they don't make good icons for several reasons (They are dark, not too crisp, don't scale very well, don't use isometric perspective and use metaphors which are not very common and easy to understand, to name just a few). \n\nIf you think you can do some better work on the icons though feel free to commit. Send your icons to icons@kde.org and I'll happily include your work into CVS if they are not too bad.\n\nGreetings,\n\nTackat"
    author: "Torsten Rahn"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-06
    body: "Thanks for taking the time to respond.\n\nDidn't realise there were so many icons in KDE. \nI did realise that GNOME icons are bigger than those in KDE and that they seem to be in PNG format rather than XPM.\n\nNot sure what you mean by \"painted\" as opposed to \"rendered\" but I have noticed that the GNOME icons seem to have been drawn (painted? :) first, had some form of ray tracing done to them and then shrunk to 48x48 (the same size as WindowMaker/OpenStep etc?).\n\nThis seems quite a good approach, so long as the edges of the icons are crisp and the meaning of them is clear - they don't have to be dark.\n\nAs for the metaphors and understandability of the GNOME icons - well that's subjective. The point is, people (subjectively)love the way GNOME looks due entirely to it's icons and window decorations. Similarly, the same people tend to think that KDE looks like windows (a view which I don't take myself).\n\nWhat I was thinking, is, if KDE's icons could be made more eye-candyish (but not at the expense of functionality) then this would go some way towards redressing the balance.\nI suppose this is about art vs function, but it need not be.\n\nAs for me doing it myself, as I pointed out earlier, I would - except that I have no artistic talent! I can give a very subjective opinion, but alas, lack the skill to do anything about it. If circumstances were different, I'd be (re)producing icons right now instead of writing this post!\n\nFinally, I *do* respect the efforts of the KDE team and am *not* sniping, carping or criticising.\n:)\nRegards\nE"
    author: "Ezz"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-10
    body: "Well, Ezz, why don't you just use Gnome icons in KDE then?\n \nIt seems that KDE should have the larger icons as default, since there is so much misunderstanding about it."
    author: "reihal"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-11
    body: "Well, because they're <i>GNOME</i> icons.<br>\nI want to see similar KDE icons.<br>\nIf KDE looks better than GNOME (which it is beginning to), then the critics are confounded.<br>\n\nOver and definitely out!<br>\n:)"
    author: "Ezz"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-07
    body: "I would like to say a little bit more about Kde\nicons. I am affraid, I've been misunderstood.\nKde mimetypes icons are good example of artistic and techical proficiency. They are consistent, because they follow the same template. At the same time, by breaking the rectangular frame of the template icon, they are interesting and live.\nApplication icons has to be upgraded, and my suggestion is that kde artist follow the good example of mimetypes icons. They are, I am sure, aware of this. For example, kab (adress book) icon looks more like gnome icon, it is too dark, and if you put it on a dark background picture, you want see anything. It is simply not a part of kde visual identity. Another example is kstyle icon - it is a part of kde visual identity, but its size is problematic. When you change icons to 48x48, kstyle icon has only the half of this size.\nThis is disturbing, and does not look nice.\nThe third example is cookie icon. The meaning of\nword \"cookie\" is not \"birthday cake\". And with a candle light? Some poor newbies will think that cookies are something good, and that cookies should be enabled by default, even if they visit some very problematic internet sites.\nBut the most of kde2 application icons are beautiful, and kde artists deserve all respect.\nAnd we are going to give our contribution, aren't we?\nRegards,"
    author: "Zeljko Vukman"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-10
    body: "Look at MacOSX. They use icons up to 128x128 and scale them down. Or do they have icons in each size from 32x32 to 128x128? Isn't someting like that possible for KDE? I know if you need icons for each size it's an impossible work."
    author: "Martin Kerz"
  - subject: "Re: Miscellaneous KDE eyecandy"
    date: 2000-10-10
    body: "> Look at MacOSX.\n\nDid that.\n\n> They use icons up to 128x128 and scale them down.\n\nDoesn't work for small sizes properly\n\n> Or do they have icons in each size from 32x32 to 128x128?\n\nYes, they do have icons for 128x128, 64x64, 48x48, 32x32 and 16x16 IIRC. Everything above 48x48 is a waste of time imo.\n\n> Isn't someting like that possible for KDE?\n\nYou can do this already with KDE2. But don't expect me to paint all those huge icons for 0.01% of the users. I got better things to do. :-)\n\n> I know if you need icons for each size it's an impossible work.\n\nindeed\n\nTackat"
    author: "Torsten Rahn"
  - subject: "The New Style"
    date: 2000-10-05
    body: "For a look at the new style mosfet and the rest are discussing, point your browsers <a href=http://www.mosfet.org/newdefault.gif>here</a>"
    author: "Chris Aakre"
  - subject: "Re: The New Style"
    date: 2000-10-10
    body: "The tack is back! A tack is not tacky."
    author: "reihal"
  - subject: "More icons"
    date: 2000-10-06
    body: "Some extraKDE icons can be found at:\nhttp://home.uwnet.nl/~vita/linux/index.html\nAlso there: black & white icons which might make it to KDE later on..."
    author: "Ante"
  - subject: "err... 404 splash screens..."
    date: 2000-10-09
    body: "All the screens look the same... <BR>\nThey say something about a 404 :) <P>\n\nKeep up the good work guys... <BR>"
    author: "Sensi Millia"
  - subject: "Re: err... 404 splash screens..."
    date: 2000-10-09
    body: "If you can live with low resolution go here:\n\nhttp://www.martin-kerz.de/kdesplashscreens.htm"
    author: "Niels Soenderby"
  - subject: "Re: err... 404 splash screens..."
    date: 2000-10-09
    body: "If you have cached copies, let me know and I'll link to them or put them somewhere else.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: err... 404 splash screens..."
    date: 2000-10-09
    body: "Unfortune\u00e1tely I killed the splashscreens... oops.\nHas anybdy saved them on hardisk? I know this is'nt pofessional, but it happens... :("
    author: "Martin Kerz"
  - subject: "Re: err... 404 splash screens..."
    date: 2000-10-10
    body: "The links should work now! I'm really sorry ;)"
    author: "Martin Kerz"
  - subject: "what about letting people choose"
    date: 2000-10-11
    body: "How about have a default startup splash screen, but someone in the screen options, allow people to choose another one or use one of their graphics?"
    author: "Justin Goldberg"
---
Over the course of development of a project like KDE, a lot of art gets created.  Following, purely for your viewing pleasure, are some examples of art that have not yet made it to KDE CVS, for one reason or another.  First off, Martin Kerz offered several suggestions for the KDE2 splash screen: <a href="http://www.martin-kerz.de/startup1.jpg">startup1</a>, <a href="http://www.martin-kerz.de/alternative.jpg">alternative</a>, <a href="http://www.martin-kerz.de/krassnagel.jpg">krassnagel</a>, <a href="http://www.martin-kerz.de/colouredk.jpg">colouredk</a>.  Sreshth Kumar offered yet <a href="http://www.deutech.com/kde/splash.html">another</a>.  For the default WM style, Mosfet proposed an interesting <a href="http://www.mosfet.org/newstyle.gif">hybrid</a> a while back but apparently got outvoted (new proposals are in the works).  There is obviously a lot more of such art floating around -- if you have more links, feel free to post them in the comments.








<!--break-->
