---
title: "KDE2 is News (and Other Quickies)"
date:    2000-11-01
authors:
  - "numanee"
slug:    kde2-news-and-other-quickies
comments:
  - subject: "PR and Lobbying"
    date: 2000-11-01
    body: "Please, continue the attack.\n\nSend out e-mails, pdf-files, written letters, etc to all IT (Information Technology) related magazines, AND major international magazines.\n\nLinux together with KDE2 deserves ALL the backup it can get.\n\nMoney? I hope there is some funding for postal charges, somewhere...\n\nPlease, do not lose momentuum!"
    author: "Torben Skolde"
  - subject: "Re: KDE2 is News (and Other Quickies)"
    date: 2000-11-01
    body: "It was interesting to read the criticisms of KOffice. I have noticed similar problems with other KDE apps.\n\nI've not reported them, the reason being that KDE's bug system sucks so bad. If KDE used Bugzilla these things would get reported."
    author: "No-one"
  - subject: "Re: KDE2 is News (and Other Quickies)"
    date: 2000-11-01
    body: "<i>If KDE used Bugzilla these things would get reported.</i><p>\nAnd everything would be fine within a jiffy..."
    author: "Lenny"
  - subject: "Re: KDE2 is News (and Other Quickies)"
    date: 2000-11-01
    body: "<a href=\"http://bugs.kde.org/frontend/index.php\">http://bugs.kde.org/frontend/index.php</a>\n<br><br>\nis IMHO a LOT easier to use than bugzilla. I once tried to search the bugs in mozialla, and believe me, I failed. Check <a href=\"http://bugzilla.mozilla.org/query.cgi\">http://bugzilla.mozilla.org/query.cgi</a> if you don't believe me how horrible it is."
    author: "ac"
  - subject: "Re: KDE2 is News (and Other Quickies)"
    date: 2000-11-01
    body: "<p>Oh nonsense.  KDE's bug tracking system is simple enough to describe on a single page.  Bugzilla is an ugly monster, and even uglier to set up (believe me, I've tried).\n<p>\nReally, I've had no difficulty submitting bugs and even getting great feedback from the KDE developers."
    author: "Anon"
  - subject: "Re: KDE2 is News (and Other Quickies)"
    date: 2000-11-01
    body: "I wonder what KD/Embedded will look like? Will it run on the IBM watch or just the Compaq iPAQ ?"
    author: "Paul Leopardi"
  - subject: "Re: KDE2 is News (and Other Quickies)"
    date: 2000-11-01
    body: "If somebody writes a framebuffer-driver, there will be Qt/Embedded."
    author: "Lenny"
  - subject: "Re: KDE2 is News (and Other Quickies)"
    date: 2000-11-01
    body: "I suppose you meant \"KDE/Embedded.\"  There already is a Qt/Embedded, which is what the story on AllLinuxDevices was about.\n\n<p>\n\nAs for KDE/Embedded... it's already happening to the extent it makes any sense:  developers are porting some KDE apps over to the platforms supporting Qt/Embedded, such as the iPAQ and Cassiopeia.  The thing to keep in mind is that porting the entire environment over is pointless for a PIM.  You  just want consistency of interface."
    author: "Michael Hall"
  - subject: "Re: KDE2 is News (and Other Quickies)"
    date: 2000-11-01
    body: "There might be a point in porting the whole of KDE over to Qt/Embedded, since that would allow users to run KDE without also having to run X.  Granted, such users wouldn't be able to run legacy X apps, but then again who would want to?\n\nMy understanding is that Qt/Embedded supports some features (such as anti-aliased fonts) that would be nice to have and aren't supported by X, so I can see some reasons for going down this path.  Of course, it really depends on how much work it would be; if it's a lot then it probably wouldn't be worthwhile."
    author: "Kyle Haight"
  - subject: "Re: KDE2 is News (and Other Quickies)"
    date: 2000-11-01
    body: "\"My understanding is that Qt/Embedded supports some features (such as anti-aliased fonts) that would be nice to have and aren't supported by X, so I can see some reasons for going down this path.\"\n\n<p>\n\nThat's a good point.  My comment was mainly aimed at the notion of porting the entire environment and its related baggage over to handheld platforms."
    author: "Michael Hall"
  - subject: "KDE2 in your hand"
    date: 2000-11-01
    body: "I would love to see a KDE CEish thing for the iPaq. Also, I can see most of KDE being usefull on a handheld (I would just love to be able to do some DTP in KWord from a handheld), and I drool at the thought of SpaceDuel and KPat available to them me 24/7 (Eat this Zap!2000 [The game, not the cleaing solution] :-) ). Of course, korganizer, kmail, and a lot of konqueror would be usefull as well, also perhaps kmidi (detached from arts) to play midi files for music, which are convient because of their small size.\n\nFinally, although XF86 has internal fonts, couldn't a text message widget be reimplemented using QT, supporting it's own font system, and bypass XF altogether? XF also has a couple of advantages, namely it's great networking abilities, and 3d acceleration."
    author: "David Simon"
  - subject: "Re: KDE2 in your hand"
    date: 2000-11-02
    body: "I think a KPE (K Pocket Environment) would be nice. I think a lot of code from KDE can be used, but some programes must be rewritten for a PocketPC (e.g. an other desktop and panel which take account of the smaller display)."
    author: "Bernd"
  - subject: "Re: KDE2 in your hand"
    date: 2000-11-10
    body: "Not only could you run KPE on a PocketPC, but also perhaps on the Palm port of Linux. It would require a whole load of RAM, possibly would only run on a Palm Vx or the like, but it would work. The big reason I like Palms better then PocketPCs is simply that the batteries last way, way longer. This may change, however, with the popularity of NiMH rechargables."
    author: "David Simon"
  - subject: "Other window managers"
    date: 2000-11-01
    body: "<i>..Enlightenment, which supports both KDE and GNOME.</i><p>Has anyone had decent results using a different window manager with KDE 2? I've had ugliness with WindowMaker, severe ugliness with Enlightenment (E also uses a fake root window which causes major conflict with kdesktop) and nothing really usable with icewm.<p>Any good experiences to report?"
    author: "Otter"
  - subject: "Re: Other window managers"
    date: 2000-11-01
    body: "I'm using kdesktop, the GNOME panel and E 0.16.5 and everything works fine...\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: KDE2 is News (and Other Quickies)"
    date: 2000-11-01
    body: "Anyone know where I can find RPM's of the latest qt that KDE2 requires?  I found RPM's for a lot of the other KDE stuff on kde.org, but no RPM for qt.  I'm running Redhat 7, incidentally."
    author: "blue penguin"
  - subject: "Re: KDE2 is News (and Other Quickies)"
    date: 2000-11-01
    body: "rpmfind.net"
    author: "Ben"
  - subject: "Re: KDE2 is News: RPMs"
    date: 2000-11-02
    body: "For a slightly more elaborate answer, see my posting <A href=\"http://lists.kde.org/?l=kde-user&m=97299020422157&w=2\">Be careful: KDE 2.0 answers about RPMs and libs depend on distribution</A> on the kde-user archive.\n<br>\nI actually couldn't find Red Hat 7 RPMs for QT 2.2.1, so I listed an RPM for RawHide 1.0."
    author: "Paul  Leopardi"
  - subject: "Re: KDE2 is News: RPMs"
    date: 2000-11-03
    body: "So basically what this means is, that at this time, I cannot install KDE 2 Final using RPMs on RedHat 7?  I have to compile it myself?"
    author: "Blue Penguin"
  - subject: "Requirements of KDE2"
    date: 2000-11-02
    body: "What is the smallest PC you can run KDE2 on and still get a reasonable perfomance? Will a Pentium 100Mhz w 32MB of RAM do the trick?"
    author: "Birger"
  - subject: "Re: Requirements of KDE2"
    date: 2000-11-02
    body: "<p>This certainly depends on your definition of reasonable performance and what you want to do with your system.</p>\n\n<p>Nevertheless P100 and 32 MB RAM is probably too slow for the average user. I have 128 MB and a P200 and it is ok. I guess 64 MB and a P133 is the absolute minimum but I never tried that configuration...</p>\n\nciao, Marco"
    author: "Marco Krohn"
  - subject: "Re: Requirements of KDE2"
    date: 2000-11-02
    body: "<p>This certainly depends on your definition of reasonable performance and what you want to do with your system.</p>\n\n<p>Nevertheless P100 and 32 MB RAM is probably too slow for the average user. I have 128 MB and a P200 and it is ok. I guess 64 MB and a P133 is the absolute minimum but I never tried that configuration...</p>\n\nciao, Marco"
    author: "Marco Krohn"
  - subject: "Re: Requirements of KDE2"
    date: 2000-11-03
    body: "I got a 500Mhz 128MB PC, so KDE2 doesn't seem slow in any way, but according to 'top' it does take up a lot of memory, almost all I can throw at it.\n\nI remember once using win 3.11 on a P133 with 24Mb. That was fast, really fast, but not very usefull. Word 2 would start in \u00bd second. It was really cool though so I wonder if I'll ever get that experience again.\n\nPerhaps if one shaved all unnecessary daemons and stuff of the system and ported everything to fbcon? Of course, by the time you're finished, everyone will have 1Gz pc's w 256Mb probably."
    author: "Birger"
  - subject: "Re: Requirements of KDE2"
    date: 2000-11-06
    body: "\"What is the smallest PC you can run KDE2 on and still get a reasonable perfomance? Will a Pentium 100Mhz w 32MB of RAM do the trick?\"\n\nDefinatelly not. I have a Pentium 120Mhz and 32MB RAM at home. even KDE 1 isn't really useable on that machine. At Work I have a AMD K6/3 with 450Mhz and 386MB Ram. This machine rocks with KDE 2."
    author: "cdo"
  - subject: "Re: Requirements of KDE2"
    date: 2001-11-21
    body: "KDE1 IS isable with p120/32meg or ram!!! you would benefit from 48 meg or ram!\n\nKDE2 is UNUSABLE with p2-450 it is a slow bullucks!!!"
    author: "mike"
  - subject: "Re: Requirements of KDE2"
    date: 2005-10-30
    body: "I found this manufacturer  is selling the smallest pc in the world!!! Even CAR PC, Amazing products!!!\nHope you guys enjoy it!!!\n\nhttp://www.sd-omega.com\n\nMac mini or PC mini ? CAR PC ??? The smallest pc in the world!!!\n\n\nFull function Pentium 4 system\n800Mhz FSB\n\nProduct Features \n\nFully functional P4/Celeron Northwood 2.0Ghz ~ 3.20Ghz w/512KB L2 Cache \nFully functional P4/Celeron Prescott up to 3.0Ghz w/1M L2 Cache \nIntel 865GV / ICH 5, supporting 800MHz FSB \nFully support Intel Hyper Threading Technology \nOn-board 10/100 LAN, USB 2, 1394(Optional). \nUltra low noise and low power consumption \nSupport standard 3.5\" hard disk \n5.8\"(W) x 10\"(D) x 2.75\"(H) (14.8 x 25.4 x 7 cm) \nApplications \n\nHome Entertainment Center \nGateway, Router, Transportable PC \nPersonal Desktop, Office System \nIndustrial, Control, Data Collection, Data Center \nApplication Server, File Server, Game Server, ...etc. \nDetail Specifications \n\nProcessor\nIntel P4/Celeron Northwood 2.0Ghz ~ 3.20Ghz (512KB L2 Cache)\nIntel P4/Celeron Prescott up to 3.0Ghz (1M L2 Cache)\nSupport Hyper Threading Technology\nVRM 10.0 standard \nMemory\nSupport Dual-Channel DDR 400/333/266 DIMM\nMaximum 2 GB, with 512Mb technology \nCore Logic\nIntel 865GV / ICH 5(North / South)\nFSB support up to 800Mhz \nVideo\nIntel Extreme Graphics 2\nSupport up to 64MB DVMT video memory \nAudio\nSoftware AC '97 Audio CODEC \nLAN\nBuilt-in high speed Ethernet 100/10Mbps LAN controller \nFireWire (Optional)\nBuilt-in OHCI 1394a 1.1 compliance\nSupport up to 400Mb transfer rate \nUSB 2.0\nIntegrated 4 independent OHCI controller supporting USB 1.1 ports\nIntegrated 1 EHCI controller supporting USB 2.0 ports\nDynamic connection support to USB 2.0 or USB 1.1 devices \nIDE\nDual Independent ATA-100/66/33 support\nSupport up to 4 IDE devices on dual channel \nHardware Monitor\nSystem, processor temperature, voltage and fan speed monitor\nAuto Thermal FAN speed control \nPower Management\nACPI 1.0b compliance and OS direct power management\nWake-on event: RTC/USB Keyboard/Modem/LAN/Keyboard/Mouse \nSwitch and Jumpers\nFront Panel:\n1 x IEEE 1394 Firewire port\n1 x USB 2.0 / 1.1 Port\n1 x MIC Port\n1 x Ear Phone output Jack\nPower-on button\nReset button\nPower-on and HDD LED \nBack Panel:\n1 x RS232 COM port\n1 x parallel printer port\n1 x RJ45 10/100Mb Fast Ethernet\n1 x Line-Out output Jack\n2 x USB 2.0 / 1.1 Ports\n1 x 15 pin VGA\n1 x 19v DC Power Jack input \nDimension & Weight\n14.8 x 25.4 x 7 cm \n2.5KG (fully equipped) \nAC/DC Adaptor\nInput 100~240V AC Universal\nOutput 19V DC out \nStorage Devices\nSupport all Standard Slim CD-ROM/DVD-ROM, CD-RW, DVD-RW Combo\nSupport all 3.5\" standard IDE hard drive, ATA-100/66/33\n\nMore details from the manufacturer's website: http://www.sd-omega.com\n"
    author: "Andy"
  - subject: "KDE2 mirrors.... please"
    date: 2000-11-04
    body: "I spent 3 hours trying to download from that slow damn sourceforge site.\n \nPLEASE, PLEASE list mirrors."
    author: "bad"
---
<i>In KDE2 related news,</i> ZDNet have reviewed KDE2 and  <a href="http://www.zdnet.com/products/stories/reviews/0,4161,2644805,00.html">love it</a>.  Very much. LinuxToday.com.au like it too, as seen in this <a href="http://www.linuxtoday.com.au/r/article/jsp/sid/316480">article</a> titled after a well-known Australian TV advert.  LWN have provided coverage on the KDE2 release <a href="http://www.lwn.net/2000/1026/">here</a>. Trolltech congratulate KDE on the new release <a href="http://www.trolltech.com/company/announce/kde2.html">here</a>. <a href="http://www.suse.com/">SuSE Linux</a> have <a href="http://linuxpr.com/releases/2786.html">announced</a> their KDE2 update. Speaking of updates, new  KDE2 packages for Tru64 with fixes by Tom Leitner can be found <a href="ftp://finwds01.tu-graz.ac.at/pub/kde2/tru64/latest_version">here</a>.  

In the food-for-thought dept, <a href="mailto:kreichard@internet.com">Kevin Reichard</a> submitted this KOffice <a href="http://www.linuxplanet.com/linuxplanet/previews/2558/1/">article</a> by dep, and this <a href="http://www.linuxplanet.com/linuxplanet/reports/2552/2/">article</a> on <a href="http://www.enlightenment.org/">Enlightenment</a>, which supports both KDE and GNOME, while providing a fascinating desktop experience.

<i>And, finally, in Qt-related news,</i> a new beta of Opera for Linux is <a href="http://www.opera.com/linux/">available</a>. Even more exciting, Trolltech have made the announcement that Qt/Embedded will be available under the GPL.  Read the <a href="http://www.trolltech.com/company/announce/gple.html">official announcement</a> and an interesting interview by All Linux Devices <a href="http://alllinuxdevices.com/news_story.php3?ltsn=2000-10-30-002-03-NW-DV">here</a>. Watch this space closely for more coverage on this important new development and its potential impact on KDE. 







<!--break-->
