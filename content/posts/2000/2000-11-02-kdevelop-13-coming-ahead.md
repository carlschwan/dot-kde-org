---
title: "KDevelop 1.3 is coming ahead !"
date:    2000-11-02
authors:
  - "rnolden"
slug:    kdevelop-13-coming-ahead
comments:
  - subject: "KDE Studio ?"
    date: 2000-11-02
    body: "Hi,\n\nI use KDE Studio for developing in KDE2 ( see the website of www.thekompany.com ). It has KDE2 support for a long time and run native on KDE2 !! I use the CVS snapshop version, very cool, it also has Designer support etc.\n\ngive it a try !"
    author: "Herwin Jan Steehouwer"
  - subject: "Re: KDE Studio ?"
    date: 2000-11-02
    body: "I checked out KDE Studio from cvs, and ran \"make -f Makefile.cvs\". This gave the following:\n\n*** Concatenating configure tests into acinclude.m4\n*** Creating list of subdirectories in subdirs\n*** Searching for subdirectories...\n*** Retrieving configure tests needed by configure.in\nmake[1]: aclocal: Command not found\nmake[1]: *** [cvs] Error 127\nmake: *** [all] Error 2 \n\nAny idea what I'm doing wrong?"
    author: "Haakon Nilsen"
  - subject: "Re: KDE Studio ?"
    date: 2000-11-03
    body: "You don't have autoconf and/or automake installed."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: KDE Studio ?"
    date: 2000-11-03
    body: "Thanks, turned out to be a whole lot of stuff I haven't installed :)\n\nI'm almost there now, but I get this error:\n\n/usr/bin/ld: cannot find -lfl\n\nI cannot find any lib called \"fl\" so I'm stuck.. What to install?"
    author: "Haakon Nilsen"
  - subject: "Re: KDE Studio ?"
    date: 2000-11-03
    body: "flex"
    author: "Rob"
  - subject: "Re: KDE Studio ?"
    date: 2004-10-28
    body: "I have the same trouble.  \"lf: cannot find -lfl\"  This is during the X11 installation.\n\nHowever I know that I do have flex installed.\n\nI get this error trying to install XFree86 or xOrg as my X11.\n\nFor me, this is on a completely clean machine with nothing installed except for Linux.\n\nAnybody have another answer?"
    author: "frozenJim"
  - subject: "Re: KDE Studio ?"
    date: 2005-07-05
    body: "I think you must install flex.\nBye."
    author: "Antonio Bardazzi"
  - subject: "Re: KDE Studio ?"
    date: 2000-11-02
    body: "Anyway which one you use, have a look at:\n<p>\nhttp://www2.linuxjournal.com/lj-issues/issue79/4323.html\n<p>\nKDevelop won the third place directly after GCC and CodeWarrior. We\u00b4re doing it for free, whoever wants to use it, use it. We\u00b4re helping everyone in problems and invite everyone helping to improve KDevelop. And most importantly, the code is almost a 100% written by our team - while I won\u00b4t make a comment about other IDE\u00b4s..."
    author: "Ralf Nolden"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-02
    body: "I'm still not sure.  Will KDevelop 1.3 link against KDE1 or KDE2?  I understand that it still supports KDE2 development either way.  :)\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-02
    body: "From what I've seen on the kdevelop-devel list it will be using the KDE 1 libraries."
    author: "Richard Moore"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-02
    body: "That is THE real show stopper.\nThere are no more QT 1.xx packages available for debian (woody), kde1 never was.\nSo where is the reason to give kdevelop a try?\nIt still depends on anachronistic software."
    author: "tracer"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-02
    body: "If you are a programmer, you can get KDE1.\n\nThe code you develop, you will develop easier, and it will not depend on KDE 1. That is the reason to use kdevelop: it's a damn good IDE!\n\nWhat more reason do you need?"
    author: "Anon. Advocate."
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "<i>That is the reason to use kdevelop: it's a damn good IDE! What more reason do you need?</i><br>\nThere are other IDE available. Before I know myself, which of them are damn good, I must try them. And I just don't give it a try, if it is not a native KDE2 app.<br>\nThere are some apps which I'm missing from KDE1, e.g. krabber, but as long as they are not ported, I just live without them.<br>\nBut, I can live without rabbing CDs for a while, but I need to build some small apps NOW, so I take what is available now _and_ fit my needs."
    author: "tracer"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-02
    body: "<P><EM>That is THE real show stopper. There are no more QT 1.xx packages available for debian (woody), kde1 never was. So where is the reason to give kdevelop a try? It still\n      depends on anachronistic software.</EM>\n</P><P>\nI think KDE packages were made available for Debian in the non-free branch.  If qt is no longer available that's Debian's fault, and in any event you can compile qt yourself. Qt 1.4 -- which currently runs on millions of computers -- is hardly \"anachronistic\".\n</P><P>\nI'm sure the KDevelop team is working on upgrading to KDE 2, but it takes a bit of time. Try learning the virtue of patience, or roll up your sleeves and help port it to KDE 2 ;-)\n</P>"
    author: "Dre"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "<i>I'm sure the KDevelop team is working on upgrading to KDE 2, but it takes a bit of time. Try learning the virtue of patience, or roll up your sleeves and help port it to KDE 2 ;-)</i>\nMaybe I would have done that, but a look into the daily snapshot shows, that even the current code depends on qt1.<br>\nSo in the world of free software, I have the choice just to chose another IDE.<br>\nTo make my statement clearer:<br>\n<ul>\n<li>I see what great work has been kdevelop 1.x. Thanks to the programming team.</li>\n<li>I am sureley able to fetch all tarballs, and build everything myself. But I just don't want to. I want to have (now where KDE2 is released) an IDE that runs on KDE2.</li>\n</ul>\nAnd I think, many people have the same opinion. So, what I mean with the \"show stopper\": If KDevelop 2.x isn't available (even as a Beta) soon enough, many people will just switch to another IDE."
    author: "tracer"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-06
    body: ">If KDevelop 2.x isn't available (even as a Beta) soon enough, many people will just switch to another IDE<br><br>\n\nYes, but please consider that KDevelop isn't a product by a company or similar. KDevelop is a community project and everyone can help to implement it. \n<br>\nA KDevelop2 release will only depends on how much help the project got, NOT when it's time to release something."
    author: "Sandy Meier"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-06
    body: "<i><b>\n>If KDevelop 2.x isn't available (even as a Beta) soon enough, many people will just switch to another IDE\n</b></i>\n<br>\n\n<i>Yes, but please consider that KDevelop isn't a product by a company or similar. KDevelop is a community project and everyone can help to implement it. \nA KDevelop2 release will only depends on how much help the project got, NOT when it's time to release something.</i><br>\n\nOne very good point of you, I even use it myself when arguing about opensource. It is released, when it is ready.\n<br>\nJust to make it clear again, I'm not demanding anything, but I would have loved to have a KDE2 based Kdevelop NOW.\n<br>\nIf it is possible for me to help out finishing KDevelop2 (or get it to a usable beta), I'm pleased to do so, but I want a straight environment.\n<br>\nThe \"SHOW STOPPER\" was not meant to offset anybody, just to impress my feelings.\nYeap, I still say \"anachronistic\" to QT1/KDE1.x (it become, when GNOME 1.2 was released), but I know that everybody producing free software has the right to do with it, what he wants.<br>\nI just urge that's bad for KDevelop to release a \"clean\" 2.x version too late.<br>\nkudos, tracer"
    author: "tracer"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-10
    body: "<p>&gt; <i>I'm sure the KDevelop team is working on upgrading to KDE 2, but it takes a bit of time.</i></p>\n<p>No. KDevelop2 is being coded from scratch, not adapted from KDevelop1. And there ain't many people coding it.</p>\n<p>Maybe some coders can try to port KDev1.3 to KDE2's libraries.</p>\n<p>If you want to learn more, take a look at the <a href=\"http://fara.cs.uni-potsdam.de/~smeier/www/fforum/list.php3?num=1\">KDevelop Forum</a>, there is a lot of question and comment about KDev and KDE2.</p>"
    author: "GeZ"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "KDE 1.x was never officially supported in Debian due to the licensing battle. Now that KDE2 solves this, I'm not sure why the KDE 1.x \"unofficial\" tree disappeared -- you'd have to ask the individual person who maintained it.\n\nIn any case, the lazy thing to do is obtain the 1.x RPM for what you need, then \"alien\" it to a proper .deb file and dpkg -i it.\n\nAlien isn't the preferred method of obtaining debian packages, but it works great for *non-system critical* software (i.e. this should be fine but don't alien XFree86 4.0!)\n\nScott"
    author: "Anonymous"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "<i>KDE 1.x was never officially supported in Debian due to the licensing battle. Now that KDE2 solves this, I'm not sure why the KDE 1.x \"unofficial\" tree disappeared</i><br>\nIt hasn't disappeared. It's still available at tdyc.com. Just qt1 has dissappered from debian.<br>\n\n<i>In any case, the lazy thing to do is obtain the 1.x RPM for what you need, then \"alien\" it to a proper .deb file and dpkg -i it.\n</i><br>\nBefore I would alien an rpm, I would prefer to build it myself.<br>\n\n<i>Alien isn't the preferred method of obtaining debian packages, but it works great for non-system critical software (i.e. this should be fine but don't alien XFree86 4.0!)</i>\n<br>\nTheres no need for such a thing. Have a look at the xfree86 task force (www.debian.org/~branden).\nThere are binaries for IIRC i386 and sparc."
    author: "tracer"
  - subject: ":-("
    date: 2000-11-02
    body: "In KDevelop 1.2, I have to browse through the opened documents by using the Window menu. :-(\nWill there be a feature compareable to GnomeMDI (MDI browsing through tabpages)?"
    author: "Anonymous"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-02
    body: "Does anybody know, if future version of KDevelop\nwill also support other languages like Java, Python, Ruby or Perl.\n\nI do not know, if this would be a hard task to implement this, but I think a lot of programmers\nwould like it."
    author: "markus jais"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-04
    body: "Well there is a new IDE coming for perl and python. Its based on Mozilla being produced by activestate.\nIts called Komodo and they just had a Technology preview release for just win 2000, but they will have versions for Linux as well.\nhttp://www.activestate.com/Products/Komodo/index.html"
    author: "mike"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-05
    body: "From what I've read on the site, KDE2 is supposed to have support for plug-ins, so you will be able to make your own plug-ins for different languages."
    author: "acidos"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-05
    body: "By KDE2 I meant KDevelop 2.0 .. sorry, its been a long day."
    author: "acidos"
  - subject: "automake integration? Woowie!"
    date: 2000-11-02
    body: "KDevelop will understand existing automake/autoconf projects? That's it! Now I *will* start coding for KDE! :)\n\nSeriously, for some it may not be important but many of us migrating ex-windows-programmers really have become dependant on a good IDE. KDevelop has been good (and very stable) for quite a while now, and I've done some smaller qt-experimentation programs with it, but this is a real killer feature since almost all programs distributed as source use automake/autoconf and this will help developers trying to get on board on existing projects.\n\nGreat work!"
    author: "jd"
  - subject: "When will a native Kde 2 version be available?"
    date: 2000-11-03
    body: "From the comments posted so far, I understand that KDevelop 1.3 will require Qt 1.44 and Kde 1.12, at least the support and libs.  It will not be based on Qt 2.21 and Kde 2.0, although it will generate Kde 2 compatible code if so desired.  If this understanding is incorrect please inform.\n\nI also understand that a Qt 2.21/Kde 2.0 based KDevelop is also being worked on.  When will a usable version be available?  I've compiled the snapshots a few times over the last few months, but not within the last few weeks. It compiled but was mostly non-functional. \n\nBack when I was using kde 1.12 Kdevelop was very, very nice, even though I didn't use it much as I was not doing much Kde app development then.  Looks like I'll have to reinstall Qt 1.44 and Kde 1.12 just to use it again. The built in debugger and class browser is worth it, in my opinion.  \n\nI guess the focus will shift to a native Qt 2.21/Kde 2 version after the release of 1.3.  An estimate on when the new version will be available (a usable beta that is mostly functional, not perfect) would be greatly appreciated. Also, if the current snapshots are halfway usable now that would be better than nothing.\n\nJohn"
    author: "John Califf"
  - subject: "Re: When will a native Kde 2 version be available?"
    date: 2000-11-03
    body: "This info perheps help you.\n[[[Taken from \nhttp://www.kdevelop.org/fforum/read.php3?num=1&id=2393&thread=2360]]]\n\n> When can we expect a KDevelop 2.0 version which will run under KDE2.0 \nCurrently we have only 3 or 4 active developers which started rewrite KDevelop. Rewriting is necessary because of the huge feature list which forces to a new approach to make all wishes possible. That's why do not expect a finished version 2.0 in the next half year. \n \n> is there already a release schedule ? \nNot in the current state of rewriting. But we could need some more developers. Interested? \n \nBut there will be KDevelop-1.3 in 1 or 2 months. It is mainly a reaction to TT's Designer, Qt-Linguist, KDE2 release, Qt-2.2"
    author: "attila sulyok"
  - subject: "Re: When will a native Kde 2 version be available?"
    date: 2000-11-03
    body: "It would be nice if KDevelop 1.3 was ported to KDE 2.0 without all the rewrite stuff.  Then people wouldn't have to install KDE1!"
    author: "KDE User"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "Really, KDevelop should be ported to KDE2.0/Qt2.2.1 as soon as possible. KDE2.0/Qt2.2.1 is so much better than KDE1.1.2/Qt1.4* that I think it is just not worth trying to stick with this framework. So,\nplease, port it to KDE2.0/Qt2.2.1, it will be\nbetter for everyone. And people who mantain their\nKDE1.* applications should also port them to KDE2.0 - it not so much work. I have ported a quite complex application from Qt1.44 to Qt2.0 in less than one hour, so..."
    author: "Bojan"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "It seems that the KDevelop team decided to make the kde2 port and a rewrite for new features at the same time. Unfortunately the latter seems to have taken longer time than planned. I don't know how much effort would it take to port the 1.3-tree to KDE2.0 separately, but it would be nice not to depend on those soo old kde1/qt1-libraries while waiting for the KDevelop2.0 :)"
    author: "ac"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-05
    body: "<i>I don't know how much effort would it take to port the 1.3-tree to KDE2.0 separately, but it would be nice not to depend on those soo old kde1/qt1-libraries while waiting for the KDevelop2.0 :)</i><bR>\nMaybe one could make a cvs split, at least till the \"original\" version will compile against current libs?"
    author: "tracer"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-04
    body: "Well, then go ahead and port KDevelop 1.3 to\nKDE2 in less than an hour. Nobody stops you."
    author: "Bernd Gehrmann"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-05
    body: "<i>Well, then go ahead and port KDevelop 1.3 to KDE2 in less than an hour. Nobody stops you.</i><br>\nMaybe not porting (using all new features), but at least make it compile using curent libs?"
    author: "tracer"
  - subject: "MDI support"
    date: 2000-11-03
    body: "In KDevelop 1.2, I have to browse through the opened documents by using the Window menu. :-(\nWill there be a feature compareable to GnomeMDI (MDI browsing through tabpages)?"
    author: "Anonymous"
  - subject: "Re: MDI support"
    date: 2000-11-06
    body: "There will be MDI/SDI support in KDevelop2."
    author: "Sandy Meier"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "I can't get even kdevelop 1.2 installed. I'm using KDE2, which removed KDE1.1.2 when I installed it. Kdevelop requires KDE1 libs, but <B>I can't find KDE1 libs RPMs any longer from anywhere!</B> Especially for RedHat 6.2. I don't want to compile them.<P>\n\nWhy on earth was necessary to remove all KDE1.1.2 directories from all mirrors????"
    author: "Marko Gr\u00f6nroos"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "Surely you must have heard of rpmfind and google.\nI mean, just a minute ago I was looking at RH6.2\nrpms for KDE 1.1.2."
    author: "ne..."
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "Yeh, thanks. They seem to be in RH6.2 install directories too. But it would have been nice to find those from the kde/stable directories in FTP sites. Requires too much thinking to find them elsewhere... 8-/"
    author: "Marko Gr\u00f6nroos"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-04
    body: "Sheep. Using the RH6.2 libs was really a mess. Had to fight a day before I got it working.<P>\n\nThe problem is that KDE2 wants to be installed in /usr. It doesn't like to be relocated, for some reason. But KDE1 wants to be installed there too. So, I relocated KDE1 to /opt/kde1. Fine.<P>\n\nFirst problem is that the kdevelop-autoconf-whatever default path for KDE includes is /usr/include/kde. Ok, edit the acinclude.m4.in manually to change that to /opt/kde1/lib. Of course the automation doesn't now work on other machines, but what the hell.<P>\n\nSecond problem is that the rpm relocate did not appropriately alter libkdecore.la, now in /opt/kde1/lib. It internally points to /usr/lib, where the wrong version of libkdecore is, so you have to change that too.<P>\n\nHey, it works now (at least on this machine)! A mess, eh?<P>\n\nI really wonder how a beginner could ever get the program working under KDE2.<P>\n\nIt also seems that kdevelop doesn't know how to generate Qt-2.2.1 dialog sources. Thus, can't do my project with KDE2/Qt2."
    author: "Marko Gr\u00f6nroos"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-08
    body: "Please see my posting <A href=http://lists.kde.org/?l=kde-user&m=97299020422157&w=2\">\"Be careful...\"</A>\n<br>\nThe directory structure for KDE and KDE 2 which the RPMs assume depends very much on which distribution and version the RPMs were built for."
    author: "Paul C. Leopardi"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "Hey, guys!  KDE is not Microsoft Windows!  Keeping KDE 1 around and tucked into /opt/kde1 *won't* hurt you!  The existance of a new version does *not* make all old versions worthless!\n\nYes, it'd be nice if KDevelop would look like KDE 2 apps, but does that make it less useful to you?\n\nIf you really want KDevelop, there are two choices: help the KDevelop 2 effort, or install KDE 1."
    author: "Neil Stevens"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "I would rather not clutter up my hard drive with obsolete libraries...  Sorry, but that is just how I am.\n\nBut since it is _my_ harddrive and not yours, I will wait for KDevelop 2.0 before I install it.  \n\nIt is a shame that they didn't port to 2.0 first, then start adding features to the new system...  That way they would have only had one unknown at a time...  This way when they run into a problem developing a new feature they won't be sure if it is a QT 2.0 bug, or an implementation problem.\n\nSolving for one variable is always easier than solving for two variables.  It would have gone much faster."
    author: "Jimmy the Geek"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-04
    body: "I ported KDevelop to KDE2.0 about 14 months ago,\nwithout adding any features. It turned out that the\ncodebase was in such a state that it would get\ncompletely unmaintainable when adding new \nfeatures. So I introduced a new component system\nwhich made it much easier to modularize KDevelop\nand separate its subsystems into separate units.\nLater we used KParts to put the whole component\nsystem on a more advanced foundation. So your\nway of describing the KDevelop history is completely\nwrong.\n\nWhat's also wrong is that Qt 2.0 are a problem. Of the\nwhole code KDevelop is based on, Qt is definitely\nthe most stable."
    author: "Bernd Gehrmann"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-05
    body: "You don't need all of KDE 1.0\nJust QT 1.45 and the KDE 1.1.2 libraries.\nI'm running KDE 2.0, and have KDevelop running this way. It's only really another 10Meg or so ( over and above KDK 1.2 that is ), and you can delete em from your hard drive when KDevelop 2 comes out ;-).\nIf you're that straped for space, then I dont know what else to tell ya *grin*"
    author: "Eddy"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "Hello !\n\nWhat is 'linguist' ?\nThanks in advance.\n\nbye Uwe"
    author: "Uwe"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-03
    body: "Uwe, come on now. Browse the dot.kde site, read\nALL the articles, especially the ones for Tues.\nOct. 24. You'll read all about it then."
    author: "ne..."
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-04
    body: "How about providing statically linked binaries of KDevelop1.3, so that people won't have to install the KDE1 libs? Granted, this may be extremely naive wrt file sizes, and even legal matters..."
    author: "Haakon Nilsen"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-07
    body: "Hi, is possible uses the KDE 2.0 with KDevelop 1.1 and Qt 1.44? My Linux is Mandrake 7.2."
    author: "Daniel R. Brahm"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-07
    body: "Yes, it should be possible. Maybe Mandrake distribute a KDE1 compatible package?\nPlease read also http://www.kde.org/kde1-and-kde2.html"
    author: "Sandy Meier"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-08
    body: "I took a look at <A href=\"http://www.kde.org/kde1-and-kde2.html\">http://www.kde.org/kde1-and-kde2.html</A> very recently. It seems out of date to me, since it only talks about KDE 2 snapshots, assumes you will want to run KDE1 as primary, and does not take into account the distributions for which the KDE 2 RPMs replace KDE1. The page also says nothing about the Mandrake KDE1 compatibility library.\n<br>\nAs at 5 Nov, David Faure ( david@mandrakesoft.com ) said he was too busy bugfixing to look into updating this page.\n<br>\nMy problem is I only run SuSE and I suspect this page is not right for SuSE.\nI wish I could get this all sorted out, but even for SuSE, this will have to wait until next week when I have a working Jaz drive and less Mathematics assignments to worry about."
    author: "Paul C. Leopardi"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-08
    body: "If you have SuSE it's very easy. Just install the KDE1 and KDE2 rpm's and all should work. \nOn my machine it does. (SuSE7.0)"
    author: "Sandy Meier"
  - subject: "Re: KDevelop 1.3 is coming ahead !"
    date: 2000-11-08
    body: "Thanks for the reply. I installed from the RPMs at KDE and am left with the following post-installation issues:\n<UL>\n<LI>\nI'm still using old kdm.\n<LI>\n/opt/kde2/bin is not in default path or my current $PATH\n<LI>\n$KDEDIRS is /etc/opt/kde2;/opt/kde2 but $KDEDIR is /opt/kde\n<LI>\nA number of config files under /sbin and /etc have /opt/kde hardcoded.\n<LI>\nI am not sure which files to change and which to leave alone, as I have seen no documentation on this.\n</UL>\n<br>\nTo check your system, do a Find file on files containing /opt/kde under /sbin and then the same in /etc. Then do the same for /opt/kde2 and $KDEDIR\n<br>\nOnce you have these results you will need to look up the manuals to figure out which config file is an input file to SuSEConfig, etc.\n<br>\nIf you have the time, please do this, post the results somewhere public and let me know."
    author: "Paul Leopardi"
  - subject: "KDevelop for KDE 2 in 6 months?"
    date: 2000-11-11
    body: "No offense to the KDevelop people but I won't be installing KDE 1 libs to use it.\n\nThere is as far as I can tell no detailed instructions for getting it working on newer systems like Mandrake 7.2.\n\nThis probably sounds silly to many command line linux developers but I don't enjoy configuring my system in order to program.  In fact I refuse to install anything that isn't installable via RPM.  I program mostly in PHP and Python and have used Kdevelop for the occasional C++ program.\n\nThe Kdevelopers insistenctence on maintaining and updating outdated code base seems a tad bit silly."
    author: "Bryan Br"
  - subject: "Re: KDevelop for KDE 2 in 6 months?"
    date: 2000-11-14
    body: "KDevelop1.3 is't a outdated code base, because there is no newer one. For KDevelop1.3 we will create a package which will work on every Linux distribtion. You don't need to have to install KDE1. \nThere is also a preview which works on Mandrake 7.2\n\nftp://ftp.kdevelop.org/stud/smeier/kdevelop/"
    author: "Sandy Meier"
---
Breaking news! The <A HREF="http://www.kdevelop.org/">KDevelop Team</A> will release version 1.3 of its C/C++ IDE for UNIX within the next couple of weeks. The new release has a lot of new features regarding KDE 2 and Qt 2.2 development, particularly a project file generator which creates a project file out of your existing automake/autoconf project on the fly so you can start working with KDevelop on your former project within seconds. I´ve tried this last night with the kdebase module - and it worked!


<!--break-->
<p>
Furthermore, the new version supports the new Qt Designer and Linguist (so you can create translations for your Qt 2.2 project just like for a KDE project type) and the KDE 2.0/Qt-2.2 templates are based on KDE´s /admin directory (so you can turn your Qt project into a KDE project with little change). Additionally, the KDE 2/Qt2.2 projects make use of the new API with XML, KAction and QAction stuff.  Finally the new <A HREF="http://dot.kde.org/972566642/">KDE 2.0 Development Book</A> will be included (hopefully it´ll be released soon!), so KDevelop will be your first shot for KDE 2.0 Development.
<p>
Please note that KDevelop 1.3 will be shown together with KDE 2.0 at the <A HREF="http://www.systems.de/">Systems Expo in Munich, Germany</A> from Nov. 7th to 10th and at the <A HREF="http://www.key3media.com/comdex/fall2000/index.html">Comdex expo in Las Vegas</A> from Nov. 13th to 17th. at the KDE booth - meet you there!


