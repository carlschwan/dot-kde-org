---
title: "Korean support for KDE2"
date:    2000-09-28
authors:
  - "numanee"
slug:    korean-support-kde2
comments:
  - subject: "Re: Korean support for KDE2"
    date: 2000-09-27
    body: "This looks wonderful :-)\nI hope KDE2 will become a success in Korea."
    author: "Case Roole"
  - subject: "Re: Korean support for KDE2"
    date: 2000-09-28
    body: "A minor correction: BIG5 encoding is not for Korean but for Traditional Chinese(used in Taiwan and Hong Kong)\n\nDoes KDE2 work well with Chinese characters? if not I can probably add chinese support when I get some free time later."
    author: "Y2K"
  - subject: "Re: Korean support for KDE2"
    date: 2000-09-28
    body: "Hmmm, thanks.  I wasn't so sure of that, though I thought I'd seen it called that way somewhere.  I'll drop the \"Big 5\" from the article.\n<p>\nI'm not sure about Chinese characters. There is a simplified Chinese team listed at <a href=\"http://i18n.kde.org/\">http://i18n.kde.org/</a>.\n<p>\nCheers,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: Korean support for KDE2"
    date: 2000-09-28
    body: "kde2 works well in big5 encoding\n, and can generate correct ps file now"
    author: "MingChe"
  - subject: "Re: Korean support for KDE2"
    date: 2000-09-28
    body: "Awesome! Does it need special patch or just work out of the box? What fonts do u need?\n\nThanks"
    author: "Y2K"
  - subject: "Re: Korean support for KDE2"
    date: 2000-09-28
    body: "I've been waiting for this, I have a lot of \nKorean customers who are frustrated with not\nbeing able to type/read Korean on their computers.\n\nGreat work."
    author: "This is great"
---
Well, this is pretty cool. Bumchul Kim has updated his <a href="http://www.mizi.com/kde/">webpage</a> with patches and details on what's needed to make KDE CVS and Qt 2.2.0 work well with the Korean charset.  Includes cool <a href="http://www.mizi.com/kde/screenshot/20000909/index.html">screenshots</a> and a patch for an <a href="http://www.mizi.com/kde/doc/onthespot/onthespot.html">OnTheSpot</a> implementation in Qt.  The <a href="http://lists.kde.org/?l=kde-core-devel&m=96996813103895&w=2">good news</a> is that a lot of the KDE patches have already made it to CVS.




<!--break-->
