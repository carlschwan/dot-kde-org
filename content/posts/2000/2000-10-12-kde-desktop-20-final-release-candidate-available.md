---
title: "KDE Desktop 2.0 Final Release Candidate Available"
date:    2000-10-12
authors:
  - "Dre"
slug:    kde-desktop-20-final-release-candidate-available
comments:
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "Just posted this to slashdot... there publicity for you :D"
    author: "Chris Aakre"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "Not that they would post it..."
    author: "Claus Wilke"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "Oh c'mon... they've linked to previous KDE2 betas... they stopped after the 2nd or 3rd one though (understandably.. how many betas will you link to?)... \n\n<P>but really, i don't think getting everyone all excited over RC2 (outside of those of us who are already kde fans) is all <B>that</B> important... i'd be more concerned and excited about PR on the actual KDE2 release."
    author: "stilborne"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "They posted it."
    author: "ac"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "Posted on Slashdot? Your asking for troubles if you do that.\nVery soon we will get flamewars the size of the maximum number you get when using a 64-bit integer!\n\nWhy we get flamewars?\nBecause it's Slashdot.\nThey always complain about everything, even world peace."
    author: "Anonymous"
  - subject: "Re: Flamewar"
    date: 2000-10-12
    body: "In any sufficiently large group, some people will dislike any given thing. The anonymity of a weblog allows people to express themselves perhaps more fervently than they would in other settings. Some people don't like KDE. Some of them read Slashdot.<br><br>\nTo be fair, some of the people on Slashdot (myself included), prefer KDE to its \"alternatives\". Don't concentrate only on those who disagree with you. Slashdot is a gathering place for people with different viewpoints, some of whom have not decided between KDE and its \"alternatives\". The shrillness of KDE's detractors only serves to make the KDE team more attractive to these neutral readers.<br><br>\nI think every story posted on this site should be cross-posted to Slashdot automatically. This could address some of the issues that inspired yesterday's post here on improving the KDE's PR."
    author: "A Coward on a Mouse"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "Trying to get konqueror (1.93) to get to the mandrake packages seems impossible, whereas mozilla reads them nicely... (konqueror just says &amp;quot;can't read /pub////rpms&amp;quot;).\n&lt;p&gt;\nWill this go away if I succeds getting theese, or should I give up???"
    author: "anders"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "Is the terrible treatment of the HTML a bug in your cgi app, or in konqueror????????\n<p>\nworks without preview???"
    author: "anders"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Availa"
    date: 2000-10-12
    body: "It's a bug in Konqueror.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Kicker Plugin shuffle"
    date: 2000-10-12
    body: "I was pleased to see the long awaited \nxeyes-kicker-plugin, that is really funny,\nbut I was sad to see that the kasbar applet\nwas missing in the suse6.4 rpms. I had to take kasbar from beta5.\n\nDoes anyone know, if it will be included in\nthe official release?\n\nGreetings, Matthias\n-- \n... left outer join auth on ..."
    author: "Matthias Lange"
  - subject: "kasbar"
    date: 2000-10-12
    body: "The package was moved to kdenonbeta, because it was not deemed ready for release now. YOu can still use it though and I think it will be included in KDE 2.x. \nJust compile it yourself if you can't get the binary."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "This worries me. Lets all take a moment here.\n\n\"Release candidate\"?\nI installed it on my laptop with the recently installed RH70 system updated with all erratas, and the amount of errors I encountered in KDE2 are too many to start listing - it started already on installing the RPMs, the kdelib-package created error messages.\n\nI'm not talking about former applications installed on kde1.2 thats no longer working - this is experienced with the supplied kde2-apps.\n\nKonquer cries for \"xkill\" frequently, a lot of problems with the mimetypes (xmms won't play files unless opened within xmms) and so on.\nIs javascript and java supposed to work?\n\nI don't wanna sound sound too pessimistic and certainly not kill any motivation around, but if this is actually a *real candidate* to be released as the official distribution, I'm afraid that it's gonna do a lot of bad stuff to the good KDE reputation. This did not even apprear as a Beta to me - maybe an Alpha-candidate?\nPlease don't rush this guys - release it when it's ready. I can live with this, but a newbie would be scared away from their Linux-curiosity sooner than you'd say \"Millennium\".\n\n\n(Hey - I don't know if this is purely a problem related to the packaging. If so I guess it's easily solved, and I'd be happy to hear that.)"
    author: "King Egypt"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "Wow, guy, you might wanna check your installation.  I'm running a pre-RC2 version of the KDE, and I haven't had to hit konqueror with an xkill in quite a while.  Everything on my system is running quite well.\n\nJava and Javascript?? That's been working for quite a while too.  Make sure that you download the JDK and configure konqueror to use it."
    author: "Christopher Lee Fleck"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "So what you're basically saying is: There's hope? ;-)\n\nOk, a few quickies to sort things out here:\nDid you install the RH70 RPM's? And all of them? No error messages during install?\nDid you remove older KDE-versions prior to the kde2 install?\nDid all the software included work? Have you got both Javascript and java-applets working in Konquer?\n\nThanx, man."
    author: "King Egypt"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "I have just installed kde2 rc2 on my RH7.0 and it works fine.\n\nThere were no error messages during installation.\nBefore installing the RPM's I removed all old kde RPM's.\nI have no problem with the included software so far.\nHave not tested Konquerre that must, since there is no support for squid proxy, but for local network browsing it works fine."
    author: "Peter Bay"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "I had a quick question regarding KDE (1.1.2) on Red Hat 7.0. This might depend on what XFree86 version that I'm running, in combination with my video card (it's supported though). When I startx, before K is fully loaded, the screen flickers A LOT... first, did you see this in your install? second, does KDE 2 do the same thing? I'm trying to determine what to do, that 'other' gui doesn't do it at all, so it's almost like KDE and XFree4 just don't like each other. While it's not a big deal, I'm just hoping that it doesn't happen with KDE 2. (sorry guys, I realize that this post isn't exactly on topic, I just really need some help and have exhausted all my other resources)...\n\n-r-"
    author: "Ryan V..."
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "I'm running XFree 4.0.1 with a Matrox G400DH and I have not seen any flicker. The same goes for KDE 1.1.2.\n\nSorry that I can't help you."
    author: "Peter Bay"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-17
    body: "I agree with the *release when it's ready* point - \nAlthough I haven't seen *exactly* the same problems - I have seen some significant repeating problems in Beta 3, Beta 5, and now RC2.\n\nI've posted the following problem to submit@bugs.kde.org twice - but I don't see my most recent post (sent yesterday) yet.\n\nHere's the simple scenario...\n\nstart kde... fine - minus the brief 'flash' to darkness when it starts restoring session information.\n\nok... now click *anywhere* and *drag*... and huge chunks - sort of triangular/geometric patches are blacked out... and do not repaint.\n\nAmazingly... I didn't have this problem with beta 4, but I keep seeing it in the three releases mentioned above.\n\nI see the identical problem with two different distributions so far -\n\nSuSE 6.4 - no previous KDE or QT installed - very bare install on the machine otherwise.\n\nSlackware 7.1 - similar install situation - no previous KDE or QT.\n\nFor the curious... I posted the screenshots at\n\nhttp://www.thegrid.net/jbyrne/screenshots/ if my description leads something to the imagination.\n\n---\n\nJust to be fair - my first experience with KDE was a 1.0 pre-alpha a while back when I was running RedHat 5.1.\n\nI've always greeted each KDE alpha/beta release with enthusiasm... but news of a final release is a bit frightening - considering my recent installs of RC2.\n\nTo pick something else more concrete - in case others have a difficult time reproducing the problem...\n\nThe development of Konqueror is *very* exciting... but it *is* in its current state - unusable for anything significant.  \n\nIt's current treatment of Java/Javascript, SSL, and performance with more 'dynamic' sites is just not usable.  I really *want* it to work... but the reality is - you have to grab Netscape.\n\nI'd be happy to wait until the end of the year... or however long it takes to stabilize KDE 2 - but an immediate release probably won't boost confidence or help what is otherwise a very positive development record.\n\n- Jason"
    author: "Jason Byrne"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-17
    body: "I'm not currently running Linux (upgraded my comp and waiting for SuSE 7.0), and I haven't tested the latest betas of KDE2... But to me it seems that the reason for those bugs in your desktop might be related to your video-card. Have you tried upgrading it's drivers?"
    author: "Janne"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-18
    body: "well... point taken - but KDE2 isn't the *only* desktop I've run on these computers.\n\nfvwm1, fvwm2, windowmaker, enlightenment, kde 1.1.2... all do fine - no bizarre repaint problems.\n\nI was ready to attribute the problem to SuSE until I saw the same thing on Slackware.  I would compile everything on FreeBSD, but I don't have that much free time to burn... considering the running track record (repeat problems in three betas, etc...)\n\nbtw... I have also tried alternate video cards.\n\nI'm not sure either card has a great Linux reputation =) ... but they are either SiS 620 or Voodoo Banshee."
    author: "Jason Byrne"
  - subject: "Re: KDE2 not just yet - KOFFICE months away"
    date: 2000-10-17
    body: "KDE2 RC2 installed quite easilly and is quite stable if I don't use kcontrol (which crashes immediately - not just for me but lots of others according to the bug reports). I even use it as my standard desktop instead of xemacs.\n\nBut KOFFICE is another thing. Almost everything in KOFFICE crashes within a few minutes - kspread even on simply entering an integer value in a field.\n\nSo that KDE2 does not get an unjustified bad name, I suggest removing KOFFICE from the KDE2 distribution.\n\nMalcolm"
    author: "Malcolm Agnew"
  - subject: "Initial experience of SuSE 7.0 installation"
    date: 2000-10-12
    body: "Starting with an up to date SuSE 7.0 running KDE1.94, I installed RC2 using the following steps\n<br>1. Found and read the README for RC2 for SuSE\n<br>2. Downloaded all RPMs from 7.0 and some from localized\n<br>3. Logged in to KDE2 as root and ran \"init 2\" to make sure I was not using KDE at all\n<br>4. Ran \"yast\", went to \"Install Packages\"\n<br>5. Installed all RPMs under 7.0 then all RPMs underlocalized\n<br>6. yast automatically ran SuSEConfig\n<br>7. No problems with yast or SuSEConfig\n<br>8. Ran \"init 3\"\n<br>9. Old kdm came up as expected, allowing me to log in to kde or kde2\n<br>10. Checking with KPackViewer, I see that KDE is still in /opt/kde and KDE 2.0 has installed in to /opt/kde2\n<br>11. Ran kppp, konqueror, old kssh, ksysguard, kcontrol, KMix, etc.\n<br>12. No show stoppers so far.\n<br>Thanks and congratulations. Hope this is the last test version before release."
    author: "Paul Leopardi"
  - subject: "Re: Initial experience of SuSE 7.0 installation"
    date: 2000-10-14
    body: "Sorry but I did exactly the same thing and my KDE2 crashes.\nI installed all the files (including QT) via YAST, ran \"init 3\" and tried to log on to my system.\nBut I just get a grey screen for a short time and then my Logon-Panel comes back. I also tried this as root - the same.\nI think there must be something wrong with the QT because as I upgraded my system YAST2 also crashes. When I downgrade my system to KDE1.94 with the older QT everything works just fine.\nAny hints for me?"
    author: "Carsten"
  - subject: "Re: Initial experience of SuSE 7.0 installation"
    date: 2000-10-14
    body: "Additional notes to my comment above.\nI finally got it working after these steps:\n1.) removed KDE2 totally\n2.) Downloaded QT-2.2.1 and compiled it myself in /usr/lib/qt-2.2.1\n3.) Changed the symlink /usr/lib/qt2 to the new directory\n4.) Installed the KDE2 binaries\n\n=> KDE1, KDE2 and YAST2 are working\n\nYAST2 still finds it\u00b4s \"old\" QT-2.1.1 libraries in /usr/lib/libqt.so.2\nThe QT-package from the release replaces this with the new one, which seems to be bad."
    author: "Carsten"
  - subject: "Re: Initial experience of SuSE 7.0 installation"
    date: 2000-10-15
    body: "Sorry, I don't have much of an idea what is happening. My guess is that the SuSE 7.0 RPMs for RC2 make assumptions about the starting point and that you and I started from a different point.\n<br>My entire upgrade path was:\n<br>1. KDE 1.1.2 on SuSE 6.3 then SuSE 6.4\n<br>2. Some updates to SuSE 6.4\n<br>3. KDE 1.93 on SuSE 6.4 from RPMs using rpm\n<br>4. Updates to SuSE 6.4 including ALSA 0.5.8\n<br>5. KDE 1.94 on SuSE 6.4 from RPMs using rpm\n<br>6. SuSE 7.0\n<br>7. Updates to SuSE 7.0 including ALSA 0.5.9c\n<br>8. KDE 1.94 on SuSE 7.0 from RPMs using yast\n<br>9. RC2 on SuSE 7.0 from RPMs using yast\n<br>At any point along this upgrade path, your configuation could have diverged from mine. I have never compiled QT or KDE myself, but I don't know if rpm or yast did any compilations. I assume they just installed binaries.\n<br>In any case, please put in a KDE bug report and contact the SuSE packager. I think the address is for SuSE is feedback@suse.de\n<br>Current SuSE users who want to use KDE 2.0 should not have to do what you did."
    author: "Paul Leopardi"
  - subject: "Re: Initial experience of SuSE 7.0 installation"
    date: 2000-10-15
    body: "Forgot to add that in my step 3, after I initially installed KDE1.93, I did the following:\n<br>Using the old KDE, logged in as root, I went in to KDM Configuration and added kde2 as a session type for kdm.\n<br>Once I did this, when kdm came up, it allowed me to choose kde2 or kde. This just worked. You will need to ask the SuSE packager how it worked, because I don't understand it. I don't even know if this is what the SuSE packager intended."
    author: "Paul Leopardi"
  - subject: "Re: Initial experience of SuSE 7.0 installation"
    date: 2000-10-18
    body: "I have had the same problems with the qt rpms\nfor Suse 7.0. After recompilaton of qt kde2rc2 (rpms) starts. I have another problem with tif and jpg images: With rc2 it is not possible to display them using the image viewer or with konqueror. Is this a problem of qt?\n\nKai"
    author: "Kai Wiechen"
  - subject: "Re: Initial experience of SuSE 7.0 installation"
    date: 2000-10-18
    body: "If I get you right you recompiled the qt yourself?\nThen you need to activate the gif- and jpeg-support in the configure-script. Just type configure -help.\nYou need to install the libraries for gif and jpeg at first of course but that should not be a problem with SuSE. Just check it to be sure.\nI can display them without problems."
    author: "Carsten"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "I\u00b4m pleased how all things develop from release to release. \nActually JavaScript is getting better and better (even if there\u00b4s a lot left to be done).\n\nIt\u00b4s getting faster!!\nKOffice_parts now start as quickly as an editor\n\nIcons are getting better.\n\nOne `showstopper`: kdm crashes again during openGL-init (and I really want to know, why kdm calls glInit at all!), this did not happen in the finalBeta. But this is maybe due to an installation fault I made (although anything else just runs as smoothly as never before)\n\nkaiman does not crash anymore.\n\nhtml-rendering is faster and much prettyer.\n\nI\u00b4m impressed and I wonder what you guys will have ready on oktober 16th. \n\nTom"
    author: "Thomas Regner"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-16
    body: "Hi\nI know it is a little *too* late to send this post. I downloaded the kde2RC2 sources and compiled them (this is my first experience at compiling KDE, just followed the instructions, spent a lot of time searching for Xfree86-devel package(I lost the cd)). I found the following problems after the compile.\n1) KDM2 has a great interface. the thing I liked is the console option. The problem is that whenever I use the new kdm, the sound driver (emu10k1.o sound blaster live value on RH6.2) does not initialize (error: no mixers found) on Kde or Gnome or any other wm. It works well with kdm1 and gdm.\n2) kaiman works well, though the default skins provided are very unintuitive.\n3) downloading a package from the net, konqueror goes partway and suddenly quits.\nMay be something wrong with compiling. The kdetoys package did not have a configure script."
    author: "rithvik"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-16
    body: "Hi\nI know it is a little *too* late to send this post. I downloaded the kde2RC2 sources and compiled them (this is my first experience at compiling KDE, just followed the instructions, spent a lot of time searching for Xfree86-devel package(I lost the cd)). I found the following problems after the compile.\n1) KDM2 has a great interface. the thing I liked is the console option. The problem is that whenever I use the new kdm, the sound driver (emu10k1.o sound blaster live value on RH6.2) does not initialize (error: no mixers found) on Kde or Gnome or any other wm. It works well with kdm1 and gdm.\n2) kaiman works well, though the default skins provided are very unintuitive.\n3) downloading a package from the net, konqueror goes partway and suddenly quits.\nMay be something wrong with compiling. The kdetoys package did not have a configure script."
    author: "rithvik"
  - subject: "Congrats"
    date: 2000-10-12
    body: "Guys, a couple of comments:\n\n1. KMail ROX.\n2. Konqueror ROX.\n3. KWord ROX.\n\nThanks, and keep up the good work."
    author: "Renai Lemay"
  - subject: "Re: Congrats"
    date: 2000-10-12
    body: "Check out\nknode, kwrite and korganizer as well."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Congrats"
    date: 2000-10-13
    body: "> Kword ROX.\n\nI have to second that. I am a GNOME user, I have been testing the betas of KDE2 but still prefer helix. That being said, KWord is the best word processor to date for linux. I have been fighting with Word Perfect for the last week trying to write a paper for Hons Calc. I was begining to think I needed to learn latex. Kword has been able to handle everything I need it to do. \n\nThanks much.\n\nNate Custer"
    author: "GenCuster"
  - subject: "Re: Try Lyx !"
    date: 2000-10-14
    body: "Hi, \n\nhave you ever tried lyx? It gives you the power of Latex but is as easy to handle as Word. It's very stable and I write all my papers with it.\nThe koffice web page says that lyx will be included in koffice. However, klyx looks currently unmaintained and is far behind lyx. Anybody more information on that?\n\nChris"
    author: "Chris Naeger"
  - subject: "Re: Congrats"
    date: 2000-10-13
    body: "Kword doesn't work at all for me. It sucks.\nI am very dissapointed with Kword. When you\nopen a new document and write the first line of\ntext it doesn't go automatically to the next line.\nSecond, the letters in document dissapers.\nThird, text boxes sucks."
    author: "Zeljko Vukman"
  - subject: "Re: Congrats"
    date: 2000-10-23
    body: "First: I tried this to see if it does the same for me. And the result was, that if I typed something, with no spaces in between, it didn't go to the next line. But if I typed a sentence, that was longer than a line, it did the thing like it was supposed to.\nSecond: Didn't experience anything like it.\nThird: ?\n\nregards,\n\nGasper"
    author: "Gasper Lakota - Jericek"
  - subject: "Cant close koffice workspace without saving"
    date: 2000-10-12
    body: "Just downloaded the packaged version for SuSE-6.4\n\nHad to recompile qt to make it work. qt was complaining about un-resolved symbols.\n\nAny way, I opened koffice Workspace and drew some squares withing killustrator. (I dont know why).\n\nNow I want to close it and the app asks me if I want to save the document. If I say no, it asks me again. The only way to kill it without saving was with xkill.\n\nAnyone else had the same problem?"
    author: "Atif"
  - subject: "Re: Cant close koffice workspace without saving"
    date: 2000-10-12
    body: "See my reply to Tyler C. Hayward below. In brief, if you really need to compile anything, in my opinion there is either a problem with the system you started with or with the RPMs themselves.\nPlease read my checklist below. \n<br>If you are confident the problem is with the SuSE RPMs for 6.4, please contact the SuSE packager via feedback@suse.de or in any other way you know how to."
    author: "Paul C. Leopardi"
  - subject: "Re: Cant close koffice workspace without saving"
    date: 2000-10-14
    body: "Hmmm... I installed the SuSE 6.4 binaries on a SuSE 6.3 system yesterday and even that works great...\n\nI hope you did uninstall older KDE2 beta's, because those normally do cause very strange bugs if you don't remove them first.\n\nCheers, Martijn"
    author: "Martijn Klingens"
  - subject: "Re: Showstoppers"
    date: 2000-10-12
    body: "<em>here is your last chance to find show-stoppers, or you'll have to live with them for a while</em>\n\n<p>Uh, this is a very bad attitude. There should be <b>zero</b> showstoppers in the final release. That's the definition of showstopper - it stops the show.\n\n<p>First impressions of count. If you say it's release quality then it had damn well better be. GNOME had a less than optimal 1.0 release, and they still haven't lived it down. Even yesterday there was an article posted to LinuxToday that essentially said \"don't judge Debian by the version we gave you at LWCE just two months ago.\"\n\n<p>If there's anything that causes a component to crash, halt the release. If you cannot find the cause, then release it with a note. If you can find the cause, then don't release it until it's fixed.\n\n<p>Or call it KDE 1.99 instead."
    author: "David Johnson"
  - subject: "Re: Showstoppers"
    date: 2000-10-12
    body: "<I>\"GNOME had a less than optimal 1.0 release, and they still haven't lived it down.\"</I>\n\nTrue, they made a mistake when releasing 1.0\nBut what proofs they are still making mistakes?\nGnome 1.2 is more stable than rock.\nEven Gnome 1.1.8, a beta release, is already more stable than 1.0 and thus already release quality."
    author: "Anonymous"
  - subject: "Re: Showstoppers"
    date: 2000-10-12
    body: "Of course, showstoppers will stop the show.  :)  That's the whole point of the RC.  But if the RC doesn't get enough testing and a showstopper slips through, then that'll just suck.  An update will probably follow.\n\nCheers,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: Showstoppers"
    date: 2000-10-14
    body: "<p>I've been keeping upto date with the\nlatest kde packages in debian/woody. At\nthe moment kicker panel won't start neither\nwill the kde window manager. Lots of other\nbugs, and I'm not going to submitting them every few minutes when an application krashes.</p>\n<p>There are heaps of other bugs, looking\nat the stack dumps they look like lack of\nexception handling.</p>\n<p>I don't think kde2 is ready, you have\nmade this time scale like a commercial\ndeadline, instead of the the open source\n<i>\"when its ready\"</i> release methodology, which is more real world.</p>\n<p>Buggy code isn't taken\nwell by the linux/*nix users compared to the users of some other operating systems.<br>Quality comes first,\nfeatures come second. Users of the other OS seem to have the opposite expectations.</p>\n<p>A premature release of KDE2 will do more harm than good, and the other *nix desktop can only gather momentum because of this.</p>\n<p>I wish KDE 2 success; users can carry on with KDE 1. Just like people have to use the 2.2 kernel while 2.4 is being made right.</p>\n<p>Keep making RCs till then!</p>"
    author: "Dale"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "Here is a showstopper for you: I recompiled and installed the latest version of qt, installed all the SuSe 6.4 RPMs. So far so good. Went to log in as a regular user, screen goes grey like KDE2 is loading, then dies and takes me back to login screen. Can login as root ok though, just not as regular user."
    author: "Tyler C. Hayward"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "This is about the most stupid bug report I've seen so far. Do you think this information could help anyone to find a bug in KDE?"
    author: "Anonymous Coward"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-12
    body: "Normally, if you use RPMs, you should not compile parts of the application yourself. \nI used the SuSE 7.0 RPMs for KDE and did not need to compile or recompile anything.\n<br>1. Did you start with an up to date 6.4? If not, see the updates page for 6.4 on http://www.suse.com and bring your 6.4 up to date.\nMake sure you have updated ALSA. That \"gotcha\" got me when I tried to install the KDE1.94 RPMs first time.\n<br>2. Did you start with an earlier KDE1.9X beta installed or is this your first KDE2 installation?\n<br>3. Did you use yast to install all the RPMs, as in my message above, or have you written a script to run \"rpm -Uvh\" on each file in the right order? Previous SuSE RPMs for KDE1.9X for 7.0 had the QT stuff in a different RPM file. This is also likely for 6.4. So order is probably important. But my upgrade of 1.94 on 7.0 to RC2 using yast just worked - see my note above.\n<br>4. Finally, and most importantly, have you contacted the SuSE packager, via feedback@suse.de or any other way? If there is a problem with the RPMs it needs to be fixed fast."
    author: "Paul C. Leopardi"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-13
    body: "I don't know if it applies here, but I've had the same problem with earlier betas on SuSE 6.4 here. The problem lies in the access permissions of your home directory. For me, it had to be GROUP readable, as far as I can remember.\n\nI don't know if this is still the same, but maybe it will help you.\n\nBy the way: if this __IS__ still the case, I'd treat it as showstopper, because making directories group or even world readable should not be obligatory for just running a desktop environment.\n\nSucces, Martijn"
    author: "Martijn Klingens"
  - subject: "cvsup"
    date: 2000-10-12
    body: "What's up with cvsup? (No pun intended). I have  a sourcetree from sometime around RC1 and I usually update it every day from ferret.lmh.ox.ac.uk. But the last couple of days it hasn't updated any files for me. Any ideas?"
    author: "tritone"
  - subject: "Re: cvsup"
    date: 2000-10-13
    body: "There is a known problem with the server where most cvsup servers get their sources from.\nA new server is required, but currently no one does provide one.\nFor now only max.tat.physik.uni-tuebingen.de has up to date sorces and the anoncvs mirrors."
    author: "Michael H\u00e4ckel"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-13
    body: "I don't know if this is a bug with KDE2 or Linux mandrake 7.2beta\n<p>\nbut, when I installed Mandrake 7.2beta it had kde 1.93 I updated to 1.99, I'm currently downloading 1.99-17mdk I have a problem where if I start in runlevel 5 it comes up with the gui to enter username and password, I can't log in as root or any other user it says login failed strait away, the same happens if I lock the screen, the only way I can log in is to start in runlevel 3 and then type startx, this doesn't really matter but it's a bit of a pain, is anyone else having this problem or know how to fix it I'd be very appreciative, if I use xdm instead of kdm that works but it looks crapy..\n<p>\nI've updated cracklib to the latest version from mandrake-devel because I thought that might fix it, I'm new to Linux so I don't know of anyway to fix it other than upgrading stuff *L*\n<p>\nalso Kpackage sometimes, well most of the time gives me a signal 11 and crash's when I press install, the same thing happens if I right click on an rpm and open it in \"konsole\" with rpm -ivh, it crash's right after the RPM is installed..\n<p>\ngo to http://www.globalfreeway.com.au with konqueror there's an extra frame which shouldn't be there\n<p>\nalso sometimes when I copy files and then change to another directory and press paste it says my clipboard is empty, I usually have to drag and drop...\n<p>\nthat's about all I can think of for now...\n<p>\nthanx in advance"
    author: "James Jose"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-13
    body: "All this problems are solved and fixed.\nYou have to download an d install the latest\npam*rpm package from Mandrake-cooker to solve\nyour login-problem.\nKpackage works perfectly now, but you have to download the latest kdeadmin*rpm from Mandrake cooker (I would also suggest kdebase, kdelibs,\nkdesupport & qt2-1.1.).\nkdesupport-1.99-1mdk\nkdelibs-1.99-5mdk\nkdelibssound-1.99-5mdk\nkdebase-1.99-17mdk\nkdesupport-1.99-1mdk\nqt2-1.1-1mdk"
    author: "Zeljko Vukman"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-13
    body: "Please don't flame me for using Hotmail, but...\n\nI'm impressed! Konqueror can use Hotmail without crashing (after enabling JavaScript)! Netscape6 PR3 can't do that...\n\nGood job, KDE developers. I'm sure anticipating the final release, but don't mind the slight delay. The fewer bugs, the better."
    author: "Anonymous105"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-13
    body: "it would really be nice to see a slackware package\nposted as fast as redhat, mandrake, and suse packages are."
    author: "kristoff d. mehta"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-17
    body: "So ask patrick (head of slackware) how to build them. I am sure it would help alot of people.\n\nThanks Ben"
    author: "Ben Woodhead"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-23
    body: "Go to www.linuxmafia.org and take a look there.\n\nThis site is dedicated to Slackware and packages for this distribution. I personally use this site alot when I wanna try something before I accually do a source build of it.\n\nEnjoy...\n\nFreeBassX\nSlacked since 1995"
    author: "FreeBassX"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-13
    body: "Does anyone know how to make Klock work? Whenever one of our users locks thier screen they have to restart X to get back in. Already changed kcheckpass to suid root and no luck. Is this just broken hard or is there a fix? I am tired of people knocking at the NOC. Otherwise KDE is getting to be solid. Rsynced out to 51 workstations accross NFS."
    author: "cockney"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-13
    body: "What happens when you run kcheckpass from the command line?  Check the exitcode especially!"
    author: "ac"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-13
    body: "Try recompiling kdebase after running configure --with-shadow\n\nThat is of course if you are using shadow passwords. This worked for me....\ntom."
    author: "Thomas B. Quillinan"
  - subject: "Did you try pressing the Enter key?"
    date: 2000-10-13
    body: "I know it's a dumb question, but I can imagine users used to the behavior of xscreensaver or the old klock, which would show the prompt for typing in the password if someone just moved the mouse, and not even thinking about hitting Enter.\n\nWorth a shot, I suppose."
    author: "J. J. Ramsey"
  - subject: "KCMInit crashes during startup"
    date: 2000-10-13
    body: "Ok, I know this aint no support-service for KDE or anything else, but I've tried on both #Linux and #KDE with no success, so please forgive me:\n\nI formatted and reinstalled KDE2_rc2 on a clean RH70 system (no gnome or KDE), and things are mostly fine now, apart from two things:\n1) During startup I get that cute little dragon telling me that KCMInit crashed and caused the signal 11 (SIGSEGV). It happends every time, on every users. Whats kcminit?\n\n2) Koffice: Seems to work nice with their own, native files, but I've tried to open a MS-Excel document (a task not too uncommon for the Koffice-suite, I suspect), and that makes not only KSpread crash bigtime, but it takes the whole X with it, and I have to do a ctrl-alt-backspace.\n(Someone interested in that Excel-file mail me and I'll send it to ya (Sorry - no business secrets in it :-D). Remove \"spam\" to mail me.).\n\n\nI guess that its no quick answer to #2, but that KCMInit - whats that?"
    author: "King Egypt"
  - subject: "Re: KCMInit crashes during startup"
    date: 2000-10-13
    body: "(1) has been solved but didn't make it into the RC unfortunately.  Something about the energy module segfaulting on some systems."
    author: "ac"
  - subject: "Re: KCMInit crashes during startup"
    date: 2000-10-13
    body: "It appears that this ONLY happends when you create a user using kuser! I added a user the good old way with adduser, and that account is working like a dream in KDE2! I even lost a couple of other annoying \"features\" that were bugging me when I use this account.\n\nConclution: Dont use kuser. Use adduser."
    author: "King Egypt"
  - subject: "Re: KCMInit crashes during startup"
    date: 2002-03-29
    body: "I also am facing problems due to SIGSEGV faults. When I try to start KPackage it causes SIGSEGV and crashes out. \nI am using Red Hat Linux 7.1. Is there any fix for this or a workaround.\n"
    author: "Madhusudhan"
  - subject: "KDM bug"
    date: 2000-10-13
    body: "Here's an old bad bug:\n\nFor some oddball reason, KDM doesn't load the background or display the icon correctly (unless I select clock).  If I go to the login manager, it shows a question mark as the default icon.  This has been a problem in every single beta I've tried (back to Beta 1), and on every system I've installed any of them on.  Has anyone else suffered from this problem?  I thought for sure it would be fixed by now.  All the systems were either Slackware 7.0 or 7.1.\n\nMaybe it's an X thing?  Weird!\n\n-Justin"
    author: "Justin"
  - subject: "Re: KDM bug"
    date: 2000-10-13
    body: "I got the same thing! \nI have KDE final beta and RH 6.2. I played alot with the X, I am almost sure it is not coming from there. I hope that will be fixed soon.\n-Kefah"
    author: "Kefah"
  - subject: "Re: KDM bug"
    date: 2000-10-13
    body: "I was having a similar problem.  I don't think I saw any mention of it in install HOWTO/FAQs, but apparantly KDM, and KWM (and probably other KDE programs) rely on QT's image-rendering capabilities, which are not compiled in by default.  make sure you configure QT2.2.1 with -gif and -system-jpeg.  \n\nHope this helps!"
    author: "Cody Casterline"
  - subject: "Re: KDM bug"
    date: 2000-10-14
    body: "According to KDM's HTML help, the background image isn't supported anymore in KDM2, instead KDM relies on other programs to set the background.\n\nI don't know why and I can't find the script SuSE uses to start kdm, because that script DOES set the background for me. Maybe someone else knows this?\n\nThis is a quote from the manual:\n---------------------------------------------\nKDMDESKTOP\n\nThis section is obsolete. In previous versions of KDM it could be used to control a background screen prior to login. The graphical configurator for KDM may still generate this section, but it is ignored by KDM\n---------------------------------------------"
    author: "Martijn Klingens"
  - subject: "Problem with kbase..."
    date: 2000-10-13
    body: "Hi\nI've been trying to install the kbase sources but then I got an error. I hadn't got any problems installing ksupport and klibs.\nThe error occured in the part kdm. Afterwards I compiled every part of kbase for itself, but now I don't have the startkde script (Can anyone send his to me?).\nHas anyone also got these problems? \nBy the way. The first answer on this message contains the full output of my error.\nThanks,  Grovel"
    author: "Grovel"
  - subject: "The make-output"
    date: 2000-10-13
    body: "Have a look at the attachement"
    author: "Grovel"
  - subject: "Compiling kdegraphics and kdetoys..."
    date: 2000-10-13
    body: "<P>Compiling RC2 from source, I ran across the following:\n\n<P><UL>\n<LI>kdegraphics suddenly seems to require TeX at ./configure time... prior beta releases did not. what's with that?</LI>\n<LI>kdetoys can't be built because there's no configure script.  \"make Makefile.cvs\" doesn't work either.  Am I missing something?</LI>\n</UL>\n<P>Any enlightenment appreciated...."
    author: "Rod Roark"
  - subject: "Re: Compiling kdegraphics and kdetoys..."
    date: 2000-10-18
    body: "kdegraphics: I just installed the appropriate TeX rpm and it worked.  Didn't look at why it's needed now.\n\nkdetoys: there's a configure.in.in (which helps none), but there's no configure.in, so using autoconf won't help.  I copied configure and some other missing files (including all the Makefile.in files and the admin directory) from a previous release, configured and compiled.  That seems to have worked fine.\n\n-- Steve"
    author: "Steve Bryant"
  - subject: "About animation"
    date: 2000-10-13
    body: "Well, KDE 2 rocks for the most part. However, I don't like the animation when minimizing/resizing windows and I can't find out how to turn it off. I really think it should be optional. Does anyone have any info on this?\nThanks"
    author: "tritone"
  - subject: "Re: About animation"
    date: 2000-10-14
    body: "Preferences->Look & Feel->Window Behavior->Actions<BR><BR>\n\nUncheck Display content in moving windows and display content in resizing windows"
    author: "Matt Newell"
  - subject: "Not the same thing"
    date: 2000-10-14
    body: "I'm tallking about when you click on an iconified window in the taskbar. There is an animation before it pop ups on screen."
    author: "tritone"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-13
    body: "Hrmf , there are some many possibilities to install KDE the WRONG WAY!! can anyone do something like HELIXGNOME ????? That would make thing much easier and there will me FAR LESS complaints about bugs that are caused by a false Installation...\n\n\ncu/"
    author: "chris"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-14
    body: "I agree that installation should be easier. The existence of RPMs and other binaries help, but:\n<br>1. The RPMs assume a certain starting point. The starting point the includes distribution, version, updates of other packages and dependencies (eg. ALSA updates), previous versions of KDE installed or not installed (eg 1.1.2, 1.94)\n<br>2. There is very little correct and complete documentation on what the assumed starting point is and what the installation steps are for each set of RPMs.\n<br>I have notified both the KDE webmaster and SuSE. See bug 10102 at http://bugs.kde.org//db/10/10102.html"
    author: "Paul Leopardi"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-13
    body: "Konqueror is giving me trouble with fonts. \n<p>\nAll well-designed pages are rendered correctly. However, whenever I go to a page that has no FONT tags or default font specified within the HTML (for instance, a 404 Error page), the very ugly \"console\" font is used instead. From that point on, ALL pages, no matter what FONT tags are specified, are rendered with the console font.\n<p>\nI've checked all the settings for Fonts in Control Center and in konqueror, and nowhere is it set to use the console font."
    author: "John Athos"
  - subject: "Source for KDE Toys."
    date: 2000-10-14
    body: "Okay, I'm trying to install the latest RC.  I get to compiling KDEToys, and find that it doesn't come with a configure file.  Where can I find one, or how can I compile it the way it is?\n\nTim"
    author: "Tim_F"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-14
    body: "I'm attempting to compile KDE2 from source, and kOffice does not want to compile.  My system is a standard red hat 7.0.  Is anyone else having this problem?  I attached the interesting part of the compile output."
    author: "Chad Frick"
  - subject: "Some problems with Konqueror"
    date: 2000-10-14
    body: "Thank for this amazing work.\nKDE 2 is very good.\n\nThe only problem I have is when I try to access\na Samba directory with Konqueror.\n\ntrying smb:/, and smb:/192.168.0.1\n\nand nothing appear, no message, \n\nAnybody knows ?"
    author: "thil"
  - subject: "Does HTTP auth work for anyone??"
    date: 2000-10-15
    body: "Hi,\n\nI've used MDK RPM's and compiled the sources for myself under FreeBSD, but whenever I go to a web page that needs http auth (like www.moses.cx/school/304 for example) Konq no longer asks me for my username and password.\n\nIt does work, however, if I manually type in:\n\nhttp://user@www.site.with.auth.org\n\nThis is new since beta 5, is it just me?\n\nBen"
    author: "bhall"
  - subject: "Re: Does HTTP auth work for anyone??"
    date: 2000-10-15
    body: "Dawit is fixing this."
    author: "ac"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-15
    body: "Can anyone tell me how much extra disk space KDE2 needs compared to KDE1?  Thanks..."
    author: "20goto10"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-15
    body: "Hi,\non my disk, the kde2 directory use 102MO.\nJust with Kdesupport, kdelibs, kdebase, kdeadmin, kdeutil, kdegraphics, and kdenetwork.\n\nThis is whitout the qt2 directory."
    author: "thil"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-15
    body: "Hi,\non my disk, the kde2 directory use 102MO.\nJust with Kdesupport, kdelibs, kdebase, kdeadmin, kdeutil, kdegraphics, and kdenetwork.\n\nThis is whitout the qt2 directory."
    author: "thil"
  - subject: "Re: KDE Desktop 2.0 Final Release Candidate Available"
    date: 2000-10-16
    body: "OK.  Although I have Red Hat 6.0, I always prefer to compile KDE by myself (I'm doing this since KDE 1.1).  I compiled all KDE2 and Qt using GCC-2.95.2 optimized for PII+.  I must say KDE2 is impressive.  Konqueror works excellent (except HTTP-Auth, making Konq fall in a infinite loop trying to get pages :-\\ ).  I compiled with Netscape Plugins support (I used an old lesstiff-0.89 I had), and Shockwave works perfect.<br>\nOK, now the bugs:<br><br>\n1.-Keystone dies horribly when tries to open a VNC connection with Win32 machines.  After this, dies with sig11.<br>\n2.-KMail personalities doesn't work very well.  Although I chose another personality, KMail sends mail with former e-mail address.  :-(<br>\n3.-kmidi and kpackage didn't compile.  I must disable in Makefile.  Perhaps this could be my old RH version.  I update many packages since installation some years ago (including glibc, kernel, PAM, etc), but I still have packages from RH6 out-of-the-box.<br>\n4.-ktoys doesn't come with a Makefile.<br>\n5.-In ksysguard if you choose CPU0 load and CPU1 load in graphs, (seems to be) the same processor... :-(<br>\nOK, now the good things:<br><br>\nI compiled two versions: for 586 and for 686.  The former is in /opt/kde2, and shared it in network, via NFS.  Qt2 is in /opt/qt2, shared too in NFS.  Voil\u00e0, the rest of the machines (mixed Mandrake 7.1/Slackware 7.0 without patches, out-of-the-box) now has KDE2, without problems.  The slower machine, a Pentium 120 w/64MB, loads in 15 seconds.  Incredible!  This machine can run simultaneously StarOffice w/KDE2 \n(StarOffice launchs in 52 seconds).<br>\nAh, i18n and l10n works excellent.  I chose Spanish/Venezuela, and now I have a Spanish KDE...\nNestor Pe\u00f1a<br>\n&lt;nestor at linux dot org dot ve&gt;"
    author: "Nestor Pe\u00f1a"
  - subject: "Disable capslock?"
    date: 2000-10-18
    body: "You can configure a helluvalot of more or less useful settings in KDE2, but heres one I've never seen before: A possibility to disable capslock! Never used it (on purpose, that is), never needed it, never will. And it's so easy to strike by acciDENT ON THIS LAPTOP. :-)\n\nSo - there ya go. I know I'm not alone on this one."
    author: "King Egypt"
  - subject: "Re: Disable capslock?"
    date: 2002-09-25
    body: "Hi,\n\ngood to read - so how does it work?\n\nCouldnt find it in the control center ....\n\nRegards,\n\nStefan"
    author: "stefan gehrig"
---
RC2, the (hopefully) final release candidate before the scheduled release of KDE 2.0 on October 23, is out, with binary packages available for Mandrake, RedHat (6.2 and 7.0) and SuSE (6.4 and 7.0).  The full announcement is below.  As the code will be frozen on October 16 for the KDE 2.0 release, here is your last chance to find show-stoppers, or you'll have to live with them for a while &lt;grin&gt;.



<!--break-->
<H3 ALIGN="center">KDE Desktop 2.0 Final Release Candidate Available for Linux<SUP>&reg;</SUP></H3>
<P><STRONG>Final Release Candidate of Leading Desktop for Linux<SUP>&reg;</SUP>
and Other UNIXes<SUP>&reg;</SUP></STRONG></P>
<P>October 10, 2000 (The INTERNET).  The <A HREF="http://www.kde.org/">KDE
Team</A> today announced the release of KDE 2.0 RC2, the second and
(barring any unforeseen problems) final
release candidate for Kopernicus (KDE 2.0), KDE's next-generation, powerful,
modular desktop.  The KDE team has previously released five Beta versions --
the first on May 10 of this year -- publicly; the sole prior release
candidate was released internally only.
RC2 is based on <A HREF="http://www.trolltech.com/">Trolltech's</A><SUP>tm</SUP>Qt<SUP>&reg;</SUP> 2.2.1 and includes the core libraries,
the core desktop environment, the KOffice suite, as well
as the over 100 applications from the other standard base KDE packages:
Administration, Games, Graphics, Multimedia, Network, Personal
Information Management (PIM), Toys and Utilities.
This release marks the last opportunity for developers and users to
report problems prior to the official release of Kopernicus (KDE 2.0)
slated for this October 23.
</P>
<H4>Downloading and Compiling KDE 2.0 RC2</H4>
<P>
The source packages for RC2 are available for free download at
<A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/tar/src/">ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/tar/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>.  RC2 requires
qt-2.2.1, which is available from the above locations under the name
<A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/tar/src/qt-x11-2.2.1.tar.gz">qt-x11-2.2.1.tar.gz</A>.
Please be advised that RC2 will <STRONG>not</STRONG> work with any
older versions of Qt.  Qt is not part of KDE's release testing.
</P>
<P>
For further instructions on compiling and installing RC2, please consult
the <A HREF="http://www.kde.org/install-source.html">installation
instructions</A> and, should you encounter problems, the
<A HREF="http://www.kde.org/compilationfaq.html">compilation FAQ</A>.
</P>
<H4>Installing Binary Packages of KDE 2.0 RC2</H4>
<P>
Some distributors choose to provide binaries of KDE for certain versions
of their distributions.  Some of these binary packages for RC2 will
be available for free download under
<A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/">ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/</A> 
or under the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>. Please note that
the KDE team is <EM>not</EM> responsible for these packages as they
are packaged by third parties, typically, but not always, the distributor
of the relevant distribution.
</P>
<P>
RC2 requires
qt2.2.1, which is available from the above locations under the name
qt-x11-2.2.1 or some variation thereof adopted by the responsible packager.
Please be advised that RC2 will <STRONG>not</STRONG> work with any
older versions of Qt.  Qt is not part of KDE's release testing.
</P>
<P>
At the time of this release, pre-compiled packages are available for:
</P>
<UL>
<LI><A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/rpm/Mandrake/RPMS/">Linux Mandrake</A></LI>
<LI><A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/rpm/RH/">Redhat 7.0</A> and <A HREF="http://master.kde.org/~bero/rc2/rh6.2/">RedHat 6.2</A></LI>
<LI><A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/rpm/SuSE/6.4/">SuSE 6.4</A> and <A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/rpm/SuSE/7.0/">SuSE 7.0</A></LI>
</UL>
<P>
Check the ftp servers periodically for pre-compiled packages for other
distributions.  More binary packages will become available over the
coming days.
</P>
 
<H4>About KDE</H4>
<P>KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environmentemploying a component-based, network-transparent architecture.
Currently development is focused on KDE 2, which will for the first time
offer a free, Open Source, fully-featured office suite and which promises to
make the Linux desktop as easy to use as Windows<SUP>&reg;</SUP> and
the Macintosh<SUP>&reg;</SUP>
while remaining loyal to open standards and empowering developers and users
with Open Source software.  KDE is working proof of how the Open Source
software development model can create first-rate technologies on par with
and superior to even the most complex commercial software.</P>
 
<P>For more information about KDE, please visit KDE's <A HREF="http://www.kde.org/whatiskde/">web site</A>.</P>


