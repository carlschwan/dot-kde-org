---
title: "KBear 1.0 released"
date:    2000-10-02
authors:
  - "numanee"
slug:    kbear-10-released
comments:
  - subject: "Compiling KBear 1.0..."
    date: 2000-10-01
    body: "When I try to compile KBear 1.0, I get the following error:\n\n\ng++ -DHAVE_CONFIG_H -I. -I. -I.. -I/opt/kde2/include -I/usr/lib/qt-2.2.0/include -I/usr/X1\n1R6/include     -O2 -fno-exceptions -fno-rtti -fno-check-new  -Wall -Wtraditional -Wpointe\nr-arith -Wwrite-strings -Wmissing-prototypes -Wmissing-declarations   -pedantic -c kbearsi\ntemanagermap.cpp\nkbearsitemanagermap.cpp: In method `class QString KBearSiteManagerMap::getParent(const QSt\nring &amp;)':\nkbearsitemanagermap.cpp:31: use of class template `template &lt;class Key, class T&gt; QMap&lt;Key,\nT&gt;' as expression\nkbearsitemanagermap.cpp:31: parse error before `::'\nkbearsitemanagermap.cpp: In method `struct SiteInfo * KBearSiteManagerMap::find(const QStr\ning &amp;)':\nkbearsitemanagermap.cpp:38: use of class template `template &lt;class Key, class T&gt; QMap&lt;Key,\nT&gt;' as expression\nkbearsitemanagermap.cpp:38: parse error before `::'\nmake[2]: *** [kbearsitemanagermap.o] Error 1\nmake[2]: Leaving directory `/home/achim/tmp/kbear-1.0/kbear'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/home/achim/tmp/kbear-1.0'\nmake: *** [all-recursive-am] Error 2 \n\n\n\nAny suggestions?\n\nAchim"
    author: "AH"
  - subject: "Re: Compiling KBear 1.0..."
    date: 2000-10-01
    body: "All this &gt; stuff  should be > of course (it looked alright in the preview...?!?)"
    author: "AH"
  - subject: "Re: Compiling KBear 1.0..."
    date: 2000-10-01
    body: "Hmmm.  Must be a bug in squish I guess.  \n\nI'll look into it.  Personally, I didn't have a problem compiling KBear on Debian Slink w/ egcs-2.91.60, btw.  I used \"./configure  --disable-debug --disable-rpath --prefix=/usr/local/kde --with-qt-dir=/usr/local/qt\"\n\nCheers,\nNavin.\n\n<> <> &gt; &lt; & &amp;"
    author: "Navindra Umanee"
  - subject: "Re: Compiling KBear 1.0..."
    date: 2000-10-02
    body: "The less/greater-than signs are a real pain in khtml. They are always replaced by html entities on each page reload! Must be a bug??\n\nAlso, the layout of the buttons etc. on this reply page surely isn't right.. it's the same on slashdot etc."
    author: "Haakon Nilsen"
  - subject: "Re: Compiling KBear 1.0..."
    date: 2000-10-02
    body: "Just for the record, I get the exact same error -- even when using Navindras ./configure line..."
    author: "Haakon Nilsen"
  - subject: "Re: Compiling KBear 1.0..."
    date: 2000-10-02
    body: "System details, please?"
    author: "ac"
  - subject: "Re: Compiling KBear 1.0..."
    date: 2000-10-02
    body: "Try changing the line which says\nSiteManagerMapIterator it = QMap::find( key );\nto\nSiteManagerMapIterator it = SiteManagerMap::find( key );\nit should compile then.\nWhat compiler version was this program developed on? (You should consider upgrading to avoid this happening again)"
    author: "Jan Borsodi"
  - subject: "Re: Compiling KBear 1.0..."
    date: 2000-10-02
    body: "Sorry, how silly of me :) Mandrake 7.1 with the KDE2 RPMs from coooker. Of course, I also have the relevant devel-packages installed."
    author: "Haakon Nilsen"
  - subject: "Re: Compiling KBear 1.0..."
    date: 2000-10-02
    body: "<P>\nSorry for the inconvienience with the compile error.\n</P>\n<P>You can find a patch at <A HREF=\"http://www.kbear.org/Download.html\">http://www.kbear.org/Download.html</A> that shoult fix it, and also some other futures, actually a upgrade to 1.0.1\n</P>"
    author: "Bj\u00f6rn Sahlstr\u00f6m"
  - subject: "Konqueror integration?"
    date: 2000-10-01
    body: "Wouldn't it be a good idea to integrate this into Konqueror as a KPart? Or is this already possible? (I haven't tried it out yet).\nIt would push Konqueror one step further as the ultimate system manager (file manager is not a good word for this application anymore, nor is browser :-) ).\nLikewise, why isn't the KDE Control Center integrated into Konqueror? But this last question is very off-topic here..."
    author: "Martijn Klingens"
  - subject: "Re: Konqueror integration?"
    date: 2000-10-02
    body: "What you wrote, was my first thought after I read the announcement. I hope it will be possible to integrate it in Konqueror (at least in future)."
    author: "Michael"
  - subject: "Re: Konqueror integration?"
    date: 2000-10-02
    body: "<i>Wouldn't it be a good idea to integrate this into Konqueror as a KPart?</i>\n<p>\nIt is.  Type ftp://mysite.com/home/myname/ into the Location, hit \"Window|Split Left/Right\", and then hit the Home button on the toolbar.\n<p>\nOr you can even get fancier by having a Directory Tree of the ftp site on the left, the contents on the right, and your local drive on the bottom.<p>\nTwo big wishes for Konqueror that would help make it better would be the ability to save views (once you get the window panes setup, save it to a profile that you can reload later).<p>\nAnother nifty thing would be to link two panes so one is a directory listing, and the other displays the content of files.  For instance, you could have a list of images on the left, and link the right pane to display the images you click on.<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: It is?"
    date: 2000-10-02
    body: "I thought that was Konqueror's own FTP implementation and not KBear, so it wouldn't have all of KBear's features. But if it already is, I haven't asked my question here :-)"
    author: "Martijn Klingens"
  - subject: "Re: KBear 1.0 released"
    date: 2000-10-05
    body: "It looks really awesome, I'm going to have to check it out. The name's kinda... um weird tho. Can't say I like it."
    author: "Chris Aakre"
---
<a href="mailto:bjorn@kbear.org">Björn Sahlström</a> wrote in to inform us of the first stable release of <a href="http://www.kbear.org/">KBear</a>, an easy to use and quite powerful FTP client for KDE2.  Amongst other features, it supports extended MDI, includes a flexible site database, and can connect to multiple hosts simultanously. Several screenshots (<a href="http://home.bip.net/bjorn.sahlstrom/kbear/images/snapshots/snapshot01.jpg">1</a>, <a href="http://home.bip.net/bjorn.sahlstrom/kbear/images/snapshots/snapshot02.jpg">2</a>, <a href="http://home.bip.net/bjorn.sahlstrom/kbear/images/snapshots/snapshot03.jpg">3</a>, <a href="http://home.bip.net/bjorn.sahlstrom/kbear/images/snapshots/snapshot04.jpg">4</a>, <a href="http://home.bip.net/bjorn.sahlstrom/kbear/images/snapshots/snapshot05.jpg">5</a>, <a href="http://home.bip.net/bjorn.sahlstrom/kbear/images/snapshots/snapshot06.jpg">6</a>, <a href="http://home.bip.net/bjorn.sahlstrom/kbear/images/snapshots/snapshot07.jpg">7</a>, <a href="http://home.bip.net/bjorn.sahlstrom/kbear/images/snapshots/snapshot08.jpg">8</a>) are <a href="http://home.bip.net/bjorn.sahlstrom/kbear/index.html#Screenshots">available</a> on the project web page.




<!--break-->
