---
title: "LinuxToday: KDE and the ashes of the QPL"
date:    2000-09-16
authors:
  - "numanee"
slug:    linuxtoday-kde-and-ashes-qpl
---
LinuxToday is running an article on recent KDE/Qt events.  The author rants a little about licensing, marvels about what KDE has accomplished so far but then worries that the Linux desktop is becoming too much like Windows.  Yup.  It's all <a href="http://www.linuxtoday.com.au/r/article/jsp/sid/342968">here</a>.


<!--break-->
