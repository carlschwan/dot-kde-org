---
title: "Database access library"
date:    2000-09-14
authors:
  - "numanee"
slug:    database-access-library
---
theKompany.com have <a href="http://linuxpr.com/releases/2512.html">announced</a> KDB, an API for browsing and manipulating databases. <a href="http://www.thekompany.com/projects/kdb/">KDB</a> is LGPL, modular and even includes an I/O slave that enables SQL capabilities in Konqueror.  KDB will be used as the backend for Rekall, a light weight personal DBMS system ala MS Access.
More details <a href="http://lists.kde.org/?l=kde-koffice&m=96850978705019&w=2">here</a>.


<!--break-->
