---
title: "Donation to KDE"
date:    2000-11-08
authors:
  - "mkonold"
slug:    donation-kde
comments:
  - subject: "Re: Donation to KDE"
    date: 2000-11-10
    body: "Hehe, imagine from now in 50 years. An utterly obsolete computer, but an awfully expensive \"oeuvre d'art\". And will still run KDE, just as any other computer of that time :-))"
    author: "Inorog"
---
The German Linux solution provider 
ID-PRO <a href="http://www.it-nachrichten.de/en/show/80219.html">is auctioning</a> a prototype computer for the benefit of a couple of free software projects, including KDE. On the occasion of <A HREF="http://paula.id-pro.org/">PAUL's</A> launching during the <A HREF="http://www.systems.de/">Systems</A> in Munich, Germany, the prototype of ID-PRO's communication server, shaped like a prism and based solely on Open-Source software, will come under the hammer - signed by several well-known KDE developers, Linus Torvalds and a few others. The <a href="http://www.id-pro.com/auction/">bidding</a> started Thursday, 19 October 2000 and ends quite soon!


<!--break-->
