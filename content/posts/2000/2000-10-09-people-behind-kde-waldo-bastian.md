---
title: "People Behind KDE: Waldo Bastian"
date:    2000-10-09
authors:
  - "wes"
slug:    people-behind-kde-waldo-bastian
comments:
  - subject: "Re: People Behind KDE: Waldo Bastian"
    date: 2000-10-14
    body: "Maybe offtopic, but what does Kalle Dalheimer does nowadays? Is he still affiliated with the KDE core team?"
    author: "Phator Que"
---
In this latest installment of <a href='http://www.kde.org/people/people.html'>The People Behind KDE</a>, <a href="mailto:tink@kde.org">Tink</a> brings us an interview with her very own <a href='http://www.kde.org/people/waldo.html'>Waldo Bastian</a>, core developer extraordinaire.  <i>"It must have been around the last beta-release before KDE 1.0 that I upgraded my libc only to discover that that broke netscape. In search for a working browser I came across KFM, the file manager of KDE, and was surprised to see that it could show quite some web-pages and was faster than netscape. Unfortunately it crashed a lot as well."</i>


<!--break-->
