---
title: "People of KDE: Stefan Taferner"
date:    2000-12-25
authors:
  - "Inorog"
slug:    people-kde-stefan-taferner
---
In the last edition for the Year 2000 of the <a href="http://www.kde.org/people/people.html">People Behind KDE</a> series, <a href="mailto:tink@kde.org">Tink</a> introduces us to <a href="http://www.kde.org/people/stefan.html">Stefan Taferner</a>, co-author of <A HREF="http://devel-home.kde.org/~kmail/index.html">KMail</A> and a main contributor to central technologies in the KDE project. The new, festive appearance of Tink's site greets us with the photo of a happy Konqi.



<!--break-->
