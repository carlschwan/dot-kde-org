---
title: "Hispanic Slides, Kandidat Review, KDE2 On Schedule"
date:    2000-10-18
authors:
  - "numanee"
slug:    hispanic-slides-kandidat-review-kde2-schedule
comments:
  - subject: "Is Konquerer good enough for online banking?"
    date: 2000-10-18
    body: "I've now got 4 different financial institutes that I need to be able to access with my web browser.\n\nNo matter how good it is, if I can't use it for my banking/401k/etc, it won't be my default.\n\nJC"
    author: "JC"
  - subject: "Re: Is Konquerer good enough for online banking?"
    date: 2000-10-18
    body: "Usually it's the server end that causes problems. Banks are sometimes more careful to be platform neutral than other sites (they *want* your money!) so they actually try to support Netscape and IE ... heh heh.\n\nMy bank was completely accessible using Lynx-SSL ... until they started using javascript generated menus. I asked them to use a non-javascript navigation menu at the bottom of the page as an option and got no response. Finally I said \"I develop software for blind people and they need this (which is not true but hey)\". I got a reply and they made the changes. \n\nAnother lie that helps is to get your browser to lie about itself (you can configure this in konqueror) since many sites just block browsers based on the  HTTP_USER_AGENT without even checking for SSL capabilities."
    author: "Some guy"
  - subject: "Re: Is Konquerer good enough for online banking?"
    date: 2000-10-18
    body: "I don't know whether Konqueror is good enough for online banking, but I have been testing its SSL implementation (via OpenSSL), and I think it's one of the best. For starters, it actually works (something that Mozilla is sorely missing), and from the sites I tested it with, I stated it used 168bit encryption.\nNot bad."
    author: "MichaelM"
  - subject: "Re: Hispanic Slides, Kandidat Review, KDE2 On Schedule"
    date: 2000-10-18
    body: "hola Luis Digital, \u00bfc\u00f3mo te va? hace tiempo que no te \"veo\" por www.linuxpreview.org.\n\nSuerte y viva KDE."
    author: "Manuel Rom\u00e1n"
  - subject: "Re: Hispanic Slides, Kandidat Review, KDE2 On Sche"
    date: 2000-10-19
    body: "Bueno me parece que ya te lo expliqu\u00e9 por correo ;-)<br><br>\nAh, y recuerda que en <a href=http://luisdigital.2y.net>Luis Digital Online</a> me puedes encontrar siempre. Bye."
    author: "Luis Digital"
  - subject: "Re: Hispanic Slides, Kandidat Review, KDE2 On Sche"
    date: 2000-10-19
    body: "no, no me hallegado nada por correo, \u00bfte has enfadado con ellos? ya s\u00e9 que muchos son unos capullos e hipocritas, pero lo que importa son las noticias.\n\nyo, desde que dije que es mucho m\u00e1s funcional KDE que GNOME me critican, as\u00ed que nunca hago login para estar como an\u00f3nimo, pero bueno...\n\nUn saludo"
    author: "Manuel Roman"
  - subject: "Re: Hispanic Slides, Kandidat Review, KDE2 On Sche"
    date: 2000-10-19
    body: "S\u00ed debes de tener un correo y ahora dos. No recordaba bien, fue a GNU_Leo que le habl\u00e9 sobre el asunto, mira: <a href=http://luisdigital.2y.net/comments.php3?op=showreply&tid=43&sid=55&pid=42&mode=&order=0&thold=0#43>Comentario</a>"
    author: "Luis Digital"
  - subject: "KMail Glossed Over"
    date: 2000-10-18
    body: "I believe the reviewer did not look deeply enough at the new version of kmail. The inclusion of search, nested folders, and message threading, make kmail an excellent choice to replace practically any full-featured mail client (I can't wait to upgrade my production PC to KDE 2) As for the popup ads, disable java/javascript b4 u visit... ;-) With KMail, KOrganizer, and KNotes, I think I can finally ditch Outlook 98 on my Windoze machine (YES!). Anybody get MS Combat Flight Simulator working under WINE, yet? :-)\n<BR>\n-Jerry"
    author: "Jerry Gluck"
  - subject: "Re: KMail Glossed Over"
    date: 2000-10-22
    body: "Also waiting for the 2.0, I just want to know if there are possibilities for multiple accounts and let the mail stay on the server (which is the most important e-mail function for people like me that need to access mails from 4 different pc's in 3 different locations). Another function that Netscape never succeded to solve (I heard that the Redmond terrorists have this function in their) is an indicator when old mail may be deleted by the mail-server. \nI have almost konverted to Kmail but do not really trust these functions after Kmail stole all my mails from one account, and then my pc running red hat 5.2 crasched. Now running Mandrake 7.1 it seems better and I got a more accurate version of Kmail."
    author: "Mats LG Rosen"
  - subject: "Re: Hispanic Slides, Kandidat Review, KDE2 On Schedule"
    date: 2000-10-19
    body: "Muy lindo!\n\nPero... Why use .gif?! When you have .png.\n\n;)"
    author: "weipruhfuds"
  - subject: ".PNG and .Gif"
    date: 2000-10-20
    body: "Las capturas desde hace un buen tiempo las realizo en formato png, pero algunos navegadores o navegadores un poco viejo no pueden leer los archivos png.\n\nDesde el 2001 en adelante usar\u00e9 png y quien no est\u00e9 actualizado para esa fecha se j* :)\n\nThe archives .PNG are still not supported in a 100%."
    author: "Luis Digital"
---
<a href="mailto:luis@luis-digital.8m.com">Luis Digital</a> wrote us about his various <a href="http://www.luis-digital.8m.com/linux/kde2/">presentations</a> on the KDE2 betas directed at the Hispanic community, but with screenshots everyone can enjoy.  In a similar vein, MachineOfTheMonth is presenting a <a href="http://www.machineofthemonth.com/articles/a36/index.html">review of Kandidat</a> (<b>warning</b>: popup ads).  As always, these kinds of reviews always tend to get out of date very quickly -- several of the issues mentioned have been addressed in more recent updates.  And finally, yes, KDE2 is <a href="http://lists.kde.org/?l=kde-core-devel&m=97169659432170&w=2">right</a> on <a href="http://lists.kde.org/?l=kde-core-devel&m=97170188314067&w=2">track</a> for a release next week.  Congrats to everyone involved!

<!--break-->
