---
title: "KDE's people: Lars Knoll"
date:    2000-11-21
authors:
  - "Inorog"
slug:    kdes-people-lars-knoll
comments:
  - subject: "Offtopic."
    date: 2000-11-21
    body: "Hi \n\nI am one of the non technical linux users around.  I find it difficult to update and keep track of all packages available on kde.  I was just wondering if an automatic update program wont be available.  I read something about a program called INCA?\n\nThank you for a good product.\n\nRegards"
    author: "Henk"
  - subject: "Re: Offtopic."
    date: 2000-11-22
    body: "Just use whatever software your distro uses to update your system, such as apt-get or yup. KDE is a package, just like every other package, and should be upgradable just like everything else is. If you distro doesn't provide a way to automatically easily upgrade, you should consider looking at another distro that does."
    author: "AArthur"
  - subject: "Re: Offtopic."
    date: 2000-11-22
    body: "as AArthur wrote: find a good distro... I recoment Linux-Mandrake 7.2 for the not-so-technical user. Its easy to figure out, and the update program is a wiz. Happy downloading ;)\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Offtopic."
    date: 2000-11-24
    body: "I am using redhat 6.2.  I do not want to change to mandrake due to several reasons.  I Also cannot download mandrake because I live in africa and it will take 10000 years with my bandwith and a small fortune. Surely an atitude of changing distros to accomodate KDE wont win you any new followers.  I am forced to stick with your competitors product because you do not have an easy update as they do and I therefor have to wait 6 months or longer for new stuff.  \n\nRegards"
    author: "Henk"
  - subject: "Re: Offtopic."
    date: 2000-11-24
    body: "download the rpms and install.."
    author: "Spark"
  - subject: "Re: Offtopic."
    date: 2000-11-25
    body: "I did, doens't work for me and also not for a lot of other people I know."
    author: "Henk"
  - subject: "Re: KDE's people: Lars Knoll"
    date: 2000-11-22
    body: "<i> I first heard of KDE a bit more than three years ago, when Kalle wrote an article in the german Ct (a computer magazine)</i>\n\nwell, i guess i just had a deja vu :)\ncould it be, that the whole core KDE team was founded this glorious day when \"Kalle wrote an article\"? ;)\nthis is at least the third time i read this."
    author: "Spark"
  - subject: "Re: KDE's people: Lars Knoll"
    date: 2000-11-22
    body: "Kalle should write more articles ;\u00b7)\nAnd they should be reproduced/translated in every country's computer mags :\u00b7)"
    author: "GeZ"
  - subject: "Re: KDE's people: Lars Knoll"
    date: 2000-11-22
    body: "is there a link to that article??"
    author: "caatje"
  - subject: "Re: KDE's people: Lars Knoll"
    date: 2000-11-22
    body: "I think it's this:\n<a href=\"http://www.heise.de/ct/english/97/08/276/\">http://www.heise.de/ct/english/97/08/276/</a>"
    author: "ac"
  - subject: "Re: KDE's people: Lars Knoll"
    date: 2000-11-23
    body: "Well, IIRC; at the time there were perhaps 35 of us already, but that article might have doubled that number.\n\nIn those days, a KDE article in a major mag was such big a deal that we actually updated our web pages in minutes to reflect it ;-)"
    author: "Roberto Alsina"
  - subject: "Re: KDE's people: Lars Knoll"
    date: 2000-11-23
    body: "Well, IIRC; at the time there were perhaps 35 of us already, but that article might have doubled that number.\n\nIn those days, a KDE article in a major mag was such big a deal that we actually updated our web pages in minutes to reflect it ;-)"
    author: "Roberto Alsina"
  - subject: "Re: KDE's people: Lars Knoll"
    date: 2000-11-23
    body: "Well, IIRC; at the time there were perhaps 35 of us already, but that article might have doubled that number.\n\nIn those days, a KDE article in a major mag was such big a deal that we actually updated our web pages in minutes to reflect it ;-)"
    author: "Roberto Alsina"
---
There's no possibly better way to present Lars to KDE fans than this: <i>"please, meet Lars KHTML...errr! <a href="http://www.kde.org/people/lars.html"><b>Lars Knoll</b></a>"</i>. Our ever-resourceful  <i>Tink</i> brings into near-view one of the legendary developers of the most powerful and most modern Free Software HTML rendering widget in existence. You're our guest into this new episode of <a href="http://www.kde.org/people/people.html">People Behind KDE</a>.


<!--break-->
