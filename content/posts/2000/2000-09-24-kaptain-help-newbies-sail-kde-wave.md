---
title: "Kaptain: help the newbies sail the KDE wave"
date:    2000-09-24
authors:
  - "raphinou"
slug:    kaptain-help-newbies-sail-kde-wave
comments:
  - subject: "Re: Kaptain: help the newbies sail on the KDE wave"
    date: 2000-09-24
    body: "Looks really cool!\nShould ship with the standart KDE package IMHO."
    author: "d00d"
  - subject: "Re: Kaptain: help the newbies sail the KDE wave"
    date: 2000-09-25
    body: "I started on a similar project a couple of month ago but never\nhad time to get any where close to this far. Needles to say\nI think it's a great  to see it realized. I hope to get some time\nto help this project. When most commandlines have been\nwrapped, we'll never have to hear that \"linux is only for\ncommandline jerks\" argument anymore ;-)"
    author: "sune"
  - subject: "Re: Kaptain: help the newbies sail the KDE wave"
    date: 2000-09-25
    body: "Would it be possible to integrate this program in the context menu for registered apps in konqueror?\nIt would be great to automatically popup this for console apps, because they normally can't be run from konqueror directly if you have no konsole part active."
    author: "Martijn Klingens"
  - subject: "Re: Kaptain: help the newbies sail the KDE wave"
    date: 2000-09-26
    body: "<p> love Kaptain.\n\n<p> review at <a href='http://www.linuxtoday.com.au/r/article/jsp/sid/131347'>LinuxToday.com.au</a>\n\n<p>keep up the good work over at kde :)\n\n<p>Renai"
    author: "renai42"
  - subject: "Re: Kaptain: help the newbies sail the KDE wave"
    date: 2000-09-26
    body: "That's so cool!  Thanks!\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "We need to get working"
    date: 2002-11-26
    body: "Has there been a project started to make grammar scripts for ALL commands?  i know we could (and should) start a project to centralize development.  This is a great Idea and Perhaps there can be a compatible \"Gaptin\" for those sailing the Gnome wave :).  I want my girlfriend to be able to use linux."
    author: "standsolid"
---
Browsing the kde-announce mailing list archive, I found an announcement for the
release of Kaptain, a universal graphical front-end for command line
programs. I
didn't understand very well what could be the use for such a program, but I was curious, and so I surfed to the project's <a
href="http://www.hszk.bme.hu/~tz124/kaptain/">homepage</a>. And what I
discovered was a little utility that can help you  quickly develop a user
interface to launch a command that would otherwise be long to type and hard to remember.  Check out this cool <a href="http://www.hszk.bme.hu/~tz124/kaptain/docs/enscript.html">example</a> with screenshots (and some more <a href="http://www.hszk.bme.hu/~tz124/kaptain/gram.cgi">here</a>).  I'm now convinced that kaptain can be used to significantly improve a newbie's experience with Unix.










<!--break-->
