---
title: "And in the news today..."
date:    2000-09-06
authors:
  - "numanee"
slug:    and-news-today
---
Evan Leibovitch @ZDNet writes an entertaining piece summing up the recent KDE/GNOME events in <a href="http://www.zdnet.com/enterprise/stories/main/0,10228,2624499,00.html">"It's the apps, stupid"</a>.  Stephen Shankland @CNET writes about the Qt GPL announcement in <a href="http://news.cnet.com/news/0-1003-200-2703728.html">"Barrier lowered between Linux interfaces"</a>.  Rick Lehrbaum @ZDNet reports on the same in <a href="http://www.zdnet.com/enterprise/stories/main/0,10228,2623592,00.html">"Good news for KDE"</a>. Last, but certainly not least, the KDE 2.0 release schedule has been <a href="http://lists.kde.org/?l=kde-core-devel&m=96814391916282&w=2">announced</a>.


<!--break-->
