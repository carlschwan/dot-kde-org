---
title: "KDevelop 1.3 Released"
date:    2000-12-11
authors:
  - "Dre"
slug:    kdevelop-13-released
comments:
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-11
    body: "Why hasn't this been ported to Qt2.x/KDE2.0?  This is a good application and should be available to integrate into the newest incarnation of the KDE."
    author: "Christopher Lee Fleck"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-11
    body: "Just wait for KDE 2.1 :)"
    author: "ac"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-11
    body: "There is a version in CVS that's a full KDE-2.0 app.  Maybe you could try that ?"
    author: "Forge"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-12
    body: "But the 2.0's version from CVS is really very unstable. I checked it out and got only bad experiences with it."
    author: "Christian Parpart"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-12
    body: "That's why it isn't released yet and the KDevelop\nteam decided to release this \"interim\" version\n1.3 that is based on KDE 1.x but allows for\nKDE 2.x development. ;-)\n\nWhenever you've got a question like \"Why isn't this ported/developed/done yet?\" the answer in\nfreeware space most probably is: Because nobody\nhas done/finished it yet."
    author: "Uwe"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-12
    body: "Hehe, you\u00b4ve checked out HEAD. But you should check out -r KDEVELOP_1_4 for the 1.3 ported over to KDE 2 recently. While the port has still some -obviously- porting bugs that have to be solved - and where help is greatly appreciated- it will ship with KDE 2.1 in spring.\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-17
    body: "because company/developper still depend on KDE 1.x\n\ni no longer use QT1.x and KDE 1.x since year and i have try kdevelop static and its perfect. it makes kde2 project fine.\n\nyes ! im having the kde2 look, but ....\n\nwe are waiting for the look, not for coding ;)"
    author: "me (smk)"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-12
    body: "Exelent... now the question is.. how does Kdevelop and Kdestudio (kstudio) fit toghether???"
    author: "Mario"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-12
    body: "how exactly KDEStudio differs from KDevelop ??"
    author: "mushtaq"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-13
    body: "KDevelop and KDE Studio are 2 different projects with similar and different aims.  KDE Studio is intended as more of a RAD tool like Delphi and KDevelop aims to be (IIRC) more of a code editor and environment, not really a GUI Designer like Qt Designer.  They appeal to different types of people depending on their style.  We use KDE Studio internally for almost everything."
    author: "Shawn Gordon"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-13
    body: "Hello,\n I tried KDE Studio, it was very very good. You mentioned that it has RAD support, but I haven't seen a few of toolbar components (like QT designer on it). Hmm.. maybe this is my mistake, but could you please confirm this?\n\nThanks,\nPrana"
    author: "Prana"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-13
    body: "KDE Studio is not on a par with Qt Designer in terms of a GUI builder, it has a few items like the dockable toolbar that was added to core KDE and the form builder, but we are working on a totally new version that should really knock your socks off when it comes out :)."
    author: "Shawn Gordon"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-13
    body: "What I meant was, I was really impressed with the screenshot of KDE Studio. I saw there was a form builder in one of the screenshot (#2). I also saw some components there (eg: toolbar, etc). However, when I downloaded the RPM package, there was no GUI builder like in the screenshot :-( I'm confused.\n\nQT Designer doesn't have menu and toolbar generator (which I really want to learn), and it doesn't have internal IDE like KDE studio.\n\nBy the way, do you need a person who's willing to write more documentation for it? Let me know if you do. Seeing your e-mail addr (@thekompany.com) maybe you're one of the developer. Maybe I can contribute/volunteer something. I usually develop gnome apps @ http://www.cyest.org, so I have a little bit of previous experience (but not with QT yet)."
    author: "Prana"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-13
    body: "I'm the President of theKompany :).  The developer is amazing with C++, but his english isn't so good :), help with doc is always appreciated, send me some private email and we can talk about it.\n\nThere have been some changes with the 2.0 release, some great new things were added, but some other things got dropped like the debugger (the reasons are too long to get into).  All will be well with the next generation known as KODE."
    author: "Shawn Gordon"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-13
    body: "I have a hard time seeing the good, aside the completion feature it dosen't really do much. It's nice that you can add some more minimal starter classes, but on the other hand the templates in kdevelop really rocks, and the debugger is a big help for sloppish typers like me.. Whe they get it to work, the \"add variable/method\" feature of kdevelop is an other argument, not to mention the integration w/ qt designer which works very well!\n<p>\n-anders"
    author: "Anders Lund"
  - subject: "Re: KDevelop 1.3 Released"
    date: 2000-12-17
    body: "just forget kdestudio ;)\n\nkdevelop rocks !"
    author: "me (smk)"
  - subject: "How about support for other languages..."
    date: 2000-12-13
    body: "Such as Java, Perl or Python...\n<p>\nI am just a begginer programmer so I apologize if this is really really really harder than it sounds..."
    author: "Abreu"
  - subject: "Re: How about support for other languages..."
    date: 2000-12-13
    body: "We hope that someone will write plugins for this languages for KDevelop2. :-)"
    author: "Sandy Meier"
  - subject: "Re: How about support for other languages..."
    date: 2000-12-13
    body: "PHP would be great too! (Although I heard this support is planned to be in the next version of Quanta)"
    author: "Carbon"
  - subject: "khartom"
    date: 2005-05-25
    body: "how to use kdevelope & qt designer to creat gui & use c language"
    author: "sara"
---
KDevelop 1.3 is out.  From the press release:  <EM>"The <A
HREF="http://www.kdevelop.org/">KDevelop Team</A> today announced the release ofKDevelop 1.3, a powerful, easy to use, Integrated Development Environment for
the C and C++ Programming Language based on KDE. KDevelop 1.3 is a major
update release that follows the successful versions 1.1 and 1.2 to enable
developers for rapid application development for the new <A
HREF="http://www.kde.org">KDE 2.0</A> Desktop."</EM>.  The full release is below.


<!--break-->
<P>DATELINE DECEMBER 10, 2000</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">New Release of the KDevelop C/C++ IDE for the KDE Desktop</H3>
<P>
<STRONG>New Version of Leading C/C++ Integrated Development Environment for
Linux and Other UNIXes Ships
</STRONG>
</P>
 
<P>December 11, 2000 (The INTERNET).  The <A
HREF="http://www.kdevelop.org/">KDevelop Team</A> today announced the release ofKDevelop 1.3, a powerful, easy to use, Integrated Development Environment for
the C and C++ Programming Language based on KDE. KDevelop 1.3 is a major
update release that follows the successful versions 1.1 and 1.2 to enable
developers for rapid application development for the new <A
HREF="http://www.kde.org">KDE 2.0</A> Desktop. KDevelop is the work of an
international team dedicated in providing an IDE under the Linux and UNIX
operating systems, their freely available product is known to be stable and
improving productivity for any C/C++ developer.
</P>
<P>
The primary goals of the 1.3 release are to simplify the development process of
new applications for the UNIX desktop KDE 2.0, to enable corporations to
easily deploy the leading API of KDE 2.0 and ease their decision to create
programs that are using the newest Office and Inter-Process-Communication
facilities that currently arose on the UNIX desktop with the latest release of
KDE. </P>
<P>
Specifically the new development tools provided by <A
HREF="http://www.trolltech.com/">Trolltech</A>, the Qt Designer and the upcomingQt Linguist, as well as the KDE internationalization program KBabel are directlysupported and integrated into the development process. Another major feature,
the Projectfile Generator included in KDevelop, allows to easily work on alreadyexisting automake projects with KDevelop as a matter of seconds that will bring
the full sense of the GPL and other Free Software licenses to their destination
of code-reuse where possible. Now developers can have a look at the full KDE
sourcecode and any other project to look up the code for the functionality they
want to provide in their application. The availability of the Qt and KDE API
within the development environment simplifies and reduces the effort developers
have to put into finding the information they need for programming under
time-to-market aspects.
</P>
<P>
While KDevelop 1.3 is still based on KDE 1.1.2 code, the team provides a binary
version that has proven to run on most Linux distributions without having KDE
1.1.2 libraries installed. Therefore, developers whose systems are not enabled
to compile KDE 1.1.2 applications can still make simple use of the C/C++ IDE forrapid application development. </P>
<P>
The KDevelop 1.3 IDE is available for free under the GNU General Public License
(GPL) as KDE is available on Open Source licenses. Likewise,
<A HREF="http://www.trolltech.com/">Trolltech's</A> Qt 2.2.2, the GUI
toolkit on which KDE is based,
is also available for free under two Open Source licenses:  the
<A HREF="http://www.trolltech.com/products/download/freelicense/license.html">Q
Public License</A> and the <A HREF="http://www.gnu.org/copyleft/gpl.html">GNU
General Public License</A>.
</P>
<P>
More information about KDevelop 1.3 is available on the
<A HREF="http://www.kdevelop.org">KDevelop Home Page</A> and on
<A HREF="http://www.kde.org/">KDE's web site</A>, along with
<A HREF="http://developer.kde.org/documentation/kde2arch.html">developer
information</A> and a developer's <A
HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html?rev=2.3">KDE 1 - KDE 2 porting guide</A>.
</P>
<P>
<H4>Downloading and Compiling KDevelop</H4>
</P>
<P>
The source packages for KDevelop 1.3 are available for free download at
<A
HREF="ftp://ftp.kdevelop.org/pub/kdevelop/">ftp://ftp.kdevelop.org/pub/
kdevelop/</A>  or at one of the many KDevelop ftp
server <A HREF="http://www.kdevelop.org">mirrors</A>. KDevelop 1.3
requires qt-1.44, which is available from TrollTech´s ftp server at <A
HREF="ftp://ftp.troll.no">ftp://ftp.troll.no</A> and the KDE 1.1.2 libraries,
available at the KDE ftp servers at <A
HREF="ftp://ftp.kde.org">ftp://ftp.kde.org</A> for compilation, for developing
applications you can feel free to choose either version of the KDE 1.1.2 or KDE
2.0 as well as Qt 1.44 or Qt >2.2.1 libraries as both are directly supported.
</P>
<P> For further instructions on compiling and installing KDevelop, please
consult the installation
instructions provided with the source package of KDevelop.
</P>
<P>
<H4>Installing Binary Packages</H4>
</P>
<P>
Some distributors choose to provide only packages of KDE 2.0 for certain
versions of their distribution already. As this prevents compiling KDevelop due
to a lack of a KDE 1.1.2 installation, the KDevelop Team provides a binary
version that is known to work under most Linux versions. The binary version of
KDevelop is therefore available for free download under <A
HREF="ftp://ftp.kdevelop.org/pub/kdevelop/">ftp://ftp.kdevelop.org/pub/kdevelop/</A> or under the equivalent directory at one of the many KDevelop ftp server
<A HREF="http://www.kdevelop.org">mirrors</A>. </P>
<P>
<H4>About the KDevelop Project</H4>
</P>
<P>
The KDevelop Project, started in summer 1998, aims to provide developers with a
good, stable and useful environment which can compete with modern IDEs found on
other operating systems. Since the beginning of the development, the team has
grown from three members of the core team to over ten codewriters and
a translation team with over 50 people all dedicating their free time to work
on this project. In the meantime, a lot of successfull users of the KDevelop IDEhave joined and shared their experience to improve KDevelop in stability and
functionality.
</P>
<P>
For more information about KDevelop, please visit the KDevelop
<A HREF="http://www.kdevelop.org">web site</A>.
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
Linux is a registered trademark of Linus Torvalds.
UNIX is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
All other trademarks and copyrights referred to in this announcement are the
property of their respective owners. <BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD NOWRAP>
Ralf Nolden<BR>
nolden@kde.org<BR>
(49) 2421 502 758
</TD></TR>
</TABLE>

