---
title: "People behind KDE: Lauri Watts"
date:    2000-09-19
authors:
  - "rwilliams"
slug:    people-behind-kde-lauri-watts
comments:
  - subject: "Re: People behind KDE: Lauri Watts"
    date: 2000-09-22
    body: "Am I the only one who has difficulty reading the \"people\" pages ?\n\nThey work fine in Windows but on a basic Mandrake 7.1 installation both Konqy and Netscape display in tiny, ugly unreadable fonts.  Adding my Windows fonts to XFS solves the problem but editing the HTML works better.\n\nWhy can't they let my browser choose the font like the rest of kde.org ?\n\n===\nThere are no underscores '_' in my Email address."
    author: "Forge"
---
The second of the on going interview of the people behind KDE is out.  This interview is with <A HREF="http://www.kde.org/people/lauri.html">Lauri Watts</A> of the documentation team.
<!--break-->
