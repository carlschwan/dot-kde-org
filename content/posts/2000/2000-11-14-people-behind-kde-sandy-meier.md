---
title: "People behind KDE: Sandy Meier"
date:    2000-11-14
authors:
  - "Inorog"
slug:    people-behind-kde-sandy-meier
---
Our interviewer-in-chief, <i>Tink</i>, comes back this week with her entertaining suite of questions, this time answered by <b>Sandy Meier</b>, member of the <a href="http://www.kdevelop.org">KDevelop</a> creators team. Go learn about Sandy's <a href="http://www.kde.org/people/sandy.html">human profile</a> and don't forget to visit his project's web page. Who knows, maybe you decide you'd like to become part of that wonderful team.

<!--break-->
