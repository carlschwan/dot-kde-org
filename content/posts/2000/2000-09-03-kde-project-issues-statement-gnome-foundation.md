---
title: "KDE Project issues statement on GNOME Foundation"
date:    2000-09-03
authors:
  - "numanee"
slug:    kde-project-issues-statement-gnome-foundation
comments:
  - subject: "Re: KDE Project issues statement on GNOME Foundation"
    date: 2001-01-12
    body: "An excellent statement.  Thank you.  It is re-assuring to know that the beautiful and functinal KDE GUI will be there.\n\nIt seem a good idea to at least extend a welcoming hand to GNOME.  They are doing similar things and probably have some excellent ideas which may help both.  Their Abiword is a great example of fine programming.\n\nAgain, thanks to all KDE programmers.  Your hard work is really appreciated.\n\nEd"
    author: "Ed Rataj"
  - subject: "Re: KDE Project issues statement on GNOME Foundation"
    date: 2001-04-11
    body: "Err, for one thing, haven't you read the articles for KDE-GNOME interoperability?\nAnd AbiWord is NOT a GNOME app and has nothing to do with GNOME; it just uses Gtk for its GUI toolkit."
    author: "Triskelios"
---
The KDE Core Team took a break from coding and wrote up an <a href="http://www.kde.org/announcements/gfresponse.html">official response</a> to the whole GNOME foundation thing.  It wouldn't have been necessary if not for the endless questions and misinformation about the project ever since the original announcement.


<!--break-->
