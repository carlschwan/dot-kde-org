---
title: "KDE packages already in Debian"
date:    2000-09-14
authors:
  - "numanee"
slug:    kde-packages-already-debian
---
Debian Weekly News <a href="http://www.debian.org/News/weekly/2000/29/">confirms it</a>: <i>"KDE packages are pouring into Debian. All of the core of KDE is already present in unstable, and more packages are sure to follow. This unexpected turn of events is due to a change in the license of Qt 2.2 -- Troll Tech released it dual-licensed under the GPL -- the KDE licensing issue is finally resolved."</i>  They have a lot of catching up to do if the ~700 KDE apps catalogued by <a href="http://apps.kde.com">apps.kde.com</a> are any indication.

<!--break-->
