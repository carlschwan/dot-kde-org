---
title: "KDE2 release schedule update"
date:    2000-09-27
authors:
  - "numanee"
slug:    kde2-release-schedule-update
comments:
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-26
    body: "THANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!\nTHANKS!!!!!!!!!!!!"
    author: "Manuel Rom\u00e1n"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-26
    body: "After reading the thread in the mailing-list, may I suggest the ultimate sales pitch:\n\n KDE 2001 - one up on Windows\n\n(sorry, couldn't stop myself)"
    author: "reihal"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "A recent scientific comparison of windows 2000\nand Linux came to the conclusiun that windows\n\nas a lead of 833% over Linux, regarding version\nnumbers.\n\nMatthias\n\n--\n  whatever you do... don\u00b4t tell anyone"
    author: "Matthias Lange"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "I'm worried. Kde2 beta has too many bugs. Konqueror crushes even more than two weeks ago,\nKpackage can't install anything etc. I installed\nevery package of Kde2 since 1.92 release, but unfortunatelly must say that preview release of Nautilus runs more stable than Konqueror. Good luck to Kde developers."
    author: "Zeljko Vukman"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "Packages?  Which packages, and where did you get them from?  What's your system?  I would advise you to compile from source before freaking out."
    author: "KDE User"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "I have used 1.91, 1.93 and now I'm typing this with 1.94 Konqueror and I have to say that the system is getting more and more stable every release.\n\nBut I agree that many parts of the system need lots of attention. I hope that the current snapshots are better as the 1.94 I'm using to that respect.\n\nSome specific points regarding 1.94:\n- Konqueror handles many Javascript statements wrong, like logging in to Netscape webmail, or even a simple (yes, annoying!) popup window\n- Konqueror has many troubles handling CSS properly\n- KOffice doesn't seem 'finished' at all to me right now\n\nAnd I'm not mentioning all minor bugs, because I guess these are already fixed now, or at least they will be when KDE will be released.\n\nI can only hope that this release date is realistic, but I share my fears with you that it might be a little early for that.\n\n- Martijn"
    author: "Martijn Klingens"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "I'm one of the neglected Linux/ALPHA users.\nI still have to run 1.92 because all later\nversions didn't compile (internal compiler\nerrors) or didn't run (when turning all\ncompiler optimizations off).\nHope this will be fixed till the release.\n\nMirko"
    author: "Mirko Saam"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-10-01
    body: "Actually - it's already been fixed. Grab the sources from the CVS (or wait for the release) and try it - it works perfectly."
    author: "Hetz"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-10-02
    body: "KDE2 runs \"quite\" fine on my system and is \"relatively\" stable. However it's not stable enough for a \"final\" release, IMHO... Does anybody remember KDE 1.0? For me it was worse than KDE1 Beta4 and KDE 1.1 was the first really \"finished\" version of KDE. until KDE1 and GNOME 1.0 I thought that open source software had honest version numbers but that does not seem to be the case... I fear that we'll have to see \"KDE 2.0 final\" as KDE2 beta6 and \"KDE 2.1\" as \"KDE 2.0 final\"... :-(\nIt's be better to wait some weeks until releasing \"KDE2 final\"...\n(BTW, kpackage works for me.)"
    author: "Gunter Ohrner"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "I like what I've seen of KDE 2.0 so far, but I have a few issues:\n\n1)  It seems to be very difficult to upgrade, particularly from Beta3 to the later betas.  I've been trying to get Beta5 up and running on Red Hat 6.9 (Pinstripe) with no success, and right now, my Beta3 installation even seems to be broken.  Not good.  I hope that any future release will have a simplified upgrade procedure that will deal with issues like setting KDEDIR and QTDIR, both of which have been giving me fits.\n\n2)  I have to agree with an earlier comment about Konqueror.  I hope it gets more stable.  To be fair, it's got some nice features I haven't seen elsewhere, but it definitely has a tendency to crash on my machine.\n\nKDE is supposed to make things simple, right?  So the installation should be simple, too.  (If it really is simple, and I've just been doing something blatantly wrong, feel free to email me and help me out.  Help's always appreciated. :))"
    author: "Brian Hartman"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "Why don't you remove beta3 before installing a later beta? You won't loose anything, AFAIK.\n\n-- Stephen"
    author: "Stephen Boulet"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "Ijust installed the latest Linux Mandrake beta and it is looking very good. Konqueror is very stable, there are a few rendering issue,but it looks very nice.\nThe biggest problem I have ran into is that the Proxies don't work right.  i set my proxy server and as long as I stay on a single page it work, but as soon as I click a link to another site it prompts for the password again.\nPlease fix this as it is very annoying.\n\nTony Caduto"
    author: "Tony Caduto"
  - subject: "Maybe there should be a second \"final\" beta"
    date: 2000-09-27
    body: "KDE 1.94 is reliable enough so that I can use as my daily desktop, but there are still quite a few bugs and things that don't work quite right. I've crashed Konqueror when making a file association, KDE &quot;legacy&quot; themes are more memory hungry on my system than the original GTK+ themes they are based on, and Kmenuedit is a bit braindead; if I want to create a menu item that I can make a working kicker panel launcher from, I have to type &quot;Application&quot; in the debatably necessary &quot;Type:&quot; field. What's up with that? In short, there are a lot of niggling little bugs that are too easy to find. If the KDE developers put out another beta or a prerelease, they could double-check that they've got all the showstoppers before they release the final, production-ready version."
    author: "J. J. Ramsey"
  - subject: "Re: Maybe there should be a second \"final\" beta"
    date: 2000-09-27
    body: "KDE 2.0 is not the end of the line.  It's time for a release, NOW.  It's as simple as that, after two years.  There will always be bugs.  Those bugs will be fixed.  But there is no point in delaying KDE 2.0.  KDE 2.0 is worthy and it's about time."
    author: "KDE User"
  - subject: "Re: Maybe there should be a second \"final\" beta"
    date: 2000-09-27
    body: "I agree that KDE 2 should be released soon but I also don't see the point to release a 2.0 version and then release a version 2.0.1 with bugfixes a few weeks later. The KDE 2 betas are getting better and better with every release but I still believe that a few more weeks of debugging and testing would not hurt anybody."
    author: "Stefan"
  - subject: "Re: Maybe there should be a second \"final\" beta"
    date: 2000-09-27
    body: "Hear, hear.  The Linux world has traditionally been about honest development- and beta-cycles, with the hard-earned reputation that <I>stable</I> releases are <I>stable</I>.  GNOME 1.0 did some serious damage to that reputation, during a time when KDE 1.X was getting more stable (and often faster) with each release.\n\n<P>I'd really rather see KDE build on their good reputation, if it only costs a couple of weeks."
    author: "Andy Marchewka"
  - subject: "Re: Maybe there should be a second \"final\" beta"
    date: 2000-09-28
    body: "It's toooo early!\nI'm using 1.94 and it doesn't look any better than previous releases. Konqueror is pretty good (not excellent) for file management but for browsing is almost useless. It renders some pages very well, but it seems to me that khtml is slower than in earlier releases.Most of the websites cannot be seen properly so I have to start bloody netscape to see some of them.Until Konqi shows majority of websites correctly it would definetely not be considered as a serious browser. Kwin and kicker are different story. They swap so much space that's unbelievable. Few nights ago, I was watching a movie and when I returned to computer they swapped about 90 Mb altogether. \nBasicaly my opinion is, don't rush the release to catch Gnome as they wind up with the foundation and stuff,just do what you do the best \"Create great code\". No matter what happened I'm with you guys"
    author: "J Blazevic"
  - subject: "Re: Maybe there should be a second \"final\" beta"
    date: 2000-09-30
    body: "Agree to the fullest.  KDE2 is getting more and more stable each day.  But it is NOT stable enough for a release.  Let's use a couple of more weeks on it.  A couple of more weeks with working will give the developers less trouble later when the masses start using it and pointing at the bugs that are there...\n\nps.  konqueror didn't even render this page properly (1.94, and yes, compiled from src)"
    author: "Christian A Str\u00f8mmen"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "I am a linux user in Taiwan.\nMay I patch KDE2.0 for Big5 ?\nBig5 is the Chinese type used by the human in Taiwan."
    author: "Rony"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "I hope so for you.  How hard is it to patch? You should better use the mailing lists and send your patches there.  Also look at <a href=\"\">http://www.mizi.com/kde/</a> you may want to work with this person."
    author: "ac"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "All I can say is COOL..I wish tomorrow was October 16..Cheers to the KDE team"
    author: "Aravind Sadagopan"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "What i saw on Beta 3 was great.\nI can\u00b4t wait no longer for the final release!\nHowever, i\u00b4m still concerned about stability and speed. Maybe it is so slow due to debug information, i hope so. The konqueror does not work stable and the KOffice suite is very very sensible. But KDE1 Betas teached me to trust the KDE Team and so i will with KDE2.\nCongratulations for the KDE people."
    author: "Matos"
  - subject: "Why do QT version changes require `porting?'"
    date: 2000-09-27
    body: "Hi,\n\n<p>\nI'm a happy KDE user, but am not familiar with the programming side of things. Could someone explain why upgrading to qt 2.0 and the upcoming qt 3.0 require the porting of the entire kde codebase, and not a simple recompilation? \n</p>\n\n<p>\nIt seems like none of the coding and bugfixing that went into kde 1.x is applicable to the upcoming kde2 release. As far as users are concerned, KDE2 could be a completely new desktop! It also seems like the reason why the number of KDE apps did not increase significantly in the last couple of years was because potential app-writers were waiting for kde2 to get released to avoid having to redo their work. \n</p>\n\n<p>\nNow, according to the roadmap, the same situation will happen when going from qt 2.0 to qt 3.0... porting will be necessary. Which means that none of the apps that people write for KDE2.0 will be naturally available for KDE2.2. \n</p>\n\n<p>\nIf this is indeed the case, this seems to be a singularly brain-dead method. I'm surprised that Trolltech's paying customers do not bitch about the regular API changes. I would appreciate any clarifications.\n</p>\n\nGeorge."
    author: "George Smiley"
  - subject: "Re: Why do QT version changes require `porting?'"
    date: 2000-09-27
    body: "My understanding of the QT versioning scheme is that the major versions (like 1.x vs 2.x) may have slight source incompatibilities where there is no other sane solution, while the minor revisions (like 2.0 vs 2.1) are backwards compatible when they introduce new features and the bugfix releases (2.1.0 vs 2.1.1) have no interface changes at all.<p>\nThe last major upgrade to 2.0 at least required a few changes on source level, see\n<a href=\"http://www.trolltech.com/developer/changes/2.0.html\">http://www.trolltech.com/developer/changes/2.0.html</a>"
    author: "Arnd Bergmann"
  - subject: "Re: Why do QT version changes require `porting?'"
    date: 2000-09-27
    body: "As you say, you are not a programmer, and you\nclearly are not familiar with the concept of\nmajor versions.\n<P>\nMajor versions break APIs. This is a good thing,\nbecause it means you can fix things that were\nwrong in the previous version, and add plenty\nof new features and facilities. This is exactly\nwhat happened with Qt 1 => Qt 2.\n<P>\nAt the same time, KDE has been re-written almost\ncompletely. It hasn't been \"ported\" as such.\n<P>\nTo illustrate, it would have been <i>possible</i>\nfor the KDE team to have produced KDE2 using Qt 1.x (a bad idea, but possible). But since they\nwere changing major version, it made perfect sense\nto move to the much-improved Qt 2 at the same time.\n<P>\nIn actual fact, a simple port of a KDE1 app to KDE2 is pretty easy. Programmers don't generally bitch about API changes between major versions, because that is what major versions are <i>for</i>."
    author: "John Levon"
  - subject: "Re: Why do QT version changes require `porting?'"
    date: 2000-09-27
    body: "OK, thanks for the clarification. I understand that going from qt1 to qt2 was necessary, and since KDE was completely rewritten anyway, I guess the apps would have to have been ported even if KDE had continued to use qt1. \n\nBut will the same thing happen while going to qt3? I guess there won't be much difference in features between kde2.0.x based on qt2 and kde2.2.x based on qt3. Will apps require porting or will a mere recompile do?\n\nGeorge."
    author: "George Smiley"
  - subject: "Re: Why do QT version changes require `porting?'"
    date: 2000-09-27
    body: "A mere recompiling they say.  It will be mostly source compatible but not binary compatible."
    author: "KDE User"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "I don't know if this still isn't too soon.  Well, after how long we've all waited, it's probably not too soon, but I still can't help but feeling that there are some things that aren't done yet.\nKonqueror doesn't do SSL through proxies.  There isn't even an option to set a port for it.  I also wish that that directory window tree in the left hand pane would remember that I hid it last time I ran Konqueror. Can't we save that state in an rc file?\n\nFor some reason, Konsole doesn't have keyboard short cuts in the menu bar.  Does it have a hot key for switching among open terminals or for opening new ones?\n\nAnd KWrite no longer lets you choose the display font.  What's up with that?  \n\nOther than SSL stuff, most of this stuff wouldn't be terribly hard.  Maybe I should try to fix it myself and send in some patches...  Probably a lot more effective than complaining, right?"
    author: "Thomas Philpot"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "Happily, Konsole supports switching between shells with Shift-Left and Shift-Right.  Don't know about the rest.  I'm pretty sure KWrite lets you change the font, Konqueror lets you store profiles and the other stuff hardly sound like showstoppers, do they?  Assuming that these problems haven't been fixed already."
    author: "ac"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-27
    body: "Congratulations. My Beta5 is very usable (in fact I use it exclusively now) but I'm allways eager for an upgrade.\nWhat really amazed my is that you have already plans for KDE2 1.2 with Qt3.0... Way to go!!!\n\nJoao"
    author: "Jo\u00e3o Cardoso"
  - subject: "KOffice not at all release ready"
    date: 2000-09-28
    body: "Konqui seems pretty unstable too - *ESPECIALLY* as a web browser.\n\nSomeone else mentioned that Koffice isn't \"ready\". It's true that Koffice has no useful help files or documentation (one is supposed to know all about MSWord and just guess that Koffice is similar or something). The reason is that KDE and KOffice are simply amazing projects - open source is way more stable than the \"shareware\" approach - but it just doesn't compare to polished commercial software (remember MSWord is in version 9 or so). But if people would/could use Koffice widely (thus helping debug it) and document it, provide tips. etc it will improve rapidly. It would be nice to have auto bug and crash data reporting; auto file saving on crashes (so the fact that it's buggy won't stop people from using it); and a template or series or python/dcops macros that would make it really easy to create a simple Koffice \"tip\" or \"how to\" (replete with screen shots) and would automatically upload them to a central web site. A categorized list of tips and how tos (in searchable XML) would be maintained so that less duplication would occur. After 3-6 months someone could edit, merge and purge the contributions and put together a fairly comprehensive bunch of documentation.\n\nThe problem will be that unlike kernel hackers which are numerous since commercial companies benefit from enhancing the system for use as server OS (and these people are knowledgable experts) typical Koffice users won't be coders.  So it will be difficult to see the same kind of improvements occur in Koffice as have occurred in the kernel. In some ways Koffice is more complex as well ..."
    author: "AC Of Course"
  - subject: "Re: KOffice not at all release ready"
    date: 2000-09-28
    body: "Try Star Office. It rarely crashes and although it is slow it does just about everything imaginable:)"
    author: "cockney"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-28
    body: "I'm questioning the version numbering scheme.  what is the deal with the KDE2 1.0 stuff then kde2 1.1 and 1.2.  It doesn't make sense.  It is confusing because it makes you think of kde1.2.  Perhaps I'm not up to speed on your guys' thinking but then again no newcomer to linux (which kde2 looks promising as that one missing link) is going to understand your version numbers.  Call it KDE 2.0 then KDE 2.1 and 2.2.  Don't include all that 1.0 stuff.  It is weird to the non developer.  \n\nSorry if I'm totally out of line but I wanted to make sure my thoughts were heard reguardless if they are stupid.  (The only stupid questions are unasked ones right?)"
    author: "Josh Bauguss"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-28
    body: "I have 50 users beta testing KDE beta3 and I can tell you that there seems to be an endless amount of bugs. I know that I have probably subjected my users to this unstable environment too early, although I feel the version we are running now surpasses the KDE 1 series by far. I figured that we will all deal with the bugs until the final release of KDE 2.0. I live in hope that the KDE team has enough professionalism to release a fully functional, stable, desktop environment and that they will not fall short by releasing something that will cost my team hundreds of man hours to support because they could not wait until all critical and grave bugs are fixed. If they do release a broken environment or one that is even partially broken I may find myself installing Winblows on 50 machines and all of my debates with my bigoted users will have been in vain. The excuse that I give now is that it is not finished. What if they release KDE 2.0 and it still is not finished. I am sure I am not the only one in this situation. \"Final\" means \"Final\" to me. It sure looks pretty though. Good job Guys!"
    author: "cockney"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-28
    body: "<i>KDE beta3 and I can tell you that there seems to be an endless amount of bugs</i>\n\nSorry, i can not feel pitty about you. Read my lips: _never_ use Beta-Software for prodcution systems. Beta-Software remains beta, even if its open-source.\nAnd second: beta3 is some months old, even the latest beta5 is stone age compared against the recent cvs."
    author: "Lenny"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-29
    body: "We have now up-graded to beta 5 and even the final versions of kde1.0 where not pruduction quality. So what you are saying is that I should never expect a production quality desktop environment from the open source community??"
    author: "cockney"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-29
    body: "He's simply saying it's a very bad idea to run ANY beta software in a production environment. As you seem to be in charge of lots of production computers, this should be basic knowledge. Unless, of course, you have hired 50 people for the sole sake of beta testing KDE2? ;)\n\n<p>KDE 2.0 will NOT be as stable as KDE 1.1.2 was. Likewise, KDE 1.1.2 was a LOT more stable and mature than KDE 1.0. If you didn't find KDE 1.1.2 to be of production quality, then frankly:\n\n<p>1) Try Windows ME instead (maybe Linux is not for you), or<br>\n2) Report or fix the bugs, or<br>\n3) Try the &quot;other&quot; desktop environment, or<br>\n4) Try the commercial Linux CDE (deXtop)"
    author: "Haakon Nilsen"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-10-02
    body: "Users will be disapointed when the released KDE 2.0 is less stable than KDE 1.1.2.\n\nI think what the original poster wanted to say is that you should wait with the anouncement until KDE 2.0 is more stable than KDE1.1.2, where i do agree.\n\nAdding features and neglecting stability is the Microsoft way to do things. An unstable final release of KDE 2 could spoil the reputation of open source. I have coleagues ho already complain, that Linux is not more stable and not faster than Windows (they mean Netscape, kfm 1.x, Staroffice, etc.). You now have the choice to teach them wrong or right! \n\nBye,\n Stefan Heimers"
    author: "Stefan Heimers"
  - subject: "question about K-word"
    date: 2000-09-29
    body: "I was just curious? does K-word have an auto save feature?"
    author: "Lance DiBitetto"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-29
    body: "Anybody knows, why exception handling is enabled by default in all packages ?\n\nKDE project wants to win prize for most bloated desktop environment obviously :-("
    author: "fura"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-30
    body: "The makefiles that I have contain -fno-exceptions.\nWho compiled your packages?\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-30
    body: ">Who compiled your packages?\n\nMe ;-)\n\nHmmm ... Maybe that's only problem on my side.\nEarlier -fno-exceptions flag was used by default.\nBut after cvsuping KDE2 a couple of weeks ago I found that it's not the case anymore, and had to modify CXXFLAGS :-/"
    author: "fura"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-29
    body: "Oh my god, two weeks! I will count the seconds."
    author: "Ingo"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-29
    body: "Contrary to what most people are saying, the lastest KDE betas are awesome! I've been using Beta5 as my exclusive desktop for some time, and it, on it's own, has never crashed. Konqueror has never crashed (and I love it to pieces), and everything else is just awesome.\nJust one thing though. Gnome, by default can detect KDE menus, and displays them as such. Perhaps the KDE team should do something similar. It would be a rather noble thing, and I know many users would welcome such an addition."
    author: "MichaelM"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-29
    body: "I have been using KDE from 1.91 as my primary desktop and I am also using 1.94 (beta 5) exclusively now, but I cannot agree with you on konqueror's stability.\nDon't get me wrong: I love it as well and I will keep using it, but it does crash sometimes nevertheless.\n\nI just hope that all bugs are fixed before 2.0 is out. I have no time to try the CVS builds, so I don't know KDE2's current status. I can only hope that KDE is more near to a 2.0 release as 1.94 suggests."
    author: "Martijn Klingens"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-09-30
    body: "Just add the location of your GNOME dir-tree to KDEDIRS. E.g. something like \n\nKDEDIRS=/opt/kde2.0:/opt/gnome\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-10-01
    body: "<B>Now I'm flaberghasted.</B>\n<BR><BR>\nI've been using beta 4 here and there, and am having trouble setting up beta 5 on my SuSE machine, but beta 4 was terribly unstable! \n<BR>Also, for i18n support, I'm working with the Icelandic translation team, and I don't think we've translated enough yet - Ok, I for one have been lazy, but we had a special meeting for the occasion two weeks ago. The status of our translations is available at <A href=\"http://www.logi.org/kde2/stada.html\">logi.org/kde2/stada.html</A>..\n\nI don't know wheter the KDE team should have one more beta before the big day or not...\n\nWhich distro will be the first to release KDE 2.0 stable in their distro?"
    author: "Sm\u00e1ri P. McCarthy"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-10-02
    body: "I tried to install KDE2 beta whatever under Mandrake 7.1, it didn't work.  When KDE 1.x came out when I made the switch from winblows to linux, and just loved KDE.  It had a sytle similar to windows and made my linux learning easier.  After a little while a small birdie landed on my shoulders and whispered, \"hey try Gnome\".  So I did, and I thought \"this is how Linux should be.  Now I am using Helix, and it works very well, just some small problems with gmc.  So everyone should use Gnome!  Don't get me wrong, KDE is great, but I am a programmer coming from WinNT, and I think GTK+ blows away QT.  Maybe KDE and Gnome should merge.  Keep the best parts of each desktop and use GTK+.\n\nMy $0.02!"
    author: "Jim"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-10-02
    body: "> So everyone should use Gnome! <BR><BR>\n\nOne of wonderful things about Linux, is that we can all use the enviroment we want to. My own system, has KDE2.0 cvs version from Sept. 29, and is nice and stable for me, on RedHat 6.2.<BR>\nAnd for the record, I do have the latest Helix GNOME installed as well, for GIMP, and XMMS, etc.. etc.. if you want to develop a program for GNOME, go ahead, I'll still be able to compile and run it on my system, but from within KDE2, which will quite nicely, force it to use my choice of Desktop colors, and fonts automagically ;-)"
    author: "Eddy"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-10-02
    body: "I've just been Konquered!!\nKDE 2 Final Beta is working very well on my SuSE 6.4 and now I look forward to October 16th!\nI hope the last bugs especially in Konqueror will be fixed so I will be able to replace Netscape for ever!!!!\n\nThanx to all KDE developers to create my favourite desktop environment!\n\nBe enlightened!\n\n(Please apologize, of course I mean Be Konquered!)"
    author: "Sebastian Wolf"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-10-03
    body: "It's 19:24 (US Central), and where is RC1? I was looking forward to that today..."
    author: "Nobody"
  - subject: "Re: KDE2 release schedule update [going too fast?]"
    date: 2000-10-03
    body: "Maybe wait another week?\n\nIt still seems really unstable to me still (my desktop keeps \"crashing\"). But maybe cleanly built total new binary installs will work better.\n\nI assume the packagers will be looking for bugs while they prepare the rpm's tar.gz's and of course now the *.deb's !!!!"
    author: "AC Of Course"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-10-03
    body: "Maybe the webmaster of developer.kde.org can update the release-schedule on that site? I've been looking on that one for some time and it doesn't change, it's still somewhere in Juli...someone here who can update it?\n\nJonathan Brugge\n\nP.S.: I've been running the beta-versions of KDE2 for some time now, and it's quite stable. One thing I would like to see is more support of Konqueror: I can't load some pages because it has no support for the protocols the page uses (like https). If it can open a page, I prefer it above the always crashing Netscape."
    author: "Jonathan Brugge"
  - subject: "Great, but ...."
    date: 2000-10-04
    body: "but how do i have to wait for Redhat 6.x/7\nbinary packages ??? A year or two ???\nUp to now, there is no complete set of 1.94 \npackages available for the Redhat distributions."
    author: "Christian Groove"
  - subject: "Re: KDE2 release schedule update"
    date: 2000-10-31
    body: "Wow! ... Wow!\n\nNeat! ...\n\nCool! ...\n\nRad!"
    author: "Morris"
---
The KOffice hack session is over, and Matthias Elter is back with a new KDE2 <a href="http://lists.kde.org/?l=kde-core-devel&m=96996372328348&w=2">release schedule</a>.  While some of the details are still being discussed (hotly), in essence, RC1 will be released on Monday October 2, and the final tarballs will be created on Monday October 9.  The public release will follow on Monday October 16. Note that the next KDE <b>will</b> be named KDE 2.0, contrary to what the announcement says. Woo!