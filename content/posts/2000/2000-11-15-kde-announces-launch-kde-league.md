---
title: "KDE Announces Launch of the KDE League"
date:    2000-11-15
authors:
  - "Dre"
slug:    kde-announces-launch-kde-league
comments:
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "This is very clever, a much better idea than that foundation."
    author: "reihal"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "I was wondering if you could elaborate on how it is better?  It is easy to say things but to support it is another!"
    author: "Secret_danger_man"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "Yeah right, there is actually no difference to the gnome foundation.\n\nAK"
    author: "Albert Knox"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "Bazaar good, committe bad."
    author: "reihal"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "You need to do better than that you crackhead!  Gnome is still under a bazaar style of development, it just a has a steering committe.  The steering committee most likely even a good idea because decisions have to get done somewhere.  I is really quite funny you are talking about developement models because KDE is actually closer to the catherdal than bazaar!  Gnome releases early and often but when how long was kde1.1.2 and kde2.0?  You get the idea, a steering committe has little, if any, affect on the development model.\nYou are welcome to try again, but be warned I am loaded and a crack shot."
    author: "Secret_danger_man"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "<P>I personally don't like the corporate control that the KDE League has given in to. One of the great things about the GNOME Foundation is that companies can't be members of the Foundation. Foundation membership is only open to contributors (ie: Hackers, Documentors, Translators and Artists). The board consists of members and is elected by members. Corporate interests are relegated to an advisory board where they can give advice and advocate their point of view - but have no direct control over anything - neither technical direction nor PR. \n<P>The KDE League model seems to give the member corporations all the control and power in determining the PR and advocacy direction. I hope that taking the control of the public face of KDE away from the hackers that built it won't harm the project too much in the long term."
    author: "Yakk"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "I expect that this would cause lots of troubles at Slashdot.\nI we will get lots of flames in a short time, just like when the Gnome Foundation was announced.\n\nBut what does this differ from the Gnome Foundation?\nBoth are non-profit organizations (altough some ignorant Slashdotters claim that they aren't)."
    author: "Anonymous"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "There is a hug difference between the two groups.  The Gnome Foundation is a steering committee that controls the direction of GNOME.  The KDE League is simply a single source PR organization (essentially) to promote KDE itself, there is absolutely no control over KDE development."
    author: "Shawn Gordon"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "Hello, Shawn. Even though it's still very early, could you give us a *hint* of what exciting things theKompany plans to bring to the table in regards to the KDE League? I know I for one am always wondering what next you are up to ;-).\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "Our plan is to lend advice from the perspective of a small software company and what the League might do to help promote the KDE mindshare.  I have a different perspective than many of the members, simply because we are smaller and younger.  The meeting we had the other day to discuss it was very positive, I was quite impressed with all the members and there very good ideas and suggestions. \n\nI really think this is a good thing and I'm proud to be a founding member."
    author: "Shawn Gordon"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "People keep saying this and it's blatantly wrong.\n\nThe GNOME Foundation is run by a board of directors which is elected by the members of the GNOME community.  Elections just ended and we had our first board meeting yesterday.\n\nThe corporations who are supporting GNOME are on an Advisory Board which has no decision-making authority whatsoever.  \n\nSo the GNOME Foundation is NOT run by the companies on the Advisory Board.\n\nIf you go to http://foundation.gnome.org, you can read more about this.  \n\nLet me know if I can further clarify this.\n\nCongratulations on the launch of the KDE League."
    author: "bart decrem"
  - subject: "Really?"
    date: 2000-11-15
    body: "The term adviser reminds me of the cold-war term \"adviser\", as in such-and-such third-world country had US/Soviet advisors.  \n\nIn reality is that the corporate advisors can and probably will influence the decision making processes due primarly to the influence that these\norganizations can exert.  Pressure could include threatening to withdraw unless a certain agenda is followed."
    author: "bruce"
  - subject: "Re: Really?"
    date: 2000-11-15
    body: "And why should there be pressure?\nIt's not like that the companies can force the foundation to do something."
    author: "Anonymous"
  - subject: "Secret message from the CIA"
    date: 2000-11-15
    body: "I strongly _advise_ you you keep your opinions to yourself, Bruskey!  <g>"
    author: "Chris Bordeman"
  - subject: "Re: Secret message from the CIA"
    date: 2006-01-05
    body: "comment fait on pour rentre\u00e9 a la CIA\n"
    author: "hauguel julien"
  - subject: "Re: Really?"
    date: 2000-11-15
    body: "Same thing could happen on the League:\n\"If you don't do as we tell you, you won't get any publicity...\""
    author: "Anonymous"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "Gnome have kommisars, KDE have bazaars."
    author: "reihal"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "You KDE guys sure know how to write good marketings articles ;-)\nMaybe Gnome should write their articles the same way, then there will be less flames from Slashdot."
    author: "Anonymous"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "Sadly, this is not the case - we've been very poor on the marketing side of things historically, despite our technical achievements. The league should help a great deal in this respect by allowing the developers to get on with coding, rather than having to waste time writing press releases/brochures etc. Keeping the league out of the development process itself means that we avoid the issue of creating a centralised authority which is something that would be totally against the KDE philosophy.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "I don't understand.\nKDE's marketing seems to be one of the best out there.\nLook at all those KDE users/zealots!\nNearly all computer magazines who writes about Linux thinks that KDE is the only desktop for it.\nMost distros install KDE as default desktop.\nHow can you say that the marketing is poor?"
    author: "Anonymous"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "The marketing is poor, it's true. The fact that alot of developers/magazines have nearly all their screen shots from KDE is due to KDE's stability. Gnome code has been buggy from the beginning (either that or Enlightenment, the old default WM, was full of bugs) and was quite unstable. Things have changed for Gnome since it began using Sawfish as it's admittedly excellent WM, but old time KDE users have even more to love about KDE2. In the Linux world, unlike the Windoze world, people like things for stability and functionality rather than PR. But I admit that KDE is in need of good PR. Who isn't?"
    author: "Afrosheen"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "The marketing from magazines was poor cause mostly they show the ugly default look. If they compared kde (1.x) with Windows they always said kde has _four_ virtual screens..."
    author: "Dirk Manske"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "Doesn't it?"
    author: "Anonymous"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "No. Using default settings kde have four screens. It is configurable (2 to 8)."
    author: "Dirk Manske"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "<P>\nSpeaking solely on my own behalf, I want to point out that there is a bigger picture involved here.  The way I see it the KDE League is not set up to compete with GNOME; instead the KDE League is set up to promote KDE as <EM>a</EM> desktop standard.  Let's not forget that the Open Sources desktops are still hovering around 5% of the PC desktop market, with an even lower market share on mobile devices.  The goal is to tap in on the 95%, not to try to gain more market share of the 5%.  Bear in mind that many of the KDE League members are also members of the GNOME Foundation. \n</P><P>\nI personally hope the two projects can coexist in harmony and provide all users of PCs, workstations and mobile devices excellent Open Source alternatives to the proprietary systems that currently dominate the PC and mobile device markets.\n</P>\n<P>\nGroups like the KDE League will hopefully go a long way to help Open Source projects compete for the mindshare of all computing users against the marketing muscle that the proprietary software vendors can muster.\n</P>"
    author: "Dre"
  - subject: "Remember this"
    date: 2000-11-15
    body: "Now we have been asked \"Will KDE ever create a KDE Foundation in the same sense as the GNOME Foundation?\" The answer to this is no, absolutely not. KDE has always been and always will be controlled by the developers that work on it and are willing to do the code. We will resist any and all attempts to change this\n\nIt is quite a semantical difference here.  I see very little difference between the above statement and what is really going on, despite the claims otherwise.\n\nFace it,  you guys got caught in a pissing match with the GNOME people, and now it is too late to get out of the \"stream\",\n\nI love KDE, but fail to see the need for this given its position as the leading desktop envrionment.  I think both groups should go sit in the corner for a while and think about what they have done."
    author: "Confused"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "The League has been in discussion for something like 16 months actually, and KDE 2 was more the motivation than anything else.  The two groups are vastly different in their mission.\n\nIf you sit back and look at how KDE is promoted, you will find there is no single source, to the outside world it is a very nebulous thing to try and get your hands around.  A single press contact, and single source to support trade shows and promotional materials and things is a very good thing, and that is mostly what we are talking about here."
    author: "Shawn Gordon"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "I for one don't agree with you in this, there is from what I understand a big difference between what was stated in the above press release and what I have heard about the Gnome Foundation.\n\nAs I see it, the Gnome Foundation is for the companies so that they can change/twist ;) Gnome to suit them, with KDE League it will, according to how I interpret the press release, be /for/ the users.\n\nThe reason I think the League will work /for/ the users is that by having a well oiled \"PR machine\" without control of the development, more applications will be ported to the platform without the developers having to change the codebase to suit any one company. Offcourse it would be stupid to refuse to even consider changes suggested to the League, it might end up giving us users a much better environment to work in (if that is possible)\n\nAs for \"pissing match\", I think KDE did fairly well before the League was created, just think of the possibilities with an active PR organization.\n\nWhen it comes to need for this kind of organization, well, maybe KDE doesn't /need/ it in the near future, but I can see a clear need in the future. Without the porting of commercial grade (not nescesary commercial) applications to KDE, in my nightmares KDE becomes an environment for like Anonymous called them in the article 'Re: KDE Announces Launch of the KDE League' zealots or worse and all the rest choose Gnome or some other environment out of nescesity to be able to integrate some functionality in thier working environment.\n\nWell...this was just my $0.2.\n\n// Fredrik"
    author: "Fredrik Larsson"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "Hi Fredrik,\n\nTo repeat: People keep saying this and it's blatantly wrong.  The GNOME Foundation is run by a board of directors which is elected by the members of the GNOME community. Elections just ended and we had our first board meeting yesterday.  The corporations who are supporting GNOME are on an Advisory Board which has no decision-making authority whatsoever.  So the GNOME Foundation is NOT run by the companies on the Advisory Board. Thus, as far as I can tell, the GNOME Foundation's Advisory Board and the KDE League are almost identical in function. More info about the GNOME Foundation is at http://foundation.gnome.org. Let me know if I can further clarify this.\n\nI think I speak on behalf of the GNOME Foundation's Board of Director in congratulating the KDE community on the launch of the KDE League.  I think it's a positive development that both projects are establishing venues for dialogue with the companies whose support will be integral in bringing a free desktop to the mainstream.\n\nAlso, I for one am excited to see that a number of companies participate in both the GNOME Foundation's Advisory Board and on the KDE League.  I hope these cross-memberships will encourage greater collaboration between the two projects."
    author: "bart decrem"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "Finally, somebody who knows what's really going on.\nYou heard that, FUD distributors (no I don't mean you)?"
    author: "Anonymous"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "OK, I've read this over so now I'm better educated :).  You're right in terms of the similarity, they both say they will act as PR and media contacts, however that is where the League stops.  The Foundation, according to the web site \"the Foundation will coordinate releases of GNOME and determine which projects are part of GNOME.\"\n\nThat is the critical distinction that I think I, and others are making, it doesn't have to do with the corporate sponsorship.  That said, hopefully this can also act as a unified conduit for communcation between the two groups to work together."
    author: "Shawn Gordon"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "Hi Shawn,\nYes, the GNOME Foundation does release coordination and other functions that, I understand, are handled by KDE Core in the KDE project.  But those functions are handled by the Board of Directors, which was elected by the hackers.\n\nThe Advisory Board is just like the KDE League (as far as I know)."
    author: "bart decrem"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "yes, that's right. i nice democratically voted in Board of Directors that has nothing to do with the companies that are part of the foundation.\n\nNOT.\n\n4 from Eazel, 2 from Helix, 2 from Red-Hat and one from Sun. leaving <B>2</B> that aren't from companies in the foundation and <B>0</B> unaligned members.\n\nGet real and see through the smoke."
    author: "Aaron J. Seigo"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "Oups, how many members of the KDE Core Team aren't \nemployed by companies in the league ?"
    author: "Anonymous"
  - subject: "Re: Remember this"
    date: 2000-11-17
    body: "That's another question; at KDE even \"employed\" programmers have to discuss their ideas with _everybody_ (including interested hackers that do not really participate in the project), while in the GNOME foundation, the Directors can make the decicions as it pleases them (or their employers?).\nIf it happens that nearly every Director has a job at a member-concern, they can (and as nobody can stop them, I'm sure they will) act not only like individual GNOME programmers, but also the way it fits the needs of the companies."
    author: "CP"
  - subject: "Re: Remember this"
    date: 2000-11-16
    body: "In case you don't know, those are individual persons too.\nThey are humans and enjoy programming."
    author: "Anonymous"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "Looking at who was elected, there is indeed a lot of corporate involvement in who now steers GNOME!  This is very different from before!"
    author: "ac"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "Well, the core developers of both KDE and Gnome tend to be hired by corporations, so it's really pretty inevitable."
    author: "ac"
  - subject: "Re: Remember this"
    date: 2000-11-16
    body: "*rofl*\n\nYou are indeed a funny man.\n\nThe Board of directors is pretty much exactly who was steering GNOME before. We have ratified this through a democratic process. I'm sure the KDE group have a similar process for electing the KDE Core."
    author: "Yakk"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "Hi Shawn,\nYes, the GNOME Foundation does release coordination and other functions that, I understand, are handled by KDE Core in the KDE project.  But those functions are handled by the Board of Directors, which was elected by the hackers.\n\nThe Advisory Board is just like the KDE League (as far as I know)."
    author: "bart decrem"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "Thanks for clarifying things for me.\n\nAs I said in the post, it was based on how I have percieved the Foundation from the little I have heard of it.\n\nI was not out to shake a fist at anyone but to explain that from my point of view there was a big difference between the Foundation and the League.\n\nAnyways, my point (even if I failed to get it through) is that an active PR organization is good for KDE no matter who is running it as long as it stays just that, a PR organization.\nSeeing that a few huge companies are willing to spend time and money on helping and supporting a non-profit development project will hopefully tip the scales for those small/mid and large sized companies who is deciding if they should stick with just Windows or port thier product to Linux.\n\nIt isn't like it is a competition between Gnome and KDE, we have like 90% or so market shares to Konquer from Gates if we have to compete with anyone.\n\nIf anyone took offense at my post, I am sorry.\n\n// Fredrik"
    author: "Fredrik Larsson"
  - subject: "Re: Remember this"
    date: 2000-11-15
    body: "Hi Fredrik,\n\nNo offense.  It's just that I too have seen many stories misrepresenting what the GNOME Foundation is.  In a way it's understandable given the hooply that was created by Sun etc.  But since I've done a lot of work coordinating the Foundation, and I've spent a lot of time working to keep control with the hackers, I wanted to set the record straight.\n\nI agree that we need to find more ways to work together and focus on converting the rest of the world to free desktops.\n\nAlso, I figure this is as good a place as any to start clearing up the miscommunications between the two projects so we can better work together in the future.\n\nBart"
    author: "bart decrem"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "IBM ?\n\nDoes it mean, that we'll be able to get AIX binaries of KDE2? GNU tools sucks on AIX, and KDE is a pain to compile on AIX :-/"
    author: "fura"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "Will Red Hat be a member of the league?"
    author: "SelectSpec"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "both RedHat and VA are still considering joining from what was discussed at the meeting."
    author: "Shawn Gordon"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "Redhat, as always, has its own agenda.  Remember is is RH who was very instremental in promoting GNOME (the other desktop) and even did not supply KDE (back in 5.x series, I think).  I would imagine that them not being part of KL is probably more of the same political bullshit.  They can't use the excuse of \"bad license\" anymore so lets see some (real) reasons."
    author: "bruce"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "I'm pretty sure RedHat 5.2 had it...  'Cause that's the first Linux distro I *really* used...  But it was hidden...  And I didn't find it untill *after* I had already downloaded and installed it..."
    author: "Nick Robbins"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "Red Hat always had a strange attitude towards KDE. Remember that they shipped it in there offical german version, while they still claimed doing this was illegal on their web site? These really were the days..\n\nWhile I'd like to see Red Hat joining, one has to notice that they don't do much on the desktop anymore. There desktop has pretty low priority and is poorly maintained (compared to for example Mandrake, S.u.S.E. or Caldera). They are clearly aiming at the server market only.\n\nAccording to what Miguel de Icaza ones posted about the early days of Gnome, it was Red Hat that motivated him to start the project by promising commercial backing. Remember Red Hat Labs? They did a big part of the early Gnome work and now they are maintaining Gtk. When they started doing this back then, it was seen as THE BIG THING. They had more developers on following up KDE than Trolltech had for Qt development at this time.\n\nJust imagine what they could have achived using the right tools rather than duplicating efforts.\n\nSometimes I think it might be interesting for KDE to do a small distribution poll among the developers. At Trolltech, we are using a polical correct mixture of Caldera, S.u.S.E., Mandrake, and Debian. And I believe we have a few Corel machines as well."
    author: "Matthias Ettrich"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "Speaking of politically correct, don't forget Bradley T. Hughes and his FreeBSD.  ;-)\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "It doesn't do much good further alienating RedHat. Remember how things were with Kde and Debian a few months ago?  All that is different now.  It can also be different with RedHat.\n\nI'm using RedHat 6.2 right now as a Kde developer simply because when I messed up my partition table removing MS Windows from my machine, the fastest way to reinstall Linux was to run down to Best Buy and pick up a boxed set of RedHat.  The support for Kde is not good.  I had to uninstall all the RedHat Kde stuff which is was /usr and rebuild everythig from source to be able to switch Kde environments between stable and development versions.  There were other problems with kppp. However, it was all worth it to get rid of Windows forever, and everything now is set up about like a Suse or Caldera box, with RedHat.  For end users, I can imagine the difficulties especially with RedHat 7.\n\nRedHat seems to be even more interested in the embedded and internet appliance market than in servers.  Perhaps Trolltech's inroads into embedded systems will provide an incentive to RedHat open its doors a little more to Kde as well, if Qt really takes off in embedded devices and appliances.  \n\nIf I can learn to be diplomatic certainly you can as well.  Your continuing hostility over what properly belongs in the past is unnecessary.  Please think about it, especially considering your position of influence in Kde circles.\n\nJohn"
    author: "John Califf"
  - subject: "Poor little redhat - sorry redhat, really!"
    date: 2000-11-16
    body: "What, might I hurt their feelings?  Quite frankly I don't give a fig for them.  I used to buy RH until 5.2 and then the whole hypocracy sickened me.  They didn't have any problem shipping closed source apps like bru, xing (some xserver) but qt was a real problem to them.  Bullshit - they saw that they needed control of a very important part of the UI and with KDE - they were in the same boat as the rest of the distros.  So GNOME was pushed and the KDE fud was spread.\n\nThey are becoming irrelevant."
    author: "bruce"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "Poor you, they didn't want to back your project.\nNo wondering you're bitter..."
    author: "Anonymous"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "There's a huge difference between not backing up a free software effort and actively working against a free software effort. I'm not talking about supporting a competitive effort, I'm talking about approaching commercial or free software developers and feeding them with missinformation.  They failed in many cases, though, where the engineers won the battle and decided to go with the technically more appealing solution. Corel is a prime example for this.\n\nI'm not bitter at all. If Red Hat said clearly \"we don't see how we can make money with Qt, but we see a great business opportunity to push and lead Gtk development\", all would have been fine. Open cards, you know. I wish they wouldn't have advertised their powertools product with a huge screenshot of my LyX software at this time - it had exactly the same \"licensing problems\" in their point of view.\n\nSeriously, I'm much more dissappointed that they hired some excellent developers from the KDE team and don't give those enough time to work on their respective KDE projects. \n\nBtw: Next time you post and insult, put your name in there."
    author: "Matthias Ettrich"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "It is actually a major problem for some of us. I am chairman of the Math. Department in Binghamton. We are running around 30 desktops with Redhat and Kde, mostly Dell machines. I chose Redhat because it is dominant around here, and Kde because it is stable. We recently installed the Redhat rpms for Kde2 on Redhat7. These rpms do not contain any documentation files. They contain the uncompiled index.docbook files, so practically no help function is working.\n(Only the programs that have not been updated to docbook format). They contain the translations for languages starting from a to h. Apparently they crashed on Hebrew. Redhat 7 broke wordperfect 8 which is absolutely essential to us until kword is a little more finished. We found out solving the problem by installing a couple of libraries from Redhat 6.2. I told Redhat they ought to post that on their website and got an incredibly snooty answer back, saying that Wordperfect was Corels responsibility not theirs. Having the upgrade break the programs of the competition is something I seem to have heard of before :). The fact that we get a snooty answer to telling them about the solution to a problem, I find a bit too much. They decided to stop Solaris support without any warning, which means we have about 8 machines we cannot keep uptodate. They are pretty old, so we can live with that, but the fact that they have not produced decent Kde2 rpms is very irritating. I realize we only send about $300 pr year to Redhat (and less in the future because Solaris is not supported), so we can not demand too much, but at least I can confirm that they have a strange attitude to kde. I really long for the days when the Redhat rpms for kde were made by the kde developers.\n\nErik Kj\u00e6r Pedersen\nchair, Dept. Of Math. Sci.\nBinghamton University"
    author: "Erik Kj\u00e6r Pedersen"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "The fact that the initial RPMs didn't have docs were caused by the fact that the released version of kdoc 2.0 couldn't process the kdelibs docs.\n\nThis has been fixed in the later builds, available from ftp://ftp.linux-easy.com/pub/kde/ and ftp.kde.org.\nIf you notice any other bugs, please report them - either directly to me or through bugzilla (http://bugzilla.redhat.com/bugzilla/).\nI don't remember seeing a bug report about this from you.\n\nAs for breaking WordPerfect, it's offtopic here, but it was actually announced in the release notes that we're discontinuing support for libc 5.x packages. libc 5.x has been obsolete for about 3 years now, definitely enough time for anyone to update his applications.\nSorry about the snooty answers you got - some people around here tend to get aggressive when they're getting a couple of \"bug reports\" reporting an announced \"feature\". The tone may not have been right, but the content probably was.\n(May I ask where you sent the message?)"
    author: "Bero"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "Kde always had a strange attitude towards Free Software. Remember when they shipped kde when Qt was licensed under a cr^H^Hpropietary license ?\nThese really were the days.."
    author: "ac"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "The Qt 1.x license (not 2.x, not QPL) was actually a bit of a problem, because it didn't allow the code to be modified.\n\nI personally think that gtk's API is a pain, and therefore it wasn't the right way to go, but the idea of a really free desktop was right. If I had been in control, I would have pushed a really free Qt workalike to run KDE with.\nI guess some people's dislike for C++ was the primary reason they decided to do something completely different.\n\nAs for RH joining the League, I'm trying to convince those in control to do just that, but I have no idea whether or not I'll have any success.\nIf you have any good arguments I should pass along, feel free to let me know. ;)"
    author: "Bero"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "<i>If I had been in control, I would have pushed a really free Qt workalike to run KDE with.</i>\n<br><br>\nand did you remember when main kde coders say that they are _not_ interested in a free Qt workalike when asked if they would switch to it?\n<br>\nso, IMHO, your pushing at that time would have been a waste of time, I think ...\n<br>\nThese really were the days.."
    author: "ac"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "Nope, they didn't include it in RH 5.2.  That in fact was the reason Mandrake started.  It was originally RH + KDE.  With KDE being the #1 desktop, and the competitive pressure of Mandrake becoming popular, RH included KDE in 6.0.  On 5.2 you DLed KDE from kde.org and installed it yourself like everybody else.  We sometimes forget how much we had to piece together in RH5.2 to get a usable desktop, with everything being included in nice distros like Mandrake these days."
    author: "Rh -> Mandrake"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "Ahh yes the good old days ... we did it uphill, in the winter, in our father's pyjamas and we LOVED it! I tell yah weee LOOOVVED it sonny!"
    author: "couard anonyme"
  - subject: "www.kdeleague.org"
    date: 2000-11-15
    body: "Fine, the KDE League is for promoting KDE. That being the case, I frankly think it's a shame that they don't even have a presence on the web:\n\nhttp://www.kdeleague.org says:\n\n\"www.kdeleague.org\n has not set up their website yet\"\n\nPerhaps it has something to do with the DNS records not having propagated all the way to Norway yet, but in any case it's kind of a bad start, promotion-wise ;)"
    author: "Haakon Nilsen"
  - subject: "Re: www.kdeleague.org"
    date: 2000-11-16
    body: "Hmmm, works fine for me in Seattle, WA  USA at 4:22p Pacific Standard (GMT -0700, I think?)\n\nCheers,\nMCT"
    author: "MCT"
  - subject: "Re: www.kdeleague.org"
    date: 2000-11-16
    body: "Works for me too. What would be real cool is if all those vendor logos were image maps that took you to the vendor web site."
    author: "Paul C. Leopardi"
  - subject: "Re: www.kdeleague.org"
    date: 2000-11-16
    body: "you just have to go to the section \"Current members\" and click the links to their main web site."
    author: "renaud Michel"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "from foundation.gnome.org\n\n/*\nThe GNOME project has built a complete free and easy-to-use desktop environment for the user, as well as a powerful\napplication framework for the software developer. GNOME is part of the GNU project, and is free software (some times\nreferred to as open source software.) \n\nThe GNOME Foundation will work to further the goals of the GNOME project.\n\nTo achieve these goals, the Foundation will coordinate releases of GNOME and determine which projects are part of\nGNOME. The Foundation will act as an official voice for the GNOME project, providing a means of communication with\nthe press and with commercial and noncommercial organizations interested in GNOME software. The foundation may\nproduce educational materials and documentation to help the public learn about GNOME software. In addition, it may\nsponsor GNOME-related technical conferences, represent GNOME at relevant conferences sponsored by others, help\ncreate technical standards for the project and promote the use and development of GNOME software.\n*/\n \nThe only real difference I see is that, yes, the GNOME Foundation will determine which projects are \"part of\" GNOME, but frankly you can still build apps on the core libraries without being recognized by the GNOME foundation."
    author: "Shane Simmons"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-15
    body: "from foundation.gnome.org\n\n/*\nThe GNOME project has built a complete free and easy-to-use desktop environment for the user, as well as a powerful\napplication framework for the software developer. GNOME is part of the GNU project, and is free software (some times\nreferred to as open source software.) \n\nThe GNOME Foundation will work to further the goals of the GNOME project.\n\nTo achieve these goals, the Foundation will coordinate releases of GNOME and determine which projects are part of\nGNOME. The Foundation will act as an official voice for the GNOME project, providing a means of communication with\nthe press and with commercial and noncommercial organizations interested in GNOME software. The foundation may\nproduce educational materials and documentation to help the public learn about GNOME software. In addition, it may\nsponsor GNOME-related technical conferences, represent GNOME at relevant conferences sponsored by others, help\ncreate technical standards for the project and promote the use and development of GNOME software.\n*/\n \nThe only real difference I see is that, yes, the GNOME Foundation will determine which projects are \"part of\" GNOME, but frankly you can still build apps on the core libraries without being recognized by the GNOME foundation."
    author: "Shane Simmons"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "The real, practical difference is that only contributing individuals (hackers, documentors, artists, translators, etc) can be members of the GNOME Foundation, whereas only companies can be members of the KDE League. Compare the membership list on the KDE League <A HREF=\"http://www.kdeleague.org/members.html\">site</A> to the membership list on the GNOME Foundation <A HREF=\"http://www.gnomefoundation.org/membership.html\">site</A>."
    author: "Yakk"
  - subject: "Is Kde League Hiring?"
    date: 2000-11-15
    body: "Does the Kde League need anyone to answer the telephone, stuff envelopes or maintain its web site?  If so, please sent one of your employment forms to the above email address.  You are an equal opportunity employer, I hope."
    author: "John Califf"
  - subject: "Re: Is Kde League Hiring?"
    date: 2000-11-15
    body: "Oops! Flunked your employment test already with a spelling error.  Should be \"send\" instead of \"sent\"."
    author: "John Califf"
  - subject: "Re: Is Kde League Hiring?"
    date: 2000-11-15
    body: "Final comment.  Before anyone asks, I've already tried the Gnome Foundation.  Beggars can't be choosers.  But they say I don't meet their height requirement. 6'- 2\" here.  They are looking for 3'- 6\" maximum."
    author: "John Califf"
  - subject: "Re: Is Kde League Hiring?"
    date: 2000-11-15
    body: "<p>Ok, while this is funny I believe the constructive ideas can be used. Maybe the league can start a jobs-board. Where any company can look for experienced C++/QT/KDE programmers.\n\n<p>Why don't you write to them with a nice proposal ;)\n\n<p>Good luck in your quest.."
    author: "Thomas Zander"
  - subject: "Re: Is Kde League Hiring?"
    date: 2000-11-15
    body: "I believe, because of TrollTech influence, that KDE League has a maximum height limit of 4'2\", John. Perhaps, at 6'2\", you would be better off applying for a job as a roadie with They Might Be Giants."
    author: "Robin Miller"
  - subject: "Re: Is Kde League Hiring?"
    date: 2000-11-15
    body: "I don't know, the 3 people I met from TrollTech at the show were all taller than me :)."
    author: "Shawn Gordon"
  - subject: "POSTER BOY!!!"
    date: 2000-11-15
    body: "So where is the glam boy?!\n\nMake sure the glam boy, the buoy, the Iguana, the Torvalds of KDE is there!\n\nAll the Glitzerati available must be there!!!\n\nTorbj\u00f6rn Skolde"
    author: "Torbjorn Skjolde"
  - subject: "Re: POSTER BOY!!!"
    date: 2000-11-16
    body: "Ok. someone has to post the question.\n<p>\nWho would that poster boy be?"
    author: "Rijka Wam"
  - subject: "Great Work as usual..."
    date: 2000-11-15
    body: "This is indeed a good sign for the user community as a whole, since, many of the user interface issues haven't been given much priority, like you couldn't use KDE (and all its applications) with using the mouse as in Windows. And some people thought that KDE won't live longer due to GNOME Foundation. This is a very intelligent step... Thanks a lot."
    author: "Rizwaan"
  - subject: "Re: Great Work as usual..."
    date: 2000-11-15
    body: "sorry, it was (intended to be) \" like you couldn't use KDE (and all its applications) WITHOUT using the mouse as in Windows\" intended."
    author: "Rizwaan"
  - subject: "Membership"
    date: 2000-11-16
    body: "<P>Why isn't membership of the KDE League open to hackers? Is it wise to give all the power over PR and marketing to a bunch of companies who probably have vested interests in pushing their own proprietary alternatives too Free Software?\n<P>The GNOME Foundation model where membership is only open to actual contributors to the project would probably have been more constructive."
    author: "Yakk"
  - subject: "Re: Membership"
    date: 2000-11-16
    body: "<P><EM>Why isn't membership of the KDE League open to hackers</EM></P>\n<P>\nPlease note that according to <A HREF=\"http://www.kdeleague.org/bylaws.html#article_iii\">Section 3.1(c) of the bylaws</A> the developers have a number of Board seats, most of which are currently vacant, and hold half the voting power of the League.  To get a \"hacker\" seat talk to the KDE developers, they can appoint whomever they want.\n</P>"
    author: "Dre"
  - subject: "Re: Membership"
    date: 2000-11-17
    body: "I couldn't see where the election process for the KDE developer representatives was set out. How do ordinary developers participate in this process?"
    author: "Yakk"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "And thus a prestige and pissing match spurred on by industry interest begins another deep schism in the Unix desktop market.\n<p>\nAs I recall, a few months ago, the DEVELOPERS involved were talking about getting the two component systems (Bonobo and KParts) to interoperate, and get a unified basis for software out there.  Whoops, sorry, we've got to look the cleverest and best, so whatever guys, nice thoughts, we're doing our own thing.\n<p>\nBoth \"Foundations\" are at fault on this.  CORBA, the long forgotten standard, now has a Component Model that is an open standard, and with just a bit of support, could get the Unix desktops onto a more equal footing with the Windows platform for development capabilities, but apparently the two great environments we've got aren't particulary interested in the *most effective* solution.  Just a perceived *best* solution.  Based I believe largely on complexity and academic merit.  \n<p>\nIt's odd isn't it?  Commercial software works to make the product that will make the most money with the minimum effort, and that will lock you in over long term.  Free software looks to make the best possible software with today's technology, and make it free and open to seize the high ground morally, which subserving other ideas and opinions of what is \"good\" to their own egos.  I think we need a middle ground, where the \"best of breed\" comes back.  Take the best, most effective parts from all of them and find a way to roll them together.  Adopt, embrace, champion.  \n<p>\nDon't get me wrong, there are people and projects in the Free and Open Source camps who believe in this type of creedo.  I'm just not seeing it from the two most critical and successful products out there, being KDE and Gnome.  That's too bad because so much rides on the fortunes of those projects. \n<p>\nI've used, and still use, both.  I still believe that KDE has a great interface, and some excellent apps facilitated by a much clearer, easier to learn library and object set.  But I still believe the Gnome infrastructure would allow absolutely fantastic things and integration with exisiting legacy and new systems on unparalleled scales.  It suffers from a kitchen sink syndrome from what I can see, and it's harder for most developers to learn because of it.  \n<p>\nDisclaimer:  I haven't coded solutions in either, so I'm speaking based on the good and bad things I have experienced and had related to me from other developers and on more than a few news groups and chat systems.  I raise the point invectively to see if anyone gives a crap about making an effective replacement on the desktop or not!\n<p>\nTell me I'm wrong, tell me I'm right, add to or detract, but at least use your mind and think about it first.\n<p>"
    author: "Dallas Hockley"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "Ok, I'm telling you that you're wrong ;)\n\nI really don't know, why people always see development of KDE and GNOME as a pissing match. Certainly there are users, who take their personal preference too far and insult the other side and sometimes they are actually desktop developers. Not nice, but something I can understand, since we are human.\n\nHowever, I really don't think that has anything to do with it. People are people and given enough large crowd, you'll certainly get very different opinions. And that's what we have.\n\nI agree with you, that it would be nice if we could join forces and develop one desktop, but even though I do like some stuff that GNOME developers created, I generally dislike their development kit, don't agree with direction and way how they develop it. I'm quite certain at least some of them feel the same, so why should we all be forced to do work on something that we don't believe in?\n\nI don't see anything really significant that KDE could benefit from GNOME and judging by views of GNOME developers I met at OLS this year, many feel the opposite. This, too, is just fine.\n\nDifferent visions and personalities are also a reason, why there are so many different programs that basically do the same thing. I'm not bothered by that, I only wish people would reuse code more and that interoperability of desktops would be a bit better.\n\nI see GNOME foundation and KDE league as two organizations that can be quite helpful for all of us as long as they'll keep attacking each other and even though it's early to tell, I do believe they won't do that.\n\nIt's interesting to see differences between them that reflect the difference in developer's attitude. KDE leaves development to developers and PR to PR people (and I agree with this approach), where GNOME leaves basically all control with hackers. I don't believe they (in general) make good PR people, so I'm glad KDE didn't choose the same way of organizing things, but I certainly don't see a reason why GNOME shouldn't have it like they do.\n\nSo, I'm quite happy with how things are going."
    author: "Marko Samastur"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-20
    body: "Thanks!  :-)\n<p>\nThe pissing match is, as always, spurred on by the advocates, and not usually the core developers.  That's the typical computer jihad effect.  :-)\n<p>\nI fully appreciate the statement you make in differences of opinions, methods, approaches and toolkits.  As far as environment, that diversity can help and strengthen the community and the Linux/Unix/wherever it evolves to desktop as a whole.  \n<p>\nThe thing I'm more driving at is under the hood.  Borland, Symantec, Microsoft and dozens of others all write interoperable apps on the Windows platform, all using very, very different approaches in the development tools.  But a use never notices that difference.  A user can't usually tell if the MFC is trolling under the button, or if it's the VCL from Borlan/Inprise.  \n<p>\nThat's the important part.  Have the drag-and-drop interoperate between KDE and Gnome apps.  Have the clipboard play nice.  And have the window managers behave and interface somewhat reasonably and consistently with all the apps.  I'm advocating the user's experience in this, not the developer's methods.  \n<p>\nI'm a software developer myself, professionally for 10 years, and over time I've realized that the user is still why most people write software.  So someone can say, \"this is great, thanks!\" to our efforts, or maybe even pay us!  :-)\n<p>\nSo if we're having a disagreement in methods or approach or opinion, fine.  But don't air the dirty laundry!  :-)\n<p>\nIs there a solution to the under-the-hood level?  Or shall we just settle on a complete platform VHS/BETA war and have it out?  \n<p>"
    author: "Dallas Hockley"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-20
    body: "Well, window managers have a new standard adopted by GNOME and KDE (and possibly in other window managers as well). If window managers supports it, it should work well in KDE and GNOME. Kwin does and I'm sure there are others as well.\n\nDrag and drop should work. There might be bugs, but both use same protocol as far as I know.\n\nI'm not to sure about clipboard though, but at least text copying usually works alright. \n\nI'm thinking a lot about component technologies lately. I plan (if nothing comes in between, which regretfully often does) to look for some way of interoperability between Bonobo and Kparts/DCOP in the comming months, even though I'm a bit pesimist about it. If not for anything else, I'm not as good developer as I would like to be. But it seems to me that I won't get much rest before I at least take a shot at it.\n\nSo, basic things definitely work, more complex sadly don't. I don't know of anyone, who would be working on them, but I wouldn't be surprised of their existance."
    author: "Marko Samastur"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-21
    body: "Ah.  I knew about the fledgling window manager standard, but I didn't know that both \"sides\" had a compliant manager.  Great!\n<p>\nD&D support, OK, I'll take your word on it... :-)\n<p>\nActually, it's more the middle/right mouse click copy/paste I usually miss.  More sophisticated clipboard use probably tromps on your main point, which is the Bonobo/KParts thing.  I recall that the leads on both these projects were actually talking at one point.  Did anything ever come of that?  My guess from very limited knowledge was that if Bonobo evolved towards the Corba 3 CMM model and KParts with what I consider a clean API at a wrapper around it to mimic that behaviour, then you would have deep interoperability, at least in principle.  I know that the CMM standard and all the bits it relates too are by no means trivial, but the biggest step is agreeing on the API.  CORBA, although admittedly Gnome biased due to rooting, is an open standard that could be non-partisan for support baselines couldn't it?\n<p>\nJust out of curiosity, what is the root of your pessimism on the integration effort?"
    author: "Dallas Hockley"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-21
    body: "AFAIK the only thing that came out of talks was that merge won't happen (yet).\n\nThe reason for my pessimism is time and interest. Both projects now have more or less finished component technology (I believe Bonobo is not finished yet, but it's not missing much either) and there are more and more applications that use them. That in itself is great, but it makes change so much more difficult. And with time it will take it even more so, because there will (hopefully) be a huge amount of applications using them. \n\nBoth sides, especially developers really familiar with these technologies, really like their approach and are not willing to drop it. Since none of projects is a clear winner, neither side feels the pressing need to work on it. The pressing need feel those developers, that would like to develop \"native\" applications for both desktops. Development of a bridge of some sort will therefore have to come from them.\n\nAnd, afterall, why would they do it? Sure, they don't oppose the idea openly, but do they really like it? For example, if there was a bridge that would allow KDE programs use Bonobo components, would it benefit KDE or GNOME? My guess, remembering OS/2, would be it would benefit GNOME.\n\nSo, here you have probably a massive project, where people most qualified to do it don't have time and interest for it. As I've said, I plan to look into it, but knowing me I wouldn't expect much ;)\n\nBtw, CORBA is open, but AFAIK GNOME developers added some extensions to it. However, I have no idea if that changes anything."
    author: "Marko Samastur"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-21
    body: "Hmm.  Good points, and thanks for the insight!\n<p>\nGnome did extend the base model I believe.  And it wasn't based on CORBA 3 and the Component Model (CMM) it includes to sort of substitute for COM on the desktop.  I wonder....  \n<p>\nI don't know if many agree with me, but with a lot of the network apps coming about (distribute network, such as a gnapster/Jabber thing) there is a growth towards systems that may be benefitted by richer integration at an object level.  I figure it will go one of two ways.  Either the CORBA/COM+ extending will continue, or SOAP is going to come on really strong, depending on how the standard seeps out, given the support from both the MS camp and the Apache project right at the outset.  Maybe we could get both camps to talk SOAP, as that doesn't favour anyone in particular, and helps everyone including MS users in integrating the UNIX systems. \n<p>\nAnybody know if there's been work on SOAP integration into either desktop framework?\n<p>\nDallas"
    author: "Dallas Hockley"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "Ok, I'm telling you that you're wrong ;)\n\nI really don't know, why people always see development of KDE and GNOME as a pissing match. Certainly there are users, who take their personal preference too far and insult the other side and sometimes they are actually desktop developers. Not nice, but something I can understand, since we are human.\n\nHowever, I really don't think that has anything to do with it. People are people and given enough large crowd, you'll certainly get very different opinions. And that's what we have.\n\nI agree with you, that it would be nice if we could join forces and develop one desktop, but even though I do like some stuff that GNOME developers created, I generally dislike their development kit, don't agree with direction and way how they develop it. I'm quite certain at least some of them feel the same, so why should we all be forced to do work on something that we don't believe in?\n\nI don't see anything really significant that KDE could benefit from GNOME and judging by views of GNOME developers I met at OLS this year, many feel the opposite. This, too, is just fine.\n\nDifferent visions and personalities are also a reason, why there are so many different programs that basically do the same thing. I'm not bothered by that, I only wish people would reuse code more and that interoperability of desktops would be a bit better.\n\nI see GNOME foundation and KDE league as two organizations that can be quite helpful for all of us as long as they'll keep attacking each other and even though it's early to tell, I do believe they won't do that.\n\nIt's interesting to see differences between them that reflect the difference in developer's attitude. KDE leaves development to developers and PR to PR people (and I agree with this approach), where GNOME leaves basically all control with hackers. I don't believe they (in general) make good PR people, so I'm glad KDE didn't choose the same way of organizing things, but I certainly don't see a reason why GNOME shouldn't have it like they do.\n\nSo, I'm quite happy with how things are going."
    author: "Marko Samastur"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "In principle I can see nothing wrong with the KDE league... if they will be (besides promotion), for example, updating KDE websites, writing documentation and similar stuff that developers can't find time to do it. Wouldn't it be nice to have the best desktop, with great bunch of applications and a complete documentation??"
    author: "Bojan"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-16
    body: "Facts: Both the GNOME Advisory Board (not the Foundation), and the KDE League are composed of companies with an interest in guiding development. These companies <i>will</i> influence development to some extent, despite statements otherwise (else what's the point of joining?).\n<p>This is a necessary evil. We need the support of companies to achieve broader acceptance. Even within the existing Linux community. People who say \"we're just in this to develop cool software, we don't care about acceptance\" fail to \"get it.\" Part of cool software is having standards and seamless interoperability that only come from having a solid user and developer base. If every major distro is released with GNOME as the default, and every major corporation with an interest in open source is developing GNOME software, and GNOME manages to stay within even 6 months of KDE (in terms of maturity), then the simple fact is everyone will be using GNOME. And KDE won't be cool anymore because it won't be able to attract the number of developers and applications which make it cool now.\n<p>Personally, I'm glad KDE got a group to fight for it on the corporate field. I like both it and GNOME, believe it or not, and don't want to see KDE go under prematurely just because it lacked industry support. May the best desktop win!"
    author: "Adrian Kubala"
  - subject: "Re: KDE Announces Launch of the KDE League"
    date: 2000-11-19
    body: "Well, count me in.\n\nPromotional work doesn't come easy to full-time nerds, and I'm allways ready for a challange.\n\nThe thing is, that in places like these (Iceland), there are about 20 people that realy do the work on the KDE translations, and maybe two of them help out with development (someone submitted a fix to a memory leak in KFM a while back).\nThe people in the translation team have been working together to promote KDE, by sending letters to the government and dancing around in the media. I for one think that the whole idea of promoting KDE should be done in the following manner (because I belive there are others like us):\n\nEach translation team chooses a promotioal manager. Then a special group of people sit around a large table and decide on promotional strategys, and they then e-mail the strategies to some maling list. Then it is up to the Promotional managers of each team to get everybody to do something...\n\nMight work better than making a smaller group do a worldwide plot ;)"
    author: "Sm\u00e1ri P. McCarthy"
---
The KDE Team today announced its collaboration with industry leaders to
form the KDE League.  The League will focus on facilitating the promotion,
distribution and development of KDE, with the goal of establishing KDE
as a desktop standard for PCs, workstations and mobile devices.  The
League will <STRONG><EM>not</EM></STRONG> be involved in KDE development.
The League will be holding a press conference at 2:00 pm Las Vegas (PST)
time on Wednesday, November 15, 2000, in Room B in the media tent in the
silver lot of the Las Vegas Convention Center (LVCC).  This is great news for KDE -- finally the marketing support that will help people learn about the technical excellence of KDE, without any changes in the KDE development model!  The full press
release follows.

<!--break-->
<P>DATELINE NOVEMBER 15, 2000</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">Open Source Developers and Industry Leaders Unify
to Create the KDE League</H3>
<P><STRONG>League Will Promote KDE as a Desktop Standard</STRONG></P>
<P>November 15, 2000 (Las Vegas, Nevada).  Developers of the
<A HREF="http://www.kde.org/">K Desktop Environment (KDE)</A>, the easy
to use, Open Source desktop environment, today announced the formation
of the <A HREF="http://www.kdeleague.org/">KDE League</A>, a group of industry
leaders and KDE developers focused on facilitating the promotion,
distribution and development of KDE.
<A HREF="http://www.kdeleague.org/"><IMG SRC="http://www.kde.com/img/kdeleague_s.png" WIDTH=117 HEIGHT=91 ALIGN="right" BORDER=0></A>
</P><P>
The founding members of the League include leaders from a cross-section
of the computer industries:
<A HREF="http://www.borland.com/">Borland</A>,
<A HREF="http://www.caldera.com/">Caldera</A>,
<A HREF="http://www.compaq.com/">Compaq</A>,
<A HREF="http://linux.corel.com/">Corel</A>,
<A HREF="http://www.fsc-usa.com/rc/index.html">Fujitsu-Siemens</A>,
<A HREF="http://www.hp.com/">Hewlett-Packard Company</A>,
<A HREF="http://www.ibm.com/">IBM</A>,
<A HREF="http://www.kde.com/">KDE.com</A>,
<A HREF="http://www.klaralvdalens-datakonsult.se/">Klarälvdalens Datakonsult</A>,
<A HREF="http://www.thekompany.com/">theKompany.com</A>,
<A HREF="http://www.mandrakesoft.com/">Mandrakesoft</A>,
<A HREF="http://www.suse.com/">SuSE</A>,
<A HREF="http://www.trolltech.com/">Trolltech</A> and
<A HREF="http://www.turbolinux.com/">TurboLinux</A>.  The League
is an open organization and other corporations
who support the goals of the League are encouraged to apply for
membership.
</P><P>
The League will focus on promoting the use of the advanced Open Source
desktop alternative on PCs, workstations and handheld devices by
enterprises and individuals and on promoting the development of KDE
software by third-party developers.  The League will not be directly
involved in developing the core KDE libraries and applications,
although League members are encouraged to contribute to the KDE
codebase in the spirit of KDE's wildly successful 'Bazaar-style'
development.
</P><P>
Instead, the industry leaders have united to provide financial, moral
and promotional support to KDE with three principal goals in mind.
First, to ensure that KDE remains a desktop standard for Linux and other
UNIX workstations and PCs and that KDE becomes a desktop standard on
handheld devices.  Second, to help KDE compete effectively, on its
merits, with proprietary and other desktops prevalent today.  Third,
to encourage third-party developers to develop for the KDE platform,
thereby providing KDE users with a wide assortment of software that
makes use of KDE's cutting-edge technologies.
</P><P>
"The creation of the KDE League marks a vital step forward for KDE,"
said Matthias Ettrich, founder of KDE.  "With the support of our
corporate partners, we can work together to ensure KDE gains wider
recognition, a greater number of applications and increased
functionality, while maintaining the open development model and
technical excellence that has made KDE the most popular Open Source
desktop."
</P><P>
Governance of the League will be controlled by a Board composed of
representatives of the core KDE developers and of each KDE League
member,  with the developers and corporate sponsors sharing power
equally.
</P><P>
       In conjunction with the announcement of the KDE League, IBM announced
that it is working with key Linux development partners such as
Trolltech, Mandrake and other League members to deliver components
of IBM's <A HREF="http://www-4.ibm.com/software/speech/">ViaVoice</A>
on KDE. IBM's ViaVoice is
currently the only voice recognition software commercially
available for the Linux operating environment.
</P><P>
 
<H3>About KDE</H3>
</P><P>
KDE is an independent, collaborative project by thousands of developers
worldwide. The KDE team recently released KDE 2.0, which for the
first time offers a fully object-oriented, component-based desktop and
office suite. KDE 2 promises to make the Linux desktop as easy to use as
the popular commercial desktops while staying true to open standards and
empowering developers and users with quality Open Source software.
</P><P>
For more information about KDE, please visit KDE's
<A HREF="http://www.kde.org/whatiskde/">web site</A>.  For more information about the KDE League, please visit the KDE League's <A HREF="http://www.kdeleague.org/">web site</A>.
</P>
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top">
<TD NOWRAP>
Andreas Pour<BR>
917-312-3122<BR>
pour@kde.org<BR>&nbsp;<BR>  
  Kurt Granroth<BR>
480-732-1752<BR>
granroth@kde.org<BR>&nbsp;<BR>
Chris Schlaeger<BR>
cs@kde.org<BR>
</TD></TR>
</TABLE>
