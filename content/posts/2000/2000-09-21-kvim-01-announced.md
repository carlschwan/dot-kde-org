---
title: "KVim 0.1 announced"
date:    2000-09-21
authors:
  - "numanee"
slug:    kvim-01-announced
comments:
  - subject: "Re: KVim 0.1 announced"
    date: 2000-09-21
    body: "Rumors about XEmacs in KDE? Wonderful! At last. Please say it's true!\n\nWe need to hear more about this. Especially after the VI people are getting theirs :-)\n\nJ."
    author: "johanw"
  - subject: "Re: KVim 0.1 announced"
    date: 2000-09-21
    body: "You're the first poster!  It's a relief to know it works.  ;)  Was getting a bit worried when we were getting all those hits but no posts.\n\nAs for the rumors, well I'm afraid we need to be a little bit more patient...  What I can add though is that reportedly the author of the GTK port originally preferred Qt/KDE for technical reasons but didn't go through with the idea for license reasons.  Now that Qt is GPL, there's nothing stopping anyone...\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: KVim 0.1 announced"
    date: 2000-09-21
    body: "That's very interesting Navin - I downloaded the GTK port to have a look at how hard a KDE version would be a couple of weeks back. I came to the conclusion that while it isn't trivial it isn't too hard either."
    author: "Richard Moore"
  - subject: "Re: KVim 0.1 announced"
    date: 2000-10-04
    body: "I'm not sure about the licensing problem. It might be even worse, because the vim license is definately not GPL compatible (GTK is LGPL not GPL), so you cannot link vim to a GPL'ed Qt.\nIt should be compatible with the QPL though. IANAL, but I think you still cannot link\nVim + Qt + (Anything with GPL'ed code).\n\nArnd <><"
    author: "Arnd Bergmann"
  - subject: "Re: KVim 0.1 announced"
    date: 2000-09-21
    body: "What about a KDE/Qt port of the Nirvana editor? (www.nedit.org)"
    author: "Mats Eriksson"
  - subject: "Re: KVim 0.1 announced"
    date: 2000-09-21
    body: "Hey, I'd like that too... I've started using nedit almost exclusively at work, it's very clean and functional.  Default syntax highlighting looks good, too."
    author: "Bill Soudan"
  - subject: "Re: KVim 0.1 announced"
    date: 2000-09-21
    body: "I love this editor too. I hope they make a native Qt/KDE version. It's heavy reliance on LessTif makes it doubtful but who knows.\n\nIt would be great though."
    author: "Robert"
  - subject: "Re: KVim 0.1 announced"
    date: 2000-09-22
    body: "[20020126: Added text.  Long standing bug that killed cataloguing. --NU]"
    author: "Martin Uhl"
  - subject: "Re: KVim 0.1 announced"
    date: 2000-09-22
    body: "Ups, Konqueror lost the Text, when I clicked back in the Preview page... )-:\n\nHere it is:\n\nYes, a port of Nedit to KDE/QT would be a real benefit to KDE. I'd really like to see such a port. Then you could choose if you'd like to use kwrite oder such a new knedit.\n\nKeep up the good work!\n\nGreetings Martin\n\n-- The gui in Penguin is pronounced K-D-E"
    author: "Martin Uhl"
  - subject: "Re: KVim 0.1 announced"
    date: 2000-09-21
    body: "This is great!  Maybe I'll take the time to really learn Vi now (seriously).  Gvim is pretty good, but it's just not a KDE app.  (No offense, I just like all my apps to look the same).\n\nAnd since there is no font configuration in Kwrite in KDE2 like there was in KDE 1.x, I might need to find a new text editor w/ syntax highlighting... Then again, vi doesn't exactly have font configuration either... :)"
    author: "Thomas Philpot"
  - subject: "Re: KVim 0.1 announced"
    date: 2000-09-21
    body: "Editor for a programmer, with syntax highligting? Look at kfte and be saved!\n\nOk, so I did the KDE port, but it *is* nice ;-)"
    author: "Roberto Alsina"
  - subject: "Re: KVim 0.1 announced"
    date: 2000-09-21
    body: "Ok...you did the port...now clean up the code and improve it!  : )"
    author: "Robert"
  - subject: "no font configuration in kwrite 2 ??"
    date: 2000-09-22
    body: "Hmmm,\n\nMaybe you should take a 2nd look at kwrite...in fact it's got font configuration. Only that it's a bit hidden under \"configure highlighting\" :(\n\ncheers\nFranz (who also was a little puzzled about font config in kwrite 2)"
    author: "FranzBE"
  - subject: "Re: KVim 0.1 announced"
    date: 2000-09-21
    body: "Great news, however, the programmers editor\ni use and like most - KFTE, seems to be a dead project. Wouldn't it be nice if it was continued and ported to KDE 2.0, since it now is the only qt1.44 & kde1.0 application i use.\nAll other programmer editors i tried are either under-featured, or ugly, or extremely slow (like code-commander). vi is nice, but kfte is so much better for my needs..."
    author: "Moshe Vainer"
  - subject: "KFTE"
    date: 2000-09-21
    body: "Is there a homepage of kfte? I didn't know this text editor (or I forgot it....)\nA kpart of nedit would not be bad either ;-)"
    author: "raphinou"
  - subject: "Re: KFTE"
    date: 2000-09-21
    body: "KFTE once had something akin to a homepage, but I switched jobs and lost access to the server.\n\nAnyway, you can get KFTE for KDE1 from ftp.kde.org, and KFTE for KDE2 from cvs, module kfte.\n\nHere's a (old) screenshot: http://www.conectiva.com.ar/kfte.gif\n\nThe toolbar should look more KDE2 now, but it should also be broken ;-)\n\nAnyway, the toolbar was never useful."
    author: "Roberto Alsina"
  - subject: "KFTE ain't dead!"
    date: 2000-09-21
    body: "In fact, if you want a KDE2 version that works, just get it from CVS, module kfte!\n\nI regularly make sure it at least builds.\n\nIt's not really dead, more like finished, since development of FTE itself seems to have stopped, and I can't understand the code ;-)\n\nI might write a config GUI for it someday, but it's a large project.\n\nAlso, making KFTE into a real KPart is something I can't even imagine myself doing. KFTE's guts are scaryly overengineered :-)"
    author: "Roberto Alsina"
  - subject: "Re: KFTE ain't dead!"
    date: 2000-09-24
    body: "Great news, but if it's in CVS, why is it not on Beta & in RPMS?"
    author: "Moshe Vainer"
  - subject: "Re: KFTE ain't dead!"
    date: 2000-09-26
    body: "Because CVS has a whole lot of stuff that is not beta or in RPMs :-)\n\nI'd say that after KDE2 is officially out I will just package what's in CVS and release it so people will build packages from it.\n\nKFTE is not beta, it's just... well, finished, kinda :-)"
    author: "Roberto Alsina"
  - subject: "Icons are not kde icons"
    date: 2002-04-11
    body: "My Icons in the toolbar ( menubar? ) are the exact same ones you get with gvim. But I didn't have vim on my system and compiled the full ( heavy ) version and not the reduced on. How do I get my icons from kde to appear instead? Nice work though.\n\nJargs"
    author: "Jargon"
---
The first developer release of <a href="http://aquila.rezel.enst.fr/thomas/vim/">KVim</a> is out, thanks to Thomas Capricelli.  In case you've been living under a rock, <a href="http://www.vim.org/">VIM</a> is one of the most popular Vi variants out there, and KVim is evidently the KDE port of that worthy editor.  A <a href="http://aquila.rezel.enst.fr/thomas/vim/kvim-pre.png">screenshot</a> or <a href="http://aquila.rezel.enst.fr/thomas/vim/rendering.png">two</a> are available.  KVim will shortly be ported to KParts, so it won't be long before you can embed it in Konqi...  (As an aside, rumor has it that progress on a KDE/Qt port of XEmacs may not be too far off either.)

<!--break-->
