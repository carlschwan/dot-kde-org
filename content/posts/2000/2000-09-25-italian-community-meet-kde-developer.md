---
title: "Italian community meet KDE developer"
date:    2000-09-25
authors:
  - "dmedri"
slug:    italian-community-meet-kde-developer
---
Jurgen Vigna, italian developer for KDE/LyX, will talk about his experience about programming with Qt/KDE at the "Linux Meeting" in Bologna - Italy. More info on: <a href='http://www.linuxmeeting.net'>www.linuxmeeting.net</a>



<!--break-->
