---
title: "KDE 2.1 Beta1 Released"
date:    2000-12-17
authors:
  - "melter"
slug:    kde-21-beta1-released
comments:
  - subject: "KOOL!"
    date: 2000-12-17
    body: "Off to the download site my friends!<br><Br>\n\nPS<br>\nThanks KDE people!"
    author: "t0m_dR"
  - subject: "Re: WHERE ARE SuSE6.4-KDE2.1beta files??????"
    date: 2000-12-20
    body: "WHERE ARE SuSE6.4-KDE2.1beta files??????\n\n\nthanks, manni\n;)"
    author: "Manni"
  - subject: "Re: WHERE ARE SuSE6.4-KDE2.1beta files??????"
    date: 2000-12-21
    body: "Just download the sources and compile them yourself.  It will work much better that way, and it will be optimized for your computer, not every computer...\n\nJustin"
    author: "Justin Hibbits"
  - subject: "Re: WHERE ARE SuSE6.4-KDE2.1beta files??????"
    date: 2000-12-22
    body: "hm, in my opinion, i think it must be better too, compiling the source.., i agree, but, i thought, it takes such a loooong time to compile so much files.., but i will try that :)\nmy maschine: intel 200MMx, 96MB RAM now (but 32 uncached) <br>\nthanks, manni"
    author: "Manni"
  - subject: "Re: WHERE ARE SuSE6.4-KDE2.1beta files??????"
    date: 2000-12-22
    body: "hi justin, <br>\ni have got another question, do you know why the Size of the source-packages or SuSE7.0-packages are just around 35MB ?<br>\ni remember, before the size of the packages was somewhat around 50-60MB right ?<br>\nare there still missing some files ? <br>\nthanks, manni\n:)"
    author: "Manni"
  - subject: "Re: KOOL!"
    date: 2000-12-22
    body: "Yup, really cool..\n\nfor SuSE 7.0 users:\n\nThe new release of KDE doesn't seem to work with qt-2.2.1. I tried to install the RPMs for SuSE and it would start before I downloaded the qt-2.2.3 and compiled it. \n\nHope that Helps,\n\nStarbuck's"
    author: "Starbucks"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-17
    body: "BTW: In what version is antialiasing supposed/planned to be supported? (I know I , I know , I don't have a lot of patience <b>:-)</b> )"
    author: "t0m_dR"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-18
    body: "Oh, we are many who can't wait for antialiasing. I really really really hope it will be supported in KDE 2.1 and whatever QT (I guess it is mostly a matter of QT support) is current at the time. :-)"
    author: "jimbo"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-18
    body: "This is a XFree86(and other X-servers) issue. It won't be supported before the X-server supports it(maybe XFree-4.1?)\n\n--\nAndreas Joseph Krogh <andreak@nettverk.no>"
    author: "Andreas Joseph Krogh"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-20
    body: "I spent last night downloading XFree864 from CVS and compiling, now binaries are out! Well, I did it for anti-alias support anyway so no loss. \n\nI'm writting this using KDE2's konqy fron CVS (also last night) with anti alias text and it looks great. \n\nThere is a real easy way (?) to set this up without applying patches to QT etc. A Simple HOWTO based on what I did is below HOWEVER, I have no idea if this is needed for the final 4.0.2 release. \n\nDownload, make and make install freetype2 from www.freetype.org, this should be a recent CVS checkout or snapshout, i used this: ftp://freetype.sourceforge.net/pub/freetype/unstab le/freetype2-current.tar.gz \n\nDownload X in source form, create the file: \nxc/config/cf/host.def \n\nTo have this line: \n#define Freetype2Dir /usr/local \n\nMake and install X with make World & make install. \n\nGet an updated qt that contains the patches to use the new render, the easiest way to do this is to do a qt-copy checkout from kde's anon CVS. This already has the patches applied and a configure option to turn on render use. \n\nConfigure qt with: \n./configure -xft -sm -gif -system-jpeg -no-opengl -no-g++-exceptions \n\nmake QT...... You now have a QT with render support, anything you compile against it will get anti-aliased text including the whole of KDE2. \n\nGood luck!"
    author: "_ganja_"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-20
    body: "I spent last night downloading XFree864 from CVS and compiling, now binaries are out! Well, I did it for anti-alias support anyway so no loss. \n\nI'm writting this using KDE2's konqy fron CVS (also last night) with anti alias text and it looks great. \n\nThere is a real easy way (?) to set this up without applying patches to QT etc. A Simple HOWTO based on what I did is below HOWEVER, I have no idea if this is needed for the final 4.0.2 release. \n\nDownload, make and make install freetype2 from www.freetype.org, this should be a recent CVS checkout or snapshout, i used this: ftp://freetype.sourceforge.net/pub/freetype/unstab le/freetype2-current.tar.gz \n\nDownload X in source form, create the file: \nxc/config/cf/host.def \n\nTo have this line: \n#define Freetype2Dir /usr/local \n\nMake and install X with make World & make install. \n\nGet an updated qt that contains the patches to use the new render, the easiest way to do this is to do a qt-copy checkout from kde's anon CVS. This already has the patches applied and a configure option to turn on render use. \n\nConfigure qt with: \n./configure -xft -sm -gif -system-jpeg -no-opengl -no-g++-exceptions \n\nmake QT...... You now have a QT with render support, anything you compile against it will get anti-aliased text including the whole of KDE2. \n\nGood luck!"
    author: "_ganja_"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-17
    body: "Wohohoa!\n\nDang, I had expected a lot less for a 0.1 version increase! You KDE developers majorly rock!\n\nThank you all!"
    author: "KernelPanic"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-17
    body: "Hi! Looks very nice!\nBut I can't open images with pixie any more!\nKonqueror doesn't show gifs and I can't define an jpeg as wallpaper!\nIs this a bug in this Beta or only on my system?\nI have Suse Linux 7.0!\nThanks Chris!"
    author: "Christoph W\u00fcrstle"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-17
    body: "Now it works!\nMy system was not configured well....\nBut KDE ist really soooo great!\nChris!"
    author: "Christoph W\u00fcrstle"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-19
    body: "This is not KDE but Qt.\nYou had to recompile Qt with options -gif and -system-jpeg"
    author: "Yann"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-21
    body: "You have to compile qt with ./configure -gif -system-jpeg\n\nThat should work!"
    author: "JochenReitzig"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-17
    body: "it's great"
    author: "Manuel Rom\u00e1n"
  - subject: "Red Hat packages?"
    date: 2000-12-17
    body: "Currently there seems to exist rpm-packages only for Mandrake and SuSe. Is there going to be packages also for other distributions?"
    author: "Redhat"
  - subject: "Re: Red Hat packages?"
    date: 2000-12-18
    body: "How about Slackware and Debian packages? I run Slackware, and a number of my friends do as well. I have a PII 400mhz and it takes a while to compile the entire kde2 source tree and all packages from scratch. several days in fact. Not having Slackware packages around is a big dissapointment. Kde2.0 had them, but kde2.01 does not, how about somebody with a fast computer compiling them and makign them available for kde2.1 final? :)\n  -an anxious Slackware user..."
    author: "Will Stokes"
  - subject: "Re: Red Hat packages?"
    date: 2000-12-18
    body: "This fellow has a good point.\n\nFor whatever reason, KDE2 takes a long time to compile. And I don't mean you can go out and get a cut of coffee long. More like you can go out on a date, spend the night, THEN have a cup of coffee.\n\nI'm not saying this is anyone's fault - personally, I think KDE2 is very great from a technical standpoint(I mean, I can actually *read* the code), and it's getting very close in terms of great usability.\n\nHowever, since it takes so long to compile, providing binary packages should be pretty high up in the list of priorities for a release. If the KDE release team has decided that \"it's the distribution's job\", then that's okay, but they should make it known.\n\nThanks for your time,\n\nDave\n\nP.S.: I'm running Debian Woody, and if someone on the KDE release team reads this and wants my help, let me know."
    author: "David B. Harris"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-17
    body: "Any RPM's for Red Hat 7.0 yet?\n\n--\nB\u00e5rd Farstad\nhttp://zez.org"
    author: "B\u00e5rd Farstad"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-17
    body: "The best thing to do would be to compile from sources... that would lead to better bug reports.  RPM's should be upcoming tho."
    author: "ac"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-17
    body: "If there is available precompiled RPM-packages, I much rather use them than compile the sources myself. Somehow it feels so much better to know that there exists an easy way to remove and update the software if needed. \n\nOr does the \"make install\" always install the self-compiled binaries (etc) on top of the files which came from rpm-packages? In this case that of course wouldn't be any problem..."
    author: "Suksi Suohon"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-18
    body: "Probably not.  You'd likely have to uninstall the RPMs before you build from source.  Some distros put the KDE files all over the filesystem rather than keeping KDE in one directory."
    author: "SubPar"
  - subject: "right"
    date: 2000-12-19
    body: "I agree, a beta release don't really need precompilled packages, only stable releases need."
    author: "renaud"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-17
    body: "Say, what about noatun, the new media player?"
    author: "David Simon"
  - subject: "is www.kde.org down?"
    date: 2000-12-18
    body: "i haven't been able to reach it at all today."
    author: "ac"
  - subject: "Re: is www.kde.org down?"
    date: 2000-12-18
    body: "Same thing here. The kde sites seem to be down quite often. Then to another thing..\n<P>\nI managed to grab all the KDE rpms for Mandrake 7.2. How should i install these?\n<P>\nI shut down X by telinit 3 and tried rpm -F *.rpm but it seems to need libkscan.so.1 I tried to find it on the net but I couldn't. Any hints? Am I doing something wrong here?"
    author: "Tomas"
  - subject: "Re: is www.kde.org down?"
    date: 2000-12-21
    body: "Compile the sources, then it will work, or it should...\n\nJustin"
    author: "Justin Hibbits"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-18
    body: "Why the KOffice doesn't been include?\n\nDoes Chinese support improve in this version."
    author: "younker"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-18
    body: "KOfice will be updated separatly from KDE.\nThe 2 products are too big to insure coordination for updating them together."
    author: "Aegir"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-24
    body: "<I>KOfice will be updated separatly from KDE. The 2 products are too big to insure coordination for updating them together.</I>\n\n<P>OK... will KOffice as already installed for 2.0.1 work with 2.1Beta1?"
    author: "Rod"
  - subject: "Re: KDE 2.1 Beta1 Released (auto-proxy config)"
    date: 2000-12-18
    body: "How do I setup auto-proxy configuration. I didn't find anything in web config of kcontrol."
    author: "Michael Goffioul"
  - subject: "Re: KDE 2.1 Beta1 Released (auto-proxy config)"
    date: 2001-09-03
    body: "please See:\nhttp://home.netscape.com/eng/mozilla/2.0/relnotes/demo/proxy-live.html"
    author: "tbarka"
  - subject: "Controversity: KDE admirer, who hates using it!"
    date: 2000-12-18
    body: "I have been using kde 1.1 for some time but then had to switch to windows for a while.<p>\n\nNow I got the chance to install KDE-2.0.1 on my\nhome machine and found out there are a couple of\nthings I really dislike. (I am using Mandrake7.2) <p>\n\nMy problem is that, although it takes only an\nhour to install Linux, it takes a week to configure KDE to work. <p>\n\nThe first thing is that the default window controls are so unlike windows. With KDE you have to click on the leftmost button to close a window,\nwith windows you click on the rightmost button.\nNow, with kde I ended up maximizing (or whatever that feature was) windows all the time. It is really annoying and I couldn't figure out how to change that although I was serching through the whole control center. There might be some themes that have more windows (or even KDE1) like functionality, but in Mandrake 7.2 there aren't.  I am not such a fan of Widnows, I just I have to switch between Linux and Windows a lot and really dont like such inconsistencies. Yuck. <p>\n\nThe second thing is the Konsole. It just happens that I don't like the default color schemes anyway. Well let's face it: half of them are black on white or white on black. Oh, of course there is also the transparency feature, which fails to refresh whenever you switch virtual desktops or restart a KDE session so why having it anyway. Well there is no feature to set up say a yellow or white letters on dark blue or gray backgrond. And why am I complaining about that? Because that is a standard. You can do that Gnome, you can do that with xterm, you can even do that in THE Microsoft Windows. Hell, you could even do that with the old kvt (the KDE1 terminal program). Apparently someone had decided it was wise to trade a useful custom color selection feature buggy transparency feature. And I guess that was just because GNOME had such a feature. <p>\n\nWhy the hell is it, that everything has to be so 'feature complete' but noone thinks about useability. I would be gladly willing to get rid of 70% of the configurability in the control center just for some rationality about what users really need. <p>\n\nI am writing this comment on Windows while waiting to cool down enough to give KDE another try. Well maybe until the the KDE 2.1 might as well arrive.\nHell I am not willing to spend my life configuring and hacking things I needn't have to. <p>\n\nAnd by the way, I guess the worst thing that happened to Linux desktops is Themeability. Now no one gives a damn what the default desktop looks like, because everyone pretends things can be fixed with different themes. <p>\n\nSorry, Peter"
    author: "Peter Kese"
  - subject: "Re: Controversity: KDE admirer, who hates using it!"
    date: 2000-12-18
    body: "To move your cloise box from teh left side to the right, right click on the windows title bar, go to decorations, choose one you like. Most of them, the close is on the right...\n\n--Garion"
    author: "garion"
  - subject: "Re: Controversity: KDE admirer, who hates using it!"
    date: 2000-12-18
    body: "Even better: in KDE 2.1 the close-button is on the right by default. And please don't forget: KDE 2.x is a complete rewrite, so nobody actually \"removed\" features. Expect to get full control over your colors in konsole soon.\n\nGreetings,\nTackat"
    author: "Torsten Rahn"
  - subject: "Re: Controversity: KDE admirer, who hates using it!"
    date: 2000-12-18
    body: "Aargh! That's so brain dead! It's too easy to hit the close button by mistake when it's only 2 pixels away from minimize."
    author: "David Johnson"
  - subject: "Re: Controversity: KDE admirer, who hates using it!"
    date: 2000-12-18
    body: "In the current default the close button is FOUR pixels away from the other buttons (unlike the other buttons which are seperated by a distance of 2 pixels. If you should still have problems with that then you might want to choose one of the alternate windowmanager styles.\n\nGreetings,\nTackat"
    author: "Torsten Rahn"
  - subject: "Re: Controversity: KDE admirer, who hates using it!"
    date: 2000-12-19
    body: "Why not make the positions of buttons an option?  I know in Gnome, using the AquaX theme, you have the choice of \"Windows style, Mac Style, KDE style\" & etc, which addresses this issue perfectly.  I don't know how the themeing works, but why not fix it now before it's too late?"
    author: "Henry Stanaland"
  - subject: "Re: Controversity: KDE admirer, who hates using it!"
    date: 2000-12-18
    body: "I vote for the close-button on the left, like 2.0.0 and 2.0.1 releases. It's soo easy to use for crazy mouses and little brains like mine: left to kill, right for other operations. If it's not the default on 2.1, I hope there will be an \"original KDE2\" decoration.\n\nMore seriously, I think it's important for KDE to have a stable default decoration and theme. If this changes on each release, people will not feel \"at home\" and will have to re-learn their desktop.\n\nAnother thing: If users are focused on this kind of left/right button problem, that means potential deep problems like stability are very far from KDE2.  Quite a good news!"
    author: "Nicolas"
  - subject: "Re: Controversity: KDE admirer, who hates using it!"
    date: 2000-12-18
    body: "You may want to consider dumping KWin for\nanother window manager. KWin is extremely\nugly-looking and seems to violate X11\nstandards in at least one place (send_event\nfield in XEvent structure). There is a lot\nof other window managers available that are\nmore mature, configurable, and better looking\n(WindowMaker,Enlightenment,IceWM,etc.)"
    author: "KDE User"
  - subject: "Dumping KWin"
    date: 2000-12-18
    body: "That would not be fair. I do believe in KDE. I do believe they (we) can make a good Window Manager. If I didn't care, I wouldn't have written that complaint anyway."
    author: "Peter Kese"
  - subject: "Re: Dumping KWin"
    date: 2000-12-18
    body: "<I>That would not be fair. I do believe in KDE. I do believe they (we) can make a good Window Manager. If I didn't care, I wouldn't have written that complaint anyway.</I>\n\nI am sure KDE team can (theoretically) write a\ngood window manager. But the current fact is\nthat KWin is nowhere near available alternatives.\nAnd I do not see why anyone would use substandard\nsoftware out of plain patriotism, when better\nalternatives are available."
    author: "KDE User"
  - subject: "Re: Dumping KWin"
    date: 2000-12-21
    body: "And how can the window manager be changed in KDE? It should be in the Control Center, but haven't found it there."
    author: "Another KDE user"
  - subject: "Re: Dumping KWin"
    date: 2000-12-21
    body: "I think you can do it by manually modifying\nstartkde script. There is no way to do it from\nthe Control Center.\n\nI do not use startkde script at all, just run a few useful KDE2 applications that work and ignore the rest."
    author: "KDE User"
  - subject: "Re: Controversity: Repost"
    date: 2000-12-18
    body: "I am sorry, about the previous post. I have been a little bit pissed. I am a Linux supporter and I have even been developing it. I know things are getting solved and I guess I should better not complain about that.<p>\n\nI guess I am the kind of guy, who could easily sit down and fix all these things and share them with others. I enjoy coding C++ very much.<p>\n\nBut with this matter, it comes down to principles. I keep trying to convince people to dump Windows and switch to Linux instead. Then, one day, I try to update my own Linux installation and end up with a pile of problems.<p>\n\nThose problems weren't there in kde1. Kvt worked fine and window buttons were exactly where I wanted them. Now since everyone made such a big deal out of kde2, I expected things to get even better. But it didn't. You know my perspective about windows/window managers was that they let me have 10 consoles open at the same time as well as some 15 web browser widnows. What I want most from kde is to help me handling that 25 windows without interfeering too much. I won't repeat that I have found some glitches with kde2. <p>\n\nI got used to a certain standard and then I was forced to change it. By the same guys. It reminds me of the Microsoft attitude: \"Standards are great, everyone should have its own.\" Why did some developers thought of changing things in such a way, that they break previous kde standards. I find it difficult to forgive such a policy. At least I should have an option to stick with my old kvt instead of forcing me to work with konsole. <p>\n\nI don't blame kde not having antialiased fonts because I know how difficult it is to achieve. But I do blame the developer of konsole for spending heaps of time to support the transparency instead of spending probably less than 1 hour for customized colors. <p>\n\nDo you think I am wrong to expect developers to keep with certain standards?<br>\nI understand (now) you can select your own window button placement with kde2, but why was the default set in a different way that kde1 users were used to?<br>\nDo you think I should better sit down and fix things (like konsole) myself? I am not sure that would fix things in long run, I guess it is better to make a loud knock on some doors. Which doors?"
    author: "Peter Kese"
  - subject: "Re: Controversity: Repost"
    date: 2000-12-18
    body: "Hmm.  I don't think there is anything wrong with putting the close button on the left as default.  Sure, it's not the same as KDE1, but the KDE team wanted to change it.  The close button on the right has long been debated as the wrong place to put it (at least when it's near other non-destructive buttons).  Just because it was placed on the right in KDE1, does it mean the KDE developers must put it there until the end of time?  They want to make the Unix desktop different than Windows.  At least they give you an option to change it to the right side.  Isn't this enough?\n\nBy the way, if you want to alter your Konsole colors, have a look in this directory:\n\n/opt/kde/share/apps/konsole\n\nAll of the schema files are in there.  Twiddle to your heart's content!  It would be nicer if this was in a configuration dialog someplace, but this should do for your purposes.  If you don't have root access, try using the same folder in your ~/.kde directory.  Usually you can override the system default settings in there.\n\nAnyway, KDE is constantly a work in progress, and it's doing great.  And it's *not* Windows.  Just remember that.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Controversity: Repost"
    date: 2000-12-19
    body: "<blockquote>The close button on the right has long been debated as the wrong place to put it (at least when it's near other non-destructive buttons).</blockquote>\n\n<p>I agree that close-at-right is wrong, but I liked the Motif approach: on the left, have a button that brings up the Window Operations menu (what you get now by right-clicking the title bar).  Make double-click on that button close the window.  Then remove the close button entirely; accidental window closure becomes unlikely.  Oh, yes, and make Alt-Space the keyboard shortcut that posts the Window Ops menu.\n</p>\n<p>MS Windows works much the same way, except it has a close button.  I gather that this behaviour was inherited from some ancient IBM standard.</p>"
    author: "Glen Ditchfield"
  - subject: "Re: Controversity: Repost"
    date: 2000-12-18
    body: "> You know my perspective about windows/window<BR>\n> managers was that they let me have 10 consoles<BR>\n> open at the same time as well as some 15 web<BR>\n> browser widnows. What I want most from kde is to<BR>\n> help me handling that 25 windows without<BR>\n> interfeering too much.<BR>\n\n<P>I agree... I'm running KDE2.1 from CVS and here is how I have it helping me manage those windows:\n\n<UL>\n  <LI> I have a child panel with a task bar and pager at the top of my screen\n  <LI> The taskbar has a little drop down menu that lists every window on every desktop just in case the buttons themselves aren't enough\n  <LI> The pager has a button that shows a small snapshot of each desktop, very nice for visual recognition\n  <LI> I'm using the RiscOS decorations which, although very unlike MSWindows, has some VERY nice features, like the button that temporarily hides a window until you click on another window. at that point the window you clicked on becomes the front window and the hidden window(s) pop(s) up in the background.\n  <LI> I have assigned accelerator keys to swtich between desktops either by number or left/right\n  <LI> Instead of having 10 consoles open, i have one Konsole, with 10 sessions started in it which i can easily switch between using Shift + Left/Right or by clicking on their icons in the tool bar (also have turned off the menu bar, scroll bar (i have a scroll mouse), and window borders... lots of screen real estate saved)\n</UL>\n\nwith all these spanky features in KDE2 i manage a 6 desktop KDE session with dozens upon dozens of windows and apps running without breaking a sweat.\n\n<P>i think that perhaps the problems you are encountering are from the newness of KDE2. 2.1 is very usable (and nice looking to boot) but does not do things how they are done in MS Windows just because they are done that way in MS Windows. instead KDE2 implements things in ways  that makes the sense (most of the time, anyways). It honestly takes about 30 minutes to get used to once you are all set up (this is from watching co-workers sitting down to their new KDE2 desktops)"
    author: "Aaron J. Seigo"
  - subject: "Re: Controversity: Repost"
    date: 2000-12-18
    body: "Standard are okay, but keep something in mind, AT ALL TIMES.\n\nWhen a standard is WRONG, change it.\n\nHaving the close button only a few pixels away from non-destructive buttons is a very bad thing. It violates so many user-interface guidelines it's funny. And those guidelines were written by people who spent years testing these sorts of things on people completely unfamiliar with computers.\n\nJust because you don't like something doesn't mean it shouldn't be changed. Microsoft changes things to cause other people harm; so don't even try to bring them into this discussion.\n\nNow, I know that there have been people saying that they're going back to the status quo for 2.1, and if they're right, I'm saddened.\n\nOh, and to correct another poster: generally, people who are developing Linux as an alternative to Windows arn't trying to make it *different* than Windows, they're trying to make it BETTER than Windows. Just to clear that up.\n\nDave"
    author: "David B. Harris"
  - subject: "Re: Controversity: Repost"
    date: 2000-12-21
    body: "Hey, you would make a fine addition to the bde crew, just pop over to http://www.blackholepro.org/ or, if that doesn't work(it is a free site) go to http://black-hole.iwarp.com/  I am the head of the bde project, a very new, innovative, customizable-to-the-point-of-insanity desktop interface that will give the true meaning to customizability.  It uses an XML GUI interface to make GUI designing very customizable, and it dynamically creates the gui on startup, so that the user can redefine the gui just by editing the XML file.  If you're interested, drop me a mail.\n\nJustin"
    author: "Justin Hibbits"
  - subject: "Don't feed the trolls"
    date: 2000-12-19
    body: "Don't feed the trolls people.. This one's pretty obvious."
    author: "Warning"
  - subject: "Re: Controversity: KDE admirer, who hates using it!"
    date: 2001-05-14
    body: "When I first installed Mandrake 7.2 with KDE 2.0, I found it difficult to get used to the close button on the left, so I changed my Decoration to KDE1.  After a while, I decided that I liked the look of the KDE2 Decoration enough that I'd try to get used to the close button on the left.  I had kind of assumed that logic dictated that the close SHOULD be on the left anyhow, and after getting used to it I found this to be correct.\n\nNow I'm getting ready to upgrade to KDE2.1 and am kind of ticked they moved the damn thing back.  I find the new buttons too small and the new icons ugly compared to the 2.0 ones (I really like the 2.0 ones).  If nothing else, I would have liked to see an option to get the KDE2.0 title bar as a Decoration, since it looked better anyhow in my opinion.  I certainly don't expect them to keep the close on the left as default, especially since they are trying to bring over Windows user's without frustrating them.\n\nIf there is any way to get the KDE2.0 Decoration in KDE2.1, could somebody please post a reply and let me know.  I'm going to back up a bunch of my old files from Mandrake 7.2, so if I just need to hack in something from KDE2.0 I can do that.  If not, I guess I'll just bear it until KDE2.2, and hope a good option presents itself."
    author: "Brian Gallaway"
  - subject: "Re: Controversity: KDE admirer, who hates using it!"
    date: 2001-07-18
    body: "Now I feel dumb.  About 2 days after I posted this I installed Mandrake 8.0 and found the Laptop decoration to be the KDE 2.0 one.  Sorry for the unnecessary flame.\n\nBrian"
    author: "Brian Gallaway"
  - subject: "Re: Controversity: KDE admirer, who hates using it!"
    date: 2001-06-09
    body: "no"
    author: "nani"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-18
    body: "I must say that I'm impressed. I'm running the current CVS, and Kde2 developers did so many improvements in such a short time (just 6 weeks after release of KDE 2.0). That proves that they've chosen the right way to go. They created a base, and now C++ & QT show their superiority. Reusability of code accelerates development of new applications and addons to existing ones.\nI feel that they can now do anything they want in no time.\nCongratulations."
    author: "Houdini"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-18
    body: "2.1 Beta 1 is cool! KDE just gets better and better. Unlike people who have been finding \"issues\" with it, I am very happy with it! Remember, it's open source software, so if you don't like the custom colors in konsole or whatever, change KDE so you can configure your own.\n<p>\nI am running 2.1 Beta 1 with the latest XFree86 4.0.2 from CVS and anti-aliased fonts work perfectly - well done Keith P. and all the QT developers. konsole works properly now and it all looks very professional...\n<p>\nKeep up the good work guys... (you never know, I might get round to helping you :-))"
    author: "jpmfan"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-19
    body: "dito"
    author: "heroo"
  - subject: "Re: KDE 2.1 Beta1 Released: SuSE 7.0 RPMs"
    date: 2000-12-19
    body: "Thanks!\nI've downloaded and installed KDE 2.1 from the SuSE 7.0 RPMs and have found some improvements and a few bugs.\n\nImprovements (at least for me):\n1. The new SuSEconfig.kdm2 now configures kdm properly for KDE2.\n2. The new version of kdm has a shutdown that works properly.\n\nBugs (at least on my SuSE 7.0 system):\n1. Out of the box, konqueror can't find Java 1.2. Java 1.2 does not install automatically in the SuSE 7.0 installation and it is not included in the SuSE 7.0 RPMs for KDE 2.1. After learning this, I then installed the IBM Java 2 JRE from the SuSE 7.0 CDs. Soon I will have another look at the KHTML doco to see if I can configure Java 1.2 properly.\n2. Color blending in the title bar of windows seems to no longer work. Perhaps this is a side-effect of the new themes."
    author: "Paul C. Leopardi"
  - subject: "Re: KDE 2.1 Beta1 Released: SuSE 7.0 RPMs"
    date: 2000-12-19
    body: "Oh, and another bug:\n3. kpackage still does not know about RPMs and as a result, can't find the installed RPMs. I had to use Kpackviewer and yast instead last night.\n\nYes, I know I need to report these via the normal channels. I will soon enough."
    author: "Paul C. Leopardi"
  - subject: "Re: KDE 2.1 Beta1 Released: SuSE 7.0 RPMs"
    date: 2000-12-19
    body: "Please us the bug reporting system to report bugs rather than just talking about them here (feel free to discuss them here too of course). If you don't send them through the bug reporter then there is a good chance they will be overlooked.\n<p>\nWith regard to the actual bugs you're talking about:\n<ul>\n<li>KPackage supports RPM just fine. You have some sort of installation problem. It may be that this is related to the packaging of the SuSE RPMs, so I suggest you report it to SuSE (after checking their support database).\n<li>Not installing a JVM is not a bug, it's a feature. Not all users want Java, and the JVMs are only available for a subset of the platforms supported by KDE. Add to this the huge size of a complete Java 2 run time and various licensing issues, and you can see why we leave it to the distribution.\n<li>The titlebar stuff depends on which WM theme you are using. Without knowing this no one can help you.\n</ul>"
    author: "Richard Moore"
  - subject: "Re: SuSE 7.0 RPMs: Java now works"
    date: 2000-12-19
    body: "I will post details on kde-user and suse-linux-e soon."
    author: "Paul C. Leopardi"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-19
    body: "Great work!\nBut I have one problem:\nI am using SuSE 7.0 and KDE 2.0 found my mixer device. But KDE 2.01 and KDE 2.1 Beta1 doesn't find my mixer. Whats can I do?"
    author: "Franky"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-19
    body: "Hiho,\n\nAre other apps finding your mixer? Is the mixer device /dev/mixer ? Do you have permission to read and write to it? \n\nIf you answered all these questions with yes, I've no idea. If you can't access /dev/mixer try setting permissions correctly. I think you need both. \n\nHope that helped,\n\nRichard"
    author: "dazk"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-21
    body: "Just so you'll know, I have the same problem with my alsa-card (a sb pci 128). Well, I can wait for 2.2, hehe..."
    author: "KernelPanic"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-19
    body: "Hi there,\n\nI just installed the new Beta from source on RH7 with all available updates installed and QT-2.2.3 installed from source. KDE sits in it's own directory with a symlink pointing to it from /usr/local/kde. ld.so.conf contains /usr/local/kde/lib and PATH contains /usr/local/kde/bin. The new system was compiled with the Symlink in place and QTDIR and KDEDIR set to the correct directories. \n\nAll other versions worked perfectly (2.0, 2.0.1) that way but with the new version I have a problem. Before I started the new version, i created a new user for test purposes only (to have a clean user without existing .kde), started KDE2.1b1 via kdm and after successfull login the splashscreen stops after \"starting system services\" it disappears and after a while I get a message wich asks if dcopserver is started. \n\nAny suggestions?\n\nThanx,\n\nRichard"
    author: "dazk"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-19
    body: "Okay, I'm a Linux Mandrake 7.2 user (but I've replaced the standard X-Windows 4.01 install with a 3.3.6 because my video card does not support the 4.01 drivers).  You know, I write software for a living, but it is still very difficult for me to get the KDE 2.0 installed and running.  Is there a fast way to do this, or at least sample it before I tear my system apart to get it installed?  The RPM's I've looked at seem to require that even very deep, highly inter-dependant libraries be replaced, those I've seen, even if they are in a directory lablelled \"Mandrake 7.2\" seem to require packages and updates that I can't find!  I really just want to use the latest build for a bit before I commit to the work involved in learning how to update so much of my system.  \n\nWhat would be nice would be a step by step instructions for installing the RPM's.  I've seen this for compiling from the source but if installing the RPM's is so involved, certainly compiling from source is going to be worse.\n\nLike I've said, I've got a Mandrake 7.2 system with the current (2.???) KDE installed off of the CD.  It seems by the screenshots and error reports that a WHOLE LOT has happened since then.  \n\nMaybe I should switch to debian -- the thinking is -- it seems to be easier to update (though perhaps harder to install the first time).\n\nAny ideas?\n\nErik Hill"
    author: "Erik Hill"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-19
    body: "I just wanted to add that, at this point, I also wonder if someone will allow me to log into their system (using an X-Server) so that I can sample the look-and-feel of KDE 2.1beta.  \n\nPerhaps Mandrake Cooker is the way to go?  I hear it has a very late version of KDE on it and therefore has all the libraries it might need installed already.\n\nErik Hill"
    author: "Erik Hill"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-19
    body: "Well, I'm still a relatively newbie, and I installed Debian in the old days in a bit of an effort, but the new version is much easier. It is starting to autodetect lots of hardware as Mandrake. \nI recomend it *very* much...It is the only distribution that made me stick to linux because I installed kde-2 very easily.\n\nDaniel"
    author: "Daniel"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-20
    body: "Debian didn't detect my ATI card and my Haupauge TV card but Mandrake 7.2 do it ... and moreover it autoconfigure them.\n\nDebian is cool (I also have it on my PC) but Mandrake is cool too : I recomend it *very* much ;-)"
    author: "Shift"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-19
    body: "I also use Mandrake 7.2 and had problems installing KDE2.1 beta using rpm -Uvh *.rpm. So the fix that worked for me was to uninstall all of the equivalent KDE2.0 packages with using the --nodeps. I then installed all of the new KDE2.1 beta packages with rpm -i *.rpm. Rebooted just for the hell of it and was using KDE2.1 beta without doing anything else. I must say it is more stable in my opinion then the 2.01 release!!"
    author: "bbqPorkChop"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-19
    body: "Thank you for your help, I will try this (and also, eventually, (I keep promising myself), Debian).  Now I am installing a clean Mandrake 7.2 installation and then I will do the process you've outlined.  I guess the answer is, the packages THINK they need the very latest versions of everything, but in fact, it runs on whatever Mandrake 7.2 happens to install by default.  \n\nI see this as a bug in the packages, it should not require (in the package) any later version of anything than it actually requires.  Where are these packages?  KDE 2.1 intended for installation on Mandrake 7.2?\n\nErik"
    author: "Erik Hill"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-19
    body: "just to clarify ... I didn't skip on any requirements. The problems I had were related to the fact that I was intalling over the old KDE2.01 packages. The reason why said to use the --nodeps option is because when I tried to remove the old KDE2.01 it said there was some gnome applet, kdesdk and some other (not at my computer right now) packages that relied on the KDE2.01 packages. But since I was going to install the equivalent KDE2.1 beta packages I didn't want to remove those other packages. This worked for me so I hoped it works for you."
    author: "bbqPorkChop"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-20
    body: "Ok, but how did you get KDE 2.0.1 to work?  Do you know offhand which KDE (and how would I find out?)  is installed by default on Mandrake 7.2?  My \"About KDE\" (remember that I've installed only the default Mandrake 7.2 installation) calls itself \"K Desktop Environment.  Release 2.0  Release Candidate 1 >= 20001002mdk\"\n\nIs this the same as 2.0.1?"
    author: "Erik Hill"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-21
    body: "I got KDE2.0.1 to work the same way. Depending on which version of Mandrake 7.2 to have it will depend on which version of KDE you received. The early retail version did not come with an official release of KDE2.0 it came with KDE1.99. The download version of Mandrake 7.2 and the second retail version was released with the official KDE2.0 version. But you should be able to upgrade to the latest version by using the method above. If you need any help e-mail me at coatesp@hotmail.com."
    author: "bbqPorkChop"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-21
    body: "Great -- multiple versions of Mandrake 7.2 ....  I thought the \"7.2\" WAS the version number! :)  Anyway, thank you, and I will in fact try that.  I'm transmitting the KDE 2.1 package to my laptop now (through, of all things, a null-modem cable) so I'll give that a shot.\n\nErik"
    author: "Erik Hill"
  - subject: "Re: KDE 2.1 Beta1 Released"
    date: 2000-12-21
    body: "Automatic install (like Debian's apt-get) in Mandrake:<BR>\n<B>urpmi packagename.i586.rpm</B>\n<BR><BR>\nUninstall automatically (including dependencies):<BR>\n<B>urpme packagename.i586.rpm</B>\n<BR><BR>\nThat'll make your life so much easier compared to Redhat's RPM management"
    author: "Prana"
  - subject: "KDE not for ADMINISTRATORS???"
    date: 2000-12-19
    body: "Hi,\n\nI really love KDE, and am impressed with KDE 2.0. I strongly believe that KDE 2.1 will supersede all other desktop environments. KDE does have many Good applicatons, utilities, games, web browser, file manager. But still KDE lacks many simplifying utilities, applications, wizards, etc. for Administrators. The Administrator has to struggle with all these, and delve into the man pages and howtos to do the job. It is really hard work for them. The KDE team is very efficient in creating good Apps, wizards etc., I request you to kindly make some Wizards and Applications BOTH Client and Server for Configuring and using these:\n\na.\tNetwork\t(kdenetwork)\t\t\n\t\n1.\tDNS\n2.\tDHCP\t\t\t\t\t\t\t\t\t\t\t\n3.\tNFS\t\t\t\t\t\n4.\tSendmail\n5.\tFTP\n6.\tTelnet\n7.\tSendmail\n8.\tApache\n9.\tDatabase server/client\n10.\tFile and print\n\nb.\tSecurity (SeKurity)\n\n11.\tFirewall (FireWalK)\n12.\tProxy Server (ProKsy)\n13.\tSSH\t(KSSH)\n14.\tPGP   (EnKryptor)\n\nc.\tHardware (KHardware)\n\n15.\tX Configuration (Xkonfig)(like in Corel Linux)\n16.\tPrinter\t(KUPS)\n17.\tScanner\t(SKanner)\n18.\tHardware AutoDetection (DeteKtor)\n\nWhen I saw Corel Linux, I was impressed with what they have done to KDE 1.1.x. Why is it that KDE team haven't took notice of the pain and suffering of System and Network Administrators? Why use Command Line Interface when we have such a great Desktop environment? Why do you think that administrators are not humans :)?\n\nI know that work is going on, but this part (the administration) did not got much attention by the great KDE Team. I trust KDE developers and the KDE team and believe that you will create such Administration Wizards, Utilities, and Apps for Administrators just like you have done for the user community! I request you humbly to kindly think about it and make some really cool Administration stuff! Thanks a lot, you are doing the right thing!\n\nYours truly,\nRizwaan."
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE not for ADMINISTRATORS???"
    date: 2000-12-19
    body: "KDE may not have all of the above mentioned features but you should check out Mandrake's DrakConf utility ... you may be suprised by what you find. www.mandrake.com"
    author: "bbqPorkChop"
  - subject: "Re: KDE not for ADMINISTRATORS???"
    date: 2000-12-19
    body: "kde is a desktop that works on many UNIX systems, and to get those things done you should use the tools provided with your distribution or the software you want to configure.\n\nfor linux systems you can use linuxconf, drakconf, rh control-panel, rh setup and many others."
    author: "Evandro"
  - subject: "Re: KDE not for ADMINISTRATORS???"
    date: 2000-12-20
    body: ">1. DNS\n>2. DHCP \n>3. NFS \n>4. Sendmail\n>5. FTP\n>6. Telnet\n>7. Sendmail\n>8. Apache\n>9. Database server/client\n>10. File and print\nMost of these tasks/configuration files are much too complex for a GUI.\n\nI think a GUI for administration/configuration of server tasks would be the wrong way (the M$-Way). Administators must be able to maintain a server over a slow dialup line, all you need is ssh and vi. I do not want to install X and K on a server."
    author: "Dirk Manske"
  - subject: "Re: KDE not for ADMINISTRATORS???"
    date: 2000-12-21
    body: "What if you have a small (low load) server? Having  X installed and running by default wouldn't be that bad.\nIf your system boot in runlevel 3 you can run the program with a remote XWindow server ;-)\nIf you can't do that, here is another solution:\n\n\t$ X :0\n\t$ export DISPLAY=:0\n\t$ yourAdmProgram &\n\nand when you are done kill the X server.\n\n\nGreetings"
    author: "Another KDE user"
  - subject: "Re: KDE not for ADMINISTRATORS???"
    date: 2000-12-23
    body: "My prefered solution for remote admin is web based configureation , since you can have a ssl enable connection, and the convience of giu configuration that works with any operating system. Check out webmin and be prepared to be amazed."
    author: "richie123"
  - subject: "Re: KDE not for ADMINISTRATORS???"
    date: 2000-12-23
    body: "I know webmin. I do not like it much, always there is a cause to use ssh. You cannot write webmin modules for every purpose."
    author: "Dirk Manske"
  - subject: "Re: KDE not for ADMINISTRATORS???"
    date: 2000-12-20
    body: "Hi there,\n\nKHardware is somewhat there, at least for information purposes. Try kcminfo. For the other tools, well, go and get yourself a copy of the open source book on how to develop KDE2 Apps and go ahead ;). Once you create an app that is good enough I'm sure the maintainers will think about including it in the standard distribution. For now, try to find what you are looking for on freshmeat.\n\nGreetings and have a nice holiday,\n\nRichard"
    author: "dazk"
  - subject: "Re: KDE not for ADMINISTRATORS???"
    date: 2000-12-20
    body: "For what concerns the CUPS system, a quite complete and powerful administration tool already exists, which is called KUPS. Maybe you already know, but have a look at http://cups.sourceforge.net/kups/index.html."
    author: "Michael Goffioul"
  - subject: "Re: KDE not for ADMINISTRATORS???"
    date: 2000-12-20
    body: "I agree.\nThere is to few good administrator utilitys for KDE.\nAnd I don't mean utilitys for adminitrating the box you're sitting on but a server someware else.\nRight now I'm using Webmin with SSL but it would be nice to do it straight from KDE."
    author: "Joakim Ganse"
  - subject: "Re: KDE not for ADMINISTRATORS???"
    date: 2002-07-22
    body: "sorry that i disturb your discussion for something other. i try to bring sendmail running over webmin, but it dont works, and i dont know what i do wrong. i try to get mails from an account with..sorry \"outlook express\". but its didnt work, can you give me some tipps to configure sendmail?\nthanks alot\n"
    author: "fernando"
  - subject: "If anyone cares...."
    date: 2000-12-20
    body: "The former problem I was having with KDE 2.0 and 2.0.1 libmimelib on Redhat 7, (see <A HREF=\"http://dot.kde.org/976035423/976046093/976148930/\">KDE 2.0.1 Problem</a> ) has magically disappeared when I installed KDE 2.1 beta.  I'm using it now, in fact.\n\nThanks,\nBrendan"
    author: "Brendan"
  - subject: "It's incredible"
    date: 2000-12-20
    body: "I just compiled the Dec 17th CVS snapshot with --disable-debug and --enable-final, it's lightyears faster than 2.0.1, stable and the new features are greatly appreciated. The Dec 20th snapshot is out, I'll compile it later."
    author: "Kaufman"
  - subject: "kdesupport-2.1beta1.tar.bz2 missing??"
    date: 2000-12-20
    body: "Isn't there a tarball for kdesupport?\nWhat gives?\nAnd why kdesupport is ho huge in the CVS (9MB)?\n\nBilly"
    author: "Billy"
  - subject: "Re: kdesupport-2.1beta1.tar.bz2 missing??"
    date: 2000-12-20
    body: "you can use kdesupport-2.01 or 20001217/20."
    author: "Evandro"
  - subject: "Where's the kdesupport-package?"
    date: 2000-12-24
    body: "Hi,\ntried to compile the whole thing for the first time (was using the rpm's for SuSe 6.4 before), but i'm getting some messages about missing lib's.\nThe Comp. FAQ tells about:\nCompile qt first (is working), then kdesupport then kdelibs and the rest in any order. But?? where the heck do i find the kdesupport package??\ni'd appreciate some help"
    author: "Newbie"
  - subject: "Re: Where's the kdesupport-package?"
    date: 2000-12-24
    body: "you can use kdesupport 2.01 or the current snapshot."
    author: "Evandro"
---
The KDE Team today announced the release of KDE 2.1-beta1. KDE 2.1 constitutes the second major
release of the KDE 2 series.

The full announcement is available <a href="http://www.kde.org/announcements/announce-2.1-beta1.html">here.</a>

<!--break-->
<P>
KDE 2.1-beta1 offers a number of additions, enhancements and fixes over
KDE 2.0.1, the last stable KDE release which shipped on December 5, 2000.
The major additions are:
</P>
<UL>
<LI>A new and much-anticipated theme manager has been added, and many icons
have been improved.  In addition, semi-transparency (alpha-blending) has
been implemented on small images and icons.</LI>
<LI><A HREF="http://www.mosfet.org/pixie/">Pixie</A>, an image viewer/editor, has been added to the Graphics package.</LI>
<LI><A HREF="http://www.kdevelop.org/">KDevelop</A>, a C/C++ integrated
development environment, has been added to the core KDE distribution.  The
version being shipped, 1.4beta, is the first version of KDevelop to
make use of the KDE 2 libraries and integrate completely with the KDE 2
desktop.</LI>
<LI><A HREF="http://www.konqueror.org/">Konqueror</A>, the KDE 2 file
manager, can now <A HREF="http://www.mieterra.com/konqueror/malte2.png">be
configured</A> to provide thumbnail previews for
<A HREF="http://www.mieterra.com/konqueror/preview.png">text and HTML files</A>.
In addition, the standards-compliant Konqueror now stores bookmarks
using the standard <A HREF="http://grail.sourceforge.net/info/xbel.html">XBEL
bookmark format</A>; a new bookmark editor complements the new standard.
Finally, auto-proxy configuration has been implemented.</LI>
<LI><A HREF="http://developer.kde.org/kde2arch/khtml/index.html">KHTML</A>,
the HTML widget, now has a special 'transitional mode' which greatly improves
its handling of malformed HTML pages.  In additon, KHTML now has greatly
improved Java support and has added support for Java security (JDK 1.2 or
compatible is now required).</LI>
<LI>The panel (Kicker) has enjoyed significant improvements.
An external taskbar has been included (familiar to
KDE 1 users), support for sub-panels has been added (which can be separately
sized and positioned), an improved external pager (Kasbar) has been added,
and support for applets has been improved (including support for
<A HREF="http://windowmaker.org/">WindowMaker</A> dock applets).</LI>
<LI><A HREF="http://www.arts-project.org/">ARts</A>, the KDE 2 multimedia
architecture, now offers a control module to configure sampling rate and
output devices, increased performance, improved user interfaces and a
number of additional effects and filters.</LI>
<LI>For developers, a number of classes have been added to the core
libraries, including a class for undo/redo support (KCommand) and
a class for editing list boxes (KEditListBox).</LI>
</UL>
