---
title: "KDE 2 Final Beta Preview is out!"
date:    2000-09-16
authors:
  - "raphinou"
slug:    kde-2-final-beta-preview-out
comments:
  - subject: "Re: KDE 2 Final Beta Preview is out!"
    date: 2000-09-21
    body: "Great job\n\nUnfortunately there are no rh-62 packages\navailable. Due to the success of this\ndistribution it would be a grant, iff\nthis environment will be considered too."
    author: "Christian Groove"
  - subject: "Re: KDE 2 Final Beta Preview is out!"
    date: 2000-09-21
    body: "The official Red Hat package manager who was responsible for this was unfortunately on vacation.  He's back now.  \n\nRobert actually tried his hand at creating the RPMs but dropped the effort once Bero came back.  So it shouldn't be too long before the Red Hat RPMs are out if they aren't already.\n\nThere's no conspiracy or boycotting involved at either end. Btw, do try compiling from source if you can't wait.  It's not that hard.\n\nCheers,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: KDE 2 Final Beta Preview is out!"
    date: 2000-09-22
    body: "Unfortunately the RPMs at the moment do not include koffice and kdegraphics (which is very important to me because of kdvi, my favorite dvi viewer).\n\nIt also seems that the rpms are assembled from scratch and not using the spec file from the Beta3 ones. If it is true -- why ?\n\nKDE is great !!! I'm looking forward for kdegraphics rpm to try the Beta5 finally.\n\nKLM."
    author: "KLM"
  - subject: "Re: KDE 2 Final Beta Preview is out!"
    date: 2000-09-21
    body: "I have RedHat and ended up compiling it. It took me a whole day. So yes a rpm would be less time consuming although I did get much more satisfaction from compiling it from the source."
    author: "Felix Rodriguez"
  - subject: "Re: KDE 2 Final Beta Preview is out!"
    date: 2000-09-21
    body: "This is why a Dual Celeron rules.\n\nAfter you have compiled qt, kdelibs and kdesupport, you can start two compile jobs (just ./configure;make in two dirs at the same time) which goes nearly twice as fast. Still takes half a day (4 to 5 hours or so) to compile a complete KDE from scratch. This using 2x466 Celeron with only 64 MB memory (which is about just enough not to let it swap when compiling 2 things at a time - 128 MB would be great, but memory is too expensive at the moment)\n\nOh, and this machine (complete with case, Abit BP6 mobo, cheapo VGA, 4.3Gig HDD, 2x466 celeron, 64MB RAM, no screen) cost me less than $500 (about 1200 dutch guilders)"
    author: "W. Havinga"
  - subject: "Re: KDE 2 Final Beta Preview is out!"
    date: 2000-09-24
    body: "just use\nmake -j 2\ninstead of \"struggeling\" with several compiles at once ;-)"
    author: "Ossi"
  - subject: "Re: KDE 2 Final Beta Preview is out!"
    date: 2000-09-21
    body: "<P>Actually, the real problem in my mind is not\nthe lack or RPMs, but the lack of binary tarballs.\nI really want to beta test KDE2, but I do real\nwork with my machine. So overwriting KDE1 with\na beta version -- even a really cool one --\nis a definite no-no.</P>\n<P>I used to try out KDE CVS builds all the time\nwhen Christopher Molnar was building binary \ntarballs. I would have loved to have done the same\nwith 1.94.</P>\n<P>Cheers.</P>"
    author: "Vad"
  - subject: "Re: KDE 2 Final Beta Preview is out!"
    date: 2000-09-22
    body: "What distribution are you using?\n\nOn my SuSE 6.4, KDE2 peacefully coexists\nwith kde1 in /opt/kde2 and /opt/kde2 respectively.\n\nit is not difficult to set up, simply type\n\nrpm -i k*.rpm\n\nMatthias\n---\npizza salami please"
    author: "Matthias Lange"
---
The final beta preview of KDE 2, code named Kandidat, has been <a href="http://www.kde.org/announcements/announce-1.94.html">announced</a> and is available for <a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/tar/src/">download</a>(remember there are several <a href="http://www.kde.org/mirrors.html">ftp mirrors</a>). It is now based on the <a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/tar/src/qt-x11-2.2.0.tar.gz">final Qt 2.2</a> and and won't work with a Qt 2.2 beta.  <B>Update:</B> Debian packages of this final beta are already available in the woody distribution.








<!--break-->
