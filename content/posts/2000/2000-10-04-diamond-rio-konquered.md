---
title: "Diamond Rio Konquered"
date:    2000-10-04
authors:
  - "numanee"
slug:    diamond-rio-konquered
comments:
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-04
    body: "it's great\n\nthanks to all"
    author: "Manuel Rom\u00e1n"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-04
    body: "Isn't it a little late for Napster?\n\nWhat is really needed is a kioslave for Windows CE! I want to browse downloaded websites offline.\nWell, ok, for iPac at least, which can run Linux. (I hope we will see embedded Qt on it some time)"
    author: "reihal"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-04
    body: "I think there is a GREAT need for IO-slaves. The whole architecture with those IO-slaves shows KDE's technical maturity, because with little plugins, you can extend the functionality of all KDE apps at the same time. There are so many IO-slaves I could think of: <BR><BR>\n<UL>\n<LI>Windows CE / Palm (for KOrganizer, Magellan...)\n<LI>Gnutella\n<LI>Freenet ?\n<LI>Napster\n<LI>LDAP (Bigfoot adress search etc.)\n<LI>A *WORKING* SMB slave\n<LI>...\n</UL>\n<BR>\nThen again, I don't know whether this is possible, because for all the IO-slaves *I* know, they have to get a straight URL, and having something like &quot;napster://Metallica-Nothing else matters&quot; is not a unique address. I mean, it would have to show a list of possible downloads, not just get the next best file. This would mean you can just as well write KNapster, because it wouldn't make too much sense embedding it in Konqueror...\n"
    author: "Anonymous Coward"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-04
    body: "<p>I think it absolutely makes sense to embed as many slaves into konqueror as possible (like your example of Napster). IO-slaves have two big advantages that come to my mind: fist, if the core of kde gets an update every \"protocol\" or \"application\" who uses IO-slaves is automatically updated and second: after all, the information which is being accessed to through IO-slaves is very \"database-like\". That means that it's sortable and everything is an object (=a file), whether it's an LDAP entry or a MP3-file on your Rio. The user benefits from a _single_ user interface and he can use sort functions of konqueror or link or drag'n'drop or whatever cool stuff konqueror does, too!\n<p>\nInstead of reinventing the wheel and create own GUIs for applications like Napster or Diamond RIO (which deal with about the same objects, more or less) why not use a very functional and stable base? It's like with templates in your word processor: you wouldn't write your company's address onto every letter you write. Instead, you create _one_ \"base\" (= the template\") and with every new letter you automatically get the newest header / footer.\n<p>\nAll in all: very cool stuff. I've been waiting for this kind of thing for a long time!"
    author: "hoshy"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-04
    body: "of course it makes sense to look for\nnapster://Metallica Nothing Else Matters\n\nbecause the kio-napster would then search the database, return a list of matches, and \"Metallica Nothing Else Matters\" would appear as a directory, containing a list of files that match that.\n\nnapster://search/Metallica\nnapster://users/usernameofnapsterperson\n\nI also think it's funny that we talk about Metallica and napster after that little lawsuit ;)\n\nAnd the kio-napster is no more than rumor, I'm not doing one.  Someone else can :)"
    author: "Charles Samuels"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-06
    body: "Great work!! I like my rio BUT I *hate* the shit windows software that have to use with it, I know there are a few tools for Linux availible but nothing I really like. Just using Konqy is the perfect solution...\n\nAre there any more details availible? Kernel version and other requirments\n\nI would also like to see a working SMB slave else, take it out before KDE2 is released. Were going to get a hugh amount of bug reports if KDE2 ships with it.\n\nAnyway, great work, shame the CVS is frozen."
    author: "David Ankers"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-06
    body: "I think Corel linux ships with a pretty good smb browser, so it shouldn't be impossible to do. (The corel code is OSS AFAIK)"
    author: "Rikard Anglerud"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-06
    body: "I think Corel linux ships with a pretty good smb browser, so it shouldn't be impossible to do. (The corel code is OSS AFAIK)"
    author: "Rikard Anglerud"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-06
    body: "If you've used krio, this will work as well.  All it requires is a working /dev/io (which we should all have).\n\nIt has a setuid \"server\" for handling this, so any user can access their rio without being root, without having to type in a password."
    author: "Charles Samuels"
  - subject: "SSH io-slave?"
    date: 2000-10-05
    body: "I would like to suggest a SSH io-slave, similar to the FTP one but using scp instead of ftp.\nThat would be great for security-conscious network users :)\nWe already have a free OpenSSH: is it possible to merge OpenSSH and io-slave technology or there are some licensing issue?\nI'm not a programmer myself, so I'm not able to write a scp io-slave, but _please_ let somebody write it!! :)"
    author: "Federico Cozzi"
  - subject: "Re: SSH io-slave?"
    date: 2000-10-05
    body: "scp has no way to enumerate files and directories.\n\nIt could be done with some seriously eviul hackery, but it'll never be done nicely.  Not until someone improves scp."
    author: "Left my login in my jeans"
  - subject: "Re: SSH io-slave?"
    date: 2000-10-06
    body: "Hmmm....I quess there is sftp thing in 2.0 \nversion?\n\n<PRE>\n~ file `which sftp`\n/usr/local/bin/sftp: symbolic link to sftp2   \n\n~\n</PRE>\nHow about that? I never tried it, but\ncould be worth of investigating."
    author: "netpig"
  - subject: "Re: SSH io-slave?"
    date: 2000-10-06
    body: "Well, you always have ssh :)\nYou connect via ssh to the remote host; you send commands to the remote shell (ls -l etc...); you parse the output.\nThis way, you could list directories, create/remove directories, rename/delete/move files using ssh, but transfer them using scp.\n\nRegarding sftp, it's included only in SSH2 from www.ssh.fi (the non-free version). So you can't use it easily."
    author: "Federico Cozzi"
  - subject: "Re: SSH io-slave?"
    date: 2001-12-23
    body: "At least 'kio_fish' package works like that.\n\nSee: http://ich.bin.kein.hoschi.de/fish/\n(or package 'kio-fish' if you happen to use Debian)\n\nUsage:\n\n  fish://username@hostname/directory/\n\n\n(You probably knew this already as the message is quite old but let's record this to the message archive anyway - in case someone finds this through a search engine.)"
    author: "jarno"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-05
    body: "A working SMB slave !\n\nYeah that's too bad :( The current SMB implementation will probably not be fixed before the release of KDE 2.0\n\nPerhaps you could think about writing a better SMB slave ? :)"
    author: "Jelmer Feenstra"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-05
    body: "the kioslaves idea was already implemented\nin kernel space at the GNU/Hurd as a translator\nbefore one could think of kioslaves.\n\nso if the hurd would be a real alternative one day\none cloud write a translator for things you have\nmentioned. then all apps would benefit and not only kde. at the hurd stuff like filesystems or devices such as /dev/null are implemented as\ntranslators and could loaded at runtime by the\nuser. take a look at:\n\nhttp://angg.twu.net/a/the_hurd_links.html"
    author: "tom"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-05
    body: "Does anyone know if a kioslave exists for\npersonal (non-public) ftp connections? I know\nthere are ftp clients that do that, like kbear,\nbut wouldn't it be much simpler and much more \nelegant to implement this as a kioslave? \nI'm thinking something along the lines of:\n\nftpu:/blah.com     \n\nwhich would automatically ask you for a username,\npassword for the site blah.com (instead of\ndefaulting to an anonymous connection like\nftp:/blah.com would do), and ask whether you \nwish this info to be saved for the next time you \nlogin. I just think this would be really handy."
    author: "Bernard Parent"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-05
    body: "ftp://username@site.net\n\nthen it prompts for a password."
    author: "Charles Samuels"
  - subject: "Re: Diamond Rio Konquered"
    date: 2000-10-05
    body: "awesome! It even remembers the password.\nThanks~"
    author: "Bernard Parent"
  - subject: "I/O Slaves"
    date: 2000-10-05
    body: "Is there already an I/O slave for the KDE Control center? I just can't understand why the control center isn't using Konqueror by default.\n\nAnother feature that I'm really missing is the ability to have nested I/O slaves. Now it is not possible to open tar files on an FTP link from Konqueror - I'd have to download the tar file first :(\nOf course the URL convention doesn't natively support this because http://ftp:// or something similar would be nonsense, but tar://http:// would definitely make sense. It only requires a few adjustments in the URL parser and a recursive approach for opening the actual content, I think.\n\nCan somebody who knows more about this tell me if I'm thinking wrong or if it's actually possible?"
    author: "Martijn Klingens"
  - subject: "Re: I/O Slaves"
    date: 2000-10-10
    body: "Tis not necesary if ftp ioslave has tha ability to recognize tar file and make a link that uses tar ioslave..."
    author: "miha"
---
<a href="mailto:hetz@linuxqa.com">hetz</a> wrote us with the news that <a href="mailto:charles@kde.org">Charles Samuels</a> has hacked together a kioslave for the <a href="http://www.diamondmm.com/">Diamond Rio</a> MP3 player. In other words, you can now reorder songs and soon upload to and download from the Rio, all within Konqueror.  Check out the <a href="http://www.derkarl.org/screenshots/kio_rio.png">screenshot</a>.  Rumor has it Charles' next project is a Napster ioslave!







<!--break-->
