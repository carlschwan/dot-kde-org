---
title: "LJ Readers' Choice"
date:    2000-10-17
authors:
  - "dsweet"
slug:    lj-readers-choice
comments:
  - subject: "Re: LJ Readers' Choice"
    date: 2000-10-17
    body: "Alright!!  It seems like KDE is taking all the awards and KDE 2.0 has not even been released!  Congratulations to everyone!"
    author: "ac"
  - subject: "Can they make it smaller?"
    date: 2000-10-17
    body: "Can people even read that banner in real life?? A huge K smashing through the banner would be way cooler... :)\n\ncongrats kde team ...."
    author: "jorge o. castro"
  - subject: "Re: LJ Readers' Choice"
    date: 2000-10-17
    body: "Woohoo.\n\nKDE has been taking the awards from day one. There's no doubt the version 2.0 will sweep the floor. Hehhe\n\nCongrats!!!\n\nbunty"
    author: "Christien Bunting"
  - subject: "Stability?"
    date: 2000-10-17
    body: "I question if they will continue with the awards after it is released, and everyone sees how unstable it is.  I think it's a great project, but there is a lot of work to be done."
    author: "KDE User"
  - subject: "Re: Stability?"
    date: 2000-10-17
    body: "I agree, it's a shame it's going to bee a \"GNOME 1.0\" release:-(\nI can't understand why they don't want to release a version out for the public to review for some time, _then_ release a rock-solid KDE2 after months of bugfixing. I realize KDE2 needs to get out to the people, but let them have the previews then. Compared to Enlightenment, Sawfish etc.  Kwin is more of a version-0.0.1 windowmanager. You can't even set window-decorations(none, border-only, etc..) which should be mandatory for an X-window-manager. I would much more appriciate a \"you'll get it when it's ready(ie. tested, proven stable..)\" attitude. I myself use it every day at both work and at home, and there are those little things that just don't feel right. Like the Kwin-thing mentioned abow(it also krashes a lot), koffice-components crashing from time to time, the filters not being ready, kio-processes dying, konqueror not handeling JavaScript correctly etc... I'm sure this will be fixed in a 2.0.[1|2] version, but why not have it ready in a 2.0.0 version _even_ if it means waiting another 2 months.\nAll in all it works _almost_ satisfactory. And I must say I'm impressed by how far they have come with this project, but I also know they'll get even further. I'm just afraid new users will install a distro with KDE-2.0 as the default, and say - \"Hey, its as unstable as Windows, I thought Linux was stable?\"(yes, they call what they see Linux).\n\nAnyway - keep up the good work, just don't get rushed:-)\n\n--\nAndreas Joseph Krogh <andreak@nettverk.no>"
    author: "Andreas Joseph Krogh"
  - subject: "Re: Stability?"
    date: 2000-10-17
    body: "<P>> You can't even set window-decorations(none, <BR>\n> border-only, etc..)\n\n<P>try right clicking on a window titlebar and going into the \"Decorations\" menu where you will find ~10 different window decorations (include \"smart\" ones, that adjust according to their environment, such as B2K).. there isn't a \"none\" option there, though one could write a plugin that would do that as i understand. \n\n<P>> koffice-components crashing from time to time, <BR>\n> the filters not being ready,\n\n<P>koffice is anything but ready for prime time.. it isn't at the same point of stability that the rest of the project is. as i understand it, though, koffice is an ancillary project to the KDE2 project. its more of a seperate project by many of the same people involved in KDE2 if nothing else.\n\n<P>> kio-processes dying, \n\n<P>yes, this is annoying... although i've seen a steady improvement over the last 3-4 releases. still, once in a while a kio* gets left around AFTER the process using it is gone.\n\n<P>> konqueror not handeling JavaScript correctly \n\n<P>that's what netscape's for =)  konquerer is the only file manager i've ever used that is even half decent as a web browser. in fact, its more than half decent, its 98% decent =)\n\n<P><I>> but why not have it ready in a 2.0.0 version<BR>\n> _even_ if it means waiting another 2 months</I>\n\n<P>because waiting longer could result in a disastrous loss of user base for kde. users want/need a new desktop now; kde1 was great but the times have moved and something better is needed\n\n<P>also, by releasing it now the number of people using it (and therefore feedback) will jump tremendously. 6 months' to a year's worth  of feedback will probably occur within 1-2 months by releasing it now as \"2.0\".\n\n<P>besides, haven't we learnt by now that *.0 means \"usable, but not a lot of real world experience\". kind of like a kid about to get his own place for the first time =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Stability?"
    date: 2000-10-17
    body: "good point; but i think his concern is that instead of reporting bugs users would just switch desktops. my guess is that it probably won't happen, but that was his point."
    author: "krazyC"
  - subject: "Re: Stability?"
    date: 2000-10-18
    body: "> Because waiting longer could result in a\n> disastrous loss of user base for kde. users\n> want/need a new desktop now; kde1 was great\n> but the times have moved and something better\n> is needed \n\nThis is bull.  Linux is still gaining users how could KDE \"lose user base\"?? Besides even if it is gradually \"losing user base\" (whatever that could possibly mean for a free OSS project) people use Kfm as a regular filemanager application as much as they use Netscape for surfing the web.  \n\nKDE 1.1.2 is a very good and **way** more stable piece of work.  There's not very many well maintained applications written specifically for KDE, but as a filemanager/desktop thingy it does the job. You obviously haven't been part of a project where a too early release has **COMPLETELY KILLED** it. Granted KDE may be too big for that too happend but if 5-10 gaping security bugs are found and something as bad as bug 10380 (which bit me in RC2) is widely experienced by \"the user base\" you'll see a lot worse situation than what happens when a release is late.\n\nLook at the Linux kernel --- it's like 6 months behind schedule but the \"user base\" is growing. Better to do it right than crap all over users. Thankfully most distribution packagers are likely going to wait a while given THE VAST NUMBER OF BUGS and unstability of KDE 2.0.0"
    author: "Non Amiss Coward"
  - subject: "Re: Stability?"
    date: 2000-10-18
    body: "<P>>Linux is still gaining users how could KDE \n>\"lose user base\"?? <BR>\n\n<P>Linux != KDE. The two may grow independantly and for different reasons.\n\n<P>>KDE 1.1.2 is a very good and **way** more >stable piece of work\n\n<P>Good point, if this was KDE2.1.2. Compare KDE1.0 to KDE2.0? On or before 2.1.2, KDE will be rock solid in every imaginably way I am sure.\n\n<P>>You obviously haven't been part of a project <BR>\n>where a too early release has **COMPLETELY KILLED** it\n\n<P>No, I strive only for successes @;-P  But seriously, the \"releasing early\" is a two edged blade. Sometimes it is *exactly* what is needed, sometimes it is the death knell. It is not in and of itself a bad thing, however. It's a judgement call.\n\n<P>It seems that a lot of people are spoiled (in a good way) by the stability of the last KDE. What is missing from this realization is that it took a while to get to that point. With KDE2, the programmers and users of KDE have to take the pill associated with any new software: its not as good as something that has had years of work put into simply polishing it.\n\n<P>When KDE2 has been spit polished into 2.1.x, I have a feeling we'll all get to go back to being spoiled KDE users and programmers."
    author: "Aaron J. Seigo"
  - subject: "Re: Stability?"
    date: 2000-10-18
    body: "> try right clicking on a window titlebar and\n> going into the \"Decorations\" ...\n\nThis is not what I mean. This just changes the look of _ALL_ windwos. What I mean is(and you probably know it) setting borderstyles on windows with sertain titles/class etc... like you could in kde-1.1.x\n\n> that's what netscape's for =) konquerer is the\n> only file manager i've ever used that is even \n> half decent as a web browser. in fact, its \n> more than half decent, its 98% decent =)\n\nThis is what I think is the main problem of KDE2 right now. Waiting another few months could make everything 100% decent. Now everything is _almost_ decent. As I said, it's those little things which makes it feel incomplete.\n\n--\nAndreas Joseph Krogh <andreak@nettverk.no>"
    author: "Andreas Joseph Krogh"
  - subject: "Re: Stability?"
    date: 2000-10-17
    body: ">Kwin is more of a version-0.0.1 windowmanager. You \n>can't even set window-decorations(none, border-only, \n>etc..) which should be mandatory for an \n>X-window-manager.\n\nTry kstart --help\n\n> I'm sure this will be fixed in a 2.0.[1|2] version, \n> but why not have it ready in a 2.0.0 version _even_ \n> if it means waiting another 2 months\n\nbecause most of the stuff you mentioned (besides the\ncrashes) are missing features and you can't add new \nfeatures in a freeze."
    author: "Christian Gebauer"
  - subject: "Re: Stability?"
    date: 2000-10-18
    body: "All i have to say is artsd.  the sound server is no where near prime time, and that is part of the core project (I think).  I am using it now also, and I think it's great in many ways, but it's just not stable enough yet for 2.0. my 2cents."
    author: "Matt O"
  - subject: "Re: Stability?"
    date: 2000-10-18
    body: "> Try kstart --help\n\nI have, it's not nearly what I'm looking for, besides - it crashes Kwin a lot.\n\n> because most of the stuff you mentioned (besides the\n> crashes) are missing features and you can't add new \n> features in a freeze.\n\nExactly, but those missing features are what makes it feel complete. I know, of course, that you cannot add features in a freeze, but hence the freeze is to early.\n\n--\nAndreas Joseph Krogh <andreak@nettverk.no>"
    author: "Andreas Joseph Krogh"
  - subject: "Re: LJ Readers' Choice"
    date: 2000-10-17
    body: "Congratulations!!!!!!!!\n\nGO FURTHER!!!!!!!!"
    author: "Manuel Rom\u00e1n"
  - subject: "Great award ... I guess Gnome finished second"
    date: 2000-10-18
    body: "and CDE finished 3rd. Next year who knows?\n\nAssuming CDE will always finish last (since most users have never used it and even if the had would think it sucked) then either Gnome or KDE could finish either first or second!!! Wow I'm excited!!\n\nThere should be a contest for the best \"network transparent windowing system\" too. It would be down to the wire but who would win?? X, VNC, or Berlin????"
    author: "AC Of Course"
  - subject: "Re: Great award ... I guess Gnome finished second"
    date: 2000-10-18
    body: "Hah hah ... best compiler and best C library would be meaningful \"contests\" too."
    author: "Non Amiss Coward"
  - subject: "Re: LJ Readers' Choice"
    date: 2000-10-18
    body: "GREAT!!<br>\nAnother award for KDE. I was REALLY wrong then to think KDE is famous only in German not in US.<br>\nMy only worry now is that from many post here and slashdot there are a lot complain about the beta and RC stability. I personally don't mind if u guys take more time to finish the main things up so that KDE 2 ready for the prime time and delivers what it known best, stability. I haven't try the betas/RCs since I don't want to work and rely on beta quality software. <br>\nI 'll wait for the release and always keep my finger cross. I'll pray for the u guys since I can see many flammers from 'the other side' is waiting for their time."
    author: "jamal"
  - subject: "Re: LJ Readers' Choice"
    date: 2000-10-18
    body: "<br>> My only worry now is that from many post here and slashdot \n<br>> there are a lot complain about the beta and RC stability.\n\n<p>Mmh, I am a bit surprised about such statements since I am running KDE2 for about 3 months now and it is getting better and better every day. For example konqueror crashed in the beginning every 20 minutes, now it is very stable IIRC the last time it crashed was about 3 weeks ago. But for reasons I don't know a few persons (even one on the developer list reported such problems) have still massive problems with kde2 ...</p>\n\n<br>> I'll pray for the u guys since I can see <br>> many flammers from \n'the other side' is waiting for their time.\n\n<p>Sure there are always people who will start flames about every single bug they find. Nevertheless I observed from several threads that the whole discussion KDE vs. GNOME is getting more and more rational. For example the last thread in the c't (a german computer magazine) contained no flames kde vs. gnome (only some linux vs. win). </p>\n\nciao, Marco"
    author: "Marco Krohn"
  - subject: "Re: LJ Readers' Choice"
    date: 2000-10-18
    body: "<br>> My only worry now is that from many post here and slashdot \n<br>> there are a lot complain about the beta and RC stability.\n\n<p>Mmh, I am a bit surprised about such statements since I am running KDE2 for about 3 months now and it is getting better and better every day. For example konqueror crashed in the beginning every 20 minutes, now it is very stable IIRC the last time it crashed was about 3 weeks ago. But for reasons I don't know a few persons (even one on the developer list reported such problems) have still massive problems with kde2 ...</p>\n\n<br>> I'll pray for the u guys since I can see <br>> many flammers from \n'the other side' is waiting for their time.\n\n<p>Sure there are always people who will start flames about every single bug they find. Nevertheless I observed from several threads that the whole discussion KDE vs. GNOME is getting more and more rational. For example the last thread in the c't (a german computer magazine) contained no flames kde vs. gnome (only some linux vs. win). </p>\n\nciao, Marco"
    author: "Marco Krohn"
  - subject: "Re: LJ Readers' Choice"
    date: 2000-10-18
    body: "<br>> My only worry now is that from many post here and slashdot \n<br>> there are a lot complain about the beta and RC stability.\n\n<p>Mmh, I am a bit surprised about such statements since I am running KDE2 for about 3 months now and it is getting better and better every day. For example konqueror crashed in the beginning every 20 minutes, now it is very stable IIRC the last time it crashed was about 3 weeks ago. But for reasons I don't know a few persons (even one on the developer list reported such problems) have still massive problems with kde2 ...</p>\n\n<br>> I'll pray for the u guys since I can see <br>> many flammers from \n'the other side' is waiting for their time.\n\n<p>Sure there are always people who will start flames about every single bug they find. Nevertheless I observed from several threads that the whole discussion KDE vs. GNOME is getting more and more rational. For example the last thread in the c't (a german computer magazine) contained no flames kde vs. gnome (only some linux vs. win). </p>\n\nciao, Marco"
    author: "Marco Krohn"
  - subject: "Re: LJ Readers' Choice"
    date: 2000-10-18
    body: "Do you allways post three times:-) ?\n\n--\nAndreas Joseph Krogh"
    author: "Andreas Joseph Krogh"
  - subject: "Normaly not but..."
    date: 2000-10-18
    body: "this posting was so important that I decided to post it 3 times ;-)\n\nSeriously I had a problem with konqui (or konqui had a problem with his stupid user :-). Nevertheless I pressed \"add\" and nothing happened for about 30 secs so I pressed \"add\" again and nothing happened again. I thought let's do it a third time and this time I got the message that the message has been sent. \n\nSorry for that ...\n\nMarco, who hopes that this article appears only once\n\nand Konqui, who hopes to find a more patient user ..."
    author: "Marco Krohn"
---
I'm pleased to announce that KDE received the
Linux Journal Readers' Choice award for
<EM>Favorite Desktop Environment</EM> at the Atlanta Linux Showcase.  You
can <A HREF="http://www.andamooka.org/~dsweet/kdeaward.jpeg"> take a look at the award poster</A>.  Congratulations everyone!


<!--break-->
