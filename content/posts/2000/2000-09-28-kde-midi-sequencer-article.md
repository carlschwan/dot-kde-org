---
title: "KDE MIDI sequencer article"
date:    2000-09-28
authors:
  - "raphinou"
slug:    kde-midi-sequencer-article
comments:
  - subject: "Looks like a nice thing!"
    date: 2000-09-28
    body: "Now if only I had any musical ear :-)"
    author: "Roberto Alsina"
  - subject: "Excellent"
    date: 2000-09-28
    body: "It's going to be great hooking up my Yamaha P80 to this!  Woohoo!  The big problem with the P80 is that it doesn't output MIDI data for the built-in sequencer (nor demos).\n\nNow if I could only play more than 3 decent piano pieces... ;)\n\nCheers,\nNavin.\n\nPS Does anyone know if there's any ear/music/sightreading training software for Linux?\n"
    author: "Navindra Umanee"
  - subject: "Re: Excellent"
    date: 2000-09-29
    body: "Navindra: \nGo to Dave Phillip's Linuxsound site at http://www.bright.net/~dlphilp/linuxsound/ and click on \"Musicians Utilities\" and you will find some stuff.\n\nHe have most everything that has to do with sound on Linux listed there."
    author: "reihal"
  - subject: "Re: Excellent"
    date: 2000-09-29
    body: "I've gone through that site several times in the past.  It's obviously THE Linux sound site, very comprehensive and all, but at the time I didn't find what I was looking for.  I suppose it's time to go take a look again.\n\nThanks,\nNavin."
    author: "Navindra Umanee"
---
Solomon wrote in to tell us that <I>"The <a href="http://www.crosswinds.net/~linuxmusic/">Linux MusicStation</a> currently has a <a href="http://www.crosswinds.net/~linuxmusic/anthem.html">feature article</a> on <a href="http://anthem.sourceforge.net">Anthem</a>, a developing KDE2 MIDI sequencer. They will also be doing an interview with the author next month, so get the <a href="mailto:linuxmusic@crosswinds.net">questions</a> in."</I>  Screenshots <a href="http://anthem.sourceforge.net/screenshots.html">here</a>. Anthem is based on the <a href="http://tse3.sourceforge.net/">TSE3</a> open source sequencer engine and is written by Pete Goodliffe.
<!--break-->
