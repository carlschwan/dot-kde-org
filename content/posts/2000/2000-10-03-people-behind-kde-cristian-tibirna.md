---
title: "People Behind KDE: Cristian Tibirna"
date:    2000-10-03
authors:
  - "numanee"
slug:    people-behind-kde-cristian-tibirna
comments:
  - subject: "Bienvenu au Canada"
    date: 2000-10-03
    body: "Il y a de bels endroits au Qu\u00e9bec aussi vous savez!! (allez voir la partie est de la rive nord du Saint-Laurent surtout).\n\nSi vous avez le temps vous pouvez m\u00eame lire un peu d'histoire canadienne ... ou l'\u00e9couter \u00e0 la TV puisque Pierre Elliot Trudeau est mort et il ya tant d'\u00e9loges, retrospectives, et tout \u00e7a. C'\u00e9tais une grand Qu\u00e9becois et un grand Canadien ... il aurait aim\u00e9 ton histoire surtout.\n\nExcuse-moi mon fran\u00e7ais - je suis anglophone de l'ouest (C.B.) :-)"
    author: "AC Of Course"
  - subject: "Re: Bienvenu au Canada"
    date: 2000-10-03
    body: "No he entendido nada. Te iba a decir que hables en ingl\u00e9s, pero bueno... yo estoy escribiendo en castellano y no pasa nada.\n\nBuena suerte.\n\nHERE WRITE IN ENGLISH PLEASE :-):-):-):-):-):-):-):-):-)\nI understand english and italian, but french no. Sorry."
    author: "Manuel Rom\u00e1n"
  - subject: "Hah hah"
    date: 2000-10-03
    body: "se puede que Cristian entiende franc\u00e9s entonces no me importa si ofendo a los d\u00e9mas  :-P\n\n:-D :-D"
    author: "AC"
  - subject: "Re: Hah hah"
    date: 2000-10-04
    body: "no ofendes a nadie\nme parece muy bien que se ponga franc\u00e9s o el idioma que sea, me gustan mucho. Desde hace un a\u00f1o me interesan los idiomas y ya entiendo 2, ingl\u00e9s e italiano, aunque, de momento, no s\u00e9 hablarlos.\nMi empresa es francesa y... haber si aprendo algo.\nLo malo es que puede haber gente reacia a este tipo de acciones; a m\u00ed, si me ponen alem\u00e1n...\n\nA sido un placer poder comunicarse con alguien en un foro de estos en el idioma en el que me puedo expresar con orgura.\n\nSuerte a todos y a The KDE Project y todas sus ramas.\n\nManuel Rom\u00e1n\nmanuelrg@telepolis.com"
    author: "Manuel Rom\u00e1n"
---
<a href="mailto:tink@kde.org">Tink</a> is back  with a brand new edition of <a href="http://www.kde.org/people/people.html">The People Behind KDE</a>.  This week, we learn more about the omnipresent and ever enthusiastic <a href="http://www.kde.org/people/cristian.html">Cristian Tibirna.</a> <i>"I was on the lyx lists when Matthias Ettrich started it in October 1996. His ideas caught me bad. After finishing some exams at beginning of 1997, I got involved with coding (kwm's smart placement and magnetic borders algorithms) and I started to do a lot of user support on the mailing lists."</i>

<!--break-->
