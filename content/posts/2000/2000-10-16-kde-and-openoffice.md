---
title: "KDE and OpenOffice"
date:    2000-10-16
authors:
  - "numanee"
slug:    kde-and-openoffice
comments:
  - subject: "It's going to be interesting"
    date: 2000-10-16
    body: "<P><FONT SIZE=2>The OpenSource world can be really strange. On of the main</FONT> <A HREF=\"http://www.kde.org/gallery/index.html#dalheimer\"><FONT SIZE=2>develope</FONT></A><FONT SIZE=2>r who helped to port StarOffice to Linux is now programming for </FONT><A HREF=\"http://www.kde.org/gallery/index.html\"><FONT SIZE=2>KDE</FONT></A><FONT SIZE=2>. OpenOffice is mainly written in C++ and should be integrated into Gnome which is programmed in C. So you have to deal with </FONT><A HREF=\"http://erik.bagfors.nu/gnome/languages.html\"><FONT SIZE=2>language bindings</FONT></A><FONT SIZE=2>. I especially like the fact that a lot of comments in OpenOffice seams to be in </FONT><A HREF=\"http://www.openoffice.org/dev_docs/source/source_overview.html\"><FONT SIZE=2>German</FONT></A><FONT SIZE=2>.</P>\n\n<P>We will see if OpenOffice has the potential to attract new developers. Working with a new and more advanced project like KOffice / KParts could be more appealing (who likes projects with 20h compile time?).</P>\n\n<P>But OpenOffice could attract a lot Beta Testers (does anyone know how I can crosscompile the Linux Version on my companies AIX servers?).</P></FONT>\n\n\n"
    author: "Andreas Trawoeger"
  - subject: "Re: It's going to be interesting"
    date: 2000-10-16
    body: "If you like the fact that a lot of the comments are in German, I wonder if you have come across the names and locations of the many OpenOffice project leaders. (Hint: there's many a Michael, Martin and Matthias there - or to stop kidding: the project leaders are no doubt original -german- StarOffice employees.)"
    author: "Case Roole"
  - subject: "Re: It's going to be interesting"
    date: 2000-10-16
    body: "I'm not very into the KOffice development. But as I've seen a very good documented XML-File-Format for almost everything concerning a office suite at OpenOffice (over 350 pages of definitions, etc.). It maybe would be worth thinking of KOffice adopting the file formats from OpenOffice.\nThat would be no harm to KOffice because only the format changes not the application. The benefit is a common file-format and the freedom of choice. One could argue that OpenOffice could adopt the ff of KOffice as well but I think this is a hopeless vision. I see the danger that there will be the same useless war between linux office suites as we saw at the desktop (where KDE is the winner of course). Even I agree with some arguments for KOffice I would probably choose StarOffice because I use it at work an a WIN machine and I think many other do so as well. If KDE doesn't participate on OpenOffice I see the danger that I sometime have to choose GNOME to use my favorite Office, which I really don't want to."
    author: "Fritz Wepper"
  - subject: "StarOffice XML file formats"
    date: 2000-10-16
    body: "Interesting, I hadn't noticed that.  Thanks for pointing it out.  Definitely looks like another thing KOffice might be able to adopt or at least support in the future.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: It's going to be interesting"
    date: 2000-10-16
    body: "This is a very interesting comment.  It would help for someone to follow up who knows more about the file formats of OpenOffice (someone from Sun) or from KOffice like Shaheed.  I'm a fairly new KOffice developer but I'm not familiar with all the KOffice file formats yet. Several points need to be clarified and several questions are raised.\n<p>\n*Actually the file format (DTD) does or should define or at least influence how the application works if both are designed well.  The app should be build around the file format if it is to work efficiently, because the document structure is defined by the native file format anyway, at least in KOffice.\n<p>\n*KOffice has file formats for all its major components.  You say that OpenOffice also has DTDs for XML file formats.  Have these already been implemented in the current Star Office or are they just proposals for the new version?  If they aren't set in stone yet then perhaps there is room for give and take between KOffice and OpenOffice in defining common or more compatible file formats for the different document types.  It should not just be a matter of KOffice adapting to what OpenOffice proposes. Give and take should work both ways.\n<p>\n*At the very worst, even if the file formats are different, the fact that both use XML and are open makes translation between them fairly easy, much easier than translating betweeen MS Office formats and KOffice.\n<p>\n*Finally, regardless of differences between the file formats and office suites, the success of OpenOffice can only help KOffice and visa versa, at least in the near future.  More users of Linux and other free unix systems means more users for both office suites, and less of a lock on file formats by closed source vendors like Microsoft."
    author: "John Califf"
  - subject: "Re: It's going to be interesting"
    date: 2005-08-17
    body: "happy brithday  to you and fritz l love you and you love me\n"
    author: "mariella cremona"
  - subject: "Re: It's going to be interesting"
    date: 2005-10-04
    body: "fritz  wepper   i  love my letters  with  she tell me is my  responding                 fritz  i love you and you  love  me  fritz she  love me  and she love me    fritz with my love  to my  kiss  i love  so much  my  beautiful   sexy man"
    author: "mariella   cremona"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-16
    body: "this is the base of the opensource"
    author: "Manuel Roman"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-16
    body: "I agree fully - there are a number of things that KOffice has that OpenOffice could learn from (e.g. XML file formats).  I'm definitely hoping that the existing KOffice apps will continue and evolve/improve - I'd hate to see them \"buried\" by OpenOffice ...\n ( I had similar concerns about the Gnome spreadsheet Gnumeric, but they (at gnome.org) laid these to rest - Gnumeric development is to continue.. Good to hear, as I believe both office suites (Gnome and KDE) are well thought-out and designed."
    author: "Andy Elvey"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-16
    body: "KOffice already uses XML file formats (though they are generally saved to disk gzipped), though I agree it would be nice to use the same DTD."
    author: "Kevin Puetz"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-16
    body: "<p>Hi,</p>\n<br><p>\nIsn\u00b4t it ironic? 9 million lines of C++-Code\nand it is supposed to become a gnome application.\n</p><p>I am also amused by the fact that the kde team\ndoes not have to deal with this monster, but can \ninstead rip all the interesting parts out of it.\n</p>\n<p>\nMatthias<br>\n-- <br>\n<a href='http://www.ireland.com/includes/webcam/liveview.jpg'>http://www.ireland.com/includes/webcam/liveview.jpg</a>"
    author: "Matthias Lange"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-16
    body: "So what's wrong with that?\nGnome might not be the best in C++, but it's still very good."
    author: "Anonymous"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-16
    body: "There is nothing wrong with that. It's just ironic, that's all. As pointed out further above in this discussion, the irony goes even further.\n\nStar Office is a really large chunk of code. Much of this code is not really related to an office package, such as the operating system independent windowing subsystem in their MDI window, many widgets that would be useful in other applications, font and printer handling, HTML viewing and processing, a script interpreter and an interface to it and the like. Such code does not actually belong into an application, but into a desktop environment. That way it becomes useful not only to a single application, but to all applications being written for that desktop environment.\n\nThe Open Office project and Gnome can now choose to incorporate this code into their desktop, turning Gnome essentially into KDE (see below).\n\nOr they choose to drop this code from Open Office, and importing the related Gnome components into Open Office, leading into the murky area of C/C++ code integration.\n\nOr they can choose to integrate Open Office and Gnome only superficially, duplicating essential parts of their infrastructure and bloating a project even more that is already suffering enormously from bloat.\n\nIf I remember correctly, back when Kalle & Co were still working at StarDiv there was some discussion in the german Linux newsgroups about the major fault of Star Office. That fault was that Star Office tried to duplicate large parts of what makes up a desktop environment, but did so inside a single application. That was a pity, as this code became largely inaccessible to other applications. Star had an offer selling their multiplatform windowing toolkit as a commercial product, but it was even less popular as Qt at that time. \n\nStar Office had to carry around all this code in order to be useful on any Unix system, as there was no support for large desktop applications at that time on such systems - there was neither KDE nor Gnome, just Motif or even worse toolkits. Looking at the SO code tells you very precisely what the advantage of the Windows API over Motif was for application developers at that point in time.\n\nA number of the people working at Star Division at that time became the core of the KDE project, and many lessons they learned doing Star Office influenced their design of vital parts of what is now KDE. Of course KDE is now much larger than that and the technologies making up KDE now have partially transcended what makes up Star Office, so the analogy is stretched pretty far. \n\nBut: If you take apart Star Office and integrate what is useful in it into any Unix desktop project, you end up with something that would be very hard to distinguish from KDE now. Integrating Star Office into Gnome will turn Gnome into KDE, and that /is/ ironic."
    author: "Kristian K\u00f6hntopp"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-16
    body: "Whether or not this is ironic depends on how you see the split between KDE and GNOME.  Certainly one aspect of the split has been C++ versus C, so there is something to this.\n\n<p>However, I think you are a little off-base when you say that the GNOME frontend will be \"turning Gnome essentially into KDE\".\n\n<p>Instead of comparing OpenOffice's C++ framework to KDE, a better comparison would be to Abiword.  They've abstracted away the differences between platforms.  This abstraction won't be introduced to GNOME, it will just have a GNOME implementation added.  The same could be (should be?) done for KDE.\n\n<p>So your last paragraph is <strong>pretty much completely false</strong>.  Having worked on Abiword a bit, the abstraction layers can make the code challenging to follow, but the rewards are significant.\n\n<p>What is really amusing is that there still (after all this time) exists enough bile for people to go looking for irony."
    author: "John Tunison"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-17
    body: "Bile? Surely not. But amused I am. I think of it more as one of the backstage jokes of history (don't know the proper english ideoms here).\n\nIt will be interesting to see how the Gnome people handle this, which path they choose to integrate some sort of proto-KDE code into their project.\n\nAnother abstraction layer inside Star Office only surely won't be a good solution - Star Office contains a lot of code that would be useful outside Star Office as part of the desktop in order to make it available to all applications. If they handle it efficiently, this technology could probably be useful to some hypothetical Gnome-KDE-integration project (and how is that for funny?)."
    author: "Kristian K\u00f6hntopp"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-17
    body: "It's not another abstraction layer, it's the abstraction layers already in StarOffice.  Which of course doesn't mean that it will be efficient -- just no more bloated than it already is, and depending on how much bloat is in their render-to-X layer, possibly less bloated.\n\nYour tenacious grip on the \"proto-KDE\" concept confuses me.  If someone made a C-based KParts component, would you say that KDE was integrating \"proto-GNOME\" code?  Call a thing by what it is.  Using unwarranted \"group A vs. group B\" words just stirs the trolls.\n\nBut at least this is more moderate than \"you end up with something that would be very hard to distinguish from KDE now\", which -- while perhaps amusing -- is true only in the most superficial sense."
    author: "John Tunison"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-28
    body: "I didn't quite understand it at first either, but look back to the original article and it says:<p>\n\n<i>A number of the people working at Star Division at that time became the core of the KDE project, and many lessons they learned doing Star Office influenced their design of vital parts of what is now KDE. </i><p>\n\nSo what he means (I think) is that because a lot of the original KDE core developers where from Star Division, that the KDE architecture resembles (understandably) a \"next generation\" Star Office.  So with Star Office seeming like a prototype KDE, GNOME adoption of SO could be seen as KDEing GNOME.  This of course assumes that GNOME attempts to absorb the architecural aspects of SO, which I don't think they would, the've invested too much into BONOBO and other object based componenty to just chuck it out and use a SO framework.  I see a more likely scenario as Galeon, taking the engine of OpenOffice and throwing away the framework."
    author: "Tsujigiri"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-18
    body: "\u00ab It will be interesting to see how the Gnome people handle this, which path they choose to integrate some sort of proto-KDE code into their project. \u00bb\n\nWhich path ? Just plain old conversion and/or binding. There is actually large quatities of KDE code in GNOME, more or less transformed. For example, the GTKHTML widget is a GTK port of KHTMLW, KFM's HTML viewer..."
    author: "Ga\u00ebl"
  - subject: "Re: KDE and OpenOffice contest"
    date: 2004-01-14
    body: "For a new installation on a MAC G4, should Linux be set up in a separate partition for a dual boot?\nSo which wordprocessor is best, most complete and closest to MAC OSX AW6?\nWhich WP will support the MAC variety of fonts?\nWhich WP has a tool bar to do all of the MAC frame tricks?\nWhich has or will support an integrated mail merge and data base app?\nOr, should I stick with my troublesome AW6?\nThanks,  Marv"
    author: "Marv"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-16
    body: "I agree that KOffice should pick at the source, implementing any use full snips, rather than port Open Office to KDE/Qt.\n\nI'm just wondering though - with the iminent release of KDE 2.0, how with this affect KOFfice, I surely hope than they will be a way to update KOffice with out recompiling the Full KOffice - maybe it can be done through <i>KFilters</i>...\n\nThis maybe out of context but I feel it would be kool if Organiser was intergrated into Kmail making kick-ass app.  I don't know upcoming KDE apps of this nature but it would be a pitty to re-design the already <i>working</i> wheel, I trullu beleive that with the KDE Widget set we can make a way better Email/Colaboration client that what Evolution could ever be!\n\nNo Flames but I happen to prefer the KDE widgets to Gnome - no offence."
    author: "Samuel Mukoti"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-16
    body: "Hi, I do a fair amount of KMail coding.\n\nAfter I finish implementing non-blocking sending and IMAP support (in a few months) in KMail. I plan to talk to Cornelius Schumacher the KOrganizer maintainer and the KNode Authors (Christian Gebauer, Dirk Mueller ...) about creating a new groupware application.\n\nNo doubt they're very busy, and I don't expect them to contribute directly to this new application. Instead I just want to make sure they are generally ok with the concept and are willing to look at patches so that code can be reused instead of duplicated.\n\nHopefully Rik Hemsley and I can work together on this kind of application. We were working on this kind of thing but the timing just wasn't right.\n\nSo basically I'm saying I support the idea of a KDE groupware application built (at least partly) out of current working components, and are planning to contribute my time to make it happen.\n\nBFN,\nDon."
    author: "Don Sanders"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-16
    body: "Hi Don!\n\nNo pun intended, but you still are duplicating code... I'm sure you have heard of Magellan. What about it, why don't you guys think of merging (parts of) it? Both groups have exactly the same aims, so this really should be possible. I know Magellan doesn't yet use any KParts, but that could be changed...\n\nWhy not?\n\n   ben"
    author: "Ben Adler"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-16
    body: "Magellan has been in limbo for the last 6 months.  It's one of those projects that started out as a great idea then fizzeled out.  Would love to see a working copy of Magellan.<p>\nThe idea of a KParts based groupware application is very appealing, if you don't like the contact manager that's being used swap it out with another KParts component that does what you like.  There's a lot of potential for something like this.<p>\nRick"
    author: "Rick"
  - subject: "Magellan is not dead"
    date: 2000-10-16
    body: "...in fact, theKompany has people working on it. It should release a preview of Magellan in the coming weeks, I've heard."
    author: "Eron Lloyd"
  - subject: "Re: Magellan is not dead"
    date: 2000-10-16
    body: "Looking forward to seeing a tarball from them."
    author: "Rick"
  - subject: "next kmail improvements!"
    date: 2000-10-16
    body: "Please could someone add also maildir support to kmail? Please? please..\n\nIMAP is nice, but I don't use it over my modem!\n\nAt the moment my maildir prevents me from even looking at kmail, as it wants to make a file where my maildir inbox is laying."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: next kmail improvements!"
    date: 2000-10-16
    body: "I'm in the same situation: still using mutt because my mailboxes are in Maildir format.\nBut it's better to have IMAP support first. Remember that once IMAP is working, you'll also be able to use your Maildirs through an IMAP server. Courier IMAP is an excellent Maildir-only IMAP server."
    author: "Wilco Greven"
  - subject: "Re: KDE and OpenOffice"
    date: 2000-10-16
    body: "I'm not a developer, but have a keen interest in both products as a user. At this point, working with older technology (200 mhz laptop, 466mhz desktop) I am definitely interested in a lighter architecture. KDE fits the bill really well. I'm hoping KOffice does, too. I'm using StarOffice on my desktop, but had to de-install it from my laptop because it was way too slow. While I like the StarOffice functionality, it's monolithic architecture is very annoying due to hard drive bloat & resource consumption. When all I want to do is check email & write a quick memo, I have to deal with a lot of baggage in Star Office. My guess is a lot of other users have requirements similar to mine, and I'm looking forward to KOffice as a legitimate contender to StarOffice."
    author: "Rob Whall"
  - subject: "KOffice code -> open office code"
    date: 2000-10-16
    body: "the openoffice.org site states that all openoffice.org code will be dual licensed under the GPL <b>and</b> the SISSL.\n\n<P>would this not block koffice code being put into openOffice, since it would be relicensing it under the SISSL? or am i completely reading the openoffice.org documentation wrong?"
    author: "Aaron J. Seigo"
  - subject: "Why is that a problem??"
    date: 2000-10-16
    body: "Why is that a problem???  Licensing has never bothered the KDE project before..."
    author: "JustMe"
  - subject: "Re: Why is that a problem??"
    date: 2000-10-16
    body: "It's a problem since you can't simply re-license other people's code w/out asking them. As an added \"boot\", the SISSL is much more restrictive than the GPL and can be changed by Sun Microsystems at any given moment in any given way.\n\n<P>So, to put KOffice code into openOffice.org (their is a distinction between openOffice, which could be forked under the GPL, and openOffic.org), the author of the code would need to give permission to have it licensed under the SISSL.\n\n<P>From openOffice to KOffice would be no problem since one can simply choose the GPL.\n\n<P>Just my two pennies."
    author: "Aaron J. Seigo"
  - subject: "Re: Why is that a problem??"
    date: 2000-10-16
    body: "I think you're confusing the SCSL with the SISSL.  You should go to www.openoffice.org and read the webpages there."
    author: "John Tunison"
  - subject: "Re: Why is that a problem??"
    date: 2000-10-16
    body: "Like I asked before..Why is this a problem??\n\nIt never bothered the KDE project to use code covered by the GPL with QT 1.x....."
    author: "JustMe"
  - subject: "Re: Why is that a problem??"
    date: 2000-10-17
    body: "Think you're the one with the problem, mate."
    author: "Ezz"
  - subject: "GNOME perspective"
    date: 2000-10-16
    body: "They are not happy with SO either: <a href=\"http://news.gnome.org/gnome-news/971624694/index_html\">http://news.gnome.org/gnome-news/971624694/index_html</a>\n<p>\nGNOME Office has been killed and now GNOME is left without anything and all bark but no bite."
    author: "ac"
  - subject: "Re: GNOME perspective"
    date: 2000-10-16
    body: "For any out there that don't recognize this as the complete nonsense it is:\n\n<ol>\n<li>\nGNOME office has not been killed, it's being worked on by folks at Helix and Sun.\n\n<li>The <a href=\"http://news.gnome.org\">news.gnome.org</a> is plagued by postings from clueless people even more than <a href=\"http://dot.kde.org\">dot.kde.org</a> is. \n</ol>"
    author: "John Tunison"
  - subject: "Re: GNOME perspective"
    date: 2000-10-16
    body: "1. Is this why there is no traffic on the GNOME   Office list?  GNOME Office has become a closed and commercial project now?  Please elaborate as I am confused.  I have been trying to follow GNOME office and now there is nothing.\n<p>\n2. Your statement proves nothing, you are just grasping for straws.  Why don't you defend your GNOME office on your own GNOME site?  Would you call Christian S., Havoc P., Miguel I., etc all clueless?  Why aren't they giving any explanations?  These are all people I have read postings from on the news.gnome.org site."
    author: "ac"
  - subject: "Re: GNOME perspective"
    date: 2000-10-17
    body: "1. Are you insane? Just because HelixCode is a company it is aiming at world domination?\nGnome Office is free software and it will stay that way!\n\nYour post already shows how stupid a lot of people are.\nAlways spreading negative comments about companies while they don't even know what's going on and how things work..."
    author: "Anonymous"
  - subject: "Access Denied"
    date: 2000-10-17
    body: "Following the link now brings up an \"Authorization is required for Zope at news.gnome.org\" dialog prompting for a username and password to be entered.\n\n\nI guess it must have been a good read."
    author: "Another AC"
  - subject: "This is complete nonsense"
    date: 2000-10-17
    body: "Just because ONE, and ONLY ONE person said AbiWord MIGHT be dead the whole Gnome Office project is dead?!?!?!?!?"
    author: "Anonymous"
  - subject: "Re: This is complete nonsense"
    date: 2000-10-17
    body: "http://mail.gnome.org/archives/gnome-office-list/"
    author: "ac"
  - subject: "Re: This is complete nonsense"
    date: 2000-10-17
    body: "Look at the CVS ChangeLogs!\nGnome Office is far from dead!"
    author: "Anonymous"
---
Sun Microsystems, probably best known at the KDE end of the spectrum for their <a href="http://www.sun.com/research/technical-reports/1994/abstract-29.html">influential paper</a> on local vs distributed computing, recently open-sourced StarOffice, making it freely <a href="http://www.openoffice.org/">available</a> to KDE as well as other projects.  What impact will this have on KDE and KOffice?  Ideally, even though OpenOffice is technically a competitor to KOffice (Sun <a href="http://www.theregister.co.uk/content/1/12585.html">apparently denies this</a>) and despite the naysayers, OpenOffice will be of sizable benefit to KDE.  Read on for a few of my initial thoughts on the matter.









<!--break-->
<p>
While OpenOffice is certainly a huge contribution to the free software community, it will likely take a certain amount of effort to integrate it well into today's free desktops[<a href="#footnotes">1</a>].  Judging by the <a href="http://www.openoffice.org/white_papers/tech_overview/tech_overview.html">technical overview</a>, OpenOffice contains and is based on a huge chunk of inhouse technology, implying that there are ways to go before a port to a free desktop environment that is compatible with all the goals of the project is complete. There is no immediate need for such a rewrite, of course, as OpenOffice is already useful and porting existing working code will be a significant task with little or no actual benefit for the applications proper. In fact, a reckless rewrite ala Mozilla might be dangerous from a market perspective, so <a href="http://joel.editthispage.com/stories/storyReader$47">caution</a> is advised. In the interim, OpenOffice <a href="http://www.linuxplanet.com/linuxplanet/previews/2470/1/">appears</a> slick and polished enough to pass for a KDE1 application and that could be sufficient for now.
<p>
In contrast, KOffice, was built from the ground up for KDE[<a href="#footnotes">2</a>], and is fully integrated with native KDE component and related technologies. It may actually be easier to import useful code and technology from OpenOffice to KOffice than to rewrite OpenOffice. In fact, Shaheed Haque, one of our filter experts, lost no time at all in <a href="http://lists.kde.org/?l=koffice&m=97152630400684&w=2">investigating</a> the Microsoft Word filter code.  At first glance it appears it will be very useful as a basis for improving KOffice's own filter code.  Almost just as promptly, Matthias Elter <a href="http://lists.kde.org/?l=kde-core-devel&m=97160659824977&w=2">discovered</a> a set of free high quality TrueType fonts that could come in useful for KDE as a whole.  Time will tell how much more technology can be imported, and whether KDE can make good use of all the work that has gone into or will go into OpenOffice.
<p>
At the same time, OpenOffice could certainly borrow technology from KOffice.  For example, they could consider adopting (and possibly refine in collaboration) the KOffice formats which already use XML and which solve some of the same issues that OpenOffice.org is currently <a href="http://lists.kde.org/?l=koffice&m=97163189925663&w=2">attempting</a> to address.  This is a wonderful opportunity for collaboration between OpenOffice.org and the KOffice/KDE community.
<p>
In conclusion, while OpenOffice is a mature product, it has a major rewrite and redesign in view if it is to compete on the Unix desktop.  KOffice, on the other hand, is already fully integrated with one of the major free Unix desktops but is relatively new with an imminent first public release.  Some may argue that one has the advantage over the other in the context of the Unix desktop, but ideally, if this plays out for the best, both KOffice and OpenOffice will see huge and mutual benefits, and both will prosper.  Meanwhile, the <a href="http://koffice.kde.org/">KOffice</a> project could certainly use your help, contributions and input.
<p>
Opinions?
<p>
<a name="footnotes">
[1] One of the stated and hyped goals of the OpenOffice project is a port to GTK+/GNOME technology.
<p>
[2] While KOffice does use KDE technology, one is not forced to use the desktop environment to run KOffice.








