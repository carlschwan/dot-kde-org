---
title: "KDE Gets Digital Camera Support"
date:    2000-12-14
authors:
  - "Dre"
slug:    kde-gets-digital-camera-support
comments:
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-13
    body: "This is an excellent example of how the architecture of KDE will accelerate the inclusion of new and diverse technologies, without having to recompile or patch existing applications to take advantage of it.\n\nKDE, showing the rest of the programming world how it can be done."
    author: "Wickyd"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-13
    body: "Without having to patch?\n\"theKompany has just released a PATCH to the well known digital camera application gPhoto2\""
    author: "Anonymous"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-13
    body: "Without having to patch *KDE 2* apps which use kio_slave."
    author: "Greg Smethells"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-13
    body: "Patch is the wrong word.  We didn't do anything to the libraries, we added the front end to the libraries.  Thanks to the fine work of the gPhoto folks, the new gPhoto2 makes this very easy.  Keep in mind that gPhoto2 is still not production release and we have no control over that, but at least it's available and we have been having good luck with it internally."
    author: "Shawn Gordon"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-13
    body: "Wowo!\n<p>\ntheKompany.com is super :) kivio also looks very nice, I compiled it...\n<p>\nCan anyone recommend a good digital camera that is cheap? Thanks.\n<p>\nJason\n<p>\nPS: Go to <a href=\"http://borgtheme.katzbrown.com/\">borgtheme.katzbrown.com</a> for a new KDE2 theme I made!"
    author: "Jason Katz-Brown"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-13
    body: "For a great site on unbiased reviews take a look at http://www.steves-digicams.com/.  Also check http://computers.cnet.com/hardware/0-1078.html?tag=st.ce.1629008.more.1078 for reviews and good comparitive pricing.  I really like http://www.compdirect.com/ for good bargains on cameras.  Probably one of the best for the money is the Olympus D-360L."
    author: "Shawn Gordon"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-13
    body: "for a start, go browse the ng rec.photo.digital."
    author: "Lee Wee Tiong"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-14
    body: "&lt;disclaimer&gt;I have no connection with this company, other than being a customer&lt;/disclaimer&gt;\n<br>&nbsp;<br>\nFor those in the UK, you might like to try:\n\n<br>&nbsp;<br>\n<a href=\"http://www.scan.co.uk/\">scan.co.uk</a>\n<br>&nbsp;<br>\n\nI recently bought the e-Studio camera for about 40 quid.  You aren't going to be using it to take wedding pictures or anything, but it is a great little toy and doubles as a web cam too.\n<br>&nbsp;<br>\n<a href=\"http://www.scan.co.uk/today.htm\">scan.co.uk/today.htm</a>\n<br>&nbsp;<br>\nlists daily \"specials\"...\n<br>&nbsp;<br>\nEnjoy,\nMurray.\n<br>&nbsp;<br>\nps. rock on KDE, gphoto, theKompany, et al"
    author: "Murray"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2001-11-24
    body: "scan e studio digital camera\n i bought the following model however i have miss laid the drivers if anyone out there has them please sent them to me thank you"
    author: "raj"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2002-11-13
    body: "can someone pls let me know where I can get the drivers as well - pls email me with the info"
    author: "Ad"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-13
    body: "Good Job!\n\nI think it is about time to forget the animosities between GNOME and KDE and start working together, each project in its own unique style, to gain from one another's work!"
    author: "Diedrich Vorberg"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-13
    body: "Now wait for the Gnome guys to send a patch to gPhoto for use with gnome-vfs and the ring is complete :)"
    author: "Anonymous"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-13
    body: "Gnome will lose this game.... not enough manpower to do the same in 5 days from now.\n\nwait for gnome 2... than they will have a small chance to get closer to kde...   it seems europe's kde-desktop is winning the contest over gnomes usa-team\n\neurope goes on top"
    author: "steve"
  - subject: "Hello troll."
    date: 2000-12-13
    body: "."
    author: "Anonymous Coward"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-14
    body: "Looks like the trolls have found dot.kde.org..."
    author: "Anonymous"
  - subject: "Yet another ignorant troll..."
    date: 2000-12-14
    body: "It has already been done a month ago!\nNow what did you say about \"not enough manpower\"?"
    author: "TrollKiller"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-14
    body: "Acutally, this has already been done:\n<BR>\n\n<A HREF=\"http://lists.helixcode.com/archives/public/gnome-vfs/2000-November/000169.html\">http://lists.helixcode.com/archives/public/gnome-vfs/2000-November/000169.html</A>"
    author: "Anonymous"
  - subject: "What about a Gimp Patch?"
    date: 2000-12-13
    body: "Great!, now just only if someone made a patch, which makes Gimp 100% intergrated with Koffice and KDE2...KDE would seriously ROCK!"
    author: "t0m_dR"
  - subject: "Re: What about a Gimp Patch?"
    date: 2000-12-13
    body: "You should look at the work that John Califf is doing with Krayon, it is really looking good and is much more user friendly than GIMP IMHO.\n\nFor those of you who aren't aware, Krayon was formerly KImageShop and is part of KOffice."
    author: "Shawn Gordon"
  - subject: "Re: What about a Gimp Patch?"
    date: 2000-12-14
    body: "Krayon sounds good, but where can I get it?"
    author: "Xiang"
  - subject: "Re: What about a Gimp Patch?"
    date: 2000-12-14
    body: "<p>Krayon (the former KImageShop) is documented and may be downloaded here:</p>\n<p>&nbsp;<a href=\"http://www.koffice.org/kimageshop/\">http://www.koffice.org/kimageshop/</a></p>\n<p>Perhaps you also might want to read this interview (October 2000):</p>\n<p>&nbsp;<a href=\"http://www.linux.org/people/gordon.html\">http://www.linux.org/people/gordon.html</a></p>\n<p><i>And as allways:</i></p>\n<p>Just in case you happen to consider starting a nice little flame war, please be a good (boy|girl) and look at this text first.</p>\n<p><a href=\"http://home.snafu.de/khz/Sonne/A_contra_B.html\">http://home.snafu.de/khz/Sonne/A_contra_B.html</a><b><tt> ;-)</tt></b></p>\n<p>\n<i>Karl-Heinz</i>"
    author: "Karl-Heinz Zimmer"
  - subject: "Re: What about a Gimp Patch?"
    date: 2000-12-14
    body: "On that page is no download, there only are documentation and screenshots. And in my last KOffice-download, there was no Krayon.\nSo, how exactly is it done?"
    author: "Xiang"
  - subject: "Re: What about a Gimp Patch?"
    date: 2000-12-14
    body: "Check one of the links under \"Installing KOffice\""
    author: "Per"
  - subject: "Hot to get Krayon! (was: What about a Gimp Patch?)"
    date: 2000-12-14
    body: "<p>Krayon (the former KImageShop) is part of KOffice.</p>\n<p>It is contained e.g. in <tt>[...]/kde/snapshots/current/koffice-20001211.tar.bz2</tt> but the name of the directory is still <b><tt>kimageshop</tt></b> so that might be the reason why you did not find it.</p>"
    author: "Karl-Heinz Zimmer"
  - subject: "where to get Krayon"
    date: 2000-12-14
    body: "Since it is still alpha-alpha-alpha, and tons of code still need to be produced to make it official you will have to get it from CVS.<br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-14
    body: "Why hasn't anyone thanked the gPhoto crew for doing the work of splitting the frontend and backend software for use in other projects such as KDE?\n\nThank you gPhoto."
    author: "chris"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-14
    body: "Yes, someone from gphoto was actively\nencouraging a KDE port over the summer on\nthe kde-devel list.  It generated a lot\nof interest but then things got quiet.\n\nDoes anyone know if there was another effort\nto do this?\n\nAnyway, your point is a good one.  Thanks\nto both the Kompany and gPhoto for another\nbig piece of the Unix desktop!"
    author: "SteveH"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-14
    body: "We picked up on it from that thread and had some very positive conversation with the gPhoto crew at the time.  They have really done a great job on these libraries.  What was a bit disappointing was when we decided to start on this about a month ago we tried talking to them again and every single email to every single person has gone unanswered in all that time.  It's a testament to the quality of their code that we were able to find everything and pull this off as quickly as we did."
    author: "Shawn Gordon"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-14
    body: "Hi Shawn\nCongratulations and Thanks for yet another piece of the desktop puzzle.\n\nI was wondering, when will Aethera hit the street?\n\nBye"
    author: "Carlos Miguel"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-14
    body: "Well I just saw some of the latest screenshots from our engineers yesterday and Aethera is looking very good.  I would say 2 weeks is a safe bet at this point.  We have some UI things to resolve, but overall I'm very happy with how it's coming along."
    author: "Shawn Gordon"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-14
    body: "Wow, this is cool!\n<p>\nOne of the main things we wanted to do in designing gPhoto2 was to allow people to use it in different environments are architectures. It is great to see that people are taking advantage of it, especially under KDE2 since that is what I'm running ;)\n<p>\nMany thanks to Shawn Gordon and the Kompany for putting the time and effort into making it work. This is something else exciting to celebrate at the local Reno LUG christmas party!\n<p>\n-=Scott\nthe gPhoto project"
    author: "Scott Fritzinger"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-15
    body: "<b>Guys, get Krayon working well, and I'll have my Dad converted to Linux + KDE2 in no time !!!</b>\n<br><br>\nHe needs tools to create HTML-Files <blockquote>=>Quanta</blockquote>to get images from his digital camera <blockquote>=> gphoto2</blockquote>and to edit these images. Well, he sure could do the latter with GIMP, but, seriously, the GIMP UI isn't the most intuitive one, at least for longtime WIN-Users.\n<br><br>\nKeep up the good work !!!"
    author: "BigNick"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-15
    body: "I just installed this on a kde-2.0.1 FreeBSD desktop.  It went in smoothly and I can get a basic file view of the files on the camera.  However, downloading any of the images doesn't work.  The downloading dialog box just sits at 0%.  If I kill it and then try another, I get one of those \"the process implementing the protocol died\" messages pop up.\n\nThe command line gphoto2 utility reads the images quite well from my PowerShot S100 usb camera...\n\nAny ideas on how to track down this thing?"
    author: "Vivek Khera"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-15
    body: "The proper place to ask is gphoto@thekompany.com.  The Engineer is gone for a few days right now.  We will be releasing the code to KDE CVS as soon as everything is set up.  The project will be renamed kio_kamera at that time."
    author: "Shawn Gordon"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-18
    body: "Thanks.  I didn't find any kind of bug reporting address on the kio_kamera site.  All I could find were the info@ and jobs@ addresses, which didn't seem appropriate."
    author: "Vivek Khera"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-16
    body: "Good!!\n\nNow, I wait for a support of video4linux as a kio_slave.\nSo, as photocameras, we can access to webcams, tv-cards..."
    author: "Alex"
  - subject: "Re: KDE Gets Digital Camera Support"
    date: 2000-12-21
    body: "I've played with this a bit tonight with a Canon digital ixus and it needs more polish yet. On the other hand it could be gphoto, as I can download files with the gphoto command line but nothing can read the jpgs produced.\n\nThe kamera page mentions that gphoto needs a patch applied but its not in the sources as, the latest CVS of Gphoto2 has the lib update applied."
    author: "David"
  - subject: "This is great!"
    date: 2000-12-21
    body: "<br>\nI got one of my friends that Mandrake Linux for Windows(You know the Red Box)for X-mas and he have a digital camera too. I guess if he figure out how to work with GIMP he'll be good to go."
    author: "Starbuck Zero"
  - subject: "Kamera"
    date: 2001-12-02
    body: "Where can I get Kamera?  According to TheKompany.com, it _should_ be a part of KDE 2.2.2.  I have KDE 2.2.2-1 on RedHat 7.2.  Maybe I'm not looking in the right place?  Again, according to TheKompany.com, it should be in Control Center -> Peripherals.  I only have mouse and keyboard.  Am I missing a step?\n\nthanks"
    author: "Scott"
---
<A HREF="http://www.thekompany.com/">theKompany.com</A> has just released a patch to the well known digital camera application <A HREF="http://www.gphoto.org/">gPhoto2</A>, which allows it to integrate seemlessly with KDE 2. This means you can now access the wide range of supported cameras (over <A HREF="http://www.gphoto.org/cameras.html">100 models</A>) from any KDE 2 application.  Now I may finally be ready to buy a digital camera!  <STRONG>Update: 12/14 10:46 AM</STRONG>.  TheKompany.com has posted a screenshot of the <A HREF="http://www.thekompany.com/projects/gphoto/kcontrol.png">KControl module</A> and of <A HREF="http://www.thekompany.com/projects/gphoto/konqueror.png">Konqueror browsing pictures</A>.




<!--break-->
<p>The patch makes gphoto available as a kio_slave, which means that any KDE 2 application which makes use of the kio architecture can (without any modification) access the images stored on a supported camera. For example, you can preview browse the images in <A HREF="http://www.konqueror.org/">Konqueror</A> and even see thumbnails. The patch also adds a new KControl module allowing you to configure the camera.
<p>
According to theKompany.com's website, <EM>if you have a supported camera, you can start using it with most KDE applications in two easy steps: simply configure your camera model and port type from a list in the KControl module, then start accessing the camera contents with a gphoto:/ URL</EM>. It's a bit hard to set up right now -- you need to download the CVS version of gphoto -- but this should be sorted out Real Soon Now(tm). <A HREF="http://www.thekompany.com/projects/gphoto/">More information</A> on the patch is available at theKompany's website, and you can find a list of <A HREF="http://www.gphoto.org/cameras.html">supported cameras</a> on the gphoto website. 



