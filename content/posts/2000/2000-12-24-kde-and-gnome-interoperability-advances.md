---
title: "KDE and GNOME Interoperability Advances"
date:    2000-12-24
authors:
  - "Dre"
slug:    kde-and-gnome-interoperability-advances
comments:
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2000-12-24
    body: ">As of version 0.0.2, QGtkWidget has been\n>tested on Red Hat Linux 7 with Qt 2.2.3, gtk+\n>1.2.8, glibc 2.2-5 and gcc 2.96-69.\n\nI know I've been using too much free software when the thought of using software at version 0.0.2 doesn't set off alarm bells. ;-)"
    author: "Otter"
  - subject: "So what?"
    date: 2000-12-24
    body: "You guys posted an article about Gnome on your website a few months ago (when the Gnome Foundation was created), claming that gnome-libs and GTK+ are just a bunch of very broken libraries and don't work at all.\nWhy bother creating QGtkWidget if GTK+ and Gnome doesn't work anyway?"
    author: "Anonymous"
  - subject: "Re: So what?"
    date: 2000-12-24
    body: "I don't think something along those lines was posted here (except, maybe, as a comment from an anonymous user) - because it's simply not true.\n\nIMO, API-wise, Qt is the better choice than gtk+ (and (partially therefore), kdelibs is the better choice than gnome-libs), but that doesn't mean the alternatives are broken (they're still much better than, for example, Xaw or Motif).\n\nEven if they were broken, there are definitely some very good applications out there using them (think of, for example, AbiWord and gnumeric) - that's why it always makes sense to improve their interoperability.\n\n#include <the/whole/kde/vs/gnome/flamewar/is/stupid.h>"
    author: "Bero"
  - subject: "Re: So what?"
    date: 2000-12-24
    body: "I agree.\n\nim the only person i know who is developing a kde app that is working with a developer who is creating the gnome version of this app and we get along very well.\n\nhas anyone else started an app who is/has worked with a gnome app developer?\n\nhttp://ghdparm.sourceforge.net - its for kde as well"
    author: "Robbie Ward"
  - subject: "Re: So what?"
    date: 2000-12-24
    body: "there's licq, which has qt, gtk and console guis."
    author: "Evandro"
  - subject: "Re: So what?"
    date: 2000-12-25
    body: "No offense, but this is what seems silly to me.  Between just these two projects (GNOME and KDE) that has been an enormous amount of duplicated effort in developing application logic that is not related to the user interface at all.  I know it is easier said then done, but I think user interface logic should be abstracted from the application and you can build it to the toolkit of your choice.  Why write the application logic multiple times?"
    author: "Iridium"
  - subject: "Re: So what?"
    date: 2000-12-26
    body: "\"No offense, but this is what seems silly to me. Between just these two projects (GNOME and KDE) that has been an enormous amount of duplicated effort in developing application logic that is not related to the user interface at all.\"\n\nTotally agree, this is exactly what licq has done and gphoto is going in their move to gphoto2. I think that this kind of development is a real step in the right direction for opensource, code that is reusable with minimal effort is critical to our future. What I would like to see happen is more projects like licq & gphoto; where the experts in a field write the underlying api without tieing it to an interface. A good example of where this isn't happening and where it would be very useful is document conversion filters for the various WP projects.\n\nBero's effort is a way to make this happen at a higher level and is certainly a step in the right direction and so is xparts."
    author: "David"
  - subject: "Re: So what?"
    date: 2000-12-26
    body: "Another example of this I just came across was GNU Cash.  Looks like a very well design architecture where the interface is properly abstracted.\n\nAnother part of the problem is that interfaces are then implemented multiple times (for each toolkit).  Is it possible to create a meta interface language where you write the interface code just once and your favorite toolkit takes care of the work?  I would guess that maybe the XML interface definitions that are in use are a good step, but I'm not too familiar with the current state of those efforts."
    author: "Iridium"
  - subject: "Re: So what?"
    date: 2000-12-28
    body: "What is xparts?"
    author: "ChrisWiegand"
  - subject: "Re: So what?"
    date: 2000-12-24
    body: "No, I've *REALLY* seen such article on the KDE page (www.kde.org!)\nIt is *NOT* just a comment at dot.kde.org!"
    author: "Anonymous"
  - subject: "Re: So what?"
    date: 2000-12-25
    body: "The one thing keeping linux off the desktop for a while is the project split of GNOME and KDE.\nI'm pretty sure (correct me if I'm wrong) that GNOME was a project split from kde due to liscenscing issues over QT. Since then, even though in open source all code is available to\neveryone, we still have two copies of many features, due to implementation problems. Therefore, if Linux is going to become userfriendly to a computer illiterate, we need to have a project rejoin, or (even harder) a parser that can convert between one toolkit to the other.Mostly, the features between the toolkits are very similar, its just a matter of personal choice. Just look what project spitting did to BSD! This project is definetly a step in the right direction, especially as resource use becomes less and less of a problem for something as non-intesnive as a UI."
    author: "KDEer"
  - subject: "Re: So what?"
    date: 2000-12-25
    body: "Merging them seems to be pretty much impossible, since the APIs of Qt and gtk+ couldn't be much more different.\nIf you want to merge them, you pretty much have to throw away all code from either, which is almost impossible to agree on.\nThe other option would be to create a library that handles both Qt/KDE and gtk+/GNOME calls (pretty much an extended QGtkWidget), but this sort of thing would introduce tremendous memory requirements (because you have to have two very different implementations of very similar functionality in memory). Are there any better possibilities I'm overlooking?"
    author: "Bero"
  - subject: "Re: So what?"
    date: 2000-12-25
    body: "Well, what about a bridge? Maybe a library that would allow using bonobo components in KDE with an interface similar to Kparts and vice versa?\n\nA bridge, that would offer developers for both desktops to use components written for other with interface very much looking like the one they already know.\n\nIt would be nice if this could be sort of embedded in kparts architecture, but even if this is not possible, a big step would be to create a framework which wouldn't require extensive knowledge of the other side to prepare components for use in both desktops.\n\nTo be honest, I have absolutely no idea if this is possible, but it is something I am currently very much looking into. I do have a problem and a question though:\n\n1. I was called to do my civil service so my free time in next seven months will be very limited. I have no idea how far I'll come between holidays.\n\n2. I see there's been great progress made in last few weeks on technologies that could make this sort of thing more easy to develop. Does that mean that someone is already working on this? If yes, I'd appreciate if that person could drop me a line or two. Maybe I can help or at least go and work on something else :)"
    author: "Marko Samastur"
  - subject: "Re: So what?"
    date: 2000-12-26
    body: "I really think this is being looked at at the wrong level, wrapping apps might be a stop gap solution but it can't be the final solution, its just a hugh waste of resources and lets face it kde is IMHO bloated enough already.\n\nThe dream is this: What these two projects need is a standardized set of api's that are sperated from the interface completely e.g. libftp, libhttp or an even better example would be a samba libary say libsmb, oh wait a sec, that's already being written :-) Of course its hard to apply this to a program like gnucash. However, in reality this is never going to happen at app level as the projects already have this core functionality in them already and nobody will scrap what they have this far.\n\nKDE already does this reuse its self with ioslaves and kparts and I think you'll see KDE start to really accelerate next year because of it. By this I don't mean accelerate away from gnome, but become a really useable desktop for joe sixpack.\n\nI'd also like you to think about what happened to OS/2 and the fact it could run win 3.1 applications, IMHO OS/2 was technically a better OS but look where it is today. IBM thought, great its the best of both worlds, it can run native and win 3.1 apps, everyone will buy it. App developers thought great, I only need to write win apps. With Linux things are different I can install both Gnome and KDE and switch between the two but if I was to find that I spent nearly all my time in Gnome as I used Gnumeric and GNUcash 90% of the time, why bother with KDE at all, just make the effort to get Gnome a bit more functional.\n\nIMHO OS/2 was missing one thing: a killer native app. You might think, Konqy is a killer app but its functionality is not massively different than mozilla's, in other words gnome users can get a functional browser and various functional file managers (natilus being the best). Kmail is not a bad example but it's still not a killer as its basic fuctionality can be supplied by gnome in various ways.\n\nThe killer app KDE needs is this: KOFFICE, I'd just like to see the core team get 2.1 out of the door and spend sometime getting this baby up to shape. If the faures, the bastians, the hausmans and the bero's of the project could spend a couple of months doing to koffice what they have done to the core of KDE I'm sure koffice would be very useable. It just seems that more and more apps are being added to KOFFICE but none of them are in a really decent state. When Linux gets to the desktop, IT departments aren't going to support two different desktops and the fact that some big players are backing gnome it already has a lead.\n\nWhile I'm moaning on about some aspects of KDE I'll flag another issue: packaging. Why oh why is it done this way? E.g. I don't want to install all of kdenetwork, I just want kmail. I don't want all of the kdeutils just kcalc & ark etc etc.... I have the abbillity to do this, I just check out the cvs and only make what I need but check the distros, you only get a choice of the full packages. I guess this is mainly why KDE is thought of as bloated? This is one thing the gnome boys or more specifically Helix have got spot on. Once my exams are finished this is the area I'll be working on, not glamours but very critical to the success of KDE.\n\nYou guys do this in your free time and I realize that but I also think that you want as many users as possible to use KDE. That surely must be a goal of the KDE project?"
    author: "David"
  - subject: "Re: So what?"
    date: 2000-12-26
    body: "What bloat? Library (or whatever form the bridge would take) wouldn't get used except if you tried to use GNOME application. In that case, you already decided to load everything that it needs (ORB, libs...), so this library wouldn't make much difference (if done right that is).\n\nThe difference between what I am looking for and what you are is that I'm trying to do something to help with current situation and avoid dreaming about what would perfect world be. As you've said, it would require scraping much of what has been done already and that just won't happen. Since we seem to agree that it won't happen, what is the next best thing if not more interoperability?\n\nI know what happened to OS/2 and to be honest, I don't care. I think achieving my goal would benefit users so I don't really care if at the same time would persuade some developers to choose GNOME technology. I dislike politics like that and I hope that their time has passed."
    author: "Marko Samastur"
  - subject: "Re: So what?"
    date: 2000-12-26
    body: "to the package problem:\ni know the problem from suse and mandrake...\nbut debian has packaged them a bit different.\ni can easily apt-get install ksnapshot (or anything, e.g. konqueror) and it installs me only what i need to run this app. it really rocks!\nso i guess you should blame your distro's packager not the KDE team ;)"
    author: "Spark"
  - subject: "Re: So what?"
    date: 2000-12-26
    body: "Perhaps a midway option. It could be called the UDE, (Unified Desktop Enviroment), and contain library suporrt for calls with similar functionality and syntax to both KDE and GNOME.\nIt wouldn't be out of the box, but it would make porting easier. This is probably a little drastic though. Perhaps someone could set up a project on a detailed HOWTO on porting from one to the other."
    author: "David Simon"
  - subject: "Re: So what?"
    date: 2000-12-26
    body: "um UDE is the Unix Desktop Environment. <a href=\"http://udeproject.sourceforge.net\">UDE project homepage.</a>\n<p>\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: So what?"
    date: 2000-12-26
    body: "oops, sorry.\nPerhaps the ECDE then?\n(Extravagently Cool Desktop Enviroment)"
    author: "David Simon"
  - subject: "Re: So what?"
    date: 2001-01-01
    body: "API differences are, of course, very important but what most users see is the UI. From a theoretical point of view, how hard would it be to have some kind of link between the GTK+ theme and the Qt theme-- so that GTK and Qt will start looking similar? I can do this if I write the same theme for GTK and Qt but is there some way to convert them automagically and change them both at once? I'm sorry if this question has already been asked...\n\nGNOME and KDE already share code. I don't know for sure, of course, but dot.kde.org looks a lot like gnotices (news.gnome.org). It must be the same app. Even the notice at the bottom about fine print."
    author: "Moses"
  - subject: "The licenses"
    date: 2000-12-24
    body: "I know this is offtopic, pointless, futile, etc. but for a collection of hackers who just wanna write code, we spend(waste?) a *lot* of time on licenses. I know why they are important: so our code doesn't get hijacked, but bero writes an interesting app, and has to release the damn thing under all those licenses?!? That's ridiculous, and wastes time we could better be using fighting The War(tm) :)  Just today's rant.\n\nErik"
    author: "Erik"
  - subject: "Re: The licenses"
    date: 2000-12-24
    body: "Well, it's partially my fault - I could just have\nreleased the whole thing under the GPL because both the LGPL and the BSD license are compatible.\n\nI just prefer not to start a holy war by re-licensing glib code under a different license. ;)"
    author: "bero"
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2000-12-26
    body: "Why not something like Wine? (Wine = something like an emulator for Windows). That means you don't need all the Gnome-libs when you want to run Gimp, for example. The program should catch calls from both WM's and send a generic call to the underlying X-libs.\nIf you run Gimp with KDE, the following would happen (same for modules in KControl, a KDE-program in Gnome or whatever):\n-Gimp would call for Gnome-libs\n-There's a replacement for those libs that takes the calls and translates them to KDE-calls\n-The KDE-calls are processed and KDE sends calls back to the replacement for the Gnome-libs.\n-The replacement converts the KDE-type calls to Gnome-calls.\n-Gimp gets a normal reply, like it needs.\n\nThe good things about this is that it doesn't produce too much overhead (comparable to the windows-on-linux-with-wine-slowdown) and that there're no changes to be made to Gnome- or KDE-applications. It even saves space on your HD, as you can remove the KDE- or Gnome-libs. If the basics of the new program work, it could be extended. In the end, there could even be a merge between KDE and Gnome (and possibly others too!)\n\nAnyone who knows enough to build something like  this? Any comments appreciated, I would like to do it myself but I don't even know Tk or Qt, so that's a problem."
    author: "Jonathan Brugge"
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2000-12-26
    body: "What this thread is about is not duplicating work. What you're suggesting would mean re-writing the gnome libs, this is exactly what wine has done by re-writing the windows libs. The wine project had to as, of course, windows is closed source.\n\nThe other issue is that gnome is in a continual state of change and rapid development, windows isn't; office 2000 runs on a 5 year old OS (win95) as well as windows 2000."
    author: "david"
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2000-12-27
    body: "In case ya didn't know: Gnome apps are working perfectly well in KDE and vice versa.\nSo why do we want to rewrite gnome-libs?\nAnd if we want Gnome/GTK+ programs look like KDE/QT apps, just install a GTK+ theme and that's it!\nGTK+/Gnome isn't so inferiour as many people claimes."
    author: "Anonymous"
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2000-12-27
    body: "I know, but don't forget that you have to load all the gnome-libs just to use one gnome-app. If you use just a small gnome-app, like gcalc, you have to load all the gtk-libs it uses. That's kinda useless, I think. That's why I want to replace the gnome-libs with some kind of wrapper. The wrapper won't be as heavy as the gtk-libs, thus reducing the overall mem-usage when running gnome-programs in kde or vica versa.\n\nJonathan Brugge"
    author: "Jonathan Brugge"
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2000-12-28
    body: "Then everytime you want to start a Gnome app, the wrapper library has to be started.\nThat doesn't resolve much, does it?"
    author: "Anonymous"
  - subject: "GUI abstraction via Mozilla's XPFE?"
    date: 2000-12-26
    body: "There's some excellent points being made about abstraction the UI layer from applications.\n\nCould something like Mozilla's XPFE be used for this?  My [very limited] understanding is that XPFE was built exactly for this reason: to abstract the UI layer so that any toolkit or window system (or OS) can be plugged in.\n\nI've heard that XPFE is one of the reasons Mozilla is so bloated and slow... anyone know if this is the case?\n\nRaul"
    author: "Raul Acevedo"
  - subject: "Re: GUI abstraction via Mozilla's XPFE?"
    date: 2000-12-27
    body: "Well, UI abstraction will probably make it harder to create your program. It will also stop you from using toolkit specific extensions i.e. you will have to code to the lowest common denominator of all toolkits which you intend to be used. I have no idea about XPFE, but I think it's the XUL stuff which makes mozilla exceptionally slow."
    author: "ac"
  - subject: "Re: GUI abstraction via Mozilla's XPFE?"
    date: 2000-12-27
    body: "Mozilla's ok fast.  Just loads like a pig.  0.6 loads much better though."
    author: "Brewsky"
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2000-12-27
    body: "It is kind of neat to see that the KDE team is working real hard at creating some form of interoperability between KDE and Gnome. are thier any kind of similar projects by Gnome developers?\nI ask out of quriosity? Because I haven't heard of any."
    author: "L.D."
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2000-12-27
    body: "I don't think that's possible.\nKDE is written in C++, which is compatible with C.\nGnome is written in C, which is not compatible with C++.\nIf Gnome programs want to make use of KDElibs/QT, they have to compile their programs using a C++ compiler."
    author: "Anonymous"
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2001-01-19
    body: "Gtk can be used in many languages.\nGtk-- is C++ version of Gtk.\n(so you are wrong.)"
    author: "Hajdi"
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2000-12-27
    body: "What would be very cool is if KDE could export\nits API via vtable pointers like Microsoft's COM.\nThen other languages (not just C) can make KDE\ncalls without going through some clumsy wrapper\nlibrary.  While sometimes necessary, wrappers\ntend to lag in quality/functionality in\ncomparison to the native API.  BTW, I'd very\nmuch like to see Konquerer run in GNOME's\nNautilus instead of Mozilla.  Is anybody working\non a Bonobo component for it?"
    author: "Anonymous"
  - subject: "GNOME and KDE philosophies"
    date: 2000-12-30
    body: "Lost in all the talk about the licensing differences between Qt and GTK+ is a deeper difference between the GNOME and KDE projects, or at least their core developers.\n\nThe central KDE team is gung-ho about interoperability and reaching out to users of other operating environments. Where the GNOME (and GNU) crowd often criticizes KDE for its resemblance to the MS Windows UI, down to control panel design, human interface standards, etc., the KDE team sees these as positives and indeed counts it as a goal.\n\nThe result is that once you show a novice that the \"K\" button on the desktop toolbar is eqivalent to the Windows Start button, Windows users often don't even know they're using something else. The trashcan/recycle bin is where it's expected to be and works as expected. Right-clicking brings up consistent and eerily familiar context menus.\n\nGNOME's UI has improved with time, but even now any three \"mainstream\" GNOME applications will have four different menu-organization schemes and some unique keybindings for common tasks. Surely \"File\" and \"Edit\" menus can be made predictable. Surely? But there's something more troubling at the core of this. Too many people with a strong influence on GNOME's direction favor difference for its own sake, designing interfaces whose defining characteristic is their difference from the Windows and MacOS user interfaces. \n\nThis wouldn't be a Bad Thing if they were following a well-planned set of UI guidelines of their own that the core developers felt was superior to existing UIs. The folks at Eazel are shooting for this, as are (in a surprising change of mind) Miguel de Icaza and his crew at Helix Code. But much of the GNOME rank-and-file either doesn't know how to follow UI guidelines or--and this seems the case based on the scathing comments that a professional UI on a GNOME application often elcits--thinks that a consistent UI is somehow selling out the GNU cause.\n\nIt's this latter attitude that will continue to hurt GNOME's \"mainstream\" aspirations if it remains on the agenda among people working on the GTK+ toolkit and key GNOME applications.\n\nI continue to wonder how much of the GNOME project's hostility to KDE still stems from the Q Public License and how much is instead a hostility to KDE's polish and ease of use. As good as GNOME's underpinnings have become, things like  window-manager independence, no trashcan and byzantine nested dialogs that seem designed only to frustrate novices without helping power users to actually <i>work more efficiently</i> don't assure me that this is going to get better."
    author: "Steve Koppelman"
  - subject: "Re: GNOME and KDE philosophies"
    date: 2001-01-02
    body: "It has been stated that GNOME does not have standards for interface design.  As it happens, GNOME does have standards, which are at least partially built into the libgnomeui libraries.\n\nThere are macros which create 'stock' menu items, such as the 'new', 'open' and 'print' items.  The standard GnomeApp struct and its accompanying functions can be used to create menus, toolbars and statusbars that conform to the preferences of the user given in the 'Control Center'.  There are even functions that allow the type of MDI used to be universal throughout the desktop.\n\nI do not know if KDE implements these features, but at any rate I like them.\n\nAs for helping users work more efficiently, I have never had any problems with the interface.  Finally, who needs the trash can anyway?  Under Windows, I always found it to be pain."
    author: "Nicholas Curran"
  - subject: "Re: GNOME and KDE philosophies"
    date: 2001-01-09
    body: "Actually, there *is* a trash can for gnome. It's just a shortcut to a directory on the desktop with a trash icom. Draging a file on it moves the file to the directory. The default HelixGNome desktop doesn't implement it but Mandrake 7.2 GNOME installation doest it."
    author: "Alex"
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2000-12-31
    body: "Gtk is written in C so that it could be used with many different languges, Qt in C++, while C++ may be a better language(in your opinion), OO languages don't work verry well with non OO languages."
    author: "_"
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2001-01-05
    body: "wrong (C++ works perfectly with C toolkits). True: Non-OO languages do not work with OO-Languages (you can't use C++ toolkits with C)."
    author: "Thomas"
  - subject: "Re: KDE and GNOME Interoperability Advances"
    date: 2001-01-03
    body: "Heh... That's cool: First you throw away your interop layer, since you've choosen the wrong CORBA implementation... And then you ask for interoperability...\n\nBTW: At least it's possible to plug KDE applets into GNOME apps."
    author: "Anonymer GNOME-Verfechter"
---
<A HREF="mailto:bero at redhat.com">Bernhard Rosenkraenzer</A> today
announced the initial (experimental) release of
<A HREF="http://www.linux-easy.com/development.php">QGtkWidget and
QGtkApplication</A>, "<EM>a class that makes it possible to embed
<A HREF="http://www.gtk.org/">Gtk</A> widgets in
<A HREF="http://www.trolltech.com/">Qt</A> applications (in the
same process space, sharing the same event loop).</EM>"
Bernhard hopes that "<EM>this will be useful (primarily) in making KDE
and <A HREF="http://www.gnome.org/">GNOME</A> more interoperable (think of
embedding GNOME applets in KDE...)</EM>" but warns that this is not
particularly efficient -- either from a memory or CPU perspective.
Thanks anyway, Bernhard, we'll take it!  A more full description is below.







<!--break-->
<BR>&nbsp;<BR>
<P>
<TT>From the README file:</TT>
</P>
<P>
<H4>What is it?</H4>
</P>
<P>
QGtkWidget and QGtkApplications are classes for combining Qt and Gtk widgets
in a single application.  While this sort of thing doesn't make much sense
under normal circumstances, it can be used to help KDE and GNOME applications
interact better (think of adding GNOME control-center plugins to KControl
and vice versa).
</P>
<P>
You should probably NOT use this because "I like Qt, but Gtk has that one
great widget" - using both libraries in one application without a real need
creates huge resource requirements.
</P>
<P>
Also, the loop handing both Qt and gtk+ events is definitely not as efficient
as using just one of them.
</P>
<P>
This version is not exactly complete (see the TODO file); it's more a proof
of concept and call for fixes.
</P>
 
<P>
<H4>I'd like to help...</H4>
</P>
<P>
Send any patches to <A HREF="mailto:bero@redhat.com">bero at redhat.com</A>.
If you're looking for something to do, read the TODO file (in this directory).
There is currently no CVS tree for QGtkWidget, but setting that up is not a
problem. If you'd like CVS access, simply let me know.
</P>
 
<P>
<H4>There's a bug... And I need that feature...</H4>
</P>
<P>
Report any bugs (preferrably with a fix ;) ) and feature requests to
<A HREF="mailto:bero@redhat.com">bero at redhat.com</A>.
</P>
<P>
<H4>What platforms does it run on?</H4>
</P>
<P>
As of version 0.0.2, QGtkWidget has been tested on Red Hat Linux 7 with
Qt 2.2.3, gtk+ 1.2.8, glibc 2.2-5 and gcc 2.96-69. Chances
are it will run on most Unix-like systems with minor or no changes.
It will not compile or work on Windows. This is a feature, not a bug. :)
</P>
<P>
<H4>What license is QGtkWidget under?</H4>
</P>
<P>
Since QGtkWidget uses code from different projects, it has to be released
under a mix of 3 different licenses.
In detail,
</P>
<UL>
<LI>gmain.c is under the LGPL (since it's taken from glib, with minor modifications to make it compile with a strict C++ compiler.  See the included diff file for details.)</LI>
<LI>qxembed.* is under the LGPL</LI>
<LI>QGtkWidget.* is under the GPL. It contains some lines of code from Qt's qapplication_x11.cpp.</LI>
</UL>
<P>
The example is released under the BSD license (without the advertising clause).
</P>






