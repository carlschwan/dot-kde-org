---
title: "VisKProg programming language"
date:    2000-10-02
authors:
  - "CP"
slug:    viskprog-programming-language
comments:
  - subject: "Re: VisKProg programming language"
    date: 2000-10-02
    body: "OK, I like VisKProg, but not is better contribute with KDevelop and make the code for implement QTDesigner and power it more????????????"
    author: "Manuel Rom\u00e1n"
  - subject: "KDevelop"
    date: 2000-10-02
    body: "KDevelop is a great IDE, and there is (theoretically) the possibility to implement just another project template in KDevelop;\nthough it doesn't fit our needs:\na) KDevelop is very powerful but therefore also\nvery complex. A beginner language doesn't need\ndozens of options, it should be as SIMPLE as possible with only as many menu entries as really needed. KDevelop on the other hand is\ndesigned to give as much control as possible about whatever.\nb) KDevelop's UI focusses on the source code, and only allows to add some dialogs in a graphical editor as an \"extra feature\". This is fine with a language like C++, meanwhile it's not a good solution for someone who mainly wants to design a window and then (as a second step) add some source code instructions. And it seems to me that this is, how programming beginners behave: they first want to do what they want to see (the UI).\nBTW, I don't like KDevelop's form designer very much.\nWith the Qt designer it's different: it's new, and now I don't see any reason why we should make a rewrite of it for VisKProg, when finishing our own \"kwinedit\" will probably take less time. Still, I agree with you that the people at KDevelop should integrate it.\nc) A beginner language like VisKProg needs much more integration between \"the language\" and \"the IDE\". Ask people who have just started to write their first VB programs under Windoze: most of them doesn't know the difference between the language and the IDE, and IMHO there is no need\nfor a visual programming language to force their users to learn the \"pure\" language (without IDE) first. The language may even need the IDE, and the IDE should work hand in hand with the language (syntax checking in the editor, code completion, and much more). KDevelop is not designed to do most of these things, and even if we would implement it, it would mainly be a waste of time, as the other languages (C/C++) won't probably need it in such an advanced form and implementing a feature \"generally\" (so it should work for EVERY langauge used with KDevelop) takes much more time then doing the same thing for VK only.\n\nRegards,\nChris (VisKProg team)\n"
    author: "Chris"
  - subject: "Re: VisKProg programming language"
    date: 2000-10-02
    body: "Yes, I agree with him.\nAnd why yet another RAD tool while we have QT Designer?\nAnd why not merge QT Designer's code with KDevelop?\n\nI have yet to see a RAD tool for Linux as powerful as Delphi..."
    author: "Anonymous"
  - subject: "Re: VisKProg programming language"
    date: 2000-10-02
    body: "Hmm... 'Kylix' from Borland would fit to your\nneeds, I think."
    author: "Thomas"
  - subject: "Re: VisKProg programming language"
    date: 2000-10-02
    body: ">And why yet another RAD tool while we have QT Designer?<br>\nWell, competition even helps open source projects by offering a choice ;-) BTW, our project is older then the Qt designer, though the Qt designer has been developed much more rapidly by people who are paid for this job.<br>\n\n> And why yet another RAD tool while we have QT Designer?<br>\nFine, why not... but ask the KDevelop programmers to do so please ;-)<br>\n\n> I have yet to see a RAD tool for Linux as powerful as Delphi...<br>\nWell, if you ask me, you disagree with yourself there: if you only want to have KDevelop, you will never get a RAD like Delphi, KDevelop is completely different from such projects.\nKylix may fit your needs, but it will be closed source/commercial. If you want such projects as freeware, you'll have to allow projects like ours to do their work ;-)\n"
    author: "Chris"
  - subject: "Re: VisKProg programming language"
    date: 2000-10-02
    body: "But why the rename from \"Visual K\" to VisKProg? :)"
    author: "ac"
  - subject: "Re: VisKProg programming language"
    date: 2000-10-03
    body: "Good question ;-) Well, it was the time, when M$ just had started to forbid some sites to use \"where do you want to go tomorrow?\", and when some other projects also changed their names to avoid (possible) future copyright problems.\nThen, we've found out that nearly every search engine finds thousands of matches when you enter something like \"Visual\"+\"K\" ... so we looked for a more individual name, that's all. BTW, \"VisKProg\" stands for \"Visual K(DE) programming language\" ;-)"
    author: "Chris"
  - subject: "Re: VisKProg programming language"
    date: 2000-10-03
    body: "Looks pretty cool actually, and should be very interesting to see where this goes."
    author: "Ashleigh Gordon"
  - subject: "No Gnome :-("
    date: 2000-10-03
    body: "I'm happy to see al these great IDE and RAD tools.\nThe only problem is they are all made for KDE while I prefer Gnome.\nGlade is very good but gIDE is useless :-("
    author: "Anonymous"
  - subject: "Re: No Gnome :-("
    date: 2000-10-03
    body: "Dear GNOME fan,\n\nVisKProg is designed not to depend on a special environment; the actual version only supports KDE, but we just need a rewrite of the \"libs\" and a new project template, not only to allow GNOME projects but even to let the _same_ VisKProg programs compile with both KDE and GNOME, on both fitting all standards. Only the\nlarge parts of the IDE will remain KDE only (as long as nobody rewrites them).\nIf you want to help us, you can start to implement GNOME support immediately, otherwise it will still take some months...\nIf you want to participate in the discussion about VisKProg support for other environments,\nplease subscribe to our mailing list (more information at www.viskprog.org).\nFor instant, we're not sure yet whether to choose \"real\" GNOME or only wxWindows/Gtk for the Gtk support - we need your opinions!\n\nRegards,\nChris"
    author: "Chris"
  - subject: "Re: No Gnome :-("
    date: 2000-10-04
    body: "I would love to help but I am already working on my own project and I don't have any QT/C++ programming experience.\n\nAnd does VisKProg supports GTK+?"
    author: "Anonymous"
  - subject: "Re: No Gnome :-("
    date: 2000-10-04
    body: "The IDE will be for KDE only, but the generated projects can be in what language and for what toolkit you like. The translators are designed to generate different output, though a Gtk/GNOME\nproject template is not implemented yet.\nYour e-mail address, please! IMHO, this forum isn't a good place for discussion on such specific details ;-)"
    author: "Chris"
---
<A HREF="http://www.viskprog.org/">VisKProg</A> is a <a href="http://www.viskprog.org/screen/screen018.html">visual</a> KDE programming language
suitable for beginning programmers.
It comes with a complete IDE (including a form editor and a window editor) and generates C++ code, so no interpreter is needed to compile/run the programs. Though VisKProg's IDE is still based on KDE 1.x, we want to release a stable v0.2 in the next weeks for KDE 2.0.  We are looking for testers to write programs and report bugs and for
developers to finish the IDE and port it to KDE 2.0 (please! ;-)).

<!--break-->
