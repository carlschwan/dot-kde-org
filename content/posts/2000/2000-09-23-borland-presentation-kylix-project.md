---
title: "Borland Presentation: Kylix Project"
date:    2000-09-23
authors:
  - "numanee"
slug:    borland-presentation-kylix-project
comments:
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-22
    body: "Does anyone know about popular applications written in Deplhi ?\n\nThe porting would be effortless also for C++ builder applications, or only for C++ applications which use VCL ?"
    author: "Andrea Albarelli"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-23
    body: "HomeSite is really popular. And WindowsCommander.\nIt's the only 2 normal programs written in Delphi I ever used."
    author: "Oblom"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-23
    body: "I know a very nice Email client, Kaufman Mail Warrior. It's written in Delphi, but it's not opensource, I'm afraid."
    author: "doganello"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-22
    body: "Aren't the C++ Builder apps based on mfc? (I really don't know infact...)"
    author: "ticon"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-22
    body: "No.  MFC are Microsoft's Foundation Classes distributed with Visual C++.  Borland have their own libraries (crossplatform?).  Check the links for more details."
    author: "ac"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-23
    body: "If you don't know, don't say anything. Espscially don't talk rubbish"
    author: "Prem S Kang"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-23
    body: "I suppose you don't know anything, as the information provided by what you said is equivalent to 0. I didn't know anything, but there is some info brought by my question."
    author: "ticon"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-25
    body: "My apologies, I didn't intend it the way that it has been taken, I thought you were implying that Borland's libraries were based on MFCs. I understand now that it was more of a question rathar than a statement."
    author: "Prem S Kang"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-24
    body: "What's that? The purpose of the newsgroup is to ask questions.\nThe only thing I really hate about Linux&CO are people like you in the newsgroups: arrogant and unfriendly.\nWe don't need any Bill-Gates-like-characters here"
    author: "Michael"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-25
    body: "I've apologised for my terseness, but you've completely lost me with the Bill-Gates-like characters thing, please explain."
    author: "Prem S Kang"
  - subject: "Time Frame?"
    date: 2000-09-22
    body: "And the question that is probably begging to be asked is: *WHEN* will we have Kylix in our claws? (even a beta)"
    author: "t0m_dR"
  - subject: "Re: Time Frame?"
    date: 2000-09-22
    body: "Haven't they been giving demos already?  The FAQ says: The Delphi edition will be the first to be released and is currently expected to be completed at the end of 2000 followed by the C++ edition in mid-2001."
    author: "ac"
  - subject: "RAD"
    date: 2000-09-23
    body: "Either way, a RAD style develeoper will benefit Linux even if the apps are not KDE based... The more Linux users, the more KDE users.\n\nFirst there were Monkeys, then there was Unix...\n\nTroy"
    author: "Troy"
  - subject: "Re: RAD"
    date: 2000-09-24
    body: "<B>First there were Monkeys, then there was Unix...</B>\n<BR><BR>\nAnd then there was Borland, and it was good... <g>"
    author: "Chris Bordeman"
  - subject: "Yes, but the question is *when*"
    date: 2000-09-25
    body: "I'm very glad with the Delphi port to LiNUX; Pascal always has been one of my prefered languages and Delphi the best RAD that I've used, so i'm stressed since Borland talked about.\n\nSo I think the question is WHEN? :)~~~\n\n(Could it be NOW? :D)"
    author: "Francisco"
  - subject: "Why \"about applications\" and not NOW?"
    date: 2000-09-25
    body: "My personal experience with Delphi on Windows suggests that making a nicely running real life app with Delphi requires reading VCL source, using Windows API and messages, and consulting with MSDN. Thus, the total amount of what a Delphi developer has to learn before being able to do quality work is equal or exceeds what a C/C++ or MFC developer has to learn. \n\nSo, I guess people start to use Delphi because \"Hello, world\" can be done easier with Delphi then with Visual Studio. People continue to use Delphi because they like Delphi Pascal better then C++. \n\nI like the following 3 Delphi Pascal features best:\n\nImplementation through delegation with \"pointer to method of object\" types. In C++ \"pointer to method\" cannot be used without a pointer to a class instance.\n\nBuilt-in \"message\" keyword. MFC has event maps to emulate that, but they are good for Windows messages only, not for creating custom messaging schemes.\n\nAutomatic reference counting with interfaces.\n\nOn Windows, Delphi scales down to a plain vanilla compiler with nice IDE that runs faster and produces faster code then VS (this is my strictly personal opinion based on my strictly personal tests), thus fully utilizing these advantages.\n\nHowever, there are 2 irritating problems with that. First, the C header file translation. This one looks inevitable to me, but I still wonder how D5 could go without Winsock2. Second, COM libraries that are loaded with every Delphi app. This one looks like a major bug.\n\nSo, I feel like the Windows Delphi is a bit \"about applications\" and I would like it not to be that way. In other words, the plain vanilla compiler level in Delphi is not perfect and being \"for applications\" hides some compromises with the basic design. But the problems are minor and I guess Delphi is OK on Windows.\n\nI have more concerns with Linux. As far as I know, Kylix does not use Qt directly. For some reasons including but not limited to multiple inheritance occasionally used in Qt, there is a layer of plain functions in Kylix between its visual components and Qt. This is not the best approach for a C++ development tool to be included with the next release of the Kylix project.\n\nQt has signals and slots (and GTK must have analogs) which I like much better then MFC message maps. They allow the same level of implementation through delegation as Delphy and go further, providing native ways to deliver a signal to multiple slots. In Delphi multiple signal delivery should be programmed by hand.\n\nThere are rumors that the \"message\" keyword will not be available in Kylix. \n\nThus, with only reference counting left, I expect Kylix to have less advantage over C++ on Linux then on Windows.\n\nOn arrival, Kylix will have to compete at least with Qt designer and Kdevelop, both running on KDE2 and both being free. It is very easy to do \"Hello, world\" with these tools. If Kylix does not scale down well and it is harder and/or less efficient to do with Kylix what can easily be done with, say, Qt designer and Kdevelop, many people may opt not to start using Kylix at all. \n\nSo, the exact meaning of \"Kylix is about applications\" must be very important. I failed to find additional comment on the Borland site. \n\nAs far as I understand, Borland is working on the compiler, IDE, and component library in parallel. However, the compiler must be ready before the IDE and the library. If Kylix is due late December, it should be working right NOW. \n \nBorland have made the C++ command line compiler for Windows a free download to attract more people to C++ Builder. Why not to make a Delphi Pascal command line compiler a free download? Even if it is alfa NOW, that will help Borland to debug the compiler and us users to get rid of some concerns regarding Kylix.\n\nFinally, I would like to share my dreams of what could have happen.\n\nWhat if Delphi Pascal interfaces were augmented so that that they  wrap Qt multiple inheritance? If that means that I can do more with interfaces then with COM, so what?\n\nQt has to implement slots and signals through MOC. Borland owns Delphi Pascal and could add native support for delivery of a signal to multiple slots. \n\nBoth additions could be used with Delphi 6 on Windows. That would make Delphi Pascal lead over C++ on Windows even greater (a necessary move if C# is not a joke) and keep the lead on Linux."
    author: "Andrei Gerasimenko"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-25
    body: "hmm\nUnder windows, dont you have to link-with and have on hand a special delphi .dll to even run a program kind of like VBasic?\n\nThis, in addition to the really bloated executables that dephi usually produces leaves me a little unsettled about this.  Can this actually be bad for linux in general?  Gives MS freaks ability to now say linux is eneffecient and has bloated programs."
    author: "Jesse"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-26
    body: "Delphi can use a dll (package) to reduce exe size if you want, but by default it produces stand alone Native EXEs. Sure a small hello world app is about 350k, but I have created very complex apps that fit on a floppy and run with zero setup.\nI would like anyone with Visual Studio to try this.  You can also use EXE compressors which bring the hello world down to 100k.  Most VB programs and their bloated setups are many megabytes in size.  I started in VB and could never get just a exe to run on any PC i wanted becasue of all the OCX/VBX versions out there."
    author: "Tony Caduto"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2000-09-26
    body: "First, Delphi does not require a \"runtime\" like VB did.  The way I understand it is that VB's run time was acutally the \"p-code\" interpreter, not compiled code.  With Delphi, anything you would be required to distribute would be compiled code.  Delphi can do anything you would want it to do.  If you want to write command line apps and not use any Windows or Linux libraries, the EXE sizes will contain only your code.  I've written Delphi Command Line Apps that were around 1K in size.  they didn't do much, but I had the capability all the same.\n\nThe big packages you are talking about are \"wrappers\" around existing Windows functionality.  These \"wrappers\" can save countless hours of research because most of the grunt work is done for you, and not to mention as you become more advanced, you can change anything you like vio OO methods.  Have you tried to override any functionality in VB?  If all you want to do is write business apps, and cover the 80/20 rule, VB is OK.  If you ever need to get down to the \"metal\" I would not recommend VB (besides the fact it would have to be on Windows).\n    VB COM (activ X) controls are expensive for the single user, and usually contain lots of bugs, and you can't get the source code.  In VB you end up being more creative doing \"work arounds\" of VB's issues, and not a creative problem solver or coder.\n   As far as Delphi/C++ builder, you can choose to compile the \"used\" functionality into your app, which on the average makes your app/EXE about 350K, or you can distribute the package (what borland has called their code libraries, on Windows) and the app usually will be around 32 - 200K (depending on what you are doing obviously).  You can mix and match which packages get compiled in and which do not.   If Borland gets their packages on the major distributions, then your app could potentially be very small.\n  We all know that you can't get something for nothing.   The code has got to exist somewhere to make all those \"pretty windows\".    With Delphi you are getting a lightning fast, native/optimized code compiler.  Not a run time interpreter.   \n  I can't wait for the release!\n\nCurt"
    author: "Curt Krueger"
  - subject: "Re: Borland Presentation: Kylix Project"
    date: 2001-01-09
    body: "I'm afraid that people of this forum don't like Borland ?\nMe, I have made some programs with Delphi and It's a very good interface and language to program !\nBut, I post this reply to ask one question : Is there a free trial version of Kylix ?"
    author: "peyu"
---
Borland is featuring an exciting <a href="http://www.borland.nl/bww/europe/norben/array/Linusem/kylix.htm">presentation</a> on <a href="http://www.borland.com/kylix/">Kylix</a>, the upcoming Rapid Application Development environment for Linux. <i>"In our view, the release of Kylix will mark an evolutionary step for the Linux world. Delphi and C++Builder developers will be ready (on the very first day) to start writing applications for Linux using Kylix. [...] Apart from the Delphi developers, Kylix also brings <b>hundreds of thousands</b> of applications, built in Delphi today, ported with Kylix tomorrow. On the first night that Kylix ships, we'll probably see more new Linux applications than we've seen in the past few months..."</i>  Kylix is based on Qt and will integrate with KDE.




<!--break-->
