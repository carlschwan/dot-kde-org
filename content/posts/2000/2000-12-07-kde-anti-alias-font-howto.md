---
title: "KDE Anti-Alias Font Howto"
date:    2000-12-07
authors:
  - "Dre"
slug:    kde-anti-alias-font-howto
comments:
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-07
    body: "Maybe you should work together with the Gnome people.\nThey are working on anti-aliasing too."
    author: "Anonymous"
  - subject: "KDE/Gnome co-operation"
    date: 2000-12-07
    body: "Exactly! I'm happy that this KDE vs Gnome thinking  is diminishing, and the focus is more on co-operation nowadays. But we need more. Both environments are going to be around for a long time so they'll have to work perfectly together.\n\n\nWhen are we going to see the first organised Gnome/KDE meeting? That would be awesome! How about sharing code?\n\n\nKeep on the good work, all of you!\n\n-tomas"
    author: "Tomas"
  - subject: "Re: KDE/Gnome co-operation"
    date: 2000-12-08
    body: "I am not a KDE core developer but I understand there have been some conversations between KDE and GNOME teams. I believe the \"desktop wars\" have been carried out by some elements of the commerical press and and fringe enthusiasts. Having said that I think there are a few points people should keep in mind before getting carried away in a love in. Regarding this, X should be specific to neither KDE or GNOME but equally accesable to each. Beyond that, even though both environments have a number of similarities and can appear similar, under the covers they are anything but. GNOME uses C with thier own object extensions. If you program you cannot deny it is more work. Nor can you credibly deny many technical advantages of QT. \n\nWhen we looked at adding features to Quanta and looking for existing code to study and perhaps extend we quickly arrived at a consensus to restrict our design to C++ code for a wide variety of reasons. It may sound warm and fuzzy to say \"lets share\" but I have to believe this is being said by people not writing the code. It is even more borne out by the understanding of \"The Mythical Man Month\" where additional man power may effectively hinder productivity. Let alone environment differences. I recently read a criticism that C++ requires more planning. Studies have shown software projects with disproportionately large planning phases are done orders of magnitude faster.\n\nForgive my being blunt, but if you have not at least attempted to research the technical aspects, let alone coded... it seems without the benefit of some technical preperation one might choose to focus instead on non-technical suggestions? Speaking for myself I have no interest in working in C, CORBA or GTK. KDE/QT has spoiled me. It is getting tiring to me to read that I should sit down with people who want to do everything differently, adamantly dislike my language and want to use a different toolkit... the idea that this would produce anything but deadlock and the total absence of joy is to me absurd.\n\nMy preference though would be that if you feel strongly on how the KDE project should be coded that you get the book on KDE 2 programming (free on line) and have a look at the tools and the code and get involved. This seems the best input. The reality is that KDE is fun to program and you are welcome. As far as productivity goes... a short study with an open mind should lead anyone to believe that the KDE project is moving at an amazing speed today and will likely explode in the coming year. Why not get involved?"
    author: "Eric Laffoon"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-08
    body: "Did you read the text? It builds upon X\nand has nothing to do with KDE - Kurt just\ntried the latest X with KDE, you could use\nGNOME just as well."
    author: "coolo"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-07
    body: "Wow! This looks wunderful :-)\nCan anyone estimate until normal users can use this great feature?"
    author: "Henning"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-07
    body: "Wow ! Nice job !\nbut, if i had to recompile XFREE, and so recompile KDE.... I really prefer to wait binairies..!"
    author: "gravis"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-09
    body: "you don't have to recompile kde just X and qt\nthe thing that made all crash is /opt/kde/lib/hcstyle.so change it to some other\nstyle and it will work, but not very good at\nleast for me. all qt programs ran as they should\n. but not kde doesn't do it very well. all the programs in kdeui runs fine but kicker dies and konqueror to. what i got to work was kcontrol\nand drkonqi :) and it was huge i couldnt find the end of the window... i tried with kde 2.01 even after a recompile it was no diffrence don't wast your time yet.. i'll try again it we get a new qt patch"
    author: "AC"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-07
    body: "This is just truly stunning.  About 14 months ago I tried introducing a friend to Linux, and KDE. Kfm wasn't up to view any web sites with Javascript, so most of the time she used Netscape.\n\nHer one biggest complaint was the small size and quality of the fonts when viewing some (mostly microsoft friendly) web sites.\n\nAlthough she liked it, the not very pleasent browsing experience and the lack of an integrated Office Suite put her off and she went back to Windows.  \n\nI feel sure that if she'd been sitting in front of the above, with Koffice to play with, I might have succeeded.   Perhaps I'll give her another try next year when I can present her with an experience that includes anti-aliasing, alpha blending and KDE 2.1\n\nStunning work people.\n\nJohn"
    author: "John"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-07
    body: "Doesn't anyone think that some of these fonts are just a bit difficult to read?  I mean, sure, the text editor appears better (IMHO), but the font used on the KMenu just feels a bit off.  The same may be said about the application menus.  Antialiasing is fine and all, but I really don't think that I'd like to have <i>all</i> of my fonts fuzzied up.  This is something that I would definately want to be configurable on a per-app basis.<BR><BR>\nKonqueror... yup<BR>\nKOffice... probably<BR>\nKonsole... Definately<BR>\nbut the panel, menu, etc... probably not<BR><BR>"
    author: "Falrick"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-07
    body: "Maybe it's just a matter of choosing the right fonts..."
    author: "Marcos Tolentino"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-08
    body: "move the fonts you don't want to be antialiasied to a diffrent folder. and add it to your fontpath. only the fonts in the dirs specified in XftConfig will be antialiasied"
    author: "AC"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-10
    body: "No I would have to say that every font there is many times more legible than ive ever seen on an X display, but yes configurable on a per app basis is essential, for performace reasons more than legibility.  We should follow the BeOS approach, its almost configurable per app as far as Be's own base apps go, and then its up to the individual application developer beyond that."
    author: "Amibug"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-07
    body: "Kurt,\ncan you show us a screenshot of a website that DO justice to the anti-aliasing?\nWe have already seen the KDE.org ;-)"
    author: "reihal"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-07
    body: "That's great news for me, personally. I first came across an anti-aliased desktop when I started to use Acorn's RISC OS (and that was back in 1992!). In my experience, you get used to this display quality very quickly and suffer heavily when working on OSes that don't (at least properly) support it, may it be WinXXX, MacOS or even (forgive me :) our beloved KDE. It's cool to get anti-aliasing under KDE now. Hope it won't slow down the desktop too much. :)"
    author: "Guido Steiner"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-07
    body: "There are two issues that I have had with KDE/Gnome vs. Windows:\n\n1) Fonts, fonts, fonts - until very recently, fonts in X have been awful.  Legible? Yes.  Usable? Yes.  Painful after a while?  Oh yes.  Anti-aliasing is an absolute necessity in my book - it's why my laptop is running Win2K.  It's smooth as butter in the hot Texas sun with fonts.\n\n2) Office apps - KDE, and to a lesser degree Gnome have made great strides in the past year, but it's still not production quality, dependable office apps (Word processor, Spreadsheet, etc...)\nI believe that we'll soon eclipse MS Office, but there's still a ways to go."
    author: "Nick Ryberg"
  - subject: "Major Steps"
    date: 2000-12-08
    body: "Anti-aliasing has been riddled too often as 'eye-candy' and 'cosmetic'.\n\nBut 100% of the text I read on the computer should be easy to read. And, a large (>95%?)percentage of all text examined is text, not gifs, jpgs, or pngs.\n\nLarge text masses NEED antialiasing.\n\nKDEs, go for it! Go together with XFree86 and the Gnomes."
    author: "jwfh"
  - subject: "AntiAliased fonts with X/KDE/Gnome"
    date: 2000-12-08
    body: "This is truely sweet! I spend 8-10 hours in front my Linux box every day, and this will without doubt improve the quality of my life :-)\nI'm trying to piece all the components together, and hopefully I'll get this thing running on my Mandrake box without much trouble. I know it's very early for RPM's, but that would just blow my mind! :-)\n\nKeep up the good work! You guys are amazing!\n\nHC,\nDenmark"
    author: "Hans Christian Saustrup"
  - subject: "Why recompile X?"
    date: 2000-12-10
    body: "I thought the cool new thing about XFree86 4.x is that we can now simply plug in new modules into it's open architecture!  Why now with this?\n\nI know that this is early news, and it is /VERY VERY/ exciting, but I would think that there should be some sort of X module/extension that could be added to a working XFree86 4.x system without a complete recompile (QT, I understand).\n\n---jeff"
    author: "Gralem"
  - subject: "Re: Why recompile X?"
    date: 2000-12-23
    body: "You should try to go to the XFree site and read\nup on how the Render extension works.\n\nIn case you haven't figured it out yet, anti-aliasing is not exactly free, in terms of CPU power... and there is more to Render than just anti-aliased fonts.\n\nSo, if you want the full monthy, and have is supported in hardware where possible, you'd better believe you'll need to recompile XFree.\n\nOn the other hand, what Keith said is that it was mostly trivial to add anti-aliased font support to qt2 once X was done, because the design of qt2 is very clean, and you don't have to change calls all over the place: all the text API calls are conveniently put together."
    author: "Marc Espie"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-11
    body: "Well, I would love to try to get this working but it appears anoncvs.xfree86.org is down and has been all weekend.\n\nAnyone know any mirrors or whats wrong?\n\nGeoff"
    author: "Geoffrey Gallaway"
  - subject: "Re: KDE Anti-Alias Font Howto"
    date: 2000-12-18
    body: "It was down for a while, and should be back up now. I just downloaded the latest CVS from there and compiled it... one interesting thing is that XFree86 is called version 4.0.2 now... i think a release is soon on the horizon!!\n\nDijon"
    author: "Dijon Page"
---
<A HREF="mailto:granroth at kde.org">Kurt Granroth</A> has taken the plunge and compiled KDE and X with a number of the cutting-edge extensions which <A HREF="mailto:keithp at keithp.com">Keith Packard</A> has been <A HREF="http://dot.kde.org/975980174/">working on</A>.  The result of hours of compiling and following a few simple instructions:  a fully anti-aliased KDE session, with a <A HREF="http://devel-home.kde.org/~granroth/anti-aliased-kde.html">screenshot</A> to show how it looks.  It's not production-quality yet, but it's darn close!  His report follows.






<!--break-->
<h2>Using Anti-Aliased Fonts with KDE</h2>
I was recently able to compile and use KDE with fully anti-aliased
fonts.  This was due to the excellent work by Keith Packard and his
Xft extension to XFree86.  This was the result:
<p />
<div align="center">
<a href="http://devel-home.kde.org/~granroth/aadesktop.png"><img src="http://devel-home.kde.org/~granroth/aadesktop-small.png" width="205" height="154"
alt="Anti-aliased Desktop" border="0"></a><br />
<small>click to enlarge. 1024x768 (~136K)</small>
</div>
<p>
I'd like to say that the screenshot doesn't do justice to just
<em>how</em> much better the everything looks with anti-aliased fonts.
In particular, words can't easily describe just how much better web
sites looks in Konqueror.  It's like the difference between viewing
text in a book and viewing text on an old Apple II+
<p />
The best part of this all is that we didn't have to do
<em>anything</em> with KDE.  All that it takes is the XFree86 changes
and a patch to Qt and magically, all KDE applications use anti-aliased
fonts!  And I do mean all of them.  If you look at the screenshot,
you'll notice that even the menus, icon labels, and dialog text are
anti-aliased.
<p />
The only bad thing to report is that it doesn't seem to be ready for
prime-time, yet.  For instance, KDesktop flickers madly every time I
move the mouse.  Other KDE apps flicker huge amounts as well.  Also,
the fonts in konsole are completely and totally unusable.  Finally,
some web pages doen't display any text at all in Konqueror.  These are
all fixable problems, though, so by the time the Xft extension is
released, I'm sure everything will work great.
<p />
For the record, here is the basic steps I took to get this all to
work:
<ol>
<li>Got freetype2 snapshot from
<a href="ftp://ftp.freetype.org">freetype.org</a>.  Compiled and
installed it.  I had to edit the ft2build.h file and make an extra
symlink.. but other then that, it went smoothly.</li>
<li>Got XFree86 from CVS.  It's <em>huge</em> and takes hours to
compile...</li>
<li>Edited <tt>config/cf/host.def</tt> and added this line: <tt>#define Freetype2Dir /usr/local</tt></li>
<li>Compiled and installed with <tt>make World; make install</tt></li>
<li>Got Qt hacks from Keith
<a href="http://xfree86.org/~keithp/download/qtkernel.tar.bz2">http://xfree86.org/~keithp/download/qtkernel.tar.bz2</a></li>
<li>Got updated XftConfig (also from Keith): <a href="http://keithp.com/~keithp/fonts/XftConfig">http://keithp.com/~keithp/fonts/XftConfig</a></li>
</ol>
 
There were some other steps, but I don't remember them.  I also had to
recompile KDE since it appears that the Qt change makes Qt binary
incompatible (my guess) and the KDE apps all seg-faulted.




