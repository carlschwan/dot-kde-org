---
title: "People of KDE: Chris Schlaeger"
date:    2000-11-28
authors:
  - "Inorog"
slug:    people-kde-chris-schlaeger
comments:
  - subject: "FSF is evil!"
    date: 2000-11-28
    body: "So he's yet another RMS lover isn't he?\nJust like the rest of the KDE and Gnome camp.\nWhen do you people realize that GNU is bad?\nThe viral GPL for example, it stands for communism and is restrictive as hell.\nI can't even control my software, how is that freedom?\nAnd RMS, a greater jerk is does not exists.\nHe and his Free Shit Foundation are only looking for power and wants to own everything.\nLook at FSF's site, and read it for yourself: \"Why software shouldn't have owners\".\nAnd unlike the free licenses such as BSD, the GPL is like communism: \"you can't have this, it belongs to us\". Freedom my ass!\n\n<RANT>\nANYBODY DENYING THIS IS A LOOSER AND IGNORANT TO THE REALITY!\nIF YOU WANT TO SHOW YOUR LACK OF INTELLIGENCE, DENY THIS AND YOU'LL BE KILLED BY GOD!\n!@#$%&*^%$&*@!&(*%$@!\n</RANT>"
    author: "Slashdotter"
  - subject: "Re: FSF is evil!"
    date: 2000-11-28
    body: "Hey please be a bit more polite in this forum. This is not Slashdot ;-)\nBesides the theory communism is not bad!"
    author: "Henning"
  - subject: "Re: FSF is evil!"
    date: 2000-11-28
    body: "Me am stupid loser and stinks."
    author: "Henning"
  - subject: "Re: FSF is evil!"
    date: 2000-11-28
    body: "LOL, I'm glad this intelligent individual knows how to spell LOSER..."
    author: "LOL"
  - subject: "Re: FSF is evil!"
    date: 2000-11-28
    body: "Me is ignorant and dumb. HA HA HA HA!"
    author: "LOL"
  - subject: "Re: FSF is evil!"
    date: 2000-11-28
    body: "The rant tags are not neccessary, they are now automatically included by the idiot-ttp parser, sort of like the HTML tags in HTTP. Oh, and by the way, communism is a very good system, it's just been implemented badly. No, you will not be respected by your little display of a need to have an enemy in order to justify your existence.\n\nP.S. Please don't insult Slashdot by calling yourself a Slashdotter. No offense, but most slashdotters are decent people, and the idiots just stick out like sore, troll-shaped thumbs."
    author: "Carbon"
  - subject: "Re: FSF is evil!"
    date: 2000-11-29
    body: "Communism _is_ inherently evil.  I guess the old adage \"those who don't learn from history are doomed to repeat it\" applies here as well.\n\nChris"
    author: "Chris Bordeman"
  - subject: "Re: FSF is evil!"
    date: 2000-11-29
    body: "Don't confuse communism with Stalinism."
    author: "GlowStars"
  - subject: "Stupidity is evil."
    date: 2000-11-29
    body: "<p>The problem with communism is that it's an utopy. An utopy is a perfect system, thus, it can't exist in a non-perfect world. Our universe being non-perfect, communism just can't be correctly implemented. That's why it degenerate nearly instantanetly, like stalinism, maoism among other example, prove it.</p>\n\n<p>Oh, and Free Software isn't communist. Aren't RedHat, SuSE or Mandrake private companies selling products ? It's perfectly capitalism-compliant.</p>\n\n<p>Imagine, you the slashdotter, you just bought a white T-Shirt. A proprietary T-Shirt will prevent you from printing something on it, giving it to a friend or reselling it. And you won't even be able to repair it if it is torn.</p>\n\n<p>But enough of this. Everyone knows that it was a troll, and this slashdotter just want to create the longest thread of indignated post possible. You just can't discuss with this kind of people.</p>"
    author: "GeZ"
  - subject: "Stupidity is evil and so is sweeping generalism"
    date: 2000-11-29
    body: "Sweeping generalisation wouldn't fit! First My apologies as this is seriously off topic, but I can't let these uninformed comments on a perfectly viable political system go unchallenged.\nThe theory of communism is far from Utopian, and even if it were, to discount it merely because individuals are flawed would be stupid. To say a perfectly circular wheel is an impossible dream because your tools are too blunt to make one would be equally stupid when improving your tools is the solution. \n\nStalinism came about because a largely agrarian peasant nation attempted to leapfrog the industrial capitalist stage whilst fighting a civil war as well as armed intervention from the Eurpoean nations and the USA after the 14-18 war and that Trotsky was afraid that if he were to become leader after Lenins demise that the revolution would be painted as a Zionist plot (he was jewish - Lev Davidovitch Bronstein)! So not only were the populace not as sophisticated politically as their European counterparts, the econmic and productive capacity of the country was reduced from it's already parlous state due to the war efforts leading to the privations and desparate measures that enabled Stalin to purge all the Bolsheviks and communists from the Communist Party central commitee and thus bring about the cult of the personality which Lenin so abhored.\n\nPlease try and get a bit of knowledge of your subject before you discount it out of hand. As for the Troll.... Yawn!"
    author: "topicshifter"
  - subject: "Re: Stupidity is evil and so is sweeping generalism"
    date: 2000-11-30
    body: "__Every__ country that has ever adopted communism has very rapidly turned to Stalinist style tactics to maintain control.  This is not an incredible coincidence of specific circumstances in each country, as you say.  The problem with communism is not this lame 'perfection' argument, instead it is an unfounded belief in the innate goodness of the human soul, that the beurocrats in government given total control would suddenly not be greedy anymore."
    author: "Chris Bordeman"
  - subject: "Re: Stupidity is evil and so is sweeping generalism"
    date: 2000-11-30
    body: "What is wrong with the idea that everyone should work for the community, that people should share things, and no one person should be allowed more than another?  Nothing a marvellous idea, no stupidly rich people, no poor starving people.\n\nHowever.\n\nYou think you deserve more than another guy cos you work harder than he does, or, you decide that whatever you do, it makes no difference, so you contribute nothing, it all falls apart due to the greed that is human nature.  Look at ants, or species like that, seems to work there doesn't it?"
    author: "Anonymous"
  - subject: "Re: Stupidity is evil and so is sweeping generalism"
    date: 2000-12-01
    body: ">What is wrong with the idea that everyone should \n>work for the community, that people should share \n>things, and no one person should be allowed more \n>than another? Nothing a marvellous idea, no \n>stupidly rich people, no poor starving people.\n\nWell, nothing.  But my point is that is that it does not mesh with greedy human nature.\n\n>You think you deserve more than another guy cos \n>you work harder than he does, or, you decide \n>that whatever you do, it makes no difference, so \n>you contribute nothing, it all falls apart due \n>to the greed that is human nature. Look at ants, \n>or species like that, seems to work there >doesn't it?\n\nUm, yes I do think that if I take the initiative to produce, I should be rewarded more!  Is that some sort of trick question?\n\nAs for the ants, WTF???"
    author: "Chris Bordeman"
  - subject: "Re: Stupidity is evil and so is sweeping generalism"
    date: 2000-12-01
    body: "Who said that every one should get the same regardless of input? Another dumb misconception! As for human nature being inherently greedy... crap. Humans are inherently social. The basic form of human societies has been cooperative and, despite 'Market' pressures to be otherwise, on the whole still is. Capitalism itself is inherently monopolistic and only maintains the sham of competition through state intervention to curb those tendencies - just take a look at M$ and then tell me that this isn't true.\n\"From each according to his abilities, to each according to his needs\" (Marx - Comm. Manif.). That is the statement of intent. Equality of opportunity is the basis of communism. To enslave production to the greater good of mankind not the reverse. The bad application of the model does not invalidate the theory. Obviously this is the wrong place for this disscussion, so I suggest that before you make any more uninformed comment, go read the manifesto. I'm a socialist (no, really?) and there is no way I would condone a Stalin or a Caucescu (Romania) and nor would any socialist/communist I know! Do your selves a favour and learn a bit if only so that you can put me down in an informed way :-)"
    author: "topicshifter"
  - subject: "Re: Stupidity is evil and so is sweeping generalism"
    date: 2000-12-01
    body: ">Who said that every one should get the same regardless of input?\n\nYour quote.\n\n>As for human nature being inherently greedy... crap. Humans are inherently social.\n\nSure, social, but that self-control has taken many thousands of years to develope.\n\n>Capitalism itself is inherently monopolistic and \n>only maintains the sham of competition through \n>state intervention to curb those tendencies - \n>just take a look at M$ and then tell me that \n>this isn't true.\n\nYou are overgeneralising.  Capitalism has SHOWN to be a thousand times more competitive than communism.  The most successful economic models, including the American and Japanese ones, are based on Keynsian Economics which has as it's premise that economies are self-organising and that government intervention can only decrease their efficiency.  Sure, government intervention in a very limited way can help solve some inequalities, but our system of Capitalism has cleary trumped the Socialist one!"
    author: "Chris Bordeman"
  - subject: "Re: Stupidity is evil and so is sweeping generalism"
    date: 2000-12-04
    body: "<p>&gt; <i>The theory of communism is far from Utopian, and even if it were, to discount it merely because individuals are flawed would be stupid.</i><br>\nIt <strong>is</strong> utopian. It claims to be \"the end of history\", because it would permit an ideal civilization: all people on the Earth united under one single nation, taking the right decisions for the good of everyone. That's a dream, and when it comes true it's twisted into a nightmare. I don't think it's stupid to say peoples are flawed: I am, you are, and every thing in this world is. You can improve your tool as much as you wish, your wheel will never be perfectly circular.</p>\n<p>However, you can get <i>semi-perfect</i> things. Modern wheels are round enough for their use, so they can be considered perfect. All depends on the scope you take to look at your wheel; on a macroscopic sight it looks perfect, when analyzing it with a microscope you see it's not as circular as it ought to be. Similarly, a 100 to 1000 communsit community could work, but when bringed to the scale of a nation the size of Russia or China, flaws becomes too evidents.</p>\n<p>The problem is, the more power you give to the owner of a place, the more greedy people will want this place, and the less wise people will want it, because they do are conscious of their frailties and won't want to risk the fate of thousands of people by their possible mistakes.</p>\n<p>Other utopies existed. From the \"familist&egrave;re\" to the various fascism, all were claiming a new future for mankind, a new and better kind of men thanks to its dogma, and glorious tomorrows. Some tried to reach the perfection by fleeing away of society, other by imposing its model by force on everyones.</p>\n<p>I really think communism tried to export itself on the most possible nations in the world, and was promising a better future. That make it an utopy.</p>"
    author: "GeZ"
  - subject: "Re: FSF is evil!"
    date: 2000-11-29
    body: "Stallmanism."
    author: "Samantha"
  - subject: "Re: FSF is evil!"
    date: 2000-12-03
    body: "Wait a sec, doesnt it seem that the viability\nof political systems is a little off-topic\nfor this site? I really think that we should stop\nthis particular flame war, and get back to what's the real culture : the gift culture that the open source community runs on! Come on guys, the more time we spend arguing about communism, the less time we spend writing docs, drawing splash screens and icons, and coding!"
    author: "David Simon"
  - subject: "Re: People of KDE: Chris Schlaeger"
    date: 2000-11-28
    body: "[BEGIN RANT]\n\nWhy would we want another *BLOATED* piece of junk?\nAnd why the hell are you using shared libs?\nThat's the worst idea ever!\nEvery piece of software needs libkde.so and hundres of others!\nWHEN ARE YOU GOING TO REMOVE THE BLOATNESS!?!?!?!?\n\n[END RANT]"
    author: "RantMaker"
  - subject: "Re: People of KDE: Chris Schlaeger"
    date: 2000-11-28
    body: "I hate rants! Rants are always bad things!\nNO MORE RANTS!\nNO MORE RANTS!"
    author: "RantKiller"
  - subject: "Abusive articles/comments."
    date: 2000-11-29
    body: "Hello,\n\nPlease stop the abusive comments.  We have traced the IP address for most of these comments and they all point to the same traceable IP in .nl -- apparently from some person who has been posting/viewing the site from day 1.\n\nIf the abuses don't stop, we will be forced to take action (specifics as yet undecided).\n\nThanks,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Abusive articles/comments."
    date: 2000-11-29
    body: "Navindra, some light moderating on this forum would be appreciated, thank you."
    author: "reihal"
  - subject: "Re: Abusive articles/comments."
    date: 2000-11-29
    body: "Ban ?"
    author: "GeZ"
  - subject: "Re: Abusive articles/comments."
    date: 2000-11-29
    body: "I apoligize for my behaviour and I promise I won't do it again."
    author: "Anonymous"
  - subject: "Re: People of KDE: Chris Schlaeger"
    date: 2000-11-29
    body: "I for one would like to say, great job Tink, and I have learned a great deal from your interviews. I hope other people don't discourage you :)\n<p>\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: People of KDE: Chris Schlaeger"
    date: 2000-11-29
    body: "The new look of the site is also very pretty ;-)\n<p>\nBut when a lot of people get interviewed, it may be a bit clumsy to have an image for each person, a table with border is functional but still pretty."
    author: "Jason Katz-Brown"
  - subject: "Search in dot.kde.org is broken"
    date: 2000-11-30
    body: "A search for the category \"Interviews\" with no other arguments on dot.kde.org returns nothing :-(\n<p>\nJust letting you know."
    author: "Jason Katz-Brown"
  - subject: "Re: Search in dot.kde.org is broken"
    date: 2000-11-30
    body: "Actually, I realized this was in the \"community\" section (don't know why :-) , but still, on search page, it doesn't work... but if you click on the subject icon it works. oh well.\n<p>\ndot.kde.org does look good otherwise! Good job!"
    author: "Jason Katz-Brown"
  - subject: "Rik's dirty"
    date: 2000-11-30
    body: "desktop 'flotte sko' at http://www.geoid.clara.net/rik/wallpaper.html\n<br><br>\nsays\n<br><br>\n'Flotte sko<br>\nVill du knulle?'\n<br><br>\n[Posh shoes<br>\ndo you wanna fuck?]\n<br><br>\nI just don't understand why Rik writes anything like that. I mean, it's not even funny."
    author: "gjkjh"
  - subject: "Re: Rik's dirty"
    date: 2000-12-01
    body: "This a long inside joke from #kde\n\nSince I'm on insider on this inside joke, I know exactly how funny it is :)\n\nAnd it's supposed to be said:\n\"Nice shoes, wanna fuck?\""
    author: "Charles Samuels"
  - subject: "Re: Rik's dirty"
    date: 2000-12-01
    body: "ja, dessa norrbaggar, tsk tsk."
    author: "reihal"
  - subject: "Re: People of KDE: Chris Schlaeger"
    date: 2000-12-04
    body: "I absolutely LOVE Free Software, I love GNU and I *HATE* Communism !\nIt is silly to tell that people supporting and encouraging Linux, Unix and other Free softwares are for communism.\nI love capitalism and Free software can go with it. People who say that they must think communism was good because they agree with the GNU movement are not clever and absolutely ignorant about what was communism. Communism is something horrible which killed millions and millions of people !\nDon't tell that the organization of a country and the industry can be compared with the fact of being for the sharing of computing's programming !!"
    author: "Eric"
  - subject: "Re: People of KDE: Chris Schlaeger"
    date: 2003-03-24
    body: "Slashdotter is right. Many Linux users don't know history. Communism doesn't kill people, it kills ideas... like Linux. Nobody has any motivation to develop new products, there is no invention. There is no rivalvry, there is no reason to compete with somebody. Computer program is service and it's normal to pay for service...\n\nDon't lapidate me please..."
    author: "Marek Mojzik"
  - subject: "communism is the only sollution "
    date: 2005-10-15
    body: "communism is the only sollution for the nations to rise up and live up through this this discusting political meaning that rules this doomed world named CAPITALISM. You can only think of the past of this thing called capitalism and the only thing you will see, is nations fighting eachother without cause. Only for the profits of those who just wanted to make their bank accounts bigger. This is the imerialism. I can't look at the past 100 years and see anything good to this shitty political system, the moment that Russia had 20.000.000 dead people in world war 2 and practicaly they won the Germans. Stalin was devoted on    his people. Devoted on the declaration of the revolution of 1917. All workers of the world should be united to stop the world from going into war. We are millions, they are just a few. That's why a war against Russia started. A cold war. Trying to prevent other countries to follow up Russians example and make them less rich. So you see Communism is not utopian. IT IS THE ONLY SOLUTION FOR PEOPLE TO TAKE WHAT HE DESERVES. We all work 8-many many hours now for the rich people to get richer. We work, we produce so we should share the profits. Don't harry up to judge someone by listening the CNN. Start fucking readning. And yes after all this, LINUX IS COMMUNISM. It is free for everyone to use just like a cooking receipe. Those who try ta make linux profitable, those are the Capitalists. And i ask you. With whom you would like to be. With those who the only think they have been doing all these years was to guide nations to war for profits? Or with Socialism?(you can see Venezela for further instructions and plz stop only watching cnn and bbc and all these mind altering media)"
    author: "juno"
  - subject: "Re: communism is the only solution "
    date: 2007-03-15
    body: "my opinion!!\n\nthe true problem is the dysfunctional policy of mis-information, thats causing the border in peoples minds. read through half the threadsv about the topic and then snatch informations about marx engels hegel trotsky lenin and other originals. like reading the sourcecode from GNU software :) afterwards you _ARE_ informed\n\ntake the oppurtunity to change your kernel sources and set new C-Flags for your opinions buid ;)"
    author: "kollege"
---
<a href="http://www.kde.org/people/chris.html">Chris Schlaeger</a> is this week's guest at the popular <a href="http://www.kde.org/people/people.html"><i>People</i></a> section of KDE's web site. <i>Tink</i> asks Chris about his work on KDE and on the KDE League, about his near ones and much more. <i>Tink</i> has also put a new lovely presentation on her section. <a href="http://www.kde.org/people/people.html">Go</a> take a look.

<!--break-->
