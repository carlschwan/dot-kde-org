---
title: "AudioCD KIOSlave Debut"
date:    2000-11-24
authors:
  - "wes"
slug:    audiocd-kioslave-debut
comments:
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-24
    body: "great!\nnow someone should do this the other way round.. dragging an .iso image or file to the cdrom should burn it... just a dream or possible? :)"
    author: "Spark"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-24
    body: "Now that would be very cool!\n\nWith the speed these guys produce new code, I wouldn't be surprised if someone announced something like that in a few days :-)\n\nBut anyway this is also very cool - if you read further in the thread, you can see what it further should do. It should be able to encode them directly to Ogg Vorbis files (and of course also get the names from CDDB or/and one of the alternatives)."
    author: "Joergen Ramskov"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-24
    body: "Cool Idea. But i think that since burning requires some settings, it would be better if just a program would be started with the file allready inserted (KOnCD?)\nThe other way round would also be cool. (Dragging a whole CD-> create iso-image)"
    author: "Worf"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2004-04-15
    body: "you can always just right-click it and send it to k3b :)\n\nAlthough a blankcd:/ kioslave might be handy."
    author: "troy the wonder ape"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-24
    body: "That would be great, just like burning CDR's with OS/2. As I remember it you could drag a bunch of files to a folder and then request it to start the physical process in the control-menu of the folder, when you were ready."
    author: "jimbo"
  - subject: "Another way to burn a cd"
    date: 2000-11-24
    body: "right click on a directory\nselect \"make cd\"\nenter title\nmaybe check some boxes\nclick start\n\nThis would be very nice."
    author: "Dirk Manske"
  - subject: "Re: Another way to burn a cd"
    date: 2000-11-24
    body: "Whoa. wait.\n\nIf you want this, I want an option to ssh to my server for the actual burn process, because the Cd writer is in my server and my desktops areo nly NFS slaves. :-)\n\nSo I want to be able to specify the command line that does the actual burning, like \"ssh server 'cdrecord ...'\"\n\nAnd of course the burn must happen in the background, so no broken CDs when the X server segfaults, for example.\n\nWant to take on a feature request battle? ;-)"
    author: "Jens"
  - subject: "Re: Another way to burn a cd"
    date: 2000-11-24
    body: ">So I want to be able to specify the command line that does the actual burning, like \"ssh server 'cdrecord ...'\"\nUse Controll Center to configure your \"remote\" cd-recorder...\n\n>And of course the burn must happen in the background, \nThis is not a real problem. Yet another daemon.\n\n>Want to take on a feature request battle? ;-)\nYes, this would be a good battle. :-)"
    author: "Dirk Manske"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-24
    body: "What about dragging mp3's to the cdburner and burning an audio cd?\n\nPracticly it should open a window with list of currentl \"dragged\" files and a \"burn\" button, and letting you rearrange the order of the songs, and showing the remaining lenght of the cd, and ..."
    author: "Rene"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-24
    body: "... and if this \"window\" is actually a view at the filesystem (for example an own directory with symbolic links to the selected files), then we are very close at emulating BeOS!\n\nAnother dream of mine is to drag mp3's directly to the device MPMan/RIO/whatever. No additional program nor another interface needed. Now that would be cool :-)"
    author: "Beat"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-24
    body: "There's already a Diamond RIO ioslave :)  Look in kdenonbeta."
    author: "wes"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-24
    body: "<p><i>Another dream of mine is to drag mp3's directly to the device MPMan/RIO/whatever. No additional program nor another interface needed. Now that would be cool :-)</i></p>\n\nAnd your dream has become true - take a look\n<a href=\"http://dot.kde.org/970662831/\">here</a>!"
    author: "Joergen Ramskov"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-25
    body: "A pop3 KIOSlave does already exist, doesn't it? So theoretically it wouldn't be too difficult to implement something like 'pop3:/jjan@humane-software.com', which would show all e-mails that are in my mail box. This would be really cool as well. You could even bookmark your mail box.\n\nOr how about that: Enter smtp:/someone@foo.com in the adress bar of Konqueror and drag & drop files on that window, which would send the files to someone@foo.com."
    author: "Jeremy M. Jansary"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-25
    body: "You can already do this.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-24
    body: "I was reading this email and then this idea came to me. Play CD's -digitally!\n\nWhy you ask? simple, my friend - here are some advantages:\n\n* if you have a good sound card (Yamaha YMF724/744/754, Sound Blaster Live, Trident NX/JX) - then you can play your cd's with 4-5 or AC3 speakers support!\n\n* With analog CD playing - you hear your cd played with 2 speakers (even if you have AC3 or Dolby Pro).\n\n* With analog CD - you can only play with the volume, bass, treble. With Digital playback - you can have tons of other effect - the only thing you'll need is some program which can play with the sounds setting - something like XMMS Mixer got.\n\n* With analog CD - your CDROM plays the CD. If you got some micro scratches - then, depends on your CDROM, it may jump from the scratch or not. With Digital CD player - you can set it with error correction since you read the tracks sector by sector.\n\nIf you want to see what I mean - then run Windows 2000/Windows ME and select your CDROM from the hardware properties and select the option to play CD's digitally - you'll definately hear the differences.\n\nHetz (HeUnique)"
    author: "Hetz Ben Hamo"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-25
    body: "Yeah, play CD's digitally, but there is something VERY important to consider:\n\nwhen playing audio CD's as analog, the CD-ROM drive reads CD at normal audio CD speed.\n\nwhen playing audio CD's as digital, the CD-ROM drive considers it is data, so it runs full speed. With a 40 X drive or faster, it's VERY VERY noisy! It's even possible it hides all the additional pleasure to get some nice audio effects...\n\nSo before playing audio CD's digitally, let's control CD-ROM drive data read speed!"
    author: "zecoolforest"
  - subject: "Re 2: AudioCD KIOSlave Debut (precision)"
    date: 2000-11-25
    body: "It's me again, I just want to precise that controlling CD-ROM drive read speed is certainly not something that can be done in KDE, but perhaps more in the Kernel. So while KDE is multiplatform, it means not only Linux kernel, but also xBSDs and others..."
    author: "zecoolforest"
  - subject: "Re: Re 2: AudioCD KIOSlave Debut (precision)"
    date: 2000-11-25
    body: "I probably didn't explain myself..\n\nWhen you play CD digitally - you actually \"rip\" the CD at the speed of X1 - which means 150k - so all the noises that you know when you read data at X40 speed (I have sort of drive like that so I know what you mean) - will not be there..\n\nHetz"
    author: "Hetz Ben Hamo"
  - subject: "Re: Re 2: AudioCD KIOSlave Debut (precision)"
    date: 2000-12-06
    body: "hdparm is the utility that controlls CD speed in Linux. You too can turn your nifty 72x cd into a 1980's quality 1x reader!"
    author: "Genki"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-25
    body: "I think AlsaPlayer has the ability to play CDs by reading the audio digitally. It has a speed control slider and it would be impossible to play at for example 150% speed if the playing was analog."
    author: "Maarten ter Huurne"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-25
    body: "That's really nifty.\n\nIf I recall, BeOS had something like this.  There was an app (called cddasomething) that did the CDDB lookup.  The app could be set to run in the background, but was a little tough to configure.\n\nAnyway, this had major advantages for the apps, as they could all treat CD tracks as seperate wave files... very cool.\n\nI hope this develops into all that it could have in Be..\n\nWay to go KDE guys!!"
    author: "Ben"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-25
    body: "Great, but how do you install a kioslave?. If the kioslave or plugin is available in binary form, how do I install it?. I am not talking about this one in particular, but in general. \nAt this rate, there will be more and more kioslaves being programmed, for the weirdest things, which I cannot think of, to the most common . All users would not need all of them, just a few for their needs. Some may be specialized (like the smb kioslave. Many people would be using this but many would use kde on networks without windowsNT or samba such as home users. They would not need this). There should be a common place to find such plugins and kioslaves and a common way to install them if it is simple enough (like in netscape, 'just untar it in the plugins directory'). Almost all sites for apps that have plugin support have a place for downloading the latest plugins (such as http://www.xmms.org for example or any browsers like netscape (http://home.netscape.com/plugins). Konqueror should be no exception. I don't think apps.kde.com is the right place for this. How about konqueror.org? On second thought, since the kioslaves affect the whole desktop, it could be under kde.org itself."
    author: "rsv"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-25
    body: "Great, but how do you install a kioslave?. If the kioslave or plugin is available in binary form, how do I install it?. I am not talking about this one in particular, but in general. \nAt this rate, there will be more and more kioslaves being programmed, for the weirdest things, which I cannot think of, to the most common . All users would not need all of them, just a few for their needs. Some may be specialized (like the smb kioslave. Many people would be using this but many would use kde on networks without windowsNT or samba such as home users. They would not need this). There should be a common place to find such plugins and kioslaves and a common way to install them if it is simple enough (like in netscape, 'just untar it in the plugins directory'). Almost all sites for apps that have plugin support have a place for downloading the latest plugins (such as http://www.xmms.org for example or any browsers like netscape (http://home.netscape.com/plugins). Konqueror should be no exception. I don't think apps.kde.com is the right place for this. How about konqueror.org? On second thought, since the kioslaves affect the whole desktop, it could be under kde.org itself."
    author: "rsv"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-26
    body: "well, beos already beat kde to this, but this time it wasn't by all that long.  i was actually messing around with this in beos earlier on today.  but, i have to be truthfull, beos still handled it better, you could mount your audio cdrom, and then looking into the folder, find a README file, and setup file (used to configure how the audio is mounted) and both a cdda folder and a wav folder, going to the cdda folder showed the regular cdda files (track01.cdda or something like that) and looking into the wav folder showed (supprise!) track01.wav and even told me how big it would be when copied.  beos also allows you to drag and drop the immaginary wav files into any program that can edit them, and the program will work just like they were read only wav files!  now thats cool.\nlook here for the info:\n<a href=\"http://www.be.com/products/freebeos/latestrelease.html\">http://www.be.com/products/freebeos/latestrelease.html</a>"
    author: "CiAsA Boark"
  - subject: "Re: AudioCD KIOSlave Debut"
    date: 2000-11-26
    body: "That's really cool... By doing this on the filesystem level, all apps benefit from it. If this could be done on Linux, both KDE and GNOME (and even things like mc and bash :)) would get instant cd-audio encoding capabilities..."
    author: "Haakon Nilsen"
  - subject: "FSF is evil!"
    date: 2000-11-26
    body: "GNU is communism!\nRMS is a jerk!"
    author: "Anonymous"
  - subject: "Re: FSF is evil!"
    date: 2000-11-26
    body: "Sure...."
    author: "Ill Logik"
  - subject: "Re: FSF is evil!"
    date: 2000-11-27
    body: "And Mr. Anonymous here is a real coward ;) Jeeez!"
    author: "RIchard Stevens"
  - subject: "Re: FSF is evil!"
    date: 2000-11-27
    body: "I am an ingnorant bozo.\nHAR HAR HAR."
    author: "RIchard Stevens"
  - subject: "Re: FSF is evil!"
    date: 2000-11-27
    body: "That't the reason we all love them. :)"
    author: "El Pepo"
  - subject: "Re: FSF is evil!"
    date: 2000-11-27
    body: "Me am dumb.\nMe sucks and stinks."
    author: "El Pepo"
  - subject: "Re: FSF is evil!"
    date: 2000-11-27
    body: "Me am dumb.\nMe sucks and stinks."
    author: "El Pepo"
---
For those of you who like to stay on the bleeding edge, <a href="mailto:rik@kde.org">Rik Hemsley</a> announced that <a href='http://lists.kde.org/?l=kde-devel&m=97501225027717&w=2'>he's officially checked in an 'audiocd:' kioslave</a>.  Provided you have the cdparanoia libraries installed, this will allow you to browse your audio CDs in Konqueror, and convert audio tracks (with error correction!) to wav format by simply dragging them to your desktop.  Great stuff!  Hooks for CDDB support have been included, but the actual code has yet to be written.