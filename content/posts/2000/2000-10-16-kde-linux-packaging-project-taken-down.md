---
title: "KDE Linux Packaging Project Taken Down"
date:    2000-10-16
authors:
  - "eseveringhaus"
slug:    kde-linux-packaging-project-taken-down
comments:
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "Ivan is a great guy.  I've followed his project from day one, and the amount of work he did and amount of dedication he has shown is simply impressive.  I urge the one person who has been stressing out Ivan so badly to please calmly apologize.\n<p>\nIvan has mentioned on the list that he *will* be producing KDE 2.0 packages for Debian Potato, although he will probably not be maintaining all the other non-core applications.  Ivan was doing over 100 packages -- 6 hours of work per day -- it was only the hugely positive feedback that kept him going.  He now simply does not have the time and resources to continue and this unfortunate incident was the last straw so to say.\n<p>\nIvan has mentioned that many many people use the packages he produced but not many have offered to help him.  If you are unhappy about this, then please consider contacting Ivan and packaging it yourself.  He has offered to sponsor new Debian developers to take over the task.  If you are serious enough about this, <a href=\"mailto:navindra@kde.org\">contact me</a> and I'll get you in touch with Ivan.\n<p>\nMy sincere personal thanks to Ivan for all the work and dedication he has put into KDE packaging, and still does.  I am one of those people who have made heavy use of his work.\n<p>\nNavin.\n\n\n\n"
    author: "Navindra Umanee"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "just a note.  I have gotten a ton of email from alot of people who are upset about this.  So far none of it negative.  I want to make it clear that the negative comments come from about 1% of the community...it's just that this 1% is always the percentage that is the loudest.  This only because they are saying that what you are doing is bad or wrong or [insert negative comment here]...\n\nAnyways...because of all the nice comments I had decided to make the KDE 2.0 potato debs available...or rather continue to make them available.  They will be uploaded (hopefully) to KDE and be apart of the distro tree along with all the other pacakges..."
    author: "Ivan E. Moore II"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "Ivan: Glad to see you've changed your mind. I hope that you can keep that 1% in perspective. There will always be those you cannot please no matter what you do. Personally, I have always felt that pissing off a jerk is just a sign of good taste. Don't worry about it, they're not worth your effort. And they certainly don't represent the 99% of us who do appreciate the efforts of yourself and other open source contributors."
    author: "David Zeitler"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "I would like to take a moment to thank Ivan for all the great he has/and will be doing in the future for kde and debian.<br>\nIvan you have done a great job in filling the void between debian and kde, and allowing people like myself to have a debian packaged version of kde and the betas of kde2.<br>\n<br>\nGordon.<br>"
    author: "Gordon Heydon"
  - subject: "KDE needs more volunteers, not only..."
    date: 2000-10-17
    body: "Not only the core code writter, but also ones to do the easier work...like localization..\n\nI am only a person who cares about KDE.\n\nThanks to you, IVAN"
    author: "Coupon"
  - subject: "Re: KDE Linux Packaging Project: I came in late..."
    date: 2000-10-17
    body: "Hi, I've only been using Linux and KDE since March. Is there some place I can find out about KLPP and its history?\n<br>I've seen a related message posted at <A href=\"http://lists.kde.org/?l=kde-devel&m=93050714332441&w=2\">http://lists.kde.org/?l=kde-devel&m=93050714332441&w=2</A>, by Troy Engel, and assume this is the project. If so, it seems a mighty effort.\n<br>As far as I can see, KLPP is not related to the SuSE Linux KDE Service at <A href=\"http://www.suse.com/us/support/download/LinuKS/index.html\">http://www.suse.com/us/support/download/LinuKS/index.html</A>\n<br>Is this correct? Are the two efforts related or unrelated?\n<br>Also, is the SuSE Linux KDE Service still up and running?"
    author: "Paul Leopardi"
  - subject: "Re: KDE Linux Packaging Project: I came in late..."
    date: 2000-10-17
    body: "You might be able to piece some of it together from the other <a href=\"http://developer.kde.org/news/weekly/\">KDN</a>.  Look for items mentioning debian or packaging. KDN's now dead unfortunately, but this site can pretty much fill that void IMHO.  :)\n<p>\nCheers,<br>\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: KDE Linux Packaging Project: I came in late..."
    date: 2000-10-18
    body: "Thanks for the tip. I get some idea of what the project was from the items in KDN, but most of these items contain links to KLPP which are now dead, so I can't dig in to any detail. I'm not looking for packages, just announcements, so I can understand the history and significance of the project and its relation to LinuKS, if any."
    author: "Paul Leopardi"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "Hi Ivan - just a brief but heartfelt \"thank you\" from down here in New Zealand....\n\n ( Mmmm ... it's the old story that \"it's easy for someone to criticise\" , but not-so-easy for that (criticising) person to get up and do work to make things better. ) \n\n ( I'm pretty much a \"newbie\" - I lurk around lists here and there , and make a positive suggestion where I can.  I'm very much of the mindset that if you can't say something positive about a person (or their work), say nothing at all ... :-)  \n\nOh well .. that's my 2 NZ cents worth (about 0.0000004 cents US ... :-)"
    author: "Andy Elvey"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "Hello Ivan,<br>\n<br>\nI've been using your KDE packages since \"slink\", and thank you very much for making these packages available.<br>\nI'm totally amazed just how much you have done and are doing for the KDE and Debian projects.<br>\nOn another note I think that the development projects like KDE should be helping Debian in the production of packages.<br>\nRegards that person I wouldn't like to be in their  shoes right now! My advice to him applogise! Before the bounty hunters come looking for you.<br>\n<br>\n<strong>Thank you Ivan!</strong><br>\n<br>\n<i>Just another 2 NZ cents worth (or about 0 US cents with rounding!)</i>"
    author: "Dale"
  - subject: "Reminds Me of my Ex-boss(es)"
    date: 2000-10-18
    body: "People who critisize like that seem to think it shows \"leadership qualities.\" In the past, I've worked for bosses who could never find anything positive to say about my work and when confronted told me that that type of critisism was \"a motivational tool.\" Perhaps this guy who critisized Ivan's work believes that some corporation will see how he \"handles people\" and snap him up as their new CEO. After all, if a person doesn't know how to do a job, the next best thing is to know how to order someone else to do it. Therein lies the road to management. :-)"
    author: "Ron Tarrant"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "What a idiot! I can't understand that people dedicate such a big amount of their time to flame the people that dedicate an even greater amount of time to make the idiots be able to flame.\n.. well I didn't understand that part either ..\nThe fact is, anyone who flames a hard working open source developer should be taken and shot imidiately -\n  We the people of the open source community are in a big symbiant circle: Kernel developers could not function without the guys behind the compilers, and vice versa. Everybody needs everybody else to help out, and the more help, the better!\n  I encourage (and demand that) the idiot that flamed this perticular developer to stand up and post a formal apology - Here.\n  \n  Sm\u00e1ri P. McCarthy"
    author: "Sm\u00e1ri P. McCarthy"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "What a idiot! I can't understand that people dedicate such a big amount of their time to flame the people that dedicate an even greater amount of time to make the idiots be able to flame.\n.. well I didn't understand that part either ..\nThe fact is, anyone who flames a hard working open source developer should be taken and shot imidiately -\n  We the people of the open source community are in a big symbiant circle: Kernel developers could not function without the guys behind the compilers, and vice versa. Everybody needs everybody else to help out, and the more help, the better!\n  I encourage (and demand that) the idiot that flamed this perticular developer to stand up and post a formal apology - Here.\n  \n  Sm\u00e1ri P. McCarthy"
    author: "Sm\u00e1ri P. McCarthy"
  - subject: "Ups?"
    date: 2000-10-17
    body: "Sorry about the double posting - my browser went heywire!"
    author: "Sm\u00e1ri P. McCarthy"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "can ya possibly blame him?  thats a lot of work, and if people (even a few people) are gonna act like that, who would want to do it?"
    author: "dingodonkey"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "You are a great guy. Good luck in whatelse you will do by now.\n\nDKJ"
    author: "Somone that can't help"
  - subject: "Why not use SourceForges compile farm for this ???"
    date: 2000-10-17
    body: "Hi,\n\nSubject sais it all. As I have understod, KDE's CVS is hosted by SourceForge. There is a possibility to have shells and run daily scripts which grabs the changes made to KDE cvs and builds binaries for the distros that are supported in the compile farm (RH,Deb,...)\nThe script could then send a log of the compilation to kde-devel. Then upload the binaries/packages to kde/upcoming something.\n\nIt has to be done once I guess...\n\nComments ?"
    author: "Sam"
  - subject: "Magnum?"
    date: 2000-10-17
    body: "Don't know if this is a good idea or not, but it seems (from reading a thread lower down) that theKompany have an installer which also will (eventually) do ftp and web updates.<br>\nDon't know how \"free\" this is, but what about this as the basis of a KDE installer?<br>\nThe link posted in that thread is: http://www.thekompany.com/products/powerplant/magnum/"
    author: "Ezz"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "Hello \n\nFirst I would like to mention that I think a few people have missed the point and I really don't want to get involved in a flaming war. \nBut, in my opinion the way that is being done is not what he is talking about, although thanks for the suggestions, the problem is that the open source community is moving away from the consept of \"I don't like it, so I will fix it\" and they are moving towards \"wait for the company or person to make a new release and just tell them the problem\". My personal opinion on that is, go back to microsoft if you want to wait for the company to fix your problem. Linux is a society that was built on this consept, of fix it yourself. Unfortunally coming with the popularity of linux is the people that where happy to sit back and wait for the fix to come to them. \nIf you have a problem fix it, before we lose the consept of linux and become another microsoft or sun or any other company that we are trying not to be. People will help if you don't know what your doing because it will benefit the community. \n\nStop complaining, and start helping, work together. For 10 years we worked this way and look how far we have come. \n\nThanks Ben Woodhead\nps. Ivan, I know the feeling, I wrote some configure scripts for different libraries and people just complained and never helped, and I didn't know anymore then anybody else but I was trying to get the problems resolved. Makes you wonder how we got this far."
    author: "Ben Woodhead"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "First of all thanks to Ivan for making the great KDE packages available.\n\nNot to reply to above comment. You are right.. the linux community was built on do it yourself. But it was also built on \"Use it and tell me what sucks and make some suggestions and if you have the expertise send me some patches\"\n\nAll the software in the opensource world is written to solve a need most commonly of the author of the program. (The exception might be companies sponsering opensource projects). But it is always helpful to get feedback and comments and bug reports to improve the software in addition to patches.\n\nSo long as people realise that the people who are writing the software that they are using are doing them a favor and communicate with that perspective in mind, there is no reason why the comments can't be constructive.\n\nBut of course if people want to come out and just say that what's being done is wrong without providing a solution, well then the best thing to do is ignore them.\n\ncheers,\n\nAC"
    author: "AC"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "Sorry, you are right feedback, comments and suggestions are very helpful and I should have done a better job explaining. Comments that are bugs, suggestions, or even complaints (complaints are best given with a potential sollution. ie I don't like the way this works, perhaps this would be a better way) about the software are all exceptable and helpful. \nThe problem is the flamming, and complaining in a non-constructive way. Its hard to make a comment and insure that you are not upsetting someone, and is a good idea to re-read you comment before sending.\nIf your having a problem with packages not being done fast enough you have the right to do it yourself, a comment in this regard really can't be helpful. It's complaining about not getting it first, or when you want it. In that case compile yourself or help pack.\nThanks Ben Woodhead\nps Sorry about the misunderstanding."
    author: "Ben Woodhead"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "Things were like that long before Linux, and it bothers me that so many people tag so many things to Linux.  Some people (not necessarily any of you) think GNU and Linux were the first open source projects.  Where am I going with this?  Way back in the day there was a great deal of this spirit of fix-it-yourself and share-it-in-hopes-somebody-will-make-it-better.  Now that open source/free software is starting to go commercial (im not going to target any specific project) it is losing that free spirit.  When you start packaging and selling copies is when people EXPECT the versions to be made and updated for them, and dont expect to do it themselves.  And I dont think its fair to expect them to share the spirit of free software and contribute to it.  Oh well, I'm probably missing the point, but regardless, this holds true to some respect, whether to this scenerio or not."
    author: "dingodonkey"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-18
    body: "I think you have found a very good point. Compare this thread to the one about PR for KDE at\n<A href=\"http://dot.kde.org/971269290/\">\nhttp://dot.kde.org/971269290/</A>\n<br>In that thread, I said that to attract novices, KDE 2.0 would need friendly support. \"We (experienced users) need some self-restraint. Novice users (and hopefully there will be many) don't yet know all the rules of etiquette (RTFM, etc.) so we MUST be patient and gentle. Otherwise they will tell the rest of the world about their negative experience.\"\n<br>In the current thread, I see some users of KDE being called: \"dumbasses\", \"lazy\", \"jerk\", \n\"idiot\", \"moron\", \"should be taken and shot\", \"Ignore them. Or laugh at them.\"\n<br>Admittedly, these users are the ones who don't (or don't yet) understand the ethos and etiquette of open source.\n<br>An analogy is when AOL users started using Usenet. There were immediate flame wars. Novice users did not know what a FAQ was, let alone where to find it. I'm sure that many of them were called \"idiot\", \"moron\" or even worse.\n<br>The main point I'm trying to make is that open source projects, when they become mainstream and scale to millions of users, go from having a customer base of do-it-yourselfers, to a customer base of real customers. Real customers expect professional service. This implies the need for professional packaging companies and professional customer relationship management.\n<br>People who complain and can't or won't fix it themselves should and in many cases do have the opportunity to pay someone else to fix it.\n<br>For the record, I have put in bug reports on KDE and have sent feedback to SuSE. I've spent over AU$240 on SuSE distributions this year (6.3, 6.3, 7.0) and do expect more from SuSE than just being called an \"idiot\", \"moron\", \"lazy\", \"dumbass\", \"jerk\" and being told to fix it myself. I don't have the time to fix it myself. That's why I buy the distributions, download the RPMs and report the bugs."
    author: "Paul Leopardi"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "Anyone who expects anything out of free software is a moron. If you haven't paid for it, then you can't expect it to work. (Which is why I'm constantly amazed by Linux :-).\n<p>\nBUT. There are a lot of morons on this planet. They wont go away in the next ten years. IMHO the best way to deal with them is not to. If they manage to affect you in a bad way with their loud rantings then you need to ask yourself why.\n<p>\nIgnore them. Or laugh at them. But don't quit because of them. If everyone did that then there would be none of all this great free software!\n<p>\nMorons can be quite amusing sometimes. Especially when you put Shockwave/Flash in their hands! :-)"
    author: "Per Erik Stendahl"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "That's <B>crap</B>; I've used a lot of free software that had better support than chargable software. Whethr free or chargable, it's up to you to check out the software and the track record of the supplier. I'd much rather trust a critical process to free software written and supported by a man that I trust than rely on shrinkwrap from a company with a lousy track record.\n<P>\nThat said, free software is done pro bono publico; you have no right to more support than the author chooses to provide. If you don't have the talent, time and training to fix a problem yourself, you can at least be polite when reporting it, and supply the author with adequate documentation. If it makes your life easier, a few words of praise wouldn't hurt either.\n<P>\nAs to Ivan's quitting; it's unfortunate, but he's the only one with standing to decide. It would be nice if he changed his mind, put he's entitled to pull out if he's getting more crap than he cares to put up with. AFAIK he has made no commitments and has no obligation to continue. I wish him the best."
    author: "Shmuel (Seymour J.) Metz"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "<I>Especially when you put Shockwave/Flash in their hands!</I>\n\n\namen to that!"
    author: "dingodonkey"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "Could someone tell me where to look to figure out /how/ to build packages?"
    author: "Ted Berg"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-18
    body: "Try checking the web pages for your distro. Debian for debs, redhat for rpms, etc.\n\nErik"
    author: "Erik Severinghaus"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "Well, it is a shame to be discouraged by the 1% of the people because 99% of the people think that your efforts have been great.\n\nI'm not an expert linux user and when I look at complex C++ code, scripts, etc.. my mind goes numb and I get dizzy.  BUT, I do know enough to keep my system running. That makes me one of THOSE people who takes everyone's hard work for granted and doesn't give back enough.  So thank you EVERYONE here who has made KDE and Linux a great operating environment, from the core developers to the packagers. Great Job."
    author: "RedTile21"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "Hi From France Ivan !!\n\ni've discovered you're work 3 weeks ago !\nAnd my first opinion was : \"great job ! best job\" \n\nso clap clap again Ivan, don't be upset by those guys, redirect their mail to /dev/null\n\nok, now, i'm not a debian maintainer but if you need assistance, contact me, and i will help you.\n\nbest regards."
    author: "St\u00e9phane"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-17
    body: "I'd like to join all of the people who are sad to see the KLPP go down.  It's an unfortunately familiar story -- I believe that the Molnarc site for Mandrake rpm's of the KDE 2.0 alphas went down for pretty much the same reason.\n<p>\nI took advantage of the KLPP site when I was running my old 486 with Debian.  It didn't have enough disk for my work and dev sources (290 Mg of disk after swap, 16 Mg of RAM), so I didn't run compiles myself.  Many packages I was interested in were available in source form or RPM's only, until KLPP came along.\n<p>\nThanks alot, Ivan.  Your efforts were much appreciated."
    author: "Andy Marchewka"
  - subject: "Hard Work"
    date: 2000-10-17
    body: "First off, many thanks to Ivan for filling a niche to provide loyal Debian users with the alternative of using KDE2 without having to go through a compilathon every few days to keep current.  I realize the shear amount of work this has to be.  However, I think this leads to a problem with KDE that the Gnome camp seems to have figured out,  I am talking about the \"Helix Gnome\" project thingy....They provide a plethora of updated packages from Gnome to a variety of different distributions...Both RPM and DEB based...No small amount of work in itself...However they have a plan to somehow make money by doing this.....Yet Ivan has been packaging up KDE2 for free and for the community -- with no intended payback...And for that CHEERS...And for all the other implications that this comment may hint at --- cough....LSB....cough..easier packaging for the less advanced users...(I don't know how many people have tried to build a complicated series of RPM's or DEB's....but it aint no walk in the park...And would be high on my Wishlist if I were to have enough money to pay a team of coders looking for something to improve the community.)"
    author: "Dusty T"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-18
    body: "<p>I'm the person who Ivan Moore is refering to. This thing started when I asked him if he would be packaging Quanta since that's something the KDE distribution doesn't have included in it and many people would find it to good use. He said he wouldn't package it and that he has enough on his plate. I asked him why he doesn't find someone else to do this then and his response was a whole lot of bitching about how come <i>I</i> don't package these (I'm rock stupid when it comes to compiling programs, let alone packaging them) and why does *he* have to find someone, etc. My point simply was that he <i>is</i> (well now he's a <i>was</i>) <i>the</i> Debian guy at the KDE Linux Packaging Project and he shouldn't be surprised if people expect him to provide KDE DEBs. If you volunteer for such a position, i.e. the Debian guy of the KLPP, then if you can't do all of it, you should have the decency to find someone to lessen the workload off you. I know that when I volunteer for something, I consider it my responsibility to make sure it gets done one way or another (e.g. with the help of someone else).</p>\n\n<p>Obviously I caught Ivan at the wrong time and there definitly must have been other things building up for him to overreact in this way to a little criticism (and if there wasn't, then Ivan is an extremely weak and sensitive person... why would you quit doing something over a few minute's worth of comments from one person?). FYI, I <i>have</i> thanked Ivan for his work in the past, although most of the time I just e-mail him whenever I'm having trouble with his DEBs in case he hasn't noticed (and lately I <i>have</i> been a little peed off at his sloppiness at the Debian servers... I shouldn't have my KDE/Debian system completely trashed, for days at a time, while doing a dist-upgrade... that has never happened until Ivan got access).</p>"
    author: "Bart"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-18
    body: "Umm...I never volunteered to be THE debian\nguy for the KLPP...I volunteered to be A debian guy for the KLPP...which means that I contributed to it...I started it..meaning I built a place for packagers to put packages.  I packaged over 100 packages as a bonus to the core packages I did volunteer to do.  This was all stated in the Debian section of the KLPP.  The index page showed who had volunteered to help with the project...no where did it say this was the ONLY people nor did it say that they volunteered to do everything..just that they volunteered to help package.\n\nAlso...what do you mean \n\n\".. I shouldn't have my KDE/Debian system completely trashed, for days at a time, while doing a dist-upgrade... that has never happened until Ivan got access)\"\n\ngot access to what?  I've been packaging the only official KDE Debian packages for almost 2 years now.  And..if you built a Deb from source...guess what..you built it with the scripts I worked on.\nWhat broke is that KDE 2.0 until yesterday was considered BETA.  don't blame the fact that beta packages croaked on me.  If you dojn't like the fact that your system could possibly get trashed, then don't run unstable programs!@#  come on..."
    author: "Ivan"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-18
    body: "Bart, I think there is one very important thing that you should really keep in mind here: free software is a gift, given to the community, including you, by people who spend a lot of time and energy on doing that. It is very fortunate that there is a lot of free software out there these days, much of it being of excellent quality. This includes KDE.\n<p>\nBut this doesn't change the basic fact that it is a gift! You may accept it happily and use it. You are most welcome to make similar contributions yourself or make useful suggestions. These are always well received. However, you are <it>not</it> in a position to expect or demand things. It is also not helpful to tell others how they should what they do. And you might want to think twice before making depreciatory comments about someone else in cases like these.\n<p>\nRead the second paragraph of your posting again, step back a little and then think what other will think about this matter, and about you."
    author: "Tobias Gloth"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-20
    body: "I agree.\n<br>\nI think the \"how can we get better PR\" thread should concentrate on things like this instead - make it clear that it's tough if it doesn't work, just like with MS products, only cheaper\n<br>\nThat way, these misunderstandings won't happen\n<br>\nThe problem is, of course, there is plenty of webspace given over to buggy opensource software\nand self-promoting ex-programmers who quite often  make someone think the software is more than it is\n<br>\nI hope KDE will sooner stop this trend rather than add to it.<br>\nIt won't be long before we start getting xemacs features to auto email slashdot whenever a new .cpp file is created :o)"
    author: "Michael"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-18
    body: "Bart,\n\nI think that you say the right words here:\n\n\"I'm rock stupid...\"\n\nYes, you are."
    author: "Anonymous"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-18
    body: "turn in your flippers & tux, Bart .\nYou ain't no penguin."
    author: "penguins can fly"
  - subject: "Constant bitch'in"
    date: 2000-10-18
    body: "Well IMHO, this flame fest got out of hand unusually early, the most well know flame fest (that I know of) took about 2 years (acording to the dates on the messages) on UseNet.\n\nBut since it's here, why not participate? ;)\n\nThe packaging project for KDE is constantly getting a bigger problem: So many packages, and not enough packagers. As Ivan said, he packaged over 100 packages plus the core, and that is quite a lot for one man to hack. When looking over the distribution download sites I noticed that Debian has a lot more .debs than SuSE and RedHat (for instance) have of .rpms, and that brings up the old distro wars, but they're beside the point: The debian packaging takes a lot longer time that the RPM's because of the magnitude of files, but yet there are a shit load more people working on the RPM packaging - Why? Couldn't the people working on the RPM packaging lend a little of their time for the debian packages? Maybe not.. how about making an automated script to do the job on a old 486 sitting in somebodies corner?\n\nWell, I don't know how the packaging people can keep up with this load of work (I'm just a humble translator =)\n\nThe fact is, nobody realy want's to do this job unless they recive thanks, and nothing but.\nSo stop flaming the people that do the job, or start RTFMing so you can DIY! I don't bitch on the people doing the job (even though I had a heck of a time installing beta 5 RPM's on my SuSE box), so why can't you people leave'em be..\n\nThanks for not taking the time to read this - I just had to get it off my chest!"
    author: "Sm\u00e1ri P. McCarthy"
  - subject: "Re: Constant bitch'in"
    date: 2000-10-18
    body: "I think some human interaction is needed for a lot of these overly-sensitive people. This guy hasn't even seen me before nor talked to me on the phone and he's personally hurt by my criticism? Geesh... how thin can your skin be?"
    author: "Bart"
  - subject: "Learned in Kindergarten..."
    date: 2000-10-18
    body: "Well, he clearly <B>does</B> feel hurt by your\ncriticism, and there's nothing that you can do \nto change that.  To quote from \"All We Really Need\nto Know, We Learned in Kindergarten\":\n<BLOCKQUOTE>\nSay you're sorry when you hurt somebody.\n</BLOCKQUOTE>\nAnother useful bit of wisdom for the open-source\ncommunity:\n<BLOCKQUOTE>\nWhen you go out into the world, watch out for traffic, hold hands, and stick together.   \n</BLOCKQUOTE>"
    author: "Fred"
  - subject: "Re: Constant bitch'in"
    date: 2000-10-19
    body: "just compare to this situation:\n\nyou give some bucks to a begger, and then the begger complains you are not giving enough.\n\nI feel ashamed that such people exist on earth."
    author: "maddog"
  - subject: "Re: Constant bitch'in"
    date: 2000-10-20
    body: "Well IMHO, if packaging for debian is so complicated just package for the other distros...\n<br><br>\nDebian will then have the choice to attract  maintainters themselves or go without.\n<br><br>That'll piss on their strawberries.<br><br>\nThe slimy toads didn't even want to entertain KDE a month ago...why kill yourself doing stuff for them now?"
    author: "Michael"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-19
    body: "When something is marked \"Beta\", you can expect it to trash your system.  Be very suprised, and pleased if it doesn't.  And keep a copy of another Window/Desktop manager on your system so you have a better chance of being able to recover from the results of installing Beta software.  I use Gnome for this purpose.\n\nRemember, KDE is free software.  Free software is about people working together and sharing their efforts to put together a great system.  I'm not very good at programming, so my contribution to free software is to help other users by replying to their queries in newsgroups.  Other people contribute by translating, packaging, designing icons or whatever.\n\nYou can't expect people who volunteer their time to give a project the same dedication as those who do it for a living.  If you wan't something which no one has volunteered to do, either do it yourself, or pay someone who knows how to do it to do it for you."
    author: "Jonathan Bryce"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-19
    body: "<i>(and lately I have been a little peed off at his sloppiness at the Debian servers... I shouldn't have my KDE/Debian system completely trashed, for days at a time, while doing a dist-upgrade... that has never happened until Ivan got access).</i><br>\n<br>\nJust a quick question... The debian packaged version of kde, is only for use with woody which at this point in time is <b>unstable</b>, At this point in time woody is the most <b>unstable</b> version of <b>unstable</b> that I have ever used. And also at the moment kde 2.0 is <b>unstable</b>. If you want a stable version of debian please down grade to potato which is a stable version of debian.<br>\n<br>\nAnd just one other point, Debian package maintaners do this for the love of it, nothing else. These people don't do it for the glory. there is no glory in going to work everyday for about 6-9 hours and then comming home to debian for another 9-11 hours most days.<br>\n<br>\nJust think about it.<br>\n<br>\nGordon."
    author: "Gordon Heydon"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-19
    body: "There is one point I am really missing in all this.\n<br>\nWhy aren't Debian packagers paid?\n<br>\nAre any packagers, of KDE or anything else open source ever paid?\n<br>\nIs the volunteer status of Debian packagers an exception or is it the rule?\n<br>\nAre SuSE packagers paid? \nIf they are, why the difference with Debian packagers? Is it economics or history?\n<br>\nIf everyone is always a volunteer, how come some open source companies actually employ people and pay them?\n<br>\nSee my other remarks on SuSE and other packagers in this thread. I have no idea if my remarks are right. I'm new to this open source thing, and obviously there is a history and a whole ethos that I just haven't learnt yet."
    author: "Paul Leopardi"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-20
    body: "SuSE, Red Hat, Mandrake, Caldera, Corel etc are commercial companies who employ staff and make money from selling their products.\n\nSoftware in the Public Interest, who produce Debian, is a charity which aims to promote the use of free software."
    author: "Jonathan Bryce"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-20
    body: "Aha! Thanks. The penny has now dropped. Debian and packaging for Debian is must be very dependent on volunteers. \nFlaming a volunteer is a very very bad idea in this circumstance, because their support network consists of other volunteers, who may not have the time or the skills to take over if they quit."
    author: "Paul Leopardi"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-18
    body: "This is a pity. I think the open source community needs better ways of rewarding and sustaining volunteers, recognizing when they need help, and fixing process problems as they come up. \n<br>Also, at some point, volunteer packaging efforts may need to morph into professional efforts, with paid staff. I don't know if the SuSE LinuKS effort is volunteer or professional, but judging from the rest of the SuSE web site, I'd guess it is professional. See <A href=\"http://www.suse.com/us/support/download/LinuKS/\">http://www.suse.com/us/support/download/LinuKS/</A>\n<br>See also my comments on packaging, PR and customer relationship management <A href=\"http://dot.kde.org/971680096/971796213/971813027/971828478/\">here.</A>"
    author: "Paul Leopardi"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-18
    body: "This is a pity. I think the open source community needs better ways of rewarding and sustaining volunteers, recognizing when they need help, and fixing process problems as they come up. \n<br>Also, at some point, volunteer packaging efforts may need to morph into professional efforts, with paid staff. I don't know if the SuSE LinuKS effort is volunteer or professional, but judging from the rest of the SuSE web site, I'd guess it is professional. See <A href=\"http://www.suse.com/us/support/download/LinuKS/\">http://www.suse.com/us/support/download/LinuKS/</A>\n<br>See also my comments on packaging, PR and customer relationship management <A href=\"http://dot.kde.org/971680096/971796213/971813027/971828478/\">here.</A>"
    author: "Paul Leopardi"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-18
    body: "Sorry for double post. I'm using IE 5.0 at work and it fooled me into re-sending the form."
    author: "Paul Leopardi"
  - subject: "A SuSE KDE2 packaging and documentation effort!!!"
    date: 2000-10-18
    body: "I've found Silvio Sch\u00fcrer's SuSE KDE2 RPM page at <A href=\"http://www.keywarrior.net/silvio/kde2/index.en.html\">http://www.keywarrior.net/silvio/kde2/index.en.html</A>\n<br>This is another mighty packaging effort. It looks like it has been superseded by the SuSE RPMs on <A href=\"ftp://ftp.kde.org\">ftp://ftp.kde.org</A> but it contains a gem of documentation which I have been asking for for about a month - the instructions on how to install the SuSE RPMs. This level of detail was missing from the SuSE README file. The instructions may now be out of date, but at least I now know that they existed, including the step which says \"first install ALSA\".\n<br>Thank you, Silvio!!!!\n<br>Now if you or someone could just update this doco and copy it to the KDE and SuSE web sites, ready for the release of KDE 2.0, there would be fewer SuSE users with installation problems."
    author: "Paul Leopardi"
  - subject: "What's wrong with procmail?"
    date: 2000-10-18
    body: "I don't understand - if somebody annoys you why not simply set up procmail to filter out the annoying messages?\nYou are not obliged to read everybody's e-mail.\nman procmailrc for details."
    author: "Pavel Roskin"
  - subject: "what the hell takes so much time?"
    date: 2000-10-18
    body: "if it takes so much time to maintain packages, why the hell not just compile the tar.gz's?\n\nWhy waste so much time just in packaging?  If it's not an automated process, or automatable, forget it!  I don't understand this waste of time anyways, so perhaps this is for the better?"
    author: "Torsten Howard"
  - subject: "Re: what the hell takes so much time?"
    date: 2000-10-18
    body: "1) A lot of people cannot compile themselves\n2) If everybody should compile the mailinglists would not be 200 mails a day, but perhaps 2000++ mails a day with people asking the same questions or reporting the same bugs.\n\nSo the packer is doing a favour to the community like the coder or the people writing the documentation. \n\nOne could take your argument further, and say why code? Why not let everybody out there make its own software for him/herself? That would not make sense either...\n\nI'm not using KDE, but I imagine that it must be a lot of work to put the packages together.\n\nPreben"
    author: "Preben Randhol"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-18
    body: "I'm a 'baby newbie' at this point.  All I really know is how to successfully load 'Redhat Linux' 5 times out of 6 on average, and not much more.  What concerns me here is attitude.  There is a \"problem\" in the Open Linux community over this situation with Ivan.  If there never are 'problems'in a creative situation, it wouldn't be creative.  Naturally in the universe, the appearance of destructiveness during a creative cycle takes place - with people too.  I'm just starting this adventure in the Linux Universe.  I'm having to constantly remind myself to relax and enjoy, even the tough moments.  I'm glad to be part of the creative universe of Linux."
    author: "Chris Madden"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-18
    body: "Ivan: Your experience and your reaction to it sounds right on to me. The Open Source community needs to take a hard look at its ugly, selfish underbelly, understand its limits, and grow up. At it's worst, it is an egotistical, self defeating mob of leaderless, egotistical jackasses who are hurting an otherwise noble endeavour. Cf. Lord of the Flies, boyz. Eric Raymond's \"gifting culture\" is bullshit."
    author: "Dennis Eberl, Founder of BFLUG"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-19
    body: "I just read others' comments to Ivan and am very impressed by the healthy generosity and genuine appreciation directed toward him. I hope my remarks are not misunderstood. For they appear to paint all members of the Open Source community with the same negative brush. To do so is inaccurate and unjust. I am trying to express the idea (as one of the posters put it so well) that the whiners should either get to work or (to paraphrase) \"go back to MS Windows.\" I will add, however, that others seeing this \"one individual\" drive a good person over the edge should probably have been more forcefull in their defense of his efforts and in their condemnation of that lone, misguided individual. I may be off base on this and if I am, please accept my apology, but I bet not many spoke up in Ivan's defense while the flaming was going on. (This was one flame war I missed, thank you.) In my experience, most are afraid to stand on a principle, afraid to take a leadership role, afraid to get involved in a heated argument. They sit passively by as a few people flame the shorts off of one another. Often, the flammers are all jerks, but in this case Ivan clearly was not and his attacker was. Again, I don't know if such \"High Noon\" passivity was the case here, but I bet it was. If you see a good person getting  stomped, it's your job to jump in and put an end to it without a lot of howdy-do's and oops, you go before me's. Timidity is a greater danger to the Open Source movement than the odd wrong headed, arrogant S.O.B. Respectfully, Dennis Eberl, Founder of BFLUG, ( http://www.bflug.org )."
    author: "Dennis Eberl, Founder of BFLUG"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-19
    body: "You really don't know what you're talking about. This wasn't some public flame war. I had just vented my frustration with KDE and Debian with Ivan in private and it just happened to be that I was the straw that broke the camel's back. I *have* apologized to Ivan since that has happened and he seems to be taking care of things very well. Haven't you noticed that Ivan wasn't the only one at the KLPP? Yet apparently he was the only one who was active. And haven't you noticed that the site hadn't been updated much anyway?"
    author: "Bart"
  - subject: "Re: KDE Linux Packaging Project Taken Down"
    date: 2000-10-24
    body: "Dear Ivan, I'm only a spanish student of Computer Science. Initially I used Linux because it is the de-facto OS in my faculty (Debian dist). In the faculty we used the FVWM window manager but this wm is horrible for Windows & Mac users (and for not too). Then, I decided to install, in my PC, KDE 1.1.2 and I was in the heaven: a nice GUI very intuitive with well-integrated apps. In addition, my faculty has decided this year to install the Potato release with the KDE desktop as the default window manager, and now, in this moment I'm downloading the KDE 2.0 packages THANKS TO YOU, IVAN. I've been waiting this moment anxious.\nThere are MANY people that love your work (me too).\nFrom here, I want to encourage you to keep alive the KLPP project. I would like to help you, but I'm very newbie in this fantastic and complex world, although if the task is very simple I will be available for you.\n\nGreetings from Spain."
    author: "Djinn"
  - subject: "KDE Linux Packaging Project Taken Down"
    date: 2005-09-11
    body: "Hey listen I know it makes you angry for one person who talks out of his ass. But still there are a lot of people like me who switch from windoze to linx and we know all the hard work that you and others put in on something on your own time. So here is a person that would like to say thank you for all your hard work. Just don't let one horse persude you from  doing something you want to do"
    author: "Ron (Seeker) LaRue"
---
Ivan E Moore has pulled the plug on the KDE Linux Packaging Project. On his <a href="http://kde.tdyc.com/">page</a> he explains the reasons, citing a lack of time and the fact that he received little help from the community, along with "one person".

<!--break-->
<p>
&lt;rant&gt;<br>
This happened with <a href="http://freshmeat.net">freshmeat.net</a> a while ago, it has happened to countless projects, and I'm *tired* of dumbasses flaming developers/packagers/webmasters/whatever who volunteer their time to OpenSource projects. Stop bitching and fix it. The full text of the notice is below. <b>Note</b>: He says he is still maintaining the kde2 packages for Debian Woody.
<br>&lt;/rant&gt;<br>
<p>
<i>
"I've decided to kill the KLPP and my packaging of anything other than the Debian woody version of KDE 2.0. The reason is due to 1 person. I know you are saying..but it's just one person. Well...I volunteer my
time and one person getting stressing me out because I don't do enough to make his life easier is one person too many. I started the KLPP to help others out. I figured since I'm doing alot of this for my wife I'll throw it up
for others to benefit. Well, aparantly this dumbass believes that isn't what I started the KLPP for. His belief is that I voluntered to take care of all KDE packaging for Debian and if I can't do it myself, to find someone who
can. 
<p>
Well, I have to say this is the one drawback to an opensource volunteer world. The fact that people are used to being able to expect a level of perfection from the people producing things they use. We created a
lazy world and you can't be lazy in the opensource world. You have to be willing to take imperfection and either live with it or take an active role to make it better. 
<p>
I'm just tired of this. I have so much on my plate right now with the packaging of KDE 2.0 for Debian woody and the other packages I maintain. And that's just my Debian duties. And don't take this as an
opportunity to say that I'm whining or trying to put a guilt trip on anyone. Screw that. I have taken these tasks on as my responsibility. For these tasks I will go out of my way to make sure they are done. The KLPP I
never made such an agreement. Next to nobody has stepped forward to help...I know there are tons of people using the resulting packages...but no one packaging anything. 
<p>
For this I say goodbye on this plane. I'll leave the files up for at least a week. And maybe once KDE 2.0 is officially released and I get the woody stuff worked out I might build some potato packages for you all."
</i>







