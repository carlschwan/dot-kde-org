---
title: "Claudia Garad, Executive Director of Wikimedia \u00d6sterreich: \"We want to create a welcoming atmosphere for newcomers\""
date:    2018-05-29
authors:
  - "Paul Brown"
slug:    claudia-garad-executive-director-wikimedia-österreich-we-want-create-welcoming-atmosphere
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/20160526_Fotoprojekt_Oesterreichischer_Film_Claudia_Gar%C3%A1d_IMG_5726_LR10_by_Stepro_1000.jpg"><img src="/sites/dot.kde.org/files/20160526_Fotoprojekt_Oesterreichischer_Film_Claudia_Gar%C3%A1d_IMG_5726_LR10_by_Stepro_1000.jpg" /></a><br /><figcaption>Claudia Garad, Executive Director of Wikimedia Österreich. Photo by Stepro.</figcaption></figure> 

<i>Claudia Garad is the Executive Director of Wikimedia Österreich, Wikipedia's Austrian chapter. Claudia will deliver <a href="https://conf.kde.org/en/Akademy2018/public/events/79">Akademy's second keynote on Sunday, 12th of August</a>.</i>

<i>Claudia graciously met up with us (<span style="color:#0a0;font-weight:bold">Ivana</span> and <span style="color:#f55;font-weight:bold">Paul</span>) to tell us all about her job, how the Wikipedia community works and the challenges it faces.</i>

<i>This is what she told us:</i>

<span style="color:#f55;font-weight:bold">Paul:</span> Welcome, Claudia, and thank you for joining us!

<span style="color:#55f;font-weight:bold">Claudia:</span> Thanks for having me :-)

<span style="color:#0a0;font-weight:bold">Ivana:</span> Hello Claudia!

<span style="color:#f55;font-weight:bold">Paul:</span> So you are the Executive Director of Wikimedia Foundation Austria, correct?

<span style="color:#55f;font-weight:bold">Claudia:</span> Correct. Since 2012. It's actually called "Wikimedia Österreich". The Foundation is only the organization in San Francisco

<span style="color:#f55;font-weight:bold">Paul:</span> Thanks for the clarification. Tell us... What led you to this job? Did you do something similar before?

<span style="color:#55f;font-weight:bold">Claudia:</span> I used to work as Head of Marketing and Communication for a major applied science organization in Germany. We were pioneers in the field of online science communication in the German-speaking world. Beyond the focus on online communication, I think the common denominator of those two jobs is making knowledge accessible.

<span style="color:#f55;font-weight:bold">Paul:</span> Of course. What does a typical day at Wikimedia look like for you? What do you do there?

<span style="color:#55f;font-weight:bold">Claudia:</span> I'm not sure I have a typical day. We work closely with volunteers, so our working hours vary. We often work in the evenings or on weekends when our Wikimedians are available. I also not only work from our office, but frequently remote when I travel for work.

<span style="color:#f55;font-weight:bold">Paul:</span> So do you oversee their work? Make sure the rules for editing articles are respected? Organize events? All of the above?

<span style="color:#55f;font-weight:bold">Claudia:</span> Wikimedia staff does not intervene into the work on the Wikimedia projects. The community decides about the rules and how to enforce them; we do not have any direct influence there.

But the task that follows me everywhere and at any time is to secure funds for our organization, i.e. fundraising, grant-making and reporting. Apart from that, one of my main tasks is to build partnerships within the Wikimedia movement, but also beyond. With like-minded communities, cultural institutions, potential donators, and so on.

<span style="color:#0a0;font-weight:bold">Ivana:</span> I take it that you face the challenge of working with people from different time zones. Could you share some advice or tools that you use to overcome scheduling issues?

<span style="color:#55f;font-weight:bold">Claudia:</span> I don't think we have super-innovative approaches in that regard. For us in Austria, it's mainly Europe and the US so far, and we found the time slots that work for most. I think the Wikimedia Foundation has probably more refined ideas, as they work with a more diverse group, but I wouldn't know the details.

<span style="color:#f55;font-weight:bold">Paul:</span> Talking of diverse, I understand you also deal with diversity and inclusion issues. How do you promote these two things?

<span style="color:#55f;font-weight:bold">Claudia:</span> Due to our "hands off" approach, we can only deal with diversity and inclusion issues indirectly: by raising awareness for the topic, encouraging mentorship, fostering solidarity networks among volunteers, and providing incentives and support for all of that. One example is the mentoring program we developed for the Wikimedia Hackathon last year. We wanted to create a welcoming atmosphere for newcomers that is reflected in the physical space, as well as in the social interactions.

<span style="color:#f55;font-weight:bold">Paul:</span> Is there a lack of diversity within the Wikipedian community?

<span style="color:#55f;font-weight:bold">Claudia:</span> It always depends on the definition of diversity, and it varies between our communities. Speaking for our Austrian communities: it is diverse in some regards, like age, and not very diverse in other, such as gender or ethnical background.

<span style="color:#f55;font-weight:bold">Paul:</span> So do you know what percentage of women Wikipedians versus men there are, for example? The percentage for each ethnicity? Is this information you collect?

<span style="color:#55f;font-weight:bold">Claudia:</span> There are roughly 10% female contributors in the German-language Wikipedia, and that reflects what I see during offline events. Non-binary is probably around 1-2%. But the numbers are not all 100% accurate, as many volunteers choose not to disclose their gender, and we respect their wish for anonymity. That is even more true for ethnicity - we do not ask for that anywhere. This is what you can get from the information people provide on their user pages. Apart from that, we do not collect any personal data.

But there are other ways to make the diversity gaps visible: by comparing the number of biographies about females to the number of articles about men. Wikidata makes that really easy nowadays. Or by looking at the language and perspectives that are represented in articles. It becomes obvious very quickly that we have a problem there, and that should be fixed if we strive to collect "the sum of all human knowledge" as our vision statement says.

<span style="color:#f55;font-weight:bold">Paul:</span> How do you solve this problem? Getting back to the activity you mentioned before, for example - how do you make a hackathon more welcoming? What do you physically do?

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/1200px-20140415_TFM_Edit-a-thon_Wien_0301.jpg"><img src="/sites/dot.kde.org/files/1200px-20140415_TFM_Edit-a-thon_Wien_0301.jpg" /></a><br /><figcaption>Wikipedia Hackathons implement special activities and spaces to encourage diversity. Photo by Clemens, CC BY-SA 3.0.</figcaption></figure> 

<span style="color:#55f;font-weight:bold">Claudia:</span> For the first time we had mentors at such an event. Their only job was to help newcomers and to pair them with other newcomers according to common interests. Usually the mentors had project ideas that were suitable for newbies to get started. The aim was that every newcomer could be part of a team that accomplished something during the weekend, and to be able to present a project at the showcase on the last day.

To make it as easy as possible to approach people, we also had a mentoring area where people could come at any time to ask questions or get help. Our Austrian community held pre-events, so people could get to know each other in smaller, more intimate surroundings before they were thrown into an international event with 250 strangers. Finally, we had an outreach coordinator who facilitated the mentor-to-mentor and mentor-to-mentees exchange before, during, and after the event.

Other ways to make event spaces inclusive are gender-neutral bathrooms, designated "quiet zones" where people can retreat to when they need a break from social interaction, stickers to customize your name badge with information about yourself that can also include how you want to be addressed in terms of gender, etc.

Many of these ideas were adapted from a youth hack event called "Jugend Hackt" that is a project of Open Knowledge in Germany and Austria.

<span style="color:#0a0;font-weight:bold">Ivana:</span> It sounds like you're really taking care of new contributors, which is awesome! It's something we're trying to be better at in our community, too. Could you tell us a bit about the onboarding process - what does it look like when someone new wants to join and start contributing? Are there any "best practices" or recommended ways to get started?

<span style="color:#55f;font-weight:bold">Claudia:</span> We learned that the best way to onboard newcomers is regularity; it's hard to achieve much with a single event. So having mentors beyond the event helps, or having regular events or follow-up events, where people can come back to when they encounter barriers. It can be further assisted with social media - chat groups and the like. Places where people can find help and advice on short notice online.

<span style="color:#0a0;font-weight:bold">Ivana:</span> Have you had any students or new contributors join Wikimedia Österreich through mentorship projects like Outreachy, Google Summer of Code or similar? Do you organize similar programs on a local scale, i.e. in the German-speaking communities?

<span style="color:#55f;font-weight:bold">Claudia:</span> We have had newcomers join via local mentoring programs, but not via the global programs you mentioned.

<span style="color:#0a0;font-weight:bold">Ivana:</span> Do you have something like a list of "junior jobs" or easy tasks that newcomers can immediately tackle? Or if you've tried a similar approach in the past, can you tell us how that worked?

<span style="color:#55f;font-weight:bold">Claudia:</span> We tried the easy task list for the Wikimedia Hackathon last year. The list was linked from the event page so people could check it out beforehand. Apart from that, there were also other tasks to help around the event that were not related to coding: writing blog posts, making a podcast, taking pictures, helping the organizers on site...

<span style="color:#0a0;font-weight:bold">Ivana:</span> Getting back to the topic of helping newcomers, you mentioned potential barriers they can encounter. In your experience, what are the most common barriers, or obstacles that newcomers have reported? And how have you worked on resolving them?

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/Elevate_Festival_2016_-_Internet_Policies_and_Activism_in_Europe_-_09_-_Claudia_Garad_x1500.jpg"><img src="/sites/dot.kde.org/files/Elevate_Festival_2016_-_Internet_Policies_and_Activism_in_Europe_-_09_-_Claudia_Garad_x1500.jpg" /></a><br /><figcaption>Claudia takes part in the "Internet Policies and Activism in Europe" panel at the Elevate Festival 2016.</figcaption></figure> 

<span style="color:#55f;font-weight:bold">Claudia:</span> I think for most newcomers the hardest part is to see where they could help and how. So the task list and mentors can help with that. However, we also still have room for improvement: After the hackathon, many newcomers complained about how long it took to get a code review. Often keeping people engaged after an event is the hardest part. For newcomers and mentors alike.

In the end, it is a question of resources. If we want new people, and especially underrepresented people, we will have to invest resources into this endeavour. Half-assed approaches usually don't work in the long run, and I'm afraid that this is something we still have to internalize as a movement.

<span style="color:#f55;font-weight:bold">Paul:</span> What about problems from the old-timers? Is there any resistance from the existing community towards the effort to promote more diversity?

<span style="color:#55f;font-weight:bold">Claudia:</span> Of course there are parts of the community who are indifferent, and some who openly work against such topics. So the art is to find the people who support the idea and include them, to address justified concerns, and ignore, or if there is no other way, get rid of people that display toxic behavior.

<span style="color:#f55;font-weight:bold">Paul:</span> What sort of problems do you see a lack of diversity causing?

<span style="color:#55f;font-weight:bold">Claudia:</span> For Wikipedia it is clear: the sum of all knowledge can not be gathered and represented by a small homogeneous group. Furthermore, quality and objectivity of knowledge are also important values in our movement that can only be achieved by including diverse perspectives.

<span style="color:#f55;font-weight:bold">Paul:</span> For somebody who wanted to join in the Wikipedia effort... What advice would you give them? What should they read? Where can they start?

<span style="color:#55f;font-weight:bold">Claudia:</span> Most Wikipedias have extensive guides on how to get started. Too extensive sometimes :-). I would see whether there is a mentoring program on your Wiki project and sign up, or whether there are local Wiki meet-ups in your home town. In Vienna, for example, we have a Wikipedia clinic for newcomers every first Tuesday of the month.

<span style="color:#f55;font-weight:bold">Paul:</span> A Wikipedia clinic! What do you do there?

<span style="color:#55f;font-weight:bold">Claudia:</span> It's basically where you can come to discuss and find help for common problems. I think there are code clinics at some events too. It's a peer approach to exchange best practices around common issues or challenges.

<span style="color:#f55;font-weight:bold">Paul:</span> Is there a trend? Like problems that new contributors come up with again and again? If so, what are they?

<span style="color:#55f;font-weight:bold">Claudia:</span> I think the challenges for newcomers vary between the projects. In the German language Wikipedia, the biggest issues are certainly the complexity that results from an elaborate rule set to ensure quality of content; the fact that most topics of general knowledge are fairly well covered so you need to find your expert niche to contribute; and the often not very newcomer-friendly atmosphere and aggressive interactions.

<span style="color:#f55;font-weight:bold">Paul:</span> I suppose people feel possessive about what they work on. Is there any sort of regulatory body that helps resolve disputes or reprimands antisocial behavior?

<span style="color:#55f;font-weight:bold">Claudia:</span> There are community-elected arbitration committees to solve conflicts on projects. But in some cases, especially when there is also offline harassment involved, the Wikimedia Foundation has to take steps to ban those people from events, the projects, or both in order to protect others.

<span style="color:#f55;font-weight:bold">Paul:</span> I guess it is normal that in such a big community you will have all sorts of people...

Moving on to happier topics. Apart from actually writing or expanding Wikipedia articles, what are other things contributors can do to help Wikipedia grow and thrive?

<span style="color:#55f;font-weight:bold">Claudia:</span> Other ways to contribute to Wikipedia are to help build the software behind MediaWiki, or to take freely licensed pictures for Wikipedia & Co and upload them to Wikimedia Commons. There are also all the other sister projects such as Wikivoyage, Wiktionary, or Wikidata.

<span style="color:#f55;font-weight:bold">Paul:</span> I guess donations also help, right? Where can we go and donate?

<span style="color:#55f;font-weight:bold">Claudia:</span> Of course - to keep Wikipedia ad-free and independent, that is probably the easiest way to contribute. You can either <a href="https://wikimediafoundation.org/wiki/Ways_to_Give">donate to the Wikimedia Foundation</a>, that distributes the money among the global communities or, if there is one, to <a href="https://wikimediafoundation.org/wiki/Local_chapters">your local Wikimedia organisation</a>.

<span style="color:#f55;font-weight:bold">Paul:</span> Claudia, thank you so much for your time. 

<span style="color:#0a0;font-weight:bold">Ivana:</span> And we look forward to your keynote at Akademy!

<span style="color:#55f;font-weight:bold">Claudia:</span> Thanks! Looking forward to meeting you in person!

<i>Claudia will be delivering the keynote at <https://akademy.kde.org/2018>Akademy 2018</a> on the 12th of August. Come to Akademy and find out live how you too can make your community more diverse and inclusive.

<h2>About Akademy</h2>

For most of the year, KDE -- one of the largest free and open software communities in the world-- works on-line by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2018">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.

You can join us by registering for Akademy 2018. <a href="https://events.kde.org/">Registrations are now open</a>.</p>

For more information, please contact the <a href="https://akademy.kde.org/2018/contact">Akademy Team</a>.
<!--break-->