---
title: "Akademy 2018 Tuesday BoF Wrapup"
date:    2018-08-15
authors:
  - "sealne"
slug:    akademy-2018-tuesday-bof-wrapup
---
<a href="https://community.kde.org/Akademy/2018/Tuesday">Tuesday</a> continued the Akademy BoFs, group sessions and hacking. There is a wrapup session at the end of the day so that what happened in the different rooms can be shared with everyone including those not present.

Watch Tuesday's wrapup session in the video below

<video width="750" controls="1">
  <source src="https://files.kde.org/akademy/2018/bof_wrapups/tuesday.mp4" type="video/mp4">
Your browser does not support the video tag.
<a href="https://files.kde.org/akademy/2018/bof_wrapups/tuesday.mp4">Tuesday BoF Wrapup video</a>
</video> 


<!--break-->