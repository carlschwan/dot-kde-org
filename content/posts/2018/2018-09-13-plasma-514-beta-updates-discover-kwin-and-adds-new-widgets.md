---
title: "Plasma 5.14 Beta Updates Discover, KWin and Adds New Widgets"
date:    2018-09-13
authors:
  - "jriddell"
slug:    plasma-514-beta-updates-discover-kwin-and-adds-new-widgets
comments:
  - subject: "Automatic update?"
    date: 2018-09-14
    body: "<p>Hi, can we set Discover/Muon to automatically update, in background?</p>"
    author: "justpassingby"
  - subject: "Typo"
    date: 2018-09-14
    body: "<p>Hi,</p><p><br />Great news. Thanks work community.</p><p><br />Just note typo of article, <span id=\"result_box\" lang=\"en\"><span>the first image is not centered on and erase left blocks (maybe my screen 13.3).<br /></span></span></p><p><span id=\"result_box\" lang=\"en\"><span><br />Best regards<br /></span></span></p>"
    author: "shann"
  - subject: "New features"
    date: 2018-09-18
    body: "<p>Hi there,</p><p>Glad to see so many new features in Plasma. The Breeze widget and improved compatibility with Libre Office deserve particular attention while switching between monitors also looks interesting. With that, I didn't quite understand the VPN feature. Can it fully replace VPN services (for example reviewed <a href=\"http://vpnservicepro.com/\" target=\"_blank\">here</a>) and offer the same functionality?&nbsp;</p>"
    author: "Andrew"
---
<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px; 
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}

figure {
    position: relative;
    z-index: 2;
    font-size: smaller;
    text-shadow: 2px 2px 5px light-grey;
}

</style>

<!-- <main class="releaseAnnouncment container"> -->
	<figure class="topImage">
		<a href="/sites/dot.kde.org/files/plasma-5.14.png">
			<img src="/sites/dot.kde.org/files/plasma-5.14.png" width="600" alt="Plasma 5.14 Beta">
		</a>
		<figcaption>KDE Plasma 5.14 Beta</figcaption>
	</figure>

	<p>
		Thursday, 13 September 2018.		Today KDE launches the beta release of Plasma 5.14.	</p>

	<p>
		Plasma is KDE's lightweight and full featured Linux desktop. For the last three months we have been adding features and fixing bugs and now invite you to test the beta pre-release of Plasma 5.14.	</p>

	<p>
		A lot of work has gone into improving Discover, Plasma's software manager, and, among other things, we have added a Firmware Update feature and many subtle user interface improvements to give it a smoother feel. We have also rewritten many effects in our window manager KWin and improved it for slicker animations in your work day. Other improvements we have made include a new Display Configuration widget which is useful when giving presentations.	</p>

        <p>
		Please test and send us bug reports and feedback. The final release is scheduled for three weeks' time.	</p>
	
<br clear="all" />

<p>Browse the full Plasma 5.14 Beta changelog to find out about more tweaks and bug fixes featured in this release: <a href="http://www.kde.org/announcements/plasma-5.13.5-5.13.90-changelog.php">Full Plasma 5.14 Beta changelog</a></p>

	<br clear="all" />

<h2>New in Plasma 5.14 Beta</h2>

<br clear="all" />
<h3>New Features</h3>
<ul>
<figure style="float: right; text-align: right">
<a href="http://www.kde.org/announcements/plasma-5.14/kscreen-presentation-plasmoid.png">
<img src="http://www.kde.org/announcements/plasma-5.14/kscreen-presentation-plasmoid-wee.png" style="border: 0px" width="350" height="264" alt="Display Configuration Widget" />
</a>
<figcaption style="text-align: right">Display Configuration Widget</figcaption>
</figure>
<li>There's a new Display Configuration widget for screen management which is useful for presentations.</li>
<li>The Audio Volume widget now has a built in speaker test feature moved from Phonon settings.</li>
<li>The Network widget now works for SSH VPN tunnels again.</li>
<li>Switching primary monitor when plugging in or unplugging monitors is now smoother.</li>
<li>The lock screen now handles user-switching for better usability and security.</li>
<li>You can now import existing encrypted files from a Plasma Vault.</li>
<li>The Task Manager implements better compatibility with LibreOffice.</li>
<br clear="all"/>
<figure style="float: right; text-align: right">
<a href="http://www.kde.org/announcements/plasma-5.14/system-monitor-tools.png">
<img src="http://www.kde.org/announcements/plasma-5.14/system-monitor-tools-wee.png" style="border: 0px" width="350" height="209" alt="System Monitor Tools" />
</a>
<figcaption style="text-align: right">System Monitor Tools</figcaption>
</figure>
<li>The System Monitor now has a 'Tools' menu full of launchers to handy utilities.</li>
<li>The Kickoff application menu now switches tabs instantly on hover.</li>
<br clear="all"/>
<figure style="float: right; text-align: right">
<a href="http://www.kde.org/announcements/plasma-5.14/panel-widget-options-old.png">
<img src="http://www.kde.org/announcements/plasma-5.14/panel-widget-options-old-wee.png" style="border: 0px" width="350" height="139" alt="Old Panel Widget Edit Menu" />
&nbsp;&nbsp;&nbsp;
<a href="http://www.kde.org/announcements/plasma-5.14/panel-widget-options-new.png">
<img src="http://www.kde.org/announcements/plasma-5.14/panel-widget-options-new-wee.png" style="border: 0px" width="350" height="139" alt="New Slicker Panel Widget Edit Menu" />
</a>
<figcaption style="text-align: right">Panel Widget Edit Menu Old and New Style</figcaption>
</figure>
<li>Widget and panels get consistent icons and other user interface improvements.</li>
<br clear="all"/>
<figure style="float: right; text-align: right">
<a href="http://www.kde.org/announcements/plasma-5.14/logout-warn.png">
<img src="http://www.kde.org/announcements/plasma-5.14/logout-warn-wee.png" style="border: 0px" width="350" height="224" alt="Logout Warning" />
</a>
<figcaption style="text-align: right">Logout Warning</figcaption>
</figure>
<li>Plasma now warns on logout when other users are logged in.</li>
<li>The Breeze widget theme has improved shadows.</li>
<!-- <li><a href='https://blog.broulik.de/2018/03/gtk-global-menu/'>The Global menu now supports GTK applications.</a> This was a 'tech preview' in 5.13, but it now works out of the box in 5.14.</li> -->
</ul>

<br clear="all" />
<figure style="float: right; text-align: right">
<a href="http://www.kde.org/announcements/plasma-5.14/discover.png">
<img src="http://www.kde.org/announcements/plasma-5.14/discover-wee.png" style="border: 0px" width="350" height="224" alt="Plasma Discover" />
</a>
<figcaption style="text-align: right">Plasma Discover</figcaption>
</figure>
<h3 id="discover">Plasma Discover</h3>
<p>Discover, our software and add-on installer, has more features and improves its look and feel.</p>
<ul>
<li>Discover gained <i>fwupd</i> support, allowing it to upgrade your computer's firmware.</li>
<li>It gained support for Snap channels.</li>
<li>Discover can now display and sort apps by release date.</li>
<li>You can now see an app's package dependencies.</li>
<li>When Discover is asked to install a standalone Flatpak file but the Flatpak backend is not installed, it now offers to first install the backend for you.</li>
<li>Discover now tells you when a package update will replace some packages with other ones.</li>
<li>We have added numerous minor user interface improvements: update button are disabled while checking for updates, there is visual consistency between settings and the update pages, updates are sorted by completion percentage, we have improved the review section of the update notifier plasmoid, etc..</li>
<li>We have improved reliability and stability through a bunch of bug fixes.</li>
</ul>

<br clear="all"/>
<figure style="float: right; text-align: right">
<video controls="true" width="350" height="200">
<source src="http://www.kde.org/announcements/plasma-5.14/kwin-glide-effect.mp4" type="video/mp4" />
Your browser does not support the video tag.
</video>
<figcaption style="text-align: right">Improved KWin Glide Effect</figcaption>
</figure>
<h3 id="bugfixes">KWin and Wayland:</h3>
<ul>
<li>We fixed copy-paste between GTK and non-GTK apps on Wayland.</li>
<li>We fixed non-centered task switchers on Wayland.</li>
<li>We have improved pointer constraints.</li>
<li>There are two new interfaces, XdgShell and XdgOutput, for integrating more apps with the desktop.</li>
<li>We have considerably improved and polished KWin effects throughout, including completely rewriting the Dim Inactive effect, adding a new scale effect, rewriting the Glide effect, and more.</li>
</ul>

<br clear="all" />
<h3 id="bugfixes">Bugfixes</h3>
<p>We fixed many bugs, including:</p>
<ul>
<li>Blurred backgrounds behind desktop context menus are no longer visually corrupted.</li>
<li>It's no longer possible to accidentally drag-and-drop task manager buttons into app windows.</li>
</ul>
<!--break-->
