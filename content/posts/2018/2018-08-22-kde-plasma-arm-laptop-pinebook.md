---
title: "KDE Plasma on ARM Laptop Pinebook"
date:    2018-08-22
authors:
  - "jriddell"
slug:    kde-plasma-arm-laptop-pinebook
comments:
  - subject: "Excellent!"
    date: 2018-08-22
    body: "<blockquote><p>&gt; The process has also yielded significant performance improvements in KDE Frameworks and Plasma; a</p><p>&gt; result every user has enjoyed with newer Plasma releases.</p></blockquote><p>Also excellent!</p>"
    author: "Sys"
  - subject: "Hardware availability"
    date: 2018-08-24
    body: "<p>Doesn't seem to actually be orderable from the linked website, and even the odd step of submitting an email to be in the queue only lists the 11.6\" option?</p>"
    author: "keithzg"
  - subject: "Kernel 3.10"
    date: 2018-08-24
    body: "<p>\"Linux kernel 3.10.105\"</p><p>Is there something available, that doesn't use stone old kernels?</p><p>This is probably a reused Android kernel, because it's the only one that has (closed source) graphics acceleration.</p><p>Even Raspbian uses kernel 4.14.</p>"
    author: "Cyk"
  - subject: "Email them"
    date: 2018-08-24
    body: "<p>I had the same trouble. Submitted a request to be in the queue then didn't hear anything for months. Finally emailed sales@pine64.org and was able to order within a week.</p>"
    author: "sez11a"
  - subject: "Raspberry Pi?"
    date: 2020-01-16
    body: "<p>Would the ISOs provided work on Raspberry Pi?</p>"
    author: "KDEUSER"
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey; text-align: center"><a href="/sites/dot.kde.org/files/pinebook-konqi.jpg"><img src="/sites/dot.kde.org/files/pinebook-konqi-wee.jpg" alt="Plasma on Pinebook photo" width="200" /></a>
Plasma running on a Pinebook.
</figure>

In the last few years, smartphone hardware has become powerful enough to drive conventional desktop software. A developing trend is to create laptops using hardware initially designed for smartphones and embedded systems. There are distinct advantages to this approach: those devices are usually very energy efficient, so they can yield a long runtime on a single battery charge; they're also rather inexpensive and lighter than conventional laptops.

One such device is the <a href="https://www.pine64.org/?page_id=3707">Pinebook</a>, created by a hardware manufacturer from China. The Pinebook is a low-cost laptop (at about 100 USD) with the full functionality one would expect. It is powered by a quad-core 64-bit ARM CPU clocked at 1.2 GHz, and comes with 2 GB of RAM, 16 GB of eMMC storage, and a 14" TN LCD at 1366x768.

Blue Systems has worked together with the manufacturer of the Pinebook to create a showcase test image that runs well on these devices. The team has adapted <a href="https://neon.kde.org">KDE neon</a> and created a bootable and installable remixed live image that works on the Pinebook. Developers have also fixed many bugs - both minor and major - across the whole software stack, kernel, graphics drivers, Qt, packaging, and in KDE Frameworks and Plasma.

The result shows that Plasma is an excellent candidate for devices like this. The process has also yielded significant performance improvements in KDE Frameworks and Plasma; a result every user has enjoyed with newer Plasma releases.

<b><a href="https://forum.pine64.org/showthread.php?tid=6443">To find out more, get instructions, default passwords, tips and tricks, and so on, check out the forum post here</a></b>. That post also contains links to the download.

<a href="https://files.kde.org/neon/images/pinebook-remix-nonfree/useredition/">You can also download the KDE neon Pinebook Remix images directly from here</a>, but remember to <a href="https://forum.pine64.org/showthread.php?tid=6443">check the forum for instructions</a>.

<img src="/sites/dot.kde.org/files/photo_2018-05-11_15-14-39_narrow.jpg" alt="Plasma on Pinebook photo">
<!--break-->