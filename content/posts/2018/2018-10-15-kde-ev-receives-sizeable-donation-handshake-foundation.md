---
title: "KDE e.V. receives a sizeable donation from Handshake Foundation"
date:    2018-10-15
authors:
  - "unknow"
slug:    kde-ev-receives-sizeable-donation-handshake-foundation
comments:
  - subject: "Link error?"
    date: 2018-10-15
    body: "<p>Hi,</p><p>Isn't there an error with the Handshake foundation link ? Is it really the handshake project (https://handshake.org) and not http://www.handshakefoundation.org ?</p><p>&nbsp;</p>"
    author: "Ga\u00ebl"
  - subject: "Calligra"
    date: 2018-10-15
    body: "<p>I'm very pleased to see Calligra receiving this donation!</p><p>I really hope it give it the boost it needs to became one of the best office suites out there (because it already has the best tecnical base by far, IMHO ;)</p>"
    author: "John"
  - subject: "Yes, that is correct. Check"
    date: 2018-10-15
    body: "Yes, that is correct. Check the handshake.org page, and you'll find Calligra and KDE mentioned."
    author: "Boudewijn Rempt"
  - subject: "Clarification "
    date: 2018-10-15
    body: "<p><span style=\"font-family: helvetica; font-size: small;\">I would really be grateful&nbsp;if you could elaborate or direct me to a long deep read on Calligra's \"<span style=\"background-color: rgba(255, 255, 255, 0.5); color: #2e3436;\">technical base\" and why it is better than the competition&nbsp;(LibreOffice)</span></span></p>"
    author: "Omar"
  - subject: "Krita"
    date: 2018-10-15
    body: "<p>Will Krita benefit from this or is Krita no longer part of Calligra?</p>"
    author: "tobias"
  - subject: "GREAT!"
    date: 2018-10-16
    body: "<p>A great thing for a GREAT future just happened.</p>"
    author: "Mircea"
  - subject: "OK, sorry. I checked but"
    date: 2018-10-16
    body: "<p>OK, sorry. I checked but probably too quickly.</p>"
    author: "Anonymous"
  - subject: "It's no longer part of Calligra"
    date: 2018-10-18
    body: "<p>It splintered off from the Calligra suite a ways back. But Krita seems to be pretty well funded these days, as attested by the constantly growing feature list coinciding with major improvements in existing features and especially overall speed.</p>"
    author: "Kawika"
  - subject: "good news for the project "
    date: 2018-10-19
    body: "<p>good news for the project</p>"
    author: "vmlinuz18"
  - subject: "Krita was part of Calligra"
    date: 2018-10-21
    body: "<p>Krita was part of Calligra until version 2.9, later on it is a separate project. It's being managed by Krita Foundation, but all Krita developers are KDE Community members, so I think they will get part of this benefit.</p>"
    author: "Adomas"
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/hi-2292499_1280_trimmed.jpg" alt="" width="1014" />

<b>We are excited to announce that the KDE e.V. received a donation of 300,000 USD from <a href="https://handshake.org/">the Handshake Foundation</a>.</b> Quite appropriate for a birthday present, as the KDE project just turned 22 this last weekend! 

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/Caliigra.png"><img src="/sites/dot.kde.org/files/Caliigra.png" width="250"/></a><br /><figcaption>The Calligra office suite project will<br />be receiving 100,000 USD.</figcaption></figure>

Of the total donation amount, 100,000 USD will be specifically allocated to pursue the development of the <a href="https://www.calligra.org/">Calligra office suite</a>.

"Handshake is pleased to be able to support KDE's international community of dedicated volunteers and their continued commitment to a free desktop environment with the current release of KDE Plasma 5 and the Calligra office suite", says Rob Myers from the Handshake Foundation.

The fruits of this contribution will soon become visible and available to everyone. Meanwhile, don't hesitate to <a href="https://community.kde.org/Get_Involved">join the KDE Community</a> and be part of our mission to help everyone protect their privacy and control their digital lives with Free Software.

Stay tuned for more updates, and <a href="https://twitter.com/kdecommunity/status/1050706945873719296">tell us</a> how you celebrated KDE's 22nd birthday!
<!--break-->