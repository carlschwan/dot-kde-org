---
title: "2018 KDE Connect Development Sprint"
date:    2018-05-28
authors:
  - "unknow"
slug:    2018-kde-connect-development-sprint
comments:
  - subject: "Great Job!"
    date: 2018-07-07
    body: "<p>Dear KDE connect team,</p><p>&nbsp;</p><p>thank you <strong>very</strong> much for the awesome tool you created and maintain! It is really highly appreciated!</p><p>As a relatively new Linux user I just discovered KDE connect today and got it to work really easily and quickly - fantastic work there! One thing that I am dearly missing though (and I am sure other KDE connect user would agree) is the feature of having SMS history or like a \"conversation thread\".</p><p>That would be an incredible improvment of the software and I hope there is a chance you can implement this in the not too far future. Unfortunately I am not a programmer myself (I'm barely managing using the Linux commands to install stuff etc...), so I cannot change the code myself.</p><p>&nbsp;</p><p>Thanks a lot for considering this feature!</p>"
    author: "GJW"
---
<img style="" src="/sites/dot.kde.org/files/photo5785146289028181435_cropped.jpg">

<b>Between the 23rd and 25th of March, <a href="https://community.kde.org/KDEConnect">KDE Connect</a> developers gathered in Verse's offices in Barcelona to work together for a weekend. It was the first meeting KDE Connect had in a while, and it was very productive!</b>

It's been some time since the sprint, and the work carried out there has already started to trickle down into our devices. Nevertheless, we wanted to shed some light on our accomplishments, and encourage everyone to participate.

Holding discussions and making decisions is much easier in person. We kicked off the sprint by going through our backlog of ideas to decide what was worth implementing. That helped us set the focus for the sprint and resume some blocked tasks.

<iframe style="display:block; margin: 1em auto 1em;" width="560" height="315" src="https://www.youtube.com/embed/sknP8BjmCc8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

One of the most requested features for KDE Connect is the ability to send SMS from the desktop. We already supported SMS to a certain degree with the ability to reply to a message. Some people have even set up Kontact to be able to send texts using KDE Connect from there, but it can be annoying to use without conversation history. During the sprint, Simon and Aleix started working on a fully-featured interface for sending SMS easily from the desktop that includes full conversation views and a full contact list.

Aleix and Nico polished the <I>Run Commands</I> interface to make it more discoverable, so that we can easily configure KDE Connect to do anything we want.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey"><a href="/sites/dot.kde.org/files/kdeconnectmediaplayer.png"><img src="/sites/dot.kde.org/files/kdeconnectmediaplayer.png" width="200" /></a><br /><figcaption>You can now see album art<br /> in your phone's lock screen.</figcaption></figure>

Matthijs improved the functionality of multimedia controls - now it's possible to display the album art from your desktop on your Android devices (both on the lock screen and in the new multimedia notification). Meanwhile, Aleix and Nico started paving the way towards better integration with PulseAudio control, sharing some code between KDE Connect and the Plasma volume control.

A less visible but crucial part of what makes KDE Connect so useful is its integration with the system. Albert Vaca worked on a KDE Connect plugin for Nautilus, so people who don't use Plasma and Dolphin can also have a great user experience.

Another very important but often-overlooked task is documentation. Matthijs invested some time in improving the onboarding process for new contributors. Hopefully we'll get more people involved in the future!

Last but not least, we fixed some ugly bugs during this sprint. Albert Astals fixed a long-standing crash in KIO, the KDE Framework used by KDE Connect for transferring files. Simon and Albert Vaca took care of some compatibility problems with Android Oreo, while Matthijs fixed a connectivity issue and even made some progress on Bluetooth support.

All in all, the sprint was a pleasant event, and I'm really happy we all got together. It was nice to meet the developers working on KDE Connect, to connect faces with nicknames, and generally agree on a common path we will follow in future development. 

Big thanks to KDE e.V. for sponsoring the travel - without their help, this sprint wouldn't have been possible.

Don't forget: you too can help KDE Connect by <a href="https://www.kde.org/donate ">donating to KDE</a>!
<hr />
<i>Story written by <b>Albert Vaca</b>, creator of KDE Connect.</i>
<!--break-->