---
title: "Reasons to Get Excited about KDE in 2018"
date:    2018-01-16
authors:
  - "skadinna"
slug:    reasons-get-excited-about-kde-2018
comments:
  - subject: "amazing"
    date: 2018-01-16
    body: "<p>It's amazing I'm very excited about this.</p>"
    author: "Morris"
  - subject: "Spreading for spanish speakers"
    date: 2018-01-18
    body: "<p>Hello!</p><p>spreading the word for spanish speakers community in my blog:</p><p><a href=\"https://victorhckinthefreeworld.com/2018/01/18/lo-que-nos-traera-kde-en-este-2018/\" target=\"_self\">https://victorhckinthefreeworld.com/2018/01/18/lo-que-nos-traera-kde-en-este-2018/</a></p><p>Keep on hacking! ;)</p>"
    author: "victorhck"
  - subject: "Can't wait"
    date: 2018-01-19
    body: "<p>Can't wait. I think 2018 will be an amazing year for KDE.</p>"
    author: "Martin Sotirov"
  - subject: "A Great 2018"
    date: 2018-01-24
    body: "<p>These are very good news from KDE, i really can't wait to see the improvements of Plasma in this year, the new KDE Apps and all the new additions and software added. This is gonna be a great year for KDE Community, i don't have any doubt about it.</p><p>Receive a huge hug KDE Community and very thanks for your great and amazing work.</p>"
    author: "Alberto D\u00edaz L\u00f3pez"
  - subject: "RE:"
    date: 2018-02-06
    body: "<p><span style=\"font-size: medium;\">Thanks for the amazing entry! It was excellent... I am sure that you will be satisfied with the&nbsp;<a href=\"http://essay-editor.net/blog/category/how-to/page/3\">essay correctors service</a>, as soon as you receive at least one written paper!&nbsp;</span></p>"
    author: "kirk"
  - subject: "I'm really exited for the new"
    date: 2018-03-03
    body: "<p>I'm really exited for the new music player :)</p>"
    author: "Cyaniden"
  - subject: "zanshin vs korganizer "
    date: 2018-08-17
    body: "<p>I can't really find what zanshin differs from korganizer. Any Zanshin user here who can tell more?</p><p>Also it would be interesting if tasks can be synced to nextcloud/owncloud like with korganizer, if it supports subtasks /subsubtask etc. and if there are options like reminder, due and start date. The homepage doesn't really give much infos.</p>"
    author: "temper"
---
Despite the security meltdown that swept over the tech community, our 2018 started out great - and it's all thanks to you. Your donations helped us reach the goal of our end-of-year fundraiser.

<figure style="position: relative; left: 50%; margin-left:-320px;padding: 1ex; width: 640px;"><a href="/sites/dot.kde.org/files/kde-2018-plans-cover.jpg"><img src="/sites/dot.kde.org/files/kde-2018-plans-cover.jpg" /></a></figure>

We would like to thank everyone who participated in the fundraiser, and also to all our community members who spread the word about it on social media and their blogs. You are all a wonderful community, and we are proud to be a part of this journey with you.

With our funds recharged and our hearts full of gratitude, we are ready to take on the new year and all the challenges it brings. The work is already in full swing - we released a <a href="https://www.kde.org/announcements/announce-applications-17.12.1.php">bugfix update for KDE Applications</a> and <a href="https://www.kde.org/announcements/kde-frameworks-5.42.0.php">a new version of KDE Frameworks</a>. But what else is there to look forward to in 2018? Read on to find out.

<h1>Plasma 5.12 Puts the "S" in Speed (and Stability)</h1>

Having released <a href="https://www.kde.org/announcements/plasma-5.11.5.php">an update for Plasma 5.11</a> just at the beginning of 2018, Plasma developers are now gearing up for the first major release of the year. <a href="https://www.kde.org/announcements/plasma-5.11.95.php">Plasma 5.12</a> will be the new LTS (Long-Term Support) version, replacing Plasma 5.8. 

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kde-2018-plans-discover.png"><img src="/sites/dot.kde.org/files/kde-2018-plans-discover.png" alt="" width="300" /></a><br />Discover, now with better application screenshots.<br /></figure></p>

Since many Linux distributions will rely on this version of Plasma, the developers wanted to make it as stable and fast as possible. Startup speed and memory usage will be visibly improved, particularly on low-end devices. 

The KScreen utility will allow Wayland users to adjust the output resolution as well as enable and disable selected outputs. Discover, the software management application, will support the dist-upgrade command for new major releases of distributions. Application screenshots will look better than ever, and support some useful options such as navigation between images. A lot of work has been done on <a href="https://pointieststick.wordpress.com/2018/01/13/flatpak-support-in-discover/">Flatpak support in Discover</a>, and plenty of critical, <a href="https://pointieststick.wordpress.com/2018/01/08/polishing-discover-software-center/">usability-impeding bugs have been fixed</a>.

It is important to note that new features for KWin on X11 will no longer be developed after Plasma 5.12. Moving forward, only the features relevant to Wayland will be added.

<h1>Krita Paints Masterpiece Features</h1>

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kde-2018-plans-krita.png"><img src="/sites/dot.kde.org/files/kde-2018-plans-krita.png" alt="" width="300" /></a><br />Krita 4.0 is almost here!<br /></figure></p>

Digital artists, rejoice! A major <a href="https://krita.org">Krita</a> release with big, beautiful changes is coming this year. With <a href="https://krita.org/en/item/krita-3-3-3/">Krita 3.3.3</a> released just recently, all the attention is now shifting towards Krita 4.0. This version will bring improved integration with Inkscape, allowing the users to copy and paste shapes directly between the two applications. 

Krita 4.0 supports Python scripting, comes with a new text tool, and allows bigger brush sizes. Expect two major changes related to file formats, too - instead of ODG, SVG will be the new default vector format. Additionally, the file format for color palettes will change, and the new one will let users create their own color groups. 

If you're feeling brave enough, you can already <a href="https://krita.org/en/item/krita-4-0-beta-1/">try Krita 4.0 Beta today</a>, and don't forget to <a href="https://krita.org/en/get-involved/report-a-bug/">report bugs</a> if you find any!

<h1>New Apps Take the Stage</h1>

We're always happy when new or small projects take off and grow to become an important part of the KDE Community. Here's a small selection of some interesting KDE applications to keep an eye on:

<h2>Atelier</h2>

If you're into 3D printing, you should <a href="https://atelier.kde.org/">try Atelier</a>. Along with its backend, AtCore, Atelier allows you to control your 3D printer, calibrate printer settings, check its status, and send print jobs to the printer. Although the application is still in beta, you can use it on Linux, macOS, and Windows, and there is even an AppImage if you'd prefer not bother with dependencies. 

A major redesign of the user interface is in progress, so you can expect better views and multiple workspaces that will allow you to manage more than one 3D printer using one instance of Atelier. 

<h2>Zanshin</h2>

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kde-2018-plans-zanshin.png"><img src="/sites/dot.kde.org/files/kde-2018-plans-zanshin.png" alt="" width="300" /></a><br />With Zanshin, you'll never forget to buy those kiwis.<br /></figure></p>

Productivity isn't such a hot buzzword anymore, but people are still looking for better ways to organize their tasks. Enter <a href="https://zanshin.kde.org/">Zanshin</a>, a small but powerful app grounded in the philosophy of simplicity. It allows you to sort your tasks into projects and divide them into different contexts. 

Zanshin integrates with the KDE PIM suite and KRunner, making it easy to add new tasks from incoming email or display them in your calendar. A new version of Zanshin <a href="https://zanshin.kde.org/2018/01/03/zanshin-0.5.0-is-out-2018-will-be-organized/">just came out</a>, with features such as recurrent tasks, support for attachments, and the ability to focus on the currently active task to minimize distraction.

<h2>Latte Dock</h2>

<a href="https://github.com/psifidotos/Latte-Dock">Latte Dock</a> officially became a KDE project (as part of our Extragear collection) at the end of 2017, but it has been a community favorite for a long time. 

This highly configurable dock allows you to organize your launchers and running applications, and new features are added constantly. Recent changes made it possible to share custom dock layouts with other users (and download theirs), and improved dynamic backgrounds for application windows that interact with the dock. 

<h2>Elisa</h2>

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kde-2018-plans-elisa.png"><img src="/sites/dot.kde.org/files/kde-2018-plans-elisa.png" alt="" width="300" /></a><br />Elisa is simple by default, powerful when needed.<br /></figure></p>

A new music player on the KDE stage, <a href="https://mgallienkde.wordpress.com/">Elisa</a> is still in development, but it's an exciting project to follow. One of its main features is music indexing, which optimizes the speed of your music collection. Elisa allows you to browse music by artist and album, or display all tracks, as well as create custom playlists. The developers are currently focused on improving the interface. You can try Elisa on Linux and Windows.

<h1>Akademy on the Blue Danube</h1>

August may seem far away, but we're already preparing for the biggest KDE Community event of the year - <a href="https://akademy.kde.org/2018">Akademy 2018</a>! The annual gathering of KDE community members will take place in Vienna, Austria, from the 11th to the 17th of August at the University of Technology (TU Wien).

The <a href="https://akademy.kde.org/2018/cfp">call for participation is now open</a>, and you can send your proposals for talks, panels, and workshops until March 12th, 2018. Of course, you can also simply come as an attendee -- after all, there is no admission fee, and everyone is welcome. Whether you're a seasoned KDE contributor or someone who just started using KDE software two days ago,  we would like to meet you!

<h1>More Ways for You to Contribute</h1>

During our end-of-year fundraiser, many people asked us about using cryptocurrency to support KDE. We listened, and we made it possible - now you can <a href="https://bitpay.com/990273/donate">donate Bitcoin using bitpay</a>. You can also donate directly from <a href="https://www.facebook.com/kde">our Facebook page</a>, or participate in our <a href="https://jointhegame.kde.org/">Join the Game project</a>, where you can become a supporting member of KDE and take part in our General Assembly meetings.

Of course, it's not all about the money. If you would like to contribute to KDE as a developer, take a look at our <a href="https://season.kde.org/?q=program_home&prg=46">Season of KDE</a> mentorship project. Want to write articles about KDE for this website? Get in touch with the <a href="https://community.kde.org/Promo">KDE Promo team</a>, and they will help you get started. There are so many venues to becoming a KDE contributor, and as part of <a href="https://dot.kde.org/2017/11/30/kdes-goals-2018-and-beyond">our long-term goals</a>, we will work on making the process of joining easier. 

Let's konquer 2018 together!
<!--break-->