---
title: "Plasma 5.12 is out, and it is faster, stabler, and has more features than ever"
date:    2018-02-06
authors:
  - "Paul Brown"
slug:    plasma-512-out-and-it-faster-stabler-and-has-more-features-ever
comments:
  - subject: "Great work, I can't wait to"
    date: 2018-02-07
    body: "<p>Great work, I can't wait to see it on my Neon machine.</p>"
    author: "Anonymous"
  - subject: "Thank you !"
    date: 2018-02-07
    body: "<p>KDE is my desktop of choice for 13 years ! I just want to thank everybody who was involved in this release :)</p>"
    author: "Heller"
  - subject: "\"you can now control music"
    date: 2018-02-10
    body: "<p>\"you can now control music playback from the lock screen,\"</p><p>&nbsp;</p><p>Sadly no way to stop strangers using this to see you whole video or audio library.</p><p>&nbsp;</p><p>Not very secure.</p><p>&nbsp;</p><p>https://bugs.kde.org/show_bug.cgi?id=389483</p>"
    author: "Tom (Manchester)"
  - subject: "Date an time of item creation"
    date: 2018-02-11
    body: "<p>Hi guys,</p><p>Is it possible to see, in this awesome release of Plasma, the date and time when a folder or file was created?</p><p>Thanks!</p>"
    author: "Leo"
  - subject: "Correction"
    date: 2018-03-13
    body: "<p>That would be 'more stable', instead of stabler</p>"
    author: "Ferry"
---
<strong>Although performance and stability have been the prime goals for this version of Plasma, some of the new features include spring-loaded folder views, improved Wayland support, interactive notifications, and more.</strong>

<figure style="position: relative; left: 50%; margin-left:-320px;padding: 1ex; width: 640px;"><a href="/sites/dot.kde.org/files/globamenus.png"><img src="/sites/dot.kde.org/files/globamenus.png" /></a></figure>

<a href="https://www.kde.org/announcements/plasma-5.12.0.php">Plasma 5.12 LTS</a> is the second long-term support release from the Plasma 5 team. Developers have been working hard, focusing on speed and stability for this release. Boot time to desktop has been improved by reviewing the code for anything which blocks execution. The team has fixed bugs in every aspect of the codebase, tidied up the artwork, removed corner cases, and ensured cross-desktop integration. For the first time, Plasma offers experimental Wayland integration on long-term support, so you can be sure it will continue to improve the Wayland experience. 

Also, you can now control music playback from the lock screen, interact with notifications, use "spring-load" folders to move files around without actually opening Dolphin, and change your applications to use global menus à la macOS.

<a href="https://www.kde.org/announcements/plasma-5.12.0.php">Read more about the release of Plasma 5.12 LTS here</a>.

<iframe width="560" height="315" src="https://www.youtube.com/embed/xha6DJ_v1E4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<!--break-->