---
title: "KEXI 3.1 Brings Database Application Building to Windows"
date:    2018-03-12
authors:
  - "skadinna"
slug:    kexi-31-brings-database-management-windows
---
<b>After many months of hard work and more than 200 bugs fixed, KEXI is back with a new major release that will excite Windows and Linux users alike.</b>

If you are looking for a Free and open source alternative to Microsoft Access, KEXI is the right tool for you. 

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kexi-announcement-interface.png"><img src="/sites/dot.kde.org/files/kexi-announcement-interface.png" alt="" width="300" /></a><br />KEXI offers an easy way <br /> to design all kinds of databases.<br /></figure></p>

As part of <a href="https://www.calligra.org/kexi/">the Calligra suite</a>, KEXI integrates with other office software, providing an easy, visual way to design tables, queries, and forms, build database applications, and export data into multiple formats. KEXI also offers rich data searching options, as well as support for parametrized queries, designing relational data, and storing object data (including images). 

A new version of KEXI has just been released, so if you have never tried this powerful database designer application, now is the right time. 

<a href="http://www.kexi-project.org">KEXI 3.1</a> is available for Linux and macOS, and after many years, for Windows as well.

<h2>KEXI Is Back on Windows</h2>

Business environments are often concerned about migrating to FOSS solutions because of compatibility issues with the proprietary software and formats they currently use. KEXI solves that problem with its Microsoft Access migration assistant that ensures database tables are preserved and editable between applications. Even better, KEXI works natively on the Windows operating system. In fact, KEXI was the first KDE application offered in full version on Windows. 

After a long hiatus, the new version of KEXI offers convenient installers for Windows once again. Although it's a preview version, the users are invited to try it out, report bugs, and provide feedback.  

<h2>Usability and Stability for Everyone</h2>

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kexi-announcement-kproperty.png"><img src="/sites/dot.kde.org/files/kexi-announcement-kproperty.png" alt="" width="300" /></a><br />KProperty is included in the first <br /> major release of KEXI Frameworks.<br /></figure></p>

Similar to Plasma 5.12 LTS, the focus of KEXI 3.1 was to improve stability and (backward) compatibility. With more than 200 bugfixes and visibly improved integration with other desktop environments, the goal has definitely been achieved. 

Usability improvements have also made their way into KEXI 3.1 dialogs. When using the Import Table Assistant, it is now possible to set character encoding for the source database. Property groups are now supported, and users can set custom sizes for report pages. 

<h2>Great News for Developers</h2>

KEXI 3.1 marks the first official release of KEXI Frameworks - a powerful backend aimed at developers who want to simplify their codebase while making their Qt and C++ applications more featureful. KDb is a database connectivity and creation framework for various database vendors. In KEXI 3.1, KDb offers new debugging functions for SQL statements and comes with improved database schema caching. 

KProperty is a property editing framework which now comes with improved support for measurement units and visual property grouping. Last but not least, KReport is a framework for building reports in various formats, offering similar functionality to the reports in MS Access, SAP Crystal or FileMaker. The most useful new feature in KEXI 3.1 is the ability to set custom page sizes for KReport.

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kexi-announcement-kreport.png"><img src="/sites/dot.kde.org/files/kexi-announcement-kreport.png" alt="" width="300" /></a><br />New options in KReport allow you to <br /> tweak the appearance of reports.<br /></figure></p>

Alongside Frameworks, KEXI 3.1 offers greatly refined APIs and updated API documentation. According to the developers, “the frameworks are now guaranteed to be backward-compatible between minor versions”. 

Translations have also been improved, and KEXI 3.1 is the first version where they are bundled with the Frameworks. This will make it easier for the developers using KEXI Frameworks, as they will be able to use translated messages in their apps. 

<h2>Make KEXI Even Better</h2>

Even with all the excitement about the new release, KEXI developers are already working on new features and improving the existing ones. If you'd like to help make KEXI better, it's never too late to join the project! Take a look at the list of available <a href="http://www.kexi-project.org/wiki/wikiview/index.php@Jobs.html">coding</a> and <a href="http://www.kexi-project.org/wiki/wikiview/index.php@NonDevJobs.html">non-coding jobs</a>. 

Although the API documentation has been updated, <a href="https://userbase.kde.org/Kexi/Handbook">the user documentation</a> could use some love. If you're good at writing or teaching others, why not chip in? 

Finally, if you know a business or an individual that's looking for a Microsoft Access replacement, tell them about KEXI. 
They just might be pleasantly surprised with what they'll discover. 

<hr>

<em><a href="https://community.kde.org/Kexi/Releases#3.1.0">Download the KEXI 3.1 source</a> or install it from the repository of your distribution. For the full list of changes in the new version, take a look at <a href="https://community.kde.org/Kexi/Releases/3.1.0_Changes">the official changelog</a>.</em> 

<!--break-->