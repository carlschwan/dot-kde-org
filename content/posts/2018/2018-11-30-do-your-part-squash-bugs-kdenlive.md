---
title: "Do your part! Squash bugs for Kdenlive!"
date:    2018-11-30
authors:
  - "Paul Brown"
slug:    do-your-part-squash-bugs-kdenlive
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/bugsquash2018.png" alt="" width="800" />

<b>On the 2nd of December, <a href="https://kdenlive.org/event/bug-squashing-day/">the Kdenlive team will be holding an open bug-squashing day</a> in preparation for the major refactoring release due in April 2019. Everybody is invited!</b>

This is a great opportunity for developers of all levels to participate in the project. The team has triaged hundreds of reports, closing more than a hundred of them in the past month. Kdenlive developers have also made a list of entry-level bugs you can get started with.

For the more seasoned developers, there are plenty of options - be it a shiny feature request or a challenge to polish some non-trivial edges. To hack Kdenlive, you need to know C++, Qt, QML or KDE Frameworks. Those with knowledge of C can join the fun by improving MLT, the multimedia framework Kdenlive runs on. 

Even if you have no programming experience, you can still help by testing fixes and features, as well as by triaging more bug reports.
 
<a href="https://phabricator.kde.org/tag/kdenlive/">Check out the list of proposed bugs to solve in our workboard</a> under the "<I>low hanging / junior jobs</I>" section.

<h2>Bug-squash Schedule</h2>

10h-10h30 CET: introduction of the event, presenting goals and tools, test the latest refactoring AppImage, how to help the project   

10h30-12h CET: Introduce yourself, get help in your development setup, choose your bug and work on it with  help from the team

12h -13h CET: Lunch break

13h - 17h CET: Bug fixing - continue morning tasks or introduce yourself if you just joined, get help in your development setup, choose your bug and get help if you need it

16h - 17h CET: Brainstorm: Timeline colors

17h - 18h CET: Closing thoughts, evaluation, general issues, future planning
 
<b>Spread the word and join us!</b> <a href="https://kdenlive.org/event/bug-squashing-day/">All the information on how to join is here</a>.
<!--break-->