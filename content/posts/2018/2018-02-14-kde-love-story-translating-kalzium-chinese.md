---
title: "A KDE Love Story: Translating Kalzium into Chinese"
date:    2018-02-14
authors:
  - "unknow"
slug:    kde-love-story-translating-kalzium-chinese
---
<strong><em> 
Today is <a href="https://fsfe.org/campaigns/ilovefs/">"I Love Free Software Day"</a>!
<strong><em> 
We're celebrating by shining a spotlight on our contributors and on our collaboration with other FOSS communities and organizations. Free Software is an integral part of our lives, and it's important to show appreciation, support, and gratitude to everyone who works on making it better every day. 
<strong><em> 
One of those people is Franklin Weng, a KDE contributor who started his Free Software journey by translating Kalzium. Franklin's contributions led him to amazing opportunities and projects. Read on to find out why he loves KDE so much.
</strong></em> 

<h3>Kalzium – The Start of An Amazing Journey</h3>

<strong>by Franklin Weng</strong>

When I was a high school student, chemistry was not my cup of tea. My grades in chemistry were not bad either, but I hated memorizing those organic compounds. Then, I decided to major in computer science at university, and from that moment, destiny tightly bonded me and Free and Open Source Software.

Around 2001 or 2002, I started to use and later contribute to KDE. However, <a href="https://www.kde.org/applications/education/kalzium/">Kalzium</a> is the start of another amazing story for me. It happened in 2007...

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kde-love-story-kalzium2.png"><img src="/sites/dot.kde.org/files/kde-love-story-kalzium.png" alt="" width="300" /></a><br />A throwback to the old look of Kalzium - in Chinese!<br /></figure></p>

I started translating KDE software around 2005. At the time, the Traditional Chinese language packages had almost been abandoned by KDE because of the very slow translation rate. Some senior FOSS community members called to others to "save" Traditional Chinese, and finally we did. From that moment on, I kept translating KDE because I simply asked myself: "Since I'm using this desktop environment, why not do it?"

So I translated everything in KDE. Everything. I started with KMail because I wanted a mail client that would reside in my system tray. Then I translated more KDE PIM applications, KDE Multimedia, KDE Graphics, KDE Games,... even KOffice. And of course, KDE Edu applications - from the simple, lovely ones, like <a href="https://www.kde.org/applications/games/ktuberling/">KTuberling</a>, <a href="https://www.kde.org/applications/education/ktouch/">KTouch</a>, and <a href="https://www.kde.org/applications/education/khangman/">KHangman</a>, to huge ones like <a href="https://www.kde.org/applications/education/kstars/">KStars</a> and <a href="https://www.kde.org/applications/education/kgeography/">KGeography</a> (that last one is enormous). Kalzium was just another one for me; I even translated <a href="https://www.kde.org/applications/office/kmymoney/">KMyMoney</a> - without any accounting background. 

Then in 2007, I got an email.

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kde-love-story-kalzium2.png"><img src="/sites/dot.kde.org/files/kde-love-story-kalzium2.png" alt="" width="300" /></a><br />Franklin's credits as the translator of the app.<br /></figure></p>

"I saw you translated the Kalzium software. Could we meet and talk about that?"

That email was from my now friend, Eric Sun, who was (and still is) the Executive Secretary of the Open Source Software Application Consulting Center (OSSACC), a project launched by the Ministry of Education in Taiwan. The project promotes free software for use in primary and high schools. At first I had no idea why this guy would like to meet me. Would he discuss chemistry with me? I wasn't a chemistry-aholic at all! 

We met at a Burger King in Taipei. He introduced himself and told me about his idea, which made me appreciate him a lot! He, as the Executive Secretary and also an FOSS enthusiast like me, wanted to introduce educational Free Software applications to teachers and students to help them acquire knowledge from many different fields, without any financial cost.

To promote those Free Software applications, the first step is, of course, to localize them. Teachers and students, especially in primary schools, would never accept software with English interfaces. He noticed that there had been some software with Traditional Chinese translations, and he was curious about who did it. Bingo! It was me.

<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/kde-love-story-kalzium-today.png"><img src="/sites/dot.kde.org/files/kde-love-story-kalzium-today.png" alt="" width="300" /></a><br />This is what Kalzium looks like these days.<br /></figure></p>

We started a series of plans. The main plan was a customized Linux distribution named ezgo - which I have already <a href="https://dot.kde.org/2013/10/02/ezgo-free-and-open-source-software-taiwans-schools">introduced here in October of 2013</a>. 

After that, we have also done a lot of work together, mostly introducing public domain educational resources, including FOSS to schools. I became one of KDE e.V. members and the president of the BoD of Software Liberty Association Taiwan, an NPO which promotes FOSS in Taiwan. In recent years, we have helped Taiwan governments to migrate to LibreOffice and ODF.

Thinking about the past 10 amazing years on the road of promoting FOSS, the start point was that I translated Kalzium. Of course, I wasn't aware that contributing translations to KDE would give me such an amazing tour in my life. Nevertheless, I always use this as an example to tell my young friends in Taiwan: "See? Chemistry is not my thing, but translating Kalzium (and many other KDE applications) made my life wonderful!"

<hr>
<strong><em>Big thanks to Franklin for contributing to KDE for so many years, and for spreading the word about our software and its educational potential!
<strong><em>
Do you have a story about how you fell in love with KDE? Let us know in the comments!
</strong></em>
<!--break-->