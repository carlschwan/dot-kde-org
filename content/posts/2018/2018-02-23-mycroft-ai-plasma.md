---
title: "Mycroft AI on Plasma"
date:    2018-02-23
authors:
  - "Paul Brown"
slug:    mycroft-ai-plasma
---
<figure style="position: relative; left: 50%; margin-left:-320px;padding: 1ex; width: 640px;"><a href="/sites/dot.kde.org/files/mycroft02.png"><img src="/sites/dot.kde.org/files/mycroft02.png" /></a></figure>

<strong>Did you know you can install an AI personal assistant on your Plasma desktop?</strong>

Mycroft is running through the last 24 hours of the <a href="https://www.kickstarter.com/projects/aiforeveryone/mycroft-mark-ii-the-open-voice-assistant">crowdfunding campaign for its Mark II assistant</a>. The machine looks awesome and offers similar functionality to other proprietary alternatives, but with none of the spying and leaking of personal data.

The Mark 2 will be delivered to backers at the end of this year, but you can enjoy the pleasures of giving orders to an AI right now by installing the <a href="https://phabricator.kde.org/source/plasma-mycroft/">Mycroft widget on Plasma</a> courtesy of KDE hacker Aditya Mehra.

As the widget is still experimental software, the installation is currently slightly cumbersome. The users have to build it from source and write to directories that should usually be accessible only to the administrator account. The widget also uses Python 2, which is a bit old at this stage.

However, a little dicky bird from openSUSE has told us that they are solving these issues, migrating the software to Python 3 and packaging RPMs. They said they will have packages for Tumbleweed *maybe* as early as next week.

Enjoy being an overlord to the AIs while you can.
<!--break-->