---
title: "The State of Akademy Sponsorship"
date:    2018-08-07
authors:
  - "Paul Brown"
slug:    state-akademy-sponsorship
---
<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><img src="/sites/dot.kde.org/files/action-2277292_1280_cropped.jpg" /><br /><figcaption style="font-size:x-small;"><a href="https://pixabay.com/en/action-collaborate-collaboration-2277292/">Photo</a> by <a href="https://pixabay.com/en/users/rawpixel-4283981/">rawpixel</a>. Licensed under CC0 license.</figcaption></figure>
</div>

<a href="https://akademy.kde.org/2018">Akademy 2018</a> is less than a week away. Apart from meeting up again with friends and colleagues, the KDE community has another reason to be joyful: <a href="https://akademy.kde.org/2018/sponsors">this year we have broken the record for the number of sponsors</a> for the event. Although there have been many sponsors of Akademy over the years, never have there been so many at one time.

Eike Hein, Treasurer of the KDE e.V. board, believes that the extra influx of sponsors is thanks to "KDE software being loved again." Eike points out that Plasma is reaching more kinds of devices every day, attracting larger communities and more hardware manufacturers -- some of which will be at Akademy this year. KDE applications are also becoming more mainstream and reaching larger audiences. Krita and Kdenlive, for example, are making inroads within the community of graphical artists, raising awareness of KDE in a whole new sector of end users. Kirigami is becoming the go-to framework for projects that need convergence on desktop and mobile devices.

"I would also attribute the increase in support to the fact that KDE actively engages with partners" says Eike. A case in point is the <a href="https://ev.kde.org/advisoryboard.php">Advisory Board</a>. The Advisory Board makes organization-to-organization interaction more rewarding and helps build a stronger network of like-minded Free Software associations and companies. Through the Advisory Board, KDE can better reach and support larger communities, which in turn reinforces KDE's position within Free Software.

<h2>About Akademy</h2>

For most of the year, KDE -- one of the largest free and open software communities in the world-- works on-line by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2018">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.

This year, Akademy will be held in Vienna, Austria, from the 11th to the 17th of August. You can join us by  <a href="https://events.kde.org/">registering on the event's website</a>.

For more information, please contact the <a href="https://akademy.kde.org/2018/contact">Akademy Team</a>.
<!--break-->