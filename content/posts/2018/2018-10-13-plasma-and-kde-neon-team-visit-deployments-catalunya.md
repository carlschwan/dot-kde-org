---
title: "Plasma and KDE neon Team Visit Deployments in Catalunya"
date:    2018-10-13
authors:
  - "jriddell"
slug:    plasma-and-kde-neon-team-visit-deployments-catalunya
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/escola.jpg" alt="" width="1014" />
<figcaption>KDE members in the impressive foyer at Escola del Treball</figcaption>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/linkat.jpg"><img src="/sites/dot.kde.org/files/linkat-wee.jpg" width="450" height="337" /></a><br /><figcaption>Meeting with Pablo of Catalan Generalitat distro Linkat</figcaption></figure>

Last week developers from the KDE neon and Plasma teams visited Barcelona. We were there to meet with some KDE software projects we had heard about in the Catalan government and schools. Aleix Pol lives locally and works on Plasma and Discover. He invited Plasma release manager and KDE neon developer Jonathan Riddell, KDE neon continuous integration master Harald Sitter, and hardware enablement guru Rohan Garg to meet the teams evaluating our software and supporting our users.

We first met Pablo who runs the <a href="http://linkat.xtec.cat">Linkat project</a> for the Catalan government. Linkat is a Linux distribution they offer to schools, and it currently runs lightweight, simple desktop environments. As Plasma 5 now tends to use as little or less memory and resources than many lightweight Linux desktops, the Linkat team is interested in trying it. We met with the officials from the education department and discussed accessibility needs, looking at <a href="https://mycroft.ai/">Mycroft for voice control</a> and <a href="https://community.kde.org/KDEConnect">integrating with phones using KDE Connect</a>.

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/photo_2018-10-13_10-35-30.jpg"><img src="/sites/dot.kde.org/files/photo_2018-10-13_10-35-30.jpg" width="250"/></a><br /><figcaption>The Escola del Treball is looking for<br />ways to keep their IT infrastructure<br />current, while at the same time<br />cutting costs</figcaption></figure>

The next day we visited the largest technical school in Catalunya, the Escola del Treball (<a href="http://www.escoladeltreball.org">school of the workers</a>). Within their impressive Gaudí-inspired building, they run a few thousand computers on which they are trying to reduce the costs. They showed us the setup they had developed using thin clients with a simple Atom computer or Raspberry Pi. The thin clients use a remote desktop protocol to talk to virtual machines on a central server. The technically-minded teachers can customize what's running on the virtual machine with a range of distributions and operating systems available. Their server has hosted over 3000 virtual machine images just on the trial computers, all for the individual use cases of the teaching staff. Unlike with proprietary setups, this means they do not have to ask for a budget to install software.

They discussed some problems their virtual machine software was having with Plasma and tested some fixes made by Aleix. Rohan was also interested in finding the best machines they could use for their thin clients.

In the evening, we met with developer Angel Docampo and talked about the deployment he worked on for the Ajuntament (city council) of Barcelona. The Ajuntament is also interested in moving towards Free Software on their computers. This deployment is based on Kubuntu, and it is currently in trial by about 30 employees. Angel reported that they are happy with the setup; however, taking it further will likely depend on the politicians‘ will to drive the change forward.

As we were about to leave, we learned about a project called openUAB at the Autonomous University of Barcelona. They are making a KDE neon-based system for their own uses. We expect to find out more about this project after Aleix meets with them in the upcoming weeks.

This was an exciting trip that opened our eyes to the increasing number and variety of users and use cases of KDE software. The insights we collected will help us deliver better software, and strengthen the bonds between our community and the rest of the world.
<!--break-->