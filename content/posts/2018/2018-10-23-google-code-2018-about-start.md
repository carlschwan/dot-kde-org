---
title: "Google Code-in 2018 is about to start!"
date:    2018-10-23
authors:
  - "unknow"
slug:    google-code-2018-about-start
comments:
  - subject: "Start your engines!"
    date: 2018-10-23
    body: "<p>Life is about to get *really* busy!</p>"
    author: "Anonymous"
---
<p></p>
After a break in 2017, the KDE community is <a href="https://opensource.googleblog.com/2018/09/introducing-google-code-in-2018-organizations.html">participating in the Google Code-in contest</a> as a mentoring organization. This means that pre-university students aged 13 to 17 from all over the world will be able to contribute to the Free Software movement by helping KDE develop software products that give users control, freedom, and privacy.

<a href="https://codein.withgoogle.com/">Google Code-in</a> is a global online contest with the goal of helping teenagers get involved in the world of open source development. Mentors from the participating organizations lend a helping hand as participants complete various bite-sized tasks in coding, graphics design, documentation, and more.

This year we have tasks from <a href="https://community.kde.org/KDEConnect">KDE Connect</a>, a project that enables all your devices to communicate with each other; <a href="https://gcompris.net/">GCompris</a>, an educational software suite; <a href="https://www.kde.org/applications/system/kdepartitionmanager/">KDE Partition Manager</a>, our disk partitioning utility; and the <a href="https://community.kde.org/Get_Involved/design">KDE Visual Design Group</a>, our interface usability experts.

At the end of the contest, each organization will select 6 finalists and 2 grand prize winners. Students can earn prizes (digital certificates, T-shirts, hoodies) and grand prize winners will receive a trip to Google’s headquarters in Mountain View, California for themselves and a parent or legal guardian for 4 nights in June 2019.

<figure><img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/google-codein-participants.jpg" alt="Sergey Popov, Kevin Funk, and Ilya Bizyaev in Mountain View, CA" width="1014" /><figcaption style="text-align: center;">Sergey Popov, Kevin Funk, and Ilya Bizyaev in Mountain View, CA</figcaption></figure>

In 2016, the two lucky grand prize winners from the KDE community were Sergey Popov and Ilya Bizyaev. They seized the amazing opportunity to travel all the way from Russia to Google HQ in the US, where they met many students and professional software developers passionate about Free Software, and made new friends! 

Both of them are still involved in KDE. As Ilya says, "contributing to KDE is an incredible opportunity to improve my programming, design and social skills while working on something that really matters".

<strong>This year, you could be the lucky one. To learn more about Google Code-in and register as a participant, visit <a href="https://g.co/gci">the official contest website</a>. We are waiting for your contributions! :)</strong>
<!--break-->