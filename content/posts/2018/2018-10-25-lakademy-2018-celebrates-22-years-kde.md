---
title: "LaKademy 2018 Celebrates 22 Years of KDE"
date:    2018-10-25
authors:
  - "unknow"
slug:    lakademy-2018-celebrates-22-years-kde
---
<strong>LaKademy, or Latin American Akademy, is the annual meeting of the Latin American KDE community - one of the biggest Free software communities in the world. The event takes place since 2012, and is open to all developers, artists, users, and everyone who wants to contribute in any way to the software created or maintained by the community.</strong>

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/lakademy-2018-4.jpg" alt="" width="1014" />

As is tradition since 2012, the Latin American Akademy happened from 11th to 14th of October 2018 at the Federal University of Santa Catarina (UFSC) in the city of Florianopolis, Brazil. Members of the local Free and open source software community - mostly Brazilians - gathered at the event. (On a side note, if you are reading this and you are from South America, please join us next year). 

It was a fantastic opportunity for everyone to work on KDE projects, but also on other unrelated projects that each person contributes to. The participants strengthened their friendship bonds and shared experiences about creating, using, and maintaining software. Finally, on Sunday (October 14th) everyone celebrated the 22 years of KDE with a cake. Konqui was there, too!

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/lakademy-2018.jpg" alt="" width="1014" />

But let us start from the beginning. LaKademy 2018 officially opened on October 11th. More than 20 participants, including Karina Mochetti and six students of Computer Science from the Federal University Fluminense (UFF, Niteroi) started resolving the issues in translation scripts used by the localization team. 

They also worked on the <a href="https://edu.kde.org/">KDE Edu software</a> - the educational suite for everyone from age 5 to 95. It was the first time that we had this kind of help - from a formal partnership between a university and its students - and also the first LaKademy with so many attendees from all corners of the continent.

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/lakademy-2018-2.jpg" alt="" width="1014" />

Artists, enthusiasts, teachers... everyone focused on working hard on the projects during the event. The 3D artists created 3D models, translators translated, and developers developed. Many bugs where squashed, too. We would especially like to thank Nicolas Alvarez for breathing in a new life into the official LaKademy website that will be live in its final form soon.

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/lakademy-2018-3.JPG" alt="" width="1014" />

On Saturday, October 13th, the traditional promo meeting took place, where the future of the Latin American KDE community was discussed. We covered a wide range of topics: from communication tools, our presence at Brazilian events, and the promotional materials to the proposal of migrating the KDE Brasil site to Wordpress. 

We also considered potential host cities for LaKademy 2019, and shared some thoughts on making it happen outside of Brazil as a way of reinforcing the "Latin-American-ness" of the event.

We would like to thank everyone who participated in this year's LaKademy, and helped make it such a welcoming and productive event. Let's do our best to make it even bigger and better next year!

<h2>List of LaKademy 2018 participants</h2>

<strong>UFF students who attended LaKademy with the mentoring of Karina Mochetti:</strong>
 
Carlos Henrique Domingos Correia Santos
Fernando Costa Rodrigues
Hugo Caetano Borges Carneiro
Luan Simões Cardoso
Lucas Henrique Tavares Monteiro
Maria Edoarda Vallim Fonseca

<strong>Other KDE Community members:</strong>

Ângela Cunha
Aracele Torres
Barbara Tostes
Bianca Oliveira
Caio Jordão Carvalho
Camila Moura
Dórian Langbeck
Eliakin Costa
Filipe Saraiva
Frederico Gonçalves Guimarães
Henrique Sant'Anna
Jamil Gleice
Nicolás Alvarez
Patrick Pereira
Pedro Arthur Duarte

<h2>Summary of LaKademy 2018 activities</h2>

<li>Fixed bugs on many different levels of different applications</li>
<li>Updated the <a href="https://timeline.kde.org/">KDE Timeline</a></li>
<li>Restructured the translation process</li>
<li>Created a new Phabricator group for the Brazilian translation team</li>
<li>Prepared documentation for newcomers to the translation team</li>
<li>Translated various applications and documentation</li>
<li>Reviewed and updated tutorials on how to use <a href="https://www.kde.org/applications/development/lokalize/">Lokalize</a>, the KDE CAT (Computer-Aided Translation) tool</li>
<li>Revised and updated the content on the LaKademy website</li>
<li>Overhauled <a href="https://atelier.kde.org/">Atelier Core</a> compatibility and added support for new technologies</li>
<li>Added cryptography to <a href="https://konsole.kde.org/">Konsole</a> History file</li>
<li>Discussed the implementation of new technologies in <a href="https://gcompris.net/">GCompris</a></li>
<li>Fixed many small papercuts in KDE-Edu software</li>
<li>Initiated the discussion about migrating <a href="https://br.kde.org/">the KDE Brasil website</a> to Wordpress</li>
<li>Had many productive discussions about art and promo activities</li>
<li>Took photos and videos of the event</li>

<!--break-->