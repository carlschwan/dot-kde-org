---
title: "Nextcloud Founder Frank Karlitschek awarded 20,000 euros -- Donates prize to promote inclusiveness"
date:    2018-10-30
authors:
  - "unknow"
slug:    nextcloud-founder-frank-kalitschek-awarded-20000-euros-donates-prize-promote
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:300px"><a href="/sites/dot.kde.org/files/4096-4096-max_1500.jpg"><img src="/sites/dot.kde.org/files/4096-4096-max_1500.jpg" /></a><br /><figcaption>Frank Karlitschek, founder of Nextcloud and recipient of this year's Reinhard von Koenig award.</figcaption></figure> 

<b>Frank Karlitschek, the founder of <a href="https://nextcloud.com">Nextcloud</a>, has won the <a href="http://reinhard-von-koenig-preis.de/">Reinhard von Koenig award</a> and will be donating the winnings, amounting to € 20,000, to start a fund called <a href="https://nextcloud.com/include/">"Nextcloud Include"</a>.</b>

The fund, set up in collaboration with KDE e.V., wants to encourage diversity in open source. It aims to help underrepresented groups participate in the global Nextcloud community and foster an inclusive and diverse space where the community can continue to collaborate and develop world-class software. Mentoring, travel support, and internships are provided as part of the program. The program is ran in collaboration with the KDE community under the umbrella of the KDE e.V.

Margit Stumpp, Member of the German Parliament (Bundestag) said:

<style>
blockquote{
  display:block;
  background: #fff;
  width: 30%;
  padding: 15px 20px 15px 70px;
  margin: 0 0 20px;
  position: relative;
  
  /*Font*/
  /*font-family: Georgia, serif;*/
  font-style: italic;
  font-size: 14px;
  line-height: 20px;
  color: #666;
  text-align: left;
  
  /*Borders - (Optional)*/
  border-left: 15px solid #0068c6;
  border-right: 2px solid #0068c6;
  
  /*Box Shadow - (Optional)*/
  -moz-box-shadow: 2px 2px 15px #ccc;
  -webkit-box-shadow: 2px 2px 15px #ccc;
  box-shadow: 2px 2px 15px #ccc;
}

blockquote::before{
  content: "\00BB"; /*Unicode for Left Double Quote*/
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 60px;
  font-weight: bold;
  color: #999;
  
  /*Positioning*/
  position: absolute;
  left: 10px;
  top:10px;
}

blockquote::after{
  /*Reset to make sure*/
  content: "";
}

blockquote em{
  font-style: italic;
}
</style>

<blockquote style="font-family: Arial, sans;font-style: italic; font-size: 14px; line-height: 20px; color: #666; text-align: left;">
Equal opportunities and diversity are very important issues for the future, especially in technical professions. I am pleased that Nextcloud Include is a new important initiative that is focusing on these issues".</blockquote>

Frank is of course very happy with the prize:

<blockquote style="font-family: Arial, sans;font-style: italic; font-size: 14px; line-height: 20px; color: #666; text-align: left; width: 85%;">
I'm extremely honored to be awarded this prize as a recognition of the incredible impact privacy issues have on our society and the importance of Nextcloud in providing a solution. By donating the prize money to a diversity goal, I hope it will help catalyze another transformation that society needs".
</blockquote>

Lydia Pintscher, president of KDE e.V.'s Board was equally thrilled: 

<blockquote style="font-family: Arial, sans;font-style: italic; font-size: 14px; line-height: 20px; color: #666; text-align: left; width: 85%;">We'd like to congratulate Frank on winning this prize and his decision to put the money to a great cause in open source. For us, collaborating with Nextcloud in this way is a bit of a homecoming as it is one of the most successful projects to emerge from our community over the past decade".</blockquote>

It is worth remembering that Nextcloud started life as a KDE project.

Nextcloud, Frank and KDE would like to invite community members who want to get involved in Nextcloud but face significant social hurdles to get in touch with our Include team. You can find more information on <a href="https://nextcloud.com/include ">Nextcloud Include page</a>.

The Reinhard von Koenig award promotes excellence in progress and technology. Previous winners include notable staff members from Daimler AG and Atlatec GmbH for work on self-driving cars.
<!--break-->