---
title: "Kdenlive: \"Test our beta, test the future\""
date:    2018-07-05
authors:
  - "Paul Brown"
slug:    kdenlive-test-our-beta-test-future
comments:
  - subject: "Kdenlive"
    date: 2018-09-12
    body: "<p>Yes tested this.&nbsp; It is not stabil but does work.</p><p>Still trying to figure out why kdenlive is giving out of sync audio on render.&nbsp; Never had this problem before.</p><p>Had to offset audio by about 24 frames ahead to fix it.</p>"
    author: "Franklin Russell"
---
<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/testthefuture.png"><img src="/sites/dot.kde.org/files/testthefuture.png" /></a><br /><figcaption>Kdenlive version 18.08 beta.</figcaption></figure>
</div>

<b><a href="https://kdenlive.org/en/2018/07/kdenlive-test-the-future/">The Kdenlive project is calling on their users to test a refactored version of their full-featured and Free Software video-editing application.</a></b>

Apart from re-writing a lot of the internals to clean up the code and make Kdenlive more efficient and easier to stabilize, this beta adds a bunch of new and interesting features. For example, the video and audio from clips are now automatically separated when dropped in the timeline, the slow motion effect now works and insert/lift/overwrite should also work reliably. Another thing you can do is install new keyboard layouts with one click. This means that, if you are coming from another video-editing software and relied on its shortcuts, you can still be equally productive with Kdenlive. For <a href="https://kdenlive.org/en/2018/07/kdenlive-test-the-future/">a full list of the new features in Kdenlive 18.08 Beta 17, take a look at the article on the projects site</a>.

You can also be part of improving Kdenlive: <a href="https://files.kde.org/kdenlive/unstable/kdenlive-18.08-beta17-x86_64.AppImage.mirrorlist">Head over to Kdenlive's download page, download the Appimage</a>, make it executable, run it and try editing some of your projects. Make a note of what doesn't work or misbehaves and <a href="https://kdenlive.org/en/bug-reports/">send in a bug report</a>.

Hey presto! You just helped make Kdenlive better!

<b style="color:red">A word of warning:</b> Kdenlive version 18.08 beta 17, as its name implies, is beta software. It is not stable and some features will not work. Do not use this as your main production video-editing software. Also do not overwrite any important project files with files produced with the beta version of Kdenlive, since compatibility with older versions of Kdenlive is a known issue.

<a href="https://kdenlive.org/en/download/">You can download and start using the latest stable version of Kdenlive from here</a>.
<!--break-->