---
title: "Plasma 5.13, a new version of KDE's desktop environment, is here"
date:    2018-06-13
authors:
  - "Paul Brown"
slug:    plasma-513-new-version-kdes-desktop-environment-here
comments:
  - subject: "Great!"
    date: 2018-06-18
    body: "<p>I love this Update so much! Thanks for it!</p>"
    author: "Karatek_HD"
  - subject: "Browser Integration"
    date: 2018-06-23
    body: "<p>Any chance to submit plasma-browser-integration plugin to Opera's store?</p><p>Or, can it be downloaded separately?</p><p>Thanks!</p>"
    author: "ricardo.barberis"
  - subject: "RHEL 7.5"
    date: 2018-06-24
    body: "<p>Is KDE Plasma 5.3 available for RHEL 7.5?</p>"
    author: "Udai Bhan Kashyap"
  - subject: "Stopped at 5.13.2?"
    date: 2018-07-20
    body: "<p>Arch Linux and PCLinuxOS are with 5.13.3 since July, 16. Waiting for it at KDE Neon User Edition.</p>"
    author: "Flavio R. Cavalcanti"
---
<iframe style="display:block; margin: 1em auto 1em;" width="560" height="315" src="https://www.youtube-nocookie.com/embed/C2kR1_n_d-g?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<b>Optimized and less resource-hungry, Plasma 5.13 can run smoothly on under-powered ARM laptops, high-end gaming PCs, and everything in between.</b>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/browser%20integration_video_controls.png"><img src="/sites/dot.kde.org/files/browser%20integration_video_controls.png" /></a><br /><figcaption>Control play back, rewind and volume even if your browser is not visible.</figcaption></figure>

Feature-wise, Plasma 5.13 comes with <i>Browser Integration</i>. This means both Chrome/Chromium and Firefox web browsers can be monitored and controlled using your desktop widgets. For example, downloads are displayed in the Plasma notification popup, so even if your browser is minimized or not visible, you can monitor the download progress. Likewise with media playing in a tab: you can use Plasma's media controls to stop, pause and silence videos and audio playing in any tab – even the hidden ones. This a perfect solution for those annoying videos that auto-start without your permission. Another Plasma-browser feature is that links can now be opened from Plasma's overhead launcher (Krunner), and you can also send links directly to your phone using KDE Connect.

Talking of KDE Connect, the Media Control Widget has been redesigned and its support of the MPRIS specification has been much improved. This means more media players can now be controlled from the media controls in the desktop tray or from your phone using KDE Connect.

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/launcher_widgets_blur_1500.jpg"><img src="/sites/dot.kde.org/files/launcher_widgets_blur_1500.jpg" /></a><br /><figcaption>Blurred backgrounds bring an extra level of coolness to Plasma 5.13.</figcaption></figure>

Plasma 5.13 is also visually more appealing. The redesigned pages in 5.13 include theming tools for desktops, icons and cursors, and you can download new splash screens from the KDE Store directly from the splash screen page. The desktop provides a new and efficient blur effect that can be used for widgets, the dashboard menu and even the terminal window, giving them an elegant and modern look. Another eye-catching feature is that the login and lock screens now display the wallpaper of the current Plasma release, and the lock screen incorporates a slick fade-to-blur transition to show the controls, allowing it to be easily used as a screensaver.

Discover, Plasma's graphical software manager, improves the user experience with list and category pages that replace header images with interactive toolbars. You can sort lists, and they also show star ratings of applications. App pages and app icons use your local icon theme to better match your desktop settings.

Vaults, Plasma's storage encryption utility, includes a new CryFS backend, better error reporting, a more polished interface, and the ability to remotely open and close vaults via KDE Connect.

Connecting to external monitors has become much more user-friendly. Now, when you plug in a new external monitor, a dialog pops up an lets you easily control the position of the additional monitor in correlation to your primary one.

Want to try Plasma 5.13? ISO images for KDE neon will probably be available tomorrow or on Friday. <a href="https://community.kde.org/Plasma/Live_Images">Check out our page with links to Live images to download the latest</a>.

We look forward to hearing your comments on Plasma 5.13 - let us know how it works for you!

<hr />

<a href="https://www.kde.org/announcements/plasma-5.13.0.php">Full announcement.</a>
<!--break-->