---
title: "Akademy 2018 videos are now online"
date:    2018-09-07
authors:
  - "Paul Brown"
slug:    akademy-2018-videos-are-now-online
comments:
  - subject: "Improve audio quality in next Akademy"
    date: 2019-09-01
    body: "<p>I listened to those great talks but I had a hard time understanding what presenter say, and people that asked questions even worse. Would be good to improve audio quality. Thanks.</p>"
    author: "John"
---
If you missed any of the <a href="https://conf.kde.org/en/Akademy2018/public/schedule/1">talks</a>, or couldn't make it to Vienna to attend this year's Akademy, now you can watch the recordings from the comfort of your home. You can find and <a href="https://files.kde.org/akademy/2018/videos/">download the videos from our repository</a>, or browse and share them from the <a href="https://www.youtube.com/playlist?list=PLsHpGlwPdtMraXbFHhkFx7-QHpEl9dOsL">YouTube playlist we have set up especially for all Akademy 2018 videos</a>.

We recommend starting with this year's keynotes, so make sure to watch <a href="https://dot.kde.org/2018/03/21/dan-bielefeld-keynote-speaker-akademy-2018-exposing-injustice-through-use-technology">Dan Bielefeld</a> talk about how the <a href="https://en.tjwg.org/">Transitional Justice Working Group</a> locates and uncovers sites for crimes against humanity committed by the Kim regime in North Korea:

<video width="750" controls="1">
  <source src="https://files.kde.org/akademy/2018/videos/Akademy2018-78-keynote_mapping_crimes_against_humanity_in_north_korea_with_foss.mp4" type="video/mp4">
Your browser does not support the video tag.
<a href="https://files.kde.org/akademy/2018/videos/Akademy2018-78-keynote_mapping_crimes_against_humanity_in_north_korea_with_foss.mp4">Mapping Crimes against Humanity</a>
</video> 

Also, don't miss what <a href="https://dot.kde.org/2018/05/29/claudia-garad-executive-director-wikimedia-%C3%B6sterreich-we-want-create-welcoming-atmosphere">Claudia Garad<a/> has to say about onboarding new contributors into an open community:

<video width="750" controls="1">
  <source src="https://files.kde.org/akademy/2018/videos/Akademy2018-79-keynote_w_for_welcome.mp4" type="video/mp4">
Your browser does not support the video tag.
<a href="https://files.kde.org/akademy/2018/videos/Akademy2018-79-keynote_w_for_welcome.mp4">"W" is for "Welcome"</a>
</video> 

If you prefer a more KDE-specific topic, watch Nate Graham lay out a seven-point plan that will help KDE take over the world:

<video width="750" controls="1">
  <source src="https://files.kde.org/akademy/2018/videos/Akademy2018-50-konquering_the_world.mp4" type="video/mp4">
Your browser does not support the video tag.
<a href="https://files.kde.org/akademy/2018/videos/Akademy2018-50-konquering_the_world.mp4">Konquering the world</a>
</video> 

<h2>About Akademy</h2>

For most of the year, KDE -- one of the largest free and open software communities in the world-- works on-line by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2018">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.
<!--break-->