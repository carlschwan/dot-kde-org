---
title: "Welcome Our New Google Summer of Code Students"
date:    2018-05-18
authors:
  - "valoriez"
slug:    welcome-our-new-google-summer-code-students
---
<p>KDE Student Programs is happy to present our 2018 Google Summer of Code students to the KDE Community.</p> 

<p>Welcome&nbsp;Abhijeet Sharma, Aman Kumar Gupta, Amit Sagtani, Andrey Cygankov, Andrey Kamakin, Anmol Gautam, Caio Jordão de Lima Carvalho, Chinmoy Ranjan Pradhan, Csaba Kertesz, Demetrio Carrara, Dileep Sankhla, Ferencz Kovács, Furkan Tokac, Gun Park, Iván Yossi Santa María González, Kavinda Pitiduwa Gamage, Mahesh S Nair, Tarek Talaat, Thanh Trung Dinh, Yihang Zhou, and Yingjie Liu!</p>

<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/kde-soc-bof-2017.jpg"><img src="/sites/dot.kde.org/files/kde-soc-bof-2017.jpg" width="600" height="auto" /></a><br /><figcaption>KDE Google Summer of Code mentors at Akademy 2017. Photo by Bhushan Shah.</figcaption></figure>
</div>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey"><a href="/sites/dot.kde.org/files/kstars_android.png"><img src="/sites/dot.kde.org/files/kstars_android.png" width="200" /></a><br /><figcaption>Students will work on<br />improving KStars for Android.</figcaption></figure>

<p>This year <a title="digiKam" href="https://www.digikam.org/" target="_blank">digiKam</a>, KDE's professional photo management application, has three students: Tarek Talaat will be working on supporting Twitter and One Drive services in digiKam export, Thanh Trung Dinh on Web Services tools authentication with OAuth2, and Yingjie Liu on adding the possibility to manually sort the digiKam icon view.</p> 

<p><a title="Plasma" href="https://plasma.kde.org/" target="_blank">Plasma</a>, KDE's graphical desktop environment, will also be mentoring three students. Abhijeet Sharma will be working on fwupd integration with <a href="https://userbase.kde.org/Discover">Discover</a> (KDE's graphical software manager), Furkan Tokac will improve handling for touchpads and mice with Libinput, and Gun Park will port keyboard input modules to Qt Quick and expand scope to cover input method configuration for <i>System Settings</i>.</p>  

<p>Another project with three students is <a title="Krita" href="https://krita.org/" target="_blank">Krita</a>, KDE's popular graphic editor and painting application. Andrey Kamakin will improve multithreading in Krita's Tile Manager; Iván Yossi Santa María González (ivanyossi) will optimize Krita Soft, Gaussian and Stamp brushes mask generation to use AVX with Vc Library; and Yihang Zhou (Michael Zhou) is creating a Swatches Docker for Krita.</p>

<p><a title="GCompris" href="https://gcompris.net/" target="_blank">GCompris</a>, the suite of educational programs and games for young learners, takes two students: Aman Kumar Gupta will port all GTK+ piano activities and get it one step closer to version 1.0, and Amit Sagtani will work on creating bitmap drawing and animation activities while preparing Gcompris for version 1.0.</p> 

<p><a title="Labplot" href="https://labplot.kde.org/" target="_blank">Labplot</a>, KDE's application for scientific data plotting and analysis, also mentors two students. Andrey Cygankov will add support for import data from web-service in LabPlot, and Ferencz Kovács will be working on plotting of live MQTT data.</p> 

<figure style="float: left; padding: 1ex; margin: 1ex;border: 1px solid grey"><a href="/sites/dot.kde.org/files/Falkon.png"><img src="/sites/dot.kde.org/files/Falkon.png" width="300" /></a><br /><figcaption>Falkon, a new member of the KDE family,<br />will also get some GSoC love.</figcaption></figure>

<p><a title="Okular" href="https://www.kde.org/applications/graphics/okular/" target="_blank">Okular</a>, KDE's PDF and document viewer, gets another two students: Chinmoy Ranjan Pradhan will be working on verifying signatures of PDF files, while Dileep Sankhla will implement the FreeText annotation with FreeTextTypeWriter behavior.</p>

<p>For <a title="Falkon" href="https://www.falkon.org/" target="_blank">Falkon</a>, a community developed web browser and a new member of the KDE family, Anmol Gautam will be working on JavaScript/QML extension support, and Caio Jordão de Lima Carvalho will finish LVM support and implement RAID support in KDE <i>Partition Manager</i> and <a href="https://calamares.io/">Calamares</a> (an advanced system installer).</p> 

<p>Csaba Kertesz (kecsap) will aim to improve the desktop and the Android version of <a title="KStars" href="https://www.kde.org/applications/education/kstars/" target="_blank">KStars</a>, KDE's planetarium program, while Kavinda Pitiduwa Gamage will work on <a title="KGpg" href="https://www.kde.org/applications/utilities/kgpg/" target="_blank">KGpg</a>, KDE's graphical key management application, to make it better.</p> 

<p>Mahesh S. Nair will expand <a title="Peruse" href="https://peruse.kde.org/" target="_blank">Peruse Creator</a>, adding more features to KDE's easy-to-use comic book reader. Finally, Demetrio Carrara will be working on the <a title="WikitoLearn" href="https://www.wikitolearn.org/" target="_blank">WikitoLearn</a> production-ready Progressive Webapp (PWA).</p>

<p>Traditionally, Google Summer of Code starts with an introduction period where students get to know their mentors, after which they start coding. The coding period for 2018 has began on May 14, and will last until August 6. We wish all our students a productive, successful, and fun summer!</p>
<!--break-->