---
title: "The New KDE Slimbook II: A sleek and powerful Plasma-based Ultrabook"
date:    2018-02-08
authors:
  - "Paul Brown"
slug:    new-kde-slimbook-ii-sleek-and-powerful-plasma-based-ultrabook
comments:
  - subject: "KDE Slimbook versus Katana II with Neon preinstalled"
    date: 2018-02-08
    body: "<p>You say Katana II doesn't come with KDE Neon pre-installed, but in their store they actually have this option (among many others). So what difference is there, does the KDE Slimbook have some extra customisations from the KDE team?</p>"
    author: "Andrei"
  - subject: "OS Updates"
    date: 2018-02-09
    body: "<p>I'd be curious on how it handles complete OS updates. One of my pet peeves is having to go through the process of backing up data (which is the least of it) in order to wipe the drive and install a \"new\" update with the latest DE, etc - complete with updated kernel. A KDE-based distro would be a new experience for me but I still want to treat it like a stable, modern OS. This leads to my second question...</p><p>&nbsp;</p><p>If, after installing tons of new programs, testing themes and doing what I would normally do with virtually any other OS - can I reset the OS back to it's original state? It's something that's built-in on all other OSes, so the question isn't far-fetched. Or do I have to get my hands dirty with lots of arcane commands, days of asking questions on 3rd-party forums, etc and in the end... not get the original distro/OS - exactly how it was shipped.</p>"
    author: "john379"
  - subject: "AMD based Slimbooks please."
    date: 2018-02-09
    body: "<p>Specter and Meltown bug ridden intel hardware noone wants.</p>"
    author: "Lumumba"
  - subject: "AMD based Slimbooks please"
    date: 2018-02-14
    body: "<p>Well, I am not convinced about the inherent supperiority of the AMD CPUs, but I prefer them myself too. So, yes, an AMD configuration would be appreciated, and probably result in a sale in my case.</p>"
    author: "MichaelSD"
---
<strong><a href="https://slimbook.es/en/kde-slimbook-2-ultrabook-laptop">There is a new KDE Slimbook on sale as from today</a>. The KDE Slimbook II is svelte and smart on the outside, but powerful and fast on the inside.</strong>

<figure style="position: relative; left: 50%; margin-left:-320px;padding: 1ex; width: 640px;"><a href="/sites/dot.kde.org/files/00004.jpg"><img src="/sites/dot.kde.org/files/00004.jpg" /></a></figure>

To start with, it comes with a choice between an Intel i5: 2.5 GHz Turbo Boost 3.1 GHz - 3M Cache CPU, or an  Intel i7: 2.7 GHz Turbo Boost 3.5 GHz with a 4M Cache. This makes the KDE Slimbook II 15% faster on average than its predecessor. The RAM has also been upgraded, and the KDE Slimbook now sports 4, 8, or 16 GBs of DDR4 RAM which is 33% faster than the DDR3 RAM installed on last year's model. 

Other things to look forward to include:
<li>a crisp FullHD 13.3'' screen,</li> 
<li>the dual hard drive bay that gives you room for a second hard disk,</li> 
<li>a bigger multi-touch touchpad that supports all kinds of gestures and clicks,</li> 
<li>a slick backlit keyboard, more powerful WiFi antennas,</li> 
<li>and 3 full-sized USB ports, one of which is the new reversible USB-C standard.</li> <br />

You can <a href="https://slimbook.es/en/katana-ii-the-ultrabook-aluminum#fichatecnica">check out the KDE Slimbook's full specs here</a> (note that the Katana II is made by the same people and is the same hardware, but does not come with KDE neon pre-installed and pre-configured).

<a href="http://kde.slimbook.es/">The KDE community has worked closely with Slimbook</a> to make sure that everything works as it should. After test-running the KDE Slimbook II extensively, we can confirm it is sleek, we can confirm it is powerful, and we can confirm that beginners and power users alike will enjoy this full-featured and modern Plasma-based laptop.

<!--break-->