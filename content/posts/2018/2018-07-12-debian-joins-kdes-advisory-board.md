---
title: "Debian Joins KDE's Advisory Board"
date:    2018-07-12
authors:
  - "thomaspfeiffer"
slug:    debian-joins-kdes-advisory-board
comments:
  - subject: "Does this mean more up to"
    date: 2018-07-12
    body: "<p>Does this mean more up to date versions of KDE in Debian stable ?</p>"
    author: "morgan"
  - subject: "That\u2019s great. I love both"
    date: 2018-07-12
    body: "<p>That\u2019s great. I love both Debian and KDE. I hope Debian switch to KDE as default desktop soon. Hhhh</p>"
    author: "Amine"
  - subject: "Welcome, welcome, welcome!"
    date: 2018-07-13
    body: "<p>As part of not only the KDE community but also the Kubuntu team, I welcome Debian into our Advisory Board. Debian's depth of experience with community and software is unmatched. We will enrich one another.&nbsp;</p>"
    author: "Valorie Cowan Zimmerman"
---
<style>
blockquote{
  display:block;
  background: #fff;
  padding: 15px 20px 15px 70px;
  margin: 0 0 20px;
  position: relative;
  
  /*Font*/
  /*font-family: Georgia, serif;*/
  font-style: italic;
  font-size: 14px;
  line-height: 20px;
  color: #666;
  text-align: left;
  
  /*Borders - (Optional)*/
  border-left: 15px solid #0068c6;
  border-right: 2px solid #0068c6;
  
  /*Box Shadow - (Optional)*/
  -moz-box-shadow: 2px 2px 15px #ccc;
  -webkit-box-shadow: 2px 2px 15px #ccc;
  box-shadow: 2px 2px 15px #ccc;
}

blockquote::before{
  content: "\00BB"; /*Unicode for Left Double Quote*/
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 60px;
  font-weight: bold;
  color: #999;
  
  /*Positioning*/
  position: absolute;
  left: 10px;
  top:10px;
}

blockquote::after{
  /*Reset to make sure*/
  content: "";
}

blockquote em{
  font-style: italic;
}
</style>


<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/dc6_group_photo_big_0.jpg"><img src="/sites/dot.kde.org/files/dc6_group_photo_big_0.jpg" /></a><br /><figcaption style="font-size:x-small;">The Debian community meets at Debconf 6 in Mexico. Photo by Joey Hess, licensed under CC By 4.0.</figcaption></figure>
</div>

Since the KDE Advisory Board <a title="Announcement of the KDE Advisory Board" href="https://dot.kde.org/2016/09/26/announcing-kde-advisory-board">was created in 2016</a>, we have been encouraging more and more organizations to join it, either as patrons or as non-profit partner organizations. With <a href="https://www.ubuntu.com/">Ubuntu</a> (via Canonical) and <a href="https://www.opensuse.org/">openSUSE</a> (via SUSE) we already had two popular Linux distributions represented in the Advisory board. They are now joined by one of the biggest and oldest purely community-driven distributions: <a href="https://www.debian.org/">Debian</a>.

KDE has a long-standing and friendly relationship with Debian, and we are happy to formalize it now. Having Debian on our Advisory Board will allow us to learn from them, share our experience with them, and deepen our collaboration even further.

As is tradition, we will now hand over the stage to the Debian Project Leader, Chris Lamb, who will tell you a bit about Debian and why he is happy to accept our invitation to the Advisory Board:

<blockquote style="font-family: Arial, sans;font-style: italic; font-size: 14px; line-height: 20px; color: #666; text-align: left;">
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/lamby2.jpg"><img src="/sites/dot.kde.org/files/lamby2.jpg" /></a><br /><figcaption style="font-size: small;">Chris Lamb.</figcaption></figure>Debian is a stable, free and popular computer operating system trusted by millions of people across the globe, from solo backpackers, to astronauts on the International Space Station, and from small companies, to huge organisations.<br/><br/>Founded in 1993, Debian has since grown into a volunteer organisation of over 2,000 developers from more than 70 countries worldwide collaborating every day via the Internet.<br/><br/>The KDE Plasma desktop environment is fully-supported within Debian and thus the Debian Project is extremely excited to be formally recognising the relationship between itself and KDE, especially how that will greatly increase and facilitate our communication and collaboration.
</blockquote>

<!--break-->