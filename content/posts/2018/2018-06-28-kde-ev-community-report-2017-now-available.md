---
title: "The KDE e.V. Community Report for 2017 is now available"
date:    2018-06-28
authors:
  - "Paul Brown"
slug:    kde-ev-community-report-2017-now-available
---
<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/board-sprint-autumn.jpg"><img src="/sites/dot.kde.org/files/board-sprint-autumn.jpg" /></a><br /><figcaption>KDE e.V. board at their autumn sprint.</figcaption></figure>
</div>

<B>If there is one document you want to read to discover what KDE has been up to and where we are right now, <a href="https://ev.kde.org/reports/ev-2017/">this is the one</a>.</B>

KDE's yearly report gives a comprehensive overview of all that has happened during 2017. It covers the progress we have made with KDE's Plasma desktop environment; Plasma Mobile (KDE's graphical environment for mobile devices); and applications the community creates to stimulate your productivity, creativity, education, and fun.

The report also looks at KDE's activities during 2017, giving details on the results from community sprints, conferences, and external events the KDE community has participated in worldwide. It also covers what is probably the most important community milestone of 2017: defining and agreeing on what are the most important global goals, goals that will direct the efforts of KDE community members for years to come.

You can also find out about the inner workings of KDE e.V., the foundation that legally represents the community. Check KDE's financial status and read up about the KDE e.V. board members, the different working groups, the Advisory Board, and how they all work together to keep KDE moving forward.

<a href="https://ev.kde.org/reports/ev-2017">Read the full report here</a>.
<!--break-->