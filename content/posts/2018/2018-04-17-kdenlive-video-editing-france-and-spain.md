---
title: "Kdenlive: Video Editing in France and Spain"
date:    2018-04-17
authors:
  - "Paul Brown"
slug:    kdenlive-video-editing-france-and-spain
---
<img style="" src="/sites/dot.kde.org/files/Captura%20de%20tela%20de%202018-04-16%2018-23-10.jpg">

<b>The Kdenlive team, creators of KDE's non-linear video editor, <a href="https://kdenlive.org/2018/04/kdenlive-in-paris/">will be holding their next sprint at the Carrefour Numérique in the Cité des Sciences in Paris next week</a>.</b>

The sprint will run from the 25th to the 29th of April, and two days will be open to the public. On Friday, 27th of April, from 4pm to 6pm the event will be open to anyone interested in getting involved. You can meet the team and learn how you can contribute to the project. On Saturday, 28th of April at 2.45pm, there will be a public presentation. You can discover Kdenlive as used by professional editors and learn about the new features.

Just in case you can't make it to Paris, but can get to the south of Spain: directly after the sprint, the team will fly to Seville to participate in the <a href="http://libregraphicsmeeting.org/2018/">Libre Graphics Meeting</a>.

Come make movies with us!
<!--break-->

<img style="" src="/sites/dot.kde.org/files/Screenshot_2018-04-16_22-58-44.jpg">