---
title: "Plasma 5.13 Beta"
date:    2018-05-18
authors:
  - "jriddell"
slug:    plasma-513-beta
---

 <style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px; 
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}

figure {
    position: relative;
    z-index: 2;
    font-size: smaller;
    text-shadow: 2px 2px 5px light-grey;
}
</style>

<main class="releaseAnnouncment container">

	<figure class="topImage">
		<a href="http://www.kde.org/announcements/plasma-5.13/plasma-5.13.png">
			<img src="https://www.kde.org/announcements/plasma-5.13/plasma-5.13-wee.png" width="600" height="338" alt="Plasma 5.13 Beta">
		</a>
		<figcaption>KDE Plasma 5.13 Beta</figcaption>
	</figure>

	<p>
		Thursday, 17 May 2018.		Today KDE unveils a beta release of Plasma 5.13.0.	</p>

	<p>
		Members of the Plasma team have been working hard to continue making Plasma a lightweight and responsive desktop which loads and runs quickly, but remains full-featured with a polished look and feel.  We have spent the last four months optimising startup and minimising memory usage, yielding faster time-to-desktop, better runtime performance and less memory consumption.  Basic features like panel popups were optimised to make sure they run smoothly even on the lowest-end hardware.  Our design teams have not rested either, producing beautiful new integrated lock and login screen graphics.	</p>
<!--break-->
	<br clear="all" />

<h2>New in Plasma 5.13</h2>

<br clear="all" />
<h3>Plasma Browser Integration</h3>
<p>Plasma Browser Integration is a suite of new features which make Firefox and Chrome, and Chromium-based browsers work with your desktop.  Downloads are now displayed in the Plasma notification popup just as when transferring files with Dolphin.  The Media Controls Plasmoid can mute and skip videos and music playing from within the browser.  You can send a link to your phone with KDE Connect.  Browser tabs can be opened directly using KRunner via the Alt-Space keyboard shortcut.  To enable Plasma Browser Integration, add the relevant plugin from the addon store of your favourite browser.</p>
<figure style="text-align: right">
<a href="http://www.kde.org/announcements/plasma-5.13/pbi-download-integration.png">
<img src="http://www.kde.org/announcements/plasma-5.13/pbi-download-integration-wee.png" style="border: 0px" width="350" height="233" alt="Plasma Browser Integration for Downloads" />
</a>&nbsp;&nbsp;&nbsp;
<a href="http://www.kde.org/announcements/plasma-5.13/pbi-video-controls.png">
<img src="http://www.kde.org/announcements/plasma-5.13/pbi-video-controls-wee.png" style="border: 0px" width="276" height="233" alt="Plasma Browser Integration for Media Controls" />
</a>
<figcaption style="text-align: right">Plasma Browser Integration for Downloads and Media Controls</figcaption>
</figure>

<br clear="all" />
<h3>System Settings Redesigns</h3>
<p>Our settings pages are being redesigned.  The KDE Visual Design Group has reviewed many of the tools in System Settings and we are now implementing those redesigns.  KDE's Kirigami framework gives the pages a slick new look.  We started off with the theming tools, comprising the icons, desktop themes, and cursor themes pages.  The splash screen page can now download new splashscreens from the KDE Store.  The fonts page can now display previews for the sub-pixel anti-aliasing settings.</p>
<figure style="text-align: right">
<a href="http://www.kde.org/announcements/plasma-5.13/kcm-desktop-theme.png">
<img src="http://www.kde.org/announcements/plasma-5.13/kcm-desktop-theme-wee.png" style="border: 0px" width="350" height="252" alt="Desktop Theme" />
</a>
<a href="http://www.kde.org/announcements/plasma-5.13/kcm-fonts-hint-preview.png">
<img src="http://www.kde.org/announcements/plasma-5.13/kcm-fonts-hint-preview-wee.png" style="border: 0px" width="350" height="252" alt="Font Settings" />
</a>
<a href="http://www.kde.org/announcements/plasma-5.13/kcm-icons.png">
<img src="http://www.kde.org/announcements/plasma-5.13/kcm-icons-wee.png" style="border: 0px" width="350" height="252" alt="Icon Themes" />
</a>
<figcaption style="text-align: right">Redesigned System Settings Pages</figcaption>
</figure>

<br clear="all" />
<h3>New Look for Lock and Login Screens</h3>
<p>Our login and lock screens have a fresh new design, displaying the wallpaper of the current Plasma release by default. The lock screen now incorporates a slick fade-to-blur transition to show the controls, allowing it to be easily used like a screensaver.</p>
<figure style="text-align: right">
<a href="http://www.kde.org/announcements/plasma-5.13/lockscreen.png">
<img src="http://www.kde.org/announcements/plasma-5.13/lockscreen-wee.png" style="border: 0px" width="350" height="197" alt="Lock Screen" />
</a>
<a href="http://www.kde.org/announcements/plasma-5.13/login.png">
<img src="http://www.kde.org/announcements/plasma-5.13/login-wee.png" style="border: 0px" width="350" height="172" alt="Login Screen" />
</a>
<figcaption style="text-align: right">Lock and Login Screen new Look</figcaption>
</figure>

<br clear="all" />
<figure style="float: right; text-align: right">
<a href="http://www.kde.org/announcements/plasma-5.13/kwin-blur-dash.png">
<img src="http://www.kde.org/announcements/plasma-5.13/kwin-blur-dash-wee.png" style="border: 0px" width="350" height="219" alt="Improved Blur Effect in the Dash Menu" />
</a>
<figcaption style="text-align: right">Improved Blur Effect in the Dash Menu</figcaption>
</figure>
<h3>Graphics Compositor</h3>
<p>Our compositor KWin gained much-improved effects for blur and desktop switching. Wayland work continued, with the return of window rules, the use of high priority EGL Contexts, and initial support for screencasts and desktop sharing.</p>

<br clear="all" />
<figure style="float: right; text-align: right">
<a href="http://www.kde.org/announcements/plasma-5.13/discover.png">
<img src="http://www.kde.org/announcements/plasma-5.13/discover-wee.png" style="border: 0px" width="350" height="327" alt="Discover's Lists with Ratings, Themed Icons, and Sorting Options" />
</a>
<figcaption style="text-align: right">Discover's Lists with Ratings, Themed Icons, and Sorting Options</figcaption>
</figure>
<h3 id="discover">Discover</h3>
<p>Discover, our software and addon installer, has more features and sports improvements to the look and feel.</p>

<p>Using our Kirigami UI framework we improved the appearance of lists and category pages, which now use toolbars instead of big banner images.  Lists can now be sorted, and use the new Kirigami Cards widget.  Star ratings are shown on lists and app pages.  App icons use your local icon theme better match your desktop settings.  All AppStream metadata is now shown on the application page, including all URL types.  And for users of Arch Linux, the Pacman log is now displayed after software updates.</p>

<p>Work has continued on bundled app formats.  Snap support now allows user control of app permissions, and it's possible to install Snaps that use classic mode.  And the 'snap://' URL format is now supported.  Flatpak support gains the ability to choose the preferred repository to install from when more than one is set up.</p>

<br clear="all" />
<h3>Much More</h3>
<p>Other changes include:</p>
<ul>
<li>A tech preview of <a href='http://blog.broulik.de/2018/03/gtk-global-menu/'>GTK global menu integration</a>.</li>
<li>Redesigned Media Player Widget.</li>
<li>Plasma Calendar plugin for astronomical events, currently showing: lunar phases & astronomical seasons (equinox, solstices).</li>
<li>xdg-desktop-portal-kde, used to give desktop integration for Flatpak and Snap applications, gained support for screenshot and screencast portals.</li>
<li>The Digital Clock widget allows copying the current date and time to the clipboard.</li>
<li>The notification popup has a button to clear the history.</li>
<li>More KRunner plugins to provide easy access to Konsole profiles and the character picker.</li>
<li>The Mouse System Settings page has been rewritten for libinput support on X and Wayland.</li>
<li>Plasma Vault has a new CryFS backend, commands to remotely close open vaults with KDE Connect, offline vaults, a more polished interface and better error reporting.</li>
<li>A new dialog pops up when you first plug in an external monitor so you can easily configure how it should be positioned.</li>
<li>Plasma gained the ability to fall back to a software rendering if OpenGL drivers unexpectedly fail.</li>
</ul><br />
<figure style="text-align: right">
<a href="http://www.kde.org/announcements/plasma-5.13/gedit-global-menu.png">
<img src="http://www.kde.org/announcements/plasma-5.13/gedit-global-menu-wee.png" style="border: 0px" width="349" height="250" alt="GEdit with Title Bar Menu" />
</a>&nbsp;&nbsp;
<a href="http://www.kde.org/announcements/plasma-5.13/media-plasmoid.png">
<img src="http://www.kde.org/announcements/plasma-5.13/media-plasmoid-wee.png" style="border: 0px" width="260" height="250" alt="Redesigned Media Player Widget" />
</a>&nbsp;&nbsp;
<a href="http://www.kde.org/announcements/plasma-5.13/connect-external-monitor.png">
<img src="http://www.kde.org/announcements/plasma-5.13/connect-external-monitor-wee.png" style="border: 0px" width="444" height="250" alt="Connect an External Monitor" />
</a>
<figcaption style="text-align: right">GEdit with Title Bar Menu. Redesigned Media Player Widget.  Connect an External Monitor Dialog.</figcaption>
</figure>



<br clear="all" />
	<!-- // Boilerplate again -->
	<section class="row get-it">
		<article class="col-md">
			<h2>Live Images</h2>
			<p>
				The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.			</p>
			<a href='https://community.kde.org/Plasma/Live_Images' class="learn-more">Download live images with Plasma 5</a>
			<a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more">Download Docker images with Plasma 5</a>
		</article>

		<article class="col-md">
			<h2>Package Downloads</h2>
			<p>
				Distributions have created, or are in the process of creating, packages listed on our wiki page.			</p>
			<a href='https://community.kde.org/Plasma/Packages' class="learn-more">Package download wiki page</a>
		</article>

		<article class="col-md">
			<h2>Source Downloads</h2>
			<p>
				You can install Plasma 5 directly from source.			</p>
			<a href='http://community.kde.org/Frameworks/Building'>Community instructions to compile it</a>
			<a href='http://www.kde.org/info/plasma-5.12.0.php' class='learn-more'>Source Info Page</a>
		</article>
	</section>

	<section class="give-feedback">
		<h2>Feedback</h2>

		<p>
			You can give us feedback and get updates on <a href='https://www.facebook.com/kde'><img src='https://www.kde.org/announcements/facebook.gif' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>
			or <a href='https://twitter.com/kdecommunity'><img src='https://www.kde.org/announcements/twitter.png' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>
			or <a href='https://plus.google.com/105126786256705328374/posts'><img src='https://www.kde.org/announcements/googleplus.png' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>.		</p>
		<p>
			Discuss Plasma 5 on the <a href='https://forum.kde.org/viewforum.php?f=289'>KDE Forums Plasma 5 board</a>.		</p>

		<p>You can provide feedback direct to the developers via the <a href='irc://#plasma@freenode.net'>#Plasma IRC channel</a>, <a href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel mailing list</a> or report issues via <a href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided'>bugzilla</a>. If you like what the team is doing, please let them know!
		<p>Your feedback is greatly appreciated.</p>

