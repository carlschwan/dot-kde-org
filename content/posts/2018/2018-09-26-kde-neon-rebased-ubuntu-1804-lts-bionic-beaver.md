---
title: "KDE neon Rebased on Ubuntu 18.04 LTS \"Bionic Beaver\""
date:    2018-09-26
authors:
  - "jriddell"
slug:    kde-neon-rebased-ubuntu-1804-lts-bionic-beaver
comments:
  - subject: "Well done!"
    date: 2018-10-01
    body: "<p>We're running a couple of PCs with KDE Neon in our business. All of them are hooked up to our network using Kerberos/ldap and NFS. We've just been doing a test-upgarde of one of the machines. And \"hats off\": everything is working as expected. Not a single issue... !</p>"
    author: "thomas"
  - subject: "Not able to upgrade"
    date: 2018-09-26
    body: "<p>I have updated and logged out but I get no notification. I am not the only one, another user on reddit has the same problem. Can you help me with this</p>"
    author: "Adutchman"
  - subject: "Advantages over Kubuntu?"
    date: 2018-09-26
    body: "<p>Advantages over Kubuntu? Disadvantages?</p>"
    author: "Anonymous"
  - subject: "Just upgraded my installation"
    date: 2018-09-26
    body: "<p>Just upgraded my installation to Bionic, didn't face a single problem.</p><p>Thanks for the great job!</p>"
    author: "caigoo"
  - subject: "ISO is still 16.04 based"
    date: 2018-09-26
    body: "<p>The ISO on the page you linked is still 16.04 based, anything wrong?</p><p>(https://files.kde.org/neon/images/neon-useredition/current/neon-useredition-20180925-0833-amd64.iso)</p>"
    author: "RedKite"
  - subject: "Wayland instead of Xorg?"
    date: 2018-09-27
    body: "<p>Wayland instead of Xorg?</p>"
    author: "Brian Zou"
  - subject: "Thank You"
    date: 2018-09-27
    body: "<p>Hi Jonathan, thank you for all your hard work at KDE and neon. I'm using neon since 2016 and so far it's very smooth. I'm using neon daily for both work and teaching purposes. I teach GNU/Linux to beginners by using neon. Thanks to all KDE developers and contributors for bringing 18.04 as new base for neon.</p><p>With respect,</p><p>Malsasa</p>"
    author: "Ade Malsasa Akbar"
  - subject: "KDE neon update"
    date: 2018-09-30
    body: "<p>All went well, no probles except&nbsp; Brother MFC Scanner installation, then thats a Brother problem as Linux and KDE are npt Windows.</p>"
    author: "Smokey"
  - subject: "Will there be an announcement"
    date: 2018-10-01
    body: "<p>Will there be an announcement of when our current 16.04 installs reaches End of Life? Or are they already unsupported now?</p>"
    author: "Anonymous"
  - subject: "KDE IS THE BEST"
    date: 2018-10-08
    body: "<p>we have KDE everything works great with KUBUNTU 18.04 well done KDE for the good work they do</p>"
    author: "linux USER"
  - subject: "Upgrade 100% Successful"
    date: 2018-10-09
    body: "<p>My little old HP 4530s is at least eight years old and runs Neon flawlessly. The upgrade kicked in last night (08/10/18 NZ). Initially thought the laptop had hung and had to reboot with the result that the upgrade dialogue popped up. Gave it the go ahead and it took about two hours but no issues. Firefox seemed a bit stuttery graphics wise for a start but quickly came right. Machine is fine today. Not a single problem.</p>"
    author: "Bruce"
  - subject: "Happy upgrade"
    date: 2018-10-22
    body: "<p>Hi, Johnatan</p><p>Every went fine with the upgrade from the previous KDE Neon baseo on 16.04. I switched from Ubuntu XFCE which I used for ten years and fall in love with Plasma at first glance.</p><p>I am also a forum KDE memeber since I install the KDE Neon version based on Ubuntu 16.04 at work.</p><p>Great job!</p><p>&nbsp;</p>"
    author: "j8a"
---
<img class="alignnone size-full wp-image-135" src="https://blog.neon.kde.org/wp-content/uploads/2018/09/neon-bionic.png" alt="" width="1014" height="147" />

The KDE neon team is proud to announce the rebase of our packages onto Ubuntu 18.04 LTS "Bionic Beaver".  We encourage all users to <a href="https://community.kde.org/Neon/BionicUpgrades">upgrade now</a>.  The <a href="https://neon.kde.org/download">installable ISOs and Docker images</a> have also been updated to run on 18.04.
<h3>What is KDE neon?</h3>
KDE neon is a project to deliver KDE's wonderful suite of software quickly.  We use modern DevOps techniques to automatically build, QA and deploy our packages.  We work directly with the KDE community rather than staying far away in a separate project.

Our packages are built on the latest Ubuntu LTS edition and today we have moved to their new 18.04 release.  This means our users can get newer drivers and third party packages.  There is an upgrade process from the previous 16.04 LTS base which we have spent the last few months writing and running QA on to ensure it runs smoothly.

We have three editions for different use cases.  A user edition for those wanting to use the latest released KDE software updated daily but only released when it passes QA tests.  And two developer editions built from unstable and beta Git branches without QA checks for those wanting to test or develop our forthcoming software.

You can use our output via the .deb package archive, installable ISOs and Docker images.  We also have work-in-progress Snap packages which we can put more development effort into now that we have rebased on 18.04.
<h3>The Upgrade</h3>
Many people have been keen for the rebase onto 18.04, thank you for your patience.  We implement many QA tests in neon and are keen that they should all pass.  A number of bugs have been found in the initial upgrade process which we have fixed.  These major upgrades can still break, especially if you have a lot of third party software, so do take backups before you start.  Please let us know on the <a href="https://bugs.kde.org">bug tracker</a> or <a href="https://forum.kde.org/viewforum.php?f=309&amp;sid=f819cb5928e2980bcb72f2320d107024">forum</a> how you get on.  Enjoy your updated neon!
<!--break-->
