---
title: "Plasma Sprint in Berlin"
date:    2018-05-14
authors:
  - "jriddell"
slug:    plasma-sprint-berlin
---
<p>Last month the developers of <a href="https://www.kde.org/plasma-desktop">Plasma, KDE's featureful, flexible and Free desktop environment</a>, held a sprint in Berlin, Germany. The developers gathered to discuss the forthcoming 5.13 release and future development of Plasma. Of course, they didn't just sit and chat - a lot of coding got done, too.</p>

<p>During the sprint, the Plasma team was joined by guests from Qt and <a href="http://swaywm.org/">Sway WM</a>. Discussion topics included sharing Wayland protocols, input methods, Plasma Browser Integration, tablet mode for Plasma's shell, porting KControl modules to QtQuick, and last but not least, the best beer in Berlin.</p>

<br clear="all" />
<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/8d7b5f11-8a2a-4a16-b79a-e21680912c0d.jpg"><img src="/sites/dot.kde.org/files/wee-8d7b5f11-8a2a-4a16-b79a-e21680912c0d.jpg_0.jpg" width="600" height="auto" /></a><br /><figcaption>Plasma Team Sprinting</figcaption></figure>
</div>

<h2>Constructive Discussions with SwayWM - Check!</h2>

<p>The effort to port Plasma to work on Wayland rather than X continues at a fast pace. Wayland protocols define how applications interact with the display, including tasks essential to Plasma such as declaring which "window" is really a panel. These protocols have to be defined by the Plasma team and preferably standardized with other users of the Linux desktop.</p>

<p>One newcomer to the field is SwayWM - a Wayland version of the i3 window manager. Drew DeVault, the lead developer of the project, joined our Plasma sprint to discuss where Wayland protocols could be shared. The team looked at their Layer Protocol, which covers much of the work of the current plasmashell protocol. We found that this protocol contains some nice ideas and suggested some improvements for the SwayWM developers.</p>

<p>The Plasma Output Management Protocol was also discussed. This protocol defines how external monitors are used, and Sway currently just reloads configuration files as needed. The team will consider this solution if the need for such a protocol arises. Protocols for Remote Access were compared and reviewed along with <a href="https://pipewire.org/">Pipewire</a> as systems for managing audio and video. <a href="https://drewdevault.com/2018/04/28/KDE-Sprint-retrospective.html">Drew wrote a blog post</a> with more information on this topic.</p>

<br clear="all" />
<div style='text-align: center'>
<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/bece1e2c-1dbc-4bf4-bfbd-4eb522bb4fbe.jpg"><img src="/sites/dot.kde.org/files/wee-bece1e2c-1dbc-4bf4-bfbd-4eb522bb4fbe.jpg.jpg" width="400" height="300" /></a><br /><figcaption>Plasma Team Sprinting</figcaption></figure>
</div>

<h2>Exciting Collaboration with Qt - Check!</h2>

<p>Shawn Rutledge, the lead developer of Qt's new input stack, also joined us for a few days of the sprint. Together, we reviewed the new API and looked at how some of the unique use-cases of Plasma would work with it. The conclusion was that "some parts, including complex drag-and-drop actions, went surprisingly smoothly".</p> 

<p>A bunch of design changes were suggested and improvements submitted. Working with Qt developers at this early stage is a great win for both projects, as it saves KDE developers a lot of time when they come to use the new features, while the Qt world gets a nicer result.</p>

<br clear="all" />
<div style='text-align: center'>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/eabc7c6e-97de-4b69-a2be-27ced2e4b903.jpg"><img src="/sites/dot.kde.org/files/wee-eabc7c6e-97de-4b69-a2be-27ced2e4b903.jpg.jpg" width="400" height="300" /></a><br /><figcaption>Thanks to Endocode for hosting us in central Berlin.</figcaption></figure>
</div>

<h2>Improved Plasma Browser Integration - Check!</h2>

<p>Plasma Browser Integration is a fun new feature that will be shipped with Plasma 5.13 next month.</p> 

<p>It means Firefox and Chrome/Chromium will use Plasma's file transfer widget for downloads and native Plasma notifications for browser notifications. Moreover, media controls will work with the task manager.</p>

<p>The browser extensions were tidied up, translations fixed, and accounts on the relevant browser store websites set up. Another decision made at the sprint was that we have a collective duty to make sure <a href="https://www.falkon.org">KDE's new web browser Falkon</a> is at feature-parity in terms of Plasma integration.</p>

<br clear="all" />
<div style='text-align: center'>
<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/2018-plasma-sprint-pinebook.jpg"><img src="/sites/dot.kde.org/files/2018-plasma-sprint-pinebook.jpg" width="400" height="300" /></a><br /><figcaption>Plasma running on a Pinebook</figcaption></figure>
</div>

<h2>Plasma on Pinebook and Tablet Mode - Check!</h2>

<p>The team continued to work on convergence with other form factors - in other words, on making Plasma run seamlessly on a variety of devices, both desktop and mobile. Bhushan worked on Plasma Mobile images for devices which supports upstream kernel, which is essential for security and more up-to-date system on mobile devices.</p>

<p>Rohan worked on making Plasma run smoothly and with all Free drivers on the low-end Pinebook laptop. This goes to show that Plasma can function as a lightweight desktop environment without losing the features.</p> 

<p>Lastly, Marco managed to get Plasma working on a convertible laptop with support for switching into tablet mode, illustrating how we can actively shift between form factors.</p>

<br clear="all" />
<div style='text-align: center'>
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/de775e15-e922-4c0d-b1d1-bc9e56bab197.jpg"><img src="/sites/dot.kde.org/files/wee-de775e15-e922-4c0d-b1d1-bc9e56bab197.jpg.jpg" width="400" height="300" /></a><br /><figcaption>Presenting to FSFE members</figcaption></figure>
</div>

<h2>Talks, Burritos, and Beer - Check!</h2>

<p>Throughout the week, we also gave talks to our host company <a href="https://endocode.com/">Endocode</a> who kindly lent us their central Berlin offices, complete with a fridge full of alcohol-free beer.</p>

<p>We also hosted an evening meetup for the local group of <a href="https://fsfe.org/">Free Software Foundation Europe</a> members and gave some talks over burritos.</p> 

<p>Special thanks to long-term KDE contributor Mirko of Endocode, who impressed us with his multi-monitor multi-activity high-definition display Plasma setup.</p>

<p>Having checked off all the items on our to-do list, we concluded another successful Plasma sprint. Look forward to seeing the results of our work in the upcoming Plasma 5.13 release!</p>

<br clear="all" />
<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/de2cf805-f484-46d8-9e83-efd6cc84317d.jpg"><img src="/sites/dot.kde.org/files/wee-de2cf805-f484-46d8-9e83-efd6cc84317d.jpg_0.jpg" width="600" height="auto" /></a><br /><figcaption>Plasma Team Closing the Sprint with Fine Dining</figcaption></figure>
</div>
<!--break-->