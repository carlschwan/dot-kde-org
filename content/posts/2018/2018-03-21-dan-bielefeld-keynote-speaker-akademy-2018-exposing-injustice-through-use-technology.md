---
title: "Dan Bielefeld, Keynote Speaker Akademy 2018:  Exposing Injustice Through the Use of Technology"
date:    2018-03-21
authors:
  - "Paul Brown"
slug:    dan-bielefeld-keynote-speaker-akademy-2018-exposing-injustice-through-use-technology
comments:
  - subject: "Kudos!"
    date: 2018-03-21
    body: "<p>Thrilled to see that such an active and effective activist will headline Akademy. Wecome Dan!</p>"
    author: "valoriez"
  - subject: "Very interesting interview."
    date: 2020-02-16
    body: "<p>Very interesting interview. Using technology to help human rights is such a great idea! <a id=\"clean-url\" class=\"element-hidden\" href=\"http://gokiwipokies.com/\">go kiwi pokies</a></p>"
    author: "Jay Miles"
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/Dan.jpg"><img src="/sites/dot.kde.org/files/Dan.jpg" /></a><br /><figcaption>Dan Bielefeld speaks at a Transitional Justice Working Group event.</figcaption></figure> 

<i>Dan Bielefeld is an activist that works for a South Korean NGO. Dan worked in the Washington, D.C. area training young activists in the areas of politics and journalism before going into researching atrocities committed by the North Korean regime. He is currently the Technical Director of the <a href="https://en.tjwg.org/">Transitional Justice Working Group</a> and helps pinpoint the locations of mass burial and execution sites using mapping technologies.</i>

<i>Dan will be delivering the opening keynote at this year's <a href="https://akademy.kde.org/2018">Akademy</a> and he kindly agreed to talk to us about activism, Free Software, and the sobering things he deals with every day.</i>

<span style="color:#f55;font-weight:bold">Paul Brown:</span> Hello Dan and thanks for agreeing to sit down with us for this interview.

<span style="color:#55f;font-weight:bold">Dan Bielefeld:</span> Thanks for the opportunity, Paul.

<span style="color:#f55;font-weight:bold">Paul:</span> You work for the the Transitional Justice Working Group, an organization that <a href="https://en.tjwg.org/TJWG_Report-Mapping_Crimes_Against_Humanity_in_North_Korea(July2017)-Eng_Final.pdf">researches human rights violations of the North Korean regime</a>, correct?

<span style="color:#55f;font-weight:bold">Dan:</span> Yes, we have a mapping project that tries to identify specific coordinates of sites with evidence related to human rights violations.

<span style="color:#f55;font-weight:bold">Paul:</span> And you were a web designer before joining the organization... I've got to ask: How does one make the transition from web designer to human rights activist?

<span style="color:#55f;font-weight:bold">Dan:</span> I was a web developer for several years before moving to Korea. When I moved here, I enrolled as a Korean language student and also spent most of my free time volunteering with North Korean human rights groups. So, unfortunately, that meant putting the tech stuff on hold for a while (except when groups wanted help with their websites).

<span style="color:#f55;font-weight:bold">Paul:</span> You are originally from the US, right?

<span style="color:#55f;font-weight:bold">Dan:</span> Yes, from Wisconsin.

<span style="color:#f55;font-weight:bold">Paul:</span> Was this a thing that preoccupied you before coming to Korea?

<span style="color:#55f;font-weight:bold">Dan:</span> I initially came on a vacation with no idea that I'd one day live and work here. In the lead-up to that trip, and especially after that trip, I sought out more information about Korea, which inevitably brought me repeatedly to the subject of North Korea.

Most of the news about North Korea doesn't grab my attention (talking about whether to resume talking, for instance), but the situation of regular citizens really jumped out at me. For instance, it must've been in 2005 or so that I read the book <a href="https://en.wikipedia.org/wiki/The_Aquariums_of_Pyongyang"><i>The Aquariums of Pyongyang</i></a> by a man who had literally grown up in a prison camp because of something his grandfather supposedly did. This just didn't seem fair to me. I had thought the gulags where only a thing of history, but I learned they still exist today.

<span style="color:#f55;font-weight:bold">Paul:</span> Wait... So people can inherit "crimes" in North Korea?

<span style="color:#55f;font-weight:bold">Dan:</span> They call it the "guilt-by-association" system. If your relative is guilty of a political crime (e.g., defected to the South during the Korean War), up to three generations may be punished.

<span style="color:#f55;font-weight:bold">Paul:</span> Wow. That is awful, but somehow I feel this is not the most awful thing I am going to hear today...

<span style="color:#55f;font-weight:bold">Dan:</span> For a long time I thought it was just North Korea, but I have since learned that this logic / punishment method is older than the division of the North and South. For a long time after the division, in the South it was hard to hold a government position if your relative was suspected of having fled to the North, for instance.

<span style="color:#f55;font-weight:bold">Paul:</span> What's your role in Transitional Justice Working Group?

<span style="color:#55f;font-weight:bold">Dan:</span> I'm the technical director, so I'm responsible for our computer systems and networks, which includes our digital security. I also manage the mapping project, and I am also building our mapping system. 

<span style="color:#f55;font-weight:bold">Paul:</span> Digital security... I read that North Korea is becoming a powerhouse when it comes to electronic terrorism. How much credibility do these stories have? I mean, they seem to be technologically behind in nearly everything else.

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/poster%20pic%20from%20foss4g%20-%20by%20David%20Weaver_1500.jpg"><img src="/sites/dot.kde.org/files/poster%20pic%20from%20foss4g%20-%20by%20David%20Weaver_1500.jpg" /></a><br /><figcaption>Dan explaining the work of the The Transitional Justice Working Group to conference attendees. Photo by David Weaver.</figcaption></figure> 

<span style="color:#55f;font-weight:bold">Dan:</span> This is a really interesting question and the answer is very important to my work, of course.

Going up against great powers like the US, the North Korean leadership practices asymmetrical warfare. Guerilla warfare, terrorism, these are things that can have a big impact with relatively little resources against a stronger power.

In digital security, offense tends to be easier than defense, so they naturally have gravitated online. Eike [<I>Hein -- vice-president at KDE e.V.</I>] and I went to a conference last year at which a journalist, Martyn Williams of <a href="https://www.northkoreatech.org/">NorthKoreaTech.org</a> said they train thousands of hackers from an early age. The average person in North Korea doesn't have a lot of money and may not even have a computer, but those the regime identifies and trains will have used computers and received a great deal of training from an early age. They do this not only for cyber-warfare, but to earn money for the regime. For instance, the $81 million from the Bangladesh bank heist.

<span style="color:#f55;font-weight:bold">Paul:</span> Ah, yes! They did Wannacry too.

<span style="color:#55f;font-weight:bold">Dan:</span> Exactly.

<span style="color:#f55;font-weight:bold">Paul:</span> Do your systems get attacked?

<span style="color:#55f;font-weight:bold">Dan:</span> One of our staff members recently received a targeted phishing email that looked very much like a proper email from Google. The only thing not real was the actual URL it went to. Google sent her the warning about being targeted by state-sponsored attackers and recommended she join their Advanced Protection Program, which they launched last year for journalists, activists, political campaign teams, and other high-risk users.

We of course do our best to monitor our systems, but the reality today is that you almost have to assume they're already in if they're motivated to do so.

<span style="color:#f55;font-weight:bold">Paul:</span> That is disturbing. So what do you do about that? What tools do you use to protect and monitor your systems?

<span style="color:#55f;font-weight:bold">Dan:</span> What I've learned over the last three years is that the hardest part of digital security is the human element. You can have the best software or the best system, but if the password is 123456 or is reused everywhere, you aren't really very secure. 

We try to make sure that, for instance, two-factor authentication is turned on for all online accounts that offer it -- for both work and personal accounts. You have to start with the low-hanging fruit, which is what the attackers do. No reason to burn a zero-day if the password is "password". Getting people to establish good digital hygiene habits is crucial. It's sort of like wearing a seatbelt -- using 2FA might take extra time every single time you do it, and 99.9% of the time, it's a waste of time, but you'll never really know in advance when you'll really need it, so it's best to just make it a habit and do it every time.

Another thing, of course, is defense in layers: don't assume your firewall stopped them, etc.

<span style="color:#f55;font-weight:bold">Paul:</span> What about your infrastructure? Bringing things more to our terrain: Do you rely on Free Software or do you have a mix of Free and proprietary? Are there any tools in particular you find especially useful in your day-to-day?

<span style="color:#55f;font-weight:bold">Dan:</span> I personally love FOSS and use it as much as I can. Also, being at a small NGO with a very limited budget, it's not just the freedom I appreciate, but the price often almost makes it a necessity.

<span style="color:#f55;font-weight:bold">Paul:</span> But surely having access to the code makes it a bit more trustworthy than proprietary blackboxes. Or am I being too biased here?

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/qgis.png"><img src="/sites/dot.kde.org/files/qgis.png" /></a><br /><figcaption>The Transitional Justice Working Group uses QGIS to locate sites of North Korean human rights violations.</figcaption></figure> 

<span style="color:#55f;font-weight:bold">Dan:</span> Not all of my colleagues have the same approach, but most of them use, for instance, LibreOffice everyday. For mapping, we use Postgres (with <a href="https://postgis.net/">PostGIS</a>) and <a href="https://www.qgis.org/en/site/">QGIS</a>, which are wonderful. QGIS is a massive project that so far we've only scratched the surface of. We also use Google Earth, which provides us with imagery of North Korea for our interviews (I realize GE is proprietary).

I agree, though, that FOSS is more trustworthy -- not just for security, but privacy reasons. It doesn't phone home as much!

<span style="color:#f55;font-weight:bold">Paul:</span> What about your email server, firewalls, monitoring software, and so on. What is that? FLOSS or proprietary?

<span style="color:#55f;font-weight:bold">Dan:</span> Mostly FLOSS, but one exception, I must admit, is our email hosting. We do not have the resources to safely run our own email. A few years ago we selected a provider that was a partner with a FOSS project to run our own email service, but we ultimately switched to Google because that provider was slow to implement two-factor authentication.

<span style="color:#f55;font-weight:bold">Paul:</span> Getting back to North Korea's human rights violations, <a href="https://www.washingtonpost.com/world/asia_pacific/where-are-the-bodies-buried-in-north-korea-investigators-try-to-prepare-for-future-trials/2017/07/18/48349113-7976-463a-a569-466ad84657c0_story.html">you are mapping burial sites and scenes of mass killings</a>, and so on, is that right? How bad is it?

<span style="color:#55f;font-weight:bold">Dan:</span> The human right situation in North Korea is very disturbing and the sad thing is it's continued for 60+ years. The UN's <a href="http://www.ohchr.org/EN/HRBodies/HRC/CoIDPRK/Pages/ReportoftheCommissionofInquiryDPRK.aspx">Report of the Commission of Inquiry on human rights in the Democratic People’s Republic of Korea from 2014</a> is a must-read on the general human rights situation in North Korea. From the principal findings section (para. 24), "The commission finds that systematic, widespread and gross human rights violations have been and are being committed by the Democratic People’s Republic of Korea. In many instances, the violations found entailed crimes against humanity based on State policies."

Their mandate looked at "violations of the right to food, the full range of violations associated with prison camps, torture and inhuman treatment, arbitrary arrest and detention, discrimination; in particular, in the systemic denial and violation of basic human rights and fundamental freedoms, violations of the freedom of expression, violations of the right to life, ... enforced disappearances, including in the form of abductions of nationals of other States," and so on.

For our mapping project, we published our first report last year, based on interviews with 375 escapees from North Korea who have now settled in South Korea.

They collectively told us the coordinates of 333 killing sites, usually the sites of public executions, which local residents, including school children, are encouraged and sometimes forced to watch. It should be noted that this number hasn't been consolidated to eliminate duplicates. Some people reported more than one site, others none at all, but on average, almost one site per person was reported.

<span style="color:#f55;font-weight:bold">Paul:</span> And how do you feel about the situation? I am guessing you have met North Korean refugees passing through your workplace and that you, like most of us, come from a very sheltered and even cushy Western society background. How do you feel when faced with such misery?

<figure style="float:left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/killingsitesperprovince.png"><img src="/sites/dot.kde.org/files/killingsitesperprovince.png" /></a><br /><figcaption>Suspected killing sites per province.</figcaption></figure> 

<span style="color:#55f;font-weight:bold">Dan:</span> It's a good question and hard to put into words what I feel. I guess, more than anything, I find the North Korean regime so unfair. Those we met in Seoul have been through so much, but they also are the ones who overcame so many obstacles and now have landed on their feet somewhere. It's not easy for them, but usually the longer they're here, the better they end up doing. 

Continuing about the mapping project's first report findings, from those 375 interviewees, we were also told the coordinates of 47 "body sites" - we use the term "body sites" because it's more general than burial sites. Most of the sites were burial sites, but some were cremation sites or places where bodies had been dumped without being buried, or stored temporarily before being buried. This 47 figure IS consolidated / de-duped (from 52), unlike the killing sites number.

<span style="color:#f55;font-weight:bold">Paul:</span> You manually plot sites on maps, correct? You have to rely on witnesses remembering where they saw things happen...

<span style="color:#55f;font-weight:bold">Dan:</span> We manually plot them using Google Earth, yes. During the interview, our interviewer (who himself is originally from the North) looks together with the interviewee at Google Earth's satellite imagery. You have to get used to looking down at the world, which takes some getting used to for some people.

<span style="color:#f55;font-weight:bold">Paul:</span> Is there no technology that would help map these things? Some sort of... I don't know... thermal imaging from satellites?

<span style="color:#55f;font-weight:bold">Dan:</span> Our goal eventually would be to interview all 30,000-plus North Koreans who've resettled in South Korea. The more we interview, and the more data points we get, the more we can cross-reference testimonies and hopefully get a better picture of what happened at these locations. I went to the big <a href="http://2017.foss4g.org/">FOSS4G</a> (G=Geospatial) conference last year in Boston and also the Korean FOSS4G in Seoul, and got to meet people developing mapping systems on drones. The only problem right now with drones is that flying them over North Korea will probably be seen as an act of war.

When we get enough data points, we could use machine learning to help identify more potential burial sites across all of North Korea. <a href="https://arstechnica.com/information-technology/2017/04/hunting-for-mexicos-mass-graves-with-machine-learning/">Something similar is being done in Mexico</a>, for instance, where they predict burial sites of the victims of the drug wars.

<span style="color:#f55;font-weight:bold">Paul:</span> Interesting.

<span style="color:#55f;font-weight:bold">Dan:</span> Patrick Ball of the <a href="https://hrdag.org/">Human Rights Data Analysis Group</a> is doing very, very good stuff.

<span style="color:#f55;font-weight:bold">Paul:</span> You mentioned that the crimes have been going on for 60 years now. What should other countries be doing to help stop the atrocities? Because it seems to me that, whatever they have been doing, hasn't worked that well...

<span style="color:#55f;font-weight:bold">Dan:</span> Very true, that. North Korea is very good at playing divide and conquer. The rivalry between the Soviets and the Chinese, for instance, allowed them to extract more aid or resources from them. 

They also try to negotiate one-on-one, they don't want to sit down to negotiate with the US and South Korea at the same time, only with one or the other, for instance. North Korea - South Korea and North Korea - US meetings are dramatically being planned right now, and it puts a lot of stress on the alliance between the US and South Korea. That's definitely a goal of North Korea's leadership. Again, divide and conquer.

So one thing that's an absolute must is for South Korea to work very closely with other countries and for them to all hold to the same line. But there are domestic and external forces that are pulling all of the countries in other directions, of course.

I would say to any government to always keep human rights on the agenda. This does raise the bar for negotiations, but it also indicates what's important. It also sends an important message to the people of North Korea, whom we’re trying to help. 

I also think strategies that increase the flow of information into, out of, and within North Korea are key.  For instance, the BBC recently opened a Korea-language service for the whole peninsula including North Korea. And Google’s <a href="https://en.wikipedia.org/wiki/Project_Loon">Project Loon</a> and Facebook’s similar project with drones could theoretically bring the internet to millions.

<span style="color:#f55;font-weight:bold">Paul:</span> Do you think these much-trumpeted US - North Korean negotiations will happen? And if so, anything productive will come from them?

<span style="color:#55f;font-weight:bold">Dan:</span> I really don't know. Also, one can't talk about all this without mentioning that China is North Korea's enabler, so if you want to significantly change North Korea, you have to influence China. 

To more directly answer the question, two US presidents (one from each party) made big deals with the North Koreans but the deals fell apart.  We’ll see.

<span style="color:#f55;font-weight:bold">Paul:</span> We've covered what governments can do, but what can private citizens do to help?

<span style="color:#55f;font-weight:bold">Dan:</span> One major thing is to help amplify the voices of North Korean refugees and defectors. There are a few groups in Seoul, for instance, that connect English speakers with North Korean defectors who want to learn and practice their English. There are small North Korean defector communities in cities like London, Washington DC, etc. I don't know about Berlin, but I wouldn't be surprised!

That's at the individual-to-individual level, but also, those with expertise as software developers, could use their skills to empower North Korean  refugee organizations and activists, as well as other North Korean human rights groups.

<span style="color:#f55;font-weight:bold">Paul:</span> Empower how? Give me a specific thing they can do.

<span style="color:#55f;font-weight:bold">Dan:</span> For instance, one time I invited an activist to the Korea KDE group. He and some KDE community leaders had a very interesting discussion about how to use Arduino or something similar to control a helium-filled balloon to better drop leaflets, USB sticks, etc. over North Korea.

<span style="color:#f55;font-weight:bold">Paul:</span> That is a thing? What do the Arduinos do, control some sort of rotor?

<span style="color:#55f;font-weight:bold">Dan:</span> I can't really get into specifics, but, speaking of USB sticks with foreign media and content on it, <a href="https://flashdrivesforfreedom.org">one group has a project to reuse your old USB sticks and SD cards for just that purpose</a>.

<span style="color:#f55;font-weight:bold">Paul:</span> What do you put on the sticks and cards? "The Interview"? "Team America"?

<span style="color:#55f;font-weight:bold">Dan:</span> There are several groups doing this, which is good, since they all probably have different ideas of what North Koreans want to watch. I think South Korean TV shows, movies, and K-Pop are staples. I have heard Wikipedia also goes on to some sticks, as do interviews with North Koreans resettled in South Korea...

<span style="color:#f55;font-weight:bold">Paul:</span> Dan, thank you so much for your time.

<span style="color:#55f;font-weight:bold">Dan:</span> Thanks so much, Paul, I look forward to meeting you and the rest of the KDE gang this summer.

<span style="color:#f55;font-weight:bold">Paul:</span> I too look forward to seeing you in Vienna.

<i>Dan will be delivering the opening keynote at <https://akademy.kde.org/2018>Akademy 2018</a> on the 11th of August. Come to Akademy and find out live how you too can fight injustice from the realms of Free Software.

<h2>About Akademy</h2>

For most of the year, KDE—one of the largest free and open software communities in the world—works on-line by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2018">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.

You can join us by registering for Akademy 2018. Registrations open in April. <a href="https://events.kde.org/">Please watch this space</a>.</p>

For more information, please contact the <a href="https://akademy.kde.org/2018/contact">Akademy Team</a>.

<!--break-->