---
title: "KDE Applications 18.12 Are Waiting for You"
date:    2018-12-13
authors:
  - "skadinna"
slug:    kde-applications-1812-are-waiting-for-you
comments:
  - subject: "emojis in konsole"
    date: 2018-12-18
    body: "<p>Can you show the link to the file cat for emojis? I want to try this but I don't know which file you have been using above.</p>"
    author: "shevy441"
  - subject: "\u041d\u0430\u0448\u0430 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f \u043f\u0440\u0435\u0434\u043e\u0441\u0442\u0430\u0432\u043b\u044f\u0435\u0442"
    date: 2021-01-09
    body: " \r\n\u041d\u0430\u0448\u0430 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f \u043f\u0440\u0435\u0434\u043e\u0441\u0442\u0430\u0432\u043b\u044f\u0435\u0442 <a href=https://gendiam.ru/prices/p_drill/>\u0443\u0441\u043b\u0443\u0433\u0438 \u0430\u043b\u043c\u0430\u0437\u043d\u043e\u0435 \u0431\u0443\u0440\u0435\u043d\u0438\u0435</a> \u0430\u043b\u043c\u0430\u0437\u043d\u0430\u044f \u0440\u0435\u0437\u043a\u0430 \u0438 \u0448\u0442\u0440\u043e\u0431\u043b\u0435\u043d\u0438\u0435."
    author: "gendiamlync"
  - subject: "\u041d\u0430\u0448\u0430 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f \u043f\u0440\u0435\u0434\u043e\u0441\u0442\u0430\u0432\u043b\u044f\u0435\u0442"
    date: 2021-01-09
    body: " \r\n\u041d\u0430\u0448\u0430 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f \u043f\u0440\u0435\u0434\u043e\u0441\u0442\u0430\u0432\u043b\u044f\u0435\u0442 <a href=https://gendiam.ru/prices/p_drill/>\u0443\u0441\u043b\u0443\u0433\u0438 \u0430\u043b\u043c\u0430\u0437\u043d\u043e\u0435 \u0431\u0443\u0440\u0435\u043d\u0438\u0435</a> \u0430\u043b\u043c\u0430\u0437\u043d\u0430\u044f \u0440\u0435\u0437\u043a\u0430 \u0438 \u0448\u0442\u0440\u043e\u0431\u043b\u0435\u043d\u0438\u0435."
    author: "gendiamlync"
  - subject: "\u0410\u043b\u043c\u0430\u0437\u043d\u043e\u0435 \u0431\u0443\u0440\u0435\u043d\u0438\u0435 \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u0439"
    date: 2021-02-10
    body: " \r\n<a href=https://gendiam.ru/service/s_diam-drill/>\u0410\u043b\u043c\u0430\u0437\u043d\u043e\u0435 \u0431\u0443\u0440\u0435\u043d\u0438\u0435 \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u0439</a> \u043f\u0440\u0438\u043c\u0435\u043d\u044f\u044e\u0442 \u043f\u0440\u0438 \u043f\u0440\u043e\u043a\u043b\u0430\u0434\u043a\u0435 \u0438\u043d\u0436\u0435\u043d\u0435\u0440\u043d\u044b\u0445 \u0441\u0438\u0441\u0442\u0435\u043c \u0432 \u0436\u0438\u043b\u044b\u0445 \u0434\u043e\u043c\u0430\u0445 \u0432 \u0437\u0434\u0430\u043d\u0438\u044f\u0445 \u0438 \u043f\u0440\u043e\u0447\u0438\u0445 \u0441\u043e\u043e\u0440\u0443\u0436\u0435\u043d\u0438\u044f\u0445"
    author: "gendiamlync"
  - subject: "\u0410\u043b\u043c\u0430\u0437\u043d\u043e\u0435 \u0431\u0443\u0440\u0435\u043d\u0438\u0435 \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u0439"
    date: 2021-02-10
    body: " \r\n<a href=https://gendiam.ru/service/s_diam-drill/>\u0410\u043b\u043c\u0430\u0437\u043d\u043e\u0435 \u0431\u0443\u0440\u0435\u043d\u0438\u0435 \u043e\u0442\u0432\u0435\u0440\u0441\u0442\u0438\u0439</a> \u043f\u0440\u0438\u043c\u0435\u043d\u044f\u044e\u0442 \u043f\u0440\u0438 \u043f\u0440\u043e\u043a\u043b\u0430\u0434\u043a\u0435 \u0438\u043d\u0436\u0435\u043d\u0435\u0440\u043d\u044b\u0445 \u0441\u0438\u0441\u0442\u0435\u043c \u0432 \u0436\u0438\u043b\u044b\u0445 \u0434\u043e\u043c\u0430\u0445 \u0432 \u0437\u0434\u0430\u043d\u0438\u044f\u0445 \u0438 \u043f\u0440\u043e\u0447\u0438\u0445 \u0441\u043e\u043e\u0440\u0443\u0436\u0435\u043d\u0438\u044f\u0445"
    author: "gendiamlync"
---
It's that time of the year again. Everyone is in a festive mood and excited about all the new things they're going to get. It's only natural, since it's the season of the last KDE Applications release for this year!

With more than 140 issues resolved and dozens of feature improvements, KDE Applications 18.12 are now on its way to your operating system of choice. We've highlighted some changes you can look forward to.

<h2>Practical File Management with Dolphin</h2>

File management encompasses a lot of activities. There's renaming, copying, and moving files around. Perhaps you want to quickly preview a file to make sure it's the right one. You're in luck, because the thumbnail preview experience has been greatly improved in the new version of <a href="https://www.kde.org/applications/system/dolphin/">Dolphin</a>. LibreOffice documents and AppImage applications can now be previewed as thumbnails, and icon thumbnails look much cleaner. If folder thumbnails are enabled, video files larger than 5 MB will be visible in them. 

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/app1802_dolphin01.png" alt="" width="800" />

Of course, there is more to Dolphin than just thumbnails. The "Control" menu makes it easier to show hidden places and create new files and folders. After unmounting a storage volume in the Places panel, it can now be remounted. Those who still own audio CDs and use Dolphin to open them will be glad to hear it can now change the CBR bitrate for MP3 files and fix timestamps for FLAC files.

Some security measures have been implemented in Dolphin to prevent users from accidentally losing their data. It no longer allows attempts to unmount the active home directory and the disk where the active OS is installed. When renaming files, Dolphin will warn you if there's an extra dot in front of the filename, which would make the file hidden. Pretty neat, right?

<h2>Okular: Annotate ALL the Things</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/app1802_okular01.png"><img src="/sites/dot.kde.org/files/app1802_okular01.png" /></a><figcaption>Okular with the new Typewriter tool</figcaption></figure>

<a href="https://www.kde.org/applications/graphics/okular/">Okular</a> has steadily grown from a document viewer into an indispensable assistant in activities such as studying, doing research, or collaborating on text in read-only file formats like PDF and EPUB. Its annotation capabilities were already powerful, but the new version introduces a new tool called Typewriter. With this annotation tool, you'll be able to write text literally anywhere in your files. Whether it's commenting on an image or highlighting a spelling mistake, your hands are now untied, and you can freely express yourself in Okular.

Other improvements in this release include better options to expand and collapse entries in the Table of Contents sidebar. If a file contains links, hovering over them will always display the full URL in a tooltip, regardless of the currently selected Okular mode. 

<h2>Konsole, Now with More Emotion</h2>

Spending hours or even days working in the terminal can get monotonous. Cheer up - the new version of <a href="https://www.kde.org/applications/system/konsole/">Konsole</a> has full support for emoji! Add a cheeky smiling cat to your commit messages, or insert a facepalm emoji into your shell scripts. 

If you're into more serious things, Konsole now makes it easier to reset the font size back to the default. When a bell is triggered in an inactive tab, the tab icon will be highlighted to visually alert you of the activity. Last but not least, if your mouse has back and forward buttons, Konsole is now able to recognize them, and you can use them to switch between tabs. 

<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/app1802_konsole.png" alt="" width="800" />

<h2>Usability Improvements for Everyone</h2>

If you have been keeping up with KDE-related news, you're probably aware of our community-wide Usability Improvement goal. After all, it's hard to miss <a href="https://pointieststick.wordpress.com/">the weekly updates</a> from our awesome developers who are dedicated to making the KDE software more accessible and friendlier to everyone. 

The KDE Applications 18.12 release integrates all those fruits of labor, and the result is a much more pleasant user experience across the board. <a href="https://www.kde.org/applications/internet/kmail/">KMail</a> now supports a unified inbox display, and emails should now be readable regardless of your color scheme. A small improvement with a big impact is the ability to repeat the last calculation in <a href="https://www.kde.org/applications/utilities/kcalc/">KCalc</a> multiple times. 

<a href="https://www.kde.org/applications/utilities/kate/">Kate</a> comes with new defaults that are meant to help you work more productively right from the start. Specifically, line numbers and the Text Filter plugin will be enabled by default. You can now change the focus of the embedded terminal in Kate by pressing the F4 key, and it will automatically synchronize the location in the terminal with the location of the currently active document.

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/app1802_kate01.png"><img src="/sites/dot.kde.org/files/app1802_kate01.png" /></a><figcaption>in 18.12 Kate comes with better defaults</figcaption></figure>

If you are using <a href="https://www.kde.org/applications/graphics/gwenview/">Gwenview</a> to fix the wretched red-eye effect in your photos, it will now be even easier thanks to the improved Reduce Red Eye tool. When taking screenshots with <a href="https://www.kde.org/applications/graphics/spectacle/">Spectacle</a>, their filenames will be sequentially numbered by default. You will also notice that saving options in Spectacle are now easier to access from the Save page. 

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/app1802_spectacle01.png"><img src="/sites/dot.kde.org/files/app1802_spectacle01.png" /></a><figcaption>New Spectacle makes it easier to save screenshots</figcaption></figure>

<a href="https://www.kde.org/applications/utilities/ark/">Ark</a> has received support for tar.zst archives, and it's now much smarter when it comes to file previews. Instead of previewing document files as archives, Ark will now launch the appropriate application for the selected file format. 

Apart from improving the standard set of applications, we have also made some of our specialized tools more usable. <a href="https://www.kde.org/applications/development/lokalize/">Lokalize, the translation and localization tool</a>, now has a better search functionality that can recognize plural forms of words. If you keep a lot of tabs open in Lokalize, you will be able to navigate between them much faster. 

<a href="https://www.kde.org/applications/education/cantor/">Cantor, the advanced mathematical tool</a>, now offers better visualizations and highlighting of command entries. You can also open multiple files in one Cantor shell. For users who need to draw mathematical functions, we have made <a href="https://www.kde.org/applications/education/kmplot/">Kmplot</a> more stable and improved the SVG export functionality.

<hr>

As always, check out the <a href="https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0">full list of changes in KDE Applications 18.12</a> to find out more.

Our work on KDE Applications continues, and we can't wait to show you what we've created in 2019. Until then, enjoy the Applications 18.12., and let us know which changes made you the happiest!
<!--break-->