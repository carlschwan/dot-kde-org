---
title: "Akademy 2019 Call for Hosts"
date:    2018-05-15
authors:
  - "nightrose"
slug:    akademy-2019-call-hosts
---
The organization of this year's Akademy is in full swing: <a href="https://dot.kde.org/2018/05/03/akademy-2018-program-now-available">the official conference program</a> is out, we have had <a href="https://dot.kde.org/2018/03/21/dan-bielefeld-keynote-speaker-akademy-2018-exposing-injustice-through-use-technology">an insightful interview</a> with one of the keynote speakers, another is coming soon, and attendees are already <a href="https://akademy.kde.org/2018/accommodation">booking flights and accommodation</a>. The #akademy IRC channel on Freenode and the Telegram group are buzzing with messages, advice and recommendations.

That said, it's not too early to start planning for Akademy 2019! 

In fact, we are now opening the Akademy 2019 Call for Hosts, and looking for a vibrant spot and an enthusiastic crew that will host us.

Would you like to bring Akademy, the biggest KDE event, to your country? Read on to find out how to apply!

<br clear="all" />
<div style='text-align: center'>
<figure style="padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/akademy-malaga.jpg"><img src="/sites/dot.kde.org/files/akademy-malaga.jpg" width="600" height="auto" /></a><br /><figcaption>In 2005, Akademy took place in beautiful Málaga, Spain. Photo by Paolo Trabbatoni.</figcaption></figure>
</div>

<h2>A Bit About Akademy</h2>

<figure style="float: right; padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/akademy-2014.jpg"><img src="/sites/dot.kde.org/files/akademy-2014.jpg" width="400" height="300" /></a><br /><figcaption>The venue of Akademy 2014 in Brno, Czech Republic. <br />Photo by Kevin Funk.</figcaption></figure>


Akademy is KDE's annual get-together where our creativity, productivity and community-bonding reach their peak. Developers, users, translators, students, artists, writers - pretty much anyone who has been involved with KDE - will join Akademy to participate and learn. Contents will range from keynote speeches and two days of dual track talks by the FOSS community, to workshops and Birds of a Feather (BoF) sessions where we plot the future of the project.

The first day serves as a welcoming event. The next two days cover the keynote speakers and other talks. The remaining days are used for BoF sessions, intensive coding and workshops for smaller groups of 10 to 30 people. One of the workshop days is reserved for a day trip, so the attendees can see the local tourist attractions.

<h2>What You Get as a Host</h2>

Hosting Akademy is a great way to contribute to a movement of global collaboration. You get a chance to host one of the world's largest FOSS communities with contributors from across the globe, and witness a wonderful week of intercultural collaboration in your home town.

You'll get significant exposure to the Free Software community, and develop an understanding of how large projects operate. It is a great opportunity for the local university students, professors, technology enthusiasts and professionals to try their hand at something new.

<h2>What We Need from a Host</h2>

<figure style="float: right; padding: 1ex; margin: 1ex;"><a href="/sites/dot.kde.org/files/akademy-2008-group-photo.jpg"><img src="/sites/dot.kde.org/files/akademy-2008-group-photo.jpg" width="400" height="300" /></a><br /><figcaption>Ten years ago we gathered in Sint-Katelijne-Waver, <br />Belgium for this cool group photo.</figcaption></figure>

Akademy requires a location close to an international airport, with an appropriate conference venue that is easy to reach. Organizing Akademy is a demanding task, but you’ll be guided along the entire process by people who’ve been doing it for years. Nevertheless, the local team should be prepared to invest a considerable amount of time into organizing Akademy.

For detailed information, please see the <a href="https://ev.kde.org/akademy/CallforHosts_2019.pdf">Call for Hosts</a>. Questions and applications should be addressed to the <a href="mailto:kde-ev-board@kde.org">Board of KDE e.V.</a> or the <a href="mailto:akademy-team@kde.org">Akademy Team</a>. 

<!--<strong>Please indicate your interest in hosting Akademy to the Board of KDE e.V. by June 15st. 
Full applications will be accepted until 15th July.</strong> -->

We look forward to your ideas, and can't wait to have fun at Akademy 2019 in your city!
<!--break-->