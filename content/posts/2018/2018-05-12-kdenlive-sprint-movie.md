---
title: "Kdenlive Sprint - The Movie"
date:    2018-05-12
authors:
  - "Paul Brown"
slug:    kdenlive-sprint-movie
---
<img style="" src="/sites/dot.kde.org/files/IMG_8152-1080-1080x675.jpg">

<a href="https://kdenlive.org/">Kdenlive is KDE's advanced video-editor</a>. This April, members of the Kdenlive project met up for five days - from 25th to the 29th - for their spring sprint. The developers Jean-Baptiste Mardelle and Nicolas Carion, along with professional community videomakers Farid Abdelnour, Rémi Duquenne and Massimo Stella, got together at the Carrefour Numérique in Paris to push the project forward.

This is what happened...

<h2>The Plot</h2>

Despite a very busy agenda, which included pitching Kdenlive to the general public, the attendees managed to work some new features into the code. For example, the next version of Kdenlive that hits your distro will include a feature that will automatically split video and audio tracks by default into separate tracks. This saves time, since the workflows for editing video and audio are substantially different, and editors often have to separate tracks to work on them individually anyway.

<p>
<video width="800" controls="true">
<source src="https://kdenlive.org/wp-content/uploads/2018/05/drop.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
</p>

The toolbar that overlays monitors got a makeover and now supports multiple layout guides. The toolbar is translucent, so you can still see what is going on in the clip, and only appears when you move the mouse to the upper right corner of the monitor. This not only looks cool (very important!), but also makes it practical, since it is invisible most of the time, not blocking your view of the clip.

<p>
<video width="800" controls="true">
<source src="https://kdenlive.org/wp-content/uploads/2018/05/overlay.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>
</p>

Apart from coding in new features, the team held two public sessions. First they talked with potential contributors. This had an immediate effect, as Camille took it on himself to update the project's wiki, and Elie submitted a patch which added the possibility to manage and download keyboard shortcut templates of other video editors such as Avid, Final Cut and Adobe Premiere Pro. This means an editor used to working with closed-source alternatives will immediately feel at home with Kdenlive.

The second public event was with video-editing enthusiasts. The audience had the opportunity to see Kdenlive in action and find out more about the software, as well as talk with the developers.

<h2>Coming to a Theatre Near You</h2>

Apart from the incremental improvements that have already made their way into the beta versions of Kdenlive's next release, more exciting features are on the way. During the the sprint, the developers agreed on a roadmap of where they want to take Kdenlive next, and made a priority of incorporating Advanced Trimming and Single Track Transitions in the upcoming releases. 

<a href="https://kdenlive.org/en/video-editing-applications-handbook/#trim">Advanced trimming allows you to roll, ripple, slip or slide a clip between two existing ones</a>. This lets you drop a clip onto a track and have the surrounding clips behave in different ways, cropping or displacing frames automatically according to what you want to do. With Single Track Transitions, on the other hand, you can overlap one clip onto another on the same track and apply a transition between the two, instead of having to figure out transitions across several tracks.

More longer term goals include Multicam Editing. This comes in handy when you have filmed the same event from different angles with more than one camera. Kdenlive will help you sync up the action so you can cut from one to the other seamlessly. Another goal is to support faster renders, splitting the workload between the multiple cores most modern computers come with, as well as sending heavy workloads off to the GPU.

<figure style="padding: 1em; border: 1px solid grey"><a href="/sites/dot.kde.org/files/multitrack%20editing_0.jpg"><img src="/sites/dot.kde.org/files/multitrack%20editing_0.jpg" /></a><br /><figcaption>This is what a multicam workflow may look like in Kdenlive.</figcaption></figure> 

One final thing to look forward to is the integration of Kdenlive with other Free Software video- and audio-editing tools. The developers are looking at Blender, Natron and Ardour, as well as graphics-editing tools like GIMP, Krita and Inkscape. The plan is to incorporate their special and specific features into Kdenlive and make sure all these tools can work seamlessly together. This would mean, for example, that you could create a 3D text effect in Blender and bridge it into Kdenlive without having to go through time-consuming exports and imports. Or you could edit a sequence in Kdenlive and frameserve it to do the compositing in Natron. 

<h2>End Credits</h2>

As with many Free Software projects, the Kdenlive team can always use more contributors. New developers can help get features incorporated sooner and bugs squashed more efficiently. Documenters and translators can help make the guides, manuals and websites more accessible to a larger audience. Join the <a href="https://mail.kde.org/mailman/listinfo/kdenlive">mailing list</a>, <a href="https://t.me/kdenlive">Telegram group</a>, or drop by the #kdenlive channel on Freenode to find out how you, too, can help.

You can also support Kdenlive by supporting KDE: <a href="https://www.kde.org/donations">donate and help make more sprints like this one possible</a>.

Kdenlive is already a very capable video-editor, but the work the team is carrying out promises to make it a world-class tool that both aficionados and professionals can use. The latest version of Kdenlive is available in many distributions, as well as in AppImage and Flatpak formats. Vincent Pinon is also working on the Windows port which is currently in a Beta stage.

Head over to <a href="https://kdenlive.org/en/download/">Kdenlive's download page</a> and get editing!

<i><a href="https://kdenlive.org/en/2018/05/kdenlive-paris-sprint-lgm-report/">You can read more about the Kdenlive's sprint here.</a></i>
<!--break-->