---
title: "KDE receives 200,000 USD-donation from the Pineapple Fund"
date:    2018-02-19
authors:
  - "Paul Brown"
slug:    kde-receives-200000-usd-donation-pineapple-fund
comments:
  - subject: "Thanks a lot!"
    date: 2018-02-19
    body: "<p>Great news for KDE community and Free software!</p><p>That fund will help to improve, create and spread KDE.</p><p>Thanks a lot!!</p><p>PS: I made a spanish translation in my blog: https://victorhckinthefreeworld.com/2018/02/19/kde-recibe-una-donacion-de-200-000-dolares/</p>"
    author: "victorhck"
  - subject: "Amazing!"
    date: 2018-02-19
    body: "<p>\\o/</p>"
    author: "Jan"
  - subject: "Great news!"
    date: 2018-02-19
    body: "<p>Congratulations to all the developers who are building the most awesome Free Software project.</p>"
    author: "Baltasar Ortega"
  - subject: "Great news for KDE Community and KDE Project"
    date: 2018-02-21
    body: "<p>It made me very happy see this new at Twitter, i really hope this can help to improve even more the projects of KDE, their development, make possible new projects, new collaborators, etc. It was a great new and it's a reason to make happy, KDE Community. As always, KDE Rocks!!</p><p>Greetings.</p>"
    author: "Alberto D\u00edaz"
  - subject: "Congrats KDE! You deserve it."
    date: 2018-02-21
    body: "<p>As an old timer KDE dev now doing Bitcoin (BCH) full time, I just want to say I'm very happy to see KDE being selected for this donation.&nbsp; I have been using KDE for my main desktop for close to 20 years, it is rock solid and amazing.&nbsp;</p><p>&nbsp;Keep on making computing great, people.</p>"
    author: "TomZander"
  - subject: "great news"
    date: 2018-02-24
    body: "<p>Congratulations! That is a tremendous news! Go KDE, go!</p>"
    author: "MichaelSD"
  - subject: "Congratulations!"
    date: 2018-02-24
    body: "<p>Congratulations!</p><p>I wonder : The Django community has used funds to pay one FTE for fixing bugs and smoothing releases. It works really well for them. How would you feel about such usage of money?</p>"
    author: "Diederik van der Boor"
  - subject: "Great news!"
    date: 2018-03-07
    body: "<p>Encouragements like this shall help to sustain and expand such great software.</p>"
    author: "Rajesh Kumar Mallah"
---
<p><figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/pineapple-1376683_K.jpg"><img src="/sites/dot.kde.org/files/pineapple-1376683_K.jpg" alt="" width="300" /></a></figure></p>

<strong>KDE e.V. is announcing today it has received a donation of 200,000 USD from the <a href="http://pineapplefund.org/">Pineapple Fund</a>.</strong>

With this donation, the Pineapple Fund recognizes that KDE as a community creates software which benefits the general public, advances the use of Free Software on all kinds of platforms, and protects users' privacy by putting first-class and easy to use tools in the hands of the people at zero cost. KDE joins a <a href="http://pineapplefund.org/#charities">long list of prestigious charities, organizations and communities</a> that the Pineapple Fund has so generously donated to.

"KDE is immensely grateful for this donation. We would like to express our deeply felt appreciation towards the Pineapple Fund for their generosity" said Lydia Pintscher, President of KDE e.V.. "We will use the funds to further our cause to make Free Software accessible to everyone and on all platforms. The money will help us realize our vision of creating a world in which everyone has control over their digital life and enjoys freedom and privacy".
	
"KDE is a vibrant community that has been developing a number of awesome products like Plasma that empower the user's freedom." said a spokesperson for the Pineapple Fund. "I especially admire the UX and design of KDE's products, as they are approachable to new audiences who are not Linux geeks. I hope this donation can power further KDE development!".

This donation will allow KDE to organize events that bring the community together; sponsor development sprints to improve the usability and performance of existing tools; pay expenses for contributors traveling from distant locations; attract more contributors and build a more inclusive community; create new and safer programs; and carry out research for future generations of KDE's environments and applications.
<!--break-->