---
title: "Promo Sprint Report: What We Did and How You Can Help Us"
date:    2018-05-25
authors:
  - "Paul Brown"
slug:    promo-sprint-report-what-we-did-and-how-you-can-help-us
---
<img style="" src="/sites/dot.kde.org/files/2018-promo-sprint-group1_cropped.jpg">

February was a big month for the Promo team - we held a long-awaited sprint in Barcelona, Spain from the 16th to 18th. The aim of the sprint was to look at information we had collected over the prior years, interpret what it meant, and use it to discuss and plan for the future. The activities we came up with should help us accomplish our ultimate goal: increasing KDE's visibility and user base.

Nine members of the team made it to Barcelona: Aleix Pol, Ivana Isadora Devčić, Jure Repinc, Kenny, Łukasz Sawicki, Lydia Pintscher, Neofytos Kolokotronis, Paul Brown, and Rubén Gómez. We met at Espai 30, an old factory converted into a social center for the neighborhood. Coincidentally, that is one of the places where the <a href="http://guifi.net/">Guifi.net</a> project started -- rather fitting for a meeting that comprised Free Software and communication.

<h2>Day 1: Informal Afternoon Meeting</h2>

Although Friday was "arrival day" without an official agenda, we could not resist talking shop over pizza and beer. Discussions gravitated towards the KDE.org website, which will be migrated from an old and clunky backend to a Wordpress framework. The improvement to the framework got us thinking on how we could improve the content, too.

The consensus was that we want to inform the general public about what KDE is - not a desktop, but the community that creates, maintains, documents, translates, and promotes a large body of multi-purpose software. Our software collection does include a desktop environment, but it also offers utilities, games, productivity applications, media players and editors, an environment and applications for mobile phones, development frameworks, and much more.

We should also make sure the website caters equally to the tech savvy and unsavvy, since KDE's software is meant for everybody. The new site should clearly direct users to our products, allowing end users to simply download and use them. At the same time, the website should ease the way for potential contributors to join the community.

<h2>Day 2: Espai 30, Stats stats stats, and Improved Communication</h2>

At the break of dawn the next day... well, actually, at 10 o'clock, sprint sessions started in earnest. Ivana gave a recap of Promo's main activities over the last year or so, revisiting funding campaigns we promoted and communication tactics we implemented.

Next we looked at hard, cold data, collected from social media accounts, web statistics, and distro popcons (application popularity contests). The bad news is that visits to our main sites have gone down over the last year. The good news, however, is that followers and interactions on social media have seen a significant increase. Although data collected from popcons are partial, it also looks like Plasma's user base is growing steadily.

<style>
blockquote{
  display:block;
  background: #fff;
  padding: 15px 20px 15px 70px;
  margin: 0 0 20px;
  position: relative;
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 20px;
  line-height: 1;
  color: #666;
  text-align: left;
  
  /*Borders - (Optional)*/
  border-left: 15px solid #0068c6;
  border-right: 2px solid #0068c6;
  
  /*Box Shadow - (Optional)*/
  -moz-box-shadow: 2px 2px 15px #ccc;
  -webkit-box-shadow: 2px 2px 15px #ccc;
  box-shadow: 2px 2px 15px #ccc;
}

blockquote::before{
  content: "\00BB"; /*Unicode for Left Double Quote*/
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 60px;
  font-weight: bold;
  color: #999;
  
  /*Positioning*/
  position: absolute;
  left: 10px;
  top:0px;
}

blockquote::after{
  /*Reset to make sure*/
  content: "";
}

blockquote em{
  font-style: italic;
}
</style>

<blockquote>
Want to help us with data-collecting and processing, or have ideas about where we can collect more useful information? Send your suggestions to our <a href="mailto:kde-promo@kde.org">mailing list</a> and we'll look into it.
</blockquote>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey"><a href="/sites/dot.kde.org/files/2018-promo-sprint1_1500.jpg"><img src="/sites/dot.kde.org/files/2018-promo-sprint1_1500.jpg" width="300" /></a><br /><figcaption>Paul made the team look at bar charts<br /> for the better part of an hour.</figcaption></figure>

The data also helps us pinpoint wins and fails in our approach to communicating with the outside world. We found a direct relation between the traffic to our news site (dot.kde.org) and to the main kde.org website. Therefore it makes sense to seriously work on increasing the traffic to kde.org first, in order to improve the visibility and effectiveness of our announcements and campaigns. We also identified ways to make our social media posts more attractive, which should help them garner more re-tweets, boosts (the equivalent of re-tweets in Mastodon), shares and upvotes, and spread our messages further.

Another way of reaching more people is through events. We discussed <a href="https://akademy.kde.org/">Akademy</a> and our plans for promoting the <a href="https://akademy.kde.org/2018">2018 edition</a> before and during the event, so that news coming out of Vienna in August can reach as many people as possible.

We also talked about visiting other technical and even <a href="https://phabricator.kde.org/T8122">not-so-technical events</a>. By showcasing our applications and letting users play with them, we think we will be able to increase our user base. In any case, we need to be well-prepared for all types of conferences, so we made a list of essentials based on our previous experiences.

We noticed that even within the FLOSS community, there is a large portion of businesses, organizations and developers who are unaware of technologies that KDE develops. Speaking and setting up booths at technical, but non-KDE/Qt events (like the upcoming <a href="https://events.linuxfoundation.org/events/elc-openiot-europe-2018/">Embedded Linux Conference organized by the Linux Foundation</a>), could help solve this problem and even attract contributors for KDE.

<style>
blockquote{
  display:block;
  background: #fff;
  padding: 15px 20px 15px 70px;
  margin: 0 0 20px;
  position: relative;
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 20px;
  line-height: 1;
  color: #666;
  text-align: left;
  
  /*Borders - (Optional)*/
  border-left: 15px solid #0068c6;
  border-right: 2px solid #0068c6;
  
  /*Box Shadow - (Optional)*/
  -moz-box-shadow: 2px 2px 15px #ccc;
  -webkit-box-shadow: 2px 2px 15px #ccc;
  box-shadow: 2px 2px 15px #ccc;
}

blockquote::before{
  content: "\00BB"; /*Unicode for Left Double Quote*/
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 60px;
  font-weight: bold;
  color: #999;
  
  /*Positioning*/
  position: absolute;
  left: 10px;
  top:0px;
}

blockquote::after{
  /*Reset to make sure*/
  content: "";
}

blockquote em{
  font-style: italic;
}
</style>
<blockquote>
Do you have suggestions for events we should attend? Join the <a href="https://phabricator.kde.org/T8043">Attend External Events</a> task and tell us about them.
</blockquote>

This brought us to the discussion on how Promo can help with <a href="https://phabricator.kde.org/tag/goal_settings_2017/">the long-term community goals</a>, especially the goal of <a href="https://phabricator.kde.org/T7116">streamlining the onboarding of new contributors</a>. 

One of the things we have started doing, for example, is creating a list of simple tasks for beginners. We are also trying to identify where people struggle in the process of joining Promo, and working on eliminating obstacles. On a more one-to-one basis, we want to be able to identify people's skills so we can direct them to teams they can join. This was one of the topics we tackled during the last day of the sprint.

<h2>Day 3: Teams, Market Research, and Publicity Stunts</h2>

We already noticed there are wide variety of jobs for our team, and agreed it would be more efficient to <a href="https://phabricator.kde.org/T8258">classify them and assign them to smaller groups of people</a> with the best skills to carry them out.
	
For example, we'd like to have a smoother communication channel with developers, so that we can better understand their work and advise them on how to promote it. The best way to do this, we thought, would be to recruit developers already in the Promo group as liaisons with their colleagues.

Likewise, experienced YouTubers and videographers can create promotional videos for product releases; journalists and editors can write or help improve blog posts and news articles; and people with a background in marketing can use their knowledge to do some serious market research.

That last thing is important because the Promo team must discover what technologies people use, how they use them, and what they like and dislike about them to be able to market KDE products. We decided to take a step back and work on a market research project that will provide us with solid information on which to base our actions. 
<style>
blockquote{
  display:block;
  background: #fff;
  padding: 15px 20px 15px 70px;
  margin: 0 0 20px;
  position: relative;
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 20px;
  line-height: 1;
  color: #666;
  text-align: left;
  
  /*Borders - (Optional)*/
  border-left: 15px solid #0068c6;
  border-right: 2px solid #0068c6;
  
  /*Box Shadow - (Optional)*/
  -moz-box-shadow: 2px 2px 15px #ccc;
  -webkit-box-shadow: 2px 2px 15px #ccc;
  box-shadow: 2px 2px 15px #ccc;
}

blockquote::before{
  content: "\00BB"; /*Unicode for Left Double Quote*/
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 60px;
  font-weight: bold;
  color: #999;
  
  /*Positioning*/
  position: absolute;
  left: 10px;
  top:0px;
}

blockquote::after{
  /*Reset to make sure*/
  content: "";
}

blockquote em{
  font-style: italic;
}
</style>
<blockquote>
Got experience in marketing? <a href="https://phabricator.kde.org/T8554">Join the effort</a>! </blockquote>

At the same time, we can entice people to use Plasma and KDE applications with straightforward advertising, or rely on the more subtle art of product placement. Regarding the former, we looked at publicity stunts that had helped other community projects in the past, like full page ads in prominent newspapers, or messages on public transport. For example, ads at bus stops in university areas may help encourage students join the community.
<style>
blockquote{
  display:block;
  background: #fff;
  padding: 15px 20px 15px 70px;
  margin: 0 0 20px;
  position: relative;
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 20px;
  line-height: 1;
  color: #666;
  text-align: left;
  
  /*Borders - (Optional)*/
  border-left: 15px solid #0068c6;
  border-right: 2px solid #0068c6;
  
  /*Box Shadow - (Optional)*/
  -moz-box-shadow: 2px 2px 15px #ccc;
  -webkit-box-shadow: 2px 2px 15px #ccc;
  box-shadow: 2px 2px 15px #ccc;
}

blockquote::before{
  content: "\00BB"; /*Unicode for Left Double Quote*/
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 60px;
  font-weight: bold;
  color: #999;
  
  /*Positioning*/
  position: absolute;
  left: 10px;
  top:0px;
}

blockquote::after{
  /*Reset to make sure*/
  content: "";
}

blockquote em{
  font-style: italic;
}
</style>
<blockquote>
Got an idea for advertising campaign which is both effective and cheap to carry out? <a href="mailto:kde-promo@kde.org">Share it with us!</a>
 </blockquote>

As for the latter, it turns out that TV shows and movies sometimes have a hard time when they want to show a flashy computer or mobile device interface. Because they can be endlessly customized, Plasma, Plasma Mobile and the applications that run on them are perfect candidates for the likes of The Blacklist, CSI Cyber, Mission Impossible 7... Okay, maybe we will have to start more modest, but remember that KDE tech was already featured on Mr Robot, albeit as the choice of the villain.

We discussed other ways of indirectly increasing the popularity of KDE, including working with journalists, bloggers and vloggers from outside of our community. <a href="https://phabricator.kde.org/T7953">We started brainstorming a list of "influencers", journalists</a> and <a href="https://phabricator.kde.org/T7867">publications</a>. 
<style>
blockquote{
  display:block;
  background: #fff;
  padding: 15px 20px 15px 70px;
  margin: 0 0 20px;
  position: relative;
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 20px;
  line-height: 1;
  color: #666;
  text-align: left;
  
  /*Borders - (Optional)*/
  border-left: 15px solid #0068c6;
  border-right: 2px solid #0068c6;
  
  /*Box Shadow - (Optional)*/
  -moz-box-shadow: 2px 2px 15px #ccc;
  -webkit-box-shadow: 2px 2px 15px #ccc;
  box-shadow: 2px 2px 15px #ccc;
}

blockquote::before{
  content: "\00BB"; /*Unicode for Left Double Quote*/
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 60px;
  font-weight: bold;
  color: #999;
  
  /*Positioning*/
  position: absolute;
  left: 10px;
  top:0px;
}

blockquote::after{
  /*Reset to make sure*/
  content: "";
}

blockquote em{
  font-style: italic;
}
</style>
<blockquote>
Do you know somebody with a solid audience on the fringes of open sourcedom that could influence a large group of people?  Go and <a href="https://phabricator.kde.org/T7953">add them to the list</a>.
</blockquote>

We also want to improve our presence in businesses. To do that, we would first have to approach businesses and contractors that already work with KDE/Qt-based technologies. The idea is to get them on board and create a marketplace/support network that other companies can rely on when considering a migration to desktop Linux.

While brainstorming other ways to increase awareness, we realized we could improve videos and help them reach a wider audience by adding subtitles. If you would like to help creating subtitles in your language, sign up for the <a href="https://phabricator.kde.org/T8258">video group</a> and tell us what you can do.

<h2>Conclusion</h2>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey"><a href="/sites/dot.kde.org/files/2018-promo-sprint-tasks_crops.jpg"><img src="/sites/dot.kde.org/files/2018-promo-sprint-tasks_crops.jpg" width="300" /></a><br /><figcaption>So much stuff still needs to be done...</figcaption></figure>

This was an intense and intensive sprint. The full list of topics we discussed is longer than this report, but we managed to devote enough time to the most pressing issues. We came up with ideas for targets and ways to work towards them that will translate into real results. We are now progressively implementing tasks that will help us reach those targets, but we need your help. 

If you think you can help us achieve our goals, please join the Promo group. We have a <a href="https://mail.kde.org/mailman/listinfo/kde-promo">mailing list</a>, <a href="irc://chat.freenode.net/kde-promo">IRC channel</a>, and a <a href="https://t.me/joinchat/AEyx-0O8HKlHV7Cg7ZoSyA">Telegram group</a>. You can also take a look at our <a href="https://phabricator.kde.org/tag/kde_promo/">workboard</a> and leave your feedback on tasks that are in progress.

Developing KDE's software is super-important, but so is spreading the message that the software exists and that everybody, regardless of their level of computer-literacy, can and should use it. That is what the Promo team is all about, and we will keep practicing what we preach. 
<!--break-->