---
title: "5 Things to Look Forward to in Krita 4.0"
date:    2018-03-22
authors:
  - "Paul Brown"
slug:    5-things-look-forward-krita-40
---
<img style="" src="/sites/dot.kde.org/files/carrotandpepper.jpg">

<b>That <a href="https://krita.org/">Krita</a> has become one of the most popular applications for painting among digital artists is an understatement. The great thing is that, with every new version, Krita just gets better and better. The latest release is a perfect example of that. Check out what you can look forward to in the new 4.0 version:</b>

<h3>1. SVG for Vector Tools</h3>

<a href="https://krita.org/en/krita-4-0-release-notes/">Krita 4.0</a> will use SVG on vector layers by default, instead of the prior reliance on ODG. SVG is the most widely used open format for vector graphics out there. Used by "pure" vector design applications, SVG on Krita currently supports gradients and transparencies, with more effects coming soon.

Krita 4.0 also includes an improved set of tools for editing objects created on vector layers, letting you tweak the fill, the shape, and other features of your vector elements.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/text-editor-features.gif"><img src="/sites/dot.kde.org/files/text-editor-features.gif" /></a><br /><figcaption>The text tool is now more reliable and more usable.</figcaption></figure> 

<h3>2. New Text Tool</h3>

The usability of the text tool has been vastly improved. The tool has been re-written to be more reliable, and has a better base for future expansion. As it also follows the SVG standard (instead of the prior ODT), it is compatible with more design applications.

<h3>3. Python Scripting</h3>

Krita now comes with a brand new Python scripting engine. This engine lets you write snippets of code that create and manipulate images, add dockers, entries to the menu, and much more. To get you started, the creators have included a large amount of example scripts. In Krita's <I>Settings</I> dialog, you can enable or disable Python plugins. <a href="https://docs.krita.org/Introduction_to_Python_Scripting">Check out the manual and learn how to pythonize your Krita</a>.

Note that this is the first release to include scripting, so it is very much a work in progress at this stage. Be advised that some things will work, but, for those that don't... <a href="https://krita.org/en/get-involved/report-a-bug/">Please tell the team</a>!

<iframe width="560" height="315" src="https://www.youtube.com/embed/OXvHHJUyBDg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<h3>4. New Brushes</h3>

If there is one thing Krita is famous for, that is its wide variety of brushes. Krita 4.0 has a special surprise in that department: David Revoy, the creator of <a href="http://
peppercarrot.com/">Pepper and Carrot</a>, has added his own personal set of brushes to this version.

<h3>5. Colorize Mask Tool</h3>

The new <I>Colorize Mask Tool</I> allows you to quickly and easily fill areas of line-art images with color. How it works: you create the mask for your line-art image, and then paint a streak of color into each area. The feature will automatically and intelligently fill each region with the colors you painted in, saving you the trouble of having to paint everything by hand or using the "dumb" fill tool.

<iframe width="560" height="315" src="https://www.youtube.com/embed/fIOhd3xXv-A" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Take a look at the <a href="https://docs.krita.org/Colorize_Mask">online documentation to find out more about the Colorize Mask Tool</a>.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/waterpaint.gif"><img src="/sites/dot.kde.org/files/waterpaint.gif" /></a><br /><figcaption>You can create a watercolor effect using two masked brushes.</figcaption></figure> 

<h3>6. Masked Brushes</h3>

Masked brushes are created by combining two brushes with each other in different ways. Say you have a brush in the shape of a heart, and then a soft sponge brush. If you combine them using the <I>multiply</I> operation, you will get a mix of both - a completely new brush!

Check out the <a href="https://docs.krita.org/Masked_Brush">manual entry for Masked Brushes</a> to learn how this feature works. 

<h3>7. Performance Improvements</h3>

As for performance improvements, Krita now multi-threads the pixel brush engine. This means Krita is now smart enough to let each of your computer's cores calculate the dabs separately, and also have them work together. Use the performance settings to let Krita know how many cores it should use. These changes only affect the pixel brush engine for now, but the feature will later be expanded to other engines like the color smudge.

Also, all brushes now have an <I>Instant Preview threshold</I> property. This speeds up a lot of smaller brushes that didn't have any performance improvement features in prior versions. Instant Preview will automatically turn on when the size of a brush changes by a certain amount.

Both things combined make painting with Krita a more fluid and pleasurable experience.

<iframe width="560" height="315" src="https://www.youtube.com/embed/a-CY4hmkg_I" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<hr />

Okay, so that was 7 things. But the fact is that Krita has long since transcended its humble origins as a clone of other design applications, and has become the tool of choice for digital painters regardless of the platform they use.

To learn more about all the changes included in this version, <a href="https://krita.org/en/krita-4-0-release-notes/">visit the complete release notes for Krita 4.0</a> or watch the videos embedded above.

Want to help make Krita even better? <a href="https://krita.org/en/support-us/donations/">Donate to the project</a>!

<!--break-->