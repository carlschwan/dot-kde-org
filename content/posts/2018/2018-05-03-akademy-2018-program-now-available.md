---
title: "The Akademy 2018 program is now available"
date:    2018-05-03
authors:
  - "Paul Brown"
slug:    akademy-2018-program-now-available
---
<img style="" src="/sites/dot.kde.org/files/akademy-2009-group-photo_letterbox.jpg">

<b>Akademy 2018 organisers have published the program for the conference part of the event. This year the event will be held in Vienna, and talks will take place on the 11th and 12th of August.</b>

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:100px"><a href="/sites/dot.kde.org/files/Dan.jpg"><img src="/sites/dot.kde.org/files/Dan.jpg" /></a><br /><figcaption>Dan Bielefeld</figcaption></figure> 

On the 11th, the keynote speaker will be Dan Bielefeld, an activist working for a South Korean NGO, who will be talking about <a href="https://dot.kde.org/2018/03/21/dan-bielefeld-keynote-speaker-akademy-2018-exposing-injustice-through-use-technology">how they use technology to expose atrocities committed by the North Korean regime</a>.

On a slightly lighter note, Lydia Pintscher and Bhushan Shah will host <a href="https://conf.kde.org/en/Akademy2018/public/events/30">a panel discussion between the KDE Community and KDE Student programs team on how to recruit new contributors</a>; Ivana Isadora Devcic <a href="https://conf.kde.org/en/Akademy2018/public/events/69">will tell us of the five ways in which release notes can be made better</a>; Timothée Giet will try to answer the question of <a href="https://conf.kde.org/en/Akademy2018/public/events/16">how to improve the experience for all kinds of media creators coming to GNU/Linux and the Plasma Desktop</a>; and Lays Rodrigues will explain how <a href="https://conf.kde.org/en/Akademy2018/public/events/38">Atelier offers a solution to control many 3D printers easily</a>; among many other things.

<a href="/sites/dot.kde.org/files/akademy-2015-work-session_small.jpg"><img style="float: left; width:150px;padding:10px 10px 10px 0px" src="/sites/dot.kde.org/files/akademy-2015-work-session_small.jpg" /></a> 

On the 12th we'll delve deep into AI assistants on the desktop with Aditya Mehra's talk on his implementation of the <a href="https://conf.kde.org/en/Akademy2018/public/events/34">Mycroft AI</a>. 
A little later, Thomas Pfeiffer will be telling us about <a href="https://conf.kde.org/en/Akademy2018/public/events/43">how to carry out low-cost, low-effort user testing to improve KDE's products by using Guerilla Usability Testing</a>; Attila Szollosi will bring us up to date on the topic of Linux on mobile with his presentation on <a href="https://conf.kde.org/en/Akademy2018/public/events/67">postmarketOS</a>; and, talking of embedded, Volker Krause will present <a href="https://conf.kde.org/en/Akademy2018/public/events/23">the current state of KDE Frameworks packages for Yocto</a>.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:200px"><a href="/sites/dot.kde.org/files/playlists-view.png"><img src="/sites/dot.kde.org/files/playlists-view.png" /></a><br /><figcaption>VVAVE</figcaption></figure> 

Of course, there will be plenty of talks about apps, from <a href="https://conf.kde.org/en/Akademy2018/public/events/10">video-editing with Kdenlive</a>, to  <a href="https://conf.kde.org/en/Akademy2018/public/events/13">music collating with VVAVE</a>, and <a href="https://conf.kde.org/en/Akademy2018/public/events/6">image management with DigiKam</a>.

The <a href="https://akademy.kde.org/2018/program">rest of the week</a> will be taken up by BoF sessions, workshops and sprints, in which KDE community members will be working elbow-to-elbow and learning from each other, intent on building a better KDE for everybody.

<a href="https://events.kde.org/">Registration for Akademy 2018 is now open</a>, and don't forget to <a href="https://akademy.kde.org/2018/accommodation">book your accommodation</a> soon, as Vienna is a busy location. Besides, if you book by email and use our discount code, you will be able to get rooms close to other Akademy participants and truly enjoy this one-of-a-kind community event.

<img style="" src="/sites/dot.kde.org/files/ShootingBack_letterbox.JPG">

<h2>About Akademy</h2>

For most of the year, KDE—one of the largest free and open software communities in the world—works on-line by email, IRC, forums and mailing lists. <a href="https://akademy.kde.org/2018">Akademy</a> provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities. For more information, please contact The <a href="https://akademy.kde.org/2018/contact">Akademy Team</a>.
<!--break-->
