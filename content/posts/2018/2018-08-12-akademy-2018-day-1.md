---
title: "Akademy 2018 Day 1"
date:    2018-08-12
authors:
  - "devaja"
slug:    akademy-2018-day-1
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:300px"><a href="/sites/dot.kde.org/files/Lydia_small.jpg"><img src="/sites/dot.kde.org/files/Lydia_small.jpg" /></a><br /><figcaption>Lydia Pintscher, President of KDE e.V. opens this year's Akademy. Photo by Paul Brown, <a href="https://creativecommons.org/publicdomain/zero/1.0/">distributed under the CC0 license</a>.</figcaption></figure> 

Akademy 2018 got off to a wet start with rains accompanying all attendees pouring into Vienna for KDE's largest annual community conference. Although the Pre-Registration event was held on Day Zero (Friday the 10th) and it was a fun-filled affair, Akademy kicked off in earnest on Saturday, with talks, panels and demonstrations. Read on to find out about Day 1 of Akademy and all that transpired:

<h2>Keynote: <a href="https://conf.kde.org/en/Akademy2018/public/events/78">Mapping Crimes Against Humanity in North Korea with FOSS</a></h2>

<a href="https://dot.kde.org/2018/03/21/dan-bielefeld-keynote-speaker-akademy-2018-exposing-injustice-through-use-technology">Dan Bielefeld</a>, the Technical Director of the <a href="https://en.tjwg.org/">Transitional Justice Working Group</a>, explained the work they do to map North Korean locations of mass burial and execution sites using mapping technologies. He also delivered insight into how North Korea and the Kim regime operates, and how his organization gleans information both from interviews with refugees and from studying satellite imagery.

<figure style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;width:300px"><a href="/sites/dot.kde.org/files/Dan_small.jpg"><img src="/sites/dot.kde.org/files/Dan_small.jpg" /></a><br /><figcaption>Dan Bielefeld talks about how the Transitional Justice Working Group tries to shed light on North Korea's crimes against humanity.  Photo by Paul Brown, <a href="https://creativecommons.org/publicdomain/zero/1.0/">distributed under the CC0 license</a>.</figcaption></figure> 

Although the topic of the suffering of North Koreans is grim, there is a silver lining, says Dan: One day there will be a transition, there will come a day when the Kim regime will end and North Koreans will regain the freedom that they have been denied for over 70 years. The work of the Transitional Justice Working Group will also help with that. Finding out what happened to loved ones and bringing those responsible for the atrocities to justice will be a crucial part of helping the nation heal.

And it makes sense, says Dan, for the Transitional Justice Working Group to work with both Free Software and Free Software communities. The software offers the group a degree of security and control they cannot find in closed source applications; and Free Software communities uphold the same values Dan's group is fighting for, that is, the right to privacy and personal freedom.

<h2><a href="https://conf.kde.org/en/Akademy2018/public/events/98">Privacy Panel</a></h2>
 
Quite appropriately, after Dan's keynote, Adriaan de Groot ran a panel where members discussed the matter of privacy. Developing privacy-respecting software is one of KDE's main goals and the panelists explained how developing free and open Personal Digital Assistants like <a href="https://mycroft.ai/documentation/plasma/">Mycroft</a> was crucial to protecting users from snooping corporations.

Another thing we rarely think about but is a source of concern with regard to personal information is trip planners. In actual fact, the amount of sensitive information that we unwittingly share by letting opaque apps tell us when and where to catch our flight is staggering. Since the 2017 Randa sprint, there are KDE developers actively working on a truly open and private solution that will help solve this problem.

The other thing the panel discussed was the state of GnuPG in Kmail. GnuPG is the framework that allows users to encrypt and decrypt email messages that, otherwise, would be sent in clear text -- a big privacy concern. At this stage of play, GnuPG is tightly integrated into Kmail and, is not only convenient for end users, but has also proved to be immune to recent vulnerabilities that have affected other email clients.

Combined with the underlying policy of all KDE apps of never collecting data subvertly or otherwise, KDE is sticking strictly to its goal of preserving user privacy.

<h2><a href="https://conf.kde.org/en/Akademy2018/public/events/7">Streamlined onboarding goal</a></h2>

Neofytos Kolokotronis talked about the progress of another of KDE's main goals, namely the onboarding of new users. Neofytos explained to attendees the progress the working group had made so far and where they wanted to go to. He had some advice on how to help new users join KDE, such as having good and clear documentation, mentoring new contributors, and building connections outside your immediate niche.

<h2>More Highlights from Day 1<h2>

Wrishiraj Kaushik in his talk titled <a href="https://conf.kde.org/en/Akademy2018/public/events/44">Winds of Change - FOSS in India</a> spoke about the current scenario of FOSS in India and his experience leading <a href="https://superxos.com/">SuperX</a> and integrating KDE with it.

The Indian union government has a nation-wide recommendation in place for the use, promotion and development of Free and Open Source software. Despite this, FOSS adoption has remained low in the country. The decision taken by some state governments to not adopt these recommendations in conjunction with the aggressive marketing carried out by proprietary software vendors in India has seriously hindered the use of Free Software. SuperX, however, has managed to find a place within the government and a few Indian universities thanks to its user-centric approach. SuperX has deployed 30,000 KDE shipments -- one of the largest deployments in the world, and there are 20,000 more in the works.

This was followed by a <a href="https://conf.kde.org/en/Akademy2018/public/events/30">panel discussion by Lydia, Valorie and Bhushan</a> in which they told the community about our KDE student programs and how to contribute to their running and up-keep. It was a talk of high relevance, given our KDE Community goal to streamline the onboarding process for new contributors and the fact that a large part of our new contributor base comes through our organized mentoring programs, namely Google Summer of Code, Google Code-in and Season of KDE.

<a href="https://conf.kde.org/en/Akademy2018/public/events/32">Mirko Boehm presented a talk on the genesis of Quartermaster</a>, a toolchain driven by <a href="https://endocode.com/">Endocode</a> and supported by Siemens and Google. Quartermaster implements industry best practises of license compliance management. It generates compliance reports by analysing data from the CI environment and building graphs for analysis, primarily performing a combination of build time analysis and static code analysis.

<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;width:300px"><a href="/sites/dot.kde.org/files/Lays_small.JPG"><img src="/sites/dot.kde.org/files/Lays_small.JPG" /></a><br /><figcaption>Lays Rodrigues showed off Atelier, the graphical interface for 3D printers. Photo by Paul Brown, <a href="https://creativecommons.org/publicdomain/zero/1.0/">distributed under the CC0 license</a>.</figcaption></figure> 

<a href="https://conf.kde.org/en/Akademy2018/public/events/38">Lays Rodrigues talked about Atelier</a>, a cross-platform program designed to help you control your 3D printer. It supports most printers with open source firmware and Lays demoed the various features of Atelier during her talk, including video monitoring of the printer, 3D preview of the print design, temperature graphs and more.

<a href="https://conf.kde.org/en/Akademy2018/public/events/40">Zoltan Padrah gave a talk on KTechLab</a> and explained how he discovered it as a student of electronics engineering in 2008. KTechLab is a program that helps simulate electronic circuits and programs running on microcontrollers. It was migrated to the KDE infrastructure and joined KDE in 2017. The developers' upcoming plans are to release KTechLab for Qt4 and Qt5 and to port it to KDE Frameworks 5, as well as add new features like support to simulate automation systems for mechanics and have KiCad import/export.

<h2>Wrapping up</h2>

Day one was so full of content, it is hard to summarize everything that went on here. This has just been a summary of a few of the  talks and demonstrations we enjoyed. There were many more talks on all topics, ranging from containerizing KDE's graphical apps, to an end users' perspective of using Kontact in a professional environment.

As we write this, already on day 2, it looks like today is shaping up to be equally exciting.
<!--break-->