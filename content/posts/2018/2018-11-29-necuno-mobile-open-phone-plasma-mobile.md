---
title: "Necuno Mobile: An open phone with Plasma Mobile"
date:    2018-11-29
authors:
  - "Paul Brown"
slug:    necuno-mobile-open-phone-plasma-mobile
comments:
  - subject: "Very Excited"
    date: 2018-11-29
    body: "<p>This is good news. The FOSS community has been wishing for mainline Linux-friendly phone hardware for many years. While it still needs some development, Plasma Mobile and Kirigami feel like a solid foundation for getting FOSS experience into the hands of mobile users. The absence of available hardware has probably contributed to lower development activity of mobile-oriented FOSS code and slower adoption of convergence-ready frameworks. Now we may have three platforms: Librem 5, PinePhone (based on Pine64), and the Necuno Mobile. Very exciting time to be a Plasma user.&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;</p>"
    author: "Numeric"
  - subject: "Ubuntu Touch"
    date: 2018-11-30
    body: "<p>Will it be open source enough to allow Ubuntu Touch on the device?&nbsp; Open source phones always have an uphill battle, being disproportionately expensive compared to equivalent android devices, the problem being with production volumes.&nbsp; The BQ phones have been the only ones whose price leevel had been competitive enough IMO. Best of luck to the guys behind this project...(but UT support would have really been exciting for me) .&nbsp;</p>"
    author: "Trafford Pike"
---
<img class="alignnone size-full wp-image-135" src="/sites/dot.kde.org/files/necunobody_clock_cropped_shadow.png" alt="" width="800" />

<b><a href="https://necunos.com/">Necuno Solutions</a> and KDE are collaborating to offer <a href="https://www.plasma-mobile.org/">Plasma Mobile</a> on the Necuno Mobile, a device Necuno describes as "a truly open source hardware platform".</b>

With a focus on openness, security and privacy, the <a href="https://necunos.com/mobile/">Necuno Mobile</a> is built around an ARM® Cortex®-A9 NXP i.MX6 Quad and a Vivante GPU. According to Necuno, none of the closed firmware has access to the memory.

Necuno Solutions is working with open source mobile communities and intends to make their hardware a welcoming platform for Free and open source operating systems in the mobile ecosystem. Plasma Mobile and Necuno Solutions are a perfect match for a community collaboration because of their shared values. The aim is to grow the KDE and Necuno Solutions communities together and attract interested early adopters and developers so that everyone has a chance to join the effort.

<style>
blockquote{
  display:block;
  background: #fff;
  width: 80%;
  padding: 15px 20px 15px 70px;
  margin: 0 0 20px;
  position: relative;
  
  /*Font*/
  /*font-family: Georgia, serif;*/
  font-style: italic;
  font-size: 20px;
  line-height: 22px;
  color: #666;
  text-align: left;
  
  /*Borders - (Optional)*/
  border-left: 15px solid #0068c6;
  border-right: 2px solid #0068c6;
  
  /*Box Shadow - (Optional)*/
  -moz-box-shadow: 2px 2px 15px #ccc;
  -webkit-box-shadow: 2px 2px 15px #ccc;
  box-shadow: 2px 2px 15px #ccc;
}

blockquote::before{
  content: "\00BB"; /*Unicode for Left Double Quote*/
  
  /*Font*/
  font-family: Georgia, serif;
  font-size: 60px;
  font-weight: bold;
  color: #999;
  
  /*Positioning*/
  position: absolute;
  left: 10px;
  top:10px;
}

blockquote::after{
  /*Reset to make sure*/
  content: "";
}

blockquote em{
  font-style: italic;
}
</style>

 
Tanja Drca, Chief Communications Officer at Necuno Solutions says:

<blockquote style="font-family: Arial, sans;font-style: italic; font-size: 20px; line-height: 22px; color: #666; text-align: left; width: 85%;">
KDE has reached very far in the mobile ecosystem by leveraging the power of the community. We feel that KDE is in a good position to challenge the duopoly in the future. This will be a new innovative approach to combine truly open source hardware with a truly open source operating system"
</blockquote> 

Bhushan Shah, one of the main developers working on Plasma mobile says:
 
<blockquote style="font-family: Arial, sans;font-style: italic; font-size: 20px; line-height: 22px; color: #666; text-align: left; width: 85%;">
It is important that developers within the mobile ecosystem are able to work with open devices which are easy to modify and tweak, and not locked by vendors to a particular operating system. Necuno Solutions is working on one such device and will ultimately help improve Plasma Mobile due to its open nature."
</blockquote>

<a href="https://necunos.com/blog/necuno-solutions-and-kde-collaboration/">Read the full Press Release on Necuno Solutions' website</a>.
<!--break-->