---
title: "KDE/Linux As Windows 2000 Replacement?"
date:    2001-09-19
authors:
  - "Dre"
slug:    kdelinux-windows-2000-replacement
comments:
  - subject: "The Biggest Mistake: use of RH 7.1"
    date: 2001-09-19
    body: "The author recommends RH 7.1 with KDE but this is a grievous mistake. Red Hat doesn't care a bit about KDE and it shows with their packaging of KDE 2.2 and 2.2.1, in which they produced packages of a STABLE version of KDE against an UNSTABLE software.\n\nIf the author does really want to unleash KDE to its full potential then it should perhaps switch to a more KDE-friendly distro. Note, please, that KDE-friendly would not mean necessarily KDE-centric. If only Red Hat would build DECENT rpm releases of KDE built with STABLE software dependencies it would certainly fit the bill. But it doesn't."
    author: "Eduardo Sanchez"
  - subject: "Re: The Biggest Mistake: use of RH 7.1"
    date: 2001-09-19
    body: "I totally disagree.\n\nRedHat is one of the best Linux distributions there is, and it is also very usable as a KDE workstation. RedHat's support for KDE has gotten a lot better during past year.\n\nThe fact, that RedHat does their KDE packages for unstable OS first, is because their resources are limited (as well as in many other software companies).\nKDE 2.1.1, which works with RedHat 7.1 is quite well suited for desktop use - not everyone needs to have the latest version of KDE.\n\nNow don't take this as a RedHat sales crap. I've used many Linux distributions during the last years, and some of them make KDE packages faster for stable versions, but in many cases these packages do not work properly. For example Mandrake packages quite fast, but there is a lot application crashes, menu items do not show translated, etc.\n\nWhen a stable release of KDE is released, that does NOT mean, that it can be used right away stable in a distribution. Packaging takes time, you need to test quite a lot because of distribution specific stuff.\n\nWould you like to have unstable untested workstation software on your production machine? If yes, good luck!\n\nEleknader"
    author: "Eleknader"
  - subject: "Re: The Biggest Mistake: use of RH 7.1"
    date: 2001-09-19
    body: ">>RedHat is one of the best Linux distributions there is<<\n\nsure if you under \"the best linux distro\" mean \"a lots of bugs by defualt out of the box\".. geeezz.. doesn't matter what software I wanna use under linux I always find something like this in some README or INSTALL:\n\"For Red Hat X.X users: notice there is a bug..\" or \"... you need to upgrade your Red Hat X.X. package...\" etc...\nI just wander why other distors doesn't have such problems?? \n\nbut you are right, Red Hat is the best distro because it is the most secure distro outhere..... (buauahaha, felt from the chair)\n\nbut anyways it is not the topic we should be talking about here\n-----------------\n\nCool that KDE 2.2.1 is out! and can't wait for 3.0"
    author: "sUpeRGR@sS"
  - subject: "Re: The Biggest Mistake: use of RH 7.1"
    date: 2001-09-20
    body: ">sure if you under \"the best linux distro\" mean \"a lots of bugs by defualt out >of the box\".. geeezz.. doesn't matter what software I wanna use under linux \nRight, the only distro i have found thats more stable is Debian,though then you have to run debian stable,which means OLD packages..\n>I always find something like this in some README or INSTALL:\n>\"For Red Hat X.X users: notice there is a bug..\" or \"... you need to upgrade >your Red Hat X.X. package...\" etc...\n>I just wander why other distors doesn't have such problems?? \nOh, they do. Folks havnt just tested it. On redhat they _have_ tested it,\nand found problems and a solution. On other distros, you are on your own :(\n\nIf you really want a *stable* OS to run KDE at, consider FreeBSD."
    author: "NO"
  - subject: "Re: The Biggest Mistake: use of RH 7.1"
    date: 2001-09-20
    body: "sure I run the most secure and coolest distro \"out of the box\": Slackware - it gives me very stable base on which I can build stuff I need, and what's most important it's damn easy to use and maintain...\n\nbut hey, such discussion is pointless, everyone has its own taste and needs.. so let's say for me the best in the world is Slack for you RH, and peace bro!"
    author: "sUpeRGR@sS"
  - subject: "Re: The Biggest Mistake: use of RH 7.1"
    date: 2001-09-19
    body: "I use KDE 2.2 on Red Hat 7.1 just fine.  I did have to work just a *little* with\nrpm to get it to install, but it's working great."
    author: "TomL"
  - subject: "Re: The Biggest Mistake: use of RH 7.1"
    date: 2001-09-19
    body: ">> Packaging takes time, you need to test quite a lot because of distribution\n>> specific stuff.\n\nSpecific stuff bugs should go away with LSB ...."
    author: "HVilli"
  - subject: "Re: The Biggest Mistake: use of RH 7.1"
    date: 2001-09-19
    body: "I am sorry, but that is not simply the case. It provides a very good starting setup for KDE, but it nullifies any upgrade path. If you use KDE 2.1.2, that means that you are *stuck* with it unless you \"upgrade\". And it hasn't got better; some time ago it even provided KDE 2.1 packages for 6.2, something that it does not do anymore (heck, it doesn't even provide packages for their CURRENT distro !).\n\n\nI understand your point of stability, but all that this means (including the excuse of limited resources) is that for Red Hat, KDE does not really matter.\n\nEduardo"
    author: "Eduardo Sanchez"
  - subject: "Re: The Biggest Mistake: Not to use of RH 7.1 ;)"
    date: 2001-09-19
    body: "If you don't like RedHat Linux, then say so, but You have no right to say anything about the author's view/liking. It's his prerogative, so you are not supposed to give judgement. Have you tried RH 7.1, along with other distros?\n\nI Love KDE for its superior ease of use (except kikcer, and konsole), and I have tried the following distros:\n\n1. Redhat 6.1, 6.2, 7.0 and 7.1\n2. Mandrake 7.1 and 7.2\n3. SuSE 6.3\n4. Corel Linux 1.0\n\nto check which distro has better KDE in it. RedHat and SuSE were equal (not meddling with many default settings of KDE like Menu arrangement and Kcontrol etc). Corel Linux had the best panel then (still kicker do not have key binding to navigate menus with keyboard), and best Integrated Configuration modules (Still KControl do not have any System and Network configuration modules, which could have been easily ported from LinuxConf to KDE's Control Center). I see a conspiracy in not (allowing) to create System and network Configuration modules by some companies, to get an advantage over competition (RH?), and you know who they could be! And I feel that These so called KDE supporting companies are hampering KDE's proper/equal development in many of the user needed parts of the Desktop Environment. If this were not the case, then KDE would have had many basic system and network configuration modules ready by KDE 2.0. As Mandrake has many Drake configuration tools it is implicitly forcing KDE to develop in other part of the desktop environment (for their advantage), so is SuSE with Yast2. Atleast RedHat contributed atleast 2-3 system configuration modules for KDE, even when RH had the good LinuxConf (not great GUI but functional and purposeful). \n\nI switched to mandrake 7.2 (some months earlier) to see how good it was, though it had nice configuration and other fun stuff still it is not supported by most of the people and in my country India, (almost) every body prefers RedHat 7.1, over other distros for its stability. And PCQuest a popular Computer magazine provides RH distro's almost every stable release, which makes it more popular in my country.\n\nNo matter what you think, I love Redhat+KDE, as these two are stable and fast (not fully applicable to the latter ;) and will bring Linux to the Desktop!\n\nUnfortunately KDE is worst at Copy, Paste and Renaming, and bugs reported in KDE 2.1.1 is discarded in the name of QT and still present in KDE 2.2 :(\n\nThe File Rename bug still lurks to crash konqueror, when you press F2 key (to rename) two to three times! I wonder how many such bug reports got ignored and labelled \"Closed\".\n\nI hope you won't take this as a flame but as constructive criticism."
    author: "Asif Ali Rizwaan"
  - subject: "Re: The Biggest Mistake: Not to use of RH 7.1 ;)"
    date: 2001-09-19
    body: "Asif: thanks for your comments, and they're really helpful. In fact, my criticism stems from the fact that I really love RH + KDE, and the RH 7.1/KDE 2.1.2 was really a killer combination, as can be readily appreciated not only by you and me, but also for the author of this article.\n\nI hope I can explain myself better now. You can certainly use KDE 2.1.2 with RH 7.1, and you can wait till the next stable distro release for an upgrade to KDE 2.2.1. But this only sends the message that a Red Hat distro is really not upgradeable, and in fact it isn't if we consider the case of the KDE releases. This also shows, additionally, that for Red Hat, KDE is not a priority; if it were so, they would make available KDE 2.2.1 RPMs for RH 7.1, and with no strange dependencies, in no time. But they are not doing it so.\n\nNow think that you are the administrator described in the article mentioned in this post, and you would like to upgrade to KDE 2.2.1. The plain fact is that you can't unles you introduce a major set of UNSTABLE packages required by dependencies and this, and not the KDE release, puts your workstation in jeopardy. Sometimes the packages are really broken, as is the case with the infamous kdeadmin-2.2 that forced you to change rpms, thus affecting db3 and everything in between by the way.\n\nThat is the problem, Asif. Many, many people are drawn to Red Hat because of KDE, only to learn afterwards that they cannot upgrade because Red Hat does not care about KDE nor its KDE users.\n\nEduardo"
    author: "Eduardo Sanchez"
  - subject: "Re: The Biggest Mistake: Not to use of RH 7.1 ;)"
    date: 2001-09-20
    body: ">now think that you are the administrator described in the article mentioned in >this post, and you would like to upgrade to KDE 2.2.1. The plain fact is that >you can't unles you introduce a major set of UNSTABLE packages required by >dependencies and this, and not the KDE release, puts your workstation in >jeopardy. Sometimes the packages are really broken, as is the case with the >infamous kdeadmin-2.2 that forced you to change rpms, thus affecting db3 and >everything in between by the way.\n\nthe kdeadmin package has always been a heache, first it was kuser, now kpackage :(, but you can get KDE's 2.2 package from http://www.opennms.org/~ben/kde2.2, just get the kdeadmin package from there. \n\nI know we all want the lastest of everything, but RedHat releases updates carefully. As you can see with mandrake package which are broken (as I have read at dot.kde.org). RH assures itself of the package security and stability before it releases (as I feel). Still KDE-Installer which will be the part of KDE 3.0 will ease the upgradation process of KDE :)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: The Biggest Mistake: Not to use of RH 7.1 ;)"
    date: 2001-09-19
    body: "I've tried almost every distro there is but I always find myself falling back to Debian. I've ran Debian since 1997 and many times I've thought \"Maybe I should try this new cool distro\". I install them, run them for a few weeks and I ALWAYS miss apt-get and the HUGE repository of up-to-date packages in testing/unstable!\nShort reviews of some of the distros I've tried the last year:\nRed Hat 7.0: Well, trying to be bleeding edge by using beta versions of gcc and glibc and other essential software. Often causes problems and incompabilities. Also has insecure default settings. (Ok, I haven't tried RH7.1, but it still doesn't have apt-get!:)\nMandrake 8.0: Nice default install, but it installs too much crap that I don't need. They have to fix their dependancies! It feels that all packages depend on each other... apt-get for Mandrake is available in contrib, but it's nowhere as good as apt-get in Debian because 1) The dependancy hell. You can't install KDE without installing Gnome for example, and the packages are not split into separate libs/app/extras packages. 2) The RPMs doesn't ask any questions when you install them. You are stuck with the default config.\nSuSE7.1: Very nice distro! Very nice! But there is no apt-get or similar functionality. Their SuSE-update tool only provide securityfixes. There is no way to upgrade versions other than to *buy* the next SuSE-version or compile them yourself, which may conflict with already installed versions.\nRock Linux: Cool distro for hackers, but I don't have the time do EVERYTHING by hand all the time...\nSlackware 8: apt-get, where are you?\nCaldera 2.4: Buggy as hell.\nFreeBSD 4.3: Very nice!! But I can't access my 80GB striped Linux-LVM LV which I keep all my OGGs on with it... :(\n\nWell, back to Debian. Run Woody and you have a stable system with 8000 up-to-date packages always available at a single command. If you want a package only available in Sid (unstable), if it's not a core-package (like glibc, gcc, ld.so etc), run \"apt-get -t unstable packagename\", and it gets installed in a few seconds/minutes/hours (depending on your connection). :) Gotta love it!"
    author: "Per Wigren"
  - subject: "Re: The Biggest Mistake: Not to use of RH 7.1 ;)"
    date: 2001-09-20
    body: "Hear hear.. debian r0x0rs j00r a55 fUx0r"
    author: "MegaBite"
  - subject: "Re: The Biggest Mistake: Not to use of RH 7.1 ;)"
    date: 2001-09-20
    body: "Hmm...I run Mdk 8 at work, and I can not understand your criticism at all.\n\n1. \"Nice default install, but it installs too much crap that I don't need.\"\nWell, then don't go with the default install! If you're such a power user that you can tell wether to install libbonobo0 or not, you should be able to customize your install.\n\n2. \"You can't install KDE without installing Gnome for example\"\nThis may be true if you try to install it via apt-get, I haven't tried it so I don't know, but otherwise it works just fine. Why not try urpmi btw? It has similar functionality, and works pretty well.\n\n/Peter"
    author: "moZer"
  - subject: "Re: The Biggest Mistake: Not to use of RH 7.1 ;)"
    date: 2001-09-21
    body: "***SuSE7.1: Very nice distro! Very nice! But there is no apt-get or similar functionality. Their SuSE-update tool only provide securityfixes. There is no way to upgrade versions other than to *buy* the next SuSE-version or compile them yourself, which may conflict with already installed versions.***\n\nthat's not true. you can download the latest (stable) versions from www.suse.de (Download und Updates) or www.suse.com.\n\nMiso"
    author: "Miso"
  - subject: "Re: The Biggest Mistake: Not to use of RH 7.1 ;)"
    date: 2001-09-28
    body: "I confess I have only ever used SuSE, no particular reason, just bought 6.3 now on 7.2, regarding updates, because I'm a bit useless I'll be buying 7.3 when it comes out \u00a328/$42 think I can afford it, but all updates for SuSE can be foud on their website and on mirrors (I use www.mirror.ac.uk) If only I knew how, I could be running a SuSE approved 2.4.7 (I've de-rpm'ed it anyway)\n\nHowever, I find SuSE a bit nifty and the tar for 2.2.1 went in without trouble (see above) which is just as well"
    author: "gerry"
  - subject: "What server is this www.robval.com running on?"
    date: 2001-09-19
    body: "I was wondering whether www.robval.com is running on Windows?\n\n-Db-"
    author: "DildoBaggins"
  - subject: "Re: What server is this www.robval.com running on?"
    date: 2001-09-19
    body: "According to netcraft (http://uptime.netcraft.com/up/graph/?mode_u=off&mode_w=on&site=www.robval.com&submit=Examine) it is running windows, why do you wonder?\n\n/J\u00f8rgen"
    author: "J\u00f8rgen"
  - subject: "Re: What server is this www.robval.com running on?"
    date: 2001-09-19
    body: "Yup.\n\n$ lynx -head -dump http://www.robval.com \nHTTP/1.1 200 OK\nServer: Microsoft-IIS/4.0\n[...]"
    author: "jeez"
  - subject: "Re: What server is this www.robval.com running on?"
    date: 2001-09-19
    body: "So much for replacing Windows then...."
    author: "DildoBaggins"
  - subject: "Re: What server is this www.robval.com running on?"
    date: 2001-09-21
    body: "It's really stupid to use Windows as a web server (and as an Internet server at all), especially NT4/IIS4 which are known to be full of bugs and security holes. But warez people thanks those stupid admins who do so !..."
    author: "Herv\u00e9 PARISSI"
  - subject: "Opera?"
    date: 2001-09-19
    body: "Why did they choose Opera when they can use khtml in konqueror? It's just as good, if not better!\n\n/J\u00f8rgen"
    author: "J\u00f8rgen"
  - subject: "Re: Opera?"
    date: 2001-09-19
    body: "I am a big fan of konqueror, but... I switched to opera because it's MUCH (I mean REALLY MUCH) faster both connectiong/rendering.\nIt really have serious problems with css/dhtml, but it's speed plus stability won the browser war on my computer :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Opera?"
    date: 2001-09-20
    body: "What, faster and more stable?? You got to be kidding... Opera does strange things and crashes all the time, ok... it renders fast, but what it renders is crappy... If you ask me Konqueror is a product far superior to Opera..."
    author: "Frederik Holljen"
  - subject: "Re: Opera?"
    date: 2001-09-24
    body: "Well, I'm using 5.05rc1 and it crashed 3 times in a month.\nI lost the number of times konqueror krashed on same time.\nSure, konqueror supports more css, dhtml, etc. But for the day-to-day use opera is far better to me.\nBut remember every time: each machine is a different case (unhapply), some things good for someone isn't good for other people.\nAnd even crashing, opera have the recovery system that works as magic. You don't loose any url when it crashes."
    author: "Iuri Fiedoruk"
  - subject: "Windows Look and feel in KDE"
    date: 2001-09-19
    body: "it seems that some the kde people abhor Windows, but if we wish to have KDE on every Intel computer then we have to give what they have been used to, like:\n\n1. Keybinding (provided)\n2. Style       (provided)\n3. Icons       (I have the Icon-theme but can't upload it on theme.org)\n4. Panel/Taskbar. Please see the attachment \n5. Easy Configuration Utilities and \n6. Many wizards which will ease the user's work.\n\nThe 'K' button in KDE is rather small at small and tiny size and it is also not resizable. I have many times requested the kicker authors for this windows-like 'K start' button. Please see the wish-kicker.png. And I would love your comment on it."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Windows Look and feel in KDE"
    date: 2001-09-19
    body: "I don't know if i like your new kicker at all....it looks TOO much like\nwinblows,\n\nwe are not trying to copy or emulate the windows environment...sure\nthere are things we have to incorporate to make transitions easier but copying the exact look is abusrd and not very creative...in my opinion i like it the way it is!\n\nKeep up the EXCELLENT work KDE team!"
    author: "Brad C."
  - subject: "Re: Windows Look and feel in KDE"
    date: 2001-09-19
    body: "I don't understand your possition, why someone would want to have a KDE desktop identic to windows?, I would like to have a even more configurable desktop, but your request would fit with a Icon-IconText-OnlyText option in the task bar, doesn't seem complicated to me, they guys have been bussy but surely they will do it some time. \n\nI preffer the whole lot of caracteristics that KDE gives me for FREE, and BTW they are a lot more than windows gives by a \"moderated price\" !!"
    author: "GaRaGeD"
  - subject: "Re: Windows Look and feel in KDE"
    date: 2001-09-19
    body: "I don't think it should be identical to windows, but I really like the k-menu being a button not only a icon on small panel to look different than other icons, putting a text with it is a good idea, but start IMHO is much windows, what about something like menu, or programs?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Windows Look and feel in KDE"
    date: 2001-09-19
    body: "On the contrary, the K button is infinitely large no matter how small it looks on the screen.  You can move your mouse as far down or left as you want and you will always hit it.  Once a user learns where it is they are unlikely to forget, so all that needs to be done is to add a little pointer to it the first time you start KDE.  Then users will discover it and always know where it is.  They'll never have trouble hitting it with the mouse.\n\nHaving a \"start\" button makes no sense.  Tell me why you think the word \"start\" would help users who already knew where the button was?"
    author: "not me"
  - subject: "Re: Windows Look and feel in KDE"
    date: 2001-09-19
    body: "Are you crazy...?\n\nNo, seriously, this would be THE wrong way. KDE shouldn't copy everything from windows, especially not the bad ones. Why should I have to click on \"start\" to finish my KDE-session? Windows has a lot of interface-mistakes. It's no good just copying them, because everyone's got used to them. KDE is much more easier to learn for beginners. And those who already know PCs from windows will ALL begin to love KDE after some time (approximately one year) of using it. Suddenly you'll see all the advantages and also the beauty of KDE.\n\nConcerning your not-ability-to-upload-the-windows-theme-to-theme.org: This is just clear. All the icons have been made by \"some firm from Redmond\" and they have the copyright of them. You're not allowed to use them outside of Windows, nor are you allowed to put them anywhere on the net.\nClearly themes.org doesn't support your criminal attempts to break law ;-)"
    author: "grovel"
  - subject: "Re: Windows Look and feel in KDE"
    date: 2001-09-19
    body: ">Why should I have to click on \"start\" to finish my KDE-session? \n\nYes, this is a non-sense that doesn't make windows easier to use !\n\nregards"
    author: "Agree"
  - subject: "Themes.org issue"
    date: 2001-09-19
    body: ">Concerning your not-ability-to-upload-the-windows-theme-to-theme.org: This is >just clear. All the icons have been made by \"some firm from Redmond\" and they >have the copyright of them. You're not allowed to use them outside of >Windows, nor are you allowed to put them anywhere on the net.\n> Clearly themes.org doesn't support your criminal attempts to break law ;-)\n\nFYI, themes.org has been dead for 3-4 months. And you have a very nice imagination :)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Windows Look and feel in KDE"
    date: 2001-09-21
    body: "Does anybody ever wonder why the Windows taskbar is in the bottom of the screen ? And why KDE does copy this really sick typical M$ behaviour ?\nI always put the taskbar at the top, where it should be, and of course most Windows apps being bad-written, they start under it, what a shame ! But at least I don't spend 80% of my computer-time looking down at the bottom of the screen... (am I getting paranoid there ? ;)\n\nI agree KDE shouldn't copy every Windows shit, we're not running a weapon-race, right !"
    author: "Herv\u00e9 PARISSI"
  - subject: "Re: Windows Look and feel in KDE"
    date: 2006-07-26
    body: "Actually, as I am a prior Windows loser, you can absolutely configure Windows apps to launch from anywhere. Right click the task bar & select Lock taskbar to uncheck   \nit. Now that you have unchecked it, right click the start button & select explore all users. This opens the window containing the make up of the start menu, including all icons contained therein. Now you may drag and drop the icons onto the taskbar for placement. Make sure that your taskbar properties has show quicklaunch checked and you should be all set. You can also right click the taskbar and under toolbars, you can choose preformatted menus or create a custom menu. But this is purely to point it out and not to say that Windows is not a turd, because it is. Cheers."
    author: "zeusx64"
  - subject: "Re: Windows Look and feel in KDE"
    date: 2006-07-26
    body: "I almost forgot to point out that there is a bitmap image on the right side of this page. Just click it to see what I mean.\nNow if you want to get down to brass tacks, KDE should not be allowing the win32 port. That is completely insane. Yes it would be funny and almost fascinating to see it there as the UI but the more people port open source to Windows, the more people will stay there. Why would they want to switch to Linux or Unix with all of the cool apps already ported to Windows. Yes, I have Inkscape and The Gimp on my Win PC at work but that is merely a fix to deal with Windows while I still have to.\nKDE is really a great environment. It was my first choice when I started using Linux and is neck and neck with Gnome at this point but Really, keep it where it belongs.\nCheers"
    author: "zeusx64"
  - subject: "Re: Windows Look and feel in KDE"
    date: 2001-09-29
    body: "I agree that the start button on KDE (if the kicker is set to \"small\") is too small, and this is a serious GUI issue.  Naturally, a button that is often used should be big and easy to hit.  \n\nAlso, let's assume for a moment that the kicker is configured in such a way as to place the K button in the lower left hand corner.  I'm at work (and therefore running under Win2K) but I tried to click the \"Start\" button under Windows by moving the mouse pointer to the extreme lower left corner and clicking.  It didn't work.  I don't know how it works under kicker (but I will try it).  This is an interface design problem!  Such a common button should be easy to hit, and by allowing it to be activated by clicking one of the four easiest-to-hit pixels on any display, the corners, it would be very easy indeed.  I hope that kicker works this way, and if it doesn't, perhaps it should.  \n\nP.S. Let's make the K button bigger on small configurations of \"kicker\", but PLEASE let's use something other than \"Start\" to fill the empty space!  Even a centered \"K\" with a little bit of colorful design on either end would be better.\n\nI'll see what I can come up with.\n\nErik"
    author: "Erik Hill"
  - subject: "Re: Windows Look and feel in KDE"
    date: 2003-11-02
    body: "A good reason can be a smooth transition from windows to linux.\n\nOr a mixed enviroment, some PCs might have windows on them some linux, It's easy for people who come from windows to drop them in a desktop enviroment that looks exactly like the one they know.\n\nThis way you avoid explaining the new desktop/layout to people, and they feel right at home.\n\nI mean in an office setting with mulitple users, and the above situation can be very desireable for the system-administration to set up a windows look.\n\n"
    author: "bla"
  - subject: "programs win -> linux comments"
    date: 2001-09-19
    body: "Here is MY opnion :)\n\nEmail: StarMail: separate E-mail/Address book. Not recommended\n>This item could well use kmail, it's a good email program and is >getting better and better.\n\nAnti-Virus: Not required with Linux, according to the experts.\n>You can use antivirus on linux to clean accounts of users with >windows files. There is a mcafee virusscan for linux, but beware, >it's default option is delete, not clean.\n\nPDF Writer: StarWriter: print as PS file, then use ps2pdf. It works.\n>KDE can do this automatically, so you can use Koffice or >Konqueror.\n\nImage Viewer: Konqueror's built-in viewer, or Pixie KDE Image Manager.\n>I shold say gqview, it's the best image viwer I found for linux, >or ShowImage it you want a KDE program, but it needs more work."
    author: "Iuri Fiedoruk"
  - subject: "Alternative"
    date: 2001-09-20
    body: "I must admit win2000 is nothing more than just an alternative Linux/KDE."
    author: "Gill Bates"
  - subject: "Share files..."
    date: 2001-09-20
    body: "Still wondering how they handle the ease of sharing files, and browsing\nother computer.\n(samba+lisa+konqueror can handle it, but there is _nothing_ easy about it,\nand its pretty buggy)"
    author: "NO"
  - subject: "How to optimize dot.kde.org articles"
    date: 2001-09-20
    body: "This is a nice example on how to optimize dot.kde.org articles and reduce the size of the HTML page as well as the traffic generated.\n\nThis articles should have been captioned \"Windows 2000 as KDE/Linux Replacement?\". That way, the article's text could have been optimized to \"Just kidding.\", and we're set.\n\nThanks for your attention, don't forget your homework, lesson's over."
    author: "Frerich Raabe"
  - subject: "Re: How to optimize dot.kde.org articles"
    date: 2001-09-21
    body: "*ROTFLOL*"
    author: "KDE User"
  - subject: "Linux/KDE vs Windows2K"
    date: 2001-09-22
    body: "I love Linux and  I seem to find myself using it more than I do win2k and 98. But things KDE really lacks is a decent X config APP that can change resolutions like windows does. Caldera also need to port it to Unixware which is out big server in the company I work at. I have tried many distros of linux and I found the best for what I use it for was Red Hat 7.1 with Kudzu and the only problem I have with Red Hat is its support of gnome. Gnome I have found to be unstable at least 60% of the time, so the new KDE is just perfect for my use on my Red Hat Fileserver (Replaced NT :) no more NT on our network just Unixware, OpenServer and RedHat). I found that Red Hat is the most stable out of 90% of the distros I have tried. On the other hand it has nothing on SCO or Debian Linux, which is still by far the most secure and stable Linux out there. I still prefer RedHat because it has a nic directory stucture and better functionality over the others!"
    author: "Shimmer The Mage"
  - subject: "Re: Linux/KDE vs Windows2K"
    date: 2002-01-30
    body: "the site is too good.but i want to know how to configure the lilo boot in the linux."
    author: "sudheer"
---
<A HREF="http://www.robval.com/">Rob Valliere</A> has <A HREF="http://www.robval.com/linux/desktop/index.asp">written</A> a success story in which he (partially) converted a small office (25 computers) from a Windows-only environment to a mixed Windows-Linux/KDE environment.  The review includes useful tables listing commonly-used Windows 2000 applications and their Linux counterparts; hardware requirements for both types of systems; a cost savings analysis; a software comparison guide (including <EM>the good</EM>, <EM>the bad</EM> and <EM>the ugly</EM>); and a migration guide.  The analysis shows that even if a small business needs to retain some Windows boxes because of the lack of comparable Linux software, significant cost savings can be achieved (in this case $5,000 for the mixed network versus $15,000 for a Windows-only network).  Now if I could only find the time to write up what I learned during my visit to the <a href="http://www.largo.com/">City of Largo</a> (which, as we <a href="http://dot.kde.org/995949998/">reported</a> a few weeks ago, switched a much larger network to KDE 2).



<!--break-->
