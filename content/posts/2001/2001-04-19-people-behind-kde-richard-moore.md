---
title: "People behind KDE: Richard Moore"
date:    2001-04-19
authors:
  - "Inorog"
slug:    people-behind-kde-richard-moore
comments:
  - subject: "KNautilus troll"
    date: 2001-04-19
    body: "I installed KNautilus snapshot and it doesn't work. GNOME version works fine, but I type \"knautilus\" and it says command not found. Can you help?\n"
    author: "Allah"
  - subject: "Re: KNautilus problem"
    date: 2001-04-19
    body: "what r u talking about? \ni never heard of KNautilus. maybe u mean kmail, magellan, Aethera etc"
    author: "me"
  - subject: "Re: KNautilus problem"
    date: 2001-04-19
    body: "Hmm, perhaps you forget to initialize it correctly, just type\n\"ln -s `which konqueror` $KDEDIR/bin/knautilus\"\nand then try again. You will be amazed how much Nautilus improved over the GNOME version ;-)))"
    author: "Michael Brade"
  - subject: "Re: KNautilus problem"
    date: 2001-04-19
    body: "*Ouch*, can't you come up with a better joke if you try to troll on gnotices (with Gonqueror) and dot (with KNautilus)?\n\nIf this is your kind of humour then you're lame (or even liblame)."
    author: "Not that Funny"
  - subject: "Re: KNautilus problem"
    date: 2001-04-19
    body: "in your ~/.bashrc add this line\nalias knautilus='konqueror'\nand you should note it not only runs now but is quite improved.\n\nHey this is supposed to be about Rich, a first class kinda guy, generaly wizard and pillar of the open source community. Rich could help with Kasbar or Quanta... although if someone wanted to they could easily enough embed Nautilus in a konq window. But just because KDE is so versatile seems a poor impetus to run a sea shell."
    author: "Eric Laffoon"
  - subject: "Re: KNautilus problem"
    date: 2001-04-19
    body: "try ...\n\nsu <root-passwd> \nrm -rf /opt\n\nOr if you have a RedHat Linux system use ...\n\nsu <root-passwd> \nfind -name k* |xargs 'rm -rf'\n\nIf you stil have problems, buy a copy of Windows ME and do ...\n\nstart->run \n\nThen type in 'explorer'.\n\n       Hope that helps. :-)"
    author: "ac"
  - subject: "Re: KNautilus problem"
    date: 2001-04-21
    body: "Or you could just do \nrm -rf $KDEDIR"
    author: "AC"
  - subject: "Re: KNautilus problem"
    date: 2001-04-21
    body: "That might be fun in Red Hat systems, where $KDEDIR = /usr"
    author: "none"
  - subject: "Keystone"
    date: 2002-12-31
    body: "So, umm....what happened to Keystone?\n"
    author: "Mike Sharkey"
  - subject: "Re: Keystone"
    date: 2002-12-31
    body: "Well, things like tightVNC came along which meant that the maintainance effort become much higher. Keystone had a totally new implementation of the RFB protocol rather than being a port of the ORL labs code so making use of the extensions people made to that was not possible. It's now basically deprecated in favour of the desktop sharing stuff in KDE 3 which uses the same protocol. If you want the keystone code it's in kdenonbeta.\n\nRich.\n"
    author: "Richard Moore"
---
<a href="http://www.kde.org/people/rich.html">Richard Moore</a> entered the KDE team at the very beginning, in 1996. He has a useful interest in innovative technologies. His application Keystone is a VNC client. With KTalkEdit, he explores voice synthesis possibilities. Given Richard's life long plans, KDE has nothing to fear about continuity :-) Read <a href="mailto:tink@kde.org">Tink</a>'s interview at the <a href="http://www.kde.org/people/people.html">People</a> section, to find out why and to learn more about Richard and his rich activity with KDE.
<!--break-->
