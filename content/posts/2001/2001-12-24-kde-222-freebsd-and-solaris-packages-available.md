---
title: "KDE 2.2.2:  FreeBSD and Solaris Packages Available"
date:    2001-12-24
authors:
  - "Dre"
slug:    kde-222-freebsd-and-solaris-packages-available
comments:
  - subject: "See, how KDE is giving freedom!"
    date: 2001-12-25
    body: "I appreciate KDE peoples' work and the package creators who gives more choice to the users. Now one can use KDE, the best desktop environment on any Unix flavor! Thanks a lot friends!!\n\nI saw Caledera OpenLinux and saw 'KXconfig' tool a nice one, can't we have that for KDE for other distros, so that Xconfiguration becomes easier?"
    author: "Asif Ali Rizwaan"
  - subject: "Great Wishes!!!"
    date: 2001-12-25
    body: "http://homepage.uibk.ac.at/service/wishlist/main.html\n\nhttp://homepage.uibk.ac.at/service/wishlist/select.html?stat=a&sec=6"
    author: "yes me"
  - subject: "Re: Great Wishes!!!"
    date: 2001-12-25
    body: "Above wishlist is closed/unmaintained for over two years now!\n\nAnd we have http://bugs.kde.org/db/si/pendingwishlist.html"
    author: "someone"
  - subject: "Thanks KDE Team"
    date: 2001-12-25
    body: "Merry Christmas everyone !"
    author: "Santa ;-)"
  - subject: "Not related but anyway!!"
    date: 2001-12-27
    body: "I have a question, want to here if someone else has got the same result. I upgraded from kde 2.2.1 to 2.2.2 and the dir browsing in konq is much slower. Could it be a setting? or what can it be. I used the mandrake rpm's. I installed all packages ... I dont know whitch I need so I installed them all."
    author: "Andreas"
  - subject: "Disable Klipper"
    date: 2001-12-27
    body: "It's the problem with Klipper and QT (as discussed earlier here). Just 'killall -KILL klipper' will correct it for now."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Disable Klipper"
    date: 2001-12-27
    body: "That dosen't always help - using Qt 2.3.2 will still cause problems, whether or not Klipper is running.  Unfortunatly, if you use SuSE rpms, you're stuck with it (last I checked, two or three weeks ago).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Disable Klipper"
    date: 2001-12-28
    body: "aktually I got a speed improvment but I dont know if was some kind cash that make the improvment. I runned the killall -KILL klipper thing and I got a no proces to kill answer so I assumed that it didnt work. But when I started to work again I was amazed of the speed."
    author: "Andreas"
  - subject: "Install problems on Solaris"
    date: 2001-12-28
    body: "I cant seem to install some of the Solaris packages.  I get the following:\n\n% pkgadd -d pcre-3.7-SPARC-Solaris8-local.pkg \npkgadd: ERROR: no packages were found in </var/tmp/dstreAAAzJaGSP>\n\nSome packages work however:\n\n% pkgadd -d xpm-3.4k-sol8-sparc-local.pkg \n\nThe following packages are available:\n  1  SMCxpm     xpm\n                (sparc) 3.4k\n\nSelect package(s) you wish to process (or 'all' to process\nall packages). (default: all) [?,??,q]:\n\nAre some of the packages corrupt, or is it just a problem with my Solaris installation?  I have full+OEM installation, and the latest recommended patches."
    author: "Funky_Peanut"
  - subject: "Re: Install problems on Solaris"
    date: 2001-12-28
    body: "Just to let you know if it's a problem with your Solaris installation then I have exactly the same problem.... I've spent ages fiddling around with them but with no success. I don't seem to be able to get any of the AROBASE* packages installed though, as you say, the SMC* packages seem to be fine.\n\nThanks in advance for any suggestions - I'll be very happy to have an up-to-date KDE to replace the terrible CDE!"
    author: "Daniel Hanlon"
  - subject: "Re: Install problems on Solaris"
    date: 2001-12-29
    body: "Replacing CDE is worth _any_ amount of work. ;)\n\nUse older KDE packages (still kicks CDE's butt) or compile 2.2.2 yourself. I installed 2.1 binaries in my $home on one of those machines, transforming the machine into something _useful_..."
    author: "Johnny Andersson"
  - subject: "Re: Install problems on Solaris"
    date: 2002-01-05
    body: "I had the same problem installing the packages on Solaris 8.\nAfter I installed all the recommended patches for Solaris (download ~52 MB) it worked fine. So my guess is to install the patches for solaris.\n\nNow I have a different problem. As root I can use KDE. But any other user it keeps saying that DCOP server is not running. Any idea's\n\nBTW KDE is a great replacement for CDE and even for windows\njohan."
    author: "Johan"
  - subject: "Re: Install problems on Solaris"
    date: 2002-01-10
    body: "PLEASE READ THE README that I included with the packages.\n\nThere is a known Solaris (not KDE) issue with /tmp/.ICE-unix permissions.  Users need to be able to write to /tmp/.ICE-unix in order for the KDE DCOPserver to start up, etc., but logging in as root before any other user causes the directory to be created root:root and 775.\n\nIf you used my packages, it includes an init script which does a chmod 777 on /tmp/.ICE-unix on bootup. If you then login as a regular user straight away, not logging in as root first, then everything works.\n\nIf you login as root, change the permissions before logging out, or write yourself a script to do so.  You could also set up a root cron job to chmod 777 /tmp/.ICE-unix every 5 minutes or so, or simply chgrp staff /tmp/.ICE-unix, assuming that your users are all members of 'staff'.\n\n-- \nLen Laughridge\nArobase Group, Inc."
    author: "Len Laughidge"
  - subject: "Re: Install problems on Solaris"
    date: 2002-01-10
    body: "Hello:\n\nYou need to install Solaris patches 110380-04 and 110934-05 which \ncorrect this issue. \u00a0Upgrading to Solaris 8 (10/01), which includes \nthese packages, also solves it. \u00a0If you're running older releases of \nSolaris 8, get the latest 8_Recommended patch cluster for Sparc. \u00a0I \nthink December was the latest cluster release.\n\n\n-- \nLen Laughridge\nArobase Group, Inc."
    author: "Len Laughidge"
  - subject: "Re: Install problems on Solaris"
    date: 2004-09-07
    body: "This same problem occurs on Solaris 9 4/04.\nThe patches for Soaris 8 do not apply.\nAny suggestions?\nThanks,\nMichael"
    author: "Michael Mehdizadeh"
  - subject: "Re: Install problems on Solaris"
    date: 2001-12-31
    body: "I also have the same problem on a 'fresh' load of Sol8....hmmm. the 2.2binaries worked fine on my other machine at werk. any info getting new new version to work would be helpful thanks...\n\ncyberslug"
    author: "cyberslug"
  - subject: "Re: Install problems on Solaris"
    date: 2002-01-10
    body: "Your version of Solaris 8 must be older than 10/01 on the machine that is not working.\n\nYou need to install Solaris patches 110380-04 and 110934-05 which \ncorrect this issue.\n\n\n-- \nLen Laughridge\nArobase Group, Inc."
    author: "Len Laughidge"
  - subject: "Re: Install problems on Solaris"
    date: 2002-01-02
    body: "I sent the developer my notes from debugging pkgadd, \nwe can probably get them happy again pretty quickly!\n\n--dave (at home) c-b\n  from Opcom, at Sun Canada"
    author: "David Collier-Brown"
  - subject: "Re: Install problems on Solaris"
    date: 2002-01-10
    body: "Hello:\n\nYou need to install Solaris patches 110380-04 and 110934-05 which \ncorrect this issue. \u00a0Upgrading to Solaris 8 (10/01), which includes \nthese packages, also solves it. \u00a0If you're running older releases of \nSolaris 8, get the latest 8_Recommended patch cluster for Sparc. \u00a0I \nthink December was the latest cluster release.\n\n\n-- \nLen Laughridge\nArobase Group, Inc."
    author: "Len Laughidge"
  - subject: "Re: Install problems on Solaris"
    date: 2002-01-10
    body: "I experienced the same problem;  a quick email to the Arobase Group, however, advised me to install Solaris patches 110380-04 and 110934-05 which correct the issue with pkgadd.\n\nI installed the entire 1/9/02 cluster (I was overdue!) and the remainder of the KDE install process went smoothly.\n\nStellar tech-support from the Arobase Group!"
    author: "Christopher Murphy"
  - subject: "is there a KDE  howto for solaris 8 anywhere ?"
    date: 2002-01-01
    body: "hi there,\n\ni have tried to install the KDE package supplied by default in the Solaris 8 CD set ( from sun), and have succeeded in installing the same.\n\nalso the dtlogin script ran without errors / displays KDE on the login screen.\n\nbut when you select kde, it just hangs, goes to the common desktop env. and stays there.\n\nanyone else with similar experiences ?\n\nms."
    author: "manohar singh"
  - subject: "Re: is there a KDE  howto for solaris 8 anywhere ?"
    date: 2002-05-17
    body: "Hi,\n\ni have got exactly the same problems with kde and my machine shows the same symptoms. But till now I have no idea, what to do else.\nDuring installation everything ran without error message.\nIf you have got any solution in meantime, please let me know.\n\nMw"
    author: "Markus"
  - subject: "Re: is there a KDE  howto for solaris 8 anywhere ?"
    date: 2002-05-22
    body: "http://xdroop.dhs.org/content/984496082.html"
    author: "Ray"
  - subject: "Great Job but..."
    date: 2002-01-02
    body: "This is a great job accomplished, but I have a problem with both KNOFITY and NOATUN. Both of them are crashing... Beside this everything is running smooth."
    author: "Michel Poirier"
  - subject: "Re: Great Job but..."
    date: 2002-01-10
    body: "Did you relocate the install directories?  Noatun works fine if everything is installed exactly as described in the readmes, but if you relocate a directory, it breaks."
    author: "Len Laughidge"
---
Good news for many
<a href="http://www.freebsd.org/">FreeBSD</a> and
<a href="http://www.sun.com/software/solaris/">Sun Solaris</a> users:
you can now get a precompiled KDE 2.2.2 from the
<a href="http://download.kde.org/stable/2.2.2/">normal</a> locations.
The <a href="http://download.kde.org/stable/2.2.2/FreeBSD/Al">FreeBSD
packages</a> are
<a href="http://download.kde.org/stable/2.2.2/FreeBSD/README">said</a>
to work only with 4.4 Stable on Intel and compatible platforms.
The Sun packages come in two flavors:
<a href="http://download.kde.org/stable/2.2.2/Solaris/Sparc/">Sparc
packages</a>, which
<a href="http://download.kde.org/stable/2.2.2/Solaris/Sparc/README.install">reportedly</a>
have been tested only on Solaris 8, and
<a href="http://download.kde.org/stable/2.2.2/Solaris/X86/">Intel and
compatible packages</a>, which presumably is only for Solaris 8.
FreeBSD users may also want to check out the new
<a href="http://freebsd.kde.org/">FreeBSD.KDE.org</a> website.
The <strong><a href="http://promo.kde.org/kde_systems.php">KDE 2.x
Systems</a></strong> page has been updated with these latest releases;
if you are aware of any omissions, please follow the instructions on the page for notifying the maintainer.

<!--break-->
