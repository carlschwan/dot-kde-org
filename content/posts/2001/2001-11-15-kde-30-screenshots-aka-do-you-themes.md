---
title: "KDE 3.0 Screenshots (A/K/A, Do You Like Themes?)"
date:    2001-11-15
authors:
  - "Dre"
slug:    kde-30-screenshots-aka-do-you-themes
comments:
  - subject: "Lovely :)"
    date: 2001-11-15
    body: "Stuff like this is candy for guys like me that are far away from their main pc (currently in the military for the year) and that haven't been able to try out kde3 yet..\n\nBut... Couldn't we get some more pictures of more of the new features (I know there isn't many) in KDE3 ?  dotNet, the iKons and Koncd can be run on kde2 too.. So it's really not that KDE3 specific.."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Lovely :)"
    date: 2001-11-15
    body: "I'm tried kde3-cvs from the 11/03/2001.\nThere are some apps like koncd, cervisia, kamera, ... that are included in this version. Also the multimedia part is improved but i didn't saw big changes in the UI part or the html render in konqueror :(  Some websites are working great with Mozilla/Galeon but still crap with konqui :(\nBut I think that the real THING in kde3 is QT-3 which is very very nice.\n\nJean-Christophe\n\nPS : I don't for others, but kde3 still slow on my machine. PIII 600 with 380Mo ram and Ultra160 disks ...."
    author: "Jean-Christophe Fargette"
  - subject: "Nothing new to _see_?"
    date: 2001-11-15
    body: "The big problem with KDE 3.0 in the users eyes will be that there will be no radical UI changes like there was from 1.0->2.0.  The cool stuff is in the murky depths of KDE libs and in the back waters of KDE base.  What we need is a way to animate some demos of things like printing, script automation, better JS support, accessing play objects via dcop, new text editing engine, graphical regular expression editor, smart card support, total rewrite of SSL and friends, and tons of other little tweaks.  \n\nMaby we should try not to got the slushdot route and post pretty screen shots but spend a day showing off how awesome the new JS engine is?  Or how the new dcop interfaces will help automation.  Personally I really want to see Waldo's \"turn off desktop\" feature in action...  I know it is hard to do these things without pictures but I have faith in our people here at the dot :)\n\nSo I hope I did not offend any the the theme and style guys, but really lets talk about what is beneath the surface of KDE. I am sure 3.1 will look much prettier, but there will be less radical library changes.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Nothing new to _see_?"
    date: 2001-11-15
    body: "I feel the same - Let's start tweaking!"
    author: "Matthias Fenner"
  - subject: "Re: Nothing new to _see_?"
    date: 2001-11-15
    body: "How about a few animated GIFs to help demonstrate the new features in action?"
    author: "nrp"
  - subject: "Re: Nothing new to _see_?"
    date: 2001-11-22
    body: "http://www.linux-mandrake.com/en/demos/\n\nMandrake has put together a fairly effective way of presenting features through a series of animated gifs.  Are there other similar sites that demonstrate a sequence of events to provide a strong end-user feel for a set of features?  Please provide links if you find something..."
    author: "Brutus"
  - subject: "Re: Nothing new to _see_?"
    date: 2001-11-15
    body: "Well, maybe someone needs to write a demo that uses JavaScript and DCOP to script the new text editor with user-editable regular expressions while playing sounds and opening a Konqeror window on a SSL site with smart card authentication and printing the page :-)"
    author: "not me"
  - subject: "Re: Nothing new to _see_?"
    date: 2001-11-16
    body: "Heee sorry but KDE isent pretty Stylieeee!\n\nKDE has a lot of good hackers but not Stylers. THat is what i missing on KDE a good default Style. Like Gnome or Aqua. HOW helps me a new SSL or libs for a UI my eyes like to see wonderful, beautiful and Stylieee buttons - icons and not new SSL.\n\nThomas \n\nsorry for my english"
    author: "Thomas"
  - subject: "Re: Nothing new to _see_?"
    date: 2001-11-16
    body: "Have a look around kde-look.org\nThere are many good styles there - QNix and dotNet are only two"
    author: "."
  - subject: "Re: Nothing new to _see_?"
    date: 2001-11-16
    body: "Aqua is hideous.  So is gtk's default look.  Even the best gtk themes don't look as good as the Hicolor KDE Default.  Make your own styles if you can't find any you like, don't just bitch."
    author: "KDE User"
  - subject: "Re: Nothing new to _see_?"
    date: 2001-11-16
    body: "Talking about the \"in depth\" stuff. What about kerberosV and acl support ?\nWell, and LDAP support for the system addressbook ?\n\nAnd, finally, what about \"Group Policies\" (sorry for that windows terms) - but, to restrict the desktop for certain groups (network wide) ?\nLike disallowing to view dotted files or change theme for that \"corporate identity\" stuff. \n\nWhat about abstracting konqueror a bit from the file system ? While KDE using \"Unix Gurus\" will hate me for that, but, a secretary does not need to know about /etc, /bin, etc. It would be enough for her to see her home share and the group shares she may access. Would be much less confusing. Take away the konsole from her group and thats it. Not for security, just for \"useability\"."
    author: "Fretto"
  - subject: "Re: Nothing new to _see_?"
    date: 2001-11-16
    body: "You can already change Konqueror to do just that. For instance, you could make a view profile that automagically opens the dir 'my documents' - or whatever you want to call it. On the left side, you can build a sidebar that only shows 'my documents', 'my pictures' and 'my networkfiles'. Every program that you want to be available is added to the group 'secretary'. Place some clear links on his/her desktop, let him/her choose from a set of predefined wallpapers / styles and everyone is happy ;) I even dare to say that it would be easier to do in GNU/Linux / KDE than with some other large OS."
    author: "Jonathan Brugge"
  - subject: "Re: Nothing new to _see_?"
    date: 2001-11-16
    body: "the aegypten project is working on an LDAP kioslave for use within KMail and elsewhere.\n\nsomeone already mentioned that it is possible to make konqueror show only certain folders. in the new Sidebar there is one panel that shows only the home dir. since 98% of the time when using konqi as a file manager i am in my own files (i use the command line otherwise, usually) i have it showing only that panel. that may even be the default, i'm not sure."
    author: "Aaron J. Seigo"
  - subject: "Re: Nothing new to _see_?"
    date: 2001-11-17
    body: "I totally agree.\nAlthough the slightly behind the scenes changes going on in KDE 3.0 are excelent and are the driving force behind KDE, I think it would be great to show how KDE 3 makes it so much easier to get things done in a better quiker way.\n\nI think its good how KDE 3 is taking the opposite route to say windows XP which is a classic case of basically all the changes being in the UI area and very little changed behind the scenes.\n\nPersonally I would like to see screenshots of task automation, flexability, console replacing dialog boxes and the ability to easily customize everything from icons to scren resolution."
    author: "Malcolm"
  - subject: "Menus..."
    date: 2001-11-15
    body: "Can anyone tell what is the name of the new stripe on menu (widget name) in KDE 3.0? Is it possible to theme it as any other widget?"
    author: "anonymous"
  - subject: "alpha blending"
    date: 2001-11-15
    body: "Isn't there any sexy alpha-blending shots ?"
    author: "delpinux"
  - subject: "Re: alpha blending"
    date: 2001-11-15
    body: "x windows does no suport alpha bledering zo kde does not suport alpha blendering. There is work to suport alpha belndering in xfree and when that is ready kde wil have alpha blendering suport"
    author: "Underground"
  - subject: "Re: alpha blending"
    date: 2001-11-15
    body: "x windows does no suport alpha bledering zo kde does not suport alpha blendering. There is work to suport alpha belndering in xfree and when that is ready kde wil have alpha blendering suport"
    author: "Underground"
  - subject: "Re: alpha blending"
    date: 2001-11-15
    body: "This is not true. XFree provides Alpha Blending with the XRENDER extension. Because of this we were able to drop Software AlphaBlending via KAlphaPainter, Qt and X do it all for us :))\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: alpha blending"
    date: 2001-11-15
    body: "Nice!  Does this mean that pixmaps with alpha channels can be used anywhere in KDE now?"
    author: "not me"
  - subject: "Re: alpha blending"
    date: 2001-11-15
    body: "x windows does no suport alpha bledering zo kde does not suport alpha blendering. There is work to suport alpha belndering in xfree and when that is ready kde wil have alpha blendering suport"
    author: "Underground"
  - subject: "This is good"
    date: 2001-11-15
    body: "KDE 3.0 screenshtos are nice, but the release date for KDE 2.2.2 was set for MONDAY!  When can we hope/expect to see it come out ?"
    author: "Anonymous Coward"
  - subject: "Re: This is good"
    date: 2001-11-15
    body: "Slightly off topic but given the above message, can anyone give an update of when KDE 2.2.2 will be released?"
    author: "Andy Cheung"
  - subject: "Re: This is good"
    date: 2001-11-15
    body: "FWIW - KDE_2_2_2_RELEASE has been available in CVS for at least a few days...\n\nI've been updating my trees for a couple of days just to be sure... but it seems to be more or less frozen (practically speaking)"
    author: "jason byrne"
  - subject: "Re: This is good"
    date: 2001-11-16
    body: "It was released on Monday. Now, distributors will make packages, and when they do, there will be an official release annoncement, and postings to here, slashdot, linuxtoday, etc.. and the crowds will be merry and all will be well."
    author: "off"
  - subject: "Re: This is good"
    date: 2001-11-16
    body: "ok so where can we get the tars?  i'm in the process of building 2.2.1, and would love to scrap it and just get/build 2.2.2.  i checked ftp.kde.org, and there's a 2.2.2 directory, but nothing's there.???"
    author: "Mark"
  - subject: "the web theme rocks"
    date: 2001-11-15
    body: "Along with the web window decoration, that's the one I use most.  A very nice clean style.  The default KDE2 style is nice too, and the B2 style kicks ass!!!"
    author: "Tim"
  - subject: "Window decorations"
    date: 2001-11-15
    body: "Most of window decorations are ugly, especially window buttons ( maybe with exception of ModSystem and Laptop ). I see that the default wm decoration is still the same as in KDE 2.1 with the ugliest buttons of all, so I would like to know if this is only my opinion - Is there somebody who likes KDE window decorations ? Why  KDE2 decoration was choosen as a default one ?"
    author: "Me"
  - subject: "Re: Window decorations"
    date: 2001-11-15
    body: "Nothing has been chosen yet.  KDE 3 isn't due until February!  There's at least one new window decoration in CVS now (glow, it's animated!  Really cool :-), and of course you can use any IceWM theme you want.  I'll be surprised if kbox doesn't make it in eventually also (at kde-look.org).  There's plenty of time to change the default style, I don't think that the artists are affected by the freeze as much."
    author: "not me"
  - subject: "Re: Window decorations"
    date: 2001-11-16
    body: "I'd be extremely happy if QNix (my favorite window style / widget style) and iKons (my favorite icon theme) made it into KDE cvs... helps me to get away from the MacOS X-ing that everyone else seems to want these days.\n\nGlow sounds interesting... interesting enough to make me check out the CVS just to see it :) Pity I'm going away for the weekend, really..."
    author: "Jon"
  - subject: "Re: Window decorations"
    date: 2001-11-15
    body: "I don't like the default decoration, too. I always use the laptop style - even with liquid."
    author: "Hey"
  - subject: "Re: Window decorations"
    date: 2001-11-16
    body: "ModSystem and Laptop are my favorite too.  But System++, Quartz, Kwix are pretty good too.  I think I had to install Quartz and Kwix from kde-look.org though"
    author: "Me2"
  - subject: "Re: Window decorations"
    date: 2001-11-16
    body: "yes, System++ with BeOS colors and \"melting glass\" background is absolutely marvelous\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Window decorations"
    date: 2004-02-06
    body: "i can't found system++ on kde-3.2. Any hint?\n\nThank you."
    author: "jj"
  - subject: "Re: Window decorations"
    date: 2001-11-16
    body: "The Laptop decoration is not the most beautiful one, but it is very clear and useable. One suggestion for the other window decorations: It would be great to have a (global?) option for defining the button order in the title bar. I really prefer to have the close button on the left-hand-side of the window, isolated from the other buttons, but that is possible only with the Laptop decorations."
    author: "Oliver Strutynski"
  - subject: "Re: Window decorations"
    date: 2001-11-16
    body: "With the default KDE decorations, you can set the locations of all the buttons in kcontrol.. I have my close button on the left side too (Amiga-style. ;)), and I have turned off the sticky and menu buttons as I feel they have no use at all.  I think all decorations should have these settings, wonder why they don't... \n\n(I _loathe_  the menu-button. ;))"
    author: "Johnny Andersson"
  - subject: "Re: Window decorations"
    date: 2001-11-17
    body: "Thanks so much. I never noticed that checkbox before."
    author: "Oliver Strutynski"
  - subject: "Re: Window decorations"
    date: 2001-11-17
    body: "most of the major third party window decors have button editing support, at least Qnix, Liquid, and kbox do. Im not sure about Kwix."
    author: "axe"
  - subject: "Re: Window decorations"
    date: 2001-11-21
    body: "Would be nice if window frames would dissapear when maximizing"
    author: "Pyre"
  - subject: "third party skins"
    date: 2001-11-16
    body: "any plans for QNix or kbox to have kde 3 support?"
    author: "man"
  - subject: "Re: third party skins"
    date: 2001-11-16
    body: "KBox compiles for KDE3 with a single line change, and it mostly works, except that changing the settings crashes KWin... But it runs in general just fine."
    author: "A Sad Person"
  - subject: "Re: third party skins"
    date: 2002-09-22
    body: "Yeah, I'll do a port of kbox to kde3 soon.. fixing a lot of bugs in the process (mainly memory leaks resulting from not destroying generated pixmaps.)\n\nI'll also probably add metacity theme support to it. Not sure if I'll name it kbox or something else. I think some metacity themes look cool, and I'd love to use them in kde3."
    author: "fault"
  - subject: "Re: third party skins"
    date: 2002-09-22
    body: "Great, now if only the GTK themes would work again in KDE. And support for GTK2 themes added."
    author: "Anton"
  - subject: "And the taksbar ?"
    date: 2001-11-16
    body: "KDE can look really nice - but one thing. The taskbar is _always_ ugly. Isn\u00b4t there sidebaranything to do about this one ? \nMaybe _alternatively_ like the  of Aethera in this shot ? \nhttp://www.thekompany.com/projects/aethera/images/aethera-contacts.png\n\nHaving this slight \"3D effekt\" and I do like the round buttons.  Or whatever. But no so boring and conserative as it is now.\n\nP.S.: I definately would love to see a middle sized version of the \"normal\" sized and the the \"small\" sized taskbar. Latter one is too small, while the \"normal\" one is a bit too big. For my taste, that is."
    author: "Fretto"
  - subject: "Re: And the taksbar ?"
    date: 2001-11-16
    body: "check out:\nhttp://www.kde-look.org/content/show.php?content=289&PHPSESSID=ae0d77ae3c77ecd2fb52d248e01d78d3\n\n(or directly http://www.kde-look.org/content/files/289-kicker_back.png)\n\nand use it as a background for kicker\n\nit is downright beautiful :O)"
    author: "emmanuel"
  - subject: "Re: And the taksbar ?"
    date: 2001-11-16
    body: "not bad at all... It may not be sufficiently generic as a standard KDE taskbar, but it should provide som inspiration! \n\nI agree: if they gave a new design to the \"start\" button and the taskbar, it would be all that is needed as a facelift for KDE 3.0..."
    author: "will"
  - subject: "What font is that?"
    date: 2001-11-16
    body: "Can someone tell me what font is being used for the menu in in snapshot1...\n\nthnx\n\nppGG;;"
    author: "puppydogg"
  - subject: "Re: What font is that?"
    date: 2001-11-25
    body: "Probably Verdana, I didn't switch to Tahoma yet when I made the screenshots."
    author: "Rob Kaper"
  - subject: "kde3 for windows ?"
    date: 2001-11-16
    body: "kde3 seems really great ...all the underwoods change are welcome.\n\nbut does kde3 will run under windows ? (as QT3 does)"
    author: "anonymous"
  - subject: "Re: kde3 for windows ?"
    date: 2001-11-16
    body: "check with cygwin. there's a kde 1.x port which seems to run under windows. i haven't tried it. there maybe a day when you can replace you windows desktop, but it's not quite there yet. under cygwin i think you run a kde on your desktop."
    author: "Mark"
  - subject: "Re: kde3 for windows ?"
    date: 2001-11-17
    body: "yep i known that but normaly as qT3 is source compatible on windows and linux ... so we should be able to compile and run kde3 application.\n\nperhaps not core function like arts, direct X11 call ...but why not for knotes,konqueror,kedit etc ...\n\n????"
    author: "anonymous"
  - subject: "missing eye candy"
    date: 2001-11-16
    body: "One of the best eye candy things I've every seen is <a href=\"http://www.geisswerks.com/drempels/\">drempels desktop</a>. Pity it's only available for doze:( For those that haven't seen it, the sw does screen-saver type wibbly-wobbly effects to your backdrop behind your windows. It's bloody amazing, and a hell of a cycle waster! I wonder how long the static desktop will last now...\n\nAny X gurus out there than can replicate it?"
    author: "tonyl"
  - subject: "Re: missing eye candy"
    date: 2001-11-16
    body: "Some of the mode are actually just a palette shift - a friend has noticed that some imgaes that he brings up have the same colors, and they rotate through as well.  So it's not much of a cycle waster at all... but also can conflict with your normal windows. \n\nThere are \"stretch and squeeze tile\" modes, but I've seen demos (fromthe assembly demoscene) that optimize those down to really really fast and tight code.  Maybe that would be a good starting point?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: missing eye candy"
    date: 2001-11-16
    body: "The trouble with palette shifting is that it doesn't work unless there's a palette. This rules out most users these days as pretty much everyone uses true color.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: missing eye candy"
    date: 2001-11-19
    body: "This Drempels thing runs under true color.  It certainly swirls and morphs a lot, and you can't do that with palette shifting.  I don't actually think it uses palette shifting at all.  It takes a picture, tiles it, then morphs it around and swirls it.  Occasionally it fades in a new picture."
    author: "not me"
  - subject: "Re: missing eye candy"
    date: 2001-11-19
    body: ":: I don't actually think it uses palette shifting at all.\n\n    Yes it does.  It does other things as well, but constant palette rotation is one of the things it does as part of the animation process.  Certain colors will show it - a friend discovered a few pictures he had that contained the colors used, and when loaded in a viewer with Drempels running, they rotated through the colors used in the background.  Showing that he has no taste, he then proceeded to make a theme where the window colors were made up of those colors, resulting in a disgusting, throbbing desktop.  :)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: missing eye candy"
    date: 2001-11-20
    body: "I think that's actually just a side-effect of the way you use DirectDraw to write a window on the desktop.  It actually writes a pink box to the screen, and then anything in that box can be blitted with hardware tricks in DirectX instead of going through the regular windows window display libraries."
    author: "Ranger Rick"
  - subject: "Re: missing eye candy"
    date: 2001-11-20
    body: "That's not palette shifting, that's overlay mode.  By some weird trick Drempels (and winamp now too) \"takes over\" one color on the screen.  Wherever this color is shown on the screen, each frame it is replaced with the background.  So if you set your window background to this color, the swirling patterns will be drawn on top of them each frame, which is trippy.  It's not palette shifting though.  Palette shifting is where you take an image and rotate all of its colors in a circular manner.  It doesn't produce images that change, it just shifts the color of sections of the picture.  So if Drempels was using palette shifting, you wouldn't see the patterns through the windows, the windows would just be a different solid color every frame.  Plus the images wouldn't swirl in the background, they would be static, with only the colors changing."
    author: "not me"
  - subject: "Re: missing eye candy"
    date: 2001-11-20
    body: "(this is also a reply to Ranger Rick)\n\n:: That's not palette shifting, that's overlay mode. \n\n    I had thought that as well, especially considering that there is bitmap warping going on at the same time.  It may be, but I thought, looking at the image, that the effect wasn't \"through\" the image, but rather hitting different, very close colors in sequence.\n\n:: Plus the images wouldn't swirl in the background, they would be static, with only the colors changing.\n\n    There are some routines for smooth animation on slow machines that involves painting four frames for the animation, generating the next frame as you palette rotate though to animate the current, onscreen frame, and then display the next frame.  Things like this are used in assembly demos, where \"impossible\" things are shown in real time.  That's why I thought that there was palette rotation going on as part of Drempels.\n\n    BUT - all of this is based on my shoulder surfing someone else's machine.  I would not be surprised if I was incorrect.  And an easy way to test it would be to see if you can play a DVD at the same time (on a video card you know only has one overlay channel - I belive I was using ATI when I found that limitation).\n\n--\nevan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: missing eye candy"
    date: 2001-11-21
    body: "In the Drempels control panel it says it uses overlay mode and allows you to choose the overlay color.  So I'm pretty sure it doesn't use any palette shifting.\n\nI'd never heard about that assembly demo stuff, that's interesting."
    author: "not me"
  - subject: "Re: missing eye candy"
    date: 2001-11-21
    body: "Oh, then you're missing out.  =)\n\nhttp://www.scene.org/\n\nOr, for linux-specific demos (still a pretty young field): http://www.lnxscene.org/"
    author: "Ranger Rick"
  - subject: "Re: missing eye candy"
    date: 2001-11-21
    body: "Would be nice if some of these demo hackers would turn their demos into screensavers.\n\nAlex"
    author: "aleXXX"
  - subject: "Re: missing eye candy"
    date: 2001-11-16
    body: "just checked this out. you are so right. this thing ROCKS.  makes me want to keep my app windows smaller and smaller just so i can see the background effect.  something like this for kde would really, really ROCK!"
    author: "Mark"
  - subject: "Re: missing eye candy"
    date: 2001-11-16
    body: "You can do various things to your root window in X, like run any of the XScreensaver hacks there.  Then you can't use your desktop icons though.  I'd be surprised if anyone could come up with something like this drempels thing.  Full-screen, swirling, morphing colors at about 30 fps or more, and it only takes 35% CPU for the whole thing on my P3 500.  Amazing.  X will never be able to replicate that until we all have 20 GHz chips."
    author: "not me"
  - subject: "Re: missing eye candy"
    date: 2001-11-17
    body: "This is ugly... I'd never want anything of the kind on my desktop. The whole idea just looks annoying. It may be nice to watch for a minute or two, but that's it."
    author: "shumway"
  - subject: "Botton Taskbar"
    date: 2001-11-16
    body: "I\u00b4d like to see improved the bottom taskbar.\n\nMostly the icons, the look&feel, the style; I think that it can be improved significantly.\n\nThe rest of KDE is SUPERB !\nBest regards & good luck."
    author: "Jorge Carminati"
  - subject: "KDE 3 will be faster ?"
    date: 2001-11-17
    body: "KDE 2 is a memory consumer!\nand KDE 3 willbe too ?\nWhat are you doing to KDE conume less memory and load very fast ?"
    author: "Carlos Marcello Dias Fernandes"
  - subject: "Re: KDE 3 will be faster ?"
    date: 2001-11-17
    body: "In case you haven't noticed, KDE has continually, with each release, been using LESS ram per feature quota. KDE 3 will follow this pattern."
    author: "Carbon"
  - subject: "Re: KDE 3 will be faster ?"
    date: 2001-11-17
    body: "that wasn't true with kde 1.x -> kde 2.x."
    author: "axe"
  - subject: "Re: KDE 3 will be faster ?"
    date: 2001-11-17
    body: "Yes it was. Notice i said \"per feature quota\". What i mean by that is, for any given amount of features added, ther's the same or better ratio between speed and features. Since kde2 added aRts, konqueror, kicker applets, and all sorts of other nifty things, for realitvely little speed decrease, it was an improvement. Also, kde 2 was a complete rewrite. kde 3 is more of a to-library port, so many of the optimized apps of kde 2 won't have to be redone from scratch."
    author: "Carbon"
  - subject: "Re: KDE 3 will be faster ?"
    date: 2001-11-17
    body: "You can always install Debian. The speed difference is huge compared to Mandrake."
    author: "Tellus"
  - subject: "liquid"
    date: 2001-11-17
    body: "Who votes to include Liquid theme as the default KDE 3.0 theme? or include it in the KDE CVS? it would be fantastic!"
    author: "daniel"
  - subject: "Re: liquid"
    date: 2001-11-17
    body: "liquid rocks, but it certainly doesn't run with kde3, due to a different style api (as far as i can understand).\n\nand the problem of incompatible license comes to mind...\na modified qpl won't suit kde really well.\n\nbut liquid rocks, so kidnap mosfet, let him (or her?) change the license, and force him/her to work in kde cvs tree ;)\n\n\n\njust kidding, mosfet!\n (or not ?)"
    author: "tooobi"
  - subject: "Re: liquid"
    date: 2001-11-17
    body: "No way. Liquid style doesn't like some graphic cards, and it would be shame to get black and grey boxes all around your desktop instead of Liquid style.\nSee attached screenshot to see combination of ProSavage graphic card & Liquid\n(this is compiled Liquid 0.7)."
    author: "antialias"
  - subject: "Re: liquid"
    date: 2001-11-18
    body: "I have got a Savage IX and my liquid looks as ugly as yours. I'm not sure if this is a problem of the graphic card. When I run the fbdev driver in Xfree 4.1.0 the problem (and the speed :-( ) are gone.\n\nPerhaps we should drop a mail to the developer of the s3 driver?"
    author: "Hey"
  - subject: "Re: liquid"
    date: 2001-11-18
    body: "I don't think it is a problem with your graphics card.  Rather, I think the problem is the version of QT or KDE you are using.  Older versions of QT and/or KDE won't work well at all with Liquid.  What version of QT and KDE are you running?"
    author: "not me"
  - subject: "Re: liquid"
    date: 2001-11-18
    body: "I used liquid 0.5 and 0.6 on KDE 2.2.1\n\nI think the driver of the graphic card is the problem. When I use the framebuffer driver, liquid works well."
    author: "Hey"
  - subject: "Re: liquid"
    date: 2001-11-19
    body: "If you've got a Savage S3 graphics card it's a problem with the driver. It is broken and cannot handle the X11ParentRelative background mode. That's why it works fine when using the framebuffer device, or any other driver that I know of ;-) Personally I test it on ATI Rage and AT3D video cards."
    author: "Mosfet"
  - subject: "Re: liquid"
    date: 2001-11-19
    body: "You are right Mosfet, I have had a lot of problems with this graphic card, and I am going to change it as soon as possible.                                    \nRegards,                                                                        \n                                                                                \nantialias"
    author: "antialias"
  - subject: "Re: liquid"
    date: 2001-11-19
    body: "You may want to try building XFree86 CVS and see if it helps. Looking at the Changelog I do see some updates and bugfixes for the Savage driver, but I'm not sure what it fixes. Building X can be a chore, but if you got a driver that has bugs sometimes it's worth it. While your at it you can always email your card manufacturer and tell them to give more resources to the XFree development team so they can support cards faster ;-)"
    author: "Mosfet"
  - subject: "Re: liquid"
    date: 2001-11-20
    body: "I cannot change the card, because it is in my built in my thinkpad :-(\n\nIf you nedd the latest drivers for the card, go to \n\nhttp://www.probo.com/timr/savage40.html\n\nwww.s3graphics.com also provides some drivers.\n\nI will mail to the developer of the driver and ask him fpr a proper \"liquid\"-support\n\ngreez\nAdrian"
    author: "Hey"
  - subject: "Re: liquid"
    date: 2001-11-17
    body: "Who votes to include Liquid theme as the default KDE 3.0 theme? \n\nPeople have different tastes.  I personally would like to note that I absolutely hate and despise the Liquid theme.  And I know I am very much not alone.  Since the \"average\" user (who is now starting to use KDE) never figures out how to change styles, I would seriously suggest such a \"love it or hate it\" style *NOT* be selected as the default style - if a distro wants to make Liquid the default, they are free to.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: liquid"
    date: 2001-11-18
    body: "Well said."
    author: "Alain"
  - subject: "Re: liquid"
    date: 2001-11-19
    body: "People are going to \"love or hate\" any UI style. People bitch about Liquid, KDE's default style (which I also designed), MS Windows 95 & XP, Aqua, MaxOS9x, GTK, Amiga, NextStep, etc... You never can satisify all the people all the time. All one can do is pretty much ignore people who \"hate and despise\" your work if it doesn't match their taste ;-) Considering the user base that has grown around Liquid, and the positive comments about it all over the place, evidently someone likes it :)\n\nDon't worry tho. I don't have any intention to try develop anything in CVS again, so you don't have to worry about anything I do being put in there. That's what the QPL is for..."
    author: "Mosfet"
  - subject: "Re: liquid"
    date: 2001-11-19
    body: "Dont say it please! .. we all want you return to the KDE CVS! you are one of the best hackers here in the KDE world team"
    author: "daniel"
  - subject: "Hey Mosfet"
    date: 2001-11-21
    body: "I have been an admirer of yours for you being such a nice programmer. But I have observed that you get annoyed very easily; and that's not good for you and us KDE users ;), you know we like your work. Especially I like QTWindows Style, but I use Liquid for its performance. Not everybody likes J Bush or Al Gore for that matter. You should be open for criticism and please don't get hurt so easily. Thanks anyway... but I wish your work's license starts with a 'G' instead of a 'Q' ;)"
    author: "Asif Ali Rizwaan"
  - subject: "They must make an effort but"
    date: 2001-11-18
    body: "They must make an effort but, this version 3 is looked like kde2 seems that they must pornele but enfasis to the styles and showy things, if they wish you win to Windows XP must much be but elegant this version 3 follows the example of Liquid \nI use kde2.2.1\n\nGood Luck!"
    author: "Omar"
  - subject: "Who cares about the wrapper!"
    date: 2001-11-18
    body: "The internals of KDE are what matter.  It seems that the KDE team are being caught up in the M$ marketing maneuver.  Produce a product that looks better than it functions.  The most important parts are functionality, stability and performance.  These are the cornerstones of UNIX programming and should not be ignored.\n\nRegards...\n\nMicrosoft: \"Give them piss and tell them its wine.\""
    author: "BOB"
  - subject: "Re: Who cares about the wrapper!"
    date: 2001-11-18
    body: "Would you buy a house, when ist very functional but looks ugly?"
    author: "Hey"
  - subject: "Re: Who cares about the wrapper!"
    date: 2001-11-19
    body: "By a similar token, would you buy a house that was a pain to live with everyday, even if it looked really nice?"
    author: "Rayiner Hashem"
  - subject: "Re: Who cares about the wrapper!"
    date: 2001-11-19
    body: "Okay, okay... both of you - it's all important.  A good desktop feels nice, looks nice and works nice.  Okay?  The look *is* important, as is stability, consistancy, available help, and several other aspects of the total experience.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Who cares about the wrapper!"
    date: 2001-11-19
    body: "> By a similar token, would you buy a house that was a pain to live with everyday, even if it looked really nice?\n\nBOB implied that internals is sufficient, while Heys comment can be taken to mean that looks are neccesary. That does not mean that Hey said that looks is sufficient.  ;)"
    author: "will"
  - subject: "Re: Who cares about the wrapper!"
    date: 2001-11-20
    body: "Actually, I know some people who are BUILDING a house that will be a pain to live in, a horror to their kids due to lack of space, but the reason they've built it like that is because it looks impressive.\n\nEveryone BUT hackers will pick looks over content, and sometimes, even hackers do that. It's why the world is ruled by idiots, and ugly people are despised around the globe."
    author: "Joeri Sebrechts"
  - subject: "big k-menu button!"
    date: 2001-11-19
    body: "plz! make the k-menu button larger ... the width should be larger then normal icons on the menu."
    author: "Andreas"
  - subject: "Re: big k-menu button!"
    date: 2001-11-19
    body: "why?"
    author: "not me"
  - subject: "Re: big k-menu button!"
    date: 2001-11-19
    body: "I actually kindof agree with this. It's not always evident to new users that the \"K\" button is where all your applications are. Sure, it may seem evident to us that the K with a gear, (which represents executables), aligned to the left where the start menu usually is at would represent the application menu. But brand spanking new, computer illiterate users shouldn't be expected to know this. Windows at least has the \"Start\" text on the button. I'm not saying we should do that, but I think doing something to identify the button as special would be a good idea. The arrow isn't really enough. Same thing applies to Gnome and it's foot, I guess."
    author: "Mosfet"
  - subject: "Re: big k-menu button!"
    date: 2001-11-19
    body: "yes gnome do the same error if you ask me.. for a gnome user and a kde user it dosnt make any diffrens. But for new users it should be obvious with button they should use. No learning by doing wrong here!! .."
    author: "Andreas"
  - subject: "Horizontally big k-menu button!"
    date: 2001-11-20
    body: "I have been bothering Kicker developers for a KStart button, I Draw an image, the style is redmond but the redmond style do not has any effect on the tasbar buttons :(, Please see this \"Wish-Kicker.png\", I would love to see \"Text-along-with-an-image\", so that I could have many other names like \"[tux-image]- Linux\", \"[Mosfet]- Liquid\" ;). Isn't it possible?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Horizontally big k-menu button!"
    date: 2001-11-20
    body: "something like that is what i'm looking for!"
    author: "Andreas"
  - subject: "Re: Horizontally big k-menu button!"
    date: 2001-11-21
    body: "That is an unbelievably blatant Windows ripoff.  Do you think KDE could ever implement something like that and keep a straight face?\n\nI'm sure you can come up with something more original.  Besides, that look is old now anyway.  Windows XP is the new wave.\n\nI think Kicker should have a nice themed look, as long as it is a unique theme and not some XP ripoff.  And like someone said above, merely redesigining kicker would be enough for KDE3 to completely stand out."
    author: "Justin"
  - subject: "Re: big k-menu button!"
    date: 2001-11-20
    body: "> It's not always evident to new users that the \"K\" button is where all your applications are.\n\nI think it is evident for anybody already using Windows. It is the first button... I have never seen a new user searching the start button...\n\nThe interest of a big K button is that it is easier to click on a big button...\n\nYou know, it is easy to have the K button as a big button, and the others as small ones. You give the width of two small buttons to the Kicker bar, you let the K button as a large icon, and you put all the other icons in the start applet with little icons... This start applet is a very interesting thing (the task applet too).\n\nHere, I only have one problem : there is the Desktop icon that I cannot (easily) put in the Start applet, so it is also a big button..."
    author: "Alain"
  - subject: "Re: big k-menu button!"
    date: 2001-11-20
    body: "Not everyone has used Windows, and knowing how to use Windows shouldn't be a requirement for KDE ;-) My Mom wouldn't know what the \"K\" button means, and she uses Windows every day anyways. She would be looking for a start button."
    author: "Mosfet"
  - subject: "Re: big k-menu button!"
    date: 2001-11-20
    body: "In the GNOME usability study a while back, it was specifically reported that the users had problems with guessing what their start icon would do. It must be the same problem with KDE. Under any circumstances, relying on knowledge about another operating system (and letting it take the job of usability) has to be bad design."
    author: "will"
  - subject: "Re: big k-menu button!"
    date: 2001-11-21
    body: "The main icon is in the first place, it's easy to understand. There is few place on the Kicker bar and it is a trouble to reduce this place with some useless thing."
    author: "Alain"
  - subject: "Re: big k-menu button!"
    date: 2002-04-15
    body: "I could not agree more, (K) or (foot) is just silly, we should have something more evident! perhaps A switch or ignision key? Never the less there are a few things in K and Gnome which do not make sense to new users, and for that matter old users!"
    author: "Alphamale"
  - subject: "Re: big k-menu button!"
    date: 2003-09-17
    body: "Well I am not a newbie and I know what the K menu button does very well.\nHowever, I do not agree at all that adding more pixels to the K-menu button would undermine all that KDE is. I see this as a matter of user preference, not as some profound matter of GNU vs MS philosophy <g>. It's dissapointing that this hasn't been added in all this time because of a fear resembling windows. \n\nPersonally i use linux because i like what's behing the GUI, the OS. Adding pixels to the K button takes nothing away from that.. \n"
    author: "ed"
  - subject: "Re: big k-menu button!"
    date: 2003-12-15
    body: "hey listen i have spent many hours with mandrake 9.2 and installed the sparkling kde 3.3 they evidently skipped 3.2 for some reason it was around but... well anyway they have solved the problem it doesnt say start like in winxp as soon as you start you get the kde menu up button depressed and when you press the windows button instead of the menu near the mouse it is where the button is and it is larger compared to the other ones and it says when you move your mouse over it KDE really really cool but this damned thing is so buggy it is almost crippled well back to the drawing boards for this x-windows but looks promising"
    author: "christopher"
  - subject: "Re: big k-menu button!"
    date: 2001-11-20
    body: "Anyone can just add  a second \"K\" button, right next to the first one\nif they're so clumsy with the mouse.\nIt is also not too much to learn rhat that button is where the programs are\nSo NO don't make it bigger."
    author: "me"
  - subject: "Re: big k-menu button!"
    date: 2007-03-19
    body: "How about giving users the option to make it bigger or leave it small.  A configuration option would do this.  That way each person could decide for themselves."
    author: "Cerephim"
  - subject: "Re: big k-menu button!"
    date: 2001-11-19
    body: "I do agree here.\nWhy not a vertical text instead of the arrow?\nSomething like menu would give the idea of exataly it is.\nAnd for the size... when using little kicker panel it really should be 22*32 or 16*32. Why? Okay, think about this, you will click on this button a lot, bigger button, easier to click, right?"
    author: "Iuri Fiedoruk"
  - subject: "Re: big k-menu button!"
    date: 2001-11-20
    body: "Well, going by the same token, what would a new user with no previous experience know about window management, pull-down menus, etc?\n\nPerhaps brand new users could be directed initially by KTip or the intro wizard to take a look at the rather well writtten KDE tutorial in the docs."
    author: "Carbon"
  - subject: "Re: big k-menu button!"
    date: 2001-11-20
    body: "And if he/she is using a old account that already have used kde?\nThis would loose the idea.\nI think there is nothing wrong on putting a small text on k-menu.\nWe need to stop this windows-fobia sometimes to think what is better for joe-user.\nThere is no harm on looking a bit like windows on parts that really can help the user if KDE do that way."
    author: "Iuri Fiedoruk"
  - subject: "Re: big k-menu button!"
    date: 2001-11-20
    body: "> I think there is nothing wrong on putting a small text on k-menu.\n\nOf course, think of i18n issues."
    author: "someone"
  - subject: "Re: big k-menu button!"
    date: 2001-11-20
    body: "hum... menu is menu in portuguese.....\nKDE can't be read by someone that only reads japaneses, arabic, but it's there."
    author: "Iuri Fiedoruk"
  - subject: "Re: big k-menu button!"
    date: 2001-11-20
    body: "Why not give the kicker panel buttons names, like the app toolbars, and have several toolbars on it - one with Kicker alone, one which absorbs all the other menus & app shortcuts, and then the usual applets. That way, distributors could set it up that way if they pleased. Also I wouldn't go with \"Start\" for a name - look at the Start->Shutdown mess. I'd much prefer Kommand. It'd be cool if you could use the K-Menu image for the \"K\" but I'm not sure how that'd work out with i18n"
    author: "Bryan Feeney"
  - subject: "Re: big k-menu button!"
    date: 2001-11-21
    body: "Why not make Toolbars and Kicker of the same type? I would like that :)"
    author: "isch"
  - subject: "KonCD"
    date: 2001-11-20
    body: "KonCD is great. But unfortunately it tends to be Linux specific. That's because it uses (as do most cd burning software) cdrecord. This requires SCSI burners. Linux gets around this because it can have IDE drives emulate SCSI. But not all unix-like operating systems do this.\n\nAny way to get KonCD to use FreeBSD's burncd as an option? Any way to change the KonCD requirements to something other than \"Linux 2.x.x\", as mentioned in the README? After all, KDE is not a Linux only desktop."
    author: "David Johnson"
  - subject: "Re: KonCD"
    date: 2001-11-20
    body: "I suppose that, if you are a BSD user, you could do it.\nIf you are not, and the KOnCD author is not, how is anyone going to get it done?\n\nYou could write a wrapper for whatever CLI tool you use on BSD to make it act\nlike cdrecord.\n\nOr you could ask the cdrecord author to make cdrecord support the BSD stuff.\n\nYou could even stop using BSD.\n\nThere are many ways to get what you want."
    author: "Roberto Alsina"
  - subject: "Re: KonCD"
    date: 2001-11-20
    body: "<em>You could write a wrapper for whatever CLI tool you use on BSD to make it act like cdrecord.</em>\n\n<p>I thought that has occurred to me several time. Perhaps I will some day, but not immediately as my time is limited.\n\n<em>You could even stop using BSD.</em>\n\nBlasphemy!"
    author: "David Johnson"
  - subject: "Contest for a new \"start\"-button?"
    date: 2001-11-20
    body: "I think most of the people involved recognize that the current \"K\" button is less than ideal usability- and designwise. But as someone mentioned on some list, the problem is really to find a good substitute. Directly implementing the windows \"start\"-button would be an improvement in usability, but it is hard to bear with this blatant lack of originality.\n\nSo I would like suggest that dot.kde.org should arrange a contest to find a new symbol/design for this button! \n\nThe button should pass the the test of asking a completely new user: \"what do you think pressing this button would achieve\", and should have a simple and attractive design that matches different themes."
    author: "will"
  - subject: "Re: Contest for a new \"start\"-button?"
    date: 2001-11-20
    body: "I honestly think that after KDE 1.0,..., 1.1.2, ... 2.2.2 the K button is known good enough among users.\nOne sentence to a new user who doesn't know the K button is enough \"click on the button with the big K to get all the apps\" and he will always know what to do.\nI don't see any need to change it.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: Contest for a new \"start\"-button?"
    date: 2001-11-20
    body: "Thanks for the reply! \n\nSee these comments about GNOME's foot in from Sun's usability report. \n\nhttp://developer.gnome.org/projects/gup/ut1_report/exploring_desktop.html\n\nKDE is should be worse off because it doesn't supply a visual metaphor all..\n\nI agree that the current option doing an OK job, but from a design standpoint it is a mediocre solution, and *that* is the real reason to change it. \n\nIt provides no metaphor, it isn't simple enough as an icon, its \"meta\"-functionality in relation to other buttons should be reflected in visual appearance (it should stand out), the gears doesn't speak to the values of common users, it isn't pretty etc.\n\ngotta run..."
    author: "will"
  - subject: "Re: Contest for a new \"start\"-button?"
    date: 2001-11-20
    body: "Just FYI, the default GNOME 2 setup will have a \u00a8menu\u00a8 panel. This is the best thing to do, IMHO, usability wise (The programs menu is not just an icon, it has a text in the side; it\u00b4s similiar to the menu of an application, the user sees it as a menu of the desktop; top-bottom is better than bottom-top, so the menu is on the top of the screen, etc)."
    author: "Carg"
  - subject: "Re: Contest for a new \"start\"-button?"
    date: 2002-09-20
    body: "Users will click on anything in the bottom left corner, whether it's a K, Start, Launch, or what have you.  It's not icon recognition that's important.  The question is, do we want a goofy K in the taskbar?  What is a K, anyways? I know it's for KDE, but there are too many K's in the KDE world.  It should be user-configurable like wallpaper.\n\nAndrew"
    author: "Andrew"
  - subject: "Re: Contest for a new \"start\"-button?"
    date: 2003-03-13
    body: "Is there realy no way to change the K-Button to a normal \"Start\"-Button?"
    author: "CJoke"
  - subject: "I agree!!!"
    date: 2001-11-20
    body: "Personally I feel that K button is very annoyingly small and annoyingly big at normal size at 800x600 resolution; I don't prefer 1024+ resolution, since I find fonts look nice on the desktop. But If we could have Both \"Text & Graphics\" to make the 'K' button make more sense like see \"Wish-Kicker.png\" please, the attachment... Hope this will give kicker some good looks, even if we choose not to have \"Start\", we could have \"Linux\", \"Menu\", \"Launch\", \"KDE\" along with a suitable graphics."
    author: "Asif Ali Rizwaan"
  - subject: "Re: I agree!!!"
    date: 2001-11-21
    body: "People will find \"start\" too similar to Windows I think - but \"menu\" or \"launch\" is fine, and I am all for that! If I were a developer I would say \"please commit\".."
    author: "will"
  - subject: "Re: Contest for a new \"start\"-button?"
    date: 2001-11-21
    body: "I like the simple button.\nOften I have more than 4 programs open and than the kicker is to small.\nI've deleted the icons and made my own, a folder in the start-menu which is included in the kicker to save space for other programs.\n\nI don't like the idear with bigger button.\nA kicker with size small looks good, but there is not enougth space.\nA bigger button and you have no space ;(\n\n\nWhat about themes an styles.\n'Just' make a config dialog for the \"Look & Feel\" section.\n\nGive the user (or a theme/style) the chance to fit the size and design.\n\n\nhave fun\nHAL"
    author: "HAL"
  - subject: "i didn't like the UI :(("
    date: 2001-11-21
    body: "Look at the MS Windoze XP. ActiveX makes Desktop living, breathing.\nAnd here....no USE of transparent console till i'm unable to see what's behind it but background....everything like...win95. yes. after kde v1.. v2 looked just great. and now....no progr\u00d0\u00b5ss. I'm very upset. I expected essential changes in UI :("
    author: "dr3node"
  - subject: "KDE 5"
    date: 2001-11-22
    body: "My guess is by KDE  5, the desktop will look even more like Mac OS X. Not true?\n\nWhy don't you guys go ahead and enjoy alpha blending. I wander what will be next...."
    author: "bandit"
  - subject: "Why not invent?"
    date: 2001-11-23
    body: "Why not create a new style to desktop computing. It is not following windows XP and Mac OS but for people who want to compute on desktop. \n\nI think the OS (operating system) is the abstract of the hardware or hard resources. the desktop should be the the abstract of the OS resource to supply the applications. For example user should have the chance to computing on the desktop but have not any acknowledge about file system and any OS conceptions!"
    author: "dingguijin"
  - subject: "Looks ugly"
    date: 2001-11-23
    body: "hey,\n\nI am sorry but kde never looked great to me. I think gnome looks much better but lacks AA which is why i don't use it. Looking at kde 3.0 screenshots all i can say is that it still looks pretty much like kde2 which was pretty ugly.\nmy 2 cents."
    author: "quitedown"
  - subject: "dotNet huh? That is pretty gay"
    date: 2001-12-04
    body: "I don't believe my eyes -- a Windows style/theme for KDE! (not cool)."
    author: "DVK"
---
<a href="mailto:kaper@kde.org">Rob Kaper</a> has graciously fulfilled the oft-heard
request for <a href="http://www.kde.org/screenshots/kde3shots.html">screenshots</a> of the <a href="http://developer.kde.org/development-versions/kde-3.0-release-plan.html">upcoming</a> KDE 3.0 release.  Take a trip
to <a href="http://www.kde.org/">KDE.org</a> and <a href="">have a look</a>
at <A href="http://www.kde.org/screenshots/index.html">the screenshots</a> featuring (1) the cool <a href="http://clee.azsites.org/kde/">dotNet style</a> (my
personal favorite after
<a href="http://apps.kde.com/nfinfo.php?id=1153">Liquid</a>)
(<a href="http://www.kde.org/screenshots/large/kde3-snapshot1.jpg">large SS</a>),
(2) <a href="http://www.koncd.org/">KOnCD</a>, now a part of the KDE base
distribution
(<a href="http://www.kde.org/screenshots/large/kde3-snapshot2.jpg">large SS</A>),
(3) a desktop shot with the really cool
<a href="http://users.skynet.be/bk369046/icon.htm">iKons</a> icon set
(<a href="http://www.kde.org/screenshots/large/kde3-snapshot3.jpg">large SS</A>), and
(4) the Media Peach color theme and Quartz window decorations
(<a href="http://www.kde.org/screenshots/large/kde3-snapshot4.jpg">large SS</A>).
This might be a good time to remind everyone:  for more information on the
amazing themes and icons available for KDE, check out
<a href="http://www.kde-look.org/">KDE-Look.org</a>, the
<a href="http://apps.kde.com/uk/0/browse/Art_Music/Themes">Themes</a>
section at <a href="http://apps.kde.com/">APPS.KDE.com</a>, and the
newly redesigned <a href="http://kde.themes.org/">KDE</a> section at
<a href="http://kde.themes.org/">Themes.org</a> (while a spiffy design,
this new site is still
&lt;cough&gt;somewhat bare bones&lt;/cough&gt;, visit the
<a href="http://kde.classic.themes.org/">"classic" pages</a> for some
actual content).  <STRONG>P.S.</STRONG>  Themes.org is
running a "favorite desktop" poll on their
<a href="http://www.themes.org/">frontpage</a>, let your voice be heard!

<!--break-->
