---
title: "Trolltech releases Qt 3.0"
date:    2001-10-16
authors:
  - "bfeeney"
slug:    trolltech-releases-qt-30
comments:
  - subject: "noncom"
    date: 2001-10-16
    body: "Is this version of Qt also under the \"non-commercial licence\" released? I did't found a command about that at the trolltech homepage."
    author: "Someone"
  - subject: "Re: noncom"
    date: 2001-10-16
    body: "If you mean is there a Free version of Qt 3.0, yes there is.\n\n       Jono"
    author: "Jono Bacon"
  - subject: "Re: noncom"
    date: 2001-10-16
    body: "He means the Non-Commercial version that is available for win32.  \n\nIt hasn't been released yet but I hear that it will be in the next couple of weeks."
    author: "ca"
  - subject: "Re: noncom"
    date: 2001-10-16
    body: "An anonymous coward on Slashdot has claimed to be a Trolltech employee and said that there will be QT/Win non-commercial in a few weeks, and also QT/Mac non-commercial!"
    author: "not me"
  - subject: "Re: noncom"
    date: 2001-10-16
    body: "Fine."
    author: "The guy from above"
  - subject: "Re: noncom"
    date: 2001-10-18
    body: "That guy was _not_ a Trolltech employee. If you want information about Trolltech, please ask us directly rather than relying on anonymous slashdot postings.\n\n\nMatthias"
    author: "Matthias Ettrich"
  - subject: "Re: noncom"
    date: 2001-10-18
    body: "was what he said true? it'd be sweet if it were"
    author: "Wiggle"
  - subject: "The old claim."
    date: 2001-10-16
    body: "Why so expensive ? And cross-platform version is even more expensive. It seem that the current price ( 3000$ + 1000$ per year ) does not include Mac version ( as I understood it must be bought separately, spending another 1500$ ). IIRC almost half of their employess are management staff. They probably eat more than a half of total salary sum. Is this a reason ? I can buy a computer with Windows and a more complete development env. from Borland for that price. TrollTech sells its toolkit to rich Western companies, so price is no problem, but what with shareware developers, in-house development in poor countries ( yes they needn't use KDE or  Linux - but this is not an anser )."
    author: "Bur"
  - subject: "Re: The old claim."
    date: 2001-10-17
    body: "When considering a toolkit for commercial development, you don't look just at the absolute price of it. Instead, you look at costs and gains. Say, tookit A costs 1000 of whatever currency and let you finish a certain project within 500 hours while toolkit B costs 2000 and let you finish the project in 300 hours. Now you've got a difference of 200 hours. How much are they worth? Are they worth the 1000 price difference? Probably. And there are more projects to come in which you experience the same increase in productivity. So you happily pay more for tookit B.\n\nThat is the very strength of Qt, highly increased productivity. It is much faster to develop with Qt than with any other toolkit I know of (at least as long as you aren't in Smalltalk ;-).\n\nThe math is different whether you are a hobby programmer or a commercial developer. As a hobbyist, you say \"Damn, USD3000 is too much, I can't afford it\" and go with something cheaper (same as for the hand drill you buy for your home use). As a commercial developer, you look at the difference in price *and* the difference in what you can produce with the tool and base your decision on the comparision of these differeces (same as for a drilling machine you buy for your workshop). Of course, if you run your business like a hobby you may be in deep shit anyway. ;-)\n\nUwe"
    author: "Uwe Thiem"
  - subject: "any FREE toolkit ?"
    date: 2001-10-16
    body: "Hi\n\nwhy doesn't KDE use a really <free> toolkit ?\nBy free, I mean that I mean under LGPL. GPL just sucks for libs IMO.\n\nMaybe you could build your own toolkit under LGPL or use an existing one... using the same syntax as QT or, even better, using GTK with KDE because GNOME also uses GTK... you see the point ?\n\nI know it's maybe too late to think about it, but I really hate paying to develop software... And I want to use KDE power."
    author: "Julien Olivier"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-16
    body: "1) A lot of people think the GTK API sucks.\n2) QT is writte in c++ just like KDE.\n3) QT has a lot of nifty features. Think of the different rendering Engines.\n   Think of the AA-Font support which GTK has problems with.\n4) If you think it's easy to write a complete Toolkit \"using the same syntax \n   as QT\" then go ahead and start coding."
    author: "TheFogger"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "It might be wise from Trolltech to sell a special versions of QT. Lets call it \"QT students Edition\" (or \"Personal edition\")\n\nThis version of QT edition can be sold for the purpose of making *small closed applications* which lets a student at home buy the a special commercial edition of QT and to make some money by selling some closed source applications.\n\nNow - this version can be a special version which limits by either (a) number of lines or (b) size of an application. I'm just throwing some ideas...\n\nAnother variant could be \"QT student edition Linux\" and \"QT student edition MP [Multi Platform]\" so one could be sold for $199 and the other $499.\n\nLots of companies are doing this business today by selling a limited version for students (like MS Visual Studio Student Edition) and its a nice revenue stream..."
    author: "Hetz Ben Hamo"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-18
    body: "I remember a statement on their website saying that they wouldn't ship anymore shareware version because it was a nightmare to manage. They seem to have tried to do that kind of shareware version and stopped it.\n\nSo the poor student is stuck with selling a GPL application, or with developing non commercial application. This is already very good in my opinion. In France we say \"you give them a hand, and they want the arm\". Be satisfied with what we have, it is already very good.\n\nElse, you can go with PyQt. It is far less expensive, equally well supported and  it is great because it uses python!"
    author: "Philippe Fremy"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-16
    body: "So you want to be able to use free software to write nonfree software?  Why should you be able to make money off of Troll Tech's software but they shouldn't?  Releasing a library as GPL seems to be, to me, MORE free than LGPL (which is quite probably why the \"L\" stands for \"Lesser\").  If you want to develop using a free toolkit, make your software free.  If you want to be able to charge for it, don't bitch about someone else doing the same thing."
    author: "Me"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-16
    body: "Ok, it sucks for you, fine!\n\nSo please start writing a QT-API compatible port of GTK+.\nI'm sure you find some guys willing to help.\n\nBut meanwhile we stay with Qt. You know, its GPL, KDE is GPL and LGPL. Its fine for our purpose. We don't have any commercial interest. We want to write only FREE software. \n\nBut please announce your toolkit when you are ready. I promise we will switch.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "As far as I can see, Qt is free as in speech, but not as in beer. If you want to write free code (by which I mean *truly* free - with all the rights required for GPL compatibility) then Qt is free. If you want to write non-free code, then Qt is (quite reasonably) non-free. \n\nIt's a bit more limited for the Win32 platform and (I think) MacOS, where you only have a free closed source version of the library for personal use, and must licence your code under the BSD or LGPL license (GPL is possible with a disclaimer AFAIK), but TrollTech need to make money, and they've done wonders for the Unix community. If they go commercial again, people can continue with the last GPLed version; if they fold they've promised to release Qt under a BSD license. Frankly there's few other companies that have made such a sizeable and *real* contribution to Linux and the open-source community, and I for one applaud them for it.\n\nIncidentally there was once an attempt to write a Qt clone, in the early days of the licence wars, called Harmony, but it collapsed. I'm not entirely unhappy that it did either. KDE has benefitted immensely from having a group of experts working 40 hours a week (in theory, judging by the QtMoz story it's a lot more in practice) on their core infrastructure. In comparison Gnome has found it hard to keep up with KDE due to work on all the libraries down, and have lost a lot of prestige, even though from what I've heard, there's a lot of seriously good work after going into the Gnome 2 infrastructure.\n\nBasically, I think it's reasonable to charge for commercial projects and not charge for free ones. You may argue about the limitations on the non-X11 ports, but it's still a massive contribution for software developers."
    author: "Bryan Feeney"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "Wow, another RMS-brainwashed fool.  Licensing is just that: licensing.  KDE is fine using QT.  QT is, in the opinion of most of us, superior to any \"free\" LGPLed libraries.  If you'd rather use an inferior environment because you like the license more (and I'd bet most people making this argument have yet to actually read nad understand the licenses), go right ahead."
    author: "dingodonkey"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "RMS defends the use of the GPL for libraries, not the LGPL. So he would be in your side in this discussion.\n\nSee http://www.gnu.org/philosophy/why-not-lpgl.html."
    author: "Evandro"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "Ditto I too am sick of the Free Software Fanatics."
    author: "craig"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "What the hell is that obsseision with gpl. I don't give a shit if it's gpl or not. I just want to click on the rpm package and download it, that's all, without paying anything"
    author: "juanjo"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "What the hell is that obsseision with gpl. I don't give a shit if it's gpl or not. I just want to click on the rpm package and download it, that's all, without paying anything"
    author: "juanjo"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-18
    body: "hi, i hope it's true"
    author: "pepe"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "RMS defends the use of the GPL for libraries, not the LGPL. So he would be in your side in this discussion.\n\nSee http://www.gnu.org/philosophy/why-not-lpgl.html."
    author: "Evandro"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "LGPL isn't any freer than the GPL when it comes to C++. From today's license-discuss list:\n\n\"I just got a response from FSF lawyers stating that \"inheritance is considered modifying the library\" ... and according to FSF, inheritance falls under \"work based on a library\" and as such should be released under LGPL.\""
    author: "David Johnson"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "Uh, wouldn't that make every GPL-incompatible piece of software using KDE libraries illegal in the eyes of the almighty FSF?\n\nScary."
    author: "nap"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "Is it possible to sell a GPL app made using QT on Linux ? on Win32 ? on Mac ?"
    author: "Julien Olivier"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "Of course! GPL does not forbid anyone to sell the software.  Look at Red Hat."
    author: "KDE User"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "So, I'm sorry...\n\njust forget my thread: I thought that, with QT licence, you couldn't sell a, app, even in GPL.\n\nBut, is QT in GPL for Windows and Mac too ?"
    author: "Julien Olivier"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "No.  Qt is available for \"non-commercial\" use on Windows and Mac.  The license basically says you are not allowed to sell an application you make nor get paid to create it.\n\nSince the Qt non-commercial package is binary only, if your app is GPL then you have to include an exception in your license like \"this app may be linked to Qt\".  Go see the Linux kernel license sometime.  It is GPL-with-exception also, to allow closed source development on Linux.\n\nBtw here is a question I want to throw out there:  Is it even possible to have a true GPL app on Windows?  Since MFC, Win32, etc are all closed source, doesn't that mean any Windows app claiming to be GPL is actually GPL-with-exceptions?\n\nAnd another question.  Say you write a GPL app that links to LessTif.  Is it alright for some user to link against Motif instead?  Do they violate your license?"
    author: "Justin"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: ">And another question. Say you write a GPL app that links to LessTif. Is it alright for some user to link against Motif instead? Do they\n         >violate your license?\n\nIt is perfectly alright as long as they don't distribute the binary."
    author: "Morty"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-18
    body: "It's perfectly OK to run GNU programs on proprietry versions of Unix, so I guess it's OK to run GPL'd stuff on Windows."
    author: "KevinA"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-17
    body: "\"why doesn't KDE use a really <free> toolkit ?\nBy free, I mean that I mean under LGPL. GPL just sucks for libs IMO.\"\n\n*sigh* First people complained \"Why doesn't KDE use a really free toolkit? By free, I mean under GPL\". Now that the toolkit is under GPL, people complian \"Why doesn't KDE use really free toolkit? I mean under LGPL\".\n\nI guess some people just are never happy..."
    author: "Janne"
  - subject: "Re: any FREE toolkit ?"
    date: 2001-10-19
    body: "\"I guess some people just are never happy...\"\n\nNo. \n\nIt's that some people just never have a clue...\n\n*re-sigh* To many people complain about things they don't even understand..."
    author: "Holstein"
  - subject: "Dockers"
    date: 2001-10-17
    body: "Are there those funny dockers, as I saw in KOffice,  included in Qt ( like KIllustrator \"Layers\" docker) ?"
    author: "Krame"
  - subject: "Re: Dockers"
    date: 2001-10-17
    body: "Please point us to a screen shot. ;)"
    author: "KDE User"
  - subject: "Re: Dockers"
    date: 2001-10-18
    body: "Here's a shot. The dockers are the little dialog boxes on top of the canvas, some angled horizontally, some upside down, etc. I believe theKompany introduced them to the KOffice codebase, and Kivio as well as Kontour are using them. It certainly is a neat little set of tools... if you want to see a radically different version of Kivio in action, check out HancomEnvision - you can download an evaluation version from the Hancom website.\n\nCheers,\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re-attempt to post"
    date: 2001-10-18
    body: "Err... for some reason the image won't upload. Let's try again."
    author: "Eron Lloyd"
  - subject: "Re: Dockers"
    date: 2001-10-17
    body: "Last time I used this on a remote KIllu..., ups Kontour this was sooo slow. OK it works on a locale machine, but on a not so performant remote line...?\nAnd its (classic-)MDI only. Try to use this in a KPart. Don't do this with toolboxes, better use a floating docked window.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "PyQt on various platforms"
    date: 2001-10-17
    body: "Does anyone know if one is allowed to write and distribute GPL'd python programs that link to Qt 3.0 on unix and windows (and Mac OSX if that becomes available)?\n\n-- Stephen"
    author: "Stephen"
  - subject: "Re: PyQt on various platforms"
    date: 2001-10-17
    body: "This is simple:\n\n1) Qt3.0 on Unix exist in GPL licence. so you can write and distribute GPL software (language doesn't matter) using Qt3.0. \n\n2) QtFreeEdition on windows can be used (link) with free software (free as in beer). The  licence doesn't matter here, you CAN'T SELL your software. If you want to sell your software on windows platform, you need to buy a QT licence.\n\nregards."
    author: "thilor"
  - subject: "Killer Component Architecture and QT Price"
    date: 2001-10-18
    body: "Sounds great! this would be a really nice way to (semi)unify the *NIX desktop.\nBut I don\u0092t think the Trolls would be keen on adopting technology they don\u0092t own, so it will probably never happen :-(\n\nAlso the price of QT stops our Company from using it for small in house projects. I think this is the case for the majority of potential commercial QT users, thus limiting the spread of QT."
    author: "Fredrik C"
  - subject: "In house?"
    date: 2001-10-18
    body: ">Also the price of QT stops our Company from using it for small in house projects.\n\nWhat do you mean by 'in house'? If it means an internal project, you can still use QT, but you cannot sell or distribute without source code unless you buy a license."
    author: "ac"
  - subject: "Misconception ??"
    date: 2001-10-19
    body: ">Also the price of QT stops our Company from using it for small in house projects.\n\nIf your projects are in house you can use a GPL licence and use:\n-QT GPL on unix\n-QT NonCommercial on windows\n\nThen, If you sell the software, you have to buy a QT licence. \n\nThis is right licencing, thank QT !!\n\nregards"
    author: "thilor"
  - subject: "Re: Misconception ??"
    date: 2001-10-20
    body: "You can't use NonCommercial QT for in-house projects."
    author: "Krame"
  - subject: "Re: Misconception ??"
    date: 2001-10-22
    body: "Wrong, check the faq at www.trolltech.com/developer/faqs/noncomm.html:\n\nThe Non-Commercial Edition is the Qt for Windows toolkit, licensed for private development of non-commercial software in a non-commercial setting.\n\nA non-commercial setting means that you must not use the package in the course of your employment or whilst engaged in activities that will be compensated."
    author: "Outsider"
---
<a href="http://www.trolltech.com/">Trolltech</a> have <a href="http://www.trolltech.com/company/announce.html?Action=Show&AID=75">announced the release</a> of Qt 3.0 for Windows, Linux, <a href="http://www.trolltech.com/company/announce.html?Action=Show&AID=76">MacOS X</a>, various Unices, and embedded systems. This version of Qt includes a rich text editor, database connectivity, improved font handling and internationalisation. As <a href="http://dot.kde.org/1002007199/">previously reported</a>, the COM functionality, long-scheduled and subject to some discussion, was dropped two weeks ago due to serious concerns about its effect on the API. Qt 3.0 comes with an upgraded Qt Designer (which provides for full application design, including menus), Qt Linguist to aid in internationalisation, and Qt Assistant, an online help browser. There is also support for perl-style unicode regular expressions, multiple monitors and 64-bit safety. The press release is <a href="http://www.businesswire.com/cgi-bin/f_headline.cgi?bw.101501/212880872">here</a>, and the downloads are available <a href="http://www.trolltech.com/developer/download/qt-x11.html">here</a>. <i>[Ed: As an aside, this proposal for a <a href="http://dot.kde.org/1002007199/1002471145/">"Killer Component Architecture"</a> by <a href="mailto:bkn3@columbia.edu">Brad Neuberg</a> may be of interest to some.]</i>


<!--break-->
