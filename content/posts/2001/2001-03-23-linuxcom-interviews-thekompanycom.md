---
title: "Linux.com interviews theKompany.com"
date:    2001-03-23
authors:
  - "jtackaberry"
slug:    linuxcom-interviews-thekompanycom
comments:
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-23
    body: "Come on Shawn, progressive metal???\nReal coders only listen to death metal! :-)\n\nAnyway, great interview!"
    author: "Jacek"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-23
    body: "are you kidding?\nReal coders listen to techno/trance \nwhile coding. That's the only way to code \nefficiently. Moreover there are no good Heavy/death  metal internet radios."
    author: "jesunix"
  - subject: "Music to code by"
    date: 2001-03-23
    body: "SQUAREPUSHER!!!!!!!\n\n- and that's all I have to say about that."
    author: "Eron Lloyd"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-23
    body: "Personally, I prefer something trippy and slow, like Scorn.\nWhen I'm under time pressure, I code listening to gabber."
    author: "Jacek"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-23
    body: "I code listening to Polvo, Syd Barrett and other experimental bands. This makes my creativity go wild and come up with some weird coding ideas."
    author: "Jes\u00fas A. S\u00e1nchez A."
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-23
    body: "Real coders don't give a damn about noises outside their heads. Not while they are really coding like real coders. Really!"
    author: "Roberto Alsina"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-23
    body: "Black Metal"
    author: "Mayhem"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "Real Coders listen to early eighties british\npost-new-wave such as Deep Freeze Mice and TV\nPersonaliies.\n<p>\n(Not really, but it would be nice).\n<p>\nP.S If you are wondering the following Shawn's\nphrase:\n<p>\n\"Honestly, we are totally different companies. Ximian's products are T-shirts and stuffed animals\"\n<p>\nvisit:\n<p>\n<a href=\"http://www.thinkgeek.com/stuff/helixcode.html\">http://www.thinkgeek.com/stuff/helixcode.html</a>\n<p>\n(I got the link from the official ximian site).\n"
    author: "Count Zero"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "No, no, no. You guys have it all wrong. REAL coders listen to Andrea Bocelli and Michael W. Smith."
    author: "Timothy R. Butler"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "Michael W. Smith?\nI listen to dc Talk ALL THE TIME!  They are my favorite group!  Micheal W. Smith is really good too, but I like dc Talk, Audio Adrenaline, Project 86, Tourniquet, P.O.D., Earthsuit, and a few others while coding, driving, sleeping, eating... I guess I could just say \"living\" and that'd cover it!\n\nCHRISTIAN MUSIC ROCKS!"
    author: "A. Cantrell"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-25
    body: "Hi A. Cantrell,\n  Let me second that for Christian Music (for coding, eating, and driving - although I'm not sure about sleeping)! I must admit that I haven't had the opportunity to hear much dc Talk, but I need to put that on my \"todo\" list.\n   I guess while I'm at it, I might as well add in a few more good CCM'ers... Steven Curtis Chapman, Wes King, Chris Rice, Hillsongs Australia, and Sixpence None the Richer are good for CCM choices for coding (and eating, and driving, and who knows, maybe even sleeping) too. They all were helpful in getting a major coding project done.\n\n  I guess I'm not the only one who thinks Christian Music is idea for coding then, eh? :-)\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-25
    body: "Have you ever checked out http://www.christiancoders.com it has a message board and some other stuff.  It's not really Linux programming, most of the people seem to be into Windows programming, but there's hope for them, eh? =)"
    author: "A. Cantrell"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-29
    body: "Hi A. Cantrell,\n  Hmm... no I hadn't. I'll have to go look at their site. Thanks!\n\n   -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "J.S Bach rocks!"
    author: "konqi"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "Was that the lead singer from Skid Row? ;-)\n<p>\n<a href=\"http://forums.delphi.com/sebastianb/start/\">http://forums.delphi.com/sebastianb/start/</a>\n"
    author: "Roberto Alsina"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "> J.S Bach rocks!\n\nAgreed - the Brandenburg Concertos are what I use when I'm doing high level design.  After that, it's really doesn't matter; it all fades into the background when I code.  Lately, I've been listening to rough, gritty stuff like Violent Femmes, Nick Cave, Black 47, Boiled in Lead... but ELO, Queen, Erasure, Oingo Boingo, Devo, Holst, 10k Maniacs, Dead Milkmen, Zeppelin, Lords of Acid... all have been good coding music.  Sony's 400 disk CD changers are great.\n\nI've very sensitive to sound and space when I code - I can't stand people behind me, and while I can do admin tasks in silence, I can't *really* get into code unless music is playing - in the 80's, I would just listen to Metallica's ...and Justice For All album on repeat for hours on end while I worked (prior to that, Pink Floyd's The Wall).  Over and over and over... but I wasn't really listening.\n\nAbout the only things I can't code to are songs by James Taylor, Zappa, Janis Joplin, late era Beatles, some soundtracks and a few others - it's not that the music isn't good... it just intrudes into my mental space, and I wind up listening rather than coding.\n\nRight this moment, though, I'm listening to quite possibly the best title for something to listen to while coding: put out by Information Society, the title of the album (which uses things like a modem connect as the start of a song) is simply:\n\n    Hack\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "Yes, the Brandenburg Concertos are great. I would also like to recommend the small pieces like the Goldberg Variations, partitas, suites and others. I think there's something in the polyphonic music  that stimulates your mind.\n\nP.S. Why is it that the world never remembered the name of Johann Gambolputty de von ausfern schplenden schlitter crassbenbon fried digger dingle dangle dongle dungle burstein von knacker thrasher apple banger horowitz ticolensic grander knotty spelltinkle grandlich grumblemeyer spelterwasser kurstlich himbleeisen bahnwagen gutenabend bitte ein nurnburger bratwustle gerspurten mitz weimache luber hundsfut gumberaber shonendanker kalbsfleisch mittler aucher von Hautkopft of Ulm?"
    author: "konqi"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "Tomorrow we'll sing St. Johns Passion..\n:-)\n\nTim"
    author: "Tim Gollnik"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "That part gave me a shock....heh...\n<p>\n'cause I'm also only programming till I 'Make it' as a musician....\n<p>\nand the progressive metal bit is even the same ;)\n<p>\nanyway...to pull this post somewhere vaguely on topic, I must say that as a lurker on koffice and koffice-devel and a regular reader of the dot that I have seen nothing but good things from Shawn and theKompany.com.  I hope he keeps up the good work, and manages to make theKompany.com a success.\n<p>\n<a href=\"http://www.mp3.com/cumulonimbus\">http://www.mp3.com/cumulonimbus</a>\n"
    author: "Stuart Herring"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "I wrote and recorded a progressive hard rock \"concept\" album about 7 years ago on 8 track in my garage with my last band.  I recently converted it to MP3's and I was thinking about putting it up somewhere.  I wish the quality was better, but it's not too bad.  \n\nIt seems like a lot of good programmers are also musicians, seems to have something to do with our superior brains and constant typing :).\n\nI appreciate your kind words, and I know our hard working programmers do too."
    author: "Shawn Gordon"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-26
    body: "Not as MP3 - you should use Ogg Vorbis instead! Take a look at http://www.vorbis.com/ to get more info, if you haven't heard about it already..."
    author: "Joergen Ramskov"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-26
    body: "Oh I love Ogg Vorbis, I just didn't have the tools to make it set up yet.  I've got some interesting ideas of some things I would like to do with that technology.  I'll have to try that new Konqueror plug in and pull them off the CD they are on, I think that will work."
    author: "Shawn Gordon"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "#define __REAL_CODERS_LISTEN_TO_GOA_TRANCE__"
    author: "604"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "Filling gaps ? There is no good charting tool. KChart is just hopeless."
    author: "Felek"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "You can do charting with VeePee, see <a href=\"http://www.thekompany.com/projects/vp/screenshots\">http://www.thekompany.com/projects/vp/screenshots</a>.php3 part of the reason for VeePee and charting was in support of Kapital, so we could do charts.  For reporting, Kugar could use some charting controls.\n<br><br>\nI haven't looked at KChart at this stage, so I can't really comment, but perhaps VeePee can be used as a replacement.\n"
    author: "Shawn Gordon"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "Hmm...strange.  I thought I wasn't unique in\nwhat I listen to while I code.  I thought most\npeople listen to Britney Spears but I guess it\nwas just me. :)"
    author: "Anonymous"
  - subject: "What's KODE exactaly"
    date: 2001-03-24
    body: "From the interview I got that KODE was an IDE for multipile languages.  It would be great if there was a 'usable' IDE for UNIX, something with an editor like SlickEdit that does simple project management and can debug threads is all I realy want(Motif support would nice though, Qt cost too much).  Can you give more info please!!!"
    author: "Lee"
  - subject: "Re: What's KODE exactaly"
    date: 2001-03-24
    body: "I don't want to get into a lot of details until things are closer to completion.  I have a severe aversion to vaporware and I don't want to create false expectations, so try to hang tight and as the product continues to come together, we will let more details out."
    author: "Shawn Gordon"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "Maybe Shawn was a bit rough on Xamian. I hear that there's a huge market for stuffed monkey's out there. Maybe they can expand into beanie babys or those MCDolands toys that they give away in the happy meals.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-24
    body: "Ahhh I listen to metal when I code. Black, Death, Thrash, Progressive, Power - I love it all...well apart from nu-metal.\n\nAnyway, I think this is defying the point. Real coders just listen to inspiration...and we all have that. :-)"
    author: "Jono Bacon"
  - subject: "Re: Linux.com interviews theKompany.com"
    date: 2001-03-25
    body: "It now appears that Xamian is an elaborate ploy to get linux users to purchase stuffed animals. I don't know if its true but i heard that Miguel came up with the idea will working as a carney at a circus in Mexico city. It may not be true but i heard  hes the third los lobos brother.\n\nCraig"
    author: "Craig Black"
---
We've <a href="http://www.linux.com/news/newsitem.phtml?sid=1&aid=11946">interviewed Shawn Gordon</a>, President and CEO of <a href="http://www.thekompany.com/">theKompany.com</a>, over at <a href="http://www.linux.com/">Linux.com</a>.  Among other things, in this lengthy interview we discuss theKompany's business model, a bit about Eazel and Ximian, their current software, and future plans for Rekall, Kivio, Kapital, and KODE.