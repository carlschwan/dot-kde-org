---
title: "KonCD in KDE, Mosfet News, Kinkatta 1.00, KDE++"
date:    2001-11-02
authors:
  - "numanee"
slug:    koncd-kde-mosfet-news-kinkatta-100-kde
comments:
  - subject: "hm"
    date: 2001-11-02
    body: "I'm not sure if the author of kit would like this.. but looking at the fact that kit has a lot less features than kinkatta, would kit be replaced by kinkatta i dkenetwork? Does kinkatta have other depenedences?"
    author: "FO"
  - subject: "Re: hm"
    date: 2001-11-02
    body: "kit wil be a jabber client in the next version \n\nJabber is a revolutionary new instant messaging and presence management platform based on XML and open standards. It suport ICQ, MSN, AIM, Yahoo IM, and IRC"
    author: "Underground"
  - subject: "Re: hm"
    date: 2001-11-02
    body: "that is good news - kde needs a jabber client.\n\nkonverse is unmaintained at the moment - i suppose that could be a good start"
    author: "Thilo"
  - subject: "Re: hm"
    date: 2001-11-03
    body: "I just knew Kit'd be mentioned, so I read...\n\nThe author of Kit wouldn't like it because Kinkatta isn't a real KDE app.  It's like KVirc - a Qt app half-ported.\n\nPersonally, if you want Kit removed, I'd not want it replaced by any AOL-bound app.  Hopefully it'll be Kit 2 that replaces it."
    author: "Neil Stevens"
  - subject: "Re: hm"
    date: 2001-11-04
    body: "Kinkatta uses KDE much more now than kvirc ever has."
    author: "FO"
  - subject: "Re: hm"
    date: 2001-11-04
    body: "Kinkatta has actually gone a long way in intigrating KDE into the main branch.  There is a QT \"branch\" in which I often talk about (getting it to compile it under windows and such).  But the main branch has come a long way.  We now use KDE itself (as seen by the themes).  We use the kde widgets (also can be seen via the themes).  We use KProcess for spawning sounds etc.  I have upgraded most all of the Qbla -> Kbla widgets.  Other then not using the KAbout (crashes for me... and I think it is ugly and needs to be hacked another day) and the KConfig (we use a xml based file settings and I don't wish to have the users go through yet another file format transition.)  The icons now install into kde's icon world (which messes up half of the time on the install on suse, mandrake, redhat... somthing intigrating into kde's cvs would fix)\n\nSo to make it \"fully\" kdeish I would a) redo the config file to use kconfig.  b) use the kabout.  c) make the chat widget xml based.\n\nIf someone wishes to submit b as a working patch I will gladly put it into cvs.  As for c the base chat widget is getting a redo with the new QT3.0 functionality and while I am at it xml can be done too :)\n\nSo it is up to you all.  In or not?"
    author: "Benjamin C Meyer"
  - subject: "Re: hm"
    date: 2001-11-06
    body: "Not I think that KDE needs Jabber not AIM wat if AOL chane there protecol then kinkatte dosend work and you can only chat agane wene a new versio of kde come out"
    author: "Underground"
  - subject: "Re: hm"
    date: 2001-11-08
    body: "well, AIM _is_ the most popular IM protocol. It'd say in. However, it'd be nice to have plugins for jabber and such, ala Gaim."
    author: "off"
  - subject: "KDE-CVSBuild"
    date: 2001-11-02
    body: "http://kde-cvsbuild.sourceforge.net/ is the home of a great little script.  This script will automatically:\n\nDownload the sources using CVSup\nProperly configure and compile QT\nCompile all the KDE packages you want, in the correct order\nInstall it to a directory of your choosing\nLog all the output so you can figure out why it didn't finish if it doesn't\n\nIt needs a little change to convert it to QT3 and KDE 3, but other than that it works great!  You just run it and walk away, the downloading and compiling is done automatically, and it can even do it in the background while you work.  If you know a little about Bash scripting, download it and give it a try.  Once it is set up, having the newest KDE is as easy as typing one command and waiting a couple of hours :-)"
    author: "not me"
  - subject: "Re: KDE-CVSBuild"
    date: 2001-11-02
    body: "I'm sure a lot of people would benefit if you could post the changes necessary to this scrip somewhere. I know I would :-)\n\nregards,\n\nMark."
    author: "Freubelaer"
  - subject: "Re: KDE-CVSBuild"
    date: 2001-11-03
    body: "There aren't many changes.  In the script itself there is a function BuildQT().  You have to change the make targets because they're changed in QT 3.  Comment out the DoCmd lines that are there to make QT and add:\n\nDoCmd \"Making $QTDIR\" \"make $mk symlinks sub-src sub-tools\" \"make (symlinks sub-src sub-tools)\" \"$thnline\"\n\nI'm not sure if this is strictly necessary but it keeps make from whining at you at least.\n\nThe kde-cvsbuild.conf file is where most of the changes need to be made.  The readme for qt-copy in CVS says you need to set the environment variable YACC like so:\n\nexport YACC='byacc -d'\n\nOf course this assumes you have byacc installed (apt-get install byacc for the enlightened).  Also, the configure line for QT has changed in 3.0.  Find the qtcfg line, comment it out, and replace it with:\n\nqtcfg=\"-system-zlib -qt-gif -system-libpng -system-libjpeg -plugin-imgfmt-mng -thread -no-stl -no-xinerama -no-g++-exceptions\"\n\nThat's all the changes I remember making to convert the script.  Of course, you have to customize the script to your system.  You have to choose where to stick the downloaded source and compiled binaries, which CVSup server to use, which packages to download/compile, and what options you want."
    author: "not me"
  - subject: "Re: KDE-CVSBuild"
    date: 2001-11-02
    body: "If I knew what changes that was, I would ask the developer to include these changes in the next version."
    author: "Per"
  - subject: "Packet writing"
    date: 2001-11-02
    body: "KOnCD is nice, but packet writing is nicer. Just drag and drop to CD-RW (or CD-R RSN)in Konqueror.\nhttp://packet-cd.sourceforge.net/\nAnyone tested this?"
    author: "reihal"
  - subject: "Re: Packet writing"
    date: 2001-11-02
    body: "Yes, but only one year ago. It s quite slow, formatting a disk takes ages. Writing to a disk sometimes freezes the PC for 20 -20 seconds. Special charcters like \u00f6\u00e4\u00fc\u00df and so on did not work in file names and I got corrputed entries in some directories.\nOTOH the files I wrote on the disk worked fine and were readable with MS Windows Roxio packet-cd.\n\n\nI don't know how much this has improved until now. Maybe you can try it and tell us?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Packet writing"
    date: 2002-01-05
    body: "I usually get this message that \"no recorders found on my system\" that's why someone told me to visit this site for solution. thank you to hear from you soon."
    author: "jabs yanks"
  - subject: "What's that style in Kinkatta screenshots?"
    date: 2001-11-02
    body: "It's not Liquid, right? It looks really cool, I'd like to have\nit. Or is it a custom style for Kinkatta?"
    author: "TheFogger"
  - subject: "Re: What's that style in Kinkatta screenshots?"
    date: 2001-11-02
    body: "It's the Acqua Cobalt theme. It's a pixmap theme, so there's no translucencies or anything. You can still get it on http://www.kdelook.org, Apple don't seem to have noticed it yet."
    author: "Bryan Feeney"
  - subject: "docbook -> html with kde look"
    date: 2001-11-02
    body: "pupeno's documents are rendered with the kde help stylesheets ...\nanyone knows a quick way to do this on a kde system ?"
    author: "ik"
  - subject: "Re: docbook -> html with kde look"
    date: 2001-11-02
    body: "Hi\n\nI didn't have a look at the files you mention, but for converting DocBook --> html there is \"meinproc\". \"meinproc index.docbook\" should give you the according html files in the same dir. To get the style sheets, insert a symbol link to \"$KDEDIR/share/doc/en/HTML/common\" in the directory you have the html-files. In this dir are all graphics and stylesheets to get the KDE HelpCenter look and feel.\n\n-physos"
    author: "physos"
  - subject: "cdbakeoven"
    date: 2001-11-02
    body: "Hi, this whole summer, I've worked on CD burnning tool, but I've run out of time :(. Chek it out at <a href=\"http://cdbakeoven.sf.net\">http://cdbakeoven.sf.net</a>. Now I have to complitely concentrate on my work. The last time I've toched the code was in early september. I've never advertized it on kde.org or apps.kde. IMHO it deserves to be complited. Take a look at it, and maybe some one will take over that thing?"
    author: "Alex"
  - subject: "Re: cdbakeoven"
    date: 2001-11-03
    body: "advertize it on apps.kde.org and freshmeat, say that it needs a maintainer, I'm sure someone would pick it up. From what I've seen, ti looks great."
    author: "FO"
  - subject: "Re: cdbakeoven"
    date: 2001-11-03
    body: "Wow, this looks great!\nAt first sight, it is the most advanced looking cd burner program I've\never seen for Linux(/Kde).\n\nWould be great if it could be integrated with kde"
    author: "yves"
  - subject: "Re: cdbakeoven"
    date: 2001-11-03
    body: "Dang, this really does look great! I've used KOnCD a couple of times, and though it works, I think it's ui sucks badly. This software looks really nice, though!\n\nI sure hope you'll find someone to maintain it! Good Luck!"
    author: "me"
  - subject: "Re: cdbakeoven"
    date: 2001-11-03
    body: "hey this is cool stuff. I've never seen a greater frontend for burning cds under linux/kde. (sorry, koncd an kreatecd are not really functional) I hope for all of us that YOU will go on working on it or another guy will do it.\nGOOD work, thank you :)"
    author: "buhzi"
  - subject: "Re: cdbakeoven"
    date: 2001-11-04
    body: "I was already about to give up on finding a truly good cd recording frontend for Linux (being it for gnome, kde, whatever...), but as a first impression I seem to have found it. I agree, it _really_ deserves to be completed."
    author: "Carlos Rodrigues"
  - subject: "Re: cdbakeoven"
    date: 2002-01-26
    body: "It can't write audio cd"
    author: "Plamen"
  - subject: "Re: cdbakeoven"
    date: 2004-09-02
    body: "This is the coolest CD burning program for Linux that I've ever used. Before I stopped using Windows about half a year ago, I used Nero, but I think CDBakeOven is easier to use and more intuitive than that. (I can't count the number of times I had to look in the help file for Nero.) I'm glad I quit using Windows. Thanks to programs like these, the free software/open-source software community can get a positive response from many Windows-to-Linux converts like me.\n\nI really hope you can find a maintainer for CDBakeOven. I would be willing to do it, except that I'm a busy student and I have little Linux programming experience and almost no GUI programming experience. Anyway, great job.\n\nP.S.: CDBO *can* make audio CD's. I saw the option on the main menu."
    author: "Dan B"
  - subject: "Pupeno's instructions"
    date: 2001-11-03
    body: "Could anyone please mirror Pupenos instructions about running 2 KDE versions concurrently - for some reason I can't access the original site :("
    author: "Anonymous"
  - subject: "Re: Pupeno's instructions"
    date: 2001-11-03
    body: "Problem solved - as I managed to get a brief look at these pages yesterday at work, I just tried to reproduce it from memory and after couple of tries it worked :)"
    author: "Anonymous"
  - subject: "why KonCD"
    date: 2001-11-04
    body: "Why did they choose KonCD? From my point of view, KreateCD is more convinient, has a better interface, from the technical it comes with filters to normalize .ogg .mp3 .au .wav to the same loudness level, which is absolutely necessary if you intend to burn audio cd s with files from different sources.<br>\nAnd even one more thing: It s got a lot more hits on apps.kde!"
    author: "ElveOfLight"
  - subject: "Re: why KonCD"
    date: 2001-11-04
    body: "i agree too, both are great, but I like kreatecd betta"
    author: "FO"
  - subject: "Re: why KonCD"
    date: 2001-11-04
    body: "Or even more provacative: Why had any of the two programs to be chosen? Must KDE3 have a CD burner program on board, in the basic distribution? I don't really think so!\n\nApart from that, I still use GToaster to create my CDs because, although it's ugly as hell, it seems to be more intuitive to use for me. And sadly WinOnCD beats all Linux frontends for CD burning! I have to admit that the first Linux CD software whicht at least looked good to me is that Bake Oven stuff mentioned above. I have to compile it!"
    author: "AC"
  - subject: "Mosfet"
    date: 2001-11-05
    body: "I'm glad that the trials of Mosfet are not going unmentioned in the dot.  I think he did a lot of good work for KDE and I hope that this whole thing with FT gets sorted out in his favour.\n\nI'd love to see his stuff back in KDE.  Oh well, maybe time will tell.\n\nChaz"
    author: "Charles Gauder"
---
In the latest news batch, <a href="mailto:wigren@home.se">Per Wigren</a>
wrote in to inform us that the worthy
<a href="http://www.koncd.org/">KonCD</a> has been added to KDE CVS.  I've
used this CD burner front-end successfully in the distant past, and it has
evidently progressed in leaps and bounds since then  --
<a href="http://www.koncd.org/screenshots.php">screenies</a> ici.  On the less fun side of things, we have had news of legal wranglings between <a href="http://www.futuretg.com/">Future Technologies</a> (site no longer accessible -- this is the FTKDE company) and KDE
developer Mosfet who wrote the popular
<a href="http://www.mosfet.org/liquid.html">Liquid</a> style, also named
FTLiquid at some point.  See
<a href="http://www.mosfet.org/">Mosfet.org</a>, <A href="http://slashdot.org/article.pl?sid=01/10/28/2322232">Slashdot</a> and
<a href="http://linuxtoday.com/news_story.php3?ltsn=2001-10-29-002-20-NW-CY-KE">LinuxToday</a> to get some of the story.  On a happier note,
<A href="mailto:icefox@mediaone.net">Benjamin Meyer</a> gleefully points out that <a href="http://kinkatta.sourceforge.net/">Kinkatta 1.00</a> is out.  He
has worked hard to provide a <a href="http://kinkatta.sourceforge.net/features.shtml">fully functional</a> and stable (not
to mention <a href="http://kinkatta.sourceforge.net/screenshots.shtml">purty</a>) AOL(TM) chat client.  Finally, for those of you having trouble with concurrent KDE installations, <a href="mailto:pupeno@pupeno.com.ar">Pupeno</a> has nicely <a href="http://www.pupeno.com.ar/runningkdes/">written up his experiences</a> on installing another KDE (such as KDE CVS) alongside a stable KDE installation.  He uses Mandrake 8.1, but the experience should be useful to anyone having trouble with <a href="http://www.kde.org/kde1-and-kde2.html">the regular instructions</a>.