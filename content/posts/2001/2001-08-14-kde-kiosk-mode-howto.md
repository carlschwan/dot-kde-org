---
title: "KDE Kiosk Mode HOWTO"
date:    2001-08-14
authors:
  - "pfehrenbacher"
slug:    kde-kiosk-mode-howto
comments:
  - subject: "Good news..."
    date: 2001-08-14
    body: "This development is a key to the deployment of Linux/KDE as a workstation environment in many organisations that today use another well known OS. Keep it coming!"
    author: "Olafur Jakobsson"
  - subject: "Re: Good news..."
    date: 2001-08-14
    body: "Why is it that as soon as a decent desktop turns up then the first thing people try to do is make it braun dead just like ms windBlows i do wish people would leave well alone the art is to educate the employees not make there computers dead mutants ,,,, GROW UP PEOPLE ..! for crying out loud this IS SUPPOSED TO BE THE 21st CENTUARY ! you know paperless office ect ..\n\nPete ."
    author: "Peter Nikolic"
  - subject: "Re: Good news..."
    date: 2001-08-14
    body: "Because there are always one, ONE, in every organisation that can/will not comlpy to admins and business rules. It's sad but it's a fact that not all peopel can be trusted with the power of a modern desktop"
    author: "Jo \u00d8iongen"
  - subject: "Re: Good news..."
    date: 2001-08-14
    body: "an old one: computers are made for people, not the other way around :)\nso if we make software we have to take into account it will be used by people."
    author: "ik"
  - subject: "Re: Good news..."
    date: 2001-08-14
    body: "Some people are happy being dead computer mutants.....it keeps people like us employed. :P"
    author: "Soulseeker"
  - subject: "Re: Good news..."
    date: 2001-08-14
    body: "Most likely the targeted users of such a system wouldn't necessarily be employees, but customers or the general public.  \n\nIf I want to deploy a set in the lobby of an office building to act as a building directory, I sure don't want random people opening up xterm on it.  Plus, deploying an old 486 in the lobby  loaded up with Linux is much more financially attractive than paying for a touch-screen system and the uber-expensive contractor to program it."
    author: "Larry Voyles"
  - subject: "Re: Good news..."
    date: 2001-08-15
    body: "This is nothing like Windows. Windows has zero security - you can tamper with anything on Windows. This should run the KDE session as non root and since shell and file system access is protected can be secure. The fact that you can avoid typing passwords into a kiosk machine which may be physically insecure to get access to services that don't need a password actually improves security over a standard system with a user login. How do you use a standard system login in a kiosk application where none of the users will have accounts? Log in to your own account and leave members of the public to tamper with it? Now that is Windows (NT) style usage (no security is Windows 98 style usage).\n\nThink of this as a really souped up graphical version of your typical boot-up menuing system, where your menuing system is the KDE desktop.\n\nThis is going to be really useful for things like POS stations, public information display systems, public appliance machines to do things like scanning, faxing, printing etc. The challenge will be preventing the applications from bypassing security. However it should be possible to adapt any open source application to be secure, and since you are only going to install a few specific applications, this should not be too difficult."
    author: "Dev Null"
  - subject: "Re: Good news..."
    date: 2003-12-15
    body: "I need a graphically attractive window manager, simple to use yet secure on linux boxes to develop for an internet caf\u00e9.\n\nI'm writing a daemon in Perl in the meanwhile to control external hardware that grant timed access to the box.\n\nI guess this is good stuff.\n\non the trusting matter.. how can you trust users that are trying to get surfing time for free?"
    author: "Ra"
  - subject: "Re: Good news..."
    date: 2001-08-15
    body: "I for one would like to start seeing things like airport kiosks, Per/hour Net Access terminals, Information kiosks, etc. running linux.  This allows for this functionality and could potentially expose KDE to the masses.\n\nInstead of telling people to GROW UP, you can think of this as KDE growing up and becoming a viable commercial alternative to Windows."
    author: "Josh Goldenhar"
  - subject: "Re: Good news..."
    date: 2001-08-16
    body: "the denver, colorado, usa airport has been using linux-based kiosk terminals for more than a year"
    author: "gary meyer"
  - subject: "Re: Good news..."
    date: 2003-04-30
    body: "YES!! I am working for a startup kiosk supplier, and as the head tech person, will have the final call on what to base our operations on. Initially it was Win2K, but the security issues, quality of software, expense etc. is a major put-off. Now I will have the opportunity to convert our existing and ongoing operations to linux. I've been using (playing with) linux since about '98, and have never been in the position where my employer trusted open source enough to use for commercial applications, until now.\nThe research is currently ongoing, and I expect to be in a position to offload all MS based software in favour of the penguin within 2 months."
    author: "Gerry Kavanagh"
  - subject: "Re: Good news..."
    date: 2004-10-05
    body: "Gerry,\nIs this you?\n\nAidan from Wales"
    author: "adoyle@lewishickey.com"
  - subject: "Prevent Ctrl+alt+Fx?"
    date: 2001-08-14
    body: "How is switching to a console with ctrl + alt etc. prevented?"
    author: "ac"
  - subject: "Re: Prevent Ctrl+alt+Fx?"
    date: 2001-08-14
    body: "Of course, to make the system \"secure\" one has\nto do more than just dealing with the\ndesktop environment, think about what\nyou can do in netscape or other applications.\nThis is not really the topic of this project.\nBut to answer your question, we allowed only\nroot to login on a virtual console, so\nthe user can switch but receives a permission\ndenied when trying to login.\n\ncheers"
    author: "Peter Kruse"
  - subject: "Re: Prevent Ctrl+alt+Fx?"
    date: 2001-08-14
    body: "It is not prevented. But only root can login on the virtual consoles. This can be done using pam\nwith an entry like\n\n-:ALL EXCEPT root:tty1 ttyS0 ttyS1\n\nin /etc/security/access.conf.\n\nNote: This is the path under Debian. No idea about other distros."
    author: "Roland Fehrenbacher"
  - subject: "Re: Prevent Ctrl+alt+Fx?"
    date: 2001-08-14
    body: "AFAIK this is an X-server-setting"
    author: "yves"
  - subject: "Re: Prevent Ctrl+alt+Fx?"
    date: 2003-03-21
    body: "I think you can do this by editing /etc/inittab.\n\nComment out the following lines with a #:\nc1:12345:respawn:/sbin/agetty 38400 tty1 linux\nc2:12345:respawn:/sbin/agetty 38400 tty2 linux\nc3:12345:respawn:/sbin/agetty 38400 tty3 linux\nc4:12345:respawn:/sbin/agetty 38400 tty4 linux\nc5:12345:respawn:/sbin/agetty 38400 tty5 linux\nc6:12345:respawn:/sbin/agetty 38400 tty6 linux\n\nI commented out the last two lines several months ago because it is much easier to type alt f5 than alt f7 and I never user tty5 or tty6.\n\nAlso, I would like to remind you that if this doesn't work, your system won't boot! Make sure you have a _working_ bootdisk and you make a copy of inittab before you change it."
    author: "Jeffrey Allen McGee"
  - subject: "Re: Prevent Ctrl+alt+Fx?"
    date: 2003-12-15
    body: "you can configure /etc/XF86Config for that.. check.. it's in the initial options :P"
    author: "Ra"
  - subject: "Re: Prevent Ctrl+alt+Fx?"
    date: 2003-12-20
    body: "DontZoom\nDontZap"
    author: "user"
  - subject: "A similar idea that would work better for some..."
    date: 2001-08-14
    body: "An idea I've been tossing around for something similar goes like this:\n\nModify Konqueror in such a way that you can pass it a parameter via the command line and have it run in \"restricted mode\". What this would do is disable all of the terminal/filemanager type abilities, and remove all of the KIO slaves except those that handle HTTP and HTTPS, allowing the user to *only* browse the web. Why is this ideal?\n\nConsider this situation. I have a neat little PC in my bedroom. When my parents come to visit, they sleep in my room. I don't want them to accidentally do any damage to my machine (which they are more than capable of doing -- don't ask how), and I want my dad to be able to hit Motley Fool and his other financial sites when he wakes up, and I want my mom to hit hotmail. All this requires is a browser. So I could run KDM, add a user with a simple password called \"guest\". This user would have no shell, and would not be allowed to log in remotely. When the user logs in via KDM, however, it starts X with no window manager, loads Konqueror in \"restricted mode\", and maximizes Konqueror on the screen. Thus, they are surfing a really dumb web terminal. Exactly what they need, no more, no less.\n\nMy 2 room-mates and I have a combined total of 6 Linux desktops scattered throuout the house. This feature would be useful on some of them. I'd love to implement this myself, if someone would only tell me where to start...\n\n--Aaron"
    author: "Aaron Traas"
  - subject: "Re: A similar idea that would work better for some..."
    date: 2001-08-14
    body: "as you say yourself, simply remove the other ioslaves, i.e. delete the .protocol (file.protocol) files and the actual ioslaves (kio_file.so)\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: A similar idea that would work better for some..."
    date: 2001-08-14
    body: "I wonder how your parents could running as a normal user (read, not root!) damage your system."
    author: "someone"
  - subject: "Re: A similar idea that would work better for some..."
    date: 2001-08-16
    body: "Surely this sort of thing could be done simply with the xml interface? AFAIK all kde2 applications have their menus and toolbars defined by a simple xml file so you could just edit it so that with profile management only a simple browser window is opened with say back, forward and home buttons. Any 'powerful' commands are simply not included in the menus or toolbars.\n\nI would imagine that most of kde could be closed up very neatly with this. Sure, people could do things if they really wanted but this sort of thing isn't meant to protect against technically competent people, its purely to make things simple for the masses by taking away all those 'confusing' options and to stop them accidentally bringing the system down\n\nAndrew"
    author: "Andrew Kar"
  - subject: "marketing"
    date: 2001-08-14
    body: "With a kiosk mode, this might be the correct way to have linux boxes on display at stores. M$ stuff locks you down so that you can't do anything at all. But a nice kiosk mode combined with a ro mounted home for KDE might be interesting."
    author: "a.c."
  - subject: "Re: marketing"
    date: 2001-08-15
    body: "It would be smarter to make a script that restores $HOME from a tar-file (or whatever) everytime the user logs out..."
    author: "Per Wigren"
  - subject: "I need to work on kde-kiosk"
    date: 2003-06-09
    body: "all links of kiosk mode and howtos are removed! i cant find it, i try to install kiosk-mode but it told me i need Qt libraries, but i think i really install everything! i work on Suse 8.2 but i can install a better distribution for work on it.\nSorry for my bad english, i am newbye on linux too thx"
    author: "Carlos from spain"
  - subject: "Re: I need to work on kde-kiosk"
    date: 2003-07-05
    body: "Check the KDE web site, www.kde.org\nthen search for kiosk.  for me the third\none down the page is the link to HOWTOs and FAQs.\nthere is a README on KDE Kiosk mode.  I am just\nstarting to look at a project myself, and thought\nthis might help.  Suerte!"
    author: "david in oklahoma"
  - subject: "Kiosk mode help"
    date: 2004-11-18
    body: "Hi dear,\n\nglad to see your acheivements.. i'm new to linux and at the same time have some tought tasks to perform. your help would be very appreciated.\njust wana do that as \" the system boostup there should be no login prompt and right away a web browser should be appear with some specified website.\n\nthen user hav no options to go outside of the browser. also no option to change the website address. + (no minimize,maximize,or close the browser)\nand no permission to perform any task e.g. open any other application etc.\n\nplease help me in this problem.\n"
    author: "jack"
  - subject: "Re: Kiosk mode help"
    date: 2004-11-18
    body: "http://mail.kde.org/mailman/listinfo/kde-kiosk"
    author: "Anonymous"
---
After two years of working with KDE, we think it is now time to share the results of our efforts to create a restricted KDE as part of our Linux-based thin client project that is now nearing completion.  A <a href="http://www.brigadoon.de/peter/kde/t1.html">write-up</a> of our design and strategy is available as well as the <a href="http://www.brigadoon.de/peter/kde/">patches</a> that we used to customize KDE to our needs.
<!--break-->
<br><br>
In a restricted desktop (kiosk-mode), the goal is to prevent the user from:
<ul>
   <li> opening a shell,
   <li> running arbitrary commands,
   <li> modifying files directly, or,
   <li> having a view to the filesystem.
</ul>

the user can:
<ul>
    <li> run applications provided by the administrators, and,
    <li> configure the desktop to a certain degree.
</ul>

KDE provides some functionality to achieve a part of this
(configuration files, environment variables, and alike).
There are certain requirements however, that can only be met by modifying
the KDE sources.  The patches can be found <a
href="http://www.brigadoon.de/peter/kde">here</a>.  Read the details and
further instructions in
the <a href="http://www.brigadoon.de/peter/kde/t1.html">KDE Kiosk Mode
HOWTO</a> that will also be submitted to <a
href="http://www.linuxdoc.org/">the Linux Documentation Project</a>.
It is our hope, that the idea of a restricted desktop will be merged into
KDE 3.0 as we know that there exists a kiosk patch for konqueror as
well.  It is our belief that this is just the thing admins need in a
big enviroment.