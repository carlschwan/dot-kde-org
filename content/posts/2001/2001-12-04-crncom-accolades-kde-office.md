---
title: "CRN.com:  Accolades for a KDE Office"
date:    2001-12-04
authors:
  - "Dre"
slug:    crncom-accolades-kde-office
comments:
  - subject: "Now is our turn to hype..."
    date: 2001-12-04
    body: "We do not have the luxury or TV commercials with flying people and all that crap, but we do have something M$ dosent have.  A very dedicated developerbase, who put long hard hours in for no immediate returns.  We also have something else that M$ lacks, we are all sysadmins, developers, insiders so to speak.  What if every one of us scheduled 15m inutes with our managers or development teams in January to show off KDE 3.0?  What if we all got out there and sent press releases to every tech publication we subscribe to?  What if we actually got out and sold our work? \n\nThis is what kind of PR KDE gets with no PR machine, what would happen if we made a noise this release?  I am not talking Slushdot or here either, if you are a linux user and have no idea what KDE is about then you have no pulse.  What we need are press releases and noise in the direction of main stream news, aka Forbes or Wall Street Journal.  Being in InfoWorld or ZDNet is a start but lets shoot bigger...\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Now is our turn to hype..."
    date: 2001-12-04
    body: "The Wall Street Journal's Walter Mossberg might be interested in doing an article about the open source desktop.  He is not a big fan of Microsoft (though he gives credit where credit is due), and he is aware of Linux.  His articles are always clueful and informative.\n\nBe warned though:  If KDE starts being reported on in the mainstream press, its flaws will be reported too.  If Mr. Mossberg did do an article, he would probably rate Linux/KDE somewhere below Windows XP.  He would comment on its speed, imperfect Javascript capabilities, inability to run most popular Windows programs, nonexistent device driver management, and other deficiencies of KDE and Linux.  If you really want KDE in the mainstream press, you have to be prepared for stuff like that.  Personally, I think we should wait until KDE 3.1 or so, to let some things be straightened out."
    author: "not me"
  - subject: "Re: Now is our turn to hype..."
    date: 2001-12-04
    body: "Well I guess that is the price we pay.\n\nBut maby if we start getting into mainstream media, we can get more industry support.  I mean look at what David and Waldo* can do in a days work, what if we could get more core developers on a payroll.  We are not flawless, but I think we are ready to move out of the garage/basement and into the office.  We have a fighting chance here, and we have tallent to match.\n\nI am more than willing to take a few critisims on this project if it means I can get payed to work on this project.  Heck I take critisim for working on it for free ;)\n\n-ian reinhart geiser\n*I know I forgot a few others, please forgive me, David and Waldo where the first two I could think of."
    author: "Ian Reinhart Geiser"
  - subject: "And what _about driver management?"
    date: 2001-12-04
    body: "Is there any work being done to create a uniform way of configuring all devices in Linux?  Windows does this very, very well.  And it's not the lack of drivers, it's the widely varying ways of installing each type of device.  The only project with the clout and means to do something like this is KDE."
    author: "Chris Bordeman"
  - subject: "Re: And what _about driver management?"
    date: 2001-12-04
    body: "KDE != Linux\n\nRepeat 20x or at least until you remember that KDE runs on Net/Free/OpenBSD, Solaris, HPUX, and Tru64. In addition to PPC.  Configureing your ethernet is your dists job not ours.  Makeing sure you can manage your files/documents is our job.\n\nReally we have a great number of frontends to these tools, but they are silly and a bad idea.  I maen the lilo config control panel is useless to me on my Alpha and PPC, and Mandrake has a better tool for it anyway.  Unlike winders we are not the only game in town, we are just the frontend.  This makes it very easy for end users to confuse things.  Really setting up your box is why you buy a dist, for KDE to waste such an effort on stuff like this has proven useless.  Yast2 and DrakConf are two very mature frontends to systems, if you lack them, then that is your fault for not choosing a dist based on your needs.\n\n-ian reinhart geiser\np.s. the same goes for the \"KDE Installer\""
    author: "Ian Reinhart Geiser"
  - subject: "Re: And what _about driver management?"
    date: 2001-12-04
    body: "Newbie Userbase == Linux\n\n<RANT>\nI've given up on distros solving the hardware and software management problems. They intend to use these features to compete and the hardware companies can\u0092t distribute drivers/software for every distro and it\u0092s version number. And due to the Distros need to pump out new versions and drop support for older to make money think the whole RPM system has collapsed under it's own weight. Nowadays I compile software myself, as it is quicker than finding a RPM that fits the fraction number of the distro I'm using. I will switch to Debian as soon I find the time to reinstall, as I've heard a lot of good thing about aptget. I think it\u0092s time for the Linux community to take control of it\u0092s own destiny.\n</RANT>\nphew..."
    author: "Fredrik C"
  - subject: "Re: And what _about driver management?"
    date: 2001-12-04
    body: "> KDE != Linux\n> \n> Repeat 20x or at least until you remember that KDE runs on\n> Net/Free/OpenBSD, Solaris, HPUX, and Tru64. In addition to\n> PPC. Configureing your ethernet is your dists job not ours.\n\nHi,\n\nWhy exactly do you think this is so?  Take your ex. of IP config.  Don't all distros need same info for this?  Just use a configurable .desktop file (put instructions in XML file maybe) where a distro can put the command to run for its system.  Mabe something like\n\nExec=distroipconfig netmask=%{netmask} ipaddr=%{ipaddr} device=%{device} . . .\n\nSame holds true for X server, video card (all distros use X, no?), etc.\n\n> Makeing sure you can manage your files/documents is our job.\n\nIf that were all KDE did, there would be a lot less there.\n\nWhat drives this, seems to me, is distros competing.  They compete on install/sysconfig, so these tools are gone from KDE.  Result:  all systems have different install (not so important) and config.  Result for distro:  bad for all but one (the one that wins).  Result for Linux:  bad in the meantime (till there's a winner), as one distro user cannot help another (fragmentation, just like w/ Unix of old days, which everyone agrees was bad for Unix -- why is this not bad for KDE?)\n\n--snip--\n\n> Yast2 and DrakConf are two very mature frontends to systems, if you\n> lack them, then that is your fault for not choosing a dist based on\n> your needs.\n\nHmm, this is surprising to hear from you, ian.  Yast2 is not Open Source and DrakConf is writtin in GTK."
    author: "Sage"
  - subject: "Re: And what _about driver management?"
    date: 2001-12-04
    body: "As for config stuff, on my PPC i use XPmac, totally different config file, same with yaboot.  I know Tru64 and Solaris also have some funky config options.  Your ethernet card is not always eth0 on all systems and afaik X != Xfree86 on all systems.  I use MetroX on one of my clients systems, it is much faster than XFree86 (compaired to X4.0.1 so this may have changed) and from what I remember that config file is too different, but they have their own tool for that.\n\n>Hmm, this is surprising to hear from you, ian. Yast2 is not Open Source and DrakConf is writtin in GTK.\n\nWhy would this matter?  Why would this surprise you?\n\nI use many tools that are closed source and really I dont care what a dist writes their stuff in.  It would be nice if it was all QT/KDE but oh well.  That is not my concern, the fact that with Mandrake 8.1 I had my DSL setup in 10 seconds was very nice... Unlike MacOS I did not have to reboot to get it to install.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: And what _about driver management?"
    date: 2001-12-04
    body: ":: Yast2 is not Open Source\n\n    Which surprises me that there is no effort to make an Open Source, distro *and* OS agnostic \"do-alike\" of Yast2.  Not a clone, but something along the same vein.  I've got extensive Solaris, AIX, BSDi and Linux experience, and I *know* it can be done - the underlying interfaces might be different in the details, but not so much so that a fairly common UI can't be built (okay, there are a few areas where there are big differences, but then seperate modules will just have to be written, and built on the basis of the target platform for the build).  I half heartedly poked at the problem with a long stick awhile back, using ash as a common shell for scripts (it's small, Free in every sense of the word, and runs on everything).\n\n    Of course, I feel somewhat guilty saying this - I pay for SuSE, and have no problem in paying for quality.  They should be able to stay ahead of the pack through sheer elegance of distro and excellent documentation.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: And what _about driver management?"
    date: 2001-12-04
    body: "you have obvoiusly never heard of linuxconf, or seen the nightmares those brave developers have gone through. i have watches linuxconf over the years with a ready eye, but i am not holding my breath any more.  really linuxconf is getting there, but it still munches it's fair share of config files...\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: And what _about driver management?"
    date: 2001-12-04
    body: ":: you have obvoiusly never heard of linuxconf, or seen the \n:: nightmares those brave developers have gone through.\n\n    I'm very familiar with it, and I wouldn't use it to touch my system... I'm a bit fan of jumping into the /etc directory tree with a CLI, a good editor and a keen eye.  But, that's just me, and I'm not a typical user.  I never said it would be easy (of course, I'm not looking at my post - maybe I did say that by accident), that's why I pay for SuSE and Yast2... quick convenience, and each user can change the basics without calling my number.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Now is our turn to hype..."
    date: 2001-12-04
    body: "Ian...you hit the nail on the head.\n\nWe are lucky in the fact that we are a team of developers in a unified viable direction in which we can develop software that we want. The people developing software for the people.\n\nI am happy to be a part of the KDE team, and I feel happy to work along with some trult talented people. I am not saying we can rule the roost with KDE and tha we can knock the big companies out, but at least we can say we have had a go.\n\nWorking for KDE is rewarding and seeing things such as this just make things even more satisfying.\n\nAnd the band played on..."
    author: "Jono Bacon"
  - subject: "Re: Now is our turn to hype..."
    date: 2001-12-04
    body: "How hard would it be to write a QT-NC staticly linked application that did a slide show of what KDE 3.0 had to offer.  Something we could show off at trade shows and such.  I played arround with a QT only KPresenter viewer some months ago, but all it did was played the web export images.  It was a verys simple tool, but really, maby that is something we could follow up on...\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Now is our turn to hype..."
    date: 2001-12-04
    body: ">We do not have the luxury or TV commercials with flying people and all that >crap, but we do have something M$ dosent have. A very dedicated developerbase\n\nand also dedicated UserBase ;). Yesterday I saw windows XP, and didnot find it great. the only thing XP is good at is SPEEEEED, which I believe KDE 3.x will surpass. The icons in XP look ugly and blurred, KDE icons are super cool, but dark. I will be adding a bright icon theme soon...\n\nOh, please KDE must get _very_fast_."
    author: "Asif Ali Rizwaan"
  - subject: "Luna style"
    date: 2001-12-04
    body: "The luna style is not that great but can be created within a day by our style devlopers. any future hopes of a luna style?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Luna style"
    date: 2001-12-04
    body: "Here are a couple:\n\nhttp://www.kde-look.org/content/show.php?content=155\nhttp://www.kde-look.org/content/show.php?content=524\n\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Luna style"
    date: 2001-12-04
    body: "Whatever happened to \"Innovate, dont imitate?\" Come on, why is everyone to do with KDE obsessed with Aqua. Some people (eg me) don't like it all that much. Neither do I like the WinXP interface it looks like it's made out of flipping Playdoh.\n\nLook at the GTK BlueMarble (bluesteel derivative) style, now THAT is a nice looking theme - not too heavy and rather consistent. I hate to say this but KDE may have all the infrastructure but as far as appearance goes, GNOME pulls K's pants down and kicks it from all directions. It's not that hard to come up with an original style; mayhaps this could yield the final edge that KDE needs to really pull ahead? (flame me for this if you will but my personal opinion is that GNOME's a waste of time. And I've used it for a year in various contexts so it's not like I'm underinformed)"
    author: "Anonymous"
  - subject: "Re: Luna style"
    date: 2001-12-04
    body: "Whatever happened to \"hey, I like that color - I think that's what I'll paint my house\".\n\n    Hey, I don't like Luna either.  I also don't like Aqua, Liquid or Win95/98.  Platinum is okay, but I personally use the QNiX style and the Web Window Decoration.  Boring?  Yes.  But I like it.  I fiddle with the colors far more than the style or icons.\n\n    But, who am I (and who are you) to tell people what they should and should not like?  It's a matter of taste, and *someone* liked Luna enough to ship it.  If you like a GTK theme, why not use it with KDE?  Assuming it's a pixmap theme, that is.  Or if you like an IceWM theme, use that.  Or even if you have a Windows theme you like, use it.  There.  Happy now?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Now is our turn to hype..."
    date: 2001-12-04
    body: "Actually anyone who as used winders 95 or 2000 can tell you XP is ssssllllloooowwwwww....  Really the big problem with KDE has to do with gcc and the way LD works.  This is an old subject and no longer open for debate.  There are fixes on the horizen, but we really are at the mercy of the gcc guys on this one.  As a side note I got to see SUSE's KDE packages in action, and it flies compaired to mandrakes packages.  Is this object prelink at work, or some other voodoo?\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Now is our turn to hype..."
    date: 2001-12-04
    body: "What about Intels compiler? There is an article in the latest issue of c\u00b4t magazine. C++ code  is 3 times faster than gcc, if this means anything to KDE, I don't know. Free for 30 days, worth a try?"
    author: "reihal"
  - subject: "Re: Now is our turn to hype..."
    date: 2001-12-04
    body: "But reality dictated otherwise. Rikkus tested it and found it not to be so spectacular.  AFAIK it is very nice for small projects, but not so hot for large libraries.  I could be wrong though.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "exactly"
    date: 2001-12-04
    body: "Wow Someone actually gets it. There has to be promotion and advertising to the General public or KDE/Linux will never reach the main stream. KDE/Linux's Technical superiority is not enough.\n\nCraig"
    author: "Craig"
  - subject: "Re: exactly"
    date: 2001-12-04
    body: "WOW!!!!! Coming from graig.\nFinally tried KDE huh!"
    author: "me"
  - subject: "Re: exactly"
    date: 2001-12-04
    body: "No, you've got it the wrong way around.\n\nCraig is the pro-KDE anti-GNOME troll.\n*Wiggle* is the anti-KDE pro-GNOME troll."
    author: "Jon"
  - subject: "Re: exactly"
    date: 2001-12-04
    body: "Sorry... My Bad :o)"
    author: "me"
  - subject: "Re: exactly"
    date: 2001-12-04
    body: "I'm the Troll?"
    author: "Craig"
  - subject: "First, work out compatibilty with M$!!"
    date: 2001-12-04
    body: "Oke, imagine a small firm switching over to Linux/KDE. It has to survive in a M$ dominated world! The only way it can do this is if the compatibility with M$ \"standards\" are worked out. This is the main concern!! My hopes are that the release of KDE3.1 and StarOffice 6 (both scheduled Q1/Q2 2002) will make the big difference. For koffice, the main issue should be to develop more or less perfect M$ filters, both import as wel as export filters.\n\nSecond, Open Source movements should lobby with goverments to switch to open document standards for public accessable information. In the Netherlands (my country) I know that all goverments are M$ shops, but the open source movement is actively lobbying for this. Is there a active lobby in you're country?\n\nJust my .02"
    author: "Freek"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-04
    body: "> For koffice, the main issue should be to develop\n> more or less perfect M$ filters, both import as\n> wel as export filters.\n\nThe problem is the Microsoft document formats are proprietary. It's a lot easier to create compatibility with open, documented formats. On the long run, standardising on open formats is a lot more effective than supporting proprietary ones.\n\nWhenever I receive a proprietary document, I refuse it and ask for a file format which is open. That's just me, but it takes only a few larger bodies (such as multinationals or governments) to think and act this way to create a major shift."
    author: "Rob Kaper"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-04
    body: "This is unfortunately the truth.  To make matters worse with M$'s documents they have special information for embedding stuff too.\n\nIdeally what I see is someone sitting down with VB or PyWin and writeing a migration toolkit.  Something that converts the documents into a form KOffice can read.  Unfortunately people seem to never remember M$ has great toolkits to read and create office documents on windows.  Did anyone remember the OS/2 -> windows migration tool kit.  It was an OS/2 app.  Or the Netware to NT migration kit, it was an NLM from what I remember.\n\nI have the M$ Excell SDK for MacOS here, and writeing a KSpread export would be quite easy, since we have an open format.  I am not willing to write such things but someone will need to.  It is infinitely easier than trying to decode a M$ document under KDE.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-04
    body: "The problem is that there is no open format that can be read by several office suites. I mean, which format will you export your text if you want people to be able to read it ? .kwd ? Few people have linux and even fewer have koffice. We need a format that would be usable by everybody. The only one is HTML but it's not usable for true wysywyg except if you fully use CSS... but it still depends on your browser's CSS implementation."
    author: "Julien Olivier"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-04
    body: "Yes, this has been talked about over and over and over... well you get the point.  The fact of the matter is it will NEVER EVER IN A MILLION YEARS happen.\n\nThat is what ASCII was suppose to be, but then that was not good enough, they wanted page formatting... then object embedding... etc...\n\nM$ embeds OLE and COM streams, KOffice uses KParts, this makes it very very hard to do.  Again this is why I say we need to do these things on the winders end.\n\nOffer a KDE migration toolkit. \n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-04
    body: "Imagine:\n\na universal conversion part that you could embed in any application. So you could view a .xls file into konqueror using the xls2html function or into kspread using the xls2ksp function etc... or exporting a web page into the .doc format (bad idea of course) using the html2doc export function.\n\nBut, in fact, if this was so trivial it would have been done for a while... it seems like it's not as easy.\n\nBTW, I'm ready to PAY for such a plugin as well as I'm ready to pay for Ximian plugin able to connect Evolution to a M$ Exchange server."
    author: "Julien Olivier"
  - subject: "Re: First, work out compatibility with M$!!"
    date: 2001-12-04
    body: "What I mean is, from a marketing viewpoint, Linux/KDE with koffice or staroffice is in a underdog position. Putting the compatibility problem on the M$ side is not a very good selling point for linux-based desktop deployment. That way, you're putting the problem one the side you are trying to communicate with. I agree with you that it _should_ be better to use open document standards, but the fact of the matter is that 90% of the office users use M$ formats.\n\nUse the same tactic as M$ is using to fight them: Embrace and Extend. Be compatible, but always default to open standards."
    author: "Freek"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-05
    body: "That brings up a good point! Is there a good way to build a plugin for Ms Office that will let you read and edit Koffice or Star Office documents in Word? \n\nIf we can't build support for MS file formats, we might be able to build linux file format support for windows. After all the name of the game is compatability, but why does it have to be by Microsofts rules?"
    author: "richie123"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-05
    body: "yes to export a word document using python is painfully simple, but it is not \"playing by their rules\".  We are offering a \"Migration Path\".\n\nUsers want to use our product, but they cannot for blah de blah reasons.  By offering the ability to move to our platform easier we get them.  I think by offing this path we gain far more than trying to support their products on our system.  When we support a word document we justify it as a standard.  When we do not support a word document but offer an Office plugin to export to KWord perfectly, then we deny them their standard.\n\nDoes that make sense.\n\nRemember the US colonies used British guns to kick them out.  Using Office against itself is poetic whatever you want to call it.  Besides I KNOW there are a few guys on the list and irc that know VB and COM but want to help KDE.\n\nCheers\n-ian reinhart geiser\np.s. I ran accross a python example that showed how to convert Excel documents to QuatroPro some time ago.  The coolest part about it is that it could convert formulas."
    author: "Ian Reinhart Geiser"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-05
    body: "> Does that make sense.\n\nWell, in theory you are, but you're putting the problem again on the wrong side. If you want KDE on corporate desktops you're fighting from a underdog position. Suppose you're working for a KDE based company and you're sending a commercial offering by Email: \"Hello customer, I hereby send you three attachments: 1) Our Offer, 2) a kwd2wrd plugin 3) the installation instructions to install this plugin on you're system. Changes are you're loosing this customer, fast! If you can talk him/her into installing it, changes are he/she can't because their coorporate desktop is locked down by their central system admins.\n\nAs long as you're in a underdog position, you have to provide compatibility on you side.\n\nFreek"
    author: "Freek"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-05
    body: "AFAIK PDF is very universal and KPrint can print right to them.\n\nIf companies want to read word documents now they must install a reader or get the latest word.  It is just that you have to decide we are not a garage project, but a legetimate project that has a future, and should be taken seriously.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-05
    body: "That brings up a good point! Is there a good way to build a plugin for Ms Office that will let you read and edit Koffice or Star Office documents in Word? \n\nIf we can't build support for MS file formats, we might be able to build linux file format support for windows. After all the name of the game is compatability, but why does it have to be by Microsoft's rules?"
    author: "richie123"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-05
    body: "That brings up a good point! Is there a good way to build a plugin for Ms Office that will let you read and edit Koffice or Star Office documents in Word? \n\nIf we can't build support for MS file formats, we might be able to build linux file format support for windows. After all the name of the game is compatability, but why does it have to be by Microsoft's rules?"
    author: "richie123"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-04
    body: "> The problem is that there is no open format\n> that can be read by several office suites.\n\nI actually remember talks about creating one document format, shared by OpenOffice, KOffice and AbiWord.\n\nKWord documents are nothing more than .tgz's that include a XML document and where appropriate the images/files embedded in the document. The entire format is documented (the DTDs and such are available from the KOffice website or else somewhere on developer.kde.org) and it should be fairly easy to write a plug-in for MS Word to read/save .kwd documents  (if MS Office has the appropriate capabilities of third-party import/export plugins, not sure about that).\n\nIf the new, unified format is no vapour and perfectly documented I don't see why it couldn't become a player in the document formats. Ogg might not be mainstream, but there is plenty of support for it, even in non-OSS environments (WinAmp). PNG might not be mainstream, but I'm sure Photoshop and alike programs support it. Linux is not mainstream, but viable enough for WordPerfect to release their product for it. If this open document format becomes as popular as any of these, there will be support for it beyond the borders of OSS-land. Imagine WP and the branded StarOffice including support for this format..\n\nIf we can offer something better than .doc, supported by a wider range of applications from various vendors, we can remove the argument that a desktop environment can only be viable when it is compatible with .doc documents."
    author: "Rob Kaper"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-05
    body: "> I actually remember talks about creating one document format, shared by \n> OpenOffice, KOffice and AbiWord.\n\nYes, pleeeease. That'd really be a good idea! \n\nBut what about embedded stuff?  In a post someone mentioned \nKParts. Would that make any sense on a different installation (without KDE)\nor even on a different platform?"
    author: "ghw"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-04
    body: "Why not adapt the Open Office XML format? It word be a big step in the right direct towards a standard."
    author: "Craig"
  - subject: "Re: First, work out compatibilty with M$!!"
    date: 2001-12-05
    body: "Not only that but you would be able to take advantage of Open Office's import filters which work a bit better on Word importation...."
    author: "AC"
  - subject: "Selective text in toolbar buttons"
    date: 2001-12-04
    body: "I have created a small icon theme with only one icon ;), the image is attached. do you think that having text in toolbar buttons will ease some work of users? I think big horizontal back button is rather easy to click."
    author: "Asif Ali Rizwaan"
  - subject: "and the theme is here..."
    date: 2001-12-04
    body: "use 'kcmshell icons' to install it. it has only one icon, the 'back' one."
    author: "Asif Ali Rizwaan"
  - subject: "Re: and the theme is here..."
    date: 2001-12-04
    body: "Better to use the thumb button. (Oh, does that only work with Logitech mice in windows?)"
    author: "reihal"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "Then we'll need translated icons. Shudder.\n\nUwe"
    author: "Uwe Thiem"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "ooooouch... bad bad bad...\nNEVER EVER put text in an image, do you understand that KDE is translated to over 20+ languages?  I mean kde-i18n is big as is, start throwing images in there and you have a mess.\n\nI would just turn on text, and it will do what you want.\n\nI have attached a picture.    I am not sure if there is a way to do this globally, but you can see, this is very bloated in the UI.\n\nCheers,\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "I think what he's trying to say is that it would be nice from the toolbar menu to be able to choose which text labels get shown instead of just displaying them all. Some buttons used frequently, like \"Back\", \"Reload\", \"Home\" would be easier to hit and understand (especially for newbies) is this was possible. I hate to say it, but IE provides this, with certain buttons displaying the text label beside them.\n\nI think it's a wonderful usability enhancement.\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "i think you forgot to look at the screenshot.\nit even works in Hebrew and Chinese.\n\nunlike IE though, we have this available in ALL kde applications.\n\ncheers\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "Here is an example:"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "gah, forgot the pic"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "gah, forgot the pic"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "Sure, I know that is possible, but this here is something different.  What I mean is to only display _certain_ icons' text labels, not the full toolbar. Most of the time if you use both text & icon for the full bars, it always goes off the screen. I made a screenshot in Windows to represent what I mean. IE calls this ability \"Selective text labels\" as can be seen in the dialog box. What we could do is perhaps have in the toolbar menu a checkbox next to each icon you would like to have the text label displayed for as well.\n\nHave a look.\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "i dont get it... \ni mean why would we break the UI like that?\n\ni mean if you want a broken UI use windows, consistancy here is the best way for a usable UI.  to do what is proposed is so wrong on so many levels it is amazing people consider the idea.  i mean this is impossible to translate, breaks the UI, causes the toolbar to not resize correctly... etc..\n\nin short, just because M$ breaks things, dosent mean we need to do so.  m$ sets no standards here, years of research have.\n\nin summary, if you want windows use windows, if you want something different use KDE.\n\n\ncheers\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "I don't know that it would break the UI. I don't want to argue about UI theory, I'm just trying to explain what the original poster mentioned. All this would do is make it a little more FLEXIBLE, so say for instance 3 out of 8 icons on a toolbar are confusing to my mom, I could configure it to show the text labels as well as the icons for _those_three_icons_only_. From what I can tell, it seems like a fairly simple and reasonable feature request. Text wouldn't be part of the graphic, it would just be pulled from the same XML file as it would any other time, keeping translations, etc. However, I cannot code in C++ or Qt so I am not able to make this happen (though one day I might..).\n\n*shrug*\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-05
    body: "pssst...  Show Mum the trick--\"touch\" the icon, oooh, tool tips!\n\nGive a man a fish...\n\n-pea"
    author: "Paul Ahlquist"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-05
    body: "That is not a broken UI, that is a Good Idea(tm).  It:\n\n1.  Uses the already-translated text, there is no text in images\n\n2.  Makes the frequently used buttons bigger while still allowing a decent number of buttons on the toolbar without those retarded menus at the right side\n\nThe only problem I see is in configuring which buttons get the text and which don't.  There would have to be a special checkbox or something in the Customize Toolbar dialog to enable the text on the buttons you want.  The way IE does it, you can't change which buttons have text when you're in this \"selective text\" mode, it chooses for you (although you can choose to have all text buttons or no text at all)."
    author: "not me"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-05
    body: "Hence the broken UI.  There is no consistancy, who decides what gets text and what dosent?  How do you configure it?  Anyone who can actually code or knows about UIs can see that this quickly would turn into a nightmare.\n\nYou only use what M$ has trained you to use, so you need to be retrained.  To retrain you need consistancy.  Look at winders 2000s random hideing of menues.  \nIdeally in the feild of UI designe you keep these things in mind:\nConsistancy \n\t(do not change things arround in the UI randomly)\nSimple metaphores \n\t(clean simple icons, with clear documentation as to their meandings with \"whats this\" tips or html help)\nNon Cluttered UI \n\t(humans mentally break down after provided more than 7 options)\n\nIf you really want to get crafty you can read up on what the Israilies are doing in UI science, with analyzing button placement and icon size.\n\nThis is all old hat though, and anyone by sophmore year in a Software Engineering degree would know this.  In short, pretending to be just like M$ is not a good idea.  Following what research has stated is.\n\nThis notion of the computer (randomly) customizing the UI to fit you has failed multiple times in the past.  Both in the market place and in hard science.  \n\nCheers\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-05
    body: ">There is no consistancy, who decides what gets text and what dosent? \n\nThere is no logical reason to NEED this particular consistency here.  Nothing is being \"changed randomly,\" the buttons that have text never change.  It is OK for some buttons to have text but others not to have text.  The reason to have consistency is to avoid confusing the user, but this doesn't confuse anybody at all.  It simply makes the frequently-used buttons bigger while still allowing space for lesser-used buttons.\n\nIdeally the user would decide which buttons get text through the toolbar config, which MS doesn't do but KDE could.  Reasonable defaults would be provided.\n\n> This notion of the computer (randomly) customizing the UI to fit you has failed multiple times in the past.\n\nThere's nothing random about it!  It is always the same, even between different computers.  The computer doesn't do it, the program designers do it.\n\n> Look at winders 2000s random hideing of menues. \n\nA totally different issue.  I agree on that.  It is random and inconsistent.\n\nAnd don't get me started on \"What's this?\" help.  In my opinion \"What's this?\" help is a TOTAL FAILURE!  I cannot recall the last time I used it and learned something important.  There are a number of reasons:\n\n1.  I never remember to use it because to use it you have to click that tiny little \"?\" next to the window minimize button, of all places!  Whoever thought that up should be shot.  Help deserves a *large* button *inside* the dialog.\n\n2.  Its use is so inconsistent.  I would be surprised if 40% of applications/dialogs actually used it.  To make matters worse, the \"?\" button appears some of the time even when there is no help.\n\n3.  Using it is clunky.  To find out if there is help, you have to follow a slow procedure of clicking the (tiny) \"?\" button, then clicking on a widget.  If no help appears, you have to go back to the \"?\" button to re-activate the help before you can check another widget for help.  (there's probably a button you can press to keep the help active, but I don't remember it and I doubt many others do either).\n\n4.  It is often redundant or uninformative.  Many times it contains the same information that is in the widget caption or tooltip, only in complete sentences instead of two or three words.  What could be more useless?"
    author: "not me"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2004-10-09
    body: "> Hence the broken UI. There is no consistancy, who decides what gets text and what dosent? How do you configure it? Anyone who can actually code or knows about UIs can see that this quickly would turn into a nightmare.\n\nThis decision would be made by the user. As from a general programming standpoint (I don't have actual KDE programming experience) it would require about an additional boolean flag for each button (ie. 'show label').\n\nThis feature is not specific to M$ applications at all, and I also don't agree on that it would introduce inconsistency (if we take it strictly, there is some already: the 'location' field has a text label next to it, regardless of the toolbar setting :)\n\nIt would rather be another feature that increases flexibility and useability, what we like KDE for :)\n\nimre\n"
    author: "Imre Tuske"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "exactly, I want to be able to enable text of different toolbar actions; which has already been translated."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "In fact, I think the role of icons is not to have text.\nI mean, why we use icons is to enable anybody to understand its purpose even if he/she can't read.\n\nWhy do you think they use a slashed cigarette to say \"no smoking\" ? They could just write \"no smoking\" but the slashed cigarette is better because anyone can understand.\n\nElse, we can suppress icons and just write labels..."
    author: "Julien Olivier"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "or read the documents and find out there is a RMB option to do all of this all ready.\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Selective text in toolbar buttons"
    date: 2001-12-04
    body: "<I> Why do you think they use a slashed cigarette to say \"no smoking\"? They could just write \"no smoking\" but the slashed cigarette is better because anyone can understand. </I>\n\nActually we use icons because 1) people want color pictures rather than dull text, and 2) they take up less space than text.  Few if any (bar the absurd \"no smoking\" example) icons are easier to understand for a native speaker on first use.  Even the printer icon takes longer to recognize than the word \"print\".  A lot of people would dump an app without even trying it if it came up in grey/black with only text labels."
    author: "Anon"
  - subject: "Build it and they'll come"
    date: 2001-12-04
    body: "Do we really care what the press does?\nBuild an outstanding product and the press will seek out and interview\nall those involved and bestow Kudus and monetary rewards.\nBUT, we need an outstanding product first.  KDE 2.2.2 is getting there,\nKOffice is almost there.  Konqueror is there but email addresses are cumbersome. \nWe need device driver support which beat M$.  We need a good OCR, an AutoCAD equivalent, Thomas Brother map software support, etc...\nYes, we are involved.  We are complaining and writing Nasty Grams to Thomas Bros and Intuit for selling software which will not run without IE.  I refuse to buy any software which is dictated by greedy M$ crap or includes the stupid IE in CD-ROMs.\nM$ is fighting for it's life by bribing and buying laws to benefit it's power.  They don't read history and don't understand that freedom always wins in the long run.\n\"It's a great time to be alive\"\nEd"
    author: "Edward Rataj"
  - subject: "Re: Build it and they'll come"
    date: 2001-12-04
    body: "Actually, I can imagine another way: Make Koffice available for Windows\nas soon as it's out of its infancy. That will provide companies with a\nfree alternative without people having to learn a new desktop. That way\nthe hurdle to overcome will be lower. Once the office is ours, taking\nover the desktop (including the underlying OS) will be much easier."
    author: "Terman"
  - subject: "Re: Build it and they'll come"
    date: 2001-12-05
    body: "how about we become a good office package for linux instead of yet another office package for Winders...\n\nreality people, noone can compete with M$ on thier own turf...  no way no how... show me one company that has pushed M$ back... AOL is about it, but even they are running scared from the desktop.  I think the Viso guys tried but M$ bought them and Quicken is doing everything M$ wants to keep Money from killing them...\n\nEvery product we have on Linux M$ cannot touch.\n\nEvery company that supports a product on Linux M$ cannot touch.\n\nM$ will never write Linux software, they have tried and failed twice now.  So if they want to compete they have to sell you the OS and the application.  They all ready do, so really this is where the fight is.  \n\nIBM knows this, every system that is based on Linux and Apache M$ cannot compeate with them.  The same will hold true for the desktop realm.  I think we are ready to start offering real companies a toolkit to start developing Unix Desktop applications.\n\nFact if you go out and write something inovative for winders M$ will buy you or run you out of town.  If you write it for Linux, they cannot touch you.  So all you budding software companies out there take note and release your software on Linux.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Build it and they'll come"
    date: 2001-12-05
    body: "Hi,\n\nThere's another company that delivers a kick-ass programming tool: Borland has\nreleased Kylix for a small year and I love that program. Okay it has some minor\nbugs, because it is very young, but after a few iterations it will scream!\nBorland has also given an 'Open Edition' for the community.\n\nI now have two computers. On the first computer a copy of Kylix 1 Desktop is\ninstalled and on the other Kylix 2 Open. Kylix 1 Desktop is a full-blown version\nwith full database support and Kylix Open Edition hasn't yet database support,\nbut this may change sooner or later. Kylix 2 Enterprise is able to create web\napplication or web servers. So this can be a reason to kick the ass of M$!!!\n\nBy the way Kylix is built on top of QT the same library on which KDE is built\nand that is very interesting. You can read more information at:\nhttp://www.borland.com/kylix\n\n\nM$ is a machinery of disrespect, discrimination and selfishness (money's only the theme);\nOpenSource is a society of Respect, Love 'n Understanding, working together for\na better world! (a better world is the theme)\nMerry Christmas and a happy 2002!\nTom"
    author: "Tom"
---
<a href="http://www.crn.com/">CRN</a> today published an <a href="http://www.crn.com/Sections/CoverStory/CoverStory.asp?ArticleID=31793">article</a> destined
to give KDE/Linux naysayers food for thought regarding the viability of KDE
as enterprise desktop software.  In this well-researched article by
<a href="mailto:fohlhors@cmp.com">Frank J. Ohlhorst</a>, the
CRN Test Center built a Linux network consisting of a server and five
workstations, with the goal of creating a reliable network that
could be used in a typical small-business environment.  The
Test Center's insightful conclusion:  &quot;<em>Linux and associated
Linux applications can accomplish many of the same tasks as the Wintel
standard at a much lower initial cost, in this case, for <strong>93 percent less</strong>
than the software cost of a similar Windows-based network, and without
many of the licensing hassles presented by traditional software
platforms</em>&quot;.  
As to choosing between the two major Linux desktop environments, the article
reports that &quot;<em>Test Center engineers found KDE the friendliest and
were impressed with the array of KDE-compatible software . . . KDE is the most actively developed Linux desktop and has the most
tools. Solution providers seeking KDE desktop open-source development tools
should go to <a href="http://apps.kde.com/">www.apps.kde.com</a>, which has
ratings on each product, including feedback from the user community</em>&quot;.
Good to see such a well-researched article, and more importantly that the non-Linux media is starting to realize the capabilities of our very
own KDE &lt;grin&gt;.


<!--break-->
