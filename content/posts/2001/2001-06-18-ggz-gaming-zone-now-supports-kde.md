---
title: "GGZ Gaming Zone Now Supports KDE"
date:    2001-06-18
authors:
  - "jspillner"
slug:    ggz-gaming-zone-now-supports-kde
comments:
  - subject: "Re: GGZ Gaming Zone Now Supports KDE"
    date: 2001-06-18
    body: "This is great!  But, does the kggz client need to be run first?  Or can one just select something like \"play against random player\" in a kde-game and then it will connect for you?   I believe that would be a good feature.."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: GGZ Gaming Zone Now Supports KDE"
    date: 2001-06-19
    body: "I'm not an expert on this, but from looking at a bit of the API docs, it appears that all the app has to do is accept some sort of external command (cmd-line probably, dcop maybe), and then connect to the given socket and hostname, using the provided GGZ library. GGZ is only the protocol, and the metaserver setup. How the app handles all this in user interface land is I believe up to the individual app.\n\nI would personally like to see it done both ways. I.e, if i want to play a particular game, I open it and have it player seek for me, but If i'm looking for any type, i could open up kggz.\n\nHmm, speaking of player-seek, perhaps there should be some sort of ranking system, tracked by metaservers so that ranks go up only when all participants send in equal results. That way, you get matched against people who are near your skill level."
    author: "Carbon"
  - subject: "Re: GGZ Gaming Zone Now Supports KDE"
    date: 2001-06-18
    body: "I sure hope the protocol used is very flexible, since it would be regrettable to be limited to low-bandwith games such as cardgames with few players..."
    author: "me"
  - subject: "Re: GGZ Gaming Zone Now Supports KDE"
    date: 2001-06-18
    body: "The current homepage for kmines is http://kmines.sourceforge.net ..."
    author: "azhyd"
  - subject: "Re: GGZ Gaming Zone Now Supports KDE"
    date: 2001-06-19
    body: "Okay, fixed.  Someone should update games.kde.org too.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: GGZ Gaming Zone Now Supports KDE"
    date: 2001-06-19
    body: "Done\n\nCU\nAndi"
    author: "Andi"
  - subject: "Re: GGZ Gaming Zone Now Supports KDE"
    date: 2001-06-19
    body: "Good, maybe some day I'll finaly see:\n\nKonqi's Adventure (thinking 2d side scroller, from a hacked up version of Pingus's code... Preferably to adventualy allow up to 4 people play?)\n\nKDE Party (think Mario party like... fun board game type thing with many fun sub-games, all getting you to the adventual goal of winning againts your 4 favorite friends or computers...)\n\nOk, I never said _I_ would program them...\n\nHowever, I'd say the first step would be to be able to compile the sources for Pingus under Kdevelop, and then work with that code from there...\n\nIf no one else does it, I promise to start within the next 10 years :)\n\nIf someone does start it, I promise to _try_ to help...  The word help is really a relative thing :)\n\nAbout the only thing I've programed is Tetris clones for Windows... And some bad 2d doesn't-scrollers. :)"
    author: "Gregory W. Brubaker"
  - subject: "Re: GGZ Gaming Zone Now Supports KDE"
    date: 2001-06-23
    body: "I have no practice in programming.... PERIOD.\nyet kde party looks good. I'm thinking....\n1. A bar at the top with the title and the four players and the color of space they landed on.\n2. a map much like MP in the center.\n3 At the bottom, a frame for chatting (messages displayed w/ charachter's name, followed by face, and then message in a speech bubble.)\nI suggest that whoever starts it use QT, and make the maps \"module-esq\" so that players can create their own maps."
    author: "Sco"
  - subject: "Included in the official distribution?"
    date: 2001-06-19
    body: "Will it be included in the kdegames package ?\nBefore or after kde2.2 ?"
    author: "renaud"
  - subject: "Re: Included in the official distribution?"
    date: 2001-06-19
    body: "There are plans or talks to integrate some portions in CVS.  And it won't be in KDE 2.2, since we're already frozen.\n\n(Sorry Josef, I did some edits for clarity and brevity, and dropped your comments on this issue)"
    author: "Navindra Umanee"
  - subject: "Re: Included in the official distribution?"
    date: 2001-06-19
    body: "No problem Navindra, if my English was really that bad :)\n\nThere are some upcoming libraries for the games section of KDE, and some source of it will be contributed by GGZ. If all apps like KGGZ will ever be part of KDE has yet to be decided, mainly because its requirements would either have to go into kdesupport then, or (as some people dislike kdesupport) will have to be \"emulated\". We're currently on discussion on that, I just had not much time during the last weeks, but I promised to start the actual work after 0.0.4 goes out, and now that we have it it's time to do so. Feel free to help."
    author: "Josef Spillner"
---
It's a well-kept secret that <a href="http://games.kde.org/">KDE games</a> are no longer toys but maturing entertainment applications. Games such as <a href="http://kmines.sourceforge.net/">KMines</a> <i>(link updated)</i> demonstrate the concept of global highscores as well as general plans to provide an online gaming framework. We're pleased to announce that the <a href="http://ggz.sourceforge.net/">GGZ Gaming Zone</a>, an open alternative to Microsoft's Gaming Zone, adds to the arsenal of KDE gaming resources with the 0.0.4 release. KDE frontends and games are now available (<a href="http://ggz.sourceforge.net/screenshots.php?op=SSsCategory&cid=4">screenshots</a>), with more planned in the future. Read on for an overview of GGZ in the KDE context.
<!--break-->
<br><br>
<h3>GGZ and KDE</h3>
GGZ brings the possibility of integrated networking to KDE games. Players will no longer need to worry about game servers or locating suitable online opponents: the applications should handle this automatically, while leaving finer-grained control to more  experienced users.
<BR><BR>
<B>Applications</B>
<BR><BR>
Currently, <i>KGGZ</i> provides chat and game launching capabilities, while <i>GGZap</i> docks to the panel and can locate a suitable online opponent upon request. More tools are planned in the future, including a <i>KGGZMetaServ</i>.
<BR><BR>
<B>Games</B>
<BR><BR>
At present, only 4 native KDE games are provided with GGZ but at least double that number (eight) is anticipated for the next release. A general collaboration with the <a href="http://games.kde.org/">KDE Games</a> project will ensure that there is no duplication of effort.
<BR><BR>
<B>Availability</B>
<BR><BR>
The packages can be downloaded <a href="http://sourceforge.net/projects/ggz/">from the project page</a> in source form, or as binary packages for Debian (apt-get: zone.berlios.de) as well as RPM-based distributions. The minimum required packages are: easysock, ggz-client-libs, kde-client and kde-games, all of version 0.0.4.
<BR><BR>
<B>Help Wanted</B>
<BR><BR>
Although the project is coming along nicely with UDP, IPv6, TLS support and (forthcoming) meta servers, your input and feedback is very much needed -- especially on the client side!
<BR><BR>
Josef
<BR>
(who is almost sure he has hit #1000 on <A href="http://apps.kde.com/">apps.kde.com</a>)