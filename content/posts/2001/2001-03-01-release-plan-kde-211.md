---
title: "Release plan for KDE 2.1.1"
date:    2001-03-01
authors:
  - "mmoeller-herrmann"
slug:    release-plan-kde-211
comments:
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "YOU GUYS ARE SUPPOSED TO RELAX FOR A FEW SECONDS AFTER A RELEASE!!!Honestly, I am impressed by KDE's development pace! Thank all you developers, your product really rocks!"
    author: "ac"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "Here's why\n\n1. Long answer:\n\ndict:relax\n\nOne entry found for relax.\n\nMain Entry: re\u00b7lax\nPronunciation: ri-'laks\nFunction: verb\nEtymology: Middle English, from Latin relaxare, from re- + laxare to loosen, from laxus loose -- more at SLACK\n\n<snip>\n1 : to become lax, weak, or loose : REST\n2 : to become less intense or severe <hoped the committee would relax in its opposition>\n3 of a muscle or muscle fiber : to become inactive and lengthen\n<snip>\n\n2. Short answer\n\nWorld domination :)"
    author: "me"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "I find KDE2 a really great product, it's free and looks way better than Windows!\n\nBut so far I've just seen one little problem: It's very big and downloading it with my 56K modem takes a quite long while. Wouldn't it be possible to release update packages so we don't have to spend hours waiting? Or maybe too many things are changed to allow an update?"
    author: "Vadim"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "This is already possible: via cvs. If you have the code once you just need to update either from time to time or when a major release is out (thus, by using tags)."
    author: "Dirk"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Is it possible to download stable releases from CVS? I missed out kde 2.0.1 because I too only have a 56k modem but, like many others I'm sure, I would love to be able to update the kde2.1 source to kde2.1.1 using CVS."
    author: "Dan"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "If you are already compiling cvs is very cool, not just because it only gets new or updated files but because it means you also only compile those parts of the code that are affected by changes. thies makes a cvs compile very fast.\n\nThe only additional command you would need is \"make -f Makefile.cvs\" prior to yoru configure. You only need this the first time or when directories have been added. \n\nCVS may be a little bit of a risk as it is bleeding edge but I've found KDE cvs to be exemplary code since several weeks before KDE 2.0 with very few exceptions. I suggest you always get kdelibs and base at the same time. Directions can be found on the KDE site and if you get Cervisia it becomes a walk in the park with the added benefit of being able to peek inside the code and really see what is going on if you like. ;-)\n\nPlus you have everything first, like the new kant editor and kprint libs."
    author: "Eric Laffoon"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "Is there any way of getting cvs behind a http proxy?"
    author: "Kde user"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "Why don't you try rsync, if I remeber well you can use cvs tag and proxy."
    author: "alonso"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "What is Kant editor? I mean, what kind of editor?"
    author: "Barracuda"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Kant is an editor based on the kwrite part with support for multiple documents.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Thanks, I've already compiled it. I like it very much.\nRegards,"
    author: "Barracuda"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "Keep up the good work. You guys rock!!!!"
    author: "George"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "2.1 really rocks!\n\nthank you!\n\nBut where's KOffice?"
    author: "Johne"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "KOffice is under massive development and redesign (especially KWord). For example, some very useful Qt3\nwidgets were backported so they can now be used with KOffice (and maybe some other KDE apps).\n\nAdditionally, it was the developers last Resort / Playground during feature freeze :)\n\nRegards\n\nDaniel"
    author: "Daniel Molkentin"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "backported to 2.2.4 or 2.3.0?"
    author: "Evandro"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "Either. The full story is here: <a href=\"http://dot.kde.org/982051267/\">Good news for KWord: Qt3 widget ported to Qt2</a>\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "<i>backported to 2.2.4 or 2.3.0?</i><p>\nActually, it's currently in the koffice source... possibly to be moved to kdelibs. Once Qt 3.0 comes out, it will (I assume) be moved there. <p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "I think they Mean Qt ver. <b>3.x</b> :)"
    author: "Mihail"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "If you see what happens e.g with Killustrator you get anxious to the improvements which asumably  could be expected. (hopefully some usefull importfilter from postscript will be implemented).\n\nIt\u00b4s  really usefull now, the new bezier-curve-tool is really good and I hope they go on!\n\nWolfgang"
    author: "Wolfgang Kesseler"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "Many seem to forget it, even the packagers, but you can certainly use the latest stable release of KOffice (as included with KDE 2.0) with KDE 2.1.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "Hey, I just wanted to know if there were any pics of the new cooler icons?? I would love to see the new icons. Thanks A Lot!"
    author: "Bakiller"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-01
    body: "Well, there aren't many new icons there for KDE 2.1.1 yet but I could show you a screenshot of how KDE 2.1 looks compiled from tarballs with Alphablending and 48x48-icons enabled.\n\nftp://www.derkarl.org/pub/incoming/screenie030101.png\n\nSome Netscape-versions have problems with png's so here comes a JPG for those ... :\n\nftp://www.derkarl.org/pub/incoming/screenie030101.jpg\n\nGreetings,\n\nTackat"
    author: "Tackat"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Tackat, you and other artist did a hell of a job. You gave KDE really modern look and feel. Congratulations."
    author: "Barracuda"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Thank god!  Has anyone got KDE 2.1 working on a RedHat 6.2 box with the RPMS?  It's very broken with RPMS and the src.rpms don't appear to be compiling friendly either."
    author: "Kevin Breit"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Nope, not yet, i agreee that the rpms for redhat 6x are broken, i tried them on my mandrake7.2 box (as we all know then mandrake developers have ben busy with 8.0 and havnt had time to make the packages) and the sound wouldnt work, and for some reason, i could only log in as root because under a normal user, kde apps were craching all over the place, well, then i decided to build kdesupport, libs, base and multimedia from source, and that fixed sound, but not loggin in as a normal user (still crashing) so i decided to wait patiently for the correct packages and install xf86 4.0.2, not that was definitely a big mistake because now both my kernels panic when they try to remount the root filesystem r/w (i can get as far readonly with init=/bin/sh from lilo)\n\nwell, the fun continues, i'll maybe have a go at fixing it tomorrow, otherwise it will have to wait until monday, when, more than likey the packages will be available, they'll just be no use to me :)\n\nahh, the the joys of linux, luckily i can fall back on good ol' winme :P"
    author: "Adam Pigg"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Ouch. I'm running a modified Mandrake 7.0 system. My\npersonal rule of thumb is to compile from sources wherever possible, and fall back to RPMs when the code wont compile even under your best efforts. Best\nof luck with getting your Linux system back up."
    author: "Sean Grimes"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "re \"Fall back on ol' winme\"...\n\nI had pretty much the same problems with Mandrake 7.2, so I went back to SuSE 7.0.  Installing KDE 2.1 on SuSE took 3min using the YaST tool and it works perfectly!"
    author: "Richard Bozzato"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "So did I. Its rock stable on SuSE 7.0, and fast."
    author: "Kde user"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Regarding crashing as normal user but not root, have you tried removing your ~/.kde dir (or just moving it to try it out)?\n<p>\nI've found that with various kde upgrades/changes, i've had to remove the old config files to get it working fine.<p>\nAs an added bonus, KDE seems a fair bit faster with a cleaned out .kde dir."
    author: "Matt Johnston aka mesh"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Yep, done that, it was the first thing i tried...\n\nanyone got any ideas why the kernel would panic when remounted r/w?\n\nC'mon, someone must have an idea\n\nmaybe its time to download mandrake 8.0b, the only reason my computer is like this now is because i wanted aa fonts, damn xfree86!)\n\n:)"
    author: "Adam Pigg"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Oh, one more thing, in kde2.1b2 (i had that one running fine ;) ) whenever i tried to play sound through arts, it was speeded up (eg mp3's or login sound) but without arts, they were fine (using xmms) i have a videologic sonicstorm and it had worked fine in previous version of kde 2.x\n\nany ideas?"
    author: "Adam Pigg"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Dear Pigg,\nI am running Mandrake 7.2 as a base system (because of great hardware detection), but all other things (including KDE 2.1) has been compiled. It is stable as a rock. Grab KDE 2.1 tarballs and compile them (come to #kde-users, so we can help you). Don't use RedHat 6.2\npackages on Mandrake 7.2 (nor Cooker ones). \nIf you have problems with sound and KDE 2.1, I suggest\nthat you come on #kde-users or subscribe to kde-user mailing list, and someone will help you there."
    author: "Barracuda"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "I have had the same problems with my box after updating to 2.1.  I am running debian and updated with the .deb's and whenever I use noatun some, but not all, of my mp3's are either speeded up or about half speed.  Looking at the song lengths that are reported it looks like maybe this is getting reported wrong.  I don't think is the arts output as I used xmms with the arts output plugin and it plays fine, so I think it is something in the time functions.  I have been looking for some sort of a connection between the mp3's that play okay and the ones that don't but can't find one.  I will keep searching, but if anyone knows what the problem may be, please let me know!"
    author: "Kmax"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "I have got it working perfectly\nIt has major speed imrovements over 2.1b2\n- First time i got icon text backgroung color working\n- also no crashes on image/html/text previews\n- looks slightly nicer too!\nOnly arts RPM seems to be broken\nMust try one that come with 2.1b2 I think!\n\nCheers!"
    author: "Yogeshwar"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "I have got it running on RH 6.2.\nI had to uninstall all previous QT and KDE 2.0 packages though.\n\nList below (plus QT package):\nkdesupport-devel-2.0.1-1\nkdelibs-sound-devel-2.0.1-1\nkdelibs-devel-2.0.1-1\nkdeutils-2.0.1-1\nkoffice-2.0.1-1\nkdetoys-2.0.1-1\nkdepim-2.0.1-1\nkdenetwork-ppp-2.0.1-1\nkdenetwork-2.0.1-1\nkdegames-2.0.1-1\nkdeadmin-2.0.1-1\nkdemultimedia-2.0.1-1\nkdebase-2.0.1-1\nkdelibs-sound-2.0.1-1\nkdelibs-2.0.1-1\nkdesupport-2.0.1-1\n\n2.1 is more stable, faster and slicker than any previous KDE release. Love it.\n\nHTH"
    author: "Pierre"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Maybe this is common knowledge, but it might save some time for people new to Linux.\nTo uninstall a whole lot of packages with something in common (i.e. the kde packages), you could use a command like this:\n\nrpm -e $(rpm -qa | grep \"kde\")\n\nThis will tell rpm to list all packages, then filter the list with grep to have only kde packages. This list is then given to rpm to uninstall (using -e)."
    author: "jliechty"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Sure, I run RedHat 6.2 with the 2.1 RPMs. I have been running RPMs since 2.0.1 without any (major) problems. I upgraded from 2.1beta using rpm -U.\n\nOf course, rpm said alot of crap regarding dependancy problems as it always seems to do. Those were \"fixed\" by the --nodeps flag to rpm ;-) and now everything seems to work just fine."
    author: "Adam Dunkels"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Sound doesn't work, this seems to be something to do with artsd not being compiled with plugins or something(doesn't bother me much anyway), IIRC the previous poster said run: 'artsd -A' for more information on available plugins and that reverting to KDE2.1b2 artsd package would solve this.  \n\nThe only serious problem I've encountered from my point of view is that kdemultimedia will not unpack correctly from the rpm and hence won't install.  \n\nI also seem to have a problem with the location that kdm wants to read from(/usr/etc/X11, instead of /etc/X11) and some of the files it copies into that directory don't work properly.  This results in the default horrible X background(kdmdesktop doesn't seem to start) and none of the sessions will load correctly.  I fix it by just 'cp -fR /etc/X11 /usr/etc'.  I don't know if that's such a great idea, but it hasn't done any (noticeable) harm yet!\n\nAs a side note, I have just installed Mandrake 7.2 on another computer I use regularly and I'm very impressed.  I'm currently running kde2.1b2 there without any major problems(well the whole menu structure seems a bit messed, but the wackiness has now settled and I once again have access to my kcontrol tree :-)"
    author: "Rob"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-02
    body: "Makes me glad I switched away from RedHat when they broke KDE in 6.0 and went over to SuSE - SuSE rpms for 7.0 downloaded, installed via YaST - no probs!\n- CyberKat"
    author: "CyberKat"
  - subject: "One request..."
    date: 2001-03-02
    body: "Is it too much to request... To get a video of a nice little konqi playing around, and then something pissining him off, causing him to morph into a big mean Konqi and bite someone's head off??? (morph from the rather humanish dragon to a dragon-dragon).\n\nKonqi is cool... we need to play with him.  I'd do it if I knew how."
    author: "Greg Brubaker"
  - subject: "Re: Release plan for KDE 2.1.1"
    date: 2001-03-11
    body: "Can't you guys make TrollTech change their \"configure\" defaults to those needed with KDE?\nI know it's my own fault that I compile QT each time wrong after a new release.  But I'm used to trust \"./configure && make\".\nI also know it's not your business, but you have the better connection to them,..."
    author: "Michael"
---
<a href="http://www.kde.org/people/waldo.html"> Waldo Bastian</a> has just <a href="http://lists.kde.org/?l=kde-devel&m=98340844504512&w=2">
announced</A> the proposed new release plan for the next official stable release: KDE-2.1.1.  
This release is in line with the KDE Project's recently adopted policy of issuing a bug fix release shortly after any major official release to fix any critical bugs quickly and to let translators and documentation catch up. 
 Read more in the official <a href="http://developer.kde.org/development-versions/kde-2.1.1-release-plan.html">
release plan</A>, which is reproduced below. 


<!--break-->
<P>
<H2>KDE 2.1.1 Release Plan</H2>
</P>
<P>
The following is the outline for our KDE 2.1.1 release schedule.
</P><P>
The primary goals of the 2.1.1 release are:
</P><UL>
<LI>Critical bugfixes</LI>
<LI>Completion of the translations in many languages</LI>
<LI>Documentation for almost all applications</LI>
<LI>Improvement of the icons (yes?)</LI>
</UL><P>
All dates given here are subject to revision, but we will try out best to
stick to them if possible.
</P><P>
David Faure is acting as release coordinator for the KDE 2.1.1 and 2.2 releases.
</P><P>
<H4>Monday March 19 </H4>
Last day for translations, documentation, icons and critical bugfixes commits
to the KDE_2_1_BRANCH.
</P><P>
<H4>Tuesday morning (GMT) March 20</H4>
The CVS is tagged KDE_2_1_1_RELEASE, tarballs are released to the packagers
</P><P>
The rest of the week is spent testing the tarballs, packaging and writing the
announcement and changelog.
</P><P>
<H4>Friday March 23</H4>
Spread sources and binaries to mirror sites.
</P><P>
<H4>Monday March 26</H4>
Announce KDE 2.1.1.
</P><P>
<HR NOSHADE>
</P><P>
<H2>Frequently Asked Questions</H2>
</P><P>
<EM>Q) Which packages are included?</EM>
</P><P>
A) The KDE 2.1.1 release is an update of the 2.1 release. The same packages
will be included. The list is:
</P><UL>
<LI>kde-i18n </LI>
<LI>kdebase </LI>
<LI>kdegraphics </LI>
<LI>kdemultimedia </LI>
<LI>kdepim </LI>
<LI>kdetoys </LI>
<LI>kdoc </LI>
<LI>kdeadmin </LI>
<LI>kdegames </LI>
<LI>kdelibs </LI>
<LI>kdenetwork </LI>
<LI>kdesupport </LI>
<LI>kdeutils </LI>
<LI>kdevelop </LI>
</UL><P>
<EM>Q) Which version of Qt should be used with this version of KDE?</EM>
</P><P>
A) Qt 2.2.3 and Qt 2.3 should work fine, but Qt 2.2.4 is recommended.
</P><P>
<pre>
--
bastian@kde.org | SuSE Labs KDE Developer | bastian@suse.com
</pre>
</P>
