---
title: "KDE Report: LinuxTag 2001"
date:    2001-07-12
authors:
  - "Dre"
slug:    kde-report-linuxtag-2001
comments:
  - subject: "Universal Components"
    date: 2001-07-12
    body: "Matthias, please publish your paper on Universal Components!  I'm sure many of us are dying to see it.\n\nHey Tackat, why not rotate those photos already?  :-)\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Universal Components"
    date: 2001-07-12
    body: "Ack!!! I'm getting a stiff neck from looking at the pictures.  Keep up the good work guys."
    author: "SD"
  - subject: "Re: Universal Components"
    date: 2001-07-12
    body: "I wish Konqi had the image rotate controls back by default.  Well, you can still do it if you use KView instead of the inline image viewer, I suppose."
    author: "Navindra Umanee"
  - subject: "Re: Universal Components"
    date: 2001-07-12
    body: "People, use KuickShow, <a href=\"http://master.kde.org/~pfeiffer/kuickshow/\">http://master.kde.org/~pfeiffer/kuickshow/</a>\n<p>\nYou can tell it to rotate images by default, although I admit, this should actually be Tackat's job :)"
    author: "Carsten Pfeiffer"
  - subject: "Re: Universal Components"
    date: 2001-07-12
    body: "Try a\n\nOption     \"Rotate\" \"CW\"\n\nin your XFree4 XF86Config-file (device section)\n\n:-)))\n\n--\n(Just kidding)"
    author: "Thorsten Schnebeck"
  - subject: "Re: Universal Components"
    date: 2001-07-12
    body: "Why \"just kidding\"? I tried it, it works.\nThe rest of the desktop is now turned as well, though.\nAny idea about how to fix that?"
    author: "Anonymous Coward"
  - subject: "Re: KDE Report:  LinuxTag 2001"
    date: 2001-07-12
    body: "There's a spelling mistake in the second headline.\nThe capital of Baden-Wuerttemberg is called Stuttgart...\n\nJuergen\n(who later found out that his company would have paid the trip to the LinuxTag and who thus still wants to kick his butt)"
    author: "J\u00fcrgen Nagel"
  - subject: "Re: KDE Report:  LinuxTag 2001"
    date: 2001-07-12
    body: "Thanks..."
    author: "Navindra Umanee"
  - subject: "Re: KDE Report:  LinuxTag 2001"
    date: 2001-07-12
    body: "Hey Rob Kaper, somehow I get the idea the random people on your pictures are mostly nice looking girls :)\n\nHowever, I really liked this one :\n\n\"The Jolt girl who kept me up for 70 hours (or at least her merchandise did)\"\n\n*very-big-grin*"
    author: "Jelmer Feenstra"
  - subject: "Re: KDE Report:  LinuxTag 2001"
    date: 2001-07-12
    body: "<i>somehow I get the idea the random people on your pictures are mostly nice looking girls :)</i>\n<p>\nDamn.. is that so? I guess I was so surprised to see any girls at a Linux event that I probably got carried away a little. ;-)\n<p>\nMakes you wonder about the pictures that _didn't_ make the page, eh? ;-)"
    author: "Rob Kaper"
  - subject: "Re: KDE Report:  LinuxTag 2001"
    date: 2001-07-12
    body: "> Makes you wonder about the pictures that _didn't_ make the page, eh? ;-)\n\nHeh, at first it didn't, but now it does :)\n\nWhere can I find them ?"
    author: "Jelmer Feenstra"
  - subject: "Random idea re: *Next* LinuxTag:"
    date: 2001-07-12
    body: "Maybe there are good reasons to not do this, so I will keep it short, but here's the idea: Sell KDE Dragon T-shirts (or other loot) starting well in advance that say \"LinuxTag KDE Support Crew\" to help pay for otherwise unfunded / unfundable KDE developers to get there. Considering the number of student developers etc, it would be cool to be able to subsidize them so more could gather at once, increase the critical mass, etc. Even if this raised only a few hundred pounds / dollars / marks, it might buy a few train tickets from European cities at least. \n\np.s. Thanks for all the photos, especially the pretty-girl ones. \n\nTim"
    author: "Tim"
  - subject: "Re: Random idea re: *Next* LinuxTag:"
    date: 2001-07-13
    body: "I've got some more ideas, but I suggest that ideas for events go to the kde-events mailinglist so they will be read by the appropriate parties and will also be archived in the mailinglist archives - not that The Dot doesn't archive comments, but the mailinglist archives is where I'd look first.\n\nSo, expect some mail from me on kde-events soon."
    author: "Rob Kaper"
---
At about the time the <A HREF="/994900141/">London Linux Expo</A> was ending, <A HREF="http://master.kde.org/~tackat/LT2001/photos/picture-044.html">sixty KDE developers</A> converged on Stuttgart, Germany for <A HREF="http://www.linuxtag.org/2001/deutsch/showitem.php3?item=30&lang=en">LinuxTag 2001</A>.   LinuxTag is the largest Linux and Open Source exhibition in Europe, drawing in 15,000 visitors and 110 exhibitors this year.  Besides having a great time seeing each other in person again (or for the first time), the KDE developers greeted thousands of visitors to their booth and presented several talks and workshops.  More details, and lots of shots from the event, are available below.
<!--break-->
<P>&nbsp;</P>
<P>DATELINE JULY 11, 2001</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">KDE Report:  LinuxTag 2001</H3>
<P><STRONG>Sixty KDE Developers Converge on Stuttgart, Germany for LinuxTag 2001</STRONG></P>
<P>
July 8, 2001 (Stuttgart, Germany).  More than
<A HREF="http://master.kde.org/~tackat/LT2001/photos/picture-044.html">sixty
KDE developers</A> from all over the world
gathered at the KDE booth during <A HREF="http://www.linuxtag.org/2001/deutsch/showitem.php3?item=30&lang=en">LinuxTag 2001</A>.
Among them were KDE developers from the United States, Austria,
the Netherlands, Sweden, Denmark and Norway.
Special guest-star from the US was
<A HREF="http://noatun.kde.org/">Noatun</A>-developer
<A HREF="http://www.kde.org/people/charles.html">Charles Samuels</A>,
who will probably release KJetLag (which consists of lots of <TT>sleep()</TT>
calls) soon after he returns to the US this week ;-).
</P>
<P>
On six <A HREF="http://master.kde.org/~tackat/LT2001/photos/picture-022.html">TFT
equipped demopoints</A>, KDE developers displayed the latest stable
version of the award-winning <A HREF="http://www.kde.org/">KDE desktop</A>, the<A HREF="http://www.koffice.org/">KOffice</A> office suite and the
<A HREF="http://www.kdevelop.org/">KDevelop</A> development IDE.
They also offered a preview of
<A HREF="http://www.kde.org/announcements/announce-2.2-beta1.html">KDE 2.2
Beta 1</A>, KDevelop 3.0 pre-alpha (a/k/a
<A HREF="http://apps.kde.com/uk/0/info/id/1068">Gideon</A>) and
<A HREF="http://dot.kde.org/994747675/">Reaktivate</A>. The latter
enables <A HREF="http://www.konqueror.org/">Konqueror</A>, the KDE
web browser and file manager, to embed
<A HREF="http://www.microsoft.com/com/tech/ActiveX.asp">ActiveX</A>
controls, such as the popular
<A HREF="http://macromedia.com/software/shockwaveplayer/">Shockwave</A>
movies, for which to date no native Linux/Unix solution exists.
The well decorated and overcrowded 24 square meter KDE booth and its
crew enjoyed the feedback and interest of several thousand visitors,
among which remarkably many stopped by to talk to the
<A HREF="http://women.kde.org/">KDE women</A> team.
</P>
<P>
The event was highlighted with several KDE-related talks and
workshops.  These
included a presentation on "Universal Components" by KDE founder
<A HREF="http://www.kde.org/people/matthias.html">Matthias Ettrich</A>;
a tutorial on "Developing a GUI Using Qt" by Jesper K. Pedersen of
<A HREF="http://www.klaralvdalens-datakonsult.se/">Klar&auml;lvdalens
Datakonsult AB</A>; a presentation on
"<A HREF="http://www.arts-project.org/">aRts</A> und
<A HREF="http://brahms.sourceforge.net/">Brahms</A> -
<A HREF="http://multimedia.kde.org/">Multimedia in KDE 2.x</A>"
by Jan W&uuml;rthner and
<A HREF="http://www.kde.org/people/stefanw.html">Stefan Westerfeld</A>;
and, last but not least, a presentation entitled
"KDE 2.2 - Your Personal Desktop", by KDevelop developer
<A HREF="http://www.kde.org/people/ralf.html">Ralf Nolden</A>.
</P>
<P>
In addition to the lectures, developers hosted several workshops on KDE
development. Particularly noteworthy was
<A HREF="http://www.kde.org/people/michaelg.html">Michael Goffioul</A>'s
presentation on <A HREF="http://cups.sourceforge.net/">CUPS</A> and the
<A HREF="http://dot.kde.org/983389174/">KDEPrint System</A>, of which a
<A HREF="http://www.koffice.org/kpresenter/">KPresenter</A> slide show is
<A HREF="http://users.swing.be/kdeprint/">available for download</A>.
</P>
<P>
A considerable number of well known representatives of the Linux
community attended the KDE booth, including people from
<A HREF="http://www.prolinux.de/">Prolinux</A>,
<A HREF="http://www.linuxnewmedia.de/">LinuxUser and LinuxMagazin</A>,
and Tuomas Kuosmanen (a/k/a "TigerT").
<A HREF="http://master.kde.org/~tackat/LT2001/photos/picture-002.html">Rob
Malda</A> (a/k/a <A HREF="http://CmdrTaco.net/">CmdrTaco</A> of
<A HREF="http://www.slashdot.org/">Slashdot</A> fame), frequently
popped by the KDE booth and proudly displayed the latest KDE beta
on his laptop.
</P>
<P>
In addition, on Thursday <A HREF="http://www.margareta-wolf.de/">Margareta
Wolf</A>,
State Secretary of the German <A HREF="http://www.bmwi.de">Federal Ministry of
Economics and Technology</A>, expressed her interest when she <A
HREF="http://linuxtag.openit.de/2001/pictures/opening/0400x0300/p7050760-accba3.jpg">
obtained</A> information on current issues concerning Linux and the KDE
project, but also shared here concerns about pending patent legislation
in the EU and its potential impact on the development of Open Source
Software.
</P>
<P>
Several sets of photos from the event and the KDE booth are available:
<UL>
<LI><A HREF="http://master.kde.org/~tackat/LT2001/photos/">Tackat's photos</A></LI>
<LI><A HREF="http://jadzia.nl.capsi.com/~cap/digicam/2001-07-09-linuxtag/">Rob
Kaper's photos</A></LI>
<LI><A HREF="http://hydra.linuxtag.uni-kl.de/~daniel/lt2k++/">LinuxTag team photos</A></LI>
<LI><A HREF="http://linuxtag.openit.de/2001/">Winfried Tr&uuml;mper's photos</A> (<A HREF="http://linuxtag.openit.de/2001/pictures/kde/">KDE photos</A>)</LI>
<LI><A HREF="http://bilder.fliegl.de/20010707-linuxtag/thumb/img_1312.html">Deti Fliegl's photos</A></LI>
</UL>
</P>
<P>
The <A HREF="http://www.kde.org/events.html">KDE-events</A>-team would
like to thank all KDE developers and others who contributed to the event
for once more making LinuxTag 2001 a huge success for KDE.
Specifically we'd like to thank:
</P>
<UL>
<LI><A HREF="http://www.klaralvdalens-datakonsult.se/">Klar&auml;lvdalens
Datakonsult AB</A>, <A HREF="http://www.trolltech.com/">TrollTech AS</A>
and others for providing the admission charge for the LinuxTag social
event;</LI>
<LI><A HREF="http://www.suse.com/">SuSE</A> for providing the five TFT
monitors, hotel rooms and miscellaneous hardware;</LI>
<LI>The <A HREF="http://www.kdeleague.org/">KDE League</A> for paying for
promotion material and KDE t-shirts;</LI>
<LI><A HREF="http://www.redhat.com/">RedHat</A> for supplying three
demo points; and</LI>
<LI>Frontsite for the "dragon food";</LI>
</UL>
<P>
and of course we'd like to thank the people who organized LinuxTag 2001.
</P>
<P>
<H3>About LinuxTag</H3>
</P>
<P>
LinuxTag is the largest Linux and Open Source exhibition in Europe.
This year 15,000 visitors and 110 exhibitors (among them more than 30
free software projects) attended. The event was hosted in Stuttgart,
Germany from July 5 through July 8.
</P>
<P>
<H3>About KDE</H3>
</P>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop
environment employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.
</P>
<P>
Please visit the KDE family of web sites for the
<A HREF="http://www.kde.org/documentation/faq/index.html">KDE FAQ</A>,
<A HREF="http://www.kde.org/screenshots/kde2shots.html">screenshots</A>,
<A HREF="http://www.koffice.org/">KOffice information</A>,
<A HREF="http://developer.kde.org/documentation/kde2arch.html">developer
information</A> and
a developer's
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html">KDE 1 - KDE 2 porting guide</A>.
Much more information about KDE is available from KDE's
<A HREF="http://www.kde.org/whatiskde/">web site</A>.
</P>