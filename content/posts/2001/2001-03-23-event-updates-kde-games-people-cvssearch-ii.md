---
title: "Event Updates, KDE Games People, CVSSearch II"
date:    2001-03-23
authors:
  - "numanee"
slug:    event-updates-kde-games-people-cvssearch-ii
---
For those of you attending <a href="http://dot.kde.org/984533000/">CeBIT 2001</a>, a schedule of the KDE presentations is now <a href="http://www.kde.com/devel/schedule.html">available</a>. On another note, a somewhat unlikely place where you can next find a KDE developer will be at <A href="http://guadec.gnome.dk/">GUADEC 2001</a>.  No other than <a href="http://www.kde.org/people/matthias.html">Matthias Ettrich</a> will be presenting a <a href="http://guadec.gnome.dk/program/#day2">keynote</a>.  Evidently, more is going on behind the scenes such as <a href="https://listman.redhat.com/pipermail/xdg-list/2001-March/000068.html">interoperability talks</a> (SSL) spanning such issues as cross-toolkit configuration (<a href="https://listman.redhat.com/pipermail/xdg-list/2001-March/000063.html">1-SSL</a>, <a href="https://listman.redhat.com/pipermail/xdg-list/2001-March/000067.html">2-SSL</a>) and a common component model. <a href="http://space.twc.de/~stefan/kde/">Stefan Westerfeld</a> will also be <a href="http://guadec.gnome.dk/program/#day2">present</a>. <a href="mailto:martin@heni-online.de">Martin Heni</a> informs us of a new <a href="http://www.kde.org/kdegames/people.html">KDE Games People</a> page. This resource should be useful to people who need help with KDE game issues, or a place for people to advertise skills and willingness to help.  <a href="mailto:amichail@cse.unsw.edu.au">Amir Michail</a> wrote in to inform us of <a href="http://cvssearch.sourceforge.net/">CVSSearch II</a>, a major rewrite of <A href="http://dot.kde.org/980299214/">CVSSearch</a>,  the search tool for code that uses CVS comments.  New features include the  ability to search across multiple CVS repositories and a completely new GUI with more explicit CVS comment <-> source code mapping. Over the next week, the demo will be expanded to cover hundreds of KDE applications.  Finally, in case you missed it, a demo for <a href="http://dot.kde.org/982559990/">Kapital</a>, the personal finance manager from <a href="http://www.theKompany.com/">theKompany.com</a>, is now <a href="http://www.thekompany.com/products/kapital/demovers.php3">available</a>.





<!--break-->
