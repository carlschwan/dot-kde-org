---
title: "Kernel Cousin KDE #14"
date:    2001-06-21
authors:
  - "numanee"
slug:    kernel-cousin-kde-14
comments:
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "The new keybindings seem interesting. However I think changes should be very conservative. If 100 million Windows users know a certain keybinding by heart, you better not change it because it makes the transition to KDE much easier. Ctrl-alt-del is the most well known of them all, and most people know it. So why change the meaning of this in KDE? Also, many people including me, use both KDE and Windows, and if the most common keybindings are the same, it would be a big advantage. But of course, not even in the most common windows applications do they have consistent keybindings for such common operations such as search, search again, replace, save etc.\n\nAnother thing to consider is that some keys are easy to type only on certain keyboards, like us-keyboards. On my keyboard (swedish)  = _ (equals and underscore) requires shift. {}[] @ requires Alt Gr for example. Such keys should not be used as keybindings because they would require an extra modifier. I think the best keys to be used in conjucntion with modifiers are the function keys/esc/insert/delete/home/end/pgup/pgdn/the numerical keyboard/print scrn/scroll lock/break/cursor keys, plus tab, backspace, and the alfanumerical ones.\n\nFinally, what is really the meta key? Is it the ugly one with a flying window on it? Can the \"menu\" key also get a good meaning in KDE?"
    author: "Claes"
  - subject: "Menu-key"
    date: 2001-06-21
    body: "Ok, I reply to myself. I would really like the menu-key to work just as in windows: bring up the \"actions\" of the selected object, the same way as right-click does. We better not dilute this key with something else, because it is very convenient!"
    author: "Claes"
  - subject: "Re: Menu-key"
    date: 2001-06-21
    body: "Under XFree86, you can bind any key to a mouse click. To bind the menu key to a right-click, run the following command:\n\nxmodmap -e \"keysym Menu = Pointer_Button3\"\n\nNow, when pressing Shift + NumLock, you will hear a beep, and you can start using the menu key as you right mouse key! The KDE context-menus can be navigated with the Tab and Shift+Tab keys.\n\nYou can also use the numerical keyboard to move your mouse pointer."
    author: "Adam Dunkels"
  - subject: "Re: Menu-key"
    date: 2001-06-21
    body: "Question :\n> > I would really like the menu-key to work just as in windows: bring up the \"actions\" of the selected object, the same way as right-click does. \n\nBad answer :\n> Under XFree86 : xmodmap -e \"keysym Menu = Pointer_Button3\n\nA end user ignores Xfree86, he only knows the parameters of KDE.\n\nI too wish to use the \"Start menu key\" and the \"right menu key\", and also Alt Gr E for Euro. Inside KDE, by default, of course !"
    author: "Alain"
  - subject: "Re: Menu-key"
    date: 2004-02-16
    body: "Someone tell me which key is the Menu Key Please. I need it for use of Alteration of my monitor sizing ( black lines down both sides ) anyone help? I new to Computers. Thanks"
    author: "Denham"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "in case it wasn't clear enough in the summarry, this new keybinding set is _not_ the default set but merely an option, for several reasons including some which you mention above.\n\nfor those who do not use windows or simply want to try out a new keybinding set that has some logical reasoning behind it, this is IMO an interesting development.\n\nall of it is completely configurable in the control center, under look 'n feel -> key bindings, where you can adjust things to your heart's content. for instance, i prefer switching virtual desktops with Alt+F# (like vt's on the linux console) rather than Ctrl+F#."
    author: "Aaron J. Seigo"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "I would like that the windows key - start and properties (right click function) to be enabled on KDE. I am used to use the keyboard to go through menu, and I am pressing the Start key, which works only for WIndows..."
    author: "Paul Dorneanu"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "To enable the Windows keys on your keyboard, bind the keys to F13-F15.  I use a file ~/.Xmodmap (named this way so that RH 6.2 and I presume later RH releases) will read it when I login.  (If you're not using RH, then you will need to run 'xmodmap ~/.Xmodmap' yourself to enable these keys).  The content of this file is:\n\nkeycode 115=F13\nkeycode 116=F14\nkeycode 117=F15\n\nNow use the control panel to bind these keys to some useful actions!  (Just press one of the windows keys when selecting a key to bind an action to and FX will be shown where X is 13, 14 or 15 depending on the key pressed).  \n\nNB To find out the keycode for a key, run the xev program from a terminal and press a key, you should be able to read the keycode from the text in the terminal.  \n\n - Rob"
    author: "Rob"
  - subject: "BTW: Howto input the euro-sign? [OT]"
    date: 2001-06-21
    body: "Euro currency is comming and I'm not able to insert the symbol with [AltGr + e] on my German Keyboard. It's a TrueType-only system setup, I have no problems to see the sign on webpages, I can display the kword euro testdoc - no problems - but I can not insert it via keyboard.\nHas anybody a tip?\n\n<xev:>\nKeyRelease event, serial 27, synthetic NO, window 0x1e00001,\n    root 0x38, subw 0x0, time 970449633, (303,654), root:(307,674),\n    state 0x2000, keycode 26 (keysym 0x20ac, EuroSign), same_screen YES,\n    XLookupString gives 0 characters:  \"\"\n\ntested with\n\"LANG=de\"\nand\n\"LANG=de_DE@euro\""
    author: "Thorsten Schnebeck"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "Well, I have to work rather seldom with windows. But sometimes it happens that I use Ctrl-Alt-F1 or something similar and strange things happen than. \nKnowing that this is an XFree isuue I whish some kind of configuration for my Win-System to open a conole on that keybinding ;-)\n\nHave fun\nEckhard"
    author: "Eckhard Jokisch"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "Did KC leave out Mac-like themes debate, Mosfet's childlike reaction and the cvs struggle by intention? Why?"
    author: "someone"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "Probably not, as far as I know, KC KDE tries to summarize completed threads and not threads in progress.  It's possible those threads weren't complete by the time the issue was written; also notice that the issue covers up to 13 Jun.\n\n(notice my dept line, btw :)\n\nAs for liquid, I will probably include that in an eyecandy article soon.  And the whole Mosfet/cvs thing has really gotten enough of a whole lot of exposure anyway.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-23
    body: "It probably feels that way to you, but most users are not really aware of what has been happening and why. The discussion has stayed on mailing lists and has not found its way to most KDE news sites.\n\nMany users are a bit surprised when they hear the news. Some get the feeling that there is a big secret here. Calmly and maturely acknowledge the situation and they will alleviate that. They will see you are just dealing as best you can.\n\nBut you do what you wish. I can only imagine at how unpleasant this sutuation must have been for you all. If you feel better simply moving on, simply move on. I'll do my best to reassure users one by one. :-)"
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "Well, probably because of the same reason why you couldn't read too much about a Gnome release coordinator stepping down on Gnotices ;).\n\nAbout the Mosfet-stuff: I really don't think it's worth too much mentioning. It has already been discussed exhaustively and not too objectively elsewhere, I think it's probably better to let the situation calm down. Especially as it really is only a very minor irritation, which only got more attention because of the Gnome coordinator resigning a few days earlier."
    author: "Ineluki"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "I think it will be mentioned not as an extra article on Gnotices but in the GNOME summary. Navindra is right when pointing out that summaries lag.\n\nOnly a very minor irritation? As mentioned on Slashdot it's the biggest irritation within KDE for at least 1 year."
    author: "someone"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "Whatever is mentioned on slashdot is not relevant here."
    author: "reihal"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: ">> Whatever is mentioned on slashdot is not relevant here.\n\n    Yes it is.  Just as if NBC Nightly News did a report on KDE, it would be relevant here.  Like it or not, Slashdot is a seriously heavyweight news source - Wired, Salon, and even MSNBC, along with several other news sources pull from it, and just about everybody in the industry can hear about a story that appears on it.  I would imagine that there is not a *nix oriented company; probably not even a major IT related company that does not have several vocal people who read Slashdot regularly.  Certainly IBM, Sun and Microsoft do - they have all dealt at a professional level with the editors regarding stories in the past year.\n\n    Once large corporations issue official responses to stories on a website, that website carries weight.  And as such, it very much is relevant.  Even if you think Slashdot posts inflammatory fluff.  Especially if you think Slashdot posts inflammatory fluff.  And really - IMHO, their stories tend to be less opinioned and inflammatory than ZDNet or CNet.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "Slashdot is not a news site, it's a flaming and trolling site."
    author: "dc"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-25
    body: ">their stories tend to be less opinioned and inflammatory than ZDNet or CNet.\n\nThat doesn't take much! After all, CNet are the people who repeatedly called linux an \"upstart OS\", and ZDNet used to distribute copies of the kernel and some spartan shell tools (and only this), and claim that you could install a working linux OS on your system with only that."
    author: "Carbon"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "Oh, I forgot the dot is the only page in the universe with proper arguments. All others only contain propaganda against KDE."
    author: "someone"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "Ah, I had a feeling that was why you brought this up. Glad that KDE had a little flamewar, aren't you?\n\nOh well, it's only about a few *styles*, not about any major decisions regarding KDE's direction."
    author: "Matt"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "> Ah, I had a feeling that was why you brought this up. Glad that KDE had a little flamewar, aren't you?\n\nWhy? I mentioned it was calm for years. Why do you argue that this is not true because I wrote \nit was pointed out on Slashdot? Idiotic logic.\n\nOnly one who got flamed is Slashdot in my eyes.\n\n> Oh well, it's only about a few *styles*, not about any major decisions regarding KDE's direction.\n\nKC covers more than only \"major direction decisions\"."
    author: "someone"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "Slashdot needs flamed.\n\nCraig"
    author: "craig"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "English you needs learned.\n\n(I assume that English is your native language.  If this isn't the case, please ignore this flame.)"
    author: "ac"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "But the summary did include a link to the mailing list, where you can read the entire story."
    author: "dc"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-23
    body: "\"As mentioned on Slashdot it's the biggest irritation within KDE for at least 1 year\"\n\nPerhaps what I wrote has been misinterpreted. What I meant to indicate was how peaceful and rational almost all the discussion on the KDE mailing lists was. So, this Mosfet thing, minor as it is, is the worst it's got for ages :)\n\nI didn't mean to make it sound like Mosfet's petulance was important, but to emphasise how flame-free KDE usually is."
    author: "Jon"
  - subject: "Mosfet story"
    date: 2001-06-22
    body: "Mosfet's departure is a big blow to KDE as he is a very talented programmer and artist.  There are also a number of issues that have been raised by the whole affair that need to be discussed, like whether or not a developer should have the right to remove code that he has already contributed to a project.\n\nAlso, if the whole thing has been covered badly elsewhere, shouldn't it be covered accurately and fairly in at least one forum (here)?  Otherwise people will end-up with a distorted understanding of the whole thing.\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: Mosfet story"
    date: 2001-06-22
    body: "Mosfet is back on now track."
    author: "reihal"
  - subject: "Re: Mosfet story"
    date: 2001-06-22
    body: "Mosfet is back on track now.\n\nSee http://www.mosfet.org/\n\n\"Well, it looks like I'll be being funded to continue my work independently. This means that things like Pixie and Liquid and all the other stuff will continue to be developed by me outside of KDE CVS (all I wanted to do in the first place). Please note that much of my code such as Liquid, Megagradient, and many of the style engines in the CVS were re-added, but those are no longer my versions, they are forks. My versions will be released under a new name shortly. I think you will be happy with them ;-) \nOf course, the licensing will remain the same (this won't be proprietary software). \n\nmosfet@interaccess.com \""
    author: "reihal"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "I can clarify this.\n\nInitially I planned on covering the thread regarding the Liquid announcement. However, the situation ignited pretty much the same time as I wanted to cover it.\n\nI would either have to leave those events out and write a summary already outdated on the moment it would be published. Or I could include them but only include some of the earliest posts, leaving more questions than answers, a lot of room for flamebait and incomplete or partial arguments.\n\nTherefore I decided not to cover it at all for last week, I believe the issue has been covered enough in various places already and my personal opinion isn't much more than \"ok, Mosfet and KDE don't get along anymore, that sucks, now let's move on and get back to coding\".\n\nI still plan to do a summary on the events for next weeks issue, but I will focus on the new maintainers of the reimported styles."
    author: "Rob Kaper"
  - subject: "Liquid location?"
    date: 2001-06-21
    body: "I've looked over the kde-devel list and that liquid theme sounds really cool... \n\nUnfortunately, all of the pointers to the tarballs & screenshots have disappeared. \n\nDoes anyone know where one can go to see what all the fuss was about?  (URL)\n\nThanks,\nPhil"
    author: "Phil Ezolt"
  - subject: "Re: Liquid location?"
    date: 2001-06-21
    body: "Liquid should still be available from apps.kde.com or else there is a low bandwidth mirror on http://capsi.com/~cap/mosfet-liquid0.1.tar.gz (please, only if you must)\n\nThe very short story is: Mosfet wanted it in kdelibs right now, others thought that the timing was not a good idea (2.2 Beta1 freeze) and the place was not a good idea either (there's kdeartwork now). This wasn't the first time such a situation was present, so Mosfet felt critized and others felt that he should give in a bit because for the benefit of the masses (the KDE users)."
    author: "Rob Kaper"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "I just grabbed the latest from CVS and I have to say it's great, but there's one thing that bugs me.  In Konqueror, whenever a link is clicked on, a box appears around it, presumably to let me know that I indeed am using my mouse properly.  I can't stand the box and I can't find any way to turn it off; is there a way or am I stuck with it?"
    author: "KDE User"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "KControl -> Peripherals -> Mouse -> Disable \"Visual feedback on activation\"\n\n.. and you are happy again :-)\n\nTackat"
    author: "Tackat"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "Ahh, but this only applies to icons (at least on my copy .. )  In the browser it still has visual feedback on links (I would have reported this as a bug but it does indeed say \"Icons\" over the frame for visual feedback).  Konqueror is so damn cool, though, that I think I can let this pass (as long as we don't also get the 'click' that internet explorer uses!)  =)\n\nThanks guys, KDE rules."
    author: "KDE User"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "Hmm, what's wrong with a little more user feedback in konq ? You're telling me you know perfectly well when you've clicked a link or not ? I surely don't.\n\nJelmer"
    author: "Jelmer Feenstra"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "I'm sorry, but that's a pointless question.  I don't like the feedback.  Period.  You know (or should know) that this is a completely subjective issue and thus your views aren't any more \"right\" than mine.  If I dislike the feedback, I dislike it.  And, might I add, I do.. :)\n\nI do know when I've clicked on a link, yes, I don't need a box or noise telling me so.  The annoyance of the rare times I miss a link is a lot less than the annoyance of a big box popping up around it every time I don't."
    author: "KDE User"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "Well it's not really a subjective issue in the case of how a lot of users experience browsing with konqueror. Perhaps you always hit the link correctly, but there are a lot of users that don't and would appreciate some sort of feedback that (for example) the host is just being contacted at the time. I think one can easily say that this sort of feedback will give most (novice) users a better feeling about both how they know they clicked a link and whether or not something is being done about it at that very moment (perceived speed). I do agree these things should be made optional though."
    author: "Jelmer Feenstra"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "Exactly my point, you see... :)  Most novice users will appreciate it.  Not all not-so-novice users will.  Thus it's subjective.  I understand the KDE guys' feelings on configuration options, though, it seems that everyone is requesting something to be configurable.  It's no big deal, I just hack the source when things I don't like appear (such as the logout/lock screen buttons on the tray)."
    author: "KDE User"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "What I meant was that I don't think it's completely subjective wether or not it's a good idea to have those feedback features in khtml, as I think there are more novice users that could benefit from it (compared to experienced users who can change it themselves anyway).\n\nWhatever. With regard to the configuration options, I had a discussion about this on #kde the other day. We concluded it might not be a bad idea to have an advanced 'tree' (Ms IE->tools->internet options->advanced) which could be used to toggle, select, name etc things. Novice users won't need to get into this tree as the most used options are present in the config dialogs as they are now.\n\nPerhaps an idea ?"
    author: "Jelmer Feenstra"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-26
    body: "I thought it was my web coding that was wrong when I saw this.\n\nHopefuly we can turn it off in the future."
    author: "dids"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "KDE severly lacks some collaboration tools,and\nautodiscovery services.\nhave the KDE developers considered e.g. slp\n(service location protocol) , see www.openslp.org."
    author: "Jonathan"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "at a superficial glance, this seems to be the same purpose the lisa stuff in kdenetwork serves (the lan: ioslave). \n\nOpenSLP looks good too though... a bit more through, perhaps (from a very brief glance at their page)"
    author: "Kevin Puetz"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "I think that a big bonus to SLP is that it is an RFC standard and is used more widly than LISA (which seems completely KDE specific).  SLP is used for autodiscovery in Novell networks, MS Win2K networks and by Caldera Volution networks plus others that I can't remember off the top of my head.  Playing nice with others is always a good thing, NIH is not."
    author: "Raven667"
  - subject: "Which mailing lists?"
    date: 2001-06-21
    body: "Which mailing lists are you covering in Kernel Cousin KDE?"
    author: "Loranga"
  - subject: "Re: Which mailing lists?"
    date: 2001-06-21
    body: "Aaron covers kde-core-devel, kde-devel, koffice-devel, kfm-devel and some others.. kdevelop-devel and kde-artists I believe..\n\nAnd I cover kde-promo, konq-e, kde-games-devel although I might summarize threads from kde-devel and kde-core-devel if I see something I'd like to cover.\n\nAre we missing important lists? Want to join? :-)\nJust drop us a note or subscribe to kc-kde (http://master.kde.org/mailman/listinfo/kc-kde) and introduce yourself!"
    author: "Rob Kaper"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "The internet keywords of konqueror are great but the ':' you must type after them is a bit long to type. \nIt would be great if you changed it to work like Opera, where you can just type 'g whatever' and it searches for whatever!\n\nDaniel"
    author: "':' takes too long to type"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-21
    body: "Do you have a really weird keyboard or something? ':' takes just about NO effort to type on my standard US keyboard - just hit shift with left hand and push down with my right pinky. : : : : : :)"
    author: "David G. Watson"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "But not everyone is using a US keyboard.\n\nAnd to type ':', you have to use shift. To type 'g', you don't have to use shift. That makes a difference."
    author: "Philippe Fremy"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "And pressing shift is so hard because.... ? Do most people lack the ability to move their hands independent of each other? ;)"
    author: "David G. Watson"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "I will repeat my comentary until it will change it. KDE needs a re-structuration of the KDE-menu. There are lot of mini applications that the end user doesnt know how to use it, or doesnt know what it is (lot of garbage .. sorry ). For example: \n\n1\u00ba.- administration tools have to be into an application named administration tools into KControlCenter , or integrated into Konqueror, like the mini-aplication kfloppy (as file maneger explorer).\n\n2\u00ba.- The first menu of KDE button has to be with less parts, like start windows button, that has only five as programs, documents, hardw conf and help. And into programs, subsections with sections like office,internet and something like that (MAC OS has the same structure). It helps the new user and is more elegant :-)."
    author: "Daniel"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "You can change your start menu the way you need, and it's no really important it's structure (I think).\n\nOther things should be fixed and / or improved.  but I thing you are right about Kfloppy\n\nnote :\n\nWhy KDE2.2alpha2 is 'alpha'?  It's really stable and I can work with it without any problems.  Only ARTS ans Konqueror crashes sometimes (but they're stable).\n\ncheers...."
    author: "Rossend Bruch Maseras"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: ">Why KDE2.2alpha2 is 'alpha'? It's really stable and I can work with it without any problems. Only ARTS ans Konqueror crashes sometimes (but they're stable).\n\nMaybe because they only want *one* beta, hence the alphas are very stable. I didn't try the 2.1.0 betas, but I believe that they had some real showstoppers and they didn't want to repeat that again. As I undertand it this release is mainly focused on stability and speed with some new features. So it not surprising that the alpha 2 is stable. The current alpha does however contain some showstopppers, but I believe that they should be fixed in current cvs (I'm compiling cvs right now to check it out, but damn how long it takes for some things to comile :-) )."
    author: "J\u00f6rgen Lundberg"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "First: \nMoving the admin-tool into an other program is no good idea because i dont like to\nstart this \"other program\" before i can use the admin-tools.\n\nSecond:\nSorry but I HATE the thing with the 'programs' submenu in the KDE-menu, because the KDE-menu is a START-menu and i only start programs so a submenu 'programs' makes no sence to me!\nWhy copy the bad things from other systems???\n\nThe KPersonalizer could be a way to choose the prefered way of the KDE-menu."
    author: "gnu128"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "I am talking about the standar kde menu, not about KPersonalizer. It is not very clear like MAC or Windows menus. And i am not talking about copying them but re-organizate it."
    author: "Daniel"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "I agree that a re-org is in order.  There are a lot of toplevel entries in the menu hiearchy.  A lot of little apps clutter K:{Utilities,System} that could be into new subfolders like Preferences or done away with in he default config and only enabled if the user asks."
    author: "Raven667"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-23
    body: "I like the basic layout of the App Launcher.  Myself when I used windows 99.999% of thr time went for Start -> Programs.  Might as well just have the program categoeries right off the Start and save me the energy. \"Configuration\" is just a bunch of programs anyway so it still si intuitive.  I like in Mandrake that there are nice organized categories to the App Launcher like Launcher -> Network -> Browsers -> Mozilla.  Hell in Win98 they aren't even in alphabetical order.  What KDE need to kill is the redundancy in the menus.  Sure you can run each of the KControl modules seperatly, but they aren't necessary on the applauncher when one entry for KControlCenter works just as well."
    author: "Justin Noel"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-26
    body: "I disagree completely. The first layer of the windows menu is a space-waste. I like the kde/windowmaker/twm/just about every #$%#$ thing apart from MS style currently in use. Perhaps a more logical solution than an out right change would be an installation option? (It is, after all, totally customisable with a little effort already...)"
    author: "C. D. Thompson-Walsh"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "From Mosfet's site\n-----\nWell, it looks like I'll be being funded to continue my work independently. This means that things like Pixie and Liquid and all the other stuff will continue to be developed by me outside of KDE CVS (all I wanted to do in the first place). Please note that much of my code such as Liquid, Megagradient, and many of the style engines in the CVS were re-added, but those are no longer my versions, they are forks. My versions will be released under a new name shortly. I think you will be happy with them ;-) \nOf course, the licensing will remain the same (this won't be proprietary software). \n\nmosfet@interaccess.com \n-----\nlooks like we got best of both worlds now\n- KDE team happy since no more just before release new themes\n- We happy since more frequent updates to\nhis lovely themes"
    author: "Mosfet Fan"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "Woohoo!"
    author: "Navindra Umanee"
  - subject: "Re: Kernel Cousin KDE #14"
    date: 2001-06-22
    body: "He,He,\n...and in HTML code this site is called <TITLE>Mosfet's Development Site</TITLE> _again_!\nMosfet, this is the best solution for all. Hope to \"see\" you again in the mailing lists :-)"
    author: "Thorsten Schnebeck"
---
Brought to you by Aaron and Rob, <a href="http://kt.zork.net/kde/kde20010615_14.html">this week's edition of KC KDE</a> covers everything from the elusive <a href="http://www.kdeleague.org/">KDE League</a> to Kamera, music applications (<a href="http://brahms.sourceforge.net/">Brahms</a>, <a href="http://rnvs.informatik.tu-chemnitz.de/~jan/noteedit/noteedit.html">noteedit</a>, <a href="http://rosegarden.sourceforge.net/">Rosegarden</a>), and details on a new keybindings scheme that exploits the Windows keys.
<!--break-->
