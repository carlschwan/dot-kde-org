---
title: "Multimedia: The Noatun Development Handbook"
date:    2001-02-02
authors:
  - "csamuels"
slug:    multimedia-noatun-development-handbook
comments:
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-03
    body: "Njaard you rewl!! :)\n\nNoatun rocks!"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-04
    body: "is it possible to use it without arts?"
    author: "Evandro"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-04
    body: "I wish!\nSome work needs to be done on arts. It's a pain for me. It doesn't like my via sound chip. but the OSS drivers in 2.4 also has problems. the problem sounds like its treating a 48khz output as a 44khz one."
    author: "mark"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-04
    body: "No, and it never will support arts.\n\nHowever, some manufacturers don't think that 44100 is a good rate. morons.\n\nEither way, send patches, help out, most of all! we're trying!"
    author: "Charles Samuels"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-04
    body: "> > is it possible to use it without arts?\n>\n> No, and it never will support arts.\n\nUh?  That's a double negative.  How can it not support arts, and yet not work without it?\n\nJ"
    author: "Macka"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-04
    body: "hmm.  I need to think more before I press \"Send\"\n\nI meant to say:\n\"No, it will never not support arts\"\n\nNow, that's a double negative, but at least it's accurate :)"
    author: "Charles Samuels"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-06
    body: "LOL  :-)\n\nJ"
    author: "Macka"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-05
    body: "Excellent!\nArts sucks. All that modular stuff behind it are\ngreat ideas, but in the real world I guess it\nbecomes the only really stinky thing in KDE2.\nIn my PII-266 usually crash few seconds after\nlaunch, as eats all cpu and keeps waiting for more :-(\nI don't believe a PIII 700 or an Athlon should be the entry level for sound in KDE! :-("
    author: "Julio Cesar Gazquez"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-05
    body: "I mean to say that I'll always require arts.\n\nIn fact, playing an MP3 here, without any effects or hoodad, it requires just a little more than mpg123, without any soundserver.  I think that's pretty good.\n\nAbout it crashing after a few seconds, your sound card's driver is probably sucky, we just need to get the little stuff worekd out.\n\naRts works well on system with good soundcard drivers, and good sound cards.  Alsa is a good idea.\n\nAbout skipping, the RT in arts means \"Real Time\", and it still just barely buffers.  Give us a little time."
    author: "Charles Samuels"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-05
    body: "Hmm, that must be some problem with your soundcard/driver. I can use arts for hours playing mp3s with xmms and the xmms-arts plugin. No problems at all. It eats up quite some ressources, though but it doesn't crash. Then again I prefer being able to hear my ICQ messages while listening to music. \n\nI have KDE 2.1 CVS from some hours ago and run Kernel 2.4.1 on an AMD K6/2 500. I used to have problems with arts crashing in KDE 2.0 but they're definately gone in the KDE Version I use now.\n\nCheers,\n\nRichard"
    author: "Richard Stevens"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-05
    body: "it even works okay, when compiling three or four KDE packages at the same time on the mentioned system ;)"
    author: "Richard Stevens"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-05
    body: "Just noticed, noatun doesn't work in that version, though. Bugreport sent in ;)"
    author: "Richard Stevens"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-05
    body: "I use KDE 2.0.1 on my FreeBSD 4.1.1 box.  I'm running a K6-2 400 system with 128 megs of ram and a Yamaha OPL-SAx sound card using the pcm drivers.\n\nOverall, KDE2 is quite nice.  Although, the arts thing rarely wants to start.  It usually crashes/exits without any explanation as to why.  Sometimes I get a surprise by the Startup.wav when I first startx into KDE2, but it pretty much goes dead until I exit KDE2.\n\nI just hope that the new KDE 2.1 version that is in development has better support for FreeBSD in general as well as a better working arts daemon.\n\nKeep up the good work and don't forget that some of us BSD junkies do like KDE."
    author: "Joey Garcia"
  - subject: "Re: Multimedia: The Noatun Development Handbook"
    date: 2001-02-05
    body: "Sadly, mp3 playing still doesn't work in mpg123.\n\nI think it's because of pthread support, or something funky going on.  We're working.\n\nHowever, I do believe it can play Wav files."
    author: "Charles Samuels"
---
As you might know, <a href="http://noatun.kde.org/">Noatun</a> (with a Kaiman module) is the new KDE 2.1 multimedia player. Noatun supports plugins on a very deep level, and a rich API allows large amount of control of the entire
application. A new <A href="http://noatun.kde.org/doc/">document</a> is now available with developer information on writing playlists, user interfaces, visualizations, and even coffee-makers for Noatun. ;-) Also available, an <a href="http://noatun.kde.org/doc/api/">API reference</a>.

<!--break-->
