---
title: "New IOSlave To Access Freenet In Konqueror"
date:    2001-08-28
authors:
  - "mmoeller-herrmann"
slug:    new-ioslave-access-freenet-konqueror
comments:
  - subject: "Fake!"
    date: 2001-08-28
    body: "Notice that in the screenshot of the GPL text, that the location bar says \"freenet://KSK@gpl.txt\", but the titlebar of Konqueror remains \"file:/home/jd/gpl.txt\". So, they've quite possibly viewed their local copy of the GPL, entered a URL beginning with \"freenet:/\" in to the location bar, and taken a screenshot.\n  Not that the IO slave doesn't exist, just the GPL screenshot isn't very impressive."
    author: "Phalynx"
  - subject: "Re: Fake!"
    date: 2001-08-28
    body: "Maybe he forgot to press Enter. :-)\n\nThis sounds like the next Napster though!"
    author: "ac"
  - subject: "Re: Fake!"
    date: 2001-08-28
    body: "No, it's nothing like Napster!\nIt's more like an alternative internet inside of the internet...\nUsing fproxy (standard part of Freenet), you can surf Freenet just like you surf the WWW except that it is Freenet you surf, not the WWW! :)\nIn the future, you will be able to use FTP, News, mail, etc etc through Freenet! Internet is only used as a \"transportlayer\" for Freenet. Actually you could do that today, but as it isn't very widespread, and still in development, it's kind of useless...."
    author: "Per Wigren"
  - subject: "Re: Fake!"
    date: 2001-08-28
    body: "FastTrack is the next Napster."
    author: "reihal"
  - subject: "Re: Fake!"
    date: 2001-08-28
    body: "And where can people find FastTrack??"
    author: "Stephan"
  - subject: "Re: Fake!"
    date: 2001-08-28
    body: "On the Net."
    author: "reihal"
  - subject: "Re: Fake!"
    date: 2001-08-28
    body: "Hmm, I don't see any FastTrack clients for any OS other than Windows at this point.  I'd say that a Linux client is likely to be pretty important to readers of the KDE forums... \n\nMP3 searches via these clients are restricted to garbage quality bitrates, 128kbps or less.  If they ever do add the ability to search for high quality mp3s, it will cost money.\n\nThe clients install spyware and adware.\n\nIn conclusion, I'm not sure how FastTrack could suck more."
    author: "phubhar"
  - subject: "Re: Fake!"
    date: 2001-08-29
    body: "The Morpeus client has an option for \"at least\" 128 kbps. The ads doesn't bother me and the spyware can't get past my firewall."
    author: "reihal"
  - subject: "Re: Fake!"
    date: 2001-08-29
    body: "I forgot: there is constantly 500 to 700 thousand users and 300 to 400 TB online."
    author: "reihal"
  - subject: "Re: Fake!"
    date: 2004-03-20
    body: "spyware can't get past my firewall?\nheh.\nspyware runs locally and you're downloading it. firewalls have nothing to do with it."
    author: "jamie"
  - subject: "Re: Fake!"
    date: 2001-08-29
    body: "Are'nt you people looking at http://apps.kde.com??? I've been useing Qtella .2 its very cool. Getting all kinds of cool stuff. Any kind of music out there is on the network. Bluegrass and Religious for me but i'm sure you'll find what your looking for as well.\n\nCraig"
    author: "Craig"
  - subject: "Re: Fake!"
    date: 2001-08-29
    body: "True the title for the GPL is file://, but the source code shot looks correct.  Both examples are real.\n\nhttp://kdewebcvs.nebsllc.com/cgi-bin/cvsweb.cgi/kdenonbeta/kio_freenet/\n\nDownload and compile yourself; send patches to ilnero@gmx.net.\n\nPeace."
    author: "Jay Oliveri"
  - subject: "Re: Fake!"
    date: 2001-09-01
    body: "The 2nd screenshot seems real..."
    author: "Carbon"
  - subject: "Kick Ass"
    date: 2001-08-28
    body: "What else is there to say. konqueror is getting better and interesting all the time thanks to these\nIOslaves, while ago scp:// and now freenet:// whats next pron:// ?-)\n\nraine"
    author: "raine"
  - subject: "Re: Kick Ass"
    date: 2001-08-28
    body: "realword://my_preferred_pub/glass_of_beer would be a good thing to have ;-)\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Kick Ass"
    date: 2001-08-28
    body: "how about\n\ninstaller://update?rpm"
    author: "caatje"
  - subject: "Re: Kick Ass"
    date: 2001-08-29
    body: "Hmmmm..  How 'bout opening a console and typing:\n\napt-get update && apt-get upgrade.  That does it for me.\n\nSeriously, the more I use Debian the more I wonder why people bother with anything else..."
    author: "Ben"
  - subject: "Where is KOFFICE 1.1 :-("
    date: 2001-08-28
    body: "I miss Koffice, isn't it officially released yet?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Where is KOFFICE 1.1 :-("
    date: 2001-08-28
    body: "ftp://ftp.kde.org/pub/kde/stable/koffice-1.1"
    author: "someone"
  - subject: "ioslaves + console"
    date: 2001-08-28
    body: "Would it be possible to write a utility to enable the use of KDE IOSlaves from the command line? I missed this just the other day, when I wanted to do something like\n\nrpm -U ftp://ftp.redhat.com/update/blablabla\n\n(I know there are utils that let me mount ftp sites in the file system, but that's not the point. ;))\n\nI haven't read up on how the IOSlaves work, could this be done with a generic utility or must every slave be modified?"
    author: "Johnny Andersson"
  - subject: "Re: ioslaves + console"
    date: 2001-08-28
    body: "You can do something like:\n\nkfmclient copy 'ftp://ftp.redhat.com/update/blablabla' 'file:.'"
    author: "AC"
  - subject: "Re: ioslaves + console"
    date: 2001-08-28
    body: "Replying to myself... hee hee\n\nEven better, try this:\n\nkfmexec pico http://dot.kde.org"
    author: "AC"
  - subject: "Re: ioslaves + console"
    date: 2001-08-28
    body: "Cool! I didn't know about those two programs. Looks like something to explore."
    author: "Johnny Andersson"
  - subject: "Re: ioslaves + console"
    date: 2001-08-28
    body: "So why didn't you do that? Rpm certainly allows it."
    author: "Marko Samastur"
  - subject: "Re: ioslaves + console"
    date: 2001-08-29
    body: "How about vim ftp://....\n\nHmm, perhaps sometime in the future, kio could be integrated as a kernel module, or as a daemon (i.e. vim /kiodaemon/ftp://...) to allow this sort of thing to all apps, and then the GUI part (download meters, etc) could be seperated."
    author: "Carbon"
  - subject: "Whoa, cool!"
    date: 2001-08-29
    body: "maybe I'll try freenet again with the help of this.\n\nNow just one question - how the hell do I compile it??? (I've already dl'ed it from CVS.)"
    author: "Somebody"
  - subject: "Re: Whoa, cool!"
    date: 2001-09-01
    body: "cvs update and try.  I've included the proper configure script.\n\nEnjoy."
    author: "Jay Oliveri"
  - subject: "sftp"
    date: 2001-08-31
    body: "How about an ioslave for secure ftp. It would be great to be able to type sftp://www.domain.com and get a secure ftp transaction in konqueror."
    author: "Scott K"
  - subject: "Re: sftp"
    date: 2004-01-20
    body: "Try fish://192.168.0.1\n\nSecure shell file browser.... What will they think of next.....\n'course I'm running the devel build 3.1.94... but I think it's in 3.1.0"
    author: "beerman"
---
A new KDE IOSlave enabling easy access to Freenet has been announced by Jay Oliveri in this <a href="http://lists.kde.org/?l=kde-devel&m=99853868805506&w=2">message</a> to kde-devel.
<a href="http://freenet.sourceforge.net/index.php?page=whatis">Freenet</a>  is a large-scale peer-to-peer network which pools the power of member computers around the world to create a massive virtual information store open to anyone to freely publish or view information of all kinds. Screenshots of the IOSlave in action can be found <a href="http://www.crisisincorporated.com/oliveri/gpl.png">here</a> and <a href="http://www.crisisincorporated.com/oliveri/kio_freenet.png">here</a>.


<!--break-->
<br><br>
KDE makes it easy to integrate other projects with its extensible IOSlave architecture. The architecture allows seamless net, file and device access for all KDE applications. 

To see the complete list of protocols supported check in kcontrol under Network->Protocols. At the moment there are already more than 35, including http, nntp, audiocd (cd grabbing), imap (mail folders), smb (windows networking), floppy (disk access without mounting), kamera (access to photos in camera).  <a href="http://freenet.sourceforge.net/index.php?page=whatis">Freenet</a> access is an interesting addition, with hopefully more to come.
<br><br>
If you are interested in writing your own ioslave, you might want to have a look at <a href="http://www.heise.de/ct/english/01/05/242/">the excellent tutorial</a> from c't that we <a href="http://dot.kde.org/995751367/">announced</a> a while ago.

