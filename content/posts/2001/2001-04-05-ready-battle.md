---
title: "Ready To Battle?"
date:    2001-04-05
authors:
  - "nzimmermann"
slug:    ready-battle
comments:
  - subject: "Re: Ready To Battle?"
    date: 2001-04-04
    body: "Seems like www.kde.org is down, in fact it seems Trolltech's main router is unavailable... Bad timing :)"
    author: "Haakon Nilsen"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-04
    body: "I can reach it just fine..."
    author: "KDE User"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-04
    body: "hmm, i don't consider this as \"big news\" (aethera 0.9.1, first kde email client supporting imap - i can't get it to work btw - , went silently), why is it on the dot ?"
    author: "ik"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-04
    body: "You can't get imap to work either? Glad I'm not the only one.\n\nErik"
    author: "Erik"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-04
    body: "We found a bug in the imap that didn't show in our internal testing early today, we are putting up new packages in about an hour.\n\nWe submitted the story to dot, but sometimes there is a delay.  I'm not really sure how it works to decide to run a story or when it runs, I just know they are all volunteers so they are probably very busy."
    author: "Shawn Gordon"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-05
    body: "Wow, that's what I call service! Post a message to a forum and get the CEO of the company responding within a few hours! And they say Linux doesn't have good support. :)  Shawn, I'll say it again, you are the man.\n\nErik"
    author: "Erik"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-05
    body: "If I don't set the example of caring for my company, then who will? :)\n\nWe put up new betas of Aethera yesterday that should fix the IMAP problem.  If anyone wants to try it out, make sure your old version of Aethera is totally wiped from your system.  Let us know."
    author: "Shawn Gordon"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-06
    body: "Very well put\n\nImp"
    author: "Imp"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-04
    body: "Well, first of all, there is no reason to put down KBattleship. It's no Quake 3, but I'm sure it's big enough news to those who appreciate how addictive even the simplest Internet games can be. ;)\n\nAs for the Aethera and Kapital issues, we did announce the first releases, and we will  probably announce the final releases.  Dre was going to mention the new betas in our new weekly app roundup. There is a *lot* of good stuff coming out of theKompany.com these days, and we're not sure if dot.kde.org should make a main article out of every item, for various reasons.  \n\nHowever, I'm glad to see that on top of the mention on apps.kde.com (and hence the app boxes on www.kde.org and dot.kde.org), LinuxToday also carried the announcements which got plenty of hits and comments.\n\nBtw, I heard that KMail CVS has IMAP too.  I still use Mutt on a Unix shell, so I wouldn't know.  ;)\n\nCheers,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-05
    body: "yep, i am sorry, and you are probably right about not posting aethera, altough i'd like to see some discussion about it (i think its a good way for the aethera developers to get feedback too), but since it will be mentioned at the end of the week, it won't be a problem.\ni surely think kbattleship is a lovely game, but i think the dot should not become a second apps.kde.com.\n\nkeep up the good work !"
    author: "ik"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-05
    body: "Hmm...\n\nA developer announced the existence and move of KBattleship from kdenonbeta to kdegames for the first time, and already you fear we're becoming a second apps.kde.com?  At the same time, you lament the lack of news on the new Aethera beta release... an app which we've already announced here in the past. ;)\n\nI've mentioned this before, but I don't mind at all focusing on the occasional \"small\" app from time to time.  It can't be that bad a thing...\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-05
    body: "well\n1 i was wrong as i admitted before (exams you know, i should not be here atm :p)\n2 aethera is seen by a lot of people as the kde equivalent of evolution, and evolution is getting a lot of attention (its even in the gnome 1.4 press release). (okay i am not saying now kde should act that way too and i am totally not saying we should fight a war, but it seemed evident to my small mind)"
    author: "ik"
  - subject: "Offtopic, but anyway"
    date: 2001-04-04
    body: "Will KDE in the future still have synchronous release cycles, with every package (kdegames, kdenetwork) named with the same version number, like 2.2?\n\nAnd/Or will KDE still keep the big collections (kdegames, kdenetwork, kdeutils) or will you split up these packages into independent applications?"
    author: "Stentapp"
  - subject: "Re: Offtopic, but anyway"
    date: 2001-04-05
    body: "It's easier for KDE to do it this way, so it's up to the distributors to split the packages. \nDebian already does this, and so does Caldera and Suse,  I believe.  No idea about the others. I think apps.kde.com might also split the packages for you, if you ask nicely.  ;)\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-05
    body: "Hmmm, nice game. I've played it in school 20 years ago and it was much more interesting then learning. Perhaps it becomes a renesance as tcp/ip-based game."
    author: "Stephan Boeni"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-05
    body: "It compiled OK on my system but then it wouldn't launch. I got an error that said that i didn't have battleship pictures installed. There in the directory though. Oh yes i think its a real pity that aethera beta 2 and kapital beta 2 haven't been listed. The Kompanys doing more than there far share for kde. Seems they should get better treatment. I now Xamians evolution isn't getting the cold shoulder over there."
    author: "Craig Black"
  - subject: "Well.."
    date: 2001-04-05
    body: "Aethera and Kapital are not programs included in KDE.  Dot.kde.org is, for the most part, about development within the KDE team.  However, the Dot did give postings for Aethera and Kapital when they first came to be.  This is good enough, don't you think?\n\n-Justin"
    author: "Justin"
  - subject: "Re: Well.."
    date: 2001-04-05
    body: "We are not giving anyone the cold shoulder.  :(\n\nIt's really frustrating for me to read such accusations as I'm torn on many sides.  I would like to think that the dot covers more than just \"development within the KDE team\", but that doesn't mean we should post long beta announcements for each app.\n\nLike I said, these betas *will* be mentioned in the weekly app round up.  We are just trying to follow a sane policy here.\n"
    author: "Navindra Umanee"
  - subject: "Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "Even worse than you realize, Eazel and Nautilus is getting the cold shoulder from many GNOME users now.  Ximian not yet, but does anyone realize that these two companies have all but destroyed GNOME?  There does not seem to be any independent GNOME activity anymore, it's Ximian vs Eazel, and Eazel is going down already, while Ximian has branded GNOME as Ximian GNOME and taken over the development and Eazel has shoved an obnoxious Nautilus down GNOME users throats.\n\nThis has nothing to do with theKompany.com which is a decent third party company trying to make a living.  They at least are not trying to take over KDE with obnoxious \"services\"."
    author: "ac"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "I'll give you an amen on that one. I guess its just frustrating. The Kompany is bring me and i know lots of users apps that we want to see. Their so small their advertising budget has to be minuscule. So for most people the dot is where they hear about their applications. I know many of us want to see the succeed desperately. This will mean the success of kde and us having the quality applications that we need. I'm glad that the qt license allows people like the kompany to sell there proprietary software. The alternative is to become stuffed monkey peddlers like Xamian. I don't think Nautilus has a prayer. You're right about them both destroying gnome. I used to be a gnome user. They've really buried it.\n\nCraig Black\nICQ# 103920729\nAOL IM blackfam972"
    author: "Craig Black"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "Umm, Ximian GNOME is not GNOME, and there is no subsciption stuff it the 1.4 GNOME release of natilus.  Eazel and Ximian have big contracts with companies like HP, SUN, DELL and more.  Also you might be a bit confused about that GNOME/Gtk liscencing, Eazel wanted to make their apps GPL.  Just because MS sells mice dosen't mean that there scrounging for money.  Of course with a mastery of the english language like yours(especialy the spelling) your FUD is not very effective(and the fact that you probably use AOL).  I use KDE but people should realize that GNOME is a very good DE, just because it may be the competition in your mind dosen't mean that you have to FUD it, some of the KDE people are worst than MS."
    author: "robert"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "Friend Microsoft sells many things. Xamian sells stuff animals and t-shirts. I don't like Microsoft but I'm not fawned of carneys peddling their stuffed animals and t shirts. Have i made a mistake? Oh i think you can pick up a coffee cup or something as well. Its not FUD its the truth.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "Excuse me, but your comments do make you seem like a troll (not a good one at Trolltech, by the way ;-).  KDE is a great desktop environment; so is GNOME.  Let the flames lie.\n\nThe GNOME community is much larger than both Ximian and Eazel, just as the KDE community is much larger than theKompany.  Nonetheless, these companies do seem to be making valuable contributions to the respective environments, no?\n\n~Andrew\n\nP.S.  What's wrong with selling plush monkeys? :-)"
    author: "andrew"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "There is no comparison between The Kompany and Eazel and Xamian. The Kompany has not or has any intention to take over the development of KDE. Plus the The Kompany doesn't sell any stupid stuffed monkeys and that an important distinction.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "Say, what is wrong with stuffed monekeys anyways? Sure, no KDE developer in their right mind would buy a stuffed moneky, but I for one would buy a stuffed Konqi."
    author: "Carbon"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "Nothing wrong with it as a side deal but that's all Xamian is selling. Not exactly a good business plan. If i want to buy a stuffed monkey I'll go to the Carnival.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "Umm, I allready wrote what Ximain is selling, but since you may have trouble understanding I'll make it simpler. \n\n\n   When you have a large project like GNOME or KDE just jumping in and developing the core can't realy be done.  Because of the size of the project it takes a while to be productive, this is where Eazel and Ximain come in.  Large UNIX vendors like SUN and HP are switching from CDE to GNOME, this means that there has to be a port of GNOME to Solaris OE and HP-UX, and not just a try to recompile fix 10 lines of code and then compile.  Unlike linux, nothing is included into Solaris OE or HP-UX until it's had extensive(many months) of testing, also GNOME dosen't do everything, SUN and HP may need many things added or fixed.  Another problem with something like GNOME is that, saying something like GNOME x.x is misleading, GNOME is just a bunch of libraries and components, these libraries depend on each other, this means that if you change the ABI to one alot of the others break.  If SUN was to fork GNOME then they would have to hire a bunh of programmers that would be unproductive for months, also they would eventualy change a bunch of the ABI's, that means that they can't just use the cool new GNOME stuff that non SUN employees write, and porting the stuff then takes a while because the SUN programmers have to learn how the stuff works, eventualy port it with lot's of work and then test it, by the time there done alot of time has gone by.  So the solution to this is for a company like Ximain to hire a bunch of the GNOME core hackers and just have them do the GNOME core work and the porting, this solves all the headaches for companies like SUN and HP.  Also SUN may have some in house programmers that work on GNOME, when these people get stuck they just call up there Ximain support and get help from the people that wrote the code.  This is Eazel's and Ximain's main source of income, everything else is just extra.  If this is too confusing for you just tell me, I will make it even simpler(You use AOL :) )"
    author: "robert"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "It's hard to argue with someone that has bought so thoroughly into Xamians investor propaganda. Porting apps is no sustainable business plan or is selling stuffed monkeys for that matter. If you believe that crap i don't know what i could say to change your mind. Come back after taking dose of common sense. Now i know someone that lost a lot of money last April when the Internet stocks tanked. What's with the AOL thing? Seems that's a favorite accusation of all the  slash dorks out there.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "Free (aka open source) software by its very nature prevents people from selling software for an appreciable profit.  So, the profit lies in services.  Selling services is what both Ximian and Eazel do.  They also write (IMHO very good) software for free (as in beer) to help with the services, to sow good will among the open source community, and to leverage their expertise with potential customers who will pay for services.\n\nThis, I think, is as ESR envisioned it.  Is this economic model truly an evil, nefarious one?  I, for one, would rather have an free software company selling services and stuffed monkeys on the side than a Microsoft-type company selling proprietary software that emprisons you as a customer."
    author: "andrew"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "Ok friend why don't you go over too their web sites and report back here with a summery of their services that there selling and that you can sign up for and how much they cost. (Hint: They don't have any.) \n\nCraig"
    author: "Craig Black"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-07
    body: "Actualy I work for SUN doing work on GNOME, I know exactly what we pay for.  The AOL thing comes from the fact that you have an AIM number."
    author: "robert"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-07
    body: "I use kaim. There are lots of aol im clones.\n\nCraig"
    author: "Craig Black"
  - subject: "Robert is a troll.  Delete this thread!"
    date: 2001-04-07
    body: "Like hell you work for Sun, more likely you work for Microsoft reading every bit about GNOME and KDE you can and then regurgitating incoherent trolls.\n\nYou have no idea what you are talking about.  Your ignorance about AIM is the icing on the cake."
    author: "ac"
  - subject: "Re: Robert is a troll.  Delete this thread!"
    date: 2001-04-07
    body: "The thing about AIM is a joke."
    author: "robert"
  - subject: "Re: Robert is a troll.  Delete this thread!"
    date: 2001-04-08
    body: "Nice try your a fraud!!\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Robert is a troll.  Delete this thread!"
    date: 2001-04-09
    body: "This guy's not a troll, Craig Black is saying something false and this guy is telling him that, if you read robert's post you'll see that he says nothing bad about KDE."
    author: "AC"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "Ximian sells GNOME support and has contracts with vendors like SUN, HP to get GNOME on their platforms, the reason that SUN or HP dosen't just fork the code is because of all sort of versioning problems and incompatible libs, it makes sense to have Ximain do the porting.  But any buisnes that has a chance to make more money will jump at it, even if it is selling stuffed monkeys.  But I garauntee you that a very small amount of there income is coming from stuffed animals.  I believe that there also selling software subscription services for linux like APT-repository but the software is not all a year old, and I think there selling CD's but I doubt they'll get any money off of them."
    author: "robert"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "Their not selling any services. Just stuffed monkeys and T-shirts. Your right about it being very little money but so is the Sun Redhat deals. Xamian and Eazel are just burning though venture capital. Sooner or later it will dry up. What then? Maybe then they will  acualy try and charge for red carpet. The end result will be them both going tits up .com. Then the gnome hackers will have to relearn to hack there desktop again without having the private companies to do it for them.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-07
    body: "I work for SUN, were spending alot of money on GNOME, there's tons of profit to be made."
    author: "robert"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-07
    body: "If there was so much money to be made for Xamian then they would'nt need all that venture capital would they?\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "Yeah, you're right.  We should all like the Kompany better because their budget is small and because they're jealous of all the attention Gnome is getting.  Oh, whoops!  Did I say that?\n\nNo one is \"destroying\" Gnome.  Point out just ONE thing that Ximian or Eazel have done to \"wreck\" Gnome.  It's likely impossible to \"wreck\" GPL'd software.  Don't like Ximian's packaging of Gnome?  Don't use it.  Don't like Nautilus?  Stick with GMC.  These companies have done nothing but improve Gnome greatly, adding features and functionality.  And thanks to the license you apparently scorn, if I don't like what Eazel or Ximian does, I can fork the code and do whatever I like with it.\n\nIf you like KDE, promote KDE.  If you like Gnome, promote Gnome, but what's the point of talking about how sh*tty the one you're not using is.  I love Gnome, but I know KDE has some VERY cool features.  I simply don't choose to use it - I prefer Gnome.  That's the beauty of Linux - since the software is Free, all becomes a matter of choice.  Using KDE and promoting it should be an inherant statement that you like it better than Gnome."
    author: "Jens Knutson"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "I don't think this was a troll post. He was saying how he felt, and for the most part, I agree with him. Look at Gnotices, many users don't like Eazel and Ximian, because it SEEMS that independent GNOME development has pretty much stalled since these companies appeared. Actually, I think GNOME's development pace HAS decreased. Thats why many people don't like these companies.\n\nOf course, you can tell anyone to continue using GMC if he doesn't like Nautilus, but GMC isn't developed any further DUE TO Nautilus' existence, isn't that so?\n\nI think thekompany has acted very wisely by not taking over development of KDE programs, like konqueror or koffice, but rather adding applications that haven't been there at all before (well, mostly). That way, they don't influence the KDE development itself that much. They just give KDE an end-user base, which it needs so much.\n\nThis isn't supposed to be a troll posting either, but I DO think eazel and Ximian are a problem for GNOME. It's not like they should disappear, as some people have said on Gnotices, but they need to find a solution to this quickly."
    author: "me"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "I read Gnotices on a regular basis, and while some people are squawking about not liking Nautilus, there are MANY who are giving very positive feedback about the project, too.  When people do complain, it's usually about the same few things:\n\n1) Speed - while a wholly valid point, this just keeps improving on every release.  What's more, I'm starting to think that perhaps people just aren't using the various speed options Nautilus provides (ie: turn off anti-aliasing, etc).  As I understand it, this also remains one of Eazel's top priorities for Nautilus - speed improvements.\n\n2) Memory usage - Nautilus uses about 12-16mb on my system.  Tales of it taking up something like 90mb are from people not knowing how to interpret memory information in top on multi-threaded programs - 6 threads x 15mb does not equal 90mb of memory used.  Yes, 12-16mb can seem like a lot, but with SDRAM becoming pennies a MB, 15mb doesn't really seem like a big deal to me.  Nautilus is feature-rich, and I expect to pay for those features in a larger footprint.  Personally, I'm impressed it doesn't use more memory for all it does.\n\n3) Lack of cut'n'paste file management - this feature wasn't included because the Eazel developers saw the cut'n'paste paradigm as \"flawed\" and wanted to implement something better.  If one reads any of the recent interviews or mailing list archives, a cut'n'paste like system will be in the very next revision of Nautilus.\n\n4) Services - some people hate the idea that Eazel is trying to make money without selling software.  How horrible.  I think that those who really hate the inclusion of Eazel Services in Nautilus are in the vocal minority, not to mention being ungrateful sods. ;)  Besides, the Gnome 1.4 version of Nautilus *doesn't* include Eazel Services.\n\nSo really, it's not so much Eazel that people don't like, they're generally just unhappy that some of their favorite features are missing from Nautilus.  This is a perfectly valid thing, but what's great about any Open Source project is that criticism is a GOOD thing - the more people talk about what they want, the more the developers know what they should consider for implementing and in what order.\n\nI also still cease to see how stopping GMC development is somehow a bad thing.  Should Microsoft be developing Win 3.1 because some people don't like Win 9x?  (If 3.1 was Open Source, those who liked it could do it themeselves, but I digress...) The reason GMC development stopped is because apparently none of the people developing Gnome care to use GMC any longer - if they wanted to continue development on it, they'd be free to - no one \"controls\" GPL'd software - all are free to do as they will, provided that they give back to the community that they have benefitted from.  If Eazel becomes unresponsive, or if Nautilus doesn't improve, a group may very well start up to work on GMC again - and all the more power to 'em!  Viva choice!\n\nThe point is, Eazel and Ximian CAN'T \"destroy\" Gnome.  If someone doesn't like what these two companies do, they can take Gnome in their own direction."
    author: "Jens Knutson"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "If someone doesn't like what these two companies do, they can take Gnome in their own direction.\n\nThats a fantasy. They would have to start a whole new project. Xamian and Eazel now control gnome. Someone could create gnome apps but they would'nt go into the default gnome. Gnome is no longer a Hacker destop. Its a corporate Desktop.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "And I'm sure that the nifty new KDE app I am working on will be in the base KDE 2.2 package (NOT!)"
    author: "Chris Slaughter"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "What u talkin bout curtis? New packages are added to every minor version relase (that is, x.x, whereas x.x.x is ultra-minor release, and x is major release). In kde 2.1, they added noatun. In kde 2.2, they are adding kbattleship. These are both just examples!"
    author: "Carbon"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "I think your exactly right, GNOME is realy going down the crapper.  KDE has the same problen with TrollTech and theKompany, but that could be solved.  I realy believe that the KDE developers should fork Qt and make CORBA bindings to get around the liscencing issue(that also fixes language bidings, and helps with stuff like Doc/View), also software written shouldn't go into the KDE you download.  Qt is not as bad as what Ximain and Eazeal has done, but look at how many KDE developers work for TrollTech and linux companies.  The thing is that Qt suffers becuse of TrollTech needing a Windows version, if Qt were made UNIX only alot of stuff could be fixed, look at just the most basic parts of Qt they use MOC instead of someting like SigC++ because of MSVC++, also they don't use stl because of MSVC++.  All this not to mention that theKompany is trying to control stuff like the KDE scriting and trying to control the development tools and API's and such, why should my painting program be more bloated just so that theKompany can sell stencils?  I think that the KDE developers should look at the mess called GNOME and realize that there heading down the same path, it's not too late to stop in KDE though.  I think if companies make something part of KDE they should do there own fork and keep KDE a community project."
    author: "John"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "John I've got to take exception to some of your points here.  Take control of scripting?  We've built a scripting environment with VeePee that is freely avaiable to everyone and runs on KDE, GNOME and Windows.  We are working with both KDE and GNOME to see if it can be put into the environment.  If you've got a scripting environment done, then by all means you should get it included.\n\nWhat does our selling stencils have to do with a bloated painting program?  Kivio has nothing to do with anything else other than the fact it is in KOffice, but it's not a painting program.  Again, if you have a flowcharting program available, then by all means release it and put it in KOffice.\n\nShawn"
    author: "Shawn Gordon"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "That was satire if you couldn't tell, I prefer KDE but GNOME is a realy amazing peice of software as well, and the companies that support it put out good stuff, these people have no idea what there talking about, I'm just trying to make them realize how stupid they sound."
    author: "john"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "Satire? Damn subtle, I must say. With the level of anti-linux-companies crap flying around, I doubt I'm the only one who got fooled :)"
    author: "nap"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "Haha, I thought you were serious, too:) The funny thing is, your's wasn't even the most irrational post! (which is why I didn't know you were joking)"
    author: "kdeFan"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "The Kompany is a part of the KDE community, but Miguel's' Xamian and Eazel control the gnome community. Theres a big difference.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "I was speaking more about TrollTech, if the KDE developers were to fork Qt and do a CORBA binding then they could clean up all the hacky stuff to get around MSVC's crappy compiler, fix the language bindings problem and end the liscencing problems with Qt.  Do you realize how many KDE developers are working for TrollTech, because of that the direction of KDE is realy controled alot by TrollTech(Lotta developers on one thing and you have the network effect)."
    author: "John"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "What licensing problem are you talking about. Qt is GPLed. For the few people who still have problems with that I can only say that even RMS agreed that Qt has no licensing problem anymore.\nAnd what the core KDE developers that work for TT are concerned, there are a some, but it is not even the majority of the KDE developers. In contrast to that even Alan Cox recently posted in a (Gnome developer) newsgroup that there has been hardly any contribution to Gnome outside of Ximian/Eazel and that he is concerned that they basically took over the project.\nGenerally I have no problem with that, but the irony is that Gnome was started in order to prevent the \"evil\" commercial influence of KDE on free software and now it seems that KDE is more in the spirit of free software than Gnome is."
    author: "jj"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-06
    body: "I did not mean legal problems with liscencing, where I work we don't GPL our software, and I garauntee you that we aren't going to pay 2000$ a developer for a toolkit when there a toolkits just as good for free, forking Qt means that TT's Qt would probably be incompatible."
    author: "John"
  - subject: "Re: Ximian/Eazel mess vs theKompany"
    date: 2001-04-05
    body: "Talk about trolling?\n\n> Yeah, you're right. We should all like the Kompany better because their budget is small\n\nActually I'm going to guess you have not been exposed to much business, expecially the school of hard knocks. I'll make this very simple but quoting a proverb. \"The borrower is the lender's slave\". If one wishes to make an honest evaluation it seems you would have to be utterly naive or totally dishonest not to recognize that theKompany's less glamorous budget gives them an advantage of not having to feed an 800 pound gorilla. To quote more wise words to business people \"If your income does not exceed your outgo then your upkeep will be your downfall\".\n\nNow, I have yet to hear anyone who's savvy I respect give me an explanation of what is rational in the Eazel and Ximian business plan. However given the old rule that 1 in 6 ventures returns a yield I guess some people are looking at tax write offs already.\n\n> and because they're jealous of all the attention Gnome is getting. Oh, whoops! Did I say that?\n\nYes, but that is alright. While I respect the GNOME core developers I have found their advocates to not be able to make a better argument than this for their opinions. I'm sorry... Shawn Gordon does not strike me as being jealous, let alone having time for such pettiness... and unlike Eazel and Ximian I can pencil out his business plan.\n\n> No one is \"destroying\" Gnome. Point out just ONE thing that Ximian or Eazel have done to \"wreck\" Gnome.\n\nI'm going to agree and disagree. Those are too strong of words, but there is little doubt that some of the lustre has been taken off. Eazel is a good example since they are openly risking their continued development by entering business without a real business plan. Spare me the sound bite GPL argument. In the first place that is LGPL which means that new features can be charged for to pacify investors. But the reality is that if they should cease to exist there appears to have been 35 programmers on the payroll for over a year to get to a nowhere near finished product. There is no way this does not add up to major slow downs in development should it come to pass. Source code is one thing... 70,000+ hours of development time to replace is another.\n\n> And thanks to the license you apparently scorn, if I don't like what Eazel or Ximian does, I can fork the code and do whatever I like with it.\n\nWho's scorning the licence? Anyway, I am 110% percent behind you forking the code or at least fishing through it trying to figure out what they hell went on. I am guessing there must be a fair pile of it so by the time you have sorted through and are ready to fork we should have had months of peace.\n\n> but what's the point of talking about how sh*tty the one you're not using is. I love Gnome, but I know KDE has some VERY cool features. I simply don't choose to use it - I prefer Gnome.\n\nWell again I agree and disagree. I know there is plenty of activity outside these companies (even though I think they both smell since I see no viable business plans). I also agree that there is no point in knocking them... however I'd like to point out that this site for KDE. People need a place to let their hair down a little and say what they are tired of getting flamed for... even if it really is not the best. The point is... if you don't use KDE... why are you reading the KDE news? Why are you trolling? I have to tell you... I don't use much GNOME, but I do not dislike it so that I look for GNOME news sites to make snide remarks on. It seems pretty petty that you are trying to open a GNOME formum on a KDE site."
    author: "Eric Laffoon"
  - subject: "trolling"
    date: 2001-04-06
    body: "The GNOME advocate was being a wee bit defensive and angry.  This is true.  It's a fine line between that stance and trolling, and perhaps he crossed it.  However, both the GNOME news site (which, BTW, has a lot of KDE trolls posting on it) and this site are open.  If people here want to knock GNOME, they should be open to GNOME people responding.  This same is true at the GNOME site.  Honestly, though, all this trolling does no one any good whatsoever.  It doesn't help KDE; it doesn't help GNOME (well, maybe it helps Microsoft ;-).  Constructive criticism is good.  Trolling is just a waste of chars.  Peace."
    author: "andrew"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-05
    body: "Yes... katzbrown.com:5801 ;)\n\nkbattleship rOx0rz !\n\nGood job WildFox!"
    author: "Jason Katz-Brown"
  - subject: "Re: Ready To Battle?"
    date: 2001-04-05
    body: "I hope I didn't disrupt any of your games... I've been trying to connect but I have to go offline now.  ;)"
    author: "ac"
  - subject: "hehe"
    date: 2001-04-05
    body: "i was at school, i just got home now, now i start server :)"
    author: "Jason Katz-Brown"
  - subject: "Idea for game makers"
    date: 2001-04-05
    body: "Nice game, but consider doing something different. \n<br><br>\nHow about a game server that can be EASILY programmed (perhaps Graphically). Imagine a server using <a href='http://www.cs.mu.oz.au/mercury/'>Mercury</a> (Compiled Functional/Logic language) as a game server. Also, imagine a nice client board with drop in pieces that can be coded easily, perhaps in C++/Python. \n<br><br>\nWhy use Mercury for games? Board games are nothing but rules/logic. Mercury was built for this and can be much faster than Python and flexible compared to C++/Java (As it relates to logic/inferences).  \n<br><br>\nWhile I like the games, it would be nice to see a gameing system that individuals can build/design. If I might point out, Hasbro/Parker Bros might get interested in this, in that it will allow games to be test marketed. Capsi, I might point out that your kmonop could be extended to do just this.\n"
    author: "ac"
  - subject: "Trademark?"
    date: 2001-04-05
    body: "Isn't \"battleship\" the trademark of some company? I know games.yahoo.com calls their version \"Naval Command\" for this reason... Perhaps the name should be changed?"
    author: "Anon"
  - subject: "Re: Trademark?"
    date: 2001-04-05
    body: "That's why we say \n\n\"KBattleship, a \"battle ship(R)\" clone\"\n\nin the documentation.\n\nI think this is sufficient. Additionally, this is open source and there are tons of other battleship clones, so what?\n\nAnyway. I am not a layer and if the company that own \"battle ship\" feels like sueing us, I won't keep them from doing so.\n\n</daniel>"
    author: "Daniel"
  - subject: "Re: Trademark?"
    date: 2001-04-05
    body: "Heh, im sure you are not a layer, Daniel. But you arent a lawyer either. :P"
    author: "Lenny"
  - subject: "Won't compile on Mandrake 8.0 Beta 3"
    date: 2001-04-05
    body: "Hi all,\n\nI can't get Kbattleship to compile on Linux Mandrake 8.0 Beta 3.  It keeps looking for libXext (I've found it in /usr/X11R6/lib) but ./compile never sees it.  I let compile know where my x includes and libs are.\n\nAny help would be appreciated.\n\nTim"
    author: "Tim_F"
  - subject: "Re: Won't compile on Mandrake 8.0 Beta 3"
    date: 2001-04-05
    body: "Hi  Tim,\nif ./compile (didnt you mean \"./configure\"?) doesnt find your libXext in /usr/X11R6/lib just put a symlink of libXext to /usr/lib - it sometimes helps.\nGood luck!"
    author: "Henri"
---
<a href="http://www.kde.org/kdegames/kbattleship/">KBattleship</a> is a TCP/IP-based boardgame similar to the common game played on a sheet of paper, where two players try to battle the other's fleet. KBattleship has just been moved into the <a href="http://games.kde.org/">KDE Games</a> package and will be publicly available with  the <a href="http://developer.kde.org/development-versions/kde-2.2-release-plan.html">KDE 2.2 release</a>. For the moment, you can only get it via <A HREF="http://apps.kde.com/uk/0/info/vid/2922">APPS.KDE.com</A>, anonymous CVS, CVSup, or a recent <a href="http://ftp.kde.org/snapshots/">kdegames snapshot</a>. Check it <a href="http://www.kde.org/kdegames/kbattleship/">out</a>!


<!--break-->
