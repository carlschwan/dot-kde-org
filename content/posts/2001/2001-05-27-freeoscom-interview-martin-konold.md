---
title: "FreeOS.com: Interview with Martin Konold"
date:    2001-05-27
authors:
  - "numanee"
slug:    freeoscom-interview-martin-konold
comments:
  - subject: "gecko vs. khtml"
    date: 2001-05-26
    body: "I am always curious as to why do we have two open source projects trying to do the same thing? What are the pros and cons of khtml over gecko?\n\nWouldn't it be really nice if konqueror and for that matter entire kde used gecko instead of khtml so that we could say \"konqueror : gecko 1.0 complient\" and gecko itself being CSS1/2 HTML 4.1 etc. complient.\n\nKonqueror is simply great.. I use it as my primary browser.. but all that is mainly because of the features provided by konqueror and not khtml I think.\n\nBy spending less time on khtml, kde developers could spend that time on other remaining issues!"
    author: "sarang"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-26
    body: "The good thing about two projects is that there is competetion. Every project focuses on different issues and when you find something in the other project that you like you can implement something similar. Basically, you get the best of both worlds by competition.\nIf you only have one project, you would be missing the choice that makes open source so valuable. As far as Konq vs Mozilla (or kHTML vs Gecko for that matter), as stated in the article when they first started people questioned as well if this would be a good idea. As it turns out Konqueror has been improving much faster than Mozilla. So there is certainly a good reason to encourage competetion. And last but not least most of the developers are doing it in their spare time for fun. No one has any right to tell them what to do and what to leave (also I think it can be very rewarding when you get positive feedback from users)."
    author: "jj"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-26
    body: "Well said jj\n\nCraig"
    author: "Craig"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-26
    body: "I understand all this. I am totally for such competition. I love the fact that we have GNOME vs. KDE going..\n\nbut, my point was not Mozilla vs. Konqueror... rather, gecko vs. khtml. If we consider gecko as a library which provides CSS, HTML etc. then why not use it if its perfect rather than build a new one? For eg. about about C++ library? KDE people arn't writing a new C++ libarary!! or for that matter arts.. \n\nFrom what I understand after reading about mozilla/gecko is that gecko is ready and perfect long time back.. the reason why mozilla is slow and _huge_ is because of their toolkit. So why not use gecko as the renderer for html? In future when new standards pop-up, kde people will have to spend time on implementing those in khtml again!"
    author: "sarang"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-26
    body: "Because you simply have control over your own libraries. If there is something you do not like in this library can you change it. If you would be using someone else's library (or rendering engine for that matter) you can ask them to implement the feature, but you can not control it. \nAnd if you plan to implement your own features on their rendering engine, there is no guarantee that the this will be picked up by the mozilla developers and you might be stuck with a fork that you have to maintain yourself.\nFinally, sometimes it is better to start something from scratch (that was the whole idea behind the rewrite of Netscape/Mozilla) and learn from the mistakes that were made in the other project."
    author: "jj"
  - subject: "Re: gecko vs. khtml"
    date: 2004-07-26
    body: "jj wrote: \"you simply have control over your own libraries.\" Sure you do, it's like building yet another linux distribution, it's open source, you have to remember that you can read the source, copy and edit.  Just remember include that GPL.  But then again some companies *cough* MS *cough* still copy and edit even though it's not open source.  \n\n\"sometimes it is better to start something from scratch\"  Yeah, have you evered programmed a browser from scratch?  I'm not trying to insult you, but if anyones been up to the chore, they'll know that it's not exactly a walk in the park.  Of course, it's not impossible, it's just probably not worth the time and frustration.  It's much simpler just to take the code, edit it to what you want and maintain the new project.  e.g. fluxbox and blackbox.\n\nAlthough, I'm still just a comp sci freshman, and don't really know the very detailed technical info behind it.  My theory behind it the reason for the different browsers is that:\n-a few GNOME libraries are used in Mozilla that KDE does seems to not want, possibly because of integration.\n-Licensing, which is rumoured to be the reason why Apple chose KHTML over mozilla"
    author: "CS student"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-27
    body: "> my point was not Mozilla vs. Konqueror... rather, gecko vs. khtml\n\nLast I checked Gecko was using Mozilla's rendering engine (maybe they've forked it?) I don't follow it that closely but I know that Konqueror uses khtml. So that *is* what you're talking about.\n\n> If we consider gecko as a library which provides CSS, HTML etc. then why not use it if its perfect rather than build a new one?\n\nI believe this was answered but I think someone is blowing smoke up your backside if you believe it's perfect. Expecially if it's using the Mozilla rendering engine that last I saw still had serious rendering errors that I was not willing to live with. See the story posted here where konqueror was in fact running the Mozilla rendering engine. What you want is out there... go get it... but note the egregious rendering problems even in recent months.\n\nIn fact I'll be blunt. Given the fact that after years of work from thousands of contributors the rendering engine in question jumbled text links, over ran the right side of the page and left form buttons hard to read I was stunned. If six months after going alpha they can't get the most fundemental basics right I have to wonder. It is politically incorrect to say bad things about the darling of OSS but last year I defended it until it finally went alpha. A year later it is not ready and the little khtml that has had a hand full of programmers working on it for less time is progressing much faster. I'm a professional web developer and I am really upset and dissapointed with Mozilla (and Gecko is Mozilla on a diet) but I enjoy Konqueror.\n\nBeing blunt... one would have to be daft to get off a winning thoroughbred onto an wheezing old nag just as they were overtaking them. The Mozilla team has done everything but focus on getting a functional browser out. In fact I think it's a good study on how a project can go wrong while khtml is a good study on why you should not critisize programmer decisions... because when they're right you will run their software.\n\n> From what I understand after reading about mozilla/gecko is that gecko is ready and perfect long time back..\n\nDon't believe everything you read. Gecko may \"de-fluff\" the rendering engine but it's being built to strict Open standards. Look up W3C. So is Konq (khtml) but it is also including some extentions for compatibility with IE. Whatever your opinion Konq maintains greater compatibility and also benefits from using a superior toolkit which happens to be the same as the rest of KDE.\n\nOkay, bottom line. Don't even think about critisizing people for what they do in their spare time... especially when it is clear that some of the points brought out here by others still have not hit home with you. What you want has been done. Look it up and use it if that is what you want. Finally if you want something changed this is open source. *You do it!* I would suggest that if you at least learned a little bit more you may not program on it but you might at least understand and respect the opinions of the people who are working on the code. \n\nIt is not right to suggest that programmers take all their experience, ambition and vision and dump it for an idea formed around an opinion that has not had the benefit of the huge amounts of information they have. Don't tell others... tell you... then do!\n\nSorry if the tone of this is not sugary enough. I'm really getting tired of people who see open source as their opinion of what others should do (thier job is apparently to provide direction I guess) when in fact they simply don't know enough to provide direction... you can't join an army of volunteers as a five star general. )Thankfully)"
    author: "Eric Laffoon"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-27
    body: "I think you have confused Gecko with Galeon.\nGecko is mozilla's html rendering engine."
    author: "Solhell"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-27
    body: "> Don't even think about critisizing people for >what they do\n\nRelax!! I am not critisizing anything or anybody. I just wanted an answer to something I was wondering about.. no I got it.\n\nsarang"
    author: "sarang"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-30
    body: "Actually, that *IS NOT* what he was talking about...He was trying to avoid talking about the products themselves...Mozilla is not Gecko, it's just the framework that Gecko is housed within, Konqueror is not KHTML, etc. etc. The previous reply answered his question I think, at least partially.\n\nI think he was looking more for technical merits of the libraries and how they differ.  \n\nGeesh, everyone is so eager to point out how stupid the other person is."
    author: "Brendan"
  - subject: "Re: gecko vs. khtml"
    date: 2001-06-01
    body: "Hi Eric, \n\nyou wrote :\n> the Mozilla rendering engine that last I saw \n> still had serious rendering errors that I was \n> not willing to live with.\n\ni'm sorry but even if i like Konqui i'm affraid it has far more of those rendering errors... \n\nand it lacks a good ECMAscript support (ECMAscript is W3-Consortium-standardized Javascript, in other words, the future of JS...)\n\nsee http://mozillazine-fr.org/tests/test_DOM_compliance_through_javascript.html\nif you don't believe me... \n\non this test page Konqui 2.1 does return the correct objects for the 2 first tests, then an incorrect object, then it returns \"Error message\" and the JS engine stops functionning for the rest of the 29 tests... you see the point.\n\npersonnaly, i would love to see KDE with a clean port of Mozilla to QT, rather than going on developping Konqueror's web abilities. keep Konqui as a clean and fast file manager and then link to a QTZilla for the web, that's what i would prefer... \ni'll add that i'm disappointed by Mozilla themes. if you could apply a KDE theme to QTZilla as to all the other apps, wouldn't it be great ?\n\nsincerely,\nHerv\u00e9 - http://mozillazine-fr.org/"
    author: "Herv\u00e9"
  - subject: "Re: gecko vs. khtml"
    date: 2003-01-24
    body: ">  personnaly, i would love to see KDE with a clean port of Mozilla to QT, \n>  rather than going on developping Konqueror's web abilities\n\nI came across this web page recently.\nhttp://www.trolltech.com/qtmozilla/"
    author: "psgivens"
  - subject: "Re: gecko vs. khtml"
    date: 2007-12-28
    body: "Actually, in 3.5, Konqueror/KHTML passes the test with 0 undefined."
    author: "Michael"
  - subject: "Re: gecko vs. khtml"
    date: 2001-06-28
    body: ">Okay, bottom line. Don't even think about critisizing people for what they do in their spare time.\n\nThere is some truth to this, but I don't think the KDE project in general and Konqueror specifically qualifies as something personal in the same way as, say ... someone's choice of music or hobbies. If KDE and Konqueror were created to actually be a mainstream desktop environment, than developers should accept ideas/critisism from anybody who does or potentially could use KDE/Konqueror (whether they are a programmer or not). Don't get me wrong, though, I (and probably every other KDE user) certainly appreciate all the hard work that people have put into KDE and Konqueror, and hopefully one day I have the time and the skills to contribute myself. But in the mean time I think that the developers and users alike should listen to each others concerns and do our best to help each other out instead of blowing each other off as shortsighted or stupid.\n\nBy the way, I am using Konqeror right now, and it works great!"
    author: "Andrew Stegmaier"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-26
    body: "Who are you to tell the developers what to do? If someone thinks they can do better in good C++, let them. I have read somewhere that gecko has to use a small subset of C++ to be compiled with M$ Visual C++. The KHTML people don't let M$ limit their technical desicions.\n\nAnd KHTML uses Qt. Gecko does not."
    author: "Erik"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-27
    body: "Actually, Corel ported Mozilla to Qt.  We were supposed to post this news, seems one of our editors never followed up with a promised interview.  ;-)\n\nHmmm, time for some quickies."
    author: "Navindra Umanee"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-27
    body: "I read something about that. Where do you download it at?\nDoes it look any different?\n\nCraig"
    author: "Craig"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-27
    body: "It should be committed to Mozilla CVS, you'll need to specify certain configure options to disable the GTK frontend and use the Qt frontend instead.  I might try to dig up the details later. \n\nI haven't used Mozilla in ages, so I don't have any first hand knowledge of it.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-27
    body: "I was wondering what ever happened to that..."
    author: "dingodonkey"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-27
    body: "do you really believe that GCC is superior to VC? I worked with both the compilers, examined the generated code, examined the C++ support ... what else. GCC is more restraining than VC++ (as VC by itself is still not completely following the standards). Both being restraining, I don't think the compiler is something that limits that much your technical desicions. Plus I don't see any advantage in a HTML renderer to use Qt. Can you show some of the advantages to the audience? (I personally think Qt is a good library)."
    author: "peter"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-27
    body: "Qt uses MOC instead of templates to compile with MSVC++, so yes they do let MS limit there technical decisions, also dont reply that they did it a better way unless you have seen the same thing done with templates."
    author: "ac"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-30
    body: "I suppose you're referring to Karl Nelson's libsig++. AFAIK it also compiles under MSVC, so what you say is false.\n\nThat said, I used to think moc was an ugly hack better replaced with templates but after 1 year of using Qt, both at work and at home, I've changed my mind. moc's \"dialect\" is actually much easier to use than libsig++. Adding keywords to the language may be unholy but it does make much much more readable headers. And the fact that they code slot and signal names as strings makes dynamic UI building totally trivial."
    author: "Guillaume Laurent"
  - subject: "Re: gecko vs. khtml"
    date: 2001-05-30
    body: "Oh, hush up.  He wasn't trying to tell anyone to do or not to do anything, he was just curious about why it was happening the way it was.  \n\nDid I forget to drink from the Pool of Aggression today?"
    author: "Brendan"
  - subject: "Re: gecko vs. khtml"
    date: 2003-01-24
    body: "I've read this whole thread.  I see alot of people explaining why there are competing products in opens source, but I don't see this question answered.\n\n> What are the pros and cons of khtml over gecko\n\nIf I am a new developer and I have to use one over the other, which might I choose and why?  My priority is ease of use.  It seems that plugins are written for netscape and therefore mozilla.  I like that, but how easy is it to embed gecko into a qt app?\n\nAs I posted previously, I found this:\nhttp://www.trolltech.com/qtmozilla/"
    author: "psgivens"
  - subject: "ADMIN: flame wars deleted"
    date: 2001-05-27
    body: "<i>[please do not reply to this message, you may <a href=\"mailto:navindra@kde.org\">mail me</a> if you have a comment]</i>\n<br><Br>\nThis forum is not for personal attacks and insults, but for intelligent discussion.  Please stay on topic. This particular flame war (now deleted) has been going on for several days, spanning several sites, including several dot.kde.org forums.  That's enough, thanks.  We've decided to delete today's war.  \n<br><Br>\nMy sincere apologies to everyone who've invested their time and energy in this. If you would like to recover any text that you previously posted here, you may obtain it from me.\n<br><br>\nMeanwhile, feel free to continue any <a href=\"http://slashdot.org/article.pl?sid=dot.kde.org&threshold=-1\">offtopic discussions in this dedicated forum</a>.\n<br><Br>\nThanks,<br>\nNavin."
    author: "Navindra Umanee"
  - subject: "GTK 2.0 == QT 1.4?"
    date: 2001-05-27
    body: "\"Are you worried about GNOME?\n\nNo. We're way ahead of them. When GTK 2.0 is released sometime this year, they'll reach where we were, at version 1.4. QT 3 is going to come out later this year and that will take us even further.\"\n\n\nYeah right. Where's the evidence?\nI don't remember that QT 1.4 has as much stock icons as GTK+ has.\nI don't remember that QT 1.4 fully supports UTF-8, bi-directional text and that kind o stuff.\nHeck, even QT 2.2 doesn't support detachable menus, which I use every day.\n\nSure, QT is better in some things, but so is GTK+.\nThere is no \"superior toolkit\".\nThey are all different, aiming at different goals.\nSo GTK+ is not the best, who cares?\nIt is a good toolkit, and just because QT is better in some areas doesn't change the fact that GTK+ is still good.\nAnd if a GTK+ widget is not good enough for me, I make a subclass of it (yes, GTK+ is OO! in contrast to what you guys think) and implement those features.\nThe same can be done with QT."
    author: "dc"
  - subject: "Re: GTK 2.0 == QT 1.4?"
    date: 2001-05-27
    body: "fine ..."
    author: "AC"
  - subject: "Re: GTK 2.0 == QT 1.4?"
    date: 2001-05-30
    body: ">Heck, even QT 2.2 doesn't support detachable menus, which I use every day.\n\nDear dc:\n\nI think that you'll be pleased to know that detachable menus are possible with QT 2.2. The CVS version of Kicker uses these now, and I suppose you can look forward to them in KDE 2.2."
    author: "Brent Cook"
  - subject: "Re: GTK 2.0 == QT 1.4?"
    date: 2001-05-30
    body: "Great! It's about time."
    author: "dc"
  - subject: "Re: GTK 2.0 == QT 1.4?"
    date: 2001-05-30
    body: "Merits aside, I do have to ask: why bother bringing up this discussion in a KDE forum?  It is obvious that the KDE developers have made up their mind about which toolkit to use.  Seen a GTK KDE program lately?  The interviewee here is a member of the KDE team, so he will obviously \"cheer\" us on.  I don't remember seeing any pro-Qt posts at Gnotices.  Explaining anything about Gtk here is a lost cause.\n\nI also have to ask: have you actually programmed with Qt?  If so, then good.  I'd be glad to finally see an argument coming from someone knowledgable about both sides (I admit I've only done Qt and MFC).  But if not, what are you doing here? :P"
    author: "Justin"
  - subject: "Re: GTK 2.0 == QT 1.4?"
    date: 2001-05-30
    body: "That's not the point. I'm not trying to convince yo guys to switch over to GTK+, I'm simply saying that GTK+ isn't as bad as you guys claims to be.\n\nAnd yes, I've programmed with QT.\nI didn't like it, but that's personal preference."
    author: "dc"
  - subject: "Re: GTK 2.0 == QT 1.4?"
    date: 2001-05-31
    body: "Qt doesn't have stock icons. KDE has its own icon loading and caching system, which includes the ability to have icon themes.\n\nQt supports Unicode since version 2.0 (which was released about two years ago). It does\nnot use UTF-8 which is a hack but a real string\nclass which allows all sorts of string manipulation and direct access to characters by\ntheir index. Bidirectional text is introduced\nin the Qt 3.0 beta.\n\nQt has detachable menus for some time now."
    author: "Bernd Gehrmann"
  - subject: "Re: GTK 2.0 == QT 1.4?"
    date: 2001-05-31
    body: "<I>Qt doesn't have stock icons. KDE has its own icon loading and caching system, which includes the ability to have icon themes.</I>\n\nSo does Gnome 1.x + GTK+ 1.2.\nGTK+ 2.0 however wil support themeable stock icons.\nConclusion: On this point, GTK+ 2.0 is further than QT 1.4.\n\n<I>Qt supports Unicode since version 2.0 (which was released about two years ago).</I>\n\nSo QT 1.4 does not support Unicode, but GTK+ 2.0 does.\nConclusion: On this point, GTK+ 2.0 is further than QT 1.4.\n\n<I>Bidirectional text is introduced\nin the Qt 3.0 beta.</I>\n\nSo QT 1.4 and 2.x does not support bidirectional text.\nConclusion: On this point, GTK+ 2.0 is further than QT 1.4 and QT 2.0.\n\n\n<I>Qt has detachable menus for some time now.</I>\n\nAlso in QT 1.4?"
    author: "dc"
  - subject: "Re: GTK 2.0 == QT 1.4?"
    date: 2001-05-31
    body: "1. You don't understand. KDE does not use Qt\nicons. It has its own icon loading system and\nits own icons. Whether Qt provides any feature\nin this area is absolutely irrelevant for KDE.\n\n2. Qt supports Unicode for two years now. Gtk\nwill in its next version, GNOME perhaps in half\na year, more probably in one year. So it is\n2.5 to 3 years behind technically."
    author: "Bernd Gehrmann"
  - subject: "Re: GTK 2.0 == QT 1.4?"
    date: 2001-11-25
    body: "This is all very interesting, but counting the number of years of advance is not what I consider useful when faced with the choice of one toolkit over the other..\n\nI have just one thing to add here : as a Perl fan, I first used Perl/TK, which I discovered is somewhat limited.\nI've been wondering for some time now whether I should switch to Qt or GTK.\nAt first I thought that Qt looked nicer, and of course more like KDE, which is my preferred desktop for now.\nBut then, switching to Qt would mean going C++ .. and although I could manage it, I'd rather stick up with Perl or 'regular' C as much as I can.\n\nNow, given that the Glade GUI designer can generate Perl-Gtk code, I'm just asking you all : is there any good reason I should prefer Qt+QtDesigner to Gtk+Glade ?"
    author: "Dawit"
---
<a href="http://freeos.com/">FreeOS.com</a> is running an interesting, circa KDE 2.1, <a href="http://freeos.com/articles/4070/">interview with Martin Konold</a>. Apart from a few misspellings and minor errors, it makes for an entertaining read.  <i>"Konqueror is really good. It supports Netscape plugins. Java is supported too. It's extremely standards compliant. When we started out with Konqueror, people questioned our decision. They questioned that Mozilla would be out soon why then, were we building our own browser. Well, it's been two years and look where Mozilla is."</i>
<!--break-->
