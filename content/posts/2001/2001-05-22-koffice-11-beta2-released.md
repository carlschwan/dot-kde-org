---
title: "KOffice-1.1 Beta2 Released !"
date:    2001-05-22
authors:
  - "dfaure"
slug:    koffice-11-beta2-released
comments:
  - subject: "Filters"
    date: 2001-05-22
    body: "just wonderin', how come i can't import MS Word or RTF documents? surely i'm doing something wrong.\n\nrunning debian unstable with KOffice 1.1 beta2"
    author: "Bren.."
  - subject: "Re: Filters"
    date: 2001-05-22
    body: "oh, wait! how odd, i *can* import MS Word and RTF documents (and exort them in RTF) however it does not show up when doing:\n\n1) File\n2) Open..\n3) Filter: <filter name>\n\nhence my confusion, please get this fixed.. it's a small thing but confusing nonetheless :)\n\nlooking good people :)"
    author: "Bren.."
  - subject: "Re: Filters"
    date: 2001-05-23
    body: "If you update kdelibs to 2.2something and recompile KOffice, it will magically work :)"
    author: "gis"
  - subject: "Re: Filters"
    date: 2001-05-25
    body: "OK. Thanx for nice job! It's getting better and better, however...\n1) XFig import filter for KIllustrator does not work in all situations. This is quite important\nfor people who try to use graphs prepared using other tools. \n2)Please, implement filters to/from DIA! I hope it's not a big deal.Currently to move diagrams from dia to killustrator i need to save them as EPS, then use \"pstoedit\" and save them as .fig file then ... well: xfig handles this, but killustrator looses a lot...\n3)What about filters to other popular vector formats?\n---\n4)According to KChart: drawings looks nice, but\nit's uncomfortable to use: Maybe I cannot use it,\nbut currently I'm not able to draw \"curve\" made of points which X amd Y coords are in two columns. :-(\n5)Kword: rightclicking on a frame should pop up some \"properties\" window... i.e.: clicking on picture frame one should be able to change file,\nrotate picture, etc. (or maybe I do not understand the idea of frames). :-(\n---\nAnyway: many, many KUDOs to KOffice team!"
    author: "piters"
  - subject: "Artwork!"
    date: 2001-05-22
    body: "I wish some of the companies investing in KDE development would hire a good artist instead of another developer.\n\nOf course, more and better development is always good, but there are quite a few great developers working on KDE/KOffice while there is a lack of artists to make outstanding icons.\n\nI noticed this because the icons in KWord actually have improved since the last time I seriously played with it, but they are still not perfect.\n\nThis is one area where the big desktop environments (Win, Mac) still have an advantage over the free alternatives. No matter what you think of Microsoft or Apple from a technical or ethical point of view, you have to admit that whatever boots just looks gorgeous.\n\nMost 32x32 icons under KDE look very nice as well, but the 22x22 toolbar icons are usually not so beautiful. :-(\n\nPlease people, if you know your way with Krayon, The Gimp or Photoshop or a similar application, read artist.kde.org and submit to icons@kde.org!\n\nIt's just eye candy, but users like candy."
    author: "Rob Kaper"
  - subject: "Re: Artwork!"
    date: 2001-05-22
    body: "Before the artists think I am dissing them, let me rephrase: there is not a lack of good ones, but a shortage. :-)"
    author: "Rob Kaper"
  - subject: "Re: Artwork!"
    date: 2001-05-22
    body: "Agreed, we could need some more Tackats and Qwertz'. Just call the T&Q Factory."
    author: "Lenny"
  - subject: "Re: Artwork!"
    date: 2001-05-22
    body: ">I wish some of the companies investing in KDE development would hire a good artist...<\n\nIIRC tackat works for SuSe, and I don't know about qwertz. They are excellent artists.  \nI have never seen better mimetypes icons on any OS, even not on Mac.\n\n> No matter what you think of Microsoft or Apple from a technical or ethical point of view, you have to admit that whatever boots just looks gorgeous.<\n\nMicrosoft Windows 95 & 98 icons are terrible, icons are very nice in Windows 2000, and Apple MacOSX icons are excellent (though not those on MacOSX dock bar - they are inconsistent). I don't agree that whatever boots there is just gorgeous.\n\n>Most 32x32 icons under KDE look very nice as well, but the 22x22 toolbar icons are usually not so beautiful. :-( <\n\nHm, 22x22 icons can not be 'beautiful'. If you want to make them nicer they have to be very simbolic and have to have only a few contrasting colours IMHO. Creating icons is not so simple as people might think, especially not the whole set of icons. 'Problem' with KDE is that it,  fortunatelly, has too many icons. There are, for example, 50-60 (or more) actions icons which come in 16x16, 22x22 and 32x32 sizes. So, just for a set of actions icons you have to create about 180 icons. And they have to be consistent in look & feel. \n\nBut, that is also the main problem why more people don't work on kde icons. I can not only create different icons for Konqueror, because some of Konquerors icons appear in Kmail (but only some of them) and some of them in Kedit, and some of them in Kword etc. So I have to create also toolbar icons for almost all kde applications. In my opinion that should be changed, so Konqueror has its own icons, Kmail has its own icons etc. Otherwise, if I create only icons for Konqueror and not for Kmail etc. my icons in Kmail etc. will be inconsistent.\nWe can only expect that artists would like to create icons for one kde application but not for all of them. Very few people have so much time."
    author: "Antialias"
  - subject: "Re: Artwork!"
    date: 2001-05-23
    body: ">> Microsoft Windows 95 & 98 icons are terrible, icons are >> very nice in Windows 2000, and Apple MacOSX icons >> are excellent (though not those on MacOSX dock bar - >> they are inconsistent).\n\nI disagree on the MacOSX dock bar. From an artistic point of view they kick ass. And lets not forget those lovely animations. I want that eyecandy in my KDE 2.2\n\nKUDOS to the KDE ARTISTS"
    author: "Raphael Borg Ellul Vincenti"
  - subject: "Re: Artwork!"
    date: 2001-05-23
    body: "> I can not only create different icons for\n>  Konqueror, because some of Konquerors icons > appear in Kmail (but only some of them) and\n>  some of them in Kedit, and some of them in \n> Kword etc.\n\nThat's wrong.\nIf you install your icons into $KDEDIR/share/apps/konqueror/icons/...\nyou can make your icons appear in konqueror _only_. :-) It's just not possible to install such \"iconskins\" using a GUI yet.\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: Artwork!"
    date: 2001-05-23
    body: "Wauu, that's great. I didn't know that. Is it possible to do the same thing for all kde apps?\nIIRC there is not Icons folder in $KDEDIR/share/apps/kmail for example. Anyway, I'll be back at home on Sunday and work, work :)\nThanks, Tackat."
    author: "Antialias"
  - subject: "Re: Artwork!"
    date: 2001-05-24
    body: "> Wauu, that's great. I didn't know that. \n\nYou didn't ask.\n\n> Is it possible to do the same thing for all kde apps?\n\nYes! This is the way the iconloader works. \nDid I already mention that the iconloader is very flexible ;) ?\n\n> IIRC there is not Icons folder in \n> $KDEDIR/share/apps/kmail for example. \n\nCreate one. And the rest of the directory-structure like in $KDEDIR/share/icons. Should work. While it was made on one hand this way to avoid name-clashes it can be used on the other hand to create icon-skins. Smart, heh? ;)\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: Artwork!"
    date: 2001-05-22
    body: "I like the icons on KDE. The only thing I don't like about them is that I can see a white box around them. Are there any plans to get rid of this?"
    author: "Daniel"
  - subject: "Re: Artwork!"
    date: 2001-05-22
    body: "The problem with them, in most cases, is that they have too many bright colors.  I have no problems with Konqueror, but in KOffice, the buttons are distracting,  Icons are something Microsoft got right: they used a mundane pallet toolbar-wide, and still managed to get high-quality, obvious and symbolic icons"
    author: "dingodonkey"
  - subject: "Re: Artwork!"
    date: 2001-05-23
    body: "I have no problem with KOffice icons. Are we talking about same KOffice? I run one from cvs, and when you have icons set to 16x16 or 22x22 they are not dictracting. Only if you set toolbar icons to 32x32 some of them are distracting because they have different size (and that's because they are not finished yet). In KOffice beta1 (and probably beta2)\nicons are OK (at least 16x16 & 22x22 ones).\nProblem with previous versions of KOffice was that when you run it you got by default too many toolbars opened, and IIRC there were too many colourful icons, but not now.\nTry to open too many toolbars in Microsoft word and you'll get the same result, you wont be able to see what is what.\nAnother thing, you forget that we all used Microsoft Word for a long time and we learned icons position on the toolbar.\nKOffice toolbars have another good feature (all kde toolbars):\nyou can use text instead of icons on some (or all) toolbars., and what is most important you can contribute and create some nice high-quality, obvious and symbolic icons :)"
    author: "Antialias"
  - subject: "Re: Artwork!"
    date: 2001-05-23
    body: "> I noticed this because the icons in KWord \n> actually have improved since the last time I \n> seriously played with it, but they are still not \n> perfect.\n\nDid it come to your mind that KOffice 1.1 hasn't been released yet? Might it be possible that the stuff you are talking about is work in progress?\n\nTackat"
    author: "Tackat"
  - subject: "Re: Artwork!"
    date: 2001-05-24
    body: "> Did it come to your mind that KOffice 1.1\n> hasn't been released yet? Might it be possible\n> that the stuff you are talking about is work\n> in progress?\n\nThat's why I noticed they actually improved and I trust they will keep improving.\n\nHm, wasn't it you who recently said that icons is one area where KDE has a shortage of volunteers? (I think it was one of the reasons for moving the locolor theme to kdeartwork)\n\nStill, wouldn't you like it if for once instead of a developer an (extra) artist would be funded? :-)"
    author: "Rob Kaper"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "Is there any chance we can hope to see a MS Word _export_ filter or a WordPerfect import and export filter in the near future? This would be really great to see, and would make it a lot easier to share documents with 93% (or so) of the office world that uses MS or Corel office suites...\n\n  Thanks,\n       Tim"
    author: "Timothy R. Butler"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "I am wondering even how MS import filters are implemented in KDE. Aren't DOC, XLS, etc. file formats \nMS proprietary?"
    author: "Bojan"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "I don't know, I guess reverse enginering, or something like that? Now that OpenOffice is out there, I suspect that new data could also be gleamed from there - I sure wish they would at least... KWord has potential, but I still don't see how it can be truly useful in a world that mainly uses MS Word, if it can't save to MS Word format."
    author: "Timothy R. Butler"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-23
    body: "Microsoft make the format for Word documents available on their website somewhere.\n\nCan't remember exactly where but it was part of the MSDN site."
    author: "Leon"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-23
    body: "They did make it availabe. Now you have to sign an agreement. I don't know its content, but I may guess that it includes a non-competing clause. But there are still some copies around.\n\nBTW, FYI, reverse engineering is legal in several countries in the world, including France. So this is NOT a problem."
    author: "Hubert Figuiere"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-23
    body: "I see. Thanx for your explanation. On the other hand,\nif format would not be in public domain, it is one hell of a\njob to reverse engineer it. :-("
    author: "Bojan"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-24
    body: "> if format would not be in public domain, it is \n> one hell of a job to reverse engineer it. :-(\n\n...which is exactly why M$ has such a strong\ngrip on the Office market.\nThey don't regularly change their Office file\nformats without a reason..."
    author: "Marvin"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-26
    body: "Ah yes, welcome to Microsoft, home of the moving target Word format. I'm fairly sure that even subminor revisions of their suite (i.e. x.x.x) involve a total redo of the file format, or at least enough changes to make it difficult for people to reverse engineer file formats. Perhaps the U.S. gvmt should have dis-monopolized M$ the quick way and forced them to OSS bazaar-style all their protocols and file formats. :-)"
    author: "Carbon"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-23
    body: "Hi All,\n\nFor what it's worth, I'm the lead developer/maintainer of AbiWord (www.abisource.com) and wvWare (www.wvware.com)\n\nMSFT's file formats aren't proprietary, nor is reverse engineering them illegal. If you go to either site, you'll see OLE2 docs, docs on the DOC format, etc...\n\nwvWare and Abi both have really good support for importing DOC. I'm trying to add support for exporting to DOC inside of wvWare right now, though. If anyone from the KWord development team is reading this and would like to collaborate on making something like this a reality, I'd really appreciate it. I'd hate to see a duplication of effort on this sort of scale when it could so easily be avoided.\n\nThanks,\nDom"
    author: "Dom Lachowicz"
  - subject: "when will abiword support tables?"
    date: 2001-05-23
    body: "So when do we get tables in abiword. I love abi.\n\nAnd are guys sharing code with the Koffice guys?\n\nCheers"
    author: "naloquin"
  - subject: "Abi=Import/KWord=Features, but me?  LyX BAY-BEE!!!"
    date: 2001-05-23
    body: "It is obvious to me that AbiWord is striving for the best DOC import quality, and features are slowly coming in.  KWord seems to be the opposite, features and capabilities are priority, DOC import secondary.\n\nFrankly, what are you doing using a document \"standard\" that the company that created it cannot even get right?!?!?!  E.g., ever try MS Word for Mac?  And, even worse, how about MS Excel for Mac?  Case closed.\n\nIf you want to have your documents last 20 years, use LaTeX or SGML.  There is a great front-end for them called LyX (http://www.lyx.org).  Better yet, you'll find yourself not wasting productivity on fonts and other BS, and writing your document, letting it do all the formatting details (table of contents/figures/tables, margin notes, indicies, footnotes, etc...).  I've been using it for close to 3 years now, where I produce 90% of my technical documentation and presentations (built-in PDF export).\n\nI still use Abi for DOC import (and then cut'n paste into Linux), or for quick, non-technical .  I'm evaluating KWord for desktop publishing, where LyX won't do (so far, so nice!).  I _hate_ word processors, cried when Lotus killed AmiPro and Adobe killed the FrameMaker beta for Linux."
    author: "Bryan J. Smith"
  - subject: "Re: Abi=Import/KWord=Features, but me?  LyX BAY-BE"
    date: 2001-05-24
    body: "Just FYI and all...\n\nLyX uses wvWare to convert DOC->LaTeX, which LyX then imports. Abi also uses wvWare, but in a different way. Writing a direct LyX output filter for wvWare, for example, should be trivial if someone wants to step up and do the work (I personally have no interest, but will add others' code to the tree). Figure out how to email me.\n\nDom"
    author: "Dom Lachowicz"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "To export to MS Word, just save as \"Rich Text\"!"
    author: "Arnd Bergmann"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "Yeah, that works - at least there is finally some kind of document type that Word users can read that KWord supports... however, I'm pretty sure RTF does not support clip art, etc. in it, right? I suppose it is at least a step in the right direction, and I can't complain about that. :-)\n\n  Thanks,\n       Tim"
    author: "Timothy R. Butler"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "I believe that the file format used by WordPerfect 8.0 for Linux is the same as MS Word.  Try it and see if it works."
    author: "Kyle Haight"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "Hi Kyle,\n  Thanks for the suggestion. I use WP8 right now, and it actually does support Word and WordPerfect format. Between that and StarOffice, there is support for Word doc's in Linux - although KOffice has a much nicer interface than the other office suites. <sigh> \n  \n  At least KOffice is one step closer to the other office suites, now that it has RTF support...\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "More Filters"
    date: 2001-05-22
    body: "...and an Acrobat PDF export filter!"
    author: "Stephan Boeni"
  - subject: "Re: More Filters"
    date: 2001-05-23
    body: "With the new printing architecture in CVS, it's already possible. This is transparent for *all* KDE applications. You can define several \"virtual\" printers, apply filters to them etc."
    author: "Lukas Tinkl"
  - subject: "More Filters"
    date: 2001-05-22
    body: "...and an Acrobat PDF export filter!"
    author: "Stephan Boeni"
  - subject: "PDF filter"
    date: 2001-05-23
    body: "From what I have heard this has already been implemented in CVS. For all apps, not just koffice. You can \"print\" as pdf with the new print system."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-24
    body: "It is actually possible to get KWord to save files you can send to your Windows friends. Use Print to File to get a PostScript version of the doc, then use the ps2pdf program to convert it into a PDF. Everyone can read those blighters - even if they don't want to!\n\n(Actually - is there support for people to Export Postscript and PDF documents in the file menu. A PDF export option would look pretty funky - you could always grey out the PDF option if ps2pdf wasn't installed on the system)."
    author: "Bryan"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "Guys from KDE, you have done great work once again.\nAnd BTW, personally I like the icons better than M$ or Mac ones."
    author: "Bojan"
  - subject: "Broken dependancies"
    date: 2001-05-22
    body: "The redhat 7.1 relies on libkdeprint which I assume is only in the CVS version of KDE. Any chance of fixing this - the press release says that it only depends on KDE 2.1.1/2."
    author: "Dr LHA"
  - subject: "Re: Broken dependancies"
    date: 2001-05-22
    body: "libkdeprint is part of the 2.2 alpha releases, yes. Please tell bero (the RH packager) and if you're lucky he will create 2.1.1 \"compatible\" packages, too."
    author: "Werner Trobin"
  - subject: "Mandrake awake?"
    date: 2001-05-22
    body: "But then why is the Mandrake 8.0 link pointing to a KOffice-1.1 Beta__1__ rpm?\nMaybe I should compile from source instead.  What is the KDEDIR/KOFFICEDIR on Mandrake 8.0?"
    author: "Confused"
  - subject: "Re: Mandrake awake?"
    date: 2001-05-23
    body: "The Mandrake packager had an off by one error :-) It has been corrected and KOffice-1.1Beta_2_rpm's should be available on the ftp site by now."
    author: "Waldo Bastian"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "Mandrake RPMs missing again? hmmm"
    author: "DiCkE"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "Im downloading for Mdk 8 right now\nhttp://ftp.sourceforge.net/pub/mirrors/kde/unstable/koffice-1.1-beta2/Mandrake/8.0/kdelibs-2.1.1/RPMS/"
    author: "Jos\u00e9"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "I was premature.. there is only beta1 and not beta2. Sorry"
    author: "Jos\u00e9"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-23
    body: "Should be fixed now."
    author: "KDE User"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-23
    body: "seemed to be not !"
    author: "cyprien"
  - subject: "Doesn't run for me"
    date: 2001-05-22
    body: "Anyone else finding that once they installed it, it just doesn't run? Starts up, but then exits before displaying anything. Doesn't crash, just exists."
    author: "Nick"
  - subject: "Re: Doesn't run for me"
    date: 2001-05-23
    body: "yup, me too.  running Redhat 7.1, KDE 2.2alpha1."
    author: "unterbear"
  - subject: "Re: Doesn't run for me"
    date: 2001-05-24
    body: "Me too. Here is my errors (RH 7.1, KDE updated to 2.1.2):\n\n[root@localhost /root]# kword\nkword: error while loading shared libraries: libkdeprint.so.0: cannot load shared object file: No such file or directory\n[root@localhost /root]#\n\n[root@localhost /root]# kspread\nkspread: error while loading shared libraries: libkdeprint.so.0: cannot load shared object file: No such file or directory\n[root@localhost /root]#\n\nThis info shows up in a terminal Window on the KDE desk top when I try to open the office apps. Had the same problem with aethera, but found there was also a lib file *.rpm. Installed the lib and aethera works, but Koffice still not. \n\nI stalled Koffice 1.1 on my other SuSE system and all Koffice apps work. (?) Maybe RH rpm problem??\n\nLove your desktop! It's finally made Linux palatable!!!\n\n\nScott"
    author: "Scott"
  - subject: "Re: Doesn't run for me"
    date: 2001-05-26
    body: "the problem is that you used the --nodeps option when installing the rpm and you shouldn't have done that.\n\nthe koffice rpm for redhat linux is compilled against kde 2.2alpha1 and requires that version of kde to run."
    author: "Evandro"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "Wow !!!\nNow KWord is coming closer to real productivity !\nWithin the first tests i noticed two problems:\n\n1.) How do i get continuous Page numbers into the footer ? Inserting a page-numer variable works fine, but the following pages show the same page number !\n\n2.) Applying 2 columns to the main text frame through Format->Page->Columns results in strange behaviour when going to the following pages. Three or more columns works pretty fine !\n\n---------------------------------------------\n\nThanks to the whole K-Office-team !!! You're doing a great job !"
    author: "Tom"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-24
    body: "1) is a known bug, I have to find a way to update variables in copied frames\n\nI don't understand 2). Two columns seem to work fine [except for a post-beta-2 bug with page removal when the last page is empty, I'm looking at this right now]. What is \"strange behaviour\" ? http://bugs.kde.org would be a good place to continue this discussion :-)"
    author: "David Faure"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-24
    body: "Oh, page removal (in the multiple-columns case) was probably broken in beta2 too. Found a nasty bug. Fixed :)"
    author: "David Faure"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-26
    body: "I've checked out CVS today and this seems to work well now !\n\nThanks."
    author: "Tom"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "Great job :)))"
    author: "Antialias"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-22
    body: "I sure do wish those rpms for redhat weren't linked against libkdeprint :(."
    author: "itodd"
  - subject: "Re: for Webmaster"
    date: 2001-05-23
    body: "Please make these disgustingly wide posts fit to the\nbrowser width. I hate (am I the only one?) dragging the bottom scrollbar just to read a single line of text."
    author: "Bojan"
  - subject: "Re: for Webmaster"
    date: 2001-05-23
    body: "Yes, you probably are.  Why doesn't your web browser auto-wrap the text?  What browser are you using?"
    author: "Navindra Umanee"
  - subject: "Re: for Webmaster"
    date: 2001-05-23
    body: "In Konqueror on this machine, I get a scrollbar if the window is less than 850 pixels wide. So if either your screen is 800x600 or your browser window doesn't fill the entire screen, it's possible you'll get a horizontal scrollbar."
    author: "Maarten ter Huurne"
  - subject: "Re: for Webmaster"
    date: 2001-05-24
    body: "Hmmmm.  I use Konqueror and the scrollbar doesn't appear until it's really thing, like 25% of my 1280x1024 screen(by then I would rather have the horizontal scrollbar rather than two words per line.  It seems perfect to me.  What version?  I have 2.1.2\n\nMaybe it is screen dependent."
    author: "Henry Stanaland"
  - subject: "Problem found"
    date: 2001-05-24
    body: "There is a really long URL in one of the posts (about Mandrake 8 packages, that turned out to be beta 1). That URL is determining the window width: it is considered a single word and therefore not split over multiple lines.\n\nThe reason you didn't notice is that since I posted, this thread grew a lot. The post body with the long URL is no longer displayed on the main thread page. If you set \"thread threshold\" high enough (1000 should do :), you will see a horizontal scrollbar much sooner.\n\nEither the browser or the site should split extremely long words. The browser is most capable of determining the best point to split, since it knows the window width. However, it may be difficult to decide for example which column in a table is too wide."
    author: "Maarten ter Huurne"
  - subject: "Re: for Webmaster"
    date: 2001-05-23
    body: "I use Konqueror, isn't that obvious? ;-))\nI don't know where is the option to set word-wrapping on.\nI thought this should be enabled by default?!"
    author: "Bojan"
  - subject: "Re: for Webmaster"
    date: 2001-05-24
    body: "You shouldn't have to do anything special. :(  Do you see the same problems on Slashdot.org?  Are you using 800x600 too?  \n\nI have never noticed a problem except when people use <pre> but no-one is doing that here...\n\n-N."
    author: "Navindra Umanee"
  - subject: "Not So Quick :("
    date: 2001-05-23
    body: "Navin... at 800x600, with deeply nested comment threads, khtml will have to scroll...\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Not So Quick :("
    date: 2001-05-24
    body: "Hmmm, suck.  Is this a problem with KDE Dot News or a problem in general?"
    author: "Navindra Umanee"
  - subject: "Re: Not So Quick :("
    date: 2001-05-24
    body: "No it is even in slashdot. Seems like KHTML problem, sorry."
    author: "Bojan"
  - subject: "khtml.."
    date: 2001-05-24
    body: "khtml seems to want to keep every comment a decent width, whilst ie will squish a deeply nested comment to 2-3 words per line.. \n\npersonally I like konqy's way better (no single comment will ever be larger than width of screen)\n\nalso note that in a 1024x768 window its all okay :)\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: for Webmaster"
    date: 2001-05-23
    body: "As a substitute for dragging sideways, you can use the mousewheel while the mouse is hovering over the bottom scrollbar (you *do* have a mouse with a wheel, don't you?  They're so nice!).  KDE's got lots of neat little features like that :-)"
    author: "not me"
  - subject: "Re: for Webmaster"
    date: 2001-05-25
    body: "Too bad that you can't have mice with two scrollwheels, since I REALLY hate scrolling with the keyboard :)"
    author: "David Watson"
  - subject: "Re: for Webmaster"
    date: 2001-05-26
    body: "Actually there *is* such a thing.  You can buy mice with both vertical and horizontal scrollwheels.  In fact, my current mouse can scroll horizontally.  It has a little post in place of the wheel that acts like those little laptop mice that are in the middle of the keyboard.  Push it up and it scrolls up, push it sideways and it scrolls sideways.  Unfortunately, sideways scrolling only works in Windows.  I don't think it's possible to do in X."
    author: "not me"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-23
    body: "Actually, what I want to know is why nobody ever mentions grammer checking(other OS's feature list went from spell checking to grammer checking about 5 years ago).  Why not a generic grammer checking library could be created so that all Linux software could use it or is it next to impossible?  ...does one already exist?\n\nDoesn't StarOffice have grammer checking?  If it does...let's borrow it."
    author: "Henry Stanaland"
  - subject: "grammar"
    date: 2001-05-23
    body: "It is called grammAr not grammer. You don't even use the spell checking.\n\nAnd basically grammar checking without real artificial intelligence is impossible and gives shitty heuristic results. PLUS it would only work in english, not in all the other languages. Anybody who needs a grammar checker, should get a new brain, not a new program."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: grammar"
    date: 2001-05-23
    body: "I fully agree with you. Here in Italy, everybody I know disables \"grammar checking\" in Word soon after the installation (it just says bullshit).\n\nI also think that even the need of \"spell checking\" is a cultural product. Here in Italy, everybody is assumed not to write spell errors (it's one of the goal of the primary school): if you write something with spell errors in it at the secondary school you automatically get a bad vote.\nOn the other hand, English (and above all Americans) are not so good at spelling (I once heard that Americans have spelling Olympiads!!)"
    author: "Federico Cozzi"
  - subject: "Re: grammar"
    date: 2001-05-23
    body: "I totally disagree, spellchecking is essential. And so is auto-ma-tic word-split-ting (I'm not sure of the English word), especially if you set up a text in 2 or 3 columns."
    author: "BL"
  - subject: "Re: grammar"
    date: 2001-05-24
    body: "is \"hyphenation.\" :)\n\nAnd yes, it's very useful, would like to see it improved! Small things like that, added together, are what make a word processor workable. \n\nAgreed re: spellchecking! I am a decent speller, and not a terrible typist, but I make plenty (more than enough, let me admit) of spelling mistakes which a spellcheck option would eliminate. What I dislike is the annoying intrusive behavior of MS's spellcheckers which by default do things like kill two initial caps, even if that's what I *really mean.* (Please, guys, make that a selectable option, not the default! :))\n\n\nTim"
    author: "the word you're looking for ..."
  - subject: "Re: grammar"
    date: 2001-05-23
    body: "Yes, Americans have spelling Olympiads. We call them spelling bees.\n\nLook at http://www.everything2.com/ and search for spelling bee. One of the explanations you will find there is as follows:\n\nby ErisDiscordia\n\nSpelling bees are especially relevant for the English language, because its spelling is so highly non-phonetic. (I remember one grade-school teacher illustrating this by writing \"PHOTI\" on the chalkboard and informing the class that, given the wacky behavior of English pronunciation, it could even spell \"fish\" -- his (?) explanation was something like\n \n \"PH\" as in \"phoneme\"\n \"O\" as in \"women,\" and\n \"TI\" as in \"pronunciation.\""
    author: "Brent Cook"
  - subject: "Re: grammar"
    date: 2001-05-24
    body: "The normal one is \n\nGHOTI \n\nGH as in touGH. \n\nthis is from George Bernard Shaw, \nwho was an ardent campaigner for spelling reform. \n\nPH kind of makes sense... GH does not!"
    author: "Rob"
  - subject: "Re: grammar"
    date: 2001-06-01
    body: "the gh comes from the v old english pronounciations. so night would sound close to the dutch nacht. the gh is like a ch (as in loch, said the scottish way) but a little softer I think. scots still has stuff like that but spelt nicht. to put it simply english was created by ppl who couldn't make the sound ch (like chi, in greek)\n\nbtw I think a grammar checker is v usefull if only to pick up double periods or to captialise words in an intelligent way. perhaps we could use this opportunity to create a decent grammar checker and not the crap M$ makes."
    author: "graeme"
  - subject: "Re: grammar"
    date: 2007-12-08
    body: "If you got PHOTI or GHOTI...try this one\n\nWhat does \"ghoughphtheightteeau\" spell?\n\nPotato\n\nGH as in hiccouGH\nOUGH as in dOUGH\nPHTH as in PHTHisis\nEIGH as in nEIGHbor\nTTE as in gazeTTE\nEAU as in bEAU"
    author: "Shannon"
  - subject: "Re: grammar"
    date: 2001-05-23
    body: "You are not trying to imply that Italians are smarter than Americans, do you?"
    author: "Bojan"
  - subject: "Re: grammar"
    date: 2001-05-23
    body: "No, I was implying that:\n1) Americans and English speaker need a spell-checker more than Italians (due to language difference)\n2) Italians don't need a grammar checker (at least, not the MSWord one) because Italian grammar is very difficult\n3) Italians need automatic hyphenation for word-wrap at end of line\n\nWhenever I use MSWord, I realize that it was written by Americans with English language in mind. MSWord has 1) and 2) but (to the best of my knowledge) not 3)"
    author: "Federico Cozzi"
  - subject: "Re: grammar"
    date: 2001-05-23
    body: "Spell checking has nothing to do with spelling skills. You can be excellent at spelling but still type incorrectly (typos)."
    author: "Martina"
  - subject: "Re: grammar"
    date: 2001-05-23
    body: "I don't know anything about the Italian language but, evidently, you know English. You must know then that English is an inconsistent language. Spelling and pronunciation are often unrelated. Haven't you ever forgotten how to spell a word? Are Italian spellings more consistent than English spellings?"
    author: "kdeFan"
  - subject: "Re: grammar"
    date: 2001-05-23
    body: "Yes, Italian spelling is very easy (with some obvious exceptions). On the other hand, its syntax is a nightmare - that's why MSWord doesn't understand it!"
    author: "Federico Cozzi"
  - subject: "Re: grammar"
    date: 2001-05-24
    body: "No, most people learning English as a foreign language, learn how to write. Optionally they learn how to pronounce it."
    author: "Anonymous"
  - subject: "Re: grammar"
    date: 2003-02-27
    body: "Yes,I also noticed the same thing.I was very surprised to see that native English speakers are such lousy spellers.In my country everyone is supposed to know how to spell the words and if you learn English and can't spell it we dont say \"He's not too good at spelling\",we say \"He hasn't learned English very well\".And I've noticed that foreigners are much better spellers both in their own languages and in English,than the native English speakers are."
    author: "Sanja"
  - subject: "Re: grammar"
    date: 2001-05-23
    body: "My bad spelling is proof that we need those features.  I am not claiming to be the best speller in the world.   I don't go to a liberal arts school....but I'll integrate the hell out of you though(we have \"Integral Bees\" not \"Spelling Bees\").\n\nBesides why are you people so mean.  If you are having family troubles or something, go see counseling, don't take it out on other people. IBM is wrong with their \"Peace, Love, Linux\" because you can't post something with a simple mistake and not have @$$ holes complaining and contradicting everything like they are right about everything.  The reason I \"didn't use spell checking\" is because browsers don't have spell checking in text boxes.  Perhaps they should...the way everybuddy(not everybody) spell checks your IM messages.\n\nGrammer checking can be usefull though.  For instance, \"Neither John or Mike will go.\"  You know the rule...\"nor is to neither as or is to either.\"  Or something like that.  I always had positive results from grammer checking(especially at like 4:00a.m. when you can't tell the difference....beside it's not like it changes your paper...it only underlines the problem.  Furthermore, grammer checking tells the difference between \"their\" and \"there\"\n\nYes, I typed grammer instead of \"grammar\" the whole time.  Pero a mi no mi vale la madre."
    author: "Henry Stanaland"
  - subject: "Re: grammar"
    date: 2001-05-24
    body: "Oh comeon, don't cry. :-) I just think there are more interesting things missing than grammar checking for one (important) language.\n\nPerfect Hyphenation (latex-like), automatic indexes, better filters for rtf and other stuff.\n\nGrammar checking is not really that useful. And that is just MNSHO."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: grammar"
    date: 2001-05-24
    body: "I agree, there are more important things that should have priority.  But you have to plan features from the beginning(not necessarily implement them), otherwise you end up with something that feels like it was glued together.  One of our professors who has intimite knowledge with MS said one of their reasons for having bad code design is because it really isn't designed.  They plan like 20% of the software features(the part people really need), and then throw the rest in later as best they can."
    author: "Henry Stanaland"
  - subject: "OK,OK"
    date: 2001-05-24
    body: "Well, IF someone feels thata grammer correction for badly written english texts is a must, I would implemenet it as a normal commandline tool (ala ispell). Then the GUI programs could use this as a plugin.\n\nI don't think you need to change any koffice design to integrate such a thing. It would also be useful (as far as this is possible...) for kmail, klyx..."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: grammar"
    date: 2001-05-24
    body: "No - I don't can't agree with you here. I find the grammar check in Word 2000 really useful. The one in Word 97 wasn't very good, but it seems to have been much improved....."
    author: "john"
  - subject: "Re: grammar"
    date: 2001-05-24
    body: ">don't can't agree \n\narghh. I grammar check would have spottet that though.... ;)"
    author: "john"
  - subject: "Re: grammar"
    date: 2001-05-24
    body: "This entire thread is a travesty of poor spelling, typing and grammar.\n\nI think that we would be better served by an irony checker. Is there a chance that someone will add that to Kword?\n\nAlanis Morissette's song 'Ironic' could have benefited that kind of tool."
    author: "Brent Cook"
  - subject: "Re: grammar"
    date: 2001-05-26
    body: "Spell checking wouldn't have picked up grammer - 'grammer schools' in the UK?\n\nAny chance of Konqueror implementing a 'check spelling' option when you right-click on a text box?\n\nI personally can put up with most grammar errors, except for apostrophes. For example, on a sign at a market stall there could be:\n\nCarrot's 30p\n\nThe intention obviously being that carrots cost 30p however I want to see the 30p belonging to the carrot that can acrue wealth :-p\n\nThe other killer is 'its'. It's not too hard really but people don't get it:\n\nThe dog pulls on its lead< - no apostrophe.\n\nI like England because it's cold - apostrophe since it is an abbrevation for 'it is' as opposed to indicating possession.\n\nI have a lecturer who subtracts marks from assignments where apostrophes have been used correctly, and on the other hand I have one who just can't use them. This is one thing that irritates me about the way English is taught in the UK - primary school English is fine, learning SPG (spelling, punctuation and grammar). However, as soon as you go to high school it's over to the novels, plays and so on so the people who do well aren't the people who can write 'good English' but the people who can blag about plays.\n\nAnyhow, just my 2c.\n\nPS. I know I've probably used 'comma conjunction' several times in this posting..."
    author: "Andrew Coles"
  - subject: "Re: grammar"
    date: 2003-02-27
    body: "I agree that grammar checkers are usually useless,because computer can't recognize if the whole sentence is written correctly or not.And as for the spelling checkers,I agree with that Italian guy,depends on a language.I was personally very disappointed to see that most native English speakers are such terrible spellers,especially Americans.I've always thought that they have to be perfect at it because it's their own language and everybody knows how to spell their own language.It would be ridiculous if I had to open the dictionary to see how to spell the word from my own language.I wrote those words too many times to forget how to spell them:) And to be honest,native English speakers are not only bad spellers (much worse than most foreigners),but they don't even understand the most basic rules of their language.For instance,they don't know the difference between \"to\" and \"too\",\"you're\" and \"your\",\"who's\" and \"whose\",\n\"they're\",\"their\" and \"there\",\"than\" and \"then\",\"of\" and \"off\",\"it's\" and \"its\"\netc.Any foreigner who speaks English will understand those differences.I noticed that this Italian guy who wrote a few comments here (can't remember his name) knows the difference between \"it's\" and \"its\" very well,and 90% of native English speakers don't know it.I think that's a real shame."
    author: "Sanja"
  - subject: "Re: grammar"
    date: 2003-02-28
    body: "You think English words are hard to spell? Try spelling these German words:\n\n offentlichkeitsreferent\n abwechlungsreiche\n begegnungsarbeit\n leistungsschutzrechte.....\n\nAnd we still don't need a spell checker,and you guys do :P"
    author: "Heintz"
  - subject: "Re: grammar"
    date: 2004-04-18
    body: "I disagree.  None of these words is hard to spell \nespecially if you know the word in the first place.  \n\nThe problem with English (as mentioned several \ntimes) is that the spelling and the pronounciation \noften don't correspond 1:1, for example \"their\" and \"there\". \n\n"
    author: "cm"
  - subject: "Re: grammar"
    date: 2003-02-28
    body: "I've also noticed that English speakers (especially Americans) are bad spellers and very dependent on spell-checkers. Here in Greece we don't need spell-checkers at all,because it's understandable that that we know how to spell our own language.And most Greeks speak English too.But I've noticed that most people who speak English as a second language are better spellers than the native speakers???"
    author: "Christina"
  - subject: "Re: grammar"
    date: 2003-02-28
    body: "Maybe it's true that English spellings and pronunciations are not related,but for God's sake,if you see and use those words every day,how can you not remember how to spell them?"
    author: "Natacha"
  - subject: "Re: grammar"
    date: 2003-02-28
    body: "French spellins is also unrelated to its pronunciation.For instance,\"O\" can be spelled as \"ault\" (Renault),\"eot\" (Peugeot),\"eaux\" (Bordeaux),\"aud\" (Rimbaud),\n\"uot\" (Clicquot) etc. But we still know how to spell the words,because we use them all the time and it's not likely that we will make a mistake."
    author: "Laurelle"
  - subject: "Re: grammar"
    date: 2007-12-08
    body: "I'm a native English speaker. I'm studying French for the sixth year as well as Latin for the second year. I have a much easier time with French spelling than English spelling."
    author: "Shannon"
  - subject: "Re: grammar"
    date: 2004-03-30
    body: "Correct me if I'm wrong, but \"Abwechlungsreiche\" is missing a \"s\" as in \"Abwechslungsreich\". As you can see, a so-called \"Spell Checker\" does come in handy... everybody makes mistakes.\n\nI'm sure 90% of native English speakers know the difference between \"it's\" and \"its\". Let's stop this Anti-English prejudice.\n\n"
    author: "iuseaspellchecker"
  - subject: "Re: grammar"
    date: 2004-03-30
    body: "Oh, you're soooo wrong. Studies show that 78% of people in the UK don't know the difference between it's and its, and I guess that percentage is even higher in the USA. "
    author: "Sanja"
  - subject: "Re: grammar"
    date: 2004-03-30
    body: "I even saw they wrote \"it's\" instead of \"its\" in an Australian TV show... And you can see that everywhere, even in the newspapers. And about that \"Abwechslungsreich\", I'm not German, I saw it on a CD cover and noticed how big the word was and just put it there :P  But see, you as a native German speaker know how to spell it correctly. Most native English speakers couldn't spell even the most common English words. Exactly my point."
    author: "H."
  - subject: "Re: grammar"
    date: 2004-04-18
    body: "Your right. Are language is not easy but I think spelling and grammer are important and people should start caring more about them."
    author: "Sean"
  - subject: "Re: grammar"
    date: 2004-04-26
    body: "Hmmm!\n\nYou're (not YOUR) right.  Our (not ARE) language is not easy, (comma goes here) but...caring more about them.  Before you ignorantly attempt to bash native English speakers, check your message before you submit.  "
    author: "Zane"
  - subject: "Re: grammar"
    date: 2006-03-06
    body: "Winner at grammar.\n\nLoser at sense of humor. :P"
    author: "an Onymous"
  - subject: "grammer checking"
    date: 2006-07-30
    body: "management has or have ?  check his sentence grammer? Any other instant dictionary for checking grammer, please help me"
    author: "george"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-23
    body: "ok, so eventually the Mandrake RPMs came around. And as far as I concern you guys are unbelivable, moving along like this and KDE got another killer app(s) like konqi! It's GREAT it's f*kin wonderful being a KDE-Linux user these days!!\n\nIn order to get more space for more innovative stuff:\n\nRPM -e netscape, (star)openoffice and whats next;)\n\nThanks for all your great work!! Lookin forward to the final release, but hey don't rush it your tempo is anything but slow anyways!"
    author: "DiCkE"
  - subject: "KWord still alpha?"
    date: 2001-05-23
    body: "I'm thrilled to see this release - it's great that Open Source is getting a really powerful and integrated office suite. The only thing that worries me is that the \"suite\" is at beta 2 and KWord, which to many people will be considered the flagship application, is still listed as \"alpha\". Not only that, but the release announcement doesn't list ANY changes since beta1 in KWord (just since 1.0, and as far as I could see this was the same list as beta1 had). Is KWord being worked on actively? Will it be \"release quality\" at the same time as the rest of the suite, or will we see something like \"although the suite as a whole is release quality, different components are at different levels of readiness [...] KWord (beta)\"?\n\nMostly I'm concerned because a high-quality word processor is my biggest office-suite need right now, and I'm sure I'm not alone in this.\n\nStuart."
    author: "Stuart"
  - subject: "Re: KWord still alpha?"
    date: 2001-05-23
    body: "\"the release announcement doesn't list ANY changes since beta1 in KWord\"\n\nYou misread!  All those changes listed in the release were NOT from KWord 1.0 but they were in fact changes from beta1!  Considering that beta1 wasn't very long ago, I'd say that KWord development is very active.\n\nI haven't been able to download it yet, but I'm looking forward to the GUI redesign - the screen always seemed cluttered when I was working in KWord."
    author: "not me"
  - subject: "Re: KWord still alpha?"
    date: 2001-05-23
    body: "I look at the developer list and cvs now and then and it seems like Kword is getting a lot of attention. They have been doing a lot of work rewriting the thing it seems... KWord was originally written by Mosfet almost as a programming exercise to learn C++ and had a lot of bad code."
    author: "john"
  - subject: "Re: KWord still alpha?"
    date: 2001-05-23
    body: "Not Mosfet, but Reggie."
    author: "gis"
  - subject: "Kivio won't start?"
    date: 2001-05-23
    body: "When starting the kivio that came with the mandrake rpm's, i get this (console) message:\n\nkoffice (lib kofficecore): ERROR: Couldn't find the native MimeType in kivio's desktop file. Check your installation !\n\n...and it doesn't start. what can i do?\n\n(Mandrake 8.0)"
    author: "Haakon Nilsen"
  - subject: "Re: Kivio won't start? - Same here"
    date: 2001-05-23
    body: "Same here, but i compiled from source, took bloody ages, about 4-5 hours on my k62 400 (maybe thats the reason ;)  )\n\nANy suggestion appreciated"
    author: "PiggZ"
  - subject: "Re: Kivio won't start?"
    date: 2001-05-24
    body: "Did you remove the standalone kivio package ? Now that kivio is part of KOffice, you have to remove the old one.\nIf this doesn't help, mail me and I'll send you a shellscript that checks for this problem."
    author: "David Faure"
  - subject: "Re: Kivio won't start?"
    date: 2001-05-25
    body: "I've run your script and the result is :\n\n----------------------------------------\nFound: /home/fabio/.kde/share/applnk-mdk/Office/kivio.desktop... ok, this is a KOfficePart\nNative mimetype :\n \nNow looking by filename\n \nLooking under applnk dir: /home/fabio/.kde/share/applnk-mdk/\nFound: /home/fabio/.kde/share/applnk-mdk/Office/kword.desktop... ok, this is a KOfficePart\nNative mimetype : X-KDE-NativeMimeType=application/x-kword\nLooking under applnk dir: /usr/share/applnk-mdk/\nFound: /usr/share/applnk-mdk/Office/kword.desktop... ok, this is a KOfficePart\nNative mimetype : X-KDE-NativeMimeType=application/x-kword\n***** Native mimetype not found !\n/home/fabio/.kde/share/applnk-mdk/ /usr/share/applnk-mdk/\n \nService Types dir /home/fabio/.kde/share/servicetypes/\nService Types dir /usr/share/servicetypes/\nFound /usr/share/servicetypes/kofficepart.desktop\n[PropertyDef::X-KDE-NativeMimeType]\n-----------------------------------------------\nI don't understand which mime-type is missing.\n\n--\nNothingman"
    author: "nothingman"
  - subject: "Re: Kivio won't start?"
    date: 2004-03-17
    body: "I had the same problem starting kivio. What I found somewhere else and solved my problem is the following:\n\nApparently the problem is that it is expecting some shared libraries to be on the folder applnk-mdk instead of the applnk alone. \nThe way i found what the path was for my case is with the command\n\nkde-config --path apps\n/home/avazquez/.kde/share/applnk-mdk/:/usr/share/applnk-mdk/\n\nWhat I did was to copy everything at the folder /usr/share/applnk to the /usr/share/applnk-mdk (I had to create it before)\n\n"
    author: "alevp"
  - subject: "try kbuildsycoca"
    date: 2001-05-25
    body: "I had this problem once...someone eventually suggested I run the program \"kbuildsycoca\".  It worked.  I have no idea if that will help (that was on KDE 2.0), and I'm not real clear on what that program does but you can try it.  \n\nJust run it as root.  I think it does something with a database of mime types.  Maybe someone who knows what they're talking about can clarify.  :-)\n\nJohn Dailey\ndailey@vt.edu"
    author: "John Dailey"
  - subject: "Re: try kbuildsycoca"
    date: 2006-06-05
    body: "\nMaybe this info can to help you:\n\nhttp://developer.kde.org/documentation/library/kdeqt/kde3arch/ksycoca.html\n\nkde uses a database called sycoca (System Configuration Cache) for looking up static system configuration information.\n\nkbuildsycoca creates/updates this database.\n\nnmt\nnumit_or@cantv.net\n"
    author: "nmt"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-24
    body: "In every version of koffice that I try, I can run kivio for the same Mime-type problem :((("
    author: "nothingman"
  - subject: "Re: KOffice-1.1 Beta2 Released !"
    date: 2001-05-25
    body: "A general observation:\nThe 1.1 Beta2 surely must be one of the most impressive software upgrades ever. \nThe 1.0 was TERRIBLE! Now, all of a sudden, ithe program is very stable, and you get a feeling of how it was conceived in the first place.\nAs a professional writer, see that this will be a thoroughly useable tool within the year, maybe with the official 1.1 release. It's awesome!"
    author: "G\u00f6ran Jartin"
---
A second beta of KOffice-1.1 is <a href="http://www.koffice.org/announcements/announce-1.1-beta2.phtml">now available</a>. The improvements since the first beta are considerable, particularly in KWord. If you are looking for a stable and full-featured word processor under Linux, give it a try! This release also features improved filter support throughout the suite including better MS Word and MS Excel support, and a new Quatro Pro filter. You can read the <a href="http://www.koffice.org/announcements/announce-1.1-beta2.phtml">full announcement</a> on the <a href="http://www.koffice.org/">KOffice web site</a>.
<!--break-->
