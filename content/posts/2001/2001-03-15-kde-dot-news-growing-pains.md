---
title: "KDE Dot News: Growing Pains"
date:    2001-03-15
authors:
  - "numanee"
slug:    kde-dot-news-growing-pains
comments:
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-14
    body: "Thank you. I appreciate you taking a strong stand on this kind of abuse. I can't tell you how angry I was last time I was reading the posts on slashdot. I had my 4 year old and 2 year old son's sitting with me and unfortunately I was browsing in windows at the time. Up popped a homosexual porno site, which was very explicit. I was not a happy camper. In fact, if I could have gotten my hands on the person who did that they'd have been beaten black and blue."
    author: "Sheldon Lee Wen"
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-15
    body: "FYI, the goatse.cx thing isn't a \"homosexual porno site\", but rather created by whomever for use exactly as it has been here - to troll.  I seriously doubt its author was even gay, as most of us find that image as irritating as you do, not only because it's not nice to look at but because it sparks anti-gay sentiment.  (My personal thought is always \"Will someone PLEASE light a match already.\")\n\nAt any rate, I consider this another reason Konqueror should include an easily accessible menu item, preferably with hot key, for enabling/disabling Javascript like Galeon has.  I prefer to surf most sites without Javascript turned on just to avoid this sort of crap, but need to turn it on for things like web banking and buy.com.  \n\nI'd like to see iframes easily disabled as well, but I've resigned myself to having to write patches for that.  Mozilla's \"block images from these domains\" feature would be nice to copy also, not just for this but to control banner ads as well.  Disclaimer: I have downloaded KDE 2.1 but haven't had time to install it yet, so maybe some of this stuff is already in there ;)"
    author: "raindog"
  - subject: "JavaScript and others"
    date: 2001-03-15
    body: "It's already possible to turn on/off javascript, java and cookies for specific sites. I agree a hotkey would be a good thing too, but only in addition. To turn off javascript, go to the konqueror-options, choose \"Konqueror-browser\" => JavaScript. Unset the option \"Enable JavaScript globally\" to disable JavaScript. In your situation, it might be a good idea to set some domains that may use it, like .buy.com. You can do the same for Java and cookies (in fact, even more for cookies).\n\nJonathan Brugge"
    author: "Jonathan Brugge"
  - subject: "Re: JavaScript and others"
    date: 2001-03-16
    body: "I know, that's what I do now.  I just don't understand why it's buried on the last page of a dialog under two levels of menus, which takes me 10-15 seconds to remember, find and set rather than a fraction of a second.  \n\nIt's not just Konqueror, though.  Netscape and IE do it too.  I always assumed it was their inherent ties to commercial interests (who most often abuse javascript, cookies, etc) that led them to make it complicated to disable.\n\nIf I do end up trying to write a patch, I'll probably also try to allow hotkey disabling of certain specific Javascript methods, like window.open (to disable just popups) or right-click onclicks (to avoid those \"You aren't allowed to view the source!\" messages which are completely meaningless when you use Konqi anyway.)  This is assuming I get over my OOPophobia and can even figure out where all that functionality is.\n\nRob"
    author: "raindog"
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-15
    body: "I think there is a plugin made for konqueror that allows you to quickly en-/disable javascript, graphics and other things via toolbar. You should take a look at the dot, it was on here somewhen."
    author: "ben"
  - subject: "Already rendered it unreadable"
    date: 2001-03-14
    body: "I own a company that does commercial sites that manage user content... I've seen TIFFs dumped into textboxes, gotten SQL commands thrown into my URLs, and even had someone try and upload a file that I believe was /dev/random!\n\nI wanted to try to execute some SQL commands in the forum, but it occured to me that you might be running the test forum on the same database the rest of the dot is running.  Also, some of the things that I thought of might bring down the dot, and I do NOT want that...\n\nPeople -- keep in mind that the point is to kill the test forum, NOT the server!!!\n\n(and give us back HTML encoding!!!  It *can* be done safely!!!)\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Already rendered it unreadable"
    date: 2001-03-14
    body: "Hmmm, yeah.  That was certainly not meant to be an invitation to disaster, but more to check that we had closed the javascript security holes.\n\nWe're working on bringing HTML back.\n\nThanks for your input!\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Already rendered it unreadable"
    date: 2001-03-14
    body: ">> but more to check that we had closed the javascript security holes.  We're working on bringing HTML back.\n\nKinda hard to check for javascript holes if HTML posting is disallowed.  For starters I'd limit the tags, and strip out all style=\"\" and on.*=\"\" strings.  Remember the classic  <p something=<script> dosomethingbad </script>trick... it shows up as a paragraph tag or a script tag to different browsers.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Already rendered it unreadable"
    date: 2001-03-14
    body: "The interesting thing is that the Javascript hack did NOT take advantage of our HTML encoding feature, but lack of checks on the Name/Email/Title fields.  They have been doing it on Gnotices as well, and that site does not support HTML encoding for articles.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Already rendered it unreadable"
    date: 2001-03-14
    body: "No need to hurry. plain text is good enough!"
    author: "reihal"
  - subject: "Re: Already rendered it unreadable"
    date: 2001-03-14
    body: "plain text is good enough!\n\nYes, but it makes links kinda hard to make, as well as limiting the ability to clearly delineate quotes versus replies.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Already rendered it unreadable"
    date: 2001-03-15
    body: "\"Yes, but it makes links kinda hard to make, as well as limiting the ability to clearly delineate quotes versus replies.\"\n\nI don't agree, just copy and paste. Plain Text rules!"
    author: "reihal"
  - subject: "Re: Already rendered it unreadable"
    date: 2001-03-15
    body: "i agree, copy paste is what i usually do anyway!!\ngreg"
    author: "greg l"
  - subject: "Re: Already rendered it unreadable"
    date: 2001-03-15
    body: "<A HREF=\"http://dot.kde.org\"><b>HTML <i>encoding</i></b></a> is not entirely disabled, actually.  Using the preview button allows you to select HTML encoding again.\n<br>\n<br>\nJust thought I'd warn you guys, although it doesn't seem like <b>HTML encoded</b> posts are really a problem (yet)."
    author: "not me"
  - subject: "Re: Already rendered it unreadable"
    date: 2001-03-15
    body: "Thank you!  I *was* trying to track that down.  I knew I had missed it.  It'll be fixed immediately.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Already rendered it unreadable"
    date: 2001-03-15
    body: "Wow, that was fast!  \n\nThe first time I did that, I thought it was just a problem with IE or something.  Was it my previous HTML post that was causing you to try and track down how HTML posts were still getting through?  I'm sorry!  At that time, I didn't know you had disabled HTML on purpose."
    author: "not me"
  - subject: "Re: Already rendered it unreadable"
    date: 2001-03-15
    body: "Yeah, I had noticed a post or two with HTML enabled.  I had actually enabled it for a couple of comments myself (to fix the formatting) but I couldn't find out how it had happened with the other posts.  It could have either been the other editors or a flaw somewhere.  I found no evidence in the logs that another editor had done it, so I pretty much guessed there was a loophole somewhere.\n\n:)\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Just get rid of them!"
    date: 2001-03-14
    body: "The best thing you can do is to yank offending posts as quickly as possible. (Probably the ones responding to them as well.) If the trolls never get to make any permanent mark they'll get bored and go away. That's why Slashdot is full of trolls and Kuro5hin has none.\n\nAs with a lot of these exploits, this javascript trick is pretty clever and requires more brains and knowledge than does a lot of programming. It's a shame the kiddies don't apply themselves to coding. They'd get more satisfaction out of it and get something they could put on their resumes. (\"Devised and implemented a way to obfustcate goatse.cx URL's in web forum posts\" doesn't look nearly as good to employers.)"
    author: "Otter"
  - subject: "Re: Just get rid of them!"
    date: 2001-03-15
    body: "Yep.  Nuke 'em good!  I believe the only reason Slashdot uses a moderation system is because it became too difficult to keep up with all of the trolls, simply because of the sheer number of posts.\n\nThe Dot receives a managable number of posts, so goatsex trolls will stand out like a sore thumb. It's plain obvious that these posts are not only offensive, but insanly off-topic.  It's unfortunate that we now have to deal with these types of posts, but I guess it's a sign that this forum has \"made it in life.\"\n\nLet's keep this place clean so we can focus on KDE.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Just get rid of them!"
    date: 2001-03-15
    body: "I say we take down that goatse.cx webpage\ninstead. I bet some good ol' naggin' from \nsome organizations can make congress or some\nauthority kill that webpage"
    author: "Francisco Leon"
  - subject: "Re: Just get rid of them!"
    date: 2001-03-15
    body: "Get rid of goatse.cx .. muwahahah. Are you fucken stupid or something??? Sure let's ban every web page that is slightly offensive. \"That'll lern 'em!\" .. don't get me wrong, that goatse.cx man is the most vile and disgusting picture I can think of; but that is still no excuse.\n\nCrawl back into your hole you fuckwit. Why don't we get rid of the whole internet, it does nothing but ruin families and help child pornographers trade their wares.\n\nASS.\n\n-- You run shitty code on your site, then expect every little faggot to come crawling out of the sewer to piss on it. I have no pity for what transpired here. --"
    author: "flikx"
  - subject: "Re: Just get rid of them!"
    date: 2001-03-15
    body: "Unfortunately, you start out correct but then you mix issues.  Freedom on the web has nothing to do with abusing another's website.  Your right to goatse does not mean you have the right to abuse dot.kde.org."
    author: "KDE User"
  - subject: "Re: Just get rid of them!"
    date: 2002-08-14
    body: "My website has fostered a global community that promotes clean air for the environment and healthy food for our children. "
    author: "The goatse.cx man"
  - subject: "Re: Just get rid of them!"
    date: 2001-03-15
    body: "There is no brilliance here.  Being a script kiddie means copying the exploits or observations of others, without any brainpower whatsoever."
    author: "KDE User"
  - subject: "Re: Just get rid of them!"
    date: 2003-01-16
    body: "whoever made that up sucks"
    author: "ac"
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-15
    body: "I'm glad you guys are keeping this place clean and useful (very). Thanks for all your work! I was wondering how banishment would work with so many people having dynamic IPs? Maybe a registration system is required? It would mean you need a new email address every time you registered, which would be a pain for the casual abuser."
    author: "kdeFan"
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-15
    body: "Anyone can force a new DHCP lease by just changing the date, and I think that there aren't many kids with static IPs."
    author: "robert"
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-15
    body: "well, and don't forget us students behind a big masquerading box, if one of us misbehaves, we are all punished with the ip system."
    author: "ajuin"
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-15
    body: "If you try to block out JavaScript, also remember to filter out this syntax:\n\n<TAG PARAMETER=\"&{alert('hey')};%\">"
    author: "Toastie"
  - subject: "Re: Filtering HTML"
    date: 2001-03-20
    body: "This will not work. Netscape, in their infinite wisdom, have introduced numerous ways to incorporate Javascript into a page, many of them undocumented (some of them unintended). Plus there are attacks that don't even require Javascript--cross site scripting attacks and redirect attacks come to mind. Hotmail have been trying to use a blacklist approach against HTML for years and they still haven't got all the holes closed up.\n\nThe *only* way to safely filter HTML is with a whitelist. Disallow everything, and then allow well-known tags with well-known parameters matching carefully designed regular expressions. Be especially careful about HREFs and %xx encodings.\n\nIf you won't take my word for it, read the numerous and exhaustive Bugtraq threads on this very topic."
    author: "Adam Rice"
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-15
    body: "What you want to do is simply to have a\n'paranoid' filter for the posts.\nBasically, you pick out all tags, delete\nall tags that don't occur in an 'allowed'\nlist.  Then for each tag that is allowed,\npick out the options (using a per-tag\nallowed list), throw away the rest.\nIn the case of links, you also filter\nthe 'ref=' etc. bits to use a protocol\nfrom an allowed list (i.e. http:// ftp://\nhttps:// and possibly a few others).\n\nThis allows you to pick out only the essential options for tags, and what you want to do is to reconstruct the message from the original by only taking text and allowed tags+etc."
    author: "John Allsup"
  - subject: "Gnotices too..."
    date: 2001-03-15
    body: "The same happened to Gnotices a while ago.\nMay the trolls get blasted by Lina Inverse!"
    author: "Kill the Trolls!!!!!!!"
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-15
    body: "just wondering, are you putting your fixes back in the original squishdot ?\n(this also applies to the threading enhancement add to the dot)"
    author: "Robbin Bonthond"
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-16
    body: "Good Q. \nAnd allso, please post the final \"product\" (allow/deny scripts/cfg-files) to the relevant sites when you guys are done filtering/baning/castrating all the possibleties that the kiddies have to make my one-and-only newspage crash! It's not because I'm interested in what will accually get done.... it's more all the soothing lime-lite that would shine on KDE and it's community ;)\n(did i just say the same as robin?)\n\nBTW!... If you are going to remove the linx... please remove \"(Check those URLs! Don't forget the http://!)\", etc from the posting form ;D"
    author: "kidcat"
  - subject: "anonymizer.com"
    date: 2001-03-15
    body: "As a start you could hurry to ban anonymizer.com and any other known service like it. People do have a freedom of speech, but if they have to hide their identity that badly then im sure the rest of us are better off not listening to them."
    author: "Troels"
  - subject: "Re: anonymizer.com"
    date: 2001-03-15
    body: "Ban anonimity?  Are you sure you're a Linux user? :>"
    author: "Gene Scott"
  - subject: "Re: anonymizer.com"
    date: 2001-03-15
    body: "You missed the point. Users of anonymizing services would be banned from *here*, not everywhere. Big difference."
    author: "David Johnson"
  - subject: "Re: anonymizer.com"
    date: 2001-03-16
    body: "see my other  posting"
    author: "migis"
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-16
    body: "Banishing people who are unwilling to write without anonymizer is the first step to kill these things.\n\nThis should be unnecessary to say, but i got the impression it was not.\n\nOFF TOPIC:\n\nI am really afraid that all the differences (paths, libs, packages) between debian, red hat, mandrake, suse will lead to no good. \nComercial companies have to spend a great efford to customize their software for different distributions. May be the costs for this are to high to engage in linux software development.\n( i read a posting from a free contractor relating this subject and framemaker for linux at www.deja.com/usenet at the tex or framemaker section)\n\nOFF TOPIC the second:\nWhat do you think about freeBSD ?\nI heard it is *better* than linux, but I was unable to install it without a handbook - so i have no personal experience.\nIf so would it be bette to spend all the effort to develop freeBSD rather than linux ?\nOr has this to do with gpl or lgpl ?\n\nNo trolling - just curious about opinions."
    author: "migis"
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-17
    body: "BSD uses a liscence that says you can take the source and do anything you want with, windows NT/2000 uses a bunch of BSD code.  The advantage of BSD is that is tends to run better under extremely heavy loads and is a tad more tested than linux, the problem with BSD is that it can be somewhat difficult to install drivers/install the OS compared to linux.  Also because linux has more hype there's better hardware support for it for closed spec hardware.  BSD tends to use open standards(as published by a standard organization), therefore BSD is a UNIX not a UNIX clone(this is probably one of linux's greatest weakness's, and one of it's strengths as well).  Another advantage with BSD is that the 1st offtopic comment dosen't apply to BSD(much).  Many benchmarks say that BSD performs better than linux, but alot say the reverse.  It realy comes down to the liscences, would you care if someone used the code you wrote and did not contribute back?"
    author: "robert"
  - subject: "Re: KDE Dot News: Growing Pains"
    date: 2001-03-20
    body: "No i would probably not. But on the other hand the discussion about the integration of real player into KDE and the search for an BSD like license\n(Peter Godmann: \".  If anyone knows of a\nBSD or equivalent -licensed implementation of a motif/kde/gnome drag &\ndrop library, I would love to hear about it.\")\nshows the demand for such a thing.\n\nI mean one way is to say the Developers are willing to keep everything open and free for linux - which is a good thing in my opinion. But on the other hand someone can say: \" let us try to make Linux a wide spread system with tons of software - most of them open source and free.\"\n\nFor this purpose BSD-license gives company the opportunity to contribute to linux and to make a profit by selling software.\n\nI am really interessted in this license-things and i would be glad to read about discussions between open source devellopers (BSD and LINUX).\n\nThese question plus the question about different packageformats (openpackage.org) and about differents paths (redhat vs suse for example) are IMHO main topics for the future devolpment for linux.\n\ncu\nmigis"
    author: "migis"
  - subject: "Squishdot 1.0.0 is Out!"
    date: 2001-03-22
    body: "<a href=\"http://squishdot.org/985283025/\">http://squishdot.org/985283025/</a>\n<p>\nWoohoo! :)"
    author: "Navindra Umanee"
  - subject: "Re: Squishdot 1.0.0 is Out!"
    date: 2003-04-03
    body: "it's up to 1.5 now, it's great."
    author: "waht"
---
As the Dot has been getting more and more successful, and more and more popular, we've been attracting script kiddies and trolls like flies.  As <a href="mailto:pour at kde.org">Dre</a> previously reported: <i>"On March 13, 2001, at 1:33 am EST, someone using the <A HREF="http://www.anonymizer.com/">anonymizer.com</A> service succeeded in putting malicious Javascript code into one of the posts.  While the code was relatively harmless -- it changed all links on the page to point to a shockingly disgusting porno site -- we feel this security lapse requires us to disable all posting until the problem in the <A HREF="http://www.zope.org/Members/chrisw/Squishdot">Squishdot</A> code is <a href="http://squishdot.org/985283025/?comment=DOT_LINK_ADDED_MAR_22_2001">solved</a>."</i> This news has been out for a while in all the wrong places (<a href="http://slashdot.org/comments.pl?sid=trolltalk&cid=&pid=0&startat=&threshold=-1&mode=thread&commentsort=1#6472">1</a>, <a href="http://www.geekizoid.com/articles/01/03/14/0923253.shtml">2</a>) and is attracting attention.  Read on to see what we're doing about it.










<!--break-->
<p>
Unfortunately for the kiddies and trolls, we care deeply about our site, and we're taking many measures to ensure that it remains a valuable resource to the KDE community. As of now, things should be mostly back to normal.  We've long reenabled posting and we've fixed the Squishdot Name/Email/Title security hole that the kids were exploiting -- Dre reports that there was failed attack attempt no less recently than this morning.  On top of this, we're working on some much more strict HTML restrictions and we're also implementing Dot banishments by IP/domain/other for severe cases.  
<p>
We will eventually have a proper policy in place for dealing with trolls and script kiddies, whether by article marking or deletion, IP banning, or other means.  We try to be fair.  For example, it's fair to talk about what's wrong with KDE, or what other environments have that KDE does not, as long as there is no obvious abuse. However, what we will not have here is Slashdot-type leniency, for the simple reason that we are not actually <a href="http://slashdot.org/">Slashdot</a>.
<p>
Have any suggestions?  Feel free to vent them.  If you want to test out potential hacks on an article, please test them in this <a href="http://dot.kde.org/984599392/">temporary forum</a>. I'd like to add that we hope to keep the <a href="http://dot.kde.org/search?subject=Administrivia&op=articles">Administrivia</a> to a minimum in the future. You can certainly look forward to much more interesting KDE news and developments on the Dot!  Look for some upcoming news on KuickShow...









