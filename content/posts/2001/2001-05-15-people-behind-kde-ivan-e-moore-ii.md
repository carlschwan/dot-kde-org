---
title: "People Behind KDE: Ivan E. Moore II"
date:    2001-05-15
authors:
  - "Inorog"
slug:    people-behind-kde-ivan-e-moore-ii
comments:
  - subject: "Re: People Behind KDE: Ivan E. Moore II"
    date: 2001-05-15
    body: "I don't use Debian, but I still think Ivan is a hero. I follow the CVS mailling list and I'm amazed by the amount of quality work he spends on KDE for Debian. The Debian folks are happy to have a guy like him."
    author: "Matt"
  - subject: "Re: People Behind KDE: Ivan E. Moore II"
    date: 2001-05-16
    body: "I agree, Ivan rocks.  He provides terrific KDE support for Debian.  This kind of contributor is the most valuable for KDE.\n\nRock on, Rev Krusty!"
    author: "ac"
  - subject: "Re: People Behind KDE: Ivan E. Moore II"
    date: 2001-05-21
    body: "I do complitelly agree, \nIvan makes just his work the best way it could be done.!\nSometimes i am suprised how kind and \ngood at his work he is."
    author: "Luigi Genoni"
  - subject: "Re: People Behind KDE: Ivan E. Moore II"
    date: 2001-05-15
    body: "\"How old would you be if you didn't know how old you is?\"\n\nThat was a good question - never heard that one before..."
    author: "john"
  - subject: "Re: People Behind KDE: Ivan E. Moore II"
    date: 2001-05-16
    body: "Ivan is a great guy, he has helped us out a few times getting Debian packages together.  I can't say enough good about him :)."
    author: "Shawn Gordon"
  - subject: "Debian packages?"
    date: 2001-05-16
    body: "Hi,\n\nI tried to apt-get install task-kde a litle bit ago, and there is no \"startkde\" program installed. all the other kde packages are installed, but just not that file as far as i can tell. so how to i initialise KDE from my .xinitrc script? when i build KDE from scratch, i just type startkde into my .xinitrc, but when i do this after apt-getting task-kde, the file is not found. I did a locate for it(after updatedb), and it is just not there.. do you know if this is a bug? or is there something special i need to do to get KDE to work by default?\n\nI heard someone say something about a program called \"usekde\" or something that will make kde be the default desktop, but i cannot find any such file.\n\nthanks for any help"
    author: "anonymous"
  - subject: "Re: Debian packages?"
    date: 2001-05-16
    body: "Try 'kde' instead of 'startkde'."
    author: "ac"
  - subject: "Re: Debian packages?"
    date: 2001-05-16
    body: "well, startkde is in the package kdebase. you could aswell put /usr/bin/kde2 into your .xinitrc\n\ntix64"
    author: "tix64"
  - subject: "Re: People Behind KDE: Ivan E. Moore II"
    date: 2001-05-16
    body: "hehe he sure is scared of his wife ;-) j/k\nthanks for all the work!"
    author: "beerman"
  - subject: "Re: People Behind KDE: Ivan E. Moore II"
    date: 2001-05-16
    body: "Thanks Ivan!\nI'm always enjoying the fast an easy\napt-get even in unstable."
    author: "kernel"
  - subject: "Re: People Behind KDE: Ivan E. Moore II"
    date: 2001-05-19
    body: "I am very thankful that someone saw the importance\nof bringing together KDE and Debian.\n\nTogether they are a potent force for Linux.\n\nThanks again to Ivan for doing this much needed\nwork."
    author: "Kris Kringle"
  - subject: "Re: People Behind KDE: Ivan E. Moore II"
    date: 2001-05-23
    body: "good for free software :)"
    author: "gnome 1.4 has it too"
---
Packaging is one of the activities in our project that requires much discipline, dedication and patience. In this week's <a href="http://www.kde.org/people/people.html">People Behind KDE</A>, <a href="mailto:tink@kde.org">Tink</a> interviews <a href="http://www.kde.org/people/ivan.html">Ivan E. Moore II</a>, who has held the office of Debian Packager for our project for a long time.
He maintains the principal KDE packages for the <A HREF="http://www.debian.org/">Debian</A> GNU/Linux distribution. <a href="http://www.kde.org/people/ivan.html">Come</a> and enjoy as Ivan entertains us with his answers.
<!--break-->
