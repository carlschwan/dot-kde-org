---
title: "People behind KDE: Michael H\u00e4ckel"
date:    2001-03-27
authors:
  - "Inorog"
slug:    people-behind-kde-michael-häckel
comments:
  - subject: "Re: People behind KDE: Michael H\u00e4ckel"
    date: 2001-03-27
    body: "You weren't responsible for that 1,000,000th second bug, were you? ;-)\n\nBut seriously, KMail works pretty fine. I use it at work and it's quite useable.\n\nThere are some stability/speed/memory concerns when loading very large messages or a very large set of messages from POP3 (cron outputs etc), as well as not being able to use Maildir format and I would like a feature alike KNode were a message stays unread for several seconds even though it is displayed so I can skim through new messages and still have them marked as unread.\n\nBut of course if these things *really* bothered me I would've filed a wishlist report or started to code. :-)\n\nKeep up the good work!"
    author: "Rob Kaper"
  - subject: "Re: People behind KDE: Michael H\u00e4ckel"
    date: 2001-03-27
    body: "1,000,000,000th"
    author: "not me"
  - subject: "Re: People behind KDE: Michael H\u00e4ckel"
    date: 2001-03-27
    body: "My only wish for KMail - which is otherwise great, is that it would remember my search paramaters. I generally want to search in _all mailboxes_ and through _all messages_. But it always wants to look in the inbox and subject heading..."
    author: "EKH"
  - subject: "Re: People behind KDE: Michael H\u00e4ckel"
    date: 2001-03-27
    body: "I have been wanting to use KMail, but need to access a Lotus Domino LDAP directory (address book). Is there any plans for LDAP support for KMail. (or is it in there and I can't find it?)\n\nThanks"
    author: "Mark Holbrook"
---
<a href="http://devel-home.kde.org/~kmail/">KMail</a> is a central tonality of the KDE harmony. Part of the team of developers who invest work and passion into this interesting project, <a href="http://www.kde.org/people/michaelh.html">Michael H&auml;ckel</a> hacks away, making great contributions to the KDE Project. Appropriately, <a href="mailto:tink@kde.org">Tink</a> has put up this latest <a href="http://www.kde.org/people/michaelh.html">interview</a> on the <a href="http://www.kde.org/people/people.html">People pages</a>. Dedicated contributers like Michael only help shine a bright light into KDE's future.



<!--break-->
