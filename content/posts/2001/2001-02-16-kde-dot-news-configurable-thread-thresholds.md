---
title: "KDE Dot News: Configurable Thread Thresholds"
date:    2001-02-16
authors:
  - "numanee"
slug:    kde-dot-news-configurable-thread-thresholds
comments:
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-16
    body: "Yoohooo!  Thanks, Navindra, I love it!"
    author: "Dre"
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-16
    body: "How small things can make the world a better place! Perfect!"
    author: "Rijka Wam"
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-16
    body: "Though it was said before:\nGreat Job, Thanks!.\n\nDD"
    author: "zapalotta"
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-16
    body: "Yeah, cool."
    author: "Anonymous Coward"
  - subject: "do-i-rule-or-what dept"
    date: 2001-02-16
    body: "<i>do-i-rule-or-what dept</i><p>You rule. This is the one thing that was lacking in this site. Thanks!!"
    author: "Otter"
  - subject: "More congrats."
    date: 2001-02-16
    body: "We will just keap adding them until the default fails.  It's not like you don't deserve thanks and cudos after all :)."
    author: "Forge"
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-17
    body: "this is so gay"
    author: "bandit"
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-17
    body: "Please don't pos offensive comments  on dot.kde.org. Although I am not gay, some of my friendsare and I find it offensive, as may gay people.\n\nIf you don't like something and wish to make a comment, please use an alternative sentence."
    author: "Jono Bacon"
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-17
    body: "Funny that you just *assume* \"gay\" was meant as an insult. :-)"
    author: "Neil Stevens"
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-17
    body: "I don't think the poster meant 'happy'.\n\nWhat other things could the poster have meant?"
    author: "Jono Bacon"
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-19
    body: "I guess the poster meant \"gay\" in its original meaning: colourful, joyful,... Besides, why would\n\"gay\" be offensive in any meaning?"
    author: "Uwe"
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-17
    body: "Hey, you troll, I bet you put that there so that you could get some idiot to flame you! Well, here's a tip, it's not gonna work! No-one is going to place any posts in response to your post that actually bring up no topic other then pointing out how futile you actions are, so you may as well give it up. Mwahahahaha, you cannot defeat me! Resistance is futi -um- -wait a sec- DOH!"
    author: "Example of what NOT to do in response to a troll"
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-17
    body: "I assume you are kidding...if not...well...nevermind. :-P"
    author: "Jono Bacon"
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-17
    body: "I'm just amazed that it wasn't already in Squishdot. Are you going to submit what u did to Squishdot? That'd be great.. Good Work.\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: KDE Dot News: Configurable Thread Thresholds"
    date: 2001-02-17
    body: "Hmmm, well not really.  Unfortunately, Squishdot is end of line and TNG version is Swishdot.  Nothing but bug fixes are going into the Squishdot line and all the new features are postponed/planned for Swishdot.  It's a bit of a pity, since there are so many ways that Squishdot could be improved *now* and that would help a great many of sites.  But I've been told that there'll be an upgrade path to Swishdot...  as long as it doesn't break the current links (tons of sites link to stories on  dot.kde.org so no sense in breaking those links).\n\nPlus, it'd be extra work to get our hack into shape for a generic public patch...\n\nCheers,\nNavin."
    author: "Navindra Umanee"
---
As some of you may have noticed, lately we've been getting more and more postings at <a href="http://dot.kde.org/">KDE Dot News</a>. With the increasing number of comments, we've also been getting more and more complaints about the fact that KDE Dot News automatically switches to threading when the number of articles in a forum goes over our global threshold. We've been trying to tweak this global threshold to satisfy everyone, but that seems to have been a lost cause:  Forums with too many nested articles take a while to download for people with slow net connections and aren't convenient for a bird's eye view of the responses, while forums which have switched to threading are painful for people who want to read each and every response.  As of now, the default threshold has been set to 40, but I'm pleased announce that I've also implemented configurable thread thresholds. Just look to the right of <a href="http://dot.kde.org/982317334/">this article</a> or <a href="http://dot.kde.org/972331966/">any other</a>, and you should see a more or less self-explicit config box -- your preferred settings should be stored more or less permanently in a cookie. If you find any bugs, or have any comments or feedback just use the <a href="http://dot.kde.org/982317334/">forum</a> or, failing that, <a href="mailto:navindra@kde.org">email me</a>.  Enjoy.






<!--break-->
