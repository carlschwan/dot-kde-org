---
title: "ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not Good Enough"
date:    2001-04-20
authors:
  - "Dre"
slug:    zdnet-compares-linux-desktops-concludes-kde-best-not-good-enough
comments:
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-19
    body: "Is there any plans for the KDE team for providing configuration tools for linux (like configuring display, network, sound, boot things like that...)\n\nThat KDE 2.1.1 is already a stable and full featured desktop, I think KDE project should slowly start providing stable and reliable configuration utilities for linux (which are badly needed by linux users...). It also would be great to have uniform looking tools across all distros of linux.\n\ncomments??????????"
    author: "mathi"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "These types of tools are problematic as distros vary so much. It creates a lot of additional work to do something for each distro, and often vary it for each point release... and then when all is said and done is it a good thing?\n\nPlease note programs like Webmin which runs from within konqueror very nicely, handles nearly anything you can imagine and works remotely via SSL. It's a pretty sweet package and it's basically a lot of Perl scripts. You might look at kaptain too as a quick development tool, and there are advantages in these ideas.\n\nHowever... perhaps you have not been looking at the recent beta? It has configuration for Lilo, printer configurations including extensive Cups support, a new kernel configuration tool as well as time and DPMS configuration... I'm probably missing more (oh yeah, user and group configuraation) and I have not looked extensively at some of the network configuration going in, not to mention the work being done on the new kdm...\n\nAdmittedly it would be nice to configure X and such in KDE... There's still time for you to learn how to work on the code I suspect though. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "I personally think that KDE development team has more expertise to put together a good desktop Linux distribution (or maybe I am just ticked-off that RedHat is not utilizing the full capabilities of KDE 2.1.1 in RedHat 7.1).\n\nI am aware of several things that can make KDE a great desktop. I have done this at home with a severely modified RedHat 6.2 setup:\n\n1. Get some real TrueType fonts. For some reason Type1 fonts do not display right, I think the Font Metrics is broken when anti-aliasing is enabled (the broken-up layout of the start page of the konqueror gives me this suspicion). When I used Win98 TrueType fonts, everything looked perfect. It took me a while to find some cyrillic TT fonts, but once those were installed I was able to read Russian news AND the fonts looked much better than they ever did in Netscape, AND it looked even better then under IE5.0 on a Win98 machine.\nIn short, KDE-2.1.1 + TTF = the best thing ever.\n\n2. Enable lisa service used by KDE-2.1.1 to enable Network Neighborhood style SMB host/share browsing. This is a system startup level service (similar to sshd/httpd/inetd), and a regular desktop user would not have a clue about how this should be installed/enabled. On a side note - lisas' documentation under KDE-2.1.1 sucks. I had to go to the sources to figure out what needs to be done to make it work.\n\nWhat bothers me, is that an industry leader like RedHat has missed these obvious (in my opinion) enhancements in their latest baby - RedHat 7.1.\n\nAfter using KDE-2.1.1 on my home-rolled RH6.2 installation, the experience with RH7.1 was disappointing even after enabling anti-aliasing.\n\nI may be paranoid, but it seems like RedHat is not giving all the attention KDE deserves. Maybe KDE could make their own Linux distribution, and enhance it along the way with things missing from RedHat 7.1 - ReiserFS, TTF, lisa, grub (a replacement for lilo), etc..."
    author: "Paul Koshevoy"
  - subject: "Fonts, lisa, and Mandrake"
    date: 2001-04-20
    body: "Fonts are a problem for Open source software.  There simply aren't very many really high-quality TrueType fonts out there that have the correct licences.  MS, of course, has the best fonts, but you can't just start redistributing their Windows fonts - that's a violation of their license.  One thing that can be done is you can download MS's freely available web font package.  However, you can't include it in a Linux distribution, because you can't redistribute the fonts.  Someone needs to go out and make some high-quality TrueType fonts and make them available to all the world!\n\nAbout lisa - how on earth *do* you configure it correctly?  I've had zero luck.\n\nI believe that KDE 2.2 will contain a totally new SMB implementation (based on a new SAMBA library) that will finally include proper SMB share browsing with no configuration.  Yay!\n\nThe distro you describe in your last paragraph sounds an awful lot like Mandrake.  Mandrake was started to add KDE to RedHat back before KDE was in RedHat.  Today its practically a whole new distribution (still mostly binary compatible with RedHat though).  Mandrake has a default KDE desktop, easy graphical install, easy graphical system admin tools, ReiserFS, grub, and more!  It even automatically imports your Windows TT fonts for use under X.  Mandrake 8.0 was released just today at www.linux-mandrake.com\n\nMy only gripes with Mandrake (I used to use it) are: I don't like their artistic style, they replace the nice K menu with their own using ugly icons and bad organization, and they use GTK for all their config tools instead of QT (can't figure that one out, since KDE is their main desktop).  Everything works great though, and you can just ignore the ugly icons and GTK widgets."
    author: "not me"
  - subject: "Re: Fonts, lisa, and Mandrake"
    date: 2001-04-20
    body: "If you have problems configuring lisa, contact me. Next week I will spent some time on improving the situation.\n\nneundorf@kde.org"
    author: "aleXXX"
  - subject: "Re: Fonts, lisa, and Mandrake"
    date: 2001-04-20
    body: "I can't use lisa because it uses portscanning and thats illegal here(campus network). I hope the libsmb kioslave will be ready soon (it will provide browsing too won't it ?)"
    author: "ik"
  - subject: "Re: Fonts, lisa, and Mandrake"
    date: 2001-04-20
    body: "How does what lisa does compare with what Windows does for seeing who's on the network?  I only know a little about how Windows does it and even less about lisa.  Oh, and I have not gotten lisa to work on my machine, but I had not put that much effort into it yet."
    author: "Jeff Smith"
  - subject: "Re: Fonts, lisa, and Mandrake"
    date: 2001-04-20
    body: "I think I have figured out part of the problem.  Am I right that in order to browse SMB shares, you must configure lisa to use nmblookup? (or do I just have the IP addres range wrong?  I can't figure it out from the README) Well, on a stock Debian system, the default is for only root to be able to run nmblookup.  That might explain why nothing is showing up in lan:/ except for localhost.  I'll have to look into changing that.\n\nBTW, the documentation and configuration for lisa definitely needs to be improved.  First of all, going to smb:/ should NOT bring up an error dialog!  If what the user really wants in this case is to go to lan:/, they should be automatically redirected, not given an error!  \n\nYou could probably deduce the correct network settings (broadcast address, etc for the most common usage case) automatically 99% of the time without even asking the user to set it up.  This needs work.  For the other 1%, you need to have more helpful help text.  The help you get from the \"help\" button reads something like:  \n\n\"For more information, look in the README file.  To read the full manual, click here.\"\n\nFirst of all, you are giving contradictory advice here (I know it's probably because of some kcontrol help template, but it needs to be fixed anyway!).  Second, clicking \"here\" has no effect, because there is no hyperlink there!  Third, this does not help the user FIND the README file (out of the hundreds on the computer), and it is not obvious where it is.  I had to run a search on my entire /usr directory to find it.  Fourth, once you do find the README file, it is of NO help if you don't already know a LOT about networking.  The README explains a lot about how lisa works, but it doesn't tell you basic usage information like how to determine what IP ranges are acceptable for your particular network.  It seems quite easy to configure lisa in a way that would either create lots of redundant network traffic or start ringing sysadmin pagers because they think they're being portscanned or something.\n\nSorry to rant, but lisa is simply way too hard to configure and use.  With Windows, you simply plug it in and it works (for 99% of the cases.  If you want something special, it requires some configuration).  lisa should be no different.  lisa sounds like a good idea, and I look forward to the improvements you plan to make.\n\nBTW, won't the KDE 2.2 smb ioslave be able to browse available SMB shares in a network-neighborhood way?  In that case, what will be the purpose of lisa?"
    author: "not me"
  - subject: "Re: Fonts, lisa, and Mandrake"
    date: 2001-04-21
    body: "Oops, I was wrong about nmblookup.  Apparently there's some other reason that lisa won't use nmblookup.  I have the \"use nmblookup\" box checked, but lisa won't list any SMB shares (or anything else, for that matter).  nmblookup works fine on the command line.  I know I don't have the IP addresses set correctly, but shouldn't lisa list the SMB shares using nmblookup anyway?  And how the heck DO you set the IP addresses correctly?"
    author: "not me"
  - subject: "Re: Fonts, lisa, and Mandrake"
    date: 2001-08-16
    body: "hi I installed Linux and I am confused how to set the LAN settings which ask me about LIsa configuring \n\nYour help would be valuable .My System is running as a part of windows 2000 .It's accessing the local host but unable to connect to Internet .\n\nThanx in ad. \n\nRegards bhishm"
    author: "bhishm sharma"
  - subject: "Re: Fonts, lisa, and Mandrake"
    date: 2001-11-07
    body: "I agree that the documentation for LISa isnt very helpfull to newby user. Has anyone had any success with this? If so could you post something that could clear up all of this newby confusion?"
    author: "John Powell"
  - subject: "LISa and KDE 2.1.1"
    date: 2001-04-20
    body: "I can see one simple improvement to the LISa configuration in KDE. It should not need one. Think about it, when you set up a linux box for networking, you specify which network the box is connected to. KDE LISa configuration tool then asks you to specify which network you want to browse. For most people, it's the same network that they have set their box up for. It seems that LISa configuration is confusing and redundant. When I was setting up LISa, I ended up giving it all the same data that can be extracted via `ifconfig -a` command. Maybe LISa configuration can be simplified by extracting the default settings from the results of the `ifconfig -a` command, instead of defaulting to 192.168.0.0/255.255.0.0 network settings?\n\n\nIn any case, here is how I got LISa working. \nI added the following commands to /etc/rc.d/rc.local file on my RH6.2 box. Your setup may differ, so adjust accordingly:\n\n  QTDIR=/usr/local/qt\n  QT_XFT=true # probably not necessary\n  KDEDIR=/usr/local/kde\n  export PATH=$PATH:$QTDIR/bin:$KDEDIR/bin\n  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QTDIR/lib:$KDEDIR/lib\n  $KDEDIR/bin/lisa --kde2 1>>/dev/null 2>>/dev/null\n\n\nLISa can be configured to use nmblookup, which is a part of the SAMBA package, so make sure that you have it installed if you configure LISa to use nmblookup."
    author: "Paul Koshevoy"
  - subject: "Re: LISa and KDE 2.1.1"
    date: 2001-09-21
    body: "this was very helpful. i had just installed RH 7.1 ang this was the first time i have setup LISa btw is it possible to mount the drives/directories using LISa?\n(some of our machine are windows boxes) and how to i do that?"
    author: "Avatar"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "If you think that RedHat is not integrating KDE well you could try a different distribution. SuSE integrates KDE quite nicely in their 7.1 distribution and I had no problems with Mandrake either (have not tested the latest 8.0, though)."
    author: "jj"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "Excuse me, but are you reviewing the same Mandrake 7.2 that I am? Mandrake 7.2 was the buggiest distribution that I have ever used.\n\nHave you tried:\n*Adding a menu item by not using the editor, but rather by adding the .desktop entry?\n*Installing anything but the default inst?\n*Re-running the X configurator?"
    author: "Carbon"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "I used Mandrake 7.2, but I did not do any of the things that you mentioned. I installed the standard, everything worked right away and I installed additional program by hand (but not from the distro).\nI have been running SuSE 7.1 for the last month and had no problems whatsoever. Maybe my hardware makes it easy (I bought a VA Linux machine), but I have had little problems with either distribution. And the configuration tools in SuSE 7.1 are superb."
    author: "jj"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "I really haven't tried SuSE myself, so I wouldn't know. I've heard YaST is good, and I keep meaning to check it out."
    author: "Carbon"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "Unfortunately Mandrake has adopted the Debian menu system to sync different desktops. The idea is that your menus will be the same under KDE, GNOME, WM, black box or whatever. What I find strange about this is loading those other desktops the menuing is NOT really the same on some. Anyway, I support diversity but have no time to play with other desktops and KDE works great!\n\nIf you write desktop files to /usr/share/applnk since mdk 7.1 the update-menus program will delete them. What I did was erase update-menus and /etc/menu-methods/kde. You still need kmenuedit and that's a pain and I hate what they did to the menus and kcontrol... so I built KDE from cvs in /opt/kde2. ;-) I now have both.\n\nAs to mdk 7.2 being buggy, the early release to get into Wallmart didn't help things. The re-release is pretty solid.\n\nBTW I always do an expert install with a lot of customizaton."
    author: "Eric Laffoon"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "Re-release. Hadn't heard about that, sounds like a decidedly MS style tactic. I would have expected Mandrake to at least change the version number so it's customers would know!\n\nI must be using the buggy version, since it always ignores my package choices in the installer (not to mention all the spelling and grammar errors in the installer help)"
    author: "Carbon"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "Though the nice thing about how the Debian packages of KDE are set up is that they use standard KDE menus for everything, then the shared Debian menu is a submenu off of the main \"K\" menu.  So, I have the convenience of having the KDE organization and also the same menu structure if I were to go to blackbox or something else."
    author: "Travis B. Hartwell"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "TT fonts take a person about 6 month to make a full set of latin only characters, there are almost no free TT fonts that you could include in a linux distribution.  There was talk during GUAEC II about getting some of the big companies that back GNOME to contribute some money towards fonts.  Fonts are an X thing so KDE can use those fonts as well."
    author: "robert"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "As far as I know such tools are part of Mandrake Linux. I don't know though how easy it would be to include them with other distributions.\n\n:-|"
    author: "Stephan"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "I've actually got a configuration program that is based on KDE in early planning stages at SF. http://sourceforge.net/projects/kisaw</a>"
    author: "Steve Hunt"
  - subject: "System Management tools."
    date: 2001-04-20
    body: "How are you planning to do this?  \n\nI had given some thought to this myself.  For most implementations of Linux/Unix, the raw information needed to configure a 'feature' is portable across platforms.\n\nFor example, the network config needs interface names, and the interfaces need an IP address, network mask, broadcast address, and optional extras like MTU sizes are usually common too. The same goes for network routes, NFS, NTP, basic user accounts, groups, Mail setup, aliases, etc, etc.\n\nSo to provide the most use, a GUI front end could be a data collector for all this kind of raw information, perhaps populating a simple database that callable standalone scripts or programs could query to do the REAL config work.  \n\nA side benefit of taking this approach would be that someone could write a char-cell front end to the same database and scripts if they wanted to do system management over remote dial-up, or on headless systems.   \n\nThe real heart of a tool like this would be the data repository and the API used to populate it and read from it.  Writing the human interfaces would be something to do later and should be platform/toolkit agnostic.\n\nHmmm ...\n\nJohn"
    author: "John"
  - subject: "Re: System Management tools."
    date: 2001-04-20
    body: "I definetly agree. One of the biggest problems with MacOS and Windows is that the GUI is too tightly bound to the kernel. Since KDE must be seperated from the kernel by X anyways, providing a front-end is the perfect solution."
    author: "Carbon"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "As others pointed out, it would involve much work for supporting the multitude of distributions.\n\nBut I believe it is still a good idea.\n\nWhat KDE can do is provide a uniform modular front-end interface for configuring different things.  Then it would be up to the distribution maintainer to do the distribution-specific work in the back-end.\n\nFor example the interface in the control center would provide the option to set network parameters.  By pressing apply, a script is executed with all the parameters available.  The script must be written by the distribution maintainer.\n\nEveryone should be happy:\n\n- Users have a standard interface on all distros.\n- KDE team does not need to cope with each distro.\n- Distro providers have an easy to use, intergrated configuration interface."
    author: "Alexandros Karypidis"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "Good idea!\n\nI was actually thinking about the issue of distro-specific config tools and especially how could KDE ensure (enforce?) a predictable and consistent user experience while letting the distro vendors distinguish themselves from others?\n\nI do beleive that it must confuse more than it helps to have two configuration tools (...or \"place to do config chores\"?), each distro adding its own tool besides the KDE Control Centre. For newbies and other non-Linux folks, it does not make any sense at all. Methinks that might be where *nix looses points vs. (say) MSWindows or MacOS in UI comparisons.\n\nSo are you thinking a a different thumbnail/pane in the Control Centre to perform distro-specific chores? Or are you thinking of blending those with the rest?\n\nI think this should be seriously looked into. In fact, it should even be the main objective of KDE 2.3, IMO, as it would tidy up one of the last few rough edges of the *nix User Experience."
    author: "Bruno Majewski"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "Sounds like a pretty good idea to me...and is the way that good GUI apps are written. ie. split the UI from the functionality. Passing an XML message from the UI to whatever configuration script you like (provided by the distros or anyone else) would make it extensible, maintainable...are easy.\n\nRich"
    author: "Richard Sheldon"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "Please don't flame me f\u00f6r this, but I believe that is excactly what ximian setup tools are trying to do (http://www.ximian.com/tech/helix-setup-tools.php3). I haven't looked closely at it, but I belive the backend are written in python and it uses xml as \"registry\". I wonder if a kde front end would be possible? I have no idea if the tools introduce to many dependcies to the GNOME libriries, but it could be worth checking out.\nAt least the backends should not have any dependcies if they are reasonably designed (well  to python or perl, but...). This could be a great oppurtunity for co-operation between the two camps. They could at least share backends and xml file fromat...\nAnd a nother gripe would probably be the name, but that can change...\n/J\u00f6rgen"
    author: "J\u00f6rgen Lundberg"
  - subject: "is there anyone from kde.org reading this?"
    date: 2001-04-21
    body: "Like the title says: are there any KDE developpers reading this?  People like David Faure, these Mosfet or Jono guys, like, one of the \"KDE Heavies\"? This is important stuff!"
    author: "Anonymous Coward"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "I run KDE on a Linux system, and on a FreeBSD system.  Is that really fair?  Everybody seems to be neglecting the other OSes now, and it's like that at all of the projects that used to work on several OSes... KDE has been doing a pretty good job, though."
    author: "dingodonkey"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "I think Linux users should first learn now to configure their desktop by hand and not by using config Tools. Isn't it enough that our best friend \"Bill\" fills up operating systems with these kind of tools which then....never work really good and produce strange not understandable errors, crash the system and so on....????"
    author: "Dipl.-Ing.M.Schulze"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-23
    body: "that's what i did,i'm using an extensively modified Slack 7.1 (kernel 2.4.3,Glibc 2.1.3 recompiled using kernel's 2.4.3 headers,gcc 2.95.3,latest binutils from HJL,it's not the GNU version,Xfree 4.0.3,etc...) but the only grippe i have is setting up GhostScript 7.00,gotta look if KDE's printing system will solve that one out.\n\nAlain"
    author: "Alain Toussaint"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "No, no, no!\n\nKDE is a desktop for *all* unices, not just Linux. All of the OS-specific stuff should be isolated in an appropriate package, like kdelinux, kdebsd, kdesolaris, etc.\n\nWhat good would all this stuff do under FreeBSD or IRIX?"
    author: "David Johnson"
  - subject: "Re: ZDNet Compares Linux Desktops: Try easyLinux"
    date: 2001-04-20
    body: "Try easyLinux, it has all the bells and whistles to provide these easy configuratuion tools.\n\nInfo: http://www.easylinux.com or visit http://www.igelusa.com for information"
    author: "Fall Tyler"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-22
    body: "Ximian is developing some setup tools. The backend and the frontend are separate programs. How hard could it be to write a KDE frontend?"
    author: "Anonymous"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-19
    body: "I think it is not easy to say which one is better, GNOME or KDE. It is just that I personally feel more confident to use KDE, simply because it uses much better underlying GUI library (QT). QT is written in C++, it is object oriented and it is very very well designed IMHO. GTK on the other hand tries to be OO, but is looks to me nothing more than C-ish hack for the OO. I don't want to discuss the look, or copy-paste features, since they are part of personal preferences. KDE simply has better GUI library and this is a good reason to use KDE for me."
    author: "Bojan"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "Be careful not to take Object Oriented languages for Object Orientation. The GTK+ library IS Object Oriented, but it is written in a programming language (C) that doesn't support OOP. Many of the things that GTK+ must do at run-time (e.g. type-checking), are done automagically at compile time by the C++ compiler."
    author: "valkadesh"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "That's nonsense. GTK+ might be C and the coding style looks like C, but it is fully object oriented in every definition of it.\nC++ is also a hack; it's just a hack that you can't see because the C++ compiler does it for you.\nEven if GTK+ is really a hack, who cares?\nIt works fine.\n\nAnd what has GTK+'s underlying technologie has to do with how good it works?\nA button is a button, no matter in what language/toolkit you write it."
    author: "AC"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "Interesting... I was not aware you could do OOP in C. How does one do inheritance? Public vs private access? What about virtual functions? If the programmer has to keep track of it, I would consider it a hack."
    author: "ac"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-21
    body: "inheritance is done by making the super-class a member of a struct.  Public vrs private is done by prefixing something with '_', virtual functions are done with function pointers, when you make a new Gtk widget you make a call like this\n\nGtkWidget *my_widget = gtk_subclass_new();\n\nthis takes this function pointers in my_widget and assigns them to the corrent functions, then when you call a function you do some thing like this\n\ngtk_subclass_foo(my_widget, i);\n\nthen definition of gtk_subclass_foo might look like \n\ngtk_subclass_foo(GtkWidget *widget, int i)\n{\n  (*widget.foo)(widget, i);\n}\n\nwhile this might look kind of of confusing you dont have to keep track of this yourself, it's done in Gtk, there's even a base class GtkObject that helps the people hacking the Gtk source from doing this stuff themselves, all your code looks like is\n\nGtkWidget my_widget = gtk_subclass_new();\ngtk_subclass_foo(my_widget, i);\n\ncompared to the C++ bindings you have to do\n\nGtk::Widget *my_widget = new Gtk::Subclass();\nmy_widget.foo(i);"
    author: "ac"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-22
    body: "Even if someone gets used to the GTK+ style of something that looks like OOP, you still have many disadvantages just because it uses C. For example,\nyou can't have two functions with same name, but\ndifferent arguments, you can't define your own operators between user defined objects, you can't have default values for function parameters, etc, etc...\nPlus, it is well known fact that C++ is much more adapted for large programs than C."
    author: "Bojan"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-22
    body: "There's nothing wrong with C, most realy big, system intensive projects are written with C, look at all UNIX kernels, or big clustering systems, or a GUI like photon.  When it comes to apps that 'you' write you can use the Gtk C++ bindings, or you can you can even use a simpler OO language like object pascal(good language for large projects).  If you dont trust C then I hope your not using UNIX, because about 90% of the stuff your using is written in C."
    author: "robert"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-22
    body: "I never said there was something wrong with C. I use it myself. OS kernels are special programms which need to be optimized for speed as much as possible. This means, that they are better to be written in C (some critical parts even in assembler), than in C++, since speed of execution is the only category where C is superior to C++. Most of the UNIX stuff is written in C, because it was written way before C++ was invented.\nYou can always write a large application in C (you can even do it in assembler, trust me), but it is easier to use an OO language. If you don't believe me, go to the nearest software company and ask them what language are they writing their applications in. I doubt they will say 'C' (here I mean strictly 'C', not 'C++'). And I never said that I don't trust C, so please read my postings carefully before blaming me for some stupidity I never claimed."
    author: "Bojan"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-22
    body: "I never said there was something wrong with C. I use it myself. OS kernels are special programms which need to be optimized for speed as much as possible. This means, that they are better to be written in C (some critical parts even in assembler), than in C++, since speed of execution is the only category where C is superior to C++. Most of the UNIX stuff is written in C, because it was written way before C++ was invented.\nYou can always write a large application in C (you can even do it in assembler, trust me), but it is easier to use an OO language. If you don't believe me, go to the nearest software company and ask them what language are they writing their applications in. I doubt they will say 'C' (here I mean strictly 'C', not 'C++'). And I never said that I don't trust C, so please read my postings carefully before blaming me for some stupidity I never claimed."
    author: "Bojan"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-21
    body: "The GTK+ Object System can do that all. GTK+ 2.0 can even do interfaces like in Java.\nLook at the GTK+ source code and you'll know it."
    author: "AC"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "I did say nothing about how good GTK+ works. It might even work better than QT, I just said that personally, I am more confident to use KDE, because I know it runs on library that is OO and written in C++. If you consider C++ just a hack, than probably every programming language is just a hack too. Even if GTK+ allows you some sort of inheritance (but only once, you can't inherit two different structures, yes, because its a hack), maybe easy creation of new widgets, it is far from being OO."
    author: "Bojan"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-21
    body: "Wrong. GTK+ is fully OO.\nIt can do inheritance, virtual functions, public/private members, type checking, bla bla bla.\nIf you just take a look at the GTK+ source code, you'll know that!\n\nAnd about inherit two different structures:\nObject Pascal/Delphi can't do that either, but dispite that it's still a very good language used by many people and companies.\nGTK+ doesn't support multiple inheritance, but it will support interfaces like in Java in the upcoming GTK+ 2.0."
    author: "AC"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-23
    body: "Most newer OO languages don't support multiple inheritance because it's considered a design flaw."
    author: "AC"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "maybe so, but the lack of expressiveness for OO would make things more difficult to do and understand. say, how do you think a \"chain of responsability\", or a \"decorator\" look in the gtk+ style of oo? it's doable, but you're missing the point. one could do oo in fortran if you were willing to type a lot, which gtk+ users certainly have to do.\nbtw, it sounds really stupid to say gtk+ is oo in every definition of it? where the hell is that definition?"
    author: "ac"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-21
    body: "I'm talking about the definition of Object Orientation!"
    author: "AC"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "Umm, and what exactly is _the_ definition of object orientation?\n\n\"I invented the term 'object oriented', and I can tell you I did not have C++ in mind.\" - Alan Kay\n\n /mill"
    author: "mill"
  - subject: "Qt vs Gtk"
    date: 2001-04-21
    body: "The only true language is assembler. Each other one is just only a hack ( compiler does it for you ). On the other side QT is horribly expensive ( if you earn 350$ ), Signals/slots neither efficient nor type safe, has no low-level graphics layer. It is rather  a Windows toolkit ported to X11 ( and Gtk is X11 toolkit ported to Windows ). It is not developed by open-source community - Sun and HP will never want to become dependent on the other company - so they never put QT/KDE on ther Unices as a default desktop. If Gtk was a good OO project written in C++ everybody would use it instead of QT. But it is not - in fact it is almost the same as Motif/CDE ( C++ in C )."
    author: "Hi"
  - subject: "Re: Qt vs Gtk"
    date: 2001-04-21
    body: "I wouldn't say Qt is too expensive.  Most software companies can pay ~$1500 without batting an eye.  It's a quality toolkit, and nothing even comes close for crossplatform Windows/X11 programming.  While Gtk has a hackjob win32 port, Qt has worked under Windows for as long as I can remember, and it is stable.  Qt is professional, and is the kind of toolkit that people pay for.  Trolltech must be doing pretty well too.  They show no sign of slowdown.\n\nI don't see why Sun and HP would not be interested in Qt.  There are many large companies using it.  And it's very common for companies to be dependent on other companies.\n\nBtw was \"a Windows toolkit ported to X11\" a figure of speech?  I thought the original history of Qt is it started on X11.\n\nLastly, I must say that KDE's success is largely due to Qt.  The GNOME folks may have Eazel developing applications, but the KDE folk have Trolltech making their base toolkit.  This is much more beneficial, and it's this solid foundation that has made KDE development so smooth.  Also, Trolltech doesn't exist for KDE.  They have an agenda of their own for supporting their paying customers.  And KDE reaps the benefits.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Qt vs Gtk"
    date: 2001-04-21
    body: "Yes, I know that we can say that all money they get they invest in the GPL community, but I would rather pay 200$, but not 2000$. What if I write simple applications for internal use in my firm - I must buy a commercial Qt license for each workplace. US/Western companies may affort to do this, but other probably not."
    author: "Hi"
  - subject: "Re: Qt vs Gtk"
    date: 2001-04-21
    body: "No, you don't have to. You just have to stick GPL license to it and you are off the hook.\n\nYou might not make any money from it, but since it was an application for internal use, you probably don't mind.\n\nHowever, if you did want to sell it and that 2000$ are what prevents it to be profitable (even if we forget for a moment the money you save with reducing development time), it's not worth doing it anyway.\n\nBtw, Sun and HP have absolutely no problem paying money for GUI toolkit. They used and still use Motif and that wasn't free either. I used to work for a firm, that developed some of HP products for them and speaking from personal experience, the only reason for spending 10000$ on some crappy NT-to-Unix GUI port library instead of Qt was an idiot manager on our side. HP liked the idea of Qt more."
    author: "Marko Samastur"
  - subject: "Re: Qt vs Gtk"
    date: 2001-04-21
    body: "Motif is an open standard, and Qt is owned by a single ( small ) company. What will they do if Gates with friends buy TrollTech ?"
    author: "Hi"
  - subject: "Re: Qt vs Gtk"
    date: 2001-04-21
    body: "Look at the page on the Trolltech site about the Free QT Foundation - \nhttp://www.trolltech.com/company/announce/foundation.html\n\nIn particular - 'Should Trolltech ever discontinue the Qt Free Edition for any reason including, but not limited to, a buyout of Trolltech, a merger or bankruptcy, the latest version of the Qt Free Edition will be released under the BSD license.'"
    author: "Jon"
  - subject: "Re: Qt vs Gtk"
    date: 2001-04-22
    body: "Jon already answered the second part. The first part...Well, Motif wasn't always an open standard. You had to pay, if you wanted to use it (that's why it never become popular in Linux). As I've said before, companies including Sun and HP never had problem with that."
    author: "Marko Samastur"
  - subject: "Re: Qt vs Gtk"
    date: 2001-04-21
    body: "Yes, I know that we can say that all money they get they invest in the GPL community, but I would rather pay 200$, but not 2000$. What if I write simple applications for internal use in my firm - I must buy a commercial Qt license for each workplace. US/Western companies may affort to do this, but other probably not."
    author: "Hi"
  - subject: "Re: Qt vs Gtk"
    date: 2001-04-22
    body: "I thought the commercial license fee only applied to commercial development, not commercial run-time.  Qt run-time is free everywhere isn't it?"
    author: "Macka"
  - subject: "Re: Qt vs Gtk"
    date: 2001-04-22
    body: "Of course, I meant 'coder's workplace'."
    author: "Hi"
  - subject: "Re: Qt vs Gtk"
    date: 2008-01-08
    body: "As was mentioned in previous posts, if it's internal stuff that you are not reselling or anything, you can just use GPL on it! No costs at all."
    author: "Justin"
  - subject: "GTK+ *is* fully OO!"
    date: 2001-04-21
    body: "Yes, unlike what most people think, GTK+ is fully object oriented, dispite that it's written in C.\nGTK+ supports virtual functions, public/private members, inheritance, whatever.\nGTK+ doesn't support multiple inheritances YET, but it will in the upcoming GTK+ 2.0, with interfaces like in Java.\n\n\n\"GTK+ is a hack\"\n\nJust what exactly is your definition of \"hack\"?\nDoes low-level code work worse than high-level code?\nI don't think so: assembly works fine.\n\n\nAnd if you guys still don't believe me, then I invite you to join the <A HREF=\"http://www.gtk.org/mailinglists.html\">GTK+ mailing list</A>.\nAsk the other developers how GTK+ is OO.\n\nAnd, I also strongly suggest you to take a look at the GTK+ or Gnome-libs source code.\nIt's the proof that GTK+ *is* OO.\n\n\nNo, don't respond yet.\nTake a look at the source code or ask the mailing list first, before making any prejudgements."
    author: "Gnome Developer"
  - subject: "I agree"
    date: 2001-04-22
    body: "'nuff said"
    author: "ac"
  - subject: "Re: GTK+ *is* fully OO!"
    date: 2003-01-16
    body: "as you say gtk+ is fully OO based\nand if not agreed that see the souce code of lib\nplease tell how to get the source of gtk+ liberaries\nif you tell me it will be a great help for me\n\nthanking you \nabhishrk kapoor\n"
    author: "abhishek kapoor"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-19
    body: "Just to be fair, GNOME 1.4 is kind of a stepping stone to GNOME 2.0, the difference between the two is musch like KDE 1.x and KDE 2.x.  GNOME 1.4 includes alot of 1.0 stuff that has not realy matured like Bonobo(Component Model), Natilus (MS-explorer clone), GNOME-VFS(like KIO-slaves) and more.  I would recomend GNOME 1.2 or KDE to non-developers. Developers should pick GNOME 1.4 or KDE 2.x."
    author: "AC"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-21
    body: "Just to be fair, KDE 2.x is kind of a stepping stone to KDE 3.0."
    author: "Nono No"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-21
    body: "What is included with KDE 2.x just to help developers write for KDE 3.x?"
    author: "AC"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-19
    body: "not worth reading - like most ZDNet articles."
    author: "Evandro"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "So very, very true. ZDNet has periodically presented misinformation. Almost every article that is OSS related by them contains numerous errors, and is responded to mostly by people saying \"Linux sucks, (Mac / Windows / DOS / WebTV / Microwavable Popcorn) doesn't, so there!\"."
    author: "Carbon"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "ZDNet is funded by Microsoft advertisements.  They might as well be a subsidiary."
    author: "DJ"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "I've read the article, and IMHO makes some good, if old, points. Trouble is, many of those 'drawbacks' are questions of interoperability among desktop environments --like drag'n drop and copy'n paste-- or even worse, of the very internal organization of Linux and thus, in good faith, lie outside the scope of the KDE project. However, I would like to see them solved.\nbtw, kudos and more kudos to the KDE people for the BEST of the BEST among ANY desktop environment I've ever seen in all aspects. KDE is stable, efficient, and beautiful."
    author: "sombragris"
  - subject: "How to compare?"
    date: 2001-04-20
    body: "<quote>However, it is important to note that KDE 2.1.1 has had the benefit of two bug-fixing updates since its major 2.0 update;we expect GNOME to make substantial gains between now and its own 2.0 release, slated for the end of this year.</quote>\n\nDoes this make sense to anyone else? Gnome has had several bug-fix updates since its last major release - if anything, it's had three or four times as long to settle down since the x.0 release. And given that even GTK 2.0 isn't finished, or even firmed up (correct me if I'm wrong), Gnome 2.0 is well on the horizon.\n\nAnyway, who cares? It seems to me the only relevant (or at least answerable) question is how they compare today."
    author: "Otter"
  - subject: "Re: How to compare?"
    date: 2001-04-20
    body: "You are right. By the time Gnome 2.0 comes out there will be KDE 2.3.1 (or something like that). They always forget that in the reviews and make it sound like KDE will undergo no development till then."
    author: "jj"
  - subject: "Re: How to compare?"
    date: 2001-04-20
    body: "When reading a review like this you have to realize that little of what they say is going to be solely factual and reasoned.  The intention behind the statement isn't bad; so it's not really worth mentioning.  They just want to state that GNOME has potential.  It would be mean to discredit GNOME completely."
    author: "Ghassan Misherghi"
  - subject: "troll"
    date: 2001-04-20
    body: "I'd like to say kudos to the people at Microsoft for once again having the better technolodgy.  You have worked very hard on your desktop and I'm enjoying it very much, I'm using IE on windows right now, and having no problems with cut-and-paste or weird inconsistices.  \n\nNote to the writers: You had barely any cover of windows, all you said was that it was better, the whole article itself was kind of fluffy and seemed kind of UNIX centric.\n"
    author: "nobody"
  - subject: "Re: troll"
    date: 2001-04-20
    body: "I totally agree. After all, what chance have they against user-friendly features like helpful MS Office registration screens (complete with a service to send the user helpful information from our associates), and context based talking paperclips.\n\n<antiflamebait>BTW, if you cannot tell, both this article and the article I am replying to are very much done tongue-in-cheek.</antiflamebait>"
    author: "Carbon"
  - subject: "Re: troll"
    date: 2001-04-21
    body: "I agree that at this moment Microsoft has a better desktop environment.  But I think that Alernative OSes are fast catching up.  OS X, is on its heels, KDE 2.1.1 is not too far behind.  GNOME is a wonderful environment but it seems to be somewhat slightly less professional to KDE at the moment.  I am looking forward to GNOME 2.0\n\nIf Windows is your kettle of fish.  Fine.  Others rather work with alternatives.  It is never proper for one company to have a virtual monopoly in any area.  Competition is DAMN GOOD.\n\nGO KDE TEAM!!\n\nAll u rough.\n\nKeep up the good work."
    author: "puelly"
  - subject: "Re: troll"
    date: 2001-04-23
    body: "In case you didn't notice, the whole article was about Unix desktops.  By the way, seen any blue screens lately?"
    author: "nimrod877"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "Hello\n\nI use the latest kde desktop (211) and i think it is the best, but wouldn't say it is not good enough. I would say it is not good at all. The not-speed of kde is driving me nuts, and then all applications, as it is usual in linux, crash most of the time. I say I am a linux fan and I hate windows, but nobody should really say linux is better than windows. Linux gets more and more like windows. The new kernel is unstable, the kde 2.0 (so called release version) was more then unstable. And nearly every softwareproduct for linux (expect tex and emacs) that is called release is in my opinion early beta.\n\ncu Robert"
    author: "Robert Ulmer"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "This is really strange. I'm using a 2.4 Kernel, XFree 4.01 and a KDE 2.2 Alpha (!) that I downloaded from CVS 2 days ago. Noting crashes. But I've compiled my whole system from ground, maybe that's the reason it works so nicely. A few days ago I tried to install SuSE 7.1 -- a nightmare. \nThat might the real problem of Linux: We're struggling with different file systems, sometimes completety incompatible libraries (RedHat), buggy distros (didn't Mandrake ship a Beta-Version of KDE in the US last chistmas?) and mega distros that have thousands of programs that nobody ever uses -- quantity instead of quality. You should have a look at the basic distros: Linuxfromscratch, Slackware, Debian. They have the quality you're looking for."
    author: "Johann Lermer"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "you should get your facts right.\n\ngcc-2.96-RH generates 100% compatible code for C programs. you can compile a C program with it and use in any system compatible with glibc-2.2.\n\nto use c++ code all you need to do is install libstdc++-2.96-81.i386.rpm from redhat. the c++ libraries are needed, since there's no compatibility between egcs 1.1.2, gcc 2.95, gcc-2.96-RH and gcc-3.0 (cvs branch).\n\nabout the nightmare you had a few days ago, you seem to be alone. i've never heard of problems with suse 7.1 until now (you). since you're so clear about it and gave descriptions to all the problems you encounterd, it's safe to believe in you.\n\nand mandrake did not ship with a KDE beta. their 7.2 release comes with 2.0-final, but they did make a few boxes with kde 2.0-rc (not beta, release candidate) for wall-mart. and they provided update cds for all the costumers who bought that version (and the box clearly said: KDE 2.0-RELEASE_CANDIDATE)."
    author: "Evandro"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "don't even talk about how gcc-2.96-RH generates 100% compatible code. the point is that the version that ships with RH7 is buggy. you can't even compile your own kernel with it. if it works, great. if it's buggy, forget about it. it doesn't matter how standards-compliant it is if it doesn't work right. as far as SuSE 7.1 goes, i recently installed it. when i tried to install it with the development package, the \"all of kde\" package, and the \"kde 2\" package, my graphics card crashed (i think) at the end of the install. during the install i could only configure x to run in 640x480 mode. nothing else would work. later i tried fixing this with sax2. nope. blank screen. and i couldn't get out of it with ctrl+alt+backspace so i had to reset my comp and run e2fsck. when my system recovered from that, i ran sax1 and it autodetected my graphics card (s3 virge/DX. only runs with generic SVGA server). i found my monitor in the monitor database, so that was easy. what i wanna know is why are there so many problems if it detected my card and had the monitor in the database? why didn't sax2 work when the graphical install did? why can't you select your monitor during the install to abvoid all these post-install configuration problems? why did my computer crash at the end of an attempted install? SuSE is a good distro, but they should take some pointers from corel without sacrificing any flexibility."
    author: "Rick Kreikebaum"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "Gcc 2.96rh is less buggy than 2.95, try something like comparison of arithmathic of floating point literals and variables, gcc itself is buggy, but 2.96rh is nothing but some bug fixes, the linux kernel has some bad stuff like forgeting to cast to const and stuff that gcc 2.5 lets you get away with, theres a kernel patch to make it compile with gcc 2.96."
    author: "ac"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "don't even talk about how gcc-2.96-RH generates 100% compatible code. the point is that the version that ships with RH7 is buggy. you can't even compile your own kernel with it. if it works, great. if it's buggy, forget about it. it doesn't matter how standards-compliant it is if it doesn't work right. as far as SuSE 7.1 goes, i recently installed it. when i tried to install it with the development package, the \"all of kde\" package, and the \"kde 2\" package, my graphics card crashed (i think) at the end of the install. during the install i could only configure x to run in 640x480 mode. nothing else would work. later i tried fixing this with sax2. nope. blank screen. and i couldn't get out of it with ctrl+alt+backspace so i had to reset my comp and run e2fsck. when my system recovered from that, i ran sax1 and it autodetected my graphics card (s3 virge/DX. only runs with generic SVGA server). i found my monitor in the monitor database, so that was easy. what i wanna know is why are there so many problems if it detected my card and had the monitor in the database? why didn't sax2 work when the graphical install did? why can't you select your monitor during the install to abvoid all these post-install configuration problems? why did my computer crash at the end of an attempted install? SuSE is a good distro, but they should take some pointers from corel without sacrificing any flexibility."
    author: "Rick Kreikebaum"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "don't even talk about how gcc-2.96-RH generates 100% compatible code. the point is that the version that ships with RH7 is buggy. you can't even compile your own kernel with it. if it works, great. if it's buggy, forget about it. it doesn't matter how standards-compliant it is if it doesn't work right. as far as SuSE 7.1 goes, i recently installed it. when i tried to install it with the development package, the \"all of kde\" package, and the \"kde 2\" package, my graphics card crashed (i think) at the end of the install. during the install i could only configure x to run in 640x480 mode. nothing else would work. later i tried fixing this with sax2. nope. blank screen. and i couldn't get out of it with ctrl+alt+backspace so i had to reset my comp and run e2fsck. when my system recovered from that, i ran sax1 and it autodetected my graphics card (s3 virge/DX. only runs with generic SVGA server). i found my monitor in the monitor database, so that was easy. what i wanna know is why are there so many problems if it detected my card and had the monitor in the database? why didn't sax2 work when the graphical install did? why can't you select your monitor during the install to abvoid all these post-install configuration problems? why did my computer crash at the end of an attempted install? SuSE is a good distro, but they should take some pointers from corel without sacrificing any flexibility."
    author: "Rick Kreikebaum"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "don't even talk about how gcc-2.96-RH generates 100% compatible code. the point is that the version that ships with RH7 is buggy. you can't even compile your own kernel with it. if it works, great. if it's buggy, forget about it. it doesn't matter how standards-compliant it is if it doesn't work right. as far as SuSE 7.1 goes, i recently installed it. when i tried to install it with the development package, the \"all of kde\" package, and the \"kde 2\" package, my graphics card crashed (i think) at the end of the install. during the install i could only configure x to run in 640x480 mode. nothing else would work. later i tried fixing this with sax2. nope. blank screen. and i couldn't get out of it with ctrl+alt+backspace so i had to reset my comp and run e2fsck. when my system recovered from that, i ran sax1 and it autodetected my graphics card (s3 virge/DX. only runs with generic SVGA server). i found my monitor in the monitor database, so that was easy. what i wanna know is why are there so many problems if it detected my card and had the monitor in the database? why didn't sax2 work when the graphical install did? why can't you select your monitor during the install to abvoid all these post-install configuration problems? why did my computer crash at the end of an attempted install? SuSE is a good distro, but they should take some pointers from corel without sacrificing any flexibility."
    author: "Rick Kreikebaum"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "don't even talk about how gcc-2.96-RH generates 100% compatible code. the point is that the version that ships with RH7 is buggy. you can't even compile your own kernel with it. if it works, great. if it's buggy, forget about it. it doesn't matter how standards-compliant it is if it doesn't work right. as far as SuSE 7.1 goes, i recently installed it. when i tried to install it with the development package, the \"all of kde\" package, and the \"kde 2\" package, my graphics card crashed (i think) at the end of the install. during the install i could only configure x to run in 640x480 mode. nothing else would work. later i tried fixing this with sax2. nope. blank screen. and i couldn't get out of it with ctrl+alt+backspace so i had to reset my comp and run e2fsck. when my system recovered from that, i ran sax1 and it autodetected my graphics card (s3 virge/DX. only runs with generic SVGA server). i found my monitor in the monitor database, so that was easy. what i wanna know is why are there so many problems if it detected my card and had the monitor in the database? why didn't sax2 work when the graphical install did? why can't you select your monitor during the install to abvoid all these post-install configuration problems? why did my computer crash at the end of an attempted install? SuSE is a good distro, but they should take some pointers from corel without sacrificing any flexibility."
    author: "Rick Kreikebaum"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-21
    body: "don't even talk about how gcc-2.96-RH generates 100% compatible code. the point is that the version that ships with RH7 is buggy. you can't even compile your own kernel with it. if it works, great. if it's buggy, forget about it. it doesn't matter how standards-compliant it is if it doesn't work right. as far as SuSE 7.1 goes, i recently installed it. when i tried to install it with the development package, the \"all of kde\" package, and the \"kde 2\" package, my graphics card crashed (i think) at the end of the install. during the install i could only configure x to run in 640x480 mode. nothing else would work. later i tried fixing this with sax2. nope. blank screen. and i couldn't get out of it with ctrl+alt+backspace so i had to reset my comp and run e2fsck. when my system recovered from that, i ran sax1 and it autodetected my graphics card (s3 virge/DX. only runs with generic SVGA server). i found my monitor in the monitor database, so that was easy. what i wanna know is why are there so many problems if it detected my card and had the monitor in the database? why didn't sax2 work when the graphical install did? why can't you select your monitor during the install to abvoid all these post-install configuration problems? why did my computer crash at the end of an attempted install? SuSE is a good distro, but they should take some pointers from corel without sacrificing any flexibility."
    author: "Rick Kreikebaum"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-22
    body: "oops. that's the last time i try to post a reply more than once. i didn't think it was working. none of the dot.kde.org pages were loading."
    author: "Rick Kreikebaum"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "I'm a sysadmin for a large ISP and I use Linux alone in my notebook. There's not a single task that I need to get done for which I can't find a good and stable Linux app to do the job. I use Mandrake 7.2, kernel 2.2.19, and KDE 2.1.1 and I don't even remember when my system last crashed, it simply doesn't happen even though I use it quite heavily with several apps running all the time. If your Linux/KDE is crashing so often, there's certainly something wrong with your installation."
    author: "Pedro Ziviani"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "What do you mean by saying that kernel is unstable??\nI have been using Linux with 2.2.x kernel for four years and never experienced a kernel crash! I mean, even X server crashed few times, but Linux kernel was still running. I don't know how is the situation with 2.4.x kernels, but I suspect it can't be worse than 2.2.x. Some unfinished KDE applications (like the KOffice ones) have been crashing, but nobody said that they were release versions, they are still developed. I do agree with you though, that KDE 2.0 was a bit too buggy for a release version, but hey, that's why they supplied KDE 2.0.1 so soon as a bugfix. Although I appreciate the work of people who work on KDE very much, I would suggest that when KDE x.0 is released, it can be even few months late (like it was a case with 2.4 kernel). It is nothing worse in software development than trying to release versions of software exactly on time, because it may hide much more hidden bugs because of all the hurry to release on time. In any way, KDE is really great desktop and everybody who is saying that KDE people did not do a wonderful job, should use Windoze for the rest of his(her) life."
    author: "Bojan"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "I completely agree with you. Specially with your last sentence."
    author: "uwe"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "KDE programs crash sometimes, that is true. But its completely ok, you can restart them and continue. Of course, as soon as KOffice starts doing that, I'd be more pissed, so they better make it stable (and/or) do this auto-saving stuff ms has tried to use for covering up their sloppy programming.\n\nBut the 2.4 kernel is indeed a little unstable, even Linus & Alan confirmed that. (I dunno the link, have a look at the kernel-ml's)."
    author: "me"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-22
    body: "yeah, the 2.4.0 kernel crashed on me once."
    author: "Rick Kreikebaum"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "simply not true !\ni'm using kernel-2.4 since 2.3.35 (or so)\nand the only problem i had the last 1 or 2 years\nis that my new cd-burner does not run and block\nmy linux.\nI'm also using kde-2.1.1 since 2.0 beta1 and \nit got very stable to version 2.1.1\nAlso the java and plugin support in konqueror \ngot very fine, and the only wish i have is better\nmsoffice support in koffice but this is comin in\nthe next koffice release.\nBut I'm compiling my apps by myself, my slackware\nis stable but some apps are slower than selfcompiled (for example kde).\n\nhappy day !"
    author: "Armin Krieg"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "Hey! My linux kernel also gets stopped when my CD reades has problems with some CD's.\nI just hangs the sistem (not actually hanged but stops for some seconds and then works ... stops...)\nWhen this situation arrives I can't umount /dec/cdrom.\n\nThis is REALLY a big problem and havn't found how to solve this. I have to say that M$ faces better with this situation with the same CD reader and the same CD's"
    author: "Landabaso"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "Try FreeBSD."
    author: "ac"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "Hi, what planet are you from? Well, my friend, I am from planet Earth, where there are certain rules to be followed in OSS discussion groups:\n\n*Complaining about anything and everything is a troll. If you don't like it, but don't have anything helpful to say, then go join a windows discussion group.\n*Hmm, KDE crashes. OSS Rule #1534: If it seems like everything is crashing, then something is misconfigured.\n*KDE is currently at 2.1.1 release, not 2.0. Complaing about 2.0 is not only pointless, it's laughable.\n*New kernel unstable? OSS Rule #366: If you find a bug, report it, with a specific example. DON'T make vague comments about \"crashing\".\n*\"Nearly every softwareproduct for linux that is called release is in my opinion early beta\". Wow, that's quite a sweeping comment there. Now not only is KDE and Linux at fault, but now it's almost every OSS product (except tex and emacs) that is at fault?"
    author: "Carbon"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "> *Hmm, KDE crashes. OSS Rule #1534: If it seems like everything is crashing, then something is misconfigured.\n\nOh yeah? I had KDE crashing all the time last year. My file system was getting corrupted and KDE would not even compile without taking down my system. Know what the problem was? Bad RAM! That's right! I tested 36 bytes on one of my 128 MB DIMMs choking badly. \n\nFrankly I'm extremely dissapointed that KDE, the kernel and other OSS software could not operate correctly with defective hardware! Of course if I had been running windoze I would not have noticed anything unusual since I expect it to crash.\n\nSince I have not gotten a response on my bad RAM bug report I'm beginning to suspect that, as I don't run winblows, OSS software must have broke it! Clearly this proves that KDE is not only buggy but dangerous.\n\n</satire> ;-)\nTo the rational empirical evaluations yield results. To the rest the revelations of cause and effect are just another conspiracy theory and illusion is more substantial than proof."
    author: "Eric Laffoon"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-21
    body: ">To the rational empirical evaluations yield results. To the rest the revelations of cause and effect are just another conspiracy theory and illusion is more substantial than proof.\n\nWell said. Is that a quote from somewhere?"
    author: "Carbon"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-21
    body: "Hi!\n\nI know i should do some more detailed failure descriptions, but maybe you are only playing with your linux box. I try to get some work done, and if i would report every bug i discover this would be a fulltime job *gg*\n\nAnd the main thing I wanted to say is that the linux-community should realy stop to release every buggy extension to the kernel as so called release. And also kde 2 should IMHO be released as 2.0 in maybe a half year....\nAnd what should be relly done is some serious testing. Testing is one of the most importand parts of software engeniering.\n\nYou can go on an play with every little kernel quirk and recompile all your apps. But I've got some work to be done.\n\ncu Robert"
    author: "Robert Ulmer"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-21
    body: "You completely missed my point.\n\nHere, I'll make it easy : what you are doing is trolling. Trolling is a waste of bandwidth, completely unhelpful, and very annoying. You are trolling because you are doing this : complaining endlessly in a completely unhelpful way. You seem to have nothing to say but \"Haha, I'm better then you, cause my software is stabler then yours, so there\". \n\nMoreover, I do not recompile all my apps regularly, and I do not play with every little kernel 'quirk'. Neither do the people who run Linux boxen and serven on a production level basis. \n\nIf you have no time to waste, and want to get to work, then please, by all means, get to it! There is no need to waste more time on dot.kde.org displaying your extreme superiority in using a \"certified\" OS."
    author: "Carbon"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-22
    body: "indeed, this is totally useless. you keep saying the same things without even trying to make some arguments.\nwhat 'buggy kernel extentions\" that are called 'release' are you talking about ? what bugs are you talking about ?\nAlso your point of view the entire oss community is a bunch of idiots playing around while being useless is at least a little bit unrealistic, not to say completely insane. think about it (if you dare).\n Maybe you should try to get some work done instead of trying to work out your frustrations of  your missed attempt to install linux (caus thats what you wanted to do didnot you) ... by the way we also get our work done , fast and efficient even. its not because you can't use linux/x/kde/... noone can."
    author: "ik"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-21
    body: "Hello;\nI simply agree with every single word you 've just said."
    author: "Hossam Al Din"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-21
    body: "Well... I wouldn't say it's all true. \nFirst of all, kernel 2.4 is *VERY* stable (for me). Its up-time is still longer than any other windows system (I'm not even referring Win9x, since its no competition for Linux or WinNT). \n\nAnother fact: as the Linux kernel goes up in\nits version, it becomes faster on the same machine. In windows, a new kernel would probably\neat up all you system.\n\nBesides, I know KDE has its crashes from time\nto time, but 2.2.1 is pretty nice (and stable,\ncomparing with earlier versions), and we shouldn't forgert its a freeware (and most of\nthe people behind KDE contribute, rather than \nget any money for their work). I think that if\nthe resources ever put in Windows/Office would\nhave put to KDE/Linux, things would probably looked different.\n\nI work both with both Win2000 and KDE on my Debian box. On a PII-300, KDE runs fast and\nflawlessly. \n\nNot good at all ?  That's not fair to say such thing.... \n\nAll the best,\n\tElad."
    author: "Elad Yarkoni"
  - subject: "Re: Jep i use KDE only sinc5 months and it sucks"
    date: 2001-04-22
    body: "JEP , i killed *all* windows stuff and installed \next2 on the whole computer , with kde , and its has so many GRAVE bugs , its nearly not usable.\nit does not even startup. ihave to try 2 times beforce it goes up, and so on .\nkwin reaaly suxx.."
    author: "chris"
  - subject: "Re: Jep i use KDE only sinc5 months and it sucks"
    date: 2001-04-22
    body: "The problems you are describing are *not* bugs in KDE.  Problems like that have to do with problems in KDE's installation or the installation of some other program that KDE depends on (X windows, etc).  If everyone was experiencing problems like that, it would be a KDE bug.  But since almost everyone here runs KDE fine, it is obviously a problem with your installation.\n\nThat doesn't mean it's your fault, though!  It probably has to do with bad packages from your distribution.  What distribution are you using?"
    author: "not me"
  - subject: "You asked for it"
    date: 2001-04-20
    body: "Since you bring in the next QT version as a remedy for the current problems:\nIs there any release date for the first beta ( stable API, not to many crahes ) on the horizon?"
    author: "ac"
  - subject: "Re: You asked for it"
    date: 2001-04-20
    body: "According to this comment posted on 10th April\nhttp://linuxtoday.com/news_story.php3?ltsn=2001-04-09-011-21-PS-0065\nit should be coming coming pretty soon"
    author: "nap"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "I have a rather simple solution to cut and paste.  I'm not sure if it's the same as the Qt-3.0 solution, as that wasn't described in much detail.\n\nHave 2 clipboards.  One for when you highlight something, and this would be pasted with the middle click.  That would keep people like me who are used to that style and find it more efficient happy.  The other clipboard would function more like the traditional Windows clipboard.  You highlight, but it isn't added to that clipboard until the user chooses to copy it (using control keys or a button).  If a user chooses to copy highlighted text, that text should then be removed from the middle-click clipboard and the old contents of that clipboard be restored.  Does that make sense?  That would satisfy both worlds, and, in many cases, come in handy for anybody.  I just hate when i accidently click and move the mouse, and highlight something else right before i'm about to paste a big document, and have to re-highlight it."
    author: "dingodonkey"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "Yes, it seems to be just about the same as the current agreement and Qt3.0 solution."
    author: "nap"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "I have found a bug in KDE. When will it be fixed?"
    author: "KDE User"
  - subject: "There's a bug in the KDE, dear Liza, dear Liza"
    date: 2001-04-20
    body: "Then fix it dear Henry. fix it."
    author: "Liza"
  - subject: "Re: There's a bug in the KDE, dear Liza, dear Liza"
    date: 2001-04-20
    body: "> > There's a bug in the KDE, dear Liza, dear \n> > Liza \n> \n> Then fix it dear Henry. fix it. \n\nWith what shall I fix it, dear Liza, dear Liza?"
    author: "Henry"
  - subject: "Re: There's a bug in the KDE, dear Liza, dear Liza"
    date: 2001-04-20
    body: "This is getting silly, now to something completely different."
    author: "reihal"
  - subject: "Re: There's a bug in the KDE, dear Liza, dear Liza"
    date: 2001-04-20
    body: "> > > There's a bug in the KDE, dear Liza, dear\n> > > Liza \n\n> > Then fix it dear Henry. fix it. \n\n> With what shall I fix it, dear Liza, dear Liza?\n\nWith your brain, dear Liza, dear Liza, dear Liza\n- with your brain:\n\n\n    The KDE 2 Development book\n    http://kde20development.andamooka.org\n\n\nHave fun!  :-)"
    author: "Charly"
  - subject: "Re: There's a bug in the KDE, dear Liza, dear Liza"
    date: 2001-04-20
    body: "> > > > There's a bug in the KDE, dear Liza, dear\n> > > > Liza \n\n> > > Then fix it dear Henry. fix it. \n\n> > With what shall I fix it, dear Liza, dear Liza?\n\n> With your brain, dear Liza, dear Liza, dear Liza\n> - with your brain:\n\n> Have fun! :-)\n> Charly \n\nBut there's a hole in my mind, dear Charly, dear Charly, there's a hole in my mind.\n\nWhat shall I do then?"
    author: "Henry"
  - subject: "Re: There's a bug in the KDE, dear Liza, dear Liza"
    date: 2001-04-20
    body: "go play with gnome :o)"
    author: "me"
  - subject: "Re: There's a bug in the KDE, dear Liza, dear Liza"
    date: 2001-04-21
    body: "yOu aRE a fAGGot"
    author: "JOE"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "right after you report it."
    author: "me"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-20
    body: "...and sometimes sooner !"
    author: "Friend of Roger DeRoy (RIP)"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "it's already fixed."
    author: "Evandro"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "> it's already fixed.\n\nOuch!\n\nMy cat was \"fixed\" once. \n\nHe didn't like it at all!"
    author: "Placido"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE"
    date: 2001-04-20
    body: "What's that bug ?"
    author: "not me"
  - subject: "This is a troll! He posted the same at Gnotices!"
    date: 2001-04-22
    body: "Ban his IP! Otherwise he will troll at both KDE and Gnome!"
    author: "ac"
  - subject: "Trolls are dying!"
    date: 2001-04-23
    body: "You don't have to be Kreskin to predict trolls' future.\nThe hand writing is on the wall: trolls faces a bleak future.\nIn fact there won't be any future at all for trolls because trolls are dying.\nThings are looking very bad for trolls.\nAs many of us are already aware, trolls continues to reduce in numbers.\nRed ink flows like a river of blood.\n\nLet's keep to the facts and look at the numbers.\n\nTroll leader ac states that there are 2000 trolls. How many usenet trolls are there?\nLet's see. The number of Slashdot trolls versus usenet trolls posts is roughly in ratio of 5 to 1.\nTherefore there are about 5000/5 = 1000 usenet trolls post.\nUsenet trolls posts are about half of the volume of Slashdot trolls posts.\nTherefore there are about 500 usenet trolls.\nSo there are only 1500 Slashdot trolls, compared to the 3 bilion Internet users.\n\nAll major surveys show that trolls have steadily decreased in numbers.\nTrolls are very sick and its long term survival prospects are very dim.\nIf trolls want survive at all it will be among trolling hobbyists, Microsoft zealots,\nand people with sick minds.\nTrolls continues to decay. Nothing short of a miracle could save it at this point in time.\nFor all practical purposes, trolls are dead."
    author: "TrollKiller"
  - subject: "Re: ZDNet Compares Linux Desktops:  Concludes KDE Best, But Not "
    date: 2001-04-21
    body: "As for fixing KDE Usability and design issues, I founded a project with Lee Jordan to identify usability issues and problems in KDE.\n\nWe are just in the process of building the KDE Usability Study up, but you can keep tracks of it at http://kdeusability.sourceforge.net/ and on kde-usability at http://lists.kde.org/.\n\nWe welcome any help and volunteers, so just get in touch if you are interested."
    author: "Jono"
  - subject: "KDE is incomplete without sys & net configuration"
    date: 2001-04-21
    body: "I have been yelling at bugs.kde.org for good system and network configuration modules for KControl, by watching and Using sources of DrakX, Yast, LinuxConf and other such configuration utilities. But No body cares! I Think that only these reviews will force KDE team to think about it!"
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE is incomplete without sys & net configuration"
    date: 2001-04-22
    body: "This is what distribuors should care of. KDE is not desktop for Linux it is for Unix. Remember this."
    author: "Hasso"
  - subject: "Re: KDE is incomplete without sys & net configuration"
    date: 2001-04-22
    body: "I still agree very much with those who ask for this. Whatever practical, principled or technical obstacles there is to a unification like this, it *must* be done one way or another. The usability benefits are just too great. I'm sure there are ways to get around these problems. For example the  shared platform nature of KDE could be \"solved\" by with a Linux specific extension, or in extreme, simply give up the goal of several platforms if there is no other way about it."
    author: "john"
  - subject: "Re: KDE is incomplete without sys & net configuration"
    date: 2001-04-22
    body: "They do care, but they have too many other things to tend to.  The KDE team is working very hard, and they will get to everything eventually.\n\nIt's not like they're doing something unimportant instead.  KOffice, KDevelop, Konqueror, and KDE2.2's new printing framework are all very necessary.\n\nAlso, like _many_ have said, right now it is the job of the distributions to make tools like these.  Maybe KDE can try to do this later with separate platform specific packages.\n\nSo for now, let them hack.  And a word of advice: \"Yelling\" at bugs.kde.org (or anyone anywhere for that matter) gets you nowhere.  This is mostly volunteer work.  Please, send a single bugwish and then get on with your life.  Or go read a HOWTO if you're having trouble configuring your system.\n\n-Justin"
    author: "Justin"
  - subject: "Re: KDE is incomplete without sys & net configuration"
    date: 2001-04-22
    body: "I sadly can't agree with you. True, KDE team works hard and stuff that you mention is important, but it's not like everyone works on that.\n\nNew printing framework is more or less developed by one guy. I don't know about KDevelop or Konqueror, but I can remember that few months back, before 2.0 release, there was a lot of discussion about KOffice. \n\nThe general agreement was that it was important to release a stable KDE2 first, after which a lot of effort would be moved to completing KOffice. That didn't happen. KOffice development community stayed more or less the same and those who did join were newcomers and not old KDE developers.\n\nI do think that good theme support is less important than good configuration utilities, but that's just my opinion and IMHO this is the catch. Everyone works on what is important to him. And that's fine.\n\nBut saying that it's distributions job to make these tools (and good packages) probably isn't. You see, there's strong competition between makers of distributions and configuration tools are one of the rare things they can use as a competitive advantage (this is probably the reason, why YAST doesn't have a free license). Therefore they don't have a real reason to work on them. \n\nPlus, they don't see the world through KDE's eyes. Caldera also wants remote configuration and aims to get it with Webmin. RedHat puts its money on GNOME. Basically you only have Mandrake and Suse and even Mandrake is very agnostic these days and the last time I looked at it, its utilities were GTK based.\n\nIt doesn't make any sense to leave a known problem open for years, just because you think it should be responsibility of someone else to fix it. Especially when these problems are a big part of user experience."
    author: "Marko Samastur"
  - subject: "Re: KDE is incomplete without sys & net configuration"
    date: 2001-04-22
    body: "I'm just wondering, why couldn't a completely new distribution based only on KDE and Qt be succesful ? I imagine this distribution having all the tools you are asking for and also having a great system of updating KDE. This distribution would be for the beginner user that wants to get some work done and not spend a lot of time installing / setting up things. What are the cons of such an idea ? Anyone ?\n\nJust a wild thought.\n\nJelmer"
    author: "Jelmer Feenstra"
  - subject: "Re: KDE is incomplete without sys & net configuration"
    date: 2001-04-22
    body: "The solution for me is Mandrake minus ANY link to GTK. That means that their own maintenance utilities are dropped. Also, you lose The Gimp. Any other GTK based applications are irrelevant.\n\nLong live KDE."
    author: "KDEBoy"
  - subject: "Re: KDE is incomplete without sys & net configuration"
    date: 2001-04-22
    body: "There's tons of distro like that, mandrake, caldera, SuSE, corel, and more, they use Gtk because RedHat uses Gtk and most of the distros are RedHat except they use KDE instead of GNOME.  Qt is great but distro's arent willing to spend the money writing the stuff themselves when they can just use RedHat's stuff and have it be almost as good."
    author: "robert"
  - subject: "Re: KDE is incomplete without sys & net configuration"
    date: 2001-04-22
    body: "There's tons of distro like that, mandrake, caldera, SuSE, corel, and more, they use Gtk because RedHat uses Gtk and most of the distros are RedHat except they use KDE instead of GNOME.  Qt is great but distro's arent willing to spend the money writing the stuff themselves when they can just use RedHat's stuff and have it be almost as good."
    author: "robert"
  - subject: "look here"
    date: 2001-04-22
    body: "www.redmondlinux.org"
    author: "not me"
  - subject: "Re: look here"
    date: 2001-04-22
    body: "hehe, I can see the sarcasm here (I think :)\n\nBut seriously, I'm just talking about the people generally want to use an easy system, more advanced users would probably be using dists like debian or slackware ofcourse. Why do you think windows became so popular ? KDE itself is rather userfriendly, but I just can't imagine installing this on my dad's system as I know he won't be able to install new programs, set up his dial-up connection, add user profiles for me, my sister and my mother etc etc. Is it really such a bad idea ?\n\nJelmer"
    author: "Jelmer Feenstra"
  - subject: "Re: look here"
    date: 2001-04-23
    body: "Just becuase people are advanced users dosen't mean that they want a distro that is difficult to use(ie slackware and debian, both suck)."
    author: "AC"
  - subject: "Re: look here"
    date: 2001-04-23
    body: "Well, I think configurability inevitably comes along with difficulty. I just think the kind of tight integration of the interface and system that windows has clearly helped it becoming so rapidly popular. In order to take over the world (ahem) we need to make the other part of linux user friendly as well, and that is the distribution, configuration of the _system_.\n\nJelmer"
    author: "Jelmer Feenstra"
  - subject: "Re: look here"
    date: 2001-04-25
    body: "No, I'm really serious!  Really!\n\nGo look at www.redmondlinux.org!  They are an acutal Linux distribution, and they are actually trying to do what you say!\n\nReally!"
    author: "not me"
  - subject: "Re: Never needed more than KDE..."
    date: 2001-04-22
    body: "I've been using Linux for over a year, and never once have I actualy loaded up Gnome...  I just never needed more than KDE.  For this last year, I've also had Windows as a safety, and I would say that I only load up Windows 0-1 time a month... (unless it crashes really fast :)\n\n  KDE is great, and who cares what some editors had to say about their first use of linux.  You wouldn't give a Mac a review on your first use, would you?  If you did, you'd be too stuck on \"How do I get my floppy back?\", and \"Where did my other mouse buttons go?\""
    author: "Greg Brubaker"
  - subject: "Re: Never needed more than KDE..."
    date: 2004-11-17
    body: "Greg,\n\nI apologize if I have the wrong Greg.  I am trying to find my brother who was adopted by Brubakers in Ohio over 30 years ago.  I met him once while in Middle School and would like to reconnect with him again now that I am a grown woman with my own family.  Our Father died in January of '73 when I was 9 months old.  I have been in touch with our sister Kelly, but would very much like to find my brother Greg again and talk.  Hope this is you, but if not I will keep searching \n\nLove Ya,\nBeth"
    author: "Beth McConn Norris"
  - subject: "Trolls are dying!"
    date: 2001-04-23
    body: "You don't have to be Kreskin to predict trolls' future.\nThe hand writing is on the wall: trolls faces a bleak future.\nIn fact there won't be any future at all for trolls because trolls are dying.\nThings are looking very bad for trolls.\nAs many of us are already aware, trolls continues to reduce in numbers.\nRed ink flows like a river of blood.\n\nLet's keep to the facts and look at the numbers.\n\nTroll leader ac states that there are 2000 trolls. How many usenet trolls are there?\nLet's see. The number of Slashdot trolls versus usenet trolls posts is roughly in ratio of 5 to 1.\nTherefore there are about 5000/5 = 1000 usenet trolls post.\nUsenet trolls posts are about half of the volume of Slashdot trolls posts.\nTherefore there are about 500 usenet trolls.\nSo there are only 1500 Slashdot trolls, compared to the 3 bilion Internet users.\n\nAll major surveys show that trolls have steadily decreased in numbers.\nTrolls are very sick and its long term survival prospects are very dim.\nIf trolls want survive at all it will be among trolling hobbyists, Microsoft zealots,\nand people with sick minds.\nTrolls continues to decay. Nothing short of a miracle could save it at this point in time.\nFor all practical purposes, trolls are dead."
    author: "TrollKiller"
---
<A HREF="http://www.zdnet.com/">ZDNet</A> has published a <A HREF="http://www.zdnet.com/zdnn/stories/news/0,4586,2709282,00.html">review</A> by Jason Brooks of <A HREF="http://www.zdnet.com/eweek/">eWeek Labs</A> comparing the Linux desktops.  He writes: <EM>"eWeek Labs found that KDE (K Desktop Environment) comes much closer to
                             delivering the sort of smooth interface that users have come to expect from the
                             Macintosh and Windows operating systems than does GNOME (GNU Network
                             Object Model Environment). In tests, KDE delivered snappier and more polished
                             performance than did GNOME on the same hardware."</EM>  However, he continues, <EM>"neither desktop interface has yet reached parity with the established
                             players-pervasive support for features such as cut and paste across the
                             interface can still be unpredictable."</EM>  Strange to have picked out cut-and-paste, as that should be <A HREF="http://dot.kde.org/986846589/986855156/">much improved</A> in Qt-3.0.

<!--break-->
