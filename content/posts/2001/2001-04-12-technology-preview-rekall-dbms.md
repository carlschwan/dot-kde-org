---
title: "Technology Preview of Rekall DBMS"
date:    2001-04-12
authors:
  - "Dre"
slug:    technology-preview-rekall-dbms
comments:
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-04-11
    body: "theKompany.com is overloaded, so there seem to be an interest in this or mayby theKompany.com just rock."
    author: "Mayhem"
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-04-11
    body: "They have been literally Slashdotted today."
    author: "KDE User"
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-04-11
    body: "It's just these type of applications that are going to bring KDE and Linux in general to a wider audiance. Keep up the good work Shawn and bravo to all you KDE and Koffice hackers out there. I looking forward to the final releases of Kapital, Aethera and Rekall. Kde will be looking very sweet by this fall.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-04-11
    body: "Hi,\n\nRekall also has a sourceforge page at http://sourceforge.net/projects/rekall\n\nDid anyone had success compiling xbsql on suse 7.1 ?\n\nI get:\n\n-I/usr/include/xbase -I. -I.     -g -c xbsql.tab.c\nIn file included from lex.yy.c:41,\nfrom xbsql.y:456:\n/usr/include/unistd.h:310: type specifier omitted for\nparameter\n/usr/include/unistd.h:310: parse error before `1'\nmake: *** [xbsql.tab.lo] Error 1"
    author: "Pyretic"
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-04-11
    body: "Hi,\n\nI think Rekall could be a major step forward towards a mature KDE-Desktop.So I tried to compile xbsql on my SuSE 6.4 machine but encountered some problems.\n\n 1) Using the xbase-rpms, I found xbase.h      \t\n    installed under /usr/include/xbase/ while\n     xbsql expects this file under /usr/include.\n    OK, I copied it.\n\n 2) Now I get \n\n    xql.cpp: In function `int main( int, char **)'\n    xql.cpp:137: initialization to `char *' from       `const char *' discards qualifiers\n    xql.cpp:164: initialization to `char *' from\n    `const char *' discards qualifiers\n\nI am not an expert at C(++), but this looks like some kind of typecast is missing, right? (Probably wrong)\nAny hints are welcome."
    author: "Olaf Bergner"
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-04-12
    body: "i had the same error.\ni did a make -k install, and installed rekall with --nodeps and it works"
    author: "me"
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-04-12
    body: "To compile it, do the following:\n\nProblem: \n/usr/include/unistd.h:310: \ntype specifier omitted for parameter\n\nFix:\nRemove the #include unistd.h\n\nProblem:\nxql.cpp: In function `int main( int, char **)'\nxql.cpp:137: initialization to `char *' from `const char *' discards qualifiers\nxql.cpp:164: initialization to `char *' from\n`const char *' discards qualifiers\n\nFix:\nAdd the cast (char*) where needed in lines\n137ff and 164ff"
    author: "Joe Dow"
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-04-12
    body: "We are looking at the compile problems right now, we'll try to have something for tomorrow."
    author: "Shawn Gordon"
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-04-12
    body: "Many thanks for your immediate response.May I add that IMHO you and your company is doing a great job."
    author: "Olaf Bergner"
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-04-12
    body: "As allways shawn you are the fastest guy i ever seen when it come to email ;-)\n\n\nkeep your Kompany alive\n\nyou guys simply rock"
    author: "jorge costa"
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-04-18
    body: "I get theese problems on my mandrake 7.2 system:\nAfter correcting the above mentioned cast probs, it goes fine up to this point:\n\n*** Warning: This library needs some functionality provided by -lfl.\n*** I have the capability to make that library automatically link in when\n*** you link to this library.  But I can only do this if you have a\n*** shared version of the library, which you do not appear to have.\n\n(The flex package installed by mandrake developer installation provides only a static libfl)\n\n\ng++ -DHAVE_CONFIG_H -I. -I. -I.     -g -c xql.cpp\n/bin/sh ../libtool --silent --mode=link g++  -g -g -o xql  xql.o -lreadline libxbsql.la\n/usr/lib/libreadline.so: undefined reference to `tgetnum'\n/usr/lib/libreadline.so: undefined reference to `tgoto'\n/usr/lib/libreadline.so: undefined reference to `tgetflag'\n/usr/lib/libreadline.so: undefined reference to `BC'\n/usr/lib/libreadline.so: undefined reference to `tputs'\n/usr/lib/libreadline.so: undefined reference to `PC'\n/usr/lib/libreadline.so: undefined reference to `tgetent'\n/usr/lib/libreadline.so: undefined reference to `UP'\n/usr/lib/libreadline.so: undefined reference to `tgetstr'\n\nwhich may be a result of the missing libfl??"
    author: "Anders Lund"
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-06-04
    body: "This probably isn't the proper way to fix it but I modified line 106 of the Makefile in the xbsql directory by adding -lncurses following -lreadline. It then built properly...\n\nxql_LDADD = -lreadline -lncurses libxbsql.la"
    author: "Scott Dysinger"
  - subject: "OT - MDI?"
    date: 2001-04-12
    body: "The screenshot look like \"crappy\" MDI paradigma.\nCome on, there _must_ be something better than this. Falks MDI-class  seems to be better the original QT-MDI \"trash\".\n\n\nGetting more OT:\n\nI also see some strong usability problems when programs using MDI-structures for control-panes (e.g. Killustrator layer-box). \nThink of a user writing a text in KWord. Now she needs a figure and inserts a (e.g.) Krayon-kpart. What she gets is a nearly unusable frame, because the pane occupies the whole kpart-frame. As a user I want to tear off the pane and fix it maybe at the kword-window and (also as a user) I want \"full-window\"-editing of an active kpart.\n\nHas somebody a good idea for a user friendly multi-document interface in a KPart world?\n\n(Before someone start: Uhh, Thorsten is whining once again about MDI:)\n\nIt's only about _this_ kind of MDI. I like the way implemented in Konq and Gideon. Also  Kant, ups Kate (sorry Lady ;-) MDI is ok! (I nagged on it in the newsgroup), hmmm - KDevelop-2 MDI is \"second quality\" (sorry ;-)\n\n[Ok, and next time I start once again nagging on silly filesector-boxes vs. a easy drag&drop \"Save\" icon ;-))]\n\nBye"
    author: "Thorsten Schnebeck"
  - subject: "Re: OT - MDI?"
    date: 2001-04-12
    body: "We have this argument internally all the time :), there are some things we are going to try out for the next release."
    author: "Shawn Gordon"
  - subject: "Re: OT - MDI?"
    date: 2001-04-12
    body: "The ugly windows-y MDI interface was what struck me first, too. I also have a problem that I see the others don't seem to have, with what all the cheering around every theKompany announcement:\n\n<ol>\n<li>The development model seems to be too much cathedral-like, mainly because all those theKompany apps are not in the KDE CVS.</li>\n\n<li> Why do they develop applications for which there is already an (KDE native) alternative? I think of Aethera: Why don't they help in extending KMail in the first place?\n<p>\nI think the development power of theKompany would be better utilized in providing applications that do <em>not</em> yet have an KDE native alternative. Kapital is certainly one of those and I don't mind them charging money for that then. </li>\n\n<li> I always wonder how they will make money from their business model. I appreciate it when a commercial company releases a huge amount of code under a free open source licence, yet if there is not a sound business model behind that, they will probably go the way Eazel currently goes: They have a real great product, but not the economic strength to maintain it (some way done the road). I also believe that domination of a single company w.r.t. the applications of <em>any</em> system is very bad for that system. It has been so (with MS in Windows of course, but also) with ASH in the AtariST world: In the beginning they were celebrated for their Magic TOS-compatibe OS, but soon they took over of what was left of the AtariST app market. So I always get a bad feeling when a single kompany <em>(sic!)</em> does <em>too much</em> for a given platform.</li>\n</ol>\n\nyet, I must admit that I haven't looked at theKompany too closely and maybe I'm unjust w.r.t. them. I just wanted to bring a more critical voice into the discussion."
    author: "Marc Mutz"
  - subject: "Re: OT - MDI?"
    date: 2001-04-12
    body: "> The ugly windows-y MDI interface was what struck me first, too.\n\nCould be the fact that they seem to have based it off of the acclaimed QT Designer?\n\n> The development model seems to be too much cathedral-like, mainly because all those theKompany apps are not in the KDE CVS.\n\nEver look at KDE CVS??? Note kdedb, kivio, kugar, kamera and probably others I'm missing. There is a fair amount of theKompany code in KDE CVS including code applicable to this release.\n\n> Why do they develop applications for which there is already an (KDE native) alternative? I think of Aethera: Why don't they help in extending KMail in the first place?\n\nDo you prefer the \"one size fits all\" model in the windoze world? FYI Aethera started out as Magellan and theKompany got involved with it. The aims are different and kmail is already well received as very good at what it does... it is just philosophically different as a program.\n\n> yet, I must admit that I haven't looked at theKompany too closely and maybe I'm unjust w.r.t. them. I just wanted to bring a more critical voice into the discussion.\n\nThere is nothing wrong with being critical. We should never lose perspective... however perhaps you should look into that which you critisize. If you don't then you run a high risk of appearing foolish or less than constructive. I think most certainly comparing theKompany (who has a business model I can understand) with Eazel (who has no business model I can discern) is rediculous. I don't know how much business you understand but here are two striking differences between theKompany and Eazel. First theKompany has a revenue stream (and model) and second, unlike Eazel if it is not $500k-$1m this month it will not produce red ink and rabid investors! In fact the list of differences between the two companies with regard to business model, philosophy, technology choices, style and involvement in the desktop model is too long to even contemplate here. The only similarities are that they produce software for Linux.\n\nAs always I strongly advocate people have some basic knowlege of what they are posting."
    author: "Eric Laffoon"
  - subject: "Re: OT - MDI?"
    date: 2001-04-12
    body: "Yes, I have looked at the KDE CVS lately. A\ntypical cvs commit looks like this:\n\n\"Added changes for latest Kivio from theKompany's CVS.\"\n\nSo obviously the kompany is not using the kde cvs\nfor development. There are just using it as \ndistribution medium.\n\nIf you advocate people to have some basic knowledge of what they are posting, fine:\nbest start with yourself."
    author: "me"
  - subject: "Re: OT - MDI?"
    date: 2001-04-12
    body: "I see your point, but I don't think that you are being entirely fair to theKompany wrt applications that they help donated to the KDE project and now help maintain in KDE CVS.  \n\nThe fact that the apps are in KDE CVS means that anyone with commit rights to that CVS code can make changes.  With this in mind, theKompany must synch the code in their CVS with the KDE CVS regularly to ensure that they are not developing with an out of date code base.  \n\nI would say that their private CVS is simply an aid to their own development and the official code is in the KDE CVS."
    author: "Rob"
  - subject: "Re: OT - MDI?"
    date: 2001-04-12
    body: "Ironic you claim little knowledge and suddenly post this \"revelation\". I smell a troll.\n\n> Yes, I have looked at the KDE CVS lately.\nI look at it several times a day.\n\n> typical cvs commit looks like this:\n\"Added changes for latest Kivio from theKompany's CVS.\"\n\nFunny? What file? I looked at several dozen of the highest rev files in the main module and did not see this in any recent commits. I wanted to be sure before caling an anonymous coward a liar. Of course in your last post it was that theKompany did not have apps in KDE CVS. That was a farce. Try another farce?\n\nIf you go back far enough to where Kivio was moved to KDE CVS you will see postings about things being moved. You will also see core KDE developers posting and different developers than on theKompany CVS... yes, they still have Kivio in their public CVS which looks odd. So I pulled it and looked. In a quick review it appears that it has not been updated since the move and they simply have not bothered to delete it from CVS.\n\n> So obviously the kompany is not using the kde cvs for development. There are just using it as distribution medium.\n\nNo, obviously you either don't understand what you're looking at... or you are just trolling, lying and and looking to smear a company that is donating free software to the KDE project.\n\n> If you advocate people to have some basic knowledge of what they are posting, fine: best start with yourself.\n\nI will take back what I said. It seems obvious that you do not wish to be confused by facts. So please don't bother posting to try to prove you what know. If you want to say something bad about someone giving you free software I suggest you assume a real identity and deal in facts for a start. It's still slimey but it's slimey with a spine.\n\nMaybe I'm old fashioned. I think if someone gives you something valuable for free you ought to have a modicum of gratitude... and if you wish to say something bad about them you ought to have the decency to do your due dilligence and be certain what you say has some merit.\n\nHow can I put this? An opinion with no basis in fact is little more than a prejudice."
    author: "Eric Laffoon"
  - subject: "Re: OT - MDI?"
    date: 2001-04-17
    body: "You are wrong about the KDE CVS.  I originally used theKompany's CVS repository for Kivio code. Then the decision was made to put it in the KDE CVS repository. However, there were several complications and over a period of time, development was done out of both KDE and theKompany's CVS repository.  This is where comments like this came from.\n\nI personally develop from the KDE CVS repo for Kivio and haven't touched theKompany's for a *long* time.\n\nAs for my comments in the KDE CVS repo, I know they are usually very brief or lacking.  I personally don't like CVS as I always forget to update my code before I start working on it and other things like that so when I type up a huge description of changes, attempt to commit, and it fails due to a conflict - I don't usually type up the same description again out of frustration.\n\n-dave\n(Kivio developer, Queesio author)"
    author: "Dave Marotti"
  - subject: "Re: OT - MDI?"
    date: 2001-04-12
    body: "> The ugly windows-y MDI interface was what struck me first, too.\n\nCould be the fact that they seem to have based it off of the acclaimed QT Designer?\n\n> The development model seems to be too much cathedral-like, mainly because all those theKompany apps are not in the KDE CVS.\n\nEver look at KDE CVS??? Note kdedb, kivio, kugar, kamera and probably others I'm missing. There is a fair amount of theKompany code in KDE CVS including code applicable to this release.\n\n> Why do they develop applications for which there is already an (KDE native) alternative? I think of Aethera: Why don't they help in extending KMail in the first place?\n\nDo you prefer the \"one size fits all\" model in the windoze world? FYI Aethera started out as Magellan and theKompany got involved with it. The aims are different and kmail is already well received as very good at what it does... it is just philosophically different as a program.\n\n> yet, I must admit that I haven't looked at theKompany too closely and maybe I'm unjust w.r.t. them. I just wanted to bring a more critical voice into the discussion.\n\nThere is nothing wrong with being critical. We should never lose perspective... however perhaps you should look into that which you critisize. If you don't then you run a high risk of appearing foolish or less than constructive. I think most certainly comparing theKompany (who has a business model I can understand) with Eazel (who has no business model I can discern) is rediculous. I don't know how much business you understand but here are two striking differences between theKompany and Eazel. First theKompany has a revenue stream (and model) and second, unlike Eazel if it is not $500k-$1m this month it will not produce red ink and rabid investors! In fact the list of differences between the two companies with regard to business model, philosophy, technology choices, style and involvement in the desktop model is too long to even contemplate here. The only similarities are that they produce software for Linux.\n\nAs always I strongly advocate people have some basic knowlege of what they are posting."
    author: "Eric Laffoon"
  - subject: "Re: OT - MDI?"
    date: 2001-04-13
    body: "This whole MDI is bad thing is really getting on my nerves.\n\nLet me add my opinion:\n\n - First, if you think that applying a GUI design paradign to any type of software and expecting a good or bad result, you are being silly. You can't just say...MDI is bad...as MDI works well in many application types. The fact is that when developing software, you first look at the document model...is it centric? is it abstractive? is it metaphorical? Once you have an idea of your document model you can base your interface model (paradigm) around it. Some software works well with MDI (like rekall), and some doesnt.\n\n - The whole MDI is bad argument is getting pretty old now. Many people have stated that UI research states that it is bad. Well this is not quite accurate. *Some* UI has stated this, just like some research states MS produces good GUI design data...it is objective.\n\n - Even if UI research states MDI is bad, this doesn't mean that practically it is bad. An example is in MS Office 2000; some designer at MS saw the UI desig rule of \"keep visibility limited\", and they came up with the arrows thing at the bottom of the menu's which hides things away. My initial research (for my Uni HCI work) shows this new design is very bad for both new and experienced users of the product. Theory is meant as a guide, not a rule.\n\n - We must remember GUI experience and metaphor when expressing opinion on GUI models. I am sure rekall is aimed at a subset of the windows community, and like it or not...the MDI model works very well in the windows world...and it doesnt matter what the usability design and realisation books say...if a user interacts well with an application or information resource...it works.\n\n - The Konqi/Katy MDI method is good for certain document centric applications (usually where the view is a view and not necceccerily  a document). Konqi does it well as the multiple views can be used for browsing multiple resources. Also, in Konqi and Katy, there is no concern of screen estate. An example is if you were using a WYSIWYG web editor such as Kafka (which I maintain), a designer may be using a resoloution of 800x600 and rely on a window and document view being the size of the product (web page). An example of this is in image editing where Photoshop uses MDI really well (as does GIMP...although GIMP unfortunatly does not use a grey image window background which affects colour interpretation and mapping). Using the Konqi style MDI would be hell for an image editor. It works well for certain things (such as Konqi), but *not* all things...such as rekall.\n\nPlease think about all of the GUI interpretation, experience, natural mapping and metaphors before making ourlandish claims. If you are nopt familier with any of this...then I suggest you get an *informed* opinion before you post.\n\nBy the way...I am in the process of building the KDE Usability Study to analyse and test KDE on usability standards and data. Join kde-usability if you want to get involved. :-)\n\n\n    Jono"
    author: "jono"
  - subject: "Re: OT - MDI?"
    date: 2001-04-22
    body: "Please! This is the first Rekall TPR.\n\nYou can blame me for most of the current code, but we don't intend to force \"crappy MDI\" on everyone. The general intent is to allow the user to choose the scheme they prefer.\n\nI've had a look at QextMDI, but I found it was currently not up to providing what was needed. The next release will most likely have a \"crappy MDI\" or \"wonderful SDI\" option.\n\nI would like to see a standard KDE library for this, to provide application consistency; if this comes along then expect Rekall to migrate to it.\n\nRegards\nMike"
    author: "Mike Richardson"
  - subject: "Questions"
    date: 2001-04-18
    body: "first of all KUDOS!\n1)since there will be two versions of the product, one OP.Source and one commercial, will they be compatible? e.g. I develop a database with ReKall commercial , will it run on ReKall Open source?. If not you will have to develop a free runtime, so that we will be able to distribute our work without paying licences/fees.\n2)Will there be some pre-built macros for users which don't know how to code?\n3)Is porting to windows being thought, for any of your apps? I certainly love using KDE, but in many cases mixed enviropments DO exist. If not porting the entire package, then just porting a Run time executable, should be more than enough, in this case\n4) How is your International support? really Support for multiple lanuages and unicode would be very cool!\nThanks for your time!"
    author: "t0m_dR"
  - subject: "Re: Questions"
    date: 2001-04-18
    body: "1) The idea is that the commercial is a series of plug ins that extend the functionality, so they aren't different code bases, Rekall is LGPL.  I'm not positive of how the run time will work, we are also working on the details of that structure.\n\n2) There will be wizards that will generate default applications against a table that will add/modify/delete/inquire.  We will evolve it from there.\n\n3) Haven't given Windows much thought, we might make it pure Qt and take advantage of that as a multi-platform layer.\n\n4) We have an international group of employees, so that will also be something we do."
    author: "Shawn Gordon"
  - subject: "Re: Technology Preview of Rekall DBMS"
    date: 2001-04-20
    body: "We patched the xbsql source and that seems to solve theproblem for many people.  Check it out."
    author: "Shawn Gordon"
---
<A HREF="http://www.thekompany.com/">theKompany.com</A> has announced
a technology preview release of
<A HREF="http://www.thekompany.com/projects/rekall/">Rekall</A>, a personal,
programmable DBMS for KDE.  Rekall will simplify building database applications
with forms and reports.  According to CEO Shawn Gordon, Rekall will have
a full complement of widgets so that applications built with Rekall will be
able to have the look and feel of any other application.  He adds that Rekall
applications can be extended in their functionality arbitrarily via embedded
<A HREF="http://www.python.org/">Python</A> as a scripting language (this
capability is not included in the first release).  This is a very
positive development for KDE, as programmable databases such as
dBase,
<A HREF="http://www.corel.com/products/wordperfect/paradox8/index.htm">Paradox</A>
and <A HREF="http://www.microsoft.com/office/access/default.htm">MS Access</A>,
available on other platforms, have enabled
users to focus on the data model and to leverage their business knowledge
into working applications.  Rekall even promises some advantages over
the aforementioned products, as it does not rely on a native database and
instead can be used with a database of the user's choice, such as
<A HREF="http://www.mysql.com/">MySQL</A>,
<A HREF="http://looking-glass.usask.ca:82/">PostgreSQL</A> or
<A HREF="http://www.orcale.com/">Oracle</A>, and uses a default database
format that is meant to be light weight, easy to use and require no RDBMS
experience.  theKompany is working with the <A HREF="http://www.koffice.org/">KOffice</A> developers to include portions of this technology in KOffice.  More information (including <a href="http://www.thekompany.com/projects/rekall/screenshots.php3">screenshots</a>) is
available at <A HREF="http://www.thekompany.com/projects/rekall/">Rekall's
homepage</A>.


<!--break-->
