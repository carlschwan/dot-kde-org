---
title: "KDE Book Used in Universities"
date:    2001-02-11
authors:
  - "dsweet"
slug:    kde-book-used-universities
comments:
  - subject: "Re: KDE Book Used in Universities"
    date: 2001-02-11
    body: "Karlstads Universitet eh?\nThis sounds like the work of the scheming Kalle Dalheimer!"
    author: "reihal"
  - subject: "Re: KDE Book Used in Universities"
    date: 2001-02-11
    body: "Hm, we are using KDE on Solaris at the \"Hochschule f\u00fcr Technik und Wirtschaft des Saarlandes\" (in Germany). But Programming etc. is made with Java.\n\nhttp://www.htw-saarland.de"
    author: "Donald"
  - subject: "New great book on KDE 2.0 Development"
    date: 2001-02-11
    body: "On a related note, a wonderful new book on KDE 2.0 programming has just come out:<p>\n<a href=\"http://www.amazon.com/exec/obidos/ASIN/1929629133/o/qid=981917256/sr=8-1/ref=aps_sr_b_1_1/105-8037335-1856710\">Programming Kde 2.0 : Creating Linux Desktop Applications by Lotzi B\u00f6l\u00f6ni</a>\n<p>\nI have looked at it and am very impressed. The author obviously has followed developments in \nthe KDE community very closely. (see for example\nhis very true comments on why it was a very smart move for us to move away from \nCORBA). I recommend this book to anyone wanting\nto understand KDE and KDE programming better."
    author: "Bernd"
  - subject: "Re: New great book on KDE 2.0 Development"
    date: 2001-06-12
    body: "I agree.  It shows plainly how simple it can be to write applications for KDE 2.0, which is my fav anyways. =)"
    author: "Gavin"
  - subject: "Re: KDE Book Used in Universities"
    date: 2001-02-11
    body: "I've seen KDE being the standard DE in the computer science department in Hamburg's Technical University. They didn't teach it (at that time), but they used it. Very good feeling seing KDE spread!"
    author: "anonymous coward"
  - subject: "Re: KDE Book Used in Universities"
    date: 2001-02-12
    body: "It's also the default in the CS department of the Technical University of Berlin (on Sun/Solaris)."
    author: "gis"
  - subject: "Re: KDE Book Used in Universities"
    date: 2001-02-11
    body: "We are planning to use KDE for teaching\nour COMP3141 course at UNSW,  \n\"Software System Design and Implementation\".\n<P>\nWe will also use the \"KDE 2.0 Development\" book."
    author: "Amir Michail"
  - subject: "Re: KDE Book Used in Universities"
    date: 2001-02-12
    body: "KDE is used in some departments of the Iceland University - the Icelandic KDE group was there last night updating our stuff..."
    author: "Sm\u00e1ri P. McCarthy"
  - subject: "ugg, not at my university"
    date: 2001-11-02
    body: "Our great up to date university uses FVWM.  University of Alberta..."
    author: "simon"
---
You might be interested to know that
KDE is being used for
<a HREF="http://www.andamooka.org/article.pl?sid=01/02/09/1457224&mode=thread">teaching in universities</A>.  I'm also pleased to say that they're using the open content <A HREF="http://kde20development.andamooka.org"><em>KDE 2.0 Development</em></A> book.

Perhaps this isn't news to students, but it was to me! If you're also reading the book, I encourage you to 
visit <A HREF="http://kde20development.andamooka.org">Andamooka</A>
and annotate it.  Your comments are very
helpful and appreciated.
<i>[<b>Ed:</b> Specifically, this fine book is being used by the <a href="http://www.fh-rhein-sieg.de/">University of Applied Sciences Bonn-Rhein-Sieg</a> and <a href="http://www.kau.se/">Karlstads Universitet</a>.  If you know of any other schools or Universities using KDE or KDE books to teach, we'd love to hear it.]</i>



<!--break-->
