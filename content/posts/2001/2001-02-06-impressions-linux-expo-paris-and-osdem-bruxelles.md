---
title: "Impressions from Linux Expo Paris and OSDEM Bruxelles"
date:    2001-02-06
authors:
  - "Dre"
slug:    impressions-linux-expo-paris-and-osdem-bruxelles
comments:
  - subject: "Re: Impressions from Linux Expo Paris and OSDEM Bruxelles"
    date: 2001-02-06
    body: "Good work David!\n\nIt is good to hear that KDE is going down so well, and it is great to hear of your work.\n\nI just wish that when we meet in London last year that I could have chatted to you as a developer instead of a journalist!\n\nKeep up the good work...and I must sort out a UK KDE developer meeting sometime..."
    author: "Jono Bacon"
  - subject: "David gets to go to all the cool happenings.."
    date: 2001-02-06
    body: "Damn damn damn (I can say damn here right?)\n\nWhy doesn't david take some of us loving kde-users with him when he goes to all those cool expos???\n\n*sigh*"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: David gets to go to all the cool happenings.."
    date: 2001-02-06
    body: "You could have come, the event was announced on this very web-site :)\n\nI guess you'll join the KDE crowd at LinuxTag in July, that shouldn't be too far. I might go there too - not that it will lack KDE developers, but in order to meet all those cool german KDE developers in real life one more time :-)"
    author: "David Faure"
  - subject: "Re: David gets to go to all the cool happenings.."
    date: 2001-02-06
    body: "Where will the LinuxTag be held?  I'm living in Norway at the west-coast (\u00c5lesund)... :)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: David gets to go to all the cool happenings.."
    date: 2001-02-06
    body: "The LinuxTag will take place in Stuttgart on 5th to 8th July."
    author: "Benedikt Meurer"
  - subject: "Re: David gets to go to all the cool happenings.."
    date: 2001-02-06
    body: "If only we had a couple of David Faures in America!  KDE PR would make a killing here..."
    author: "KDE User"
  - subject: "Re: David gets to go to all the cool happenings.."
    date: 2001-02-06
    body: "We've got Kurt Granroth right in Arizona, he does a fair amount of speaking, and he does a very good job as well.  Thanks to SuSE for supporting his efforts :)"
    author: "Shawn Gordon"
  - subject: "Re: Word translators for KOffice"
    date: 2001-02-06
    body: "<p><i>What comes out of discussing with many people is the obvious need for an office suite under Linux, and this instills me with the idea of starting to work on KWord. The need for MS Office documents filters is urgent as well, of course.</i>\n<p>\nI hope you'll keep working with the wvWare folks (http://www.wvWare.com) on Word translators. It would be great if all the Linux word processors could draw from a single source of translating talent.\n<p>"
    author: "Michael O'Henly"
  - subject: "Re: Word translators for KOffice"
    date: 2001-02-06
    body: "IMHO filters should be one of the Top priorities of the Koffice project. Even if Koffice is better than M$office it will NOT prevail if it doesnt have *excelent* filtersl. Filters that import more than 97% percent of the formating of the word/excel document and vice versa. People are enslaved under M$office and they can't get out! Help them do this!<br><br>\n\nAlso from what i've seen Kword kicks ass over Abiword but Gnumeric kicks ass over Kspread. What could a part of the KDE people do is take the code and port it to Qt and Kparts, renaming it Kalculus (or Knumeric) and making it part of the Koffice project (having 2 spreadsheets aint so bad, Gnome people have two office suites)\nSince Gnumeric is GPLed there is no *ethical* problem, because that's the point of the GPL, freely re-using software.<br><br>\n\njust my 0.02 Euro :-)"
    author: "Techios"
  - subject: "Re: Word translators for KOffice"
    date: 2001-02-06
    body: "I full agree with the submitter of this mailing. At work I'm using kde, but for the office part I must rely on other applications like staroffice or since recently xlhtml see http://www.xlhtml.org (have a look at this apllication it works quite well)\n\nThe MS import filter are despearedly needed, and should be able to process/display pictures as well.\n\nTo my opinion export filters to MS format are not important.  One never know whether a native MS application crashes on a document created with non MS apps and it needs a lot of energy as well. It's better to distribute documents in ascii, pdf or html formatted docs."
    author: "Richard Bos"
  - subject: "export filters ARE necessary"
    date: 2001-02-06
    body: "At least where I work, kword needs an rtf or simple doc export to be viable. Some text format that supports FOOTNOTES (unlike html) and simple textformatting as well as tables to exchange a working draft of a document.\n\nOhterwise I could work on a text here, but never give it to someone else to continue my work. \n(Unless they use kword, which I can't and won't force on anyone, to everyone his favorite tool)"
    author: "Moritz Moeller-Herrmann"
  - subject: "wvWare and kword"
    date: 2001-02-06
    body: "At least on the wvWare some cooperation is mentioned, but I don't know how far it goes."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Word translators for KOffice"
    date: 2001-07-20
    body: "I want to be able to send and receive documents from Microsoft to Linux and back. Please advise."
    author: "Bonner"
  - subject: "Re: Impressions from Linux Expo Paris and OSDEM Bruxelles"
    date: 2001-02-06
    body: "Excellent, David. Thank you for the whole lot of energy you put in code, articles, book texts, presentations, travels, advocating, fighting, getting rained over :-). Eager to meet you again."
    author: "Inorog"
  - subject: "Thanks David..."
    date: 2001-02-06
    body: "... for your OSDEM presentation: good presentation with a good presenter! I agree and underline also with one idea: **interoperability**. This will be a very important feature for KDE to have that, when API of 1.4 Gnome will be stables.\nAnd YES, KDE2.01 is used everyday as default without problem. For all of that Thanks David, and all KDE developpers!"
    author: "Nicolas"
  - subject: "Re: Thanks David..."
    date: 2001-02-06
    body: "Hmm, I just can't resist to tell everybody that I use 2.1 since the first beta. Ususally I compile KDE head at least every 2-3 days. It rocks already, works very stable and doesn't give me much trouble at all. As far as I'm concerned people will love it. Konqueror rocks. KMail is my primary Mail client since the days of KDE 1 and it has improved a lot. Kicker is great and since I'm trying to learn to program for KDE, I'm very impressed with the thought that was put into library development. I very often get the feeling that most everything you need is there in a rather easy to use way. \n\nI'd really love to see another OSDEM in Brussels since it would just be a 2.5 hour drive from where I live but I just couldn't bring up the time this year, sigh. \n\nFinally, keep up the good work all KDE developers and thanks a lot for the superb desktop environment. I really can't imagine how live was poosible before KDE ;)\n\nCheers,\n\nRichard"
    author: "Richard Stevens"
  - subject: "Re: no Euro support"
    date: 2001-02-06
    body: "The lack of Euro support is looking increasingly serious for Europeans, as its just 11 months till we use the new currency.  I'd love to find a solution to this soon..."
    author: "Simon"
  - subject: "Re: no Euro support"
    date: 2001-06-11
    body: "I DO NOT WISH TO JOIN THE EURO\nI WOULD ALSO LIKE TO SEE THE HUMAN RIGHTS THING\nABANDONED\nALSO ABANDON THE EURO ARMY E.T.C"
    author: "GEORGE MALONEY"
  - subject: "Re: no Euro support"
    date: 2003-01-27
    body: "Europeans are smelly, why would we wanna join their nazi empire?\nNo euro for Great Britain!\n\nhttp://www.bnp.org.uk - Vote for freedom, vote for Britain, vote for the BNP!\n\nhttp://escape.to/gblr"
    author: "Great British"
  - subject: "Re: no Euro support"
    date: 2005-02-17
    body: "you stupid asshole, cockbite, sucker"
    author: "jay"
  - subject: "For the attention of David"
    date: 2001-02-06
    body: "First of all, Thanks for giving us KDE2 and kontinously developing it. Its a divine produkt.\n\nI am not an ekspert at anything. But I just want to know why there is no effort to get KDE working on QT embedded as opposed to X.\n\nI am not a koder, Just a big fan, If I was I could have started koding the 'port' myself.\n X11 always lets me down. Network transparency? I can do without it. X applications? with Koffice and the Kompany, I wont need X apps.\n\nI heard QT embedded already has hardware acc. So why cant it be done.\n\nLet Kde kontinue going where no man has gone before.\n\nKonqi would really shine without X11tra baggage.\n\nCheers"
    author: "Psycogenic"
  - subject: "Re: For the attention of David"
    date: 2001-02-06
    body: "A KDE for Qt/Embedded is definitely a good idea.\nBut I personnally have no time for starting something like that - and I guess that the developers interested in such a thing would be those who have an ipaq or other embedded device.\n\nI suppose this will happen somewhen, but not from me :)"
    author: "David Faure"
  - subject: "Re: embedded KDE2"
    date: 2001-02-06
    body: "Hi, \n\nI've seen embedded QT on an ipaq. Quite impressive. However, there were only very little applications. Has anybody thought about a down-sized KDE2 distribution based on embedded Qt suitable for PDAs? I know Konqi was already ported. \nIt would definately rock! Look at www.handhelds.org where they are trying to build a X-based Linux distro for PDAs from scratch. We could probably do much better without X as we only have to shrink-size exsiting KDE apps and make them use embedded Qt. Anyone interested in a project like this? \nChris"
    author: "Chris Naeger"
  - subject: "Re: embedded KDE2"
    date: 2001-02-06
    body: "What would be a good start is to port Koffice/Kmail to QT embedded, that would really be cool...would give us a lot of choices.."
    author: "t0m_dR"
  - subject: "Re: Impressions from Linux Expo Paris and OSDEM Bruxelles"
    date: 2001-02-07
    body: "Hi!\n1.) Reading all the postings I want to draw your attention to MS-Outlook. \nUntill importfilters for the pst files (mail, calendar, todo, contacts) are available it will be difficult to convince users to switch.\n\n2.) The task of switching from MS-Windows/MS-Office/MS-Outlook to Linux/KDE has to be automated as it is everything else than easy and time consuming. \nImagine the task of the sysadmin of a middlesized or bigger organization to switch all the clients at the same time ..... - and nobody would like to leave this task to endusers.\n2a) because of this (impossibility) KDE has to be capable to import/export MS formated files. \n2b) For workstations (at home) without fileserver conection and mass backup possiblities, there must be a easy way to save the data (keep a temporary FAT16 partition on a harddisk with the necessary backup data)\n\nI would imagine that the Linux/KDE setup program reads the necessary settings (workstation and user(s) specific information, printer, samba ...) out of the MS-registry, repartitions the harddisk, moves the files/directories to be saved on the backup partition, installs Linux/KDE on the primary partition, imports the previous collected settings, moves the data from the backup partition to the linux partition and integrate the backup partition into the linux partition. (Of course the backup partition could also be a another backup media - CD-RW, tape).\n\nthats all ;-) - migration finished in 2 hours, (almost) no interaction necessary except feeding th CD-ROM of the distribution.\n\ncu\nferdinand"
    author: "Ferdinand GAssauer"
  - subject: "Re: Impressions from Linux Expo Paris and OSDEM Bruxelles"
    date: 2001-02-07
    body: "At least importing Outlook Mail (and adress book) is possible using KMailCvt (www.hum.org)."
    author: "Anonymous"
  - subject: "a question about a filter of xls files to html "
    date: 2002-03-14
    body: "hi, i have a question, if you could help me....\ni have installed linux at machine (mandrake) and i want to find a filter of .xls files (ms excel) to html files or txt files.\nDo you know where can i find it? i don\u00b4t know how to make this script myself.\nThanks in advance.\n\nesperanza."
    author: "esperanza"
---
All-around KDE guru <A HREF="mailto:faure@kde.org">David Faure</A> (<A HREF="http://perso.mandrakesoft.com/~david/">website</A>) recently attended the Linux Expo in Paris and subsequently the <a href="http://www.osdem.org/">OSDEM developer meeting</a> in Brussels.  Below he gives us his very interesting impressions of the events, especially how they relate to our favorite desktop.






<!--break-->
<P>&nbsp;</P>
<P>
<H3>Linux Expo Paris</H3>
</P>
<P>
The Expo organizers offered KDE a free booth. Not a big one - I would even say
that it was probably the smallest booth at the whole show :). But the booth was quite nice, with two computers (kindly provided by <A HREF="http://www.mandrakesoft.com/">Mandrakesoft</A>), two <EM>huge</EM> KDE posters (kindly printed by people at the <A HREF="http://www.hp.com/">HP</A> booth, after Gerard Delafont, the French translator who set up the booth, convinced them that they were under-utilizing their printers ;) ). We also had many papers to give away about KDE, prepared by the French "translation" team, but which also does a very good job of promoting KDE. They even had prepared a really nice press kit, which I think
should be translated into English now, so that we can get more journalists to write about KDE.
</P><P>
The booth in front of us was full of books, and I was happily surprised to see
the <A HREF="http://www.andamooka.org/kde20devel/index.shtml">KDE 2.0 Development</A> book there - for which I wrote the KParts chapter - as I had not seen it in its final form yet.
</P><P>
As usual for some time now, KDE could be seen at almost every booth. As someone said, the small KDE booth wasn't our point of presence, but the whole Expo was:). And we had many, many visitors at the booth (except the day when there was
a strike of the metro company. Don't get me started about Paris!). KDE is definitely used by most of the Linux users in France - and the fact that Linux-Mandrake represents 70% of the French market now (in terms of Linux boxes sold) certainly helps.
</P><P>
Meeting with happy KDE users is very refreshing. The developer point-of-view on the software we release is quite negative on the whole, since we mostly hear about bugs and problems, not so much about all the happy users :). So I was quite reassured about the quality of our releases after many people told me that they were using KDE 2.0[.1] every day, and had no trouble whatsoever with it. Of
course I got a few bug reports (and, hmm, some are quite grave, like AltGr not
working at all in KDM and no way to display the Euro symbol). What comes out of discussing with many people is the obvious need for an office suite under Linux, and this instills me with the idea of starting to work on <A HREF="http://www.koffice.org/kword/">KWord</A>. The need for MS Office documents filters is urgent as well, of course. It seems that attending this Expo convinced Laurent Montel to start looking at the MS Excel filter for <A HREF="http://www.koffice.org/kspread/">KSpread</A>, which is good :).
</P><P>
I also met many developers interested in using KDE as a development platform. Many more than I expected, in fact - which is very good. There may be a need for translated API documentation in KDE and Qt. Having English-only documentation
seems to be a problem for some (or many, that's difficult to say) French developers. I talked to <A HREF="http://www.trolltech.com/">Trolltech</A> about that, we'll see if something happens. In any case, <A HREF="http://www.kdevelop.org/">KDevelop</A> is of great help for many. To attract more developers to KDE, spreading the word about how powerful the KDE/Qt architecture and libraries are is very important. I might write articles about this again, because it's really
the best way to tell people about it. I was impressed to see that many had read the articles I wrote a year ago!  And it's another way of providing development support material in French.
</P><P>
On Friday I gave a talk about KDE, the usual talk about overall features and most used applications. It went well, although the room wasn't even half-full, due to the talk starting quite early (9am). No technical problems for the first time - thanks to the fact that <A HREF="http://www.ibm.com/">IBM</A> provided me with a really nice laptop a week ago. One thing that I emphasized was the fact that we were welcoming developers to join the project, and I particularly talked about the need for people working on <A HREF="http://www.koffice.org/">KOffice</A>. I realized that most of those who would like to help, need to realize they don't have to be a genius like <A HREF="http://boch35.kfunigraz.ac.at/~rs/">Reggie</A> to help with KDE/KOffice :), and that it's always possible to start with small bug-fixes before grasping everything. And indeed, we might have "recruited" a couple of new developers, as well as translators!
</P><P>
One thing about presentations: never hack <A HREF="http://www.koffice.org/kpresenter/">KPresenter</A> the night before a presentation :).  When you realize at 2am that you don't have it working , you're in trouble :).
</P><P>
<H3>OSDEM (Brussels)</H3>
</P><P>
The <A HREF="http://www.osdem.org/">OSDEM</A> is an open-source developers meeting that took place in Brussels last weekend. It was really interesting. A developer meeting is very different from the usual trade shows. Even though the audience was quite large, it really seems like most of it were developers, and the talks were definitely technical, which was quite interesting.
</P><P>
The presence of a hacking room is also a good hint about the type of event it was :).  Speaking of which, I'm quite impressed by how much <A HREF="http://www.valinux.com/">VALinux</A> did for that event. It seems they sponsored most of it, provided the hardware for the hacking rooms and paid for journalists to come over to the event. I was really surprised by how many journalists where there
- and shortly after my arrival I learned that I had an "interview schedule", to meet most of them during the two days :). This was quite interesting, the journalists being interested by very different aspects of KDE - one of them even already knew a lot about KDE, including DCOP and the kdcop tool :). The journalists came from Germany and from the UK (quite ironical for me, as I am from France and live in the UK now :)), so expect articles about KDE and/or the OSDEM in
German and British magazines.
</P><P> 
The downside of those interviews was that I didn't have much time for attending the conferences. I only went to the <A HREF="http://www.mozilla.org/">Mozilla</A> talk, and to the <A HREF="http://www.gnome.org/">Gnome</A> one :). The first one - I guess I shouldn't comment on the second one :) - made me realize that Mozilla and <A HREF="http://www.konqueror.org/">Konqueror</A> are not really competitors, because their goals are quite different. Konqueror is IMHO the browser everyone needs right now under Linux/Unix :), whereas Mozilla is the development of a cross-platform browser (and mail client etc.), with all the trouble
that cross-platform means.  For instance the fact that this involves developing your own toolkit (in the case of Mozilla it's the XUL/XBL framework), to define widgets from some XML description, etc. So when the Mozilla developers are tired of being asked "But when do we have a working browser under Linux ?", they
should point people to Konqueror :).
</P><P>
Later on I actually met with the Mozilla guys in the hacking room, and we discussed various things, from a specific infraction to the DOM specification to the bookmarks implementation. As you might know, Konqueror now uses the <A HREF="http://pyxml.sourceforge.net/topics/xbel/">XBEL format</A> for its bookmarks, a
format that was defined by the Python XML SIG group. Mozilla uses some RDF format (at least internally, and apparently because many other things use RDF in Mozilla), but hopefully they may be convinced to use something like XBEL ;).  It's true that a very good incentive for that would be if it were a <A HREF="http://www.w3.org/">W3C</A> standard.
</P><P>
My talk was Sunday morning. It was a presentation of KDE as a development platform. I presented most of the technologies provided by KDE's libraries, and one
hour is even short for talking about all that :). The evening before, I added some slides about out-of-process embedding and <A HREF="http://trolls.troll.no/~lars/xparts/doc/xparts.html">XParts</A>, after discussing with <A HREF="mailto:Thomas%20Capricelli%20%3Corzel-at-kde.org%3E">Thomas Capricelli</A> and <A HREF="mailto:Philippe%20Fremy%20%3Cphil-at-yalbi.com%3E">Philippe Fremy</A>, the <A HREF="http://aquila.rezel.enst.fr/thomas/vim/">KVim</A> developers, and realizing
that this new technology was missing from my presentation.  Thanks much to the
XParts developers for their very good white paper :). It's great to have XParts, not really for our own use, but so that people can start dreaming again about integration with other object models. I also hacked KPresenter a bit more (did I mention that this is always a bad idea right before a conference ? :)
</P><P>
The room almost full (IIRC it was a 150-people room), and the talk went well. As for all the presentations done during that event, there were 2 or 3 cameras,
filming the whole presentation! I was told that the videos would be available on the web (in a streamed format) soon. That would be really great and helpful for new developers, to have a video presentation of KDE's technologies.
</P><P> 
I enjoyed this meeting a lot, and I'm looking forward to the next OSDEM, since
I was told there may be one next year. If only it could take place in a city where it doesn't rain all day, and if there could be a bit more KDE developers, it wouldn't hurt :)
</P><P>
David Faure





