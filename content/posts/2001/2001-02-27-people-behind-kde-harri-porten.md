---
title: "The People Behind KDE: Harri Porten"
date:    2001-02-27
authors:
  - "numanee"
slug:    people-behind-kde-harri-porten
comments:
  - subject: "When will KDE 2.1 be out?"
    date: 2001-02-26
    body: "When is it comming? I can't wait!!! EAK!\n\nHorst"
    author: "horst"
  - subject: "Re: When will KDE 2.1 be out?"
    date: 2001-02-26
    body: "I believe everything's set but seems there was a delay with propagating stuff to our mirrors.\n\nI'm guessing the announce will be tomorrow morning.  ;)\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: When will KDE 2.1 be out?"
    date: 2001-02-26
    body: "its out!\nftp.kde.org (is full - no chance)\n\nfast server in germany\nftp://bolugftp.uni-bonn.de/pub/kde/stable/2.1/distribution/\n\nor look at \nwww.kde.org/mirrors.html\n\n-gunnar\n\n...and it rocks again!!!!"
    author: "gunnar"
  - subject: "Re: When will KDE 2.1 be out?"
    date: 2001-02-26
    body: "Got it! Thanks!"
    author: "reihal"
  - subject: "[OFF TOPIC] Outage"
    date: 2001-02-26
    body: "Care to comment on the recent outage? Just curious... I didn't realize how often I check this site until it went down. ;) You do a great job here!"
    author: "kdeFan"
  - subject: "Yes, please..."
    date: 2001-02-26
    body: "<P>Seeing as I also use Zope for websites, I would also like to know what the story was on the downtime. If it's not too sensitive, would you mind sharing it with us?</P>\n\n<P>Thanks again for the site! The demand for something like this was really big; I'm glad it came along.</P>"
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Re: Yes, please..."
    date: 2001-02-26
    body: "It's nothing to do with Zope and it's almost entirely not our fault.\n\nBasically, our upstream network service provider (ISP) let us down (and most if not all of their other clients) for reasons that are not entirely clear.  Since our backup line went down a couple of weeks ago, we were left stranded without network access.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: [OFF TOPIC] Outage"
    date: 2001-02-26
    body: "Yeah, I noticed the outage too.  In 2.1 beta2 there's a little news ticker applet for Kicker (very cool) that can automatically check for new news once every half hour here at dot.kde.org. When the site is down and it tries to check for news, it pops up a little error dialog.  I usually leave my computer on all night, so when I check it the next day sometime, I have like 20 error dialogs to click through!  I'm glad it's back up now.  (I know I could just make it stop checking while the site's down, but I'm too lazy :)"
    author: "not me"
  - subject: "Re: [OFF TOPIC] Outage"
    date: 2001-02-26
    body: "That annoying behaviour with those lots of error message boxes has been fixed for the version which is going to be released with KDE 2.1."
    author: "Frerich Raabe"
  - subject: "I liked all his books."
    date: 2001-02-28
    body: "Especially the first one with the Sorcerer's Stone."
    author: "Jim"
---
Harri Porten has been very busy during the last few months with JavaScript in KDE. Despite all the hard work to get things ready for KDE 2.1 
he found some time to do <a href="http://www.kde.org/people/harri.html">this interview</a> with Tink on <a href="http://www.kde.org/people/people.html">The People Behind KDE</a>. <i>[As previously featured on the <a href="http://www.kde.org/">mother site</a> during our unscheduled network downtime.]</i>

<!--break-->
