---
title: "CVSSearch, KDE Studio Gold, KSourcerer.org"
date:    2001-01-24
authors:
  - "numanee"
slug:    cvssearch-kde-studio-gold-ksourcererorg
comments:
  - subject: "And what is the eye candy distro?"
    date: 2001-01-24
    body: "I forgot to say that I was using <b>debian/sid</b> as my linux distro. The Kde/Debian package maintainer <a href=\"http://kde.tdyc.com\">(Yvan Moore)</a> does a huge and fantastic job! The support of Kde in debian is simple awesome :).\n<p>\nBTW I have hacked the kdevelop 1.4 debian package creation script. So who's interested in kdevelop 1.4 (port of Kdevelop1.2 to kde2.1) for sid ?"
    author: "Christophe Prud'homme"
  - subject: "Re: And what is the eye candy distro?"
    date: 2001-01-24
    body: "*raises hand*\noh oh oh, gimme gimme gimme :)\n\nErik"
    author: "Erik Severinghaus"
  - subject: "Re: And what is the eye candy distro?"
    date: 2001-01-24
    body: "Please commit the deabian stuff into CVS in the KDEVELOP_1_4 tag ;-) That saves double work eventually later."
    author: "Ralf Nolden"
  - subject: "KDE Studio Gold sounds GREAT!"
    date: 2001-01-24
    body: "I've already ordered KDE Studio Gold - I'm quite excited about the prospects of a fully funtional, well integrated and documented Open Source IDE that comes nicely packaged for many distros.\n\nIf that's not worth a few bucks, what is?"
    author: "Ben Hall"
  - subject: "Re: KDE Studio Gold sounds GREAT!"
    date: 2001-01-24
    body: "KDevelop?"
    author: "Anonymous Coward"
  - subject: "Re: KDE Studio Gold sounds GREAT!"
    date: 2001-01-24
    body: "(Stable) Choice?\n\nIf KDevelop matched KDE Studio's stability, then maybe I'd look into it again.  I have watched KDevelop since the beginning, I even installed it on a school lab back in Beta 2, but I've been checking out the 1.4 builds and it still has a ways to go until it's usable under KDE2.\n\nAlso, Shawn has been doing a lot for KDE Studio, and KDE on the whole.  Why not support it?"
    author: "Ben Hall"
  - subject: "Re: CVSSearch, KDE Studio Gold, KSourcerer.org"
    date: 2001-01-24
    body: "the website of theKompany.com is really slow\n\nmfg\n\n  aotto :)"
    author: "aotto"
  - subject: "Re: CVSSearch, KDE Studio Gold, KSourcerer.org"
    date: 2001-01-24
    body: "when we got slashdotted the other day we moved 10gig of data and got over 600,000 hits.  People are still trying to hit it, but it is MUCH better than it was.  We are working on getting it back to the way it was as quickly as possible.  Thanks for your patience."
    author: "Shawn Gordon"
  - subject: "KDE Studio Gold"
    date: 2001-01-24
    body: "<p>I generally think what theKompany is doing is really great. However I have a couple of questions and thoughts. I hope that someone from theKompany could answer them.</p>\n\n<H3>Thoughts and questions:</H3> \n<p>\nIt's definitely a very thin and hard line to walk if you want to make money and at the same time produce OSS software. As a pure software vendor it's even harder since your main revenue is supposed to come from the software it self.\n</p> \n<p>\nThe idea with producing stencils for Kivo is a really good one (I just hope they make enough money out of it). I would gladly pay to get nice stencils for e.g. network layout or flow control. Doing the stencils your self is boring and time consuming. Furthermore stencils can't produce bugs and errors (well they can indirectly but you have the source).\n</p> \n<p>\nIt is also a great idea to make a product that you will only sell and that isn't GPLed, but only as long as I get the source. I hope that theKompany take this approach with e.g. BlackAdder. Why do I want the source. It's mostly a support issue, see even if theKompany goes south (which I hope they don't) I will be able to support it for my own use or e.g. for the organization/company I work for.   In short if I find bug I will be able to fix it. I will also be able able to extend the program for my own use. If I want I can send the patches etc. to theKompany for inclusion in the product I bought. This approach is kind of a community license only the people/organizations how bought the program has access to the code and they aren't allowed to redistribute it however they are allowed to make changes in the code that they use.\n</p>\n<p>\nWhat I don't think is a great idea is to have extensions to a GPLed program and sell that under a non-GPLed license as with KDE Studio Gold. Why, well if I made a change or bug fix to the GPLed version it may lurk it's way into the non-GPLed version. I don't know if theKompany has included any external patches in the code base of KDE Studio. But if they have they can be in big trouble. Hence it's better to either stay GPLed or make a totally new program that isn't GPLed at all as e.g. BlackAdder. \n</p>\n<p>\nHmm, let me also make some remarks on Aethera (i.e. theKompany version of Magellan). I must say that I'm really impress with the client and I hope that what ever the difficulties are between the Magellan team and theKompany can be resolved. Anyhow to me it looks like a really slick and nice Groupware client which if it's of good quality will get wide spread use. However it's one thing that worries me and that's the backend server which they say will be non OSS. Such a key component as the schedule backend server non OSS make the use of it in a OSS desktop environment close to useless. 1: Either someone will write a OSS replacement 2: People will use a different client with a OSS backend server.</p>\n\n<p>\nRe Olof</p>"
    author: "User"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-24
    body: "Let me clear something up.  There isn't any non-GPL'd code in KDE Studio Gold.  We are selling the packaging, the enhanced online integrated help system and enhanced documentation.  Those are not for free.\n\nYou know, I just can't seem to win on this topic.  If something is closed, everyone is up in arms, if something is open then no one thinks I can sell it because it will be pirated.  The solution I think, and it is similar to what you are suggesting with BlackAdder, is to have a license the provides the source code to the customer in a non-distributable form.  So they are safe if we go out of business, but they can't make use of it for anything other than their own purpose, they can't resell it.\n\nI would debate your point that Aethera is useless if the client is open and the server is closed.  The target market for people that are going to buy the server is small to medium sized companies.  I somehow doubt that Joe Blow sitting at home is going to want our server application.\n\nI've said it before that I'm experimenting with various models to see what works best and how we get supported, so it's up to the community to make the most open route the way that works."
    author: "Shawn Gordon"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-24
    body: "<p>I definately agree with you, Shawn. To the members of our community - *PLEASE* thoroughly think through your statements when critizing others. 90% of the trolling remarks on this forum come from people that don't fully understand the situation or just feel like expressing their \"0.02 cents\". That's fine, and nothing can stop you from doing so, but bear in mind that their are other people on the other end of the wire, and statements type at a computer screen are not always interpreted accurately on the the other end. If you would have read the FAQ for KDEStudio Gold on the product page, you can see very clearly that it is in fact a GPL'd program which does ship with the source.</p>\n\n<p>Unfortunately there is no solid business model for what theKompany is attempting to do - they are operating under an extremely hybrid market plan. I for one applaud every contribution they make, and also make it a point to help support them by purchasing every single product they release, and telling others about them. Maybe some of us cannot see just how critical theKompany has become to the very infrastructure of the KDE project; frustrating Shawn and the other developers with constant mutterings of ideology will only drive them off - there is *NO* requirement for them to do as much as they are for the commons, and unfortunately, I have witnessed each gift to the community met with heated criticism.</p>\n\n<p>I try to adhere to the Free Software mantra to the dot, but unfortunately if doesn't always translate to the realities of the current software market. Businesses need to make a profit, and \"giving away your assets and hoping people will still buy them\" is quite a risk, indeed.</p>\n\n<p>Shawn, I only hope the positive feedback that most of us express will encourage you to continue. The technology world would do well to examine your business model; even if Microsoft gives schools and libraries (like mine) 10-20 computers, and money to buy more Microsoft products, it's nothing compared to the committments you've contributed to us.</p>\n\n<p>Cheers!</p>\n\nEron Lloyd"
    author: "Eron Lloyd"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-25
    body: "Thanks for the support Eron, don't think we didn't notice that you had purchased everything :).  I certainly hope our ground breaking model pans out and we are able to continue to give back to the community.\n\nShawn"
    author: "Shawn Gordon"
  - subject: "[offtopic] But on a lighter, yet serious note..."
    date: 2001-01-25
    body: "<p>How's things in California? Are you guys affected by the power situation? How can a company, such as yourselves, keep going when IT relys so heavily on the almighty watt? Maybe we should all chip in, and move you guys somewhere more stable, like here in the Northeast =)</p>"
    author: "Eron Lloyd"
  - subject: "Re: [offtopic] But on a lighter, yet serious note..."
    date: 2001-01-25
    body: "We're fine in Southern California, it's Silicon Valley that sucks up the juice and keeps getting the rolling black outs.  I'm 15 minutes from the beach and 75 minutes from the snow - you can't beat it :)"
    author: "Shawn Gordon"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-25
    body: "Eron, i fully agree to your point of view\nand applaud Shawn and theKompany for their\nfantastic work.\n\nI think its a matter of responsibility (and\nlearning to deal with free software from a\nuser point of view) to understand that software\nand infrastructure needs funding. \nAt least as a business if you make use of this\nit should be natural to pay a reasonable amount \nfor it.\n\nIf this is going to fail it would be a bad sign."
    author: "Michael Marxmeier"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-25
    body: "I think that your business model of Releasing the program and source under GPL and selling the hardcopy in normal retail stores is great:)  I don't believe that this model will work for all software, but for development tools and other business related software I think this is the way to go.  Any company that will be using your software would be out of their mind to not buy the bundled product for documentation and simplicity.\n\nIt may seem that people are flaming you about selling opensource software or not releasing stuff for free(such as documentation), but I think that there are more people that agree with your system and just don't voice their opinions.  It seems that most people that disagree are somewhat confused/jerks/trolls/uninformed.\n\nMaybe in four years or so(when I get out of college) you could give me a job :) By then I should be a pretty darned good KDE programmer since I've already been doing it for six months.\n\nMatt Newell"
    author: "Matt Newell"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-25
    body: "<p>Ahh, good then I will login and buy my version of the enhanced packaged version of KDE Studio Gold. (PS: it would be a good thing to write that info in the KDE Studio Gold product page)</p> \n<p>\nI liked your view of BlackAdder if you do it like that I will on your website ASAP buying it. It gives me the freedom (fix and enhance the code for personal use) it's not the ultimate freedom but it's good enough. </p>\n<p>\nYes home user Bill probably don't care about the server he wants a nice little client. Hence for him Aethera will be the greatest. However I think that you will still encouter argument \"1\" which will make it hard to make money out of Aethera. Secondly it's such a vital part in any larger network and most OpenSource people operating in larger networks probably don't want to depend on a \"closed\" server.</p>\n<p>\nDon't take it that negative take it as a tip. Me saying that I don't think that's the best model. The BlackAdder (as suggested in your reply) and Kivo are good ideas but  the server part of Aethera is not. </p>\n<p>\nRe Olof</p>"
    author: "User"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-26
    body: "<I>\nLet me clear something up. There isn't any non-GPL'd code in KDE Studio Gold. We are selling the packaging, the enhanced online integrated help system and enhanced documentation.\n</I>\n\nAll right. Where can I download the KDE Studio\nGold? If it is GPLed, there should be some place\nto get it for free, shouldn't there be?"
    author: "KDE User"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-26
    body: "The GPL does *not* require that I give it away for free.  It requires that the source be available."
    author: "Shawn Gordon"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-29
    body: "<I>\nThe GPL does *not* require that I give it away for free. It requires that the source be available.\n</I>\n<P>\nSo, where is the source of KDevelop Gold? And\nmind you, the complete source must also include the source for all help files and so forth."
    author: "User"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-30
    body: "You must purchase it to get the source. Please read the FAQ. It's been up for a while."
    author: "Eron Lloyd"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-31
    body: "<I>\nYou must purchase it to get the source. Please read the FAQ. It's been up for a while.\n</I>\n<P>\nIf the program is GPLed, then once you buy it,\nyou can freely spread it around (clause #1 of\nthe GPL). Thus, my question stays: where can\nI get KDE Studio Gold? An FTP address should\nbe sufficient."
    author: "KDE User"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-31
    body: "<p>Gee, I dunno... maybe ftp.cheepo.com or ftp.takebut.dontgive.net</p>\n\n<p>Its very aggravating to see this mentality. It is this attitude that will hammer the final nails into companies that invest their all into Free Software. I imagine most \"Gimme more free software - it's GPL'd!\" lamers shop at Wal-Mart for all their needs, too. Tis' ashame they have no concept of \"responsible consumerism\". I could rant about this for a good long while, as I'm sure could Shawn, but I won't waste time fighting a flame-war. I'd rather be back home coding free software.</p>\n\n<p>Cheers!</p>\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-31
    body: "You're making a great argument for us to not use the GPL.  Do you think you have a right to others works?  Do you in turn donate your salary to your neighbor?  Do you have enough courage to sign your name?\n\nIf this is the common attitude then we will be forced to abandon the GPL and create either closed source applications, or use a restricted license that would expose someone that distributed our source to severe legal exposure.  Then of course using our software would require a verifiable registration process, and all this because you think you have a right to something for free."
    author: "Shawn Gordon"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-31
    body: "<I>\nYou're making a great argument for us to not use the GPL.\n</I><BR>\nAbsolutely correct.\n<P>\n<I>Do you think you have a right to others works?\n</I><BR>\nYes, if these works are GPLed. That's what the\nlicense says, anyway.\n<P>\n<I>Do you in turn donate your salary to your neighbor?\n</I><BR>\nNo, as my salary is not GPLed.\n<P>\n<I>Do you have enough courage to sign your name?\n</I><BR>\nYes, my name is Marat Fayzullin\n<P>\n<I>If this is the common attitude then we will be forced to abandon the GPL and create either closed source applications, or use a restricted license that would expose someone that distributed our source to severe legal exposure.\n</I><BR>\nThis is what, in my opinion, you will end up doing. I do not think that your GPL-based business plan is going to impress anybody. Nor do I think that you are going to make money investing them into GPLed software development.\n<P>\nOh, you probably already know all of this. Just waiting for your products to mature before you can safely close them off, as you have just done\nwith KDE Studio Gold (\"Yes, it is GPLed, but you\nhave to pay money for it\")."
    author: "KDE User"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-25
    body: "Hmmm...\n\nfirst off, as I've often said to Shawn, thanks for doing such a great job, and for putting up with the community as a whole.  I'm often surprised that people like Shawn (and the core KDE developers to a point) keep trying so hard with anonynous people like \"User\" taking pot shots at every turn.\n\nConvenience is worth a lot.  So is source.  So is further development.  You can't expect all three without putting something into it, be it money or voluntary development.  I'd much rather pay for a nicely packaged convienient GPL'd IDE than the same nicely packaged binary only IDE, or even the same product with a source-restriced license.  I really hope that KDE Studio Gold sells well, it'd be great to think that that model would work.\n\nA while ago I was trying to get my Palm Pilot to work under BeOS.  I had to buy a $15 piece of software.  I didn't begrudge the guy my $15, it was that I was essentially paying for nothing that really got to me.  Sure I could run his app, but that was it.  I honestly felt cheated.  Now, if that guy had offered it up for free and asked for donations, I'd like to think that other people than me would have sent him his $15.  \n\nDeveloping software is a time consuming process that's worth money.  What the money actually buys is the question.  Chipping in $20-$50 to gurantee continued development of a high qulaity piece of software that might not otherwise exist is a worthwhile investment.\n\nAs for Kivio stencils, I'd like UML, Networking, FSM, and ER diagram stencils..\n\nBTW \"User,\" is \"Re Olof\" anything significant?  The only thing I could think of was \"folO eR\"  backwards.\n\nBen"
    author: "Ben Hall"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-25
    body: "It's people like you three that we are counting on with our business model, so it's good to hear the support.  We'll do our best to continue to supply quality software for you :)."
    author: "Shawn Gordon"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-25
    body: "I think you're making too much of User's mistake. He wasn't really making potshots, he just forgot to read the FAQ (everyone does that once in a while). Besides, in the article he really praises theKompany's products. \n\nBTW : Say Shawn, when do you think Kivio will go stable? And what is your opinion on k3studio? Also, do you have any job openings?"
    author: "Carbon"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-25
    body: "Kivio is very very close, we've been working on the scripting portion recently.  I imagine we should just wrap up the main program and release some stencil sets then work on the scripting, but the scripting will also enhance the stencils, so we are in a quandry, but continuing to work all the same.\n\nI only looked briefly at k3studio, but I think it's brilliant.  I had on my long term todo list to do something like this as I've been doing 3d Animation for about 11 years since my Atari 1040ST (the guys who wrote 3D Studio did their work originally on the Atari, which I used).  If things ever settle down around here I want to spend more time looking at it.\n\nI wish I could hire all the fine people that ask me for jobs almost every day, but finances just don't permit at the moment.  Please feel free to submit your resume and salary requirements to jobs@thekompany.com.  We really do keep them and have gone back through and hired people from them or handed out some contract work."
    author: "Shawn Gordon"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-25
    body: "Er, what I meant by what do you think of k3studio, I meant it's flowcharting capabilities compared to kivios, and what you think of it's recently added ability to import kivio stencils."
    author: "Carbon"
  - subject: "Re: KDE Studio Gold"
    date: 2001-01-25
    body: "Wow, didn't even notice they had put either one of those features in.  Too bad they wasted the time with the Kivio stencils since that isn't the final format and the data is copywritten."
    author: "Shawn Gordon"
  - subject: "Re: CVSSearch, KDE Studio Gold, KSourcerer.org"
    date: 2001-01-24
    body: "There's one really easy way to do better for a screenshot:\n\nDump XMMS.\n\n:-)"
    author: "Neil Stevens"
  - subject: "Re: CVSSearch"
    date: 2001-01-25
    body: "Hi,\n\nOur main fibre trunk has been damaged, so\nthe site was unavailable yesterday.\nYou can still access prototype B\nat http://linux.cia.com.au/~shao/cvssearch.\nIn any case, the main url should work soon.\n\nPlease tell us what you think using the\nshort survey.  The tool goes beyond cvs\nannotate/blame and bonsai.  Contact us if\nyou would like to know more about how it works.\n\nAmir"
    author: "Amir Michail"
---
Amir Michail, creator of the <a href="http://codeweb.sourceforge.net">CodeWeb</a> data mining tool, is back with <a href="http://www.cse.unsw.edu.au/~amichail/cvssearch/">CVSSearch</a>, a tool that searches for code fragments using CVS comments. It will eventually index over 350 KDE applications and promises to be very useful.  Send your kudos and comments <a href="mailto:cvssearch@cse.unsw.edu.au">here</a>.  In other developer news, <a href="http://www.thekompany.com/">theKompany.com</a> has announced <a href="http://linuxpr.com/releases/3193.html">KDE Studio Gold</a>, a commercial release of KDE Studio (licensed under the GPL) following the hugely popular "vanilla" edition with several features and additions for professional KDE development.  See the <a href="http://www.thekompany.com/products/ksg/">webpage</a> for more details and for the limited time discount.  <a href="mailto:victor@ksourcerer.org">Victor Röder</a> wrote us that <a href="http://www.ksourcerer.org/">KSourcerer.org</a> needs your help and support.  The site has been rewritten in PHP, but both technical and content contributions are needed to get this site going.  
Developers fiddling with XML and DOM might want to check out a previous article on <a href="http://zez.org/">zez.org: about code</a> covering <a href="http://zez.org/article/articleview/28/">Parsing XML with Qt's DOM classes</a>.  Special thanks to <a href="mailto:pkej@ez.no">Paul K Egell-Johnsen</a> for having submitted this cool little item last year. ;-)  Finally, if you're craving a little eye-candy, check out <a href="http://dot.kde.org/980299214/candy.jpg">this screenshot</a> submitted by <a href="mailto:prudhomm@mit.edu">Christophe Prud'homme</a>.  It features the new about:konqueror (although the latest version has already been improved), knewsticker, licq with KDE integration, the XMMS dock status app, the kpilot daemon and the korganizer daemon; it uses the KDevHP theme.  Surely someone can do better?  ;-)

<!--break-->
