---
title: "Weekly Apps Roundup #1"
date:    2001-05-19
authors:
  - "Dre"
slug:    weekly-apps-roundup-1
comments:
  - subject: "Re: Weekly Apps Roundup #1"
    date: 2001-05-18
    body: "great work Dre! an apps roundup is definitely welcomed, at least by this KDE user... \n\ni remember the days when people used to moan about KDE not having good PR and communication channels. it seems that the reaction has been to attack it via traditional OSS means: volunteers picking up the slack and providing the information needed/desired. between the dot, the (new) app round up, tink's great interviews, the KC KDE, apps.kde.com, and the regular announcements by the project team re: releases i think it is now very easy to keep up with what KDE is doing in a nearly blow-by-blow manner. i would dare say that KDE just might have the best/most comprehensive communications between project and users in the open source world today.\n\nit's to the point that people even occasionaly moan on linuxtoday.com about hearing too much about KDE. \n\n/me chuckles\n\noh, and nick is a very cool guy working on a very needed project. if you have time, please consider pitching in some code... the installer already does quite a bit and it would be great to see it completed..."
    author: "Aaron J. Seigo"
  - subject: "Re: Weekly Apps Roundup #1"
    date: 2001-05-19
    body: "I see that a new version of aethera, kapital and rekall were released this week by the kompany. Very cool i've noticed that kapital is moving right along.Its kind of cool to see the adds for it on apps.\n\nCraig"
    author: "Craig"
  - subject: "KDE is great, but..."
    date: 2001-05-19
    body: "First, let me say that KDE is much better than GNOME. I use it every day. But I was browsing the source code today, like I do every day, and came across the following comment in the KWin source: [troll snipped]\n"
    author: "Former KDE user"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-19
    body: "> But I was browsing the source code today, \n\nI wonder which code you were browsing. Because it didn't appear in today's CVS nor did it appear in CVS from 12 days back:\n\ntackatx@D228:/usr/src/kde0705/kdebase > grep -i -r cock *\nkcontrol/TODO:The Dialog suffers from the cockpit-dilemma.\ntackatx@D228:/usr/src/kde0705/kdebase > cd /usr/src/kde1905/kdebase/\ntackatx@D228:/usr/src/kde1905/kdebase > grep -i -r cock *\ngrep: config.h.in: Keine Berechtigung\nkcontrol/TODO:The Dialog suffers from the cockpit-dilemma.\ntackatx@D228:/usr/src/kde1905/kdebase >\n\n> I am going to commit a patch to the GNOME CVS today \n\nAh, you are just trolling ...\nGo and troll somewhere else ...\n\nTackat"
    author: "Tackat"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-19
    body: "Good to see such strong Free Speech support, being a Free software community site and all.\n\nIt sucks when somene spends 15 minutes trying to share their thoughts with the community, then, all of a sudden realizes \"Oh, shit, All my words are belong to KDENews.\"\n\nKeep up the good work, admins."
    author: "James"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-19
    body: "Do you mean \"[snip troll]\"? Most people think freedom ends, where it starts to affect other people's freedom."
    author: "Lenny"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-20
    body: "That's not what it means to radical egalitarians."
    author: "Chris Bordeman"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-19
    body: "Why should we tolerate lies, slander and homophobia on this site?  Why should we tolerate articles meant to cause tension and hatred within our community, and especially articles that have absolutely zero basis in reality?  Who does this article help or benefit?\n\nIn fact, I should have deleted the whole article.  The only reason I didn't was because of Tackat's response."
    author: "Navindra Umanee"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-19
    body: "You're in the right, I think.  In fact, I think you should delete this whole thread.  Marking trolls as such or deleting parts of their comments is bound to stir up more controversy than just deleting them.  Responses to trolls are hardly ever relevant anyway, and they only serve to encourage more trolling no matter how scathing they are.\n\nOnly thing is, if you're going to do this, you should probably have a well-defined policy.\n\nP.S.  Pleeeeeeeease can we have HTML posting back?"
    author: "not me"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-19
    body: "We will.  Now that Gnotices has been upgraded, we're bound to follow too.  ;)\n\nThe thing is, our current version of Squishdot doesn't have secure HTML posting, so I either have to fix it or upgrade.  I'm going for the upgrade, but that also involves upgrading Zope. Also, we have made changes to the version of Squishdot that we are using so I'll have to port them forward.  \n\nI'll probably start on this tonight or tomorrow, and hopefully minimize the downtime (that is, if we don't run into hardware problems...)\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-20
    body: "homophobia??? That word reeks of little minded Political correct thought police. Please lets not make this site a political correct zone. \n\nCraig"
    author: "Craig"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-20
    body: "Fine, then ignore the word if you don't like it.\nOn Internet Explorer you can filter out websites with words that you don't like or can't handle.  Konqueror doesn't have this yet, because no-one has felt a need.\n\nHope this helps you in your situation."
    author: "KDE User"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-20
    body: "Sorry no windows on this machine but i think your advice would be better suited for the thought nazis's.\n\nCraig"
    author: "Craig"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-20
    body: "I think part of the point the other posters were making is that this bulletin board is for fruitful discussion of KDE.  Not for attacks of KDE, GNOME, or other environment.  Not for attacks of various social groups (including gays).  After all, some KDE software developers just might be gay, and you just might be using software written by a queer.  There are other venues for political commentary of either type on message boards elsewhere.  The KDE moderators are definitely limiting speech, but they have that right because this board is their property."
    author: "Air/Conditioner"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-21
    body: "Spreading lies and hate, that's what Nazis do usually. Calling people nazis, because they dont want to tolerate hate and lies, indicates that you got many things very wrong."
    author: "Lenny"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-21
    body: "No my friend nazi's spread lies and intolerance of others options and views. Nazi's imprisioned people for what they said. The also had an extreme intolerance of religious views while being lead my homosexuals and occultists. Just like todays liberals and there thought censorship. While is seems ok these days to make fun of the pope or other religious leaders as soon as someone pokes fun at the homosexual agenda the libs come out of the wood work like cockoaches screaming homophobe homophobe. I just don't have tolerance for the thought nazi's and there intolaerance of anything not PC.\n\nCraig"
    author: "Craig"
  - subject: "Enough is enough."
    date: 2001-05-21
    body: "Sorry, but I'm starting to get tired of this.  You don't even know half the facts, yet you rant like a crazy lunnie at me.\n\nAs for your misguided rant, the troll is ACCUSING people of more than one type of discrimination and presents LUDICROUS and FALSIFIED proof of its statements, just to get people like you out of the woodworks.  These kinds of lies, falsity and obvious trolls we do not tolerate here, period.  Sorry, there is no discussion about it.\n\nIf you don't like it, well apparently there's nothing I or you can say to change that. Why not go to Slashdot for the kind of stuff you are looking for?  They are extremely open and even have a forum dedicated to trolls (see the trolltalk forum).  You can also try www.geekizoid.net when they come back."
    author: "Navindra Umanee"
  - subject: "Re: Enough is enough."
    date: 2001-05-21
    body: "lol I was'nt supporting the trolling efforts of the post i just hated your PC liberal to the rescure routine.\n\nCraig"
    author: "Craig"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-22
    body: "Ok, my last reply on this thread. Craig, so you think the police is commiting a crime, putting murders and thefts into jail? Hey, they are cutting their freedom to hurt and murder people. police is baaaad. Get a clue."
    author: "Lenny"
  - subject: "Re: KDE is great, but..."
    date: 2001-05-22
    body: "O man don't i feel stupid. I did'nt realise that some poor innocent homosexual was robbed and murdered here on the dot. I better watch what i say in the future. Boy do i feel bad. \n\nCraig"
    author: "Craig"
  - subject: "playing around..."
    date: 2001-05-21
    body: "Strange, strange,... Very strange.\n\nI am used to check regulary these pages, long time without such kind of words. Suprisigly enough, I chek also Gnome web site... and found similar comment (undigned post or obviously missigned one). Just guess that someone is playing arround to get Gnome and KDE people fighting?\n\nLets do like gnome people, ignore that stuff\n\nStephane"
    author: "stephane PETITHOMME"
  - subject: "Re: playing around..."
    date: 2001-05-21
    body: "He's a Craig Mundie's follower. They just dont like GPL. Well, with Qt they can choose to pay ;-)"
    author: "V\u00edctor R. Ruiz"
  - subject: "Re: Kivio"
    date: 2001-05-20
    body: "On the last version I compiled I had the problem that I could not connect anything.\nAm I too stupid to find out how to do this or is it a bug?\nAnybody the same problem?\nBut still looks like a great tool."
    author: "Michael"
  - subject: "Re: Kivio"
    date: 2001-05-20
    body: "Okay, I saw it on \"Kernel Cousin KDE #9 For 15 May\"."
    author: "Michael"
  - subject: "Re: Weekly Apps Roundup #1"
    date: 2001-05-20
    body: "I was wondering... is there a chance that killustrator and kivio could be merged? They do very similar things in many situations, that is, they are both mainly tools to graphically represent something in an organized way. Wouldn't it be nice if you could use certain kiilustrator features (blending, curves) in kivio, and certain kivio features (stencil organization, connectors) in kivio? Just a thought"
    author: "Carbon"
  - subject: "fixed kivio"
    date: 2003-01-09
    body: "Michael: Indeed it was a problem with the kivio version I had too.. major bug if you'd ask me.\nkoffice 1.2.1 fixes the problem\n\ncan't find killustrator... stupid adobe causing problems..."
    author: "Funk"
---
Some time ago I decided to start a weekly column, <EM>Apps Roundup</EM>,
to summarize some of the interesting things happening with KDE applications.  This inaugural issue features status updates
for <A HREF="http://devel-home.kde.org/~kate/">Kate</A>,
<A HREF="http://www.thekompany.com/products/kivio/">Kivio</A>,
<A HREF="http://lisa-home.sourceforge.net/">LISa</A> and the KDE Installer
Project.  As I plan to make this a regular column, please <A HREF="mailto:dre at kde.com">email</A> me if you know of an app that should be featured here.
<!--break-->
<br><br>
<STRONG>Kate</STRONG>.
<A HREF="http://devel-home.kde.org/~kate/">Kate</A>, the KDE MDI programmer's
editor based on <A HREF="http://apps.kde.com/uk/0/info/id/521">KWrite</A>,
has a new <A HREF="http://devel-home.kde.org/~kate/">homepage</A> which now
includes a
<A HREF="http://devel-home.kde.org/~kate/developer/pluginhowto.php">how-to</A>
on writing Kate plugins.  The idea is to keep Kate light-weight and add support
for things like CVS support and project management in plugins.
</P>
<P>
<STRONG>Kivio</STRONG>. 
<A HREF="http://www.thekompany.com/">theKompany.com</A>, which has contributed
<A HREF="http://www.thekompany.com/products/kivio/">Kivio</A>
(<A HREF="http://dot.kde.org/971885798/">story</A>), a Visio<SUP>tm</SUP>-like flowcharting program for KDE, to <A HREF="http://www.koffice.org/">KOffice</A>,
has <A HREF="http://linuxpr.com/releases/3687.html">launched</A> a <A HREF="http://www.thekompany.com/products/kivio/ksdp/">Kivio
Stencil Developer Program</A>.  This is an opportunity for stencil developers
to earn some cash (but of course the stencils become the property of theKompany).
</P>
<P>
<STRONG>LISa</STRONG>. 
Some of you may have noticed that
<A HREF="http://lisa-home.sourceforge.net/">LISa</A>, the LAN Information
Server, shipped with KDE 2.1.1
(<A HREF="http://lisa-home.sourceforge.net/img/lan1.jpg">screenshot</A>).
Since that release the developers have fixed a number of bugs, improved
the control module and added documentation.  Currently only a KDE client
is available for LISa, though the architecture supports clients using any
GUI toolkit.
</P>
<P>
<STRONG>KDE Installer Project</STRONG>. 
On the troubled waters front, we
<A HREF="http://dot.kde.org/982158722/">reported some
months ago</A> that <A HREF="mailto:nbetcher@usinternet.com">Nick Betcher</A> (aka Error403) had launched the KDE Installer project.  Nick reports making
large strides but he has not received the help he needs to keep this
project going.  Please drop him a note if you can contribute to this fun project.