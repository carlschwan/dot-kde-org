---
title: "KOffice Suite Beta Released"
date:    2001-04-25
authors:
  - "Dre"
slug:    koffice-suite-beta-released
comments:
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "Congrats. The filtes are still not up to Star Office 5.2 standards, but it looks nice and starts fast.\n\nEmbedding kword views in konqueror crashes unfortunately...\n\nWorth checking out and I look forward to much constructive critisism...."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "This is about the tenth time the dot people have misused the word alpha, you can't release alpha software, once you release it, it's beta.  Please stop saying that your releasing alpha software, it's just wrong."
    author: "ac"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "Actually, we can release even worse than alpha :-)\n\nSince the code is always there, we can not have private alphas. Consider this a \"technology preview\" if you want a trendy term.\n\nMe, I will just keep on using alpha ;-)"
    author: "Roberto Alsina"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "Another accurate term would be \"pre-release canidate.\""
    author: "ah"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "I think a release candidate comes after the betas. What used to be called a gamma in the old days."
    author: "Roberto Alsina"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "> you can't release alpha software, once you release it, it's beta.\n\nThat may be true in the proprietary world, but things are a bit different in the open source world.  Open source developers do not have a \"Testing\" department to shoot code to.  Instead, they rely on the community of interested users for testing.  By labeling something as alpha, the developers are effectively saying they are looking for testers.  It's a way for non-developers to contribute to KDE by reporting bugs (and hopefully patches as well), without having to compile from CVS."
    author: "Dre"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "Than what exactly does \"beta\" mean ?"
    author: "not me"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "I think the definition of \"alpha\" and \"beta\" are quite easy:\n\nalpha:\nThis _should_ work in most parts, but _must not_. Have a look at it, send in bugreports especially for critical bugs and patches, look at its brightness and be happy (aka technologie preview, as stated before)\n\nbeta:\nThis _should_ work. If it doesn't, it should\nbe reported immediatly. Report anything that keeps you away from normal/productive work (bugs, annoyances, etc).\n\nHTH,\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "Alpha means \"in the lab\", or in the case of software, \"being used by the developers only\".  Beta means \"out of the lab\", or in the case of software \"being used by non-developers\".  Anouncing a pre-release of software, not to developers, is generaly asking people to use it(non developers), this makes the release a beta release.  Open source kind of makes most OS software available to anyone, but you generaly don't want people to report bugs on it or something, because you probably know all the obvious bugs, when you want people to test it, you make a nice tarball and anounce it as beta, and then take bug reports."
    author: "ac"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "Language evolves.  Get over it.\n\nOver time in the open source world, alpha has come to mean \"software that doesn't work for anything yet, but it compiles and runs so we're letting people try it out if they really want to.\"  Its just a step down from beta, which is \"software that works but still has a lot of bugs so we're releasing it and people can send in bug reports.\"\n\nMaybe that's not what alpha used to mean, but that's the generally understood definition nowadays.  Since you (and everyone else who read this) understood what the article was saying, does it really matter that much?"
    author: "not me"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "But that's wrong.  Your confused as well.  OSS people are the only people that get that wrong, that's like 1-2% of the world, you'll look like an idiot one day when you tell your boss that your going to release an alpha copy of some software so the clients can test it.  He'll know what you mean, but you'll still look stupid."
    author: "ac"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-26
    body: "This makes no sense. OSS people are wrong becuase we use a word in a way specific to OSS?\n\nEnglish is weird, and one of the strangenesses about it is words with multiple defintions. If my boss (in the situation you presented) knows what I mean, and I know what I mean, then where exactly does the problem lie?"
    author: "Carbon"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "But that's wrong.  Your confused as well.  OSS people are the only people that get that wrong, that's like 1-2% of the world, you'll look like an idiot one day when you tell your boss that your going to release an alpha copy of some software so the clients can test it.  He'll know what you mean, but you'll still look stupid."
    author: "ac"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-26
    body: "This whole debate on alpha and beta demonstrates the importance of terms such as signifier and signified, as well as some of the classic debates on the public/private distinction (Richard Rorty for example).  \nSuch debates are certainly important in sociology, political philosophy and indeed most social sciences, especially those involving textual analysis.  They are of less importance to those involved in open source software development (unless of course our signifiers and signified are so out of whack as to make everything non-sensical or completely misinterpreted).  My thoughts are let the software developers do software development, and social scientists discuss textual analysis.\nNice to see that KOffice is continuing to develop.  A question - is it likely that KLyx, or when Lyx becomes GUI independent, Lyx compiled for KDE, is going to be part of the KOffice suite?\nNot that it matters - just curious."
    author: "Aaron"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-30
    body: "I dont' know if you are trolling but that is the most insightful and funniest thing I have ever read in the comments to a news posting.\nI dont think that these debates should be limited to sociology though, I just went through a sociology degree and nobody understood a damn word of what I said about open source/Free software and why it was important, let alone the implications of the language used in the discussion forums (and my thesis suffered for it).\nWhy shouldn't the developers engage in a little textual analysis? It might help give another perspective to more people on how open source is changing software development. In an environment that is almost completely textual I would have thought this would be more popular.\nOh well, I look forward to checking out KOffice and KLyX would/will be cool to."
    author: "Mark"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "Correct."
    author: "not me"
  - subject: "Changes in file format?"
    date: 2001-04-25
    body: "After updating the KOffice-Version shipped with SuSE 7.1 (or was it 7.0?), KSpread crashes when opening spreadsheets created with the \"older\" version. The underlying KDE is 2.2alpha. Has anybody else experienced this problem?"
    author: "AC"
  - subject: "New Screenshots?"
    date: 2001-04-25
    body: "Most Screenshots in koffice website seem to be old and outdated. Most people, including me, get the first impression about any software by looking at screenshots.\n\nSo how about some new nice screenshots?"
    author: "Janne Kylli\u00f6"
  - subject: "Re: New Screenshots?"
    date: 2001-04-25
    body: "Go ahead and make some :)"
    author: "Lukas Tinkl"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "Great stuff. Kspread Excel filters are working very well for simple documents, Kword filters still weak though. We need export filters!! KOffice is overall a very good suite, keep on the good work."
    author: "Pedro Ziviani"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "Yes! We need export filters for msoffice world formats!!! msoffice has becoming a standard in any workplace, so koffice has to speak msoffice languages to progressively beat it."
    author: "Alex"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-26
    body: "I agree. However, in the area of word processing, the utmost priority should be filters for RTF. Although proprietary formats such as MS-\"moving target\"-Word have some features RTF doesn't, for simple documents involving shapes, images, and standard formating, RTF is readable and writable by :\n\nWord\nWordPerfect\nStarOffice\nAbiWord\nOpenOffice\nWordPad\n\nand pretty much every other word processor out there."
    author: "Carbon"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-25
    body: "save the file as RTF and rename it to .doc."
    author: "Evandro"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-26
    body: "You can't possibly be serious.\n\n1) RTF has nothing to do with Winword format\n2) Winword can open rtf files w/o modification anyways!"
    author: "Carbon"
  - subject: "Export filters"
    date: 2001-04-26
    body: "I think the point is that saving in RTF format is a good way to share Kword documents with coworkers, and renaming to .doc is less likely to confuse people not familiar with RTF.\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: Export filters"
    date: 2001-04-26
    body: "RTF is good, don't get me wrong. The problem though, is that changing the extension to doc can (and has, in my experience) confuse Word as to the file type. Windows (98 at least) relies entirely upon extensions to identify file types."
    author: "Carbon"
  - subject: "Re: Export filters"
    date: 2001-04-26
    body: "Windows does not use extensions only to identify file types per se. It uses the extensions to associate a file with an application. So for a basic file type you can rename your extension to anything you like provided the associated program is capable of opening it. Thus for people without Word both rtf and doc will open wordpad. Renaming possibly may not work with more complicated apps like Word because they use dynamic data exchange in their file associations but I believe it would work. Renaming an RTF file to DOC will still open either wordpad or word which will use the appropriate filter.\n     RTF is a full text based tag format (like a primitive XML in a way!) and was an excellent standard in its day even if it was a MS standard. I believe Kwords default format is XML and over the next few years I think you will see all word-processors support XML as even the new Word will have it as its default format (although an XML->MSXML convertor may be necessary as some wit posted to me the other day).\n Still people want to support their legacy DOC apps and I believe Shaheed has doc export filters in the pipeline.\n\nCongratulations on Koffice, it is coming along brilliantly and those docking panels and new rtf-widget renderer are winners!\n\nAndrew"
    author: "Andrew Kar"
  - subject: "Re: Export filters"
    date: 2001-04-26
    body: "could that rtf editing widget also be used for things like html composing in kmail ?"
    author: "me"
  - subject: "Re: Export filters"
    date: 2001-04-26
    body: "iirc kafka is more likely to be used\nfor that purpose as the rtf widget won't\nbe able to import html whereas kafka should\nbe able. though kafka's development seems\nto have slowed down quite a bit lately...\n\nAlex"
    author: "alex"
  - subject: "Re: Export filters"
    date: 2002-11-20
    body: "I need help to open two file that were sent to me in wordpa format but when I try to open they say that filters are needed. I tried other extension but they didn't work. I need help to open these files which are about 300kb in size, if you have any suggestions they would be appreciated.\nThank you , Charlie Eby"
    author: "Charles Eby"
  - subject: "No dice with RedHat 7.1 RPM"
    date: 2001-04-26
    body: "I tried to install the Redhat 7.1 RPM on my RedHat 7.1 system, but it complains about a missing libkdeprint.so.0.  Where can this library be found?  I think I have all of the KDE RPMs installed.\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: No dice with RedHat 7.1 RPM"
    date: 2001-04-26
    body: "that library is provided by kde 2.2alpha1. if you're using 2.1.1 you need to rebuild the package from the .src.rpm (see the README file for details)."
    author: "Evandro"
  - subject: "Re: No dice with RedHat 7.1 RPM"
    date: 2001-05-26
    body: "Calling these packages \"Red Hat 7.1 rpms\" doesn't make sense if in fact they are compiled against a KDE 2.2-alpha1 version which is not shipped with Red Hat 7.1."
    author: "Michael Schwendt"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-26
    body: "Call me stupid, but I never figured out this: how do I check what version of Qt I currently have? That way I can know if I can easily upgrade to KOffice.\n\nDavid"
    author: "dave"
  - subject: "Re: KOffice Suite Beta Released"
    date: 2001-04-26
    body: "try \n\nldconfig -v | grep qt\nor\nldd /full/path/to/a/qt/prog | grep qt"
    author: "ac"
  - subject: "Questions about KFormula typography"
    date: 2001-05-01
    body: "1) I noticed in the screenshot that the variables are in a roman font while the function names are in italics, which is just the opposite of normal math typography. (Open a calculus or college physics textbook to see what I mean.) Is this just for the screenshot, or is it the normal KFormula default?\n\n2) Vectors in math typography are typically shown in boldface, and some vectors, such as torque and angular velocity, are represented by Greek letters. Can KFormula do bold Greek letters?"
    author: "J. J. Ramsey"
  - subject: "Re: Questions about KFormula typography"
    date: 2005-03-19
    body: "Will 670 characters set in Garamond Light at 9/11 fit in a 13 pica column width that's 22 picas deep?\n\nHow many lines will the above example run?\n\nThank you."
    author: "Nathalie Horner"
---
The <A HREF="http://www.kde.org/">KDE
Project</A> today announced the highly-anticipated release of
<A HREF="http://www.koffice.org/">KOffice</A> 1.1beta1.
KOffice is an integrated office suite for KDE which utilizes open
standards for component communication and component embedding.
The primary goals of the the release are to provide a preview of
KOffice&nbsp;1.1 and to involve users who wish to request missing features or
report problems.
Code development is currently focused on stabilizing KOffice 1.1,
<A HREF="http://developer.kde.org/development-versions/koffice-1.1-release-plan.html">scheduled</A>
for final release this summer.  The complete press release is below.

<!--break-->
<P>&nbsp;</P>
<TT>
<P>DATELINE APRIL 24, 2001</P>
<P>FOR IMMEDIATE RELEASE</P>
</TT>
<H3 ALIGN="center">KOffice Suite Beta Released for Linux</H3>
<P><STRONG>KDE, the Leading Linux Desktop, Ships Beta of KOffice Suite</STRONG></P>
<P>April 24, 2001 (The INTERNET).
The <A HREF="http://www.kde.org/">KDE
Project</A> today announced the highly-anticipated release of
<A HREF="http://www.koffice.org/">KOffice</A> 1.1beta1.
KOffice is an integrated office suite for KDE which utilizes open
standards for component communication and component embedding.
The primary goals of the the release are to provide a preview of
KOffice&nbsp;1.1 and to involve users who wish to request missing features or
report problems.
Code development is currently focused on stabilizing KOffice 1.1,
<A HREF="http://developer.kde.org/development-versions/koffice-1.1-release-plan.html">scheduled</A>
for final release this summer.
</P>
<P>
This release includes the following components:
<A HREF="#kword">KWord</A> (a frame-based, full-featured word processor);
<A HREF="#kpresenter">KPresenter</A> (a presentation application);
<A HREF="#kspread">KSpread</A> (a spreadsheet application);
<A HREF="#killustrator">KIllustrator</A> (a vector-drawing application);
<A HREF="#krayon">Krayon</A> (a bitmap image editor);
<A HREF="#kivio">Kivio</A> (a flowchart application);
<A HREF="#kchart">KChart</A> (a chart drawing application);
<A HREF="#kformula">KFormula</A> (a formula editor);
<A HREF="#kugar">Kugar</A> (a tool for generating business quality reports); and<A HREF="#filters">filters</A> (for importing/exporting documents).
</P>
<P>
While the entire release is designated as a beta release, individual
components are all at various stages of development, which is indicated
next to each component's description below.
Changes to individual KOffice components are also enumerated below.
In addition, a
<A HREF="http://www.koffice.org/announcements/changelog-1.1.phtml">list of
changes</A> and a <A HREF="http://www.koffice.org/faq/">FAQ about
KOffice</A> are available at the KOffice
<A HREF="http://www.koffice.org/">website</A>.
</P>
<P>
KOffice and all its components are available for free under
Open Source licenses from the KDE
<A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/">server</A>
and its <A HREF="http://www.kde.org/mirrors.html">mirrors</A>.
As a result of the dedicated efforts of hundreds of translators,
KOffice 1.1beta1 is available in
<A HREF="http://i18n.kde.org/teams/distributed.html">25</A>
languages</A>. 
</P>
<P>
<H4>KOffice Components</H4>
</P>
<P>
KOffice is an integrated office suite consisting of numerous components
which can interact, share data and embed each other using KDE's open KParts
and DCOP technologies.  The 1.1beta1 release includes the following components:
<OL>
<!-- KWORD -->
<LI>
<A NAME="kword"></A><A HREF="http://www.koffice.org/kword/"><EM>KWord</EM></A>
(<EM>alpha</EM>).
KWord is a frame-based word-processing and desktop publishing application.
KWord's features include:
<UL>
  <LI>paragraph style sheets (alignment, spacings, indentations, default font, etc.);</LI>
  <LI>a stylist to edit, add, remove and update paragraph styles (including a number of provides ones);</LI>
  <LI>frame orientation;</LI>
  <LI>multiple columns per page;</LI>
  <LI>tables;</LI>
  <LI>image and other document embedding;</LI>
  <LI>in-place formula editing;</LI>
  <LI>headers and footers;</LI>
  <LI>footnotes and endnotes;</LI>
  <LI>chapter numbering;</LI>
  <LI>auto-generation of table of contents;</LI>
  <LI>mail merge;</LI>
  <LI>autocorrection and spell checking; and</LI>
  <LI>templates.</LI>
</UL>
<BR>
Changes since the last release include:
<UL>
  <LI>New overall design and new formatting engine (using the Qt RichText
widget).</LI>
  <LI>Improved display adapts to the DPI settings (of the screen and of the
printer), so the page on screen has the same size as the real paper page.</LI>
  <LI>Much better performance with huge documents (the text is formatted
as a background job, the screen redrawing is done much better, etc.).</LI>
  <LI>Smaller file size.</LI>
  <LI>Real document/view separation. Working with different views on the same
document is fully functional.</LI>
  <LI>Brand new formula widget (with doc/view design, zoom support etc.)</LI>
  <LI>Redesigned table support.</LI>
  <LI>Much improved find/replace functionality.</LI>
  <LI>Real bullets instead of using characters as bullets.</LI>
  <LI>Implemented breaking paragraphs at the end of a frame or page ("Keep linestogether" can now be disabled).</LI>
  <LI>Right margin for a paragraph.</LI>
  <LI>Undo/redo fully working (all text operations, frame operations,
etc.)</LI>
  <LI>Embedding a KPresenter part into KWord now works.</LI>
  <LI>Copy/Paste and drag'n'drop work as expected (instead of for full paragraphs only).</LI>
</UL>
</LI>
<!-- KPRESENTER -->
<BR>
<LI>
<A NAME="kpresenter"></A><A HREF="http://www.koffice.org/kpresenter/"><EM>KPresenter</EM></A>
(<EM>beta</EM>).
KPresenter is a presentation application which features:
<UL>
  <LI>inserting all kinds of graphic objects;</LI>
  <LI>inserting and editing rich text (with lists, indentations, spacings, colors, fonts, ...);</LI>
  <LI>inserting autoforms;</LI>
  <LI>inserting pictures and clipart;</LI>
  <LI>embedding other KOffice parts;</LI>
  <LI>setting many object properties (background, many types of gradients, pen,
shadow, rotation, object specific settings, ....;</LI>
  <LI>working with objects (resizing, moving, lowering, raising, ...);</LI>
  <LI>grouping/ungrouping objects;</LI>
  <LI>headers/footers;</LI>
  <LI>advanced undo/redo;</LI>
  <LI>setting background (color, gradients, pictures, cliparts, etc.);</LI>
  <LI>assigning effects for animating objects and defining effects for changing
slides;</LI>
  <LI>playing screen presentations with effects;</LI>
  <LI>print as PostScript;</LI>
  <LI>creating HTML slideshows with a few mouse clicks;</LI>
  <LI>using and creating templates;</LI>
  <LI>using XML as document format; and</LI>
  <LI>a Presentations Structure Viewer.</LI>
</UL>
<BR>
Changes since the last release include:
<UL>
  <LI>Added "Copy Page" feature.</LI>
  <LI>Much faster and flicker-free switching to fullscreen mode.</LI>
  <LI>Sidebar, painting and ellipses drawing fixes.</LI>
</UL>
</LI>
<!-- KSPREAD -->
<BR>
<LI>
<A NAME="kspread"></A><A HREF="http://www.koffice.org/kspread/"><EM>KSpread</EM></A>
(<EM>beta</EM>).
KSpread is a full featured spreadsheet program.  It can work as a traditional table-oriented sheet but also supports complex mathematical formulas and statistics.  In addition, KSpread supports displaying data graphically, scripting using a restricted (for security purposes) language and templates.<BR>
 
<BR>
Changes since the last release include:
<UL>
  <LI>Support for applying attribute changes to complete rows/columns.</LI>
  <LI>Rows and columns can be hidden.</LI>
  <LI>Improved undo/redo, grouping operations when applicable.</LI>
  <LI>Added configuration dialog box.</LI>
  <LI>New dialog box to define the series used by the auto-fill feature.</LI>
  <LI>Copy and paste with insertion.</LI>
  <LI>Data validation.</LI>
</UL>
</LI>
<!-- KILLUSTRATOR -->
<BR>&nbsp;
<LI>
<A NAME="killustrator"></A><A HREF="http://wwwiti.cs.uni-magdeburg.de/~sattler/killustrator.html"><EM>KIllustrator</EM></A>
(<EM>alpha</EM>).
KIllustrator is the vector drawing program for KOffice. The aim
of the KIllustrator project is the development of a freely available
vector-based drawing application similiar to
<A HREF="http://www3.corel.com/cgi-bin/gx.cgi/AppLogic+FTContentServer?pagename=Corel/Product/Details&id=CC1IOY1YKCC">Corel
Draw</A> or
<A HREF="http://www.adobe.com/products/illustrator/main.html">Adobe Illustrator</A>.
<BR>
 
<BR>
Changes since the last release include improved speed and usability.
<UL>
  <LI>New canvas support.</LI>
  <LI>Added multipages support</LI>
  <LI>Redesigned layers dialog to dock in main window or tear off.</LI>
  <LI>Embedding KOffice parts now works.</LI>
  <LI>Improved rulers.</LI>
</UL>
</LI>
<!-- KRAYON -->
<BR>&nbsp;
<LI>
<A NAME="krayon"></A><A HREF="http://www.koffice.org/krayon/"><EM>Krayon</EM></A>
(<EM>beta</EM>).
Krayon (f/k/a KImageShop) is an effort to make a professional level bitmap
image painting and editing component of the KOffice project.
<BR>
 
<BR>
Krayon is being released with KOffice for the first time.<BR>
</LI>
<!-- KIVIO -->
<BR>&nbsp;
<LI>
<A NAME="kivio"></A><A HREF="http://www.thekompany.com/projects/kivio/"><EM>Kivio</EM></A>
(<EM>stable</EM>).
Kivio is a flowcharting program which offers more than basic flowcharting
abilities. Objects are scriptable, and a backend plugin system will offer
the ability to make objects do just about anything. Feed it a directory of
C++ header files, or even Java files, and let it generate a graphical class
map for you. Give it a network and let it explore and map out the network
for you. All this is possible through the scripting/plugin architecture
Kivio will possess.  The scripting language chosen is
<A HREF="http://www.python.or g/">Python</A>, though knowledge of Python
or programming is not required to use Kivio.  Kivio's features include:
<UL>
  <LI>Multiple paper size support. All standard paper sizes are supported
(letter, A4, etc...), and you have the ability to create your own.</LI>
  <LI>Dynamically loadable stencils. Stencils support various attributes such
as border width, foreground color and fill style (gradient, solid, pattern,
pixmap, arrow heads and text).</LI>
  <LI>Multiple pages per document to allow you to organize your document
according to your own needs.</LI>
  <LI>Grouping. Select any stencils you want, and group them into a single
element for easier manipulation.</LI>
  <LI>Full clipboard support (cut/copy/paste/duplicate).</LI>
  <LI>Clipboard history. Paste previously copied stencils!</LI>
  <LI>Layer support. Stencils can be layed out on different layers for easier
organization. Stencils also default to specific layers. For example, if
you are laying out a floorplan, furniture goes on one layer, while walls
and structural shapes go on another, and wiring on yet another.</LI>
  <LI>KParts support, which allows placing a Kivio document in a
<A HREF="#kword">KWord</A> document or inserting a
<A HREF="#kspread">KSpread</A> spreadsheet into Kivio for diagramming.</LI>
  <LI>Customizable helper lines for easy alignment.</LI>
  <LI>Subselections. Some stencils are made up of numerous small shapes;
with subselections you can change it.</LI>
  <LI>Python scripting. Scripting allows not only the programming of the
stencils, but also Kivio.  Scripting can be used to add new interface
elements or visualization tools.</LI>
  <LI>C++ Plugin interface.</LI>
  <LI>Customizable user interface. If you do not like the layout of the
screen by default, drag the various elements around until you are satisfied.
Some elements even allow you to change their colors for a more
pleasing look.</LI>
  <LI>Dynamically resizing grid. As you zoom in further and further on a
document, the grid will constantly refine itself to allow you to place
your stencils with more precision.</LI>
  <LI>Stencil builder. The stencil builder permits you to create your own
stencils without doing any programming.</LI>
</UL>
<BR>
Kivio is being released with KOffice for the first time.<BR>
</LI>
<!-- KCHART -->
<BR>
<LI>
<A NAME="kchart"></A><A HREF="http://www.koffice.org/kchart/"><EM>KChart</EM></A>
(<EM>alpha</EM>).
KChart is an embeddable chart drawing application.  Features include:
<UL>
<LI>Simple wizard-based chart creation.</LI>
<LI>Support for bar charts, line charts, pie charts, hi-lo charts and area
charts</LI>
<LI>Seamless integration into <A HREF="#kspread">KSpread</A>.</LI>
</UL>
</LI>
<!-- KFORMULA -->
<BR>&nbsp;
<LI>
<A NAME="kformula"></A><A HREF="http://www.koffice.org/kformula/"><EM>KFormula</EM></A>
(<EM>beta</EM>).
KFormula is a formula editor which provides easy-to-use input facilities.
Some of its most exciting features are:
<UL>
  <LI>Easy Greek letter insertion.</LI>
  <LI>Intelligent cursor movement.</LI>
  <LI>Advanced syntax highlighting.</LI>
  <LI>Multi-level undo support.</LI>
  <LI>LaTeX export (copy and paste into a text editor).</LI>
</UL>
<BR>
Since the last release, KFormula has been completely redesigned
and much improved.<BR>
</LI>
<!-- KUGAR -->
<BR>
<LI>
<A NAME="kugar"></A><A HREF="http://www.thekompany.com/projects/kugar/"><EM>Kugar</EM></A>
(<EM>stable</EM>).
Kugar is a tool for generating business quality reports. Besides the standalone
report viewer, Kugar is available as a KPart report viewer. This means that any
KDE application can embed the report viewing functionality and that reports can
be viewed using the Konqueror browser.
Kugar works by merging application generated data with a template to produce
the final report. Both the data and the template are specified using XML.
This approach means that applications only need worry about generating the
data itself. A template can be referenced via a URL which allows businesses
to create a centrally managed template library.  Kugar templates include
the following features:
<UL>
  <LI>Report headers and footers.</LI>
  <LI>Page headers and footers including page numbers and current date and
time.</LI>
  <LI>Calculations of counts, sums, averages, variance and standard
deviation.</LI>
  <LI>Number formatting based on value.</LI>
  <LI>Currency and date formatting.</LI>
  <LI>Full control of fonts, colors, text alignment and wrapping.</LI>
  <LI>Line drawing in multiple styles.</LI>
</UL>
<BR>
Kugar is being released with KOffice for the first time.<BR>
</LI>
<!-- FILTERS -->
<BR>
<LI>
<A NAME="filters"></A><A HREF="http://www.koffice.org/filters/"><EM>Filters</EM></A>.
Various import/export filters are available in most of the KOffice
applications to support document exchange with users of other office suits.
For more information on KOffice filter status, including import filters for
MS WinWord 97 and MS Excel 97, please visit the
<A HREF="http://www.koffice.org/filters/status.phtml">KOffice filters status
page</A>.
<BR>
 
<BR>
Changes since the last release include:
<UL>
  <LI>Many new filters: Applixware import, Abiword import/export, Docbook
export, LaTeX export, etc.</LI>
  <LI>Improvements to the existing filters, especially the MS WinWord and
MS Excel import filters</LI>
</UL>
</LI>
</OL>
</P>
<P>
<H4>Downloading and Compiling KOffice</H4>
</P>
<P>
The source packages for KOffice 1.1beta1 are available for free download at
<A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/src/">http://ftp.kde.org/unstable/koffice-1.1-beta1/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>.
KOffice 1.1beta1 requires qt-x11-2.2.3 or greater, which is available from
<A HREF="http://www.trolltech.com/">Trolltech</A> at
<A HREF="ftp://ftp.trolltech.com/qt/source/">ftp://ftp.trolltech.com/qt/source/</A>
under the name <A HREF="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.3.tar.gz">qt-x11-2.2.3.tar.gz</A>, although
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-x11-2.3.0</A>
is recommended, as well as kdesupport-2.1.x, kdelibs 2.1.x and kdebase-2.1.x,
which are available from the KDE ftp servers at
<A HREF="http://ftp.kde.org/stable/2.1.1/distribution/">http://ftp.kde.org/stable/2.1.1/distribution/</A>
(<EM>see also</EM> the
<A HREF="http://www.kde.org/announcements/announce-2.1.1.html">KDE 2.1.1
press release</A> for more information).
KOffice 1.1beta1 will not work with versions of Qt older than 2.2.3 or
versions of kdelibs older than 2.1.x.  Note that kdelibs-2.1.2
is scheduled for release the week of April 30 to fix some bugs encountered
with KOffice; users downloading KOffice 1.1beta1 are strongly encouraged to
upgrade to kdelibs-2.1.2 when it becomes available.
</P>
<P>
For further instructions on compiling and installing KOffice, please consult
the <A HREF="http://www.koffice.org/install-source.phtml">installation
instructions</A>.
</P>
<P>
<H4>Installing Binary Packages</H4>
</P>
<P>
Some distributors choose to provide binary packages of KOffice for certain
versions of their distribution.  Some of these binary packages for
KOffice 1.1beta1 will be available for free download under
<A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/">http://ftp.kde.org/unstable/koffice-1.1-beta1/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution (if you cannot find a binary package for your distribution,
please read the <A HREF="http://dot.kde.org/986933826/">KDE Binary Package
Policy</A>).
<P>
KOffice 1.1beta1 requires qt-2.2.3 or greater, which is available from
<A HREF="http://www.trolltech.com/">Trolltech</A> at
<A HREF="ftp://ftp.trolltech.com/qt/source/">ftp://ftp.trolltech.com/qt/source/</A>
under the name <A HREF="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.3.tar.gz">qt-x11-2.2.3.tar.gz</A>, although
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</A>is recommended, as well as kdesupport-2.1.x, kdelibs-2.1.x and kdebase-2.1.x,
which are available from the KDE ftp servers at
<A HREF="http://ftp.kde.org/stable/2.1.1/distribution/">http://ftp.kde.org/stable/2.1.1/distribution/</A>
(<EM>see also</EM> the
<A HREF="http://www.kde.org/announcements/announce-2.1.1.html">KDE 2.1.1
press release</A> for more information).
KOffice 1.1beta1 will not work with versions of Qt older than 2.2.3 or
versions of kdelibs older than 2.1.x.  Note that kdelibs-2.1.2
is scheduled for release the week of April 30 to fix some bugs encountered
with KOffice; users downloading KOffice 1.1beta1 are strongly encouraged to
upgrade to kdelibs-2.1.2 when it becomes available.
</P>
<P>
At the time of this release, pre-compiled packages are available for:
</P>
<P>
<UL>
<LI><A HREF="http://www.suse.com/">SuSE Linux</A> (<A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/README">README</A>):
<UL>
<LI>7.1:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/i386/7.1/">i386</A>, <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/ppc/7.1/">PPC</A> and <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/sparc/7.1/">Sparc</A>; please see the <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/NOARCH/">noarch</A> directory for common files</LI>
<LI>7.0:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/i386/7.0/>i386</A>, <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/ppc/7.0/">PPC</A> and <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/s390/7.0/">S390</A>; please see the <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/NOARCH/">noarch</A> directory for common files</LI>
<LI>6.4:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/i386/6.4/">i386</A>; please see the <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/NOARCH/">noarch</A> directory for common files</LI>
<LI>6.3:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/i386/6.3/">i386</A>; please see the <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/SuSE/NOARCH/">noarch</A> directory for common files</LI>
</UL>
<LI>Tru64 Systems:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/Tru64/">4.0e,f,g, or 5.x</A> (<A HREF="http://ftp.kde.org/unstable/koffice-1.1-beta1/Tru64/README.Tru64">README</A>)</LI>
</UL>
</P>
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages will become available over the
coming days and weeks.
</P>
<P>
<H4>About KOffice/KDE</H4>
</P>
<P>
KOffice is part of the KDE project.
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environmentemploying a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.                      
ww.koffice.org/info/">web site</A> where you can find,
among other things, information on
<A HREF="http://www.koffice.org/getinvolved/">contributing to KOffice</A>. </P> <P> <H4>Corporate KOffice Sponsors</H4> </P> <P> Besides the valuable and excellent efforts by the <A HREF="http://www.koffice.org/developers.phtml">KOffice developers</A> themselves, significant support for KOffice development has been provided by <A HREF="http://www.thekompany.com/">theKompany.com</A>, which has contributed <A HREF="#Kivio">Kivo</A>, <A HREF="#Kugar">Kugar</A> and the soon-to-be-released <A HREF="http://www.thekompany.com/projects/rekall/">Rekall</A> to KOffice, and <A HREF="http://www.mandrakesoft.com/">MandrakeSoft</A>, which sponsors KOffice developers <A HREF="http://perso.mandrakesoft.com/~david/">David Faure</A> and Laurent Montel.  Thanks! </P> <HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center"> <FONT SIZE=2> <EM>Trademarks Notices.</EM> KDE, K Desktop Environment, KChart, KFormula, KIllustrator, KOffice, KPresenter, Krayon, KSpread and KWord are trademarks of KDE e.V. Kivio, Kugar and Rekall are trademarks of thekompany.com. Adobe Illustrator is a registered trademark of Adobe Systems Incorporated. Corel Draw is a registered trademark of Corel Corporation or Corel Corporation Limited. MS WinWord 97 and MS Excel are registered trademarks of Microsoft Corporation. Linux is a registered trademark of Linus Torvalds. Unix is a registered trademark of The Open Group. Trolltech and Qt are trademarks of Trolltech AS. All other trademarks and copyrights referred to in this announcement are the property of their respective owners. <BR> <HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center"> <TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0> <TR><TH COLSPAN=2 ALIGN="left"> Press Contacts: </TH></TR> <TR VALIGN="top"><TD ALIGN="right" NOWRAP> United&nbsp;States: </TD><TD NOWRAP> Kurt Granroth<BR> granroth@kde.org<BR> (1) 480 732 1752<BR>&nbsp;<BR> Andreas Pour<BR> pour@kde.org<BR>
(1) 917 312 3122
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
faure@kde.org<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
konold@kde.org<BR>
(49) 179 2252249
</TD></TR>
</TABLE>
