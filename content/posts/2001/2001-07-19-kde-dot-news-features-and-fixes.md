---
title: "KDE Dot News: Features and Fixes"
date:    2001-07-19
authors:
  - "numanee"
slug:    kde-dot-news-features-and-fixes
comments:
  - subject: "Re: KDE Dot News: Features and Fixes"
    date: 2001-07-19
    body: "Yay!  Nice to know that stuff is going on behind the scenes.  All these improvements are much appreciated.  The Dot is a great site, and _certainly_ getting better and better!"
    author: "not me"
  - subject: "Good job, Navindra!"
    date: 2001-07-19
    body: "All this sounds good but the most important development is that the new hosting seems to be working really well, at least for me. Speed is excellent and I haven't seen any availability problems.\n\nThis site is really turning out to be a great resource and perhaps _the_ center for the KDE community."
    author: "Otter"
  - subject: "Re: Good job, Navindra!"
    date: 2001-07-19
    body: "Not to be a downer, but I have exactly the opposite.  80% of the time dot.kde.org is\nVERY slow, 10% of the time it's fast/medium and the other 10% of the time it's unreachable.  I can live with the 80% slow . . . I can live with 100% slow, but 10% downtime really is rough.\n\nI don't know if it's a hop between my ISP and dot.kde.org, or if dot.kde.org just doesn't have a fat enough pipe to serve up all the connections, or what, but I don't think it is my ISP's problem since I can go to just about any other site 99% of the time w/out any problems whatsoever."
    author: "A. C."
  - subject: "Re: Good job, Navindra!"
    date: 2001-07-19
    body: "Unreachable meaning that you can't even ping it?\n\nThis is news to me...  as far as I knew, things were going quite well.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Good job, Navindra!"
    date: 2001-07-19
    body: "I've not tried pinging it, but Netscape/Mozilla/IE all say \"Connecting to\" (or the equivalent) but fail to say \"Downloading data\" (or the equivalent).  And since it does that not only on multiple browsers on the same machine, but multiple machines, and I can access other sites, I take that as dot.kde.org as being unreachable (at least, unreachable to me as far as downloading content via HTTP)"
    author: "A. C."
  - subject: "Re: Good job, Navindra!"
    date: 2001-07-19
    body: "Does this happen every day, or just when we happen to be slashdotted? :-)\n\nWe made a technical gaff the day we were slashdotted but I think we should hold up better next time.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Good job, Navindra!"
    date: 2001-07-19
    body: "Finally!  It let me get on.  I didn't know when the dot would be back up, so I sent you an e-mail w/ the results of my ping, since I was unable to read what you had said when you replied (I knew you replied because of the e-mail notification).\n\nAnyway... maybe that answers you question: it happens everyday, any time of day/night.  It normally only lasts for about 5-10 minutes, but it has gone on for hours/days before."
    author: "A. C."
  - subject: "Re: Good job, Navindra!"
    date: 2001-07-19
    body: "Truly strange, because in the meanwhile I was accessing the dot just fine.  If this happens again, please send me the results of \"tracert 193.103.254.20\".\n\nHopefully we can get to the bottom of this.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Good job, Navindra!"
    date: 2001-07-19
    body: "You got it!\n\nAppreciate the help! =)"
    author: "A. C."
  - subject: "Re: Good job, Navindra!"
    date: 2001-07-19
    body: "Ditto.  \n\nI have the same problem at least once a day where I can't access the dot.  The browser eventually times out.  Give it 15 mins and usually things are back up again.  I will make a note of the times this happens.  Hopefully this can be sorted out.."
    author: "KDE Fan"
  - subject: "Re: Good job, Navindra!"
    date: 2001-07-19
    body: "I've had the exact same problem. I want to add the  Dot's headlines to my deadman's redirect, but it's  so often down, I can't."
    author: "Lenny"
  - subject: "Cool Logo"
    date: 2001-07-19
    body: "I also quite like the new logo at the top of the page. :)\n\nJason."
    author: "Jason Tackaberry"
  - subject: "Re: Cool Logo"
    date: 2001-07-19
    body: "Of course!  How could I fail to mention this...  Our beautiful logo is the work of an \"anonymous\" benefactor!  :-)\n\nThe original logo was by Kurt Granroth, btw.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Cool Logo"
    date: 2001-07-19
    body: "Anonymous??? I thought this was the talented tackat yet again?"
    author: "DavidA"
  - subject: "Re: KDE Dot News: Features and Fixes"
    date: 2001-07-19
    body: "The \"subscribe\"-link to the left is broken."
    author: "someone"
  - subject: "Re: KDE Dot News: Features and Fixes"
    date: 2001-07-19
    body: "A problem with our code.  It was fixed for the mainpage but I had to go through several files which had the exact same code and update them... sigh.  We inherited this situation from the Squishdot demo.\n\nAnyway, it should be fixed now.  In the future I hope we can do better code sharing.\n\nThanks,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: KDE Dot News: Features and Fixes"
    date: 2001-07-19
    body: "I have no complaints, I just wonder where you found this:\n\"No fake - I'm a big fan of konqueror, and I use it for everything.\" -- Linus Torvalds"
    author: "reihal"
  - subject: "Re: KDE Dot News: Features and Fixes"
    date: 2001-07-19
    body: "<a href=\"http://bugs.kde.org//db/27/27340.html\">bugs.kde.org</a>, you might also want to check <a href=\"http://groups.google.com/groups?ic=1&selm=9iikor%24mi2%241%40penguin.transmeta.com\">groups.google.com</a>. :-)"
    author: "Navindra Umanee"
  - subject: "Re: KDE Dot News: Features and Fixes"
    date: 2001-07-20
    body: "Heh, well, he's a smart kid with good taste."
    author: "reihal"
  - subject: "Re: KDE Dot News: Features and Fixes"
    date: 2001-07-19
    body: "Great news - things seem to always be getting better!\n\nOne feature I would like to see on the dot is to be able to view an entire thread.\n\nWhen the number of postings gets long, I can click on a 'child' posting, and view it and its children, but I can't do this for the 'parent' posting, which means that if there are several children of the original post, I have to start hitting the back and forwards buttons like crazy to view each of the groups of 'children' in turn.\n\nIs it possible to highlight the parent as well, so that I can then view the entire thread?\n\nCheers\n\nWill\n  --"
    author: "Will"
  - subject: "Re: KDE Dot News: Features and Fixes"
    date: 2001-07-19
    body: "I think increasing your 'Thread Threshold' might temporarily alleviate the problem. I currently have mine set at 150 and this works quite well for me."
    author: "ne..."
  - subject: "Re: KDE Dot News: Features and Fixes"
    date: 2001-07-20
    body: "I have mine set at 1000.  Sure, the page takes a couple seconds longer to download, but I only have to do it once!\n\nI think that's really the best solution."
    author: "not me"
  - subject: "Re: KDE Dot News: Features and Fixes"
    date: 2001-07-21
    body: "Yeah, I've been wanting to do this. I'll do it soon.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: KDE Dot News: Features and Fixes"
    date: 2001-07-23
    body: "I have added a \"View This\" option under each comment.  \n\nIt should do what you want, provided your thread threshold is high enough (eg, if your thread threshold is 10 and the number of articles in a thread is greater than 10, the thread will be threaded ;-)\n\n-N."
    author: "Navindra Umanee"
  - subject: "Blank-out subject for initial replies"
    date: 2001-07-19
    body: "First, it's great to see so much work go into the dot!\n\nI mentioned this a long time ago, but I'm curious if this feature is possible or has been lokked into:\n\nI think it would be nice if the initial reply to a story would have a blank subject by default instead of \"Re: Whatever the story was\".  This way, people would enter meaningful subject lines, which would make threads easier to identify.\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: Blank-out subject for initial replies"
    date: 2001-07-21
    body: "Makes sense.  I'll have to fool around with the cookies and stuff, I'll look into it.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Blank-out subject for initial replies"
    date: 2001-07-25
    body: "Done!\n\n-N."
    author: "Navindra Umanee"
  - subject: "ICQ notification"
    date: 2001-07-19
    body: "What would be *really* cool is new-news notification via ICQ or AIM - preferrably only to users that are online at the moment."
    author: "Johan"
  - subject: "Can we help?"
    date: 2001-07-20
    body: "Howdy.  I'm one of the Zope guys and a dot addict.  I've been meaning to contact you folks and see if I can get some of us to help.\n\nI can think of a couple of suggestions to make things a lot better for the site.  I can also get one of the engineers assigned to spend a few hours giving suggestions.\n\nMaybe in return someone can write a kio_slave for Zope. :^)\n\n--Paul"
    author: "Paul Everitt"
  - subject: "Re: Can we help?"
    date: 2001-07-20
    body: ">Maybe in return someone can write a kio_slave for Zope. :^)\n\nI can't tell wether this is a joke or not.\nWhat do you have in mind?"
    author: "reihal"
  - subject: "Re: Can we help?"
    date: 2001-07-21
    body: "<p>\nOhh, I'm not kidding about this.  I kid about \nother things, but not this. :^)\n</p>\n<p>\nHere's what I'm talking...first and foremost, have it do <a\nhref=\"http://www.webdav.org/\">WebDAV</a> as well as it does FTP.  If\nyou want to be wacky, have it support locking/unlocking and DAV\nproperty viewing/changing.\n</p>\n<p>\nIf you want to see what this would look like, take a look at three\nproducts for Windows that do this: <a\nhref=\"http://www.webdrive.com/\">WebDrive</a>, <a\nhref=\"http://www.webifs.com/products/webifs/index.html\">WebIFS</a> and\n<a href=\"http://www.teamstream.com/product.htm\">TeamDrive</a>.  All\nthree are let you map a drive letter to a DAV folder.\n</p>\n<p>\nThis is a pretty simple extension to HTTP.  Zope (and other dynamic\nsystems) also need an option to be implemented that no other DAV\nclient has yet to do, which is to retrieve the *source* of a page\nrather than the rendered GET of the page.  The DAV spec anticipates\nthis.\n</p>\n<p>\nLet's see, the DAV spec requires digest authentication to be used\ninstead of basic authentication in cases where SSL isn't used.  I\ndon't know if the existing HTTP support in KDE can do digest auth.\n</p>\n<p>\nGnomeVFS implements DAV, but for some reason that mystifies me, they\ndon't provide any ability to do authentication.  I don't know why\nsomeone would want a read-write web with no authentication.\n</p>\n<p>\nThere's good functionality beyond the above, but it wouldn't be the\ndomain of the kio_slave.\n</p>\n<p>\nSo, there you have it -- I wasn't kidding! :^)\n</p>\n--Paul"
    author: "Paul Everitt"
  - subject: "Re: Can we help?"
    date: 2001-07-21
    body: "well dqwit is working on DAV integration into the http io slave, so the DAV part is covered. Knowing (because I use it) that apache has an excellent mod_dav there would be no nreal need to integrate it directly into zope as long as the data that will be m odified can be ins ome for accessed in it hard coded form by apache. I do not know if zope uses a databse.\nanyways WebDAV is in thw works, because I wanted to offer writing an io slave for it and dawit said he was on it"
    author: "dmalloc"
  - subject: "Re: Can we help?"
    date: 2001-07-22
    body: "First, could you give me the email address for dqwit?  I want to find out, and beg if necessary, if he's doing the part of WebDAV that retrieves the document source.\n<br><br>\nNext, to cover your subsequent points...Zope already supports DAV. Basically, Zope runs as a process outside of Apache.  Zope includes protocol handlers for HTTP, FTP, xml-rpc, FastCGI, etc.  You can put Apache in front of Zope and use mod_proxy as the RPC.\n<br><br>\nYes, Zope uses a database, it's internal object database written in Python.  More info at:\n<br><br>\n1) <a href=\"http://www.zope.org/Documentation/Articles/ZODB1\">http://www.zope.org/Documentation/Articles/ZODB1</a><br>\n2)   <a href=\"http://www.zope.org/Documentation/Articles/ZODB2\">http://www.zope.org/Documentation/Articles/ZODB2</a>\n<br><br>\nThe database is organized like a filesystem, rather than relationally. This makes it a great fit for DAV.  Objects in the database are instances of Python classes, meaning there are real first class \"document\", \"image\", \"folder\", and user-defined classes for DAV to tinker with.\n<br><br>\nHere's the HTML \"GUI\" for working in the object database, <a href=\"http://www.zope.org/Members/michel/ZB/UsingZope.dtml#2-1\">http://www.zope.org/Members/michel/ZB/UsingZope.dtml#2-1</a>\n<br><br>\nWe've been doing DAV for two years now and thus I have a huge desire for a good DAV client environment.  Having a better DAV client in KDE would be a big win.\n<br><br>\nDo you use Python much?"
    author: "Paul Everitt"
  - subject: "Re: Can we help?"
    date: 2001-07-23
    body: "Dawit Alemayehu <adawit@kde.org>\n\nGoogle Search:\nhttp://www.google.com/search?client=googlet&q=dawit%20KDE"
    author: "APW"
  - subject: "yeah! :)"
    date: 2001-07-20
    body: "Great to hear from you, Paul!\n\nThere are definitely some Zope issues that we could use some help with, and I would also love to hear your suggestions.  With Zope we're still not too sure how to optimize performance/memory or how to load-balance the dyn-generated stuff. \n\nAs for site limitations proper, we're based on Squishdot so we're mostly limited to its features.  Unfortunately Squishdot is a dead-end product right now and most of the maintainer's development effort is going into the next generation Swishdot which will be based on ZPK.\n\nI don't too much time at the moment  to go into details or work on this, but we should definitely correspond.  :-)\n\nAs for a kio_slave, what kind of advantages do you think such a thing would have over the Web interface?\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: yeah! :)"
    date: 2001-07-21
    body: "Oops, sorry for the misfire on the links in my previous post.  Shoulda\nused preview. :^)\n\nPlease feel free to email me directly and we'll try to get started on\nhelping get Zope going better.\n\nRegarding how the kio_slave would be better...people that want to\nauthor documents in text, HTML, files in Word, images, etc. could just\nuse any KDE tool to do so.  They could drag-n-drop folder hierarchies\noff their local machine onto the remote folder.  Or they could work in\nplace, creating folders and documents etc. on the remote server.\n\n--Paul"
    author: "Paul Everitt"
  - subject: "Re: Can we help?"
    date: 2001-07-20
    body: "I know it is rather off topic, but could we please port the python part to mod_python. The python processes itself do burn a lot of ressources. Cheers"
    author: "darian Lanx"
  - subject: "What's wrong with this picture:"
    date: 2001-07-20
    body: "Off-topic, I know...\n<br><br>\n<a href=\"http://www.stardock.com/products/desktopx/dx-jan01.jpg\">http://www.stardock.com/products/desktopx/dx-jan01.jpg</a>\n<br><br>\nThat's a screenshot from Stardocks new shell-replacement for Windows, DesktopX. According to them, that there provied users \"Unix-like\" way of using their computer. Take a look at the *cough* Start-button *cough*."
    author: "Janne"
  - subject: "Re: What's wrong with this picture:"
    date: 2001-07-20
    body: "AWESOME!  \n\nThis Windows is getting better all the time.  I really should check it out. ;-)"
    author: "KDE User"
  - subject: "Re: What's wrong with this picture:"
    date: 2001-07-20
    body: "I guess imitation is the sincerest form of flattery. But... Has KDE trademarked any of the graphics associated with KDE? I mean, I for one find it rather weird that somebody uses KDE-graphics on a Windows-project that has nothing to do with KDE!"
    author: "Janne"
  - subject: "Re: What's wrong with this picture:"
    date: 2001-07-20
    body: "The let loose the KDE-lawyers, they will eat them alive!"
    author: "reihal"
  - subject: "Re: What's wrong with this picture:"
    date: 2001-07-20
    body: "geezzzzz .. i was looking at that shot of the desktop .. lmao ... guys it looks like a linux theme .. take a good look at the other icons that are there.. it looks like a desktop of someone who likes linux but is forced to work on windows!\n\n\nMike"
    author: "Micheal"
  - subject: "Re: What's wrong with this picture:"
    date: 2001-07-21
    body: ">Take a look at the *cough* Start-button *cough*.\n\nAnd the window decorations!  Obviously the author of that Stardock theme is a KDE fan.  Those guys are hippocrites, though - their license prevents KDE from making a Stardock theme importer (which would be rad IMHO, there are TONS of GREAT themes for WindowBlinds), yet they go and use _our_ graphics with impunity.  Someone ought to go copyright the KDE look+feel and hire some lawyers..."
    author: "not me"
  - subject: "UPDATE:"
    date: 2001-07-22
    body: "That theme is made by individual developer, not by Stardock. So in a way, it's no different from the stuff at kde.themes.org"
    author: "Janne"
---
Lately, we've been spending some time improving the dot and implementing new features.  While our efforts are far from over, we thought we'd announce a few of the things we've done so far. Dre has <a href="http://www.kde.com/news/announce.php?id=14">finally implemented</a> one of the most requested features of the dot: You can now receive timely email notification of <a href="http://lists.kde.com/lists/listinfo/dot-headlines">dot-headlines</a>, or, depending on your preference, you can receive the full text of the <a href="http://lists.kde.com/lists/listinfo/dot-stories">dot-stories</a>. As if that wasn't enough, I went ahead and implemented <a href="http://dot.kde.org/static/flatforty">Flat Forty</a>.  Flat Forty can be thought of as a poor man's flat mode with a twist: You can now view the 40 most recent stories and comments <i>globally</i>.  While mostly useful as an administrative trollbuster, we think you might enjoy it too.  We hope to optimize Flat Forty eventually so that we can provide an efficient timely dynamic generation of the page. Read on for some dot fixes...

<!--break-->
<br><br>
I've spent a few hours trying to optimize some of my less smart dot code.  In particular, I've optimized the Past Articles box on the right of the <a href="http://dot.kde.org/">main page</a>. In the process, I've shaved off 1 or 2 seconds in the dot response time as well as changed the format of the box to display the 14 most recent articles that are <i>not</i> posted on the main page.
<br><br>
Related to this code, our <a href="http://dot.kde.org/articles">All Articles</a> feature has been optimized.  Although still slow, it's now <i>much faster</i> than before.  In the speed improvement department, we have also switched to static hosting for much of our static content.  Our images are now mostly served off of a static webserver, and we have a few tools in place for automatically providing dot content in a static format (as we did for Flat Forty).  More behind the scenes backend improvement is going on.
<br><br>
Finally, I've been working on rewriting the dot code to generate validated HTML.  As a first step, I hacked a basic <a href="http://dot.kde.org/template">template</a> that tries to set the basic dot format -- you can <a href="http://validator.w3.org/check?uri=http%3A%2F%2Fdot.kde.org%2Ftemplate&doctype=Inline">see for yourself</a> that it validates!  The fixes will eventually percolate throughout the whole site.
<br><br>
You can expect more features and fixes in the short term.  Top of my head and top of my list: (1) Provide HTML format support for comments, (2) Add no-comments and flat modes for article reading, (3) More speed improvements, ...
<br><br>
Enjoy!
