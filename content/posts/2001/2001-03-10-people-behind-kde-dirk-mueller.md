---
title: "The People Behind KDE: Dirk Mueller"
date:    2001-03-10
authors:
  - "KDE.org"
slug:    people-behind-kde-dirk-mueller
comments:
  - subject: "OT: Konquerer and Pogo.com"
    date: 2001-03-10
    body: "Anyone had any luck with the Java games on pogo.com? With the second beta of 2.1 I was able to play about half of them, but I can't get any of them to work with the final release. ;-(\n\nOh well, still love Konquerer."
    author: "JC"
---
This week on <a href="http://www.kde.org/people/people.html">The People Behind KDE</a>, our beloved <a href="mailto:tink@kde.org">Tink</a> interviews KDE perfectionist Dirk Mueller, one of the people you can thank for how well <A HREF="http://www.konqueror.org/">Konqueror</A>, and KDE in general, work these days. <i>"Right now I'm mainly working on the Konqueror HTML engine of KDE 2.x, it's
very interesting and challenging work so I spend most of my time on this. 
In general I like improving KDE overall, fixing performance and usability issues
or bugs. I try to keep an eye on the development and take a look at whatever is
interesting to me at the moment. Especially, I like working on the nasty little details
that make things perfect, hence my addiction to KHTML."</i>


<!--break-->
