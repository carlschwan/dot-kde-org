---
title: "GPL'd Quanta Plus 2.0 is Out"
date:    2001-12-10
authors:
  - "numanee"
slug:    gpld-quanta-plus-20-out
comments:
  - subject: "Hmm"
    date: 2001-12-10
    body: "awesome, thanks eric! I've spent a LOT of time writing pages in Quanta Plus over the last year and I'm thankful the free version's not dead."
    author: "not the holy them"
  - subject: "ummm i dont get it..."
    date: 2001-12-10
    body: "i dont mean to troll, but I am sure some will think it is?\n\nit looks identical to the old version, am i missing something?  I dug arround for a meaningful changelog and found none.  \n\nat least the other one looks like an updated UI... \n\ni think the KDE cvs version builds under KDE 3.0 now, is that the update?\n\natleat it is still in KDE cvs and not in KDE nonbeta :)\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: ummm i dont get it..."
    date: 2001-12-10
    body: "Changes doesn't mean only \"surface\" changes. If you look at the code, you can see what has chaged. And yes, there are also some new features..."
    author: "Andras Mantia"
  - subject: "Re: ummm i dont get it..."
    date: 2001-12-10
    body: "What where these changes?\nJust bug fixes?  Added a js debugger?  Added a css library or a web page effects wizzard?  I am interested.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: ummm i dont get it..."
    date: 2001-12-10
    body: "Taken from the Changelog file:\n\n- yes,bugfixes (these are just as important as new features)\n- rename possibility of files/folders in the project tree view added\n- mailto inserting dialog added (with KAB support)\n- CSS syntax higlighting added\n- tooltips added for the tree views\n- inserting of empty directories is possible\n- improved  \"New project\" wizard\n- rescan project tree will also find the empty dirs, and remove the missing ones\n- save project menuitem added\n- you can select the active opened file also from a list\n- it's possible to use KDE (e.g. KDevelop) style tabbars. Use the --with-ktabbar switch when you configure Quanta\n\nSome of these can be seen from the UI point of view."
    author: "Andras Mantia"
  - subject: "Re: ummm i dont get it..."
    date: 2001-12-10
    body: "The other is an updated UI only in the sense that it is not a KDE UI any longer."
    author: "ac"
  - subject: "Re: ummm i dont get it..."
    date: 2001-12-10
    body: "No actually they cleaned up a few UI problems, and updated the editor.  As far as I am concerned the editor is the biggest part of a development enviroment.  I could be wrong though, I have always programmed all my life and never done HTML before other than to play.   \n\nIt irritateing to have some of the KDE features not there, but they did a good job of makeing up for that in the editor changes.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: ummm i dont get it..."
    date: 2001-12-10
    body: "There is significantly more to Quanta Gold than the UI changes, the ftp enhancements, new editor, CSS stuff and lots of other things.  Feel free to try out a demo at ftp.rygannon.com/pub/QuantaGoldDemo.\n\nWe will be posting a message about the history of QuantaGold soon as there are some inaccuracies in Erics statements that the original authors would like to clear up."
    author: "Shawn Gordon"
  - subject: "Re: ummm i dont get it..."
    date: 2001-12-11
    body: "> There is significantly more to Quanta Gold than the UI changes, the ftp enhancements, new editor, CSS stuff and lots of other things.\n\nBut in the process many of the changes, good or bad, were required due to no longer being able to include a fair amount of GPL code Quanta Plus depends on. I mean I got new ftp features when I built kio_fish because I can now upload secure. kio is one of the cool things about KDE and because Quanta Plus depends on it a cross platform build would have to replace ftp functionality. Anyway this was about the GPL'd version.\n\n> We will be posting a message about the history of QuantaGold soon as there are some inaccuracies in Erics statements that the original authors would like to clear up.\n\nAs I told them all they have to do is let me know and if I have made an error I will be happy to change it, I posted only the briefest mention here that they chose to develop the commerical project instead. I also took great care to find a way to word it all to avoid offending anyone. As someone who already has to walk the tightrope producing commerical products in the open souce community you know how easily people can blow small things out of proportion.\n\nI have always said honesty is the best policy. Thus when my firends decided to produce the commercial product and I read on line posts that I felt were speculations that were unfavorable towards theKompany I often would make the correction for what I felt was an obvious reason. If anyone should be expected not to defend theKompany in this it would be me, so therefore my defense of them in stating that Shawn had correctly said what he did (in those cases) should clear up the speculation. I did this for one reason and one reason only... that I value honesty and integrity.\n\nSince one could wonder I want to say publicly what I said privately, regardless of how one may version a story... if there is anything I need to correct due to innacuracies I will do so.\n\nAs for my part I intend to focus on developing a superior program and have only reluctantly entered into the arena of talking about it to stem the tide of email I have been getting. One would think that would be all the more important if you had to sell the program too."
    author: "Eric Laffoon"
  - subject: "Re: ummm i dont get it..."
    date: 2001-12-11
    body: "> The other is an updated UI only in the sense that it is not a KDE UI any longer.\n\nDoncha love the confusion? Are you talking Quanta Plus or Quanta Gold? Quanta plus is still linked to the KDE libraries. Maybe you can point to some style elements? Or maybe there is some confusion. Quanta Plus is a KDE app if I have anything to say about it."
    author: "Eric Laffoon"
  - subject: "Re: ummm i dont get it..."
    date: 2001-12-13
    body: "Sorry for the confusion, and let me just thank you for reviving GPL Quanta despite all the adversity in the project's history!"
    author: "ac"
  - subject: "The CVS version?"
    date: 2001-12-10
    body: "The linked project seems to have a CVS archive at Sourceforge. What is up with the Quanta in the KDE CVS tree? Is it being actively maintained?"
    author: "Otter"
  - subject: "Re: The CVS version?"
    date: 2001-12-10
    body: ">What is up with the Quanta in the KDE CVS tree?\n\nIt's bogus :-).\nThe REAL cvs of the GPL Quanta is now part of KDE's cvs.  Follow the directions on KDE's website regarding getting anonymous CVS access, and just do a \"cvs co quanta\".\n\nNote that for me at least, the current quanta (on the KDE3/\"HEAD\" branch) compiles but doesn't quite run on KDE3 (well, as of a few days ago).  The Quanta 2.0 release source has (or had) a minor configure/compile issue that had to be worked around to get the compile to finish, but it seems to run beautifully (under KDE 2.2.2).\n\nThanks, incidentally, to Mr's Laffoon and Mantia and any other developers (and thanks, even, to the developers who went proprietary, for the work they did up to that point) who've joined the GPL fork, for keeping this alive!  $50US just seems a bit much for my budget to get a proprietary version that promises to add a bunch of bells-and-whistles that I likely won't really need...(P.S. Get the price under $30US and make sure it runs under KDE3, and I'd still consider getting the commercial version as well, anyway!)"
    author: "DrDubious DDQ"
  - subject: "Re: The CVS version?"
    date: 2001-12-10
    body: "well the electronic version is under $40, and it will work just fine under KDE3, and I imagine the features we are adding to the base product will be very useful to you overall, things like CVS and enhanced FTP and a flexible syntax highlighting text editor so that we can add Python and Perl highlighting and some other features that Zope developers are looking for."
    author: "Shawn Gordon"
  - subject: "Re: The CVS version?"
    date: 2001-12-11
    body: "The KDE 3 versionis already under development. As for all of it I have to actually update cvs as Andras has not had consistent access due to firewall constraints for the time being. I hope to have this up as well as the branch within a day. I have had a bunch of other things going on too.\n\nWRT features in the KDE 3 version if you read the release it mentions a lot. One thing mentioned here is cvs. I have really enjoyed Cervisia for a long time. Becasue it is a kpart now, thanks to Rich Moore, it will be pluggable in Quanta as a part and we will make it integrate nicely. \n\nThere are also some nice things being done with DCOP scripting enhancements and we are looking into what we can do with integrating code being developed now for Gideon for PHP. That's the great thing about GPL'd code... you can mix and match from a wide pallette and greatly leverage your work. ;-)"
    author: "Eric Laffoon"
  - subject: "soft wrapping?"
    date: 2001-12-10
    body: "anybody know if this version supports soft line wrapping? I love pretty much everything else about Quanta, but the lack of soft wrapping drives me nuts.\n\nguess if nothing else I'll just have to grab it and see... :)\n\n-r-"
    author: "Ryan..."
  - subject: "Re: soft wrapping?"
    date: 2001-12-10
    body: "No, it doesn't support (yet)."
    author: "Andras Mantia"
  - subject: "Re: soft wrapping?"
    date: 2001-12-12
    body: "Is the script dialogs still broken or is it me who is still unable to use it??\nIf I do a simple script \n#!/bin/bash\necho \"coucou1\"\necho $1\necho \"2\"\nif I configure an action to use it\nwith\ninput is selected text\noutput is current position or new document\ni get:\ncoucou1\n\ncoucou2\nwill it work soon? or is it me (then it's hopeless p)?"
    author: "hm"
  - subject: "gpl ?"
    date: 2001-12-10
    body: "hi,\n\ni'am pretty new in linux/kde world but does a company can take an open source project and sell it ?\n\nthe gpl is not here for protect this ?\n\nok they must give the code of the old open source project but they can i guess do inovation in plugin for not give back to the comunoty the new sources.\n\nwhat do you think ?"
    author: "blue"
  - subject: "Re: gpl ?"
    date: 2001-12-10
    body: "The source \"owner/author\" decides the licence. For Quanta the authors decided to have a fork and licence the Quanta Gold tree commercial (through theKompany) and the Quanta Plus GPL.\n\nIt's totally legal and done with some other apps from theKompany too afaik"
    author: "maxwest"
  - subject: "Re: gpl ?"
    date: 2003-01-10
    body: "Whether one can sell a copy of a program depends on that program's license.  The GNU GPL does not prevent distributing copies of GNU GPL-covered software for a fee.  In fact the Free Software Foundation (who wrote the GNU GPL) encourages people to distribute the software at whatever price you can get for it (see http://www.gnu.org/philosophy/selling.html).  The summary of the news item is confusing because it fails to use the term \"commercial\" clearly.  GNU GPL-covered software is commercial software if you paid to receive a copy of the software.  The GNU GPL prohibits proprietary derivatives, not commercialization.\n\nIt's also odd to see people discussing the GNU GPL in the context of the Open Source movement.  The Open Source movement had no role in writing the GNU GPL.  The GNU GPL was in use well before the Open Source movement existed.  The Open Source movement does not share the same philosophy as that expressed in the GNU GPL.  To properly understand why the GNU GPL says what it does you need to look to the Free Software movement which is over a decade older than the Open Source movement.\n\nFor more information on both movements, please read http://www.gnu.org/philosophy/free-software-for-freedom.html which directly addresses the difference between the two movements without insulting the reader or reducing either movement to a triviality.  Check out http://www.gnu.org/philosophy/ for more information on the concept of Free Software."
    author: "J.B. Nicholson-Owens"
  - subject: "Strict HTML?"
    date: 2001-12-10
    body: "Does anyone know, will Quanta's pages pass W3C's validation? \n\nI think that we don't need any more proprietary HTML-variants.\n\nBTW, thank you developers for great work!"
    author: "Juha Tuomala"
  - subject: "Re: Strict HTML?"
    date: 2001-12-10
    body: "Well it will comply if you write W3C code!  :-)\n\nBUt in a word --  yeah it does...."
    author: "Matt Bottrell"
  - subject: "Re: Strict HTML?"
    date: 2001-12-10
    body: "So it does not do any syntax checking by itself? Only color highlighting?\n\nSorry, perhaps I should take time and read it's manual.\n\nCheers,\n\nJuha"
    author: "Juha Tuomala"
  - subject: "Re: Strict HTML?"
    date: 2001-12-10
    body: "It does. Right click->Syntax check. Uses weblint for this."
    author: "Andras Mantia"
  - subject: "Re: Strict HTML?"
    date: 2001-12-11
    body: "You need nsgmls and a HTML 4.x DTD for any real checking. weblint just gives you hints and points out uglynesses (which nsgmls does not)."
    author: "HolgerSchurig"
  - subject: "Re: Strict HTML?"
    date: 2001-12-11
    body: "Of course http://validator.w3.org/ is very powerful as wel."
    author: "Jos"
  - subject: "Re: Strict HTML?"
    date: 2001-12-10
    body: "Quanta is not a Front Page or something like that. With Quanta you edit\nhtml source, not graphical view of the page. (At least last time I used it!)\nSo Quanta does not generate any compliant or non-compliant pages."
    author: "Eeli Kaikkonen"
  - subject: "Re: Strict HTML?"
    date: 2001-12-10
    body: "It has a nice tag editor, but really with little effort one could write a Kate plugin to edit HTML tags, and to view the DOM tree and we would have something just as cool.\n\nQuanta has a few other features, but only useful to those who do not have access to KDE.\n\nHTML Preview, and network transparency.\n\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: Strict HTML?"
    date: 2001-12-10
    body: "Such a plugin for Kate basically already exists, see\nhttp://www.danielnaber.de/tmp/"
    author: "Daniel Naber"
  - subject: "Re: Strict HTML? (Don't forget PHP!)"
    date: 2001-12-10
    body: ">Quanta has a few other features, but only useful to those who do not have access to KDE.\n\nNot necessarily true.  Quanta+ is also the most 'comfortable' PHP(/HTML/Javascript) editor that I've run into so far.  Even when I write PHP classes (without HTML) or even PHP 'standalone' scripts, I find Quanta more comfortable to use than Kate.  Quanta's PHP/Javascript/HTML syntax highlighting seems much better than Kate's as well, at least last time I took a look at it.\n\n(Not to denigrate Kate, which is a rather nice text editor in its own right...)\n(And, of course, I hear rumors that KDevelop will have PHP support at some point in the future as well...)"
    author: "DrDubious DDQ"
  - subject: "Pre-compiled binaries..?"
    date: 2001-12-10
    body: "At the Sourceforge download page there was only a tar.bz2 package. Does anyone know if there will be some binaries available, for example RPM's for Red Hat...?"
    author: "scb"
  - subject: "WYSIWYG editor coming soon."
    date: 2001-12-10
    body: "I am looking forward to this. I make extensive use of dreamweaver on the win platforms, and have been longing for something similar for Linux. Yes, it's very good to be able to get at the code and make changes, but as a designer, it is much faster to be able to resize tables with a click and a drag, and other similar WYSIWYG features."
    author: "porter235"
  - subject: "Re: WYSIWYG editor coming soon."
    date: 2001-12-10
    body: "Perhaps you should contact Macromedia and talk them into\ndoing a Linux version of Dreamweaver Ultradev. \nIt really can't be that hard since most of the application is built in javascript.\n\nHaving an Ultradev for Linux would make sense, since many of\nthe people that use Ultradev deploy on Linux, so people might wan't to\ndevelop in Linux as well.\n\nI have urged them, to do so several times, but they can't see\nthe market for it. But the more people that asks....\nI would certainly buy it if it was priced as the windows/Mac version\n\nBut naturally, it would be nice with some WYSIWYG in quanta\nas well."
    author: "Uno Engborg"
  - subject: "Re: WYSIWYG editor coming soon."
    date: 2001-12-11
    body: "I know what you mean.  The best thing we have is \"Netscape Composer.\"  Which isn't bad for Personal Home Pages...but that's about it.\n\nDo you hear that Shawn Gordon!  I know you're reading this.  If you want to make a LOT of money(I think), make Quanta more like Dream Weaver.  That's what we need.  I would pay for that.  I won't pay for the current Quanta.  I won't even use the free one :-)\n\nAlthough, for now I think I'm gonna see what'll work under WineX.\nYes, I refer to it as WineX because that's why I use!"
    author: "HotPot"
  - subject: "Re: WYSIWYG editor coming soon."
    date: 2001-12-11
    body: "why don't you send me a private email for what features specifically you are looking for.  I don't want to offend anyone with this statement, but I've found most professional web developers despise WYSIWYG tools for web development, they generate crap HTML typically and are often slower to work with if you really know what you're doing.\n\nWhen i started doing web programming about 7 years ago, it was all notepad and HTML 1.0 if I recall correctly.  Back then I wrote a really cool little preprocessor that allowed you to put variables in HTML and do parameter substitution and execute segments of web pages like sub-routines, i suppose I should have stuck with it.  \n\nIn any case I had tried a number of WYSIWYG tools, but the best thing I found for being productive was HotDog from Sausage Software, and Quanta really reminds me of that tool.\n\nSo let me know what features you are looking for (I never used Dreamweaver myself) and we'll see what can be done.  The component architecture of Quanta Gold makes future plug ins quite simple to implement, this was one of the limitations with the Quanta+ that we solved in the rewrite."
    author: "Shawn Gordon"
  - subject: "Re: WYSIWYG editor coming soon."
    date: 2001-12-11
    body: "WYSIWYG is a myth in the Web world. What you see in Dreamweaver or any other such editors is KIND OF what you get. Dreamweaver is NOT the only solution forWeb design/development, but I have to admit... it is one of the best. I guess what Dreamweaver excels at that Quanta does not have is the fact that it lets you format things visually and interactively. It lets you see KIND OF what you get with tables... adding and deleting COLUMNS of tables is HELLISH by hand if you would pardon my strong language here.\n\nWhat I AM going to do for Quanta is to list out some specific things about Dreamweaver that shines:\n- Table formatting - albeit inaccurate sometimes, but that's the BEST way really to format Web pages\n- Visual feedback of what font/size would be on the page\n- Library/template engine makes maintaining a large Web site a breeze\n- Behaviors - predefined/wizards to JavaScripts is nice because I don't care to write the same roll over code twice\n- Customization - the extensions (perhaps we can make plugins easily without compilation of source code...??)\n- It is cross-platform in Win & Mac... honestly, Linux isn't the choice in Web DESIGN simply because it doesn't have something like Dreamweaver for me!\n\n\nBTW... Does Quanta HAVE to affiliate with KDE?  I mean, if it only depends on QT, can't we port it to the other OS's as well??\n\nJust a thought... email/reply to this as you have comments/suggestions!!"
    author: "Kenneth Chau"
  - subject: "Re: WYSIWYG editor coming soon."
    date: 2001-12-11
    body: "Quanta has a CSS editor, but I know what you mean about tables.  God, nested tables within tables to align data is just a nightmare with HTML.  I've run your ideas by the engineers.\n\nQuanta Gold runs on Linux and Windows today, and has since the day it was released.  We are working on the Mac version now and should have it available by the end of the month."
    author: "Shawn Gordon"
  - subject: "Re: WYSIWYG editor coming soon."
    date: 2001-12-11
    body: "Shawn, you are awesome. 12 minute response time. I'll note that about Quanta :)  Who knows how long before folks at Macromedia would take before replying to ANYTHING I write. Once again, thank you."
    author: "Kenneth Chau"
  - subject: "Re: WYSIWYG editor coming soon."
    date: 2001-12-13
    body: "> Table formatting - albeit inaccurate sometimes, but that's the BEST way\n> really to format Web pages\n\nArgh... NO!  That is not the *best* way - it's an extremely ugly hack that is only necessary to support the layout in browsers that don't support standards (standards that are fairly old now).\n\nPlease, if you add the capability to lay out pages with tables, don't make it the default, whatever you do.  It's about time people just bit the bullet and accepted the fact that it is OK to have old-fashioned layout for text browsers and five year old browsers."
    author: "Jim"
  - subject: "Re: WYSIWYG editor coming soon."
    date: 2006-04-01
    body: "That was many years ago... I've since moved on from using tables for laying out all of my sites. With advances like CSS/XHTML/JS/Ajax and what have you, you can really ditch the WYSIWYG world completely! Just mod your code and hit a refresh on your browser to see your results..."
    author: "Kenneth Chau"
  - subject: "Re: WYSIWYG editor coming soon."
    date: 2001-12-12
    body: "Just thought I'd toss my 2 cents in here on this.  I too would happily toss down cash on a reasonably decent WYSIWIG editor for web pages.  I've been using Dreamweaver quite a bit since back to version 2.0 of their product.  Presently I own a copy of 3.0 and haven't upgraded yet due to way too much focus on wiz bangy features that I don't want or need.\n\nAs a web developer I generally start my design work in Dreamweaver to get my layout put together.  DW's HTML isn't perfect, but it's close enough.  I would then take that code from that and bring it into an app like HomeSite to clean up the code a bit to my liking, and add my PHP into it.  After the initial design work I rarely bring code back into DW to work on it, unless the page remained static.\n\nMain features that I personally require out of DW are:\nTable editing\nImage placement\nForm building\nCSS creation and editing\nBasic text formatting\n\nWhat I don't use or need:\nPrebuilt Javascript\nFloating frames\nTemplates\nDW Objects\n\nEven without the layout options, there are a number of goodies in the file management that could be worked into something like Quanta.  Namely, the mapping of a remote FTP site to the local directory structure.  It is pretty cool to be able to click on a file and simply say \"put\".  This is far more than just cool when you've got a client on the phone requesting a stack of little changes in various parts of the site.\n\nMaybe a little later I'll bug ya with a full rant on what this developer would consider a full suite of web tools would consist of.  Quanta is a damn fine start down that path, but it's not the whole picture."
    author: "Michael Collette"
  - subject: "Re: WYSIWYG editor coming soon."
    date: 2003-05-29
    body: "HTML tables are hellish in Dreamweaver! DW adds an incredible amount of garbage to the code; I have no idea why. Often what you see in DW design view looks nothing like in IE. DW always seems to add extra space that you don't want, and then when you try to delete it, it throws the entire table out of whack. I try to fix it in the code, but DW has such a strange way of doing things, this is very difficult. I saw your site and wrote this after many hours of unsuccessfully attempting to place additional table cells alongside a prewritten table that I imported into DW. Anyone who sings the praises of DW probably has not used it much, but what is a better alternative?"
    author: "gringoloco"
  - subject: "Re: WYSIWYG editor coming soon."
    date: 2001-12-13
    body: "I've been using Quanta for around a year and love it. When testing pages in Windows I use UltraEdit. I think people asking for WYSIWYG features in Quanta are making a category mistake.\n\nI see Dreamweaver as the kind of tool a graphic designer might use but not something a person interested in keeping up with W3C standards, studying  browser quirks &etc. might feel the need of. \n\nI see Quanta as a tool that suits the latter kind of developer - it adds convenience to code writing for the person who enjoys both mastering it and increasing their technical vocabulary. If a person views  HTML and related languages as an unenjoyable burden Dreaweaver would be more likely be their  tool of choice. For instance; IMHO it's well worth the  time learning Javascript than have Dreaweaver spit out dozens of lines of convoluted code to perform a simple image swap that is best performed with three lines or so of code.\n\nWYSIWYG features would just get in the way if introduced into Quanta.\n\nThanks to all who developed Quanta - something I've used nearly daily of late. I really appreciate it."
    author: "Stognoster"
  - subject: "Re: WYSIWYG editor coming soon."
    date: 2003-02-12
    body: "I like the fact that Quanta is kind of paired down in comparison to DW, however having said that there is stuff in DW that I feel make life much easier.\n\n1) Visual handling of tables.\n2) Javascript for mouseovers.  I like putting one image path on a toolbar follwed by the swap image.  But I would also like this in Quanta.  \n3) Disjointed rollovers fireworks plus DW make this a so easy could you do the same?\n4) Style sheet generation in the home area of the initial page.\n\nTo be fair DW allows you to edit code directly just most designers don't.  I never generally have to leave the DW environment when building.\n\nAlso could this run in the Gnome interface pleeeeeeeeeeeeeeeeeease!"
    author: "Symo"
  - subject: "Installation fails... Still !"
    date: 2001-12-12
    body: "I am tired of the KDE bz2 install, it always failed !\n\nWhen you look at the readme file all is easy, you unzip then you do ./configure then make then make install (as root). Yes, easy, but it NEVER works !\n\nSurely - perhaps - it is not the fault of KDE, it is Mandrake (8.1) or my install !... \n\nNEVER on Windows I have failed an install. On Linux/KDE all the bz2 install failed, It is tiring and frustrating... And there is no easy message to help, only :\n\nchecking for libz... configure: error: not found. Check your installation and look into config.log\n\nWith such things, Linux is still an OS for hackers, not for end users...\n\nWell, I see that the Readme file of Quanta Plus is better than others, it is said :\n\nShould you have trouble during ./configure or the make process, try the\n following:\n - export KDEDIR=<path to kde installation> and export QTDIR=<path to qt2>\n  before running ./configure.\n - run  \"make -f Makefile.cvs\" before ./configure .\n\nBut where is my KDEDIR, how I find it ? And the QTDIR ? Why there is not an automatic search of this path ? \n\nNow for non hackers user, it is impossible to run the non packaged KDE apps. There is something to do..."
    author: "Alain"
  - subject: "Re: Installation fails... Still !"
    date: 2001-12-12
    body: "You are right, installation from sources on some systems/configurations can be painful. You can try to:\n- search for a binary package. For the wider spread apps, the distros usually make binary packages. I checked the SuSE site, and there is already an rpm for Quanta 2.0. Ask for your distribution maker to do a package for this.\n- dig a little on the net and in the Howto-s, Readme-s and log files and try to install it. Most of the time when you succeed with installation of one KDE app from the sources, you will not have more problem with other apps (because you already \"corrected\" your system. Maybe you have to use the same flags for ./configure as for the first app.\n\nIn your case what you can do is:\n\n> checking for libz... configure: error: not found. Check your installation and look into config.log\n\nAs it says, look at the end of the \"config.log\" file to see what's the problem. 99% it will complain about a missing header file (zlib.h). In this case you should install the libz-devel (or something similar) package. As a general rule, when compiling from sources, you need also the devel package for your libraries (KDE, Qt, libz, ImageMagick, etc.)\n\nKDEDIR and QTDIR should be set by the distro, but if it fails, they should point to your KDE (and QT) installation. Eg. KDE is in /opt/kde and QT in /usr/lib/qt2, than set these variables to these paths.\n\nI will try to make a better Readme, but I don't think that in one Readme file I can describe every solution why configuring (compiling, linking) fails. But I will try..."
    author: "Andras Mantia"
  - subject: "Re: Installation fails... Still !"
    date: 2001-12-12
    body: "> As a general rule, when compiling from sources, you need also the devel package for your libraries (KDE, Qt, libz, ImageMagick, etc.)\n\nThank you... I think it is the cause. I am not a developper, so I did not install the development packages in the Mandrake installation. There are many ones... Pfff... it is not obvious..."
    author: "Alain"
  - subject: "Plus vs Gold"
    date: 2001-12-13
    body: "Hmm, \n\ni just want to know the difference between thoose two version ? \n\nis there any advantage of buying the Gold version ?"
    author: "somekool"
  - subject: "Re: Plus vs Gold"
    date: 2001-12-17
    body: "try out the demo and see for yourself.  QG has fixed a number of inherit design flaws that caused performance problems on large projects, it is very actively developed, we have updates every 1 to 2 weeks since it was released.  Support is typically rapid.  You get all available platforms for a single price.  There is a pretty comprehensive manual, integrated CVS support, advanced FTP integration, etc."
    author: "Shawn Gordon"
---
<a href="mailto:sequitur@kde.org">Eric Laffoon</a> reports that the stories of the demise of <a href="http://quanta.sourceforge.net/">GPL'd Quanta</a> have been greatly exaggerated, and to prove it, the Quanta Plus developers have released <a href="http://quanta.sourceforge.net/release2.php">Quanta Plus 2.0</a>.  For those of you who don't know yet, Quanta Plus is a popular and rapid web development tool for KDE.

Some of you will also know that Quanta development has split into 2 branches, one of which is the <a href="http://www.opensource.org/">Open Source</a> KDE project known as <a href="http://quanta.sourceforge.net/">Quanta Plus</a>, licensed freely under the <a href="http://www.gnu.org/licenses/gpl.html">GPL</a>, and the other is a commercial Qt product known as <a href="http://www.thekompany.com/products/quanta/">Quanta Gold</a> supported by <a href="http://thekompany.com/">theKompany</a> and made available under <a href="http://www.thekompany.com/products/license.txt">a commercial license</a>.  Thankfully, both projects are well and alive, and have their place in this world.  Hey, and both look great, too (<a href="http://quanta.sourceforge.net/screenshots.php">Quanta Plus</a>, <a href="http://www.thekompany.com/products/quanta/screenshots.php3">Quanta Gold</a>).
<!--break-->
