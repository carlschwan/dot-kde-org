---
title: "developerWorks: Introduction to KDE"
date:    2001-05-20
authors:
  - "Anonymous"
slug:    developerworks-introduction-kde
comments:
  - subject: "Neither Java, Javascript nor Flash?"
    date: 2001-05-19
    body: "What interests me about that misunderstanding is why the lead programmer of Loki would think that Konqueror does not support these!\n\nAs far as I know all these options are turned on by default, so that cannot be the problem.\n\nWere his Netscape plugins not in one of the default Konqueror nsplugin paths? No JDK installed? (Netscape ships with its own JRI, again AFAIK which may not be much)\n\nOr is it too hard to make these operational somehow?"
    author: "Rob Kaper"
  - subject: "Re: Neither Java, Javascript nor Flash?"
    date: 2001-05-19
    body: "Are they really enabled by default?  On my Mandrake 7.2 with KDE 2.1.1 binary packages downloaded from the official Sourceforge mirror, Java, JavaScript are disabled by default and nsplugins/flash don't work at all (they used to work on my old Debian system though).\n\nHowever, in KDE 2.1.1 Konqueror comes with a nice splash screen that easily lets you enable Java/Javascript/Flash, so Java and Javascript were easy to turn on and work fine.  Not sure if that was the case in KDE 2.1 which Sam is reviewing, also note that he is using a very old Red Hat."
    author: "Navindra Umanee"
  - subject: "Re: Neither Java, Javascript nor Flash?"
    date: 2001-05-19
    body: "If his RH system is very old, it may only have a pre-1.2 JDK which is only supported in a limited fasion. For decent Java support you need a Java 2 VM (such as the one from IBM...) so that Wynn's improvements can be used.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Neither Java, Javascript nor Flash?"
    date: 2001-05-20
    body: ">Are they really enabled by default?\n\nNope, we decided against enabeling it by default on kfm-devel for security reasons. But yes, the splashscreen should enable everyone to find his way to the corresoponding configure switch :)\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: Neither Java, Javascript nor Flash?"
    date: 2001-05-20
    body: "Also, if I recall correctly, Mandrake enables them by default. Other distros may as well.."
    author: "A sad person"
  - subject: "no flash :("
    date: 2001-05-22
    body: "on mandrake 8 i got lot of trouble for having a working flash.\n\nflash was not installed by default on my system and now after my manualy install ...all browser take a lot of time to initialize (mozilla,konqueor,galeon...).\n\nI'am working in flash and i need it.\n\nif someone can help me ..."
    author: "blitz"
  - subject: "Re: Neither Java, Javascript nor Flash?"
    date: 2001-05-21
    body: "I have SuSE 7.1 and global support for both Java and JavaScript are disabled by default. Netscape plugin for Flash is enabled by default."
    author: "Steven Bosscher"
  - subject: "Re: Neither Java, Javascript nor Flash?"
    date: 2007-04-12
    body: "I have Vista...does that insinute a reason???"
    author: "Ricklloyd@ rogers.com"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-19
    body: "He sounds very positive all the way up to the conclusion:\n\n<I>KDE has a little way to go before I would consider it a solid desktop environment, but it's definitely worth playing with and deserves its place among the leading Linux desktop environments.</I>\n\nThat doesn't strike me as a very constructive remark. He doesn't give any explanation to why it isn't \"a solid desktop\" and maybe I'm biased but I really can't see any desktop for Linux that equals KDE. Considering there are only 2 or 3 (does xfce count?) to \"deserve a place among\" them doesn't seem very hard :-(\nI hope he doesn't consider Window Manager == Desktop Environment"
    author: "konqi"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-19
    body: "You know, Sam is a personal friend of mine and a great guy in general, a year ago he was a Gnome fan and hadn't looked at KDE at all.  I talked him into looking at KDE, but  as far as I know he has barely ever looked at it.  Between getting married 2 weeks ago, his honeymoon, carrying the bulk of the work at Loki and maintaining SDL, I'm surprised that he got this writting gig at all, especially concerning KDE.  I'm glad that it was basically positive, but I wish he had asked me a few questions while he was doing it :)."
    author: "Shawn Gordon"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-20
    body: "Wow, that was a constructive name dropping comment. Thanks Shawn."
    author: "me"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-21
    body: "My biggest qualm is the size of those screenshots.  The Loki screenshot (huh?) is *huge*, right in the middle of the review, and seems to be pretty durn pointless.  Although at least he did mention his bias.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-20
    body: "There's oen very little thing bugging me: The way you post the stories. This isn't meant to be a troll or flame, let me say what I mean: \n\nIn all your article-posts, every word that could possibly be linked to something is linked. Be it to www.ibm.com or to www.konqueror.org. While I think this is a little exaggerated, I can't really argue against it.\n\nBut please, never link to the same thing twice. In the above post, both \"review and introduction\" and \"the article\" both point to the same thing. So what I do is hover over the links and find out where they'd take me, then compare and realize that one of them is redundant.\n\nLaughing at me now? You're probably right, I'm getting silly. Anyway, just some little thing bothering me... ;)"
    author: "me"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-20
    body: "Point taken.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-20
    body: "Use an intelligent browser. Both Konq and Netscape show you the url in the status bar when you hover over a link. No need to click on the link to see it's the same as above."
    author: "Inorog"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-21
    body: "\"So what I do is hover over the links and find out where they'd take me, then compare and realize that one of them is redundant.\"\n\n???"
    author: "me"
  - subject: "OT: redundand links"
    date: 2001-05-21
    body: "\"me\" is right. Avoiding redundant links belongs\nto basics of www-page design. One rule of thumb\nis that if user can see two links simultaneously\n(in the text body), they should not point to same target. Redundancy is confusing and makes\nuser to consentrate on form rather than on contents."
    author: "Eeli K"
  - subject: "LinuxWeeklyNews review"
    date: 2001-05-20
    body: "Has anybody read the <A HREF=\"http://www.LWN.net/2001/0517/desktop.php3\">review</A> \non LWN?\nHe sais that anti-aliased font support is terrific, but then he complains about a missing AAF installation. As if this wasnt mainly up to the distribution to install the fonts (e.g. like mandrake which incorporates fonts from win partitions AFAIK ). I heard a KDE font installer is also under way.\n<p>\nBut then he writes: ...I know I don't have anti-aliasing enabled in my XFree86 server or in the version of the KDE libraries I'm running..\n???\n<p>\nHe sais Konqueror rendering is not good enough, because he cant see is own site Graphics Muse displayed correctyl, among others ( this is a valid point ).\n<p>\nAt the end he wants to remove \"the icons in the root window\". I dont really know what these are, but if its the icons in the task list:\nThey are great, better implemented than on IE5 (without update he sometimes forgets them ) and now even slashdot has one ( on sub-pages)."
    author: "ac"
  - subject: "Icons in the root window"
    date: 2001-05-21
    body: "I think that by \"the icons in the root window\" he means the icons on the desktop. I know many people that like them, but if I had the option to turn them off I would. I never click them and the wallpaper looks better without them. You can kill the \"kdesktop\" process to see the effect."
    author: "Maarten ter Huurne"
  - subject: "Re: Icons in the root window"
    date: 2001-05-21
    body: "Well Maarten, I must say I agree. I think it's rather useless to have icons on your desktop. An option to be able to turn them off would really be nice. Do you know the maintainer of kdesktop by any chance ?"
    author: "Jelmer Feenstra"
  - subject: "Re: Icons in the root window"
    date: 2001-05-21
    body: "I think I have just found a simple solution that doesn't require changing any code. If you delete every item on the deskop, there are no more icons. In the Control Center you can specify directories for Trash and Autostart, Trash is \"~/Desktop/\" by default, so moving that someplace else is a good idea. Apart from that, I didn't see any items that will break KDE if they are removed from the desktop. Still, it's probably a good idea to move rather than delete the desktop items if you want to try this."
    author: "Maarten ter Huurne"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-20
    body: "Konqueros has been claiming \"full\" support for Javascript and Java since years ago.\nBut the truth is that navigating with Konqueror I can't log into the Netscape Messenger server that holds my webmail, navigating with Konqueror I can't download Acrobat Reader because the pop-up menus at adobe(dot)com don't show up.\nBoth Netscape and IE have no such problems and I dont't really care about the \"we comply with standards\", I rather prefer \"we <B>DISPLAY</B> pages\".\nSo let's stop the Konqueror feverish feature-creeping and try to make possible use Konqueror as the _only_ browser needed to navigate instead of forcing the user to have a Navigator handy."
    author: "Missa"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-20
    body: "\"... since years ago\" ;-)\n\nI test this and Acrobat Reader download has nothing to do with Java or JS, I turned it off and get the tar.gz in Konqis embedded arc-view. I think Konqi (here KDE-CVS-2.2pre) can now do 95% of my internet browsing. There are still problems with some jsp-pages (e.g. online-banking, bug-report is still open). Looking at these pages, they are often only bad html. Sorry can't test Netscape Messenger server.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-20
    body: "I have not been able to log into my account at Sourceforge forever it seems with Konqueror, have to use Netscape or Opera.  One would think that this problem would have been cured as long as its been around.  I've used every version of openssl I could get my hands on and still no go.\n\n -jon-"
    author: "Jon"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-20
    body: "No problem here. Sure you use an actual version off openssl? I think its 0.96a. Compile it with\n\n./config  --openssldir=/usr/local/openssl shared\n\nThis directory is one that is auto-detected, for non-standard locations there is a kcontrol-gui in CVS. I can everyone recommend to go with the source code. First there a some traps, but then you have a very stable, full-featured system. Of course, you need a fast computer ;-)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-21
    body: "I am running Mandrake7.2 and 8.0, have used the standard install of the openssl rpm's that come on the cd's and the is0's.  I will try doing as you say and compiling my own version, but one would think that the rpm's that come with the distro would be right for the system, guess I'm wrong on that huh?\n\n -jon-"
    author: "Jon"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-21
    body: "I am running Mandrake7.2 and 8.0\n\nOdd.  With Mandrake 7.2 I kept the system \"clean\" (i.e., only used Mandrake RPMs), and had no problem whatsoever with Sourceforge.\n\nMandrake, on the other hand, was late in releasing updated KDE RPMs (Since the great Chris Moll.* left), so I switched to SuSE last weekend.  Sourceforge works fine there too.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-21
    body: "RPM (and friends) is good for the start of your \"linux-career\" ;-)\n\nI started with SuSE 5.0 and later I bought SuSE 6.3 also, but then I realised that I was unhappy with the delay between the development in the community and the production of a new version or update for a distribution, so I started to drop the distribution system. \n\nOf course you have to know, if you go with the source (== tar.gz) you break your dependency database and maybe your system-administration-tool\n\nToday I am not afraid switching to a new glib or gcc version. For compiling my KDE-CVS (with all the stuff) I have little scripts. I have my own boot&rescue floppy disc with kernel 2.4 and ReiserFS-3.6 (based on busybox ;-) - you get it: you need some time to learn.   \n\nBut once again, distributions are the right start, for the source you need a fast computer (XFree-CVS needs some time :-)) and you need more than basic skills.  \n\nIf you know your system, you will really like it.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: developerWorks: Introduction to KDE"
    date: 2001-05-20
    body: "Looking forward to your bugfix patches."
    author: "anonymous"
  - subject: "Konqueror, the lacks of the default installation ["
    date: 2001-05-20
    body: "> (the author states that Konqueror supports neither Java, Javascript nor Flash) \n\nAfter installation of Mandrake 8.0, I had JavaScript, not Java nor Flash.\n\nI tried to install Java by doing what is said on a page of the Konqueror site, but it failed... And for Flash, I had big difficulties to understand that the rpm netscape-plugins is missing in the 2 CD of Mandrake... After, yes, I succeeded to install Flash...\n\nAlso, in Konqueror, I have the network neighborhood only by being logged in root. And, before, it is nor pretty to launch the \"lisa -K\" command...\n\nAnd I had difficulties to use the Network-FTP (I succeeded, and it is a very good function, however there are some few issues as the disabled up button), and the Network-HTTP (I failed, however it seems less important) (and there is no documentation...)\n\nSo I don't think it is false to say that Konqueror has not all its pretended functions...\n\nBefore adding new functionnalities, I feel important to give all the existing ones, without having something to do, excepted - perhaps - updating some easy parameters...\n\nBut Konqueror is a fantastic programm, I enjoy to use it !...\n\n(and I am surprised with the favicons. Of course it is very pretty and useful, but it is not free (Cf. http://www.favicon.com)... Is it not possible to create another system - with an icon.png file for example -, a free system ? Here a collaboration between Nautilus-Galeon-Mozilla-Konqueror may be interesting...)"
    author: "Alain"
  - subject: "Re: Konqueror, the lacks of the default installation ["
    date: 2001-05-21
    body: "After installation of Mandrake 8.0, I had JavaScript, not Java nor Flash.\n\nMandrake 8 (and the recent lack of attention to KDE updates to 7.2) turned me off from Mandrake.  This is not a flame or troll at all; I'm merely voicing my opinion that to me it appears that Mandrake has become *much* much less KDE focused lately.  With their system tools all GTK, never releasing packages on time for KDE updates, and Mandrake 8.0's poor KDE install.\n\nAgain, this is only my opinion recently about Mandrake; until recently I was referring people who asked about Linux to their distro, and I am curious if it was just me, or if other people have the same feeling I do about Mandrakes sudden drop of interest in KDE.\n\n(And for ghods sake, don't reply \"Well, -foo- is better because...\"  I'm not trying to start a distro war).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Konqueror, the lacks of the default installation ["
    date: 2001-05-21
    body: "Can you tell us more about the poor KDE install other than what Alain has told us already?  What has changed with regards to 7.2?  I am running Linux-Mandrake 7.2 and considering an upgrade but it will not be to Linux-Mandrake 8.0 if things are as bad as you say they are."
    author: "KDE User"
  - subject: "Re: Konqueror, the lacks of the default installation ["
    date: 2001-05-21
    body: "I must agree, Mandrake has allways been my no1 choise of Linux dists. but I have lately started to look for somthing more KDE oriented. And of course the GTK drake stuff in Mandrake doesnt help! It's in the right direction but it should defenantly have been done in QT. Prefarable integrated in KDE Control center.\n\nAnd looking at Suse and Caldera I must say they are defenantly comming along!\n\nJust my true opinion."
    author: "DiCkE"
  - subject: "Re: Konqueror, the lacks of the default installation ["
    date: 2001-05-21
    body: "Yes, Evan, I am OK with your analyse. And if I find a better KDE distribution (with the Gnome programs, too), I will leave Mandrake.\n\nThings are not still really bad, but it does not go in a good direction for KDE users. And we are KDE users before being Mandrake users...\n\nIn Mdk 8.0 (the very first version), two others things irritated me :\n\n- it is impossible to use the child panels extensions, because they are empty after restarting KDE. It seems to have been a very temporary bug of KDE, but nothing is said in the Mandrake site and MandrakeUpdate does not correct this\n\n- After some manipulations, I had very bad fonts in Konqueror. I had many difficulties to fix this (I don't even know what exactly was the cause and the solution...), and there was nothing in the Mandrake site (excepted a complex page) and in the MandrakeUpdate...\n\nSo I hope that some day the KDE team will directly manage a FTP KDEUpdate..."
    author: "Alain"
  - subject: "Re: Konqueror, the lacks of the default installation ["
    date: 2001-05-24
    body: "And now I see something else :\n\nKuser is enable to change the passwords, it seems buggy. In reality, Mandrake manages the NIS passwords (even if NIS is not installed !). So only DrakeConf may change the passwords !...\n\nAnd nothing is said to the user, so he only thinks he has a buggy KDE program... :-("
    author: "Alain"
  - subject: "Re: Konqueror, the lacks of the default installation ["
    date: 2001-05-21
    body: "Mandrake is a complete rip off of RedHat, the only diffrence is that it defaults to KDE, GNOME is getting to the point where it can compete, so mandrake is not duplicating effort, instead they are copying RedHat, which happens to have more GNOME support."
    author: "AC"
  - subject: "Re: Konqueror, the lacks of the default installation ["
    date: 2001-05-21
    body: "About favicons: what do you mean by not free ?\nDo you know that you can create .ico files from within KDE itself (using kiconedit) ?\nWhy develop a new incompatible system (and lose all the existing favicons) when the existing one works ok ?\n\nThe .ico file format was initially invented by Microsoft. So what ? We can read _and_ write .ico files, so it's just one format among many others, as open as it can get."
    author: "David Faure"
  - subject: "Favicons"
    date: 2001-05-21
    body: "David says :\n\n> About favicons: what do you mean by not free ?\n\nI may be wrong...  I must be wrong ;-)... Happily. And so I deny what I said. THanks for your informations.\n\nI saw nothing in the Konqueror documentation, nothing in Konqueror or KDE sites. Very strange...\n\nI searched on the Web and I only found the www.favicons.com site, where the favicons seem not to be free... I asked in a KDE forum, no answer... Very strange...\n\nThere are many and many new things in Konqueror, it is like a revolution and it is not easy to understand all about...\n\nI still hope to find a clear documentation about the mechanisms of the favicons (at least, is it the good word ?)"
    author: "Alain"
  - subject: "Re: Favicons"
    date: 2001-05-21
    body: "Favicons are just a standard 16 by 16 picture in .ico format, placed in a particular location on the website (I believe /favicon.ico). Internet explorer 5 and above as well as a few other browsers (like Konqueror) check for a favicon for every site they connect to.\n\nhttp://www.users.f2s.com/faq/favicon.php3\n\nmight be informative."
    author: "Jon"
  - subject: "Re: Favicons"
    date: 2001-05-22
    body: "Thank you, Jon. However, this good page http://www.users.f2s.com/faq/favicon.php3 is not plainly informative. Happily, it is possible to add the missing informations ;-)...\nAnd I note that favicons are missing in the bookmarks of Konqueror ! Perhaps in next version ?..."
    author: "Alain"
  - subject: "Re: Favicons"
    date: 2001-05-22
    body: "I wrote :\n\n> I note that favicons are missing in the bookmarks of Konqueror\n\nTrue for pre-installed bookmarks, wrong for users created bookmarks, because they have their favicons in the menus.\n\nFor the pre-installed bookmarks, it would be fine that they get their favicons the first time they are called..."
    author: "Alain"
  - subject: "How do you install flash?"
    date: 2001-05-23
    body: "Or realaudio ?\n\nI added all these things to mozilla, but I didn't found how to do this on konqueror..\n\nI didn't found any documentation on this either..\n\nThanks."
    author: "reno"
  - subject: "So little trolls..."
    date: 2001-05-20
    body: "What's your secret? Gnotices is filled with trolls.\nDo you guys delete posts?"
    author: "xv"
  - subject: "Re: So little trolls..."
    date: 2001-05-21
    body: "Gnomers made themselves a target to M$. You can have the M$ FUD Trolls."
    author: "ac"
  - subject: "Re: So little trolls..."
    date: 2001-05-21
    body: "Unfortunately, KDE articles are trolled to such a point on LinuxToday, that the new direction mhall appears to be rejecting many KDE articles that would be normally published.  Instead he is publishing more GNOME articles."
    author: "KDE User"
  - subject: "Re: So little trolls..."
    date: 2001-05-21
    body: "It always seem like the opposite to me.\nIn my vision, GNOME is being trolled much more than KDE."
    author: "xv"
  - subject: "Re: So little trolls..."
    date: 2001-05-22
    body: "Well, you know what they say...\n\n\"Trolling is in the eye of the troll-ee.\" :)"
    author: "David Watson"
---
<a href="http://www-106.ibm.com/developerworks/">IBM developerWorks</a> is featuring a positive <a href="http://www-106.ibm.com/developerworks/linux/library/l-kdei/">review and introduction</a> to KDE 2.1 by Sam Lantiga of <a href="http://www.lokigames.com/">Loki Software</a>. Despite being slightly out of date, and having a few errors or misunderstandings (the author states that <a href="http://www.konqueror.org/">Konqueror</a> supports neither Java, Javascript nor Flash) the article is generally positive with nice tips and screenshots, and is worth reading especially for users new to KDE.
<!--break-->
