---
title: "KDE Available on CD-ROM"
date:    2001-02-09
authors:
  - "kgranroth"
slug:    kde-available-cd-rom
comments:
  - subject: "Re: KDE Available on CD-ROM"
    date: 2001-02-09
    body: "I regard this for a good thing, but IMHO there is still a need for a KDE installer, so really everybody could easily could get a copy and is able to install it without checking lots of dependancies and spending time with compilation."
    author: "ddd"
  - subject: "Re: KDE Available on CD-ROM"
    date: 2001-02-09
    body: "There should be an ISO-image with built installer available for the latest stable version and beta.\nAt least for the most common distributions, about 4 or so.\nOnce the ISO is downloaded there is no problem with bandwidth, everybody and his dog has a CDR these days."
    author: "reihal"
  - subject: "Re: KDE Available on CD-ROM"
    date: 2001-02-10
    body: "I agree that there should be ISO of KDE like there are ISOs for distributions. In that way KDE would be easy to share with your friends."
    author: "JLP"
  - subject: "Re: KDE Available on CD-ROM"
    date: 2001-02-11
    body: "Look what I found:<br>\n<ul>\n<li>http://realityx.net/kde/kdeinstaller/custom_required.png\n<li>http://realityx.net/kde/kdeinstaller/install_type.png\n</ul>\n<br>\nLooks like something will come out soon."
    author: "Al_trent"
  - subject: "Installer doesn't need to be fancy"
    date: 2001-02-12
    body: "From my perspective at least, I must agree that installing KDE is time consuming and complicated and an installer is needed.  However, I don't think we need a big fancy installer.  When I installed KDE 2 on my PC, I used a shell script provided at Caldera's website and it installed everything perfectly in no time and all I had to do was download a script then type ./update.sh and it's installed!  Something like this script, except capable of determining which files are needed and stuff should be totally adequate to most people's needs.  Small, Simple, and Quick updating.   \n\nKuba Soltysiak\nkuba@kites.org\n\nbtw  KDE 2.1 rocks!  Never seen or imagined a better desktop environment!"
    author: "Kuba Soltysiak"
  - subject: "Install of KDE"
    date: 2001-02-09
    body: "I think it's time for KDE to use an installer. KDE is still the most advanced desktop but it's too difficult to install compared to Helix 's install."
    author: "Jerome"
  - subject: "Re: Install of KDE"
    date: 2001-02-09
    body: "Yes, and compare the \"Helix Update\". An automatic update via ftp like Gnome/Ximian has it would be a great thing."
    author: "juergen"
  - subject: "Re: Install of KDE"
    date: 2001-02-09
    body: "agreed, this would save me a lot of pain to keep our network uptodate. \n\nBut this should include all KDE stuff _and_ all supplemental stuff needed like ssh, qt...\n\nI even would pay for this service. Keeping KDE up-to-date in my office costs me a hell of time. -> money. The installer by Helix (or whatever their name is) is a very strong argument. \nHowever I do not consider GNOME as much developed as KDE. Plus I am more an object oriented person. However, if the first improoves we might consider to change. To catch up with new features & bugs simply costs too much time(money). \n\noliver"
    author: "oliver"
  - subject: "Re: Install of KDE"
    date: 2001-02-09
    body: "From a user's point of view, if the distro install can do it automatically why can't a downloaded version be as easy.<br><br>\n\nI remember the look on my friend's face when I was compiling a downloaded update for KDE 1.x.  Yep, he wasn't going to be giving up Windows anytime soon.<br><br>\n\nEither an upgrade installer should be written or something as simple to use as the Windows Update.  That way more people are likely to feel comfortable with KDE (or indeed any interface)."
    author: "Jason"
  - subject: "Yup, install is essential"
    date: 2001-02-10
    body: "I feel an installer for KDE is essential.  It will help my job as a network engineer tremendously.  \n\nEach time a new RPM for KDE comes out, I feel I have to reinstall my system.  I hope to have one day to seamingly upgrade my system without a full reinstall.  Something like Ximian or HelixUpdate.  Right now I use KDE exclusively, and loving it every bit.  An installer will make me a KDE worshipper. :)  I already think David Faure is a god.  \n\nThank you\n--kent"
    author: "Kent Nguyen"
  - subject: "How 'bout distribution-level install support..."
    date: 2001-02-10
    body: "You know, Debian rocks.  More specifically, Apt.\n\nI'm running Debian Unstable on my main workstation, I had KDE2.1 beta2 builds the day the source tarballs were available.  If I want, I get a snapshot every couple fo days.\n\nI've read that Connectiva (an RPM-based distro) is using apt as well now.  This is truly a fantastic tool.  Want KDE?\n\napt-get update && apt-get install task-kde.  Then, to update EVERY package (not just KDE) run apt-get upgrade.  Poof, you're done!\n\nNow, put a nice pretty front-end on apt. make it easy to find and manage packages, voila.\n\nSeriously, the \"amazing\" Helix installer isn't anything on Debian.  It adds a line to /etc/apt/sources.list and then instructs you to apt-get upgade every now and again.  It only looks amazing on distributions that lack a tool like apt.  \n\nSo, lets make apt more usable and port it to other OSs...  It is package-agnostic....\n\nOn the other hand, an installer on KDE is a good short-term solution..\n\nCheers,\n\nKDE 2.1 is AWESOME!!\n\nBen"
    author: "Ben Hall"
  - subject: "Re: How 'bout distribution-level install support..."
    date: 2001-02-10
    body: "There is a package management tool availible. It's called kpackage :P"
    author: "Torkel Lyng"
  - subject: "Re: How 'bout distribution-level install support..."
    date: 2001-02-10
    body: "Yeah, and it's pretty sweet.  I've found it a little akward as a front-end to apt though.  Maybe I just didn't dig deep enough, but a kde equivalent to stormpkg+enhancements was kinda what I was thinking of.  I don't recall KPackage having working search capabilities.  Again. it could be that I just didn't dig deep enough.\n\nBen"
    author: "Ben Hall"
  - subject: "Re: How 'bout distribution-level install support..."
    date: 2001-02-10
    body: "if u use slackware, there are just 8 kde packages, use autoslack to download all upgraded packages, then installpkg kde* qt* koffice* in /var/spool/packages... done..\n\nIts much more fun to use cvs tho :))\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Earlier discussion..."
    date: 2001-02-11
    body: "This is maybe redundant thanks to Ben Hall's great reply, but this has been discussed earlier in another thread, see <a href=http://dot.kde.org/979149870/979210878/>http://dot.kde.org/979149870/979210878/</A>.<BR><BR>\n\nMy reply can be read at <A HREF=http://dot.kde.org/979149870/979210878/979236657/>http://dot.kde.org/979149870/979210878/979236657/</A>.<BR>"
    author: "Stentapp"
---
One of the most common requests I get is "can I get KDE on CD-ROM?", I'm happy to say that the answer now is "yes!". I've created a page on <a href="http://www.kde.org/">KDE.org</a> that lists all known vendors that have <a href="http://www.kde.org/cdrom.html">KDE on CD-ROM</a>.  I've also noted whether the vendor contributes part of the proceeds back to the KDE Project or not.  Check it out <a href="http://www.kde.org/cdrom.html">here</a>.
If you know of any other vendors, <a href="mailto:granroth@kde.org">send me</a> a quick note.




<!--break-->
