---
title: "IBM makes KDE available for AIX"
date:    2001-01-31
authors:
  - "numanee"
slug:    ibm-makes-kde-available-aix
comments:
  - subject: "2.1 b2"
    date: 2001-01-31
    body: "why 2.1 beta 2 hasn't been announced yet?"
    author: "1"
  - subject: "Re: 2.1 b2"
    date: 2001-01-31
    body: "kde-core-devel, 30. Jan, Richard Moore in reply to a similar question:\n\n\"I think there are still a few packaging issues: there have been some mails about troubles with kde-i18n-2.1beta2 on the docs\nlist.\""
    author: "Marco Krohn"
  - subject: "Re: 2.1 b2"
    date: 2001-01-31
    body: "I know I've had no end of troubles with the latest 2.1beta debs.  Odd too, since they're usueally so good..\n\nAnyone know where to post debian specific bugs to?\n\n(My issues have been that Konq won't draw all graphics and that clicking URL .desktop files won't work.  The first was fixed by compiling from sources. I'm still at a loss on teh second one..)\n\nBen"
    author: "Ben Hall"
  - subject: "Posting debian specific bugs"
    date: 2001-01-31
    body: "try the debian-kde mailing list,\n(subscribe/unsubscribe through\ndebian-kde-REQUEST@lists.debian.org).\nu can brows the archives on lists.debian.org"
    author: "coba"
  - subject: "Pure evil"
    date: 2001-01-31
    body: "Note:  I'm posting this here so you'll read it before you see the post below.  \n<b>DON'T CLICK ON THE \"SCREENSHOT\" LINK IN THE NEXT COMMENT!!!</b>  Trust me, you don't want to know...    Please, somebody delete that post (and this one too afterward)"
    author: "not me"
  - subject: "Postings coming from Australia"
    date: 2001-01-31
    body: "I've deleted the thread and I'm going through the other stories as well.  This is the IP of the moron:\n\n202.139.64.38 - - [30/Jan/2001:22:07:49 -0500] \"POST /980887194/. HTTP/1.0\" 200\n3168 \"http://dot.kde.org/980887194/addPostingForm\" \"Mozilla/4.72 [en] (X11; U; Linux 2.2.18 i586)\"\n\nproxy5.dynamite.com.au  A       202.139.64.38\n\ninetnum:     202.139.64.0 - 202.139.79.255\nnetname:     DYNAMITE\ndescr:       CWO Customer Network\ndescr:       GPO BOX 4658TT\ndescr:       Melbourne VIC\ncountry:     AU\nadmin-c:     OA3-AP\ntech-c:      OA3-AP\nmnt-by:      MAINT-OPTUSCOM-AP\nchanged:     ipadmin@cwo.net.au 20010113\nsource:      APNIC\n \nrole:        OPTUS IP ADMINISTRATORS\naddress:     101 Miller Street North Sydney\nphone:       +61-2-93427681\nphone:       +61-2-93420848\nphone:       +61-2-93420983\nphone:       +61-2-93420813\nfax-no:      +61-2-9342-0998\nfax-no:      +61-2-9342-6122\ne-mail:      noc@optus.net.au\nadmin-c:     NC8-AP\ntech-c:      NC8-AP\ntech-c:      SC120-AP\ntech-c:      DB30-AP\ntech-c:      CB39-AP\ntech-c:      EH19-AP\nnic-hdl:     OA3-AP\nmnt-by:      MAINT-OPTUSCOM-AP\nchanged:     chris@optus.net.au 20000407\nsource:      APNIC\n\nFeel free to contact their admin and get this twink into trouble.  We can stop the abuse.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Postings coming from Australia"
    date: 2001-01-31
    body: "what did he/she do that was so bad? i can't tell because the post has already been deleted\n\nnathan"
    author: "nathan"
  - subject: "Re: Postings coming from Australia"
    date: 2001-01-31
    body: "Posting of porn on a couple of articles + posting of oversize articles in a Denial of Service attempt.\n\nBtw, I would like to add that in general we do not go out of our way to track IP addresses and that this information is generally kept private and protected. We do not generally delete articles either.  \n\nIn the case of abuse however, things have to be handled differently, hopefully to the benefit of everyone truly interested in KDE news and developments.\n\nThanks,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: Postings coming from Australia"
    date: 2001-01-31
    body: "Well done Navindra. Is it possible to filter his postings out in the future?\nRegards"
    author: "Zeljko Vukman"
  - subject: "Re: Postings coming from Australia"
    date: 2001-01-31
    body: "lets do this to gnomers, too. (STOP ! JUST KIDDING !!! PEACE, LOVE, UNDERSTANDING AND STANDARDS !!!)"
    author: "AC"
  - subject: "Re: Postings coming from Australia"
    date: 2001-01-31
    body: "Fuck You and your censorship"
    author: "Anonymous Coward"
  - subject: "Re: Postings coming from Australia"
    date: 2001-01-31
    body: "Fuck you for your bad manners."
    author: "Lenny"
  - subject: "Oh go home."
    date: 2001-01-31
    body: "It's their board.  They can do whatever they darn well please.  Don't like the rules?  Don't post here.  It's that simple.\n\nThe posting was removed because it was insanely offtopic.  It should be treated as a spam message.  Not only that, but it was deceiving as well.\n\nIf you think it's okay to be dishonest then you should definitely not be here.  Go start your own message board that welcomes spam and lying.  Let's keep dotkde clean.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Oh go home."
    date: 2001-02-01
    body: "Collapse the thread and modify the subject line.  If you can't do it, don't modify and/or remove the post.  Censorship in any shape and form is scary.  We're all adults here (or, as the case may be, close enough).  We don't need to be protected from pr0n.\n\nNotice the Slashdot editors NEVER, under ANY circumstances modify or remove a post.  Even if it's misleading (Clicking on reply-to takes you to goatse.cx).  There's a reason for this.  It's called integrity.  Censorship is always a unilateral, subjective decision.  And for those reasons, it is inherently wrong.  I wouldn't expect to have to explain these basic principles to intelligent people posting to (or administrating) a Linux/UNIX oriented message board.  As it was written, I would hope that EVERYONE in the Open Source community would hold these truths to be self evident.\n\nNo, I'm not kidding."
    author: "James"
  - subject: "Re: Oh go home."
    date: 2001-02-01
    body: "Collapse the thread and modify the subject line.  If you can't do it, don't modify and/or remove the post.  Censorship in any shape and form is scary.  We're all adults here (or, as the case may be, close enough).  We don't need to be protected from pr0n.\n\nNotice the Slashdot editors NEVER, under ANY circumstances modify or remove a post.  Even if it's misleading (Clicking on reply-to takes you to goatse.cx).  There's a reason for this.  It's called integrity.  Censorship is always a unilateral, subjective decision.  And for those reasons, it is inherently wrong.  I wouldn't expect to have to explain these basic principles to intelligent people posting to (or administrating) a Linux/UNIX oriented message board.  As it was written, I would hope that EVERYONE in the Open Source community would hold these truths to be self evident.\n\nNo, I'm not kidding."
    author: "James"
  - subject: "Re: Oh go home."
    date: 2001-02-01
    body: "a) Adults? Says who? I have gotten mail from 12 year olds who use KDE. They surely got my email address from a KDE webpage. They could come here too.\n\nb) Why should a privately owned and operated site be forced to carry any content some idiot thinks is funny? \n\nc) Slashdot HAS in the past removed posts.\n\nd) It can be called censorship, it can also be called moderation. I suggest you go and rant at LinuxToday about how they moderate each and every post previous to public display."
    author: "Roberto Alsina"
  - subject: "Re: Postings coming from Australia"
    date: 2001-01-31
    body: "Thank you Joe McCarthy.  Fuck off."
    author: "James"
  - subject: "Re: Postings coming from Australia"
    date: 2001-02-01
    body: "Posting dirty stuff was not wise, but posting the IP in a public forum is (IMHO) even more stupid.\n\nCheers,\n\n--fred"
    author: "None"
  - subject: "Re: Postings coming from Australia"
    date: 2001-02-01
    body: "No it was very wise, I can't think of a better deterrant."
    author: "reihal"
  - subject: "Re: Postings coming from Australia"
    date: 2001-02-02
    body: "Well indeed, as I was deleting articles, the perp. was simply posting more and would have kept it up. That was abruptly stopped with the IP post. \n\nHonestly, some of us really don't have the time or patience for nonsense like this, and it is pretty braindead easy to not get your IP posted here.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: IBM makes KDE available for AIX"
    date: 2001-01-31
    body: "I think this is great news for KDE, especially getting noticed and adopted by some of the 'bigger players' in the market....\n\nCongrats!"
    author: "Another One"
  - subject: "Re: IBM makes KDE available for AIX"
    date: 2001-01-31
    body: "Hey, is this supposed to be about IBM ?\n\nAnyway, thanks to IBM for the KDE stuff."
    author: "anonymous coward"
  - subject: "Re: IBM makes KDE available for AIX"
    date: 2001-01-31
    body: "You will find all kde 2.01 packages for AIX 4.3.3 on IBM's ftp server. I've installed it on my RS/6000 F40 without problems now. It works fine."
    author: "Stephan Boeni"
  - subject: "Re: IBM makes KDE available for AIX"
    date: 2001-01-31
    body: "That's great. At my company we've always bought Dell computers, but as a thank you to IBM for their Linux efforts I will consider to switch."
    author: "Matt"
  - subject: "Re: IBM makes KDE available for AIX"
    date: 2001-01-31
    body: "I use IBM hardware exclusively for Intel servers, and also a lot of RS/6000s, and can say their hardware is excellent.  They really know how to build it.   And IBM's Linux support for their hardware in recent months has been nothing short of astounding...."
    author: "Ray DeJean"
  - subject: "Re: IBM makes KDE available for AIX"
    date: 2001-01-31
    body: "> And IBM's Linux support for their hardware in recent months has been nothing short of astounding....\n\nExcept never mention \"dual-boot\" to their technical help line. A problem on a T20 here that was actually caused by a BIOS problem would apparently fixed by \"going into FDISK\" and \"deleting your Linux partition\" as \"we don't support dual boot machines\". Nice. Good to see that technical support lines still have little clue - even if the high ups in IBM are putting big bucks into Linux."
    author: "Dr_LHA"
  - subject: "Re: IBM makes KDE available for AIX-And A lot more"
    date: 2001-01-31
    body: "It's a little bit interesting how this post implies IBM has prted specifically KDE to AIX.  This is not the case.  IBM released a 'PowerPack' of Linux tools for AIX, Development tools such as gcc, gdb etc, KDE & GNOME, and Many, many other packages.  It may have been less tacky to mention the other packages."
    author: "James"
  - subject: "Re: IBM makes KDE available for AIX-And A lot more"
    date: 2001-01-31
    body: "this is a kde site, thus the focus is on kde"
    author: "caatje"
  - subject: "Re: IBM makes KDE available for AIX-And A lot more"
    date: 2001-02-01
    body: "Hey, we're just relaying the information that IBM sent us.  Aren't you glad they packaged KDE 2.0.1 for us?  And that they promised to follow new releases of KDE closely?\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: IBM makes KDE available for AIX-And A lot more"
    date: 2001-02-01
    body: "Definitely, I think it's highly relevant too:)  It's something I was entirely unaware of, and am glad to know:)\n\nSome context would have been nice though:)  Didn't mean for the tone of that post to be negative.  Sorry:)"
    author: "James"
  - subject: "How about SGI"
    date: 2001-02-01
    body: "I've been searching for about 2 months for anyone that has any information on how to get KDE 2.* working in Irix.  So far I can't find anyone.  Hasn't anyone been able to get it to work?<BR><BR>\nWhile KDE does compile using GCC, everything core dumps (apparantly caused by a bug in the SGI gcc port).  I can't get anything compiled using the SGI MIPSPro compiler.  Anyone have any better luck?"
    author: "Falrick"
---
<a href="mailto:arbab@austin.ibm.com">Reza Arbab</a> wrote in to inform us that IBM has made KDE 1.1.2 and KDE 2.0.1 <a href="http://www.ibm.com/servers/aix/products/aixos/linux/index.html">available</a> (download <a href="http://www-1.ibm.com/servers/aix/products/aixos/linux/download.html">here</a>) for AIX. Reza intends to keep up to date with the latest KDE releases, but does need help with currently OS-specific parts of KDE (related to sound, kcontrol info panels, etc). Thanks Reza, thanks IBM.




<!--break-->
