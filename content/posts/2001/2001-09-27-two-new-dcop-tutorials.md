---
title: "Two New DCOP Tutorials"
date:    2001-09-27
authors:
  - "Dre"
slug:    two-new-dcop-tutorials
comments:
  - subject: "Scrip language, Rexx port for KDE / DCOP?"
    date: 2001-09-27
    body: "Using DCOP as an automation interface from scripst is very interesting. Do someone know if KDE plans to choose a default script language for all KDE programs like ARexx on the Amiga? I think a script language like ARexx is one part that is missed on KDE. I think there is a Rexx language (Regina) for Linux. But in my opinion Rexx needs programs with Rexx ports (Rexx communication interface). Maybe a Rexx interpreter for KDE can use the DCOP interface for communication with the applications."
    author: "Bernd"
  - subject: "Javascript?"
    date: 2001-09-27
    body: "Perhaps Javascript would be a good default scripting langauge.\n\n* It was designed as scripting language in the first place.\n* It's quite easy to use/learn.\n* It's supports Objects and OO in a simple/natural way. (Unlike Perl for example).\n* KDE already has a Javascript interpreter.\n* Many people know it already, thanks to the web.\n* It's C style syntax won't freak out the unix types too much.\n\nPerfect candidate IMHO.\n\nAlso developers need to add DCOP interfaces that power users can use for day-to-day scripting.\n\nIt would also be nice if you could launch scripts from a menu inside every KDE program (well, at least the major apps). Kind of like how most Amiga apps would let you run ARexx scripts without leaving the app.\n\n--\nSimon"
    author: "Simon"
  - subject: "Python?"
    date: 2001-09-27
    body: "I thought I heard something about making Python the standard scripting language for KDE a while back.  Doesn't some of KOffice already use it?"
    author: "David Walser"
  - subject: "Re: Javascript?"
    date: 2001-09-27
    body: "The best way IMHO is to make DCOP interface accessible from as many languages as possible and leave the actual choice on user."
    author: "Peter \"proc\" Rockai"
  - subject: "Re: Javascript?"
    date: 2001-09-27
    body: "No, no, ruby is far better...\n\nSee, there is a big problem what default should be. What I think would be pretty cool, are some standard bindigs which are connected to all major scripting languages.\nThis way everybody can chose whatever fits best. I'm not sure how this can be realised. Perhaps somebody could tell me if and how this is possible. \n\nPeter"
    author: "Peter"
  - subject: "Re: Javascript?"
    date: 2001-09-27
    body: "Look in kdebindings. \nJava and Python are already there, and\nPerl maybe even Ruby."
    author: "Morty"
  - subject: "Re: Javascript?"
    date: 2001-09-27
    body: "Problem with JS is that it's (iirc) closed source."
    author: "Carbon"
  - subject: "Re: Javascript?"
    date: 2001-09-27
    body: "Both Konqueror and Mozilla have open-source implementations of JavaScript...\n\nDavid"
    author: "David Joham"
  - subject: "Re: Javascript?"
    date: 2001-09-27
    body: "It's certainly true that embedding KJS (Konq's javascript engine) is easy - I wrote some code last week which embeds it, and it only took an hour. I've since added a bridge that lets the scripts access the properties of any QObject (like the Qt-DCOP bridge). It would even be easy enough to write a plugin that uses this stuff (I already made a KPart) and this lets it extend existing apps thanks to KParts.\n\nThere are plenty of other decent script engines and embedding perl or python is no harder than javascript, and people want to use these too. The big advantage of using DCOP for scripting, is that you can use any language you want to (or even mix several languages). In addition, the Qt-DCOP bridge lets you access the the interfaces of objects that the developer did not specifically expose for scripts.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Javascript?"
    date: 2001-09-27
    body: "You wouldn't even have to embed it per se. You could just put the JS interpretter in an external prog and have programs start it themselves and also pass the name of the script to run and maybe the name of the calling program (for convinenence). By keeping stuff out of process mem you could have as many different interpretters as you wanted, but without any extra library/startup overhead.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Javascript?"
    date: 2001-09-27
    body: "There are advantages and disadvantages to both approaches, the nice thing is we have the choice (and you can even use both in the same app).\n\nRich."
    author: "Richard Moore"
  - subject: "How about recording?"
    date: 2004-11-02
    body: "Did you notice if it would be easy to get all the dcop events to be sent to another application/object so that they could be recorded.\nThis would be great for scripting and producing 'tutorials' for applications instead of just the plain old docbook help."
    author: "Oliver Stieber"
  - subject: "Re: Javascript?"
    date: 2001-09-29
    body: "I think that would be a realy great thing for KDE. I had an Amiga in the past, and using AREXX from a math program that uses DPaint to draw its results was wonderfull.\n\nBut I agree very much that javascript is a perfect candidate. More over, I think that it would help find more bugs (because of more complex usages and different contexts)\n\nI've seen some other posts speaking about other languages. Of course, it could be cool (more programmers), but at last, it may confuse the user. Imagine that you have to use a javascript lib-package from a pyton plugin. (\"What a mess\").\n\nOr imagine just a pyton package for emacs that uses lisp libs... somewhat strange. More over, it can add bugs.\n\nIf you look at internet explorer: it can be scripted in javascript and in vbscript. So bugs are double, because you have javascript-engine bugs and vbscript-engine bugs. Behaviours are differents if you choose one or another language. (for example: regexp doesn't behave the same in ie6 :-( ). If you had an enhancement in one language, or if you change a behaviour in one language, you have to report it in the others (ex: regexp, security, ...). I think it could be difficult to synchronise such \"big details\".\n\nAnd finaly, concerning the implementation devel speed. If one language is better implemented than another, other teams working on others languages implementation will have less developpers and become marginal. At the end, this could be a big lost of time, and I think that dividing efforts is not good at this time.\n\nA last point I don't know is: can vbscript be easily converted into javascript?\n1) yes => one language is good, and you can import M$ Office docs with scripting\n2) No  => we are forced to implement multilanguage like KBasic or other vblike.\n\n(IMHO)"
    author: "Olivier LAHAYE"
  - subject: "Re: Javascript?"
    date: 2001-09-29
    body: "There is no point in using a \"javascript-lib-package\",\nsimply because there *aren't* libraries for JavaScript.\nJavaScript was made to script web pages, not more, not\nless. It is not designed as a general purpose language,\nand it doesn't have standard techniques for extensions.\n\nIn contrast, general purpose languages like Python and\nTcl are designed for extending applications from the\nbeginning. In particular, Python is extensible to the\nbone, there is no difference between an extension class\nand a Python class. Look at the scripting engine of \nKDevelop in the HEAD branch. It makes DCOP objects fully\ntransparently available. \n\nFrom the Python programmer's  point of view, a DCOP object \nlooks exactly like a Python object. He can also connect \narbitrary DCOP signals to a Python function. From the \napplication programmer's point of view, there is no Python\nat all. He just writes DCOP interfaces with methods and\nsignals. It is irrelevant from which language they are used.\nYou could also write a bridge for Tcl that does the same."
    author: "Bernd Gehrmann"
  - subject: "Re: Javascript?"
    date: 2001-09-30
    body: "javascript can use libs just like serverside javascript does, and you can include new javascript objects. You can create some objects and classes just like java.(even in html you can do that by including .js files defining classes using the <SCRIPT SRC=\"xxx.js\"></SCRIPT> syntax)\n\nConcerning dcop or otherthing access, it would be done the same way you access activeX objects or java applet. The extention  extends the local DOM with objects and functions.\n\nlet suppose we want to load a new document called \"foo.doc\" in kword\n\nthen a document.open(\"foo.doc\") would do the job.\nyou could also let the user choose the document with the following line:\n         document.menu.file.open.select();\nor you could rename that menu:\n         document.menu.open.rename(\"Open a File\");\n\nNow let say you want to include a picture:\n         oMyPic=document.createElement(\"IMG\")\n         oMyPic.src=\"foo.png\";\n         document.appendChild(oMyPic);\n\nNow, more complex thing: you want to include a speadsheet:\n         oMyEmbedSpreadSheet=document.createElement(\"EMBED\");\n         oMyEmbedSpreadSheet.src=\"foo.xls\";\n         document.appendChild(oMyEmbedSpreadSheet);\n\nNow you want to create an empty embeded kspread object:\n         oMyEmptySheet=new Kspread(\"Sheet1\",\"Sheet2\");\n         oMyEmbedEmptySheet=document.createElement(\"EMBED\");\n         oMyEmbedSpreadSheet.nodeValue=oMyEmptySheet;\n         document.appendChild(oMyEmbedEmptySheet);\n\nNow you want to set the background of the 1st cell of the first sheet to red:\n         oMyEmptySheet.sheet[0].cells[0][0].style.background=\"red\";\n\nAs you see, oMySpreadSheet would just behave as a standard html object with datas and methods.\nAnd by that way, exporting thoses pages to the net with scripting would be possible.\n\nJavascript also include security concepts.\n\nthat was just my 2 cents.\n\nPS: don't try to think in terms of netscrape 4.x javascript, its DOM is so poor that of cours almost nothing complex is possible and most of its usage is reserved to string computation and document.write()."
    author: "Olivier LAHAYE"
  - subject: "Re: Javascript?"
    date: 2001-09-30
    body: "javascript can use libs just like serverside javascript does, and you can include new javascript objects. You can create some objects and classes just like java.(even in html you can do that by including .js files defining classes using the <SCRIPT SRC=\"xxx.js\"></SCRIPT> syntax)\n\nConcerning dcop or otherthing access, it would be done the same way you access activeX objects or java applet. The extention  extends the local DOM with objects and functions.\n\nlet suppose we want to load a new document called \"foo.doc\" in kword\n\nthen a document.open(\"foo.doc\") would do the job.\nyou could also let the user choose the document with the following line:\n         document.menu.file.open.select();\nor you could rename that menu:\n         document.menu.open.rename(\"Open a File\");\n\nNow let say you want to include a picture:\n         oMyPic=document.createElement(\"IMG\")\n         oMyPic.src=\"foo.png\";\n         document.appendChild(oMyPic);\n\nNow, more complex thing: you want to include a speadsheet:\n         oMyEmbedSpreadSheet=document.createElement(\"EMBED\");\n         oMyEmbedSpreadSheet.src=\"foo.xls\";\n         document.appendChild(oMyEmbedSpreadSheet);\n\nNow you want to create an empty embeded kspread object:\n         oMyEmptySheet=new Kspread(\"Sheet1\",\"Sheet2\");\n         oMyEmbedEmptySheet=document.createElement(\"EMBED\");\n         oMyEmbedSpreadSheet.nodeValue=oMyEmptySheet;\n         document.appendChild(oMyEmbedEmptySheet);\n\nNow you want to set the background of the 1st cell of the first sheet to red:\n         oMyEmptySheet.sheet[0].cells[0][0].style.background=\"red\";\n\nAs you see, oMySpreadSheet would just behave as a standard html object with datas and methods.\n\nNote that you could also have acces to a DCOP object and its methods and variables in the same way. (although, the purpose of a scripting language is maily to automatize things that can often be do by hand. I think it would be a bad idea to write plugins with it (even if possible). Just look at msword or other scriptable programs. Scripts are only used to automatize the behaviour of the document. I've never seen a vbscript that extends M$ Word behaviour)\n\nBy that way, exporting thoses pages to the net INCLUDING scripting would be less difficult.\n\nJavascript also include security concepts.\n\nthat was just my 2 cents.\n\nPS: don't try to think in terms of netscrape 4.x javascript, its DOM is so poor that of cours almost nothing complex is possible and most of its usage is reserved to string computation and document.write()."
    author: "Olivier LAHAYE"
  - subject: "Re: Scrip language, Rexx port for KDE / DCOP?"
    date: 2001-10-01
    body: "I've also been missing ARexx. I never really gave DCOP much thought; however, having read these tutorials, I understand it's a lot more versatile than I thought."
    author: "Johnny Andersson"
  - subject: "Re: Scrip language, Rexx port for KDE / DCOP?"
    date: 2001-10-01
    body: "I've also been missing ARexx. I never really gave DCOP much thought; however, having read these tutorials, I understand it's a lot more versatile than I thought."
    author: "Johnny Andersson"
  - subject: "Re: Scrip language, Rexx port for KDE / DCOP?"
    date: 2003-09-12
    body: "You can use ANY language, if you use the XMLRPC daemon to talk to DCOP\n\n\nhttp://developer.kde.org/documentation/library/kdeqt/kde3arch/xmlrpc.html"
    author: "Alec Smith"
  - subject: "Isn't DCOP a security violation?"
    date: 2001-09-27
    body: "isn't it?\n\nCan't I create a link on an html page like:\n\nexec:/path/to/some/program\n\nAnd have konqui execute that program?\n\n\n(Just wondering)"
    author: "Greg"
  - subject: "Re: Isn't DCOP a security violation?"
    date: 2001-09-27
    body: "No, you can't. The exec protocol is only available to the about pages.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Isn't DCOP a security violation?"
    date: 2001-09-27
    body: "How does Konqui decide whether or not to allow a page to execute stuff?"
    author: "not me"
  - subject: "Re: Isn't DCOP a security violation?"
    date: 2001-09-28
    body: "The about page is a special KParts component (based on KHTML obviously).\n\nThere is no \"one Konqui\", there are many components in it.\n\nNo \"exec:/\" in an html page is going to do anything - AND, this has nothing to do with Javascript."
    author: "David Faure"
  - subject: "Re: Isn't DCOP a security violation?"
    date: 2001-09-28
    body: "> nothing to do with Javascript.\nnor with DCOP either ;)"
    author: "David Faure"
  - subject: "Re: Isn't DCOP a security violation?"
    date: 2001-09-28
    body: "does that have something to do with dcop ?"
    author: "ik"
  - subject: "Way off topic but ......"
    date: 2001-09-27
    body: "I know this is so far off the topic that I should be flamed, but I had nowhere else to go. I did searches on google/google groups, cheaked all relevent sites and on and on. I have found no answer and plead for your help.....\n\nI just installed KDE 2.2.1, I have SUSE 7.2, I set up my CDburner and I want to use KIOSLAVE audiocd:/ but it tells me that:\n\nthe file or folder\n/\ncan not be found\n\nI tried to CHMOD 666 cdrom and cdrecorder but to no avail\n\ndoes anyone know what is wrong??\n\nthank-you, I will never Off post again, thank-you\n\nJeremy"
    author: "Jeremy Petzold"
  - subject: "Re: Way off topic but ......"
    date: 2001-09-27
    body: "don't know but just intuiting...\nwhat about cdrom:, that is WITHOUT THE \"/\" ?"
    author: "emmanuel"
  - subject: "Re: Way off topic but ......"
    date: 2001-09-27
    body: "no, thats not it, I get it when I select the services button and click on audiocd. it has worked before on another system I have used, and it worked on my machine for 2.2, but in 2.2.1 it is not working. \n\ndoes audiocd scan for hdx only? if so could it be because I have the cdburner working and used the ide-scsi module? if so, how would I correct that?"
    author: "Jeremy Petzold"
  - subject: "Re: Way off topic but ......"
    date: 2001-09-27
    body: "It's so much the worse you recognize it being off topic but post nevertheless.\nThere are enough newsgroups and mailing-lists to discuss such problems!"
    author: "someone"
  - subject: "Re: Way off topic but ......"
    date: 2001-09-27
    body: "I have attempted that."
    author: "Jeremy Petzold"
  - subject: "Re: Way off topic but ......"
    date: 2001-09-28
    body: "See the archives for the debian-kde mailing list for the solution to your problem (lists.debian.org).  Search for \"audio cd\" or somesuch."
    author: "David Bishop"
  - subject: "Virtual inheritance"
    date: 2001-09-28
    body: "'Note that DCOPDemoIface is a virtual base class for the demo widget, there's no need to explain the details of the implications of this but it is important to make sure you don't forget'.\nI'd like to know the details. I don't see a single reason for doing so."
    author: "L.Lunak"
  - subject: "Re: Virtual inheritance"
    date: 2001-12-12
    body: "rtyrtyrty"
    author: "rtyrty"
  - subject: "Starting a program via DCOP?"
    date: 2001-09-29
    body: "In Olaf's tutorials, I find it rather disturbing to see programs started in DCOP scripts like this:\n\nkonqueror &\nsleep 3\ndcop konqueror whatever..\n\nThis is really really really yucky.  If Konqui happens to take more than those 3 seconds to start up, for whatever reason, then the whole script will fail.  This also means that scripts like this are totally non-portable between machines.\n\nIs there really no way of starting an application via DCOP and having this DCOP command block until the app is started, then return the pid or (better yet) the DCOP interface name of the app?\n\nIt would be better still if DCOP returned the existing interface name if the app is running and it is a KUniqueApplication, returned -1 if the app started but has no DCOP interface, and perhaps returns -2 if some other error occurred like the app crashed on startup.\n\nI'm not much of a hacker but this seems vital if DCOP is going to be anything but a toy.\n\nAlong the same lines, is there any standard way of quitting an app through DCOP?"
    author: "Amazed of London"
  - subject: "Re: Starting a program via DCOP?"
    date: 2001-09-30
    body: ": konqueror &\n: sleep 3\n: dcop konqueror whatever..\n\n: This is really really really yucky. If Konqui happens to take more than those 3 seconds to start up[...]\n\n    The quick answer is that it should be:\n\nkonqueror && dcop konqueror whatever..\n\n: It would be better still if DCOP returned the existing interface name[...]\n\n    I think you might be confusing dcop with DCOP... i.e., DCOP the interface, which is accessable through a variety of languages like C++ and Python, and dcop the application that allows you to access DCOP through the command line.\n\n    Although I love DCOP, I agree that dcop could use a bit of work (like being able to format output based on command line paramaters and such).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Starting a program via DCOP?"
    date: 2001-09-30
    body: "> The quick answer is that it should be:\n> konqueror && dcop konqueror whatever..\n\ndoh!  You are of course correct.  However, this approach (starting apps manually rather than through DCOP) has its limitations...\n\nImagine a scenario where you are using DCOP over XMLRPC to control an application on a remote computer.  Now, in order to start an app on that remote computer, you have to ssh in, set an appropriate DISPLAY environment variable, then start the app.  But if you have ssh access to the machine, then what's the point in using DCOP over XMLRPC?  Why not just use the dcop command that's on the remote computer?\n\nThis is the reason (I think) that nobody really used xmlrpcd in KDE 2.x and it is now not started by default in HEAD...\n\n\nAs for DCOP vs dcop, no, I'm fairly sure I'm not confusing them.\n\nWhen an app registers its first DCOP interface, dcopserver assigns the app a name to be used within DCOP space.  If the app is not a KUniqueApplication, then the name assigned is formatted like this:\n\n<command name>-<PID>\n\ne.g. konqueror-15357\n\nSo, in order for you to know how to even list the DCOP interfaces for an app that you have started from your script, you need to know exactly what the application is called (easy), whether the app is a KUniqueApplication or not, and if it isn't, then you need to know its PID (not really easy at all if you want it to be foolproof).\n\nHow do you then deal with apps that can be either a KUniqueApplication or not depending upon their settings, e.g. noatun, and may thus either be noatun or noatun-15967 in DCOP space?\n\nA way of starting apps via DCOP, that returns the name of the app in DCOP space would solve both these problems straight away.\n\nPlease do tell me if I've missed something blindingly obvious."
    author: "Amazed of London"
  - subject: "Re: Starting a program via DCOP?"
    date: 2001-09-30
    body: ": Please do tell me if I've missed something blindingly obvious.\n\n    Nope - what you talk about for extending dcop (the application) makes sense.  It's just that if you're using DCOP (the protocol) in a \"real\" language, then you don't need all of that (since exec() and family return pid in most languages).\n\n    Seems to me that I've seen a program or bash command that returns the pid of a given command as the errorlevel.  That would work, and would make me feel better than dcop spawning the program.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "OT: What about KDE Kernel Cousin?"
    date: 2001-10-01
    body: "Anybody knows what's happend to the Kernel Cousin KDE? In the beginning there were almost weekly summaries of the KDE developper's mailing lists, and now the latest issue is still 21th of August or something like that."
    author: "AC"
  - subject: "Re: OT: What about KDE Kernel Cousin?"
    date: 2001-10-01
    body: "Yeah, I miss it as well.  I was subscribed to some mailing lists over the summer but I don't have time to read them now and I'd really like a summary of what's happening every week.  Where are you, Aaron J. Seigo?"
    author: "not me"
  - subject: "Re: OT: What about KDE Kernel Cousin?"
    date: 2001-10-01
    body: "Oh, a quick trip to lists.kde.org shows the answer:  Aaron's busy too.  Well, I can certainly understand that.  Hope you get some free time soon, Aaron!"
    author: "not me"
---
<a href="http://developer.kde.org/documentation/library/dcop.html">DCOP</a>
is KDE's interprocess communication (IPC)/remote procedure call (RPC)
technology.  DCOP provides a simple protocol with authentication over TCP/IP
or Unix domain sockets.  Two new complementary tutorials on DCOP have just been made available
at the <a href="http://developer.kde.org/">KDE developer site</a>.  The
<a href="http://developer.kde.org/documentation/tutorials/dot/dcopiface/dcop-interface.html">first
one</a>, written by <a href="http://www.kde.org/people/rich.html">Richard
Moore</a> and entitled <em>Creating a DCOP Interface</em>, explains how to
add a DCOP interface to a simple KDE application, and illustrates how it can be
used.  According to Richard, &quot;<em>Adding the interface is easy.
The example defines a number of methods of different types, and as you'll
see all these methods can be quickly used via DCOP</em>.&quot; The
<a href="http://developer.kde.org/documentation/tutorials/automation/index.html">second
one</a>, written by <a href="mailto:olaf.zanger@gmx.net">Olaf
Zanger</a> and entitled <em>Automation of KDE2</em>,
introduces DCOP as an automation interface (<em>e.g.</em>, using
scripts to access another application's DCOP interface).  According to Olaf, the tutorial is useful to &quot;<em>learn
how to access features you used to tackle with your mouse or key-combinations
with your favourite scripting environment</em>.&quot;