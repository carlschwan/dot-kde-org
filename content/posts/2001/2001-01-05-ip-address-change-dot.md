---
title: "IP Address Change for the Dot"
date:    2001-01-05
authors:
  - "Dre"
slug:    ip-address-change-dot
comments:
  - subject: "Re: IP Address Change for the Dot"
    date: 2001-01-05
    body: "Is it just the ip-address that is changing, or is the site being moved to somewhere else?  If so, where and why?"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: IP Address Change for the Dot"
    date: 2001-01-05
    body: "Just the IP is changing. ISP internal matters, but it may also end up helping with our network \"speed\".  The site is still hosted by KDE.com/MieTerra on the same server.\n\nAs you may have also noticed, the change hasn't yet occured. It's still planned.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: IP Address Change for the Dot"
    date: 2001-01-05
    body: "You could just use squid as a transparent reverse-proxy pointing to the new IP while the DNS changes over and nobody would know the difference....."
    author: "Anonymous"
  - subject: "Re: IP Address Change for the Dot"
    date: 2001-01-05
    body: "If we had both IPs, yes.  Unfortunately it's an either/or situation.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: IP Address Change for the Dot"
    date: 2001-01-07
    body: "This is semi off topic, but I'd like to see less of the \"discussion\" news that has been on kde news lately. For instance, \"Do interoperability technologies help or hurt kde development\". This isn't news, it's a flame war magnet. KDE developers are already aware of topics such as these, and they dont need things like this to encourage discussion. Thats what the mailing lists are for. Instead, kde news should focus on just that : news. BTW, this isn't meant as a flame toward kde news. This should be interpreted as a suggestion."
    author: "Carbon"
  - subject: "Re: IP Address Change for the Dot"
    date: 2001-01-09
    body: "I tend to agree with this."
    author: "SteveH"
  - subject: "Re: IP Address Change for the Dot"
    date: 2001-01-10
    body: "So do I. News on the DOT should (sugestion only) be more of an introductory presentation to a technical discussion about how/why something should work/be adressed. <br><br>/kidcat"
    author: "kidcat"
  - subject: "Finding developers"
    date: 2001-01-09
    body: "To stay offtopic, I would like to post a \"search request\".\nI'm searching experienced or wannabe qt/kde or just Unix developers, to share thoughts and wisdom.\nMaybe some of them are lurking around here?\nI would really like to have some of them in my ICQ contactlist, for asking and answering questions.\nMy ICQ number is 51165217, feel free to contact me.\nI would be also glad if someone could point me to well known developers forums, mailinglists, newsgroups. I mean this kind of places, where beginners can ask questions and even have to chance to get them answered.\nI'm currently working on the NetRaider browser and some other projects and I have a lot of questions concerning Qt, KDE, autoconf, CVS... but I have no real contacts which I could ask or just talk about some problems. Any ideas, how I could change that?\n\nThanks in advance.\n\nSpark\n\nBTW: Email contact would be appreciated, too of course. :)"
    author: "Spark"
  - subject: "Re: Finding developers"
    date: 2001-01-09
    body: "(Oh Boy, ICQ reached 50 million users?)\nOne good place is kde-devel@lde.org and #kde."
    author: "Lenny"
  - subject: "Re: Finding developers"
    date: 2001-01-10
    body: "it already reached 100 million... ;)"
    author: "Spark"
  - subject: "Syware Blaster"
    date: 2007-04-10
    body: "www.SOFTe.org\n\nhow to check for ICQ number and find IP address?"
    author: "Spyware Blaster"
---
Effective 12:00 p.m. EST January 5, 2001 the IP address of KDE Dot News is scheduled to change from <a href="http://216.186.210.168/">216.186.210.168</a> to <a href="http://64.22.20.150/">64.22.20.150</a>.  Although we expect the transition to be a smooth one, you may experience difficulties accessing the site by the regular hostnames (<a href="http://dot.kde.org/">dot.kde.org</a>, <a href="http://news.kde.org/">news.kde.org</a>, <a href="http://www.kdenews.org/">www.kdenews.org</a>, etc...) around that time; using the new IP address in lieu of the domain name may help.  <b>Update: 01/05 7:31 PM</b> by <a href="mailto:pour at kde.org">D</a>:  The ISP experienced technical difficulties in making the change and hence the transfer has been postponed until mid next week.  We'll keep you posted.


<!--break-->
