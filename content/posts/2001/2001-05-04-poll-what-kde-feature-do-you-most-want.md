---
title: "Poll: What KDE Feature Do You Most Want?"
date:    2001-05-04
authors:
  - "numanee"
slug:    poll-what-kde-feature-do-you-most-want
comments:
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I definately would go for a linuxconf KDE frontend, simply because linuxconf already has a huge amount of modules. \n\nReally to bad I do not have internet access at home (nor will I have in the near future). Also, I have a diskless HPUX terminal at work, so bringing stuff from home is also a problem. Are there other ways to help ?"
    author: "Robbin Bonthond"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "That would be nice for those using linuxconf, but many don't. For example most Debian, Slackware and FreeBSD users.\n--\nCasper Gielen"
    author: "CAPSLOCK2000"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Exactly. Nonetheless, this would be a valuable add-on for users who *do* use it. It just should not be in the main release, but in a seperate module (perhaps a distribution subdir of kdelinux)."
    author: "Rob Kaper"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Please, not linuxconf, the most worsly designed most irritating, buggy, horrible program ever designed.  We should make a KDE specific platform generic configuration app of our own.\n\nAnd not me.  No.  I will not do this :)"
    author: "Charles Samuels"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "i would like a xml-based centralized config-file with extensions for user specific data like screen-resolution and such. this one would be much like a combination of suse-style rc.config and the windows registry (yeah i know, but not everything ms produces.. or buys ;) is bad).\n\na config deamon can watch changes in this file and execute distribution specific plugins, that do the actual systemconfig, if needed. to avoid speed problems, it would be possible to syncronize the config file with a serialized binary representation like a dom tree for example. \n\nthis aproach has some advantages:\n1. it will still be possible to edit configuration by hand as long as one edit the xml.\n2. technology for merging different config-files (like system overriding user-values) is present (xslt)\n3. syntax and to some extend semantic checking can be parametrisized (dtd, schema)\n4. frontends to edit the configuration would be very simple to write (load dom-tree, edit data, save dom-tree)\n5. differences between the distributions (e.g. sysconf-dir) will be hidden\n\nunfortunatly there a disadvantages as well\n1. redundant data holding (sysconf - xmlconf)\n2. concurrent configuration access (linuxconf - xmlconf)\n3. recreation of the wheel ;)\n4. distribution-developer support needed (for the plugins)\n\ni would like to see a tool/system like this, but i know that the arguments against this proposition are strong. i would like to see what standpoint other kde-users or developers have..."
    author: "cylab"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "About 1 1/2 years ago, I started on a configuration project that used xml to describe configurators that could then be code-genenerated. It used a 2 level scheme \n1) describes the data and allowed for flat files, cli, internal calls. Also describes the valid values (RE based). This would generate a structure that could be used elsewhere.\n\n2) describes the gui layout. This basically maps the componenets to the structures above. Also, would use structures from other sections. Such as, httpd.conf or hylafax.conf was using the Passwd class vector for validation.\n\nUnfortunatly, I ran out of time and need to change this to 3-tier set-up.\n\nI realize that this may not be the most efficient approach in terms of code, but this yields a consistent look and feel across all configurations. Also, as the project revs, it makes it easy to move to the next rev. Also, there may be different approachs to underlying apporach. Finally, I was hoping to generate both KDE and Gnome. One archetecture makes it easy and desirable to everyone, including weekend warriors and vendors"
    author: "g.r.r"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I completely agree with you\n\ni've been in a love-affaire with linux for about 5 years now, but since a year or two i mainly code java and xml server stuff with jboss, tomcat, cocoon, etc.. i never have that much fun to code with OO programming, design pattern, xml stuff, well you know, \"modern\" stuff and it made me realize how many obsolete unix stuff should disapear..\n\nit's not really kde related (i use gnome too) but what i would like to happen is:\n\nget rid of the /etc mess..\nhave a central registry (probably based on xml, it's not because MS registry sucks that the idea is wrong)\na better security mechanism based on acl\na easy way to give privilege to a user.. we should not have to log root to install software, etc.. ok sudo does it but it should be \"easy\"..\n\nMost config Apps/Tools should be based on a MVC approach, an xml model for config, a controller (the actual apps) and a simple protocol to talk to frontends (console, qt, gtk, web, whatever!)\n\ni've just read an article on Jxta and it's really interestings, it's like a unix shell but it's commands output XML (and you can pipe them, etc..)\n\netc.. etc.. \n\nbut the biggest problem i see is to get everyone agree on a way to do it! and there's always those elitists guy that wish that unix remain cryptic (like i read too often on slashdot) and that is the biggest obstacle to make a step forward and give linux and free software to the masses..\n\nwishful thinking...\n\ni know, MacOS X does a lot of this, but it is closed source, expensive, and not enough \"tweakable\" and configurable (which is the main feature that made me switch to linux from OS/2).. \n\n\neric"
    author: "eric sperano"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: ">not linuxconf, the most worsly designed most irritating, buggy, horrible program ever designed<\n\nRight. Please, forget linuxconf. It has never been useful here. Too buggy. Never sure that it will save changes you apply in it. Design? Grrrr... I have a good memory but never figured out where is what in linuxconf. Irritating? Oh, yes, never sure that it will accept keyboard inputs. Njaard is right. Kill linuxconf."
    author: "Antialias"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: ">not linuxconf, the most worsly designed most irritating, buggy, horrible program ever designed<\n\nRight. Please, forget linuxconf. It has never been useful here. Too buggy. Never sure that it will save changes you apply in it. Design? Grrrr... I have a good memory but never figured out where is what in linuxconf. Irritating? Oh, yes, never sure that it will accept keyboard inputs. Njaard is right. Kill linuxconf."
    author: "Antialias"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "linuxconf is a Redhat-ism.  Switch to SuSE; use YaST2.  It's KDE/Qt based."
    author: "Scott"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Linux config is not working any more in Redhat 7.1"
    author: "underground"
  - subject: "What *I* want?"
    date: 2001-05-04
    body: "Something that is out of the scope of KDE, actually.\n\nI would love the ability to detach a running X session so someone else can log in, or to reattach it from a different computer. Kind of like the features in Windows XP, although actually the idea was inspired by the console utility screen. Of course, this is largely an  X11 issue and not merely one of KDE, which does its best with reasonbly decent session management.\n\nAnd of course I've got a few other small wishes such as good support for fixed positioned layers (with hidden and automatic overflow) in khtml.\n\nOr a good panel based mp3/audio/cd player (kind of like xmms-kde does, only then native without requiring a xmms window to be open). I don't need a full interface for playing music, just a few controls, that's it.\n\nI'm still improving my Qt/KDE coding skills, but who knows, I might implement wishes rather than talking about them sooner or later. :-)"
    author: "Rob Kaper"
  - subject: "Re: What *I* want?"
    date: 2001-05-04
    body: "I realize that Noatun still had a\nbit of a long start up time, but its\nsystray element is pretty cool. \n\nAnd if not Noatun, then why not \nxmms-kde ?\n\nAlex"
    author: "Alex"
  - subject: "Re: What *I* want?"
    date: 2001-05-04
    body: "To answer that question from my standpoint, yes noatun's systray element is great but I have had more problems with noatun than I care to mention.  I am sure it will be a great MM app in time, but for now it is just not there and xmms definately is.\n\nI do use xmms-kde and it is a wonderful applet that is very customizable, but I agree that it would be very nice to be able to completely dock xmms so that it is not sitting on a destop and I can just click on the dock icon and see it wherever and whenever I need it.\n\nJust my $.02 worth."
    author: "kmax"
  - subject: "Re: What *I* want?"
    date: 2001-05-04
    body: "To answer that question from my standpoint, yes noatun's systray element is great but I have had more problems with noatun than I care to mention.  I am sure it will be a great MM app in time, but for now it is just not there and xmms definately is.\n\nI do use xmms-kde and it is a wonderful applet that is very customizable, but I agree that it would be very nice to be able to completely dock xmms so that it is not sitting on a destop and I can just click on the dock icon and see it wherever and whenever I need it.\n\nJust my $.02 worth."
    author: "kmax"
  - subject: "Re: What *I* want?"
    date: 2001-05-04
    body: "To answer that question from my standpoint, yes noatun's systray element is great but I have had more problems with noatun than I care to mention.  I am sure it will be a great MM app in time, but for now it is just not there and xmms definately is.\n\nI do use xmms-kde and it is a wonderful applet that is very customizable, but I agree that it would be very nice to be able to completely dock xmms so that it is not sitting on a destop and I can just click on the dock icon and see it wherever and whenever I need it.\n\nJust my $.02 worth."
    author: "kmax"
  - subject: "Re: What *I* want?"
    date: 2001-05-04
    body: "Can't this already be done?  I believe it is possible to run more than one X session on the same computer, and switch between them just like you can switch between X and virtual terminals.  As long as you remember to lock your display for security, then I think you can go to a virtual terminal and let someone else log in and start X.  I can't try it right nows, so I might be wrong (I'm running Windows because KPresenter doesn't have a PowerPoint export filter).  Of course, what would be *really* nice is if the same instance of X would manage both displays so you don't waste memory - that might even be possible too!\n\nFor an audio player control, try the Keyz plugin for Noatun.  It takes up a whole 0% of your screen space yet gives you total control over Noatun.  Ctrl-Alt-right skips to the next song, Ctrl-Alt-Left skips back, no matter what application you happen to be in.  There are other keys for other actions (seeking, volume control, show playlist, etc).  And if you don't like the default keys, you can change them!"
    author: "not me"
  - subject: "Re: What *I* want?"
    date: 2001-05-04
    body: "Yes, Keyz is nice, also nice is \"Mediacontrol\" in the kdenonbeta cvs module.  It is a applet for the panel.  Oh, and you'll be surprised how much faster noatun starts in KDE2.2 alpha1."
    author: "Charles Samuels"
  - subject: "starting multiple X"
    date: 2001-05-04
    body: "Yes it is possible, I did it.\nYou have to do a\nstartx -- :1.0\nand it will start a second X server with display :1.0 (by default, the first has display :0.0).\nBut it is a waste of memory and CPU time and it take a long time to switch from a running X to another (I have an Athlon 600 with 192MB of memory)."
    author: "renaud"
  - subject: "Re: starting multiple X"
    date: 2001-05-04
    body: "Xnest and you get the other display in a window in the same session"
    author: "ac"
  - subject: "Re: starting multiple X"
    date: 2001-05-07
    body: "That is an interesting feature Xnest.  I tried Xnest :1 which opens another X window.  But when I try to run xterm -display :1 it reports the following error.\n\nXlib: connection to \":1.0\" refused by server\nXlib: Client is not authorized to connect to Server\nWarning: This program is an suid-root program or is being run by the root user.\nThe full text of the error or warning message cannot be safely formatted\nin this environment. You may get a more descriptive message by running the\nprogram as a non-root user or by removing the suid bit on the executable.\nxterm Xt error: Can't open display: %s\n\nAny help would be appreciated"
    author: "Joel"
  - subject: "Re: starting multiple X"
    date: 2001-05-13
    body: "I had the same problem with Xnest but found the solution. You have to run Xnest with a command line flag (unfortuanely I don't remember it and am on holidays with only a Windoze PC so I can't check). Do a \"man Xnest\" and you should find a setting for allowing clients to connect to the server. Hope this helps. Quite a cool feature though Xnest...."
    author: "Nicholas Allen"
  - subject: "Re: starting multiple X"
    date: 2001-05-07
    body: "That is an interesting feature Xnest.  I tried Xnest :1 which opens another X window.  But when I try to run xterm -display :1 it reports the following error.\n\nXlib: connection to \":1.0\" refused by server\nXlib: Client is not authorized to connect to Server\nWarning: This program is an suid-root program or is being run by the root user.\nThe full text of the error or warning message cannot be safely formatted\nin this environment. You may get a more descriptive message by running the\nprogram as a non-root user or by removing the suid bit on the executable.\nxterm Xt error: Can't open display: %s\n\nAny help would be appreciated"
    author: "Joel"
  - subject: "Re: starting multiple X"
    date: 2003-04-24
    body: "Try using the following shell script (or some version thereof) to start your\nXnest:\n\n#!/bin/sh\n\nMCOOKIE=$(mcookie)\nxauth add $(hostname)/unix$1 . $MCOOKIE\nxauth add localhost/unix$1 . $MCOOKIE\nstartx /usr/bin/startkde -- /usr/X11R6/bin/Xnest :3\nxauth remove $(hostname)/unix$1 localhost/unix$1\n\nexit 0\n"
    author: "Joey Smith"
  - subject: "Re: starting multiple X"
    date: 2005-11-30
    body: "Xnest -ac :1"
    author: "www.insaner.com"
  - subject: "Re: starting multiple X"
    date: 2003-06-04
    body: "I'm on RH 8 and tried your script.  The xauth stuff fails.  However \nthe following will work and start Xnest:\n\n  start KDE in an Xnest window:\n    startx /usr/bin/startkde -- /usr/X11R6/bin/Xnest :2\n\n  start my default desktop (in my case, gnome) in Xnest:\n    startx -- Xnest :2\n\n  start twm for those really light X sessions:\n    startx /usr/X11R6/bin/twm -- Xnest :2\n\n\n"
    author: "Wade Hampton"
  - subject: "Re: What *I* want?"
    date: 2001-05-04
    body: "You can do this and more already with X/Linux (although I'm not quite sure what the XP feature actually looks like)\n\n- To start up another X session, try\n\n     startx -- :1\n\n  You can either start this from outside X, or after doing su from within a current X session. You can toggle between them with, typically CTRL+ALT+F7, CTRL+ALT+F8. Of course you can also start more than one. caveat: If you are logging in as the same user then KDE might have some problems as sockets are just named for the user + host rather than user + Xserver - anyway, that's what it looks like too me, I might be wrong!\n\n- You can \"su\" to another user and run individual applications with in the same X session - which is usually more useful.\n\n- You can log in to a remote machine and, with appropriate setting of the DISPLAY environment variable or the \"-display\", together with use of \"xhost\" to grant permissions run graphical applications locally.\n\n- You can use your local machine as a console for many remote servers by doing things like:\n\n  X :1 -query server1\n  X :2 -query server2&gt\n\nand so on. This means that it is quite possible to have a graphical display on a remote headless server, which is very nice for administration if you are more comfortable with a \"windows\" type interface...\n\nX has had this capability since the mid '80s - the rest of the world is only just starting to catch up!"
    author: "Ian Castle"
  - subject: "Re: What *I* want?"
    date: 2001-05-04
    body: "> detach a running X session so someone else can log in, or to reattach it from a different computer\n\nSearch for \"xmove\", it's a X11 pseudo-server to support mobile X11 clients and works fine as long the screen resolutions & depths are the same.\n\nOne drawback: It's very old and not developed anymore."
    author: "ShyGuy"
  - subject: "Re: What *I* want?"
    date: 2001-05-04
    body: "You might also try VNC, the Virtual Network Computing server (http://www.uk.research.att.com/vnc/). Allows even to acces your machine from a Windows client ;P."
    author: "Magnus Kessler"
  - subject: "Re: What *I* want?"
    date: 2001-05-04
    body: "Definitely try VNC.  Many labs use VNC in this fashion, and I believe the code is optimized for the special case of displaying a VNC session on the same machine.  Your desktop can then keep its state and be accessed from anywhere."
    author: "chchchain"
  - subject: "Re: What *I* want?"
    date: 2001-05-04
    body: "It seems like you understood my desire for [de|re]attaching sessions and not just wanting to run multiple X servers.\n\nPerhaps it's time to move Xmove capabilities into XFree86 again.. :-)"
    author: "Rob Kaper"
  - subject: "Uhh VNC??"
    date: 2001-05-04
    body: "hasn't anyone heard of this??\n\nClient and server for displaying X MacOS Windows and BeOS desktops to and from each other. The cool thing about using X this way is that it's stateless on the client. So the client machine can crash and you can login and find your apps just as they were. Plus you can share sessions, start multiple sessions etc. etc. Yet another idea that MS got from someone else (citrix - which basically was inspired by X). First MS said it was dumb, then they harassed and sued and finally bought the company, now they include the concept and their own implementation with their products heh heh\n\nAnyway try VNC it's free and it's been around for a very long time."
    author: "guy"
  - subject: "Re: What *I* want?"
    date: 2003-06-27
    body: "Something like that ... xmove .. xnest .. distributed .. rdesktop . . . brb."
    author: "terbo"
  - subject: "Re: What *I* want?"
    date: 2007-05-18
    body: "I completely agree with the original poster. I want XP-like session management (or actually, Terminal Server like).\n\nRight now, I think I am *king* of Xvnc, Xinetd, Xdmcp, KDM, XNest *and* xmove \n(oh and screen of course). But alas, for all the king's horses I don't seem to get both sides of the coin.\n\nHere's the chasm:\n\nUsing (x)inetd to ingeniously launch Xvnc with a KDM session (via KDM's xdmcp support!!!) I get the ability to VNC into my *nix box and create a new X session. However, since the Xvnc is spawned for an external connection\n(1) subsequent connections spawn more X sessions (which is GREAT because it gives me LTSP/Citrix like functionality)'\n(2) the Xvnc instance is killed when the connection is dropped\n\n--- so what I'd like to have is the ability to 'disconnect' and 'reattach' (cf. screen or MSTS/Citrix etc).... Hmmm that's easy:\n\nUsing plain Xvnc to start an X instance. It will run indefinitely and I can transparently attach/detach (multiple) VNC viewers. Great, but how about have my cake and eat it too?\n\nTo date my hack to get the behaviour I want, comes down to this:\n\n1. at boot I launch a headless X session (Xvnc on, say :73)\n2. next I launch xmove to listen on :1 and use :73 as default server\n\nI carry on as always\n\nexcept that once I start a local X client that I suppose I might want to 'migrate' to a remote session at a later point, I make sure to launch it on DISPLAY=:1. This will make it governed by xmove. Then is and I have a view of \n     xmovectrl :1 -moveall :mycurrentdisplay \nthe process. Before I go, I need to 'background' the xmove-able clients\n     xmovectrl :1 -moveall :73\n\nThat will keep them running until I 'beam-them-up' (xmove) from another X session later - perhaps from a remote location.\n\nSound clumsy? It is very much. Still it is how I go about it sometimes because it is just what I insist on getting. Oh, and to make it a little more manageable, I sometimes throw in an extra Xnest/twm into the mix, just so I don't have to keep thinking about which clients are running under xmove (don't you just hate it when you find out that you ought to have started that lengthy compile job in a screen session LOL)\n\nIf anybody has a cleaner way to do this, please share your thoughts because, this is far from ideal, and I hate to say that a standard XP installation has smarter session management.... uhoh\n\nCheers. Thanks for hearing me out. I'm glad i'm not the only *ux user that is struggling to get X11 to behave like screen :D:D:D:D\n\nSeth"
    author: "Seth"
  - subject: "Re: What *I* want?"
    date: 2009-01-15
    body: "Have you tried screen?\n\nIt is a very old terminal switching program that was used pre X11 and Windows.\nIt continues to live today. I find that a combination of screen and SSH works very well for maintaining windows to many machines.\n\n"
    author: "tim"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Please, some more *nix-like behaviour.\n\nThis is not a troll, too ;) One thing that I've found really irritating is the seemingly pointless removal of some standard *nix-like behaviour. For instance, triple-clicking text.\n\nIn most *nix apps, triple-clicking on a line of text will select the entire line. Hold down on the third click, and you can select more text, on a line-by-line basis.\n\nThis behaviour has disappeared! Now, I would understand if, for instance, line-selection was a double-click. Then, people coming from other OSes might have difficulty. But they're not likely to go about triple-clicking things, are they? So why remove it?\n\nI don't know. I find it irritating. There are other examples of this(like the rather limited selection of default keymap translations in Konsole[F1, CTRL+F1, and ALT+F1 all send the same control sequence), but I think this is more of an attitude than anything else.\n\nNow, as far as a real implementation feature? :) More default \"views\" for Konqueror; I'd love a \"helpbrowsing\" view, for instance, so that I can view all help files launched by various apps with a similar layout.\n\nPerhaps you guys could with with John Harper(author of Sawfish) a bit to make Sawfish <-> KDE2 interaction a bit better. There are still a number of bugs; and while I understand that KDE2 has Kwin, it's not as featureful as Sawfish, nor, in any sense of the word, as flexible.\n\nThank you very much for hearing by point of view, and for all the great work you guys have done already :)"
    author: "David B. Harris"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: ">More default \"views\" for Konqueror; I'd love a \"helpbrowsing\" view, for instance, so that I can view all help files launched by various apps with a similar layout.\n\nIn CVS there is a seperate help-browsing application now.  (well, it might be Konqueror morphed with config files, but it looks different at least!)  I've always found it irritating that Konqueror looked exactly the same as a help-reader and a web browser - it was confusing.  I am happy about this change."
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I think this has a lot to do with the fact that (in my opinion) KDE tries to be user-friendly to the majority of people. I think that the majority of users right now are x-Windows9x users. So KDE tries to be as much like them as  possible, and add some of its own unique features to the mix."
    author: "Derek Petersen"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Well ... :) My entire point was that the triple-clicking thing was something that a \"Windows9x\" user would never know existed. So why remove it?\n\nI'm not bothered so much by some other things; Alt+F4 to close a window, so on and so forth(as defaults), because that's what a Windows user would *expect*. Seeing a difference, it might confuse them.\n\nHowever, what would they expect of triple-clicking? Nothing. They'd probably never do it. So why remove it?"
    author: "David B. Harris"
  - subject: "Re: Poll: What KDE Feature Do You Want Most?"
    date: 2001-05-05
    body: "Actually, most Windows users would expect triple click to select the current paragraph, which is not all that different.  Since both types of selections have their utility depending on what types of files you primarily edit (for config files, line selection is usually better, for word processing documents, paragraph selection is more appropriate)."
    author: "Chad Kitching"
  - subject: "Re: Poll: What KDE Feature Do You Want Most?"
    date: 2001-05-05
    body: "Thoughtful response :) Thank you.\n\nThat raises a good point. Is this sort of thing configurable at source-level for KDE2? My guess would be yes; it'd make a lot of sense for word processing programs to select paragraphs as opposed to lines; and apps line konsole(and a host of others) to select by the line.\n\nI'd still argue that line-by-line selection is \"better,\" in terms of ease-of-use(as opposed to ease-of-learning), but I wouldn't do so to anyone but myself ;)"
    author: "David B. Harris"
  - subject: "Re: Poll: What KDE Feature Do You Want Most?"
    date: 2001-05-05
    body: "For most purposes, I think they'd be roughly equiv.  Triple-clicking on a line in MS Windows selects all the text between two newlines (CRLF pairs in text files, <P> in IE HTML renderer, etc), with no regard to the physical soft-breaks of the lines.  Of course, this wouldn't make a whole lot of sense in something like Konsole, where you may not always know where the logical lines start and end, and where the software wrapping occurred.  The real difference is what happens when you start to move the mouse after triple clicking and holding down the last click.  On windows, it selects the paragraph (or line depending on how the text is implemented), and then modifies the selection like a normal single click select (which is somewhat inconsistent with how Windows handles double-click selections that are being extended).  To me, this is incorrect.  It should let you select other paragraphs/lines afterwards (e.g. the selection mode should be on a line/paragraph basis, just as double clicking on something and then continuing the selection should select on a per-word basis).\n\nIn all honesty, I'm not completely familiar with how most X programs do it, since I don't think I've ever used that particular feature.  However, the primary concern with ease-of-use shouldn't be how easily a Windows user will catch onto it, but rather how easily users in general will.  Triple clicking to select is a pretty obscure feature, even in Windows.  I don't see much danger of confusing users, only frustrating them a little the first time they realize it works a little differently."
    author: "Chad Kitching"
  - subject: "Re: Poll: What KDE Feature Do You Want Most?"
    date: 2001-05-21
    body: "Not to mention, triple-clicking is way too painful and likely to cause RSI faster!"
    author: "KDE User"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I never said that they should remove it. I just think that making KDE very user-friendly to Win9x users was top priority. I think that they were simply got so caught up in making KDE mimic Windows that they failed to consider traditional Unix behaviors."
    author: "Derek Petersen"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "i GUARENTEE you kwin is as flexible as sawfish! KWin clients are written in c++.... and can do ANYTHING.\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Yes indeed, I must agree.. I don't see how sawfish is such a big deal.. Can you give us some examples of userfull features sawfish has to offer and kwin is lacking?"
    author: "xastor"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Well, kwin is a lot less configurable. With sawfish (or windowmaker which I used for quite some time) it is possible to set a lot of attributes per window. For example, I can't tell kwin to pop up my licq windows on a specific place on the screen, without them being bound to the desktop where I selected 'store settings'. Another thing is that for as far as I know it is currently not possible to have a borderless window, at least, not one which can still be managed using alt-mousebuttons. There are actually quite some other issues that I find lacking in kwin. Now don't get me wrong, I think it is ok to have a easy-to-use-but-not-very-featureful windowmanager as a default for KDE. However, I _do_ think it would be nice if it were possible to use other windowmanagers. I tried using sawfish, but noticed that I just can't get things like the system tray working.\n\nWell, I should probably look at the code to see why sawfish()<->KDE won't get along, but I think it is a bit out of my league.\n\nThanks,\n\nJelmer"
    author: "Jelmer Feenstra"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Well, kwin is a lot less configurable. With sawfish (or windowmaker which I used for quite some time) it is possible to set a lot of attributes per window. For example, I can't tell kwin to pop up my licq windows on a specific place on the screen, without them being bound to the desktop where I selected 'store settings'. Another thing is that for as far as I know it is currently not possible to have a borderless window, at least, not one which can still be managed using alt-mousebuttons. There are actually quite some other issues that I find lacking in kwin. Now don't get me wrong, I think it is ok to have a easy-to-use-but-not-very-featureful windowmanager as a default for KDE. However, I _do_ think it would be nice if it were possible to use other windowmanagers. I tried using sawfish, but noticed that I just can't get things like the system tray working.\n\nWell, I should probably look at the code to see why sawfish()<->KDE won't get along, but I think it is a bit out of my league.\n\nThanks,\n\nJelmer"
    author: "Jelmer Feenstra"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Well ... ANYTHING is *incredibly* flexible if you want to go to the source. That wasn't my point, and you know it.\n\nSawfish has more user-visisble flexibility than KWin; it doesn't need a recompile if you want to modify some behaviour slightly :)\n\nAnd for the other posts, those who asked \"well, what can it do that KWin can't?\", you should go try Sawfish(and its config utility) before you write any more in this thread. If you need to ask, you obviously havn't used it."
    author: "David B. Harris"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "Hrmm\n\nI have used Sawfish. It has lots of configurable options..\n\nNo part of KWin has to be REcompiled to completely modify its look and feel. kdebase/kwin/clients/* houses kwin clients, which control the win manager and if u add one u can use it right away.. very powerful, feel free to write one incorporating features u want, or modify the main kwin sources. I will soon..\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Hello,\n\nAt first thank you for the very good job of KDE 2.\n\nNo, I don't think that configuration tools are a priority, even if it would be nice to have an icon to change the screen resolution or to stop-restart-jump music.\n\nI feel that the first priority is to have MS import-export tools in Koffice, and -more - to work with MS formated files as easily than in the MS programs. Included the feeling. For instance it is a pity to see in KWord that ctrl + left key don't jump a word (it is done in Kedit or Kmail)...\n\nThen a better stability and optimization, a well finished work, including an updated and translated documentation. My bigger trouble in KDE2.1-Mdk8.0 is to find that my child panel is empty after a restart of KDE... I am irritate in Quanta Plus when I find that the tool bars are not on the left as I wished on my previous session... and the copy-paste is troubled with the use of the third mouse button... And many others details to improve... I hope to find the same look and feel in all the KDE programs, and, so, it needs more parameter in the \"look and feel page\" of the control panel. Also a \"Save configuration\" command in all the KDE programs, even (chiefly ;-)...) in the KReversi...\n\nAlso :\n\n- For konqueror\n -- a better integration with samba, for instance an easier configuration of the local network (I did not reach to  install it (in the left frame)),\n -- and also for the ftp icon (very powerful, but it is not easy to put an ftp site on it), and also for the http icon (however it seems less interesting, but I don't know how to use it)\n -- use the back key to go to the previous page\n -- add a \"copy with options\" for changing or not the owner and group of copied files\n -- in mp3 directories display the mp3 tags (but better than in Nautilus, please, with all the tags)\n -- make the \"+\", \"-\" cases available (in the hierarchy frame) on the mounted disks\n -- on a Html page, let the \"Open with...\" opening Kedit on the true source in update mode, not on the tmp source in read-only mode\n -- have more profile (one for documentation, as someone says)\n -- make easier to have java and flash support. I tried but not succeeded and I don't think it is well documented...\n -- have more buttons in the tool bars (See hidden files, see Index files...)\n -- have more efficient downloadings\n \n- I wish a fast image browser (like ACD See). Perhaps ShowImg is the solution, but it is not in Mdk 8.0 and I didn't reach to install it...\n\n- improve Quanta Plus, very good program ! At first fix the bugs, make documentation and translations...\n\n- improve noatun, at first for mp3 management, so that it will be as good as Xmms, even better\n\n- improve Kmail, Korganiser and the adress book so that they work together\n\n- add sheets to Kedit so that it becomes multi-documents (see Notetab, in Windows, the best graphic text editor...)\n\n- I also think that \"a professional sound recording and editing software is missing\"\n\n- a \"Quicken like\" program is also a good idea\n\n- create a KAtaxx game, better than GAtaxx, which is a poor ataxx (see ideas on my site http://pressibus.org/ataxx) (it is possible to use the very good GPL game Extreme Ataxx, see my site, in the win section)\n\nI hope again to be astonished...\n\n-- \nAlain"
    author: "Alain"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "\"improve Kmail, Korganiser and the adress book so that they work together\"\n\nThis is being worked on.\n\n\"add sheets to Kedit so that it becomes multi-documents\"\n\nThere will be a new editor in KDE 2.2 called Kate.  It is very nice.  It has a sidebar that lets you switch between many open text files and a nice fileselector.\n\n\"a \"Quicken like\" program is also a good idea\"\n\nI believe that Kapital by theKompany (www.thekompany.com) is exactly what you are looking for.  Buy it and support theKompany!  They have been contributing a lot to KDE development."
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Ok, I'm off topic for the discussion, but I feel compelled to agree with you about sawfish.\n\nIt really is the *most* configurable wm around.  The power that lisp provides without any necessity for recompiles is virtually limitless.  \n\nAs David wrote in another post, for anyone who thinks kwin comes close in configurability they should check out sawfish and it's config utility before posting anymore....I guarantee you there's a good chance you won't look back :-)  \n\nTo get (somewhat) back on topic, due to the NETWM spec compiled by both KDE and GNOME camps together, sawfish should be as integrated as kwin and vice versa wrt their respective desktop environments."
    author: "Rob"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "Well ... just to be clear:\n\nI don't ask that KWin emulate Sawfish. Sawfish isn't really fast. It's rather complex. It might eventually suffer from serious bloat problems, due to its easily-extensible nature.\n\nI don't suggest that KWin in any way be modified. It simply isn't aimed at the segment of the market which I occupy. Unfortunaly, most ardent *nix/Open Source supporters are indeed in the same segment as myself.\n\nSo, the solution? Use the right tool for the right job. Sawfish caters to my segment, and I'm happy with it. Just a bit more collaboration to make the two(Sawfish and KDE2[Konqueror and kicker specifically]) co-operate better would be ideal :)\n\nThanks again :)"
    author: "David B. Harris"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "I use Windows at school, and triple clicking highlights a whole line throughout the system.. so people from other OSes (like me) *would* go around triple-clicking things, as I have been :P  Double-clicking should highlight a word, triple-clicking should highlight a line.\n\nAlso, if there isn't one.. a KDE update program would be nice.  I know I sound lazy, but if you want to make it easy-to-use, this is a must.  Maybe you could have it check for updates when the person boots up (if on a constant connection) or when the person connects to the internet, then make a window pop up asking if they'd like to update it.  All they should have to do is click \"yes\" and then, when it's done, click \"ok\" to verify that they know it's done.  Anything else would complicate things too much.\n\n-John"
    author: "John Schwinghammer"
  - subject: "Wow"
    date: 2001-05-04
    body: "At the time of this writing, \"Configuration Utilities\" have the highest number of votes.  Are distributions really doing that bad of a job, and now KDE is to come to the rescue?\n\nI also notice 0% for Docs and Translations.  There are quite a bit of translations at this point, but let's all not forget docs.  Although this is a poll of priority.  All items on the list are important.\n\nI voted for Konqueror.  IMO, the most important items on the list are: KOffice, Konqueror, Speed, and Stability.  However, Konqueror is KDE's key application (KOffice is too, but Konqi is more-so).  Stability is quite good for KDE right now.  Also, Konqueror in particular is probably the only app that truly needs a speed boost, since it is fired-up so often.  So this kills two birds in one stone (\"Speed\" and \"Konqueror\").\n\n4-5 seconds to start up is acceptable for a web browser, but not for a file manager (especially when competing with the unix shell prompt!).  Maybe the file manager components could be preloaded by option?  I'm not using CVS, does anyone know if this has been attempted/considered yet?\n\n-Justin"
    author: "Justin"
  - subject: "Re: Wow"
    date: 2001-05-04
    body: ">Are distributions really doing that bad a job\n\nOh yes.\nEvery distro I have ever seen has managed to mangle KDE somehow, and graphical config tools are generally just plain sucky.\n\nPerhaps its time for KOLD (KDE Oriented Linux Distribution)"
    author: "Carbon"
  - subject: "Re: Wow"
    date: 2001-05-04
    body: "Which KDE distribution are good, which are not so good ?\n\nGood: Caldera, Mandrake,... ???\nNot so good: RedHat,... ???\n\nI hear the next Caldera version will NOT support the AMD K6-II processor, it makes me a bit sad. I've have got the impression its the best KDE distro around, and I want to try it, but I'm not gonna buy a new PC to run it :-("
    author: "Stig"
  - subject: "Re: Wow"
    date: 2001-05-05
    body: "Caldera: Hmm, i had forgeten about this one. Tried 2.3, was very well done KDE implementation, but everything else was bs.\n\nMandrake: Frankly, just not all that great. The speed introduced is not worth the various bugs and convolutions introduced. Universal menus? Ick\nAdmittedly, I'm using Mandrake right now, but in order for it to be usable at all, I recompiled QT and KDE, and removed the autologin and redid several X config files.\n\nI've heard from other KDE'rs that Debian is a great OS overall, since it is much more oriented towards the whole idea of Open Source in general, rather then trying to sell it as a product. Also, the new packages are supposed to be great, even if they are marked \"unstable\". I keep meaning to try that OS..."
    author: "Carbon"
  - subject: "Re: Wow"
    date: 2001-05-05
    body: "The only thing is that you shouldn't expect to find a graphical configuration utility in Debian -- there isn't for the most part (yes, linuxconf is in the dpkg databases and can be installed, but never by default).  Probably one of the neatest features of Debian is the Debian menu system -- a unified menu of all X applications that is automatically updated for virtually any desktop or window manager you choose, including such favories as twm, fvwm, etc.  You don't need to worry about some of the silliness behind the disagreement with panel menu file format differences, so installing a gnome program (say, like XMMS or The Gimp) into an otherwise KDE-only system works and is accessable from your standard menu.\n\nOn the console side, there's the \"Alternatives\" system that allows you to install a few different programs that serve basically the same purpose (like nvi, vim, elvis, etc), and decide which one will handle the default startup command (like \"vi\" for those three vi clones I mentioned).\n\nAnd do I even have to mention \"apt\"?  My only complaint is that KPackage is a little too RPM centred, in that packages for RPM only have two real states -- installed or not installed.  Debian packages have quite a number of different states, including installed, held, removed, purged and unconfigured.  Unforuntately, that often ends up forcing you to ultimately use dselect, which despite being rather quirky, does it's job extremely well."
    author: "Chad Kitching"
  - subject: "Re: Wow"
    date: 2001-05-05
    body: "About those menus : Mdk tried the same thing, and it just plain died. Debian does this correctly, then?"
    author: "Carbon"
  - subject: "Re: Wow"
    date: 2001-05-06
    body: "Oh yes, the Debian menu is a seperate submenu.  It leaves your regular K menu alone.  Also, it doesn't have ugly icons :-)"
    author: "not me"
  - subject: "Re: Wow"
    date: 2001-05-04
    body: "idea:\n\nI seem to remember that CVS has the \nability to only fully load konqi for its\nfirst instance and for all new windows to\nsimply become new windows for this instance.\n\nTherefore, together with --geometry 0x0-0x0\nand --desktop 6 (does this option exist yet?\niirc cvs has it) why not just create a simple\nscript that run's konqi with those options\non startup. Something like:\nstartkonqi:\n  while true do\n     konqueror --geometry --desktop 6\n  done\n\nOr even better, any way of making a window\ndisappear from the taskbar? (cmdline option\nrather than just a devel thing)\n\nWow, i've forgotten bash syntax...\n\nAlex"
    author: "Alex"
  - subject: "Re: Wow"
    date: 2001-05-04
    body: "Look at SuSE 7.1, they have their configuration utilities (YaST2) not only as stand-alone program, but also embedded into the KDE control center. Only problem is that it takes too long to start up YaST2's modules."
    author: "AC"
  - subject: "Re: Wow"
    date: 2001-05-04
    body: "This is the way all distributions should handle it. I've attached a screenshot of what it looks like.\n\n/Trynis"
    author: "Trynis"
  - subject: "Re: Wow"
    date: 2001-05-06
    body: "Will it work behind a proxy?"
    author: "AC"
  - subject: "Re: Wow"
    date: 2001-05-07
    body: "Looks nice...\n\nhowever, what I'm interested in is the corruption of the highlight rectangle around the text 'Online Updates' (the fact that the bottom isn't straight)\n\nI have this same problem occuring all over the place on my system. Originally I thought it had something to do with the fonts I was using... however many combination later I'm not so sure."
    author: "Peter"
  - subject: "Re: Wow"
    date: 2001-05-07
    body: "It seems to be bug in X. Check out this comment\nhttp://dot.kde.org/988313536/988326287/988351061/988352473/988363914/988375459/988399222/988690415/"
    author: "nap"
  - subject: "Re: Wow"
    date: 2001-05-08
    body: "I wonder if there are any patches to X 4.0.3 that fix this... I'm not too keen on getting X from CVS."
    author: "Carlos Rodrigues"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "My wife and I use one computer.  Wouldn't it be great if I could put my work to sleep or run it in the background, switch the computer over to her account, and five minutes later when she's done doing whatever it is she does, switch back to my account.  I hear Windows XP will have this feature.  \n\nIt's not the most important feature in the world but it sure would be cool/convienent."
    author: "Andrew"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "you can already do it.. \n\nswitch to a console (Ctrl-Alt-F1), login as root, type \"startx :1\" (i don't remember the exact command.. but its similar) and you'll get another X running. Now you can switch between the two Xs at Ctrl-Alt-F7 and Ctrl-Alt-F8"
    author: "sarang"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "You could even start the second X Server from an xterm/konsole in the first X session. This way you'll be brought back to you desktop automagically if the second X sessions ends. My grildfriend and I are doing this as it's a very nice solution if you just have to wirte an eMail fast or something similar...\nThe other way suggested would have the advantage that you can lock your first X session before starting the second so this solution should be reasoably secure. (At least for not-too-security-critical use of the machine. I don't think secrutity matters that much if you and your wife are the only users of the machine.)\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "while I can see that this provides a working solution, why not take a more minimalistic approach. On my machine my girlfriend does:\nxhost +\nrlogin busoni -l linda\n<password>\nkmail &\n\nI bet this is faster than starting a whole new X session.\nAs you can see I'm not worried about security vulnarabilities, but I don't think most people do in a home environment."
    author: "Mark Smeets"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "This sounds pretty neat.\n\nSo (If I had enough ram) I could start 2 kdm sessions on differnt virtual terminals?\n\n\nInstead of logging out I could just lock the screen and let somebody else log in on the second one?\n\nI wonder how hard this would be to automate..."
    author: "Pete"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "sarang is correct although the command is just a little off.  To open an additional X session go to an unused console and type:\n\nstartx -- :1\n\nThis will start an additional X session accessable if one hits ctl+alt+F8 several more sessions can be started if one replaces the 1 with 2 or 3 and so on.  A window manager can also be specified in the command such as:\n\nstartx /usr/bin/kde2 -- :1 \n\nAny window manager that is installed can be specified here.\n------------------------------------------\n\nAs far as improvements to KDE I agree that a file browsing window should net take > 5 seconds to open.  Thats fine for a web browser be not a file or help browser.  One begins to wonder weather the window is coming up if if one should click again.  \n\nYou guys have done an incredible job though with your desktop enviroment.  I'm a huge fan of KDE and hope to be developing KDE apps soon.  Keep up the amazing work!\n\nNathan Jett"
    author: "Nathan Jett"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "you can use k secure shell(k->internet)! Simply type in the username and press connect. \n\nStart the programm you want out of the new terminal. The apps from different persons will run on top of your desktop.\n\nThere is nothing faster ;-)\n\nThis works also remote:\ninstead of localhost, type in the url to the remote computer. *quite* impressive!\n\nBy the way - Windows XP switching needs *lots* of RAM - thus decreasing overall performance and will not be much faster than log out - log in.\n(The well known c't Computer Magazine allready sent kudos to kde for its switching technology...:-)"
    author: "Christoph Held"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "you can use k secure shell(k->internet)! Simply type in the username and press connect. \n\nStart the programm you want out of the new terminal. The apps from different persons will run on top of your desktop.\n\nThere is nothing faster ;-)\n\nThis works also remote:\ninstead of localhost, type in the url to the remote computer. *quite* impressive!\n\nBy the way - Windows XP switching needs *lots* of RAM - thus decreasing overall performance and will not be much faster than log out - log in.\n(The well known c't Computer Magazine allready sent kudos to kde for its switching technology...:-)"
    author: "Christoph Held"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "you can use k secure shell(k->internet)! Simply type in the username and press connect. \n\nStart the programm you want out of the new terminal. The apps from different persons will run on top of your desktop.\n\nThere is nothing faster ;-)\n\nThis works also remote:\ninstead of localhost, type in the url to the remote computer. *quite* impressive!\n\nBy the way - Windows XP swithing needs *lots* of RAM - thus decreasing overall performance and will not be much faster than log out - log in.\n(The well known c't Computer Magazine allready sent kudos to kde for its switching technology...:-)"
    author: "Christoph Held"
  - subject: "kdesu"
    date: 2001-05-04
    body: "Currently when my dad needs to surf the net, he simply clicks the Link to Application I've created for him, running Konqueror with his user, getting his own bookmarks and even Russian localization (while I use the English one).\n\nSince I assume you and your wife trust eachother not to sabotage each other's work :), you can simply create two named desktops, one for you and one for your wife, and leave your applications on your own desktop."
    author: "Toastie"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "What you want already exists: it's called \"vnc\"\nYour wife's complete desktop would then\nbe just one mouseclick away."
    author: "Malcolm Agnew"
  - subject: "Konqueror recovery"
    date: 2001-05-04
    body: "Any chance of storing a reference to all\nfiles currently loaded in konqueror and\nallowing them to be easily reloaded?\n\nWierd interface idea, how about extending\nthe current history support with a flag\nshowing the window is open.\n\nThen add the \"yesterday\", \"last week\" feature\nthat tacket (iirc?) put in one of the 2.1\nidea screenshots but with an added \"crashed session\" category?\n\nthanks,\nAlex"
    author: "Alex"
  - subject: "Re: Konqueror recovery"
    date: 2001-05-04
    body: "yes!\n\nNow I simply stay logged in, because I have thousands of konquerors and konsoles and I don't want to loose my environment by logging out."
    author: "eva"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Configuration Utilities are the last thing that\nshould be on the list. Each distribution is \nresponsible for developing those aspects.\n<br><br>\nTo ask KDE to do so would be a monumental waste\nof resources. What linux needs is a fully\nfunctional web browser that supports todays\nstandards and extensions AND is fast. Second\nwe need office tools and fonts.\n<br><br>\nThe largest roadblock I have hit when trying\nto get family and friends to try linux\nare the lack of a good browser, office suite\nand e-mail. They like te existing products that\nthey use. Try to get a novice user like your\ngrandmom to switch from using IE5, \nWord and Outlook to Netscape, emacs and mutt.\n<br><br>\nConfiguration is not the answer. Most people\nbuy a computer and plug it in. That's it. To\nthem it is like another electric appliance.\nLet the distributors and OEM's worry about\nconfiguration. KDE bring us more productivity\napps my mom can use \n<br><br>\nI know blah blah ...If they can't use a computer\nthey should'nt have the privelige of using\nlinux. The same argument could be used as such\n..If a user cant install and configure a box from\nthe command line...should they be using it?\n<br><br>\nLinux needs growth on the desktop market and I applaud KDE's efforts for making linux more\nuseful to people."
    author: "DanM"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Netscape, emacs and mutt?\n\nNever heard of:\nkonqueror(www.konqueror.org), \nkmail(devel-home.kde.org/~kmail) and \nkword(www.koffice.org/kword)?"
    author: "geekster"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I agree with the original post, I think KDE is mature enough to be used as a desktop replacement for windows. The problem are the apps. I personally use Konqueror for browsing, but quite often I have to start Opera or Netscape because Konqueror could not handle the page. I think the KDE team has done a great job, and it would be even better if they would concentrate in polishing the existing features instead of adding new ones. And remember, we can say we have one of the best desktops out there, but without professional apps like compilers, cross compilers, embedded tools, CAD, etc, the choice for serious work not related to networking will remain WinNT. I hope the tandem KDE/Qt will change that very soon.\n\nJose"
    author: "Jose C. alvarez"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Well, perhaps you're right about konqueror, though it's along time since I've had any progblems with it but perhaps I've just been lucky. \nBut I still can't see why you'd have to use mutt for emails, kmail does the job just fine I think... and it's easy to use. \nAnd true, I don't use word processores all that much so perhaps I should'nt comment on that issue. But mutt? what is wrong with kmail?"
    author: "geekster"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I absolutely agree. I would add that the ability to import and export without flaw Microsoft Word and Excel documents is also essential. This would enable people to swap documents between their work environment (which is currently likely to be Microsoft) and their home Linux systems.\n\nKDE's desktop is already up to scratch, but with fully functional and Microsoft compatible office suites, browsers, etc., people who are not interested in the niceties of operating systems and software, but who simply want something that they can use, are much more likely to be enticed to try Linux. I might even be able to convert the other members of my family!"
    author: "David Jarvie"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "I sure agree with DanM that the one of the most important features needed right now is a web browser that can be a replacement to IE5. An office application is also of very great importance because that is I guess one of the most used applications. I have been working in a Unix environment the last 1.5 year and therefore\n\"converted\" from Windows to Linux (SuSe 7.1) and really appreciate the stability and speed of LInux/KDE. The KDE 2.1.1 is great. But there is one drawback though and that is the lack of an office application. I use StarOffice 5.2 and it's do it's job but I do miss Microsoft Office sometimes.  I've also tried out Applixware and this Office application is fast and nice but dosn't support Word documents other than through RTF format and also have problems with using Excel formated spreadsheets. So from my point of view an Office suit (mostly Word processor and Spreadsheet program) that is capable of reading/saving to Microsoft format using TrueType fonts is one of the most important things needed for me (and many other user's I think)."
    author: "Claes Carlberg"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I would really like to see an option to disable automatic copying to the clipboard on selecting text. This is really anoying. I hate not being able to select anything else after something is already on the clipboard. Yes, I know about the clipboard manager thingy saving everything you copy to the clipboard for later use, but it is a pain to have to use that. In the latest version of KDE I didn't even see an option to turn this \"feature\" off."
    author: "Derek Petersen"
  - subject: "Qt 3.0"
    date: 2001-05-04
    body: "This is the job of the toolkit, and thankfully Qt 3.0 will address this problem.  There will be two clipboards: one for selection and one for ctrl-C/V/X style.  This way you get the best of both worlds."
    author: "Justin"
  - subject: "Qt 3.0"
    date: 2001-05-04
    body: "This is the job of the toolkit, and thankfully Qt 3.0 will address this problem.  There will be two clipboards: one for selection and one for ctrl-C/V/X style.  This way you get the best of both worlds."
    author: "Justin"
  - subject: "Qt 3.0"
    date: 2001-05-04
    body: "This is the job of the toolkit, and thankfully Qt 3.0 will address this problem.  There will be two clipboards: one for selection and one for ctrl-C/V/X style.  This way you get the best of both worlds."
    author: "Justin"
  - subject: "Whoops"
    date: 2001-05-04
    body: "Sorry for the flood there.  The connection was timing out here, I didn't realize the posts were actually going through.."
    author: "Justin"
  - subject: "Re: Qt 3.0"
    date: 2001-05-06
    body: "Yep, QT3.0 will finally do it the X11 way :-)\n\nthe selection buffer/quickpaste was never meant to interfere with the explicitly copied and pasted clipboard. One could paste directly from the selection with middle-click (or shift-ins), or do a more permanent copy to a seperate 'clipboard' area explicitly, which would then be preserved.\n\nThis is actually how most X apps handle it, Qt just didn't get it quite right (about the only thing they did mess up though!), and I've *very* glad that instead dropping the very useful select-on-copy setup, they actually reread the clipboard docs and are going to fix it right!"
    author: "Kevin Puetz"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Ah ha, Klipper. Pain in the ass waste of memory that you can't turn off. This was once a part of KDE utils but the developers deemed it so good that it got in to base, why?? You can turn it off by deleting the autostart file though which is:\n\n/usr/local/kde/share/autostart/klipper.desktop\n\nOf course if you installed kde in usr/local that is, regardless it's in share/autostart under your main kde path.\n\nI'd like to see KDE add NOTHING else till it's cleaned up a bit. Strip base down to be a base system and nothing else: Window manager, panel, konqy & things like the control center. Essentials without the nice to haves. KDE is too damm big"
    author: "David"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "Are you one of those people with a 486 and a 500 meg hard drive? lol\n\nI don't think KDE is too big at all. The auto-copy on selecting text it just really really anoying and you can't even turn it off."
    author: "Derek Petersen"
  - subject: "Hi, here's what I want :)"
    date: 2001-05-04
    body: "I vote for \"Configuration Utilities\".  I've tried \"sndconfig\", \"linuxconf\", \"Xconfigurator\", \"Xdrake\", \"chkconfig\", \"ifup, ifdown\", \"network\", \"Mandrake Control Center\", \"Desktop Switching Tools\", etc.  Having a nice user interface, and centralize these configure utilities are a bless.\n\nWhat I want is to be able to turn on and off services in KDE Control Center, configure my apache, configure my ftp, configure my ssh, configure my smtp, configure my pop3, configure my network, configure my usb hardware, configure my PCMCIA adapter, configure my monitor, configure my screen resolution, configure my sound, and and and ...:)  This will make my life, my friends, my co-workers, and billions who will use KDE after us a happier camper.\n\nWhat I want second most is KOffice.  I compiled a CVS version yesterday.  It's in terrific shape.  There's this one bug that I find Kivio should be at 1.0, is the connector thingy.  It's not connecting.  I used to be able to connect in 0.9 or something.  KWord is improving quite well.  What I want in KOffice is to copy and paste embedded objects.  For example I would like to select a part of KSpread and want to paste it in KWord.  Another feature is drag and drop.  Being able to drag an object from KWord and drop it in KSpread or KPresenter would be awesome.\n\nMy third is Konqueror.  There are some websites Konqueror is not rendering correctly,  www.espn.com for example.  A feature I want in Konqueror is to sort the \"History\" by date or categorize by date rather than have a list of URL visited.  Another feature I want is the ability to drag an image from a website and drop it in my desktop.  Yet another feature I want is to be able to paste over selection in the \"Location\"; it's annoying to me to select the URL in \"Location\" and when I tried to \"paste\" my previously copied URL, it simply use the current selected URL.  This got to be very annoying.\n\nMy fourth is speed.  I don't particular care too much about speed since I don't turn my computer off often.  :)\n\nThank you for listening.\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: Hi, here's what I want :)"
    date: 2001-05-04
    body: "I agree for the most. KDE team is doing an excellent job, but maybe it's time to get real system configuration utilities in KDE Control Center.\n\nGetting configuration utilities to work needs a lot work, and I think we should get distributors making those apps to KDE. This way KDE developers do not have to spend their valuable time making these tools. Most distros make their configuration utilities with GTK, which makes their look&feel different from KDE. Since KDE is used on most Linux (*nix) desktops, those utilities should be in KDE Control Center.\n\nAbout KOffice, what we really need is FILTERS. I know that this has been discussed on developer lists and lot has been done (look at KOffice site), but Krayon, KIllustrator and Kivio are not very useful without filters.\n\nKDE team has done very good job already, and hopefully I can help them more in future. So far I've done some translating."
    author: "Eleknader"
  - subject: "Re: Hi, here's what I want :)"
    date: 2001-05-04
    body: "I agree too.\nIf its Mandrake, RedHat... should not make the big difference on Linux configuration. Of course, KDE cannot configure the various application that the different Distribution contribute with, but \"standard\" things like, postfix, sendmail, IP security, sndconfig.... SHOULD and MUST be a part of KDE.\nLets take Mandrake. All configuration is done with Gtk+ application. Here you can tune your computer, make software upgrades etc. of course its Mandrake who has made this, but it is a thing that made a couple of people I know switch into Gnomers.\nIf KDE should win some more \"market\" it needs to focus on three things, stability, speed and general Linux configuration."
    author: "jegjessing"
  - subject: "Re: Hi, here's what I want :)"
    date: 2001-05-04
    body: "CVS Konqueror permits sorting of History by Date, although it is still organized by sites in this case.\n\n:-)"
    author: "Sad Person"
  - subject: "Re: Hi, here's what I want :)"
    date: 2001-05-04
    body: "> CVS Konqueror permits sorting of History by Date, although it is still organized by sites in this case.\n\nnot just CVS, this is possible since 2.1 IIRC. And spark and some of his friends are working on making it categorizable by date as well, so stay tuned :)"
    author: "gis"
  - subject: "sort history by date (was:Hi, here's what I want)"
    date: 2002-07-01
    body: "I read elsewhere that this is possible. but i can't find out how...\n\nhow do you do it?"
    author: "Joey Sark"
  - subject: "Re: sort history by date (was:Hi, here's what I want)"
    date: 2002-07-01
    body: "oh poo. \n\ndon't menuclick on \"History\" - rather, menuclick on any subentry (website) and --> sort --> by date. of course, this does not sort just the history for that website, but all of them. just a strange place to find this menu option.\n\n"
    author: "Joey Sark"
  - subject: "Cut & Paste - me too [Re: Hi, here's ]"
    date: 2001-05-05
    body: "Pasting is my pet peeve. I wish I could select the text to overwrite and hit \"paste\"."
    author: "george moudry"
  - subject: "Re: Hi, here's what I want :)"
    date: 2001-05-07
    body: "The location thingy used to annoy me too, but the little \"x\" next to location now clears the field so you shouldn't have that problem anymore.\n\nYou already CAN drag images from a website to the desktop!"
    author: "Martin Fick"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "To be honest, what kde really needs are womans.. yeah.. for me ;-)\nnah.. really.. what we need right now is a full office suit, we need to speed up koffice cuz' i think its what the future user will need.\nI think the config part is something that distribution should do, kde is a unix enviroment, not a linux enviroment.. and i think we can't afford to lose developement time in a linux-only feacture..\nother thing.. make konqueror a real browser(to me is) but we are lucky of a good bookmark manager, a good history manager.. a sidebar(just like the one you can find in mozilla) and things like that.. speed is important too..\n\nWell. i know i asked a lot of things.. but i hope someone read them and think about them.. as soon as a i lean c++ i will start coding something for kde..\n\nkde is a great desktop enviroment/programming plataform..  thanks kde people\n\nSee you\n\nR"
    author: "What we really need are... womans"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "as much as I would love to have a nice installer for KDE I think Konqueror needs as much development as possible, Lay on the features and plug in support, add a quick bookmark feature (I think I have seen that feature allready?) I think a built in \"getright-like\" program would be pretty cool, I know there is one out there, but it just doesn't feel very smooth (intergrated) A z-modem like resume built in to Konqi would be a superlative feature! I'm just a filthy end-user so I just have filthy end-user needs :) \nI think KDE should try to put IE and Nautulus to shame! Konqi allready does it in alot of ways, but KDE's flagship Killer APP should outshine them all, the pride of KDE and it's users!\nwhatever you people do is allright with me, It allways reflects what KDE users want. and that is what truly matters."
    author: "L.D."
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I think a Getright-like application is extremely important. So I decided to start coding !!\nI am already working on it. I will be releasing it in a month or so, so keep tuned!!\n\nRegards"
    author: "PMC"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "I thought there all ready was one... something like Caitoo?"
    author: "Steve Hunt"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "Yes, there is that one but it is really not that intergrated into KDE that well, I have used it and it is missing somthing, I think it needs to be a little more intuative as well. If it was like \"Getright\" it would excellent.   I thing that it should be intergrated allmost seemlessly into KDE. heck, maybe a Konqueror specific app. \nI think would really make Konqueror an even better app than it is now."
    author: "L.D."
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "Integrating it into Konqueror sounds good.  Maybe we could do what Opera does with downloads.  Opera has a powerful download manager built into the browser.  That might be a good solution."
    author: "Steve Hunt"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "EXACTLY! an intergrated download manager, with resume and segmented downloading all the other valuable features that getright and the Opera download manager has! That will go far in making Konqueror the absolute best browser,ftp,file manager there is! done brainlessly like Getright! I don't mean that it has to be an idiots program, just intuative. if it's intuative that is 90% of the battle right there! \n\n\nBelow is just some ideas i'm throwing out. I am just a dirty-filthy no-account end-user with no programming skills. please don't brow-beat me :)\n\n(maybe even have a download percentage box built into the browser (that you can click and detach if you would like)\nthat  you roll over it and it displays expanded information in a pop up! something that doesn't get in the way and doesn't clutter up the desktop or your browsing.)"
    author: "L.D."
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2004-11-02
    body: "There is very good download manager for X (d4x :), but.. gtk2..\nBeside this, it's quite fast and nice from perspective of system (in compare to flashget under windows, which kills system performance)"
    author: "What about d4x?"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "koffice...continue improving it until secretaries demand it of their managers..."
    author: "frank"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "If Koffice could be a <b>good replacement</b> for M$ Office, Kde would be almost perfect !!I just want it to be lighter ... It 's a quite <i>slow</i> on my PII 350 !<br>\n<br>\n<br>\nthank you for your incredible work !"
    author: "Philippe Lafou"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "The frustrating question here is the reality that KDE already does a very good job at this. The new print functions, adding a journal and gantt capabilities to korganizer, filter enhancements to kmail and the work being done on koffice. It is coming along and it just takes time. Although I miss seeing development on krayon. The upcoming QT 3 and Gideon are really exciting...\n\nIn my opinion what KDE needs to do now is not focus on providing programs that are as good or better than windoze alternatives but focus on extending the paradigm by extending the use of what is now available in dcop and kparts. For example who uses knotes? They were a rage a few years back but just being able to stick them on a desktop is only so good. How about promoting a framework to integrate KDE applications even easier then it is now? How about being able to attach knotes to a document and have the document be able to launch knotes when called (if knotes is on the system)? So I could send a document to you and it would be a compound document with information that it had attached knotes... or korganizer calendar event, or kjot or whatever.\n\nPossibly defining interactions within kparts so that when a KDE program opens up something with an attached document an integration toolbar is shown with an icon for it. Clicking on the icon shows what is available for the particular application.\n\nIt is already possible to launch an application with korganizer alarms for instance and to send it command line args. How about making this work within a framework where a drag and drop provides a list of options. How about having a compound document viewer to see not only what they primary document is but what has been attached so you can vew and edit attachements.\n\nWhat I'm suggesting is a method to make it even easier to develop (via a framework for interaction and compound documents) and easier to use application interaction. The idea is that while I would like to have various interactions among programs I'm not ready to take the time, therefore I don't begin to realize the potential. By making it more efficient and structured suddenly stand alone applications integrate as seamlessly as monoliths with very little developer or user effort. \n\nI'm suggesting this not just for koffice but for all KDE apps so that computing becomes very organic in that you can interact with far less constraint. The idea is to have a UI method and guideline for for easy and consistent compound documents that works as slick as kparts does.\n\nAnother thing I noted was the idea Eazel had for notes in directories. It's one of the very few things that sound attractive about it. With the idea I've suggested this would be easily done but in contrast it would be far from the limitations of a design like Eazel. Any program could be enabled to a set of prescribed interactions of another by implimenting a framework and possibly some form of IDL.\n\nI hope this gets some note. While these things are possible now with KDE they are not yet at a level that they are widely used and certainly not where they invite a paradigm shift. This would be a grand use that would turn heads! In effect any application could inherit another on a whim provided they are enabled and instead of developing large feature sets we could instead focus on buidling great features and merging them with our favorite apps and widgets."
    author: "Eric Laffoon"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "> How about being able to attach knotes to a document and have\n> the document be able to launch knotes when called (if knotes\n> is on the system)? So I could send a document to you and it would\n> be a compound document with information that it had attached\n> knotes... or korganizer calendar event, or kjot or whatever.\n\nHey, I like that idea -- a reusable annotation mechanism.  What about sticking notes other places as well, like in konqueror, attached to specific directories and files or even web sites?"
    author: "Andy Marchewka"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: ">Hey, I like that idea -- a reusable annotation mechanism. What about sticking notes other places as well, like in konqueror, attached to specific directories and files or even web sites?\n\nThe concept I'm advancing would not be limited to knotes... it would mean a document from any emabled application and to anywhere that KDE touchs so you could attach a note to a web page, a wave file to a directory or attach a web page to an email and indicate it is to be opened with Quanta and display attached notes as well as a Kivio diagram.\n\nThe concept is to provide a framework for melding all of KDE into one seamless and intelligent (as opposed to the fairly static presentation now) experience. The next level!"
    author: "Eric Laffoon"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Actually, the idea of attaching notes to folders,\ndocuments, programs, web sites, etc, so when you visit then again in Konqui you see the note, is\ngreat.\n\n Another nice thing would be also to incorporate the nautilus notion of \"emblems\", to file icons.\nCoupled with alpha blending, they will be both\nusefull and cool."
    author: "Count Zero"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "> Actually, the idea of attaching notes to folders, documents, programs, web sites, etc, so when you visit then again in Konqui you see the note, is great.\n\nThe idea I'm advancing is not file manger bound! It's a framework that means you could see it in konqi sure, but you could attach a sound to an image or a note to a web page in Quanta or a link to a calendar item in a kword document..."
    author: "Eric Laffoon"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I would clearly mostly want some general configuration files mangement app. \nSomeone talked about an xml based app.\nI think this would be great and would probably allow distributions to adapt to their own system.\n\nSecond important thing: I think KDE relies too much on advanced or newer features of XFree. No problem at home but at work I have an \"old\" NCD Terminal that keeps reporting Unknown Extensions. And I can't switch keyboards anymore. I don't think the 1997 Ncd X server uses that Xkb mechanism...\n\nI know there is a font installer under developement, so I will not ask for this. I just hope it will be a great one!\n\nOh! And that \"preview sound files\" Nautilus has is just cool when you're looking for a specific song you don't remember the title what that goes lalala....\n\nAlso the Mail notifyier should somehow be able to interact more strongly with kmail and maybe other mail apps. Like a right mouse menu allowing you to either check your mail or launch the editor...\nI had to find the command line option for kmail (--check) in the docs to have it behave as I wanted.\nAnd I still haven't found a way to have it check the mail only on the mailbox it's currently monitoring.\nNot very user friendly ?"
    author: "Aris"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I agree with your point about a configuration management app.  I think that it would be possible to make configuration files so that they hint at how a gui should be created and still make them editable by hand.\n\nAs far as your XServer problems, they are unfortunate, but not likely to get fixed.  Almost everybody uses XFree86 or one of the commercial X servers, which support these extensions.  You will have to find a developer that has access to the XServer you mention, and convince him to fix the problems(not likely:().\n\nI have seen the font installer, and it looks very good.  It doesn't have XRender support yet, but the author said that he would work on it:)\n\nThere is a lightweight embeddable sound program called Kaboodle in CVS that should be able to preview sounds in Konqueror.\n\nMatt Newell"
    author: "Matt Newell"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Why ask? You have a whole database full of open bugs, many of them dated. Just wade through and fix as much as you can. I know it sounds boring but it would be very useful for the users.\n\nWhen you are done there are alway the wishlist reports :-)\n\nI myself have a big folder in Kmail full of confirmations of KDE bugs I have reported. Sometimes I look through that folder to see if I can delete something, but the net change during the last moths has been a grow.\n\nAnd yes, I do have plans to help. I have signed up for a C++ class in the fall."
    author: "Erik"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "At the moment, I most want KWord to be fully usable.  Also, I'd like to see ATM-style typeface management.  My understanding is that both of these things are being actively worked on, so keep it up. :)\n\nAnd by the way: I'd like to thank all the KDE developers for their hard work producing such a great system.  I used to be a GNOMEr, but KDE 2 won me over.  I use KDE both at home and at work, and I haven't regretted switching for one moment.  KDE rocks, keep up the good work!"
    author: "Michael Ashton"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I would really like to have distribution lists in kmail. The list could contain entry's from the various supported addressbooks and distributions lists (recursive lists). double addresses due to the recusive nature should be filtered out upon sending. Related, it would be great to have the ability to forward a message to a distribution list, thereby excludeding the adsress it came from, from the distribution list.\n\nI allready put these wishes on the kmail wishlist."
    author: "pasha"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Well!\n1) Speed and Stability\n2) Offline Browsing support in Konqui\n3) Good import filters for MS Word in KWord\n4) More Themes\n5) Ability to save images along with the web page"
    author: "KDE_User"
  - subject: "Good offline browsing support exist already ..."
    date: 2001-05-04
    body: "Use WWWoffle - konqui should play nice with it.\n\nA nice interface or kio-slave to wwwoffle's cache would be good (the one it ships with is pretty boring but simple to modify). A way for konqui to detect if wwwoffle is running and then add a few buttons or menu items for controlling it would be neat."
    author: "guy"
  - subject: "Re: Good offline browsing support exist already ..."
    date: 2001-05-07
    body: "Yes, wwwoffle is perfect for dial-up connections. And konqui is playing perfect whith it."
    author: "milan"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "The ability to save images along with web pages is being implemented in a Konqueror plugin called Web Archiver.  It's in CVS in the kdenonbeta or kdeaddons (it's currently being rearranged)\n\nThanks\n\nSD"
    author: "SD"
  - subject: "Speed, Size and Information"
    date: 2001-05-04
    body: "...are my most wanted features\n\nSpeed: I know, that KDE applications are not dramatically slower than other programs, but the startup time of _any_ KDE application, even the most simpole one, is a pain. I know, that much work on this issue was done in the past and as far as I remember, developers said that this is mainly a problem of GNU's linker. So, is anybody in contact with the GNUs to improve this?\n\nSize: Well, not so much the memory footprint, but the size of the binaries. I might be wrong, it could even be a problem of KDevelop. Just an example of a little (VERY little) application I wrote some days ago: First, I wrote the program, made the makefile by hand and the resulting binary was about 10k in size. Then, I created a project in KDevelop, compiled again and had a binary 300k in size. As KDevelop is using autoconf/automake just like KDE, I fear that KDE suffers from the same auto features.\n\nBTW: Speed and Memory Footprint are two most most named disadvantages of KDE (from the people I know) \n\nInformation: This is what's lacking most, IMHO. I don't mean the KDE Help System, I mean technical information. What is KDE doing, how and why? Although there's a little bit of information more and more coming in (like the Panel Applets tutorial by Matthias Elter a few days ago), I have the strong feeling, there could be more. Here a small example from yesterday: From time to time I'm compiling from anonymous CVS. But for one or two weeks now, compiling fails all the time. Yesterday I found out that even ./configure failed with error messages that were too esoteric for me to understand. Around midnight I had the idea (from a hint for CVSUP) to download kde-common and link the admin dir to the modules manually, after deleting the existing admin directories. Now it compiles. But no information about this admin stuff, the auto routines, configure and other secret things that are happening inside KDE can be found -- or at least I'm too dull to find it, but then there's lacking the information about where to find the information ;-)"
    author: "Johann Lermer"
  - subject: "Re: Speed, Size and Information"
    date: 2001-05-04
    body: "As far as the size of kdevelop apps, did you turn off all debugging features? while working with a project in kdevelop some libs are statically linked and extra debugging information included in the binary."
    author: "Anders Lund"
  - subject: "Re: Speed, Size and Information"
    date: 2001-05-04
    body: "Well...err... yes, I guess. I'll have another look at the compile options this evening."
    author: "Johann Lermer"
  - subject: "Yes KDE (esp konqui) is WAY too slow"
    date: 2001-05-04
    body: "The filemanager is the key application - it is much much much too slow.\n\nKDE developpers should try out a Windows system or MacOS/X and see how fast their filemanager type apps are able to pop open windows.\n\nSigh I fear that a good usable filemanager is a dying breed these days. Windows integrates **everything** into their filemanager and KDE integrates even more! ;-)  MacOS/X finder (which is a little like the NeXT workspace app) is more my style - a good fast, gui and keyboard accessible filemanager is crucial you can start apps or open files from there - the integration part with apps opening \"inside\" konqui and explorer is silly IMHO.\n\nI didn't mention nautilus since it is completely unusable unless your box has at **least** a 600mhz CPU. Nautilus may become popular in a few years but right now it is way ahead of its time in terms of the resources it needs. My fear is KDE will become the same ... I have 100s of 400mhz machines that I DON'T want to upgrade (and won't) for several more years. Right now those that have FreeBSD and Linux (about 25) all run kde1 since kde2 is much much much too slow."
    author: "someone new"
  - subject: "Re: Yes KDE (esp konqui) is WAY too slow"
    date: 2001-11-21
    body: "yeah, right!\nkde2 is so slow, piece of useless software created by spoiled ignorant programmers.\n\nkde1 was running smooth like silk on my p2-400mhz (as well as on p1-166,\nkde2 cant even run as fast on p2-400)\n\nBAD JOB, kde2 people!"
    author: "mike"
  - subject: "Re: Speed, Size and Information"
    date: 2001-05-05
    body: "Speed is indeed a problem with modern computers and programs. Not just KDE programs, but as we're talking about KDE, let's use that example.\n\nI want my KDE desktop running on my 400 MHz 128 MB PC to feel as fast as my 14 MHz 4MB Amiga did a few years ago. It's a matter of perceived speed, and that's what's important. Say I want to look at some files in a directory . Click on the desktop icon in KDE, and you sit there for 3-5 seconds waiting for the computer to do a very, very basic thing; list a directory. Do the same on the Amiga, and the window pops up immediately. Granted, KDE shows fancy icons and lots more info, but surely my 400 MHz box should be able to do that just as fast as the old Miggy?\n\nSure, todays apps have tons of more features, complex theming and so on, but the computer are much more powerful, too! \n\nI think something important happened when the PC started to take off as a home computer. As the computers got more powerful, the programmers could be a bit more lazy. It is indeed a good thing to be able to write software in high-level languages instead of assembly, but it went too far, and people started to get used to slow programs. \n\nSo lets break the trend with KDE. Er, you guys do it, I don't have the time. ;-)"
    author: "Johnny Andersson"
  - subject: "Re: Speed, Size and Information"
    date: 2001-05-06
    body: "I noted that all KDE apps startup _much_ faster\nwhen using a different window manager (Xfce or ICEWM).\nSo I guess the KDEWM is a good thing to improve.\n\nAlso, various KDE programs have resource-consuming issues (look at your processor use when viewing an animated gif in Konqueror, now this IS a BUG)"
    author: "AC"
  - subject: "Re: Speed, Size and Information"
    date: 2002-10-24
    body: "I don't think K Apps are much faster to load under other window managers.  On my 4 year old box, OpenOffice Writer takes about 30 seconds to load under IceWm, Gnome, and KDE 3.0.  I think it is the actual applications that are a bit bloated."
    author: "S.W.B"
  - subject: "Talk about \"lost\" features"
    date: 2001-05-04
    body: "Of course there are always wishes for new features but don't forget \"old\" features of KDE 1.1.2 which got overseen when KDE was rewritten:\n\n- Hot corners for activating screensavers\n- Frameless/hidden/sticky windows based on class or title\n- Automatic hiding of [external] taskbar\n\nI bet there are more, but these are the ones I was most used to and still remember. Others may miss other \"old\" features. The code for all this exists and someone who is familiar with both KDE-versions should be able to easily convert it."
    author: "ShyGuy"
  - subject: "Re: Talk about \"lost\" features"
    date: 2001-05-12
    body: "add\n- give back control of the root window (stop covering it)\n- restore the ability to configure the decoration widgets\n- make click through, raise and activate actually activate\n- make upgrading much easier (i'm on SuSE 7.1)\n- allow multiple desktops running on same machine\n\nOverall though it is improving fast, <B>THANKS</B>!"
    author: "jw"
  - subject: "Re: Talk about \"lost\" features"
    date: 2001-06-08
    body: "The external taskbar which is hideable is the feature I miss most. \n\nI also miss the configuration of the terminal window with respect to colours of my choice.I used to use an amber on black terminal and find it very comfortable."
    author: "Graham Palmer"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "No new features, just \"minor\" changes:\n- good ol' KDE1 hideable taskbar\n- even more stability in general\n- make kab read/write vCard format\n- give kmail some sort of journaling. when my kde crashes, i get plenty of doubles in my inbox which already filed.\n- even more Solaris 8 support\n\nrunning 2.1.1 on Solaris 8..."
    author: "Kai"
  - subject: "Other \"platforms\", like KDEMacOSX"
    date: 2001-05-04
    body: "Is it possible to make KDE run on MacOSX? Rumours says Qt will soon be available for MacOSX. If you replace the X11-dependent code with code dependent on Qt/MacOSX, wouldn't it theoretically be possible to run KDE on MacOSX?"
    author: "Loranga"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "One little thingie still iritates me from time to time.. I used to be quite a big ctrl-XCV user, but the problem I have in kde (x in general actually) is the following.. I make a selection, ctrl-C, and usually I then make another selection and press ctrl-V to replace this selection by the previous selection.. But ofcourse, by selecting the to-be-replaced selection, the previous one is gone and the visual effect is that i don't get a paste at all.. The truth is that X sees selecting text as a copy operation. This can be solved by using the kde copy/paste history tool (quite handy), but not so handy if you just want to make a quick ctrl-XCV-style paste I used to do in windows.. \n\nIs there a possibility that kde can do something about this of is it a matter of XFree?"
    author: "xastor"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "QT3 should solve this."
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "yeah, QT2 mishandles X11 clipboards a little, resulting in this annoying behavior :-)\n\nif you are explicitly copying and pasting, it should avoid being broken by the selection - which it will be in QT3.\n\nOf course, here's hoping that someone backports the fix into QT2 as well :-)"
    author: "Kevin Puetz"
  - subject: "Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Get shot of Netscape.\n\nNetscape crashes. I got the new netscape thinking it would be better. But its big and clumsy and loses your emails and guess what, it crashes.\n\nIf it was only my machine this happens loads on I wouldn't bother grumbling. But it crashes on all of the Linux'es I have going at home and works servers.\n\nI haven't got the new KDE yet, but I'm hoping when I do it will be a 'full' web explorer so I can stick Netscape in the bin.\n\nOh, that bit about not having the new KDE yet. I have actually got it as I bought Suse7.1, this fell out of favour with me when it miss-guessed my monitor and put the frequency UP. I soon knew about it of cause, my monitor automatically gave a signal that there was a problem. A puff of smoke."
    author: "Robert"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Improvements to Konqueror would be nice.\n\nAt the moment it chokes oon certain JavaScript.\n\nIdeally what i'd like to see is a way to select different rendering engines in Konqueror.  Say for example the default one doesn't work on a site, you open up a menu option and switch to the Gecko engine, or even (though i don't hold out much hope of that one) the Opera one.\n\nThis would also have the added advantage of allowing webdesigners to check what their work would look like in multiple browsers, while retaining the UI they are familiar with.\n\nAlso a good media player would be nice.\n\nI know there's a lot of good media players out there, but you seem tohave to use so many different ones to be able to play everything.\n\nXMMS for Audio and a few Video formats,\nAviplay for various Windows formats using Windows codecs,\nXine for MPEG/DVD.\nRealplayer for RealMedia, and so on.\n\nWhat i'd like to see is, well not something exactly like Windows Media Player, but a central Media center.\n\nSomething that can handle all the above media formats and more, whether by built-in support or drop in modules.\n\nThere are other areas that need work, but the majority of computer users use their PC's for the Internet, Media and the odd bit of typing, so that's the area to concentrate on IMO."
    author: "Ack"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I would like to see the version of QT and KDE without X-window available for desktop, too. It has been developed for PDAs etc., but there are many older PCs that could use a light version of Linux + QT + KDE. Eg. I have an old P100 with 16 MB of memory. It runs Win95 well, including surfing with Opera. X-window + KDE is far too heavy for it.\nBefore Linux was advocated for having smaller memory footprint than Windows. Not any more! Or, you could use some ancient window manager, but that is not realistic for new users.\nThink also how Linux and its desktops are getting popular in poor countries, because those people are not able to pay for Microsoft. They can't pay for the newest hardware, either.\n\nJuha"
    author: "Juha Manninen"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Just wait for that SuSE package to arrive. In SuSE 7.1, the YaST 2 modules are integrated in KDE's control center."
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Far more cooperation & standardisation with Gnome.\n\nI would like to see DCOP and Orbit work together as suggested in http://kt.zork.net/kde/kde20010331_4.html#3\n\nWhat about Qt layered on top of GTK? (QTK)\n\nReally, this is not meant as a troll.\nI can dream, can I?"
    author: "freedesktop"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "#1 for me is m$ import/export filters on koffice apps.\n\n#2 gnome has the option to 1) close a window 2) nuke it (or somesuchphrase). i think that's handy. in kde i have to open up a terminal and kill -9 it if it's frozen. (i.e. every 5 minutes with netscape...)\n\n#3- more stability. stuff in kde 1.x used to crash once in a blue moon, kde2.1 on my computer is certainly no more stable than windows 98 (eek!). personally i put stability _way_ above things like animating/fading menus etc. (lack of stability is one of the things that moved me away from windows in the 1st place...)\n\ngreat job with kde 2.1 'tho folks, i've been raving to all my friends about it since installing it a few weeks ago! #:-)\n\np."
    author: "phil"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Regarding #2: Press Ctrl+Alt+Escape or whatever you have defined under Look&Feel/Key bindings and click on a window."
    author: "ShyGuy"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "for #2\nHit Ctrl-Alt-ESC and you'll get a skull as your mousepointer. Now click the desired window and it will go away *g*\nAnd if you want to be really sure (for instance about netscape which likes to close its windows and still run in the background) open the minicli (Alt-F2) and type \"killall -9 netscape\"."
    author: "mETz"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "#1 - in work\n#2 - covered above\n#3 - I'm running KDE 2.2 alpha and it's rock steady. Of course I compiled it. However from 2.0 on it's been good and getting better. I can't remember the last time KDE crashed and with stable releases app crashes are pretty rare. Admittedly some system configurations or other things can be a problem but for the most part stability issues are configuration related. If you have stability issues iwth KDE maybe you should look into this? I have a desktop up sometimes for months."
    author: "Eric Laffoon"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "#1 - in work\n#2 - covered above\n#3 - I'm running KDE 2.2 alpha and it's rock steady. Of course I compiled it. However from 2.0 on it's been good and getting better. I can't remember the last time KDE crashed and with stable releases app crashes are pretty rare. Admittedly some system configurations or other things can be a problem but for the most part stability issues are configuration related. If you have stability issues iwth KDE maybe you should look into this? I have a desktop up sometimes for months."
    author: "Eric Laffoon"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "thanks for all the follow-up's folks. i should perhaps qualify #3 a bit further- the desktop itself has not (in about 2 weeks since install) failed once, although in a typical day i seem to get about 1 or 2 of the key applications (normally conquerer i guess) falling over at some time."
    author: "phil"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Completely agreed on #3."
    author: "not me"
  - subject: "Focus"
    date: 2001-05-04
    body: "When I'm using an app, and I decide to start another app, either by the panel or a menu option, I don't want the launched app to appear on top when it starts, nor do I want it to grab focus.  What I want is:\n\n* focus follows mouse\n* focus stays with whatever currently has focus, unless I change it by clicking somewhere else\n* No windows are ever created on top of the window which currently has the focus\n\nI haven't found any window manager or desktop environment that does this."
    author: "Jon Evans"
  - subject: "Re: Focus"
    date: 2001-05-06
    body: "Yes!\nThis is interesting in so,e case:\nWhen I right click on link in konqueror, \nand choose \"new windows\",\nI don't want to see this windows on top."
    author: "thil"
  - subject: "Re: Focus"
    date: 2001-05-06
    body: "I would like this for opening new windows in konqueror to open behind it"
    author: "fix"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Does this exist? An installer such as the ones in windows. You click on setiup.exe and it asks you several questions such as aggree on the terms and conditions, directory to install, packages to install, serial number :))), etc."
    author: "Solhell"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "I've never understood why people want a KDE installer.....unless they mean a KDE frontend to rpm -i or (or other distribution / OS equivalent)\n\nI think a lot of people don't realise that package management and installation on Linux and other Unixes is actually better than windows.\nWindows 9x has NO standard installation and package management tools besides a registry key for adding the name of your package, and the uninstaller.\nSo as a result every program has a slightly different installation procedure (though usualy rather simple) and dependency issues are difficult to the point that most software always installs everything it needs to run, which can result in DLL version hell...\n\nbesides you cant really get much simpler than rpm -i *"
    author: "Stuart Herring"
  - subject: "Multiprocess Konqueror"
    date: 2001-05-04
    body: "What I want is a multiprocess web browser, like IE. It stinks to have ALL browser windows disappear because of some bug triggered by a badly written web page.\nProcess switching under Linux is extremely fast, and besides improving stability, so making Konqueror multi-proccess would actually increase it's speed as no locks would be needed to protect access between several threads to shared data. Have you ever notice how slow goes Konqueror when you open more than just a couple of windows?"
    author: "Anonymous Coward"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-04
    body: "I agree that threading Konqueror *could* make it faster.  But it is absurd to think that this would somehow lead to increased stability.  Threading is very difficult in a complex application such as Konqueror and would need extensive testing before it would stabilize.  Not to mention how hard it is to debug a multithreaded app.\n\nMatt Newell"
    author: "Matt Newell"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-05
    body: "I said \"multi-process\" as opposed to \"multi-threaded\". Each browser window is conceptually independent of the others (except maybe for some popups), so I really can't see why it can't be multi-process. It would only involve the use of some locks to control access to the history and bookmark files..."
    author: "Anonymous Coward"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-05
    body: "I forgot to say: every unix program is multi-process by default. Try this on a xterm:\n$ xlogo &\n$ xlogo &\n$ ps\nSee? xlogo is multi-process...\nEven if you need launch a process from another one all you have to do is fork()...\n\nMemory protection is a Good Thing. IMO, since process switching on linux is fast, the main reason linux programers use threads (or single process programs) is because they're too dumb or lazy to use IPC."
    author: "Anonymous Coward"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-06
    body: "You gotta be kiddin'.  Processes take up memory and are slow to load on Linux (given the large number of symbols that must be resolved).  A large program like Konqueror, which links with many libraries, can take a while to load.  Notice the difference in time lag between the first Konqueror window you open (make sure no konqueror shows up in 'ps -auxw') and subsequent ones?\n\nAnd no, people don't use threads b/c they are stupid or lazy.  Threaded apps are quite a bit harder to write than ones that fork.  So people who use threads are both smarter and more diligent.  Threads have a number of advantages, like (a) memory consumption, and (b) speed."
    author: "Sage"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-06
    body: "New processes which must exec() are slow to load, fork()ed processes using the same code (i.e., no exec()) are fast.\nAnd yes, many times people use threads because they are stupid (or brainwashed) and lazy. There are some applications where threads are clearly preferable to processes, mainly in programs where most data is to be shared, but different browser windows don't need data sharing except for cache and bookmarks, so using threads is just plain stupid.\nPeople usually learn the advantages of threads from OSs where there's a big difference in the timings for thread switching and process switching. Linux has a one-to-one mapping between threads and processes, so it doesn't apply here. About memory consumption, using threads buys you *nothing* if the data accessed by different threads/processes isn't the same, as is the case with browsers, and you have copy-on-write fork()."
    author: "Anonymous Coward"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-05
    body: "Actually, IE is worse than Konqueror in this respect...\n\nIf IE locks up for some reason...your entire desktop is frozen....and when IE crashes, every browser window, explorer window and even the start menu and taskbar all disapear."
    author: "Stuart Herring"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-05
    body: "\"What I want is a multiprocess web browser, like IE.\"\n\nHAH!  That's really funny.  I *wish* IE was a multiprocess web browser.  In actuality, it's all one process, and sometimes it seems like the entire GUI is being run in the same process.  An IE bug can take down not only ALL your browser windows, but all your other applications, your desktop, and even the OS kernel!\n\nMaking Konqueror multi-process would take much more memory.  I think what should be done instead is Konqui should keep a temporary file with the URLs of the sites that are open (or install a crash handler that would do this).  Then if it crashes, it could just open the sites back up again (asking you whether to open each site of course to avoid infinite crash loops!)\n\nA \"Save State\" menu option would be nice too.  Imagine saving the state of all Konqui windows on your computer at the same time!\n\n\"making Konqueror multi-proccess would actually increase it's speed as no locks would be needed to protect access between several threads to shared data. \"\n\nI do not think Konqueror is a threaded application.  QT is not thread-safe, or at least it wasn't when KDE 2.0 came out, so I don't think it would be possible to use threads in Konqueror."
    author: "not me"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-05
    body: "Your wish has come true: look at the configuration options of IE and you'll find a tiny little check box, disabled by default, to make IE multi-process. I've been using it since I installed IE5 the first time. It rocks. And no, I found no noticeable slowdowns compared to the default multithread behaviour.\nOne small drawback: a different process is launched only when you start IE again and copy/paste the link. \"Open in new window\" will just launch another thread.\n\nAs for multi-process taking a lot of memory, this isn't but a misconception: the linux implementation of fork() doesn't copy the code to the child as it's read-only. This way, even if you have 100 processes forked from the same parent, there's only ONE copy of the code for all of them, but a private memory space for modifiable data pages for each one of them. If only what's conceptually distinct (the data) is private, there's no memory waste: data for each open document would exist anyway."
    author: "Anonymous Coward"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-06
    body: "\"you'll find a tiny little check box\"\n\nOh yes, I know all about that little checkbox.  Problem is, it has disappeared from my IE options!  I think it happened when I installed IE 5.5.  Anyway, it's gone, and so IE is all single-process for me.  It also doesn't change the fact that Windows is a bad OS and any process can crash any other process and even take down the kernel.\n\nAbout memory usage:  each new instance of Konqueror started from the panel takes about 10 MB, while opening a new window of an existing instance makes the exsisting instance about 1.5 MB larger (rough figures obtained from KPM, your mileage may vary).  I don't think Konqueror is forking itself, but it is being forked off of kdeinit, if this makes a difference.  If this is the case, then the question becomes:  why is Konqueror not forking itself?  That would give stability _and_ low memory usage! (and probably faster startup as well!)"
    author: "not me"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-05
    body: "It _does_ work this way.  If you start a new Konqi window from an existing window they share the same process, but if you start a new window from the Panel it is a seperate process.  The memory footprint for konqueror is around 5MB-20MB depending on how complex the page is."
    author: "raven667"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-06
    body: "In fact...\nI didn't know about this. Thanks a lot for the tip.\nNow it should be extremely easy to add an \"open in new process\" entry in the context menu of a link..."
    author: "Anonymous Coward"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-06
    body: "Another solution for this problem would be if konqueror cud remember what pages you have open and the current state of all pages, so when konqueror does crash, it can reopen all pages automatically (except for the page that cause the crash)"
    author: "Martin Juhlin"
  - subject: "Re: Multiprocess Konqueror"
    date: 2001-05-06
    body: "Just received this reply to my message to the konqueror wishlist:\n\nUpdate to kde-2.2-alpha1, there is a kcontrol setting for when to create a new process and when not to.\n\nKDE is like a song from the Beatles' Sargent Pepper's: it's getting better all the time!!!"
    author: "Anonymous Coward"
  - subject: "Re: Multiprocess Konqueror"
    date: 2002-10-04
    body: "To those who want a multiprocess application,\n\nI wonder which debugger tool you guys use to develop multiprocess programs for Linux. I chose multithreaded programming because I can run 'ddd' to track my program, but I din't find a proper tool to debug multiprocess program. \n\nBernard."
    author: "Bernard "
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I remember we had a KDE2 wishlist. Couldn't we have something similar for future releases of KDE? It would be great if it was arranged the same way as the old one."
    author: "newbe"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "bugs.kde.org has an integrated wishlist.  You can submit a wish at any time and the developers will see it.  It works quite nicely."
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "#1 is SPEED and STABILITY !!! while stability is better than speed , it crashes much too often to work with.\n#2 Able to choose which Programm can reside in Memory for faster startup , because most of us have very much ram , and i can hold the complete kde in ram if i want to , and that would be very cool, (no i dont want to copy it on a ramdisk)\n\n\ngreetings.."
    author: "chris"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Careful Chris,\n\nThey will call you a \"troll\" if you say that!\n\nRegards,\nJames Alan Brown"
    author: "James Alan Brown"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Nah, only if you continually bitch about problems others don't have but never submit even submit a real bug report ;-)"
    author: "Mosfet"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Opps, bad grammar again - just woke up and trying to multitask with email... wonder if me will correct me (grin)."
    author: "Mosfet"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "You wakeup @ 10 to 12! Man that's a life. Of course you work all night though but still you're in mosfet's own timezone."
    author: "David"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "Well, I'm no longer employed right now so can sleep as late as I want :)"
    author: "Mosfet"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I really miss the browse button in Menu Editor... It is really difficult for beginning users to add gnome / X / terminal applications to start menu if they do not appear there automagically."
    author: "bro1"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I really would love imporovements on kde help center:\n\n* the ability to add books, like the NAT, downloaded books, howtos etc\n* possible inclusion of kde, qt, .. api docs\n* the search feature can be improved with more details: search only this book/app, select\n\nThe helpcenter should be available as a sidebar/tab in konqueror."
    author: "Anders Lund"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "1. Browsing windows shares. All clients are configured to authenicate with a central password server. It would be good to be able to connect to SMB shares using the current login name and password. Oh, and have access permissions as per your restrictions on the SMB share.\n\n2. IMAP server availability in KMail. 'nuf said on that one."
    author: "Ian"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "\"IMAP server availability in KMail. 'nuf said on that one.\"\n\nKMail 2.2alpha1.  'nuf said."
    author: "not me"
  - subject: "Define the Audience First"
    date: 2001-05-04
    body: "At this moment in time, it's my belief that KDE is not only perfectly usable now, but more so than its MS or even Apple competitors.  For what it does in the way of a user interface I really do believe it is either the best, or the only thing I've seen on the right track to being so.\n\nThe basic question here has to do with what applications are missing.  I've read the previous posts here talking about config tools, office apps, and browsers.  I think this misses the point a bit.  Konqueror is making tremendous progress, and only rarely do I need to fall back to some other browser to properly view a page.  It also looks like KOffice is on the road to be something really exciting.  The config issue gets back to the core of what I'm getting at.\n\nFrom what I've read, and personal experience it would seem that the core of the *nix user base is comprised of system admins and webmasters looking to work on the box they're designing they're work around.  On the web side, it is encouraging to see Quanta making significant progress.  Thing is, there still isn't any GUI application that compares with Macromedia's Dreamweaver.  Please, if you've only spent five minutes looking at it, scoffed, and went back to emacs you can save your reply.  This is a hugely powerful web designing platform without any *nix equivalent.  I fully realize this would not be a simple app to develop, but if KDE is looking for that \"killer app\"(tm), that would be it.\n\nOn the config side, there are some areas that I believe KDE should step into, not specifically relying upon a distro.  X configuration comes to mind.  A GUI should be the place where folks adjust the GUI.  KDE, Konqueror, and Samba should be something nearly synonomous with each other.  Not only to look at shares, but to create ones.  Other front line daemon configs could also fit in here, such as Sendmail, QMail, Inn, Apache, or even IRC.  These kinds of things aren't distro specific, and well thought out config tools for those kinds of core Internet services would be a killer app for KDE with admins and users alike.\n\nLinux, the BSD's, and the like may not be ready for converting over the generic Windows ME user (god help them), but it could be made very ready for the core users that are here now.  Convert the already faithful, and they'll bring many more to the party.\n\nDISCLAIMER: I may be dead wrong about the make up of the bulk the *nix crowd.  Even still, defining exactly who is now using these operating systems will make decisions like what are the next majorly important applications to develop that much easier."
    author: "Michael Collette"
  - subject: "Re: Define the Audience First"
    date: 2001-05-04
    body: "> On the web side, it is encouraging to see Quanta making significant progress.\n\nThank you! I'm depressed that we have not made more progress on DTDs, intelligent tag management, autocompletiion, templating and a few more things... but at least we have got our infrastructure in place and these things can be done much quicker.\n\n> Thing is, there still isn't any GUI application that compares with Macromedia's Dreamweaver.\n\nYeah, Rich Moore is pretty hot on Dreamweaver and we have discussed some of the UI concepts and how we might learn from and improve on them in Quanta. Also there is a wysiwyg web tool for KDE being developed and that is Kafka in kdenonbeta.\n\nHere is what is on the horizon with Quanta. For one there will be a new rich text widget in QT3. We've thought about doing a wysiwyg part for Quanta and something like that will happen, but not likely a full implimentation any time soon. Our current design objective is partial wysiwyg for a few reasons.\n1) wysiwyg editors are notorious for disregarding the integrity of your code so we have to address that.\n2) most importanly wysiwyg editors primarily are good at static layout... and let's face it that is largely history on the dynamic web!\n\nOur focus is to have graphical layout tools to enhance and accelerate layout design, but to really focus on development tools, like our primary PHP focus, to enable far greater productivity here.\n\nPlease note that Quanta is not just an \"I want to build an app\" web tool but a product by and for professional web developers. It is also intended to be quick and easy to pick up by people new to web development and usable by newbies... but it is not focused on being a \"so you want to build a home page\" tool. Kafka is much more focused in that direction. Unfortunately it is not very usable yet but it has some sharp people working on it.\n\nWe believe that this year Quanta will become a truly killer app on KDE and we are focusing on that. Currently we are in the 99.9% activity level at source forge... so we can comfort our impatience with the idea we must be doing something right. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Define the Audience First"
    date: 2001-05-04
    body: "I agree that Quanta is a great application. In fact it has been the one app that has enabled me to persuade several people to use KDE / Linux rather than that 'other' choice. In a similar vein to the kdevelop thread though, I would like to know if there are any plans to embed kvim as the editor. Granted, the kwrite thing is easier for newbies, but I find vim far more productive than quanta.\n\nIt would be nice for example to go\n:gg\n=GG\nIn quanta to autoformat everything like I do about a zillion times a day in vim :)\n\nAnother feature I would really like to see in quanta is an object tree browser (ala kdev) so that I can see all my php classes and drill down to a given function, bringing the editor to that place in the code.\n\nVim + code completion in quanta = killer app!\n\nTim"
    author: "Tim"
  - subject: "Re: Define the Audience First"
    date: 2001-05-04
    body: "Sorry, make that \n\ngg\n=GG\n\n:~)"
    author: "Tim"
  - subject: "Re: Define the Audience First"
    date: 2001-05-05
    body: "Thanks for a great reply Eric.  As a follow up I'd like to illustrate how Dreamweaver fits into my web development for a dynamic web site.  Just one little ol' perspective out here.\n\nPretty much any page I'm going to be working on starts in Dreamweaver today.  Yes, even if it's to be a dynamic database fed page.  I start by dumping in dummy data into tables so as to get a visual feel for how the data will be laid out.  All the graphics and tables are put in at this point in the process.  When I'm cool with the look, I'll replace the data with the variable names right within the GUI editing.\n\nFollowing this initial layout work, I would then pull the source code into HomeSite and add in my PHP code to fill in those variables.  This process drasticly reduces the amount of code I have to type in since darn near all the HTML is already in place.  Lines with variables just get the framing PHP tags and a print statement added.\n\nWhere this is a decent time saver for doing results layout, it's invaluable when designing a fairly complex form with different types of input tags.  Within DW I fill in default values with variable names, and name all the objects.  By the time I get the file into a text editor, again all I need do is add some framing tags and a wee bit of code to make it all work dynamicly.\n\nI just fealt this kind of perspective might be helpful in how a GUI based editor truly does fit into the dynamic web.  At this point on Windows it requires both a Dreamweaver and a HomeSite combo to get this kind of thing to work nicely.  A single integrated product that works this kind of thing would be a very appealing application for webmasters of all sorts I would think.\n\nLastly, since we're talking about Quanta I need to get a couple of wishes plugged in here :)  Snippets and Server Mappings, both of which are HomeSite features sorely missing from Quanta.  \n\nSnippets are small bits of code that get stowed away into a dir of their own to be inserted into the active file.  Instead of trying to map all kinds of funky key bindings, you just get an extra window allowing for multiple bits of code that can be double-click inserted.  Over in HomeSite I've got a personal library of about 50 of these little buggers, broken down into subdirectories and such for organization.  That's easily the biggest thing I miss when working with Quanta.\n\nServer Mappings are used as a redirect for previewing files.  The idea here is that directories on the local hard drive are mapped within the editor to an http domain.  This allows your server side scripts run through the local web server for testing.  Puts Apache to work for ya.\n\nOkay, rant over... thanks for the ear."
    author: "Michael Collette"
  - subject: "KIndic - a font engine to render Indic fonts"
    date: 2001-05-04
    body: "I would like to have a font engine to render Indic fonts with Unicode. How about Devanagari for a start?\n\nMy dream is a Linux desktop supporting all major Indian languages, at least one in each Indian village.\n\nI am ready to be a part of the KIndic project.\n\nThanks for all the good work.\n\nRegards,\nSandy."
    author: "Sandeep Meher"
  - subject: "Re: KIndic - a font engine to render Indic fonts"
    date: 2001-05-04
    body: "Now that QT 2.3 and KDE 2.1 support true-type fonts, all you need to have Indic font support is to find .ttf files for Indic fonts and import them into your X11 truetype font list.\n\nIf these truetype fonts don't exist, you can probably find someone who is working on making some.\n\nOther than that, you may also want to join the KBabel project to create KDE localizations in your preferred Indic dialect.\n\n--bfelger"
    author: "Brandon"
  - subject: "Re: KIndic - a font engine to render Indic fonts"
    date: 2001-05-06
    body: "It's not quite as simple as that. To properly display Indic languages requires the displayer to pick the right glyph for each letter. It's like English had four different forms for A, and the user just hit A and the computer had to figure out the right form to use, depending on where it was in the word."
    author: "David Starner"
  - subject: "two small ideas up for the grabs"
    date: 2001-05-04
    body: "Two small ideas:\n\n1. cvsKIOslave:\ntype cvs:pserver: ..etc..\nat the location bar in konq and you get a list of the \navailable modules.\nMove a module to the Desktop and you checkout\na version.\n(I know cervisia, but a tighter integration into kde would be\ncool, and KIOslave seems and obvious choice. For \nbrowsing the local checkedout version cervisia as \na kpart could be an idea, konq could be made to switch\nto cvs mode when such a directory was entered.)\n\n2. Singlet app.\nMake an option for the application launching buttons in\nthe panel that only one application will be launched at a\ntime. If the button has been pushed before and the app\nis still running it should be brought in to focus, just like\npressing on the taskbar. This is mostly for newbies to avoid\nthem having fire up a flood of identical application while\nwaiting for a slow app to start.\nMy kids do this all the time :-)\n\nThe last suggestion seems so obvious perhaps its already\nimplemented? If so please tell me how to use it :-)\n\nSune"
    author: "sune"
  - subject: "Re: two small ideas up for the grabs"
    date: 2001-05-04
    body: "> 1. cvsKIOslave:\n> For browsing the local checkedout version cervisia as a kpart could be an idea\n\nFunny you should mention this... we have been discussing these very ideas wrt to cvs implimentation in Quanta. Our ideal scenario is waking up and finding out Bernd did it. ;-) But if we could ever crystalize how we wanted to handle some of the particulars we'd like to contact Bernd (Cervisia's author) and launch into it."
    author: "Eric Laffoon"
  - subject: "Re: two small ideas up for the grabs"
    date: 2001-05-04
    body: "I'm afraid your 'ideal scenario' won't happen\nas my priorities are very different at the\nmoment ;-(\n\nOf course, that doesn't mean I wouldn't like someone else to work in that direction :-)"
    author: "Bernd Gehrmann"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I think it would be very nice for developpers to have an equivalent of libglade for Qt/KDE.\nThis would result in a very clear code to use an xml file (.rc ?) and to dinamically load the interface, especially when used with kdevelop."
    author: "BW"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "This already exists, and has done for a while...(KDE >= 2.0)\n\nI believe it's called xmlgui or something similar.."
    author: "Stuart Herring"
  - subject: "The way KDE should work"
    date: 2001-05-04
    body: "Most emphatically .. a uniform config utility. \n\nFor example:\nTo configure the screen, you ALWAYS need to be able to change the screen resolution, colour depth and monitor capabilty (refresh rate etc.).\n\nSo why can we not have a config tool that is a frontend with ALL the potential options that can be chosen. The distribution comes along and selects which of these options can be changed with their own backend tools. This KDE config tool will then only be slightly different as each distribution configures it to suite themselves.\nEach backend tool then uses the KDE config tool as an interface to the user. The KDE tool does not need to know how the backend works, nor does the backend tool need to know how the config program renders the info to the user. \n\nThis must be possible. This would be completely platform independent, as the config tool will not know how to change a single thing, each backend tool will supply the info to the config tool which will, in turn, give it to the user.\n\nI don't understand why KDE doesn't work like this in EVERY single facet. \n\nThis is my dream way for KDE to work ...\n\nKWord needs to open a file, it calls a separate \"application\" that opens an Open \"dialog\" box. KWord, has no code in it to open the dialog box, it only knows how to call it.\nTo save, print, spellcheck, addressbook, in other words, every single common-to-another-application task, must be done externally. \n\nWhy?\n\n1. Every single application can then call a common \"dialog\" box which does the work of that particular function.\n2. Troubleshooting will be easier because these smaller applications are common, smaller and singular (only one exists).\n3. Applications become smaller in size.\n4. Smaller pieces of the puzzle need to be upgraded as changes are made.\n5. Apps will load faster as there is almost nothing to load. Every time you need something done, a new process is started which does the actual work.\n6. These separate \"apllications\", whatever you want to call them, can then be loaded at boot time and be kept running, depending on your amount of RAM, or can be switched off in KDE. In other words, to open Konqueror would just be a case of opening a standard window in which the framework is defined to call other applications to do the actual work. Why must I start the KHTML rendering engine if all I want to do is browse my home directory?\n\nThis is my dream :)\n\nIf you have a deep understanding of how the KDE framework has been developed and you can shoot this idea down in flames, please do, but please tell me at the same time why this can't be done."
    author: "KDEBoy"
  - subject: "Re: The way KDE should work"
    date: 2001-05-04
    body: "Isn't this exactly how it works now?\n\nCheck out the developer info on kde2 and tell me if\nthis is not your dream come true :-)"
    author: "sune"
  - subject: "Re: The way KDE should work"
    date: 2001-05-04
    body: "Your response begs the following question .. why does Konq. take approx. 5 seconds to load on my P11 400?\n\nUnder Win2k, IE loads in under 1 sec. the first time and is completely negligable the second time.\n\nIf it worked the way I wished, Konq. would load even faster then IE, because there would be nothing to load :)\n\nThen when you load a URL, in the background the KHTML rendering engine could start, or, the render engine could be loaded at boot time and be instantly accessible."
    author: "KDEBoy"
  - subject: "Re: The way KDE should work"
    date: 2001-05-04
    body: "> why does Konq. take approx. 5 seconds to load on my P11 400?\n\nGet an Athlon 1.33 G with a 266 FSB ;-)\n\n> Under Win2k, IE loads in under 1 sec. the first time and is completely negligable the second time.\n\nThat is incorrect. IE is already loaded. It takes that one second to draw the window. Try clicking the gear on the konq toolbar and see how fast the new window opens? \n\nThe KDE team decided not to assume the overhead to load the browser automatically for the benefit of those with less system resources. I'm sure you could patch it otherwise.\n\nHere's an idea... how many desktops do you have on W2K? Why not set up a browser desktop and just leave Konq open on it? Of my 12 desktops #3 is my default browser desktop. It takes me zero seconds to get a browser window."
    author: "Eric Laffoon"
  - subject: "Re: The way KDE should work"
    date: 2001-05-05
    body: "What you describe is indeed how KDE really works.  In fact you are describing QT and the KDE libraries almost exactly!  KHTML is a KPart and as such is loaded on demand.  All of KDE is built on a component architecture in exactly this manner, it is one of the main features of KDE2.  File open dialogs are centralized in the KDE libaries - Applications call functions in the KDE libraries that open up common dialogs for them (like the file open, save, color chooser, key config dialogs, etc).  This is what gives KDE its distinctive but consistent look and feel.\n\n\"To save, print, spellcheck, addressbook, in other words, every single common-to-another-application task, must be done externally\"\n\nAnd they are!  All the tasks you describe are already performed in the KDE libraries.  This means that all KDE applications can access them and no KDE application needs to have code to perform these tasks.\n\nNow, why is KDE not as fast as Windows?  Various reasons.  For one, Windows loads IE at startup, as some other people have already mentioned.  This means that to compare Konqueror and IE you should start Konqueror first and then open up new windows.  Unfortunately Konqueror still loses, but it is much closer than before (especially if you're using the latest CVS copy of Konqueror - many speed improvements).\n\nOptimizing is something that is still being worked on.  There are so many things to do, though, that the KDE developers can't possibly get to them all (just look at all the comments on this article!)  Certainly, though, you will continue to see speed improvements in every aspect of KDE as time goes on.  The CVS version of KDE has many speed improvements over 2.1.1).  It is being worked on!  Have faith in the KDE developers - they listen to you!"
    author: "not me"
  - subject: "Re: The way KDE should work"
    date: 2001-05-05
    body: "Thank you for your comments. \n\nI use a Riva TNT2 display card and last night I patched the kernel with the nvidia driver and loaded glx support. The net result was that Konq. now loads in almost HALF the time, and I am not even using 2.2alpha! \n\nI was too quick to judge the speed issue in the first place.\n\nAll the very best to the Konq. and KDE team."
    author: "KDEBoy"
  - subject: "Re: The way KDE should work"
    date: 2001-05-04
    body: "I'm not too involved in KDE development, but AFAIK you exactly describe the way KDE is designed. I'd even say that the whole KDE philosophy is to be modular!"
    author: "AC"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I would like to see a CD burn programe integredet withe KDE"
    author: "underground"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "That would be cool.  What would be really cool is a CD Burning program that isn't dependant on other programs.  One where all the code is built into the program.  This would help liberate the users from dependency errors, and other evil problems like that."
    author: "Steve Hunt"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Atop of the list: Speed. Optimizations.\n\nNext: Prepare more tools for distro-customizations. First and foremost, code examples: Give example KControl applets for people to prepare kcontrol-apt, the kernels' make kcontrolconfig, kcontrol-updatealternatives (Debian admin warning ;), etc. Give code examples for content producer, for 'enterprise-portals'. Or say Big Company Inc. wants to install KDE on a few billions desktop (whatever) make it easy for the sysadmins to add BCI's own few applications in the default environment. Like a phone directory, an internal instant messager, a few domain-specific applications.\n\nNext: Speed. Optimizations. (Yup.)\n\nNext: Speed. Opti... (Imagine I said it a few more times)\n\nNext: Start working on removing code duplication: For one, try to go from DCOP/MCOP to MCOP only. (AKA Speed, Optimization and Memory Footprint)\n\nAnd the wishlist from hell: The merging of KDE sessions. Example: I'm running base KDE on debian.ylavoie.com (home comp) and then pop in the McGill labs at <some address here>. I wanna access the KWord I left home open (or that I just opened using ssh, or whatever) and be able to remotely send it some messages from the desktop itself, maybe even clone the app and work on the document remotely. (While the original KWord still gets the changes)\n\nStill from the wishlist from hell: Continue X separation. Get rid of KDE-ICE, get rid of anything that's remotely X-specific. There are many projects to replace X at various levels, let's make sure KDE runs on all of them.\n\nAnd at last: Continue like you've been doing for years. The thing KDE needs most is brilliant minds. But thanx god, it already has those...\n\nYours Truly =)\nChristian"
    author: "Christian Lavoie"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I would appreciate most\n\na) decent IMAP support for Kmail\n\nb) further improvement of konqui\n\nc) speed increase, memory decrease\n\nI have the impression that since the release of KDE2 most people concentrate a bit more (too much) on eye candy, look and feel instead of core functionality. I still miss KDE2 versions of KDE1 apps that reached a high level of usability, e.g. kisocd, krabber.\n\nStefan"
    author: "Stefan"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "> a) decent IMAP support for Kmail\n\nThis is in 2.2alpha.\n\n> b) further improvement of konqui\n\nThis is also in 2.2alpha.\n\n> c) speed increase, memory decrease\n\nProbably some of this too."
    author: "Erik"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I would like to see a Ul*raEd*t clone for KDE."
    author: "me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "I would like a Multi-Edit clone. I can do it myself, but i'm not much of a cpp guy. (still waiting for Kylix opensource)"
    author: "me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "Try Kate (in kdebase cvs or KDE 2.2 later).\nNot a clone but a editor which looks a bit like ultraedit and has some of the ultra**** features ;=)"
    author: "Christoph Cullmann"
  - subject: "C*deWright"
    date: 2001-05-08
    body: "A CodeWright clone is what I'm dreaming of in Unix. Unless I have a good editor with Brief(tm) bindings I won't switch from Windows as my default dev platform."
    author: "cezarg"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "stability \u00fcber alles"
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "The problem with this poll is the embarrassing question: How is it possible to improve an already good thing? Yet, I would like to see the following:\n\n1) Improve Konqi. Make it faster, leaner, and take out some bugs (for example, the annoying one that from time to time forbids me to write with accented, non-english characters in the editboxes).\n\n2) Pay great attention to drag and drop, and copy and paste.\n\n3) Improve font and charset handling for konsole. I cannot use it right now; it refuses to print my accented characters.\n\n4) Could it be such a thing like a font installer?\n\n5) Mouse independent operation for all programs. For example, right now, I do not know how to play Klondike in KPat using only the keyboard.\n\n6) Make KDE run well in a machine with 32 MB of RAM.\n\n7) I would like better control over the Advanced Editor. It is advanced, but it does not let me change their font, nor the endline (Win/*nix/Mac), etc. I grew tired of the really tiny and unreadable font it has by default.\n\n8) Be careful with the binaries. I know you only provide the sources, but make sure, if possible, that the binary distributions comply with the directory structure and other features from the standard source distribution. I am using now KDE 2.1 under RH 6.2. I am stuck because RH chose to implement KDE in a different directory tree than the standard sources, and without RH-provided upgrades for 6.2 I am not able to upgrade to KDE 2.1.1, for example.\n\nWell KDE people. I am impressed. You have made the best desktop environment EVER and now want to improve it. Thank you for making my computing more and more useful and reliable, and for giving to the community a state-of-the-art, top-of-the-line, desktop environment."
    author: "Eduardo Sanchez"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "1) Improve Konqi. Make it faster, leaner, and take out some bugs (for example, the annoying one that from time to time forbids me to write with accented, non-english characters in the editboxes).\n\nProbably your fixed font kicking in, select a font that has your local characters..\n\n3) Improve font and charset handling for konsole. I cannot use it right now; it refuses to print my accented characters.\n\nSame thing.. Find yourself some fonts that have your accented letters, that's the only reason you might not be able to see those...\n\n4) Could it be such a thing like a font installer? KFontInst is on the way :)  Be patient...\n\n6) Make KDE run well in a machine with 32 MB of RAM. \nCompile it for yourself.. My mothers machine has only 32MB ram, and kde (after being compiled from source) works perfectly there..\n\n7) I would like better control over the Advanced Editor. It is advanced, but it does not let me change their font, nor the endline (Win/*nix/Mac), etc. I grew tired of the really tiny and unreadable font it has by default.\n\nYou should look around a bit more ;)   Press the settings menu and search, it's there (hint: highligthing, and yes, it has been moved to the settings dialog in kde 2.2 cvs)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "> 3) Improve font and charset handling for\n> konsole. I cannot use it right now; it refuses\n> to print my accented characters.\n\nI had that probem too (with \u00e5\u00e4\u00f6\u00fc\u00df). But for some reason I can write them now. I don't know exactly what caused the improvement (I upgrade lots of stuff all the time), but I guess it was the new kdelibs 2.1.2."
    author: "Erik"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "---Compile it for yourself.. My mothers machine has only 32MB ram, and kde (after being compiled from source) works perfectly there..---\n\nI find this statement absolutely incredible. This completely negates every single speed/performance/memory footprint issue that every single person has posted so far. Are the distributions really doing that bad of a job with KDE? \n\nI run Mandrake 8.0, and my system is always over 90% RAM usage with 128mb. Now you are trying to tell me that KDE works perfectly in 32mb of RAM? This is either a blatent lie, or the KDE developers must make a point of telling people to compile their own KDE to get it to work properly.\n(On a side-note, the only thing I enjoy about LM8 after using it for 3 days solid, is that everything is up to date so that I don't have to do it myself. The distribution as a whole is not production quality, and I would not suggest it in a large-scale linux deployment.) \n\nI understand that not many people are even willing to try compiling something from scratch as well as the time needed to compile KDE, as well as the download time, but then the package managers should make sure that the packages they are making are as good as they can possibly be.\n\nI read somewhere in this thread that even on a 1.4GHz processor, KDE is not instantaneous. This is completely ridiculous. \n\nI would rather buy myself one more 128mb DIMM and have the entire KDE load into RAM than have KDE respond the way it does now. That would be incredible."
    author: "Gob-smacked"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "1) Improve Konqi. Make it faster, leaner, and take out some bugs (for example, the annoying one that from time to time forbids me to write with accented, non-english characters in the editboxes).\n\nProbably your fixed font kicking in, select a font that has your local characters..\n\n3) Improve font and charset handling for konsole. I cannot use it right now; it refuses to print my accented characters.\n\nSame thing.. Find yourself some fonts that have your accented letters, that's the only reason you might not be able to see those...\n\n4) Could it be such a thing like a font installer? KFontInst is on the way :)  Be patient...\n\n6) Make KDE run well in a machine with 32 MB of RAM. \nCompile it for yourself.. My mothers machine has only 32MB ram, and kde (after being compiled from source) works perfectly there..\n\n7) I would like better control over the Advanced Editor. It is advanced, but it does not let me change their font, nor the endline (Win/*nix/Mac), etc. I grew tired of the really tiny and unreadable font it has by default.\n\nYou should look around a bit more ;)   Press the settings menu and search, it's there (hint: highligthing, and yes, it has been moved to the settings dialog in kde 2.2 cvs)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "1) Improve Konqi. Make it faster, leaner, and take out some bugs (for example, the annoying one that from time to time forbids me to write with accented, non-english characters in the editboxes).\n\nProbably your fixed font kicking in, select a font that has your local characters..\n\n3) Improve font and charset handling for konsole. I cannot use it right now; it refuses to print my accented characters.\n\nSame thing.. Find yourself some fonts that have your accented letters, that's the only reason you might not be able to see those...\n\n4) Could it be such a thing like a font installer? KFontInst is on the way :)  Be patient...\n\n6) Make KDE run well in a machine with 32 MB of RAM. \nCompile it for yourself.. My mothers machine has only 32MB ram, and kde (after being compiled from source) works perfectly there..\n\n7) I would like better control over the Advanced Editor. It is advanced, but it does not let me change their font, nor the endline (Win/*nix/Mac), etc. I grew tired of the really tiny and unreadable font it has by default.\n\nYou should look around a bit more ;)   Press the settings menu and search, it's there (hint: highligthing, and yes, it has been moved to the settings dialog in kde 2.2 cvs)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Key features"
    date: 2001-05-04
    body: "Hi all !\n\nI've been using KDE since 1.1.1 and now I'm taking active part in applications development. The applications I have chosen are the ones that I consider more useful in Windows and I feel they are missing in KDE (at least with the same level of functionality).\n\nI consider these ones key applications:\n\n1) A really fast image viewer and fully integrated with desktop (such as Windows' ACDSee)\n\n2) An Outlook-like mail calendar collaboration tool.\n\n3) A video player with support for latest codecs.\n\nI know there are projects similar to these being developed now, but I think we should concentrate our strengths in imnproving them. They are pretty usable now, but we need to make them better than the ones in the other O$ (except for the latest Windows Media Player version ... ;-P )\n\n\nApart from that, KDE has evolved to a really mature desktop, pioneer in several new technologies, and from the point of view of the programmer (as I have seen) very easy to write applications to.\n\nPlease keep up the good work. I would like to thank all te developers, translators, artists and volunteers in this fantastic and challenging project. Thank you all!!\n\n\nRegards"
    author: "PMC"
  - subject: "Re: Key features"
    date: 2001-05-04
    body: "You mean something like this?\nhttp://apps.kde.com/na/2/info/id/287"
    author: "Anonymous Coward"
  - subject: "Re: Key features"
    date: 2001-05-05
    body: "> 1) A really fast image viewer and fully integrated with desktop (such as Windows' ACDSee)\n\nFor the fast part, have a look at http://master.kde.org/~pfeiffer/kuickshow/ :)"
    author: "gis"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "1. Improve KOffice, including I/O filters (this is really essential if KDE wants to get anywhere near M$ Windoze on desktop).\n2. Improve Konqueror in the way, that it can display all pages correctly, including those using JavaScript (certain buttons used within JavaScript don't work correctly).\n3. Add IMAP support to KMail. Also, KMail does not handle my mailbox, the one I use with Pine, correctly. I about 400 emails in the inbox and KMail only shows 388 of them, some of them duplicated. The last email it shows is dated to 22th of November 2000, although there are many emails in the inbox I received after that. All this makes KMail unusable for me, so I have to stick with Pine :((.\n4. KDE does not need configuration utility. Actually, what do you mean by configuration utility at all? What is KConfig then? If you mean configuration utility for the whole system, this would really be a waste of time. One, that is not able to configure Linux system by hand (I mean, edit configuration files using vi or emacs) should not be considered Linux system administrator, IMHO.\n\nRegards,\n\n        Bojan."
    author: "Bojan"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "> 3. Add IMAP support to KMail.\n\nThis is in 2.2alpha.\n\n> 4. KDE does not need configuration utility.\n> Actually, what do you mean by configuration\n> utility at all? What is KConfig then? If you\n> mean configuration utility for the whole\n> system, this would really be a waste of time.\n> One, that is not able to configure Linux\n> system by hand (I mean, edit configuration\n> files using vi or emacs) should not be\n> considered Linux system administrator, IMHO.\n\nI can see how system configuration in Kcontrol colud be useful. I would like to see a Kcontrol as the central konfiguration place, like the Control Panel in MS Windows. This is good for average users who don't have a Linux system administrator at home.\n\nI know how to configure my kernel using xconfig, but I think it would be kool to do it from KDE!"
    author: "Erik"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Unfortunately the only possibility to vote for bugfixing (in particular \"improving functionality\") is to chose \"stability\".\n\nStability is IMHO already good. But there are many remaining bugs which prevent some KDE programs from being useful.\n\nThis is not meant as a bugreport - I know bugs.kde.org - anyhow here\u00b4s an example:\n\nI would prefer Konsole before xterm, but there are a few important programs (joe, less over ssh) which don\u00b4t work well with Konsole - so I can\u00b4t use it.\n\nAnother example is kdvi. I always have to open new Documents with xdvi first since only xdvi creates missing fonts. Otherwise kdvi leaves out text. So to be sure I always have to use xdvi.\n\nTo use bugs as an argument is always critical: Perhaps it works for you?\nBut these are only examples to make clear that improving functionality by fixing bugs is IMHO more important than adding new features.\n\n\nand don\u00b4t get me wrong: it\u00b4s great to get all these kde programs for free - but this is a wishlist, isn\u00b4t it? ;-)\n\n\nHape"
    author: "Hape Schaal"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Well,\nin konsole you can try several options (schema's/modes ... keyboard)\nalso, the joe problem got fixed for me i don't know when. before it was fixed i used 'control-R' after starting up joe to fix it.\n(btw, you can also try different export TERM=blabla settings)"
    author: "ikke"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "English Grammar checker for Kword. And it would be nice if it also functioned as a stand alone package."
    author: "M"
  - subject: "longing for koffice"
    date: 2001-05-04
    body: "To make KDE a full desktop system there surely must be a stable office package with resonable features (not necesseraly the same that M$ has...) and a lot of io filters (like latex input??).\n\nRecently I worked with StarOffice to make a A0 poster for a presentation and while doing this a missed the following thing:\nWhen I decided to change the font type and size, color, ... I always had to enter each text object, go through all the dialogs and change the attributes by hand. Very annoying work, if you have a lot of objects, which you usually have in posters or presentations.\n\nSo I thought it would be nice to be able to copy sets of attributes from one object to others. That made me aware of the fact, that I am quite often not very sure what attributes my objects really have, because they are hidden or several attributes can have the same effect. This is in contrast to latex where you always very well know, which environment you are in and which attributes your text/paragraph/picture/... has. (I really love latex!)\n\nWhat I would like to suggest for koffice apps is an attribute editor and a hierarchy editor like the one in the QT designer. With the ability to copy and paste attributes\n\nThx,\neva"
    author: "eva"
  - subject: "Re: longing for koffice"
    date: 2001-05-08
    body: "Hi, what you described here can be done simply with styles.\n\nYou create styles for each type of text, and changing the style changes the attributes of all text that uses that style.\n\nSo for instance you have a text with a piece of coding in between all the texts. This coding has to be in courier since it should be monospaced.\n\nYou go to the stylist (extra menu I think, changed recently in cvs) and create a new style, set all the settings you want for this style.\n\nThen you select the paragraph you want this style to be used on and select the style you just created. (format->styles->your_style I think also changed recently in CVS)\n\nChanging the style in the stylist later on will change all the texts that have that style in your whole document to reflect that change.\n\nThis is what we designed them for, perhaps you will find extra usage for them ;)"
    author: "Thomas Zander"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Speed, speed and once more speed. (Oh, and memory footprint). \nIMHO KDE has enough features, and by adding new visual extras KDE is becoming very much like MS - improve system look, forgetting about speed and stability.\n\nBoris"
    author: "Bor-ka"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "What I want ?\n\n''Simple'' : Make KDE use LESS memory, LESS cpu-time !!!\n\nOr, maybe, I prefer to have choice to disable some functionnalities ( Task Bar, Dock Panel, Icons on teh Desktop, and so much functionnalities, that I don't need everyday ) via a small utilitie which permit us to launch them as daemon, in order to make KDE more usable in a small configuration ...\n\nAnd maybe, thinking to make KDE using more than 1 CPU ! ( paralellisate the code please ! )\nBecause, now, we see more and more powerful computer with multi-processors, and we have so uch powerful librares for this ! ( MPI, MOSIX, IPC ) ...\n\nIt's all ( I think, some of yours prefers to say : \"\" It's enough ! \"\" ;) )\n\nPS: Great Job guys !"
    author: "RoX ..."
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Speed, speed and once more speed. (Oh, and memory footprint). \nIMHO KDE has enough features, and by adding new visual extras KDE is becoming very much like MS - improve system look, forgetting about speed and stability.\n\nBoris"
    author: "Bor-ka"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Hi,\n\n  some features i miss most :\n\n    KDE should use it's own package-system\n    the reason for this is:\n      1) be *independent* from different\n         Linux flafours\n      2) give the user an interface to the\n         local used package-system (rpm)\n      3) the internal dependencies are better\n         known by kde developers\n      4) create it's own auto-compilation\n         toolkit. today i download kdb 1.2.1\n         to compile it with SuSE7.1 nothing\n\t works. I used (and compiled) it with\n\t KDE 1 on Suse 6.* it was very good but\n         now i have nothing KDE2 but no kdbg.\n         Currently i download Insight5.0\n         It's an user-interface to gdb5.0 but\n         the download is 14 MB."
    author: "Andreas Otto"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "Why on earth would you want to create a situation where a user has to use one installer to install a KDE application, and another to install a non-KDE application?\n\nWhat does that achieve except confusion?"
    author: "Stuart Herring"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Hi,\n\n the confusion is made by the different Linux\n flavours with inoperable package-system(s)\n\n To use a KDE package system would be a way out\n not in confusion.\n\n KDE as the strongest single application environ\n ment and has the power to do the job. They can\n even use \"rpm\" which is bullet proof on Linux\n but manage there own package database under\n /opt/kde????\n\n => the goal for this is ...\n\n  be at most as possible independent of the\n  underlying Linux. You can call it\n\n     \"Enemy Host Technology\"\n\n  or never trust anything which you can't \n  control\n\n\nmfg\n\n  aotto :)"
    author: "Andreas Otto"
  - subject: "Browse for printers and servers"
    date: 2001-05-04
    body: "I have not tested 2.1 yet, but one thing I missed in 2.0 was the possibility to mount a printer, or remote server directory by browsing among available.\n\nSomething like add printer, and map network drive in MS-Windows."
    author: "Johan"
  - subject: "Re: Browse for printers and servers"
    date: 2001-05-06
    body: "Have you tried CUPS? It was soo easy to add my printer! I could even see one of my neighbour's printer, her user name and job queue. I tried to print on it but I don't think the printing was completed. I couldn't check because I don't know which neighbour it was. There are 280 apartments in this building.\n\nPrinting on my own printer worked at least as well as with the old printing system I had, as far as I could tell from the test page.\n\nI first tried version 1.1.6 but it messed up my printing system so I couldn't print. Then they released 1.1.7 and it worked!"
    author: "Erik"
  - subject: "Re: Browse for printers and servers"
    date: 2001-05-07
    body: "KDE supports now several printing architectures directly, amongst them CUPS: You can create your printers directly with KConfig! But as for now, I'm still using Kups (have a look at Sourceforge) as it provides even better configurability of the printers."
    author: "Johann Lermer"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "The following things come to my mind:\n\n   - advance in Kvim, a Vim editor-part\n   - a port of gimp to kde/qt, including a       rework of the userinterface\n   - first class javascripting\n   - a replacement of motif in java-awt\n\n\n Regards\n Micha."
    author: "Michael Schulz"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "\"a port of gimp to kde/qt, including a rework of the userinterface\"\n\nYou mean like Krayon?  http://www.koffice.org/krayon/\n\nIts not exactly a port, but it seems like if it matures, it could be all you want in an image manipulation program.  Lots of thought has been going into the user interface.  Disregard the screenshot on the website, I think it's from the ancient KDE 1 version!\n\n\"first class javascripting\"\n\nEvery day Konqueror's Javascript improves.  My 2.2alpha1 Konqueror runs several sites that don't work in 2.1.1.  It is being actively worked on right now!\n\n\"advance in Kvim, a Vim editor-part\"\n\nWow, tons of people want this!  And the funny thing is, no one wants a KEmacs part!  Do all KDE users like Vim or something?"
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "Krayon is no GIMP, and will not be for a while.  A Qt port of GIMP would be easy if it used GNOME canvas(which would be a bit of work), but after that your 90% done."
    author: "AC"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "But what would be the point?\n\nIt would be a lot of effort to produce something that already exists...\n\nIt's not like you can't use the GIMP right now...\n\nAnd Krayon will fit in with the look and feel philosophy of KDE more than the GIMP ever will.\n\nPorting a GTK application the size of the Gimp to QT/KDE would be a huge task, and would probably end up taking longer to reach a similar level of functionality and stability to original GIMP than for Krayon to reach those levels.\n\nAnd that's assuming that there's even anyone interesting in taking on such a task.."
    author: "Stuart Herring"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2004-10-12
    body: "I would love to see an advanced media player that acts and looks like windows media player 10.  It should only use mplayer, not xine or xmms or any crap that doesn't work well with all distros.  It should use mplayer as it is the only media player for linux that supports almost anything including all microsoft media formats right out of the box.  However, it should not be a \"front end\" for mplayer.  It should use mplayer code embeded in the application so it will work with any distro running kde without any compilation. "
    author: "sleepkreep"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "In my opinion, the most important \"feature\" for kde is speed. On my box (AMD K6/2 350) even the opening of a simple kwrite window takes about 3 seconds. Starting Konqueror (browser-mode) takes about 4-5 seconds. Even sluggy Netscape 4.7 starts up slightly faster. \n\nEverytime I try to convince some members of the Windows - using communtiy of the many advantages of linux their first complaint is \"oh it's so much slower than my windows\". And indeed, on the same box und Windows the starting of Internet Explorer takes less than a second. \n\nThe startup of Kde (after logging in) takes about as long as the whole startup of Windows. \n\nAnother important feature I'd like to see added is a much improved support for DOM, CSS and Javascript in khtml, especially the possibility to manipulate the page (DOM) dynamically via Javascript after it has been loaded. This is usually called DHTML and is still very incomplete, at least in the KDE 2.1.1 - version I last tested. I believe this to be very important for the future of the internet, especially for the fight against non-standard, semi-open technologies like Flash etc. \n\nOther cool features for khtml/Konqueror would be:\n- support for TRUE alpha shading with png - graphics\n- support for vector graphics on html pages (SVG)\n- Python scripting additional to Javascript!!! Javascript is a really ugly language and I heard about a Python scripting ability to be implemented in Mozilla."
    author: "Benno"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I would like a panel applet that would take over the display of the application menu and display it wherever I put it on a panel.  With this, I could duplicate the MacOS menubar's mixture of application and system features, not possible with the existing \"in the style of MacOS\" feature."
    author: "Mike Ladwig"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Better KDE/GNOME interop!\n\nI want, for example, Konqueror to automatically notice that it's running under GNOME and use my selected GNOME theme by default. (oh, and I want the \"legacy theme importer\" to not crash on my choice of theme, and I'd like the name \"legacy\" removed).\n\nI want embedding of KParts in Bonobo, and vice versa. I want to be able to embed a Gnumeric spreadsheet into KWord and then use KChart to graph it.\n\nI want drag'n'drop and cut'n'paste to be seamless between the two environments. I want prefs I set in one to be honored by the other, where applicable.\n\nIn short, I DON'T WANT TO HAVE TO CARE which desktop I'm running. Right now my desktop is GNOME, but I'm increasingly using KDE apps and it's frustrating that they don't fit in. And I'm sure the same goes for KDE users who want to use GNOME apps. Getting the interoperability better should be priority #1 in my humble opinion.\n\nStuart."
    author: "Stuart Ballard"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "well, perhaps the name legacy should go as it's kinda inappropriate now, but I think it was originally there because the importer was a tool for importing KDE1 themes. So it wasn't a slur or anything, promise :-)"
    author: "Kevin Puetz"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Allow simple applications (for config) to be easily embedded into the control center, and then distributions can take it from there... support for linuxconf modules in the control center would be nice too, but these are low priorities.\n\nMy biggest concern is the capabilities of Konqueror (javascript is a serious issue), KOffice, and the slow loading time of KDE when you first log in.  I run on a 750MHz Duron with 256mb of memory and 512mb of swap, and still have these problems.\n\nThose are my biggest concerns.  A good Office environment is vital, but speed (and stability) should come first."
    author: "dingodonkey"
  - subject: "Network transparency"
    date: 2001-05-04
    body: "The most important thinks has already been written.\n\nI have only a small wish for next releases:\n\nUpload changed remote file on action \"Save\" not \"Quit\"....\n\nCMIIAW"
    author: "VK"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Hallo Users & Hackers!\n\nIt's very easily to say what is that kde most needed feature: The stability followd by the speed of the hole thing.\n\nRecent the KDE is very slow and have an high mimeory consumption; 64 MByte aren't enough for that (!). The hardware requirements appear to be higher as from Windoze 2000!\n\nA lot of the shipped apps show an unstable behavior. KOffice is THE extrem cause: Start an KOffice app like KWord and try to work something. You don't need more as 15 min. to the next bomb of kcrashguard appears.\n\nThe problems of konqui with JavaScript are not so important if the complains someone concern ...\n\nI started with KDE 1.0 and to 1.2 the things appeared more stable as the KDE 2.1 today. Features are the one thing, but imcomplete without enough stability and speed. \n\nMagnar Hirschberger"
    author: "Magnar Hirschberger"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "> I started with KDE 1.0 and to 1.2 the things\n> appeared more stable as the KDE 2.1 today.\n\nSo you are using 2.1 and complain about stability? Try latest stable, 2.1.2!"
    author: "Erik"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Faster login and logout, please."
    author: "David Jarvie"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Hello,\nI'm using Kde more and more but I still have to go back to the Microsoft World to do must of my serious work...\n\nFrom my point of view : an historian writing articles and having a years of notes kept in files what is keeping me from making the great switch to Kde and Linux is not only a good wordprocessor but also a full-text indexer like FastIndex for Windows that make possible to find quickly a file from his content.\nMy wish therefore is a program that would index in the background my text files (ASCII, HTML, XML, Latex, LyX, Koffice) and a plug-in for Konqueror and Kword to search for the files. An even more powerfull mechanism would be to add in the index metadata about these files, like owner, long name, keywords, annotations etc...\nIt seems that Eazel with Medusa has something more or less similar but I have not tryed it.\n\nIf this KfastIndex module is developped that would be another step to create a better alternative to Windows and Lotus Notes.\n\nCheers,\nCharles"
    author: "Charles de Miramon"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "As many have said, putting a KDE face on LinuxConf is nothing I'd like. What I <I>would</I> like to see though, is a KDE configurator for <B>X</B>.\n\nEditing XFConfig and XFConfig-4 is a daunting task for any newbie or GUI enthusiast - yet so necessary at times. I'd donate a chocolate cookie or two the person who creates a KDE configurator for those two files.\n\nThe main features would be to change Screen Resolution, Color Depth and Monitor type. In the advanced section there would be checkboxes for Xinerama, DRI and GLX. Ahh, nice..."
    author: "Matt"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Developing the KOffice Suite should be the top priority, especially developing interoperability with MS Office products and MS Word specifically. This would be the most significant improvement to get Linux/KDE accepted on the desktop.\n\nHowever, here are some close seconds that apply at least in my case:\n\nKonqueror is slow, and memory hungry. This is especially true if you try to open multiple windows.  Otherwise, I really like what has been done here.\n\nKDE in general is slow and memory hungry.  More so than the current Gnome project (but not by much) and more than the MS products.\n\n---------\n\nI feel that there is too much attention in current GUIs to make them customizable.  In many cases, all this customization confuses users.  I think a very simple yet useful and consistent GUI is better.  This is something I appreciated about BeOS.\n\nAll in all I think the KDE team is producing impressive results, I hope you can keep the momentum going.\n\nJames Stewart"
    author: "James Stewart"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "* Speedier with smaller memory footprint. Faster startup of Konqueror (as a filemanager) \n* The bug with rectangles on a NVidia driver should be fixed once and for all. \n* KOffice should be 99.9 % compatible with MS Office. No vbscript though. \n* Much better Icons. Really now, the icons (expecially the large ones) are horrible.. Icons from the Slick theme (see kde.themes.org) are excelent \n*  Closing a Window if pressing Ctrl + Left Click on the Taskbar \n*  Huge efforts should be put in Noatun and Kaboodle \n* KVirc instead of KSirc, Kpass in kdeutils \n* Maybe a unbloated Active Desktop like the one in Windoze ? \n*  smarter KDM and the posibility to run a program before KDM \n* Something similar with \"View as Music\" in Nautilus would be cool. \n* I don't think that a KDE Installer should be made. The efforts should be put in KPackage and a install option for a *.rpm, *.deb should be put in konqui. \n*  KMail should be optimised for large mail-directories. \n* Get Mosfet to work on the UI Engine again ! ;) .. btw. MegaGradient style doens't work very well"
    author: "linuxdewd"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "About the icons...  I think KDE has wonderful icons, but the smaller ones need some work here and there.  I think the slick icons are nice with a dark theme like that, but not for normal use for the average user.  This is just my opinion, of course.  I do, however, like the icons from photon (<a href=\"http://kde.themes.org/php/pic.phtml?src=themes/kde/shots/943815508.jpg\">picture</a>, <a href=\"http://kde.themes.org/themes.phtml?action=search&themetxt=photon&submit=Search&odby=download&disptype=norm&numthemes=10&showmod=off\">link</a>).  Still, they are not as good as the KDE icons (although the icon used for the K menu <i>is</i> pretty catchy).\n\nYou're right, a KDE Installer should not be a high priority.  If somebody feels like contributing one, fine, but don't go out of the way for it.  <i>Most</i> KDE users that I know of are using KDE from an out-of-the-box distro, and this is handled for them."
    author: "dingodonkey"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "About the icons...  I think KDE has wonderful icons, but the smaller ones need some work here and there.  I think the slick icons are nice with a dark theme like that, but not for normal use for the average user.  This is just my opinion, of course.  I do, however, like the icons from photon (<a href=\"http://kde.themes.org/php/pic.phtml?src=themes/kde/shots/943815508.jpg\">picture</a>, <a href=\"http://kde.themes.org/themes.phtml?action=search&themetxt=photon&submit=Search&odby=download&disptype=norm&numthemes=10&showmod=off\">link</a>).  Still, they are not as good as the KDE icons (although the icon used for the K menu <i>is</i> pretty catchy).\n\nYou're right, a KDE Installer should not be a high priority.  If somebody feels like contributing one, fine, but don't go out of the way for it.  <i>Most</i> KDE users that I know of are using KDE from an out-of-the-box distro, and this is handled for them."
    author: "dingodonkey"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "MegaGradient is still under development. If there is a bug that doesn't show up on http://www.mosfet.org/megagradient.html let me know! Most of the drawing bugs are when you first change to the style or change the color scheme and shouldn't appear in normal use (but have to be fixed! I'm working on it - but want to get extended color schemes into CVS). I hope to have all this stuff working well way before 2.2 is released."
    author: "Mosfet"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: ">Much better Icons. Really now, the icons (expecially the large ones) are horrible..<\n\nYou're welcome to create some new icons. I think that default kde icons are great, especially large ones 48x48. Some improvement of some actions icons is necessary\n(editcut, for example), but I think that only Tackat works on kde icons, and creating nice and usable icons is extremly hard task. Try to create an icon set, and you will see how hard it is."
    author: "Antia"
  - subject: "KMail, KNode, KOffice"
    date: 2001-05-04
    body: "Here is my list -- mostly just refinements on what exists.\n\nKMail: distribution lists, IMAP support, Outlook (not \"Express\") .PST import, header-only download, selective e-mail download.\n\nKNode: Multi-part MIME support.  I've seen this in other, GPL, KDE news readers.  Can't that code be incorporated?\n\nKOffice: Correct view/import/export of MS Word & Excel 95/97/2000 file formats -- including embedded MS DRAW images.\n\n -Charles"
    author: "Charles Hill"
  - subject: "Re: KMail, KNode, KOffice"
    date: 2001-05-06
    body: "Try Pan (sorry though it's GNOME) at http://pan.rebelbase.com."
    author: "LukeyBoy"
  - subject: "Good connectivity with PDAs"
    date: 2001-05-04
    body: "Yes you can backup your Palm data with kpilot or (even better) jpilot, but can you use those email addresses with kmail? Not yet."
    author: "Stephen Boulet"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I first want to say... KDE 2.1.1 Rocks. Very usable.\n\nOne feature that comes to mind would be have Konqui a little more intergrated with samba.\nIt would be nice to have the ability to right click on a local folder and create a share, or ability to browse and mount one.\n\nAlso I have to admit the Cut and Past thing mentioned above also iritates me. (Copying everything I select)\n\nThanks a Million for such a wonderful piece of software!\n\nKreg Steppe"
    author: "Kreg Steppe"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Configuration utilities are definitely what is most required.  Display configuration tools (change resolution and colour depth), Network and Samba tools and better support for Samba in the File Manager, Printer Tools (easy printer setup and GUI print job cancelling) etc.  Maybe you can get some of this stuff from Corel since they are getting out of the Linux business and they had started to deliver these things.  The other projects like KOffice and web stuff are nice to have but are not as important since options like StarOffice and Mozilla exist."
    author: "John Yorke"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I think that KDE is the best GUI for UNIX!\n\nBut, the Qt library is very very very....  \"memory eater\", and KDE 2 runs slowly in cheap computers.\n\ni would like a KDE more fast and lite."
    author: "Carlos Marcello Dias Fernandes"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "well, poll should be equiped of check box, not radio.\n\nits too difficult for me to choose between all of these very importtant thing.\n\nbut before doing any graphical KDE installer, maybe a KDEDB could be create to keep file information. to which package this file belong, file version number, etc.\n\nafter that, it would be very more easy to build graphical tool for installing/upgrading/erasing any KDE package.\n\ngive me feedback about the idea ...\nthanks ! \nsomekool"
    author: "somekool"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "A logout status would be nice, a window that informs me on what is going on. Something that is in two stages. \n\nStep 1) This is where application ask if I want to save or cancel the logout.\nStep 2) This is where the termination is going on. When the system is reaching this stage the logout process is non-stoppable and whatever I as a user is trying to do the system will logout. Locking the keyboard or something too my be a good idea. If I hand over the system to ?Mr. Hacker?, he wont be enabled to do anyting that can harm the system or my user without a new login. \n\nToday I feel that I have to baby-sit the logout process and don't know when I can leave the computer or not.\n\nThank for listen to my comment on the subject."
    author: "Martin Juhlin"
  - subject: "Not removing things that worked in 1.1"
    date: 2001-05-04
    body: "My current pet peeve is double clicking on the title-bar. KDE 1.1 has 5 options for what this does. KDE 2.x has 2.\n\nI've been double-clicking on title bars to minimize them for years now (first with Motif, then KDE 1). Now it no longer works and it confuses the hell out of me.\n\nAlso, why can't we move minimize/maximize/whatever buttons around anymore? This used to be a neat feature of 1.1, but was removed in 2.x\n\nI find myself in agreement with an earlier poster who complained about the Windows-ization of KDE. Lots of us are Unix-diehards too.\n\nOh yeah, after that a KDE front-end to linuxconf and other things would be great. Work with Mandrake. Linux needs a \"Control-panel\" equivalent that can do everything but is consistent across all the different apps."
    author: "Eric Vaandering"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Hi! \n\n1) Don't write letters -  vote on the poll-page.\n2) Need a help to choose a feature? Look at the  page of results.\n\n\nI (probably among others) suggested this poll and was inspired by bugs.kde.org.\n\nWish you a nice weekend. Regards Martin"
    author: "Martin from Heilbronn/Germany"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "The KDE feature I would like most is to have the configuration utility configure the whole computer system as much as possible.\n\nThe applet I am most insterested in is the system monitor.  The KDE one try crashes at startup (running KDE 2.1.1 on MDK 7.2).  Thus I have to use the GNOME one.  I run KDE applications in a GNOME desktop.\n\nThe application I am most interested in is an Outlook clone, like Evolution.  I want Kmail, Korganizer, and the addressbook to work with each other.  Right now their integration is minimal.\n\nCongratulations and keep up the great work!!"
    author: "Salvatore Enrico Indiogine"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: ">> I want Kmail, Korganizer, and the addressbook to work with each other. \n\nI suggest you look in the latest issue of Kernel Cousin KDE ( http://kt.zork.net/kde/kde20010427_8.html ) - this issue seems to be currently under discussion."
    author: "Matt"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I would like to see some inprovements to dekktop drag-and-drop behavior.  Look at the functionality provided by \"dragtext\" on os/2 desktop ...\n\nhttp://e-vertise.com/dragtext/\n\nIt's hard to desribe how many places grag-and-drop really works for you when you use this.  For example:\n\n1.  drag text from a window and drop it on the desktop.  A text file is created witt the text in it.  \n\n2.  drag some more text and drop it on the text file - it will be appended\n\n3.  just drop that same text on the printer and it squirts right out the printer."
    author: "Mike Johnshoy"
  - subject: "Konsole suggestions"
    date: 2001-05-04
    body: "1. Right now, each konsole window can run several shell sessions, accessible through the tabs at the bottom. It should be nice if it where possible to \"detach\" one of these sessions into a new window (kind of like the Emacs \"new frame\" command). And maybe later reattach it.\n\n2. A keyboard shortcut to open a new session in a given konsole. I know this would reduce the key combinations passed on to the shell session. So my suggestion is as follows: Shift-Left and Shift-Right should work as usual when the current konsole session has peers on both left and right. But if I press Shift-Left in the leftmost konsole session, it should open a new session instead of wrapping around the session list. Similar for Shift-right.\n\n3. A konsole schema editor. Right now, I have to edit text files to create my own konsole schema.\n\n4. Better handling of session titles: each konsole session should remember the title it had before it got switched away. I have a bash PROMPT_COMMAND that sets the title of the xterm to the current directory. Every time I switch to another session in the same konsole and then back, the title is gone, replaced with the useless text \"Konsole\". Also, maybe the buttons at the bottom (the list of sessions) should reflect these titles.\n\n5. When I right click on a session button (from the list of sessions at the bottom), I should get either the session menu (from the menubar) or the signals menu.\n\n6. I would be really cool if I could search through the text of a session. This should not be too hard to implement, since the session screen history is already stored somewhere.\n\nThat's it for now. As I think of more, I will let you guys know :-)"
    author: "warkda rrior"
  - subject: "Re: Konsole suggestions"
    date: 2001-05-06
    body: ">3. A konsole schema editor. Right now, I have to edit text files to create my own konsole schema.\n\nDone, it is on CVS for KDE 2.2"
    author: "Andrea Rizzi"
  - subject: "Re: Konsole suggestions"
    date: 2005-07-26
    body: "Where is it? I can't find it anywhere... ><"
    author: "Susan Calvin"
  - subject: "Re: Konsole suggestions"
    date: 2005-07-26
    body: "\"Settings/Configure Konsole...\", tab \"Schema\""
    author: "Anonymous"
  - subject: "Re: Konsole suggestions"
    date: 2007-05-14
    body: "How about a schema editor that actually works?"
    author: "Oops"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "I would love to see Double-Click preferences.\n\nExample:\n\nInactive Window:\nSingle click in window -> Activate and pass click\nDouble click in window -> Active, bring to front\n\nActive Window:\nSingle click in window -> Pass click.\nDouble click in window -> Bring to front\n\nNot sure how difficult to implement, but I loved this feature on the Amiga."
    author: "Kenton Groombridge"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "KWord that imports MSWord documents.\n\nAfter all the repeated requests and the shear volume of them at that, I am still amazed that I heard this was mentioned on the mailing list from a developer that:\n\n \"If we want this feature we should use abiword\".\n\nIMHO this absolutely misses the mark of any real committment to provide a viable alternative to the other platform. Much less take it seriously.\n\nPlease, I know this has been a frustrating issue. But please write me that this was just a knee-jerk response brought on by the heat-of-the-moment when the sometimes apparent lack of empathy from the users sometimes seems to overwhelm the patience of the incredibly hard-working development community.\n\nPlease tell me that this has *not* become actual policy. Because again, IMHO, this would really be missing the mark in terms of strategy.\n\nJust my .02 cents.\n\nSteven"
    author: "Steven"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "\"Please tell me that this has *not* become actual policy.\"\n\nIt hasn't.  That would just be silly!  MS Word filters are in the works.  Expect to see some in the next release of KOffice and more later."
    author: "not me"
  - subject: "KMail mostly."
    date: 2001-05-04
    body: "KMail is the KDE app I use the most. I switched over from Netscape about 2 months ago. I really like it but there are some things I'd like to see added.\n\n1. IMAP support. I know, it's being worked on, and should be here soon. It caused me to delay switching for 3 months though.\n2. Maildir folders. This allows multiple clients to access the mail messages and prevents corruption of the mail store.\n3. Scoring/ranking/killfiles. I want to have important messages (like from my boss) always appear at the top of the list and spam to appear at the bottom, if at all. This should be a part of the filtering.\n4. More configuration of the info displayed in the messages list. The columns should be able to be hidden/shown, rearranged. The Also sorting by multiple criteria.\n5. Better address book. The current ones (all of them) are almost unusable. Should allow import/export/sharing with Netscape and other address books.\n6. LDAP support for global address books.\n7. Better right-click context menus. Why can't I set the message status from the context menu?\n8. Choice of program to use to launch URLs.\n\nI also use KPresenter and KWord some. They are pretty good (I like them a lot more than Star Office) but need some work on Microsoft Office import/export and keyboard usaeg.\n\nOne other thing I would like to see is more general look-and-feel configuration. All themes should have the option of where to place the scroll arrows. I should be able to configure how the file browser looks and acts (what view, directories separate from files, etc.). I should be able to decide whether triple-click-and drag lets me select complete lines or move text."
    author: "Craig Buchek"
  - subject: "maildir folders in kmail"
    date: 2001-05-07
    body: "Please, please. I use mutt normally, but using kmail once in a while would be nice and no, I am not going to use mbox mailboxes."
    author: "Moritz Moeller-Herrmann"
  - subject: "Child panels that autohide"
    date: 2001-05-04
    body: "What I want is very simple.  I want child panels in kicker to be able to dock somewhere and autohide.  The main panel isn't big enough and adding a child panel takes up too much screen space."
    author: "Whitney"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "The most important KDE feature (IMHO of course) is Koffice. The most important part of Koffice, in turn, is <i>interoperability</i> with the corresponding Microsoft packages -- Word, Excel and so on. Those of us who live in hostile, i.e Microsoft dominated, environments cannot operate at all unless we can read word documents. KDE itself has all of (if not more than) the features I need -- it is the inability to communicate with colleagues and the university administration that is hurting me most.  While there are stopgap measures I can take, these measures are not totally satisfactory.\n<p>\nOh, yes, and it would be very nice if Kmail would support IMAP. I have to use Netscape messenger to read my mail."
    author: "Juan Rivero"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "> Oh, yes, and it would be very nice if Kmail\n> would support IMAP. I have to use Netscape\n> messenger to read my mail.\n\nThis is availible in kde 2.2alpha1."
    author: "Erik"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "Konqueror should be the top priority. It should\nbe the best browser on any platform! Please don't\nmake me use Netscape. Konqueror is my default\nbrowser but there are some things it still\ndoesn't do. My daughter uses Mozilla now and I\nnoticed that Galeon works well on many sites. I\nhate to startup netscape because I know there is\na good chance it will die and take down my system\nwith it. You can't call that production code.\n\nKoffice is also a high priority but I can use\nabiword, gnumeric or even staroffice. Long term\nlinux won't win big on the desktop without the\nfunctionality of M$office but most people don't\nuse 90% of it's features. Don't make Koffice as\nslow to load as staroffice.\n\nYou should limit config tools mostly to KDE\nconfig. \n\nThnks for all your work. KDE is already great."
    author: "Dan Clayton"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "*shurd* star-office... Do you have all day ?"
    author: "Sam Andersson"
  - subject: "Simplify, simplify, simplify"
    date: 2001-05-04
    body: "Let the user chose the level of complexity of the interface. Use a simple interface with not so much features where mre features can be activated. SOmehow like Eazel has done it for Nautilus. Look at the Finder of MacOS X. Just one button for back is enough to navigate!"
    author: "Slartibartfas"
  - subject: "Re: Simplify, simplify, simplify"
    date: 2004-08-30
    body: "test"
    author: "YEUNG Pui-yi"
  - subject: "Re: Configuration Utilities"
    date: 2001-05-04
    body: "Regarding the config tools, I think the implicit problem is that the tools that we have don't provide much.  The two big problems are:\n\n1) They are a one-to-one wrapper for Unix config files, which are often 10 times more powerful/hard to use than we really need.\n\nFor instance, on 1) above, there are several files for networking.  One is simply called hostname and holds... the hostname.  But wait!  The docs explain it may not really be your hostname.  (-:  Any tool that simply provides a mirror of this \"mess\" is no better.\n\n2)  The tools often get confused or cause confusion when the user want to change the files by hand.\n\nOn 2) this is a hard problem, and I have never seen a reasonable solution on Linux or a commercial product like SGIs admin tools.\n\nI think deeper still, the Unix start-up scripts are archaic and difficult.  The run levels don't even make sense and most daemons/servers are started the same way, but still require special batch files anyway.  All start-up config tools try to wrap this mess.  I would LOVE for there to be an alternative to Unix start-up scripts.  Just replace them!\n\nToo bad we can't also simplify the basic servers that are hard to config (ftp, http).  It would be nice to start a movement to provide a simpler interface to these, then the config tools would fall into place!"
    author: "Charlie"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-04
    body: "In the logout dialog I would like to have these options: \n\n1. Halt\n2. Reboot\n3. Just plain logout to KDM\n\nI've seen this a long time ago in GNOME."
    author: "Anders"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "> 1. Halt\n> 2. Reboot\n> 3. Just plain logout to KDM\n\nYes, I would like that too."
    author: "Erik"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "To me, top priority should be given to having a printing infrastructure (this includes the UI portion of it letting mere mortals to chose between landscape/portrait, scaling, pages-per-sheet, etc.) and font management system *at least* on par with what MacOS & M$/Windows can do. People need to print stuff and they need for it to look good, you know!\n\nAnd to be \"on par\" does not mean to be \"just like\", it means to be as functional, as capable to produce the same kind of results your typical M$/Word-using corporate report writing systems architect can produce. Not to troll, but there are capabilities commercial OS users usually take for granted that KDE users still don't have -- easily accessible (sp?). Let's hope these glaring missing features will be addressed in KDE version 2.2, because it will probably win a lot of brownie points with many potential users."
    author: "Bruno Majewski"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "\"top priority should be given to having a printing infrastructure\"\n\nWell then you'll be happy to know that one is in the works.  It is based on QTCUPS and it is in KDE 2.2alpha1.  Also planned is a font manager to install, manage, and remove fonts for all font-using applications (X, StarOffice, GhostScript, etc) at once."
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Thanks for the info, though I already knew work was already done in these areas. The question is, though, is how much ressources (sp?) are devoted to the printing infrastructure, to make it a top-notch one, something that doesn't pale in comparison with what is found in the leading commercial OSes?\n\nAs for the font manager, AFAIK, it is not even a control panel (er, control centre?) item yet and I don't know if it will make it in KDE 2.2. I truly wish it will, because it would simplify everyone's life tremendously.\n\nMy point was \"make sure both make it into 2.2, and in fully-featured state\", not \"is there...?\"."
    author: "Bruno Majewski"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Ah.  Well, a lot of work has been going into the printing architecture.  Even though it is only being developed by one guy, it is coming along quite nicely.  Here's a screenshot of the control center module (attachment at bottom).  A lot of stuff is greyed out because I have never taken the time to set up my printer under Debian, but you can get the basic idea."
    author: "not me"
  - subject: "configurable keyboard (internationalisation)"
    date: 2001-05-05
    body: "It would be a great improvement in writing texts with non-Latin1-letters to have a configurable keyboard. For example for classical greek texts (or biblical) you need greek letters with several different accents which don't appear in modern greek, you can find all of them in the unicode-block \"greek extended\", but how can you type them? (Of course you can choose the way \"insert special character\", but that's really not a good way if you have to do this at an average rate of one or two for each word!!)\n\nA good feature wood be a \"visual keyboard\", as it is called I think in MS-Word2000, where you can see the foreign keyboard on the screen, so you always know where the letters are on your non-virtual keyboard. I think it is even possible to type the letters by clicking on them on the visual keyboard.\n\nI think there is a great need for such a tool, especially in linguistics, where texts in living or dead languages such as hebrew, classical greek, sanskrit, georgian, aramaic  etc. are examined. Just be inspired by the names of the unicode-blocks!\nThe invention of unicode is already a great improvement for this, but a smart keyboard utility would be perfect!"
    author: "Dirk Heimann"
  - subject: "configurable keyboard (internationalisation)"
    date: 2001-05-05
    body: "It would be a great improvement in writing texts with non-Latin1-letters to have a configurable keyboard. For example for classical greek texts (or biblical) you need greek letters with several different accents which don't appear in modern greek, you can find all of them in the unicode-block \"greek extended\", but how can you type them? (Of course you can choose the way \"insert special character\", but that's really not a good way if you have to do this at an average rate of one or two for each word!!)\n\nA good feature wood be a \"visual keyboard\", as it is called I think in MS-Word2000, where you can see the foreign keyboard on the screen, so you always know where the letters are on your non-virtual keyboard. I think it is even possible to type the letters by clicking on them on the visual keyboard.\n\nI think there is a great need for such a tool, especially in linguistics, where texts in living or dead languages such as hebrew, classical greek, sanskrit, georgian, aramaic  etc. are examined. Just be inspired by the names of the unicode-blocks!\nThe invention of unicode is already a great improvement for this, but a smart keyboard utility would be perfect!"
    author: "Dirk Heimann"
  - subject: "Re: configurable keyboard (internationalisation)"
    date: 2001-05-05
    body: "Your \"virtual keyboard\" sounds very much like KeyCaps on MacOS -- \"Classic\".  I've always found it to be a Godsend when you're not sure how to generate non-english characters and a glaring omission in WinXX (like if \"character map\" was as functional. Yeah, right)."
    author: "Bruno Majewski"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "Yes, Speed, Konqueror improvement, Configuration utilities, and KOffice are a must for KDE to be the best Desktop Environment. \n\nHere, I would request regarding Configuration utilities. We can have the sources from:\n\n1. LinuxConf (use the concept not the dialogs)\n2. DrakConf (for X and other system configurations)\n3. Other configurations tools/utilities (like from corel linux, redhat's and suse's)\n\nI want the sources and the concept to be used in KDE's configuration utitlities, and not the same old bad interface of some good but weird looking utilities like LinuxConf. Thanks.\n\nYours truly,\nRizwaan."
    author: "Asif Ali Rizwaan"
  - subject: "More Tools in Controll-Center"
    date: 2001-05-05
    body: "I would like to see more system-tools integratet in the ControlCenter. E.g. UserManager or the VInit Control tool.\nAlso some emprovments of existing tools should be done. I am missing a feature to configer my wheelmouse (it still does not work unter KDE 2.1.1 / XFree4.02) and a good font manager which is as easy to use as the font folder unter windows (copy/paste/drag'n drop/delete fonts). Preview function of non installed font's would also be nice.\nI'm missing a tool for configuring the keyboard, too. I always become confused when the numerical block does different things unter Windows and Linux: Try the following. Switch Numlock off, put the cursor in the middle of a text line, hold the shift key and press \"End\" in the nummerical block. Try it under Windows and Linux.\n\nok, that's all for tonight.\n\nAndreas"
    author: "Andreas"
  - subject: "Great Speed tips for Linux users:"
    date: 2001-05-05
    body: "I read a _great_ posting on Slashdot that really helped my system.  Unfortunately, it's only good for Linux users.\n\nHere's a expert (sorry, no link:)\n\n\"I found if you change the /usr/src/linux/include/asm-i386/param.h file to #define HZ 1000 rather than #define HZ 100 everything GUI was _much_ _much_ faster. The GIMP seems to load in literally 1/5 the time.\"\n\n(Blah blah, recompile the kernel with this option etc.)\n\nI'll second his notion!  This trick worked great!  Oh, the other tip is to renice X to -10 and should work on any Unix system, rather than just Linux.\n\nBoth of these tips are meant for workstations only.  Apparantly changing the Hz on a server slows down the back-end processes.\n\nOh, and despite claims to the contrary, anything I've used kernel wise from 2.2.x up to and including 2.4.4 has set HZ to a default of 100.\n\nAnyway, that's my tip.  Of course any kernel mods must be taken with caution, so if you don't feel comfortable attempting it, don't.\n\n\nCheers,\n\nBen"
    author: "Ben Hall"
  - subject: "I really would like to have..."
    date: 2001-05-06
    body: "that link!!\n\nOn a sidenote, a few weeks (or is months allready ;-) I read about a problem with GNU malloc in the KDE lists. A guy used another malloc and found _many_ memory footprint implovements. Alltought he said to contact the GNU guys about it, I have since then never heard about it since."
    author: "pasha"
  - subject: "Re: Great Speed tips for Linux users:"
    date: 2001-05-06
    body: "If you change HZ and your system get real busy you'll do what's called trashing, which is that the system will be so busy keeping up with the task that none of the actual task will get done."
    author: "ac"
  - subject: "Re: Great Speed tips for Linux users:"
    date: 2001-05-06
    body: "If you change HZ and your system get real busy you'll do what's called trashing, which is that the system will be so busy keeping up with the tasks that none of the actual task will get done."
    author: "ac"
  - subject: "Re: Great Speed tips for Linux users:"
    date: 2001-05-06
    body: "\"I found if you change the /usr/src/linux/include/asm-i386/param.h file to #define HZ 1000 rather than #define HZ 100 everything GUI was _much_ _much_ faster. The GIMP seems to load in literally 1/5 the time.\"\n\nWhat does this do precisely ?"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "The Link and Thread:"
    date: 2001-05-06
    body: "http://slashdot.org/comments.pl?sid=01/04/24/1551243&threshold=-1&commentsort=3&mode=thread&cid=51\n\nis the link.\n\nWhat I gathered was that this changes the amount of time a process gets before context switching.  I could be off though.\n\nAnyway, it's a very interesting thread.  Full opf useful tidbits.\n\nBen"
    author: "Ben Hall"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "I would _love_ to have TV functionality in Noatun. :-) kwintv is far too unstable, and it would be cool to have all things in the \"visual & audio\" department in Noatun.. :-)\n\nNjaard!  get coding! ;)\n\n-- \narcade"
    author: "Rune Kristian Viken"
  - subject: "Mail, mail, mail"
    date: 2001-05-05
    body: "First of, amazing job guys.  KDE just improves at lightspeed.  There is no better file manager than Konqi anywhere, and you all have set the standard for smart, easy-to-use Cookie, Java and UserAgent management.\n\nAs for things I want:\n1)  My highest priority is an IMAP mailer.  I have been using KMail in KDE-2.2-1 alpha, but it has a few serious problems.  The key one is that it does not seem to cache IMAP mail locally.\nI would like to see KMail do good IMAP caching, and syncing, with the kind of fine-grained control the KDE so excels at.  One thing that would advance KMail way beyond other IMAP mailers would be if it could keep track of the tasks it is performing and allow the user to prioritize or cancel any one of them, without stopping others.  Also nice would be persistent folder views (like the strange Polarbar mailer).\n\n2) Compatibility with Windows.  Sorry folks, I just can't get rid of Windows any time soon - it'd be really nice to be able to read and write Word docs, read the Windows address book, and IE bookmarks and generally have access to Windows data while in KDE.  This would also smooth the transition for everyone.\n\nAgain, thanks for all the great work on KDE.\n\nCheers,\n\nEric"
    author: "Eric E"
  - subject: "Re: Mail, mail, mail"
    date: 2001-05-05
    body: "\"My highest priority is an IMAP mailer.\"\n\nWow, the KDE developers really know what users want!  IMAP is in Kmail 2.2alpha1 and will be in the next KDE release.\n\n\"Compatibility with Windows\"\n\nWord filters for KOffice are in development.  I'll second the motion on IE bookmarks, though."
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "durnit, i wanna be able to copy and paste text with konsole.  maybe konsole will do it and i'm gonna be really happy when i upgrade, i dunno.. (gnome terminal has had this for a while now, and i'm jealous.)"
    author: "unterbear"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "What are you talking about?  Highlight some text, press middle mouse button!  Konsole wouldn't be much of an xterm replacement if it didn't have copy and paste!"
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "argh! /me slaps forhead..\n\nguess the oldschool X-style copy paste isn't all that intuitive in a desktop that looks and (in other ways) acts like that, err..  \"other\" OS.  it could at least be on the RMB menu or something..\n\nthis is the same thing as has been driving people nuts with Konq, too, isn't it?  so it might be easier for us lusers when we get Qt 3..  i can hope.."
    author: "unterbear"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "I would really like to see an option to enable or disable Viewing hidden Folders in Tree View of Konqueror,and eventually more configurable View of Treeview :-)\n\nThank you for listening\n\nAB"
    author: "A.B"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "I would like that too and even more importantly: the same with files for the \"Open File\" dialogue. There used to be this option to show hidden files in KDE1."
    author: "Anton V."
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "What about:\nreduce code size?\nOr increase component reusability?\n\nI think those would really help KDE on the long run.\n\nKeep up the excellent work!\n\nAndrea"
    author: "Andrea"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "To click the button 'update kde', and have the latest version with the most wanted features running on my system! ;)"
    author: "Roger"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "You can get the next best thing!  Try Debian.  Here is how to update KDE on a Debian system.  You open a Konsole and type:\n\napt-get upgrade\n\nAnd then you sit back and relax while the latest KDE, along with all other Debian applications you have installed that have been updated, are downloaded, installed, and configured, all automatically!  You can even leave and do something else while it does this!\n\nI switched to Debian just for this feature and I have never looked back..."
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "So far, everybody wants a winblowz clone.  I don't need a winblowz clone!!  I need speed, efficiency, stability, and security, not necessary in that order.  Less spoon-feed-me-features with CPU and memory hogging side effects, and not to mention stability and security.\n\nThere exist many feature-ridden OS' available, and you know where they come from.  I need an alternative to this feature-driven philosophy.\n\nCurrently, I am trying out KDE2 on OpenBSD.  The efficiency and speed is slower than 98 or even nt4.  Haven't got it set up properly, yet, but I am not impressed.  I'll admit that it's nice and is an improvement to KDE1, but just slow for my taste."
    author: "Tom"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "I agree. My pal (windoze user) installed Mandrake 7.2 with KDE2 and he was disappointed and said \"I thought linux was robust and fast on old (133mhz 32mb) hardware\". Not very good advertising for unix-newbies either."
    author: "Anton V."
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "the ability to save x,y window locations when an\napplication opens.\n\nthanks to all the kde hackers for their fine\nefforts in making linux a better experience!"
    author: "kdfpod"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "What I _really_ would like is for konsole to support ANSI fonts such as vga.pcf, etc. Konsole is such a great terminal, but it doesn't support the _real_ linux console fonts."
    author: "Brandon"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "The most thing i want is better multimedia capability.\nKDE has got a quite good media player (noatun). But have you tried to play AVI-Files containig special encodings ? I have a lot of those files on cd but most of them do not work. Also a professional sound recording and editing software is missing.\nI also would say that an image viewer like kuickshow should be part and standard viewer of the KDE distribution because it's simple to use, allows fullscreen view without the windowbar, has an auto-scale option for the best view and last but not least it's very fast !"
    author: "Tom"
  - subject: "NeXTSTEP like features"
    date: 2001-05-05
    body: "I would love to see the option of having a NeXTSTEP style menubar, with tear off sub menus.  I find it much more elegant than having the menubar at the top of the screen or in the app's window.  If KDE can do a Mac style menubar then a NeXTSTEP style menubar shouldn't be too much of a problem.  Maybe it could even let you pop up the whole menubar under the mouse pointer when pressing a keyboard shortcut, or have it mapped to an extra mouse button.\n\nThere is a screenshot showing the NeXTSTEP menubar here:\n\nhttp://www120.pair.com/mccarthy/nextstep/intro.htmld/newsflash.gif\n\nI would also like to see the option of a NeXTSTEP like file browser, preferably with the extra features that are in WinBrowser.  I find that I can browse and manage files very quickly with a single window open using this kind of file browser.\n\nHere is a screenshot of WinBrowser:\n\nhttp://www.winbrowser.com/mainscreenimage.htm"
    author: "Craig Dole"
  - subject: "Re: NeXTSTEP/Apple Cocoa column view file browser"
    date: 2001-05-07
    body: "Yes, I agree tearoff menus would be nice - I believe they are going to be part of Qt 3.0 - so possibly could be in KDE 3.0.\n\nI would love a Smalltalk/NeXT/Apple column browser widget, rather than the 'explorer' style ones we \nhave at the moment. The icon with a cross if very small, and hard to click on, and you have to keep closing up everything you open - something you don't have to bother with a Smalltalk/NexT/Apple, and a shelf/dock/bookmark system.\n\nI don't like 'List view' on the Mac 9.x Finder either, that is just as hard to use. It's so much easier to navigate the file system on Mac OS X with colunm view.\n\nBut my personal top request is for copy and paste to be sorted out - no copy on select please. That will be a real improvement with Qt 3.0.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "<p>kde is changing rapidly.average users need a way to upgrade.please put all of the packages in one directory with an installer or at least a script.\ni use mandrake7.2. tried an up grade to 2.1 from 2.0 and got lots of error messanges.having killer the computer many times i quit.a working out of date computer is better than an attempted up grade that fails.\nhow can i recomend linux when it is so hard to upgrade?</p>\n\n<div>thanks for kde\npatrick</div>"
    author: "fatboy9"
  - subject: "Need a movie player"
    date: 2001-05-05
    body: "Lately I've been trying to watch some movies, and I had extremly hard time. I ended up booting to windows and watching the movies there, which wasn't very pleasant since it crashed about every 8 minutes.\n\tIt would be nice if KDE had a movie player that supports mpeg, some common avis, mov files, and the OpenDivX plugin (www.projectmayo.com). \n\nThanx!"
    author: "George Vulov"
  - subject: "http://mplayer.sourceforge.net ( anything goes )"
    date: 2001-05-06
    body: "http://mplayer.sourceforge.net/homepage/news.html"
    author: ".o."
  - subject: "Re: Need a movie player"
    date: 2001-05-06
    body: "Strange that you should have to do that.\nI view a lot of video'es that Windows does not show.\nHavent you installed noatun and the needed libraries?\nIf you install a distribution version of KDE and select the KDE multimedia package you WILL be able to see movies"
    author: "jegjessing"
  - subject: "Re: Need a movie player"
    date: 2005-07-02
    body: "I think I installed all of KDE, I can't find a program to view movies. What is the name of it. I'm trying to find caffine, I've heard it works but I'm having trouble finding it.\n"
    author: "bobbu"
  - subject: "Re: Need a movie player"
    date: 2005-07-02
    body: "Noatun of kdemultimedia plays videos. The format it supports depends on the libraries you have installed (xinelib)."
    author: "Anonymous"
  - subject: "Addressing in KMail"
    date: 2001-05-05
    body: "With all the folks talking about IMAP, I thought I'd hit on a little something different here.  How KMail handles addresses.\n\nAt this point in time, my personal opinion is that Netscape 4.7x handles this better than any other mail client out there.  For a moment let's just ignore NS's other probs.\n\nFirst off, the address book itself is fantastic.  Allows for multiple books to organize addresses, and the storage of all kinds of other goodies.  Built for syncing in with Palm Pilots, which will do nothing but increase in importance.  Also, full and seemless integration with an LDAP server allowing for a centralized address book to call from.  This is a priceless feature for anyone providing addresses to a company or large organization (like KDE folks for instance).\n\nCouple this with an elegant auto completion feature in the composition window, and nothing else even comes close.  The auto complete is even capable of mapping to an LDAP server defined in the address book.  This is a huge feature for me where I work, and why I've kept my users using Netscape mail right up to this day.\n\nA little something that Mozilla just got in there is an auto grab for addresses coming into your inbox.  All these addresses go into a folder of their own to allow you to copy them to live folders later if you so choose.  You can force an address into the book by simply clicking on it from within the mail body.  In short, the address book should have MUCH tighter integration into KMail.\n\nThe other part of this is how each address you enter appears on a line of it's own in the composition window.  Each one shows both the name and address next to eachother, and allows for setting any line to \"To:\", \"cc:\", and \"bcc:\".\n\nFor me, I can use IMAP or leave it.  POP is usually a viable option for me.  A truly intuitive addressing system on the other hand is something that hits home with me every time I write a new E-Mail."
    author: "Michael Collette"
  - subject: "Re: Addressing in KMail"
    date: 2002-06-27
    body: "I couldn't agree more.  I know there is work under way from the Sphynx project to get LDAP support into Kmail, but haven't seen any definite from anybody as to when it will become available."
    author: "Flare"
  - subject: "Re: Addressing in KMail"
    date: 2003-03-10
    body: "Hmmm .. strangely .. the autocompletion in Netscape is the thing I hate about it above all others // you decide to type in an address for someone not in your address list and NS starts trying to geuss bits of names, auto-capitalises the first letter (which you then have to go back and change) etc etc .. I'd use NS as my main mailer if it wasn't for that one irritating feature."
    author: "Robin S"
  - subject: "FTP Setup"
    date: 2001-05-05
    body: "I've been finding that Konqueror makes for a really outstanding FTP client.  Along these lines, it'd be nice to see an FTP configuration section in the prefs.  Also, some FTP specific bookmark values would be sweet.  There's a number of things that an FTP client might need to be configured for that simply don't apply to a web bookmark.  It'd be nice to be able to define a bookmark as an FTP site, and allow a user to add site specific data.  Stuff like whether or not to use PASV, port numbers (yeah, I know that can go in the URL), setup for sftp, and goodies like that.\n\nI'd also like to be able to use the side panel in Konqueror to navigate directories on the remote server similar to what a user sees on the local hard drive.  Some 3rd party Windows Explorer extensions allow you to add FTP sites over in the left hand pane along with directories and such.  Something along these lines would really trick up Konq's FTP abilities.\n\nAs it is, Konq does pretty good at FTP without all that.  Even still, it'd be nice to see some of this come about."
    author: "Michael Collette"
  - subject: "Re: FTP Setup"
    date: 2001-05-06
    body: "SFTP from openssh2 would be just the thing! When I transfer files now its all with lengthy scp commands - sftp support & implementing an even better ftp interface as Michael Collette suggests would make konqueror top choise for remote file management. But again I guess most developers certainly wouldnt want to transfer anything in plain text."
    author: "Jeroen van Drie"
  - subject: "Re: FTP Setup"
    date: 2001-05-07
    body: "Jeroen, you could set up a simple port forward in your ssh config file to do about the same thing.  I agree, ssh integrated into the client would be pretty nice, but it's not a show stopper.  Just put the following in your $HOME/.ssh/config file...\n\nhost www.domain.com\nLocalForward 2121 www.domain.com:21\n\nGet yourself an ssh session going with that domain, then access your FTP site with...\n\nftp://username:password@localhost:2121/\n\nOh sure, it's a bit of a workaround, but it does the trick."
    author: "Michael Collette"
  - subject: "Re: FTP Setup"
    date: 2001-05-07
    body: "Hi Michael,\n\nYes this is true but it would only encrypt the control channel over port 21 not the data channels unless we use passive. \n\nI am paranoid so all I can use this or any other FTP client for is to download from public archives. Sftp does a fine trick of encrypting the entire session - Globalscape http://www.cuteftp.com cuteftp has a commercial windows client available (I am thinking it talks to the sftpd or uses ssh/scp).\n\nIt wouldn't be a show stopper for most, but it would be for me. Maybe Konqueror though is not the place for integrating sftp.\n\nStill - I have no complaints; it would only be a luxury, a session in Konsole and an scp commandline work just fine! I'm extremely impressed with KDE."
    author: "Jeroen van Drie"
  - subject: "SFTP"
    date: 2001-05-07
    body: "Hi Michael,\n\nYes this is true but it would only encrypt the control channel over port 21 not the data channels unless we use passive. \n\nI am paranoid so all I can use this or any other FTP client for is to download from public archives. Sftp does a fine trick of encrypting the entire session - Globalscape http://www.cuteftp.com cuteftp has a commercial windows client available (I am thinking it talks to the sftpd or uses ssh/scp).\n\nIt wouldn't be a show stopper for most, but it would be for me. Maybe Konqueror though is not the place for integrating sftp.\n\nStill - I have no complaints; it would only be a luxury, a session in Konsole and an scp commandline work just fine! I'm extremely impressed with KDE."
    author: "Jeroen van Drie"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-05
    body: "I know this doesn't relate directly to KDE.. but it'd be nice to have a red-carpet channel.  This would take care of the installer, at least for me and many others."
    author: "Jeremy"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Heh. Reading such a huge amount of requests I ask myself: ho is gonna do it? People, please, give these hardworking guys a break. Take it easy.\nKonqueror is in pretty good shape (hm, java is sometimes a problem, but as far as I know, the sites that sucks are sites 'best viewed in Micro$ft Internet Explorer'. Kword is much more stable now (I am talking about 1.1 beta), and import and export filters will be finished in a few months. If Micro$soft goes xml way, better for us, and easier for us.\nSo if we get a stable kdeoffice with kde 2.2 we should all be happy. And then, I am sure that kde developers are aware that next step should be multimedia, and when I say multimedia I mean  video and streaming media. Unfortunately, only real.com has a guy who works on konqueror integration of realplayer, and we can't expect that other bigwigs do something in this direction: Apple & Microsoft. I've noticed lately that many sites which have supported realplayer now only support window's media player and apple's\nquick time. I don't know. This is going to be difficult task.\nBut hey, remember, just a few months ago antialiased fonts on Linux desktop was a dream, and now it is a normal thing (at least in kde;-). Kde developers gave us so many improvements in such a short time that it is incredible. \nI have only only one request: enjoy and have some fun, as much as I enjoy and have fun using kde."
    author: "Antialias"
  - subject: "What about missing features of KDE 1 in KDE 2.x?"
    date: 2001-05-06
    body: "I feel bad that KDE 2.x do not convert all important and useful options and modules from KDE 1.x, like \n\n1. Find in Page\n2. Window buttons module\n3. Color in Konqueror (as in KFM) users can't have their own html background and foreground color in KDE 2.x. that's bad. and please this should be there, instead of CSS sheet files.\n\nBy the way 'Corel's Find Computer' application should be placed into Find submenu and 'Video Card' module of KControl can be easily ported to our KDE 2.2. Please have Configuration tools in KDE 2.2, now Redhat's DNS (bind), Apache etc., config tools should also be ported and Integrated to KDE for a complete Desktop Environment. \n\nDon't mind but it seems that \"KDE is at the Mercy of Gnome's tools!\". KDE should be self dependent and should provide the ease of administration to everyone using Linux or any Unix flavor. \n\nfor KDE's sake please integrate Configuration tools in KDE.\n\nExpecting and wishing for configuration tool integration in KDE 2.2...\n\nKDE Lover!!!"
    author: "maarizwan"
  - subject: "Re: What about missing features of KDE 1 in KDE 2.x?"
    date: 2001-05-06
    body: "1. Find in Page\n\nPress the Edit menu and click on Find (how could you overlook this?)\n\n2. Window buttons module\n\nIt's in kde2.2 cvs ;)\n\n3. Color in Konqueror (as in KFM) users can't have their own html background and foreground color in KDE 2.x. that's bad. and please this should be there, instead of CSS sheet files.\n\nCSS sheet files is a very important feature, and in kde2.2 cvs there's options for setting specific stuff like background and so on..\n\nDon't mind but it seems that \"KDE is at the Mercy of Gnome's tools!\". KDE should be self dependent and should provide the ease of administration to everyone using Linux or any Unix flavor.\n\nEh... I haven't been using gtk/gnome apps here for a long time.. And for the configuration modules, there's recently been started a project by Charles Samuels.."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "IMAP support for KMAIL."
    author: "Richard Lynch"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "It's in for 2.2, lucky you!"
    author: "Con Kolivas"
  - subject: "features are nice, but. . ."
    date: 2001-05-06
    body: "Features and toys and programs are nice, but what good is it if they don't work?  If it doesn't work correctly at least 99% of the time, don't bother.  I'd much rather be told I can't do something and find a replacement,  than be told I can and have it not work.  (This goes for you too Microsoft).\n\nBTW: IMAP4 support in Kmail"
    author: "characterZer0"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "konqueror needs to remember my passwords and login data, I can't :)\n\nAnd we need a quicken clone with similar features and importing of quicken files\n\nAdd a cold beer, and I'll be happy ;)"
    author: "nomind"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "\"konqueror needs to remember my passwords and login data, I can't :)\"\n\nKonqueror will begin helping your memory starting with 2.2.  I'm using a CVS Konqueror right now and it already has this feature.\n\n\"we need a quicken clone with similar features and importing of quicken files\"\n\nCheck out Kapital by theKompany (www.thekompany.com).  It has exactly what you want.  The only problem is, it's not free.  But the money goes to a good company.  TheKompany has been helping out a lot with KDE and giving most of their stuff away for free, but unless people buy the products that they do sell, they won't be able to stay in business.  So buy Kapital and support theKompany and KDE!"
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "\"Konqueror will begin helping your memory starting with 2.2. I'm using a CVS Konqueror right now and it already has this feature.\"\n\nIs it stable?\n\nI paid for quicken, if Kapital will replace it I'll write the check with a smile.\n\nThanks for the info, I'll look into these"
    author: "nomind"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Konqueror from KDE CVS or daily code snapshots is not as stable as the 2.1.1 release, but it is useable.  It is a development version, of course, so not everything works perfectly.  I recommend you wait for KDE 2.2 unless you _really_ need this feature."
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "konqueror needs to remember my passwords and login data, I can't :)\n\nAnd we need a quicken clone with similar features and importing of quicken files\n\nAdd a cold beer, and I'll be happy ;)"
    author: "nomind"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "The Kompany-\n  \nhttp://www.thekompany.com\n\nhas kapital available now. I tried the download of the 1st beta but it crashed horribly on both my systems :(. It's not Free Software (in the speech and beer sense- both) but it may be something worth looking into.\n\nThe Kompany has been doing a lot of work on Open Source projects otherwise."
    author: "Joseph Nicholson"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Thanks, I'll check it out"
    author: "nomind"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "I voted for an Installer albeit perhaps blindly. Is this an installer for KDE itself (updates, etc?) or for other and/or all software packages?\n\nIt would be way cool for those from the Mac and Windoze world to have a ''one click'' installer for everything from .tar, .tgz, .deb and .rpm packages. Of course, having used exclusively Linux for 2 1/2 years now I prefer to compile my own apps from the shell whenever I can :).\n\nThe importance of this of course would be for the influx of refugees from the GUI-based operating systems ... a click that unzips the file and tells you what's going on ... an interactive question thingie like:\n\nConfig ready. Proceed? [Y/n]\n\n(Neat output to screen)\n\nMake ready. Proceed? [Y/n]\n \n (User watches in wonder as it it does it's thing).\n\nMake Install ready. Proceed? [Y/n]\n\nPlease give root password:\n\n(More output and ... )\n\nDone! Menu item installed in Internet--> gaim\n\nClick on there or type  gaim on the commandline to start program. Type  gaim &  if you'd prefer to return control of the shell to you ...\n\nAnd a function similar to apt_get that would perhaps prompt you for the disk or take you to a site to get an unsatisfied dependency?\n\n======\nOf course this is a flight of fancy that would have a lot of purists flame me but what's wrong with choices? \n\n\nLastly, I'd like to see Netscape plugins work \"out of the box\" including already installed ones like rpnp.so, flash, etc. (come to think of it I think I watched kimble.org in Konqueror on my Mandrake 8.0 laptop but I'll have to check that. Ditto for JavaScript, etc.\n\n\nNice work KDE team all around. Thank you! You've given 8 of my close friends a reason to switch or even start out on computers with Linux!"
    author: "Joseph Nicholson"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Hehe.. you already have a program that does this (the compile stuff)... Take a look at kconfigure ;)\nhttp://kconfigure.sourceforge.net/"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Yes, there is the KDE Installer Project, but it's still vaporware.  It's made mostly by Nick Betcher.  I help out some too.  Here's the site: \"http://kdeinstaller.rox0rs.net\"."
    author: "Steve Hunt"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "1) Konquerer - especially printing:\n\na) I never know for sure if konquerer will print\na page or not until I walk downstairs and look\nat the HP Postscript printer. Usually it does\nbut never if I am looking at a *.pdf file.\n\nb) At least 1 out of 3 Java applets seem to\nbreak konqueror. \n\nc) Sessions - like in konsole - for that neat\nterminal emulator window.\n\n2) A parameter which if on says : if in doubt\nuse emacs/xemacs key bindings rather than Windoze bindings.\ni.e. as I am writing this:\nCntr-E (good, I get end of line)\nCntr-A (bad, it selects the whole text)\nCntrl-D (oops where am I now)\n\n3)\nChose another name for Advanced Editor - xemacs\nIS the advanced editor.\n\n4) KMail\nA much higher expectation of success if I open\nan atachment sent me by a Windoze user.\n\n5) Lots more of course but each new version of\nKDE2 suprises me with the rapidity of how my\nwish list shrinks?\n\nFor me KDE2, has won the catch up on Windoze\nexcept for the quality of fonts but one can hardly blaim KDE for that."
    author: "Malcolm Agnew"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Printing is being improved in all of KDE for 2.2.  Konqueror's printing capabilities in particular are being improved.\n\nAbout key bindings:  the key bindings module in KControl allows you to change the default key bindings in all of KDE at once.  You can't make 2-key shortcuts like C-x C-c but you can make a lot of key bindings more Emacs-like.\n\nIn KDE 2.2 there will be a new editor called Kate.  Hopefully it will replace the \"Advanced Editor.\""
    author: "not me"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "1) Konquerer - especially printing:\n\na) I never know for sure if konquerer will print\na page or not until I walk downstairs and look\nat the HP Postscript printer. Usually it does\nbut never if I am looking at a *.pdf file.\n\nb) At least 1 out of 3 Java applets seem to\nbreak konqueror. \n\nc) Sessions - like in konsole - for that neat\nterminal emulator window.\n\n2) A parameter which if on says : if in doubt\nuse emacs/xemacs key bindings rather than Windoze bindings.\ni.e. as I am writing this:\nCntr-E (good, I get end of line)\nCntr-A (bad, it selects the whole text)\nCntrl-D (oops where am I now)\n\n3)\nChose another name for Advanced Editor - xemacs\nIS the advanced editor.\n\n4) KMail\nA much higher expectation of success if I open\nan atachment sent me by a Windoze user.\n\n5) Lots more of course but each new version of\nKDE2 suprises me with the rapidity of how my\nwish list shrinks?\n\nFor me KDE2, has won the catch up on Windoze\nexcept for the quality of fonts but one can hardly blaim KDE for that."
    author: "Malcolm Agnew"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "in konqueror i'd like to save an html page and its images into a SINGLE file. ie5 has this feature (look in win2000 for .MHT files) and i have already lots of mht files: i cannot switch to linux until i can read and save .MHT files!\n\nfor more info you can find the RFC here:\n\nhttp://www.dsv.su.se/~jpalme/ietf/mhtml.html\n\nthanks"
    author: "matteo porta"
  - subject: "Re: Poll: What KDE Feature Do You Most Want? - mht"
    date: 2002-08-26
    body: "I would like to see ALL browsers, especially those under FreeBSD and Unix in general, support File> Save As... > mht web archive single file :) What's the latest on this, vis-a-vis KDE, Netscape, and Gnome? Thanks."
    author: "Peter Leftwich"
  - subject: "Re: Poll: What KDE Feature Do You Most Want? - mht"
    date: 2002-08-26
    body: "You can archive pages in a single file with Konqueror.  Install KDE Addons."
    author: "ac"
  - subject: "Re: Poll: What KDE Feature Do You Most Want? - mht"
    date: 2004-04-11
    body: "You do not have to save as mht files the new ones, Konqueror has its own *.war file (web archive). You just need to view the old ones that you have.\n\nIn reality, war and mht are both e-mail or news base64 encoded \"messages\" that embed the data inside the file.\n\nThere is a way Netscape Messenger can view the archives - I've tested it, but I dropped this hint brcause even viewing pictures and text, Netscape Messenger could not replicate the layout of the original page as saved by Internet Exploder. The pictures came always at the bottom of the page and their initial position was void but in the dimentions of the picture. That's the tragedy when you rely on proprietary non-standard formats. The blame is entirely on Microsoft."
    author: "micro"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "hi,\n\na wysiwyg - html editor/webgraficstool such as Dreamweaver/fireworks would make KDE the best web-ide ever.\n\nand .. please speed up kde .. as (at the moment it is much solower than M$ guis)"
    author: "uli"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "That would be great.  I think a Front Page semi-clone would be nice, just forget the proprietary tags and server-extension issues.  But yes, a nice free (or cheap) wysiwyg editor designed for KDE would be great :-)  I'll ask TheKompany about that, see if they're interested."
    author: "Steve Hunt"
  - subject: "Better XML support in Konqi... and KDE"
    date: 2001-05-06
    body: "I'd like to see Konqi become *THE* XML browser - XML is now the standard markup language of the Web, and unfortunately IE is beating everyone else to the punch. I'd hate to see everyone start building on it's propreitary XML/XSL extensions. I've already seen some prelimenary code functionality - click on an XML document and it will attempt to parse it (against a CSS). However, it crashes out on simple scripts. Opera (the Qt browser) seems to be gaining some better XML support as well. This is reminds me of something I've been wondering - what kind of long range plan exists for XML support in KDE (and in the Qt core)? GNOME seems to be working on some XML/XSLT libs, and I know of some support for DOM & SAX Lv. 2 in Qt 2.x. The IE engine and MSXML form the foundation for EVERY XML app (generally) in Windows. I believe we need to provide that same (if not better, more compliant) component kit for developers looking for their next generation platform. More than half of all desktop application developed in the future will handle XML in some way - let's become the best platform for XML development, both on the Web and on the desktop!"
    author: "Eron Lloyd"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "It is a good idea to do that !\n\nWhat i want in kde2.2 :\n-a trash (as in win$ or MacOS)\n-a good DnD with the bookmark and history in konqueror,\n-a faster start ok kde2.2 !\n....\n\nJP"
    author: "Martin"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "> -a trash (as in win$ or MacOS)\n\nExists since ages\n\n> -a good DnD with the bookmark and history in konqueror,\n\nDefine, please!"
    author: "Daniel Molkentin"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "i have an athlon 1400mhz with 512 ram fast hard disk , and it take 1 second to open the preferences from konqueror!\nit take 2.5 seconds to start an konqueror window\nfrom the panel!\n2 seconds for kmail to startup.\n4 seconds to get notun startet when i kilck on an mp3 file directly.\n3 seconds to start Quanta +\n4 seconds for kdevelop\n3 seconds for kword\n\ncant they be INSTANT there ???"
    author: "chris"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-12
    body: "Whith 512 MB of RAM you only have to do this once.\nWhy do you keep shutting them down?"
    author: "reihal"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "ok. you asked for it ;)\n\nI would like applications to appear on the same desktop that they were started from. Dammit, we had that feature before, I want it, please!\n\nWhen you click to maximize a window, it fills up the whole screen, except for kicker's space. So far so good. What I'd like to do is hold down shift or alt or whatever and drag a rectangle on my desktop. Now, whenever I click on maximize, the windows will be exactly on this rectangle. I think this would be very handy when you have a big screen (i do ;) and want to leave some space on the side for some status-info apps.\n\nI don't think it would be too hard to do that, since it is already done with child-extensions of Kicker etc. They are not covered by other windows on maximize, either.\n\nThank you for listening!"
    author: "me"
  - subject: "make easier the work of the bugs reporter"
    date: 2001-05-06
    body: "It's not THE feature, but it could be very useful. For example, the bug report must be in english, but a non-english user can't know the english name of a button or a menu, so it could be very useful to change the language of ONE application DYNAMICALLY, sothat it could use the real english name in his bug report. An another feature : the wizard web bug report is cool but it will be more cool if it use kmail to send a mail, sothat the bug reporter can manage his bug report thanks his mail client AND take advantage of the easiness of the web interface.\nAn another another feature : replace that web interface by a programm, sothat the bug reporter just must connect for sending his bug reporter, and not to create his bug report too.\nJust three idea, JSL."
    author: "JSL"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Two things stability and speed. \n\nOne of the reason for using Linux instead of win9* was stability - and speed is always fun."
    author: "mg"
  - subject: "Don't reinvent the wheel"
    date: 2001-05-06
    body: "\"A good artist copies.\nA great artist steals.\"\n-Patrick Norton, I think.\n\nThis is what we should do with many things.  KDE is definately ahead of GNOME when it comes to the desktop, but many GNOME apps are ahead of their KDE counterparts.  What should we do?\n\nSTEAL THEM!!!\n\nThink about it!  They're under the GPL, right?  That means that we can legally take them, KDE'ify them, and package them.  Nothing wrong with that.\n\nWe should not make a new EVERYTHING if there is a similar, open source program that does the job.  Just port it over to KDE, and go.  That will save lots of time, allowing developers to add even more useful features in their programs.\n\nJust a suggestion, though."
    author: "Steve Hunt"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "I know I'm late to the party, and this will probably be lost in the noise, but what I want from the next KDE - \n\n- Ability to selectively sync KDE settings, bookarmarks, desktops, etc between two accounts. So that when I bookmark a webpage at work, it automatically shows up on my KDE at home.\n\n- Ability to bind progams to key presses. When you need a new konsole windows, it sucks to have to leave the keyboard to launch one (and the K-menu is difficult to keyboard-navigate quickly)."
    author: "Russ Steffen"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "u can do the last one in menu editor...\n\ngood luck\njason"
    author: "Jason Katz-Brown"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Enough speed so it can actually be used on average computers.\nComputers that run KDE1.x just fine."
    author: "Irv Mullins"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "What KDE really needs is stability. In my opionion the developers concentrated more on new features and pretty graphics then on keeping the windowmanager stable and fast. Though KDE is feature rich, user friendly, and a pure eyecandy, it doesn't make me want to be my default windowmanager until it get's more stable."
    author: "./scythe::lhfd->s"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-06
    body: "Bookmark organisation by drag'n'drop directly in the sidebar.\nAnd remembering that the sidebar was open last time I used konqueror."
    author: "n"
  - subject: "Multimedia!!"
    date: 2001-05-06
    body: "We need a media player that can play avi, divx etc etc.. And we need it now!! \n\nKde is great!!"
    author: "Olle"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "<P><STRONG>First of all thank you for making KDE.</STRONG></P>\n<P>I think the main missing feature is</P>\n<P>\n<UL>\n<LI><STRONG>Stable KOffice.<STRONG> And it should be able to open/save all MS Of\nfice formats perfectly (or at least that KWord is able to open/save MS Word 97 format</LI>\n</UL>\n</P>\n<P>I think the MS Office is the key to MS success. If the KDE-team can make KOffice a better and faster product then MS Office then the users will start using KDE and Linux instead of Windows.\n</P>"
    author: "Rune Nordvik"
  - subject: "I miss my loved INCREMENTAL-SEARCH from Emacs!"
    date: 2001-05-07
    body: "I think, by pressing Crtl-s, the cursor should jump into the status line or so (_not_ open a much to big window, which hides the text I wanna search!) and expect my incremental-search. And best of all, this should work in every KDE-program (konqueror, editor, kmail, ...) - I'd LOVE it!"
    author: "Ansgar John"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "The feature that I would like to see added most is something small, but I would like to see the spinning K gear in konqueror changed to something a little more useful, namely a bandwidth meter.  It seems like every browser since mosiac has had to have a completely useless spinning globe type icon sitting in the upper right hand corner to be considered complete...how about a change?"
    author: "Elliott"
  - subject: "problems with \"Dock Application Bar\" and java apps"
    date: 2001-05-07
    body: "I really like KDE's support for WindowMaker dock apps and I use it a lot. There is one bug on this feature though, that prevents me to use it alll the time. Once the \"Dock Application Bar\" is enabled, most java aplications will incorrectly \"dock\" into the docking bar, forcing me to shut java application the docking bar down. I would appreciate it very much to see this bug fixed."
    author: "Pedro Ziviani"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "KDE is doing a wonderful job of unifying the UNIX desktop.  It just needs to continue down that road until all aspects of using a desktop computer can be done from KDE.  This includes:\n\n- Configuration utilities.  I agree that this should be in the hands of the distribution writers, but they should be writing plug-ins for the Control Center so that you can handle ALL aspects of system configuration from here, including network setup, printer setup (printtool works great but looks like crap), and so forth.\n\n- KOffice.  So far all of the apps are attractive, polished, and easy to use.  Unfortunately they are not terribly useful; trying to edit anything more complex than a letter in KWord is pretty difficult.  Ditto for the other KOffice apps.  However, I'm much more inclined to use them versus the more mature StarOffice, Applix, or even WordPerfect, for the simple reason that they bear the polished, integrated, slick look and feel of KDE.  All the other office apps are, quite simply, clunky.\n\n- Interoperability.  KOffice needs to lift the import/export filters for MS Word and Excel docs out of StarOffice so that I can use KWord and KSpread all the time, not just when I'm creating docs/spreadsheets that no one will ever use but me.\n\n- Speed.  If Konquerer had the snapiness that (say) Netscape 4 does, I'd be in heaven.  I still use Konquerer because it's better, but even on an Athalon 700 w/ 384megs of RAM it still slows down quite a bit when I've got six or eight reasonably complex pages open.  Logging in and logging out are two things that could also use optimization - sharing a computer amongst a number of roommates is great on a true multiuser system, but it's a 90 second turnaround (or so) to log out and log back in, even when all of KDE should be in the memory cache.\n\n- Documentation!  There are a million cool features in KDE, but most of them are impossible to learn about without either browsing the code, find out from one of the programmers, or reading it on dot.kde.org.\n\nKDE is, in my opinion, the best desktop enviornment ever created, for any platform.  So it's really exciting to see it continuing to move forward; just imagine how good it will be in a year, or two years time."
    author: "Adam Wiggins"
  - subject: "Re: Poll: What KDE Feature Do You"
    date: 2001-05-07
    body: "Finally...\n\nI was looking for a post in this poll, that I \ncould reply to, as simply saying: \"lean back, \nit's alright\" seems not suitable.\n\nThe mentioned incompatibility of KOffice is \nreally one thing I cannot criticize but lament\n\nWho wants M$-Word, if he can use better things?\nWell-, about all the folks I have to exchange \ndocuments with...\n\nBut I deem the task a heavy one and would not\nventure to urge anybody.\nIn fact: My incompetence to contribute\nsome useful code myself is what makes me really\nsad.\n\nMichael"
    author: "Oops"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "<p>I want the development team to stop adding features, for about a year, and concentrate on making KDE smaller and faster.\n\n<p>Konquerer (the web browser) needs some serious work, especially.  It routinely locks on me."
    author: "WM"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "I'm surprised no one though of this.   SInce KDE is such a good desktop replacement, wouldn't it be ideal for client-server computing?  ow about an X-server/KWM verson for Windows and Mac that can connect to a remote Linux box, and run KDE (Similar to Exceed or something, but I know KDE only really likes it's own window manager...)?  All the processes and files and programs are stored on the remote Linux box (Mainframe, server, whatever) and the Wintel/Mac just acts as a dumb terminal.  It would also be a good environment to LEARN Linux from...rather than having 20 machines for a computer class for each OS, or setting them to dual-boot (And don't say VMWare...requirements are too beefy for the average workstation) have them run Windows, and establish a session to a mildly meaty remote KDE box."
    author: "Bryan Pizzuti"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "So why not just install Linux on them ?\nAnyway, X and Linux have always been network-oriented and multi-user!\nThere's Xfree now under Window$ and of course X under MacOS X so connecting to a remote KDE is just a matter of DISPLAY"
    author: "Herv\u00e9 PARISSI"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "Mark 1 for faster and less of memory hog for me.\n\nTwo more wishes:\n\n1.  Some adaptive organization facility that learns what you use and what you don't and better presents that to you when you use the system.  Just look at System and Utilities submenus from the K.  These are menus that are reaching critical mass with programs that people use less than 5-10% of the time.  *****Commandline utilities should be removed from the standard dist, if people want them, click on some script that'll put them in, otherwise it clutters the whole thing.*****\n\n2.  A better keyboard engine that can emulate VI and EMACS.  I want to hit ESC and then / in any window and have the search window come up.  I want to hit ESC and : to run commands from any window.  I want to bind macros to any command in any program to any key from the keyboard.  The status bar would be a great place to embed such technology.  ***Anyone up to the challenge??***  Remember the Tom Christiansen exchange on slashdot?  I think it is a serious problem when you dumify a WM you leave out expert users.  You add this and make it speedy, you make it an %%%AWESOME%%% experience for everyone.\n\nThanks for a great desktop."
    author: "Jeff"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "Konqueror file manager: \n1. Enable the user to publish a file/ directory/ printer/ modem... as a (Samba SMB) share. (as in Windows as of WfW) \n\n2. Have a facility to pick up ressources (SMB shares, any mountable device, printers, scanners, audio equipment etc.) and plug them into the system. As in WfW there could be a tree view of all network visible machines showing all their published resources.\n\nPerhaps one could integrate a general user interface and API for sharing ressources into KDE that would enable anyone to program their own plug ins for shared access. (For resources to be shared in that context I think of: databases, applications, scientific devices, robots etc.)\n\nBeside of that\n- Make loading and starting faster.\n\nConrad"
    author: "Conrad Beckert"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "I'd like to see an option in kdm to display/see the chooser-dialog first. (If it's already in there, i couldn't find it.)\n\nDoes the chooser dialog work at all? I've tried running it, but it never shows any of the xdmcp-servers on the network."
    author: "Martijn Bruns"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "I'd like to see an option in kdm to display/see the chooser-dialog first. (If it's already in there, i couldn't find it.)\n\nDoes the chooser dialog work at all? I've tried running it, but it never shows any of the xdmcp-servers on the network."
    author: "Martijn Bruns"
  - subject: "Strictly limited HTML rendering in KMail"
    date: 2001-05-07
    body: "I'd really like to be able to have KMail render HTML mail with only text formatting. No images, javascript, cookies etc. Qualcomm's Eudora has an internal HTML renderer that does this and I really miss it. With it you can read HTML mail without the security/privacy problems associated with accessing resources external to the machine."
    author: "Dennis Hostetler"
  - subject: "Acckkk! Am I too late?"
    date: 2001-05-07
    body: "Please, please, please...do this:\n\nMake it possible to edit bookmards \"in place\" at the time they're added to Konqueror. Perhaps the \"Add bookmark\" command pop up an edit box so that you can fiddle with the label. Too often a page is named something incomprehensible or too long to be useful. When you add a bookmark, you then have to fire up \"Edit bookmarks\", find the damn thing, and then edit its name. It would be s-o-o-o nice not to have to do this. ;-)\n\n(Yes, I have submitted this as a wishlist item...)"
    author: "Michael O'Henly"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "I'd like to see a consistent copy and paste mechanism in all of the KDE applications, including terminal applications and Netscape. Integration of this mechangism into gnome as well as KDE would be nice."
    author: "John Cox"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "Maybe a \"Metascript\" for installing KDE from sources. Like choosing which software should be installed (base system apart) and wether alsa (native) alsa (oss) or oss should be used, wether cups or lpd should be used. And, of course, where KDE should be installed and a field for optional CFLAGS.\nCould be similar to the curses based kernelconfig or \"make config\" from the i4l package. \n\nEven if there is no \"Meta Script\", would be nice to choose for the single packages. Most ppl don\u00b4t need 3 Editors or all the games, but would like some of them. \n\nWould just make life a bit easier (especially taking the releasecycles of KDE into account) for people who prefer to compile themselves.\n\nEven if compiling has the reputation of being only for somewhat advanced users, and it is no real problem to change into that corresponding dir for \"make install\", such a \"source installer\" would speed things up a bit and would be distribution independend anyway."
    author: "klaus berbach"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "1. IMAP Support in KMail\n2. Efficient support for large mailboxes (5,000+ messages) in KMail w/ and w/o IMAP."
    author: "Andrew Yochum"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-07
    body: "One of the reasons I keep switching back between WindowMaker and KDE as my preferred desktop, is that WindowMaker has icons that are specific to a desktop.  Switch to desktop one, and you have Terminals - switch to desktop 2, and you have various editors, desktop 3: various internet apps, etc."
    author: "Desktop-specific Icons/Application Launchers"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-08
    body: "IMO, one of the most important features still missing in KDE is the lack of a comprehensive\ninstaller, which 1st analyses the system to install to, 2nd asks some questions what the user wants (packages, w/o java support...) and then decides how to proceed - included\na brief description of what to do when something\nthat is reqiured to install the desired components is missing.\nwatching the kde-user mailing list, one can clearly see that most questions posted are around this issue.\nKDE itself is already very good, stable, with many, many features - keep on the excellent work !"
    author: "Werner Joss"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-09
    body: "As Quanta (a very great html editor) have a configurable toolbar with add shell script support.\nFor Konqueror itself, it will be better to have configurable toolbar action ...\n\nFor example, when we view a Japanese page, we may want to translate it to English, then we\ncan go Mozilla's translate function .... \n\nHowever if we have a tool in our computer (Under Unix, it like to be a shell script) , we can pipe\nthe input from current document and get the output after that action .... (Quanta does this) ..\n\nAnother example is, if we have many xml files, we can look it directly, but will only have default\nview of that xml. But if we can write a XLST, and write a shell script (by perl or php or anything!)\nWe can use different XLST to transfer that xml file and have a very powerful viewer ...\n\nMore over, if some one have a transcoder for ms'd doc, we can adapt that program and even able\nto read a doc file ...... \n\nAnd we may establish a collection web site for other\ncontribution of these scripts or even take them as a\nplug-in ..... Then all new feature will no-longer hard\ncode by core KDE developer but all KDE programmers\nor even a normal User ..... \n\nThanks for you and all of your\ngreat works on KDE and \n\nKonqueror  !!!!!\n\n-- \nWaily Yang\nhttp://Waily.warp.com.tw\nSoftware Engineer in DigitalSesame, Taiwan"
    author: "Waily Yang"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-09
    body: "Speed in startup and shutdown. I have only tried kde 2.1.0 so far, but its time to get you to work after logging in and especially to have you ready to leave the terminal after selecting \"logout\" are just very long compared to simple fvwm2 type enviroments. \nIn fact after selecting logout, it just puts the grayed screen back to normal as if I had selected cancel, and I can continue to work for at least a minute before it then suddenly does shut everything down and logs me off. That means I cannot just click on logoff and leave, I have to wait until it is safe to leave. \nAnd when I quickly want to read my email I tend to go to the ascii console now because KDE just takes too long. (I use 1 minute on the console and 5 minutes on KDE)\n\nMaybe some resources could be loaded delayed and less sequentialized. \n\nMaybe there could be a \"quick unsafe logoff\" option that does just \"kill -1 -1\" for nonroot users. I created an icon on my desktop to do just that and it helps. (I never lost anything, and I do only use it if I do not want any background processes of mine running after logoff.)"
    author: "Michael Will"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-09
    body: "Which people do mostly work with Linux? IMO thats professionals, programmers and advanced people. Such people doesn't need any gimmick, which ruins the stability of the basic functions of file-managment.\n\nAn example, on day Linux crashed and I have to reboot. After that, Konquerer doesn't start anymore. The solution was deleting some files in the .kde2/share/apps/konquerer directory. Also the update from KDE 2.01 to 2.1 was an horror-trip. After that KDE always flooded the system with IO/Slaves error-messages and refuse working.\n\nWhat we need is stability, stability, stability of the basic functions..\nOnly then I could say to customers: look at this beautiful Application running on a stable unix/linux desktop!\n\nBut above all, I do not forget what KDE did for the community... thanks!"
    author: "Harald Nikolisin"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-09
    body: "May I frame the question somewhat differently:\n\nThe costs of using an operating system/operating environment can be broken down into two parts.\n\n1.Fixed costs (or acquisition costs). \n2.Variable costs (or the cost of maintaining the product). \n\nPeople commonly focus on the fixed costs (which have been reduced to 0 by the Linux/KDE/... revolution) but ignore variable costs, which can be surprisingly high (a few years ago, the variable cost of a PC was estimated to be about $6,000/annum). Variable costs get driven up in a number of ways -  stability, upgradability, ease of use, the need for new users to be trained, the availability of help, the ease with which computers can be added, removed or configured on a network all contribute.\n\nOne way to think about Apple's value proposition is in terms of relatively high fixed costs for relatively low variable costs. \n\nToday, fixed costs keep dropping while variable costs keep increasing.\n\nIt is time for KDE to attack the variable cost question - what features would simplify the lives of administrators and users and minimize the cost of running KDE in an office environment?"
    author: "Thomas Philips"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-10
    body: "Konq features I'd like to see:\n\nIn addition to bookmarks it would be great if there was a \"stack\" that I could throw links onto for later perusal.   Once a stacked link is selected, it is removed from the stack. Thus I can stack up interesting links without having to stop reading or mucking up my more permanent bookmarks. \n\nSecondly, as I am on a slow connection, it would be a nice feature to \"queue\" a link for download.  The links in the queue are downloaded n at a time, where n is dynamically determined by connection load or configurable to some maximum value."
    author: "eric"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-10
    body: "1. speed in startup/shutdown\n2. kmail to support POP3 over SSL.\n3. konquerer to have a home button that points to a URL, not ~home.  maybe it can do this now, not sure??\n4. Kdevelop to be able to import multiple files at a time and update the makefiles for all the files being imorted.\n5. an installer.  similar to red-carpet."
    author: "Mark Webb"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-11
    body: "Improved Speed, Reduced Memory Consumption and better integrated FreeBSD support."
    author: "frank"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-12
    body: "I would like to see standardisation of back end services such a a single place to store and access fonts."
    author: "Paul Templeton"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-12
    body: "Hi,\nI would like a callback utility with KDE2.2\nI don't know whether you could configure kppp for callback.\n\nKeep on doing the great work!!! I'll use KDE till the day I die.\n\nKarl"
    author: "Karl Fischer"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-13
    body: "What KDE really needs is an easy installer/updater, like GNOME has with Ximian's Red Carpet. After using Red Carpet, going through the installation procedure of KDE is a nightmare."
    author: "Havard Bjastad"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-13
    body: "This whole thing about configuration has made me think... Some thinks that its a KDE issue to make them, and yet others think its a Distro issue. I think this whole deal goes both ways. First KDE needs to make the \"frame\" for configuration of eg apache, monitor, lilo and so forth. Then the Distros need to make a module that mathc their specific file-structure (and other things):\n\nX-Linux: \n{\nNAME=XMOD\nVERSION=3.3.6\nCFG=/etc/XF86Config\nDRIVER=SomeScriptThatCanFindTheRightLineAndChangeIt\n}\n\nY-Linux:\n{\nNAME=XMOD\nVERSION=4.0.2\nCFG=/etc/X11/XF86Config-4\nDRIVER=SomeScriptThatCanFindTheRightLineAndChangeIt\n}\n\nThis will (in my mind) make two things happen: A: its easyer to write a module with some info/scripting than it is to write a conf-util, thus they will rely on KDE (and not GTK) for conf'ing. B: since a coherent system is now awailable to all Distros they will rapidly c that its a good thing to make the distros more similar, since this will allow them to use eachother's mods for configuration. \n\nThats just my two cent... but i think its a cool thought ;-)\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-13
    body: "This whole thing about configuration has made me think... Some thinks that its a KDE issue to make them, and yet others think its a Distro issue. I think this whole deal goes both ways. First KDE needs to make the \"frame\" for configuration of eg apache, monitor, lilo and so forth. Then the Distros need to make a module that mathc their specific file-structure (and other things):\n\nX-Linux: \n{\nNAME=XMOD\nVERSION=3.3.6\nCFG=/etc/XF86Config\nDRIVER=SomeScriptThatCanFindTheRightLineAndChangeIt\n}\n\nY-Linux:\n{\nNAME=XMOD\nVERSION=4.0.2\nCFG=/etc/X11/XF86Config-4\nDRIVER=SomeScriptThatCanFindTheRightLineAndChangeIt\n}\n\nThis will (in my mind) make two things happen: A: its easyer to write a module with some info/scripting than it is to write a conf-util, thus they will rely on KDE (and not GTK) for conf'ing. B: since a coherent system is now awailable to all Distros they will rapidly c that its a good thing to make the distros more similar, since this will allow them to use eachother's mods for configuration. \n\nThats just my two cent... but i think its a cool thought ;-)\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-14
    body: "Better international support. I use cyrillic and KDE behaves strange when typing cyrillic letters (but may be this is a problem of X or QT?). Also better koffice; Curently there is no office package for Linux/Unix that can work with cyrillic without monts of hacking, installing fonts etc."
    author: "krassy"
  - subject: "Re: Poll: What KDE Feature Do You Most Want?"
    date: 2001-05-15
    body: "Speed and system integration ( consistant and easy hardware / desktop configuration ).\n\nGNOME and KDE have gotten slower =/ almost to the point where windows is getting faster on the same hardware. Ok I'm comparing KDE 2.11 to Windows NT on a 96MB P 266MMX ( the best I could get out of my employers =) but still, KDE 2.11 is beginnning to feel like a heavily loaded windows NT system ( as to say slow ).\n\nSome other integration ( such as easy to make links to programs on the desktop and the menu ) would make things easier as well.\n\nSome kind of structure that could be shared with GNOME as well could be a good move ( such as KDE and GNOME working together to provide automatic mirroring of menu / desktop configs so the distro doesnt have to do that )."
    author: "Greebo_Brat"
  - subject: "KDE Light. A light option."
    date: 2001-08-16
    body: "For users that are depending on compilation and slower machines <= 500 Mhz, the KDE is a real real pain to compile. \n\nI want to suggest the birth of KDELight. The light desktop, packed in a single tarball up to 9 MB, containing the basic components like window manager, internet dialer, and file manager. Exclude mail client, browser, and other stuff."
    author: "lince"
  - subject: "Re: KDE Light. A light option."
    date: 2005-09-10
    body: "Well...your prayers have been answered. There's a new distro called PocketLinux which has a KDE Light desktop. Check it out below:\n\nhttp://distrowatch.com/table.php?distribution=pocket"
    author: "1c3d0g"
  - subject: "Re: KDE Light. A light option."
    date: 2006-12-20
    body: "Yea , How the heck do you get the KDE light desktop to even start up in pocket linux...???\n\nI've tried and tried but i get nothing...!\n\nIs it supposed to start up automaticaly...?"
    author: "jason"
  - subject: "Re: KDE Light. A light option."
    date: 2006-12-21
    body: "try typing in \"startx\" ?"
    author: "Hairy"
---
Polls are an oft-requested feature of the <A HREF="http://dot.kde.org/">dot</A>.  <a href="http://www.kde.com/">KDE.com</a> has risen to the challenge with its <a href="http://www.kde.com/polls/vote.php?poll=5">latest user poll</a>: <i>"What Should Be the Highest Priority of KDE Developers Leading Up to KDE 2.2?"</i>.  I just installed <a href="http://www.linux-mandrake.com/">Linux Mandrake</a> 7.2 (until my <a href="http://www.suse.com/">SuSE</a> package arrives), and after upgrading to KDE 2.1.1, I feel that a KDE port of the configuration utilities could bring a huge amount of polish to this distribution.  A KDE interface to <a href="http://www.solucorp.qc.ca/linuxconf/">Linuxconf</a> might be a good start.  Others would however prefer a KDE installer, and some simply think that KDE should be faster and/or less of a memory hog. Here's <i>your chance</i> to <a href="http://www.kde.com/polls/vote.php?poll=5">cast a vote</a> and <a href="http://dot.kde.org/988934998/addPostingForm">voice an opinion</a>.







<!--break-->
