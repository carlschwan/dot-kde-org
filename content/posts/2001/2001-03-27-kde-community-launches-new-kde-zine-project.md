---
title: "KDE Community Launches New KDE Zine Project"
date:    2001-03-27
authors:
  - "tgasperson"
slug:    kde-community-launches-new-kde-zine-project
comments:
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-26
    body: "And best wishes for success!"
    author: "Tina Gasperson"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-26
    body: "And, of course, a big thanks to yourself for volunteering your time on this!\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "Remember that KDE doesn't have a figure head like the gnome project. That means it becomes even more important that we all get involved in kde's promotion. It doesn't take very long to right up a application review or an opinion article or something like that. So please get involved.  \n\nCraig"
    author: "Craig Black"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "Yes, but in such situations, propper grammar would be nice.  \"Right up a application\" should be \"write up an application.\" ;-)\n\n\n-Scott"
    author: "Scott"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "Cool with all those english skills i'll be looking forward to zine contribution.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "Proper spelling would be nice as well!\n\n;-)"
    author: "Tick"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-28
    body: "Lets look at this closer.\nComments are in '[ ]'.\n\nRemember that KDE doesn't have a figure head like the gnome[GNOME] project [Starting a sentence with \"Remember\" is not a good practice]. That means [that] it becomes even more important that we all get involved in kde[KDE]'s promotion. It doesn't take very long to [w]right up a[n] application review[,] or an opinion article[,] or something like that [Repeated 'or's that many times looks bad, also the sentence is more or less redundant, you should have said 'an article']. So please get involved[Starting a sentence with 'So' looks bad]. [Sentences are seperated with two spaces]\n\nCraig[.]\n\n\nWow, I hope Craig is not an editor!"
    author: "Hank"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-28
    body: "[w]right should be write.  :-)\n\nSentance spacing in HTML does not matter.  It is always a single space when rendered."
    author: "Tick"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-28
    body: "Doh, anyways sentance is incorrect."
    author: "Hank"
  - subject: "Don't be too quick to criticize...."
    date: 2001-03-29
    body: "Hank, I must say that you've got a few things to learn yourself, when it comes to writing skills.\n\nStarting a sentence with 'remember' is completely correct. There also was no need to insert a 'that' in the second sentence - too wordy. \"right\" should have been \"write,\" not \"wright\" as you edited it. And, sentences are no longer separated by two spaces. The correct way is to use one space -- and you misspelled 'separated.'\n\nSo (it's ok to start a sentence with so, by the way) stop harassing the writers! :-)\n\nTina"
    author: "Tina Gasperson"
  - subject: "Re: Don't be too quick to criticize...."
    date: 2001-03-29
    body: "Re: the sentence thus:\n\n  \"It doesn't take very long to [w]right up a[n] application review[,] or an opinion article[,] or something like that.\",\n\nshould likely have been formed:\n\n\"It doesn't take very long to [write] an application review, opinion article, or something similar.\"\n\nThere was no need for 'up' or 'or's.\n\nIn addition, I would like to ask when the two spaces separating sentences was dropped to one, and by whom ?\n\nJason"
    author: "Jason Hanford-Smith"
  - subject: "Re: Don't be too quick to criticize...."
    date: 2001-03-29
    body: "Hi Jason,\n\nDouble spaces between sentences is a rule that hails back to the days of typewriters. Now that we all have computers, which are basically typesetting machines, one space is better, and while not universally accepted, it is quickly growing in acceptance. Adobe has done a pretty good job of explaining it:\n\nhttp://www.adobe.com/print/columns/felici/20001030.html \nScroll down about half way to \"Why space-space is a no-no.\"\n\nTina"
    author: "Tina Gasperson"
  - subject: "Re: Don't be too quick to criticize...."
    date: 2001-03-30
    body: "Thanks for the heads-up. I'll try (nay *struggle*) to modify my computer-based output to reflect this!"
    author: "Jason Hanford-Smith"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "You could be an editor and check peoples righting."
    author: "John"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "Good idea first i'll use my write hand and blacken your I."
    author: "Craig Black"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "You could be an editor and check peoples righting."
    author: "John"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "Come on people, at least Craig is in their and trying to contribute, so what if he doesn't take the time to spell and grammer check his postings to a feedback forum.  Given the incredibly poor grammer and spelling of most posts by most people, it's hardly worth ganging up on someone that is actually helping the cause.  I'm sure whatever Craig writes for the Zine will be properly scrutinized for your protection. :)"
    author: "abc"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-28
    body: ">at least Craig is in their\nyou mean 'in there' :-)\n\nSeriously though, you're write (argh, it's contagous). Most publishing progs have spell check anyways, and that will certainly help."
    author: "Carbon"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-28
    body: "No it's contagious.  And I can't tell you what's wrong with 'prog' because I can't even guess what that is."
    author: "Hank"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-28
    body: "All of you have horrible structure to your writing."
    author: "John"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-28
    body: "At least my grammar aren't badish done like your's be. :-)"
    author: "Carbon"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "\"Remember that KDE doesn't have a figure head like the gnome project.\"\n\nSo what? Is having a figure head a bad thing?"
    author: "AC"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "Lack of a figure head to promote kde makes it that much more important for users to get involved in kde promotion. Have you noticed that the gnome project has been getting more press over the last 8 months or so? Its because they have a clear organization than kde. So as members that requires us to be more invoved.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-28
    body: "Its because they have a clear organization than kde\n\nrequires us to be more invoved.\n\nIn case you wondered, the \"Preview\" button is just to the right of the \"Add\" button."
    author: "Hank"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-28
    body: "I'll debate ideas."
    author: "Craig Black"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "Well, out of interest I decided to read the archives from the mailing list. Suprisingly enough, apparently you cannot access them unless you are subscribed. \n\nhttp://master.kde.org/mailman/listinfo/kde-zine/ says: \"The current archive is only available to the list members.\"\n\nIs there a good reason for this, or is this just something that hasn't been changed yet in a new project? I'd really like to browse for some details, but don't want to subscribe necessarily yet. Not a great way to promote interest.\n\nErik\n\nP.S. Some details can be located at http://master.kde.org/pipermail/kde-promo/2001q1/thread.html"
    author: "Erik"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "> Suprisingly enough, apparently you cannot access\n> them unless you are subscribed. \n\nThis is intentional.  Subscriptions also have to be approved.  The thought was that people should be able to express themselves freely, without regard to what employers/competitors/etc. might think.\n\nI find this very positive.  Editorial discussions should not be part of the public record at the risk of curtailing free and open debate.  The dot editorial discussions aren't even archived, and this privacy has ensured a healthy level of debate that would never happen with a public archive."
    author: "Dre"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "> Editorial discussions should not be part of \n> the public record at the risk of curtailing \n> free and open debate. The dot editorial \n> discussions aren't even archived, and this \n> privacy has ensured a healthy level of debate \n> that would never happen with a public archive.\n\nI may be missing something, but I really have to disagree with your contention. I see no correlation between archives and restriction of speech. If one fears punishment from others, would one not simply post or email anonymously? My employer can read the dot whether or not archives are available, and I may be mistaken but I believe that the kde-zine mailing list is open as well. \n\nSaying that the editorial discussions should not be part of the public record I also feel is wrong. Everything which happens in public decision making bodies in the U.S., be it Congress or Courts is put on open record for the examination of all. It is that way for a reason. Restricting access to archives simply adds an extra layer in attempting to access information on why decisions are made, and simply leads to the rehashing of old debates. Before posting to a list, I always look through archives for answers to questions or old and relevent debates. I don't think these things should go on in secret, in our open and community-driven system, unless it is absolutely necessary and beneficial. I don't really see where this is either.\n\nErik"
    author: "Erik"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "> I may be mistaken but I believe that the \n> kde-zine mailing list is open as well. \n\nSorry, I didn't read your post thoroughly. I do understand restriction of archives if subscriptions are restricted as well, though I still don't necessarily agree with it.\n\nErik"
    author: "Erik"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "I think its great that the creation of the promo-group is bringing results. I was quite surprised, though, that it would be a print-magazine. This brings me to a few questions:\n\nWho is the target-audience? Surely not the coder, and probably not the avid-user, right?\n\nIf the KDE Zine will be available online, why would one buy a printed version? I think the only answer to that could be \"Because reading all that stuff on your screen isn't fun\", which of course requires a lot of content. The printed copy doesn't have any other advantages I could think of, seeing the online version would probably feature the whole archive of issues, forums etc...\n\nOf course, that won't keep me from wishing you all the best in terms of success!"
    author: "me"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "Well, i haven't gotten this one totally straight, and I broke my computer, so can't check my e-mail, BUT, to the best of my knowledge it will be packaged with another magazine...  Much the way that Linux Journal occasinally comes with supplements...  And it will must be a monthly publication, for people who don't scour the inernet for KDE news, to write about a new KDE release, a review of software... And \"such\"..."
    author: "Nicholas Robbins"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "Why does the html validator link at zine.kde indicate that the webserver runs on IIS and Windows? Is that a mistake by the validator?"
    author: "Capt Pungent"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "HEAD http://zine.kde.org/ HTTP/1.0\n \nHTTP/1.1 200 OK\nDate: Mon, 26 Mar 2001 21:55:38 GMT\nServer: Apache-AdvancedExtranetServer/1.3.12 (Linux-Mandrake/30mdk) PHP/4.0.2 DAV/1.0.2 mod_ssl/2.6.6 OpenSSL/0.9.5a ApacheJServ/1.1.2\nLast-Modified: Thu, 22 Mar 2001 00:31:00 GMT\nETag: \"2b1dc-65b-3ab947c4\"\nAccept-Ranges: bytes\nContent-Length: 1627\nConnection: close\nContent-Type: text/html"
    author: "KDE User"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "> Why does the html validator link at zine.kde indicate that the webserver runs on IIS and Windows? Is that a mistake by the validator?\n\nOr by you. I'm showing it running Apache on Linux Mandrake which is consistent with other pages like the webcvs which posts the Mandrake logo as they donated server space."
    author: "Eric Laffoon"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "From validator.w3c.org:\n\n\nServer: Apache-AdvancedExtranetServer/1.3.12 (Linux-Mandrake/30mdk) PHP/4.0.2 DAV/1.0.2 mod_ssl/2.6.6 OpenSSL/0.9.5a ApacheJServ/1.1.\n\nperhaps you read the wrong field? or the validator just puked for you?"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "This is what I get when I run\nHEAD http://zine.kde.org/ HTTP/1.0\n\n200 OK\nDate: Tue, 27 Mar 2001 05:03:08 GMT\nVia: 1.1 cache-har (NetCache NetApp/5.0D14)\nServer: Apache-AdvancedExtranetServer/1.3.12 (Linux-Mandrake/30mdk) PHP/4.0.2 DAV/1.0.2 mod_ssl/2.6.6 OpenSSL/0.9.5a ApacheJServ/1.1.2\nContent-Length: 1627\nContent-Type: text/html\nETag: \"2b1dc-65b-3ab947c4\"\nLast-Modified: Thu, 22 Mar 2001 00:31:00 GMT\nClient-Date: Tue, 27 Mar 2001 06:02:58 GMT\nClient-Peer: 64.252.95.56:80\n \n404 Object Not Found\nDate: Tue, 27 Mar 2001 05:03:10 GMT\nVia: 1.1 cache-har (NetCache NetApp/5.0D14)\nServer: Microsoft-IIS/4.0\nContent-Length: 1090\nContent-Type: text/html\nClient-Date: Tue, 27 Mar 2001 06:02:59 GMT\nClient-Peer: 213.160.64.61:80\n\nSo what's happening here?"
    author: "Dave"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "The first one is the webserver message.... the second one is the proxy message.\n\ncya. markus"
    author: "Markus"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "Christopher Molnar is responsible for PDA-interface, koooool.\nI can read it with Konqueror in embedded Qt?"
    author: "reihal"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "There will be an HTML edition, so you'll be able to read it under Konqueror or any HTML browser.\nPDA-interface is for PalmPilot (and similar) formats."
    author: "aegir"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-27
    body: "There will be an HTML edition, so you'll be able to read it under Konqueror or any HTML browser.\nPDA-interface is for PalmPilot (and similar) formats."
    author: "aegir"
  - subject: "Re: KDE Community Launches New KDE Zine Project"
    date: 2001-03-28
    body: "Cool,\n\nI hope I will be able to get it in the uk. Will it be sold in the shops or subscription only?"
    author: "Mark Hillary"
---
The KDE community, made up of <a href="http://promo.kde.org/zine.php">developers and avid users</a>, has a new project: the <a href="http://zine.kde.org/">KDE Zine</a>. Just a few short weeks ago, discussion about the possibilities of creating a print magazine cropped up on the <a href="http://promo.kde.org/">KDE-promo</a> mailing list. Today, the discussion has spawned its own <a href="http://master.kde.org/mailman/listinfo/kde-zine/">mailing list</a>, the beginnings of a Web site located at <a href="http://zine.kde.org/">zine.kde.org</a>, and a small group of <a href="http://promo.kde.org/zine.php">volunteers</a> who are busy working out myriad details involved in bringing a print publication to life.  Here's your chance to <a href="http://master.kde.org/mailman/listinfo/kde-zine/">get involved</a>. More on <a href="http://www.newsforge.com/article.pl?sid=01/03/25/2128205">Newsforge</a>.

<!--break-->
