---
title: "People of KDE: Carsten Pfeiffer"
date:    2001-02-20
authors:
  - "Inorog"
slug:    people-kde-carsten-pfeiffer
comments:
  - subject: "Re: People of KDE: Carsten Pfeiffer"
    date: 2001-02-20
    body: "Klipper is one of the best and most useful KDE utils ever.  It simply rules!"
    author: "KDE User"
  - subject: "HAHAH"
    date: 2001-02-20
    body: "Coolo's quote at the end Kracks me up! hee hee.\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "The most important question!"
    date: 2001-02-20
    body: "Why \"gis\" ?"
    author: "Otter"
  - subject: "Re: The most important question!"
    date: 2001-02-20
    body: "Why \"Otter\" ? ;)"
    author: "anonymous"
  - subject: "Re: The most important question!"
    date: 2001-02-20
    body: "Carsten == Global Information System\n\n;-p"
    author: "KDE User"
  - subject: "Re: The most important question!"
    date: 2001-02-20
    body: "gis is just short for gismoe, a nickname I've had for a long time (remember the movie Gremlins?). No, I don't behave weird when eating something after midnight :)"
    author: "gis"
  - subject: "Re: The most important question!"
    date: 2001-02-20
    body: "Heh, we all know that \"gis\" short for \"Gisela\".\nIs it it that what happens to you after midnight?\n>;)\n\nCheers,\n\nDaniel"
    author: "Daniel Molkentin"
  - subject: "Re: The most important question!"
    date: 2001-02-20
    body: "<i>Heh, we all know that \"gis\" short for \"Gisela\". Is it it that what happens to you after midnight? >;) Cheers, Daniel</i><br><br>\nNo, but actually I do behave a bit weird around midnight, because that's when that !#*&% SuSE cronjob starts to scan my entire harddisk, basically killing anything else you're currently doing ;)"
    author: "gis"
  - subject: "Re: The most important question!"
    date: 2001-02-21
    body: "Hehe ;-)<br>\n<br>\nHi Carsten :)<br>\n--<br>\nLukas"
    author: "Lukas Tinkl"
  - subject: "Re: The most important question!"
    date: 2001-02-21
    body: "Hi Lukas,\n\nyes, I did notice your new @suse.cz address :)\nCongrats! BTW, the midnight-cronjob seems to be fixed in SuSE 7.1, at least it seems to be not that annoying anymore."
    author: "gis"
  - subject: "Re: The most important question!"
    date: 2001-02-21
    body: "why not try:\nhdparam -c1d1 /dev/<hardrive>\nmight at least make the suffering shorter :)\nIt makes a huge diffrense on my box.\nDon't work with scsi but i don't\n think you have it :)"
    author: "molgan"
  - subject: "Re: The most important question!"
    date: 2001-02-21
    body: "should be /dev/\"harddrive\"<br>\nit seems to be an error in the tag validation code,<br>\nthe last post broke something :)"
    author: "molgan"
  - subject: "Re: The most important question!"
    date: 2001-02-21
    body: "Yes, I have those settings already... They had quite a positive effect, but the cronjob still sucked."
    author: "gis"
  - subject: "Re: The most important question!"
    date: 2001-02-21
    body: "Well, that's SuSE geek protection. Take it serious and go to bed ;)"
    author: "Daniel"
  - subject: ".emacs"
    date: 2001-02-20
    body: "Hi Carsten, please share your .emacs with us ;-))"
    author: "Henning"
  - subject: "Re: .emacs"
    date: 2001-02-20
    body: "It's now at <a href=\"http://master.kde.org/~pfeiffer/emacs-stuff.tgz\">http://master.kde.org/~pfeiffer/emacs-stuff.tgz</a> (containing a dir with .emacs and .xemacs-options for those interested)."
    author: "gis"
  - subject: "Kuickshow"
    date: 2001-02-20
    body: "Kuickshow is the only KDE 1 program I still use. I am eagerly awaiting the new release..."
    author: "Claus"
  - subject: "Re: Kuickshow"
    date: 2001-02-21
    body: "If you want to test-drive the KDE2 version, check http://master.kde.org/~pfeiffer/, there's a preliminary version that does sort of work. Some parts are non-functional tho, e.g. the config-dialog.\n<br><br>\nA fully working version should be available in 2-3 weeks."
    author: "gis"
---
If you're using klipper, the enhanced KDE clipboard manager, or if you have noticed Konqueror's excellent history support, then you already know a bit of who <a href="http://www.kde.org/people/carsten.html">Carsten Pfeiffer</a> is and what he does for KDE. He answers the questions this week in Tink's popular interviews series with the <a href="http://www.kde.org/people/people.html">People of KDE</a>. Enjoy!

<!--break-->
