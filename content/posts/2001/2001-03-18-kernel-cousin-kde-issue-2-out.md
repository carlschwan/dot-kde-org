---
title: "Kernel Cousin KDE Issue #2 is Out"
date:    2001-03-18
authors:
  - "numanee"
slug:    kernel-cousin-kde-issue-2-out
comments:
  - subject: "Re: Kernel Cousin KDE Issue #2 is Out"
    date: 2001-03-18
    body: "This guys really sum things up :)\n\nI'm subscribed to almost everyone of the kde mailinglists, but somehow I missed two of the points listed on KC KDE, thx for the help guys!\n\nKeep up the good work :)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Kernel Cousin KDE Issue #2 is Out"
    date: 2001-03-18
    body: "Yeah, I know.   Same here.  It gets harder and harder to follow all the lists...\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Kernel Cousin KDE Issue #2 is Out"
    date: 2001-03-18
    body: "Kudos! KC KDE is terrific.\n\nWe should bug O'Reilly to print yearbooks of all available KC's. Wouldn't it be great to have the development history of the Linux kernel, KDE, etc available in book form?"
    author: "Rob Kaper"
  - subject: "OFFTOPIC: Queue Submission regarding latest KT"
    date: 2001-03-18
    body: "This was submitted to us as an article, but since we're probably not going to post it as a full article, I'm mentioning it here for kicks.  Amusing, but I hope it's not too flame inducing... ;)\n<p>\n<b>Author: pbkg</b><br>\n<b>Subject: Linus and KDE</b><br>\n<b>Dept: ha-ha-just-as-i-always-thought</b><br>\n<p>\n<i>\nJust thought you might be interested in this <a href=\"http://kt.zork.net/kernel-traffic/latest.html#3\">quote</a> from Kernel Traffic #111 for the 6th of March. More specifically, section 3 and the quote from Linus, in the third paragraph.... \n</i>\n"
    author: "Navindra Umanee"
  - subject: "WHOOAAAA...."
    date: 2001-03-18
    body: "Linus runs KDE.\n\nThat just sums it up. \n\nps.  uninstalling all G related thing as u'r reading :)"
    author: "me"
  - subject: "Re: OFFTOPIC: Queue Submission regarding latest KT"
    date: 2001-03-18
    body: "See his <a href=\"http://www.uwsg.indiana.edu/hypermail/linux/kernel/0103.0/0112.html\">email</a> and I quote:\n<i>\"I had something like six missed merges while running my normal set of applications (X, KDE etc).\"</i>\n\n"
    author: "ac"
  - subject: "Re: OFFTOPIC: Queue Submission regarding latest KT"
    date: 2001-03-18
    body: "Yep, I read this on kernal traffic, too\nBut Alan Cox is involved in GNOME. So, our \"Linux kernel heros\" behave in a political correct way ;-)\n\nBTW: I use KDE for my programming tasks only and it's my desktop, but it is also quite interesting, (inspirational, sometimes amusing) to read the GNOME-mailing lists via archive.\n\nOur (KDEs) opponent is M$ - GNOME is our competitor.\n\nGo Linux"
    author: "Thorsten Schnebeck"
  - subject: "Re: Kernel Cousin KDE Issue #2 is Out"
    date: 2001-03-18
    body: "The scan integration into koffice is truly awesome. The developers are my hero's. I hope icon anti aliasing is on someones radar screen for 2.2. A movie and sound editor would be very cool as well. I'm sure someone will get around to it eventually though.\n\nCraig"
    author: "Craig black"
  - subject: "Re: Kernel Cousin KDE Issue #2 is Out"
    date: 2001-03-18
    body: "AA is already in KDE 2.1 - if you use Qt 2.3 or Qt 2.2.4 with the AA patch.  Oh yeah, and you need XFree86 4.0.2 at least...\n\nTroy\ntroy@kde.org"
    author: "Troy Unrau"
  - subject: "Re: Kernel Cousin KDE Issue #2 is Out"
    date: 2001-03-18
    body: "I have AA fonts with qt 2.3 and kde 2.1 but i only have anti aliased fonts, but i'd like to see it for icons as well.\n\nCraig"
    author: "Craig black"
  - subject: "Re: Kernel Cousin KDE Issue #2 is Out"
    date: 2001-03-18
    body: "Do you mean an AA-version of QPainter? KIllustrator with an AA-drawing mode for lines, circles... Would be nice :-)"
    author: "Thorsten Schnebeck"
  - subject: "Re: Kernel Cousin KDE Issue #2 is Out"
    date: 2001-03-18
    body: "I was just thinking about my desktop icons. My font is AA but my desktop icons are not.\n\nCraig"
    author: "Craig black"
  - subject: "Re: Kernel Cousin KDE Issue #2 is Out"
    date: 2001-03-19
    body: "How would anti-aliased desktop icons work?  Do you mean the little shadows that you can enable in Kicker?  Or do you mean transparent desktop icons that you can see the wallpaper through?  Or do you mean something else?"
    author: "not me"
  - subject: "Re: Kernel Cousin KDE Issue #2 is Out"
    date: 2001-03-19
    body: "> I was just thinking about my desktop icons. My font is AA but my desktop icons are not.\n\nClick Look&Feel -> Icons in KControl and choose \"Blend alpha channel\" for the \"Desktop/Filemanager\" and \"Panel\" entry. Also you might want to choose 48x48 icons for the desktop and large icons for the panel. As for true antialiasing through scaling: It's already part of KDE since version 2.0 it just doesn't make much sense using 48x48-icons. You can enable it though by playing around in the index.desktop-files in your icontheme (\"hicolor\" e.g.). You need to replace \"fixed\" by scalable to make this happen.\n\nGreetings, \nTackat"
    author: "Tackat"
---
This week in <a href="http://kt.zork.net/kde/latest.html">KC KDE</a>, you can read all of 14 topics covering <a href="http://www.real.com/">RealPlayer 8</a> and KDE2 integration,  HTML form completion in <a href="http://www.konqueror.org/">Konqueror</a>, the new KDE2 scanner library and GUI, SDI vs MDI, <a href="http://www.millenniumx.de/">kISDN</a> and kppp integration, and more. Go get it <a href="http://kt.zork.net/kde/kde20010316_2.html">here</a>.




<!--break-->
