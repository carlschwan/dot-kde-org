---
title: "KDE Print Presentation"
date:    2001-07-14
authors:
  - "Inorog"
slug:    kde-print-presentation
comments:
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-14
    body: "And I really love the print to fax plugin. Great stuff !"
    author: "Pyre"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-14
    body: "Could use a config util for print to remote smb/windows printer though"
    author: "Pyre"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-15
    body: "It's ther. Didn't you see it?"
    author: "me-the-cups-fan"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-16
    body: "Being able to create faxes easily is one of the killer apps left for me in Windows.  Now if someone could rewrite kvoice for KDE2 and we'd have faxing worked out perfect"
    author: "Simon"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-14
    body: "Could you add a coffe machine in Konqueror, please? It will make me feel completely happy :)"
    author: "ViRgiLiO"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-15
    body: "We must DEMAND RFC2324 compliance!  For too long have internet browsers omitted this very important URI scheme.  Hyper Text Coffee Pot Control Protocol is the wave of the future, I tell you!"
    author: "Chad Kitching"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-15
    body: "... and Konquis nice cookie support makes it perfect :-)"
    author: "justforfun"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-15
    body: "I know the new printing architecture in KDE 2.2 is not out yet, that there are a few missing bits in it (i.e. lack of support for some TCP/IP-based print spooling mechanisms, IIRC) and that this should be addressed for KDE2.3. I'm happy to see that, if KDE2.3 comes out before the end of the year and if all goes as planned, TCP/IP-based printing will be quite well supported in 2001. We are no longer talking about a vapor-ish schedule, no \"next year\" or whatever, this stuff is happening NOW, THIS YEAR!\n\nBut after reading this other gentlemen's comment, I'm now realizing that KDE will also need to support Windows/SMB printing as well as TCP/IP printing, all in the same timeframe.  Maybe Novell NetWare-based printing should also be looked into -- hello, Novell, want to contribute to The Cause (TM)?\n\nAnyway, my point is that if we want *nix/KDE to take over The Desktop, we need to be able to play well with what's out there already. This, want it or not, includes a lot of NT and some Novell.  The day a *nix/KDE box can be dropped into an existing LAN and make use of existing resources (file sharing, printing and e-mail/messaging) with minimal effort will the beginning of the True Rise of the *nix Desktop."
    author: "Anonymous Coward"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-15
    body: "ugh ? it ain't vapourware. i installed kde 2.2 beta1 , i have cups installed, and all i had todo to use my neighbours smb printer was click on new printer, choose everything (even his pc) from a list and i was up and running. (its integrated into kcontrol). I also can use ipp printers, remote LPR, serial, parallell and with the 'ext' plugin _every_ print system that linux supports. and it works really nice and well. here is how to do it in debian:\napt-get install cupsys cupsys-* cupsomatic-ppd kdelibs3-cupsys"
    author: "ik"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-15
    body: "It *already* supports smb/windows-based printing?  Whoa. \n\nAny known shortcomings, though? (This would be perfectly normal, kdeprint is still  fairly new, after all...)"
    author: "A.C."
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-15
    body: "CUPS supports smb printing, and kdeprint probably just piggybacks on that.  So you will need CUPS installed for it to work."
    author: "Justin"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-15
    body: "which is perfectly okay, kdeprint does not try to be a print spooling system... why duplicate existing functionality we can just use ...\n\nbtw, the kcontrol cups applet is a great frontend, looks way better than the older 'printtool' and is way more functional, and you can simply browse for your smb printer in a 'network neighbourhood'"
    author: "ik"
  - subject: "Print Preview"
    date: 2001-07-15
    body: "Does it do print preview like GNOME print, which is totally cool"
    author: "Noi"
  - subject: "Re: Print Preview"
    date: 2001-07-15
    body: "Yes."
    author: "Timothy R. Butler"
  - subject: "Print preview"
    date: 2001-07-15
    body: "Ther is a print preview option and it opens kghostview, which lets you zoom etc.."
    author: "ac"
  - subject: "Re: Print preview"
    date: 2001-07-16
    body: "Is it possible to change this to open ghostview instead?  (I don't need the graphics module installed except for kghostview at the moment)."
    author: "another ac"
  - subject: "Congratulations"
    date: 2001-07-15
    body: "The combination of CUPS and KDE Print is now superior in pretty much every way I can think of to the Windows and Mac print systems - except one.\n\nColour matching appears to be absent.\n\nWhat's the score on that?  I strikes me it shouldn't too difficult - as long as the low-level print drivers implement Postscript colour matching and understand sRGB.\n\nI notice liblcms, a colour matching system, is one of the dependencies of KDE - where's it used?\n\nIt's a key feature to have if KDE is ever going to become the desktop of choice for graphics people, so why not get it right now, whilst KDE Print is young, rather than bolting it on later and making a bit of a mess of it (a la Windows)?"
    author: "Amazed of London"
  - subject: "F%&^king Printer/Prepress companies"
    date: 2001-07-15
    body: "have locked up and **PATENTED** various color description schemes. Yes they have patented a method for describing color. Granted it tooks some work and all and they need to profit on their investment, but they plan on milking dough out of their patent on color for years to come.\n\nThe only way forward for linux/bsd etc. is to leverage friendships with IBM (which has **excellent printer support for Linux and Unix the Lexmark E-312 rocks!) and maybe Apple (they may like the idea of paying less for licenses to print color and MAKE FRIENDS WITH KINKO'S (one of the largest printers in the world and a big user of color matching and print servers - they might like the idea of a cheap linux based print server that has decent color matching with existing 50K printers) in order to create a competing standard.\n\nThe International Color Consortium was supposed to work out some kind of scheme for printer manufacturers (a kind of POSIX for color) but it's usually easier for them to buy commercial (closed and patented) color matching methods. Apparently if Linux or Ghostscript came up on it's own with a way of matching its own colors to the print color scheme of the existing commercial suppliers it would violate the patent. Color is so blatently obvius that it is impossible to reverse engineer any \"protocol\" and create a competing standard compatible with the existing ones ...\n\nthe bastards ... see http://www.color.org/"
    author: "Mad as hell"
  - subject: "Re: F%&^king Printer/Prepress companies"
    date: 2001-07-15
    body: "What countries would recognise this patent? Would it be possible to locate a specific module for color matching on a set of machines that are out of reach of this patent?\n\nOf course I would not want to advocate giving a bad name to KDE (or OSS in general) as being disrespectful of law and society (that would sure be some nice material for some prop. vendors), but on the other hand you just feel the humongous stupidity of the whole situation that you can't implement something that is (as I understand it) quite simple.\n\nIt reminds me of some of the mp3 patents Ogg/Vorbis had to deal with. IIRC mp3 had patented some almost basic mathematical ways of computing something, so Ogg needed to implement those in a totally different and non-obvious was just to avoid the patent.\n\n-- \nMatthijs"
    author: "Matthijs Sypkens Smit"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-15
    body: "Another impressive thing is that this online pres was created and generated entirely with KPresenter, and that isn't even stable yet either!"
    author: "Carbon"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-15
    body: "Actually it is, KDE people have been using KPresenter for presentations at conferences and such (and putting them on the web afterward, just like that one) for *years* now.  I remember seeing some of Kurt Granroth's KPresenter presentations posted on the web 2 years ago."
    author: "David Walser"
  - subject: "RFC: Pamphlet printing"
    date: 2001-07-15
    body: "Hi!\n\nOne thing I liked when using my prior wordprocessor is a special printing mode called \"pamphlet printing\" (PP). This was very useful but it is a little bit tricky to do it right. PP means: you have e.g. 2 sheets of A4 paper (landscape) one on top the other, now you fold this in the middle and you have a little A5 pamphlet of 8 pages. If you watch how to print these 8 A5 pages on the 2 A4 sheets you see the problem:\n- Zooming (optinal) to adjust a document to the printing size (in this example: A5)\n- Before the printer starts the driver needs the number of pages, the number of document pages  has to be a mutiple of 4 (four pages on one sheet), so an adjustment with empty pages is needed. \n- Printing has to be divided in Top/Bottom printing parts. In my 2 page example this would be:\ntop printing\nsheet one: doc pages 2,7 (left, right)\nsheet two: doc pages 4,5 (l, r)\n\nthan printing stop, user feeds the printer for bottom printing:\nsheet one: doc pages 8,1 (l, r)\nsheet two: doc pages 6,3 (l, r)\n\nAs I said, its a little bit tricky :-)\nNow, is it posssible to put this in the kprinter framework (so every KDE app can print as descriped or whould this be part of a e.g. KOffice kprinter-customization?\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: RFC: Pamphlet printing"
    date: 2001-07-15
    body: "Have a look at this:\nman:/psbook\n\nI guess it would be easily possible to add a filter and GUI to the kde printing system, that uses this functionality."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: psbook"
    date: 2001-07-15
    body: "Hmm, but this is PostScript-only, or? I want to use gimpprint cups-modules for my new colour-printer - and I think this driver does not generate any PostScript?!"
    author: "Thorsten Schnebeck"
  - subject: "Re: psbook"
    date: 2001-07-15
    body: "Printing on unix is always Postscript. The gimp-print printers just trabslate the Postscript to the Printer languages.\n\nBasically the way goes\n\n[Application]\n    |\n    | (postscript output)\n[Print filter] \n    |   - this may be gimp print, what ever\n    |\n[Spool System] (lpr, cups, lprng,...)\n    |\n[Printer] \n\nYou can easily see, that print filter and spool system belong together, it it no problem to generate a brochure with psbook and then hand it to gimp-print."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: RFC: Pamphlet printing"
    date: 2001-07-15
    body: "no need to code if you want to use psbook. just create an instance of your printer, go to the filter tab and add a psbook command line with the right options to it ..."
    author: "ik"
  - subject: "Re: RFC: Pamphlet printing"
    date: 2001-07-15
    body: "Thanks for the info! Found it :-)\nNow the problem is, psbook needs the number of pages (-s option) and I don't know this if I print e.g. with kate. And then, how to print  first top then bottom?"
    author: "Thorsten Schnebeck"
  - subject: "Re: RFC: Pamphlet printing"
    date: 2001-07-16
    body: "seems like a question for Michael...  i think a system to configure/set parameters of external filters would be extremely usefull ..."
    author: "ik"
  - subject: "Re: RFC: Pamphlet printing"
    date: 2001-07-16
    body: "No, you don't necessarily need the number of sides. If you don't specify anything, psbook will calculate it for you, and this \"default\" value is usually what you want.\n\nIn the current structure, the easiest way to do pamphlet printing is to define 2 filters: one for odd pages, and the other for even pages. What the user would have to do then is:\n- print with odd page filter: basically what it would do is \"psbook infile.ps | psnup -2 | psselect -o > outfile1.ps\"\n- get the pages and put them again in the printer input slot (in the right direction)\n- print with even page filter: \"psbook infile.ps | psnup -2 | psselect -e > outfile2.ps\"\n\nWriting those 2 filters is an easy task, and I can make it available for 2.2 release. Of course this is not \"integrated\" in the sense that everything is done automatically. I'll see what I can do for an integrated solution in future development.\n\nMichael."
    author: "Michael Goffioul"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-15
    body: "Why the dont add to the KDE system the CUPS package? I mean, if KDE Print uses it, and is a front-end for it, i think it has to be with the KDE whole package .. i think :-)"
    author: "Daniel"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-16
    body: "So if KDE uses X11 , QT whatever, it has to be in the KDE package?\n\nWe have distributions for this. If yours doesnt come with CUPS then\na) compile & install it yourself\nb) switch to a different one"
    author: "ac"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-18
    body: "It would be cool to see an alternative organizational destribution to Debian, perhaps putting KDE at the center, but I can see why that should definately be a seprate and non-KDE-endorsed project.<P>\n\nKDE is all about features, ease of use, performance, etc.  I like how they leave the philosophy to the user, OSS, and GNU, and how KDE is purely a software project to improve computing that lets anyone who agrees to share in on the modification and distribution."
    author: "Gilded Rooster"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-15
    body: "I'm wondering what's the current status on LPRng support (as in RedHat 7.1)?"
    author: "Carlos Rodrigues"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-16
    body: "It's on its way. But won't be available for coming 2.2 release. Only some basic support that will allow you to print. No management.\n\nMichael."
    author: "Michael Goffioul"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-16
    body: "I would love an easy admin for priters that counts pages, I know cups can do it (but getting the right filter can be a bit of trouble).\n\nIt would be good for options such as get page count from printer via parallel, TCP/IP, or giving options as using ghostscript to count pages. You need these options to cater for all printers. I have a HP 6P printer connected via parallel and am trying to get the page count feature working on printer, all to no avail yet. I know of a ifhp (input filter for hp) but that is built for lprng system and not cups.\n\nThis would be great for people trying to set there computer as a print server (even though you probably shouldn't use X windows system in the first place).\n\nBest of luck with adding features..\n \nAlso: does anybody know how to use the script feature for Koffice, and will they be allowing auto record macros like Microsoft Office (allways find that very simple to get the basic macro written)..\n\nCheers"
    author: "Bradley Crouch"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-16
    body: "iirc, only kspread at the moment supports scripting"
    author: "Carbon"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-17
    body: "CUPS is doing the \"print account\" by passing nearly every job through a \"pstops\" filter which  does the page counting. Output of this filter there may be piped into other filters (like pstoraster --> rastertopcl) or send to the printer directly (if it is a PostScript printer). In any case, this works for network, parallel, serial or USB printers the same.\nFor pstops to work, it needs DSC, \"Document Structuring Convention\" compliant PostScript (or near-equivalent) as input. So it calculates the pages during filtering on the print server und writes info about any page (what time, which user, which job-ID and -name, which printer,  how many copies of which pages of the document, how many kilo-bytes?) into \"/var/log/cups/page_log\". (By the way: on my personal \"wishlist\" is a hack of \"webalizer\" to read and analyse the page_log and give a similar output. Anyone?)\n<p>\nHowever, it is *not* giving correct results  in the following cases:\n<UL>\n<LI>-- the printer jams and maybe therfore throw away the job (real live experience; or maybe throwing away the job because of problems with the data format)</LI>\n<LI>-- jobs printed as \"raw\" are always counted as size of 1 page (and maybe mutliple copies). If CUPS works as a print server for Windows clients using the original native Windows driver for the target print device,  this guarantees the correct formatting of the job on the clients already; therefore the server should not touch it and print \"raw\"; therefore no filtering is started (and is not possible as the input from the Clients is not PostScript as <I>pstops</I> expects;  hence no pagecount other than \"1\"...</LI>\n<UL>\nTherefore the page accounting of CUPS is \"only\" an approximation (in many cases an excellent + good one, in others a quite poor one)\n<p>\nthe only reliable print count is the one done by the internal printer counter. (Because this is the one you pay for, if you are on a \"click price\" or similar.) Some, by far not most, printers can be queried remotely for that information via SNMP (Simple Network Management Protocol). That means, in a bigger network with many different printers there *is* just no completely reliable and accurate page accpunting tool!"
    author: "Kurt Pfeifle"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-17
    body: "Sorry a very dumb question..\n\nUnfortunately my cups config does not create a page count. Where do I put the pstops filter, I take it is not in the printcap file? For a very simplistic approach what line and where do I put the line to specify pstops as a filter?\n\nThanks"
    author: "Bradley Crouch"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-17
    body: "You don't need to do this manually. Every CUPS installation will put all its needed filters into \"/usr/lib/cups/filter/\". There you'll find pdftops, pstoraster, imagetoraster, rastertoepson, imagetops, rastertohp etc. It also will need no further configuration. It recognises the handled files as MIME types and pipes through a filter chain accordingly. In the PPD [=(PostScript) Printer Description file] which is part of the printer \"driver\" it is defined which one as the last filter (which produces the output to be send to the target printer) should be used.\n\nSame goes for printcap: CUPS does not need it. However, some old-moded, legacy applications (like pre-KDE 2.2 ;-) do need it. They just don't print if they can't find a printcap file wkith printer names in it. But CUPS is very forgiving for this: it creates a \"dummy\" printcap with just its printer names in it for those old programms which have a hard-coded (non-configurable) \"lpr\"  as their only print command. To tell CUPS to create such a file, just make sure you have a line \"PrintcapName /etc/printcap\" in your /etc/cups/cupsd.conf\" (Don't trust the comments about its \"default\" setting. To be sure, just uncomment the above quoted line.\n\nFind more docu on CUPS at 2http://www.cups.org/documentation.html. It's excellent and it is complete. Just don't think, CUPS is \"an old wine in new hoses\". Yes, CUPS is quite easy to handle, once you're familiar with it. But also: yes, CUPS is new. And therefore you have to learn new things..."
    author: "Kurt Pfeifle"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-18
    body: "Thanks for the reply...\n\nI tried installing a new Postscript printer, using the generic postscript file and created a print to file printer. This worked, it logs the page count.\n\nBut when I use a HP 6P print driver it does not. I thought CUPS prints to postscript then converts to raster then prints to printer. I believe the printer is using the foomatic printer filter, reading from the log messages.... \n\nCheers\nBradley Crouch"
    author: "Bradley Crouch"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-18
    body: "Aaaah... \"foomatic\" you're using! \n\nThat means a bit of a difference. (I'm not quite sure about the details, maybe you ask in the foomatic forum at \"www.linuxprinting.org\").\n\nHere is my knowledge about it: foomatic, in its incarnation as \"cupsomatic\", is a \"third party\" development contribution to CUPS. It is a wrapper, (based on Perl-Scripts) that is able to plug any ghostscript-filter into the CUPS printing system. \n\nFor this, there are the special \"Foomatic-PPDs\" (one for each possible working printer/ghostscript-combination you might think of). The Foomatic-PPD can be choosen during printer installation as usual in CUPS. It calls the \"cupsomatic\" filter (a Perl-Script, which you also must copy into your normal CUPS-filter directory). This filter then \"hi-jacks\" your print data away from the \"normal\" CUPS filter chain and pipes it to the appropriate ghostscript filter.\n\nAnd as I don't know if this happens before or after \"pstops\" has done its work, I don't know if this is the cause of your problem. But I strongly suspect, cupsomatic is bypassing the page-counting of CUPS.\n\nFor all you who are curious: you can go to www.linuxprinting.org/foomatic.html and use the \"CUPS-O-Matic\" PPD-generator. Just download \"cupsomatic\"; then select your printer, the ghostscript-filter you want to use (there might be a suggestion for the \"best\" one there) and let Grant Taylor's CUPS-O-Matic generate the PPD for you... \"cupsomatic\" is a fantastic tool to make *all* printers working with ghostscript to work with CUPS equally well (or badly :) -- There might be \"better\" solutions for many printers now (like what the gimp-print project is doing for most modern inkjets), but cupsomatic often is the only way to make your printer go with CUPS, which then gives you the best of both worlds: the big base of ghostscript-supported printers and the nice features of CUPS (maybe without page accounting) like Web Interface, load balancing, failover, printer browsing, etc.\n\nEspecially the printer browsing is a real killer feature of CUPS:  it makes all clients, which are newly plugged into the LAN, \"Plug'n'Play\" within 30 seconds without needing to install a printer driver or configuring a printer, just by \"listening\" to the CUPS server's short messages which are broadcast every 30 (or what's configured) seconds...\n\ncupsomatic is just one member of the whole foomatic family of wrappers: besides cupsomatic there are pdq-o-matic (for PDQ), lpdomatic (for LPD) and mfomatic (for LPRnd/Magicfilter)  doing similar jobs for the named spooling systems (if you still aren not a CUPS-addict like me ;-)"
    author: "Kurt Pfeifle"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-18
    body: "for the HP 6P you might want to use the gimp-print source (from \"gimp-print.sourceforge.net/\"): download, compile (after doing \"./configure --with-cups --without-ghost --without-gimp\", if you just want the printing functionality with CUPS. As it compiles also all the CUPS-PPDs for the supported printers (this part of the code is due to Mike Sweet, chief CUPS developer), there should be support for page accounting included, as it is not bypassing pstops but part of the normal filter chain of CUPS.\n\ngimp-print has become, contrary to what the name suggests, a very broad free software project. Having started (with Mike Sweet as the originator) as the print plugin for the Gimp, it now has evolved to a project, that compiles its code base into three different incarnations:\n\n-- still the gimp print plugin;\n\n-- the \"stp\" filter (this used to stand for\n   \"Stylus Photo\"), which is compiled into a\n   standard ghostscript filter and may be \n   used the same way as any ghostscript filter\n   (that means also as a \"foomatic + stp\" one\n   inside CUPS)\n\n-- and as a standard CUPS filter, compiling\n   also all the needed PPDs (presently around\n   120 printer models are supported, amongst\n   them most modern inkjets with a real photo\n   quality), making it to work \"the standard\n   CUPS way\" as far as I know...\n\nHope this was a round-up that helps you to decide which way to go and try next..."
    author: "Kurt Pfeifle"
  - subject: "Re: KDE Print Presentation"
    date: 2001-07-18
    body: "Thanks for the reply... \n\nThis could be a good lesson to have a postscript printer.. ;^)\n\nHave asked www.linuxprinters.org about cupsomatic on their newsgroup, any luck I will get a reply there. I have also going to give the gimp driver a try, any luck it will work.\n\nCheers,\nBrad"
    author: "Bradley Crouch"
---
<a href="mailto:goffioul@imec.be">Michael Goffioul</a>, the main architect of the new KDE Print system, has posted <a href="http://users.swing.be/kdeprint/www/index.html">an HTML version</a> of the presentation he gave at <a href="http://dot.kde.org/994930585/">LinuxTag</a>. The presentation is complete with screenshots and gives a good overview of the KDE Print technology featured in KDE 2.2.  More info,  screenshots and photos are available from <a href="http://users.swing.be/kdeprint/">the KDE Print site</a>.  Michael is also <a href="http://www.kde.org/jobs/jobs-open.html">seeking help</a> in documenting the KDE Print components, both for developers and for users.  If you think you can help, please be sure to <a href="mailto:goffioul@imec.be">contact him</a>.