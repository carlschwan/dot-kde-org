---
title: "People of KDE: Gregory Mokhin"
date:    2001-01-16
authors:
  - "Inorog"
slug:    people-kde-gregory-mokhin
---
Many agree that one of the greatest strengths of KDE is the professional quality of the user interface translations and localization.  In this week's <A HREF="http://www.kde.org/people.html">People Behind KDE</A>, <a href="mailto:tink@kde.org">Tink</a> presents to us <a href="http://www.kde.org/people/grisha.html">Gregory Mokhin</a>, member of the Russian KDE translation team. With charm and a subtle sense of humor, Grisha speaks about his work and life. Many thanks to Tink for another fascinating piece of <a href="http://www.kde.org/people/people.html">KDE life</a>

<!--break-->
