---
title: "Dot Down, Master Down, Newsforge on Largo"
date:    2001-08-14
authors:
  - "numanee"
slug:    dot-down-master-down-newsforge-largo
comments:
  - subject: "bill's revenge"
    date: 2001-08-14
    body: "i bet microsoft did it."
    author: "elliott"
  - subject: "HP Liquid 0.4"
    date: 2001-08-14
    body: "In theme news, <a href=\"http://www.mosfet.org/liquid.html\">Liquid 0.4</a> is out."
    author: "KDE User"
  - subject: "Re: HP Liquid 0.4"
    date: 2001-08-14
    body: "mosfet.org appears to be running on a 28.8 modem."
    author: "ac"
  - subject: "Re: HP Liquid 0.4"
    date: 2001-08-14
    body: "Mosfet, better update http://www.mosfet.org than repeating here what was written long ago in the apps.kde.com ticker."
    author: "someone"
  - subject: "Clueless"
    date: 2001-08-15
    body: "Huh? I keep mosfet.org updated with the latest Liquid version. As for repeating things here I wasn't the one to make the original post... but I can't help it if people like Liquid ;-) I use my own account when posting here, even when flaming people :P"
    author: "Mosfet"
  - subject: "Re: Clueless"
    date: 2001-08-15
    body: "mosfet.org still says \"News 07/25: High Performance Liquid Version 0.3 is available\" as latest news."
    author: "somone"
  - subject: "Re: Clueless"
    date: 2001-08-15
    body: "I keep the Liquid page updated with the latest version info. I don't work much on the index page, as I have better things to do like actually code. What have you done lately?\n\nI still didn't make the original post so your comment's still a stupid troll, Mr. Anonymous ;-)"
    author: "Mosfet"
  - subject: "Oops"
    date: 2001-08-14
    body: "Perhaps you should look into openBSD and RAID for your servers."
    author: "Oh sure!"
  - subject: "Re: Oops"
    date: 2001-08-14
    body: "Good RAID is expensive.  Cheap RAID is evil.\n\nIt would be nice if all websites could be run on 8-way UltraSparcs, but unfortunately they can't:)\n\nOpenBSD is not an entirely bad suggestion though:)"
    author: "AC"
  - subject: "Re: Oops"
    date: 2001-08-14
    body: "Not even OpenBSD is magical enough to counteract moving-part hardware failure."
    author: "Carbon"
  - subject: "Re: Oops"
    date: 2001-08-14
    body: "No, OpenBSD may help with the security holes though.\n\nAs for the RAID, I'm sure that the KDE folks know enough about servers (!) to understand the issues. Since KDE doesn't run on RAID, the money must not be there. Perhaps now IBM or someone else could be talked into a donation though. It would be for a good cause..."
    author: "Oh sure!"
  - subject: "Re: Oops"
    date: 2001-08-14
    body: "OpenBSD has a lot going for it but is hardly a magic security tonic.  It is secure by default because it runs the absolute minimum and what it does run by default has been pretty well tested.  Add an application that is open to a DoS attack for whatever reason, and as far as that goes it becomes pretty irrelevant what type of unix you're running on--it just plain won't work if exploited.  If OpenBSD user don't realise this, it's secure default install probably does as much harm as good.  Sysadmins being put under the impression that they don't have to worry about security is a bad thing.. we're a lazy enough group as is."
    author: "victwenty"
  - subject: "Re: Oops"
    date: 2001-08-14
    body: "OpenBSD... that's what I'm running on my development box, I should probably move the ole web server over to that one of these days, but I'm too lazy.  I also think that would be a good move :)\n\nI hear 2.9 is nice, I'm still running 2.7...  too lazy to download and recompile"
    author: "dingodonkey"
  - subject: "Woody has .debs"
    date: 2001-08-14
    body: "KDE 2.2-Final binary packages have been in Debian Woody all day.  Ah, the beauty of a distributed, volunteer effort.\n\nMake sure to dist-upgrade right away."
    author: "jeb"
  - subject: "Re: Woody has .debs"
    date: 2001-08-14
    body: "Hmm, the translation packages and several others still appear to be marked beta."
    author: "Carbon"
  - subject: "Re: Woody has .debs"
    date: 2001-08-14
    body: "there are also SuSE KDE 2.2 final packages on \nhttp://www.suse.de/en/support/download/LinuKS/index.html"
    author: "its_me"
  - subject: "Re: Woody has .debs"
    date: 2001-08-14
    body: "thanx 4 the link - never would have looked anywhere else when it is not officially announced/available on kde.org! now: in what order do I install them?   I go into console mode and with yast1 first qt, then kdelibs, then all the rest at once?"
    author: "sgipan"
  - subject: "Re: Woody has .debs"
    date: 2001-08-14
    body: "Yes. But it's safe to select all the packages you downloaded before starting the installation.\n\nrpm should take care of the order to use to upgrade."
    author: "Evandro"
  - subject: "Re: Woody has .debs"
    date: 2001-08-14
    body: "German users can use ftp://ftp.gwdg.de/pub/linux/suse/ftp.suse.com/suse/i386/KDE/update_for_7.0/source/ to get Suse source packages."
    author: "anon"
  - subject: "Re: Woody has .debs"
    date: 2001-08-14
    body: "What apt sources do you use? I still get kde 2.1.1 for woody.\n\nMy sources.list:\n\ndeb ftp://sunsite.cnlab-switch.ch/mirror/debian/ woody main non-free contrib\ndeb ftp://debian.ethz.ch/mirror/debian/ woody main non-free contrib\ndeb ftp://ftp.debian.org/debian/ woody main non-free contrib\n\n\nI see the packages in the pool directory, but they are not found by apt-get dist-upgrade or apt-get install kdebase.\n\nAnd yes, I did apt-get update!\n\n\n Stefan"
    author: "Stefan Heimers"
  - subject: "Re: Woody has .debs"
    date: 2001-08-14
    body: "yep, the original poster was inaccurate unfortunately. I'm sure he meant unstable (sid) instead of testing (woody). KDE 2.2 is indeed not yet in woody.\n\n(since I'm using unstable, I enjoy my KDE 2.2 fix though ;-))\n\n-Arondylos"
    author: "Arondylos"
  - subject: "Re: Woody has .debs"
    date: 2001-08-14
    body: "and add to that it is compiled with objprelink\nCheers!"
    author: "ac"
  - subject: "Re: Woody has .debs"
    date: 2001-08-15
    body: "Wow!  The speed difference is quite noticeable.\n\nI have an Athlon 900 with 768MB RAM and two Debian installations:  A Libranet/Woody mix as the stable one and Sid for fun.  I've compiled KDE 2.2 myself on the stable system, and the one is Sid seems a lot more responsive.\n\nKonsole, Kate and KControl open instantly, Koqnueror takes less than two seconds.\n\nWow.  I hope this speed trick makes it in to the regular KDE distro, it's pretty sweet.\n\nHey, putting it in Debian Sid ought to give it the wide testing that was mentioned in th KC article.\n\nOh, way to go KDE team.  2.2 is simply outstanding.  The depth of this environment is amazing.\n\nBen"
    author: "Ben Hall"
  - subject: "Re: Woody has .debs"
    date: 2003-10-13
    body: "i want to upgrade kde 3.1.2\ngive proper ftp sites"
    author: "pk jithesh"
  - subject: "Re: Woody has .debs"
    date: 2003-10-13
    body: "posting here (in an article from 2001) is sure to let you be ignored, if the posters have notfication by mail turned off.\n\nanyway, ftp.kde.org has the semi-official packages for woody:\nftp://ftp.kde.org/pub/kde/stable/latest/Debian\n(read the README file there)\n\ngood luck\n-arondylos"
    author: "Arondylos"
  - subject: "Re: Woody has .debs"
    date: 2001-08-14
    body: "KDE 2.2.0 will make it in Woody, only it will take a day of 14 before it is in Woody. You can also download the Sid-packages and upgrade manually with dpkg."
    author: "Hans Spaans"
  - subject: "Think positive :-)"
    date: 2001-08-14
    body: "Why do people write viruses for Outlook/Windows ? Why do they attack www.microsoft.com, www.amazon.com ? Because they are famous targets, and if successful, they will have a huge impact, sometimes even in the news.\n\nSo why did they attack the dot ? Think positive:-)\n\nThis also calls for more distributive hosting for the KDE sites/repositories, to make it survive this kind of attack...\n\nPascal"
    author: "Pascal"
  - subject: "Re: Think positive :-)"
    date: 2001-08-14
    body: "They attacked the dot because they are pathetic little gnomers who are growing tired of constantly being out done."
    author: "Kraig"
  - subject: "Re: Think positive :-)"
    date: 2001-08-14
    body: "You have trolled.\n\nYou will now be denied tea and biscuits for two days.\n\nDo *not* troll again!\n\nThank you."
    author: "TrollAlert v0.1"
  - subject: "Re: Think positive :-)"
    date: 2001-08-14
    body: "Heh... have you seen my posts on /. or something? I go by \"The Troll Catcher\" :).\n\nBut I like TrollAlert v0.1 better... ;)"
    author: "David G. Watson"
  - subject: "Re: Think positive :-)"
    date: 2001-08-14
    body: "While I agree trolling is a BAD BAD EVIL THING, that was the first thought that entered my mind when I read the story :)\n\nPoor dot, I don't think it's ever been up for a month straight :)"
    author: "dingodonkey"
  - subject: "Re: Think positive :-)"
    date: 2001-08-15
    body: "Craig (he usually spells it with a \"C\") trolls everywhere. You should see the junk he posts on MandrakeForum.com. What a joke."
    author: "ScumRemover Pro"
  - subject: "Re: Think positive :-)"
    date: 2001-08-14
    body: "Who are you Pascal???\nHow did you know it was me \"VisualC\" who attacked the dot??? :-)"
    author: "VisualC"
  - subject: "FTP and mailing-lists are up again"
    date: 2001-08-14
    body: "Seems like the university of Bonn has taken over ftp.kde.org and they have the KDE 2.2 release online under ftp://ftp.kde.org/pub/kde/stable/2.2 I hope that their anoncvs-server is not slowed down to much because of this. ftp.us.kde.org and ftp.us.kde.org haven't mirrored it yet, did the path change on the server?  http://bugs.kde.org is still down."
    author: "someone"
  - subject: "gcc 3.0 or 2.96-mdk ?"
    date: 2001-08-15
    body: "Which is better to build KDE 2.2, gcc 3.0 or 2.96 from Mandrake 8.0 ?\n\nDoes objprelink works with Mandrake 8.0 (glibc 2.2.2) ?\n\nTIA"
    author: "2WhyNo"
  - subject: "Re: gcc 3.0 or 2.96-mdk ?"
    date: 2001-08-15
    body: "gcc 2.96. There are some issues with Mandrake 8 as far I remember, perhaps you need a new binutils or objprelink doesn't compile unter Mandrake 8 but someone made available a binary which he compiled under Mandrake 7 which also works unter Mandrake 8. Something like that, please search in the KDE mailing list-archive."
    author: "someone"
  - subject: "Re: FTP and mailing-lists are up again"
    date: 2001-12-23
    body: "Re: FTP and mailing-lists are up again"
    author: "steve"
  - subject: "KDE 2.2 and Red Hat"
    date: 2001-08-15
    body: "I'm going through the same cycle every year. I install latest Red Hat distribution (as long it's not .0 release) and feel great. I start to wonder how come I've been so fed up with it last year, but can't remember. And then few months pass, new KDE release comes along, I download all the packages for my distribution and I REMEMBER.\n\nIt's because there's always problems with installing new release of KDE on it. I can never just upgrade the damn thing. There's always some catch. And new packages always depend on things that are not a part of distribution and, what's really annoying, are not even part of updates that can be found on Red Hat's site.\n\nSadly this time is no different. I downloaded all stuff and discovered the following:\n\nkdebase wants following libraries:\nlibvorbis.so\nlibcrypto.so\nlibssl.so.2\nI have no idea why rpm didn't find first two, but last one simply doesn't exist. And I have no idea where I should get it. I know where it isn't and that's latest openssl updates found on RH's servers.\n\nkdebindings-python wants python2.1. Fair enough. But you can't have it for RH. You only get (in powertools) 2.0 and there's no update either.\n\nSimilar thing for kdeaddons-noatun. You need SDL1.2.\n\nAnd there's kdeadmin, which wants rpm 4.0.3. RH 7.1 ships with 4.0.2 and there are no updates of rpm on RH sites.\n\nSo, now I have KDE 2.2, which btw looks great, that I can't trust when it comes to security. That is if it doesn't core dumps because of missing library.\n\nI know these packages are done on voluntary bases and I really do appreciate this. Thank you for your effort. If my post sounded to hard on the packager, I really do apologize. It's just difficult to go through the same frustration every time and I wrote it in hope that next time it would be better.\n\nBut nothing will make me happier than someone who can demonstrate that I'm just an idiot by pointing out those missing packages to me."
    author: "Marko Samastur"
  - subject: "Re: KDE 2.2 and Red Hat"
    date: 2001-08-15
    body: "Maybe you can use www.rpmfind.net or freshmeat.net to get RPMs, or you could go to:\n\nwww.vorbis.com\nwww.openssl.org\nwww.python.org\n\netc to get the very latest versions. If you are still stuck just do the usual google search e.g.\n\ngg: libcrypto.so\n\nin konqueror and it'll find something. HTH."
    author: "ac"
  - subject: "Re: KDE 2.2 and Red Hat"
    date: 2001-08-15
    body: "I have similar problems with a few previous releases, so I gave up. I suppose that packager has a messed RedHat instalation on his computer - in fact it should be a pure RedHat installation on a separate partition but I think that packager use the same system, which he use for everyday KDE development.  Doesn't RedHat provide newest KDE packages ?   It is RedHat's fault ..."
    author: "Bee"
  - subject: "Re: KDE 2.2 and Red Hat"
    date: 2001-08-15
    body: "Did you look in the nonkde section of the binary rpm's on the ftp server?"
    author: "TJ"
  - subject: "Re: KDE 2.2 and Red Hat"
    date: 2001-08-15
    body: "Yes. Did you? ;)\n\nSeriously, I think I did all that's reasonable. I d/l everything that could be even remotely useful from Red Hat directory on ftp.kde.org, I looked on RH 7.1 discs and searched for updates on Red Hat servers.\n\nI could probably find at least some if not all software needed in form of rpm packages somewhere on the net, but I don't feel comfortable installing software from completely unknown (untrusted) sources."
    author: "Marko Samastur"
  - subject: "Re: KDE 2.2 and Red Hat"
    date: 2001-08-15
    body: "These are all great reasons to be a real man and compile from source.\n\nSo you can either stick with 2.1.1 or grow a pair and install 2.2."
    author: "Anony-Moose"
  - subject: "Re: KDE 2.2 and Red Hat 7.1 -- packages you need"
    date: 2001-08-15
    body: "Yes, it's a hell but well worth it. KDE is just too cool. I used http://rpmfind.net as usuall to find RPMs, it is extremely useful.\n\nI did it this way and could finally install all the packages I needed to get KDE running. Here is my list (\"ls |grep -v kde\"):\n\nSDL-1.2.2-2.i386.rpm\narts-2.2-1.i386.rpm\naudiofile-0.2.1-2.i386.rpm\naudiofile-devel-0.2.1-2.i386.rpm\nfam-2.6.4-9.i386.rpm\nfam-devel-2.6.4-9.i386.rpm\nkdoc-2.2-1.noarch.rpm\nlibogg-1.0rc1-2.i386.rpm\nlibogg-devel-1.0rc1-2.i386.rpm\nlibvorbis-1.0rc1-5.i386.rpm\nlibxml2-2.4.1-2.i386.rpm\nlibxml2-devel-2.4.1-2.i386.rpm\nlibxslt-1.0.1-3.i386.rpm\nlibxslt-devel-1.0.1-3.i386.rpm\nopenssl-0.9.6b-3.i386.rpm\npcre-3.4-2.i386.rpm\npcre-devel-3.4-2.i386.rpm\npopt-1.6.3-0.88.i386.rpm\nrpm-4.0.3-0.88.i386.rpm\nrpm-build-4.0.3-0.88.i386.rpm\nrpm-devel-4.0.3-0.88.i386.rpm\nrpm-perl-4.0.3-0.88.i386.rpm\nrpm-python-4.0.3-0.88.i386.rpm\n\nAs a note, I recompiled RPM-4.0.3 from the src.rpm . Also, some of these packages are in non-kde. Also: I used the RawHide packages in some cases. FInally: my login screen screwed up, I cannot configure it from the KDE control-center. I think I know what it is but i'll try tomorrow.\n\nBy the way, I feel the same way (even though I have lots of simpathies for the RedHat folks.\n\nTHANK YOU VERY MUCH TO THE KDE COMMUNITY. \nThis release is just miles away from any other desktop experience under Linux.\n\nCheers,\n-- leo\n\nPS: I had to install things in a certain order, I had to use --force many times and some times use \"rpm -i\" instead of \"rpm -U\". I know, you can compile, but I like rpms to keep track of dependencies an for clean uninstallations ..."
    author: "Leo Milano"
  - subject: "Re: KDE 2.2 and Red Hat"
    date: 2001-08-15
    body: "why is ther not 1 rmp to install the new kde 2.2 one my redhat 7.1 system now I have to download 10+ RPM and when I try to install the rpm's I get erors in my rpm installer"
    author: "Underground"
  - subject: "Re: KDE 2.2 and Red Hat"
    date: 2001-08-15
    body: "Having less packages would not solve errors in rpm.\nBut telling us what the error is would :-)"
    author: "Carbon"
---
The dot was down again this weekend as many of you had noticed,
 and while I myself was blissfully enjoying Halifax.  This time it was the result of somebody exploiting a security hole in our apache configuration and DoS'ing us in the process.  We've closed up the problem, and hopefully we will go forward with <a href="http://dot.kde.org/996512729/">our plans</a> to provide better availability in the future.  In worse news, master.kde.org, one of the key KDE servers that handles mail, mailing lists, KDE FTP, several websites, and some other duties has been unreachable for some time now --  word on the street is that the machine has suffered a big harddrive crash over at <a href="http://www.uni-tuebingen.de/">Uni-Tuebingen</a>. As a result of this, primary KDE communications have been cut, packagers can't upload the binary packages for KDE 2.2, and inevitably the KDE 2.2 official release has been delayed. Hang in there folks, hardworking KDE people are busy migrating essential services over to other servers.   In more fun news, Newsforge is running <a href="http://www.newsforge.com/article.pl?sid=01/08/10/1441239">some interesting coverage</a> of the Linux/KDE deployment by the City of Largo, Florida, that we <a href="http://dot.kde.org/995949998/">featured on the dot</a> some time ago. (as seen on <a href="http://slashdot.org/article.pl?sid=01/08/13/1248233&mode=thread">Slashdot</a>) 
Other tidbits that may be of interest: <a href="http://capsi.com/kmonop/">KMonop</a> has now been <a href="http://freekde.org/article.pl?sid=01/08/13/1258209">renamed to Atlantik</a>, and <a href="mailto:jsinger@leeta.net">Otter</a> wrote in with the good news that <a href="http://kde.themes.org/">kde.themes.org</a> is not dead but simply undergoing <a href="http://www.themes.org/php/comments.phtml?forum=news.1.101">a big upgrade</a>.  Get ready to submit those KDE2 themes!
<!--break-->
