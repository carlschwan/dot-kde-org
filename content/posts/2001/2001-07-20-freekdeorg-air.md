---
title: "freekde.org is on the air"
date:    2001-07-20
authors:
  - "numanee"
slug:    freekdeorg-air
comments:
  - subject: "OT: Is anyone planning to develop Katan?"
    date: 2001-07-21
    body: "Is anyone planning to develop Katan (KDE version of Gnocatan)? I'm learning C++\nright now and would be interested in helping developing Katan."
    author: "Erik"
  - subject: "Re: OT: Is anyone planning to develop Katan?"
    date: 2001-07-21
    body: "Just join us on IRC on server irc.kde.org on #kde. There's lots of KDE folks and programmers, maybe you could tell us some details and we'll help you with Qt/KDE programming :)\n\nCheers,\n\nRalf"
    author: "Ralf Nolden"
  - subject: "The first shot is fired?"
    date: 2001-07-21
    body: "\"and whether to use tabs or spaces for indentation\"\n\nSo this is how it is to be?  Prepare for war!\n\n* Neil lets out a primal yell, raises his scimitar, and charges into the fray..."
    author: "Neil Stevens"
  - subject: "Re: The first shot is fired?"
    date: 2001-07-22
    body: "hehe ;)\n\nset expandtab\nset sw=3\nset ts=3\n\nyes, i'm guilty m'lord....\n\nAlex"
    author: "alex"
  - subject: "Re: The first shot is fired?"
    date: 2001-07-22
    body: "\"Indentation? I will show you an indentation in your skull!\" -- freekde.org"
    author: "ac"
  - subject: "Re: The first shot is fired?"
    date: 2001-07-22
    body: "Yeah, but that doesn't mention tabs and spaces. :-)"
    author: "Neil"
  - subject: "Free KDE"
    date: 2001-07-21
    body: "Come to freekde.org and join the fight to free KDE today!\n\nWhat?  KDE is already free?"
    author: "not me"
  - subject: "Re: Free KDE"
    date: 2001-07-21
    body: "Arg.  Now what am I going to do with all these picket signs I just made?\n\nI could scribble in some letters to make them say \"Freeze KDE\" but I think KDE is already doing that too."
    author: "Justin"
  - subject: "Re: Free KDE"
    date: 2001-07-21
    body: "ROTFLOL"
    author: "KDE User"
  - subject: "Re: freekde.org is on the air"
    date: 2001-07-21
    body: "I just want to thank Neil for all his help he gives to KDE users on #kde-users channel. Freekde.org looks nice, and I hope it will be as succesfull as dot.kde.org."
    author: "antialias"
  - subject: "WARNING!!"
    date: 2001-07-24
    body: "WARNING!! It appears that freekde.org is a RMS propaganda machine. Its name is free but thats not what they're pushing. There against people having freedom with there won code and seem to be against The Kompany!!"
    author: "KDEFan"
---
If you thought news here was a tad slow lately, you'd be right.  Most of our editors are either on vacation or have taken some time off, and we haven't really gotten all that many submissions from <i>you</i> either.  Fortunately, Neil Stevens <a href="http://freekde.org/article.pl?sid=01/07/20/0812243">comes to the rescue</a> with his brainchild <a href="http://freekde.org/">freekde.org</a>, a dedicated KDE news site with an editorial slant towards Free Software and whether to use tabs or spaces for indentation.  They already have a few articles and reviews of interest on the site, so go check it out.  While it's about time that KDE had more than one news site dedicated to it, I admit I'm keeping a jealous eye out on the new competition in town.  Oh, and if you're still lacking for desktop news, you might want to check out <A href="http://www.gui-lords.org/">GUI-Lords.org</a> as well.  And if you're <i>still</i> bored, please <a href="http://dot.kde.org/addPostingForm">submit some content</a> to the dot!
<!--break-->
