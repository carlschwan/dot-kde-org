---
title: "KDE 2.1 Release Delayed 1 Week"
date:    2001-02-11
authors:
  - "numanee"
slug:    kde-21-release-delayed-1-week
comments:
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-10
    body: "yes the screenshot is beautiful.\n\ngood idea. more of these cool desgin would help to make kde the best desktop on unix/linux.\n\nimho i often thought gnome has brilliant design-ideas (+ very good pr-team) and kde has the better technic and solutions....\n\ni see the kde-team is ready to make cool design-stuff.\n\ngo further kde-team.\n\nbye\n\nsteve qui"
    author: "steve"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "You said it! That was exactly what I was thinking. I don't know if KDE will do the same as Gnome/Helix Update agent for novice KDE users to make it more user-friendly."
    author: "Satyan"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-23
    body: "I don't think that \"cool design\" like this increases the usability of a desktop. If I want to change control settings I'm not interested in watching pictures. So I dont't think that this helps to make kde the best desktop. It is more the better technic and solutions that do this.\n\nIMHO the design of a desktop should not attract attention, it should help the user to focus on the important things.\n\nFortunately most of the design in KDE is configurable. For me the design of KDE 2.0 was the best design ever in KDE and I fear that the design of KDE 2.1 is a regress (after seeing the current version in CVS).\n\nRegards,  Oliver"
    author: "Oliver"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-10
    body: "What is there to say, it looks great!!! Take 1 or take 2 weeks it doesn't matter couse it's just getting better!!\n\nLooking in the mirror I see various other desktop environments:)\n\nI'm just never gonna stop being impressed.......\"KDE is killing Bill\" *grin*."
    author: "DiCkE"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week(correction)"
    date: 2001-02-10
    body: "Should of course be the rear mirror:)"
    author: "DiCkE"
  - subject: "Take your time"
    date: 2001-02-10
    body: "I *just* upgraded (reinstalled to reset the RPM database) to Mandrake 7.2 in order to prepare for 2.1... I'm salivating at the thought, *but*... take your time, I think most users would tell the wonderful people who are developing the software that we will wait for greatness (because we've come to expect it?  No pressure :)<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-10
    body: "Great KDE Team ! only a few little question.  What about animated menus? the 'shade' effect of W2K is perhaps the best improvement on that 'lovely' O.S. jejejjjj.  Is this possible or it's a 'job' for QT?"
    author: "Rossend Bruch"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-10
    body: "It is not the more important, isn't it ? :)"
    author: "Shift"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-10
    body: "It's more a joke than an important mather, but I think that KDE has reached a more than acceptable madurity and little tips are important.  \n\nGood feeling and visual desing are very important for a desktop."
    author: "Rossend Bruch"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "It is implemented in Qt.\nSee QApplication::setEffectEnabled()"
    author: "Bernd Gehrmann"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Thought this was only in the windows 2000 implementation\nof Qt?!\n\nIf not then... umm...\n\nWhy has nobody used it?"
    author: "Alex"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "'Cuz it sux! I had it on for about five minutes before I had to turn it off in disgust."
    author: "David Johnson"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-18
    body: "Well, my licq 1.0.2 uses it for tooltips. Looks nice or crappy, whatever you want. ;)"
    author: "Andras B."
  - subject: "QT already has it..."
    date: 2001-02-11
    body: "QT already has this implemented, although you need to set an X resource (\"*guieffects: fademenu\") before you start your QT programs. \n\nThere are some other X resources to try:\n*guieffects: animatedmenu (doesn't work together with fademenu)\n*guieffects: animatedcombo\n*guieffects: animatedtooltop\n*guieffects: fadetooltip\n\n/adam"
    author: "Adam Dunkels"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Well, if \u00edt's possible then I think kde should uwse it as default. The shadow on the crusor looks very good. Yeah i'm a w2k copycat =)"
    author: "Andre4s"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Ok, Cursor Shadow and fade effect.  Best improvments on w2k technologies. jejej ;)"
    author: "Rossend Bruch"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "And also the most irritating thing in w2k..\nGood thing one can turn it off, else i'd gone mad\nseveral months ago.. its cool for a couple of days,\nbut working daily with it just makes my work\nslower.."
    author: "Nils O. Sel\u00e5sdal"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-16
    body: "I'm your opinion, KDE should look on things making\nwork faster....\n(thanks for the great desktop!)"
    author: "Helmut"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-10
    body: "I look forward to getting the new distro as it certainly looks beautiful.\n\nDo hope that solve the problem of the disappearing modules in the KControl centre (2.1b2) prior to release that's a real bummer that no one has been able to solve.\n\nGreat work you guys are doing!"
    author: "Marvin Pierce"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-13
    body: "if you are using the 2.1 beta 2 rpms to install on your system, you need to install the rpms again. i had ML 7.2 i did the rpm install of kde 2.1 beta 2 then there were no menus on the kcontrol, i forced install off all the kde rpms once again and its working FINE withouth ANY PROBLEMS\n\nhope that helps\ncheers\nCrazyCrusoe"
    author: "Syed Irfan"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Please, please, please don't forget window geometry control. It'd give KDE a much more polished feel.\n\nRight now i still have to maximize Konqueror every time i open it.\n\nHmm. Come to think of it, Kmail and Knode do seem to work right. Is this a KDE problem at all, or just a Konqueror problem?"
    author: "Cihl"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "There has been an option to save window size/position for Konqueror in (\"view profiles\"  or so) for quite a while..."
    author: "HoHa"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Why in the world isn't this the default then???  This is not something I should have to go configure!"
    author: "Chris Bordeman"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "This should work:\n --- load up a brand new konqi\n --- maximise it\n --- use \"Window - Save Webbrowser Profile\" to save\nWorked for me...\n\nYup, I admit, not the most natural way of doing it,\nmaybe it should be in ktip?\n\nAlex"
    author: "Alex"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "If saving the window size would be the default, then I would complain! But an idea could be to have a (global?) switch to automatically save (or not) window sizes and positions\n\nJohann"
    author: "Johann Lermer"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "hmm, right clicking on the title bar and selecting \"Store Settings\" always worked for me... well nearly always........"
    author: "Brendon Leese"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "Hmm, you would ? :}\n\nI was planning to make Konqueror auto-save the\nwindow size in the profile after 2.1.\n\n(In the profile, so that file-manager windows\nand web-browsing windows can still have different sizes).\n\nOf course the current \"Save View Profile\" option does that, but people seem to have a hard time finding it (and not getting it to remember the current URL, i.e. doing it on a blank new window)."
    author: "David Faure"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Where are the useful functionnality of KDE1 with the mouse click on the top of a window ?\n\n- left : select + in front\n- middle : above\n- right : only select"
    author: "Shift"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Right in the control center, under Window Behavior, along with tons more options for your tweaking pleasure.  Check out the attatched screenshot of the relevant Control Center module."
    author: "not me"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Ho Ho !!\n\nI am tired : it's 02:00 in France :)\n\nOf couse, it is configurable but I am not sufficently intelligent to go to the \"window behavior section\"\n\nWhat am I doing in an Engineering school ? :)))"
    author: "Shift"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Fine, use time to Polish it.\nIt's important that the UI is attractive, and not boring.\nYou're doing a fantastic job.\nMy oppinion is that Linux got much better after having KDE.\nJust two thing that annoyes me:\nTooltips keeps popping up in the panel.\nI cannot remove them, and if I move from one button to another it keeps poping up with no delay.\nSecond I keep starting to do a logout, because the\nKmenu is moved donw into the K-button and when I release the mousebutton it is done over the logout item.\nThanks for the best WM EVER!!"
    author: "Jarl E. Gjessing"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Please explain what you mean about the the tooltips - I don't have this problem. Which version of the code are you using?\n\nThe second problem you raise is fixed in the CVS IIRC. Like all bugs, please tell us *exactly* how to reproduce them or it is very difficult for us to fix things.\n\nCheers\n\nRich."
    author: "Richard Moore"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "I see the tooltip problem too.  KDE checkout from this afternoon, clean user account with all default KDE settings.  Qt 2.2.3.\n\nHover mouse over a kicker button until a tooltip appears.  Move pointer over kicker, tooltips pop up immediately and \"feel\" in the way.  It is a bit off-putting.\n\n-N. (who has had tooltips off, forever, but can't       find how to toggle the setting anymore)\n\nPS Does anyone know how to turn on favicons in the    latest Konqueror?\n"
    author: "Navindra Umanee"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "ditto, 2.1beta2-0128 (debian packages)\n\nIt seems to be somwhat focus-related, as moving the focus to another applet in the kicker (say, krunapplet's commandline) fixes it. Moving focus to another window doesn't change anything though."
    author: "puetzk"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "<i>Hover mouse over a kicker button until a tooltip appears. Move pointer over kicker, tooltips pop up immediately and \"feel\" in the way. It is a bit off-putting.</i><br><br>\nI think that's just how tooltips work, even in Windows. Same happens in every toolbar. Imagine you want to check out all the tooltips for every button and you have to wait two seconds for every tooltip. That would suck :)\n<br><br>\n<i>PS Does anyone know how to turn on favicons in the latest Konqueror?</i>\n<br><br>\nHmm, you're the second one to ask that... it should be on by default (it is). Check your kdeglobals:<br><br>\n[HTML Settings]<br>\nEnableFavicon=true<br><br>\nUnless you have explicitly set it to false, it should work just fine."
    author: "gis"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "I test new KDE snapshots on a new user account, with all KDE configuration wiped.  Yesterday's Konqueror CVS has no favicon support on by default, and I can't find the GUI option that used to be there and let me toggle favicon on/off.  Was that removed, if so why?  I imagine some people will want to turn off favicon support as it's non-standard.\n\nAlso, has anyone found out how to turn on and off tooltips from the control panel yet?  I know that used to be possible...  (Btw, Netscape/Motif and GNOME do not have the tooltip behavior described, didn't have Windows around to test though.)\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "<i>I test new KDE snapshots on a new user account, with all KDE configuration wiped. Yesterday's Konqueror CVS has no favicon support on by default,</i>\n<br><br>\nThat's the problem actually :) There was a bug -- favicons only worked after the first start of konq -- the iconloader didn't consider the new favicons directory during icon-lookup. Just fixed that.<br><br>\n\n<i>and I can't find the GUI option that used to be there and let me toggle favicon on/off. Was that removed, if so why? I \nimagine some people will want to turn off favicon support as it's non-standard.</i><br><br>\n\nWhy would you want to turn it off? The config-option is still there (kdeglobals, [HTML Settings], EnableFavicon=bool)."
    author: "gis"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "Thanks for the fix! I'll test it out when I get a chance to update....\n\nAs for disabling favicon, well, *I* don't want to. But some webmasters don't appreciate phantom requests for http://site/favicon.ico , and coupled with the fact that this is a non-standard feature, I think it would be nice if this could be easily turned off in Konqueror.\n\nAnd also, I imagine however unlikely it seems now, that the favicon feature may have security exploits or DoS exploits, and having an easy way for the user to turn this feature off might be desireable.\n\nI understand that you can turn it off in kdeglobals, though.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "Okay, now it works but there's still a problem.\n\nThe default mini-icon is now the ugly \"question mark\" instead of the regular web document icon.  You can reproduce this by going to http://slashdot.org/ or any site that does not support favicon.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "<i>The default mini-icon is now the ugly \"question mark\" instead of the regular web document icon. You can reproduce this by going to http://slashdot.org/ or any site that does not support favicon.</i>\n<br><br>\nDid you update kdelibs/kio as well? There was another fix."
    author: "gis"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "That works!  Thanks for your patience...\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "<i>And also, I imagine however unlikely it seems now, that the favicon feature may have security exploits or DoS exploits, and having an easy way for the user to turn this feature off might be desireable.</i><br><br>\nThere is no special code to get the icons, it's just using a KIO Job to fetch the icon whenever you open an http URL (and you don't already have the icon). It's nothing but one more additional request to the webserver.<br><br>\nThe config option apparently was removed because it would be config-overkill and would be rather confusing than helpful."
    author: "gis"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "Well, Konqueror permanently saves favicon.ico somewhere, I believe.  That *might* be an issue. (filesize itself may slow down konqueror performance... dot.kde.org itself employs an oversized favicon.ico which is unfortunately currently out of my direct control) \n\nAlso what if it's gibberish?  People routinely put gibberish in that file, check http://www.slashdot.org/favicon.ico, for example.  I know there's no problem *now*...  I'm just worried about future possibilities and the possible need to conveniently turn the feature off.\n\nAnyway, it probably isn't that much big of a deal...  The default is perfectly fine by me and hasn't been a problem so far.\n\nCheers,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-13
    body: "<i>Well, Konqueror permanently saves favicon.ico somewhere, I believe</i><br><br>\nI think it performs some auto-cleaning after an icon hasn't been used for a certain time. Can't check at the moment tho.\n<br><br>\nRegarding gibberish... right, that might be a reason. But then, I can't really imagine visiting a site that purposely uses a gibberish favicon :)"
    author: "gis"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "KDE usability is getting better all the time, but here are a few areas I'd like to see improved:\n\nI'd like to see maximized windows behave like they do on windows: lose the left, right, bottom, and top borders except for the title bar with it's icons, and make the window unmoveable (like in fullscreen mode) until I restore.  An important thing usability-wise is to be able to zip the mouse over to the right edge to click and drag the scrollbar, _without_looking_ to determine the width of the border first.  To do so requires the border to not be there.  And don't do 1 pixel like in windows (why do they do that?).\n\nAlso, I'd _love_ to be able to adjust the size of those tiny scrollbars.\n\nThanks guys!"
    author: "Chris Bordeman"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "This is very annoying and restrictive behavior for Linux/Unix people!  Not being able to move or resize a window is Very Bad.  \n\nHowever, you can turn this Windows bug ;) on for yourself in the Control Panel at: Look&Feel|Window Behavior|Advanced|Allow Moving and Resizing of maximized windows."
    author: "KDE User"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "That doesn't do it.  It does indeed prevent you from resizing or moving a maximized window, but it doesn't completely remove the window border.  The bottom border is still completely there, and there's a 1-pixel border along the sides that prevents you from just sliding the mouse to the edge to access the scrollbar.  It's the worst of both worlds!  \n\nI'd really like a \"Don't show window border when maximized\" feature instead of the feature we have currently.  Then you could still resize the window with Alt-RMB-drag but there would be no wasted space and the scroll bar would be completely at the right edge of the screen, so you could hit it immediately when you shoved the mouse to the right edge.  \n\nThis is a real usability issue, GUI experts know that the most easily accessed place on the screen is the very edge."
    author: "not me"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Have you tried deselecting \"allow Moving and Resizing og maximized windows\" under window behavior in Control Center ?"
    author: "\u00d8ystein Heskestad"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "I hadn't seen that, but it really should be default so that you don't accidently move out of 'maximised' position/size.  Someone said that restricting movement here is not good.  In this case in fact, it is important.  After moving a maximised window, it becomes marked as 'restored' again, losing the previously recorded 'restored' size and position.  This sucks because users like predictability and simplicity, and this is just confusing.\n\nOf course, I could just not move the window, but if I can I will, even when I shouldn't, because I'm not thinking.  Making it unmoveable keeps me from accidentally moving it, if I want move it I restore first, move, and size like I want; I don't lose the original size/position info and it simplifies things.\n\nThe most important thing, though, is to hide the borders (but not the title bar) when maximised such that screen real estate is not lost, the complexity of the display is reduced, and I can easily access elements at the edge of the screen w/o looking like the scrollbar or a special toolbar."
    author: "Chris Bordeman"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "Tiny scrollbars?\nOpen *.themerc and go to [Misc] section and add:\n\nScrollBarExtent=20 \n\nor whatever number of pixels you want. Reload theme\nand you will have 20 pixels wide scrollbar.\nEnjoy ;"
    author: "Zeljko Vukman"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "hye, thanks!  It would be nice to have this in the control center tho, also, the ability to set fonts for different display elements like menu text and window and message texts."
    author: "Chris Bordeman"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "Kcontrol > Look and Feel > Fonts"
    author: "Zeljko Vukman"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2004-12-05
    body: "I've been looking everywhere to find how to increase the tiny scrollbar size too - surely this should be a simple thing to do.  For me Linux isn't currently useable due to the fixed tiny size of the scrollbars.  "
    author: "Paul J"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2006-02-24
    body: "just been trying to do this myself. I'm used to the windows windows so all the big resize bar made me constantly resize windows whenever i was trying to scroll or reposition them... anyway, the following settings seem best to me...\n\nControl Center > Desktop > Window Behavious > Moving > Turn off \"Allow moving & resizing of maximised windows\"\n\nControl Center > Appearance & Themes > Window Decorations > Use Plastik with normal border size and all three options unticked.\n\nControl Center > Style > Use Plastik.\n\nThat seems the most Windows like to me so far anyway :o)"
    author: "pepsi_max2k"
  - subject: "KWM and XEmacs"
    date: 2001-02-11
    body: "Excellent work.  KDE (and konqi) were what pushed me over to using linux 100%.\n\nAfter installing 2.1beta2, I ran into a rather silly problem with XEmacs (that I can't figure out how to fix).  Alt-x no longer maps to M-x (oddly, it does in vanilla emacs).  Is this an xemacs setting or a KWM setting?\n\nThanks for any help.\nseth"
    author: "seth"
  - subject: "Re: KWM and XEmacs"
    date: 2001-02-11
    body: "That has more to do with X than KDE.  I don't know why installing KDE would affect that at all.  If you have an M$ keyboard, try using the windows key instead of alt.  I finally figured out that my Windows key was mapped to the Meta key (notice that if you try to assign the windows key to an action in the Control Center, it says you pressed the meta key).  There are various settings in the XFree86 configuration file that can map Meta to alt and stuff, so if it still doesn't work, you can search around the XF86Config man page (notice the capitalization) and make the necessary modifications to your config file.  Good luck!"
    author: "not me"
  - subject: "Re: KWM and XEmacs"
    date: 2001-02-12
    body: "The real fix for this is with xmodmap.\nUse xmodmap, it will show you the modifiers.\nThere you can add/remove modifiers, and test with xev.\n\nHmm, Alt is mapped to meta in my xemacs and I simply have\nmod1        Alt_L (0x40)\nin the xmodmap output. There's a missing link somewhere..."
    author: "David Faure"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "KDE looks absolutely great! I must say, I am very impressed with whats happened so far. It's almost up to the level of user-friendlieness of windoze, and already is more powerful and more stable.\n<br><br>\nBegin rant:<br><br>\nHowever, I see a problem constantly that really bugs me : shitrific distros. I'm talking about distros who, although are great companies, have an annoying habit of screwing things up and acting in a very non-open source way. Distros such as these are constantly trying to build brand new apps and remake the wheel. A good example is hard-ware configuration. Both RedHat and Mandrake (and a few others) have built entirely new apps to do this with, instead of building upon the very cool framework of Linux-conf (which I still want to be ported to QT). Another example is X configuration, which is done in both distros, repeatedly, in a way which suggests that their QA teams highest expendature is donuts. Very blatantly, in Mandrake 7.2 they include a universal menu system, which\n<ul>\n<li>Does not allow updates by, say, inserting an acutal .desktop file into the hierarchy. Any vain attempts by the user to do this causes the menu to be reloaded, but often only partially, so that about 80 to 90% of the items in the menu are missing, until you run the menudrake command and sit for 10 minutes while it loads the new hierarchy!</li>\n<li>Is very badly spelled and organized (behold the evil of a menu labelled Applications, when everything in the menu is an application. Bah!)</li>\n<li>Will remember to put in entries that are dependant upon the DE you are using, but leaves the destination field blank!</li>\n</ul>\nAlmost every distro I have tried has problems that reach this point. This includes Caldera, Redhat, SuSE, and others. Although they have great intentions, and they have some very good talent on their side, I think that perhaps the rush to release a certain package ahead of the competition is very hard to resist, even if it results in bugs that a casual user can notice within 5 minutes of installing. The solution : I'm not sure. Maybe we can get some of the awesome KDE bugfixers on a distro we like, and start killing bugs. Or perhaps the solution is that I simply have no decent taste in distros.<br><br>\nEnd Rant\n<br><br>\nIn any case, I applaud the KDE team for your seriously high cool factor, and also the GNOME team for your also very seriously high cool factor. And, I would also like to add that I am not saying \"All distros suck\". Troll bashers, lay off!"
    author: "Carbon"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Stay tuned to the dot. Currently working on something which will help your rants. ;-)"
    author: "George Ellenburg"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "I know, that Mandrake menu crap is the worst.  Good thing it doesn't even work on Gnome.  But I sure would like the good old KDE menu editor back.  And I agree, rather than Mandrake creating whole new things, they should, for instance add a KDE LinuxConf to KDE itself you know?  Although, I see where they may want to create Distro-specific apps, otherwise, why would people use that distro if its the same as all the others?  (I disagree, the installation is where they should be different)."
    author: "Henry"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "Well, if a distro did something using standard open source methods, then wouldn't that make them DIFFERENT from their competition? :-)\nAlso, a truly distro specific app is impossible anyways, because only an idiot distro would release something distro-specific that isn't open source. I always wondered why other distros didn't adapt or fork the one exceptional part of caldera, LIZARD."
    author: "David Simon"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "Mandrake 8.0 will make it possible to disable that \"menu crap\" and leave the menus up to the desktop environments. Not really a step back, more like letting the choice up to the user.\n\nBTW if you use cooker, you can already\nrm -rf applnk\nmv applnk.kde applnk\nget rid of the menu packages\nand use kmenuedit"
    author: "David Faure"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "<em>Both RedHat and Mandrake (and a few others) have built entirely new apps to do this with, instead of building upon the very cool framework of Linux-conf</em>\n\n<p>Um, linuxconf *IS* a distro-specific app. It's one of Redhats. Unless they've fixed it recently, Redhat actually <b>requires</b> GNOME before it will install the text-only version of linuxconf.\n\n<p>And speaking of distros, there will be no single configuration utility that will work. There's no way a universal configuration will work for each of Debian, Slackware, SuSE, FreeBSD, Solaris, AIX, HPUX, etc. KDE is not just for Linux, let alone Redhat based Linuces."
    author: "David Johnson"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-13
    body: "Not really. Linuxconf does not specifaclly require GNOME, unless you compile it with GTK support, which still allows you to run it in text mode. Basically though, even though I might have been mistaken on that, my main point was basically that stupid actions by distro companies really screw up really good software such as KDE (and GNOME, and GIMP, and XMMS, and PHP, and so on), and also drive people away from Linux (and other open source OSes), who assume that the problems introduced by the distro companies is not a distro specific bug, but rather a general problem with the program."
    author: "David Simon"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-17
    body: "<I>....Not really. Linuxconf does not specifaclly require GNOME, unless you compile it with GTK support, which still allows you to run it in text mode....</I>\n\n<P>maybe Linuxconf doesn't need gnome to work in text mode even if compiled with GTK support but the package manager may think otherwise,let me recall a situation which hapenned to me a few years ago.....</P>\n\n<P>i was running debian and i wanted to install a sound app requiring the alsa drivers,since i already had the newest kernel at the time (one of the 2.2.1X series IIRC,i think 2.2.12),i also had my alsa driver (also the latest),i fire up apt-get which download all the requisite package including some less recent alsa driver (mine werent in dpkg's database,i compiled them)....well...those debian alsa driver conflicted with the same kernel they depended upon and apt-get decided to uninstall my kernel !!!! you're free to imagine what kind of mess this situation created....</P>\n\n<P>Alain</P>"
    author: "A. Toussaint"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-17
    body: "Wow, that does sound messy. However, how did it unistall the kernel. I can almost see removing the source file off the hard disk, but I can't possibly imagine an app packager stupid enough to set his package to screw with the MBR. And btw, I have never used it, but I keep hearing people say the Debian's pkg manager is basically proof to the kind of nutsiness that goes into RPM, and I guess that no tool is idiot-proof after all! Ah well, at least in open source we have the ability to fix stuff like that when it pops up."
    author: "Example of what NOT to do in response to a troll"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-21
    body: "distro with lowest weasel moves quotient?\nDebian."
    author: "penguano"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-22
    body: "Keep in mind KDE runs on other platforms beside Linux, who does not have the problem (with the various distros fsck'ing up everything) at all. :)\n\n/leg - happy KDE-user on FreeBSD 4.2"
    author: "Lars Erik Gullerud"
  - subject: "KDE 2.1.0 with improved looks!!"
    date: 2001-02-11
    body: "Just to answer a question above (regarding something like helix gnome for kde), a kde installer is being worked on as we speak (a graphical frontend for selecting which packages you wish to install (workstation, minimal, custom and so on), and then either lets you fetch the packages from a ftp mirror site, or from a local directory.\n\nAnd the new look for 2.1.0 is simply great :)\n\nCheck out http://realityx.net/kde/ for A LOT of cool screenshots of the new look and other various kde goodies :)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: KDE 2.1.0 with improved looks!!"
    date: 2001-02-11
    body: "IMHO this installer sould be a static binary so that one can install QT/KDE with no requirements\n\n--\nAndreas"
    author: "Andreas Joseph Krogh"
  - subject: "Re: KDE 2.1.0 with improved looks!!"
    date: 2001-02-11
    body: "It is.."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: KDE 2.1.0 with improved looks!!"
    date: 2001-02-11
    body: "*blink, blink, blink*.. hmmmm, i presumed M$ would once again take the lead of most good looking GUI with Whistler.. ..but.. ..i think.. ..i was wrong.."
    author: "Brendon Leese"
  - subject: "Good job guys!"
    date: 2001-02-11
    body: "Just want to say good job KDE team!  Can't wait for the \"official\" 2.1\n\nI wonder if they ever have time to sleep :)\n\n-Justin"
    author: "Justin"
  - subject: "Re: Good job guys!"
    date: 2001-02-11
    body: "sleep -- sleep .. hm\n\n/me doing a \"dict:sleep\" in konqueror.\n\new -- does anybody really do something like *that*?\n\nWhat's this for?\n\nIrritated, \nTackat"
    author: "Tackat"
  - subject: "Re: Good job guys!"
    date: 2001-02-11
    body: "<i>What's this for?</i>\n\nTo dream of new great icons."
    author: "Lenny"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "I've been a Gnome user since summer '99 (Red Hat 6.0) and I liked it more that KDE 1.x. Last week I installed Mandrake 7.2 and started using KDE 2 for the first time. I'm really shocked. There is nothing in Gnome that is better than it's counterpart in KDE. KDE simply feels complete, whereas Gnome seems a neverending work in progress. I will continue to watch both projects, but I now understand what the KDE people have been saying all along. It's slick, beautiful, highly configurable and highly stable. Incredible work."
    author: "Brad Stewart"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "It's funny you say \"highly stable\" because even though I love KDE2's look & feel, I just got through installing Nautilus and using Gnome(I have Mandrake 7.2 also).  It's mostly because I am tired of KDE apps crashing so much.  Also, because Mandrake really screwed up the KDE menu system.  Even though I prefer KDE2's look & feel, I think with Gnome2, Eazel & Helix Code's UI designers & artists, they may actually surpass KDE's great looks, while keeping robustness and stability.\n\nPlease don't reply, I'm feeling really bad, because this is the first time I've actually started to prefer Gnome over KDE--you just have to spend a whole day downloading stuff first.  Then again, maybe 2.1 will solve some of the stability problems, and maybe Mandrake will get rid of the menu thing.  Hell with it, maybe I'll just give in and use RedHat."
    author: "Henry"
  - subject: "Don't worry -- KDE 2.1 rulez for sure"
    date: 2001-02-11
    body: "> Gnome2, Eazel & Helix Code's UI designers & artists\n\n*yawn* Gnome2 is actually at least 8 months away -- maybe even a year. They need to port everything to Gtk2 first (which doesn't even exist as a beta yet). And as the API of Gtk2 is claimed to be so much better they probably need to rewrite *everything*. Eazel & HelixCode have been there for more than a year now and have aquired about $30mio within that time. Still KDE/Qt is technically concerning almost everything ahead of Gnome/Gtk. \n\nAnd yes: KDE 2.1 is a huge improvement over KDE 2.0. For a user maybe even a bigger improvement than KDE 2.0 was over KDE 1.1.2. And of course it's much more stable."
    author: "yes"
  - subject: "Mandrake 7.2 shipped prerelease of KDE 2"
    date: 2001-02-11
    body: "Actually Mandrake 7.2 came in two versions with one of them being a Release-Candidate and not the final version. Maybe you installed one of these. Furthermore KDE 2.0.1 was shipped afterwards which has been a lot more stable than KDE 2."
    author: "Anonymous"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-11
    body: "And further: a significant amount of crashes people saw was due to bad packaging. Of course it's your choice to trash a great product because you chose a bad distro..."
    author: "Lenny"
  - subject: "lan:// browsing"
    date: 2001-02-11
    body: "I installed KDE Beta2 for Mandrake 7.2, and everything is great, but in Konqueror, I put lan:\\ into the address bar, and Konqi trys to search for \"lan:\\\" on Google.....\nI cant find the LAN Browsing module in KControl either.\nDo I have to download and SRPM for kdenetwork and compile it by hand????\n\nChris Adams"
    author: "Chris Adams"
  - subject: "Re: lan:// browsing"
    date: 2001-02-11
    body: "I am also using KDE 2.1 Beta2 on Mandrake 7.2 and was having the same problem.\nNext I _disabled_ the Control Center -> Web Browsing -> Enhanced Webbrowsing -> Enable Internet Keywords.\n\nNow, when I put 'lan:/' or 'cd:/' in the addressbar, I get \"protocol not supported\". However putting 'audiocd:/' in the addressbar works.\n\nAny Ideas?"
    author: "Kundan Kumar"
  - subject: "Re: lan:// browsing"
    date: 2001-02-13
    body: "i have the same problem"
    author: "xe"
  - subject: "Re: lan:// browsing"
    date: 2001-02-11
    body: "Did you install the KDE-Network package? That package contains LISA, needed for the lan:/ thingy."
    author: "Richard Stellingwerff"
  - subject: "Re: lan:// browsing"
    date: 2001-02-12
    body: "Yes, I did install kdenetwork.\nkio_lan was installed with the Mandrake RPM, just wasnt loaded into konqi, but there was no libkcm_lanbrowsing.so in the package.\n\nI have uninistalled kdenetwork, and compiled it from source, and after some toiling, managed to get the lan:/ and rlan:/ into konqi, and get the configuration dialog running, and set it up, but when I put lan:// in, konqi says:\n\nlan://localhost not found.\n\n??????"
    author: "Chris Adams"
  - subject: "Re: lan:// browsing"
    date: 2001-02-12
    body: "isnt that smb:// ?\nWorks here if its access to samba/windows shares/workgroups you're after.."
    author: "Nils O. Sel\u00e5sdal"
  - subject: "Re: lan:// browsing"
    date: 2001-02-12
    body: "In KDE Beta 2, smb:// doesnt give you a workgrougp list. You have to type smb://PC1/root to access that share or smb://PC1/ to get a share listing, and lan:// for a list of machines"
    author: "Chris Adams"
  - subject: "Re: lan:// browsing"
    date: 2001-08-24
    body: "You need to have installed: kdenetwork, after you will need to configure lan settings in Madrake Control Center, and finally and the most import point, that you are missing.\nYou have to start lisa daemon.\n#/usr/bin/lisa --kde2\n\nnow you can point to:\nlan://localhost and you will get it :))))"
    author: "Anonymous"
  - subject: "KDE"
    date: 2001-02-11
    body: "KDE is stable and good looking but not as user friendly and intelligent and animated as other desktop environments... like Photon (window manager of QNX) and windows.\n\nKDE can have these but it is not implementing. I wonder why?\n\n1. Animated mouse pointers: like AMOR this can be created in a jiffy.\n\n2. Instance management: you can openup too many instances of any application which eats up whole memory. Windows on the other hand do not allow to create more instances above physical memory.\n\n3. Administration: Both system and network administration is a drawback or missing feature of KDE. Mandrake which is a great supporter of KDE is using Gnome's application for doing system and network configuration using Linuxconf, X configuration, font installation, hardware detection, and even menu management of Kicker!\n\n4. Securing: unlike windows, many application of KDE can mess things up.\n\n1. Kwrite/KEdit: can open any media (picture, audio, video) file and can change its content. \n\n2. Konsole can run system related dangerous commands which may lead to loss of user's data. like if you enter \"telinit 0\". all your work that is unsaved data is lost.\n\n3. Kpackage runs shellscripts/binary files, when clicked on the filelist view. which is not desirable.\n\n\nDelaying the release is welcome. What we expected in KDE 2.0 is what we are going to get in KDE 2.1."
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE"
    date: 2001-02-11
    body: "I'm only going to comment on a couple of these points, because the others don't affect me at all, OR are good ideas.\n\n5) KWrite/KEdit opening any file\n\nYou suggest that editors should be LIMITED in what they can edit?  This is a very bad idea.  If a user opens a non text file and start typing random things, this is his own fault.  Don't restrict people who know what they're doing because some people don't.\n\n6) telinit 0 in konsole\n\nYou also want to restrict what programs konsole will run?  First, to run telinit you need to be root (if you don't on your system, that is NOT KDE's fault, your OS is really screwed up).  Second, unless you have a very bad setup, init will send SIGTERM to all apps, which gives them a chance to save anything that might be open (among other cleanups) and terminate themselves.  Don't blame konsole for anything a stupid user does as root."
    author: "Joe KDE User"
  - subject: "restricted kwrite"
    date: 2001-02-12
    body: "kde-developer: dont restrict kwrite to any kind of files!!!!!!!\nplease: NO restriction in any application!!!!!!!\n\nwass soll der scheiss! nur micro$oft denkt, dass der benutzer zu bloede ist und darum davon abgehalten werden muss nicht-text dateien zu aendern.\nwenn ich immer erst umstaendlich irgendwas umstellen muss um ne file zu laden, dann werd ich kwrite nich mehr benutzten.\n\nso einen schwachsinn hab ich echt noch NIE gehoert!!!!\n\ngood work!\nthnx for kde2.\n- gnu128"
    author: "gnu128"
  - subject: "Re: KDE"
    date: 2001-02-12
    body: ">       KDE can have these but it is not\n> implementing. I wonder why?\n<BR>\n>         1.Animated mouse pointers: \n>  like AMOR this can be created in a jiffy.\n<BR>\n>          2.Instance management: you can openup    <BR>\n> too many instances of any application which eats\n<BR>\n>  up whole memory. Windows on the other hand do <BR>\n> not allow to create more instances above <BR>\n>  physical memory. <BR>\n\nI don't understand exactly what you mean, but I'll add a few comments: <BR>\n1)  Memory management of KDE (and Linux) is much better than Windows IMHO.   <BR>\n2)  Be careful when getting the memory from top (or kpm, KDE's process management tool).  The memory it lists for each application includes the shared memory so it looks like apps are being bigger memory hogs than they really are.  This is especially true in KDE2 from what I've seen. <BR>\n\n>         3.Administration: Both system and <BR>\n> network administration is a drawback or missing <BR>\n> feature of KDE. Mandrake which is a great <BR>\n> supporter of KDE is using Gnome's application <BR>\n> for doing system and network configuration using <BR>\n> Linuxconf, X configuration, font installation, <BR>\n> hardware detection, and even menu management of <BR>\n> Kicker! <BR>\n\nPart of this is distro specific.  There isn't a single administration tool under Linux that is truly excellent, although a think Caldera's is the most interesting.\n <BR>\n\n> 4.Securing: unlike windows, many application of <BR>\n> KDE can mess things up. <BR>\n\nI have absolutely no idea why you would think this.  You've obviously never had an application screw up and hose your Windows Registry. Unlike Windows (SE or older) where any user can screw up the Windows system, Linux/Unix systems are protected by restricting actions to a root (or other something like \"admin\") account.  A user should NOT be using the root account for regular usage.\n<BR>\n\n> 5.Kwrite/KEdit: can open any media (picture, <BR>\n> audio, video) file and can change its content. <BR>\n\nThe ways to manipulate text varies dramatically from how you would manipulate the image.  Having a single tool do both is silly.  Having a single tool imbed the other tools, like KOffice, makes sense, and is being done.\n\n>         6.Konsole can run system related <BR>\n> dangerous commands which may lead to loss of <BR>\n> user's data. like if you enter \"telinit 0\". all <BR>\n> your work that is unsaved data is lost. <BR>\n\nYou can only do this if you're root.  If you are stupid enough to type \"telinit 0\" while you are logged in as root, then you shouldn't be root.\n\n> 7.Kpackage runs shellscripts/binary files,  <BR>\n> when clicked on the filelist view. which is not <BR>\n> desirable. <BR>\n\nThis is more of an issue of the fundamental package systems being used by Linux (rpm or pkg).  If you don't want to run scripts by installing an rpm, then go download the source and build it from scratch.  The ability to run scripts is very essential to the whole concept of packages."
    author: "SK"
  - subject: "Re: KDE"
    date: 2001-02-12
    body: "1. They're pretty much useless. They'll arrive eventually but they're not exactly urgent...\n\n2. What on earth are you talking about? I don't think you really know, if you do then please explain.\n\n3. A system admin front end isn't really our job - we run on a very wide range of UNIX platforms for one thing. I guess this will come along at some point, but I think you'll see tools provided by your distro integrating with KDE much more quickly. WRT Kicker, there have been tools to manage the menus since before 1.0.\n\n4. What do you mean?\n\n5. This is a feature not a bug.\n\n6. It is a console, what do you expect it to do? To give an example from your book: you can run 'format c:' in a Win32 DOS prompt.\n\n7. There aren't any Linux package formats which are totally data driven at the moment, so I don't see how this can be avoided. I agree it's not ideal, but I can't see a way to avoid it without sacrificing compatability."
    author: "Richard Moore"
  - subject: "Sweet! (Score:5, Troll)"
    date: 2001-02-12
    body: "The title says it all! ;)"
    author: "Stentapp"
  - subject: "Re: Sweet! (Score:5, Troll)"
    date: 2005-02-23
    body: "first post!\nGNAA"
    author: "Anonymous Coward"
  - subject: "Re: KDE"
    date: 2001-02-12
    body: "Instance management: Why shouldn't I be able to open up as many instances as I wish?\n\nAdministration: Not everything running KDE will be Linux. Or even a PC. KLinuxConf on a SPARC Solaris would be as useless as tits on a boar.\n\nSecuring: unlike windows...: You saying that every Windows app is well behaved? Hell, not there's a lot of MS issued apps that can screw up your system in a heartbeat.\n\nKwrite/KEdit: If you don't let me do it in KWrite, I'll just go do it in vi. I want KWrite/KEdit to have at least the basic functionality of a text editor. And that means they can edit XPM files. \n\nKonsole: I want Konsole to have at least the basic functionality of xterm and rxvt. That means I get to run telinit.\n\nConclusion: three instances where you want to eliminate the user's choice of action, one instance where you want to eliminate the user's choice of software, and one instance where you want to eliminate the user's choice of hardware or operating system."
    author: "David Johnson"
  - subject: "Re: KDE"
    date: 2001-02-13
    body: ">Animated mouse pointers: like AMOR this can be >created in a jiffy.\nThe point of this is to annoy the user?\nSuppose this can be done anyway... you\nactually tried?\n\n\n>Instance management: you can openup too many >instances of any application which eats up whole >memory. Windows on the other hand do not allow >to create more instances above physical memory.\nIsnt that up to the user?\nAnd on my windows machine.. i sure can\npop up a few dozen of IExplorers ,Excels \nthat uses 400MB of my 160 MB's...\n\n\n>Administration: Both system and network >administration is a drawback or missing feature >of KDE. Mandrake which is a great supporter of >KDE is using Gnome's application for doing >system and network configuration using >Linuxconf, X configuration, font installation, >hardware detection, and even menu management of >Kicker!\nYup... but it's getting there.. many\napplications for sysadmins to come..\n\n>Securing: unlike windows, many application of >KDE can mess things up.\nNothing has EVER messed up things on a computer\ni've seen like IExplorer and office 2000 occasionly does to me...\n(ad i suppose the ILoveU virus was neither\na windows application, nor did it mess things up ?)\nNot to mention Dr. Watson that wisits 3-4 times\na week...\n\n\n>Kwrite/KEdit: can open any media (picture, >audio, video) file and can change its content.\nOh?.. I can very well open jpeg/mp3/zip whatever\nfiles in kwrite...(right click a file, choose\nOpen With, select Advanced Editor or Kwrite...)\n\n>Konsole can run system related dangerous >commands which may lead to loss of user's data. >like if you enter \"telinit 0\". all your work >that is unsaved data is lost.\nWhat do you want ? a fool prof OS.. id not\nbe using linux if i couldnt do what i wanted\nwith it.. You are joking about thisone?\n(You are not one of those who is always\nlogged in as root ? )\n\n\n\n>Kpackage runs shellscripts/binary files, when >clicked on the filelist view. which is not >desirable.\n\nBecause?"
    author: "Nils O. Sel\u00e5sdal"
  - subject: "Re: KDE"
    date: 2001-02-16
    body: "The only one that makes any sense whatsoever here is #1. Everything else is either \n<ul><li>Designed to make life difficult by limiting actions in such a way as to confound the user without providing any advantage (adduser, anyone?): #5, #6, #7</li>\n<li>Totally dogmatic : #2, #4</li>\n<li>Utterly untruthful : #3 (DrakConf is NOT a GNOME app, and system configuration details are NOT the job of a DE, other then perhaps by providing a front-end. Now, where is the kde app that provides any easy way for OS developers to put front-ends for their system configuration apps, hmm, how about kcontrolcenter, which has been there since pre 1.0, providing front-ends for such awesome apps as kjoystick)</li></ul>\nAnd all of this is pointless anyways, because requests like these will make no difference unless they are done in a non troll-like way, and put into their respective catagories in the bug database!<br><br>\nBTW, what \"we\" are you referring to?"
    author: "Carbon"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "I couldn't resist to change Mandrake's ugly splash screen with this one."
    author: "Baracuda"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "That IS a beautiful splash screen!"
    author: "Joe Black"
  - subject: "Mandrake graphics"
    date: 2001-02-12
    body: "I installed Linux-Mandrake 7.2 Complete yesterday, because I wanted to see what it was like before I sent it to my dad.  (I use Debian myself)  \n\nI was a bit surprised at the amateurish graphics they've decided to use in this distribution when compared to the KDE2 originals.  The Mandrake GTK applications are definitely ugly too, uneven and unpolished (sorry).  Quite useful some of them, though.  We should see a port to KDE for many of these utilities... There was no automatic sound config in this distribution, btw...  My dad simply\ncan't wait for the next release of OpenLinux so I had to get Mandrake for him.  ;)\n\nThe KDE splash screen is certainly not the one used in Mandrake, and all those ill-defined penguins are Mandrake's own doing.  ;)\n\nOn the whole, I like Mandrake enough (they make very good use of KDE2 by default, the only distribution to do this currently) that I'll send it to my dad so that he can play around, and I'll keep around an install.  Caldera OpenLinux 2.4 is definitely much more polished though, and I have no doubt that an updated version will be absolutely killer.\n"
    author: "KDE User"
  - subject: "Re: Mandrake graphics"
    date: 2001-02-12
    body: "In Mandrake 7.2 sndconfig isn't installed automatically. Simply do a \"urpmi sndconfig\" :)"
    author: "Haakon Nilsen"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "Ok, I'm dying to know HOW you changed the default Mandrake KDE splash screen to the beautiful one you created!\n\nTell me! Tell me! Tell me! (grin)\n\nThanks,\n\nSteve"
    author: "Stitch"
  - subject: "spalshscreen"
    date: 2001-02-12
    body: "Go to /usr/share/apps/ksplash/pics and replace the ugly bad-taste-penguins from mandrake (make a backup of it...). Give the new pic the same rights as the originbal one.\n\nAnd yes, its uggggly...."
    author: "Geir Kielland"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "Is there a possibilty that KDE could use pixel based scrolling, the line based gives me a bad headache.  Is this a the Qt peoples job or the KDE's?"
    author: "robert"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-12
    body: "Scrolling in khtml (konqueror) is definitely pixel based. Are you talking about an application in particular ?"
    author: "David Faure"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-13
    body: "Apps in particular."
    author: "robert"
  - subject: "Now, what about attention to details?"
    date: 2001-02-12
    body: "Yes, it's great, and it's definitly better than what exists.\nbut why does it needs to look like windows, and why little things aren't tought of?\n\nLike, in *every* KDE application, when there is a list, the columns jumps all over the place, seemingly 'to fit' what is to display.\nreally it's anoying, I'd like it to save my prefered view and stop doing that.\nSame applies to dialogs and lists, which are either too small, or too big.\n\nThere are *very* little improvements that would make it looks as good as macos. Instead of going for huge leaps in hoggling technologies, I'd rather see a pause in that, and some work done on a few details."
    author: "BusError"
  - subject: "The Problem With Kde art"
    date: 2001-02-13
    body: "Is that every one of kde icons and pics has a black outline. Why? \n\nThis makes all the icons butt ugly.\n\nIt was ok with kde1 since color was not emphasised. In kde2, the finish on the widgets is so smooth. But the icons are still not of photo quality like in macos x, windowmaker, and that other open source desktop.\n\nget rid of the black outlines. Tackat should really do something cool this time around.\n\nI hope the new icons will be better.\n\nCheers"
    author: "Psycogenic"
  - subject: "Re: The Problem With Kde art"
    date: 2001-02-13
    body: "Is icons the only thing you care about?."
    author: "KDE USER"
  - subject: "Re: The Problem With Kde art"
    date: 2001-02-13
    body: "I think this really is a thing where gnome is long ahead, although it's not terribly important. But I think more stylish icons would give kde a nice finished look."
    author: "Matti Pajari"
  - subject: "Re: The Problem With Kde art"
    date: 2001-02-15
    body: "KDE has the highest-quality artwork I've ever seen in an OSS project - very professional indeed. As much as I admire TigerT's work in GNOME, it is not \"good\" UI practice to use those high-color but low-contrast images as icons. Ask anyone with bad vision. Tacket rocks!"
    author: "Eron Lloyd"
  - subject: "Re: The Problem With Kde art"
    date: 2005-04-07
    body: "Ask anyone with good vision. Tacket sucks!"
    author: "YesPapa"
  - subject: "Re: The Problem With Kde art"
    date: 2005-04-07
    body: "> Ask anyone with good vision.\n\nThat's obviously not you.  You missed the right \"reply to this\" link. ;-)\n\n"
    author: "cm"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-22
    body: "keep up the quality work!\nthanks and cheers to the team"
    author: "rbjr"
  - subject: "Re: KDE 2.1 Release Delayed 1 Week"
    date: 2001-02-23
    body: "I am running both  Caldera OpenDesktop 2.4\n\nand Mandrake Linux 7.2\n\nWill there be a CD released that will cover both\nof these versions of Linux?"
    author: "Bruce Stanley"
  - subject: "Convert me, please."
    date: 2001-02-23
    body: "First, let me say I am really excited for KDE 2.1.  \nI do all my gui programming with qt, and I feel that KDE is much snappier to use than gnome, yet I still use gnome.\n\nThere are couple of problems I have with KDE, that though I have tried several times to use it, I always go back to gnome/sawfish.  One problem is configurability of the panel and window manager.  The second is the look of the icons. I will elaborate.\n\nFirst, the configurability.  Some issues I have:\n\t- Everything being single click with that ugly line (I can't choose the way I want)\n\t- I can't split the panel up into parts.\n\t- The pager leaves much to be desired, though much improvement in 2.0 series.\n\t- If you look at sawfish, enlightenment, or fvwm2, they all had a _lot_ of control on what happened when you click, double-click, or modifier-click parts of the title bar, window, borders, etc.  This type of control is really necessary for me.  Once you set something up the way you like it, you need to keep it.  I started out with fvwm2, and moved to enlightenment, then to sawfish, and only because each of them provided that level of control.  Sawfish has really added a lot in this area.  They also all allow button addition and subtraction very easily, and every button can do whatever you tell it to.\n\nSecondly, I feel that KDE 2.0/2.0.1 has made _major_ improvements in the icon look department, but has a long way to go to look as good as gnome.  The blocky, busy, look of the KDE icons really wear on me.\n\nIn conclusion, please don't take this wrong.  I am sincerely looking to move to KDE, it is just that when you have something that you like, it is hard to live without that functionality, even if you want to change over.  I really want to use KDE for several reasons, I just am interested if anyone else has encountered these feelings, has any suggestions, and can help me get into KDE.  I would also be interested to know if the KDE team has tackled any of these issues and decided to exclude them for a reason, or if these things have not been requested before.\n\nSincerely,\n\nJim Wray\nwray@byu.edu"
    author: "James Wray"
---
David Faure, our release manager, has <a href="http://lists.kde.org/?l=kde-core-devel&m=98149878126562&w=2">announced</a> the latest KDE 2.1 release schedule.  KDE 2.1 will now be officially announced Feb 26, mainly due to popular demand and the fact that the <a href="http://artist.kde.org/">KDE Artist Team</a> is hard at work polishing up this release.  For a preview of what's to come, check out the new <a href="http://master.kde.org/~me/kcontrol.jpg">KControl design</a>.  Definitely seems worth the wait!


<!--break-->
