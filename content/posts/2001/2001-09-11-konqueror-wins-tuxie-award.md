---
title: "Konqueror wins Tuxie Award"
date:    2001-09-11
authors:
  - "Inorog"
slug:    konqueror-wins-tuxie-award
comments:
  - subject: "Yawn!"
    date: 2001-09-11
    body: "Awards,awards. Day in, day out, week after week, it's always the same: awards, awards, awards.\nCan't you guys dumb it down a little so we can have a rest? ;-)"
    author: "reihal"
  - subject: "Re: Yawn!"
    date: 2001-09-11
    body: "I wholehartedly have to agree! You KDE guys should please concentrate more on big press releases and generating hype than coding and earning awards for it ;) ;) !!!!"
    author: "AC"
  - subject: "please....correct png support"
    date: 2001-09-11
    body: "Konqueror is already a wonderful FileManager/Finder/Explorer.\n\nI hope Konqueror/KHTML will be one day (soon?) an example of a web browser that is standards compliant... like gecko based ones (Mozilla, Galeon, Skipstone, K-Meleon, Netscape6) are.\nIMHO, standard compliance is something important for the image of KDE.\nI think the lack of PNG support is unfortunately quite important in this scope.\n\njust a wish."
    author: "oliv"
  - subject: "Re: please....correct png support"
    date: 2001-09-11
    body: "What are you talking about? We've supported PNG for *ages*. We also support the MNG format. If it's missing for you then the chance are you've compiled your Qt library with png support disabled.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: please....correct png support"
    date: 2001-09-14
    body: "actually, some features are still not support. its not an 100% thing."
    author: "RAJAN"
  - subject: "Re: please....correct png support"
    date: 2001-09-11
    body: "png was supported since KDE1, and is largely used by all KDE programs (i believe all kde icons are in png no?).\nwell anyway no worries. check you have libpng before you compile QT."
    author: "emmanuel"
  - subject: "Re: please....correct png support"
    date: 2001-09-11
    body: "I think the author was talking about full PNG support ... last time I checked the alpha channel in PNG wasn't supported ( alpha under 127 was transparent and >= 127 was visible )."
    author: "Andrea Albarelli"
  - subject: "Re: please....correct png support"
    date: 2001-09-11
    body: "Qt 3 uses the XRender extension to provide hardware accelerated alpha channel support. This means that the alpha information in an image with will be applied automatically when the image is drawn.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: please....correct png support"
    date: 2001-09-11
    body: "I wonder though, this X-Render thing is supposed to be so cool, but does it act on changes behind the alpha blended image?  Because Mosfet's transparent menus do not update when a window beneath it (xchat for example) changes. \n\nNow this usually isn't an issue with websites, but I was just wondering, is this a limitation of the X-Render, or just lazy coding from Mosfet (or anyone who uses the X-Render code)?"
    author: "Richard Stellingwerff"
  - subject: "Re: please....correct png support"
    date: 2001-09-12
    body: "I may be wrong, but I think that the X-Render extention it's not yet in place.\n\nIf I remember well, Mosfet make transparent menus by first saving in a buffer the image behind the menu and then showing it as the background of the menu; that is why it doesn't get updated when the real background does."
    author: "anonymous"
  - subject: "Re: please....correct png support"
    date: 2001-09-12
    body: "It is wrong. XFree has full support for XRender since 4.1 on almost all drivers. \n\nQt3 supports XRender. So from now on we can use XRender in KDE.\n\nGreetings,\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: please....correct png support"
    date: 2001-09-11
    body: "Yep. KDE doesn't fully support transparent PNGs.\nJust look at http://www.libpng.org/pub/png/pngs-img.html with Mozilla and Konqueror or go to http://bugs.kde.org/db/20/20155.html ."
    author: "Marcus Camen"
  - subject: "Re: please....correct png support"
    date: 2001-09-12
    body: "Exactly what I meant :)\nThanks for precisingg my thought (for me, it was obvious, but it did not seem to actually be for everyone)\n\nTreating PNG without the alpha channel is like dispaying a colour image in grayscale. Of course we can see something, but it's just wrong."
    author: "oliv"
  - subject: "Try this example"
    date: 2001-09-11
    body: "Try this example PNG created with GIMP ... the center shourd be blended.\nTry saving it and loading with the GIMP ...."
    author: "Andrea Albarelli"
  - subject: "Re: Try this example"
    date: 2001-09-11
    body: "This example show few bugs:\n\n1. If you right-click and select \"New Window\" for the first time, the new window will show the binary data.\n\n2. The you just click and you will see a red box with a smaller white box in the center-\n\n3. Right click again and select \"New window\". You will see only a red box.\n\nPackage  konqueror 2.2.0.20010822-1."
    author: "Ricardo Galli"
  - subject: "Re: Try this example"
    date: 2001-09-12
    body: "Same for me - Konq. 2.2.0 Suse 7.1"
    author: "Matthias Wieser"
  - subject: "Re: Try this example"
    date: 2001-09-11
    body: "Shows a red border which is blended into a black rectangle in the middle. Dunno whether it's supposed to be black, tho, but the blending certainly works."
    author: "Frerich Raabe"
  - subject: "koffice got a tuxie"
    date: 2001-09-11
    body: "I subscribe to that magazine and if I'm not mistaked Koffice nadded a tuxie as well!  Way to go guys.\n-Adam"
    author: "Adam Black"
  - subject: "No full Java support and still get an award !"
    date: 2001-09-11
    body: "Amazing The Java (chat.yahoo.com) just won't work and many pages like Mandrake.com, www.nvidia.com won't look nice in Konqueror unlike Mozilla (perhaps missing fonts).\n\nI have sun Jdk 1.3.1, but still can't chat on yahoo with konqueror, though games.yahoo.com's chess work nicely. And many applets seems to stop after every second (perhaps Java problem).\n\nThe text entries in Konquror seems to lag! (when I enter my user name, or typing this text I see a delay in the text appear)\n\nAnd I have vowed not to chat with other browsers ;)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: No full Java support and still get an award !"
    date: 2001-09-11
    body: "all I can is, it works for me."
    author: "fault"
  - subject: "Re: No full Java support and still get an award !"
    date: 2001-09-11
    body: "You may want to check these sites out in other browsers as well.  Konqui uses the same Java that Mozilla uses.  I have tried chat.yahoo.com and it does not load in Mozilla or Konqui with Sun's or anyone's Java clients.  Yahoo seems to change their chat client often and have not made it to work with any java client except for IE's and Netscape 4.x's."
    author: "Eric Thomas"
  - subject: "Re: No full Java support and still get an award !"
    date: 2001-09-12
    body: "Yes, The applet error shows something related to Netscape javascript, after the applet get initialized.\n\nwhat bothers me is even when I use Netscape plugin and not just /usr/java/jdk1.3.1/bin/java (though path is included in profile); I see applet a bit faster but chat @ yahoo should also work."
    author: "Asif Ali Rizwaan"
  - subject: "Re: No full Java support and still get an award !"
    date: 2001-09-12
    body: "It shouldn't bother you.  They are one in the same.  The plugin is just another way of accessing the java you have installed.  It is not a different java than the one you installed, its the same exact one.  So if there is a problem with one, there should be a problem with the other one.  The only java i have seen work with this is the original java that comes installed with netscape 4.x."
    author: "Eric Thomas"
  - subject: "Re: No full Java support and still get an award !"
    date: 2001-09-12
    body: "I recommend you to try Blackdown's JRE.\n\nI remember playing some Nokia games on it."
    author: "Evandro"
  - subject: "Re: No full Java support and still get an award !"
    date: 2001-09-12
    body: "I agree.\nOther examples?\nwww.playsite.com\nwww.gazzetta.it"
    author: "cristian"
  - subject: "Computer Lab Requirement"
    date: 2002-12-18
    body: "Networking in Linux Softwatre"
    author: "mukesh Srivastab"
  - subject: "Nifty-rama"
    date: 2001-09-11
    body: "Yep, definetly a well-deserved award. Konqi only needs to keep going down the path it's already on to become even more superior to the closed-source competition! Devel guys, just keep on improving what works very well, and keep on working on the newer projects on the block. After all, whatever you've been doing so far, it's been most successful!"
    author: "Carbon"
  - subject: "SVG?"
    date: 2001-09-11
    body: "Are there any plans regarding SVG support?"
    author: "Joergen Ramskov"
  - subject: "Re: SVG?"
    date: 2001-09-12
    body: "I'd be interested in this too - would be very very useful for me"
    author: "Shane Wright"
  - subject: "And MathML?"
    date: 2001-09-12
    body: "Very important..."
    author: "jsantos"
  - subject: "Re: SVG?"
    date: 2001-09-12
    body: "Yes, I would be very interested in knowing if some work is done in this area. :)"
    author: "altrent"
  - subject: "Re: SVG?"
    date: 2001-09-12
    body: "Yes, there is work going on with the project ksvg in kdenonbeta.\nThere is still a lot of work to do, but a reasonable start has been\nmade so that at least a few simple svg examples work. The most work\nhas still to be done in the rendering engine, text/font/glyph and css\nsupport.\nThe development has been slowed a bit lately, but we hope to continue\nfrom next week on. If anybody wants to help they are always welcome ofcourse."
    author: "rwlbuis"
  - subject: "Re: SVG?"
    date: 2001-09-13
    body: "As I remember it will be natively supported as a second metafile format in Qt 3.0"
    author: "Krame"
  - subject: "Well deserved!(but)"
    date: 2001-09-12
    body: "The Konqueror team really deserves this. Congratulations!\nI'm always anxiously awaiting the next release to check the improvements, however in many cases only to find out that I still can't use it for all sites I visit regularly.\nKeep up the good work, guys, so one day when I check a new version I can say \"yes! no more Explorer, no more Netscape!\" :-)\n\nAlex"
    author: "Alexander Kuit"
  - subject: "Well done."
    date: 2001-09-12
    body: "Kongratulations!"
    author: "KDE User"
  - subject: "Re: Well done."
    date: 2001-09-12
    body: "Konqratulations ! ;-)"
    author: "aleXXX"
  - subject: "where do you find ......"
    date: 2001-09-12
    body: "the plugins for konq and the IO slaves?"
    author: "Jeremy Petzold"
  - subject: "Re: where do you find ......"
    date: 2001-09-12
    body: "If you mean the audio io slave\n\nyou need to compile kdebase with libvorbis support and cdparanoia using ./configure --your requirement"
    author: "dids"
  - subject: "Re: where do you find ......"
    date: 2001-09-12
    body: "If you mean plugins in general, there isn't really a site to get them from, since there aren't any stable at the moment that aren't already a part of KDE itself (although there might be some in devel I don't know about, try apps.kde.com). You'll find all the known ones in either kdebase or kdeaddons."
    author: "Carbon"
  - subject: "Wrong link"
    date: 2001-09-12
    body: "Why do link to september issue of Linux Magazine when you can not read anything there about award?"
    author: "antialias"
  - subject: "Javascript support in 2.2.1?"
    date: 2001-09-12
    body: "I finally figured out how to get CVS from the KDE_2_2_1_RELEASE branch and get it built.  It's beautiful so far.  The only problem I'm running into with this build (just built day-before-yesterday - since then only a single file has been changed in kdelibs and non in kdebase) is with \"javascript:someFunctionOrOther(somedata)\" style links, and related things, which seems to be an on-again, off-again problem in Konqueror.  Anybody know if 2.2.1 is still being worked on, or is it a \"done deal\" at this point being packaged up for the distributors?  (Trying to decide if it will be helpful or just annoying to report this as a bug in pre-2.2.1 at this late stage...)\n\nOther than this one thing (which crops up on websites just often enough to be irritating), though, Konqueror is running just plain beautifully for me on this build..."
    author: "DrDubious DDQ"
  - subject: "Please post a few less \"Me To!\" and \"Yeehaws\""
    date: 2001-09-13
    body: "I shurely do understand that we are all very happy about KDE, but it is SO uninterresting to go to a news item where alle of the replies are like:\n\nYo this is great\nYou guys rock\nKDE rules\nAren't we all great (\"pad\" \"pad\")\n\nIt would be so more interresting if the people responing had something more relevant to say ... please.\n\nKind regards"
    author: "Max M"
  - subject: "Re: Please post a few less \"Me To!\" and \"Yeehaws\""
    date: 2001-09-13
    body: "I can vouch for the fact that, since I am getting all this for free, I like to thank the developers.  Since this is the only forum that can reliably reach them (with the exception of things like the developer lists, which would disrupt their communication), I tend to thank them here when a new release or award is won.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Please post a few less \"Me To!\" and \"Yeehaws\""
    date: 2001-09-13
    body: "This is not a site for that, I mean to say something relevant. This is official kde-news site and people get informations about kde-related things here. And this is not right place to \"think different\" because there are some other places to think different:\n\n1. not satisfied with something = do it yourself\n2. you have wishes = bugs.kde.org\n3. something doesn't work = a) it works here, or b) your distro sucks or, c) you are stupid, or d) send a bug-report\n4. if you don't do as mentioned under 1,2,3 = you are troll\n\nBest regards,"
    author: "antialias"
  - subject: "konqi2.2 wouldn't have won any prize, if"
    date: 2001-09-14
    body: "there had been konqi2.2.1 in the game. :-) This one is really BIG.\n\nThank you"
    author: "Michael Hanke"
  - subject: "yeah, yeah"
    date: 2001-09-19
    body: "Give me a break,\n\ni run a low end machine (200 mmx, 64 mg ram). konqueror is both slow and tedious to use. \n\nSure, all you folks with ball busting machines might be happy but what about us poor guys with average gear?\n\nOn a low end machine this happens frequently;.\n\na. lock ups are very frequent (can be renovated with Xkill)\nb. programmes sometimes just will not start.\nc. things just don't wotk properly like Mandrake update etc. Any cpu intensive stuff invariably screws up.\n\nlost the plot?\n\ng"
    author: "bitch"
  - subject: "Re: yeah, yeah"
    date: 2001-09-21
    body: "Gee, I wish it ran fast on my XT too..."
    author: "Greg"
  - subject: "Re: yeah, yeah"
    date: 2004-01-25
    body: "time to upgrade boy.  my p3 600 smokes w/ mdk 9.0.lol.\n\nkonqueror will never be a contender with the masses (just the way i like it) until you don't have to spend hours trying to figure out java support etc. (please don't tell me where the executable goes tyvm!)\n\nnevertheless,  i use konqueror every day.\n\nit really has the potential to be a stellar product instead of just a great product.  if only it's coders could stop looking at porn and figure out an easy solution for the FUCKING GODDAMN JAVA SUPPORT.\n\t"
    author: "servitor of evil"
---
In the <A HREF="http://www.linux-mag.com/2001-09/toc.html">September 2001 issue </A> of <A HREF="http://www.linux-mag.com/">Linux Magazine</A>, <A HREF="http://www.konqueror.org/">Konqueror</A>, the KDE all-in-one <EM>wunder kind</EM>, has been awarded the <i>Tuxie for Best Web Browser</i> as part of the 2001 Editors' Choice Awards (the Tuxies). This new honour adds to the impressive <a href="http://www.kde.org/awards.html">list of awards</a> that the KDE developers have earned in what seems now like many years of dedication and persistence. Congratulations to the hard-working developers who brought us Konqueror, KHTML, KJS, NSPlugins, Internet Keywords, native plugins and all the other marvelous technologies which we know and love as <EM>Konqueror</EM>.
<!--break-->
