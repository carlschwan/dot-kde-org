---
title: "People of KDE: Rinse de Vries"
date:    2001-02-05
authors:
  - "Inorog"
slug:    people-kde-rinse-de-vries
comments:
  - subject: "Re: People of KDE: Rinse de Vries"
    date: 2001-02-05
    body: "This is a great series of interviews. It has really made clear to me the committment and effort the KDE team puts into this project. For instance, many of the most critical functions are performed by people who've never met another member of the KDE team in person."
    author: "Michael O'Henly"
---
<a href="http://www.kde.org/people/rinse.html">Rinse de Vries</a> is a Dutch cook. He's also the leader of the team which translates the KDE interface to Dutch. He's this week's <a href="http://www.kde.org/people/rinse.html">guest</a> of <a href="mailto:tink@kde.org">Tink</a> in the <a href="http://www.kde.org/people/people.html">People behind KDE</a> series of interviews. You might have noticed the last time that Tink focused on KDE buddies working on translations. And for a good reason. One of the main strengths of our project is its ability to let people from all over the world be comfortable with their computers without learning a foreign language. One other interesting aspect about translators is that, compared with the code developers (for example), they have even more diverse real-life occupations and extremely interesting anecdotes about how they came to the KDE community. Thank you, Tink, for this new excellent interview.


<!--break-->
