---
title: "KDE Dot News: We're Back, Sorta"
date:    2001-07-31
authors:
  - "numanee"
slug:    kde-dot-news-were-back-sorta
comments:
  - subject: "ext3?"
    date: 2001-07-30
    body: "You're using ext3 on a production system?\n\nWell, umm, I guess it's your choice, but your admin won't be getting anywhere near _my_  servers. :)\n\nLet us know how it runs after 6 months though. :)\n\nCheers,\nJason."
    author: "Jason Tackaberry"
  - subject: "Re: ext3?"
    date: 2001-07-30
    body: "rpmfind.net has been using it for almost a year now... just fwiw.\n\nAnd they certainly move some serious bits."
    author: "Kevin Puetz"
  - subject: "Re: ext3?"
    date: 2001-07-30
    body: "I'd suggest to use something well-tested instead: ReiserFS."
    author: "Anonymous"
  - subject: "Re: ext3?"
    date: 2001-07-30
    body: "It's more tested, true, but I still don't feel comfortable putting it into production.  I found a bug (just last week in fact) after about a day of testing.\n\next2 may suck, but it sure does have a whole lot of testing behind it.\n\nCheers,\nJason."
    author: "Jason Tackaberry"
  - subject: "Re: ext3?"
    date: 2001-07-30
    body: "Were you using reiserfs on a 2.4 kernel or a 2.2 kernel?  I have used reiserfs very successfully on heavily used production machines using custom 2.2 kernels for over a year now, and have never encountered any bugs.  Its been compiled into the SuSE kernels since the 6.x series.\n\n2.4, on the other hand, seems a little flakey in many respects.  I still refuse to use it on any sort of production machine."
    author: "bah"
  - subject: "Re: ext3?"
    date: 2001-07-30
    body: "ReiserFS has been on my systems for over a year now, and I have not encountered any problems with it thus far. I might add, I've actually had several problems with ext2. :-)\n\n -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: ext3?"
    date: 2001-07-31
    body: "I use ReiserFS. One day, I shred a text file, and then some mp3 files are gone. Anyone knows what happened?"
    author: "reza"
  - subject: "Re: ext3?"
    date: 2001-07-31
    body: "The RIAA snuck in and deleted them while you weren't looking."
    author: "Matt"
  - subject: "Re: ext3?"
    date: 2001-07-31
    body: "I don't know what \"shring\" is, maybe your \"shring\" program has a bug?"
    author: "Carg"
  - subject: "Re: ext3?"
    date: 2001-08-04
    body: "I didn't find the word \"shring\" on this page in any other comment than yours. Where did you read it?"
    author: "Rolf"
  - subject: "Re: ext3?"
    date: 2001-08-13
    body: "OK, I admit, my fault. Maybe I just use \"shredded\" instead. Bad english."
    author: "reza"
  - subject: "Re: ext3?"
    date: 2001-08-04
    body: "You say run ReiserFS, but that is the worst choice you can ever make. ReiserFS is not designed as a JFS. It has now JFS-support but its not working right. If your system crashes the blocks of the open files are mixed and you end up with wrong data. I don't think you want that and it was stupid of Linus to ignore Alan and putting ReiserFS in the mainstream kernel. We have seen enough of these problems (kernel-developers have seen them also) to not use ReiserFS until these problems are fixed. Ext3 is the journaling layer for ext2 and is doing a better job. And for a performance increase you don't have to switch. Ext3 will go to the mainstream kernel soon (currently, patches are available and also in the ac-patches if I'm not mistaken)."
    author: "Hans Spaans"
  - subject: "Re: ext3?"
    date: 2001-07-30
    body: "You'll see it'll work just fine.\n\nI've been using ext3 on my normal workstation (read: heavy disk activity because of constantly running makes and gccs) for 6 months without any problems.\n\nThe latest version of the ext3 patch has been running on our test machines with heavy load tests for a week, and didn't cause any problems.\n\nSome more reasons why I've used ext3 rather than any other JFS:\n\n- You can convert ext2 to ext3 without data loss\n- The userland recovery tools are the same as for ext2, therefore they've had sufficient real-world testing and bugfixing\n- Same for the filesystem itself - basically ext3 = ext2 + journaling code\n- ext3 has been audited for security problems, memory leaks and bad VM interactions\n- ext3 has had a 14 months beta phase, and was in production use on rpmfind.net and others even before that.\n- I have direct access to the main ext3 developer, so if something should go wrong, as unlikely as this is, it'll both be recovered and help getting the problem fixed\n\n\nI'm not saying the other JFSes are bad, but there are good reasons to use ext3."
    author: "bero"
  - subject: "Re: ext3?"
    date: 2001-07-30
    body: "Another reason:\n -ext3 is controlled by RedHat\n\n AC"
    author: "Anon"
  - subject: "Re: ext3?"
    date: 2001-07-30
    body: "Umm no, not a reason.\nIf I insisted on using Red Hat controlled stuff, I'd be using <puke>gtk</puke> ;)"
    author: "bero"
  - subject: "Re: ext3?"
    date: 2001-07-30
    body: "Oh come on, let's have a flamewar on dot.kde.org on the right journaling filesystem to use ;) ! That would something different, hehe.\n\nSeriously, I don't mind which filesystem, hopefully the dot is up longer from now on (51 was already good in comparision to earlier times)."
    author: "AC"
  - subject: "Re: ext3?"
    date: 2001-07-30
    body: "ok... how should we start? My JFS is better then your JFS!!! ;-)\n\n -Tim"
    author: "Timothy R. Butler"
  - subject: "JFS Flamewar!"
    date: 2001-07-30
    body: "No, *MY* JFS is longer than your...\n\ner, never mind."
    author: "Jens"
  - subject: "Re: JFS Flamewar!"
    date: 2001-07-31
    body: "I don't know about that... If your JFS isn't ReiserFS, lets look at the figures:\n\nReiserFS (8 char. long)\next3     (4 char. long)\nJFS (sgi)(3 char. long)\n\n  Clearly ReiserFS is longest of all!!!! :-) \n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: JFS Flamewar!"
    date: 2001-07-31
    body: "Remember... it's not the length of your JFS name, but how you use it."
    author: "AC"
  - subject: "Re: JFS Flamewar!"
    date: 2001-07-31
    body: "> Clearly ReiserFS is longest of all!!!! :-)\n\nI use ReiserFS-3.6.25 [15 chars] so I win anyway :-)"
    author: "Jens"
  - subject: "Re: JFS Flamewar!"
    date: 2001-08-01
    body: "<sigh> I guess I better get a build newer then 3.5.29-0 so I can have the longest JFS. :-)\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: JFS Flamewar!"
    date: 2001-08-01
    body: "You realize you need a driver license for it then, do you?\n\n[I wonder if anyone else is gonna understand this one]"
    author: "Jens"
  - subject: "Re: JFS Flamewar!"
    date: 2001-08-04
    body: "I dunno, I bet only a few KDE list veterans would remember that important piece of information. What about this, why don't we co-author a paper so that people will know that they must have a drivers license to operate a PC? :-)\n\nHey, maybe we better not spread the word... You can make a lot more on fines if people don't know that they are violating rules. <g>\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: ext3?"
    date: 2001-07-30
    body: "My mommy can beat up your mommy :).\n\nSeriously, I've been using reiserfs on all my installs now (for me and friends) and none of us have encountered any problems. Of course we're not running make 24/7 either but these machines all remain up and up all the time (save for power outages everytime there's storms at one location :(  ). \n\nI'll have to check out ext3 though, on my main workstation as   /home is still ext2 (didn't want to lose 6gb of music), although I have backed up the rarer ones offsite :)\n\nUptime 38 days 13 hours 46 minutes <=bragging rights. Uptime is since the install of Mandrake 8.0=>"
    author: "Joseph Nicholson"
  - subject: "Re: ext3?"
    date: 2001-07-31
    body: "I have a production system that stores a lot of data, currently around 30 gig and this grows on average by 20 meg every 5 mins (if you're interested it's a netflow collection system.\n\nThis thing was powered down by accident about a week ago and because of all the data took over 30 mins to run fsck, ext3 is something worth while here but are there any FAQs / Howtos that cover migration to ext3 from ext2 ??"
    author: "DavidA"
  - subject: "Re: ext3?"
    date: 2001-07-31
    body: "I don't think there's any detailed documentation on it - but simply updating e2fsprogs and kernel to versions that support ext3, then running\n\ntune2fs -j /dev/hda1 (replace hda1 with your partition)\nvi /etc/fstab (replace ext2 with ext3)\numount /whatever\nmount /whatever\n\ndoes the job."
    author: "bero"
  - subject: "Re: ext3?"
    date: 2001-07-30
    body: "*sight*\nYet another example of how you guys always complain about everything related to the FSF...\n(FSF <-> GNOME <-> RedHat <-> Ext3)"
    author: "ac"
  - subject: "Re: ext3?"
    date: 2001-08-04
    body: "Euh... ext3 is controlled by Alan, owkee that he works for RedHat. Someone needs to pay Alan for his work :)"
    author: "Hans Spaans"
  - subject: "Someone needs to get a grip here"
    date: 2001-07-31
    body: "Dear All,\n\nAm I the only one to wince everytime dot.kde.org.goes down? How can the primary KDE advocacy website only manage an uptime of 51 days - hey, I've seen longer from an NT box :)\n\nGreat software is not just great coding, the marketing dudes need their voice listening to too.\n\nCheers,\n--Db--"
    author: "DildoBaggins"
  - subject: "Re: Someone needs to get a grip here"
    date: 2001-07-31
    body: "> hey, I've seen longer from an NT box :)\n\nI always thought that NT has a maximum uptime of about 49 days, because afterwards an internal counter produces an overflow.\n\nOr wasn't that NT?"
    author: "Anonymous"
  - subject: "Re: Someone needs to get a grip here"
    date: 2001-07-31
    body: "I guess it was Win98 - though I wouldn't be surprised if NT would have this feature, too."
    author: "AC"
  - subject: "Re: Someone needs to get a grip here"
    date: 2001-08-01
    body: "No, NT has a different security feature set. Instead, it rotates your configuraiton settings randomly, and if it's running DHCP, renames all the hosts once every 3 hours. Imagine the security holes if all the systems on your network were up at once! :-)"
    author: "Carbon"
  - subject: "Re: Someone needs to get a grip here"
    date: 2001-07-31
    body: "No OS is safe from hardware failures - the dot crash was one.\n\nThe harddisk crashed because it was getting too hot for too long (heat wave in southern Germany).\n\nThe fix was adding another fan to the server."
    author: "bero"
  - subject: "Re: Someone needs to get a grip here"
    date: 2001-07-31
    body: "Despite summer occurring on a regular and well forseen basis?\n\n--Db--"
    author: "DildoBaggins"
  - subject: "Re: Someone needs to get a grip here"
    date: 2001-07-31
    body: "Ja ja\nEso no se lo cree ni...."
    author: "Antonio Gutierrez"
  - subject: "Re: Someone needs to get a grip here"
    date: 2001-08-01
    body: "*hands bero a proper Server room with a proper cooling device to keep the temperature steady, then sniggers and ducks reaaaalllyyyy low :) *"
    author: "Darian Lanx"
---
Sorry guys, the dot went down again this weekend after 51 days of uptime on the new server.  We suspect that the problem is an overworked harddrive.  Bero is on it (bonus: woo, we have ext3 now), but you can expect minor flakiness as we fully work out the issues.
<!--break-->
<br><br>
This has been good enough motivation for me to look into a redundant system, perhaps by co-hosting the dot between <a href="http://www.bero.org/">bero.org</a> and <a href="http://www.kde.com/">KDE.com</a>. We have had offers of help and advice from the <a href="http://www.zope.org/">Zope guys</a> (and several of you also mailed us during our downtime -- sorry if we haven't contacted you yet), so things should look better for the future.
<br><Br>
For the short term, I've updated our backup of the dot, set up an adhoc 30-minute incremental backup system, and will be working on getting a more robust and regular backup strategy in place so that we can change hosting for the dot on the dot.  For the long term, we want some kind of failsafe hosting so that if one server goes down, we aren't fully stranded.  
<Br><Br>
Thanks for your patience and understanding.