---
title: "Writing Plugins for KDE Applications"
date:    2001-01-25
authors:
  - "numanee"
slug:    writing-plugins-kde-applications
comments:
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-25
    body: "Way to go Richard, this is very cool."
    author: "Shawn Gordon"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-25
    body: "I aim to please ;-)"
    author: "Richard Moore"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "One word: Amazing.  I just downloaded and compiled the source code.  Works like a charm.  I got a new toolbar with the ABC spellcheck icon on it.  The toolbar even magically goes away in FileManager mode. It's a wonderful feeling to run a browser that is as easily extendable as this.\n\nHow do I add the icon ABC automatically to the main toolbar?  I tried editing plugin_htmlvalidator.rc by changing mainToolBar to Main Toolbar but that didn't work.\n\nOne funny thing is that the source code is only a few lines long but the Makefile stuff is hundreds of kilobytes. :-)"
    author: "KDE User"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "> How do I add the icon ABC automatically to the\n> main toolbar? I tried editing\n> plugin_htmlvalidator.rc by changing mainToolBar\n> to Main Toolbar but that didn't work.\n\nThe easiest way to do this is to use the standard edit toolbar dialog - the new action is fully integrated with the app framework.\n\nThe size of the build system is because it uses the standard KDE scripts - I think the simplicity makes it worth the size. The same scripts can build everything from plugins to complete applications. This is perhaps a slight example of over-engineering for a 100 line program...\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "Well, I know about that, but can I make it go to the standard toolbar by default when the user installs the plugin?"
    author: "KDE User"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "Makefiles are usually (esp KDE) automatically generated and so contain a lot of extra features, targets, etc.  Theoretically, they prolly could be cut down, but it's not needed.  Now a few lines of source code and a beautiful browser with extensible features is what is wonderful as that is a great combonation that I love to see.\n\nKeep up the work KDE!"
    author: "Nicholas Hagen"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "I'm pleased to see that I had to wait for KDE 2, but not for a bad WM !\nIt's really great ! It's perhaps more easy to add parts to the \"explorer\" under KDE in C++, than under Windows, with VB ;-)\n\nI wish a long life to the KDE Project !"
    author: "Chucky / Outmax!"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "Amen!"
    author: "Soknet"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "Thats true...\ngreat work."
    author: "Gunnar Johannesmeyer"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "Gnome what is your answer yet?"
    author: "steve_qui"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "Writing Bonobo components is just as easy."
    author: "ac"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "Dispite how you guys all hate CORBA because it's \"complex\" and \"bloated\" and \"slow\", Gnome's implementation is not.\nIt's a fact, so face it."
    author: "ac"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "? ? ? o.k. ....  we face it :)"
    author: "Thomas"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "LOL!!!!!!!!!!!!!!!!!"
    author: "kde-rules"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "oh, stop this nonesense. bonobo is pretty good."
    author: "kdeguy"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-30
    body: "GNOME has bonobo, KDE has kparts.  Deal with it.  I'm sure there are advantages and disadvantages to both.  What's the point it starting a GNOME vs. KDE war every time a GNOME or KDE technology is in a news item?\nWhatever works for you, use it!\nMight as well just end this thread, because it'll never accomplish anything claiming which technology is better.\n\n-Brandon"
    author: "brandon"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "I have tried out the libhtmlvalidatorplugin form the tutorial. After installation there was no entry for the plugin in the tools menu of konqueror. After I changed the line \nkpartgui name=\"htmlvalidator\" library=\"libhtmlvalidatorplugin\" to \nkpartgui name=\"htmlvalidator\" library=\"libhtmlvalidatorplugin\" <B>version=\"1\"</B> in plugin_htmlvalidatior.rc it worked."
    author: "Helmut Zechmann"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "Strange.  For me I didn't have a problem with KDE 2.1 beta2 as it worked out of the box.  Perhaps Rich knows what's going on."
    author: "KDE User"
  - subject: "Changes to example + Babelfish plugin"
    date: 2001-01-26
    body: "After reading Rich's excellent tutorial, I was inspired to hack around for a bit.  I downloaded the tutorial and starting cleaning things up.  For instance:\n<p>\n<ul>\n<li>BUGFIX: Put the ToolBar container outside of the MenuBar container. This was why the action created it's own toolbar instead of appending to an existing one.</li>\n<li>BUGFIX: Put the toolbar action inside of \"extraToolBar\" instead of \"mainToolBar\" since that's what the extra toolbar is for.</li>\n<li>CLEANUP: Got rid of a lot of the old KDE 1.x 'configure' and 'automake' stuff and replaced with new and improved KDE 2.x stuff.</li>\n<li>OTHER: Created a 16x16 and 22x22 icon just for the validator</li>\n<li>OTHER: Changed some references of 'spellcheck' to 'htmlvalidator'</li>\n<li>OTHER: Changed '\"&amp;Validate Web Page (plugin)\"' to 'i18n(\"&amp;Validate Web Page\")' since no other plugin claims to be a plugin AND it's always good practice to use i18n.</li>\n</ol>\n<p>\nBut while I was doing that, I wondered how easy it would be to create a plugin to do page translation as suggested in the above article.  The result was the 'babelfish' plugin.  It puts a KActionMenu list of all of BabelFish's translations (English to French, etc) in the menubar and toolbar.  It's pretty slick.\n<p>\nI'm not 100% sure what to do with all this so for now, I just put it up on the web for anybody to get:<br>\n<a href=\"http://devel-home.kde.org/~granroth/plugins-0.2.tar.bz2\">http://devel-home.kde.org/~granroth/plugins-0.2.tar.bz2</a>"
    author: "Kurt Granroth"
  - subject: "Re: Changes to example + Babelfish plugin"
    date: 2001-01-26
    body: "<p>thanks a lot for your quick improved sources.  The ones provided by Richard let my konqueror crash.  Really excellent</p>\n\n<p>Just wundering would another plugin be able to interact with the <a href=http://dot.kde.org/979681957>KDE.com Offers Free Docbook Compilation Service</a>?<br><br>\n</p>"
    author: "Richard Bos"
  - subject: "Re: Changes to example + Babelfish plugin"
    date: 2001-01-27
    body: "<i>I'm not 100% sure what to do with all this so for now, I just put it up on the web for anybody to get:</i>\n<p>\nSeems to me that these might not look bad in CVS or perhaps at www.konqueror.org. Or even better, just like kde.themes.org offers themes, there should be a repository for handy plugins once more follow, no?"
    author: "Rob kaper"
  - subject: "Re: Changes to example + Babelfish plugin"
    date: 2001-01-27
    body: "I suggest to put at least the Babelfish plugin in CVS (after the freeze) for KDE 2.2"
    author: "Marcus Camen"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-26
    body: "Would it make sense to write a cd-writer plugin for konquerer?\nOr how should something like this look like?"
    author: "Michael"
  - subject: "No, it would NOT make sense"
    date: 2001-01-27
    body: "What you want is called UDF, half implemented for linux already. \n\n(packet-cd available here: http://packet-cd.sourceforge.net)\n\nYou can the use cdrom-RWs like floppies. What do you need a cd writer pluginsfor? \n\nFor music CDs you can't use file manager anyways, you need more settings, than you can put into a plugin."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: No, it would NOT make sense (but Apple...)"
    date: 2001-01-28
    body: "Um, apple has implemented it where the cdr is an icon on desktop, u open it, makes a normal window, u drag stuff on to it, then u throw icon in trash and it asks u if u want to burn it.\n\nSounds kinda stupid tho, i agree with Moritz Moeller-Herrmann :)\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: YES I think it would make sense"
    date: 2001-01-29
    body: "I want to have a filemanager, were I can do the following:\n\n* I drag the cdrom-icon onto the cd-rw icon and the   CD gets copied.\n\n* I drag a file onto the cd-rw folder and it asks     whether I want to create a music/data cd. \n\nAnd this has to be done in the filemanager:\n\n* it's about files\n\n* you have a filemanger, which replaces netscape, ghostview,... but can't even get along with /cdrw,....  that can't be! It has to satiesfy all my needs related to the filetree!"
    author: "Michael"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-27
    body: "Excellent. And it looks way better than the ugly Gnome UI."
    author: "Kirby"
  - subject: "Yeah right"
    date: 2001-01-27
    body: "So Gnome looks ugly? Ha!\nHave you seen this yet:\nhttp://www.jaded.org/screenshots/2001_01_21_041006_shot.jpg"
    author: "Anonymous"
  - subject: "Re: Yeah right"
    date: 2001-01-27
    body: "<p>OK, that's a nice screenshot. All right, very nice. But it has one serious flaw; the maximize/minimize/close buttons in inactive windows looks disabled (grey). There is no reason they should be -- you can use them, can't you?</p>\n\n<p>And Gnome's triangular back/forward buttons <em>are</em> ugly. And so are KDE's blue arrows...</p>"
    author: "Anonymous"
  - subject: "Re: Yeah right"
    date: 2001-01-29
    body: "no, there is no need to make the buttons active. with the same reason you could draw everything of the border active, but that would be quite senseless. ;)\ni like it that way."
    author: "Spark"
  - subject: "Re: Yeah right"
    date: 2001-01-27
    body: "That's very nice.  Their panel is looking more and more like Kicker. In fact the whole desktop looks very much like KDE now except the icons look more multimedia-ish if you know what I mean.\n\nAre there any themes like this for KDE?"
    author: "KDE User"
  - subject: "Re: Yeah right"
    date: 2001-01-28
    body: "That's a very nice window border theme in that shot.  It reminded me that I have not seen ANY kwm themes out there.  Have I been looking in the wrong places, or are there really no themes out there except for the ones that come with KDE?  I've seen some widget themes on kde.themes.org, but no window manager themes."
    author: "not me"
  - subject: "Re: Yeah right"
    date: 2001-01-31
    body: "Hm, I assumed you would post a screenshot making gnome look good, but you just posted something pretty ugly.  The windowmanager sucks (who wants their close box near maximise/minimise, that's asking for trouble), the gtk 'theme' is less than inspired, and the panel looks like a chintzy kicker ripoff.  I would be ashamed to have that as my desktop."
    author: "Joe KDE User"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-28
    body: "Try using a theme on the ugly GNOME UI."
    author: "robert"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-28
    body: "Thanks Rich,<br><br> due to your nice tutorial I finally implemented qwertz' suggestion to configure Java/Javascript/Cookies/Image loading in a small dropdown menu, so you don't have to open the config-dialog for those often-changed settings.\n<br><br>\nGrab the plugin <a href=\"http://devel-home.kde.org/~pfeiffer/khtmlsettingsplugin-0.1.tgz\">here</a>.\nFor the cookie settings to work, you need a current kdelibs snapshot (beta2 is not sufficient), but the other settings should work just fine with 2.0.\n<br><br>\nCheers,<br>\nCarsten"
    author: "gis"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-29
    body: "Thanks to you programmers, what a nice world you create.\n\nAn old man's wish; a plugin that brows' all the man-, info- and docu pages.\n\nBeatiful thanks\nguran"
    author: "guran"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-29
    body: "khelpcenter can do that IIRC."
    author: "caatje"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-29
    body: "please try....\n\nopen konqueror\ntyp in the url-field  man:mount\n\nit's a cool feature......"
    author: "steve"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-29
    body: "hey, that even works with galeon! :)\ngnome and kde both rock."
    author: "Spark"
  - subject: "Re: Writing Plugins for KDE Applications"
    date: 2001-01-30
    body: "Anyone knows any __SIMPLE__ example of plugin app + ex. plugin? or is willing to write one?"
    author: "anon"
  - subject: "factory name"
    date: 2005-04-15
    body: "Is it necessary for the plugin's factory method to be called 'create ()'?"
    author: "jimmy"
---
In the first of what we hope to be an exciting series, <A HREF="mailto:rich@kde.org">Richard Moore</A> has published a <a href="http://developer.kde.org/documentation/tutorials/dot/writing-plugins.html">tutorial</a> on how to write KPart plugins for KDE 2. In this tutorial, Richard gives us a glimpse into the power of KParts by demonstrating how simple it is to extend Konqueror with a plugin. In particular, he implements a plugin for HTML Validation, but the same mechanism could have just as easily been used to write a plugin for convenient Babelfish translation of the current webpage, or indeed something much more fancy. In general, once you understand what's here you should be able to write plugins for pretty much any application or component in KDE.
The <a href="http://developer.kde.org/documentation/tutorials/dot/writing-plugins.html">full tutorial</a> can be found on the <a href="http://developer.kde.org/">KDE Developer Site</a>. <EM>This story was amended and republished at 04:00 PM.</EM> <b>Update: 01/26 04:35 PM</b> by <a href="mailto:navindra@kde.org">N</a>: <a href="mailto:granroth@kde.org">Kurt Granroth</a> has made available various modifications to the htmlvalidator example and, best of all, has implemented a nifty BabelFish plugin.  See his <a href="http://dot.kde.org/980429423/980540994/">article</a> for details, download the plugins <a href="http://devel-home.kde.org/~granroth/plugins-0.2.tar.bz2">here</a>. <b>Update: 01/28 07:55 PM</b> by <a href="mailto:tibirna@kde.org">I</a>:
As yet more proof of the power and simplicity of KParts-based plugins, <a href="mailto:pfeiffer@kde.org">Carsten Pfeiffer</a> <a href="http://dot.kde.org/980429423/980689351/">has created</a> another <a href="http://devel-home.kde.org/~pfeiffer/khtmlsettingsplugin-0.1.tgz">plugin</a>, which allows easy configuration (from a light drop-down menu) of Java, JavaScript, cookies and image loading. Thanks, Rich, Kurt and Carsten.

<!--break-->
