---
title: "People of KDE: Rob Kaper"
date:    2001-12-03
authors:
  - "Inorog"
slug:    people-kde-rob-kaper
comments:
  - subject: "hah"
    date: 2001-12-03
    body: "i like this line, \"I've smoked some strange stuff but never anything of that caliber!\". good work rob."
    author: "jz"
  - subject: "Re: hah"
    date: 2001-12-03
    body: "No strange stuff necessary for setting the ashtray on fire. It just depends on how full you allow the ashtray to become."
    author: "Uwe Thiem"
  - subject: "DOH!"
    date: 2001-12-03
    body: "DOH!"
    author: "[Bad-Knees]"
  - subject: "Gym"
    date: 2001-12-03
    body: "> They once locked me up in a gym in South Germany for a couple of nights during\n> LinuxTag 2001. That's probably the meanest thing that happened so far.\nHehehe :-)) Same here! I remember very.... uhhm, better not to tell ;) And note \nthe \"locked me up\" *LOL* Nice interview, Rob!\n\nCiao,\n  Michael"
    author: "Michael Brade"
  - subject: "Re: Gym"
    date: 2001-12-03
    body: "Haha, IMHO Malte's constant snoring didn't make it any more comfortable :-) - at least the Gym crew got mentioned on /. (and those wussies who stayed at the Hotal didn't). :-}"
    author: "Frerich Raabe"
  - subject: "Re: Gym"
    date: 2001-12-03
    body: "Bah, /me thinks mine and Nikos delirium to get to our sleeping place is even worse than all your Gym tales together (speaking of.. euhmm.. wussies ;)\n\n</daniel> (ending the \"mine was worse\" thread)"
    author: "Daniel Molkentin"
---
For this week's edition of "<a href="http://www.kde.org/people/people.html">People of KDE</a>", <a href="mailto:tink@kde.org">Tink</a> interviews Rob Kaper, author of <a href="http://capsi.com/atlantik/">Atlantik</a>, all-round KDE evangelist and one of the few KDE developers marked "up for adoption" (and he kindly includes his dishes in the for-adoption kit :-). <a href="http://www.kde.org/people/rob.html">Say hi to Rob</a>.

<!--break-->
