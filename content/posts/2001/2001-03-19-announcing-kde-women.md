---
title: "Announcing KDE-Women!"
date:    2001-03-19
authors:
  - "eva"
slug:    announcing-kde-women
comments:
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "First Congratulations! ;-) \n\nHey, good stuff, guys.  Great initiative!"
    author: "Navindra Umanee"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Guys?"
    author: "reihal"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Guys does not specify males as far as I am concerned!"
    author: "Martin"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "waaaaaaaaaaaah , hauptsache emanzipation raush\u00e4ngen :)"
    author: "web"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Can you please explain that to me in english?\nAnd please - have a look at the project before writing flames ;-)"
    author: "eva"
  - subject: "explanation ;)"
    date: 2001-03-20
    body: "Freely translated:\n\"waaaaah they just show off emancipation....\"\n\nBut that doesn't make the posting more intelligent...\nPropably that's why he wrote it in German....\nAnyway I don't get what all that's got to do with emancipation.\n\nfranz\n\nLet's see what the Devils Dictionary says:\n\nEMANCIPATION, n.  A bondman's change from the tyranny of another to\nthe despotism of himself.\n\n    He was a slave:  at word he went and came;\n        His iron collar cut him to the bone.\n    Then Liberty erased his owner's name,\n        Tightened the rivets and inscribed his own.\n                                                                  G.J."
    author: "franz"
  - subject: "Re: explanation ;)"
    date: 2001-03-22
    body: "Your translation is not very accurate. A more accurate one would be \"The most important thing is that the emancipation is shown off\", which is not necessarily a negative comment and the ;) implies some tongue in cheek."
    author: "cosmo"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Sigh.  I should have known that \"guys\" would not \"internationalize\" well here.  I did not mean to start a controversy.\n"
    author: "Navindra Umanee"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "\"Folks\" is on the safe side. ;-)\n\nUwe"
    author: "Uwe"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: "they are just posting water...8-)"
    author: "gege"
  - subject: "Fine, but why the separation ?"
    date: 2001-03-19
    body: "OK, but why separate ? We do not need to be separated according to certain human attributes. What will be next ? Maybe KDE-AfroAmericans or KDE-AmericanIndians ? Really, why do we need this ? Yeah, we need everyone to contribute: women, American Indians, Chinese, Japanese, whatever (we do not care about those human attributes), but it's not necessary to be divided..."
    author: "Bill"
  - subject: "What separation?"
    date: 2001-03-19
    body: "Why is it divided?  It is only divided in that KDE is currently a predominantly male project and KDE is currently not experiencing as many contributions from the full community as possible. KDE-Women is out to rectify that."
    author: "KDE User"
  - subject: "Re: Fine, but why the separation ?"
    date: 2001-03-19
    body: "> OK, but why separate ? We do not need to be\n> separated according to certain human attributes.\n\nKDE already serves a number of divergent communities with different resources.  For example, there are a number of different-language KDE sites for people who speak different languages.  There is a site for artist, one for games, one for multimedia, etc.  If there is enough interest to address any specific niche in the \"global\" community, and someone wants to fill it, more power to them!\n\nAs to the analogy to an African or Native American KDE site, I see no problems with that either.  Just like a University can have a Women's/black/whatever club, so can KDE.  And I would note that the site is not excluding anyone."
    author: "Unifier"
  - subject: "Re: Fine, but why the separation ?"
    date: 2001-03-20
    body: "Providing for different languages is a _necessity_ for those people, I don't see why women need a special site.  Are women any less capable of understanding and using KDE???"
    author: "Chris Bordeman"
  - subject: "Re: Fine, but why the separation ?"
    date: 2001-03-22
    body: "Hi,\n\nI don't want to troll here, but I think that might be the case. As you know there are a lot more male than female geeks around, setting up such a site can only help women to get that little push in the back to get involved (or at least a good place to start). I for one don't mind having that site around at all !"
    author: "Jelmer Feenstra"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Who drew that nice female konqy?"
    author: "Matt"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Thats not Konqi, stupid.\n Thats Kate, his girlfriend!"
    author: "reihal"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "\"Katie\" actually :)\n\nBe careful with \"stupid\" next time.\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Alright, allright, I thought he was trolling."
    author: "reihal"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Seriously? He just asked an honest question. And I thought *you* were trolling ;) I guess we're getting a little paranoid here these days ;)"
    author: "Haakon Nilsen"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Yes, I guess I am. I am sorry."
    author: "reihal"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: "Assuming that Katie is Konqi's girlfriend forces the potentially liberated woman into the stereotypical subserviant gender role of slave to the so-called dominant male.  \n\nWhy should Katie not be strong enough to survive and thrive on her own?  If she is truly liberated, perhaps she should HAVE a girlfriend rather than BE a girlfriend of the so-called man.\n\nBTW- I think the whole KDE-Women is a crock.  It just pushes stereotypes more and more.  But my opinion doesn't count in this debate, because I have a peepee.  Now if this were just a way for desparate KDE developers to get dates....\n\nI would love to join KDE-total-loser-needs-a-date-and-a-shower.  Let me know when we have our own websit.  Oh wait, that exists at Slashdot.\n\nGet a clue.  Write and develop.  Don't worry about crap like this."
    author: "ed"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: "quote:\n>Assuming that Katie is Konqi's girlfriend forces the potentially liberated woman into the stereotypical subserviant gender role of slave to the so-called dominant male. \n\nWrong assumption konqi is katie's friend . And konqi is a bisexual hermaphrodyte, so katie will probably have no time for coding left because she has to continually take care of konqi complicated psychological disorders because of that."
    author: "wim bakker"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-21
    body: "what ??!!!\n\nKatie & Konqui are a modern femal+male couple.\n\nDominance dosn't enter the debate because they are both too busy loving each other to heck who is \"in charge\"."
    author: "Forge"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "I have drawn Konqi's girfriend \"Katie\". See <a href=\"http://www.imagegalaxy.de\">http://www.imagegalaxy.de</a> for a larger pic and more Linux creatures! :)\n<br><br>\nRegards,<br>\nAgnieszka\n"
    author: "Agnieszka Cazjkowska"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Hey, that's really cool!  And Tori \"myra ellen\" Amos rules."
    author: "myra"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Took a look at your site. Good work.\nI would spell it 'glass', though."
    author: "pedantic"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "The KDE Women web site look beter than the KDE site !!!!"
    author: "Gerrit Posthumus"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-21
    body: "This makes PERFECT sence and dose not surprise me.  Women  have a better grasp of beuty.  \n\nGo ahead, call it a steriotype."
    author: "Forge"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Great! \nComputing in general is way too male dominated! Congrats to all @ women.kde.org. (is this the first OSS project that specifically acknowledges women?). \n\nAfter all.. women are slightly more than half the population - unless KDE is made to be attractive to them too, we can never have world domination :)\n\nYou rock! \n\n-henrik"
    author: "henrik"
  - subject: "women projects"
    date: 2001-03-19
    body: "No, this is not the first project run by women. There is linuxchix.org, some mailing lists (like the german lynn-list) and running titles in linux magazines (as in Revista do Linux, a brazilian magazin)."
    author: "eva"
  - subject: "Re: women projects"
    date: 2001-03-20
    body: "i knew about those.. i was thinking more about women doing what you're doing - getting involved in a major project and organizing themselves. I have a feeling women have a much harder time than men to be taken \"seriously\" (if a girl is involved in the OSS scene it is often assumed her S.O. is a \"real\" coder and she's just there for his sake and/or that she only does artwork or html.) so womens advocacy groups are very important. \n\nIf women and men truly treated each other equally there wouldnt be a need for genderspecific groups, but we rarely do. Men are probably much worse (we're just now (last 50 years or so) getting into our think heads that women arnt just pretty things that cook)\n\nBtw, thanks for the tips page - i had no idea what that little X next to the location: field was =)"
    author: "henrik"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "I want to start a club for males with lactating breasts that feed their children.  Any takers?\n\nBeing serious for a second, distingushing the female coders from the male coders is stupid in my opinion.  Distingushing is nothing more than seperation and seperation leads to discrimination (seperate but equals does not juve in the courts you see).  Why embrace the differences?  Instead of gender specific clubs we need non-gender specific clubs.  Gotta go, the young one needs fed."
    author: "bill gates"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "Agree with that - this is a completely sexist and old fashioned idea. What difference does gender make?"
    author: "Anonn"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "You are right, this project should not encourage differences between male and females in the KDE community.\n\nThe problem is that the difference excisted before this project was ever started. Look at the mailing lists, women are just not treated the same as men.\n\nThe fact that women found the need to start this project is only proof of that.\n\nYou probably like it to be treated with respect, I have seen many women being asked things that would not have been asked if she was a man. Things like: \"Do you know HTML?\"\n\nMy take; this project lowers the barriers for women to enter the KDE world."
    author: "Tommy"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: ">Look at the mailing lists, women are just not treated the same as men.\n\nCould you provide a link? I have never seen any sort of sexism on the kde mailing lists myself, but I'm not a frequent participant."
    author: "Carbon"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: "I was gang-raped by the Pope and his Archbishops (or whatever).  And since I have made the claim, it is true.  After all, isn't the seriousness of the charge more important than evidence?"
    author: "Chris Bordeman"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: ">You probably like it to be treated with respect, \n>I have seen many women being asked things that \n>would not have been asked if she was a man. \n>Things like: \"Do you know HTML?\"\n\nOr maybe statistically women are in fact more likely to be artists or HTML coders than men.  This is not sexism, it is simple observance of fact.\n\nBesides, maybe the guy just looking for someone who knew HTML!"
    author: "Chris Bordeman"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "I think it's a STUPID thing to create a separate group to show the KDE work done by women.\n\nWomen spend it's whole life talking about something called \"INTEGRATION\", but now they want to be separated from men in the KDE project, what a madness!. KDE is a project belong to none, but to everybody.\n\nHey,man, could somebody give me a manual about \"How do those pretty things called women run?\"?. I'm getting a terrible headache with all this biz.\n\nBy the way I want to create a KDE-spanish-dark-haired-brown-eyed-medium-built, can I ?, can I ?... XD\n\nSorry for my bad English (Spanish rulez XD )"
    author: "BitRider"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "btw if you don't like what I've write simply say it. Don't send me any executable I won't execute it \n\nI want to thanks who send me the Hybris virus as answer to this text, but I already have this master virus piece in my colection (29A rulez, you suck) XD."
    author: "BitRider"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: "Chill .. it doesn't do any harm.  If segregation had been forced on them, then that would be something to get excited about.  But that isn't the case.  They have chosen to do something for themselves for their own benefit, and that is their right!  \n\nMany wars have been fought the world over for freedom of thought, speech and expression. For womens rights just as much as mens. It is not our place to criticise how someone else chooses to articulate this choice.  \n\nKDE Women is cool."
    author: "Macka"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: "Bullarky.\n\nIt does do harm to separate people into arbitrary groups because it breeds envy and resentment.  Why form a \"women's\" group unless to 'undo' some evil discrimination by men?\n\nAnd what do you mean it is not our 'place' to criticise?  There is also a message in the _means_ by which you convey your point, and I don't see how that should be exempt from critisism."
    author: "Chris Bordeman"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: "Where's the problem? Can minority harm anybody? - I don't think so... KDE-women does not exclude people, you better read before you judge. \n\nYou guys should have noticed, that there are more KDE Projects, like artists, multimedia, etc. So why do make a drama of a KDE-women? \n\nGreetings,\nAga."
    author: "Agnieszka Cazjkowska"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-22
    body: "Hi,\n\nSomehow the total misunderstanding of some guys posting here doesn't surprise me at all. All they're shouting is \"Why seperate women from men !! Why !! I want women around !\". Well, ok, not the last part (but that's what I reckon they would like to add :). Unless they actually have a founded opinion about matters like these I would just tend to ignore them."
    author: "Jelmer Feenstra"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-19
    body: "I've said it before. I'll say it again. The worst thing in open source is not what people involved in open source do... it is what people complain about others doing instead of getting involved doing themselves!\n\nIf women feel empowered because of this that is a good thing. I recently read the only place in the US where the ratio of women to men was lower than in Alaska is silicon valley. Take a hint. Most women are too intimidated to get past the technical jargon and paradigms. Men also generally seem more naturally inclined to visualize design in empty space. However while differences between the sexes may affect one's tendencies they do not in any way limit their potential!\n\nSo six months or a year from now when you are enjoying that seriously cool app, or the forehead slapping new feature on your old favorite you should stop... because KDE women could have worked on it.\n\nIf you look at the list of KDE developers how many are women? This is not some dumb government employment quota. This is more like a club. It is the most logical group to focus on the obviousl disparity of involvement between men and women. It is not targeted at exclusion by gender or race (they say they welcome men but I'm not doing the dishes) it is focused on inclusion and on a more equitable inclusion.\n\nIt is good to question new things... but not so good to critisize without really considering them."
    author: "Eric Laffoon"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: "/me claps hands and cheers loudly"
    author: "Carbon"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: "There aren't as many female coders compared to male coders; therefore, any attempt to introduce or allow females to increase their presents in the technical community, especially Open Source, then this is a superb move.\n\nTo me, the reason for males to reject this movement is because males don't have these focused attention upon them.  There seems to be too many feminist movements, or there are too much talk about women rights; however, this attention on women is rightly due, to some extent.\n\nI am not a feminist, but I think this initiative will yield good results for the technical community at large, if not for KDE alone."
    author: "Tom"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: "I would like to thank you for creating this site. I've been trying to get my girlfriend more involved in Linux/Internet( the future ). That has been a chore and now, she is getting interested and likes the site. Once again, thanx."
    author: "A.C."
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: "Oh great! Now those dumb Slashdot trolls will think that competive projects like Gnome is not women-friendly just because they don't have a special women website.\nI'm expecting yet another day trolled down by Slashdot."
    author: "AC"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-20
    body: "As many other people, I just don't see the reason why would like women to be separated, even if men are allowed in their group. I'm wondering why was it created... perhaps to make it easier for us, the weird guys to find a girlfriend? ;-)\n\nI can understand other kinds of separation like graphics artists, coders, etc. That's done for functionality, to group people with similar skills so they can help each other. Now, where's the purpose of separating men from women? And what's that about providing a place \"where women can find a starting point\"? Are you meaning that they because of their gender need more help than we do?\n\nYes, I know, there are few female programmers, and that's sad, but not because they're different, just because some silly people think it's weird that they would like to code. Sadly, the people around you often make decisions about how you have to live your life."
    author: "Vadim"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-21
    body: ">As many other people, I just don't see the reason\n>why would like women to be separated, even if men\n<are allowed in their group. I'm wondering why was\n>it created... perhaps to make it easier for us,\n>the weird guys to find a girlfriend? ;-)\n\nGrasshopper say:  you hit nail on head.  You may have said it jokenly, but the fact is that many young (single?) men view women as potential \"girlfriends\" or \"party-dates\".  The need to satisfy other emotions -- love or lust or whatever -- makes the nature of male-female interactions different.  I see it on the mailing lists -- pratically every time a women posts something guys start e-flirting.\n\nI'm not saying that's bad.  If kept within acceptable levels, it's necessary, as men who aren't aggressive in pursuing women will likely find themselves old and alone.  On the other hand, men often go over the line.  I think therefor that it makes sense for women to form a mailing list where they will *not* be considered someone's potential girlfriend but where they get treated with the respect and courtesy of a colleague.\n\nSomeone in a post above even claims that men have not discriminated against women.  The way I remember history until 100 years ago women generally could not vote.  In many countries today (and in virtually all the industrialized countries up to a few generations ago) women could not own property due to a fictitious \"joinder\" of the woman into the man upon marriage.  There are still places today where women are burned when their husbands die and where they are forced to wear restrictive clothing so that uncontrolled men don't have \"incitement\" for raping them.  Practical enslavement of women for prostitution is a huge industry worldwide.  In industrialized countries positions of power are still 90%+ male-dominated.  And this is all just the tip of the iceberg.  All in all, I can't think of a readily identifiable group that has been discriminated against so much and for so long in so manly places like as have women.\n\nSo if women (from all countries of the world, even those where women are not treated \"equally\") want a website to feel comfortable, more power to them, and I hope they pump out some rockin' code!"
    author: "Grasshopper"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-21
    body: "I give this 6 months, then nobody interested in this topic anymore.  and it will die like the KDE Installer project:\n\n   *High motivation in the beginning;\n   *But no real driving force, because there's not really a need for it;"
    author: "Michael"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-21
    body: "By the same reasoning, GNOME is dead.  Not.  Just because *you* don't use it or care for it, doesn't mean a project is dead or will die."
    author: "KDE User"
  - subject: "that's none-sense"
    date: 2001-03-21
    body: "Why? There is a need for a Desktop on Linux.  That's why there is KDE, Gnome,....\nFor splitting developers in male/female is no need at all.  \nSo much about my reasoning, maybe it's wrong but YOUR example doesn't show that.\n\nAnyhow, blacks fought to be treated equal.  If women want to fight for the opposite goal, fine with me:) But for an outsider KDE-women has the taste of KDE-Kindergarten.\n\nBut let's see what happens.  Anyhow explain me why you need KDE women.  And than explain it 1 month later again.  I wonder if you still see a reason for it.  It's just a hype...."
    author: "Michael"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-21
    body: "So now we have read the opinions of all three sides: women, men, and certain people posting under male names ;-).\nWhat I concluded is: there are two real arguments for that new site: women are somehow \"afraid\" of being part of the \"main\" KDE Project, or they would like to be part of a smaller group that they feel is closer to them in some way.\nTo make it clear: If this new project makes women (or anyone, for that matter) more willing to contribute, or just feel better about KDE, then OK: by any means, go on with it. We're happy with it and we warmly welcome you (no kidding). And remember to also create new projects that have importance: KDE-blind, KDE-disabled etc. Isn't it a shame that we have KDE-Women, but dont't have KDE-Disabled? What do you think?\n\nNow some specific answers:\n\nYou're telling us that KDE is male-dominant, but how do you know? Did you check the German, French, Hebrew, etc. dictionaries for the names? If yes, why? Do you need to know if we are males or not?\nWe do not need paranoia here. Nobody discriminates anyone else in KDE.\nYou're also telling that we, men, are welcome at KDE-Women. OK, thanks, but why then is it called KDE-Women ? Come on, if it says \"Dog exhibition\", you do not take your cat there. If you do, then, well, it's a pet exhibition, not a dog exhibition."
    author: "Bill"
  - subject: "about our name"
    date: 2001-03-21
    body: "You are right - there might be groups like KDE-blind, KDE-disable etc. but don't you compare wrong things? Women are about 50% of the society, and we don't need help necessarily. We simply want to do something instead of being dump users. And how many female developers are there out of the hundreds of KDE coders? (I found two who are very happy to meet other women in KDE-Women). This is not about a seperation of KDE, it is more about an addition, if this project works out ;-)\n\nSo why are we called KDE-Women? KDE-women is not _for_ women, but mainly _by_ women. That is why we cannot do KDE-disabled or KDE-blind. I am not disabled and I am not blind, so I cannot talk for them as well as I cannot talk for men. \nBy the way - why do you go to a \"dog exhibition\" if you are a human and not a dog? ;-)  In KDE-Women you can see our work and hopefully it will spread to the other projects as soon as KDE-women has grown. \n\nI can't remember the word \"discrimination\" to be used on the KDE-women pages. I don't feel discriminated! If i felt discriminated, I wouldn't use KDE... But I experienced that men and women do things in a different way, also they still can successfully do the same things. And as a woman I simply feel better, if there are people around me that do it in a similar way as I do and I don't have to talk about why and how (as we do on this page...;-) )."
    author: "eva"
  - subject: "Re: about our name"
    date: 2001-03-21
    body: "I don't want to judge if the establishment of KDE women is right or not. It is there and we have to live with it. The women obviously felt it is necessary to create KDE women site and they did it and that is their right. I work as a teaching assistent on the Faculty for Computer and Information Science and I teach programming, algorithms and data structures and computer graphics. In all these years, I have noticed that male students can learn abstract concepts of programming skills much better than female, although there is no such genetic difference between two sexes that would imply this. For example, there is probably some genetic reason, why women can't run 100 meters below 10 seconds, but I see no reason why they could not learn how to write good programs. Don't get me wrong, I know some very good female programmers, but they are a rarity. Now how this connects with KDE women? I have noticed also that there is a stereotip between male students, that is, that women can't write programs (just like, one more usual, that women can't drive a car). And this is bad. I understand that many women, who get the job as a programmer, feel a bit silly if men don't really trust them, and men are usually a majority in all software companies. For that reason, I see nothing bad in creating KDE women. This way we can all become aware of their involvement in KDE project. We know that they are good artist (that's probably why KDE icons look so nice and so professional) and we might soon find out that they are capable of playing much more important role in KDE project. With KDE women site, I believe, they can proudly say to everybody: look, this is our work, this is our part in KDE project. And I don't see nothing bad in that. BTW, isn't KDE project and Open Source in general all about freedom of choice? If you like it, use it, if you miss some features, just add them, if you want to create KDE women site, just go for it. This is just my opinion."
    author: "Bojan"
  - subject: "KDE icons"
    date: 2001-03-21
    body: "Hehe... The KDE icons are mainly made by tackat who is a man... This again shows how missleading stereotypes can be ;-)"
    author: "eva"
  - subject: "Re: KDE icons"
    date: 2001-03-21
    body: "you are absolutely right about stereotypes, I apologize."
    author: "Bojan"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-21
    body: "I'm still studying. At school I see every day boys and girls going in mostly separated groups. Girls go with girls, boys with boys and when they go together is mostly when it's with their boyfriend or girlfriend. The internet is supposed to be a more free place. Why don't we just start to mix at least here?\n\nI've been previously at that site, and now I looked once more. And I still don't see the need for it. \n\nI mean, women have nothing that men doesn't have, so why would they need a \"place to get started\"? What's on that site is mostly tips and resources, excepting only the programs made by women. \n\nSo, why don't you just call it \"KDE beginners\" or \"KDE newbies\"? You make it look like women need some special help, when they don't."
    author: "Vadim"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-22
    body: "If one woman made Kde-Women then the males have to shut up the mouth."
    author: "oda"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-22
    body: "Hmm, but this is an already starting discrimination... I just think it's strange to have a kde-women group. If we begin like that, we could end with kde-men, kde-blacks and kde-whites and even worse stuff. I like a lot the idea of women joining the KDE project, but why don't they just join the main group?\n\nBTW, your statement sounds really offensive to me. I'm not going to shup up by the simple reason that I don't think that a woman's opinion is more important than mine. I don't think that my opinion is more important than a woman's either. I think we're equal and I abosuletly won't take any decisions based on the gender of the author of anything.\n\nFollowing your statement, if I'm a man, then I have the right to start KDE-men and women have to shut up, and if I'm a terrorist, I have the right to start KDE-terrorists and all the others have to shut up..."
    author: "Vadim"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-23
    body: "Here are the premises to your argument, I think, as culled from the above:\n \n  (1) men's and women's opinions matter equally\n  (2) you are a man\n  (3) you (and some other men) believe there is no need for a women's site\n  (4) women quite popularly believe there is a need for a women's site\n \nFrom these predicates you conclude:\n \n   => Women are wrong in thinking they need a women's site\n \nSomehow, I think this argument works better for you if you replace (1) with:\n \n  (1) men's opinions are more important and more often right than women's\n \nIt is this sort of invidious (but subliminal) discrimination which lurks\nunderneath the statements of the men that make them which proves that in\nfact there is a need for a women's site.  If men were more understanding\nof women's needs, there wouldn't be a need for a site like women.kde.org.\nBut you prove only that men are not understanding of women's needs, think\nthat women's understanding of women's needs is \"wrong\" and that therefor\nwomen should get their own site where their needs will be respected.\n \nThe Grasshopper Has Spoken"
    author: "Grass Hopper"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-23
    body: "Oh, hell. As usual somebody always has to find just the opposite interpretation of my words... well, english isn't my native language anyway...\n\n(1) That's right\n(2) Yeah, although first I'm a human being\n(3) Wrong, women believe that too, my mother for example\n(4) Read what follows...\n\nI'm not saying that at all. What I'm trying to say is that if women have always tried to be equal, then I just think that they should be in the same group as everybody else. BTW, my mother thinks the same. And here's a friend's opinion:\n\n\"No.  There is no difference between male and female programmers.  I find that quite insulting in the way it is dumbed down for women :)\"\n\nIt's prefectly reasonable to have a group for coders, another for artists and another one for i18n, just because some people can code, others can draw well and others know languages. That's useful. But why women? Aren't women also artists and coders? Don't they know languages?\n\nIf they have needs, that's just fine, but I simply don't see that the site has any content at all that would satisface those needs. At least what I've seen is projects made by women and tips and tricks. It looks much more like a newbies site than a women one. Correct me if there's something wrong here.\n\nThe programs made by women is the only really women-related thing I can find there, and I couldn't think of a very good reason to have it either. Ok, there are few female programmers. So? I think that women are equally capable of writing code, so why a special section?\n\nIf I was a woman, I would find it offensive just because of it's tips section... I mean, why the tips to get started are on the women site? Don't men also need some help? Or women are less intelligent? That's what I'm thinking about, not what you're saying."
    author: "Vadim"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-24
    body: "It is a madness a man to rule on the neeeds of women.  If one  woman makes  KDE-Woman\nand if you dont like it then  turns the dial, change the station. If their  pages is against your rights then look for your lawyer but dont cry like a baby when a woman doesnt have the same opinions of your mother.  You see, I am doing to you what you are \ndoing to the women  of KDE-Woman. I am ruling on what you should do. Do you like it? I dont.\nThen we need to shut up..."
    author: "oda"
  - subject: "Re: Announcing KDE-Women!"
    date: 2001-03-24
    body: "Look, first, I'm not crying like a baby. I've explained very clearly my position. And I'm not trying to rule at all. All I want is an explanation, and I'm definiely not resorting to a lawyer because I don't have the money, think that it would be really silly and also think that this can be perfectly discussed with a human language instead of lawyer stuff. Second, the fact that a woman did it means abosuletely nothing to me, because I consider women and men completely equal, with the exception of the obvious anatomical differences, of course. In my life I've met many very intelligent and nice women and stupid men, and also many stupid women and intelligent and nice men too. So please, stop telling me that a woman made it because it means nothing to me. BTW, you have only mentioned my mother. Well, my friend wasn't male. I was curious and asked more people at school, and all think the same as I. If they didn't, probably I wouldn't continue with this.\n\nSo far, I've asked several times a question that hasn't been answered yet. Why a site that contains mainly help is called KDE-women? Wouldn't it be better to call it KDE-newbies?\n\nI am *not* against that site. What I don't like is separating men from women. \n\nI'll explain it with a practical example. When I began using Linux, I didn't know about the apropos command. Now, I would never imagine that to get useful tips for the command line I could go to a site called KDE-women. No, not because I am a man. I wouldn't think that if I was a woman either. It's because I couldn't imagine what could be on that site.\n\nFinally, I just would like to say, that I've never intended attacking it agressively. I'm just a thoughtful person who likes to discuss things. I always like to come to a point where either somebody explains me clearly why is my position wrong, admits that I am right, we both agree that our different opinions are compatible, or we decide that it's impossible to come to an agreement (this one would be the result of discussing purely philosophical things like what is life and if there is a god). I'll be very happy and will shut up immediately as soon as anybody gives me a good answer to the question from the second paragraph.\n\nSo far, the people who have replied me have either skipped what I believed was the most important part of my posts to focus on something I intended to be a joke, or told me how stupid I am for discussing this. Well, it'll make me really happy if somebody replies to me nicely and explains why am I wrong instead of shouting at me. Either here or by email. Then I'll be really glad to shut up. Really.\n\nBTW, I don't \"change the station\". If everybody did that women would still be considered inferior, to begin with."
    author: "Vadim"
  - subject: "Re: Announcing KDE-Women!"
    date: 2004-06-07
    body: "Dear all, \nWe seriously support the idea of involving women in KDE in Tajikistan.\nThis week we are going to create \"Young Ladies Tajik Linux Group\"\nthat will be concentrated in computer strings translations.\n\nI really would appreciate if you will send me some of recommendations\nto make the group succesful, and not just with translating the strings!\n\nRegards,\nMarina."
    author: "Marina"
---
The latest addition to the KDE web family is already a few days
old and in search of enthusiastic women coders, writers and designers.
The new <a href="http://women.kde.org/">KDE-Women</a> website has set
out to do this and much more.  The stated goal of the website:  <EM>
"We want to build an international KDE forum for women by providing a
place where women can present what they already contribute to KDE and
where
women, who want to contribute, find a starting point. But the content on
these pages is not only for women but for everybody. By doing this
project
we actively want to contibute to the success of KDE."</EM>  At the
website
you can already find <a
href="http://women.kde.org/projects/coding/tutorials.html/">tutorials</a>,
a big bag of KDE/Linux <a href="http://women.kde.org/tips/">tips &amp;
tricks</a>, a schedule of <a href="http://women.kde.org/events/coming.html">events</a>, and more.  If you are interested in our <a href="http://women.kde.org/projects/">project</a>, you can subscribe to
the
<a href="http://master.kde.org/mailman/listinfo/kde-women/">mailing
list</a>
or simply chat with us on channel #kde-women at irc.kde.org.
You can also look forward to meeting some of us at the upcoming
<a href="http://women.kde.org/events/coming.html">events</a> in Germany.


<!--break-->
<br><br>
<a href="http://women.kde.org/"><img src="http://women.kde.org/contact/katiewhite.jpg" width=147 height=163 ALT="[Katie]" border=0></a>

