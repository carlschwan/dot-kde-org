---
title: "KDE 3.0 Multimedia Meeting"
date:    2001-10-23
authors:
  - "Dre"
slug:    kde-30-multimedia-meeting
comments:
  - subject: "DVD Player"
    date: 2001-10-23
    body: "I'd like to see someone throw together a KDE front end to the ogle DVD player. It has a backend and a gtk front end so  a KDE interface would be cool. If you hav'nt tried it you should. Its much more simpler than xine and actually works. It does menus and encryted dvd's as well. All i had to do was make a symbolic link from /dev/dvd to /dev/cdrom it get it to work."
    author: "craig"
  - subject: "Re: DVD Player"
    date: 2001-10-23
    body: "I'll do one if I'm paid enough :)\n\nNow, the scary part of this comment is not that I posted it in general, but that I actually mean what I said."
    author: "Charles Samuels"
  - subject: "Re: DVD Player"
    date: 2001-10-23
    body: "Why is that scary?\n\nI love Free Software, but I do not have the free time to help out on any projects.  If I were to write something for someone, I would have to charge for it, or they  would have to wait a *very very* long time for it to get done.  Perhaps costing them more time/aggrevation than just paying me.\n\nFree Software is wonderful, but I don't see anything wrong with someone getting paid, nor asking/wanting to get paid for his work.  A workman is worthy of his hire.  If he wants to work for free, fine, if he doesn't/can't that's fine too.  I can't."
    author: "A. Cantrell"
  - subject: "Re: DVD Player"
    date: 2001-10-23
    body: "so what would it cost? any idea?\n\ni will give u 20$ for this? anybody else?\nmaybe there are 100 other people who will give u some bucks...\n\n-gunnar"
    author: "gunnar"
  - subject: "Re: DVD Player"
    date: 2001-10-23
    body: "The more that's offered, the more I'm encouraged :)"
    author: "Charles Samuels"
  - subject: "Re: DVD Player"
    date: 2002-09-30
    body: "XMS 600 Series Plays:\n \n * DVD's NTSC / PAL \n * All Video Cd's\n * All Audio Cd's\n  \nFeatures:\n\n * MPEG 2 LSI Video Decoder\n * Dolby Digital 5.1 ( AC3 Audio Decoder )\n * 96khz /20 - bit D/A Converter  \n * NTSC / PAL Playback \n * AV Out,Super Video Out ( Leads Included) \n * 2 Karaoke Ports \n * Reads CD/RW\n \nComes With:\n \n * Remote Controll ( Batteries Included )\n * Owners Manual \n * Warrenty Card\n\n$320.00 AUD ( Australian Dollars ) \n\nThis Console Uses An AUS AC Outlet \n"
    author: "Obi-Wan"
  - subject: "Re: DVD Player"
    date: 2004-03-02
    body: "i also have a xms and so far am unable to get the karaoke?mic to sound the picture format and songs play and have normal volume just the mic has nothing please tell me  if u know how\n"
    author: "faik"
  - subject: "Re: DVD Player"
    date: 2001-10-23
    body: "I agree that noatun needs seriously implementation for things like dvd / divx / avi files (the ones aktion can read through xanim) / good mpeg support.\nYou might say that there s a div x plug in already, but\n\nfirst you need to download it seperately (okay, not too bad)\n\nsecond it has a lot of dependancies, and it takes some time to figure out which libs are needed and which are not. (!hint: the plug in page should contain direct links to all files needed)\n\nlast point I never made it work (which may be my fault\n\navi support shouldnt be to heard to be ripped from aktion and made ready for noatun\n\nmpeg okay, the codec works quite well. Anyone cosidered *resizing* a window, or full screen?\n\ni think this should be the major focus for the 3.x release..\n\njust my 2p."
    author: "ElveOfLight"
  - subject: "Re: DVD Player"
    date: 2001-10-23
    body: "The DivX plugin has never given me trouble.\n\nAvi support in kaction uses xanim (and just reparents the window).  It's not as \"easy\" as it seems.  And xanim supports far too many formats for us to want to do this anyway.\n\n\ndouble-size a window is right click, fullscreen is left click (and a right click again to double size).  If you have Xvideo, normal resizing works, and it's very very sexy :)\n\nAnd I can't stress this enough.  Noatun is nothing but a GUI wrapper for aRts."
    author: "Charles Samuels"
  - subject: "Re: DVD Player"
    date: 2001-10-23
    body: "what do ye mean with \"if you use xvideo\"?"
    author: "ElveOfLight"
  - subject: "Mplayer?"
    date: 2001-10-23
    body: "I think that most problems of KDE sound would be solved by using the mplayer core and writing a KDE GUI for it as well as a configuration utility.\n\nThis player beats the crap out of anything I have seen and the gui is nice, even though it uses GTK (ATM).\n\nIt plays AVI (i386 codecs), Divx;-) on all platforms, ASF, WMV, MPEG, encrypted DVD, ...\nExtremely configurable. Very fast. Sound/Video output on every system (alsa, arts, oss, yvideo, SDL, framebuffer, ...)\n\nCheck it out here: http://www.MPlayerHQ.hu/homepage/\n\n\nAs a side note, anyone who mentions xanim/aktion for avi support has no clue about videos you get on the internet."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Mplayer?"
    date: 2001-10-23
    body: "why create redundancy? because Mplayer doesnt use QT, therefore its wrong... man you guys need to get over the QT for everything attitude. we have enough media playing apps in linux, most of which are gtk+ based. why not just use this?\n\nmplayer works and performs fine, why waste your time trying to QT-ISE it? \n\nthis is like re-creating a winamp-type mp3 player, whats wrong with xmms? oh yeah thats right..its not QT.. you guys are just <B>pathetic</B>."
    author: "dude13"
  - subject: "Re: Mplayer?"
    date: 2001-10-23
    body: "I think there's nothing wrong with creating a beautiful KDE \ninterface and *KDE integration* of mplayer. I must say that\nmplayer is a **very** good piece of software for the user that\nuses it. I don't know about software engineering techniques. \nMaybe you should use the mplayer kernel in the NOATUN\nenvironment. Though of course it is largely i386 based. Or am\nI wrong here..."
    author: "Hans Dijkema"
  - subject: "Re: Mplayer?"
    date: 2001-10-23
    body: "Because I don't want, or need, to install GTK when I have Qt."
    author: "reihal"
  - subject: "Re: Mplayer?"
    date: 2001-10-23
    body: "a) most distro have gtk+/glib installed by default\nb) many apps use gtk, like gnumeric, xmms, gimp, abiword, evolution, mplayer.\n\nhey even the most hard-core kde users will still use some gtk apps,...just keep on denying it..if it makes you feel better.\n\nc) whats the worse it can do? take up a few mb's?\n\nconsider this, gtk+ is owned by the \"community\"...QT is owned by a company, i can see why i prefer this. sure it has investment from a few companys, but hey its not fully controlled by any of them.\n\nTrolltech own you, they make the decisions... the GPL license of QT on the linux side of things is just a cover, considering that OS-X and windows dont have a GPL version. Probably since users of both platforms arent screaming to trolltech about no GPL/QT, thats the reason why. they own you.\n\nkonq is such a piece of broken shit btw, you guys are trying to hard to be IE.. ditch khtml, mozilla is so more usable it makes konq look like a joke. btw why does KDE act so much like windows? do you guys have any sense of innovation? I choose linux to get away from windows...not see another windows-wannabe. \n\nwhy not ditch the QT/GPL license and just place it under LGPL? GPL is shit license for libs."
    author: "dude13"
  - subject: "Go away troll .."
    date: 2001-10-23
    body: ".. you're obviously not here to add anything constructive to this conversation, just to cause trouble.  This site is about KDE, for discussion of KDE subjects and the KDE software by KDE users ; so if you want to wave your willy about gtk, take it elsewhere."
    author: "John"
  - subject: "Re: Go away troll .."
    date: 2001-10-23
    body: "he did make some valid points though.\n\nDave."
    author: "Dave Richardson"
  - subject: "Re: Go away troll .."
    date: 2001-10-23
    body: "he lost all claim to validity when he started using 4 letter expletives to throw insults at the products of other people's hard work and generously given free time; because they doen't happen to match HIS tiny world view.  I suggest you scan all of his posts here, he's just trying to start a fight."
    author: "John"
  - subject: "QT is what the whole desktop is based on"
    date: 2001-10-23
    body: "They'd want to \"waste time\" QT-ising it so that it becomes part of the desktop, i.e. so you can integrate it as a KPart, and control it with DCOP, and have it use the same theme and colors as the other apps. GTK based apps have basically no access to the infastructure that makes a KDE app part of the desktop, similar to how QT apps have little or no access to GNOME infastructure."
    author: "Carbon"
  - subject: "Re: QT is what the whole desktop is based on"
    date: 2003-05-26
    body: "Why i whould have to load GTK? i already have QT and KDE libraries loaded, \nwhy should i waste more memory with another widget set, most of you don't care \nbut i do have little memory and i want to keep it's use as little as posible\nto have more resource to other importan apps like OpenOffice or other things\ni use. Though i don't see why not use gmplayer, generally you'll use only\nmplayer to see movies instead of OpenOffice and Mplayer with xmms together."
    author: "Carlos Daniel Ruvalcaba"
  - subject: "Re: Mplayer?"
    date: 2001-10-23
    body: "please post url of mplayer. I never heard about it. But if it is that good, I dont think it should replace noatun but *simply* be forged into the code of it.. i really like the little ant noatun.."
    author: "ElveOfLight"
  - subject: "Re: Mplayer?"
    date: 2001-10-23
    body: "Well, as he wrote: http://www.MPlayerHQ.hu/homepage :)"
    author: "Per Wigren"
  - subject: "Re: Mplayer?"
    date: 2001-10-23
    body: "fork mplayer, get it some nice qt widgets, that's it!"
    author: "me"
  - subject: "Really, Really Great"
    date: 2001-10-23
    body: "I always waited for these features!\nSo now we get a fine multimedia player but most notably we get a good basic framework for software like (usable) music sequencers etc. which is the only thing I miss on linux.\n\nI hope Brahms or other software like anthem and noteedit implement those features and will be stable (!) shortly after the KDE 3.0 release. The last time I compiled it (which btw was the first time I managed to compile it with KDE 2.x) it seemed to be still missing many features and options."
    author: "musician"
  - subject: "Really, Really Great"
    date: 2001-10-23
    body: "I always waited for these features!\nSo now we get a fine multimedia player but most notably we get a good basic framework for software like (usable) music sequencers etc. which is the only thing I miss on linux.\n\nI hope Brahms or other software like anthem and noteedit implement those features and will be stable (!) shortly after the KDE 3.0 release. The last time I compiled it (which btw was the first time I managed to compile it with KDE 2.x) it seemed to be still missing many features and options."
    author: "musician"
  - subject: "Really, Really Great"
    date: 2001-10-23
    body: "I always waited for these features!\nSo now we get a fine multimedia player but most notably we get a good basic framework for software like (usable) music sequencers etc. which is the only thing I miss on linux.\n\nI hope Brahms or other software like anthem and noteedit implement those features and will be stable (!) shortly after the KDE 3.0 release. The last time I compiled it (which btw was the first time I managed to compile it with KDE 2.x) it seemed to be still missing many features and options."
    author: "musician"
  - subject: "XMMS?"
    date: 2001-10-23
    body: "Has there been any consideration of integrating XMMS more tightly with KDE?  It seems to be a much more usable player than noatun for dealing with music due to its familiar (and likely superior) interface.  Even if you guys end up creating a program similar to Media Player on win32, it would be nice to have the choice to keep XMMS as an mp3 player (this of course can still be done, it just won't be as integrated into the KDE multimedia layer as the KDE player would be).  I don't know much about the implementation of XMMS or KDE's media players, but it seems like it would be less work to integrate XMMS into KDE than to duplicate its functionality in KDE.  I just think that some people still like to have a separate mp3 player instead of an integrated one, and that XMMS fits the bill well.  \n\nThe integrated player will also be a very welcome addition to KDE, and it is probably of more importance to get good video capabilities in one program now than anything else.  I'm just wondering whether something like XMMS could still have a place in KDE along with the new media player(s)."
    author: "Slightly Underinformed Coward"
  - subject: "Re: XMMS?"
    date: 2001-10-23
    body: "no, they just want to re-create existing apps...pointless redundancy, lets wait for galeon-kde, sure another pointless app.."
    author: "dude13"
  - subject: "Re: XMMS?"
    date: 2001-10-23
    body: "No need, we got Konqueror + KMozilla already ;)"
    author: "Jonathan Brugge"
  - subject: "Re: XMMS?"
    date: 2001-10-23
    body: "galeon ownz konq/kmoz ..and its not even v1.0"
    author: "dude13"
  - subject: "Re: XMMS?"
    date: 2001-10-23
    body: ".. and your mom ownz you."
    author: "dude13's mom"
  - subject: "Re: XMMS?"
    date: 2001-10-23
    body: "Hey! Look! Wiggle's back!"
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Re: XMMS?"
    date: 2001-10-23
    body: "You also have to think of this from a developers point of view. When you are creating a KDE application it would be much easier to use the KDE media libraries rather than using pieces of XMMS / another media player."
    author: "PaulG"
  - subject: "Re: XMMS?"
    date: 2001-10-23
    body: "aRts is independent of KDE. They're trying to stay away from it as much as possible AFAIK (hence the use of MCOP and a MCOP->DCOP bridge). It runs just as well under Gnome, and in fact is supported by the Gnome Control Panel.\n\nBasically it's attempting to usurp ESD's role, while bunging in a whole lot of high level functionality as well."
    author: "Bryan Feeney"
  - subject: "Re: XMMS?"
    date: 2001-10-24
    body: "Ah okay then."
    author: "PaulG"
  - subject: "Re: XMMS?"
    date: 2001-10-23
    body: "Why not create an ability to use XMMS input plugins. There's a whole raft of them about, and they seem to be seeing a lot more active development than the KDE equivalents. There's no point re-inventing a wheel that good.\n\nPersonally, I love KDE, but I've never found noatun/kaiman to be acceptable. XMMS is what I use for almost all my video and music."
    author: "Bryan Feeney"
  - subject: "Re: XMMS?"
    date: 2001-10-23
    body: "That sounds nice, but it has complications. Integrating XMMS input plugins would make them available to Noatun, sure, but if we do it as aRts plugins and put everything as low level as possible, then you will be able to later, say, put all the audio and video formats that Noatun supports into usage inside KPresenter presentations, and previewing inside Kaboodle, and lots of other things I'm sure I haven't thought of yet."
    author: "Carbon"
  - subject: "Re: XMMS?"
    date: 2001-10-25
    body: "I looked into that once, but at least when I looked at it, XMMS input plugins had two disadvantages.\n\n1. They are usually designed to play/decode only one file at a time, which makes them unsuitable for a media framework, where you can't know whether a DJ application will be using your playing components (and thus, you will need to play two mp3s at a time for crossfading).\n\n2. They tightly integrate with the GUI for configurations. Thus, you can't do the configuration in the client application, while letting the actual decoding take place on the server (which is what apps like noatun need to do). You actually even need a Gtk+ event loop to use the GUI dialogs, which you can't provide inside artsd.\n\nXMMS is a great player, and there is no reason why XMMS fans shouldn't use it, together with the XMMS-aRts output plugin in KDE. But in my opinion, its code is largely unusable for anything but a player, whereas aRts is trying to be fairly generic."
    author: "Stefan Westerfeld"
  - subject: "Re: XMMS?"
    date: 2001-10-23
    body: "> Has there been any consideration of integrating XMMS more tightly with KDE? It seems to be a much more usable player than Noatun for dealing with music due to its familiar (and likely superior) interface.\n\nI use XMMS because Noatun has some weaks, but I would prefer using noatun. So I hope that in KDE 3.0, it would acquire the functionnalities of XMMS.\n\nFor me, now, the biggest lack is that clicking on a m3u link in Konqueror does not load the content of the m3u in the noatun playlist. Very bad integration ! (perhaps is it a problem of Konqueror more than Noatun ??... I don't think, because it is good with XMMS...)"
    author: "Alain"
  - subject: "Re: XMMS?"
    date: 2001-10-24
    body: "The only problem you pointed out with Noatun is its interface. I disagree with you:\n\n1) Anyone who has used a music player knows how to work on Noatun;\n\n2) The Noatun interface is a plug-in. So you can use XMMS skins with the XMMS interface plugin (which replaces the default interface plugin). See the noatun homepage for more details on plugins.\n\nbtw, this approach (the interface is a plugin) will be taken in future versions of XMMS too."
    author: "Evandro"
  - subject: "Re: XMMS?"
    date: 2004-07-31
    body: "try to google for beep... if I remember correctly it should use gtk2 which can be KDE-ised with the qt-gtk-theme(this is especially worth dowloading! http://www.freedesktop.com)\npretty unstable now but under development, they also want to support winamp3 skins -> whoa!!!"
    author: "miro"
  - subject: "Non Linux awareness ?"
    date: 2001-10-23
    body: "Well, on FreeBSD noatun does not work at all. xmms does. Dunno, wether this is KDE (arts) or port specific. Same for mplayer: the only Videolpayer that works here (well, vlc does fine for mpeg).\nBut the need of disabling arts to be able to listen to music is not really cool.\n\nAssuming that the port maintainers of kde or I myself are not dumb, maybe a bit more portability would not really mind. In case the latter one is true, just forget about this post.\n\nFinally - under Linux - I have trouble when having two or more KDE sessions open with diferent users. Would be nice if both were able to play music since I thought that is what arts is all about - playing multiple streams simultaneously. Even when different users try to do this."
    author: "Kem Dural"
  - subject: "All good things are four"
    date: 2001-10-23
    body: "P.S.: You also have muse (http://muse.seh.de/) in mind, don\u00b4t you (next to brahms, rosegarden and anthem)"
    author: "Kem Dural"
  - subject: "do not use win32 dlls"
    date: 2001-10-23
    body: "Most of the solutions for playing video uses win32 dlls. This way of doing things just don't work very well (to talk the truth, on my system it's asking for a total freeze - I think the guilt if from xfree 4.1...).\nAnyway, I ask you to please try to do a native solution. I know using winelib/dlls is a lot easier, etc and tal. But we need native solutions! Can't ypu reverse eng. the codecs or something like it?"
    author: "Iuri Fiedoruk"
  - subject: "do not use win32 dlls"
    date: 2001-10-23
    body: "Most of the solutions for playing video uses win32 dlls. This way of doing things just don't work very well (to talk the truth, on my system it's asking for a total freeze - I think the guilt if from xfree 4.1...).\nAnyway, I ask you to please try to do a native solution. I know using winelib/dlls is a lot easier, etc and tal. But we need native solutions! Can't ypu reverse eng. the codecs or something like it?\nOh yeah, and noatun dosen't work for me, I vote for a qt/kde xmms :)"
    author: "Iuri Fiedoruk"
  - subject: "artsd never works for more than a few minutes"
    date: 2001-10-23
    body: "I guess I'm the only person that doesnt like arts out there.  I have tried and tried with it, but no matter what I do, it always ends up crashing a few minutes after I get it going. sometimes it makes it through a few songs, but then, right as i begin to think, maybe it's working, it craps out on me again.  i use slackware linux and have tried using the packages that slackware-current has out, and compiling everything myself.  even when i tried using artsd on a different machine running red crap 7.1 it still crashed on me after a few minutes.  i think the best thing KDE could do for the entire project would be to switch to esd, which has never ONCE failed on me on any computer for any reason.\n\njust my $.02"
    author: "Elliott"
  - subject: "Re: artsd never works for more than a few minutes"
    date: 2001-10-23
    body: "Umm, if aRts is failing on your system, then it's a problem with your setup. aRts does not normally behave that way (if it did, do you think KDE would be using it?). ESD is also considerably less powerful then aRts, from the perspective of powerful multimedia apps like MIDI editors (Brahms uses aRts's own functions for traversing and altering MIDI, iirc)"
    author: "Carbon"
  - subject: "Re: artsd never works for more than a few minutes"
    date: 2001-10-25
    body: "Usually thats a problem of the kernel driver you are using. A lot of linux applications (like xmms/esd) use only a part of the kernel driver API, namely blocking I/O. Artsd uses a different part, namely non-blocking I/O. Which is the reason why some kernel drivers (such as the aureal driver, or i810) have problems running artsd, but no problems xmms/esd. So the right place to complain about it is the author of the driver.\n\nSee also the \"Compatibility with legacy/broken OSS drivers\" in the KDE3.0 planning document."
    author: "Stefan Westerfeld"
  - subject: "Re: artsd never works for more than a few minutes"
    date: 2001-10-29
    body: "You're not the only one. I hate it because it takes over my sound card. I'd like to be able to run my kde sounds through arts, but still be able to put other apps like avifile through the direct /dev/dsp."
    author: "David Findlay"
  - subject: "Re: artsd never works for more than a few minutes"
    date: 2001-10-29
    body: "You're not the only one. I hate it because it takes over my sound card. I'd like to be able to run my kde sounds through arts, but still be able to put other apps like avifile through the direct /dev/dsp."
    author: "David Findlay"
  - subject: "Re: artsd never works for more than a few minutes"
    date: 2006-03-06
    body: "I have two Slackware 10.1 systems, one with 2.6.15.4, one with 2.6.15.5.  Both use KDE 3.5.1.\n\nartsd works fine for root user on both systems.  For ordinary users, artsd crashes.\n\nWTF?"
    author: "__spc__"
  - subject: "artsd never works for more than a few minutes"
    date: 2001-10-23
    body: "I guess I'm the only person that doesnt like arts out there.  I have tried and tried with it, but no matter what I do, it always ends up crashing a few minutes after I get it going. sometimes it makes it through a few songs, but then, right as i begin to think, maybe it's working, it craps out on me again.  i use slackware linux and have tried using the packages that slackware-current has out, and compiling everything myself.  even when i tried using artsd on a different machine running red crap 7.1 it still crashed on me after a few minutes.  i think the best thing KDE could do for the entire project would be to switch to esd, which has never ONCE failed on me on any computer for any reason.\n\njust my $.02"
    author: "Elliott"
  - subject: "Re: artsd never works for more than a few minutes"
    date: 2001-10-23
    body: "I've had this problem before. I have a Sis motherboard with some kind of Trident/Sis audio chipset builtin. At first I used the standard OSS kernel sound modules and esd wouldnt break up playing mp3s but artsd would when I used noatun. It would fuzz out after 5 seconds, which was annoying so I disabled artsd.  A couple of weeks ago I switched to the Alsa sound drivers and now I dont get anymore sound breakups with artsd.  So definetly try the Alsa sound drivers if you want artsd to run with KDE..."
    author: "diamondc"
  - subject: "Re: artsd never works for more than a few minutes"
    date: 2001-10-23
    body: "Um, last I checked, GNOME 2.0 was going to use aRts as well. ESD _really_ sucks. I can't get it to run anymore. I know several other people who can't get it to run as well. It hasn't been updated in a year or two either. It is _dead_. As dead as dead can be. I suggest that you file a bug report for aRts in order to get help fixing your problem. Don't sit back and complain; do something about it. Its not that hard to file a bug report you know? Except when the package broke for a while (good old Debian unstable...), aRts has worked fine for me (of course, I use WindowMaker and only run aRts for apps like brahms)."
    author: "Clinton Ebadi (the unknown_lamer)"
  - subject: "Re: artsd never works for more than a few minutes"
    date: 2001-10-23
    body: "I had that kind of problem before. Then I turned off realtime priority in the kontrol center. It has worked flawless for many months since then. The problem is that the crappy sound driver in the kernel can't handle realtime priority.\n\n(Yes, I do know about ALSA. I will try it again when it comes with Linux.)"
    author: "Erik"
  - subject: "Midi - Karaoke"
    date: 2001-10-23
    body: "I am very glad that midi will be enhanced, thanks ! \n\nWith my old sound card Kmid didn't work, kmidi was good, but I didn't reached to replace Kmid by Kmidi in Konqueror. With my new sound card, Kmid works, but without sound (!!), and I don't still try Kmidi. And Kmid crashes Konqueror when I klick on it... I even don't see a button to stop music... Please be care with the integration in Konqueror...\n\nI also hope that Kmid will play karaoke as well as karawin, with little balls showing the speech coming (see www.karawin.com)... for KDE 3.1 !"
    author: "Alain"
  - subject: "arts and alsa"
    date: 2001-10-24
    body: "What about making arts compatible to _native_ alsa. I do not want OSS compatibility when using alsa."
    author: "Terkum"
  - subject: "Re: arts and alsa"
    date: 2001-10-25
    body: "There is _native_ alsa support in aRts (artsd -a alsa or kcontrol -> sound -> soundserver -> sound i/o -> method -> alsa). Right now, it works for alsa-0.5.0 only, but Takashi Iwai is looking into porting it to alsa-0.9.0. If you just can't wait for it, feel free to port it yourself and send me a patch."
    author: "Stefan Westerfeld"
  - subject: "Building a better Cubase"
    date: 2001-10-25
    body: "Nothing on Linux comes close to Cubase at the moment, even though I feel Cubase is flawed in its design. This is a pity as it means I still use Windows for midi.\n\nHowever, all this stuff sounds brilliant. What KDE needs is a robust, well designed framework of re-usable components for audio/video/midi which can be combined easily."
    author: "qwerty"
  - subject: "what about ESD ?"
    date: 2001-10-26
    body: "can't aRts provide an interface to esd, so that all the apps out here written to use esd can work with aRts ? (nearly all of the commercial apps written for Linux do so...)\nall-in-one media player is a good idea though, but u'd rather not use noatun, which is crappy, heavy and unstable piece of software (especially compared to xmms), and has odd features, such as the systray icon, which is not really needed...."
    author: "loopkin"
  - subject: "A New Multimedia Player"
    date: 2001-10-29
    body: "noatun is good, but on video I've noted it to be much more jumpy on video than mplayer or avifile. Could we have a better name for it, as well as a version that plays all file formats, and is fast?"
    author: "David Findlay"
  - subject: "CTI"
    date: 2001-10-31
    body: "What about computer telephony devices?"
    author: "ART"
  - subject: "H323 and video conference support"
    date: 2001-12-26
    body: "Dear all,\n\nThis are really great features ahead.\n\nDo you also plan to integrate H323 libraries to offer embedded video conference support. There is also a need for a better support of instant messaging tools (Jabber, http://sourceforge.net/projects/konverse/).\n\nToday, I use GnomeMeeting which is a mature product for video-conference. Do you plan something like in the future?\n\nBest regards,\nJean-Michel POURE"
    author: "Jean-Michel POURE"
  - subject: "Re: H323 and video conference support"
    date: 2002-07-25
    body: "hi\ni am mohamd and don't family with h323 i want create a video conference with h323 for windows please gudie me"
    author: "mohamd"
  - subject: "Re: H323 and video conference support"
    date: 2004-01-07
    body: "Check out the open source libraries for H.323 protocol called OpenH323 here:\nhttp://www.openh323.org/\nlatest versions could be found here:\nhttps://sourceforge.net/projects/openh323/\nAnd MyPhone - my Windows H.323 client, based on these libraries here:\nhttps://sourceforge.net/projects/myphone/\n\n-=MaGGuS=-"
    author: "-=MaGGuS=-"
  - subject: "Re: H323 and video conference support"
    date: 2004-02-26
    body: "Hi,\n\nI've the same problem that you had some months ago.I would like to use H323, only for voice, but I don't know how to beguin. I have the code but I don't know how to execute and use it. Can you help me, please?"
    author: "Laura"
---
<a href="http://www.kde.org/people/stefanw.html">Stefan Westerfeld</a>
has <a href="http://www.kde.com/maillists/show.php?li=kde-devel&f=1&l=20&sb=mid&o=d&v=list&mq=-1&m=549735">posted</a>
a summary of a IRC discussion held by the KDE Multimedia team
last month.  Essentially a KDE 3 roadmap for the multimedia team, the
discussion covers topics ranging from MCOP and OSS compatability to recording
and video embedding.  A slightly edited version of his post follows.
<!--break-->
<p>&nbsp;</p>
<a name="index"><h2>The KDE3.0 Multimedia Meeting, IRC discussion
summary</h2></a>
<p>
This summary provides an overview of the things that we talked about
during the KDE3.0 multimedia meeting which took place on IRC (2001-09-04).
</p>
<p>
We talked our way through more or less the topics that were posted before on
the list.  I will summarize this document to a list list of keywords for
the KDE3 planned features document (which is relevant for the freeze), but
this is the more verbose source of what will be done.
</p>
<ol type="1">
<li><a href="#mcop">Making the MCOP component model more open to the world</a></li>
<li><a href="#compatability">Compatibility with legacy/broken OSS drivers</a></li>
<li><a href="#i18n">Enabling i18n</a></li>
<li><a href="#recording">Recording</a></li>
<li><a href="#video">Video embedding / streaming</a></li>
<li><a href="#playobjects">PlayObjects (special point ;)</a></li>
<li><a href="#midi">MIDI</a></li>
<li><a href="#gsl%20engine">GSL Engine</a></li>
<li><a href="#gsl%20other">GSL other stuff</a></li>
<li><a href="#samples">Samples</a></li>
<li><a href="#environments">Environments</a></li>
<li><a href="#decisions">Decisions &amp; Open Discussion</a></li>
<li><a href="#conclusion">Conclusion</a></li>
<li><a href="#meeting">Meeting Information</a></li>
</ol>
</br>
<h4><a name="mcop">Making the MCOP component model more open to the
world</a></h4>
<ul>
<li>MCOP&lt;->DCOP bridging: the plan is to provide access to MCOP based
services from DCOP - thats useful for scripting, as well as development,
and desktop integration.</li>
<ul>
</br>
 ==&gt; responsible: Nikolas Zimmermann
</ul>
</br>
<li>JMCOP: There is a working but unreleased partial Java MCOP implementation,
so if you want to work on something like this, don't start from scratch -
however, there is currently no fixed completion date for this, nor will
it probably be distributed bundled with KDE3.0.</li>
<li>UCOM: There was a bit discussion about this one, but nobody seemed to
have looked too much into UCOM yet.</li>
</ul>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="compatibility">Compatibility with legacy/broken OSS
drivers</a></h4>
<ul>
<li>Threaded OSS support: some applications acquire better results with
respect to compatibility with various possible kinds of hardware than
aRts, because of their threaded implementation of accessing the sound
card. The threaded oss patch is a patch that opens the driver in blocking
mode and spawns a seperate thread.
</br>&nbsp;</br>
While there was discussion whether the observed increase in quality (less
dropouts, better support on more hardware) was caused
<ul>
<li>additional buffering gained through this (especially for cards with
small hardware buffer)</li>
<li>brokenness of various OSS drivers for completely non-blocking I/O</li>
<li>the usage of select(...) which might be badly implemented for some
OSS drivers or a combination of those, it generally was considered a good
idea to do it</li>
</br>&nbsp;</br>
 ==&gt; responsible: Matthias Welwarsky
[ I wrote him here because he wrote the patch, but since he wasn't
at the meeting, we might need to change this ]
</ul>
</br>
</ul>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="i18n">Enabling i18n</a></h4>
<ul>
<li>All strings emitted by aRts should be internationalizable in KDE3.0 - they
are currently not, because aRts does neither use libkdecore (so it can't
use the standard i18n), nor has own means to deal with that.
</br>&nbsp;</br>
 While workarounds might be possible (such as artsd only emitting error
codes, and seperate in-the-gui-layer-translation), the right solution,
which was also somewhat discussed on the lists will be adding own i18n
capabilities to libmcop. Whether or not libc i18n can be used, or whether
the kdecore code could be copied, is still to be evaluated. As there are
no unicode strings in aRts (no QString), utf8 is probably the way to go
for transport to the GUI.</li>
<ul>
</br>
 ==&gt; responsible: Jeff Tranter
[ Volunteered on the lists recently ]
</ul>
</br>
</ul>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="recording">Recording</a></h4>
<ul>
<li>The aRts C API didn't implement clientside recording until now, but it
should. Likewise should the Arts::SoundServer interface. It might be good
finally following the KDE2 plan to provide a KAudioStream class in
libartskde, to provide recording/playing streams in a nice, canned,
easy-to-use way. Including an end user sound recorder into kdemultimedia
might be useful, too.</li>
<ul>
</br>
 ==&gt; responsible: Matthias Kretz
</br>
</ul>
</ul>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="video">Video embedding / streaming</a></h4>
<ul>
<li>There is an VideoPlayObject interface in KDE2.2 already, which supports
playing videos via X11 embedding. This is a quite simple solution, but
using it more in KDE applications will make viewing videos much more
user friendly than it is now.</li>
<li>There was some discussion about videos and noatun. Noatun currently doesn't
use the embedding interface, and this might be hard to do, because every
skin needs to be changed for that. On the other hand, noatun is the
official KDE media player, so it should probably be able to embed videos.</li>
<li>There was some discussion about eventually implementing video editing, but
we currently lack mainly the codecs to do it. The codecs don't support
precise frame based seeking and delivery of the raw image as data. Besides
this, video support would greatly benefit from introducing more structured
asynchronous data delivery (with timestamps and frames) into aRts.</li>
<li>Extending Brahms towards embedding on the other hand seems less hard, so
that it might be reasonable simple to compose film sound tracks with Brahms
eventually, or - if we have better codecs - even edit films the same way
Brahms can edit music.</li>
</ul>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="playobjects">PlayObjects</a></h4>
<ul>
<li>Malte does have a RealMedia PlayObject ready (based on the Real library, so
the implementation is not entierly open source). Currently, there are
unsolved licensing issues.</li>
<ul>
</br>
 ==&gt; responsible: Malte Starostik
</br>&nbsp;</br>
</ul>
<li>There was some discussions on other ways to get codecs. Basically, writing
a PlayObject for one of
<ul>
<li>ffmpeg</li>
<li>libmpeg3</li>
<li>xine</li>
<li>gstreamer</li>
</ul>
could extend the fileformats aRts will be able to play. Martin Vogt is
working on xine, combined with code to standarize rendering and output
more, to make integration of new video codecs, and later, video editing,
simpler. There is experimental libmpeg3 code in the CVS.</li>
</ul>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="midi">MIDI</a></h4>
<ul>
<li>Midi sequencers (Brahms/Rosegarden/Anthem) have started to use aRts
as way of outputting midi, whereas other applications (kmid) still use
libkmid, and some things (for instance ALSA) are not supported by aRts
at this time. For KDE3.0, aRts should provide a complete solution.</li>
<li>The midi interfaces should finally support accessing sound card hardware of
all fashions (ALSA, OSS-hw-synth) - that will mean a rewrite of some stuff,
but is necessary for making the midi interfaces reasonable complete.</li>
<li>Precise synchronization between midi/audio should be adressed.</li>
<li>Libkmid could finally be moved upon the new midi interfaces.</li>
<li>The midi libs should be mainstream (i.e. kdelibs) and stable after KDE3.</li>
<li>A midi PlayObject would be helpful, to enable media players like noatun
to deal with midis.</li>
<ul>
</br>
 ==&gt; responsible (all midi issues): Antonio Larrosa, Stefan Westerfeld
</br>
</ul>
</ul>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="gsl%20engine">GSL Engine</a></h4>
<ul>
<li>GSL will be the new engine for scheduling synthesis modules. This will
not cause a glib dependancy.
<ul>
</br>
 ==&gt; responsible: Tim Janik, Stefan Westerfeld</li>
</br>&nbsp;</br>
</ul>
<li>more lengthy explanation about what/why/how --
</br>
aRts currently has an engine that decides which modules are to be
calculated when, given a certain flow graph - this is called scheduling
(and implemented in kdelibs/arts/flow/synthschedule.cc)
</br>&nbsp;</br>
The GSL engine is a complete replacement of the algorithm with the
following characteristics:
<ul>
<li>it should be quite a bit more efficient in the common case
<li>it supports dispatching calculations in multiple threads, which for
instance allows to fully exploit multi processor systems</li>
<li>it allows putting module calculations in a seperate thread from the
aRts main thread, thereby avoiding dropouts from lengthy aRts operations,
such as for instance calling open() on an NFS drive</li>
<li>it is designed in a way to support advanced scheduling features that
aRts currently does not support, that is</li>
<ul>
<li>feedback loops (circles)</li>
<li>sample precise timing</li>
</ul>
<li>it should allow achieving very very low latencies reliable (where very
very low is something like 3ms) - aRts currently can't do this</li>
<li>it is implemented in plain C, using some glibisms, however it can be
integrated into aRts with a glib-compat source in a way that it does
not bring additional dependancies (no glib required)</li>
</ul>
The GSL engine should first act as a drop-in replacement - however to take
advantage of its capabilities, it will be necessary to make sure that the
modules are implemented in a way that they can run in a seperate thread
without interfering with anything else.
</br>&nbsp;</br>
That means, that there will two types of modules: the old modules (which
will still be running in the aRts main thread), and the hard-rt-modules
(which will be able to run in the engine threaded).
</br>&nbsp;</br>
You will get the very very low latency benefit only if you use only hard-
rt-modules, but everything should work with any mix of modules.
</br>&nbsp;</br>
Where is GSL from?
</br>&nbsp;</br>
GSL is designed by Tim Janik and me, and contains a lot of know how about
music apps (Tim Janik has been working for years on Beast - best.gtk.org,
and I have been working for years on aRts).
</br>&nbsp;</br>
GSL code is in the CVS and working (although yet somewhat slow) already.</li>
</ul>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="gsl%20other">GSL other stuff</a></h4>
<ul>
<li>GSL provides a sample cache which supports partial caching and preloading,
the integration of that will get rid of the long outstanding issue that
aRts always needs to load whole WAV files (or other formats), and support
GigaSampler like "read-from-HD-and-play" style sample sets of huge size.</li>
<li>GSL provides filter design code for butterworth, tschebyscheff-1/2
and soon elliptic low/high and soon bandpass/stop filters, which can be
wrapped in new aRts modules (aka Arts::Synth_HIGHPASS).</li>
</ul>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="samples">Samples</a></h4>
<ul>
<li>While aRts is good at creating synthetic sounds and using these as
instruments in a sequencer (i.e. Brahms), playing samples will greatly
enhance the possibilities.</li>
<li>I have a prototype of a .PAT loader - you can import a whole timidity
style patchset with that, which means that we have a useful GM-sample-set
for aRts. There is also AKAI loading code in kmusic/ksynth. GSL provides
another native format. All of these probably should use the GSL sample
cache for efficient, preloaded, replaying.</li>
<li>Combined with an aRts PlayObject, media players (noatun, kaboodle) gain
not only the capability to do similar replay than kmid (output midi via
card synth or external midi), but also kmidi (output midi as calculated
audio data from sample sets).</li>
<ul>
</br>
 ==&gt; responsible (all points): Antonio Larrosa, Stefan Westerfeld</li>
</br>
</ul>
</ul>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="environments">Environments</a></h4>
<ul>
<li>The work started with mixers/environments should be completed - for instanceit would be very useful if the mixer could save/restore itself properly, and
if it could also contain insert-stereoeffects and a master volume. Also,
the mixer look more like a real-mixer(tm), for instance groups or panning
is not there right now.
<ul>
</br>
 ==&gt; responsible: Matthias Kretz</li>
</br>&nbsp;</br>
</ul>
<li>Visualizations (led-bars) would be nice, but currently have the technical
problem that they overload the MCOP layer easily, this can probably only
be fixed by changing MCOP to deal more intelligently with change
notifications.
<ul>
</br>
 ==&gt; responsible: Matthias Kretz</li>
</br>
</ul>
</ul>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="decisions">Decisions, Open Discussion</a></h4>
<ul>
<li>aRts will break binary compatibility (KDE does so, so we should, too). </li><li>the general consensus seems to be that moving aRts in an extra CVS module
(so that it is easier to obtain, compile, develop, use, package without
KDE) seems to be a good thing do to</li>
<li>whether to merge the SoundServerV2 interface into SoundServer (and so on),
is still an open issue:</li>
<ul>
<li>advantages: simpler code, cleaner interfaces</li>
<li>disadvantages: wire compatibility (running KDE2 and KDE3 apps on artsd)
will break - or is at least very hard to keep</li>
</ul>
</ul>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="conclusion">Conclusion</a></h4>
<p>
There are about three major areas which the work focuses on. First of all,
it's important to continue to improve everyday usage for casual users. Mostly,
recording, i18n and the threaded OSS support fall in that category.
</p>
<p>
On the other hand, a lot of things should be simply unified, made more
consistent, and merged. A media player which can play everything might sound
a lot better to the user than noatun, kaboodle, kmid, kmidi and aktion.
</p>
<p>
Finally, there is a lot of work to be done especially for music applications,
which includes the gsl engine, more support for samples, midi improvements,
and environments/mixers.
</p>
<p>
I hope that with the way we split up the work/responsibility on several
developers, we'll be able to achieve the goals listed here in time for
KDE3.0.
</p>
<p>
<a href="#index">Back to top</a>
</p>
</br>
<h4><a name="meeting">Meeting Information</a></h4>
<p>
The original IRC log can be found
<a href="http://space.twc.de/~stefan/kde/download/kde30-mm-log.txt">here</a>.
Meeting participants (in no particular order) were:
</p>
<ul>
<li>Carsten Niehaus</li>
<li>Stefan Schimanski</li>
<li>Andreas Burman </li>
<li>Robert Gogolok</li>
<li>Achim Bohnet</li>
<li>Michael Seiwert</li>
<li>Stefan Westerfeld</li>
<li>Matthias Kretz</li>
<li>Nikolas Zimmermann</li>
<li>Neil Stevens</li>
<li>Tim Janik</li>
<li>Jan Wuerthner</li>
<li>Daniel Haas</li>
<li>Malte Starostik</li>
<li>Martin Vogt</li>
<li>(... there was a lot of joining and leaving on the channel, so maybe
I forgot somebody ...)</li>
</ul>
<p>
Cu... Stefan</br>
-- </br>
</p>
<p>
-* Stefan Westerfeld,
<a href="mailto:stefan@space.twc.de">stefan@space.twc.de</a> (PGP!),
Hamburg/Germany</br>
KDE Developer, project infos at <a href="http://space.twc.de/~stefan/kde">http://space.twc.de/~stefan/kde</a> *-
</p>