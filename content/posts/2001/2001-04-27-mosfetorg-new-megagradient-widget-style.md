---
title: "Mosfet.Org: New MegaGradient Widget Style"
date:    2001-04-27
authors:
  - "numanee"
slug:    mosfetorg-new-megagradient-widget-style
comments:
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "Not bad - but not entirely good either. Obviously it is very difficult to make a style that is both catchy and unobtrusive at the same time. I think that the squared icons in the bottom panel don't work quite here. They are too blocky and takes to much attention in a bad way. Also: the use of colors in the screeshots make the overall appearance too dark.\n\nActually, I think that the use of a shadow behind the icons in the bottom panel that Gnome uses works very well. Its a good way of making \"more\" with \"less\"...."
    author: "john"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "Those Kicker tiles aren't part of the widget style, they are pixmaps I downloaded off ftp.kde.org. Look at KControl->Panel->Buttons.\n\nAs far as the other colors, that's what the color schemes are for... this is KDE, you can configure the color of everything - including all the gradients ;-)"
    author: "Mosfet"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "Thankyou for your kind words regarding my work on your website Mosfet - much appreciated.\n\nKeep up the great work. (I'm switching to MegaGradient right now ;)\n\nCheers,\n\nKarol (gallium)"
    author: "gallium"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "Kicker can have a shadow behind the icons in the panel as well.  You just enable icon anti-aliasing somewhere in the Control panel.  It works on all icons everywhere, too (as long as they were designed with an alpha channel - all the default KDE icons were).\n\nI agree about the button backround tiles though.  A lot of themes seem to have them, but they always look ugly (IMHO), especially when they're all different colors.  Thank goodness they can all be disabled with a single checkbox!"
    author: "not me"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "I am working on two icon themes for Konqueror, Kmail etc. toolbars.\nHere is one of them, and I would like to get some response, because I am not going to continue if people don't like them :)"
    author: "Antialias"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "oohhhww... different...\nNice, very nice\nkeep up the good work !"
    author: "Robbin Bonthond"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "ooh. yes, very cool indeed. This is the kind of creative designwork that should be applauded\nvery much.. Here are some quick comments:\n\nto \"special\" to be a \"mainstream\" theme? Not neccesarily if it is made very very clean.\n\n\n- the basic idea of using lines with shadows for icons are very stylish \n- i like the backround on the filemenu in the way it constrasts with the background on the icons\n- the quartertransparent texture of top bar is also good as well as the dots at the right end of that bar\n\n- the icons make a bit too much noise presently - too much wire. Looking at most of them it is neither easy to figure out their function. Perhaps all should look as simple as the one on the left + good spacing\n- The  background on the title text in the top bar makes also too much noise - perhaps some other way to do this?\n- the way the window control buttons at the right in the top bar stands out seems a bit misplaced somehow. Seems like they shouldn't be associated with the same level of control as the browserbuttons.\n\nI'm not sure of any of this. Just some quick reactions....\n\nit is really interesting indeed\n\n\n\n\n\n\n\n - the icons"
    author: "john"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "I really appreciate your response. Yes, you are right about toolbar icons. I know where the problem is but I am not sure yet how to designe these two icons which make noise: editpaste and edit copy. These two icons are not as clean as other icons, and I am going to change them. Printfile icons should be better also, and I am working hard :)\nBackground under titlebar text and titlebar icons can be easily changed (thanks to gallium and his kwin client :), so you can use any titlebar decoration. I just wanted to create some toolbar icons for kde, because IMHO different toolbar icons can also give a different 'look & feel' (not only titlebar decorations and widget styles). And, of course I would very much like that other people start creating some icons for kde (tackat is tired and he need some sleep - seemenow :)."
    author: "Antialias"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "Very nice ! Congrats allready ! :)\n\nDo you have a website where you show other work ?\nI'm very interested to see what your ideas are.\n\nSpark"
    author: "Jelmer Feenstra"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "yep. me too. I am also interested...."
    author: "john"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "WOW!  Very Cool and very clean!  This one could be a candidate for KDE default, I think.\n\nI _love_ the window buttons especially, but I do agree that some of those buttons are a bit perplexing, though, and the titlebar background behind the text is intrusive.  Like the other guy said, maybe the window buttons could 'raise' a little less.\n\nI love it, please continue!"
    author: "Chris Bordeman"
  - subject: "wow"
    date: 2001-04-28
    body: "This is amazing!!!  Plain coolness!!!"
    author: "KDE User"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "The icons look great."
    author: "danske"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "Hello!\n\nI have one (admittedly very small) problem with KDE. Not that it would really bug me, but I am curious about how such a bug can exist for such a long time.\n\nPlease, take a look at http://www.mosfet.org/desertred.jpg. In the lower middle of the screenshot of this screendump, you will see a KPPP-window, which has a connect-button in its lower right. This button seems to be the activated one, since it has that little border inside. This border, though, is not drawn correctly.\n\nI have the same 'problem' on my KDE, for quite a long time. It seems this bug even made it through quite a lot of re-compilations.\n\nAnybody know where this comes from?"
    author: "me"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "I don't know but this happens to me also. I always thought it was a video card driver problem with styled lines, not a Qt/KDE problem.\n\nAre you using an Nvidia card?"
    author: "anonymous coward"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "I think it's nvidia related. I used the default x11 driver for a few days after I got my geforce2 and it didn't exhibit these problems. After \"upgrading\" to the nvidia drivers, however, I have the same problem. Too bad they won't open the code for this new driver architecture of theirs :( It's either use the closed, buggy drivers they provide or lose out on 3d acceleration."
    author: "kdeFan"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "Hmm, regarding that same weird-line error, I find myself having the same problem. I am using the nv driver (which if I am not mistaken is the xfree-supplied driver). I've had this problem for ages already, and haven't got a clue how to solve it. Anyone??\n\nBtw, great work on KDE guys. I was wondering though, now that I am posting here, is there a way to get konqueror loading more quickly?"
    author: "Alex"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "> I think it's nvidia related\n\nNope, I'm having the same problem both on MGA400 and on a Neomagic chipset. Maybe related to antialiasing, where some wrong fontmetrics are returned? It happens with all widget-styles, even Qt's native ones."
    author: "foo"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "> Maybe related to antialiasing, where some wrong\n> fontmetrics are returned?\n\nhmmm... I don't think so. I have this problem since i use the nvidia drivers (before the antialiasing).\n\nI've failed in activating correctly the antialiasing on my TNT2... I've followed the qt tutorial.\n\nI've no idea on what's wrong with my settings.\nIf somebody have an idea : contact me please!"
    author: "ervin"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "This is from Control panel. \nRH 7.1 \nIntel 815\nAntialiasing off (i815 driver doesn't support)."
    author: "pz"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "I saw the same problem (outline on selection isn't complete) with i810 video.\n\nBTW - XFree86-4.0.99.3 from CVS adds RENDER support for the i810 chipset (not sure about i815).  Also - the 'outlines' are now complete with the XFree86 from CVS on i810 with my machine.\n\nI have heard users with nvidia cards mention a similar problem...\n\n- jason"
    author: "Jason Byrne"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-05-01
    body: "It's a bug in XF86.  The software dotted/dashed line generator is broken.  For drivers with accelerated dashed line drawing, it's not a problem, but for the rest of us...  You need to use a very recent CVS version to fix this, apparently."
    author: "Chad Kitching"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-05-11
    body: "Yes, it was apparently an XF86 bug; there was a discussion about it sometime ago. I used to have that problem when I used XF86 4.0.2 on a Rage 128 RF. It seemed to have gone away when I upgraded to 4.0.99.1 from CVS (using 4.0.99.3 now)."
    author: "Triskelios"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-30
    body: "This dosent happen with every style. Only ones which use the dotted style focus line \n(not kstep kmotif and all the other boring ones)\n\nEven when this line isnt broken up its still ugly\nIts Windows style of focus box, evil indeed.\n\nPerhaps it could be replaced with a solid line or an alpha transparent line with the Xrender stuff matures and starts getting implemented into kstyle, or mabe a pixmap themeable line like GTK."
    author: "Amibug"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-30
    body: "This one annoys the hell out of me, Even tho its a very small problem, it shows itself over everything.\n\ntheres a few other glitches with kstyle (or whatever they can be attributed to), aswell.\nFlickerey toolbars on mouse over, They also go quite nuts sometimes when they dont all fit on the window, try moving shrinking a window so the toolbar is cut off and indicated with an arrow, and then move the mouse over the area, buttons flicker and flash like crazy.  \n\nI also get some evilness with selected icons, the dithered selection color is very messy and unevenly distributed around the icon, Although it seems to be fixed with alot of the newer icons. (I think the problem is with alpha)\n\nAnyway, selected icons could be replaced with instead of dithering, actual coloring, not unlike when you set an active color for mouse over, see the code is already there even."
    author: "Amibug"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-30
    body: "This one annoys the hell out of me, Even tho its a very small problem, it shows itself over everything.\n\ntheres a few other glitches with kstyle (or whatever they can be attributed to), aswell.\nFlickerey toolbars on mouse over, They also go quite nuts sometimes when they dont all fit on the window, try moving shrinking a window so the toolbar is cut off and indicated with an arrow, and then move the mouse over the area, buttons flicker and flash like crazy.  \n\nI also get some evilness with selected icons, the dithered selection color is very messy and unevenly distributed around the icon, Although it seems to be fixed with alot of the newer icons. (I think the problem is with alpha)\n\nAnyway, selected icons could be replaced with instead of dithering, actual coloring, not unlike when you set an active color for mouse over, see the code is already there even."
    author: "Amibug"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "I've noticed that bug as well but i has'nt bugged enough to report it. Here's another one that bugs me. I'm an terrible speller but on kpackage if it can't connect to an ftp site it spells can,t with two n's. Like cann't."
    author: "Craig Black"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "Ok. Mosfet has done some real kool widgets. We all know that. But we also know that not many people are doing kde widget themes. If you go to themes.org, acqua is the only widget theme available. \nHowever, the gtk guys have numerous  widgets themes, which become ugly once imported into kde.\n\nIn fact, the best kde thems were made for kde1.1.2.\nNow it looks like somebody has recognised this and wants us to use windows themes(sic).\nI WANT KDE THEMES. KOOL THEMES.\nI AM NOT AN ARTIST NOR A PROGRAMMER. ALL I WANT IS COOL THEMES.\n\nNot that crap currently hosted at kde.themes.org.\n\nCheers\n\nnalogquin"
    author: "nalogquin"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "I think (and hope) will se some more themes now that the tutorial for writing them (http://www.geoid.clara.net/rik/widget_style_tutorial.html) has been made. You can also take a look at these (http://gallium.n3.net/). and this (http://apps.kde.com/na/2/info/id/1046)\n\nWhy can't I select HTML Encoding?"
    author: "J\u00f8rgen Adam Holen"
  - subject: "Gallium Rules!"
    date: 2001-04-27
    body: "Good job, Gallium!!"
    author: "KDE User"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "A hole in the HTML posting system was exploited here about a month ago (a guy had changed every link on the page to an unmentionable URL).  So they took off the option to post in HTML."
    author: "Justin"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "I'd rather spend my time improving the GTK theme support. I don't see the sense in making artists make all new themes when there are tons already available.\n\nIn my ideal world, you would have one wizard that could take widgets and WM themes from GTK, icewm (gallium already started this), sawfish (or whatever it's current name is), WindowMaker, Windows, etc... and apply it to your KDE desktop. There are already hundreds of themes available - it makes more sense to support all of those first."
    author: "Mosfet"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "I've made themes for Qt and ones for Gtk, and Gtk is much esier.  The problem is that the legacy theme importer is broken, using a single theme engine for GNOME and KDE would be realy great, but please choose the Gtk one."
    author: "ac"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "I'm slightly annoyed you didn't mention my theme... borg_dark, a widget theme.. I love it :)\n\nThere are others too\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-29
    body: "Acqua is the only well-designed widget theme for KDE 2 'cause probably I'm the only one who have fully read the Mosfet's tutorial. Once read this tutorial I've found making themes quite simple. But the tutorial is long. Many theme designers now take a well known theme and simply modify it.\nThis is the reason why many themes are very ugly."
    author: "N3M0"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-30
    body: "Frankly, this is trolling. You are saying that\n\na) You want cool themes, and you want them now! Moreover, you feel that somehow we owe you themes right now, even though KDE is an OSS project and we specifically say that there is no warranty for fitness for a particular purpose. KDE developers enjoy helping users, and making code and non-code that helps users, but this is done as a free service!\n\nb) You also do not want to admit that there are indeed lots of good windows themes out there. Windows themes are easy mainly because a windows theme is basically a collection of pixmaps, and not as involved as a kde theme. Not all that is Windows is evil.\n\nc) You do not feel like recognizing the work already put into the still unfinished theme engine, and the also still unfinished gtk importation engine. \n\nKDE themes need work, definetly, but trolling about it is not the way."
    author: "Carbon"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-30
    body: "Carbon, in almost every thread I read an anti-troll response that amounts to nothing. Telling people to stop trolling, won't stop the trolling. People are frusrated because they want KDE to be better than Gnome, in EVERY, respect, but they want someone else to solve the problem. The best way is to ignore these rants from ignorant users. \n\nYour rants, telling people not to troll, just frustrates the people that don't troll.\n\nJust ignore the post and move on."
    author: "KDEBoy"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-05-01
    body: "You're absolutely right. Almost always, the response amounts to nothing useful.\n\n*sigh*\n\nIt's just infuriating watching people act like this, since it gives KDE a bad name.\n\nHowever, don't be surprised if I continue to respond somewhat to trolls. Since their intial post is basically pointless anyways, then a response can't truly worsen the situation (Ask a stupid question....) . Besides, it's always possible that I could end up converting a troll!"
    author: "Carbon"
  - subject: "goodbye mosfet"
    date: 2001-04-27
    body: "I hope pixie will not the only thing you\nwill be still working on for KDE.\nBest wishes for your new position.\n( Do sysadmins get payed as much as programmers?)\n\nThanks for your nice work I use daily"
    author: "compile lazy"
  - subject: "Not good enough."
    date: 2001-04-27
    body: "Sorry mosfet, but i would concider the KDE2 theme engine a failure. It's been a long time since kde 2 was released and there arent even a handful 3:rd party themes, and most of those that exist are very ugly. \n\nNot even the built in styles are very good - they mainly show off the wonderfully quick gradient engine - but that does not a nice theme make.  Gnome in general has a much nicer appearence, with nice icons, beautiful themes and a huge variety of styles to choose from. \n\nThere's absolutly nothing like beautiful GTK themes that exist for KDE (although there's a huge amount of crap there too). I'd like to know why: Are KDE themes too hard to make? Is it that no one knows how to do them? Am i too impatient?  Why has no one made a brushed metal theme for kde?\n\nI think forcing theme creators to use C++ to create powerful themes was a big mistake - it's a lot more complex compared to editing a text file. \n\nWe need a powerful theme editor, damn it!\n\nBTW - someone said that the gnome hackers are artists and the kde coders engineers.. there seems to be a lot of truth to that.\n\nYes, i know i'm not entitled to complain since i havent done anything to improve the situation. Just take it as suggestions for improving KDE.\n\n-henrik"
    author: "Henrik A"
  - subject: "Re: Not good enough."
    date: 2001-04-27
    body: "Erm, then use the friggin GTK themes! ;-) You do know KDE widgets support GTK themes, and have since 2.0, don't you? Why should we recreate existing themes when you can just load the old ones?"
    author: "Mosfet"
  - subject: "Re: Not good enough."
    date: 2001-04-27
    body: "Right. Of course, i've never managed to sucessfully use the theme importer. (granted, i havent played with it that much). It always seem to screw up fonts. \n\nOh well - i'll quit whining now and try to do something about it: if you see any new cool looking themes coming out soon, they might be by me.  :)\n\n-henrik"
    author: "Henrik A"
  - subject: "Re: Not good enough."
    date: 2001-04-27
    body: "I'm working on it now. I'm not the original author of the GTK support and it hasn't really been touched since 2.0."
    author: "Mosfet"
  - subject: "Re: Not good enough."
    date: 2001-04-27
    body: "Great! And thanks for your responsiveness and in general all for the wonderful work you have done for the users of KDE..."
    author: "john"
  - subject: "Re: Not good enough."
    date: 2001-05-01
    body: "I've found GTK+ themes applied to KDE apps to be pretty memory intensive and slow, slower than the GTK+ themes applied to GTK+ apps. Also, pixmap backgrounds that are scaled in GTK+ apps are tiled in KDE apps, which looks just not quite right. In short, telling KDE users to simply just use the GTK+ themes doesn't work too well."
    author: "J. J. Ramsey"
  - subject: "Re: Not good enough."
    date: 2001-04-28
    body: "Hmm... weird.\nI wouldn't consider it a \"failure,\" especially not since I've been realizing as of late just how incredibly powerful it is; just because nobody's using it yet doesn't mean it's failed. \n\nThe theme engine itself is incredible. Bar none, it has some of the best theming support I've ever seen. It's just as easy to create a KDE Pixmap theme as it is to create a GTK pixmap theme, if not easier. I rather like the KDE theme syntax, too - reminds me of my \"olden\" days with Windows .INI files. But pixmap themes suck; they're reasonably quick under KDE2, but the real power is with coded styles; and these are, IMHO, much easier to make than GTK ENGINES. \n\nIt's hard to write styles (for noncoders). But noncoders can write pixmap themes and use the GIMP or (eventually, I guess) Krayon to do their pretty graphics. However, the coded styles are much faster than the GTK equivalent engines, and I think that C++ is worth learning enough of just so that hardcore themers can write kickass styles for KDE. That's just my opinion, though.\n\nAs an example of how good the *support* is, check out http://clee.azsites.org/kde/ and take a look at the second screenshot. With only about two days' worth of coding, I managed to get a style that looks pretty damned close to the style included with Microsoft's OfficeXP suite, and their style is quite unique. I'm not going to officially release it since I don't feel like battling off Microsoft's hordes of IP lawyers, but it's a nice proof-of-concept and we just need some interested coders to take a look and create some serious themes. The code is there; the support from the theming community isn't.\n\n-Chris"
    author: "Chris Lee"
  - subject: "Re: Not good enough."
    date: 2001-04-28
    body: "I really like that, I'm running it on my Linux desktop now."
    author: "David"
  - subject: "Please commit"
    date: 2001-04-28
    body: "Please commit this to KDE CVS (kdeaddons? kstyles?). We already have a Windows style, so why not this one?  The more the better!"
    author: "KDE User"
  - subject: "Re: Not good enough."
    date: 2001-04-29
    body: "Help! This looks really really good but how do I install it??? I have the full KDE source from a CVS checkout from last night. I can't find where to copy the relevent style directories in the code to copy the files to.\n\nThis really is a cool looking theme and it should go in to CVS, there is already a windows 2k theme anyway. and anyway, if MS say anything, just remove it, its that simple really.\n\nBrian."
    author: "Brian"
  - subject: "Re: Not good enough."
    date: 2001-04-30
    body: "Most GTK themers dont actually write themes.\n\nTheyve just taken an existing theme and changed the pixmaps, you only need to change one or few color lines in the rc file.\n\nIt seems as though there is no decent 'template' themes available on k.t.o or with kde for this.\n\nUntil Acqua, this one has potential to be reused alot. I know i'll be banging out a few themes soon based on this one.  I feel as though the kde theme situation is going to change very soon, with the developerWorks comps and check out the great widget theme tutorial on mosfet.org, it gives you everything you need to know and more."
    author: "Amibug"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "You don't really like that, do you! Woooaa! Ths is really ugly!"
    author: "Slartibartfas"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "*shrug* Then don't use it. A lot of people like highly gradiented (Metal) styles, and now we have a very fast one."
    author: "Mosfet"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "I dunno, I rather fancy it myself.  It's attractive without being gawdy or overwhelming.  There's way to many of the latter type of themes around these days.\n\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-05-10
    body: "Id..t. Response expected more from a pig than a truly thinking man. Mosfet, thanx again for your work for KDE community."
    author: "mpattonm"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "Just something i've been wondering about.. can you create a not-square k-button in the kicker? I mean, is it possible (in any way) in the current builds? (i use 2.2alpha) I know you'll all convict me now of wanting a windows-flag as k-button, but it's not about that at all.. It could be anything, I just wondered.."
    author: "xasto"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-27
    body: "Wow!!! This IS VERY, VERY, VERY good looking! Thanks!\n\nOne more thing... How about transparency on top of it?!"
    author: "norp"
  - subject: "Not really good unless you are using 24bpp"
    date: 2001-04-27
    body: "Since the gradient is not dithered, it has visible banding on 16bpp. On 24bpp it's ok, but I don't have 3d acceleration in my i810!\n\nSo, gradients or tuxracer? Life is so full of hard choices :-)"
    author: "Roberto Alsina"
  - subject: "Re: Not really good unless you are using 24bpp"
    date: 2001-04-29
    body: "You don't want to use this with dithering. Looks like crap ;-)"
    author: "Mosfet"
  - subject: "Re: Not really good unless you are using 24bpp"
    date: 2001-04-30
    body: "I think that you're forgetting the third choice : buy a GeForce MX (which I think supports 3dac in 24, but if it doesn't plz correct me).\n\nHey, tuxracer is worth it! :^)"
    author: "Carbon"
  - subject: "KoolPaint"
    date: 2001-04-27
    body: "Mosfet mentioned on his update that he has some code for a traditional paint program (like xpaint or mspaint) called KoolPaint.\n\nPersonally, i've been looking for something like this under Linux, but can't find anything like it.  I always resort to using mspaint under Wine for icons, tiles and the like (I can't stand xpaint's interface).\n\nKIconEdit isn't too bad, but it won't do curves, image flipping, rotation or similar basic features that I need.\n\nUsing somthing like Krayon or the GIMP for simile icons and small tiles is extreme overkill, IMO.\n\nAnyway, I hope he finishes it and releases it someday!\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: KoolPaint"
    date: 2001-04-30
    body: "I agree that kiconedit and xpaint aren't good enough, and that krayon and gimp are overkill. However, i think that koolpaint's niche should be more oriented toward the ms paint niche, as a fun to use painting program, with few practical applications.\n\nNo, I'm serious! I have seen people attempt to do serious image editing on ms paint, and it's ugly as heck. However, ms paint is fun for kids doing small images, and it helps total computer newbs to learn the mouse in a fun way, and to do quick, personalized background images, etc.\n\nSo, graphic editing (imho) should be:\nKivio or Killustrator for diagrams\nKrayon for web page images, logos, photo manip, etc\nKiconedit for icons, tiles, and pixel-ta-pixel image editing\nKoolPaint for wasting time :-)\n\nThe problem with kiconedit is that it is unmaintained.\nIt still doesn't even have an undo cache! Any volunteers? Certainly, improving kiconedit would also make the jobs of kde artists easier too."
    author: "Carbon"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "Great Mosfet :) Thanks to your work & gallium's kwin clients full themeability of kde is now possible."
    author: "Antialias"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "One small quibble: GTK+ didn't *have* to use a gradient pixmap scaled to the right window size, that was just the easiest way for lazy people to do it.\n\nThe harder/more time consuming but quicker in execution speed is to make a theme engine rather than use raster's slow ass pixmap theme engine.\n\nIs this doing anything that a custom GTK+ theme engine can do?"
    author: "Me"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "One small quibble: GTK+ didn't *have* to use a gradient pixmap scaled to the right window size, that was just the easiest way for lazy people to do it.\n\nThe harder/more time consuming but quicker in execution speed is to make a theme engine rather than use raster's slow ass pixmap theme engine.\n\nIs this doing anything that a custom GTK+ theme engine can do?"
    author: "Me"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-28
    body: "One small quibble: GTK+ didn't *have* to use a gradient pixmap scaled to the right window size, that was just the easiest way for lazy people to do it.\n\nThe harder/more time consuming but quicker in execution speed is to make a theme engine rather than use raster's slow ass pixmap theme engine.\n\nIs this doing anything that a custom GTK+ theme engine can do?"
    author: "Me"
  - subject: "GTK+ theme engines"
    date: 2001-04-28
    body: "He is right, GTK+ themes can be amazingly fast by writing a theme engine!\nSo why don't you people just accept it and respond instead of blindly believing that FUD?!?"
    author: "TrollKiller"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-28
    body: "> He is right, GTK+ themes can be amazingly fast by writing a theme engine!\n\nYeah, but KDE \"themes\" are even quicker when they're written as Styles, so your argument is still lost.\n\nHere are the facts, to get rid of the FUD you trolls are always screaming about:\n1) GTK+ pixmap themes are SLOW.\n2) Qt can do pixmap themes, even GTK+ ones, faster.\n3) GTK+ Engines are faster than GTK+ pixmap themes. (Then again, the only thing I can think of slower than GTK+ pixmap themes are Mozilla themes.)\n4) Qt Styles still kick the ass off GTK+ Engines when it comes to speed. Qt renders things with a quickness.\n\nThink I don't know what I'm talking about? I've got code to back up what I'm saying. It's not the best code, nor is it well-optimized, but I do have it. \n\nhttp://clee.azsites.org/kde\n\nSo why don't you people just accept it instead of blindly believing that FUD?!?"
    author: "Chris Lee"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-28
    body: "Well said ;-)"
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "pixmap themes are only slow cos of Raster's pile of shit engine. It redraws the images 3 frigging times. I could draw the themes faster in paintbox faster than the GTK pixmap engine can. Thats not a failing of the engine idea, it's a failing of the pixmap engine. So saying QT can do gtk pixmap themes faster than gtk isn't that big a deal. Everyone has accepted it's slow.\n\nAnd whats the difference between a style and an engine? \n\nAll I asked was, is this doing anything that a correctly written (IE not by Raster, and doesn't draw everything 3 times) engine can't do?"
    author: "me"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "i'd be interested to know where i can find the pixmap engine written by you, since the problem with the current one is all Raster's fault and you said you can easily do better."
    author: "Evandro"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "He said he can paint them faster in an image manipulation program, than they are drawn by the engine. Not that he can do a better engine. \nRead more carefully..."
    author: "Spark"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "and how does your code back up what you're saying? It's a KDE theme, and so has no relation to GTK themes. If you had a KDE theme and a GTK theme that we could run and say \"Yes, the KDE theme is faster\" then that would back up what you're saying, but giving us a KDE theme doesn't back it up at all."
    author: "me"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "Because GTK themes work faster under KDE as well. Remember, KDE supports both native KDE themes and GTK widget themes. The *same* GTK theme operates visibly faster under KDE."
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "Now you are running in circles...\n\nChris said, that Qt Styles are faster than Gtk Engines and that he can back this statement up, with his own Qt Style. Me was asking for a comparing Gtk Engine to show, that the Qt Style is indeed faster than the Gtk Engine.\nAnd you tell us it is, because Gtk PIXMAP THEMES are slower than Qt PIXMAP THEMES.\nWe are talking of engines now...\nWhat can Qt Styles do what Gtk Engines can't do? Where can I find an objective proof that Qt Styles are faster? I use Gtk Engines all the time, they are everything but slow. I don't know if they are faster than Qt Styles, probaly not. But it'a FACT, that I can't feel the difference (I couldn't even with my old P133). And if I can't feel or see the difference by any means, what value is a slightly faster engine? The only difference I can SEE is, that Gtk Engines look much better than any Qt Style I know (they do basically all copy existing widget styles of other operating systems, or are simple gradient ones). \nI don't want to bitch, I just hate this FUD spread against Gtk Engines. They are by no means slow or weak. And I would really appreciate some nice Qt Styles, so I can use KDE applications again without getting headaches.\nSorry, but it's the truth, I used KDE for quite a long time and know I can't see it anymore. I try it from time to time, but it's just plain ugly.\n There are even several image errors with the default style.. I hope this will change soon, but I was already hoping that several months ago."
    author: "Spark"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "What errors? Ever submit a bug report? Either way, this entire thread of GTK users bitching started out because I stated that the way GTK pixmap themes handle gradients is very, very slow on my webpage. That's true. The entire thing is slow and wrongheaded.\n\nNever used a GTK engine. Don't really use GTK apps except the GIMP. I *have* seen the API, and I know Qt styles are easier to develop."
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "> Either way, this entire thread of GTK users bitching started out because I stated that the way GTK pixmap themes handle gradients is very, very slow on my webpage. \n\nNo, you didn't mention _pixmap_ themes. Quote: \"[...] GTK themes at least used to actually use a gradient pixmap [..]\"\n\n> Never used a GTK engine. Don't really use GTK apps except the GIMP. \n\nSo proably you shouldn't make negative comments about something, you don't know. :(\nI don't use _any_ Qt application anymore, but that doesn't mean, that I'm not interested in Qt technologie. \n\n> I *have* seen the API, and I know Qt styles are easier to develop.\n\nMaybe, I don't know. I didn't look at any of those API's yet. All I know is, that there are several great Gtk engines, while Qt is still lacking such. Maybe they will come, but it's about time... Qt 2 isn't really \"new\" anymore."
    author: "Spark"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-30
    body: "Dude, get a life. O Said theme, not engine, and specifically mentioned pixaps. And you tell me not to talk about things I don't know??? Who are you to tell me shit when you never even looked at code while I have?\n\nThis is why I get annoyed at people like you..."
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-30
    body: "Eek, bad spelling. My brain is fried from actually doing some *coding* for themes instead of whining ;-)"
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-30
    body: "So *this* is why you're taking a lower profile in kde development, huh? I don't blame you one bit. Even if I did have the motivation and skillz to hack on kde, I would use an alias and a disposable email address. Sad state of affairs. Thanks mosfet, you've done some great work and I enjoy it every day."
    author: "kdeFan"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-30
    body: "> Dude, get a life. O Said theme, not engine, and specifically mentioned pixaps. And you tell me not to talk about things I don't know??? Who are you to tell me shit when you never even looked at code while I have?\n\nWell, thanks for the nice words... I'm not the one who said, Qt styles would be weak. They are surely great, but so are Gtk themes. And you all should stop this Gtk bitching, I'm getting sick of it.\nGtk is in now way bad, ugly, or slow. If you say something against the pixmap engine, than make this clear please. Just saying \"Gtk themes\" is the fastest way to spread FUD (as you can see on some comments by clueless KDE fanatics).\n\n> This is why I get annoyed at people like you...\n\nThanks again... now guess why I got sick of some KDE community."
    author: "Spark"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-30
    body: "Actually, I don't see any cluelessness by the KDE users. I for one haven't said anything that's not true, but the truth sometimes makes people's panties bunch up. Now your just sitting here being an arse trying to say I'm spreading FUD because I said something true about GTK themes. Deal with it. Saying what I say is true but I should also say you can make it better by rewriting the whole damn thing is absurd.\n\nThe only one I see trolling here is you. I'm still interested to see your \"drawing errors\", since AFAIK there are none. If you can't even say here specifically what they are I am forced to believe your full of crap and won't respond to you further."
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-30
    body: "Oh, and still waiting for your bug report. Or were you just complaining?"
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "Should also say that the comment I responded to was talking about themes, not engines, so I wasn't running aorund in circles either. If you can take the same *theme*, and run it in both environments, the results tell you something."
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "Sure, \"theme\" probaly wasn't the right word. But I'm sure he meant theme engines, because Trollkiller was talking about theme engines and Chriss Lee was talking about engines/styles, too.\nMaybe just a misunderstanding... \nI just wish, KDE folks would accept that Gtk themes doesn't have to be slow and there seams to be nothing, that Qt styles can do, what Gtk engines can't.\nAnd the API can't be SO complicated, because they are several Gtk engines out there, the best of them popped up after the KDE 2 release, so time isn't an argument either. \nQt styles may be nice, but so are Gtk engines! And of course you can also theme Gtk engines with pixmaps, they are just not as flexible as this terrible pixmap engine."
    author: "Spark"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "Sure, but it would also be nice that whenever people say \"Damn GTK pixmap themes are slow\", people don't also say \"Well, write your own theme engine in C then\"! That's pretty stupid, since it's a problem everyone recognizes."
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "And I'm still waiting for your bug report ;-)"
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-30
    body: "As a matter of fact, almost all 3rd party GTK engines except one seem to simply use pixmaps. This should be done using themes, not require the artist to write C code, but they have to because the theme engine *sucks*. And your trying to make this seem like a feature?"
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-30
    body: "Erm, no.\nThose aren't engines.\nThey're ... themes which use the pixmap engine.\n\nFor someone who's supposed to understand all this, you're (NB the spelling) not doing a very good job."
    author: "me"
  - subject: "Re: GTK+ theme engines"
    date: 2001-05-01
    body: "No, I specifically mean the engines. Look at the source code for them. Most of them are just pixmaps packaged as an separate engine with some C glue in order to avoid the pixmap theme engine. \n\nLook at the sources for them yourself... For somene who's supposed to understand all this, you're (NB the spelling) not doing a very good job."
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-28
    body: "You do understand that your saying GTK pixmap themes can be made blazingly fast by not using the GTK pixmap theme engine at all (because it's *slow*), and writing your own code instead in C? ;-)"
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "yes. And this is different to what you're doing.....how? (Execpt your stuff is C++)\n\nThe correct solution is for someone to rewrite the pile of shit that raster wrote so that it doesn't redraw things 3 times. Owen Taylor wrote one for GDKPixbuf that was quite a bit faster, but then he moved it to GTK2 so most people haven't had the fun of using it."
    author: "me"
  - subject: "Re: GTK+ theme engines"
    date: 2001-04-29
    body: "All this stuff goes into the KDE theme engine."
    author: "Mosfet"
  - subject: "Re: GTK+ theme engines"
    date: 2001-07-19
    body: "bleh."
    author: "john"
  - subject: "Also adding translucent menus"
    date: 2001-04-28
    body: "Check this - I worked on it last night ;-)\n\nhttp://www.mosfet.org/transmenu.html"
    author: "Mosfet"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-28
    body: "Wow! Very cool! I like that. Transparency would probably be useful in aqua theme too... Dosen't the real thing (OS X) have semi-transparent menus? Anyway, cool hack!"
    author: "kdeFan"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-28
    body: "mosfet:\nI know I should look at the code, but how does this work?  I thought X didn't support translucencies?  and it can't be a simple background image replacement because I see a window behind the menu...\n\nCome on, throw us a bone, this is awesome! \nI've always wanted a _trully_ transparent xterm.\ncould this be done?\n\nHorst"
    author: "horst"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-28
    body: "For popup windows like menus you can simulate it by grabbing the area the window will cover (QPixmap::grabWindow()) then blending the popup info the pixmap. This is still not true transparency, but it looks cool. :-)\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-29
    body: "Exactly. It works well for menus, since you know exactly what's going on on the desktop before it pops up and it isn't there for long (so you don't really need to update it).\n\nFor other windows (like terminals), you need to update it whenever something is going on in the area you are covering. This is what makes things difficult. Keith Packard is working on a XRender server for doing translucency between apps, and I'll support that."
    author: "Mosfet"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-29
    body: "\"and I'll support that\"\n\nAh I got you there, you already have some code sitting there at your desktop don't you ? :)\n\n(is the API for the XRender stuff anywhere near done ?)"
    author: "Jelmer Feenstra"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-29
    body: "XRender is working, the alpha channel server isn't. I did some hacks with KDesktop and a clock that does a translucent clock just to make sure it's possible, and Keith did a twm (!) hack, but we need the server."
    author: "Mosfet"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-29
    body: "L33t :)\n\nI'll be waiting !"
    author: "Jelmer Feenstra"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-28
    body: "great job!"
    author: "Evandro"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-28
    body: "Yeeeaaah, Mosfet rocks ;-).\nHe, he, ho, ho, antialias is dancing..."
    author: "Antialias"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-28
    body: "cool. In fact at this very moment I understood that transparancy can have real practical benefit - it makes sense to have pop-up/drop-down menu transparant because it is a real practical benefit to not loose the information on the screen while you are choosing from a menu.\n\nHowever it must be used as a form-element that is harmonical and matches well with the form elements and colors/textures of the rest of the theme to avoid that it looks spurious - \"glued on\" or whatever. It may not match well with many themes out there."
    author: "john"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-28
    body: "HEY!\nHow do you have Konsole working with AA fonts?!?\nI have to run it without them because otherwise it has lots of problems.\n\nAnyway, cool looking menus!"
    author: "not me"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-29
    body: "I'm just using the custom fixed font, and it works fine."
    author: "Mosfet"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-29
    body: "\"custom fixed font?\"\nIn my konsole I have \"Linux,\" \"Unicode,\" and \"Custom\" font options.  Custom brings up a dialog that allows me to pick any of the many fonts I have installed - however, none of them work correctly, not even the fixed-width ones (though they come closest to working).  Courier 10-pitch works OK for some stuff, but it still doesn't work with full-screen console apps such as the Midnight Commander or Debian dselect (looks really horrible - you can hardly tell what's being displayed).  Maybe its fixed in CVS?  I hope so."
    author: "not me"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-29
    body: "i have mapped monotype.ttf to fixed\nand run with unicode and it works for me."
    author: "AC"
  - subject: "Re: Also adding translucent menus"
    date: 2001-06-13
    body: "hummm ok.. i'm very lazy...:-) but can anyone tell me where i find some good mono-spaced TrueType fonts? i just dont use AA fonts because i so much dependent of Konsole and my ttf is so horrible....\n\nwhere do you picked this monotype.ttf?? from windows? i dont have access to any windows-machine....:-("
    author: "Jedi"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-29
    body: "WOW o WOW o WOW! Transparent menus!! Why hasn't this been thought of before!? Mosfet I think this is even cooler than the gradiant mega-widgets..did you get this idea from that guy who wanted transparent mega gradiants?  Really cool man, I just poped my ugly grey kde menu :(  I want my transparent menus!! :)))\n\nLooking forward to the next kde beta :)"
    author: "anonymous coward"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-29
    body: "Yeah, no-one ever thought of things like this before. How long before OSX and EFM and things start copying kewl KDE ideas?"
    author: "me"
  - subject: "Re: Also adding translucent menus"
    date: 2001-04-29
    body: "I don't know how many of you have used Enlightenment 0.16.X Series but it can do Translucent windows while moving them around. It was real eye candy\nAnd there was even option to turn it off all together\nThis menu stuff is really looking good\nAlso I presume there will be option to turn this off\n\nGreat stuff\nLooks like KDE look concerns are finally going away"
    author: "Yogeshwar"
  - subject: "Thanks, Mosfet..."
    date: 2001-04-29
    body: "I just want to say a quick \"thank you\" for all the work that Mosfet's done.  I think the KDE2 theming engine is quite nice..it always feels faster than the GTK+ engines to me.\n\nFrom his comments on mosfet.org I can fully understand where he's coming from with respect to the rapidly decaying Linux \"community\".  I too have become quite jaded about what our community has become...it's tiresome and really wears on you (especially if people are slagging your hard work that you're doing FOR FREE for no good reason). \n\nGood luck in the future!"
    author: "Radagast T. Brown"
  - subject: "Re: Thanks, Mosfet..."
    date: 2001-04-30
    body: "I disagree. Trolls aren't part of the OSS community at all, rather, they are a lower form of life, all of whom need to be locked in individual cubicles and forced to do web development using an on-screen keyboard and an iMac mouse!"
    author: "Carbon"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-30
    body: "I too have been playing with the KDE theme stuff. For those who remember RISC OS (or still use it), there is now a widget theme to go with the KWin theme.\n\nObligatory screen shot at http://rjw57.robinson.cam.ac.uk/desktop.jpg (should be the attachment too).\n\nNotice that there is also a KSplash skin too. I'm hesitant to put this on kde.t.o due to the blatent copyright stuff..."
    author: "Rich"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-30
    body: "You should make this available. It's quite nice and we already have another RiscOS window manager style, platinum styles, window styles, etc... \n\nAre the people that made RISCOS even in business anymore?\n\nAnyways, it's cool so I would put it up. Just avoid the KSplash thing (that's a little too much ;-)\n\nThe general rule is copying logos is a no-no, everything else is okay."
    author: "Mosfet"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-30
    body: "Acorn aren't in business anymore but RISC OS lives on through the aptly named RISC OS Ltd (http://www.riscos.com). \n\nAfter my finals (*glerk* tomorrow!) I'll package the theme up and send it to kde.t.o"
    author: "Rich"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-30
    body: "That's really nice! I did notice one very small detail that you might want to change if you release it: Look at the spelling of \"initiali[z|s]ing\" on the ksplash window. Above the icons it's spelled with an 's' and below, on the black bar, it's spelled with a 'z'. Is it spelled with an 's' in the UK, by chance? If it can't be localized, then maybe should pick just one of the spellings, for the sake of consistency. Or you could just ignore my obsessive compulsive tendencies (ex-military, I'm permanently brain-washed ;) Otherwise, that's a *very* nice theme."
    author: "kdeFan"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-04-30
    body: "AFAIK there is no localisation for English English (or at least I couldn't find one). \n\nThe KSplash screen is only for my own benefit really so I don't mind too much."
    author: "Rich"
  - subject: "Ohhh, gooood old times...."
    date: 2001-04-30
    body: "Nice! Very nice ;-))\n\nAnd? Have do you have a secret \"RiscOS Style Drag 'n Drop\" option for me? ;-)) Maybe we should send your screenshot to Icon Technology, if they want to port TechWriter* to KDE?\n\n*TechWriter is one of the most intuitive and productive word-processors I ever used.\nHey KWord-Team look at:\nhttp://www.iconsupport.demon.co.uk/Products/TechWriter/TechWriter pro.html\n\nLook at their screenshot - it's a little bit like KDE, isn't it? But RiscOS is 12 years old!!!\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Microsoft strikes again"
    date: 2001-04-30
    body: "Hello Guys.This is a serious matter completely seperate from Mosfet.\nI am a computer science student.In our lab Nt 4 Service Pack 6 is installed . I have noticed a strange thing that as soon as i load intel.com\nor apple.com/macosx/*.html on my IE 5.0\na strange error occurs and Dr Watson stops me and generates an exception error closing all windows of explorer.I have tried this thing on 5 computers of my lab and i have got the same result.\nAnother example of Microsoft's ..............."
    author: "Androthi"
  - subject: "Re: Microsoft strikes again"
    date: 2001-04-30
    body: "Don't do drugs ! Drugs are baaaad"
    author: "manfred"
  - subject: "Why don't you KDE guys go bitch about Windows?"
    date: 2001-05-01
    body: "C'mon people. Saying that GTK+ themes are slow won't get you anywhere.\nSure, GTK+ renders themes slowER than QT does, but not slow.\nI'm using a Pentium 166 MMX and I don't notice any slowdown.\n\nAnyway, that's not the point.\nKDE is \"technically superior to Gnome\".\nOK, but why get Gnome involved?\nWhy not get Windows involved?\nSo instead of yelling \"KDE is superior to Gnome\", get it all out and yell \"KDE is superior to Microsoft Windows!\" and \"DCOP is better than COM\" or \"KParts is better than ActiveX\" or \"Konqueror is faster than M$IE!\"\n\nMicrosoft is our enemy, not Gnome.\nThink about it: what use is it to trying to convince Gnome users?\nThere are way more Windows users out there.\nSo why not change any comments about Gnome to comments about Windows?"
    author: "ac"
  - subject: "Re: Why don't you KDE guys go bitch about Windows?"
    date: 2001-05-01
    body: "All I said was the way GTK theme engines handle gradients is slow, explained exactly why, and explained why this code (which was a test for a new KDE engine) is better. Then GTK/Gnome people got all upset. *shrug*"
    author: "Mosfet"
  - subject: "Re: Why don't you KDE guys go bitch about Windows?"
    date: 2001-05-01
    body: "So? Is that a reason to comment about Gnome instead of Windows?"
    author: "ac"
  - subject: "Re: Why don't you KDE guys go bitch about Windows?"
    date: 2001-05-02
    body: "Yeah, because a) I haven't really used Windows for several years (I was using Xenix and SCO before Linux and BSD), b) I don't have the source code for windows so can't comment on it's code, and c) I'll comment on whatever I want, as long as the comment is justified technically. It says a lot about certain segments in the Linux community when you can't make a totally justified technical statement (and offer your solution with code) without getting flamed.\n\nI don't have any recent experience with Windows, so can't say anything about it. It may kick butt, who knows? It's not relevant to me because I can't hack it, so I can't comment on it. Microsoft is not an enemy to me, it's a non-issue in my life."
    author: "Mosfet"
  - subject: "Re: Mosfet.Org: New MegaGradient Widget Style"
    date: 2001-05-02
    body: "Yet another screenshot of a unfinished theme.. \n\nHere is it.. somewhat brushed-metal inspired (i stole the pixmaps) and very unfinished.  The menubar is about the only widget that looks right so far. :) \n\nAnd yes - it started out as the tutorial webstyle - and quite a few widgets still look the same. :)\n\nPS. Sorry Mosfet for whining earlier. Creating themes is a lot more work than i thought it'd be - and i'm just using your code. Appologies. \n\n-henrik"
    author: "Henrik A"
---
<a href="http://www.kde.org/gallery/index.html#mosfet">Mosfet</a> took the pre- and circa- KDE2 world by storm not so long ago.  His fingerprints are all over many of the styles, themes, window decorations as well as much of the KDE2 we are familiar with today.  He churned out thousands of lines of code accompanied by slick screenshot after slick screenshot. Mosfet is now back with a funky new widget-style dubbed <a href="http://www.mosfet.org/megagradient.html">MegaGradient</a> (screenshots: <a href="http://www.mosfet.org/mycolors.jpg">1</a>, <a href="http://www.mosfet.org/desertred.jpg">2</a>, <a href="http://www.mosfet.org/solarisCDE.jpg">3</a>), currently in <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kstyles/megagradient/">KDE CVS</a>. As an aside, those of you wondering where Mosfet has been lately can check out <a href="http://www.mosfet.org/">Mosfet.Org</a> for an update.



<!--break-->
