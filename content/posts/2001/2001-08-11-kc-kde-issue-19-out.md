---
title: "KC KDE Issue 19 is Out"
date:    2001-08-11
authors:
  - "tbutler"
slug:    kc-kde-issue-19-out
comments:
  - subject: "oops!"
    date: 2001-08-13
    body: "<embarassment>ok, it's not issue #30. it is issue #20. it was a typo. it will be fixed soon.</embaressment>\n\n*sigh* i really ought to take gallium up on that proof-reading offer. =P"
    author: "Aaron J. Seigo"
  - subject: "Re: oops!"
    date: 2001-08-13
    body: "When I first read this week's KC KDE I thought, \"Wow 30 issues has gone by fast...,\" although then I realized that the last issue was number 18. :-)\n\nSay, if you need a second proof reader, I'm all ears (hey, I will use any excuse to get to read KC KDE sooner). \n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: oops!"
    date: 2001-08-13
    body: "What about issue #19? :)"
    author: "KDE User"
  - subject: "WHat happened to the dot?"
    date: 2001-08-13
    body: "It happened, AGAIN????"
    author: "Antonio Gutierrez"
  - subject: "Re: WHat happened to the dot?"
    date: 2001-08-13
    body: "Never heard of Free-Weekends?\n\n\nAnon"
    author: "Anon"
  - subject: "LinuxToday"
    date: 2001-08-14
    body: "Why isn't this on LinuxToday?"
    author: "ac"
  - subject: "Re: LinuxToday"
    date: 2001-08-14
    body: "zack brown usually takes care of that. it should be appearing soon... an LT editor actually emailed me yesterday asking why it hadn't been submitted yet, so they noticed too which is somewhat bizarre but cool nonetheless.\n\nif it doesn't appear on LT by this evening I'll submit it myself. my guess is that Zack already has though."
    author: "Aaron J. Seigo"
  - subject: "usability studies"
    date: 2001-08-14
    body: "The usability study is a great intiative and is exactly what is needed at this point in the development of KDE..\n\nBut I am a bit confused about these reports being posted. Are these single individuals opinioning on the selected application in a report form? This might imply a possible misunderstanding about the nature of usablity studies. \n\nThe real value of a usability study is likely to be  in the empirical report of actual user behaviour. In other words: replicable reports about what selected users (typically not knowing too much about the application/utility and certainly not including the tester itself). \n\nThis requires presenting a group of users with tasks, and the report on the usability problems as matter of fact occuring. This could well be done efficiently in an informal way, for example by catching a few fellow students/colleagues not familiar with Linux/KDE and make then perform the central tasks: and then write a report of the problems occurring.\n\nIf one has suggestions on behalf of oneself it is probably better to post each suggestion on a discussion list... A single person - unless that person is a seasoned GUI-expert -  can basically only express one owns preferences and likings, which may or may not improve the usability for other people, and is not in a position to write a \"report\" about the usability qualities of an application."
    author: "will"
  - subject: "Re: usability studies"
    date: 2001-08-14
    body: "i am not an active participant in the usability project, but from following the threads on the usability list, what appears to be happening is this: they are  constructing an online testing system that different usability tests  can be uploaded to and results tracked through. at that point your general user can be put through the test(s). their experience will be pooled with all the other participants into one body of data.\n\nuntil that system is ready to go however, what appears to be happening is people are drafting reports that will be embelished upon later with results from the tests. right now they contain mostly known usability gaffs in the applications based upon known and agreed upon usability and (KDE) design standards."
    author: "Aaron J. Seigo"
  - subject: "Re: usability studies"
    date: 2001-08-15
    body: "Thanks for the info and - btw - thanks also for these great newsletters and for taking the no doubt considerable time to write them up..."
    author: "will"
  - subject: "Re: usability studies"
    date: 2001-08-16
    body: "\"The usability study is a great intiative and is exactly what is needed at this point in the development of KDE..\"\n\nI have to disagree. These studies would have been necessary two years ago. I see quite a few bad decisions that were made in KDE that could have been avoided if one had cared about usability from the start. Usability is nothing that you can add afterwards, you must consider UI issues from the start."
    author: "stew"
  - subject: "Re: usability studies"
    date: 2001-08-18
    body: "I am sorry but I disagree. I run the usability study for KDE and I beieve that usability can be engaged anytime in the development process. If it couldnt, we would have never improved interfaces."
    author: "Jono"
---
It looks like Aaron J. Seigo is at it again with another interesting double issue of <A HREF="http://kt.zork.net/kde/index.html">Kernel Cousin KDE</A>. This issue covers the <A HREF="http://usability.kde.org/">KDE Usability Project</A>, speed enhancements, Konqi improvements, what's ahead for KDE 3.0, and more. <a href="http://kt.zork.net/kde/kde20010803_19.html">Check it out here.</a>


<!--break-->
