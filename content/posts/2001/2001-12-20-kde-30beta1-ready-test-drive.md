---
title: "KDE 3.0Beta1:  Ready For a Test Drive"
date:    2001-12-20
authors:
  - "Dre"
slug:    kde-30beta1-ready-test-drive
comments:
  - subject: "Work enough"
    date: 2001-12-20
    body: "Hmm... they have to work day & night to get that all done :-)"
    author: "Storm"
  - subject: "Re: Work enough"
    date: 2001-12-24
    body: "Developer:\n\nOh, I'm a KDE developer\nI code all night and I code all day\n\nDotters:\n\nHe's a KDE developer\nHe codes all night and he codes all day\n\nDeveloper:\n\nI debug apps, I read my mail\nI sync my CVS\nOn Wednesdays I read the dot\nand debate the best of teas\n\nDotters:\n\nHe debugs apps, he reads his mail\nHe syncs his CVS\nOn Wednesdays he reads the dot\nand debates the best of teas\n\nHe's a KDE developer\nHe codes all night and he codes all day\n\nDeveloper:\n\nI debug apps, I click my mouse\nI like to write up memos\nI put on both my suit and tie\nand hang around in Redmond\n\nDotters:\n\nHe debug apps, he clicks his mouse\nHe like to write up memos  (confused look)\nHe puts on both his suit and tie\nand hangs around in Redmond?!?!? (horrified looks)\n\nHe's a KDE developer\nHe codes all night and he codes all day\n\n\"Developer\":\n\nI debug apps, I use Outlook\nVisual Basic and notepad\nI wish I'd been a paperclip\nJust like my dear Papa\n\n\"Developer\" and Dotters:\n\nHe debugs apps?  He uses Outlook?!?!?\nVisual Basic and notepad?!!?!?!!\n(Dotters walk away in disgust)\n\n\"Developer\": \n\nI wish I'd been a paperclip\nJust like my dear Papa\n\nWoman off to the side:\n\n\"Oh, Billy, you said you'd *invented* Open Source!!  Waaaaa...\"\n\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Work enough"
    date: 2001-12-27
    body: "I just read this and it cracked me up... how long did it take you to write that?\n\nThe Monty's would have been proud.\n\n--J"
    author: "Jason Hanford-Smith"
  - subject: "Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "I gave the SuSE RPMS a spin today. After some tweaking the SuSE binary RPMS let me start KDE3 just fine. Good.\n\nFirst impressions: Not much has changed. Some little tweaks here or there, some things have become a little faster. No crashes so far. Looks good.\n\nI have found a few little bugs:\n\nNo Font AA. Hmmm. My fault? SuSE's fault?\nNetwork broswing (either lan or rlan) still does not work, the old komba2 works.\nNo Euro currency for Germany (yet?) and the euro key is broken.\n\n\nWhat disturbs me is that Kcontrol is still not under control. \nHorrible new and old entries there:\n\nWhat is \"Network-SOCKS\"? No explanation. \"System-XML-RPC\"??? How will a newbie react to these? \"Talk\"?\n\nPeripherals-Keyboard let's me \"deactivate keyboard layout\". Hmmm, interesting.\nHow about letting me \"deactivate keyboard layout switching\"?\n\nThemes and Styles are still seperate, there is no usable way to configure sound events\n\nHmm, still lots of work to do... Bug reports to write and so on.\n\nDoes anyone get font aliasing to work?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of con"
    date: 2001-12-20
    body: "From the KDE3 info page ( http://www.kde.org/info/3.0.html ) :\n\n    * Apparently there is a bad interaction between kdeinit and XFree86 which causes XRender (and therefore Xft) support to be disabled. It is currently investigated."
    author: "Al_trent"
  - subject: "Re: kdeinit/Xft"
    date: 2001-12-20
    body: "Restarting kdeinit after KDE launches seems to work somewhat. Applications that are started afterwards have perfect anti-aliasing. Restart kicker, kwin and kdesktop and everything is smooth!"
    author: "Rob Kaper"
  - subject: "Re: kdeinit/Xft"
    date: 2001-12-21
    body: "I also had this problem, until I compiled QT (instead of using qt3.0.1 from debian/unstable) myself. That solved many bugs..."
    author: "nq"
  - subject: "Re: kdeinit/Xft"
    date: 2001-12-21
    body: "I also compile Qt (qt-copy) myself, so I wonder if that is really the problem.."
    author: "Rob Kaper"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "One Question about the SuSE RPM's. Do the SuSE RPM's replace the KDE2.2.2 system or is a parallel installation of KDE3beta and KDE 2.2.2 with this RPM's possible? I couldnt find a readme about this on the ftp server."
    author: "Bernd Lachner"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "3.0 does not overwrite 2.2...."
    author: "Juergen Appel"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-21
    body: "Does that mean that if I install it on my primary, working, daily use, desktop machine, I should have no problems?  In other words - can I just install the RPMs, add a new user, and login from KDM, picking on the dropdown \"KDE3\", and try it out, and then go back, log in as my normal user, and continue working as normal?\n\n--\nEvan \"A little nervous to try it, considering I have deadlines crunching my butt\" E."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-21
    body: "Absolutely. You should have no problems if you read the README file from your distribution."
    author: "Evandro"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "euro IS supported. that's a xfree issue, you have to bind the key in XF86Config.\nNOTE: you or your distribution (if you ask me, that's their job).\n\nhttp://www.kde.org/documentation/faq/configure.html#id2764594\n\n(yes, i know the link is broken at the moment)\n\nthere is even an example kword document with an euro symbol in it, just to show it IS done.. from the KDE side."
    author: "emmanuel"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "See also http://www.koffice.org/kword/euro.phtml"
    author: "David Faure"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "--Themes and Styles are still seperate\n\nand they should be, cause they are two completely different things"
    author: "[Bad-Knees]"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "AA worked here out of the box. I compiled KDE 3 beta1 from cvs.\nRegards,\nantialias"
    author: "Antialias"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "ummm, no it doesn't.  I looked at your screenshot and you deffinitely do not have anti-aliasing of fonts."
    author: "Travis Emslander"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "Check the titlebar"
    author: "Eron Lloyd"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "Yes, it was the wrong shoot :(. \nSee the new one :)"
    author: "Antialias"
  - subject: "Re: New Rule"
    date: 2001-12-21
    body: "There should be a hard and fast rule about posting screenshots - always state what theme/style/etc. you're using.\n\n    In this case - what theme, style, window decoration, color scheme, icon scheme, and fonts are you using?  Those icons are the first I've seen that might tempt me away from the KDE defaults.\n\n    Very nice.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: New Rule"
    date: 2001-12-21
    body: "Also the background - seriously, that's a nice desktop.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-21
    body: "What icon-theme is that?"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-21
    body: "A new one. Kudos to qwertz."
    author: "antialias"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-22
    body: "A damn nice one.\nIs it on kde-look.org?"
    author: "kde_user"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "QT 3 supports AA only for XFree 4.1 and higher. So the AA support is only compiled in for the SuSE 7.3 rpms."
    author: "Adrian"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "This is not the case, the problem is an interaction between kdeinit and XRENDER which disables Xft. The problem is resolved if you are running Xfree greater than 4.1, basically cvs."
    author: "vertiloto"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-21
    body: "I haven't had eny problems with AA, just had to edit ~/.xftconfig and installed my TrueType fonts to ~/.kde/share/fonts/TrueType."
    author: "Antialias"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: "This is not the case, the problem is an interaction between kdeinit and XRENDER which disables Xft. The problem is resolved if you are running Xfree greater than 4.1, basically cvs."
    author: "vertiloto"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-20
    body: ">Network broswing (either lan or rlan) still does not work, the old komba2 works.\n\nDid anybody say that network browsing will work with KDE3?\n\"Working Network browsing\" would be a completely new feature, maybe introduced with KDE3.2"
    author: "sfgfs"
  - subject: "lan browsing"
    date: 2001-12-21
    body: "I don't know what the problem is, rlan:/ works great here. Remember to set the reslisa binary to suid root and delete /tmp/resLisa-username and you're set! That's about all the setting up you need to do. \n \nMost rpms do not ship with suid binaries (rightly so, but this one should be safe as it drops privileges soon) so you'll have to do this once.\n \nSome other problems though, like plugins not working, and CUPS not being detected at all are keeping me from running it all the time, are these things issues or am I messing something up (SuSE 7.1 RPMS)? Very stable, no crashes yet."
    author: "cosmo"
  - subject: "Re: lan browsing"
    date: 2001-12-21
    body: "Silly me, forgot to install the nsplugins package.\n \nNavindra, can you delete the extra posts, got carried away :)"
    author: "cosmo"
  - subject: "lan browsing"
    date: 2001-12-21
    body: "I don't know what the problem is, rlan:/ works great here. Remember to set the reslisa binary to suid root and delete /tmp/resLisa-username and you're set! That's about all the setting up you need to do. \n \nMost rpms do not ship with suid binaries (rightly so, but this one should be safe as it drops privileges soon) so you'll have to do this once.\n \nSome other problems though, like plugins not working, and CUPS not being detected at all are keeping me from running it all the time, are these things issues or am I messing something up (SuSE 7.1 RPMS)? Very stable, no crashes yet."
    author: "cosmo"
  - subject: "lan browsing"
    date: 2001-12-21
    body: "I don't know what the problem is, rlan:/ works great here. Remember to set the reslisa binary to suid root and delete /tmp/resLisa-username and you're set! That's about all the setting up you need to do. \n \nMost rpms do not ship with suid binaries (rightly so, but this one should be safe as it drops privileges soon) so you'll have to do this once.\n \nSome other problems though, like plugins not working, and CUPS not being detected at all are keeping me from running it all the time, are these things issues or am I messing something up (SuSE 7.1 RPMS)? Very stable, no crashes yet."
    author: "cosmo"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-22
    body: "I too tried to give the SuSE rpm's a spin. Almost nothing works - immediate crashes in kmail, konqueror, kontrol, etc. etc. So I removed and went back to kde 2.2 (which is not as stable as 2.1)."
    author: "Malcolm Agnew"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-23
    body: "Please, tell us that you posted bug reports!  If you've got a knack for finding problems, use it to let developers fix them."
    author: "Will Stephenson"
  - subject: "Re: Looks virtually unchanged. KCONTROL out of control"
    date: 2001-12-23
    body: "No I didn't send a bug report. The reason was that so much didn't work with KDE3.0.beta1 that I was fairly desperate to  get back to a version  of KDE which worked.\n\nBut if someone can give me a hint on how I can do get something which works a bit better than my first disasterous attempt then I could give it another try. Christmas is a good time to experiment.\n\nFor instance, should I have first disinstaalled KDE2.2 (which I didn't).\n\nMy hardware is a new AMD 1.4 GB, 40 GB Disc, 640 MB memory very standard sytem.\ni.e. no problem installing first SuSE 7.2 and then updating to SuSE 7.3.\n\nFor development work I have been using Qt3.0 for some time but this was seperate from the version KDE2.2 was using and the version which can be installed from the SuSE KDE3.0 binary rpm's.\n\nMalcolm"
    author: "Malcolm Agnew"
  - subject: "Talk to us"
    date: 2001-12-20
    body: "If all that people did was submit a bug report when they came across errors it would be a start.\n\nPlease don't moan in public if you haven't told us by the official channels.\n\n:o)\n\nMalcolm\n\n(KDE English proof reader)"
    author: "Malcolm"
  - subject: "Testing, testing..."
    date: 2001-12-20
    body: "My Debian 2.2 box is now compiling Beta 1. I will also give beta 1 a try on a Solaris 5.8/Sparc machine too."
    author: "Loranga"
  - subject: "Re: Testing, testing..."
    date: 2001-12-20
    body: "Hi,\n\nI never got KDE to build on Debian 2.2 (Potato) after KDE 2.2, KDE 2.1 builds fine. \nDebian (dpkg-buildpackage) always complained dependencies were missing or outdated, when I upgraded all the dependecies I again broke other depedencies.\nThat's the reason I'm running Sid/Woody by now (only to get the latest KDE \npackages).\n\nCan you please tell me what you do to build KDE in Debian 2.2?\n\nThanks,\n\nMichael"
    author: "Mike Machuidel"
  - subject: "Re: Testing, testing..."
    date: 2001-12-20
    body: "Hi,\n\nDue to a slow machine, kdelibs hasn't finished compiling yet (PIII-550, 128 MB RAM, SCSI disks). I started to complile Qt 20 minutes after I posted the former message, so I can't give you a complete answer.\n\nBut anyway, the Debian 2.2 I use isn't vanilla (as you might expect :) ). \nQt 3.0.1 compiled without problems, but for kdelibs, Potato required some \"minor\" updates.\n\nI have installed OpenSSL and PCRE from source instead of using the Potato ones. I use the two day old PCRE 3.8. Potato provides 2.0.8, which is from 1999 :) Other dependency problems are libxml2 and libxlst, but they are not mandatory, so I skipped them. Of course, if you have your old prce installed, and not want to install the newer pcre, just use --disable-pcre.\n\nGood luck!\nMats\n\nBTW: Thanks KDE for updating the configure script with the following information: \n\n\"Some influential environment variables:\n  CC          C compiler command\n  CFLAGS      C compiler flags\n  LDFLAGS     linker flags, e.g. -L<lib dir> if you have libraries in a\n              nonstandard directory <lib dir>\n  CPPFLAGS    C/C++ preprocessor flags, e.g. -I<include dir> if you have\n              headers in a nonstandard directory <include dir>\n  CPP         C preprocessor\n  CXX         C++ compiler command\n  CXXFLAGS    C++ compiler flags\n  CXXCPP      C++ preprocessor\n\nUse these variables to override the choices made by `configure' or to help\nit to find libraries and programs with nonstandard names/locations\""
    author: "Loranga"
  - subject: "They should finish bugfixes before adding features"
    date: 2001-12-20
    body: "Bug 23205 is still a problem, although they saw fit to close it with no explaination given."
    author: "Antonio D'souza"
  - subject: "Re: They should finish bugfixes before adding features"
    date: 2001-12-20
    body: "As far as I know, the bug number 23205 is not theirs - it's fault of Xfree86. XRender extension that provides antialiased fonts, requires separate configuration file with separate directory list, and doesn't work with fonts provided by font server. As simple as that. GNOME just doesn't use antialiased fonts by default, it should be configured very special way. Offtopic,but anyway -  this font management thing has always driven me just crazy - why should I maintain two configuration files with two directory lists for antialiased fonts and for non-antialiased fonts? XRENDER for this, regular X don't know what for that... Add to that CUPS/ghostscript with yet another one font list, and the picture is perfect. Why can't there be one font server for it all, antialiased by request or by configuration, the same for display and for printing? Why create a \"font hell\" with our own hands?\n\nOk, on topic. I completely agree with your point - bugfixes before, features after. Bug number 33254 - the developers declared the feature working, and it doesn't work. And the bug is there since KDE 2.2 untouched  as of KDE 2.2.2.\nOr bug number 33152 - it is there since KDE 2.0. Little thing, but not so pleasant nevertheless."
    author: "Alex"
  - subject: "Re: They should finish bugfixes before adding features"
    date: 2001-12-20
    body: "Provide the \"still doesn't work with KDE x.x\" information to the bug system.\ndot.kde.org is not bugs.kde.org!"
    author: "someone"
  - subject: "Re: They should finish bugfixes before adding features"
    date: 2001-12-21
    body: ">Provide the \"still doesn't work with KDE x.x\" information to the bug system.\nThat's exactly what I did :-) I've provided bug numbers, as you see.\nThey declared some feature working, and then I submitted the bug."
    author: "Alex"
  - subject: "Better AA"
    date: 2001-12-21
    body: "And it would all be so much more worth it if only they could have the default AA be in 256 shades instead of just 5.  The freetype library actually includes an alternate smoothing algorithm which is far superior to the default one.  It produces 256 shades of gray in one pass, and is suposedly faster than the default renderer for font sizes under 20 points.  It seems that Nautilus and the GNOME desktop (but none of the other apps, of course) use this alternate module, but KDE still uses the crappy method.  \n\nFor an example, look at the difference between Times New Roman (or probably any roman font) at sizes 12, and 14.   At 12 it looks skinny and blury, and at 14 it looks fat and bold.  However, if Nautilus is configured to use Times New Roman, it scales very nicely.  I think the text produced by GIMP is another example.  \n\nBoth WinXP and OSX have finally gotten AA right, and for Linux to be a contender on the desktop, this is a must."
    author: "DaCh"
  - subject: "Re: They should finish bugfixes before adding features"
    date: 2001-12-20
    body: "Better write this to 23205@bugs.kde.org or ask the developer who closed it _as the text that informed you told_."
    author: "someone"
  - subject: "A shame that kmail will never get fixed..."
    date: 2001-12-20
    body: "You know what the biggest shame of all is?  The kmail developers are completely unwilling to fix their message threading problems.  Want to see it for yourself?  It's extremely easy to replicate...\n\n1. Subscribe to a mailing list.\n2. Use procmail to filter your list into a folder\n3. Use kmail via IMAP to read the folder on the mail server.\n4. Turn on threading for the mailbox for your list\n5. Start reading.\n\nPresumably you'll have several threads.  Suppose the first two threads, A and B came in in this order:\n\nA: 1,5,6,7\nB: 2,3,4,8\n\nWhen you first start reading the folder, you hit thread A first.  Suppose you delete the first message.  Kmail immediately removes this message from your view, then proceeds to re-order your mailbox, leaving this:\n\nB: 2,3,4,8\nA: 5,6,7\n\nI filed a bug on this, only to be told this isn't a bug, and in fact, I'm somehow reading my mail incorrectly, since I deleted a message.  I suggested they take a look at how threading works in mutt or even Evolution.  They still think they're right, and there's no problem.  Nothing like closed-mindedness, huh?"
    author: "jcostom"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-20
    body: "I cannot seem to duplicate your example here... \n\nI would try thinking harder, or at least present them with more descriptive errors.  I know that I have had two bugs fixed by them only after actually telling them the problem and provideing test cases and screen shots.  Chances are it is your config.\n\nCheers\n\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-20
    body: "Hi,\n\nWell, I do know the problem. When sorting by date, the threads are sorted by the date of the message starting the thread. If the user deleted the first message in the tread, this can cause the thread moving to a different position in some cases, because the date of the message starting the thread can be different now.\n\nI consider this correct behaviour, because this is just how sorting by date works.\n\nRegards,\nMichael H\u00e4ckel"
    author: "Michael H\u00e4ckel"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-20
    body: "To me it is also the correct behaviour. However, it would be nice to have 'Yet Another Configuation Option': \"do not rearrange threads when in date sorted view\". You could add an easy shortcut to it by having a RMB popup on the date/sender/subject bar with the option."
    author: "ac"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-21
    body: "I think rearranging the user's view is bad, even if it means the messages will be out of order temporarily.  Certainly the next time the folder is loaded the messages should be sorted that way, and even if the user tries to sort the messages again the should be rearranged that way.  However, switching the order of messages on deletion is bad behavior, even if it is technically correct.\n\nThink of it this way:  When you are looking through a list of messages, and you delete one, which is more important to you:  that the messages stay in absolute technically correct sorted order, or that the list stays the same as much as possible to avoid interrupting your train of thought.  I think the desires of the user win out over the strict sorting here."
    author: "not me"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-21
    body: "And how shall that work then, if new mail arrives in such a folder with broken sorting?\nBesides that we currently cache the sort order to not require resorting always the folder is selected. If we can't assume the mails being sorted correctely this causes a heavy slowdown with huge folders.\nThe only possibility I see to solve this is to mark messages as deleted and really remove them later."
    author: "Michael H\u00e4ckel"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-21
    body: "You could store a date for each thread, which is initilised to \nthe date of the first message of that thread.\n\nThreads are then sorted by their dates and emails are then\nsorted by their dates within their respective thread."
    author: "adrian Bool"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-21
    body: "> The only possibility I see to solve this is to mark messages as deleted and really remove them later.\n\nI was just about to suggest that, until I noticed you just did.  That seems like a good solution to me.  Would it be too much trouble to implement?"
    author: "not me"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-28
    body: "As I see it reading/deleting/moving mail and collecting new email are two different processes (OK I know that's just because I'm brought up on offline email readers, and have no use for biff or whatever to auto-reread my mail), but when I'm reading/deleting/moving my mail, I want to have the order preserved for the entire read/delete/move of all of my new mail.\n\nWhen new mail arrives it should trigger a sort process, before it is displayed, and I'm happy for new mail to re-arrange the sort order, but deleting/moving an email should NOT retrigger the sort process.\n\nThe two solutions as I see it are (as suggested by others) mark the message as deleted, and expunge later (as pine etc), OR delete the message, and make a journal of it for the later sort process, waiting for a (user option of: resort on newmail/manual resort only/resort on open folder/etc)sort process to kick in, thus allowing the sort process to maintain it's assumption that the list is always sorted.\n\nHope this is of some help to someone,\n\nWill\n  --"
    author: "Will"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-22
    body: "Heh..  Well, you go ahead and think harder. :)  There are plenty of folks that can easily re-produce this bug....  I'm simply amazed at the developers unwillingness to fix it.\n\nA fix is very simple - rather than delete a message immediately, flag it deleted, then expunge either when the user invokes an expunge option, or exits the folder, causing an expunge."
    author: "jcostom"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-22
    body: "If it is such very simple, where is your patch so we can discuss if this is better?"
    author: "someone"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-21
    body: "I agree. I've noticed something \"screwy\" with Kmail and threading. I drives me nuts when I delete a message and then I appear somewhere else in my list of emails (not at the next message). Very strange behavior. I've used Mozilla's mail client and it handles much more like I'd expect it to.\n\nScott"
    author: "Scott Patterson"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-23
    body: "Have you considered evolution. It seems lightyears ahead of kmail in both been mature and in facility. Excellent printing might I add and it also does proper html and embedded html. I use it in combination with kde all the time. Just my 2 cents :)\n\nCheers"
    author: "jargon"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-23
    body: "evolution is pretty bloated and too microsoft outlookish\nwhich makes me say gah. I only use pop3, so kmail is fine for me. in the changelog, it says imap support for kmail in kde3 has been done? can anyone verify this?"
    author: "gt"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2001-12-23
    body: "Why should they lie? Basic imap support is already contained in KDE 2.2.2."
    author: "anonymous"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2006-02-02
    body: "I found this place because I am annoyed by another sorting problem:\nWhen I sort a folder by date and it contains threads, I would like the threads sorted by considering the LAST entry in the thread, not the first. That way, I have the threads SORTED BY THE LATEST POSTS, ie the one with the latest post is always at the bottom (or top, depending on ^-v-selection). Thunderbird does it that way, KMail not :-/\n\nI believe when you deal with a \"great amount of items\", e.g. several 1000 mails from several mailing lists, all kinds of sorting and being able to easily change  and configure it becomes the TOPMOST IMPORTANT feature of the \"item dealer\", in this case the mail client. Maybe the developers don't have enough mail to play with? ;-)\n\nHave fun!\nDirk"
    author: "Dirk S."
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2006-04-05
    body: "I'm thinking of quitting using kmail because of the same sorting problem with which Dirk is annoyed. It would be great if anybody could change this."
    author: "Felippe"
  - subject: "Re: A shame that kmail will never get fixed..."
    date: 2008-02-09
    body: "That one feature I miss a lot, too!!! \nI was just in the process of switching to kmail - because of its good integration with calendar and todos - while I realized, that that kind of sorting is missing. \nThat made me puzzled. I am thinking what to do ...\nI consider this absolutely crucial, with great amount of emails we get nowadays, especially from different sources, using different frequencies, etc.\nUsing this sorting, in a natural way, you get the most urgent matters on top/bottom of your mailbox, still inside its context (threads).\n\nAre there any plans to make it possible?\n\nOther thing - I like the feature, that I can drop an email as todo item - but I do not get this email as an attachment - as I get when I drop email as an calendar entry - this probably could be fixed easily ... pls :)"
    author: "Adam "
  - subject: "Yay!"
    date: 2001-12-21
    body: "From the feature list:\n\n\"KWin\nswitching desktops as necessary when dragging a window, <bastian@kde.org>\"\n\n\nThis is great, probably the only thing that has been bothering me about KDE compared to Gnome or Wmaker..."
    author: "scb"
  - subject: "Re: Yay!"
    date: 2001-12-21
    body: "I also love the possibility to know the size of a window when resizing (impossible with kwin)"
    author: "Anonymous"
  - subject: "I'm all right"
    date: 2001-12-22
    body: "such a feature is very useful when you develop a web site, and want to see your work with an exactly 800x600 window for example."
    author: "Capitaine Igloo"
  - subject: "Arabic and RTL"
    date: 2001-12-21
    body: "Ok, the SUSE RPM's worked fine on SuSE 7.2 i386..\n\nBut how we are going to test the Arabic Lang & RTL?\ni can't find it in the KDE control panel..\nany idia here?\n\nDan"
    author: "Dan"
  - subject: "Screenshots!!!!!!!!!!!"
    date: 2001-12-21
    body: "I'm always asking this, but \"Screenshots!!!!!!!!!!!!!!!!!!!\""
    author: "screenshot-addict (Rajan Rishyakaran)"
  - subject: "Re: Screenshots!!!!!!!!!!!"
    date: 2001-12-22
    body: "And it's always the same answer:\nGo to www.themes.org and take the screenshots from there, as a screenshot only shows you the current style and theme.\nIf you are asking for screenshots for applications, then go to the applications page, but the applications in KDE3 haven't changed much yet."
    author: "nofips"
  - subject: "Re: Screenshots!!!!!!!!!!!"
    date: 2001-12-22
    body: "http://www.kde.org/screenshots/kde3shots.html ;-)"
    author: "Anonymous"
  - subject: "How to add KDE3 to the KDE login manager?"
    date: 2001-12-21
    body: "I have an out of the box Mandrake 8.1 Linux box and I installed KDE3 with rpm -ivh --force --nodeps *.* . Now, I can launch KDE3 applications, but I want to have the full KDE3 'experience'. How can I add KDE3 to the KDE login manager? (I already tried to add startkde3 to the KDE login manager in KDE Control panel, but this doesn't work). What should I do?"
    author: "Kristof"
  - subject: "Re: How to add KDE3 to the KDE login manager?"
    date: 2001-12-21
    body: "Add kde3 instead of startkde3."
    author: "Syllten"
  - subject: "Re: How to add KDE3 to the KDE login manager?"
    date: 2002-01-15
    body: "I have a similar problem, I also have a Mandrake 8.1 Linux box and have tried to install KDE 3 with rpm -ivh --force --nodeps *.*\n\nHowever I cannot even launch programs or anything and the only evidence of its installation is the presence of 'startkde' command, so what do I do now?"
    author: "Roger"
  - subject: "Re: How to add KDE3 to the KDE login manager?"
    date: 2005-03-22
    body: "Well, this is too late but you can try and look at directory /etc/X11/Sessions and see what you got.\n\nCreate a kde3 script or edit an existing one and save it as kde3."
    author: "whitescarf"
  - subject: "CUPS?"
    date: 2001-12-21
    body: "I've installed SuSE 7.1 RPMs and the printing system doesn't seem to like my CUPS installation which worked perfectly with 2.2.2. It's not even offered as an option. Is this the packages' fault or am I doing something wrong? \n \nThat and Xft. Whatever I do, I can't get my TrueType fonts working."
    author: "cosmo"
  - subject: "Re: CUPS?"
    date: 2001-12-22
    body: "Was SuSE 7.1 delivered with CUPS? If not, the RPMs will not contain CUPS support.\nDid you install KDE 2.2.2 from sources?"
    author: "someone"
  - subject: "Re: CUPS?"
    date: 2001-12-23
    body: "SuSE 7.1 does not use CUPS as a default printing system, although I think it is offered on one of the CDs. I installed the RPMS for both KDE 2.2.2 and 3.0b1. In the 2.2.2 case, the CUPS support was there even before I started using CUPS at all. \n \nIt is possible that it is not compiled in by default, I'll have to check by compiling from sources I guess."
    author: "cosmo"
  - subject: "New kdeedu package: feedback needed"
    date: 2001-12-22
    body: "Hi all, \nPlease note the new kdeedu package (for the first time in KDE) which aims to provide educational software for parents and teachers. This new project needs your feedback and testing and also new ideas are welcome.\nWebsite: http://edu.kde.org/\nMany thanks in advance and Merry Christmas!\n\nannma"
    author: "Anne-Marie"
  - subject: "Re: New kdeedu package: feedback needed"
    date: 2001-12-26
    body: "Trust me... im gonna test 'em all over and over again :) I think that kdeedu is the way for KDE since most schools dont have all that much to spend on edu-software (since win9x + office eats the budget). And KDE would be a Good Thing (TM) in that particular sector... since thats where the next generration of KDE developers are comming from ;-D\n\n/kidcat\n--\nhardtoreadbecauseitissolong"
    author: "kidcat"
  - subject: "Some windows don't scale right"
    date: 2001-12-23
    body: "KDE3 works fine most of the time, but there are some annoying things. E.g. some windows don't scale right (Have a look at then attached picture). This makes it very difficult use certain KDE3 applications (desktop settings is one of them)."
    author: "Kristof"
  - subject: "Re: Some windows don't scale right"
    date: 2001-12-23
    body: "It is beta, isn't it?"
    author: "antialias"
  - subject: "Re: Some windows don't scale right"
    date: 2001-12-26
    body: "This problems exists also in the Kde-2.x series when you change the fonts sizes.\nThe windows doesn't scale right and you don't have the display of the whole area. I didn't find out why :-("
    author: "Jean-Christophe Fargette"
  - subject: "How exactly can people like me help with KDE 3?"
    date: 2002-01-14
    body: "quote:\n\"Since beta releases always provide an ideal opportunity for newcomers to get involved and make a difference, the release also provides intriguing suggestions to those of you who may in the past have pondered, \"I really want to contribute to KDE and help make my favorite desktop even better, but what can I possibly do?\"\"\n\nWhat exactly are those intriguing suggestions?\nI am one of those peopel who has often pondered the wanting to make a difference but don't know how question, I have installed KDE3 but apart from maybe submitting bug reports I can't think of any way to really help as although I know a little bit of C, c++, JAVA, Perl etc. I have no where near the knowledge required to start hacking away at KDE 3's advanced system routines etc.\n\nSo, how exactly can people like me help with KDE 3?"
    author: "Malcolm Davis"
  - subject: "Re: How exactly can people like me help with KDE 3?"
    date: 2002-01-14
    body: "http://www.kde.org/jobs.html especially \"Open jobs\" as mentioned in the announce."
    author: "someone"
---
The KDE Project today (OK, yesterday now)
<a href="http://www.kde.org/announcements/announce-3.0beta1.html">announced</a>
the release of KDE 3.0Beta1.  The announcements contains the typical
package locations and summary of changes (visit the
<a href="http://developer.kde.org/">developer's website</a> for
a <a href="http://developer.kde.org/development-versions/kde-3.0-features.html">more detailed list</a> of planned feature additions for
KDE 3.0 and the current progress).  Since beta releases always
provide an ideal opportunity for newcomers to get involved and make a difference, the release also provides intriguing suggestions to
those of you who may in the past have pondered, &quot;<em>I really want
to contribute to KDE and help make my favorite desktop even better, but
what can <strong>I</strong> possibly do?</em>&quot;.


<!--break-->
