---
title: "Kernel Cousin KDE #15"
date:    2001-06-29
authors:
  - "Inorog"
slug:    kernel-cousin-kde-15
comments:
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-29
    body: "To be a-retentative, you cannot really say KDE is forking those styles.  You have to consider that other people have contributed and worked on those styles.  Some styles such as B2 contain much code by other developers.  KDE is simply continuing to maintain what has always been there."
    author: "ac"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-30
    body: "Though the things in CVS are just continuing on, in one instance of Daniels's code, it could be said that I am forking.\n\nThe \"Liquid\" widget, KWin, and color styles never were in cvs.  I downloaded it, and am trying to finish it.  At first, I just saw myself as continuing maintenance of something that lost its maintainer.  But since I guess there is the chance Daniel may return, I decided not to use the name Liquid, and renamed it Glass.\n\nAnd since it's likely I may have to disable the transparent menus, I guess Glass does qualify as more of a fork."
    author: "Neil Stevens"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-30
    body: "and.... where can we download it?"
    author: "me"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-30
    body: "Honestly, I don't believe you should fork the Liquid widget/kwin/color styles and put it in CVS.  This style is a brand new Mosfet style and even Mosfet was refused to enter it into CVS.\n\nNow that you got rid of Mosfet, you are going to import Liquid?  That's ridiculous!  And the transparent menus is one of the features that distinguishes Liquid and surpasses Aqua!\n\nMosfet is back and will be developing Liquid.  I don't object to a fork, but having that fork in CVS is controversial."
    author: "ac"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-30
    body: "> Honestly, I don't believe you should fork the\n> Liquid widget/kwin/color styles and put it in\n> CVS. This style is a brand new Mosfet style\n> and even Mosfet was refused to enter it into\n> CVS.\n\nCareful now. Mosfet was refused to enter it into the *kdelibs* module *during the freeze*.\n\nGlass is very welcome to enter CVS in kdeartwork (or where the styles in kdeaddons? hmm...) when CVS is unfrozen again for KDE 2.3, supposing Neil has taken some of the \"change the look a little bit\" legal suggestions that were part of the discussion."
    author: "Rob Kaper"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-29
    body: "So, uh, once KDE _has_ been compiled with GCC 3.0, what kind of performance can we expect?  Will a GCC 3.0-compiled KDE be faster?  Or slower?\n\nAlso, how does GCC 3.0 affect binary compatibility issues?  I heard the new ABI will allow more changes between versions while maintaining binary compatibility.\n\nSince the ABI is changed, doesn't that mean there's a new (or heavily modified) dynamic linker?  Has it been improved in speed?"
    author: "not me"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-29
    body: "AFAIK:\nPerformance depends on the processor. IF you use a binary pre-compiled KDE, it is probably compiled for some sort of generic Pentium; so if have anything better than that you won't gain much.\nThe Big improvements of gcc-3 are in processor-specific optimization. So if you compile KDE yourself than, yes, you'll get a performance benefit. gcc-3 is especially better than 2.9x with AMD cpus. You should check the gcc homesite for the details."
    author: "Dan Armak"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-30
    body: "Well, just to add a few pointers.\nGcc3 has allready a few verified bugs in the handling of C++ code and they will surely be resolved in the first maintenance release. futhermore it is _not_ smart to upgrade to gcc 3.0 just now, every release of gcc with a minor of 0 (in our case major 3 minor 0) has been known to still have quirks that need to be ironed out.\nGCC 3.0 as a whole does have a lot of optimisations in regards to code generation. Some more new CPU extensions and code optimizations have been added and once all the quirks are corrected, the code generation of C++ code should be a lot more clean than the one of the last official release 2.95.3"
    author: "darian Lanx"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-29
    body: "Ah, Rod Kaper's also helping out with the KC. Thanks, Rod."
    author: "Matt"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-29
    body: "Rob Kaper, I mean, of course. Thanks, Rob! :)"
    author: "Matt"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-29
    body: "You're welcome. :-)"
    author: "Rob Kaper"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-29
    body: "I just wish that kpilot in the KDEPIM project would work with my Visor... All I get is \"no such device /dev/pilot\" or something along those lines.. Everything works fine with Jpilot (pointing to /dev/pilot also...) Are some sort of complier switches or something to get it to work with a USB Visor? Kpilot has never worked with my visor in any version of KDE I've had..\n\nI'll tell ya this, I've been using KDE 2.2alpha2 for a week or so now, and it rocks.. Except for that one issue above...\n\n--Garion"
    author: "garion911"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-29
    body: "KPilot (as well as all of KDEPIM) is in progress of a major redesign probably for KDE 2.3.\nAs a starting point, Adrian de Groot imported\nKitchenSync into kdenonbeta. It aims to be a\ngeneric syncing interface for all kind of devices (hint for the QPE develpers :)). Support for Visor is planned, too. \n\nGreetings,\n\nDaniel"
    author: "Daniel Molkentin"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-30
    body: "Shouldn't KPilot, KPIM, KOrganizer, KMail, KNode be one project? Or at least there should be some major integration between them."
    author: "Robert Gelb"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-30
    body: "They are part of one project. The name of that project is KDE.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-30
    body: "I meant part of one application."
    author: "Robert Gelb"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-30
    body: "That is a classic one line Waldo answer, do you use a script for these?\n\nI believe the author meant that these programs should be more tightly intergrated for example the way M$ foutlook is with schedule, caldering & mail etc. The answer is that this is the general idea as this is exactly what Mike wants to aim at; to make a full PIM framework. \n\nFurthermore, Kmail can already use the KDE address book that is share with the PIM apps so it seems work is progressing in that direction."
    author: "David Ankers"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-30
    body: "IF you want Outlook, use Outlook.  Don't look for KDE to clone it and save you some money.\n\nNo, this isn't a script, but it could be for how often I have to say this."
    author: "Neil Stevens"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-30
    body: "Neil, with due respect I feel that misses the point; doesn't the fact that you have to say this so oftern reveal that there is a huge demand for such an app? Ximian and the Kompany certainly think so. Like it or hate it, outlook is a damm nice intergrated app.\n\nSaving money is far from the reason that these requests are made and I fail to see the logic there. People *want* to use KDE but find they miss certain fuctionality that they require to be 100% productive. Following your logic you could also state that people use KDE just to save money on Windows, regardless adding to KDE's user base is not a bad thing what ever the reason.\n\nAnyway, KDE will get an app with the fucntionality of Outlook sooner or later thanks to KParts, if the new pim framework is designed with kparts in mind and calendaring / sticky notes etc are made as parts not as fixed apps combining them in to an elegant gui will be a lot more straight forward.\n\nThe dot is a user's site more than anything, it where the end users visit to keep up with kde developments. Given this I don't like to see smart comments from core developers when a user asks an honest question on a users site. If he posted this to core-devel of course beat him with a clue stick but remember all of kde's developers, doc writters & artists were all users first."
    author: "David Ankers"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-07-01
    body: "this is assuming that the Outlook style is actually better. not everyone believes this (I am one of those non-believers). i rather detest applications that try and do too much in one place, they are a product of operating systems that are not robust, do not have a sufficiently advancec component model and/or screen real estate IMO. in windows/mac its much nicer to have a bunch of functionality in one window (ala windows style MDI) because as soon as you start opening lots of applications things get very crowded and messy. with X and its multiple desktops, this really isn't an issue. add to that the fact that *nix can withstand running dozens of applications simultaneously w/out blowing up, and you have the option of using many purpose built applications that work well together. sounds kind of like one of the base philosophies of Unix right? well, it is. and KDE is a desktop for (everybody now) Unix.\n\nwhat if you have a notes app, an addressbook app, an email app, etc... that you can run seperately but also work together perfectly; e.g. you can connect a note to an email (not possible right now in KDE2), or use the same addy book in your email app, organizer and _any other application that requires it today or tomorrow_ (which is how its done right now in KDE2)?\n\nshoving all these pieces into one big window via kparts is not only lacking in imagination, but something that would actually be less efficient and usable given the option of multiple cooperative applications.\n\nmaybe users need to be reeducated a bit to understand the benefits, but there is no reason to sacrifice the chance to do it Right just because some users have the wrong idea (mostly thanks to experiences with less capable OSes)."
    author: "Aaron J. Seigo"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2005-03-16
    body: "This is nice to see, I came across this post while trying to track David down via google to offer him a job, hopefully he is still in Holland. As well as being one of the best Cisco guys about, he also put me on to KDE which I'm now using company wide. Well, 4 years later, one of the best things about KDE is the Kontact app, which is intergrated mail, calandar etc exactly like Outlook.  "
    author: "James Church"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-06-30
    body: "as waldo pointed out, they are part of the same project in that they are all applications developed under the umbrella of KDE.\n\nobviously there are going to be different people working on the individual applications, but being part of KDE as a whole opens up the opportunity to create and share large infrastructure parts between them. up to now, the PIM applications in KDE haven't really kept up with the level of integration we are seeing elsewhere in KDE. that was the entire point of the very long discussion that has been taking place the last few weeks on kde-pim and which was covered in this KC KDE.\n\ni didn't want to get into specifics in the summary beyond the fact that there was a call to action and that discussion is occuring because i really don't want to pre-announce things that don't happen giving KDE a reputation for being \"vapour ware\". i'd recomend going to the kde-pim mail list archives yourself and reading about some of the rather fascinating ideas that are being tossed around with the intent of implementation."
    author: "Aaron J. Seigo"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-07-01
    body: "The KC link talks about much data integration with the whole keeperd system. This really makes sense to me, all this data is handled by the same daemon.\n\nI assume that what you are referring to is making them the same app, ala Outlook. I always disliked Outlook because it forced me to integrated unrelated tasks into the same GUI. For the life of me, how the heck is email related to scheduling and sticky notes? No, lots of seperate apps connecting to a single daemon are better because then the designer can use the right UI for the right task, and keep these UIs as far away from the protocol as possible."
    author: "Carbon"
  - subject: "Confusion ?"
    date: 2001-07-01
    body: "Today Waldo, Aaron and Carbon say that the PIM KDE will be integrated in KDE as a same application in several applications. And there is a will to do something different and better than Oulook. OK.\n\nBut two weeks ago, in the page \"Focus on infusion\" (http://dot.kde.org/992627943), there was a will to do something like Outlook, called Infusion, Magellan or Aethera.\n\nSorry, I am confused. I see contradictions or I badly understand..."
    author: "Alain"
  - subject: "Re: Confusion ?"
    date: 2001-07-01
    body: "The projects Infusion, Magellan, Aethera, and KDE PIM are all seperate. All but the last are not actually part of KDE, but produced by an outside party, and all have slightly different approaches."
    author: "Carbon"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-07-01
    body: "What about Kvirc ? Why Kvirc is not part of KDE 2.x ? i mean, i think the problem are the libraries, but why they dont work hard together (KDE and Kvirc team) to include kvirc into KDE 2.x ? I think its the better irc X client for Qt/KDE and Linux, and its so strange that KDE team include into KDE 2.x serie the Ksirc irc client what is very bad."
    author: "Daniel"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-07-02
    body: "> Why Kvirc is not part of KDE 2.x ?\n\nI'm personally say (I am not a KDE community developer) that one good reason is that kvirc violates most KDE UI standards.  I use it, and it drives me nuts... for one thing, it uses MDI, which is verboten for standardized KDE apps.\n\nCheck out Gwenview, which (almost) perfects a new SDI interface that I really like - windows that can dock together or be ripped apart and placed around your desktop.  It's not standard either, but at least it uses KDE widgets, unlike kvirc (which I use and enjoy).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-07-03
    body: "\"kvirc violates most KDE UI standards\"\nAnd why they dont work on it? Kvirc team i mean ...\n\n\n\"it uses KDE widgets, unlike kvirc\"\nits not very difficult reprogram it to use KDE widgets in stead of Qt widgets .. \n\n\nBut i repeat, Kvirc is better than ksirc .. and if we want that KDE to be a Desktop, we need features like kvirc, and not like Ksirc."
    author: "Daniel"
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-07-03
    body: "First you say: \"And why they dont work on it? Kvirc team i mean ...\", and then, \"\nits not very difficult reprogram it to use KDE widgets in stead of Qt widgets\".  So do it!  You *ARE* a KDE developer - everybody on the planet is.  You just haven't contributed any code (yet).\n\nA guess that I'm just going to toss out (this is not based on anything but conjecture) is that the people who have written the current codebase for KVirc have been more interested in features and all the things you like about it, and have just left the interface alone.  Maybe it started out as a Qt app, and they consider updating the UI a low priority because they are more concerned with the feature set.  Sending patches to people is a good way to make friends, start a hobby and feel a bit of pride when a app starts up that you contributed code to.\n\n--\nEvan (Who sits on more patches than he sends, and should take his own advice)"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kernel Cousin KDE #15"
    date: 2001-07-03
    body: "And why KDE team dont include more stable and serius aplicationes as KMyMoney2, Kapital or K3b (the best burning program) into KDE 2.x serie ? I think there are lot of programs that have to be include into it."
    author: "Daniel"
---
Here's Rob Kaper's and Aaron Seigo's priceless <a href="http://kt.zork.net/kde/kde20010622_15.html">KC KDE #15</a>. Select your choice of mailing-list sublimate: style code maintainance rockades, PIM roadmaps, multithreading, GCC3 issues, and much more.
<!--break-->
