---
title: "KDE Edutainment Project Takes Off"
date:    2001-09-22
authors:
  - "Dre"
slug:    kde-edutainment-project-takes
comments:
  - subject: "Big thanks to the translators"
    date: 2001-09-21
    body: "The Announcement is available in seven languages on the website, and they are English, German, French, Spanish, Dutch, Hebrew, and Russian.\n\nOn behalf of the KDE Edu team, I would like to thank the people who did the translations, some at extremely short notice. \n\nThanks people, you were great! You made the task of preparing the announcement and website so much more pleasant. I know now why I like the Open Source community, and especially the KDE people!"
    author: "tap"
  - subject: "School Library book inventory and borrowing"
    date: 2001-09-22
    body: "Is anybody planning to launch a program for school library book\ninventory and book loan?\n\nI have been looking for such program from open source. A german \nweb-base php application was found but the programmer is no plan\nto translate it."
    author: "Jeff Tong"
  - subject: "Re: School Library book inventory and borrowing"
    date: 2001-09-22
    body: "Try Koha, it's a web based system, and it works quite well. It's not specifically for school libraries, but I can't imagine how it wouldn't work for one."
    author: "Carbon"
  - subject: "Re: School Library book inventory and borrowing"
    date: 2001-09-23
    body: "Have you tried http://www.seul.org/edu/ ?  Excellent resource for using Linux in the education sector."
    author: "John"
  - subject: "Re: School Library book inventory and borrowing"
    date: 2002-01-09
    body: "I am looking for this also, did you ever find anything?"
    author: "Samantha"
  - subject: "Topography suite"
    date: 2001-09-22
    body: "One thing that would be really hard to make, but which would be immensely cool if achieved, is a topography package. Does anybody know if such a thing already exists for Linux in general? I think the hardest part would be to obtain good graphical representations of area's. Has anyone an interest in such a project as well? I don't think I would be able to start such a project or do heavy programming for it, but if someone else is I would certainly be interested in helping out where I can.\n\n-- \nMatthijs"
    author: "Matthijs Sypkens Smit"
  - subject: "Re: Topography suite"
    date: 2001-09-22
    body: "I think that's it a great idea, please comeover to the #kde-edu channel, and we'll discuss this further under open forum."
    author: "Elhay Achiam"
  - subject: "Re: Topography suite"
    date: 2001-09-22
    body: "Check out this site this guy is trying to do just that. \nhttp://www.kartographer.org/"
    author: "Craig"
  - subject: "Re: Topography suite"
    date: 2001-09-23
    body: "Hmmm, seems my musings at kartography.org have attracted some attention :-)  I've updated the page with a lot more information about my ideas, links and the many resources available out there.  Due to problems with my domain parking service, you can find it <A href=\"http://members.austarmetro.com.au/~jlayt/kartographer/\">here</A> until they get their act together.\n\nAs you will read on the page, I don't really have the time or skills to lead this, but I'm willing to help anyone who wants to run with it."
    author: "John"
  - subject: "Re: Topography suite"
    date: 2001-09-24
    body: "OK, parking problems solved, it's now up at www.kartographer.org"
    author: "John"
  - subject: "Language tutor thingy"
    date: 2001-09-23
    body: "I tried out that Klatin (did it at school) but it just \ncame up with a big blue screen. I know latin is a dead\nlanguage and all, but blue screens? Alt-Tab thankfully \nstill works.\n\nAaanyway, surely all these language tutors should be \nsome kind of unified thing, rather than one for each language. \nI really don't know how easy it would be to store translation tables in anything but language to language, which makes n*2 for the number of \nlanguages. It might be possible to do some kind of fallback. \nEg Romantic languages fallback to latin. So eg you have \nFinnish->Latin->French. I don't think mandating english\nas intermediate would work. This is a big hairy subject \nthat people do whole degrees on, so I woun't go into it \nanymore...  I dunno."
    author: "Rob"
  - subject: "Re: Language tutor thingy"
    date: 2001-09-23
    body: "This was suggested when the project was at an early planning stage. However, it was decided that since one language is so different from another to learn, that this approach would not work."
    author: "Chris Howells"
  - subject: "Re: Language tutor thingy"
    date: 2004-05-10
    body: "Live the name not inbrase it"
    author: "bob"
  - subject: "Re: Language tutor thingy"
    date: 2001-09-23
    body: "I have the answer for the blue screen: it is because KLatin installs in /usr/local/bin and cannot find the pics related to the app. \nWhat you need to do is:\n./configure --prefix=/your/kde/dir\nand then it will be OK.\nI'll add that to the KLatin page.\n\nAs for teaching languages, and as a matter of fact for any other edu project, this is just the beginning but we have to start with something. We already talked a lot about how things could be done but in the end, we are just a bunch of beginners that try to code. I just know that even if the application is a small one, it is a beginning and it is used in schools (KLettres which is really very small will be adapted to english soon for a school).\n\nannma"
    author: "annma"
  - subject: "Re: Language tutor thingy"
    date: 2001-09-23
    body: "hmm... Just a big blue screen?\n\nOn all the test systems I've run so far, it should come up with a blue screen with the words \"Welcome to KLatin, Press any key to continue\". Basically, press a keyboard key, and it'll take you to a selection screen. Then I've only got a vocab trainer, which has a limited vocab. Oh... And if you were running it in KDevelop, with \"Execute Program\", it won't work. You need to make install first. I'd better start writing an FAQ for this stuff. :)\n\nGeorge"
    author: "George Wright"
  - subject: "Edutainment?"
    date: 2001-09-24
    body: "Edutainment?\n\nThat is a bad name in my opinion.  It sounds like something Elmer Fudd would say."
    author: "illya_the_great"
---
The <a href="http://edu.kde.org/">KDE Edutainment</a> team today announced
the official launch of the KDE Edutainment Project. The project's
goal is to create educational software based around
<a href="http://www.kde.org/">KDE</a>, and not just for children.  I have been watching this project grow momentum over the past two months and the team has achieved great enthusiasm, organization and, hence, promise.  The press release follows; and there is of course a lot more information available on the project's <a href="http://edu.kde.org/">web site</a>, including information on how to join and help this worthy project.

<!--break-->
<p>&nbsp;</p>
<p>DATELINE SEPTEMBER 21, 2001</p>
<p>FOR IMMEDIATE RELEASE</p>
 
<h1>KDE Project announces new education software project for Linux/UNIX</h1>
 
<h2>New educational and entertainment software for KDE</h2>
 
<p>September 21, 2001 (The INTERNET).
The <a href="http://edu.kde.org/">KDE Edutainment</a> team today announced
the official launch of the KDE Edutainment Project. The project's ultimate
aim is to create educational software based around
<a href="http://www.kde.org/">KDE</a>.  The press release follows.
 
<p>By doing this, the team aims to address what we perceive to be one of Linux's shortcomings: that there is little software suitable for children, or of an educational nature. Eventually, the 'kdeedu' module will be released simultaneously with future releases of KDE.</p>
 
<p>The KDE Edutainment web site, <a href="http://edu.kde.org/">http://edu.kde.org/</a> is the main place to obtain information about the project, and the software that it contains. The site contains information suitable for parents, developers (including programmers, artists, musicians), and teachers.</p>
<p>You can join the public <a href="http://mail.kde.org/mailman/listinfo/kde-edu/">'kde-edu'</a> mailing list if you would like to contribute to the project in
some way, or if you want to contact the developers. The <a href="http://mail.kde.org/mailman/listinfo/kde-edu-news/">'kde-edu-news'</a> mailing list is a read-only list where announcements and a regular newsletter, regarding the project, will be available. You can also join us on IRC, on server irc.kde.org, in channel
#kde-edu.</p>
 
<p>We are still looking for many more people to contribute to this open source software project, so if you are interested in contributing in some way, please contact <a href="mailto:kde-edu@kde.org">kde-edu@kde.org</a>.  If you still have any further unanswered questions, please consult the Frequently Asked Questions below. If your question still remains unanswered, please contact <a href="mailto:kde-edu@kde.org">kde-edu@kde.org</a>.</p>
 
<hr />
 
<h2>Current Applications</h2>
 
<p>Currently, the project consists of the following applications</p>
<ul><li><a href="http://edu.kde.org/khangman/">KHangMan</a> -- A hangman-style game</li>
<li><a href="http://edu.kde.org/keduca/">KEduca</a> -- A form-based revision and exam program</li>
<li><a href="http://edu.kde.org/klatin/">KLatin</a> -- A Latin language tutor</li>
<li><a href="http://edu.kde.org/klettres/">KLettres</a> -- A French language tutor, for very young children</li>
<li><a href="http://edu.kde.org/ktouch/">KTouch</a> -- A program to teach touch
typing</li>
<li><a href="http://edu.kde.org/kmessedwords/">KMessedWords</a> -- Rearrange the letters to form words</li>
</ul>
 
<h2>Frequently Asked Questions</h2>
 
 
<p>
<strong>Q</strong>:  <em>What is Educational Software (also known as Eduware or
Edutainment)?</em>
</p>
<p>
<strong>A</strong>: Educational software is software that helps a student of any age learn a skill. For example, a child may learn to calculate the perimeter of a shape, or to use language correctly (such as the difference between <em>its</em> and <em>it's</em> in English); or an adult may learn how to cook.</p>
 
<p>Educational software should be fun, colorful and attractive. In addition, the pupil (except in the case of young children) should be able to use the software without parental supervision.  See the <a href="http://edu.kde.org/screenshots/">screenshots</a> on our website for more information.</p>
 
<p><strong>Q</strong>:  <em>Who can take part in the project?</em> </p>
 
<p><strong>A</strong>: <b>Anybody</b> can join the project! We welcome people of all ages and abilities, and there is no need to be a developer. Many different
people with different skill sets will be needed to make the project a success. These include:
</p>
<ul>
<li><i>Artists:</i> To create mascots, icons, pictures, animations, etc.</li>
<li><i>Designers:</i> To design the overall interface of an application, to give consistency in the project so the pupil could switch from maths to languages and find the same sort of interface.</li>
<li><i>Developers:</i> Both beginners and more experienced developers are welcome, everyone should be able to find or start a suitable application project. These application projects will be hosted in KDE CVS.</li>
<li><i>Musicians:</i> For sound and music, as well as recorded voices.</li>
<li><i>Parents and teachers:</i> Feedback is needed to tell us what is needed and how it can best be accomplished, to test alpha and beta versions, to help with the content, etc.  If you have an idea, send a application proposal, and your idea will be placed in our <a href="http://edu.kde.org/projects/">application project list</a>.</li>
<li><i>Children:</i> Children, will of course be required to help test the software. They can also tell us their ideas and wishes.</li>
</ul>
 
<p>After the application projects are launched, they will be listed on our <a href="http://edu.kde.org/projects/">application status page</a>. You will then be
able to contact people directly to join a particular application project.</p>
<p><strong>Q</strong>:  <em>Where can I get more information about the project?</em></p>
<p>The best place to start would be the <a href="http://edu.kde.org/">http://edu.kde.org/</a> website, where you can find, amongst other things, an <a href="http://edu.kde.org/projects/">application projects page</a> listing our current software.</p>
 
<p>If you are interested in joining the project, or would like to make a suggestion about what the project should create, please feel free to contribute!</p>
 
<p>Copyright &copy; 2001 <a href="http://edu.kde.org/">The KDE Edutainment Project</a>. All rights reserved.</p>
