---
title: "Newsforge: Developing for the Linux desktop"
date:    2001-07-13
authors:
  - "Dre"
slug:    newsforge-developing-linux-desktop
comments:
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-13
    body: "And how does Tina give tips? By quoting you for 80% of the article, dear Dre. :-)\n\nStill good stuff though, except the part that claims that KDE/Qt development is hard to start with (as opposed to GNOME for example), which doesn't completely strike me as being true. Then again, that is the only quote whuch was not yours. :-)"
    author: "Rob Kaper"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-13
    body: "Yeah, they did seem to imply this, didn't they?  But, at the risk of flamebaiting, GNOME C isn't the simple C everybody implies it is. When compared to KDE C++, GNOME C is cluttered and complex, IMHO, especially when you consider all those casts and attempts at OO syntax."
    author: "Navindra Umanee"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-13
    body: "It is no attempt at OO syntax. It _is_ OO syntax, just that it reads a whole lot different in ANSI C"
    author: "dmalloc"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-13
    body: "And also happens to be really ugly compared to nice clean QT C++.\n\nI've TRIED learning GTK, believe me - I just can't cope with such ugliness. Or maybe it's the fact that the docs are absolutely horrible?\n\nOh well. I'm happy writing code for KDE/QT... and just to prove I'm not trolling, you can see my software at http://www.mcs.kent.edu/~dwatson/."
    author: "David G. Watson"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-14
    body: "I tried learning GTK+... and I succeed in 1 week.\nIt was also the first time I learned using C.\n\nSo the code isn't ugly, it just depends on what you're used to.\nThe GTK+ docs are good enough for me.\n\nAnd \"nice clean C++\" looks really ugly compared to Object Pascal."
    author: "dc"
  - subject: "Nice Clean C++ uuhh not clean compared to ObjC"
    date: 2001-07-14
    body: "Does anyone uhh know that uhh ObjC uuhh exists?\n\nEven with OpenStep class framework and extensions it's clean and simple (like C plus about 5 other things to remember) and pretty darned well xplatform (unix (linux/bsd), nt, os/x).\n\nA team of 6 people working part time without even a finished/working display server have produced a free version of possibly the BEST libraries for software development *ever*. Qt has better documentation - that's about it.\n\nWith a measly portion of the 13million Eazel burned on their filemanager the display component (ghostscript enabled \"vectorized\" display postscript extensions to XFree) would have been done long ago. Sigh ....\n\nLook how easy it is:\n\nhttp://www.ens-lyon.fr/~pyrivail/myfirstapp/myfirstapp.html"
    author: "Slow User"
  - subject: "Re: Nice Clean C++ uuhh not clean compared to ObjC"
    date: 2001-07-14
    body: "> Does anyone uhh know that uhh ObjC uuhh exists?\n\nDo you know uhh that it's uuh dead ?\n\nSearch for books about Objective C on Amazon. Look at how many posts there is daily on comp.lang.objective-c. Objective C bindings for GTK+ were declared dead a couple of months ago by their maintainer (Eliott Lee I think) because of lack of interest. Show me a mainstream, widely used application written in Objective C (like a word processor, an email client, a browser or a file manager).\n\nObjective C is certainly a very nice language. However it has virtually no existance in the current landscape.\n\nIts sole purpose right now is to be brought up in C vs. C++ discussions by people who think they've discovered the Grail and like to show off saying \"you're all wrong, I know what the best language is\"."
    author: "Guillaume Laurent"
  - subject: "So how does C# compare with C++"
    date: 2001-07-15
    body: "Since we are discussing various flavours of C, how does C# compare?"
    author: "SL"
  - subject: "Re: So how does C# compare with C++"
    date: 2001-07-15
    body: "I read a C# tutorial some days ago. Microsoft claim that C# is almost like C++, but in facts, it's a Java ripoff."
    author: "PE"
  - subject: "Re: So how does C# compare with C++"
    date: 2001-07-16
    body: "> Microsoft claim that C# is almost like C++, but in facts, it's a Java ripoff.\n\nHeh, that's not terribly striking, as Java is a C++ rip off :)\n\nHarold"
    author: "Harold Hunt"
  - subject: "Re: So how does C# compare with C++"
    date: 2001-07-16
    body: "The difference is that Sun makes no secret of Java's inheritence from C++ but Microsoft is not so keen to admit that C# is actually a merge of Java and Delphi (non Microsoft technology...hmmm does anybody have a single example of technology innovated by Microsoft except Visual Basic??)"
    author: "Roger"
  - subject: "Re: So how does C# compare with C++"
    date: 2001-07-17
    body: "From <a href=\"http://msdn.microsoft.com/vstudio/nextgen/technology/csharpintro.asp\">Visual Studio .NET: C# Introduction</a>,\n\n\"More than anything else, C# is designed to bring rapid development to the C++ programmer without sacrificing the power and control that have been a hallmark of C and C++. Because of this heritage, C# has a high degree of fidelity with C and C++. Developers familiar with these languages can quickly become productive in C#.\"\n\nI've never heard that C# inherits from Delphi (Pascal), but Microsoft doesn't seem to be trying to keep a secret that C# inherits from C and C++.  Rather, they are bragging about that heritage.\n\nHarold"
    author: "Harold Hunt"
  - subject: "Re: So how does C# compare with C++"
    date: 2002-01-13
    body: "Harold,\n\nYou are the biggest stooge that I have ever had the displeasure of hearing.  Now crawl back into your MS hole and stay there."
    author: "Mike Myers"
  - subject: "Re: So how does C# compare with C++"
    date: 2003-05-01
    body: "you can crawl in your ms hole, but the \"ms hole\" happens to command about 80% of the software industry... give up the anti-gates fight guys! Like it or not, he wins, because he's done it right. Period. Quit bitching."
    author: "A realist"
  - subject: "Re: So how does C# compare with C++"
    date: 2003-02-21
    body: "What does the name C# mean?"
    author: "Mitchell Geere"
  - subject: "Re: So how does C# compare with C++"
    date: 2005-12-14
    body: "I still don't know why they decided to call it C#... I know that it has something to do with the musical key C# but that is as much as I know... :P"
    author: "Mitchell Geere"
  - subject: "Re: So how does C# compare with C++"
    date: 2008-05-07
    body: "My understanding is that it is supposed to allude to the next logical step C, C++, C#.  C++ was the incrementation of C.  C# would be (musically) C++ raised to the next half step."
    author: "Brandon Robinson"
  - subject: "Re: So how does C# compare with C++"
    date: 2002-04-26
    body: "Maybe because the father of Delphi, Anders Heijlsberg, is also father of C#?"
    author: "Jens"
  - subject: "Re: So how does C# compare with C++"
    date: 2003-05-17
    body: "i dont know much about it...but yes i have read many ppl writing on C#....N the most confusing thing is that some ppl say that  the same program they ran on   C# n C++ proved that C# took  longer time to compile and run but then there is another group of ppl who says that it takes the same lenght of time and its program is shorter than that of C++. what i want to know is \"where is lies the truth\".i hope u ppl understood my question..\n"
    author: "rabi"
  - subject: "Objective C is Alive"
    date: 2002-06-03
    body: "It's called Cocoa, and it's in use by a company based in Cupertino, called Apple Computer, Inc. Strangely enough, this little known company has the second highest volume OS, under Windows, in the world. The most recent version, Mac OS X, makes extensive use of Objective C in many of its mainstream programs."
    author: "Bob"
  - subject: "Re: Nice Clean C++ uuhh not clean compared to ObjC"
    date: 2003-01-08
    body: "> Show me any mainstream applications written with Objective-C.\n\nOkay,\n\nMail, Address Book, iTunes, Safari, iCal, OS X Finder, Dock, blah, blah, blah\n\nShall I go on?  The entire Mac OS X world using Objective-C.\n\n:)"
    author: "Ted Wood"
  - subject: "Re: Nice Clean C++ uuhh not clean compared to ObjC"
    date: 2003-01-08
    body: "I can't believe anyone would still post on this after more than half a year.\n\nOK, so Mac OS X uses ObjC. Good for them. Now, outside of OS/X, is anyone using Objective C ? Is anyone working on improving the language, or standardizing it ? Are there books written about it ? Compilers, debugger, and IDEs written for it ? And, given the current usage statistics, can you really call OS/X \"mainstream\" ?\n\nGive it a rest, please.\n"
    author: "Guillaume Laurent"
  - subject: "Re: Nice Clean C++ uuhh not clean compared to ObjC"
    date: 2003-01-08
    body: "Maybe you should do your research. Just to illustrate my point, yesterday, a brand new web browser application called Safari, was downloaded 300,000 times in one day. Guess nobody uses OS X."
    author: "Ted Wood"
  - subject: "Re: Nice Clean C++ uuhh not clean compared to ObjC"
    date: 2003-01-09
    body: "Oh gee, 300,000 times, really ? Gosh that's a lot. You must be right.\n\nNow since you've done your research, can you tell me how many more Linux users are there ? And how many more Windows users ?\n\nWhile we're at it, how often do you write code in Objective C ? How many ObjC developpers do you know ? How often do you see job ads requiring knowledge of ObjC ? Which book is to ObjC what K&R is to C and C++3rdEd. to C++ ?\n\n\n"
    author: "Guillaume Laurent"
  - subject: "Re: Nice Clean C++ uuhh not clean compared to ObjC"
    date: 2003-01-09
    body: "Responding to these silly debates isn't normally my style, but this is too easy.  300,000 times in one day is alot, I think. I don't know any ObjC developers personally, aside from myself, but I'm just a student right now teaching it to myself. I never see job ads requiring knowledge of ObjC. What is K&R?\n\nGee, I guess you're right. ObjC is dead. Nobody is using it. There are no ObjC developers. Nobody is hiring ObjC developers (probably because there's no market for OS X applications). There are no good reference books for ObjC. Guess Apple went out of business years ago, too. Guess the Mac platform sucks. KDE rules.\n\nThank you for enlightening me. I'm going to drive my truck over my Mac OS X laptop now."
    author: "Ted Wood"
  - subject: "Re: Nice Clean C++ uuhh not clean compared to ObjC"
    date: 2003-01-09
    body: "Oh grow up will you ? First you start posting to a thread that's several months old, then whine because you realize that MacOS/X is hardly more than a rounding error in the current OS usage statistics, and so is ObjC ?\n\nYou're acting like a child who can't stand his new toy to be criticised.\n\nMacOS/X will probably achieve mainstream status, but even if it does, chances are that ObjC will still not be very much more common. Why do you think Steve Jobs was touting it as \"the best platform for Java development\" ? ObjC isn't the only programming language available for MacOS/X.\n\nAnd no, that doesn't mean the Mac platform sucks, I never said that. Quite the contrary, Mac OS/X achieves what even KDE still hasn't. I've spend about 10mn playing with it at an Apple dealer shop, and that was more than enough to convince me. If the hardware wasn't so expensive, I'd have purchased one long ago. As for Objective C itself, I also learned it back in 1998 (we were considering using it for Rosegarden, see http://www.all-day-breakfast.com/rosegarden/history.html ), and it's an excellent language, though in dire lack of standardization (not having a fixed Object base class is a huge mistake). At that time I even added Objective C support to XEmacs' func-menu (grep my name in func-menu.el). And finally, I even had the chance to briefly talk with Simson Garfinkel (http://www.oreilly.com/catalog/buildcocoa/) who told me that Cocoa was \"the best platform for desktop application development on the planet\", which didn't really came as a surprise.\n\nSo MacOS/X and ObjC are both kick-ass platforms, but, at the moment, in the current industry landscape they represent a very very tiny part of the market. That's quite regrettable, may be that will change, but in the meanwhile, deal with it.\n\n\n"
    author: "Guillaume Laurent"
  - subject: "Obj-C++"
    date: 2003-02-19
    body: "Obj-C is great and works, and that should be all that matters.\nBesides, if you are worried about compatability of old C++ code with the use of new Obj-C apps, well, don't worry.  Apple also uses Objective-C++.  Both syntaxes work in the same file.  All you need to do is change the Obj-C source file's extension from \".m\" to \".mm\".\nI like Obj-C++ better since I can still use \"new\", \"delete\", \"cout\", and etc but still use Obj-C objects.\n\nPS:  On Mac OSX, Obj-C's root object is not \"Object\", it is \"NSObject\".  In fact, all of the Cocoa frameworks standard objects have \"NS\" at the front of the identifier.  Guess what the \"NS\" stands for..."
    author: "mike"
  - subject: "Re: Obj-C++"
    date: 2003-02-19
    body: "> Obj-C is great and works,\n\nLike dozens of other languages.\n\n> and that should be all that matters.\n\nNot in this world, sorry.\n\n> I like Obj-C++ better since I can still use \"new\", \"delete\", \"cout\", and etc but still use Obj-C objects.\n \nNow that's a strong and technically sound argument.\n\n> On Mac OSX, Obj-C's root object is not \"Object\", it is \"NSObject\".\n\nI know, and guess which one you have under gcc ? As I said, not having a standard Object class (I'm talking about the concept of a root Object class here, its name is irrelevant) is probably one of the biggest design flaw of ObjC.\n\n> Guess what the \"NS\" stands for...\n\nOh gee, I really wonder.\n"
    author: "Guillaume Laurent"
  - subject: "This is a long one...sorry."
    date: 2003-02-21
    body: "http://dot.kde.org/994996315/994997539/994998200/995008796/995051810/995062640/995085263/995127393/\n(better go ahead and go to the bathroom first, cause this is long.  Sorry)\n>>\"Okay, Mail, Address Book, iTunes, Safari, iCal, OS X Finder, Dock, blah, blah, blah.  Shall I go on?  The entire Mac OS X world using Objective-C.\"-Ted Wood\nIn response to Ted Wood's claim that iTunes was written in Obj-C, I'm pretty sure you're mistaken.  iTunes is actually written using the Carbon frameworks, specifically the Mach-O kind, not the CFM type.  Don't make the false assumption that it is Cocoa just cause an app on OSX is an app package and you have to option to right click and see a \"show package contents\".  Apps that run in OS9 & OSX are Carbon CFM, OSX only apps are either Carbon Mach-O or Cocoa (both of which are packages).\nNow you're probably wondering why Apple didn't use Cocoa, well, iTunes was out on OS9 a long time ago and was undoubtedly written in C++.  Is it really worth the time to redo all that code?  Thats an example of why Cabon was made in the first place.\nRegarding Safari, uh, I'm willing to bet it isn't C++ either.  Reason is because Apple alone isn't responsible for Safari.  Safari is based on an open source Linux browser (Konquorer I think).  True, Apple did lots remodeling, and not just the interface, I mean they optimized lots of code it so it is faster than its Linux counterpart, but they probably left it in C++ and made it a Carbon Mach-O app.\n\n>>\"Nobody is hiring ObjC developers (probably because there's no market for OS X applications).\"-Ted Wood\n>>\"How often do you see job ads requiring knowledge of ObjC ?\"-Guillaume Laurent\n[hmm, should I really try debating 2 guys at once?...aw, lets go for it!]\nNo one is hiring Obj-C developers because just about anyone with C++ know-how can learn Obj-C in a few days (it is very easy to learn with prior OOP experience).  Also, even if they wanted to write OSX apps and were hardcore C++ guys, then they would just use the Carbon frameworks.\n\nAnother very big reason (probably the main one) you don't see a Obj-C developer search is that before OSX and its developer tools, 90% of Mac apps were written in Metrowerks' CodeWarrior (or so they advertised).  It too can make Carbon CFM apps as well.  I learned C++ on that compiler in school on the PC version and have my own Academic version for my Mac as well.  It is awesome compiler, but it can't use Obj-C so I lean away from it nowadays (well you can check a box saying \"attempt to use Obj-C syntax\" but the word \"attempt\" scares me away from it).\nI got sidetracked...the point is that CodeWarrior is a Mac programmer standard like Photoshop, and some people would like to stay cozy in what they are familiar with.  I can almost sure that 90 percent of OSX shareware is Cocoa which must use Obj-C or Obj-C++ because they don't have to worry about using massive amounts of old C++ code.  If you need to use massive amounts of old C++ code use Carbon, cause that's why it was made (the traditional C++ programmer would choose that over Obj-C++ just for familiarity).\n\n>>\"MacOS/X is hardly more than a rounding error in the current OS usage statistics\"-Guillaume Laurent\nYes, we are a small community (cult?)  You don't need to be the standard to put out good stuff, and trust me, to get your work done you don't need standards like Windows.  I sure don't (hell, in highschool I would compile a windows exe on my Mac with CodeWarrior and turn it in on a disc).  All our cult-(oops)-I mean community needs is more game support, but as far as real work needs, we are covered.\n\n>>\"Obj-C is great and works, and that should be all that matters.\"-Me\n>>\"Not in this world, sorry.\"-Guillaume Laurent\nOk, if you're a professional programmer, yea, my comment was rash.  If you're a cross-platform programmer, yea, my comment was heroically stupid, and best corrected by this joke http://conversatron.com/archive/83769.html\n\nIf you are a student like Ted Wood claims to be and I am, well, we're young, can only afford one kind of computer & OS, and perfect candidates for Obj-C (++).  Also, I know Obj-C and Cocoa are currently my only hopes of easily producing GUI apps.\n\nI also strongly feel that a root object class is silly (yes, you can make your own root class, but the work involved really isn't worth it).  I think it makes all other Obj-C objects inherit the memory allocation methods or some other low-level issue that couldn't possibly need customization.  In case you don't know, Obj-C and Obj-C++ only work in Cocoa and not Carbon, and Cocoa is sweet (pun intended?).  Ok, I think I [pant] got em [wheez] all...\nI now await any helpful corrections (probably from Guillaume).\n(wow, that was long, sorry!)"
    author: "Mike"
  - subject: "TYPO"
    date: 2003-02-21
    body: "When I said \"Regarding Safari, uh, I'm willing to bet it isn't C++ either.\"\nIt should read \"Regarding Safari, uh, I'm willing to bet it isn't Obj-C either.\"\nBrain fart."
    author: "mike"
  - subject: "Re: This is a long one...sorry."
    date: 2003-02-21
    body: "(For the record, I am a professional programmer, have been since 1995.)\n\nAbout the root class : I'm not sure what you mean. If it is that the ability to define your own root class is silly, then I agree, ObjC should have a fully specified root class.\n\nIf you mean that the very concept of a standard root class is silly, then I totally disagree. Having a full-featured root class helps tremendously (although it doesn't replace templates as is often naively thought). There's a good reason why Java and C# have one as well.\n\nThe drawback is that objects become expensive, as opposed to C++ where the overhead of the object model is very small or even non-existant depending on how you define your class.\n\nJava has this problem, C# \"fixed\" it by having two object models (classes and structs) at the expense of complexifying the language... There's no winning solution."
    author: "Guillaume Laurent"
  - subject: "Re: This is a long one...sorry."
    date: 2004-03-19
    body: "Gilgamesh, quit being such a fag."
    author: "Anaun!"
  - subject: "Re: Obj-C++"
    date: 2008-02-28
    body: "I'm trying to port an old Objective C code that I had written some time ago on a NeXT machine to my Mac OS X in XCode.\n\nI have the following statements I need to replace:\n\ntypedStream = NXOpenTypedStreamForFile(desc,NX_WRITEONLY);\n\nI need the replacements for NXOpenTypeStreamForFile and NX_WRITEONLY\n\nIs there any documentation anywhere about porting NextStep codes to Carbon?\n\n(BTW, our company CEO is one of the original authors of Objective C, Tom Love.  \nIt was written by him and Brad Cox and then they formed Stepstone, which sold\nthe first OO software library)."
    author: "John Wooten, Ph.D."
  - subject: "Re: Nice Clean C++ uuhh not clean compared to ObjC"
    date: 2003-08-29
    body: "Hi Guy's,\n\nI know this thread is fairly old now but I felt I had to give it my two cents (or pence if you are from the UK like me).\n\nI am a PHP web app developer and have toyed around with different languages for the last 23 years and have just started to learn C (ANSI C) for the second time. I have settled on the Mac (OS X) platform after using many other OS's over the years because I like it the best and want to start developing OS X only apps. I really couldn't care less if my apps are portable to other OS's except maybe Linux if I can be bothered so I guess the guy who said that \"ObjC with the Cocoa API is all you really need\" in my case is correct.\n\nI have read so many forums, tutorials and articles on the Net that say Objective-C is a much friendlier Language than C++ and I just found out that it was developed back in 1983, the same year that C++ was born so you guy's who slate it compared to C++ can keep your opinions. Also the fact that there is a far smaller demand for ObjC programmers is superficial. If you are intelligent enough to program a computer with an OO compiled language then why work for someone else to give them profit. Go self employed. Companies who contract out a programmer to build them an in-house application don't care less what you use as long as it works and if you build a killer App to sell en mass then again use what works best for you. "
    author: "Paul Randall"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-13
    body: "KDE/Qt is hard ? Compared to what ? Motif/MFC/GNOME /BorlandC++ patents ?  In fact Qt is the easiest and most effective - GNOME/Motif/MFC are really hard ! I have always thought that it is the one of the main KDE/Qt advantages.  There is KDevelop/QtDesigner, so all C/C++ beginners should rather start using Qt than any of the above."
    author: "Ups"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-13
    body: "GNOME patents????\n\nA while ago, that Motif guy claims that Motif is superior to GTK+ and QT...\n\nBefore I started programming for Linux, I used Delphi (a great IDE, the GUI designer part is way better than QT Designer or Glade).\nWhen I first started programming for Linux, I programmed for GTK+.\nI used the book \"Linux development using GDK and GTK+\".\nIt was very easy, and I learned GTK+ quickly.\nAfter a while I discovered that using gnome-libs is a lot easier than just plain GTK+.\nSo I switched to GNOME, which is not hard at all.\n\nGNOME programming isn't so difficult as many C++'ers claims it is."
    author: "dc"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-14
    body: "> GNOME programming isn't so difficult as many C++'ers claims it is.\n\nWe don't claim it's hard, we claim that our way is easier.\n\nNow we've been through this argument already, haven't we ? :-)."
    author: "Guillaume Laurent"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-14
    body: "Yet again.\n\nBTW, how does KDE/QT compare to M$VC++/MFC?"
    author: "dc"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-14
    body: "I've done the equivalent in MFC of about the first 3 Qt tutorials.  In other words, not much.\n\nWhat I can tell you though, is that picking up Qt was much easier.  MFC didn't make a whole lot of sense, and I think part of the problem is the way it is tought, and how VC++ babies you.  VC++ generates some code (like stdafx, etc) so it is difficult to tell exactly what is going on.  I'm sure WinMain() is in there someplace.\n\nBut Qt?  I often hack Qt in vi.  The first Qt tutorial is a hello world example with a label.  The program is around 10 lines, uses function main() (what the heck is WinMain anyway?), and every line makes total and absolute sense.  I'm sure someone can learn the ins and outs of MFC to completely understand it, but Qt makes more sense from the get-go.\n\nTrolltech's page has a listing of what their customers have said.  Notably, a few comments say how much better Qt is to MFC.  Granted, it's Trolltech's page, but still.  I've never seen anyone say a reverse argument.  In fact, the MFC programmers I do know always complain about it (kinda like Windows users who complain about Windows, yet still use it).  Heck, the only bad things I've heard about Qt come from people who hate C++, not from people who have actually used the library.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2003-06-29
    body: "hello there,\ni'm a student of comp. engineering in bhilai,india.\ni'm doing a project involving image processing of .ppm\nfiles in linux.i've created the code in \"c\" language.\nnow i need some interfacing n ppl tell me QT is just\nlike VB.can anyone teach me/give some useful links 2 know\nthe drag n drop functions n a li'l detail on QT designer.\ni just dunno anything abt QT etc\ni've downloaded some docs from trolltech,kde site,etc.\ni need 2 submit the project by mid-july.\nis there any chance?\nplz help!!\n-aparna"
    author: "aparna"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2003-06-29
    body: "If you are coding in C and not in C++ it might be more wise to use GTK (http://gtk.org/) for your interface since it's C based and thus might be easier to understand for you. QT is basically a slightly extended C++, it's really not comparable with VB at all."
    author: "Datschge"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2003-06-30
    body: "hi there,\nthnx 4 the quick response.\ni'll go thru the gtk site.\ncan u plz tell me what exactly do i need-qt or gtk.\nhere's what i wish to do:\ni've created a few progs in \"c\" for diff. operations\nlike blur,sharpen,filter,etc(image processin) in .ppm files.\nnow i need an interface so that my project looks good!\nsomething like visual basic,but i want it on linux.\nsorry 2 bother u,actually i've started using linux just \na few days back!!\n-aparna"
    author: "aparna"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-13
    body: "Nice articel, but: how to submit a patch? How can I create a patch for something?"
    author: "Norbert"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-13
    body: "> How can I create a patch for something?\n\nIn general: use the diff program. Most projects prefer the -u option because it increases readibility, but I would ask per project.\n\nKDE specific: the best way would be to get KDE from CVS; if you have no account use AnonCVS (http://www.kde.org/anoncvs.html). That way you can create a patch with 'cvs diff' which will be a patch against the current tree and will be the most easy way for developers to use."
    author: "Rob Kaper"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-13
    body: "Seems like good questions for Philippe's FAQ."
    author: "ac"
  - subject: "Micro-HOWTO: How to submit a patch"
    date: 2001-07-13
    body: "Intending to do something about an 'optimizable functional inadequaty' or willing to add new functionality to a KDE project you might want to act like this:\n\n\n0.\nMake sure the bug is not being worked on at the moment by looking at the projects bug list:\n\n   http://bugs.kde.org/db/ix/packages.html\n\nAdditionally you might want to subscribe to the respective mailing-list to see if there is discussion about the bug (or the desired feature, resp.).  The list of all mailing-lists and instructions for how to subscribe is here:\n\n   http://www.kde.org/mailinglists.html\n\n(Before contacting  kde-devel  please have a look at the list of 'Dedicated lists' to see if there is an own list for the project that you are interested in.)\n\n\n1.\nGet the newest sources from CVS and compile them to see if the issue has not been solved by somebody else in the meanwhile.\n\nLet us assume that you have the KDE code in the following directory:\n\n   ~/KDE_SOURCE\n\n\n2.\nFind out in which directory you are going to make your modifications.\n\n\n3.\nCopy the entire content of that directory into a freshly created save-directory that you will *not* touch later.\nE.g. if planning to dig into the KMail sources you could save the code in a directory\n\n   ~/KDE_SOURCE/kdenetwork/kmail_ORIG\n\n\n4.\nHack the code to get the new functionallity into it (or for fixing the bug, resp.).\n\n\n5.\nTest everything carefully, think about side-effects . . .\n\n\n6.\nUsing the diff program now create a so-called 'patch' file containing the differences between the original code and your industriously hacked version, e.g. like this:\n\n   cd ~/KDE_SOURCE/kdenetwork\n   diff -u kmail_ORIG kmail > mynicepatch\n\n\n7.\nSend this patch file as mail attachment to the respective project maintainer.\nIf you are not sure about whom to send the patch to you might find the name in the program sources or you might want to subscribe the projects mailing list to obtain further information from there.\n   \n\n8.\nIn the unlikely(!) case that you receive no reply to your mail just contact the kde-devel mailing-list (or a dedicated mailing-list, as mentioned above in (0.), resp.) to find out whom to send the patch to.\n\n\nYour help is very much appreciated by the KDE team and sending a well-tested patch is one of the best ways how to contribute to KDE."
    author: "Karl-Heinz Zimmer"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-13
    body: "what good online books/documentation do you recommend?"
    author: "Carg"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-13
    body: "Does everyone here actually like coffee?"
    author: "Jason Ormes"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-14
    body: "I don't. Tea r00ls! :)"
    author: "dc"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-14
    body: "Ditto here.  I've happily discovered that Lipton Cold Brew tea is not bad (yes, I'm here in the Merkin South, and we ice our tea... deal with it).  Before that, I used to brew a couple of gallons at a time because it was so annoying to have to wait.\n\nNow, you might ask: \"Hey, Evan... you're usually on topic, and have something to say about KDE.  Why the post about tea?\"  Well, the point of this post is to request that the dot have some \"humanistic\" polls.  Right now, we're still a community.  In three to five years, if things go well, the average KDE user will punch in at 9am, type memos in KWord, fill out their employee's timesheet info in KSpread to forward to HR, and go home at 5pm. (YMMV for other cultures).  They won't know or care what version KDE they are using, never heard of aRts and think XML is the newest model BMW that just came out.  Right now, we're still a closed community - most people know what version of KDE they are using, unlike the current average Windows user - and it's interesting to know - who drinks coffee versus tea versus soda.\n\nWhat is your favorite color, what do you use as a desktop wallpaper (portrait, pattern, landscape, etc.) would also have a useful aspect for the theme and style developers.  I'm curious.  \n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-14
    body: "Evan where do you live in the south? I'm in West Virginia.Which is almost south.\n\nCraig"
    author: "craig"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-14
    body: "> Evan where do you live in the south? \n\n   As far south as you can get - South Florida (West Palm Beach, technically).  If anybody else is in the area, I'd have no problem joining/starting a local KDE users group - in fact, is there a registry for such groups anywhere in the *.kde.org hierarchy?  If not, I'm offering to create one, as long as I can get someone from Europe to volunteer to help with that region.  I can whip out a database driven site for it next week, I would imagine.  Any interest?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-14
    body: "To make that clear, I could use advice from Africans, South Americans and Asia other than the Pacific rim.  I have a good friend that I can ask about Austrailia.  If you've got a KDE group, want to offer input or know of something that already exists, email me at kdegroups@timewarp.org.  \n\n    Language translation and sensible regional divisions are what I'm looking for - I can handle the coding with no problem, and while I'm no artist, I can fake it.\n\n    And just in case it's not obvious, I've already started the project.  I'm just gonna be upset if I do this, and I can't find any users local to me.  :)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-14
    body: "Great idea is there anyone in west virgina? I know its unlikely but i can dream.\n\nCraig"
    author: "craig"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-16
    body: "Jesus in a rowboat! Has everyone disabled the KDE startup tips????\n\nIf you didn\u00b4t you would know that after informal research, KDE developers prefer tea!\n\nPersonaly, I prefer mate (with no sugar, I\u00b4m a traditionalist), and a few other herbal infusions, but even plain tea beats coffee ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Newsforge: Developing for the Linux desktop"
    date: 2001-07-16
    body: "Earl(y) Grey reminds me of going to bed before the break of dawn..."
    author: "MagMic"
---
One of the most frequently asked questions on the KDE developer lists is, "How can I start contributing to KDE?" --  <A HREF="http://www.newsforge.com">NewsForge.com</A> may just have the answer.  In an <A HREF="http://www.newsforge.com/article.pl?sid=01/07/11/185221">article</A> entitled <EM>Joining the Round Table: How to get started developing for the Linux desktop</EM>, <A HREF="mailto:tina@newsforge.com">Tina Gasperson</A> gives some helpful tips and links on how to get your feet wet with KDE or other Open Source coding.