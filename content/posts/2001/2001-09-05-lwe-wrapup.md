---
title: "LWE Wrapup"
date:    2001-09-05
authors:
  - "Dre"
slug:    lwe-wrapup
comments:
  - subject: "Should be faster now.."
    date: 2001-09-05
    body: "If you happened to be among the few early birds who landed on a slow server for the picture page, my apologies. Apparently convert did not compress the images, making the thumbnails 25k each instead of the 5k they are now.\n\nAlso, most links should now point to my jadzia server which (while being a mere P100) has a much better uplink (1Mbit) than ezri."
    author: "Rob Kaper"
  - subject: "Re: Should be faster now.."
    date: 2001-09-05
    body: "If those are Star Trek DS9 references, I must say I am confused.\n\nI always thought jadzia had a much nicer hardware than ezri, even if somewhat older.\n\nYou must immediately move jadzia to something like a ultra10, or a Alpha 233."
    author: "Roberto Alsina"
  - subject: "Re: Should be faster now.."
    date: 2001-09-05
    body: "No, it makes sense: jadzia is a machine from 1995. Other machines in my network include kes (old laptop), 7of9 (my old workstation, sold however), ezri (my new server, replacing jadzia, thus completely in style) and kira (current workstation)."
    author: "Rob Kaper"
  - subject: "looked fun"
    date: 2001-09-05
    body: "Hey, good job with the report, the conference looked very good for KDE.  \n\nOut of curiosity, which make/model of camera did you use to take those photos?"
    author: "Navindra Umanee"
  - subject: "Re: looked fun"
    date: 2001-09-05
    body: "Crappy Kodak DC215 Zoom. Pictures of items nearby are usually pretty good, however it's a bit blurry often for items in the background and when there's no full light. And it eats batteries. But for only $200 a year ago, it's a pretty decent camera.\n\nHave my heart set on one of the new Fuji models. Those are purty. Especially the 6900, now that's a camera!"
    author: "Rob Kaper"
  - subject: "Re: looked fun"
    date: 2001-09-05
    body: "Got my hands on a DC3400 (basically an updated DC280) some time ago, although it was recently made obsolete by the DX3600.  I like it very much actually -- it takes very good pictures, beautiful colour, strong flash, etc.  Once I was even lucky (?) enough to get significantly better pictures than a rival Canon A20 owner under tough indoor/low-light conditions.  ;) \n\nAnd of course, most Kodaks are supported fine under Linux thanks to Gphoto.  Overall, I've gotten a very positive experience from the dc3400 and tend to view Kodaks favorably these days."
    author: "Navindra Umanee"
  - subject: "Re: looked fun"
    date: 2001-09-05
    body: "Speaking of Linux Camera Support, my new Olympus D-510Z (aside from excellent picture quality) has a feature called \"USB-Autoconnect\", which is a fancy way to say that the camera behaves as a USB mass storage device. Under Mandrake 8.0 all I had to do is plug the USB cable, and Linux automatically loaded the proper modules and recognized it as a scsi device (/dev/sda1), then I setup a mount point and added it to fstab. Now I can access the camera from Konqui, load/store files, view/generate thumbnails, without the need of the gphoto ioslave...sweet!\nOnly one complain: Konqui (or Linux itself?...it'll be easy to find who's guilty ;)) is constantly polling the camera (as the transfer LED blinks all the time, meaning access to the smartmedia card), which in the long run drains more power."
    author: "Carlos Miguel"
  - subject: "Re: looked fun"
    date: 2001-09-05
    body: "Colour me jealous, although I don't really like those smartmedia cards.  I've heard only good things about the Olympus models, but those smartmedia cards  do need some maintaining with those exposed pins and all..."
    author: "Navindra Umanee"
  - subject: "Re: looked fun"
    date: 2001-09-06
    body: "Many newer cameras can be used as mass-storage devices, including the mentioned Fuji Finepix."
    author: "Anonymous"
  - subject: "What are you drinking, Rob? :)"
    date: 2001-09-05
    body: "Look at http://jadzia.nl.capsi.com/~cap/digicam/2001-09-01-lwce/sanfrancisco-116.jpg\n\nWhat's in the glass?"
    author: "Loranga"
  - subject: "Re: What are you drinking, Rob? :)"
    date: 2001-09-05
    body: "> What's in the glass?\n\nAs crappy as it is compared to genuine Belgium flavours, the USA does sell some sort of liquid substance under the name \"beer\"."
    author: "Rob Kaper"
  - subject: "Andreas' semi-violent side"
    date: 2001-09-05
    body: "Protect himself with lozenges?!? Was he throwing them at the PR types?\n\n-pos"
    author: "pos"
  - subject: "Re: Andreas' semi-violent side"
    date: 2001-09-05
    body: "*ROTFLOL*"
    author: "ac"
  - subject: "what"
    date: 2001-09-05
    body: "about Noatun ? why it is not included into KDE 2.2 ? And what about Quanta ? i thought that it will be included into KDE 2.2 or 3. I hope."
    author: "Daniel"
  - subject: "Re: what"
    date: 2001-09-05
    body: "Noatun came with my KDE-2.2. Perhaps you bought a defective one. Take it back to your retailer and get a return :-)\n\nQuanta is nice, but not included. It would be nice if every good KDE application were included in KDE, but then the download times would reach another order of magnitude. Some things need to be left out of the base distribution, as it's getting too big as it is.\n\nOutside of certain metropolitan areas, most people are still on 56K modems. Let's not frustrate them with a 56Meg packages."
    author: "David Johnson"
  - subject: "Re: what"
    date: 2001-09-06
    body: "i think Quanta is QT only no? it's a kde app.."
    author: "emmanuel"
  - subject: "kio_audiocd?"
    date: 2001-09-05
    body: "how do I set konq up to do this? where do I find this plug-in? \nI am so happy about this feature, this has been lacking I think for a long time, I just wanted the ability to browes my music CDs, but KDE has taken it a step further, and allowed autoriping/encoding.....I love that :)"
    author: "Jeremy Petzold"
  - subject: "Re: kio_audiocd?"
    date: 2001-09-05
    body: "kio_audiocd is part of kdebase for 2.2 if not already since 2.1. Support for encoding to mp3 and ogg depends on whether the LAME and Vorbis libraries were found during compilation, I'm not sure how the various distributions handle this.\n\nJust enter audiocd:/ into Konq's Location bar and off you go. Drag and drop files from the various subdirectories to another folder or your desktop to start encoding! The audiocd:/ link should also be available under Services in the Konqueror sidebar."
    author: "Rob Kaper"
  - subject: "Re: kio_audiocd?"
    date: 2001-09-06
    body: "when I click on the audiocd browser in the side bar or type audiocd:/ in the location bar, I get \"could not read /\" what is going on?\n\nthanks, \n\nJeremy"
    author: "Jeremy Petzold"
  - subject: "Re: kio_audiocd?"
    date: 2001-09-06
    body: "This is most likely because you don't have the right permissions to access\n/dev/cdrom (or whatever your drive is called). You may also have to change\npermissions to /dev/sg0."
    author: "Meretrix"
  - subject: "So get some CD's!"
    date: 2001-09-06
    body: "First of all, I've expressed some skepticism in the past of whether the KDE League is doing anything useful. I'm happy to see that I was wrong and it sounds like they contributed a lot here, with financing and the PR. One suggestion, though:\n\n>>>Some other frequently heard requests were for CD's with KDE installed...it should also be mentioned that most Linux distributions ship with KDE, many of which have it as the preferred or default desktop environment.<<<\n\nIf people are asking for CDs, next time make sure to give them CDs!"
    author: "Otter"
  - subject: "Re: So get some CD's!"
    date: 2001-09-06
    body: "Actually, there was a discussion about this at one point on the promo list....   It's not really feasible...   You can only fit 1 or 2 Distros per CD(I don't know the size of the current tree off-hand...)   So it would be rather expensive......."
    author: "Nicholas Robbins"
  - subject: "A \"standard\" installation of KDE ?"
    date: 2001-09-06
    body: "> You can only fit 1 or 2 Distros per CD(I don't know the size of the current tree off-hand...)\n\nIt is a bad thing that each distro installs KDE in a non standard way. Because this standard does not exist...\nIt would be good that the KDE team deliver some install recommandations so that KDE would be installed in a \"standard\" way... No ?\n\nAn other davantage would be that a KDE user would change its distro and keep its KDE config..."
    author: "Alain"
  - subject: "Re: A \"standard\" installation of KDE ?"
    date: 2001-09-06
    body: "The main problem is not the standard installation, but the different versions of libraries and compilers used be the different distros.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: So get some CD's!"
    date: 2001-09-06
    body: "Better idea is to put them at the local libraries so they can check them out.\n\n--kent"
    author: "Kent Nguyen"
  - subject: "You should follow Del Caza dan XIMIAN"
    date: 2001-09-06
    body: "You see, this KDE things only well known for people who are involved in Linux, not in popular presses where the opinion making for masses are created.\nAll these \"great\" things about KDE only known among yourself. That's way I said you peoples just keep masturbating.\nLook at Gnome, Ximian or DelCaza, they are very popular in presses and sometimes in some very positive articles and headlines. \nSo...Where is KDE in mainstream press? \nNONE...."
    author: "theman"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-06
    body: "So what?\nDoes the mainstream press make applications?\n\nAnon"
    author: "Anon"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-06
    body: ">So what?\n>Does the mainstream press make applications?\n\nYes,it does.  \nMainstream press leads to company interest, which leads to company involvement, which leads to investments, which leads to more hackers hacking full time, which leads to more apps.  \n\nBoth Linux and KDE are far past the point where they can ignore the rest of the world if they want to grow.  Industry and mainstream support are needed to bring the projects forward, and this can only be done if the industry and mainstream have heard of the project."
    author: "dagw"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-07
    body: "Wow, so much response from the gnome-people. They all read the dot. That's fine :)"
    author: "ac"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-06
    body: "That's \"de Icaza\", not \"Del Caza\"."
    author: "Guillaume Laurent"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-06
    body: "He can't have read any headlines himself.\n\nI think that hiring the services of that PR-firm \nis good enough for KDE promo."
    author: "reihal"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-06
    body: "I stand corrected.De Icaza It is (I am not sure how to pronounce it)."
    author: "theman"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-07
    body: "Something like:\n\nDe Icaza\n\nDe  : as in De-finite\nI   : as in \"E\"\nca  : as in Ka-nt\nza  : as in sa-tin  (more or less)"
    author: "Juan Pablo"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-06
    body: "I totally agree! The Ximian booth was beautiful and well worth the money! I think the participating parties in the KDE League should spend $50k on a booth for KDE next year instead of hiring developers! :-)\n\nNot."
    author: "Rob Kaper"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-06
    body: "Nowhere I suggested that KDE league should stop hiring developers or anything like that. \nBut you guys have to confess De lCaza, Ximian or gnome has won mainstream press attention and appear in the mainstream press FAR a lot more than KDE or KDE developers. \nAnd these KDE peoples keep proud on their OWN website that they are 'the best open source project' or 'the leading linux desktop'. \nReality shows that outside Linux users, De lCaza and his Ximian has been known as the leader/representative Open Source (read: Linux) movement, like it or not. \nTime Magazine, Washington Post etc or even Holywood movie has depicted de lCaza a hero of Open Source Movement. \nWhat about KDE project, Matthias Ettrich or KDE guys? I surely not the only one who notice there is no mainstream press mention them..."
    author: "theman"
  - subject: "Miguel..."
    date: 2001-09-06
    body: "Well if your want good PR, you might want to contact Ximian (hey, there is a business plan...).\n\nIf you want a good desktop, head for the KDE team. I mean: \"Look at Gnome\" With all their \"corporate\" support, they are still nowhere near KDE-2.0.\n\nThe only GTK app I use is the first one: gimp (ok and mozilla and vlc/xine)."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Miguel..."
    date: 2001-09-06
    body: "Huh...Have you tried Ximian gnome?\n\nYou can't possible mean that it's not a good desktop?!\nI mean, although I prefer gnome, I still think KDE is a\nnice desktop environment..\n\nAs for : ...\"still nowhere near KDE-2.0\" -> what in h... do you mean?\nTalking as a user or a developer?\n\nFor my use, I find Ximian gnome (in current state) to be superior\ncompared to KDE (and yes, I've tried the latest version).  \n\nThe Ximian desktop and menues are less bloated compared to KDE, which makes\nXimian more plesant to use. Moreover, I still have not found a KDE mail client\ncomparable to Evolution. Red Carpet, the update tool for ximian is a killerapp,\nand KDE has nothing to offer on that side. I could go on and on..\n\nKDE is very feature rich and is a nice environment to work with, or it would be,\nif I took the time to remove all sort of things that I don't need.(even though KOffice is nice, it is useless for my purposes.  I need to use  StarOffice or OpenOffice for my needs).\n\nI can't see one compelling reason for me to switch to KDE."
    author: "Eivind"
  - subject: "Re: Miguel..."
    date: 2001-09-06
    body: "What are you doing here then? Trolling?"
    author: "reihal"
  - subject: "Re: Miguel..."
    date: 2001-09-06
    body: "If you say the same thing at Gnotices you will get flamed down by trolls, telling you that you guys call people trolls way too fast.\n\nIn *this* case, I agree with them. He was not trying to bash KDE, he was just trying to tell you that GNOME is not as bad as you think.\nWhat's wrong with that?"
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-06
    body: "> You can't possible mean that it's not a good desktop?!\n\nIt is a much better desktop than kde-1.2 right now, but from my (admittedly short) plunges into Gnome-1.4, there is still alot missing. E.g. Xmms is nice, but does not \"fit\" into the desktop one bit. Same for Mozilla.\nI would miss the internet awareness of KDE. The seamlessness. Everything just looks the same, behaves the same and so on. Gnome is pretty, but not as streamlined. And I would miss stable true type font support with AA.\n\nAbiword e.g. is nice, but not very gnome specific. Could you please explain to me why I would ever choose the gnome variant of an app over a gtk variant? What do I get, what do I miss?\n\nFinally Red Carpet: What is this? \"Red Carpet, the update tool for ximian is a killerapp\" It does not work with my distribution, it will trickily remove KDE (from what I have heard), it will modify my stystem. How is updating an \"killer app\" ? Check out SuSE You or Debian aptget. Same thing. Not very desktop specific."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Miguel..."
    date: 2001-09-06
    body: "> Same for Mozilla.\n\nMozilla is not part of GNOME, it's independend.\nIf you want a browser that integrates better than Mozilla, use Galeon.\n\n\n> And I would miss stable true type font support with AA.\n\nActually, there is already a patch for AA for GTK+ 1.2.\nIt's not perfect, but AA and font hinting will be supported completely in GTK+ 2.0.\n\n\n> Abiword e.g. is nice, but not very gnome specific. Could you please explain \n> to me why I would ever choose the gnome variant of an app over a gtk variant? \n> What do I get, what do I miss?\n\nGNOME dialog boxes, the use of gnome-print.\nThe GTK+ version is for people who, for some weird reason, thinks gnome-libs is \"bloated\", or simply doesn't have gnome-libs installed (though I can't imagine why anybody doesn't want to install gnome-libs).\n\nCompare it to Licq. You can choose to use pure QT or add KDE support.\n\n\n> Finally Red Carpet: What is this? \"Red Carpet, the update tool for ximian is \n> a killerapp\" It does not work with my distribution, it will trickily remove \n> KDE (from what I have heard), it will modify my stystem. How is updating \n> an \"killer app\" ? Check out SuSE You or Debian aptget. Same thing. Not very \n> desktop specific\n\n\"trickily remove KDE\", that problem is resolved long ago.\nThis was a bug in the dependency handling in some prerelase of Red Carpet.\nI use Red Carpet regularly and KDE and all it's apps still works fine without any problems.\n\nAnd Red Carpet is not tied to GNOME or even RPM.\nYou can port it to DEB or add a KDE channel (which is not likely to happen, since none of you KDE people trust Ximian for some reason)."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-06
    body: "> > And I would miss stable true type font support with AA.\n>\n> Actually, there is already a patch for AA for GTK+ 1.2.\n> It's not perfect, but AA and font hinting will be supported \n> completely in GTK+ 2.0.\n\nHe said he wanted STABLE true type AA support (now). Why do you reply \nwith something that you admit is not stable right now, but will be \nperfect in FUTURE?\n\n> GNOME dialog boxes, the use of gnome-print.\n> The GTK+ version is for people who, for some weird reason, \n> thinks gnome-libs is \"bloated\", or simply doesn't have gnome-libs \n> installed (though I can't imagine why anybody doesn't want to \n> install gnome-libs).\n\nWhy would I install something I don't use? AFAIK, but I could be wrong, GTK+ \nversion of AbiWord doesn't lack any functionality to other versions.\n\nI've used GNOME a bit at this year's OLS and had the same feeling as \nlast year. Better than used to be, but not there for ME. Neither is KDE, but \nit's definitely much closer."
    author: "Marko Samastur"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "> Why do you reply with something that you admit\n> is not stable right now, but will be perfect in\n> FUTURE?\n\nBecause it can be a good TEMPORARY solution until the perfect implementation is ready!\nOr would you rather wait for GTK+ 2.0?\n\nAnd besides, the AA patch works very well for many people.\n\nIf we use your reasoning, then why should developers release prerelases at all?\nPrereleases are not perfect either.\n\n\n> Why would I install something I don't use? AFAIK,\n> but I could be wrong, GTK+ version of AbiWord doesn't lack any\n> functionality to other versions.\nThen DON'T use it! Nobody force you to download the GNOME version!\nLet the developers do what they want to and stop complaining!\n\nBesides, the GNOME version will get Bonobo support in the future."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "Look, can you tell me which part is so troubling for you to understand?\n\nHe said he wants AA support NOW. KDE has it, GNOME doesn't yet (stable one that is). Judging by your replies, even you agree with that, but for some reason you think you need to justify GNOME.\n\nI have nothing against prereleases and I really can't see how you could misconstruct what I think in that way, but I really don't care. It has nothing to do with what was debated.\n\nAbout second part...you yourself asked why anyone wouldn't want gnome libs and I told you. Because I have no use for it. Abiword (which was application discussed) works the same without it (right now). Someday it probably won't and then I might feel the need to have gnome libs too, but right now there's no such need. \n\nSo where did I complain? You probably mean the last paragraph with that but I haven't said anything developers don't think themselves. If they didn't think that, they would have nothing to do because their work would already be finished.\n\nPlease grow up."
    author: "Marko Samastur"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "> Look, can you tell me which part is so troubling for you to understand?\n\nCan you tell me which part you don't understand?\nI never offered a solution, I merely stated that GTK+ 1.2 has limited AA support now and that it will fully support AA in the future.\nI never told him that it's a good solution for him.\nBut don't you think other people deserves the right to know about it?\n\n> Someday it probably won't and then I might feel the need to have gnome\n> libs too, but right now there's no such need.\n\nThen what's your problem? It's not like anybody force you to use it.\nAnd porting AbiWord to gnome-libs now will make porting it to GNOME 2.0 and Bonobo much easier, now that the AbiWord codebase is still relatively small."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "I'm sorry, but you really piss me off. You really like to argue, don't you?\n\nNo matter how benign I try to write it, you'll always find something to argue about. If you are such a tender spirit, what the hell are you doing on site where you know you'll find more than plenty of people who disagree with you.\n\nAnyway, I've had enough. I'll just ignore you from now on which is too bad because at first you actually sounded reasonable."
    author: "Marko Samastur"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "Oh and you think you don't piss me off? Whenever I try to prove that GNOME is not as inferior as you think, you find another reason to complain about.\nIf you want to ignore me, then fine, go ignore me. I don't care."
    author: "Stof"
  - subject: "KDE vs GNOME"
    date: 2001-09-06
    body: "My personal experience: KDE is a MUCH better \"newbie\" desktop. GNOME is a marginally better \"power user\" desktop. As a \"power user\" myself, I use GNOME. But my wife, who lies somewhere in between (far, far above the level of most of today's \"newbies\", comfortable with a limited number of commandline commands \"killall netscape\" etc) wouldn't switch to Linux fulltime until I showed her KDE.\n\nI really tried to like KDE: switched my home machine's default login for over a month. But I couldn't - the major factor for me was the general feel of bloatiness. Note that I'm NOT accusing KDE of *being* bloated, just of *feeling* bloated. Apps took a LONG time to start, and for some (psychological) reason KDE apps (like konqueror) feel like they should be \"light\" applications which you can start instantly. Konq is much faster to start than Mozilla, but I found Konq impossible to browse in because I always wanted to open new Konq windows (which is still slow), wheras with Moz it somehow feels more heavyweight and you don't have the inclination to stop-and-start it so much (I keep two windows open at all times and just reuse them). Other GNOME applications definitely feel faster to start - gnome-terminal over konsole, or Abi over KWord.\n\nI feel that GNOME is good if you want the desktop environment to stay out of the way, which is what I do. If you want a complete and integrated feel and don't mind paying a slight performance cost, though, get KDE.\nEither is the right decision :)\n\nStuart."
    author: "Stuart Ballard"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-06
    body: "Funny... I think of it as exactly opposite.\nKDE actually has too much features, and too disorganized menus for it to be\ntruly user friendly.\nNautilus is also way easier and prettier than Konqueror.\n\nThat said, KDE and Konqueror does have more features, and is better\nintegrated. Technically KDE is a bit ahead in the desktop-area, while in\napplications there is pretty much a tie. KDE is way ahead in Office-apps, GNOME a little bit ahead in browsers (Galeon vs Konqueror), quite a bit ahead in email (Evolution vs Kmail), way ahead in updating (Red Carpet vs nothing) and quite a bit ahead in configuration ( Ximian Setup Tools, which are sweet btw.).\nIn i18n KDE is way way way ahead.\n\nI do agree with you on either being right decisions."
    author: "Gaute Lindkvist"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-06
    body: "every one keeps refering to ximian v. KDE but remember that ximian is a company and has Redcarpet and evolution as product. KDE is a not profit Org and would therefor not have some features that ximian does. however, since KDE is more feature ritch and easier to integrat stuff into thanks to Kparts, I think it would be realy cool if TheKompany.com came out with its own version of KDE that it could use to leverege its app onto. then one could see a redcarpet type application and TheKompany.com has an outlook type app that does what evolution will do. as far as it is now though, comparing ximian to kde is like comparing apples to oranges, it would be better to compare the stock Gnome DE to KDE."
    author: "Jeremy Petzold"
  - subject: "Please, a standard installation of KDE"
    date: 2001-09-06
    body: "> I think it would be realy cool if TheKompany.com came out with its own version of KDE\n\nO please, no... It's enough complex now with as many KDE as distros...\nAs I already said elsewhere, I think that it would be better to define a standard KDE installation to be used by all the distros.\n\nI don't speak about the compilation mode, but about the name of directories used and so...\n\nI have read yesterday that for compiling a KDE app on Mandrake 8.0, it is necessary to do  something like export KDEDIR=/usr/ What a foolish thing !! If Mandrake don't know how to install KDE, I wish that the KDE team explain how to do...\n\nI repeat (sorry...) : the definition of a standard installation of KDE would be very useful for all the users."
    author: "Alain"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-06
    body: "KParts is not something unique to KDE.\nGNOME has Bonobo, which does about the same job, but is based on\nCORBA, whereas KDE chose to go its own way.\nRed Carpet and Evolution may be Ximians products, but they are just as free\nas the rest of GNOME and KDE.\nWhy is this any different than the fact that Trolltech has Qt as a product (free), and this is used by KDE. Both things are perfectly fine.\n\nBtw. Ximian and The Kompany should not be compared too much. All of Ximians products are GPL, The Kompany makes proprietary products, which is fine, but\nmean that they can never be part of KDE."
    author: "Gaute Lindkvist"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-06
    body: "Yes, but Ximian's main buisness is GNOME, while KDE is not Trolltech's work."
    author: "John Porter"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-07
    body: "Also remember that Ximian has a lot of influence on the GNOME core development. TheKompany just make applications."
    author: "Loranga"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-06
    body: "1. how is Galeon better than Konqueror?\n2. how is Evolution's email features better than Kmail's?\n3. updating\n   up to the distro\n4. configuration\n   up to the distro"
    author: "John Porter"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-07
    body: "Galeon better than Konqueror:\n1. Faster, both startup and rendering.\n2. Better and more solid rendering.\n3. Better support for plugins and java\n4. Better handling of bookmarks (including the great auto-bookmarks feature\n5. A fantastic feature in which you specify the default downloading-directory in\n   your configuration, and never have to deal with the whole \"save as\"-dialog        \n   for downloading again.\n6. Optional tabbed-browsing\n\nKonqueror does have some neato features like more elegant cookie-handling and\nanti-aliased fonts.\nEvolution better than KMail:\n1. Much, much better GUI\n2. Faster\n3. Included groupware\n4. Incredibly elegant filter-handling\n5. Incredibly elegant integration with GPG\n6. Great search-features\n\nKMail does have some better features like Outlook-importing.\n\nUpdating:\nYou have a very easy solution, that is really not good enough. THe distros\nmostly freeze such packages between releases. Your own updating make\nsure you can have the newest and best packages at all times. The only\ndistro that does this well enough is Debian Unstable.\n\nConfiguration:\nYour solution makes sure that configuration is different on all distributions.\nBesides Ximians configuration-tools are incredibly slick and better integrated\nwith the desktop-environment.\n\nIs it really that hard to see that some things are actually better in GNOME?\nKDE is NOT better at absolutely everything."
    author: "Gaute Lindkvist"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-07
    body: "Galeon better than Konqueror:\n 1. Faster, both startup and rendering.\n\nOpening konqueror inside KDE takes no time at all. And I think khtml is faster than gecko because like IE, it progressivly renders the page.\n\n 2. Better and more solid rendering.\n\nMaybe. But not for 99% of most sites. The other 1% are nonstandard, sites with buggy HTML that Konqueror wants to avoid (such as sites with the <nobr> tags), but could support if it wanted to. I think the next release of KDE will fix most of these issues.\n\n 3. Better support for plugins and java\n\nI think you have this the other way around.\n\n 4. Better handling of bookmarks (including the great auto-bookmarks feature\n\nI would not say better, the auto-bookmarks feature is nice.. But I like the bookmarks in the extended sidebar of Konqueror better.\n\n 5. A fantastic feature in which you specify the default downloading-directory in your configuration, and never have to deal with the whole \"save as\"-dialog \nfor downloading again.\n\nYou can specify the default downloading directory in Konqueror too. The avoiding save as feature sounds cool tho. However, does galeon have the ability to drag links to directories to save them? I think not.\n\n 6. Optional tabbed-browsing\n\nYeah, I wish Konqueror had this.\n\n Konqueror does have some neato features like more elegant cookie-handling and\n anti-aliased fonts.\n\n Evolution better than KMail:\n 1. Much, much better GUI\n\nThat's quite subjective. In fact, I hate libgal (helixcode/ximian's widgets). Whole Evolution tries to be Outlook Express 5.0, KMail tries to be like PegususMail/Eudora/and Outlook Express 4.0. I think that is a huge difference there.\n\n 2. Faster\n\nSeem same speed to me.\n\n 3. Included groupware\n\nAgain, this isn't the goal of KMail. Those features can be put into other apps.\n\n 4. Incredibly elegant filter-handling\n\nSo is KMail's.\n\n 5. Incredibly elegant integration with GPG\n\nSo is KMail's.\n\n 6. Great search-features\n\nSo is KMail's.\n\n KMail does have some better features like Outlook-importing.\n\n Updating:\n You have a very easy solution, that is really not good enough. THe distros\n mostly freeze such packages between releases. Your own updating make\n sure you can have the newest and best packages at all times. The only\n distro that does this well enough is Debian Unstable.\n\nActually, several other distros give you great updating tools. Mandrake and SuSE seem to be filled with KDE users, and they do.\n\nBesides, redcarpet always has old packages (from my experience).\n\n Configuration:\n Your solution makes sure that configuration is different on all distributions.\n Besides Ximians configuration-tools are incredibly slick and better integrated\n with the desktop-environment.\n\nYes, but.. they do not work reliably under many circumstances.\n\n Is it really that hard to see that some things are actually better in GNOME?\n KDE is NOT better at absolutely everything.\n\n1. true\n2. true\n3. I think GNOME still has not caught up with KDE 2.0. Do you know why? Because it lacks a stable compartment architecture that is in use by many apps. KDE 2.x has this. Gnome 1.0/1.2/1.4 doesn't. GNOME 2 will, but currently it is a bunch of test apps (now that Gtk2 is almost frozen, that should gradually change)."
    author: "Cory Johnson"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-07
    body: "> Opening konqueror inside KDE takes no time at all. And I think khtml is faster than gecko because like IE, it progressivly renders the page.\n\nSorry, but that was a poor statement. :)\nUnfortunatly (for you) Gecko features much better progressive page rendering than KHTML. That's why KHTML performes very badly on slow connections (and does not even work with constantly streaming content like webchats), it really shines for fast connections and local files.\nStartup time is indeed nothing we should worry about. For some configurations Konqueror may be faster, for other configurations, Galeon may be faster. That's pointless.\n\n> 2. Better and more solid rendering.\n\nHere you (Cory) are right. I was impressed by the latest KHTML versions, it mostly renders \"reallife\" websites even better than Gecko. Partly because KHTML supports a lot of proprietory IE tags (Gecko does not).\nGecko has still more features (in the DHTML department), but most of them aren't used today or used very rarely.\n\n> 3. Better support for plugins and java\n> I think you have this the other way around.\n\nYep, I think Konqueror has better plugin support... I don't know about Java though, I rarely use it. \n\n> I would not say better, the auto-bookmarks feature is nice.. But I like the bookmarks in the extended sidebar of Konqueror better.\n\nIt's a matter of taste of course, but Galeon really has a lot of amazing bookmark features. Like the \"smart bookmarks\". For example you can show those smart bookmarks in a toolbar and they will show up with an input box. You can insert text there and it's used as part of the bookmark. There are so many features, it's unbelievable. :) It really shows that the Galeon developers spend their WHOLE time on the GUI (nothing else).\nYou can say what you want, but the Konqueror shell just can't compete with the Galeon shell. You can compare Konqueror with Internet Explorer. Internet Explorer doesn't stand a chance against Galeon as well (as the GUI).\nAnd if you think you know Galeon: Make sure to check the latest version, it's unbelievable how fast they progress.\n\n> You can specify the default downloading directory in Konqueror too. The avoiding save as feature sounds cool tho. However, does galeon have the ability to drag links to directories to save them? I think not.\n\nThat works with Konqueror? Let me see...\nYeah, that worked. But Konqueror hung after moving the file to trash. Damn. Fortunatly it wasn't crashed, it just hung for a few minutes! I thought I had lost all this text! :)\n\n> Yeah, I wish Konqueror had this.\n\nAgreed, Tabbes are the number one feature of Galeon. Never saw them implemented that great.\n\nI don't comment all those mailer questions. It's a matter of taste. Evolution IS great. But KMail is great as well. While KMail is clean and simple, Evolution is powerfull and has some very neat features which makes your life easier. \n\n> Because it lacks a stable compartment architecture that is in use by many apps.\n\nIs \"compartment\" a mixture between \"component\" and \"parts\"? ;)\nYes it's quite nice. But guess what? It's not THAT important. \nAnd it's not true, that Gnome 1.4 doesn't has it. It has.\nBonobo is already there. It's just not used that much yet (Nautilus beeing the exception).\nI'm getting tired of hearing \"KDE is superior because of KParts\" all the time. :)\nTo me it's mostly image, the practical gain is there, but not THAT important.\n\nIMHO KDE and Gnome are too different, to be compared in a \"A is better than B\" way. Like GNU/Linux and Windows or XFree and DirectFB."
    author: "Spark"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-08
    body: "It'd be neat to have tabbed browsing in Konqueror, but if you really want/like tabbed browsing, use Opera. It's tabbing is a bit more advanced than Galeon's is (mainly because it has the ability to have a unified workspace as well).\n\nAs for the rest of us who actually have the screen real estate (like most people), we'll use konqueror or mozilla."
    author: "Matthew Clark"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-08
    body: "Oh no, how stupid (sorrysorry!).\nOpera is one of the uglyst browser I know.\n1) The HTML engine is crap.\n2) Fonts are crap.\n3) It's no KDE app, so what? \n4) Tabbed more advanced than Galeon? LOL\nThe MDI of Opera is AWEFULL. I never liked it. I want tabs, not windows in windows!\nAnd it does not even feature half of the features that Galeon feature! Err... you know what I mean. Tabs are pretty simple in Opera.\n5) You are FORCED to use this tabs! Ugly!\n6) The tabs bar does not disappear when only viewing one document, so screen estate is wasted all the time.\n7) Screen estate? I use a 19\" Monitor with 1600x1200 resolution. Surprised?\n\nIt's obvious, that you never worked with the latest versions of Galeon."
    author: "Spark"
  - subject: "Re: KDE vs GNOME"
    date: 2002-09-13
    body: "Why does Galeon provide faster download speeds when I use gnome compared to KDE?\nI would much rather use KDE because its honestly easier on the eyes but at the same time it is all about the SPEED.  Is there anything that I can do to boost the performance in KDE?\nWhen I go to www.bandwidthplace.com with Gnome and Galeon  I avge 1.5 - 1.8 Mbps\nWHen I use KDE & Galeon I get about 1.0 - 1.3 Mbps\nand most depressing of all is when I use Konqueror I get 500Kbps - 900Kbps\n\nAny input would be greatly appreciated."
    author: "Josh"
  - subject: "Re: KDE vs GNOME"
    date: 2002-09-13
    body: "Who knows if the test is accurate?\n\nI'm on WinXP atm, and with ie6, I get 1 megabits per second (127.6 kilobytes per second), and on moz 1.1, I get 1.2 megabits per second (141.1 kilobytes per second).\n\nWith the second trial, I get 902.6 kilobits per second/110.2 kilobytes per second IE6, and 810.4 kilobits per second/98.9 kilobytes per second with Mozilla 1.1."
    author: "dc"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-08
    body: "I have to deal with the very untrue parts here first:\n1. You think plugin-support is actually _better_ in konqueror? Yes, it may support some ActiveX in development-versions, but when it comes to standard plugins Mozilla (and thus Galeon) truly smashes Konqueror.\nFor instance, Konqueror claims to support Java fully, and Flash, but so far, in all the latest versions of KDE (I've used every version from the 2.0-betas), it has never worked properly. Java is slow, or doesn't work fully, and Flash just hangs and crashes any other browser apart from Netscape 4.x and the mozilla-based ones.\n2. Opening Konqueror inside KDE _does_ take some time. Galeon launches instantly on my laptop, Konqueror 2.2 still uses around 3 seconds.\nEven if some of your arguments against Red Carpet and Ximian Setup Tools are at least partially correct, it does not change the fact that GNOME _has_ these, while KDE has none whatsoever.. and even though you think they are not necessary, I do, and plenty of people do. Besides, my integration point is very valid, and so is my point on regular updating.\nI'm not saying that GNOME is technically at KDEs level right now, but there ARE areas where GNOME is superior, and the KDE-people seem to be incredibly defensive on this point (not the developers... the users)."
    author: "Gaute Lindkvist"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-08
    body: "Your computer must be on crack or something. \n\n1. It's a bitch getting java to work in mozilla or galeon.\n2. Speed of Java execution has nothing to do with konqueror or galeon. It has everything to do with the speed of the jdk. \n3. Konqueror loads in less than one second in KDE. Make sure you use \"kfmclient openProfile webbrowsing\" (the dcop interface to launch it). Galeon loads in less than one second in GNOME. Seems like a tie to me. \n4. Flash has always worked for me in Konqueror. I was amazed on how integrated it seemed."
    author: "Cory Johnson"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-08
    body: "It is simple to get java to work in Mozilla... it might not have a GUI-tool, but it is easy. The problem is that the GUI-interface for selecting java-vm in Konqueror has never worked _at all_ for me.\n\nln -s /pathtojavavmplugin /pathtomozilla/plugins\n\nThats it, and it works perfectly.\n Perhaps Konqueror seems slow at java because it hasn't used the correct plugin, but falled back to kaffe or something, in which case it is probably both mine and Konquerors fault.\n\nAnyhow, even if I misunderstood the plugin-issue, Mozilla/Galeon still renders better, has better support for current and coming standards (like xml), and gecko does not hang bad as khtml sometimes does. The only big issues where khtml is better is anti-aliasing (but AFAIK Mozilla can be compiled with Qt for the same result), and memory-consumption (and not a big difference here)."
    author: "Gaute Lindkvist"
  - subject: "Re: KDE vs GNOME"
    date: 2001-10-26
    body: "Somebody's computer has been smoking way too much crack.\n\nKonqueror's plugin support is excellent, although it is at most on par with Galeon's. Flash works wonderfully in Konqueror. Those of you who think Konqueror has problems with Java, y'all must be trippin'. Because I use the latest 1.4 beta java plugin with both browsers and Konqueror actually loads it faster. Believe it or not. I play Yahoo games quite a bit so I know what I'm saying.\n\nOh yeah, and startup speed. Apparently those who use Gnome or some other WM complain that Konqueror's start up is slow. Answer: because when you load up Konqueror (or any other kde app for that matter) in an environment other than kde, it has to run the kdeinit scripts and some other stuff first to get its act together. It's a testimony to the beautiful component integration that exists in KDE...once that whole engine is started up, everything else loads up pretty quickly. For me, first time I load up Konqueror takes about 2-3 seconds. Any other KDE application after that loads up almost instantly. \n\nI don't even worry about that as much as the application's speed/responsiveness. I find Konqueror to be a many times more responsive than Galeon (which means it responds to button clicks and stuff like that pretty much instantaneously). That is why I still prefer Qt to GTK+, because it simply seems to be better designed and faster responding. And KDE's component architecture also appears for me to be more responsive and quicker than GNOME's in all my experience with both environments. No further comments about that.\n\nAnd I don't fathom what some of you folks are saying about bloatishness. I personally find many gnome applications to feel more \"bloatish\" than KDE applications. KDE feels mature, stable and responsive. Gnome always feels like an obese beta beast in most of its apps .. Galeon, GIMP, Evolution, etc. So I don't know what the heck you fellas are talking about.\n\nSpeaking of Galeon, I agree with that previous comment saying that tabs are its best feature. If there's anything I would really miss in Galeon, it's the tabs. Opera's tabs suck. Galeon's tabs ROCK. Konqueror needs to get tabs. As for smart bookmarks, I don't really miss them. Because in Konqueror I can easily type gg:search_request in the address bar to search the internet, or fm:progname to search for software - no time wasted, no productivity lost for me.\n\nIt is for these reasons that I have chosen Konqueror as my default web browser. (and by the way, I use enlightenment for a desktop, so I'm pretty much on neutral ground). I have used Galeon for a while and I found it to be pretty cool (in fact, I still use it from time to time), but Konqueror is simply better. 'nuff said."
    author: "Jeopardy"
  - subject: "Re: KDE vs GNOME"
    date: 2001-10-26
    body: "I don't really like galeon's tabbing much.. it feels way too limited compared to Opera's. For example, in Opera, you can switch from single page view to multiple cascades of windows (or tiles of windows).\n\nSeriously, I think opera was the first app that implements MDI right. for any future tabbing/mdi implementation in konqueror, I think it should probably be moddled after Opera's (since it is a superset of Galeon's tabs)."
    author: "Nusa"
  - subject: "Re: KDE vs GNOME"
    date: 2001-10-26
    body: "khtml is still the most standards compliant engine around.. not to mention the fact that konqueror loads pages faster and reders better than mozilla or galeon. it also feels much more responsive, and the gui is consistant (the scrollbars are actually consistant with scrollbars in other apps :-) ) also, I think more plugins work with konqueror but I'm not sure (I haven't setup java or flash with mozilla yet)."
    author: "SB"
  - subject: "Re: KDE vs GNOME"
    date: 2001-10-27
    body: ">Java is slow, or doesn't work fully\n\nYup, sounds like you've just described Java."
    author: "Michael Bushey"
  - subject: "Re: KDE vs GNOME"
    date: 2002-09-30
    body: "Hi,\n\nPersonally i disagree with most KDE fan posts. You must be using some\nmisconfigured system, because i mesaured resource usage of both KDE and\nGNOME, and the latter always used less resources (MEM and CPU), needs less \"kdeinit\" style daemons, and integrates better with different software. If you like KDE, great, but please stop \nflaming (those comments about smoking crack... please stop it).\nThe distribution thats being used is an important factor. For example SuSE works best using\nKDE, Gnome doesn't work well on SuSE, because it includes stone age like versions, that aren't\nproperly setup. RedHat or Debian work equally well with Gnome or KDE.\n\nI would like to make a callout to stop the flames.\n\nBest regards.\n\nmjander"
    author: "mjander"
  - subject: "Re: KDE vs GNOME"
    date: 2002-10-01
    body: "> (those comments about smoking crack... please stop it).\n\nAlright, stop shooting up heroin... You're replying to posts from nearly a year ago (Oct 2001). Did you notice the timestamp?\n\nAnyways, whatever they said a year ago isn't necessarily true anymore with the rapid pace of development in KDE and GNOME. They are both getting close to each other in terms of feature sets. Use whatever one you prefer and stop catering to trolls."
    author: "fault"
  - subject: "Re: KDE vs GNOME"
    date: 2007-05-07
    body: "I agree!\n\nNow, where's my heroin?\n\n\n(Sorry, I couldn't resist).  I found this because, as a new linux user, I'm still trying to decide which to use.  I'm using Debian, which defaulted to gnome, but I installed KDE, too.  \n\nI'll have to admit, I like Kontact much better than Evolution for email/calendar.    And Konqueror is a nice file manager. But when I was running Suse with KDE, after editing the menu, it went to hell and wouldn't display a lot of the programs until I added some fake entries, for some reason.  Don't know if I did something it didn't like, or if the menu editor is just buggy.  But gnome isn't very easy to edit either.\n\n-- Marty"
    author: "Marty"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-07
    body: "Well, I don't feel well saying \"KDE is better than Gnome\" because I tried Gnome only for a very short time.\nBut I what I can really say, you can configure konqy any way you want to make it look whatever you want, to have menus und toolbars the way you want, you can use styles to make your whole KDE look different, you can use your own funky icons, and I'm only aware of exactly two features konqy doesn't have, otherwise it has more or less all features you could think of :-)\n\nBye\nAlex\n\nwhat makes me think, having a menu editor like the toolbar editor would be a good idea..."
    author: "aleXXX"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-07
    body: "> to have menus und toolbars the way you want\n\nNot really. You can't edit the menus (and they are a mess btw) and you can't edit the toolbars like you can do with Galeon (try it). For example I never managed to get the stupid extra buttons of the toolbar and the locationbar into the standard toolbar.\nAnd did you try to move the toolbars around while dragging them on their handle? It's unbelievable, how bad that works. :) I messed up everything and had a hard time dragging them back like they were. ;)\n\n> you can use styles to make your whole KDE look different\n\nSure, and so you can use Gtk engines. ;) \n\n> you can use your own funky icons\n\nHmm well. Galeon can use the Nautilus Icons. Nautilis themes are pretty cool. I don't know how to choose some nicer buttons for Konqueror without changing my desktop theme. Do you?\n\n> otherwise it has more or less all features you could think of :-)\n\nNah, you should play a bit more with Galeon. ;)\nI used both Galeon and Konqueror a long time and only Konqueror for the last time, it indeed IS lacking a lot of features. Most important of all is the tabbed browsing.\n\n> what makes me think, having a menu editor like the toolbar editor would be a good idea...\n\nEven better would be a clean and simple menu structure, not the mess that Konqueror menu's currently are... Compare that to the Galeon menus.\n\nI don't want to bitch, Konqueror is great. Just not perfect...\nIt mostly suffers from beeing an \"all in one\" app, which isn't the best idea IMHO."
    author: "Spark"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-08
    body: "I think you should compare Konqueror and Nautilus. Konqueror in that case beats Nautilus hands down in web browsing. Compare these two because both are split file browsing/web browsing applications."
    author: "John Kerin"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-08
    body: "Yep, but Nautilus does not even TRY to be a Webbrowser. It's just a filebrowser with the ability to show HTML pages.\nThe same should Konqueror do and there should be an alternative KHTML Webbrowser that concentrates ONLY on webbrowser (so it can compete with Galeon).\nATM there is no equivalent to Galeon for KDE."
    author: "Spark"
  - subject: "Re: KDE vs GNOME"
    date: 2003-08-02
    body: "Galeon is the worst web browser I have ever used. It's just terrible.\nThe un-intuitive means of using the tabs, the shoddy, slow page rendering.\nAweful."
    author: "Ez"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-06
    body: "After KDE apps start, they should be as fast/if not faster than GNOME applications. Have you tried objprelink? It should speed up loading of KDE apps by 20-40%."
    author: "John Porter"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-07
    body: "Same technology can be used by GNOME. Some guys at the mailing lists are already planning to implement such feature."
    author: "Stof"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-08
    body: "What? Objprelink with C programs? Impossible. Objprelink uses C++'s virtual tables. The very few applications that use C++ in GNOME (such as Gabber), could use objprelink, but 95% of the rest can't.\n\nPlease provide URL's to the mailing list discussions. Perhaps they were talking about something else."
    author: "John Kerin"
  - subject: "Re: KDE vs GNOME"
    date: 2001-09-08
    body: "OK, perhaps not the same, but similar I guess. They call it \"ELF prelinking\".\n\nHere's the mail:\nhttp://mail.gnome.org/archives/gnome-2-0-list/2001-August/msg00400.html\nRead the section about \"Too many libraries\"."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "> E.g. Xmms is nice, but does not \"fit\" into the desktop one bit.\n\nErr, why? Because of the skin? Well, some people prefer applications with \"charakter\" and a nice skin above apps that \"all look the same\". :)\nAll that matters is that dialogs, etc are Gtk, so it DOES fit in.\n\n> Same for Mozilla.\n\nMozilla is for everyone!\nIf you want a Gnome browser, try Galeon. Fits perfectly. And is btw a better browser GUI than Konqueror. :) But currently I like KHTML better than Gecko. It's lighter and it has great rendering for *current* websites. Gecko is a beast and very capable when it comes to JavaScript and DOM. Most pages don't use that very much yet, so KHTML fits my needs very good. You just can't say that Konqueror is the better browser. It's a matter of taste and as I said, Galeon is definetly the better browser UI than Konqueror (could be because Konqueror is a \"let's view and edit everything\" application. ;)) Something like Galeon for KDE and KHTML would just rock. Well, maybe that's something I could try to code. :) It shouldn't be that hard to embed KHTML into a new app, right?\n\n> I would miss the internet awareness of KDE.\n\nThe what? :)\nI don't see a difference in \"internet awareness\" when using KDE or Gnome.\n\n> Everything just looks the same, behaves the same and so on. Gnome is pretty, but not as streamlined.\n\nThat's a matter of taste, not an argument for one of each sides. :)\n\"Everything the same\" can be nice, but \"everything looks different\" can be just as nice as well. Gtk/Gnome has the advantage, that Gtk and Gnome apps look the same, that's really bad with Qt and KDE (fortunatly this will change with Qt3 and KDE3 AFAIK :)).\n\n> And I would miss stable true type font support with AA.\n\nTrueType support is just as (un)stable as in KDE... and AA is a cosmetic detail, which isn't perfect in KDE either. I needed a long time since it didn't look awefull anymore... but I have to admit, once it's configured nicely, it's very good. :)\nBut really just a detail and Gtk will have this soon (it already has, just not \"official\").\n\n> Abiword e.g. is nice, but not very gnome specific.\n\nWho cares? All that matters is, that all Gtk apps look and behave the same.\nThere is no such thing as \"the Gnome desktop\". \"Using Gnome\" basically means, that your desktop mainly consists of Gtk apps and Gnome-Libs are installed. Most of the time you will also load the \"panel\" and if you like, you can use the Gnome session-managment. \n\n> Finally Red Carpet: What is this? \"Red Carpet, the update tool for ximian is a killerapp\" It does not work with my distribution, it will trickily remove KDE (from what I have heard), it will modify my stystem. How is updating an \"killer app\" ? Check out SuSE You or Debian aptget. Same thing. Not very desktop specific.\n\n(It does not remove KDE anymore!)\nYep, that's the point. It's distro specific. Red-Carpet is just gold on a Red Hat system, while it's absolutely useless on my Debian box. I figured that Gnome fits perfectly in a Red Hat system, while KDE runs great on SuSE and especially Debian (I'm currently writing this from KDE 2.2 on Debian 2.2 ;)).\nI have Ximian GNOME installed on my other Linux partition with Red Hat (I use this for gaming and KDE for coding)."
    author: "Spark"
  - subject: "Re: Miguel..."
    date: 2001-09-06
    body: "Why did you install Koffice, if you don't need it?\n\nIt's not a part of standard set of KDE packages so it's very unlikely, it got installed by itself."
    author: "Marko Samastur"
  - subject: "Re: Miguel..."
    date: 2001-09-06
    body: "It is generally accepted that GNOME has not cought up with KDE 2.0 yet. Even most GNOME developers will say this (they point to Gnome 2.0, which seems to be a collection of test apps right now). \n\nKmail has ALL the email features that Evolution does. The extra stuff in Evolution are either features that are in other KDE applications (such as knode). \n\nXimian desktop and menus bloated compared to KDE? hah. I think you have that the other way around. Nautilus is WAY more bloated than konqueror is. \n\nOther than that, I think GNOME development has basically slowed down to a point that it will take 1). a long time for GNOME 2.0 to be released 2). continue to lose market share, after getting a lot of market share with GNOME 1.2."
    author: "Morgan Tong"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "Slowed down? Dude, look at the mailing list, look at the CVS commits! The project is on steroids!\nThe only reason why people think GNOME is not progressing is because they don't release as often and because most work is done on the architecture rather than user-visible things."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "I have debian/unstable, with the newest version of Nautilus. It is still a lot slower/resource intensive than Konqueror is. And Konqueror doesn't try to take over the whole desktop like Nautilus does."
    author: "Morgan Tong"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "You can always disable desktop icons."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "> Ximian desktop and menus bloated compared to KDE? hah. I think you have that the other way around. Nautilus is WAY more bloated than konqueror is. \n\nWell, besides of that you somehow screwed up in this sentence, the menus of Nautilus are WAY more clean and simple than those of Konqueror. \nYes, it's not a big deal from a technical point of view, but for the enduser experience, those are almost the most important things. It's just fun to use. KDE should learn from it instead of looking down to it...\nKDE could be a great desktop if they would do more usability testing."
    author: "Spark"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "I am a fan of both KDE and GNOME, and I use apps from both environments. However, I have been using GNOME as my main environment since the 1.0.x days. I should add that I am not trying to troll. I am simply stating why I like GNOME over KDE in order to provide constructive criticism for KDE (and some for GNOME as well).\n\nI have found that nothing can beat the configurability of the Sawfish WM and the GNOME panel. There are literally <i>hundreds</i> of keybinding options for Sawfish, and it is very easy to configure an environment that requires no mouse use at all. Sawfish is also very extensible, with its own LISP-like scripting language. There are Sawfish scripts available on some websites, and many add unique and interesting functionality. It is still very user-friendly, and a great deal can be done via its GUI menu options. If you don't like Sawfish, GNOME makes it easy to switch to another WM (I sometimes use Enlightenment or IceWM instead). \n\nIn the panel department, the GNOME Panel is extremely configurable. You can put panels wherever you want, and even have them floating in the middle of the screen (not off an edge). They can be any size you want, and its applets (e.g. the Deskguide and Taskbar applets) tend to have more configuration options than applets for other types of panel. This is just a small fraction of what it can do.\n\nGNOME most certainly needs some polishing when it comes to little things like drag-'n-drop support. It is more loosely-coupled than KDE by design. This has been beneficial in that you can add/remove components (e.g. WMs and desktop managers) as you please, giving the user more choice and flexibility while creating a spirit of healthy competition and co-operation (yes, you can have both) amongst developers. However, it is also a disadvantage in that it is less integrated, making it less user-friendly for the newbie. \n\nI believe that GNOME is like this because its focus in the past has not been the ordinary user, unlike the KDE Project. GNOME was made by hackers for hackers, and has only targetted ex-Windos users comparatively (to KDE) recently. This is meant to be addresseed by GNOME 2.0 and beyond, but I doubt that GNOME would (or could) fully dump its previous development method (i.e. loose and flexible) for a more integrated KDE-like approach. After all, the <i>developers</i> have the final say in what direction the project takes, and I can't imagine veteran GNOME hackers dumping their old beliefs. The more options that you give someone, the harder it is to integrate them, and the more likely it is that you'll confuse the user. In other words, there is a trade-off between user-friendliness and configurability, and this is GNOME's predicament. They are doing a good job (IMHO) at picking defaults for some things and limiting the amount of choices to the user, so that they don't become confused. For example, the Sawfish configuration options has a beginner/intermediate/expert setting to determine which options are made available to the user. Nautilus has a similar setup. However, more work needs to be done here.\n\nI remember reading an article recently about Ximian, which stated their three target markets. The first is existing GNOME users. It is far easier to keep a customer than to gain a new one. Ximian supports distros like Red Hat and Debian better than they support distros like Mandrake and Suse because there are a much larger proportion of GNOME users in them. The second is the underprivileged. This diverse group includes schools, poorer people and nations, and the disabled. There is much usability work going into GNOME to enable things like mouseless operation, which is vital for many people. The third target market is the corporate desktop. This is the area that is receiving the most support, through contributions by corporations like Sun, HP and Red Hat. Evolution, for example, has been deliberately engineered to be a drop-in replacement for MS Outlook (hence the same look and feel), which has become firmly entrenched on corporate desktops. I am not a programmer, but from what I have read it appears that Kparts is more suited to single desktop than to a large network, whereas Bonobo (which is based on CORBA) is built for the enterprise (CMIIW). This can explain why GNOME chose a CORBA-based solution over something faster, like Kparts.\n\nNote that in the three target markets I just mentioned, there is no mention at all of the consumer desktop market, which is where KDE is mostly pointed."
    author: "Yama"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "'Bonobo (which is based on CORBA) is built for the enterprise'\n\nBut what does this mean, really? This phrase 'built for the enterprise' keeps on being trotted out as if it's a) meaningful b) a good thing c) applicable to CORBA. CORBA is an over-engineered, bloated by design specification which you can only implement quickly by not supporting large parts of it. It is completely unsuitable for fast inter-process communication. DCOP is a fast, light interface which can be extended to the network if necessary (via SOAP, if nothing else).\n\nYou should make things as complicated as necessary, but no more. CORBA is too complicated to be the solution to the problem DCOP solves."
    author: "Jon"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "> CORBA is an over-engineered, bloated by design specification which\n> you can only implement quickly by not supporting large parts of it.\n\nJust because the CORBA KDE used (MICO or something) is slow doesn't mean ORBit is slow.\nIn fact, ORBit is quite fast and is proven to be the fastest CORBA implementation.\n\nBesides, computers gets faster and faster these days.\nIf your computer can run KDE smoothly then you won't notice the speed difference between CORBA and DCOP.\n\n\n> CORBA is too complicated to be the solution to the problem DCOP solves.\n\nIf it's really that difficult, then tell me how the GNOME developers managed to understand CORBA in a short time."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "If you notice that every once in a while, GNOME developers talk about switching to DCOP. Even Miguel has suggested it. It of course, always gets flames."
    author: "Menderes Chiracouglu"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "I check the mailing lists regularly, but I've never read such thing.\nPlease provide a link to the email."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-09
    body: "Using Orbit is still slower than using libICE. \n\nCobra works through network sockets. ICE works through fast shared address space. \n\nThis makes a huge difference in slow computers."
    author: "Gary Storla"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "'I can't see one compelling reason for me to switch to KDE.'\n\nThe compelling reasons for me:\n\n1) Konqueror\n\nThe best free UNIX web browser - along with Mozilla (which is more standards compliant, but has created YET ANOTHER completely new set of widgets to bloat your memory with)... and that's only a small part of what Konqueror does. File manager, ftp client, Audio CD ripping front end... it's what Nautilus could have been in about 2 years if Eazel had spent another 30 million (!) on it.\n\n2) Kate / KWrite\n\nNow this is a lovely text editor -- pity it doesn't have Python highlighting like the 2.1 KWrite did, though.\n\n3) KDevelop\n\nGives you a warm fuzzy feeling inside.\n\n4) The new printing architecture\n\nIf I had the money I'd send the guy who wrote this \u00a350 as a thank you. Combined with CUPS this FINALLY makes KDE as a whole a decently integrated system.\n\n--\nOther reasons:\n\nWhenever I use Gnome the dark and blurry icons hurt my eyes.\n\nKDE has a pretty dragon mascot. Gnome can't even manage a whole leg.\n\nKDE actually seems to have been people developing it in the last year. It's finally reached the stage with 2.2 where it feels polished.\n\nI don't like the arrogance and attitude of De Icaza. You can trust Germans. (and this is coming from someone from the UK :)"
    author: "Jon"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "> it's what Nautilus could have been in about 2 years if Eazel\n> had spent another 30 million (!) on it.\n\nFirst everybody whines about Eazel's existence, and now everybody whines about it because Eazel *doesn't* exists?\nHonestly, those guys have done great work and sacrificed their company, and you bitch about that?\n\nBesides, add CD-ripping features is quite easy: just write a gnome-vfs module.\n\n\n> KDevelop\n> Gives you a warm fuzzy feeling inside.\n\nI tried KDevelop a month ago (yet again), but I still don't like it.I prefer Anjuta instead, which feels much more comfortable to me.\n\n\n> 4) The new printing architecture\n> If I had the money I'd send the guy who wrote this \u00a350 as a thank you. \n> Combined with CUPS this FINALLY makes KDE as a whole a decently integrated\n> system.\nGnome-print can be easily extended to use CUPS by writing a backend for it.\nBesides, the default PostScript backend can be configured to use CUPS.\n\n\n> Whenever I use Gnome the dark and blurry icons hurt my eyes.\n\nI don't. I like the photorealistic icons. They look beautiful to me.\n\n\n> KDE has a pretty dragon mascot. Gnome can't even manage a whole leg.\n\nSo what? What does a mascot have to do with development?\n\n\n> I don't like the arrogance and attitude of De Icaza. You can\n> trust Germans. (and this is coming from someone from the UK :)\nIf somebody is arrogant then it's those who oppose De Icaza's *opionions*.\nWhenever he has an opinion about something, people bitch about it."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "I can't help but noticed that you keep saying it is easy to implement this and that in Gnome.\nIt proves that these things haven't implemented yet while in KDE it has. \nWhether it is easy or not to do that , well... it is quite easy to make such claims, but people really believe you when you have finished the job well. \nBut since we haven't seen any proves then we don't believe you !"
    author: "jamal"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "If you look at the architecture then you'll know that it can be done.\nI could care less wether you believe me or not."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "That's a cheap shot, man.\nTalk is easy, but when I ask you to prove it, you can't. Instead of show us what gnome is planning to do or something, you simply said \"look at the architecture..\". Gee.. anybody can do that.\nOh, well..at least we know what type of people you are."
    author: "jamal"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "1. Open gedit\n2. Choose menu File->Print\n3. Choose PostScript driver\n4. Use this command: cups\n5. Click on Print\n\nAnd there: printing using cups in gnome-print! Ain't that easy?\nIf this can be done so easily, then tell me why it's not possible to write a driver that prints to cups directly."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "So...? Nobody said it not possible.\nIt is nothing new. Cups support has been in KDE and Gnome for sometimes if you followed both. \nBut didn't we talk about Konqueror and Nautilus support in Gnome for these facilities such CD ripping and printing?\nAnd since you acknowledge the lack of these which certainly missed by some (because it is not implemented yet but easy, you said), that's the problem. I didn't see the implementation of these in Gnome's nautilus but in konqueror it has. Anybody can make a claim something is easy to be done and we will believe them if they gave us the result."
    author: "jamal"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "this is not very elegant..\n\nagain GNOME has a less integrated feel than KDE\n\nof course, i do not want to see GNOME die, so I wish it shall catch up more when GNOME 2 gets released\n\nremember, the best thing that ever happened to KDE was GNOME. KDE would not be so nice if GNOME did not exist. So I say, GO GNOME. AND GO KDE!!\n\nIf GNOME will not get its act together, I think KDE would progress slower. In the last year (and especially the last 6 months), KDE has seemed to go very fast and bring many users in from GNOME. It has lots of momentum. I hope GNOME also releases new version so that KDE will go faster again. Competetion is good."
    author: "Juan Cortez"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "I've tried both ajunta and gide in the last week, and neither of them have feature sets anywhere near to KDevelop2. Seriously, have you tried all three recently? Gide seems like an build of an early kdevelop (pre 1.1). Ajunta was a little bit better, but seemed to be more like kdevelop 1.2. \n\nThis was released 3 years ago."
    author: "David Rihaur"
  - subject: "Re: Miguel..."
    date: 2001-09-09
    body: "I agree. Ajunta is nice-looking and everything, but it still lacks features that kdevelop has had from the start. kdevelop is probably the defacto free GUI ide for Linux (and yes, gide has existed as long as kdevelop has)."
    author: "Gary Storla"
  - subject: "Re: Miguel..."
    date: 2001-09-09
    body: "kate in cvs has python editing support, as well as Makefile editing support (both were oversights, afaik). I'm not sure if this has been placed into the kde 2.2.1 branch or not, but it will definatly be in kde 3.0."
    author: "Gary Storla"
  - subject: "Re: Miguel..."
    date: 2001-09-06
    body: "I think all GNOME's \"mainstream press\" has actually done more harm than good.\nLook at all the people today: they are all anti-GNOME and pro-KDE.\nI can't see why, but for some reason people just *WANT* to believe that Ximian and all other GNOME-supporting companies are evil, while Ximian hasn't do anything harmful (wether Mono will be harmful or not will be clear in the future; nobody can tell what will happen just by using prejudgements).\n\nYou said that GNOME is nowhere near KDE 2. I disagree.\nDispite KDE's \"technical superiority\" or \"better open source project\" or \"better useability\" or whatever, I still prefer to use GNOME.\nYou can't say that KDE is better. Every desktop has it's own advantages and disadvantages. Wether a desktop is better is an opinion, not a fact.\nAnd GNOME is good, so unless it gets completely messed up (which will not likely happen), I will keep using GNOME as my primairy desktop environment with some GNOME and KDE apps."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-06
    body: "Why do you like GNOME better, in your opinion?"
    author: "John Porter"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "- If feels more comfortable to me.\n- The panel is more configureable.\n- I prefer to program in C rather than C++, using GTK+ and gnome-libs.\n- It is faster (I have a Pentium 233 with 48 MB RAM, so it's quite noticeable).\n\nI don't login to KDE. When I want to do something only KDE apps can, I open a terminal and types 'kfoobar' to do it.\nAll KDE programs runs fine in GNOME anyway."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "I doubt that GNOME would be faster than KDE on a Pentium 233. On my Pentium 166, originally, KDE1 was very usable, KDE 2, and Gnome 1.2/1.4 were not. After I installed 96mb of ram, and installed a faster rpm hard disk, KDE 2 was usable (but still not as fast as KDE1), while Gnome was not. Note that gtk apps were usable, but not Gnome apps. Konqueror ran at acceptable speed but neither Mozilla nor Galeon rendered at acceptable speeds. On my Athlon 750, Konqueror and Galeon are about the same speed, so that leads me to beleive that Gecko is a bit more resourcee intensive. \n\nAlso, I think that KDE 2.2's panel's configurability has cought up to Gnome's. \n\nI can understand it being more comfortable, as I ran fvwm and ksh for a very long time :). \n\nBtw, on my Pentium 166, I'm running kwin. It is extremely fast (as fast as blackbox)."
    author: "Menderes Chiracouglu"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "I once had a Pentium 166 too. I was using RedHat 6.0/6.1, and I had GNOME 1.0.\nBack then, I prefer using KDE 1 because it was faster than GNOME.\nHowever, when GNOME 1.2 was released, and I replaced Enlightenment with IceWM, GNOME became much, much faster.\nI prefer the highly customizeable GNOME panel over the KDE one, so I switched to GNOME.\nI never had stability problems with GNOME 1.0, probably because I install every update and bugfix immedietly when they were released.\n\nAnd I don't notice a speed difference between GNOME apps and GTK+ apps (except for startup time, but the same applies to QT and KDE apps).Now I have a Pentium 233 with 48 MB RAM, and Galeon runs faster than Konqueror here."
    author: "Stof"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "Remember that Konqueror is a lot more feature filled than Galeon is. After all, Konqueror is NOT a web browser, but can be one.\n\nI'd think that konqueror-embedded would be faster than Galeon, mainly because Galeon still has to load gtkmozembed (which STILL takes more resources than khtml (konqueror takes more memory using kmozilla than khtml)). \n\nI'd like to see a way to use gtkhtml2 in galeon. It is a nearly complete port of khtml, just like gtkhtml is a port of the old khtmlw (kde 1.x's html widget). Of course, it'd miss things such as kjs. This'd make galeon be a bit less bloated (right now what's holding it back is gtkmozembed). Also, it'd make galeon render pages better (namely ones than use IE rendering features)."
    author: "John Martin"
  - subject: "Re: Miguel..."
    date: 2001-09-09
    body: "Yeah, khtml seems to be faster on slower systems than gecko.\n\nCompare konq-embedded to something like light (the small gtk+-based browser that uses gecko). Comparing these two will show the speeds of the rendering engines, and not the outer user interface. \n\nFrom my experience (on a Pentium 200), these things happen.\n\nGecko is faster for loading large pages (>300kb).\nGecko is faster in loading pages with MANY medium sized (40k) images.\nkhtml is faster for loading medium-sized pages. \nkhtml is faster for loading pages with large (200k) images.\nthey seemed to be the same in loading small pages and small images.\n \nI think khtml had the advantage in speed because konq-embedded was \"zippier\" overall. By this, I mean that it was more responsive to the mouse and keyboard, as well as opening new windows. Also scrolling and selections were noticebly faster with konq-emb. Curiously, I think that this is reveresed in faster machines. Scrolling is about the same, but selections are slower in konq. Weird."
    author: "Gary Storla"
  - subject: "Re: Miguel..."
    date: 2001-09-06
    body: "xine is not a GTK app"
    author: "Matthew Johnson"
  - subject: "Re: Miguel..."
    date: 2001-09-07
    body: "> The only GTK app I use is the first one: gimp (ok and mozilla and vlc/xine).\n\nWow, great argument. ;)\nI used a Gnome desktop for several months without ANY Qt or KDE app at all and I really liked it. I was never able to run a desktop without Gtk apps (yet) so KDE always felt incomplete to me.\nGimp, Xmms, Loki Installer, Debian Tools, XQF (Quake browser), etc. All those little tools are writtin in Gtk. There is nothing left in the Gtk world, Qt/KDE is/was not quite there (yet)."
    author: "Spark"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "And I've used KDE 2.1/2.2 for the last 6 months without having even having Gtk app. \n\nGimp->krayon (yeah, I know, krayon sucks. but the next version of gimp will remove all gtk dependencies, so the kimp project can be revived).\nxmms->noatun OR kaboodble (noatun and aRts is a much more featured filled program than xmms will ever be).\nLoki Installer->This has Gtk+ STATICALLY linked. \nDebian Tools- because you use the gnome frontends to them maybe? try kpackage, it's a lot more elegant (imho) anyways. \nxqf- never heard.. pretty much a niche program I guess"
    author: "John Martin"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "> Gimp->krayon (yeah, I know, krayon sucks. but the next version of gimp will remove all gtk dependencies, so the kimp project can be revived).\n\nYep, this might change in the future (I hope so) but *currently*, the Gimp is the only real choice.\n\n> xmms->noatun OR kaboodble (noatun and aRts is a much more featured filled program than xmms will ever be).\n\nHmm well. I'm using noatun currently and it's not bad. I don't know if aRts is used, I always had problems with aRts and disabled it, but it runs pretty fast now. Maybe it's disabled or has improved a lot. But I don't really need a sound server...\nGranted, noatun is a nice replacement for xmms. It lacks skins, but I hide it all the time, that's quite neat. The dockbar icon does everything I need. I miss a possibilty to change the volume though. It would also be neat if I could change the volume by using the scrollwheel above the noatun dock item (this works with xmms).\n\n> Loki Installer->This has Gtk+ STATICALLY linked. \n\nSo what?\n\n> Debian Tools- because you use the gnome frontends to them maybe? try kpackage, it's a lot more elegant (imho) anyways. \n\nElegant thant WHAT? You don't even give a name but you know it's more elegant? :)\nFact is, that Deity-Gtk is THE graphical APT frontend in development currently.\nBut I don't use an APT frontend, so it doesn't matter.\nThere is also the Gtk frontend to debconf and the progeny configlets for the Gnome Control Center. But I figured, that I don't need them. It's not that bad.\n\n> xqf- never heard.. pretty much a niche program I guess\n\nYeah, that's the point... all those little \"niche\" programs I found were for Gtk. I couldn't live without a good quake server browser for example.\nThe only reason I don't need this yet is, that I don't have NVidia drivers running on my Debian box (where KDE is installed).\nIt seems to get better. \nI also noticed, that there are less \"Gnome developers should stop working for Gnome and develop for KDE\" trolls lately. I guess that's because they are not so frustrated anymore, that KDE lacks applications. ;)"
    author: "Spark"
  - subject: "Re: Miguel..."
    date: 2001-09-09
    body: "> > xmms->noatun OR kaboodble (noatun and aRts is a much more featured filled program than xmms will ever be).\n\n> Hmm well. I'm using noatun currently and it's not bad. I don't know if aRts is used, I always had problems with aRts and disabled it, but it runs pretty fast now. Maybe it's disabled or has improved a lot. But I don't really need a sound server...\n Granted, noatun is a nice replacement for xmms. It lacks skins, but I hide it all the time, that's quite neat. The dockbar icon does everything I need. I miss a possibilty to change the volume though. It would also be neat if I could change the volume by using the scrollwheel above the noatun dock item (this works with xmms).\n\nNoatun lacks skins? It's skin support is in fact a LOT better than xmms's. Kjofol and Kaiman skins are so much better than Winamp style skins (simply because they can be shaped in so many ways possible. In fact, Kjofol was the choice mp3 player of skinners for nearly two years before AOL/nullsoft/netscape bought the programmer out ;p. \n\nAlso, I suppose you could use the winskin noatun plugin in noatun-cvs. It lets you use all winamp skins in noatun. \n\n> > xqf- never heard.. pretty much a niche program I guess\n\n> Yeah, that's the point... all those little \"niche\" programs I found were for Gtk. I couldn't live without a good quake server browser for example.\n The only reason I don't need this yet is, that I don't have NVidia drivers running on my Debian box (where KDE is installed).\n\nHeh, I tried xqf just now. It sucks. It is nothing like gamespy or eye. Nuff said. \n\n\n> I also noticed, that there are less \"Gnome developers should stop working for Gnome and develop for KDE\" trolls lately. I guess that's because they are not so frustrated anymore, that KDE lacks applications. ;)\n\nWell, kde 1.0.x, kde 1.1.x had more apps than gnome 1.0.x\nand gnome 1.2.x had more apps than kde 1.2.x and kde 2.0.x\n\nNow from what I've noticed, kde 2.1.x and kde 2.2.x have had a increasing number of apps compared to gnome 1.4.x.\n\nResults? About tied right now I think. Name a few more gtk+ \"niche\" apps that do not equivalents in kde or qt. I really can't think of any others. Exclude distro apps, because each distro picks the toolkit they use to build config tools with. Afaik, SuSE, Caldera, TurboLinux, and Mandrake use qt/KDE, while currently, redhat and debian (debian and progeny) use gtk+/GNOME. \n\n\n\n > > Loki Installer->This has Gtk+ STATICALLY linked. \n > So what?\n\nThe point I was trying to make it that you do not need to have gtk+ installed to run these.\n\n\n\n> > Debian Tools- because you use the gnome frontends to them maybe? try kpackage, it's a lot more elegant (imho) anyways. \n\n>  Elegant thant WHAT? You don't even give a name but you know it's more elegant? :)\n\nI was comparing it to gnome-apt, deity-gtk, stormpkg (any apt frontends that work in X11). Out of these, gnome-apt is abhored by even the diehard gnome fanatics, deity-gtk is not completely stable nor completely finished, and stormpkg showed promise, but hasn't been updated in nearly 8 months). \n\n> Fact is, that Deity-Gtk is THE graphical APT frontend in development currently.  \n\nNo, Deity is. Deity-Gtk is just a module for it. There might never be a qt/kde module for it. Why? Because there is already kpackage. There needed to be a deity-gtk because Gnomeapt was not good.\n\n> But I don't use an APT frontend, so it doesn't matter.\n\nNeither do I.\n\n> There is also the Gtk frontend to debconf and the progeny configlets for the Gnome Control Center. But I figured, that I don't need them. It's not that bad.\n\nI've tried the gtk (or was it GNOME) frontend to debconf. It seemed pretty buggy at that time (seemed more like a toy/demo). Of course, that was a few weeks ago, and so it might have changed. I think most users will continue to use something like the slang frontend. \n\nWell,"
    author: "John Martin"
  - subject: "Re: Miguel..."
    date: 2001-09-09
    body: "I've been listening to this discussion for a while, so I'll put in my 2 cents. I am a long time GNOME user, but I've installed and tried almost every KDE version since 1.2. KDE 1.x felt too windows-like for me to use and KDE 2.0 and (to less of a degree, KDE 2.1), didn't \"feel\" right. Although I've long wavered between GNOME and KDE (and ultimatly chosen GNOME every time), KDE 2.2 made me switch completely to KDE. As for this thread, here goes.\n\n1)  Noatun has become an amazing program. Previously, it was either incomplete (in the form of Kaiman in KDE 2.0), or buggy, (in the form of the noatun in kde 2.1). Now that most of the bugs in noatun and aRtsd have been worked out, I think it's great. Noatun and aRtsd are definatly something that GNOME is missing. aRts isn't a simple soundserver like esound, it is a complete environment to itself. I heard GNOME 2.0 might be adopting aRtsd, so there will hopefully a frontend to aRts for GNOME. And yes, Noatun's kjofol skins support is great. Kjofol skins are just _awesome_. Xmms has a kjofol vis. plugin, but the default winamp-like interface is shown, so It does not integrate as well as the Kjofol support in noatun.\n\n2. Yeah, xqf isn't a great app. However, it works. KDE definatly needs a game server browser or something like gamespy or EyE for windows. Any kde developers listening? ;). Even a simple qstat frontend like xqf would _work_, but I tend to avoid xqf.\n\n3. I also agree with you that the number of KDE programs has increased a lot with KDE 2.1 and KDE 2.2. I also think that there is way too much duplication in program efforts in gtk+ and/or gnome apps. There is almost two seperate apps that do almost the same thing. \n\nThis is after the GNOME 1.2 days when there seemed to be a huge lead in gtk+ programs. Except for licq, I don't think I used any qt or kde apps at all then. I could have even probably deinstalled qt and kdelibs. But now, I think that the situation has *almost* completely reversed. I don't think i've used any gtk+ apps in a regular basis in the last month since I switched to KDE.\n\n4. I don't use Debian, so I don't have knowledge of those programs, but I also use kpackage(for rpms). In fact, I didn't even know kpackage could work with apt. I think that is a positive feature to be transparent like that :). Also, SuSE(my current distro) provides excellent configuration/updating tools in the KDE Control Panel that are similiar to the various ximian tools.\n\nSo, for right now, KDE is my DE of choice. But I will try and test GNOME 2.0, just like I tried all the versions of KDE. \n\n-Shane"
    author: "Shane Colson"
  - subject: "Knome versus GDE"
    date: 2001-09-09
    body: "1.) Well, I think I have to agree on this one... Just tried the skins and... yes, pretty damn nice. :) The last time I tried it, it was \"Kaiman\" and that has to be the most ugly media player I've ever seen. Nice to see it's gone. :) \nI also found out, that I can place a kmix icon in the dock and that does adjust the volume using the scrollwheel. :) Yay!\n\n2.)  XQF is nice! I don't need much more. However, I don't know, if I could come up with more examples... it's just that I searched for something like that and the only nice thing I found was a Gtk app...  that was the case always. Loki installer is still Gtk, even if statically linked... Mandrake uses Gtk for it's config tools (most of them), although they started out as a Red Hat clone with KDE...\nWhatever, I figured that I don't really need much apps...\nI even didn't miss anything important in BeOS. :)\nI think KDE 2.2 has come a long way... it is also currently looking amazingly outstanding. The Liquid style is very nice. :) Now if Linux wouldn't have such a crappy hardware support (yeah, it lacks binary compatible drivers) and I could get my USB mouse and the newest NVidia driver to work (the one with mousecursor shadow!) this would be a great OS.\n\n4.) Yes, SuSE is doing so but who cares? YAST is somewhat proprietory, so nobody else can use it. And no, I won't buy SuSE just because of this... in fact this is the reason, while I'll NEVER AGAIN buy SuSE (I did way too often).\nXST is free and it works. At least on Red Hat and on Debian. Sure, it's not there yet, but it could if everybody would help (distributors could finish the parts for their distro, add new modules, etc) and it really needs a KDE frontend.\n\n5.) Gnome still rocks. :)\nLike BeOS, AtheOS, Enlightenment and sometimes even Windows 2000 do. \nI think one of the main reasons why we get so much \"I used XY for the last month, but version x.x of YZ has totally convinced me!\" messages is, that people often get bored of something and are very excited about a change. That's a good thing.\nWe need diversity. :)\nLike we need different languages. What a big waste of resources... but sometimes different languages are fun. :)\nAnd at the end, this is all that counts.\nWhat is all this \"money\" and \"success\" for, if not to have some fun?\n\nYeah, I'm tired. Yawn. Seeya!\n\n./Spark"
    author: "Spark"
  - subject: "Re: Miguel..."
    date: 2001-09-08
    body: "And I've used KDE 2.1/2.2 for the last 6 months without having even having Gtk app. \n\nGimp->krayon (yeah, I know, krayon sucks. but the next version of gimp will remove all gtk dependencies, so the kimp project can be revived).\nxmms->noatun OR kaboodble (noatun and aRts is a much more featured filled program than xmms will ever be).\nLoki Installer->This has Gtk+ STATICALLY linked. \nDebian Tools- because you use the gnome frontends to them maybe? try kpackage, it's a lot more elegant (imho) anyways. \nxqf- never heard.. pretty much a niche program I guess"
    author: "John Martin"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-06
    body: ">That's way I said you peoples just keep masturbating.\n\nHere class, we have an example of what makes the difference between a suggestion and a troll. That is, a suggestion is giving a reccomendation polietly, and realizing that you aren't the definitive source of sagely KDE advice. A troll is insulting the developers because they aren't doing things the way you'd like them too, whether that be how they code or how they PR."
    author: "Carbon"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-07
    body: "Gee.\nI am not even suggesting anything and I don't want persuade anybody.\nI just want to tell you guys the simple fact that all these great things about KDE that you guys keep repeating : \"The leading linux desktop\" or \"The best open source project\" is only well known among KDE peoples or maybe Linux users. But what about outside Linux users which is represnted in the mainstream media? Gnome, Ximian and Icaza is the representative of Linux/Open source."
    author: "theman"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-07
    body: ">I am not even suggesting anything and I don't want persuade anybody. I just want to tell you guys the simple fact that ... Gnome, Ximian and Icaza is the representative of Linux/Open source.\n\nIf you aren't suggesting antyhing and don't want to persuade anybody, then why are you trying to pass of a bit of dogmatic information (that also happens to entirely support your arguments) as fact?"
    author: "Carbon"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-07
    body: "Sorry, \nfor me the representatives seem to be:\n\nFor Linux: Linus Torvalds, who's friendly when it comes to interviews and behaves exact the right way to make others trust in Linux.\n\nFor Open Source: well known Mr. Stallman of FSF, who advertises himself and the FSF by representing aggressive opinions in the public press.\n\nFor PR: the CEO's of all major Linux distributors like SuSE, Debian, Mandrake and RedHat + the press related persons of companies getting more and more involved into Linux/OpenSource like IBM, HP, and Sun.\n\nBesides: None of the major companies (including Sun) would ever tie itself to only one dektop. For me it seems, that de Icaza is spending to much time in convincing these companies. This way he gets addicted to them and looses his independence. But he gets nothing back. Even Sun will _never_ tie itself to the ideas of Mr. Icaza.\n\nWhen it comes to Gnome it is 'de Icaza' and Ximian. Both are _not_ official representatives of the Gnome project and I doubt, they always behave like the free, non-paid Gnome developers want them to.\n\nJust my 0.02 euro"
    author: "Thomas"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-08
    body: "Hmm, did you mean to post this to the comment I replied to, as opposed to mine?"
    author: "Carbon"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-08
    body: "no, sorry, it was meant as reply to theman.\nIt was just the wrong [reply-to] link"
    author: "Thomas"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-07
    body: "I think you misunderstood what I said or u just simply take my words out of the context.\nWhat I really said is that for people outside linux which is represented by the mainstream media, Icaza, gnome or his Ximian is the representative of Linux/Open Source. Just read some edition of time magazine couple months ago or read Washington post articles, or watch some holywood movie like 'antitrust', you saw him there. All of them talk about Gnome or Icaza as an Open source leader.\nMeanwhile these KDE peoples keep repeating that they are \"the leading Linux desktop\", but nobody outside linux will think KDE when talking about linux desktop. They are talking about gnome since the popular media mention it."
    author: "theman"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-07
    body: "People who are interested in desktop computing read standard Computer-mags to find valuable information. Compare the amount of KDE-related articles to the ones related to ''de Icaza''. \nAnd whoops... the project ''KDE'' seems to get a lot more attention than Mr. Icaza. How can that be, if that man even jumps on the screen of your local cinema...?"
    author: "Thomas"
  - subject: "Re: You should follow Del Caza dan XIMIAN"
    date: 2001-09-08
    body: "To be nitpicky, Antitrust does not mention Icaza or Gnome at all, it just shows it on a few computer screens in passing, without actually mentioning it. Ha, told ya so! ;-)\n\nHmm, seriously though, I really am not sure as to the importance of this. Almsot every distro includes both KDE and GNOME, and they don't have to be gotten ahold of seperately, so even introductory OSSOS (Open Source Software Operating System :-) users who may have never heard of KDE (or even GNOME) before, will have an oppurunity to try both. \n\nIn a loosely related way, this is why you never often see M$ ads for Windows itself. Though they often advertise upgrade package, Office, their set-top box, and X-Box like crazy, non-PC specific audiences (like people who watch TV, or read Time) aren't told to buy Windows itself. \n\nThis is because (unfortunately) Windows is bundled with almost every single desktop PC sold, they don't need to advertise it, they can rely on the ads of the computer companies to sell systems with Windows on it. In the same way, a user can hear about GNOME and not at all about KDE, but they are likely to get a chance to use both (along with Enlightenment, WindowStep, etc) when they grab a distro.\nOnce the user tries Linux (or BSD), they will almost certainly try everything a little bit, since at this point, PC tinkerers are the most likely non-programmer audience to buy a desktop distro. Then, once they make a choice, they will get news about their"
    author: "Carbon"
  - subject: "Re: About KDE in Antitrust"
    date: 2003-02-15
    body: "When Milo does a \"ps\" on the console you can see a process called \"kpanel\" running as root. isnt that kde?\n\nok, everywhere else you can find gnome desktops running."
    author: "SuD"
  - subject: "charles...."
    date: 2001-09-06
    body: "CUT YOUR HAIR!!!\n\nThat's where all the gnomes are hiding!"
    author: "me"
  - subject: "Konqi"
    date: 2001-09-06
    body: "So where I can buy a stuffed Konqi? Who made it for you?"
    author: "Sam"
  - subject: "Re: Konqi"
    date: 2001-09-06
    body: "Have a look at http://www.freibergnet.de/."
    author: "Michael H\u00e4ckel"
  - subject: "out of contex (maybe)"
    date: 2001-09-06
    body: "Well this is probaply been discuised before, but why it seems \nthat ximian/gnome is getting all the atention outside linux-centric \npress. Even tho it has not even reached the same technical merits as kde?\n\nIs it besauce gnome foundation? + bigest linux shop redhat is supporting it or\nmaybe besauce i dont know what?\n \nshouldnt be suprise anyway, since when the quality of a product has been the key to sucses, well never. Its all about relations and marketing.\n\nsad but true"
    author: "anon3"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-06
    body: "GNOME getting more attention? Yeah right!\nI think KDE gets a lot more attention than GNOME does.\nLook at all the rewards. Look at all the users.\nLook at all the \"GNOME sucks, KDE rules!\" trolls!\nI'd say KDE is a lot more popular than GNOME."
    author: "Stof"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-06
    body: "I agree"
    author: "Robin Sharf"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-07
    body: "in the end, does it realy matter who is getting more industry press?\nall of the distros that are making a desktop centric product use KDE, most that are making a server/workstation centric product use gnome, the ones that wanted true freedom at a time when thier was a big diffrence between KDE and Gnome use Gnome. the Unix vendors choose Gnome, the desktop ISVs choose KDE, I see very little competition between the two, one is good for grandma, the other is good for the admins and engineers. that is how I predict the two will eventualy develope into, KDE=desktop pc, Gnome=server/workstation.\n\nand to mae it even better, the engineer can come home, and work in the environment he uses at work(Gnome) while his wife and kids can work in KDE when they are doing normal home PC stuff.\n\nnoif we can just get a common clipboard so we can cut/paste from a GTK+ app to a QT app, then Life will be perfect, almost >:)"
    author: "Jeremy Petzold"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-07
    body: "Doesn't both GTK+ and QT use X selections?\nI can cut & paste between KDE apps and GNOME apps just fine."
    author: "Stof"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-08
    body: "There's much more to life than plain text :)"
    author: "Matt"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-08
    body: "You can copy & paste images between KDE apps?\nI've never seen a program that can do that."
    author: "Stof"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-08
    body: "Yes, you can. I just did from Kpaint to Kword."
    author: "Marko Samastur"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-08
    body: "I can't even copy and paste within KPaint itself..."
    author: "Stof"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-08
    body: "Edit->copy\ngoto kword, edit->paste\n\nthis is the standard way to copy/paste across most platforms, if you did not know."
    author: "John Martin"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-09
    body: "1. I start KPaint.\n2. I draw something.\n3. I select something using the Selection tool.\n4. Edit->Copy    => the selection disappears.\n5. Edit->Paste   => Nothing happens."
    author: "Stof"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-09
    body: "6. I close KPaint  => it segfaults.\nKDE apps seem to crash sometimes at shutdown when I start them in a non-KDE environment."
    author: "Stof"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-09
    body: "Instead of your bitching and trolling, please submit a report to bugs.kde.org.\nThat's a lot more constructive.\n\nThanks."
    author: "Gary Storla"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-09
    body: "Bitching and trolling you say? What I just said are FACTS!\nThis is MY computer, and you have no right to tell me what problem I do and do not have!\nI don't care what you call it, the fact is that the problem still exists.\n\nBesides, sending a bugfix now is useless. I'm still using KDE 2.1.1, and I can't upgrade to 2.2 until I get Debian installed.\nIf the bug has been fixed in 2.2, then I would have send a duplicate bug.\nIs that what you want me to do?"
    author: "Stof"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-09
    body: "Just tried the Ximian Gnome, it crashed so many time. Especially the red carpet doorman and Evolution. What I also don't like about it is that it takes over all my mandrake menus. Now I can't see KDE or Manrdrake 8.0 menus in Ximian gnome. I have to type in program name in console if I now their name.\nAnd I should say that ximian Gnome is pretty desktop. They paid professionals to do designing and they do a great job. KDE can learn something from them.\nBUT overall, I still like my KDE2.2 much better, though. Look forward to KDE2.2.1 for the bug fixed version."
    author: "hind"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-09
    body: "Then use KDE. It's your computer, so you decide."
    author: "Stof"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-09
    body: "Apparently that problem does not exist in KDE 2.2. So please upgrade before a whole thread pops up :)."
    author: "Gary Storla"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-09
    body: "Yes, you can copy/paste many types of mimetypes."
    author: "Gary Storla"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-08
    body: "If what you say is true then in the near future we will see some KDE engineers / programmers do KDE programming in Gnome..:)\nThe truth is that both desktop (or even all Linux distros) want to make it right for every type of users. That's way you have choice when you installing linux which packages you want to install. It is just user preference that make it difference what kind desktop they use. And these users include engineers too. There is evidence that Gnome is prefered by engineers and KDE by home users."
    author: "jamal"
  - subject: "A Correction"
    date: 2001-09-08
    body: ">> There is evidence ...\n\nShould be \"There is NO evidence..\""
    author: "jamal"
  - subject: "Re: out of contex (maybe)"
    date: 2001-09-08
    body: "I currently have two GNU/Linux systems installed:\nRedHat with Gnome and\nDebian with KDE.\n\nI use RedHat/Gnome to play games, etc and Debian with KDE to code.\n\nIMO the RedHat/Gnome system is much simpler to use, looks better and is just \"cool\". ;) \nThe Debian/KDE System is way more complicated, but it does everything I want. APT is not as easy as Red-Carpet, but it works much better and reliable. And the KDE desktop is not as easy to use as Gnome (yeah really!), but  it just works and KDevelop seems to be a quite powerfull toy.\n\nIMHO KDE is currently ahead in the \"does the job\" department and is quite polished. But Gnome is ahead in usability and looks. Both should learn from each other."
    author: "Spark"
---
Rob Kaper and myself have collaborated on a summary of the happenings at last week's <a href="http://www.linuxworldexpo.com/">LinuxWorld Expo</a>.  We tell you what <em>really</em> took place at the most fabulous booth at the show &lt;grin&gt;.  And don't forget to check out Rob's extended and hilarious <a href="http://jadzia.nl.capsi.com/~cap/digicam/2001-09-01-lwce/">picture gallery</a> of the event!



<!--break-->
<p>
<h3 align="center">KDE Report: LinuxWorld Conference and Expo 2001</h3>
<div align="center">
<strong>
KDE Project Shows Off KDE 2.2 and KOffice 1.1 at LWE 2001 (San Francisco, CA)</strong></br>
Rob Kaper and Andreas Pour</br>
September 4, 2001</br>
San Francisco, CA, USA</br>
</div>
<p>
The KDE Project used the occasion of
the <a href="http://www.linuxworldexpo.com/">LinuxWorld Expo</a> in San
Francisco to show off the most recent release of the <a
href="http://www.kde.org/">K Desktop Environment</a>, <a
href="http://www.kde.org/announcements/announce-2.2.html">KDE 2.2</a>. The
Expo was also the perfect place to announce and demonstrate the new <a
href="http://www.kde.org/announcements/koffice-1.1.html">KOffice 1.1</a>,
the KDE office and productivity suite.</p>
<p>
KDE was represented by Bay Area locals Jim Blomo, Jason Katz-Brown and
Charles Samuels as well as Bohemians Waldo Bastian, Kurt Granroth, Rob
Kaper, Andreas Pour and Chris Schl&auml;ger. Also present at the KDE booth with
the best of intentions were Paul Campbell, Bill Huey and Eunice Kim.</p>
<p>
One of the highlights of the event was the <a
href="http://dot.kde.org/999120705/">announcement</a> that KDE had won the
LWE Excellence Award for Best Open Source Project. After receiving the
award, it was proudly displayed at the booth by the developers present, who
felt it was great to see appreciation for the combined efforts of the entire
KDE community.</p>
<p>
Over the three days that the exhibition was open, many visitors were seen
at the KDE booth. Most of them were impressed by the demonstrations given.
Most popular were <a href="http://www.konqueror.org/">Konqueror</a>,
<a href="http://noatun.kde.org/">Noatun</A>-plugin Madness, the Internet
keyword architecture, renewed text editor Kate and especially the KIO
slave architecture.</p>
<p>
Demonstrating the kio_audiocd plugin with the flair of a magician
(&quot;see, this is a regular CD&quot;) to rip and encode audio tracks to
MP3 and <a href="http://www.vorbis.com/">Ogg Vorbis</a> almost caused some
visitors to leave the exhibition so they could install KDE on their
computers immediately. Also met with great interest was the mention of the
kio_freenet and kio_sftp slaves for respectively the <a
href="http://freenet.sourceforge.net/">Freenet</a> distributed file network
and secure file transfers using <a
href="http://www.openssh.com/">SSH</a>.</p>
<p>
Interest from the media was also intense.  Andreas was forced to purchase
lozenges to protect himself after the large number of interviews the
KDE League's PR firm arranged for him (thanks Eunice!).  Earlier in the
week Andreas took part in a live interview and another recorded interview for
<A HREF="http://www.techtv.com/techtv/">TechTV</A>, an international
technology cable channel.</p>
<p>
One of the most frequently asked questions at the booth was the
difference between <a href="http://www.kde.org/">KDE</a> and <a
href="http://www.gnome.org/">GNOME</a>, the two most popular open source
desktops. Most users did not realize
that both projects share the same goals (improving UNIX usability) while
approaching them from a different technical point-of-view. Despite popular
belief, the <a
href="http://jadzia.nl.capsi.com/~cap/digicam/2001-09-01-lwce/sanfrancisco-154.jpg">KDE
developers and GNOME developers</a> did not engage in WWIII but instead met
under friendly conditions.  Andreas also met with Nat Friedman from
<a href="http://www.ximian.com/">Ximian</a> in an effort to improve
relations and to discuss ways that KDE and GNOME can work together to make
Open Source more attractive to computer users.  The meeting went very
well and concrete actions to improve KDE/GNOME interoperability were
discussed.  Some of these ideas were already planned in a KDE/GNOME hackfest
to be held at the <a href="http://www.usenix.org/events/xfree86/">XFree
Technical Conference</a> this November under the umbrella of
<a href="http://www.xfree86.org/~keithp/">Keith Packard</a> of the
<a href="http://www.xfree86.org/">XFree86 Project</a>.</p>
<p>
Some other frequently heard requests were for CD's with KDE installed
and whether MieTerra's <a href="http://www.kde.com/kdepr/lwe-sf-2001/sanfrancisco-114.jpg">big stuffed Konqi</a> could be given away. For various
reasons the KDE team could not comply, but were able to point to a page
on the <a href="http://www.kde.org/">KDE website</a> with <a
href="http://www.kde.org/cdrom.html">third part CD-ROM resellers</a> and <a
href="http://www.kde.org/kde-stuff.html">KDE merchandise</a>. It should also
be mentioned that most Linux distributions ship with KDE, many of which have
it as the preferred or default desktop environment. We would like to
specifically mention both <a href="http://www.suse.de/">SuSE</a> and <a
href="http://www.mandrake.com/">Mandrake</a> and thank them for the hardware
they contributed for the KDE booth. Another thank you goes to the <a
href="http://www.kdeleague.org/">KDE League</a> for providing food for the
developers in attendance.
</p>
<p>
A photo impression of the event and the KDE booth made by Rob Kaper is available <a href="http://jadzia.nl.capsi.com/~cap/digicam/2001-09-01-lwce/">here</a>.</p>


