---
title: "Report on London Linux Expo"
date:    2001-07-12
authors:
  - "numanee"
slug:    report-london-linux-expo
comments:
  - subject: "Inside joke"
    date: 2001-07-12
    body: "For those who don't want to spend a while trying to find the joke, look at the picture of annma (near the bottom). On the wall there's a piece of paper which says 'KIllustrator', but that's crossed out and written below it is 'KPhotoShop'.\n\nDo I get a prize? :)"
    author: "David G. Watson"
  - subject: "database tool"
    date: 2001-07-12
    body: "How is the work on rekall going? the article says that a lot of companies want a simulair tool."
    author: "Jasper"
  - subject: "Re: database tool"
    date: 2001-07-12
    body: "Rekall is coming along nicely, we have another beta with the Python scripting active that should come out next week.  Final release should be the first of October."
    author: "Shawn Gordon"
  - subject: "KDE merchandise?"
    date: 2001-07-12
    body: "Hey, does KDE have any cool things to buy?  I'd like a stuffed KDE dragon, thank you.\n<p>\nFrom Linux.com's photo of the day archive:\n<a href=\"http://www.linux.com/interact/potd.phtml?potd_id=105\">http://www.linux.com/interact/potd.phtml?potd_id=105</a>"
    author: "Justin"
  - subject: "Re: KDE merchandise?"
    date: 2001-07-12
    body: "Go to http://www.kde.org, click \"Eveything else\" and \"KDE Merchandise\"."
    author: "Per"
  - subject: "Database integrated in KOffice"
    date: 2001-07-12
    body: "Feedback:\n> What people want the most is a database integrated in KOffice. People with small businesses in particular would like to make the move to Linux/KDE but they need a database tool.\n\nVery important, yes... even a like-integrated database..."
    author: "Alain"
  - subject: "Re: Database integrated in KOffice"
    date: 2001-07-13
    body: "Perhaps one could be quickly put together in the following way:\n<p>\n<ul>\n<li>many linux distros come with mysql or postgresql</li>\n<li>perhaps we just need to write a nice pretty front end in QT - that way all the hard stuff is already taken care of.</li>\n</ul>\n<p>\nJust a thought.  Maybe if I ever have time I'll start to do something about it."
    author: "Seth Davenport"
  - subject: "Rekall, Killustrator, KPlato"
    date: 2001-07-13
    body: "I see in a previous thread that there is the Rekall project.\n\nIt is explain here  :\nhttp://www.thekompany.com/projects/rekall/?dhtml_ok=0\nhttp://linuxpr.com/releases/3575.html\n\nBut nothing is said on the Koffice site... However Shawn Gordon says here \"Final release should be the first of October\"\n\nWill it be integrated into KOffice ? (not 1.0, of course, but after ?)\n\nIn www.koffice.org, I see that Killustrator has not still a new name, I hope it is not going to give some lateness for KOffice 1.0...\n\nAnd I see KPlato... I think I never saw it before (?)...  Good ! Ready for KOffice 1.0 ?"
    author: "Alain"
  - subject: "Re: Rekall, Killustrator, KPlato"
    date: 2001-07-13
    body: "Actually, the next release of KOffice will be 1.1.  It was already released as 1.0 some time ago, although it was a mistake.  None of the apps were really useable for anything at that time and they crashed a lot.  The 1.1beta1 that was released was actually much more stable and useful than the 1.0 release (especially KWord)."
    author: "not me"
  - subject: "Re: Report on London Linux Expo"
    date: 2001-07-12
    body: "One thing I would've loved from the KDE stand is a CD with the latest stable/devel tar balls (and maybe RPMS).\n<BR>\nNothing fancy, just a CDR to save me the hours of downloads that I've just done!\n<P>"
    author: "Darren Poulson"
  - subject: "The Problem with KDE distribuition !"
    date: 2001-07-13
    body: "You know, KDE people keeps us stuck to older versions of KDE by their kind actios ;)\n\n1. Not giving out upgrades like windows' IE upgrades for Konq and other imp. tools/app.\n\n2. There must be KDE-Installer to install KDE upgrades and applications for a distribution-free binary upgrades.\n\nI know you would say that KDE works on Unices and not just linux but if you look at Netscape, Adobe Acrobat, Word Perfect, StarOffice and other commercial apps, they just work fine on Unices including Linux-es. Here we are just forced to get Mandrake RPMS!!! Oh for God Sake get us Distribution free KDE upgrades."
    author: "Asif Ali Rizwaan"
  - subject: "Re: The Problem with KDE distribuition !"
    date: 2001-07-13
    body: "You pay for the huge amount of time it takes to create such binaries, and then YOU use them: they are slower.\n\nYou are using a distribution. A distribution\u00b4s main job is to get you the software we produce nicely packaged and optimized for you in a convenient and speedy fashion.\n\nIf your distribution is not doing that for you: change distribution to one that does. I repeat: THAT IS THE DISTRIBUTION\u00b4S JOB. The unspoken social contract between distros and developers includes a tiny clause saying \"you distro guys take care of making the packages for your distros once many users use the things we write\".\n\nBut the KDE project is not going to waste time making universal binaries: days have only 28 hours!\n\nOn the other hand: sure, netscape produces binaries that work on all distros... except they don\u00b4t, they crash in some because of incompatible libraries. And even they only produce such beasts about twice a year. KDE produces way more frecuent releases.\n\nWordperfect costs a few hundred bucks: pay me what WP costs, and I will hand-compile all of KDE and a bunch of other apps to be perfectly optimized for your computer ;-)"
    author: "Roberto Alsina"
  - subject: "Re: The Problem with KDE distribuition !"
    date: 2001-07-13
    body: "YES!\n\nThis is needed - like the Ximian setup tools for Gnome, KDE needs some standard configuration tools. I think it's a bit stupid that every distribution makes their own tools. \n\nIt would be much easier for the users if the same configurations tools were available no matter what distribution you use or if you change to another distribution.\n\n==========\n\nIt is said here :\nhttp://dot.kde.org/989997858/990014513/"
    author: "Alain"
  - subject: "Re: The Problem with KDE distribuition !"
    date: 2001-07-13
    body: "Blah.\n\nConfiguration tools should not be desktop-specific.\n\nFurther, configuration tools should not require a graphical environment. What can a config tool need that can not be done in a slang app?\n\nI can\u00b4t think of anything, and I would much prefer such tool to work over ssh from a windows box without a X server, really."
    author: "Roberto Alsina"
  - subject: "Re: The Problem with KDE distribuition !"
    date: 2001-07-13
    body: "yes!!! i agree totally"
    author: "Rick Kreikebaum"
  - subject: "Re: The Problem with KDE distribuition !"
    date: 2001-07-14
    body: "look at sysconfig module in kde cvs. \nIt is a kde front end to ximian\nsetup tools. \n\nThese setup tools look like they\nmight end up pretty universal - \neg kde, gnome, web and terminal \ninterface to them are easily possible.\nAlso they sould be presented as \na corba object or a web service with soap. \n(making sure of security, of course). \n\nRob"
    author: "Rob"
  - subject: "Re: Report on London Linux Expo"
    date: 2001-07-14
    body: "I found the London Linux Expo rather disappointing.\nI hope it was a success for KDE and the other exhibitors. But for the many visitors it was just too small.\nNeither Trolltech nor RedHat were present.\nIt didn't surprise me really. The Birmingham Expo (which will hopefully better)is just in two months time and the LinuxTag had been almost in parallel.\nThe best thing was \"The Great Linux Debate\". John Terpstra from Caldera was just brilliant. \nGood one!"
    author: "Michael"
  - subject: "Re: Report on London Linux Expo"
    date: 2001-07-15
    body: "The lack of exhibitors was rather limited, and it was much smaller.\n\nI am organising the KDE presence at the Birminham expo so expect to see us there as well. :-)"
    author: "Jono"
---
While most headed off to the LinuxTag event, a handful of brave KDE people decided to tackle the <a href="http://www.linuxexpo.co.uk/">London Linux Expo</a>.  Read Anne-Marie's <a href="http://women.kde.org/events/london_expo/expo.html">entertaining account</a> for some of the great stuff that went on there -- including photos of the bar encounter with a friendly gnome. Also check out Lee's site for <a href="http://duracell.dyndns.org/photos/expo2001-london/">more photos</a>. Many thanks and kudos are due to Giles, Anne-Marie, Oliver, Lee, Jono and Mark who staffed the booth (and did an excellent job), our sponsors, and anyone else who helped, including: the <a href="http://www.kdeleague.org/">KDE League</a>, <A href="http://www.suse.co.uk/">SuSE UK</a>, Nick Veicht from <a href="http://www.linuxformat.co.uk/">Linux Format</a>, and <a href="http://www.cheeplinux.com/">Cheep Linux</a>. Oh, and bonus points if you can <a href="http://women.kde.org/events/london_expo/expo.html">spot</a> the Adobe inside joke.
<!--break-->
