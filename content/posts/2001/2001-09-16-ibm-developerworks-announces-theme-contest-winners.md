---
title: "IBM developerWorks Announces Theme Contest Winners"
date:    2001-09-16
authors:
  - "Dre"
slug:    ibm-developerworks-announces-theme-contest-winners
comments:
  - subject: "Eww"
    date: 2001-09-16
    body: "I really hate to rain on anyone's parade, but all of these themes are grotesque, IMNSHO."
    author: "ac"
  - subject: "Re: Eww"
    date: 2001-09-16
    body: "I'm not saying I could do any better, but I know I couldn't do any worse.\n\nI guess I should have entered the contest.  All I would have had to do\nwas take a basic KDE theme, make it BUTT UGLY and submit it, thus\nwinning myself a few thousand dollars. =)"
    author: "A. C."
  - subject: "Re: Eww"
    date: 2001-09-16
    body: "Well done IBM for sponsoring this and for donating money to open-source projects. Well done to the participants for having a go. However, if the aim was to create something either original or beautiful, then it has failed. Either there weren't many participants or the judges are colourblind (and are not aware of the existing range of KDE themes :-)\n\nAre there any *original* themes out there? Something tasteful yet functional. I haven't seen too many KDE users using anything but the default theme (which looks bloody beautiful, thanks to Mosfet - a coder with artistic talent).\n\n- Sertorius"
    author: "Sertorius"
  - subject: "Re: Eww"
    date: 2001-09-16
    body: "Yeah, you're right, Mosfet rules!"
    author: "reihal"
  - subject: "Re: Eww"
    date: 2001-09-16
    body: "Yeah, his liquid theme is soooooooooooooooo much better than all of those!"
    author: "dfgsfg"
  - subject: "Re: Eww"
    date: 2001-09-17
    body: "Well... that's cause liquid has it's own widget style, which actually recodes parts of the UI drawing system, as opposed to just setting pixmaps and colors to things."
    author: "Carbon"
  - subject: "Re: Eww"
    date: 2001-09-17
    body: "I remember seeing a Widget Designer on Mosfet's site a long time ago.\nThere was a screenshot, did anyone save this?\nMaybe Mosfet himself could tell us about it?\nWhere are you, man?"
    author: "reihal"
  - subject: "Re: Eww"
    date: 2001-09-18
    body: "> ...liquid theme is soooooooooooooooo much better than all of those!\n\nWell, it's your opinion, and I deserve it.\nAnd I think that Liquid sucks, in all its variants.\nAt least in its current state - may be in one year it will become better :-)\n\nBR,\n\nVadim Plessky"
    author: "Vadim Plessky"
  - subject: "\"originality\" [Re: Eww]"
    date: 2001-09-18
    body: "> ... if the aim was to create something either original or beautiful, then it > has failed. Either there weren't many participants or the judges are colourblind \n\nDo you seriously expect that some good designers will work for $3000, even if money is going into his pocket (not a charity to Open Source project)?\nBesides, I can tell you that good designers just *can't* design themes.\nThey are ok to paint background, but nothing else...\nUser Interface is rather specific thing.\nThere is a limitation on screen size, resolution is pretty low, and many icons have just 16x16 size (hopefully, KDE supports 21x21 as well :-)\nIt's quite difficult in such environment to create something tasteful and working at the same time.\n\nAs about originality:\nMay I ask you *where* you have seen something similar to my Egypt Office theme?\nI doubt you have ever been in Egypt. Taking into consideration current political situation in the Middle East, I doubt you will visit Egypt in nearest future. Colors are original, window decorations - original. layout - original.\nI hope you have also tried complimentary Widget theme - at least Egypt widgets (not Egypt Office, but..) are available  on my web site, and on apps.kde.com\n\n\nBest Regards,\n\nVadim Plessky\nhttp://kde2.newmail.ru"
    author: "Vadim Plessky"
  - subject: "Re: \"originality\" [Re: Eww]"
    date: 2001-09-19
    body: "Going by the screenshot on the contest web site, I see only 3 of the 5 buttons on the top are original, making it look inconsistent, plus the scrollbars etc. look like they've been pinched from the SGI theme, just with a different colour. Likewise, apart from the colour, the panel is exactly the same as the standard one. The background is just the default KDE gradent... so from this it appears that the theme is just 3 new buttons and some new colours.\n\nIf OTOH you need some other bits to make this look decent then you should have included that in your theme package. If that was against the rules of the contest then it was a pointless contest... at least some open-source project got some funding I guess. I'll try your theme with the widget theme as well.\n\nAs to whether good designers will work for free, look at the fine work of Mosfet... some people (probably true for you as well I'd guess) do it for the love of the work. That's the same reason I write Free software...\n\n- Sertorius"
    author: "Sertorius"
  - subject: "Re: \"originality\" [Re: Eww]"
    date: 2001-09-20
    body: "> Going by the screenshot on the contest web site, I see only 3 of the 5 buttons on the top are original, making it look inconsistent, plus the  scrollbars etc. look like they've been pinched from the SGI theme, just with a different colour. Likewise, apart from the colour, the panel is exactly the same as the standard one. The background is just the default KDE gradent... so from this it appears that the theme is just 3 new buttons and some new colours.\n--\nOk, I see your point. And I agree that screenshot for my theme (Egypt-Office) on IBM site is not good, at all!\nUnfortunately, there was no option to include \"reference\" URL with author's screenshot (in IBM KDE Theme Contest).\nI am going to prepare such page ASAP and post URL here.\nIn the mean time, pls see attached screenshot from my original submission (no changes from May 31st, 2001!)\n\nCheers,\nVadim"
    author: "Vadim Plessky"
  - subject: "First check this, than blaim me!.. [Egypt-Office]"
    date: 2001-09-20
    body: "I think there is a basic misunderstanding between \"what was supplied to the contests\", contest's rules and user's expectations.\nI'd like to clarify at least my work [Egypt-Office] and reasons for my participation.\nFirst of all, look at attached *original* submission.\nAs dor.kde.org doesn't allow to attach two files, I will attach Widgtes theme in second posting, next to one.\nI have to admit here that Widget Themes are much more complex that Window Decorations. I fact, I had to draw/paint around 75 pixmaps doing my theme, comparing to around 15 for Window Decorations.\nSorry that my Egypt Widget Theme is not shown on IBm site. :-((\nReally sorry!\nPlease try this attachement, together with next one, and than start blaiming me!..\nThanks.\n\nYours,\n\nVadim Plessky"
    author: "Vadim Plessky"
  - subject: "Egypt Office - Widget Theme"
    date: 2001-09-20
    body: "Here is a complementary Egypt Office Widget Theme. It should be used *together* with Window Decoration theme from me.\nScreenshot (60k) is attached to my reply to mail from Sertorius.\nSo, you can try screenshot first, and if you like it - download both Widget and Window Decoration themes. \nNote that there are two backgrounds included in my themes (textured dark brown color). If I remember correctly, first one is mapped to Desktop1, second - to Desktop2. So, switch between Desktop1 and Desktop2, and compare backgrounds.\n(I supplied themes on May31st, 2001, so it's more than 3 months left, I don't remember such details...)\n\nThanks for trying my stuff!.. :-)\n\nVadim\n\nP.S. You can find on my web site instructions how to install themes correctly. \nURL: http://kde2.newmail.ru, \"Themes\" section.\nFor people who don't have time to browse my site, here is direct link:\nhttp://kde2.newmail.ru/themes_install.html\n\nHope you willl find \"missing 3 buttons\" in title bar, after all. :-)"
    author: "Vadim Plessky"
  - subject: "Egypt Office - Widget Theme"
    date: 2001-09-20
    body: "Here is a complementary Egypt Office Widget Theme. It should be used *together* with Window Decoration theme from me.\nScreenshot (60k) is attached to my reply to mail from Sertorius.\nSo, you can try screenshot first, and if you like it - download both Widget and Window Decoration themes. \nNote that there are two backgrounds included in my themes (textured dark brown color). If I remember correctly, first one is mapped to Desktop1, second - to Desktop2. So, switch between Desktop1 and Desktop2, and compare backgrounds.\n(I supplied themes on May31st, 2001, so it's more than 3 months left, I don't remember such details...)\n\nThanks for trying my stuff!.. :-)\n\nVadim\n\nP.S. You can find on my web site instructions how to install themes correctly. \nURL: http://kde2.newmail.ru, \"Themes\" section.\nFor people who don't have time to browse my site, here is direct link:\nhttp://kde2.newmail.ru/themes_install.html\n\nHope you willl find \"missing 3 buttons\" in title bar, after all. :-)"
    author: "Vadim Plessky"
  - subject: "Re: Eww"
    date: 2001-09-16
    body: "I don't know if I deservered to win or not, but I'm very happy I did...  And that prize goes to a charity, in the winner's name...\n\nPersonaly, what I entered was what I liked, and that's why I'm glad I did place.  It just so happened to be one of my favorite backgrounds I have made (while the entire view is obstructed, I truely do feal that the figures in the background are playing, and the simplicity of the figures, and their general desire to just have fun, and play...  might just be what all of us are doing!!!)\n\nMoral of this story...  Next time a call to arms of this sort occurs, don't just sit on your lorals, actualy partisipate... you'll have fun, and might win!\n\nP.S., While I'm not a huge fan of any theme that uses green [I actualy do kind of like Atlas Green], that Egyption Office theme is really cool...\n\nSo everyone whose being mean, grow up already! (and then go be a kid again with a new theme)\n\nthanks,\nGreg\n\nP.S. Anyone willing to take up that challenge to get involved is more than welcome to come over and have fun with the Open Racer team (join the mailing list today):\n\nhttp://openracer.worldforge.org for more information"
    author: "Gregory W. Brubaker"
  - subject: "Re: Eww"
    date: 2001-09-17
    body: "I know how you feel. I was expecting awesome widget themes with matching window decorations and appropriate background. Oh well.\n\nBut I should have expected it. I have written a few themes myself and they are not easy to do. The basic mechanics of making a new theme are easy. Making it look good is very hard. Widget themes are even harder."
    author: "David Johnson"
  - subject: "grotesque! [Re: Eww]"
    date: 2001-09-18
    body: "> ... but all of these themes are grotesque, IMNSHO.\n\nShould we take this as a compliment?\n\"Grotesk\" means \"modern, new, original\". At least in Font Design.\nSo, do you want to blaim us for creating new, MODERN art?..\n\nAs about \"original\" - well, everybody defines itself what for him original and what - not. For example, I think of some \"water\"-centric or \"liquid\" themes as of not very original. To be precise, not original at all!\n\nCheers,\n\nVadim Plessky"
    author: "Vadim Plessky"
  - subject: "Hmm, a Conspiracy?"
    date: 2001-09-16
    body: "Why do #2 and #3 have both kaiman/noatun running with the same song, and the same apps (in the taskbar) ?"
    author: "Charles Samuels"
  - subject: "Re: Hmm, a Conspiracy?"
    date: 2001-09-16
    body: "Nah, I'd presume that the screenshots were taken by an IBM person who just switched theme and retook the screenshots.  \n\nThough it's more fun to think it's a conspiracy :-)"
    author: "Oz"
  - subject: "Re: Hmm, a Conspiracy?"
    date: 2001-09-16
    body: "So, which judge has the bad taste of using the Kaiman skin loader, and the bad taste of selecting that song? ;)"
    author: "Charles Samuels"
  - subject: "Re: Hmm, a Conspiracy?"
    date: 2001-09-16
    body: "It's definitely kitsch. The song used for the winner - Weird Al's \"Alberquerque\" if I'm not mistaken - is awesome.\n\nNo offence to the theme designers, but it looks like they spent an entire five minutes designing their themes. If I knew the standard would be this low, I would've entered the competition myself. Someone ought to give Mosfet a prize for his Liquid style engine."
    author: "Yama"
  - subject: "5 minutes?.."
    date: 2001-09-18
    body: "5 minutes? Are you joking?<br>\nNo offense to you, but  have you ever looked at kde.themes.org, when it was up and running?<br>\nNo offence to themes.org team, but I started doing ownj themes when I found that I can't get good enough from kde.themes.org.\nBy the way, I prepeared soewhat 30+ themes, some original, some backports, and they were accessible (ready for download) for about 3 months already.\nYou can check <a href=\"http://apps.kde.com/na/2/info/vid/3239\">apps.kde.com entry</a> for tarball and and screenshots.\nAbout themes' creativity and quality: you could just drop me an email and tell what you don't like in them. :-) \n<br>Remember there were special requirements in themes' contest (original design, easy of use, etc.) which prevented me from doing some more exotic design.\n<Br><br>\nCheers,\n<br><br>\nVadim"
    author: "Vadim Plessky"
  - subject: "Re: Hmm, a Conspiracy?"
    date: 2001-09-16
    body: "Bad taste in song selection?  I guess \"when it comes to songs, IBM ain't got nothin' to do with your selection.\"  :o)\n\nI'd say \"when it comes to themes, IBM ain't got nothin' to do with my selection,\" but that's just me.  :o)"
    author: "David Walser"
  - subject: "Hardly a conspirousy..."
    date: 2001-09-16
    body: "Those screenshots were obviously taken from the judges' computers.\n\nThe judges very obviously wanted to give you a common setting such that you were not judgeing something like the kia skin, but the actual theme.  I don't think the screenshots best show off the themes, but IBM representatives are not miracle workers.\n\nI do hope those themes are added to the kde themes collection\n\nI also hope that everyone here who screamed bloody murder about the themes actual tries them...  but only because I hate you."
    author: "friend"
  - subject: "Need a clue"
    date: 2001-09-16
    body: "Let's assume that I'm a \"coder with artistic talent\", and that I want to create a theme. But this would not be only an \"ugly colors + background on top of existing theme\" theme, it would be a complete redesign of every widget + sound + mouse cursors + icons + fonts. Where would I get the information on how to design such a theme?"
    author: "Simon Perreault"
  - subject: "Re: Need a clue"
    date: 2001-09-16
    body: "Right here.  At least for the widgets."
    author: "not me"
  - subject: "Well I give you the Cursor :)"
    date: 2001-09-16
    body: "Just copy it to /usr/X11R6/lib/X11/fonts/misc folder or in ~/.kde/share/fonts/override and restart X+KDE."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Well I give you the Cursor :)"
    date: 2003-07-19
    body: "That is an awesome cursor"
    author: "Rorry"
  - subject: "Re: Need a clue"
    date: 2001-09-17
    body: ":: Where would I get the information on how to design such a theme?\n\nIs there an interest in starting a mailing list for theme and widget designers?  I've mocked up what I want in Gimp, and I'm looking at doing it soon (I am under the impression that I need to have the source tree to KDE, and the space to compile it, and since I'm running at 99%+ disk usage at the moment, I need to get a new drive first).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Need a clue"
    date: 2001-09-17
    body: "You won't need the space for the *entire* KDE source. You only need the kstyle section of kdelibs. And you don't even need that, though it helps to see how other widget themes were done."
    author: "David Johnson"
  - subject: "Not bad."
    date: 2001-09-16
    body: "Despite what others are saying...   However, I must say I prefer Vadim's theme the most.  Seems more work went into this one than the others but he only got 3rd prize.  Anyone know how many total submissions there were?"
    author: "KDE User"
  - subject: "The theme is broken"
    date: 2001-09-17
    body: "The theme looks good, but can't you see the maximise and sticky buttons are not following the theme. That was always a problem with Vadim's themes. I can't believe that they included a broken theme into their top 3. Maybe they didn't even have more to choose from."
    author: "JB"
  - subject: "Re: The theme is broken"
    date: 2001-09-18
    body: "erm? thats a bug in the theme loader."
    author: "Jason Katz-Brown"
  - subject: "Re: The theme is broken"
    date: 2001-09-18
    body: "Theme is ok. KDE 2.1 and KDE 2.2alpha1, alpha2 need special fix for \"themes.mapping\". I have posted instruction about this on my web site, let me guess, in the beginning of June 2001 (on Theme Install page)\nI have notified during submission that to be fully operational, this theme needs fix to KDE. Just wonder now why screenshot doesn't have this fix.\nBy the way, in KDE 2.2 theme manager (corresponding pixmap themes engine) is broken, so to get \"right\" look you need KDE 2.1 with patch. I am currently investigating how to fix things in KDE 2.2, but as I mentioned, it seems to me that engine is broken, \"standard 2.1 themes\" are fine.\n\nBR,\nVadim Plessky"
    author: "Vadim Plessky"
  - subject: "Re: Not bad."
    date: 2001-09-18
    body: "Oh, thanks a lot, I am pleased!..\nAnd I think most important is participation, not winning. Finally, users will have better themes, choice of favourite colors, etc. Let's hope this is not the last contest, and we will have more flexible options for theme submission next time! (it was possible to submit only one theme in this IBM contest, which is somewhat unfare, in my opinion...)\n\nCheers,\n\nVadim"
    author: "Vadim Plessky"
  - subject: "is this a joke ?"
    date: 2001-09-16
    body: "No offensse to the winners..but is this a joke or something ? \nSuch pathetic themes won a contest ? I seriously dont get it. Maybe only \nthree people participated.Any why it took IBM forever to publish the results ?\nsomething's wrong !"
    author: "hackorama"
  - subject: "Re: is this a joke ?"
    date: 2001-09-16
    body: "How can you ask the last question? Of course it took IBM months of \"balls gathering\" to publish the results. I bet the suggestion \"Lets do our own themes and submit them\" was on the table at one point."
    author: "J\u00f6rgen S"
  - subject: "Re: is this a joke ?"
    date: 2001-09-16
    body: "rotflmao!!! ;-)\n\nseriously - props to IBM for making the donations and/or giving out the prize money - whatever the winning themes were.  May the winners spread the joy with pizza and soda/beer among co-workers and fellow developers...\n\nthanks for the earlier tutorial zip file - I'm not the least bit surprised that mosfet (the king of graphic hacks w/Pixie, liquid-theme, to name a couple) is the author.\n\n(excellent work as always mosfet - love the latest high perf liquid efforts ;-)"
    author: "jason byrne"
  - subject: "Re: is this a joke ?"
    date: 2001-09-16
    body: "I couldn't believe it as i saw the \"winning\" theme. I think this is a bad joke ... Use a standart theme and use CDE colors and you have $3000 ? No no i think they must create own buttons and window decorations and so on ... but here is nothing of such stuff. I think i saw a little of such things on the 3rd place theme but the first two were only a joke .. (I think)"
    author: "Andreas Scherf"
  - subject: "Re: is this a joke ?"
    date: 2001-09-16
    body: "Yes, it really feels like a joke. \nThanks to IBM for giving a few dollars away though..."
    author: "Al_trent"
  - subject: "Re: is this a joke ?"
    date: 2001-09-16
    body: "Do you guys watch the Special Olympics and laugh too???\n\nsad, sad, people...\n\nYou'd think with the current problems in the world, you'd find something better to complain about...\n\nGreg Brubaker,\nNew York, USA"
    author: "Gregory W. Brubaker"
  - subject: "Re: is this a joke ?"
    date: 2001-09-16
    body: ">>Do you guys watch the Special Olympics and laugh too???\nYou are not *seriously* comparing yourself to Special Olympics\nparticipants, are you? :) I mean, few people laugh at that stuff,\nbut effectively calling yourself a retard is a different business.\n\nSeriously, this IBM contest looked disconnected from the reality\nfrom its very beginning."
    author: "KDE User"
  - subject: "Re: is this a joke ?"
    date: 2001-09-16
    body: "It's really a paradox...\n\nIf you didn't enter, do you loose?\n\nIf you are sore, and did not enter, are you a sore looser?\n\n\nI only believe I know the answer to this one:\n\nIf you would have won, but did not enter, has the entire world lost?"
    author: "Gregory W. Brubaker"
  - subject: "Re: is this a joke ?"
    date: 2001-09-17
    body: "Yeah, by the time IBM had even bothered to announce the contest on a forum with more than 3 KDE people on it, there were only something like 2 weeks left.\n\nI kept saying to myself \"man, I bet anything there will be like 10 entries at most, and none of them could possibly be concieved of and refined in that short a time, that just isn't nearly enough time!\n\nMaybe next time IBM will give at least 6 months and announce on the Dot immediately; it is after all news central for KDE."
    author: "Chris Bordeman"
  - subject: "Re: is this a joke ?"
    date: 2001-09-18
    body: "> Seriously, this IBM contest looked disconnected from the reality\n> from its very beginning.\n\nAnd waht you were expecting from contest where winner (1st prize) get nothing, and $3000 is charity donation?\nDo you think people doing themes do not deserve to get some money?\nDo you think $3000 is enough to support, say, KOffice, and make MS Offfice filters for KWord/Kpresenter working?\nNo. No.\n$3000 (or $1000 donation, which I won) is better than nothing, but *it is not enough*\nEven Quake championship in US had $40000 1st prize.\nAnd yes, I think I am more talented and skillful than Quake players..\n\nNot to offend someone, but that's just my opinion.\n\nCheers,\n\nVadim"
    author: "Vadim Plessky"
  - subject: "Re: is this a joke ?"
    date: 2001-09-16
    body: "Certainly not.\nAnd I am not laughing at your theme either, it's a nice theme, but it has really nothing great. I always respect other's work, especially when done for free\n\nI was anticipating some great themes coming out of this contest, and I got surprised to see those three 'standard' themes winning the contest.\nNetscape had some great themes for its Netscape 6 skin contest, I was expecting the same level here.\n\nAll of this is my personal opinion though, upload your theme to themes.org and we'll see if I am the only one thinking this."
    author: "Al_trent"
  - subject: "Re: is this a joke ?"
    date: 2001-09-18
    body: "To be honest, so was I...\n\nbut I'm hardly about to complain :o)"
    author: "Gregory W. Brubaker"
  - subject: "Re: is this a joke ?"
    date: 2001-09-18
    body: ">  All of this is my personal opinion though, upload your theme to themes.org > and we'll see if I am the only one thinking this.\n\nwell, that's exactly what I did.\nYou can find my work here: http://apps.kde.com/na/2/info/vid/3239\nThere are about 1500 downloads from this location, plus around 2000 from www.astralon.ru and my site (http://kde2.newmail.ru)\n\nNote that I was not ripping Matrix, Star Wars or whatever.\nAnd \"Mummy Returns\" appeared on screens *after* I did my Egypt theme :-)\n\nThere are some limitations in KDE Widget and Window Decorations engines. I can't overcome them. But current choice I have on my desktop is superior to Gnome, SawFish and IceWM. That's was my goal when I started doing my themes. I am pretty happy that I succeeded. Going to make more in the future! :-)\n\nVadim Plessky\nAuthor of Egypt Office theme (and some other KDE themes)\nhttp://kde2.newmail.ru"
    author: "Vadim Plessky"
  - subject: "Re: is this a joke ?"
    date: 2001-09-16
    body: "Laugh at the special olympics??\n\nDude, are you trying to tell us that you are quadriplegic or something?  I serioiusly doubt it, because some chap with no arms, no legs and just his tongue to type could do way better than these themes. I think most people can see that there were probaby only 3 entries. Hell, I should have entered the default theme with a new background image, that surely would have won."
    author: "Cabbage Head"
  - subject: "Re: is this a joke ?"
    date: 2001-10-03
    body: "Obviously Cabbage Head didn't have the guts to leave his email for responses.  You obviously haven't stopped whinning since you left the crib.  You also know nothing about the contest or its judges.  I can hardly believe I'm responding to your garbage.\n\nhttp://www-106.ibm.com/developerworks/linux/library/l-kde-c/?dwzone=linux"
    author: "Peter"
  - subject: "Well"
    date: 2001-09-16
    body: "I remember that when this contest was announced it was for KDE 1.x only? Right? If I had know it included 2.x styles/themes, I would have probably submitted something. \n\nOut of the top three, I like vadim's the best for artistic quality, but I don't think I'd ever use it personally.\n\nAnd props to IBM for supporting this contest :)."
    author: "sashmitb"
  - subject: "Who's mosfet?"
    date: 2001-09-16
    body: "Who's him? Where is his theme?"
    author: "Anonymous"
  - subject: "Re: Who's mosfet?"
    date: 2001-09-16
    body: "http://www.mosfet.org"
    author: "anonymous"
  - subject: "Re: Who's mosfet?"
    date: 2001-09-18
    body: "Well, not only has Mosfet done themes, he also was a primary force behind the devel of the style engine, and a whole bunch of other nifty stuff."
    author: "Carbon"
  - subject: "http://themes.kde.org"
    date: 2001-09-16
    body: "I think it is about time that we start themes.kde.org, and I think that we have many people around who would like to maintain it. Themes.org has been down (no uploads) for so long time, and I gave up creating icon themes and kde styles because of that.\nWe could also have our own theme contest where the best themes would become the part of official KDE releases.\nAnyone interested in these ideas?"
    author: "antialias"
  - subject: "Re: http://themes.kde.org"
    date: 2001-09-16
    body: "i think thats a bloody good idea!.. i started some time ago on a scheme, but now i want to go all the way: widgets/icons/colors/everything!\n\nthe goal is a \"Hacker Theme\" that is both cool & mean looking... but stille usable on an everyday basis. (wins:dark-gray, txt:bright-green).. havent figured the icons out yet tho... sugestions wellcome ;-D\n\n/kidcat\n\nPS.: I would be a SO annoying to all my freinds if i got a theme in the official release ;-D\n\n--\n\nhardtoreadbecauseitissolong"
    author: "kidcat"
  - subject: "Are you reading Dre, we want themes.kde.org!!!"
    date: 2001-09-16
    body: "Yes I also would love to see a themes.kde.org as I have created Icon theme but I could not upload that at kde.themes.org\n\nLets reverse kde.themes.org to kde.themes.org ;)\n\nI wonder dre will do it or not :^O"
    author: "Asif Ali Rizwaan"
  - subject: "that was"
    date: 2001-09-16
    body: "kde.themes.org to themes.kde.org :)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: http://themes.kde.org"
    date: 2001-09-16
    body: "themes.org is going to relaunch *very* soon, check out http://alpha.themes.org/"
    author: "Sashmit Bhaduri"
  - subject: "Re: http://themes.kde.org"
    date: 2001-09-16
    body: "Great, alpha.themes.org looks promising, but what does 'soon' mean?"
    author: "antialias"
  - subject: "Re: http://themes.kde.org"
    date: 2001-09-16
    body: "within a week and a half."
    author: "sashmit bhaduri"
  - subject: "Re: http://themes.kde.org"
    date: 2001-09-16
    body: "Few years ;)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: http://themes.kde.org"
    date: 2001-09-16
    body: "Themes.org is almost ready to go back up again!  Check out alpha.themes.org for a taste of the new site."
    author: "not me"
  - subject: "Re: http://themes.kde.org"
    date: 2001-09-17
    body: "There is already artist.kde.org. Is it not enough? If there really are\ninterested people, they could make a.k.o better - it seems to be in a\nquite poor condition now (\"news\" from year 2000...). In my opinion \"theme\"\nis a subclass of \"artwork\", so a.k.o is a good name and can include sounds,\ngraphics etc. besides complete themes. There is also a list called kde-artists,\nmaybe you interested should discuss there - or is it a proper place for this?"
    author: "Eeli Kaikkonen"
  - subject: "Re: http://themes.kde.org"
    date: 2001-09-17
    body: "Well, kde.themes is ran by completely different people then a.k.o. However, a link between the two would probably be appropriate."
    author: "Carbon"
  - subject: "Re: http://themes.kde.org"
    date: 2001-09-18
    body: "Let me second back this.\nWe NEED themes.kde.org!!! (as it was already discussed several times, on different KDE lists)\n\nBR,\nVadim Plessky"
    author: "Vadim Plessky"
  - subject: "Yuck, were those themes"
    date: 2001-09-16
    body: "I would never use those so called winning themes even if IBM gives me $3000 to use it for a month. I just abhor those themes. Now I see how artistic IBM is and their sense of beauty is very ...."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Yuck, were those themes"
    date: 2001-09-16
    body: "Hey, come on,\n\nthis competition is made by the people who brought us OS/2!\n'nuff said."
    author: "Thomas"
  - subject: "OS/2 .."
    date: 2001-09-17
    body: "ROFL!!!!!\n\nOS/2 is the ugliest OS on planet earth.\n\nI think the problem with this theme competition is that once the dot knew the competition was open, we only had approx two weeks to get our submissions in. \n\nThat is why the submissions just plain sucked. \n\nMosfet would win hands down if the competition was held now.\n\nI also agree with the sentiment that IBM was not in touch with reality when they created this competition as the original requirement was for KDE 1 only, and KDE 2 was already out for quite some time.\n\nCongrats to the winners, I am not taking anything away from you as I did not enter, so I can't complain :) but please be aware that you are lucky to win with such crap submissions.\n\nI agree with hosting a themes.kde.org site ... that would rock."
    author: "kde-user"
  - subject: "Re: Yuck, were those themes"
    date: 2001-10-03
    body: "Asif - You apparently knew little about the contest.\n\nPrize money is donated to non-profit open source organization, not the creator.  So if you're intentions are as pure as you puport them to be, why didn't you enter the contest?\n\nSubmissions were not judge by IBM. They were judged by 2 external judges.  Here's info on the judges.  They most likely did not have many entries.\n\nhttp://www-106.ibm.com/developerworks/linux/library/l-kde-c/?dwzone=linux\n\nThe results are not so much related to the judges as they are to the number and quantity of entries.\n\nObviously, it's easier to critcize than to actually create something."
    author: "Peter"
  - subject: "I thought I saw demos of better ones once..."
    date: 2001-09-16
    body: "Hey, I thought I saw some pictures of demos of some people's submissions way back when this was started. The guy had new buttons for everything and it all looked very very good. This is why I didn't bother to try. But something happened. Does anyone know why those themes (if you remember them) didn't get anywhere close? I presume they weren't submitted..."
    author: "EHK"
  - subject: "(winners)^2"
    date: 2001-09-16
    body: "MAN, I wish I would have entered!!!  I could have not only won, but pissed all you f*ckers off at the same time!!!\n\nA VERY LARGE CONGRATUATIONS GOES TO TODAYS WINNERS!!!"
    author: "friend"
  - subject: "Re: (winners)^2"
    date: 2001-09-16
    body: "Ofcourse you would have !\n\nBut can you show me your theme perhaps ? You got me a little bit interested now :)"
    author: "Jelmer Feenstra"
  - subject: "Re: (winners)^2"
    date: 2001-10-03
    body: "Jelmer - Go ahead and mail me your submission.  I'll be sure and post it up here for everybody to see. \n\nThanks."
    author: "Show us your theme Jelmer"
  - subject: "Hard to believe"
    date: 2001-09-16
    body: "Phew. I would like to see those themes that did not win.\nHow could THEY look ?"
    author: "AB"
  - subject: "KDE and broken beauty :("
    date: 2001-09-16
    body: "I like most of you feel bad that KDE has every look and feel modules scattered into different places so that the user have to struggle configuring the look&feel painfully and anybody who wishes to create Theme which might include:\n\n1. colors,\n2. style,\n3. window decoration\n4. fonts,\n5. kicker size,\n6. widget sizes (not yet implemented)\n6. and other look&feel configurations\n\nKDE must include all look and feel modules into a theme manager then only KDE will get very nice looking themes. But do kde team care for this other than hunting bugs ;)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE and broken beauty :("
    date: 2001-09-17
    body: "I agree that those modules should be integrated."
    author: "AB"
  - subject: "Re: KDE and broken beauty :("
    date: 2001-09-20
    body: "> 1. colors,\n> 2. style,\n> 3. window decoration\n> 4. fonts,\n> 5. kicker size,\n> 6. widget sizes (not yet implemented)\n> 6. and other look&feel configurations\n\nMost annoying to me that you can't install fonts (and even can't select from installed ones).\nMany themes are looking terrible with wrong (bitmapped) fonts.\nColors you can configure via Theme manager, Styles you need to install manually.\nWidget Sizes - well, you can control TitleBar height in KDE2 default theme ;-)\n\n> KDE must include all look and feel modules into a theme manager then only \n> KDE will get very nice looking themes. But do kde team care for this other \n> than hunting bugs ;)\n \nI do not think that somebody is actevely working on this.\nMosfet promised several times to fix \"style\" (engine) module and window decoration handling, but hasn't fixed.\nAs he announced his divorse with KDE some time ago, he is working on it.\n\nBesides, you can try to ask Rik Hemsley - may be he has some patches :-)\n\nCheers,\nVadim"
    author: "Vadim Plessky"
  - subject: "Themes.KDE.org"
    date: 2001-09-16
    body: "I was wondering if you wanted to collaborate on a themes.kde.org?  \nI can provide the background technical framework, but I'm not much of an \nartist beyond being able to match pallettes and so on.\n\n--\nEvan"
    author: "Evan"
  - subject: "Geeks != Designers"
    date: 2001-09-17
    body: "Geeks may write good code, but they ain't got a clue about good design..."
    author: "Mr Bongo"
  - subject: "Unless they're called Mosfet. n.T."
    date: 2001-09-17
    body: "n.T."
    author: "Spark"
  - subject: "only wise asses around..."
    date: 2001-09-17
    body: "listen to yourselfs for a moument guys.... everybody is complaying (including me) that he didn't contribute, but this was exacly the reason why the quality of the themes was not so good. \nSo stop accusing IBM that they don't have any taste - they just didn't have too many themes they could choose from.\n\nare you not happy that such big (rich) company as IBM supports Linux and OS community? In not best times recently for us.. (read newsforge.net - all goes down).\n\nIf we want to keep IBM supporting linux we must act! - the lesson for the next time.\n\nBig thx to IBM for all it does to Linux and big thx to guys who contributed!"
    author: "sUpeRGR@sS"
  - subject: "Oh...my...God..."
    date: 2001-09-17
    body: "I have never made a KDE theme before, but I could easily have outdone those ugly things...  I wish I could be more polite about it, but they are just pitiful."
    author: "dingodonkey"
  - subject: "Re: Oh...my...God..."
    date: 2001-09-17
    body: "Man if I knew I could have made a couple of thousands bucks by sending a screenshot with the default theme and an ugly background...."
    author: "75"
  - subject: "Re: Oh...my...God..."
    date: 2001-09-18
    body: "Why not to make a theme and put an URL where we can download it?\nI was waiting 6 months (!!) after KDE 2.0 release to see new themes - and everything was of non-acceptable quality...\nThat's *how* I started doing my own themes.\nBest Opne Source principle: \"stop talking, start coding!\"\nAnyone?.. :-)\n\nBR,\nVadim Plessky"
    author: "Vadim Plessky"
  - subject: "Not so good."
    date: 2001-09-17
    body: "I'm a new user to Linux/KDE.\nLinux/KDE has attracted me by its stability as well as its excellent interface.\nI'm now using KDE 2.1.1 and i think any theme i've used on KDE is better than\nthe ones mentioned in the article...\nWhy have they won the contest with so poor design and look-feal?\nIt's a puzzle...\nLook at kde.themes.org,where there are so many splendid themes with pretty icon\nand beautiful background.\nMaybe excellent theme designers have no time to participate such an contest!"
    author: "okay"
  - subject: "Yes, they are ..."
    date: 2001-09-17
    body: "Judges are obviously ... BLIND, grrr ...;-)"
    author: "loco"
  - subject: "Disappointing... very disappointing !"
    date: 2001-09-17
    body: "What is this, some sick joke ? I mean, hey, the second place has a nice background, and the third place has three new icons in the title bar but the rest is just some color changes ! How lame ! $3000 for the winning theme is simply absurd !\n\nJust look at the Acqua theme for some fine example of what one can do with his KDE desktop... combine it with an IceWM titlebar with Aqua-like buttons and you've got a desktop that makes Windows users wish MS had such eye-candy.\n\nI expected someone would go ahead and write their own KStyle, but this contest is really disappointing... it's a laugh !"
    author: "StonedBones"
  - subject: "Re: Disappointing... very disappointing !"
    date: 2001-09-17
    body: "The $3000 is donated to CHARITY, no one is getting $3000 richer just for submitting a color scheme!  Geez, you guys, read the article!  If you could have done better, you should have entered!  Tough luck."
    author: "not me"
  - subject: "Horrible Themes!"
    date: 2001-09-17
    body: "These themes are horrible! If this is the future of graphical environments, I'll stick with console."
    author: "L-Wave"
  - subject: "Calm down"
    date: 2001-09-17
    body: "When I first though those shots, I really expected something different. Sure. But I don't think that the price is \"indeserved\". :) Simply because those prices are DONATIONS. That means, that they it should be used to support open source organizations. That's a good thing, isn't it?\nThank you IBM!\nToo bad that there isn't any creative new theme, but who cares? Liquid is all you need for fancy stuff. ;)\nI still think a \"non-fancy\" style is missing (the KDE default looks too much like \"plastique\" and not very good), but I'm perfectly happy with Liquid."
    author: "Spark"
  - subject: "s/indeserved/undeserved/ n.T."
    date: 2001-09-17
    body: "n.T."
    author: "Spark"
  - subject: "Not Impressed"
    date: 2001-09-17
    body: "I must commend the guys that took part in this competition but I am sorry to say that they are not up to par.  I was fired u  and ready to install these winning themes but after seeing screenshots, I was influenced otherwise.  Maybe is because they are sytle themes as opposed to widget themes.  The kde community has, in my opinion, preferred the widget themes.  I do agree with the guys that are proposing a 'themes.kde.org'  The prolonged \"update\" period of themes.org is hampering the creativity of many of our KDE theme artists.\n\nWhere do I sign up for such a undertaking?\n\nMy 2 cents."
    author: "puelly"
  - subject: "Widget Themes [Re: Not Impressed]"
    date: 2001-09-18
    body: "Complementary widget theme (to my Egypt Office) was submitted to IBM KDE Theme Contest. Ask people who were orginizing contest why they haven't used it.\nWanna better choice, than 3 winners?<br>\nCheck my themes (VadimThemes-0.2) on apps.kde.com. In addition to Window Decoration themes, you will find widget styles, too! :-)<br>\nURL: <a href=\"http://apps.kde.com/na/2/info/vid/3239\">apps.kde.com entry</a>\nThanks in advance for comments. :))\n<Br><Br>\nBR,<br>\nVadim Plessky"
    author: "Vadim Plessky"
  - subject: "Re: Widget Themes [Re: Not Impressed]"
    date: 2001-09-18
    body: ":: Check my themes (VadimThemes-0.2) on apps.kde.com. \n\nJust out of curiosity, why are all the image links broken on your homepage?  I went to go check out any new screenshots, and all I get is that annoying \"broken image\" icon.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Widget Themes [Re: Not Impressed]"
    date: 2001-09-18
    body: "> Just out of curiosity, why are all the image links broken on your homepage? > I went to go check out any new screenshots, and all I get is that annoying > > \"broken image\" icon.\n\nProvider deleted all contest while I was out of city :((#$%^\nBesides, they changed their FTP server, so Konq doesn't login to it anymore.\nSo, I just could not upload 400 files \"file by file\", via \"put\" in console.\nTired after 35 files...\n\nPlease use apps.kde.com (http://apps.kde.com/na/2/info/vid/3239) until I restired everyhing.\nThere are screenshots on it as well, together with tarball.\n\nCheers,\n\nVadim"
    author: "Vadim Plessky"
  - subject: "Re: Widget Themes - screenshots uploaded"
    date: 2001-09-19
    body: "Hi,\n\nI uploaded themes and pictures (screenshots) on http://kde2.newmail.ru, again.\nPlease revisit site.\n\nP.S. I am also going to make good screenshot for Egypt Ofice theme, and make it available to public (together with widget theme)\nObviously, I was waiting for IBM to release contest results, and I just couldn't make it public before (Contest' rules...)\n\nVadim"
    author: "Vadim Plessky"
  - subject: "Thank God I've Got Mac OS X!"
    date: 2001-09-18
    body: "If this is was themes are like in Linux/KDE, i'm really thankful to have my beautiufl Mac OS X to go back to!\n\nWhats up with those themes? Their look horible! Where's the individuality? The most the themes do is change some colors. Part of the blame goes to the KDE project itself for not having a theming architecture that can support revolutionary new themes and UI ideas.\n\nhttp://www.apple.com/macosx"
    author: "Indigo"
  - subject: "Re: Thank God I've Got Mac OS X!"
    date: 2001-09-18
    body: "KDE (and Qt) has a rich theming engine, as long as you create your own widget themes but even with the normal (and mostly slower) pixmap theming you can archive much better results than in this contest (no flaming, just reality, but I am a bad designer, too ;).\nThe theming contest wasn't what I thought of, but thats not the fault of the KDE architecture or of the winners of the contest.\nThanks to IBM for supporting the OpenSource community with money and work and thanks to all people who has taken part in the contest."
    author: "Cullmann Christoph"
  - subject: "Re: Thank God I've Got Mac OS X!"
    date: 2001-09-18
    body: "But still, most of the themes I see on kde.themes.org are just change the single-tone color from that [ugly] light gray to a hidious green or something. How about, for example, some icons to go with the theme to give your desktop that overall make-over.\n\nSame thing with the kicker/kpannel thing. You can only change the color from one mono-chrome color to another -- how plain.\n\nYou have to admit that even the GTK themes are better and more original than these KDE themes.\n\nHow about fades, shadows, transparenticies, and opaques in KDE? I know some of this foundation is dependent on support in X11 itself."
    author: "Indigo"
  - subject: "Re: Thank God I've Got Mac OS X!"
    date: 2001-09-18
    body: "He, look at for example at mosfet's liquid theme, just nice + fast (k, edges could be nicer rounded, but thats not very fast without a good XRender extension therefor). The theming engine is quiet powerful, if you put a lot of work in your theming code (like mosfet, thx ;)\n\nBut the most themes at kde.themes.org are like the stuff in the contest, you are right :(\nSome GTK Themes are very nice, but the GTK stuff have no real fades, shadows, transparenticies, and opaques (they use mostly pixmaps). Thats perhaps easier to do than code the widget stuff in C++, but much slower (especially on < 300 Mhz machines, like my old one)."
    author: "Cullmann Christoph"
  - subject: "pug-ugly themes"
    date: 2001-10-30
    body: "those judges need their eyes tested."
    author: "timewasterX"
---
<A HREF="mailto:solrac@us.ibm.com">Frank</A> was the first of many to write
in to tell us that <A HREF="http://www-106.ibm.com/developerworks/">IBM
developerWorks</A> has
<A HREF="http://www-106.ibm.com/developerworks/linux/library/l-kde-c/">announced</A> the winners of the <A HREF="http://dot.kde.org/987115027/">KDE theme
contest</A>.  The top prize (along with a $3,000 donation) goes to Matthias
Fenner of Germany for the
<A HREF="http://www-106.ibm.com/developerworks/linux/library/l-kde-c/theme1.html">Winning theme</A> (<A HREF="http://www-106.ibm.com/developerworks/linux/library/l-kde-c/988319063-79002-lms-kde2-theme.ktheme">download</A>), second prize (along with a $2,000 donation) goes to
Gregory Brubaker of the United States for the
<A HREF="http://www-106.ibm.com/developerworks/linux/library/l-kde-c/theme2.html">Happy People theme</A> (<A HREF="http://www-106.ibm.com/developerworks/linux/library/l-kde-c/gregbrub-Happy_People.ktheme">download</A>), and third prize (along with a $1,000 donation)
goes to Vadim Plessky of Russia for the
<A HREF="http://www-106.ibm.com/developerworks/linux/library/l-kde-c/theme3.html">Egypt
Office theme</A> (<A HREF="http://www-106.ibm.com/developerworks/linux/library/l-kde-c/vplessky-Egypt-office.ktheme">download</A>). Each winner now needs to choose a non-profit, Open Source
organization to which developerWorks will make a donation on his behalf.  Congratulations to the winners, and thanks to IBM developerWorks for their sponsorship of this rewarding project!