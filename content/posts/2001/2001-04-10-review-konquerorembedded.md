---
title: "Review of Konqueror/Embedded"
date:    2001-04-10
authors:
  - "ostrutynski"
slug:    review-konquerorembedded
comments:
  - subject: "Re: Review of Konqueror/Embedded"
    date: 2001-04-10
    body: "redhat users will be interested in the konq-nox package, at ftp://ftp.redhat.com/pub/redhat/linux/rawhide/powertools"
    author: "Evandro"
  - subject: "Re: Review of Konqueror/Embedded"
    date: 2001-04-11
    body: "How can Konqueror be getting positive reviews if it can't even do a simple style.display DOM method.  KDE needs to fix various DOM/CSS problems first before putting it out to every device"
    author: "Robert Gelb"
  - subject: "Re: Review of Konqueror/Embedded"
    date: 2001-04-11
    body: ">How can Konqueror be getting positive reviews if it can't even do a simple style.display DOM method. \n\nPerhaps because it WORKS!  Maybe KHTML hasn't implemented your particular pet feature yet, but don't let that color your view of the whole program!  Konqueror works for the majority of sites out there.  I've used it to pay bills, fill out surveys, get my hotmail e-mail, watch Flash animations, go to sodaplay.com, and sign up for accounts on various websites, and it has always worked very well.\n\nAnd don't forget that every day and in every way, Konqueror is getting better and better!  The dedicated efforts of the KHTML team never fail to amaze me.  Submit a bug report today, and it will probably be implemented for KDE 2.2.  Try doing THAT with your Internet Explorer!"
    author: "not me"
  - subject: "Re: Review of Konqueror/Embedded"
    date: 2001-04-11
    body: "The problem with an incomplete browser for an embeded system is that you can't fall back on netscape 4.x, saying the thing your using is a palmtop, you get pissed and then you say the palmtop sucks and you'll never buy anything from the company that made the palmtop, and tell your friends that the company's stuff is broken and not to buy it.  Also not to be a troll or anything IE is the only 100% standards complient brower you can get today, and is one of the best pieces of software put out by microsoft."
    author: "AC"
  - subject: "Re: Review of Konqueror/Embedded"
    date: 2001-04-11
    body: "If there were a significant number of pages that Konqueror botched and Netscape 4.x displayed correctly, then you could call Konqueror \"incomplete.\"  However, I think Konqueror is superior to Netscape 4.x in every respect.  I don't even have Netscape installed on my computer any more, because I found that I never used it even as a backup.  Konqueror can do everything I want.\n\nI'll have to agree with you about IE though.  While it may not be 100% standards compliant (ask some web designers about that one) it's the fastest, most feature complete and easiest to use browser out there now.  The speed is of course due to Microsoft's unique ability to hack IE into the bowels of the OS, and IE's features come from the fact that Microsoft can sink billions of dollars into developing IE without even blinking.  IE is the premiere application product of the most gigantic software company in existence.  Of course it's good!\n\nWhat's amazing about Konqueror is that it cost several orders of magnitude less money to develop than IE, and it is still a viable competitor.  In fact, if Konqueror keeps improving at the same rate, it will be better than IE in the not-too-distant future ;-)"
    author: "not me"
  - subject: "Re: Review of Konqueror/Embedded"
    date: 2001-04-11
    body: "a really true and honest answer!\nGo on like this KDE."
    author: "kernel"
  - subject: "Re: Review of Konqueror/Embedded"
    date: 2001-04-11
    body: "The point is that wether Konqueror is a better browser than netscape or not, if you can't open a page most people consider that broken, if your using an embeded system and you can't open a page that you _realy_ need or want to open, you get pissed.  This is why embeded developers are going to be using Mozilla, Mozilla is _slow_(imagine it on a palm pilot) but is getting close to 100% standards complient, that's why people are going to be using Mozilla instead of Konqueror.  For konqueror to get any chance on an embedded system, it needs to be 100% standards complient as well."
    author: "AC"
  - subject: "Re: Review of Konqueror/Embedded"
    date: 2001-04-12
    body: "> For konqueror to get any chance on an embedded\n> system, it needs to be 100% standards\n> complient as well.\n\nActually, no. Most of the websites that do not work are not because of a lack of support for advanced features, it is usually because the HTML on the websites is horribly broken.\n\nWhile KHTML is not as close to 100% standards compliant yet, it is trying to get there and will eventually get there. But there is also a lot of work going into fall-back parser modes for sites with horrible HTML on them, so Konqueror and Konq/E deserve the positive reviews IMHO: very good browsers, getting better every day."
    author: "Rob Kaper"
  - subject: "Re: Review of Konqueror/Embedded"
    date: 2001-04-12
    body: "and also dont forget that some web designers like the idea of \"best viewed int bla bla bla browser\".  using html tags specific to one particular browser is also part of the problem.\n\ncheers!"
    author: "japy"
  - subject: "Re: Review of Konqueror/Embedded"
    date: 2001-04-11
    body: "I agree that it is good.  However, having a bug on such a key DOM feature for so long sucks. BTW, I did submit a bug report long time ago but nothing was done on it as of yet."
    author: "Robert Gelb"
  - subject: "Re: Review of Konqueror/Embedded"
    date: 2001-04-12
    body: "the display attribute was implemented at one point in time, but was not stable enough to be put in a release, so it was disabled."
    author: "Tick"
---
<a href="http://browserwatch.internet.com/">BrowserWatch</a> is running a <a href="http://browserwatch.internet.com/news/stories2001/news-20010409-1.html">very positive review</a> of Konqueror/Embedded, Simon Hausmann's efforts to build a lightweight version of Konqueror for <a href="http://www.trolltech.com/products/qt/embedded/">Qt/Embedded</a> systems. They mention the common codebase of both Konqueror versions, and have some nice words about KDE's <a href="http://www.konqueror.org/">Konqueror</a> in general.



<!--break-->
