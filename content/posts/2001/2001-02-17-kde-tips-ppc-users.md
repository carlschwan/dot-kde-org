---
title: "KDE tips for PPC users"
date:    2001-02-17
authors:
  - "wbastian"
slug:    kde-tips-ppc-users
comments:
  - subject: "Re: KDE tips for PPC users"
    date: 2001-02-17
    body: "Thanks, Ian. My old StarMax will live again!"
    author: "reihal"
  - subject: "Re: KDE tips for PPC users"
    date: 2003-07-04
    body: "do you have any tips re: ppc search engines please?\nbrgds\nhttp://www.netscake.com"
    author: "harry poker"
---
<a href="mailto:geiseri@yahoo.com">Ian Geiser</a> alerted us that our friends over at 
<a href="http://www.ResExcellence.com/"> ResExcellence</a> have a very nice
<a href="http://www.ResExcellence.com/linux_icebox/02-14-01.shtml">article</a> about running KDE 2.1 on your PPC. When you are finished with this one, you might want to click <a href="http://www.ResExcellence.com/linux_icebox/"><i>Up</i></a> to read some of the other interesting stories about KDE and Linux PPC that you can find on this site.
A big thanks goes to Ian for his hard work on the KDE packages for PPC.

<!--break-->
