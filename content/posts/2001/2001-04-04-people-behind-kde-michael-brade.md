---
title: "People behind KDE: Michael Brade"
date:    2001-04-04
authors:
  - "Inorog"
slug:    people-behind-kde-michael-brade
---
<a href="mailto:tink@kde.org">Tink</a> continues the <a href="http://www.kde.org/people/people.html">People series</a> with <a href="http://www.kde.org/people/michaelb.html">Michael Brade</a>. Michael is one of the younger developers and became involved with KDE fairly recently. His current interests include <a href="http://apps.kde.com/nf/1/info/id/466">KNotes</a> and cleaning up the contents of the <a href="http://bugs.kde.org/">bug tracking system</a>. Please also take note that Tink has a newsflash on the main <a href="http://www.kde.org/people/people.html">people page</a> where she announces her intention to develop a resource for KDE-related job offers and demands. Take a look, and let <a href="mailto:tink@kde.org">her know</a> if you're interested.

<!--break-->
