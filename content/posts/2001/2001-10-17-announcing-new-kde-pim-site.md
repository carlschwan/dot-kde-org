---
title: "Announcing New KDE-PIM Site"
date:    2001-10-17
authors:
  - "numanee"
slug:    announcing-new-kde-pim-site
comments:
  - subject: "Empath?"
    date: 2001-10-17
    body: "What has happened to the Empath project (sources in CVS-kdepim-module). Is it dead or will work be taken on again on it (since KDE3 will support threading...)?"
    author: "Oliver Bausinger"
  - subject: "Re: Empath?"
    date: 2001-10-19
    body: "no, it's not \"dead\". As Rik Hemsley informed there's quite some information I can put on pim.kde.org about that - unfortunately I had no time to do that yet - but it will be online within the next few days ... \n\nCheers,\n\nKlaus"
    author: "Klaus"
  - subject: "Regarding KPilot"
    date: 2001-10-17
    body: "As a minor contributor to Kpilot, I'd just like to update ya'll on where's it at and where it's going.  Currently it is in that infamous \"state of flux\" where cvs isn't going to do you much good :-)  However, it's really starting to solidify, and we hope to have a 2.x compatabile release (seperate from kde3) here within a month.  The improvments will be various, including real live usb support, much better conduit support, actual *working* conduits :-), reworked dialogs both for ease of use and ease of programming (lots of kde-designer based stuff), new icons (for all kde-pim stuff, hopefully), and did I mention better conduits?\n\nAnyways, just a heads up that it is going great guns and Expect Good Things (esp. those of you like me who have usb-cradles for their handsprings).  Of course, we also have visions for where we are going in the future wrt more interoperability with the various apps and \"workflow stations\", but for now I'm pretty content with just getting the program working properly for the most number of people :-)\n\nOh, and just to answer the FAQ, no, palm 50x's are not supported and won't be until pilot-link is updated.  Sorry :-(\n\nHAND!"
    author: "David Bishop"
  - subject: "Re: Regarding KPilot"
    date: 2001-10-17
    body: "This is great news! I'm really looking forward to using kpilot with my visor"
    author: "Thomas Olsen"
  - subject: "Re: Regarding KPilot"
    date: 2001-10-17
    body: "What about support for platforms other than Linux?  The thing I love about KDE is how easily it compiles on multiple platforms, especially Solaris 8 x86.  I've never been able to get KPilot to compile correctly, however.  Has any work been done on solving these kinds of issues?  I've yet to find a decent program for syncing my Palm IIIc with Solaris!"
    author: "Steve Nakhla"
  - subject: "Re: Regarding KPilot"
    date: 2001-10-17
    body: "I know Adriaan DeGroot (the current maintainer) has installed FreeBSD soley for testing with kpilot, and I also know he has mentioned Solaris issues before (with length of device names) so it is definetly something he is concerned about.  However, and this is always true, if you have a box that you can test stuff on, even if it's just a \"make/compile breaks here\" sort of report, it will be gladly recieved and fixed. Your best bet is to download the current cvs version and go from there.  Step-by-step instructions on how to do that is at http://lists.kde.org/?l=kde-pim&m=100283864713485&w=2 however, it's now down to: steps 1-6 and step 7 being cd kpilot && make.  OH, and there was a typo, step 6 should have read --with-extra-includes=/usr/includes/libpisock.\n\nHTH and HAND."
    author: "David Bishop"
  - subject: "Re: Regarding KPilot"
    date: 2002-02-04
    body: "I've been trying for weeks to get my USB Handspring Visor Deluxe to Sync with KPilot.  The kpilotDaemon just 'freezes' when I hit the button.  I'm 'bout ready to give up."
    author: "R Berndt"
  - subject: "Re: Regarding KPilot"
    date: 2001-10-19
    body: "What about an IOSlave?\nThere was a discussion here a while back about an IOSlave/Konq kpart. This would allow a \"My PDA\" top level folder in Konq to which we can drag applications ton install. I can imagine right clicking it to sync and clicking it in order to see kpilot embeded in Konq."
    author: "Shai"
  - subject: "Re: Regarding KPilot"
    date: 2001-10-19
    body: "Feel free to implement that :-)  Well, in all fairness, there are some other things on our plate right now (like making core stuff work), but that sounds like a great idea, and should be fairly easy once the rest is in place.  If you could right-click->install on .pda, I would probably stop using pilot-xfer :-)"
    author: "David Bishop"
  - subject: "HI"
    date: 2001-10-17
    body: "I want evolution/outlook like app!\n\nand klyx too!"
    author: "Wiggle"
  - subject: "Re: HI"
    date: 2001-10-17
    body: "If you aren't looking for a KDE specific app, Ximian's Evolution is maturing quite rapidly."
    author: "Eron Lloyd"
  - subject: "Re: HI"
    date: 2001-10-17
    body: "Try Aethera?\n\nits made by TheKompany"
    author: "Bill"
  - subject: "Re: HI"
    date: 2001-10-18
    body: "And it looks good, but have almost no working function.."
    author: "NO"
  - subject: "Re: HI"
    date: 2001-10-17
    body: "Hmmm why does everyone want to have an Outlook look-a-like program?"
    author: "Rinse"
  - subject: "Re: HI"
    date: 2001-10-17
    body: "No, only the people that got used to it (and liked it)."
    author: "Evandro"
  - subject: "Re: HI"
    date: 2001-10-17
    body: "Well, I use it, got used to it, but couldn't find any reason wy to use it in KDE as we., I do my notes with knotes, mail with kmail adresses in Kadressbook, and my Agenda is in KOrganizer. \nNice set of apps that do the job, interact with each other (ok some parts could interact a bit more), and best of all, don't use the resources that Outlook or Evolution use.\nBut well, that's my $0.02\n\nRinse"
    author: "Rinse"
  - subject: "Re: HI"
    date: 2001-10-18
    body: "I understand that. I personally prefer integrated seperated applications (like in KDE 3 or somewhat evolution, but evolution is package in one package).\n\nBut some people like to do these tasks in the same application, and that's why there are applications for them and for you too ;)"
    author: "Evandro"
  - subject: "Re: HI"
    date: 2001-10-18
    body: "A shell with kmail and korganizer would be nice! like koffice shell! i just want more IMAP things in KMAIL TOO!\n\nand do nto forget klyx! I HATE XFORMS LYX."
    author: "Wiggle"
  - subject: "Re: HI"
    date: 2001-10-18
    body: "I thingk Kmail is uggly .. mozilla mail looks nice but is terrible slow and therefor not unable."
    author: "Andre4s"
  - subject: "Re: HI"
    date: 2001-10-18
    body: "Some of us use our computers as tools. I guess you don't."
    author: "Wiggle"
  - subject: "Re: HI"
    date: 2001-10-19
    body: "Oh, come on. Computers need to be at least somewhat visually appealing, as long as it isn't taken to such extremes that it becomes a priority over performance (see iMac). That's what styles, window decorations, and backgrounds are for.\n\nOn the other hand, I rather prefer kmail's look to any of the mozilla themes I've tried."
    author: "Carbon"
  - subject: "Re: HI"
    date: 2001-10-18
    body: "LyX will currently be ported to QT2 (and GTK, too). The more people are willing to help, the earlier we will see a QT2-Version (actually, most of the dialogs are ready). And maybe there will be a real KDE-Port eventually...\nHave a look at http://www.devel.lyx.org/guii.php3"
    author: "jsp"
  - subject: "Re: HI"
    date: 2001-10-19
    body: "some days ago I read that klyx (or qlyx) exists and works, so search the web :-)\n\nAnd I have to use outlook at work, IMO it sucks beyond believe, no comparison to kmail. I don't understand how people can actaully use it. One of the worst GUI's I've ever seen. Even www.gmx.net is better.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: HI"
    date: 2001-10-19
    body: ">Even www.gmx.net is better\n\nLOL \n\nYes, we want x-rated ad pics with every GUI.\n\n<still choking with laughter>\n\nSorry, have to check my mail at gmx, immediately\n\n<evil grin>"
    author: "MarkD"
  - subject: "Re: HI"
    date: 2001-10-19
    body: "KLyX exists indeed (I use it quite a lot), but there's one major problem: afaik it's only available for KDE 1.x (or Qt 1.x, for that matter). The next problem (for me anyway) is that it depends on a non-free (as in: closed-source) lib (xforms). Luckily there's a port underway: LyX (not KLyX) is being ported to Qt2, as someone already said."
    author: "Jonathan Brugge"
  - subject: "Re: HI"
    date: 2001-10-19
    body: "Oh Gimme a break .. are you just saying that to be politically correct?  \n\nOutlook is a very well thought out and feature rich application.  It's only problem is that it isn't written using QT/KDE and doesn't run on Linux.\n\nMy default desktop at the moment is KDE, so I'm using kmail.  I like it, but there are definately some features I used to use in outlook that I miss.  For example, Kmail doesn't have the ability to apply filters to views of mail folders.  I used to view some folders on Outlook with filters set so that only new + threaded mails were visible.  \n\nNor is there an easy way to mark a folder \"read\" without first selecting everything in the folder and that is expensive when there are 10,000+ mails in the folder (as there are in many of my old Outlook mail list folders).  Outlook has a RMB menu option to mark the folder as read.  A small feature, but one I used several times every day!\n\nSo don't knock Outlook because because it's not Linux ; it has a lot of good things going for it."
    author: "John"
  - subject: "Re: HI"
    date: 2001-10-19
    body: "What KDE-PIM need is a konquerer like framework that allow kmail, korganiser, kaddressbook, kpilot, etc fit into one. User customisable of course. If someone dont need a function just have to take it out. Fully plugable"
    author: "Mike Lee"
  - subject: "Re: HI"
    date: 2001-10-19
    body: "Is there any possibility to use the actual Konqueror framework for that? As far as I understand, this just requires wrapping kmail, korganizer and others as KParts."
    author: "Alex"
  - subject: "Re: HI"
    date: 2001-10-18
    body: "Because it works. :)"
    author: "And Repeat"
  - subject: "Why an outlookalike?"
    date: 2001-10-18
    body: "Because it integrates a KMail-like and a KOrganizer-like application:\nemail (duh!) + automatic organiser + ...\n\nYou  receive a proposal for an appoint, and you can accept/reject it. If you accept, your organizer is updated automatically. I never thought it would work (I'm a KDE junky) but in my current job we all use it and I got to admit, it's a great application (that is, if you're not running Win2k and don't mind the crashes etc.)\n   \nKDE could do this if KMail and KOrganiser were integrated. This has been suggested before, but I don't think anybody is actually doing it (??)."
    author: "Steven"
  - subject: "Re: Why an outlookalike?"
    date: 2001-10-19
    body: "You should try the CVS version of KOrganizer. Set up a mail filter in KMail, which calls the script korganizerIn and you will get what you want. Help is welcome to finish this feature."
    author: "Cornelius Schumacher"
  - subject: "Re: HI"
    date: 2001-10-17
    body: "And I want a red Ferrari."
    author: "anonymous"
  - subject: "Re: HI"
    date: 2001-10-17
    body: "I think I could make do with a nice shiny new 2 GHz Dell with SuSE 7.3 on it. ;-) Especially if it was *in* the red Ferrari....\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: HI"
    date: 2001-10-18
    body: "Hey, how about 100 or so custom rack-mount boxen, each with 4 2ghz procs in it. Then, I can form them into a arch, and drive the Ferrari under it! :-)"
    author: "Carbon"
  - subject: "Re: HI"
    date: 2001-10-18
    body: "Hey, why not, I won't complain. ;-) Sounds good, should we add anything else?"
    author: "Timothy R. Butler"
  - subject: "Re: HI"
    date: 2001-10-18
    body: "How about a Holodeck, so you can simulate as many cars and Ferarris as you want?"
    author: "them"
  - subject: "More stuff that doesn't make sense"
    date: 2001-10-18
    body: "Imagine a Beowulf cluster of Ferraris!"
    author: "Johnny Andersson"
  - subject: "Re: HI"
    date: 2001-10-22
    body: "He, that's my line!\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "perhaps ot: korganizer startup speed?"
    date: 2001-10-18
    body: "has anyone else seen abysmal launch times with korganizer? when i click on the alarm icon (in the tray) to launch korganizer, it takes about 1:45 before the window opens.\n\ni'm running debian sid, latest kde 2.2.1, kernel 2.4.12 & xfree86 4.1.0."
    author: "Blue"
  - subject: "Re: perhaps ot: korganizer startup speed?"
    date: 2001-10-18
    body: "that's weird. you should either submit a bug report or email one of the mailing lists (kde-linux perhaps?). It takes about 2-10 seconds for me (typical of any kde app that has not been launched yet)"
    author: "Wiggle"
  - subject: "Re: perhaps ot: korganizer startup speed?"
    date: 2001-10-18
    body: "I have the same problem on my machine. Starting korganizer takes almost a minute as soon the calendar contains a few entries.\n\nI'm using KDE 2.2.1 on Solaris 7."
    author: "Stefan"
  - subject: "Re: perhaps ot: korganizer startup speed?"
    date: 2001-10-18
    body: "I have no such problem. It appears almoast at the moment I click on the icon."
    author: "Steven"
  - subject: "Sync with Windows CE?"
    date: 2001-10-18
    body: "Are there any plans for an addon allowing syncing with a Windows CE device?\n\nIt would be great for handhelds that can actually run linux, but can't run anything useful on linux."
    author: "Jonno"
  - subject: "Re: Sync with Windows CE?"
    date: 2001-10-18
    body: "Well, it's easy enough to port things to the Agenda VR3 (X on a handheld... insane!), but I assume you mean stuff you can slap QT-E on, like the iPaq."
    author: "Carbon"
  - subject: "Re: Sync with Windows CE?"
    date: 2003-11-13
    body: "No, No, No, read the question.  This person is talking about \"syncing\" not porting.  They don't want QT Embedded linux on the pda, although I agree it's a great option, assuming it's doable on your CE Devices architecture.  They are talking about a program to sync their email, documents, etc...like KDE already has for the palm.  The only one I've ever seen didn't get very far...it was called lince.  Now if you search for it, you get a lot of mips architecture port pages.  I'm sure there is a way, though.  I'm connecting through serial, right now, and using internet functions to download my email through the linux server.  So now all that is needed is a frontend with some conversion software.  "
    author: "A.G.Gavalas"
  - subject: "How about a new Address Book AIP?"
    date: 2001-10-18
    body: "Like MS has Addressbook Providers and Messagestore Providers, KDE-pim shoud have some interfaces which can be implemented by various backends. Then we could have an SQL-backend for storing addresses/groups etc, or a FILE backend or what ever you implement as an Addressbok Provider. For Messagestore Providers one could implement backends for storing and indexing email, included indexing of mail-headers(ok. might be beon scope of kde-pim) which would be greate for enterprises and search-functionallity. Having this interoperating with KMail, KOrganizer etc. wolud truely bring _real_ groupware funktionallity to KDE.\n\n--\nAndreas Joseph Krogh"
    author: "Andreas Joseph Krogh"
  - subject: "An LDAP backend would be better"
    date: 2001-10-18
    body: "Seems like a project to cut one's teeth on!"
    author: "Corba the Geek"
  - subject: "Kandy with (a users-trasparent) IRDA/serial_line s"
    date: 2001-10-18
    body: "As an owner of Nokia 8210 (with IRDA support) i'll be glade to connect kandy to my cell.phone by my Toshiba laptop with IR port. I try gnooki utils but this approach is too nerd..\n<p>kandy -> IRDA port --> cell.phone"
    author: "IRDA-cell.phone.owner"
  - subject: "KAddressbook <-> LDAP Directory connectivity"
    date: 2001-10-18
    body: "I would like to have my KAddressbook to look at a LDAP Directory\nserver if it could not find the address in his own files.\n\nIs this a planned feature?"
    author: "Thomas Hedler"
  - subject: "Re: KAddressbook <-> LDAP Directory connectivity"
    date: 2001-10-18
    body: "Yes.  This is part of that Sphynx initiative from Germany."
    author: "David Bishop"
  - subject: "Konqueror & Psion?"
    date: 2001-10-18
    body: "I remember reading about a project which would let you access Psion-organiser through Konqueror. It worked more or less similarly as Psions PsiWin-software. Any news on that project?"
    author: "Janne"
  - subject: "Re: Konqueror & Psion?"
    date: 2001-10-18
    body: "You can download what you need from http://plptools.sourceforge.net/\n\nRich."
    author: "Richard Moore"
  - subject: "Bad KAB, bad.."
    date: 2001-10-18
    body: "When can I use KAB to group persons/groups (make mailinglists)\nAnd share the addressbokk with my company (LDAP sounds good)"
    author: "NO"
  - subject: "PIM with Database Back-end"
    date: 2001-10-19
    body: "I have started a PIM application call VirtuaLaw using QT3 and its database widgets.  The app will be a cross platform QT3 app.  It will be multi-user, allowing for the sharing of calendars and contacts.  It will support PostgreSQL and hopefully a local database that can be accessed through QT3 ODBC.\n\nYou can see a screen shot here:\n\nhttp://dunsinane.net/bryan/virtualaw.png\n\nThe complete version of VirtuaLaw will be a legal case management system.  I will be releasing a version under a different name that just does contact, calendar and mail."
    author: "Bryan Brunton"
  - subject: "Re: PIM with Database Back-end"
    date: 2001-10-19
    body: "Oh, yeah the Source Forge site is here:\n<br><Br>\n<a href=\"http://sourceforge.net/projects/virtualaw\">http://sourceforge.net/projects/virtualaw</a>"
    author: "Bryan Brunton"
  - subject: "Re: PIM with Database Back-end"
    date: 2001-10-19
    body: "looks very nice :)\nhow do you like the database support in qt3? good?"
    author: "Wiggle"
  - subject: "Re: PIM with Database Back-end"
    date: 2001-10-19
    body: "Its hard to rate the database support as I haven't written too many lines of code that use it.\n\nThere are a couple strikes against it.  I think that the Trolls should have done more testing.  At the very least, they need to include more complete example applications that demonstrate rigorous use of the db APIs.  The examples are currently pretty weak.\n\nI found and reported one infinite loop bug in the database code.  Some things are quirky and require too much code: you have to create a property map if you want the text of a qcombo box that is bound to a qsqlform to update the db field.  For some silly reason the Trolls designed it so the field that automatically updates is the index of the qcombobox selected item.\n\nAnother quirk: once you have added columns to qdatabase table, there is no way to retrieve the names of the field that is associated with the column.  \n\nClearly these APIs haven't been fully field tested."
    author: "Bryan Brunton"
  - subject: "kde users have issues"
    date: 2001-10-19
    body: "why are there people who want evolution-type for kde, whats wrong with using evolution within kde? have you guys got a complex over gtk/gnome?"
    author: "mdew"
  - subject: "Re: kde users have issues"
    date: 2001-10-19
    body: "Evolution runs in KDE, but it isn't a KDE app. Meaning, no integration with kab or konqi (you have to manually add it to the email client list, etc), no using the same widget style as other kde apps (unless you are using a gtk style in kde), no control from the system tray or via dcop, and so forth. These are relatively minor issues, but a DE is really designed to be as integrated as possible."
    author: "Carbon"
  - subject: "Re: kde users have issues"
    date: 2001-11-19
    body: "Personally, I have to say that I find GTK/gnome apps tend to have fairly bad UIs, particularly when it comes to file selection - can't say I've used evolution, so have no idea what it is like as an individual app, but I get constantly annoyed by GTK apps that have unfriendly file dialog boxes..."
    author: "Jules"
  - subject: "KOrganiser is still not \"well finished\""
    date: 2001-10-20
    body: "I tested KOrganiser some months ago and I found there was some troubles to use it. I test it again today, I find it is better, but I have quickly found some troubles (KDE 2.2.1) :\n- When I check a category for an event, it is then impossible to uncheck it !\n- About the recall (bell), there are two abilities, sound and program, but it lacks an obvious one : show a popup message\n- About displaying the period as busy or free, I do not see how the two display differ...\n- I see no help, the button \"?\" seems without effects...\n- In the tasks I do not see the difference when the \"No associated hour\" (translated from french) is checked or no...\n\nI am sorry, I find it is not a program ready for the end-user... Here would be the first priority, I think...\n\nAlso about kadressbook I feel as a big weakness that adresses may not be ranged by families."
    author: "Alain"
  - subject: "Re: KOrganiser is still not \"well finished\""
    date: 2001-10-20
    body: "I said :\n\n> - I see no help, the button \"?\" seems without effects..\n\n\"?\" is without effect, yes, but there is a very good help, well translated (I searched for a contextual help). So now I understand what means \"No associated hour\", but I still have no answers about other \"troubles\"."
    author: "Alain"
  - subject: "Re: KOrganiser is still not \"well finished\""
    date: 2001-10-20
    body: "This is the wrong place to put such information. If you'd like these features to be implemented, then the place to put them is at bugs.kde.org, which allows the developers to easily track bug/feature todo lists, order by priority, etc. Putting it here, developers will go \"hey, i could implement that, but i'm busy doing xzy-important-feature and 123-big-bug-fix right now\", and once they have time to do it, they'll already be working on something else.\n\nIt should be possible display a popup message for alarms. I believe there is a command you can do to do have kde display a messagebox with arbitrary text (perhaps a dcop call to knotify, i'm not certain).\n\nAs for kadressbook not being ranged by families, why don't you just do a search for the last name of the particular family you are trying to get a listing for?"
    author: "Carbon"
  - subject: "Re: KOrganiser is still not \"well finished\""
    date: 2001-10-20
    body: "> This is the wrong place to put such information.\n\nI think it is \"a\" (not \"the\") right place. Here it is speaking about the new features of KDE-PIM and I think it is good to say that before improving new features, it is important to finish old ones.\n\n\n> It should be possible display a popup message for alarms. I believe there is a command you can do to do have kde display a messagebox with arbitrary text (perhaps a dcop call to knotify, i'm not certain).\n\nIt is not for the end user. I mean something obvious, integrated, simple. At least written in the documentation...\n\n\n> As for kadressbook not being ranged by families, why don't you just do a search for the last name of the particular family you are trying to get a listing for?\n\n!!??! By families, I mean categories..."
    author: "Alain"
  - subject: "Re: KOrganiser is still not \"well finished\""
    date: 2001-10-20
    body: ">I think it is \"a\" (not \"the\") right place. Here it is speaking about the new features of KDE-PIM and I think it is good to say that before improving new features, it is important to finish old ones.\n\nWell, this is a user discussion forum. If you want to get developers to look at it, it's logical to use a communications medium that primarily developers use. I'm not trying to OT-Nazi you, I'm just saying you'll have considerably better luck with developers actually implmenting said features if you put in the bug/wishlist database.\n\n>!!??! By families, I mean categories...\nAh, nevermind then, I misunderstood."
    author: "Carbon"
  - subject: "Re: KOrganiser is still not \"well finished\""
    date: 2001-10-20
    body: "Categories will be implemented in the new addressbook for KDE3.\nAnd at least in my version a messagebox is shown when it's time.\n\n// Peter S"
    author: "Peter Simonsson"
  - subject: "on a comlpetely unrelated note"
    date: 2001-10-20
    body: "..are there any plans to make konqueror more 'nautilus-like'?\n\nAt the moment konqueror is a good file manager and browser, but nautilus kicks konquerors but in the eyecandy department. But not only that, there are also one or two features that would be nice to see in Konqy.\n\nfor example notes..\n\nokay, make that 'feature' :)\n\nBut honestly, while nautilus is slow (heh, on a pentium 60...) i still prefer it over Konqueror.\n\nJust to make this more general: What is planned for Konqy (browser and filemanager wise)?"
    author: "yatsu"
  - subject: "X improbements"
    date: 2001-10-21
    body: "Would it be posible to fix the jumps with the X system? I mean: When you set the default login as a X login (KDM for example), firts, it appears the console login. Then it shows a X jump to a gray X background, later the kdm background and after it, the kdm login manager. and I think it is not very elegant. It would be posible to fix this jumps?"
    author: "daniel"
  - subject: "X improbements"
    date: 2001-10-21
    body: "Would it be posible to fix the jumps with the X system? I mean: When you set the default login as a X login (KDM for example), firts, it appears the console login. Then it shows a X jump to a gray X background, later the kdm background and after it, the kdm login manager. and I think it is not very elegant. It would be posible to fix this jumps?"
    author: "daniel"
  - subject: "Re: X improbements"
    date: 2001-10-21
    body: "And if you set a image KDM background, you have another jump :("
    author: "frank"
---
<a href="mailto:klaus.staerk@gmx.net">Klaus Staerk</a> alerted us to the launch
of <a href="http://pim.kde.org/">pim.kde.org</a>.
The main goal of this site is to organize KDE Personal Information Management news updates as well as group together all the related projects and goals.
The site already has some basic information about not so well-known PIM
projects such as <a href="http://pim.kde.org/components/kandy.php">Kandy</a>,
<a href="http://pim.kde.org/components/karm.php">Karm</a>, and
<a href="http://pim.kde.org/components/kalarm.php">KAlarm</a>. Also available is information on <a href="http://pim.kde.org/development/common_roadmap-2001-09-21.php">current</a> and <a href="http://pim.kde.org/development/ksync.php">future</a> roadmaps for KDE PIM.
<!--break-->
