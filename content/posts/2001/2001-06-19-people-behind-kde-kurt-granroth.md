---
title: "People behind KDE: Kurt Granroth"
date:    2001-06-19
authors:
  - "Inorog"
slug:    people-behind-kde-kurt-granroth
comments:
  - subject: "Wanted, Wanted"
    date: 2001-06-19
    body: "Choice \"Kurt Granroth\" quotes, for addition to the dot fortune cookie file. Suggest your favorite out-of-context or foot-in-the-mouth or now-that's-just-plain-smart quote for eternal preservation and glorification as a dot fortune."
    author: "Navindra Umanee"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-19
    body: "Slashdot, but not the Dot?\n tsk tsk tsk"
    author: "reihal"
  - subject: "Pun"
    date: 2001-06-19
    body: "Slashdot not the dot.\nMy god am I funny today. :-)"
    author: "Anonymous Coward"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-19
    body: "Just one question: When will kbiff enter kdenetwork and replace korn?"
    author: "someone"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-19
    body: "I ask myself that same question everyday ! ;)"
    author: "Billy Nopants"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-20
    body: "It's not likely anytime soon (if ever).  I removed KBiff from kdenetwork after it became clear that I couldn't maintain it sufficiently (there are simply too many other more important things to do).  I tried to enlist another maintainer but was unsuccessful in finding somebody that would care about it as much as used to.\n\nSo until somebody steps forward, KBiff will remain a sometimes updated app on my website."
    author: "Kurt Granroth"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-20
    body: "Hm, currently many people try to get their hands dirty with kde. Maybe you should retry from time to time."
    author: "Lenny"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-19
    body: "It's good to see that there's at least one person from the US on the KDE team!  I'm excited about all the good work that's going on all over the globe; I think that's one of the real strengths of KDE (and Linux as a whole), but it's also cool to see a Michigander on the team.  I'm pretty sure Kurt's the first person who's been interviewed who both lives in and is from the US."
    author: "Scott"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-20
    body: "My dear husband Waldo, one of KDE's best developers and being payed by SuSE is also in the US, as am I.\n\n\n--\nTink"
    author: "Tink"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-20
    body: "There are actually a decent number of people from the US working on KDE.  I'm thinking about 20.. but I don't know where a few people are actually from so I'm not sure about that.\n\nAlso, I never thought of myself as a Michigander.. I was a Yooper!  But now I'm a Arizonan for life :-)"
    author: "Kurt Granroth"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-20
    body: "Yeah, I'm a Texan in Michigan; looks like we traded--southwest for midwest.  \n\nI find it interesting that there are that many Americans on the team.  How many core members are there worldwide?  \n\nFrom reading the \"People behind KDE\" I've assumed that most of the development goes on in Europe.  It's sad, but I think I've actually read them all. :-)  I'm quite the KDE geek/evangelist."
    author: "Scott"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-20
    body: "Charles samuels (noatun)\nNeil stevens (kit and some noatun)\nTink and Waldo (everything)\nMe (kdenonbeta/krpn, which is broken :)\n\nand thats just in California! ;-)"
    author: "Jason Katz-Brown"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-20
    body: "I'm in Boston. Error403 (KDE installer) is somewhere in the midwest, I think, as are the old konsole maintainer and Ian who did kpp (not to be confused with kppp) and the weather applet.\n\nOh, and Mosfet. ;-)"
    author: "Otter"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-20
    body: "I noticed that I had missed a couple when the dot was down.  However, I still find it interesting that (from what I can tell) Kurt was the first person who was born in the US to be interviewed.  I don't know what nationality the rest of you consider yourselves to be (After how long in a place does it become home?) or if your birthplaces aren't indicative of where you've grown up, etc."
    author: "Scott"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-20
    body: "Mosfet was never interviewed."
    author: "reihal"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-20
    body: "I'd say, he missed the chance forever."
    author: "Lenny"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-20
    body: "Don't say that."
    author: "reihal"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-20
    body: "He's gone forever. Get over it."
    author: "ac"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-22
    body: "He's back!"
    author: "reihal"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-21
    body: "Every once in a while this question comes up. About a year ago Mosfet was one of the first persons I asked for an interview. Although he agreed to answer some questions he never actually returned them. I 'chased'  and 'bugged' him for months about. He let me know that with his performance in clubs etc. he really didn't have time to do an interview and besides that, it would interfere too much with his personal life. He had a life outside KDE that was totally different and he wasn't looking for more publicity.\n\nI'm sorry  but that's it folks. You can't force people, and you have to respect other people's right to privacy.\n\n--\nTink"
    author: "Tink"
  - subject: "Re: People behind KDE: Kurt Granroth"
    date: 2001-06-21
    body: "Congrats all.  I just marvel at the wonderful things that come out of your heads!  This newbie\n is most appreciative of all of you for the hard work I see you put into all aspects of KDE. \n\nI hope to get up to speed, and contribute my energy in some way to some small portion of\nOpen Source!  Right now, I am just promoting GNU/Linux since 1997. I am a retired Air Force\n Techie, and have an affinity for building over-clocked computers (clusters - next?)...\n\nAs a technical writer, I especially enjoy your concise and clear documentation. Well done!"
    author: "Patrick in Orlando"
---
Summer is approaching at full-speed while <a href="mailto:tink@kde.org">Tink</a> pines for her well-deserved holidays. This installment of the <a href="http://www.kde.org/people/people.html">People behind KDE</a> series is quickly nearing a summer break as well, and this week, as well as the next, we will enjoy double interviews.
The week's set of questions are first aimed at <a href="http://www.kde.org/people/kurt.html">Kurt Granroth</a>. A veteran of KDE, Kurt is deeply involved in our project's development and management. He speaks today about his achievements, his interests and his plans.
<!--break-->
