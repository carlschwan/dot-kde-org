---
title: "KDE Plans Large Presence at LinuxTag 2001"
date:    2001-06-23
authors:
  - "Dre"
slug:    kde-plans-large-presence-linuxtag-2001
comments:
  - subject: "Re: KDE Plans Large Presence at LinuxTag 2001"
    date: 2001-06-23
    body: "Interested in who is coming?\n<p>\n<a href=\"http://women.kde.org/events/linuxtag2001/index.html\">http://women.kde.org/events/linuxtag2001/index.html</a>"
    author: "Rob Kaper"
  - subject: "[OT] women.kde.org"
    date: 2001-06-23
    body: "Why no men.kde.org?\n\nI thought the Internet was The Great Equaliser?"
    author: "Yama"
  - subject: "Re: [OT] women.kde.org"
    date: 2001-06-23
    body: "you forget man, basically its ok for woman to have advantages, just not men\n(yep, its fucked, as ive said all along, there should be no advantages for anyone regardless of sex, race age or anything else, then again, also no disadvantages for anyone except disabled people, but ofcourse: that would be fair, so ofcourse it wont happen"
    author: "nervlord"
  - subject: "Re: [OT] women.kde.org"
    date: 2001-06-23
    body: "oops\nmy above msg makes it look like im saiyng disabled peopel should be disadvantaged\ni meant they should be the only people getting any advantages at all"
    author: "nervlord"
  - subject: "Re: [OT] women.kde.org"
    date: 2001-06-23
    body: "> you forget man, basically its ok for woman to\n> have advantages, just not men\n\nWhat advantage?\n\nKDE-Women is a group of (female) KDE developers and users joined efforts to increase visibility of the female developer and user base of KDE and lower the entrance level, which is something I can fully understand in the tech culture which unfortunately still is predominantly male. You could even argue that in such a culture women are actually disadvantaged.\n\nKDE-Women also happens to keep track of KDE related events and they have some other projects as well.\n\nI bet that if you seriously started a project to have lots of software for KDE aimed at children (both educational and games) you could get kids.kde.org to promote your project. Of course noone would even think \"those damn kids, why kids.kde.org, there is no grownups.kde.org!\".\n\nOr perhaps someone would like to have 3rd-world.kde.org to donate hardware with KDE preinstalled to third world countries in Africa, Asia and South America. You wouldn't mention a lack of rich-people.kde.org either."
    author: "Rob Kaper"
  - subject: "Re: [OT] women.kde.org"
    date: 2001-06-23
    body: "i was talking about in general\nits more than ok for there to be a woman.kde.org site, thats killer, means more linux users who are female, which is also good\ni meant in general"
    author: "nervlord"
  - subject: "Re: [OT] women.kde.org"
    date: 2001-06-28
    body: "Except for one glaring inconsistency: KDE is not aimed at men, but children's progs are aimed at kids!  If there are fewer women in programming, it is because women are less wired towards the technical than men, and please don't tell me there is no difference between man and woman brains.  All this 'situational equality' crap ignores the huge and well documented differences in chemical, physical, and genetic makeups between races, sexes, etc.  That is why we have the freedom to pursue, not the freedom to have."
    author: "Chris Bordeman"
  - subject: "Re: [OT] women.kde.org"
    date: 2001-07-30
    body: "Of course there is a difference between a mans brain and a womans - But saying Women are less wired toward the technical is rubbish. \nWomen are not encouraged as much as men to take a technical route. \nI found at school and in college all the attention and praise in technical classes was heaped on to the boys, where the girls were looked at with a \"well, we won't expect much\" attitude. If you treat people like that, they go a different route.\nAnd your statement makes me sound like a freak for being a technical female. Thanks."
    author: "JK"
  - subject: "Re: [OT] women.kde.org"
    date: 2001-07-30
    body: "You're just wrong.  The differences between man and woman brains are much more pronounced than the feminazis would like you to think.  Of course this doesn't mean women are bad at math, just means that women are considerably less likely to be interested in it.  And you're not a freak, nerd girls are really cool!"
    author: "Chris Bordeman"
  - subject: "Re: KDE Plans Large Presence at LinuxTag 2001"
    date: 2001-06-23
    body: "Why is it called Linuxtag if it runs over three days?"
    author: "All_troll_no_tech"
  - subject: "Re: KDE Plans Large Presence at LinuxTag 2001"
    date: 2001-06-23
    body: "Judging from \n<p>\n<a href=\"http://www.linuxtag.org/2001/deutsch/showitem.php3?item=163&lang=en\">http://www.linuxtag.org/2001/deutsch/showitem.php3?item=163&lang=en</a>\n<p>\n\"Since the first LinuxTag in 1996--an evening devoted to GNU/Linux at the University of Kaiserslautern, attended by 70 visitors--the event has grown exponentially.\"\n<p>\nit was one day when it started and although later it lasted for several days it kept its name.\n<p>\nTackat"
    author: "Tackat"
  - subject: "Re: KDE Plans Large Presence at LinuxTag 2001"
    date: 2001-06-25
    body: "Maths problem for anyone interested:\n\nGiven that LinuxTag was 1 day long in 1996, and is 3 days long in 2001; and given that this growth is exponential; in which year will LinuxTag grow to be 366 days long and start to overlap the NEXT year's LinuxTag.\n\nAnswers on a postcard, please."
    author: "Stormie"
  - subject: "Re: KDE Plans Large Presence at LinuxTag 2001"
    date: 2001-06-23
    body: "At the same time, the 4th and 5th July will be the London Linux expo (over 80 exhibitors). What are the plans of the KDE team regarding this event?"
    author: "annma"
  - subject: "London Linux Tag, URL?"
    date: 2001-06-23
    body: "Hmmm, this might get more expensive, but it\nsound interesting. Does anyone have an url at\nhand for this?\n\nSince a flight from Luebeck/Germany to London\nhas about the same price as a rail ticket\nto LinuxTag/Stuttgart, this could be an alternative..."
    author: "Matthias Lange"
  - subject: "Re: London Linux Tag, URL?"
    date: 2001-06-25
    body: "More info on the UK Linux expo is at\n\nhttp://www.linuxexpo.co.uk/index/\n\nHowever note that: \n\n> Sorry, we regret that we cannot accommodate Students or Under 18's"
    author: "Tim Colgate"
  - subject: "Re: London Linux Tag, URL?"
    date: 2001-06-25
    body: "I am running the KDE booth at the London Linux Expo and the Birmingham Linux Expo in September.\n\nWe will be there with info on KDE, a few presentations and more.\n\nCome along and see is if you can. :-)"
    author: "Jono Bacon"
  - subject: "Re: KDE Plans Large Presence at LinuxTag 2001"
    date: 2001-06-23
    body: "There's also the Libre Software Meeting (LSM) at Bordeaux from the 4th to the 8th of July, France.\n\nThat's a real free events, as computers companies doesn't sponsor this event and we don't have to say \"thx ibm\", \"thx compaq\" ...\n\nThe LSM website: http://lsm.abul.org"
    author: "Baptiste Malguy"
  - subject: "Re: KDE Plans Large Presence at LinuxTag 2001"
    date: 2001-06-23
    body: "Well, LinuxTag doesn't cost us anything either - except for promotional articles, hardware and stuff like that (which will be paid by the KDE-League). The furniture and the boothspace is free at linuxtag and the people who have been organizing LinuxTag have been doing so in their spare-time."
    author: "Tackat"
  - subject: "Re: KDE Plans Large Presence at LinuxTag 2001"
    date: 2001-06-23
    body: "I believe Jono will be attending this, it's possible I might be able to make it too. We'll also be having a presence at the Birmingham Expo in September. I'll be posting a story explaining what we'll be doing at the Birmingham expo (papers, a booth etc.) early next week.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: KDE Plans Large Presence at LinuxTag 2001"
    date: 2001-06-23
    body: "Here is the link:\nhttp://www.linuxexpo.co.uk/index/\n\n(sorry, I can't set this answer to HTML so the link is not active :( )\n\nannma"
    author: "annma"
  - subject: "Linus at the KDE Booth"
    date: 2001-06-23
    body: "On page 1 of http://www.linux.com/, there is a photo of Linus Torvalds with the text \"Linus at the KDE Booth\", without any other explanation. \nAnd few more by clicking on the photo and going to the page http://www.linux.com/interact/potd.phtml?potd_id=275\n\nWhat about it ?"
    author: "Alain"
  - subject: "Re: Linus at the KDE Booth"
    date: 2001-06-24
    body: "It's a photo from LinuxWorldExpo 2000 in Frankfurt. On the photo you can see Martin and Ralf presenting the Final Beta of KDE 2.0 to Linus.\n \nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: KDE Plans Large Presence at LinuxTag 2001"
    date: 2001-06-25
    body: "Take lots and lots of pictures!\n<p>\nJustin Cook<br>\n<a href=\"mailto:jsc@themes.org\">mailto:jsc@themes.org</a><br>\n<a href=\"http://kde.themes.org\">http://kde.themes.org</a>"
    author: "Justin Cook"
---
<A HREF="http://www.linuxtag.org/">LinuxTag 2001</A> is the largest Linux and Open Source exhibition in Europe.  Last year over 17,000 visitors and 100 exhibitors attended the event.  This year's event will be hosted in Stuttgart, Germany from July 5 through July 8.  The KDE Team will have a large presence there, including several presentations and workshops, a large number of KDE developers, and of course the KDE mascot <EM>Konqi</EM>.  Read the press release below. <b>Update: 06/30 12:43 PM</b> by <a href="mailto:navindra@kde.org">N</a>: Ralf has assembled <a href="http://master.kde.org/~nolden/linuxtag2001.html">some updated information</a>. 
<!--break-->
<P>&nbsp;</P>
<H3>KDE @ LinuxTag 2001</H3>
<P>
The <A HREF="http://www.kde.org/">KDE Team</A> today announced its plans for
<A HREF="http://www.linuxtag.org/">LinuxTag 2001</A>, the biggest Linux and
Open Source exhibition in Europe (last year 17000 visitors and 100 exhibitors
attended).  LinuxTag 2001 will take place from July 5th to July 8th in
Stuttgart, Germany.
</P>
<P>
Besides staffing a KDE booth where users can see the latest KDE, ask
questions and mingle with KDE developers, KDE developers will make numerous
presentations targeted at users and developers.  The <EM>Congress Program</EM>
will feature talks by the notable KDE developers
<A HREF="http://www.kde.org/people/matthias.html">Matthias Ettrich</A> and
Kalle Dalheimer. The <EM>Forum Program</EM> will offer a talk about
<EM>Developing a GUI Using Qt</EM>, and programs in German about
<EM>Multimedia in KDE 2</EM> and  <EM>KDE 2.2 - Your Personal Desktop</EM>.
In addition, developers will host several workshops on KDE development.
</P>
<P>
The KDE Team would like to thank the <A HREF="http://www.kdeleague.org/">KDE
League Inc.</A>, <A HREF="http://www.borland.com/">Borland Software
Corporation</A>, <A HREF="http://www.calderasystems.com/">Caldera Systems
Inc.</A>, <A HREF="http://www.compaq.com/">Compaq Computer Corporation</A>,
<A HREF="http://www.fsc-usa.com/rc/index.html">Fujitsu Siemens Computers</A>,
<A HREF="http://www.FutureTG.com/">Future Technologies Inc.</A>,
<A HREF="http://www.hancom.com/en/">HancomLinux Inc.</A>,
<A HREF="http://www.hp.com/">Hewlett-Packard Company</A>,
<A HREF="http://www.ibm.com/">IBM</A>,
<A HREF="http://www.kde.com/">KDE.com</A>,
<A HREF="http://www.klaralvdalens-datakonsult.se/">Klarälvdalens Datakonsult AB</A>,
<A HREF="http://www.mandrakesoft.com/">Mandrakesoft</A>,
<A HREF="http://www.mizi.com/">Mizi Research Inc.</A>,
<A HREF="http://www.redhat.com/">Redhat Inc.</A>,
<A HREF="http://www.suse.com/">SuSE GmbH</A>,
<A HREF="http://www.trolltech.com/">Trolltech AS</A>, and
<A HREF="http://www.turbolinux.com/">TurboLinux Inc.</A> for helping KDE
at LinuxTag 2001.
</P>
<P>
We invite everybody to meet more than 40 KDE developers, KDE enthusiasts and
of course our mascot <EM>Konqi</EM> at the KDE booth.
</P>
<P>
For more information on LinuxTag, see
<A HREF="http://www.linuxtag.org/">http://www.linuxtag.org/</A>.
For more information on KDE, see
<A HREF="http://www.kde.org/">http://www.kde.org/</A>.
For information relating to this press release, please contact
<A HREF="mailto:tackat@kde.org">Torsten Rahn</A>.
</P>