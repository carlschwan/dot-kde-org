---
title: "KDE 2.2 Ships (Visit an FTP Server Near You)"
date:    2001-08-15
authors:
  - "Dre"
slug:    kde-22-ships-visit-ftp-server-near-you
comments:
  - subject: "Something wrong."
    date: 2001-08-15
    body: "Is there something that I miss, or why is it that Redhat 7.x i386 packages have no kdebase or kdelibs listed?"
    author: "Rithvik"
  - subject: "Re: Something wrong."
    date: 2001-08-15
    body: "Maybe because they didn't work?\n\nI downloaded them yesterday and tried to install them this morning. They had several wierd dependencies (incl. fam. Imon patch doesn't work with 2.4.8) that I had to get from rpmfind because they are not in RH7.1\n\n...once installed KDE crashes on startup...\n\nKDE is the WORST thing to install/upgrade (I really like it when it works though)"
    author: "blah"
  - subject: "Re: Something wrong."
    date: 2001-08-15
    body: "The main problem results from a wrong installed fam. Get the fam package from ftp://ftp.kde.org/pub/kde/contrib. The binary did not work for me, so I tried to rebuild the source which did not work automatically. I had to include a <tt>#define NULL 0</tt> in include BTree.h . Also the test directory had to be removed from the Makefile. A working version can be found under http://www.aerie.at/rh71kde .\n\nYou will also need to correctly configure xinetd if you are running this daemon ( most likely if you run RH71 ). You will find my fam config file under the location meant above. Just install it under /etc/xinetd.d and restart the daemon. BTW, this I write under KDE2.2. It runs just fine, after installation troubles ;-)\n\nCU Roland"
    author: "Roland"
  - subject: "Re: Something wrong."
    date: 2001-08-15
    body: "\"The binary did not work for me, so I tried to rebuild the source which did not work automatically. I had to include a <tt>#define NULL 0</tt> in include BTree.h .\"\n\nI think someone forgot to #include<stdlib.h>, but I'm glad to see you worked it out. :-)\n\n\"Also the test directory had to be removed from the Makefile.\"\n\nI think this is only the case if you do not have imon support in the kernel, but this should not happen.  I will see if I can fix it.\n\nFeel free to contact the list (fam@oss.sgi.com) with any problems or feedback.\n\nThanks.\n\n-- \nMICHAEL WARDLE\nSGI FAM Maintainer"
    author: "Michael Wardle"
  - subject: "Re: Something wrong."
    date: 2001-08-15
    body: "I use SuSE 7.2, and had no troubles at all installing the rpm's :-)"
    author: "Steven"
  - subject: "Re: Something wrong."
    date: 2001-08-31
    body: "You mentioned that you used suse 7.2. I downloaded all the rpms from the Suse site (kde support section) under the base packages and I got lots of dependency errors. After much messing about I got it installed but the only problem is I'm getting errors (as a user not a roo) like not being able to access dev/video (perm denied).\nWhere and how did you get your rpms from ?"
    author: "Andy O"
  - subject: "Re: Something wrong."
    date: 2001-08-15
    body: "\"fam. Imon patch doesn't work with 2.4.8\"\n\nThe latest Linux version the imon patch has been verified to work on is 2.4.2.  I am working on a release for 2.4.6 and 2.4.8, and they should be out very soon.\n\nYou will probably notice that fam works without imon support in the kernel, but the performance may be somewhat worse.\n\nIf you have any problems, please contact me, or the fam project mailing list fam@oss.sgi.com.\n\nBy the way: do you know which component of KDE relies on fam?  I understand that fam support had been added to KDirWatch, but I'm not sure that this made it into the release version of KDE.\n\nThanks.\n\n-- \nMICHAEL WARDLE\nSGI FAM Maintainer"
    author: "Michael Wardle"
  - subject: "Re: Something wrong."
    date: 2001-08-15
    body: "Will the 2.4.6 patch work with 2.4.7?  That's what I run at home and at work, since I've not heard one person that says 2.4.8 is stable.\n\nJason."
    author: "Jason Tackaberry"
  - subject: "Re: Something wrong."
    date: 2001-08-15
    body: "I had a similar problem...I downloaded yesterday and found lots of things missing and crashes on starting KDE.  Found out that they did have all of the packages up 'til after 5:00 EST.\nI went back and downloaded all of the stuff under the 7.x i386 dir and got everything out of the non-kde dir and it worked fine."
    author: "Steven Gilroy"
  - subject: "Installing KDE RPMs on RedHat HOWTO"
    date: 2001-08-15
    body: "Hello folks.\n\nI have successfully installed KDE 2.2 on my RedHat 7.1 system using RPMs. Since many people seem to be having trouble, here is how you should do it.\n\nNote that a very easy way to install KDE 2.2 RPMs is to use --force --nodeps, but that screws your system slightly. What I describe takes longer, but  it is clean (mostly).\n\n1. Know your dependencies\n\nDownload all the KDE RPMs in one directory.\n\ncd kde_rpms_directory\nrpm -Uvh *.rpm\n\nMake a note of all missing packages or files. Then, go to rpmfind.net. Enter the exact name of the missing file or package and download the appropriate package. Always get the RPMs built for  RedHat Rawhide (their current development tree).\n\nHere are the packages I personnally had to download:\n\nlibfam\nopenssl\nlibxml2\nlibxsltpcre (and pcre-devel, I think)\n\nYou might also have to get the following:\n\nlibogg\nlibvorbis\n\n\n2. Ogg and Vorbis\n\nIf you have Ximian Gnome installed, you are in an interesting situation. rpm does not believe these packages to be installed, but they are -- under different, Ximian-chosen names. Just ignore that specific error message.\n\nIf you do not have Ximian Gnome, do not ignore the message. Get the RPMs and install them.\n\n\n3. The openssl problem.\n\nopenssl is a special case. If you try to upgrade it, you will be told that many packages need it. That is true. What is also true is that both these libraries can co-exist on a system. Do the following:\n\nrpm -i --test openssl*.rpm\n\nThe only error message you will see is that some one file clashes. That file is not the library itself, but an executable binary. This is a file you can safely overwrite with a newer version.\n\nrpm -i --replacefiles openssl*.rpm\n\nNow you have both versions installed.\n\n\n4. The pcre problem\n\nYou will not be able to install the pcre RPMs because they would overwrite files from your already-installed kdesupport RPM. So we will have to remove kdesupport before we install pcre... We will do that later.\n\n\n5. The other dependencies\n\nThe other RPMs I listed do not cause any problems. Install or upgrade them. If you need additional ones (it could happen), get them from RPMfind and install or upgrade them.\n\n\n6. Uninstalling KDE\n\nLog out and use an old-fashioned tty login to log\non as root. Or switch to another desktop environment and open a root console. Do the following:\n\nrpm -e kdelibs\n\nIt will not work. You will get an error message telling you about all the packages that depend on it. Make sure these are just the normal KDE packages (they were for me), and remove them. The, you can remove kdelibs and kdesupport\n\nrpm -e kdemultimedia\nrpm -e kdeadmin\nrpm -e koffice\n...\nrpm -e kdelibsrpm -e kdesupport\n\n\n7. Re-installing KDE\n\nFirst, install the problematic pcre RPMs we skipped earlier.\n\nrpm -Uvh pcre*.rpm\n\nFinally, you are ready to go.\n\ncd kde_rpms_directory\nrpm -Uvh *.rpm\n\n8. More problems?\n\nIf you have Ximian gnome installed (like I do), you will have the libogg and libvorbis problem I mentioned earlier. Some multimedia packages will complain about it. Use the \"--nodeps\" flag. There is not much else you can do.\n\nrpm -i --nodeps kdelibs-sound*.rpm\n\nIn my case, I did not install kdebindings-python because I did not want to install Python 2.1. I do not recommend upgrading Python right away unless you are certain you need kdebindings-python.\n\nI have removed koffice and not re-installed it because (a) I do not use it much, (b) want to wait for koffice 1.1, and (c) it depends on kdesupport, which I no longer have. However, if you still want koffice 1.0, do the following:\n\nrpm -i --nodeps koffice*.rpm\n\nThe missing kdesupport package annoys rpm but since all kdesupport components have been installed separately, there is no real problem.\n\n\n9 Enjoy!\n\nI have just started using KDE 2.2. It  seems wonderful. Enjoy yourselves, and report those bugs."
    author: "Jerome Loisel"
  - subject: "HOWTO Errata"
    date: 2001-08-15
    body: "Hello again,\n\nThe package list should have read:\n\nlibfam\nopenssl\nlibxml2\nlibxslt\npcre (and pcre-devel, I think)\n\nAn important note abour rpm:\n\nkdeadmin will whine about not having rpm 4.0.3. That is because kpackage has been built against it. I do not recommend upgrading rpm. That can be downright dangerous at times (though it shouldn't be in most cases).\n\nSolution: install kdeadmin with \"--nodeps\" and do not expect kpackage to work properly. That is what I did. I can live with a non-working kpackage, as you can probably tell. :-)\n\nThe \"Uninstalling KDE\" part should have read:\n\n...\nrpm -e kdelibs\nrpm -e kdesupport\n\nYes, I think that both these posts sum up why many people prefer Debian now. :-)"
    author: "Jerome Loisel"
  - subject: "Re: Installing KDE RPMs on RedHat HOWTO"
    date: 2001-08-15
    body: "All I did was download roswell isos, install Roswell, then download all KDE rpms and type rpm-Uvh *.rpm, seemed to work fine for me (except for python-bindings which I simply just did not bother to install)"
    author: "C. Stone"
  - subject: "Re: Installing KDE RPMs on RedHat HOWTO"
    date: 2001-08-15
    body: "I've also written a short HOWTO for installing the RedHat RPMs, and it has worked for me and a few others. It's not perfect, but as I said, it works for me.\n<br><br>\nPlease note, my solution needs no files from Rawhide, and just uses the files available on the kde ftp site.\n<br><br>\nThe HOWTO is at <br>a href=\"http://www.athame.co.uk/kde-redhat.html\">http://www.athame.co.uk/kde-redhat.html</a>"
    author: "Andy Fawcett"
  - subject: "Re: Installing KDE RPMs on RedHat HOWTO"
    date: 2001-08-15
    body: "Heh, I love Zope, I really do :)\n\nSorry for the fscked up URL, you'll just have to cut and paste :)\n\nAndy"
    author: "Andy Fawcett"
  - subject: "rpm 4.0.3 and other RH 7.1 updates"
    date: 2001-08-15
    body: "Visit:\n\nhttp://rpmfind.net/linux/rawhide/1.0/i386/RedHat/RPMS\n\nto get the latest RH 7.1 updates including rpm 4.0.3, SDL, openssl etc."
    author: "Asif Ali Rizwaan"
  - subject: "Re: rpm 4.0.3 and other RH 7.1 updates"
    date: 2001-08-16
    body: "Thanks Debian! :)\nI see lotsa lesser mortals having\nproblems with other distros\n\n----\napt-get into it"
    author: "ac"
  - subject: "Re: rpm 4.0.3 and other RH 7.1 updates"
    date: 2001-08-16
    body: "The so called RH7.1 upgrade is NOT for RH7.1 !!\nA RedHat 7.1 upgrade of KDE that requires me to first upgrade glibc? What on earth is that. I'll wind up installing RedHat 7.2 beta before i can get this baby going. This is not good. Looking forward to a series of rpm's actually build for RedHat 7.1. Thankyou."
    author: "R.K."
  - subject: "Re: rpm 4.0.3 and other RH 7.1 updates"
    date: 2001-08-17
    body: "You're right. Bero (the packager) said on Slashdot that the packages are meant for the upcoming RH7.2. So you can either wait for that, or install new sh-utils and glibc-packages plus all the stuff non-kde directory before trying to install the kde-packages. Even then you will break something minor like gnorpm.\n\nI actually tried to satisfy all the dependencies with the latest Rawhide rpms, but gave up after an our or so of package hunting. Anyway, KDE2.2 works nicely with the rpms I mentioned above."
    author: "Janne S"
  - subject: "Re: rpm 4.0.3 and other RH 7.1 updates"
    date: 2001-08-17
    body: "I ran into weird problems and decided I'd rebuild everything myself.  The results are at http://www.opennms.org/~ben/kde2.2/ (sorry, all I get as an encoding choice is \"Plain Text\") -- you may need Ximian Gnome installed to meet some of the dependencies (not sure, I already had it installed when I started building -- libxml2 and such may get picky on you).  I also made a \"kdeliquid\" RPM with mosfet's liquid theme engine.\n\nYou should be able to pull the whole big directory down and do \"rpm -Uvh *.rpm\" and it will work just fine (I think -- haven't tried on any other 7.1 boxen)."
    author: "Ranger Rick"
  - subject: "Re: rpm 4.0.3 and other RH 7.1 updates"
    date: 2001-08-19
    body: "Oh well. I've shopped every official RH release till now, cept 7.0. I'll do it again. Looking forward to meet KDE2.2 in RH7.2 then. (Will study the new bootloaded in the meanwhile.)\n\nFWIW: I would have gladly have bought a Ximian'alike CD of KDE for RH7.1, but i guess some serious market research needs doing before the KDE-team jumps that bandwagon."
    author: "R.K."
  - subject: "Re: rpm 4.0.3 and other RH 7.1 updates"
    date: 2001-08-19
    body: "doh.. i'm lying - shopped every release since 4.2 that is."
    author: "R.K."
  - subject: "Re: Something wrong."
    date: 2001-08-15
    body: "For all of you who are asking about CUPS/KDEPrint: to quote the really superb (even if incomplete) KDEPrint Handbook: \n        #### \n\n<quote> \nThis handbook describes KDEPrint. KDEPrint is [...] an intermediate layer between the KDE (or other) application and the selected (and installed) print subystem of your Operating System... \n\nKDEPrint is <emphasis>not</emphasis> a replacement of the printing subsystem itself. KDEPrint therefore does <emphasis>not</emphasis> provide for the spooling, and it does <emphasis>not</emphasis> do the basic processing of PostScript or other print data.... \n\nIt should be noted that both the developer of this application, and the author of this document are most familiar with CUPS as a printing system. At the time of writing, CUPS is the best supported printing subsystem, and it is the best documented. This handbook is work in progress, and later versions of the KDEPrint software and editions of this handbook will support and explore more closely other printing systems. \n\nIn the meantime, even if your printing subsystem is not yet well covered, you are encouraged to explore the Printing Manager module in KControl, and you will find its operation to hopefully be fairly self evident, no matter what printing  subsystem you use. </quote> \n\nSo it's up to you, not KDE, to install CUPS if it is your favourite. Or tell you distributor to bundle it as default...  ;-)"
    author: "me-the-cups-fan"
  - subject: "object prelinking"
    date: 2001-08-15
    body: "So they decided to do it anyway, any details?"
    author: "Yatsu"
  - subject: "Re: object prelinking"
    date: 2001-08-15
    body: "It's not contained in the [source code] release. Some packagers/distributors may have applied it to their packages on their own."
    author: "someone"
  - subject: "Re: object prelinking"
    date: 2001-08-15
    body: "ah, thank you.. i forgot KDE only supplied the source code.\n\n..and would anyone know if debian (sid) has this applied in it's kde packages?"
    author: "Yatsu"
  - subject: "Re: object prelinking"
    date: 2001-08-15
    body: "i think so, altough i did not notice much difference (maybe because my disks have always been the bottleneck)"
    author: "ik"
  - subject: "Re: object prelinking"
    date: 2001-08-15
    body: "I read on freekde or gui-lords or somewhere that it does, but I haven't had time to upgrade yet."
    author: "Erik Severinghaus"
  - subject: "Re: object prelinking"
    date: 2001-08-15
    body: "..and would anyone know if debian (sid) has this applied in it's kde packages?\n\nYes it does, and on my old 64MB P2-233, it makes all the difference.  Konqueror absolutely flies in use now as each embedded KPart loads in milliseconds rather than a second or two.  Startup time for Konqui and KMail (the two worst offenders in KDE) are about halved, indeed KMail now appears almost instantly - this on such an old system, despite the 15000+ mailinglist emails lying about.\n\nIf you haven't upgraded to the 2.4.8 kernel yet, that comes recommended too, as there are some changes to the VM that make it FAR nicer in interactive use like KDE.\n\n(2.4.8 still goes completely loony if you load it really heavily and are running out of swapspace, but this rarely happens on the desktop - I happily run and work with Konqui, Mozilla, the GIMP, KMail and licq simultaneously without much slowdown - this on a 64MB machine!)\n\nMy comparison of speed on this old box has always been against Win98.  Whilst Win98 is generally very sucky, it has pretty low overheads and thus runs very fast, especially so on such old slow, memory-light machines.\n\nKDE2.2 running with a 2.4.8 kernel now surpasses Win98 in terms of speed, and of course beats it to a bloody pulp (and has done for a very long time) in terms of stability, beauty and power.  Win2k, KDE's real competitor, is bigger, heavier, slower, runs like a slug by comparison.  And it's still not as pretty!\n\nWell done KDE team.  You've gone from obscure niche to one of the most important desktop GUIs in existence in what... 2, 3 years? - MacOS X and Windows XP are the only others in the same league, IMHO.  KDE is the lightest, fastest, most powerful and most configurable of all of them.\n\nLooking forward to KDE3 changing the world some more..."
    author: "Amazed of London"
  - subject: "Re: object prelinking"
    date: 2001-08-16
    body: "> KMail now appears almost instantly - this on\n> such an old system, despite the 15000+\n> mailinglist emails lying about.\n\nIf you have thousands of messages in say the inbox and sent-mail folders then this speed up is probably primarily due to the rewritten folder code in KMail rather than object prelinking.\n\nTo be frank I'm just glad that people have noticed it's faster.\n\nAbout being 'almost instant', yes there is still room for improvement :-)\n\nDon."
    author: "Don Sanders"
  - subject: "Re: object prelinking (Mandrake 8.0 RPMS)"
    date: 2001-08-15
    body: "Mandrake 8.0 RPMS comiled with optimizations can be found via http://www.pclinuxonline.com/"
    author: "R. Bos"
  - subject: "Re: object prelinking"
    date: 2001-08-15
    body: "There seems to be an experimenal directory in the SuSE dirs, it puts stuff in /opt/kde2/lib/i686, is this the prelinked stuff ?"
    author: "frido"
  - subject: "Re: object prelinking"
    date: 2001-08-16
    body: "Are these patches available somewhere?\nOr are they already comitted to CVS?\n\nThanks"
    author: "raph"
  - subject: "Re: object prelinking"
    date: 2001-08-16
    body: "Somewhere? Sure: http://www.research.att.com/~leonb/objprelink/"
    author: "someone"
  - subject: "Re: object prelinking"
    date: 2001-08-16
    body: "This is a bit confusing. The press release seems to indicate that prelinking is available. I does not seem to be. \n\nI installed objprelink and applied the configure-patch but I got some strange errors compiling kdelibs. I'm trying again without it right now.\n\nAnyone compiling themselves who got it to work? Any tricks to it?"
    author: "jd"
  - subject: "Re: object prelinking"
    date: 2001-08-23
    body: "This is very late....\n\nIf you're using a RedHat or Mandrake system, use the kg++ and kgcc compilers. Yes, you've got two compilers on your system. The K compilers are an older and more stable version of GCC, the default version isn't really any good (which is fair enough considering the improvements the GCC team is making IMHO)\n\nI can never remember the right order for these commands - basically\nrename gcc gcc.bak\nrename cc cc.bak\nrename c++ c++.bak\nrename g++ g++.bak\n\nThen create symbolic links\ngcc -> kgcc\ncc -> gcc\ng++ -> kg++\nc++ -> g++\n\n\nDespite all that, I never got my KDE to compile - but fortunately pclinuxonline have prelinked i686 RPMs. I have my suspicions about Mandrake 8.0's dev cred."
    author: "Bryan Feeney"
  - subject: "compiling kdelibs"
    date: 2001-08-15
    body: "I downloaded kdelibs (KDE_2_2_RELEASE) from CVS.\n\nAfter running \"make -f Makefile.cvs\" I got some warnings about \"AC_TRY_RUN\" which had no default\nfor the purpose of cross compiling.\nI started ./configure ... and got an error.\nA test program couldn't execute, because of cross compiling.\n\nCan anybody give me a solution to compile kdelibs properly?"
    author: "gjw"
  - subject: "Re: compiling kdelibs"
    date: 2001-08-15
    body: "RTFM:\n\nhttp://www.kde.org/documentation/faq/install.html\nhttp://developer.kde.org/documentation/other/compiling.html\n\nHope this helps. General hint: add the EXACT error message or nobody can help you but yourself."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: compiling kdelibs"
    date: 2001-08-15
    body: "##kdelibs KDE_2_2_RELEASE\n\n#make -f Makefile.cvs\n\n*** Creating configure\nconfigure.in:92: warning: AC_TRY_RUN called without default to allow cross compiling\nconfigure.in:757: warning: AC_TRY_RUN called without default to allow cross compiling\nconfigure.in:1217: warning: AC_TRY_RUN called without default to allow cross compiling\n*** Creating config.h template\nconfigure.in:92: warning: AC_TRY_RUN called without default to allow cross compiling\nconfigure.in:757: warning: AC_TRY_RUN called without default to allow cross compiling\nconfigure.in:1217: warning: AC_TRY_RUN called without default to allow cross compiling\n\n\n#./configure\n\nchecking if getaddrinfo works using numeric service with null host... configure: error: can not run test program while cross compiling"
    author: "gjw"
  - subject: "Re: compiling kdelibs"
    date: 2001-08-16
    body: "Thgese warnings are okay, I got these, too, but the packages compiled anyway. So I don#t know why it fails for you but it#s not because of the warnings.\nMaybe you have a cross compiler installed but did not know or want to have it? Try it with a standard gcc...\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "It was a linker problem"
    date: 2001-08-18
    body: "It was a linker problem so that the test programm couldn't be executed.\nSomething messed up my system (maybe some SuSE rpms).\nI reinstalled my SGI RedHat Linux 7.1 XFS\nand now everything seems to work fine.\n\nHave a nice weekend and enjoy KDE\n\nG.J.W."
    author: "gjw"
  - subject: "Re: compiling kdelibs"
    date: 2001-08-16
    body: "> RTFM:\n> \n> http://www.kde.org/documentation/faq/install.html\n> http://developer.kde.org/documentation/other/compiling.html\n\nThis didn't help and I read them before.\n\nWhat about the packagers?\nMaybe they had the same problem?"
    author: "gjw"
  - subject: "Failed dependencies"
    date: 2001-08-15
    body: "Hi !\n\nKDE 2.2 fails to install on a Redhat 7.1, due\nto dependency errors :\n\nlibrpm-4.0.3.so   is needed by kdeadmin-2.2-1\n        librpmbuild-4.0.3.so   is needed by kdeadmin-2.2-1\n        librpmdb-4.0.3.so   is needed by kdeadmin-2.2-1\n        librpmio-4.0.3.so   is needed by kdeadmin-2.2-1\n        libcrypto.so.2   is needed by kdebase-2.2-1\n        libssl.so.2   is needed by kdebase-2.2-1\n        python2 >= 2.1 is needed by kdebindings-python-2.2-1\n        kdesupport is needed by koffice-2.0.1-2\n\nI rebuild rpm-4.0.3-0.88.src.rpm but then I got\na lot of other dependency errors when trying to install it.\n\nWhat does this mean ? Many hours of work in order to upgrade 90 % of the system ?\nMartin."
    author: "Martin Andersen"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "I fear yes. I've rant about this in previous article, so I don't want to repeat myself.\n\nI've just build new openssl package, but the system refuses to upgrate it, because plenty of packages depend on libssl.so.1.\n\nI always get a strange taste in my mouth when my only choice seems to be --nodeps or --force."
    author: "Marko Samastur"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "To install kde2.2 , you will need some extra packages. Download binaries from rpmfind.net. In case of dependency problem about libssl and libcrypto, just put a symbolic link of lib*.so.2 to lib*.so.1 . Thats' it. I got it working within 5 minutes ! yes, dont't upgrade packages independently but upgrade them simultaneously with command like \"rpm -Uvh *\" . Otherwise you will be in trouble ."
    author: "vk"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "I've always liked dependencies in GNOME. It required about forty upgrades before it started. It seems that KDE finally reached this level - bravo KDE guys, we beat those GNOMES once again ! Look at those posts - the newest KDE packages won't run on SUSE, Mandrake, RedHat, Caldera ( not mention FreeBSD's or Sun's ). SUSE users must --force --nodeps all RPMS, but it seems that RH users are recommended to change glibc !. The next release probably will run seamlessly only on Microsoft Windows !"
    author: "Bee"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "Oops.\n\nMy KDE 2.2 on SuSE 7.2 seems to be a hallucination. And I installed via\n\n  rpm -Uvh *.rpm\n\nPerhaps it makes a difference to use the \"official\" SuSE binaries from the *SuSE* server?"
    author: "anonymous"
  - subject: "Re: Failed dependencies"
    date: 2001-08-16
    body: "I'm glad to hear that it worked on Suse, but I have RedHat and I'm surely not going to change my glibc., so I have to wait. It is probably Suse itself, which provides those packages and  I hope RedHat will also provide its packages in a few weeks. We'll see."
    author: "Bee"
  - subject: "Re: Failed dependencies"
    date: 2001-08-16
    body: "Correct me if I am wrong,\n\nBut KDE don't make the binaries they only give out the source code. So shouldn't you be ranting at RedHat instead of the nice people at KDE?\n\nI am one of those strange people that likes to compile from source. And I have had no problems so far (Only done libs and base).\n\nFeel free to shoot me I'm having a bad week at work anyway :)\n\nRoyce"
    author: "Royce"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "RedHat has backwards-compatibility rpms called \"openssl095\" and such -- install these (see rpmfind.new or ftp to rawhide.redhat.com/redhat/linux/rawhide/) to satisfy rpms that use the old libraries"
    author: "Matthew Kay"
  - subject: "Re: Failed dependencies"
    date: 2001-08-16
    body: "i have no sucess.  when i startx, i get the splash screen it says kde is up and running.  but i don't get anything 'cept the background.  i'm running on suse 7.1 i first downloaded the packages from a suse mirror.  i noticed that the mirror was not the same as suse.com.  i tried the binaried there.  i finally had to run rpm with --nodeps and --force.  i kept getting depndency errors for packages that were installed.  i don't know what else to do, or what i'm missing.  i don't know what else to try at the moment its just been one big frustration.  and i can't get the previous version to work either."
    author: "zed aardvark"
  - subject: "Re: Failed dependencies"
    date: 2001-08-16
    body: "Yes I have a similar problem. I have rh7.1 I notice that kded is running twice when I do a \nps aux."
    author: "john Mallett"
  - subject: "Re: Failed dependencies"
    date: 2001-08-16
    body: "Hmm, what about switching to debian :)\n\nWhy don't you guys satisfy the RPM dependencies, that is what they are for!\n\nIf you install with --nodeps or whatever, yes you are bound to get problems.\n\nBesides, there should be different RPMs for different versions (of your OS), and they should install smoothly. If not, ask the packager."
    author: "Thomas Zander"
  - subject: "Re: Failed dependencies"
    date: 2001-08-17
    body: "I agree a lot of dependecies to be satisfied but it is worth it.  I use RedHat 7.1 and installed all the updates from the beta/roswell dir on one of the mirrors that where not in the non-kde dir except for python as that breaks my favourite card game pysol.  I had to disable auth in /etc/fam.conf for programs to load corectly, I think that may be due to the fact I use ldap, anyone comment?  DON'T use nodeps or force unless you are positive it won't break something you need.\n\nI have had no problems except for cosmetic ones like icons missing etc and the system has been up 3 days so far.  A few sites crash konq but that has always been there and java still does not work correctly with 1.3 from Sun, can anyone recommend one that does?\n\nGood luck to all who go down this path, try it on a system that does not matter if you blow it away, not your production system until your are sure all is fine:-) On the other hand wait for 7.2 or what ever but you do have it backed-up don't you?\n\nColin"
    author: "Colin"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "You may ignore the kdesupport dependency. RPM 4.0.3 has not been released yet as stable for RH71 by RedHat itself, although I did not run into any troubles without it (using --force and --nodeps).\n\nlibcrypto.so.2 and libssl.so.2 although are a little nastier. These are neede for HTTPS. What you might try is to create links from the openssl libraries, which might work ( it does for me ). OpenSSL is actually at 0.9.6b, and I really have no idea which library provides the correct dependency. Python you might only need if you actually use Python for scripting or you are developing with it yourself. \n\nThe biggest problem I ran into was installing FAM. Therefore see my site at http://www.aerie.at/rh71kde/\n\nCU Roland"
    author: "Roland"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "there is actually a non-kde directory that isn't off the RPMS dir, it is off a parent dir.\n\nIt has all of the packages you will need to support kde 2.2\n\nEv."
    author: "Evan Read"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "I'm aware of these extra files, they\ndid not solve many dependency problems.\n\nMartin."
    author: "Martin Andersen"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "Last night new rpm came in for the non-KDE bunch. But two problems remain:\n\nKPackage won't work with my (RH 7.1) rpm-package, the upgrade to the rpm-packages in non-KDE breaks dependency with glibc. No way here!\n\nSecond problem is although I have installed everything that lies below 2.2./rpm/redhat/7.x except of rpm-4.0.3-x there is _always_ a core-file in $HOME when logging out of KDE."
    author: "Oliver"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "try this:\ngdb -core $HOME/core. it will tell you what program (probably a non kde program since kde programs have crash dialogs) generated the core dump.\nthen do\ngdb -core $HOME/core /path/to/program\nand type 'bt'\nand copy the output to a file and send it as a bug report to the maintainer of that program."
    author: "ik"
  - subject: "Re: Failed dependencies"
    date: 2001-08-16
    body: "OK, I have the same problem with 2.1.1 (RH 7.1). This is the backtrace:\n#0  0x40ccd00a in chunk_free (ar_ptr=0x40d75f00, p=0x34768) at malloc.c:3125\n#1  0x40cccd59 in __libc_free (mem=0x808a7e0) at malloc.c:3054\n#2  0x40c0a136 in __builtin_delete (ptr=0x808a7e0) from /usr/lib/libstdc++-libc6.2-2.so.3\n#3  0x4077875d in QObject::~QObject () from /usr/lib/qt-2.3.0/lib/libqt.so.2\n#4  0x4053799e in KInstance::~KInstance () at eval.c:41\n#5  0x404d2e05 in KApplication::~KApplication () at eval.c:41\n#6  0x40566f99 in KUniqueApplication::~KUniqueApplication () at eval.c:41\n#7  0x4114ee62 in main () from /usr/lib/kdesktop.so\n#8  0x0804a49b in strcpy () at ../sysdeps/generic/strcpy.c:31\n#9  0x0804add1 in strcpy () at ../sysdeps/generic/strcpy.c:31\n#10 0x0804b2a6 in strcpy () at ../sysdeps/generic/strcpy.c:31\n#11 0x0804bff1 in strcpy () at ../sysdeps/generic/strcpy.c:31\n#12 0x40c69177 in __libc_start_main (main=0x804bc20 <strcpy+7972>, argc=2, ubp_av=0xbffffbfc, init=0x8049764 <_init>, fini=0x804c460 <_fini>,\n    rtld_fini=0x4000e184 <_dl_fini>, stack_end=0xbffffbec) at ../sysdeps/generic/libc-start.c:129\n\nLooks like a delete[] problem, no ?"
    author: "Biswa"
  - subject: "Re: Failed dependencies"
    date: 2001-08-16
    body: "I'm in a similar (identical?) situation.  Here's the details after upgrading sh-*, glibc*, and the other recommended rpms from rawhide;\n\n$ file core;\n\n  \"core: ELF 32-bit LSB core file of 'kdeinit' (signal 11), Intel 80386, version 1\"\n\n$ gdb ~/core\n\n  \"... This GDB was configured as \"i386-redhat-linux\"...\"/home/USERNAME/core\": \nnot in executable format: File format not recognized\n\n  (gdb) bt\n  No stack.\"\n\nBefore updating glibc* and sh-* from Rawhide, gdb did report the same 12 line output as Biswa mentioned in his/her reply.  For completeness, here are a few more details...\n\n$ cat ~/.xsession-errors;\n\n  \"DCOPServer up and running.\n  kdeinit: Shutting down running client.\n  ---------------------------------\n  It looks like dcopserver is already running. If you are sure\n  that it is not already running, remove /home/USERNAME/.DCOPserver_localhost.localdomain_:0\n  and start dcopserver again.\n  ---------------------------------\n\n  kdeinit: Communication error with launcher. Exiting!\n  KDE Daemon (kded) already running.\n  KLauncher: Exiting on signal 11\n  kdeinit: Communication error with launcher. Exiting!\n  connect() failed: : Connection refused\"\n\nNote that ~/.DCOPserver_* was removed after each crash before attempting to run KDE again.  Runlevels were switched from 5 to 3 to 5 and ps and kill were used to locate and kill any stray DCOP processes before running KDE from gdm again."
    author: "Andy Longton"
  - subject: "Re: Failed dependencies"
    date: 2001-08-17
    body: "Solved this; I didn't upgrade enough libraries.  All (?) of the libs from non-kde and kdoc from noarch are required or kdeinit will crash with the current RH KDE rawhide rpms."
    author: "Andy Longton"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "First, install everything from Redhat's update page to make sure that your system is current. Then install everything from the non-KDE directory except openssl* and rpm-*. After that openssl should install cleanly.\n\nThen go to rpmfind.net and get and install the latest versions (Rawhide) of the following:\nsh-utils\nglibc\nglibc-*\n\nAfter that installing the latest rpm-* works with --nodeps (it still breaks gnorpm and a few minor things, maybe updating them would solve the dependency problems).\n\nThen use --nodeps and --force to install all kde packages, as they still require efax, libsane and python2, which at least I don't need.\n\nIt works: I'm typing this in KDE2.2, which is wonderful by the way :) Thanks a lot, KDE folks!"
    author: "Janne"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "Oh only a few details - just change glibc ! Ha, ha ha !  Doing in such way I can install even Microsoft Office on my RedHat ( but MS doesn't call its distribution \"RH7.1 rpms\" ). Can't all this stuff be succesfully recompiled on a standard RedHat distribution ?"
    author: "Bee"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "> Then go to rpmfind.net and get and install the \n> latest versions (Rawhide)\n\nRawhide is the unstable version of RedHat. It is not 7.1. Why KDE packages have been compiled for Rawhide and not for RH 7.1?"
    author: "V\u00edctor R. Ruiz"
  - subject: "Re: Failed dependencies"
    date: 2001-08-16
    body: "Hmmm...this seems to be common problem for\nOSS sw developement. \n\nI can understand that sw is developed using the not-yet-released tools and libraries, but usually,\nwhen the product finally comes out - all required stuff has already been deliverd to endusers with\nOS upgrades.\n\nFor developers/packagers I would like to make a wish, that when something is released, it should \ninstall cleanly to latest *intact* OS (wether it is RH, Debian, Solaris, etc). \n\n_That_is_sharing_the_responsibility_areas_.\n\nIf this kind of situation continues, we will never gain mainstream users from the EvilEmpire!\n\nBR,\n\nJ\n\nPS: I at least leave sw uninstalled if it needs removing original libs from system."
    author: "NetPig"
  - subject: "Re: Failed dependencies"
    date: 2001-08-20
    body: "I would also suggest getting the Red Hat Raw Hide\nFAM package (version 10) rather than the one in\nnon-kde, as this seems to fix some problems."
    author: "Michael Wardle"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "hehe...\n\nnothing against KDE - but it sounds like you guys have a simple case of rpm hell...\n\nI've never had a problem installing from source on an 'rpm-free' system...\n\n(reminds of the little lady walking out of the house at the end of Poltergeist - 'this distribution is rpm-free' - *tears* *joy* *relief*\n\nsorry... couldn't resist ;-)"
    author: "jason byrne"
  - subject: "Re: Failed dependencies"
    date: 2001-08-15
    body: "I think it should be possible for the guys working on KDE (by the way, they are doing a great job!) to make rpms for standard installations (e.g. RH 7.1) and not for patched\nor beta versions. Otherwise people like me (i think i am not alone), who like linux for years,\nare getting tired about this mess. It would be sad if such a great project like KDE dies because\nit's not possible to provide packages for different distributions. And to have different\nones is one of the great things on linux !!\n\nI hope I'm sounding not too angry.\n\nBye"
    author: "Linux goes Win"
  - subject: "Re: Failed dependencies"
    date: 2001-08-17
    body: "Do you even pay attention to the KDE developer community? The KDE developers DO NOT MAKE ANY binary distributions, including RPMS. They just release source. The individual distributions make the packages. If you must blame someone, blame the distro package maintainers, not the KDE developers."
    author: "Aaron Traas"
  - subject: "I can't believe people who have...."
    date: 2001-08-17
    body: "Discovered the joy of Linux, and switched back to Windows.  Linux is a beautiful operating system.  You have the source code, you're in control.  There's something for everybody.  You don't have to pay extra for tools like Photoshop, it includes them.  No program takes over a part of your system for its own advertising.\n\nKDE is a great part of the OS, and it gets better with every upgrade.  One goal is in mind, and that is to improve the experience.  With Windows, it's to make $ by forcing people over to your technology.\n\nAfter a little while, it becomes part of the next version of the distribution, and can be simply installed.\n\nPlus, all the big distributions do have packages.  Just download and install.  If you read the instructions, installation goes smoothly.\n\nThere will always be those who want to be free.  That's why as long as the medium (computers) exists, KDE will never die."
    author: "Benjamin Atkin"
  - subject: "Re: Failed dependencies"
    date: 2001-08-16
    body: "Read the README - you need to update to the packages in the non-kde directory. It has all the updates and extra packages our KDE 2.2 build depends on.\nIt also states you can safely use --nodeps if you're using a stoneage version of KOffice or anything else that requires kdesupport."
    author: "bero"
  - subject: "Re: Failed dependencies"
    date: 2001-08-17
    body: "I tried, and go a dependency telling me my RH7.1 glibc is too old. If i upgrade that i guess the next dependency will be the kernel. Something is wrong here. The acclaimed RH7.1 packages are NOT for RH7.1 - it's as simple as that. It's a set of rpm's seemingly more tuned to the RH7.2 beta relase."
    author: "R.K."
  - subject: "Konqueror Issues"
    date: 2001-08-21
    body: "I tried to install KDE 2.2 on my system .After installing almost all dependencies (rpms), I have left with one rpm 4.0.3.rpm .When I tied to install it showed lot of other dependencies ,so I ommitted that and I installed KDE using --force option.Everything was okay till ,I tried to open kpackage which is dependant on latest rpm.After that Konqueror refuse to show up.Can anyone help me out\n\nKDE team something similar to RedCarpet to overcome these issues"
    author: "Rajeev Narayanan"
  - subject: "Re: Konqueror Issues"
    date: 2001-08-21
    body: "Not much you can do other than upgrade your RPM or downgrade your other packages.  There's a reason they make you add --force to the command-line to install if dependencies fail; it often causes problems exactly like this."
    author: "Ranger Rick"
  - subject: "Re: Konqueror Issues"
    date: 2001-08-22
    body: "I installed all latest rpms including glibc and rpm\nI guess it is due to some other issue.When I invoked Konqueror from Ximian Gnome ,it worked fine.But it fails to show up from KDE.Any help !."
    author: "Rajeev Narayanan"
  - subject: "Taskbar Buttons"
    date: 2001-08-15
    body: "I just installed KDE 2.2 and I am pretty impressed by the overall first feeling. I could not figure out however how to turn off the fading of button texts in the taskbar, if the text is to long to be fully displayed. Any ideas?"
    author: "anon"
  - subject: "Re: Taskbar Buttons"
    date: 2001-08-15
    body: "It's not configurable."
    author: "anon2"
  - subject: "Problems with audiocd io-slave"
    date: 2001-08-15
    body: "Trying to use the audiocd slave i always encounter the error message \"file or directory / does not exist\" with kde2.2. Following the faq I tried  \"cdparanoia -vsQ\" and I do see a track list for the audio cd.\n\nls -l /dev/sg0\n\"cr--r--r--   1 root     disk      21,   0 Aug  6  2000 /dev/sg0\"\n\nOh yes, I am using scsi-ide emulation for a plextor cdrw."
    author: "nobody"
  - subject: "Re: Problems with audiocd io-slave"
    date: 2001-08-15
    body: "try audiocd:/?device=/dev/sg0\n(and if it does not work, try giving yourself write permissions also, and more important, you also need read access to scd0 i think)"
    author: "ik"
  - subject: "Re: Problems with audiocd io-slave"
    date: 2001-08-15
    body: "audiocd:/?device=/dev/sg0 did not work (I had tried that before). I had to give write permissions to the normal user to make it work. Now I am trying to figure out how to convince the audiocd slave to use my http proxy for connecting to cddb...\n\nThanks for your help!"
    author: "anon"
  - subject: "Re: Problems with audiocd io-slave"
    date: 2001-08-16
    body: "I believe there is a KControl module to configure the default cdrom device, CDDB settings, etc.  It is Sound->AudioCD ?? (I'm trying to remember, I'm in windoze right now)."
    author: "uhmmmm"
  - subject: "Re: Problems with audiocd io-slave"
    date: 2001-10-01
    body: "I had the same problems (root only was able to access the cdrom via audiocd:/)\nUsers who were / should have been;) allowed to play with the cd-dev are in group cdrom;\nls -l /dev/scd* ==> root.cdrom brw-rw----\n \nSolution to this was:\nI had to set /dev/sg* to the same perms like scd*, even when specifying that /dev/scd1 should be used (in kcontrol)\n\nHope this will help ...\nAlex"
    author: "Alex Luschan"
  - subject: "Re: Problems with audiocd io-slave"
    date: 2001-12-30
    body: "That worked for me too....I'm using ide-scsi emulation, and changing the permissions for /dev/sg* works.... audiocd io slave is an awesome feature :-)"
    author: "torre"
  - subject: "KOffice 1.1 release date?"
    date: 2001-08-15
    body: "In the wonderful vein of users *always* clamoring for more, more, more: Does anyone know the new release date for KOffice 1.1 final?\n\n    Don't get me wrong - I am overjoyed at 2.2, I just really want to play with all the new printing in KWord, and shrug off the last bit of \"VNC into the windows machine\" that remains by converting all my files to KWord and KSpread.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KOffice 1.1 release date?"
    date: 2001-08-15
    body: "http://developer.kde.org/development-versions/koffice-1.1-release-plan.html"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: KOffice 1.1 release date?"
    date: 2001-08-15
    body: "Okay - color me dumb.  Not only did I search for that, I *was* there, and misread it (either that, or it changed just recently).  I read it as the final release occuring on the 12th.  Thanks for the (repeated) pointer.  ;)\n\n--\nEvan (Who is logged in as root under startx to look for Vorbis and docbook rpms... 2.2 soon!)."
    author: "Evan"
  - subject: "frustration"
    date: 2001-08-15
    body: "This is absolutely ridiculous! I am using Mandrake 8.0, and for what is supposedly a great integration, it is seemingly impossible to upgrade to 2.2! The never-ending heirarchy has mad me angry to no end! After an hour and a half of searching for this and that, and this and that, and then this and then that, I now have a headache and have given up! I don't plan on trying again either. This is exactly the type of thing that makes Gnome more attractive!"
    author: "Jason"
  - subject: "Re: frustration"
    date: 2001-08-15
    body: "I think it's wrong to blame such problems on KDE, as I just moved from Mandrake (having similar experiances) to SuSe 7.1. Even though it's an old copy of SuSe (they're up to 7.2), the website still provides RPM's for KDE 2.2, and a lot of applications for download.\n\nI download them, run:\n\trpm --nodeps --force -Uvh *.rpm\non the base files, and then the same later on the multimedia/koffice/apps stuff, restart the XServer, and I'm done. :-)\n\nMandrake has a great installer, but it drops you in a real mess when it's done."
    author: "David"
  - subject: "Re: frustration"
    date: 2001-08-15
    body: "If you have to use --nodeps and --force then your install is broken and you're not any better off running SuSE over Mandrake."
    author: "Jason Tackaberry"
  - subject: "Re: frustration"
    date: 2001-08-15
    body: "> and then the same later on the multimedia/koffice/apps stuff, restart the XServer, and I'm done. :-)\n\nYup... I downloaded the Vorbis RPMs from their site, and every dependancy except docbook_4 was satisfied.  After installing with nodeps and force, it looks to work great (except antialiasing is broken on G400/dual head in new and amusing ways).\n\nAnybody know what docbook_4 is (and/or wanna fix the G400 driver's antialiasing issues?).\n\nOh, and if it's not too much trouble, can the developers provide world peace, promote literacy and give us all a slice of cake with the next release?  I think that's all that's left.  ;)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: frustration"
    date: 2001-08-15
    body: "I installed LM8.0 as it was so easy to get everything up and running. The ease of installation is however deceptive, as trying to do anything slightly out of the ordinary afterwards caused me endless headaches.\n\nI have subsequently moved to Debian Linux and I am a happy camper.\n\nEverybody .. move to Debian already :)\n\nP.S. I have not used SUSE at all, so I have no idea how friendly SUSE is to upgrade"
    author: "kde-user"
  - subject: "Re: frustration"
    date: 2001-08-15
    body: "That's funny, 'cause I'm using must-pull-our-teeth-to-include-kde Debian and have been tracking the updates every couple days or so. And all it takes is a simple apt-get dist-upgrade.\n\nOk, so I'm a distro trolling bitch. Sue me. Moral of the story, blame the guys who *distribute* the binaries, not the guys who write the source.\n\nErik"
    author: "Erik Severinghaus"
  - subject: "Re: frustration"
    date: 2001-08-15
    body: "Installing the Mandrake8 packages were in fact extremely easy. Just do\n\nurpmi.addmedia kde22 file://directory-where-you-put-the-rpm-files\n\nThen, for instance do 'urpmi kdebase' which will take care of any dependencies.\n\nThis was all in the README file accompanying the packages."
    author: "Haakon Nilsen"
  - subject: "Re: frustration"
    date: 2001-08-15
    body: "KDE 2.2 installation on my Mandrake 8 box couldn't have been easier. All I have to do was a \"rpm -Uvh * --force --nodeps\". Everything is working fine now and KDE 2.2 rocks."
    author: "Anonymous"
  - subject: "Re: frustration"
    date: 2001-08-15
    body: "Running MandrakeFreq 2 here and had no real problems.\n\nI had a few dependancies crop up with Kdevelop and the dependancies had dependancies.  So in the end i left Kdevelop out.\n\nThe only other dependancy problem i had was for the nsplugins; it required netscape-common and netscape isn't included with MandrakeFreq2.  I installed it with nodeps since i have Mozilla 0.9.3 installed and it worked fine.\n\nOther than that things are going great.  There's been one crash of X caused by Konqi when trying to access hotmail (not sure what the exact problem was, i wasn't the one using it at the time).\n\nI'm very impressed with 2.2 and my thanks go out to the KDE team for creating such a wonderful desktop environment."
    author: "R."
  - subject: "CD audio grabbing"
    date: 2001-08-15
    body: "Audio grabbing in Konqueror is a nice feature, but the binarey packages only come with ogg support and not with mp3 support too.\n\nO.K. I understand, that for patent reasons the library for mp3 can't be included in the binary packages. But isn't it possible, that the grabbing IO-Slave autodetect at runtime if a mp3 library is installed? If it detect the library it can also show the mp3 directory. At the moment the library is only detected at compile time. I think a detection at runtime is much better because the grabbing IO-Slave needn't compiled and only the mp3 library must be compiled and installed in the system.\n\nIf autodetection at runtime is not possible, at least a small source package with only the source needed to compile the grabbing slave should be on the KDE download page."
    author: "Bernd"
  - subject: "Re: CD audio grabbing"
    date: 2001-08-15
    body: "Actually... it is.\nYou see, I haven't any mp3 encoding library, and no mp3 was shown in audiocd, but when I installed mp3encode, voil\u00e1! Audiocd detected it and show the mp3 option."
    author: "Iuri Fiedoruk"
  - subject: "Re: CD audio grabbing"
    date: 2001-08-15
    body: "I think libmp3 from the lame project is used by the grabbing IO Slave. Hmm. What Is needed for mp3 grabbing in Konqueror?"
    author: "Bernd"
  - subject: "Re: CD audio grabbing"
    date: 2001-08-15
    body: "In theory just a mp3 encoder installed on your system.\nI'm lazy so I don't compiled lame, but found a encoder named mp3encode (hehehe what a name) and installed the rpm here, just it, nothing more, and then mp3 option shows up on kcontrol and konqueror audiocd kio."
    author: "Iuri Fiedoruk"
  - subject: "Re: CD audio grabbing"
    date: 2001-08-16
    body: "I have lame compiled and installed but no mp3 option is shown by KDE. My opinion is that KDE needs libmp3 from the new lame versions. At least the KDE 2.2 beta version works after recompiling the grabbing slave with installed libmp3.\n\nMayby I should try with mp3encode. But first I must find mp3encode."
    author: "Bernd"
  - subject: "Any packages for RedHat 6.2"
    date: 2001-08-15
    body: "Does anyone know where I could find KDE 2.2 packets from RH 6.2 ?"
    author: "Robert"
  - subject: "Re: Any packages for RedHat 6.2"
    date: 2001-08-15
    body: "As the person who made the KDE 2.1.1 packages for RedHat 6.x, I can say the job  wasn't too hard: I just rebuilt the RedHat 7.x SRPMS (with a few minor modifications).  I would venture that porting KDE 2.2 might be a little more difficult because it looks like that it requires more prerequisite packages not included in rh6.x.  Also consider that koffice 1.1 currently won't build with rh6.x's egcs compiler.\n\nThat being said, I probably won't be doing the same for KDE 2.2 as I've almost completely upgraded all my machines to rh7.1 (and you should too (-; ).  Anyway, most likely, in order for rh6.x RPMS to appear, someone will have to volunteer to do it.  Any takers?\n\nFor more background and info on how to become a \"packager\", see \"KDE Package Policy Explaned\" at:\nhttp://dot.kde.org/986933826/"
    author: "Rex Dieter"
  - subject: "Re: Any packages for RedHat 6.2"
    date: 2001-08-15
    body: "If someone does do it I'll be very happy. I tried compiling the SRPMS and got nowhere, tho my system is a bit patchy."
    author: "Gabriel Golden"
  - subject: "Re: Any packages for RedHat 6.2"
    date: 2001-08-16
    body: "> koffice 1.1 currently won't build with rh6.x's egcs compiler...\n\nAlmost... IIRC, it's KChart and the Quattro import filter for KSpread that won't compile through gcc 2.91 (which RH-6.2 provides).  The rest of KOffice compiles fine."
    author: "Paul Ahlquist"
  - subject: "thanks on all developer"
    date: 2001-08-15
    body: "i try to thank in this way to all developer of kde for this great work\n\ngreetings \nharald"
    author: "harald"
  - subject: "Re: thanks on all developer"
    date: 2001-08-15
    body: "Dank an alle KDE-Entwickler f\u00fcr die Arbeit.\nEine feine Benutzerumgebung :-))\n --\nother languages? ;-)"
    author: "Thorsten Schnebeck"
  - subject: "Re: thanks on all developer"
    date: 2001-08-15
    body: "un grand bravo \u00e0 toute l'\u00e9quipe de d\u00e9veloppement de KDE. C'est un produit exceptionnel et je trouve dommage qu'il ne soit pas plus soutenu par l'industrie ( je pense tout particuli\u00e8rement \u00e0 Sun qui \u00e0 choisi Gnome comme future desktop... )\n\nMerci \u00e0 tous."
    author: "Renault JOHN"
  - subject: "Re: thanks on all developer"
    date: 2001-08-24
    body: "GNOME is not that bad!"
    author: "Rajan Rishyakaran"
  - subject: "Re: thanks on all developer"
    date: 2001-08-15
    body: "Obrigado a todos os programadores e contribuidores do KDE pelo seu trabalho excepcional :-)"
    author: "Ricardo Correia"
  - subject: "Re: thanks on all developer"
    date: 2001-08-16
    body: "Muchisimas gracias a todos los programadores de KDE por el trabajo desarrollado. Habeis creado un entorno de trabajo para el usuario de la mas alta escuela!!"
    author: "Luis Miguel Diego Alvarez"
  - subject: "Re: thanks on all developer"
    date: 2001-08-15
    body: "Bedankt KDE ontwikkelaars voor dit prachtig programma. Ik ben nu volledig M$ vrij :-)"
    author: "weda"
  - subject: "Re: thanks on all developer"
    date: 2001-08-15
    body: "Mi agradeciemiento a todos los desarrolladores y aquellos que hayan contribuido en KDE. Un excelente trabajo."
    author: "ViRgiLiO"
  - subject: "Slackware pkgs"
    date: 2001-08-15
    body: "Does anyone know if there are any slackware packages available?  I would much rather do a simple upgradepkg than compile the whole thing from the source."
    author: "Elliott"
  - subject: "Re: Slackware pkgs"
    date: 2001-08-15
    body: "yeah, i'd like them too :)\ni'd compile it my-self but it doesn't compile on slack8.0 just tried to do this in 7.1 and it worked well, but not on the 8. compiling kde was one of the things i liked most :) but now it is not :("
    author: "skinncode"
  - subject: "Re: Slackware pkgs"
    date: 2001-08-15
    body: "What was your problem compiling with slack 8? I did a fresh install of slack 8 and compiled kde 2.2 without problems\n\n// Tony"
    author: "Tony"
  - subject: "Re: Slackware pkgs"
    date: 2001-08-16
    body: "if you had trouble compiling it it's probably due to the fact that slack 8 doesn't have /opt/kde/lib in the /etc/ld.so.conf, so try adding that line and running ldconfig and then compiling it again."
    author: "Elliott"
  - subject: "Re: Slackware pkgs"
    date: 2001-08-15
    body: "k, seems i found them :)\ncheck this:\nhttp://hal.astr.lu.lv/pub/linux/Slackware/kde-2.2/\n\nit's strange that the creation date of these packages is 08-Aug-2001 but i hope that it'll be ok :)"
    author: "skinncode"
  - subject: "Re: Slackware pkgs"
    date: 2001-08-27
    body: "Hi...\n\n Yeah KDE 2.2 for Slackware 8.0 at http://hal.astr.lu.lv/pub/linux/Slackware/kde-2.2/\n\nWell I found this site accidentally...but I 've downloaded KDE packages from the site and everything is fine...but before that I've had removed (uninstall) the KDE 2.1.x packages first.\n\nNow I think I'm one of the slacker with KDE 2.2 without compiling it by myself.\n\nBest regards.."
    author: "Linuz"
  - subject: "Caldera RPM's"
    date: 2001-08-15
    body: "The caldera RPMs are broken.\n\nDon't try it!\n\nMissing kdeconfig, and several libraries.\n\nI have to reinstall because of.\n\nThat's sad.  Usually Caldera beats the socks off RedHat when it comes to stability."
    author: "Caldera User"
  - subject: "Re: Caldera RPM's"
    date: 2001-08-15
    body: "Did you follow the README? We tested the RPMs with several install, and we didn't see problems. What is exactly going wrong?"
    author: "Matthias Hoelzer-Kluepfel"
  - subject: "Re: Caldera RPM's"
    date: 2001-08-15
    body: "Just installed it.  Everything seems OK so far.\n\nQuestion:  Is Caldera rpm compiled with the optimal linker (objprelink)?\n\nIt seems a little faster than the 2.1.1 but not as significated as expected from objprelink hack.\n\nOverall, it runs and looks great.  You guys and gals did a great job."
    author: "anh"
  - subject: "Re: Caldera RPM's"
    date: 2001-08-15
    body: "Read at http://www.research.att.com/~leonb/objprelink/ the step-by-step instructions and compare the amount of different relocations given there with your installation using the provided script."
    author: "someone"
  - subject: "Re: Caldera RPM's"
    date: 2001-08-15
    body: "For one thing, can't find \"kdeconfig\" when trying to install kdebase.  It wasn't even on the ftp.kde.org site as of last night.  This is OpenLinux 3.1."
    author: "Caldera User"
  - subject: "Re: Caldera RPM's"
    date: 2001-08-15
    body: "I am not the previous poster, but I can confirm that there are problems with the KDE 2.2 rpms for Caldera Workstation 3.1.\n\nI installed them, made sure that anti-aliased fonts were turned on, and restarted.  When anti-aliased fonts are turned on, it messes up the KDE environment... like it can't handle the new fonts.  In many dialogs the lines of text overwrite each other and cause the controls to shrink to the point that everything is unusable.  When I turn the anti-aliased fonts off, everything goes back to normal.\n\nPlease help?  I can tell that the anti-aliased fonts will look BEAUTIFUL when they work right, and I'm anxious to get it there.  I've been waiting for this to happen to Linux for a long time..."
    author: "Bill"
  - subject: "Re: Caldera RPM's"
    date: 2001-08-16
    body: "Caldera RPMs worked OK for me. I rebuilt the\nRPMS anyway. However, I noticed that smb ioslave\nis missing. In fact, I checked Suse kdelibs RPM,\nand it does not have smb ioslave either.\nAny ideas?"
    author: "Murat Eskiyerli"
  - subject: "Re: Caldera RPM's"
    date: 2001-08-16
    body: "Then you don't have vector fonts installed to show up for the normal stuff. \nAnyway not the arial/helvetica/courier it searches for.\n\nYou can try to get something nice with editing the fonts from kcontrol, but you really need vector fonts first!"
    author: "Thomas Zander"
  - subject: "How about Anti-Aliasing?"
    date: 2001-08-15
    body: "Will Anti-Aliasing be supported in this version? Also, will it work equally well for all Qt/KDE apps?\nFinally, many of the international fonts did not look well on my machine. Of course, this is mostly xfs's responsibility, but I still wonder whether this can be helped."
    author: "Uri"
  - subject: "Re: How about Anti-Aliasing?"
    date: 2001-08-15
    body: "I don't see any reason why they would remove AA support from KDE 2.2.  AA has been supported since KDE 2.1 (provided you use XFree4 and a video card the supports the Render extension)."
    author: "Rex Dieter"
  - subject: "Re: How about Anti-Aliasing?"
    date: 2001-08-16
    body: "What about anti-aliasing of fonts in KDM? In 2.1 I used to 'export QT_XFT=true' in runlevel 5, and then KDM would give anti-aliased fonts. Now that doesn't appear to work. any ideas?"
    author: "Francis Sedgemore"
  - subject: "Re: How about Anti-Aliasing?"
    date: 2001-08-16
    body: "Quick idea;\nwhat about selecting a vector font in the kcontrol module: system->login manager ?\n\nDon't know though, I still run X3 ;)"
    author: "Thomas Zander"
  - subject: "Any screenshots?"
    date: 2001-08-15
    body: "I won't d/l it until i see it in screenshots. (I like to save time and expensive bandwidth)"
    author: "Rajan Rishyakaran"
  - subject: "Re: Any screenshots?"
    date: 2001-08-15
    body: "A funny thing is usally are the screenshots bigger than the app itself. Well not in this case but..."
    author: "AC"
  - subject: "screenies here!"
    date: 2001-08-15
    body: "Plenty of screenshots are coming, but q threw some together a few weeks ago to show some of the newer features, and I quickly threw them up on <a href=\"http://static.kdenews.org/content/kde2.2/\">this temporary site</a> for convenience -- the text needs fixing and all that, but it's still good to get a quick idea of some things that have changed.\n<br><br>\nBe patient though, I hear a lot more cool stuff is coming!"
    author: "Navindra Umanee"
  - subject: "Re: screenies here!"
    date: 2001-08-15
    body: "Two questions: What is this in Sidebar TNG with the icon of Kdiskfree displaying an URL (shot 2)? And how do I set this funny \"lined paper\" background for Konsole's terminal emulator (shot 3)?"
    author: "someone"
  - subject: "Re: screenies here!"
    date: 2001-08-15
    body: "Sidebar TNG button:\nYou can add your own buttons\n\nKonsole:\nIts a  part of the Style\n\nQwertz"
    author: "Anon"
  - subject: "Re: screenies here!"
    date: 2001-08-15
    body: "Question:\nwhere can I get this window decoration similar do quartz? (quartz use squares instead of gears on right side of windows);"
    author: "Iuri Fiedoruk"
  - subject: "Re: screenies here!"
    date: 2001-08-15
    body: "It's an IceWM-Theme called \"MenschMaschine\" and is contained in the kdeartwork module."
    author: "someone"
  - subject: "Re: screenies here!"
    date: 2001-08-15
    body: "Thanks, I found it.\nThose themes are so beautifull than I tought they where hardcoded.\nCongrats to galliun, qwertz and all KDE team."
    author: "Iuri Fiedoruk"
  - subject: "Re: screenies here!"
    date: 2001-08-16
    body: "Many thanks.\n\nIt was my pleasure making the IceWM theme importer. Enjoy!"
    author: "gallium"
  - subject: "Re: screenies here!"
    date: 2001-08-16
    body: "We sure are enjoying it! It was an important step for KDE."
    author: "Matt"
  - subject: "turn of underlining"
    date: 2001-08-15
    body: "whoa - beautiful! \n\nJust one tip though - turn off the underlining of desktop icons and files. No other OS uses is - and for a good reason - it clutters and disturbs the image... (less is more)."
    author: "will"
  - subject: "Re: turn of underlining"
    date: 2001-08-15
    body: "I agree, I turn off underlining *everywhere* even in my browser, it really seems to clutter up the interface.  Unfortunately underlining seems to have crept back into Konqueror by default.  It used to be hover AFAIK."
    author: "Navindra Umanee"
  - subject: "Re: turn of underlining"
    date: 2001-08-17
    body: "You can set this with Settings/Configure Konqueror then choose File Maganer/Apperance and deselect Underline Filenames .\n\nand for the desktop rightclick it and choose Configure DEsktop and then Appearance.\n\n...or did you mean that they should change the default settings? i'm all for that"
    author: "sabbo"
  - subject: "Re: turn of underlining"
    date: 2001-08-17
    body: "It would be nice if icon text could be rendered shadowed, like Windows XP can. That looks very nice imo."
    author: "Jos Backus"
  - subject: "Re: turn of underlining"
    date: 2001-08-18
    body: "Thanks for the tip!!\n\nWhile I like underline for web browing, I *hate* it for the desktop.\n\nI'm all for changing the default setting for the desktop, for the browser I'm neutral..\n\nMaybe some poll would be usefull."
    author: "renoX"
  - subject: "theme?"
    date: 2001-08-15
    body: "where can i find that theme (the one in most of the screenshots with the cool blue wallpaper) ?"
    author: "3max"
  - subject: "Re: theme?"
    date: 2001-08-15
    body: "All the stuff comes with KDE 2.2\n+ mosfets suff.\n\nq"
    author: "Anon"
  - subject: "Re: theme?"
    date: 2001-08-15
    body: "That's strange.... seems like dosen't come to me :(\nWhat is the decoration name? I have here:\n\nKDE2 Default\nB II\nIceWM\nKDE 1\nKStep\nLaptop\nLiquid\nModSystem\nMwm\nQuartz\nRedmond\nRISC OS\nSystem ++\nWeb\n\nMaybe I missed some package on redhat 7.x rpms....(?)"
    author: "Iuri Fiedoruk"
  - subject: "Re: theme?"
    date: 2001-08-15
    body: "Its in Kdeartwork\nIceWM Themes\n\nq"
    author: "Anon"
  - subject: "kdeartwork?"
    date: 2001-08-15
    body: "ok, stupid newbie q. Is 'kdeartwork' an obvious feature in the look and feel section of the control panel? I think a problem I, and others, are having is that kdeartwork was not initially installed."
    author: "kdeuser"
  - subject: "Re: kdeartwork?"
    date: 2001-08-15
    body: "Its a KDE module like kdebase...etc\nJust grab the rpm.\n\n\nq"
    author: "Anon"
  - subject: "Re: screenies here!"
    date: 2001-08-15
    body: "Those screenshots sure are beautiful. It seems the graphics artists have been working every bit as hard, and producing every bit as good a result as the coders, and that certainly proves a lot about the graphics artists!\n\nEveryone involved in making KDE 2.2 deserves every positive comment they receive for their incredible work, and surely a lot more than that too! Too bad there aren't enough people in the world to think of enough superlatives. :)\n\nIn summary: I wish I never have to see that horrible redmondian user interface (what's it called by the way?) again. This release surely makes me confident that this wish might actually come true quite soon. You guys rule my computing world."
    author: "Mattias"
  - subject: "Re: screenies here!"
    date: 2001-08-16
    body: "thanks for the screenies.... i do have a reason to d/l and show off to my XP-is-better-than-your-linux-crap friends..."
    author: "Rajan Rishyakaran"
  - subject: "KDevelop jaggies!"
    date: 2001-08-16
    body: "Cool screenies... just a quick question:\n\nIt's obvious AA is enabled, so why is the 'Welcome to KDevelop' a jagged? Is that the way for all KDE help/intro screens?\n\n--BN!"
    author: "Billy Nopants"
  - subject: "Re: KDevelop jaggies!"
    date: 2001-08-17
    body: "No, something must be wrong with Type1 fonts on that system. Usually that title is smooth."
    author: "Matt"
  - subject: "SuSE / RPM Install Order"
    date: 2001-08-15
    body: "Ok, when I tried 2.2beta alot of stuff failed and alot of stuff was missing. Can someone tell me the correct order / instructions on installing the RPM packages? And do I still need the kdesupport rpm? Where is it? Oh, and one more question: in the SuSE ftp directory they have a subdir called experimental which contains kdelibs, kdebase and something else (i forget) I'm assuming these rpms in this 'experimental' directory are the ones that feature the prelinking fast startup capability. Am I right? Thanks to the developers, you rock!"
    author: "zaq rizer"
  - subject: "Re: SuSE / RPM Install Order"
    date: 2001-08-15
    body: "On SuSE 7.1 I had the same problems as mentioned above, installation failed because of missing libs etc.\nThat was when using the \"rpm -i\" or \"rpm -U\" command. So I tried using Yast, which has in the Software-Installation Part a possibility to Install Packages. There it works just fine."
    author: "Thomas Witzenrath"
  - subject: "Re: SuSE / RPM Install Order"
    date: 2001-08-15
    body: "Thanks for the tip; did you use YaST1 or YaST2?"
    author: "zaq rizer"
  - subject: "Re: SuSE / RPM Install Order"
    date: 2001-08-15
    body: "There are perhaps cyclic dependencies so there is no \"correct order\", simply pass all rpms at once as argument to 'rpm' when installing. kdesupport doesn't exist anymore, just install the missing dependencies as indiviual rpms like that for libxml and libaudiofile."
    author: "someone"
  - subject: "Re: SuSE / RPM Install Order"
    date: 2001-08-16
    body: "Ok, typing:\n\trpm -Uvh *.rpm\nfrom a directory with all of the rpm's did not report any errors, and seemed to work ok. After a while I noticed some things were not working, and checked:\n\trpm -qi kdeaddons\nwhich reported \"...not installed.\"\n\nSo I had to go through and manually install the remaining packages individually.\n\nShouldn't the first command have installed everything?!\n\nThanks,\nMatt Fago"
    author: "Nobody's Home"
  - subject: "Re: SuSE / RPM Install Order"
    date: 2001-08-16
    body: "yes, maybe your rpm database is corrupt.\ntry a rpm --rebuildb"
    author: "Adrian"
  - subject: "Re: SuSE / RPM Install Order"
    date: 2001-08-16
    body: "Yea, that's the first thing I tried. No effect. Odd."
    author: "Nobody's Home"
  - subject: "Just switched over to KDE."
    date: 2001-08-15
    body: "Well, it took ages to compile KDE 2.1.1 on my FreeBSD box. Now KDE 2.2 has just come out :-)\nI wonder if there are any FreeBSD native packages...I used to use Gnome 1.2, but they've been on Gnome 1.2 for ages, and I couldn't upgrade to Ximian Gnome (compile errors). Noone else seems to be interested in getting the latest Gnome up and running for FreeBSD !! Well, that's fine. They've definitely lost my market share. KDE 2.1.1 compiled fine. Has anyone had any luck compiling 2.2 for FreeBSD, and/or are there any packages for it ? I'm running 4.3-RELEASE"
    author: "Warrick Grahamson"
  - subject: "Yup"
    date: 2001-08-15
    body: "It's linked to from the announcement.\n\nhttp://master.kde.org/pub/kde/stable/2.2/FreeBSD/"
    author: "KDE User"
  - subject: "Icon Themes howto ?"
    date: 2001-08-15
    body: "Hello, I posted this on slashdot too but i think it's better to write it here.\n\nThere used to be a Icon theme online manual on\n\n http://www.mosfet.org/themeapi \n\nbut mosfet removed that one :-( I want to make such a theme for kde 2.x but I could nowhere find a manual and themes.org manual seemed outdated."
    author: "kaltan"
  - subject: "Re: Icon Themes howto ?"
    date: 2001-08-16
    body: "Hi,\n<br><br>\nIf you are interested in creating a new style (not pixmapped, but coded), you should look at the tutorial at this URL :\n<br><br>\n<a href=\"http://www.geoid.clara.net/rik/knowledge.html\">http://www.geoid.clara.net/rik/knowledge.html</a>\n<br><br>\nIt's written by Rik Hemsley btw.\n<br><br>\nHave fun."
    author: "Jelmer Feenstra"
  - subject: "Why am I not surprised..."
    date: 2001-08-15
    body: "...to see that the most of the posts are dealing with problems with RPMs. Why don't install from source or use .debs instead? :)\n\n\nLoranga, a.k.a Mats"
    author: "Loranga"
  - subject: "Re: Why am I not surprised..."
    date: 2001-08-17
    body: "well,\n1) most don't have a debian or a fork of debian\n2) compiling from source is hard and slow for some.\n3) i compiled mine!"
    author: "Rajan Rishyakaran"
  - subject: "screenshots (repost)"
    date: 2001-08-15
    body: "For those who asked for them, plenty of screenshots are coming, but q threw some together a few weeks ago to show some of the newer features, and I quickly threw them up on <a href=\"http://static.kdenews.org/content/kde2.2/\">this temporary site</a> for convenience -- the text needs fixing and all that, but it's still good to get a quick idea of some things that have changed.\n<br><br>\nBe patient though, I hear a lot more cool stuff is coming!"
    author: "Navindra Umanee"
  - subject: "wallpaper?"
    date: 2001-08-15
    body: "I like your wallpaper in the screenshots. Where did you get it?"
    author: "ide"
  - subject: "Re: wallpaper?"
    date: 2001-08-15
    body: "Note that these are not my screenshots, they're q's. :-)\n\nI believe this screenshot is distributed with KDE 2.2.  Probably in kdeartwork if it's not in kdebase.\n\nLater,\n-N."
    author: "Navindra Umanee"
  - subject: "wallpaper name?"
    date: 2001-08-15
    body: "Hmmm... it seems I don't have it (?). By chance, does someone have the filename of that wallpaper available? I wonder if I am missing a kdeartwork package..."
    author: "rower"
  - subject: "Re: wallpaper name?"
    date: 2001-08-15
    body: "default_blue.jpg"
    author: "someone"
  - subject: "Re: screenshots (repost)"
    date: 2001-08-16
    body: "Which of these *nice* window border decorations are shipped with KDE 2.2? Everyone except Liquid?"
    author: "Matt"
  - subject: "Re: screenshots (repost)"
    date: 2001-08-16
    body: "The decoration of shot 1 is not the Liquid one but an IceWM-Theme. All others are shipped."
    author: "someone"
  - subject: "Re: screenshots (repost)"
    date: 2001-08-16
    body: "Well that's just great! Now I hope that all major distributions will include the 'kdeartwork' module. Because it makes Linux attractive, in the true sense of the word."
    author: "Matt"
  - subject: "Re: screenshots (repost)"
    date: 2001-08-16
    body: "I liked snapshot9 the most because it shows that you can have another type of digits in the digital clock than those usual ugly ones. :)"
    author: "Joe"
  - subject: "Konqueror"
    date: 2001-08-15
    body: "Finally Konqueror performs nicely on my slow computer (a measly P60), the GUI no longer becomes completely unresponsive when opening a website and images are displayed faster.\n\nIn the past using Mozilla on a Pentium 60 with 98 RAM was a more pleasurable browsing experience than with Konqueror. I'm glad to find that has changed since KDE2.2.\n\nThank you :)\n\nOh, by the way.. there was some mention about http pipelining. How would this affect performance and when will this be implemented in Konqueror (or is it already)?"
    author: "Yatsu"
  - subject: "CUPS support in Redhat"
    date: 2001-08-15
    body: "I was pretty excited about the whole new printing thing, but now it seems that the Redhat RPMS for KDE2 don't recognise the fact that CUPS is installed. I know this isn't the Redhat standard print daemon (it should be IMHO), but wouldn't it be better to cover all bases.\n\nPlus I have say that KDE 2.2 on RH7.1 seems unstable compared to 2.1.1/2. I've had a few total resets back to KDM since installing it yesterday.\n\nI can finally pay my bills in Konquerer though ;-)"
    author: "Dr_LHA"
  - subject: "Re: CUPS support in Redhat -- Debian, too"
    date: 2001-08-15
    body: "Well, I've got the same problem under Debian Woody; I installed KDE 2.2 form the Sid tree, then decided to upgrade to XF86 & Cups from Sid, while I was at it.\nWhen I tried to test the printing, 2.2 could not see  CUPS as one of the underlying printing engines (? hmmm, can I say that?).\n\nNo stability problems with KDE 2.2, though. It's just the printing that's not working."
    author: "bruno majewski"
  - subject: "Re: CUPS support in Redhat -- Debian, too"
    date: 2001-08-16
    body: "I rebuilt the kdelibs RPMs for Redhat from the src.rpm file, and the configure script detected the presence of CUPS on my system and it now supports it. The missing file was called:\n\n/usr/lib/kde2/libkdeprint_cups.so \n\nI'm still having problems printing to one of our printers - it complains about \"imagetops\", I think that might be a local configuration problem however (although it works every other way I use it)."
    author: "Dr_LHA"
  - subject: "Re: CUPS support in Redhat -- Debian, too"
    date: 2001-08-16
    body: "Imagetops is a small utility included in KDE-2.2. So again it seems to be a package problem.\n\nMichael."
    author: "Michael Goffioul"
  - subject: "Re: CUPS support in Redhat -- Debian, too"
    date: 2001-08-16
    body: "Did you install the kdelibs3-cups package. It contains the CUPS plugins for kde.\n\ntix.64"
    author: "tix.64"
  - subject: "Re: CUPS support in Redhat -- Debian, too"
    date: 2001-08-16
    body: "No, I did not. Missed that one when I was installing the myriad of debs necessary for KDE 2.2 (man, if there was a good example for the need for KDE installer, that is it).\n\nI did that and now KDE 2.2 can now see CUPS as one of the underlying printing engines. The only problem is that when I try to define a remote LPD printer (my printer is attached to my DSL gateway/firewall box that serves also print-server duties. It's running a two-floppy hybrid of Coyote Linux & Charles S.'s EigerStein LRP Linux that I haphazardly (sp?) cobbled up), the control centre segfaults. Every time. And only for remote LPD queues.\n\nDid anyone else encounter that? I tried upgrading libc6 et al from Sid, to no avail: I still get segfaults on that."
    author: "bruno majewski"
  - subject: "Re: CUPS support in Redhat -- Debian, too"
    date: 2002-08-23
    body: "Thanks, adding that kdelib3-cups file worked.\nThough I wonder why Debians dselect did not pick that up?\nAnyways thanks much,\nAl"
    author: "Alfred"
  - subject: "Re: CUPS support in Redhat"
    date: 2001-08-16
    body: "To have CUPS support in KDE, CUPS must be present at compile time. So it's more a packager problem. As CUPS is not the standard print spooler under RedHat, I'm not really surprised by this problem. The solution would be to have a separate kdelibs-cups package to include all CUPS-related stuffs. But this is a RH-packager issue.\n\nMichael."
    author: "Michael Goffioul"
  - subject: "Same Trouble to KDE users :( by KDE People ;)"
    date: 2001-08-15
    body: "Look O' KDE developers, maintainers and project Leaders!\n\nAgain its rpm vs source headache for your KDE users. Missing dependencies, and many such troubles just to get our favourite Desktop Upgrade. \n\nRemember, You people Promised KDE-Installer right in the HELP of KDE 1.1.2? Even After 1-2 years where is it??? No KDE installer yet and no relief to your users from upgradation troubles.\n\nJust kick-off RPMs, DEBs, etc., and use generic + distribution dependent binaries (excuse the sources for now) like Netscape, Adobe Acrobat, Corel Wordperfect, StarOffice, and ... to Install and Upgrade KDE (making it more user friendly).\n\nWhen these Non KDE people can develop packages generic and functional on any Linux distribution, why can't KDE yet have this capability or technology??? I can use adobe acrobat painlessly in SuSE, RedHat, Mandrake, etc., installed from one acrobat-4.0-tar.gz (binary) with an installation script. \n\nI am not talking Unix specific installer, but KDE is by Linux, for Unix and of Linux/Unix.\n\nI have to allege that you are denying our basic right of getting upgrades quicker and you are giving us trouble by only providing sources.\n\nPlease for goodness sake and for the unity of Linux distribution, create KDE installer and don't force KDE lovers to use Mandrake/SuSE just because they like different distros like Caldera, RedHat, Turbo Linux, etc.\n\nI have to complain because it gives me lot of pain and anguish just to upgrade the next version of my lovely KDE on my RedHat (latest). Don't you feel the trouble we are going through all the time (well every six month ;)."
    author: "Asif Ali Rizwaan"
  - subject: "A note for all rpm users"
    date: 2001-08-15
    body: "This complaining about RPMs not working is pointless when you direct them at the developers.  The don't package them, the write the code and put it in nifty little things of only source.  It is up to a maintainer to create the packages for specific distributions (ie RedHat, SuSE... ). Please don't complain to the KDE developers for poor packaging.  It isn't them.\n\nAnd as for the binary install from a tarball, it can be easier said then done.  Several distributions don't place the files in the proper places, probably the biggest violator being redhat.  You can get rid of the dependencies and all with tarballs, but each system expects KDE in a different place.  Scripts help, but then there is no promise all dependencies are satisfied.  There is a solution.  First get the source and spend the time to make it yourself.  Second try other distributions.  I have used many and SuSE always seems to work with the RPMs.  They also place them in the correct locations on the disk.  Ask for help on how to get RPMs to work is fine.  Bashing the KDE team is not right when it comes to the RPMs.\n\nAside from that, thanks to the KDE team.\n\nEntigo"
    author: "Entigo"
  - subject: "Not Bashing KDE team"
    date: 2001-08-15
    body: "We have KDE-Installer but in its ALPHA stage, which works (I guess) on Debian, RedHat, SuSE, Mandrake, and even on FreeBSD.\n\nPlease visit:\n\nhttp://kdeinstaller.rox0rs.net/news/feb01/distroinfo.html\n\nBut if few or just few KDE developers give a very little time of their coding to improve this *Essential Application*, then we won't have any problem for upgrading and installing KDE on any flavor of Linux!\n\nI love KDE team and developers, and I don't dare say anything bad to them! :)\n\nHope you understand that it's not about RPMS or DEBS or tar.gz's or RedHat or SuSE or Mandrake but it (topic) is basically about KDE upgrade trouble caused on every new release of KDE and the users (many, excluding me) are bothered by this trouble. And the will of KDE team before KDE 2.x to ease the upgrade process."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Not Bashing KDE team"
    date: 2001-08-15
    body: "If KDE has an installer, I hope it\u00b4s better than ximian. I tried the Gnome1.4 for SuSE7.0\nDownloaded 70mb in 3 hours and nothing worked.\nEven all gtk-apps didn\u00b4t work in KDE.\n\nSo if a KDE-installer comes, then please provide an installer that knows all the distributions, knows the shipped compiler, & compiles all packages automatically, after resolving dependencies.\n\nGo on with your great work.\n\nI\u00b4m still downloading the 80megs with my 64k-isdn with Webdownloader 4 X 1.27 :-("
    author: "Christian Schuglitsch"
  - subject: "Re: Same Trouble to KDE users :( by KDE People ;)"
    date: 2001-08-15
    body: "\"I have to complain because it gives me lot of pain and anguish just to upgrade the next version of my lovely KDE on my RedHat (latest). Don't you feel the trouble we are going through all the time (well every six month ;).\"\n\npain and anguish, come on people, the KDE developers are working their collective tails off for you for free and you have the nerver to complaing that its too hard to type 3 lines:\n\n./configure\nmake \nmake install\n\nit isn't rocket science.\n\nin fact it can be cut down to 2 lines for those that have a little knowledge:\n\n./configure\nmake all install\n\nvoila, do that in each directory of the source and you are done.\n\nI have always used source to upgrade, in fact I upgrade all the time from the CVS tree. Never had a problem, and all my libs are enabled, including that pesky copyrighted mp3 lib.\n\nSo give the developers a break, this arguement is old, and most of the true KDE lovers don't want the delevelopers wasting their time with the distribution portion. If your binary packages don't work complain to the author of the package not to these wonderful people.\n\nI hate to rant but this is ridiculous, every time there is an upgrade we hear the same people with the same complaints, if you don't like it create your own solution or use something different.\n\nWWarlock"
    author: "WWarlock"
  - subject: "Solution is to *improve* KDE-Installer"
    date: 2001-08-15
    body: "> I hate to rant but this is ridiculous, every time there is an upgrade we hear the same people with the same complaints, if you don't like it create your own solution or use something different.\n\nYes, I agree with you, but isn't my question of KDE-Installer which is still in Alpha stage and not seen any boost from March 2001 be released which helps install KDE on Debian, RedHat, Mandrake, SuSE and even FreeBSD. I feel sorry that I couldn't learn KDE programming; and there are many users who are non-programmers and hence can't contribute to KDE by algorithms.\n\nI wish that some of the best developers (we have all the best ones) gives few hours to KDE-Installer improvement or say development then these _stupid_ complaints won't appear in future on every KDE release and Our busy developers don't have to listen to these irritating complaints regarding rpm, deb, tgz, etc.\n\nRegarding the compile from sources, I did that too:\nwhen I ran \"rpm --rebuild kdelibs-2.1.1.src.rpm\", I slept and woke up it took 3 hours to get only 1 package get compiled, could you expect everyone or say anyone to waste this much of time 15-20 hours just to get KDE upgraded. I guess this will discourage KDE user. \n\nI appreciate KDE developers for creating such a great Desktop environment painstakingly and giving to others for free, but does that mean creating it with hardwork should also mean getting it with hardwork? I guess no!\n\n> So give the developers a break, this arguement is old, and most of the true KDE lovers don't want the delevelopers wasting their time with the distribution portion. \n\nKDE-Installer is distribution independent regarding installing part (it will choose appropriate packages and dependencies to install/upgrade) please visit: http://kdeinstaller.rox0rs.net/ for more info.\n\nI feel that if fully functional (non-beta/stable) KDE-Installer is provided by our KDE team as a base package then the trouble is over (almost)."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Same Trouble to KDE users :( by KDE People ;)"
    date: 2001-08-15
    body: "I find it ironic that KDE, whos goal is to build an *easy to use* and *graphical* user interface, yet requires you to go into *big trouble* of installing and upgrading it and/or knowledge of *commandline* tools and CVS use.\nIf you build a commandline software, then require commandline to install/upgrade. Perl is a good example of a very user friendly commandline installer/ugprader(CPAN.pm). In KDE's case they have to focus on a graphical interface. Be it just a basic interface to the commandline with three buttons (CONFIGURE/MAKE/MAKE_INSTALL) to install from source; or a more advanced interface. It doesn't matter. \nMoreover, if I have already installed KDE 2.1.1 on some Linux distribution, upgrading to 2.2 should be one click. All the configuration details should be there since I already have an old version installed."
    author: "Sol Hell"
  - subject: "Re: Same Trouble to KDE users :( by KDE People ;)"
    date: 2001-08-16
    body: ">> yet requires you to go into *big trouble*\n\n    KDE does not require *you* to do anything - they require the distro/OS provider to provide KDE through normal update channels.  If that's a graphical icon labeled \"Updates\" on your desktop, then that's what you do.  If it's a .tgz package that you must \"tar zxvf\", that's another.  \n\n    Anyone who has a graphical apt-get front end, for instance, points and clicks to update.  Is that what you're looking for?  Get a different OS or flavor of that OS.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Same Trouble to KDE users :( by KDE People ;)"
    date: 2001-08-17
    body: "Don't tell me that KDE require you not to install KDE by yourself but wait for the distributions."
    author: "Sol Hell"
  - subject: "Re: Same Trouble to KDE users :( by KDE People ;)"
    date: 2001-08-17
    body: "Precisely.  KDE does not do any packaging.  It is up to the distros to do so.  Who would you rather have doing the packaging: someone who is making packages for a dozen distros, or the people who developed the distro?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Same Trouble to KDE users :( by KDE People ;)"
    date: 2001-08-18
    body: "Read my original post. It is not about packaging. It really doesn't matter. You need open your eyes and improve your vision. If you and other KDE guys keep repeating the same thing there is no need to discuss anything.\nAs I said, it doesn't matter. Be it installing from sources, using CVS, some special KDE packaging, etc. etc. it doesn't matter. It should have a graphical interface with user friendly on screen help. If you ask people to open README.txt, INSTALL.txt, etc. simultanously read all of them and execute command line files, edit some conf files, and other non-sense; and if you are KDE who aims to become a user friendly GUI; it doesn't make sense to me."
    author: "Sol Hell"
  - subject: "Re: Same Trouble to KDE users :( by KDE People ;)"
    date: 2001-08-16
    body: "I'm afraid that over 95% computer users already use \"something different\" and it will be 100% soon if you keep answering to all that asking for help and complaining is \"ridiculous\" or \"create your own solution\".\n\n I think that those posts are also read by packagers."
    author: "Krame"
  - subject: "Re: Same Trouble to KDE users :( by KDE People ;)"
    date: 2001-08-16
    body: "I absolutely agree... the original poster (Asif Ali Rizwaan) made a some good comments, particularly about the ability of Netscape, et al. to deliver a (mostly) distro-independant product.\n\nWhy is this not possible? At least in part. I can understand the need to possibly build the kdelibs as distro-dependant, but the rest of the system should be abstracted from this. Now that speed improvements are available (objprelink and the upcoming gcc optimisations) shouldn't the focus now be on making the installation process easier.\n\nIf the kdelibs/base layer were distro-dependant, couldn't this be downloaded with an X-app and compiled and then a KDE-native installer bring the rest of the compiled binaries down? These would (should?) not be reliant on the dependencies we're seeing at the moment.\n\nI think the whole \"we only do source code\" argument is wearing thin. You make the product, you should at least take some charge of quality assurance. After all, if a distro releases a \"bad\" version of KDE, who do think will be the target of complaints?\n\nSo, getting back to Asif's comments: although I understand that KDE is much bigger than StarOffice, Netscape, etc... there MUST be a way to release a mostly distro-independant version of KDE... otherwise how do THEY do it?\n\n</big_rant!>"
    author: "Billy Nopants"
  - subject: "Re: Same Trouble to KDE users :( by KDE People ;)"
    date: 2001-08-16
    body: "Providing of binaries is the task of the distributors.\n\nEvery distribution has its own compiler ( which changes sometimes with the release ), uses its level of optimisations ( e.g. compiled for i586 in Mandrake vs i386 RedHat, prelinking? etc. ) and has different install directories.\n\nWhy should they provide useless ( non optimised ) generic packages - when distributors anyway will package KDE in the best form for their distribution? If your distribution doesnt provide good packages, then complain there, where you have paid for the service, not here, the developers have different things to do.\n\nQuote\"\nI have to allege that you are denying our basic right of getting upgrades quicker and you are giving us trouble by only providing source\nYou have absolutely no right for anything\n\"\nNo you dont have any rights regarding the software. And calling an update a \"basic right\" is bad rhetoric.\n\nJust look at the Acrobat binaries - Unix is not Linux ( PPC is missing of course ). What you are asking for is ridiculous.\n\nAcrobat Reader+Search 4.05 for IBM AIX - English\nArobat Reader 4.05 for Linux - English\nAcrobat Reader 4.05 for Sun Solaris x86 - English\nAcrobat Reader+Search 4.05 for Dec Alpha OSF/1 - Acrobat Reader+Search 4.05 for HP-UX - English\nAcrobat Reader+Search 4.05 for SGI IRIX - English\nAcrobat Reader+Search 4.05 for Sun Solaris SPARC"
    author: "ac"
  - subject: "Re: Same Trouble to KDE users :( by KDE People ;)"
    date: 2001-08-16
    body: "For crying out loud! Don't you think that Adobe's solution is still clearer than ours?\n\nJeez, they're offering different versions of their software for COMPLETELY different OS's that have nothing in common from a binary standpoint.\n\nOr did you not notice that Linux (EVERY BL**DY DISTRO!) is represented by just one link?\n\nThat's what we should aim for... or at least something aproaching it.\n\n--BN!"
    author: "Billy Nopants"
  - subject: "Re: Same Trouble to KDE users :( by KDE People ;)"
    date: 2001-08-16
    body: "This is another reasons why KDE doesnt do the final packaging:\n\nThe choice of the size of the packages i.e. all in one big file (100MB - no choice ) or many smaller packages - ( choice and less download problems ,but maybe difficult to install if your distribution doesnt provide something like apt-get ). Nothing demands from the distributor to use the same packaging that KDE provides. \n\nWhy do you think one link ( == file ) is better?\nThere are many who would disagree. And again: its\nnot every distribution. PPC is missing, the binaries are not optimised, you have to tell the installer the target directory ( and thats only for one program, not the 50? KDE consists of, and which need install directories dependent on the distribution.\n\nThe distributors provided KDE adjusted to their distribution on the release date.\nWhere is the problem?\n\n>That's what we should aim for... or at least something aproaching it.\n\nNormally I would say: so when do you start packaging?\n\nBut in this case:\nThis is the task of the distribution - I think some distributions allow for submission from outside."
    author: "ac"
  - subject: "RED-CARPET for kde?"
    date: 2001-08-19
    body: "I agree to a certain extent although I have to say that I compile just about everything that is on my system.\n\nWhat kde needs is a sort of red-carpet (like what ximian gnome has. it really is awesome) on steroids that can use wget and netselect to check various mirrors around the world to find out which one is the fastest etc and make it really easy in the sense that you can simply select what you want to install and it checks the mirrors, downloads and configures software etc etc and make an advanced users mode that lets people override stuff as well.\n\nFor non techies it really is quite difficult for them to set it all up compared to the gnome which installs *very* easily with the go-gnome script. (not to say that i'm a gnome user i just think they do the install part much better)\n\nUs debian users really are spoilt with apt-get! :)\nThe kde developers really need to sort this kde-debian thing out, and fast!"
    author: "Anonymous Coward"
  - subject: "RED-CARPET for kde?"
    date: 2001-08-19
    body: "I agree to a certain extent although I have to say that I compile just about everything that is on my system.\n\nWhat kde needs is a sort of red-carpet (like what ximian gnome has. it really is awesome) on steroids that can use wget and netselect to check various mirrors around the world to find out which one is the fastest etc and make it really easy in the sense that you can simply select what you want to install and it checks the mirrors, downloads and configures software etc etc and make an advanced users mode that lets people override stuff as well.\n\nFor non techies it really is quite difficult for them to set it all up compared to the gnome which installs *very* easily with the go-gnome script. (not to say that i'm a gnome user i just think they do the install part much better)\n\nUs debian users really are spoilt with apt-get! :)\nThe kde developers really need to sort this kde-debian thing out, and fast!"
    author: "Anonymous Coward"
  - subject: "Re: RED-CARPET for kde?"
    date: 2001-12-06
    body: "I would be happy if somebody just created a \"KDE\" channel for Red Carpet. Why duplicate the effort if Red Carpet works? Most users will probably mix KDE and GNOME to select the \"best programs\" each platform has to offer. At least, I do. I like and use KDE, but nothing in KDE comes close to Ximian Evolution."
    author: "Marcel Offermans"
  - subject: "Re: RED-CARPET for kde?"
    date: 2001-12-07
    body: "> I would be happy if somebody just created a \"KDE\" channel for Red Carpet.\n\nWhy don't you? ;0\n\n> Why duplicate the effort if Red Carpet works? Most users will probably mix KDE and GNOME to select the \"best programs\" each platform has to offer.\n\nYes, true. It'd probably be a weekend job to convert the Red Carpet interface into a native Qt/KDE app. Most of the package/network handeling code is nicely abstracted into a lib in redcarpet anyways.\n\n> At least, I do.\n\nI think quite a few people do. I do (I used xmms and xchat, although I may consider at least replacing xmms) once KDE 3 rolls around.\n\n>I like and use KDE, but nothing in KDE comes close to Ximian Evolution.\n\nHow about kmail+kpim? I think that the major things missing from Kmail that Evolution has will be added in KDE 3. See http://developer.kde.org/development-versions/kde-3.0-features.html. Look at the Kmail finished stuff. I've tried kmail-cvs, and it seems to fix many of the imap bugs that were present. \n\nOf course, if you need exchange, neither Evolution nor Kmail+kpim will do. You'd have to buy Ximian Connecter."
    author: "jz"
  - subject: "Re: giving you trouble?? you bastard!"
    date: 2001-08-22
    body: "You wrote: \"I have to allege that you are denying our basic right of getting upgrades quicker and you are giving us trouble by only providing sources.\"\n\nIt's this kind of attitude that makes me doubt whether Open Source can ever succeed. Do you not realize that there is no such thing as a \"basic right to upgrades\". What the fuck were you thinking? There are dozens or hundreds of people working on a project - for free! - and you bastard start telling them about your \"rights\" to THEIR work. You should be happy that they let you use, copy, and modify (hey, that's an idea: stop whining and write a KDE installer; how about that?) the fruits of their labours.\nYour last statement is probably the sickest thing I've heard in years. Does it not occurr to you that first of all they are not \"giving you trouble\", but hundreds of thousands of lines of (mostly well working) code???\n\nI like reasoned arguments, but for once I'll let my instincts get the better of me; so I conclude with a hearty FUCK YOU!"
    author: "Hugo Hering"
  - subject: "Take Cygwin as an example, was: Re: Same Trouble"
    date: 2001-09-01
    body: "For me as a SuSE user it normally is not such a big problem to upgrade KDE, because they compile binary packages and offer them for download as they are required to. And I am a software developer, so I can usually solve that. \n\nBut for other people who have not installed KDE at all or do not have such a KDE-friendly distribution, when the are not software developers or other computer people, but just non-technical linux users they have a big problem.\n\nTo give an example/vision how an easy installation of a huge software system can work: at work I recently installed CygWin on Windows NT: \nAt first you click on \"INSTALL NOW\" on their homepage http://cygwin.com, then a setup_cygwin.exe is downloaded, which you start. It shows the already installed files and the available new versions, you can select by clicking what you want to upgrade. Then you can select some user settings, e.g. the download directory on your machine etc. Now you can select an FTP-server from a huge list and finally it downloads the stuff showing a download progress window similar to Netscape.\nAt the end it starts the installation of the downloaded packages and shows the README.\n\nThe additional problems we have are:\n- different distros need different KDE-binaries\n- the download source depends on the distro\n\nThat should not be so difficult to solve: if such an installer is part of the base KDE installation, the distro will be required to put it in their KDE installation (distro _and_  download offers) - maybe even customized to the appropriate download directories for that distro. That would make the first install _and_ upgrade from KDE to KDE easiest. \n\nFabian"
    author: "Fabian"
  - subject: "RedHat RPMS do not have CUPS printing support"
    date: 2001-08-15
    body: "SO far, everything has been wonderful after upgrading to KDE 2.2, however it seems that my CUPS printing isn't available via kdeprint.  I'm assuming that this is because CUPS support wasn't compiled in.  This kinda sucks that kdeprint isn't a seperate package that I could just rebuild with CUPS support.  Any ideas on how I can get this?  (Yes, I know RedHat does not ship CUPS, but I like it ;) )\n\nAnyway,  besides a few minor issues and one crash that I've had, everything has been working very well.  Good work KDE Team!"
    author: "Christopher Young"
  - subject: "Re: RedHat RPMS do not have CUPS printing support"
    date: 2001-08-15
    body: "Some chap commented on this a few posts earlier. To repeat my reply to his posting, the same happened to me -- but there's a twist: I'm using Debian Woody (testing), with XF86 + CUPS from Debian Sid (unstable).\n\nYour posting points to a possible explanation to our predicament: that you (might) need to compile in support for whatever underlying printing engine you use. Urgh, this is not good...\n\nMichael Goffioul: is this true? Or does kdeprint automagically adapts to whatever is present?"
    author: "bruno majewski"
  - subject: "Re: RedHat RPMS do not have CUPS printing support"
    date: 2001-08-16
    body: "What you can do is to grab a kdelibs source package, uncompress it and configure it with\n\n./configure --prefix=<where-kde-is-installed>\n\nThen go to kdeprint/cups/ and try \"make\". The problem will be at linking time, as the Makefile's a configured to used the libraries from the source tree and not installed libraries. So you may need edit the Makefile's and do the needed changes in the *_LIBADD tags to use the libraries installed in $KDEDIR/lib. You can contact me privately if you need some assistance to do it.\n\nMichael."
    author: "Michael Goffioul"
  - subject: "Re: RedHat RPMS do not have CUPS printing support"
    date: 2001-08-16
    body: "Since we don't ship cups, we can't link anything against libcups.\n\nHowever, I've expected that this problem would come up (seriously, LPRng sucks) - simply install the source rpm, change the \"%define cups %{nil}\" somewhere near the beginning to \"%define cups cups\", rebuild, and it'll work."
    author: "bero"
  - subject: "Re: RedHat RPMS do not have CUPS printing support"
    date: 2001-08-16
    body: "Thanks bero,\n\nI'll give this a try and hopefully, things work out well. :)  It's amazing.. you ask about something, and then you find out that it's there; just need to put a little work in it! :)\n\nChris"
    author: "Christopher Young"
  - subject: "Re: RedHat RPMS do not have CUPS printing support"
    date: 2001-08-16
    body: "Red Hat won't ship CUPS with 7.2 either?"
    author: "Matt"
  - subject: "Re: RedHat RPMS do not have CUPS printing support"
    date: 2001-08-16
    body: "No.\nDon't ask my why, I most certainly disagree."
    author: "bero"
  - subject: "Re: RedHat RPMS do not have CUPS printing support"
    date: 2001-10-05
    body: "So you would reccomend CUPS for a production environment?  I agree that LPRng is complete mess, but I was under the assumption that RedHat wasn't shiping CUPS because it wasn't robust enough yet."
    author: "Craig Kelley"
  - subject: "KDE and Ximian gnome?"
    date: 2001-08-15
    body: "While doing an apt-get of kde, I noticed that it removes gdm. I went ahead and installed kde, while intending to reinstall gdm. However, once kde 2.2 was installed, I found that installation of gdm required removed of kdm and kde.\n\nI really prefer to have gnome and kde available. Any suggestions? (please don't flame --- 'use kde all the time.' Maybe I am an unusual user, I like kde and gnome)"
    author: "ide"
  - subject: "Re: KDE and Ximian gnome?"
    date: 2001-08-16
    body: "Why can't you run Gnome from KDM? (or KDE from GDM for that matter). You don't really need both login managers...."
    author: "JC"
  - subject: "Re: KDE and Ximian gnome?"
    date: 2001-08-16
    body: "Why can't you run Gnome from KDM? (or KDE from GDM for that matter). You don't really need both login managers...."
    author: "JC"
  - subject: "KDE, KDM, Gnome and GDM :)"
    date: 2001-08-16
    body: "KDE2.2 removed GDM on install. I cannot reinstall GDM because GDM requires removal of KDM and KDE. So, I could stick with KDM. I am having problems with KDM, though. Perhaps it is because I am inexperienced with KDM (how could a *DM be difficult?) --- KDM does not accept input from the keyboard, although when exit I x the keyboard is fine (and this was not a problem with GDM). Also, I assume the sawfish option in KDM == ximian gnome?"
    author: "anonymous"
  - subject: "cant start gnome in kdm"
    date: 2001-08-16
    body: "obviously sawfish is the option to start gnome, but this option does not work for me. Where can the session settings for kdm be found? A session can be added in configuration, but the parameters/bash script cannot be modified like in gdm."
    author: "anonymous"
  - subject: "ok, to start gnome in kdm"
    date: 2001-08-16
    body: "use 'gnome-session' Thought sawfish & exec gnome-session was required."
    author: "anonymous"
  - subject: "Re: KDE and Ximian gnome?"
    date: 2001-08-16
    body: "kde package is a virtual 'task' package which requires all of the KDE packages, including kdm.\n\nkdm, gdm and xdm are mutually exclusive (only one may be installed at a time).\n\n'kdm' is a part of a complete KDE installation (the 'kde' task), so if you're not installing it, you no longer have a  \"complete KDE installation\".\n\nSolution? Install the 'kde' package (let all dependencies install along), install 'gdm' (this will remove 'kdm' and 'kde').\n\n(Since 'kde' doesn't really contain files, but simply depends on the rest of the KDE packages, you can safely remove it.)"
    author: "Toastie"
  - subject: "Re: KDE and Ximian gnome?"
    date: 2001-08-16
    body: "Thanks for the really good information.\n\nSo, my understanding (and you were clear, just want to verify I guess) is that I can install gdm and when removing kde, I can still work/start in kde because 'kde' is useless/not really the package for the desktop or applications?"
    author: "tweet"
  - subject: "Re: KDE and Ximian gnome?"
    date: 2001-08-17
    body: "Yes, feel free to remove package 'kde'. The basic KDE stuff (startkde, kdeinit etc.) is in 'kdebase' really."
    author: "Toastie"
  - subject: "Thanks Toastie"
    date: 2001-08-17
    body: "I really appreciate your help :)"
    author: "Tweet"
  - subject: "gdm and kdm"
    date: 2001-08-16
    body: "I'm not sure why people have said you can't have gdm and kdm installed at the same time. Of course you can only use one, but one doesn't remove the other. I prefer gdm to kdm, and installing kde did set my system to use kdm... but it didn't REMOVE gdm. There is usually a file called /etc/X11/prefdm that is supposed to let you select which you use! IN Mandrake and Redhat, though, that file is a fairly complex script with failsafes and stuff.\n\nThe gdm executable was still  in /usr/bin, so I went to that directory (where the kdm binary is found) and renamed the kdm binary, and then made a link to gdm called kdm:\n\nls *dm\n(should show gdm, kdm, maybe xdm)\nmv kdm _kdm\nln gdm kdm\n\nand then gdm is executed when kdm is called. You could also change /etc/X11/prefdm."
    author: "juln"
  - subject: "In Debian, gdm is removed..."
    date: 2001-08-16
    body: "yep, gdm is not there. Gone. Moreover, I confirmed the removal of gdm during kde install (the install would not continue without gdm removal and I planned to simply reinstall gdm (but gdm cannot be installed without removing kdm and kde)). \n\nPerhaps this is an issue with debian packaging. While kdm is ok, it would be very nice to have kdm and gdm available. Not happy having any application required over another."
    author: "tweet"
  - subject: "Re: gdm and kdm"
    date: 2006-06-10
    body: "When you install kde with apt-get in Ubuntu Linux, you will get a dialog box asking if you want to use gdm or kdm...\n\nNow i can boot in both kde and gnome, and i use gdm..."
    author: "Hyperknuck"
  - subject: "Re: KDE and Ximian gnome?"
    date: 2004-04-15
    body: "Here's an even simpler way to choose your display manager:\n1) Verify 'gdm' or 'kde' are at your disposal in /usr/bin by doing a 'ls -l *dm'\n2) su to root\n3) open the file called '/etc/sysconfig/desktop' with your favorite text editor (gedit) You should probably see an entry such as 'DESKTOP=\"GNOME\"'  Leave this entry alone.\n4) Add a newline 'DISPLAYMANAGER=\"GNOME\"'  Save and exit\n5) Restart X or reboot\n\nThe reason this works is the script /etc/X11/prefdm contains a line (if [ -f /etc/sysconfig/desktop ]; then .. if [ \"$DISPLAYMANAGER\" = GNOME ]; then preferred=gdm ...fi fi)  These lines look to the file /etc/sysconfig/desktop for an entry for $DISPLAYMANAGER.  Since by defaut an entry for DISPLAYMANAGER does not exist, you must add it manually.\n\nHope this helps people. I like having a choice also.\n\n\nJon  \n"
    author: "Jon"
  - subject: "KDE2.2 Revisions to xf86config-4?"
    date: 2001-08-15
    body: "What revisions, exactly, does the install of kde2.2 make to xf86config-4? thanks"
    author: "cable"
  - subject: "Re: KDE2.2 Revisions to xf86config-4?"
    date: 2001-08-16
    body: "none"
    author: "ck"
  - subject: "Impressions: Preferences / Fonts / Konquorer"
    date: 2001-08-15
    body: "First off: Hats off to all of the KDE developers!\n2.2 is smoother than 2.1.2, with even more things that \"just work.\"\n\nNow, a few constructive criticisms. Perhaps some of these should be bug-reports.\n\n(1)\nKDE2.2 completely wiped out my personal preferences as well as the login \"sessions\" and login manager preferences. Not a nice thing to do. Not the end of the world, but a warning would be nice. It would be even better to respect previous preferences.\n\n(2)\nAt least on my machine, the default fonts are completely illegible, and anti-aliasing makes it worse. Perhaps it's my LCD panel. Not a big deal.\n\n(3)\nKonquorer is acting a bit strange. eg. clicking on a text file launches Korganizer and gives an error.\n\n(4)\nThe preferences seem to get corrupted. Nothing definite yet, just \"odd\" behavior.\n\n(5)\nGtop still reports 135MB in use by KDEInit with only Kmail, Konquorer, and emacs running. This seems a bit large.\n\n\nThanks for all of the hard work!\n\nMatt"
    author: "Nobody"
  - subject: "More oddities"
    date: 2001-08-16
    body: "I installed the RedHat 7.x RPMS and Kicker is unstable. It seems unpredictable, but when I set to auto-hide and wait for it disappear, then put my mouse at the bottom of the desktop, it core dumps (!). It also core dumps when I start KDE, forcing me to start a terminal and restart it. (Dr Konqi isn't invoked)\n\nAny ideas as to what is wrong?"
    author: "Rk"
  - subject: "Re: More oddities"
    date: 2001-08-16
    body: "I thin I've found the problem - \n\nQGDict::hashAsciiKey: Invalid null key\n---\nCan anyone explain what this means?\nDoes it have something to do with applets?\nHow can I fix it?"
    author: "Rk"
  - subject: "Re: More oddities"
    date: 2001-08-16
    body: "The problem appears by the Taskbar applet...\nSo, my new shiny taskbar, well, can't be a taskbar, at least for the moment. Oh well, there's always Alt+Tab"
    author: "Rk"
  - subject: "Re: Impressions: Preferences / Fonts / Konquorer"
    date: 2001-08-16
    body: "It's like the old saying, no good deed goes unpunished. ;-) Something are done by others and reflect poorly on the wrong people. Case in point. RPMs are generally assembled by somebody working for the distro. In fact they change requirements for every version so...\n\n> (1)\n KDE2.2 completely wiped out my personal preferences as well as the login \"sessions\" and login manager preferences. Not a nice thing to do. Not the end of the world, but a warning would be nice. It would be even better to respect previous preferences.\n\nThis is one place that is a challenge. Try installing XFree86 binaries and answering yes to everything and see what I mean. X start up is highly customised not only for each distro but differes greatly even by revision. Translation, this is a distro issue... one that seems to frequently get hosed.\n\n> (2)\n At least on my machine, the default fonts are completely illegible, and anti-aliasing makes it worse. Perhaps it's my LCD panel. Not a big deal.\n\nYup, that's a problem. If you are running a card capable of xrender and have QT built for it along with KDE you will almost certainly need to edit XftConfig. Yes, you now have two font config files and no, there is no simple way to deal with this. If KDE does it then the distros will change it anyway. It's their mark, and there are not many decent fonts for free anyway. If you can import Windows fonts they are pretty good and most likely if you play with it you will get nice results. Search out antialiasing fonts here on the dot.\n\n> (3)\n Konquorer is acting a bit strange. eg. clicking on a text file launches Korganizer and gives an error.\n\nCool! ;) Go to kcontrol and see what's hosed in your file associations. Someone didn't have enough coffee.\n\n> (4)\n The preferences seem to get corrupted. Nothing definite yet, just \"odd\" behavior.\n\nAre they not being saved? Run this \n$ ls -l ~/.kde | grep root\nIf you have fonfig files with root ownership then using your [username] as root\n# chown username.username -R /home/username/.kde/*\n\n> (5)\n Gtop still reports 135MB in use by KDEInit with only Kmail, Konquorer, and emacs running. This seems a bit large.\n\ngtop? Hey man... this is the dot. ;) Ctrl+Esc gets KDE System Guard... but they use the same resources. Note kmail running 30 megs? This has been discussed on kde-devel. Essentially these numbers are not entirely accurate (though I forget teh exact explanation after a long day).\n\nCheers"
    author: "Eric Laffoon"
  - subject: "KDE 2.2 dependencies"
    date: 2001-08-15
    body: "Is there any reasons why the KDE packaging team is not posting ALL of the dependencies for KDE 2.2 in their non-kde directory? I have RedHat 7.1 and update it regularly, and I do not have glibc-2.2.3, lesstif, or rpm-4.0.3. I would sure hope RedHat (http://www.redhat.com/)and RPM.org (http://www.rpm.org/) would have the latest version available. 4.0.3 isn't even out yet.\nWhat is going on here? I don't think KDE 2.2 is worth the hassle of tracking down and compiling all of the dependencies. I have an awkward processor that requires me to compile everything and not use rpms. This just takes too long."
    author: "David van Hoose"
  - subject: "Get KDE 2.2 dependencies"
    date: 2001-08-15
    body: "To get all (perhaps most) Dependencies/updates to RedHat 7.1 Visit:\n\nhttp://rpmfind.net/linux/rawhide/1.0/i386/RedHat/RPMS\n\nAnd download rpm-4.0.3, etc."
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE 2.2 dependencies"
    date: 2001-08-16
    body: "Glibc is there now, so you can update to rpm-4.0.3 (you will break dependency with gnorpm)."
    author: "Oliver"
  - subject: "Re: KDE 2.2 dependencies"
    date: 2001-08-16
    body: "People, read the README files. I don't write them for nothing.\n\nftp://ftp.kde.org/pub/kde/stable/2.2/RedHat/7.x/non-kde\n\nhas all the dependencies you might need."
    author: "bero"
  - subject: "Re: KDE 2.2 dependencies"
    date: 2001-08-16
    body: "I've scoured the README's, looked in the above listed directory, but what is still desperately needed is a concise list of all libraries that KDE is capable of using. I'm compiling from source and am on an SGI, so rpms do me no good. Please, add this to the FAQ.\n\nYes, I can glean some information from the above directory, but what about LDAP?  What LDAP library do I need? I'm sure that there are other such optional dependencies that are not in the above directory. Besides, this should be somewhere that is easy to get at. I'd make the list if I had any idea what is needed. But I don't, so I can't."
    author: "Falrick"
  - subject: "Re: KDE 2.2 dependencies"
    date: 2001-08-17
    body: "Well there is one dependency missed in that rpm-4.0.3 asked for glibc-2.2.3.\n\nI was wondering why you chose to build against rahide or roswell?  If these rpms are designed for 7.x, then the least common denominator of 7.x should be used as that way anything greater could be supported as there is binary compatibility inside of 7.x on redhat.  Building against rawhide or an unreleased beta seems that it can only cause headaches for new users :)\n\nI can't say I had any trouble with this, but I can see where confusion could come from.\n\nBill"
    author: "Bill Vinson"
  - subject: "Re: KDE 2.2 dependencies"
    date: 2001-08-20
    body: "Thanks for the packages bero.\n\nI had some problems (konqueror hanging for one)\nusing the version of FAM supplied (fam-2.6.4-9),\nbut the latest FAM from Raw Hide (fam-2.6.4-10)\nworked well.  I would recommend using this\ninstead (or changing rpc_version=1-2 to\nrpc_version=2 in /etc/xinetd.d/sgi_fam)."
    author: "Michael Wardle"
  - subject: "Will artsd ever use less than 100% CPU"
    date: 2001-08-15
    body: "It's probably on my system only but for I don't know how many releases, the sound servers have messed up KDE on this desktop at least."
    author: "Jarek Luberek"
  - subject: "Re: Will artsd ever use less than 100% CPU"
    date: 2001-09-01
    body: "Did you consider possibly that artsd is scaling it's usage? I don't know too much about arts myself, but programs sometimes do this (that is, using as much CPU as possible, but giving it up if the kernel/another process requests it). \n\nAlso, did you try turning off realtime priority?"
    author: "Carbon"
  - subject: "Re: Will artsd ever use less than 100% CPU"
    date: 2001-11-22
    body: "same on my system.\n\nUses about 85% leaving 10% for dnetc and 5% for other misc processes."
    author: "dan"
  - subject: "no https???"
    date: 2001-08-15
    body: "Thanks, this is a great step forward. Looks better, behaves better. The improvements of Konqueror (which already was good) are great, with the very notable exception that I'm told HTTPS is not a supported protocol. Where did HTTPS go? I need it!\n   _\n/Bjorn."
    author: "bjorn"
  - subject: "Re: no https???"
    date: 2001-08-16
    body: "HTTPS is still there, but you need a new version of OpenSSL to use it."
    author: "Rk"
  - subject: "Mandrake 8.0 Install"
    date: 2001-08-15
    body: "Did anyone get KDE2.2 to install cleanly via the posted RPMs under Mandrake 8?  I get a boatload of dependency errors.  I tried using urpmi and rpm -UVh.  Any suggestions or could somebody outline their process?\n\nThanks,\nJoe"
    author: "Joe Gadell"
  - subject: "Re: Mandrake 8.0 Install"
    date: 2001-08-16
    body: "Yep, I'm running it on Mandrake 8.0 right now. I had to uninstall some earler kde2.x rpm's, and get a few ones from mandrake (I used cooker)\n\nThe packages I grabbed were:\nlibpcre0-3.4-3mdk\nlibpcre0-devel-3.4-3mdk\npcre-3.4-3mdk\nOh yeah, and I installed apmd too, but you might already have that installed. \n\nhe package I uninstalled wes\nkdeaddutils-2.0-3mdk, but there might have been one or two more. Check rpm's output. \n\nThe Mandrake packages seem to come without objprelink (well, no massive speedups here at least). Defintiely snappier than before but, I'm going to see if I can't find some with objprelink"
    author: "Rikard Anglerud"
  - subject: "Re: Mandrake 8.0 Install"
    date: 2001-08-16
    body: "Try http://www.pclinuxonline.com\n\n--BN!"
    author: "Billy Nopants"
  - subject: "HTTPS isn't working after transition from 2.2beta1"
    date: 2001-08-15
    body: "I get \"The https protocol isn't supported\" (in GERMAN :-)\n\nSo I can't access my dri.sourceforge.net account any longer.\n\nSuSE Linux 7.1\nAm I missing something?\n\nThanks,\n Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: HTTPS isn't working after transition from 2.2beta1"
    date: 2001-08-15
    body: "Too old version of openssl. You need at least 0.9.6, which can be found somewhere on SuSE ftp-server."
    author: "someone"
  - subject: "Re: HTTPS isn't working after transition from 2.2beta1"
    date: 2001-08-16
    body: "Thanks,\n\nI had it but I've upgraded to the latest SuSE 7.1 one, now.\n\nSunWave1>rpm -qa|grep ssl\nopenssl-0.9.6a-42\nopenssl-doc-0.9.6a-42\nopenssl-devel-0.9.6a-42\n\nDidn't help :-(\n\nIf I do some little diagnostics I think only libkssl is involved?\n\nSunWave1>l /opt/kde2/lib/libkssl.*\n-rwxr-xr-x    1 root     root         1001 Aug 10 22:22 /opt/kde2/lib/libkssl.la\nlrwxrwxrwx    1 root     root           16 Jul 11 17:35 /opt/kde2/lib/libkssl.so -> libkssl.so.2.0.2\nlrwxrwxrwx    1 root     root           16 Jul 11 17:35 /opt/kde2/lib/libkssl.so.2 -> libkssl.so.2.0.2\n-rwxr-xr-x    1 root     root       172370 Aug 10 22:22 /opt/kde2/lib/libkssl.so.2.0.2\n\nLatest SuSE 7.1 KDE-2.2.\n\n-Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: HTTPS isn't working after transition from 2.2beta1"
    date: 2001-08-16
    body: "This is intentional so far. Since several users have already updated their OpenSSL package to a security fixed, but this version is not binary compatible to older version (the several 0.9.6* version are not binary compatible to a different 0.9.6*) the SSL support has been disabled to protect against crashes for SuSE < 7.2.\n\nI will build a kdelibs version which include a OpenSSL version in the next days. ( it was a suggestion from Dre)."
    author: "Adrian"
  - subject: "Re: HTTPS isn't working after transition from 2.2beta1"
    date: 2001-08-27
    body: "Thanks a lot, Adrian!\n\nIf I only could my XFree 4.1.0 (SuSE 7.1 update) plus DRI running together with the new kdm...\n\n-Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "slackware compile problems"
    date: 2001-08-15
    body: "hi,\nonce again bing thanks to all kde developers for another great release.\n\n...but i can't compile it on slack8.0. kdelibs compiles fine, but the kdebase is a bit problematic. first thing is kpersonalizer.\nlots of \"undefined symbols\" and looks like that\n*.cpp files are empty (well only\n\n#include <klocale.h>\n#include \"somefilename.moc\"\n\nin them. should it be this way?\nsame thing in ksysguard and kscreensaver.\n\nthis would be not a problem to me. i could live without kpersonaizer and ssavers and i use ksysguard rarely, but thats not it.\n\nso i ignore make errors (make -i) and install it anyways. i startx & startkde and while loading\ni get segfaulted: kded, kontify also lotsa problems with DCOP communication. then i can't go to control center (half of modules have undefined symbols), konqueror crashes a lot, etc, etc, etc...thats very strange.\nanyway i hope that some1 had the same problem and solved it \n\nTIA\nutopia\n\nk now here's what i use:\negcs-2.91.66"
    author: "utopia"
  - subject: "Re: slackware compile problems"
    date: 2001-08-16
    body: "As a slack user you probably know this, but the ecgs fork was moved back into gcc (the Gnu Compiler Collection), as a result, kde may not be trying very hard to support egcs.  Try putting g++ on your system, perhaps one of the newer 2.x versions, and compiling it.\n\nGood luck, though!"
    author: "Benjamin Atkin"
  - subject: "Where's the 2.2 source on ftp.us.kde.org ??"
    date: 2001-08-16
    body: "I checked ftp://ftp.us.kde.org\n\nThere are the following trees, but no sign of 2.2:\n2.0.1\n2.1-update\n2.1.1\n2.1.2\n2.1\nlatest (2.1.1)"
    author: "wireless"
  - subject: "\"Automatic Download and Install\" Script?"
    date: 2001-08-16
    body: "I am new to Linux.\n\nWhen I wanted to install Ximian GNOME, I became\nsuperuser and simply typed:\n\nlynx -source http://go-gnome.com/ | sh\n \nIs there a similar script for KDE 2.2?"
    author: "NB"
  - subject: "Re: \"Automatic Download and Install\" Script?"
    date: 2001-08-16
    body: ">> Is there a similar script for KDE 2.2?\n\n    In theory, yes.  You see, KDE lets the individual distributions package KDE, rather than doing it themselves.  This leads to some nice benefits down the road.\n\n    For instance, if you're using debian, you might type somthing like \"apt-get kde\", or if you're using a rpm based distro, you might click on the \"Upgrade\" or \"Updates\" icon on your desktop, and choose KDE.\n\n    Basically, since the distro maker packaged it, they should provide it in their normal \"update\" channel.  The upside is that you don't have to learn a new way to install it - it's the same way you install all upgrades and patches.  The downside, of course, is that you have to know what to do for your distro.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "KDE headers installed...."
    date: 2001-08-16
    body: "I use Redhat 6.2 with kernel 2.2.19. So, I'm left with compiling the binary myself.\n\nIs there any order in which one needs to do the compile?\n\nAlso when I compile the kdesdk I get the following message:\nchecking for KDE... configure: error: \nin the prefix, you've chosen, are no KDE headers installed. This will fail.\nSo, check this please and use another prefix!\n\nWhat does this mean?\n\nthanks,\n-Mathi"
    author: "mathi"
  - subject: "Re: KDE headers installed...."
    date: 2001-08-16
    body: "I've now compiled the KDE Source and installed them. But when I do a startkde I get the following...\n\nxsetroot:  unable to open display ''\nxset:  unable to open display \"\"\nksplash: cannot connect to X server \nAborting. $DISPLAY is not set.\nAborting. $DISPLAY is not set.\nERROR: KUniqueApplication: Pipe closed unexpected.\nksmserver: cannot connect to X server \nconnect() failed: : Connection refused\n\nI run Redhat6.2 with gcc 2.95.3 & XFree4.1. Do I need to make any modifications? Any help???\n\n-Mathi"
    author: "mathi"
  - subject: "KDE needs X"
    date: 2001-08-16
    body: "To start KDE, you need to \nuse X.\n\nAlso see the FAQ about this question:\nhttp://www.kde.org/documentation/faq/install.html#startkde"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: KDE needs X"
    date: 2001-08-16
    body: "thanks for the reply. I got it working. KDE is awsome...\n\n-Mathi"
    author: "mathi"
  - subject: "Re: KDE headers installed...."
    date: 2004-05-19
    body: "i don't understand your reply please help me"
    author: "wissasm"
  - subject: "help needed KDE headers installed...."
    date: 2002-03-18
    body: "hi \n   i am installing koffice on kde 1.1.1 on redhat 6.2\ni have intalled qt but when i configure kofiice i get the same\n\nhecking for KDE... configure: error: \n      in the prefix, you've chosen, are no KDE headers installed. This will fail.\n      So, check this please and use another prefix!\n \nif u fixed ur prob. then u can help me.what has to be done\n\n1.did u install kde2 or u made it work on kde 1.1.1?\n\ndo mail me\n\nregards\nanuj\n\n\n"
    author: "anuj"
  - subject: "Re: help needed KDE headers installed...."
    date: 2002-04-15
    body: "I have the same problem, I downloaded the kdevelop-2.0.2 and did a configure and I got the same error checking for KDE... configure: error:\nin the prefix, you've chosen, are no KDE headers installed. This will fail.\nSo, check this please and use another prefix!\nWere u able to fix the problem\n\nRegards\nSherif\n"
    author: "Sherif Hammouda"
  - subject: "Re: help needed KDE headers installed...."
    date: 2002-05-11
    body: "I had the same problems trying to build several programs.\nIn the end i fixed it by installing the kdelibs-devel RPM (Redhat 7.3).\n\nHope that works for you too\n\nCheers"
    author: "Lachlan Laycock"
  - subject: "Re: help needed KDE headers installed...."
    date: 2003-01-31
    body: "Thanks,\n\nThis also helped in SuSE 8.1 installing the remote desktop package.\n\nCheers,\n\nFT"
    author: "Frank Thomas"
  - subject: "Re: help needed KDE headers installed...."
    date: 2004-04-10
    body: "works also for me in Suse 9.0\n"
    author: "Alejandro Paal"
  - subject: "Re: help needed KDE headers installed...."
    date: 2004-07-31
    body: "This also helped me. Thank you.\n\njs"
    author: "Joseph Semlak"
  - subject: "Re: help needed KDE headers installed...."
    date: 2006-12-04
    body: "Worked in Ubuntu 6.06 while trying to compile Amarok from source.\nThanks for the help. This has been bugging me all day.\n"
    author: "zela"
  - subject: "Re: help needed KDE headers installed...."
    date: 2007-01-10
    body: "hoho, this worked for me too, on openSUSE 10.2"
    author: "Dennis.se"
  - subject: "Re: help needed KDE headers installed...."
    date: 2007-02-16
    body: "Worked on compiling amarok from source on ubuntu for me, as well .. THANX"
    author: "P\u00e5l Hatlem"
  - subject: "Re: help needed KDE headers installed...."
    date: 2007-06-04
    body: "THANKS\n\nThis was just the fix for configuring kfolding on Kubuntu 7.04"
    author: "booga"
  - subject: "Re: help needed KDE headers installed...."
    date: 2007-06-26
    body: "w00t! Thanks this helped me too. Trying to get kdenlive compiled from source on Kubuntu Feisty. :)"
    author: "Liam"
  - subject: "Re: help needed KDE headers installed...."
    date: 2008-05-17
    body: "thanks  that helped me to compile kfreeflight on suse 10.3"
    author: "pjhoyer"
  - subject: "Re: help needed KDE headers installed...."
    date: 2004-09-02
    body: "Ya, This is worked fine for me in Suse 9.0 OS"
    author: "rajesh"
  - subject: "Re: help needed KDE headers installed...."
    date: 2005-02-10
    body: "dont work in mepis2004... any tips?"
    author: "Elv13"
  - subject: "Re: help needed KDE headers installed...."
    date: 2005-07-14
    body: "Helped - Debian sarge  (installing Umbrello 1.4.1)"
    author: "Hand"
  - subject: "Re: help needed KDE headers installed...."
    date: 2006-04-22
    body: "it also works on suse 10.0\n\nthanks"
    author: "gijs"
  - subject: "Re: help needed KDE headers installed...."
    date: 2008-09-27
    body: "worked for fedora 9"
    author: "hkernel"
  - subject: "Re: KDE headers installed...."
    date: 2002-09-03
    body: "Try this :\n\nfind / -name \"kapplication.h\"\n\nthen ./configure --prefix=/path/to/kdeheaders\n\nhope this helps.\n"
    author: "Mohamad Hassan Mohamad Rom"
  - subject: "Re: KDE headers installed...."
    date: 2003-11-07
    body: "Same problem with Debian Woody, but I found the kde headers in one of this packages... But I don't know which one...\n\nRelease  Package (size) \n\nstable  kdebase-dev 4:2.2.2-14.7   (46.4k)   \n        KDE core applications (development files) \nstable  kdelibs-dev 4:2.2.2-13.woody.8   (725.5k)   \n        KDE core libraries (development files) \nstable  libkonq-dev 4:2.2.2-14.7   (44.6k)   \n        Core libraries for KDE's file manager (development headers)\n\n"
    author: "Debian user"
  - subject: "Re: KDE headers installed...."
    date: 2004-01-25
    body: "It's the first one, kdebase-dev."
    author: "Ville Kumpulainen"
  - subject: "Re: KDE headers installed...."
    date: 2004-04-28
    body: "thanks kdebase-dev works indeed."
    author: "mat"
  - subject: "Re: KDE headers installed...."
    date: 2008-01-30
    body: "This also worked for me when trying to install Kmess SVN. Thanks"
    author: "Fred"
  - subject: "Re: KDE headers installed...."
    date: 2004-12-10
    body: "I followed your guide to finding kheaders.h but to no avail!\n\nI bought a Cherry CyMotion keyboard and am trying to compile the source for an old SGI machine, a i486 I believe, that I am running Mabdrake 10.1 with the latest KDE.\n\nI ran\n./configure --prefix=/usr/include/kheaders.h and\n./configure --prefix=/usr/include/ both to no avail\n\nI have installed as many header and devel packages as I can find in Mandrake package manager and still the ./confie says it can't find the KDE headers.\n\nCan you help me find them, install them (if not already) and point my ./configure in the right location. Thanku."
    author: "Dan"
  - subject: "Re: KDE headers installed...."
    date: 2004-12-10
    body: "I followed your guide to finding kheaders.h but to no avail!\n\nI bought a Cherry CyMotion keyboard and am trying to compile the source for an old SGI machine, a i486 I believe, that I am running Mandrake 10.1 with the latest KDE.\n\nI ran\n./configure --prefix=/usr/include/kapplication.h and\n./configure --prefix=/usr/include/ both to no avail\n\nI have installed as many header and devel packages as I can find in Mandrake package manager and still the ./confie says it can't find the KDE headers.\n\nCan you help me find them, install them (if not already) and point my ./configure in the right location. Thanku."
    author: "Dan"
  - subject: "Re: KDE headers installed...."
    date: 2004-12-26
    body: "I have the same problem with Cherry CyMotion and Mandrake 10.1.\nHas anyone solved the problem?"
    author: "J"
  - subject: "Re: KDE headers installed...."
    date: 2005-05-02
    body: "i had the same issue compiling anything! i seem to have now solved it by using --pref=\"/usr/include/\" but i did notice my kde libs are in /usr/include/kde, maybe your environment is more picky then mine? might be worth a go?"
    author: "neil"
  - subject: "Re: KDE headers installed...."
    date: 2005-08-29
    body: "I have a similar problem (I'm running Slackware 9.3).\nI've tried Mohamad's suggestion and found kapplication.h in /opt/kde/include.\nBut when I tried ./configure --prefix=/opt/kde/include I got the following error:\n\nchecking for KDE... configure: error:\nin the prefix, you've chosen, are no KDE libraries installed. This will fail.\nSo, check this please and use another prefix!\n\nWhat else can I do?"
    author: "Gabi"
  - subject: "Re: KDE headers installed...."
    date: 2006-01-25
    body: "For mandriva 2005 the following did the trick.\n\nurpmi kdelibs-devel"
    author: "Casey Watson"
  - subject: "Re: KDE headers installed...."
    date: 2006-01-30
    body: "I tryed the mandriva 2005 trick (above) and it didn't work. Urpmi says no package named kdelibs-devel. I'm having the same issue. What can I do?"
    author: "Cassio Lima"
  - subject: "Re: KDE headers installed...."
    date: 2008-02-27
    body: "se hai mandriva 2006 installa libkde4-devel"
    author: "Angelo"
  - subject: "Re: KDE headers installed...."
    date: 2004-05-10
    body: "I am having the same problem in redhat 7.3 (trying to install apollon-0.9.3) . so, as much as i hate trying to use rpms, I tried installing kdebase-devel-3.0.0.....rpm . This needs kdebase and kdebase needs about 30 other things which I dont believe. \nerror: Failed dependencies:\n        lm_sensors is needed by kdebase-3.0.0-12\n        libcrypto.so.2 is needed by kdebase-3.0.0-12\n        libDCOP-gcc2.96.so.4 is needed by kdebase-3.0.0-12\n        libkabc-gcc2.96.so.1 is needed by kdebase-3.0.0-12\n        libkatepartinterfaces-gcc2.96.so is needed by kdebas . . . . . \n\nI have been building things myself for a while would this have anything to do with it?"
    author: "Tim"
  - subject: "Re: KDE headers installed...."
    date: 2004-05-19
    body: "I have the same probleme with kde please check me the solution .I need the solution quickly please i can't  works without the solutions .I use Debian 3.0 please help me"
    author: "wissasm"
  - subject: "Re: KDE headers installed...."
    date: 2005-11-03
    body: "This should do the trick ./configure --prefix=`kde-config --prefix`"
    author: "S\u00f8ren J"
  - subject: "Re: KDE headers installed...."
    date: 2006-04-24
    body: "In Mandriva2006:\n\n      urpmi libkdebase4-devel\n\nGood Luck!\nMagnoTonus"
    author: "MagnoTonus"
  - subject: "Re: KDE headers installed...."
    date: 2006-05-04
    body: "Hi,\n\nI've installed kde-libs4-dev on Ubuntu 5.10 Breezy Badger and then invoked  \n\n./configure --prefix=/usr/include/kde/\n\nand it worked.\n\nMike\n"
    author: "Mike"
  - subject: "Re: KDE headers installed...."
    date: 2007-12-15
    body: "Also thanks the replier. I also solved my problem."
    author: "Ma Lin"
  - subject: "Re: KDE headers installed...."
    date: 2008-01-10
    body: "yum install kdevelop\n./configure\nGood - your configure finished. Start make now\n\n\n"
    author: "___Black___"
  - subject: "Re: KDE headers installed...."
    date: 2008-06-16
    body: "I've got the same problem when I setup the kscope on kubuntu8.04, \nbut it works after install the kdebase-dev-kde4."
    author: "JackChing"
  - subject: "Re: KDE headers installed...."
    date: 2008-08-04
    body: "Hi, I'm also having the same problem when trying to install twinkle SIP Phone in FEDORA 9 using the source. Please give me a solution.\n "
    author: "chamal"
  - subject: "KDM Problems"
    date: 2001-08-16
    body: "Hi,\n\nI just upgraded with the rpm's from SuSE, for 7.1. All works fine apart from kdm. Somehow, I managed to ruin my kdm configuration completely. The fonts don't show up anymore, and the different sessions apart from kde and failsafe have disappeared. Also, failsafe doesn't work anymore (the little window in the bottom right corner doesn't get focus, so that I cannot enter anything).\n\nThe kcontrol module for kdm doesn't seem to be able to fix my font problem (although I can adjust other things without problems). Has anybody experienced similar problems, or knows a solution?"
    author: "Claus Wilke"
  - subject: "Another Question..."
    date: 2001-08-16
    body: "I'm having some somewhat related (and I hope simple) problems with KDM --- 1)how does KDM start from term? (kdm or KDM does not work) and 2)I am not able to input from my keyboard in KDM --- any thoughts?"
    author: "anonymous"
  - subject: "KDM and SuSE 7.1 fails for me too"
    date: 2001-08-17
    body: "I have installed XFree86-4.1.0 on a SuSE 7.1, and I installed the new KDE rpms after removing all the old ones. Now kdm does not come up anymore and its only complaint is in /var/log/kdm.log:\n/opt/kde2/bin/kdm: error while loading shared libraries: /opt/kde2/bin/kdm: undefined symbol: _XdmcpWrapperToOddParity\n\nWhat gives?"
    author: "Michael Will"
  - subject: "Re: Another Question..."
    date: 2001-08-17
    body: "I had the same problem with my Suse7.1.  I think it had something to do with an error during kde-base install.  There is a directory called /opt/kde2/share/config/kdm that it tries to move to /opt/kde2/share/config/kdm.old inorder to create a link called kdm.  The kdm.old directory was already there, probably from some old install.  If you mv the kdm.old dir to kdm.old2 and reinstall the kdebase package it should properly install kdm."
    author: "Tim"
  - subject: "Applets? SuSE vs KDE? kdelibs-i686"
    date: 2001-08-16
    body: "Ok, where have all of the Applets gone to? Not only can I not find any of the new ones mentioned in the press release, but many of the older ones have dissapeared too (eg.system monitor).\n\nAnyone have an idea?\n\nBtw, I installed the rpm's from SuSE's site (the SuSE KDE service or whatever), not the ones from ftp.kde.org. Anyone else having trouble with these? I noticed that at least one of the rpm's on SuSE (kdeutils) was still from the beta. The rest report version 2.2.0 though.\n\nFinally, FWIW, the kdelibs-i686 personally make my machine completely unstable. As in Konquorer SigFaulting after opening up ANY website. So beware."
    author: "Nobody"
  - subject: "Re: Applets? SuSE vs KDE? kdelibs-i686"
    date: 2001-08-16
    body: "The KNOWN-BUGS file says that Javascript will Krash Konqueror."
    author: "LB"
  - subject: "Re: Applets? SuSE vs KDE? kdelibs-i686"
    date: 2001-08-16
    body: "Ok, reinstalling kdeaddons fixed the applets thing.\n\nAs for kdelins-i686, konquorer was crashing on ANY website, not just one with javascript. Removing the kdelibs-i686, and everything was fine. I'll have to look into this more."
    author: "Nobody"
  - subject: "Re: Applets? SuSE vs KDE? kdelibs-i686"
    date: 2001-08-16
    body: "As described in the README file the *-i686 packages are experimental at the moment.\n\nA new version will hit the servers anyway soon."
    author: "Adrian"
  - subject: "Re: Applets? SuSE vs KDE? kdelibs-i686"
    date: 2001-08-16
    body: "Do this mean that the next SuSE realese will be optimazed for i686 (HOPE SO)."
    author: "KDE user"
  - subject: "Re: Applets? SuSE vs KDE? kdelibs-i686"
    date: 2001-08-16
    body: "I'm running SuSE 7.1, and installed from their rpms -- but the trouble I was having was really myserious. Everything (*everything*) worked perfectly, except KDevelop, which sporadically crashed, listing \"Bad Drawables\" on the console, and KMail, which simply wouldn't start at all, listing DCOP communication errors from the KUniqueApplication constructor. I use KDE 100% for the slick object-oriented delopment APIs, and I *love* kdevelop as an IDE, so I had to go back to KDE 2.12, where KDevelop & KMail still work. \n\nAny idea why this might happen? I recall, when I upgraded to XFree 4.1 a few months ago, I had to run some SuSE scripts (SuSEconfig) to fix similar problems (\"Bad Drawable\" errors, again) but it doesn't help this time.\n\nIs this all becuase SuSE paps things up with extra complexities that normal mortals cannot comprehend? It seems to me that if the binaries are there and ldconfig has been run, everything should Just Work, but then I'm new to linux, coming from BeOS, where everything really did Just Work."
    author: "Shamyl Zakariya"
  - subject: "Little Quirks"
    date: 2001-08-16
    body: "Fantastic release!!! Much thanks goes out to the amazing talent of the KDE team & community. Everything in the new version looks very complete - the only thing I had to do was generate a new .kde folder, which seemed to speed things up (for some _odd_ reason). Two things that stick out however, are:\n\n1. What happened to the little \"modified\" disk icon in the taskbar? Can't seem to find a setting for it anywhere...\n\n2. Is there a way to shut off the mouseOver effects in the panel and in the file browser that makes the icons slightly shrink and blur (it's not in my taste).\n\nALSO... why did Mandrake move QT to libqt*.rpm?? This breaks several major apps of mine (like theKompany KDEStudio), and it looks like they are going to stick with this (I saw it in Cooker). Oh well :-\\"
    author: "Eron Lloyd"
  - subject: "Ask Mandrake"
    date: 2001-08-16
    body: "They seem to have gone on a library-renaming frenzy in recent months, some of which seems to be aimed at defeating the (rather chaotic) version numbering scheme in RPM.\n\nThe numbering scheme, or Mandrake's reaction to it, would drive me to Debian if many other features of the distro weren't so useful and user-friendly.\n\nI can hand a CD to a complete novice and the odds of them getting it installed well enough to be useful exceed the odds of them doing the same with Windows. The RPM circus is making upgrading a different kettle of fish."
    author: "Leon Brooks"
  - subject: "Re: Ask Mandrake"
    date: 2001-08-16
    body: "Actually, I like what Mandrake is doing with their rpm naming scheme. RPM's should not directly refer to other RPM package names in it's dependencies. Dependencies should only refer to library and file dependencies provided by other RPM's. (Listed by doing a -q --provides).\n\nWhat the new naming scheme does is make it a lot easier to have multiple versions of incompatible libraries at the same time. Suppose I have 2 applications, one needs gtk-1.2 and the other gtk-2.0. If the packages were named gtk-1.2.8 and gtk-2.0.1, I'd have to install them both with -i. Now, suppose I have a gtk-1.2.9 package that is a new version of gtk-1.2. If I try to upgrade with -U, I get problems because of the existing gtk-2.0. However, if the packages are named libgtk1.2-1.2.8 and libgtk2.0-2.0.1, then they are treated like two different packages altogether and keeping both concurrently is a lot easier."
    author: "Loban Rahman"
  - subject: "Re: Little Quirks"
    date: 2001-08-19
    body: ">2. Is there a way to shut off the mouseOver effects in the panel and in the file browser that makes the icons slightly shrink and blur (it's not in my taste).\n\nYes, it's possible to disable the mouseover effect:\nKontrolCenter->Icon->Advanced->disable 'blend alpha channel'\n\nregards"
    author: "thierry"
  - subject: "Whoa!"
    date: 2001-08-16
    body: "It looks GOOD! Installation went smooth as silk (rpm -i *.rpm --nodeps --force). Everything works right from the start! I'm on SuSE 7.1.\n\nGood work KDE-team! KDE 2.2 simply ROCKS!\n\nOf course, there are few things that annoy me (there always is).\n\n1. It nuked my KDM, and I can't change it back. I had nice background in KDM, but now I have dull grey background. Well, I didn't spend much time fighting with this last night, so I might be able to fix it today.\n\n2. When I click the K-menu (start-menu, whatever), and the menu appears. If I then decide to close it, I can't simply move the cursor away from the menu and make it disappear. I have to click the K-button again. Yes, I know that's a minor issue.\n\n3. (this is a REALLY minor issue. But I guess it tells something about the quality of KDE 2.2 if I have to mention things as minor as this)) I use the fading menus setting. But in K-menu it only works for the first few menus. For example, the M-menu itself fades in nicely. If I move to \"multimedia\" for example, the multimedia-menu fades in. But if I then move to some other menu, then don't fade, they just appear there.\n\nThank you for AWESOME desktop! You people are the best there is!"
    author: "Janne O"
  - subject: "Re: Whoa!"
    date: 2001-08-16
    body: "I have the same problem with the submenus of the K-menu on my RH7.1. A quick fix is to disable menu effects from Control Center / Look&Feel / Style. After that all menus go away by clicking on the desktop like they used to do."
    author: "Janne S"
  - subject: "Re: Whoa!"
    date: 2001-08-16
    body: "2) It's a bug of Qt 2.3.1, you may install qt-copy which holds (at the moment until HEAD switchs to Qt 3) a bugfixed Qt 2.3.1 version."
    author: "someone"
  - subject: "Re: Whoa!"
    date: 2001-08-16
    body: "I don't actually think it's a bug. Win2K shows exactly the same fading behaviour. And personally I think it's correct and useful behaviour anyway."
    author: "Jelmer Feenstra"
  - subject: "Re: Whoa!"
    date: 2001-08-16
    body: "Win2K allows you to click somewhere on the desktop to close the menu.\n\nThe problem being refered to here is that this doesn't work with Qt 2.3.1 installed. The ONLY way to close the menu is by clicking the \"K\".\n\nThis is most definately a bug.\n\n--BN!"
    author: "Billy Nopants"
  - subject: "Re: Whoa!"
    date: 2001-08-16
    body: "Well, I tried it just now (with fading menus) and the menu did disappear if I clicked on the desktop. And KDM also worked (well, the fonts were wrong but otherwise it was flawless). \n\nhttp://www.angelfire.com/linux/serenity/kde22.htm\n\nRunning well and looking good :)!"
    author: "Janne"
  - subject: "KDE 2.2 is great but...."
    date: 2001-08-16
    body: "Looks really good but when you upgrade you always notice things that used to work that don't:\n\n Tooltips are displayed completely in black with no text in them whatsoever! I managed to fix this by setting the tooltip effects to none in Control Centre instead of fading. Well fading to black is not exactly a useful option!\n\nAlso I had to disable the menu effects so that when I click elsewhere the menu disappears. Otherwise you can't get rid of the menu!\n\nAnother really bad thing I have noticed is when you are filling out a form in Konqueror the text entry widget is REALLY REALLY slow. I have a 1Ghz processor and 384 Mbs of RAM so I don't understand this. It takes about half a second to type a character. I can type about 3 times faster than it can display and I'm not even a fast typer!\n\nAnother thing I noticed is that the panel doesn't look as cool as it did. Not sure why though.\n\nDon't get me wrong though I love KDE. It's definately the best and far more powerful/usable than any other desktop (especially Windows...)"
    author: "Nicholas Allen"
  - subject: "Re: KDE 2.2 is great but...."
    date: 2001-08-16
    body: "About the menu effect/menu disappear thingy: It's a bug of Qt 2.3.1, you may install qt-copy which holds (at the moment until HEAD switchs to Qt 3) a bugfixed Qt 2.3.1 version."
    author: "someone"
  - subject: "JavaScript problems on Konq2.2"
    date: 2001-08-16
    body: "KDE 2.2 looks great and was easy to install on my Mandrake 8, even though I KDM doesn't work anymore, forcing me to use GDM instead. The real problem though, is that Konqueror no longer works on the Yahoo Mail page, nothing happens when I click on the \"Send\" button after composing an email. That used to work on KDE2.2beta1..."
    author: "Pedro Ziviani"
  - subject: "Re: JavaScript problems on Konq2.2"
    date: 2001-08-16
    body: "Yep!!\n\nI agree with you there.\n\nI install kde2.2 last night (have mandrake 8).\nI thought it might be javascript to. It a bugger, cos ther are a couple of web sites that i go to that use either javavscript or java buttons... ie (www.seek.co.nz).\nI had a couple of dependency problems but nothing to bad.\n\nI hope the developer guys hear about this one.\n(i'll try t send them a bug report now!)\n\nCheers\nCoomsie\n\n\np.s. Just like to say well done to the many developers out ther that helped kde2.2!!!"
    author: "Coomsie"
  - subject: "Re: JavaScript problems on Konq2.2"
    date: 2001-08-18
    body: "also getting the same problem on Yahoo mail.  Didn't see a bug on the kde buglist.  Did anyone file it?"
    author: "mfrnka"
  - subject: "Binairies for Debian Potato ?"
    date: 2001-08-16
    body: "Hi all,\n\nGreat work ! I tested it on SuSE.\n\nBut I am looking for binairies for Debian Potato.\nI know that there won't be an official port of kde-2.2 to Potato, but in my case, I can't use Woody. This is for a server for X terminals which uses KDE. I'd like that users benefits from the new features of kde-2.2, but I need the stability of Potato. Up to now, I use kde.tydc.com or a mirror of it, but there is only kde-2.0 on it.\n\nIs anyone else compiling kde-2.2 for Potato ?\n\nThanks,\n\nYann"
    author: "Yann Forget"
  - subject: "Re: Binairies for Debian Potato ?"
    date: 2001-08-16
    body: "You may want to have a look at this site:\n\nhttp://kde.debian.net/\n\nCiao,\nGordon"
    author: "Gordon Tyler"
  - subject: "Dead Keys?"
    date: 2001-08-16
    body: "Wha happens with dead keys?. They do not work in my computer. Has anybody the same problem? What is the solution?. BTW, my distro is RedHat 7.1.\n\nThanks"
    author: "Juan PC"
  - subject: "Re: Dead Keys?"
    date: 2001-08-16
    body: "Hey! I only noticed it when I read your post. You are right. Dead keys don't work in KDE 2.2 in a RH7.1 box. :("
    author: "Helder Correia"
  - subject: "Re: Dead Keys?"
    date: 2001-08-20
    body: "Neither do they work on Mandrake 8.0 with the KDE 2.2 RPMs from Texstar... :-(\nAlso the prelink thing doesn't speed up anything noticeably. Well, to be fair, konsole starts noticeably faster, but all other apps I use show the same speed (or lack of it :) as always."
    author: "Rui Prior"
  - subject: "Mr"
    date: 2001-08-16
    body: "Hello !\n\nI have tried yesterday to compile kde22 but I have encountered some problems with kdeaddons module. It cannot find some .h et .so files, for example kate/*.h, konq_dir.h etc. I have found these files in kdebase/kate/interfaces/ (for example). Is this a problem of kdebase install ? Should I copy all the missing files in the corresponding KDEDIR/include and KDeDIR/libs (they are not there) ? \n\nAlso , I was astonished to see that there is no startkde script. Is this a problem of compilation (I have compiled and installed everything except kdeaddons) or is linked to the make install of kdebase ? \n\nThanks,\n\tFlorin"
    author: "Florin Cremenescu"
  - subject: "Re: Mr"
    date: 2001-08-16
    body: "Install kdebase first.\nkdeaddons requires headers from kdebase, startkde is part of kdebase."
    author: "bero"
  - subject: "Re: Mr"
    date: 2001-08-16
    body: "I have installed everything except kdeaddons. I shall try to reinstall kdebase, maybe it was an error I haven't noticed. But I doubt. \n\n\tThanks,\n\t\tFlorin"
    author: "Florin Cremenescu"
  - subject: "Re: Mr"
    date: 2001-08-18
    body: "It was my fault. I have configured kdebase  to install in /opt/kde22 and the other packages to install in /opt/KDE22. Everything works fine now. \n\tI am impressed by KDE22 ! \n\n\tThanks guys ! \n\n\tFlorin"
    author: "Florin Cremenescu"
  - subject: "Since lists.kde.org is down..."
    date: 2001-08-16
    body: "I figure I might as well ask here...\n\nFirst off, thanks once again to the KDE team.  Konquerer, especially, is much faster in this release.  The little features (like being able to enable/disable java/javascript/cookies/etc right from the \"tools\" menu really make the difference.\n\nThe new startup notification is great!  I like the look very much.  Everything feels very slick.\n\nHowever, some glitches I'm curious about:\n\n1) KDM -- with this release, KDM isn't working well at all.  If I log in and then log out, the KDM screen will pop back up for about 10 seconds, and then the X-server will RESET again.  VERY irritating!  I'm not convinced this is just KDM though...I think XFree 4.02 has bugs that may be involved in this.\n\n2) Dual-head strangeness...I have a dual-head G400.  I am -not- using xinerama, I just have two displays.  When I log into KDE, my second display goes blank!  This only happens with KDE...my WindowMaker (which is dual-head aware as well) works fine with the second display.\n\n3) KDE sound startup -- when I log in, a dialog pops up claiming /dev/dsp is in use...however, arts has no problem starting and playing sounds.  I am using ALSA 0.5X.  Sounds still work, it just seems like a spurious error message.\n4) Java in Konqueror -- I haven't been able to get this working since KDE 2.0.  It always claims that the java executable cannot be found, even if I explicitly set the path to the java executable.\n\n5) Misc build issues--certain packages in the kdesdk tarball just won't build.  The conduits in the kdepim package also will not compile without hacking the source a bit (GCC 2.95.X).\n\nAnyway, kudos to the KDE team...keep up the great work, as usual!"
    author: "Frobozz"
  - subject: "Java + Konqueror (howto)"
    date: 2001-08-16
    body: "Here is the installation script I created for JDK 1.3.1, you can replace that with JRE1.3.1\n\nThe most important line is:\n---\nln -sf /usr/java/jdk1.3.1/jre/plugin/i386/ns4/javaplugin.so  /usr/lib/netscape/plugins/\n---\n\nPlease read this \"install\" shell script file (very simple one)\n\nGet jsse1.0.2 from sun's website, the link is given in Konqueror+Java howto at http://www.konqueror.org.\n\nProblem: I can get most java applet working with konqueror except http://chat.yahoo.com/ (chatting at yahoo) though I can play chess at games.yahoo.com :) Any help?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Java + Konqueror (howto)"
    date: 2001-08-17
    body: "Here's what I did to get Java working.\n\njdk is installed in /opt/jdk1.3.1\n\nso that my java directories are always the same, I created the following symbolic link:\n\nln -s /opt/jdk1.3.1 /opt/j2sdk\n\nI then created a symbolic link to the executable:\n\nln -s /opt/j2sdk/bin/java /usr/bin/java\n\nAfter this, just tell Konqueror to use \"java\" without any path info. It works for me."
    author: "Horst Wetjen"
  - subject: "KDE 2.2 Texstar's objprelink version now available"
    date: 2001-08-16
    body: "hey,\n\ngo to www.pclinuxonline.com to download \nTexstar's objprelink version of kde 2.2. People are reporting that it is much faster than the ones compiled by the kde team.\n\n\nhttp://www.pclinuxonline.com/\n\n--M. Khawar Zia\nquitedown@hotmail.com"
    author: "M. Khawar Zia"
  - subject: "Re: KDE 2.2 Texstar's objprelink version now available"
    date: 2001-08-16
    body: "\"Texstar's objprelink version of kde 2.2. People are reporting that it is much faster than the ones compiled by the kde team.\"\n\nWe do not compile them -- distributors do. SuSE did it and they marked it as experimatial for a reason! Enjob the speedup but please:\n\nDo _not_ report any crashes with objprelink'ed versions of KDE to bugs.kde.org (as already revised in the SuSE README)!\n\nCheers,\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: KDE 2.2 Texstar's objprelink version now available"
    date: 2001-08-27
    body: "I downloaded them and installed them and I have to admit that is it faster than 2.1 but not 30% or 40%. Anyway I am very pleased since many new things are supported but I have to complain about the qrouping in the taskbar which is not as clever as in Win XP. For example: Grouping in KDE is done not matter how many windows you have open, so even if you have only Konqueror open with 5 windows, you only have one in taskbar making it difficult to change among them, while in Win XP grouping is done only when there is no available space in the taskbar."
    author: "George Billios"
  - subject: "Little quirks"
    date: 2001-08-16
    body: "I really have to say this new one is a VERY beautiful desktop.\n\nIt has, however, a couple of quirks. Some annoying, some less annoying. I downloaded this release from the SuSE 7.2 directory on sourceforge.net. Installed it with rpm -Uvh --nodeps --force *.rpm. Also deleted .kde and .kde2 directories from my homedir.\n\n1) After installation kdm doesn't work. I solved this by a symlink from /etc/X11/xdm to /opt/..../kdm\n2) Icons seem to pop back to top-left constantly. They won't stay where i put them.\n3) Maximised windows don't align with the screen. Especially with the old Netscape this is very annoying.\n\nJust a few little things. Other than that it's really pretty."
    author: "Cihl"
  - subject: "Re: Little quirks"
    date: 2001-08-16
    body: "> 2) Icons seem to pop back to top-left constantly. They won't stay where i put them.\n\nYep. I found this as well - it's a known bug (I looked on the dev mailing lists). My solution was to move my $HOME/Desktop file to $HOME/Desktop.old and restart KDE. The default (at least under RH7.1) icons seem to not have this annoying behaviour, and copying back the Icons from Desktop.old with konquerer one by one I was able to get back to a desktop layout like I had in 2.1.1. Took me 5 minutes and now KDE 2.2 is useable for me.\n\nAnnoying bug  - I'm used to this with KDE though - it seems that every major release breaks some configuration files somehow."
    author: "Dr_LHA"
  - subject: "Re: Little quirks"
    date: 2001-08-17
    body: "2. find and remove sockets named kdesktop* and owned by you in /tmp directory"
    author: "simon"
  - subject: "Can't define remote LPD queue under Debian?"
    date: 2001-08-16
    body: "After I added kdelib3-cups to my Woody/Sid box, I was able to get KDE 2.2 to see CUPS as one of the underlying printing engines. But I am now facing a new problem: when I try to define a remote LPD printer the control centre segfaults. Every time. And only for remote LPD queues -- it will work with everything else. It dies when I click on \"next\" after typing in the IP address & the print queue.\n\nMy printer is attached to my DSL gateway/firewall box that serves also print-server duties. It's running a two-floppy hybrid of Coyote Linux & Charles S.'s EigerStein LRP Linux that I haphazardly (sp?) cobbled up.  Everything works fine with Window$, MacOS and did previously with Mandrake 8/7.2 and Caldera (2.3, 2.4, 3.1).\n\nDid anyone else encounter that? I tried upgrading libc6 et al from Sid, to no avail: I still get segfaults on that.\n\nFWIW, here's a copy of one of the tracebacks:\n\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...0x40d72479 in wait4 () from /lib/libc.so.6\n#0  0x40d72479 in wait4 () from /lib/libc.so.6\n#1  0x40de9024 in __check_rhosts_file () from /lib/libc.so.6\n#2  0x40599bf5 in KCrash::defaultCrashHandler () from /usr/lib/libkdecore.so.3\n#3  0x40cfb6b8 in sigaction () from /lib/libc.so.6\nCannot access memory at address 0x69727020.\n\n\n...and here's the tail-end of another one, just in case:\n\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...0x40d709b9 in wait4 () from /lib/libc.so.6\n#0  0x40d709b9 in wait4 () from /lib/libc.so.6\n#1  0x40de6368 in __check_rhosts_file () from /lib/libc.so.6\n#2  0x40599bf5 in KCrash::defaultCrashHandler () from /usr/lib/libkdecore.so.3\n#3  0x40cfa318 in sigaction () from /lib/libc.so.6\nCannot access memory at address 0x6e69740a."
    author: "bruno majewski"
  - subject: "Troubles on Red Hat 6.2"
    date: 2001-08-16
    body: "It seems I have some terrible configuration problem. Konqueror crashes on almost any site. Even on slashdot , even though I am sure Konq was tested on it :).  It says when it dies:\n\nkdecore(KLibLoader): WARNING: library=kjs_html: file=/opt/kde2.2/lib/kde2/kjshtml.la :/opt/kde2.2/lib/libkjs.so.1 :undefined symbol : _Q23KJS9ObjectImp.Info\nKCrash: crashing ...\n\nIs libkjs.so.1 somehow related to javascript ? \nAlso, it takes forever for Noatun to load a short list of names of mp3 from the directory. Everything was compiled from sources on patched RH6.2 with gcc2.95.2, qt2.3.1 with openssl-0.9.6b, lesstif-0.93.0 and  pcre-3.4 added. \nAnyone run into anything similar ? Any ideas ?"
    author: "belochitski"
  - subject: "problem solved"
    date: 2001-08-17
    body: "it turned out that libpcre (which has to do with regular expressions parsing in kde's javascript implementation) by deafault creates and installes itself into /usr/local/lib  which was not present in /etc/ld.so.conf for some reason. thanks everyone."
    author: "belochitski"
  - subject: "Konqueror RMB URL paste"
    date: 2001-08-16
    body: "Hmm... I have been hoping to see some added flexibility in the KHTML component regarding middle mouse button-pasted URLs.  For example selecting a whole row in the terminal window by triple clicking will include a trailing newline character that confuses KHTML and the URL will not open properly when pasted to a Konqueror window...  =(\n\nIf possbile without serious side-effects, it would be most useful to see a \"guessing\" algorithm for these pasted URLs as well, for example \"www.foo.com\" without the protocol prefix would ideally be assumed to be \"http://www.foo.com/\" and so on.\n\nMaybe in the next versions... =8-)  Keep up the excellent work!"
    author: "Kimmo"
  - subject: "Gran Trabajo (Great Work!!!!)"
    date: 2001-08-17
    body: "Hola!!!\n\nMi pc es una pentium200 y con KDE va bien, estuve casi una semana compilando KDE 2.2beta1 y cuando lo observ\u00e9 parecia que estaba so\u00f1ando, desde el principio cuando salio el configurador de kde quede perplejo de tal kalidad grafika (K!!!) Micro$oft Window$ XP queda en la basura (creo que es mucho para el ) una sugerencia pongan los RPMS de RedHat (rpm4 y 3)\n\nBye..."
    author: "Funny Tux"
  - subject: "Problem with the RPMs for SuSE 6.4"
    date: 2001-08-17
    body: "I just installed the RPMs for SuSE 6.4. Upon trying to launch KDE 2.2 via the new KDM I just find /var/log/messages complaining about not being able to execute /usr/X11R6/lib/xdm/Xstartup and ../Xsetup.\nAnd indeed, my installation of SuSE 6.4 (which I deem to be pretty much standard) misses these scripts.\nSwitching back to KDE 2.1.0 and it's KDM I find myself even unable to start fvwm2, at least as a non-root user.\nI can hardly imagine that a distro of KDE 2.2 explicitly packaged for SuSE 6.4 should make false assumptions about the underlying OS. So what did I do wrong? Oh yes, I started SuSEConfig, but I think from this day on I will refrain from doing so.\nAny help would be greatly appreciated. Thanks in advance,\n\nOlaf"
    author: "Olaf Bergner"
  - subject: "objpreelink with FreeBSD"
    date: 2001-08-17
    body: "Does anybody know of someone, or has anyone tried the opjprelink with FreeBSD, or where I can find information about it working or not.\n\nThanks\n\n--Reggie"
    author: "Reggie"
  - subject: "Just got it installed. Nice job, guys."
    date: 2001-08-17
    body: "I just installed KDE 2.2. It was virtually a no-brainer from RPMS on RedHat 7.1. I'm impressed.\n\nThank you all for your hard work. KDE is a fine environment. Stable, clean, fast, pretty. I think I could even get my wife to use it ;-)\n\nThis is enough to make an old C hack like me want to contribute to KDE 2.3..."
    author: "Tom Vilot"
  - subject: "Re: Just got it installed. Nice job, guys."
    date: 2001-08-17
    body: "Try KDE 2.2.1 or KDE 3.0.  There will be no 2.3."
    author: "Anony-Moose"
  - subject: "Trouble with kded, konqueror"
    date: 2001-08-17
    body: "First let me say that I really appreciate all the hard work and quality of the KDE programmers, KDE2.2 is very impressive. \n\nI'm having trouble with konqueror, sometimes it won't start. If i kill kded then it starts...Restarting kded gives a fatal DCOP communication problem. I had the problem with mandrake 8 and rpms, so I installed from source and the problem is still there.\nAll source components installed except kdeadmin, gives an error can't find lbz2, but that component doesn't look essential (?)\nAndy ideas ??\n\nthanks Andy"
    author: "Andy Rayner"
  - subject: "Re: Trouble with kded, konqueror"
    date: 2001-08-17
    body: "I was unable to get Konqueror to run also, with a kded error on the console.  I was working with a clean 7.1 RedHat install.  I had not yet loaded kdegraphics or kedutils due to a couple failed dependencies (libsane.so.1, zip, unzip, efax).  After I loaded those missing RPM's, and loaded kdegraphics and kdeutils, konqueror started working correctly.\n\nHope this helps!\n\nForgiveR"
    author: "ForgiveR"
  - subject: "Re: Trouble with kded, konqueror"
    date: 2001-08-18
    body: "I had the same problem *after* installing all the RPM packages and satisfy the dependencies in RH 7.1. This is a major showstopper :("
    author: "Eduardo Sanchez"
  - subject: "Re: Trouble with kded, konqueror"
    date: 2001-08-19
    body: "I also can not open konqueror.  The konqueror loading icon disappears from the taskbar after 30 seconds (as per user prefs) and it never loads when run from the command line (it just keeps loading indefinitely).\n\nThis is a shame, since I had just started using konqueror as my default browser. :-(\n\nAs previously mentioned, killing kded seems to allow konqueror to start, but I do not receive kded-related errors so thanks for mentioning the workaround.  Any ideas for a more permanent solution?  What does kded do?\n\nAlso, my thanks to all the KDE developers, things are looking really good."
    author: "Michael Wardle"
  - subject: "Re: Trouble with kded, konqueror"
    date: 2001-08-20
    body: "I found out that kded now uses FAM, and I noticed\nsome FAM error messages in my system log file\nsuch as the following:\n\n-----\nfam[1518]: Client \"client 10\", whose requests are being serviced as uid 99, was denied access on /home/michael/.kde/share.  If this client might be running as uid 99 because it failed authentication, you might disable authentication.  See the fam(1M) man page.\n-----\n\nUpgrading to the latest FAM Red Hat Raw Hide\n (Rawhide?) package (fam-2.6.4-10) seems to have\nfixed the problem.\n\nIf you don't want to install the Red Hat package,\nensure that you have a working FAM installation.\nIn many cases, you should be able to fix the\nproblem by changing \"rpc_version = 1-2\" to \"rpc_version = 2\" in /etc/xinetd.d/sgi_fam.\n\nLet me know how you go."
    author: "Michael Wardle"
  - subject: "Re: Trouble with kded, konqueror"
    date: 2001-08-20
    body: "It worked for me. \n\nI use Red Hat 7.1 on an i386.\n\nKonqueror didn't startup the first time I logged in. But when I pressed CTRL-ALT-BACKSPACE and logged in again konqueror did launch. Konqueror also started when I killed kded.\n\nAnyway changing rpc_version to 2 solved the problem on my machine.\n\nThnx!"
    author: "Gert-Jan van der Heiden"
  - subject: "True Type Fonts"
    date: 2001-08-17
    body: "I did an apt-get install and installed KDE (debian is sweet) but my true type fonts don't show up as an option in kde,  they work fine in Mozilla, and in all other X apps.  Did I miss something??"
    author: "kujo"
  - subject: "Re: True Type Fonts"
    date: 2001-08-19
    body: "edit your XftConfig, kde/qt when antialiased, use freetype/xrender to draw fonts"
    author: "aaron leahman"
  - subject: "Re: True Type Fonts"
    date: 2001-08-20
    body: "True Types work in KDE now!!  Thanks very much, it was something so simple yet I would never have known.  \n\nThanks a lot."
    author: "Kujo"
  - subject: "Red Hat 7.1 RPM install order (sample)"
    date: 2001-08-17
    body: "Thanks to KDE team, nice work!\n\n  here is a sample order of rpm install I used in Red Hat 7.1:\n\nqt-2.3.1-3.i386.rpm\nfam-2.6.4-9.i386.rpm\nlibxml2-2.4.1-2.i386.rpm\nlibxslt-1.0.1-3.i386.rpm\nkdesdk-2.2-1.i386.rpm\npcre-3.4-2.i386.rpm (--force,--nodeps)\nkdelibs-2.2-5.i386.rpm (--force,--nodeps)\nlibogg-1.0rc2-1.i386.rpm\nlibvorbis-1.0rc2-1.i386.rpm\nlibao-0.8.0-1.i386.rpm\npopt-1.6.3-0.91.i386.rpm\nnscd-2.2.4-5.i386.rpm\nlibmng-1.0.2-1.i386.rpm\nopenssl096-0.9.6-6.i386.rpm\narts-2.2-5.i386.rpm (--nodeps)\nkdelibs-sound-2.2-5.i386.rpm\nopenssl-0.9.6b-4.i3\nkdepim-2.2-1.i386.rpm86.rpm\nqt-Xt-2.3.1-3.i386.rpm\nkrb5-libs-1.2.2-12.i386.rpm (--force,--nodeps)\nkdebase-2.2-3.i386.rpm\nkdegraphics-2.2-1.i386.rpm\nkdenetwork-2.2-3.i386.rpm\nkdeutils-2.2-2.i386.rpm\nkdeartwork-2.2-1.i386.rpm\nkdeaddons-konqueror-2.2-2.i386.rpm\nSDL-1.2.2-2.i386.rpm\nxinetd-2.3.0-6.i386.rpm\nkdeaddons-kicker-2.2-2.i386.rpm\nkdeaddons-knewsticker-2.2-2.i386.rpm\n\n---\nbest,\n        Yuri"
    author: "Yuri Gotra"
  - subject: "arts,https,pass fail,konq term emulation,dhtml,pdf"
    date: 2001-08-18
    body: "Hello !\n\n\tI have installed KDE2.2 (great!) but I still have some problems. \n\n\t1. Sound : the sound server doesn't work fine. When KDE starts, during the 'Loading Window Manager' phase, it pauses some seconds and then it pops up a message , 'Sound server fatal error, cpu overload, aborting'. After that I have a crash of Knotify (if useful, I can send you the stack trace). Noatun doesn't play , it tries to launch the sound server, doesn't succed and crashes. \n\tI have compiled KDE with support for oss and nas, without alsa (maybe a mistake ?). The sound is compiled into the kernel 2.4.5 (it is not a module). \n\tWhat's strange that all non-kde sound applications perform correctly : gqmpeg, aviplay, realplay...\n\t\n\t2. konqueror https does not work. I have compiled kde with openssl 0.9.6 support, I can see all the modules listed in Settings/SSL, but in Setting/OpenSSL it asks me for the path to open ssl (shared library). I have only the .a version of libssl. I have tried to convert it to .so (gcc -shared libssl.a -o libssl.so) but this solution doesn't work. Should I recompile to obtain a .so version ? When connection to a secure server, konqueror tells me that it cannot connect to the proxy server. Netscape works fine. \n\n\t3. When I lock the screen , I cannot reenter anymore. It tells me 'Password fail' (or something like that), and I have to kill the X server. I am sure that I am typing correctly. \n\n\t4. Konqueror terminal emulation doesn't display correctly (in fact does not clear correctly). Any ideas (its a great add-on and I would like to use it). The same is true for the kdevelop konsole. \n\t\n\t5. Does konqueror support dynamic html ? Nothing works at www.dynamicdrive.com . Is it a problem of identification or dhtml is not implemented ? \n\n\t6. PS/PDF viewer doesn't work. When I try to open a file (from konqueror or from File/Open) it tells me : 'Error openning file /path/to/file/file.pdf). No such file or directory. Success'. Acroread has no problem reading it. \n\n\t7. ViewFilter refuses to appear in konqueror main toolbar. \n\n\t8. Copy/Paste does not work with jbuilder4.0. Also if numlock is on, double click doesn't work anymore inside jbuilder. If I put it off, everything goes fine. I had the same problem with KDE2.1.1\n\n\t9. When I execute a  program from konqueror using CTRL+E (Tools/Execute shell command), if it has a large output, finally it will crash. For example, try 'make'. Also the scrollbar remains up during command execution, and I cannot see the last output. \n\n\t10. And, finally, is there a possibility to set a proxy for kdict ? \n\n\tI think that's all for the moment. Beside these (small) problems, I tell you that I am impressed by the great leap forward from KDE2.1.1 to KDE2.2 Just waiting for KDE3\n\n\tThanks,\n\t\tFlorin"
    author: "Florin Cremenescu"
  - subject: "did you remember to add"
    date: 2001-08-18
    body: "did you remember to add paths to openssl directory and kdelibs directory to /etc/ld.so.conf and run /sbin/ldconfig then ? openssl installs itself into /usr/local/ssl. so , you'll have to have something like \n/usr/local/ssl/lib\n/opt/kde2.2/lib\n/usr/local/lib\nin your ld.so.conf"
    author: "belochitski"
  - subject: "Re: did you remember to add"
    date: 2001-08-19
    body: "Indeed, I have forgotten to add /usr/local/ssl/lib. Now, when I click on the button Test in Settings/Crypto/OpenSsl, it tells me 'OpenSsl loaded successfully'. But this change nothing : I cannot connect to a secure site. Have you been successful in installing SSL with KDE22 ? \n\tThanks,\n\t\tFlorin"
    author: "Florin Cremenescu"
  - subject: "yeah, it works for me"
    date: 2001-08-20
    body: "you should probably provide more information on error messages you're getting. try switching to first virtual console and see what konqueror says after it couldn't connect to secure site."
    author: "belochitski"
  - subject: "Re: yeah, it works for me"
    date: 2001-08-20
    body: "I got the error : \n\nkdeinit: Got EXEC_NEW 'kio_http' from launcher.\nkio (KLauncher): kio_http (pid 1822) up and running.\nkdecore (KLibLoader): Loading the next library global with flag 257.\nkdecore (KLibLoader): WARNING: KLibrary: /usr/local/ssl/libcrypto.so.0: undefined symbol: X509_free\nkdecore (KLibLoader): WARNING: KLibrary: /usr/local/ssl/libcrypto.so.0: undefined symbol: RAND_egd\nkdecore (KLibLoader): WARNING: KLibrary: /usr/local/ssl/libcrypto.so.0: undefined symbol: CRYPTO_free\nkdecore (KLibLoader): WARNING: KLibrary: /usr/local/ssl/libcrypto.so.0: undefined symbol: d2i_X509\nkdecore (KLibLoader): WARNING: KLibrary: /usr/local/ssl/libcrypto.so.0: undefined symbol: i2d_X509\n\n\n\nI think there are some problems with libcrypto and libssl. I obtained them from the .a version : \n\tgcc -shared libcrypto.a -o libcrypto.so\n\nWhere can I find a tar.gz with sources which compiles directly to .so ? \n\nThanks,\n\tFlorin"
    author: "Florin Cremenescu"
  - subject: "um, you should probably try recompiling openssl"
    date: 2001-08-21
    body: "afer runninig its confugure script with 'shared' option:\nconfig shared\nYou may want to look into INSTALL file for details.\nAfter that you'll have .so's in you openssl dir."
    author: "belochitski"
  - subject: "Re: yeah, it works for me"
    date: 2001-08-23
    body: "Have you put your openssl libs directory in LD_LIBRARY_PATH or in /etc/ld.so.conf ?"
    author: "2WhyNo"
  - subject: "Re: yeah, it works for me"
    date: 2001-08-23
    body: "Yes, I have put them. \n\tI put as attachment the output of kfmclient when I try to connect to a secure site. Maybe someone has an idea...\n\n\tThanks,\n\t\tFlorin"
    author: "Florin Cremenescu"
  - subject: "Same arts problem"
    date: 2001-08-20
    body: "Hi!\n\nI've got exactly the same bug using aRts on a Mdk 8.0 and a mainboard-integrated sound card. It's REALLY annoying because I can't listen to music within KDE apps whereas I can use mpg123 without any problem.\n\nAnother problem I have is that kdm doesn't work anymore. It doesn\"t start, telling me that it idled and will start again in 5 minutes (or something like that... sorry).\n\nFor the terminal problem, I had the same but I managed to fix it. In fact, you have to change the font. I changed all the fonts to TTF fonts and now, I don't have any problem with them.\n\nBy the way : is there a reason why no TTF font is provided with KDE ? why not including Verdana,Times New Roman, Adobe Helvetica and so on without having to install them from a windows partition ? I think it's more than a little request: fonts are a big weakness of linux now while it should not be."
    author: "julo"
  - subject: "Crash addresses on konqueror"
    date: 2001-08-18
    body: "Some urls which do not work well with konqueror : \n\n\t1. news.bbc.co.uk . Normally, this site shows the latest news in the up left corner of the browser. Konqueror shows only the word 'Latest'. \n\n\t2. http://news.cnet.com/news/0-1003-200-341072.html?tag=rltdnws\n\nThis address blocks in nsplugin. Oh, no !  \n\n\t3. This address do not allow me to continue after completing the form. \nhttp://www6.software.ibm.com/dl/dklx130/dklx130-p"
    author: "Florin Cremenescu"
  - subject: "Kate, Konqueror, Kicker, KNewsTicker, KSirtet..."
    date: 2001-08-18
    body: "One Katastroph :\n- Kate was an editor I was waiting for. Alas, it is bugged :\n -- A very irritating and ridiculous bug : Task bar always appears on top, with text. It is impossible to keep it in vertical and with icons only\n -- A big bug caused I lost a big file... It's easy to reproduce (perhaps there is a simpler way) :\n  --- put on the destop a copy of a big file\n  --- modify it with Kate and save it\n  --- exit Kate\n  --- rename the file\n  --- open the file in Kate : there is only 4 ou 5 lines, all the text is lost\n OK, it was not easy to find, because the trouble exists only when the file is on the desktop... But many users like to have some important text files directly on the desktop. It is the worse bug I have never seen in KDE...\n\nTroubles that I succeed to fix :\n- bad fonts, in menus, Konqui... 3 times I have to update font parameters... Now it seems stable...\n- lost favicons. Now good, after removing  $HOME/.kde/share/apps/konqueror/faviconrc\n- all icons of destop aligned on the left side\n\nTroubles I didn't succed to fix :\n- applets cannot stay on Kicker when the bar is vertical, so I put it horizontal\n- KNewsTicker don't launch Konqueror, so I now not use this program\n- Quanta + crashes when exiting (and it is impossible to save the taskbars configuration - in 2.1.1 too)\n\nOld troubles always present in the new version :\n- After installation, some Konqueror functionnalities are unavalable (some ones would say it is normal, I feel abnormal that only a minority of end users can implement these functions, Konqueror is for anybody, no ?)\n -- Flash (I spent many time to fix it in the old version, and now I have to do it again...)\n -- Java\n -- Local Network\n- in Konqueror it is impossible to display (in an editor) the source of a page, when this page is in a frame (with mouse right button) (excepted by doing many clicks with \"Open with...\")\n\nOld troubles now fixed :\n- in Konqui, it is now possible to put the bookmark toolbar beside the URL toolbar (only if the bookmark toolbar is narrow...)\n- In Kicker, child panels now are well memorized\n\nVery good stuff (for my use, of course) :\n- Launcher and taskbar applets in Kicker\n- \"Stop animation\" in Konqui (I hope a \"Stop sound\" or \"Never sound\" in the next version...)\n- CD-Audio services in Konqueror\n- Print preview and PDF printer\n\nGood stuff :\n- extended bar in Konqui\n- better display, better speed in Khtml (but still <nobr> don't work...)\n- Automatic Preview in \"Open file\" windows\n- View filter in Konqui\n- many good little improvements, like the \"initial level\" in KSirtet. I enjoy when I feel that developpers like to regularly improve their program...\n\nSome things I still don't understand :\n- KDict\n- PIM\n\nAnd, of course, some improvements I don't use (for example group similar tasks in toolbar, I need to see the favicons ;-)... And the virtual desktops minimize the number of tasks per desktop...)\n\nI hope, now, that it is finished for bad things and that I have many new good ones to discover... Happily I have not the troubles of Florin (sound, PDF....).\n\nI am not glad, but I am happy to see it's going in a good direction of consistent improvements. Congratulations !"
    author: "Alain"
  - subject: "Big bug !!?"
    date: 2001-08-20
    body: "Waaahh... I now loose a second important file !!!\nIt is not Kate, as I thought, now it is with Kedit ! And the bug is on the desktop.\n\nMay anybody reproduce it, to know if it is specific of my installation :\n- create on the destop a file Text1.txt, containing the line \"This is my text\". Save and exit the editor.\n- on the desktop change the name Text1.txt to Text2.txt\n- See the content of Text2.txt, now it is :\n[Desktop Entry]\nName[fr]=Text2.txt\n\nSo the bug is in the Desktop renomination ! \nTwo important texts file removed, really a Katastroph... Hope it is specific for my installation. Hope there is something to do for quickly correct such a thing..."
    author: "Alain"
  - subject: "Re: Big bug !!?"
    date: 2001-08-22
    body: "wow, indeed, that's a bad bug :/ I'm sorry that your files got trashed. This will certainly get fixed for 2.2.1."
    author: "Carsten Pfeiffer"
  - subject: "Re: Big bug !!?"
    date: 2001-08-22
    body: "Ah, I am not alone...\n\nYes, what a dangerous bug. If possible, it would be interesting to have a patch...\n\nHappily I have find a backup of my second file.\n\nSo I don't post a bug report... But I have others ;-), often little things... However the last is very surprising : Ctrl C - Ctrl X - Ctrl V don't work in Kate... (Kate is still almost a great editor !...)\n\nAlso it would be fine that in all KDE programs the key F3 does \"Find next\"... (for me, it disturbs in Kate and Konqui...) (and F2 = Begin in all KDEgames...)"
    author: "Alain"
  - subject: "Re: Big bug !!?"
    date: 2001-08-24
    body: "> Yes, what a dangerous bug. If possible, it would be interesting to have a patch...\n\nThanks to Kurt, here's a patch.\n\n> Ctrl C - Ctrl X - Ctrl V don't work in Kate...\n\nHm? Certainly works for me."
    author: "Carsten Pfeiffer"
  - subject: "Re: Kate, Konqueror, Kicker, KNewsTicker, KSirtet..."
    date: 2001-08-22
    body: "3 days later :\n\n- the \"Katastroph\" : the bug seems only for me, so I can't call this a bug (?)...\n\n- Konqueror is OK for Flash (without doing something...)\n\n- Often, Kmail is unable to send/receive mail. I must close and open it...\n\n- I do not reach to replace Kmid (with error) by Kmidi (OK) when clicking on a .mid in Konqueror...\n\n- I said : \"in Konqueror it is impossible to display (in an editor) the source of a page, when this page is in a frame (with mouse right button) (excepted by doing many clicks with \"Open with...\")\"\nIt is good by using \"Display the code Html\". The old bug is fixed (it was the temporary file which was opened). Very good for me !\n\n- Kate : the problem of Taskbar icons and position exists in several programs (Quanta Plus, Hexexedit...). I feel that such a thing is a very annoyant bug. I hope it will be updated with KDE 2.2.1...\n\nWell, so I will use Kate (although, too, it is not translated in french). I will manage my Web pages with Kate for small updates and Quanta Plus for big updates..."
    author: "Alain"
  - subject: "cada vez mejor pero mas dificil de instalar"
    date: 2001-08-19
    body: "Cada vez que sale una version de kde ya sea alpha, beta o estable voy corriendo al ftp y me la bajo para probarla pero es que cada vez se me hace mas dificil la instalacion.Con la beta1 ya tuve algunos problemillas y ahora con la 2.2 definitiva ni te cuento.No hay problema, despues de forzar un par de paquetes y no satisfacer un par de dependencias ya ta instalada, claro esta q hay algunas cosas que no funcionan correctamente.\n A pesar de todo es el escritorio que mas me gusta y que utilizo normalmente.\n\nBuen trabajo"
    author: "buba"
  - subject: "KDE Install-- Font Issue"
    date: 2001-08-19
    body: "I installed KDE 2.2 on SuSE 7.2; however, now I have only one choice when I try to select different fonts.  Noteworthy, when I log in as root I do not have this problem.\n\nHow can I fix this issue?\n\nThanks"
    author: "Plato"
  - subject: "Re: KDE Install-- Font Issue"
    date: 2001-08-19
    body: "I am haveing the same trouble as either root or plain user. Here is a detailed desciption:\n\nJust installed KDE 2.2 on my Suse 7.2 system. Everything was fine till I logged out of KDE and back in. Then all my fonts defaulted to E Serif font. This is the only font that shows when I try to change the fonts. After reading some messages I decided to delete the .kde2 directory from my user (root) directory. All was fine, except that I had to run through the options again for initial kde setup. Many fonts showed up in the font setup menu. Then after logging out. The font is back to E Serif again. No other fonts is listed. \n\nAt first I was not sure if a file or directory in my .kde2 was causing the problem. So tried coping things back only to find the font messed up on the next log in. So just did a fresh configure after deleting the .kde2 directory. Logged out and back in and poof! Screwed up font again! Also there seems to be something that is taking a long time to load in the background as the mouse has a light bulb that is pulsating by it for a about 30 seconds and then disappears. My Suse has a little console window that has messages. It was saying something about: \n\nConsole log for linux\nAug 18 20:05:24 linux modprobe: modprobe: Can't locate module char-major-116\nAug 18 20:05:25 linux last message repeated 15 times\n\nNo sure if this is related to the problem.\n\nCan anyone tell me how to fix this?\n\nor\n\nLet me know how to debug (as simple as possible) this to find out what might be missing or added in to cause this?\n\nThanks!\n\nScott"
    author: "Scott"
  - subject: "Re: KDE Install-- Font Issue"
    date: 2001-08-20
    body: "hmh, it looks like that you have enabled\nfont anti-aliasing. (\"E Serif\" is a true\ntype font)\n\n1.) look in the control panel, section \"style\" (i think thats the name in english)\nand disable font anti-aliasing, restart kde\nand everything should be fine.\n(font anti-aliasing needs true type fonts, a\nstandart install has only one, so thats\nwhy you only see one font)\n\n2.) you can also download truetype fonts,\nsuse 7.2 has a script called \"fetchmsttfonts\"\nwich downloads the fonts.\n\n\nand you can ignore the:\n> Aug 18 20:05:24 linux modprobe: modprobe: Can't locate module char-major-116\n> Aug 18 20:05:25 linux last message repeated 15 \nthis has nothing to do with kde, this only\nsays that modprobe can't findt a module for\nsound, as long as your soundcard works fine\nthis can be ignored.\n\nsimon."
    author: "simon"
  - subject: "Re: KDE Install-- Font Issue"
    date: 2001-08-20
    body: "Thanks, I appreciate the help.  In addition, my sound card does not work.  Would you know how to fix this issue?  Thanks Very Very Much, Rob"
    author: "Plato"
  - subject: "Re: KDE Install-- Font Issue"
    date: 2001-08-22
    body: "the easy way to get a soundcard to work\nunder suse 7.2 is to use yast 2,\nyast 2 will setup the soundcard using\nthe alsa soundsystem, if yast 2 is unable\nto get the soundcard to work, have a look at:\n\nhttp://www.alsa-project.org/~goemon/\n\nthis is a list of soundcards that are\ncurrently supported by alsa.\n\nsimon."
    author: "simon"
  - subject: "Font/ Configuration Issue"
    date: 2001-08-19
    body: "Your problem sounds like my problem.  Because root has access to all the fonts, but no other users do-- I think that something is messed up with the file permissions.\n\nthanks,"
    author: "Plato"
  - subject: "All great except Arts"
    date: 2001-08-19
    body: "It's been more than a month since my NT is broken, but still I don't miss it. \nAnd now that KDE 2.2 is out, I'm thinking of deleting my NTFS.\nI compiled it from soure tarballs and got many features I need: https, netscape plugin, cookie management, java applet, unicode (in konqueror).\nFont AA with xfree 4.1.0 works fine.\nKonsole+zsh made me rarely use konqueror as file manager.\nFor this time I'll stick with gvim than kate, it got better syntax highlighting, opens gzipped and bzipped files transparently (Or maybe I can do that too with kate ?)\nBut I think I end up with broken arts, each time arts apps executed, it always ends up with SIGABRT and this:\nanyref.cc:49: void ::Arts::AnyRefHelperStartup::startup(): Assertion `anyRefHelper == 0' failed.\nI even chmod 000 knotify (I've turned off system notification but still knotify is launched), the same with noatun.\nAnd, I compiled kdelibs with alsa support, but I don't see alsa option in sound server setting. It's the same when I compiled with gcc 2.96 or egcs 1.1.2+objprelink.\nAny suggestions ?\nIt doesn't bother me though, I can still use xmms, timidity, realplayer and kwintv as long as it doesn't use arts."
    author: "2WhyNo"
  - subject: "Screenshots (no, not a request)"
    date: 2001-08-20
    body: "One thing that has been getting on my nerves for a while is the need to go to other sites to see nice screenshots showing off KDE.  Why is it the screenshots section of the KDE site are always out of date?  You should post a new set of pictures for each release, then link to old pictures.  Otherwise, people coming to the site having never used KDE are seeing outdated pictures lacking many of the newer features, and that's never good."
    author: "dingodonkey"
  - subject: "RedHat 7.1 RPM Hell"
    date: 2001-08-21
    body: "Now, first, yes, all you Debian users are superior (make you all feel better now? hehe). But look, after scouring through many of the posts on this topic, I must say 2 things:\n\n1) why, oh why were the RPM's for RedHat 7.x built under rawhide? It makes no sense. How many of the average users who would want KDE 2.2 are bound to be using a beta release of RedHat Linux? Not many. And certainly not me. I like my server to be as stable as possible thank you very much. But I do expect one thing from RedHat (whom yes, I actually do go out and buy CD's from, well, did for 6.1 and 7.1)... please continue to support the current non-beta distrib, and don't focus all of your energy/manpower on the upcoming, not-yet-stable one. Argh!\n\n2) while I realize that it's definately not up to the KDE folks to package their desktop for all the distribs... it does reflect very poorly on 'em when distribs have lousy packages that don't install. While some slick installer (aka the ximian one, even though I kinda don't like that) would be totally sweet... at least do some quality control over the packages that you get. It looks very bad for KDE when I download RedHat 7.x RPM's that refuse to work w/o tons of silly dependancies for a not-outta-beta version of RedHat. A simple requirement to the packagers to make sure it works on the latest STABLE release of their distribution would be fantabulous. :)\n\nHowever, after a LOT of frustration, and then some luck (thanx to Ranger Rick for building actual 7.1 packages) I got KDE 2.2 on my RedHat 7.1 system... and while there are some minor issues, overall it's a definite improvement over 2.1... thanx guys!\n\n-r-"
    author: "in RedHatHell"
  - subject: "Re: RedHat 7.1 RPM Hell"
    date: 2001-08-24
    body: "Can you ellaborate ?\n\nI have all the 2.2 rpm's but no doc or note on the 2.1 to 2.2 upgrade path ?\n\nRedHat 7.1 vanila KDE install.\n\nRegards,\n\nDominic"
    author: "Dominic Baines"
  - subject: "Re: RedHat 7.1 RPM Hell"
    date: 2001-08-25
    body: "hey, I also had a vanilla RedHat 7.1 setup. The RPM's that I grabbed from ftp.kde.org wouldn't work w/o having RPM 4.0.3, and a new version of glibc, which I tried and it hosed my server... so I rebuilt and didn't wanna work with those RPM's anymore (since they were clearly built for RedHat 7.2beta)... \n\nFortunately I'd found a post on this topic on the dot, by Ranger Rick, here's the URL:\nhttp://www.opennms.org/~ben/kde2.2/\n\nyou can just go there, grab all his 7.1 rolled RPMs and then rpm -e your current KDE stuff (starting with games, multimedia, etc, working your way back towards your kdebase, kdelibs, and kdesupport), then in the directory you grabbed all the RPM's from the above URL, you can pretty safely do a rpm -Uvh *.rpm... if you try doing that before you uninstall kde 2.1, you can see more details about what you might be missing... then go over to rpmfind.net and track 'em down.\n\nRanger Rick did say that when he made these RPMs, he had Ximian Gnome installed, so there may be some libs or something that come into play...\n\nhope this helps...\n\nIf nothing else, I guess we all wait for either A) real, honest-to-God RedHat 7.1 RPM's to be made, or B) RedHat 7.2 to come out, and we all upgrade... grrrrr... \n\nanyway, good luck, read other postings on this topic here on the dot for more info... that's where I found everything I needed...\n\n-r-"
    author: "in RedHatHell"
  - subject: "Sound Server Issue w/ SuSE 7.2"
    date: 2001-08-21
    body: "After installing KDE 2.2 everytime I restart linux I need to reconfigure my sound sound card and re-start the sound server in order to have the sound work.   How can I fix this issue?  Thanks, Robert"
    author: "Plato"
  - subject: "Re: Sound Server Issue w/ SuSE 7.2"
    date: 2001-08-22
    body: "sounds a little bit strange,\nhmh, have a look at /etc/rc.config\nif START_ALSA is set \"yes\"\n\ndoes it help when you just restart alsa (instead\nof configure the soundcard everytime again):\n\n./etc/rc.d/alsasound restart\n\nif you still have the problem, have a look\nat suse's mailinglists, i think you will get\nthere any help you need.\n\ngerman  --> http://www.suse.de/de/support/mailinglists/index.html\nenglish  -->  http://www.suse.com/us/support/mailinglists/index.html\n\nsimon."
    author: "simon"
  - subject: "Disappointing Kicker"
    date: 2001-08-21
    body: "Well, at the 2.1 release I complained about a showstopper: Kicker's childpanels don't show up at login, have to launch Kasbar to show it. It didn't get better with 2.1.1. Now with 2.2 Kicker's childpanels show up but at what cost ? Kicker crashes so often it's a real shame ! Kicker is one of the most important parts of KDE, and I find it really disappointing that it's such a nuisance.\n\nNot only does Kicker crash extremely often, it also has refresh/display problems when I (re-)start it: I've installed the Acqua theme, and all I see from Kicker is a nice blue bar with a gradient to cyan. All applets and icons only show up when I get my mouse over them.\n\nThe big problem is not me, I use KDE since 1.x, but the newbies: what does a newbie do if his Kicker/Taskbar vanishes repeatedly ? In the worst case, he'll delete the Linux partition. Kicker/Taskbar are more important to the user than any other programm, IMHO.\n\nI think what the KDE-Project is missing is Quality Assurance. The first start of KDE 2.2 cleaned my desktop, all my links are gone, and the Kicker problems drive me nuts\n\nBut kudos go the Konquerer/KHTML teams, which did a good job, IMHO. No crash so far, but still some Java problems. Also all other applications seem to run just fine (e.g. KMail), which makes the Kicker disaster even bigger, IMHO."
    author: "Marc Haisenko"
  - subject: "Re: Disappointing Kicker"
    date: 2001-08-22
    body: "> Not only does Kicker crash extremely often, it > also has refresh/display problems when I\n> (re-)start it: I've installed the Acqua theme, > and all I see from Kicker is a nice blue bar \n> with a gradient to cyan. All applets and icons > only show up when I get my mouse over them.\n\nWell, then the Acqua theme is obviously buggy. Write a bugreport to the author, I don't think he can be expected to read your bugreport here.\n\nRegarding the crashes: are you one of the\n\"rpm -Uvh --nodeps --force\" guys? No wonder everything's crashing. I haven't had kicker crashes for ages.\n\n> I think what the KDE-Project is missing is \n> Quality Assurance. The first start of KDE 2.2 > cleaned my desktop, all my links are gone, and > the Kicker problems drive me nuts\n\nWhich distribution is that? Maybe the directory has changed (I think e.g. SuSE names it ~/KDesktop, while, originally it is ~/Desktop.\nMaybe all your links are in the other dir."
    author: "Carsten Pfeiffer"
  - subject: "Re: Disappointing Kicker"
    date: 2001-08-23
    body: "I am a \"rpm -Uvh --nodeps --force\" guy (Mandrake 8.0) and I have no crashes with Kicker. Only problems of applets in vertical mode (and KNewsTicker)."
    author: "Alain"
  - subject: "Re: Disappointing Kicker"
    date: 2003-06-11
    body: "Whoa whoa whoa.\nI'm using kicker in version 3.1 of KDE, and it crashes REPEATEDLY on me.  I can almost reliably reproduce the crash as well!\n\nI'm using the KDE install that comes with Slackware 9, and the ONLY thing that seems to crash repeatedly is kicker.\n\nNodeps etc.. has nothing to do with this.  If the panel shows up, the libraries are there.  If a dependancy is missed, it most likely won't run!\n\nThere are definatley some stability issues with certain KDE apps, but it has been getting better.  I feel no need to go out of control complaining.  Everything will solve itself in the long run."
    author: "Omega"
  - subject: "Re: Disappointing Kicker"
    date: 2003-06-11
    body: "Forget KDE on Slackware 9: They shipped a borked KDE<->Qt version combination."
    author: "Anonymous"
  - subject: "Re: Disappointing Kicker"
    date: 2001-08-24
    body: "never been a problem to me. check ur compiling."
    author: "Rajan Rishyakaran"
  - subject: "Re: Disappointing Kicker"
    date: 2004-03-23
    body: "I totally agree with this message...\n\nThe kicker crashes EACH time I start KDE, I've reinstalled the system for several times in various variants, still the same... Furthermore, there is no contact information given where to submit bug reports... Anyone know an address?\n\nThe only solution to this problem is to use a better operating system like Solaris :-)"
    author: "Stefan Probst"
  - subject: "Re: Disappointing Kicker"
    date: 2004-03-23
    body: "It's always http://bugs.kde.org - how did you manage to reply to a three year old comment?"
    author: "Anonymous"
  - subject: "Re: Disappointing Kicker"
    date: 2004-03-23
    body: "tried to search for a solution to my problem in google :-). Google doesn't matter about months or years.\n\nAnyway, thanks a lot!"
    author: "Stefan Probst"
  - subject: "Re: Disappointing Kicker"
    date: 2005-06-08
    body: "Hello! Now its 2005 and this thread is still so right: I have 3.4.1 wich is masked as unstable in gentoo (~x86), but nothing crashes as often as kicker. and it looses (forgets) applets. What is so tricky about that kicker-app?\nAre there any alternatives?\n\nHello 2006?"
    author: "Stefan Kr\u00fcger"
  - subject: "Re: Disappointing Kicker"
    date: 2005-06-08
    body: "> What is so tricky about that kicker-app?\n\nNothing. I have run the 3.4.0 and 3.4.1 versions of kicker from the time they came out for Debian Sarge. The packages are called \"experimental\" but kicker hasn't crashed on me a single time.  \n\nSo it's either Gentoo (maybe it's masked as unstable (hint, hint) for a reason?) or you (have you optimised it into borkage?)."
    author: "cm"
  - subject: "Slack 8, KMail issue"
    date: 2001-08-22
    body: "I have Slackware 8.0, and after some fiddling, I have managed to compile and install KDE 2.2 from the sources.\n\nEverything seems to be working ok, however, I am an avid user of KMail which poses a rather annoying problem: it locks up SOLID. So solid, in fact, that the only option is to reboot the machine (can't seem to kill X) though I haven't tried fixing it remotely. After rebooting, the homedirectory of the user that was logged on in KDE is trashed, and I unfortunately lost a lot of emails (forgot to backup).\n\nAlso, rather annoying, is that the installation instructions on the site speak about the ksupport package, which is nowhere to be found in the 2.2 tree. I assumed it has been incorporated into the klibs?"
    author: "Shoikan"
  - subject: "KDE 2.2.x Packages for Debian Woody"
    date: 2001-10-30
    body: "Please, post a source (for exapmle in \"deb ...\" form)\nfrom where i can \"apt-get\" the\ndeb packages (binaries) for Debian Woody.\nI haven't found any source yet.\nThanks!"
    author: "Gabor Fekete"
---
After some delay caused by a severe hardware failure on KDE's ftp server,
the KDE Project has <A HREF="http://www.kde.org/announcements/announce-2.2.html">announced</A>
the official release of KDE 2.2.  This release brings a <EM>lot of goodies</EM>,
including:  faster startup times (using the experimental
<A HREF="http://www.research.att.com/~leonb/objprelink/">objprelink
method</A>) and performance; numerous improvements to HTML rendering
and JavaScript support; the addition of IMAP support (including SSL and
TLS) to KMail; a new plugin-based print architecture with integrated
filter and page layout capabilities; a number of new plugins for Konqueror
(including a Babelfish translator, an image gallery generator, an HTML
validator and a web archiver); native iCalendar support in KOrganizer; and
a new personalization wizard.  <A HREF="http://www.compaq.com/">Compaq</A>
has also announced the addition of KDE to
<A HREF="http://www.tru64unix.compaq.com/">Tru64</A>.  Time to tell the boss to forget XP, and use KDE (hmmmm, back in my college days that would have made a nice chant:  'Forget XP, use KDE', . . .).

<!--break-->
