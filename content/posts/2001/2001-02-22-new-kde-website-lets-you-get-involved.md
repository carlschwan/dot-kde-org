---
title: "New KDE Website Lets You Get Involved"
date:    2001-02-22
authors:
  - "Dre"
slug:    new-kde-website-lets-you-get-involved
comments:
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-21
    body: "Nice idea... how bad that i am sitting in an internetcafe running win95... ;-)"
    author: "Henri"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "Bad boy ;)"
    author: "Shift"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "Great site!  One useful thing would be to be able to assign or take unassigned tasks otherwise you're stuck with a list of unclaimed tasks."
    author: "ac"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "Good point, done (provided you are logged in, of course)."
    author: "Dre"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "Well, considering that 2.1 isn't actually out yet, would it be alright if we just took some shots from 2.1beta2?"
    author: "CiAsA Boark"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "Sure, that would be great."
    author: "Dre"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "Well, there are things missing in 2.1beta2 that are in 2.1 like the KControl and Konqueror artwork.  Maybe you can update to the KDE 2.1 branch from CVS."
    author: "KDE User"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "Hehe, not on a 56k i can't.  Especially if the real 2.1 is supposed to be out in a couple of days.  Does anyone know if there is a comprehinsive changelog of whats going to change from the 2nd beta to the final 2.1 release.  I'll try not to include any apps that will have changed signifigantly in the release."
    author: "CiAsA Boark"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "Although there are lots of different pages out there that advocate the use of Linux, I haven't seen many that advocate using it in a family setting. (Not everybody who uses KDE is a college student you know) I currently  have KDE 1.x on my machine at home and have set up accounts for my whole family.  Even my three year old (Four in a few days) can log in and play kasteroids.  Linux is truely multiuser and with the kids each having their own accounts they can play with desktop themes and draw or do several things - BUT they can't delete anything important of MINE.  (This is not true of the other common OS for X86 machines.  I can personnally testify about that.)\nAdministering a linux machine may take a little learning and computer skill, but with KDE the kids don't have any trouble using Linix to do things they want to do.\n\nI said all that to say that one thing that I have been thinking about is \"Linux Family Page\" which would advocate use of Linux for kids as well as encourage programs the whole family can use.  I think this new promo page of KDE could include a page with family use how-tos and testamonies."
    author: "Keith Sogge"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "<P>\nHi,\n</P>\n<P>\nDo you really want a family page?  Then there's an easy solution -- make one!  That's what promo.kde.org is all about -- so everyone can contribute and bring their ideas to life.  Because there is no programming involved, it doesn't matter if you can program or not.  Everyone can contribute.\n</P>\n<P>\nIf you want to do a group project, you can easily start one as well.  Although only one username/password pair can edit a \"todo item\" once assigned, there is no reason a group can't share an account.  So if you have an idea like this, it's in your hands to make it come to life.\n</P>\n"
    author: "Dre"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "I'm thinking some Gphoto KIO slave in action...\n\nwhat do ya think (P.S. I'll actualy leave this for you to do :)"
    author: "Greg"
  - subject: "Re: Gphoto KIO slave"
    date: 2001-02-22
    body: "Does this work in beta2? And how?"
    author: "jakke"
  - subject: "Re: Gphoto KIO slave"
    date: 2001-02-23
    body: "I'm not sure how stable or useful it is yet, but thekompany uses a neat looking little project called kamera to interface with gphoto (as far as I am aware of)."
    author: "Carbon"
  - subject: "Sign up  for a task now!"
    date: 2001-02-22
    body: "Well, to all those of you who's ever wished that KDE had better PR, *now's* your chance to get involved.\n\nSign up for *one* concrete thing to do on promo.kde.org and that will add up to a lot!  So far we have two suggestions with no owners.  Sign up and take a task!  Submit a new task.\n\nCheers,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "hi,\nis it possoble to take a screenshot of kdm/dm (the first loginscreen)\n-gunnar"
    author: "gunnar"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "Sure, why not, I can do this tonite (with my digicam of course) :-))\n\n\nWhere can I drop that off?\n\n\nbye\n\tL"
    author: "Mathias"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-22
    body: "Oh yay, me and my stupid self...\njust hop to that KDM from some other machine, with Xnest -query $otherhost, then take a snapshot of that Xnest window..."
    author: "Mathias"
  - subject: "One of my Ways..."
    date: 2001-02-22
    body: "hi,<br>\ni love kde/linux.<br>\nmy way to promote kde: writing stuff like this:\nhttp://www.dynamic-webpages.de/18.editoren.php?type=editor&id=21\n<p>\nor this new screenshot:<br> \nhttp://jhm.ibs-ev.de/demo/screenshots/quanta.png\n<p>\nlooks nice, isn't it?<br>\n-gunnar"
    author: "gunnar"
  - subject: "Here's my contribution:"
    date: 2001-02-22
    body: "Can I point out a bunch of spelling and grammatical errors on the page?<p><i>contemporary functionality, and outstanding graphical</i><p>No comma needed there.<p><i>This does not mean developer's are not welcome here;</i><p>developers<p><i>It's success is driven by</i><p>Its success<p><i>Bascially anything else</i><p>Basically<p>Also, kpp is already the name of a (very useful) app for creating RPMs."
    author: "foo"
  - subject: "Kool Desktop Environment"
    date: 2001-02-23
    body: "you people lie that K in KDE doesn't stand for anything but here when Matthias Ettrich started KDE he named it Kool Desktop Environment, see his 1996's posting:\n\nhttp://www.via.ecp.fr/lyx/archive/9610/msg00368.html"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Kool Desktop Environment"
    date: 2001-02-23
    body: "Yo, dats kool!"
    author: "reihal"
  - subject: "Re: Kool Desktop Environment"
    date: 2001-02-23
    body: "Sure, it has to stand for something. No one will ever believe it was chosen randomly. A random name for a DE ? Come on..."
    author: "Anonymous Coward"
  - subject: "You have a problem with verbs."
    date: 2001-02-23
    body: "It doesn't stand for anything.\nIt stood for Kool."
    author: "Roberto Alsina"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-23
    body: "you should add kde promo link to the primary kde site\n\ncheers \njorge"
    author: "jorge"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-23
    body: "Hi,\nwhere on promo.kde.org can I find screenshots? Or is it just impossible because still there are no ones?\ncheers"
    author: "Henri"
  - subject: "screenshots on promo?"
    date: 2001-02-23
    body: "All incoming screenshots are collected and will then be uploaded to a gallary (not sure on what server).\n\npromo.kde.org is for promoters while the shots are indented for newbies (and should therefore be uploaded somewhere with high bandwidth and then be announced on the dot.\n\nGreetings\n\nDaniel"
    author: "Daniel Molkentin"
  - subject: "Re: New KDE Website Lets You Get Involved"
    date: 2001-02-23
    body: "I have public (for KDE :) FTP space (uploads and downloads)\nat ftp://207.44.242.45 (ftp://derkarl.org)\n\nFeel free to download the many screenshots there, and also upload :)"
    author: "Charles Samuels"
---
Just a few weeks ago we <A HREF="http://dot.kde.org/981408721/">announced</A>
the launch of the KDE Promo
<A HREF="http://master.kde.org/mailman/listinfo/kde-promo">mailing list</A>.
Today we are launching a website,
<A HREF="http://promo.kde.org/">promo.kde.org</A>, to go along with it.
The site is dedicated to the promotion of KDE, and is for anyone who wishes
to contribute to KDE other than coding (for which we have
<A HREF="http://developer.kde.org/">developer.kde.org</A>) or
translating (for which we have <A HREF="http://i18n.kde.org/">i18n.kde.org</A>).
A suggestion for an easy way to contribute today follows.


<!--break-->
<P>
In preparing the press release for KDE 2.1 I thought it would be nice
if we could really show off our beautiful desktop to others.  So I came
up with the idea of a KDE 2.1 Desktop Slideshow, which would combine thumbnails
from various categories into a slideshow presentation.  If you would like to
contribute to this, fire up your favorite screenshot grabber and show us
what you got.  If you don't have a server to place the image let me know
and I can give you a link to store it, just ask; otherwise please email me the link
only to pour&#064;kde.org.  Please include a brief sentence (what applications are being show,
how they are being used, etc.) and your name (so you can get credit).
</P>
<P>
The slideshow will have the following categories:
</P>
<UL>
<LI> full desktop shots -- not too busy but giving an overview of the whole thing;</LI>
<LI> Konqueror shots;</LI>
<LI> KMail shots;</LI>
<LI> control center shots;</LI>
<LI> panel shots (with applets/etc.);</LI>
<LI> KDevelop shots;</LI>
<LI> KOffice shots;</LI>
<LI> other-language shots (particularly non-European languages); and</LI>
<LI> anything else which you think shows a nice working KDE desktop.</LI>
</UL>

