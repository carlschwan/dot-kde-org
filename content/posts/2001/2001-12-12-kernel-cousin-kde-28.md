---
title: "Kernel Cousin KDE #28"
date:    2001-12-12
authors:
  - "rkaper"
slug:    kernel-cousin-kde-28
comments:
  - subject: "Error - Does not compute."
    date: 2001-12-12
    body: "\"Kernel Cousin KDE #28\"..."
    author: "Ill Logik"
  - subject: "Re: Error - Does not compute."
    date: 2001-12-12
    body: "Oops, typo. Can one of the Dot editors fix the title please? Thanks."
    author: "Rob Kaper"
  - subject: "Re: Error - Does not compute."
    date: 2001-12-12
    body: "Also, the Beta1 link should be kde-3.0-release-plan.html, not kde-3.0-release-schedule. Remind me to drink more coffee before I make another announcement."
    author: "Rob Kaper"
  - subject: "What stops the developers from.."
    date: 2001-12-12
    body: "... simply lifting a lot of the code from librsvg (GNOME) to KDE? SVG-support in Gnome is pretty advanced, although unstable and a theme utilizing it is in development. \nLook at these screenshots:\nhttp://jimmac.musichall.cz/screenshots/gorilla6.jpeg\nhttp://jimmac.musichall.cz/screenshots/com.jpeg"
    author: "Gaute Lindkvist"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-12
    body: "I dunno, Niko and Rob have done a great deal the last few weeks.  While we where busy yammering on the lists, they have been codeing their brains out.\n\nNiko and Rob are not provideing screen captures though, mainly because they are too busy adding features.  I think in the last two weeks they passed three more tests. Check out KSvg from kdenonbeta, it may not be the endall be all SVG viewer but it really is on the road.\n\n-ian reinhart geiser\np.s. didnt someone say that we could have SVG support in icons allready because QT supported static SVG?"
    author: "Ian Reinhart Geiser"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-12
    body: "When QT3 came out and the docs said it could load and display SVG files, I wrote a quick test program just to see how well it worked. Well, most SVG files didn't even load. I can't recall what the trouble was, but it had to do with DOCTYPE -- so I muddled with the parts of the files that made the XML parser angry, and it worked -- *but* the output was crappy as hell. Fills were wrong, and the display wasn't antialiased. Of course, there are dirty ways around antialiasing, like oversampling, but it seems to the the GNOME guys, with their (beautiful) gnome_canvas, have the right idea.\n\nGo competition!\n\n(That's why I like that Linux has more than one desktop, even though I prefer KDE)"
    author: "Shamyl Zakariya"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-13
    body: "Qt's SVG support was rudamentary. That's why ksvg was written. If SVG becomes a major standard (which it hasn't yet, mainly because of lack of many windows apps using it), then you can expect more SVG support in Qt. This is how PNG/MNG was put into Qt as well. The first support was there because people wanted it, but now after it's fairly commonly used, it's absolutley great."
    author: "jz"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-13
    body: "Perhaps KDE should incorporate libart? http://www.levien.com/libart/\n\nI'm sure it could be put to very good use."
    author: "ac"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-13
    body: "I was writing a vector graphics api for BeOS last year (http://home.earthlink.net/~zakariya/vector_api.html); but when I saw libart and another project, Zodius (http://www.tinic.net), I realized other folks had already done it, and better. Sigh. Well, I've attached a snapshot from that project. If any of you KSVG folks want me to lend a hand, I'd be happy to! (But I confess I'm rather busy on robotics stuff right now)"
    author: "Shamyl Zakariya"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-13
    body: "http://mail.kde.org/mailman/listinfo/ksvg-devel exists, but it doesn't see much traffic at the moment. Otherwise you could contact the developers directly if that doesn't work. Nice pic by the way :)"
    author: "ac"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-13
    body: "I'm sure it could be done, as libart doesn't depend on glib, gtk, or gdk...\n\nbut the question now is why? a lot of what is needed from libart is already implemented in ksvg. somethings are not in ksvg that are in libart, like \nMicroTile Arrays, but then again, that's not needed :)\n\nand from my prior experience with libart's source, I can tell you that it's pretty messy. that's mainly because 95% of the code hasn't been revised at all in over two years. on the other hand, libart works :)"
    author: "reflection"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-13
    body: "Libart does support anti aliasing though, which would be a nice thing to have with vector graphics. I don't think that ksvg does AA at the moment. Perhaps they could write a wrapper round the library so if/when someone cleans the code up, they wont be affected too much."
    author: "ac"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-12
    body: "librsvg uses gdk stuff extensively and is written in C. i don't think grabbing a bunch of stuff from librsvg would have done much good since probably all they would've been able to salvage would have been the parsing bits, which are the trivial parts (as opposed to the actual drawing)\n\nas one would expect, ksvg integrates with kde well. it includes a konqueror plugin and uses Qt where appropriate. also, Niko and Rob probably Just Wanted To Do It. ksvg already passes a lot of the SVG tests and contains ~16k LOC. that ain't bad."
    author: "Aaron J. Seigo"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-13
    body: "from the svg-isn't-as-easy-as-it-looks-like-dept:\ntechnical problems ahead\nhttp://lists.kde.org/?l=kfm-devel&m=100557333002835&w=2"
    author: "anonymous"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-13
    body: "the svg problems in Qt talked about in that thread were one of the primary reasons why ksvg was started. the thread you linked to is over a month old and since then ksvg has improved quite a bit, surpassing the Qt svg support.\n\nit would have been much more interesting (and worrying) to link to the threads that talked about the rather advanced font rendering required by SVG and how far along the people from the batik project figure ksvg is.\n\nif you are going to troll, at least do it with some style."
    author: "Aaron J. Seigo"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-15
    body: "I think the ksvg project is wonderful. But are you sure it is suited for icon loading? To me it looks optimized for completeness (in terms of the API as well as in features) , which sounds just right for say usage in a browser or even a vector drawing app. But I think for icon loading performance and low memory consumption counts more. I wish there would be an easy pattern allowing to cache svg data for icons."
    author: "anonymous"
  - subject: "Re: What stops the developers from.."
    date: 2001-12-16
    body: "That's why we are also implementing an icon server that does intelligent caching based upon the file type. ksvg has an acceptable speed, but there are still a few optimizations needed to be done. Note that any complicated vector format where many calculations need to be done."
    author: "zzz"
  - subject: "Mouse gesture support discussion"
    date: 2001-12-12
    body: "There /was/ some discussion about this, see Simon's forward to kfm-devel and the replies:\n<a href=\"http://lists.kde.org/?l=kfm-devel&m=100676595321522&w=2\">http://lists.kde.org/?l=kfm-devel&m=100676595321522&w=2</a>"
    author: "Carsten Pfeiffer"
  - subject: "Re: Mouse gesture support discussion"
    date: 2001-12-12
    body: "thanks Carsten.. i'll make sure there is an addendum in next week's issue."
    author: "Aaron J. Seigo"
  - subject: "Re: Mouse gesture support discussion"
    date: 2001-12-15
    body: "Isn't there a kgesticure for kde, I've tried it. It's nice, but difficult to make a good gesture.\nYou have to repeate a \"M\" three times to save it, and that isn't easy. But it's good and needs more work on it.\n\nThis could be integreated in kde, and then also in konqueror, koffice and k... Maybe in 3.1, or 3.2.\nIm thinking about Koffice --> draw a \"b\" for bold, \"I\" and \"I\" again for Import an Image...\n\nonly a user suggestion ;)\n\n\nhave fun\nHAL"
    author: "HAL"
  - subject: "kde3beta1"
    date: 2001-12-12
    body: "wasn't it supposed to be out on the 10th?"
    author: "Bob"
  - subject: "Re: kde3beta1"
    date: 2001-12-12
    body: "and if you read the last few week's KC KDE issues you'll know why it isn't out yet."
    author: "Aaron J. Seigo"
  - subject: "Re: kde3beta1"
    date: 2001-12-12
    body: "But QT 3.0.1 is out now, so beta 1 should come out pretty soon now, shouldn't it?\n\nOn an only slightly related note, I see lots of good improvements in QT 3.0.1, most notably in QFont:\n\nX11 only: improved line width calculation. Fixed off by one error in interpreting Xft font extents. Allow the use of both Xft and non Xft fonts in the same application. Make sure fonts are antialiased by default when using xftfreetype. \n\nDoes this mean AA works in KDE 3.0 now?  Does this also mean we can finally use all of the fonts installed on our system instead of only the ones that XRender finds?  It sounds promising!"
    author: "not me"
  - subject: "Re: kde3beta1"
    date: 2001-12-12
    body: "Yes, antialiasing now seems to work properly as do fonts and font selecting widgets (thanks to some KDE hackery done by Waldo and some adjustments in Qt). But for some reasons some programs don't draw all entries in the menu bar since pre Qt 3.0.1 times but I am not sure wether this is a QToolBar problem or rather a problem with the KDE classes.\n\nCheers,\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: kde3beta1"
    date: 2001-12-13
    body: "> Does this also mean we can finally use all of the fonts installed on our system instead of only the ones that XRender finds?\n\nyes"
    author: "jz"
  - subject: "Re: kde3beta1"
    date: 2001-12-13
    body: "Relax, KDE is only 3 days behind timetable while Gnome is 6 days behind. ;-)"
    author: "someone"
  - subject: "Re: kde3beta1"
    date: 2001-12-15
    body: "Who cares? Anyway you can't compare: One is a \"Developer Beta\", the other a \"User Beta\"."
    author: "anonymous"
  - subject: "KDE2+3 howto"
    date: 2001-12-12
    body: "Just to say that for french talking people there is a \nKDE2+3 howto on http://www.kde-france.org/article.php3?id_article=17"
    author: "Laurent Rathle"
  - subject: "Re: KDE2+3 howto"
    date: 2001-12-13
    body: "the article looks cool.. maybe do an english translation and submit to the new free software magazine? ;)_"
    author: "jz"
---
Another <a href="http://kt.zork.net/kde/">Kernel Cousin KDE</a> has arrived! <a href="http://kt.zork.net/kde/kde20011207_28.html">Issue 28</a> covers mouse gestures in <a href="http://www.konqueror.org/embedded.html">Konq/E</a>, SVG support for KDE, <a href="http://www.appel-systems.de/jay/norsk.png">Norwegian verbs</a>, another <a href="http://women.kde.org/projects/coding/kde2+3.html">KDE2+3 howto</a>,  and other exciting events in the world of KDE development. <a href="http://developer.kde.org/development-versions/kde-3.0-release-plan.html">KDE 3 Beta1</a> is on its way, but that doesn't seem to stop developers from coming up with interesting ideas for the future of your favourite desktop!
<!--break-->
