---
title: "KDevelop 3.0 Now Developer-Ready"
date:    2001-04-07
authors:
  - "numanee"
slug:    kdevelop-30-now-developer-ready
comments:
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "KDevelop is a nice enough IDE but the editor is severely lacking IMHO. I mean.. project handling, integrated docs and class browsers are all nice, but most of the work when programming is still typing. This is why i'm still sticking with emacs - mainly because it idents the code the way i want it without me having to fight it. Will Gideon have something more of powerful editor? \n\nKWrite is severly lacking for programming.. kwrite vs emacs is like wordpad to word (in the windows world). I have nothing against kwrite, it's a fine editor - but not tailored specifically for programming.\n\n-henrik"
    author: "Henrik A"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "Perhaps you want to check KFTE? It's a very nice programmer's editor, if I may say so myself :-)\n\nCurrently it can't be integrated into kdevelop, but I think I know of a way to achieve it. Maybe."
    author: "Roberto Alsina"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "I tried it out... it rocks :-)"
    author: "ac"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-08
    body: "Does it support multiple views per document?\nDoes it support breakpoints? Of course, you\ncould integrate any other editor, including kfte. But without these features, you will\nlose quite some functionality."
    author: "Bernd Gehrmann"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-09
    body: "Multiple views per document? Yes, but it's broken :-)\n\nBreakpoints? That's a debugger's job ;-)\n\nI was comparing it to kwrite, not kdevelop as a whole.\n\nRight now, I am working on a DCOP interface to KFTE's guts, so it will be \"integrable\" into kdevelop eventually, and everyone will be happy ;-)"
    author: "Roberto Alsina"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-09
    body: "I understand that you were comparing it to kwrite.\nMy point is that 'making everyone happy' is probably\na bit more complicated than having a couple of\ncommands like 'open file foo.cpp'. For example,\non a monitor less than say 19\", you will want to\ncoordinate window management between kdevelop and\nthe editor, otherwise half of the time the editor\nwill cover your documentation or error message\nwindows, and half of the time the kdevelop windows\nwill cover your editor (aka the 'gimp effect').\n\nAs for breakpoints being the debugger's job I\nassume you know that kdevelop's debugger uses\nkwrite for displaying source code? :-) When\npeople use the integrated debugger, I assume they\nwant to use the source display for editing files\nimmediately. Otherwise the point of an integrated\ndebugger looks quite moot to me. OTOH, the only\ndebugger commands I ever use are run and bt,\nso maybe I'm the wrong person to discuss this ;-)"
    author: "Bernd Gehrmann"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-09
    body: "KFTE will have an embeddable view. The plan is to make a fully fledged kpart. Precisely because of the gimp effect :-)\n\nAbout the debugger stuff... well, I suppose it's just a matter of providing the same interface KWrite does. Since the exposed functionality of FTE's EBuffer class seems to do all that and some more, I don't see a problem.\n\nIt's just a matter of work :-)\n\nMaking KFTE a KPart, on the other hand, requires some thought, which is why I say it is a plan, right now :-)"
    author: "Roberto Alsina"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "Maybe people already know if it, but what about scintilla (<a href=\"http://www.scintilla.org/\">http://www.scintilla.org</a>)?  I don't know how much it would take to integrate it, but I think that thekompany is using it for blackadder, activestate is using it for komodo.  it has auto-indent, code folding, lots of lexers, and is fast...\n"
    author: "n/a"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-08
    body: "We're going to release our port of Scintilla to Qt shortly.  We are just coordinating with the scintilla group.  We also ported it to BeOS, QNX and AtheOS, so there is a bit to organize."
    author: "Shawn Gordon"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2002-08-01
    body: "Shawn, when is soon? I've seen several announcements from TheKompany that this is going to happen any time now. Would be nice if it really happened."
    author: "Wolfgang"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-08
    body: "I fully agree with you. Emacs might not be the nicest editor, it's still the most powerful and when you start programming, why would you not choose the best editor ? The way \"Tab\" works is really fantastic !\nKDevelop is nice, help etc. is cool but I still use Emacs (though it's really Esc Meta Alt Control Shift so you'd better use it daily to remember all key comibnations).\nSomething between Emacs and Borland's Builders (class completion, help, on-the-fly compilation for JBuilder: as a friend says, nowadays processors really allow that) would be the best."
    author: "Henri"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "KDevelop is nice, but an editor that can do auto-indention and code-insight stuff would be about 100 times cooler.  I'm not realy sure how the progress on Kvim is but alot of work is being done to have Gtk and Qt use the same message loop and gvim works perfectly, and that means that since XEmacs is going to switch to Gtk that XEmacs could be used as an editor as well.  Also for code-insight stuff, I once wrote something like that for Gtk but I deleted it some time ago, if I ever re-write it I'll probably mail it to one of the KDevelop people, and I'd be happy to help write one for KDevelop if it has little dependancy on Qt(I'm not a particular fan of the GPL either)."
    author: "robert"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "XEmacs is not going to <B>switch<B> to Gtk. Gtk is now one possible widget set that you can compile XEmacs to use. Work is I believe either in progress or soon to start on a Qt/KDE binding for XEmacs."
    author: "Dan Smart"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "Yes, and it is not even a stable or recommended port.\n\nI strongly believe \"robert\" is an incoherent troller.  Like he wrote a code-insight editor in Gtk and then threw it away.  Like he will write a new one and mail it to KDevelop developers. Right...\n\nBest to ignore him.  I don't understand why these trolls don't stay on the GNOME site."
    author: "ac"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "There was alot of talk about changing XEmacs to use Gtk by default on X, there's a Gtk port as well, Gtk and Qt will be in the same message loop soon that means that Qt programs will be able to use Gtk widgets with much less overhead, as far as I know the Gtk port is further along with vim and Xemacs, it's stupid to not use something because of the toolkit it's based on, the power of open-source is that the code is allready there. \n\nAlso this code-insight editor is not all that hard to write, just do this: \n\nHave a bunch of structs like\nvar_t \ntype_t\nfunction_header_t \n\nThat have there positoins on the text held by int pointers that are maped to a list of int's that get updated with text mutations(like if I insert a char go to that point and incriment all the values, if text is deleted decrement some values and update alot of your variable data stuff(keep reading)) and have them also hold a bunch of info like the acessor(public, private etc) and what the real type they are(strip out stuff like auto etc).  Then write a nice parser that works with std::string's that first cleans comments and macros out of the string and get's rid of extra spaces, the parser can then seperate the words getting rid of problems like ' std :: string' is one word, from there it's easy to find functions, var declerations and such, when you get the decleration take the type or the return type and get the actual type(clean out words like auto const and such, that way if the type is a class you can look that up).  Then  make a class like scope_t and give it a virtual function like parseline, have maps that map strings that are variable and function and types names and such to there coresponding structs, also the have pointers to the children scopes.  Make classes like class_t and namespace_t and have them extend scope_t, scope_t should also have something like base_parseline, that takes care of getting the acessors right and such.  Then make a struct called something like scope_lookup_data that has the stuff that the code insight shows, this should take a parameter that can be passed that shows the acess type('::', '->', '.') and the part of the word allready typed, this function should also be virtual.  Then have a queue that holds the lines that were changed(there can be all sorts of optimization to make sure the same line dosen't get queued a bunh of times).  Then have a global var thats a scope_t named something like scope_in, if a line is found that has something like the beginning of a class definition then you should have a list of children pointers in the scope, set one of them to a 'new' scope and set scope_in to new scope.  I'm sure I forgot some stuff, the reason that I deleted it is because it only took me like a week to write, and I didn't work with any editors or IDE's so it was pretty useless, plus VisualStudio and SlickEdit work fine for me.  Also the reason that I used Gtk is because I don't like the GPL, if I did give it away it would be under a BSD type liscence."
    author: "robert"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "Don't worry about XEmacs. It's okay, but most people use it only for the identation and because they have spent ages customising it. Why don't you \ntry out:\n<br><br>\n<a href=\"http://devel-home.kde.org/~kate/\">http://devel-home.kde.org/~kate/</a><br>\nit's the new Kwrite.\n<br><br>\nAlso KFTE... it's very good.\n<br><br>\nKvim is being developed... but (I think) it is not being ported to QT, it's instead being turned into an XPart. Should be cool once it's finished.\n<br><br>\nYou seem to know what you want, maybe you could help out on one of these projects? :-) If you don't like QT because it's GPL, well, that's no problem since it's also under the QPL licence, so no worries releasing code linked to QT if it's under a BSD style licence.\n"
    author: "anon"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "Hmm, XEmacs has *all* features listed on the Kate page and about a million more.  I think I'll switch to Kate _right now_..."
    author: "Anon"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "I meant to say that there are alternatives if you use XEmacs just as a simple editor. Of course, if you use those million features then stick with XEmacs :-)"
    author: "anon"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "If you would like to be productive, why not stop your trolling and fudding?  You seem like a reasonable guy.\n\nSo why do you perpetuate myths that you can't release code under BSD for Qt?  The KDE project does it all the time. Just go read the licenses, and ask rms@gnu.org for clarification if you cannot understand the GPL.  English can be a tricky language, especially legalese.\n\nAnd what is this message loop crap you talk about?  Don't you realize your vaporware talk helps nobody?"
    author: "ac"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "I'm sorry if I said something wrong, I don't use linux and I just started working on some stuff with KDE and GNOME about a month ago, I'm not real familiar with all the stuff.  Also there will eventualy be a merge of the Qt and Gtk message loops, this wont effect develops becuase Qt and Gtk both use signals."
    author: "robert"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "All that needs to be said here is ... bullshit baffles brains. He doesn't use Linux but has already written a GTK project, and thrown it away? He then right that he is not familiar with \"all the stuff\". \n\nAnd his last sentence is an absolute gem as well.\n\nrobert, go away, now!"
    author: "KDEBoy"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "Actualy the thing about the message loops is true."
    author: "AC"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "I use Solaris OE, GNOME and KDE both run fine, I've never used linux before though."
    author: "robert"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-06-12
    body: "KDEBoy is suffering from mind bias, but that's understandable. not-Linux doesn't imply not-KDE ;-). What robert says is 99% right, no need to attack him.\n\nMessage loop merging is being worked on, and there's nothing fantastic about it. That's what windowless controls in Windo$e do all the time (sort of, indirectly), no big deal, even crappy M$ programmers do it.\n\nrobert is OK with me, I'd say. Only those people who look deeply into the future are making the progress. I presume KDE founders were accused of trolling or making-useless-things a few times in the history as well. Arguments like `Oh but why another desktop environment' and `better concentrate on Gnome' or whatever must have been raised from time to time.\n\nThis is a discussion forum that, to my understanding, is meant to promote flow of ideas. One can really make a big piece of code in one week. Just because M$ Visual Studio takes so many megs to do simple code-insight editing doesn't mean it cannot be done in about 6000 lines of code, with similar efficiency.\n\nMy coworker, sitting next to me, ported a Windows realtime data display application (written in API, no MFC), to Qt on Linux in 2 days. That was about 3000 lines of code. At the end of 2nd day it worked flawlessly. That's just an example. Another is porting mozilla to Qt, which took one mythical-man-month. There's no way I'd ever dare not to believe robert about what he did.\n\nMyself I threw out and/or abandoned lots of code. I had a simple statistics application for hand-made measurement processing, that was about 15000 lines long and it was in Pascal for DOS. I still have it somewhere, but it's one copy on one floppy somewhere. For practical purposes it's a work thrown away. I'm nearly sure that in my life I abandoned about 50000 lines of code in several projects, most of them were quite useful. I'm 25, if that matters.\n\nCheerz,\nKuba"
    author: "Kuba Ober"
  - subject: "xemacs/gtk not stable?"
    date: 2001-04-08
    body: "I've run xemacs/gtk since September, and have not suffered a crash since November. That's pretty stable to me. IMHO, it should be default, if nothing else, for the new file chooser <grin>\n\nI love xemacs too, I wish startup time was a bit faster, but with all the lispy's I need for my development, it takes a bit.\n\nNow, if kdevelop got a little farther, had some plugins for Ruby, Perl, and PHP development for me, it could definitely be a very good thing. I already use xemacs & joe, I'd hate to add a 3rd editor for just my C work. Glad to see KDevelop getting better, now my roomate no longer bitches about crappy IDE's for C."
    author: "Thomas Stromberg"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "That's what I meant, sorry if that sounded misleading, but the thing is that Gtk will soon use the same message loop and Qt(hopefuly) which means that overhead will be minimal and becuase of QGtkWidget it would easy to get a good editor now, and I had never heard of a Qt port of XEmacs."
    author: "robert"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "Congratulations!  Trust the KDevelop guys to develop two branches at once!\n\nI must say you guys really did a brave thing in deciding to put HEAD on hold and port KDevelop 1.2 to  the KDE 2.x platform.  Everyone wanted this and we all love you for doing it!  Now we have KDevelop 1.4 a thing of beauty, next KDevelop 2.0, and then everybody's dreams will be realized with KDevelop 3.0.\n\nIgnore your detractors and forge ahead!  There is a big need for this project."
    author: "KDE User"
  - subject: "YES !!"
    date: 2001-04-08
    body: "Agree with you.\nKedevelop 1.4 is practical and beautiful.\nAnd Kdevelop 3 look's great.\n\nThank !!"
    author: "thil"
  - subject: "Developer-Ready?"
    date: 2001-04-07
    body: "Clarification?  So who are the developers here?  I guess \"users\" of KDevelop are not developers in this sense, right?  3.0 is maybe ready for people to develop _for_ it, but not for people to develop _with_ it?\n\n-Justin"
    author: "Justin"
  - subject: "Re: Developer-Ready?"
    date: 2001-04-07
    body: "You're about right. Only the bravest of the developers have started eating the dogfood :)"
    author: "nap"
  - subject: "Re: Developer-Ready?"
    date: 2001-04-07
    body: "Well, I've been using KDevelop (gideon) for the last week, and have found it pretty stable. There are rough edges, and missing features, but it didn't crash on me. Bernd's post above on Friday morning is slightly out of date, as since Friday afternoon KDevelop 3.0 has had Java and Objective-C support added. You need to check out the current 'kdebindings' module and build it to compile and run the Java project template.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-07
    body: "I havent played around with Kdevelop for a while because of the several aformentioned inconvieniences.  I was wondering though if KDevelop has extensive context sensitive support and other code-helpers? Like the kind of integration you would get with Visual Assist with VC++??  Visual Assist IMHO, is a very good model to mimick, as every feature makes _every_ single task you do 10x faster and that much more usefull."
    author: "Jesse"
  - subject: "Vim support"
    date: 2001-04-07
    body: "I hope we gonna get the possibility to use a vim like editor in kDevelop. Would be a nice combination I think.\n\n--\nbram"
    author: "bram"
  - subject: "Re: Vim support"
    date: 2001-04-08
    body: "Agreed.  I use some version of VIM for most of my programming at work and home.  I'd really find IDEs like KDevelop easier to use if they had VI-like keybindings available as options."
    author: "bgb"
  - subject: "Re: Vim support"
    date: 2001-04-09
    body: "Right,  the current editor is great for people how aren't used VIM of EMACS as they can use Ctrl+C/Ctrl+V,...  \nBut onced you tasted vim or emacs you can't live without it anymore.  I really like KDevelop, because of generating makefiles, headers and so on, but for writing I still prefer vi."
    author: "Michael"
  - subject: "Re: Vim support"
    date: 2001-05-10
    body: "I second this. \n\nCTRL-C/CTRL-V... can be real pain in the neck, so to say. I am used to vi and must say that it has multiplied my productivity when I started using it. \n\nEditor in which it is easier to use mouse to copy and paste is just not \"fast\" enough. :)))"
    author: "Lada"
  - subject: "Re: Vim support"
    date: 2001-04-09
    body: "You might get kvim from kdenonbeta/kvim cvs. It is getting there.  It could still use some more developers. Right now the interfaces for making kvim a kpart are there, but they are not hooked into kvim.\n\nOnce kvim implements these interfaces it will be much closer to being able to be used by kdevelop.\n\nOther then that kvim is still buggy, but has most of the functionalty of vim.\n\nceler"
    author: "celer"
  - subject: "Great!"
    date: 2001-04-07
    body: "Cool! Does this mean KDevelop can be fully integrated with Gnome technologies like Glade?\n\nAnd what about automake/autoconf integration?\nI never liked KDevelop's autoconf/automake system.\nIt caused a lot of compilation problems on other systems the last time I used KDevelop."
    author: "Kirby"
  - subject: "Re: Great!"
    date: 2001-04-07
    body: "Bernd has done interesting stuff with automake. Whereas KDevelop 1.x/2.x uses its own project file to generate the Makefile.am's, KDevelop 3.0 is driven directly from automake .am files. So if you understand automake, I would say you'll like it, but on the other if you want to be hidden from it you might not. Not all projects have to be automake ones - that is just a plugin.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Great!"
    date: 2001-04-07
    body: "I'm using KDevelop only as a multi-file editor because it has its own project management ( I prefer to edit makefiles by hand ) and a horrible self-closing class-tree. Project management based only on makefiles is a great idea but what with the second point ? Does KDevelop 3.0 contain a better class-tree view ?"
    author: "Felek"
  - subject: "Re: Great!"
    date: 2001-04-08
    body: "Well, it contains a better class-tree view, which\ne.g. properly supports classes with the same name\nin different namespaces. It also supports browsing\nthe namespace hierarchy, and showing the\nfully class names with full scopes.\n\nCurrently, it still builds the tree from scratch\nwhenever the source info database has changed.\nHowever, there is no design problem with \nimplementing this. I'm sure you could do it in\ntwenty minutes."
    author: "Bernd Gehrmann"
  - subject: ":-D"
    date: 2001-04-09
    body: "FINALLY! You don't know how long I've been waiting for this! :)"
    author: "Kirby"
  - subject: "Does it support Emacs-like searching"
    date: 2001-04-08
    body: "The reason I don't use any KDE editor yet is that I like the Emacs-style searching. You type the word you search for and Emacs stars searching immediately. Often you don't have to type the whole word. In KDE editors you have to type the whole word i a dialog box and then search. That irritates me. Just as bad as when I used Windows."
    author: "Erik"
  - subject: "Re: Does it support Emacs-like searching"
    date: 2001-04-09
    body: "Sure, that's a feature I cannot live without :-)\nThat's why I have implemented it for KDevelop :-))\n<br><br>\nHave a look at\n<a href=\"ftp://ftp.kdevelop.org/incoming/gideon1.png\">gideon1.png</a>"
    author: "Bernd Gehrmann"
  - subject: "Re: Does it support Emacs-like searching"
    date: 2001-04-09
    body: "Great! :-)\nLooks just like in Emacs (except that Emacs puts a space after the kolon, but that is trivial) but with the superior GUI of KDE. I will definitely give it a try when it is released as stable.\n\nDid you implement this functionality in normal kwrite as well? I mean, it could be very useful when editing files in general, not only sourcecode. Or should I replace Emacs with KDevelop for allround text editing as well?"
    author: "Erik"
  - subject: "superior GUI?"
    date: 2001-04-09
    body: "Superiority isn't necessarily in appearance.  In fact, one of the most important features of XEmacs is its set of key bindings and its window management: you can do everything quickly and efficiently from the keyboard, and windows get created and disappear with minimum intrusiveness.  I think KDevelop would do well to\ntry an emulate some of those features."
    author: "Mike M."
  - subject: "Re: superior GUI?"
    date: 2001-04-09
    body: "I was thinking about GNU Emacs and it's grotty window placement and resizing behaviour. I also hate it's undo feature. That's why I want to switch to a KDE editor eventually, because KDE applications got both these things right.\n\nI have not tried Xemacs, but I disagree that all keybindings are superior. For example that I have to press 2 shortcuts to save instead of 1 like in KDE applications."
    author: "Erik"
  - subject: "Re: superior GUI?"
    date: 2001-04-09
    body: "Sigh. Why don't you, instead of talking, just\ntry the HEAD branch or at least look at the\nscreenshots. Then you may come back with\nconstructive criticism."
    author: "Bernd Gehrmann"
  - subject: "Re: Does it support Emacs-like searching"
    date: 2001-04-09
    body: "The 'normal' old kwrite is not maintained anymore\nand its successor is more or less a subset of\nkdevelop. For use as an 'allround text editing\ntool', we would certainly want a little plugin\nmanager that allows you to switch off things like\nthe documentation tree which you don't need then\nand which just clutters the gui if you don't it.\n\nTo the 'replace Emacs with KDevelop' I would say\nit depends on what you expect from an editing\nenvironment. KDevelop will certainly never compete\nwith Emacs in terms of features which can be better \nimplemented in a separate application. So\nit will certainly never be a mail reader or an\neliza clone. In my experience, most (but not all)\npeople prefer emacs because of only a small number\nof features which includes a) incremental search,\nb) scripting and c) indentation. We now have\na) and a starting point for b). It would be nice\nif someone implemented c)."
    author: "Bernd Gehrmann"
  - subject: "Target platform interface ?"
    date: 2001-04-09
    body: "It would be nice if it could be prepared so the it can be used as an IDE/debugger for a target platform, connected via serial bus for instance.\n\nIt if was prepared for this, chip vendors would have a standard platform to write connection modules for their development boards.\n\nThis would, more than anything else boost Linux as an embedded system development platform."
    author: "Johan"
  - subject: "Re: Target platform interface ?"
    date: 2001-04-09
    body: "If you and others want to implement this, you are\nwelcome. AFAICT, none of the current developers\nhas any idea which issues are involved here."
    author: "Bernd Gehrmann"
  - subject: "Re: KDevelop 3.0 Now Developer-Ready"
    date: 2001-04-12
    body: "Two idle things\n1) Why Gideon? Just curious...\n2) I'd like to register my vote in favour of strong (EMACS/ViM-style) code-editing basics. I'm sorry, but I think Visual Assist is silly :-), and only feasible in VBasic..."
    author: "C. D. Thompson-Walsh"
  - subject: "What would be really cool.."
    date: 2001-06-10
    body: "..would be if they would gut the code from vim to handle editing.  Since most editors like vim/emacs work on buffers now anyway, how hard would it be to make a 'virtual buffer' layer that looks like a standard buffer to the original editor code but manages a editing window instead?  It would sure make a damn nice KDE widget.."
    author: "Programmer guy"
---
<a href="mailto:bernd@physik.hu-berlin.de">Bernd Gehrmann</a> writes: <i>"As you may have <a href="http://lists.kde.org/?l=kde-cvs&m=98640430414136&w=2">noticed</a>, the HEAD branch of <a href="http://www.kdevelop.org/">KDevelop</a> now
contains Gideon which is very near to being usable and complete. This is not just another version of KDevelop, it's a big step forward: it's modular, it's extensible, it can support multiple languages, it has a project management feature that is usable for non-trivial projects. Gideon can be thought of as a  a modular lego-like framework for an IDE
that could be used to build a C++ IDE, a Python IDE, or even a web
development platform. A complete C++ IDE is already in place."</i> <a href="http://apps.kde.com/na/2/info/id/1068">Gideon</a> is the codename for KDevelop 3.0 and is already highly usable. This is your chance to <a href="http://www.kdevelop.org/index.html?filename=mailinglist.html">get involved</a> whether by contributing plugins or new features such as Python scripting. For more details, you can also check out Bernd's <a href="http://lists.kde.org/?l=kde-core-devel&m=98598814600661&w=2">initial email</a> as well as the screenshots (<a href="ftp://ftp.kdevelop.org/incoming/gideon1.png">1</a>, <a href="ftp://ftp.kdevelop.org/incoming/gideon2.png">2</a>, <a href="ftp://ftp.kdevelop.org/incoming/gideon3.png">3</a>, <a href="ftp://ftp.kdevelop.org/incoming/gideon4.png">4</a>, <a href="ftp://ftp.kdevelop.org/incoming/gideon5.png">5</a>).  Note that KDevelop 2.0 is still based on the KDevelop 1.4 branch and is <a href="http://lists.kde.org/?l=kdevelop&m=98352993506471&w=2">scheduled</a> to ship with KDE 2.2. <b>Update: 04/07 02:04 PM</b> by <a href="mailto:navindra@kde.org">N</a>: According to KDE hacker <a href="mailto:Richard_Dale@tipitina.demon.co.uk">Richard Dale</a>, Gideon now also <a href="http://dot.kde.org/986594487/986607333/986664758/986666615/">includes</a> Java and Objective-C <a href="http://lists.kde.org/?l=kde-cvs&m=98655952428862&w=2">support</a>.



<!--break-->
