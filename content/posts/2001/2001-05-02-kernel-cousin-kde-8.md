---
title: "Kernel Cousin KDE #8"
date:    2001-05-02
authors:
  - "numanee"
slug:    kernel-cousin-kde-8
comments:
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-02
    body: "I would like the developpers of KMail to install and take a look at the features and behaviour of the email program called Calypso (http://www.calypsoemail.com). I think they got most of the things right. I amusing it for the last couple of years and I receive about hundred email per day to my 4 main email accounts. I am very happy with calypso. Unfortunately it only runs on Windows.\nDon't just follow microsoft's crappy email client as competition and cutting edge, where there are much better programs around."
    author: "Solhell"
  - subject: "KMail"
    date: 2001-05-02
    body: "I love KDE, I like KMail. They did a pretty damn good job so far.\n\nStill, I wouldn't want to part from my PMMail (for OS/2).\nNot only because of the coloured folder icons on new mail."
    author: "moppel"
  - subject: "Re: KMail"
    date: 2001-05-03
    body: "I'm running KDE 2.2 CVS and it shows an open folder, the name in bold and the count of unread mails by the folder. Plus there is now more extensive filtering and message scoring... I'm trying to remember the name of the mail program I used with OS/2 but so far the only thing I seem to find missing is the ability to make mail groups, unless I missed it. It was handy to mail people by groups."
    author: "Eric Laffoon"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-02
    body: "Please, do not integrate KNode and KMail.\nSeparates are the best for me, thank you."
    author: "reihal"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-02
    body: "mmh, then how about doing it the Netscape Communicator-on-Windows-way? You could start \"KMessenger -mail\" to get the kmail interface, \"kmessenger -news\" to get the knode interface, and \"kmessenger\" to start kmessenger ;)\n\nI guess my comment makes it obvious, though, that I am not a programmer ;)"
    author: "me"
  - subject: "Integrate Kmail and Knode?"
    date: 2001-05-02
    body: "I really think this is the best approch.  I can see the advantage of integrating the two apps so that they can share code, etc., but it would be a good idea to be able to bring it up in mail or news mode so that you only see the interface you need.\n\nFor those that use both Kmail and Knode, the combined interface would be nice, but those who only use one or the other don't need to clutter the interface with functions they will never use.\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: Integrate Kmail and Knode?"
    date: 2001-05-03
    body: "A better solution for kde as as awhole would be for knode and kmail to join forces with the Aethera project. Then we would have a good multipurpose mail application.\n\nCraig"
    author: "Craig"
  - subject: "Re: Integrate Kmail and Knode?"
    date: 2001-05-06
    body: "With KOrganize seems to like OUtlook 2000.\n\nAlso Korganize,Kmail and Knode will be one application. It will be part of Office.\n\nIs it better idea ?"
    author: "Edward Tie"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-03
    body: "Why not just use common libaries to share the code? And if the clients were made lightweight enough, they could be designed so they could work well together (and due to their relance and share memory via. libaries they would be fast and small)."
    author: "AArthur"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-03
    body: ">Why not just use common libaries to share the code?\n\nHelp is underway. Namely libkdenetwork :)\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-04
    body: "I think this is by far the sanest idea for integrating KNode and KMail. Just integrate all common code step-by-step into libkdenetwork, until kmail and knode are just thin clients to the library. This has two advantages:\n\n - Stepwise development,\n - Less memory usage if both are open,\n - Less code-duplication -> less bugs.\n\nNot that there are that many bugs, but you get the idea. After this has been done, THEN we can look again at making KMail and KNode one app, or leaving them, or creating a new app altogether.\n\n(b.t.w. - KMail and KNode rock :)"
    author: "Theo van Klaveren"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-02
    body: "What would be the chances of getting the new font installer/manager, the new print architecture *and* some level of integration between the two in the 2.2 release of KDE?\n\nMy God, if this were the case, KDE + *nix would have made a *giant* stride towards becoming a truly viable desktop in the workplace!  Not only that, but I'd be a step closer to be able to dump Win98SE from my main box!\n\nNow, if we could just get Corel to port/update its applications to the latest and greatest of KDE (2.2+), as well as Macromedia, Adobe and others (like Apple and QuickTime?)... (Don't hold your breath for M$)  Remember, for Joe Six-pack, Suzy Secretary,  Pointy-Haired CIOs and other directors, if it can't be seen shrinked-wrapped on a store shelf, it does not exist.  The platform *is* getting there, we only need the shrink-wrapped apps to convince the masses that *nix is a viable option to M$."
    author: "Bruno Majewski"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-02
    body: "Hmm, I'm not sure as to what you mean by fontmanager/kprinter integration. X handles the actual fonts, the fontmanager is just a frontend.\n\nIch, Quicktime is the most proprietarily awful toolkit in the universe. I wouldn't mind a plugin, but Quicktime itself is a bad thing for a non-apple OS to become at all reliant on.\n\nShrink-wrapped is good, but OSS and included with KDE is better."
    author: "Carbon"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-02
    body: "Looking at my response, it seems a little harsh. I see where you are coming from, and agree that shrink-wrapped is the way to go for highly specialized, higly complx stuff (such as ultra-professional 3d modelling). This sort of thing doesn't fit with the OSS model of doing things in pieces. However, smaller, more common computer applications, such as \n\n<ul>\n<li>smaller 3d modelling tasks (think Bryce)\n<li>groupware (aka Outlook)\n<li>movie and audio codecs (such as Quicktime) \n</ul>\n\nIMHO are much better off open sourced, when on an open source platform."
    author: "Carbon"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-02
    body: "About font-management & printing management integration: I'll have to dig back to find the implications, but I do remember that this can be an issue when doing DTP and whatnot.  It escapes me, right now, I'll have to think what were the issues.  (MacOS-centric sites did have a few items & discussions on the topic, maybe this might be a good place to start looking.)\n\nHmm, you're the first that says (if I understand you correctly) that QuickTime is badly architected. Yes, it is proprietary, but it is widely used and it would be nice if *nix could support it.\n\nThe OSS movement has produced some software of amazing quality (KDE itself is a good example), but like I said, if people can't see it at the store, it does not exist. (If I had a dime for every time I've heard \"there's not software for Unix\", I'd be rich.) They know more about Corel & M$ than, say, Abiword, KSpread, etc. Your averange computer/software buyer wants to start with stuff they recognize: do not understimate this. That's why I'd like to see some shrink-wrapped software from the big names, that's what will bring in new users (amongst other things)... and free those of us from having to keep a WinXX partition on our machines."
    author: "Bruno Majewski"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-03
    body: "Check out what the Kompany is doing their going to be putting good apps on the shelf for kde.\n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-03
    body: "Yep, and what's really great about theKompany is that all their desktop apps so far (except for Kapital) have been totally or partially Open Source!\n\nWay to go, tK!"
    author: "Carbon"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-02
    body: "About font-management & printing management integration: I'll have to dig back to find the implications, but I do remember that this can be an issue when doing DTP and whatnot.  It escapes me, right now, I'll have to think what were the issues.  (MacOS-centric sites did have a few items & discussions on the topic, maybe this might be a good place to start looking.)\n\nHmm, you're the first that says (if I understand you correctly) that QuickTime is badly architected. Yes, it is proprietary, but it is widely used and it would be nice if *nix could support it.\n\nThe OSS movement has produced some software of amazing quality (KDE itself is a good example), but like I said, if people can't see it at the store, it does not exist. (If I had a dime for every time I've heard \"there's not software for Unix\", I'd be rich.) They know more about Corel & M$ than, say, Abiword, KSpread, etc. Your averange computer/software buyer wants to start with stuff they recognize: do not understimate this. That's why I'd like to see some shrink-wrapped software from the big names, that's what will bring in new users (amongst other things)... and free those of us from having to keep a WinXX partition on our machines."
    author: "Bruno Majewski"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-03
    body: "I wasn't saying that Quicktime was badly done, so much that it's ultra-closed liscense makes it extremely bad for OSS software. It's popular, it has it's own format, but Apple is very tightfisted with it.\n\nAt the risk of sounding like a troll, I'd like to say about the problem with people not seeing it unless its shrink-wrapped : who cares? If the user refuses to look at it unless its closed, that's no reason for us to make it closed, rather, that's more reason for us to make our free-as-in-beer work more publically known!\n\nDon't take this the wrong way : proprietary software is good, and users and enterprises noticing KDE is very good. In addition to the examples listed in my previous response, another good place for proprietary software is games. However, other proprietary and popular windows apps can and are being redone as Open Source, and this is good.\n\nBesides, when proprietary vendors start having Open Source competitors nipping at their heels, that will just force them to work harder to make better products to keep up! (At least theoretically, didn't work for Windows 2k web server, which currently sucks chicken-of-the-sea eggs compared to UNIX/Apache  :^)"
    author: "Carbon"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-03
    body: "Kmail is a great single purpose email client. However i don't think the UI is set up right to become a multipurpose email client. Look at evolution. It just looks very sharp. We need a multipurpose client to go head to head with it. Kmail is cool for what it is but i think the community would be better served if the talent at the kmail project and the knode project would join forces with the people working on the Aethera project. Then we would have a good multipurpose client that would meet the needs of the kde users and go up agaist the Xiamian folks who have done a great job on evolution.\n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-03
    body: ">> However i don't think the UI is set up right to become a multipurpose email client. Look at evolution. It just looks very sharp.\n\nDo me a favor... load up the KOffice shell (koshell), and picture the same thing with KMail, KOrganizer, and KNotes listed down the left side, with the programs embedding themselves just like KWord, KSpread, etc. do in KOffice shell.\n\nIs that a starting point for what you want?  Can you (or anyone else) see why that wouldn't work to create a quick Outlook/Evolution/Notes, etc. clone with available source?  \n\nThe things that I see missing are some of the nicer advanced features that Athera (or however you spell it) offers, with scripting and full data searches.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-03
    body: "Someone already did that it looked cheap. They just combined everything like that. I wish i could remember the app. It showed up on apps acouple of months ago. Athera is really the best way for kde to go. It looks very nice. Most of us will use it instead of kmail anyway. Seems like we'd all benefit if they just joined forces. I just hope their's a better reason than pride for not doing it.\n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-03
    body: "i couldn't find anything like that in apps.kde.com. and even if one implementation of this idea was \"cheap\", that doesn't mean all of them will.\n\ni also disagree that aethera will replace kmail for everyone. i would rather use a small mail client that integrates well with news/calendar/addressbook than one big app. perhaps the kmail authors feel the same and that\u00b4s a good motivation for them to keep working on kmail.\n\nalso, i don\u00b4t think they would want to work for a kompany for free when others would do the same amount of hacking but earning a paycheck at the end of the month (that\u00b4s just my guess)."
    author: "Evandro"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-03
    body: "others would do the same amount of hacking but earning a paycheck at the end of the month (that\u00b4s just my guess).\n\nHuh? That happens in kde all the time.\nI was there acouple of months ago but they only had a RH 7 rpm and the tar.gz package would'nt compile on my system so i did'nt get a chance to try it out. I only saw the screen shots. They did just what your talking about though. Aethera is just way more advanced than that. My suspistion is that it will ingregrate well with the calender and address book etc. when its done. Ask shawn what his plans are for the application i'm sure thats in there.\n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-03
    body: "\"Huh? That happens in kde all the time.\"\n\nno it doesn't. when a kde hacker is getting paid by suse, for example, he/she is getting paid to work on kde, not suse linux.\n\nin your suggestion, the kmail hackers would work for a company on the company's products. sure, it's free software, but i guess the kmail hackers would prefer if theKompany worked with them in kmail.\n\nas for aethera, it will indeed be what i want but in one big application. i'd rather have separated applications with one other app that integrates them. the technology behing KDE 2 allows that to be done easily, as it was done with KOffice."
    author: "Evandro"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-03
    body: "I would rather have a nice looking intregrated application that does what i want it to. As far as small once you crame all the differnt apps together your not going to get a reduction in space. Aethera is'nt that big. I think the kompany is working for kde with aethera. So i don't see your point. We are getting a free in every sense of the word mail client. Kmail will start to become a liability for kde. Damn look at evolution again. kmail is an ugly little app. It was great in its time back in the kde 1 days but the linux desktop has advanced and Kmail has'nt. As far as the ui is concerned. Most of us will be useing Aethera anyway but it would be nice if the community pitched in instead of shawn forking out thousands of dollars for kde and not getting crap in return as far as i see it. I'm sure there are those helping out his efforts and as far as you guys go us regular desktop users applaud you. \n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-04
    body: "i don't think kmail is ugly, i think it's a great mail client. and if they kmail authors think so too they should continue to work on it instead of something they don't like (one app that does it all). it's not about pride."
    author: "Evandro"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-04
    body: "Its ugly in comparison with evolution. Its fine for a little simpile mail client but now their talking combining it with knode. I think we'd all be better served if that team combined with the aethera team and kicked out a awesome full featured app that we all could enjoy with pride. Kmail is a bland little orange app that was good for it day but the stacks have been raised by the Xiamian folks and its time to responde. Pretending that Kmail can compete with evulotion is foolish. It must suck to have put all that work into kmail and have it ditched. So no ones wants to bring it up because the Kmail folks have put alot of hard work into it. But come on these are'nt the kde 1.0 days. Althera the future of mail clients for kde. Its tempting to be PC and keep Kmail but evolution is going to be kicking is ass all over the desktop. \n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-17
    body: "Hmmm... For those of us who have had it with Windoze Kmail is *great*.. i never liked Outlook because it had to many buttons/functions. Kmail is such a nice little program, that is so easy to use. It requires less then zero brainwrok to figgure out. Kmail is to Althera what Kwrite is to Kword. So lets keep it... no one would kill Kwrite just because Kword is in 1.0... right? maybe Althera should be on the kicker by default and not Kmail... but dont stop working on it just yet. It still has some issues and it still has its justification.\n\n/kidcat"
    author: "kidcat"
  - subject: "KMail INTEGRATES!"
    date: 2001-05-04
    body: "Aethera doesn't. Its interface looks very different than the rest of KDE, and it has many helper apps that act like KMail. Imagine your KDE Desktop as an MDI window, open up korganizer, kmail, and kab... tada, an integrated, all-same-looking-and-acting set of apps... awesome!\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: KMail INTEGRATES!"
    date: 2001-05-04
    body: "I think it fits in just nicely. If you have'nt installed the latestest beta then at least check out their screen shots. http://www.thekompany.com/projects/aethera/screenshots.php3\nIt would take along time to get kmail up to par with aethera. It would just make better since to have the different projects join forces to make a world class email application. Aethera is almost there. Kmail is just to far behind the curve.\n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-15
    body: "The app i was talking about just released the next version today. It looks like crap. Its not even in the same league as evolution. Look on app.kde.com under infusion.\n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-05
    body: "TheKompany should not have developed a seperate application from Kmail or fork the Aethra code just do they have better control of development.  We could have a almost perfect E-mail client now if it wasen't for that."
    author: "John Poter"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-05
    body: "I wish theKompany would just go away."
    author: "ac"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-05
    body: "In my option they had to. We need another player. Kamail was cool a year ago but kde is avanceing so quickly that Kamail now seems out of place. Aethera on the other hand will be a email client that will do justice to the rest of kde.\n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-05
    body: "Yea man i wish all the people that make up the kde community would go away. That makes so little sense that I'm sure that your embarrassed after haveing said it.\n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-06
    body: "No offense to the Kmail guys, but the code has been hacked up to hell and back and doesn't provide the framework for what we needed to do.  I can understand your opinion because what we are doing with Aethera ultimately is not obvious at the moment, I think it will make more sense in late summer.  Our objective goes far beyond a simple email client.  Keep watching, it will all make sense soon."
    author: "Shawn Gordon"
  - subject: "Re: Kernel Cousin KDE #8"
    date: 2001-05-07
    body: "Huh?  What have you got up your sleeve?  Whatever it is, it sounds cool.  Are you going for a total Outlook/Exchange server killer? \n\nTheKompany has been doing great stuff.  I've been plugging Kapital in the article about the KDE feature poll (a couple of people said \"I wish there was a Quicken clone for KDE that could import Quicken files!\").  Keep up the good work!"
    author: "not me"
---
In this week's <A HREF="http://kt.zork.net/kde/latest.html">KC KDE</A>, <A HREF="mailto:aseigo@mountlinux.com">Aaron J. Seigo</A> covers everything from <a href="http://korganizer.kde.org/">KOrganizer</a>'s architecture improvements, improved message filtering in <a href="http://kmail.kde.org">KMail</a>, the new KDE configuration file upgrading mechanism, to the exciting new <a href="http://www.cpdrummond.uklinux.net/kfontinst-new/">KDE font installer</a>.  Get this, and much more, <a href="http://kt.zork.net/kde/kde20010427_8.html">here</a>.