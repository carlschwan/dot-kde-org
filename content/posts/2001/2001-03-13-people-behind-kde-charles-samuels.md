---
title: "People behind KDE: Charles Samuels"
date:    2001-03-13
authors:
  - "Inorog"
slug:    people-behind-kde-charles-samuels
comments:
  - subject: "Re: People behind KDE: Charles Samuels"
    date: 2001-03-13
    body: "Makes me fell like a piece of crap 28 year old with no codeing skills.\n\nCraig"
    author: "Craig black"
  - subject: "Re: People behind KDE: Charles Samuels"
    date: 2001-05-10
    body: "To cod at 16 is nice, but it is not too late for you to start coding too. Good luck"
    author: "Googel"
  - subject: "Re: People behind KDE: Charles Samuels"
    date: 2001-03-13
    body: "is it possible to use esound or oss instead of arts with noatun?"
    author: "mre"
  - subject: "I agree"
    date: 2001-03-14
    body: "I get lots of freezes (in noatun)while trying to play mp3.  Killing noatun and restarting it = no sound.  I suspect these problems are arts related.  Anyone know how to kill arts and restart it?"
    author: "Rimmer"
  - subject: "Re: I agree"
    date: 2001-03-15
    body: "Yeah, just go to the \"sound server\" part of KControl, uncheck and then re-check a box, and click \"apply.\"  That will restart aRts.  However, if you are having problems with aRts, simply killing and restarting it will not do much good.  Playing with the response time slider helped a lot for me.\n\naRts isn't perfect yet, hopefully in the future it will become bug-free."
    author: "not me"
  - subject: "Re: People behind KDE: Charles Samuels"
    date: 2001-03-14
    body: "I agree it is really a shame that kmysql has been abandoned. It means we have to maintain kde1 compatibility forever."
    author: "Erik Kj\u00e6r Pedersen"
  - subject: "Re: People behind KDE: Charles Samuels"
    date: 2001-03-14
    body: "check out http://www.ksql.org"
    author: "Tony Johnson"
  - subject: "Re: People behind KDE: Charles Samuels"
    date: 2001-03-14
    body: "Appears to be pretty inactive. I hope I am wrong though\n\nErik Kj\u00e6r Pedersen"
    author: "Erik Kj\u00e6r Pedersen"
---
<a href="http://www.kde.org/people/charles.html">Charles Samuels</a> is this week's subject for the popular series of questions from <a href="mailto:tink@kde.org">Tink</a> in the not less famous <a href="http://www.kde.org/people/people.html">People...</a> series of interviews. Here is your chance to learn a bit about the author of the multimedia swiss army knife that comes with KDE-2.x, popularly known as <a href="http://noatun.kde.org/">Noatun</a>.  Charles notes that Jason Katz-Brown knows how to <A HREF="http://www.kde.org/people/images/me.wav">pronounce</A> this unforgettable name.


<!--break-->
