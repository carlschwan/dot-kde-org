---
title: "KDE Theme-Building Tutorial"
date:    2001-04-12
authors:
  - "Frank"
slug:    kde-theme-building-tutorial
comments:
  - subject: "IBM sponsors a KDE Theme Contest"
    date: 2001-04-11
    body: "The tutorial is nice but, IBM also sponsors a theme contest - that is IMHO even more important and deserves some advertisement.\n\nhttp://www-106.ibm.com/developerworks/linux/library/l-kde-c/"
    author: "Wilhelm Schlegel"
  - subject: "Re: IBM sponsors a KDE Theme Contest"
    date: 2001-04-11
    body: "Ya the lack of good kde 2 themes is one of the few complaints I have with kde. You can easily make themes that make gnome/sawfish look like crap, but themers have just been slow on the uptake. Maybe I'll have a look at the tutorial and see if I can get anything going."
    author: "richie123"
  - subject: "Re: IBM sponsors a KDE Theme Contest"
    date: 2001-04-11
    body: "Hmmm, maybe someone should talk to IBM that KDE 1.x is history and this article is not really interesting.\n\n1) Yes, I logged in with Konq, not with Netscape or IE and it worked (surprise, surprise!)\n\n2) This \"tutorial\" has six section. KDE-2 themes start at section 5 (section 6 is feedback) and has no info.\n\n3) From the contest page:\n\"Themes must be original, previously unpublished, and compliant with KDE 1.1.x or 1.2 and the KDE Theme Manager; non-compliant themes will not be judged.\"\n\nThank you IBM, this tutorial is too late."
    author: "Thorsten Schnebeck"
  - subject: "Re: IBM sponsors a KDE Theme Contest"
    date: 2001-04-11
    body: "Unbelievable, we're way beyond the 2.0 release already and they start a contest for KDE 1.\n\nWell, I couldn't have entered myself anyway, as you have to be a US citizen in order to enter the contest."
    author: "Jelmer Feenstra"
  - subject: "Re: IBM sponsors a KDE Theme Contest"
    date: 2001-04-12
    body: "I use KDE 1 because my computer is slow(no I can't upgrade my computer, I live in a poor country) it's good to see that KDE 1 is still somewhat supported.  The latter KDEs are almost usable though, it'll be good if KDE 2.2 will run good in 16 megs of memory."
    author: "KDE1.x user"
  - subject: "Re: IBM sponsors a KDE Theme Contest"
    date: 2001-04-19
    body: "With only 16MB of memory, you're better off sticking to a char-cell only environment.  You can't seriously expect a modern GUI to run effectively in just that.  Even if it did there would be no memory left for apps.\n\nI'm surprised you even find KDE 1.x usable.\n\nThere are a whole range of good char-cell apps you can use for email, editing, browsing the web (w3m), reading newsgroups, etc, etc.  You don't have to use a GUI to be productive when using a GUI clearly doesn't make much sense.   At least with Linux you have the choice."
    author: "Macka"
  - subject: "Re: IBM sponsors a KDE Theme Contest"
    date: 2001-04-12
    body: "1.2?  Was there ever a 1.2 release?\n\nI know there was a 1.1.2.  Maybe that is what they mean?\n\nEither way, it is a worthless contest if they do not accept kde2 themes.\n\nTick"
    author: "Tick"
  - subject: "Re: IBM sponsors a KDE Theme Contest"
    date: 2001-04-12
    body: "I looked at kdebase cvs-tags:\n\nThere is no 1.2-tag! Quite hard being compatible ;-)\n\nBye"
    author: "Thorsten Schnebeck"
  - subject: "Speaking of themes..."
    date: 2001-04-11
    body: "I get a crash trying to use the Legacy Theme Importer with the marble3D GTK theme (search for it on gtk.t.o if you're interested). The error message says something like \"GTKCurve: unknown object\" (sorry, not in front of the relevant computer atm) and KControl crashes. I had to delete my .kde/share/config/kstylerc (or something, again this is from memory) to be able to get into KControl again.\n\nDoes anyone know what I can do, if anything, to get this theme working with klegacyimport? Also, it would be nice if trying to use a broken/unsupported legacy theme resulted in graceful degradation, rather than a sudden and unrecoverable crash that requires configuration editing to recover...\n\nThanks,\nStuart."
    author: "Stuart"
  - subject: "Re: Speaking of themes..."
    date: 2001-04-11
    body: "Why are you trying to use Gtk's Marble theme when KDE comes with one?  I use KDE's marble theme, and I have Marble3D selected as my Gtk theme.  I wouldn't want to use Gtk's Marble theme in KDE because it has less intuitive widgets and would be slower."
    author: "David Walser"
  - subject: "Re: Speaking of themes..."
    date: 2001-04-12
    body: "So true... Yet you are completely missing the point."
    author: "BnW"
  - subject: "Send them to themes.org!!"
    date: 2001-04-11
    body: "...and if you make anything you even suspect other uses might enjoy, submit it to themes.org!"
    author: "Otter"
  - subject: "Browser requirements?"
    date: 2001-04-11
    body: "\"You will need Netscape 4.x or higher, or Internet Explorer 4.x or higher, with JavaScript enabled.\"\n\nI hate to nitpick, but aren't messages like these getting a little old?  It's especially strange since it's a page about KDE.  They should at least have the decency to mention Konqueror. :P\n\nIn contrast, I absolutely *love* what our site designer put on our company's website (affinix.com) in the \"About\" section:\n\n\"This page is best viewed with a monitor.\"\n\n-Justin"
    author: "Justin"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-12
    body: "Uh?\n\nFirst thought: Yeah, finally KDE will look nice!\n\nSecond thought: What? KDE 1?\n\nThis is... weird. :)\nOne of the reason, I don't like KDE 2 currently, is the lack of a really nice theme. Some month ago I thought there would be a lot of new and great themes too... no... nothing. Disappointing. :(\nThis contest would be a great opportunity and it's for KDE 1... would be amusing, if it wouldn't be so sad. :)"
    author: "Spark"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-12
    body: "Yeah, right... and nowhere in the deep spaces of the internet is a good tutorial for making themes... but if you're interested to design your own theme, i can help you! i worked a lot on this stuff and believe me, it's so great... there is near to nothing you cannot change.\nGOOD JOB, KDE-Team!!!\ngot you? - write me"
    author: "dee"
  - subject: "KDE2 is covered"
    date: 2001-04-12
    body: "But only at the end, read the last pages for more information.  Still, pretty nice effort by IBM."
    author: "KDE User"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-12
    body: "I was mostly speaking of the contest. I'm sure it will produce some great themes. But for KDE 1. D'oh! :)"
    author: "Spark"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-12
    body: "Nothing special. It is good for newbies, but for serious themeing it lacks a lot of crucial informations. And it is written more for KDE 1xx than for KDE 2xx.\nFirst, Theme manager module in Kcontrol doesn't have all functions it had in KDE 1xx. Colours? Not a big deal. Colours can easily be changed through Colours module in Kcontrol. Wallpapers? Same story: Background module in Kcontrol. Icons? Very few themers have time and skills to create a set of original icons. Those I've seen in some KDE themes are ugly. And now we come to Window decorations. Hm, here we have a problem.\nI've seen nice portings of iceWM and Sawfish decorations, but Maximize/Minimize button sucks. When you click on Maximize button your themed button disappear and you get a square button like in kde 1xx.\nAnd if you've chosen a pixmap for your titlebar when resizing the window the pixmap change form and becomes ugly (you have to resize it again to fix it).\nThat is a little bit about *.ktheme format.\n\nAnother story is Styles. Themeing of Toolbars in kde is pain in the arxx. That's because position of toolbar icons is on toolbar buttons and not on the toolbar. If you choose a pixmap for a toolbar, and you want to select the same pixmap for toolbar buttons, the pixmap has to be plain (no gradient, and no complex pixmaps), otherwise pixmap applied on toolbar and pixmap applied on toolbar button wont match (and you also get stripes around toolbar buttons, so you are forced to apply highlight to avoid that).\nAnd since kde 1.98 (I really don't remember, it was before kde 2.0 was released), choosing background for your style sucks. Why? Because it draws the same background on your windows and inside your windows, so you can't use anymore Konqueror menu > View > Background image for Konqueror. So option is simply this: don't use background image in your styles! Why? Performance. Konqueror simply slows down a lot, and on not fast machine it could be a problem (it is problem here on Pentium 200, but my Athlon 800 can live with it).\n\nSome said here that there are not many themes on kde.themes.org. As you probably know kde.themes.org\nhaven't had people to maintain the site and that happend at that time KDE 2.0 was released. So, I remember, I uploaded an theme in august 2000 and it showed up on kde.themes.org 3 months after. I also noticed that guys on kde.themes.org changed some things this week, and some themes pople posted there last few months can't be downloaded anymore (it says those themes have 0 bytes). I mailed maintainers (and asked them about changes) and they don't answer at all.\n\nWhat to say? To theme or not to theme?"
    author: "Antialias"
  - subject: "More on kde.themes.org"
    date: 2001-04-12
    body: "On a related note, I was supposed to become the maintainer for mozilla.themes.org about six months ago and I still have no username/password to get in and set up the site.\n\nIf the maintainers of kde.themes.org are in the same situation, there is little hope of much being updated on the site."
    author: "Andy"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-13
    body: "If you would like to email everything you want to know/have questions with directly to me(jsc@themes.org), i will get on it and answer your questions, I dont remember seeing your mail, but i get quite a few, and i would be more then happy do deal with your thoughts/ideas.\n\nThanks,\n\nJustin Cook\nhttp://kde.themes.org admin"
    author: "Justin Cook"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-18
    body: "Most of the things you mention fit under the catagory of \"fixable bugs\". Please, when you find a bug, if you have some spare time and C++ experience, fix it yourself! Even if you don't, a bug will much more likely be fixed if you report it to bugs.kde.org, which provides the developers some very effective bug organizing tools.\n\n--\nWith Windows, you resinstall about every 6 months. With Linux, you reinstall about every 6 months.\nThe difference is, with Linux, the version numbers change"
    author: "Carbon"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-12
    body: "The one thing that really distresses me about KDE2 theming is the complete lack of a central theme manager. Do I use the decorations? Do I change it in Style? What about in Colors? Or do I use the theme manager? How about I use Gnome, where it's all in one spot, like it *should* be.\n\nThe decoration idea was a good idea, but very VERY poorly implemented, imho. There should be one entry in KControl that controls the entire look and feel of your desktop environment, right down to the last pixel. Right clicking on the title bar and choosing a \"decoration\" style should affect only that window, not the entire window manager!\n\nAnd who's friggin idea was it to make title bar configuration an exercise in C++ coding, and not something that theme artists can do themselves? There are about a dozen themes for KDE2.x on kde.themes.org, and guess how many of them have original title bars? None. Zero. Zip. Zilch. Good idea... NOT. \n\nSo please, get this whole \"theme\" engine together in one spot, and make it so that the theme artists themselves can change the title bars and frames of the windows, like on other (and vastly more popular to themers) window managers. And please allow me to move the buttons (min/max/close/stick/icon) to where *I* want them to be.\n\nThanks for listening, sorry for the rant.\n-Steven"
    author: "Steven Hatfield"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-12
    body: "Please note that I'm making an icewm theme importer for kde, and a titlebar button position selector - fully integrated with kwin, kde and kcontrol. It has customisable button positions, and other nifty features that artists really want.\n\nThe kcontrol module I'm making allows customisation of button title positions via dnd. Similarly, all compatible decorations will be able to have their custom options and other specific decoration settings fully customisable in the one kcontrol place.\n\nSee my site http://gallium.n3.net/ for a preview. Hope this is what you were asking about :)\n\ngallium"
    author: "gallium"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-12
    body: "This is *exactly* what I'm talking about! You have hit the proverbial nail on the head!\n\nIf you need a beta tester, I'm your man!\n\nThanks!\nSteven\n\n(ashari@knightswood.net)"
    author: "Steven Hatfield"
  - subject: "Good job !!"
    date: 2001-04-12
    body: "Yes, I use your themes until some weeks\nI hope this is in Kde 2.2\n\nIf you want to try :\ngo to apps.kde.com\nand search 'kwix'\n\nregards."
    author: "thil"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-12
    body: "A theme contest for KDE1?! Only for US residents?\n\nThat sure is going to help A LOT KDE2..."
    author: "Al_trent"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-12
    body: "Yeah, a contest about themes for such a highly internationalized software as KDE is and that on the internet only for US residents. Guess someone missed something here. I wouldn't participate anyway, but this is discriminating, thanks IBM!"
    author: "Malte"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-12
    body: "I agree Malte, and the contest shouldn't be called KDE Theme Contest but KDE Theme Contest For US Citizens :("
    author: "Antialias"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-12
    body: "Okay, an international contest seems to be rather expensive and difficult wrt. legal issues.\nAs for the version numbers, that's a mistake, the contest _is_ about 2.x"
    author: "Malte"
  - subject: "IBM gone MaD :O)"
    date: 2001-04-12
    body: "It's insane to:\n1. Organize a Theme contest for KDE 1 instead of KDE 2.\n2. And only for Americans when The KDE is mostly develops at Europe.\n\nAnyway, Why can't Mandrake, SuSE, Caldera, etc., think of KDE 2.2 Themes Contest? It will promote their Distro and also encourage people. Who cares :(!?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: IBM gone MaD :O)"
    date: 2001-04-12
    body: "That one with the distributions is a good idea.\nThe distributors would have a chance to gain\nthe heart of gimmick fans out there, while \npromoting their selfs.\nBy the way, why didn't IBM organise a Theme contest for finnisch people (dedicating it to\nLinus Torvald) ?\n\nAbout the IBM contest, i think such kind of ideas\nare ALLWAYS welcome, but this one shows once\nagain that Big Blue did never understand how to\nkeep a OS actual and modern.\n\nDid you already realise that neither http://www.kde.org nor http://kde.themes.org \ndo have a tutorial on themes for KDE 2.X ? Even the installation of a theme can become an adventure!\nI was already about to make some themes, but i\ngave up, because i just can do it on my free hours\nand don\u00b4t have time to try to find how does it\nworks my self.\n\nFilipe"
    author: "Filipe"
  - subject: "Diff. BG between KDE and Gnome developers"
    date: 2001-04-12
    body: "Core KDE developers seems to be engineers, core Gnome developers artists.\n\nIF you want a product made by engineers, choose KDE. If you want a product made by (visual) designers and artists, choose Gnome.\n\nIf unsure, why not compare with choosing a car or a house. A car made by engineers or artists. Or a house made by engineers or artists."
    author: "Loranga"
  - subject: "Re: Diff. BG between KDE and Gnome developers"
    date: 2001-04-14
    body: "I work on GNOME, I have a degree is chemical engineering from UT and I can't draw at all."
    author: "rob"
  - subject: "Importing Window Themes"
    date: 2001-04-12
    body: "Thanks to \"Divide by Zero\" you will now also be able to import themes made for MS Windows in the theme manager.\n\nCheck out KDE 2.2alpha1 next week!\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-12
    body: "BullShit ! \n\nKDE 1 is too old. i dont care about it.\nand there are a lot of theme for it, already.\n\nthats sux, we want more theme for KDE 2.1\n\nwe need more theme for KDE 2.x\n\nalso, if i've well understand, any submitted theme will become the propriarity of IBM.\n\nTHATS REALLY SUX.\nthey will be able to build and sell complete GUI from an artist idea.\n\ndont submit your theme, keep it for a free contest."
    author: "me (smk)"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-13
    body: "IBM appears to have changed (or just clarified) the rules, you don't assign sole ownership to them or any such, you're just required to release under an opensource.org approved license.\n\nThey are accepting themes for both kde1.1.x and kde 2.x.\n\nNobody panic, just read the linked-to-article :-)"
    author: "Kevin Puetz"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-13
    body: "Another thing they should probably change is the deadline. I imagine that a really good theme needs  more time than 2 weeks to make. This way they will limit the number of possible participants (who doesn't have the time right now to do this) and make do with an assemblage of relatively hurried work."
    author: "Wilhelm Schlegel"
  - subject: "Re: KDE Theme-Building Tutorial"
    date: 2001-04-13
    body: "Ooooops, I've just solved the problem with Maximize button on titlebar. No more need for hardcoded kwin clents, now we can theme ::::::)))))))).\nIn a few days I will post detailed tutorial on my web-site\n(but only about *.ktheme format and titlebar decorations)."
    author: "Antialias"
---
This new <a href="http://www-105.ibm.com/developerworks/education.nsf/linux-onlinecourse-bytitle/B390A002F1F75BD186256A29005F94ED?Open&l=kde1,t=gr,p=KDE-Tutorisl">KDE tutorial</a> on <a href="http://www.ibm.com/developerworks/">IBM developerWorks</a> is designed to teach you about KDE themes: Learn how to create, save, load, and share the fundamental look and feel of the KDE environment. After completing this tutorial, you should feel confident in your ability to customize KDE to fit your personal working style. <i>(free registration required)</i>.  <STRONG>Update: 04/11 18:34</STRONG> by <a href="mailto:pour at kde.org">D</a>: <A HREF="mailto:ssdhanoa@us.ibm.com">Shailendra</A> wrote in to inform us of a <a href="http://www-106.ibm.com/developerworks/linux/library/l-kde-c/?open&l=1974,t=gr,p=kcst">KDE Theme Contest</A> currently sponsored by <a href="http://www-106.ibm.com/developerworks/linux/?open&l=1974,t=gr,p=lnxhp">IBM developerWorks</a> in conjunction with the tutorial, with a bounty of $2,000, $1,500 and $500 for the top three submissions.  The contest runs until May 1, 2001, and submissions must be of <EM>original, previously unpublished themes (one entry per person)</EM>.  Note that only KDE 1 themes are eligible.  <STRONG>Update: 04/11 19:54</STRONG> by <a href="mailto:pour at kde.org">D</a>: After a closer look at the rules I discovered that <EM>by submitting your theme you assign your copyright in the theme to IBM developerWorks</EM>.  Also, the contest is only open to US residents.






<!--break-->
