---
title: "Aethera Messaging Client Beta 1"
date:    2001-01-18
authors:
  - "numanee"
slug:    aethera-messaging-client-beta-1
comments:
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-17
    body: "Looks way cool, hats off to the kompany! I'm definately gonna install it when I get home to my kde2 desktop..."
    author: "David Talbot"
  - subject: "Aethera Messaging Client Beta 1 on RedHat 7.0"
    date: 2001-01-17
    body: "have anybody installed aethera on a red hat 7.0 machine? on the kommpany-server doesnt exists rpms for rh7 and the source i can't compile :( can anybody help me?\n\nthanx & best regards\nar"
    author: "ar"
  - subject: "Re: Aethera Messaging Client Beta 1 on RedHat 7.0"
    date: 2001-01-17
    body: "We don't even want to touch RH 7 with all the problems it has.  Hopefully someone can help you with the compile.  I'm sorry your having trouble with it (send RH a reprimand for using the pre-alpha glibc).\n\nIf you do get it compiled and can make an RPM, let us know and we'll put it up with the others."
    author: "Shawn Gordon"
  - subject: "Re: Aethera Messaging Client Beta 1 on RedHat 7.0"
    date: 2001-01-18
    body: "Hi,<br><br>\n\nI just got the sources and compiled Aethera on my RedHat 7.0 System. First of all: WOW This looks way cool. Good Job. I'm sure it will eventually be the program of choice for all my mail thigs. Got to play with it a little more, though. <br>\n\nCompiling on RH isn't that much of a problem. At least ist wasn't for me. There are two files that need to be changed. Only #include stdlib.h (the brackets are missing. I wish I knew, how to quote them to get them through the html filter...) has to be added in the following files:<br>\nvtimezonecomponent.cpp <br>\nsetupwizard.cpp <br>\n\nI use RedHat 7 fully up2date and compiled Aethera with KDE2.1 from CVS 17 Jan 2001 - 17:00 CET. I encountered no further problems and the application seems to run quite stable.<br>\n\nOnce more, congratulations and a huge thank you to the kompany for once again bringing us great apps!!!<br><br>\n\nHmm I never made any RPMs myself but if wasn't too much of a problem I would happily provide some if someone could tell me how to do it.<br><br>\n\nCheers,<br><br>\n\nRichard\n"
    author: "Richard Stvens"
  - subject: "Re: Aethera Messaging Client Beta 1 on RedHat 7.0"
    date: 2001-01-18
    body: "I'd like to build KDE2.1 on RH7.0 also, but I don't know\nwhat are the best configure settings for this environment.  Could you give me hints at how you\nbuilt KDE (and also, did you build qt?).\n\nThanks."
    author: "Neal Becker"
  - subject: "Re: Aethera Messaging Client Beta 1 on RedHat 7.0"
    date: 2001-01-19
    body: "Did the mail I sent you regarding your question arrive?"
    author: "RIchard Stevens"
  - subject: "Re: Aethera Messaging Client Beta 1 on RedHat 7.0"
    date: 2001-01-19
    body: "sorry; there is no message in my inbox :("
    author: "ar"
  - subject: "Re: Aethera Messaging Client Beta 1 on RedHat 7.0"
    date: 2001-01-18
    body: "Please stop this FUD.  There is nothing wrong with RH7.  The gcc-2.96 was the best quality available.  All gcc have bugs.  gcc-2.96 just has different bugs than gcc-2.95.\n\ngcc has an extensive regression test suite.  The quality is known.  gcc-2.96 was used (I believe) to build all of RH7's 3 CDs.\n\nI use C++ heavily, every day.  The current 2.96 is the least buggy available at this time.\n\n\"pre-alpha glibc????\" what are you talking about?"
    author: "Neal Becker"
  - subject: "Re: Aethera Messaging Client Beta 1 on RedHat 7.0"
    date: 2001-01-18
    body: "<i>Please stop this FUD. There is nothing wrong with RH7. The gcc-2.96 was the best quality available. </i><p>\nI wouldn't call it FUD when the gcc development team (including those that actaully work for Red Hat) say that it was a stupid thing to do, was against their wishes, and that it should not have been done.<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Aethera Messaging Client Beta 1 on RedHat 7.0"
    date: 2001-01-19
    body: "Bull*!\nNone said it was stupid. It was a tough decision.\nEvery change Redhat made to gcc-2.96 went back to CVS. But in the end it was the only decision to have better Alpha, I64, C++ and more support.\nGcc would have broken the BAI anyway.\nDo your homework and document yourself a little more. \n\nciao,\nMik\n\nbtw. Zero problems here compiling Aethera (and X, kde, moz, gnome....) on RH7.0"
    author: "Mik"
  - subject: "Re: Aethera Messaging Client Beta 1 on RedHat 7.0"
    date: 2001-01-19
    body: "<i>Bull*! None said it was stupid.</i><p>\nIf you want to call what I said bullshit, please say it.  We are not children here, and censoring your own words to appear more civilized insults me more than a simple curse.<p>\n<i>document yourself a little more. </i><p>\nAgreed.  No one said it was \"stupid\"... however, the <a href=\"http://gcc.gnu.org/ml/gcc-announce/2000/msg00003.html\">censure by the GCC steering committee</a> was certainly negative in tone, and basically said: \"No one should ever do that again\".  Which meant to me that they considered the inclusion of a compiler that produces \"object files that are not compatible with those produced by either GCC 2.95.2 or the forthcoming GCC 3.0\" a mistake, a.k.a. a stupid move.<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Aethera Messaging Client Beta 1 on RedHat 7.0"
    date: 2001-01-23
    body: "1) the incompatible object code is only the c++ one\n2) name me ONE single gcc/egcs version that didn't break c++ ABI with a new major release.\n3) Ever bothered to see a gcc-cvs changelog?\n\ngreets,\nPaul"
    author: "Paul"
  - subject: "Re: Aethera Messaging Client Beta 1 on RedHat 7.0"
    date: 2001-01-18
    body: "<i>Please stop this FUD. There is nothing wrong with RH7. The gcc-2.96 was the best quality available. </i><p>\nI wouldn't call it FUD when the gcc development team (including those that actaully work for Red Hat) say that it was a stupid thing to do, was against their wishes, and that it should not have been done.<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "The error \"hit list\""
    date: 2001-01-22
    body: "The latest Redhat compiler (2.96-69) is good enough to compile every other KDE application on the planet.  And glibc 2.2 final is now standard from \"up2date\" - the same version that's in the latest Debian among others.\n\nThe problems I've seen so far:\n\n- stdlib.h isn't included in 2 files, causing GCC to error out (it now considers unprototyped functions a problem at the warning level Aethera's makefile uses).  Having your includes in order is good practice even if you're using a compiler that doesn't whine.\n\n- A ton of icons are defined as static and not used.\n\n- Aethera appears to be incompatible with KDE 2.0.1.  The constructor to KApplication isn't what it's expecting.  I'm pretty sure this isn't a compiler problem either.\n\nOnce I figure out the right way to fix that last problem I'll continue onward (I still don't have it compiled properly yet)."
    author: "Ian Schmidt"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-17
    body: "There are no SuSE rpms either. ( I'm tooo lazy to compile myself)"
    author: "reihal"
  - subject: "Filters?"
    date: 2001-01-17
    body: "I downloaded the (*gasp!*) source of Aethera and compiled it.  Wowwww-eee, that is one sharp looking app!  Kudos to theKompany!!\n\nMy only whine is that filters don't seem to be working.  That is all I'm missing to give KMail the boot!\n\nSince someone asked about Palm support on LT, I'll ask about message threading.  It will do that eventually (or it might now . . . I don't know), right?  And do it well?  Please?\n\nI'd use it even if it didn't thread, but . . . =)"
    author: "Ed Cates"
  - subject: "Re: Filters?"
    date: 2001-01-18
    body: "We spent a lot of time designing the UI, so I'm glad you like it :), it will get more refined in the next releases, there are still some rough edges at the moment.\n\nWell we did say in the release that it didn't have filters yet, but that it would, hopefully in the next release :).\n\nPalm support is coming as is threading.  It's not there now, but it will be in the near future."
    author: "Shawn Gordon"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "I played with it this morning.  This is one fantastic program!  Hats off to thekompany.com.\n\nI used the Mandrake RPM, and it works wonders!\n\n-- kent"
    author: "Kent Nguyen"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "Well, I think you should read the lines at the Magellan site. It basically says that the Kompany took the code of and didn't contribute back to Magellan. See  http://zamolxe.csis.ul.ie/faq.html#9\n\nNot that I'm negative to the Kompany but all it doesn't hurt to know. \n\nI must also say that the Aethera looks really cool, what would be even cooler is if Gnome and KDE could use the same server for the Schedule/Calender part. Then it would really rock - so I hope someone that is openminded thinks about that. \n\nRe User"
    author: "User"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "This really saddens me.  I had 3 developers basically full time working with Magellan for at least 2 months before things soured.  I've not gotten into the particulars and I don't plan to, and I always praise Teddy when given a chance, and cleared up any misconceptions of the relationship whenever they have arisen.  Teddy got a lot of work from us for Magellan, and since Aethera is open as well, he is free to borrow from it, which is what we said at the time that we terminated the arrangement.\n\nI think if you look at the two projects you will see that they are vastly different in terms of look and stability and that is going to get even more pronounced as time goes on, and that is the underlying reason why we split off.  Within a month there will be very little of the original Magellan code in Aethera.\n\nAs far as Gnome/KDE interoperability on the server, I guess we'll see where everything is once we get there, we've already championed some projects that support both desktops."
    author: "Shawn Gordon"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "Congratulations on the release Shawn. Aethera looks amazing! I downloaded the source and noticed that the different Aethera views weren't kparts. Are there no benefits from making them so? I only ask because it seems like a cool thing that the mail composer, say, is a kpart that could be changed on the fly, or embedded into konqueror. This is not a critique by any means I'm just trying to learn about this stuff. Congratulations again."
    author: "kde fan"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "That's an interesting idea.  I know we would like to get some level of integration with KOffice, but I'm not sure what that is going to look like just yet, we need to get more of the application finished first.\n\nThanks for your thoughts :)"
    author: "Shawn Gordon"
  - subject: "Questions"
    date: 2001-01-18
    body: "a) Will there be a windows version of the <b>client</b>? (it would be very helpfull in mixed enviropments...)\nb) Will the Groupware/Server part be open sourced as well??\nc) Is there any plan for support Microsoft MAPI? (MS Mail , Exchange etc)"
    author: "t0m_dR"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "a.  it's possible, it is mostly written to Qt.\nb.  No - we have to make money somehow\nc.  We've been looking at it, not sure just yet."
    author: "Shawn Gordon"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "Thanx for the answers! Good work once again. If you implement Mapi it would be great if you implement it on the server as well (it would help on mixed environments as well)\n\n....now if you would just make an M$ Access clone rad tool (without the bloat and the bugs) which would use Python as scripting language instead of Basic... ;-)"
    author: "t0m_dR"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "Funny you should mention that, we are almost ready with our first public beta of Rekall, which is exactly what you are asking for.  It's probably about 2 or 3 weeks away from coming out, and the Python scripting won't be in the first release, but I think you will like it.\n\nAs usual we will make announcements so that you will know about it."
    author: "Shawn Gordon"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "Really, it's almost getting funny -- it seems everytime someone tells you \"I wish you would make Application X\", your reply is \"Funny you should mention that...\" :) You really seem to have an eye for what people want, and if that's not the recipe for success then I don't know what is :)"
    author: "Haakon Nilsen"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "I can't stress enough what a fantastic team of developers we have.  When you contrast what we are doing, with the number of people and no external funding, to what some of these other companies are doing with their multi-million dollar investments, you are really struck by how incredibly productive the Qt and KDE environments are.\n\nWe have even more things in the works, but since I hate vaporware, we won't talk about them till they are about ready to release, or even when we release them."
    author: "Shawn Gordon"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "OK!\n\u00cdow if you could just make Koffice import and export MSOFFICE files 100% unaltered , it would really ROCK! :-) \n\n...anyway as everybody else stated, the Kompany seems to get the pulse of the people...I really hope that continues in the future , and that you are profitable...kudos.."
    author: "t0m_dR"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "OK!\n\u00cdow if you could just make Koffice import and export MSOFFICE files with 100% compatibility , it would really ROCK! :-) [i'm just trying to find something you CAN'T do! ]\n\n...anyway as everybody else stated, the Kompany seems to get the pulse of the people...I really hope that continues in the future , and that you are profitable...kudos.."
    author: "t0m_dR"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "OK!\n\u00cdow if you could just make Koffice import and export MSOFFICE files with 100% compatibility , it would really ROCK! :-) [i'm just trying to find something you CAN'T do! ]\n\n...anyway as everybody else stated, the Kompany seems to get the pulse of the people...I really hope that continues in the future , and that you are profitable...kudos.."
    author: "t0m_dR"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "OK!\n\u00cdow if you could just make Koffice import and export MSOFFICE files with 100% compatibility , it would really ROCK! :-) [i'm just trying to find something you CAN'T do! ]\n\n...anyway as everybody else stated, the Kompany seems to get the pulse of the people...I really hope that continues in the future , and that you are profitable...kudos.."
    author: "t0m_dR"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "sorry , netscape FSCKed UP!... :("
    author: "t0m_dR"
  - subject: "Re: Questions"
    date: 2001-01-19
    body: "Use Konqueror!\n\n(sheepishly posted from Mozilla in win32 (at work)..)"
    author: "Ben Hall"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "he should change his name to \"whishmaster\" <g>"
    author: "ac"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "Thanx for the answers! Good work once again. If you implement Mapi it would be great if you implement it on the server as well (it would help on mixed environments as well)\n\n....now if you would just make an M$ Access clone rad tool (without the bloat and the bugs) which would use Python as scripting language instead of Basic... ;-)"
    author: "t0m_dR"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "Shawn,\n\nThe work you are doing for KDE is wonderful,\nalmost too good to be true. I hope your company\nmakes a shitload of money. But realistically,\nhow are you guys doing on that regard? Are you\nmaking enough money to make all this possible, \nand worthwhile from your POV in the future? \n\nIn any event, THANK YOU! You are one of the \ncoolest companies I have ever known.\n\nMagnus."
    author: "Magnus Pym"
  - subject: "Re: Questions"
    date: 2001-01-18
    body: "I won't lie, it's a tight race.  We've spent the last 18 months developing software and only about 6 months selling it.  If people really like what we do then you can support us by buying software, even if you don't need it :)."
    author: "Shawn Gordon"
  - subject: "Re: Questions"
    date: 2001-01-19
    body: "Shawn,\n\nI intend to do just that. Thank you for all of your contributions to our community!\n\nDavid"
    author: "djoham"
  - subject: "Re: Questions"
    date: 2001-01-22
    body: "I think that David's idea is actually briliant. This guys from theKompany are really doing a wonderful job writing a lot of useful applications. And for that they deserve to make a lot of money, since their products are much better than products from most of other \"only-$-making\" software companies. I recommend that, like David, everyone that reads this, buys some product from theKompany. This money would not be given to them out of mercy, but thanks to the guys from theKompany (and indirectly to all KDE developers) for making such great products."
    author: "Bojan"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "I'll elaborate on the sad bits here:\n\n(1) the \"3 developers basically working full time with Magellan\" have commited back about 40 lines of code (the project has over 100000); surely they've worked a lot, but not for Magellan.\n\n(2) of course \"borrowing\" works only one way, which is from Magellan to Aethera, since Magellan is BSD-style licensed and Aethera is under the GPL (more restrictive). What Mr. Gordon states here is a practical impossibility, as we don't want to restrict the uses of Magellan nor transform it into a license mess."
    author: "Teddy"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "hmmm...if what you say is true , than I believe tha the Kompany should at least Licence a copy of Aethra to Magellan team with the BSD Licence... as I see it it's the ONLY way to go if shawn wants to be OK with the ethical side..."
    author: "t0m_dR"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "I have to humbly disagree with you, ethics aside. The right to fork is one of the basic freedoms of Open Source. The BSD license is such that theKompany could even repackage Magellan and sell it commercially and closed. That's one of the things the GPL protects against. The Magellan team is free to re-license Magellan to be able to include code from Aethera, or they could even fork Aethera and call it Magellan."
    author: "Haakon Nilsen"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "well of course I agree this is the case, but I was talking about the ethics , and not the legal background. I think it's only proper to give them access to the code through their licence , since they gave them the base. That's just a good gesture , an act of friendship in my book..."
    author: "t0m_dR"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "Agreed. This is why people use the BSD license afterall (&quot;the GPL is too <i>rescrictive</i>...&quot;). To say that the &quot;GPL <em>protects</em> against [this]&quot; is an excellent choice of words."
    author: "Tom Vaughan"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "I have avoided, and will continue to avoid airing the laundry about the Magellan situation.  Teddy has his opinion of it and we have ours.  I don't think it's productive to get into a pissing contest over this.  I hope you all continue to enjoy the number of projects that we have delivered to the community."
    author: "Shawn Gordon"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-19
    body: "Why? Because your arguments don't pan out very well in public.  Re-licence your work back to Teddy.  He is the one that really put the work into it.  Do it because it's only fair, do it because you and the Kompany have honor, do it because you need to sell the PR."
    author: "ac"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-19
    body: "Whats wrong with the GPL? Teddy can take everything he wants from the code as long as it stays GPL. \n\nWhat if the Kompany used/will use code/patches from someone else who doesnt want to release under non GPL?\n\nThe GPL and the other open source license models are made for forking. The writer has to be acknowledged, but thats all. Contrary to people equating it to socialism, its simply an awful Darwinian system without any direct enforcement of cooperation/fairness."
    author: "ac"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-20
    body: "I'm all for the GPL, and I wish all software could be released under the GPL.  Look, all I'm saying is that the changes done by the Kompany should be given back to Teddy - to be fair.  If the Kompany receives submissions from people outside the company wishing to keep their code GPL then they can; however, all code for the program written by the Kompany should be given back to Teddy for his use also.  Everybody wins then - not everybody wins minus Teddy.  A good conscience should make the Kompany not want to profit from all of Teddy's work if Teddy can't profit from their work.\n\nYour point about the GPL is good; however, I think most KDE people are used to working with code that is under different licences.  Right?"
    author: "ac"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-19
    body: "Sorry, but I disagree. The BSD license is specifically designed to allow this.\nApple can steal FreeBSD and sell it as Mac OS X and no one is upset, although it is a much larger borrowing of code than what has happened here...and they're not giving their changes back (i.e. Aqua GUI, etc.).\nIf Teddy doesn't like what theKompany did, that means he's using the wrong license and should switch to GPL."
    author: "Jacek"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-20
    body: "I'll second that.  Shawn could have taken Magellan, polished it, made it binary-only and sold it.  He didn't.  He re-licensed it under the GPL, protecting his derived works.\n\nThe BSD license has its place, as does the GPL.  What has happened here is becuase of the original choice to go with the BSD license.  We're all just lucky that Shawn GPL'd his changes.\n\nBesides, didn't he fund Teddy for a while?\n(Sorry if I'm wrong..)\n\nBen"
    author: "Ben Hall"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-20
    body: "This wasn't an easy decision to make, and we didn't do it lightly, but we tried to do it correctly.  Also just to be clear, I didn't fund Teddy in any way."
    author: "Shawn Gordon"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-22
    body: "Well, the issue is that it is just the polite thing to do to ask the maintainer of the project for his opinion and try to work with him as much as possible.\n\nIt seems to me that from Teddy's post theKompany did not devote any sort of real resources to the project and it is just building on the work from Teddy.\n\nIt just seems very rude"
    author: "Anonymous Coward"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-22
    body: "Please don't make opinions or pass judgement without knowing all the facts.  I really don't want to get into details because it is not productive for anyone.  We spent many many hours on Magellan, but the bottom line is that we both had different ideas of what to do with it.  From your perspective it gives you a choice of what you want to use."
    author: "Shawn Gordon"
  - subject: "Using the GPL as a weapon"
    date: 2001-01-22
    body: "Hey Shawn,\n\nIf you are not sharing your code with Teddy, then in my opinion you are using the GPL as a weapon against his email client rather than co-existing in \"open source\".  You are in other words using his work against him.  That's just my opinion however. You could explain yourself better on this subject to clear up all misconceptions."
    author: "ac"
  - subject: "Re: Using the GPL as a weapon"
    date: 2001-01-22
    body: "At the point in time that we put the GPL notice in Aethera, the Magellan site had conflicting information, at some points it said GPL and some it said MIT/X.  The code actually said MIT/X, at least the version we had as the Magellan CVS became suddenly unavailable about a month before we did our fork.  Given this conflicting information, we weren't sure if Teddy had changed the license and it seemed safer to make sure that the Aethera code was GPL just to be sure."
    author: "Shawn Gordon"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-24
    body: "\"Within a month there will be very little of the original Magellan code in Aethera\"\n\nuntil then, you should have given the proper credits. shame!"
    author: "Evandro"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-24
    body: "Evandro we have done that.  Look at our web page at http://www.thekompany.com/projects/aethera/overview.php3 the very first sentence gives this credit, and the credit was in the story header.  I've given Teddy credit at every turn.\n\nPlease check your facts before you point your finger of shame."
    author: "Shawn Gordon"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "While other companies just talk and talk the Kompany delivers! Good work Shawn and the guys."
    author: "Matt"
  - subject: "Make it a revolutionary PIM"
    date: 2001-01-18
    body: "There seem to be two classes of PIMs.\nThe first are ones like Aethera.  The second are ones like www.thebrain.com or www.memes.net (OK they aren't usually called PIMs).\nThe ideal PIM would be one that combined both of these approaches into one application.\n\nFor instance, entries in the contact list could have links to other contacts, emails, or notes.\nSo that when I receive an email from Bob, I could pull up the contact entry for Bob and discover I had added links that point to his girlfriend Jill's contact record, links to organizations he belongs to, a link to a note I entered with info about our last conservation, links to the emails we exchanged, links to contact records for our common friends,\nlinks to calendar entries involving Bob, etc.  Each of these items could then have its own links.\n\nLinks could have have user entered descriptive info like:\n\"works for\", \"first cousin of\", \"interested in\",\netc.\n\nThe structure of links would not be preordained but could evolve so that each user could grow their own web of information.\n\nJim"
    author: "James H. Thompson"
  - subject: "Re: Make it a revolutionary PIM"
    date: 2001-01-18
    body: "Yes! I agree thats what we need to make Aethera leading edge - lets not just aim to have features comparable to M$ Outlook, lets inovate!\n\nTalking about inovation, I like the idea of mobile SMS messages staight from Aethera, WOW!!!  I hope the next preview will have some of this, or has this being dropped and only available in Magallan?\n\nThanks again theKompany for yet another Kick-ass app!"
    author: "iCEmAn"
  - subject: "Re: Make it a revolutionary PIM"
    date: 2001-01-18
    body: "If you look at the Aethera roadmap you will see that we have a number of unique and innovative features, there are more that we are working on but didn't want to talk about because it seems like every time we do the feature suddenly shows up somewhere else :).\n\nSMS and all the other things will be in Aethera, don't worry."
    author: "Shawn Gordon"
  - subject: "Re: Make it a revolutionary PIM"
    date: 2001-01-23
    body: "What?! Other projects are copying an open-sourced project's features? I'm shocked!\n\nSeriously... I don't understand your logic. If Aethera is one of your GPLd (i.e. non money-making) products, why are you worried about others copying your features? Isn't that what open-source is all about, the ability to copy other people's ideas and/or code?\n\nconfused,\n\n-Avdi"
    author: "Avdi"
  - subject: "Mind Mapping !!"
    date: 2001-01-18
    body: "> www.thebrain.com\n\nMy God!!  A dynamic mind-map interface to all my data .. that's [stunning] !!   The difficulty of trying to keep all the years of information & email I've collected in some form of organasation that remains useful over time has been driving me nuts for ages.  Heirachical folders run out of steam beyond a certain point and info just gets lost or fogotten about.  \n\nMy first eye opening experience to the possibility of organising data differently came several years ago in the form of \"ML\" - an IMAP mail client from a guy at Stanford Uni that allowed you to use filtering rules to create \"Virtual Folders\".  It was amazingly powerful ; but died a death because 1) the guy stopped developing it and went to work for Netscape ; 2) it staticly linked in Motif; and 3) its dependance on large flat file mail files (behind IMAP) ran out of steam performance wise when they got too big (which they quickly did).  Realisticly it needed a back end that a) used a database and b) had some built in smarts that enabled it to cooperate with the Virtual Folder generation. \n\nIt will be interesting to see if the Gnome folk can re-visit this with Evolution and take the concept onto the next level.\n\nI do like theBrain's mind-map interface though .. I just can't way WOW enough times.  This could be the answer to my prayers.  I wonder if they've ever heard of KDE ;-)\n\nJohn"
    author: "John McNulty"
  - subject: "Re: Make it a revolutionary PIM"
    date: 2001-01-18
    body: "This is the direction on which gofai-wmt is headed. It is already doing similar things for webpages with konqueror, try it in autopilot mode.\n\n  In fact, if you would have a contact list client with a sufficiently reach DCOP interface, the functionality could be added in an afternoon. \n\n       Lotzi"
    author: "Lotzi Boloni"
  - subject: "Re: Make it a revolutionary PIM"
    date: 2001-01-18
    body: "Is gofai-wmt still alive? -- its web pages seem to have vanished. (I tried: http://www.cs.purdue.edu/homes/boloni/GofaiWeb/)\n\nThe key to making something like this work is ease of use.  It's not clear you could accomplish that easily using a separate program.\n\nThere is also the issue of how objects get annotated/extended.  For instance for a contact you would like to be able to put in as many phones numbers as you wish, each with some note: 'home', 'summer home', 'tuesdays work place', 'european cell number', etc.  The only PIM I've seen that handles this gracefully is http://www.ajsystems.com/amigo.html"
    author: "James H. Thompson"
  - subject: "Re: Make it a revolutionary PIM"
    date: 2001-01-18
    body: "Yes, it is alive. \n\nhttp://www.geocities.com/boloni2/GofaiWeb/index.html\n\nor try kdenonbeta/gofai-wmt"
    author: "Lotzi Boloni"
  - subject: "Newsgroups?"
    date: 2001-01-18
    body: "If Outlook Express is stripped down Outlook, why doesn't Outlook support newsgroups?  I ask myself that every time I have to load that crappy program at work and then load a newsgroup reader to check my subscribed newsgroups.  Ugh!  I'd like to see Aethera support newsgroups.  And I think that the previous post about making the different chunks of your program into KParts is essential!  Looks pretty damn cool though!"
    author: "James"
  - subject: "Re: Newsgroups?"
    date: 2001-01-18
    body: "It's also frustrating that OE has better Mail Rules that Outlook is missing.  I'm amazed that MS couldn't use ALL the features of OE in Outlook PLUS some.  In many ways - OE is superior to Outlook in the email program part of it."
    author: "Laura"
  - subject: "Re: Newsgroups?"
    date: 2001-01-19
    body: "who told that oe is a descendant of outlook? as far as i know both programs do not share a common root..."
    author: "thePooh"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "Hi Shawn,\n\nI downloaded and installed this last night, and it looks real sharp. My only complaint is that it seems to store it's data in binary format. I have a large flat file containing 150+ contacts, and I was hoping to convert it into Aethera's format with a python script. Is there any reason that this is not stored in plain text? XML would seem to work very well here. If there is some reason for the binary format, is it at least documented somewhere? \n\nJust my $.02."
    author: "Larry Wright"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "Well, we are working on some import features, so we will more than likely make it possible to create custom import routines."
    author: "Shawn Gordon"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "That's good to know, but was there a reason for choosing a binary format?"
    author: "Larry Wright"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-19
    body: "Don't bother about import features, that will be an endless nightmare. Just provide a dcop interface to add new contacts. Then any importing script can just do\n\ndcop aethera add_new_contact firstname lastname address yada yada yada"
    author: "Guillaume"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "So, now what we need is for kmail and kdepim to start rolling in the good parts of Aethera, so that when the proprietary modules start rolling in, open source alternatives can be made available."
    author: "Neil Stevens"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "And that will be the perfect way to make sure that we don't make money on anything and go out of business.\n\nWhat bothers me is the notion that open source must also be free of charge.  I honestly have no problem with including the source to an application that someone has purchased, or that we are giving away for free.  What bothers me is this misconception that the software should also be free of charge.  This is what forces a company to create closed source so that they can protect their financial interest.\n\nI've been coding for 22 years and creating commercial software for 15 years now, and it seems I've spent half my life coming up with copy protection devices to avoid piracy.  This was one of the lures of Linux to me was the idea of Open Source, but I've been disappointed to a certain degree on the misunderstanding of \"free\".\n\n95% of the email I get is very supportive in both financial and cheerleading, but I do get that 5% that are angry at me for having the audacity to charge for our work.  It really blows me away.\n\nSo you see, the \"for sale\" modules of Aethera can be open source as well, but that depends on the public."
    author: "Shawn Gordon"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "Right on, Shawn.<br>\nDon't worry, as soon as all the \"all open-source must be free\" fellows get their first mortgage, you'll be amazed how quickly they'll become more realistic in their expectations :-)<br>"
    author: "Jacek"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "Well, i don't think you have a problem in this case, since the Server version will be Proprietary.\n\nbtw: why don't you create a new licence in which you will give the proprietary software, in which anyone will be able to modify the code for personal use, but only *IF* he was purchaced a copy, and he will not be able to (legally) distribute it or create something else with the code without agreement from your company..."
    author: "t0m_dR"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "The server is only one of the pieces we will sell.  All the normal features of a Mail, PIM and Newsreader will be free, it's some, not all, of the unique features that will carry some sort of price.\n\nAnother license is an interesting thought.  I've been skiddish on this because of the heat Trolltech (wrongly) got over the QPL, but we can dual license it.  I'll have a talk with my high priced attorneys and see what we can come up with :)."
    author: "Shawn Gordon"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-19
    body: "If you come up with a license that will allow paying customers to modify and improve the code, and allow those customers to share their improvements (but without the problems that were the downfall of Minix), that would completely change things.\n\nIt would change things for the better.  Customers would retain freedom.  You could get that revenue from selling copies that you want.  In this situation, everybody wins, right?"
    author: "Neil Stevens"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "Hi Shawn.\n\nI've been using Linux for four or five years now, and I have yet to find an e-mail client that holds a candle to those found on other OSs.  The closest thing I've got so far is Pine, which, while amazing, sucks on many levels.\n\nIf you come up with _the_ client, I'll gladly pay for it.  Especially if it's Open Source!\n\nI personally think that there's plenty of money to be made in OpenSource software.  I also think that developer time should be paid for, not the actual products.  This is why I like the CoSource idea so much.  You've got your plans, and I think they may work too.  \n\nThe idea of giving the source along with the binaries is a fantastic first step.  We need steps like these before development can make the transition from proprietary to Open Source.  Your company is one of the few that seems to be making an honest attempt at walking this fine line.\n\nSo, if you come up with this PIM and it has Palm and IMAP support, I'll happily send you $50CDN.  This is a promise from me to you.\n\nOh, on a related note, I was VERY excited about your release of KDEStudio.  I'd also be happy to pay something for that.  Sadly, I've been unable to get it to compile on my Debian Woody system running KDE 2.1 betas.  If you were to statically compile it, and offer binaries with docs (thereby giving an IDE approaching MSVC++) I'd also happily pay for that.\n\nMaybe give the source and sell the binaries??  I guess that's what Helix Code is kinda trying to do..\n\nBTW, as an ex-Be user I simply will not pay for Binary-only software anymore.  So, if BlackAdder also comes with the source, then I may be interested in buying that too..\n\nGood luck, thanks for reviving all of these half-dead KDE projects,\n\nBen"
    author: "Ben Hall"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "I'm leaving for an overnight holiday in a few minutes, so if I don't respond to things in my standard rapid fashion, I apologize.\n\nAre people reading my mind lately?  Watch for an announcement about KDE Studio on Monday or Tuesday next week.\n\nWhat about BlackAdder exactly do you want to see open source?  Some of it is, but we don't have control over all of it.  I've gotten this question 2 or 3 times, but no one can tell me what it is they want open (I really am curious)."
    author: "Shawn Gordon"
  - subject: "KDEStudio in Debian"
    date: 2001-01-19
    body: "Use source from CVS. I had some problems also compiling KDE Studio in Debian (Potato). Problems are fixed already in CVS."
    author: "Hasso Tepper"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "I've been trying (and pretty much failing) to explain to my business partners and employees (programmers and html artists, mostly) what open source is all about, and how you really can make money off of it.<p>\nI'm hoping that in six months, I can point to theKompany and other true open source success stories.  Red Hat et al. were tied up into the dot com stock market bubble... open source really hasn't got some solid success stories for corporations or individuals.<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-18
    body: "How many times must I repeat:  I don't care if you charge money.\n\nHave you not heard of the expression: \"Free speech, not free beer\"?  Without the freedoms outlined in the Debian Free Software Guidelines (a subset of the terms demanded by the GPL), my computer, and therefore my data, is under the control of another.\n\nI cannot accept that.  If there are enough people who can (and Microsoft has proven there are), then you will make money, Shawn.  If you, with your funded resources, can provide a high-quality product and good support, better than an open source project can, then you will make money, Shawn.\n\nIf, however, you assume that you cannot successfully compete with an open source project, and if you think that you cannot provide better software and support than an open source volunteer project, then that doesn't say much about your confidence in your coders and staff."
    author: "Neil Stevens"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-20
    body: "I've often heard you say things to the effect of \"If you like what we're doing for the community, please buy or other products.\"\n\nThis actually strikes me as something that should be looked into much more in the Open Source community.  As the sentiment goes around here, I have absolutely no qualms about paying for software.  I just want the source.  I've often thought that a donations-based model for driving Open Source development would be interesting.\n\nAfter all, if people want to see quick implementations of features and new ideas, full-time developers are required.  And these people do have to eat.\n\nMy guess is that if an organization such as the KDE League, or GNOME Foundation, or even the Free Software Foundation, were to setup a fund to support free software development, people would contribute.  I certainly would.\n\nIt has been said that Linux is used by 2 million people in the desktop market alone.  If 1 out of 5 of these people were to contribute 20$ once a year, that would be 8 million dollars to fund Open Source development.  That seems to be very conservative to me - with the efforts of a large organization behind things, I bet more could be raised.\n\nHell, Amnesty International has no problems getting 50$/year from me.  I'm sure the Open Source community, in which I have much stronger convictions, could get quite a bit more."
    author: "James Morton"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-20
    body: "It might have even been mentioned here, and I think it's a good idea.  Basically a license that gives you the source code, but prevents you from distributing the code to other people.  This gives you the ability to modify the code as you want, you could even submit it back if you wanted, but it also protects our interests by preventing the wholesale distribution of our code to people who didn't pay for it."
    author: "Shawn Gordon"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-22
    body: "So your solution is to discourage free software development? This is the main point of the kde project, a \"free\" desktop environment. \nThat's not to say you can't charge for your software if you wish to, but if kde developers feel like \"rolling in\" the good gpl'ed parts of your application into kmail they should be able to do so, and I hope they do. That's the whole point. \n\nFrankly I'm surprised more people haven't raised this point."
    author: "ac"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-22
    body: "Well,there has to come a point where commercial software companies have to have a chance to make some money of Linux software as well, right?\nThis is one thing I do not understand as well: if no software companies can make money off Linux products, who will employ Linux developers?\nIt would be pretty sad if the majority of Linux developers had to develop for Windows from 9-5 and only do Linux at night as a hobby...sounds like a recipe for mass burnout.\nIf theKompany plays by the rules, bases their product on open standards and does not lock in the users into a proprietary closed solution, then what's wrong with them making some money?\nSeems like the market for KDE apps has come a long way in a short time just because of a little profit-based motivation that theKompany has behind it. If they can maintain superior product quality, I would have no problem paying for their product. If their products will suck and free alternatives will be available, then they'll go out of business, but that should not be the case if they manage to provide good value...which seems to be the case here.\nNo one seems to have a problem with IBM making lots of $$$ thanks to Linux, selling DB2, WebSphere, hardware, etc...a little profit never hurt anybody."
    author: "Jacek"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-22
    body: "That's fine. There is absolutely nothing wrong with it. Nothing. But Shawn's original reply to the suggestion that the good parts of Aethera were incorporated into kdepim, and kmail was:\n\"And that will be the perfect way to make sure that we don't make money on anything and go out of business\"\n\nI don't have any problems with him trying to make a buck selling software. As a matter of fact, I would probably pay for Aethera. But to discourage developers (let's face it, he's getting pretty influential in the KDE community) from taking the parts they like about Aethera is unthinkable. If he actually succeded in doing so, I would switch to gnome (as much as I hate that prospect)."
    author: "ac"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-22
    body: "Oh come on, let's not go overboard here.  The point I was making was that if the only intention of the public is to wait for us to come up with a really good piece of software and then peel it off into something else, then it will most certainly do us no good, and then we will be gone and that will be that.\n\nThink closely about what you are saying here.  For the sake of argument, say we charged $50 for Aethera (it is free, but for our example), but it is open source and so all the code gets put into KMail and KPIM.  Why would you buy Aethera?\n\nTry not to over-parse my statements as I don't always have time to craft an elegant carefully worded response.  I make it a point to reply to all mail and as much of these feedbacks as quickly as possible, which means my responses will not always come across correctly.\n\nI've said time and again that I have no problem with having our stuff open sourced, but I also can't to that to the detriment of my business.  I'm doing my best to respect the open source community, and I just ask for the same in return.\n\nAs far as influece in the KDE community, well I have virutally none and I don't try to exert any.  All we are doing is releasing as many products as we can for KDE that plug the gaps (as I perceive them).  Almost all of our employees came directly from the KDE community as well."
    author: "Shawn Gordon"
  - subject: "Con't compile it (am not using RH 7)"
    date: 2001-01-18
    body: "Using a Caldera eServer 2.3.1 (all updates). I can't compile aethera/libs/vparsers/rdate.cpp:\n\ng++ -I. -I. -I../.. -I/opt/kde2/include -I/usr/lib/qt2/include -I/usr/X11R6/include             -I../../src             -I../../data            -I../../icons -O2 -Wall   -c rdate.cpp\nIn file included from rdate.cpp:34:\nrdate.h:49: warning: ANSI C++ forbids declaration `Period' with no type\nrdate.h:49: parse error before `;'\nrdate.h:52: type/value mismatch at argument 1 in template parameter list for `template <class T> QValueList<T>'\nrdate.h:52:   expected a type, got `RDate::Period'\nrdate.h:52: warning: ANSI C++ forbids declaration `periods' with no type\nrdate.h:80: syntax error before `('\nrdate.h:88: syntax error before `('\nrdate.cpp:44: syntax error before `::'\nrdate.cpp:46: syntax error before `;'\nrdate.cpp: In method `void RDate::parseString(class QString)':\nrdate.cpp:51: warning: comparison between signed and unsigned\nrdate.cpp:60: `tb' undeclared (first use this function)\nrdate.cpp:60: (Each undeclared identifier is reported only once\nrdate.cpp:60: for each function it appears in.)\nrdate.cpp: In method `void RDate::setFinshed()':\nrdate.cpp:77: `Finished' undeclared (first use this function)\nrdate.cpp: In method `int RDate::getNextOccurencePrivate(class QDateTime)':\nrdate.cpp:89: request for member `count' in `RDate::periods', which is of non-aggregate type `int'\nrdate.cpp:93: invalid types `int[int]' for array subscript\nrdate.cpp:106: invalid types `int[int]' for array subscript\nrdate.cpp:108: invalid types `int[int]' for array subscript\nrdate.cpp:108: invalid types `int[int]' for array subscript\nrdate.cpp:109: invalid types `int[int]' for array subscript\nrdate.cpp:117: invalid types `int[int]' for array subscript\nrdate.cpp: At top level:\nrdate.cpp:129: syntax error before `::'\nrdate.cpp:137: syntax error before `::'\nmake[3]: *** [rdate.o] Error 1\nmake[3]: Leaving directory `/home/storm/CVS/kompany/aethera/libs/vparsers'\n\nThis error is in the kompany cvs for atleast two weeks. I post this here because i don't see any bug reporting address in the web pages or source.\n\nIs this known ?\n\nPS: KDE2.1 from CVS. QT 2.2.3. i can compile everything else flawlessly."
    author: "Storm"
  - subject: "Re: Con't compile it (am not using RH 7)"
    date: 2001-01-18
    body: "Two weeks?  We just made this available yesterday.  I would suggest using the source RPMS, which is what I did on my COL 2.4 system.\n\nThe support address is aethera@thekompany.com, I put it in the press release, but forgot to put it on the web page, we are correcting the mistake now.\n\nI've also forwarded your problem to the above email address.  Please try the SRPM and see how that does for you, any other problems, please let us know at the above address.\n\nThanks!"
    author: "Shawn Gordon"
  - subject: "KOMPANY"
    date: 2001-01-18
    body: "Hey! is there still development going on ? or is theKompany doing all New stuff , LOL !!!\n\ngo kde go"
    author: "ch"
  - subject: "MySQL backend?"
    date: 2001-01-18
    body: "Now that we all have huge hard disks, how about allowing the use of a full-blown database for storing mail, calendar, and contacts data?\n\nPronto (and others) use MySQL. It's riduculously fast with folders (and vfolders) containing, say, 20-30,000 messages, and rock-solid."
    author: "Michael O'Henly"
  - subject: "Re: MySQL backend?"
    date: 2001-01-19
    body: "This would be great as an option. That way you could easyly access all data through other means, too. Let's imagine some kind of webfrontend for contacts. I'd love this."
    author: "RIchard Stevens"
  - subject: "Not MySQL, but DB backend"
    date: 2001-01-19
    body: "This is my usual response to this:\n\nLook at how KRN 0.6.11 does it. It's just as insanely fast with folders containing up to hundreds of thousands of messages (ok, you need 256MB of RAM for that ;-)\n\nIt uses Gigabase, an embedded DB backend, which is free, and needs no setup at all.\n\nIt's cool, it's fast, its stable, it's nice, and the author gives great support. It can even be used in commercial stuff, I think."
    author: "Roberto Alsina"
  - subject: "Re: Not MySQL, but DB backend"
    date: 2001-01-19
    body: "i'm sorry but what's KRN? i'd like to take a look at what you refer."
    author: "ac"
  - subject: "Re: Not MySQL, but DB backend"
    date: 2001-01-19
    body: "http://krn.sourceforge.net"
    author: "Roberto Alsina"
  - subject: "Re: Not MySQL, but DB backend"
    date: 2001-01-19
    body: "ah. i've been using knode. will give this a try.\ntks."
    author: "ac"
  - subject: "Re: MySQL backend?"
    date: 2001-01-20
    body: "Actually one of the things we've been discussing for some months is using a database for the virtual file system.  It has a lot of interesting advantages, we just have to see if there are overwhelming disadvantages first :)."
    author: "Shawn Gordon"
  - subject: "Re: MySQL backend?"
    date: 2001-01-20
    body: "Thanks for the reply.<p>\n\nThis would be so excellent. It seems to me that we spend all our efforts on the frontend instead of taking advantage of the real benefits that lower-cost hardware and robust databases offer.<p>\n\nHaving a large data store behind a PIM opens up possibilities for the application that could be really useful (i.e., full-text indexing, vfolders based on search criteria, arbitrary linking based on real-world relationships defined by the user, etc...)<p>"
    author: "Michael O'Henly"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-19
    body: "plz..can anybody give me an URL for alternative download because I can't browse/download from www.thekompany.com page, I wan source codes....tar.gz :>"
    author: "orpheus"
  - subject: "Too popular?"
    date: 2001-01-19
    body: "Can't download it either... Is theKompany becoming a victim of its own popularity?\n\nAll their servers are painfully slow!\n\nAnyone know of another site where I could download the Mandrake RPM?"
    author: "Okeo"
  - subject: "Re: Too popular?"
    date: 2001-01-19
    body: "It is probably still being slashdotted."
    author: "ac"
  - subject: "Mirror please!"
    date: 2001-01-19
    body: "Mirror is needed!!"
    author: "Kent Nguyen"
  - subject: "Re: Mirror please!"
    date: 2001-01-20
    body: "I've been trying for two days to download it.  I am amazed that they were not prepared with mirrors available, knowing that once it hit slashdot, among other sites, their servers would get killed..."
    author: "Chris Slaughter"
  - subject: "Re: Mirror please!"
    date: 2001-01-20
    body: "We actually had no idea that slashdot was going to cover it, they've ignored everything else we've ever sent them in the last 8 months, and without it ever happening, we weren't sure if we had enough to cover it or not, and apparently we don't.  It's frustrating for us as well, we will look into setting up some mirrors."
    author: "Shawn Gordon"
  - subject: "I can mirror, but what is it you need mirroring"
    date: 2002-12-30
    body: "What product needs mirroring?  I will see if I want to mirror it."
    author: "Jeremy"
  - subject: "Source TGZ download here!"
    date: 2001-01-20
    body: "Hi,\n\nI grabbed the source before Slashdot killed the website.  You can get it here:\n\n(NOTE: Konqueror doesn't seem to like this link, sorry.)\n\nhttp://www.xdrive.com/share/979937047978UhxTLUieqeHnnIgRRg3C   \n\nShawn, don't read the Slashdot articles too carefully.  Man, they're negative on _everythin_ lately.\n\nSlashdot has really fallen down over the last few years.\n\nSigh.\n\nBen"
    author: "Ben Hall"
  - subject: "Re: Source TGZ download here!"
    date: 2001-01-20
    body: "Konqueror in 2.1 likes that link :)"
    author: "Storm"
  - subject: "Re: Source TGZ download here!"
    date: 2001-01-20
    body: "Hmm... mine doesn't.  I'm running off of the newest Debian Woody debs (20001-01-16) you?\n\nThankfully, this new version fixes my CSS issues.\n\nAlso, do you notice bizarre font handling?\n\nCheers,\n\nBen"
    author: "Ben Hall"
  - subject: "Re: Source TGZ download here!"
    date: 2001-01-20
    body: "I'm running CVS from today :)\nI update and compile almost daily.\n\nI haven't noticed any weird font problems, no."
    author: "Storm"
  - subject: "Re: Source TGZ download here!"
    date: 2001-01-20
    body: "Hey thanks for putting that up Ben!\n\nAs far as Slashdot, it looked like 85% of the posts were either lame and unrelated comments, or people arguing about Outlook.  I like to read these comments because it's a good way for me to get input and feedback as well.  Believe me, I read them all, and I pass the information on to the developers if they didn't read it."
    author: "Shawn Gordon"
  - subject: "Aethera strangled my system..."
    date: 2001-01-21
    body: "I'm quite pleased with Aethera so far, although it's quite clearly not a \"stable\" release. However, having tried it out yesterday, today I found my /home partition constantly filling up to 100%, even though I deleted some quite large files and freed up a lot of space. Given some time, it would eventually be 100% full again. Also, I noticed how the system was slowing down, so I started \"top\", which revelaed this as the most cpu sucking process: (pasted from top)\n\n28655 haakon    17   0  2644  196   112 R    78.4  0.1  1424m aethera\n\nTo sum it up: the program used 78.4% of the cpu, and had been running for almost 24 hours. However, after testing Aethera I shut the program down, so I don't understand why it has been running ever since. I also don't know why it sucked up my HD space, but as soon as I killed the process the HD stopped filling up. \n\nMy drive is still pretty full though. I would like to hear suggestions on where to look for files Aethera might have created that I can now delete :)"
    author: "Haakon Nilsen"
  - subject: "Re: Aethera strangled my system..."
    date: 2001-01-21
    body: "Haakon,\n\nThis feedback is good, and we need to hear about this so we can continue to improve the project.  I've forwarded this info to the developers, but please send other reports to aethera@thekompany.com to make sure we get them and can address them.\n\nthanks!"
    author: "Shawn Gordon"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-22
    body: "Looks cool, but the $1 000 000 question for me is, will it interoperate with M$ Outlook? My business partner is still a slave to Windows and we really need an interoperable groupware system that isn't some slow web-based solution.\n\nDave"
    author: "dave hj"
  - subject: "Re: Aethera Messaging Client Beta 1"
    date: 2001-01-22
    body: "We are evaluating having the server be able to work with Notes and Exchange.  Notes has a published API, but Exchange is a different matter.  We are looking at it, but I don't know what the outcome will be yet."
    author: "Shawn Gordon"
  - subject: "Suggestion for Notes"
    date: 2001-01-23
    body: "Why not make the Notes \"IM\" like.\n\n-- kent"
    author: "Kent Nguyen"
  - subject: "Re: Suggestion for Notes"
    date: 2001-01-24
    body: "IM as in Instant Messanger?  Can you describe your thought better for me?  I would like to evaluate it.\n\nthanks"
    author: "Shawn Gordon"
---
Good news from <a href="http://www.theKompany.com/">theKompany.com</a> keeps pouring in.  As several of you have pointed out, the first beta of <a href="http://www.thekompany.com/projects/aethera/index.php3">Aethera</a> has been <a href="http://linuxtoday.com/news_story.php3?ltsn=2001-01-17-005-06-OS-KE">announced</a>.  In case you haven't been following, Aethera is theKompany's fork of the greatly hyped/anticipated <a href="http://www.kalliance.org/">Magellan project</a>.  Beta 1 of Aethera sports POP3, SMTP, HTML, DnD, a contacts interface, sticky notes, and more.  IMAP, Calendar support, etc are promised for the next beta. There is no mention of the license although source is available from  the website -- most of the source files seem to be under the BSD license. For more details on Aethera, <a href="mailto:kreichard@internet.com">Kevin Reichard</a> also points us to this favorable <a href="http://www.linuxplanet.com/linuxplanet/previews/2914/1/">review</a> on <a href="http://www.linuxplanet.com/">LinuxPlanet</a> by Dennis E. Powell.  Be sure to check out the <a href="http://www.thekompany.com/projects/aethera/screenshots.php3">screenshots</a> as well.








<!--break-->
