---
title: "KDE 2.2beta1:  Ready to Roll"
date:    2001-07-05
authors:
  - "Dre"
slug:    kde-22beta1-ready-roll
comments:
  - subject: "Screenshots?"
    date: 2001-07-05
    body: "Anyone have screenshots of this thing?"
    author: "Dude"
  - subject: "Re: Screenshots?"
    date: 2001-07-05
    body: "Hahaha, you'll need about a 100 screenshots to show all new things!!"
    author: "Pasha"
  - subject: "screenshots here"
    date: 2001-07-06
    body: "I came across these (as mentioned on the <a href=\"http://www.gui-lords.org/\">gui-lords</a> site):\n<p>\n<a href=\"http://home.houston.rr.com/texstar/index.html\">http://home.houston.rr.com/texstar/index.html</a>"
    author: "Navindra Umanee"
  - subject: "Re: screenshots here"
    date: 2001-07-06
    body: "Interesting screenshots, but it's a real pity that they are using a Windows window&widget style: the default KDE one looks much prettier, and doesn't give the casual viewer the impression that KDE is just a Windows rip-off. If I want to use Windows, I'll use Windows."
    author: "Jon"
  - subject: "Re: screenshots here"
    date: 2001-07-06
    body: "Also, the first thing that guy did after installing the beta was to replace the Konqueror icon in Kicker with Opera!  What kind of KDE fan is he?!  Konqueror is way better!"
    author: "not me"
  - subject: "Re: screenshots here"
    date: 2001-07-06
    body: "Whatever he wants to run is his business. Opera for Linux rocks in my opinion."
    author: "Jason"
  - subject: "Re: screenshots here"
    date: 2001-07-06
    body: "Opera for Linux doesn't have multilanguage support at all. It is probably OK for english speaking users...\nI experienced Opera not being faster than Konqueror and Netscape 4.7. \nI dunno about Java support in Opera for Linux."
    author: "Antialias"
  - subject: "Re: screenshots here"
    date: 2001-07-06
    body: "I've found that opera is fast enough for me to use on my p200; which apart from netscape - nothing else is. (konqueror is fairly fast while browsing in one window - but opening new windows takes a long time, and tends to block the UI.)"
    author: "Rikard Anglerud"
  - subject: "Re: screenshots here"
    date: 2001-07-08
    body: "try opening new windows with the Ctrl + N command. they\u00b4ll start much faster (less than 1 second here)."
    author: "Evandro"
  - subject: "Re: screenshots here"
    date: 2001-07-08
    body: "What's the reason for all \"unstable\" versions of KDE having only the right kicker hide button and the stable releases having both or none by option? I think it could be nice to be able to choose between left, right, both or none (replace left/right with top/bottom for vertical panels) in the stable releases. Admittedly, it's not the most necessary feature, though."
    author: "Mattias"
  - subject: "Re: Screenshots?"
    date: 2001-07-06
    body: "lol, that guy doesn't know how to make screenshots =))\n\nAnd why do i get the feeling this dude is Pro-Windows? KDE has other themes, too!\n\nNah, don't judge the new KDE on these screenshots... they're really bad compared to the real thing. In my humble opionion that is =)"
    author: "Remenic Q. LeBeau"
  - subject: "Re: Screenshots?"
    date: 2001-07-06
    body: "Dont be so hard on the guy, he threw them up rather quickly so everyone could atleast see the new wizard. Geesh..."
    author: "Jason"
  - subject: "Re: Screenshots?"
    date: 2001-07-06
    body: "Hmm okay, you're right. To be honest, i didn't really look at it that way... I hope the author can forget what i said. Sorry."
    author: "Remenic Q. LeBeau"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "Whow... KDE2.2 is gonna rock!"
    author: "mpattonm"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "Whow... KDE2.2 is gonna rock!"
    author: "mpattonm"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "oh my god,\n\nthis rocks, ford better has a close look :-).\n\nthe personalizer is a great move (just there is no undo yet :-).\n\ni'm always amazed in how many areas (small and large) changes happen. all these improvements have a time to market vendors even don't dream about.\n\nfor sure these are some of the great benefits of open source that gives it the capability to move beyond other \"software business plans\".\n\nthanks to the community.\n\nmaxwest"
    author: "maxwest"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "Actually I tried the latest snapshot two days ago, but it crashes a lot of time (font selection in KControl and during printing). I thought that it's a good idea to use the latest beta and report the bugs (if they are present) for this one, so I downloaded it (yesterday and today). But there is a problem:\n The kdesupport package is missing both from the source and Suse 7.1 directory, and the when I configure the kdelibs, it claims that I will not have sound without the kdesupport. \n Do we really need it (I think we do)? Now I'm trying to use the snapshot version of the kdesupport."
    author: "Andras"
  - subject: "Where's kdesupport (Was: Re: KDE 2.2beta1: Rea...)"
    date: 2001-07-05
    body: "The kdesupport module is deprecated except as a convenience for the cvs compiling people who do not want to go hunting for the right pieces of software (the one piece of software not available elsewhere, mimelib, was moved to kdenetwork). It is the job of your distributor to provide the necessary packages like libxml and, in your case, libaudiofile."
    author: "Kalle Pahajoki"
  - subject: "Re: Where's kdesupport (Was: Re: KDE 2.2beta1: Rea...)"
    date: 2001-07-05
    body: "Thanks for clarification! I hope that the Suse packages are working correctly. I just had the problem because I'm compiling the packages for a Solaris 2.6 machine (as user), and it was present in the CVS, but not in this release. Anyway, the KDE is getting better each day!"
    author: "Andras"
  - subject: "Re: Where's kdesupport (Was: Re: KDE 2.2beta1: Rea...)"
    date: 2001-07-05
    body: "If you succeed,  would you put tarballs online. Please ?\n\nI have been unable to compile KDE under solaris 2.6 since the 1.1 series -- because of gcc not compiled with libtools. And the admin does not have time."
    author: "Hmmm"
  - subject: "Re: Where's kdesupport (Was: Re: KDE 2.2beta1: Rea...)"
    date: 2001-07-05
    body: "There is a good chance that I will succeed, because I've done it a lot of time (altough I didn't compiled everything, just the kdelibs, kdebase, kdenetwork, kdeutils). The problem with making the tarballs is, that I do this at my work (as an user), with restrictions about where will it be installed and so on. I wrote a \"tutorial\" how to do this, I can post it to you (or to anyone who is interested in - maybe put it somewhere on the web (KDE pages???) ). It may help you to get it work."
    author: "Andras"
  - subject: "Re: Where's kdesupport (Was: Re: KDE 2.2beta1: Rea...)"
    date: 2001-07-05
    body: "Please, I'm VERY interested in this tutorial. I'm also running Solaris 2.6 at work ,and I tried many times compile to compile KDE 2.x with no success."
    author: "AC"
  - subject: "Re: Where's kdesupport (Was: Re: KDE 2.2beta1: Rea...)"
    date: 2001-07-05
    body: "Please, I'm VERY interested in this tutorial. I'm also running Solaris 2.6 at work ,and I tried many times compile to compile KDE 2.x with no success."
    author: "AC"
  - subject: "Compiling on Solaris 2.6 (Was Re: Where's kdesupp)"
    date: 2001-07-05
    body: "For thos who are interested: please send me a private mail, so I can send the tutorial for you. (Otherwise provide an email address when you write a message to this site)."
    author: "Andras"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "Someone has noticed that kdenetwork* RPM packages for Mandrake 8.0 of KDE2.2beta1 are missing?\n\nBest regards."
    author: "Aronnax (Acqua Theme designer)"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "maybe its just your favorite mirror\nmine has stopped updating at kdeartwork.\n\naaaargh!\n\nanyone who knows a better one?"
    author: "ac"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "the french i18n file is missing, too"
    author: "vluvlu"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "All packages are available on Mandrake Cooker mirrors.\n\nRegards,"
    author: "Antialias"
  - subject: "Mandrake 8 kde-network RPMS available."
    date: 2001-07-08
    body: "If you don't want to go and install tons of Cooker packages to meet the dependancies...  Texstar of <a href=\"http://PCLinuxOnline.com\">PCLinuxOnline</a> has built the missing kde-network and kde-network-devel packages.  These work with the packages for Mandrake 8 available on ftp.kde.org (and its mirrors).\n\nI believe he used a modified spec file from 2.2alpha1 against the 2.2beta1 source tree.  Clever, that guy...\n\n*TheDarb\n<a href=http://GUI-Lords.org\">GUI-Lords</a> Host\nDedicated to Linux Domination of the Desktop."
    author: "TheDarb"
  - subject: "Re: Mandrake 8 kde-network RPMS available."
    date: 2001-07-08
    body: "Argh... those HTML tags sure didn't work, now did they!  *sigh*  Oh well, you get the idea... Texstar on PCLinuxOnline.com has what you are looking for.  He be your KDE pusher man. ;)\n\n*TheDarb"
    author: "TheDarb"
  - subject: "Attention SuSE Linux users!"
    date: 2001-07-05
    body: "SuSE Linux users might find that KDM (the login thingy) doesn't work after they have installed the new KDE 2.2beta1: The keys simply do not work, so it's impossible to enter your password. The solution is to push Menu -> Restart X Server - then the keys will work. To avoid that in the future, you could change the KDM settings to allow automatic login. Within KDE, push K -> Preferences -> System -> Logon. Choose the last tab in the menu and enable automatic login for your user."
    author: "Jonas Koch Bentzen"
  - subject: "Re: Attention SuSE Linux users!"
    date: 2001-07-05
    body: "Hi,\n\nthere's an additional solution to this problem.\nYou simply have to edit the file (IIRC) /opt/kde/share/apps/kdm/Xservers. There's a comment inside which explains what to do (simply uncomment a line and comment out another one).\nI'm not at home at my linux box, so I cannot give\nmore details.\n\ndaniel"
    author: "daniel kranich"
  - subject: "Re: Attention SuSE Linux users!"
    date: 2001-07-05
    body: "The file is called (on SuSE):/opt/kde2/share/config/kdm/Xservers\n\nAnd it contains the following text, maybe it is just a bad default setting:\n\n# Xservers - local X-server list\n#\n# This file should contain an entry to start the server on the\n# local display; if you have more than one display (not screen),\n# you can add entries to the list (one per line).\u00b7\u00b7\n# If you also have some X terminals connected which do not support XDMCP,\n# you can add them here as well; you will want to leave those terminals\n# on and connected to the network, else kdm will have a tougher time\n# managing them. Each X terminal line should look like:\n#\u00bb\u00b7\u00b7\u00b7\u00b7\u00b7\u00b7XTerminalName:0 foreign\n#\n \n:0 local /usr/X11R6/bin/X\n \n# For Linux use this one\n#0 local@tty1 /usr/X11R6/bin/X vt7\n \n# For Solaris use this one\n#:0 local@console /usr/X11R6/bin/X"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Attention SuSE Linux users!"
    date: 2001-07-05
    body: "same here. debian unstable.\nseems like a kdm bug"
    author: "ik"
  - subject: "Re: Attention SuSE Linux users!"
    date: 2001-07-06
    body: "Another glitch I'm experiencing (on SuSE 7.1) is:\n\nWhen I start as a normal user I get a lot of error messages of the form: \n\nkio (KSycoca): WARNING: Found version 32, expecting version 38 or higher. kio (KSycoca): WARNING: Outdated database found\n\nThe result is no kioslaves available (even file:/), no icons to speak of, most of the configurable menu entries are gone, and no apps run from the Run Command...\n\nWhen I start as root, everything works fine.\n\nAny suggestions as to which file has the wrong perms on it?\n\ncheers\n\nWill"
    author: "Will Stephenson"
  - subject: "Re: Attention SuSE Linux users!"
    date: 2001-07-06
    body: "Fixed:\n\nremoved \tkde-<username>\n\tksocket-<username>\n\tmcop-<username>\n\nand it works now\n\nWill"
    author: "Will Stephenson"
  - subject: "Re: Attention SuSE Linux users!"
    date: 2001-07-09
    body: "Solution:\n\njust don't start the FAM daemon and all will work!"
    author: "Matthias Pfafferodt"
  - subject: "Re: Attention SuSE Linux users!"
    date: 2001-07-09
    body: "How do I prevent the FAM from starting?"
    author: "Jonas Koch Bentzen"
  - subject: "Re: Attention SuSE Linux users!"
    date: 2001-07-09
    body: "with yast you can set \"START_FAM\" to \"no\" and all works fine (for me <g>)"
    author: "Matthias Pfafferodt"
  - subject: "Re: Attention SuSE Linux users!--HELP!"
    date: 2001-07-09
    body: "another suse user writes: \nI feel like I screwed it up real bad. I did everything I could to install the update right but I MUST be missing something. Using kpackage, I first installed kdelibs (well, first i installed cups b/c i needed it) then i installed kdelibs2.2beta. then i tried to install kdebase2.2beta and i need oggvorbis and libogg or whatever, so i installed those. ok...so then i installed kdebase. tried to run konqui, and it didn't work -- no buttons on the toolbar and it wouldn't browse anything. tried to restart kde and here's what i'm getting (something like this:): unreferenced object line XX changeToolbar_style8Q .   like i said, i'm not sure if it's that exactly, but it won't run. note: i went straight from kde 2.0.1 to 2.2beta is that the problem? do i need to install kde 2.1 first? how can i now that i've installed 2.2beta. i'm such a newbie...sorry...\nthanks in advance for any and all help"
    author: "zaq rizer"
  - subject: "Re: Attention SuSE Linux users!"
    date: 2006-06-04
    body: "Hi there, \n\nI have recently downloaded and installed suse linux 10.1 (KDE environment). When I  finished all the installing process and was about to start using it, I found out that i cannot login. The screen appeared says \"linux-ptmv login\" and, although I can type there, when I press enter it ask me a password but i can not type anything. How can I fix this problem, after three days i have not still use linux. I am really pissed off.\n\nLazaros\nGreece"
    author: "lazaros"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "It's great,\n\nI wonder, would it be possible to get it compiled against qt-3.0.0-beta2????"
    author: "wim bakker"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "I don't believe QT 3 is binary compatible with QT 2, so they'd have to bump up the major number of KDE. \n\nProbably, after KDE 2.2 they'll release 2.3 about 6/9 months later, then release a 3.0 fairly soon after that, which will be 2.3 ported to QT 3. Luckily, the changes from QT 2 to QT 3 seem a lot smaller than the changes from QT 1 to QT 2."
    author: "Jon"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "Actually they are saying that there will be a 2.9 release that will be KDE2 on QT3, and then they will release KDE3 some time after that.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "Yes, thanks for reminding me. I'd forgotten the exact plans."
    author: "Jon"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "With every new KDE release the number of pro-KDE and anti-GNOME trolls increases...\n(while the entire overhyped GNOME vs KDE war was only invented by trolls who have nothing else to do)\nLet's hope this article doesn't reach Slashdot."
    author: "ac"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-07
    body: "> Let's hope this article doesn't reach Slashdot.\n\nit did ;-)"
    author: "someone"
  - subject: "Mosfet Does it again!!"
    date: 2001-07-05
    body: "---- Found this on mosfet site ---------\nUpdate (7/01): Actually, this isn't entirely true anymore. While everything will still remain free software, I'm going to switch to the modified QPL (some of the commerical restrictions will be removed). I'm being forced to do this because I've just been notified of KDE developer's intent to fork code that I never even had in CVS and add it to the KDE packages. It seems anything I do is subject to immediate forking by the KDE team and unofficial versions included in the base packages as a matter of procedure. If I don't want my code there they will just fork it, which has been their right but forks are usually done to add features or fix bugs. In the case of KDE2.2 this code has less features and more bugs! In many cases they didn't even rename their forks, so it makes it difficult for users to tell which versions are the ones in KDE CVS and not officially maintained by the author, and which ones are my versions and maintained by the original developer. Some developers even had the nerve to say *my* versions are the forks, even though I'm the primary author and copyright holder! Nonsense.\n\nThus the switch to QPL. The following software is going to be re-released under the QPL: Highcolor default, KDEStep, Laptop, Liquid, MegaGradient, ModernSystem, System, and Marble. Sorry for the inconvience this may have caused if this is a problem for you. Of course, KDE has the right to distribute forks based on obselete versions of my software, which were BSD licensed. The new license *will* be enforced for future versions, nonetheless. This is a shame, because I do truly believe the most effective development model is completely open, but this is not the case when the people designing the platform your developing for consistently take inferior versions of your code and add it to the system against your wishes instead of letting users just use your versions instead. It wouldn't be so bad if some independent coder decided to just fork my stuff and release worse versions, I really don't mind competition - but when the entire platform your developing for decides to do it simply because you've decided to start developing independently and adds less features and bugfixes you have a problem.\n --- End of Message\n\nI guess someone from KDE Developement Team can clarify rhis to stop this FUD"
    author: "KDE_User"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-05
    body: "It's not a problem. Most of those styles really should be replaces with pixmap styles/themes, anyhow (and it's thanks to Mosfet that we have a nice fast pixmap style engine).\n\nIt's a bit of an anachronism to have these code based styles in, that offer absolutely nothing over pixmap styles. Once they are made pixmap themes/styles, they can be also made much easier to update and install.\n\nThe one thing we need with KDE 2.3 is a nice window/widget style designer. I imagine people are working on it, but over the summer I might write one as a good introduction to KDE programming for me."
    author: "Jon"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-05
    body: "Except beeing faster, which I find to be very important. The also offer a greater liberty as to how the widgets look."
    author: "Hmmm"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-06
    body: "Code styles do offer several big advantages, including having very precise control over how changes to the color system affect everything, and also adding functionality to a theme as well as looks. For instance, semi-unrelated but take a look at the Be-like window decoration. It would be impossible to do _that_ with pixmaps alone."
    author: "Carbon"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-07
    body: "A windows skinning utility called WindowBlinds does that... And it supports *only* Pixmap themes. (www.windowblinds.net)"
    author: "Nomar"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-07
    body: "What I was mostly referring to was the ability by this window decor to automatically (mostly) keep title bars from covering each other if possible. It can't be done with pixmaps alone. WindowBlinds might support it, but it has to have code to sit behind the pretty pixmaps.\n\nBesides, it's for Windows, who cares? :-)"
    author: "Carbon"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-07
    body: "Take a look at megagradiant. The menus are translucent, that'd be imposssible with a pixmap theme."
    author: "Jeff Brubaker"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-07
    body: "Yes - and this translucent effect should be generalised, so you can apply it to any style (just like you can apply the fading in/rolling effects to any style). That way you could turn it on or off as required.\n\nAs far as I can tell, there is very little in the coded styles which couldn't be done in pixmap styles much quicker and easier. Another thing I'd like to look at when I have the time is coding a code style which actually does something interesting - for example, having a button pulse in and out when the mouse is over it :)"
    author: "Jon"
  - subject: "Mosfet."
    date: 2001-07-06
    body: "You can't reason with Mosfet.  He insists on inventing facts of his own and refuses any outside argument, so what is the point.  \n\nGood luck, Mosfet."
    author: "ac"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-06
    body: "As much as I like the KDE project, and am very saddened that you're losing Mosfet's new code, I have to take his side on this one.\n\nI just wish everybody could make up and things go back to normal.  Ah well."
    author: "dingodonkey"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-06
    body: "why can't the kde project let mosfet control his own stuff, the way linus controls the kernel? are pixmap based styles really THAT much better? personally, i'm gonna mosfet's styles. i don't care about them being easier to update and install. what i do care about is speed and flexibility. (and innovation of course, where the kde project will be at a disadvantage cause they can't fork mosfet's new code.)"
    author: "Rick Kreikebaum"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-06
    body: "The KDE project *does* allow mosfet to control his own stuff. As a matter of fact, he has always been free to do this. However, when codefreeze has been publicly announced for a certain date and that date has arrived, that is *not* the time to announce new features. He had ample opportunity to let the project know what he was up to. Why he chose to use that opportunity unwisely, only he knows. I do wish him well in his endeavors and hope that his new styles integrate easily into KDE."
    author: "ne..."
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-07
    body: "hmm... well that does make it a little harder for me to sympathise with him."
    author: "Rick Kreikebaum"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-06
    body: "www.mosfet.org is all text... Where do I download his styles from?"
    author: "AC"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-07
    body: "They're included in kde. If you want to see all of them you need a cvs version, since some are disabled for the release because they are incomplete/unstable."
    author: "Jeff Brubaker"
  - subject: "Re: Mosfet Does it again!!"
    date: 2001-07-06
    body: "I think the best solution to this problem is lobby mosfit to change his mind. Feel free to write him at mosfet@interaccess.com, mosfet@kde.org, and mosfet@mosfet.org.\n\nTell him why *YOU* think licensing his own code under the QPL is a bad idea, for himself, the community, and for KDE.\n\nYou can explain that the QPL will give mosfet a bad reputation in the community. People in the open source community, simply do not trust software that does not have one of those magic 3 letter licenses -- GPL or BSD. \n\nThere will probably be problems including his software in Debian, if it's QPL licensed. Other distros will probably not include it for fears of legality problems or in protest.\n\nKDE developers won't be able to include older versions in their distro. So their version maybe modified to fit into the purpose of KDE, and will be older then the \"offical\" version, it's not the first time KDE has not had the latest version of programs included. Many of the programs in kdesupport were older then the offical version, and some of them were modified from the orignal version. \n\nAt any rate, please be polite, and try to use sensible arguments to help change his mind. I'm sure you can come up with better ones.\n\nI have sent my message to him, have you yet?"
    author: "AC"
  - subject: "Why RedHat-RPMs late?"
    date: 2001-07-05
    body: "i'm just wondering why the binary-RPMs for the RedHat-Distribution are \"late\"? who is doing the RPMs? the distributors? is redhat lazy?"
    author: "Till"
  - subject: "Re: Why RedHat-RPMs late?"
    date: 2001-07-05
    body: "That's realy annoying... I'm almost switching my distro... maybe Debian and apt is the answer for me."
    author: "ms"
  - subject: "Re: Why RedHat-RPMs late?"
    date: 2001-07-06
    body: "Especially since us Debian unstable users have had 2.2beta1 for a week already :)."
    author: "David G. Watson"
  - subject: "Re: Why RedHat-RPMs late?"
    date: 2001-07-06
    body: "yeah, i'm switching to debian, cause apt with official .debs is supposed to be better than anything rpm. and i heard that unstable isn't really that unstable :)."
    author: "Rick Kreikebaum"
  - subject: "Re: Why RedHat-RPMs late?"
    date: 2001-07-05
    body: "I agree.\nIt's a shame.\n\nRed Hat is one of the largest distributions, if not THE largest.\n\n\n/Richard"
    author: "Richard"
  - subject: "Re: Why RedHat-RPMs late?"
    date: 2001-07-06
    body: "Er, RH has no obligation to provide you with _beta_ rpms."
    author: "ne..."
  - subject: "Re: Why RedHat-RPMs late?"
    date: 2001-07-06
    body: ">Er, RH has no obligation to provide you with _beta_ rpms.\n\nRH has even no obligation to provide me stable or official rpms since I didn't buy anything from them...\n\nI'm just looking for better options."
    author: "ms"
  - subject: "Re: Why RedHat-RPMs late?"
    date: 2001-07-06
    body: "> Er, RH has no obligation to provide you with _beta_ rpms\n\ncorrect. but why ARE there rpms for mandrake and suse? i'm just wondering. is this because it's too complicated to compile kde for redhat? or are suse and mandrake just more customer-focused?\n\nin deed i BOUGHT redhat 6.2, i BOUGHT rh 7 and i BOUGHT rh 7.1. if i wanted i COULD compile kde myself but i don't WANT to. i am a PAYING customer (silly me...), and it seems suse and mandrake are supporting customers better than redhat does.\n\non the other hand, consumers aren't the \"real\" customers for redhat, are we? and, of course, industry isn't very hot for beta-versions...\n\ncu\ntill"
    author: "Till"
  - subject: "Re: Why RedHat-RPMs late?"
    date: 2001-07-09
    body: "Well RH is a gnome based distro so i'm not sure that kde or the desktop is a very big priority for them. If they cared about the desktop they'de be a kde distro.\n\nCraig"
    author: "craig"
  - subject: "Re: Why RedHat-RPMs late?"
    date: 2001-07-10
    body: "GNOME based?\nI've been running KDE on Red Hat for a while now. I have also installed it directly when doing an upgrade och install.\n\nDo you run Red Hat?\n\nIf so try to choose KDE during the next install and you'll see that it has KDE as an option just like with GNOME.\n\nStop this FUD:ing...\n\n\n/Richard"
    author: "Richard"
  - subject: "Why RedHat-RPMs late?"
    date: 2001-07-05
    body: "i'm just wondering why the binary-RPMs for the RedHat-Distribution are \"late\"? who is doing the RPMs? the distributors? is redhat lazy?"
    author: "Till"
  - subject: "FreeBSD port?"
    date: 2001-07-05
    body: "Anyone who happens to know when (if?) the FreeBSD port will appear?"
    author: "Chucky"
  - subject: "Re: FreeBSD port?"
    date: 2001-07-05
    body: "I wouldn't hold your breath on a FreeBSD port for a beta of KDE.  Rarely are any beta apps posted, and I've personally never seen it for something as large as KDE.  There's a lot to a port of that size, and usually a couple of days lag time kicks in.  For KDE 2.1 the port maintainer had been working early with the KDE folks, so the port came out darn near on release day.  This is more the exception than the rule, and Will did a great job with that exception.\n\nAlso, I'd caution you to not try upgrading to KDE 2.2 immediately after the port hits the tree.  That is, unless you're looking to submit bug reports.  Best to let it settle in for at least a few days to get all the bugs worked out.  Again, this is a large port involving a number of scripts.\n\nAhh, but when them bugs do get worked out, the port install of KDE works like a dream.  It's too bad there aren't a LOT more ports for KDE apps outside of the core install for FreeBSD.  Stuff like KAim and KBear yet to have any ports done up for them.  I've attempted porting these myself, but the only thing I managed to get done was to prove that my skills aren't quite yet up to the task.  For what ports are there, KDE runs really sweet under FreeBSD."
    author: "Metrol"
  - subject: "Re: FreeBSD port?"
    date: 2001-07-06
    body: "Hello ;)\nThere will be no FreeBSD port for the BETA 1\nYet Will (wca) the maintainer has assure dme, that he will be doing the release port update as soon as the release for KDE 2.2 is scheduled/annouced.\nYou are free to compile from CVS, it works on FreeBSD too with a few tricks ;)\n-d"
    author: "darian Lanx"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "Oh man, I wish I had more synonyms for superlative forms of \"good\"! :)\nSeriously, the KDE project has to be the single most impressive Open Source project ever (rivalled perhaps by GNU), and the one that is likely to inspire the most people to switch to Open Source desktops. If there's any technological reason in the world, everyone should be perfectly able to make a fulltime switch from windows any day. The only exclusion being people wanting to run new games which require DirectX, though that might not be true for a very long time, considering the impressive progress of Wine.\nNot only is KDE technically proficient, but it is also perfectly possible to make it look better than commercial desktops (win XP, OS X, etc.), thanks to the incredibly talented artists in the KDE project.\nActually, I can't think of a single desktop \"core\" task possible in any other desktop environment, be it commercial or non-commercial, closed source or open source, etc, that isn't also possible to do with KDE. A lot of times, the power and freedom of choice are even greater in KDE than in its \"counterparts\", such as the almost infinite configuration possibilities and incredible flexibility of in particular kicker and konqueror.\nAnd on top of that, we have great apps, of which many are conveniently gathered at apps.kde.com. We have a great office suite with the K in it, we have administration programs, we have nicely integrated utilities for archiving, document viewing, media playing, etc, we have CD burning, scanning... I could go on for ages.\n\nWords shouldn't be needed to counter the argument that desktop linux is dead. Simply showing all the greatness we have should quench all possible doubt to linux desktop viability.\n\nThe inevitable conclusion would have to be:\nThe desktop is ready to be konquered. :)"
    author: "Mattias"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "I installed Tribes 2 on my current computer, a 500mhz Celeron, 128MB ram, Mandrake 8.0, TNT-2 Vanta MX and was very pleased!  While I only ran at 640x480, it looked a lot better in that resolution than I would have thought, and ran quite smoothely, except for a slight spike here or there, not often.\n\nI wasn't going to install this baby until I got my 1.4Ghz AMD Athon +mobo+case+power supply, in.  I figured that I heard such a game wouldn't run on PC well under windows, so I figgured I might want to be well off with a faster computer...  But was quite wrong.\n\nLinux (as long as it has games that can appease you) is ready Now!!!\n\nTrue, if you run linux on your old garbage box, you won't see much more than old garbage performance, but Linux is ready to dominate the main attraction!!\n\nIt's a catch-22...  If you don't demand the game for linux, they won't make it, but your not demanding it, because they're not making it...\n\nWell then slap yourself for a minute, be prepared to play a lesser (but good, and often fully satisfactory) set of Titles, cause if you buy it, they will come!\n\nSupply and Demand.\n\nSo, what should you for that aging box of yours?\n\nPick up a Case/Power_supply/Proc/mobo set from somewhere like Pricewatch ($200-$400),\n\nPick up a Geforce2 MX, or wait for a Geforce 3 MX to come out and be cheap :) (geforce2 MX's can be gotten for as little as $50, but $73 is more likely for 64MB, and even $100+ is descent for a Geforce 2 MX 400 (instead of a 200).\n\nOh yeah, I don't care how much you pay for it, but pick up a Logitech cordless set (Keyboard + mouse)...  Not only is it the best mouse design (for righties) in the world (opinion?), but its optical, and both pieces can run in either PS/2 or USB...\n\nhehe, I've got my usb keyboard hooked up to a PS/2 converter, hooked up to another converter, forgot what its called, but its big, and looks very familiar to the conector on my XT...\n\nNow, on with the Gaming...\n\nIMPORTANT SUGGESTION!!!!\n\nIf you are a linux New-bie, which is nothing to be ashamed of, as it's definition is \"if you are not coding to advance Linux, GNU, X, Gnome, or KDE...\"\n\nThen you might want to stick to games that use Loki's very nice updater... (this does exclude older titles like Quake III Arena, but includes newer titles like Tribes 2).\n\nThis updater is very nice, but if a update fails, make sure to see why, instead of keep trying blindly (click details...)\n\nI couldn't isntall the patches to Tribes 2 at first because there wasn't enough room, so I uninstalled it, and reinstalled it under my home directory, but kept the link in the bin directory...  because thats where it should be...\n\nThen, for your KDE link, just set it to tribes2, and forget about directories.\n\nMy site may not do anything but waste your time, but if it does, your talking about a descent amount to time to waste... which I'll take as a complement:\n\nCheck under Linux, or whatever else you want to see...  Included is some of the source code for a Tetris game I wrote (mostly just the fucntions, as the game was written in MS Visual C++)\n\nhttp://www.acs.brockport.edu/~gb7788\n\nDarn, I'm stupid, I forgot to mention the programing Linux games book by Loki.  I'll probably review it if I ever get a copy...  Man, they should be paying me!"
    author: "Greg Brubaker"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "I'm replying to myself... odd...\n\nAnyway, I actualy primarily use Linux for productivity...\n\nGames are just a very, very, very, very nice dessert.\n\nI personaly really like Kdevelop, AbiWord, Gimp, Gphoto, Quanta Plus, KDE (in general), and even Mozilla/Netscape 6.1  [hehe, had to leave Netscape 6.0 out of that one...]\n\nI look very much forward to KDE developement, but also to Mozilla, which once was so young, but now is approaching such maturity (damn, shouldn't have made that comment and given my email address...  But I really do like Mozilla, so please forgive me...) ;)\n\nLive free, or Die.\n\nand while you're at it, check out the Penguin Page...\n\nhttp://lwn.net/Gallery/\n\nPersonaly, I like:\n\nhttp://www.stack.nl/%7efidget/pguin/"
    author: "Greg Brubaker"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "Man, don't worry about that.  Two kick-a$$ browsers aren't going to hurt anybody.  It's really nice to see them coming along.\n\n(you said:)\nbut also to Mozilla, which once was so young, but now is approaching such maturity (damn, shouldn't have made that comment and given my email address... But I really do like Mozilla, so please forgive me...) ;)"
    author: "blandry"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "yeah, i can't wait to try out kde and some linux games with a REAL computer, as opposed to my dad's old celeron 266 :) hopefully i'll get a job and save up some money... then i'll get the lowest-clocked athlon 4 they have, for good overclockability, and an n-force mobo with with a geforce2 mx 200 for price reasons, which i'll later upgrade to a geforce 3. man, that's gonna be sweet! dual channel ddr, best sound chip for PCs, integrated into the mobo, super fast bus (amd hypertransport), one driver for the whole mobo/graphics card, and hopefully some significant cost savings (since i won't be buying an sb live.) and have you seen the geforce performance increases in xf86 4.1 with the latest nvidia dfivers? awesome!"
    author: "Rick Kreikebaum"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "sweeeet..."
    author: "Gregory W. Brubaker"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2002-07-25
    body: "Linux gaming will not be very adopted as widely as we wanted because all they are are ports.  We need someone (and loki is a good bet) to create games that run natively on linux. Here is a list of games that would be nice on linux.\n\n\nNFL Superbowl  - only on linux\n\nbikeing madness - only on linux\n\nTux the super-pengiun - only on linux (for the kiddies)\n\nand of course the FPS The Gunman Prophecies - only on linux\n\nRTS would be like Empire Giants - only for linux\n\n\nTell me what you think and if you would like these titles tell loki or any other big game developer for these games.\n\nI can't progam games or these would be already created :)\n\n \n\n"
    author: "Mike C. Reynolds"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "Cool !\n\nHas something been done about overall KDE speed ?\n(Meaning startup time of KDE and applications)"
    author: "Jasper"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "Yeah .. with this beta release there are some speed tweeks .. like starting KDE takes about a third of the time (thank god!) I havnt REALLY noticed a BIG diffrence in launching apps ... but then again i am running the beta off of my laptop! Also .. althougth this IS a beta its very stable compaired to the beta set in kde 2.1. Of course this means that the non-beta is going to rock.\n\n\nMike"
    author: "Micheal"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "Isn't the compiler the major source of that slowness? We can hope that GCC 3.0 is faster (once the bugs gets killed). I guess you could tweak KDE-code to be faster, but I guess it has already been done. There's only so much that you can do."
    author: "Janne"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "No, it's not. It's the linker (ld). There was an article some time ago by one of the core developers, you might want to read it.\n\nGCC 3.0 won't do much about speed.\n\nLatez\n   Stephan"
    author: "Stephan Oehlert"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "Definitely we need speed.\nKDE is very good, but not very usefull because the speed problem.\nYou could tell me to buy better hard, but one of the motives of KDE is to make people save money.\nAnyway, KDE rocks and I only can say Thank You to the KDE team!"
    author: "lanbo"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "Certainly ld affects KDE startup time, but I thought that the kdeinit hack fixed the problem with ld's slowness after that.  If so, what is keeping KDE from being fast now?\n\nAlso, wouldn't the fact that GCC 3 uses a different ABI mean that ld would have to be re-written?  Maybe the new one is faster.\n\nBTW, the article about KDE's speed, ld, and kdeinit is at www.derkarl.org"
    author: "not me"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "No, didn't fix it, just went around a part of the problem (i.e. made most libs loaded once and used by all programs guaranteed). But according to ld developers (this is what Waldo Bastian - the kdeinit hack maintainer - told me), there is new code for at-start symbol lookup table computation that makes ld like 7-8 times faster for C++ libs. I might be mistaken though. I can't wait and see this :-)"
    author: "Inorog"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "Yupps, There will be some extra utils for glibc so you can relocate sybols for libraries/binaries so conflicts don't need to be resolved during execution. This _will_ speed up the start of c++ program and save some memory too. I'm not sure if they will start 7-8 times faster though. But it will start faster if you pre-link them.\n\n/Sam"
    author: "Sam"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "I used to think you people complaining about KDE speed were just nit-picking.  But I'm temporarily using a Celeron 450 with 64MB RAM and you guy swere right...very slow.  Can't wait till I get back to my good old Athlon 950.  Maybe faster computers are the answer to this problem?  Just kidding!"
    author: "Henry Stanaland"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "Well, it's not just application startup time. Things like opening a new window in konqueror wa slow in the previous release. (so much so that I'm using blackbox/opera). \nThen again, I'm running on a p200.\nThat said, I hope it's faster, I'm looking forward to getting this beta installed!"
    author: "Rikard Anglerud"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-07
    body: "yeah, and the scrollbar isn't very responsive in most apps."
    author: "Rick Kreikebaum"
  - subject: "Cool Konqueror"
    date: 2001-07-05
    body: "> Konqueror enhancements include stopping animated GIFs (thank you, thank you!!)\n\nNo irritating animations, oh yes, thank you !\n\nMore important : I hope it is possible to shut up sounds !\n\nAlso about Konqueror :\n\n- cookies management : I wish it is possible to delete all cookies, excepted for allowed domains (accepted hosts)\n\n- File manager, right button : possibility to copy the path/name of the file (for then pasting elsewhere)\n\n- Ability to delete all favicons, so that they will be generated again (Ok, I know it is already possible by going on the good directory and deleting them, but it would be more easier with a parameter)\n\n- I hope that java and Flash will be installed by default\n\nBravo for the new and next enhancements !"
    author: "Alain"
  - subject: "Re: Cool Konqueror"
    date: 2001-07-05
    body: "> - cookies management : I wish it is possible to \n> delete all cookies, excepted for allowed \n> domains (accepted hosts)\n\nYou can delete all cookies, but all means all, so: good idea.\n\n\n> - File manager, right button : possibility to\n> copy the path/name of the file (for then \n> pasting elsewhere)\n\nJust _copy_ the damn file. Then you can paste the url...\n\n> - Ability to delete all favicons, so that they \n> will be generated again (Ok, I know it is \n> already possible by going on the good directory \n> and deleting them, but it would be more easier \n> with a parameter)\n\nGood idea.\n\n> - I hope that java and Flash will be installed > by default\n\nIf your JRE-1.3+ is installed correctly (java in PATH) it will work out of the box. Same goes for a correctly installed flash plugin for netscape. No need to installl that stuff again with KDE.\n\n\nYou should add your good ideas (TM) to bugs.kde.org as wishes."
    author: "Moritz Moeller-Herrmann"
  - subject: "Not Cool Konqueror !"
    date: 2001-07-06
    body: ">> - I hope that java and Flash will be installed > by default\n> If your JRE-1.3+ is installed correctly (java in PATH) it will work out of the box. Same goes for a correctly installed flash plugin for netscape. No need to installl that stuff again with KDE.\n\nNo, It is difficult, not user-friendly. I had difficulties to install Flash and I did not succeed to install java (however I did what is said in the Konqueror site). And when I see this page http://www.pclinuxonline.com/article.php?sid=174&mode=thread&order=0 I see it is still difficult. (and it is a different install than Konq site !!)\n\nI feel it is a BIG PROBLEM. It seems that for KDE, it is only a distrib problem, however in Mandrake 8.0, nothing was done (even the \"netscape-plugings\" rpm was not in the 2 CD !)\n\nAnd I don't think it is a good way to use some Netscape things... Konqui needs Netscape as a tutor ??\n\nSo now the BIG LACK of Konqueror is here : Flash and Java are not installed BY DEFAULT, there are manipulations to find and to execute and to execute again because it is not easy... (and many users have these difficulties, see the mailing lists and forums...)"
    author: "Alain"
  - subject: "Re: Not Cool Konqueror !"
    date: 2001-07-06
    body: "I have an idea. Someone from KDE could contact Macromedia to do the following: When the URL  for Flash's download is opened, the server checks if the browser is Konqueror. If it is, automaticaly starts to send the Netscape plugin file. For they it's easy to do that.\n\nOn the Konqueror side, when a plugin is requested, Konqueror opens in background the linked page for the plugin and waits if an automatic download starts. If so, Konqueror installs the plugin automaticaly, and its ready to use!\n\nIf there is no download from the site, Konqueror shows the pages as is today."
    author: "Julio"
  - subject: "Re: Not Cool Konqueror !"
    date: 2001-07-06
    body: "it works fine in suse 7.1"
    author: "Rick Kreikebaum"
  - subject: "Re: Not Cool Konqueror !"
    date: 2001-07-06
    body: "Java on Konqueror works just fine on my Mandrake 8.0 running KDE 2.2beta1. I just downloaded the file IBMJava2-SDK-1.3-7.0.i386.rpm from IBM website and installed it using \"rpm -Uvh\", and on Konqueror settings I used \"/opt/IBMJava2-13/bin/java\" as the path to the java executable. That was all, java was from then working flowlessly on Konqueror. IBM JDK is also MUCH faster then Sun's."
    author: "Pedro Ziviani"
  - subject: "Re: Not Cool Konqueror !"
    date: 2001-07-07
    body: ">IBM JDK is also MUCH faster then Sun's.\n\nnot true anymore (atleast here) ... (but bugs in the sun implementation keep me from switching back to sun)"
    author: "ik"
  - subject: "Re: Cool Konqueror"
    date: 2001-07-06
    body: "file manager: right mouse button menu on a file includes the copy option. select it, then do a paste in a text editor or a konsole and see what you get =)\n\njava/flash installed by default: if installing binaries that is up to your distributor; if you are installing from source and have lestiff for plugins and a good jre you should be fine."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-05
    body: "The startup-slowness reminded me.... I have a weird problem with my 2.1.2. When KDE starts up, the \"restoring session\" (if I remember correctly. the last think it loads when KDE starts) flashes for a long time. If I try to launch applications, it disappears and everything works like it should. Not fatal, but annoying."
    author: "Janne"
  - subject: "The File Dialog - superbe!"
    date: 2001-07-05
    body: "This is great - customisable to ones best behaviour \ncool stuff really!"
    author: "ac"
  - subject: "Problems with SSL (HTTPS etc.) support?"
    date: 2001-07-05
    body: "Note - this version (KDE 2.2beta1) requires the development package in OpenSSL to *run* (not to do development). If you want HTTPS sites to work, install the openssl-devel RPM package on RedHat and other RPM-based systems, or libssl-dev package on Debian-based systems."
    author: "Toastie"
  - subject: "Re: Problems with SSL (HTTPS etc.) support?"
    date: 2001-07-06
    body: "Actually this should not be the case, but it turns out to be so for some people.  What it needs is the .so file.  You can make a symlink to the .so.0 files for libcrypto and libssl as a workaround.  I put the .so.0 filename into CVS for the final release so this shouldn't be an issue anymore.  Sorry guys...."
    author: "George Staikos"
  - subject: "Re: Problems with SSL (HTTPS etc.) support?"
    date: 2005-04-04
    body: "openssl-0.9.6b-8\nopenssl095a-0.9.5a-11\nopenssl-devel-0.9.6b-8\nopenssl096-0.9.6-6\nperl-Crypt-SSLeay-0.35-15\nopenssl-perl-0.9.6b-8\n\nperl-5.6.0-17\nperl-Digest-MD5-2.13-1\nperl-MIME-Base64-2.12-6\nperl-libwww-perl-5.53-3\nperl-libxml-perl-0.07-5\ngroff-perl-1.17.2-3\nperl-DateManip-5.39-5\nperl-HTML-Tagset-3.03-3\nperl-libnet-1.0703-6\nperl-Parse-Yapp-1.04-3\nperl-URI-1.12-5\nperl-XML-Encoding-1.01-2\nperl-XML-Parser-2.30-7\nperl-libxml-enno-1.02-5\nperl-XML-Twig-2.02-2\nperl-Crypt-SSLeay-0.35-15\nperl-HTML-Parser-3.25-2\nperl-Storable-0.6.11-6\nperl-XML-Grove-0.46alpha-3\nperl-XML-Dumper-0.4-5\nopenssl-perl-0.9.6b-8\n\n\nI have installed the above packages on my system but still I am not able to uses https\n\nKindly Suggest ne the Solution"
    author: "Raghavendra"
  - subject: "Compiling KDE 2.2beta1 on solaris with gcc 2.95.2"
    date: 2001-07-05
    body: "Here are some of the problems I've encountered while trying to get things to compile.\nNOTE: I have not yet started this version\n\nar assumed to be gnu-tar in kdeartworks\n\nkdenetworks:\nadded to lanbrowsing/lisa/netscanner.cpp\n#ifndef INADDR_NONE\n#define INADDR_NONE (-1)\n#endif\n\nin lanbrowsing/kcmlisa/findnic.cpp to get /usr/include/sys/sockio.h included by ioctl.h :\n#ifdef USE_SOLARIS\n#define BSD_COMP\n#endif\n\n\nin kdevelop kdevelop/Makefile need to change LEXLIB to\nLEXLIB=-lfl\n\nWhile not strictly part of KDE 2.2beta1 -\nin koffice kivio/kiviopart/ui/kivioabout.cpp line 3304 it seems that the license string\nis too big and does not get a closing \") ); \nIs this a limitation of the number of chars the sed command can take on solaris ?"
    author: "CPH"
  - subject: "Re: Compiling KDE 2.2beta1 on solaris with gcc 2.95.2"
    date: 2001-07-05
    body: "It's the stupid Solaris sed which breaks the kivio compile. I had to use GNU sed which works fine."
    author: "kk"
  - subject: "Konqueror does not work"
    date: 2001-07-05
    body: "When I upgrade to the new version, konqueror stops working.  All I get is an error message \"Unknown Host [sitename]\" for every site I browse to.  I know that it is related to KDE because browsing works fine in KDE 2.1.1, and I can got back to KDE 2.1.1 and browsing will work fine again.\n\nSystem:\nMandrake 8.0\nKDE 2.2beta2 -- built from source\n\nHas anyone else seen this problem?\n\nJon Doud"
    author: "Jon Doud"
  - subject: "Re: Konqueror does not work"
    date: 2001-07-06
    body: "if you have both versions installed on the same machine, my guess is that when you are running the new beta, libraries from the older install are still being loaded (instead of the new beta ones) due to them being in your path..."
    author: "Aaron J. Seigo"
  - subject: "No cute release name?"
    date: 2001-07-05
    body: "It seems like all previous KDE releases had a nickname for the release.  Have they ended this practise, or did I miss the release name this time?\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: No cute release name?"
    date: 2001-07-06
    body: "I Don't know, too.\n\nBut if i could choose a name for it i would name it \"Kool\" :-)\n\nThanks, Tom !"
    author: "Tom"
  - subject: "Re: No cute release name?"
    date: 2001-07-06
    body: "Or maybe Kute?"
    author: "Michael"
  - subject: "Re: No cute release name?"
    date: 2001-07-06
    body: "Or maybe Kute?"
    author: "Michael"
  - subject: "Re: No cute release name?"
    date: 2001-07-09
    body: "Maybe they've grown up and they don't need anymore names to get attention? ;)"
    author: "lokem"
  - subject: "Congratulations KDE Team"
    date: 2001-07-06
    body: "Once you use this, you cannot go back!  Everything else is inferior now."
    author: "KDE User"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "Please could someone tell me why kdelibs complains that I need libxml2-2.3.9 or >  even though I've already got libxml2-2.3.13 and the devel package as well."
    author: "saj"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "look for xml2-config, which will probably be in /usr/bin. It is a shell script and when the version number of libxml2 is being checked by the KDELibs ./configure script it returns a value on the library's behalf.\n\nlook for something like :-\n\n--version)\n    echo 2.3.6\n    exit 0\n    ;;\n\nchange the returned version number to the one you have and all should be well."
    author: "Dave Brooke"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "Can someone please help me with installing this newest beta on Mandrake 8.0?  I have a fairly standard installation of Mandrake on a Toshiba Satellite 2805-S202 laptop.  I downloaded all of the RPM's for Mandrake 8.0, and tried to use rpm -Uvh on the new kdelibs.  It had several broken dependancies.  I tried to use MandrakeUpdate, but for some reason I got a \"all packages unreadable\" error.  I was logged in as root when I ran MandrakeUpdate and also when I ran rpm -Uvh.  Is there an rpm tool which just automatically runs around and finds and installs all of the dependancies for you (other than MandrakeUpdate which I can't seem to get to work)?  Otherwise, this is going to take 10 years to fix!  \n\nErik"
    author: "Erik Hill"
  - subject: "rpm flags for MAndrake 8.0 installation"
    date: 2001-07-06
    body: "Just use \"-Uvh\" and \"--force --nodeps\" to install the rpm packages and everything will work fine. It worked for me, on Mandrake 8.0."
    author: "Pedro Ziviani"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-07
    body: "Whenever updating a mass of big packages that you're sure you meet all of the basic requirments for the --force --nodeps options are your friends. Also, I doesn't hurt to drop down to the console and remove your old KDE packages. This isn't essential, but somtimes when RPM hits an old (non $HOME) config file it will leave it alone, and install the new file as a *.rpmnew. This is not always a good thing. Beta 1 is sweet, I've been using it since the day the Mandrake RPMs were released, and it's more stable than most MS products at the end of their life cycle, seems quicker (than previous KDEs) too. . ."
    author: "James Spencer"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-07
    body: "Yes, that's what I finally did, and it WORKED!  Yay!  Anyway, it's too bad that the packages don't have dependancies which reflect their actual dependancies (the specific versions of everything that they need to run on, and no later) but the --force --nodeps worked fine.  \n\nThings crashed a bit until I reset X-Windows (no idea why) and I still get a crash or two now and then (whereas the last version seemed a bit more stable) so I'm sure something didn't quite go in right, will research it.  \n\nSpecifically, the sound mixer doesn't work (but aumix does).  I need the KDE sound mixer to work so that it can correctly reset its settings when I go into KDE however.  Noatun can't seem to play mp3's or ogg files right now, so something there is also misconfigured.\n\nI also updated KOffice.  Wow, lot of progress there too.  KOffice is nearly useable now, though I'm very picky about crashing, so I don't consider it quite ready for prime time.  In my mind, the number of acceptable crashes per year under extreme conditions and constant use is ZERO.\n\nOther than these small problems, a very nice improvement.  I've noticed that the number of times I've said to myself (over the years) \"I can't do this in Linux, I need to load Windows back up\" is going down and down and the number of times I say, \"I can't do this in Windows, I need Linux\" is going up and up.  After I lost the contents of my HD one day, I had to reinstall Windows ME and Mandrake 8.0.  I got a kick out of the fact that Mandrake 8.0 was _easier_ (note the word, \"easier\" in this sentence) to get installed and working with my hardware than Windows ME was on exactly the same hardware (Toshiba 2805-S202), AND WINDOWS ME CAME WITH THE COMPUTER.\n\nThe one thing I can't do in Linux that I need Windows for is writing to my CD-RW, which is a USB connected Iomega ZIP_RW.  Also, for some reason openoffice 6.27 hangs X-Windows pretty hard (but not the Linux kernel) immediately after starting the install process, so I hang on every KOffice release now, as I'll go crazy without a good office suite.\n\nErik Hill"
    author: "Erik Hill"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "This one rocks!! \nIt's a never ending story, KDE team always makes me feel like a little child with every new release. After removing old files in /tmp KDE started with a nice config dialog and full screen antialiasing, fading menues, preview of audio-files (although audiocd slave still does not work), greater speed and a much more professional look. This peace of code is so damn good it should be commercial. ;)"
    author: "Marcus Reuss"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-06
    body: "Check the permissions on your generic SCSI device. You need both read and write permission in order for this ioslave to work. Unfortunately the 3rd party library used by the code has very poor error reporting, so it is difficult for the slave to give decent error messages right now."
    author: "Richard Moore"
  - subject: "Problem compiling kdebase package"
    date: 2001-07-06
    body: "it seems that a header file is missing in kcontrol/input :\nkmousedlg.h\ndoes anybody else miss this file ?  wonder what happend to it. hm.\ni downloaded the src archives of the kde2.2beta1 release.\n\nthanks\ndromtom"
    author: "Dromtom"
  - subject: "Re: Problem compiling kdebase package"
    date: 2001-07-08
    body: "Its there on mine.  Ill reproduce the file below, but no promises on how well it stays formatted...\n\nBegin kmousedlg.h\n-------------------\n/****************************************************************************\n** Form interface generated from reading ui file './kmousedlg.ui'\n**\n** Created: Sat Jul 7 01:57:20 2001\n**      by:  The User Interface Compiler (uic)\n**\n** WARNING! All changes made in this file will be lost!\n****************************************************************************/\n#ifndef KMOUSEDLG_H\n#define KMOUSEDLG_H\n\n#include <qvariant.h>\n#include <qwidget.h>\nclass QVBoxLayout;\nclass QHBoxLayout;\nclass QGridLayout;\nclass QButtonGroup;\nclass QCheckBox;\nclass QFrame;\nclass QLabel;\nclass QRadioButton;\nclass QSlider;\n\nclass KMouseDlg : public QWidget\n{ \n    Q_OBJECT\n\npublic:\n    KMouseDlg( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );\n    ~KMouseDlg();\n\n    QButtonGroup* handedBox;\n    QRadioButton* rightHanded;\n    QRadioButton* leftHanded;\n    QLabel* mousePix;\n    QButtonGroup* ButtonGroup5;\n    QCheckBox* cbAutoSelect;\n    QRadioButton* cbCursor;\n    QRadioButton* doubleClick;\n    QFrame* Line1;\n    QLabel* lDelay;\n    QLabel* lb_short;\n    QLabel* lb_long;\n    QSlider* slAutoSelect;\n    QCheckBox* cbVisualActivate;\n    QCheckBox* cbLargeCursor;\n\nprotected:\n    QVBoxLayout* KMouseDlgLayout;\n    QGridLayout* handedBoxLayout;\n    QGridLayout* ButtonGroup5Layout;\n};\n\n#endif // KMOUSEDLG_H\n\n----------------------\nEnd kmousedlg.h"
    author: "CiAsA Boark"
  - subject: "Re: Problem compiling kdebase package"
    date: 2003-08-09
    body: "kmousedlh.h should be generated by kmousedlg.ui. Doing a build of base it's the first time you invoke this rule. My hunch is that your make is not GNU make, because some others can't cope with invoking this rule (are you using *BSD by any chance, for example?). You can make the file by going into the \"kcontrol.input\" directory and doing \"make kmousedlg.h\" but you'll soon become bored of doing this. Making with gmake might be more sensible.\n"
    author: "Dan Sheppard"
  - subject: "SuSE RPMs disappeared?"
    date: 2001-07-06
    body: "What happened to the SuSE RPMs?  The directories are now empty."
    author: "Evan \"JabberWokky\" E."
  - subject: "Suse RPM problems"
    date: 2001-07-06
    body: "I took home yesterday the Suse RPM's for 7.1 and I tried to upgrade my kde 2.1.1 (with 2.1.2 kdelibs). Due to some dependency and conflict problems after a while I've uninstalled the old kde, I've removed the /opt/kde2 dir and installed the 2.2b1 again. I also created a new user in order to have a clean $HOME dir. But I have aproblem with noatun/arts. When KDE starts, I can hear the startup sound and I can play MP3's with xmms if I select the \"arts\" output plugin. But if I start the noatun it complains the unix:///tmp/mcop-usernam/mcop* file is not avaliable or something like that (I don't know the exact error message, because I don't have my laptop here). Ater this, even the xmms refuses to work, complaining about an arts initialization error. \n Another problem, which may be releated is, that if I enable the debug messages for arts, sometimes (during login, or when restarting the artsd), I got a lot (~120) error dialog boxes with a message like \"file format in /opt/kde2/share/sounds/ is not supported/avaliable\" (sorry again for the incorrect message text), altough the startup sound is in that directory, and I can play them by using \"cat filename > /dev/dsp\". Any idea? Should I send a bug report (of course, after I make some notes about the exact situation and error messages)?"
    author: "Andras"
  - subject: "Re: Suse RPM problems"
    date: 2001-07-06
    body: "I've had the same Problems.\n\nI think the SuSE 7.1 mulitmedia-RPM is bad compiled. After downloading the kde-multimedia-package as source and compiling and installing noatun from that it worked fine.\n\nAnother problem with the precompiled SuSE 7.1 package was that audiocd:/ did not work on my machine. compiling it from source code on my own gave success."
    author: "Tom"
  - subject: "Re: Suse RPM problems"
    date: 2001-07-09
    body: "Later on I've found the problem. You must use the updated libaduiofile library (I think it's audiofile rpm in Suse), and also install the libogg and libvorbis (to get the Ogg Vorbis plugin working). For me it solved the multimedia problems. I also do not have the KDM login problem (I can write my login name and password also at the first time), but I'm wondering why was the \"Console mode\" option removed from the KDM shutdown menu."
    author: "Andras"
  - subject: "A couple of questions about Konqueror"
    date: 2001-07-06
    body: "I think Konqueror is great, but there are some things that annoys me:\nWhy is it that once a image has started loading it is impossible to interrupt the download? The animated gif support is also pretty bad, sometimes sucking up 100% of the CPU and making the gifs animate way too fast. Is this a bug in QT? Apart from this it rocks."
    author: "Konqi"
  - subject: "Re: A couple of questions about Konqueror"
    date: 2001-07-07
    body: "> The animated gif support is also pretty bad, \n> sometimes sucking up 100% of the CPU and \n> making the gifs animate way too fast.\n\nThis has bugged me on several internet sites, at least animations can be disabled in the upcoming 2.2 release.\n\nJohan V."
    author: "Johan Veenstra"
  - subject: "Re: A couple of questions about Konqueror"
    date: 2001-07-15
    body: "Hi\nI had the same problem with Konqueror (2.1.1) and animated gifs, until I have installed XFree 4.03. Now it seems to be all right. Maybe you should check this out...\nBye"
    author: "Burna"
  - subject: "Red Carpet"
    date: 2001-07-06
    body: "Will KDE packages ever appear on a Red Carpet channel?\nI don't see the problem and I really don't see why Ximian will try to stop that."
    author: "ac"
  - subject: "Re: Red Carpet"
    date: 2001-07-06
    body: "$imian want$ big buck$ for that $ort of thing.  if it'$ not gnome forget it."
    author: "another ac"
  - subject: "Re: Red Carpet"
    date: 2001-07-09
    body: "And you know that for a fact?\n\n\n/Richard"
    author: "Richard"
  - subject: "Re: Red Carpet"
    date: 2001-07-09
    body: "Come on are you so deceaved?"
    author: "craig"
  - subject: "Looks great!"
    date: 2001-07-06
    body: "THe KDE artists team have _really_ done a great job with this release. THe personalizer just looks scrumptious!"
    author: "Rikard Anglerud"
  - subject: "Re: Looks great!"
    date: 2001-07-06
    body: "Agree. Quertz & Tackat great job. Default background picture is awesome, dot.kde.org's new logo is beautiful, icons are eye candy, have I forgotten something? :)\n\nOoff topic, some icons for Konqueror toolbar are missing.\nCan anyone tell me the proper name for them, so I can create some for my everyday use:\nFor example: Show bookmark toolbar icon is missing, Show extra toolbar is missing etc."
    author: "Antialias"
  - subject: "Problem with font selection"
    date: 2001-07-06
    body: "I have another problem with the SuSE 7.1 packages; the font selection does not work - no font names are listed.\n\nDid anybody else experience that ?"
    author: "Bastardo"
  - subject: "Re: Problem with font selection"
    date: 2001-07-07
    body: "Do you have Anti-Aliasing enabled? If so, you've got something messed up with your /etc/X11/XftConfig."
    author: "David G. Watson"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-07
    body: "Anyone else thing the flashing black and white background that's hour-glass-like is about as attractive as an animated gif?  \n\nI think it's a great idea to have the symbol indicating the application, but I could do without the flashing.  Is there a way to turn it off?  I somehow magically hoped this would have disappeared from the recent CVS build that I did."
    author: "Scott"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-07
    body: "Look at Control Center/Look&Feel/Launch Feedback and turn off the flashing."
    author: "Someone"
  - subject: "First impressions on KDE 2.2beta1"
    date: 2001-07-07
    body: "Lots of nice surprises on this release... even though it's not quite there yet, JavaScript support in Konqueror has improved a lot and now I can finally do everything on my Yahoo Mail account through Konqui. The \"Archive Webpages\" option also rocks. I was pleased to see the many improvements on both Kasbar and the Dock Application both, both work flawlessly now. Konsole finally handles transparent backgrounds properly. I like the transparent menus on the \"Multigradient\" style, but it would be nice to be able to inverse it, i.e. having a bright background and dark text, making the menus look more like MacOS X's. In all I must say I'm very pleased with KDE 2.2beta1, well done KDE-team!"
    author: "Pedro Ziviani"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-07
    body: "The Kdebase package for Mandrake 8.0 in ftp site has 36 MB.\nThe kdebase for SUSE  has 15 MB.\nThe same is for the other packages.\nWhich are the differences? \nThanks."
    author: "Daniel"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-07
    body: "One may have been compiled with debugging turned on, the other with it off."
    author: "Jon"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-08
    body: "Yes, IIRC, Mandrake has decided not to remove debugging information from Alpha and Beta packages to make error-reporting easier. Unfortunately, it makes them huge.."
    author: "A Sad Person"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-07
    body: "I have a laptop and when I installed this new kde beta, my sub-pixel rendering stopped working.  Regular anti-aliasing works, but even though my Xresources is still set correctly (Xft.rgba: rgb) it will not render sub-pixel.  Does anyone know what might be wrong?"
    author: "Erika Hill"
  - subject: "Memory usage?"
    date: 2001-07-08
    body: "Did they lower the memory usage on this thing?  I was swapping on a system with 384MB of memory with the previous KDE."
    author: "Vincent Janelle"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-08
    body: "What's the reason for all \"unstable\" versions of KDE having only the right kicker hide button and the stable releases having both or none by option? I think it could be nice to be able to choose between left, right, both or none (replace left/right with top/bottom for vertical panels) in the stable releases. Admittedly, it's not the most necessary feature, though."
    author: "Mattias"
  - subject: "Re: KDE 2.2beta1:  Ready to Roll"
    date: 2001-07-09
    body: "Interesting theory, but KDE 2.2 will also have the left/top hide button disabled by default to prevent new users accidently clicking it when they want to open the K-menu. You can choose between left, right, both or none hide buttons in \"unstable\" and \"stable\" versions: Examine panel preferences, tab \"Hiding\"."
    author: "someone"
  - subject: "Addressbook mix"
    date: 2001-07-10
    body: "I know this is OT, but: WTF is going to be the default addressbook interface for KDE, kab or kaddressbook?\n\nYes I know they both use the same database, but they definitely don't use it in a compatible way! Everything is fine with EMails and so on, but as soon as you want to enter addresses, both programs store it differently in the database.\n\nYou can't use addresses entered with kab in kaddressbook and the other way round. Or it works, but the fields get mixed up.\n\nThis definitely has to be changed before KDE 2.2 is out, Im pretty angry after I entered my whole contacts with 2.2a2 and had to see that the addresses where gone when using 2.2b1's kab.\n\nWhy two similar programs with slightly different functionality and incompatible ways of handling the database?"
    author: "Ineluki"
---
It's finally official:  KDE 2.2beta has been
<A HREF="http://www.kde.org/announcements/announce-2.2-beta1.html">announced</A>. <EM>"With this release, KDE is in a great position to deliver a very strong KDE 2.2 release,"</EM> said <A HREF="http://www.kde.org/people/waldo.html">Waldo Bastian</A>, the KDE 2.2 release coordinator.  <EM>"With support for IMAP, the totally new printing framework and improved proxy support, KDE 2.2 will be an excellent foundation for the desktop needs of many businesses."</EM> Some other goodies:  KMail now can send mails without
blocking. Konqueror enhancements include stopping
animated GIFs (thank you, thank you!!), "Send File" and "Send Link"
options, new file previews and lots of new plugins.  Noatun boasts
an improved plugin architecture and some new visualization plugins.  KWin has Xinerama support.  A number of new applications are part of the package, such as KPersonalizer (desktop configuration) and Kooka (scanning).
For a longer list, read the
<A HREF="http://www.kde.org/announcements/announce-2.2-beta1.html">announcement</A> (also attached below),
and for a really long list of improvements, read the
<A HREF="http://www.kde.org/announcements/changelog2_1to2_2.html">ChangeLog</A>. As always, enjoy, and thanks to the KDE "we never sleep" Team!






<!--break-->
<P>&nbsp;</P>
<P>DATELINE JULY 4, 2001</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">New KDE Beta Released for Linux Desktop</H3>
<P><STRONG>KDE Ships Beta of Leading Desktop with Advanced Web Browser, Anti-Aliased Font Capabilities for Linux and Other UNIXes</STRONG></P>
<P>July 4, 2001 (The INTERNET).
The <A HREF="http://www.kde.org/">KDE
Project</A> today announced the release of KDE 2.2beta1,
a powerful and easy-to-use Internet-enabled desktop for Linux. KDE
features <A HREF="http://www.konqueror.org/">Konqueror</A>, a
state-of-the-art web browser, as an integrated
component of its user-friendly desktop environment, as well as
<A HREF="http://www.kdevelop.org/">KDevelop</A>,
an advanced IDE, as a central component of KDE's powerful
development environment.  KDE 2.2beta1 completely integrates the
<A HREF="http://www.xfree.org/4.1.0/fonts5.html#30">XFree anti-aliased
font extensions</A> and can provide
a fully anti-aliased font-enabled desktop.
</P>
<P>
The primary goals of this release, which follows two months after the release
of KDE 2.1.2, are to provide a preview of KDE 2.2 and to involve
users and developers who wish to request/implement missing features or
identify problems. Code development is currently focused on stabilizing
KDE 2.2, scheduled for final release later this quarter.
Despite all the improvements, KDE 2.2 will be binary compatible with KDE 2.0.
</P>
<P>
Major changes to KDE since the last stable release are
<A HREF="#changelog">enumerated below</A>.  In addition, a
<A HREF="http://www.kde.org/announcements/changelog2_1to2_2.html">more
thorough list of changes</A> since the KDE 2.1.1 release,
<!-- NOT READY YET
and a <A HREF="http://www.kde.org/info/2.2beta1.html">FAQ about the release</A>, are
-->
as well as <A HREF="http://www.kde.com/Information/Fonts/index.php">information
on using anti-aliased fonts</A> with KDE, are available at the KDE
<A HREF="http://www.kde.org/">website</A>.
</P>
<P>
KDE 2.2beta1 and all its components are available for free under
Open Source licenses from the KDE
<A HREF="http://ftp.kde.org/unstable/2.2beta1/">server</A>
and its <A HREF="http://www.kde.org/mirrors.html">mirrors</A> (including
many <A HREF="#Package_Locations">precompiled packages</A>) and can
also be obtained on <A HREF="http://www.kde.org/cdrom.html">CD-ROM</A>.
As a result of the dedicated efforts of hundreds of translators,
KDE 2.2beta1 is available in
<A HREF="http://i18n.kde.org/teams/distributed.html">38 languages and
dialects</A>.  KDE 2.2beta1 ships with the core KDE
libraries, the core desktop environment (including Konqueror), developer
packages (including KDevelop), as well
as the over 100 applications from the other
standard base KDE packages (addons, administration, artwork, bindings, games,
graphics, multimedia, SDK, network, PIM and utilities).
</P>
<P>
<A NAME="changelog"></A><H4>Incremental Changelog</H4>
</P>
<P>
The following are the major improvements, enhancements and fixes since the
KDE 2.1 release earlier this year:
<UL>
<LI>KDE has added a new plugin-based printing framework, which features:
<UL>
  <LI>support for <A HREF="http://cups.sourceforge.net/">CUPS</A>,
      lpr and rlpr, though support for other printing systems can be
      easily added;</LI>
  <LI>a Control Center module for managing printers
      (add/remove/enable/disable/configure), print servers (currently
      only CUPS), and print jobs (cancel/hold/move);</LI>
  <LI>a configurable filter mechanism (for setting number of pages per
      sheet, etc.);</LI>
  <LI>a print job viewer for the KDE panel's system tray; and</LI>
  <LI>support for configurable "pseudo-printers", such as fax machines,
      email, etc.;</LI>
</UL>
<LI>Konqueror, the KDE file manager and web browser, sports a number of
    new features:</LI>
<UL>
  <LI>HTML and JavaScript handling has been improved and made faster;</LI>
  <LI>Ability to stop animated images;</LI>
  <LI>New file previews, including PDF, PostScript, and sound files;</LI>
  <LI>New "Send File" and "Send Link" options in the <TT>File</TT>menu;</LI>
  <LI>Added a number of new plugins:</LI>
  <UL>
    <LI>A web archiver for downloading and saving an entire web page, including
        images, in an archive for offline viewing;</LI>
    <LI><A HREF="http://babelfish.altavista.com/">Babelfish</A>
        translation of web pages;</LI>
    <LI>A directory filter for displaying only specified mimetypes (such as
        images);</LI>
    <LI>A quick User Agent changer to get Konqueror to work with websites
        that discriminate based on the browser you are using;</LI>
    <LI>An HTML validator using <A HREF="http://www.w3c.org/">W3C</A> to
        validate the XML/HTML of a webpage (useful for web developers);
        and</LI>
    <LI>A DOM tree-viewer for viewing the DOM structure of a web page (useful
        for web developers);</LI>
  </UL>
  <LI>New configuration for user-defined CSS stylesheets;</LI>
  <LI>Saving toolbar layout in the profile;</LI>
  <LI>A new "Most Often Visited" URL in the <TT>Go</TT> menu; and</LI>
  <LI>Many other enhancements, usability improvements and bug fixes.</LI>
</UL>
<LI>KDevelop, the KDE IDE, offers a number of new features:</LI>
<UL>
  <LI>Enhanced user interface with an MDI structure, which supports multiple
      views of the same file;</LI>
  <LI>Added new templates for implementing a KDE/Qt style library and Control
      Center modules;</LI>
  <LI>Updated the kde-common/admin copy (admin.tar.gz); and</LI>
  <LI>Extended the user manual to reflect the new GUI layout and added
      a chapter for using Qt Designer with KDevelop projects;</LI>
</UL>
<LI>KMail, the KDE mail client, has a number of improvements:</LI>
<UL>
  <LI>Added support for IMAP mail servers;</LI>
  <LI>Added support for SSL and TSL for POP3 mail servers;</LI>
  <LI>Added configuration of SASL and APOP authentication;</LI>
  <LI>Made mail-sending non-blocking;</LI>
  <LI>Improved performance for very large folders;</LI>
  <LI>Added message scoring;</LI>
  <LI>Improved the filter dialog and implemented automatic filter
      creation;</LI>
  <LI>Implemented quoting only selected parts of an email on a reply;</LI>
  <LI>Implemented forwarding emails as attachments; and</LI>
  <LI>Added support for multiple PGP (encryption) identities;</LI>
</UL>
<LI>New Control Center modules:</LI>
<UL>
  <LI>Listing USB information (attached devices);</LI>
  <LI>Configuring window manager decoration;</LI>
  <LI>Configuring application startup notification;</LI>
  <LI>Configuring user-defined CSS stylesheets;</LI>
  <LI>Configuring automatic audio-CD ripping (MP3, Ogg Vorbis); and</LI>
  <LI>Configuring key bindings;</LI>
</UL>
<LI>Added Kandy, a synchronization tool for mobile phones and the KDE address
    book, and improved KPilot address book synchronization;</LI>
<LI>KOrganizer, the KDE personal organizer, has a number of improvements:</LI>
<UL>
  <LI>Added a "What's Next" view;</LI>
  <LI>Added a journal feature;</LI>
  <LI>Switched to using the industry-standard iCalendar as the default file
      format;</LI>
  <LI>Added remote calendar support; and</LI>
  <LI>Added ability to send events using KMail, the KDE mail client;</LI>
</UL>
<LI>Noatun, the KDE multimedia player, sports a number of new features:</LI>
<UL>
  <LI>Improved the plugin architecture and added a number of new plugins:</LI>
  <UL>
    <LI>An Alarm plugin for playing music at a specified time;</LI>
    <LI>A Blurscope plugin which creates an SDL-based blurred monoscope;</LI>
    <LI>A Luckytag plugin for guessing titles based on filenames;</LI>
    <LI>A Noatun Madness plugin, which moves the Noatun window in sync with
        the music being played;</LI>
    <LI>A Synaescope plugin, based on
       <A HREF="http://yoyo.cc.monash.edu.au/~pfh/synaesthesia.html">Synaesthesia</A>,
       which provides an impressive SDL-based visualization; and</LI>
    <LI>A Tyler plugin, which is similar to
        <A HREF="http://www.xmms.org/">XMMS</A>'s
        <A HREF="http://www.xmms.org/plugins_vis.html">Infinity</A>;</LI>
  </UL>
  <LI>Added support for pre-amplification; and</LI>
  <LI>Added support for hardware mixers;</LI>
</UL>
<LI>Added a Personalization wizard (KPersonalizer) to configure the desktop
    settings easily;</LI>
<LI>Added <A HREF="http://www.rhrk.uni-kl.de/~gebauerc/kdict/">KDict</A>,
    a powerful graphical dictionary client;</LI>
<LI>Added KDE-wide scanning support with the application Kooka;</LI>
<LI>Replaced the default editor KWrite with the more advanced editor Kate,
    which provides split views and basic project management;</LI>
<LI>The window manager now supports Xinerama (multi-headed displays);</LI>
<LI>Improved the file dialog, including mimetype-based file previews;</LI>
<LI>Improved the configurability of the KDE panel;</LI>
<LI>Added IPv6 and socks support to the core libraries;</LI>
<LI>Improved application startup:
<UL>
  <LI>applications are now placed on the desktop from which they were
      launched; and</LI>
  <LI>startup notification can be configured with a new Control Center module,
      with options including a busy cursor next to the application's icon;</LI>
</UL>
<LI>Improved icons and added new 64x64 icons;</LI>
<LI>New window manager decoration styles (quartz, IceWM themes, MWM, Web);</LI>
<LI>Improved the help system, which is now XML-based;</LI>
<LI>Added support for the Meta and AltGr keys for shortcuts; and</LI>
<LI>Made many other usability improvements.</LI>
</UL>
</P>
<P>
For a much more complete list, please read the
<A HREF="http://www.kde.org/announcements/changelog2_1to2_2.html">official
ChangeLog</A>.
</P>
<P>
<H4>Downloading and Compiling KDE</H4>
</P>
<P>
<A NAME="Source_Code"></A><EM>Source Packages</EM>.
The source packages for KDE 2.2beta1 are available for free download at
<A HREF="http://ftp.kde.org/unstable/2.2beta1/src/">http://ftp.kde.org/unstable/2.2beta1/src/</A>
or in the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>.
</P>
<P>
<A NAME="Source_Code-Library_Requirements"></A><EM>Library Requirements.</EM>
KDE 2.2beta1 requires at least qt-x11-2.2.4, which is available from
<A HREF="http://www.trolltech.com/">Trolltech</A> at
<A HREF="ftp://ftp.trolltech.com/qt/source/">ftp://ftp.trolltech.com/qt/source/</A>
under the name <A HREF="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.4.tar.gz">qt-x11-2.2.4.tar.gz</A>,
although
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.1.tar.gz">qt-2.3.1</A>is recommended (for anti-aliased fonts,
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</A>and <A HREF="ftp://ftp.xfree86.org/pub/XFree86/4.0.3/">XFree 4.0.3</A> or
newer is required).
KDE 2.2beta1 will not work with versions of Qt older than 2.2.4.
</P>
<P>
<EM>Compiler Requirements</EM>.  Please note that
<A HREF="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc 3.0</A> is not
recommended for compilation of KDE 2.2beta1. Several known miscompilations
of production C++ code (such as virtual inheritance, which is used in <A HREF="http://www.arts-project.org/">aRts</A>) occur with this compiler.
The problems are mostly known and the KDE team is working with the gcc team
to fix them.
</P>
<P>
<EM>Further Instructions</EM>.
For further instructions on compiling and installing KDE, please consult
the <A HREF="http://www.kde.org/install-source.html">installation
instructions</A> and, if you encounter problems, the
<A HREF="http://www.kde.org/compilationfaq.html">compilation FAQ</A>.
</P>
<P>
<H4>Installing Binary Packages</H4>
</P>
<P>
<EM>Binary Packages</EM>.
Some distributors choose to provide binary packages of KDE for certain
versions of their distribution.  Some of these binary packages for
KDE 2.2beta1 will be available for free download under
<A HREF="http://ftp.kde.org/unstable/2.2beta1/">http://ftp.kde.org/unstable/2.2beta1/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution (if you cannot find a binary package for your distribution,
please read the <A HREF="http://dot.kde.org/986933826/">KDE Binary Package
Policy</A>).
</P>
<P>
<EM>Library Requirements</EM>.
The library requirements for a particular binary package vary with the
system on which the package was compiled.  Please bear in mind that
some binary packages may require a newer version of Qt and/or KDE
than was included with the particular version of a distribution for
which the binary package is listed below (e.g., LinuxDistro 8.0 may have
shipped with qt-2.2.3 but the packages below may require qt-2.3.x).  For
general library requirements for KDE, please see the text at
<A HREF="#Source_Code-Library_Requirements">Source Code - Library
Requirements</A> above.
</P>
<P>
<A NAME="Package_Locations"><EM>Package Locations</EM></A>.
At the time of this release, pre-compiled packages are available for:
</P>
<P>
<UL>
  <LI><A HREF="http://www.debian.org/">Debian GNU/Linux</A> (package "kde"):  <A HREF="ftp://ftp.debian.org/">ftp.debian.org</A>:  sid (devel) (see also <A HREF="http://http.us.debian.org/debian/pool/main/k/">here</A>)</LI>
  <LI><A HREF="http://www.linux-mandrake.com/en/">Linux-Mandrake</A>
  <UL>
    <LI>8.0:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/Mandrake/8.0/i586/">Intel i586</A></LI>
    <LI>7.2:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/Mandrake/7.2/i586/">Intel i586</A></LI>
  </UL>
  <LI><A HREF="http://www.suse.com/">SuSE Linux</A> (<A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/README">README</A>):
  <UL>
    <LI>7.2:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/i386/7.2/">Intel i386</A> and <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/ia64/7.1/">HP/Intel IA-64</A>; please also check the <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/noarch/">noarch</A> directory for common (language) files</LI>
    <LI>7.1:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/i386/7.1/">Intel i386</A>, <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/ppc/7.1/">PowerPC</A>, <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/sparc/7.1/">Sun Sparc</A> and <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/axp/7.1/">Alpha</A>; please also check the <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/noarch/">noarch</A> directory for common (language) files</LI>
    <LI>7.0:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/i386/7.0/">Intel i386</A>, <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/ppc/7.0/">PowerPC</A> and <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/s390/7.0/">IBM S390</A>; please also check the <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/noarch/">noarch</A> directory for common (language) files</LI>
    <LI>6.4:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/i386/6.4/">Intel i386</A>; please also check the <A HREF="http://ftp.kde.org/unstable/2.2beta1/SuSE/noarch/">noarch</A> directory for common (language) files</LI>
  </UL>
  <LI>Tru64 Systems:  <A HREF="http://ftp.kde.org/unstable/2.2beta1/Tru64/">4.0e,f,g, or 5.x</A> (<A HREF="http://ftp.kde.org/unstable/2.2beta1/Tru64/README.Tru64">README.Tru64</A>)</LI>
</UL>
</P>
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages will become available over the
coming days and weeks.  In particular,
<A HREF="http://www.redhat.com/">RedHat Linux</A> packages should be
available shortly.
</P>
<P>
<H4>About KDE</H4>
</P>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environmentemploying a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.
</P>
<P>
Please visit the KDE family of web sites for the
<A HREF="http://www.kde.org/documentation/faq/index.html">KDE FAQ</A>,
<A HREF="http://www.kde.org/screenshots/kde2shots.html">screenshots</A>,
<A HREF="http://www.koffice.org/">KOffice information</A>,
<A HREF="http://developer.kde.org/documentation/kde2arch.html">developer
information</A> and
a developer's
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html">KDE 1 - KDE 2 porting guide</A>. 
Much more information about KDE is available from KDE's
<A HREF="http://www.kde.org/whatiskde/">web site</A>.
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
KDE and K Desktop Environment are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
PostScript is a registered trademark of Adobe Systems Incorporated.
Trolltech and Qt are trademarks of Trolltech AS.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
granroth@kde.org<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
pour@kde.org<BR>
(1) 917 312 3122
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
faure@kde.org<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
konold@kde.org<BR>
(49) 179 2252249
</TD></TR>
</TABLE>





