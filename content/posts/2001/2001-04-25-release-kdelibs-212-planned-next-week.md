---
title: "Release of kdelibs 2.1.2 Planned Next Week"
date:    2001-04-25
authors:
  - "Dre"
slug:    release-kdelibs-212-planned-next-week
comments:
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-25
    body: "I was hoping that they were going to do a 2.1.2 release, rather than simply charging ahead with 2.2 since there are a number of minor bugs in 2.1.1 that need fixing. \n\nSo hopefully this release will adress a few anoying bugs with konqueror such as the \"protocol: javascript not supported bug\" and the \"lost conection to host\" bugs.\n\nGreat to see Kde is looking out for it's users and moving toward building the most reliable desktop for Linux, as well as the most feature rich"
    author: "richie123"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-25
    body: "I sure hope they will include a kdebase 2.1.2 too... I'd be thrilled if I could upgrade from KDE 2.1 without living with the bugs that prevent applets from loading (at startup) on a child panel, and the bug that causes KDE to crash at startup when you enable anti-aliasing.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-25
    body: "There will be no kdebase 2.1.2.\n\nCrashes with anti-aliasing are unlikely to be caused by KDE itself. See\nhttp://lists.kde.org/?t=98634011600003&w=2&r=1\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-25
    body: "Hi Waldo,\n  Thanks for the tip, it looks like dep has a good article on configuring anti-aliasing  (referenced from the thread you referred me to) that might help me find the problem.\n  Before I race off and switch to KDE 2.1.1 however, do you have any tips on how to fix my the problem with Kicker child panels and KDE 2.1.1? The problem seems to consist of, I add something to a child panel (say a clock or taskbar), restart KDE, and the child panel is empty.\n\n  Thanks,\n       Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-25
    body: "Maybe it'll work if you close everything except your kicker plugins and chose \"restore session when logging in next time\" when logging out? (I did that to make KMix appear in Kicker at startup)"
    author: "Johnny Andersson"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-05-09
    body: "This has been fixed in 2.2, the problem was with the interaction of child panels and auto-hide."
    author: "Michael Driscoll"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-26
    body: "Will there definitely be no kdebase 2.1.2?\nEven if bugs are found in 2.1.1 (and I am\nquite sure there will be some)? Even if users\nsupply fixes for these bugs?\n\nI believe that I am not the only one which\nwould like to have a KDE desktop which becomes\nmore and more stable while staying compatible\n(for example with respect to configuration\nfiles).\n\nUnfortunately I do not have the time to upgrade\nto a completely new version of KDE every few\nmonths and to learn how to do this and that\nwith the new version which was different with\nprevious versions. Apart from that, as we all\nknow, completely new versions usually introduce\nsome new bugs...\n\nI understand that maintaining two branches of\nKDE (HEAD and 2.1 in this case) require some\nresources (manpower, hardware, ...). But do I\nreally have to feel like an oldtimer if I want\nto use a given KDE version (including bug fixes)\nwhich was released half a year ago?\n\nRegards\n    Ingolf"
    author: "Ingolf Steinbach"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-27
    body: "I'm afraid that, IMHO, it wouldn't be a good idea to have your cake and eat it too. Bugs are reported everywhere, and many of the bugs are actually not in kdebase and libs.\n\nYes, new versions introduce new bugs, but minor-minor version updates, aka x.x.+1, are generaly designed to fix bugs. Just look at kde 2.1.1, and all the bugs fixed there.\n\nAlso, frankly, i would imagine it's very difficult to fix bugs, especially UI consistency bugs, if you don't want there to be any changes in the look n feel. As for config files, almost every KDE update (except from the 1.x series to the 2.x series) has been capable of automatically adpating for changes in config files. For instance, konqueror automatically compensated for the format change in the bookmarks system.\n\nUpdating kde is not hard, it's almost always a matter of downloading some RPMs or DEBs for the new versions of kde and qt, and just installing them."
    author: "Carbon"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-05-01
    body: "Your comment stating that you are quite sure there will be some bugs found lead me to believe that you have not yet encountered any bugs.  That being the case, would you not agree that KDE 2.1.1 is already a very stable desktop and sufficient for your needs and hence KDE 2.1.2 is not necessary?\n\nAlso, if you cannot upgrade regularly and if KDE 2.1.2 and KDE 2.2 were both available by the time you next decided to upgrade, would you really download and install KDE 2.1.2?  \n\nKDE is the most stable desktop available and KDE 2.2 is an important evolution.  The new Print and IMAP support are two major enhancements that do not deserve to wait any longer than necessary before being introduced.  Also, it is my understanding that there will be few major core changes(the new Print mechanism is a simple drop in replacement for the old one and I expect it to be quite stable by release time).  Features that were present in KDE 2.1.1 should therefore be as stable as they were in the previous release and any bugs associated with them should be cleared up.  New features will always have bugs, so we will just have to live with them until the next release but they should not interfere with the operation of the program as it is today."
    author: "Rob"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-05-02
    body: "It's amazing how people succeed in not reading what I have and have not written. I did never say that KDE 2.1 is not pretty stable on x86.\n\nWhat I said is that it would be a pity to (IMHO) prematurely exclude another bug-fix release even if it might prove necessary. And even if there were already fixes for these bugs (see my original post).\n\nWithout wanting to step on the developers' feet (who really do a great work!), but there is a well-known OS out there which we all love so desperately and which \"does not have any bugs its users want to get fixed - and if you think you found a bug in version 98 of this OS, why don't you upgrade to version 2k?\".\n\nAnd yes, I would not upgrade to 2.2 if there were 2.1.2 available as I would expect 2.1.2 to be more stable than 2.2. In case of KDE, I am a *user* rather than a developer and cannot spend hours on bug hunting. (And to anticipate some possible replies: I do contribute to free software projects and have contributed to KDE once or twice.)\n\nBTW, there are companies which cannot switch easily to new versions of software they use (be it commercial or non-commercial software) without a significant amount of management activities while installing patches or bugfix-releases is permitted and much easier.\n\nI have not yet located any bugs in KDE 2.1(.1), but I have had some strange behaviour on non-x86 platforms (sparc and m68k) which might be caused by bugs in KDE (these could be for example endianness issues). I don't know when I will have the time to investigate this further.\n\nIMNSHO, software which is \"bugfree by definition\" is rather user unfriendly (see the other OS). As I am not an active KDE developer I cannot (and don't want to) blame anyone for not fixing bugs. But as a user of the software I hope that I will not be left out in the rain if I do not want more features but just bug fixes incorporated in a release which is say a year old.\n\nRegards\n    Ingolf"
    author: "Ingolf Steinbach"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-25
    body: "I, too, think that a KDE 2.1.2 or 2.1.3 release would be nice as there are quite a few minor but nevertheless annoying bugs in KDE 2.1.1 and KDE 2.2 is still some way ahead and will probably introduce some new bugs.\nA rock-solid desktop environment for production use would be nice and KDE 2.1.1 - isn't although it's great, of course."
    author: "Gunter Ohrner"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-26
    body: "Hey, test KDE 2.2 alpha 1! There are many bugfixes added, including the problem with the Newsticker! Only KMail 1.2.1 is very unstable on my computer. But this is the only thing because of which you can call it an alpha release!\nAnd IMHO I think that things like the new printing framework, the IMAP support in kmail and the IPv6 support are required features of a desktop environment for enterprise use!"
    author: "Patrick Werner"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-26
    body: "I did test KDE 2.2alpha1 but only as I'm more interested in experimenting with software than using stable releases... <g>\nIf I'm using KDE for real work I use KDE 2.1.1 and I must say that it's still too buggy.\nAs I already mentioned KDE2 is a great desktop environment (else I would not be using it) but I fear that the developers are more on a feature hunt than hunting bugs. (That's easily understanable to anyone who ever had to debug software, of course. ;-) IMHO a missing feature is less critical than a buggy feature if you're trying to use software to get your work done instead of using it just to try it out or play with it.\nI, in the meantime, do my best to help the developers to catch the bugs by reporting any I can find - more would be beyond my limited time and knowledge. :-("
    author: "Gunter Ohrner"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-27
    body: "Hi! I think a bugfix release of kdelibs is a very good idea and I completely agree that if you are\ninteresting in \"getting your work done\" or writing some programs for KDE instead of\ndeveloping core KDE components themselves, you need these bugfix releases.\nIMHO it would be a good idea to have stable and development branches, like e.g. Linux kernel does.\nWhy KDE team did not adopt this practice?\nKeep up the good work,\nTima."
    author: "Tima Vaisburd"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-26
    body: "Tim, I posted a fix to the child panel issue soon after 2.1.1 was released. Check the kde-devel archives, no patch included (the fix is a one-liner)."
    author: "Catalin Climov"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-26
    body: "Hi Catalin,\n  Thanks for the tip! I'll go check it out.\n\n   -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-28
    body: "Hi Catalin,\n  I tried the patch, and after compiling a few libs from around KDEBASE I was able to successfully get the child panel extention to install on top of my RPM-based install of KDE 2.1.1.\n  Thanks so much for your help!\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "need to re-compile KDE 2.1.1 with new kdelibs ???"
    date: 2001-04-30
    body: "Is it enough just to upgrade kdelibs to 2.1.2 or has kdebase and the rest of KDE 2.1.1 to be re-compiled against kdelibs 2.1.2 ?\n\nMalte"
    author: "Malte"
  - subject: "Re: Release of kdelibs 2.1.2 Planned Next Week"
    date: 2001-04-30
    body: "With limited resources, I'm glad the KDE team is pushing towards more complete functionality over bugless but featureless software.\n\nOnce they've got this surge of functionality out of the way, than I think they should stabilize things more."
    author: "Mateo"
---
The KDE Project is planning to release kdelibs-2.1.2 early next week.  The unscheduled release was prompted by the <A HREF="http://dot.kde.org/988151496/">release today</A> of <A HREF="http://www.koffice.org/">KOffice</A> 1.1beta1, which has problems with the kdelibs from KDE 2.1.1, as well as a security fix for kdesu, resulting from a security bug reported this week.  However, the release will include a number of other bugfixes as well.  Stay tuned.

<!--break-->
