---
title: "On a Kollision Kourse"
date:    2001-02-07
authors:
  - "rmoore"
slug:    kollision-kourse
comments:
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-06
    body: "Well, it's not really a surprise that you KDE guys get such good reviews and write ups.  You have thoroughly deserved it. I'm not a zealot for any window manager, but what you guys have done really sets you apart from the rest of the field.\n\nWell done, and keep up the good work."
    author: "Gareth Williams"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-07
    body: "Perhaps articles from the press could get the \"Press:\" prefix? To differenciate it from real news."
    author: "Matt"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-07
    body: "I'm sorry, will you *forgive* my ignorance. You are right KDE r0x0rz, and Gnome 5uck50r5. I am just a lame zealot and I smell of ham."
    author: "Richard Stallman"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-07
    body: "y'r forkiven"
    author: "k"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-07
    body: "Well. I must say that KDE is bloatware. (Don't wory there are many other bloatware WM's and so on). It takes a lot of system resources and is so slow, no structured way to only install what you want, and so on. Really bloatware. I liked the beta1, beta2, beta3, beta4 versions and of course the KDE 1.0. They rocked, but today? I'm happy with blackbox.... \nI mean, skip all the 3d rendering and alpha shading or what it is , or  atleast make it easy to turn it off.\n\nGood luck with kde 3.0. I have great expectations..."
    author: "anonymous covard"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-09
    body: "KDE2 + the rest of the system takes about 35 MB after starting it. I wouldn't call that bloatware. If you build it properly it runs decently on a Pentium 166 with 166 MB."
    author: "abc"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-09
    body: "That was supposed to read \"on a Pentium 166 with 48 MB\"."
    author: "abc"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-09
    body: "Really?  I run KDE 2.1b2 on a 300 MHz Celeron w/128MB Ram.  If I open a few konsoles and a couple of Konqueror windows, my swap partition gets a really harsh workout.\n\nDon't get me wrong, I like KDE 2 a lot, your post that it should work well with only 166MHz/48MB makes me think I have something set up wrong. \nAny ideas?\n\nAnyway, memory is so cheap now, I just ordered 128M more so I won't have to worry...\n\ncheers,\nJason"
    author: "LMCBoy"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-09
    body: "However, you seem to completely forgot people that own notebook PCs -- that can't just upgrade their memory as easy as you, desktopers, can."
    author: "cefek"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-10
    body: "Sorry, I was just talking about my situation, and didn't mean to imply it was easy for everyone to upgrade.  I meant no offense.\n\ncheers,\nJason"
    author: "LMCBoy"
  - subject: "of course"
    date: 2001-02-12
    body: "I also took this as non offensive :-) just wanted to drop in my 2c."
    author: "cefek"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-10
    body: "I'm running KDE 2.0 on a P366 laptop.  It ran fine when I had 64 MB of RAM, and it runs even better now that I have 192MB.  \n\nThe new Konqueror is significantly faster than the old file manager from KDE 1.0.  \n\nI don't know what people are talking about who say that Gnome is faster.  Sure, the file manager is, but it's not as capable as Konqueror is, either.  For everything else, I have found Gnome more sluggish, which is one reason I usually use KDE.  \n\nIt's unreasonable to expect that KDE be as fast as minimalist window managers, because it does a lot more.  We need a capable desktop to attract newbies, otherwise the only people who run Linux will be those who know already how to do everything from a terminal window.  \n\nI like IceWM when I don't need KDE's features..."
    author: "Jeff"
  - subject: "To answer my own question..."
    date: 2001-02-10
    body: "I read on another message board that aRts can be a big resource hog, so if you think KDE uses too much RAM, you might try disabling aRts.  Of course, this doesn't help you if you use aRts a lot...\n\nJason"
    author: "LMCBoy"
  - subject: "Re: To answer my own question..."
    date: 2002-11-18
    body: "How can I marry with Soheil (my lover)?"
    author: "Arezou"
  - subject: "Re: To answer my own question..."
    date: 2002-11-18
    body: "Soheil ?\nSoheil Ayari, the french Formule 3000 driver ?"
    author: "Julien Olivier"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-09
    body: "35 MB just after starting it may seem decent for you, but it's still way too much. A system running for example a full-fledged icewm takes typically 15MB less than when running a KDE session (at the beginning!). I mean, even Gnome takes less, and the gap is even more visible when you compare KDE and Gnome complex appls (Office, graphics, etc...). We've even reached a point where the guy which makes an heavy use of XEmacs and its extensios to get a full desktop environment needs less resources. (X)Emacs synonymous of economy, did you expect it to happen some day?\n\nDid you know, BTW, that obsolete PCs are among the worst source of harmful pollution in developped countries: quite a good reason for trying to save 10 or 15 megs, don't you think? Bloatware kills the planet!"
    author: "JK"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-10
    body: "Yes, 15 MB is the difference between planetary destruction and survival.\n\nYou'e one of Darth Ballmer's stormtroopers aren't you?"
    author: "reihal"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-11
    body: "I think it's just become the cool thing to say nowadays that anything that's just come out is bloatware.  You do, but I've even seem some that don't even seem to know what the term is and use it (For ex. \"Should I download TweakUI for Win2k or is it just bloatware?\").  Next thing people are going to be saying is 'ls' is just bloatware. If they took out the code for printing in color they could save a whole 281 bytes?!?!?! (not accurate info, used to get a point across).\n\nLets get real people. This isn't the 60's and hard drive space isn't all that scarce. Of course there are some (not many) people who don't have the space to run KDE, or other \"bloatware\" but for them there's plenty of wonderful wm's out like WindowMaker and Afterstep (I've used both and they're both very good).  Just don't expect EVERYBODY to cut all the new nifty features out because there are lots of us with machines that can do it and we want those features."
    author: "MGMorden"
  - subject: "Re: On a Kollision Kourse"
    date: 2001-02-12
    body: "Being first and foremost an heavy XEmacs user, I don't recognize myself as someone who is reluctant to features (that is, useful ones) in the name of small footprint.\n\nHowever, in the case of KDE, we have actual proofs that it is possible to implement functionally similar counterparts to KDE apps with better performances; The panel is huge (look at windowmaker's dock and applets), the editors are huge, while very poor in terms of functionnalities, the window manager is huge, the system monitor is huge -compare ktop with qps or gps, or even gtop-, even the office apps and graphic tools could be smaller -compare with gnumeric or gphoto, for example-. In general, a session is huge. Konqueror is the only exception -and an impressive one-, at least if you don't count these damn kdeinit processes (please, oh please someone, fix them so that they are killed when you close konqueror)."
    author: "JK"
---
There's a very complimentary article about KDE 2.1 on <a href="http://www.linuxtoday.com.au">LinuxToday AU</a>. From the article: <i>"They certainly don't mess around on planet KDE. This week saw the release of the second beta of KDE 2.1 - a major upgrade which adds a huge number of improvements to this impressive, must-have software</i>". You can read the full story
<a href="http://www.linuxtoday.com.au/r/article/jsp/sid/595874">here</a>.
<!--break-->
