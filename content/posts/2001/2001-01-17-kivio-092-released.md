---
title: "Kivio 0.9.2 released"
date:    2001-01-17
authors:
  - "highstick"
slug:    kivio-092-released
comments:
  - subject: "Re: Kivio 0.9.2 released"
    date: 2001-01-17
    body: "If you click the back button and look to right under \"Recent software\" you'll see that Aethera 0.9.0 is out too.\nBusy Kompany, that one ;-)"
    author: "reihal"
  - subject: "Re: Kivio 0.9.2 released"
    date: 2001-01-17
    body: "Seems like apps.kde.com have been kde-dotted by the overwhelming interest ;-)"
    author: "reihal"
  - subject: "Re: Kivio 0.9.2 released"
    date: 2001-01-17
    body: "I did sent the Aethera announcement to dot this morning, but I guess it hasn't hit yet.  I actually sent this Kivio announcement 2 days ago.  Sometimes the stuff shows right up, sometimes it takes a little while :), regardless I think dot is a great site, I hit it more than /. these days."
    author: "Shawn Gordon"
  - subject: "Re: Kivio 0.9.2 released"
    date: 2001-01-17
    body: "Hello again, Shawn. Sorry to have disturbed you plans, I'll shut up about Aethera for now ;-)"
    author: "reihal"
  - subject: "Re: Kivio 0.9.2 released"
    date: 2001-01-17
    body: "Well, the kivio thing was a slight screw-up.\n<p>\nAnyway, we could sure use some more help here at dot.kde.org.  Personally, I've been busy lately, and it seems so have others -- I'll see what I can do about getting some active help for the site.\n<p>\nIt does help if the articles you submit are formatted/phrased nicely already, otherwise we end up with a big blob of text and someone has to find the time to edit it before posting the story.\n<p>\nIn any case please KEEP SUBMITTING ARTICLES! ;-)\n<p>\nThanks,<br>\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Kivio 0.9.2 released"
    date: 2001-01-18
    body: "Can anyone get kivio to work on Mandrake 7.2? The last beta worked ok, but this one doesn't even seem to load..."
    author: "Eron Lloyd"
  - subject: "Re: Kivio 0.9.2 released"
    date: 2001-01-18
    body: "Make sure you have totally removed the prior version, we've updated the FAQ to explain this.  You could also try compiling the source instead, but make sure you have the old version gone first."
    author: "Shawn Gordon"
  - subject: "No luck..."
    date: 2001-01-18
    body: "<p>Hrmm... still erroring out... just spits out it's shutdown code if launched from the terminal.</p>\n\n<pre>\n[elloyd@localhost elloyd]$ kivio\nkdecore (KLibLoader): add loaded lib 0x80966a0\nkivio: KivioConfig - created\nkivio: KivioConfig::KivioConfig() - StencilBackgroundType: 0\nkivio: KivioConfig::KivioConfig() - StencilBackgroundFile:\nkivio: KivioConfig::KivioConfig() - StencilBackgroundColor: 75 210 255\nkdecore (KLibLoader): Deleting KLibLoader 0x808f0c8  unnamed\nkdecore (KLibLoader): The KLibLoader contains the library 0x8099870\nkdecore (KLibLoader): Deleting KLibrary 0x8099870  libkiviopart\nkdecore (KLibLoader): Factory still has object 0x80a0dd0 Document\nkdecore (KLibLoader):  ... deleting the factory 0x8099f90\nkivio: KivioConfig::~KivioConfig() - deleted\nkdecore (KLibLoader): Deleting KLibFactory 0x8099f90\nkdecore (KLibLoader): add pending close 0x80966a0\nkdecore (KLibLoader): try to dlclose 0x80966a0: yes, done.\n</pre>\n\n<p>I deleted all the files like you said (unless I missed some that are installed elsewhere instead of the Home directory). Guess I'll have to investigate some more :-(</p>"
    author: "Eron Lloyd"
  - subject: "Re: No luck..."
    date: 2001-01-18
    body: "Hey Eron,\n\nThis is something completely new to me.  I've never seen it auto-shutdown like that.\n\nHave you tried installing from the source tarball?\n\nThere is one thing I know which needs to be changed though I'm not sure if it is required for the program to work properly (I doubt it).  The libraries currently get installed under 'lib' instead of 'modules', or something like that.  I've seen messages like this scroll by when I start the program.\n\n-dave"
    author: "Dave Marotti"
  - subject: "Re: No luck..."
    date: 2001-01-19
    body: "I haven't done anything since then on it... guess\nI could try to compile it - what did you say needed to be changed?"
    author: "Eron Lloyd"
  - subject: "Re: No luck..."
    date: 2001-01-22
    body: "Nothing should need to be changed.  I was just saying there are some code changes I need to make, actually, Makefile changes.  The program complains that it found some libraries in lib, and not modules.  I need to change that.  But that shouldn't affect your execution  of the program."
    author: "Dave Marotti"
  - subject: "Re: Kivio 0.9.2 released"
    date: 2001-01-18
    body: "Hello,\n\nI develop Kivio under Mandrake 7.2, so I'm pretty sure it works :)\n\nI am however, using CVS versions of KOffice and I don't recall the version of KDE I'm developing with, it may be CVS (older) as well.\n\n-dave"
    author: "Dave Marotti"
  - subject: "Re: Kivio 0.9.2 released"
    date: 2001-01-19
    body: "got that, on a pretty standard M7.2, too.\n\n[luc@mars luc]$ kivio\nKivioConfig - created\nKivioConfig::KivioConfig() - StencilBackgroundType: 0\nKivioConfig::KivioConfig() - StencilBackgroundFile:\nKivioConfig::KivioConfig() - StencilBackgroundColor: 75 210 255\nKCrash: crashing.... crashRecursionCounter = 2\nKCrash: Application Name = kivio path = <unknown>"
    author: "luc"
  - subject: "Re: Kivio 0.9.2 released"
    date: 2001-01-24
    body: "I had the same trouble with kivio and Mandrake 7.2: here is what I get\n\n[john@jmahon john]$ kivio\nKivioConfig - created\nKivioConfig::KivioConfig() - StencilBackgroundType: 0\nKivioConfig::KivioConfig() - StencilBackgroundFile:\nKivioConfig::KivioConfig() - StencilBackgroundColor: 75 210 255\n~Kapp: cleanup in the library loader: destruct all factories and unload the libraries (Simon)\nKivioConfig::~KivioConfig() - deleted"
    author: "John Mahon"
---
<a href="http://www.thekompany.com">TheKompany</a> has just released kivio-0.9.2.
<a href="http://www.thekompany.com/projects/kivio/">Kivio</a> is now officially part of KOffice, and the code is in the KOffice CVS as well as links on <a href="http://www.koffice.org/">www.koffice.org</a>. New features include printing, right-mouse-button menus on more tools, double clicking a stencil now puts you in text-edit mode, size and position actions have been moved off the toolbar and into a dock-window, snap to grid is now implemented, better zooming abilities, birds-eye-view of canvas, a zoom manager, and various bug fixes.

<!--break-->
