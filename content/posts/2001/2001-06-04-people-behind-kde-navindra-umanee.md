---
title: "People Behind KDE: Navindra Umanee"
date:    2001-06-04
authors:
  - "numanee"
slug:    people-behind-kde-navindra-umanee
comments:
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-04
    body: "Forgive my ignorance, but is \"Navindra\" a man or a woman's name?  I had always thought it sounded like a woman's name, but from this Q&A it seems to me that Navindra might be a man (even though it isn't stated anywhere explicitly as far as I can tell).\n\nSo, which is it? :)"
    author: "Justin"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-04
    body: "Navindra is obviously a male name.  (Well obviously if you know about Indian names I guess.)"
    author: "Jaldhar"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-05
    body: "It's not so obvious to us \"ignorant Americans\" :-)\n\nIn America, names that end in \"ra\" tend to be female (Barbara, Kendra, etc).  So, to an American who doesn't know better, Navindra will sound vaguely like a female name.  Thanks for clearing that up.\n\nI think it's great that KDE is such an international project.  It does cause some confusion sometimes, though."
    author: "not me"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-05
    body: "But if you read the newspapers you'll notice almost anything ending in \"inder\" \"indra\" etc. is male.\n\nBut who cares? Linux could be transexual for all I care - and lots of oldtime ATT source-code hackers (BSD) and developers were gay hippies who used acid so ..."
    author: "Reading is good"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-06
    body: "What about Indira Gandhi, she was a woman, wasn't she?"
    author: "Bl"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-06
    body: "The difference is not obvious in the latin script, but it is in the indian devanagari script. The masculine indra has the last 'a' generally silent, while in the feminine it is pronounced."
    author: "justme"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-05
    body: "> I would port KWM from KDE1 to KDE2, or at least many of the features that we are currently missing in KWin.\n\nI second this. And don't forget \"lost\" panel functionality like automatic hiding of taskbar/panel extensions."
    author: "kde-fan"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-05
    body: "Forget the last. I just read it's on the way (http://lists.kde.org/?l=kde-devel&m=99164703719074&w=2 :-)."
    author: "kde-fan"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-05
    body: "These are already there. Since long. Just look in the config module of kicker.\n\nNavin, your interview is very entertaining. Thanks a lot for your excellent PR work on KDE."
    author: "Inorog"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-05
    body: "I looked in the kicker config and all I can find is an auto-hide button that controls kicker itself.  If I create, say, an external taskbar and turn on auto-hide, kicker hides but the external taskbar does not."
    author: "Kyle Haight"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-05
    body: "the functionality you are looking for was committed to CVS just today. i forget who did it, but i saw their CVS commit message; they haven't added the config options to kcontrol yet though i'm sure that will come shortly. =) look for it 2.2."
    author: "Aaron J. Seigo"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-05
    body: "i just finished posting the above reply and checked my email and lo and behold there was the following CVS commit:\n\n----\n kdebase/kicker/ui k_mnu.cpp,1.16,1.17 panelop_mnu.cpp,1.21,1.22\n Author: firebaugh\n Tue Jun  5 06:42:16 UTC 2001\n \n \n Modified Files:\n          k_mnu.cpp panelop_mnu.cpp\n Log Message:\n - Reload configuration of panel and all extensions on dcop call.\n - Panel extension configuration is complete and should work properly now. :)\n\n--------\n\naaah.. kde..."
    author: "Aaron J. Seigo"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-05
    body: "Navindra, beware about your filthy appartment as when I come to live in Montreal, I won't tolerate that behaviour! ;-) \n\nThanks for the job you do with the Dot, this is really great to be up-to-date with everything.\nCheers!"
    author: "annma"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-05
    body: "ROTFLOL (and a little nervously too).  Good one. ;-)\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-05
    body: "The 'Dot' and Kernel cousin KDE are \nvery well done.\n\nThank Navindra !\nAnd thank Aaron and Rob !"
    author: "thilor"
  - subject: "Re: People Behind KDE: Navindra Umanee"
    date: 2001-06-05
    body: "Micheal jackson??????\n\nCraig"
    author: "Craig"
  - subject: "Perseverance"
    date: 2001-06-05
    body: "As I remember it, KDE Weekly News wasn't all that \"weekly\" and folded at least once to be restarted again. I always valued the postings and I am very happy that dot.kde.org is such a success now.\n\nThank you for keeping it up!"
    author: "Case Roole"
  - subject: "Hyperlink guy"
    date: 2001-06-05
    body: "Even the replies are having hyperlinks. Sort of a habit from the dot, I guess ;-)"
    author: "justme"
  - subject: "No Hindi in KDE Navindra Baba"
    date: 2001-06-06
    body: "Friend Navindra,\n\nI would like to complain about KDE not having HINDI Localization. Don't you think that We Indians prefer more American English than our National Language 'Hindi'. KDE fortunately have Tamil but not Devanagari localization. Isn't there a standard Unicode Hindi Font and Localization for KDE? And there is no reply from the Devanagari localization maintainer :( I would have done some good work."
    author: "Asif Ali Rizwaan"
  - subject: "Re: No Hindi in KDE Navindra Baba"
    date: 2001-06-06
    body: "You sound like you could do something about this.  If the maintainer is AWOL, then certainly you should be able to take over the task...  Are you on the kde-i18n-doc list (http://lists.kde.org/?l=kde-i18n-doc)?\n\nSee also http://i18n.kde.org/, and talk to E. Bischoff (e.bischoff@noos.fr) if you have to. Good luck!\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "KDE aur Hindi"
    date: 2001-06-07
    body: "I haven't found any free devanagiri or gurumukhi unicode fonts but I believe there is a company selling them commercially. KDE in google-style\nhindi (have you tried it?) would be just sad..."
    author: "Taj"
  - subject: "Re: KDE aur Hindi"
    date: 2002-01-25
    body: "I actually found a web site listing available Unicode Hindi fonts:\nwww.geocities.com/fontmagicus\n\nDinesh"
    author: "Dinesh Agarwal"
  - subject: "Re: KDE aur Hindi"
    date: 2003-08-20
    body: "> I haven't found any free devanagiri or gurumukhi unicode fonts\n\nSee the link http://geocities.com/alkuma/seehindi.html titled \"Why can't I see the Hindi section?\""
    author: "Alok Kumar"
  - subject: "Re: KDE aur Hindi"
    date: 2003-11-27
    body: "The page has moved, to:\nhttp://devanaagarii.net"
    author: "Alok Kumar"
  - subject: "Hindi followup"
    date: 2001-06-07
    body: "Oops I forgot to add the one indian font resource that I have: <a href=\"http://cgm.cs.mcgill.ca/~luc/indic.html\">http://cgm.cs.mcgill.ca/~luc/indic.html</a>"
    author: "Taj"
  - subject: "Re: Hindi followup"
    date: 2001-06-07
    body: "Heh, cool.  That's one of our local crazy genius profs.  I was aware he was doing *something* with fonts years ago, no idea his work was so close to home. ;)"
    author: "Navindra Umanee"
  - subject: "Re: Hindi followup"
    date: 2001-06-12
    body: "Comes as no surprise that he's both a genius and crazy -- look at his webpage ! \n\n-- Donovan, A somewhat humbled font HOWTO author"
    author: "Donovan Rebbechi"
  - subject: "Re: Hindi followup"
    date: 2001-11-29
    body: "For some Indian-language solutions in Linux (it's very important for this country of 1000+ million) please visit\nhttp://rohini.ncst.ernet.in/indix\nYou can download this IndiX system from NCST in Mumbai.\nCheck out indlinux.org\nAlso http://indlinux.sourceforge.net/\nThen there's http://www.tenet.res.in/Donlab/Indlinux/ \nOne group is working to translate KDE/Gnome into Tamil. You can contact balaji.narayanan@wipro.com <Balaji N> or send an email to tamilinix-subscribe@yahoogroups.com\nLot's is happning here. We're hoping to have some significant breakthrough soon. -- Frederick Noronha in Goa, India / fred at bytesforall dot org\nPS: I'm a journalist, not a techie..."
    author: "Frederick Noronha"
  - subject: "Re: No Hindi in KDE Navindra Baba"
    date: 2001-11-29
    body: "> I would like to complain about KDE not having HINDI Localization.\n\nBecause someone hasn't done it yet. ;-P\n\n> Don't you think that We Indians prefer more American English than our National Language 'Hindi'. \n\nI think that is a pretty broad generalization. Some of us do.. however, remember that Hindi isn't terribly standardized in India itself. In fact, many of us know English better than Hindi (one reason English is the associate language of India and used exclusivly in government affairs). I'm a Bengali myself, and I'd rather use English than Hindi. I'd of course love a Bengali translation.\n\n> And there is no reply from the Devanagari localization maintainer :( I would have done some good work.\n\nIf he/she doesn't respond after a period of time, you might be able to take it over. Until that time, remember that you do not have to be the maintainer of something to work on it :-)."
    author: "Sashmit Bhaduri"
  - subject: "Re: No Hindi in KDE Navindra Baba"
    date: 2004-08-14
    body: "who the hell cares about indians!! "
    author: "Karl"
  - subject: "Re: No Hindi in KDE Navindra Baba"
    date: 2002-02-08
    body: "Actually someone did (some/all) the translations for KDE, but they were not willing to release it , as they wanted to be paid for that work & also sign a commercial agreement with them on its future use.\n Since amout was big & their conditions were not metable, so it never got used.\n\nKarunakar\n"
    author: "G Karunakar"
  - subject: "Re: No Hindi in KDE Navindra Baba"
    date: 2007-04-29
    body: "I donot know the person who translated KDE to Hindi. But, I would like to request him to release his works for public use. He/She should not care for money. Everyone will be rewarded. No ones good work goes in vain. I would like to request him that he is an Indian and work for India. We have the greatest computer engineers in the world. It is a matter of shame that Indic computing is not as developed as English computing although, I am not an opponent of English computing. It is our moral responsibility to work for our motherland and culture. We should not be inclined towards the West. We should give our National language the FIRST priority."
    author: "Tushar Mukherjee"
  - subject: "Re: No Hindi in KDE Navindra Baba"
    date: 2002-02-08
    body: "Actually someone did (some/all) the translations for KDE, but they were not willing to release it , as they wanted to be paid for that work & also sign a commercial agreement with them on its future use.\n Since amout was big & their conditions were not metable, so it never got used.\n\nKarunakar\n\nThe Indian Linux project\nwww.indlinux.org"
    author: "G Karunakar"
  - subject: "Re Names..."
    date: 2001-11-29
    body: "Indian names don't necessarily follow the same rules as Western ones. For instance, not every name ending with a 'a' would be a feminine name. \nGreat work Navindra. We back in India look forward to inspiration from people like you. Frederick in Goa."
    author: "Frederick Noronha"
---
This week, in <a href="http://www.kde.org/people/people.html">The People Behind KDE</a>, yours truly speaks candidly to Tink. <i>"I'm rooting for the KDE League to have some form of impact on the PR scene any time now, but it is quite clear that community involvement is more important than ever. The community can accomplish quite a lot on its own that the KDE League could never do, though there are definitely things (such as mainstream press) best left to the League."</i> <a href="http://www.kde.org/people/navindra.html">Read it all here.</a>
<!--break-->
