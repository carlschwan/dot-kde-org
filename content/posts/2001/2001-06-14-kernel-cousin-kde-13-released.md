---
title: "Kernel Cousin KDE #13 Released"
date:    2001-06-14
authors:
  - "rmoore"
slug:    kernel-cousin-kde-13-released
comments:
  - subject: "Flash Plugin"
    date: 2001-06-14
    body: "I'm happy that they have a Flash Plugin for Konqueror Embeded.  It would be neat to be able to see Flash animation on a PDA.  However, I still can't get the flash plugin to work in the desktop version of Konqueror :-("
    author: "Steve Hunt"
  - subject: "Re: Flash Plugin"
    date: 2001-06-14
    body: "Flash works perfectly on Mandrake 8.0, both in Konqueror and Netscape. I never knew what I was missing before.  Flash is apparently so much better than web/Java ever was. Fast loading, and those (vector?) graphics are just so damn sharp!  :-)\n\nAnyway, does your flash plugin work in Netscape at all? Is nsplugins (nspluginscan,   nspluginviewer) properly compiled/installed on your system?  Is Konqueror detecting the presence of the flash plugin?"
    author: "Navindra Umanee"
  - subject: "Re: Flash Plugin"
    date: 2001-06-14
    body: "Actually, I use Mandrake 8.  It works fine in Netscape, it doesn't work in Konqueror.  Oh well.  Yes, it detects the plugin.  It's no big deal, I don't really view that many Flash pages anyway :-)"
    author: "Steve Hunt"
  - subject: "Re: Flash Plugin"
    date: 2001-06-14
    body: "Is the lesstif RPM installed? Just a thought as I guess the KDE2 RPMs had lesstif as a dependancy but you never know. Try and rpm -Uvh the lesstif RPM from the CD if it installs re-try flash, it it is already installed, I have no idea."
    author: "David"
  - subject: "Re: Flash Plugin"
    date: 2001-06-14
    body: "install the kdebase-nsplugins package."
    author: "Evandro"
  - subject: "Solution"
    date: 2001-06-16
    body: "I had the same problem, and I know what to do about it. The mime type is set incorrectly. Go to you konqueror config, File Assocations, and set futuresplash and X-shockwave-flash to the embedded Netscape plugin viewer. It works (a little) for me...."
    author: "Wouter van Kleunen"
  - subject: "Re: Flash Plugin"
    date: 2001-08-02
    body: "kdebase nsplugin is installed\nFlash plugin is detected but It does not show on the screen the flash video.\n\nI have mandrake 8\n\nI need this working, because I do see a lot of flash sites\n\nI did not find the embebed netscape plug in in the configuration of konqueror\n\ntks"
    author: "ozp"
  - subject: "Re: Flash Plugin"
    date: 2001-11-12
    body: "Excuse me ,can you tell me how can i get the Flash Plugin for Konqueror Embeded"
    author: "xujun"
  - subject: "Re: Flash Plugin"
    date: 2001-11-26
    body: "Hi , I also want the same. Did you get one??\nThanks \nJeevan"
    author: "Jeevan"
  - subject: "Re: Flash Plugin"
    date: 2002-11-27
    body: "Hey all.. I had the same problem with Flash 5.0.  I could get Flash to work in mozilla but not in konqueror. Yes, my konqueror is setup to use Netscape plugins 'cuz my java plugin is working.  I downloaded the Flash 6 beta and now  www.xdude.com is working along with www.bestflashanimationsite.com.  I don't get it, but if you're having problems try the new version.  Use the installer they give you.\n\nUsing:  Red Hat 8.0 (yes RH 8.0) with RH 7.3 plugin from Macromedia"
    author: "Toppy"
  - subject: "Re: Flash Plugin"
    date: 2002-11-27
    body: "hi\n\nIn fact Java doesn't use the Java plugin in konqueror so, maybe, you haven't activated the NS plugins even though you think so because you think it uses java plugin.\n\nDo you follow me ? :)"
    author: "Julien Olivier"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-14
    body: "There was a nice screenshot here --> http://www.geocities.com/newyen/label.png\n\nDoes anyone on the board know what window manager or kwin decoration is being used?  \n\nI'm afraid that I'm just not keeping up with the latest, but enquiring minds want to know..."
    author: "APW"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-14
    body: "I'm guessing it's probably an IceWM skin. KWin has a nifty IceWM plugin that can use any IceWM theme :)."
    author: "David Watson"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-14
    body: "Yep - I just checked and it's a theme called liQuid. You can get it from themes.org (well, actually the sourceforge mirror - just go to themes.org, then click on \"sourceforge mirror\", then go into the icewm directory - it's in there).\n\nOnce you have it, just put it in your KWin IceWM themes dir (just go to the KControl KWin IceWM config page, and click on the link to open konqy in that location). You will need to be running 2.2alpha2 or better."
    author: "David Watson"
  - subject: "artist.kde.org icon factory is a mess"
    date: 2001-06-14
    body: "busted links etc ...\n\nplus I think the main kde site could use some nicer screen shots ..."
    author: "zoneur fou"
  - subject: "Re: artist.kde.org icon factory is a mess"
    date: 2001-06-14
    body: "Yah, artist.kde.org needs some maintainance. I suppose I could look at the broken links and fix those, but I wouldn't go much further than that, the content is really Tackat's domain.\n\nIndeed, I should update my screenshots. On the other hand, they don't always need to show \"nice\". I try to show some of the themability but also some of the other features."
    author: "Rob Kaper"
  - subject: "Re: artist.kde.org icon factory is a mess"
    date: 2001-06-14
    body: "> I suppose I could look at the broken links \n> and fix those, but I wouldn't go much \n> further than that,\n\nNo no, if anyone would like to maintain artist.kde.org feel free to volunteer.\nThis includes bugs, design and content of course. :-)\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-14
    body: "Could someone please provide a few screenshots of KPersonalizer?"
    author: "Carbon"
  - subject: "first page"
    date: 2001-06-14
    body: "..."
    author: "Aaron J. Seigo"
  - subject: "second page"
    date: 2001-06-14
    body: "..."
    author: "Aaron J. Seigo"
  - subject: "Re: second page"
    date: 2001-06-14
    body: "The one thing I would improve about KPersonaliser would be this section -- to be able to change the presets right here. Just because I seem to like a style that noone else likes :) (normal KDE with focus follows mouse, basically).\n\nI know, I can change it in the control centre at the end..."
    author: "Jon"
  - subject: "Re: second page"
    date: 2001-06-14
    body: "> I know, I can change it in the control \n> centre at the end...\n\nMaybe for KPersonalizer 2.0. For the current version it's just meant as an easy solution to make it possible for newbies to easily customize the look&feel according to their needs. As kcontrol isn't really newbie-proof with it's 1001 options and tuning-settings kpersonalizer provides a simplified version.\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: second page"
    date: 2001-06-14
    body: "this looks just great.\ni had a good laugh about the window and the apple.\n\nlet's throw the apple out of the window :-)\n\ngreat work\n\nolaf"
    author: "maxwest"
  - subject: "Re: second page"
    date: 2001-06-15
    body: "did you see the \"factoid of the day\" hint already in the konqueror custom style sheet tab in kde 2.2 alpha yet ? :)"
    author: "ik"
  - subject: "third page: Eye Candy - Meter"
    date: 2001-06-14
    body: "..."
    author: "Aaron J. Seigo"
  - subject: "Re: third page: Eye Candy - Meter"
    date: 2001-06-14
    body: "I'm stunned by the graphics! It so good it makes me want to USE the program, if only to see what picture that comes next. Perhaps one of the most stylish productions to ever hit the Linux/Unix world. Yes, I'm in awe. :)\n\nGood job!"
    author: "Matt"
  - subject: "Re: third page: Eye Candy - Meter"
    date: 2001-06-14
    body: "How slow is a \"slow\" processor and how fast is a \"fast\" processor?"
    author: "Matt"
  - subject: "Re: third page: Eye Candy - Meter"
    date: 2001-06-15
    body: "KDE could test the processor speed and suggest what effects would be enabled. Don't show many options for newbies, unless you put a 'suggested' option."
    author: "Julio"
  - subject: "Re: third page: Eye Candy - Meter"
    date: 2001-06-15
    body: "> KDE could test the processor speed and \n> suggest what effects would be enabled.\n\nThat's what is already on the TODO-list :)"
    author: "Tackat"
  - subject: "fourth page: Everybody Loves Themes"
    date: 2001-06-14
    body: "..."
    author: "Aaron J. Seigo"
  - subject: "Re: fourth page: Everybody Loves Themes"
    date: 2001-06-14
    body: "Hey, thanks!\n\nimho, perhaps \"standard\" or \"simplified\" would be better then boring."
    author: "Carbon"
  - subject: "Re: fourth page: Everybody Loves Themes"
    date: 2001-06-14
    body: "Too late now - there's a message freeze leading to the beta release :)"
    author: "Jon"
  - subject: "Re: fourth page: Everybody Loves Themes"
    date: 2001-06-14
    body: "I love that wording \"boring\" .. keep it in. It is a direct slate of M$, so leave it in :)"
    author: "kde-user"
  - subject: "No Trash Talk.."
    date: 2001-06-14
    body: "Let's not resort to bashing M$ now..  True they bash linux but let's be the good guys and get ahead by superiority..  not childish trash talking."
    author: "KDE Fan"
  - subject: "Re: fourth page: Everybody Loves Themes"
    date: 2001-06-14
    body: "The current entries are only dummy-entries which will be replaced by the final themes before the final release. Due to the fact that the artists also need some time after the code is done the messages in this step of the kpersonalizer might change. We'll try to make this not too painful for the translators, though.\n\nGreetings,\nTackat"
    author: "Torsten Rahn"
  - subject: "Re: fourth page: Everybody Loves Themes"
    date: 2001-06-15
    body: "Awesome. This is once again an example of what I love most about Open Source : ask for a change and your response will almost always be : \"We're already working on it\", or \"Check CVS, it's already there\""
    author: "Carbon"
  - subject: "Re: fourth page: Everybody Loves Themes"
    date: 2001-06-14
    body: "One more thing - if these are 'themes' (I don't know, because I haven't tried them), why aren't they on the KControl/Look&Feel/Themes section (and vice versa)? If there're *Not* themes, you need to choose a different name to avoid confusing people."
    author: "Jon"
  - subject: "final page: Time To Refine"
    date: 2001-06-14
    body: "..."
    author: "Aaron J. Seigo"
  - subject: "Re: final page: Time To Refine"
    date: 2001-06-14
    body: "Wow! I just want to say that the screenshots look amazing... very slick and professional. I love the idea of this wizard and offer my congratulations to all those responsible."
    author: "kdeFan"
  - subject: "Re: final page: Time To Refine"
    date: 2001-06-14
    body: "> the screenshots look amazing\n\n    ...and, in the continuing vein of the most annoying and repeated question about screenshots (yes, the app looks great, but I can't help but ask...) what theme are you using?\n\n    I personally vow that if I ever post a screenshot, I'll mention in the message what theme/ color scheme I'm using at the time (KWiX right now, default colors).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: final page: Time To Refine"
    date: 2001-06-14
    body: "He uses Mosfet's megagradient style and window decoration theme is probably IceWM for KDE one."
    author: "Antialias"
  - subject: "Re: final page: Time To Refine"
    date: 2001-06-15
    body: "widget style: megagradient\ncolor scheme: default KDE2\nwindow decor: ice-wm theme \"bb.themes.org\""
    author: "Aaron J. Seigo"
  - subject: "Re: final page: Time To Refine"
    date: 2001-06-14
    body: "I don't know who has made those graphics, but I strongly assume that tackat is to blame for this.\n\nTackat, you're a god! KDE would be half of what it is today without you!\n\nI think people don't credit him (and graphics artists in general) enough. It's really hard to imagine how many icons and other graphics he has already created. If we imagine KDE has about 100 icons (i really have no clue how many there are), they have done each version in 22^2, 32^2, 48^2, each one in locolor and hicolor (am I right?).\n\nI'm afraid of multiplying these numbers!\n\nThank you, graphics-guys! ;)"
    author: "me"
  - subject: "Re: final page: Time To Refine"
    date: 2001-06-14
    body: "Yeah, and as tackat seems to be bored at the moment, he's working on 64x64 icons ;)\n\nThe images on the left of the wizard are from qwertz."
    author: "Carsten Pfeiffer"
  - subject: "Re: final page: Time To Refine"
    date: 2001-06-15
    body: "> Yeah, and as tackat seems to be bored at the moment, he's working on 64x64 icons ;)\n\nI hope he doesn't forget to re-include lowcolor icons."
    author: "someone"
  - subject: "Re: final page: Time To Refine"
    date: 2001-06-14
    body: "> Tackat, you're a god!\n\nWell, I'm only the icon-god ;)\nQwertz is the awesome-large-images-god.\n\nGreetings,\nTackat"
    author: "Torsten Rahn"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-15
    body: "Wow! KPersonalizer has tremendous graphics.  How come Konq and other KDE apps have such bland ones?"
    author: "Robert Gelb"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-15
    body: "How can you say that with a straight face looking at the awesome Konqueror startup screen? Those certainly aren't bland. \n\nOn the other hand, the KPersonalizer graphics are just plain _neat_! They're quirky, but in a highly professional sort of way. Thanks again, almighty omnipotent KDE Artists of the land of the immortal!"
    author: "Carbon"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-16
    body: "I didn't mean the startup screen.  I meant the toolbar buttons & the like.  I generally compare KDE to MacOSx or WinXP (in beauty) and at the moment it doesn't compare favorably.  Configurable icon sets would be great (like in the old AmiPro), but for the time being a snazzier set for Konq would be great."
    author: "Robert Gelb"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-16
    body: "There ARE icon themes that you can download, just not many of them yet. Check out KControl->LookNFeel->Icons."
    author: "David G. Watson"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-14
    body: "I noticed the personalizer some time before but ignored it due to the fact that I already had configured my desktop. With KDE's 06/13/2001 CVS sources I was surprized to see the KPersonalizer appear on first startup. \nIt claimed it would configure my desktop !\nOK, there was that 'Skip' button in the lower left of the window.\nPressing it, I saw that KPersonalizer warn that it couldn't configure my desktop if I proceeded [to skip] and then \"threatened\" to install the default KDE Settings [over my existing ones] in that case.\nI preferred sending a SIGKILL ... ;-)\nI wonder what that kind of compulsion is for in an environment like KDE?\nOK, I do not really believe that this behavior was intended..."
    author: "Andreas Leuner"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-14
    body: "Yes, I think these severe warnings should go too.  They violate the <a href=\"http://static.userland.com/gems/joel/uibookcomplete.htm\">User Interface Design guide</a> for programmers, for one. :-)"
    author: "Navindra Umanee"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-14
    body: "Wow! Thanks...coooooool link!!!"
    author: "Michele Di Trani"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-14
    body: "If you like that you will love: \n<br><br> \n<a href=\"http://www.amazon.com/exec/obidos/ASIN/1568843224/o/qid%3D969013186/sr%3D8-1/ref%3Daps%5Fsr%5Fb%5F1%5F3/103-7979867-4587042\">\"About Face\"</a>\n<br><br> \nIt's the best book on the subject\n<br><br> \n--<br>\nSimon"
    author: "Simon"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-14
    body: "Personally I didn't like this book much. It has some good ideas, but it is ruined by the author's insistence on using his own weird terminology, and his insistence on highlighting so many words. For a book on UI design to have such poor interaction with the reader is rather ironic...\n\nYou'll find the same information in pretty much any recent UI design text book, so personally I'd recommend reading something else.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-14
    body: "> OK, there was that 'Skip' button in \n> the lower left of the window.\n\nWell, you are using CVS ... how do you get the idea that things do work as supposed there?\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-15
    body: "Maybe he was just pointing it out to insure that the problems will be gone when it's released as stable..."
    author: "Per Wigren"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-15
    body: "Its gone."
    author: "Anon"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-14
    body: "since there are millions of options in kde2 (which makes it configurable and therefor great IMHO) there are about as many discussions about if a newbee could anyhow deal with all that. \n\nnewbee can not (i did win95/98 support for 2 years for 14-65 year old people in playing, learning and business setups)\n\nso what about an option to lock all these wonderfull options (probably even on kde install with unlock only happens with root password)\n\nthat would keep DAU's in business settings as well as wives and girl-friends (yes, also otherwise round) from distroying their setup and from playing with user interface. i do it all the time and get horrible results from time to time (yes, it's my free time :-)\n\nolaf"
    author: "maxwest"
  - subject: "KDE and alpha channel"
    date: 2001-06-15
    body: "I have a question about KDE apps :\n\n*WHY* alpha channel is only used with icons ?\n\nKview, konqueror, pixie don't support alpha channel :(\n\ngqview, galeon, gimp support it.\n\nWhere is the problem ?\nI want full alpha channel support everywhere ;)"
    author: "shift"
  - subject: "Re: KDE and alpha channel"
    date: 2001-06-15
    body: "> *WHY* alpha channel is only used with icons ?\n\nThere's no technical reason.\n\n[X] Create a patch to make it work :-)\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-15
    body: "I think is time to reorganizate the kde applications menu. There are lot of mini-applications and is very very confuse for the begginers.\n\nOn windows menus,for example (they have spent lot of $ on I+D), there are a few real applications as Internet Explorer, outlook, word, ppp,or winpad, and the rest of the applications are part of wizards menus ,administration tools or explorer filemanager options. For example, the KDE application Kdisk. In KDE is a program, but on Windows, is part of the filemanager.\n\nDo you know what i mean ?\n\nBest regards ..."
    author: "Daniel"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-15
    body: "Excellent idea.  I really like the fact that the K menu is logically arranged but the Utilities and System subsections really need cleanup.  I've been using KDE since 1.0 and always have trouble finding things in the System and Utilities menus."
    author: "raven667"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-15
    body: "And I think it should be possible to arrange the items, e.g. it is easier to find something if it is arranged alphabetically (Actually it is, but only if you use english language)"
    author: "Norbert"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-15
    body: "I agree strongly, the kde menu could be improved.\n\nFirst, for each application have a name and a short description dispayed: konqueror (web browser), noatun (multimedia player), etc.\n\nThe current way have either only the name which is not always explicite (what's noatun ?) or only the short description which makes you ignore what the real application is (it took me a while to figure out that this \"internet dialer\" on my mandrake was in fact kppp).\n\nThis require to introduce a new field in the .desktop but it is necessary in my opinion. The problem is that we have thousand of free software, with names that are not very explicit, especially for newbie.\n\nSecond, allow to easily reorganize the K menu and provides more than one setup. The standard organization is ok, but we also need a windows-friendly organisation (6 apps on top, everything in submenu), some distibution specific organisation, ...\n\nThis would help distribution and users to sort out their applications."
    author: "Philippe Fremy"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-15
    body: "The real problem is not that, its the whole application idea.\n\nWho _cares_ that we use noatun, xmms, kaiman or winamp-under-wine to play music? (Let's hallucinate that they all equaly well solve the problem, are easy to use, burn as much resources, etc. for the sake of argumentation)\n\nI know my users don't. (The sysadmin in me is talking) They want to _play music_, or _write a letter_ or _format a floppy_, etc. They couldn't care less applications were named 'noatun, 'kword', 'kfloppy' (?), etc. The real problem comes with having a menu at all. We need some sort of 'User task manager' that revolves (?) around a few strong points:\n\n* What are users doing most often. (Secretarial work, c++ developer, 3-year-old kid trying to play 'Pick-a-dilly-pair', etc.) [Pick a dilly pair was the game I was playing on an Apple ][, some 18 years ago... =) ]\n\nThis already is embodied somewhat in the 'most recent used' entries in KMenu, in task-* debian packages (go on http://packages.debian.org, search for packages starting with task in the unstable distro), and in the debian installer 'profiles' (workstation, server, etc.)\n\nBut we need to improve on this a bit, in the user interface. Say that new KPersonalizer thingamajig: Add a few pages saying \"I'm doing secretarial work\" and add a  \"Secretarial Work\" menu or applet, or whatever, on Kicker, with the most obvious applications in there: KWord, maybe KSpread, etc.\n\n* User's level of UNIX expertise. Trust me, users don't give a hoot about man/info pages. This doesn't need to be proeminent in the KHelpCenter. People that really use man pages know where to find those anyway.\n\nThis has millions of other advantages: Big Company Corp. (thereafter known as BCC) just install a new 'Task Kicker Extension' for BCC's own applications, and there you go. Or Debian just adds 'Debian Kicker Extension' for Debian Sysadmins, and there you go. And if I ever get promoted to chief-research scientist at McGill, I just add the 'Maths and Computer Science Theory' Task Kicker Extension and there I go.\n\nThere is no need for a menu in the first place, KDE needs to focus on what users want to do. They want to 'Write a letter', not 'Run KWord'. They want to 'play that mp3', not 'run noatun' (sorry Charles), etc. etc. etc.\n\nThe drawbacks: \n\n* TASK NAME EXPLOSION: Not everyone wants to 'Write a letter', some people want to 'Write their PhDs', and others want to 'Create labels for BCC' and yet others want to 'Enhance that document I got from grandma' and the craziest amongst us want to 'keep track of the grocery list with headers and embedded spreadsheet with a graph of the history of prices of african tomatoes for the last 50 times I bought some here'. Try to make reasonable task names for those... (I know I can't =)\n\n* MAJOR UI PARADIGM CHANGE: Removing the K menu (or at least superseding it with something I feel is better)... Well, let's just say the K menu has been here since 1.0, hasn't it? Since win95 on windows; and the apple logo on MacOS is probably half as old as I am...\n\n----\n\nThe good idea(s) might just be:\n\nTo give more desktop-space to the templates that come with various applications, say the KWord new Labels templates, instead of insisting on KWord itself.\n\nTo include a few new KParts to Konqueror (remember that playlist viewer in Nautilus?) and make Konqy more central than it actually is right now.\n\nTo let our users Boldly Do What No One Else Has Done Before.\n\n-----\n\nI call to dot readers: Does this make sense? Do you agree? Should I call an asylum to make reservations?\n\nHave fun,\nChristian"
    author: "Christian Lavoie"
  - subject: "Re: Kernel Cousin KDE #13 Released"
    date: 2001-06-18
    body: "www.redmondlinux.org\n\nI posted this address before and someone didn't believe that it was real.  Let me assure you it is.  Go there and check out their \"task-oriented menus.\""
    author: "not me"
---
Kernel Cousin KDE #13 has just been published. In this week's issue: <a href="http://www.geocities.com/newyen/index.html">Avery label templates</a> for KWord, improvements to Kicker, and a new <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdebase/kpersonalizer/README">personalizer</a>  wizard to make configuring KDE easier. You can read <a href="http://kt.zork.net/kde/kde20010608_13.html">the full article here</a>.
<!--break-->
