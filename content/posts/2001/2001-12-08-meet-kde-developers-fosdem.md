---
title: "Meet KDE Developers At FOSDEM"
date:    2001-12-08
authors:
  - "pfremy"
slug:    meet-kde-developers-fosdem
comments:
  - subject: "Finally..."
    date: 2001-12-07
    body: "Finally a meeting in Belgium. I was waiting for it :-)"
    author: "Storm"
  - subject: "Re: Finally..."
    date: 2001-12-07
    body: "maby the Americans will get the spirit too, but judging by how spead out we are I am not holding my breath...\n\nSo who will be at LWE in NY this January?\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "LWE in NY"
    date: 2001-12-08
    body: "Hey, call me a big dope but where can I find out about LWE? I live not too far from NY (I can easily take a train) and would love to go."
    author: "Shamyl Zakariya"
  - subject: "Re: LWE in NY"
    date: 2001-12-08
    body: "Hey try this:\n\nhttp://www.linuxworldexpo.com\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: LWE in NY"
    date: 2001-12-08
    body: "check out:\n\thttp://www.linuxworldexpo.com\n\nIt would be cool to have some developers there.  Ironicly it is infinitely easier to get the EU crowd together, than the US/CA crowd.\n\nCheers\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "This is great for Americans"
    date: 2001-12-08
    body: "...the opportunity to interact with the KDE movement, which seems to have a lot more momentum in Europe than here. And Amsterdam is so close, too..."
    author: "redapd"
  - subject: "Re: This is great for Americans"
    date: 2001-12-10
    body: ">: This is great for Americans\n\nNot for us west coasters :/. Having a meeting in say.. asia.. would be easier to goto than Europe.\n\n> which seems to have a lot more momentum in Europe than here\n\nNot really, there is quite a lot of momentum in the US too.\n\n> And Amsterdam is so close, too...\n\nAmsterdam is not Brussles :D"
    author: "not the holy them"
  - subject: "Re: This is great for Americans"
    date: 2001-12-10
    body: "> Amsterdam is not Brussles :D\n\nNo, but it's close.. and foreigners (especially Americans) seem to think that Amsterdam is this paradise city with pretty girls, legalised pot and legalised hookers.\n<p>\nOh wait, it is. Just like the rest of Holland. :-)"
    author: "Rob Kaper"
  - subject: "How about coming to Minnesota?"
    date: 2001-12-08
    body: "Nothing ever happens here."
    author: "Anonymous Hero"
  - subject: "Ill be there!"
    date: 2001-12-08
    body: "Not like I am life of the party but I will be on the north side of the twin cities from Dec 26th until Jan 1st. If anyone wants to meet for some GeeK Speak I am up for it.  Maby even some codeing.  If anyone wants to arrange going of for coffie/beer let me know, I can set something up.\n\nCheers\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Ill be there!"
    date: 2001-12-10
    body: "DOH, i ment to respond to the twins cities thing.\n\nUnfortunately I cannot afford to take off time wise at that time, because of a current project.  What I ment is I will be in Minisota, so that if any KDE developers want to get together there.\n\nSorry for the confusion, and the PPC is not all that strange compaired to my BeBox or evil looking ][fx  running AUX.\n\nCheers,\n\t-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: Ill be there!"
    date: 2001-12-10
    body: "Ooops, my bad..."
    author: "Navindra Umanee"
---
KDE developers are cordially invited to the Free and Open Source Software Development Meeting (<a href="http://www.fosdem.org">FOSDEM</a>) which is taking place in Brussels on February 16-17, 2002.  A dedicated KDE room will be made available for development talks as well as presentations.  Developers already confirmed for this mini-KDE event include: <a href="http://www.kde.org/people/david.html">David Faure</a>, Laurent Montel (<a href="http://www.koffice.org/">KOffice</a>), <a href="http://www.kde.org/people/rich.html">Richard Moore</a>, <a href="http://www.kde.org/people/kalle.html">Matthias "Kalle" Dalheimer</a>, <a href="http://dot.kde.org/983311036/">Richard Dale</a>, <a href="http://www.kde.org/people/michaelg.html">Michael Goffioul</a>, Thomas Capricelli (<a href="http://boson.sourceforge.net/">boson</a>), Mickael Marchand (<a href="http://klotski.berlios.de/kvim/">KVim</a>, KEdit stuff) and probably others such as <a href="http://www.kde.org/people/jono.html">Jono Bacon</a>, <a href="http://www.kde.org/people/michaelb.html">Michael Brade</a>, and <a href="http://www.kde.org/people/rob.html">Rob Kaper</a>.  Unfortunately due to exams, some german developers such as  <a href="http://www.kde.org/people/tron.html">Simon Haussman</a> or <a href="http://www.kde.org/people/ralf.html">Ralph Nolden</a> may have difficulties attending.  <b>Addendum:</b> Kristof Borrey (<a href="http://users.skynet.be/bk369046/icon.htm">of iKons fame</a>) will also be present.  Ian Reinhart Geiser (KDE wizard, also known to dabble with <a href="http://www.resexcellence.com/index_02-01b.shtml#02-16_linux">exotic CPU architectures</a> <i>and</i> <a href="http://kweather.sourceforge.net/">the weather</a>) won't be there afterall.


<!--break-->
<p>
The place is much bigger than last year. Contrary to a Linux Expo, the FOSDEM is an event held by developers, for developers. You may attend many interesting presentations, mingle, hold discussions with other projects, etc...  For a better idea, perhaps check out what David Faure <a href="http://dot.kde.org/981420515/">had to say</a> about last year's <a href="http://www.osdem.org">OSDEM</a> (the old name). 
<p>
So to meet all these KDE guys and the plenty of others who will be attending, simply join us at the FOSDEM. The official site offers hints on <a href="http://www.fosdem.org/lodging">where to sleep</a> in Brussels. Last year, some of us even managed to crash with local developer folks; this might also be possible this year. Finally, if you intend to come, please be sure to <a href="http://www.fosdem.org/register">register</a> so that the organisers can estimate how many people to expect.

