---
title: "Linux Journal Readers Love KDE; Quanta Gets a Plug"
date:    2001-10-13
authors:
  - "Dre"
slug:    linux-journal-readers-love-kde-quanta-gets-plug
comments:
  - subject: "Quantification"
    date: 2001-10-13
    body: "As far as I understand, there will be three Quantas (Quantum?).\nOne from HanCom, one from TheKompany and one from Eric Laffoon.\nChoice is good."
    author: "reihal"
  - subject: "Re: Quantification"
    date: 2001-10-13
    body: "> One from HanCom, one from TheKompany and one from Eric Laffoon.\n\nIt's a little different, I think : one from HanCom/TheKompany (inside Hancom Office, I doubt there will be 2 versions...) and one in GPL.\n\nThank you Eric Laffoon for the GPL version !\n\nWhat would be the exact names ? Eric Laffoon spoke about \"Quanta\", and Hancom/TheKompany about \"Quanta+\" (see http://dot.kde.org/999199700)..."
    author: "Alain"
  - subject: "Re: Quantification"
    date: 2001-10-14
    body: "theKompany will be selling \"Quanta Gold\" as a stand alone app.  We will also have a rebranded version in HancomOffice known as \"HancomWebBuilder\".  And no offense to Eric, but you should be thanking Alex and Dmitry who actually wrote Quanta+ and had been working on it for a year before Eric got involved.  As I understand it Eric provided some financial support, web design for the home page, public advocacy and feature requests."
    author: "Shawn Gordon"
  - subject: "Re: Quantification"
    date: 2001-10-14
    body: "Well, yes that's true. But what I thought of when he thanked Eric for the GPL version was thanks for his efforts to keep a GPL project going. It's no small task. Financial support also helps. \n\nI am interested in thekompany's future efforts and will probably use whichever version is the best for my needs regardless of cost. (Well, regardless, up to a modest level...)\n\nThanks to Alex, Dimitry, Eric, thekompany, Linus.\n\nThis newbie just ditched windows last night! Whooooeeeee! What took me so long? I don't know, but I know little about computers and haven't had any trouble installing Mandrake 7.2, getting my adsl working, downloading and installing apps and having so much fun I've been up for about 24 hours! The stuff about Linux being tough so far seems like a bunch of hogwash. I spent plenty of hours debugging my Windows setup in the past and that was no cakewalk at times.\n\nThanks to ALL who program for Gnu/Linux!!! Many, MANY, thanks! So far the most fun thing is when an app hangs (Konquerer gets trashed by the most heavily active-x using sites, but so did IE 5.5) it just shuts down gracefully and I restart it. No reboots. Koffice 2 and Konqueror are terrific."
    author: "John Marttila"
  - subject: "Re: Quantification"
    date: 2001-10-15
    body: "I meant Koffice 1.something. It seems like version 2 as I had to upgrade from what came with Mandrake 7.2, the apps were so buggy as to be nearly unusable but now (ver 1.1) both Kword and Kspread are working nicely so far. Konqueror (1.9.8 for me) seems to be rendering most websites very well. I have switched between Konq and Netscape and Konq is preffered. I'll download Mozilla and Opera and compare but I can't see leaving Konqueror.\n\nI AM finding out that getting all the right libs and resolving dependencies on various flavors of such libs is more tricky then I first thought. But when I get evrything working on an app they seem to work great. Slower to load sometimes then my memories of windows but they seem quicker once loaded. My system seems to really rock, speedwise, with Linux (I have an AMD k-300 with 128 meg ram)\n\nAnd best of all, I will never have to sign over my firstborn to Microsoft by using XP.\n\nIf you're lurking Shawn G. or if anyone else reads this, are their any recent reviews of Kapital that you know about? I need something to replace Quicken and am going to start that hunt today. At this time I am going to lean toward KDE apps where possible as so far I prefer it over gnome."
    author: "John Marttila"
  - subject: "Re: Quantification"
    date: 2001-10-16
    body: "we just put a new version of Kapital up today, we are going to announce it shortly."
    author: "Shawn Gordon"
  - subject: "Re: Quantification"
    date: 2001-10-16
    body: "Hello,\nwhat is the status of the gpl version as there are still some annoying bugs ?\nWill it be finished by its authors or have they abandonned it ?\nI think there is nothing wrong in you hiring them, but in all fairness we should be informed of the gpl version future. If the developpers are not to go back to finish this version, others will, at least for the remaining bugs.\nthanks."
    author: "try"
  - subject: "Observations"
    date: 2001-10-13
    body: "Interesting that SuSE doesn't show up under the first three places in this poll. LinuxJournal doesn't seem to have many German users compared to the poll at http://www.heise.de/ct/01/17/186/ (results only available in German).\n\nFirst placed RedHat and second placed Debian doesn't have KDE as their default desktop or included in their latest stable release. Nevertheless KDE wins the desktop category with 40% of all votes.\n\nKWord underlies AbiWord by less than 19 of 6.500 votes.\nKMail underlies Netscape by less than 70 of 6.500 votes.\n\nI don't understand why the didn`t publish absolute counts and up to 5th places."
    author: "someone"
  - subject: "Re: Observations"
    date: 2001-10-13
    body: "The last version of Debian Stable was released before KDE2, which was before the license issues were resolved. You can easily find KDE 2.1.x packages for Debian Stable. The versions of KDE in Testing and Unstable are quite fresh, and will be included in future stable versions of Debian.\n\nOh, and Debian has no default desktop.  However, two debian-derived distributions, Libranet and Progeny, favour KDE and GNOME respectively, though both offer both.  Overall, KDE is now quite well represented in Debian and many Debian users are also KDE users.\n\nAs for Red Hat, I haven't used it in a while, but they've got quite a lot invested in GNOME, so they have a good incentive to use it..."
    author: "Ben Hall"
  - subject: "Re: Observations"
    date: 2001-10-13
    body: "You can easily find packages but users have to explictly select/download KDE. Many Debian and RedHat users don't just use what they come across - that was my point. I'm sure Debian 3.0 with included KDE 2.2.2 will increase KDE user count further."
    author: "someone"
  - subject: "Re: Observations"
    date: 2001-10-13
    body: "Well, by the time Debian 3 comes out, It may have KDE 3.1 or so; from the Debian site : \"We'll release it when it's ready\" :-)"
    author: "Carbon"
  - subject: "Re: Observations"
    date: 2001-10-14
    body: "I don't think new major versions are allowed to enter before Debian 3 release."
    author: "someone"
  - subject: "Re: Observations"
    date: 2001-10-15
    body: "In fact, this is a usual approach in Debian. \nYou install the programs in the moment you need them.\nIf you need KDE2 (in testing or unstable) just type:\n#apt-get install kde2\nThere is no reason to change this great behaviour.\nSo, I don't think, that Debian is lowering the\nnumber of KDE users."
    author: "BoodOk"
  - subject: "Re: Observations"
    date: 2001-10-13
    body: ">First placed RedHat and second placed Debian \n>doesn't have KDE as their default desktop \n>or included in their latest stable release.\n>Nevertheless KDE wins the desktop category \n>with 40% of all votes.\n\nIt's called vote-rigging. Heavily advertise the poll, and get all the little advocates to go and vote for your choice. Votes like this are more a test of slobbering zealotry than popularity or technical quality - it's hardly surprising that KDE won!"
    author: "Wiggle"
  - subject: "Re: Observations"
    date: 2001-10-13
    body: "Name the KDE-related webpage, mailing-list or newsgroup where this poll was advertised."
    author: "someone"
  - subject: "Re: Observations"
    date: 2001-10-13
    body: "Hey it'S just Wiggle. Don't expect him to back up the allegations he makes..."
    author: "AC"
  - subject: "Re: Observations"
    date: 2001-10-13
    body: "whay are you here if you feel that way? It serves no purpose to make insulting  comments. If you don't like KDE spend time on a site with somthing you like. Slashdot is a good place for you. :)"
    author: "L.D."
  - subject: "Re: Observations"
    date: 2001-10-13
    body: "Sorry, I was really drunk when I posted this comment. \n\nIn reality, KDE rocks! I've been in love with it since version 2.0.  No wonder that KDE won. I personally know a lot of people who have switched from GNOME to KDE in the last year. Perhaps, a year and a half ago, these stats would have been reveresed. But since then, GNOME has been publishing press releases while KDE has produced a high quality desktop environment. Good work KDE developers!"
    author: "Wiggle"
  - subject: "Re: Observations"
    date: 2001-10-14
    body: "I'm a gnome convert. After KDE 2.0 came out gnome is just to archaic to really use."
    author: "craig"
  - subject: "Re: Observations"
    date: 2001-10-14
    body: "ummm, yeah. OR just maybe KDE is actually a desktop GUI that is breaking new ground for linux/*bsd. It is a flat-out fact that kde is superior to gnome. gnome is a bloated clunky pile of crap that needs to either shape up or ship the hell out. Distros that are using gnome as their default desktop are morons and only holding the linux movement back (THIS IS A FACT and anyone with a logical thought process can see this). anyone who is desktop neutral or uses a window manager other than kde or gnome always says something like \"well, i don't use either, but kde is pretty damn slick these days, and konqueror.. wow!\". get over it gnome zealots, your desktop is crap, use it, consume all of your ram with your cool fat hoggy desktop + fat hoggy nautilus & evolution. woop DEE doo! you guys are always crying, it's getting annoying. use what the hell you want, and stop bitching and trying to put down the top dog, kde developers put in a lot of sweat to make kde the way it is, i don't know what the hell the gnome people are doing.. squirting butter on the gnome source code hoping they will see a speed increase?? who knows! they certainly arent revamping any of that bloated code to get a speed increase (wooo ok, they supposedly are with gnome 2.0.. we'll see how great that is soon enough).. where as with kde 2.2, holy smokes! they totally worked on speed with that release, i use kde every other release or so to keep an eye on it, man.. WOW.. great going kde team, gnome on the other hand i just said \"pfft\" after 1.2, get real, am i using a a GUI or a pile of thick GLUE?. i actually used blackbox for over 3 years and window maker for a few before that, but i will admit with the kde 2.2 release they have won me over because of the speed improvements. hopefully one day i can say the about gnome, but i highly doubt it."
    author: "kernski"
  - subject: "Re: Observations"
    date: 2001-10-15
    body: "Yes, KDE rocks. But please: no GNOME bashing. Thx."
    author: "AB"
  - subject: "Re: Observations"
    date: 2001-10-14
    body: "Every poll that i've seen this year has KDE coming way ahead and everytime some Gnome Free Software Fanatic has claimed that it was rigged. Get over it!! Way can't you Gnome zealots just admit KDE is Way whead in acual use? The Mandrake ceo even proclaimed that there was a 40-40 split last spring. Sone after a Mandrake user poll on Pclinux online show it was more like 75-25 with KDE ahead. Seems like the competion would rather spread lies and misinformation then face the truth that there loseing this battle. Poor poor Miguel at least its not your own money your throwing away.\n\nCraig"
    author: "craig"
  - subject: "Re: Observations"
    date: 2001-10-14
    body: "I admit it, sorry.\n\nI was just going through a really rough time in life, so trolling against KDE made it easier. But, the reality is, that KDE is really the way to go, and GNOME, although nice visually, is just not as focused and integrated right now.\n\nMy apologies to all KDE Developers/users and all theKompany folks in particular, I'm really ashamed of it...only did I recently come to realize that I should finally grow up and stop behaving like that.\n\nHere's to KDE 3.0, Qt/Trolltech and all..."
    author: "Wiggle"
  - subject: "Re: Observations"
    date: 2001-10-16
    body: "Hey man how can u be so false !?!! first u write something so bad and after u appologize to us. \n\nAnd 2 days after u write something like this !?!??\n\nhttp://linuxtoday.com/news_story.php3?ltsn=2001-10-15-003-20-NW-CY-KE-0004\n\n\n\nwtf !?!?  whats happening in ur head ?!?!  \n\n\nPerhaps KDE team should rethink the way they are releasing software. \nSome ppl are getting to much nervous ;("
    author: "Ricardo Mesquita"
  - subject: "KDE rocks"
    date: 2001-10-13
    body: "KDE has the power to do many things:\n\n1) Release often\n2) Release Well\n3) Kick Butt\n\nI have never even been as excited about _any_ operating system release (except for maybe GNU/Linux) as I am each time KDE whips out another release.\n\n(note: I'm not calling KDE an Operating System, but for most Window-ites the GUI is all they know... :o)  Thus for some people, KDE makes many OS's a viable OS.)\n\nSometimes the new is just too much to read, and you nod off for a second, but when you open your eyes, you'll be amazed the thing you were reading about is out already!\n\nWe don't know how you do it, but it only makes us want to get involved in the community...  So congrats to all!\n\n(My personal favorite would be Kdevelop...  which I must depart to go use... (college) project is due :o)"
    author: "Gregory W. Brubaker"
  - subject: "Re: KDE rocks"
    date: 2002-08-29
    body: "Greg (or anyone else out there)...<P>I'm looking for a thorough, sensible write up why KDE rocks (esp. over Gnome). If you know of one, please let me know. If you don't stop by www.xandros.com in about a month."
    author: "Mike Bego"
  - subject: "A long rant"
    date: 2002-08-29
    body: "> write up why KDE rocks (esp. over Gnome)\n\nWell, as an off and on contributer to KDE software (both within cvs and without), i'd like to point out that this \"competition\" between KDE and GNOME is a bit ill-founded. Both exist, and neither of them will go away as long as developers have interest.\n\nI love KDE? Why? Because it suits my tastes. GNOME suits other people's tastes. Use both desktops and settle on which one you like. Don't make essays that advocate either of the desktops over each other. I think that simply fragments the community. \n\nInstead, write stuff why KDE or GNOME is better than commercial desktops such as Microsoft Windows (c)(tm), or Apple MacOSX (c)(tm). \n\nYes, I realize that competition between KDE and GNOME spurs development, but I think it has gone too far. Most developers in both projects develop for fun, not because they want to kill the other desktop. Many of the comments in this article I think are taking things too far. People who say that Redhat is out to destroy KDE forget that they made many changes to \n\nAs for the about KDE menu item, I think it's out of place for people who run KDE applications outside of the KDE environment. I personally think it should be taken out of all KDE apps anyways. The user should (and is) given a chance to run KDE apps outside of KDE. "
    author: "fault"
  - subject: "Love KDE?? Ahhh! ;-)"
    date: 2001-10-14
    body: "List:     kde-cvs\nSubject:  www\nFrom:     CVS by granroth <kde-cvs@kde.org>\nDate:     2001-10-14 7:24:37\n\nwww webmirrors.html,1.96,1.97\nAuthor: granroth\nSun Oct 14 07:24:27 UTC 2001\n\nModified Files:\n         webmirrors.html\nLog Message:\nRemoved kde.nebsllc.com since it's... well, let's just say it's not a\nKDE mirror anymore!\n\n--\n*LOL*"
    author: "Thorsten Schnebeck"
  - subject: "Congrats KDE team!"
    date: 2001-10-15
    body: "For a job well done!\n\nIt's fantastic what you have created here!"
    author: "Joergen Ramskov"
  - subject: "What a better gift to the KDE ppl!"
    date: 2001-10-16
    body: "This poll from Linux Journal just seems to show one more time that KDE rulez!!\nDont take me wrong guys, GNOME is still a good Desktop Enviroment but dont answer all ma questions as a desktop should be. \n\nI like that integration with koffice and Konqueror its superb for new linux users, like ma little bro.\n\nBut contrary to one opinion KDE wide usage didnt came all from SUSE but manly cause of Mandrake."
    author: "Ricardo Mesquita"
  - subject: "Choice"
    date: 2001-10-16
    body: "If KDE can get 3.0 out the door before Xmas, that will be quite an accomplishment.  I think that would also leave GNOME way behind in the dust.\n\nUnfortunately, GNOME suffers from the scatter brain approach of software development.  (GNOME developers are doing .Net emulation which, in my opinion is a complete rewrite of what we already have with Java Servlets/Applets/XML/XSLT and RMI/Corba.\n\nAll of which you can build apps with today and they run anywhere and everywhere.\n\nYou will gain NO technology advantage what-so-ever by switching to .Net.  You will however, increase your costs DRAMATICALLY, simply do to the fact you can't implement any enterprise wide initiative unless you have Microsoft's approval via your check book.  Absolutely rediculous.\n\nI therefore do not see interoperability at stake here between Linux and Windows, as to require .Net support.\n\nIf you are a company planning on supporting .Net, watch out.  Microsoft has some pretty nasty surprises planned for you once you buy all thier software!\n\nThe fact is, Java runs just fine on Windows and very well, in fact.\n\nI am very concerned though, because it looks as though I won't have equal choices on my desktop from now on.  KDE will be the default while GNOME is a second hand desktop at best.\n\n-gc"
    author: "Gregory Carter"
  - subject: "Re: Choice"
    date: 2001-10-18
    body: "um, don't you mean XML-RPC? The open standard? \n\n.Net is just MS's \"brand\" or implementation of it.\n\nbtw: Have a look for kxmlrpcd on developer.kde.org."
    author: "poorboy"
  - subject: "Re: Choice"
    date: 2001-10-20
    body: "Leaving GNOME to dust is not KDE\u00b4s goal. KDE\u00b4s goal is to make the best desktop environment available, which has nothing to do with GNOME. If people use GNOME that\u00b4s OK, because it\u00b4s easier to go to KDE from GNOME than from Windows, for example.\n\nKDE 3.0 will ship in February, because that\u00b4s when it will be ready. It will be released when it\u00b4s ready, not when it\u00b4s good according to the marketing people.\n\nAlso, GNOME is not adopting .NET. Ximian is developing Mono, indeed, but Mono is still not ready to be used to write applications. When it\u00b4s ready, the GNOME developers will decide if it will become a part of the platform or not. But chances are it won\u00b4t, if you read the mailing lists for the GNOME development. It won\u00b4t be a part of the platform just like Ximian\u00b4s GAL and GTKHTML libraries aren\u00b4t."
    author: "Carg"
---
<a href="http://www2.linuxjournal.com/index.html">LinuxJournal</a> has
<a href="http://www2.linuxjournal.com/lj-issues/issue91/5441.html">published </a>the results of their 2001 Readers' Choice Awards, based on ballots gathered from six
weeks of on-line voting.  KDE itself won in the
"Favorite Desktop Environment" category, with the LJ editors explaining: &quot;<em>This was one of the most popular categories, and KDE is the clear winner, receiving 40% of all votes. GNOME came in second with 24.5%, and the favorite write-in was XFce.</em>&quot;  In addition, individual KDE
packages/applications fared extremely well:
<a href="http://www.koffice.org/">KOffice</a> placed in the
"Favorite Office Suite" category;
<a href="http://devel-home.kde.org/~kmail/index.html">KMail</a> placed in the
"Favorite Email Client" category;
<a href="http://www.koffice.org/kword/">KWord</a> showed in the
"Favorite Word Processor" category;
<a href="http://www.kdevelop.org/">KDevelop</a> showed in the
"Favorite Development Tool" category; and
<a href="http://www.konqueror.org/">Konqueror</a> showed in the
"Favorite Web Browser" category.  
In a separate
<a href="http://www.unixreview.com/documents/s=1442/urm0110f/0110f.htm">review</a>,
<a href="http://www.unixreview.com/">UnixReview</a> gave favorable marks to
<a href="http://quanta.sourceforge.net/">Quanta Plus</a>, an
HTML editor designed for quick web development.  Quanta features
<a href="http://www.php.net/">PHP</a> support and will be part of the <a href="http://dot.kde.org/999199700/">HancomOffice</a> package under the name WebBuilder.
<!--break-->
