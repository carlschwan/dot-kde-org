---
title: "Tutorials On Using Anti-Aliased Fonts With KDE"
date:    2001-03-16
authors:
  - "Dre"
slug:    tutorials-using-anti-aliased-fonts-kde
comments:
  - subject: "OT: KDE 2.1-update?"
    date: 2001-03-16
    body: "Hey what is this 2.1-update I see in the \"stable\" directory at ftp.kde.org ?  There are only a few files in there, and they are listed as pre-2.1.1.  Are these some late fixes?  Did I miss an announcement? :)\n\n-Justin"
    author: "Justin"
  - subject: "Re: OT: KDE 2.1-update?"
    date: 2001-03-16
    body: "you didn't miss anything ;)\n\nas you probably know, 2.1.1 will be 2.1 + critical bug fixes, new icons and improved i18n and documentation. so those are the critical bug fixes, new icons and improved doc they have so far. you can get the i18n-updates from cvs."
    author: "Evandro"
  - subject: "It was the FontPath all the time"
    date: 2001-03-16
    body: "... and I spent almost a week dissecting kdelibs, qt and libXft!\nWhy me?"
    author: "Arnd Bergmann"
  - subject: "Re: It was the FontPath all the time"
    date: 2001-03-16
    body: "I peel your fain... er... feel you pain. ;-)\n\nI didn't quite go that far but I failed to comprehend that xfs and xft load seperate font collections. Seems a bit weird and less than efficeint. Then again it's interesting to have font servers running like that. I found XftConfig a long way from my other config file.\n\nHowever my system NEVER looked so good so it was worth it!!!\n\nBTW anyone get Corel Draw's font server working with this menagerie?"
    author: "Eric Laffoon"
  - subject: "Re: It was the FontPath all the time"
    date: 2001-03-16
    body: "Damn, it was not the FontPath. I just thought that if the article is so specific about it, there has to be some meaning behind it.\n\nAnyway, I still can't select non-antialiased fonts in KDE. The most obvious symptoms of this are that in Konsole, you can only choose a font called 'fixed', which can be replaced by anything in XftConfig or is automatically replaced by the alphabetically (!) first font otherwise. \nThe same happens to the font settings in Control Center when you try to get back to defaults or have a fresh installation.\n\nI have fixes for both symptoms in kdelibs, but the real problem is somewhere deeper.\n\nDoes anyone actually have non-AA fonts in kde2.1/Qt2.3/XFree86-4.0.2-branch or does is work in another version?"
    author: "Arnd Bergmann"
  - subject: "Re: It was the FontPath all the time"
    date: 2001-03-16
    body: "The problem is with Xft. Xft has a bug related to choosing fixed width fonts. Please look at http://lists.kde.org/?l=kde-devel&m=98399428807752&w=2\nwhich contains a fix for your problem.\n\nRavi"
    author: "Ravi"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "If you're on Debian/Unstable, all you should need to do is put set the environment variable QT_XFT=true. The newest KDE and QT have built-in support for anti-aliasing."
    author: "John Donagher"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "I'm using Debian, but I'm using only the testing branch, and apparently I have the wrong XFree packages (even though they are 4.0.2 not 3.3.X).  Trying to start an xterm with anti-aliased text gives the error \"missing extension RENDER on Display 0.0\" or some such.\n\nThe KDE and QT packages are correct (QT_XFT is set to 1, not that it helps without the render extension).\n\nIs there any way to get XFree packages with the Render extension enabled without going over to unstable entirely? (I tried compiling XFree from source once but it failed.  I have the worst luck compiling software!  I can never get KDE to compile either!)"
    author: "not me"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "What video card are you using? maybe that's  caused if your video driver doesn't support the render extension (for example, the old nvidia closed-source drivers)."
    author: "funkychild"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "I'm using a S3 Savage4, and I had heard that the Savage driver didn't yet support the render extension, so I tried the VESA driver(which does).  It didn't work either :-("
    author: "not me"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-17
    body: "The latest driver for the savage does have the render extention in it .. go to tim's page and pick it up \n\nhttp://www.probo.com/timr/savage40.html"
    author: "cthulu"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "I FINALLY got Anti-Aliased TT fonts to work under KDE - VERY nice!\n\nI have only one question - the \"fixed\" font keeps getting set back to \"adabi\" (which was at first the default everything was set to) - this font seems to be used in quite a lot of web-pages, and I would like to choose something more appropriate.  Any suggestions?"
    author: "Matthew Vinton"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "This is a bug in XFree86, I believe.  You'll either have to update to XFree86 CVS or apply a workaround patch to Qt, which I don't have handy but I think Lars posted one on the kde-devel list."
    author: "KDE User"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "XFree86 4.0.3 is very close to release. Hang in there for a few days."
    author: "Matt"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "are you sure?\n4.0.2 was said to be \"very close\" during months.\n\nWhere did you get this information from, I'd like to know more about 4.0.3 (esp. if there will be a working G400 with 3D hardware accel., using what Gareth Hughes did the past weeks for DRI project)."
    author: "oliv"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "look here:<br>\n<a href=\"http://www.xfree86.org/cvs/changes_4_0_2.html\">http://www.xfree86.org/cvs/changes_4_0_2.html</a>\n<p>\nas you see, Xfree 4.0.3 is really close\n"
    author: "krzyko"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-19
    body: "You can get Xfree86 4.0.3 now from their ftp site (or better yet, one of their mirors). It is available as a patch to 4.0.2, but I have yet to find the complete source tarball for it."
    author: "Josh Liechty"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "This is a quick fix. It will not solve the problem, but you will have a fonttype that is readable. In your ~.kde directory, go to share/config/ and open the file kdeglobals in kwrite or something. Edit the fontspart of it, have a look and you'll know what I mean.... You should then change the fonttype \"default\" to courier, or some other available font of your choice. Don't go back to \"look and feel/fonts\" in Kcontrol. It will reset your manually chosen font. Then you have to edit kdeglobals once more.... Cheers!Geir K"
    author: "Geir Kielland"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "Hm, not luck here. I compiled Freetype first, then downloaded XFree from cvs, edited host.conf file, compiled XFree (without any errors), then compiled qt-2.3beta1 (I suppose that is also good one) with -xft option, then compiled whole kde 2.1 from tarballs, then added path to my ttf directory in XftConfig file, then deleted fonts directory in /.kde/share and deleted kdefonts file in /.kde/share/config dir, etc.etc. but nothing happens. I don't have antialiased fonts. When I run\nxdpyinfo in konsole I don't have RENDER extension there and I get this: XFree86 version: 4.0.99.1. Is it the right version? I downloaded it from XFree cvs (cvs co xc).\nI read all tutorials (I also did: export QT_XFT=true in .xinitrc file in my home dir), but nothing helps. I have Matrox Millenium 450 graphic card. Is it card that is problem?"
    author: "Barracuda"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "[david@dolphin david]$ xdpyinfo\nname of display:    :0.0\nversion number:    11.0\nvendor string:    The XFree86 Project, Inc\nvendor release number:    4002"
    author: "David"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "Hi,\n\tI got XFree-4.0.2 binary from Rawhide, alongwith freetype2 , qt2.3.0 which is compiled with xft support. When I try running KDE (or any QT app) after setting QT_XFT=true, I get \" Xlib:  extension \"RENDER\" missing on display \":0.0\" \" , which is true, since I dont' see RENDER from output of xdpyinfo.\nQuestion is how to enable RENDER ? I have a Intel 810e chipset.\nCan anyone help me to see clearly :) ?\n\n\nParag"
    author: "Parag"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "I also have the same issue. I am using Debian\\testing with i810 card.."
    author: "Kundan Kumar"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-17
    body: "The RENDER extension needs the module 'extmod' to be loaded in XF86Config, and FreeType also needs the 'freetype' module. Btw, QT_XFT should normally be set to 0 or 1.\nMy X display info:\n\nname of display:    :0.0\nversion number:    11.0\nvendor string:    The XFree86 Project, Inc\nvendor release number:    40099001\nXFree86 version: 4.0.99.1\n...\nnumber of extensions:    30\n    BIG-REQUESTS\n...\n    RECORD\n    RENDER\n    SECURITY"
    author: "Triskelios"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-17
    body: "Trisk, can you please explain how to load \"extmod\" module? When I add the line Load \"extmod\" in XF86Config-4 then I can't start X, I get error that it is incorrect. And how do you set QT_XFT to 0 or 1 ?\nThanks."
    author: "Barracuda"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-17
    body: "Yeeeeees, I've solved the problem. Matrox has released the new driver for Matrox Millenium G200, G400 and G450 for XFree 4.0.2. I just installed the new driver and \nantialiased fonts are here. Bravo Matrox :::)))"
    author: "Barracuda"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2002-01-20
    body: "Hey this last step was a bit to fast - i still have the antialiasing problem in my setup - and i havnt found a solloution yet.\n\nive using a Mandrake 8.0 - KDE with a TNT card.\nfirst of all, my KDE is running fine at this time, but im having a problem compiling a program using Qt (3.0.1) - and it seems to me that im getting the same problem here as you are referring to.\n\nive downloaded the sources from Trolltechs home page and tried compiling it both with and without support for antialiasing (both occasions it took almost 8 hours to complete, and im not willing to do it all to often.) and i got the same result in both situations)\n\nwhere am i? what shall i do? - download the XFree kit and recompile it as well?\n\nBefore i do, i hope i can get a good advice from someone who have had that problem.\n\n-Tobibobi-"
    author: "Tobibobi"
  - subject: "what should be said"
    date: 2001-03-16
    body: "prior to anything is wich driver it works with\ni tried a few months ago and recompiled xfree many times in search of my mistake when it was the drivers which were unable to render AA.\nno, it's not funny...."
    author: "nonono"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "For anyone using SuSE 7.1 there is a nice guide here:\n<br><br>\n<a href=\"http://www.nadmm.com/show.php?story=articles/aafonts-HOWTO.inc\">www.nadmm.com/show.php?story=articles/aafonts-HOWTO.inc</a>\n<br><br>\n(Probably useful for people running other distros as well.)\n"
    author: "Matt"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-17
    body: "Yesss!!!!\n\nThanks man. This tiny tutorial got me up 'n running in under five minutes!\n\nThanks again."
    author: "Cihl"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "I just wanted to say, that if you are running either on a laptop, or are using a _digital_ LCD screen, then you really should turn sub-pixel rendering on.  Add this to your /etc/X11/Xresources (or wherever your dist put it, I'm using Mandrake 8.0beta) and add this somewhere:\n\nXft:rgba\n\nAnyway, that's it.  It makes as big a difference, I think, compared to regular anti-aliasing, as comparing no aa with normal aa.\n\nErik"
    author: "Erik Hill"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "I just wanted to say, that if you are running either on a laptop, or are using a _digital_ LCD screen, then you really should turn sub-pixel rendering on.  Add this to your /etc/X11/Xresources (or wherever your dist put it, I'm using Mandrake 8.0beta) and add this somewhere:\n\nXft:rgba\n\nAnyway, that's it.  It makes as big a difference, I think, compared to regular anti-aliasing, as comparing no aa with normal aa.\n\nErik"
    author: "Erik Hill"
  - subject: "QT_XFT gets reset to 0!!"
    date: 2001-03-16
    body: "I'm running Redhat 6.2 with XFree86 4.02 and an RPM of Qt 2.3.0 built with Xft.  xdpyinfo lists RENDER extention.\n\nI have the following line in my .Xclients file:\n\nQT_XFT=true\n\nAfter I start X and display my enviroment variables, I see that this has been changed to:\n\nQT_XFT=0\n\nWhat gives?  I also checked Preferences -> Look & Feel -> Style, and I did not have an option there to enable anti-aliased fonts.\n\nIdeas????\n\n\n-Karl"
    author: "Karl Garison"
  - subject: "Re: QT_XFT gets reset to 0!!"
    date: 2001-03-16
    body: "This bugged me for awhile.  First you will want to be sure anti-aliased fonts are working at ALL... while in KDE, open up a console and type QT_XFT=TRUE, then open \"designer\", which will run as a pure QT app, and if you get TT fonts, you will be golden.\n\nKDE doesn't work because kicker (I beleive) sets QT_XFT=0 everytime one starts KDE.  What you need to do is edit .kde/share/config/kcmdisplayrc, and add the following lines (both lines...)\n\n[KDE]\nAntiAliasing=true\n\nThis will perform the same action as using the non-existant check mark.\n\nGood luck!"
    author: "Matthew Vinton"
  - subject: "Re: QT_XFT gets reset to 0!!"
    date: 2001-03-16
    body: "Thanks for your help.  I assume that it will be obvious if the fonts are drawn anti-aliased then?\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: QT_XFT gets reset to 0!!"
    date: 2001-06-05
    body: "I put the lines in .kde/share/config/kcmdisplayrc and it works! Nice. Thanks."
    author: "ady"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-16
    body: "I thought I would slap up my experiences doing this with/ RH 7.0 and binary packages...  \n<p>\nsee<br>\n<a href=\"http://www.vinton-sys.org/kde-antialiasing.htm\">http://www.vinton-sys.org/kde-antialiasing.htm</a>\n<p>\nand tell me what you think...\n"
    author: "Matthew Vinton"
  - subject: "libxft - static"
    date: 2001-03-17
    body: "after compiling and installing xfree86 4.0.2 I got only static library libXft.a\nhow to get shared one? what's wrong?"
    author: "man"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-17
    body: "Well, I finally got it working after recompiling X from source with FreeType2 support enabled. It's wonderful, but Xft is only serving PostScript fonts. xfontsel can see the TrueType fonts, but nothing else can - Gimp, KDE, nothing. Any ideas anyone? I have TrueType working properly using the normal FontPath method and the freetype module, and I did remember to comment out the FontPath line in XF86Config and I did remember to add the dir line in XftConfig. Is there anything special I need to do to the directory beyond the normal ttmkfdir / mkfontdir?"
    author: "Matt"
  - subject: "Only non-KDE2 apps are werking -- weird (solved)"
    date: 2001-03-19
    body: "Welp, I deinstalled X and everything depending on it today for a fresh start. So I installed: XFree86 4.0.3, Qt 2.3.0 + xft, and KDE 2.1\n\nAnd a great mystery happened: If I start a normal QT2 app, like qhacc, or avifile/aviplay, it works great. If I try a kde app, watch:\n\nhelix@subedei (~)% export QT_XFT=false\nhelix@subedei (~)% kcalc\n(works great!)\nhelix@subedei (~)% export QT_XFT=true\nhelix@subedei (~)% kcalc\nKCrash: crashing.... crashRecursionCounter = 2\nKCrash: Application Name = kcalc path = <unknown>\n\nIf this happens to you, or KDE starts blank, do a ktrace on the process -- you will see which TTF file it's trying to read and crashing on. I guess KDE tries to load every font?\n\nUnfortunatly, I had 357 truetype fonts, and I don't yet want to go through them one by one to see what's crashing. The microsoft fonts work great however.. ugh. That was a sick statement to make"
    author: "Thomas Stromberg"
  - subject: "Re: Only non-KDE2 apps are werking -- weird (solve"
    date: 2001-03-19
    body: "Thank You!\n\nI had everything working fine, then it suddenly broke for this very reason.  After this post I'm exiting mozilla and restarting konqueror.\n\nThanks again!"
    author: "jason p"
  - subject: "How to get bitmap-fonts working when aa is enabled"
    date: 2001-03-19
    body: "After all, I got aa working. But: How can I use bitmap-fonts when aa is enabled? I read all the tutorials, but didn't find a hint."
    author: "Oliver Heins"
  - subject: "bitmap fonts"
    date: 2001-03-20
    body: "As far as I understand it, you just need to edit your XftConfig and add FontPath's for all of your other (Type1, bitmapped, etc) font directories."
    author: "Michael Driscoll"
  - subject: "Re: bitmap fonts"
    date: 2001-03-21
    body: "No, that does not work. It looks to me like Qt currently does not use the bitmapped fonts through libXft, i.e. it does never set XFT_CORE for any Xft funtion.\nOn the other hand, applications that do set this flag should work even when they are not in the directories defined in XftConfig (they are only for Type1 and TrueType) but come from the font server.\nIf anybody definiately is using both xft and core fonts in KDE, could you please mail me your configuration?"
    author: "Arnd Bergmann"
  - subject: "Re: bitmap fonts"
    date: 2001-03-22
    body: "> If anybody definiately is using both xft and core fonts in KDE, could you please mail me your configuration?\n\nAOL ME (or even better: post it here)\n\nolli"
    author: "Oliver Heins"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-20
    body: "Please help me! I am the last person on earth that does not have antialiasing working!\n\nI have aa in my xterm\nI use debian/testing with lastest kde from kde.tdyc.com.\nIn the docs I read that all packages have AA compiled in.\nNo old kcmfont etc. All made from scratch\nQT_XFT=1 etc.\nThe only thing missing is a libXft.so. I have only the static libXft.a.\nI have a lot of truetype fonts and 3 font servers xfs xfstt xfs-xtt\nXftConfig by Keithp\n\nWhat am I doing wrong?"
    author: "Mario Giammarco"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-30
    body: "Same problem here... xterm works, kde has the AA checkbox enabled, QT_XFT is correctly set but no AA in KDE!!\nAnybody knows what's happening?"
    author: "Leonel Martins"
  - subject: "It nearly works"
    date: 2001-03-28
    body: "I've set everything up nicely now - lovely anti-aliased TT fonts abound, apart from one exception - Konsole. It comes out     l    i   k    e     t    h    i    s. What's the problem?"
    author: "Dave"
  - subject: "Re: It nearly works"
    date: 2001-03-28
    body: "I have just solved the same problem for my setup - SuSE 7.1, KDE 2.1.1.  \n\nI'd used the SUSE script fetchmsttfonts to get my first truetype fonts - it downloaded 4 .exes from the MS site, but could only extract the fonts from 1 - giving my fonts like comic, georgia, trebuchet.  I also got the k o n s o l e spacing problem, and it reminded me of seeing the same symptoms when trying to used a proportional font in KDE 1 konsole.  \n\nI appropriated the truetype fonts from my Windows installation, restarted KDE and the spacing problem went away .\n\nMaybe expanding your font selection will help you too."
    author: "clickfurtle"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-28
    body: "Ok, maybe I'm just missing something here.....\n\nI've downloaded all of the Mandrake 7.2 packages that are listed in the 2.1.1 distributions directory on ftp.kde.org.  This includes XFree 4.0.3, qt-2.3.0, and all of kde 2.1.1.  I've installed all of these RPMs and thats fine.\n\nBut, no matter what I try here, I'll be darned if I can get AA fonts to work properly.\n\nI'm using the latest 0.9.7 NVidia driver, which does support the RENDER extension.\n\nI've tried updating my XF86Config to match the one in the tutorial, updated my XftConfig file accordingly, got the fonts from Keith's site, but still can't get this sucker to work.\n\nAnyone out there either using Mandrake or an NVidia card that's managed to get this to work??"
    author: "Graham TerMarsch"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-29
    body: "Exactly the same with my mandrake. Up to date drivers from Nvidia, the mandrake binaries etc. \n\nEverything seems to be right, but I suspect the XftConfig file is not really read in.\n\nIs there a way to get a verbose output from Xft or something like that?"
    author: "Josh"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-29
    body: "I've installed KDE 2.1.1t for mdk 7.2 (video card: ATI Rage-128), I got the KDE anti-alias option, but if I comment out the TT FontPath in XF86Config-4 and set up XftConfig, I have no TT fonts at all ! I've installed FreeType2 from freetype2-2.0.1-0_helix_1.i386.rpm and even recompiled it from the sources, but that doesn't seem to come from FT2.\nSo I guess the problem really comes from Xft.\n\nIf anyone has managed to get AA fonts under mdk7.2 with the KDE 2.1.1 rpms, please post a \"HOWTO\" ! Thanks !"
    author: "Herv\u00e9 PARISSI"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-29
    body: "Strangely, KDE rpms for mdk7.2 have disappeared from some mirrors..."
    author: "Herv\u00e9 PARISSI"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-29
    body: "Have any of you tried addingthe following section to ~/.kde/share/config/kcmdisplayrc\n\n[KDE]\nAntiAliasing=true\n\nThis worked for me after I got the right packages installed."
    author: "Joe"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-29
    body: "I tried that too! But I think in 2.1.1 the setting  has moved to \"kdeglobals\" or the like where it was already set (it is an option in KDE/Settings/Style now).\n\nI have - like the others - NO TrueTypeFonts in the font choice lists. Not even in QT-Designer, so it is probably not a KDE problem.\n\nWho did the mdk packages on sourceforge anyway?"
    author: "Josh"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-03-30
    body: "I've already got the AntiAliasing=true, but I can't get any TT fonts if I comment out the TT FontPath line in XF86Config. I checked the checkbox in kcm Look&Feel Style, but it really looks as Xft is disabled :("
    author: "Herv\u00e9 PARISSI"
  - subject: "Package to compile to include Look & Feel checkbox"
    date: 2001-03-29
    body: "Hey,\n\nI was just wondering if anyone knows which specific SRPM or source tarball would need to be compiling against Xft-enabled QT to enable the Anti-Aliasing checkbox in the Look & Feel section of the control panel.  \n\nI am using Mandrake 8.0beta2, and everything is set up fine for anti-aliased text already.\n\nThanks!"
    author: "Joe"
  - subject: "Debugging of XFT (Mandrake problems etc.)"
    date: 2001-03-30
    body: "Found the following in the XFree86 mailing list \"Xpert\". Can't try it now, maybe someone else can?\n\n<quote>\n\n> How do I pass/tell Xft I want to see some of the traces\n> \n> #define XFT_DBG_OPEN 1\n> #define XFT_DBG_OPENV 2\n> #define XFT_DBG_RENDER 4\n> #define XFT_DBG_DRAW 8\n> #define XFT_DBG_REF 16\n> #define XFT_DBG_GLYPH 32\n> #define XFT_DBG_GLYPHV 64\n> #define XFT_DBG_CACHE 128\n> #define XFT_DBG_CACHEV 256\n> #define XFT_DBG_MATCH 512\n> #define XFT_DBG_MATCHV 1024\n\nThere's an XFT_DEBUG environment variable that you can set to the sum of \nany of these values.  This is true even in the regular libXft builds.\n\nkeithp@keithp.com        XFree86 Core Team              SuSE, Inc.\n\n</quote>"
    author: "Josh"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-04-01
    body: "The Mandrake XFree86 4.0.3 RPM's are broken in some way with regards to antialiasing. Compiling and installing 4.0.2 RPM's results in fully functional antialiasing. And boy does it look sweet!"
    author: "Phil Messenger"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-04-02
    body: "The XFree86 4.0.3 packages really seem to be buggy.\nI loaded older packages from a cooker mirror (XFree86-4.0.2-4mdk, ~server and ~libs) and it  works!\n\nNow I can start fine tuning the fonts ..."
    author: "Josh"
  - subject: "Micro-HOWTO: AA & MDK 7.2"
    date: 2001-04-12
    body: "1) install the RPMS (there're been some new ones on KDE ftp - libmng, freetype2 and a README)\n2) setup everything for AA, at this point it should not work (generally), if it works, you're luckier than me !\n3) telinit 3 (or exit XFree if you don't use [kgx]dm)\n4) recompile & install freetype2\n5) recompile & install Xft or get a working one\n6) telinit 5 (or restart XFree if you don't use [kgx]dm)\n7) if it's still not working, you're on your own - use your brain :( Make sure your video card supports the RENDER extensions...\n\nAA fonts are really worth the effort of making it works!"
    author: "Herv\u00e9 PARISSI"
  - subject: "Re: Micro-HOWTO: AA & MDK 7.2 - FreeType2 issues"
    date: 2001-04-12
    body: "I forgot:\n*before* compiling FreeType 2.0.2,\nchange the file\ninclude/freetype/config/ftoption.h:\n\n314c314\n< #undef  TT_CONFIG_OPTION_BYTECODE_INTERPRETER\n---\n> #define  TT_CONFIG_OPTION_BYTECODE_INTERPRETER\n\nif you don't #define this, you'd get *ugly* fonts (alas, the freetype2 rpm doesn't seem to be compiled correctly - that's why you have to compile FT2 yourself, if you were wondering).\n\nRead the CHANGES file to know more about this issue and remember: software patents are not a concern for _users_, especially outside the USA, so Apple (tm of the Beatles...) won't sue us!\n\nTo compile FT2 :\nmake CFG=\"--prefix=/usr\"\nand after that:\nmake (or make install, directly)"
    author: "Herv\u00e9 PARISSI"
  - subject: "Re: Tutorials On Using Anti-Aliased Fonts With KDE"
    date: 2001-05-07
    body: "I have a starnge prob with the AA. I follow all the steps in the tutorial and restart kde - the fonts that appear *are* anti-aliased, however i seem to lose half of them!!\n\nThey mostly seem to be the M$ fonts stolen from my 'doze installation. They are available to all kde apps with AA set to off, but seem unavailable when AA is on!\n\nHelp! Has anyone else had this prob?? Cheers."
    author: "Richard Varney"
---
<A HREF="http://www.linuxplanet.com/">LinuxPlanet</A> recently published an <A HREF="http://www.linuxplanet.com/linuxplanet/tutorials/3093/1/">article</A> which contains a <A HREF="http://www.linuxplanet.com/linuxplanet/tutorials/3093/3/">tutorial</A> on how to set up anti-aliased fonts on your KDE 2 box.  This article complements the <A HREF="http://trolls.troll.no/~lars/fonts/qt-fonts-HOWTO.html">tutorial</A> posted some time ago by <A HREF="http://www.kde.org/people/lars.html">Lars Knoll</A>.