---
title: "Qt ported to Mac OS 9/X, BeOS"
date:    2001-05-22
authors:
  - "pfremy"
slug:    qt-ported-mac-os-9x-beos
comments:
  - subject: "BeOS screenshots!"
    date: 2001-05-22
    body: "That's what I call pure screenshots! Cool! :)"
    author: "Stentapp"
  - subject: "Rock on, Trolltech."
    date: 2001-05-22
    body: "What a coincidence.  I was just talking to someone who works at Autodesk, a company which uses Qt internally, and two comments which stuck out were that although they liked Qt, it was:\n\n1. Expensive.\n\n2. They would use it much more if a Mac port existed.\n\nThe actual emphasis was on 2, so good to see it being taken care of!  Trolltech strategy is just getting better and better (what with Qt/Embedded and all), and what's good for Qt is good for us.  ;)\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Rock on, Trolltech."
    date: 2001-05-22
    body: "> 1. Expensive\n\nI find this odd considering that Autodesk is not known (to me) for:\n- making cheap products\n- being so broke that they can't spend 1.2k$ on a developer for a toolkit they know saves them time (hence money)\n\nP.S: Is it possible to type html in comments or no? Encoding gives me only plain text, but a little bit lower there's a list of valid html tags."
    author: "Marko Samastur"
  - subject: "Re: Rock on, Trolltech."
    date: 2001-05-23
    body: "Escpecially since the ACIS (3D solid modelling) toolkit they use was upwards of $100K per developer seat last time I checked."
    author: "DCMonkey"
  - subject: "Re: Rock on, Trolltech."
    date: 2001-05-22
    body: "does that mean, that sooner or later\nthere will be AutoCAD for Linux ?!\n\nbye,\njochen"
    author: "jh"
  - subject: "That would be wonderful"
    date: 2001-05-23
    body: "A couple of years/releases ago Autodesk started making a big deal of how they were shifting the customizable GUI of Autocad from thier textfile based DCL to MFC. Maybe they are starting to realize that MFC is  a dead end with MS pushing .NET (and WinForms, its GUI framework), and are exploring alternatives that will also give them the cross-platform capability that, IMO, they foolishly abandoned in the rush to a Windows-only product. AutoCAD used to run on a few unices and its UI framework (Menus, Toolbars, Dialogs, Command Line, Status Bar) was, and I suppose still is to some extent, completely customizable by text files. QT would be a great complement to thier ObjectARX c++ programming interface.\n\nPS: You haven't lived till you've helped students write custom menu files for AutoCAD R10 using nothing but EDLIN !! Ack!!"
    author: "DCMonkey"
  - subject: "License?"
    date: 2001-05-22
    body: "I like the Troll's concept of proprietery/free double licensing under *nix. They need money to live.\n\nBut if I got it right, my GPLed Qt/*nix app cannot be compiled by others under Windoze/BeOS/MacX without paying fees. Is that right? If so, why this asymmetry? Isn't double licensing good for all platforms?\n\nCheers,\nPeter"
    author: "SilverSun"
  - subject: "Re: License?"
    date: 2001-05-22
    body: "> But if I got it right, my GPLed Qt/*nix app \n> cannot be compiled by others under \n> Windoze/BeOS/MacX without paying fees. Is that \n> right? If so, why this asymmetry? Isn't double \n\nYes, that's right. Well, if my memory serves me right, it's because of free software community on free Unix clones (btw, AFAIK Qt for proprietary Unixes is also not free) and because they use and like free software.\n\n> licensing good for all platforms?\n\nI'd presume so, but it's their call."
    author: "Marko Samastur"
  - subject: "Re: License?"
    date: 2001-05-22
    body: "I'm an idiot. Qt free edition is available for all supported Unix OSes."
    author: "Marko Samastur"
  - subject: "Re: License?"
    date: 2001-05-22
    body: "No, you didn't get it quite right. You can take the Free Qt/X11-version and port it to any platform you want (Windows, Mac, whatever), because it's licensed under QPL/GPL. \nThe \"official\" Windows and Mac ports that are made by Trolltech are not licensed under QPL/GPL, and you probably won't even get your hands on them without showing the money first."
    author: "nap"
  - subject: "Re: License?"
    date: 2001-05-22
    body: "Yes, but he has to port it. Which is more than just fixing few Makefiles and is probably the reason that KDE project for Win uses Cygwin (to make Windows look as much as Unix as possible) and why there's no free Qt edition for Windows.\n\nSo, in effect, he's right. You can't do what he wants."
    author: "Marko Samastur"
  - subject: "Re: License?"
    date: 2001-05-22
    body: "The point I was making that there isn't any gotchas like \"You cannot use this code on other platforms\" in the license of the Qt Free Edition.\n\nAbout the difficulty of the porting, Mosfet had a few insights couple of days ago here:\nhttp://dot.kde.org/990126889/990222387/"
    author: "nap"
  - subject: "Re: License?"
    date: 2001-05-22
    body: ">I like the Troll's concept of proprietery/free double licensing under *nix. \n>They need money to live.\n\nYes, it's a realistic way:\n-If you want to learn or try QT, you can do it for free.\n-If you want to develop in open source, you can do it.\n-If you want to make money, you can buy a licence.\n\nAnd they don't dream on hypothetical support.\n\nThank you!!"
    author: "thil"
  - subject: "Re: License?"
    date: 2001-05-22
    body: "The way it works is this -\n\nYou can get Qt licensed using the GPL, but anything you do with it must also be released under the GPL (good for students, not for companies). Under this someone could port Qt to Windows, but the Windows port would have to be under the GPL, and so any Windows programs using the port would have to be under the GPL\n\nor\n\nYou can get Qt under their proprietary license, and then release your software under any license you feel like."
    author: "Bryan Feeney"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "Looking at that Aqua QT Designer is almost scary; I keep thinking that it's a faked screenshot.\n\nBut, to a certain extent, all OSes seem to be going POSIX/*nix based, with even Windows vaguely heading in that direction.  It would be very interesting if KDE/Qt became the \"write once/run anywhere\" environment that Java always promised to be.  (Sure, there is a compile for each platform in there, but it's a painless one).\n\nLet's review, shall we:\n\nBSD, AIX, Solaris, Linux with X11 - Qt/X11, KDE\n\nOSX - Qt/Mac, KDE pending\n\nBeOS - Qt/Be port 90% complete, KDE pending\n\nWindows 95, 98, NT4, 2K, XP - Qt/Cygwin getting there, KDE getting there\n\nAny OS with a Framebuffer (PocketPC, Internet Appliances, Tablets) - Qt/Embedded, Konqueror ported, KDE pending\n\nWhat else *is* there?  What OS did I miss (I'm sure a few *nix flavors), and what ports did I miss?\n\nAny core developers care to talk about how they feel about KDE expanding out to a portable desktop environment?  \n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "It's probably a good thing for Trolltech that they can sell Qt for yet another platform, but I don't see why KDE should follow were Qt goes. \n\nIt's hard enough just to make an excellent desktop environment for *nix. I think that pording to so many new platforms will result in worse performance on *nix.\n\nSo the question should be wether or not KDE should be ported to all of the above."
    author: "Steven Bosscher"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "IF KDE works under BeOS Than BeOS denvolopers will help denvolop KDE"
    author: "Underground"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "But there are things like the new administration system. Its hard to coordinate different linux distros but its harder to maintain complete different systems. You need _some_ developers, more discussions, more coordination...\n\nAnother point:\nIf you take a look at QT3 I see some danger:\nWith all the different ports and the new features it is not so interesting for the commercial programmers to write native KDE-apps but QT-only.\nThis is the point, where I say, ok TrollTech should use the QPL for _all_ systems, if the user wants to write free software. This would be KDEs chance to present this free desktop or some of its apps. But I *think* we are not strong enough for being such a big player. So lets face Unix and maybe embedded systems and stay community-based.\n\n(other opinons? ;-)"
    author: "Thorsten Schnebeck"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "> Looking at that Aqua QT Designer is almost scary; I keep thinking that it's a faked screenshot.\n\nIt's real, it runs pretty fast and it magically uses the common Mac menubar.\n\nMatthias"
    author: "Matthias Ettrich"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: ">> Looking at that Aqua QT Designer is almost scary; I keep thinking that it's a faked screenshot.\n\n> It's real, it runs pretty fast and it magically uses the common Mac menubar.\n\nJust in case anyone was wondering about my starry-eyed post above, I *don't* think the screenshots are fake (you can stop emailing me, especially the three Gnome trolls that wanted proof to \"topple the joke\" (?!?)), just that it was so impressive and integrated it looked like a concept shot... I wanted to jump in and play with the controls.\n\nAs for the rest, I never said it was a *good* idea to port KDE to every platform... and I'm not now saying it's a bad idea.  It seems Qt is going everywhere, and it's something to mentally chew on; that's why I was wondering if any core developers had thought about it (since it's up to \"those who do\").  \n\nI had done a search several weeks ago looking for OSX in the devel mailing lists to see if anybody was thinking about a port.  There was no mention in any of the lists I checked.  And I will admit that part of it is because if I had the money right now, I'd snap up a Titanium PowerBook.  Drool, drool.  But I'd want to have Konqueror and a few other KDE apps for comfort factor (switching back and forth), while still running MacOS for the Video editing apps.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "But OmniWeb 4.0 is a great Mac OS X browser"
    date: 2001-05-24
    body: "You wrote: \"But I'd want to have Konqueror and a few other KDE apps for comfort factor (switching back and forth), while still running MacOS for the Video editing apps.\"\n\nNo disrespect to the excellent Konqueror, but why wouldn't you use OmniWeb 4.0 for browsing on Mac OS X - it is arguably the World's best browser in typography terms, the longest established (under development for 8 years, and predates Netscape)?\nWill Shipley, the developer discusses it here:\n\nhttp://www.oreillynet.com/pub/a/mac/2001/05/11/w_talk.html\n\nBut other than that, I agree how great it would be to run Qt/KDE apps under Mac OS X.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "But OmniWeb 4.0 is a great Mac OS X browser"
    date: 2001-05-24
    body: "You wrote: \"But I'd want to have Konqueror and a few other KDE apps for comfort factor (switching back and forth), while still running MacOS for the Video editing apps.\"\n\nNo disrespect to the excellent Konqueror, but why wouldn't you use OmniWeb 4.0 for browsing on Mac OS X - it is arguably the World's best browser in typography terms, the longest established (under development for 8 years, and predates Netscape)?\nWill Shipley, the developer discusses it here:\n\nhttp://www.oreillynet.com/pub/a/mac/2001/05/11/w_talk.html\n\nBut other than that, I agree how great it would be to run Qt/KDE apps under Mac OS X.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "What would be really neat is if somebody decided to build an operating system from the ground up, and put KDE in its core!  That would be great."
    author: "Steve Hunt"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "A \"KDE\" based (or Qt based) OS would be really great in terms of of speed & power.\n\nBut, I think the birth of such an OS would be the crack that eventually fractured linux into a number of incompatible *nixes.  \n\nMaybe someday if a common GUI toolkit is supported and <B>embraced</B> across the current *nix platforms..."
    author: "Eric Nichlson"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "Personally, I think that Linux is already incompatible with itself.  I love it anyway, but it already is very diverse.  That is exactly a new OS would be nice.  Probably not based on UNIX, also."
    author: "Steve Hunt"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "I was wondering how hard it would be to cut X dependencies out of KDE and port Qt to GGI using KGI drivers for hardware acceleration.  ICE would be the biggest problem, as far as I can see, but it doesn't sound impossible.  \n\nThat would be a pretty tight package.\n\n - Walt"
    author: "Walter Reel"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "Qt/Embedded uses the Framebuffer Console, which makes things even better.  No need for GGI."
    author: "Justin"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "Actually, QT/Embedded is quite slow due to the lack of hardware acceleration.  If GGI has hardware acceleration, then a port to it would be much faster and therefore better."
    author: "not me"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-23
    body: "I thought I heard that the framebuffer was slow in comparison (even to X), I'm guessing due to a lack of hardware acceleration. Would GGI have hardware acceleration?"
    author: "anonymous"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-23
    body: "Hasn't the Xfree86 4.X driver model been modularized (so that card makers can provide binary only drivers)? Would it be practical/advantageous to create a minimal layer between these drivers (source or no source) and a toolkit such as QT?"
    author: "DCMonkey"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-23
    body: "Yes, the architecture in XFree86 4 provides for loadable drivers that can be in binary form (this is how Matrox distributes their HAL drivers now).  The idea though, for the KGI project, is to provide a clean interface to hardware drivers without being specific to a particular display implementation (like an X server).  That would be adventageous to video-driver writers because their efforts could be used by an X server or SDL or Berlin etc...\n\nI was thinking that a GGI port would be adventageous for Qt/KDE because of the _potential_ performance boost that would be brought about via KGI and hardware accelerations like BLT's and ROP's not to mention ease of access to alpha channels, true transparent term sessions, fade effects, anti-aliasing blah blah blah.  Mind you that that KGI project is in alpha and, to my knowlege, has only one card supported  ~:| \n\nXRender with DRI is supposed to be offering all those features too but I find that there are a lot off people who still would like to se an X'less desktop (especially me after XFree hardlocks my stand alone box and I have to hammer the reset button and fcsk!).\n\nJust some ramblings...\n\n - Walt"
    author: "Walter Reel"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "One guy _has_ decided to build an operating system from the ground up.  Furthermore, he has nearly finished!  Check out AtheOS at www.atheos.cx.  Only problem is QT is not available for AtheOS.  Now that's a port I'd like to see."
    author: "not me"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-23
    body: "That's exactly what came into my mind when I was reading this comment.  \n\n...if this guy only would have used Qt!"
    author: "Michael"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "I think that now Trolltech every major UNIX, Windows, Mac and Embedded covered, the next step is to develop a Qt port of AWT/SWING for Java. Once that is done, I think most bases are then covered.\n\nI would like to express great thanks to all the guys and gals at Trolltech. They make a great product and work very hard for it. If it were not for Qt and Trolltech I would not have learned so much about GUI coding, and it has been a real education for to learn about Qt and how to code a GUI app. One thing that many people seem  to forget is that not only are you getting a great piece of software with Qt, but you are getting a great opportunity to educate yourself and learn a lot. This combined with the general goodwill of the developers makes Trolltech a very approachable company.\n\nI hope that with all the fininacial difficulties that are being experienced by Linux distros and companies, that they keep afloat. I am particularly keen on Trolltech, theKompany and Mandrake to succeed. Keep up the good work, and thanks a lot. :-)"
    author: "Jono Bacon"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "---------->\nIf it were not for Qt and Trolltech I would not have learned so much about GUI coding, and it has been a real education for to learn about Qt and how to code a GUI app. One thing that many people seem to forget is that not only are you getting a great piece of software with Qt, but you are getting a great opportunity to educate yourself and learn a lot.\n<----------\n\nThrow in a mention of KDevelop and you have my experience exactly. I knew how to write Hello World  and how to read other people's code but it was the Qt/KDE libraries, tools and documentation that enabled me to step out on my own."
    author: "Otter"
  - subject: "Here is a start"
    date: 2001-05-23
    body: "http://developer.kde.org/language-bindings/java/"
    author: "DCMonkey"
  - subject: "Aqua style"
    date: 2001-05-22
    body: "Since QT draws everything by itslef and doesn't use native peers like Java, it means that they implemented entire Aqua style, thus we will soon see real Aqua style in QT available on all platforms!"
    author: "Michael"
  - subject: "Re: Aqua style"
    date: 2001-05-22
    body: "I can't comment on whether thats true from a technical point of view, but even if it were, I don't think the Aqua style will make it to Qt/X11.\n\nApple is sueing everyone *thinking* about cloning the Aqua style, be it only with themes. I'm sure Trolltech doesn't want to have those problems and will refuse to publish it."
    author: "me"
  - subject: "Re: Aqua style"
    date: 2001-05-22
    body: "IIRC the only Cease and Desists it sent were to people using the name 'Aqua' for their theme."
    author: "Richard Moore"
  - subject: "Re: Aqua style"
    date: 2002-12-15
    body: "Hi, I would be interested if someone had a copy of one of those cease and desist letters they could send me or e-mail me, so I could check out what exactly it is that Apple are asking people not to do.\n\nTIA, \n\nAlex Thurgood\nIP Attorney, France"
    author: "Alex Thurgood"
  - subject: "Re: Aqua style"
    date: 2001-05-23
    body: "Historically, Apple's lawyers have had no problem with you emulating the Mac look-and-feel on the macintosh, but they'll send cease-and-desists if you make Mac look-and-feel available on platforms other than Mac.  That was the case back when I worked for Visix, yet another cross-platform gui toolkit company. (see e.g. http://www.byte.com/art/9507/sec9/art8.htm).  Visix kept tighter control over its source code, though, so a compile time NO_MAC_LAF flag meant something.  It will be interesting to see how trolltech handles this."
    author: "baxter"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "I don't see any mention of MacOS 9. So I suspect, since they talk about MacOS X, that the port is MacOS X only. That makes quite q difference. A MacOS 8/9 would surely be a great advantage."
    author: "Hubert Figuiere"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "Code-wise, a OS 9 port is not terribly difficult, just that OS 9 is a nightmare to develop for. Random resource limitations, no shell, no memory protection, ... \n\nBut the real question is: How many MacOS users will _not_ upgrade to MacOS X?"
    author: "Matthias Ettrich"
  - subject: "How many Mac OS users will not upgrade to OS X?"
    date: 2001-05-22
    body: "I was a Mac OS 9 user.  I heard of some $130 product called Mac OS X, but I decided to get Linux instead, because the LinuxPPC distribution was $20 and I didn't have to wait for March 24.  Now I use KDE.  (I could have chosen GNOME, but it didn't look as pretty, and I don't have an orange iMac for no reason...)\n\nAnyway, I know most Mac OS X programs use an API called Carbon.  So I would guess that Qt for Mac was also Carbon.  Carbon programs work in Mac OS 8.6 and later, too, so Qt for Mac would also work in Mac OS 9 (and probably 8.6).\n\nIf it wasn't Carbon, it would have to be either something called Classic or Cocoa.  Classic is the old API for writing a program in old versions of Mac OS.  If Qt for Mac was Classic, it would work in OS 9 and OS X (but it the OS X Classic emulator).  Classic doesn't do protected memory, and since the Classic and Carbon APIs are so similar, it makes since to use Carbon.\n\nCocoa is a so called \"object oriented\" environment.  I thing this has something to do with OpenStep (I remember that Apple acquired NeXT) and the Cocoa API is so different from the Classic or Carbon ones.  Cocoa programs don't work in OS 9, so if Qt for Mac was Cocoa, it wouldn't work in OS 9.\n\nBottom line: most Mac programs in OS X work in OS 9, so Qt would probably work too (my guess).\n\nI think that the worst part about OS X is its $130 price tag (OS 9 was less than $100) and also that it is a very different OS from OS 9."
    author: "Former Mac User"
  - subject: "Re: How many Mac OS users will not upgrade to OS X?"
    date: 2002-06-22
    body: "OS X is probably the best, and most user friendly unix distribution ever.\n"
    author: "jack"
  - subject: "Re: How many Mac OS users will not upgrade to OS X"
    date: 2002-07-01
    body: "And now supports KDE with fink http://fink.sourceforge.net"
    author: "Sroddy"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-23
    body: "I have a Mac and based on my discussions with others in this arena it will be some time before there is a critical mass of people using MacOS X.  It still really isn't done, they are releasing updates almost weekly.  I'm very excited to hear that Qt 3 works with OS 9 and X, but I'm wondering what wizardry you used.  Is it native for both or are you using the emulation layers?"
    author: "Shawn Gordon"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "Hmmm, they have MacOS 9 as part of the HTML title on the announcement: Trolltech - Qt Mac OS 9 / X."
    author: "Navindra Umanee"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-22
    body: "To what extent does the KDE code rely on X11? I think it'd be wise to start taking out the X11 specific code from the core (Maybe this is already done? I haven't checked the code all that carefully; but if it was, I figure we'd have KDE for fbdev now)  and put a layer between KDE and the display (manager).\n\nWe have a good start with QT. Even if we're not interested in porting KDE to other operating systems, we are, with any luck, going to get a better system to replace X sometime in the future. Better get ready for it!"
    author: "Johnny Andersson"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-23
    body: "I believe there is barely any X dependence in KDE, so they are in good shape.  The desktop, window manager, and ICE are probably all that would need to be changed (IMO they should do something about ICE now though).\n\nThe reason you don't see an fbdev KDE is because Qt/Embedded doesn't support X applications, and I don't think it supports a custom window manager either (can anyone correct me?)\n\nHere's a thought:  What if someone wrote an X server for Qt/Embedded?  Kinda like those win32 X servers.  Find a way to use a custom window manager in Qt/Embedded and port Kwin.  That might be pretty cool.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-23
    body: "We ship our own, slightly enhanced, version of ICE in 2.2.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-23
    body: ">>To what extent does the KDE code rely on X11? I think it'd be wise to start taking out the X11 specific code \n>>from the core (Maybe this is already done? I haven't checked the code all that carefully; but if it was, I figure \n>>we'd have KDE for fbdev now) and put a layer between KDE and the display (manager).\n\n>>We have a good start with QT. Even if we're not interested in porting KDE to other operating systems, \n>>we are, with any luck, going to get a better system to replace X sometime in the future. Better get ready for it!\n\nQT already runs on the framebuffer, so does konqueror embedded.\n\nUnfortunatly it stops there.  Having KDE on the framebuffer in its current state would absolutly suck.  Just imagine, no HW accel(except blitting), no 3d, no remote applications, no xlib/gtk/motif/x support.\n\nYou people like to blame X for everything, but you really don't have the slightest idea what you're talking about.\n\nMatt Newell\n\nPS. Sorry for being a dick, but I am sick and tired of people spreading FUD about X.\n\nPPS.  You guys really need to get the HTML security stuff fixed, plain text sucks.\n\nPPPS.  What would be really cool is if Konqueror had text formatting for line edits by using a subset of html tags. This could be fairly easily done by using the QT3 richtext control.  Maybe if I get time this summer:)"
    author: "Matt Newell"
  - subject: "X"
    date: 2001-05-23
    body: ">>QT already runs on the framebuffer, so does >>konqueror embedded.\n\nYes, that's why I used that as an example. KDE on the framebuffer wouldn't be very useful (yet), but in this crazy open-source world, someone's bound to try it. ;)\n\n>>You people like to blame X for everything, but >>you really don't have the slightest idea what >>you're talking about.\n\nX is fine, I'm not blaming it for anything; we all know that X has its shortcomings (need I enumerate them here? probably not), but if it wasn't for X, I'd be staring at a tty right now. I'm just saying that if anything new (and good, and network-transparent, like X!) comes up, we should be open to it. We're already replacing old X programs with (better?) KDE-ized ones, so the legacy X apps we need aren't that many.\n\n>>PS. Sorry for being a dick, but I am sick and >>tired of people spreading FUD about X.\n\nYeah, well I'm sick and tired of the \"word\" FUD. ;-)"
    author: "Johnny Andersson"
  - subject: "Check out Berlin"
    date: 2001-06-30
    body: "Berlin is basically pre-alpha at this point, but between it and ggi there is some COOL stuff happening.  X is good, but even the best software sometimes ages after several decades.  Even the great TeX, possible the single best piece of end user work ever, lacks some useful abilities, like handling jpg and png natively, which would require restructuring.  X was built for dumb terminals - most networked computers aren't dumb terminals anymore.  (Although at my school sometimes it feels like they've had a lobotomy...  WHY did we have to pay for NT just to remotely display X applications?)\n\nAhem. Anyway.  If X gets updated, great.  If Berlin storms in, great.  If both happen, best of all.  Then we get to decide on merits, and since they are both open they can ultimately talk to each other if someone wants that to happen.  You gotta love open source."
    author: "CY"
  - subject: "Multi-platform KOffice"
    date: 2001-05-23
    body: "To be able to run KOffice whether I'm sitting in front of a system running MS Windows, Linux, *nix, *BSD, BeOS or OSX ... now that has the potential to really make someone in Redmond sit up and take notice.  \n\nIt would be a very liberating experience too. I'd be effectively free to float between OS's as I chose.  It would also do wonders in attracting new developers from all kinds of places.\n\nJust imagine if KOffice ran on Windows and rivaled MS Office to the point where ordinary MS users were happy to use it as their first choice.  It would really open the door for new people to try out Linux (or any non-MS platform) with minimal risk to their data.\n\nThis could create an incredible amount of leverage.  Yum :)"
    author: "Macka"
  - subject: "Re: Multi-platform KOffice"
    date: 2001-05-23
    body: "Microsoft Office will not be easily detronized.\nPeople are used to it. There are some free office suites (in much better shape than KOffice) for windows and not so many people use them. They don't need them. Microsoft Office is everywhere. And most PC sellers ship MS Word and MS Works with their PC's. So, most people see MS Word as a free application. I know many people and none of them bought MS Word. But Microsoft is its own enemy. If it starts changing licensing policy (charging per year or something) then Linux & KOffice have chance to expand. But then...multimedia :( There is a long way to catch up. More and more internet sites offers streaming media only if you use MS Media Player or Apple's Quick Time. Real player simply disappears. Guess why... Multimedia IS a future.\n\nThe future Office suite will be nothing but the multimedia player. You will be able to edit your documents in it, you will be able to listen to mp3 (or whatever music), you will be able to watch streaming video, you will be able to create multimedia presentations, browse internet, chat etc. And if KDE goes right way, and I am sure it does, you will be (very soon) able to do all above mentioned things in Konqueror. Integrated. Konqi's toolbar will not look like it looks today. It will have 5-6 buttons when you launch it: Office, Multimedia, Internet, Control, Development. Click on Internet button and whauu, you have a browser, click on Control button - you get Configuration tools (now Kcontrol), click on Development button and you get Kdevelop embedded in Konqueror etc. KDE already has some of these things but partially: in Kcontrol, in Konqueror, KOShell."
    author: "Martina"
  - subject: "The Mac is where KDE/GNOME will be decided."
    date: 2001-05-23
    body: "I can't emphasize enought what a good idea this is.\n\nAnything that's good for the Mac is good for Linux, and a toolkit that makes Windows/Mac/Linux porting easy is the best thing that could happen for Linux.\n\nWhy?\n\nBecause every ISV that doesn't have a Mac version of their product wants to have one.  Linux is a distant third for most of them (though that could change).\n\nStill, even though they'd like it, they don't have those Mac ports.\n\nWhy?\n\nBecause it's hard!!  And (in my case at least), they told themselves back in '95 that they'd be able to use MS's WIN32 cross-development tool to get there easily and then found out that it didn't work completely and was withdrawn from the market (actually, I've heard that it works now, but only MS has access to it).  And the current Mac market isn't big enough to justify the cost of maintaining 2 code bases.\n\nNo matter how good KDE and GNOME are, they're not going to get mainstream support on those merits alone.  But Windows/Mac/Linux from one code base is too good to ignore.\n\nSo, TT.  You should advertise this and advertise it hard.  One suggestion, though.  The price has got to come down to the same ballpark as VC++ or Metrowerks.  Not because Intuit can't afford it, but because you need to get that snowball rolling.  At least come up with an exception for GPL'd apps.  Anything to get the buzz going.\n  Once you succeed in becoming the de-facto standard multi-platform tool, you'll make plenty."
    author: "Rob"
  - subject: "Re: Qt ported to Mac OS 9/X, BeOS"
    date: 2001-05-30
    body: "The BeOS port ain't that exciting yet as it requres X11 for BeOS. The GTK port is closer to finishing, as it already runs without the need for an X server."
    author: "stew"
  - subject: "You guys... are cool."
    date: 2001-09-14
    body: "I'm one of those weirdos who know a bunch about Windows, MacOS *and* Linux.\n\nI use Linux at home, I try (miserably, since we have this horrid Lotus Notes solution) to use Linux at work, but I've always liked the Mac and stood up for it when them close-minded Window/Linux-fanatics have been whining about how little you can do with the MacOS (which I must add, is complete and utter nonsense and always has been, but that's entirely beside the point).\n\nI seriously dislike Java Swing apps, because they try (often miserably, may I add) to emulate what the operating system is supposed to do for you. A good example is scroll-mousing, which I haven't figured out how I'm supposed to get up and running in a Swing app. This is due to this (in my opinion childish) solution of emulating widgets that don't really talk to the underlying operating system at all... this whole virtual machine thing just gives my the creeps when it comes to GUI apps. You are supposed to be able to choose an operating system by the features it has. If I want to use MacOS, it's because I want the Aqua thing, the menubar in one place, the scroll-mouse thing working and so forth. If I want to use Windows, It's because I want the cold steel-look and manubars all over the place. I *don't* want applications to look and feel different than the ones of my neighbor, and I *do* want an operating system's user to decide for himself what he/she prefers in a GUI.\n\nFirst of all, I must congratulate you on the dual-license for X11. Furthermore, I must congratulate you on *not* also using the dual-license in the Windows/Mac versions. This is not because I'm a Linux-fanatic (on the contrary I'm perfectly satisfied with the marketshare of Windows/Mac/Linux as it is), but because I think commercial apps belong to commercial operating systems, and open-source apps belong to open-source operating systems. Two satisfied consumer-groups which I believe must be kept apart up to a considerable extent, and are not meant to be mangled too much together. The open-source people should have their playground, and the closed-source group should have theirs. The groups have incredibly different goals & needs, and that's how I believe it's supposed to be. I wouldn't legislate anything, but rather use a form of encouragement... exactly the form you're using. *Allowing* Windows/Mac developers to create open-source software, but encouraging them not to do so, by making them pay for their GUI library. Furthermore, due to the dual-licensing, *allowing* Linux developers to create closed-source and commercial software for UNIX, but encouraging them to do open-source by letting them have the code for free as long as they only do open-source applications. Quite obviously, the Windows/Mac communities are *not* trustworthy enough to be given the free version, simply on their word for that they'll only develop open-source software with it. The open-source community (mostly Linux/BSD, I assume) however, is.\n\nWhoof. I just can't stop babbling about this. ;)\n\nNow, using the native components of every operating system, writing in a programming language that is supported by every single home computer operating system I'm aware of, is just beyond belief.\n\nI write open-source software in my spare time, but I'm tellin' ya... the moment I'm capable of writing something useful in Qt on X11 with the GPLed version... I'm going to buy myself a copy of Qt for Windows and Mac, compile my applications on those platforms, and release them. Then I'm gonna sit back with a cigar and a beer, and think \"Long live TrollTech\". Sounding like a fanatic, I know, but this is just so great. :) Cross-platform C++ programming without even having to buy myself the operating systems I'm developing on. So much money and time is saved, and you deserve a piece of the pie for your work.\n\nFurthermore, maybe this will open people's eyes towards the Mac and other exotic platforms. :) Maybe people will actually try them out before judging, thus, strengthening the all-so healthy and badly needed commercial competition in the operating system market. You guys might really, REALLY make a difference for operating systems like MacOS and not to mention BeOS. Just promise us you won't sell out to Microsoft. ;)\n\nJust... great work, ladies and gentlemen. ;) I can't say it often enough. Great, f***ing work.\n\nAnd for the bottom, I agree to what someone above mentioned... this is the best learning tool I've found for C++. I've tried Borland C++Builder and VC++, but they always either do *everything* or *nothing* for you. I'm writing a package manager for UNIX in Qt, doing fine so far, and I plan on writing some Internet applications as well. I've never been able to learn so much C++ in such a short period of time... well, with the exception of Gtk--, but still, you're still a little bit ahead when it comes to an easy API."
    author: "Helgi Hrafn Gunnarsson"
---
<a href="http://www.trolltech.com/">Trolltech</a> have announced that they have <a href="http://www.trolltech.com/company/announce/mac.html">successfully ported Qt</a> to the Macintosh platform. A demo can be downloaded <a href="ftp://ftp.trolltech.com/qt/mac/QtMac-pre30-demo.zip">here</a>, and screenshots can be viewed <a href="http://www.trolltech.com/image/mac/qtmac-snapshot1.png">here</a>, <a href="http://www.trolltech.com/image/mac/qtmac-snapshot2.png">here</a>, and <a href="http://www.trolltech.com/image/mac/qtmac-snapshot3.png">here</a>.
At the same time, <a href="http://www.trolltech.com/products/download/freelicense/">Qt Free Edition</a> has been <a href="http://www.escribe.com/software/bedevtalk/m24544.html">ported to BeOS</a> (screenshots: <a href="http://members.optushome.com.au/beoz/QtX11Beos.jpg">1</a>, <a href="http://members.optushome.com.au/beoz/QtX11Beos01.jpg">2</a>, <a href="http://members.optushome.com.au/beoz/QtX11Beos02.jpg">3</a>, <a href="http://members.optushome.com.au/beoz/QtX11Beos03.jpg">4</a>, <a href="http://members.optushome.com.au/beoz/QtX11Beos04.jpg">5</a>, <a href="http://members.optushome.com.au/beoz/QtX11Beos05.jpg">6</a>, <a href="http://members.optushome.com.au/beoz/QtX11Beos06.jpg">7</a>, <a href="http://members.optushome.com.au/beoz/QtX11Beos07.jpg">8</a>, <a href="http://members.optushome.com.au/beoz/QtX11Beos08.jpg">9</a>) by Zenja Solaja, who is also considering a port of KOffice, Konqueror and KDE to BeOS. Along with <a href="http://www.trolltech.com/products/qt/embedded/">Qt/Embedded</a> and <a href="http://sourceforge.net/projects/kde-cygwin/">Cygwin/KDE</a>, KDE/Qt might soon be the most ported toolkit and desktop environment.
<!--break-->
