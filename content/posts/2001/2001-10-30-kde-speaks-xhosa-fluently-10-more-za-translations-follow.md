---
title: "KDE Speaks Xhosa Fluently, 10 More .za Translations To Follow"
date:    2001-10-30
authors:
  - "dbailey"
slug:    kde-speaks-xhosa-fluently-10-more-za-translations-follow
comments:
  - subject: "Tarzan say good!"
    date: 2001-10-29
    body: ">translate KDE into the eleven official languages of South Africa.\n\nUm-Gawwah KDE\n\n\nTarzan like! Jane, come see tarzan's kde. Cheeta no touch!"
    author: "Tarzan"
  - subject: "Re: Tarzan say good!"
    date: 2001-10-29
    body: "So now we know that stupid racists also post their comments here."
    author: "ac"
  - subject: "Re: Tarzan say good!"
    date: 2001-10-30
    body: "I thought Tarzan and Jane were white, and Cheetah was a monkey.\nAre you accusing Tarzan of racism against his own race, or specism against Cheetah?"
    author: "Roberto Alsina"
  - subject: "Re: Tarzan say good!"
    date: 2001-10-30
    body: "No,\nI accuse someone who associates \"the translation of KDE to South African languages\"\nwith stupid phrases ( Um Gawwah KDE ) of racism.\n\nIt shows that he thinks people in South Africa have no real language to express themselves - its simply saying blacks are stupid in disguise. It is irrelevant if the person he uses to express his views is a white movie character."
    author: "ac"
  - subject: "Re: Tarzan say good!"
    date: 2001-10-30
    body: "Could you all please stop making a full-fledged thread just because of some juvenile remarks?"
    author: "AC"
  - subject: "Re: Tarzan say good!"
    date: 2001-10-30
    body: "Oh, well. Whatever."
    author: "Roberto Alsina"
  - subject: "Re: Tarzan say good!"
    date: 2001-10-31
    body: "It was supposed to be a joke, a play on Tarzans word from the movies. It has nothing do to with black people what so ever."
    author: "Tarzan"
  - subject: "Re: Tarzan say good!"
    date: 2001-11-01
    body: "is Um Gawwah a racist word"
    author: "d"
  - subject: "Great but ..."
    date: 2001-10-29
    body: "we need more KDE 3 related news too and a few screenshots. Are there any plans to have Wizards and configuration tools much like Future Technologies' ones:\n\nhttp://www.linuxutilities.org/index.html\n\nIf KDE can speak African languages, indeed it will make KDE the most used DE and Linux the most used OS! My applause and good wishes!"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Great but ..."
    date: 2001-10-29
    body: "an unwritten policy of the KDE project is to not announce anything before it is actually ready. so during large devel cycles things can seem quiet around the KDE camp, unless you are right in there reading the mailing lists or following CVS. i am nearly done this week's KC KDE (got slowed down by the weekend's Halloween party... *hic*) which should be out tomorrow and covers KDE3 devel. so, if you call that news, there it is.\n\nas for screenshots, the look really isn't all that much different. IMO KDE3 is very much a usability enhancement in the form of APIs that will be stable for a long time (allowing apps to mature instead of having to be ported as soon as they get to a stable release), better performance, bug fixes, better support for things like BiDi, security certificates, IMAP and ASMTP, etc, etc.. in other words, it will be hard to see 90%+ of the differences without actually using it."
    author: "Aaron J. Seigo"
  - subject: "Re: Great but ..."
    date: 2001-10-30
    body: "Well, the look of either KDE2 or 3 (as well as GTK), is done by plugins. This means that the rendering of widgets can be dynamically replaced anytime - you don't need to do a new KDE release. So while KDE3 doesn't include any new styles AFAIK, this doesn't mean people aren't designing new style or theme engines, they most certainly are. Most of the \"official\" work for KDE3 seems to be in theme and style management, making everything easy to install for the end user, not in new rendering engines themselves. But it *is* being worked on, and in fact things like Liquid and Megagradient originated as just tests for a new theme engine. I wrote the pixmap theme engine for KDE2, and never liked it. It had some cool stuff and a lot of efficency hacks, but is rather primative in dealing with things like alpha masks and window background options. A lot of my current style work is actually just trying ideas out and solving bugs for new theme engines. For example, Liquid looked cool but had a ton of bugs back in 0.1. Now they are almost all fixed and these solutions directly apply to new theme engine designs. While I haven't worked on MegaGradient as much since I personally use Liquid, it too has helped identify many bugs and what needs to be fixed.\n\nQT3 does have a new QStyle as well that expands the number of widget items you can do custom rendering for. I was able to hack around things not supported by the Qt style mechanism before using paint event filters, but this was messy.  \n\nMy goal is to have a pixmap theme engine that operates just as efficently as Liquid, has improvements for things that are slow in GTK's engine (like custom code for gradients drawn to the window size), support for things that are cool in the GTK engine like good alpha mask handling, more options like mouse hover, and dynamically adjusting HSV that matches the users color scheme - even for pixmap themes. For the last item, this means the person making the theme would just supply a grayscale pixmap and the engine would automatically do HSV adjustments to match the user's colors.\n\nThis won't be included in the offical KDE, since I am no longer a core developer and I could already tell that the release schedules will probably conflict yet again, but it will be available as a separate package."
    author: "Mosfet"
  - subject: "Re: Great but ..."
    date: 2001-10-30
    body: "> But it *is* being worked on, and in fact things like Liquid and Megagradient \n> originated as just tests for a new theme engine.\n\n\u00fcbercool ... \n\n> This won't be included in the offical KDE, since I am no longer a core \n> developer and I could already tell that the release schedules will probably \n> conflict yet again, but it will be available as a separate package.\n\ni don't think this is (thankfully) as much of an issue as it was in the past. with kde-look.org and apps.kde.com around there are very public and very easy ways to distribute kde specific code to a very broad audience on one's own schedule. IMO this is a very important step forward for KDE (and those who wish to deveop code for, though not necessarily with, KDE).\n\nthat said, it's great to see you back and around mosfet! hope 2002 is a better year for you (starting now ;-) and we all look forward to seeing your next creation(s).\n\n===\n\nand now to try and be slightly on topic: an inexpensive, sane and regionalized operating system (and desktop) is exactly what can make Linux + KDE an international success. leap frogging other systems in those markets can only help in a \"surround the castle\" sort of way."
    author: "Aaron J. Seigo"
  - subject: "Ever so slightly back on topic"
    date: 2001-10-30
    body: "> and now to try and be slightly on topic: an inexpensive, sane and \n> regionalized operating system (and desktop) is exactly what can make Linux \n> + KDE an international success. leap frogging other systems in those markets \n> can only help in a \"surround the castle\" sort of way.\n\nThe translation issue is one we are using to sway Governement to better support/adopt Opensource software.  Supporting hundreds of small regional languages is something that only Opensource software will be able to do, because often there is very little commercial motivation.\n\nI hope that projects like this will appear in other developing nations and help create the groundswell we need."
    author: "Dwayne Bailey"
  - subject: "Re: Ever so slightly back on topic"
    date: 2001-10-30
    body: ">The translation issue is one we are using to sway Governement to better \n>support/adopt Opensource software. Supporting hundreds of small \n>regional languages is something that only Opensource software will be \n>able to do, because often there is very little commercial motivation.\n\nSo, I take it many/most/all of the 11 official languages are not supported by Windows or MacOS? \n\nThis is a terrific project, the kind of thing that shows why large, international, collaborative hacker projects like KDE are special. I'm glad to see it's already produced one finished product."
    author: "Otter"
  - subject: "Re: Ever so slightly back on topic"
    date: 2001-10-30
    body: "> So, I take it many/most/all of the 11 official languages are not supported \n> by Windows or MacOS?\n\nThe 11 includes English so that doesn't really count (we do help out on the en_GB project).  There might be support in Windows for Afrikaans not too sure.\n\nThere definately isn't support for the other 9 languages, although I have seen some mention of dictionary support for Xhosa and Zulu in applications like Word and WordPerfect."
    author: "Dwayne Bailey"
  - subject: "Re: Ever so slightly back on topic"
    date: 2001-10-30
    body: "> So, I take it many/most/all of the 11 official languages are not supported \n> by Windows or MacOS?\n\nThe 11 includes English so that doesn't really count (we do help out on the en_GB project).  There might be support in Windows for Afrikaans not too sure.\n\nThere definately isn't support for the other 9 languages, although I have seen some mention of dictionary support for Xhosa and Zulu in applications like Word and WordPerfect."
    author: "Dwayne Bailey"
  - subject: "Re: Great but ..."
    date: 2001-10-30
    body: "Great! I also hope that pixie makes it's way back into KDE (might be wishful thinking). I grew quite accostomed to it, and then, poof, it was gone :(."
    author: "FO"
  - subject: "Re: Great but ..."
    date: 2001-10-30
    body: "Not very likely since it's no longer a KDE app, but runs just with Qt in order to support both Unix and Windows ;-) Pixie on Windows is one of the things I'm getting paid to work on these days, but it will still be free and integrate well with KDE. The thing has been completely rewritten and is much easier to use and even faster than before. It's been split into two parts: browser and effects, with the browser as the main interface. The Unix browser is done, but the effects are not."
    author: "Mosfet"
  - subject: "Re: Great but ..."
    date: 2001-10-30
    body: "Nice to see you back.\n\nYou should see how impressed people are with high performance liquid. I have had windows users ask me how they could get this look, and when I told them that they could not because it is not for windows they were really perplexed. \n\nWhat do you think about replacing X by something like DirectFB? Will this simplify writing real transparent menus, and most importantly are there significant performance gains when dropping X? I think you are in the best position to know this :-)\n\nbest regards,\n\n   Androgynous Howard"
    author: "Androgynous Howard"
  - subject: "True Window-Level Alpha Channels"
    date: 2001-10-30
    body: "These effects seem to be the most sought after visual improvement these days. I did some looking into it recently, and summed up the situation in a post to kde-devel:\n\nhttp://lists.kde.org/?l=kde-devel&m=100324780628509&w=2\n\nI don't know much about the DirectFB side of things, but my reading tells me it would take longer to get qt ported over than to implement this properly into X.\n\nHere's the summary. This is possible to implement, but requires changes to X. Many of the basic required features are already in X (hardware accelleration being the most important). It seems all that is needed is a proper \"translucent window extension\" protocol, some internal reorganisation of X, and of course, XFree86 developer support.  Keith Packard (of Render fame) is working on such an extension and predicts a 6-month time frame for early prototypes.\n\nThis would enable more than just translucent menus - think colour, alpha-blended mouse cursors; translucent applications (including translucent Noatun skins...!); transparent on-screen displays... the possibilities are huge.\n\nI am interested in seeing if I can get my skills up to work on X, we'll see."
    author: "Hamish Rodda"
  - subject: "Re: True Window-Level Alpha Channels"
    date: 2001-10-30
    body: "Yep, Keith's translucent window server is something I've been hankering after for a long time ;-) He's a busy man, tho, and this is probably low on his priority list. The guy is very good and has a lot of tasks. I don't think it will support cursors - that's not a X11 Pixmap but a bitmap, (although I don't know), but real translucent window support in the server would be quite cool in itself."
    author: "Mosfet"
  - subject: "Re: True Window-Level Alpha Channels"
    date: 2001-10-30
    body: "From what I've seen, implementing colour/alpha mouse cursors would be basically for free if tackled with the translucent window extension, as there would be an easy way for the extension to fall back to software rendering should hardware acceleration not be available.\n\nOther effects which could be tackled at the same time are window-level graphics filters, anything the hardware supports or your CPU can afford (focus/blur, maybe even refraction maps... think of cool effects with a glass theme...)\n\nIf anyone is interested in helping speed up the implementation, drop me an email (XFree86 is written in C). I'm not sure if I will be tackling it or not, my holidays are filling up with projects pretty quickly as it is."
    author: "Hamish Rodda"
  - subject: "Re: True Window-Level Alpha Channels"
    date: 2001-10-30
    body: "There is already software cursors in X11. That doesn't help change all the code that relies on it being a bitmap ;-) Making it a XRender drawable isn't going to make it any easier at all, the reverse is true."
    author: "Mosfet"
  - subject: "Re: Great but ..."
    date: 2001-10-30
    body: "No, not a good idea. I'm writing up a user-oriented X11 graphics FAQ for mosfet.org, which will explain in detail X graphics. The quick answer is X11 is rather modular these days, has many more accelerated drivers than the framebuffer, supports multi-layered channels (XRender), and has got a direct rendering interface for apps that need it. So while having QT Embedded, etc... is cool for most end users X11 is better IMO. Not to mention virtually every GUI toolkit in the Unix world supports it."
    author: "Mosfet"
  - subject: "Re: Great but ..."
    date: 2001-10-30
    body: "I think the long term goal should be to use the 3D accelerator functions of modern graphics cards for everything including two-dimensional drawing. With the huge performance and nice features of 3D cards there should be some really nice possibilities. If this is possible without dropping X it is very nice.\n\nOne thing that annoys me about X is that since drawing and user feedback happens in a separate process, there is some unnessecary latency in the user feedback. But I guess that this problem will go away when the linux kernel switches to a decent scheduler and a higher scheduling frequency, so it is not X's fault.\n\nAnother thing that sucks about X is font handling. But I guess the problem is that there are not enough good scalable unicode fonts available and that most distributions come with ugly bitmap fonts as defaults.\n\nregards,\n\n    Androgynous Howard"
    author: "Androgynous Howard"
  - subject: "Re: Great but ..."
    date: 2001-10-30
    body: "Hi Mosfet,\n\nnice to hear from you again :-)\nI (and probably many many others) were more or less concerned about your, how to put it, circumstances, mosfet.org sounds quite frustrated.\n\nLooking forward nice things on kde-look.org :-)\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: Great but ..."
    date: 2001-10-30
    body: "small correction \"mosfet.org sounded quite frustrated\".\n\nWell, I'm wondering, this ain't a picture of you ???\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Great but ..."
    date: 2001-10-30
    body: "Are you talking about the logo? Yeah, that's me in a picture from a goth club, modified to look more techno and demonic. It's one of my more normal pictures >:)"
    author: "Mosfet"
  - subject: "Re: Great but ..."
    date: 2001-10-30
    body: "How much coffee did you have to drink for the glowing eyes? :-)"
    author: "Carbon"
  - subject: "Re: Great but ..."
    date: 2001-10-31
    body: "The glowing eyes were one of the modifications ;-) The other one was HSV modification in order to make the hair more green and better match the circuit background."
    author: "Mosfet"
  - subject: "KDE improvements"
    date: 2001-10-29
    body: "Anyway, we will se that KDE improvements will be less with new versions, because the whole of the core kde has been programed. I mean, is the same with Windows95. From 95, to XP, the improvements have been less than 3.11 to 95-98. KDE will be the same. Dont be waiting for greatests news with the KDE core and the apps. Is normal. The progression will be with the documentation for example, or somethings like that. Is my opinion."
    author: "Daniel"
  - subject: "Re: KDE improvements"
    date: 2001-10-29
    body: "Err, the facts you based your opinion on are wrong.\nWindows:\nWin3.11 = Dos( 6?, 16 bit ?? IIRC )\nWin95/98 = Dos( 8 ? 32 bit ?? new Fat32 IIRC )\nWinXP = WindowsNT ( built with WindowsNT new Technology )\nThe step from Win95 to WindowsXP is actually no \"step\" regarding development,\nbut a completely different system. Different kernel, driver model, filesystem, \n\nKDE changed ( + bidi just to get more on topic ).\nMain features ( = the API ) will not change, since KDE is already very modular and extensible ( Kparts, IOSLaves ) but still, new interfaces are added to the core and these will bring an even thighter integration/interaction between applications.\nNow KDE will have standardised SQL database support, which brings up a whole new way of doing things if the KDE developers will take the jump and make \na SQL Database a requirement for KDE.\n\nAnd to think that better documentation is not as big an improvement as a new \"core\" once again perpetuates the myth of programming alchemy. A program is useless if people dont know how to use is, and documentation is very important, both for users and developers."
    author: "ac"
  - subject: "Re: KDE improvements"
    date: 2001-10-30
    body: "I believe he meant Windows as in the Windows desktop, not the Windows operating system.\n\nJust like people use the word \u00a8Linux\u00a8 to describe a kernel called Linux together with hundreds of other software packages. Yes, it\u00b4s wrong, but it\u00b4s easier and people are lazy. ;)"
    author: "Carg"
  - subject: "Re: KDE improvements"
    date: 2001-10-30
    body: "Please use XML databases for KDE, not SQL.\nSQL is obsolete. Please forget it.\n\nStart by using XML as the native file-format for all KDE applications, and go on with good XML databases like LivingXML."
    author: "Philipp G\u00fchring"
  - subject: "Re: KDE improvements"
    date: 2001-10-30
    body: "Only someone with no real-life programming experience could say something like this.\n\nI'd like to see your XML \"database\" handle a load of 5 million transactions in 3 hours...I'd like to see the XML \"database\" handle a monthly report that summarizes 40 million transactions...I'd probably retire before it completes.\n\nIf you say SQL is obsolete it's probably because your only experience with it was Access.\n\nXML is great for passing small amounts of data, SOAP, config files, etc.\nStoring any serious amounts of data in it and then retrieving it (which requires parsing it, etc.) would be a total dog compared to a much more efficient SQL-based RDBMS implementation.\n\nYou need to use the best tool for each job. I wouldn't store small config files in SQL and I wouldn't store any large amounts of data in XML. \n\nWhen Wal-Mart (100 million transactions per week from what I last read) switches to an XML database, I will change my mind. Until then, I believe they are still running mainframes (which were also supposed to be obsolete...yeah, right).\n\nThe only people pushing XML for data storage are...storage vendors (EMC, etc.) For them the size bloat introduced by XML is a boon to their business."
    author: "Jacek"
  - subject: "Re: KDE improvements"
    date: 2001-10-30
    body: "Well, I have a 3 GB (250000 records) XML database running on my Pentium 100 ...\n\nMainframes? Yes, the style of Cobol is more XML than SQL.\nXML Overhead? I don't care about some percents overhead, if I can create applications much faster and better than I ever imagined. I reengineered an application with XML in one week, where the original developers (~ 10 developers) were working for 5 years.\nI agree that XML might not be the best at the moment when it comes to databases which are bigger than 100 GB. But how many KDE applications want to work on that big databases?\n\nSize bloat? I delivered one of my XML applications one one 3,5\" floppy disk, which included the whole database, application, and even some test data.\nJust show me a commercially graded SQL database that fits on one floppy disk."
    author: "Philipp G\u00fchring"
  - subject: "Re: KDE improvements"
    date: 2001-10-30
    body: "Well, I have a 3 GB (250000 records) XML database running on my Pentium 100 ...\n\nMainframes? Yes, the style of Cobol is more XML than SQL.\nXML Overhead? I don't care about some percents overhead, if I can create applications much faster and better than I ever imagined. I reengineered an application with XML in one week, where the original developers (~ 10 developers) were working for 5 years.\nI agree that XML might not be the best at the moment when it comes to databases which are bigger than 100 GB. But how many KDE applications want to work on that big databases?\n\nSize bloat? I delivered one of my XML applications one one 3,5\" floppy disk, which included the whole database, application, and even some test data.\nJust show me a commercially graded SQL database that fits on one floppy disk."
    author: "Philipp G\u00fchring"
  - subject: "Re: KDE improvements"
    date: 2001-10-30
    body: "3 GB...?\nHow about 3 terabytes? \n\nI'm sorry, but when you get to really huge data sets, nothing but the most intense optimizations will work. SQL has proven itself over the last 20 years ...if you managed to re-engineer in XML in 1 week an app that took 5 years to build in SQL...well, I congratulate you on your skill...and recommend you immediately fire the previous developers, 'cause they obviously didn't know what they're doing.\n\nThe reason you managed to fit your app on a floppy is because your XML DB offers a lot less than what an RDBMS DB can. Stored procedures? Triggers? Foregin keys on master (i.e. non-data) tables? Optimization hints? Statistics-based query plans? Parallel query processing? Data partitioning (i.e. transparent partitioning of tables based on key values for drastic query speed improvements)? Indices (to avoid a table scan...I've seen when an index made a difference of a report cutting down from 2 hours to 4 minutes of retrieval time).\n\nI'm sure I could strip all of that out from let's say MySQL (which doesn't have half of these features anyway) and fit just a bare-bones RDBMS storage system\non a floppy.\n\nXML might be fine for maybe smaller databases, but you'd be surprised how often these small projects blossom into something larger...and then you will regret not going with a solution that emphasizes flexibility and speed."
    author: "Jacek"
  - subject: "Re: KDE improvements"
    date: 2001-10-30
    body: "That is not to say that an XML database might be the best choice for storing KDE config files...you could be 100% correct there...\n\nI just in general strongly disagree with your statement that SQL is obsolete and XML is the manna from heaven that is the perfect solution for everything."
    author: "Jacek"
  - subject: "Re: KDE improvements"
    date: 2001-10-30
    body: "Uh, it shouldn't matter at all. With QT's database middle-man, you could code any sort of QT Db interface you wanted to, and every app using QT's database support would use that w/o complaint. AIRC?"
    author: "Carbon"
  - subject: "Re: KDE improvements"
    date: 2001-10-31
    body: "Maybe yes, maybe not.\nI recall my friend who closely monitored the Qt3 beta period telling me that many users accessing DBs via the Qt middle-man instead of native APIs reported a noticeable hit in performance.\n\nThe extra added layer of abstraction (on top of ODBC for example, which in itself is already a layer of abstraction) really can add up when you are retrieving 15,000 rows of data.\n\nBut then, I'm sure the Qt guys can optimize it as the time goes by.\nJust don't expect for the first version to be just as fast as native access (e.g. calling the Oracle OCI APIs directly).\n\nJust wondering though...with Qt providing a lot of server-side X-platform code (DB access, multi-threading, XML, HTTP/FTP/file access, and soon a component model, etc.) I was wondering if someone had ever thought of coding server-side apps (i.e. no GUI) in Qt instead of let's say Java.\n\nIt seems Qt has provided a lot of the platform abstraction for key server-side modules, so don't be surprised if Trolltech releases a server version of Qt soon...all it needs is some middleware and Java better look out."
    author: "Jacek"
  - subject: "Re: KDE improvements"
    date: 2001-10-31
    body: "I can see no reason why a good database engine should not be able to support many interfaces, including XML and SQL.  There's absolutely no reason to alienate yourself from milions of SQL knowledgable users by supplying an XLM-only interface to your database.\nKDE is built on standards. Well, make a standard database interface that can plug into anything, from text-files over MySQL to Oracle or DB2.  That way you'll never get caught with your pants down.  When small UI configurations might be saved faster in flat-files, a 500 user server might benefit from a central database, possibly even running on another machine?\nDon't get me wrong, I like XML as the next geek, but it's the possibility to choose depending on your situation that makes Unix strong.\n\nJust my 2 Euro cents.\n\nCheers,\n\nMatt\n\nP.S. To prove my point, Oracle 8i offers many interfaces to it's database, including SQL, XLM, Java and PL/SQL.  And the engine is always the same."
    author: "Matt"
  - subject: "Re: KDE improvements"
    date: 2001-11-01
    body: "HUH??\n\nSQL and XML\nApples and oranges"
    author: "[Bad-Knees]"
  - subject: "Re: KDE improvements"
    date: 2001-11-01
    body: "I think ac is looking for an argument."
    author: "[Bad-Knees]"
  - subject: "Re: KDE improvements"
    date: 2001-11-01
    body: "...again!"
    author: "Billy Nopants"
  - subject: "Just like Norway ;)"
    date: 2001-10-30
    body: "Reminds me much of our own efforts to translate KDE and other open-source software into Norwegian Nynorsk. Nynorsk is the smaller of the two official Norwegian languages, which means that practically no commercial software is available in Nynorsk.\n\nThere is some more information in this year-old KDE News article: http://dot.kde.org/972035764/ (And of course, a *lot* has happened since then!)"
    author: "Gaute Hvoslef Kvalnes"
---
<a href="http://www.translate.org.za">Translate.org.za</a> (partly sponsored by <a href="http://www.obsidian.co.za">Obsidian Systems</a>) is spearheading an effort to translate KDE -- the complete desktop including Konqueror, KMail, KWord, KSpread -- into the eleven official languages of South Africa. As a first start, the Xhosa translation is already <a href="http://i18n.kde.org/teams/distributed.html">part of KDE 2.2.1</a>.
The<a href=http://www.mg.co.za> Mail &amp; Guardian</a> newspaper has <a href=http://www.mg.co.za/mg/pc/2001/10/xhosacomp.htm>a full article</a> on the project. 
<!--break-->
<Br><br>
KDE was chosen because of its maturity and active translation efforts and it is hoped that the dictionary built through translating KDE will help in translating other non-KDE applications.  The next steps in the project are to work on the <A href="http://www.mozilla.org">Mozilla</a> browser to allow users of Windows access to a Xhosa web-browser, a Xhosa dictionary, and <A href="http://www.openoffice.org">OpenOffice</a> for a cross-platform Office suite. 
<br><br>
After that the project will be moving on to the Zulu translation of KDE, eventually making computers accessible to people in all of the 11 official languages in South Africa. The languages that have the highest populations will be targeted first, facilitating the highest possible impact of the software. Translate.org.za has received sponsorship from the <a href="http://www.tsf.org.za">Shuttleworth Foundation</a> for continuation of the project.<br><br>
The possible impact of this project  is immeasurable, in my opinion.