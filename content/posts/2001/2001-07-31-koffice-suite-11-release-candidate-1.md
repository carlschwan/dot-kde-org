---
title: "KOffice Suite 1.1 Release Candidate 1"
date:    2001-07-31
authors:
  - "Dre"
slug:    koffice-suite-11-release-candidate-1
comments:
  - subject: "Suse binarys"
    date: 2001-07-30
    body: "Where is the Suse binarys..\nOh! there they are (overachievers). Just like I like them."
    author: "KDE user"
  - subject: "RTF...?"
    date: 2001-07-30
    body: "I've read some discussions on the mailing lists, but when will RTF *import* be supported in KWord (I'm guessing after 1.1 now)? It's quite important for round-tripping - I can export RTF docs fine, but I can't import them again.\n\nAnybody have any clues...?"
    author: "philmes"
  - subject: "Looks Great!"
    date: 2001-07-30
    body: "But kword is still missing essential features.\nOr am I just blind? I can't find footnotes or hyphenation."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Looks Great!"
    date: 2001-07-30
    body: "That's for after 1.1.\nWe have to stop the features at some point, and get the baby out. Otherwise people would still judge koffice from koffice-1.0, where KWord wasn't useable. At least koffice-1.1 is useable for many many common tasks. Those two features are necessary in the long run but not so much \"essential\" to get some basic work done.\n\nFootnotes are definitely coming some time after 1.1.\n\nHyphenation is a very tricky problem due to the different rules in various languages, I have no idea how to tackle that one. Does abiword do hyphenation ? :)"
    author: "David Faure"
  - subject: "Hyphenation"
    date: 2001-07-30
    body: "I guess the best thing would be to somehow make use of the Latex hyphenation code, convert/implement it to kword somehow. Latex claims to have excellent hyphenation and it has very good support for many languages.\n\nDon't know how hard that would be, but good hyphenation is hard to do, so why not build upon the works of others?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Looks Great!"
    date: 2001-07-30
    body: "Just a suggestion:\n\nFor every document the user should be able to set the language (which is saved within the document) it is written in. So there can be different \"KOffice-Plugins\" in this specific laguage for the hyphenation.\n\nWhat do you think?"
    author: "Andreas Pietzowski"
  - subject: "Re: Looks Great!"
    date: 2001-07-30
    body: "The problem is, if you have several languages in one document. Since KOffice uses xml, one could use a tag like <language=de> bla bla bla </language> for defining a language for a paragraph, a sentence or even a single word!"
    author: "Anonymous"
  - subject: "Re: Looks Great!"
    date: 2001-07-30
    body: "Since KOffice uses xml, one could use a tag like <language=de> bla bla bla </language> for defining a language for a paragraph, a sentence or even a single word!\n\nFirst comes the observation that country codes do not equal languages.  Some countries have several very different languages, some of which have the same name: America has dozens, perhaps hundreds of languages within its borders.  I have a friend who used to correspond with her father in the language of the Seneca tribe.  While KDE as a whole will probably never be translated to the vast majority of these languages, they *will* be word processed.  I don't know of any language code, but it wouldn't surprise me if there is an ISO standard.\n\nSecond comes the question: is this an appropriate forum for such discussion?  Maybe yes, maybe no.  I know the developers read the dot, as several are regular posters.  Maybe a vote is in order, just to gague people.  (Personally, I like such discussion - I think many users of KDE are people like me, developers who have too heavy a work load to take on a KDE project, or sysadmins with programming experience).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Looks Great!"
    date: 2001-07-31
    body: "> First comes the observation that country codes do not equal languages.\n\nNo, butr in this case Germany (country) and German (language) happens to have the same code. Some others don't: se=Sweden, sv=Swedish.\n\n> I don't know of any language code, but it wouldn't surprise me if there is an ISO standard.\n\nYou're correct. You can find it here: http://lcweb.loc.gov/standards/iso639-2/englangn.html\n\n> Second comes the question: is this an appropriate forum for such discussion?\n\nIf you want to discuss this more thorough you should head to the I18N mailinglist."
    author: "Per"
  - subject: "Re: Looks Great!"
    date: 2001-07-31
    body: "Ok, then lets define _one_ language at the beginning of the document and this language is the language for the whole document. (If you have multiple languages you have to make your own hyphenation).\n\nIs it a big problem to count all languages all over the world (not all dialects!) and give them different names in the <language>-tag?"
    author: "Andreas Pietzowski"
  - subject: "Re: Looks Great!"
    date: 2001-07-30
    body: "This is the point. \nHyphenation (or at least the possibility to set a 'soft-hyphen') *is* an essential feature of word processing. Without hyphenation, kword  (and abiword of course ;) are more editors then word processors. And for KWord soft-hyphenation is extremly important, because the text of the printed page never looks like the text in the kword window. \nA KOffice final release without any kind of hyphenation will never be an alternative to commercial word processors. I don't know *one* word processing application for Windows or OS/2 without this feature. \n\nThomas"
    author: "Thomas Piechocki"
  - subject: "Re: Looks Great!"
    date: 2001-07-30
    body: "Implement it yourself, ye old windbag. Where they currently are at 1.1 is not a bad position--remember when this project started, and how far it has come since 1.0. They cannot make a full-blown word processor capable of competing with Lotus or MS Word in a few months.\n\nVent your anger towards their ineptitude by implementing hyphens and sending them the patch. That is at least a little more productive than vainly blowing your creaky vocal bagpipes, exasperating that kword \"will never be an alternative\" to Windows-based word processors. Next I expect some foul, rotting lizard to crawl half-dead out of your gizzard to inform the world that \"KDE will go bankrupt\" due to \"uncompetetive offerings.\" Spare us the pleasure."
    author: "Bob Jones"
  - subject: "Re: Looks Great!"
    date: 2001-07-31
    body: ">blah blah blah hyphenation blah blah\n\nWhat the heck is everyone going on about?  I've used MS Works 4.0 for years and never bothered with hyphenation.  For your average guy writing a letter or term paper, hyphenation is waaaay down on the list of desired features (in English anyway, is it different in other languages?).  If you're writing a newspaper or something, well that's different.  But KWord isn't ready for that anyway.\n\nI hardly think hyphenation is more important than, say, footnotes or WYSIWYG printing.  Fix those first!\n\nBTW, MS Works 4.0 and all its predecessors (possibly some of its sequels too) belong on your list of Windows word processors without hyphenation."
    author: "not me"
  - subject: "Re: Looks Great!"
    date: 2001-07-31
    body: "WYSIWYG is already almost done in a Branch which will be merged to HEAD after the 1.1 release.\n\nCheers,\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: Looks Great!"
    date: 2001-07-31
    body: "> If you're writing a newspaper or something, well that's different. But KWord isn't ready for that anyway.\n\nI feel that KWord wants to be both MS Word and MS Publisher, and it seems possible. I think a minority of Word users use hyphenation, but a majority of Publisher users use it. KWord will be a tool for writing \"newspaper or something\" as you say, so hyphenation is necessary and important.\n\n> I hardly think hyphenation is more important than, say, footnotes or WYSIWYG printing. Fix those first!\n\nYou are right, there are priorities, everything can't be done at the beginning. I see there is a great work on Kword, things are going better and better and, continuing in this way, it is going to be a great tool !\n\nI am less confident with some other KOffice apps. For example I don't see arrows coming in Kivio, I fear that Krayon will continue to stay as bad as today... Yesterday, I have tried a \"drag and drop\", and then a \"copy-paste\" from Konqueror to Krayon (beta3), both were impossible, these KDE applications don't work together... I wish that the KWord developpment (as the Konqueror one and some others) is an example to follow..."
    author: "Alain"
  - subject: "Re: Looks Great!"
    date: 2001-08-01
    body: "KOffice is not ready for general use yet. No one is saying that any of these featues is unimportant, but the whole point is \"They're getting to it\"\n\nBesides, DnD and C&P operations are both being majorly worked on in QT3, iirc."
    author: "Carbon"
  - subject: "Copy/paste and mouse third button : very bad"
    date: 2001-08-01
    body: "> Besides, DnD and C&P operations are both being majorly worked on in QT3, iirc\n\nI am surprised, I thought it was one if the first things to do...\n\nWorse : there are confusions between the copy/paste use and the use of Select + the mouse third button.\n\nExamples :\n\n- In KMail : I make Ctrl C on \"Bla1\", then I select \"Bla2\". I do Ctrl V and \"Bla2\" is appearing. Bad !!\n- In KWord, idem : \"Bla1\" is appearing. Good.\n\n- In Kmail : I make Ctrl C on \"Bla1\", then I select \"Bla2\". I do third button and \"Bla2\" is appearing. Good.\n- In KWord, idem : \"Bla1\" is appearing. Bad !!\n\nSuch things are VERY VERY important to work quietly. It needs a global policy in all KDE for doing the same things when using Copy/Paste, Select/Third button, Drag&Drop and others reflex that all users need..."
    author: "Alain"
  - subject: "Re: Copy/paste and mouse third button : very bad"
    date: 2001-08-01
    body: "I think you misunderstand. What I mean by \"It's being majorly worked on in QT3\" is that it'll be in the first stable QT3, (at least, I'm pretty sure it'll be). KDE 3 will be exclusivly based on QT3, meaning you'll get a global policy at the QT/X level, including support for copying and pasting imagery, rich text, etc.\n\nAlso, about c&p operations and the third mouse button : those are seperate buffers, by design. The select & middle mouse operation is low-level X stuff, with only support for text."
    author: "Carbon"
  - subject: "Re: Copy/paste and mouse third button : very bad"
    date: 2001-08-01
    body: "> KDE 3 will be exclusivly based on QT3, meaning you'll get a global policy at the QT/X level, including support for copying and pasting imagery, rich text, etc.\n\nVery good. But I did not misunderstood : today it is badly done, there are many incoherences. I am glad it will soon being better (however 6 months is a little long...)\n\n> The select & middle mouse operation is low-level X stuff, with only support for text.\n\nYes, but, as I said in my example about Kmail, there are bad interactions with copy/paste. Are you sure that Qt3 will correct such a thing ? Is'nt it a bug, and have I to post it in  bugs.kde.org ?"
    author: "Alain"
  - subject: "Re: Copy/paste and mouse third button : very bad"
    date: 2001-08-02
    body: "Well, it's not really a bug, imho. The X clipboard, which is the middle mouse thing, is inherently text based, and has quite a few limitations (I believe there is an arbitrary size limit, could someone with more knowledge about XFree check?). The new clipboard in QT3 (as well as the current KDE clipboard) is, iirc, totally seperate from the X buffer. \n\nI suppose though, that many people would like the features of the X clipboard (i.e. instant selection) while still keeping consistency and only one clipboard. Maybe KDE could interrupt the whole process, but I don't know much about it (again, could someone check?)\n\nActually, there was a debate quite a while ago about this on #kde, whether or not to have two clipboards or one."
    author: "Carbon"
  - subject: "Re: Copy/paste and mouse third button : very bad"
    date: 2001-08-02
    body: "Actually X has more than one Clipboard already\nSo Qt3 will be now just using the exiting one based on selection as well as one which will be compatible with windows model of clipboard"
    author: "x_fan"
  - subject: "Re: Copy/paste and mouse third button : very bad"
    date: 2001-08-02
    body: "Er, I may have misread that kmail thing. Idem. means what?\n\nAnd are you sure that when you paste, the paste itself isn't selecting it and adding it to the x select buffer?"
    author: "Carbon"
  - subject: "Re: Copy/paste and mouse third button : very bad"
    date: 2001-08-02
    body: "> And are you sure that when you paste, the paste itself isn't selecting it and adding it to the x select buffer?\n\nI think that pasting don't have to select text and fill the X buffer (as in Netscape, Gedit and usual non KDE programs...)\n\n- In my first test (bad for Kmail), I copy first, then select then paste.\n- In my second test (bad for KWord), I copy first, then select then middle button.\n\nAbout Kmail, it seems there is only one buffer, but it would be two buffers without interactions. So I think it is a bug. But, as any KDE program has not the same  behaviour...\n\nKword uses two not independant buffers. Now I try Konqueror, I see it is like KMail... For me all are bad, there would be two independant buffers for copy/paste and select/middle button (as in Netscape, Gnumeric, Gedit and many other programs). \n\nSo I think it is a bug at the level of whole KDE. I wish it would be updated with KDE 3, perhaps before, if it don't need Qt3..."
    author: "Alain"
  - subject: "Re: Copy/paste and mouse third button : very bad"
    date: 2001-08-03
    body: "Whoever said that the current KDE clipboard is supposed to be seperate from the X clipboard was wrong.  That's a QT 2 thing and can't be changed; there's only one clipboard, and selecting some text overwrites whatever is in the clipboard.  As soon as KDE 3 comes out this will be fixed, because it is already fixed in QT 3.  There will be two clipboards:  One that is replaced whenever you select something and is pasted with the middle mouse button, and another one that is only replaced when you do a Ctrl-C or 'Copy' and is only pasted when you do a Ctrl-V or 'Paste.'  Two independent buffers, just like you want."
    author: "not me"
  - subject: "kdesupport required?"
    date: 2001-07-30
    body: "I thought the kdesupport module was deprecated.  Shouldn't this dependancy now be removed and the individual libs outside of the KDE project be included in the requirements instead?"
    author: "Rob"
  - subject: "How much doesn't work under KDE 2.1.2?"
    date: 2001-07-30
    body: "Hi,\n\nJust compiled under KDE 2.1.2 and RH7.1 and was wondering how many of the problems I see are due to it being compiled under 2.1.2. I knew I'd lose the decent printer support, but what about the following:\n\n1. I can only start applications under koshell, selecting kword from the menu or typing kword in an xterm just exits (same for all other components). I had this problem with previous betas of Koffice 2.1 as well.\n\n2. I can't import any thing, including an old kspread file from koffice 1.0. HTML, abiword, Excel, etc. imports all do nothing for me.\n\n3. Inserting formulas or using kformula doesn't work properly. If I type a long line of text I can only see the bottom few points as if the window over the text is positioned too low.\n\nCheers."
    author: "Dr_LHA"
  - subject: "Re: How much doesn't work under KDE 2.1.2?"
    date: 2001-07-30
    body: "I just noticed that I'm completely unable to save in any format as well! Is this something to do with mimetypes not being setup/recognised properly?"
    author: "Dr_LHA"
  - subject: "Re: How much doesn't work under KDE 2.1.2?"
    date: 2001-07-31
    body: "I had the same problems when I was using koffice beta3 compiled from source on rh 7.1. \nThe solution I found now was to install the mandrake rpms for kde 2.1.2, they work exelent here on my redhat box....\nSeems like once Redhat dosen't care about rpms anymore that I'll be forced to use mdk ones :)"
    author: "Protoman"
  - subject: "Re: How much doesn't work under KDE 2.1.2?"
    date: 2001-07-31
    body: "Here's what I did:\n\nI managed to fix it by installing bero's daily koffice build with --nodeps, it depends on CVS KDE 2.2, which I don't want to install until it's released (production system). I then went to my koffice build and did \"make install\", which copied over the RPM, so I could actually run the binaries without getting library failures.\n\nEverything now works.\n\nBizzare way of doing things I know :-)"
    author: "Dr_LHA"
  - subject: "new KChart ?"
    date: 2001-07-31
    body: "Who are they ? - \"Klar\u00e4lvdalens Datakonsult AB (which contributed the new KChart to KOffice)\". What is the license ? I didn't noticed kchart on the list of new features. Any screenshots ?"
    author: "Krame"
  - subject: "Re: new KChart ?"
    date: 2001-07-31
    body: "It's the KDE-veteran Kalle Dalheimer's company.\nDon't worry."
    author: "reihal"
  - subject: "Staroffice is now open source and does hypens [1]"
    date: 2001-07-31
    body: "... so steal the code from there why doncha ;-) ??\n\n\n\n---\n[1] They also do footnotes!"
    author: "Speak to me softly"
  - subject: "Re: Staroffice is now open source and does hyphens"
    date: 2001-07-31
    body: "And spellchecking!!\n\ns/hypens/hyphens/g"
    author: "Speak to me softly"
  - subject: "Re: Staroffice is now open source and does hyphens"
    date: 2001-08-01
    body: "Actually, OpenOffice doesn't have SpellChecker, its third party :("
    author: "Rajan Rishyakaran"
  - subject: "Hyphenation"
    date: 2001-07-31
    body: "I think it should be implemented at the level of Qt's rich text widget for Qt 3.0 ... I mean if they can do bi-directional text what's so hard about hyphenation? ;-)\n\nBut yeah likley TeX or LaTeX are the places to look. AFAIK Abiword has none of this either."
    author: "Spelling Guy"
  - subject: "Its open office, and there is Klyx"
    date: 2001-08-01
    body: "staroffice is not open source, open office is, that is until staroffice 6 comes out, which would have a propeitry front end, because openoffice is lgpl. plus, code base is very different.\nAlso, heard of Klyx, Lyx version for KDE? Its a latex word prossecor."
    author: "Rajan Rishyakaran"
  - subject: "What about..."
    date: 2001-07-31
    body: "What happened to Krayon, anyways?\nWho started this KPlato thing that's mentioned loosely on the KOffice site, and when do they think it will become usuable and in KO? (Sounds very cool, by the way)\nWere there absolutely no changes to Kugar and Kivio between beta3 and rc1?"
    author: "Carbon"
  - subject: "Re: What about..."
    date: 2001-07-31
    body: ">> Who started this KPlato thing that's mentioned loosely on the KOffice site\n\n    Curious, I looked it up.  Anybody who wants a bit more info, read:\n\nhttp://www.koffice.org/kplato/\n\n    and from the linked docs:\n\nhttp://www.koffice.org/kplato/docs/\n\n    \"The purpose of the K PLAnning TOol (KPlato) project is to develop a Project Managment tool for Linux that will allow people to effectively plan and schedule projects in many different fields, including software development, manufacturing processes, and construction.\"\n\n    Interesting.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: What about..."
    date: 2001-07-31
    body: "Kool! I need one of those! *sigh*"
    author: "reihal"
  - subject: "This is terrific"
    date: 2001-07-31
    body: "I'd like to thank absolutely everyone involved with this release.  I don't seem to need StarOffice for much anymore and kword and Kontour are extremely nice to use.\n\nThanks to all.\n\n\nRegards"
    author: "Mark Grant"
  - subject: "Is this really correct?"
    date: 2001-07-31
    body: "<i>...find any remaining bugs before we are stuck with them until the 1.2 release </i>\n\nThis does not sound like a successful road to a stable product. It seems KOffice will remain in constant development, where bugs are common and not fixed until new ones are created together with new features. I hope I got it wrong though.... What about fixing bugs until it is stable, and then start developing 1.2?"
    author: "Claes"
  - subject: "Re: Is this really correct?"
    date: 2001-07-31
    body: "> This does not sound like a successful road to a stable product. \n\nYou're new to software development, aren't you?  That's what beta testing *IS*... finding any bugs that the developers missed.  The idea is that the actual release has no bugs, but that can only happen if all bugs are reported.  This is just a friendly reminder that, if you find a bug, report it.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Is this really correct?"
    date: 2001-07-31
    body: "No I'm not. I have used Linux since 1996. And - I think Linux has a better development model with the stable and development versions. I guess you are the new one. Ever heard the phrase \"it will be released when it is ready\"?"
    author: "Claes"
  - subject: "Re: Is this really correct?"
    date: 2001-08-01
    body: ">> No I'm not. I have used Linux since 1996. And - I think Linux has a better development model with the stable and development versions. \n\n    And I've been using Unix since 1979, and the guy I had lunch with a few hours ago was using punch cards in the 1960s.  It doesn't matter.  The only reason I mentioned it was because you're objecting to the statement (paraphrased): \"We'd better find all the bugs before the release so we can fix them\".\n\n    What is your alternative?  Not search for bugs, or not fix them?  Linux has the exact same development cycle - Add features (in major.odd), debug them, release a \"stable\" version (major.even), and then say \"oops! we missed this bug\" and release fix versions (major.even.revision).  As someone who runs many SMP machines, I can tell you that the 2.4.x series is in no way bug free yet.  Heck, on my home machine, I have to append=\"\" a few paramaters to fix some bad assumptions made about power management.\n\n    Think of the b and rc series in KDE and KOffice as the dot odd versions of linux.  If anything, KDE uses a versioning system that is much more in line with industry conventions - and it works the same (functionally) as linux releases.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Try it before speaking!"
    date: 2001-08-01
    body: ">This does not sound like a successful road to a stable product.\n\nMany bugs reports are in fact feature not yet implemented.\n\nBefore speaking like this, try kword!\n\nregards."
    author: "thierry"
  - subject: "Glad KDE user"
    date: 2001-07-31
    body: "I would like to thank Thomas Zander, Shaheed Haque, David Faure, Laurent Montel, Igor Janssen \nWerner Trobin, John Califf, Andrea Rizzi, Ulrich Kuettler, Stephan Kulow for making Koffice, especially Kword a usable office alternative.  I have been using it for my office suite, printing letters, envelopes, making budgets, preparing presentations, since 1.1beta.  Thank you from the bottom of my heart."
    author: "Jason Spisak"
  - subject: "Re: Glad KDE user"
    date: 2001-08-02
    body: "I have to second that!\n\n I'm a brand new Linux user. I have been running Mandrake 7.2 and KDE as a trial for about three months (Actually bought some Mandrake disks last fall but didn't try it much until recently. I may also try SuSe and Debian soon too.). KDE is nice, and I find the KOffice suite plenty full featured and stable enough for my needs. And I work from home and use the computer extensively for my business. \n\nI'm amazed that this great software is available and I can now make the switch without paying exhorbitant Microsoft upgrade fees. I have no problem paying for good software, but it's Microsoft's tactics and monopoly that have sent me to Linux. This \"smart links\" feature they are talking about with the browser in the XP system has made up my mind. I have a few more things to try tonight and then I suspect I am going cold turkey and eliminating NT from my system.\n\nThanks for a beautiful set of KOffice apps! Hopefully I can contribute in the future. If not programming, maybe with docs.\n\nThanks again!"
    author: "John Marttila"
  - subject: "Krayon?"
    date: 2001-08-01
    body: "Is Krayon being maintained?\nIf so, how many people are working on it?\nJust one?\n\nThere are no changes listed since the last release\n(.beta3)."
    author: "Rk"
  - subject: "Re: Krayon?"
    date: 2001-08-01
    body: "Yes, it's unmaintained. Do you want to join the development and eventually become the maintainer?"
    author: "Lukas Tinkl"
  - subject: "Re: Krayon?"
    date: 2001-08-01
    body: "> Yes, it's unmaintained. \n\nWow, there were 5 mainteners (see \"about\"...) , now 0 ! It seems there is a very big problem...\n\n> Do you want to join the development and eventually become the maintainer?\n\nI hope that Rk is a developper...\n\nIs it really necessary to put Krayon in KOffice 1.1 ? It can't open a .jpg or .png... It has many defaults... \n\nIn page 1 of www.koffice.org, it is said that Krayon is \"a pixel-based image manipulation program like The GIMP or Adobe\u00a9 Photoshop\". Today, it is ridiculous !\n\nIt is bad for KOffice to have such a bloated app. I think it would be better to remove it from 1.1, and wait some developpers ...\n\nWhat a pity, because a such app would be very useful..."
    author: "Alain"
  - subject: "Re: Krayon?"
    date: 2001-08-02
    body: "Alas, I am not a developer, I merely wanted to know whether it was being maintained or not."
    author: "Rk"
  - subject: "Re: Krayon?"
    date: 2001-08-03
    body: "Actually, it can open .jpg and .png files, it just has a bad user interface.  You can't use the File->Open dialog, there's a separate menu option \"Import\" to import non-Krayon files."
    author: "not me"
  - subject: "Re: Krayon?"
    date: 2001-08-03
    body: "Wow, yes, in the middle of the menus, Image-import... It is the only way to open a jpg (no drag & drop, no copy/paste...)\n\nAnd to save you have only to do Image-export, without any button or default  key shortcut...\n\nBad user interface, as you say...\n\nNow, I search to resize and rotate the image, I don't find in the menus...  (strange, 5 menus are empty...)\n\nAnd I have to be very carefull when exiting Krayon, there is no message when you have forhotten to save the file...\n\nP.-S. : I succeed in resizing the image. I had to understand that zooming means resizing..."
    author: "Alain"
  - subject: "KOffice for Mac OS X and Windows"
    date: 2001-08-01
    body: "Isn't QT available for Mac OS X and Windows? Why not we make a version for it? Just a few simple UI change would do, and interegration with Windows Explorer (for Windows) and Finder (for Mac OS X) instead of Konqueror, etc. Isn't that better to get more users? The more users, the developers, the more developers, the more features. It would only help KOffice. Plus Krayon would be the first Photoshop-like app on Mac OS X."
    author: "Rajan Rishyakaran"
  - subject: "Re: KOffice for Mac OS X and Windows"
    date: 2001-08-02
    body: "isn't gimp available on MacOSX ?"
    author: "KDE User"
  - subject: "Re: KOffice for Mac OS X and Windows"
    date: 2003-09-20
    body: "only under x11"
    author: "Lesly"
  - subject: "not so good"
    date: 2001-08-02
    body: "Excuse my english..\nI have just  installed the Koffice RC1 on my suse7.2 box and I'am quite disapointed with this release.\nIt has many bugs with the language used (french) and often crashed (when trying to include a Kchart doc in kword for example).\nI hope there will be some improvement before the final release.\nDoes anyone have such troubles or is it my installation ?\nThank you for your nice work..."
    author: "KDE User"
  - subject: "Red Hat RPMS"
    date: 2001-08-02
    body: "According to the release note, rpms for Red Hat should be available on July 31.\n\nToday is Aug 02.\n\nWho is in charge of the rpms for Red Hat?\nYes, yes I know it isn't the KDE team...\n\nRed Hat is one of the largest distributions if not THE largest. If the koffice team want people to test it then release rpms for Red Hat. Since a *lot* of users use Red Hat.\n\nIf there already is someone preparing or will soon prepare the rpms but hasn't had the time, then I apologize.\n\nIf there isn't any maintainers then the KDE team should *actively* go out and try to find someone. Not put a notice on the note-about-binary-maintainers, which few people read.\n\nI unfortunately already maintain 3 programs for Redhat RPMS, and work 70 hours a week....\n\nI don't want to sound ungrateful, but it's better just to write in the release notes that \"No Red Hat packages are going to be released by anyone as far as we know.\"\n\nOther than that. The KDE and Koffice teams are doing a GREAT job!\n\n/Richard"
    author: "Richard"
---
KOffice RC1 has descended onto the well-known KDE ftp servers.  Your best bet is to use it with KDE 2.2beta1 and Qt 2.3.1, but KDE 2.1.1 and Qt 2.2.4 will work (though not as well).  The next release, scheduled for mid-August, will be 1.1 final, so this is your last chance to give KOffice a whirl and help find any remaining bugs before we are stuck with them until the 1.2 release &lt;grin&gt;.  <A HREF="http://www.suse.com/">SuSE</A> gets the binary-build over-achievement award.  You can read the press release, complete with an incremental ChangeLog and other interesting stuff, if you <EM>Read More</EM>.

<!--break-->
<P>&nbsp;</P>
<P>DATELINE JULY 30, 2001</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">KOffice Suite Release Candidate Available for Linux/Unix</H3><P><STRONG>KDE, the Leading Linux Desktop, Ships First Release Candidate of KOffice Suite</STRONG></P>
<P>July 30, 2001 (The INTERNET).
The <A HREF="http://www.kde.org/">KDE
Project</A> today announced the release of
<A HREF="http://www.koffice.org/">KOffice</A> 1.1 Release Candidate 1 (RC1).
KOffice is an integrated office suite for KDE which utilizes open
standards for component communication and component embedding.
The primary goals of this release are to provide a preview of
KOffice&nbsp;1.1 and to involve users and developers who wish to
request/implement missing features or identify problems.
Code development is currently focused on stabilizing KOffice 1.1,
<A HREF="http://developer.kde.org/development-versions/koffice-1.1-release-plan.html">scheduled</A>
for final release in mid-August, 2001.
</P>
<P>
Although a release candidate, KOffice 1.1RC1 constitutes the most stable and
feature-complete release of KOffice to date.  The KOffice team encourages
all users of earlier KOffice releases to upgrade to KOffice 1.1RC1.
In particular, this release is substantially more stable and
feature-complete than KOffice 1.0, which was released together with KDE 2.0
in October 2000.  Changes to individual KOffice components since the last
beta release are <A HREF="#changelog">enumerated below</A>.  In addition
<A HREF="http://www.koffice.org/releases/1.1rc1-release.phtml">Release
Notes</A>, a
<A HREF="http://www.koffice.org/announcements/changelog-1.1.phtml">list of
changes</A> since the KOffice 1.0 release, and a
<A HREF="http://www.koffice.org/faq/">KOffice FAQ</A>, are
available at the <A HREF="http://www.koffice.org/">KOffice website</A>.
</P>
<P>
This release includes the following components:
<A HREF="http://www.koffice.org/kword/">KWord</A>
(a frame-based, full-featured word processor);
<A HREF="http://www.koffice.org/kpresenter/">KPresenter</A>
(a presentation application);
<A HREF="http://www.koffice.org/kspread/">KSpread</A>
(a spreadsheet application);
<A HREF="http://www.koffice.org/kontour/">Kontour</A>
(a vector-drawing application f/k/a KIllustrator);
<A HREF="http://www.thekompany.com/projects/kivio/">Kivio</A>
(a flowchart application);
<A HREF="http://www.koffice.org/kchart/">KChart</A>
(a chart drawing application);
<A HREF="http://www.koffice.org/kformula/">KFormula</A>
(a formula editor);
<A HREF="http://www.thekompany.com/projects/kugar/">Kugar</A>
(a tool for generating business quality reports); and
<A HREF="http://www.koffice.org/filters/">filters</A>
(for importing documents created by, or exporting documents for use with,
other office suites or office programs).
</P>
<P>
The most significant improvements since KOffice 1.1beta3, the last
KOffice beta release which was announced last month, occurred in
KWord.  These improvements
include: support for Asian languages with
<A HREF="http://www.xfree86.org/current/XOpenIM.3.html">XIM</A>;
drop'n'paste of images; and
copy/paste of tables.  KWord also benefited from numerous bug-fixes,
particularly in the auto-sizing and auto-scrolling features.
For a further list of improvements to KOffice since the last beta release,
please refer to the <A HREF="#changelog">incremental changelog</A> below.
</P>
<P>
KOffice and all its components are available for free under
Open Source licenses from the KDE
<A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/">server</A>
and its <A HREF="http://www.kde.org/mirrors.html">mirrors</A>.
As a result of the dedicated efforts of hundreds of translators,
KOffice 1.1RC1 is available in 27 languages</A>.
</P>
<P>
<A NAME="changelog"></A><H4>Incremental Changelog</H4>
</P>
<P>
The following are the major improvements, enhancements and fixes since the
KOffice-1.1beta3 release last month:
<UL>
<LI><EM><STRONG>All suite applications (KOffice libs)</STRONG></EM></LI>
<UL>
<LI><EM>New Features and Improvements</EM>:</LI>
<UL>
<LI>added a submenu for selecting a type of embedded document to insert;</LI>
<LI>improved useability of template creation dialog;</LI>
<LI>improved template names to permit special characters, like '(';</LI>
<LI>improved the select-color actions with color palette;</LI>
<LI>improved the WMF-loader to parse the DPI of WMF files; and</LI>
<LI>replaced the filters' embedded dialog into the file chooser with normal dialogs; and</LI>
</UL>
<LI><EM>Bug Fixes</EM>:</LI>
<UL>
<LI>fixed the automatic naming of DCOP interfaces;</LI>
<LI>fixed the recent files list (some ways of opening documents didn't add to that list);</LI>
<LI>fixed the autosave feature to only save if the document was changed since the last autosave;</LI>
<LI>fixed "Configure keys" to list all the available actions; and</LI>
<LI>fixed problems with embedded documents and zooming.</LI>
</UL>
</UL>
<LI><EM><STRONG>KWord</STRONG></EM></LI>
<UL>
<LI><EM>New Features</EM>:</LI>
<UL>
<LI>added XIM support (for Asian languages);</LI>
<LI>added save/restore UI settings (formatting characters, frame borders, zoom);</LI>
<LI>added "keep aspect ratio" option when inserting/resizing images;</LI>
<LI>added support for dropping and pasting images;</LI>
<LI>added frame dialog for headers and footers;</LI>
<LI>enabled changing the charset for selected characters (helps non-AA displays
and when printing);</LI>
<LI>enabled disabling the grid by pressing shift while moving/resizing a frame;
and</LI>
<LI>implemented copy/paste of tables;</LI>
</UL>
<LI><EM>Improvements</EM>:</LI>
<UL>
<LI>improved paragraph borders to span the entire frame/page width;</LI>
<LI>improved auto-resizing when inserting very large images;</LI>
<LI>improved auto-scrolling (also applies when moving a frame, etc.);</LI>
<LI>improved cell auto-sizing in tables;</LI>
<LI>improved preview in style dialog;</LI>
<LI>improved calculation of floating tables size and placement;</LI>
<LI>optimized speed when typing;</LI>
<LI>made the "insert special character" dialog non-modal;</LI>
<LI>increased the number of settings saved to/loaded from the KPresenter configuration file;</LI>
<LI>revised class design for future code sharing with other KOffice applications; and</LI>
<LI>made coordinates relative to page corner in frame dialog; and</LI>
</UL>
<LI><EM>Bug fixes</EM>:</LI>
<UL>
<LI>fixed text-flow bugs;</LI>
<LI>fixed frame selection in case of overlapping frames;</LI>
<LI>fixed many undo/redo bugs (especially for tables) and grouping of commands;</LI>
<LI>fixed inline items in headers/footers/copied frames;</LI>
<LI>fixed copying frames (with inline frames);</LI>
<LI>fixed table of content (regeneration and page numbers);</LI>
<LI>fixed typographical quotes (never triggered in beta3);</LI>
<LI>fixed undo/redo when applying a new format to a variable, and when pasting text with inline items;</LI>
<LI>fixed loading of multi-columns documents, and loading of cliparts;</LI>
<LI>fixed saving of copied frames; and</LI>
<LI>fixed KPart objects, zooming, and preview mode.</LI>
</UL>
</UL>
<LI><EM><STRONG>KPresenter</STRONG></EM></LI>
<UL>
<LI>increased the number of settings saved to/loaded from the KPresenter configuration file;</LI>
<LI>reduced size of file saved;</LI>
<LI>switched to menu XML for all actions;</LI>
<LI>fixed web presentation (author, email information); and</LI>
<LI>fixed icon names.</LI>
</UL>
<LI><EM><STRONG>KSpread</STRONG></EM></LI>
<UL>
<LI>fixed autofill bugs;</LI>
<LI>fixed crash when displaying an area (table name was not translated);</LI>
<LI>fixed GUI problems with the dialogbox;</LI>
<LI>closed some memory leaks (formula dialogbox and consolidate dialogbox were not deleted);</LI>
<LI>fixed "list sort" to not delete predefined lists (month, day);</LI>
<LI>fixed tags in header/footer (<TT>&lt;name&gt; &lt;file&gt; &lt;author&gt;</TT>); and</LI>
<LI>fixed print setup.</LI>
</UL>
<LI><EM><STRONG>Kontour</STRONG></EM></LI>
<UL>
<LI>renamed application from KIllustrator to Kontour;</LI>
<LI>added ability to delete objects with the backspace key;</LI>
<LI>added ability to use the delete key in a text object;</LI>
<LI>switched to KPrinter for printing (provides users with much greater control
over layout/formatting of printed pages);</LI>
<LI>fixed helpline (the helpline is not moved and the cursor is not changed when the helplines are hidden);</LI>
<LI>fixed XIM positioning;</LI>
<LI>fixed undo/redo/cut actions;</LI>
<LI>fixed undo/redo change fill parameters;</LI>
<LI>fixed GUI to ensure tool is always checked;</LI>
<LI>fixed layerPanel (enable/disable button and other fixes); and</LI>
<LI>fixed crash when deleting a table.</LI>
</UL>
</UL>
</P>
<P>
For a list of major changes since the KOffice 1.0 release last October, please
visit the
<A HREF="http://www.koffice.org/announcements/changelog-1.1.phtml">KOffice
website</A>.
</P>
<P>
<H4>Downloading and Compiling KOffice</H4>
</P>
<P>
<A NAME="Source_Code"></A><EM><STRONG>Source Packages</STRONG></EM>.
The source packages for KOffice 1.1RC1 are available for free download at
<A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/src/">http://ftp.kde.org/unstable/koffice-1.1-rc1/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>.
</P>
<P>
<A NAME="Source_Code-Library_Requirements"></A><EM><STRONG>Library
Requirements</STRONG></EM>.
KOffice requires recent versions of Qt and kdelibs, and even more recent
versions are recommended.
</P>
<UL>
<LI><EM>Required</EM>:  KOffice 1.1rc1 requires
<A HREF="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.4.tar.gz">qt-x11-2.2.4</A>or greater, kdesupport-2.1.x or greater, and kdelibs-2.1.2 or greater.  The
KDE packages are available from the KDE ftp servers at
<A HREF="http://ftp.kde.org/stable/2.1.1/distribution/">http://ftp.kde.org/stable/2.1.1/distribution/</A>
and <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/">http://ftp.kde.org/stable/2.1.2/distribution/</A>.
Please note, however, that kdelibs-2.1.1 will cause some crashes and
anti-aliased font problems.  Moreover, both kdelibs-2.1.1 and kdelibs-2.1.2
will not properly accept .doc documents in the file selection dialog (see the
<A HREF="http://www.koffice.org/releases/1.1rc1-release.phtml">release
notes</A> for a workaround).
<BR>&nbsp;<BR>For more information on these kdelibs
releases, please see the
<A HREF="http://www.kde.org/announcements/announce-2.1.1.html">KDE 2.1.1
press release</A> and the
<A HREF="http://www.kde.org/announcements/announce-2.1.2.html">KDE 2.1.2
press release</A>.
</LI>
<BR>&nbsp;<BR>
<LI><EM>Recommended</EM>:  The KOffice team recommends the use of
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.1.tar.gz">qt-x11-2.3.1</A> and kdelibs-2.2beta.  Kdelibs-2.2beta (and the pending kdelibs-2.2RC1)
provide a few additional features, such as improved printing support
(kdeprint), a scanner plugin, and proper recognition of
.doc files in the file selector dialog.
KDE 2.2beta1 is available from
<A HREF="http://www.kde.org/unstable/2.2beta1/">http://www.kde.org/unstable/2.2beta1/</A>
and KDE 2.2RC1 should be available shortly from
<A HREF="http://www.kde.org/unstable/2.2rc1/">http://www.kde.org/unstable/2.2rc1/</A>.
Please note that the kdesupport package has been discontinued for KDE 2.2.x
due to the fact that most distributions already provide the libraries
and programs which were included in that package.  If you still require
this package, you may use one of the
<A HREF="http://ftp.sourceforge.net/pub/mirrors/kde/snapshots/current/">snapshots</A>.
<BR>&nbsp;<BR>
For more information on the kdelibs-2.2beta1 release, please see the
<A HREF="http://www.kde.org/announcements/announce-2.2-beta1.html">release
announcement</A>.
</LI>
</UL>
KOffice 1.1rc1 will not work with versions of Qt older than 2.2.4 or
versions of kdelibs older than 2.1.1.
</P>
<P>
<EM><STRONG>Compiler Requirements</STRONG></EM>.
Please note that some components of
KOffice 1.1RC1 (such as the Quattro Pro<SUP>&reg;</SUP> import filter
and the new <A HREF="http://www.koffice.org/kchart/">KChart</A>) will not
compile with older versions of <A HREF="http://gcc.gnu.org/">gcc/egcs</A>,
such as egcs-1.1.2 or gcc-2.7.2.  At a minimum gcc-2.95-* is required.
</P>
<P>
<EM><STRONG>Further Instructions</STRONG></EM>.
For further instructions on compiling and installing KOffice, please consult
the <A HREF="http://www.koffice.org/install-source.phtml">installation
instructions</A>.
</P>
<P>
<H4>Installing Binary Packages</H4>
</P>
<P>
<EM><STRONG>Binary Packages</STRONG></EM>.
Some distributors choose to provide binary packages of KOffice for certain
versions of their distribution.  Some of these binary packages for
KOffice 1.1RC1 will be available for free download under
<A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/">http://ftp.kde.org/unstable/koffice-1.1-rc1/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution (if you cannot find a binary package for your distribution,
please read the <A HREF="http://dot.kde.org/986933826/">KDE Binary Package
Policy</A>).
<P>
<EM><STRONG>Library Requirements</STRONG></EM>.
The library requirements for a particular binary package varies with the
system on which the package was compiled.  Please bear in mind that
some binary packages may require a newer version of Qt and/or KDE
than was distributed with the distribution version for which the binary
package is listed below.  For general library requirements for KOffice,
please see the text at <A HREF="#Source_Code-Library_Requirements">Source Code -Library Requirements</A>.
</P>
<P>
<EM><STRONG>Package Locations</STRONG></EM>.
At the time of this release, pre-compiled packages are available for:
</P>
<P>
<UL>
<LI><A HREF="http://www.linux-mandrake.com/en/">Linux Mandrake</A> (<A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/Mandrake/README">README</A>):
<UL>
<LI>Cooker: <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/Mandrake/cooker/i586/">i586</A> and <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/Mandrake/cooker/ia64/">ia64</A></LI>
<LI>8.0:  for kdelibs 2.1.2, <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/Mandrake/8.0/i586/kde-2.1.2/">i586</A> and <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/Mandrake/8.0/ppc/">PPC</A>; for kdelibs 2.2beta1, <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/Mandrake/8.0/i586/kde-2.2beta1/">i586</A></LI>
</UL>
<LI><A HREF="http://www.suse.com/">SuSE Linux</A> (<A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/README">README</A> - requires Qt-2.3.x):
<UL>
<LI>kdelibs 2.2beta1</LI>
<UL>
<LI>7.2:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/KDE-2.2/i386/7.2/">i386</A>; please see the <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/KDE-2.2/noarch/">noarch</A> directory for common files</LI>
<LI>7.1:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/KDE-2.2/i386/7.1/">i386</A>, <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/KDE-2.2/ppc/7.1/">PPC</A> and <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/KDE-2.2/axp/7.1/">Sparc</A>; please see the <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/KDE-2.2/noarch/">noarch</A> directory for common files</LI>
<LI>7.0:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/KDE-2.2/i386/7.0/">i386</A>, <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/KDE-2.2/ppc/7.0/">PPC</A> and <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/KDE-2.2/s390/7.0/">S390</A>; please see the <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/KDE-2.2/noarch/">noarch</A> directory for common files</LI>
<LI>6.4:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/KDE-2.2/i386/6.4/">i386</A>; please see the <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/KDE-2.2/noarch/">noarch</A> directory for common files</LI>
</UL>
<LI>kdelibs 2.1.2 and kdesupport 2.1.1</LI>
<UL>
<LI>7.2:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/i386/7.2/">i386</A> and <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/ia64/7.2/">IA64</A>; please see the <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/noarch/">noarch</A> directory for common files</LI>
<LI>7.1:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/i386/7.1/">i386</A>, <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/ppc/7.1/">PPC</A>, <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/axp/7.1/">Alpha</A> and <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/sparc/7.1/">Sparc</A>; please see the <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/noarch/">noarch</A> directory for common files</LI>
<LI>7.0:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/i386/7.0/">i386</A>, <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/ppc/7.0/">PPC</A> and <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/s390/7.0/">S390</A>; please see the <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/noarch/">noarch</A> directory for common files</LI>
<LI>6.4:  <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/i386/6.4/">i386</A>; please see the <A HREF="http://ftp.kde.org/unstable/koffice-1.1-rc1/SuSE/noarch/">noarch</A> directory for common files</LI>
</UL>
</UL>
</UL>
</P>
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages may become available over the
coming days and weeks; in particular,
<A HREF="http://www.redhat.com/">RedHat</A> packages are expected tomorrow and
<A HREF="http://www.debian.org/">Debian</A> packages are expected late
this or early next week.
</P>
<P>
<H4>About KOffice/KDE</H4>
</P>
<P>
KOffice is part of the KDE project.
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environmentemploying a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.
</P>
<P>
For more information about KDE, please visit KDE's
<A HREF="http://www.kde.org/whatiskde/">web site</A>.
For more information about KOffice, please visit KOffice's
<A HREF="http://www.koffice.org/info/">web site</A> where you can find,
among other things, information on
<A HREF="http://www.koffice.org/getinvolved/">contributing to KOffice</A>.
</P>
<P>
<H4>Corporate KOffice Sponsors</H4>
</P>
<P>
Besides the valuable and excellent efforts by the
<A HREF="http://www.koffice.org/developers.phtml">KOffice developers</A>
themselves, significant support for KOffice development has been provided by
<A HREF="http://www.mandrakesoft.com/">MandrakeSoft</A> (which sponsors
KOffice developers <A HREF="http://perso.mandrakesoft.com/~david/">David
Faure</A> and Laurent Montel),
<A HREF="http://www.thekompany.com/">theKompany.com</A> (which
contributed Kivio and Kugar to KOffice),
and
<A HREF="http://www.klaralvdalens-datakonsult.se/">Klar&auml;lvdalens
Datakonsult AB</A> (which contributed the new KChart to KOffice).  Thanks!
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
KDE, K Desktop Environment, KChart, KFormula, Kontour, KOffice,
KPresenter, Krayon, KSpread and KWord are trademarks of KDE e.V.
Kivio and Kugar are trademarks of thekompany.com.
<!--Adobe Illustrator is a registered trademark of Adobe Systems
Incorporated.-->
Quattro Pro is a registered trademark of Corel Corporation or Corel Corporation
Limited.
<!--MS WinWord 97 and MS Excel are registered trademarks of Microsoft
Corporation.-->
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
granroth@kde.org<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
pour@kde.org<BR>
(1) 917 312 3122
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
faure@kde.org<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
konold@kde.org<BR>
(49) 179 2252249
</TD></TR>
</TABLE>
