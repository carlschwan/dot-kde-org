---
title: "KDE 2.2beta1 and KOffice 1.1beta3 now frozen"
date:    2001-06-13
authors:
  - "numanee"
slug:    kde-22beta1-and-koffice-11beta3-now-frozen
comments:
  - subject: "Re: KDE 2.2beta1 and KOffice 1.1beta3 now frozen"
    date: 2001-06-13
    body: "This is good news as there are some bugs in the cvs that should be fixed before the release.. Even if it's only a beta-release..\n\nPpl.. look forward to the new sidebarTNG in konq ;)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "increase the synergy!"
    date: 2001-06-13
    body: "Wise decision!\ngo for stabilty more then for speed. \n\nYou should plan for a \"reduce the code\" or \"increase synergy\" release where no additional functionality is added but the code-base of the whole project is reduced by looking for possible reuse of the already developed components through all the KDE projects.\n\nIt would be like going a bit on diet as a preparation for the next match (with KDE 3.0)aginst the other desktops.\n\nKeep up the magnificent work!\n\nAndrea"
    author: "Andrea"
  - subject: "Re: increase the synergy!"
    date: 2001-06-13
    body: "besides the obvious \"can't make volunteers do what they don't want to\", code reduction and refactoring happen continuously during KDE development. just in the last few weeks several pieces of code that were found to be used often (the rule of thumb is \"used in 3 different applications\") have been moved to libraries for greater, as you put it, synergy. =)\n\ni've also noticed that in probably the last 2-3 weeks there have been noticeable speed-ups due to various optimizations in the code base across the entire desktop."
    author: "Aaron J. Seigo"
  - subject: "Re: increase the synergy!"
    date: 2001-06-14
    body: "I hope some of the performance improvements were somewhat drastic, because my laptop with it's poor little 4200RPM drive runs pretty poorly.  Konq will take close to twelve seconds to launch, and most other apps are the same.  I think this could be improved, even on a device with such a slow drive.  Once loaded, apps are fairly responsive (except when they end up getting stuck in the swap space, although that should happen a little less often since I upgraded the RAM from 64MB to 192MB), it's just the initial loading and shared object file loading that takes the time.  If KDE 2.2 ends up loading apps faster than 2.1 does on my laptop, I'll be satisfied."
    author: "Chad Kitching"
  - subject: "Re: increase the synergy!"
    date: 2001-06-13
    body: "I totally agree with you. If this was done for every main feature of KDE, I am sure that thousands of lines of code would disappear. Take tab completion as an example, it should be available across every application just as anti-aliasing is. Having more than one address book is unnecessary, etc. \n\nTake the BeOS for instance, they have the idea of translators. A programmer writes a jpg translator, bmp etc. Therefore, when another programmer writes an image viewer program, he does not need to know how to view jgp's! I saw a complete image viewer in under 60k as it did not need to know how to deal with image file formats. \n\nNow THAT is synergy.\n\nWhy is this amazing? You could have ONE algorithm that deals with mpegs, that KDE keeps as a translator (or whatever KDE calls it). If a media player, Konq window, KOffice presentation, whatever, needs to call the translation of the mpeg, it could do so without needing to know how to deal with mpegs.\n\nWhy is this even more amazing? You could have one person that writes a better mpeg algorithm, and replaces the current one. In one patch, the entire KDE can view mpegs better.\n\nDuplication of effort is the biggest problem at the moment as you have scores of people working on the same thing, but as part of a different project. A classic example is email programs. I can't even count how many email programs there are available online, all doing the same thing. The small crap email programs are striving to be what other email programs already are. \n\nAnd that is my 2c worth :)"
    author: "KDEJunkie"
  - subject: "Re: increase the synergy!"
    date: 2001-06-13
    body: "I am eally no coder, but I think this is exactly how it works in KDE. Everything else would be just plain stupid. I think there is this functionality built-in in Qt.\n\nSee, Konq for example can only view gifs when you compiled gif-support into qt. I think that's a strong hint that KDE-Applications use the Qt-library for all their images.\n\nIt's exactly the same thing with the io-slaves. The ftp-ioslave, for example. Konq surely uses it, and separate ftp-programs can use it, too.\n\nLooks like KDE is already ahead of your wishes ;)"
    author: "me"
  - subject: "Re: increase the synergy!"
    date: 2001-06-13
    body: "hehe.. This is exactly the way KDE works.. Technologies like kpart and similar makes this possible, and writing a simple image-viewer in KDE can be done in about 1k!\n\nFor me, (except for everything else) this is what sets KDE apart from all other Desktop Environments out there.."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: increase the synergy!"
    date: 2001-06-13
    body: "Tab completion, you say? Take a look at the KDE library reference page on KCompletion :). In my program it took about 30 lines of code to completely implement tab completion (including storing between sessions and avoiding duplication, and I have a number of different fields which can be completed).\n\nAddress book, you say? Well - perhaps this is why there is KAB, the KDE Address Book, which can be used in an application just by including the right libraries.\n\nImage file formats, you say? Take a look at the KImgIO library.\n\nA better MPEG algo, you say? Hmm... take a look at ARTS/MCOP plugins. :)\n\nSo basically everything you mentioned has already been implemented in KDE. Perhaps in the future you should do a little research and not just assume that the coders are dumb. ;)"
    author: "David G. Watson"
  - subject: "Re: increase the synergy!"
    date: 2001-06-14
    body: "> In my program it took about 30 lines of code to completely implement tab completion (including storing between sessions and avoiding duplication, and I have a number of different fields which can be completed).\n\nJust one comment: you usually don't need to take care of duplicates in a KCompletion (or derived) object. This is due to the way the strings are stored (as a tree of characters -- there can't be any dupes).\n\nSo you can call addItem( \"foo\" ) as often as you want, it will only be stored once. If you use 'weighted' completion however, then every addItem( \"foo\" ) call will increase the weight of this item. An item with a greater weight will be preferred over one with less weight, during completion."
    author: "Carsten Pfeiffer"
  - subject: "Re: increase the synergy!"
    date: 2001-06-14
    body: "Well, I'm not actually storing the strings in the KCompletion object - I'm loading them into the dialog each time I create it. There's probably a better way than the one I'm using, but getting it working was more of a priority than doing it the \"correct\" way."
    author: "David G. Watson"
  - subject: "Re: increase the synergy!"
    date: 2004-12-24
    body: "is the new one out yet?"
    author: "zelnick"
  - subject: "Re: increase the synergy!"
    date: 2001-06-14
    body: "I think her you are assuming that I assume.... ;)\n\nAll I wanted was to suggest: intensify your efforts on throwing away things. \nWhat about (for example) having somebody explicitely responsible for finding possible duplications in the code and formulating better solutions?\n\nI am aware that KDE is the right place for this, since, as many of you said, you are already doing this in a more or less coordinated way.\n\nAndrea"
    author: "Andrea"
  - subject: "Re: increase the synergy!"
    date: 2001-06-14
    body: "\"What about (for example) having somebody explicitely responsible for finding possible duplications in the code and formulating better solutions?\"\n\nGood idea, but not very realistic. THERE IS NOONE WHO WANTS TO DO THIS. I really do think this would be a great thing, since that one person would probably be quite effective at hunting down redundancies after doing it for a while, but you really have to understand that its not exactly a very satisfying job to do.\n\nAs far as I understand, developers want to add features. Fixing bugs is noone's hobby, as you can see from the number of commits after a feature-freeze ;)"
    author: "me"
  - subject: "Money solves problems sometimes"
    date: 2001-06-14
    body: "Dedicate a person for this and set up a paypal account for him/her, and support this person with money :)\n\nJust a thought\n\nMats"
    author: "Loranga"
  - subject: "Re: increase the synergy!"
    date: 2001-06-15
    body: "May be a eveloper would not be the right person for such a job. An 'architect' is more appropiate, one that have enough understanding of software and KDE in order to find the best candidates for 'synergize'.\nThe decision about HOW to get a better structure there should be taken by the developers and gurus of the KDE project.\n\nI, for example would be willing to do this job, if I knew the KDE team would need it.\n\nAndrea"
    author: "Andrea"
  - subject: "The Dot functionality"
    date: 2001-06-13
    body: "Navindra\n\nThis is for you :)\n\nWhen we post a message and the confirmation loads that reads: Your reply has been posted, can't we have a link that takes us straight back to the list we were reading with our post included. If you are reading a post with hundreds of entries and you want to post to more than one message, I find that after posting a reply I have to go back to the root of the dot, click on the thread that I was reading and then scroll down to where I was before. If there was a link that could do all those steps in one, it would be great.\n\nThank you."
    author: "kde-user"
  - subject: "Re: The Dot functionality"
    date: 2001-06-13
    body: "I second that.  :)"
    author: "KDE Fan"
  - subject: "Re: The Dot functionality"
    date: 2001-06-13
    body: "Let's see if that link really is missing."
    author: "Inorog"
  - subject: "Re: The Dot functionality"
    date: 2001-06-13
    body: "OK. If you click the OK button you get to see what you posted. Which of the \"parent\" \"thread\" links and the DOT logo aren't obvious enough?"
    author: "Inorog"
  - subject: "Re: The Dot functionality"
    date: 2001-06-14
    body: "testing.."
    author: "Test"
  - subject: "Re: The Dot functionality"
    date: 2001-06-14
    body: "Yes, and pressing the 'back' button on the browser after posting brings one to the confirm page and ends in a double post."
    author: "rit"
  - subject: "Suggestion on how to switch to Qt 3"
    date: 2001-06-13
    body: "Hi,\n\nI assume all can verify that there was a big pain switching from KDE to KDE-2 and Qt-2 in the same time. Porting KDE-2.{3,4} to use Qt is not an option either since it will break BC for existing applications. Therefor I'd suggest that next KDE release after 2.2 is still based on Qt-2. Then a \"transit release\" where BC will be broken as much as possible is released named KDE 2.99. This would be a KDE-2 port to use Qt-3. After this unstable/stable preview has been made the next thing to do would be aiming for KDE-3 with a lot of changes.\n\nBut then, people who don't care about BC issues and still would like a more-stable-than-cvs recent desktop could use it, and people who don't want several BIC releases could wait for KDE-3.\n\nWhat do you all say ?\n\n/Sam"
    author: "Sam"
  - subject: "Re: Suggestion on how to switch to Qt 3"
    date: 2001-06-13
    body: "Actually, if I have understood this correctly, QT3 is quite compatible with QT2 only minor incompatibility. And KDE will continue on the 2.X branch, using QT3. \n\nThe QT3 version of KDE should be released sometime this winter. (KDE 2.X will be \u0093ported\u0094 to QT3). While there will be a separate KDE3 scheduled for release sometime in the future.\n\n\\J\u00f8rgen"
    author: "J\u00f8rgen"
  - subject: "Re: Suggestion on how to switch to Qt 3"
    date: 2001-06-13
    body: "ehhh... Quite compatible !?!. Qt-3 is not even source compatible!\n\nQMultiLineEdit has been deprecated and some other widgets as well. Use Qt-3 for KDE-2.{3,4} will break a lot of applications out there I believe.\n\n/Sam"
    author: "Sam"
  - subject: "Re: Suggestion on how to switch to Qt 3"
    date: 2001-06-13
    body: "Qt1 to Qt2 was a major change.  Qt3 isn't so drastic, and consists mainly just new feature additions.  Most of the other things they got right the second time :)  The application I'm currently working on didn't even require any changes to compile under Qt3.  That may be different for KDE, but I'm sure the changes will be minor.\n\nKDE2 took so long to come out that at the time, distributions would include both Qt1 (for KDE1) and Qt2 (for everything else, like Licq).  This wasn't a very desirable setup.  I think in this case, the changes are minor enough that it would be worth the effort.\n\nAnd I'm looking forward to being able to use the new Qt3 clipboard in all of KDE. :)"
    author: "Justin"
  - subject: "Re: Suggestion on how to switch to Qt 3"
    date: 2001-06-13
    body: ">The QT3 version of KDE should be released sometime this winter. (KDE 2.X will be \u0093ported\u0094 to QT3). While there will be a separate KDE3 scheduled for release sometime in the future.\n\nI don't think that's correct. When KDE libraries break binary compatibility, major version number gets bumped up (just like with Qt). So KDE 2.x based on Qt3 would be KDE 3.0 and the separate revamped version would be KDE 4.0."
    author: "nap"
  - subject: "Re: Suggestion on how to switch to Qt 3"
    date: 2001-06-13
    body: "Thats why a \"2 to 3 transit\" 2.99 release would be good where the intention of breaking as much binary compatibility as possible could happen. Just as I proposed."
    author: "Sam"
  - subject: "Re: Suggestion on how to switch to Qt 3"
    date: 2001-06-13
    body: "This is sort of right, but not quite. There will be a 2.90 release which will be the latest Qt 2.x based version of KDE ported to Qt 3.0. This release won't include new features etc., it'll just be a way of releasing Qt 3 ported KDE quickly.\n\nOnce this has been done we can get on with the task of changing things we don't like, extending our APIs for the next generation etc., making use of the new Qt features... When this code is ready we will release KDE 3.0.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Suggestion on how to switch to Qt 3"
    date: 2001-06-13
    body: "Ok, didn't know about the naming strategy. Anyway, 3rd party KDE app-developers (especially closed source ones) are probably going to see nightmares, as the life-expectancy of a KDE-BC platform halves each time :)"
    author: "nap"
  - subject: "Re: Suggestion on how to switch to Qt 3"
    date: 2001-06-13
    body: "The reasoning in this case is that the new version of gcc will break binary compatability anyway, so we might as well make use of it by breaking everything at once. From what I've read it should be easier to maintain compatability with the new vtable layout etc. so we can avoid breaking compatability for a long time after this.\n\nPeople distributing binaries don't really have too much to worry about in any case, because they can still link the KDE 2.x libraries. Those releasing source have even less cause for concern as the required code changes are likely to be minimal."
    author: "Richard Moore"
  - subject: "Re: Suggestion on how to switch to Qt 3"
    date: 2001-06-14
    body: "But I guess that they do have to worry about interface differences of other processes in the system changing between releases ( e.g. if the DCOP interface changed ) ?"
    author: "CPH"
  - subject: "Re: Suggestion on how to switch to Qt 3"
    date: 2001-06-14
    body: "Not really, as long as we don't remove any thing the DCOP interfaces will remain compatible. It's not affected by binary compatability.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Suggestion on how to switch to Qt 3"
    date: 2001-06-14
    body: "Hi Rich,\n\nGreat! This was exactly what I was thinking about. Release a somewhat stable KDE 2.99 version that is mostly a port to Qt3. This release is not an official stable relase but merely a stable binary incompatible preview.\n\nThen from there on start hacking what will become KDE-3\n\nYes, From gcc 3 (due in 3 days now !?!) there will be a stable c++ vtable among other things. I have also seen on the ml that pre-compiled header support will probably go in 3.1 and saw there were a 20 factor speed-up compiling kde! (Just search gcc list for PCH)\n\n/Sam"
    author: "Sam"
  - subject: "Re: Suggestion on how to switch to Qt 3"
    date: 2001-06-14
    body: "Yeah, I read about this speed-up - as a developer I'm certainly looking forward to it!\n\nRich."
    author: "Richard Moore"
  - subject: "Re: KDE 2.2beta1 and KOffice 1.1beta3 now frozen"
    date: 2001-06-13
    body: "Hi All,\n\nI just cvsup'ed yesterday morning and spent the rest of the day compiling. I must say that I'm impressed. I'm not sure if it's just the fact that they give so much more visual feedback or that it really is faster, but to me it _feels_ at least a little bit faster.\n\nI haven't tested it extensively or for a long period of time so my comments on stability aren't exactly scientific, but I've yet to experience a crash during operation. Noatun crashes during startup, but hey, it's cvs: no garrantuees that everything works all the time.\n\nI like the fact that kde2 offers so much customizabilty, but in the end I still find myself using most of the default settings for colors and window decorations. I do turn off all the fancy effects though such as fading, window minimization, etc.\n\nI continue to be amazed at the pace of development. Sometimes when reading KDE KC I get the idea that when an experienced kde developer gets an idea, s/he can get a basic proof-of-concept implementation up within a few hours. If someone suggest the need for a certain kcontrol module then half an hour sometimes even seems to suffice. At least that's the impression I get.\n\nThe only problem I've had so far is with kdm, so I'm using the kde 2.1 version for the time being.\n\nbtw; for all the Debian/KDE fanatics out there, have a look at http://kde.debian.net/ (NB. it's .net not .org!)\n\n-- \nMatthijs"
    author: "Matthijs Sypkens Smit"
  - subject: "Some compilation probs with KDE 2.2 alpha2 on sun"
    date: 2001-06-13
    body: "Hi \n\nHere are the following changes I needed to make to make KDE 2.2 alpha2 compile for me using\ngcc 2.95.2 on a Solaris 8 machine :\n\nkdelibs:\n        set CXXFLAGS=-fpermissive before running the configure script\n\n        change the use of mmap ( it's not using void*, but char* ) in kdelibs/kdecore/kconfigbackend.cpp\n\n        added \"#include <netdb.h>\" in a new #else statement for # ifndef HAVE_STRUCT_ADDRINFO in kdecore/netsupp.h\n\n        #undef AF_INET6 before relevant code in inet_ntop in file kdecore/netsupp.cpp\n\n        the Makefile for kio/kpac seemed to be missing a LIBS = $(LIBSOCKET)\n\nkdebase and kdeartwork :\n        During a \"make install\" no check is made whether the tar being used ( for themes mostly ) is gnu tar\n        or \"normal\" tar ( GNU tar has many more options ).\n\nNote that due to work commitments I have not had a chance to verify that this compiled code actually runs, but at least it compiles.\nI know that this is probably not the right forum, but please keep up the good work.\n\nThanks,\nCPH"
    author: "CPH"
  - subject: "Forum for non-Linux users and KDE"
    date: 2001-06-13
    body: "Interesting. There are actually someone more than me who run KDE, but not on Linux! I run KDE on Solaris 7, not without problems, but OK anyway. \n\nSince almost everyone on the KDE core team develops KDE on Linux/i386, I was thinking about setting up a mailing list for non-linux-i386 questions and discussions about KDE.\n\nAnyone interested about having a such list? Maybe it's possible to have this list among the other official KDE mailing lists at lists.kde.org?"
    author: "Loranga"
  - subject: "Re: Forum for non-Linux users and KDE"
    date: 2001-06-13
    body: "Yes, that would be very interresting. I can't even get QT 2.3.0 to compile under NetBSD/Sparc 1.5. He can't found c++rt0.o while linking."
    author: "Nicolas DUPEUX"
  - subject: "Re: Forum for non-Linux users and KDE"
    date: 2001-06-14
    body: "I'm using KDE 2.1.1 under IBM AIX 4.3.3.\nAnd IBM offers an own mailing list on IBM's developper work zone for KDE and other Linux-like programs (such as gnome, gzip,...)."
    author: "Stephan B\u00f6ni"
  - subject: "Re: Forum for non-Linux users and KDE"
    date: 2001-06-14
    body: "Sounds like a good idea. Suggestions for a name?\nnon-linux-i386@kde.org sounds a bit long.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Forum for non-Linux users and KDE"
    date: 2001-06-14
    body: "It's long, but not too long, I think. It's a good name anyway, I think you understand what the list is about."
    author: "Loranga"
  - subject: "KDE behaves like MSDOS"
    date: 2001-06-14
    body: "I Love KDE but, I feel that KDE lacks future planning or technically \"Backward Compatibility\"; it is a pain to see that KDE 2.0 apps doesn't work in KDE 2.1.1 (same with Qt 2.0 and above).\n\nBy default KDE simply ignored the User Aspect of their applications. I just can't use good old kdebase KDE 1.x  apps in KDE 2.1; this just reminds me of \"Incorrect DOS Version\" error used to happen with MS-DOS. \n\nAlso the Fastest Developing apps like Konqueror, Kmail, etc., should be independently distributed for upgrades, like Windows IE upgrades; We the users have to wait for Damned Six Months for RH / Mandrake to include these upgrades. And binary RPM packages just crawl and Sources just won't help (compiling needs ages)...\n\nCan't we have a more modular distribution along with the present packaging system? Who cares! :("
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE behaves like MSDOS"
    date: 2001-06-14
    body: "you have it somewhat backwards: 2.x apps will work in 2.y where y > x (e.g. a 2.0 app will work fine in 2.1) but there are no guarentees where x < y (e.g. a 2.1 app in 2.0). this is called forward compatibility and makes a lot of sense since you get the benefits of keeping compatible with older binaries without giving up license to extend the current system.\n\nas for kde1 apps in kde2, these are two exteremely different systems. it wasn't simply an upgrade, but a complete overhaul and reengineering. kde1 was ok, but exremely limited (i used blackbox w/kmail and konsole back in them days). kde2 is a different beast and so compatibility is expected to be broken. of course, you can always attempt to port a kde1 application to kde2. i did this on IRC one evening for a fellow. the application in question was decently written and had only a few thousand lines of code; it took under half an hour to port it to kde2 (a poorly written app will not be as portable and may warrant a complete rewrite; YMMV).\n\nas for modular binary shipping, the KDE developers are responsible for providing the compilable source code. if your distribution is too slow, either complain to them, switch to a better distribution or get involved yourself. binary distribution is not part of the KDE team's duties."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE behaves like MSDOS"
    date: 2001-06-14
    body: "The problem with over backward capatiblizing (is that a word?) things can be demonstrated by just looking at Windows 9x. Beyond the fact that it makes Windows slow and bloated, you still can't get a Windows 2000 app to run happily even on Windows 95. It's just difficult to have good backward compatiblity when you improve functionality."
    author: "Timothy R. Butler"
  - subject: "Re: KDE behaves like MSDOS"
    date: 2001-06-14
    body: "Sure you can, by statically compiling all dependant libs into the executable ;)"
    author: "Sam"
  - subject: "Re: KDE behaves like MSDOS"
    date: 2001-06-14
    body: "Can't you see it - a calculator that is 100 megs in size because they statically compiled in X includes, QT includes, KDE includes, Mozilla (to look up info on sines), fonts, and a sound server to play error messages. :-)\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: KDE behaves like MSDOS"
    date: 2001-06-19
    body: "lol... I'm suddenly reminded that Word XP will be 180MB and Outlook XP 150MB!\n\nKMail is almost functionally the same as outlook, bar some really bad PIM stuff, and KMail also has PGP integration :) But KMail is a minute fraction of that size... KWord is the same.\n\nThis is partially why my 90GB of c:\\ on Windows is already half filled! :(\n\n::cuddles the KDE:: At least when KOffice 1.1 is released finally, we get pretty much full compatibility, then I can almost kiss Windows goodbye and my boss can't complain :)"
    author: "Dawnrider"
  - subject: "Re: KDE 2.2beta1 and KOffice 1.1beta3 now frozen"
    date: 2001-06-17
    body: "Hello,\n\nMy somebody direct me to any site of howto on making themes for KDE. \n\nI'm planning to make a theme that just look like XP or win2k. My mom will surely find at home when using it. I look to make it really or truly look like win2k.. and somebody cannot guess except by looking at icon on taskbar name terminal :)\n\n-dman"
    author: "dman"
  - subject: "Re: KDE 2.2beta1 and KOffice 1.1beta3 now frozen"
    date: 2001-06-17
    body: ":))) With Dosemu 1.0.2 with freedos :)\nAnd wine :)"
    author: "Edward Tie"
  - subject: "KDE 2.x themes"
    date: 2001-06-17
    body: "Theme factory:\n\nhttp://www.mosfet.org/themeapi\n\nx3c0"
    author: "x3c0"
---
Due to recent instabilities in the CVS code, Waldo Bastian has announced that KDE 2.2beta1 has been delayed in favour of a <a href="http://lists.kde.org/?l=kde-core-devel&m=99229264024258&w=2">message and feature freeze</a> that will last a few days.  During that time, developers will be busy finding bugs and bringing KDE CVS up to shape for a beta release.  This probably means that KDE 2.2 will be released closer to July 30 than the <a href="http://developer.kde.org/development-versions/kde-2.2-release-plan.html">previously planned</a> July 23.  KOffice, on the other hand, is almost <a href="http://developer.kde.org/development-versions/koffice-1.1-release-plan.html">right on schedule</a> for the 1.1beta3 release.  David Faure <a href="http://lists.kde.org/?l=koffice-devel&m=99237539607130&w=2">announced the beta3 freeze</a>, but encourages users to put the current CVS version of KOffice through its paces -- apparently too few KWord bugs are waiting to be fixed.
<!--break-->
