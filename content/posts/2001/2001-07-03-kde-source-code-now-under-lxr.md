---
title: "KDE Source Code Now Under LXR"
date:    2001-07-03
authors:
  - "numanee"
slug:    kde-source-code-now-under-lxr
comments:
  - subject: "Where's KDE 2.2 Beta 1? It's 02/07!"
    date: 2001-07-03
    body: "I'm waiting..."
    author: "Estevanm Neto"
  - subject: "Re: Where's KDE 2.2 Beta 1? It's 02/07!"
    date: 2001-07-03
    body: "*caugh* FTP-Server *caugh*"
    author: "SteffenH"
  - subject: "Can't find kdesupport-2.2beta1"
    date: 2001-07-04
    body: "I checked on 2 mirrors that seemd to have all packages of 2.2beta1 except for kdesupport. Where is it? Or should I use kdesupport of 2.2alpha2?"
    author: "Erik"
  - subject: "Re: Can't find kdesupport-2.2beta1"
    date: 2001-07-04
    body: "Theoretically you don't need kdesupport anymore (well, it was barely needed in the past too, but now changes have made that the absolutely needed stuff went to the appropriate kde* package - like kdenetwork). It mostly contained packages external to KDE that were anyways shipped with most distributions (this also potentially created version conflicts for some packages).\n\nOf course, it much depends on the distro packagers if they prepared their packages so that they don't need anything in kdesupport. Let's hope."
    author: "Inorog"
  - subject: "Re: KDE Source Code Now Under LXR"
    date: 2001-07-03
    body: "Hey,\n\nwhy don't we set up some kind of 'reusability' contest for:\n1)the most (re)used classes\n2)the application that (re)uses the most of them?\n\nIt would be fun to see the hit list of them.\n\nThis would also give a better sensation to the programmers of how good a class has been tested.\n\nAndrea\n\nPS: is there a way of having infos at the method level, like: 'method foo1() of class AClass has never been used, method foo2() of class AClass has been used 25 times...' ?"
    author: "Andrea"
  - subject: "Re: KDE Source Code Now Under LXR"
    date: 2001-07-03
    body: "> is there a way of having infos at the method \n> level, like: 'method foo1() of class AClass\n> has never been used, method foo2() of class \n> AClass has been used 25 times...' ?\n\nMaybe, depends what you mean exactly.  For example, go to the LXR page for KConfig class as given in the above example, and click on any method name (say, virtual void reparseConfiguration();)."
    author: "KDE User"
  - subject: "LXR, CodeWeb, CVSSearch"
    date: 2001-07-04
    body: "There is another great set of source code analysis tools that a friend of the KDE project has put in place: Amir Michail developped a code reusability measurment tool: CodeWeb, as well as a advanced CVS search tool: CVSSearch. It's a pity people don't put just a tiny bit of heart into getting these tools used in their daily work. They are great. Well, shame on me, as I'm one of those who doesn't.\n<p>\nThese, together with lxr, could really really improve KDE developers' productivity a lot.\n<p>\nHere is an URL or three: <br>\n<a href=\"http://codeweb.sourceforge.net\">http://codeweb.sourceforge.net</a>;<br>\n<a href=\"http://cvssearch.sourceforge.net\">http://cvssearch.sourceforge.net</a>;<br>\n<a href=\"http://www.cse.unsw.edu.au/~amichail/\">http://www.cse.unsw.edu.au/~amichail/</a>"
    author: "Inorog"
  - subject: "offtopic: logo"
    date: 2001-07-03
    body: "Great new logo!  It revamps the site completely."
    author: "KDE User"
  - subject: "Re: offtopic: logo"
    date: 2001-07-03
    body: "Yes really cool,\nyet is it just me or the favicon has switched to yahoo's?\nBy the way, the favicon has not worked by me since the dot moved. Hope this gets fixed since I love my bookmarks bar in konqueror..."
    author: "Aris"
  - subject: "Re: offtopic: logo"
    date: 2001-07-03
    body: "Hmmm, it works for me and it's definitely not the Yahoo favicon.  I guess konqueror is caching some wrong icon on your side, somehow.\n\nTry deleting the appropriate icon from .kde/share/icons/favicons/"
    author: "Navindra Umanee"
  - subject: "Re: offtopic: logo"
    date: 2001-07-04
    body: "Well that was exactely it. \nThanks a lot. \nNow my bookmarks bar looks great again :)"
    author: "Aris"
  - subject: "Re: offtopic: logo"
    date: 2001-07-03
    body: "I completely agree. \n\nStylish .. very stylish"
    author: "kde-user"
  - subject: "Re: offtopic: logo"
    date: 2001-07-03
    body: "Yes. Is that the master qwertz doing it again? I'd really like to see an icon set by qwertz. Has he made any icons for KDE? I think it would make the KDE desktop look _very_ classy. No offense, Thorsten. :)"
    author: "Joe"
  - subject: "Re: offtopic: logo"
    date: 2001-07-03
    body: "Problem is: icons have to be hand-crafted to look really good. Rendered and scaled-down icons always look worse. For large pictures, rendering rocks though."
    author: "Lenny"
  - subject: "Re: KDE Source Code Now Under LXR"
    date: 2001-07-03
    body: "That's great et all, but where is 2.2 beta 1? Should've been out today.."
    author: "MiniMe"
  - subject: "Re: KDE Source Code Now Under LXR"
    date: 2001-07-03
    body: "Whoops never mind ;)"
    author: "MiniMe"
  - subject: "Re: KDE Source Code Now Under LXR"
    date: 2001-07-03
    body: "\"Really, the importance of cross-referenced links to any large project can't be emphasized enough\"\n\nRight.\n\n\"The only reason we haven't had something like this before is because they are such a PITA to set up\"\n\nThanks for the great work !\nGo KDE Go !!!!"
    author: "ac()"
  - subject: "KDE 2.2 beta1"
    date: 2001-07-03
    body: "Why I can't download the KDE-2.2beta1 files from the ftp site ? :(\n\nThanks"
    author: "Jean-Christophe Fargette"
  - subject: "Re: KDE Source Code Now Under LXR"
    date: 2001-07-04
    body: "KDE needs 1 more application now. A graphical FTP client similiar to CuteFTP."
    author: "Boris Stevenson"
  - subject: "Re: KDE Source Code Now Under LXR"
    date: 2001-07-04
    body: "KBear??"
    author: "PE"
  - subject: "Re: KDE Source Code Now Under LXR"
    date: 2001-07-05
    body: "Hmmm.. isn't Konqueror a graphical ftp client?"
    author: "Kavau"
  - subject: "Re: KDE Source Code Now Under LXR"
    date: 2001-07-04
    body: "Sorry, I don't know much about LXR or CVS, and I thought I'd take this opportunity to ask some questions.  I'll understand if you don't have time to reply.  ;-)\n\nHow does the code get into LXR?  Does LXR use the code in CVS directly, or must it have a copy?  (I'm assuming CVS is still the code repository.) \n\nIf it needs a copy, can LXR get it automatically, or must a script (or something) transfer occasional code snapshots?\n\nAre there manual steps that must be taken to update the cross-references after LXR gets a new copy of the code?\n\nAre you aware of a way to put comments in a separate file (tied, for example, to an identifier) and have LXR create a cross-reference to those comments?\n\nDo you know of anything like LXR that works for Perl code?\n\nThanks,\nRandy Kramer"
    author: "Randy Kramer"
  - subject: "Re: KDE Source Code Now Under LXR"
    date: 2001-07-04
    body: "LXR needs a local copy of the code to cross reference. No...it can't get it automatically. Someone needs to put the code there. Right now, the code is refreshed daily and cross referenced by a script. The LXR developers are working on a plug-in system that would make it possible to cross reference other languages."
    author: "Nadeem Hasan"
  - subject: "Re: KDE Source Code Now Under LXR"
    date: 2001-07-05
    body: "Nadeem,\n\nThanks very much for your response!\n\nRandy Kramer"
    author: "Randy Kramer"
  - subject: "Source code documentation?"
    date: 2001-07-05
    body: "Just a simple question by an ignorant KDE fan - why can't I find any comments in the KDE source code? Is documenting the source code considered \"uncool\" here? Wouldn't it help new would-be KDE developers tremendously if there would be a minimal amount of documentation within the source code? It's a long time ago that I was an active programmer, but I remember a rule of thumb that said you should have at least as much comments as you have commands in your source code..."
    author: "Kavau"
  - subject: "Re: Source code documentation?"
    date: 2001-07-05
    body: "Hi,\n\nThat's one of the reasons we built CVSSearch.  We noticed that KDE developers typically write CVS comments but not code comments.  So if you can associate the right CVS comment with the right code fragments in the most recent version of the code, then you have commented source.\n\nAmir"
    author: "Amir Michail"
  - subject: "Re: Source code documentation?"
    date: 2001-07-06
    body: "The .h files (API) are some of the best documented though.\n\n-N."
    author: "Navindra Umanee"
---
The KDE source code is now available under the <a href="http://lxr.kde.org/">LXR system</a>, courtesy of our friends at <a href="http://www.nadmm.com/">nadmm.com</a>.
Users and developers may now browse the KDE source code complete with cross-references, which should prove extremely useful.  For those of you wondering about the difference between <a href="http://lxr.kde.org/">lxr.kde.org</a> and <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/">webcvs.kde.org</a>, read on for an explanation from Kurt.
<!--break-->
<br><br>
<i><a href="mailto:granroth@kde.org">Kurt Granroth</a> writes:</i>
<br><br>
<blockquote>
There is actually a <i>huge</i> difference between a webcvs and an lxr site.

Using WebCVS, you can view the contents of a file at any revision and compare
differences between revisions.

Using LXR, you view only the current revision, <i>but</i>, every object in the file
is cross-referenced to every other object in the LXR database.
<p>
Let's take the KConfig class for example.  I can go to the <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kdecore/kconfig.h">kconfig.h</a> file in
WebCVS and view the current revision... or I can view past revisions... or I
can compare various revisions.  To see how it has changed historically, this
is very handy.  But say I want to know where KConfig is used elsewhere in KDE.
This is impossible to do automatically in WebCVS.
<p>
If I go to the <a href="http://lxr.kde.org/source/kdelibs/kdecore/kconfig.h">KConfig class</a> in LXR, though, I see that all objects and
methods in the file are hyperlinks.  If I click on <a href="http://lxr.kde.org/ident?i=KConfig">KConfig</a>, I get a list of
where it is defined, where it is declared as a forward declaration, and where
it is referenced (in 939 files!).  I can click on any of those links to go
directly to where it is used.
<p>
Or say I want to see where KSocket is defined and used in KDE.  I go to the
<a href="http://lxr.kde.org/ident">identifier search</a> and enter <a href="http://lxr.kde.org/ident?i=KSocket">KSocket</a>.  I get a page listing everything I need to know.
<p>
Really, the importance of cross-referenced links to any large project can't
be emphasized enough.  The only reason we haven't had something like this
before is because they are such a PITA to set up. :-/
</blockquote>