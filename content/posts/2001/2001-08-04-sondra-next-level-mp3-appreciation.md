---
title: "Sondra, The Next Level in MP3 Appreciation?"
date:    2001-08-04
authors:
  - "numanee"
slug:    sondra-next-level-mp3-appreciation
comments:
  - subject: "Sondra"
    date: 2001-08-03
    body: "Excellent.  The next step is to link it to a database that links the cut to a style, CD, artist list, date of composition, date of release, etc. and let each one of these variables influence the next selection.  Going even further: tempo, theme, musical period, and significant lyrics could be linked.  This is what radio djs do (or used to).  Heck, a thesaurus function or pun linker could even get into the act!"
    author: "Ross Baker"
  - subject: "Re: Sondra"
    date: 2001-08-04
    body: "I agree completely. I've wanted this for a long time, and have thought about trying to write my own version of this.  If your extension ideas get added, I'll be in hog heaven!"
    author: "TomC"
  - subject: "Ogg me"
    date: 2001-08-03
    body: "It would be great if this could work with *.ogg files."
    author: "Mark Hillary"
  - subject: "Re: Ogg me"
    date: 2001-08-03
    body: "As an Ogg supporter, I was wondering if you knew of a hardware player that supports Ogg.  Otherwise, I'm getting a portable player that only supports MP3 due to the simple dual facts of 1) it's cheap and 2) it's really nicely engineered.\n\nAnd, of course, that pretty much sets my personal format for the near future.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Ogg me"
    date: 2001-08-04
    body: "This interests me too.\nIn particular, it would be great to see some schematics for building your own hardware based Ogg player."
    author: "Konqi"
  - subject: "Re: Ogg me"
    date: 2001-08-04
    body: "errmmh, it would be great, yes.\nUntil someone makes a chip for that you will have to handcode the decoding code in assembler for some one-chip micro-controller. (or in C, possibly)\nNot impossible, but a lot of work."
    author: "reihal"
  - subject: "Re: Ogg me"
    date: 2001-08-04
    body: "I think the iomega hip zip supports .ogg\nfiles - you may have to reflash it. \n\nRob"
    author: "rob"
  - subject: "the problem with the hip zip ..."
    date: 2001-08-04
    body: "is that it sucks :)\n\nActually, that may not be true for some people, no offense to Iomega is intended. \n\nHowever, those \"pocketzip\" disks (formerly \"clik!\" -- guess that name didn't work out too well ;)) aren't common, and I doubt they ever will be. They're (not even) the Bernoulli carts of the early 21st century ... I don't trust Zip drives, and have no reason to trust the little version either. If they were the only alternative, maybe I would, but I can't think of any area in which a CD-R / CD-RW is beaten by Zip except longest dimension, since the Zips are small and rectangular.\n\nIf Iomega sees the potential in Ogg, as they seem to, I wish they would come out with a player with a better chance of survival -- like one that plays burned CD-Rs or CD-RWs, or even DVD-RAM. Or if big-spinning-disks players are too bulky, maybe one using SmartMedia or CompactFlash? \n\nEven (and I say this because I'd like to see more of the tiny CD-Rs ;)) tiny CD-Rs? I'd like to see a cute little round clamshell that plays vorbis from those mini-disks (not MiniDisks). iomega would be a perfectly adequate name to have on the outside of it, but so would a lot of others. \n\nIomega isn't wedded to Zip anymore, which is good -- I just wish they would make a clean break from them, since they have low capacity and low speed, and are not particularly reliable. \n\nHmmm. That small-CD clamshell player is sounding like a good idea to me, anyone want to sell one to me? :) \n\ntimothy"
    author: "timothy"
  - subject: "Re: the problem with the hip zip ..."
    date: 2001-11-18
    body: "Dear Timothy,\n\nhave u ever tried to go around or jogging with a portable Cd player? I have one and just bought a hip zip for 99 $...it sounds well and, surprise!!!, no junps....\n\nGood look with your big and heavy cd player....  : )\n\nThx anyway for your advice\n\nTony"
    author: "tony"
  - subject: "Re: Ogg me"
    date: 2001-08-07
    body: "the intel blah blah mp3man has support for adding new codecs.\nWhen I emailed them about this they said they were polling for input on what codecs people would like support for in future upgrades (thats software upgrades too).\nmy vote has been added. has yours?\n(of course its windows only... but so dinky <sigh>)"
    author: "Caoilte"
  - subject: "Re: Ogg me"
    date: 2001-08-04
    body: "Well you can simply change the \".mp3\" in the code and you should be find.  I actually thought about having it be user defined so you could also have .wav, etc in it"
    author: "Benjamin C Meyer"
  - subject: "What about ID3?"
    date: 2001-08-04
    body: "I thought ID3 was an MP3 thing only? Does it really work for ogg and wav?"
    author: "Navindra Umanee"
  - subject: "Re: What about ID3?"
    date: 2001-08-04
    body: "Actually, .ogg files have a generic name-value system for adding arbitrary fields to each file, so if anything it should be _easier_ to implement this for ogg.  A quick eyeballing of the libvorbis headers would probably get one most of the way there."
    author: "Space Coyote"
  - subject: "Sounds like Tivo"
    date: 2001-08-04
    body: "So its not exactly the same thing, but it is similar in concept.  Play the kind of music that I like.  What?  I've been listening to a lot of Clasical?  Oh, well lets see what other clasical music we have on disk and queue it up for playback.\n\nI'm curious if this couldn't be extended to include Tivo favorite list like features for TV tuners.  Or to search MP3.com or some other online free repository of music for music similar to what is being played.  I'd DEFINATELY use something like that.\n\nVery original.  Very cool.  I like it :)"
    author: "Falrick"
  - subject: "Re: Sounds like Tivo"
    date: 2001-08-04
    body: ">Or to search MP3.com\n\nYeah, that way it could, say, dial-up in the middle of the night and download songs it thinks you might like. Perhaps the system could even be set to occasionaly download a random song, just to see if you like it and might want to get into that subgenre/artist."
    author: "Carbon"
  - subject: "Storing info outside of the MP3 file"
    date: 2001-08-04
    body: "I put my MP3 files in a directory accessible from the LAN so that other users can play them as well. This conflicts with writing the information inside the MP3 file. First problem is that I made the files read-only, to avoid silly mistakes putting holes in my MP3 collection. Second problem is that every listener has a different taste in music, therefore their listening records should be kept separately.\n\nSo I would prefer if the gathered information would be stored outside of the MP3 file. There are other arguments in favor of external storage. Not every format can hold additional info; XMMS has a large number of input plugins and can play for example all the tracker formats (MOD, S3M, XM etc). Another argument for external storage is file sharing: if you swap music with others, you don't want their listening info mixed with yours."
    author: "Maarten ter Huurne"
  - subject: "Re: Storing info outside of the MP3 file"
    date: 2001-08-05
    body: "Rank them before you put them as read-only."
    author: "Benjamin C Meyer"
  - subject: "Re: Storing info outside of the MP3 file"
    date: 2001-08-05
    body: "the whole purpose of this inventnion is that the ranking happens dynamically. every time you manually select a file, the rankings change. if you made the mp3's read only (as almost EVERY person i know does) then the dynamic ranking cannot happen. this defeats the wholr purpose of the entire technology."
    author: "dude who cares"
  - subject: "Re: Storing info outside of the MP3 file"
    date: 2001-08-05
    body: "If you use an OS with a sensible file permission system, it is trivial to make the files read-only to everyone who doesn't know your password, but still allow yourself to write to them.  I don't see what the fuss is about.\n\nThe other problem (that music preferences shouldn't be shared along with the music) is actually much more of a problem.  The only way this could be solved is if Sondra started storing its data in an external database instead of inside the .mp3 files (which sounds quite sensible to me, actually)."
    author: "not me"
  - subject: "Re: Storing info outside of the MP3 file"
    date: 2001-08-09
    body: "Err - i can clearly understand what the problem is when you store your data inside the mp3file.\n\nIt results in _no_ multi user ability.\n\nYou know, most ppl like different kinds of music like one has punk as his favourite - and the other would like to hear some techno music.\nTheir rankings probably won't match.\n\nBesides, I really don't like to save _any_ data\nexcept the music itself inside the mp3.\nid3 sucks big times.\nUse a clear structure for your mp3's like \"artist - album - track# - title.mp3\" and store data like ranking information in a separate library (mysql would be a good choice fe.)\n\nbye, valor"
    author: "valor"
  - subject: "for those people dual booting, the Windows version"
    date: 2001-08-04
    body: "Sondra is not unique.\n\nThe free (but not open source) program MP3 Control does this exact same thing.  It is in a .7 release at the moment.  More info can be found at http://mp3control.virtualave.net/.  I've been using the feature that track song usage for a while, and it generally works pretty well.  Reading the faq, it sounds like the auther wrote it for the same reason a lot of free software gets written:  nothing else did what he wanted.  \n\nIt can integrate with winamp or be used on its own.  Don't know how it works under wine."
    author: "Bob"
  - subject: "Re: for those people dual booting, the Windows version"
    date: 2001-08-04
    body: "The .7 hardly means anything.  I could have called my application.9 if I so desired.  As far as I know there is only 1 bug, so it is pretty far done.  Not what I would call beta."
    author: "Benjamin C Meyer"
  - subject: "Re: for those people dual booting, the Windows version"
    date: 2001-08-04
    body: "Site is down...  I can't find any information about it and the site seems to be down..."
    author: "Benjamin C Meyer"
  - subject: "Similar idea..."
    date: 2001-08-04
    body: "I've had a similar idea in the past.  What I was considering was having a ranking system the same as Sondra, except the music would come from an Icecast server, and the rankings would be sent back there from multiple users (think an office of people listening to the same speakers).\n\nThat way, more popular songs would be played a little more often than the obscure ones, and, as people get sick of a song being played too much, the ratings go down and the song gets played less.  A \"veto\" option would also be good..."
    author: "Jim"
  - subject: "Re: Similar idea..."
    date: 2001-08-04
    body: "Now that you mention it, I swear I saw this used as an example in some Linux magazine.  Could have been L. Journal or L. Magazine.  IIRC, it was used as an example of using PERL to make interactive web pages.  People ranked the song that was playing, the page was sent back to the server and then the playlist weightings were changed. Darned if I can remember more about it at this point."
    author: "Bob"
  - subject: "Re: Similar idea..."
    date: 2001-08-04
    body: "should be possible to use dcop+perl or php\nto make sondra do that job (if sondra has a \ndcop interface, that is)"
    author: "ik"
  - subject: "Re: Similar idea..."
    date: 2001-08-04
    body: "Well adding sandra to a web page is something that can be done quite easily using the 2 command line tools."
    author: "Benjamin C Meyer"
  - subject: "Ogg Vorbis"
    date: 2001-08-04
    body: "I also would like to see support for Ogg Vorbis."
    author: "Joergen Ramskov"
  - subject: "It's not that origional..."
    date: 2001-08-05
    body: "Umm, I've seen things like this on Freshmeat for years now. I think it was Freshmeat, anyway, it might have been on sourceforge, but it definately existed and it definately sounded just like this. It didn't mention storing information in ID3 tags, but that was the only difference."
    author: "Chris Carlin"
  - subject: "Re: It's not that origional..."
    date: 2001-08-05
    body: "Please Bring forth this fantome!  Quick search here, quick search there, nada.  You are probably thinking of playlist generators, that would just scan files This is nothing special.  Sondra generates the playlist based upon rank which is special."
    author: "Benjamin C Meyer"
  - subject: "Re: It's not that origional..."
    date: 2001-08-06
    body: "<a href=\"http://pimp3.sourceforge.net\">http://pimp3.sourceforge.net</a>\n<br><br>\nRank things as you play them, and it starts to play songs you like more often than those you don't."
    author: "Lauri Watts"
  - subject: "Re: It's not that origional..."
    date: 2001-08-07
    body: "www.mserv.org"
    author: "Peter"
  - subject: "Re: It's not that origional..."
    date: 2001-08-07
    body: "Please, Benjamin...\n\nPlease stop trying to push your software as some new \"nifty wowie-gee I can't imagine anyone else even thinking of this idea before me\" plugin.  Many many people have had this idea before you, and I'm sure many more before them.  Hell even I did (I used to be a DJ and actually started a database with genre, bpm, beginning and ending \"style\", etc.) -- Your idea is *not* original.  It's a hard lesson to learn but once learned it saves a lot of head/heartache in the future.\n\nWith that being said, let me commend you for actually getting software out the door.  *That* is what many many people with the idea have *NOT* done (or not done well), and that is exactly what you should be proud of.  When I get home I will be installing sondra and giving it a whirl.\n\nNow as far as ideas for improvement go:\n- plugins for your plugin so the ranking system can be modified\n- database support (metadata on 8000 mp3s in a flat file is not a good idea), I humbly reccomend PostgreSQL, as it is capable of stored functions and I can modify the database based on the modifications you make\n- the ability to \"clump\" songs together.  i.e. if I have a ripped trance CD where each individual track is a song but they're meant to be played as a group, tag this so when a song in the clump is selected, it actually plays the first song in the clump and then plays the entire clump (subject to ranking, of course) before bouncing out of it.\n- a ranking system which is more or less automatic:\n..- if I let a song play all the way through, bump it up slightly.\n..- if I skip a song after it's started playing, bump it down slightly.\n..- if I repeat a song, bump it up more\n- try to make it player-agnostic.  I use XMMS myself so it's not a personal request, but I'm sure many others would like to use this on their favourite player\n- try to make it cross-platform so the Win32, QNX and MacOS guys can get at it\n- Add support for a tracker-style mixer, perhaps as a plugin.  (this is something I was working on when I was a DJ) -- it's bascially a little \"procedure\" which gets executed when tracks start and/or end.  (Think Scream Tracker interface) -- Of course the procedure would need to know what song is playing and what song is next so it could choose the appropriate mix track.  Mix tracks would allow things like volume changes/slides, playback speed slides, sample playing/offsetting, sfx keying and so on."
    author: "Andrew"
  - subject: "Re: It's not that origional..."
    date: 2001-08-05
    body: "The Apollo player (for windows, unfortunately.. it was pretty decent) did something like this."
    author: "sesquiped"
  - subject: "Re: It's not that origional..."
    date: 2001-08-05
    body: "Nope, they don't have ranking of any sort.  They have a little bit more advanced search capability, but not playlist generation."
    author: "Benjamin C Meyer"
  - subject: "Talking about song ranking..."
    date: 2001-08-05
    body: "I'm thinking back about 4 years now...\n\nI'm sure there used to be a web site which would ask you to rate albums, and then recommend new music to you, based on previous ratings by other people. Does anyone remember this, and know if it's still going (I doubt it's still there, this was quite a while ago: before MP3s became as big as they are now).\n\nOn a different tack, there used to be a site which was a sort of stock market in event probabilities (from coin flips, to thermonuclear war). This would be about 4 years ago as well. Does anyone remember this?\n\nI used to have all these sites bookmarked, but on computers that I don't own anymore :)"
    author: "Jon"
  - subject: "Same Data, Read-Only MP3s?"
    date: 2001-08-05
    body: "I love the idea, but I don't want the MP3s themselves written!  Does it support writing it to a separate database, even just a text file?\n\nHow can you make a shared MP3 database for the house, if everyone's different preferences are written to the MP3 files themselves?  That also suggests that you have WRITE access to the MP3 filesystem, where a good jukebox would lock off most writes to avoid accidental deletions."
    author: "Speare"
  - subject: "Re: Same Data, Read-Only MP3s?"
    date: 2001-08-06
    body: "Well generally you would rank the songs simply based upon how good they were.  Generally people will rank the songs about the same.  You could then rank it before you mark it read-only.  Also if you want to have a file or database then you can simply re-write one of the files in Sondra (song.cpp) and you are all set."
    author: "Benjamin C Meyer"
  - subject: "Offtopic: KDE2.2?"
    date: 2001-08-06
    body: "Does anyone know the status of KDE2.2 ?  According to the updated release schedule (posted on the Dot a few weeks back), KDE2.2 would be available via ftp on the 3rd (two days ago), and announced tomorrow.\n\nI don't see the packages on ftp.kde.org, but the CVS has been tagged as KDE_2_2_RELEASE, so it looks like things are rolling.\n\nAnyway, good work!  As you can tell, I can't wait :)"
    author: "Justin"
  - subject: "is anyone else reminded of STTNG?"
    date: 2001-08-06
    body: "\"Computer, I want some music, play some classical.\"\n\n\"Hmmm. That's not quite right- something slower, more romantic...\"\n\nI think the conversation like this was in the one where Geordi is trying to get into Leah Brahms' knickers...."
    author: "graspee"
  - subject: "Unique yes.  Useful?"
    date: 2001-08-07
    body: "Interesting idea, however just because you've listened to the Macarena 99 times doesn't mean that it is now the only song you ever want to hear.   Going to have to be careful how those playlists are generated."
    author: "Steve"
  - subject: "Re: Unique yes.  Useful?"
    date: 2001-08-08
    body: "In the source there is a file called ThinkingLogic or something that explains just what detirmines what will be in the next batch of songs."
    author: "Benjamin C Meyer"
  - subject: "Altering MP3's is a Bad Idea - From My perspective"
    date: 2001-08-07
    body: "I like to keep my mp3's high quality and sharable.  I prefer to keep them in pristine condition, without tainting them with my own personal ID3 tags.  I also don't want to find mp3's with other peoples commentary other than that of the original creator/etractor/ripper...."
    author: "GoofBall"
---
<a href="http://www.csh.rit.edu/~benjamin/">Benjamin Meyer</a>, author of <a href="http://kinkatta.sourceforge.net/">Kinkatta</a> (the app formerly known as Kaim, renamed at the kind suggestion of AOL), is ready to present his next project, <a href="http://www.csh.rit.edu/~benjamin/desktop/programs/sondra/">Sondra</a>.
Sondra is an on-the-fly MP3 playlist generator that is really quite original in its concept.  You can rank a song while listening to it, and this personal ranking, along with some other pertinent information such as the number of times you have listened to the song, is written to the MP3 using the new ID3 format.  Sondra uses this information to generate the playlist on the fly, creating the amazing feeling that one is listening to a radio station but one that is particularly geared towards one's preferences. Sondra has a <a href="http://www.csh.rit.edu/~benjamin/desktop/programs/sondra/screenshots.html">KDE-interface</a>, a command-line interface, and the backend is implemented as a library, so anyone can use it with maximum flexibility.



<!--break-->
<p>
According to Benjamin, this concept may well be unique and original enough that it probably hasn't been implemented elsewhere (including Windows-land). Currently, Sondra is tied to <a href="http://www.xmms.org/">XMMS</a>, but should be easy to generalize to use <a href="http://noatun.kde.org/">your favorite player</a>.  I also wonder if Sondra could be made to use Ogg Vorbis, given the patent troubles with MP3.
</p>


