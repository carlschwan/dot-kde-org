---
title: "Konqueror, KDevelop won the Linux New Media awards"
date:    2001-10-22
authors:
  - "Inorog"
slug:    konqueror-kdevelop-won-linux-new-media-awards
comments:
  - subject: "Kongratulations and Kudos"
    date: 2001-10-22
    body: "Kongratulations and Kudos to Konqi and Kdev teams !"
    author: "hackorama"
  - subject: "KDE memeber sites are not updated"
    date: 2001-10-22
    body: "konqueror.org, koffice.org etc., has not seen any updates for years, The documentation is outdated.\n\nNo surprise that Konqueror and Kdevelop got the best app award ;) All KDE applications are superb.\n\nI wonder if anybody could port ZZplayer to KDE 2.x or 3.0 http://zzplayer.sourceforge.net. A Video CD player still lacking in KDE multimedia package."
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE memeber sites are not updated"
    date: 2001-10-22
    body: "Best would be to write a good KDE gui for mplayer, it is\nthe best video player out there (Although the developers are\na little difficult sometimes, they do not like gcc 2.96, ICEWM,\nsawfish, KDE and users who don't RTFM)"
    author: "Danny"
  - subject: "Re: KDE memeber sites are not updated"
    date: 2001-10-22
    body: "Such writing should be easy with KDevelop, last but not least since it has won the Award. ;-)"
    author: "F@lk"
  - subject: "Re: KDE memeber sites are not updated"
    date: 2001-10-22
    body: "or use aviplay - its C++/QT\nand integrate a standard lib for mpeg2/dvd\nand integrate \"transcode\" into artsbuilder objects..\nand ...\n\n(*dream*)\n\nBye\n\n  Thorsten \n\n--\nhttp://avifile.sourceforge.net/\nhttp://www.theorie.physik.uni-goettingen.de/~ostreich/transcode/"
    author: "Thorsten Schnebeck"
  - subject: "Re: KDE memeber sites are not updated"
    date: 2001-10-22
    body: "But aviplay only supports AVI/ASF/WMV(7)s... MPlayer also supports MPEG1/2s, WMV8 and playing directly from encrypted DVDs. And it is more optimized... And rumours say that they are also planning on support for RealVideo and QuickTime movies..."
    author: "Per Wigren"
  - subject: "Re: KDE memeber sites are not updated"
    date: 2001-10-22
    body: "<a href=\"http://xine.sourceforge.net/\">Xine</a>"
    author: "Anon"
  - subject: "libs vs. apps"
    date: 2001-10-22
    body: "Yes, there are quite some very nice universal players like xine, mplayer...\n\nThis is not about \"assimilation\" something into KDE.\nThis is about integration into KDE. We need a plugin-like player/arts-object/component also for integration in apps like kpresenter.\n\nSo maybe better go with a pool of libs than with one app (?)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: libs vs. apps"
    date: 2001-10-23
    body: "Well, this being Open Source, we can have our cake and eat it too. We can just re-integrate any given codec as an aRts library, and then it should run (theoretically :-) in any app using KParts, including Konqi and KPresenter. Of course, (in KPresenter for example) doing things like making sure movies play at the right time in presentations, grabbing the first and last frame and using them for in and out effects, etc etc, would require special coding, but once that is done, you could swap out the backend for another fairly easily."
    author: "Carbon"
  - subject: "Re: KDE memeber sites are not updated"
    date: 2001-10-24
    body: "Ogle rocks for DVDs, it just rocks #1.\nAviplay for ASF/AVI.\nGTV for MPEG.\nRealPlayer for the crappy RMs.\n\nThat's all you need. :-)"
    author: "The_Iconoclast"
  - subject: "Re: KDE memeber sites are not updated"
    date: 2001-10-22
    body: "You're right about that! I wonder how many times ARPi has had to get a new keyboard because the keys R, T, F and M are broken... :)\n\nI have been thinking about making a KDE mplayer GUI for quite some time, but my C++ skills are just not there yet... I'm trying to learn though..."
    author: "Per Wigren"
  - subject: "mplayer & kde"
    date: 2001-10-22
    body: "well you can have fullscreen in xv mode in kde / icewm and others\njust use the sample config file and select the proper option (the file itself is well documented).\n\nOtherwise, fullscreen works fine with -vo sdl"
    author: "fl"
  - subject: "Re: KDE memeber sites are not updated"
    date: 2001-10-22
    body: "mplayer simply ROCKS! It's too bad the gui is gtk, cuz I didn't install those libs and I'm not planning to because gmplayer needs them.. I currently merged mplayer in my filetypes thingie and it's heaven! real player and quicktime support would make it even more powerfull then m$ mplayer, quicktime and realplayer in one!! (really curious about that rumour :P) and last but not least, it even works with arts thru sdl! Maybe it's possible to write a noatun plugin?"
    author: "Pieter Bonne"
  - subject: "Re: KDE memeber sites are not updated"
    date: 2001-10-22
    body: "mplayer really sucks compared to smpeg, especially in the CPU dept.  Is it possible to use SDL from a KDE app?"
    author: "David Walser"
  - subject: "Re: KDE memeber sites are not updated"
    date: 2001-10-22
    body: "ZZplayer Does use Smpeg and SDL nicely but the Author Nicolas Vignal did not tried to port this to KDE 2.2 and above. I wonder why. it's a nice app. I wish any kde developer with average or above kde coding skill can port it. I just don't know how to code KDE :("
    author: "Asif Ali Rizwaan"
  - subject: "Daniel M. Duley aka Mosfet drops out"
    date: 2001-10-22
    body: "Mosfet takes his marbles and goes home:\n\nhttp://www.mosfet.org"
    author: "APW"
  - subject: "Re: Daniel M. Duley aka Mosfet drops out"
    date: 2001-10-22
    body: "...big loss, surely...\nBut I am confident others will pick up where he left off. \nAnyway, with Mosfet's up-and-down attitude, maybe he'll get bored of his new Windows XP machine and return to KDE... *grin*"
    author: "Jacek"
  - subject: "Re: Daniel M. Duley aka Mosfet drops out"
    date: 2001-10-22
    body: "I don't intend to diminish his contributions to KDE, but Mosfet is only one of many persons contributing to KDE. In fact there are probably many coders with more importance. \n\nAnd what generally seems even more important to the KDE development is good teamwork. I think much of the success and development speed of KDE is due to the fact that the hackers work together as a team and not as jealous individuals.\n\nInsofar it's sad that Mosfet decided to leave Linux altogether, as he is surely talented, but he is certainly not undispensable. \n\nWho picks up the Liquid theme ;) ? BTW, is this possible with the current license?"
    author: "AC"
  - subject: "Re: Daniel M. Duley aka Mosfet drops out"
    date: 2001-10-22
    body: "This one is from Tackat (Torsten Rahn):\nMosfet has ported Liquid to KDE3 with a BSD-licence.\n\n(Source: http://www.pro-linux.de/news/2001/3573.html, search for \"tackat\", German only)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Daniel M. Duley aka Mosfet drops out"
    date: 2001-10-22
    body: "It doesn't seem to be anything other than a rumor, though.  There's no link, and from the discussion that went on in the kde list, it sure doesn't sound like he had any plans to do so.\n\nIf anyone else had ported it to kde3, it still needs to be distributed as patches to liquid-0.6 (because of the QPL) or you would have to re-implement based on the liquid-0.1 code (which was released as BSD)."
    author: "Ranger Rick"
  - subject: "Re: Daniel M. Duley aka Mosfet drops out"
    date: 2002-10-03
    body: "He is a She"
    author: "steve"
  - subject: "Re: Daniel M. Duley aka Mosfet drops out"
    date: 2002-10-03
    body: "Actually, she's a he."
    author: "fault"
  - subject: "What again!"
    date: 2001-10-23
    body: "What is it that's upset him this time?"
    author: "John"
  - subject: "Re: What again!"
    date: 2001-10-23
    body: "He's tired of getting screwed over by Linux companies."
    author: "someone else"
  - subject: "Re: Daniel M. Duley aka Mosfet drops out"
    date: 2001-10-23
    body: "From the website:\n\nMosfet.org is no longer maintained. I will not be responding to emails or comments about Linux, since I will no longer be using it on a regular basis. Sorry."
    author: "Brutus"
  - subject: "Awards"
    date: 2001-10-22
    body: "Kant you stop getting new awards? :)"
    author: "UnBlessed"
  - subject: "X improbements"
    date: 2001-10-22
    body: "Would it be posible to fix the jumps with the X system? I mean: When you set the default login as a X login (KDM for example), firts, it appears the console login. Then it shows a X jump to a gray X background, later the kdm background and after it (if you have set a image background, there is another jump between the color background and the image), the kdm login manager. and I think it is not very elegant. would it be posible to fix this jumps?"
    author: "daniel"
  - subject: "Re: X improbements"
    date: 2001-10-22
    body: "This is really something that annoys me for aeons of time.\nLinux would be much nice without these 'hicks'"
    author: "me"
  - subject: "Re: X improbements"
    date: 2001-10-23
    body: "Boot sequence as appears on monitor:\n\nBIOS\nLILO\nKernel bootup/run level entry\nX starting\nKDM background showing\nKDM showing\nKDE blank background / splash screen\nKDE user background\n\nThis is fairly simple to fix. There is no easy way to handle the very first one, so we can ignore it. Caldera and Mandrake have both built a system that makes 2 and 3 look identical for the most part. 4 is just a matter of KDM loading it's background first thing, and using a background close or identical (though in higher res of course) to the 2 and 3 backgrounds. After that, it should switch to the user's custom background, meaning a jump only 3 times. Mostly just a matter of installing Mandrake or Caldera's (perhaps other?) custom tools, using a matching kdm background, and moving a couple xsets to the very beginning of scripts."
    author: "Carbon"
  - subject: "Re: X improbements"
    date: 2001-10-23
    body: "'kdmdesktop' is the program that loads the kdm background.  Unfortunately, like all KDE apps, it takes a day and a half to startup.  Maybe the kdm background loader could be the nice job of a non-KDE/Qt app.\n\nI believe there is a way to tell X what to use as the initial background (like instead of that gray checkered thing).  You could just make it a nice shade of blue instead.  Then a fraction of a second later the background should load."
    author: "Justin"
  - subject: "Re: X improbements"
    date: 2001-10-23
    body: "This is from the startkde script (which is executed by kdm to start KDE):\n<quote>\n# Set the background to plain grey.\n# The standard X background is nasty, causing moire effects and exploding\n# people's heads. We use colours from the standard KDE palette for those with\n# palettised displays.\n\nxsetroot -cursor_name left_ptr -solid '#C0C0C0'\n</quote>\n\nSo all you have to do is change the grey color to the color of the kdm background or the color of the user's background. If you use an image as background then using the -bitmap option instead of the -solid option should help. Unfortunately xsetroot can't set a gradient background."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: X improbements"
    date: 2007-05-15
    body: "edit the file /etc/kde3/kdm/backgroundrc and change the line 'Wallpaper=<filename>'\n\nIn my case it's Wallpaper=Kubuntu-Volutes.jpg \n\nThat will fix the annoying jumps.  Just change this when you happen to change your KDM theme and/or splash screen with the matching background file and it will be seamless every time.\n\n"
    author: "Ddog"
  - subject: "KDE Toolbar"
    date: 2001-10-23
    body: "Quick question.  Is the toolbar used in Konq, KMail and other KDE apps a QT widget or a KDE widget?  It is exhibiting strange behaviour (maybe it is implemented this way), when you resize the app.  For instance, the toolbar bands jump from one line to another (wrap).  Is it possible to lock the toolbars that they don't wrap around, sort of like IE or MS Office.\n\nThanks"
    author: "Robert Gelb"
  - subject: "KMPlayerlib"
    date: 2001-10-24
    body: "If you could rewrite mplayer as a KDE lib to be used in any K app it would be heaven.\n\nmplayer currently supports:\n\n    * VCD (Video CD) directly from CD-ROM or from CDRwin's .bin image file\n    * DVD, directly from your DVD disk, using libdvdread for decryption\n    * MPEG 1/2 System Stream (PS/PES/VOB) and Elementary Stream (ES) file formats\n    * RIFF AVI file format\n    * ASF/WMV 1.0 file format\n    * QT/MOV file format with (un)compressed headers (INCOMPLETE!)\n    * supports reading from file, stdin, or network via HTTP\n\n    * MPEG1 (VCD) and MPEG2 (DVD) video\n    * DivX ;-), OpenDivX (DivX4) and other MPEG4 variants\n    * Windows Media Video 7 (WMV1) and 8 (WMV2) used in .wmv files\n    * Intel Indeo codecs (3.1,3.2,4.1,5.0)\n    * MJPEG, ASV2 and other hardware formats\n\n    * MPEG layer 2, and layer 3 (MP3) audio (native code, with MMX/SSE/3DNow! optimization)\n    * AC3 dolby audio (native code, with SSE/3DNow! optimization)\n    * Ogg Vorbis audio codec (native lib)\n    * Voxware audio (using directshow DLL)\n    * alaw, msgsm, pcm and other simple old audio formats\n\n\n....and a few others PLUS ALL Xanim codecs as well (ie you can play quicktime, 3ivx's etc with mplayer)\n\n\nmplayer can make use of X11/SDL/GL/XV for video output rendering, and ie with xv I can play divx's very good in full screen with my PII 300 thanx to my TNT2 (mplayer supports YV12 for pixel rendering, making extensive use of TNT2)\n\n\n\nPLEASE consider this an alternativ, even though it hasn't been started as a \"K\" project!\n\nThanx"
    author: "Zeb"
  - subject: "Re: KMPlayerlib"
    date: 2001-11-08
    body: "now it even supports ViVO and encoding from AVI/VOB/ASF/WMV/VIV/MOV to DivX4+VBR MP3!!\n\n\ndon't you say how amazingly fast it proceeds?\n\nif not implemented as arts modules, can we at least have a nice KDE GUI for this amazing app?"
    author: "Kuser"
---
<A HREF="http://www.kde.org/people/ralf.html">Ralf Nolden</A>, our friend and colleague, brought us the news late last week that two major products of the KDE project won Linux New Media awards at the <A HREF="http://www.munichtradefairs.com/Systems/default.htm">"Systeme" fair</A> in M&uuml;nchen.  <A HREF="http://www.konqueror.org/">Konqueror</A>, the versatile KDE web browser, file manager and document viewer, won the award for "Best client-side Open Source Software". <A HREF="http://www.kdevelop.org/">KDevelop</A>, the outstanding integrated development environment (of which Ralf is a principal developer), merited the "Best development tool" award. Details about these and other awards can be found at <a href="http://www.linux-community.de/Neues/story?storyid=2174">linux-community.de</a>. Congratulations to all the members of our community who make these successes possible through their relentless work on code, graphics, translations, testing and all the other components of our project.
<!--break-->
