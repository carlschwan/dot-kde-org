---
title: "Gideon Development Update"
date:    2001-06-09
authors:
  - "numanee"
slug:    gideon-development-update
comments:
  - subject: "Re: Gideon Development Update"
    date: 2001-06-09
    body: "Great Stuff!\nlooks amazingly!\n-gunnar"
    author: "gunnar"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-10
    body: "can anybody tell me how to get the window-geometry of a mainwindow + window decorations?\n\nthe QT docs\n(e.g. file:/usr/lib/qt-2.3.0/doc/html/geometry.html )\n\nsay that frameGeometry() \nis including the window frame\nand geometry() is excluding it!\n\nBut\n\nkmainwindow->frameGeometry().height- kmainwindow->geometry().height == 0\n\n... frameGeometry() doesn't seem to include the window frame :(("
    author: "?"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-10
    body: "frameGeometry() is a QFrame (== client side) thing and has nothing to do with the frame drawn\nby the window manager.\n\nThere is no way for an application to find out\nthe window manager frame geometry."
    author: "Bernd Gehrmann"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-12
    body: "That's not true... \"xwininfo -frame\" will tell you."
    author: "AC"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-12
    body: "That's unreliable. It depends on how the window\nmanager reparents windows. There are window\nmanagers that create two nested windows and\nreparent the client window into the inner one.\nIn that case you will get a wrong result."
    author: "Bernd Gehrmann"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-09
    body: "i really look forward to see the new version :)"
    author: "Kyle"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-09
    body: "What about RAD and CodeInsight?\nIs it possible to make KDevelop fully RAD like Delphi while not requiring the RAD module.\nWill there be a feature like Delphi's CodeInsight?\nThose two are my most wanted features.\n\nAnd will there be tabbed MDI support?\nIn the current version, I need to click on the Window menu to be able to switch to another open document, which I find very annoying.\nA tabbed solution like GnomeMDI would be much better."
    author: "dc"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-09
    body: "IMO, GnomeMDI isn't the answer to all ui\nquestions. It is designed for applications that\nhave only one type of document, view resp. In\nan IDE you want to browse source code and \ndocumentation at the same time, or you want to\nwork with two source code windows side by side.\nAlso, tabs become unusable when there is a large\nnumber of them.\n\nOTOH, KDevelop's user interface will certainly\nbecome more flexible in the future."
    author: "Bernd Gehrmann"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-10
    body: "It's just an example.\n\nBTW, GnomeMDI for GNOME 2.0 supports Window-in-Window like QT/Winblow$ does."
    author: "dc"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-10
    body: "> BTW, GnomeMDI for GNOME 2.0 supports \n> Window-in-Window like QT/Winblow$ does.\n\nIf I have anything to do with it, that shit is being removed. It's ugly as fuck."
    author: "me"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-10
    body: "If you want to troll, go to Slashdot.\n\nAll the deprecated code is already removed from gnome-libs in CVS.\nOnly good code is left, which mean the window-in-window MDI mode (which is optional) is good code."
    author: "dc"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-10
    body: "He isn't trolling, he is fully right. Drawing\nwindow decorations is the window manager's job.\nImitating that by the application will always\nsuck. It doesn't integrate with the window\nmanager's look and feel, it uses different \nbuttons for maximizing or closing windows, and\nit doesn't adjust to the window manager's focus\npolicy."
    author: "Bernd Gehrmann"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-11
    body: "No, he isn't. The Window-in-Window mode is just one of the many modes in GnomeMDI.\nYou can also choose for the Toplevel or Tab mode.\nSo there's nothing one can complain about.\n\nBesides, QT and KDE also do the Window-in-Window thing."
    author: "dc"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-11
    body: "Somehow I get the impression that gnome advocates\ncan not read. Here it is again:\n\n>> BTW, GnomeMDI for GNOME 2.0 supports \n>> Window-in-Window like QT/Winblow$ does.\n\n>If I have anything to do with it, that shit is >being removed. It's ugly as fuck.\n\nThat's what you responded to. So you are\ntrolling, not him."
    author: "Bernd Gehrmann"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-11
    body: "I don't believe him. How does he know that that code is \"shit\" and is being removed?\nLink to mailing list archive please."
    author: "dc"
  - subject: "Some People Really Can't Read"
    date: 2001-06-11
    body: "> How does he know that that code is \"shit\" and is being removed?\n\nHe doesn't know any of those because he said neither of those! Re-read the original message again:\n\n> > If I have anything to do with it, that shit is being removed. It's ugly as fuck.\n\nHe only sais that _IF_ he was concerned he'd vote to get Window-in-Window MDI out, not that someone actually decided to do so!\n\nAlso he _NEVER_ said the code was ugly, only that the Window-in-Window MDI itself is ugly.\n\n*sigh* Please read what people say first and don't take everything personally. It would make your life so much easier..."
    author: "Martijn Klingens"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-12
    body: "How do I know it's shit?\n\nI used it.\nIt's ugly.\n\nIt may be good code, but the end results are nothing to be proud of.\n\nAnd your assertion that only \"good code\" is left in gnome-libs is just laughable."
    author: "me"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-12
    body: "Ignore the above post. It's posted by a troll who stole my nickname."
    author: "me"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-13
    body: "Oh no it wasn't"
    author: "me"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-18
    body: "Oh yes it was. Now shut up and go steal others' nicknames."
    author: "me"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-10
    body: "No MDI works well with a large number of files, all implimentations will suck becuase there is no perfect way to do it.  Also Gtk 2.0 supports Doc/View very well, Qt Doc/View is difficult."
    author: "robert"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-10
    body: "No, it does not work well with a large number of\nfiles. As soon as you have perhaps 5 files open,\nthe tabs won't fit into the window width (on a\nusual 17\" monitor) and you have to scroll. And\nscrolling a tab bar is a lot slower than choosing\na file from a popup menu.\n\nFor the other statement, I suggest you go\ntrolling on gnotices."
    author: "Bernd Gehrmann"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-11
    body: "you also can place tabs on two or more rows (like together do). that way you can access a lot of files really fast"
    author: "ik"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-11
    body: "And at the same time you fill valuable vertical screen space with your tab rows.\n\nBesides, you can reach the same effect by putting\nbuttons with all file names in a tool bar. In\nthis way, you don't restrict the user interface\nto one show one file at a time, which is what\ntabs do."
    author: "Bernd Gehrmann"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-11
    body: "i have no problem sacrificing screen space to be able to reach every file in one click (because when coding, i really have to switch a lot, and 90% of the procedures i write are really small, so i can see a whole procedure in together in an editor window that takes less than 50% of the screen (i also have an uml class view open)\nanyway ... seems we ran into a matter of taste yet again :) configurability for president !"
    author: "ik"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-11
    body: "I agree that it quite annoying to have more than one mouse click to switch to another source file. The tabbed editor in KDE Studio provides a nice solution in my opinion, but it is probably a matter of taste. The ultimate solution could be to make it configurable (I know, it seems to be the solution for everything ;-)"
    author: "Alexander Kuit"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-10
    body: "(Huuaa - my friend MDI;-)\n<br><br><\nAnother discussion is here:<br>\n<a href=\"http://linuxtoday.com/news_story.php3?ltsn=2001-06-09-008-20-PS\">http://linuxtoday.com/news_story.php3?ltsn=2001-06-09-008-20-PS</a>"
    author: "Thorsten Schnebeck"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-09
    body: "I would love to see this support palm os dev. I would do it myself but I wouldnt know where to start."
    author: "Toddler"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-09
    body: "yes, rad support would be great. look at borland kylix as an example. i think kdevelop should move in that direction."
    author: "matteo porta"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-10
    body: "Maybe you should try the designer of Qt 3.0 :-)\n\nI don't think it makes much sense to integrate\na dialog editor into KDevelop that can't\nseriously compete with Qt designer. So turning\nKDevelop into a rad environment would need an\nambitious developer with much energy :-)"
    author: "Bernd Gehrmann"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-11
    body: "As Gideon has already C support I think it would be nice to have an application template for the Palm OS (including makefile,configure...) first and then some plugins for starting the emulator ,transfer the program,GUI editor and so on. Please read the file HACKING to get an overview how to add plugins and study a small Gideon kpart. It'is not so difficult. :-) If you have problems you can subscribe to the KDevelop-Devel mailinglist."
    author: "Sandy Meier"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-09
    body: "I am very happy about Gideon, because we will be able to programe with any lenguage , as C, C++, Java, Php ...\n\nBut, the problem i see is that the documentation about new lenguages will not be created by KDE team. The QT/KDE documentation is great, one of the best docs over Linux documentation. Then, if Kdevelop support new languages, as Php for example, KDE team has to integrate the Php Manual from www.php.com, and there are lenguages that there is not a good work about their documentation.\n\nAnother thing i am thinking is why there is not a\ndocumentation about ANSI C and C++ with the documentation in Kdevelop. I mean, when you are programming, sometimes you have question about some sentence, and you have to search a book to read how it is. Then, it would be great, if in the kdevelop documentation it has a section named ANSI C-C++.\n\nAnd to finish my willings :-) , when Kdevelop will have an integrated GUI-Builder alo Borland C++ Builder? I mean, a \"glade\" integrated into Kdevelop (I dont like much more Qt-Designer because you cant code KDE or gnome aplications). Then, with this function, will be able to build big aplications, without worrying about the GUI interfaz. With this feature, will be able to programe Java aplications too.\n\nI have to give to KDE and Kdevelop team, my congratulations about you work."
    author: "Daniel"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-10
    body: "Actually, you can have Qt Desginer support KDE widgets. If you have the KDE headers installed, there's an option to add the KDE widgets to Qt Designer at compile time.\n\nYou may want to check if the packaged version that comes with the KDE distribution has that feature. My guess us yes, but it's been a time since I last checked it."
    author: "Marcelo"
  - subject: "About documentation"
    date: 2001-06-10
    body: "I'm with KDevelop 1.4 opened right now, and in the \"Books\" Tab I have access to the GNU C library documentation, STL documentation, Python doc., BONOBO docs., GTK, Glib, Quanta+, E2FSlibs, full Debian documentation, a C/C++ Reference et al. I don't know if it's a Debian \"bug\", but I can see (and even browse) these docs.\nI'm using Debian 2.2 (x86) with KDE 2.1.1 form http://kde.tdyc.com and XFree86 4.0.3 from http://people.debian.org/~cpbotha."
    author: "Claudio da Silveira Pinheiro"
  - subject: "Re: About documentation"
    date: 2001-06-10
    body: "Not a bug, but an integration of Debian's \ndocbase :-) I implemented this two years ago\nwhen I still used slink. Nice to see that it\nstill works. Unfortunately other distros don't\nhave anything similar.\n\nDocbase doesn't provide much meta info about\na documentation item though. In HEAD, you can\nsearch through the documentation index of libc \nand stl with autocompletion. For the Python,\nPerl and PHP documentation, also the table of\ncontents is directly displayed in the doc tree."
    author: "Bernd Gehrmann"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-09
    body: "I'm especially excited about the Fortran support.  Fortran is not dead and having an open source IDE for g77 will be very, very useful!\n\nAny plans for integrating g95 when it is ready?"
    author: "Anonymous"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-09
    body: "Considering the current state of g95, this\nappears like looking far into the future ;-)\n\nAnyway, writing a parser for free form Fortran\nis sigificantly more effort than for fixed\nform FORTRAN 77."
    author: "Bernd Gehrmann"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-10
    body: "One of the noticable things about the PHP (and possibly other language) docs is the user comment system. It's noticable in particular with PHP since almost all the in-code examples for the function reference are submitted by the users. It would be very nice indeed if developers, for any given release of the Gideon copy of the docs, included these coments as of that date.\n\nAlso, what about Kate? It seems suspisously similar to what Gideon is beginning to look like (Multi-doc editing, syntax highlighting, embedded konsole, etc). I'm not suggesting a merge, that would almost certainly not work well. However, what about making some sort of base \"kde style mdi editor\" framework, then using that as the basis of Kate (editing framework and hierarchial sidebar in kate would be nice), KDevelop, and, in the future, maybe even KWord (for power-user support for vim style editing in kword!)"
    author: "Carbon"
  - subject: "Sick scary news about Visual Interdev"
    date: 2001-06-10
    body: "Some people use Front Page and Visual Interdev\ntogether to create SQLServer/ASP powered websites (I don't know how they work together ActiveX OLE thingamaboobies). Any cool ways to do similar things with Gideon kpart/konqui/some html source code editor and MySQL/PHP/Zope/Python etc. ??"
    author: "sick puppy"
  - subject: "Re: Sick scary news about Visual Interdev"
    date: 2001-06-10
    body: "....some html source code editor...\nyou mean someting like Quanta+ :)"
    author: "Morty"
  - subject: "Re: Sick scary news about Visual Interdev"
    date: 2001-06-10
    body: "Interdev can do ASP + SQL Programming together. Frontpage is for the newbies while Interdev is for the real asp developers.\n\nYou would not really use them both, it's basically one or the other (based on target user).\n\nKevin"
    author: "Kevin I"
  - subject: "Re: Sick scary news about Visual Interdev"
    date: 2001-07-02
    body: "Have to respectfully disagree with you here. I build some quite large and complex websites (London Stock Exchange, jobs.telegraph.co.uk) and my team all use HomeSite :-) I use Visual Interdev, FrontPage and Homesite, because each has it's stengths and weaknesses.\n\nFrontPage has a good preview mode, InterDev ruins the pages a little less ;-> and HomeSite has the most capable text editor, but has sucky preview. It's all horses for courses really."
    author: "Ivan Hawkes"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-11
    body: "WohohohohohohohoW!!!\nThis rox man. You kde people never seize to amaze me. Why even bother to use windows? (rethorical question, everyone knows that the answer is \"no reason\")"
    author: "ddd"
  - subject: "And Basic ?"
    date: 2001-06-11
    body: "I see Perl, Python, Fortran, but nothing about Basic...\n\nWhy ?\n\nHow is going KBasic ?\n\nHas KBasic some interactions with Gideon ?\n\nIs really KBasic the only solution inside KDE to nearly program in Basic ?\n\nOr is existing something else like to translate Basic in Python, for example ? (not only about VB programs, but also about MS Office VBA programs...)"
    author: "Alain"
  - subject: "KBasic"
    date: 2001-06-11
    body: "You can follow the KBasic progress at <a href=\"http://www.kbasic.org/\">http://www.kbasic.org/</a>.\n<p>\n-N."
    author: "Navindra Umanee"
  - subject: "Re: KBasic"
    date: 2001-06-11
    body: "Thank you. But I know this site from several months, and I am surprised to see, today, that Fortran is suddendly coming, while nothing seems changing about KBasic... Always the same old screenshots... Although, yes, it seems there are some progress... many more slower than Gideon-Fortran...\n\nI am wondering whether devopping Basic inside Gideon would not be better ??...\n\nAlso, there is PyKDE for Python. Is'nt it in duplication with the Python of Gideon ?\n\nBut, certainly, there are many things I don't understand..."
    author: "Alain"
  - subject: "Re: KBasic"
    date: 2001-06-11
    body: "No. PyKDE is a library that allows you to access\nthe KDE API from a program written in Python.\nKDevelop is an IDE. You can write programs using\nPyKDE with the help of KDevelop. I think it'll\nmake a good combination :-)"
    author: "Bernd Gehrmann"
  - subject: "Re: KBasic"
    date: 2001-06-11
    body: ">Also, there is PyKDE for Python. Is'nt it in duplication with the Python of Gideon ?\n\nNot really. PyKDE/PyQT allows _any_ python app, written in kdevelop or not, to create and use QT/KDE widgets, signals/slots, etc.\n\nHowever, python support in kdevelop allows you to write python apps inside kdevelop, whether or not said apps use PyQT/PyKDE is up to the developer. The point is, you could and still can use kdevelop to develop non-KDE or KDE apps, and now you can do this with other languages then C++."
    author: "Carbon"
  - subject: "Re: KBasic"
    date: 2001-06-11
    body: "Thank you, I understand about Python. And I see that KBasic is not similar to PyKDE, but is similar to Gideon/Python.\n\nSo, again, I don't understand about Basic. It is now possible to write Fortran, Perl and Python apps inside Gideon, and quickly. Why not Basic ?\n\nIsn't is a better way than the slow KBasic ?\n\nPerhaps my question is disturbing, sorry..."
    author: "Alain"
  - subject: "Re: KBasic"
    date: 2001-06-11
    body: "I really wasn't saying anything about KBasic in my post above, I was just blabbering about Python. I haven't used KBasic myself, and there's a reason for that : the Basic language itself is sucky. I have yet to seen it done properly, and the closest it ever came to being useful, Visual Basic, was so utterly proprietary, and the IDE was so utterly useless, it was silly.\n\nBasic was designed as an introductory language before moving on to something more powerful and useful. In much the same way as the Athera widget set, it was taken beyond this model and people attempted to use it for useful stuff. The result was horrendously difficult to read and just plain stinky.\n\nI guess my point is, if you want a language that's easy to learn and still useful, including GUI stuff, try (in the near future :-) KDevelop/Python/PyKDE. Attempting to have people write useful progs or even \"Hello World\" progs with Basic will just give them bad programming habits. Believe me, I started out on Visual Basic, and the minor inconsistencies with every other language out there (no semicolon at end of statement, '=' having two meanings) wreaked havoc for me . When I started out on PHP, I was constantly and annoyingly assigning variables instead of comparing them in 'if' clauses.\n\nYour question isn't disturbing though! Believe me, although Basic specifically is bad, it's always good to have a simplistic, easy-to-learn, RAD-oriented programming language, as long as it's not too proprietary. But perhaps this would really be more suited toward (dun dun dun) Gideon/Python/PyKDE! :-)"
    author: "Carbon"
  - subject: "Basic and KDE : what policy ?"
    date: 2001-06-14
    body: "When I wrote my last posts, T was hesitating to say something like :\n\nPlease, I only want to have answers about my questions, please don't answer something like :\n> the Basic language itself is sucky\n> Basic will just give them bad programming habits\n> Basic specifically is bad\nand so and so...\n\nHmmm....\n\nI don't want to answer about such things. Only I agree with the page \"\"Why is KBasic useful for the Linux community ?\" :  http://www.kbasic.org/1/manifesto.php3\n\nI try an other way for my question :\n\nGnome has a policy about Basic (it is GBasic and screenshots show it is advanced). What is the policy of KDE ? Let the Basic only out of Gideon and the other languages (even some few used as fortran), or let work together Gideon and KBasic ?"
    author: "Alain"
  - subject: "Re: Basic and KDE : what policy ?"
    date: 2001-06-14
    body: "Well, that really was my answer : KDE's policy on Basic should be : \"No Basic allowed\".\n\nOn the other hand, this is only my opinion. If the majority of the KDE community disagrees with me, then I will bow (grumbling and griping all the way, but still bowing :-) to their vote. Bazaar development is never about a huge group of people, such as the large and wide-spread KDE development team, obeying the commands of only one person.\n\nAlso, please don't take my posts as a flame against KBasic. I'm absolutely fine with the objectives of KBasic, as long as it's not (IMHO) a language that shouldn't even exist in the first place. Moreover, I would be delighted if the KBasic team proved me wrong and built a Basic-oriented programming tool that knocked everyone's socks off I'm just highly doubtful that this can be done with Basic. Here's why :\n\n1) There is not currently a significant existing Basic codebase, at least there isn't that I've ever heard of. Most Basic apps are either based on a proprietry, ultra-closed extension of Basic (i.e. Visual Basic), or are (for the most part) really, really old console apps. If I am wrong (and I might very well be) someone supply a URL to some sort of Open Source Basic-based project repository.\n\n2) Basic teaches bad programming habits : Evil, old-fashioned things like GOTOs, which should never be used, and also auto-sensing of certain syntaxes to the point where certain methods become useless (such as what I mentioned earlier about the = sign).\n\n3) There are better alternatives. I am aware of several languages that are much cleaner then Basic, but still easier to learn. For instance, Ruby or PHP (which has no KDE bindings currently, but it might be worth looking into)."
    author: "Carbon"
  - subject: "Re: Basic and KDE : what policy ?"
    date: 2001-06-14
    body: "Mr Ayatollah said :\n> KDE's policy on Basic should be : \"No Basic allowed\"\n\nNow it is a bad answer, because KBasic exists. \n\nWhy is KBasic useful for the Linux community ? : http://www.kbasic.org/1/manifesto.php3\n\nMy question was :\n> > What is the policy of KDE ? Let the Basic only out of Gideon and the other languages (even some few used as fortran), or let work together Gideon and KBasic ?\n\nAnd I add :\nAre some contacts existing between the Gideon team and the KBasic team ?\n\nIf no, I hope that such a communication will be open..."
    author: "Alain"
  - subject: "Re: Basic and KDE : what policy ?"
    date: 2001-06-15
    body: "No, you misunderstand : I'm totally for KBasic, and I'm totally for it's objective of easy RAD. It's just that the language Basic, is frankly awful!\n\nI have read that manifesto, and (please don't take this as a troll, it's just my personal opinion) English-like _takes away_ from the usability of a programming language. First of all, not everyone knows English, and making use of a programming language totally or almsost totally dependent upon knowledge of a certain spoken language is restrictive to those who don't know that language. \n\nMoreover, English is not a good language for logical constructs. The point at which someone can write code by simply typing in, in their native language, what they are thinking, it is no longer programming, it's automagical. It may be possible, but it certainly isn't with Basic in it's current form, because right now Basic isn't even close to a direct translation of English into machine-compilable form. The manifesto seems to imply this, but this is not true, Basic follows very specific syntax rules, just like any other language.\n\nAlso, (again IMHO) Basic teaching bad programming habits is not a non-issue. Even if you never use another langauge then Basic, it still makes it incredibly difficult to debug your code. Bad programming habits are not entirely about optimizations, as seems to be implied by the manifesto. Rather, it's about writing clean code that _makes sense_, not just to the compiler but to the writer 3 months later, and to other people wishing to understand the code.\n\nWhat I would suggest for an easy language is (yeah, I've said this before, but I sure do enjoy driving my point home! :-) PHP. PHP is nice in that it borrows from what's good about languages such as C, perl, etc., but avoids low-level stuff as much as possible. PHP is already capable of a huge amount, it's a well established language that already has an inceredible userbase in the Open Source community. It's currently the most popular module on the most popular web server in the world! \n\nFinally, I really wasn't trying to answer your question about Gideon/KBasic compatibality :-). I was just stating my personal take on the whole topic of Basic."
    author: "Carbon"
  - subject: "Re: Basic and KDE : what policy ?"
    date: 2001-08-06
    body: "Basic Coding != Bad Programming Practice\n\nIt's a myth these days -- the language provides a GOTO..it doesn't force you to use it.  :)"
    author: "Damo"
  - subject: "Re: Basic and KDE : what policy ?"
    date: 2004-10-08
    body: "The \"Goto problem\" is a wrong problem : Other languages have it, including C.\n\nAnyway, everyone should be free to use the language they like without being put in ghettos...\n\nHave a nice week-end."
    author: "Derek"
  - subject: "Re: Basic and KDE : what policy ?"
    date: 2003-07-29
    body: "Looking down your nose at basic is exactly why Linux is not more successful.   Most windows apps are developed with basic.   Enabling this ability for Linux would kick off the biggest development spurt in the history of development spurts.  It is petty and short sighted to take the attitude that basic is inferior.  Make it superior.   Bring those millions who develop things for work they do that keeps them using bill's junk.   All the time I have to develop small apps to control some device or make some computation etc.  I do this in vb because it is simple and quick.   I have tried to make sense out of the socket wrench set plethora of compilers and environments.   I don't have a life time to try and make something work.   vb works now and fast and uses the c compiler so it executes plenty fast.   Or you could just continue on believing the world will adopt what ever you decree.   ms works because they try to fill markets like the one you are ignoring."
    author: "Ben"
  - subject: "Re: Basic and KDE : what policy ?"
    date: 2003-07-29
    body: "Is this because Basic is such a brilliant language, or because Basic is just one out of many easy to use languages and the one Microsoft decided to use for its RAD tool?\n"
    author: "AC"
  - subject: "Re: Basic and KDE : what policy ?"
    date: 2003-07-31
    body: "It is because so many people/companies use it to develop real world applications.  There is a large population of developers just waiting for this to happen.  I don't know anyone these days that loves ms or wouldn't like to see Linux be successful. Linux developers have been so long concerned with the operating system, desktop and basic office tools that I don't think they are aware of the real world apps developed in vb.  I am in radio and all the database software, which we use to analyze ratings, do traffic logs, accounting etc., are all developed with vb as are most other special purpose softwares.  I write engineering programs in vb that are otherwise unavailable to me.  While there are probably legitimate arguments regarding quality of these programs, there is the reality that vb brings the possibility of programming to professionals in disciplines other than programming, where their expertise is important.   Gbasic set out to develop a vb compatible tool.   Assuming it would work, that would instantly make available a huge number of programs.   It, like all the others, seems to be dead in the water.   Kbasic is dormant.   Ybasic almost works has years to go.  All these people are working alone and can\u0092t seem to get together.  Python with a rad environment like boaconstructor is the one most often put up as a replacement.   I am learning that now but I don\u0092t believe most people would go through what is necessary to find/install all the necessary programs then learn a completely new language.   Can it really be that hard?  Are Linux developers just biased against it?  Reminds me of the old saying \"cutting off your nose to spite your face\".   In other words somewhat self defeating.  Someone explain this to me please."
    author: "Ben"
  - subject: "Re: KBasic"
    date: 2004-10-03
    body: "I have to disagree with the \"Basic is bad\" generalisation. Bad for whom? Bad for what?\n\nRemember that BASIC started out as an 8K ROM on computers which had from 16 to 64 KB RAM and ran at 1MHz. I suspect that a lot of the initial features were dictated by these limitations. Since then the language has developed out of all recognition. I personally started with Sinclair (Timex in the US) Basic and worked up through IBM Basic, PBasic, CBasic under CP/M, TurboBasic and the Visual Basic from VB3 upwards. With every move (over 25 years) I have been able to port programs I wanted to.\n\nToday, under Windows, I use VB6 and VBA in the MS Office suite. I can use MS Access or VB6 to access MySQL amd MS SQL Server databases with virtually no limitations. \"Dave, we need to track the value of XXXX every minute and produce graphs accesible only by Production Managers\" generally results in 10 minutes work on the Control System, 20 minutes on the Data Logging app (written in VB6) and another 20 in MS Access to produce the client module.\n\nUnfortunately this type of functionality seems not to exist currently under Linux - AND I DESPERATELY WANT IT TO! There must be a whole horde of people out there with similar needs to mine. An elitist \"who needs BASIC anyway\" mindset is only going to slow down the takeup of Linux.\n\nTake OpenOffice. They dumped BASIC in favour of a different language. It has made the migration from MS Office to OpenOffice EXTREMELY difficult. Yes you can migrate, but at the expense of dumping all the macros and custom code one had in MS Office. In other words, desirable as OpenOffice (and StarOffice) is, there is a serious hurdle to overcome in migrating. To my mind the problem is not \"retraining\" users - this is mostly necessary to stop the whining of \"It's so different and not powerful enough for my needs\" - but the rewriting of all the custom stuff in what appears to be a cumbersome and convoluted language, which is intended to be an \"improvement\" on BASIC.\n\nMy view: forget the linguistics, the purism and the elitist views. BASIC has worked well for years and is familiar to LOTS of people. You don't have to use it if you don't want to. On the other hand having it available will make Linux a lot more attractive to prospective migrants. Don't get me wrong, I prefer Linux to Windows and OpenOffice to MS Office but the lack of BASIC is a major handicap.\n"
    author: "davcefai"
  - subject: "Re: KBasic"
    date: 2001-06-11
    body: ">Always the same old screenshots... \n\nTo me it looks like they are concentrating on the core language functionality right now, the interpreter, not the GUI tools such as the form designer.  So there isn't anything new to take screenshots of.  Althogh there doesn't seem to be much development of any kind going on anymore...\n\nThe reason Fortran is being added to Gideon and KBasic is not is that Fortran is a complete language and KBasic is still in the pre-alpha stages.  When (if?) KBasic matures it could be added to Gideon, but won't it have its own integrated IDE and form designer?\n\n>Also, there is PyKDE for Python. Is'nt it in duplication with the Python of Gideon ?\n\nYou seem to be confusing the language binding with the IDE.  PyKDE is a language binding, a set of Python functions to access KDE features.  Gideon's Python support is just a set of nice features in Gideon's editor and project manager to make it easier to edit python files and projects."
    author: "not me"
  - subject: "KBasic - fine by me!"
    date: 2005-01-27
    body: "BASIC is a programming language; assuming use of a reasonably good version thereof, what makes a program good or bad is the programmer, not the language used. A bad programmer will generally find a way to write badly whatever the language they use to program in.  True, some languages attempt to force good practice, but a truly (un)talented bad programmer will still be able to write bad code in them. Whereas a good programmer may be prevented from creating something uniquely wonderful by constraints imposed purely to try to force some particular style or methodology of coding. \n\nAlso, equating MS Visual BASIC with the BASIC I learnt on many moons ago seems a bit like comparing chalk and cheese to me. I found VB got in the way of letting one actually code so much that it infuriated me to the point where I simply gave up before ever having created anything of use in it! I well recall arguments way back for and against the strong typing of variables etc, and which language was \"better\" (BASIC, Pascal, FORTRAN, COBOL, Lisp, FORTH...). IMHO, it's to some extent a matter of personal preference, and to some extent a matter of suitability for the purpose at hand. \n\nFor simply giving those new to the whole idea of programming an idea of the basic concepts, old-time BASIC is wonderful. And it has its uses (I still have an old Sharp PC1211 handheld with 1.4k of RAM for BASIC programs, woo-hoo! Still use it now and then, too).  Anyone wanting or needing what that kind of BASIC cannot deal well with (or at all) will look for another language that will do the trick. I know I will (I dream of one day giving the Gimp a more familiar front-end, if someone else doesnt do it first!)\n\nPersonally, after a gap of many years away and some events that badly hurt my confidence, I want to get back into programming, and I'd welcome something like a good old-fashioned BASIC for Linux, to help shake the rust off and rebuild my confidence. I know I'll probably want to use Perl and Python in the long run, but I havent the time to muck about learning a totally new language at the moment. \n\nI look forward to seeing KBASIC, and hope to see FORTH and LISP one day, too!\n\nEsme\n"
    author: "Esme"
  - subject: "Basic and Linux"
    date: 2006-06-02
    body: "To say basic is sloppy, well some are, but look @ RealBASIC. Really, take a good look at it. It is more structured then any other basic aside from ASIC (very old school, nevermind) and it is just a tip of the iceburg. A *BIG* reason Windoze beat OS/2 & Geos was that you could quickly and simply design applications with Visual Basic. OS/2 used Rexx and C/Pascal and Geos used C and an API only. Funniest thing is, NT kernel all the way until 4 had the \"OS/2\" string in it, and OS/2 Warp 4 was by far the most advanced O/S on the planet when it came out but it was not easy to program for, and died.\n\nThe first person who makes a viable COMPILED basic for Linux will usher in the next Operating system standard and make a ton of money. I know, I watched computers since they 8K. 8192 bytes. total. Over 65 Thousand TIMES less then what is standard today/ :)"
    author: "Eric"
  - subject: "Re: KBasic"
    date: 2006-06-18
    body: "Having looked at the KBasic website just now, I would have two points to make:\n\nFirst, looking at the syntax KBasic supports made me cringe.  I firmly believe that BASIC in any carnation is bound to be evil.  (I would certainly suggest using Python as your first language! or one of the many of the other available scripting languages besides BASIC.)\n\nSecond, this doesn't matter, because (from what I've seen, at least) KBasic is out to liberate Visual Basic users, rather than provide a useful language.  They certainly aren't out to win over Python, PHP or Perl users!  In this sense, KBasic is the most wonderful thing available to those stuck with lots of VB code!  If successful, those who programmed in VB will be able to get away from it, and even port their code to an alternative OS.  This is very good.\n\nPerhaps those making the transition will someday buckle down and learn a new language, like Python.  If they do so, however, they can make the transition smoothly, still being productive in KBasic while learning a real language like Python.\n\nSo KBasic certainly has my stamp of approval, whatever that is worth!\n"
    author: "Alphy"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-11
    body: "CodeInsight anyone?"
    author: "dc"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-11
    body: "Maybe one day emacs embedded in KDevelop with the editor framework?\nI'd love to have this ! =)"
    author: "Michael Pujos"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-15
    body: "Yeah, I like emacs myself, but I can't help but wonder just how much more bloated a Kdevelop + emacs distribution would be (two separate installs, tons of disk space needed =).  It'd probably be easier to integrate XEmacs though, because it was designed to be embedded into X applications."
    author: "Logan Lewis"
  - subject: "What's wrong with MDI?"
    date: 2001-06-11
    body: "Most of the posts here are quite negative towards MDI. Why is that? I think it is probably the best \"invention\" yet.\n\nI hate being able to view just one editor pane at once. So, please, keep the MDI-stuff in Gideon. At least as an option, that would make me happy. (Window splitters aren't that bad either, so keep them too. ;)"
    author: "J\u00f6rgen S"
  - subject: "Re: What's wrong with MDI?"
    date: 2001-06-11
    body: "Window-in-window MDI is a pretty flamewar-magnetized issue, mainly because most newbies are confused by it. This is something I can attest to personally, as I've seen time and time again people wondering why, when they minimize their document windows in M$ Orifice, they can't find them on the taskbar to restore them.\n\nAlso, it's important to note that MDI does not neccesarialy imply window-in-window. There are other mehods, like the frame splitters you mentioned, also konsole style where you can have several documents, but only view one, and also tabbed style, plus probably many others I've never heard of. \n\nI'd be more inclined to go for an enchanced frame style, where you don't lose any window space for documents, and that still makes it easy to organize frames.\n\nAlso, what would really be nice is the kicker taskbar menus that were recently implemented (another article on the Dot talks about this, can someone provide a link?). How about a list of MDI documents in each window displayed on the menu of any MDI app, as well as the system currently in place just for KDE-noncompat or SDI apps?"
    author: "Carbon"
  - subject: "Re: What's wrong with MDI?"
    date: 2001-06-11
    body: "Absolutely!!!\n\nGrats to the Whole KDevelop team!"
    author: "Sangohn Christian"
  - subject: "Re: What's wrong with MDI?"
    date: 2001-06-11
    body: "MDI is a word with different definitions. Often its interpreted as a handling of multiple docs in a way you can see since Win3.1 and MS-Office. Others called tabbed views, docked windows or embedded docs MDI, they are right, too - all this is a kind of a M_ultiple D_ocument I_nterface.\nMy personal friend (*ironic*) is the first MDI-paradigma. I think windows in window (win-in-win) does not fit in a virtual desktop system like x-windows, here KDE. Here we don't have a single desktop like M$-Win, we are used to organize our work on many screens and are used to have a powerful global taskbar/pager to navigate. The (I call it) classic MDI does not fit this X paradigma. The win-in-win-problems can you see also with current KOffice-KPart: in an extra windows I could edit much better, than now in an active KOffice-KPart.  Another problem is the using of classic MDI-windows as application toolbox. This does not work well as KPart, too. You are not able to use this toolbox outside the KPart-frame (see: krayon-frame in kword)\n\nOn the other hand, there are realy good examples of intelligent MDI: Konqi split-views, KDevelop-1.4-CVS (very flexible: tab-view, tear-off and of course classic MDI), Konsole (easy but useful), Kate fast file selector and so on. But PLEASE, don't use classic MDI."
    author: "Thorsten Schnebeck"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-11
    body: "Why dont someone implement a class for Gideon that make code autocompletion ? I mean, while you are coding, if a sentence belongs to the language sentence, then, the kdevelop autocomplete the sentence.\n\nIf i am writting for example:\" fo ...\" then, kdevelop has to writte\"for _ = _ to _ step _\" where _ is a space. Or put the {} automatic.\n\nOr another idea is to integrate a sintaxis corrector (i dont remember the name of the program).\n\nWhat do you think about these ideas?"
    author: "Daniel"
  - subject: "Gideon has Objective-C support too!"
    date: 2001-06-11
    body: "It's a little known fact that Gideon has Objective-C support - class browser, syntax highlighting. If you use a 'patched for Objective-C' version of gdb ,it works fine with John Birch's gdb front end. But it still needs a GNUStep, and 'Kangaroo' (forthcoming KDE Objective-C api) project templates.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "What ever happened to Quanta?"
    date: 2001-06-11
    body: "I know they're not for the same thing, but this story reminded me that Quanta has gone w/o updates for a very long time. It's one of my favorite KDE apps. Anyone know if development will continue?"
    author: "Lenny"
  - subject: "Re: What ever happened to Quanta?"
    date: 2001-06-11
    body: "Its in kde CVS, it compiles very well with current KDE-CVS but I don't see so much new features comparing to the last stable release. There are some UI-improvements."
    author: "Thorsten Schnebeck"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-12
    body: "Supports Gideon code completition ? It's fine thing for sclerotic people like me... Espetially for me it is more important tool than class browser."
    author: "Petr Husak"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-12
    body: "\"Furthermore, even KDevelop plugins can now be written in Java.\"\n\nI'am not sure it is a good idea. Java is slow, the JVM is memory-hungry. Java is an excellent\nsolution to slow down a 1Ghz computer to a 100Mhz one, and a good vector for multi-platform viruses.\nTherefore, my question is : Do we really need Java plugins in KDevelop ?"
    author: "Alain Gibaud"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-12
    body: "IMO yes. 1) it demonstrate whats possible with current KDE desktop technologies 2) some developer don't like C/C++ 3) you can use the complete available Javasourcecode (Java Debug Architecture)"
    author: "Sandy Meier"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-12
    body: "4) you don't have to use it if you don't like it\n:-)"
    author: "Bernd Gehrmann"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-13
    body: "\"IMO yes. 1) it demonstrate whats possible with current KDE desktop technologies 2) some developer don't like C/C++ 3) you can use the complete available Javasourcecode (Java Debug Architecture)\"\n\"4) you don't have to use it if you don't like it\"\n\n1) Is KDevelop a demonstration program for KDE\nor a useful application ? A feature have to be\nuseful (good functionality/costs ratio) and not only \"possible to implement\".\n2) Sure!  Sun (and others) propose some very nice development environments for them.\n\n4) Yes, but maybe I have to pay for it\n(see http://dot.kde.org/992083107/992372437/992420887/ )"
    author: "Alain Gibaud"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-13
    body: "\"Is KDevelop a demonstration program for KDE\nor a useful application ? A feature have to be\nuseful (good functionality/costs ratio) and not only \"possible to implement\" \"\n\nYes, features should be useful, but please don't forget that this is a hobby project. I I think if someone wants to implement a \"strange\" feature or technology demonstration its ok if it doesn't break the application completely.\n\n\"Sure! Sun (and others) propose some very nice development environments for them.\"\n\nI disagree. They have IDE's but I know many people who would really like a fast and easy to use IDE for Java. Under Linux JBuilder,Forte... are _slow_, even on a GHz computer with hundreds of Megabytes RAM. :-("
    author: "Sandy Meier"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-13
    body: "\" think if someone wants to implement a \"strange\" feature or technology demonstration its ok if it doesn't break the application completely.\"\n\nOk, Ok, I am not a policeman too :-)\n\n\"They have IDE's but I know many people who would really like a fast and easy to use IDE for Java. Under Linux JBuilder,Forte... are _slow_, even on a GHz computer with hundreds of Megabytes RAM. :-(\"\n\nThis is THE recurrent problem with Java. \nIt works properly, as long as everything around it (IDE, various software layers, etc..) is *not* written in Java. Java needs C/C++ code, but as a C++ user/writer, I don't want to be slow-down by a (maybe) poorly designed and (certainly) misused language."
    author: "Alain Gibaud"
  - subject: "What's wrong with making Java play well with KDE?"
    date: 2001-06-14
    body: "\"I don't want to be slow-down by a (maybe) poorly designed and (certainly) misused language.\" - you mean C++ of course :-)?\n\nThe Java runtime is not inefficient - server side Java works very well. But Swing is a dog as you correctly point out.\n\nThe KDE Java bindings ignore the conventional wisdom you talk about - I don't care how much C++ is involved as long as I end up with a useful and efficient Java api to KDE.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-13
    body: "1) There is no conflict between demonstrating KDE\ntechnologies and beeing a useful application.\nFor many people, Konqueror is the most useful\nKDE application, and it is essentially a quite\nthin layer on KParts, KIO and KHTML which can be\nused in other applications as well.\n2) And others propose nice desktop environments,\nnice IDEs and nice web browsers as well, So let's\nstop KDE development!\n4) No, you only pay for what you want, nothing more. That's the whole point of the modularization in KDevelop's HEAD branch."
    author: "Bernd Gehrmann"
  - subject: "Is KDevelop a demonstration program for KDE?"
    date: 2001-06-14
    body: "Yes, this is a demonstration of 'Internet enabled KParts'. But it wasn't written for that purpose - the requirement was for a useful api to write the KDevelop Java debugger. That side of the deliverables wasn't experimental.\n\nA smiley followed the comment about Java slowness here (start up time for the initial Java part is the only real problem): \nhttp://dot.kde.org/992083107/992372437/992420887/\n\nGideon is much more efficient compared with the Java plugin architecture for NetBeans.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Do we really need Java plugins in KDevelop?"
    date: 2001-06-13
    body: "Good question, well put! I wrote this in a recent email to Bernd:\n\n\"But now, as it (javasupport part) is written in Java, it uses 5 Mb more memory, takes 8 seconds to start the javasupport module, and is harder to\nconfigure and slower to run...\" ;-)\n\nI wrote the Java plugin api mainly to allow an easy interface to Sun's Java debugger api. There were some technical problems with the approach of piping commands to jdb, in the way that the KDevelop gdb front end works.\n\nOn the other hand, here is my explanation I gave to Bernd of what I thought was interesting about Java KParts:\n\n\"Here is the particular code snippet that I find 'exciting':\n\n urlLoaderClass = env->FindClass(\"kdevjavapluginapi\");\n if (urlLoaderClass == 0) {\n         return;\n }\n\n addURLmethod = env->GetStaticMethodID(urlLoaderClass, \"addURLString\", \"(Ljava/lang/String;)V\");\n if (addURLmethod == 0) {\n         kdDebug(9013) << \"addURLString(): no such method\" << endl;\n         return;\n }\n\n classPath = \"file:\";\n classPath += instance()->dirs()->findResourceDir(\"data\", \"kdev$APPNAMELC$/kdev$APPNAMELC$.jar\");\n classPath += \"kdev$APPNAMELC$/kdev$APPNAMELC$.jar\";\n\n env->CallStaticVoidMethod(      urlLoaderClass,\n                                                         addURLmethod,\n                                                         env->NewStringUTF((const char *) classPath) );\n\n cls = env->FindClass(\"$APPNAME$Factory\");\n if (cls == 0) {\n         return;\n }\n\nThis is from the initialisation code of the Java part project template. It is\nusing the KInstance/KStandardDirs mechanism to locate a Java resource, a '.jar'\nfile. Then it builds up a 'file:...' type URL for that resource and passes it to\nthe Java class loader. When 'env->FindClass(\"$APPNAME$Factory\");' function is\ninvoked, the JVM looks for the class and loads it. If the URL had been an\n'http:..' style URL the Java part would have been loaded from across the\ninternet\n\nI hadn't been aiming to include this sort of capability, just wanting a decent\napi to use to write the debugger, which I was 'half excited' about. But\nwhen I realised the possibility of internet enabled KParts, I didn't do much\nelse that week until I managed to get it working. On the other hand, I haven't\nreally thought of a use for it yet - someone else might.\"\n\nI would say the jury is out :-)..\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Gideon Development Update"
    date: 2001-06-21
    body: "You have actually heard of native compiled Java, haven't you?  Or is GCC development something that happens to other people?"
    author: "Tsujigiri"
---
Two months ago, we announced the birth of <a href="http://dot.kde.org/986594487/">Gideon</a>, codename for the next generation version of <a href="http://www.kdevelop.org/">KDevelop</a> that was most notable for its modularity and extensibility.  Since then, Gideon has made enormous strides -- not the least of which includes Java, Perl, Python, PHP and Fortran support, full Python scripting, and an editor framework that will allow one to plug in a favourite editor. Furthermore, thanks to the remarkable efforts of hacker <A href="mailto:Richard_Dale@tipitina.demon.co.uk">Richard Dale</a>, KDevelop plugins can now be <i>developed in Java</i>.  Read on for the full update from <a href="mailto:bernd@physik.hu-berlin.de">Bernd Gehrmann</a> including screenshots and download link.
<!--break-->
<br><br>
<i>Bernd Gehrmann writes:</i>
<br><br>
It's been two months since KDevelop's HEAD branch has had a story
on the dot. In this time, a lot of functionality has been added, so
I thought I'd provide a little update. :-)

<ul>
<li>
  <b>Java support.</b> Java programs can now be maintained with automake-based
  project management. Classes are displayed in the class view and
  class browser. The application wizard includes a template for Java
  programs based on the Qt/KDE API and using Richard Dale's
  <a href="http://dot.kde.org/983311036">Koala framework</a>.
  Furthermore, even KDevelop plugins can now be written in Java. (sic!)
  This may, for example, be used in the future for a debugger based on the
  <a href="http://java.sun.com/j2se/1.3/docs/guide/jpda/">Java Platform Debugger Architecture</a>.
<br><br>
<a href="ftp://ftp.kdevelop.org/incoming/gideon_java.png">Screenshot: Debugging the Qt scribble example program in Java.</a>
<br><br>
<li>
  <b>Perl, Python and PHP support.</b> Functions and classes are parsed and
  available in the class view. The documentation TOCs
  are integrated into the documentation tree with the
  documentation index being (partially) searchable. Python docstrings can be browsed dynamically
 through an ioslave based on pydoc. Work is underway to allow the
  configuration of php.ini and the usage of the PHP debugger.
<br><br>
<a href="ftp://ftp.kdevelop.org/incoming/gideon_python.png">Screenshot: 
  Editing the Sketch program</a>.
<br><br>
<li>
  <b>Fortran support.</b> This includes a frontend for
  <a href="http://www.dsm.fordham.edu/~ftnchek/">ftnchek</a>
  and compiler options, dialogs for g77 and the
  <a href="http://www.pgroup.com/prodworkpghpf.htm">Portland Group compilers</a>.
<br><br>
  <a href="ftp://ftp.kdevelop.org/incoming/gideon_fortran.png">Screenshot of   Fortran support</a>.
<br><br>
<li>
  <b>A new and flexible editor framework</b>, not yet fully implemented. This will allow
one to plug in other editors, even when they have a different feature set
  than KWrite or use  different UI paradigms.
<br><br>
  <a href="ftp://ftp.kdevelop.org/incoming/gideon_cpp.png">Screenshot:
  Editing KDevelop</a>.
<br><br>
<li>
  <b>A port of the gdb debugger frontend</b> from the current KDevelop.
<br><br>
<li>
  <b>Full scripting through Python.</b> The scripting framework needs almost no
  maintenance. The interfaces of all DCOP classes in KDevelop are
  automatically available in Python.
<br><br>
<li>
  <b>An integrated Konsole part.</b>
<br><br>
  <a href="ftp://ftp.kdevelop.org/incoming/gideon_tools.png">Screenshot
  showing the konsole part and some scripting examples</a>.
<br><br>
<li>
  <b>An integration of <a href="http://astyle.sf.net">Artistic Style</a></b>
  for indenting source files and configuring the indentation style.
<br><br>
<li>
  <b>A frontend for <a href="http://ctags.sf.net">ctags</a></b> for
  "turbo-grepping" through all identifiers in a project.
<br><br>
  <a href="ftp://ftp.kdevelop.org/incoming/gideon_tools2.png">Screenshot
  showing ctags and Artistic style integration</a>.
<br><br>

<li>
  <b>A new view</b> that allows one to view files grouped by their file type. Groups
  are configurable by the user.
<br><br>
<li>
  <b>An improved application wizard.</b> Apart from the usual KDE and Hello World
  stuff, this includes templates for  kicker applets, KControl modules,
  KIO slaves, and even GNOME applications.
 
</ul>
 
That's about the main points I recall off the top of my "HEAD". ;-) Of course,
many many details have been improved or newly implemented,
such as, for example, a Tools menu, an overhauled and much more user-friendly
project management interface, automatic loading of the previous project at
startup, an improved compiler frontend, etc.
<p> 
A source snapshot is available
<a href="ftp://ftp.kdevelop.org/incoming/gideon-0.2.tar.gz">here</a>.
 
<p>
 
There are still many areas where we could use more developers, whether to add
features,  complete existing ones, or to  polish the user interfaces. Please don't hesitate to
<a href="http://www.kdevelop.org/index.html?filename=mailinglist.html">get involved!<a>