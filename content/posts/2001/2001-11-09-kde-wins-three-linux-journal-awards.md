---
title: "KDE Wins three Linux Journal Awards"
date:    2001-11-09
authors:
  - "ostrutynski"
slug:    kde-wins-three-linux-journal-awards
comments:
  - subject: "Schweet"
    date: 2001-11-09
    body: "That totally kicks ass.  Way to go guys/girls/seemingly non-human things."
    author: "Adam Black"
  - subject: "keep coding"
    date: 2001-11-09
    body: "Congrats."
    author: "KDE's FAN"
  - subject: "Re: keep coding"
    date: 2002-06-04
    body: "I need code of ASP to attach a file with email."
    author: "Saeed"
  - subject: "Testing"
    date: 2003-02-25
    body: "this is a test message from Babu"
    author: "babu"
  - subject: "Re: Testing"
    date: 2003-05-02
    body: "Hi, This is a test message"
    author: "Babu"
  - subject: "Admirer"
    date: 2001-11-09
    body: "Congratulations, KDE."
    author: "Govind"
  - subject: "Re: Admirer reawakening"
    date: 2004-09-10
    body: "realives what go out with me who wants a kiss good couples and for prefect each others closerest together you need go to relives spa treatmnet haves measse tables maybe sauna even hot whrilpool bring your boyfriends with you nice great candlelight dinner also releases gazillionths gaplus flamingoes does ortachs goes orcas to orcsa coming into the airs earth nature howling trees hooting  howling to night moons by walking peace quit beaches breezy windy chills to cool howling reawakening. chesterton, In to porter,In to portage, In to valaporiso, In"
    author: "amonyous"
  - subject: "Phil Hughes was right"
    date: 2001-11-09
    body: "In May last year he asked the question which they now have answered:\nhttp://www2.linuxjournal.com/lj-issues/issue66/3648.html"
    author: "reihal"
  - subject: "What a clueless person!"
    date: 2001-11-09
    body: "That article is total waste of bits. Here is an example:\n\n\"Encouraging people who aren't computer literate to think they are is as dangerous as encouraging people who don't know how to drive to think they do. In either case, we end up with clueless people out there driving, whether on the streets of Ballard or on the Internet.\"\n\n600 people die every year in driving accidents. Many thousands become disabled. (In Sweden.) And the internet is just as dangerous?\n\nThe rest of the article doesn't make any sense either."
    author: "Erik"
  - subject: "Re: What a clueless person!"
    date: 2001-11-09
    body: "As many as 50,000 people die each year in the U.S.A. in motor vehicle accidents. My sister in-law was permanently disfigured in a car accident, and I think the writer used extremely poor taste. Comparing novice Linux users to those who cause car accidents sounds like the writ of a \"Linux Elitist\". I think we need less of that sort representing the community!"
    author: "Sean Pecor"
  - subject: "Re: What a clueless person!"
    date: 2002-09-12
    body: "The fact that you whould even say that shows more then ever that linux is a TOY OS (as much as MicroSlut's stuff).  I have never met a IT pro. that I have any respect for that uses Linux in ANY production setting.  The reasons are too\nnumerious to address but basically Linux basically fails the course in part or all of the \"4 S's of IT\" (Scalablity, Stability, Responibility [when stuff goes wrong you know exactly what went wrong and why as well who the responible party is] and accessibility [the ability for autorized users to have 24/7/365 anywhere access to there data in a form that is usable]).  Use a serious OS like Solaris, HP-UX, FreeBSD (it is thru and thru mainstream UNIX with all the advantages of 30+ years of hard-lessons behind it not to mention the development model as well as the conservatism of the core team means anything that has any questionability in the above [or atleast the best theortically possible outcome] the current release will be held to it is FIXED [not some down and dirty hack but a real and perminate well tested fix]).  Also it is quiete trival (in most cases) to use all your Linux apps without recompiling. (this is how I am using Kdev)"
    author: "sarek"
  - subject: "Re: What a clueless person!"
    date: 2002-09-13
    body: "IBM is selling quite a bit of $100,000 and up installations that run Linux.\n\nLinux is starting to scrape at the serious enterprise computing market.  And I'd say it's at the point where it's capable of doing so.  Call me a *nix newbie if you want.  I've got a sheaf of certs, been using *nix since 1983, used a variant as my primary OS since Coherent was available, and I've been a profesional sysadmin for *nix systems since 1992.  I have years of experience on AIX, Solaris, and BSDi, and have contracted on a dozen other variants.  Linux is becoming quite a capable operating system, and early adopters are reporting great success.\n\nI already use SuSE on a few semi-critical servers and have had no problems.  As we need new servers, they will have Linux rather than Solaris.  Cost is a major concern - even for companies with millions of dollars.  Two years ago, I didn't even consider Linux - we went with AIX for a decent sized Oracle server for a financial company.  Now I'm comfortable to put just about anything on it - and I'd imagine in the next year or two, I'll be comfortable to move core business to it and run a Linux shop across the board.\n\nIncidently, in 95 or 96 the comany I was with went with another new OS.  It was a cheap ($9000) OS that ran on commodity intel hardware (plus RAID on a board rather than a hardware solution).  It was untested technology, they said, too unreliable.  That was BSDi, whose decendants you now vaunt as being so great.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: What a clueless person!"
    date: 2002-09-13
    body: "Correct me if I'm wrong but, nowhere is it writ in any part of the Internet, that respect from Sarek - or Sarek's advice for that matter - is of any real benefit. To me you sound less like an intelligent and well-balanced IT systems administrator, yet more like a gent who royally screwed up a Linux server and then got angry because he couldn't find someone else to blame it on...\n\nI understand Solaris, HP-UX and FreeBSD are competent operating systems. Even so, I've managed one Linux web and mail server running a variety of high-load database driven web sites and it has suffered no downtime in the past three years save for two hardware upgrades. Credible evidence regarding Linux and it's inability to scale is hard to find; but if you've found some please don't hesitate to provide us with references.\n\nI think the world would be a much better place if your apparent attention to detail spilled over into your skill with written English. Then perhaps your very strong opinions wouldn't seem so paper thin to me.\n\nSean.\n\n"
    author: "Sean Pecor"
  - subject: "Re: What a clueless person!"
    date: 2003-03-25
    body: "I've been trying to hunt you down for a street address to which I will send my MarketingTool.com payment ... the site(s) don't provide good Contact information in that regard.\n\nPlease provide at the email address I've provided above. Or call:\n\n330-305-9098. "
    author: "Neil Cotiaux"
  - subject: "Re: What a clueless person!"
    date: 2002-10-05
    body: "Screw off"
    author: "Computer Literate"
  - subject: "Re: What a clueless person!"
    date: 2003-02-18
    body: "Dear Erik,\n\nPhil Hughes knows more about Linux, and has done more to promote it and serve the community, than you could ever dream of, no matter how potent your hallucinogens. You want to see a sterling illustration of clueless, go look in the mirror."
    author: "Alice Sysadmin"
  - subject: "Congratulations"
    date: 2001-11-09
    body: "More well deserved awards.  Congratulations once again.  Looks like KDE is paving the way for Linux as a viable desktop system.  Once that happens M$ are in trouble."
    author: "Anthony Enrione"
  - subject: "Well..."
    date: 2001-11-09
    body: "You guys are winning a little to much awards...\nBut anyway, kongrats and kudos...\nI waiting for that KDE 3, so don't sit don't, put up your legs and pat yourself on the back (okay do that, but not extensively..).\nRemember, the major evil is not Microsoft, or GNOME or even Apple. It is yourself. Think of new ideas. Fresh ideas. Take the best from different UIs. For example, instead of having a menu-centred panel (Kicker) like Windows taskbar, have something like RISC OS dock... Also remember, the fine details can actually mean a lot. Now, I don't want KDE going all out think of a fresh new UI mainly built around eye candies, but do something that is usable. All the people I have seen using Windows XP reverted based to the Classic look. And Aqua, used to be a friend, now a foe to many Mac users.\n\nP.S. Don't flame me."
    author: "Rajan Rishyakaran"
  - subject: "Re: Well..."
    date: 2001-11-10
    body: "/me flames Rajan :-)\n\nNo, seriously, I'm gonna have to disagree with you heavily here. KDE developers shouldn't implement \"fresh ideas\" just for the sake of so-called innovation. What works, works. If it doesn't work, fix it. Kicker works for me, in that everything I need to do on a omnipresent panel is implememented, and it's plugin system is extensible enough to allow you to do nearly anything you want with it. There's absolutely no reason to dump it and switch to a completely different mindset on the topic of panels/docs/taskbars/etc."
    author: "Carbon"
  - subject: "Re: Well..."
    date: 2001-11-10
    body: "As one of the original Kicker panel authors, (Matthias Ettrich and I did most of the original code before Matthias Elter took over and promptly rewrote most of it and made it better ;-), I'm interested in why people think a dock is more usable than a panel such as what we did for KDE2. I've used a couple dock implementations, mostly WindowMaker's, and feel that the panel mechanism is much more user friendly. You can easily add applications, folders, drag and drop files, etc... It has an application menu, pager, and taskbar most people are used to, and can be easily extended via plugins. You also now can have multiple panels. So I am genuinely interested in hearing why some people think a dock is easier to use, since I haven't found that to be the case at all. I think both KDE's and Gnome's panels are more powerful than any of the dock implementations, but I may be missing something."
    author: "Mosfet"
  - subject: "Re: Well..."
    date: 2001-11-10
    body: "Well, I just like the dockapps better, if kde gets better looking/functional applets it'll take me one step closed to kde. I'm just interested in functionality these days and I don't care if it comes from gnome, kde or tcl/tk :)"
    author: "Pyretic"
  - subject: "Re: Well..."
    date: 2001-11-10
    body: "Dockapps aren't any more functional than panel applets... KDE's panel applet API is much more powerful than most Dock applet API's I've seen, which mearly grab the window of a fixed size application. KDE panel applets on the other hand can be easily ran from the panel menus, resized to provide more or less information vs. space with the mouse, etc... KDE can even run dock applets for things like WindowMaker. If this is the only argument people can come up for a dock, I think I'll stick with a panel mechanism ;-)"
    author: "Mosfet"
  - subject: "Re: Well..."
    date: 2001-11-11
    body: "I like Kicker too... I like it very much in fact. There is just one thing..\nThe applets that gets loaded (by me, out of curiosity) can be a real pain to remove again. I would like a GUI item next to Add->Applet||Menu||Child Panel... guess what I want it to say? \"Remove Item\"! And then the mouse curser should change to something (like the X-kill curser). And then a left-click on the app-lnk, childpanel, menu or applet. Wouldn't that be Intuitive(TM)?\n\n/kidcat\n\nPS.: It would be nice if it \"auto-shrinked\" the new awailable space so the pager takes it all up."
    author: "kidcat"
  - subject: "Re: Well..."
    date: 2001-11-11
    body: "Can't you just right-click the applet handle and click \"Remove\"?"
    author: "not me"
  - subject: "Re: Well..."
    date: 2001-11-12
    body: "Yep ;-)"
    author: "Mosfet"
  - subject: "Kicker & SVG"
    date: 2001-11-10
    body: "Leave Kicker alone !! Kicker forever !! :-)\n\nSeriously though, what's wrong with it?  At the moment KDE is the only desktop I'm using every day, and I'm very happy with the job kicker does for me.\n\nAnd while we're on the subject of KDE futures, has anyone seen this Yahoo announcemnt of Adobe's SVG viewer:\n\n http://dailynews.yahoo.com/h/mc/20011107/tc/adobe_svg_viewer_3_0_supports_mac_os_x_10_1_more_1.html\n\nAccording to this it will even run Javascript embedded in SVG files.  Where is KDE 3.0 going with SVG?  Is anything in the pipeline?\n\nJohn"
    author: "John"
  - subject: "Re: Kicker & SVG"
    date: 2001-11-10
    body: "It doesn't support JS, but Qt has SVG support built in. To quote the docs, \"SVG is a W3C standard for \"Scalable Vector Graphics\". Qt 3.0's XML support means that QPicture can optionally generate and import static SVG documents. All the SVG features that have an equivalent in QPainter are supported.\". This means that there already is support for most SVG files, KDE just needs to take advantage of it. I don't know why there wasn't more noise made about this when it came out."
    author: "Mosfet"
  - subject: "KDE for Windows?"
    date: 2001-11-09
    body: "Hi,\n\nCongratulations to the KDE developers for winning the awards. They really deserve it. If they can increase stability and performance a little bit more, KDE will be *THE* *BEST* Desktop for Power Users. It sure is the best desktop for me.\n\nI sometimes have to work on Win2K at work (In fact I am posting this from a Win2K box), and I really miss KDE. I miss the konqueror filemanager, I miss the konqueror browser, I miss the beautiful high performance liquid theme. I even think that the art in KDE is much better than in windows. The available background images are really high quality, and I also like the non-photorealistic icons.\n\nThus I would really like to see a KDE version running as a shell replacement on windows. That would make it extremely easy to show the quality of open source software to windows users without touching their partition table. And it would enable people who have to use Windows to do it with style :-)\n\nIt should not be too hard to port KDE to windows, since Qt/Windows is now available. What are the reasons it does not compile out of the box? Does KDE use something in X directly?\n\nregards,\n\n    A.H."
    author: "Androgynous Howard"
  - subject: "Re: KDE for Windows?"
    date: 2001-11-09
    body: "While there is a win32 version of Qt libs, there is no win32 version of the KDE libs.  Also, KDE is GPL'd, and I think the win32 Qt is not compatible with the GPL (even the non-commercial version).\n\nJason"
    author: "Jason"
  - subject: "Re: KDE for Windows?"
    date: 2001-11-09
    body: "See\n\nhttp://kde-cygwin.sourceforge.net/\n\n-Vasee"
    author: "vasee"
  - subject: "Re: KDE for Windows?"
    date: 2002-03-10
    body: "Please, I need evaluate this product.\nIf you know in what site I can find the product.\nIn the Ecuadorian Red Cross we are implemented the Proxy Red Hat Linux and we\nneed used the diferent clients for workstation.\n\nBest regards.\n\nMario Pazmino"
    author: "Mario Pazmino"
  - subject: "great"
    date: 2001-11-09
    body: "Seems to be normal , while such a good work has been done !\nCongrats too"
    author: "pointer2002"
  - subject: "Let's make KDE the Best Desktop Environment!"
    date: 2001-11-11
    body: "We all love KDE, and rejoice when we see that our KDE apps are getting awards for excellence, but most of us do not try to improve KDE and remain passive users. The reason must be:\n\n1.many of us don't know how to code and\n2.Lack of time or leisure.\n\nbut *you can make a big difference in KDE* even when you don't know any KDE or QT programming :). Some of the important contribution we could do are:\n\nDeveloping an application is exhausting: Even writing a few hundred lines of program/application  would exhaust one's brain, and experimenting with the application to find out bug becomes very annoying and tiresome. And _instinctively_ one prays 'oh god, please send someone ease my work'.  May be I am exaggerating it a bit but Developers *do* get tired and feel bored sometimes.\n\nWe as appreciation to their creative but exhausting work, must do something to help them. I pondered about how we can be useful to them.  And here are my thoughts, these may not be great but have a look:\n\nA.  Helping the developers!  No, not by coding but by _hunting bugs_ for them.\n\n1. Bug Hunt: Experimenting with an application either KDE's own or from apps.kde.com, and doing weird and stupid things will yield many usability bugs and even severe bugs like the rename bug of konqueror \"Pressing F2 key twice on a selected file or folder crashes konqueror\". Experimenting with an application will give you the most knowledge of the application and its features but also how it should behave for an average user. You may find even simple bugs like \"single icon being used by two or more KDE applications, like 'kfm.png' is being used by \"File browsing\" and by \"File manager\" in kicker. Or look and feel bugs like the 'kcmstyle' dialog is too big to be useful and TTF fonts do not show up when antialias option is checked.\n\n2. Wishful thinking: Missing Features of Previous versions and nice features which will ease the users' work is great. Say, \"Kmail must have an option to relocate it's ~/Mail folder; since using the FAQs and manually moving the folders are troublesome.\" There are real useful wishlist at http://bugs.kde.org. But we do have creative minds, don't we? ;)\n\n3. Debugging the http://bugs.kde.org: There are sooooo many bugs at bugs.kde.org and you know developers find it hard to individually read each one and try to figure it out, many of the bug reports are fixed but still remain there. And many unfixed bugs got closed, like the \"Rename F2 key bug of Konqueror\". Now if we could get permission (from Kulow) to verify the bugs listed and to approve that they are indeed fixed and are still unfixed then the bug fixing time will reduce considerably and the 'Deadly closed bugs' like the 'F2 rename bug' can be brought to justice ;) The 'F2 rename bug' was reported before KDE 2.1 but still in KDE 2.2, it is still lurking there.' unfortunately David Faure, closed it giving reasons that 'it is a Qt bug'. IMHO This is not the way to deal with KDE bugs! Once a bug is marked closed then nobody will bother to open it. I wonder if we could get our hands dirty on the closed and yet to be closed bugs then KDE Development will be quite faster than it is now!\n\nYou may find my bug & feature reports at bugs.kde.org if you search with keyword 'maarizwan'. Some are real stupid and some are real insightful like \"Optimizing KDE for better Performance\". By no means I try to say that I or anybody who reports bugs and feature requests are better than others who don't. But when you report a bug and got it fixed and a feature you requested got implement that gives you a sense of satisfaction and you feel personal touch in KDE. Need to say that you deserve KDE since you've contributed something to it!"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Let's make KDE the Best Desktop Environment!"
    date: 2001-11-11
    body: "Good Post(TM)!\n\n>... and doing weird and stupid things will yield many usability bugs and even severe bugs ...\n\n Yes. Ctrl-Alt-Shift (and any combination) clicking on every gui-item is a good start. Taking a .jpg file and rename it to .mp3 and feed it to Noatun is not a stupid idea either. Be creative! Make a SPORT out of it!\n\n A bit of knowlege about coding is not a bad thing... so: Many bugs happens because something that the programmer didnt think of occures. This is a good examble:\n\nif ( size > 0 ) {\n        doStuff();\n        return(0);\n}\n\nelse if ( size < 0 ) {\n        doOtherStuff();\n        return(0);\n}\n\nnow what happens if size = 0 ??? The program will do somthing unexpected (and hopefully crash)! And its all about finding theese \"holes\" in the condition handling. The art of doing this can be hard to an experinced user, since they tend to use the program corectly all the time. But the newbe may missunderstand how the program works and do somthing wierd.\n\n So the point here is often (but not limeted to ;-): think like and idiot, click like a maschinegun.... and find a way to reproduce what just happened. A bugreport that says: \"if i click all the buttons 200 times in 10 secs the program crashes\" is totaly useless since the programmer cant reproduce the bug.\n\n/kidcat\n\nPS.: If ur above 18 (or 21) its not a bad approch to get (reasenoble)drunk an start hacking in the config files either ;-)\n\nPPS.: Keep up the exelent work guys!!! You are my idols and heroes!!!"
    author: "kidcat"
  - subject: "Re: Let's make KDE the Best Desktop Environment!"
    date: 2001-11-12
    body: "You don't really need to get permission from anybody for manipulating bug reports. On bugs.kde.org are instructions how to do it.\nBut only close/reassign/whatever bugs if you are sure about it.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Hooray (is that the correct spelling?)"
    date: 2001-12-06
    body: "KDE rules. Who cares if it is done in C++? gnome laps up warm choad."
    author: "Tai"
  - subject: "Re: Hooray (is that the correct spelling?)"
    date: 2002-01-29
    body: "hi my name is jenna. i need help on how to spell efficent is that the correct spelling. please email me asap. thank you. jenna"
    author: "jenna "
---
The KDE Project today <a href="http://noframes.linuxjournal.com/articles/buzz/0052.html">received</a> three Editors' Choice Awards in this year's round of the <a href="http://www.linuxjournal.com">Linux Journal</a> Editor's Choice awards. The KDE Web Browser <a href="http://www.konqueror.org/">Konqueror</a> received the award for the best web client thanks to its high quality and for its very good integration into the desktop. <a href="http://www.kdevelop.org/">KDevelop</a> was awarded the prize for the best development tool, with its flexibility and broad adoption by the industry being mentioned as the primary reasons.
<a href="http://www.kde.org/">KDE</a> was selected as the best desktop environment for its "improved architecture" and the "very active development". Whole hearted congratulations to the dedicated KDE developers. And many thanks to the editorial team of the Linux Journal, for their annually refreshed vote of confidence.
<!--break-->
