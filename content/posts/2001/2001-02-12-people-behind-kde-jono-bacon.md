---
title: "People behind KDE: Jono Bacon"
date:    2001-02-12
authors:
  - "Inorog"
slug:    people-behind-kde-jono-bacon
comments:
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-12
    body: "Jono Bacon wrote :\n\n>> I would love to see a decent graphical\n>> installation system similer to Installshield.\n\nOne small comment on the InstallShield part.\nLinux is ahead of the competition in\nthe packaging and installation area.\nThe only decent part of InstallShield and other\nWindows based packaging software is the GUI the\nuser gets during installation. So if anyone\ndecides to make something which looks like\nInstallShield, please make the internals such\nway that it uses RPM or the Debian counterpart.\n\nBest regards,\n\nEric"
    author: "Eric Veltman"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-15
    body: "I just recomend, even when  people love X windows\nto port Qt to Super VGA libs so we don't have to \nsuffer the pain."
    author: "juan"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-12
    body: "There are two projects like this, kinst, which is coming along nicely, and Inca, which I think was cancelled."
    author: "Carbon"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-12
    body: "You have to differentiate: There is room for:\n\n-A KDE installer\n-The KDE installer\n\nWhile the former would be an answer to installshield, the latter is needed to tackle the installation of kde itself. but i think both things are being worked on right now, and I'm really looking forward to these, since I oftentimes have problems with installing software on linux."
    author: "its me again"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-12
    body: "When I said about an installerI meant an installer for packages such as RPM's and DEB's. I would love to se a GUI installer, and I feel it would make the usof UNIX/Linux for new users easier.\n\nAlthough there is KInst, I am not sure if the project is still running. If not, would the author like to open it up for another developer?"
    author: "Jono Bacon"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-12
    body: "loki has made its game installer availlable<br>\nit s xml based , works without X also and is free and customizable:<br>\n<a href=\"http://www.lokigames.com/development/\">here</a><br>\nit just uses gtk ;)"
    author: "dent"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-12
    body: "An installer is in the works.\n\nIt uses Qt ;-)"
    author: "reihal"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-13
    body: "So then Qt is statically linked into the setup program so this can handle installing Qt as well, right?\n\nWill this give the option to install qt-copy so we can get cool stuff like AA or Alpha in X?\n\nAnd why doesn't multimedia go into the developer's install?  I assume most developers want to access multimedia in their apps!  Maybe this should be an option or a checkmark under the developers radio button..."
    author: "Chris Bordeman"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-13
    body: "Also, will this include KDE Studio as well as KDevelop?"
    author: "Chris Bordeman"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-13
    body: "ill bet 99.9% of everybody will choose to install everything, not a stupid cheesy \"profile\"\n\nJason"
    author: "well.."
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-12
    body: "Thank u Jono for all of ur hard work!\n\nYou and all the KDE developers are great and set a super example.. and help peps follow that example.\n\nJason\n\n* RPN kalculator for kde: <a href=\"http://katzbrown.com/krpn-0.1.tar.gz\">katzbrown.com/krpn-0.1.tar.gz</a>\n\n* is wildfox ever going to be interviewed?"
    author: "Jason Katz-Brown"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-12
    body: "Is mosfet going to be interviewed?"
    author: "reihal"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-13
    body: "you should really interview WildFox."
    author: "stephan"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-14
    body: "Send me Wildfox bio and I'll think about it :-) I can be braggged with boxes of Swiss or Austrian chocolate and bottles of French red wine ;-)\n\n--\nTink"
    author: "Tink"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-14
    body: "Mosfet has agreed to do the interview about 4 months ago. Unfortunately he still hasn't send in his answers. I think he just to busy coding and doesn't want you to know who he really is, maybe he has some kind of secret life ;-)) You could all email him and beg him to send in his answers, maybe that'll work. I tried doing that but had no luck whatsoever.\n\n--\nTink"
    author: "Tink"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-15
    body: "Indeed mosfet has a secret life. If only you had seen THAT picture!\n\nWhat the heck, interview me, nobody remembers me, I do nothing anymore, but I promise strange answers ;-)"
    author: "Roberto Alsina"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-16
    body: "I remember the might ralsina. ;--P"
    author: "Jono Bacon"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-14
    body: "Hey Tink,\n\nborn 15.03.1986\ndeath hope in > 70 years :)\n\nThat's it ;)"
    author: "Nikolas Zimmermann"
  - subject: "Re: People behind KDE: Jono Bacon"
    date: 2001-02-18
    body: "haha wildfox :P"
    author: "Jason Katz-Brown"
---
Welcome to your weekly tour of the KDE people gallery. This time, <a href="http://www.kde.org/people/jono.html">Jono Bacon</a> answers <a href="mailto:tink@kde.org">Tink</a>'s questions. A student, a writer, a guitar player and a KDE developer, Jono acknowledges to have come to KDE by necessity and to have stayed by love. Visit Tink's <a href="http://www.kde.org/people/people.html">talk lounge</a> for other great interviews.

<!--break-->
