---
title: "RedHat RPMs for KDE 2.2.1"
date:    2001-09-23
authors:
  - "Dre"
slug:    redhat-rpms-kde-221
comments:
  - subject: "One thing to watch out for..."
    date: 2001-09-23
    body: "Although the symlink is fixed now for /usr/share/config/kdm, if you've changed your kdm settings at all from what was in the default RPMs (like changed the background color or logo or whatever), the directory will be renamed kdm.rpmsave and the symlink will replace it, but will not contain your \"saved\" files, so kdm *still* won't start.\n\nI don't have the time to do this build tonight, it's already 3:30am for me, but I'll see if I can fix the rpms (or check for fixed ones in rawhide) and get them put together.  Other than that, I'm running the 7.1 rpms on my desktop and it's looking pretty smooth."
    author: "Benjamin Reed"
  - subject: "Re: One thing to watch out for..."
    date: 2001-09-26
    body: "What is t\"he symlink is fixed now for /usr/share/config/kdm\"?  I just built all from SRPMS, now kdm won't start.  I see something changed in /usr/share/config/kdm setup, but what is the correct symlink?"
    author: "N Becker"
  - subject: "Re: One thing to watch out for..."
    date: 2001-09-28
    body: "Kdm still causes some trouble on my rh7.1 -  after making sure that the kdmrc is ok etc.\n\nkdm_greet is trying to call Xinerama functions which are not in my libs.  I am running XFree86-4.0.3.  There is a static libXinerama.a.  Is this linked (no pun) to the actual Xserver being run (i760)?  So of course gdm fails.\n\nRegards \nJohn"
    author: "John Floyd"
  - subject: "Vivat packages for RH6.x"
    date: 2001-09-23
    body: "It's perfect. I haven't reinstall my linux box :-)"
    author: "Milan Svoboda"
  - subject: "Distributed KDE Packaging to acclerated KDE develo"
    date: 2001-09-23
    body: "I would like to discuss a very simple way to improve and accelerate kde and\nkoffice development by breaking big packages to smaller ones. So, the objectives are:\n\n 1. Ease KDE package download and Installation.\n 2. Respecting Users' need and Avoiding unwanted package installation\n 3. Freeing up the clogged ftp servers :)\n 4. Easy upgrade of specific packages using KDE-Installer or manually\n 5. Easy Access to sources and More bug fixes by users and new kde developers.\n 6. Overall satisfied KDE user \n\n\nTo-do: break every big source package to about 1-2 MB maximum size (depending on the sources) but not more than 3 MB. The Kdebase, Kdelibs, Kdegames and other packages are of huge sizes :( kdebase=10MB, kdelibs=5+MB, Kdegames=10+MB and so on. I would suggest that these huge packages be broken into smaller bits, like:\n------------------------\nkdebase.core.tar.bz2\nkdebase.konqueror.tar.bz2\nkdebase.kicker.tar.bz2\nkdebase.kcontrol.tar.bz2\nkdebase.kate.tar.bz2\nkdebase.kpersonalizer.tar.bz2\nkdebase.ktip.tar.bz2\n------------------------\n\nlikewise kdegames can be parted into many smaller packages:\n------------------------\nkdegames.kpat.tar.bz2\nkdegames.sokoban.tar.bz2\nkdegames.kwin4.tar.bz2\nkdegames.kmines.tar.bz2\n------------------------\n\nI'm not asking to change the default way of distributing the sources and binary packages but asking for one more folder in the ftp.kde.org and its mirror which contains a folder 'separated' or 'Distributed' or whatever you may like, which will contain the split-upped kde packages.\n\n\n1. Ease Download and (binary) Installation: no big deal, rpm, tar.bz2 and tar.gz or debs, will be quite smaller so it will reduce the download time required, you would appreciate this if you have a modem dial-up connection.\n\n2. Avoiding unwanted packages: a typical kde user may not like to have all the package kde provides, take me for example, I really love to have the following games in kdegames package:\n\n a. Patience\n b. Kmines\n c. Sirtet\n d. Jezzball\n\nand I do not want to have other games of the default kdegames package. I have no other option to discard those games from my installation (but to install and delete). if kdegames has a distributed smaller packages like:\n\n i.   kdegames.kpat.tar.bz2 \n ii.  kdegames.kmines.tar.bz2 \n iii. kdegames.sirtet.tar.bz2\n iv.  kdegames.kjezz.tar.bz2\n\nin this way I can get what I really need. And due to this huge size of kdegames new games cannot be included into the package like knights a nice chess frontend (http://knights.sourceforge.net) which is around 1MB of size. It could be easily become a package of kdegames as: \n\n kdegames.knights.tar.bz2\n\nIn the Koffice I would love to have (download and install) only two Kword and Kspread, as like many kde users don't have to use other koffice apps even rarely. It would be nice to see:\n\n i.   koffice.kword.tar.bz2\n ii.  koffice.kspread.tar.bz2\n iii. koffice.kpresenter.tar.bz2\n iv.  koffice.krayon.tar.bz2 (etc.)\n\n3. Freeing up clogged ftp servers: As the packages become smaller (around 1-3 MB), the download time reduces considerably and hence ftp servers become unclogged and will be accessible for more people.\n\n\n4. Easy Upgrade of Specific KDE apps using KDE-Installer or manually: most kde users just wish for individual konqueror, kmail, and other apps updates, but they could not do so at present. As I am happy for KDE 3.0's inclusion of kde-installer, but I am afraid it will also try to download the huge kde packages again making itself rather useless, except installing packages in proper sequence. The Kde-Installer will be benefitted with this type of distributed kde packages, allowing more flexibility and options to choose from. KDE-Installer can allow individual package upgrades like konqueror (satisfying the dependencies) or other apps. Users can also download their preferred apps for upgrading.\n\n5. Easy Access to Source and More Bug Fixes: kde users and new kde developers are discouraged by the huge source sizes to download 10MB kdebase source to just to tweak a bit here or there. I was discouraged and unhappily forced to download 10MB kdebase.tar.bz2, just to check out and modify the kicker sources or to get (31k) cursor_large.bdf sources from kcontrol/input :( with which I created the white mouse cursor for KDE/X. \n Not just tweaks, the bugs can be located and fixed easily as it get easy to check out the sources by new and experienced kde developers alike with the new 'distributed kde packages'.\n\n\n6. Overall Satisfied KDE users: these things will definitely satisfy a kde user:\n\n a. Reduced time on downloading source/binary kde packages\n b. Easy, fast Accessible ftp download\n c. Individual package upgrade\n d. Easy to access source code\n e. Fully (almost) customized kde\n f. Easy installation with kde-installer\n g. Alpha, Beta, RC1 etc version can be accessible to user (due to small package sizes)\n\nI know that there are other factors affecting the above points like bandwidth, knowhow etc., I believe that the 'Distributed KDE Packages' will accelerate and improve kde development. And I would appreciate if the KDE Team kindly allow these distributed packages along with the standard (or current) packages. Thanks for reading up to this point ;)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Distributed KDE Packaging to acclerated KDE develo"
    date: 2001-09-23
    body: "Well, if you get the sources via cvs instead of ftp you have at least all the possibilities you are asking for, or even more.\nIf you are interested in kdebase/kcontrol/input, just type \"cvs co kdebase/kcontrol/input\" for example.\nOr if you are just interested in kword, type:\ncvs co -l koffice\ncvs co koffice/lib\ncvs co koffice/kword\n\nHow distributions organize their RPMs, that is their job. Some distributors do it already the way you want to have it."
    author: "Michael H\u00e4ckel"
  - subject: "Re: Distributed KDE Packaging to acclerated KDE de"
    date: 2001-09-23
    body: "Conectiva and Debian for example."
    author: "Evandro"
  - subject: "Re: Distributed KDE Packaging to acclerated KDE de"
    date: 2001-09-23
    body: "As well as Caldera. So there are three already."
    author: "anonymous"
  - subject: "Re: Distributed KDE Packaging to acclerated KDE develo"
    date: 2001-09-23
    body: ">How distributions organize their RPMs, that is their job\n\nAnd other such as RH, Mandrake, SuSE etc., organize every package ditto to kde's package."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Distributed KDE Packaging to acclerated KDE develo"
    date: 2001-09-23
    body: "Seems like a great idea for source distribution. But as always... Open Source is like that having few people willing to do stuff or always suggesting a difficult way. I support fully your suggestion. KDE is huge and compilation time is precious. There is 40% of stuff I don't use in KDE which I wished could be automatically out. \n\nBut if no ones really accept your suggestion and put in practice, you can go for Conectiva Linux. (www.conectiva.com) - they do that thing for all packages to reduce installation size. RPMS are more tolerable than RedHat's or MDK's. I personally recommend giving up any distro and going for Conectiva. They also have apt-get for RPM etc... \n\nBut as Slackware being the best distribution around yet, it's hard cause few people are willing to do even Slackware packages. Some even tell that Slackware has not got a package manager. They really don't know what they're talking about."
    author: "Reality Check"
  - subject: "Re: Distributed KDE Packaging to acclerated KDE de"
    date: 2001-09-23
    body: "I totally agree.\n\nWhen I compile a new KDE on my laptop (333MHz K6-2 w/ 64MB RAM)\nit takes hours and hours.  I'm talking, 12+ hours or more.\nAt least, it seems like it was that long.  It has been a while\nsince I've done it--primarily because I just don't have time\nbetween work and school.  Most of the stuff that gets compiled\nI don't even use.\n\nI can't even imagine how long it would take on my p200 w/ 128MB RAM.\nI doubt the extra RAM would really make a huge difference, but I'm\nsure the much slower CPU would."
    author: "A. C."
  - subject: "Re: Distributed KDE Packaging to acclerated KDE de"
    date: 2001-09-24
    body: "Yes. Good that I you think like that too. Coz certain package maintainers don't like to hear such thing at all... \n\nI have a 400Mhz Celeron B (128 L2)- Celeron is not a top line processor but its math-processor is better than even Pentium in some cases. 163MB of RAM, and a very fast Fujitsu 8GB Disk.\n\nKDE 2.x takes up to 15 hours compiling code, without 'install'. \n\nEven if you have a 1GHz Processor with 512 MB RAM, you will have also to spend at least 2 1/2 hours waiting for compilation be done."
    author: "Reality Check"
  - subject: "Re: Distributed KDE Packaging to acclerated KDE de"
    date: 2001-09-24
    body: "I would love for it to be broken up.  The worst part about working on creating packages, regardless of the distro you're talking about, is you get through the entire build before you find out if something goes wrong.  So I didn't just build kdebase once, for an hour.  I built it 5 times, as little things broke or fixed themselves.\n\nWith smaller packages, that debug time will drop dramatically.  It's a great idea, maybe it's worth putting together something that works in conjunction with the CVS tree and builds micro-packages."
    author: "Benjamin Reed"
  - subject: "Thanks Ben!"
    date: 2001-09-24
    body: "Dear Ben,\n\nThank you, thank you very much for creating KDE 2.2.x packages for RedHat 7.x, I appreciate your work very much and I understand how boring and troublesome creating RPMS from a source especially these KDE ones. Thanks again for your contribution to the User Community.\n\nYours thankfull :)\nRizwaan."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Distributed KDE Packaging to acclerated KDE de"
    date: 2002-10-02
    body: "download kde"
    author: "jacques fuchs"
  - subject: "Re: Distributed KDE Packaging to acclerated KDE develo"
    date: 2003-01-04
    body: "I need Kde base packages in a splited bunzip files. how can i downlaod this. where can i get this package."
    author: "Yuvashree"
  - subject: "redcarpet is great"
    date: 2001-09-23
    body: "RedCarpet!!!!!!!"
    author: "bondowine"
  - subject: "Re: redcarpet is great"
    date: 2001-09-23
    body: "Yeah, I don't like gnome but KDE definitely needs tool like redcarpet."
    author: "Matti Palaste"
  - subject: "Order of installing RH 7.1 RPMS for KDE 2.2.1"
    date: 2001-09-23
    body: "Ok, maybe this qualifies as a \"stupid question,\" but I bet a few other people are wondering as well...\n\nIs there a certain order to install the RH 7.1 RPMS that will help all the dependency problems?  If I just try to do *.rpm for all the packages, I get a hoard of dependency issues (with a stock RH 7.1 install).  If I installed them in a certain order would it help this, as it seems like certain new packages depend of certain other new packages...\n\nI don't want to do a --force and risk really screwing up my system.\n\nThanks,\n\n\nJeff"
    author: "Jeff"
  - subject: "Re: Order of installing RH 7.1 RPMS for KDE 2.2.1"
    date: 2001-09-23
    body: "Any dependency for my RedHat packages is either in the directory on the FTP/HTTP site, or is available straight from your RedHat CDs or RedHat update.  If there's something missing, you should be able to use RedCarpet or autorpm or apt for RPM or any of the other RPM tools (or FTP =) to grab them from RedHat.\n\nYou shouldn't need a --force, that I'm aware of.  I installed on a stock machine and it worked out OK, after downloading some things that weren't automatically installed by a workstation install."
    author: "Benjamin Reed"
  - subject: "Re: Order of installing RH 7.1 RPMS for KDE 2.2.1"
    date: 2001-09-27
    body: "Hi Ben,\n\nI just replied to another post with this same issue, but your \nmessage here begs another question.\n\nWhat are the \"some things\" that you had to download to make\nthis install happy?  And where can I get them.\n\nI've just tried to get the RedCarpet package that you referred\nto and it just dies on my system (RH7.1,AMD1.2,512MB).  Of course\nmaybe I picked the wrong package from the 29 that rpmfind.net \nlists.  I tried red-carpet-1.1.2-ximian.1.i386.rpm.  Now I'm \ndownloading autorpm-1.9.9-2.i386.rpm to give that a whirl.\n\nThanks for the help,\n\nMark Aubin"
    author: "Mark Aubin"
  - subject: "Re: Order of installing RH 7.1 RPMS for KDE 2.2.1"
    date: 2001-10-02
    body: "You don't need Red Carpet, but you can use it to install automatically sort out the dependencies."
    author: "Carg"
  - subject: "Re: Order of installing RH 7.1 RPMS for KDE 2.2.1"
    date: 2001-09-30
    body: "But what should I do if I have kde2.0 on my RedHat 7.0?\nThanks."
    author: "Wei Sun"
  - subject: "Re: Order of installing RH 7.1 RPMS for KDE 2.2.1"
    date: 2001-09-29
    body: "Yes this is a stupid question.\n\nReason: are you really talking about *installing* KDE 2.2.1 or are you talking about *Upgrading*?\n\nThere is an order for installing:\n\nQt\nArts\nKdelibs\nKdelibs-sound\nKdebase\nOther stuff\n\nBut, there is no order for Upgrading!!!!!\n\nIn fact, you need to install everything with one command or use Kpackage.\n\nJRT"
    author: "James Richard Tyrer"
  - subject: "Thanks !!!!!!!"
    date: 2001-09-23
    body: "Thank you Benjamin ! This is what I was looking for !\nThank you very much!"
    author: "Eduardo Sanchez"
  - subject: "Where do you start?"
    date: 2001-09-24
    body: "I hope I don't sound stupid, but where do you start? I see a directory full of rpm's, but none that obviously says that it is the first to download, or which other ones to download to get things started.\n\n-- IV"
    author: "Me"
  - subject: "Re: Where do you start?"
    date: 2001-09-24
    body: "All this information is available on kde.org, but I'll put it here for other's convienence:\nYou need to install KDE packages in this order:\n\nkdelibs\nkdebase\n(else)\n\nAfter kdelibs and kdebase (in that order), it doesnt matter what other packages you install, nor in what order you install them. If you want to upgrade QT, such as using qt-copy or just upgrading to the latest TT QT, get the latest in the 2.x series, and install/upgrade QT before you install kdelibs."
    author: "Carbon"
  - subject: "FreeBSD packages any soon?"
    date: 2001-09-24
    body: "I hope that somebody will release FreeBSD packages, I tried texstar's objprelink RPMs and although the release is just 0.0.1 it is really a vast improvement over 2.2 especially in stability department. Konqi is so bloody good!"
    author: "J Blazevic"
  - subject: "Is it running on AMD K6-2 ?"
    date: 2001-09-24
    body: "I wasn't able to run previous KDE2.2 packages on my AMD K6-2 ( i585 ), so I'would like to ask if someone with K6 checked those new packages. I'm trying to avoid downloading all packages ( with my low-bandwidth connection ... ) only to see that they are not working."
    author: "Krame"
  - subject: "Re: Is it running on AMD K6-2 ?"
    date: 2001-09-24
    body: "I can't imagine how your processor could prevent KDE binaries from running, as long as it's x86 (which the K6-2 is, along with every other Cyrix, Intel, and AMD processor). More likely, it's the way your distribution's software is set up."
    author: "Carbon"
  - subject: "Re: Is it running on AMD K6-2 ?"
    date: 2001-09-24
    body: "well...ofcourse they will not run if compiled with compiler option -march=i686 (package name *.i686.rpm). But they will run faster on k6-2 if you use -march=i586 -mcpu=i686 (package name *.i586.rpm). Best ofcourse is to use -march=k6 (*.k6.rpm). Although I hear gcc3.0 is awfull in this respect, but then, kde should still be compiled with 2.95.....or 2.96 if you do not feel bad about this non-release.\n\nAll quite compilicated."
    author: "Danny"
  - subject: "Re: Is it running on AMD K6-2 ?"
    date: 2001-09-24
    body: "well...ofcourse they will not run if compiled with compiler option -march=i686 (package name *.i686.rpm). But they will run faster on k6-2 if you use -march=i586 -mcpu=i686 (package name *.i586.rpm). Best ofcourse is to use -march=k6 (*.k6.rpm). Although I hear gcc3.0 is awfull in this respect, but then, kde should still be compiled with 2.95.....or 2.96 if you do not feel bad about this non-release.\n\nAll quite compilicated."
    author: "Danny"
  - subject: "Re: Is it running on AMD K6-2 ?"
    date: 2001-09-25
    body: "Well, RPMs compiled using special processor only optimizations are usually named differently. I.e kdelibs-2.2.1.i686.rpm or somesuch\n\nPlus, I haven't heard of any distro putting out processor optimizied files except for Mandrake."
    author: "Carbon"
  - subject: "Re: Is it running on AMD K6-2 ?"
    date: 2001-09-25
    body: "All rpms was named *.i386.rpm but all executables refused to run showing message:  Illegal instruction."
    author: "Krame"
  - subject: "Re: Is it running on AMD K6-2 ?"
    date: 2001-09-25
    body: "What distribution, QT version, and KDE version are you running?"
    author: "Carbon"
  - subject: "Re: Is it running on AMD K6-2 ?"
    date: 2001-09-25
    body: "Ergh.  I guess the kde configure is doing sse or something goofy regardless of what CFLAGS are given.  Sorry, but it'll probably take a rebuild then..."
    author: "Benjamin Reed"
  - subject: "Re: Is it running on AMD K6-2 ?"
    date: 2001-09-25
    body: ">Ergh. I guess the kde configure is doing sse or something goofy regardless of what \n>CFLAGS are given. Sorry, but it'll probably take a rebuild then...\n\nMaybe its kde, but not neccesarily; gcc 2.96 (which RH and Mandrake use) is severely broken in some respects. I noticed that the mandrake src rpms have a few extra lines setting the compiler march and mcpu options (have a look at one of mdks spec files, do not have them here at work).\n\nbye\ndanny"
    author: "Danny"
  - subject: "FTP a tad overloaded... =)"
    date: 2001-09-24
    body: "The FTP server is going a bit wiggy, but it looks like the RPMs have made it to kde.org's FTP site at ftp://ftp.kde.org/pub/kde/stable/2.2.1/RedHat-unofficial/, so feel free to grab them there instead.  ;)"
    author: "Benjamin Reed"
  - subject: "Erg... CUPS"
    date: 2001-09-24
    body: "OK, I'm sure that CUPS is much better than LPRng.  On the other hand, I had LPRng configured and working with my two printers (old Canon BJ200ex on parallel, Epson 860 on USB).\n\nDoes kdelibs really require cups?  The RPM is claiming that it needs CUPS in order to be installed, and CUPS won't let itself be installed unless I remove LPRng.\n\nSo I remove LPRng and install CUPS.  However, as best I can tell, your CUPS RPM here doesn't support USB printers.  Is this correct?\n\nI'm one who uses Gnome most of the time... I'd love to try out KE 2.2.1, but ideally I'd like to do it with a minimum of pain.  If I can get my printers working easily with CUPS, then great, otherwise, I'll wait for something which integrates better with RedHat 7.1.\n\n-Rob"
    author: "Rob Knop"
  - subject: "Re: Erg... CUPS"
    date: 2001-09-24
    body: "I think you should be able to --nodeps them and skip cups, but I can't guarantee it will work.  I just tried removing cups and seeing if the print manager still comes up (and it does), but I don't have a printer here at home to test with.  =)\n\nIf you really want to be sure, you can grab the .src.rpm and remove the cups references from the spec file and rebuild, but it looks like it should still work."
    author: "Benjamin Reed"
  - subject: "Re: Erg... CUPS"
    date: 2001-09-24
    body: "Try a stable release of gimpprint for cups\n\nhttp://gimp-print.sourceforge.net\n\nIf your driver supports USB then CUPS/KDE support it, too\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Who IS Benjamin Reed?"
    date: 2001-09-24
    body: "Installing rpms built by people you don't know is one of the biggest security threats for linux users.  You should only install rpms from trusted sources, e.g. ftp.kde.org.  If Mr. Reed really wants to package KDE, he should convince the maintainers of ftp.kde.org to post them.  There is safety in numbers, lots of people using the rpms means lots of people to figure out if they are trojan horses.  I have very little doubt that Mr. Reed is an honest volunteer of his time, but this issue should be at the forefront of anyone's mind as s/he downloads and installs rpms.  I also have very little doubt that there are rpms out there, even in trusted locations, that have trojans inside them."
    author: "Albert Schueller"
  - subject: "Re: Who IS Benjamin Reed?"
    date: 2001-09-24
    body: "i have no idea who the hell this guy is either, but what i've learned from reports of others is that his redhat RPM's actually work on a 7.1 system.  (go ahead, try the official RPM's from redhat before coming crawling back to these RPM's).  from the url on the site, it looks like he might be an employee of opennms (appears to be another free software company)\n\nthere's obviously lots of people using these RPM's as ben mentioned his ftp server was getting hammered. in a nutshell, this is a trusted source."
    author: "Mark"
  - subject: "Re: Who IS Benjamin Reed?"
    date: 2001-09-24
    body: "Still doesn't mean I'm a trusted source, but I thank you for the vote of confidence.  =)"
    author: "Ranger Rick"
  - subject: "Re: Who IS Benjamin Reed?"
    date: 2001-09-24
    body: "Who IS Ranger Rick?"
    author: "Anonymous Coward"
  - subject: "Re: Who IS Benjamin Reed?"
    date: 2003-06-23
    body: "Benjamin Reed was a worker for Anheiser Busch in 1892 in Jacksonville, Florida.\nHe killed a white worker for A.B. in an altercation on the 4th of July, gave himself up to the authorities, and was placed in the city jail.  Word spread that there might be a lynching, so hundreds of armed blacks surrounded the jail and prevented any whites other than the authorities from passing. The state militia was called in and a gatling gun was placed in front of the jail. For a week troops maintained order until things calmed down.  Later Reed was tried and convicted of manslaughter, served less than 5 years, returned to Jacksonville, got a job, and lived out the rest of his life in the city without incident."
    author: "William Brainard"
  - subject: "Re: Who IS Benjamin Reed?"
    date: 2007-01-30
    body: "Heh, wrong Benjamin Reed.  :)"
    author: "Ranger Rick (a.k.a. Benjamin Reed)"
  - subject: "Re: Who IS Benjamin Reed?"
    date: 2001-09-26
    body: "Well, Ben Reed is personally known to me -- he's the head RPM guru for OpenNMS, and in general a really responsible guy.  I'd install his binaries without worrying.  Of course, that begs the question -- who am I?  You can look me up at http://unicornsrest.org/craig/ ... or just do a Google search for me and see what you turn up.\n\nIf you're still concerned, you can always download the source RPMs, check the spec files and build them yourself -- I do that for a lot of packages."
    author: "W. Craig Trader"
  - subject: "Objprelinked ?"
    date: 2001-09-24
    body: "Hi Ben\n\nThanks for the RPMs - I spent hours doing rpm --rebuild on Bero's SRPMS, unfortunately, too many dependencies and takes too much time.\n\nQuestion: Are the RPMs objprelinked ? Apparantly, it makes a big difference to startup speeds (I have objprelink installed though I guess that wouldn't make much difference).\n\n- Biswa."
    author: "Biswapesh"
  - subject: "RH7.1 dependancy?"
    date: 2001-09-24
    body: "I have tried the rpms, and am stalled at installing kdelibs.  I have resolved all buy libxstl and libxml2, of which I don't seem to find RPMs from RedHat.  Would anyone know where I can find the appropriate RPMs for the above two dependancies?  Thanks in advance"
    author: "Anonymous Coward"
  - subject: "Re: RH7.1 dependancy?"
    date: 2001-09-24
    body: "Try www.xmlsoft.org or www.rpmfind.net. They are available at both places.\n\n- Biswa."
    author: "Biswapesh"
  - subject: "Re: RH7.1 dependancy?"
    date: 2001-09-24
    body: "Oops, sorry about that.  Forgot to copy the libx*.rpm files from the 7.0 directory.  I had to rebuild those myself, since they weren't in RedHat's releases.  You should be able to get them from the 7.0 directory on my site or the mirrors."
    author: "Ranger Rick"
  - subject: "Dependency problems"
    date: 2001-09-24
    body: "This is what I get when I try to install the libs.\nI don't see an rpm for kdesupport.  Do I need that?\nThanks.\n\nFound 0 source and 1 binary packages\nDependency Problem:\n kdelibs is needed by kdelibs-devel-2.2.1-0.rh71.1.cups\n pcre-devel is needed by kdelibs-devel-2.2.1-0.rh71.1.cups\n kdesupport-devel is needed by kdevelop-1.4.1-2\n kdesupport-devel is needed by kdevelop-1.4.1-2\n kdelibs-devel is needed by kdelibs-sound-devel-2.1.1-5\n kdelibs-devel is needed by kdelibs-sound-devel-2.1.1-5"
    author: "Wendell"
  - subject: "Re: Dependency problems"
    date: 2001-09-25
    body: "I compiled 2.2.1 from source on 7.1 and had few problems. I now have the system up and running pretty well.\n\nDrop by #kde-users on openprojects.net and we'll chat about it if you like.\n\n-mh"
    author: "magnethead"
  - subject: "Re: Dependency problems"
    date: 2001-09-27
    body: "Yea, I'm getting this and worse.  Where does this \ndependency cycle end?  I've downloaded all the RPM's \non Ben's opennms.org site and I've got the RH7.1 CD's.\n\nI must admit that I'm somewhat new to the RPM process,\nbut I can't believe it really is as hard as it seems.\n\nSo I have already done \"rpm -iv kde* qt*\" from the \nRH7.1 CD's.  This worked perfectly.\n\nThen I downloaded Ben's RPM's and was able to upgrade\nthe QT packages with a simple \"rpm -Uv qt*\".  But I can't\nmake much headway with the kde packages.  \n\nDoes anybody out there have a step by step proceedure \nstarting from a blank disk, installing RH7.1, and then \nupgrading all the KDE and QT libraries?   Please don't\ntell me that I have to go to rpmfind.net and look for\neach package one at a time.   \n\nWhat I fear is that most people who have been able to \nsuccessfully install these packages have a whole bunch of\nother RPM's already added since the vanilla install that I\ndon't know about.  \n\nI'm trying to make some final decisions about the direction\nwe take with our GUI development.  I think the KDE/QT option\nmight be the best, but I really need to evaluate the newest\nKDevelop and QT Designer to see if they will do what we need.\n\nThanks for any help,\n\nMark Aubin"
    author: "Mark Aubin"
  - subject: "vorbis, lower versions?"
    date: 2001-09-26
    body: "When I loaded the rpms for kde2.2.1 for  rh7.1, I came up with the following questions:\n\n1) kdebase says that it requires vorbis, but doesn't specify the version. The only one that I can find via rpmfind is libvorbis-1.0rc2-2.i386.rpm from rawhide, which does not seem to satisfy the dependency, as rpm -V kdebase.... still complains. This package does not seem to be included with the non-kde packages on the download site.\n\n2) the following packages seem to have LOWER versions than the ones that I previously loaded for kde 2.2. The previous versions are in parens and I got them by following the links on the kde website:\n\nkdemultimedia-2.2-0.rh7x.1.i386.rpm (2.2-3) \nkdemultimedia-devel-2.2-0.rh7x.1.i386.rpm (2.2-3) \nkdepim-2.2-0.rh7x.1.i386.rpm (2.2-1) \nkdepim-devel-2.2-0.rh7x.1.i386.rpm (2.2-1)\nkdeutils-2.2-0.rh7x.1.i386.rpm (2.2-2)\nkdoc-2.2-0.rh7x.1.noarch.rpm (2.2-1)"
    author: "Mary"
  - subject: "Cannot change fonts"
    date: 2001-09-26
    body: "Hi Ben,\n\nWith these RPMs, I cannot change the fonts in KDE. Everytime I change the fonts in Control Center->Look and Feel->Fonts, the new settings are reset to the defaults upon restart. And the defaults are pretty bad (on my system, at least).\n\nAnything I missed?"
    author: "binand"
  - subject: "Re: Cannot change fonts"
    date: 2001-09-26
    body: "You are not alone, I've got the same problem, all fonts just use a fixed font."
    author: "Fredrik"
  - subject: "Re: Cannot change fonts"
    date: 2001-09-27
    body: "Hey! The same on my PC!! I thought it was my fault... Anybody noticed Benjamin about that??"
    author: "Mbok"
  - subject: "Re: Cannot change fonts"
    date: 2001-10-30
    body: "Same on mine.  Is there a fix for this?  It's making my eyes hurt."
    author: "Jeff Perry"
  - subject: "Re: Cannot change fonts"
    date: 2001-10-12
    body: "I've got the same issue.  What is the solution?"
    author: "Rahim"
  - subject: "LAME dependencies"
    date: 2001-09-26
    body: "Hi all,\n   I'm trying to install kdebase, but I get dependencies for lame >=2.89 and libmp3lame.so.0. I've looked for a suitable lame rpm on the net -- no luck. I have just finished downloading and compiling the lame source (works OK), but that doesn't solve my RPM dependency issue. Is lame included in one of the other packages that need to be installed prior to kdebase? I HAVE installed the (4) kdelibs -- but that also does not help. Any ideas? Thanx."
    author: "Jim"
  - subject: "Re: LAME dependencies"
    date: 2001-09-26
    body: "Same problem here!"
    author: "VisualC"
  - subject: "Re: LAME dependencies"
    date: 2001-09-27
    body: "Why did he put a dependency for Lame ? what does encodfing mp3 files got to do with kdebase ?"
    author: "Anonymous"
  - subject: "Re: LAME dependencies SOLVED!"
    date: 2001-09-27
    body: "Hi again,\n   I just noticed that the opennms.org link contains more up-to-date files than found on the KDE site -- including LAME as well as a new kdebase (plus other stuff as well). Cheers."
    author: "Jim"
  - subject: "can't send email with kmail 2.2.1"
    date: 2001-09-27
    body: "I've tried both the 'sendmail' and 'SMTP' options.  Netscape uses SMTP, and it works fine.  'Mail' uses 'sendmail' (local to my machine), which also works\n\nWhen I send mail, I get the message (on the bottom line of Kmail)\nUnrecognized transport protocol, could not send message"
    author: "Joe VanAndel"
  - subject: "Re: can't send email with kmail 2.2.1"
    date: 2001-12-18
    body: "As long as you have Sendmail configured properly, the following will fix your problem.\n\nGo into Kmail \"Settings\".  Anywhere you find the Sendmail entry as such:\n/usr/sbin/sendmail\n\nChange every instance to:\nfile:///usr/sbin/sendmail\n\nYour problem will then be corrected and again, as long as you have Sendmail configured properly, your messages will complete their journeys successfully.\n\nLive long and prosper!\n\nKurt"
    author: "Kurt Schanaman"
  - subject: "send mail"
    date: 2002-04-22
    body: "i tried file:///usr/sbin/sendmail and it still wont work. Kmail suck, one day it\nworks the next it a pointless program."
    author: "josh"
  - subject: "Re: can't send email with kmail 2.2.1"
    date: 2004-05-16
    body: "Actually, /usr/sbin/sendmail works fine but make certain that you empty your outbox before trying to send mail again or you will get the same error. Kmail now by default will not send anything until the first listed message in the outbox is sendable..p"
    author: "Peter"
  - subject: "Re: can't send email with kmail 2.2.1"
    date: 2002-09-11
    body: "First install kdebase to ur system then try to end mail using smtp..\n\n\nManish Jain"
    author: "Manish Jain"
  - subject: "CAN'T SEND EMAIL TO MY FAMILY"
    date: 2004-02-14
    body: " I THINK YOU SHOULD BE ABLE TO SEND EMAIL TO ANYBODY!!!!!!!!!!!!!  PLEASE FIX THIS PROBLEM"
    author: "WAYNE MITCHELL"
  - subject: "Re: CAN'T SEND EMAIL TO MY FAMILY"
    date: 2008-04-09
    body: "LOL, it is now 2008 and this still isn't fixed.\nI spend an hour trying to figure out why smtp wasn't working. Oh you need to remove the first message in the outbox before you can send a new email. Well, of course, silly me...\n\n"
    author: "Henk"
  - subject: "Thank you Ben!"
    date: 2001-09-29
    body: "U R A god."
    author: "Rokko"
  - subject: "Thank you Ben!"
    date: 2001-09-29
    body: "U R A god."
    author: "Rokko"
  - subject: "how  about solaris pkgs"
    date: 2001-10-01
    body: "2.1.1 is on ftp.patriotsoft.com but they haven't released any since."
    author: "Mark"
  - subject: "Thank you Benjamin Reed!"
    date: 2001-10-02
    body: "Hello. I currently develop beowulf software for the Naval Surface Warfare Center in Bethesda, MD. I had been using KDevelop 1.3 for development, but I wanted to upgrade to the latest one, which requires and is bundled with KDE 2.2.1. Using his \"unofficial\" Red Hat 7.1 packages, it took me about 1.5 hours to upgrade. It took me a while to locate and take care of certain dependencies, but persistence pays off.\n\nKDE 2.2.1 is a beautiful and functional interface. KDevelop 2.0.1 seems an order of magnitude better than the version 1.4 packaged with Red Hat 7.1 (no more 'unknown child process died' messages').\n\nI would recommend the upgrade. Thanks for all of the help in response to other users questions. That helped me out a lot."
    author: "Greg Hildstrom"
---
<a href="mailto:ben@opennms.org">Benjamin Reed</a> wrote in to tell us that he has helped out RedHat's KDE users and put together KDE 2.2.1 RPMs for RedHat 7.0 and 7.1.  &quot;<em>Since I got such a great response for my "unofficial" RPMs last time, I thought I'd do it again.  After what seems like years of building, I've got everything together.</em>&quot;  The packages are available via <a href="http://www.opennms.org/~ben/kde-2.2.1/">http</a> or <a href="ftp://ftp.opennms.org/people/ben/kde-2.2.1/">ftp</a>.
If you can mirror these packages, please let him know.  (He adds:  &quot;<em>Coming soon: RedHat 6.2 packages -- who needs a life?  =)</em>.&quot;




<!--break-->

