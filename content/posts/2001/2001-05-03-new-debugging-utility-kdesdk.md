---
title: "New Debugging Utility in kdesdk"
date:    2001-05-03
authors:
  - "rmoore"
slug:    new-debugging-utility-kdesdk
comments:
  - subject: "Cool"
    date: 2001-05-03
    body: "This is amazing:\n\nIntegrate this and something like gvd and qtdesigner into Kdevelop and you have the equivalent of Borlands C++ Builder*.\n\nI hope the next beta rpms come out soon!\n\n\n\n* maybe all these things are already integrated,\nsince I still do the layout by hand, maybe I am\ntalking BS. I even have to start make myself familiar with tmake, since maintaining my makefiles by hand starts to take to much time."
    author: "compile lazy"
  - subject: "Re: Cool"
    date: 2001-05-03
    body: "Sigh... then again if people followed KDE news... well, maybe that would take all their time. ;-)\n\nHere's the deal. First there are two good tools for KDE, Kdevelop and (theKompany) Studio. Second QT Designer is integrated in Kdevelop and third... this will all be old news soon.\n\nThe new Kdevelop is called Gideon and it is under development to work with mulitple languages. For instance if you want to work with Java there are full Java bindings for KDE and Gideon is equiped to develop Java. Also on the horizon is QT 3. In the next version there will be a number of new widgets along with an improved QT Designer. The next version of QT designer will be able to work on the full UI not just dialogs among other improvements.\n\nIn case you're not putting this all together... the Kdevelop of the not too distant future will have an ingetrated RAD GUI tool for your entire interface and allow you to choose from a number of languages to develop while offering such advanced widgets as rich text tools, data access widgets and anything else in QT and kdelibs.\n\nInterestingly you can look at all this stuff if you are not afraid of CVS or FTP and a compile. ;-) Instead of hypothesizing you could be making the future. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: New Debugging Utility in kdesdk"
    date: 2001-05-03
    body: "I want to know if there are proyects or some roadmap that says that KDE-Designer (Qt-Designer) or a GUI-Builder will be integrate into Kdevelop. I think that Kdevelop need some GUI-Builder (alo Glade) to be the definately RAD por writte aplications for KDE and for others languajes and plataforms (gtk, gnome, java, tcl/tk, etc...)."
    author: "Daniel"
  - subject: "Re: New Debugging Utility in kdesdk"
    date: 2001-05-03
    body: "QT Designer is alredy integrated into KDevelop.\nAs for multiple languages, check out the comment above on Gideon.\n\nThis is why i love OSS. Make any feature request on a public msg board and most of the time your answer is \"already under development, look for it in a couple of releases\" :-)"
    author: "Carbon"
  - subject: "Re: New Debugging Utility in kdesdk"
    date: 2001-05-04
    body: "Are you sure that Qt-Designer is already integrate into Kdevelop? I mean, two diferent applications (kdevelop and Qt-designer) that can interoperate together or one application named Kdevelop that has a GUI-designer (alo Borland C++ Builder)? And if Gideon will have multiple languages, will be able the GUI-Builder for the new languages? I mean, a GUI-Builder for Java por example, using the Java widgets .... Thanks :-)\n\nDaniel"
    author: "Daniel"
  - subject: "Re: New Debugging Utility in kdesdk"
    date: 2001-05-04
    body: "Kdevelop and Qt-Designer are seperate apps, but Qt-Designer can be configured to use KDE widgets, and it outputs xml gui files, which you can then load with a bit of code, afaik.\n\nAny kde app, including those built with another language that has been bound to kde, should be able to load those files. I don't know about other lang's widgets."
    author: "Carbon"
  - subject: "Re: New Debugging Utility in kdesdk"
    date: 2001-05-03
    body: "there is any code complete project to kdevelop?"
    author: "mentat"
  - subject: "code completion"
    date: 2001-05-04
    body: "there is something in kdestudio by thekompany.\nThere are at least plans for kdevelop.\nHava a look at the mailinglist"
    author: "ac"
  - subject: "code completion"
    date: 2001-05-04
    body: "there is something in kdestudio by thekompany.\nThere are at least plans for kdevelop.\nHava a look at the mailinglist"
    author: "ac"
  - subject: "code completion"
    date: 2001-05-04
    body: "there is something in kdestudio by thekompany.\nThere are at least plans for kdevelop.\nHava a look at the mailinglist"
    author: "ac"
  - subject: "Re: New Debugging Utility in kdesdk"
    date: 2001-05-04
    body: "AFAIK the code completion support is well on the way in KDevelop, although there has been some discussion of how the new gcc can help.\n\nDon't quote me on this as I am not a KDevelop developer...just what I picked up. Check http://lists.kde.org/ for more info.\n\n    Jono"
    author: "Jono"
---
Debugging code is always a pain in the neck, but there's a tiny new utility in
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdesdk/">kdesdk</a> which might make things a little less stressful. The utility, <a href="http://dot.kde.org/988852243/kspy.png">KSpy</a>, gathers information about the state of a running KDE application and
displays it graphically.  Read more below.



<!--break-->
<br><br>
The core of KSpy is a tiny library which displays a tree of all the QObjects in
an application.  Selecting an object allows you to view its properties, and the
signals and slots it defines. This information is already available to
developers via the Qt methods <a href="http://doc.trolltech.com/qobject.html#af854e"><TT>QObject::dumpObjectTree()</TT></a>
and <a href="http://doc.trolltech.com/qobject.html#57f1a6"><TT>QObject::dumpObjectInfo()</TT></a>,
but their output is not particularly convenient (and sends a lot of messages to
the console if you forget to remove it!). KSpy has only minimal overhead for the
application, as the kspy library is loaded dynamically using <a href="http://developer.kde.org/documentation/library/2.1-api/classref/kdecore/KLibLoader.html">KLibLoader</a>.
You can see a picture of the KSpy probing it's own test program in this <A HREF="http://www.ipso-facto.demon.co.uk/development/kspy.png">screenshot</A>.
<p>

To use KSpy from your code, first build and install kdesdk/kspy, include the
header kspy.h, and finally call <TT>KSpy::invoke()</TT> when you want to run the probe.
You can easily wrap this call in a slot so it can be bound to a keystroke or an
action. In the future, KSpy will probably gain a few more abilities such as the
ability to watch the events an object receives and the ability to see which
config files are in use, but it should be pretty useful already.

<p>

While you're looking at KSpy, you should check out some of the other goodies in
kdesdk - this module has a whole host of tools for developers:
<ul>
<li><b>KBabel</b><br>
  A tool for application translators which makes it easy to edit .po files.

<li><b>KAppTemplate</b><br>
  A script that can generate the basic framework for a KDE application.

<li><b>KMTrace</b><br>
  A memory leak hunting tool.

<li><b>KStartPerf</b><br>
  A tool for measuring app startup times.

<li><b>Scripts</b><br>
  Scripts for tasks such as packaging an app from CVS, updating your local copy
  of KDE and more.
</ul>


