---
title: "Tutorial:  Extending the KDE Panel"
date:    2001-05-01
authors:
  - "Dre"
slug:    tutorial-extending-kde-panel
comments:
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2001-05-01
    body: "can someone please help me with a problem that I have with the extension child panel.  I dislike the extension taskbar panel but i get my desired result when i use a child panel with the taskbar applet enabled.  Problem is that everytime I restart KDE the applet does not start with the panel.  I have to add it everytime.  Anyhelp on how to get the taskbar applet to start with the child panel when i load KDE.\n\nThanks."
    author: "Anthony Lander"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2001-05-01
    body: "try adding it to your /usr/bin/startkde or ~/.kde/Autostart directory"
    author: "fix"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2001-05-02
    body: "What command do I add to the \"startkde\" script.  I need a command that would start the child panel applet at the top of my screen with a taskbar applet running in it."
    author: "Anthony Lander"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2001-05-05
    body: "I made up a quick shell script to do this.. it only handles one child applet right now and uses DCOP to add the applet.  Just take the shell script at the end of this post and save it in /usr/bin/kickapplets.sh (or wherever else you want it) and then edit /yourhomedir/.kde/share/config/kickerapplets and add the .desktop files one by one.. mine looks like\n\nktaskbarappler.desktop\nclockappler.desktop\n\nThen put an entry in your Autostart folder or /usr/bin/startkde to call 'sh /usr/bin/kickerapplers.sh' and your done.  \n\nDoes anyone have any hints as to how to set the initial size and position of applets? this is the only annoying thing left.. all my applets get crammed on the left side of the screen.\n\n----- begin sh script\n#!/bin/sh\n \nappletfile=\"$HOME/.kde/share/config/kickerapplets\"\nchildDCOPid=`dcop kicker|grep ChildPanel`\necho \"ChildPanel DCOP id is: $childDCOPid\"\n \nfor i in `cat $appletfile`\ndo\n        echo \"Adding $i to $childDCOPid\"\n        dcop kicker $childDCOPid addApplet $i t\ndone\n-----end sh script"
    author: "Hitesh Patel"
  - subject: "Cool applet!"
    date: 2001-05-03
    body: "Wow... that kfifteenapplet is indeed very useless but addicting\nWill it be in 2.2?"
    author: "Proton"
  - subject: "Bug #23234, I've fixed it"
    date: 2001-05-04
    body: "This is bug #23234, and debian bug #93006 ( http://bugs.debian.org/93006 ). I found the problem and submitted a patch to Debian, which AFAIK is in the latest Debian packages in their pool (hence that Debian bug is marked closed). I don't know whether anyone's sent it \"upstream\" yet.\n\nIf anyone's interested, here's the (very small) patch to fix it. Someone just forgot to call _containerArea->init() in the ChildPanelExtension constructor, so it doesn't load the settings. Looking at the files shows it's fine saving them.\n\n-- \nChris Boyle\nWinchester College"
    author: "Chris Boyle"
  - subject: "Also..."
    date: 2001-12-08
    body: "Is it an applet?\nIf so then under preferences on the panel (right click) click the applets tab, and make sure that that applet is trusted and loads internally. This assumes that you trust it of course..."
    author: "Lo Wang"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2005-08-09
    body: "can anyone tell me what are the files responsible for the panels ............\n\nlike if I want to modify the panel for all users before making the installation CD....... here i am trying to make a knoppix based distribustion.\n\nThanks in advance\n"
    author: "linneighborhood"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2001-05-01
    body: "Ahhh, very nice. I've written a few applets and I quite like the API. What's stopped me from releasing them was the Makefile.am stuff, which the tutorial talks about.\n\nComing soon to a KDE application page near you:\n\nKSetiApplet\nKRadioApplet\nKBubbleMonitorApplet\n\n--ernie"
    author: "Ernie"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2001-05-01
    body: "Can anyone show me how to add the Show Desktop applet to the quick launch applet, since it saves panel space."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2001-05-02
    body: "Configure Panel --> Add --> Desktop Access"
    author: "HGG"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2003-03-25
    body: "Can anyone tells me how to run the thing is launch when I click on the \"Desktop Access\" Button.\n\nI have a extended keyboard (with special key, and with lineakd i can run kcalc, netscape, etc...) but I want to assigne a special key to \"Desktop Access\" (like CTRL+ALT+D)\n\nAny idea ?\n\nThanks"
    author: "BRACH"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2003-03-25
    body: "CTRL+ALT+D already works for me by default.  You can change it in the Global Shortcuts menu under Panel->Toggle Showing Desktop."
    author: "Navindra Umanee"
  - subject: "Does it *have* to go all the way across the screen"
    date: 2001-05-02
    body: "Can someone make the panel do what GNOME's does in terms of not having to go all the way across all three of my monitors at 1600x1200? That behavior really sucks."
    author: "Johna"
  - subject: "Re: Does it *have* to go all the way across the screen"
    date: 2001-05-02
    body: "I agree on this one, I have a widescreen 16x10 monitor where running at 1920x1280 wastes a lot of space along the taskbar."
    author: "Ben Meyer"
  - subject: "Re: Does it *have* to go all the way across the screen"
    date: 2001-05-02
    body: "Ask and ye shall recieve!  Actually, the KDE developers are one step ahead of you - its been in CVS for a while (which means it'll be in 2.2), along with the ability to have only one panel-hider button instead of two, a couple more panel applets, faster Konqeror, more and better window decorations (IceWM theme importer!), more KControl modules (and improvements to existing ones, such as the screensaver module - now I have almost *twice* as many screensavers available!), more bugfixes, IMAP in KMail, and more!  Everything people have been asking for and some stuff that they haven't even thought to ask for yet!\n\nHee hee hee...  I love having CVS!  Go KDE!"
    author: "not me"
  - subject: "Re: Does it *have* to go all the way across the screen"
    date: 2001-05-02
    body: "Now if you could also be able to hide it on individual virtual desktops like you could with kpanel.  I like kicker, but I don't need it on every desktop..."
    author: "Anon"
  - subject: "clock in mac style menu bar"
    date: 2001-05-02
    body: "could there be a clock in the mac style menu bar?"
    author: "horst"
  - subject: "Extending KDE"
    date: 2001-05-02
    body: "One thing I was wondering about is whether KDE applications can be extended in the same fashion as third-parties manage to extend Microsoft Office and especially Internet Explorer.\n\nJust look how deeply the Internet Explorer Powertools (or however they called this extensions pack) manages to hook into IE. Copernic's (www.copernic.com) search tools also manage to plug into all of IE's context-menus, allowing you to perform a search of any word inside the page you're seeing (not to mention integrating into the toolbar and menus).\n\nMy credit card company has recently begun offering disposable credit card numbers, which are generated and filled-in simply by clicking a button on the IE toolbar. Assuming they wanted the feature for Linux as well, would they have an actual ability to build it?"
    author: "Toastie"
  - subject: "Re: Extending KDE"
    date: 2001-05-02
    body: "My credit card company has recently begun offering disposable credit card numbers, which are generated and filled-in simply by clicking a button on the IE toolbar. Assuming they wanted the feature for Linux as well, would they have an actual ability to build it?\n\nIn a word - yes.  In fact, you can add a tool to any properly written KDE application, not just Konqueror.\n\nThere was even a tutorial posted to the dot recently (that created a HTML Validator), that immediately spawned a Babelfish translator plugin and quick Java/Javascript/cookie configure plugin. See http://dot.kde.org/980429423/ for more information.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2001-05-02
    body: "Is it possible to configure kicker in order to use different panel applets for different virtual desktops ?\n\n\nThanks again for KDE"
    author: "xavier"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2001-05-02
    body: "A propos panel, is it still possible to swallow\napps in the panel?\n\nI found some documentation about using\n.kdelnk files for swallowing apps but this\nnever worked for me with KDE 2.x.\n\nAny help?\nOly"
    author: "Oliver Tonet"
  - subject: "Settings vanishing?"
    date: 2001-05-03
    body: "When I log out from kde some settings (I mean child panel applets and some changes to the icons of application button) are lost. Every time.\nI use Mandrake 8.0 and kde-2.1.1\nAny suggestion is appreciated."
    author: "striscio"
  - subject: "Re: Settings vanishing?"
    date: 2001-05-03
    body: "It's a bug, it's been fixed in CVS long ago.\n\nSee \n\nhttp://bugs.kde.org//db/22/22660.html\nhttp://bugs.kde.org//db/23/23051.html\nhttp://bugs.kde.org//db/23/23857.html\n\nand\n\nhttp://lists.debian.org/debian-kde-0104/msg00274.html"
    author: "Jens"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2001-05-09
    body: "It's great to see the improvements in kicker (as well as all other parts of KDE!), but would it be out of line to suggest making it more GNOME-like, in terms of flexibility sometime in the future? Recently I played around a bit with the GNOME panel, and was really impressed. Some of the features I'd like to see eventually:\n\n1. No distinction between \"child\" panels and the main panel. Right now, it's kind of awkward to work with the child panel, and it doesn't work quite the same as the main one (And IIRC you can only have one child panel onscreen). \n\n2. More mobility. Allow panels to be positioned along any edge or corner of the screen; improve resizability (I'm using a CVS version right now that allows short panels, but it's a little buggy); perhaps the ability to slide them around easily.\n\n3. Some way to group items in \"sub-panels.\" I'd like to see a scenario like this: Say I want to put lots of app buttons on a panel, but I want some to be small and some large. I could create a sub-panel object to hold, say, the small icons, and that object would rest on the panel. The subpanel would work pretty much like a regular panel: you could configure it to show large/small/medium icons and other things like that.\n\n4. Better taskbar and/or kasbar. It'd be nice to be able to configure it to \"group\" windows by certain criteria (window caption, class, etc.) so that all windows of a group would only show up as one item on the taskbar; then clicking on that button would pop up a menu of the actual windows.\n\n5. Ability to edit and move menu items right in the menus, like in Win98 and above. In my experience it saves a lot of time not having to open a menu editor app.\n\nMy 2c."
    author: "Rakko"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2001-05-10
    body: "I agree with the suggested improvements.\n\nA workaround for #5 which may save you some time, is to drag a shortcut to the panel, make the changes from there, then remove the extra shortcut.  It adds two extra steps, but still saves considerable time compared to loading a menu editor and trying to find the right item again."
    author: "Warren E. Downs"
  - subject: "Re: Tutorial:  Extending the KDE Panel"
    date: 2005-12-06
    body: "There is a fair contingent of windows users that never edit their menus and aren't aware that they can do so.\n\nI've noticed users who migrate quickly cotton onto how to do it with kde and lots of those use that knowledge to improve their menus at some point.\n\nI think that how things are now is pretty usable.\n\nIn place editing would be a boon perhaps, but it shouldn't replace what we have now entirely.\n\nKDE is highly configurable, it's the main strength of KDE over any other DE because no other DE does it in such a mature and easy-to-use way.\n\nLet's not start limiting everything and obfuscating functionality to make things appear simpler (until you want to configure anything, that is).\n\nHCI guidelines that go down that road are tosh."
    author: "Gorky"
  - subject: "How do I get XMMS gnome panel applet on Kicker??"
    date: 2003-08-01
    body: "Does anyone knows how to get the gnome panel applets to work on KDE? Like I want this XMMS applet which is on gnome panel to work on KDE."
    author: "Anup"
  - subject: "Re: How do I get XMMS gnome panel applet on Kicker"
    date: 2003-11-21
    body: "check this link:\n\nhttp://rpm.pbone.net/index.php3/stat/4/idpl/854247/com/xmms-kde-3.1-2-RedHat9.i386.rpm.html"
    author: "anony"
  - subject: "Could someone send me the tutorial"
    date: 2007-03-12
    body: "I've tried to see the tutorial but the link doesn't work. I know this is an old tutorial but I need to learn to programme applets for KDE panel.\n\nMy email is dacedos@gmail.com"
    author: "Dani"
---
<A HREF="mailto:elter@kde.org">Matthias Elter</A> has written a <A HREF="http://master.kde.org/~me/panelapplets/panelapplets.html">tutorial</A> (<A HREF="http://master.kde.org/~me/panelapplets/panelapplets.ps">print version</A>; <A HREF="http://master.kde.org/~me/panelapplets/panel_applets_tutorial.tar.gz">example source tarballs</A>) on developing panel applets for Kicker, the KDE 2 panel.  Kicker is a complete rewrite from the KDE 1 panel; this was done mainly to increase the panel's extensibility.  KDE 2.0 introduced the panel applet API and KDE 2.1 added the panel extension API.  The tutorial teaches you how to implement a simple panel applet using the panel applet API. From the tutorial:  <EM>"Both the applet and extension APIs are simple. This qualifies writing a panel applet as
a suitable task for an introduction to KDE programming."</EM>

<!--break-->
