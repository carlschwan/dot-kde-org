---
title: "theKompany.com releases BlackAdder"
date:    2001-01-10
authors:
  - "sgordon"
slug:    thekompanycom-releases-blackadder
comments:
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "The fine editors (and I do mean that) made a mistake when they added the url for the screenshot, so here is one that works.\n\nhttp://www.thekompany.com/products/blackadder/screenshots.php3"
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "Damn, sorry. The trouble is the editing interface loses the attachment every time you make a change so the picture fell off when the title was fixed. It's attached to this comment for those who want a local link.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "Hey, great idea!\n\nGreat choice of name too, Black Added was always my fav English comedy, and keeps well with Python's Monty Python roots!\n\nHm, any chance of a 30-day evaluation download like products from other major software vendors (hint, hint)?\n\nCheers,\nJacek\n\nP.S. At least this means that PyQT will be actively maintained. $399 for the business edition seems to be a pretty decent price (the tools I have to use at work cost my employer $3000 for the enterprise version), especially since you get the Qt Windows version included."
    author: "Jacek"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "Oops, mea culpa.  I had to repost your edit in order to update the timestamp.  Sorry for losing your screenshot.\n\nCheers,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "Wow, this is great! And it's something that can stand being released commercially -- although I kind of wish there were a demo version, either time-limited or crippled. Well I hope BlackAdder will generate enourmous income for theKompany! :)"
    author: "Haakon Nilsen"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "WOWOWOWOWOW!  I've been dreaming of this tool for 2 years now.  I'd love to give it a try, but I'm not going to shell out for it until I get to see it in action.  Shawn, can you make a demo version available?\n\nAlso, I need clarification on the licencing.  What does \"for business use\" mean?  What parts are not allowed to be redistributed in the home version?  pyQt?  That's free to begin with!\n\nThanks for making the product I want.  \n\nP.S.\nAny chance of source availability?  Even for a price?"
    author: "Ben Ploni"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "Hi Shawn,\ndoes BlackAdder have autocompletion?"
    author: "hakan"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "Yes, I'd like an IDE that supports tab completion so that I can type:\n\nclass <HIT TAB>\n\nAnd it will finish my project for me."
    author: "ac"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "Let me answer all the current outstanding questions at once.\n<br><br>\n1.  The beta will not have code completion, but it should make it into the final release.  On the off chance it doesn't, then it will be in the next release.\n<br><br>\n2.  We probably will *not* have a demo version, but we are discussing it.  If we do, then the demo would rely on you already having all the components on your system other than our Designer, such as Qt, Python, etc.  No ODBC support would be included.\n<br><br>\n3.  The limitation on the personal edition is by the licenses of the respective toolkits.  So you have Python, which is easy, then Qt so you are limited to using the GPL rules there, and finally mxODBC which has a number of permutations depending on if you already own it or not.  At the moment the are working on enhancing the license for 2.0, the license for 1.1 is similar to the Qt license.  So it just depends on what you want to do with it.\n<br><br>\n4.  Source availability: What source would you like to see available?"
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "Shawn,\nwith all due respect the 30-day version is a must, nothing gets done these days without that.\nGo to Macromedia or Borland and you can pretty much download test versions of nearly everything.\n<br>\nOut of curiousity, looking at the screenshots, they look nice, but aren't really quite up to the level of IDE sophistication as let's say VC++ in terms of the GUI. Is it possible to make dockable windows in Qt, instead of just having lots of overlapping windows, which I find to be a bit of a mess to work with (just look at Gimp)?\n<br>\nGreat job nevertheless, I think I'll be downloading Python + tutorial today. I've had an idea for a Linux/Win32 database-based app for a while now... :-)"
    author: "Jacek"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "Well it's selling pretty well so far :).  Those guys also aren't selling a package for $50 either.  If we can figure out a secure way to do it that isn't going to generate a ton of support requests asking how to install Python or something, then we will do it.\n<br><br>\nRemember, this is just the first release, it will continue to get more sophisticated with subsequent releases.  You will see it evolve into a more VB/Delphi like tool later this year once the KODE framework is complete."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "What's the KODE framework?\nYet another hidden theKompany project that slipped your tongue? :-)"
    author: "Jacek"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "I've talked about KODE in interviews in the past, it stands for Kompany Open Development Environment.  It makes use of our Kore libraries (available on our site) and will be a language agnostic visual IDE like Delphi (we actually started this 18 months ago).  Python is one of the languages it will support.  A lot of the infrastructure for the project is done, we are just working on pulling the pieces together right now."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "Out of curiousity, does Black Adder integrate with Qt Linguist for internationalization?<br>\nCheers,<br>\nJacek"
    author: "Jacek"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-10
    body: "It should, but we haven't made any specific tests at this point."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-11
    body: "Hi Shawn , Great job!. About KODE : did I understand correctly? The visual IDE Part will be totally language agnostic, and you will be able to use lanuages as plu-ins? In that case i bet it will be easy for you to implement language plug-ins for all popular languages (perl,FreePascal, Basic,etc etc ), really easy. This could bring a wealth of Rads for Linux , in less than a year after the completion of KODE. \n  About a trial edition: if you are worried about security, don't be. Because if yor product is as good as it looks it will pop up immediaqtely on Warez servers, so not having a trial edition would just end up hurting you. On the other hand people that have the chance to try a limmited edition won't have to find a (cracked) warez edition to try it out, and thus will  not have to face the temptation of keeping such a version. You see I wouldn't buy a tool , if i havent tryied it out first..."
    author: "t0m_dR"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-11
    body: "Shawn,\nwith all due respect there is one major flaw that your co. will have to address: the ODBC license.\nA per-deployement license for that is out of the Middle Ages these days. The guy wants 5-10 % of the retail price of a product or $500 per PC(!!!). He's smoking something, big time.<br>\nIt makes more sense to buy the enterprise edition of Delphi/Kylix or any Java IDE (probably around 3K) then have to pay such ridiculous licenses for something as basic as ODBC access, something that really adds very little added value to an application.<br>\nConsider developing your own ODBC layer (preferably under a BSD or LPGP license, so it can be used in proprietary apps as well) as your next major project.<br>\nWe are a small vendor of POS touchscreen apps (right now under Windows written in PowerBuilder), but we are very much interested in moving on to Linux and dropping the Win32 version altogether.<br>\nPaying for the ODBC license would cost us more than we pay for our database engine license (+ client access fees) under Windows right now!<br>\nThe ODBC deployment license has got to go completely or be replaced with something with no deployemnt fees.<br>\nUntil that point, I don't see any commercial vendors adopting Black Adder en masse...and that is the market you are targeting with this commercial tool ($399), correct?"
    author: "Jacek"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-11
    body: "I don't think you are reading it right.  By purchasing BlackAdder you are getting certain rights to the mxODBC license.  He recently had someone take his code and brand it as their own and so he got a little touchy and went a little far in the other direction.  The final 2.0 license is being worked out right now and should be much more accomodating.  This is really the best ODBC option for Python, so we wanted to make it available for people, but not all people will make use of it.  If you have a specific scenario you want to address then send me a private email and we can work out the details."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-11
    body: "The website says:<br>\nIncludes the rights to distribute the run-time elements (<u>except mxODBC</u>) with your application. <br>\nThere is no other info about mxODBC, so I went to\nthe original site and those were the licensing terms I found...<br>\nWhat is the real deployment fee for mxODBC then?"
    author: "Jacek"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-11
    body: "It's different depending on your situation.  It is too hard to explain the permutations in the press release.  There are currently hundreds of mxODBC users, so if you already have mxODBC you aren't paying for it twice and you have whatever rights come with it.  Depending on how you want to deploy your application with mxODBC will control what fee's are involved (if any).  We are working on the actual wording with the attorney right now, which is why I wanted to know about ay particular scenario, then I could give you a specific answer.  We will get it all clearly spelled out on the web site shortly, probably the first of the week."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-11
    body: "KODE will natively do C++, the other languages will be for a fee.  We have a short list of ones that we plan on doing immediatly, and then others will be driven by user deman.\n\nWe've been soliciting input on a type of trial edition that people would find acceptable and would cover us as well.  The general consensus is that a full version that has the save ability disabled is the way to go, so that is what we will pursue.  There should be something up in 2 weeks, maybe sooner."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "Will KODE be GPLed or commercial??"
    author: "t0m_dR"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "I don't think we will use the GPL, but I think the C++ one will probably be free, and the language plug in's will cost.  We haven't decided yet."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "Oh , and btw since you are developing KODE , don't forget to give it a good Report designer/generator tool...it's really a *must*"
    author: "t0m_dR"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "That's part of what Kugar is all about :)"
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-11
    body: "What happened to Visual Python, which supports Gnome as well?"
    author: "Kirby"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-11
    body: "The Visual Python project was renamed as VeePee last summer.  It is still active, and once BlackAdder has settled down we will be talking to both the GNOME and KDE people about including it in the base distribution.  We've already had some discussions, but we are just too swamped to follow it up right now.\n<br><br>\nVeePee is at a 1.0 release and works very well currently, it could probably use more widgets, and that is where we would like to hear from people."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-11
    body: "But why create BlackAdder while they already have VeePee?"
    author: "Kirby"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-11
    body: "BlackAdder and VeePee are entirely different things.  VeePee is a set of scripting widgets and abilities built on top of Python to allow you to extend your applications through scripting.  BlackAdder is a GUI development environment for building Python based applications that work on Linux and Windows."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "Hey, if you're thinking about changing name for \"VeePee\" why not reveal by the name that it's about scripts? Apparently there are a number of people who are uncertain about the differences between the \"VeePee\" and \"BlackAdder\" projects. That way you would achive two things at the same time, a nicer name for \"VeePee\" and a product line that's easy to understand.\n\nNo, I'm not getting any great name ideas right now, but if you think \"Python Script\" you get my point. :)"
    author: "Matt"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "There is always the struggle to come up with a name that meets at least 2 criteria.  It should be *good* :), and unique.  The third possible criteria is if the name should be descriptive, typically to be descriptive it will end up violating at least one of the other 2 rules.  Your point is well taken, but you can see how difficult it can be.  Just look at the name change Helixcode is going through :)."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "Yes, but as soon as people hear about VeePee and understand the origin - Visual Python - they will think of a BlackAdder type program."
    author: "Matt"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "Is BlackAdder open source?\nThey are talking about \"trial versions\" and \"demos\".\nIs it commercial proprietary software?"
    author: "Kirby"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "BlackAdder is composed of a number of pieces.\n\n1. Qt - open source\n2. A modified version of Qt Designer, which is not open source.\n3. Python - open source\n4. PyQt - open source\n5. mxODBC - not open source"
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "why is this kde news?"
    author: "sm"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "Ehh? It's a KDE program."
    author: "Matt"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-13
    body: "I was going by the posting description. Does this program uses the kde libraries? (at least in the linux version)"
    author: "sm"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-13
    body: "It is using PyQt and Qt on all platforms."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "why is this kde news?"
    author: "sm"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-02-01
    body: "Well it's simple really.\n\nThe news is (trumpets, drum roll please)\n\nIf you want the best development tools and software for Linux you've gotta pay for them (and then watch them get developed)\n\nOoo...err...that seems wrong somehow....if you're going to pay for Delphi-like developement tools why not just get 'em from Borland?\n\nCan't wait for Visual Studio 6.0 for Linux..Bill was right, down with opensource, long live thekompany :o)\n\napt-get install gnome tomorrow I think"
    author: "Michael"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-12
    body: "BTW, you should hire Torsten Rahn to make more elegant buttons for you. :)"
    author: "Matt"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-15
    body: "Ask Trolltech :-)\n(It's a modified Qt Designer AFAIK)"
    author: "Lukas"
  - subject: "Re: theKompany.com releases BlackAdder"
    date: 2001-01-15
    body: "That is correct.  Qt Designer was such a good program and did so much of what we wanted that it made sense to use it for what we wanted to do.  We made some modifications for the support of Python and PyQt."
    author: "Shawn Gordon"
  - subject: "Still unsure about licensing"
    date: 2001-01-15
    body: "<p>Hello, Shawn - you've got us all *REALLY* excited about Black Adder (I ordered a copy on Friday). Although there has been much discussion (both here and on the PyKDE mail-list) about the product, I'm still uncertain about fair usage policy. When you release your products to our community, you should try to be as specific as possible about what rights (or limitations) you'd like us to have when purchasing them.</p>\n\n<p>In the free software world, the term \"re-distribute\" is very construed. Does this mean someone cannot produce a GPL'd program using the personal edition? What about internal usage within a non-profit? Also, what constitutes a \"Black Adder\" component? Someone that just codes some basic python modules, and simple Qt interface might be unsure of this (because of python & Qt both being freely redistributable). I'm not trying to be difficult; maybe I even missed your detailed explanation of this somewhere. I just want to make sure I (and the others in our community) fully understand.</p>\n\n<p>Anyways, GREAT WORK, guys! I can't wait to get my copy!</p>\n\nEron Lloyd"
    author: "Eron Lloyd"
  - subject: "Re: Still unsure about licensing"
    date: 2001-01-15
    body: "Hi Eron,\n\nThanks for the support :).  Here is how it works in black and white.  If you buy the personal edition you can distribute your code without impact, you cannot distribute Qt and mxODBC, the user has to get a copy of that by having either owned them or downloading the appropriate version for their situation.  In the case of a KDE person, they already have Qt, so there is no problem.\n\nThe Business Edition allows you to redistribute Qt for use with PyQt only, and in fact there are limits that make using Qt with anything else pretty much physically impossible.  This product is in no way designed to bypass the Qt license, it is meant to open up Qt programming to another group of people.\n\nThe mxODBC license for the business edition has a few distinctions depending on the deployment required and we are working on getting the final wording posted."
    author: "Shawn Gordon"
  - subject: "Re: Still unsure about licensing"
    date: 2001-01-15
    body: "<p>Right, so even if I produce a GPL'd program for distribution, like most applications, the end user must supply only Qt on their end. I'm interested (and maybe this is more of a question for Phil) in how Black Adder packages an app - does it create a tar of all necessary files (.py, .uic, images, etc) and what about support libs (like Qt binding files, SIP stuff, etc.) Is this packaged as well?</p>"
    author: "Eron Lloyd"
  - subject: "Re: Still unsure about licensing"
    date: 2001-01-15
    body: "The current plan is to include DistUtils after the beta as a way to package up your application, we are looking at it right now."
    author: "Shawn Gordon"
  - subject: "Re: Still unsure about licensing"
    date: 2001-01-16
    body: "PyQT for windows seems to have QT for windows included. So I won't have to distribute Qt since someone who wants to use my program on windows could download the PyQT package from theKompany website!? Well, my question is: Suppose I wrote a GPLed program with the personal edition of Black Adder and I distribute it without QT, will the user be allowed to use it on windows by downloading Qt (PyQT for windows) himself? Or have I got something wrong?"
    author: "Benno"
  - subject: "Re: Still unsure about licensing"
    date: 2001-01-16
    body: "Starting with the next version of PyQt, I will not be producing a binary package for Windows. So, if you write a GPL program with BlackAdder (Personal Edition), your Windows users must either buy a copy of Qt from Trolltech and build PyQt for Windows themselves - or they buy their own copy of BlackAdder. Obviously UNIX users can still build PyQt for themselves using the GPL Qt."
    author: "Phil Thompson"
  - subject: "Re: Still unsure about licensing"
    date: 2001-01-16
    body: "So PyQt is not a very good solution for developing cross-platform (at least Linux and Windows) GPLed applications. wxPython seems to be the better choice, though it uses gtk on Linux/Unix (wxGTK)..."
    author: "Benno"
  - subject: "Re: Still unsure about licensing"
    date: 2001-01-16
    body: "I'll be frank: if I develop a GPL app I would only be interested in a Linux version, forget Windoze, it doesn't deserve my time, IMO.<br>\nIf I'm doing a commercial app, then I would do both, 'cause that's where the $$$ is. Obviously, in that case I can affort to buy BlackAdder Business Edition.<br>"
    author: "Jacek"
---
<a href="http://www.theKompany.com/">theKompany.com</a> is pleased to announce the release of its Windows/Linux GUI development environment for Python, <a href="http://www.thekompany.com/products/blackadder/">BlackAdder</a>.  BlackAdder combines a <a href="http://www.thekompany.com/products/blackadder/screenshots.php3">visual design environment</a> with debugging, syntax highlighting, ODBC interfaces and extensive HTML documentation into a comprehensive platform for developing Python applications. <b> Update: 01/14 10:50 AM</b> by <a href="mailto:soudan@kde.org">wes</a>: The press release has been updated with corrections from Shawn Gordon.








<!--break-->
<p>
We worked with Trolltech, the creators of the Qt windowing toolkit on which BlackAdder is based, and eGenix, creators of the multi-platform ODBC interfaces for Python known as mxODBC, and were able to derive a license arrangement to bring the most power and flexibility to BlackAdder. 
<p>
What is especially exciting that BlackAdder not only runs on Linux and Windows, it generates applications that will run on either system as well. This protects your investment in development by enabling you to develop and deploy your applications almost anywhere. Unlike similar products, BlackAdder includes all the elements needed for developing complete applications and the rights to re-distribute the necessary run-time elements, highlights of the BlackAdder are:
<p>
<ul>
<li>Organizes your Python scripts and GUI designs into easy to manage projects.
<li>An editor that includes highlighting of Python keywords and code folding.
<li>An interactive Python interpreter (v2.0) that allows you to execute any Python commands while your application is running.
<li>A license to access the Qt GUI toolkit API (v2.2.3) from Trolltech
via the PyQt Python bindings only.
<li>Qt Designer from Trolltech with a modified user interface compiler
that generates Python code.
<li>A debugger that supports single stepping and 
breakpoints.
<li>ODBC database connectivity (using mxODBC v2.0 from eGenix).
<li>Over 50Mbytes of HTML documentation.
</ul>
<p>
BlackAdder is available in two editions, "Personal" aimed more at the home hobbyist and "Business", both editions produce the same high quality, professional applications the only restriction is the ability to create proprietary applications for resell.  The pricing and differences are described below, note the special discount pricing during the initial beta period.
<h4>BlackAdder Personal Edition 
Includes:</h4>
<ul>
<li>Python
<li>PyQt
<li>Qt (for use with PyQt only)
<li>Comprehensive HTML documentation 
<li>mxODBC (ODBC for Python) 
<li>IDE (including GUI designer, editor and debugger)
<li>20% discount voucher for any book from O'Reilly
</ul>
<b>License:</b> For personal home use only, not allowed to re-distribute any BlackAdder components. 
<b>Cost:</b> $79.99 ($49.99 if purchased during the Beta period).
<br>
<b>Support:</b> Informal mailing list. 
<br>
<b>Updates:</b> $59.99 per year for up to 4 updates per year ($39.99 during the Beta period)
<p>
<h4>BlackAdder Business Edition 
Includes:</h4>
<ul>
<li>Same as the Personal Edition.
</ul>
<b>License:</b> For commercial use. Those BlackAdder components needed to provide a run-time environment (ie. Python, PyQt, Qt) may be re-distributed at no extra cost. One mxODBC license is included in the purchase price, valid mxODBC licenses must be obtained for any additional installations of generated software using the mxODBC component.
<br>
<b>Cost:</b> $399.99 ($249.99 if purchased during the Beta period). 
<br>
<b>Support:</b> First month of support is included, extended support contracts are available on the web site. 
<br>
<b>Updates:</b> $299.99 per year for up to 4 updates per year ($199.99 during the Beta period)
<p>
Detailed information and screenshots can be seen at <a href="http://www.thekompany.com/products/blackadder/">http://www.thekompany.com/products/blackadder/</a>







