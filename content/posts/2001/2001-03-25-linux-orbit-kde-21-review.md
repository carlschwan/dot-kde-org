---
title: "Linux Orbit: KDE 2.1 Review"
date:    2001-03-25
authors:
  - "numanee"
slug:    linux-orbit-kde-21-review
comments:
  - subject: "Re: Linux Orbit: KDE 2.1 Review"
    date: 2001-03-25
    body: "Very cool review.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Linux Orbit: KDE 2.1 Review"
    date: 2001-03-26
    body: "Thanks :)"
    author: "Zodiacus"
  - subject: "Re: Linux Orbit: KDE 2.1 Review"
    date: 2001-03-25
    body: "nice review.. but at the end he says he is waiting for a good ripper for KDE! I mailed him saying that its there in front of him... type audiocd: and see the magic :)"
    author: "sarang"
  - subject: "Any performance gains?"
    date: 2001-03-25
    body: "I have a 233mhz laptop w/ 64 megs of RAM and it looks like I'm stuck with kde1 since kde2 was unusably slow.\n\nBinaries were so bad that I went to the trouble of compiling everything myself (it took almost and entire day the machine is so slow) with recommended -O settings etc. etc. I was still disappointed with the slowness of the interface.\n\nkde1 is the only linux GUI that is anywhere close to the speed of MS-Windows on my machines (all of which are 300mhz and slower - and still the speed at which kfm windows open menus popup etc is at least 2-3 times slower than Windows.  I also have a 233mhz desktop with 128 megs of RAM dual booting win2k and RH 7.0 and the speed difference in the GUI's is measurable with a wrist watch! (gnome is very slow) ... is there any discussions of ways to increase the speed of X/KDE GUI and make it more \"snappy\"? (caching pixels or something?)"
    author: "guy"
  - subject: "Oops that was with 2.0"
    date: 2001-03-25
    body: "so I was wondering if 2.1 was faster than 2.0 for people..."
    author: "guy"
  - subject: "Re: Oops that was with 2.0"
    date: 2001-03-25
    body: "at least file copying/moving/deleting and the khtml rendering engine are *much* faster than in 2.0\n\nkonsole in current cvs starts on my system in 1.8 instead 3.6 seconds as in the 2.1 release :-)\n\nmore work on improving performance is on its way"
    author: "aleXXX"
  - subject: "Re: Oops that was with 2.0"
    date: 2001-03-25
    body: "In my opinion this is at the moment the only thing lacking with KDE. Speed.\n\nI'm always running the latest KDE CVS on my athlon 900, 256 mb, matrox G450 and I must say I'm impressed with how much work is being done *every* day ! However, as I stated above, speed really is far worse when compared to MS Windows. To name a few issues, redrawing of windows is slow, starting apps and popping up dialogs is slow :( I know that a good design as is the case with KDE(2) comes with a few speed drawbacks, that's why I think more has to be done thinking in the direction of preceived speed. For example, opening outlook express is (on a less capable system than mine) popping up the program almost instantly, however it's a few more milliseconds before something can actually be done with it. What I mean is, the user sees a response to clicking the outlook express icon almost immediately (it looks really fast) and after that the user will most of the time rethink what he/she was about to do, so the small delay in the app itself isn't a problem.\nWhen opening kmail (just an example), I get a spinning disk in my taskbar for about 3 seconds until kmail it's window pops up. Same story for konqueror (it starts a bit faster than kmail though). It is exactly that feeling of snappyness that is needed for users to start seeing KDE as a fast and very usable desktop. I'm not very well into the code of the various speed-needing parts of KDE, so I really don't have a clue on how to get things faster.\n\nI know concerns regarding speed have been expressed before, I would therefore like to state that I think the KDE developers kick ass and are doing a superb job. If only it all could be made it little bit more responsive, I could convince so much more users to switch from windows to KDE. And for as far as I can tell that is part of KDE's mission, right ? :)\n\nGood luck,\n\nJelmer Feenstra"
    author: "Jelmer Feenstra"
  - subject: "Re: Oops that was with 2.0"
    date: 2001-03-25
    body: "I have a GeForce 256 DDR with 32mb, and screen updates are blindingly fast. This is due in large part to the native Linux / XFree86 drivers supplied by Nvidia. Perhaps your Matrox card's drivers need more optimization?\n\nKDE2.1 overall seems much faster to me than Windows 98. I too wait 3 seconds for Kmail to launch, but then things are very fast from that point on. I forgive the initial wait considering I have over 10,000 email messages in 75+ folders. I have 90+ complex email filters that can rip through thousands of email messages in mere moments. Searching is also incredibly fast in comparison to Outlook.\n\nWith that said, I myself have noticed that KDE 2.1 seems a tad more sluggish. I think with the release of KDE 2.2 the focus should then be moved to code optimizations in critical portions of the code. It would be nice to have the likes of someone like Michael Abrash take a whack out of converting certain pieces of code into hand written x86 assembly :). We'd probably be looking at a 400% speed increase."
    author: "Sean Pecor"
  - subject: "It could be g++ and gcc"
    date: 2001-03-26
    body: "I agree that xfree86 is the problem with screen redraws. But with other performance issues, I believe g++ may have something to do with it.\n\nMost Windows based compiliers have really improved over the years including Microsofts Visual c++. Some things like polymorphism and inherietence run quicker on newer compliers due to optimization. However g++ is quite awefull and behind the times. I beleive only part of the STL is even supported. This is one of the reasons why gnome is written in c and not c++. \n\nThe STL issue might also explain why redhat chose gcc 2.96 instead of 2.95.2. gcc 2.96 supports the STL better and will make Unix to Linux porting easier. Windows98 is faster then Windows95 is some aspects because Visual C has improved and has been alot more optimized. I believe there were more misses in brach prediction with earlier compiliers when trying to implement standard c++ sutff like inheritence and polymorphism. I know borland has released their linux c++ compilier for free but I haven't tried it with kde yet. I wonder if kde will run faster after being compilied with it.\n\nBut I am glad that KDE did not do the mistake gnome did by chosing c for c++ like work because of compilier worries. I believe when gcc and g++ 3.0 come out, Kde2.1 will run alot faster. I wish i knew some assembler to help out.\n\nThe bright side is that once version 3.0 of gcc and g++ come out it will beat gnome in terms of performance because c is not optimised for c++ features."
    author: "Tim Gibney"
  - subject: "Re: It could be g++ and gcc"
    date: 2002-09-05
    body: "Hello;\nIt could be g++ and gcc.\nthanks"
    author: "sofiane"
  - subject: "Re: Oops that was with 2.0"
    date: 2001-03-26
    body: "About the usage of assembler, I'm afraid we'll quickly loose portability in that case :)\n\nConcerning the screenupdates, It's not that my card has problems putting the pixels on the screen, but more that switching desktops will result in noticing apps popping up (you can see them being 'constructed'). Scrolling for example a large webpage in konqueror is *very* fast :)"
    author: "Jelmer Feenstra"
  - subject: "Re: Oops that was with 2.0"
    date: 2001-05-06
    body: "I doubt that x86 asm will really help that much. These days, ASM is mainly only useful in tight loops, something that's doesn't occur much in a windowing environment. While coding in ASM for the inner loop of a software renderer might gain you some speed (though not as much as it used to back when Abrash wrote his books, thanks to optimizing compilers) it wouldn't do much for a desktop, where performance bottlenecks aren't so closely grouped in one place. The real reason that KDE is slow is because it wasn't designed for performance. While the whole \"dozens of processes communicating over IPC\" is an elegant design, it is also insanely slow. The reason apps take so long to launch is because tons of administrative work has to be done with the object system. Lastly, KDE 2.1 is the fourth layer of software between the app and the hardware (the hardware driver, XFree86 itself, Qt's rendering engine, then KDE.) Compared to something like the windows GDI (which has the hardware driver, then the GDI, both of which run inside the kernel) the design just begs for poor performance. Still, KDE needs to shoulder a lot of the blame for its poor performance. Other window managers (blackbox, Window Maker) manage to provide decent performance while still dealing with X. Methinks that if the KDE developers would stop concentrating on \"nifty\" or \"elegant\" (who cares if you can make remote RPC calls to another object over the internet? Give me a simple, fast messaging system in the kernel any day) and start making the thing usable (I should not be able to *time* the startup time of an app. Even Office 97 starts up instantly on my 300MHz PII running Windows 2000) then a lot more people would find the Linux desktop appealing. \n\nPS> For an example of what *not* to do, look at Nautilus. IIRC, Windows 95 did most of the things Nautilus does (document previews, searches, etc) at 10x the speed."
    author: "Rayiner Hashem"
  - subject: "Re: Oops that was with 2.0"
    date: 2001-03-26
    body: "use something like sawfish blask box or WindowMaker by itself, then in your .xinitrc start kicker or gnome-pannel or something, or buy a comercial X server and a comercial CDE there fast."
    author: "robert"
  - subject: "Re: Any performance gains?"
    date: 2001-03-25
    body: "That's very strange. I'm running on a 200mhz system with 64mb of ram, and kde 2.1 runs at just about the same speed as kde1 (with a slight slowdown when I login, mainly because of aRts). Although it isn't as peppy as windoze, it runs quite fast enough for daily use. If I open a kwrite, kppp, konsole, and konqueror one after the other, it takes about 30 seconds for them all to load, and that seems fairly fast considering my system."
    author: "Carbon"
  - subject: "Re: Linux Orbit: KDE 2.1 Review"
    date: 2001-03-25
    body: "Yeah - K *was* for \"kool\". I think some people have tried to crush that idea, saying it's something after \"CDE\" or whatever. But I remember the messages which started it all. Basically my thought back then was \"Yeah, another group of lame h4x0rs that can't spell. They'll get far...\"\n\n:)"
    author: "EKH"
  - subject: "poor Performance"
    date: 2001-03-25
    body: "I am also very annoyed with the slow/sluggish performance of KDE 2.x. Can't we have a website dedicated to Optimizing and speeding up KDE like http://optimize.kde.org ? KDE 1.1.2 is 10 times faster than KDE 2.x. I feel sorry for that... anyway expecting great performancing from KDE 3.x :)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: poor Performance"
    date: 2001-03-25
    body: "I think this comparison between KDE 1.1.2 and KDE 2.1 is almost the same like the comparison between Win 3.11 and win 98.\nThere win 3.11 just pops up direct and win 98 take a while.\nIt's not based on the same code.\nKDE 2.1 has much more things to load than 1.1.2 does'nt have.\nI think it's wrong to make this sort of comparison.\nBut it's not wrong to try to optimize the code.\nThey are doing a great job!  :)\n\n(Sorry for my bad English but I hope you understand what I mean)"
    author: "Magnus K"
  - subject: "Re: poor Performance"
    date: 2001-07-02
    body: "Linux is an OS that suppose to let old machines with limited resources run... Running KDE2 or Gnome requires a Ghz machine with 1 Gig of RAM. It is incredibly slow kde2... running 2 apps would cause my 64 Meg machine to swap... Windows GUI have more functionality and consume less memory. Win32 guis are always crisp & fast. Plus KDE2 comes with really eyesore fonts. what's with that???"
    author: "Steamboat_Annie"
  - subject: "Re: poor Performance"
    date: 2001-11-08
    body: "KDE is just too slow. I'm running 2 machines side by side, an Ultra 5 270 Mhz with Solaris 8 and CDE and a Pentium 3 550 with Mandrake 8.1 and KDE 2.2. The intel box is dog slow compared to the Sun box - but ONLY when running KDE.\n\nI'm quite impressed with its functionality but they have GOT to make it run faster. ANY version of windows on this hardware gives better performance with respect to the graphical environment."
    author: "Nick Huggins"
  - subject: "Re: poor Performance"
    date: 2001-12-26
    body: "I have to agree with this thread. I've got a Dell PII 450, 384 MB ram. On this hardware, KDE is dog slow. Admittedly, it's faster now that I bumped up the RAM to 384 over the 128 the machine came with, but it's too damn slow."
    author: "Dan"
  - subject: "Re: poor Performance"
    date: 2002-05-21
    body: "I also agree that KDE 2.x. are very slow compare to M$ windows..\n\nBut I love KDE, I setup my computer to dual boot.... RedHat 7.2 or Win98...\nMost of the time when I am not in a hurry I boot the RedHat and run KDE (I can't resist the cleaner and nice looking interface)...but I paid the price for the long boot (have you ever wonder why a regular non technical user has too see all those messages popping? some of them scrolls up so fast that you can't see them, only with dmesg) and slow KDE...\n\nWhen in a hurry no choice... WIN98\n\nyeah yeah yeah put more memory...I have 384Mb\nyeah yeah yeah change the computer....\nUnfortunatelly I can't afford a whopping 1.5 Ghz computer...I have to stick to AMDK6/2 500Mhz...\n\nThe again.... if for every new Linux version or KDE or GNOME we need a better and faster machine....what's new and good about that? Doesn't M$ already do that everytime they come out with a new Win?\n\nI bet WinXP will run dog slow in my computer....\n\nHope the code gets better in version 3.x and don't forget us (the people with the old machines)\n\n\n"
    author: "Patrick Park"
  - subject: "Applications do launch slowly..."
    date: 2001-03-26
    body: "Especially Kmail and Konqueror.  But after this the KDE 2.1 interface is extremely FAST (at least as fast as Win98 on my computer).  I have no problems moving and resizing opaque windows.  I test this by trying to move windows after launching Kmail and a couple Konquerors.  Overall I'm VERY impressed with KDE 2.1 (I used to be a GNOMER).  I never used the first KDE much so I can't really compare it (speedwise) to KDE 2.1\n\nI do have a few complaints though :)\n\n1) I wish Kmail would keep redrawing itself while sending a large mail (Outlook Express does the same thing)\n\n2) I wish I could right click on an entry in the taskbar and kill that application (I'm not a big lover of key combinations like ctrl-alt-escape).\n\n3) I wish noatun would stop freezing on me ehehe\n\nOverall I think GNOME 1.4 has a tough act to follow (especially in the file manager department)"
    author: "Rimmer"
  - subject: "slow kde2 & threads"
    date: 2001-10-04
    body: "I've read some comments and i must say that kde2 runs well on my 266Mhz PentiumII with 64mb ram.\nI use a pIII-800 to build kde. I've compiled kde 2.2 (and qt-2.3.1) a first time with these C(XX)FLAGS: \"-O3 -march=i686\". That worked well (i usually disable most cpu/ram-intensive options, like sounds, different wallpapers etc..). I've then added the \"--threads\" to qt and to the kde-packages that listed this option (or --with-threads, --enable-threads) in their \"configure\"-scripts (it's sometimes marked experimental, so then it's not enabled by default). I compiled the whole beast again, only to see if it would work ok. It did and it seems to be more snappy and faster."
    author: "denbrice"
  - subject: "slow kde2 & threads"
    date: 2001-10-04
    body: "I've read some comments and i must say that kde2 runs well on my 266Mhz PentiumII with 64mb ram.\nI use a pIII-800 to build kde. I've compiled kde 2.2 (and qt-2.3.1) a first time with these C(XX)FLAGS: \"-O3 -march=i686\". That worked well (i usually disable most cpu/ram-intensive options, like sounds, different wallpapers etc..). I've then added the \"--threads\" to qt and to the kde-packages that listed this option (or --with-threads, --enable-threads) in their \"configure\"-scripts (it's sometimes marked experimental, so then it's not enabled by default). I compiled the whole beast again, only to see if it would work ok. It did and it seems to be more snappy and faster."
    author: "denbrice"
  - subject: "Re: slow kde2 & threads"
    date: 2001-10-29
    body: "I am also using a 266Mhz PII but with 256MB. I got a very, very bad performance. I tried with a PIII 700Mhz with 128MB and I could work but I still having performance problems.\n\nI really apreciate if the author of the previews message could send me some information about how to compile the KDE to make it faster.\n\nThanks and sorry for my english."
    author: "Jos\u00e9 Louren\u00e7o Teodoro"
---
While waiting for KDE 2.1.1, it might be worth checking out this <a href="http://www.linuxorbit.com/features/kde21.php3">nice review</a> of KDE 2.1 on <a href="http://www.linuxorbit.com/">Linux Orbit</a>. <i>"With the release of KDE 2.0 on October 23, 2000, the KDE development team upped the ante in the bid for the hearts and minds of GNU/Linux desktop users. With major improvements in features and stability, KDE users couldn't wait for the next version. When KDE 2.1 arrived on February 26, 2001, few were disappointed."</i>  Lots of pretty pictures.




<!--break-->
