---
title: "Qt 3.0.0 Beta6 out, QCom gone"
date:    2001-10-02
authors:
  - "numanee"
slug:    qt-300-beta6-out-qcom-gone
comments:
  - subject: "Unexpected"
    date: 2001-10-02
    body: "Wow! That was kinda unexpected, I think. I've not looked at Qt-3 yet so I've only seen the little info on QCom that was linked from the previous dot story, but to me this seems like a good thing. I do believe linux/unix needs a component system. I do believe we need a crossplatform component system (which is efficient for in-proc calls, unlike corba) but I don't think a system looking this close to COM is the way to go. I've done some C++ COM-programming under windows, and while QCom seemed a bit better, it really isn't even close to the standard of Qt in general, with reference to ease-of-use and iniuitiveness.\n\nThis puts to shame the strange noises being heard sometimes about trolltech only adding features and not fixing bugs.. removing a BIG feature like this to keep the API homogenous is a very impressive decision as isn't, *short-term*, the most commercial one to make.\n\n(btw, the \"corba-vs-com dept.\" seems a bit off..)"
    author: "jd"
  - subject: "Re: Unexpected"
    date: 2001-10-02
    body: "It still wonder if anyone here has looked at recent corba-developments. I am actively working with the C++-ACE/TAO-project. For in-process-calls you have a delay of under 200 Nanoseconds. And it's getting less. Not that i would recommend TAO as an orb for KDE at the moment (compiling-time just sucks). But Corba is the most widely supported cross-platform-standard for distributed applications. And despite the many prejudices it has developed and is maturing... \njust my 2c holler"
    author: "holler"
  - subject: "Re: Unexpected"
    date: 2001-10-02
    body: "How is ORBit?"
    author: "Carg"
  - subject: "Re: Unexpected"
    date: 2001-10-02
    body: "I began recently some testing of omniORB3.\n\nIt appears that for the same level of functionnality,\nit outperforms similar DCOM components by 30%\n(but this is remote invocation, I didn't finished\nthe test for inproc calls, as I want a mean to get\ncomponents loaded at demand as in COM).\n\nHowever, they (the omniORB team) claim that\nlocal invocations take place without extra stuff\n(at least, there'll always be parameters conversion,\nbut they are also present in COM).\n\nBy the way, I do not like the way COM does\nreference counting, and I was not really happy\nwhen I saw Qt was to support a COM-like mechanism.\nNow, I know Qt's guys will take a little more\ntime to remove all the problems COM suffers."
    author: "Christian"
  - subject: "Re: Unexpected"
    date: 2001-10-02
    body: "200 nanoseconds on what type of processor? If it is something like a pentium III 500MHz, that's about 100 clock cycles, which is awfully long for a function call. On my 300Mhz machine, it takes a little over 12 clock cycles to perform a normal (2 32-bit parameters, 1 32-bit return value) COM (C++ virtual function, same thing) call."
    author: "Rayiner Hashem"
  - subject: "Erh"
    date: 2001-10-02
    body: "I'm not quite sure if this is supposed to be good or bad..?\n\nI'm not against QCom myself, but a lot of people seemed to look at this as the way Trolltech would go to take more control (see Microsoft), their decision to not include it just for these specific reasons seems to prove to me that Trolltech is a company that will not exploit KDE (in a bad way).."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Not pushing"
    date: 2001-10-02
    body: "Basically we went through the process a month or two ago in comparing QCom to Korelib to decided what to use in the new HancomOffice modules.  Our kompany was pretty much split on the decision until we sat down and had an in depth review of the whole thing and decided to go with Korelib instead for a variety of reasons, but basically it was more mature and we had been using it for the last year.  Don't let the 0.1 version fool you, it's actually at 0.8 and very stable and has already been used in Kapital and Aethera for some time.\n\nKorelib is GPL and if you want to use it you can, it is documented and includes examples and has been around for over a year now.  We've recently added Python bindings to it and are working on a new update that add's 10 new classes that should be available in a couple of weeks."
    author: "Shawn Gordon"
  - subject: "Re: Not pushing"
    date: 2001-10-02
    body: "shawn,\n\nits not 0.1 thats fooling people, its 0.0.1 ;) I know you guys are doing a great job out there, but you need to keep the websites rolling too! I am using korelib and its excellent.. but I would really like to see it being released as LGPL rather than GPL. My main concern as I already pointed out to you is that I cannot use korelib in a library released as LGPL becuase then anyone wanting to use my library in a comercial application wil have to get a commercial licence for korelib from you. (correct me if i am wrong). \n\nI request you to publish korelib under LGPL so that it gets widely used and accepted.\n\nthanks!\nsarang"
    author: "sarang"
  - subject: "Re: Not pushing"
    date: 2001-10-02
    body: "I ditto the request that korelib be put under the LGPL (or BSD or QPL/GPL, or anything but vanilla GPL). There are are lot of free KDE programs out there that aren't under the GPL which would not be able to use korelib otherwise."
    author: "David Johnson"
  - subject: "Re: Not pushing"
    date: 2001-10-02
    body: "well that's what I get for using my laptop, but in any case it's pretty much at 0.8 at this point and we'll be getting the web site updated soon (yes, we get behind on this at times).\n\nThe idea is a dual license, I think we are even using the QPL, I have to double check.  So it is GPL/QPL, if you want to use it in a commercial product, then you will have to pay a license fee to us.  Publishing it under the LGPL doesn't do us any good.  If a commercial company wants to use it to make money, then they can pay a commercial license.\n\nI'm glad you like it BTW :)"
    author: "Shawn Gordon"
  - subject: "Re: Not pushing"
    date: 2001-10-02
    body: "well, ok but what about a group that wants to LGPL their stuffso that they can give it away to companies as well as the OSS community? GNOME does this. if a Project wanted to do this they would not want to buy a licence, but they would not want to use the GPL either as it would make thier product look less appealing to business. \n\nwhat do you think Shawn?"
    author: "Jeremy Petzold"
  - subject: "Re: Not pushing"
    date: 2001-10-02
    body: "Not to sound like a jerk, but it doesn't matter what we do, people complain.  We have a closed product, people complain, we have a GPL project, people complain, we have a restricted source license, people complain.  It really is a no-win situation for us.  I believe whatever license fee we decide on will be more than fair enough that if someone wants to do something commercial with it or to get the commercial license of it, then they can."
    author: "Shawn Gordon"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "I didn't mean to sound like a critic...sorry, I was just wondering what you thought about the open source group that my want to use your lib in an LGPLd project. obviousely your stance is \" to bad\" which is fine with me, I did not want to start a flame....I actualy love the work you do and can't wait for hancomOffice....I hope it does as good as or a better job at import/export MSWord than SO6......oh...is the personal DB going to convert Access DBs?\n\nJeremy"
    author: "Jeremy Petzold"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Hi Jeremy,\n\nFirst - thanks for all the supportive posts and emails to everyone, it seems the supporters far out weight the detractors :).\n\nWRT to Rekall importing Access.  Technically you should be able to use ODBC to get to an Access table in it's native state, but it's not really feasible to convert the AccessBasic code to Python, at least at this stage.  I do recognize that there will be value at some point in trying to do something like that, but it's rather far down on the list at the moment."
    author: "Shawn Gordon"
  - subject: "Re: Not pushing"
    date: 2001-10-02
    body: "Not to sound like a jerk, but it doesn't matter what we do, people complain.  We have a closed product, people complain, we have a GPL project, people complain, we have a restricted source license, people complain.  It really is a no-win situation for us.  I believe whatever license fee we decide on will be more than fair enough that if someone wants to do something commercial with it or to get the commercial license of it, then they can."
    author: "Shawn Gordon"
  - subject: "Re: Not pushing"
    date: 2001-10-02
    body: ">Not to sound like a jerk\n\nNow that would be a good trick. No matter how many weasel words you use, I suspect you know all too well the difference between LGPL and GPL... and why people complain, and that it is justified.\n\nEvery message from you just confirms that TheKompany is a total Free software quisling... as is KDE as a whole."
    author: "Wiggle"
  - subject: "Re: Not pushing"
    date: 2001-10-02
    body: "You know, it's stuff like this that pushed me towards Qt in the first place.  How about if we close it and not make it available at all - would that be better?  There is no justification for complaining about something you are getting for free.  That would be like me coming to your house for a party and then bitching because I didn't like the food and drinks you served."
    author: "Shawn Gordon"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Shawn, this makes no sense to me. This crap is what had you go Qt? What you wrote elsewhere about the horrors of making binaries for *every* conceivable distro and solving that with statically linking just Qt and not using the KDE libraries made a lot more sense to me.\n\nI actually more or less expected you to lose patience one of these days, I'm only sad to see it happen. I for one respect you and theKompany in it's attempts to get a business working on top of the KDE/Linux environment. It really is a shame if these half-wits finally got to you. I can't say I blame you though."
    author: "jd"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "To tell you the actual story, it was people pushing my buttons one day (complaining about contributions we had made) that got me really frustrated, so I started mapping some things out.  I had always had a single minded determination for KDE, never gave Gnome a second thought, and had only grazed upon the Qt notion briefly.  At the same time I was helping our packagers get a new release of something out and struggling with all the variations of the different versions and distros (here is a tip, never help package, it is much easier to tell other people to do it).  Also that same week I had talked to a major software distributor and they had been pretty clear that they weren't going to put Linux only software on the shelves, but they had been very interested in our BlackAdder product because it was cross platform.  All of these things gelled in my mind at the same time and I sent a note out to my team to see if building directly to Qt would solve these packaging problems.  At the same time I would get multi-platform capability and be able to get wider spread distribution.  Basically we had a win win win situation.  It solved build problems for us, it solved install problems for our users and it solved getting major distribution.\n\nNow I really do love KDE, it's a wonderful desktop with wonderful apps and I have some other OSS projects in mind I would like to do and contribute once things settle down.  Like any human being I get frustrated, and since I act as the major conduit for almost all communication with theKompany (and now a lot for Hancom) the bitching and moaning starts to feel like rats biting at your heals as you walk.  I do get some very good complimentary emails, but human nature is to say something to bitch, not to compliment so the bitching usually outweighs it.\n\nOur ultimate objective is still Linux/KDE on the desktop.  I got a call yesterday from a major company that wants a site license for HancomOffice because they are mixed Linux and Windows and want to totally migrate to Linux and getting HO would work perfectly for them.  This is exactly the kind of thing we are trying to do.  So many people don't have the big picture, they are mired in this silly minutia.\n\nKorelib is what it is, it is licensed the way it is licensed, it is there for you to use on the terms that are available.  All I did was point out it was there and could satisfy your need for this kind of technology.  If you have a *real* situation that you want to address about using Korelib, and not a make believe \"what if\" scenario, then write me privately and we'll see what we can do.\n\nI do believe that is all the time I have to spend on this particular topic."
    author: "Shawn Gordon"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Shawn,\n    The way I see it is, you produce a piece of software, make it available under some license and if people want it they use it based on the terms and conditions of the license and if they dont like it then FSCK em.  I don't think you are under any obligation to please *anyone*.  From what I can tell you seem to be an honerable man attempting to do the right thing for the community and at the same time attempt to run a company and produce a profit.  Last I checked, The Kompany wasn't a charity. \n\nI firmly believe the OSS community as a whole are well intentioned folks. But every now and then you run across numbskulls like Wiggle who strive to spread lies and cause trouble for their own selfish reasons.  Better to just ignore them and hope they die in their pool of stupidity >than let them get your dander up.\n\nI guess I'm just saying I understand your situation and how frustrating it must be to deal with individuals who are SO emotional. Me thinks some people should here should get a girl friend or something. Yeah thats the ticket. Too much social masterbation is a bad thing. \n\nMy 2 cents worth.  Keep up the good work."
    author: "Ron Cooper"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Shawn,\n\nI see it this way.  To paraphrase Luke 10:7, a workman is worthy of his hire.  So, basically, I say this: If you write something, you can get to choose how it should be disseminated and which license(s) it should be under.  GPL, LGPL, BSD, ABC, XZY, LMNOP, etc.\n\nJust so you know, there are people that appreciate all of the software that TheKompany has given to the community, be it under any license.\n\nI agree with you.  If someone wants to make money using code that you wrote, you should have the right to demand payment."
    author: "A. C."
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Shawn,\n\nI see it this way.  To paraphrase Luke 10:7, a workman is worthy of his hire.  So, basically, I say this: If you write something, you can get to choose how it should be disseminated and which license(s) it should be under.  GPL, LGPL, BSD, ABC, XZY, LMNOP, etc.\n\nJust so you know, there are people that appreciate all of the software that TheKompany has given to the community, be it under any license.\n\nI agree with you.  If someone wants to make money using code that you wrote, you should have the right to demand payment."
    author: "A. C."
  - subject: "Re: Hancom Office"
    date: 2001-10-03
    body: "Hi Shawn!\n\nDon't let these zealots get to you.  I think your company is making great contributions to KDE, personally.  Vivio needs a lot of work for it to be a serious contender to Visio, imho, but I don't have a problem with the rest of your products.\n\nWhat I am wondering about however, is when a Chinese version of Hancom Office will be available for Linux.  i.e. a version of Hancom Office that supports GB2312-80, possibly Big5, and pinyin input.\n\nThanks in advance.\n\nDaniel"
    author: "dbl"
  - subject: "Re: Hancom Office"
    date: 2001-10-03
    body: "You can get the Chinese version of HancomOffice today.  They have very good support for it.  If you can read Chinese then check out the Chinese links on the site."
    author: "Shawn Gordn"
  - subject: "Re: Hancom Office"
    date: 2001-10-03
    body: "Ah...ok\n\nI can't read Chinese, but my partners asked me to try and find Chinese office support for Linux (they're both Chinese).  Unfortunately, I can only speak Chinese at the moment.\n\nI'd read in a Linux Journal a while back that Hancom Office was available in Chinese for Linux, but I didn't see any references to it on the English site; only references to the Chinese version for Windows.\n\nWould you happen to have any links I could follow for the Chinese version for Linux, not Windows?\n\nThanks in advance."
    author: "dbl"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Shawn - don't waste your time on trolls - ignore them!"
    author: "Joergen Ramskov"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "You know, this really is borderline evil... you don't have an argument at all, just some stupid innuendo.\n\nMaybe I'm just *really* stupid, but how can it be justified for people to complain about a product not being released under a specific license? If you *need* LGPL instead of GPL, then you really are asking for free beer, not free speech."
    author: "jd"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: ">If you *need* LGPL instead of GPL, then you really are asking for free beer, not free speech.\n\nWhat rubbish. If you need LGPL, you are asking for both."
    author: "Wiggle"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Bullsh*t! Since when is the GPL not free speech? I would agree that libraries are best licensed under the LGPL, but the library author has no way to make money by releasing it under the LGPL. The argument that you can't make commercial software using korelib without buying a license is nonesense because you will already have to pay trolltech for QT anyway. The Best thing that the Kompany can do is release it under the same license as QT so that programmers know where they stand and don't need to interpret to distict licenses."
    author: "richie123"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: ">Bullsh*t! Since when is the GPL not free speech?\n\nWhen did I say it wasn't? Read first, understand, then post."
    author: "Wiggle"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "If you are being offered free speech, and you say, \"Nay! I want free speech and free beer, and have no less than that\", allow the spectators to guess that what you really want is a brewsky."
    author: "Roberto Alsina"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "You know, I should know better than to respond... with commments like \"TheKompany is a total Free software quisling... as is KDE as a whole\" I really should just conclude that you are trolling. At the very least you are very bad at backing up your arguments.\n\nThe GPL is the most agressively Open Source license in wide use out there, as far as I know. The LGPL certainly makes sense for core libraries in a desktop system wanting to attract commercial development, but where do you get off claiming that a commerical vendor (theKompany) has an obligation to not only open the source to Open Source projects (GPL) but also to other companies commercial projects(LGPL)??\n\nYou only *need* the LGPL if you are doing a closed source (in most cases probably commercial) application, using Korelib (in this case). But if you are selling your product, how can you expect theKompany to give you their product for free? And to top it off, to claim that their unwillingness to do so somehow is detrimental to the Open Source movement?"
    author: "jd"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: ">You only *need* the LGPL if you are doing a closed source (in most cases probably commercial) application, \n\nAgain, total rubbish. Many companies will not even allow in-house development if it has to be linked against a full GPL library. You also apparently seem to miss the fact that this is yet another (\"kore\" - I'm sure Shawn wishes) library  that has to be licensed for closed-source stuff.\n\nAt this rate, developing non-GPL apps for KDE will be more expensive per-developer than Windows-XP."
    author: "Wiggle"
  - subject: "Re: Not pushing"
    date: 2001-10-05
    body: "If the cost of application development was simply the cost of the tools you use to develop said applciations, you would be right.\n\nAnd even if it were, what is the problem with that, exactly?"
    author: "Roberto Alsina"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Give the guy a break - theKompany is working on some good software, and trying to make open-source a reasonable proposition for both producers and users.  Let them decide how they want to license the stuff - if you don't like the license it comes with, you don't have to use it.  Go and write some of your own."
    author: "Kevin Donnelly"
  - subject: "What about free as in free beer software?"
    date: 2001-10-06
    body: "Some companies can't make their software open source. So, prolly offer a strip down version of Korelibs for these ppl under LGPL? then ppl won't be force to write their own libs nor force to use gtk or make their software paid for."
    author: "RAJAN"
  - subject: "What about free as in free beer software?"
    date: 2001-10-06
    body: "Some companies can't make their software open source. So, prolly offer a strip down version of Korelibs for these ppl under LGPL? then ppl won't be force to write their own libs nor force to use gtk or make their software paid for."
    author: "RAJAN"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "> GNOME does this.\n\nNot all Gnome libs are LGPL I think, some are GPL. What's more rms had always made it clear that libs which have no equivalent in the closed-source world should be GPL (not that I agree with that, but you have to like the irony).\n\n> if a Project wanted to do this they would not want to buy a licence\n\nWhy not ? You know, contrary to a widespread belief, most software projects actually survive the purchase of a license. And what's more, theKompany would actually get some money out of this. Pretty neat concept uh ? :-)"
    author: "Guillaume Laurent"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "All the libraries in the core GNOME platform are LGPLed.\n\nGal, for example, is GPL, but it\u00b4s not a part of the GNOME platform."
    author: "Carg"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "> And what's more, theKompany would actually get some money out of this. Pretty neat concept uh ? :-)\n\nAnd they could even use this money to develop more products that they would give away to the community, or hire some more free software programmer to help them develop new products.\n\nIf they release it under LGPL, a company can use their product without paying any fee. Which could lead them to bankrupt.\n\nI think it is in our interest that TheKompany earns money. And we should be happy that they released something under GPL, they could perfectely have kept it closed-source.\n\nSo your critics are not welcome by me. Oh, by the way, you need to critisize Trolltech for Qt because they do exactely the same thing: GPL/QPL."
    author: "Philippe Fremy"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "> So your critics are not welcome by me.\n\nHey Philippe,\n\nI assume you're talking to the guy I was answering to :-)."
    author: "Guillaume Laurent"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: ">If they release it under LGPL, a company can use their product\n>without paying any fee. Which could lead them to bankrupt.\n\nOne wonders why you bother with open source at all...\n\n>Oh, by the way, you need to critisize Trolltech for Qt because\n>they do exactely the same thing: GPL/QPL.\n\nAnd don't we know it..."
    author: "Wiggle"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "So that's fine.  A project can release their code under GPL and LGPL / BSD / MIT.  If someone wants to later use it not in accordance with GPL, then they must license Korelib.  This is between them and the owners of Korelib, and has nothing to do with the project giving away their code.  What's wrong with that?  If you don't like it, write your own code.  Programmers like to eat and pay the rent just like everyone else."
    author: "David Phillips"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "If someone wants to write LGPL code that uses Korelib, it is perfectly legal to do so.\n\nThat is the exact same situation as with kdelibs.\n\nShawn & co. are just making a gift.\n\nWe either take it and use it because we like it, or not take it and not use it because we don't like it.\n\nComplaining about thegift is uneffectual, silly, and a very good way to stop getting gifts in the future. Not to mention rude (not about this post I am replying to, but about the general tone of nastiness in this thread)."
    author: "Roberto Alsina"
  - subject: "Re: Not pushing"
    date: 2001-10-02
    body: "The software you put out under the GPL under the excuse of returning to the community that provided you with tons of libraries under the LGPL/BSD is now going commercial.\n\nSo, what\u00b4s your new way of returning to the KDE community? With Qt-based commercial apps?\n\nThanks,\nCarg"
    author: "Carg"
  - subject: "Re: Not pushing"
    date: 2001-10-02
    body: "What kind of fantasy land, knee jerk comment is that?\n\nHow much is enough for someone like you?  How should I run my business such that you would be satisfied?  Would that business model make money so that it can pay developers to do these altruistic things?  Everyone beats us up to use the GPL, so then we use the GPL and people complain.\n\nWhat does it take, how much is enough."
    author: "Shawn Gordon"
  - subject: "Re: Not pushing; How much is enough [...]?"
    date: 2001-10-03
    body: "Shawn,\n\nI think most free software people are in some ways more comfortable with software that is CLEARLY CLOSED, as compared with software that is \"free with a catch.\"  \n\nThings like korelib are an invitation to become dependent on commercial libraries that might later vanish, or become prohibitively expensive due to arbitrary decisions not merely by you now, but perhaps one day by the corporate owners of your company offshore, in Korea perhaps.  \n\nAs well, I assume your revenue generating plans will require you to reject contributions that do not assign copyright to you.  These sort of terms generally don't seem as appropriate for a for-profit company as with a non-profit like with FSF.  Or do you plan to share revenues with contributors?  That would be novel.  But in that case, contributors should have a say in the licensing terms, fees, and restrictions associated with the package.\n\nFrequently there are good reasons to use commercial libraries, but in general, I strongly favor a clean separation between commercial and open source code.  I am doubtful about GPL'd libraries for that reason, except with a GCC or GNAT style exception which permits linkage with client code without restriction. \n\n\nMatt"
    author: "Matt Benjamin"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Shawn--not all of us are ungrateful.\n\nPeople can gripe all they want to about liscensing.  I for one, and I belive that there are many others, thank you for coming up with a business model that helps KDE, helps Open Source and helps a handful of people get paid.\n\nI don't see how people can on one hand scream, \"Software wants to be Free (libre)!\" and then turn and say \"But please let us make free (gratis) for closed source companies\".  \n\nAnd how about \"Beware of Greeks beaing gifts.\" -- To that I say, beware all you want to.  Reject it all together.  And shutup.\n\nBest wishes to Shawn and to all of the fine folks at the Kompany.\n\n-Scott"
    author: "Scott"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: ">>I don't see how people can on one hand scream, \"Software wants to be Free (libre)!\" and then turn and say \"But please let us make free (gratis) for closed source companies\".\n\nHits the nail on the head. GPL/Commercial licensing is fair for everyone: if you want to give your software away for free, you can use mine for free; if you want to sell software that includes mine, send some of the money my way. If you want a fair fight between free and commercial software, don't let the commercial companies act as parasites."
    author: "an anonymous coward"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Shawn,\n\ndon't care to much about these comments, there are always people complaining.\nIf you want to earn money if somebody wants to use your stuff commercially, ok. That's the way Trolltech does and nobody complains (maybe except RMS).\n\nLooking forward more cool stuff from TheKompany :-)\nAlex"
    author: "aleXXX"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Don't respond to these trolls, Shawn (especially that Wiggle guy), you're beating _yourself_ up!  Just do what you need to do, and know that there are some rational people out here who like theKompany and what you guys are doing.\n\nI'm looking forward to seeing the results of your joint venture with Hancom, it looks interesting.\n\nBest of luck to you and everyone at theKompany!"
    author: "not me"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "someone want a creator of GPL apps, to change it to LGPL , so he can use in his commercial apps *to get some money* without paying.\nis this the scenario above ? \nwhat a niiiiice world :)"
    author: "jamal"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "I have a story.\n\nOnce upon a time, a man and his son went for a travel. They brought along with them their only lovely donkey. At first, the son riding the donkey while his father walking.\n\nThe went thru a village. The people there said \" How rude are you son, you suppose give priority to your farther to ride the donkey. You are stronger than him\". Upon hearing that, the son went down and his father rode on the donkey.\n\nThe went thru another village. The peoples there yelled \"What an irrensponsible father, you suppose give the donkey to your son. You'd sacrifice for your son\". Passing the village, the father went down and the son said \"Now let we both walk since if anyone of us ride it, people think we are wrong\".\n\nThen after a while they went thru another village. The people there scream at them \"How come you don't ride the donkey. It's for you to ride.\". Upon hearing that, both the father and the son rode the donkey.\n\nThey went thru another village, and the people there yelled \"Stupid! You beind so rude to the donkey!\".\n\nThe son said \"Father, what is right and what is not? Everything is wrong in peoples' eyes\".\n\nThe father replied \"Yes, what is right is what we believe. Whenever we believe in what other believe, we are wrong. When we are trying to satisfied all, we also wrong. Nothing right in us for every person\".\n\nTo conclude, I believe that Shawn should do what he himself thought as right. Forget of those who just talk but never contribute something to the community. All they want is \"free beer\". They are parasites in our community.\n\n- deman"
    author: "deman"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "ditto :-)"
    author: "Filippo Erik Negroni"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "I will try to get the maintainer of the fortune mod to axept this little story... good words at the right place!\n\n/kidcat\n--\nhardtoreadbecauseitissolong"
    author: "kidcat"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Shawn,\n\nnow that hopefully the license discussion is behind us... \nWhere is the 0.8 version of korelib? As it was said before the version out on\nWeb is 0.0.1 and very preliminary. I'd be really interested to see it. I am\na Python fan so your comments on the script capability make me very curious.\nThanks!"
    author: "Wolfgang"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "I'm not sure if I said it here or somewhere else, but basically Korelib is the foundation upon which all of the embedding technology for HancomOffice is based.  We are working on an updated release that has the required bits for this, it is a week or two off and then we will update the web pages and the other stuff.  We are in serious crunch mode right now to hit this November release date with the whole suite.  Sign up to our announce list on our web site under 'press' to get updates as they occur."
    author: "Shawn Gordon"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Shawn - \n\n Hope you get to read this, after all it is at the bottom of a LONG thread.\n\n Try not to get frustrated with people like Wiggle.  He, like many in the extreme \"free\" software crowd, are so narrow minded that nothing will ever please them.  There is a certain bias from many of them towards QT and KDE that no amount of logical reasoning and hoop jumping will ever change.  Do what is right for your Kompany first :)  Trust me, your business model right now has many of rest us very impressed.  I truly hope it is successful.\n\n\n -John (looking forward to Aethera and Kapital 1.0)"
    author: "John Sinnott"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: ">Try not to get frustrated with people like Wiggle. He, \n>like many in the extreme \"free\" software crowd, are so narrow\n>minded that nothing will ever please them. \n\nOh yes, II'm a free software extremist who is in favour of a \nmix of Free software and closed-source development - just not scam \nartists like TheKompany."
    author: "Wiggle"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "Hey Wiggle, why don't you stop using GPL software altogether? Don't use Linux, don't run a standard FreeBSD (which includes gcc among others), etc. Just use your BSD software and Windows.\n\nIf people like to release Free software under the GPL, let them do it! It is their choise. People like you who try to restrict Free software shouldn't be trolling in discussions about Free software!\n\nRun Windows if the commercial Microsoft license pleases you!"
    author: "Niftie"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: ">Hey Wiggle, why don't you stop using GPL software altogether? \n\nWhy? Do you understand anything about this?\n\nA license has to reflect the use the software is put to. Why do you think the Vorbis guys changed from LGPL to BSD for the encoder/decoder. The GPL is great, in its place... but here it is not being used for freedom, it's being used to force fees out of people."
    author: "Wiggle"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "> The GPL is great, in its place... but here it is not being used for freedom, \n> it's being used to force fees out of people.\n\nThe main idea behind the GPL is to force people; it is usually used to force people into releasing their code as Free software. In this case, at least there is a choise. Either release the source code under Free software conditions, or buy a license under commercial conditions. Freedom *and* choise!\n\nIn any case, since the code is released under the GPL it will be Free forever, even though some people may have bought the code and used it in closed circumstances."
    author: "Niftie"
  - subject: "Re: Not pushing"
    date: 2001-10-03
    body: "wtf?\n\nHow is TheKompany a scam artist?  I think they're one of the best things to happen to KDE.  There's been a few KDE applications put out by them, or sponsored by them.\n\nGranted, Vivio isn't all that useful, imho (I like Dia, personally).  But, now that I've got KDE 2.2.1 installed, I'll try kUML.\n\n*shurg*\n\nEveryone's entitled to their own opinions, I suppose..."
    author: "dbl"
  - subject: "Do it like Qt?"
    date: 2001-10-03
    body: "If you want people to take Korelib seriously as a component architecture, can I suggest that you modify the licensing terms to EXACTLY the same as Qt, and market it as an extension to Qt?\n\nThat is, triple-license it, allowing Free Software authors on Free *nix to choose between GPL and QPL, whilst proprietary software authors on any platform pay for a commercial license?  Perhaps (although this is not necessary) also releasing a non-commercial version for authors of non-commercial software on Windows?\n\nTrolltech have spent a lot of time and effort thinking about the ramifications of their particular licensing schemes, and it seems to work well.  It allows Trolltech to contribute to KDE, support all forms of Free Software (gaining a great deal of PR and goodwill in the process), and at the same time make money from commercial licensing.\n\nAny criticisms of the Korelib licensing scheme then are also criticisms of the Qt licensing scheme, and this allows TheKompany to shield itself behind the enormous amounts of goodwill generated within the Free *nix community by Qt. \n\nProgrammers who are using Qt as a platform will already have considered the licensing issues involved with their particular type of software.  If the licensing terms for Korelib are identical in every way to Qt's, then using Korelib becomes a total no-brainer.\n\nJust a thought."
    author: "Amazed of London"
  - subject: "No News is Good News!?"
    date: 2001-10-02
    body: "Why is KDE News not showing news, reviews, etc. lately perhaps 5 days?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: No News is Good News!?"
    date: 2001-10-02
    body: "Because you're not submitting any?  We can certainly use all the help we can get...  Personally, I'm away on the sunny island of Mauritius, I'm not at the computer a fraction of the time, and when I am, my internet connection is often uselessly slow.  But we all have our excuses.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: No News is Good News!?"
    date: 2001-10-02
    body: "Simple, the developers are all too busy playing with KDE 3 while they still can! Message, source, and even binary incompat is a precious commodity, and they aren't wasting a single minute of it! ;-D"
    author: "Carbon"
  - subject: "What's in a name"
    date: 2001-10-02
    body: "OK, the first change that should be made is to the name.  QCom will never win the hearts of programmers who hate COM as long as it is still named QCom.  It just emits bad vibes or something."
    author: "not me"
  - subject: "Re: What's in a name"
    date: 2001-10-02
    body: "Hear! Hear!  Now Miguel, there's a bonobo who can think up some good names."
    author: "KDE User"
  - subject: "Re: What's in a name"
    date: 2001-10-03
    body: "QAintCom ?"
    author: "Carbon"
  - subject: "Move on Shawn....."
    date: 2001-10-03
    body: "Shawn,\n\nKeep doing what you're doing. You got programmers you gotta pay and you gotta make a living. I'm perfectly happy with what theKompany has given to the community - thanks. \n\nWiggle - You're an idiot, if you don't like it, USE SOMETHING ELSE. If you don't like the license, USE SOMETHING ELSE. But you better hurry on your LGPL Korelib clone if you're going to make it in time for KDE3."
    author: "jorge castro"
  - subject: "xft support in qt-3.0beta6?"
    date: 2001-10-03
    body: "When I configure qt-3.0beta6 like this:\n\n% ./configure -xft -qt-gif -qt-jpg\n\nI still get -DQT_NO_XFTFREETYPE in the Makefiles.  Am I doing something wrong, or is xft disabled in the beta?  (I have TrueType fonts working with Qt-2.3, and I have ensured that I have libxft and X11/Xft/XftFreetype.h).\n\nTIA,\nJason"
    author: "LMCBoy"
  - subject: "Who Wiggle really is"
    date: 2001-10-03
    body: "It should be obvious by now that Wiggle is yet another identity that Miquel uses on boards like this and linuxtoday to troll.  The style is exactly the same as a popular \"ac\" that use to post these exact kinds of things.  He got found out and shut down there.  I would assume that he would either be to weak or busy with MONO to keep messing around with KDE.  It's really sad that he was one of the original KDE developers but has turned into such a spaz."
    author: "A.C."
  - subject: "Re: Who Wiggle really is"
    date: 2001-10-03
    body: "Are you seriously saying that the person behind Wiggle the Troll is the head developer of the GNOME project? This is a pretty strong claim. Can you back it up with some references?\n\nNiftie Noufty Noouh!"
    author: "Niftie"
  - subject: "Re: Who Wiggle really is"
    date: 2001-10-03
    body: "Well, Miguel spent several days on #kde saying stuff like \"Why are you helping this evil project?\", so I put nothing beneath Miguel.\n\nOk, an alligator without a hat *could* be lower than Miguel, but I am not betting."
    author: "Roberto Alsina"
  - subject: "Re: Who Wiggle really is"
    date: 2001-10-03
    body: "How do you know this was Miguel on #kde ?\nMaybe it was just his dog ?\n\nI dont think that thinking every troll is a desparate Gnomer is the right attitude.\n\nPlease be more nice to each other. Open source has enough problems ( i.e. venture capital running out ), we dont have to spread hate among competing groups.\n\nWe should ignore the ones who cant cope with our moral standards. This is the\neasiest thing to do, it\n1) saves time. \n2) To the naive, it doesnt look as if someone has pay real attention to his\narguments. Just look at the thread he has caused ~100 messages. He must have \nhad something to say, didnt he? \n\n\nArguing is a god thing to do, it keeps you from starting to get squared, but not every argument is sane .... ( It seems I cant reach a nice conclusion, so I go to sleep )"
    author: "ac"
  - subject: "Re: Who Wiggle really is"
    date: 2001-10-04
    body: "> How do you know this was Miguel on #kde ?\n\nThe linux world was much smaller then. I had already been on IRC with him.\nAnd he replied to his own email address, so unless he was a very clever fake, he was himself."
    author: "Roberto Alsina"
  - subject: "Re: Who Wiggle really is"
    date: 2001-10-03
    body: "I think you have watched too much X-Files or other serials of this kind, and I think Miguel is clever enough to spend his time doing some more interesting stuff ! Anyway, while KDE popularity is increasing, the number of stupid comments such as those of Wiggle will grow up... (maybe we should add an option \"ignore stupid trolls\", and, for those who like trolls, an option \"ignore constructive comments\" :-)\n\nAs for technical aspect, I would really appreciate if someone could give me some information on technical aspects of Korelib, especially in comparison with COM, KParts and Bonobo."
    author: "Beubeu"
  - subject: "Re: Who Wiggle really is"
    date: 2001-10-04
    body: "There is a lot of confusion that Korelib is a Kparts replacement.  This is not true, it doesn't do the UI plug in part, they are complimentary pieces, and are both used in Aethera for example.  I've asked my programmers to write up a short synopsis to explain the difference and the uses.  Maybe the dot will run it as a story if we are nice to them, I saw Navindra asking for content recently :), and I think that given the amount of discussion here, it coule be a worthwhile piece."
    author: "Shawn Gordn"
  - subject: "Re: Who Wiggle really is"
    date: 2001-10-04
    body: "I have to say that is has been most fun ever to read discussion above. This wigle guy seems to have serious \"my way only way\" problem, or was it gnu way\nonly way, don't know don't care.\n\nIn my short life i have noticed at those people who have this obsession of being right, no matter what, are usually those who are often most wrong.\n \nAnd if he really is some head guru of \"other\" project,he has even less reasons to keep noise, if he is right, the opposite will win eventually, no matter who says and does what and when.\n\nAnyway thanks for the kde people for wonderful desktop. Even tho this is posted from win2000 i still have linux lying around here somewhere. =)"
    author: "RR"
  - subject: "Re: Who Wiggle really is"
    date: 2001-10-04
    body: ">Anyway thanks for the kde people for wonderful desktop. \n>Even tho this is posted from win2000 i still have linux\n>lying around here somewhere. =)\n\nAhhh.... nothing sums up KDE supporters like that statement.\n\nIgnorance, rank hypocrisy and total non-involvement - all in one rotten package."
    author: "Wiggle"
  - subject: "Re: Who Wiggle really is"
    date: 2001-10-05
    body: "Oh well i am so sorry my comment didnt please you. I will try to do beter next time. :p \n\nBesides how do you know where i am involved in? oh yeah i am running win2000 in work i must be totally clueless, woe me  \n\nend of thread hopefully, naah have your last word i know you want to say it anyway."
    author: "RR"
  - subject: "Trolling, trolling, trolling"
    date: 2001-10-04
    body: "It should be plain by now that whoever this Wiggle character really is, they are only here to troll.\n\nRemember that trolling is only done to provoke a reaction.  Ignoring a troll cuts off their air supply, and eventually they die.\n\nSo, as often seen on bridges:  Please don't feed the trolls.  (Well, maybe you can send something to the ones who make Qt, but they're fluffy trolls.)\n\nIt matters not one bit really what Wiggle thinks or posts.  KDE is the de-facto standard desktop for Linux.  With all but one of the major distributors shipping it as the default, I can't see that changing, either.  If Wiggle doesn't like that, then I suggest that they get involved with Enlightenment, because e17 looks like the most interesting and viable (not to mention sexy) alternative.\n\nBTW, some people might find this article on The Register interesting, for an industry viewpoint that's outside the Linux desktop turf wars:\n\nhttp://www.theregister.co.uk/content/4/22025.html"
    author: "Amazed of London"
  - subject: "KDE, instead of Qt!"
    date: 2001-10-05
    body: "Instead of independently realizing the widget-library (under LGPL) and independently to determine direction of development of the project (KParts, instead of QCom), developers KDE are on the bit TrollTech :-(\n\nBecause of it constant copying a code is necessary them... \n\nIt is favourable TrollTech, but not to developers KDE."
    author: "Andi"
  - subject: "Re: KDE, instead of Qt!"
    date: 2001-10-06
    body: "wtf?"
    author: "Me"
  - subject: "Proposal for a Killer Component Architecture"
    date: 2001-10-07
    body: "Hi.  I am a former Java CORBA developer.  I have also played around with GNOME Bonobo, Windows COM components, KParts, OpenOffice's Universal Network Object's (UNO) infrastructure, and Mozilla's XPCOM architecture; you can say I'm really into component technology.  This is just one developer's opinion of what KDE and QT should do when it comes to a strong component architecture.  \n\nFirst, since my primary background is CORBA, I'll talk about that first.  I worked with CORBA for about 2 years, on and off work projects, and I can say that it is not the way to go.  It has nothing to do with performance concerns, which the ORBit project briliantly solved.  The chief issue is that Raw CORBA, sans Bonobo, is really quite hard to develop with both mentally and while programming.  First it takes a developer an eon to come up to speed with the million different acronyms and ideas in CORBA (BOA - Basic Object Adapter, POA - Portable Object Adapter, plus twenty thousand other acryonyms); this assumes that the developer is already quite sohisticated with both object oriented programming plus advanced object patterns.  CORBA code, especially when dealing with C code accessing a CORBA ORB, is almost completely unreadable in my opinion.  It really destroys any hope of self-documenting code.  Further, keeping the generated CORBA skeletons and stubs in sync across a huge project is very difficult and time consuming, even with advanced makefiles; basicly CORBA interfaces and generated code can be quite brittle.  For some languages, such as Java, many files are created for a single CORBA class; various Helpers and Holders, for example.  I used to get a headache just dealing with the many different ways of accessing, storing, and dealing with my CORBA classes, for example.  The only thing worth saving in CORBA, in my opinion, is IIOP (Internet Inter-Orb Protocol), the standard that makes it possible for CORBA ORBs to communicate with each other over the Internet.\n\nThe Bonobo project attempts to answer these problems, but it is my opinion that it is not successful.  It adds a layer above the CORBA objects such that you don't have to deal with IDL (Interface Definition Language) or any CORBA concepts; instead you deal with property bags and pass these around.  While this does make it much easier for the individual programmer, the chief problem is that turning everything into property bags destroys interface based programming and is akin to variables hanging off an HTTP statement.  As soon as you want to go 'clean' and design nice IDL interfaces you have to delve into CORBA again.  So my chief criticism of Bonobo is that by making CORBA easy with property bags it throws the baby out with the bath-water.  \n\nWindows COM was nice, just several years ago.  It makes it possible to do some really powerful stuff, but the completely unreadable GUIDs combined with all of the complex OLE and Automation interfaces when you want to do anything GUI related are unspeakably complex.  Windows COM _does_ get it right by using something known as typelibs.  Typelibs are a binary file that holds descriptions of the components you are dealing with, such as the method names and so on.  This means that you don't need to generate stubs and skeletons, so code management becomes much easier and can be more 'automagic' based and linking can be done more at runtime.  My chief criticism of Windows COM is that it gets really complex when you want to do higher-level GUI-type work, has developed several years of 'interface cruft', and is of course proprietary without payola porting toolkits.\n\nMozilla XPCOM is nice; its cross-platform, and can be binded to by several different languages.  It's sort of a nicer Windows COM; it doesn't go further than that.  It also doesn't address cross-machine communication.  One problem is that it is not that 'battle-hardened' and optimized, since it is newer than other component techs.\n\nThe two nicest component technologies I have dealt with are OpenOffice's UNO components and KDE's KParts technology.  \n\nI'll start with OpenOffice's UNO components.  The UNO technology is several years old and has been used to support several releases of a huge project, and is therefore more solid in my opinion.  It uses typelibs rather than generated skeletons and stubs, so it is much easier to work with.  When writing code that accesses an UNO component, it is very readable and understandable, which I believe is important.  It has very strong 'programing in the large' support, which are easy to use, such as supporting interfaces, services, and aggregration support.  OpenOffice already has an extensive set of API's that can be called as UNO components, and these are readable and easy to use, getting rid of the complexity that Windows COM introduces when you want to do higher-level GUI things.  It is also cross-platform, and seamlessly supports OLE and Windows COM transparently to the programmer.  Writing an UNO component is also very easy.\n\nKParts is one of the easiest component technologies I have ever seen, and it gives you alot of bang for the buck.  The use of XML to do user-interface merging is brilliant, in my opinion, getting rid of one of the biggest sources of GUI component complexity.  Creating a KPart component is very easy, and by using the DCOP IDL compiler you can easily do interprocess and cross-machine calls.  One downside to KParts in my opinion is that it is not cross-platform.  Further, while I don't know much about it, binding different languages to KParts doesn't seem that easy.  Also, the inter-machine communication protocol doesn't seem accessible outside Unix.\n\nI believe the perfect component technology would merge KParts and OpenOffice's UNO components.  Merging more of KPart's ease of use with OpenOffice's strong UNO infrastructure would be killer.  UNO makes cross-language binding easy, as well as bridging over different component technologies easy; it has the name Universal Network Object's for a reason.  It already supports CORBA IIOP for communication with Bonobo and Java ORBs, as well as OLE functionality cross-platform.  I think this would be a killer combination.  Throw in support for XML-RPC and SOAP using UNO's bridging technology, and you would have an easy way to do cross-platform (since this works with Solaris and Windows), cross-desktop (since this would interact with Bonobo through the Bonobo bindings occuring at OpenOffice), and easy to script both locally and over the network (using XML-RPC and SOAP), and you would have a killer answer to .NET in my opinion.\n\nI'd love to hear people's opinions on this (both heated and non-heated :).\n\nThanks,\n  Brad Neuberg\n  codinginparadise.org"
    author: "Brad Neuberg"
  - subject: "Re: Proposal for a Killer Component Architecture"
    date: 2001-10-16
    body: "Maybe you would like to expand on this some and submit it as an article?"
    author: "Chris Bordeman"
  - subject: "Re: Proposal for a Killer Component Architecture"
    date: 2001-10-17
    body: "This sounds great! I'm not much of an expert on component technologies myself (read : no experience :-) but, anything which allows more integration between KParts and Bonobo, as well as allowing more diverse networking support, sounds great. \n\nQuestion : would this mean that, say, OpenOffice would be easily able to load KOffice documents, just by inserting said document as an object? Or vice versa? It would seem so, since they would both be using the same component model. Then again, as I said, I know very little about this particular subject."
    author: "Carbon"
  - subject: "Re: Proposal for a Killer Component Architecture"
    date: 2001-10-17
    body: "If OpenOffice and KOffice were rebuilt to use the same component architecture that I proposed, then they could indeed by embedded in each other."
    author: "Brad Neuberg"
  - subject: "Re: Proposal for a Killer Component Architecture"
    date: 2001-10-17
    body: "Is what you propose at all related to Apple's (dried-up) OpenDoc technology?"
    author: "jb"
  - subject: "Re: Proposal for a Killer Component Architecture"
    date: 2001-10-18
    body: "Bonobo isn't all about PropertyBags. PropertyBags are simply a way for components to share key/value pairs, and get notifications when they change.\n\nBBonobo has a lot more than simply propertybags. I've programmed major bonobo applications and have only used a propertybag once. Most of Bonobo is nothing to do with Propertybags"
    author: "Iain"
  - subject: "Re: Proposal for a Killer Component Architecture"
    date: 2001-10-18
    body: "Lain, thanks for responding.  Could you tell me more about what Bonobo offers other than PropertyBags?  How else does it hide the CORBA underpinnings?\n\nThanks,\n  Brad Neuberg"
    author: "Brad Neuberg"
  - subject: "Re: Proposal for a Killer Component Architecture"
    date: 2001-10-18
    body: "Bonobo also offers:\nPersistant Streams\nEmbedding\nAgregation of interfaces into one object\nControls (not quite the same as embedding)Menu/Toolbar merging, using XML like KParts\nEventSources and Listeners\nPrinting and zooming of embeddables\nIt hides the CORBA underpinnings by linking CORBA_Objects to GtkObjects (GObjects in Bonobo2). This means that in basic usage of Bonobo you never have to deal with CORBA itself, you just call methods on the GtkObjects that you create and Bonobo does the work for you. If you want to create your own interfaces, then yes, you'll need to get dirty with some CORBA stuff, but again Bonobo has macros and helper functions so that all you really have to think is how to do what your interfaces do (rather than how to set up your interfaces for CORBA)\n\nAs an example, I've written a Bonobo based CDDB querier. Of the 600 lines of C that it took to write, less than 100 of them are to do with Bonobo, and of that, most of them are boilerplate."
    author: "Iain"
  - subject: "Re: Proposal for a Killer Component Architecture"
    date: 2001-10-20
    body: "Sounds like an excellant idea. Although I don't think this would be able to find its way into KDE 3.0 unless a great deal of interest amoung developers is fostered. Even then, integration of UNO would probably push back the release of 3.0 by at least a month or two. However, I think it would be worth the wait. (This all assumes that UNO will not undergo any major changes before the 1.0 release of OpenOffice)."
    author: "Daniel Mayer"
---
Normally a new beta wouldn't be dot news, but the new Qt 3.0.0 beta6 release is notable for one reason: QCom is gone.  In a mail sent to <a href="http://dot.kde.org/1002007199/qt-interest.txt">qt-interest</a>, Trolltech explains, <i>"The feedback we received on this module during the 3.0 beta phase has been mixed. Many users think this module lacks the intuitiveness and compactness that they have learned to expect from a Qt API. Therefore, we have made the difficult decision
to withdraw the QCom API from the Qt 3.0 release. We will continue to develop this API until it is evolved enough for our customers, and will include the improved version in a later release."</i>.  They also note that the new Qt3 plugin functionality is still available under a much simplified API, see the <a href="http://www.trolltech.com/developer/changes/3.0beta6.html">changelog</a> for full details.  IMHO, this is all quite reasonable and is probably for the best -- even on the dot, people turned out to be <a href="http://dot.kde.org/997082845/">quite opinionated</a> about QCom, although most of the heat seemed to be directed towards COM itself.  As a final aside, theKompany is pushing its <a href="http://thekompany.com/projects/korelib/">Korelib</a> as an alternative option.
<!--break-->
