---
title: "KDE Is the \"Best Open Source Project\""
date:    2001-08-30
authors:
  - "Dre"
slug:    kde-best-open-source-project
comments:
  - subject: "Congratulations!"
    date: 2001-08-30
    body: "A big \"Thank you\" to the entire KDE Team including developers, translators and artists for the efforts that made this success possible."
    author: "A Happy KDE User"
  - subject: "Congratulations nr. 2"
    date: 2001-08-30
    body: "Great :)"
    author: "Another happy KDE user"
  - subject: "Excellence rewarded"
    date: 2001-08-30
    body: "Nice to see that the great work of the KDE team is being recognized by the whole community"
    author: "hoju"
  - subject: "KDE Rules!"
    date: 2001-08-30
    body: "And Gnome drools!  Or something like that.... *grin*  I seriously think 1/2 of the \"desktop flamewars\" result because people /need/ a \"home team\" to cheer on.  And if you're cheering someone on, it's only one step to jeering at someone else.  So let's all celebrate this as very nice recognition of all the hard work that volunteers have put into this, and keep it civil!  Woohoo!\n\n\nP.S.  As the (4th? 5th?) comment posted, nobody's yet said anything about Gnome, so consider this a premptive strike ;-)"
    author: "David Bishop"
  - subject: "Re: KDE Rules!"
    date: 2001-08-30
    body: "Well I'm mostly a Gnome app on xfce desktop user - but I still say congratulations. KDE has been through licensing problems, compiler hell and more and its not just survived  - its still growing\n\nAlan"
    author: "Alan"
  - subject: "Me too"
    date: 2001-08-30
    body: "I prefer Gtk/Gnome on X, but KDE is still a great project. :) Not my taste (besides of KHTML which is unbelievable outstanding), but what does it matter?\n\nKDE was the first thing I saw when running GNU/Linux for the first time... it was a great feeling... :) KDE 1.x thought... I kinda miss it... *sigh* :)\nThere is some really bad thing about Computers and Software... everything is evolving so fast, that you have plenty of things to miss. :(\n\nI miss the C64, the good old MSDOS, KDE 1, etc.\n\nBTW, please all KDE developers (and fans of course), please stop beeing so nasty about Ximian.\nThey just do their job and I can't remember that they spoke about KDE once, while there is plenty of talk about Ximian in the KDE camp and even TheKompany does it from time to time.\nI was really happy when Gnome invited KDE develoeers to Guadec, I hoped it would have changed some minds.\nBTW, KDE is absolutely not better than Gnome, whatever you might think. It's just different.\nLike BeOS is not better or weaker than X, it's just different."
    author: "Spark"
  - subject: "Re: Me too"
    date: 2001-08-31
    body: ">>>\nLike BeOS is not better or weaker than X, it's just different.\n<<<\n\nOh man, in light of recent events, that is just not a good comparison :-|"
    author: "blandry"
  - subject: "Re: Me too"
    date: 2001-08-31
    body: "BTW, please all KDE developers (and fans of course), please stop beeing so nasty about Ximian.\nThey just do their job and I can't remember that they spoke about KDE once, while there is plenty of talk about Ximian in the KDE camp and even TheKompany does it from time to time.\n\nThe reason there is (\"was\" would probably be more appropriate) so much talk about Ximian, is because of their past behavior. Miguel made a lot of nasty comments about KDE shortly after GNOME was started. In addition to that the Ximian ad campaign on Google was not the nicest thing either.\nHopefully, these things will not repeat itself again. Currently it looks like there is more cooperation than competition going on (between the desktops) and that is a good thing."
    author: "jj"
  - subject: "Re: KDE Rules!"
    date: 2005-10-11
    body: "I agree, KDE does rule!  I didn't like it much before, but last night I played with 3.4 for a few hours, and I must say I am very impressed.  They cleaned it up a LOT.  My old laptop couldn't even run it before without heavily swapping on the HD (I only have 192MB RAM), but now it only uses half of my memory, which obviously makes the computer run much faster.  Another thing I was very happy to see was the absence of many buttons on Konqueror.  It has a much cleaner interface now.  Anyway, I just wanted to say great job to the many people working on KDE!"
    author: "Dan"
  - subject: "Yet more GNOME bashing..."
    date: 2001-08-31
    body: "> And Gnome drools!\n\nReally... why do you even mention GNOME? GNOME has a different goal than KDE and if you look at the news then you'll notice that they're definitely not drooling but making lots of progress.\nSo stop this useless and off-topic crap about someone-elses-desktop is better than yet-again-someone-elses-desktop-which-has-a-different-goal.\nYou don't hear the GNOME team whining about how Bonobo is technically superior before XParts was born, do you?"
    author: "Stof"
  - subject: "Re: Yet more GNOME bashing..."
    date: 2001-08-31
    body: "ROTFL!!!  Did you even bother reading the rest of my comment???  To quote:\n\n\"So let's all celebrate this as very nice recognition of all the hard work that volunteers have put into this, and keep it civil!\"\n\nThe entire tone of my message was (supposed to be) let's cheer on the home team without resorting to jeering the \"other team\".  Sorry if that wasn't excruciatingly clear :-)"
    author: "David Bishop"
  - subject: "Best open source project."
    date: 2001-08-30
    body: "Yes it is, isn't it? A well deserved prize.\n\nSeth"
    author: "Seth"
  - subject: "Congratulations"
    date: 2001-08-30
    body: "I hate to be a \"me too\", but I just have to say that I've been a KDE user at both home for about a year and at work for almost four months now,  and have been impressed, to say the least, at the progress of the KDE desktop and office suite.  This award is well-deserved.\n\nErik Hill"
    author: "Erik Hill"
  - subject: "I don't like KDE"
    date: 2001-08-30
    body: "Why? you may ask.\n\nWell, perhaps it was a truth with modifications.\n\nKDE is developed so fast, I cannot keep up with all the goodies using my 56 kbps modem...\n\nActually, yes, I am impressed! :)\n\nCheers! Hooray!"
    author: "Dr No"
  - subject: "Congrats!"
    date: 2001-08-30
    body: "I have known for a long time that KDE is THE Open Source project! And I'm happy to notice that others have noticed that too :)!\n\nCongratulations to the KDE-team!"
    author: "Janne"
  - subject: "Congrats!!! :)"
    date: 2001-08-30
    body: "I wish all success for my nice KDE people and users, And hope that KDE 3.0 will be the best Desktop environment, the compters have ever seen ;). \n\nI have been requesting for 2 features in KDE, which I expect to get fulfilled in KDE 3.0:\n\n1. White & colorful Mouse Pointers\n2. Keybinding in Kicker (pressing the first letter of submenu opens it, making K Menu Keyboard friendly as in Windows or Corel Linux)."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Congrats!!! :)"
    date: 2001-08-30
    body: ">White & colorful Mouse Pointers\n\nGo talk to the XFree86 people, KDE can't do much about colorful/animated mouse pointers without some help from them.  White mouse cursors would be possible, but why?  I like them the way they are.  Think different :-)"
    author: "not me"
  - subject: "Re: Congrats!!! :)"
    date: 2001-08-30
    body: "Dude. Ever heard of a program called \"xaniroc\" (or something similar)?\n\nI compiled and ran that on an AIX/X11R5-machine in 1995. What did it do? It animates the mouse pointer. So it is possible!\n\nIIRC it's on ftp.x.org/pub/contrib or similar."
    author: "J\u00f6rgen Sigvardsson"
  - subject: "Re: Congrats!!! :)"
    date: 2001-08-30
    body: "Ever seen the mouse pointer in enlightenment?\n\nIt sure isn't your usual ugly black cursor"
    author: "Joe"
  - subject: "Re: Congrats!!! :)"
    date: 2001-08-30
    body: "Yep, it's indeed an unusual ugly black cursor. :)"
    author: "Spark"
  - subject: "color pointers - it should be possible"
    date: 2001-08-30
    body: "Everbody ever run a SDL program? They can have colored, animated, etc mouse cursors, as sdl works on X.... I think it is possible.\nAnd plus, I think we NEED (ok, ok, we WANT actually) it on kde 3.0.\nIt's a shame having a nice desktop with ugly mouse cursors :("
    author: "Iuri Fiedoruk"
  - subject: "Re: Congrats!!! :)"
    date: 2001-08-30
    body: "what would really be nice is to be able to change the cursor to whatever we want, maybe even have cursor themes, or integrate the cursor into the theme system...\n\nthis would especially help the visually impared, as the \"large cursor\" isnt really all that big...\n\nbut its a great desktop, and congratulations!"
    author: "Spiral Man"
  - subject: "Great !"
    date: 2001-08-30
    body: "Great work ! Congratulations.\n\nI just cant wait for KDE 3.0 !!!"
    author: "MBrain"
  - subject: "Congratulations to all of  the KDE people!!"
    date: 2001-08-30
    body: "You guys are the absolute very best there are!!\nYou guys deserve all the praise in the world for your hard work. We all appreciate it ver much. It's very nice to see \"official\" recognition (it is well deserved)\n\nThank you guys \n\nhave a lot of fun..\n\nOmar"
    author: "omar"
  - subject: "Congratulations to all of  the KDE people!!"
    date: 2001-08-30
    body: "You guys are the absolute very best there are!!\nYou guys deserve all the praise in the world for your hard work. We all appreciate it ver much. It's very nice to see \"official\" recognition (it is well deserved)\n\nThank you guys \n\nhave a lot of fun..\n\nOmar"
    author: "omar"
  - subject: "Congratulations to all of  the KDE people!!"
    date: 2001-08-30
    body: "You guys are the absolute very best there are!!\nYou guys deserve all the praise in the world for your hard work. We all appreciate it ver much. It's very nice to see \"official\" recognition (it is well deserved)\n\nThank you guys \n\nhave a lot of fun..\n\nOmar"
    author: "omar"
  - subject: "Congratulations to all of  the KDE people!!"
    date: 2001-08-30
    body: "You guys are the absolute very best there are!!\nYou guys deserve all the praise in the world for your hard work. We all appreciate it ver much. It's very nice to see \"official\" recognition (it is well deserved)\n\nThank you guys \n\nhave a lot of fun..\n\nOmar"
    author: "omar"
  - subject: "Congratulations to all of  the KDE people!!"
    date: 2001-08-30
    body: "You guys are the absolute very best there are!!\nYou guys deserve all the praise in the world for your hard work. We all appreciate it ver much. It's very nice to see \"official\" recognition (it is well deserved)\n\nThank you guys \n\nhave a lot of fun..\n\nOmar"
    author: "omar"
  - subject: "Re: Congratulations to all of  the KDE people!!"
    date: 2001-08-30
    body: "I think they got the message :0)"
    author: "me"
  - subject: "KONGRATULATIONS !"
    date: 2001-08-30
    body: "Way to go Konqui ! Kongratulations !"
    author: "hackorama"
  - subject: "Of Course..."
    date: 2001-08-30
    body: "KDE is the Best Open Source Project.  This is something that every KDE user already knows!\n\nI have used KDE ever since I first started using Linux and I even use it at work now on Digital Unix machines.  It has made my personal and work computer use a much more enjoyable and pleasant experience.\n\nCongratulations on the award (but I could have told you that years ago) and thanks from all of us loyal KDE users.\n\nBill"
    author: "wwelch"
  - subject: "Best desktop"
    date: 2001-08-30
    body: "Last year I said that KDE was good but still inferior to Win 98 desktop. Now I think it is superior, even superior to Windows XP desktop in terms of functionnalities. Konqueror, Kmail/KNode, Kicker, KWord are the leaders, Explorer/IE, Outlook Express, Wordpad are behind... And KDE has much more parameters to be adapted in different ways... And it is free...\n\nIt is still young, some little defaults or lacks are going to be fixed... It will take some time that the quality of the KDE work will be recognised. Today it is about the Open Source Project, tomorrow it will be about all desktops...\n\nI hope that it is the beginnning of a new OS, Linux/KDE, already different of the GNU/Linux that some ones try to impose by speech.\n\nDeveloppers, why some of you have not choose the best open source project ? There are so many things to do... It is inside KDE that you will satisfy the greater number of users, it is inside KDE that your programs will have the best future... Here you will be the most efficient... The KDE users are happy, you will be happy !\n\nAlso, I think that it would be fine to now create a third set of KDE programs (after desktop and office), something like KDE Tools or KDE Extra, with some great programs, KonCD, KWave, Quanta Plus and so...\n\nCongratulations and encouragements for the KDE team, developpers, artists, translators and others, there are many hard things to do, you are in the good way !..."
    author: "Alain"
  - subject: "Re: Best desktop"
    date: 2001-08-30
    body: "I wanted to do that too but don't have the time.\n\nGnome had the \"fifth toe\" concept which is exactely that.\n\nPackage a few publically available very good KDE programs that are not part of KDE cvs after each release.\n\nDoes someone want to help ?"
    author: "Philippe Fremy"
  - subject: "Re: Best desktop"
    date: 2001-08-30
    body: "I wanted to do that too but don't have the time.\n\nGnome had the \"fifth toe\" concept which is exactely that.\n\nPackage a few publically available very good KDE programs that are not part of KDE cvs after each release.\n\nDoes someone want to help ?"
    author: "Philippe Fremy"
  - subject: "Re: Best desktop"
    date: 2001-08-30
    body: "Free in the sense of freedom.  Without GNU we would not have Linux or KDE or any of the other wonderful projects out there."
    author: "Anthony Enrione"
  - subject: "Re: Best desktop"
    date: 2001-08-30
    body: "> Free in the sense of freedom. Without GNU we would not have Linux or KDE or any of the other wonderful projects out there.\n\nWho knows ? Not in the same way, not in the same time... GNU has not the monopoly of freedom... It does not use freedom when it tries to enslave Linux with the word GNU... \n\nI agree with the strenght of the GPL, I disagree with the GNU project.\n\nI may say \"I work on KDE\", it is precise, I may say \"I work on Linux/KDE\" (not on BSD/KDE), I may say \"I work on Linux\", it is less precise. I cannot say \"I work on GNU\" or \"I work on GNU/Linux\", it is false. Directly, I don't use any GNU programs, excepted bash, sometimes... I don't think that gcc and bash are more important that the Kernel, XFree or my distro tools.. And KDE is the most important, of course..."
    author: "Alain"
  - subject: "*sigh*"
    date: 2001-08-30
    body: "There is no operating system called \"Linux\". Face it. There wasn't and there will probably never be.\nOperating systems are things like FreeBSD, NetBSD, BeOS, Mac OS X, ... you get the point. There was never THE distributed Linux system. You could cal Debian, RedHat and SuSE \"Operating Systems\".\nLinux is just a part of it, like X or KDE.\n\"GNU\" is not about the software. There is no software called \"GNU\". GNU indeed IS an operating system.\nGNU was almost a complete operating system, but the kernel missed. Linux delivered. Now it's the GNU/Linux operating system.\nTo call it \"Linux\" is kinda stupid, cause you could even place Linux into Mac OS X... would it still be Linux? Hm? No, it would be Mac OS X with Linux kernel. You just changed a part, not the system.\nTo bitch about that cause you think \"Linux\" is everything is really, really shortsighted.\n\nI really can't stand this Anti-GNU zealotry anymore.\nYes, they are strange, but they are in no way evil.\nThey are a bit strict about free software, but that's there job after all! Like it's the job of Microsoft to promote closed software.\nOn which side do you stand?\nI would prefer a free world to a closed world, although I don't have a problem with something inbetween!\nThere is a lot of Anti-GNU FUD going around and some of you are gladly picking it up.\nThe only party who really profits from that is Microsoft and other closed software companys.\nThink about it.\nSometimes I get the impression that some KDE users are just upset because RMS was against KDE as it wasn't really free."
    author: "Spark"
  - subject: "Re: *sigh*"
    date: 2001-08-30
    body: "I think you're right except for the \"linux is not an operating system\" part.\nBecause Linux is an operating system.\nIt has memory management, process management, filesystems, drivers. Thats an operating sytsem, or do I miss something?\n\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: *sigh*"
    date: 2001-08-31
    body: "Its an operating system kernel... nothing more.\nThe design happens to be monolithic which means all that crap you mentioned is in one place with not too much thought about the possibility of ever serparating it.  \n\nTry using linux without ls, cp, bash, and Xwindows sometime.  I bet its not much of an Operating System then.  GNU Linux is the linux kernel based OS using the GNU tools to complete the project.\n\nThe BSD's have in their CVS all the source to everything you need for a complete BSD OS.  This is where linux is different.  All you get from kernel.org is the kernel."
    author: "Dave Leimbach"
  - subject: "Re: *sigh*"
    date: 2001-08-31
    body: ">  Try using linux without ls, cp, bash, and Xwindows sometime.\n\nBash is not the only shell on Linux, I may easily use sh or csh, they also have ls and cp... I don't remember if XWindows is a part of the GNU project, but it is neither GPL, neither LGPL. And if something is important above the Kernel, it is the desktop for a majority of users..."
    author: "Alain"
  - subject: "Re: *sigh*"
    date: 2001-09-02
    body: "You didn't get the point.\nThis is absolutely NOT about \"what is most important so we should call it like that\", it's just about the simple fact, that \"Linux\" is not a complete operating system and as long as there is not the \"Linux OS\", we need different names for it.\n\"Debian, \"Red Hat Linux\", etc are names for such system. But if you take the very basic of it (like building a Linux from scratch), you will end up using the \"almost finished\" GNU system with the Linux kernel. It's your choice, how you want to call it.\nGNU/Linux is the most appropriate thing I heared yet. \"Linux\" is definetly not and it would be very stupid to find a new name for it, just because you don't like GNU."
    author: "Spark"
  - subject: "Re: *sigh*"
    date: 2001-08-30
    body: "> There is no operating system called \"Linux\n> To call it \"Linux\" is kinda stupid\n\nIt seems that nothing exists excepted GNU... The Kernel, X, the desktop, the distro tools and so and so, all is kinda stupid in comparison...\n\nDo you really know that KDE is not a part of the GNU project ?\n\nI feel tired that some integrists want to impose a little part of the OS in its name. You are the agressors, it is not surprising that it creates some reject... I prefer a free world to the closed intolerant GNU world. But the first thing in my choice is the good KDE work and fun... And a good mind, as the Linus one...\n\nAnd I repeat that I think that the GPL is a good thing, so RMS is great. But as many great ones, he has some defaults...\n\nSome complementary informations :\nhttp://www.linuxplanet.com/linuxplanet/opinions/3365/1/"
    author: "Alain"
  - subject: "*sigh* indeed..."
    date: 2001-08-31
    body: "What's all the fuss about RMS' *opinion*?\nIt's not like he force you or anybody else to say GNU/Linux instead of Linux.\nJust because he THINKS that the term GNU/Linux is more correct, somehow the entire GNU project is evil?"
    author: "Stof"
  - subject: "Re: *sigh* indeed..."
    date: 2001-08-31
    body: ">  It's not like he force you or anybody else to say GNU/Linux instead of Linux.\n\nHe, and his friends, has a continuous moral pressure on the medias. See :\nhttp://www.linuxplanet.com/linuxplanet/opinions/3365/1/\nYou also may read some of his speechs, for example in an university one month ago..."
    author: "Alain"
  - subject: "Re: *sigh* indeed..."
    date: 2001-08-31
    body: "Linus will end this abuse of his owned trademark.\nI wonder why he hasn't done it already."
    author: "reihal"
  - subject: "Re: *sigh* indeed..."
    date: 2001-08-31
    body: "> Linus will end this abuse of his owned trademark.\n\nIt will be a very good thing.\n\nA revealing anecdot :\n\nInternet Actu is perhaps the most french popular weekly newsletter (55000 readers). Around one month ago, it replaces everywhere the word \"Linux\" by\n\"GNU/Linux\" (you may verify at http://www.internetactu.com/archives/index.html#2001). I wrote to explain my desagreement. One journalist explains me that \"GNU/Linux\" was used with the\nagreement of Linus Torvalds. I then explained that it was false, I showed the article of Brian Proffitt. In his answer, the journalist explained that he understood my position, but, alas, that he could not change the things...\n\n*sigh* indeed..."
    author: "Alain"
  - subject: "Re: *sigh* indeed..."
    date: 2001-08-31
    body: "Actually, I have SEEN RMS force people to say it.\n\nSimply, he stood there screaming his head off about it whenever the other guy (who was giving a conference) said \"linux\".\n\nShort of beting him with a club, he did anything he could to force him to say GNU/Linux.\n\nPersonally, if he ever did that to me, I would have him expelled from the audience, because it is not acceptable behaviour in a public event."
    author: "Roberto Alsina"
  - subject: "Re: *sigh*"
    date: 2001-09-02
    body: "> It seems that nothing exists excepted GNU... The Kernel, X, the desktop, the distro tools and so and so, all is kinda stupid in comparison...\n\nIt's really not about the software.\n\n> Do you really know that KDE is not a part of the GNU project ?\n\nOf course. :) And so are XFree, Apache, vi, Mozilla, whatever. I'm absolutely aware of this fact.\n\n> I feel tired that some integrists want to impose a little part of the OS in its name. You are the agressors\n\nWe are what?\nAll I say is that it's more than stupid to bitch against RMS for calling it GNU/Linux. It all depends on what you are talking about. \nGo on calling the kernel \"Linux\", no problem. Talk about \"Linux based operating systems\". No problem either. But if you just talk about \"Linux\" if you refer to any Linux system out there, I'm really confused.\nThere was and is no Linux OS. Simple as that. You can't say anything against this.\nSO you can't say that Linus is the one who produced this \"OS\", cause _there is no Linux OS_.\nThere are a lot of OS out there like Debian GNU/Linux, Linux Mandrake, Red Hat Linux, SuSE Linux, Caldera Open Desktop, ...\nThey basically do just that: They use the GNU foundation tools and the Linux kernel.\nSo the most correct naming would be something like \"The Debian operating system is based on GNU/Linux\", or \"The Debian operating system is based on GNU software\", or \"The Debian operating system is based on the Linux kernel\". Interesting is, that the latter is not even true.\nDebian is almost kernel independent, there is Debian GNU/Hurd and even a Debian FreeBSD project.\n\nThe interesting questions are:\n1) How do we call everything based on Linux?\n2) What is the main part of the free system we all use and which is a great danger to MS Windows systems?\n3) So how should we call the \"core\" of what is similar to all current \"Linux based\" systems like Debian, Red Hat and SuSE?\n4) Is it ok from RMS to call every current Linux based system \"GNU/Linux\"?\n5) Why does this argument even exist?\n\nMy opinions:\n1) Simple as that: \"Linux based systems\". Those systems are not \"Linux\". Like every system that uses X for the desktop isn't \"Xfree\", but it's based on Xfree. I know that it's more difficult to express than just saying \"Linux\", but it's correct.\n\n2) Let's see... it's basically Unix. Linux is POSIX compliant and therefore almost a real Unix. It is basically a free and quite easy to use \"Unix\".\nThe kernel is the \"lowest\" part and talks to the hardware.  On top of this, we need a lot of software, like Bash, etc. Those are provided by GNU, but most of it also by BSD and others. You have alternatives, it may be possible to build a system completely without any GNU software.\nThan there is Xfree. Probable the most important part here. Why? Because most of our applications depend very heavily on X. You can port X to Windows or Mac OS and you will be able to run those applications. Now imagine a System without X. You would have to start from scratch, almost. \nNext on this stack come the toolkits like Qt and Gtk. They are also very important, cause some software only relys on them. So if you port the toolkit to another graphic system, you will probably be able to port most of the apps that use this toolkit.\nOn the very top of this, there is the software like KDE, Gnome and all the apps.\nSo basically we have this structure:\n- Kernel\n- Environment\n- Graphic System\n- Toolkit\n- Software\nIs anything of that replaceable? No, not anything but _everything_. \"Linux\" is the name of the most popular Kernel for such systems, but if you talk about \"Linux\", you talk about almost everything that would use Linux as the base.\nThis is what is curretly looks most of the time:\n- Linux\n- Different tools, most provided by GNU\n- XFree\n- Gtk/Qt\n- A bunch of software\n\nWhich of this is most important? I think there is no answer to this question. KDE runs on \"XFree\", not on Linux or FreeBSD. I don't think there is any kernel-dependent code in KDE.\nIs Xfree the most important part of the system? This could even be true. While this isn't exactly the best or biggest or greatest software, it might be the hardest to replace, while everything else is quite flexible.\nBottom line: I don't know. What makes our todays \"Linux\" systems is basically just a bunch of software that can \"free\"ly mixed together. And this might BE the most important part... think about it.\nThe key is, that distributions can use whatever they want and that everyone can mix together everything. It is basically FREE software that lead to the success of Linux/GNU/X.\nSo we could refer to the whole bunch of things as \"free software\". But this is just a description, not a name. You see, it's almost impossible to come up with a name. And maybe that's what it is. Impossible. It maybe comfortable to have a single name for all this stuff, but it seems to just not be possible! \n\n3) The core actually is Linux. Yet.\nWhat if the core changes? If we refer to \"Linux\" as the OS all the time, we might confuse people VERY much. It even confuses me.\n\"Linux based\" still seems to be the correct term for those operating systems. And again, there is no \"Linux OS\" out there. Nothing. Nada.\nMaybe we should talk about Unix compatible operating systems and X. If we want to talk about a specific OS, we should really use the NAME of the system. Like Debian, Red Hat Linux, etc. Those are complete (Linux based) operating systems.\n\n4) Most \"Linux\" Distributions try to be compatible. There is nothing yet that for example didn't feature Xfree or the bash.\nIt makes sense to find a common name for everything.\nAnd if you are REALLY honest, you should accept that all current distributions use 1:1 the GNU system environment combined with a Linux kernel.\nSo YES, I find the term GNU/Linux to be quite appropriate for THIS KIND of Linux based operating systems. There might be other Linux based systems in the future, so this should be called differently so you know what's going on.\nThat's why I think it's ok from RMS (and me) to refer to a \"GNU/Linux\" System if talking about this kind of Linux based systems. \nOf course this is not set to stone, there is no OFFICIAL name for anything. There just ISN'T anything like \"the Linux system\" which could be called \"GNU/Linux\". It's just an appropriate name for everything that is based on GNU/Linux. \nIf someone comes up with a better suggestion, great! Feel free to use it. But don't just change the name for the sake of changing, cause GNU/Linux is quite well known today. \nI agree that it's not nice from RMS to force anyone to say Linux. But that's just something personal about RMS. It's his opinion and he expresses his opinion in a very strong way (that's why he is such a successfull man btw). It's not that anything bad will happen to you if you don't follow his advice. You just have to be as strong as he is, if you think that your expression is more appropriate. :) This will be difficult if you prefer the term \"Linux\", cause we just saw that \"Linux\" is terrible inaccurate and misleading if you aren't talking about just the kernel. Good luck.\n\n5) This is simple. Because the \"Linux community\" is a terrible mess. Everyone is doing what he want to, there are no clear rules or even guidelines. It's just a bunch of creative chaos.\nThere is nothing wrong with it, but under this light, they really have NO right to insist on any clear terms. It's _their_ fault that they didn't come up with anything that could be used as a reference. \"Linux\" is the name of the kernel. That's it. It was made to fit together with the GNU system, not the other way round. \n\n\n> I prefer a freebworld to the closed intolerant GNU world. \n\nHm... I accept your choice to prefer anarchy style freedom, but why don't you accept other choices to prefer protected freedom?\n\n> And a good mind, as the Linus one...\n\nLinus is just a human beeing with faults, like anyone else too (like RMS).\nDon't take anything what he says for granted just because he's the center of the Linux hype (which is not even really deserved btw).\n\n? And I repeat that I think that the GPL is a good thing, so RMS is great. But as many great ones, he has some defaults...\n\nDefaults? ;)\nWell, of course he has.\nBut that has nothing to do with GNU, right?\nSo why do you call it a \"closed intolerant world\" or say things like \"it tries to enslave Linux with the word GNU\"?\nThat's simply not true. \nI really don't understand your problems with the word \"GNU\". And I get sick of reasonings like \"Sure, GNU did a lot for us, but who knows if others wouldn't have done it if there wouldn't have been GNU\".\nWhat a bunch of crap.\n\"Sure, Linux is nice, but who knows if something else wouldn't have been done something better, if their wouldn't have been Linux?\"\nDo you get the irony?\n\n> http://www.linuxplanet.com/linuxplanet/opinions/3365/1/\n\nQuite interesting read.\nAnd he comes to almost the same conclusion as I do.\n\n\"But I would caution the critics of the term \"GNU/Linux\" not to gloat in their apparent victory in this instance. For while my sense of\n                                   professionalism has bound me to one term, on a personal level, I think the GNU Project has every right to stand up and argue for this\n                                   point. The origins of GNU and Linux are intertwined more than enough to make a case for the use of the combined term.\"\n\n\"Just remember not to lose sight of what is really important: that it's not about personalities or egoism, no matter what some might\n                                   have you believe.\"\n\nI think Brian got it right again.\nAnd guess what? RMS didn't FORCE him to do anything, he just ASKED him to do so.\nBrian still lives and RMS didn't do anything against Brian. So RMS doesn't seem to be THAT bad, right?\nWe need crazy guys like RMS, otherwise the world would be grey in grey.\nRMS is not evil.\nAs RMS figured out, MS does not really fear Linux (there is no need to, Linux is not so revolutionary from a technical POV), but the GPL.\nDo you remember that Ballmer said the GPL would destroy innovation and that he believes in the \"American way\" or crap like that?\nAnd do you remember that Ballmer jumped around like a bunny on exctasy on a press conference, screaming out loud, how much he loved his company?\nDid you ever see RMS doing anything like that? :)\nBallmer is not less (or even more) fanatic and crazy as RMS. We need someone like that on our side who is able to talk to the massses. \nIt's ok to NOT follow RMS in everything he says (after all he IS crazy ;)), but it's just lame to bash him for what he's doing.\nKeep your energy to bash at Steve. ;)"
    author: "Spark"
  - subject: "Re: *sigh*"
    date: 2001-09-03
    body: ">  It's really not about the software.\n\nIt's about policy ?\n\n>  \"Linux based\" still seems to be the correct term for those operating systems.\n\nYes, without GNU (in the words, but with GNU things and many others in the system), and to be short, we say \"Linux\". \n\nI see in your speech how you mix GNU and Free, as if any GPL program was GNU. As if anybody OK with the GPL would be OK with the GNU Project. It is not. Of course you guess that I am less opposite to GPL/Linux than GNU/Linux... and you mix these two concepts, you use the GPL as a weapon for the expansion of GNU.\n\nBut if I leave policy to return to software (and the OS is a software), I always see few GNU programs in the system I use."
    author: "Alain"
  - subject: "Re: *sigh*"
    date: 2001-08-31
    body: "> Sometimes I get the impression that some KDE users are just upset because RMS was against KDE as it wasn't really free.\n\nAnd you obviously don't understand what free means. KDE was always free and GPLed, every part of it. So watch otu what you are stating."
    author: "jj"
  - subject: "Re: *sigh*"
    date: 2001-09-02
    body: "Can you call something \"really free\" if it depends on something, that isn't?\nI _do_ watch out what I'm stating. Mostly. :)"
    author: "Spark"
  - subject: "Re: Best desktop"
    date: 2001-08-31
    body: "> Who knows ? Not in the same way, not in the same \n> time... GNU has not the monopoly of freedom... \n> It does not use freedom when it tries to enslave \n> Linux with the word GNU...\n\nOh sure, just because RMS has an opinion, suddenly the entire GNU project is guilty.\n\nIf Torvalds disagrees with RMS and calls the entire system just \"Linux\", RMS will sue him, remove his right to use the GPL, and threaten him to call it \"GNU/Linux\".\nDoes that sound stupid? Of course it does.\nHow can GNU \"enslave\" Linux just because of one man's opinion?\n\n\n> I cannot say \"I work on GNU\" or \"I work on \n> GNU/Linux\", it is false. Directly, I don't use \n> any GNU programs, excepted bash, sometimes... \n\nThen that is YOUR opinion, seperated from RMS'.\nPerhaps RMS don't use KDE, but use GNOME or no desktop at all.\nPerhaps he only use GNU software. If that's true, then he has every right to call his system GNU/Linux.\nAnd you have every right to call your system whatever you want.\nGet it?"
    author: "Stof"
  - subject: "Re: Best desktop"
    date: 2001-08-31
    body: "> Oh sure, just because RMS has an opinion, suddenly the entire GNU project is guilty.\n\nI did not say such a thing. And I do not think it, of course.\n\n> And you have every right to call your system whatever you want.\n> Get it?\n\nSome ones want that anybody call it GNU/Linux. But the problem is not at first about my rights, it is about the fact that many medias now speak of KDE as a desktop of the \"GNU/Linux OS\", because some integrists (it's not \"just one man opinion\") try to impose GNU by the speech. Here is the agression, the wish to enslave the word Linux."
    author: "Alain"
  - subject: "Re: Best desktop"
    date: 2001-08-31
    body: "1. There are people who try to enforce the term \"GNU/Linux\" and bash the term \"Linux\".\n2. There are people who try to enforce the term \"Linux\" and bash the term \"GNU/Linux\".\n\nLooks more like civil war to me..."
    author: "Stof"
  - subject: "Re: Best desktop"
    date: 2001-08-30
    body: "You wrote...\"Last year I said that KDE was good but still inferior to Win 98 desktop. Now I think it is superior, even superior to Windows XP desktop in terms of functionnalities. Konqueror, Kmail/KNode, Kicker, KWord are the leaders, Explorer/IE, Outlook Express, Wordpad are behind... And KDE has much more parameters to be adapted in different ways... And it is free...\"\n\nThis is of course utter nonsense. KDE is significantly slower than NT4. Konq is significantly slower than Opera and IE. NT4 is significantly easier to upgrade (I have trashed two Linux installs now because of installation dependancies (I'm a newbie). With MS, at least you get a Windows Update, and with Gnome you get Red Carpet (which is excellent also). Open Office is at least WYSIWYG unlike KOffice. Outlook express is miles ahead of KMail...\n\nBut anyway, keep up the good work guys. I'm looking forward to KDE 3.0."
    author: "Rob"
  - subject: "Re: Best desktop"
    date: 2001-08-31
    body: "> KDE is significantly slower than NT4. Konq is significantly slower than Opera and IE\n\nOn an old PC with few memory, it is significant. It is less important in many config, for example 350 Mhz, 128 Mo. \n\nA Windows upgrade is esier than a Linux upgrade, yes, but I spoke about the desktop, not the distros upfates.\n\nAnd, sorry I was a long time a user of Outlook Express and Kmail/Knode is better. Some examples : automatic wrap while typing text, the hierarchy in the mail messsages, a non-proprietary file format, marks for answered mails, the colors in the >, > >... sentences, better tooolbar management and so..."
    author: "Alain"
  - subject: "yahoo!"
    date: 2001-08-30
    body: "Good job guys!  Major rewliness!\n\nPS Looks like we need an awards category. :-)"
    author: "Navindra Umanee"
  - subject: "Kongrats!!"
    date: 2001-08-30
    body: "This is wonderful. But i hope the following features would be implemented.\n\n- WINE support\nWell, wouldn't it be nice to hover a .exe made for windows icon in konqi and right click it, select \"Windows application\" and it would open in WINE from that day onwards in Konqi, KMenu and Kicker and ofcourse, the desktop!\n\n- Better GNOME and Motif support\nThere is a lot of GTK and Motif apps around. I don't use GNOME, so possibly KDE can apply themes on GTK and Motif (on user demand). This is because my Netscape window (not the title bar :) look different than my GIMP window which looks different from the rest of the desktop.\n\n- Use Gecko in Konqueror.\nYes i know there is KMozilla, but i want to use Gecko in my fave file manager. So Konqi team should put an option for users to choice between Gecko and KHTML. At least Konqi users don't have to suffer from KHTML setbacks.\n\nBest of luck to you guys. Wish you all developers good health and God bless!"
    author: "Rajan Rishyakaran"
  - subject: "Re: Kongrats!!"
    date: 2001-08-30
    body: "I thought that Codeweaver's WINE bundle already did what you're asking for."
    author: "Psidekick"
  - subject: "KDE and wine :)"
    date: 2001-08-30
    body: "Just define new application shortcut (.desktop file) which executes wine, than (if not defined) define mime type for .exe files and than let newly created application for wine (you may call it Windows Application.desctop) to be default application for the .exe mimetype.\nThat's all you need to make konqui to open .exe with wind :) (ofcorse the distro could do it for your self)\n\nAlso about the KDE support for wine, I must tell ya that wine supports kde :) because the windows icons of apps appear correctly in kicker, even apps that use systray icons appear in kicker system tray :)\n\nAbout the shortcuts in KDE start menu you can simly create .desctop file and place it in your users home or global KDE menu dir. The shortcut command line should be comething like:\nwine fullpath_of_windows_app\ne.g.\nwine /mnt/win_C/Windows/notepad.exe\n\nenjoy :)"
    author: "kde user"
  - subject: "Re: Kongrats!!"
    date: 2001-08-30
    body: "- WINE support\nWell, wouldn't it be nice to hover a .exe made for windows icon in konqi and right click it, select \"Windows application\" and it would open in WINE from that day onwards in Konqi, KMenu and Kicker and ofcourse, the desktop!\n\n\nWell I have it in 2.2. RMB on a .exe shows a \"Wine\" entry\nthat runs the app through wine"
    author: "yves"
  - subject: "So..."
    date: 2001-08-30
    body: "... just where are we going to keep that lovely shiny piece of glass?  Can we take turns at keeping it perched atop our monitors?  Let's see, if everyone gets to hold it for a day, that will take at least how many decades to get around everyone??? \n\n:-)"
    author: "John"
  - subject: "Re: So..."
    date: 2001-08-31
    body: "just make sure KDE get's another one next year...\n\nKDE rocks my world, for the past eight months I've been continually amazed by the functionality and constant blitzkreig (sp?) development.\n\nHurra!"
    author: "Eric Nichlson"
  - subject: "Where's KDE 3.0 wishlist site?"
    date: 2001-08-30
    body: "Good job! Now where's KDE 3 wishlist site.. i wanna make wish :P"
    author: "deman2k"
  - subject: "Re: Where's KDE 3.0 wishlist site?"
    date: 2001-08-30
    body: "go to each projects web page, find a email, and emails galore!"
    author: "Rajan Rishyakaran"
  - subject: "Re: Where's KDE 3.0 wishlist site?"
    date: 2001-08-30
    body: "> ... and emails galore\nand of course with patches included :-)\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Where's KDE 3.0 wishlist site?"
    date: 2001-08-30
    body: "Wishes can be send through the bug reporting system. All you have to do is go to http://bugs.kde.org and send a bug with severity Wishlist."
    author: "Wilco Greven"
  - subject: "Well deserved!"
    date: 2001-08-30
    body: "Kongrats to the pointed glassy thingie and to the nice blue ribbon for Konqi.\n\nI knew this was coming when I first saw KDE 1 beta in SuSE 5.2."
    author: "reihal"
  - subject: "port vim to KDE?"
    date: 2001-08-30
    body: "Wonderful, folks@KDE - well deserved, I must say.\n\nNow if only someone took the initiative and made a KDE port of vim (like the current GTK version, gvim)..."
    author: "kdeonfreebsd"
  - subject: "Re: port vim to KDE?"
    date: 2001-08-30
    body: "Or KEmacs, rewritten from scratch in a real programming language (and least one with types :-)), with all the features (especially indenting/completion) and binary modules (no guys, elc is just bytecode).\nWhere's a project page ? I am willing to join !"
    author: "Henri"
  - subject: "Agree"
    date: 2001-08-30
    body: "Would be really cool to have! I've heard that a gtk+ version will be built by the XEmacs gang. It would give the KDE project a lot of status if KEmacs can be completed before gtkEmacs :)"
    author: "Loranga"
  - subject: "Re: Agree (to KEmacs !)"
    date: 2001-08-31
    body: "Yes, I had a look at these, but\n1) it does not use an OO tk\n2) it was just about binding Xemacs to Gtk while keeping the lisp monolith\nSo a KDE Emacs rewritten from scratch (with the actual KDE Text component) could be a real winner. If I had the time, I would love to start this project myself but all I can offer yet is participation if such a thing actually begins..."
    author: "Henri"
  - subject: "Re: Agree (to KEmacs !)"
    date: 2001-08-31
    body: "I just read in the kdevelop mailing list that Gideon has a Emacs KPart :-)"
    author: "Roberto Alsina"
  - subject: "Re: port vim to KDE?"
    date: 2001-08-31
    body: "That's an interesting suggestion.  Personally I don't find the lisp source code to be such a bad thing.. You've gotta admit lisp is a cool language! .. and, I believe it's what's made emacs so customizable and extendable.\n\nThis is not to say things couldn't be \"better\".  Certainly your \"monolith\" argument is *right* on the money.  If \"they\" (I hate it when people use that word!) could decouple the interface (I'm visualizing a generic lisp frontend to various gui toolkits and/or curses, sure, easier said than done), the elisp interpreter, the, uh, doctor, and all the other kitchen sink bits, it would rock.  And we'd get several nice programs/libraries for the price of one.\n\n(as I understand it the reason it wasn't originally built this way was the tight memory/disk space requirements 20 years ago .. too bad emacs is still a pig today.)\n\nI don't know how far the xemacs team is going with this kindof thing; personally, if they can redo the customization buffers so they behave like one single other interface on earth, I'll be impressed.\n \nWhile I'm dreaming, I wish the fsf would finally run info2html on their helpfiles and forget they ever invented the 'info' format.  Sometimes oldschool is nice, and sometimes it's just painful..\n\n</gripe>\n\nThe amazing thing about emacs is how it's still better than anyting else :-)"
    author: "blandry"
  - subject: "Re: port vim to KDE?"
    date: 2006-05-04
    body: "just f.y.i. (i'm not trying to suggest that anyone in particular was ignorant of this fact) but lisp is a very efficient language.  most untyped langages are interpereted scripting languages like perl and ruby.  recursion also has a bad name in performance, since other languages' compilers do not optimize for tail-recursion (this is one big reason why 'compilers' that turn new languages into C or JVM bytecode are never that good.)  lisp and its compiler are very efficient.  a well written lisp program will run as fast and sometimes faster than an equivalent C program.\n\nthe only problem that lisp has is its lack of familiarity to programmers.  but there's a very good reason RMS chose lisp for emacs: functional programming is awesome for text manipulation.  ruby's strong and elegant fp implementation is a very big part of its sudden popularity.\n\ni would suggest keeping as much of the lisp code as possible (total rewrites are almost always a recipe for endless project delays, look at what happened to netscape 5.0) and refactor the display code to be independant of the backend and scripting implementation.\n\nthere are a lot of libraries out there for making C/C++ get along well with lisp.  if this project happens, i'd suggest a restructure & reimplementation of the display, rather than a complete rewrite.\n\np.s. a complete rewrite would almost certainly break compatibility with at least some of the enormous plethora of plugins out there."
    author: "kartar"
  - subject: "Re: port vim to KDE?"
    date: 2001-08-30
    body: "gvim is nice and all, especially if you have a PDP-11 keyboard.  If any editor needs to get a real hard look at for a native KDE port, it's jEdit.\n\nProper syntax high-lighting that's actually configurable.  Ability to install plug-ins on the fly to handle all kinds of stuff.  Gobs of features to boot.\n\nOnly down side is the memory usage due to running under the Java VM.  The reason I'm mentioning all this isn't so much to plug jEdit, but to encourage those folks working on editors for KDE to give the UI on this thing a really hard look."
    author: "Michael Collette"
  - subject: "Re: port vim to KDE?"
    date: 2001-08-30
    body: "Have you suggested it to the vim people?  The gtk version of gvim was done internally, not as a port by the gtk people, afaik."
    author: "pants"
  - subject: "Re: port vim to KDE?"
    date: 2001-08-30
    body: "A port of vim to KDE is underway. It can be found in kdenonbeta/kvim. It's not finished yet though."
    author: "Wilco Greven"
  - subject: "REAL men use vim in console only!"
    date: 2001-08-30
    body: "I HATE the Gtk version of Vim!  In fact, the main reason I like vim over emacs is because vim, is well, less \"graphical\" for lack of a better word.\n\n:)  Insanity can be a beautiful thing.\n\n-ron"
    author: "ron"
  - subject: "Re: REAL men use vim in console only!"
    date: 2003-12-11
    body: "i agree. however it would be nice to have vim integegrated seemlessly into kde? like to type out a message in kmail. or even enter text into a text box in konqueror / mozilla ... ?"
    author: "gi1242"
  - subject: "KDE is better than Windows"
    date: 2001-08-30
    body: "Well deserved recognition for the great work you have all done on KDE.  I am new to KDE (used to use gnome) and i was mortified when i first installed it to find a well integrated desktop environment.  Konqueror and KMail are fantastic apps and fast unlike Mozilla.  What is really cool is that at work :< i use win2000 which looks outdated compared to KDE.  There is no going back, the future is bright and it's KDE."
    author: "Anthony Enrione"
  - subject: "Re: KDE is better than Windows"
    date: 2001-08-30
    body: "I really like KDE and the main reason why I cannot use it more is the lack of support for S/MIME email and good IMAP support. I know Netscape 4 does it but I have always found that rather inferior to the smooth interface and comparatively good functions of Outlook Express.\n\nDoes anyone know if S/MIME is going to be more widely supported? I believe Mozilla are going to do something but do not think they have got very far. Unfortunately PGP is not an option because that is not what is used by all the people I have to correspond with.\n\nLook forward to the days when I can justify working all day on a Linux machine with a KDE Desktop.\n\nCongratulations on what has been achieved so far."
    author: "Flemming"
  - subject: "Re: KDE is better than Windows"
    date: 2001-08-30
    body: "in this week's KC KDE (not available yet) there is a section on S/MIME development that is going on as we speak for KDE3."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE is better than Windows"
    date: 2001-08-30
    body: "It's nice to see that KDE is always improving. \n\nOK, anybody don't always may use some programs, because he finds a lack for his personnal usage. But he knows it will come, he tries new versions, he prepares himself to use it. And then the function comes, he is ready... And when he uses the program regularly, he finds some little annoyances, he still does not retrieve a good comfort. And then improving continues, annoyances disappear and some little great things come... \n\nToday what I need about Kmail or KNode ? Nothing, it is perfect for my usage. About Konqueror, still some annoyances, they decrease while great things continue... About KWord, first usage, still some difficulties, already some goodies... KSpread, no, still not ready for me, it will come with next version, and so...\n\nAnd thank you Aaron for the KC KDE, it is precious to have a good visibility..."
    author: "Alain"
  - subject: "Re: KDE is better than Windows"
    date: 2001-09-10
    body: "KDE _is_ fantastic; but Mozilla is doing some real progress right now; I used yeterday to browse the web with Mozilla 0.9.4 pre \"Nightly\" 010909-05/Gecko 010909 compiled for i686 Linux; in my opinion it\u00b4s the by far best \"non IE\" browser yet, and now it's _fast_, very fast, too. And stable :-) Whatch out for the final Mozilla 0.9.4(not to mention the upcoming 1.0):) could be that the code now is so functional; that the team have started doing some real optimizations ? Anyway; as far as webbrowsing is concerned, in my opinion; even KDE would, from now on, benefit by incorparating gecko as default html renderer :-)"
    author: "Per"
  - subject: "Kicker needs new Style!"
    date: 2001-08-30
    body: "Yes, KDE bits win9.x/win2000 a lot where it comes to look & feel, but don't forget about XP.. in general it's a rip of Mac but you must agree it looks cool...\n\nI've created few themes for KDE (when kde.themes.org will allow to upload new themes again??? grr) and what I've noticed was lack of possibility to control kicker layout via ktheme/kstyle manager. In KDE 2.1, you could at least set the background image, now even this is not possible anymore. :/\n\n\nKicker is very important part of KDE, after all most of us look at it all the time, so it should be possible to control its look via new \"Kicker styles/themes\" like you can do for whole KDE now.\n\nKicker should have independed style/theme support. Imagine you want to have kicker menu in blue while the rest of the KDE menus are red...\nDon't getting the idea? Look at some M$ XP screenshots and you will know what I mean...\n\nWhat do you think about such idea? \n\nAnyways KDE ROX and congratulations to you, guys!!!\nLet the Big Pinguin bless ya all!"
    author: "sUpeRGR@sS"
  - subject: "Re: Kicker needs new Style!"
    date: 2001-08-30
    body: "I think that it would be best if we could select different themes for each individual application. Who wants a wordprocessor that looks like a CD-player or a CD-player that looks like a CAD program? In a world where all windows do not look the same you can find the right window much faster... but it might look a bit ugly - maybe it's enough to have different colour-settings for different apps, but always the same theme..."
    author: "Johan"
  - subject: "Re: Kicker needs new Style!"
    date: 2001-08-30
    body: "> it's enough to have different colour-settings for different apps, but always the same theme...\n\nIt already exists on many programs : you only have to change the background colour. So Konsole, Kmail, KNode, KEdit, Kate, Quanta Plus and some others have different colours.\nAlso, some programs, like KCalc have some nice coloured parameters... Yo, such things are rare in MS Windows, KDE has a very good look, also with nice readable icons and the lovely favicons of Konqueror...\n\nAs it is said elsewhere, I think that the first look & feel lack is now a blank or coloured cursor..."
    author: "Alain"
  - subject: "KCalc"
    date: 2001-08-30
    body: "KCalc is also a nice example of a program that gets screwed up by many new, oh so beautiful styles/themes. Using Aqua-ish button style wastes an enormous amount of desktop space when you have 20-30 \"small\" buttons!\n\nMaybe we should introduce the concept of thin-edge buttons to stop thick-edge-themes from wasting space in specific situations? It's wonderful to have large, nice OK / Cancel buttons, but the 20-30 calculator buttons should really be kept just big enough to do their job. But they still have to look good and fit in with the rest, so it's really the theme/style's job to provide these \"cheap buttons\"."
    author: "Johan"
  - subject: "Re: KCalc"
    date: 2001-08-30
    body: "Uhm, sorry but your comment's totally wrong. If you've done any checking you'd see that KCalc uses the same sized buttons no matter if your using old styles like the default or new styles like Liquid. Try it and see for yourself. I honestly don't know why people make such comments when it's visibly incorrect..."
    author: "Mosfet"
  - subject: "Re: KCalc"
    date: 2004-01-22
    body: "It uses the same amount of space on the desktop, but it loses more space from the buttons.  Either way it's kinda bad.  I wouldn't mind if KCalc used rows of rounded buttons with squarer sides, instead of them all being the bulgy things."
    author: "Trejkaz"
  - subject: "Re: KCalc"
    date: 2004-01-22
    body: "The GIMP suffers from this exact problem too if you use the Geramik theme.  The buttons all squish up and don't always look right."
    author: "Trejkaz"
  - subject: "Work needed in the style department"
    date: 2001-08-30
    body: "I congratulate the creators of KDE 2.2 as a programmer I can relate to the enormeous effort you have put into the system. I hope it is great fun for you.\n\nAs a regular user of KDE 2.2, I have noticed the following which needs to be adressed in the style/theming department of KDE3.X. \n\n1) Kicker's menus, toolbar, applet backgrounds, systray and extensions need a unified style engine model like the rest of the KDE desktop. It needs to allow proper plugins and backgrounds. Right now the image theming of kicker is very bad because of the default border color schemes (try setting any non white-to-white gradient picture as the background and be scared by the pure ugliness of the new look). And the menu's aren't themable at all (at least to a GUI user).\n\n2) The entire control of the look and feel is messy and all over the place. They should be split into Look and Feel. The styling of widgets, windows, fonts, menus, backgrounds and possible kicker should be in one submenu by themselves. In fact the Control Panel could do with a clean up and maybe some kind of user sophistication setting so that simple users like my mother, don't get thrown thousand terms in the face, which they don't need to see at all. Gnome's UI studies shows this clearly and it applies here as well.\n\n3) Kicker from a UI point of view is a little messy. The combination of grips (on the different parts of the GUI), insets on both the systray, clock and taskbar create a lot of visual noise, look to the new Mac OS X bar and the WinXP one as well to see how this is done best, less is more.\n\nOn another note:\nIf I understand correctly Actions, such as Save, Copy, Paste and so on, are signals send to applications in KDE. If that is so, then it could also be possible that the accelerator keys for these action are per-user configured from a KControl Module, so that all supporting applications suddenly are able to share and respect each users wishes for keyboard accelerators. Would that be possible?\n\nAll entries in the KMenu of Kicker should be editable by drag and drop just like in Windows (i.e. all entries are just files). It boggles my mind how this major usuability feature could be missed. It should however be User and Group respecting, so that people like the city of Largo, could install programs onto a central location and if the file is in the right place, the next time a user logs on or the menu cache updates the program appears on their menu...\n\nThese things will be entered into bugs.kde.org as wishlist items soon, and I might even find the time to code some stuff myself.\n\nBest regards ya all\nMikael Helbo Kj\u00e6r"
    author: "Mikael H. Kj\u00e6r"
  - subject: "Re: Work needed in the style department"
    date: 2001-08-30
    body: "I understand that good things may be more good, but here, I don't understand :\n\n> 3) Kicker from a UI point of view is a little messy. The combination of grips (on the different parts of the GUI), insets on both the systray, clock and taskbar create a lot of visual noise, \n\n?? You manage the systray and taskbar as you want. Did you tried the launch bar and task bar applets ? They are very fine.\n\nBut, yes, the clock may have more parameter (I think the LED style is not the most readable). Also there are some bugs to memorize applets when Kicker is vertical (+ clock too small)).\n\n>  All entries in the KMenu of Kicker should be editable by drag and drop just like in Windows \n\nYes, here it is an advantage of Windows, the menu management is heavy, and also we have no menu in the right click, so it is not possible to manage files in, the K Menu (but how many Windows users do it ?).\n\nI hope you will code, but, please, carefully, without adding some dysfunction ;-)..."
    author: "Alain"
  - subject: "Re: Work needed in the style department"
    date: 2001-08-30
    body: ">Yes, here it is an advantage of Windows, the menu management is heavy, and also we have no menu in the right click, so it is not possible to manage files in, the K Menu (but how many Windows users do it ?).\n\nMost Windows users I know that manage the system them self.\nThe whole Desktop needs to use Drag'n Drop consequent if users are to rely on it and use it."
    author: "tomten"
  - subject: "Re: Work needed in the style department"
    date: 2001-08-30
    body: "It should be always possible to turn off such drag'n'drop. I often happens to me to drag some incons out of kicker. It is also  much harder to manage other people desktops if they can screw their desktop with a one click !"
    author: "Krame"
  - subject: "Re: Work needed in the style department"
    date: 2001-08-31
    body: "On the matter of the taskbar and systray (well mostly the taskbar) the size and look of the grips are a tad too big, and the border within the taskbar I think needs to configurable (so that I could make it go away like I want it to).\n\nOn the matter of the drag'n'drop management of the K Menu entries (or any other entries) I know many Windows user, who expect this functionality (I had forgotten about the right click menus, but they can't be too hard to add). \n\nAnd if I code I most likely will start with something small, but then again I might not. And I always code carefully I have been doing programming (Java and C++) work professionally five years now."
    author: "Mikael H. Kj\u00e6r"
  - subject: "Re: Work needed in the style department"
    date: 2001-09-02
    body: "> On the matter of the taskbar and systray (well mostly the taskbar) the size and look of the grips are a tad too big,\n\nPerhaps, it is not obvious for me.\n\n> and the border within the taskbar I think needs to configurable (so that I could make it go away like I want it to).\n\nHere I prefer the KDE way (with position left, top...), it offers the same abilities than in Windows and it is less dangerous. I have seen too many persons on Windows changing the place of the bar by an error of mouse moving and unable to put it on the usual place.\n\n> On the matter of the drag'n'drop management of the K Menu entries (or any other entries) I know many Windows user, who expect this functionality\n\nYes. \n\n> (I had forgotten about the right click menus, but they can't be too hard to add)\n\nThe right menu is important, and a \"Move\" function may be added (as in kicker) to change the place of the program in the menus. It would be better that the moving by direct mouse drag in Windows (which may be done by error).\n\nI think that on Windows it is too easy to change the menu configuration by a bad mouse moving, I hope that such errors would be less easy on KDE.\n\nGood luck !"
    author: "Alain"
  - subject: "Re: Work needed in the style department"
    date: 2001-08-30
    body: "> then it could also be possible that the \n> accelerator keys for these action are per-user \n> configured from a KControl Module, so that all \n> supporting applications suddenly are able to \n> share and respect each users wishes for \n> keyboard accelerators. Would that be possible?\n\nkcontrol -> look 'n feel -> key bindings -> application shortcuts tab\n\nas for the style stuf, well, Qt3 introduces a new style engine and this change seems to be bringing about larger changes to KDE's theming capabilities. will be interesting to see what KDE3 holds in the look dpt."
    author: "Aaron J. Seigo"
  - subject: "Re: Work needed in the style department"
    date: 2001-08-31
    body: "Thank you for the tip. A load of my mind and all that. Good I didn't post the wishlist stuff yet."
    author: "Mikael H. Kj\u00e6r"
  - subject: "Re: Kicker needs new Style!"
    date: 2001-08-30
    body: "Have you seen www.stardock.com ? its fantastic! Why kde doesnt have a total change of the look&Feel of the menu bar for example, and add the MAC menu bar look&feel ? i mean, i would like a total GUI personalizer apps. Anyones knows if there is a proyect of an app to create a theme ? i think using XML will no be very difficult. You only have to set the colours or the xpm for the buttons, rules, menus, etc.. and it will be copied into an XML form and converted into a theme packet.\n\nExcuse my english :)"
    author: "Daniel"
  - subject: "Re: Kicker needs new Style!"
    date: 2001-08-30
    body: "all of this works today. what is missing in KDE is an easy way to access it. most things are scattered across several kcontrols and not very well integrated. therefore many people don't realize the customizability available to them.\n\nobject desktop is simply a program to allow MS Windows to work like X Window. now who's leading? =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Kicker needs new Style!"
    date: 2001-08-30
    body: "With Object Desktop you can customize ALL the desktop. On KDE or Gnome you cant."
    author: "Daniel"
  - subject: "Re: Kicker needs new Style!"
    date: 2001-08-31
    body: "really? what haven't i configured on my KDE? well, here is what i _have_ customized: widgets, window borders, panel colours and background, panel arrangement and behaviour, icons (mac+kde2 blend of an icon theme), desktop background (w/pattern blending, though i could have used a program's output too), sounds, desktop menus (and the mouse buttons), window behaviour, global keyboard shortcuts, application launch keys, application keys, notifications (this is going to be better in kde3, btw), taskbar behaviour, ...\n\ni could also have messed around with fonts, but i haven't.\n\ni'm just not seeing what i'm missing. nothing on my desktop looks like how it was when KDE first started up. in fact, many things don't even _behave_ the same. so, care to share some specifics of what i can't change under KDE that i can with object desktop, Daniel?"
    author: "Aaron J. Seigo"
  - subject: "Re: Kicker needs new Style!"
    date: 2001-09-01
    body: "As I wrote: try to set blue color for menu in kicker while the rest of KDE menus are set to red... impossible.. \n\nI wrote that KDE theming needed few tweaks... and for the beginning kicker was what we needed most.\n\nMaking KDE theme is really an easy task!\nAnd thx to the developers for such nice engine... (any1 who tried to make skin for Mozilla, knows what I mean).\n\nI don't think that applaying new Theme/Style is hard now.. all you have to do is click few times in Style, Theme, maybe sounds &icons,... but that way it's really flexible!\nYou can have sounds from one theme and look from another, etc... and this is cool, don't change it ala M$+... \n\nI agree with Mikael H. Kj\u00e6r - the kicker components should inherit the kicker style, and/or it should be possible to skin them separately...\n\nThe Kicker is THE FACE of KDE and you can't just leave things like they are now.... we need kicker to kick winblowz morons asses.. ehkm... eyes!"
    author: "sUpeRGR@sS"
  - subject: "masturbation ala KDE"
    date: 2001-08-30
    body: "News like these just some act of masturbation by KDE peoples...\nKeep masturbating and feel good about yourself."
    author: "theman"
  - subject: "Re: masturbation ala KDE"
    date: 2001-08-30
    body: "At least we have a good reason to feel good about ourselves. Unlike some other projects, KDE progresses fast and keeps on getting better all the time."
    author: "Janne"
  - subject: "Re: masturbation ala KDE"
    date: 2001-08-30
    body: "Oh yeah, let's all hush hush this news under the carpet. \nDon't let it slip out or the people behind KDE might feel good about them self... shock! horror! horror!\nYou sound like a jealous fool from another project or one of those types who whines about slashdot posting about a new linux kernel.\n\nYour theman! or whatever..."
    author: "geekster"
  - subject: "Re: masturbation ala KDE"
    date: 2001-09-01
    body: "well ... what's wrong with masturbation ?\n;-)"
    author: "tobs"
  - subject: "You know"
    date: 2001-08-30
    body: "that you made me to start using Linux.\nNow I'm the happy user KDE and Linux(Debian).\n\nBest Regards and Run KDE Run ;-)."
    author: "..."
  - subject: "Congratulation"
    date: 2001-08-30
    body: "Bravo ! F\u00e9licitations !"
    author: "Orph\u00e9e"
  - subject: "Re: Congratulation"
    date: 2001-08-30
    body: "Tu pourrais pas causer englais ?\nAh, au fait, bravo pour les d\u00e9veloppeurs de KDE !"
    author: "Tyrand'O"
  - subject: "Re: Congratulation"
    date: 2001-09-02
    body: "Ok, ok... I just say only a few words in french, and I choosed very known and internationnaly known words... :-)\nSo I repeat : Bravo KDE !"
    author: "Orph\u00e9e"
  - subject: "KDE Is the \"Best Open Source Project\""
    date: 2001-08-30
    body: "KDE has Konquered !!!\nCongratulations"
    author: "Prashanth"
  - subject: "Congratulation but too much like MS Windows"
    date: 2001-08-30
    body: "Congratulation to all the KDE developers. It is a very impressive job.\n\nBut I'm still a gnome and window maker user. I think that KDE is just a copy of the MS Windows desktop for Linux. It doesn't give a different approch to the computing experience. I use linux because it is a great OS and also because I want something different, a different desktop, a different approach. So I prefer to support projects that invent and create something different."
    author: "Nicolas"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-30
    body: "I have always wondered what makes KDE a \"windows-clone\" while Gnome is not? I mean, for everyday user they look similar, all three even have a \"Start-button\" in the bottom-left corner.\n\nSo do tell me why KDE is a windows-clone while Gnome is not?"
    author: "Janne"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-30
    body: "rose coloured glasses"
    author: "Aaron J. Seigo"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-30
    body: "The widget behavious, the \"sluggish\" look and feel, the colors, the windows style ;) the integrated Filebrowser/Webbrowser (no, Nautilus is not a Webbrowser although it can show HTML pages), etc.\nOf course everything is a bit different, but the style is pretty much the same. \nEven the menustructure is very much the same.\nI mean things like \"view\" and \"windows\" menus.\nHeck, KDE even copies the confusing and absolutely NOT user friendly settings/configuration mess of Windows. :)\nWindows may be some of the worst systems concerning usability, it really shouldn't be the thing to copy.\n\nBesides of that you are right, Gnome is pretty much a Windows copy as well. But at least Gnome is not that integrated. You don't HAVE to have a start button in the lower left, you can do whatever you want with your panel. :) You can make it floating in the middle of the screen and showing only your ICQ applet, while another Panel hides to the left of the screen and shows the pager if you want to.\n\nAnd the Toolkit does not even come close to the \"sluggish\" Windows look&feel. ;)\nThat's a matter of taste though."
    author: "Spark"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-31
    body: "What a bunch of BS. \n\n\n>  The widget behavious, the \"sluggish\" look and >  feel, the colors, the windows style ;)\n\nAll widget behavior can be styled/themed. The look AND feel can be changed. \n\n\n> the integrated Filebrowser/Webbrowser (no,  Nautilus is not a Webbrowser although it can show > HTML pages), etc.\n\nI agree with you partially here. Konqueror is a file browser/web browser. In fact, Nautilus really should use gtkhtml (port of khtmlw), or gtkhtml2 (port of khtml), AFAIK it'd speed loading local html pages up.\n\nKonqi is much more than a Filebrowser/Web Browser tho. It really shows how awesome the various kde2 technologies are (kparts, dcop, kio_slaves, etc..). For example, you can rip an audiocd while looking at a webpage, while using a terminal, all from the same konqueror window :). In fact, sometimes, instead of loading a window manager, I just load up konqueror. Split some views, and you are done :).\n\n>    Of course everything is a bit different, \n>    but the style is pretty much the same. \n>    Even the menustructure is very much the \n>    same.\n>    I mean things like \"view\" and \"windows\" \n>    menus.\n\n???? /me notices \"windows\" and \"views\" menus in many MacOS apps. Yeah, I'll have to admit, I'm writing this from my new ibook :). Both of these menus are present in Photoshop (version 5.0), and GraphicConverter(equivalent to imageMagik on Linux).\n\n>  Heck, KDE even copies the confusing and \n>  absolutely NOT user friendly\n>  settings/configuration mess of Windows. :)\n\nNo, it absolutly does not have something like the registry. KDE uses a system called KConfig. It uses easy-to-edit Key/Value pairs. In fact, it is somewhat similiar to Windows 3.1. \n\nIn fact, it is GNOME that copies the Windows Registry. GConf is exactly that. Maybe that's why there are flame wars every 2 days on Gnome devel mailing lists over using GConf versus bonobo-config :).\n\n> Windows may be some of the worst systems \n> concerning usability, it really shouldn't be \n> the thing to copy.\n\nright. \n\n> Besides of that you are right, Gnome is pretty much a Windows copy as well.\n\nright.\n\nif you want to talk about Window's copy. Look at Evolution. Totally Microsoftesque. ;)\n\n> But at least Gnome is not that integrated. You \n> don't HAVE to have a start button in the lower\n> left, you can do whatever you want with your\n> panel. :)\n\nHmm, this makes me think that you have never used KDE for more than 5 minutes. To remove the Kmenu, right click on it and click on remove. Heh.\n\n> You can make it floating in the middle of the  > screen and showing only your ICQ applet, while > another Panel hides to the left of the screen > and shows the pager if you want to.\n\nMake a new kicker extention panel, add the applet to it, change the panel size to 1%, and click on resize when necessary, chose the position. Make another kicker extention panel, chose auto-hide, add the pager applet to it, and chose the location and size. There you go. Easy wasn't it. Use KDE more than 5 mins next time.\n\n> And the Toolkit does not even come close to\n> the \"sluggish\" Windows look&feel. ;)\n\nQt 1.44's default X11 theme was quite Window-ish, I agree. Since Qt 2.x, I think gtk 1.2.x's default theme looks more Window-ish.\n\nBesides, that the default KDE2 theme(s) have no similiarty with qt's default theme.\n\n\n\nAnd yes, unlike you, I *do* try GNOME for more than 5 mins every few months to see how it is progressing. I *always* go back to KDE, which I have been using since 2.0, after using GNOME 1.0 and GNOME 1.2 for some time(I didn't like KDE 1.1 much),."
    author: "Jason Roks"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-31
    body: "What a bunch of BS. \n\n\n>  The widget behavious, the \"sluggish\" look and >  feel, the colors, the windows style ;)\n\nAll widget behavior can be styled/themed. The look AND feel can be changed. \n\n\n> the integrated Filebrowser/Webbrowser (no,  Nautilus is not a Webbrowser although it can show > HTML pages), etc.\n\nI agree with you partially here. Konqueror is a file browser/web browser. In fact, Nautilus really should use gtkhtml (port of khtmlw), or gtkhtml2 (port of khtml), AFAIK it'd speed loading local html pages up.\n\nKonqi is much more than a Filebrowser/Web Browser tho. It really shows how awesome the various kde2 technologies are (kparts, dcop, kio_slaves, etc..). For example, you can rip an audiocd while looking at a webpage, while using a terminal, all from the same konqueror window :). In fact, sometimes, instead of loading a window manager, I just load up konqueror. Split some views, and you are done :).\n\n>    Of course everything is a bit different, \n>    but the style is pretty much the same. \n>    Even the menustructure is very much the \n>    same.\n>    I mean things like \"view\" and \"windows\" \n>    menus.\n\n???? /me notices \"windows\" and \"views\" menus in many MacOS apps. Yeah, I'll have to admit, I'm writing this from my new ibook :). Both of these menus are present in Photoshop (version 5.0), and GraphicConverter(equivalent to imageMagik on Linux).\n\n>  Heck, KDE even copies the confusing and \n>  absolutely NOT user friendly\n>  settings/configuration mess of Windows. :)\n\nNo, it absolutly does not have something like the registry. KDE uses a system called KConfig. It uses easy-to-edit Key/Value pairs. In fact, it is somewhat similiar to Windows 3.1. \n\nIn fact, it is GNOME that copies the Windows Registry. GConf is exactly that. Maybe that's why there are flame wars every 2 days on Gnome devel mailing lists over using GConf versus bonobo-config :).\n\n> Windows may be some of the worst systems \n> concerning usability, it really shouldn't be \n> the thing to copy.\n\nright. \n\n> Besides of that you are right, Gnome is pretty much a Windows copy as well.\n\nright.\n\nif you want to talk about Window's copy. Look at Evolution. Totally Microsoftesque. ;)\n\n> But at least Gnome is not that integrated. You \n> don't HAVE to have a start button in the lower\n> left, you can do whatever you want with your\n> panel. :)\n\nHmm, this makes me think that you have never used KDE for more than 5 minutes. To remove the Kmenu, right click on it and click on remove. Heh.\n\n> You can make it floating in the middle of the  > screen and showing only your ICQ applet, while > another Panel hides to the left of the screen > and shows the pager if you want to.\n\nMake a new kicker extention panel, add the applet to it, change the panel size to 1%, and click on resize when necessary, chose the position. Make another kicker extention panel, chose auto-hide, add the pager applet to it, and chose the location and size. There you go. Easy wasn't it. Use KDE more than 5 mins next time.\n\n> And the Toolkit does not even come close to\n> the \"sluggish\" Windows look&feel. ;)\n\nQt 1.44's default X11 theme was quite Window-ish, I agree. Since Qt 2.x, I think gtk 1.2.x's default theme looks more Window-ish.\n\nBesides, that the default KDE2 theme(s) have no similiarty with qt's default theme.\n\n\n\nAnd yes, unlike you, I *do* try GNOME for more than 5 mins every few months to see how it is progressing. I *always* go back to KDE, which I have been using since 2.0, after using GNOME 1.0 and GNOME 1.2 for some time(I didn't like KDE 1.1 much),."
    author: "Jason Roks"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-30
    body: "I don't totally agree with you.  In some respects, yes, KDE is similiar to windows, but in others, it's view and particularly it's widgets (as opposed to MFC stuff ) are different as well.\n\nI also see how you could see differences between Gnome and windows, but honestly, one of the reasons that Sun is supporting Gnome is precisely because of it's similarity to windows - enabling users to have an easier time converting.\n\n( at least that's my impression! :D )\n\nAs it happens, I'm also a GNOME fanatic, if your post has been a normal flame, I would have gleefully joined you (as one person pointed out in these responses - what's the point of cheering for a home team if you have no one to jeer at? ), but since your post seemed to be serious, I felt it worthwhile to reply.\n\n-ron"
    author: "ron"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-30
    body: "I think he was talking about the look&feel of KDE, and besides Kicker it REALLY feels alot like Windows.\n\nAbout Sun using Gnome because it's similar to Windows... nah, not really, Sun used CDE before, so if it's interested in user migration, it would be from CDE to Gnome. Remember that Ximian GNOME has a preconfigured CDE style? I think that has a reason... :)\n\nBesides of that, everything Linux seems to look very much at Windows. \nJust recently I took a look at BeOS and was impressed. Not because it's a serious competition (it's quite dead...) but because it's a SO much better thing to copy than MS Windows.\n\nCompared to Windows, (Debian) GNU/Linux(/[KDE|Gnome|Whatever]) already shines, but compared to BeOS, every Linux based system today just sucks on the desktop. :(\nIt performces poorly and the user interface isn't great either.\nThat's not intendet as a flame, just as a suggestion for everyone to look more closely at BeOS, than on Windows...\n\nBTW, I think it's just what you should expect that KDE behaves like Windows, think of Qt. Qt is designed to be 100% integrated into the Windows desktop. And that shows... sure, it has other styles, but they don't change too much.\nThe Windows style is absolutely perfect, the Motif and Motif+ styles are not. And besides of that, Motif sucks, so it doesn't help either to have a Motif Style.\nIf Qt could come up with a style that looks as clean and non-windowish as Gtk-Raleigh (aka the new Gtk2 default theme) or something similar (in other words, a clean and solid style which is not copied from anything else and does NOT feature fancy stuff like gradients), I would be happy.\nDidn't find something like that yet.\n\nOk, enough of that. :)"
    author: "Spark"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-31
    body: "kde can import 90% of gtk themes fine (pixmap ones). unfortunatly, the only fast gtk themes are the ones that are based on gtk-engines, and comprise the other 10%.\n\nI personally like kde/qt's widget styles better. It just seems more configurable. I've taken a liking to mosfet's liquid kwin and kstyle as of late. There is really no equivalent to this kind of theme in gtk. And the best thing is that most of the bugs have seemed to disappear, especially with the transparent menus. \n\nKicker has a lot of things that gnome-panel doesn't have. And vice versa. I think the ability to make panels floating in gnome-panel is great. On the other hand, I think Kicker's extentions are better, especially kasbar :)."
    author: "fault"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-31
    body: "Keep in mind that kde2's default style is not the same as qt's default style (motif+).\n\nAlso, I looked at a screenshot of gtk-raleigh (http://people.redhat.com/otaylor/gtk/raleigh-ss.png). It looks like a weird combination of macos (the colors, the \"spinners\", radio buttons, frames), and windows (square buttons instead of round buttons like in MacOS platinum).\n\nAs for BeOS, i agree that it was/is great. But, besides the lack of applications, I think the big downfall of BeOS ('s Interface) was that it was not configurable AT ALL (besides simple font changes, etc..). \n\nR.I.P. BeOS"
    author: "fault"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-31
    body: "No, Nada, Incorrect. Sun decided to migrate to GNOME (instead of KDE) from CDE because:\n\n1. GNOME was C\n2. GNOME was lgpl\n\n\nThat was then. Now, there are internal pressures in Sun to quietly switch to KDE, but I won't tell you who I heard that from. ;)"
    author: "Anonymous h4x0r"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-30
    body: "Eh?  Too much like windows... I guess KDE 3.0 will have to stop doing the \"right thing\" when segfaults occur [krash handler] and replace it with a BSOD.\n\nBut seriously.... I use KDE because its easy.  Sure it may look like Windows to some people but the feel is certainly different.  By the way since we are pointing the copycat finger, did anyone notice how much Ximian GNOME looks like OSX's desktop?  Rounded corners and everything.\n\n\nOne really shouldn't point out the speck in another's eye...."
    author: "Dave Leimbach"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-30
    body: "Uh huh.  And Windows was just a Mac clone, and Mac was stole from Xerox, etc.  There is a reason that all of these desktops look and behave similar.  The reason is that it is most efficient and it is what the user's expect.  Much research has been done into designing GUI's and desktops that maximize productivity and usability, and the desktops we see now are the result.  \n\nI myself am not so anti-MS that I would use GNOME just because it looks different than Windows, or at least more so than KDE.  I'll use the desktop that suits my needs the best.  For me right now, KDE2 is that desktop on *NIX systems.  I think that the KDE team has done an excellent job in pumping out new applications and improvements to the desktop.  Although GNOME also is doing good work, if you would have to say that one of the projects is \"winning\", I'd have to say that it is KDE.  Hence this well deserved award.  Congratulations KDE."
    author: "gilligan"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-31
    body: "Gnome and WindowMaker user?\n\nLook at what these projects innovate in.\n\n1. Gnome - cheap ripoff of Windows. Look at the curious similiarty between Evolution and Outlook Express. :D\n\n2. WindowMaker - cheap clone of the beautiful NeXTStep/OpenStep interface. After using NeXTStep for several years (and falling in love with it), I find WindowMaker to always be off a little bit. It never feels like the same. \n\nI agree that KDE takes ideas from Windows and other OS's. At least KDE took them and actually made a *Pleasent to use desktop*.\n\nCongrats on the award! It's nice to see a desktop environment that is NOT controlled by corporations ;)."
    author: "Robin Zarrf"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-08-31
    body: "I have heard numerous complaints about KDE being too similar to Windows.  However, I still have seen no more similarity between KDE and Windows than say, Gnome and Windows.\n\nSo, if anybody knows more about that than me, please enlighten me :-)  I'd be interested in finding out."
    author: "Steve Hunt"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-09-01
    body: "I think the main reason that KDE is compared to Windows is because the UI design is pretty well thought out, easy to use, and well integrated with itself - just like (most of) Windows.\n\nI don't like Microsoft a lot, but I have to hand it to them, the Explorer interface is really *very* good these days.\n\nIn this context, I think KDE being compared to Windows' UI is a real compliment, so I'd like to thank the GNOMEr who suggested it.\n\nPeople who want their desktop to be arcane and difficult to use can look elsewhere..."
    author: "Amazed of London"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-09-03
    body: "I understand that in some cases, it is a compliment to be similar to Windows.  However, here is my general impression when I talk to Linux advocates who are not really desktop-oriented:\n\nKDE is the simple, ease to use desktop that looks a lot like Windows.\n\nGnome is the cool, sexy desktop.\n\nWhich would this be complementing?\n\nI don't think that this thread was really meant to say that KDE has a nice blend of features over several desktops.  Just take a look at the message that started this thread :-)"
    author: "Steve Hunt"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2001-09-03
    body: "> KDE is the simple, ease to use desktop that looks a lot like Windows.\n\n> Gnome is the cool, sexy desktop.\n\n> Which would this be complementing?\n\n*shrug*\n\nFor me at least, KDE is both the desktop that is easy to use and also the cool, sexy desktop.\n\nCool, sexy things that KDE does that GNOME doesn't right now:\n\n- proper anti-aliased text (no hacks)\n- Mosfet's Liquid style (ubersexy!)\n- total network transparency (the audiocd ioslave is the best example)\n- DCOP (this is just the most amazingly cool thing, and you can have SO MUCH fun messing with your users' desktops and their heads if you're root ;)\n- usable and very pretty file manager (Nautilus is nice but totally unusable due to its... how do I put this nicely... lack of speed.. on my home machine whilst Konqueror does more, is more usable and flies on the same machine)\n\nCool, sexy things that GNOME does that KDE doesn't right now:\n\n- umm...  help me out here?  Oh yeah, ok - being able to drag windows from desktop to desktop by moving them against the desktop borders, but that's not really a sexy thing, that's a usability thing, isn't it?\n\nI'm really puzzled as to why some users think GNOME is sexy whilst KDE isn't.  Can anyone shed any light?"
    author: "Amazed of London"
  - subject: "Re: Congratulation but too much like MS Windows"
    date: 2005-10-01
    body: "No bad, but I think that Gnome is the windows 9X desktop clone as its best. It's got my computer, similar look to it and lots of things. Even nautilus acts like explorer in most situations. It's allmost as good as explorer panel to crash too. Yeah you gnome people have done a efforts to make it go away from windows and now you have your eyes in Apple OS9, you have started to clone it. While KDE first was windows look emulator(acctually motif but who cares?) in KDE1 kde developers at least thought to do something by themselfs and kde2 was more like it's own than windows 9x emulator. KDE3 has a lots of its own features which makes it whole another desktop than windows it got nothing similar in the end. KDe is a modular desktop as it can be themed to very end. you can make it look and work like windows if you want or you can make it look and work like OSX if you want. Kde is going to be allways that two steps ahead of gnome people if gnome developers doesn't start thinking features by themselfs.\n\nOkey this is a old thread, but I found it from google and I wanted to reply."
    author: "Antti"
  - subject: "Well done!"
    date: 2001-08-30
    body: "I have been looking forward to this release for some time, and I have also been looking forward to seeing its adoption on some commercial platforms (particularly Tru64 UNIX) as well as the usual Linux systems.\n\nI am very pleased to see how well this work has progressed.  Congratulations to everyone!  Keep up the great work!  Perhaps I will get a chance to contribute to this effort.  I'd like that very much, too.\n\nThe Mas"
    author: "Brian Masinick"
  - subject: "Huge Congrats ! KDE Team you truly deserve it"
    date: 2001-08-30
    body: "A bit boring another post congratulating the KDE team ? \n\nNO ! Not at all, you deserve it all, and for users like myself its from the heart too !! No I am not getting all soppy, but thanks to all on the KDE team for so competently and completely showing the World there is another better way than filling the obscenely fat wallets of Microsucks. \n\nAnyhow I don't want to let this become a political post, so I will sign off with a BIG congratulations, and know how proud we the users are for you and of you, the developers, artists etc...\n\nRegards and Admiration\n\nP Sauter\nChief Web Officer \nThe Operating Room WorldSite\n\n(A Non-profit organisation with a significantly healthier budget for those in need thanks exclusively to KDE/Linux)"
    author: "P J"
  - subject: "IBM got it right..."
    date: 2001-08-31
    body: "I am more and more thinking IBM makes sometime good choices. Whereas Sun and lots of others jumped in a monkey-business (and I really wonder if they named themselves like this on purpose ;-)), IBM was not long ago the only one to choose KDE.\nAnd, it was the good choice since the quality of KDE is quite amazing now. Bravo !\nOn the other monkey side, it looks like A.C. is having a good time figuring out how the mess of a code works."
    author: "Henri"
  - subject: "An additional feature reqd. if not already there"
    date: 2001-08-31
    body: "Public recognition to the KDE suite of wonderful programmes on which so many linux users rely and love.Pl add my congrats. too.\n The only feature I miss is an offline browsing feature on Konqueror unless it is already there and it is I who have missed it."
    author: "adavi"
  - subject: "KuDoS!"
    date: 2001-08-31
    body: "Congrats Guys!!! A great job!       \nKeep it up and take some rest too!!      \nLooking forward for more such awards and , \"Absolute Dominance\" !!!"
    author: "parry"
  - subject: "KDE2.2-Great job done,but..."
    date: 2001-08-31
    body: "Congratulations,boys and girls! As a proud KDE 2.2 user and somewhat translator(i18n-BG) I'm very pleased to see that all the efforts were for good. I'm ABSLUTELY sure KDE is the best desktop in the world(I used to think it's Mac OS X,but no longer). But there is a problem,too. I installed KDE2.2 on several computers,all from source. Unfortunately, virtually all of them experience serious hangs and even crashes. I thought it was because of the buggy NVidia drivers,but nothing changed for good after replacing the videocard. No one could tell me what is all about and I came to the conclusion that it has MORE bugs even than the alpha version! WHY???"
    author: "Stoyan Zalev"
  - subject: "Re: KDE2.2-Great job done,but..."
    date: 2001-09-01
    body: "Because of languages like C++ and C."
    author: "Ada 95"
  - subject: "Nice but too bloated software; slow. .."
    date: 2001-09-06
    body: "I like hte design and features esp konqueror, but I have to quit using kde as it \ntoo slow in my Intel celeron 400Mh 128 Mb memory. Just a desktop environment it \nconsumes a large part of system resourses. It is not worth it.\n\nJust wonder why kde team got this award? \n\nI run GNOME, much faster , albeit not so polished but it is obviously not a dumb \nscreen. \n\nWish kde team improve kde so that after start kde I still got 64Mb Free in Memory like I got it now when running gnome :-)"
    author: "skieu"
  - subject: "Re: Nice but too bloated software; slow. .."
    date: 2001-09-10
    body: "Hey, it's _not_ that slow; but you have _very_ little RAM considering RAM is cheap these days. Upgrade to at least 256MB, but rather more, and you will see that KDE becomes _almost_ as fast as Gnome, and Konqi is much faster than Nautilus. Besides, you'll benefit in all other computing ways too, with a RAM upgrade :-)\n\nIf you don't like RAM; then use Blackbox or Xfce..."
    author: "Per"
  - subject: "Re: Nice but too bloated software; slow. .."
    date: 2001-09-26
    body: "Well I use Celeron-400 and 64 MB RAM and I have to saya that I'm impressed with KDE 2.x and it's not that slow, I think my Win98 SE is slower.\n\nMaybe you need a custom kernel or less daemon ?"
    author: "Irzadi Siregar"
  - subject: "Kudos"
    date: 2001-10-17
    body: "you guys have done a great great job."
    author: "general failure"
  - subject: "Congrats !! "
    date: 2005-12-05
    body: "Congratulations to the KDE-team!"
    author: "infosrama"
---
Moments ago, at <A HREF="http://www.linuxworldexpo.com/">LinuxWorld Expo</A> being hosted in San Francisco, CA, the KDE Project was honored as the winner in the "Best Open Source Project" category.  On hand to accept the Open Source Product Excellence Award on behalf of the KDE development community were KDE developers <A HREF="mailto:charles@kde.org">Charles Samuels</A> and <A HREF="http://cx.capsi.com/personal.html">Rob Kaper</A>, as well as yours truly.  Congratulations to the KDE developers, and thanks to IDG for recognizing the excellence of the KDE desktop!  <STRONG>Update, Thursday August 30, @1:15AM</STRONG>:  Rob Kaper has <A HREF="http://capsi.com/~cap/digicam/2001-08-30-lwce-award/">posted pictures</A> of the award.

<!--break-->
