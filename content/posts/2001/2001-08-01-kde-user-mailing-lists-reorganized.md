---
title: "KDE User Mailing Lists Reorganized"
date:    2001-08-01
authors:
  - "Dre"
slug:    kde-user-mailing-lists-reorganized
comments:
  - subject: "so no support for non-linux users?"
    date: 2001-08-01
    body: "So where does one post non-linux kde/operating system dependent questions?\n\nThis seems a bit silly unless you are officially stating that you will not support any o/s other than Linux."
    author: "mark.rowlands"
  - subject: "Re: so no support for non-linux users?"
    date: 2001-08-01
    body: "See, it's only the Linux users that have the clueless misguided questions like \"how do I install ghostscript?\" or \"how do i compile the kernel for usb?\" on the kde lists.  Figure Solaris and BSD users know better than that or know where to ask those questions."
    author: "ac"
  - subject: "Re: so no support for non-linux users?"
    date: 2001-08-01
    body: "Yeah, I considered that option, but having had some non-trivial bsd/kde issues in the past it would be nice to know whether there is any intention on the part of  the kde team to abandon all support for non-linux os'. \n\nAhh well\n\nthere's always Gnome\n\nhttp://mail.gnome.org/archives/gnome-1.4-list/2001-February/msg00074.html"
    author: "mark.rowlands"
  - subject: "Re: so no support for non-linux users?"
    date: 2001-08-01
    body: "I'm sorry you missed this fine point:  it's not about the KDE developers abandoning support for any OS, it's about KDE users providing each other with support.  KDE itself supports a broad array of Unixes, a fairly complete list is available at <a href=\"http://promo.kde.org/kde_systems.php\">http://promo.kde.org/kde_systems.php</a>.  If BSD users want a list to support each other, or Windows users using KDE 1.1.2 on CygWin want a list to support each other, I am happy to create one for them.  If you are a non-Linux user and don't mind Linux questions, subscribe to both lists.  If Linux questions really annoy you, you have the choice now not to be annoyed; if they don't, sign up for both lists.\n<br><br>\nPlease try to see this for what it is:  providing users a forum to help each other."
    author: "Dre"
  - subject: "Re: so no support for non-linux users?"
    date: 2001-08-01
    body: "Perhaps the kde list should be kde-any-os?\n- Alan"
    author: "Alan"
  - subject: "Re: so no support for non-linux users?"
    date: 2001-08-01
    body: "1) This has nothing to do with development.\n\n2) If there were a plot to get rid of non-Linux support, what's the point of creating a general list and a Linux-specific one? The whole idea is to help out non-Linux users by sparing them questions about getting scanners to work under Red Hat.\n\n3) The jibe about Gnome might have carried some bite in February. But since we now know how well the 1.4 release ran on Solaris (real Gnome, not the Ximian packages that came out months later), it falls a little flat."
    author: "Otter"
  - subject: "Re: so no support for non-linux users?"
    date: 2001-08-01
    body: "The fact is that Linux-only questions are prevalent and Solaris-only questions are not.  All this does is give an opportunity for non-Linux users to avoid Linux questions, and for the list admin (me) not to have to keep telling people they are off-topic when they have a question which for them seems quite on-topic.\n\nIf there is enough demand -- say 8 or more people tell me (pour at kde.org) that they will subscribe to a kde-solaris list, and one of them volunteers to be the list admin -- I will happily create the list.  Obviously finding 8 interested Linux users is not a problem, we already have 13 subscribers and the list is but an hour old.\n\nAlternatively, if the non-Linux users want a combined list (which doesn't make much sense to me), I will create that as well, I just need to see a real desire for it before creating lists."
    author: "Dre"
  - subject: "Re: so no support for non-linux users?"
    date: 2001-08-01
    body: "Dre,\nleaving alone the \"waaaa, my OS ain't supported\", assumed there is a question that is tied to, say solaris, where do I post them?\nThis is not about \"is there enough demand to create a specific list\", but where do posts go for OS's that don't have enough demand.\nWill \"kde\" subscribers happily deal with a few solaris or a irix (yac) related messages a month?\n\nAxel"
    author: "Pike"
  - subject: "Re: so no support for non-linux users?"
    date: 2001-08-01
    body: "> assumed there is a question that is tied to, say\n> solaris, where do I post them?\n\nI don't know, perhaps the same place that you have asked Solaris questions in the past?\n\n\n> Will \"kde\" subscribers happily deal with a few\n> solaris or a irix (yac) related messages a month?\n\nI don't know, that community has yet to form.  As the initial rules are no, if enforcement is demanded by a significant part of the community, then it probably would be a good idea to create a list for the people who wanted to discuss those issues.  But I don't see the point in dicussing parades of horribles.  There is nothing stopping KDE users with additional common interests from associating freely.  If any group wants to associate on a KDE-related topic, KDE has created a mailing list for it.  Just browse http://master.kde.org/mailman/listinfo to see what I am talking about -- there's even a kde-soccer list."
    author: "Dre"
  - subject: "Re: so no support for non-linux users?"
    date: 2001-08-01
    body: ">> assumed there is a question that is tied to, say\n>> solaris, where do I post them?\n\n> I don't know, perhaps the same place that you have asked Solaris questions in the past?\n\nI think you are being deliberately obtuse here, why not just answer the question?\n\n> Operating system specific questions and\n> discussion are strictly off-topic on this\n> (new kde) list. \n\n> The other new mailing list, kde-linux, will\n> focus on problems facing a desktop user \n> running KDE on a Linux distribution."
    author: "mark.rowlands"
  - subject: "Well..."
    date: 2001-08-01
    body: "...I just doubt whether the average user will know when to post in which mailing list. And in the end everyone will post anything to kde-linux and the Solaris etc. users are left in kde...."
    author: "AC"
  - subject: "Re: Well..."
    date: 2001-08-01
    body: "That's the current situation on the mailing lists, that all users regardless of OS receive Linux-only questions?  The *nix users can join kde-linux as well, they just can't complain about Linux questions there.  At least now they have a choice."
    author: "Dre"
  - subject: "Re: Well..."
    date: 2001-08-01
    body: "As I'm no subscriber of any of the KDE lists (I would like to, but hey, I simply costs too much time, I would always be tempted to post something), I'm in a bad postion to complain ;) .\n\nI just think that time will bring more and more \"newbie\" users to KDE who just won't know whether their question is KDE- or more OS-related."
    author: "AC"
  - subject: "What about a kde-nonlinux list?"
    date: 2001-08-01
    body: "The point about list usage is fair enough, but you do need somewhere to collect all the OS specific stuff that isn't Linux. Why not create a group called kde-unix or kde-nonlinux to collect these extra mails lying around. The usage will fall far short of the kde or kde-linux groups, but I'd imagine there would be a steady tickle of traffic nonetheless. KDE is, afterall, meant to be a multi-platform UI.\n\nJust my \u00800.02"
    author: "Bryan Feeney"
  - subject: "Re: What about a kde-nonlinux list?"
    date: 2001-08-01
    body: "The idea of kde-nonlinux seems fair, and if  we can get the contributors to keep to the convention of adding their OS class in the subject line that is even better.\n\nAt least I will then have somewhere to send my solaris questions.\n\nThanks\nCPH"
    author: "CPH"
  - subject: "Re: What about a kde-nonlinux list?"
    date: 2001-08-01
    body: "> Why not create a group called kde-unix or \n> kde-nonlinux to collect these extra mails lying > around.\n\nBecause all non-Linux Unices are not alike, and I would not want to be viewed as treating them that way.  The point of having a kde-linux list is there undisputably is great deal of commonality in how you solve problems, whether you use distro X or Y.  It is also clear that user questions on the lists tended to be rather heavily Linux-oriented.\n\nThat said, if people want another list, I am more than happy to create it.  There is no list rationing, other than to provide only lists that will in fact be used.   People just have to tell me what list they would like and I will create it.  I know there was a demand for linux support, nobody had to tell me about it because I see it on the mailing lists every day."
    author: "Dre"
  - subject: "Re: What about a kde-nonlinux list?"
    date: 2001-08-01
    body: "I'm happy to own a kde-openserver list. I get quite a few questions on it as is and would like to know the demand for a port of KDE 2..."
    author: "Donald"
  - subject: "Re: What about a kde-nonlinux list?"
    date: 2001-08-08
    body: "I'd prefer it to be named kde-misc. Consider that in the near future you will find that a kde-freebsd or a kde-solaris list is also necessary. Then, kde-nonlinux would be a misnomer.\n\nSo, if your os doesn't have a kde-osname list, post to kde-misc. OS independent questions to the master kde list.\n\nMakes sense."
    author: "KDE User on FreeBSD"
  - subject: "Will non linux messages be posted to both?"
    date: 2001-08-01
    body: "You say \"Any question appropriate for kde is also appropriate for kde-linux, but not vice-versa\" . Does this mean that non linux specific messages posted to kde-linux will always be cross posted to kde? If not then as a Solaris user I will have to subscribe to kde-linux to see those messages, thus losing the point of having a separate linux list."
    author: "Steve Evans"
  - subject: "New List Proposal"
    date: 2001-08-01
    body: "So you catered to the Linux lusers.  Now I propose a new list from information carefully gathered in this forum:\n\nkde-why-kde-linux-list-and-not-a-list-for-other-os@kde.org\n\nThis list will have many subscribers with the important question of discussing the rationale behind kde-linux, and why KDE does not support other OSes.\n</satire>"
    author: "ac"
  - subject: "kde-nonlinux@kde.org"
    date: 2001-08-02
    body: "Hi,\n\nI justed added kde-nonlinux@kde.org for general\nquestions related to running KDE on operating systems other than Linux.\n\nTo subscribe send mail to kde-nonlinux-request@kde.org.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: kde-nonlinux@kde.org"
    date: 2001-08-03
    body: "But what if the need arises for a kde-solaris@kde.org list, and it gets created, then kde-nonlinux would have to be renamed kde-nonlinuxorsolaris@kde.org!  \n\nI feel that this is very important and needs to be addressed promptly, because the alternative would be anarchy and chaos.  We will probably see an increased drug use amongst disillusioned users of KDE on HP/UX."
    author: "Steve"
  - subject: "If the idea is that Linux users are lusers"
    date: 2001-08-04
    body: "(this has been somewhat implied in the posts to the lists and on this forum) then the \"default\" list should be the one intended for Linux users.  As a self-confessed luser (albeit a FreeBSD luser) myself, I will always try to choose the most appropriate list for my questions, but frequently simply do not have the knowledge of which is the appropriate list, and will therefore post to the most general list possible.  If I were using Linux, I would almost surely post (or cross-post) to the _kde_ list, because I don't know whether my problem is related to Linux or KDE.\n\nI think it would be better to operate from the principle that *non-Linux* users are more readily able to judge whether their problems are due to their OS rather than KDE itself.  Additionally, I'd much rather have a list purely for FreeBSD than having to wade (okay, \"wade\" may be an exaggeration) through the Solaris-related issues.  So why not have a _kde_ list for basic (assumed to be Linux) questions, a _kde-core_ (or something) list for issues known to be related to KDE itself, and lists like _kde-bsd_ and _kde-solaris_ for \"fringe\" systems?"
    author: "bjrubble"
---
The two venerable KDE mailing lists, <EM>kde</EM> and <EM>kde-user</EM>, both hosted at <A HREF="http://lists.netcentral.net/">lists.netcentral.net</A>, are being phased out in favor of two new mailing lists, <EM>kde</EM> and <EM>kde-linux</EM>, hosted at the KDE.org domain.  Current subscribers to the existing mailing lists will need to subscribe to the new mailing lists.  Administration of the old mailing lists will cease August 15 and they will be shut down altogether on August 31.  A big shout-out to NetCentral for providing excellent hosting services to KDE users for many years! <b>Update: 08/02 5:25 AM</b> by <a href="mailto:navindra@kde.org">N</a>: By popular demand, a <a href="http://master.kde.org/mailman/listinfo/kde-nonlinux">kde-nonlinux</a> list has been created as well.
<!--break-->
<P>
For several years now I have served as the list admin for the <EM>kde</EM>
and <EM>kde-user</EM> mailing lists, the two "user-land" KDE mailing lists.  The lists
were created long before I became list admin.  In the interim, a lot has
changed with KDE and the people subscribed to the lists.  Moreover, the
lists are the only large KDE lists to be hosted outside the kde.org domain
(they have been hosted at
<A HREF="http://lists.netcentral.net/">lists.netcentral.net</A>).  While I
have nothing but the greatest appreciation for NetCentral's contributions and
support of KDE (thanks!), KDE has outgrown the mailing list bot software installed
on that system, and we are unable to upgrade it.  Accordingly, we are
scrapping the current kde and kde-user mailing lists at lists.netcentral.net
in favor of two new mailing lists,
<A HREF="http://master.kde.org/mailman/listinfo/kde">kde</A> and
<A HREF="http://master.kde.org/mailman/listinfo/kde-linux">kde-linux</A>,
each hosted at <A HREF="http://master.kde.org/mailman/listinfo">master.kde.org's mailman interface</A>.
</P>
<P>
The new mailing list <A HREF="http://master.kde.org/mailman/listinfo/kde">kde</A>
will focus specifically on operating-system independent questions and
discussions regarding KDE and third-party KDE software.  Operating-system
independent means the questions and discussions will pertain to each KDE user
using the applicable KDE software, whether using FreeBSD, HPUX, Linux or
Solaris.  Operating system specific questions and discussion are strictly
off-topic on this list.
</P>
<P>
The other new mailing list,
<A HREF="http://master.kde.org/mailman/listinfo/kde-linux">kde-linux</A>,
will focus on problems facing a desktop user running KDE on a Linux
distribution.  Any question appropriate for <EM>kde</EM> is also
appropriate for <EM>kde-linux</EM>, but not <EM>vice-versa</EM>; the
latter is a superset of the former.
</P>
<P>
What, by way of example, is the difference?  For instance, the question
"<EM>How do I change the margin size for KWord documents</EM>" is
appropriate for the <EM>kde</EM> list, as this has to do with KWord's
user interface and thus applies to users of all OSs.
On the other hand, the question
"How do I set up my printer using KDE" is only appropriate for the
OS-dependent <EM>kde-linux</EM> list, as it involves system configuration
of a printer and possible installation of device drivers.
If you don't understand the difference, and you use KDE with
Linux, please subscribe to <EM>kde-linux</EM>.
</P>
<P>
In my time as service as list admin for <EM>kde</EM> and <EM>kde-user</EM>, my observation has been that OS-dependant questions very heavily, if not exclusively, are centered on
Linux.  For this reason the only OS-dependent list created at this juncture is <EM>kde-linux</EM>.  Of course, if there is sufficient demand, mailing lists for other
operating systems will also be created (such as <EM>kde-bsd</EM> or <EM>kde-solaris</EM>).
But please don't request a list just to be "fair" -- there should be a real
need for the list, as an inactive list is just clutter.
</P>
<P>
Warm regards,
</P>
Andreas "Dre" Pour<BR>
KDE/KDE-User Mailing List Admin