---
title: "$200 Brazillian Net PC to run Linux and KDE"
date:    2001-02-02
authors:
  - "jcunha"
slug:    200-brazillian-net-pc-run-linux-and-kde
comments:
  - subject: "Re: $200 Brazillian Net PC to run Linux and KDE"
    date: 2001-02-02
    body: "This is kewl... This will get those countries who can't affort to support Dictator Bill on the 'net  too. Let's hope that more countries will follow this example, so their opinions get heard too. Prehaps they can teach the \"money-corps\" that its perfectly possible to use KDE/Linux as an end user solution, and not just as a webserver! <BR><BR>/kidcat"
    author: "kidcat"
  - subject: "Re: $200 Brazillian Net PC to run Linux and KDE"
    date: 2001-02-02
    body: "For those of u who don't wanna w8 half an hour to c the img of the baby... here it is :)<BR><BR>/kidcat"
    author: "kidcat"
  - subject: "WARNING!!! GROSS ATTACHMENT!"
    date: 2001-02-02
    body: "DON'T CLICK THAT ATTACHMENT!! IT'S GROSS!!!!"
    author: "warning"
  - subject: "Re: WARNING!!! GROSS ATTACHMENT!"
    date: 2001-02-02
    body: "WTF?\n\nClick on it, its anice little pc. Gross?"
    author: "me"
  - subject: "Re: $200 Brazillian Net PC to run Linux and KDE"
    date: 2001-02-02
    body: "Thanks. :)\n\nI wonder how they got KDE to fit in this computer without a harddrive!"
    author: "KDE User"
  - subject: "Re: $200 Brazillian Net PC to run Linux and KDE"
    date: 2001-02-02
    body: "Linux + KDE in 16Mo ?<br>\n<br>\n\nWhere can I find the <b>Linux+KDE_in_16Mo_HowTo.txt</b> ?\n\n:)))"
    author: "Shift"
  - subject: "16 Mb"
    date: 2001-02-03
    body: "Perhaps with cramfs.  Supposedly you can get Linux + Xfree86 in 8 Mb with cramfs, and cramfs is designed specifically for Flash memory."
    author: "not me"
  - subject: "Re: $200 Brazillian Net PC to run Linux and KDE"
    date: 2001-02-02
    body: "I'm really happy about this announcement, and although I wonder how KDE & Linux & X fit on 16MB, I think that this might make a very usable machine.\n\nThe only thing I'd be worried about is the current state of KWord. In my opinion, its not quite ready for prime time, at least in terms of stability. I am personally looking forward to the new KWord, with much improved architecture and hope it'll find its way into those boxes soon.\n\nLet's hope, we might even gain some developers from this!"
    author: "ben"
  - subject: "Re: $200 Brazillian Net PC to run Linux and KDE"
    date: 2001-02-03
    body: "What new and improved KWord? I update koffice from CVS quite frequently and I have seen very few changes taking place.  Is there a different branch or something?\n\nMatt Newell"
    author: "Matt Newell"
  - subject: "KOffice not usable"
    date: 2001-02-03
    body: "I agree with you. While I think that KDE2.1 is \na great desktop, KOffice is unusable.\nIf I recall correctly, this is also the Opinion\nof the  developers.\nHopefully this situation will improve (TheKompany is working on at least KWord) also because of the\nmillions brazilians which will now start to code and send in bugreports ;-)."
    author: "ac"
  - subject: "Re: KOffice not usable"
    date: 2001-02-04
    body: "Not entirely true. Killustrator is a nice piece of software, which I actually use quite often."
    author: "yngve"
  - subject: "Re: KOffice not usable"
    date: 2001-02-07
    body: "I don't think there will millions of coders from Brazil ... From what I have gathered the braz. govt. wants these computers to be some kind of net /office appliance - that's why you don't have a HD and Linux/KDE is stored on flash memory. It's a preconfigured comp. with no messing around with tweakings and configuration files."
    author: "Delcides F. Sousa Jr."
  - subject: "Re: $200 Brazillian Net PC to run Linux and KDE"
    date: 2001-02-02
    body: "How much space does the system take up on this 16MB flash card?  Is there any space left for saving documents etc.<br>\nAnd where's the mouse?  The picture didn't show any, surely there must be a mouse with the pc."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: $200 Brazillian Net PC to run Linux and KDE"
    date: 2001-02-04
    body: "Did you folks see the article on Brazil and AIDS treatment in last Sunday's New York Times Magazine?\n\nThe Brazillian govt. decided that the drug companies' pricing of AIDS medication was immoral, and patents be damned, they went ahead and produced cheap generics which are having a huge impact.\n\nThis story reminds me of that.  Nice to see a 'third world' government take active steps to improve the situation of their people - although I gather that situation needs a LOT of improving.\n\nBy the way, on the 'Linux/KDE in 16MB' question, one of the links mentions 64MB.  From my experience, though, I wonder whether 64MB is even enough to comfortably run KDE2.  It runs great on machines with 128MB, but my home machine has 64, and it feels sluggish."
    author: "Rob"
  - subject: "Re: $200 Brazillian Net PC to run Linux and KDE"
    date: 2001-02-04
    body: "Well, my anemic home PC has 48 MB of RAM and a 133 MHz pentium and it runs kde \"okay\" - I guess it is a function of what you are used to (I could probably scream through my browsing with one of those brazilian boxes!).\n\nMartin"
    author: "Martin Andrews"
---
As announced by Pimenta da Veiga, minister of
communications of Brazil, in the next 120 days, a Linux-based computer will be available on the Brazilian market for US$ 200. The computer configuration is a 500 Mhz AMD processor, 64 Mb RAM, 16 Mb flash disk (no hard disk included), 14 inch monitor, sound, 56 Kbps modem, ethernet, printer and USB ports. The software is Linux, KDE, KOffice, Konqueror, and all other technical specifications are public domain. The is not taxed by the Brazilian government, and it will be sold in 24 monthly payments of US$ 13. <i>[<b>Ed:</b>Also, from <a href="http://slashdot.org/article.pl?sid=01/02/02/0635239&mode=thread&threshold=0">Slashdot</a>: The Brazilian government notice is <a href="http://www.mct.gov.br/sobre/noticias/2001/31_01.htm">available</a>, as are <a href="http://www.pontobr.org/noticia.php3?nid=1764">pictures</a> of the device. Nice.]</i>

<!--break-->
