---
title: "KOffice 1.1 Rolls Out"
date:    2001-08-29
authors:
  - "Dre"
slug:    koffice-11-rolls-out
comments:
  - subject: "good work!"
    date: 2001-08-29
    body: "Congratulations to KOfficers ! \n\nSince KOffice is now stable, the next priority should be to get filters working. For real world usage you really need .doc support."
    author: "hackorama"
  - subject: "Re: good work!"
    date: 2001-08-29
    body: "Actually, getting someone to work on Krayon ought to at least be worth thinking about (no, I can't, I have almost no skill in C++ whatsoever)\n\nIt is unlikely any open source projects will be able to properly read MS Office formats: they're complicated, and use Windows-native technologies like OLE2. \n\nInstead, the focus should be making KOffice more functional, faster & more stable."
    author: "Rk"
  - subject: "Re: good work!"
    date: 2001-08-29
    body: "Won't help me much, all my files at home are WordPerfect (5.1 & 6.x formats) documents (plus various QuattroPro files).  At work it's SmartSuite (although I don't expect to need to convert those anytime soon)."
    author: "James E. LaBarre"
  - subject: "Re: good work!"
    date: 2001-08-29
    body: "Yep...I agree, filetes are a must...how about .lwp support? (ya know, lotus word pro?)"
    author: "Fred Thiele"
  - subject: "Re: good work!"
    date: 2001-08-29
    body: "I think focusing on filters is important but\npremature. Once e.g. KWord gets feature\nparity with Word (XP or whatever then current\nversion would be), then it makes sense to try to\ntranslate to and from Word format. At this\npoint you can't recover all formatting\nproperly anyways. For instance, without\nhyphenation even plain text Word document will\nnot have proper formatting when imported into\nKWord."
    author: "All_troll_no_tech"
  - subject: "Re: good work!"
    date: 2001-08-30
    body: "For real world usage we need a windows version (also free, at least as in beer). This way, windows users will at least take a look at it (since it costs nothing), and if it's good, maybe they'll forget about M$ Office."
    author: "Rui Prior"
  - subject: "Nice"
    date: 2001-08-29
    body: "I am just now compiling/installing KDE 2.2, so maybe I'll grab this while I'm at it :)\n\nOT: has anyone had any trouble using objprelink with KDE2.2 final?  The patch worked fine for kdelibs, but when I patch kdebase it causes autoconf to hang on an infinite recursion when I run 'make'.  Maybe it's just my version of autoconf.  I'm running Slackware 8.0.  I would really like to get this to work!"
    author: "Justin"
  - subject: "Re: Nice"
    date: 2001-08-29
    body: "qt kdelibs and kdebase all compiled perfectly while using objprelink on my debian unstable box. Limit the level of recursion to 500 and see if that helps. I want to try kde without objprelink; once the app has fired up, it seems to run slower.\n\nOT: can any1 explain --enable-final?"
    author: "eze"
  - subject: "Re: Nice"
    date: 2001-08-29
    body: "--enable-final is just for optimisation.\n\nAll sources files are appended in one big source file, and then compiled. It allows some bette opt. but requires a lot of RAM to compile."
    author: "aegir"
  - subject: "Re: Nice"
    date: 2001-08-29
    body: "Actually the main interest is not better optimisation but (much) better compile times."
    author: "Guillaume Laurent"
  - subject: "Re: Nice"
    date: 2001-08-29
    body: "This is no joke either. First time I tried it, the compile finished so quickly I thought something was busted.. nope.. worked fine.  :)\n\nIt doesn't always though, some projects can't be built with --enable-final."
    author: "jd"
  - subject: "Re: Nice"
    date: 2001-08-31
    body: "How much memory do you have?\nI have not tried it as it warns that you need a lot of memory."
    author: "Jim"
  - subject: "Re: Nice"
    date: 2001-08-29
    body: "You have to return autoconf form pasture in slack 8, the new one is not really compatible."
    author: "Flaker"
  - subject: "Re: Nice"
    date: 2001-08-29
    body: "On the subject of objprelink, has anybody managed to run it succesfully on RH6.2(with compiler updated to gcc-2.95.3 of course)?  Do I need to update other packages(eg binutils) also?  Does anyone else still run RH6.2? :-)\n\nNow, off to compile koffice.  :-)"
    author: "Rob"
  - subject: "Re: Nice"
    date: 2001-08-30
    body: "- Use a pre 2.50 autoconf \nAND/OR \n- Rude hack!\nmove m4 to gm4 and then make an 'm4' shell script: /usr/bin/gm4 -L2000 $@\n\nThere's a way to set the M4 recursion with an environment variable, but I could not quickly find documentation on it.\n\nI did notice that most/all of the packages take more time to compile using autoconf 2.50"
    author: "Beehive"
  - subject: "REDHAT RPM"
    date: 2001-08-29
    body: "why the announcement is so late? Its  rpm became available many days ago an rpmfind.net."
    author: "vk"
  - subject: "Re: REDHAT RPM"
    date: 2001-08-29
    body: "You didn't read the first sentence of the news, didn't you?"
    author: "someone"
  - subject: "Re: REDHAT RPM"
    date: 2001-08-29
    body: "First distribute to mirrors, then announce. Otherwise the original site will be slashdotted, as has happened previously."
    author: "reihal"
  - subject: "Word view"
    date: 2001-08-29
    body: "Wouldn't it be possible to view word documents using Microsofts own viewer inside the new ActiveX compatible KDE browser? \n\nI would be really glad if I could just view and print office document more or less the way they are supposed to look. \n\nThe ability copy text and pictures from this word-viewer would also be very handy, but if you want to make a wordprocessor that can load and save word documents without screwing them up I guess that this program has to be stupid in exactly the same way as Word is. And THAT should really not be the goal of the KOffice project."
    author: "Johan"
  - subject: "Re: Word view"
    date: 2001-08-29
    body: "Take a look at this ./ article:\n\nhttp://slashdot.org/articles/01/08/28/1552247.shtml\n\nIt talk about the ability to play Quicktime trailers and view Word documents.\n\nJohan V."
    author: "Johan Veenstra"
  - subject: "Well done people :-)"
    date: 2001-08-29
    body: "I would like to say a congratulations to the KOffice team. They have toiled hard over a hot computer to bring this to the world, and it gets better and better. The future is bright. :-)"
    author: "Jono"
  - subject: "Re: Well done people :-)"
    date: 2001-08-29
    body: "They all toiled over the same computer? Wow."
    author: "Anomalous Coward"
  - subject: "FrontPage like tool and GNOME support needed."
    date: 2001-08-29
    body: "Well done guys. But you guys still need one more app, a FrontPage or Netscape Composer like tool. Sure, sure, I know HTML, but most of the world doesn't. Plus, kOffice should release a version with a GTK frontend for GNOME. After all, if kOffice wants more market share, it has to look to the competitior. Hey, Microsoft ports its own Office suite to competitor Apple."
    author: "Rajan Rishyakaran"
  - subject: "Oh and Windows Mac versions too."
    date: 2001-08-29
    body: "Since KDE is based on QT, and QT is available on Windows and Mac, port kdelibs and Koffice to windows and mac. Plus kOffice could do with a little comestics to make it look purely native on Windows and Mac. Maybe one day, KOffice might exceed MS Works and Apple Works (MS Office is just a little too far, okay?)"
    author: "Rajan Rishyakaran"
  - subject: "Re: Oh and Windows Mac versions too."
    date: 2001-08-29
    body: "> Since KDE is based on QT, and QT is available on \n> Windows and Mac, port kdelibs and Koffice to\n> windows and mac.\n\nI got interested in that. Is there any plans for porting KOffice?\n\nJuha"
    author: "Juha Manninen"
  - subject: "Re: Oh and Windows Mac versions too."
    date: 2001-08-31
    body: "Before even thinking about porting to other platforms, one will have to make a KDE version witch is not X dependent. After that you could probably port KDE with little effort, probably also compile a version for the Linux frame buffer witch world be great.. :) But for me at least, this is not a big deal, I like freeBSD :)"
    author: "J\u00f8rgen"
  - subject: "Re: Oh and Windows Mac versions too."
    date: 2003-05-11
    body: "hi"
    author: "allam"
  - subject: "Re: FrontPage like tool and GNOME support needed."
    date: 2001-08-29
    body: "I agree, That would be somthing that would be really usefull, as far as I can tell there is no front page alternative for Linux. or even somthing similar to the homestead (homestead.com)site builder. now that is a easy and powerfull site builder!"
    author: "L.D."
  - subject: "Re: FrontPage like tool and GNOME support needed."
    date: 2002-04-01
    body: "I used to use Homestead.com for building sites when I used windows. They're service is unbeetable because it has a superior sitebuilder/editer that doesn't require html knowledge- unless you're not using MS windows. I wish they would port to linux- or I wish there was an inway on our end so us linux users can use homestead. My son's site was built with it- http://tekkenray.homestead.com/home.html which can no longer be edited because we no longer do windows of which I don't regret even a little bit."
    author: "Mary"
  - subject: "Re: FrontPage like tool and GNOME support needed."
    date: 2001-08-29
    body: "We don't need a new Frontpage tool. What we need is a WYSIWYG web editor, that makes *standards compliant code*."
    author: "Joergen Ramskov"
  - subject: "Re: FrontPage like tool and GNOME support needed."
    date: 2001-08-29
    body: "BTW the easiest way to have a such tool is probably to write an HTML import/export filter for KWord, no ?"
    author: "aegir"
  - subject: "Re: FrontPage like tool and GNOME support needed."
    date: 2001-08-29
    body: "good idea! which is why someone wrote one ;-)"
    author: "Aaron J. Seigo"
  - subject: "Re: FrontPage like tool and GNOME support needed."
    date: 2001-08-30
    body: "Well, I should have said a *GOOD* HTML filter :-)\n\nBTW, the available filter just import datas from HTML files(what is very usefull),  but not tables, images and so."
    author: "aegir"
  - subject: "Re: FrontPage like tool and GNOME support needed."
    date: 2001-08-29
    body: "You sure know Quanta+ ???? \n\ngreat kde Web-editor , like Frontpage or netscape composer , or even better  i dont know , i use it as picture viewer :--))))"
    author: "chris"
  - subject: "Re: FrontPage like tool and GNOME support needed."
    date: 2001-08-29
    body: "quanta is buggy.\ni use it for my personal use and in training courses. its very naice but has some very silly bugs.\nand the worst: it is not maintained for a while.\nso - comparing frontpage with quanta is a joke (remember i am not a windows user, althoungh uses dreamweaver a lot and tought frontpage at class)\n\ngunnar."
    author: "gunnar"
  - subject: "Quanta Plus, Kate and KWord"
    date: 2001-08-29
    body: "> quanta is buggy.\n\nYes and no. If you use it without the notion of project, page per page, it is good... excepted that it is impossible to manage the toolbars... As for Kate and some other KDE programs, it is very irritating to be enable to save the position of toolbars... I hope it will be fixed in KDE 2.2.1 or 3.0...\n\n> and the worst: it is not maintained for a while.\n\nYes, it is the worst. Is it a good choice when it seems not maintened ?... No evolution... It's a pity to see such a great project going to death... perhaps... I hope no...\n\n> so - comparing frontpage with quanta is a joke\n\nHmm, I feel Quanta Plus better than Frontpage... Any text editor is better than Front page, and Kate seems very good. Add to Kate a Khtml preview, and it replaces Quanta, for me...\n\nI think that Kde don't need a FrontPage like. It would be done by KWord and a good Html export. For big sites, you need to know Html, so you may work in an advanced editor like Quanta... Imho..."
    author: "Alain"
  - subject: "Re: Quanta Plus, Kate and KWord"
    date: 2001-08-30
    body: "> Yes, it is the worst. Is it a good choice when it \n>seems not maintened ?... No evolution... It's a \n>pity to see such a great project going to death... \n>perhaps... I hope no...\n\nthis must be good news then:\nhttp://kdewebcvs.nebsllc.com/cgi-bin/cvsweb.cgi/quanta/quanta/\nshows recent cvs activity ('fix crush' five days ago)"
    author: "ik"
  - subject: "Re: Quanta Plus, Kate and KWord"
    date: 2001-08-31
    body: "we ll see quanta as Hancom WebBuilder 2.0..."
    author: "gunnar"
  - subject: "Re: FrontPage like tool and GNOME support needed."
    date: 2001-08-29
    body: "Noooo! FrontPage is evil!\n\nSeriously though, emacsen do well as html editors, but there's a serious need for a WYSIWYG editor and especially site maintenance tools a la Dreamweaver.\n\nAs it is now I've had to stoop to using Windows at work because I can't find serious alternatives to DW..."
    author: "Tony A. Emond"
  - subject: "Re: FrontPage like tool and GNOME support needed."
    date: 2001-08-29
    body: "Well, FrontPage XP supprised me a bit. The code is actually very clean, clearly indented and all. Compliance I don't know about, but its definetly a step forward."
    author: "mg"
  - subject: "Re: FrontPage like tool -> Kafka"
    date: 2001-08-29
    body: "Do you know kafka?\nhttp://www.asci.fr/kde/kafka/"
    author: "gorbeia"
  - subject: "I hope you are joking \"GTK frontend for Gnome\""
    date: 2001-08-29
    body: "dont hold your breath for a gnome/gtk Koffice (notice the K there).\n\nI think if you read about Kparts vs bonobo, etc. (the diferences are many) you will see that Windows, maybe *could* be possible (there is more to KDE than just QT), but Gnome... naganahappen.\n\nps. frontpage = vomit. i like the idea of html export filter for kword, though."
    author: "Pablo Liska"
  - subject: "Re: I hope you are joking \"GTK frontend for Gnome\""
    date: 2001-08-30
    body: "i think it should be technically possible to build a bonobo - koffice parts bridge, just like we saw a mozilla bridge (which ain't complete yet, but a nice proof of concept)\nhowever, somebody has to find it important enough to work on it ..."
    author: "ik"
  - subject: "Re: FrontPage like tool and GNOME support needed."
    date: 2001-08-30
    body: "You can run KDE programs in GNOME just fine..."
    author: "dc"
  - subject: "keep going!"
    date: 2001-08-29
    body: "After reading the posts on this side, i feel it\u00b4s necessary to point out, that the work the KOffice developpers have done is greatly appreciated by the users! \nThe fact that some complain about missing filter functionality should not offend anyone who has contributed to this great work. Especially because the filters are at the moment much better than one could expect from the comments above (I\u00b4ve tried cvs-checkout a week (or so) ago and I could import word documents with german umlauts, tables etc. without any problems into kword). \n\nOnce again: Congratulation\n\nWolfgang"
    author: "Wolfgang Kesseler"
  - subject: "I second this.."
    date: 2001-08-29
    body: "Yeah, I'd like to second this.\n\nReading the posts for both the KDE 2.2 and KOffice 1.1 announcements can be a little depressing.  The fact remains that KDE and its surrounding projects are very high-quaility products.\n\nI develop on Windows all day long, and when doing web development I long for something as complete and fun to use as Quanta+.  KDevelop looks great, and looks like it might be a good competitor for Visual Studio (which I use daily and quite like.).  There's simply no denying that the default KDE install is far more feature-rich and useful than a bare Windows install.\n\nThink about it, Notepad, Paint, no Zip utility, no PDF viewer..  KDE is very nice.\n\nI use Konqi for about 90% of my browsing, and I almost _never_ see any problems whatsoever.  KHTML must be well designed, as Kurt chose this rather than Gecko as his HTML rendering engine in AtheOS.  \n\nThe overall design and care that goes into the various KDE projects is astounding.  Keep up the great work.  If it wasn't for KDE, Desktop Unix/Linux wouldn't be where it is today.\n\nThanks a lot.  I look forward to using KOffice, and am a very happy KDE user."
    author: "Ben Hall"
  - subject: "What about krayon?"
    date: 2001-08-29
    body: "It seems like krayon devel is going very slow, is this subproject still live?\n\nWhen krayon could compete the another project's gimp?\n\nShame on krayon, this project is started 4 years ago and it's still:\n\n> in the early stages of development but is\n> already usable for experimenting with painting\n> techniques and composing images using brushes\n> and layers.\n\nAlso, ...! somebody knows something about already dead koffice subproject, kdatabase (or something like that (kdb, katabase)) that was planned to be ms access competitor in koffice?\n\nAnyway some of other apps really rocks - kword, kspread, kpresenter, kontour. It's very hopefull release, as soon as you imlement more filters you will compete staroffice too :)\n\nAlso KDE2.2 is really the best DE!\n\nKeep on  good work!"
    author: "kde user"
  - subject: "Re: What about krayon?"
    date: 2001-08-29
    body: "Krayon developpment is slow, but project is still alive.\n\nThis month fuctions for loading/saving multi-layers pictures, and printing functions has been added.\n\nAbout Katabase, IMHO the project will resurrect with KDE 3 because Qt 3 provides tools for database connectivity and data presentation."
    author: "aegir"
  - subject: "Re: What about krayon?"
    date: 2001-08-29
    body: "> About Katabase, IMHO the project will resurrect\n> with KDE 3 because Qt 3 provides tools for\n> database connectivity and data presentation\n\nWhere i can find more info about katabase project? home url?\nI am really interested to contribute katabase resurection in flame of QT3 database connectivity.\n\nAbout krayon I understand the difficulties of open-source delev. but there is no any info, the latest news is from November 21, 2000, there is only one screenshot from (08/99), which is for kde1. Community needs to be informed about recent developments, last weeks news were very rare, it seemed like kde died after post that it released version 2.2 and the review news. But there is no such article about latest improvements in krayon.\n\nBTW recent screenshots of koffice (and krayon too) can be found at http://www.mslinux.com - a koffice guide (only first two chapters are ready). I think also that the Dot news should post an article about this site :)\n\nJust want to be informed ;)"
    author: "kde user"
  - subject: "Re: What about krayon?"
    date: 2001-08-29
    body: "About crayon, seen the \"kimageshop\" mailing list on KDE website.\n\nAbout katabase, I don't know."
    author: "aegir"
  - subject: "Re: What about krayon?"
    date: 2001-08-31
    body: "AFAIK that list hasn't been used in quite a while..."
    author: "Rk"
  - subject: "Re: What about krayon?"
    date: 2001-08-30
    body: "Actually I think it looks like Katabase has been replaced/superceded/evolved into/whatever rekall by theKompany.  And it looks very good so far (only up to version 0.5 (beta 3), but is shaping up to be VERY good).\n\nIt' so far can use MySQL, PostgreSQL or xBase files as a backend, uses Kugar for reports (I think) and the form logic is programmable in Python.\n\nLooks very good.\n\nhttp://www.theKompany.com/projects/rekall\n\n;-)"
    author: "Tsujigiri"
  - subject: "KDE print produces huge and buggy .ps files?"
    date: 2001-08-29
    body: "Has anyone had a problem with KDE printing,\nwhere the print produces HUGE .ps files that\nare 3-10 times larger than similar files\nproduced by other apps? In particular, I have\nin mind files produces by mozilla and konqueror.\nAlso, sometimes, .ps files produced by KDE\napps are unreadable by ghostscript, and many\nprinters (esp. HP) do not interpret them\ncorrectly. Is this a known issue or am I\ndoing something wrong?\n\nMagnus."
    author: "Magnus Pym"
  - subject: "Re: KDE print produces huge and buggy .ps files?"
    date: 2001-08-29
    body: "I've seen this too.  It appears to be because QT converts TTF fonts to postscript and includes them in the file.  And it appears to forget some of them.  Today I had to edit an output postscript file and change a Misc-fixedList to a VerdanaList to get a document to print properly.\n\nIt definitely needs some looking at, but I'd wager it's a QT thing, not a KDE thing.\n\n-Dom"
    author: "Dom2"
  - subject: "Re: KDE print produces huge and buggy .ps files?"
    date: 2001-08-29
    body: "You are right, this is entirely a Qt issue, and you should make sure you have the latest Qt 2.x release.\n\nPrinting will be much improved with the Qt 3 port, but that's for later."
    author: "David Faure"
  - subject: "Thank you!"
    date: 2001-08-29
    body: "Thank you KOffice people, for all your hard work.\n\nWith AbiWord, KDE, and KOffice all releasing new versions recently, it's been like Christmas on my Linux box.  :)\n\nzo."
    author: "Johnzo"
  - subject: "WYSIWYG printing?"
    date: 2001-08-29
    body: "One question... Does Kword print in wysiwyg?"
    author: "Billy"
  - subject: "Re: WYSIWYG printing?"
    date: 2001-08-29
    body: "Didn't in the last release candidate for KOffice, and I believe won't here. The KDE developers have a WYSIWYG branch of Kword in CVS, so it should work in the future. At the moment this is KWord's biggest missing feature as I found out to my loss when working on a presentation using KWord."
    author: "Dr_LHA"
  - subject: "Re: WYSIWYG printing?"
    date: 2001-08-29
    body: "Replying to myself. Just installed it and it doesn't. In case anyone doesn't understand this limitation of KWord, try typing a full page of text in a big point font. Then zoom out to 33% and see how the layout changes. In my document I only realised this at the end of writing it, and I had to keep doing print preview and adjusting it to get it write. This is pretty silly. \n\nAbiword behaves correctly in this manner, so I currently use that for my word processing needs.\n\nHopefully KOffice 1.2 will address this - until it does I'll not be able to use KWord unfortunately (which is a shame because I like everything else about it apart from it's ability to accurately lay out words!)."
    author: "Dr_LHA"
  - subject: "Re: WYSIWYG printing?"
    date: 2001-08-29
    body: "Yes, it's not in 1.1, and I'm working on this.\nThe code in CVS (HEAD branch of course) already has WYSIWYG support, so this is almost done. Feel free to test it and report bugs. The current known issue is that it leads to quite big spaces between words, I might have to add some pixels between the letters in some cases to prevent that. But in any case the printing is _really_ WYSIWYG (the text is always flown at the same high resolution internally - same solution as Abiword's)."
    author: "David Faure"
  - subject: "Re: WYSIWYG printing?"
    date: 2001-08-30
    body: "<Mr Burns impression> Excellent!\n\n;-)"
    author: "Dr_LHA"
  - subject: "Re: WYSIWYG printing?"
    date: 2001-08-29
    body: "Didn't in the last release candidate for KOffice, and I believe won't here. The KDE developers have a WYSIWYG branch of Kword in CVS, so it should work in the future. At the moment this is KWord's biggest missing feature as I found out to my loss when working on a presentation using KWord."
    author: "Dr_LHA"
  - subject: "Kivio Stencils"
    date: 2001-08-29
    body: "Kivio supports plugin stencil sets. theKompany has many more available for 5 or 10 dollars. However it would be very nice if more free stencil sets were available by default.\n\nOne thing I am interested in as UML stencils. The \"Basic UML\" stencils at theKompany just don't cut it though. Dia is awesome for UML diagrams, and there's no reason why Kivio can't be either. Anyone interested in making some UML stencils?"
    author: "David Johnson"
  - subject: "Re: Kivio Stencils"
    date: 2001-08-29
    body: "I agree, and am thinking about bulding some UML stencils in my spare time. I havn't the time right now to start programming stencil sets, and I can't find the \"stencil builder\" anywhere. Is there any docs or HOWTOs on programming stencil sets?"
    author: "eze"
  - subject: "Re: Kivio Stencils"
    date: 2001-08-30
    body: "Let me know if you find those docs, i'd be glad tp help :-)"
    author: "cobaco"
  - subject: "Re: Kivio Stencils"
    date: 2001-08-30
    body: "Let me know if you find those docs, i'd be glad tp help :-)"
    author: "cobaco"
  - subject: "Re: Kivio Stencils"
    date: 2001-08-31
    body: "There will be a stencil builder available in a week or two that you can buy.  Stencils don't have to be programmed, but they do need to be created by someone with some level of artistic skill.\n\nShawn"
    author: "Shawn Gordon"
  - subject: "Re: Kivio Stencils"
    date: 2003-02-28
    body: "Now that some time has passed, are there any more Kivio stencils publically available, specifically for UML?"
    author: "Eric Kaps"
  - subject: "Re: Kivio Stencils"
    date: 2003-04-24
    body: "deffinatly interested send info"
    author: "SGT. DAGS"
  - subject: "Re: Kivio Stencils"
    date: 2003-08-31
    body: "now. more time has passed. where are the stenclis and where is a stencil builder? does any one here know?"
    author: "shinai"
  - subject: "Re: Kivio Stencils"
    date: 2003-10-08
    body: "Well, under the Tools->Add Stencil Set you can find a good number of stencils (at least in version 1.92.\n\nAlso at http://sourceforge.net/projects/xfig2sml/\nyou can find a number of nice stencils (for instance stencils converted from Dia).\n\nHTH,\nMartijn"
    author: "Martijn"
  - subject: "Re: Kivio Stencils"
    date: 2005-11-21
    body: "i tried to install/use this\nwhat a mission and it got me no where :)\ni reccomend you either have a doctors degree in science or leave it untill you have many hours to spend to get this working.\nthe install file helped me nothing, \n\n\"copy this script in the xfig-library path\nkivio don't handle a deep directory structure, so you have\nto \"flatten\" the structure (see *stencils-*.bz2) - this are already \ngenerated stencils\"\n\nthats al it said, and i tried to follow instructions but it got me no where\nthanks anyways, and kivio is an awsome tool, but i think ill wait till i have more patience or that doctors degree ;)"
    author: "winston"
  - subject: "Re: Kivio Stencils"
    date: 2008-11-19
    body: "To successfully flatten, I did this:\n\n1. From Kivio/File/Install, install the stencil file\n\n2. Change to $HOME/.kde/share/apps/kivio/stencils/stencils\n\n3. Move all directories to the directory above:\n\nmv Electrical Knitting Libraries Maps Optics Structural_Analysis ../\n\n4. Restart Kivio"
    author: "Instructor"
  - subject: "Praises and Wishlist"
    date: 2001-08-29
    body: "Great work Koffice team. Koffice is shaping up very nicely. The hard work that has gone into this project is very evident. And the best part is that it is only going to get better. I can't wait to see where this app goes. Speaking of which here is a WISHLIST for future versions:\n  1. underline mispelled words as they are typed\n  2. \"New table\" in the table menu\n  3. \"Delete table\" in the table menu\n  4. Make table UI more MS Word like\n  5. Text to table\n  6. Shading (as in borders and shading)\n  7. More styles\n\nBy more Word like I mean rather than selecting a cell, modifying the cell width/height which modifies the row/column hight you sould get that \"resize\" cursor when you mouse over a cell border and resize only those cells that are selected, or all if none are selected. \n\nAgain big ups. This is a great product from a great team of dedicated opern source developers. If I have time, I would like to add some of these items myself."
    author: "eze"
  - subject: "Re: Praises and Wishlist"
    date: 2001-08-29
    body: "> I would like to add some of these items myself.\nCool, please do ;-)\n\nIn any case, please use http://bugs.kde.org to register the wishlist items (with slightly more details). Otherwise they'll just get lost."
    author: "David Faure"
  - subject: "Wondering"
    date: 2001-08-29
    body: "As a newbie, I'm just wondering why the developers don't pour their development efforts into porting OpenOffice to KDE rather than duplicating all this work in KOffice. As a newbie, I will probably go with Gnome as it (I understand) will use OO as its main office suite? At present OO on windows works very well, has M$ Word, Excel, etc. importing that work well, and is WYSIWYG. Comments?"
    author: "Rob"
  - subject: "Re: Wondering"
    date: 2001-08-29
    body: "OpenOffice is 400 MB of source code (!), and some of StarOffice hasn't been made opensource (AFAIK).\nSo it's all quite messy....\n\nWhy do I spend my time on KWord rather than on porting OO ?\n\n1) because I think it's MUCH more fun :-)\n\n2) because I believe we have the right tools within Qt/KDE (and the koffice libs) for a great office suite, using all the KDE technologies\n(for instance, is openoffice network transparent ? I strongly doubt it - or if it is, it's a duplicated effort wrt all the kioslaves KDE has ;-) Do you realize that you can now save a koffice document directly onto a FTP server, optionnally using a secure connection (kio_sftp slave) ?\nEmbedding, configurable toolbars, etc. etc. all of this comes for 'free' for any koffice application.\nRight-to-left editing (for hebrew/arabic) will come with very little or no effort, with Qt3, etc.\n\n3) because KOffice will remain much more lightweight than staroffice/openoffice, even for the same features, especially when used within KDE (with all the libraries already being loaded).\n\n4) KOffice is very well designed, which helps extending it step by step. Starting from very old sources (with not such a great design) doesn't help at all. I'm not saying this is the case with openoffice (haven't checked the sources), but in general, several-years-old sources (often C) are no match to new, well designed, object-oriented code (like C++).\n\n5) alternatives are good - that's what Linux is all about ;)\n\n6) re-read reason 1, it's the more important one ;-)"
    author: "David Faure"
  - subject: "Re: Wondering"
    date: 2001-08-29
    body: "Interesting reply. Question : What happens then for example re imports filters. Can you / do you grab the Open Office code and modify it. This would seem sensible. As someone currently trying to get rid of all M$ stuff (just sold my copt of M$ office), I need an office suite that will replace it. At present, that is OO build 638. Or is it more efficient to start from scratch?"
    author: "Rob"
  - subject: "Re: Wondering"
    date: 2001-08-29
    body: "I don't personnally work on the filters (only on KWord and the KOffice libraries), but I know that those who do have had a look at the other available filters (e.g. wvware).\nI'm actually not sure that the filters from OpenOffice are opensource - at least, at the beginning they weren't.\n\nAnyway, two koffice developers are currently working with the AbiWord filter developers (wvware library) on a common layer for importing MSWord documents. One area where work won't be duplicated anymore, between the two opensource projects.\n\nAlso, KOffice might switch to ZIP instead of tar.gz, for technical reasons, bringing more compatibility with OpenOffice. No promises though, just evaluating this at the moment."
    author: "David Faure"
  - subject: "Re: Wondering"
    date: 2001-08-30
    body: "I've had a  look at the OO filters, but couldn't get my head around them. I was considering doing a powerpoint filter for Kpresenter, but there is very little in the way of documentation for powerpoint file formats, and I couldn't work out how OO was doing it. Though the OO filters are very good, using OO638 powerpoint files open perfectly. Maybe I should have another look."
    author: "Ash G"
  - subject: "Re: Wondering"
    date: 2001-08-31
    body: "A suggest talking with one of the OO developers. The filters are indeed the best non MS filers for MS programs available (heck, they open even files Word itself chokes upon..*grin*). But it will be difficult to copy them to koffice due to complete different designs of the filter system. What it can do however is, give you information about undocumented binary stuff in the file format.\nreally really talk with one of the OO people!"
    author: "Danny"
  - subject: "Re: Wondering"
    date: 2001-09-04
    body: "Hmmm.. how about a filter for Open Office formats? \n\nI mean, why port the filtering parts when you could probably encapsulate them, have them convert to OO format (which is what they do in OO anyways), then use KWord's OO filter. Can anyone tell me if this would be at all feasible?"
    author: "Carbon"
  - subject: "Re: Wondering"
    date: 2001-08-30
    body: "I'm not working an KOffice, but I can offer some reasons why:\n\n1) KDE does not do what GNOME is doing merely because GNOME is doing it. Hopefully the reverse situatin is true as well.\n\n2) KOffice was started before the OpenOffice project got started. We were there first.\n\n3) It is much more efficient to start an Open Source project from scratch rather than to start with a recently opened proprietary code base. Konqueror started much later than Mozilla, but still finished earlier.\n\nDon't get me wrong, OpenOffice is going to be great when it's done (just as Mozilla will be). But that doesn't scratch the \"itch\"."
    author: "David Johnson"
  - subject: "Great!"
    date: 2001-08-30
    body: "Too bad KDE as a whole is ___DREADFULLY__ slow on a pentium 200 with 32 MB of RAM! Pray for OpenWatcom, everyone!"
    author: "microhajk"
  - subject: "Re: Great!"
    date: 2001-08-30
    body: "go buy yourself some ram.......or better yet a whole new motherboard + cpu+ram....wont cost you much at all.  Memory and cpu are so cheap now.\n\nP.S.  I use gnome but I still use kde apps....great work guys...keep it coming"
    author: "anonward"
  - subject: "RAM is cheap."
    date: 2001-08-30
    body: "I'm running a pentium166 with 192 megs of RAM and KDE is great; perhaps a little slow opening some windows but I love it. Thanks, KDE team.\n\nMandrake and KDE for me? for free?\nAhhhhhh, the joys of the revolution."
    author: "EdlinUser"
  - subject: "Re: Great!"
    date: 2001-08-30
    body: "So fuck*** what?!\n\nSo, KDE is slow on some 5 year old technology.\n\nIs anyone forcing you to use the latest soft-\nware on ancient technology? There are plenty\nof alternatives for slower machines, use them.\n\nQuake won't run on an Atari 2600, and Linux doesn't run on a 286 with 16MB. \n\nDoes that mean they are bad?"
    author: "Lune"
  - subject: "Office Plugin"
    date: 2001-08-30
    body: "Would it be possible to write a plug-in for MS Word that would allow it to read this proposed XML standard? Or does RTF already satisfy the needs of people wanting to take their documents from KOffice to Word?\n\nThe reason I have to use MS Word at home is because I use it at school; if a 30 second install at school allowed me to write the KOffice format in Word then I wouldn't have any interoperablity problems. Granted, for people who are recieveing .doc from other people this wouldn't be very useful, but it would for me."
    author: "Ian"
  - subject: "Kword won't start..."
    date: 2001-08-30
    body: "I'm not sure if this is an appropriate place, but anyways... I can't get Kword to start (even though other Koffice programs run just fine).\n\nI tried to run it as root and also with another  user account, and there was no problem. But with the user account I normally use, it doesn't work. \n\nClicking on the Kword icon or typing \"kword\" on a console make the taskbar show \"kword is starting\" animation thingy, but then.. nothing happens. Not even any error messages appear.\n\nI was wondering if there are some lock-files that need to be removed or something.. Any ideas?\n(I'm running RH 7.1, by the way.)"
    author: "Joni"
  - subject: "Re: Kword won't start..."
    date: 2001-08-30
    body: "execute kword in konsole and paste the output here\n\ntry to run kbuildsycoca"
    author: "me"
  - subject: "Re: Kword won't start..."
    date: 2001-08-30
    body: "It gives no output whatsoever:\n\n[joni@localhost joni]$ kword \n[joni@localhost joni]$ \n\nThe CPU usage goes up for a second and then comes back down. Pretty odd, huh?\n\nKbuildsycoca doens't say anything, either..."
    author: "Joni"
  - subject: "Problem solved"
    date: 2001-08-30
    body: "I managed to locate the problem to a file named \n~/.kde/share/applnk/Office/kword.desktop\nwhich had the following in it:\n\n[Desktop Entry]\nHidden=true\nName=empty\n\nAfter removing that file, Kword starts just fine."
    author: "Joni"
  - subject: "Unable to install RPM on RH7.0"
    date: 2001-09-09
    body: "I've just installed KDE2.2, and went on to install KOffice 1.1. However, I get the following dependency-problems:\n$ rpm -Uvh koffice-1.1-2.i386.rpm\nerror: failed dependencies:\n        libcrypto.so.2   is needed by koffice-1.1-2\n        libexpat.so.0   is needed by koffice-1.1-2\n        libssl.so.2   is needed by koffice-1.1-2\n\nTo get around the expat problem, I just had to install expat-1.95, but the libraries from openssl seems trickier. It seems that they're called libssl.so.0.96 in the version I have, but if I try to install an RPM where they're named .so.2, I get an error because several packages are using them.\n\nI seems that a lot of people have been able to install KDE2.2 and KOffice1.1 on RH7, could you please help me find the right packages?\n\nThanks,\nHB."
    author: "Havard Bjastad"
  - subject: "Re: Unable to install RPM on RH7.0"
    date: 2001-09-16
    body: "Yes, I've had similar problem with RH7.1 - RedHat distributes\nrequired libs in separate RPMs called openssl095a, openssl096.\nYou should be able to find them with rpmfind or in redhat-rawhide\ndistrib. (though I'm not sure if they'll work with RH7.0)\nHope this helps..."
    author: "thomas"
  - subject: "Re: Unable to install RPM on RH7.0"
    date: 2001-09-17
    body: "You can find the missing rpm from the web site \nftp.rpmfind.net. I was using Redhat V7.1 and installed\nthe missing rpm and it works fine."
    author: "Ashok Marudanayakam"
---
After a series of three betas and one release candidate, the <A HREF="http://www.kde.org/">KDE
Project</A> used the occasion of the first day of the <A HREF="http://www.linuxworldexpo.com/">Linux World Expo</A>
in San Francisco, CA to
<A HREF="http://www.koffice.org/announcements/announce-1.1.phtml">announce</A>
(<A HREF="http://www.kde.com/kdepr/koffice-1.1.html">alternate with fixed
table</A>) the much-anticipated stable release of KOffice 1.1.  KOffice
is a free, Open Source,
integrated office suite demonstrating the richness and power of the KDE
development environment.  The announcement contains links to the
source and binary packages as well as a good deal of information about
the current features of the KOffice packages.  A candid assessment by
yours truly follows.

<!--break-->
<P>
Like all of KDE, the interface of each KOffice application is really slick
and gorgeous.  The available functions are easy to use.  The KOffice
developers have again demonstrated their canny ability to make the
transition from other office suites as easily as possible, but making
improvements and innovating where appropriate.  </P>
<P>The feature set
is probably adequate for the great majority of users (and the price tag can't be beat!).
For example,
KPresentation is great and has many useful and snazzy features, but
lacks layers and the ability to easily reproduce layers across
selected pages.  KWord is easily up to the task of generating nice letters,
letterheads, memos, faxes and papers, but lacks hyphenation,
mail merge (or any database integration) and endnotes/footnotes.
Similar stories for the other applications.
</P>
<P>
But, with all due respect to the diligent work of the filter developers, the biggest obstacle
to KOffice right now is the filters for MS Office documents.  So while I will make
KOffice my primary office suite, someone who (1) has a repository of .doc
files; and/or (2) receives many .doc files by email; and/or (3) needs to collaborate on document production with someone tied to non-KOffice formats, and/or (4) has unusually
demanding office needs, will likely not be happy with KOffice as their exclusive Office Suite (yet -- things are improving quickly!).  I hope all the Open Source office developers (Abiword/etc., KOffice, Open Office) can collaborate on writing filters for the extremely complex and poorly-documented proprietary formats into an intermediate, standard-based XML format).
</P>
