---
title: "Kernel Cousin KDE #9:  System Configuration Tools, etc."
date:    2001-05-16
authors:
  - "Dre"
slug:    kernel-cousin-kde-9-system-configuration-tools-etc
comments:
  - subject: "System Configuration Tools"
    date: 2001-05-16
    body: "YES!\n\nThis is needed - like the Ximian setup tools for Gnome, KDE needs some standard configuration tools. I think it's a bit stupid that every distribution makes their own tools. \n\nIt would be much easier for the users if the same configurations tools were available no matter what distribution you use or if you change to another distribution."
    author: "Joergen Ramskov"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-16
    body: "But *please*, do not create a clone of Ximian setup. Instead, built on the GUI independent backends of Ximian setup so that Gnome and KDE share a common base."
    author: "freedesktop"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-16
    body: "Yes, I agree. \nMozilla can be compiled with gtk (default) or qt, why not simply work togheter with xiniam to add a --with-kde --with-qt options, so both enviroements will share the same (mostly) look of tools, user simply can choose if he wants use the setup tools using kde/qt or gnome/gtk.\nThis would be very smart and I think, really good for user.\nBut One thing I'm 100% sure: we need setup tools, the distros aren't really doing a good job. Yes, I know mandrake and suse are doing, but this is the problem! The tools chould be the same on all distros, all enviroements, etc.\nPlease KDE and Gnome/Xiniam people, go for a standard."
    author: "Protoman"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-16
    body: "Yes, that's a good idea.\nJust build some GUI around the backend.\nIt's much easier to do and improves Gnome/KDE cooperation."
    author: "dc"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-16
    body: "I think That KDE cane onlie be big if they have setup tools. My girl frent cane not config a OS whitout any good setup tools"
    author: "underground"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-16
    body: "No need to reinvent the wheel.\n\nEvery distribution I know comes with setup tools.\nSome distributions (like SuSE and Caldera) even come with fully QT-based installation- and setup-tools.\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-16
    body: "Setup tools for each distro is not open source.  In addition, there development is distro centric.  It's best for KDE developers to work on a tool that is distro agnostic.\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "> Setup tools for each distro is not open source. \n\nThis is wrong. Yast as well as Coas *are* OpenSource. Coas is even GPL!"
    author: "Me"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "Nope, Yast ain't open source. It may come with the source but for example there is no way Red Hat could use it."
    author: "nap"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "You confuse open source with GPL. He is actually saying the exact same thing that you are."
    author: "jj"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "No I don't. I use the Open Source Definition presented here:\n\nhttp://www.opensource.org/docs/definition.html\n\nYast license violates the very first requirement:\n\nThe license shall not restrict any party from selling or giving away the software as a component of an aggregate software distribution containing programs from several different sources. The license shall not require a royalty or other fee for such sale."
    author: "nap"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-16
    body: "True.\n\nAnd some of us are using FreeBSD for which such a tool would probably not be useful."
    author: "Michael O'Henly"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "It's important for everyone to know that this tool is not Linux specific.  We even have a person who volunteered to write FreeBSD backends."
    author: "Charles Samuels"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "That's really great to hear! Thank you for the clarification."
    author: "Michael O'Henly"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "Good point... here is mine:\n\nThe KDE team could create a template, if you will, that all distro's could follow to create a standard config section in the control panel.  For example, init scripts for runlevel 3 in redhat = /etc/rc.d/rc3.d.  These same scripts in debian = /etc/rc3.d.  It would be possible to create a template that would allow users to add/delete services from each runlevel and leave it to the distro's to do the backend part.  The gui would be the same on Suse, Redhat, and Debian despite the fact that these 3 have startup scripts in different locations.\n\nAs far as FreeBSD, NetBSD, or OpenBSD is concerned, the template could be modified slightly for bsd init scripts.  The major work is the backend stuff again.  The Linux and BSD versions of the KDE control panel would only need to differ slightly and a user probably would probably be able to use either regardless of the underlying system.  This could work for configurations for mouse settings, video settings, network settings, printer config, and much more.\n\nConclusion:  \nI propose a general template for all configuration.  Make the template modular in such a way that all Linux distro's or even the BSD's could write backend plugins for their specific configurations.  I think this will lead to much more solidarity among KDE desktops without the user having to worry about whether the system is Suse, Redhat, FreeBSD, etc...\n\nUntil the LSB finalizes, a common system config section in KDE will make Linux appear more uniform to most end users.  Standardization of the configuration interface will most definately help the adoption of *nix on the desktop.\n\nMy 2 cents,\n\nPosthumous"
    author: "posthumous"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-16
    body: "*This tool is NOT KDE specific!*  It even says that on the web site.  \"Note: This is not a KDE project.\""
    author: "Charles Samuels"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-16
    body: "Charles,\n\nin that case you might want to consider moving your cvs code to Sourceforge or elsewhere.\n\nI've followed the threads (also on the dedicated list) and I know the intention is to make generic middleware which can work with all sorts of distributions (backends) and GUIs (frontends), much like SANE, but when the shit is within KDE cvs, initiated by KDE people etcetera, it *will* be seen as a KDE project."
    author: "Rob Kaper"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-16
    body: "...or at least create an own homepage on sourceforge, where you can download the stuff separatly.\nHaving it in the KDE cvs is a good thing I think, many users, developers, bugfixing\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "> *This tool is NOT KDE specific!* \n\nWell you really drove home that point by calling the mailing list kde-sysconfig :-P\n\nBtw: I love the idea of a non-distribution non-os non-desktop-environment spesific config program :)"
    author: "Anon"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "Charles,\n\nOn kde-specificness: I have followed this discussion a bit on KDE-core-devel, here and on your new lists' archives. I have not read everything, but I have read a bit. I have a question.\n\nI have seen you mention several times that this effort is not KDE-specific. I have seen people who agree with you saying that there should be some Gnome integration. Yet, even though the question has been asked (ie on this forum), you have never said a word about Ximian setup tools.\n\nForgive me if I am wrong, but your description of what you want to do fits the description of what Ximian wants to do with its tools precisely. They want to be distribution (and *nix) agnostic. They want to be desktop agnostic... So what is the problem?\n\nHave you even looked at their code? Is it good enough that you might contribute instead? If not, is it so dreadfully bad that it is really better for you to forget about collaboration and start your own thing instead?\n\nTalking about writing desktop-agnostic code is fashionable at the moment. Why don't you take it a step futher? Actively collaborate with the Gnome people to build a solid, shared infrastructure...\n\nNot whining, just asking. You code, you decide."
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "according to their web site, it requires gnome:\n\n\"Each backend/frontend combination is standalone and has no other dependencies than a working GNOME setup and the interpreter for the backend script.\"\n\nI also don't trust Ximian and know their implementation isn't C++.  Ximian also doesn't support non-linux (hell, it barely doesn't support non-redhat)."
    author: "Charles Samuels"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "The backend of the Ximian Setup Tools is written in Perl, with only the GUI written with C/GTK+.  To me, it makes perfect sense for the backend perl codebase to be shared, with a C++/QT interface implemented for use in KDE.\n\nSuch collaboration would not only decrease the amount of work required (the XST project is maturing well), but also allow for a common feature set, to which users can become accustomed (whether using either KDE or Gnome).\n\nAs for the support of various systems - to date, many different Linux distributions are supported, as well as planned support for Solarus and HP-UX.  See http://www.ximian.com/newsitems/xst_04.php3 for the details of the latest XST release and overview.  It looks like quite a capable system that would also benifit and improve with the help of others."
    author: "Tim Riley"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "Why don't you trust Ximian?\nAre there evil aliens working behind Ximian?\nAren't the developers just humans too?\nThink about it: Ximian Setup Tools is GPL.\nThe developers would be happy if they hear that KDE wants to cooporate.\n\nSo Ximian don't support non-Linux operating systems yet.\nWho said they wouldn't in the future?\nAnd how do you know this is also the case with Setup Tools?\nEven if they don't support other operating systems, if you guys work together that can be easily added and saves both parties lots of time.\n\nIf you're afraid that Ximian will turn it onto a closed source project, then think again.\nIf KDE contributes then that means the rights are shared, so they can't relicense the code anymore."
    author: "dc"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-17
    body: "You did'nt know? Yes they are evil alien stuffed monkey peddlers. Not to be trusted."
    author: "Craig"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-19
    body: "That's like saying that KDE is a group of ev1l green dragons."
    author: "dc"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-19
    body: "Yea of course where have you been?"
    author: "Craig"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-18
    body: "\"Each backend/frontend combination is standalone and has no other dependencies than a working GNOME setup and the interpreter for the backend script.\"\n\nThe backend/frontend *combination* requires GNOME, NOT the backend alone.  The backend is entirely independent of GNOME as I understand it.  Of course Ximian's frontend requires GNOME, after all, it's what they do.  It would be a shame to just ignore the work that Ximian has done here because of a misunderstanding.  Writing a KDE frontend to Ximian's setup tools (and perhaps working on and improving the backends too) would seem to be the best course of action here.  After all, the setup tools themselves are GPL and Ximian can never take them away.  Ximian has no control over the existing code.  Why re-do all the work Ximian has done so far and make it impossible for them to help in the future when you could use what they already have and get a head start?\n\n\"I also don't trust Ximian and know their implementation isn't C++.\"\n\nWell, their frontend, being GNOME-based, is in C.  But its only one possible frontend.  The backends (the only part of the software that this project would use) are in Perl, I think.  You wouldn't want to program the backends in C++ anyway, would you?  Seems like perl or python would be a better choice."
    author: "not me"
  - subject: "Re: System Configuration Tools"
    date: 2001-05-21
    body: "What more is there to say? So much for co-operation. Good luck on your project."
    author: "Jerome Loisel"
  - subject: "System Configuration Tools"
    date: 2001-05-16
    body: "Yes I want this.\n\nI want to have _total control over my box, regardless which platform (Linux, *bsd,...) and all with a consistent GUI.\n\nI don't have any usage of the different GUIs of the distributions.\n\nPlease give me total control!\n\nPhilipp"
    author: "Philipp"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration To"
    date: 2001-05-16
    body: "I think system config tools is a great idea. With proper tools integrated tightly into the UI will make it easier for Windows users to move to Linux. Difficult config is what stops most people!"
    author: "Dave Oxley"
  - subject: "A spiral ? [System Configuration Tools]"
    date: 2001-05-17
    body: "Hello,\n\nHum, I was confused about KDE system configuration tools, because it is out of the desktop environment... However, I now see that there are some good arguments in this direction...\n\nAs a basic user, I felt some disappointment in the Mandrake 8.0 when I saw that interesting tools where not KDE-like, but Gnome-like... Out of my comfort... With some duplications with Kde (rpmdrake-kpackage...)\n\nI also saw that the start menu was managed by Mandrake, not by KDE... However it is, here, more comprehensive for having the same menu in KDE and in Gnome... But I only use the KDE desktop...\n\nSo, in facts, Gnome has already system configuration tools and it is growing and growing. So, in the end, they may be more grouped for offering a larger set of programs and it will, of course, minimise the KDE Desktop.\n\nSo it is a good policy to go in the same way and try to do better...\n\nAnd, as told in previous messages, there are others arguments to go in this direction...\n\nIt seems to be a step, a necessary step. So KDE is going to be more than a desktop environment... I hope that it will not weaken the desktop development...\n\nSo, both in Gnome and KDE, there seems to be some logic of larger and larger growing, of eternal separation...\n\nAnd I wonder about this spiral... Is then going the time where there will be Gnome distributions and KDE distributions ?? And then a Linux-Gnome and a Linux-KDE ??? \n\nI hope good job..."
    author: "Alain"
  - subject: "Re: A spiral ? [System Configuration Tools]"
    date: 2001-05-17
    body: "I think that people will see something very different happen over the next year.  It's becoming clear to everyone that the huge gap between KDE and Gnome can't continue to exist.  Forcing developers to choose one toolkit over the other and cut out 1/2 of the possible audience is not something that most people find all that attractive, and unfortunately it's holding back both KDE and Gnome.\n\nHaving both KDE and Gnome existing seems to be very positive for both projects, however interoperability between them will be key for the long term survival of both projects.  Many positive things could happen if developers are free to choose whatever desktop environment they prefer, users are free to choosee, and the choices don't need to coincide for the app to perform optimally."
    author: "Chad Kitching"
  - subject: "Re: A spiral ? [System Configuration Tools]"
    date: 2001-05-17
    body: "\"Many positive things could happen if developers are free to choose whatever desktop environment they prefer, users are free to choosee, and the choices don't need to coincide for the app to perform optimally.\"\n\nWhere do people get the idea that this is not already the case?  Look, you can run KDE programs on a GNOME desktop and GNOME programs on a KDE desktop (you can even run some windows programs on either with WINE).  You can use whatever setup tools you want (KDE's, Ximian's, WebMin, etc).  Developers can choose to write programs for whatever desktop environment they want and know that anyone with a reasonably recent Linux distro will be able to run them regardless of whether they use KDE, GNOME, Enlightenment, WindowMaker, TWM, or whatever!  Where is the problem?  I see no problem here."
    author: "not me"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration Tools, etc."
    date: 2001-05-17
    body: "I think there is no need for setup tools that works both in console and GUI. Either some one configure tools dirrectly in the configuration files, wither he use the GUI tool.\n\nAlso, you can think more, to not only to just a setup tool, but to an entire management system. I mean, for example, that instead of writing a setup tol for Apache, you can develop an \"Apache Control Center\", that could be used for both configurations and management (watching logs, stop/restart daemon, monitor performance, statistics (even in the way of KDE System Guard, updating), training (manuals, embeding websites like apachetoday.com or even embeding kmail for specific discussion lists)."
    author: "Socrate"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration Tools, etc."
    date: 2001-05-17
    body: "I think there is no need for setup tools that works both in console and GUI. Either some one configure tools dirrectly in the configuration files, wither he use the GUI tool.\t\n\nI couldn't disagree more. If X is unavailable for some reason, and a user doesn't know how to edit config files, it will be *crucial* to have a ncurses or some type of interface for them. Besides, there are plenty of servers out there that don't use X at all.\n\nErik"
    author: "Erik Severinghaus"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration Tools, etc."
    date: 2001-05-17
    body: "Well, in that case there is already Linuxconf. Why spend time reinven it?\n\nThe motivation that such setup tools can be used if no X is available is not valid, because KDE is suposed to be used where X is available, for peoples that need it (and of cors for the people that love it).\n\nBesides, this time could be used to design what I proposed as \"Someserver Control Center\" (where someserver could be any one of current widly used servers: Apache, BIND, Sendmail/Qmail/Postfix, DHCP, Samba, wuftpd/proftpd, SSH, etc) in the way I provided an example for Apache in my first message.\n\nI think that such integrated \"management control centers\" for services where Linux rulez could be more helpful for both KDE and Linux, because they could provide a smoother learning and transition both for newbie and peoples from another OS world, than jumping dirrectly in setup files, logfiles, BSD or SysV startup scripts, shel, etc. This way, Linux could attract more peoples that are used with other GUIs and that don't know from where to start.\n\nShortly, the ideea is that Linux and KDE needs more that just some setup tools. Simply, let's go further and do things that are really needed."
    author: "Socrate"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration Tools, etc."
    date: 2001-05-18
    body: "\"KDE is suposed to be used where X is available\"\n\nThese proposed config tools are NOT KDE- or even Linux-specific.  The hope is that they will be a flexible backend that can be adapted for any system and take a wide range of frontends, from a KDE/QT or GNOME/GTK frontend to a ncurses console-based frontend.  In fact, they sound exactly like the Ximian setup tools that are being developed in this respect.\n\n\"Well, in that case there is already Linuxconf. Why spend time reinven it?\"\n\nLinuxconf only works on some Linux systems, as opposed to these proposed tools which would work on all Linux, BSD, and other Unix-like systems.  Besides, many people are not satisfied with Linuxconf.\n\nMost of the services you mention will probably have control panels in the proposed setup tools, if they actually become a reality.  To truly be a total system control panel, these would have to be configurable."
    author: "not me"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration Tools, etc."
    date: 2001-05-17
    body: "I think all you guys against this idea do not use linux for anything serious.\n\nI have a bunch of NT kids trying to get to grips with linux. And every time the have to configure something, the first question they ask is: Which tool do I use? kcontrol? Mandrake control? linuxconf? netconf? netcfg?\n\nEach one of these does something better than the other.\n\nWhen you move accross distros, its total chaos. Yast, Lisa, Yast2, coas etc.\n\nwith kde-sysconfig, all a user will have to learn is KDE. They wont even need to learn linux or BSD or whatever.\n\nCheers"
    author: "nalogquin"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration Tools, etc."
    date: 2001-05-17
    body: "Why not just improving Ximian Setup Tools?\nIt has a GUI-independent backend.\nAnd it can always be ported to other OSses.\n\nSaying \"Because I don't trust Ximian\" is not a valid answer.\nThey exists more than 2 years and haven't taken over GNOME, nor messed up it's codebase, dispite what people have predicted for months."
    author: "dc"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration Tools, etc."
    date: 2001-05-17
    body: "What do you mean they hav'nt taken over gnome? Thats just silly."
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration Tools, etc."
    date: 2001-05-19
    body: "Well then, show me your proof.\nI don't have to ask Ximian before I download/modify/sell GNOME.\nI don't have to ask Ximian before I send bug reports.\nI don't have to ask Ximian before I can send patches to the mailing list.\nDo you?"
    author: "dc"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration Tools, etc."
    date: 2001-05-19
    body: "The proof can be found on the tag thats on the Xiamian monkeys. If you hav'nt bought one then you don't know. Never trust a carney stuffed monkey peddler.\n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration Tools, etc."
    date: 2001-05-21
    body: "Craig,\n\nWhy do you have so much venom for Ximian and GNOME?  Working together with GNOME would *help* both environments, and as far as I can tell, most of the developers of the respective environments are starting to do that.\n\nThe plush monkey-peddler comments really come off as trollish.  Look at what Ximian is doing.  Look at how they are purposefully separating the backend and frontend code, so that other toolkits and environments can use it.  See how even things like Bonobo (which is used *only* by GNOME right now) could be used with other toolkits as well.\n\nLet go of the venom and the hatred.  Those things only lead to the dark side ;-)."
    author: "andrew"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration Tools, etc."
    date: 2001-05-21
    body: "Get over it. It was a joke. I can't believe you replyed to it. \n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration To"
    date: 2001-05-17
    body: "he not trusting ximian is one of the reasons.\n\nthe main one is:\n\n\u00a8Each backend/frontend combination is standalone and has no other dependencies than a working GNOME setup and the interpreter for the backend script.\u00a8 from  http://www.ximian.com/tech/helix-setup-tools.php3.\n\nwhat they\u00b4re aiming to create would work on a freebsd box with only gnome, no kde, for example."
    author: "Evandro"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration To"
    date: 2001-05-19
    body: "Have you asked the developers wether they would change that or not?\nThey just might be willing to do that!"
    author: "dc"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration To"
    date: 2001-05-19
    body: "\"Each backend/frontend combination is standalone and has no other dependencies than a working GNOME setup and the interpreter for the backend script.\"\n\nThe backend/frontend *combination* requires GNOME, NOT the backend alone."
    author: "dc"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration To"
    date: 2001-05-17
    body: "\u00a8They exists more than 2 years and haven't taken over GNOME\u00a8\n\nthat\u00b4s a questionable affirmation.\n\nthey control: bonobo, gnome-print, gdk-pixbuf, glade, gnome-basic, gal; ximian gnumeric, ximian evolution; not to mention \u00a8the gnome foundation\u00a8, which ximian is a part of, where bussiness people make decisions for gnome, not developers.\n\nlook here: http://linuxtoday.com/news_story.php3?ltsn=2001-05-16-010-20-NW-GN.\n\ndo you know who sander vesik is? well, he works for sun. quoting miguel de icaza: \n\n\u00a8    * Aim low: we do minimal cleanup work on the core libraries; we do little or no integration of modules like gnome-vfs and bonobo and we just port things over to the Gtk 2.0 platform.\u00a8\n\n\u00a8  The blue sky scenario might lead to a number of problems:\n\n* Force vendors like Sun to either delay their adoption for GNOME, or make them stick to the GNOME 1.4 platform.\u00a8\n\n\u00a8 We do have existing evidence from free software projects that the above might very well happen. Linux 2.4, KDE 2.0, Gtk+ 2.0, Nautilus 1.0, Evolution 1.0 have all slipped on their schedules as they were projects with more ambition than time on their schedules.\n\u00a8\n\nthe full document at primates.ximian.com/~miguel/gnome-2.0"
    author: "Evandro"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration To"
    date: 2001-05-18
    body: "How does Ximian control the GNOME Foundation?  \n\n\nNothing in the minutes of the Foundation can be used to back up your claim.  The GNOME-2.0 documenta that you quote was my `proposal' for the way we should do GNOME 2.0 and be careful about how things moved forward.\n\nThis was *not* the proposal that was approved by people at GUADEC, a different version was approved which based on mine, it was the `multi-split libraries' proposal.\n\nThat proposal is also in public, and actually the GNOME 2.0 is being coordinated by volunteers at SuSE and Sun.\n\nMiguel."
    author: "Miguel de Icaza"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration To"
    date: 2001-05-19
    body: "Aha. So I have to ask Ximian before I can modify/sell Gnumeric, gnome-vfs, glade and stuff?\nDo I have to ask them before I can fork them?\nI don't. Do you?"
    author: "dc"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration To"
    date: 2001-05-22
    body: "nothing like this ever happened in the gnome community.\n\nhttp://kt.zork.net/kde/kde20010511_9.html#1\n\nThe Kompany controls parts of kde(kvivio), \nwhereas ximian welcomes the community."
    author: "ac"
  - subject: "This is a critical moment for Linux on the desktop"
    date: 2001-05-17
    body: "Two months ago I awoke to a distance sound, almost like a drum far over the horizon.  Today that sound is louder and I know what it is.  I also know that many of you have heard this sound.  It is a beast of monsterous proportions striding forth from its cave in Redmond, Washington.  Branded on its twisted forehead is a strange symbol: XP.  Make no mistake people... This monster will test the Penguin and his allies like nothing before.\n\nDo you remember the hoopla surrounding Win95?  Keep in mind that Microsoft has sworn to spend (and yell) even more for the launch of XP.  This is not the moment for GNOME and KDE to fight.  The future of Linux on the desktop is balanced on a knife edge.  Anyone who doesn't agree is fooling themselves.\n\nThis is an opportunity for the KDE guys to show the Open Source community the correct path... cooperation instead of competition.  There have been hints recently of the ice between KDE and GNOME beginning to thaw.  Using the same backend for system configuration would be a huge step in the right direction.  End users will like it and the media will love it.  Maybe this can't be done... maybe the Ximian backend cannot be abstracted suffeciently to allow KDE to use it.  However, the effort must be made.  There is too much at stake."
    author: "Rimmer"
  - subject: "Re: This is a critical moment for Linux on the desktop"
    date: 2001-05-18
    body: "Yes, Windows XP will be a true test of Linux.  XP is the result of Microsoft finally waking up and realizing what their customers want.  XP will be *very* stable, possibly even as stable as Linux, probably more stable than XFree+KDE, and certainly light-years ahead of any other Microsoft product.  It will also have features that Linux has had for years, such as true multi-user capability and remote-desktop features like X.  The difference with XP is that all these features will be easy-to-use and accessible to anyone from the start, unlike the same features in Linux which practically require a doctorate in system configuration files to set up.  Unified config tools such as these proposed tools are the only way KDE can hope to compete with XP's new ease-of-use."
    author: "not me"
  - subject: "What the hell are you talking about?"
    date: 2001-05-19
    body: "Have you ever LOOKED at the source code if XST?\nYes, that's right, YOU CAN GET THE SOURCE CODE!\nAnd guess what? You don't have to ask Ximian for permission and you can even fork it!\nWow, amazing, everybody said that it's controlled by Ximian!\nApparently not..."
    author: "dc"
  - subject: "Ouch"
    date: 2001-05-21
    body: "Er... I think I was saying that KDE should try to use the Ximian setup tools.  Isnt that what I said? *ponder*"
    author: "Rimmer"
  - subject: "Re: Ouch"
    date: 2001-05-21
    body: "??????????????????????????\nOops, must be my bat and inkompleet knowledge of English. Sorry. :)"
    author: "dc"
  - subject: "Re: Kernel Cousin KDE #9:  System Configuration Tools, etc."
    date: 2001-05-18
    body: "This sort of rubbish is exactly what KDE should avoid getting involved in. At best it would be a misuse of developers time. Let the distros clean up their own mess."
    author: "Spooq"
  - subject: "You might want to look at the Ximian Setup Tools"
    date: 2001-05-18
    body: "The Ximian Setup Tools have been architected into front-ends and backends, the backends do not even have a GUI and no dependency on GNOME.\n\nThe goals are pretty much the same, and we have been working for some time on them.\n\nCheck the white paper here:\n\nhttp://primates.ximian.com/~miguel/helix-setup-tools.html"
    author: "Miguel de Icaza"
  - subject: "GNOME & KDE code sharing ?"
    date: 2001-05-18
    body: "\"..There is no need to duplicate this functionality between GNOME and KDE...\"\n\t\t- Chema Celorio \n\nhttp://www.derkarl.org/kdesysconfig-archives.phtml?id=63"
    author: "Jose"
  - subject: "System Configuration Program"
    date: 2001-05-18
    body: "Actually, I have started a project at Source Forge which is exactly what you are talking about.  It's called KISAW, or KDE Integrated System Administration Wonder.  There's still no code, but the project exists.  So, if anyone wants to help out, please visit http://sourceforge.net/projects/kisaw."
    author: "Steve Hunt"
  - subject: "Sorry about the multiple posts :-("
    date: 2001-05-18
    body: "Sorry about that.  Believe me, it was accidental!! PLEASE!!!\n\nAdmins:  Please, just delete them :-)"
    author: "Steve Hunt"
  - subject: "System Configuration Program"
    date: 2001-05-18
    body: "Actually, I have started a project at Source Forge which is exactly what you are talking about.  It's called KISAW, or KDE Integrated System Administration Wonder.  There's still no code, but the project exists.  So, if anyone wants to help out, please visit http://sourceforge.net/projects/kisaw."
    author: "Steve Hunt"
  - subject: "System Configuration Program"
    date: 2001-05-18
    body: "Actually, I have started a project at Source Forge which is exactly what you are talking about.  It's called KISAW, or KDE Integrated System Administration Wonder.  There's still no code, but the project exists.  So, if anyone wants to help out, please visit http://sourceforge.net/projects/kisaw."
    author: "Steve Hunt"
  - subject: "Ah, now I get it."
    date: 2001-05-19
    body: "You guys don't cooperate with Ximian not because Ximian Setup Tools' backends aren't good enough, but because you dont WANT to!\nGreat. Keep up the FUD. Believe what you want.\nBelieve that GNOME is controlled by Ximian and you have to ask permission first before you can fork the source code.\nBelieve that the GPL isn't \"viral\" enough to protect GNOME.\nBelieve that the FSF will do nothing about it.\n\nI don't ask permission to do that. Do you?\nI don't see TrollTech or TheKompany be able to take over KDE, do you?\nI don't see the FSF sit there and do thing if such thing happens, do you?"
    author: "dc"
  - subject: "Re: Ah, now I get it."
    date: 2001-05-19
    body: "Good news, troll, we're cooperating with the ximan config thingy, and will begin working on a KDE version, which means soon, unix will have a single unified config system soon! Yeehaw!"
    author: "Charles Samuels"
  - subject: "Re: Ah, now I get it."
    date: 2001-05-20
    body: "Well take another look. I encourage the use of one single backend for both configuration tools and you call me troll?\nNow it's obvious who the real troll is."
    author: "dc"
  - subject: "Re: Ah, now I get it."
    date: 2001-05-20
    body: "The way you state your argument certainly makes you seem like a troll. And unlike the person you are responding to, you are not contributing any code, do you?"
    author: "jj"
  - subject: "Re: Ah, now I get it."
    date: 2001-05-21
    body: "Which part of my comment looks trollish?\nI based them from the other comments I read.\nMost people don't want to work together with Ximian because they fear/don't trust Ximian.\nRead the comments and you know it."
    author: "dc"
---
After missing a week due to other commitments, <A HREF="mailto:aseigo@mountlinux.com">Aaron J. Seigo</A> is back in full swing with <A HREF="http://kt.zork.net/kde/kde20010511_9.html">KC KDE # 9</A>.  Highlights include a new project to build KDE system configuration tools, improvement of the KDE printing framework to direct output to arbitrary devices (including faxes), and access control lists for KDE CVS.  Read more <A HREF="http://kt.zork.net/kde/kde20010511_9.html">here</A>.