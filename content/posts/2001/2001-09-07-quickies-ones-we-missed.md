---
title: "Quickies: The Ones We Missed"
date:    2001-09-07
authors:
  - "numanee"
slug:    quickies-ones-we-missed
comments:
  - subject: "Themes.org situation"
    date: 2001-09-07
    body: "From the latest news item at http://www.themes.org/:\n---------------------------------------------------------------\nA few people have been asking if Themes.Org is dead, the answer is NO! \n \nWe are currently developing a new site, official release date is August 22nd. \n \nAnd until then we are not accepting any new themes. Why? So we can use all our time focusing on the new site :o) Look forward to it, it's going to be loaded with new cool features. \n----------------------------------------------------------------\n\nApparently, the August 22nd hasn't been met :-(\n\nAnyway, this messages explains why there have been no new themes at http://kde.themes.org/"
    author: "Case Roole"
  - subject: "Re: Themes.org situation"
    date: 2001-09-07
    body: "Good, themes are just annoying. Mosfets styles are good enough."
    author: "reihal"
  - subject: "Re: Themes.org situation"
    date: 2001-09-07
    body: "That notice has been up for a while (August 4) -- if you read the discussion, you'll see some editors explaining that it's taking longer than they expected and they're making progress. Themes.org is not the most, errrr, professionally run site, despite its VA Linux ownership.\n\nI'm not sure why this bit was posted today. There doesn't seem to have been any movement at all recently. At least you can still download my WindowMaker themes.  ;-)\n\nOn a more juvenile note, I was amused by the loading of images from the kde-ass directory, like http://images.themes.org/templates/kde-ass/icon-shots.gif. Yeah, it's simple-minded, but it is Friday."
    author: "Otter"
  - subject: "Re: Themes.org situation"
    date: 2001-09-07
    body: "I thought kde.themes.org had been down all this time but was brought back up on Aug 22 as scheduled.  My bad."
    author: "Navindra Umanee"
  - subject: "Re: Themes.org situation"
    date: 2001-09-07
    body: "In my experience, whenever a site posts a message saying they are definitely not dead, it is because they are in fact dying.  Especially when said message mentions a due date that isn't met or even changed after it has passed."
    author: "not me"
  - subject: "What happened to IBM's theme contest ?"
    date: 2001-09-07
    body: "The subject sais it all. \n\nHere is the link that IBM gave us\n\nhttp://www-106.ibm.com/developerworks/linux/library/l-kde-c/?open&l=1974,t=gr,p=kcst\n\nAny comments on what is going on ?\n\n/sam"
    author: "Sam"
  - subject: "Offer: Konqueror Password Manager"
    date: 2001-09-07
    body: "While talking about offers: is there a good place (something like Sourcexchange or CoSource, but that is still open and possibly better) for offering money for features?\nI would like to offer 20 Euro or USD via PayPal/Paybox to the first person who writes a Mozilla-like Password manager for Konqueror and gets this into CVS. But to make it a bit worthwhile for the author I'm searching for other people who also offer money. So how can I find them?"
    author: "Tim Jansen"
  - subject: "Re: Offer: Konqueror Password Manager"
    date: 2001-09-07
    body: "The lack of a good password manager under KDE has bothered me as well. There are some, but none that satisfies my needs. What I would like to see - and might start working on - is:\n\n- a wallet that is locked by default and can only be unlocked by a password to  decrypt the stored passwords\n\n- the option to had different sub-wallets, allowing for my company to send me a new encrypted file with the passwords at work. Each sub-wallet would have a different master-password.\n\n- A docked GUI, with the ability to quickly lock and unlock (sub-)wallets or auto-lock them after so many microts of inactivity.\n\n- Interaction with kio_sftp and minicli. All passwords cached or stored should be managed by the desktop wallet or its sub-wallets.\n\n- The ability to have a RMB or menu item \"paste from wallet\" in HTTP forms. I don't want autocomplete here, but a key binding or RMB to insert a password from the wallet makes sense - it would require manual interaction/knowledge and that the wallet is unlocked (either already or at the moment of the action).\n\nI wish I had more time, but hopefully I will find some and keep this list as a TODO."
    author: "Rob Kaper"
  - subject: "Re: Offer: Konqueror Password Manager"
    date: 2001-09-07
    body: "This is far more than what I had in mind. When you fill out a HTML form in Mozilla that has exactly one password input field the password manager asks you whether it should remember this login for the session or forever. It stores the content of all input fields in the form. If you then go visit this or another page on the same host with the same field names it will enter your last input automatically for you. The login data can be managed using a dialog in the config menu where that you can see all the stored login data and can delete entries. This is what I want. It is actually so useful that I prefer to start Mozilla only to visit EBay, even if Konqueror is already open, because it is so much more convenient."
    author: "Tim Jansen"
  - subject: "Re: Offer: Konqueror Password Manager"
    date: 2001-09-07
    body: "I realize what I wrote down is more than you asked for, but if I were to write a  wallet/password manager I would prefer doing it right and personally I would require all of those features. Possibly as a non-KDE engine with a KDE front-end which allows for different front-ends.\n\nOf course web forms would probably be the place to start.\n\nIdeally, this would be a Hailstorm alternative on the client side. No need to share login information on the server side if your authorization methods are well organized and managable on the client side. You probably have different keys for work, home, your car as well: no problem as long as it's easy for you to use them. Same thing on the Internet, no?"
    author: "Rob Kaper"
  - subject: "Re: Offer: Konqueror Password Manager"
    date: 2001-09-09
    body: "This sounds very interesting. I'd like to suggest that you talk about this on both the KDE _and_ GNOME lists when you find the time to implement it."
    author: "Evandro"
  - subject: "Re: Offer: Konqueror Password Manager"
    date: 2001-09-09
    body: "I thought the real alternative to Hailstorm on the client \nside was client side certificates.  Non? \n\nRob"
    author: "Rob"
  - subject: "Re: Offer: Konqueror Password Manager"
    date: 2004-11-23
    body: "Please what is the password for the konqueror console file manager.\n\nThanks."
    author: "Omotunde Olorunwunmi"
  - subject: "Pay per feature"
    date: 2001-09-07
    body: "How about setting up a general pay-per-feature website which allow people to bid on features they would like to see. The hard part is probably to make the offers binding, so people dont misuse the system and never pay.\n\nOf course general improvements like speed and security should also get honored somehow."
    author: "srb"
  - subject: "Users opinion must be included in KDE's developmen"
    date: 2001-09-07
    body: "The Pay-per-feature is a nice Idea but this Idea comes why? Because Our KDE team is ignoring Users' feature requests in the name of feature-freeze :( \n\nI would ask KDE Team or Dot.kde.org's maintainer to do a daily/weekly feature Poll which we see at bugs.kde.org as wishlists and also allow kde users or members (how about a free kde membership, after registration at kde.org?) to present and vote for features. For example,\n\n----\nPoll No    : 1\t\nApplication: Kicker\nFeature    : Keybindings in Kicker\n\nDescription: \nKicker would be very easy to navigate with Keyboard when we can use Windows like keyboard navigation. e.g., in windows to open wordpad, I type Start->P->A->W (where P, A, W, stands for Programs, Accessories, and WordPad respectively), and so I want this feature in kicker which allow me to open/run any submenu or applnk when I press the first-character of the submenu/app name. e.g., K (Alt+F1)->E (editors)-> T (text editor).\n\nAdvantage/Need: I prefer keyboard use to mouse as I am much used to it in Windows.\n\nWould you like to see the above feature integrated in next KDE version? Options are:\n\n( ) Yes.\n( ) No.\n(*) I don't know.\n\nIf the Yes votes are more than No votes then this feature will be included in next KDE version. Thanks for your vote!\n-------\nHere are some more features I can think now:\n\n1. Integrating Thememanager, Icon Themes, SoundThemes, Background settings.\n2. Themes should also get individual fonts and mouse cursors\n3. Size of Kicker should be small at 800x600\n4. Konqueror should connect to internet via kppp if it is not connected\n5. KDE specific System and Network Utilities irrespective of OS (for consistency on any platform)\n6. Like Konsole all apps should allow scrollbar Left/Right/Hide option, this will help left handed people. (though I am right-handed :)\n7. KDE should release only stable apps in final releases (noatun in KDE 2.0 was broken, kpackage in KDE 2.1.1 shows error messages for installing)\n\nThe above wishlist I submitted when KDE 2.0 was the Latest Desktop environment. But still in KDE 2.2, I can't see this feature and I feel annoyed to use Kicker by using up and down arrow keys. And bugs.kde.org has got tired of my wishlists and said \"why don't you stop bugging bugs.kde.org? it is not a discussion forum!\".\n\nI feel that KDE developers want to incorporate feature which they like only, they don't care for User's wishes and needs. So, I wish that dot.kde.org should daily/weekly conduct feature request polls, and see what really KDE users want. And the very nice they can say it \"Get the sources and make it yourself! don't bother us, we are too busy with _bugs_\".\n\nAnyway, Thank you KDE Team for everything you have created."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Users opinion must be included in KDE's developmen"
    date: 2001-09-07
    body: "No, the idea comes because it is probably the only known way to finance open-source work. All the companies that have failed so far tried to sell something that is free(-beer) or tried to finance open-source work with things that are only related to the development itself (like support or training). But if it is the progress of the software that costs money, then people should pay for the progress. \nBeside that I have problems demanding from people to implement the features that I want, especially if they do it as a hobby. Paying for it helps :)"
    author: "Tim Jansen"
  - subject: "Paypal & Opensource"
    date: 2001-09-07
    body: "Hi,\n\nThis is something I submitted to another site, since it wasn't posted I didn't receive any comments. Anyway I had the same idea as Tim:\n\n<quote>\n\nUsing paypal in opensource development\n\nLately I've been thinking about using systems like paypal in opensource development. As a user I would find it very interesting if I could somehow sponsor the development of programs in a certain way. For example I would love to attach a payment (using paypal) to a certain feature or bugfix request, and afcourse other users could also do the same, which would hopefully amount in a nice pile of cash the developer could use to have some fun with :) However it strikes me as odd that a simple idea like this has never been done before. Would this somehow make programming  on opensource programs less fun ? Is this a good way to make money out of open source ? Are developers just doing it for the kick or itch ? I'd love to hear what people think about this, especially developers.\n\n</unquote>"
    author: "frido"
  - subject: "Interesting!"
    date: 2001-09-07
    body: "This is really interesting! A question is how this can be done in a organized and maybe systemated way. This discussion then leads in why/how open source/Free Software can be supported with money in a more direct way than buying a distro which supports your favourite project (in this case KDE or a KDE application).\n\nWhy not post your thoughts to kde-promo@kde.org, kde-devel@kde.org or maybe write an article and post it to kde-zine@kde.org?"
    author: "Loranga"
  - subject: "Re: Paypal & Opensource"
    date: 2001-09-08
    body: "What's to stop BigCompany from pouring thousands of dollars (so to speak) into feature request X, when I just gave $100 to implement feature Y? What if X and Y are mutually exclusive?\n\nad."
    author: "Alexander Deruwe"
  - subject: "Re: Paypal & Opensource"
    date: 2001-09-08
    body: "I don't know if you have used Java Developer Connection? Anyway, if you register there you can browse through all reported Java bugs and also report your own. You also have 3 votes that you can put on the bugs that bother you the most. There is a top-25-bug-list and also a top-25-RFE-list where you can see the features and bugs that most Java developers want to have fixed.\n\nI wonder if not something like this could make sense with KDE. But instead of voting for bugs you offer money for them to be fixed, or for features, and many other users could do the same. I think the biggest problem would be when to consider a bug to be fixed, and who fixed it."
    author: "Claes"
  - subject: "Re: Users opinion must be included in KDE's developmen"
    date: 2001-09-07
    body: "> Because Our KDE team is ignoring Users' feature requests in the name of \n> feature-freeze.\n\nSorry, but this is a big difference between Open Source and proprietary, commercial software.  The KDE team's purpose isn't to make you happy, it's to make a product that they like and are proud of.  If there's something that you really want, it's your responcibility, not the KDE team's.  This thread really eximplifies that if you can't commit time to it, commit money.  \n\nThis isn't to say that your feedback isn't at times appreciated, but if they don't want to implement something, they're not going to.  Open Source always requires a developer to have some motivation to do something.  Like when something really bugs you, or there's a feature or program that just doesn't exist, you go after it."
    author: "Scott"
  - subject: "Re: Users opinion must be included in KDE's developmen"
    date: 2001-09-08
    body: "I don't know why your so fired up about being able to do the fancy ALT<F1> whatever just to launch a program. I found that with ALT<F2> and completion turned on I can run any program on the path without touching the mouse and it's faster the second/third  time as usually all I need is 2-3 chars and the listbox \"finds\" the correct command a quick down arrow and enter and I'm off to the races. \nIf you want a feature bad enough then learn to code and maybe you will find out that it might be more trouble implementating your feature than the few keystrokes you supposedly save.\n\nAll spelling mistakes are left as is for the anal-retentive."
    author: "Scott Manson"
  - subject: "Re: Users opinion must be included in KDE's developmen"
    date: 2001-09-13
    body: ">Kicker would be very easy to navigate with Keyboard when we can use Windows \n> like keyboard navigation. e.g., in windows to open wordpad, I type \n> Start->P->A->W (where P, A, W, stands for Programs, Accessories, and WordPad \n> respectively), and so I want this feature in kicker which allow me to \n> open/run any submenu or applnk when I press the first-character of the \n> submenu/app name. e.g., K (Alt+F1)->E (editors)-> T (text editor).\n\nYou can probably do this yourself by adding an ampersand ('&') before the character that you want to become the accelerator into those .desktop files below share/applnk. Or use the menueditor for that."
    author: "Carsten Pfeiffer"
  - subject: "KMail feature contest"
    date: 2001-09-07
    body: "Well up to now, nobody seemed to be really interested in these 350$. At least I haven't heard of anyone.\nJust independant of this contest someone showed up, who wanted to implement manual and interval mailchecking, but I haven't seen a patch yet and don't know, if he still works on that.\nCopying, moving mails to from and between IMAP folders as also requested as features definitely worked well in the released version, btw.\nLarge e-mail folders are already handled more effective in the CVS version due to permanent caching, but initial header retrieving is still at bit slow on a fast network.\nI do of course continue to work on IMAP myself, but not always with top priority. I haven't done anything to it in the last two weeks though."
    author: "Michael H\u00e4ckel"
  - subject: "IMAP support"
    date: 2001-09-08
    body: "Thank Michael for IMAP support. For mail notification of my IMAP folders I use Kurts KBiff which is great too, but it lacks the support of secure-IMAP (IMAPS). One thing that should be improved in current implementation is the speed of deleting messages. If I make my monthly mailinglist clean-up, KMail needs minutes to do this.\n\nOf course, I would like to do the clean-up by KMail filter setup and I would like to have smtp-authentification and language settings as part of the profile and free names for profiles, so that \"default\" is an attribute and not the name of a profile and ....\n\nUsers are so dissatisfied :-)))\n\n--\nKDE is great!"
    author: "Thorsten Schnebeck"
  - subject: "Re: IMAP support"
    date: 2001-09-08
    body: "Well the whole issue with missing features sometimes appears to be:\nIf there are 10000 users and every user has two wishes, that makes at least 15000 different missing features :-)\nI think, KMail is already in a state, where it it completely sufficiant for many people and most people use only a very small part of its capabilities.\nEspecially the whole IMAP support is alredy something, that isn't used at all by probably at least 95% of the users.\nBTW: We do now finally have SMTP authentication, at least in CVS."
    author: "Michael H\u00e4ckel"
  - subject: "Re: IMAP support"
    date: 2001-09-09
    body: "You might think that IMAP is not important, but it is. Nowdays most of the users\nare home users using ISP provided mail spools and thus they need POP.\n\nKDE's next field to conquer is corporate networks. In these environments\npeople might need to change their workplace/computers and they use laptops - \nin office as well when mobile. Thus local storage with POP is simply not possible.\n(without talking about centralized backups etc etc)\n\nAnd when thinking what is keeping KDE away from enterprise networks is lack of\noffice productivity applications. Wordprosessors, spreadshees, presentation and\nvector drawing programs are behind the corner, as is email client supporting IMAP.\nBut we still need teamworking applications like shared, centralized database based\ncalendar software (korganizer is good, I use it :-), centralized contact information/addressbook databases and other teamwork tools. \n\nI know that there is not much intrest in these topics in open souce developer community,\nbut it's very far when someone releases commercial alternatives. And without these tools,\nenterprise desktops will remain controlled by EvilEmpire(TM). \n\nSo, IMAP is only on brick in the wall, but very important one.\n\nFeel free to comment if you disagree. \n\nBR,\n\nJuha"
    author: "Juha Tuomala"
  - subject: "KDE Color Picker"
    date: 2001-09-07
    body: "Is there any KDE application that allows to pick a color from any visible window or the desktop background and display the rgb values of that color? Would be pretty useful as at the moment I always start pixie for that, create a screenshot and pick the color from the resulting image file."
    author: "Anonymous"
  - subject: "Re: KDE Color Picker"
    date: 2001-09-07
    body: "There is a panel applet called, fair enough, Colour Picker that does what you are seeking for."
    author: "anonymous"
  - subject: "Re: KDE Color Picker"
    date: 2001-09-07
    body: "Yep, this is what I use. It even copies values to the clipboard :)"
    author: "Mosfet"
  - subject: "Re: KDE Color Picker"
    date: 2001-09-07
    body: "KRuler does that, and I think any app with a color picker dialog would work too, because there is a \"dropper\" tool that you can pick up colors from the screen with (very useful :-)."
    author: "not me"
  - subject: "we need"
    date: 2001-09-07
    body: "an application to build a theme,like the app that Xdesktop has built. i mean, a wizard that only has to ask about the colors or the pixmaps you want for the different parts of the KDE GUI (toolbar, buttons, etc...). Then, the results will be into an XML to create the KDE 2.x theme. I think it is easy for an KDE coder expert. We need it."
    author: "Daniel"
  - subject: "kde themes where is the up to date info"
    date: 2001-09-07
    body: "Well I would love to make a theme.\n\nBut I'm blowed if I know where the info for doing it is.\nMosfet had a howto but thats gone.\n\nThe IBM tutorial is also out of date ( or was when I looked )\n\nanyone got any info ?"
    author: "dids"
  - subject: "Re: kde themes where is the up to date info"
    date: 2001-09-07
    body: "I don't know if Mosfet's widget theme tutorial is still accessible from his site.  Luckily, though, I have a copy of it lying around.  Here it is, in .zip format!"
    author: "not me"
  - subject: "Re: kde themes where is the up to date info"
    date: 2001-09-07
    body: "Thanks !! I see what I will be reading tomorrow."
    author: "dids"
  - subject: "Re: kde themes where is the up to date info"
    date: 2001-09-08
    body: "That tutorial is not GPL'd so you can't distribute it freely!"
    author: "not_mosfet"
  - subject: "Re: kde themes where is the up to date info"
    date: 2001-09-08
    body: "On the contrary: (it has a much better license than GPL)\n\nLicense\nThis document is entirely public domain with absolutely no arbitrary restrictions on it's distribution. It may be freely distributed and enhanced at will."
    author: "reihal"
  - subject: "THANK YOU!"
    date: 2001-09-08
    body: "Thank you much!!! I have been looking for  that\nor something similar for a while now, i already have \ntwo theme ideas i am working on but couldn't figure out\nhow to implement them correctly.  \n\nhopefully this will help!\n\nMaybe we should start our own kde themes website?\nespecially since themes.org seems to be losing it\n<themes.kde.org>???\n\n-Brad\nhttp://arctic-circle.wso.net"
    author: "Brad C."
  - subject: "Re: THANK YOU!"
    date: 2001-09-08
    body: "t.o is coming back anyday now. If you want to start because you think it\u00b4s dead I urge you to contact them.\n\nIf you have other reasons, well, good luck with your project! ;)"
    author: "Evandro"
  - subject: "KDE Themes"
    date: 2001-09-08
    body: "Hear hear, I'd love to see some new KDE themes... for some reason there seems to be some obsession with Aqua. I must be the only one but personally I absolutely hate Aqua. When I use GNOME I usually use the BlueMarble GTK+ theme and BlueSteel Enlightenment theme and it looks absolutely amazing. KDE may kick the living bejesus out of GNOME in terms of architecture but it's still a hell of a lot uglier. And don't tell me to use GTK+ import, I tried that but the colour settings are not fine-grained enough to let me match it up perfectly - either the menu dividers look ugly or main windows look ugly. That and some bits don't quite match up; I'll post a screenshot if anyone's interested."
    author: "Somebody"
  - subject: "Re: KDE Themes"
    date: 2001-09-08
    body: "/me is interested. let s c what u got."
    author: "ElveOfLight"
  - subject: "Re: KDE Themes (new theme)"
    date: 2001-09-10
    body: "I have made a new theme based on my existing RISC OS theme in KDE 2.2. I've made it IMHO a lot cleaner and have changed the scrollbars (*cough* Mozilla *cough*).\n\nTake a look at http://www.srcf.ucam.org/~rjw57/screenie.jpg or http://www.srcf.ucam.org/~rjw57/screenie.png and tell me what you think.\n\nIt can be downloaded from http://www.srcf.ucam.org/~rjw57/cleannsimple.tar.gz (simply untar in ~/.kde/share/apps/kstyle or /usr/share/apps/kstyle ).\n\nRich"
    author: "Rich"
  - subject: "Kinkatta"
    date: 2001-09-08
    body: "Yah, Finally it is on the dot.  Before i put the new release up I was getting several e-mails a day!  It was crazy.  Oh hey for anyone in the rochester Area I am going to be having a 1.0 party in a few weeks.  Send a e-mail to icefox@mediaone.net if you are interested.\n\n-Benjamin Meyer"
    author: "Benjamin C Meyer"
---
Some time ago, <a href="mailto:bowie23@excite.com">Bowie J. Poag</a> wrote in with a link to the <a href="http://system26.com">System 26 GUI Component Stockpile</a>,  a large public repository of raw materials such as individual sounds (clicks, beeps, bells and whistles) and images (buttons, textures, pixmaps, etc) specifically geared for use within GUIs.  This may to be a good place to start if one were building a new KDE theme.  And speaking of themes, <a href="http://kde.themes.org/">kde.themes.org</a> seems to be back although there does not appear to be any recent submissions.  Are KDE users simply not interested in themes these days?  Also a while back, <a href="rsk@u.arizona.edu">rsk</a> wrote in with this news: <i>"Wow, it looks like the same guy that offered up all the suggestions for integration of the KDE PIM features has put his money where his mouth is. He is offering $350 and any product from <a href="http://www.cafepress.com/cp/store/store.aspx?storeid=linuxports">Tux Store</a> to the developer(s) that implements the features he's asked for in his story."</i>  I'm unaware of the current status of this contest, but any KDE developer interested can check out <a href="http://www.linuxports.com/entry.lxp?lxpe=106">the full offer</a> and contact <a href="mailto:jd.nospam@commandprompt.com">the author</a> directly for details</a>.  Finally, Robin Sharf wrote in to inform us of <a href="http://www.bebits.com/app/2422/">a port of Konqueror/Embedded to BeOS</a> (with <a href="http://members.optushome.com.au/beoz/konqueror.jpg">screenshot</A>). <b>Update: 09/07 6:00 PM</b> by <a href="mailto:navindra@kde.org">N</a>: Also managed to miss this one: The first releases (since the AOL fiasco) of <a href="http://kinkatta.sourceforge.net/">Kinkatta</a> are out.  Go to the page for a bigger update and the full skinny.  On the plus side, KDE support is (literally) <a href="http://kinkatta.sourceforge.net/screenshots.shtml">looking good</a> but Benjamin would like to step down as maintainer and is seeking someone to fill in those shoes.
<!--break-->
