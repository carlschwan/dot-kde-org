---
title: "Kernel Cousin KDE #17"
date:    2001-07-17
authors:
  - "numanee"
slug:    kernel-cousin-kde-17
comments:
  - subject: "PrtScr"
    date: 2001-07-17
    body: "One addition about the \"PrtScr\" topic:\n\nKWord (CVS) can handle this very well. Just press <PrtScr>, change to KWord and do \"edit->paste\" ( <ctrl>+v )\n\nVery nice if you want to write KDE-reviews ;-)\n\nBye\n\nThorsten"
    author: "Thorsten Schnebeck"
  - subject: "KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "One interesting thread on kde-core-devel that wasn't covered in this issue (presumably because the topic is still being debated) is whether the next KDE release should be based on the current Qt (KDE 2.3) or be based on the new Qt 3.0 (KDE 3.0).\n\nQt 3.0 is still in beta, but it would very likely be complete before KDE 3.0 would be ready.\n\nhttp://lists.kde.org/?t=99502854200004&w=2&r=1&n=53\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "not only is it still being debated, but it started after the deadline for a thread making it into that week's KC KDE.\n\nthat said, it is already slated to be covered in issue #18."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "Perhaps, the KDE developers might want to wait for QT 3.0.1 or somesuch before beginning the porting process for KDE 3. This way we'll be able to work around any post-last-minute changes in the way QT works."
    author: "Carbon"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "And what about the new gcc version?\nHas anybody tried to compile KDE against gcc 3 and experienced some speed improvements?\nI'd really like to see a FAST FAST FAST KDE!\nKDE rocks!"
    author: "lanbo"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "It currently cannot compile everything\nI think aRts or something like that doenst compile\nPeople are working on it"
    author: "ac"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-19
    body: "i compiled qt and kde 2.2beta1 with gcc-3.0 using the -O3 switch. but it is not much faster! the main problem is still the linker and not the compiler!"
    author: "gnu128"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "By the way: is it gcc a good compiler?\nHas anybody tried another one?\nI'm a complete ignorant in this subject. Just curious about that."
    author: "lanbo"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "Gcc is a good compiler but rather slow when it comes to compile C++ code.  It just my opinion."
    author: "Ups"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "> By the way: is it gcc a good compiler?\n\ndepends on what you define as \"good\" compiler:\n\nsupports a wide range of platforms: yes\ncan be used as crosscompiler: yes\ncompiles fast: no\ncreated executable is fast: no\n\n(all this IMHO of course)\n\n> Has anybody tried another one?\n\nI am afraid there is no other one :-(\nAs far as I know Intel is working on an optimized compiler (means a compiler which produces very good (read: fast) executables). This compiler is not yet ready but if it is (if i remember correctly it will not take them much more time) KDE shoule profit from that.\n\n\n> I'm a complete ignorant in this subject. \n\nNo problem. Hope the above helps a bit."
    author: "Marco Krohn"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "As far as I know KAI C++ is probably the best compiler in terms of optimizations (produces much faster code than gcc or eg. MS Visual C++) and it is available for LINUX (you can get a demo from http://www.kai.com)."
    author: "Felix E. Klee"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-19
    body: "> compiles fast: no\n\nTrue for C++.\n\n> created executable is fast: no\n\nNot true."
    author: "Carlos Rodrigues"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-19
    body: ">>> > created executable is fast: no\n>>> Not true.\n\nUmm, the original poster didn't state that very well; \"fast\" is a subjective term.  Now it is certainly true that there are certain platform/compiler/program combinations where the native compiler will spew faster code than gcc.  In some cases \"noticeably\" faster.  (the opposite is true as well, I'm sure)\n\nOne thing anybody (I think) forgot to mention was standards compliance.  gcc 3 is about as compliant as you can get.  Many other compilers are just out-of-date, and some well-known ones purposefully won't fix their behavior because it would break the programs they compile.\n\nI believe mozilla's coding standards are a good example of the hoops they have to jump through to get it to build on all the compilers out there."
    author: "blandry"
  - subject: "Compiler from Sun Solaris?"
    date: 2001-07-18
    body: "I run KDE compiled with gcc on a Solaris/Sparc machine. Why can't I use the Sun compilers instead of gcc?"
    author: "Loranga"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "Anyone who knows something about Intels compiler?"
    author: "reihal"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "Source compatibility is kept between all versions of Qt within a give major (e.g. 1.x, 2.x or 3.x). So porting to 3.0 won't leave anything broken in the face of 3.0.1, and a 3.0.1 would most likely simply be bugfixes within Qt and not an actual functionality upgrade/change.\n\nQt will be (already is?) frozen before the 3.0 release, so writing for 3.0 even before it is released will be safe. There shouldn't be any major changes this late in the game that would give pause to using it."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "Hey, why not keep it simple?  Release KDE 3.0 earlier that it was planned.  Let it use whatever version of QT 3.x is ripe at the time.  Compile it with GCC 3.x and updated gnu libraries.  I would suggest that in tandem a new linker be developed and used.  After KWord gets up to speed, so to speak, KDE's next frontier will be SPEED.  We have seen from earlier analysis that problems with subclassing and the current linker add time and bloat to the code.  I suggest that KDE 3.0 is the time to attack it.  I said release 3.0 earlier than planned, because I assume that the developers probably have more grandiose plans than these -- but I don't know.\n\nAny comments?"
    author: "Ross Baker"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "gcc and the gnu linker are not part of the KDE project, though the experiences from the KDE project using them have provided lots of positive feedback to those who work on those tools. gcc3 can already compile most of kde2, though some parts break (e.g. aRts). distros probably won't be shipping a gcc3 based OS until next year, anyways...\n\nthe big issue seems to surround the life-span (or lack thereof) of kde2. it hasn't been out that long, due to the time it took to go from 1.x to 2.x and now there is discussion of moving on to KDE3.x which will break binary and (to a small extent) source, compatibility. much of the current debate centers around this point, since an earlier KDE3 makes for a fairly short KDE2 but a potentially long lived KDE3, while a later KDE3 extends the life of KDE2 but shortens KDE3's."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "Yes, I understand.  All I'm suggesting is that all the binary and source incompatibilities be bunched together synchronous with KDE 3.0.  I see too many breakpoints coming up -- Qt 3.x, KDE 3.0, GCC 3.x, compatible gnu libraries for GCC 3.x, and the very important gnu linker efficiency deficiencies.  I would suggest bringing out KDE 3.0 when each and every one of these can be addressed simultaneously. I know that there is not one group controlling this, but some sort of co-operation could reap huge dividends in ease of transition!  I think the KDE folk ought to get involved with the linker folk.  For one thing, the GNOME people don't care about the outcome nearly so much as the KDEers -- they use C not C++.  The linker inefficiency is possibly even a BENEFIT to their project since it is an impediment to KDE.  I am not against the GNOME project, but I prefer KDE."
    author: "Ross Baker"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "I don't think so. Most of the GNOME developers don't care which projects becomes the \"winner\" of the desktop war (the desktop war doesn't exists anyway: it's made up by trolls with too much spare time).\nThey just want the best desktop environment in their vision and goal."
    author: "dc"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "Well... they better start coding for kde then.\n(Sorry... couldn't resist :7)"
    author: "me"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "In this linux world nobody cares about binary compatibility.\nIn windows everybody would complain but here you ugrade everything with some CD from a magazine and that's all. This is what I think.\nAnd the next thing to do is (as everybody agrees( the speed.\nCome on KDE!"
    author: "lanbo"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "i think you're wrong.\n\neg: redhat gives 18 months of BC (x.0 - x.2). also, in the current redhat release you'll find libraries to run apps back from redhat 5.x."
    author: "Carg"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "I dont think this( 5.x binary compatibility ) counts in C++ apps since the ABI has changed since 8.0, and will change again as soon as gcc3.0 comes ( 3.1 again? ).\n\nBC does play a role for closed source apps, which some hope, think, whish (etc) are necessary for linux and KDE.\n\nWhile I think that KDE is really good, I wonder if the developers really think that its perfect. \n\nIs a simple QT3 port really enough for 3.0?\nIsnt there any idea out there which would require major architectural changes, but which is really worth implementing?\nShouldnt they all be put together for a weekend, and have a brainstorm session about features which should be new in KDE3 and which would bring us, and them something revolutionary ( like the kparts for 2.0 )\n\nWhat about .net. It may be hype, but can KDE collaborate with .net applications?\nWhat about a logging function for all aplications,  which builds a aoutomatic working diary.\nOr a scripting function, etc.\n\nI am sure, there are other things out there, and I would be amazed, if the KDE framework wouldnt have to be changed a little bit.\n\nThe step to KDE3 sould be made thoughtfull, but then giving advice from outside is always cheap."
    author: "ac"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "They should try to develop a platform-independent binary format. \n\nWether they just use the .NET runtime on linux or develop an independent bytecode is not that important. But if a .NET runtime becomes available with an acceptable license I see no harm in using it. Especially since C# is in some ways very similar to the beefed up C++ with moc that KDE uses. \n\nAnother nice thing would be the option to use KDE without X. I know that many people use the networking features of X, but I don't, and having out of process frame buffer access like in X is a huge performance hog in a local setup."
    author: "R\u00fcdiger Klaehn"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-19
    body: "Oh hell.\n\nWe develop a Desktop Environment, not an operating system.  If you want a platform independent binary format, consider using java, or consider even porting Qt/KDE to another platform and using our javaqt and javakde bindings instead of Swing.  All we want to do is make a desktop environment!"
    author: "Charles Samuels"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "[OFFTOPIC]\nWhat is BC anyway? When I compile gnome-libs, configure checks for BC and always says that it isn't installed. Then it tells you that BC is included in UNIX by default for centuries now and that you should tell your distributor to wake up and include it. ;-)\n[/OFFTOPIC]"
    author: "dc"
  - subject: "Re: What is BC/bc?"
    date: 2001-07-18
    body: "In the conversation above, \"BC\" is \"Binary Compatibility\".\n\nThe \"bc\" that UNIX has had for centuries is \"bc - and arbitrary precision calculator language\".\n\nIn other words it is a command line calculator. See also \"dc\" - which is similar but uses Reverse Polish Notation (RPN).\n\nI suppose you could say that \"UNIX has had bc since about 1 BC but that has nothing to do with BC\" which would really clarify things.\n\nHope that helps ;-)."
    author: "Corba the Geek"
  - subject: "Re: KDE 2.3 or 3.0?"
    date: 2001-07-18
    body: "i perhaps should've put more emphasis on the source compatibility side of things. some KDE developers are worried that introducing source incompatibility so quickly ater 2.x might discourage application developers.\n\npersonnaly, i'm all for a quick KDE3 which will extend its potential life as well as allow the KDE team to fix/correct/change some things that simply can't be done in a binary and source compatible way."
    author: "Aaron J. Seigo"
  - subject: "Re: Kernel Cousin KDE #17"
    date: 2001-07-18
    body: "I think the coders have to work on this:\n\nhttp://www.suse.de/~bastian/Export/linking.txt\n\n\nthe problem is the dinamic linker, but this is not job for the KDE team."
    author: "Daniel"
  - subject: "Re: Kernel Cousin KDE #17"
    date: 2001-07-19
    body: "Nothing prevents some of the KDE people to contribute to the dynamic linker, especially if that helps KDE."
    author: "Carlos Rodrigues"
  - subject: "KDE doesn't have a VCD player for its users :("
    date: 2001-07-19
    body: "I just Miss a KDE based VCD player; ZZplayer seems to have died and noatun, kaiman, aktion, etc., don't even care for VCD playing capability.\n\nWhy is it that KDE doesn't include a VCD player in in KDEMultimedia Package? Using Xine and ZZplayer sources it could be created within a week by any hardcore KDE programmer/Developer ;)\n\nHope I could see a VCD playing application in KDEMultimedia package in KDE 2.2. (optimistic:)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Kernel Cousin KDE #17"
    date: 2001-07-19
    body: "Start with mplayer and ffmpeg sources and add a KDE-gui similar (but better) to MS Mediaplayer 6.4!\nThen we would have a great videoplayer with support for AVI, ASF, MPG, WMV with almost every codec there is! ffmpeg is a GPL C-reimplementation of DivX;-), Intel Indeo 5 and many other codecs, so now even us non-x86ers (I'm an Alphaian:) can watch our daily dose of pornclips... :-)  mplayer uses Windows-DLLs to decode the videos and is much more stable than aviplay (avifile also contains videoediting and other stuff though).\n\nThere is a lot of working code out there that is just waiting to get a nice KDE-GUI!"
    author: "Per Wigren"
  - subject: "Re: Kernel Cousin KDE #17"
    date: 2001-07-19
    body: "Parent was supposed to be a reply to \"KDE doesn't have a VCD player for its users :(\", but what the heck.."
    author: "Per Wigren"
  - subject: "Re: Kernel Cousin KDE #17"
    date: 2004-01-15
    body: "good"
    author: "ben obrien"
---
KC KDE is back this week with a <a href="http://kt.zork.net/kde/kde20010713_17.html">double issue</a> spanning the last 14 days.  Read about video embedding in Konqi, KOffice news and plans, a new proxy configuration GUI (complete with screenshots), and much more.  Thanks, Aaron.