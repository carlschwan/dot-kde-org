---
title: "People of KDE: Meni Livne"
date:    2001-12-24
authors:
  - "wbastian"
slug:    people-kde-meni-livne
comments:
  - subject: "Go Tink, Go!"
    date: 2001-12-24
    body: "This People of KDE series is really going well. Congrats to Tink for doing such\na good job managing it all. The new interview questions are quite good, \nthough I'm still a little skeptical if anyone will respond that a cutting edge\nKDE really did make them bleed :-)"
    author: "Carbon"
  - subject: ":-("
    date: 2001-12-25
    body: "What about the palestinians... Anyway, you done a great job, and just to let you Jews know I'm siding you guys"
    author: "scared to bits"
  - subject: ">:-/"
    date: 2001-12-25
    body: "What does it have to do with anything?"
    author: "?"
  - subject: "Re: :-("
    date: 2001-12-25
    body: "uhh? this has what to do with either his interview or kde in general?\n\nfor what you know, he could be a palestinian :)"
    author: "gt"
  - subject: "Re: :-("
    date: 2001-12-26
    body: "Excuse me, but what _you_ know? \"All those Orientals are all very similar, I can never tell Jew from Arab\"? Palestinian he is certainly not. Palestinian, in the first place, would NEVER act as Hebrew KDE translation maintainer."
    author: "Alex"
  - subject: "Re: :-("
    date: 2001-12-28
    body: ">Palestinian he is certainly not. in the first place, would NEVER act as Hebrew KDE translation maintainer.\n\nUhm, I reiterate that it doesn't matter anyways...but.. you still can't generalize like that. Many Palestinians know Hebrew too (from the necessity of jobs, mainly)."
    author: "gt"
  - subject: "Re: :-("
    date: 2001-12-29
    body: "It matters sometimes... I didn't ever say that Palestinians don't know Hebrew - many of them do, and even better than Israelis. I just said that Palestinian would most probably not ( if you don't like the word 'never') act as Hebrew translation maintainer - mostly of ideological reasons."
    author: "Alex"
  - subject: "Re: :-("
    date: 2001-12-29
    body: "I would figure that they would not translate programs to Hebrew since the Israeli army insists on rolling in to their neighborhoods and sniping people at random.  It seems like a good reason to me.  I would not translate programs to the native tongue of whatever group is occupying my country either."
    author: "illya_the_great"
  - subject: "Re: :-("
    date: 2001-12-30
    body: "Excuse me, but I felt compelled to correct the errors expressed in the above comment. \n\nIsrael fights terrorism by eliminating terrorists before they have a chance to strike. Specifically, Israeli snipers target bomb makers,terroristorganizational leaders, terrorist tactical leaders, terrorist infrastructure, and would-be terrorist suicide bombers. These are not \"random people.\" The Palestinian terrorist groups target random Israelis by detonating themselves in packed Israeli districts. Palestinian terrorists do not CARE whom they hit -- this includes Israeli arabs who may just be shopping in downtown Jerusalem when they are blown to bits along with twenty other Jewish kids. To say that Israelis are \"rolling in\" and \"sniping people at random\" is plain incorrect. \n\nAdditionally, to state that Israelis occupy any Palestinian's \"country\" is similarly incorrect and inciteful.  What Palestinian land do Israelis now occupy? The West Bank and Gaza?  or just everything between Lake Kineret and the Mediterranean sea?  To argue that Israelis occupy any Palestinian land not already under the control of the Palestinian Authority implies that Israelis do not belong in their own country -- and this flies in the face of Israel's established right to exist. The whole concept is rendered irrelevant considering that there is technically no Palestinian \"country\". \n\nPerhaps this kind of warfare has (hopefully, G-d forbid) not happened in your country, and you therefore can not relate. \nI encourage you to read your history so that you can post opinions that are rooted in fact, and not in what is spoon fed to us by CNN and BBC et al."
    author: "Betzalel Ben-Tzion"
  - subject: "Re: :-("
    date: 2001-12-30
    body: "According to some spelling nuances in the nick, it seems to me that this particular guy you answered to is spoonfed not by CNN or BBC, but by Russian propaganda machine. And this country is well known for dual morality - they conduct anti-terrorist war in Chechnya to defend themselves,  and they blame Israel for conducting anti-terrorist war to defend themselves. It's all a matter of perception, though - Chechnya is not perceived by the world as \"occupied country fighting for independence\" ( in spite of that it is exactly this in Chechens' perception), and therefore Chechnya war is an \"internal affair\" of Russia."
    author: "Alex"
  - subject: "Re: :-("
    date: 2002-01-02
    body: "I am not Russian, I live in the United States.  I can speak some Russian, though.\n\n>>Alex:\"According to some spelling nuances in the nick, it seems to me that this particular guy you answered to is spoonfed not by CNN or BBC, but by Russian propaganda machine. And this country is well known for dual morality - they conduct anti-terrorist war in Chechnya to defend themselves, and they blame Israel for conducting anti-terrorist war to defend themselves.\"\n\nOkay you seem to be too much of a jingoist.  Do you attack what you think the nationality is of anyone who makes a comment or criticism of Israel?  You seem to, because you think I am Russian, seek to discredit me by ad hominem attacking me for what my nation supposedly does.  That is not how arguments are done.  You address my argument and not wherever you happen to think I live.\n\n>>Alex: \"It's all a matter of perception, though - Chechnya is not perceived by the world as \"occupied country fighting for independence\" ( in spite of that it is exactly this in Chechens' perception), and therefore Chechnya war is an \"internal affair\" of Russia.\"\n\nYou are correct that terrorism is a relative term, but other than that you were inaccurate.  A good definition of terrorism as the medias and governments of the world use the word is:\n\nTerrorism- any group/nation/entity/etc. that your media and government see as against their interests that use violence.\n\nThe best example I can think of is, when the United States was the ally of Iraq, the PKK(Kurdish insurgents) were 'terrorists' in the American press.  When Iraq was the enemy of the United States these same 'terrorists' were portrayed by the press as 'freedom fighters.'   What caused this change?  Iraq went from being my nation's ally to our enemy.   When they were our ally you never heard about any of this \"Iraq has weapons of mass destruction,\" nonsense.  I do not hear, from my media, about how Israel and Turkey, our allies have even more 'weapons of mass destruction' than Iraq ever has possessed.  It seem that they do not care about nations having chemical weapons, using violence, unless they happen to be our enemy."
    author: "illya_the_great"
  - subject: "Re: :-("
    date: 2001-12-30
    body: "\"bomb makers,terroristorganizational leaders, terrorist tactical leaders\" are only a small fraction of a total number of casualties in this war. Demonstrators, often kids, who were throwing stones, are the rest, it didn't bring the order but rather raised the conflict. Israel de facto occupies the West Bank and Gaza, of course, but the problem is that the both nations belong to their own country ( this is the same country if you ask ), but they don't want to live together - not the first time in the history - it led to the tragic end many times ...\nI'm afraid that the both sides can't live in other way - they are addicted to it. Arm industry is the biggest part of Israel economy( about 20% IIRC ), military-industrial complex isn't interested in the peace, what will all those generals be doing after all ? Believe me I'm very far from conspiracy theories, but if you want to know how military-industrial complex works you may ask illia_the_great ( from Russia ) even if they have it much below 20% now.\n\nPS.\nIn United States you have a very small sample of this - energy industry and bush jr administration."
    author: "Me"
  - subject: "Re: :-("
    date: 2001-12-30
    body: "Betzalel,\n\nClearly, not all Israelis are as enlightened as yourself. Maybe you have some education to do in your home country? I mean just look at what some of the less intelligent Israelis are getting up to - building websites like this:\n\nhttp://www.btselem.org/\n\nOf course, there are many Americans, some of whom have never been to Israel, who could use some education too:\n\nhttp://www.nimn.org/\n\nBut to convince the world that Israel is fighting terrorism, and not oppressing people who struggle to live in a land that Israel continues to occupy illegally, you'd have to enlighten the United Nations. Maybe you could start with that damn \"general assembly\" (G-d only knows what a waste of time some of those countries are) - they are just a huge mob of UN officials who read everything spoon fed to them from CNN, BBC et al. and end up writing trash like this:\n\nhttp://domino.un.org/UNISPAL.NSF/ec8db69f77e7a33e052567270057e591/33c13e085d306717852569570055cda8!OpenDocument\n\nAnd damn, they write a lot of it too! There's an entire _database_ of it!\n\nhttp://domino.un.org/UNISPAL.NSF?OpenDatabase\n\nWE MUST STOP THEM, BETZALEL!!"
    author: "Duraid Madina"
  - subject: "Re: :-("
    date: 2002-01-02
    body: "Betzalel Ben-Tzion>>\"The whole concept is rendered irrelevant considering that there is technically no Palestinian \"country\".\"\n\nThe Israeli government and media claims it wants 'peace'.  Apparently this invovles sending settlers into the Gaza Strip and West Bank to take even more Palestinian land and bringing in the army along with it to choke of these small strips from the rest of the world.  \n\n>>Betzalel Ben-Tzion:\"I encourage you to read your history \"\n\nBe forwith and honest.  Just say you want me to read zionist history.  The history that says it is the Jews divine right to occupy what they now call Israel.  I have heard that history, and it does not seem in touch with reality.  I have also heard about how the Jews were persecuted by Rome, the Russians, Spainards, Germans and they now deserve a homeland, even if that means violently displacing another group, the Palestinians.  It does not make sense, now does it?  The Jews are persecuted and it was wrong according to Zionist history, but the Jews as persecutors is right of Palestinians today, is somehow right?  So according to Zionist history, persecution is only wrong when committed against the Jews.  No thanks, I do not want to believe that revisionist history."
    author: "illya_the_great"
  - subject: "Re: :-("
    date: 2002-01-02
    body: "Betzalel Ben-Tzion>>\"The whole concept is rendered irrelevant considering that there is technically no Palestinian \"country\".\"\n\nThe Israeli government and media claims it wants 'peace'.  Apparently this invovles sending settlers into the Gaza Strip and West Bank to take even more Palestinian land and bringing in the army along with it to choke of these small strips from the rest of the world.  \n\n>>Betzalel Ben-Tzion:\"I encourage you to read your history \"\n\nBe forwith and honest.  Just say you want me to read zionist history.  The history that says it is the Jews divine right to occupy what they now call Israel.  I have heard that history, and it does not seem in touch with reality.  I have also heard about how the Jews were persecuted by Rome, the Russians, Spainards, Germans and they now deserve a homeland, even if that means violently displacing another group, the Palestinians.  It does not make sense, now does it?  The Jews are persecuted and it was wrong according to Zionist history, but the Jews as persecutors is right of Palestinians today, is somehow right?  So according to Zionist history, persecution is only wrong when committed against the Jews.  No thanks, I do not want to believe that revisionist history."
    author: "illya_the_great"
  - subject: "Re: :-("
    date: 2002-01-02
    body: "Betzalel Ben-Tzion>>\"The whole concept is rendered irrelevant considering that there is technically no Palestinian \"country\".\"\n\nThe Israeli government and media claims it wants 'peace'.  Apparently this invovles sending settlers into the Gaza Strip and West Bank to take even more Palestinian land and bringing in the army along with it to choke of these small strips from the rest of the world.  \n\n>>Betzalel Ben-Tzion:\"I encourage you to read your history \"\n\nBe forwith and honest.  Just say you want me to read zionist history.  The history that says it is the Jews divine right to occupy what they now call Israel.  I have heard that history, and it does not seem in touch with reality.  I have also heard about how the Jews were persecuted by Rome, the Russians, Spainards, Germans and they now deserve a homeland, even if that means violently displacing another group, the Palestinians.  It does not make sense, now does it?  The Jews are persecuted and it was wrong according to Zionist history, but the Jews as persecutors is right of Palestinians today, is somehow right?  So according to Zionist history, persecution is only wrong when committed against the Jews.  No thanks, I do not want to believe that revisionist history."
    author: "illya_the_great"
  - subject: "Re: :-("
    date: 2002-01-04
    body: "To answer both to your answer to me and to your answer to Betzalel:\nI happened to be born in former USSR, so I know their dual morality well. And don't attack you according to your nationality, both I and Betzalel just are sorry that people make their opinion about the world and Israel in particular according to CNN spoonfeeding or according to Russian propaganda.\nAnd once more, read the history - and read between the lines sometimes. Do you know that the name \"Palestine\" by itself was established by the Romans, according to the name of some tribe, which had been defeated and ceased to exist several centuries before Romans came?"
    author: "Alex"
  - subject: "Re: :-("
    date: 2001-12-26
    body: "It is not a football game, no sides to be fan of. We can only regret that there is no KDE Palestina site."
    author: "Me"
  - subject: "Focus follows mind"
    date: 2001-12-26
    body: "I tried the focus follows mind, but all I got was porn..."
    author: "HappyHolidays"
  - subject: "Racial slurs?"
    date: 2001-12-28
    body: "Excuse me, KDE is poised to be the best desktop environment for *nix and I am reading what appears to be a bunch of \"teen bantering\"? Get real poeple!"
    author: "Frank kInser"
  - subject: "Re: Racial slurs?"
    date: 2002-01-02
    body: "Please, don't gauge a project by the quality of the posts of some of it's\nusers. Otherwise, both Vim and Emacs would be a strange combination of\nthe ultimate editor which should be used by everyone in every field, and a\nblight upon the human reputation of which all copies should be destroyed\nwith no mention in the history books :-)\n\nBesides, the conversation is at least moderately on topic (just take a look\nat the article they're replying to, it's about a KDE translator)"
    author: "Carbon"
  - subject: "Re: Racial slurs?"
    date: 2002-02-17
    body: "Ass."
    author: "Sam"
  - subject: "Re: Racial slurs?"
    date: 2002-03-19
    body: "i don't know what this is for but i think that it is worng to use racial slurs, i am 15 years old and i find that most teen-agers my age do use racial slurs, often times not acttually knowingi what they really mean, i think that if they do know what they mean then they are just dumb beacuse how would they like it if someone called them that? They probally wouldn't like it at all, i know i don't. I hate it when i hear someone call someone else the \"b\" word or and ( sorry for my language)  An asshole, it just doesn't sound right comming from a teens mousth, it also puts a bad rrap on the other teens, all i hear from the older genneration is \" Oh them teens are nothing but bad, all of them\" i know that's just their opinion but other people share that same opinion, not all teens are like the ones who couse and who call others nnad names! "
    author: "Madison"
  - subject: "BEANER"
    date: 2002-11-22
    body: "DAMN BEANERS STOLE MY BIKE"
    author: "John Maxim"
  - subject: "Re: BEANER"
    date: 2005-08-26
    body: "good"
    author: "look at my new bike"
  - subject: "Re: BEANER"
    date: 2006-05-16
    body: "bitch please!"
    author: "Yo daddy foo!"
  - subject: "Re: BEANER"
    date: 2006-05-16
    body: "bitch please that bike was already mine!\nstupid cracka"
    author: "Yo daddy foo!"
  - subject: "racial slurs and dirogatory comments"
    date: 2002-12-11
    body: "hey!  i am doing and editoral  on racial  problems   pleasegimmie  soem info sites  and  email me back !-thanks  kim!"
    author: "kimberly"
  - subject: "Re: Racial slurs?"
    date: 2003-02-10
    body: "The use of racial slurs simply implies ignorance toward the rest of the world (a denial that we are all one and the same) or a subconcious hatred for oneself (the root of the need to insult another person stems from one's discomfort for themselves).  Education and acceptance are our only hope to help curb this still very prevelent problem."
    author: "Jackson"
  - subject: "Re: Racial slurs?"
    date: 2007-03-05
    body: "I don't understand why anyone cares is someone calls another a wetback, gook, nigger, faggot, or whatever. Every single race, skin tone, job, and personality has a slur, and people DO have the entitlment to speak as they want, how they want and when. There's no reason someone should get upset over what I think or say, they have the right to ignore me even more so than i have the right to say something, but no one has the right to limit what I, or anyone else, chooses to say. People need to understand there is no reason at all to get upset over what someone else thinks, they think it? SO WHAT? What do you care if they think you're a 'nigger' cause you're black, or think you 'steal jobs' because you're an immigrant, if you arent stupid or steal jobs, why should you give two craps?"
    author: "Satisfaction Jackson"
  - subject: "Typo"
    date: 2001-12-29
    body: "There's a typo in the pretty poem on the site.\nIt reads \"cames\", it should read \"comes\"\n\nPeace love and nuclear bombs"
    author: "NDJ"
  - subject: "Kword in Hebrew?"
    date: 2002-10-29
    body: "shalom,\ni live in Germany, my computer here is no the newest, but i manage to work with Red Hat quite well.\nis there already, or is it coming soon - a version of the Kword that enables to write in Hebrew? what about Emails?\nthank you for your reply,\nAssaf Levitin"
    author: "Assaf Levitin"
  - subject: "Re: Kword in Hebrew?"
    date: 2007-02-22
    body: "You should already be able to write in Hebrew, I do it by changing my keyboard layout to Hebrew and then just typing :)  Here's a very good step by step to get it working, http://www.startcom.org/docs/en/Configure%20Keyboard%20Hebrew%20Input/kde_hebrew.html\n\n&#1513;&#1500;&#1493;&#1501;"
    author: "Joshua Austill"
---
<a href="http://www.kde.org/people/meni.html">Meni Livne</a>, who lives in Israel, is KDE's Hebrew localization coordinator and the maintainer of the 
<a href="http://www.kde.org/il/">KDE Israel</a> website. You can read all about him in this weeks episode of 
<a href="http://www.kde.org/people/people.html">People behind KDE</a>. On a
snownote, <a href="mailto:tink@kde.org">Tink</a> and <a href="mailto:konqi@kde.org">Konqi</a> have made a cute 
<a href="http://www.kde.org/people/people.html">snowman</a> this week for the snow-challenged among us.

<!--break-->
