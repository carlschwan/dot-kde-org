---
title: "KDE 2.2 Packages For Solaris"
date:    2001-09-01
authors:
  - "numanee"
slug:    kde-22-packages-solaris
comments:
  - subject: "good work but..."
    date: 2001-08-31
    body: "archives for kde-solaris list are not available?"
    author: "ac"
  - subject: "Re: good work but..."
    date: 2001-08-31
    body: "Ofcourse:\n\nhttp://lists.kde.org/?l=kde-solaris&r=1&w=2"
    author: "reihal"
  - subject: "Re: good work but..."
    date: 2001-08-31
    body: "Thank you but the link on the linked page is incorrect."
    author: "ac"
  - subject: "Re: good work but..."
    date: 2001-08-31
    body: "You are too lazy:\n\nhttp://lists.kde.org/?l=kde-solaris&r=1&w=2&b=200108"
    author: "reihal"
  - subject: "Re: good work but..."
    date: 2001-08-31
    body: "No:\n\nhttp://mail.kde.org/mailman/listinfo/kde-solaris\n\nlinks to:\n\nhttp://mail.kde.org/pipermail/kde-solaris/\n\nbut thanks for the real link."
    author: "ac"
  - subject: "Re: good work but..."
    date: 2001-09-01
    body: "I guess, I have to beg the admins again ;-)"
    author: "Eva Brucherseifer"
  - subject: "That is really great news"
    date: 2001-09-01
    body: "I just want to thank the KDE team team for all their excelent work. It's great that I can enjoy KDE not only on my Linux notebook, but on my Solaris box at work as well."
    author: "Pedro"
  - subject: "KDE on Solaris rocks!!!"
    date: 2001-09-02
    body: "I just love KDE 2.2 on my Ultra60!\nI haven't seen anything so beautiful\non that stupid solaris.\n\nThanks Guys!!\n\nRaul"
    author: "Raul"
  - subject: "Re: KDE on Solaris rocks!!!"
    date: 2001-09-02
    body: "Yeah, I am posting this from konqueror on an UltraSparc. \n\nThe alternative was netscape.\n\nI am so happy it works, I could just cry.\n\nReally.\n\nA HUGE thanks to those who made this possible."
    author: "hmmm"
  - subject: "What about x86?"
    date: 2001-09-02
    body: "What about Solaris x86?  I love KDE 2.1.1 on my Solaris 8 box, but compiling 2.2 is tricky, and still buggy.  Are there any packages out there for x86?\n\n\nSteve"
    author: "Steve Nakhla"
  - subject: "Re: What about x86?"
    date: 2001-09-03
    body: "I've made x86 packages available from\nftp://66.65.70.97/pub/solaris/i86pc/kde/2.2\nhttp://66.65.70.97/~ftp/pub/solaris/i86pc/kde/2.2\n\nSome packages -- such as kdeaddons, kdesdk, kdevelop and language supplements are not available as yet."
    author: "Vladimir Annenkov"
  - subject: "How to install solaris 8 on my second hard disk"
    date: 2001-09-03
    body: "I have two hard disks, I have installed Win2k, Mandrake Linux on my first hard disk(hda), and I \nwant to install solaris 8 on my second hard disk \n(hdc), but when I use the CD to boot my computor and setup solaris, it doesn't let me install solaris on my second hard disk, when I choose fdisk, it always use my first disk.\n\nHow can I do?\n\nThanks for any help.\n\n\nI love KDE"
    author: "younker"
  - subject: "How to install solaris 8 on my partition"
    date: 2001-09-30
    body: "Halo,  please help me, i want to installing solaris but i do not know \nhow to do it ?\n\ni need tutorial step by step , my HD is 20 Gb quantum and i use win XP ,\nmandrake 8 and BEOS 5.....\n\nis there anyone can help me...?\n\nsend to my mail....\n\nthank you very much\n\nregard\n\nRUDY"
    author: "rudy@indoit.net"
  - subject: "Re: How to install solaris 8"
    date: 2001-10-12
    body: "I cannot load the second CD of two during the install process. I've been flailing with this *%#@ Solarius 8 software for weeks. What' the trick? I have tried placing solaris on the 3rd partition and take whatever else it gives me for swap file, etc. Does anybody have an \"Solarius 8 Install for Dummies\" After I get the first CD loaded, there is no front panel, nor help or man pages--does that come after successful install of the 2nd CD?"
    author: "Garry Sheppard"
  - subject: "Re: How to install solaris 8"
    date: 2001-10-20
    body: "Make sure your root partition is not over i gig on older systems it wont properly install with a root larger than i gig possibly 500 meg \n\ngood luck"
    author: "Chris"
  - subject: "Re: How to install solaris 8"
    date: 2003-01-28
    body: "Hallo,\n\nI've read your question.\nI have the same problem.\n\nHave you found a solution already?"
    author: "Agustin"
  - subject: "Re: How to install solaris 8 on my second hard disk"
    date: 2001-12-14
    body: "hi,\n    you try to change the hardisk as u inserted the harddisk in ur secondary slave . insert the second harddisk in ur primary slave and start rebooting. hope it works\n\nbye\nsathya"
    author: "Sathya"
  - subject: "Re: How to install solaris 8 on my second hard disk"
    date: 2008-07-07
    body: "hay man i have worked with computers all my life and i cant do that ethir ive tried but if the jumper on the hard drive is set to slave you cant do it and there can only be one master, but you can put a partition on your frist hdd for solaris 8 good luck."
    author: "finnigan"
  - subject: "Any Chance on Mac OS X"
    date: 2001-09-03
    body: "I would love to have KDE running  on Mac OS X. Gnome is available. Using Fink (debian like package manager for Darwin) it is breeze to install gnome.\n\nI really miss KDE. Any news on KDE ported to MAc OS X? QT 2.3 is already available.\n\nThanks"
    author: "Kundan Kumar"
  - subject: "I built pkgadd format for this package"
    date: 2001-09-05
    body: "Based on this package, I recompiled all things and built a pkgadd format package, it's easier to install. Just bunzip2 kde-2.2-sparc-solaris.pkg.bz2, pkgadd -d kde-2.2-sparc-solaris. Would anyone advise me where and how can I upload it to share? It's approximately 65MB.\n\nIt includes:\n- All things in ftp://ftp.kde.org/pub/kde/2.2/stable/src\n- qt-2.3.1\n- bzip2-1.0.1\n- jpeg-6b\n- libmng-2.0.2\n- libpng-1.0.10\n- libxml2-2.4.3\n- openssl-0.96b\n- pcre-3.4\n- tiff-v3.5.5\n- xpm-3.4k\n- zlib-1.1.3\n- DT settings to enable KDE in Solaris dtlogin window"
    author: "Yale Yu"
  - subject: "Re: I built pkgadd format for this package"
    date: 2001-09-05
    body: "I forgot to list upper, but of course, it also includes koffice-1.1 and koffice-i18n-1.1."
    author: "Yale Yu"
  - subject: "Re: I built pkgadd format for this package"
    date: 2001-09-06
    body: "Hi,\n\nkann you tell me the URL where the pkgadd format file is.\n\n(sorry for my english am a German)\n\nThx Andreas"
    author: "Andreas"
  - subject: "Re: I built pkgadd format for this package"
    date: 2001-09-07
    body: "Hi, I'm discussing with Eva how to upload it, before that, I have no web space to upload a so large file.\n\nHope it can be spread to test soon."
    author: "Yale Yu"
  - subject: "Re: I built pkgadd format for this package"
    date: 2001-12-28
    body: "I've been trying to install this kit on Solaris 8 on an Ultra-60, and have problems with several of the packages, including the package for kde itself.\n\nSymptom is: after pkgadd -d <package filename>, I get\npkgadd: ERROR: no packages were found in <var/tmp/{mumble}>\n\nFor example, pkgadd -d /export/home/.../kde-2.2.2-SPARC-solaris-local-1.pkg\n\nresults in;\n\npkgadd: ERROR: no packages were found in <var/tmp/dstreAAAdaaWid>\n\nMy current directory is the one containing the package, in each case.\n\nI've downloaded the kit twice - once in pieces, once all-in-one, and I get the same problem on the same packages. Note that several of the packages, including qt, do *not* have the problem, and seem to install fine. The lack of consistency is puzzling.\n\nAny clues?"
    author: "David Coldrick"
  - subject: "Re: I built pkgadd format for this package"
    date: 2002-02-19
    body: "I have the very same problem. Did you find any solution?"
    author: "Andreas Goebel"
  - subject: "Re: I built pkgadd format for this package"
    date: 2002-03-22
    body: "Install SUN patches 110934-05 and 110380-04\n\nI had same problem on Solaris 8. I loaded the patches, rebooted and was able to install KDE 2.2.2.\n\nGo here http://sunsolve.Sun.COM/pub-cgi/show.pl?target=patches/patch-access\nand search for the above numbers.  There is a bug in pkgadd that causes the install of KDE to fail.\n\nBona fortuna!!\n\nChad"
    author: "0chad0"
  - subject: "problem - libGLU.so.1 isn't found!!!"
    date: 2001-09-09
    body: "I installed everything as said, and added the paths to the libraries in my LD_LIBRARY_PATH.\nHowever, when I try to launch kdevelop or almost any other application (standalone, I didn't configure KDE to be my desktop) it complains that it doesn't find libGLU.so.1\nI used find to search for the library in my system, but didn't find it."
    author: "Niv"
  - subject: "Re: problem - libGLU.so.1 isn't found!!!"
    date: 2001-12-10
    body: "Same problem here ... !!!"
    author: "Murali"
  - subject: "Both .tar.gz and .pkg format"
    date: 2001-09-18
    body: "KDE 2.2 for SPARC Solaris with supplementary libraries (passed test on SPARC Solaris 2.6, 7, 8)\n \ninclude:\n- All things in ftp://ftp.kde.org/pub/kde/2.2/stable/src\n- qt-2.3.1\n- bzip2-1.0.1\n- jpeg-6b\n- libmng-2.0.2\n- libpng-1.0.10\n- libxml2-2.4.3\n- openssl-0.96b\n- pcre-3.4\n- tiff-v3.5.5\n- xpm-3.4k- zlib-1.1.3\n- koffice-1.1\n- koffice-i18n-1.1\n- DT settings to enable KDE in Solaris dtlogin window\n \nDOWNLOAD\n========\n\nkde-2.2-sparc-solaris.tar.gz 78,155,245 bytes, http://download.pchome.net/php/dl.php?sid=8746\n\nkde-2.2-sparc-solaris.pkg.gz 78,462,032 bytes, http://download.pchome.net/php/dl.php?sid=8747\n\nINSTALLATION\n============\n \n1. kde-2.2-sparc-solaris.pkg.gz (root required)\n \n# gunzip kde-2.2-sparc-solaris.pkg.gz\n# pkgadd -d kde-2.2-sparc-solaris.pkg\nthen quit OpenWindow or CDE, you may find KDE session in dtlogin window\n \n2. kde-2.2-sparc-solaris.tar.gz (root not required)\n \n$ cd <SOMEWERE_YOU_SPECIFIED>\n$ gunzip kde-2.2-sparc-solaris.tar.gz\n$ tar xvf kde-2.2-sparc-solaris.tar\n \nadd the following lines in $HOME/.profile\n \nKDEDIR=<SOMEWERE_YOU_SPECIFIED>/kde\nQTDIR=<SOMEWERE_YOU_SPECIFIED>/qt\nLD_LIBRARY_PATH=$KDEDIR/lib:$QTDIR/lib:$LD_LIBRARY_PATH\nPATH=$KDEDIR/bin:$QTDIR/bin:$PATH\nexport PATH LD_LIBRARY_PATH QTDIR KDEDIR\n \nthen, if you have root permission\n \n$ su\n# mv etc/* /etc\nthen quit OpenWindow or CDE, you may find KDE session in dtlogin window\n \nif you have no root permission\n \n$ echo \"exec startkde\" > $HOME/.xinitrc\nchoose OpenWindow session in dtlogin window\n \nFor further information, please contact me yale_yu@yahoo.com"
    author: "yale_yu"
  - subject: "Compiled kde for solaris sparc"
    date: 2001-10-08
    body: "go to \nftp://ftp.matrix.com.br/pub/kde/stable/2.2/Solaris/Sparc/"
    author: "Bhasker C V"
---
<a href="http://www.rt.e-technik.tu-darmstadt.de/~eva/main.html">Eva</a> wrote in to inform us that the Solaris packages for KDE 2.2 are finally ready.  They are targeted at  the Sparc and Ultra Sparc architectures, and work with Solaris 6/2.6, 7/2.7 and 8, and possibly Solaris 2.5 and 2.5.1 as well (Don't blame <i>us</i> for these confusing version numbers).  You can get them <a href="ftp://ftp.sourceforge.net/pub/mirrors/kde/stable/2.2/Solaris/">here</a> -- be sure to check out the <a href="ftp://ftp.sourceforge.net/pub/mirrors/kde/stable/2.2/Solaris/README">README</a>. You may also be pleased to learn of the new <a href="http://mail.kde.org/mailman/listinfo/kde-solaris">kde-solaris list</a> which already has a great number of subscribers despite the fact that it has not yet been announced!  This is a good list to use if you are having problems with the binaries.  Hopefully more of you running Sun systems can now enjoy KDE on your desktop and with less problems.
<!--break-->
