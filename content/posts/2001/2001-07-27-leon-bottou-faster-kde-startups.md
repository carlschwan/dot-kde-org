---
title: "Leon Bottou: Faster KDE Startups?"
date:    2001-07-27
authors:
  - "numanee"
slug:    leon-bottou-faster-kde-startups
comments:
  - subject: "Kernel re-Compile will also speed up KDE"
    date: 2001-07-27
    body: "I am also bothered by KDE's not-so-fast performance. I recompiled KDE packages but didn't saw any improvement. But when I recompiled Linux Kernel 2.4-2 on RH 7.1, I saw 40% improvement in KDE + its apps."
    author: "Asif Ali Rizwaan"
  - subject: "Exceptional! But what about non intel platforms ?"
    date: 2001-07-27
    body: "This will be really great if it has no undesirible side effects.\n\nBut I use solaris at work, and of course gcc is used, so will this mechanism also work ( with the \"minor\" change of i386 to SUN - or whatever ) to speed up KDE on solaris ?\n\nCPH\n\nBTW: For any company trying to compete in the same space as KDE, the rate of advance because of the open source model, must be frightening them !"
    author: "CPH"
  - subject: "G++?"
    date: 2001-07-27
    body: "Could this eventually be integrated into g++?\n\nMan, this is really great news. Talking about HUGE performance improvements. Imagine applying this to server software written in C++ ...\n\nOf course, it will be great for KDE, too, but that is not even the area where it will matter most, IMHO."
    author: "Jeremy M. Jancsary"
  - subject: "Re: G++?"
    date: 2001-07-27
    body: "server software ???\n\nThe improvement is in program *startup* (or actually the time it takes for the program to find and move around all internal symbols that it needs), not overall performance.\n\nAnother package that I believe will benefit from this is Star Office and in some parts Net^H^HMozilla\n\n\n/Sam"
    author: "Sam"
  - subject: "Re: G++?"
    date: 2001-07-27
    body: "OK, I'll have to explain this ... otherwise I might end up looking like an idiot.\n\nI agree that what I wrote can easily be misunderstood :)\n\nI was talking about CGI applications etc. Apps that will have to be started lots of times.\n\nI suppose a website might be able to handle a lot more traffic if the underlying CGIs start up more quickly (I might be wrong of course)."
    author: "Jeremy M. Jancsary"
  - subject: "Re: G++?"
    date: 2001-07-30
    body: "I think you can do something like that already with apache + mod_perl...not sure though whether it's only for the perl-compilation or for both the compilation and the execution."
    author: "Jonathan Brugge"
  - subject: "Re: G++?"
    date: 2001-07-30
    body: "It's for both.\n\nThere is several way to use mod_perl, but most of the time, you will use it to cache the compilation process of Perl on your script, and then you will simply re-call it the next time it is request. Then, your re-call of your script will be handled by mod_perl like a function call."
    author: "Holstein"
  - subject: "prelinking in the linker is working these days."
    date: 2001-07-27
    body: "Subject says it all - I'm running on a completely prelinked system these days.\n<br><Br>\nSource available at<br>\n<a href=\"ftp://rawhide.redhat.com/pub/redhat/linux/rawhide/SRPMS/SRPMS/prelink-0.1.3-2.src.rpm\">prelink-0.1.3-2.src.rpm</a>\n<br><br>\nYou'll also need the corresponding binutils patches, part of<br>\n<a href=\"ftp://rawhide.redhat.com/pub/redhat/linux/rawhide/SRPMS/SRPMS/binutils-2.11.90.0.8-5.src.rpm\">binutils-2.11.90.0.8-5.src.rpm</a>"
    author: "bero"
  - subject: "Corrected link"
    date: 2001-07-30
    body: "The pub is missing from the above link:\n\nftp://rawhide.redhat.com/pub/redhat/linux/rawhide/SRPMS/SRPMS\n\nI'm trying it out now.  :-)\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: Corrected link"
    date: 2001-07-30
    body: "Thanks, might have been my fault.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: prelinking in the linker is working these days"
    date: 2001-07-30
    body: "Does KDE have to be rebuilt to see the effects of this?  I installed it, and startup times do seem faster, but it may just be wishful thinking on my part.  ;-)\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: prelinking in the linker is working these days"
    date: 2001-07-30
    body: "No need to recompile - you need to prelink the\napplications though (run prelink --all)."
    author: "bero"
  - subject: "Re: prelinking in the linker is working these days"
    date: 2001-07-30
    body: "So you can run this on a binary install of KDE? I might give it a try after all then...\n\n  Thanks,\n       Tim"
    author: "Timothy R. Butler"
  - subject: "Re: prelinking in the linker is working these days"
    date: 2001-07-31
    body: "I just realized the program you refer to is different then the one in the news article. Does it do the same thing?\n\n  Thanks,\n       Tim"
    author: "Timothy R. Butler"
  - subject: "Re: prelinking in the linker is working these days"
    date: 2001-07-31
    body: "Anyone know if a debian version of this hack exists ??"
    author: "Lovechild"
  - subject: "Re: prelinking in the linker is working these days"
    date: 2001-07-31
    body: "If you follow the link to Leon's original message you will find the source code for the hack.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: prelinking in the linker is working these days"
    date: 2001-07-31
    body: "I wouldn't know what to do with it, even if you payed me... \n\nYes, I'm stupid - but I have a urge for speed"
    author: "Lovechild"
  - subject: "Re: prelinking in the linker is working these days"
    date: 2001-08-02
    body: "No you are lazy... all the instructions are on Leon's webpage including how to compile the objprelink.c file. And about being stupid: even you can copy and paste the gcc line into a konsole and copy the resulting objprelink executable into /bin, /usr/bin/ or /usr/local/bin, where ever you want it."
    author: "lafaard"
  - subject: "Re: prelinking in the linker is working these days."
    date: 2001-07-30
    body: "stupid question: is glibc 2.2 required or is 2.1 enough"
    author: "aleXXX"
  - subject: "Re: prelinking in the linker is working these days."
    date: 2001-07-31
    body: "It doesn't work on my debian potato system(glibc 2.1). But I think it's binutils, which are not new enough."
    author: "Tschortsch"
  - subject: "Re: prelinking in the linker is working these days."
    date: 2001-08-02
    body: "I updated my binutils to 2.11. Now it works. \nIt's really fast."
    author: "Tschortsch"
  - subject: "Re: prelinking in the linker is working these days"
    date: 2001-07-31
    body: "I tried it on mandrake 7.2 glibc 2.1.3 with newest binutils (2.11.0.8) and libelf(0.7). Wouldn't compile (some missing declarations, STV_DEFAULT and others) after some playing to include these declarations from binutils it compiled and runs. prelink with the n option (dryrun) seems to work fine, but if I want to prelink for real it bails out with something like: no space for dynamic.\nWhatever that means...\n\nI think it could be made to work, but prolly has no real priority since everyone goes to 2.2...\n\nDanny"
    author: "Danny"
  - subject: "Re: prelinking in the linker is working these days."
    date: 2001-07-31
    body: "Tried going to the links - get \"unable to login\" messages?? Do you have to be a registered Rad Hat customer?  I'm running Mandrake v8.0 w/ KDE v2.1.1. Is there anywhere else I might find it?? \n\nThanks -"
    author: "Greg"
  - subject: "Re: prelinking in the linker is working these days."
    date: 2001-07-31
    body: "I get a lot of errors about not having enough room to add .dynamic entry\nThis seems to happen because of an empty .bss and/or .sbss in the library\n\nAny suggestions?\n\nThanks"
    author: "Alin Vaida"
  - subject: "prelink-0.1.3-2.src.rpm won't install, objprelink"
    date: 2001-07-30
    body: "Hmm, trying to install prelink-0.1.3-2.src.rpm, by doing a rpm --rebuild - I get\n\ncxx.c:200: `STV_DEFAULT' undeclared (first use in this function)\n\n( amongst some other warnings. ) This is a RH6.1 based system, although with many updated bits ( including libelf 0.7.) Any ideas? ( I also tried objprelink, which compiles but seg faults. )"
    author: "Adam Hill"
  - subject: "Re: prelink-0.1.3-2.src.rpm won't install, objprel"
    date: 2001-07-31
    body: "I think its due to glibc 2.1. I could make it compile but after that it still doesn't work (see my other post).\n\nDanny"
    author: "Danny"
  - subject: "Re: prelink-0.1.3-2.src.rpm won't install, objprel"
    date: 2001-07-31
    body: "Yep, installed glibc-2.2.3 and it works fine now... except that nothing will prelink because ld-linux-2.2.3 won't prelink ( \"not enough room to add .dynamic entry\" ) :-("
    author: "Adam Hill"
  - subject: "Re: prelink-0.1.3-2.src.rpm won't install, objprel"
    date: 2001-08-01
    body: "hey..than it wasn't due to 2.1.3 since I managed to compile it on 2.1.3 but also get this .dynamic error(and thought it to be due to glibc). If it happens in 2.2 as well it must be something else? Maybe I'll send a mail to the author this evening.\n\nDanny"
    author: "Danny"
  - subject: "vs KDE Init & Prelinking?"
    date: 2001-07-30
    body: "Already people are reporting great speedups with this hack.  Everyone seems in favor of including it in KDE 2.2.  What does this mean for KDE Init and for distributions with prelinking already?  Is it still worthwhile?"
    author: "KDE User"
  - subject: "always compile that speed up stuff"
    date: 2001-07-30
    body: "seems this trick has lots of advantages (speed especially), so why not always compile kde with the speed improvements from now on?\n\nKDE is better than any other WM, EXCEPT when launching applications (it's so slow!).\n\nIf we can improve KDE's speed by up to 50%, then all new release should be tuned like this (I really dunno why all of a sudden KDE is capable to be so fast and that nobody discovered or put it on focus before)"
    author: "ced"
  - subject: "Re: always compile that speed up stuff"
    date: 2001-07-31
    body: ">nobody discovered or put it on focus before\n\nwell, people have been talking about it for a while, actually. I believe there was a dot article about it a while back"
    author: "Carbon"
  - subject: "Mandrake rpms"
    date: 2001-07-30
    body: "Texstar has some kde 2.2 beta Mandrake 8.0 rpms built with the new code. You can get them at <a href=\"http://www.pclinuxonline.com/\">www.pclinuxonline.com</a>\n<br><br>\nCraig"
    author: "Craig"
  - subject: "prelink-0.1.3-2.src.rpm won't install, objprelink"
    date: 2001-07-30
    body: "Hmm, trying to install prelink-0.1.3-2.src.rpm, by doing a rpm --rebuild - I get\n\ncxx.c:200: `STV_DEFAULT' undeclared (first use in this function)\n\n( amongst some other warnings. ) This is a RH6.1 based system, although with many updated bits ( including libelf 0.7.) Any ideas? ( I also tried objprelink, which compiles but seg faults. )"
    author: "Adam Hill"
  - subject: "Re:"
    date: 2001-07-31
    body: "Does this works in C too?"
    author: "dc"
  - subject: "Re:"
    date: 2001-07-31
    body: "No"
    author: "Tschortsch"
  - subject: "Another Speed Issue"
    date: 2001-07-31
    body: "While we are talking about speed, has there been any improvement on image rendering/decoding, last time I checked (2.1.1) Konqueror and Pixie where unusable as a thumbnail viewers because of the horrible preview speed. I believe they both use the same libs(Qt or KDE core?) for this. Would\u0092nt there be a big performance boost for the whole environment if it were to be optimised (or at least for Konqueror and Pixie)."
    author: "Fredrik Corneliusson"
  - subject: "Huh? Pixie ain't slow."
    date: 2001-07-31
    body: "I'm not sure if it got into 2.1 or not, but Pixie's thumbnail manager has supported load on demand for quite some time that's extremely fast when browsing existing thumbnails. I've also just implemented load on demand for mimetype data as well, so you can enter a directory of > 2000 thumbnailed images (I took all my photos and makde a bunch of copies ;-) and start browsing any thumbnail essentially immediately. It used to take around 5-6 seconds, not bad but this is even better. It's faster than anything else I've been able to compare it to, both on Linux and Windows. A new version should be released in about a week. If load on demand wasn't implemented in KDE 2.1, I strongly suggest you upgrade. You'll get a new UI and other goodies as well."
    author: "Mosfet"
  - subject: "Re: Huh? Pixie ain't slow."
    date: 2001-07-31
    body: "Hi mosfet,\nI was not commenting on the speed of viewing existing the thumbnails, sorry if that was unclear.\nIt is the speed that KDE handles pics, if you for example click on an jpeg image in konq you can see it gradually appearing, but for example in GQview it's displayed immediately. I don\u0092t know what makes it so could it be kio? But I seem to remember a thread on the mailing list concerning poor performance in KDE image libs, no optimised ASM code for instance.\nI\u0092ll check out Pixie as soon as possible, will the new release be based on KDE 2.2?"
    author: "Fredrik Corneliusson"
  - subject: "Re: Huh? Pixie ain't slow."
    date: 2001-07-31
    body: "Well, you said thumbnails, so you were pretty unclear ;-) Your seeing it slower in Konq because it's incrementally loading and rendering it. Good for web based images, bad for local files. Use a different component for viewing images, not the HTML widget (which is what you have it set to ;-). \n\nThis was never an issue with Pixie, which never did incremental loading (it's be nice to add for remote images, tho). I don't think you used it much... As far as ASM and other things for loading images, that won't help at all. The main bottleneck in loading images is disk. It could help for things like smoothscaling thumbnails, but 2/3 of the time is spent in disk I/O (I checked), so not much. The \"poor performance\" of KDE/QT image loading is mostly people not knowing what they are talking about. For example, both Qt and imlib both call libgif in essentially the same way, same for libjpeg, libpng, etc... for loading data."
    author: "Mosfet"
  - subject: "Re: Huh? Pixie ain't slow."
    date: 2001-07-31
    body: "BTW, sorry for bad grammar, 6:10am and I haven't slept yet >:) Working on Pixie ;-)"
    author: "Mosfet"
  - subject: "Seems odd to me."
    date: 2001-07-31
    body: "I can't imagine that looking up even a few tens of thousand symbols in a symbol table should make any appreciable difference in program startup time for a properly implemented symbol table.\n\nWhile this is a neat hack, it sounds to me that the problem is not with g++ but with the data structures that the runtime system uses for relocation (linear search?).  Probably that should get fixed, and that would speed things up generally, not just in this special case."
    author: "Mike"
  - subject: "Re: Seems odd to me."
    date: 2001-07-31
    body: "Well, the startup time of non-prelinked binary, at least on my machine, is mostly filled with hard drive seek tests (double 450Mhz PIII, kernel.org linux-2.4.5). I wonder whether prelinking doesn't streamline some disk accesses at the same time by coincidence, maybe just by not referring to the pages which don't need to be accessed at startup. I imagine that if the relocation tables are spread around the binary, there will be a decent amount of seeking at startup, just to get the right pages in.\n\nWell, my assumption is that linux does some memory<->disk mapping of binaries' pages, if it doesn't then I'm obviously wrong.\n\nWould linear search be sooo slow given that there really aren't that many symbols to look-up (I doubt it's tens of thousands). I imagine that a typical symbol table would be - well, the one in libc-2.2 is about 2k symbols. Maybe that really goes up to tens of thousands for kde+qt apps??? :-("
    author: "Kuba"
  - subject: "Re: Seems odd to me."
    date: 2001-08-03
    body: "KDE apps tend to have 50000+ symbol resolves.\n\nrun this (no quotes):\n\"LD_DEBUG=statistics kwrite\""
    author: "Ben Ploni"
  - subject: "Re: Seems odd to me."
    date: 2001-08-04
    body: "I ran a couple of tests with amazing results:\n\nholle@chaos:~/.p > LD_DEBUG=statistics /opt/kde/bin/kedit\n01109:                   number of relocations: 14053\nholle@chaos:~/.p > LD_DEBUG=statistics /opt/kde2/bin/kedit\n01110:                   number of relocations: 47329\nholle@chaos:~/.p > LD_DEBUG=statistics /opt/gnome/bin/gedit\n01111:                   number of relocations: 13878\n01113:                   number of relocations: 1466\n01113:                   number of relocations: 794\n\nSo Gnome (version 1.2.1) had about the same amount of relocations as KDE1 (1.1.2). The big hit came with KDE2 (2.1.1/2). This is, so I think, directly related to the DCOP stuff and all the other things going on in the background. Look at kwrite from KDE2 starting:\n\nholle@chaos:~/.p > LD_DEBUG=statistics /opt/kde2/bin/kwrite\n01151:                   number of relocations: 51023\n01152:                   number of relocations: 1466\n01152:                   number of relocations: 46994\nDCOPServer up and running.\n\nNow that is a lot ...\n\nI think we need to streamline the API a little bit. Make more use of inline functions and try to get rid of function duplicates i.e. two functions doing mainly the same thing.\nMaybe we can come up with a late binding feature like python has, where the functions code gets bound at the very moment it is used and not earlier (and for again and again for python ...)\n\n- Holger"
    author: "Holger Lehmann"
  - subject: "Re: Seems odd to me."
    date: 2001-08-04
    body: "Using inlining for methods is not a good idea for\na C++ library. Great for apps, bad for libs. Think\nof what happens when you try to change the\nimplementation later. I needed to change some\nkstyle* stuff a while ago and couldn't. Argh.\n\nRik"
    author: "Rik Hemsley"
  - subject: "Re: Seems odd to me."
    date: 2001-08-04
    body: "Sorry, KStyle has no inline methods... doh! KThemeStyle does, but that is not called by any other applications, only dynamically loaded by the theme engine. Either way, inline methods are very common in libs (look at Qt: grep inline *h | wc --lines gives you  697 occurances)."
    author: "Mosfet"
  - subject: "Re: Seems odd to me."
    date: 2001-08-04
    body: "And it is KThemeStyle which is the problem.\nI wrote a global pixmap server for KDE, with\nthe intention of alleviating the overhead\ngenerated by KThemeStyle when loading\npixmaps on app start.\n\nThen I found that I couldn't re-implement\nparts of KThemeStyle, so now this has to\nwait for KDE 3.\n\nRik"
    author: "Rik Hemsley"
  - subject: "Re: Seems odd to me."
    date: 2001-08-04
    body: "Write a new style plugin based off of KThemeStyle, it's a plugin that provides the theme engine, remember. Those headers are included in the KDE libraries simply so people could derive from them, but no one ever did (people wrote very few styles period). \n\nIf you really do have a style you can release it today.\n\nYour change would of also most certainly required private and protected member and data changes anyways, so still doesn't make an argument against inline methods, which are used hundreds of times in both KDE at QT. \nShould we dump private and protected members as well?"
    author: "Mosfet"
  - subject: "Re: Seems odd to me."
    date: 2001-08-04
    body: "We already did dump private members by\nusing the 'pimpl' paradigm, for this exact\nreason.\n\nPutting code in headers causes BC issues\nlater on. Don't do it.\n\nRik"
    author: "Rik Hemsley"
  - subject: "Re: Seems odd to me."
    date: 2001-08-04
    body: "A) This does not prevent you from making a new KStyle or KThemeStyle, as you claimed. I made very sure you can do anything you want with the plugin mechanism and saying BC prevents you from doing anything is just incorrect. \n\nB) KDE headers currently include 1,797 incidents of inline methods. They are a very good way to optimize code and are the equivalent of #define macros in C. Dropping them isn't what I'd recommend to any developer, unless if you like unneeded method call overhead.\n\nJust correcting some falsehoods..."
    author: "Mosfet"
  - subject: "Re: Seems odd to me."
    date: 2001-08-04
    body: "You're right, I could make a new style plugin. I had\nforgotten that I could change the plugin used by\na 'themed style' by altering the .themerc file.\n\nI _would_ recommend dropping out inlined methods\nwherever you can. Only keep them in for code which\nis on a critical path. If you fill your headers with code,\nno-one can override that code later.\n\nOptimisation rule #1: Don't. Not until you've measured.\n\nRik"
    author: "Rik Hemsley"
  - subject: "Re: Seems odd to me."
    date: 2001-08-04
    body: "If you do what your recommending you have apps (that may or may not be in a critical loop, how are you to know as a library developer?), in absurd situations like being in loops making method calls for things like \"a=b;\" and \"++i;\". This is insane and may be acceptable to you, but certainly isn't to me and judging by it's use not acceptable to others as well. This is exactly why people use inline.  \n\nAlso, you can't rely on the compiler optimizing small methods inline automatically like you would if a C function did the same thing."
    author: "Mosfet"
  - subject: "Doesn't seem too odd for me"
    date: 2002-09-26
    body: "One thing to keep in mind is that each relocation also results in a dirty page.  If the relocations are spread throughout the application, then you're going to generate a lot of page faults very quickly, and make many trips into and out of the kernel.  If any of these pages would be sharable, they get COW'd (Copy on Write).  Ouch. \n\nA secondary benefit of prelinking is that apps which use the same libraries are more likely to share pages.  This reduces system memory footprint and regains one of the key advantages of shared libraries."
    author: "Joe Zbiciak"
  - subject: "?"
    date: 2001-08-06
    body: "Anybody has a solution for the \"no space for .dynamic\" error???"
    author: "me"
---
As a follow-up to <a href="http://www.kde.org/people/waldo.html">Waldo Bastian</a>'s <a href="http://www.suse.de/~bastian/Export/linking.txt">analysis</a> of KDE startup times, <A href="mailto:leonb@research.att.com">Leon Bottou</a> has implemented <a href="http://lists.kde.org/?l=kde-devel&m=99618500904021&w=2">an inspired hack</a> to improve the startup of C++ programs under GNU/Intel systems. <i>"Waldo Bastian's document demonstrates that the current g++ implementation generates lots of expensive
run-time relocations.  This translates into the slow startup of large C++
applications (KDE, StarOffice, etc.). The attached program "objprelink.c" is designed to reduce the problem. Expect startup times 30-50% faster."</i> <b>Update: 08/01 4:52 AM</b> by <a href="mailto:navindra@kde.org">N</a>: Consult <a href="http://www.research.att.com/~leonb/objprelink/">Leon's objprelink page</a> for some great details and up-to-date information on this hack as well as on the prelinker <a href="http://dot.kde.org/996240227/996254900/">mentioned</a> by Bero.  Thanks to <a href="http://freekde.org/article.pl?sid=01/07/31/2220220&mode=nocomment">freekde</a> for the tip-off.
<!--break-->
<p>
If I understand correctly, Leon's hack works around the problem by adding a level of indirection - a stub - to each function in a class's virtual table, and changing references to the function to point to the new stub instead -- thereby eliminating a whole lot of symbol lookups and relocations.
</p>
<p>
Check out <A href="http://lists.kde.org/?l=kde-devel&m=99618500904021&w=2">Leon's email</a> for the exact juicy details and for the Intel/GCC-specific C code of the program you will need to process object files before linking.  One possible downside of this optimization is that virtual function invocations may now be slower due to the extra indirection involved.  
</p>
<p>
And of course, no matter how brilliant the hack, we are still working around faults in the GNU linker.  Apparently some work is going on in that area as well as can be seen in <a href="http://lists.kde.org/?l=kde-core-devel&m=99134101312638&w=2">this email</a> from <A href="mailto:jakub@redhat.com">Jakub Jelinek</a>.
</p>