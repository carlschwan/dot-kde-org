---
title: "KDE @ LinuxWorldConference & Expo 2001"
date:    2001-10-26
authors:
  - "Dre"
slug:    kde-linuxworldconference-expo-2001
comments:
  - subject: "how about few nice KDE 3.0 screen shots"
    date: 2001-10-26
    body: "I am very excited about KDE 3, I wish I could see some nice screenshots!"
    author: "Asif Ali Rizwaan"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-26
    body: "I don't think there is much to see yet.  It is my understanding that the alpha of KDE3 was just a straight port of KDE2.2 to QT3."
    author: "Terry"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-26
    body: "Well, there is the one or the other new thingy in there like the side graphics in the K menu (which can be turned on or off) and the new spashscreen :) But basically, it's improvements that are now implemented. I'd like to mention that KOffice and KHTML seem to be the place with the most activity :)"
    author: "Ralf Nolden"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-26
    body: "KOffice is active ? Where can I have some news about the current development ? The koffice.org seems freezed for months."
    author: "julo"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-26
    body: "koffice is DEFINITELY active!\ncheck out lists.kde.org/?l=koffice\nand\nlists.kde.org/?l=koffice-devel\n\nfor mailing list archives.\n\nof course, few more developers would still help, it's amazing how much they did considering the little number of them...\n\nbtw, writing my dissertation (about 45 pages for now) on kword from koffice 1.1 :o))"
    author: "emmanuel"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-27
    body: "How do you make you citations endnotes, footnotes ?\nIs there support for this in KWord?"
    author: "ac"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-27
    body: "in cvs yes"
    author: "FO"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-26
    body: "There're also lots of nice improvements to the DCOP scripting facilities.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-26
    body: "I'm running then alpha version (2.91 CVS >= 20011012) and it seems to be alot faster then\nkde 2.2.1. There is also a very cool window deco named Glow where as the name says glows when the mouse is over one of it's buttons. Very very Kool"
    author: "me"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-26
    body: "Really? Faster than KDE 2.2.1?  Well, that's a relief and very good to hear. (unfortunately, I can't check for myself as I'm on vacation and the downloads are just impractical at this point...)"
    author: "Navindra Umanee"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-26
    body: "I've also found it to be faster, when starting up the desktop and when launching new konq windows. Unfortunately, kcontrol and konq keep krashing, but what do I expect with an alpha? :)"
    author: "Rakko"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-29
    body: "so !?\n\nWhere are the screenshots everybody is waiting for ? :))\n\nPlease make us happy !"
    author: "julo"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-29
    body: "well... it looks/feels like kde-2.2.1 on steroids (=faster)"
    author: "me"
  - subject: "Re: how about few nice KDE 3.0 screen shots"
    date: 2001-10-29
    body: "There really are very few user-visible changes.  Everything looks exactly the same except for the side K-menu graphic, the startup spash screen, and the fact that applet handles on Kicker have little menu popup buttons.  Also, there is a bug preventing anti-aliased fonts from being used so my desktop doesn't look as cool as it used to.  There are a lot of bugs, in fact.  The splash screen is available at www.kde-look.org if you want to take a look.\n\nA lot of work is going on \"behind the scenes\" right now, and I think the user-visible changes (like new styles/icons/etc) won't happen until later."
    author: "not me"
  - subject: "Stuffed Konqi"
    date: 2001-10-26
    body: "Is it possible to buy a stuffed Konqi?   If not you might considering selling them online.   I would buy one to support Kde development and give it so my son."
    author: "Mastenson"
  - subject: "Re: Stuffed Konqi"
    date: 2001-10-26
    body: "I can second that, except that it would be for my girl friend :)"
    author: "TT"
  - subject: "Re: Stuffed Konqi"
    date: 2001-10-26
    body: "Here is a web page for information on stuffed konqis. However, it looks like the vendor has only a web page in German.\n\nhttp://www.kde.org/kde-stuff.html"
    author: "jj"
  - subject: "Re: Stuffed Konqi"
    date: 2001-10-26
    body: "If you click in the right spots you'll find an english version I believe.  I'm from the USA, and I got my stuffed Konqi from them without problems."
    author: "Justin"
  - subject: "Re: Stuffed Konqi"
    date: 2008-06-10
    body: "Where did you click? I was looking all over. I used altavista.com to translate it and it still could not be found. They also want you to register with them to shop unless there is another way."
    author: "Inuchan"
  - subject: "Re: Stuffed Konqi"
    date: 2008-11-21
    body: "You'd have to press \"Weitere Produkte und Preise\" to see the prices and you'll be given the option to add a ~10 in Konqi or a ~33.5 in Konqi to your basket (Warenkorb).  Once you're done, you'll have to click \"Zur Kasse\"."
    author: "Quatrerwin"
  - subject: "(OFFTOPIC) more mosfet.org drama"
    date: 2001-10-28
    body: "Mosfet.org is back, but maybe not for long."
    author: "Anonymous"
  - subject: "Re: (OFFTOPIC) more mosfet.org drama"
    date: 2001-10-29
    body: "Looks like it is for long."
    author: "reihal"
  - subject: "KMenu - graphics"
    date: 2001-10-29
    body: "Could somebody please post a picture of the kmenu with the side graphics?"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Screenshot"
    date: 2001-10-29
    body: "Here it is.  I don't like it and I turned it off right away.  Luckily it is easy to do as you can see in the screenshot."
    author: "not me"
  - subject: "Re: Screenshot"
    date: 2001-10-31
    body: "Doesn't look very nice, IMHO. Pitch-black is probably not ideal. And a gradient would definitely look nicer. Oh well... as you say, it looks like I can easily turn it off.\n\nThanks for the screenshot."
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Re: Screenshot"
    date: 2001-10-31
    body: "Well, what I don't like about it is that it gets in the way.  You have to move the mouse sideways to get around it before you can use the menu.  I have nothing against the actual graphic.  I suppose I could get used to it but I still think it is a step backwards."
    author: "not me"
---
The <a href="http://www.kde.org/">KDE Project</a> today revealed its plans
for the <a href="http://www.linuxworldexpo.de/">LinuxWorldConference and
Expo</a>, a European B2B summit for the Open Source community.
Last year, in the conference's debut, over 11,000 trade visitors and
100 exhibitors attended. The conference will take place from
October 30 - November 1, 2001, at the
<a href="http://www.linuxworldexpo.de/gb/01gen/0108.htm">Frankfurt
Fairgrounds</a>, Frankfurt, Germany.
We invite everybody to meet KDE developers, KDE enthusiasts and, of course, our
mascot <a href="http://www.kde.org/people/konqi.html">Konqi</a>, at the
KDE booth.  Some details about the planned events, demonstrations and
presentations, courtesy of
<a href="mailto:kpfeifle@NOSPAM_danka.de">Kurt Pfeifle</a> and
<a href="mailto:tackat@NOSPAM_kde.org">Torsten "Tackat" Rahn</a>, follow.

<!--break-->
<p>&nbsp;</p>
<h4 align="center">KDE at LinuxWorldConference and
Expo 2001</h4>
<p><em>General</em>.  As usual, KDE will staff a
booth where KDE users can see the latest stable (2.2.1) and development
(3.0.0alpha) KDE releases, ask questions, and socialize with KDE
developers.</p>
<p><em>KDE Printing Demonstration</em>.  <a href="mailto:kpfeifle@NOSPAM_danka.de">Kurt Pfeifle</a>
will be demonstrating the latest
<a href="http://users.swing.be/kdeprint/">KDEPrint</a> capabilities using
the high-quality <a href="http://cups.sourceforge.net/">CUPS</a> and
<a href="http://gimp-print.sourceforge.net/">Gimp-Print</a> drivers.
The KDE booth will feature two printers:
</p>
<ul>
<li>an <a href="http://www.epson.com/">Epson</a><sup>tm</sup> <a href="http://www.epson.co.uk/product/printers/photo/styphoto895/">
Stylus Photo 895</a>, which with the aforementioned drivers produces
superior photo quality output; and</li>
<br>&nbsp;<br>
<li>a 21-page-per-minute, full-color, full-featured <a href="http://www.danka.de/">Danka</a>
<a href="http://www2.danka.de/jboard/produkte/main_3_2.htmlo?Trans_ID=5388">IC 2100</a> laser printer, which will be used to generate personalized
output for booth visitors
(including, if you want, a nicely constructed KDE pamphlet, with the
option of including a freshly taken digital photo of the visitor at
the booth or with KDE developers).</li>
</ul>
<p><em>User/Developer Presentations</em>.  KDE developers will also offer
two presentations, targeted at developers and
users, respectively, through the LWE Conference Program:
</p>
<ul>
<li> <a href="mailto:blackie@NOSPAM_klaralvdalens-datakonsult.se">Jesper
Pedersen</a>, Senior Software Engineer at
<a href="http://www.klaralvdalens-datakonsult.se/">Klar&auml;lvdalens
Datakonsult AB</a>, will offer the presentation "My work on
developing KDE widgets"; and</li>
<br>&nbsp;<br>
<li><a href="http://www.kde.org/gallery/#konold">Martin Konold</a>, a
Consultant for <a href="http://www.suse.de/">SuSE LINUX AG</a>,
will offer the presentation "KDE - Past, Present, Future".</li>
</ul>
<p><em>Thanks</em>.
The KDE Project would like to thank the <a href="http://www.kdeleague.org/">KDE
League Inc.</a>, Epson and Danka for supporting KDE
at the LinuxWorldConference & Expo 2001.
</p>
