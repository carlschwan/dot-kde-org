---
title: "People of KDE: Wolfram Diestel"
date:    2001-01-23
authors:
  - "Inorog"
slug:    people-kde-wolfram-diestel
comments:
  - subject: "Esperanto?"
    date: 2001-01-23
    body: "Some friends and I learned a bit of Esperanto about 20 years ago just for fun, but I had no idea it was actually used anywhere.  Is it the official language somewhere?"
    author: "Shawn Gordon"
  - subject: "Re: Esperanto?"
    date: 2001-01-23
    body: "Before you get the wrong impression, NO is it is not the official german language ;-).\n\nEsperanto real name is \"Lingvo Internacia\"\nwhich means as much as \"international language an is 100% artificial, invented in 1887 by <i>Dr. Lazarus Ludwig Zamenhof.</i> Esperanto (Hopfull Person) was his nickname. He came up with it with the idea in mind to creat an easy to learn language that could be used to communicate between the different cultures.\n\n\tMax\n\nP.S: just found an link: \n\nhttp://www.esperanto.net/info/index_en.html"
    author: "Max Reiss"
  - subject: "Re: People of KDE: Wolfram Diestel"
    date: 2001-01-29
    body: "Ironically, there is a character set that covers German, Esperanto and English just fine - Latin-3 has all the characters needed to write German."
    author: "David Starner"
  - subject: "Re: People of KDE: Wolfram Diestel"
    date: 2001-02-02
    body: "But when I send a Latin-3-Email to a german \nWindows user, normally he cannot read it.\nWith UTF-8 it works fine."
    author: "Wolfram Diestel"
---
Sometimes wise men say that the quest for speed and comfort drives the humankind away from its cultural vocation. The time might have come, thanks to the free software credo, to reconcile our quest for technology and our thurst of culture. <a href="http://www.kde.org/people/wolfram.html">Wolfram Diestel</a> is one of the people that work towards this goal. Wolfram is <a href="mailto:tink@kde.org">Tink</a>'s guest this week, and he presents his work on esperanto translations of KDE. Go read the interview at <a href="http://www.kde.org/people/people.html">KDE people's page</a>.

<!--break-->
