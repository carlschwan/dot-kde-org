---
title: "SecureONE and KDE In The Business"
date:    2001-11-01
authors:
  - "jbacon"
slug:    secureone-and-kde-business
comments:
  - subject: "Hint to RedHat"
    date: 2001-11-01
    body: "Hello RedHat...  This is not the first article or business using RedHat Linux with a KDE Desktop but RedHat fights the customer and uses GNOME default.   Earth to RedHat, give us a KDE default!  It is more polished for the enterprise and gives a better first impression of Linux."
    author: "ac"
  - subject: "Re: Hint to RedHat"
    date: 2001-11-01
    body: "Yup... one of the reasons why i use Mandrake... it may not set KDE as default... but at least it asks whatever i want a KDE or Gno-go workstation.\n\nOhh... THUMBS UP (congrats, this is the milionth time ;-) to the KDE core team for making an eXelent framework!!!\n\n/kidcat\n--\nhardtoreadbecauseitissolong"
    author: "kidcat"
  - subject: "Re: Hint to RedHat"
    date: 2001-11-01
    body: "It would be spectacular to see Red Hat use KDE as the default.  The only problem is that they are some of the original Gnome supporters.  Wouldn't it be sort of a conflict of interest for them to have KDE as default?\n\nJust a quick reality check :-)"
    author: "Steve Hunt"
  - subject: "Re: Hint to RedHat"
    date: 2001-11-01
    body: "They might have some of the original GNOME guys but part of doing good software is knowing when to accept and cut your losses.  \n\nGNOME has lost alot of steam from what I've seen when Ximian went down the .NET system route.  RedHat is the last major supporter of GNOME in the commerical realm."
    author: "ian"
  - subject: "Re: Hint to RedHat"
    date: 2001-11-01
    body: "I really love KDE, I use it all the time, and I can't see myself going over to Gnome any time soon.\n\nHowever to say that Gnome has lost a lot of steam is wrong. Gnome is in the same limbo KDE went through in the 1.1.* days when we waited with a primtive dog ugly desktop while KDE 2 was being developed. Back then Gnome looked like the obvious choice, and everyone thought KDE was going to crumble.\n\nThe tables have been turned now though, KDE is the one with the looks and features, but I wouldn't get cocky. There's a lot of advanced stuff going into Gnome 2 (particularly under the hood) that should go a long way to evening the playing field just as KDE 2.0 did when it came out. The result should be a far more polished system, and one which integrates far better with KDE, which is something I'm really looking forward too. Imagine editing an image in the Gimp, copying it, then pasting it into KWord.\n\nIt's gonna be great."
    author: "Bryan Feeney"
  - subject: "Re: Hint to RedHat"
    date: 2001-11-01
    body: "not to get pedantic about it (ok, i will), but GIMP will not benefit from GNOME2. it benefits from Gtk+2. GIMP does not use the GNOME libs.\n\nsecondly, don't expect GNOME2.0 to be a 100% uniform platform where all the apps have been ported over and everything is seamless. 2.0 is going to be a platform release with the libraries in order and core apps ported over as much as possible. the next release after 2.0 will probably be the one that has all the applications in line and is more visibly improved from a user's perspective when comparied to 1.4. (this is according to the GNOME developers on the GNOME lists, and i assume they know what they are talking about.)\n\nthis is an unfortunate turn since it means a bit of a longer wait until the interoperability we've all been waiting for is truly here. 2.0 will be a great step in the right direction, but there will still be work to be done. basically they are stuck between releasing on schedule or pushing back their timeline yet again (which they are not going to do).\n\nbest of luck to all the developers!"
    author: "Aaron J. Seigo"
  - subject: "Look at the good things in Gnome"
    date: 2001-11-02
    body: "Thank you for these interesting news. Perhaps some day you will make a KC Gnome ? ;-)\n\nI have tried the last Gnome (I'm curious, too). I had some difficulties to find a light theme, however not very light.. I was astonished by all the options, too many, too heavy... however some ones are missing (for example, I didn't found how to maximize, de-maximize when double clicking on a title bar) and some ones very usual are not by default (for example Alt Tab for changing tasks, it is W-C-Tab, I don't know what it is, it looks like some emacs things, very simple for every one)...\n\nWell, I am off topic, but I looked at gnome for searching some interesting things, and I found three ones :\n\n- In every (or many) Gnome program, any menu may be separated. In KDE, it is only true in the K menu and in one alone menu of one alone program (at least, because I didn't tried all the menus of each program). It is in Konqueror file menager when files are shown with details, the menu Display - Show details. It would be a great thing to have the ability to separate all menus in every KDE program. For example, in a game, you will play a new game (or search a hint) only in one click on the separate menu, instead of two clicks in the integrated menu.\n\n- In Galeon, there may be sheets (\"onglets\" on french) inside a window, so, that you may put all the pages of a site (or a search) in the same window. It would be good in Konqueror...\n\n- Also, it would be good in Konqui to display the mp3 tags, like in Nautilus. In this URL http://www.del.f2s.com/images/screenshots/qnix_full02.jpg, I see that it is done, but it seems to be a specific plug-in of Konqueror, not in my 2.2.1 version, not in the future version (?)."
    author: "Alain"
  - subject: "Re: Look at the good things in Gnome"
    date: 2001-11-03
    body: "> - Also, it would be good in Konqui to display the mp3 tags, like in \n> Nautilus. In this URL \n> http://www.del.f2s.com/images/screenshots/qnix_full02.jpg, I see that it is > done, but it seems to be a specific plug-in of Konqueror, not in my 2.2.1 \n> version, not in the future version (?).\n\nIt will most probably be possible in KDE3, not only for mp3 ID-tags tho, but for different kinds of meta-data."
    author: "Carsten Pfeiffer"
  - subject: "Re: Look at the good things in Gnome"
    date: 2001-11-03
    body: ">> - Also, it would be good in Konqui to display the mp3 tags, like in  Nautilus\n>  It will most probably be possible in KDE3, not only for mp3 ID-tags tho, but for different kinds of meta-data.\n\nVery good thing ! I hope it will be better than in Nautilus, with all the tags (including album title, comment, year...)"
    author: "Alain"
  - subject: "Re: Look at the good things in Gnome"
    date: 2001-11-03
    body: "And the other thing - when I highlight some option in menu and press, for example, Ctrl-C this key becames a shortcut for this option."
    author: "Krame"
  - subject: "Re: Look at the good things in Gnome"
    date: 2001-11-04
    body: "this already works for kDE2 apps. try it."
    author: "FO"
  - subject: "Re: Look at the good things in Gnome"
    date: 2001-11-04
    body: "I was just about to post that it didn't work, but then I found the secret:  You have to hold down the mouse button while you hit the shortcut key (!).  It is a well-hidden feature.  I don't see any reason you should have to do that.  It is cool anyway, though!"
    author: "not me"
  - subject: "Re: Look at the good things in Gnome"
    date: 2001-11-06
    body: "My god, why didn't someone tell me about this? That is soooo cool, and I have used KDE for a year and never figured it out!\n\nI think this should definitely go in the tips database...\n\nd"
    author: "dave"
  - subject: "Re: Hint to RedHat"
    date: 2001-11-01
    body: "I think people here are a bit hard on Redhat, admittably they are a bit slow in producing updated RPMS for KDE, but since RH7.1 they've included KDE 2 (KDE 2.2 on the most recent 7.2), and when you install you're asked whether or not you want a KDE or Gnome Desktop."
    author: "Dr_LHA"
  - subject: "Re: Hint to RedHat"
    date: 2001-11-01
    body: "Huh?!  RedHat 7.2 comes with KDE 2.2.1  Also, when I first got 2.2.1 RPMs from two separate sources, each collection was full of bugs that are (happily for me) absent from the canned installation that comes with RH7.2\n\nAlso, RH7.2 *does* ask which desktop should be your default.  Then it leaves everything alone.  Doesn't the letter G (Gnome) come first in the (US English) alphabet before K?  Perhaps that's why it's the first radio button checked (in nearly all RH distros since 6.2) -- I'm speculating like a mad man, now :-)\n\nBut what's with all the bitterness toward RH?"
    author: "Jon"
  - subject: "Re: Hint to RedHat"
    date: 2001-11-01
    body: "Well, it comes down to this.  There are three really big Linux distrobutions:  RedHat, SuSE and Mandrake.  RedHat sucks the most with KDE.  Even some of the smaller distros put more time and effort into KDE.\n\nI use RedHat at work and SuSE at home and I can attest to the fact that it's much easier to work with the SuSE & KDE combo.\n\n-Scott"
    author: "Scott"
  - subject: "Re: Hint to RedHat"
    date: 2001-11-01
    body: "You should check out www.pclinuxonline.com they have been a mandrake site in the past but are now supporting Redhat as well. They have some KDE rpms over there as well.\n\nCraig"
    author: "craig"
  - subject: "Re: Hint to RedHat"
    date: 2001-11-02
    body: "Well, Mandrake was born because Red Hat didn't support KDE."
    author: "reihal"
  - subject: "Re: Hint to RedHat"
    date: 2001-11-01
    body: "I would love to see Redhat start to support KDE in a much bigger way. Gnome is just to incomplete. KDE is the obvious technical choice.\n\nCraig"
    author: "craig"
  - subject: "Re: Hint to RedHat"
    date: 2001-11-02
    body: ">Gnome is just to incomplete\n\nGnome is off topic. Go talk about gnome using the appropriate mediums; this is a news article and a news site about kde."
    author: "Carbon"
  - subject: "hmm"
    date: 2001-11-01
    body: "if you guys really want to send a message to rh to ship KDE as default, write a feature request to redhat's bugzilla :)."
    author: "FO"
  - subject: "Off Topic"
    date: 2001-11-02
    body: "Where is kover gone? Since the release from the 30.10 it is simply gone. Homepage erased, files gone. Anyone who's got a copy of the 0.8.1 release?"
    author: "ElveOfLight"
  - subject: "Re: Off Topic"
    date: 2001-11-02
    body: "Eh..\n\nWorks just fine going from the link currently at www.kde.org.\n\nLocal DNS problem at your end perhaps ?\n\n/Ulf"
    author: "Cadwal"
  - subject: "Re: Off Topic"
    date: 2001-11-02
    body: "nope. redirect link to lisas.de/kover was a 404 all the time. now it s just fine."
    author: "ElveOfLight"
  - subject: "Cost of software could be more than dollars"
    date: 2001-11-02
    body: "Hi,\n\nI've seen it a few time now where the cost of a linux distribution is seen to be a major factor when considering using a linux distribution. But isn't the software they use a linux community effort? So maybe there should be more encouragement for linux user to become part of the community. Getting involved somehow in making the software better. There's bound to be something that suits the user. And yes I think this interview is a great donation by SecureONE! \n\nBy the way the GNOME vs KDE thing is not so interesting. Yes it's great to have a choice in desktop. And in the long term _all_ benefit by the competition between the desktop choices. But there's no rule that say competitors can't work together to maximize their performance. So let the games begin!\n\njust my 2 cents worth."
    author: "Keith"
---
I have just added <a href="http://enterprise.kde.org/interviews/secureone/">our second interview</a> with a business to <a href="http://enterprise.kde.org/">KDE::Enterprise</a>.  This time the focus is on Internet security company <a href="http://www.secureone.com.au">SecureONE</a>, who tell us just why they have chosen to use KDE on their Red Hat workstations. <i>"Our understanding of KDE's backend technologies helps us appreciate the framework and end-user interface even more. The ability to drag and drop between pretty much all applications, common dialogs, etc.... makes it a more usable desktop for our users. "</i>