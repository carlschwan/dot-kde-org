---
title: "Ripping Audio CDs Made Child's Play"
date:    2001-03-13
authors:
  - "Dre"
slug:    ripping-audio-cds-made-childs-play
comments:
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "This is sooooo cool :)\n\nI've been using this for some time now (since the day the commited it to the cvs), and it works like a charm!  Ripping audiocds have NEVER been easier...\n\nKEEP IT COMING!!!............"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "I should add, that the code for wiring mp3 and\nogg encoding to the grabbing was developed by\nCarsten Duvenhorst."
    author: "Michael Matz"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "The hits just keep on coming!! Glad i'm not one of the xiamian investers. Thats what happens when you believe to much press and to much slashdork hype.\n\nCraig"
    author: "Craig black"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-14
    body: "Theres stuff just like this in GNOME allready."
    author: "robert"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-14
    body: "I went to gnome.org and freshmeat and found at least 13 apps that do the same thing written for GNOME.  They just aren't integrated into a browser, which is probably a good thing, protozilla lets you do that though."
    author: "John"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-15
    body: "Theres an MP3-OggVorbis DB and player integrated into natilus.  Theres almost too many GNOME mp3 DB's for GNOME(like 20), it's good to see that KDE is cathing up on the software side of the desktops though, it's allready superior as a desktop, there's just not as much good software for it as there is for GNOME."
    author: "Lee"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-15
    body: "Huh? What good software Nautilus and redcarpet. Is there anything else? Gnome office? Is there such a thing? Please please don't say open office its not a gnome app. look at there app page compared to apps.kde.com. It even looks like crap. The apps are all amateur trash. What major project thats not corporate is going on there? Its all corporate in gnome land. Nothing grass roots is going on there anymore.\n\nCraig"
    author: "Craig black"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-15
    body: "I use nothing but KDE as a windows manager and I prefer Qt/DKE apps, but I find myself using about 60% Gtk/GNOME apps, 20% motif apps 10% Qt/KDE apps and 10% something else(actually I use about 75% of my time in vi/ksh).  Maybe it's just me but most of the stuff I need is not written in Qt or KDE, though I wish it was, I realy prefer KDE and yea I think that they need to catch up on apps, but the thing is that the KDE people are catching up.  Hopefuly in a year Gtk and Qt will switch places on my comparison above and it's looking like they will."
    author: "Lee"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-15
    body: "What apps? I hav't seen any real good gnome apps. Grip and gimp are not gnome apps. What else is there? Am I missing something? I'm not counting Nautalus and redcarpet because they were developed by people with corporate interests. If you enclude those you have to through in the Kompanies apps and then of course kde wins by a landslide.\n\nCraig"
    author: "Craig black"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-16
    body: "I use Gnumeric, gvim, gimp(though it's a gtk app), GNUcash, GBurn Xchat and a few apps that aren't GPL for work, like DB front-end stuff(too small to buy a Qt liscence, and some we bought).  I realy don't want to get in some sort flame war, but I use mostly Gtk/GNOME apps.  I realy think that KDE is a better desktop and it's what I use for a window manager, I also use a ton of windows apps not because windows is better it just has that stuff I need(im using IE right now).  If there is a KDE replacement to any of the apps that is just a functional please show me I'd much rather use it.  And again I'm not tring to start a flame war."
    author: "Lee"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-15
    body: "Man I'm sure you'd feel stupid if you went to freshmeat and saw that there's over 3 pages of GNOME mp3 rippers.  For future refrence you look like an idiot if you post stuff that is just wrong, people can't ever take you seriously now.  You can hope that people took your post as a troll except on dot.kde.org that's not realy a troll post, so I guess they either take your word or know that your an idiot."
    author: "KDEguy"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-15
    body: "Hey there friend i know theres gnome mp3 rippers. Thats not the point. I use grip all the time even though its not really gnome just gtk. I'm excited at the rate of kde advantacement compared to gnome. The kde developers are really kicking butt.\n\nCraig"
    author: "Craig black"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-15
    body: "Your an idiot. Read my post before run your cake hole."
    author: "Craig black"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "<p>This is a great feature.</p>\n\n<p><i>However,</i> I have one question to ask: I'm not sure how the kioslave stuff works - but I would expect it's pretty modular and plugin like. \n\n<p>In this case would it be possible to distribute updates of this kind as small files (rpm or tar.gz whatever) so we don't have to wait for an entire update of KDE to get it and test it? I'd like to add this functionality to my KDE now without going to CVS.</p>"
    author: "Dr_LHA"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "> However, I have one question to ask: I'm not\n> sure how the kioslave stuff works - but I\n> would expect it's pretty modular and plugin\n> like. \n\nRest assured that the kioslave stuff is very modular ; so much so that you can create your own and distribute it as you would a standalone app.  Even the ones that come with kdelibs and kdebase by default can be ripped out, packaged by themseleves and distributed. They were not packaged by themselves because some of them are sort of more essential than others.  For example, the three ioslaves that come with kdelibs: the file, ftp and http io-slave are important to doing anything on the desktop.  \n\nRegards,\nDawit A."
    author: "Dawit A."
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "Hey, that's pretty slick!\nI'm not a KDE user, but I've got to say 'Way to go KDE team'. That's a real nice feature."
    author: "Geoff Lane"
  - subject: "audiocd:/Ogg Vorbis ?"
    date: 2001-03-13
    body: "Are you allowed to have spaces in the URL?  Shouldn't that be audiocd:/Ogg%20Vorbis ?  I dunno if it causes problems, but it probably does look nicer as it is.\n\nOtherwise, rock on!"
    author: "KDE User"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "This is very cool. One question, will this take an MP3 and write it back to wav format, so it can be burned to an audio CD? If not, I'd like to add this to a wish list of features."
    author: "Harvey London"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-14
    body: "Yes, I want that too.\nIntegrated CD-Burner software in Konquerer would be so great...."
    author: "Michael"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "um, excuse me, but holy ****!  This is too awesome.\n\nOne thing though, I wanna know how it's supposed to get the album/song names too, now that someone bought and commercialized CDDB and KDE isn't allowed to use it anymore.\n\nand I wanna know how they mixed the marble and beos themes in that audiocd screenshot :o)\n\nWhat I wouldn't give to be able to use kioslaves from the console!"
    author: "David Walser"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "http://www.freedb.org/\nnuff said :)"
    author: "Matthew Parslow"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "<p>>What I wouldn't give to be able to use kioslaves from the console!\n\n<p>I agree, why should these be limited to the desktop. How hard would it be to let console programs use kioslaves.\n\n<p>Wouldn't it be great if I could issue the command:<br>\n\n<i>cp http://www.kde.org/somepic.png floppy://pics</i>\n\n<p>Maybe kioslaves belong outside kde?"
    author: "Dan"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-14
    body: "try:\n\nkfmclient copy http://www.kde.org/somepic.png floppy://pics"
    author: "JPK"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "<p>>What I wouldn't give to be able to use kioslaves from the console!\n\n<p>I agree, why should these be limited to the desktop. How hard would it be to let console programs use kioslaves.\n\n<p>Wouldn't it be great if I could issue the command:<br>\n\n<i>cp http://www.kde.org/somepic.png floppy://pics</i>\n\n<p>Maybe kioslaves belong outside kde?"
    author: "Dan"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-14
    body: "kfmclient copy http://www.kde.org/somepic.png floppy://pics\n\nYou could write a shell script to wrap \"kfmclient copy\" and make it behave like regular copy, if you really want it."
    author: "Roberto Alsina"
  - subject: "virtual directory"
    date: 2001-03-13
    body: "Looks great!\n\nThis leads me to a UI issue: Could we get some \"virtual directory\" icons in there? It would be good to convey to the user that the icons don't represent actual data yet but instead a process that will create that data.\n\nThe usual paradigm is one where you select the verb first (open the tool you are going to use to do your work) and then the noun (select the files to work on). An verb-noun order example would be opening a word processor then opening a file to edit. An example of noun-verb order is the microsoft \"open with...\" right click popup menu. Noun-verb seems more natural. Does anyone have a link to some good methods of representing this on the desktop?\n\nKDE is taking steps toward a unique UI experience that I think will differentiate it. Perhaps we could add something that will indicate the verb a little better.\n\nAnyways, congradulations on helping KDE move beyond the current paradigms. :)\n\n-pos"
    author: "pos"
  - subject: "Re: virtual directory"
    date: 2001-03-13
    body: "Just like links that are represented with a small arrow, to-be-computed-datas could be represented with a small gears (like kde one)\n"
    author: "Olivier LAHAYE"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "<p>Cool.  But now, can I do the reverse?  For example, insert a disc of MP3's, and reconstitute them as WAV files on the hard drive -- complete with a total time/size listing in the target?\n\n<p>This would make it quite easy to create audio discs for the car from MP3 collections..."
    author: "friartux"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-08-30
    body: "Try eroaster for burning Audio CDs directly from your MP3 Files. Last week, the program gets Ogg-Vorbis Support too."
    author: "assassin"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "Pretty cool!  I didn't know that the audiocd:/ interface was present in 2.1.  I've gotten a little impatient and really don't want to toy with CVS so I tried to implement this using MIME types and have had partial success so far.  Maybe someone can help me out, here's what I did:\n\nIn Konqueror select Go->Applications\n\nIn the Applications directory right click and Create New->Link To Application and name it Ogg Vorbis Encode.desktop.\n\nUnder the Execute tab in the Command box enter\noggenc -o /home/john/mm/mp3/%f.ogg %f\nand select Run in terminal.\n\nUnder the Application tab highlight audio/x-wav and click the << button. Click OK.\n\nNow, when you right click on a .wav file you'll see Ogg Vorbis Encode as an \"Open with\" option.\n\nHere's the problem, when KDE executes the above command it's not replacing the %f with the name of the file you're trying to encode and the encoded file ends up being named \".ogg\"\n\nCan anyone point out what I've missed here?  It seems like %f worked in KDE 1.0, but it's not working here. \n\nThanks"
    author: "John Coleman"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "Well, you know, the BeOS already does something like this; in other words, it does the ripping but not the automatic encoding.\nCool to get it on GNU, though!"
    author: "FnordLord"
  - subject: "mixed-mode cds"
    date: 2001-03-13
    body: "(I haven't test audiocd:/ system yet)\nThis looks very cool for audio cds, but what about mixed-mode and data-cds? \nis it bad idea to add in future example cdfs:/ -file system to konqueror\nwhich opens a tracks for anykind of cdroms just like cdfs-filesystem patch?\n( http://www.elis.rug.ac.be/~ronsse/cdfs/ )\nIt just makes possible to make iso-images and audio-tracks \nripping (and maybe cdrdao images)for drag and drop style."
    author: "Onni Kumpunen"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-13
    body: "I have a few questions:\n\nWhat about the bitrate? Is it fix? Can I change it?\n\nCan I edit the CDDB/ID3TAG info myself as well?\n\nAnd one more question: I think you said it uses paranoia library for ripping? I know that there might be difference in quality of ripping the same song when doing this more times. Esp. when the disc is scratched, dirty, etc, it can be ripped badly (with more or less successfully corrected errors). Will then KDE tell me about such problems?\n\nThank you,\nWanthalf\n"
    author: "Wanthalf"
  - subject: "Misplaced functionality"
    date: 2001-03-14
    body: "First of all I want to thank you for contributing to the kde project, but I have a few remarks. \n\nI seems to me that not al the functionality should be implemented in the IO slave. \n\nIt is a good idea to have different views concerning renaming of \"TrackXX.cda\" to \"<Artist> - <songname>\", but the views concerning the virtual .wav, .mp3, .ogg files are out of place. \n\nThe proposed (and implemented) method provides (an interface to) the functionality of converting .cda files to .wav/.mp3/.ogg formats. BUT this functionality is only provided when you're copying from a CD (audiocd:/). \n\nA different interface has to be implemented to provide he same functionality when copying/converting .cda/.wav files from a normal directory (file:/). This is the tell tale sign of functionaly implemented in the wrong place.\n\nWhen a file is moved you get a little popup menu that lets you choose between 'copy here', 'move here' and 'link here'. This popup menu could be extended in such a way that conversions can be done on the fly AND from any kind of url.\n\nSo when one drags a .cda file the popup menu should give a choose to 'copy here', 'move here', 'link here', 'convert to wav here', 'convert to mp3 here' and 'convert to ogg here'. Similar options could be provided for the .wav, .mp3 and .ogg file formats.\n\nThe right click popup menu should provide similar options.\n\nI hope you find my comments usefull, if you have any questions, just e-mail me or reply, I'll be happy to clarify my comments.\n\nJohan Veenstra."
    author: "Johan Veenstra"
  - subject: "Re: Misplaced functionality"
    date: 2001-03-14
    body: "I agree with you. The ioslave as it is, is a nice idea and is very useful. It makes me use only one sofware ! (like the ftp ioslave). So it is very great. The need for virtual files and a folder with the title of the album is clear to me, not only in this specific context. \nBut the conversion function is confusing and mixes concepts,as you mentionned it. I probably  canno't compress .wav files to .mp3 if I have them on my disk even if the code has for sure been implemented in the audiocd-ioslave.\n\nI think the idea of extending the functionnality of copy/move/link to copy/move/link/convert could be usefull, not only for ripping or converting music but for any kind of files conversions or for backups. \nBut if you want to convert a file, you don't always want to move it. So an other convert-icon on a toolbar too would be great too (as the filter-icon is).\n \nCheers to all kde people here :-)"
    author: "Xavier"
  - subject: "Re: Misplaced functionality"
    date: 2001-03-14
    body: "This would be a great enhancement indeed although the audiocd-kioslave as it is now is a great thing, too.\nThe most flexible way would be to register additonal drop-menu items for certain file types \"on the fly\" that is, an \"extension\" can add such a menu item if it's loaded or if KDE is started. (\"Uncompress to directory Archive.tar.gz\" if dropping a tarball?)\n\nSounds like a great idea! However I doubt that this will make it into KDE 2.2... :-(  )\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-14
    body: "I think that this is great.\n\nWill this be able to send info to CDDB, like grip.\nAlso will it fill out ID3 info for mp3s, and the same for oggs. \nCan you name the files before you rip them. \n\nI think that this should be possible to do to any wav file on your harddrive.\n\nMaybe something like this. When you right click on a wav file, it has an option for extend veiw, an extra panel will open in konquerer where these options are aviable."
    author: "Mark Hillary"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-14
    body: "This is so exciting!\n\nJust one question, a little off topic ...\n\nWe are getting so many new tags for konqueror -- smb:/, audiocd:/, floppy:/, etc.\n\nDoes anyone know what the entire list is?  Is it posted somewhere?\n\nJon"
    author: "Jon Doud"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-14
    body: "I don't know of a list of all ioslaves ever written, but you can find a list of those that are installed on your computer by going to the control panel, choosing the item about information, and then clicking on the \"ioslaves\" one."
    author: "jliechty"
  - subject: "kioslave list"
    date: 2001-03-24
    body: "check this \nhttp://www.andamooka.org/reader.pl?anid=kde20develkde20Bkioslavebase_h&cid=1#Akde20Bkioslavebase_h"
    author: "ignoto_deo"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-03-15
    body: "Not mentioned:\n\nYou will need the latest libmp3lame (  > lame 3.88a) from the CVS to be able to encode to MP3.\nlame 3.87 will NOT work.\n\nlame is available at www.mp3dev.org;\n\nLame CVS Tarballs at http://cvs.sourceforge.net/cvstarballs/lame-cvsroot.tar.gz"
    author: "Carsten Duvenhorst"
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-04-29
    body: "I can't wait for the next release (2.2) with this GREAT, AMAZING, WONDERFULL, fetaure. Great work!"
    author: "Jose Andres Martinez S."
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-04-29
    body: "I can't wait for the next release (2.2) with this GREAT, AMAZING, feature. Great work!"
    author: "Jose Andres Martinez S."
  - subject: "Re: Ripping Audio CDs Made Child's Play"
    date: 2001-06-06
    body: "wouldn't it be a lot easier if there would be an option somehow on selecting a kioslave to use, because the amount is growing rapidly. (e.g. I even read that someone is developing a diamond rio kioslave.) . maybe you could have an option just like network and root etc, named kioslaves or something, and there list them so everybody knows what is available."
    author: "Nick"
  - subject: "Which MP3 library?"
    date: 2001-09-17
    body: "When compiling KDE2.2, I have trouble linking the audiocd files.\nThe various MP3 libraries do not contain functions like:\n\n\tlame_set_VBR\n\nI have these versions available, and I don't see the functions:\n\n\tlame-3.13 lame-3.89 lame3.50 lame3.70 lame3.87\n\nSo, which lame is the code to be compiled against? I know that\nyou prefer Ogg Vorbis, but portable Ogg Vorbis players are\nrather rare."
    author: "Roger Oberholtzer"
  - subject: "Hmmmm...."
    date: 2004-10-14
    body: "Its kool. ond quetchion tho, when u rip files from a cd, does it reck the original copy?"
    author: "ebbah"
  - subject: "Setting MP3/OGG Bitrate and Other Options..."
    date: 2005-09-25
    body: "KMenu->Settings->Control Center->Sound and Multimedia->Audio CDs"
    author: "Stuart Morrison"
---
Last week, <A HREF="mailto:matz@kde.org">Michael Matz</A> contributed some code
to the <EM>audiocd:/</EM> IO slave in KDE CVS to make ripping audio CDs child's play.  When you browse an audio CD with this new code (using, for example, 'audiocd:/' in <A HREF="ftp://ftp.kde.com/pub/screenshots/audiocd/audiocd1.png">Konqueror</A>), you will find two subdirectories:  <EM>MP3</EM> and <A HREF="http://www.xiph.org/ogg/vorbis/">Ogg Vorbis</A>.  Each subdirectory will list
all the tracks on the audio CD with the appropriate extension (<A HREF="ftp://ftp.kde.com/pub/screenshots/audiocd/audiocd3.png">.mp3</A> and <A HREF="ftp://ftp.kde.com/pub/screenshots/audiocd/audiocd2.png">.ogg</A>).
Each track can then be <A HREF="ftp://ftp.kde.com/pub/screenshots/audiocd/audiocd4.png">dragged</A> to any KDE drop location and be converted automatically into the MP3 or OGG compressed formats.  In addition, if the track has a CDDB entry, the file is automatically named for you and the .MP3/.OGG tags are set appropriately.  The obligatory screenies are linked in the text above.  Expect rollout for KDE 2.2, currently <A HREF="http://developer.kde.org/development-versions/kde-2.2-release-plan.html">scheduled</A> for beta release on April 2. How's that for simplicity?  A more complete explanation from Michael is available below.

<!--break-->
<P>&nbsp;</P>
<P>
Michael Matz explains how audioCD ripping works:
</P><P>
Audiocd:/ creates a virtual view (or better multiple views) of the content
of audio CDs.  These multiple views are collected in virtual directories
which all (besides one exception) are subdirectories of the root directory.
The actual set of views available to the user depends on how kio_audiocd
was compiled, and if a network connection was possible (for the CDDB request).
Most of these views consist of one file for each audio track.  The difference inthe views is only the actual name of these files, and the content, when
those (up to then virtual) files are read:
</P>
<OL>
<LI>There is one view listing the tracks by number in "By Track/"; the
   files are named "Track XX.wav" with XX being a number.  When read they
   are normal .wav files.</LI>
<LI>One view lists the track by track-name; in "By Name/".  The files are
   called "XX <name>.wav", where 'name' is the track title as it comes
   from CDDB.  The content of these files are identical to those from (1),
   just the name is different. This view only exists if a network
   connection exists for CDDB lookups.</LI>
<LI>Another view has the same content as (2) (same name and content of the
   .wav files) but is located in the directory "<ToD>", where ToD is the
   Title of the Disc coming from CDDB.  If there was no CDDB query this
   view nevertheless exists, in which case the title is "No Title" and the
   track names are "Track XX.wav" (so the content is identical to (1)).
<LI>If libmp3lame was available when compiling kio_audiocd, there is a view
   in the directory "MP3/"
   (<A HREF="ftp://ftp.kde.com/pub/screenshots/audiocd/audiocd3.png">screenshot</A>).
   The names of the files there are the same as
   used by (3), except the extension is ".mp3" instead of ".wav".  When
   read, those files are normal mp3 files (they are coded on the fly,
   while ripping the track/reading the file).</LI>
<LI>If libvorbis was available when compiling kio_audiocd, there is a view
   in the directory "Ogg Vorbis/"
   (<A HREF="ftp://ftp.kde.com/pub/screenshots/audiocd/audiocd2.png">screenshot</A>).
   The names are the same as in (3), except now the extension is ".ogg".
   When read those files are Vorbis Ogg files (created the same way as (4).</LI><LI>The root directory itself has a view.  The files are in the form
  "trackXX.cda"
  (<A HREF="ftp://ftp.kde.com/pub/screenshots/audiocd/audiocd1.png">screenshot</A>),
  which contain the raw headerless data of
   this track (the exact same info as the .wav files, without the 44 Bytes
   header).</LI>      
   <LI>The are two other directories which should be ignored for now:
   "Information" and "dev").</LI>
</UL>
<P>
It might be good to know, that some of the strings above are i18n()'d
(internationalized).  These include the "Track XX", "By Name", "By Track",
"No Title" and "Information" strings.  The files in the "MP3", "Ogg Vorbis" and
"dev" directories and the "trackXX.cda" files are not i18n()'d.
</P>
<P>
As should now be clear, the existance of certain directories depends on
compile time environment and runtime environment and is not changeable by
the user (except the CDDB information depends on a network connection).
The files in most subdirectories are virtual, because they don't exist,
similar to e.g. the pop3 slave which also looks as if it provides "files"
although these are messages on some mail-server. But the virtuality makes
no difference, as no user of audiocd:/ could tell if this or that
particular URL now is a file, or not, which of course is true for all
URLs, not only the audiocd:/ ones).
</P>
<P>
When a URL from one of the "MP3" or "Ogg Vorbis" directories is dropped,
its content is copied to the destination.  For (1)-(3) this results in
.WAV files, for (4) and (5) in some compressed audio files (corresponding
to .MP3 and .OGG, respectively), which additionally have encoded the
CD-title, author and trackname if CDDB was available at the time (those file
formats provide a means to encode such auxilliary information).
</P>
