---
title: "Fer de Lance - Truly Intelligent Multimedia Browsing"
date:    2001-05-07
authors:
  - "Inorog"
slug:    fer-de-lance-truly-intelligent-multimedia-browsing
comments:
  - subject: "Re: Fer de lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "The Dutch scientist Ben Schouten recently published on this subject: How does a computer recognize a cow? \n(http://www.cwi.nl/cwi/press-releases/2001/PhD-Schouten-0301.html). He wrote some software to it too. Don't know if it's GPL though."
    author: "Jos van den Oever"
  - subject: "Integrating retrieval into the desktop"
    date: 2001-05-07
    body: "Hi,\n\nThanks for the link.\n\nWork about content-based image retrieval has been around for quite a while. This field is very active, and I hope it continues to be.\n\nFer-de-Lance is an effort to bring this work to the normal user, and to show that retrieval services like the ones the GIFT and similar systems can offer are useful for daily work. \n\nAt the same time this can give an impulse for research on query engines that are *more* useful for the user.\n\nThe task is huge, but there are many useful milestones, so things should be fun to do. \nCheers,\nWolfgang"
    author: "Wolfgang Mueller"
  - subject: "Re: Fer de lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "Can anyone translate that? I can't read dutch :-("
    author: "Fabian Wolf"
  - subject: "Re: Fer de lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "If you are interested in the field as a whole, try looking for\n\nContent-Based Retrieval, CBR\nContent-Based Image Retrieval, CBIR\n\nOr look for systems:\nViper (the query engine of the GIFT)\nPhotobook (MIT, relatively and famous)\nQBIC (Query By Image Content, IBM)\n\nThe pointer page of the group I am working in has much more links like that. (http://viper.unige.ch/other_systems/)\n\nCheers,\nWolfgang"
    author: "Wolfgang Mueller"
  - subject: "Re: Fer de lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "Just a quick note: any search for CBR is likely to turn up pages for Case Based Reasoning, not the same thing!"
    author: "Rob"
  - subject: "Re: Fer de lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "i only speak a tiny bit, but \nyou can gather the main bits just from\nthe english. the screenshot is not quite\nso enlightening as carsten's, but the \ndescription confirms that it is in the\nsame vain of \"question and answer\" sessions\nto improve the computers ability to recognise.\n\nthe main thing i got was in english though:\nuse of the 'drost effect', in a fractal version of an image the image still takes the same representation even when the details become apparent. nice...\n\nAlex"
    author: "alex"
  - subject: "Link to thesis"
    date: 2001-05-07
    body: "Well, I do speak Dutch, but the link provided is a rather uninteresting press release about Ben's work. His PhD thesis is also available online though, and it is in English (don't let the first few Dutch pages fool you). You can download his thesis from\n\n   ftp://ftp.cwi.nl/\n\nNext, click /pub and then /bens. There are two PDF files. Somehow a direct link to the PDF files doesn't work.\n\nEnjoy,\n\nMartijn"
    author: "Martijn Anthonissen"
  - subject: "Re: Link to thesis"
    date: 2001-05-07
    body: "Dank je wel"
    author: "David"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "Damned, you guys are so bleeding edge. :) Cool stuff!"
    author: "Lenny"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "This is simply cool :)\n\nNow, anybody care to convince google to use this ?  ;)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "Thanks for trying :-)\n\nGoogle etc., they are all aware of the field. They have all people working on it (I suppose), but there is the problem that they earn only a finite number of cents per click, and they cannot spend more floating point ops than they get paid.\n\nFurthermore, if you have really HUGE collections you run into problems that GIFT/Viper does not encounter, because it's dealing \"just\" with a couple (50) of thousands of images in this demo.\n\nCheers,\nWolfgang"
    author: "Wolfgang Mueller"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-08
    body: "Main scalability problems are:\n\n- Speed of the search with increasing number of images, and if you want to create an index instead of linear sarch, how to design the index.\n\n- Quality and relevance of the search results when the number gets huge. This is quite challenging for a general data. This is usually the most important point and probably requires advances in the low level image processing algorithms. This part is usually what CS people are ignoring and focusing on the first part of the problem."
    author: "Solhell"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-08
    body: "Thanks for the clarification.\n\nI take it you're from the business :-)\n\nI rather think that the current focus on indexing and learning techniques is rather a backswing from the times where people were looking for the \"holy grail\" of image retrieval, the \"perfect\" distance measure.\n\nI guess we agree that we need both good learning from interaction, and good image modelling. What I would like to see happen, is that the Fer-de-Lance enabled desktop becomes a playground for cutting edge research, with immediate feedback from interested users.\n\nI also think that for the success of both the GIFT and of Fer de Lance, it is crucial to have an open framework into which everybody who's interested can integrate their plugin to add some functionality. This is research, it is moving fast, so we need to be open for everybody with ideas.\n\nThe GIFT is a framework into which you can add your query engine, and the next step would be now to make it easier for outsiders to add their own feature set for their own file types (i.e. without adding a complete query engine).\n\nWolfgang"
    author: "Wolfgang Mueller"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "Related to this topic, there was a news-item on content-based audio-retrieval at http://www.heise.de/newsticker/data/thd-03.05.01-000/\njust a few days ago (sorry it's in German). \n\nCurrently, it only accepts MIDI files as input, but they say it will be able to query for whistled input in autumn. Very interesting."
    author: "gis"
  - subject: "Window border"
    date: 2001-05-07
    body: "BTW, where did that window border come from?  Looks great!"
    author: "KDE fan"
  - subject: "Re: Window border"
    date: 2001-05-07
    body: "As usual, the great artwork of qwertz :) It's designed as an IceWM decoration, which kwin can make use of, thanks to gallium.\n\nI'm not sure there's a download page for this deco yet, but you might ask qwertz <kraftw@gmx.de> for it."
    author: "gis"
  - subject: "Re: Window border"
    date: 2001-05-07
    body: "Wow, different docorations like that should really be a part of the main KDE distribution (that don't take a lot of space) to show how many different looks KDE can have."
    author: "Matt"
  - subject: "Re: Window border"
    date: 2001-05-07
    body: "It does.. it's from the kde 2.2 cvs.."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "BTW, for those who try out kmrml, you will see that it doesn't provide a history yet, i.e. you can't hit the \"go back\" button from one query and it will bring you back to the previous. This will be fixed in some of the next versions. You might as well send in a patch tho :)\n\nOh, and if you the leave the mrml part (i.e. by hitting the back-button) and spuriously get a Konqueror crash, this might be due to some library unloading problem (static objects involved). Well, I didn't get any, but in case you did, it will get fixed :}"
    author: "gis"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "WOW!  This is really cool for KDE.  What we need now is the ability of KDE application to use images from mrml:// -based sites.\n\nThis way, I could use a KOffice app to search for an image to insert into a presentation or word processing document.  That would be really cool.  The only problem is that the File/Open dialog doesn't handle Non-Direcory-based URLs, so I guess this would take quite a bit of work.\n\nDefinitely, this needs a configuration tool that allows you to keep up with servers/image directories that you find useful.\n\nThis could really become the new way to get clipart for an application.  Now we just some sort of simple interface that allows a KDE-based appliction to get the images.\n\nAnyway, this is way cool! Keep up the excellent work."
    author: "Chris Young"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-08
    body: "Actually, the mrml-slave _could_ return some kind of directory listing, namely all the urls returned by the server. I'm not sure it would be really userfriendly tho. Starting a search from the filedialog? Hmm.\n\nLukas Tinkl is/will be working on an image-gallery for KOffice and that's where kmrml could be integrated easily. You have a thumbnail view of your existing images and can ask for similar images -- simply drag the result into your KWord-document or presentation.\n\nA configuration dialog will definitely be added, soon. As stated in the README, kmrml is currently more proof-of-concept than a final solution. So contributions and also ideas how and where to integrate this are very welcome."
    author: "gis"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "It is fast, it is beautiful.\nhere is my contribution to screenshot collection."
    author: "Antialias"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "Woah!!  That's soo awesome.\n\nI never can believe KDE 2 can look that good as well as that functional.  *drool*.\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "Hey dude ! That looks nice :)\n\nWhen are you going to release those new icon sets ? They look really 'different'."
    author: "Jelmer Feenstra"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-07
    body: "My contribution as well.  Just noticed an extra option when right-clicking on a file.  Check out the right-click menu item :)"
    author: "Chris Young"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-08
    body: "Eeeek!  It's the result of a mad scientist's cross between Windows and MacOS!\n:-)\n\nAcually, that's pretty cool.  Now, if only we could get menu options like this when dragging archives around..."
    author: "not me"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Browsing"
    date: 2001-05-08
    body: "When will konqueror add links to mount points in its tree? So, for example, your tree in konqueror would look like:\n+Bookmarks\n+History\n+Home Directory\n+Network\n+Root\n+System Logs\n+Floppy Disk\n+CD-Rom\n+Windoze\n+BeOze"
    author: "johnson"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Br"
    date: 2001-05-08
    body: "Probably not a good idea to let konqueror try to figure out mountpoints, it could get messy.\n\nA configurable tree, which would enable me to define my own quick-access links, would be cool though.\n\nI would add Printer(configuration and job list) and maybe Scanner to your suggestions (is there a scanner kpart yet?)"
    author: "jd"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Br"
    date: 2001-05-08
    body: "Well, figuring out mount points seems to be pretty easy.\n\nJust call mount, and see what it gives :-)\n\nThat is *probably* not portable, but any platform could get it implemented in minutes."
    author: "Roberto Alsina"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Br"
    date: 2001-05-09
    body: "kio/global.* has code to determine mount points in a portable way already (that is, it has ifdefs for the 4 different types of API !).\n\nAll that's needed is someone to call that and create the desktop files - one question is when, so that it basically happens only once :). Well the version number maintained by konq_tree could be used for that.\n\nAny takers ? Should be quite easy to do.\n\nDavid, who gives priority to KWord for now :)"
    author: "David Faure"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Br"
    date: 2001-05-11
    body: "It sure would be nice to have CD rom and  floppy links in the side bar, by default.\n\nFor the longest time I didn't even know that the SideBar was configurable. \n\nI hardly ever use the \"Go\" Menu item in Konqueror, but now that I found the \"SideBar Configuration\" option in there, I added links to some URLs and to CDROM devices via the right mouse button menu in the folder that comes up.\n\nI think it would be great if there was an alternative way to configure the SideBar\n\nFor example, right click in the SideBar, \nCreate New \n   Link to Location\n   CDROM Device\n   ...\n\nOr, Drag a folder to the sidebar and choose something like \"add URL\"\nAFAIK, Choosing \"Link\" is not good enough to make it appear in the SideBar."
    author: "Risto Treksler"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Br"
    date: 2001-05-11
    body: "It is a good idea !\nJP"
    author: "Martin"
  - subject: "Re: Fer de Lance - Truly Intelligent Multimedia Br"
    date: 2001-05-08
    body: "Drag a directory from konqy to the sidebar.... tada!\n\nJason"
    author: "Jason Katz-Brown"
---
<a href="mailto:Wolfgang.Mueller@cui.unige.ch">Wolfgang M&uuml;ller</a> is the maintainer of <a href="http://www.gnu.org/software/gift/">GIFT</a>, <A HREF="http://www.gnu.org/">GNU</A>'s very interesting Content Based Image Retrieval System (CBIRS).  A CBIRS is a system to search for images based on their content. Wolfgang wrote to <i>the dot</i> recently inviting us to make public his latest efforts to integrate CBIR and related technologies in KDE. He has launched a new project, <a href="http://www.fer-de-lance.org/">Fer de Lance</a>, which is meant to properly integrate GIFT's technology in Free Software desktop environments and browsers. The goal is to have a completely open source infrastructure which can make open source desktops "content-aware".  <a href="mailto:pfeiffer@kde.org">Carsten Pfeiffer</a> found this new development very innovative, and started to write kmrml to provide KDE support for GIFT. Kmrml, which already has a preliminary <A HREF="http://www.konqueror.org/">Konqueror</A> plugin as well as an IO slave, is available <a href="http://master.kde.org/~pfeiffer/kmrml/">here</a> (<a href="http://master.kde.org/~pfeiffer/kmrml/screenshots/kmrml.png">screenshot</a>). Wolfgang and Carsten hope that you will find a minute to read the relevant information at the sites linked above, and then want to contribute to their projects' development.


<!--break-->
