---
title: "KDE 2.1 Has Landed At a Server Near You"
date:    2001-02-27
authors:
  - "Dre"
slug:    kde-21-has-landed-server-near-you
comments:
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Of course, kudos to the developers, artists, beta testers and everyone else who contributed! Fortunately I am installing Linux for a friend tomorrow so I can actually celebrate the release, it's not a real milestone for CVS users. ;-)\n<p>\nBy the way, small typo on The Dot:\n<p>\n<i>It has a whole slew of improvements over 2.1</i>\n<p>\nShould be 2.0, right?"
    author: "Rob Kaper"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Yep -- Thanks.\n\nCheers,\nNavin.\n\nPS Yahooo!!"
    author: "Navindra Umanee"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Great! Again! Thanks to all of you!"
    author: "reihal"
  - subject: "YeeeHaw!"
    date: 2001-02-27
    body: "Propz out to Njaard, tronical, gis, dfaure.. the rest of you :)"
    author: "_shad"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Mandrake packages are missing. I checked also Mandrake mirros, the result is: totally emptiness.\nRegards,"
    author: "Baracuda"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Mirror didn't pick them up?\n\nYou'll have to get in to ftp://ftp.kde.org/ which should have them."
    author: "ac"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Mandrake's RPMS still missing. They are not on ftp.kde.org. I suggest you remove Mandrake's link to KDE 2.1. \nBest selling Linux distro in USA, what a shame."
    author: "Baracuda"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-28
    body: "I'm disapointed in mandrake this is deffinately a black eye on them.\n\nCraig"
    author: "craig"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Same goes to SuSE ones !!"
    author: "RE: How about speed"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "hey ,\n I am also looking for mandrake rpms, :-( i could find them. \nanybody got hold of any mirrors for this. no news at mandrakeforum also.\n\ncraving.."
    author: "Rajil Saraswat"
  - subject: "not finished?"
    date: 2001-02-27
    body: "The packagers probably didn't finished packaging.\nThe announce sais that KDE 2.1 is avaible, that means source code is avaible. I suppose binary packages will be avaibles in a couple of days."
    author: "renaud"
  - subject: "Re: not finished?"
    date: 2001-03-01
    body: "Then clearly the good people at KDE need to pull the link from this page.  They were notified the day of the release that the link was no good (and neither are the links for *most* of the packages) and still they do nothing.\n\nOh, but KDE is still using QT.  And its open source now.  So thats good news.\n\nI guess."
    author: "yttrx"
  - subject: "Re: not finished?"
    date: 2001-03-01
    body: "Then clearly the good people at KDE need to pull the link from this page.  They were notified the day of the release that the link was no good (and neither are the links for *most* of the packages) and still they do nothing.\n\nOh, but KDE is still using QT.  And its open source now.  So thats good news.\n\nI guess."
    author: "yttrx"
  - subject: "hunh?"
    date: 2001-02-27
    body: "I didn't find the tar files up on ftp.kde.org, although the rpms, debs, etc are there...anyone know of a mirror when I can get them?"
    author: "Jim"
  - subject: "Re: hunh?"
    date: 2001-02-27
    body: "Looks like you'll have to get them directly from ftp://ftp.kde.org/ (notice ftp not http).  Ouch."
    author: "ac"
  - subject: "Re: hunh?"
    date: 2001-02-27
    body: "Nope, mirrors won't have anything the main site hasn't.\n<p>\nI contacted David Faure, he's the release coordinator and I'm sure he will fix the problem ASAP."
    author: "Rob Kaper"
  - subject: "Re: hunh?"
    date: 2001-02-27
    body: "Found a mirror that has it (from Slashdot):\n\nftp://bolugftp.uni-bonn.de/pub/kde/stable/2.1/distribution/tar/generic/src/\n\nEnjoy."
    author: "KDE User"
  - subject: "Get Source Here!"
    date: 2001-02-27
    body: "As mentioned below you can get the source packages from the <a href=\"http://ftp.kde.org/stable/2.1/distribution/deb/dists/potato/main/source/\">Debian directory</a> until this is fixed.  Get all the *.orig.tar.gz."
    author: "KDE User"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Sweet!"
    author: "Michael M Nazaroff"
  - subject: "No source packages?"
    date: 2001-02-27
    body: "<i>The source packages for KDE 2.1 are available for free download at http://ftp.kde.org/stable/2.1/distribution/tar/generic/src/ or in the equivalent directory at one of the many KDE ftp server mirrors.</i>\n<p>\nUnfortunately, none of the mirrors seem to have the source packages (many do not even have 2.1 at all, hopefully that changes soon) and it looks like the main FTP site doesn't carry them also."
    author: "Rob Kaper"
  - subject: "There is no main site"
    date: 2001-02-27
    body: "http://ftp.kde.org redirects to many mirrors, with a frequency depending on their bandwidt.\n\nIt seems that not all of the mirrors have updated yet. Be patient. Take 24 hours if you must ;-)\n\nAnd no, I will not say how to reach the master server, since that would ensure no mirror will ever update :-)\n\nSo: KDE 2.1 will not rot in the morning. Doesn't this feel like christmas? Then all be good boys and girls and unwrap the packages in the morning (rpm as well as deb ;-)"
    author: "Roberto Alsina"
  - subject: "Re: There is no main site"
    date: 2001-02-27
    body: "I ment the \"KDE server\" (on SourceForge) as mentioned in the announcement.\n<p>\nAnd actually in our family we exchange Christmas presents the evening before Christmas, so that must be why I am so impatient. ;-)"
    author: "Rob Kaper"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "thanx!<br>\nits great. everything went smooth and these lines r hacked in konqueror 2.1\n\nlocely!\n-gunnar"
    author: "gunnar"
  - subject: "Try Debian Source!"
    date: 2001-02-27
    body: "They will work just as well as generic source!\n<p>\n<a href=\"http://ftp.sourceforge.net/pub/mirrors/kde/stable/2.1/distribution/deb/dists/potato/main/source/\">potato/main/source/</a>\n<p>\nGet all the *.orig.tar.gz these are the original tar.gz files with no modifications."
    author: "ac"
  - subject: "Re: Try Debian Source!"
    date: 2001-02-27
    body: "Unfortunately, configure  is missing in kdesupport_2.1-final.orig.tar.gz. Everything else seems to have it. I'm curious if it's a problem for debian source packages only."
    author: "lesha"
  - subject: "Re: Try Debian Source!"
    date: 2001-02-27
    body: "Good mirror here:\n\nftp://bolugftp.uni-bonn.de/pub/kde/stable/2.1/distribution/tar/generic/src/\n\nThe kdesupport has a configure script in that one."
    author: "KDE User"
  - subject: "Re: KDE 2.1 Has Landed -- with more KWin styles"
    date: 2001-02-27
    body: "There's also a new Win2K KWin style, which is missing in the changelogs ;)"
    author: "Karl"
  - subject: "Re: KDE 2.1 Has Landed -- with more KWin styles"
    date: 2001-02-27
    body: "Thanks.  :)\n\n-N."
    author: "Navindra Umanee"
  - subject: "Conquer Your Desktop"
    date: 2001-02-27
    body: "Can't say it enough but this is the best Linux Desktop release ever!  It's Fast, Stable, Pretty, and so many darn features and apps!  If this does not bring Linux to the desktop, I don't know what will!"
    author: "Anon"
  - subject: "Re: Conquer Your Desktop"
    date: 2001-02-27
    body: "Hmm, \npretty - Oh yeah, it looks great\nstable - what I've tested in this short time, yes\nbut\nfast ???\n\nIt seems to be even slower than 2.0.x, and can't still reach the old 1.4.x performance.\nNo, its definitly not fast."
    author: "Adrian"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "No apt-get line?  Bit of a pain to manually download each package..."
    author: "quinto"
  - subject: "kde.debian.net"
    date: 2001-02-27
    body: "http://kde.debian.net/ has apt-get lines."
    author: "KDE User"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "ftp> prompt\nftp> mget *"
    author: "Scott Wheeler"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Or you should download it using konqueror (if you have an older version of KDE installed) to download files via drag and drop. ;)"
    author: "Bojan"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "No need to do that. Just add: \ndeb http://non-us.debian.org/debian-non-US stable/non-US main contrib non-free\ndeb-src http://non-us.debian.org/debian-non-US stable/non-US main contrib non-free              to your /etc/apt/sources.list and apt-get update. You should then be able to apt-get install kdebase and the deps taken care of in the usual way. Alternativley, use dselect, update, and then you can select the parts of KDE2 you want from the menu, again with the deps taken care of.\n The reason there are two sources listed above is because most sources I tried don,t carry the crypto and ssl libs for it for some strange reason. There may be mirrors nearer to you, but these worked for me.\nHope this helps any fellow debian users.\n\nRegards, Ian"
    author: "Ian"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Oh, and here's the link on <a href=\"http://slashdot.org/comments.pl?sid=01%2F02%2F26%2F2354223&cid=&pid=0&startat=&threshold=0&mode=thread&commentsort=0\">Slashdot</a> and <a href=\"http://linuxtoday.com/news_story.php3?ltsn=2001-02-26-025-04-PR-KE\">LinuxToday</a>."
    author: "Anon"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "2.1 is great(er).  Thanks and congrats to all whom contributed.  This truly is an \"enterprise\" ready desktop and has allowed me to get rid of that other OS I had used at the office for years.  \n<br /><br />\nCheck out my experience on switching over via this small article on TotSP:  <a href=\"http://screaming-penguin.com/main.php?storyid=1559\">http://screaming-penguin.com/main.php?storyid=1559</a>\n<br /><br />\nThanks again KDE!"
    author: "Charlie Collins"
  - subject: "Can't find it at main ftp site,i tried some mirror"
    date: 2001-02-27
    body: "here it is\n\nftp://bolugftp.uni-bonn.de/pub/kde/stable/2.1/distribution/tar/generic/src/\n\nbad news: i just costed 8 hours to install kde 2.1 -beta2 in my computer,so this time i have to install qt-2.2.4and kde 2-1 again,anyway,i fell  exciting about it"
    author: "chinese_guy"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "I can't find a mirror with mandrake rpms. Has anyone found one?\n\nCraig"
    author: "Craig black"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "<p>Can't find Mandrake packages either.\nI haven't made it onto the clogged ftp.kde.org, but I did try rsync with it, and it seemed to be missing the Mandrake rpms as well.\n\n<p>I'd be curious to hear from ANYONE who actually found them somewhere... I beginning to think they just didn't make it. :(\n\n<p>Also, if anyone can confirm/disconfirm rumors about the debs supporting anti-aliasing, I'm curious about that too.\n\n<p>Thanks,\n<br>Alexander"
    author: "Alexander"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Well, I did make it into ftp.kde.org a few minutes ago and have confirmed that they are no there either. :-(\n\nDo we know if someone is working on making Mandrake packages?"
    author: "Nikunj Bansal"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-28
    body: "This may explain the lack of KDE 2.1 RPMS for Mandrake.\n<p>\n<a href=\"mailto:molnarc@nebsllc.com\">Chris Molnar</a>:\n<p>\nHello, \n<p>\nI can no longer speak for Mandrake as I am no longer working for the company, \nbut here is my guess: There was probably a slight confusion over who is \nresponsible for making the new RPM's. I had been creating all RPM's for the \n7.2 KDE updates until about 2 weeks ago. It takes time to reassign \nresponsibilities. I have had some email with people who know what is going on \nand they are working on building rpm's for kde 2.1. You have not been \nforgotten about. \n<p>\n-Chris"
    author: "Chris Adams"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Absolutely awesome! I have constantly heard idiots on ZDNet, CNET, etc, claim that although Linux is great on the server, it'll never be as good on the desktop as linux! All we need now is a better KOffice, and a merge or compat with GNOME (no easy task, but I'm positive that the hard working KDE developers can accomplish it) and within 5 years, M$ will be making nothing but mice! Summary : KICKIN! Also, kudos the the awesome developers at Enlightenment, GNOME, WM, and all other OSS projects! The future of computing rests on OSS's shoulders, but I am not worried in the slightest."
    author: "Carbon"
  - subject: "No Red Hat 7.0 i386 aRts RPM"
    date: 2001-02-27
    body: "The precompiled aRts RPM is missing from the Red Hat 7.0 i386 directory."
    author: "Anonymous"
  - subject: "Re: No Red Hat 7.0 i386 aRts RPM"
    date: 2001-02-27
    body: "I get the same, also missing libmng :-("
    author: "Leon"
  - subject: "Re: No Red Hat 7.0 i386 aRts RPM"
    date: 2001-02-27
    body: "Thanks for not letting me know.\n\nI forgot to upload it, it's in the packager home directory now, waiting for someone to move it over to ftp."
    author: "bero"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "I have missed Kdevelop so much!!\n\nThat will deffinately be on my top two things to be happy about... but there is so much more... what to pick for my second??????\n\nMaybe I'll just have to define the rest with a Doctor Suse word, preferably made up of letters after z."
    author: "Greg"
  - subject: "Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "Gnome is way better than KDE!  It kicks butt.  I've tried KDE, KDE2, and KDE2.1b, and I am not impressed with any of them.  KDE is actually the best of the three.  Gnome has a cleaner interface.  It has better Apps and no need for tools such as Krash.  It's installation is so easy!  Plus, it's really free (unlike KDE, which uses software from TrollTech).  It looks much, much, much cooler!!!!!!!!!!!"
    author: "Benjamin Atkin"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "Go away"
    author: "Jes\u00fas Antonio"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "Installationg might be more complicated, but I think KDE works a lot better than gnome. Kmail is the best Linux mail client out there I'd say. Konqueror, is a great browser, not slow like netscape or others. No Kde component has ever crashed on me (2.0.1) and I hope it continues this way with 2.1."
    author: "k0"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "I use gnome AND konqueror.\nIt' s no problem to use a gnome application with\nKDE or a KDE application with gnome.\nSo I don't understand the discussion KDE versus\nGNOME. Both are cool.\nBye\n   Harald"
    author: "Harald"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "<I>Plus, it's really free (unlike KDE, which uses software from TrollTech).</I><P>\n\nLast I knew, the QPL was pretty darn free. If you don't believe me, go to GNU's website and look at what licenses RMS himself considers to be free. In any case, isn't it clear we're having fun celebrating 2.1 here? You don't like KDE, write some bug reports or something; don't whine. As for the coolness factor, perhaps you haven't heard of klegacyimport. *sigh*"
    author: "Locando"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "KDE2.1 is GPLed, so it is just as free as Gnome.\n\nAnyway, you must be trolling, so I won't respond further."
    author: "Jagasian"
  - subject: "Both KDE and GNOME deserve enormous praise."
    date: 2001-02-27
    body: "While we are all entitled to our own opinions, that is no reason for you to be a troll. While I prefer GNOME as a desktop, I still like KDE and I find that the main apps I use are from KDE (namely Konqueror and Kmail). A major reason I chose GNOME in the first place was because of QT's restrictive licensing. This, however, has been rectified, with QT now having a GPL-compatible license. There is no longer any real political reason to choose one over the other. Both are technically brilliant, and I hope that the competition between the two continues so that they push each other to new heights. This doesn't mean that they cannot cooperate on simple things like drag and drop.\n\nIn short, congrats to the KDE team for version 2.1 and good luck to the GNOME team for version 1.4. And long live free software!"
    author: "Sridhar Dhanapalan"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "I am against TrollTech.  I will troll all I want because I hate TrollTech and there stupid not really free Qt development kit."
    author: "Benjamin Atkin"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "It is free. It is GPL:ed"
    author: "Alex Hamer"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "There is absolutely no need to hate Trolltech. They are marvelous programmers, they have created the best toolkit ever in the history of computer science and they put it under GPL license, so what do you want more from them??? \nIf you want, you can use their toolkit for free, absolutely free. Talking about stupid development kit, I personally think that, if any of the two, GTK+ fits more to this description than QT. QT is really OO, has clean interface, implements some features that are not in the GTK+ (yet), ... I also don't understand why you complain in the KDE site about this. If you don't like it, just don't use it and please stop critisizing people who put their free time into creating better, GPL-ed desktop. Instead, they deserve praise and admiration from all of us."
    author: "Bojan"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "QT is available under the GPL.  Unless you are a BSD fanatic, that should be as free as you can get.  If you are a BSD fanatic, you won't like Gnome's licence either."
    author: "Jonathan Bryce"
  - subject: "No Brains."
    date: 2001-02-27
    body: "<p>Flame on.\n<p>You sir, are an idiot.  Oh, and a troll...but you even admit that (I'm surprised you've got the two brain cells to rub together for that level of honesty).\n<p>In the off-chance that your semi-literate whining is in some way real, let me just say that -you- are a prime example of why the Linux community is going rapidly down the toilet.\n<p>Qt is GPL'd.  GEE-PEEE-ELLL'd.  In other words, for you Slashdorks out there, \"Free as in speech\" and \"Free as in beer\".\n<p>People like you seem to be desperate for 'something' to hate TrollTech for, now that they've released Qt under the GPL....I bet you miss the good old days of the Cold War, too!  You're so desperate to find a villan to hate...well let me tell you, TrollTech has done a LOT for the Linux community, despite the mountains of heaping BS that has been foisted on them by ungrateful mouse-jockeys like yourself over the past several years.\n<p>So please...do us all a favour...return to Slashdot, LUsenet or whatever your favourite cesspool hangout is.  \n<p>Flame off."
    author: "g"
  - subject: "Re: No Brains."
    date: 2001-02-28
    body: "LOL i think i'm in love. I'm sick of the \"slashdorks\" as well.\n\nCraig"
    author: "Craig black"
  - subject: "What about cross-platform devel?"
    date: 2001-02-28
    body: "QT is not free for windows, nor dos it work well.  GTK is free for Windows and it works well.  GTK is better, faster, cooler, freeer, than QT.  KDE is the worst thing for Linux.  They get a bunch of good developers behind them and make sophisticated programs that are all meant to be used only under KDE.  KOffice, Konquerer, etc, could have been made for X desktops in general and with the Krap left out, even using the Qt toolkit, having a different name/about/etc and be much more useful to the Linux/BSD community."
    author: "Benjamin Atkin"
  - subject: "Re: What about cross-platform devel?"
    date: 2001-02-28
    body: "You just want to dig yourself deeper, don't you?<br>\n<br>\n<I>QT is not free for windows</I><br>\n<br>\nSo port the X11-version to Windows, nobody's stopping you.<br>\n<br>\n<I>, nor dos it work well. GTK is free for Windows and it works well. GTK is better, faster, cooler, freeer, than QT</I><br>\n<br>\nThis removes any possibility, that you have actually tried to program with either one.<br>\n<br>\n<I>KDE is the worst thing for Linux. They get a bunch of good developers behind them and make sophisticated programs that are all meant to be used only under KDE.</I><br>\n<br>\nHow bout trying to come up with something more substantial than that general handwaving?<br>\n<br>\n<I>KOffice, Konquerer, etc, could have been made for X desktops in general and with the Krap left out</I><br>\n<br>\nThat \"Krap\" you want removed is exactly what makes these programs so sophisticated. Konqueror for example is, for most part, just a wrapper to this \"Krap\". Go back under the rock, troll."
    author: "bert"
  - subject: "I never was under a rock, idiot."
    date: 2001-03-01
    body: "I never, never was!"
    author: "Benjamin Atkin"
  - subject: "Your true intentions .."
    date: 2001-03-01
    body: "You obviously don't use KDE, or even like it.  So the fact that you are prepared to go out of your way to venture onto this site and say what you're saying proves that you are only here to:\n\n1) cause trouble\n2) demonstrate to everyone your complete lack of respect for others who support of the Open Source way!\n3) show us what a complete moron you are\n\nGet lost!!  We don't want your kind on this forum."
    author: "Macka"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "We have the KDE developers and TrollTech.\n\nYou're just a troll (not tech). Kid, grow up. If you don't like it, just don't use it."
    author: "Fr\u00e9d\u00e9ric L. W. Meunier"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-28
    body: "KDE users unite agaist the rabid slashdorks!!! LOL\n\nCraig"
    author: "Craig black"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "Hmm, as far as I know, QT is dual licensed. Either GPL or QPL. Choose what you like. Please explain what's more free than free. I you don't have to pay a single cent for a pice of GPLed Software. How can there be something that's more free than that?"
    author: "Richard"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "Of course when we talk of free we mean source code and not cost. But that's by the by."
    author: "Kev"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "It is GPL, therefore free as in speech as well as free as in beer."
    author: "Juergen"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "I really don't think anyone is impressed with your comments.  \n\nKrash = bug-buddy aren't they the same? \n\nAnyways there is room for both Kde and Gnome, so do me a favor and just go troll around gnome.org there should be something of interest for you there.  \n\nHave a nice day and hopefully I won't see you around! :o)"
    author: "Michael M Nazaroff"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-27
    body: "Clean interface??!!! KDE2 has a much better and cleaner interface. Gnome is just a fucking mess. Too many gnome apps have too many little windows floating around. KDE2 is genereally cleaner because everything is neatly organized in one window.\n\nGnome need a installer because it was such a pain to install. KDE has allways been easy. Just download the rpms and type : rpm -ivh *.rpm.\nHow hard is that??\n\nAnd Gnome doesn't look much much better. It looks like shit. The widgets are layd out terrible (probably because Gtk's layout manager sucks). The shape and sizes of buttons and widgets are almost allways wrong. Either too small or too large. The icons is disgusting with their military brown green color. And since KDE2 can use Gnome themes I don't see why Gnome can look better."
    author: "Erik Engheim"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-28
    body: "Qt is free you moron."
    author: "craig"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-28
    body: "And get depressed.\nClean my #$%?. Its the most depressing interface I've seen (And believe me I've seen lots.).\nApps? I'm trying to find a simple PCB design tool that works correctly under GNOME.\nAnd Mr. Rip-Van-Winkle The new QT has ben made free way back. You should read more often.\nGNOME at best is good to prove that people should continue using KDE2.\nGNOME crashes and since all crashes are unrecoverable its no use giving an app like KRASH. I ran out of patience recording GNOME bugs and said \"Forget it!! Why bother?\" \nGNOME might be best used to play FREECELL for two hours (Cause it crashes after that)."
    author: "fukkat"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-02-28
    body: "cool... RMS trolls on dot.kde.org...\n\n\nSCNR\n\tL"
    author: "Mathias"
  - subject: "Re: keep cool!"
    date: 2001-02-28
    body: "this gnome seem's like a little guy\nwho try to be important by making much noise.\n\nchildish reaction."
    author: "thilor"
  - subject: "You are the weakest link - GoodBye!!          *nt*"
    date: 2001-02-28
    body: "NT"
    author: "Anonymous Coward"
  - subject: "Re: Tough Installation - Try GNOME!!!"
    date: 2001-03-02
    body: "nowadays, everybody likes to use KDE and GNOME at the same machine. I am not the exception, I like both.\nOf course, this war is good for Linux/Unix. KDE and GNOME are growing very fast and they are improving too.\nTHE SOLUTION ARE BOTH.Both are really cool."
    author: "luisdlr"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "I'm having some trouble with the RH 7.0 RPMs. The kde-multimedia package, fails to find some dependencies. any ideas?\n\nEverything else works great... Thank you.\n\n\n        libartsc.so.0 is needed by kdemultimedia-2.1-0.70.2\n        libartsflow.so.0 is needed by kdemultimedia-2.1-0.70.2\n        libartsflow_idl.so.0 is needed by kdemultimedia-2.1-0.70.2\n        libkmedia2_idl.so.0 is needed by kdemultimedia-2.1-0.70.2\n        libkmid.so.0 is needed by kdemultimedia-2.1-0.70.2\n        libmcop.so.0 is needed by kdemultimedia-2.1-0.70.2\n        libqtmcop.so.0 is needed by kdemultimedia-2.1-0.70.2\n        libsoundserver_idl.so.0 is needed by kdemultimedia-2.1-0.70.2"
    author: "k0"
  - subject: "Re: Trouble with the RH 7.0 RPMs"
    date: 2001-02-27
    body: "I have the same problems and found, that the same applies to kdelibs-sound-2.1-0.70.2.i386.rpm"
    author: "Holger"
  - subject: "Re: Trouble with the RH 7.0 RPMs"
    date: 2001-03-01
    body: "I have some strange problems.  I've lost my settings for sound and in superuser mode, I finally got sound to work but it was strange and don't know how exactly I did this.  Biggest problme was finding a way for midi.  I could see one selection that wasn't set, and did it.\n\nHowever, when not in superuser mode, settings are gone and can't figure it out.  I tried doing the same thing when I was in superuser mode but nothing shows up to select!\n\nCould somebody tell me how to get my sound settings back?\n\nAlso, something happened in the installation and I also the sounds settings for Gnome!  Help!\n\nSigh...\n\nDan"
    author: "Dan Frezza"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "the reason is that artsd is missing in the red hat 7.0 packages."
    author: "greg"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Best solution I could find was to pull the arts.rpm file from the last 2.1beta2 rpm directory for RH 7.0.  Perhaps a bit backlevel, but it works fine and resolved all those dependency issues...  Regards, Diego"
    author: "Diego Gallina"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-09-04
    body: "Yes, I'm having trouble also!  On Suse 7.1, I downloaded all the RPMs needed to upgrade to KDE 2.2, and I try installing kdelibs-2.2.0-1.i386.rpm first (since the install instructions say to install that one first).  \n\nI get the error message:\n\n  libartsflow.so.0 is needed by kdelibs-2.2.0-1\n  libartsflow_idl.so.0 is needed by..... (etc...)\n\nIf you get a resolution to this, please let me know!"
    author: "Nathan Gopen"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-10-27
    body: ":)... just install  audiofile.rpm which you can download from SuSE server..."
    author: "rezach \"chipset\""
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "When do you think it will be ok to remove the word \"will\" from the KHTML description?\n\nIt makes it seem like it does not support everything that is listed yet.\n\nIt might be better to say that it supports most of them.. ?"
    author: "Tick"
  - subject: "kdesdk: Wrong CVS tag ? KDE_2_1_RELEASET ?"
    date: 2001-02-27
    body: "Hello developers. Congratulations! Now a question. I just upgraded my CVS sources to the KDE_2_1_RELEASE tag. But I noticed the module kdesdk I got with cvs co was downgraded to admin/ and various *.desktop files. Doing a cvs status -v Makefile.cvs (thanks Johannes!) I see there are KDE_2_1_RELEASET (shouldn't it be KDE_2_1_RELEASE ?), KDE_2_1, and KDE_2_1_BETA2 tags.\n\nBTW, KDE 2.1 should build without major problems with the upcoming gcc 3.0 (no, I won't use the snapshots) ?"
    author: "Fr\u00e9d\u00e9ric L. W. Meunier"
  - subject: "Anyone tried the FreeBSD binaries?"
    date: 2001-02-27
    body: "The release says that a precompiled package is available for FreeBSD. I'm trying to decide if I want to try it, or build kde from source. I've had problems with binaries (on Linux Mandrake) in the past (they were *slow*) so I'm wondering what this batch is like.\n<p>\nThanks!<br>\n-kdeFan"
    author: "kdeFan"
  - subject: "Re: Anyone tried the FreeBSD binaries?"
    date: 2001-02-27
    body: "Compiling from source is always a safe bet I would say. Also, I fear those binary packages are a myth.\n\nWe really need a kde-freebsd list to make sure KDE is always kosher."
    author: "ac"
  - subject: "Re: Anyone tried the FreeBSD binaries?"
    date: 2001-02-27
    body: "I've been using it since this morning on my FreeBSD 4.2-STABLE and I haven't find any significant bugs. Everything works great and fast. I think there's no reason to compile from source unless you want to make some optimizations (like -mpentiumpro and so on).\n\nI'd like to thank all the KDevelopers! Thank you guys, KDE rocks! :)"
    author: "bison"
  - subject: "What happened to my Nice AA fonts?"
    date: 2001-02-27
    body: "Hi,\n\nI just upgraded from 2.0.1 to 2.1. I was using the Redhat Wolverine XFree 4.0.2 and qt which gave me nice AA'd fonts on 2.0.1. I didn't upgrade qt - only the KDE packaged and the AA fonts have dissappeared. Is there an option in 2.1 to switch them back on?\n\nThey still appear in kdm and in the splash screen BTW.\n\nCheers."
    author: "Dr_LHA"
  - subject: "Re: What happened to my Nice AA fonts?"
    date: 2001-02-27
    body: "K > Settings > Look N Feel > Style\n\n(the names may not be the same as i described, i'm using kde in pt_BR)"
    author: "Evandro"
  - subject: "Re: What happened to my Nice AA fonts?"
    date: 2001-02-27
    body: "Evandro wrote:\n\n>> K > Settings > Look N Feel > Style\n\nI don't see anything there relating to AA fonts. I'm using the RH7.0 RPMS for KDE2.1 BTW.\n\nCheers."
    author: "Dr_LHA"
  - subject: "Re: What happened to my Nice AA fonts?"
    date: 2001-02-27
    body: "hum.. if you have XFree86 4.0.2 and qt-2.2.4-4 you need the wolverine packages."
    author: "Evandro"
  - subject: "Re: What happened to my Nice AA fonts?"
    date: 2001-02-27
    body: "Makes sense. Thanks - I'll try that."
    author: "Dr_LHA"
  - subject: "Re: What happened to my Nice AA fonts?"
    date: 2001-02-27
    body: "That worked! Cheers."
    author: "Dr_LHA"
  - subject: "Re: What happened to my Nice AA fonts?"
    date: 2001-02-28
    body: "I am having this same problem with redhat 6.2<br>\nAnyone have any ideas?"
    author: "Ted Roden"
  - subject: "Re: What happened to my Nice AA fonts?"
    date: 2001-04-20
    body: "dummy."
    author: "pm"
  - subject: "Re: What happened to my Nice AA fonts?"
    date: 2001-02-28
    body: "What is \"wolverine\"? Can I download XFree-4.0.2 with AA-fonts RPMS somewhere?\n\n--\nAndreas"
    author: "Andreas Joseph Krogh"
  - subject: "Re: What happened to my Nice AA fonts?"
    date: 2001-02-28
    body: "<p><i> What is \"wolverine\"? Can I download XFree-4.0.2 with AA-fonts RPMS somewhere?\n</i>\n\n<p>It's the latest Redhat 7.1 Beta test. With a few downloads I got the XF86 4.0.2 RPMS to work under RH7. With this and the wolverine qt rpms I magically got AA fonts with KDE 2.0.1.</p>\n\n<p>Get wolverine here (or mirrors) <a href=\"ftp://ftp.redhat.com/pub/redhat/beta\">ftp://ftp.redhat.com/pub/redhat/beta</a>\n</p>"
    author: "Dr_LHA"
  - subject: "NS plugs"
    date: 2001-02-27
    body: "I want use NS plugins. I read that I should go to \"Preferences/WebBrowsing/Netscape plugins\". But there is (at preferences/webbrowsing) no any item like \"netscape plugs\" :( what's wrong?"
    author: "man"
  - subject: "Re: NS plugs"
    date: 2001-02-27
    body: "Just got to Settings -> Configure Konqueror and choose Netscape Plugins on the left of the appearing dialog... it's simple"
    author: "Thomas"
  - subject: "Re: NS plugs"
    date: 2001-02-27
    body: "Just go to Settings -> Configure Konqueror and choose Netscape Plugins on the left of the appearing dialog... it's simple"
    author: "Thomas"
  - subject: "Re: NS plugs"
    date: 2001-02-27
    body: "yes, of course. I understand it.\nbut what I want say it's _there is no_ such thing!\nso there are:\nfile manager, file assoc, konquer brows, enchans brows, cook, prox, crypt, user agent.\nand there is no NS plugins!! :(\nsomething is wrong. but what?"
    author: "man"
  - subject: "Re: NS plugs"
    date: 2001-02-27
    body: "It isn't in Konqueror itself, actually.  It's under Web Browsing in the KDE Control Center."
    author: "SubPar"
  - subject: "Re: NS plugs"
    date: 2001-02-27
    body: "you need to compile kdebase with lesstif installed."
    author: "Evandro"
  - subject: "Many, many thanx! (-)"
    date: 2001-02-28
    body: "."
    author: "man"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "just let me say THANK YOU\n\nYou are the reason i ever tried Linux\n\nYou guys are the reason that actualy let me use linux as my only desktop.\n\nAgain thanks\n\nBest regards\n\nFrom Portugal\n\nJorge"
    author: "Jorge"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "ALL YOUR DESKTOPS ARE BELONG TO US!"
    author: "Anonymous Custard"
  - subject: "Take off all Kparts .. For great justice!"
    date: 2001-03-01
    body: "nt"
    author: "ZiG"
  - subject: "Re: Take off all Kparts .. For great justice!"
    date: 2003-09-03
    body: "What on earth is this topic about!?\n\nMake your time!"
    author: "Varda"
  - subject: "Re: Take off all Kparts .. For great justice!"
    date: 2004-02-13
    body: "What you say !!"
    author: "w00t"
  - subject: "Re: Take off all Kparts .. For great justice!"
    date: 2004-03-02
    body: "All your zig are belong to us. "
    author: "Zig zag"
  - subject: "Re: Take off all Kparts .. For great justice!"
    date: 2004-03-02
    body: "All your zig are belong to us. "
    author: "Zig zag"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-27
    body: "Hi!\n\nAm I the only one having problems or are there broken packages? I downloaded two copies of the kdebase-2.1 SuSE 7.0 rpm packages from two different mirrors with my V34 modem but I get an \"cpio: bad magic\" error if I try to install any of them... :-(\nAnd I was really looking forward to testing KDE2.1 this evening... :-((\n\nGreetinx,\n\nGunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "sweet"
    date: 2001-02-27
    body: "This is polished! I couldn't wait, I am play with the system as the rest of it compiles. All of the righ edges in 2.0 have been polished into beauty. Kde developers are true code artists."
    author: "eze"
  - subject: "No sound in Redhat 6.x?"
    date: 2001-02-28
    body: "Is anyone else who is using the Redhat 6.x packages having problems with sound?  Sound worked fine under KDE 2.0.1, but as soon as I upgraded, it no longer works in KDE.  Other applications that use sound are unaffected.\n\nI've tried testing sounds under System Notifications, and testing sound in the setup of the sound server, and I don't get any errors, but hear no sound.  My volume settings are at max.\n\nAnyone have any ideas on this?"
    author: "Karl Garrison"
  - subject: "Re: No sound in Redhat 6.x?"
    date: 2001-02-28
    body: "i too have the same problem. For some reason arts doesn't work with my Redhat 6.2 and I couldn't listen to music. When I start the KDE mediaplayer, it just core dumps\n\nThings were fine with KDE 2.0.1 and 2.1 beta 2\n\nApart from that, things look great.\n\ncongratulations KDE team,\n-Mathi"
    author: "mathi"
  - subject: "Re: No sound in Redhat 6.x?"
    date: 2001-02-28
    body: "I forgot to mention the media player core dump; I get that as well.\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: No sound in Redhat 6.x?"
    date: 2001-02-28
    body: "See the posts farther up about the missing aRts packages for Red Hat.  If you still can't find them, try the old aRts packages from 2.1 beta2."
    author: "not me"
  - subject: "Re: No sound in Redhat 6.x?"
    date: 2001-02-28
    body: "Actually, the arts package for Redhat 6.x was there, and I did install it.  I think it was only missing in the Redhat 7.0 directory."
    author: "Karl Garrison"
  - subject: "Re: No sound in Redhat 6.x?"
    date: 2001-02-28
    body: "The Arts rpms for Redhat 6.x appear to come without sound-plugins. Only the null-plugin appears to be compiled in (artsd -A will give this info). Installing the arts rpm for 2.1beta2\nfixes the problem if you use oss. If you use alsa you're still out of luck."
    author: "Anthony Meijer"
  - subject: "Re: No sound in Redhat 6.x?"
    date: 2001-03-02
    body: "That means compiling a src.rpm should help\nam I right?"
    author: "Yogeshwar"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-28
    body: "This version does look very polished, but what is up with the application logging?  After having KDE2.1 up and running for 10 minutes, my .xsession-errors file is up to 1081436 blocks(it started at 0)  Come on guys, this stuff needs to be taken out.  I don't like my home directory being a dumping ground for debug messages.\n\n-TWB"
    author: "tbecker"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-28
    body: "Sounds like the packager did not compile the packages with debugging off?\n\nStrange...  That's a really huge .xsession-errors. What's in it?"
    author: "KDE User"
  - subject: "Same problem here!"
    date: 2001-02-28
    body: "I have the same problem with KDE 2.1. Redhat 7.0-packages. My .xsession-errors grew yesterday to huge size, until I deleted it. Now it's growing again, though a bit more slowly. The file seems to contain just information about what DCOP, kio, kdecore, kicker, etc. are doing - not even errors or warnings."
    author: "I have no name"
  - subject: "Re: Same problem here!"
    date: 2001-02-28
    body: "I should note that I am using the RH 7.0 packages as well, apparently little care was taken when making these up.  As mentioned above, aRts was left out as well. . .  Can someone at KDE look into this?\n\n-TWB"
    author: "tbecker"
  - subject: "Re: Same problem here!"
    date: 2001-02-28
    body: "You need to contact the person/distributor who made the packages.  Try bero @ redhat . de .  KDE proper itself does not usually take responsibility for packages."
    author: "ac"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-28
    body: "2.1 looks good, but I am having a display problem on my Sony VAIO notebook computer (it was actually there with the 2.1 beta as well though I didn't try 2.0).  I installed RH6.1 on it, and when I run the default KDE 1.1.2 or even Gnome, the display looks very crisp and sharp.  However, whenever I run KDE2, the disply is very grainy.  I can see the pixels quite easily and it is annoying.  Anyone know what might be going on?  I thought maybe it was an X problem, but since KDE1 and Gnome display correctly, it would seem to rule out X.  Thanks for any help as I would really like to get it looking good on my portable.\n\nOther than that, it looks really good.  Thanks again KDE team.\n\nPaul....."
    author: "Cire"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-28
    body: "I figured the problem out last night.  I'm not sure why I had to, but I ran the Xconfigurator program again and set up my adaptor and display.  After doing this, KDE 2.1 looks beautiful.  I'm not sure why I had to configure again though.  As I said, KDE1 and Gnome displayed properly all along.\n\nNow on to other fun stuff.  \n\nPaul....."
    author: "Cire"
  - subject: "Problems getting it to start"
    date: 2001-02-28
    body: "Anyone else having problems getting KDE 2.1 to start w/ Redhat 7.0 rpms? It just sits there for me, saying \"Setting up interprocess communication\"\n\nthe last lines that xfree puts out to the console are as follows:\n\nLD_LIBRARY_PATH=/usr/local/qt/lib:/usr/local/qt/lib::/usr/lib\nkdeinit: Launched DCOPServer, pid = 783 result = 0\nDCOPServer: SetAPRoc_loc: conn 0, prot = local, file = /tmp/.ICE-unix/785\nkdeinit: Launched KLauncher, pid = 786 result = 0\n\nhmm thanks for help in advance :)"
    author: "Chris Aakre"
  - subject: "Re: Problems getting it to start"
    date: 2001-02-28
    body: "Structured Text: (sorry)\n\nAnyone else having problems getting KDE 2.1 to start w/ Redhat 7.0 rpms? It just sits there for me, saying \"Setting up interprocess communication\"\n\nthe last lines that xfree puts out to the console are as follows: \n\nLD_LIBRARY_PATH=/usr/local/qt/lib:/usr/local/qt/lib::/usr/lib\nkdeinit: Launched DCOPServer, pid = 783 result = 0\nDCOPServer: SetAPRoc_loc: conn 0, prot = local, file = /tmp/.ICE-unix/785\nkdeinit: Launched KLauncher, pid = 786 result = 0\n\nThanks for help in advance :)"
    author: "Chris Aakre"
  - subject: "Re: Problems getting it to start"
    date: 2001-03-02
    body: "I also have this problem.\nSame message and nothing else after that."
    author: "Patrick Lafarguette"
  - subject: "Re: Problems getting it to start"
    date: 2001-10-07
    body: "I am having a problem (RH 7.1) as well. The underlying messages indicate that the dcopserver process is not started, and that the kdcop process cannot communicate with the server.\n\ncontents of .xsession-errors ... \nxset:  bad font path element (#42), possible causes are:\n    Directory does not exist or has wrong permissions\n    Directory missing fonts.dir\n    Incorrect font server address or syntax\nXlib:  extension \"RENDER\" missing on display \":0.0\".\n/usr/bin/startkde: line 232:  1469 Bus error               LD_BIND_NOW=true kdeinit +kcminit\nBus error \nXlib:  extension \"RENDER\" missing on display \":0.0\".\nBus error \nCould not register with DCOPServer. Aborting."
    author: "Daemeon Reiydelle"
  - subject: "Re: KDE 2.1 Has Landed (mostly)"
    date: 2001-02-28
    body: "Everything seems to have installed smoothly except for kde-base-2.1.0-2 package doesn't install.\n\nApparently I'm missing the following:\n\nicons\nlibcdda_interface.so.0\nlibcdda_paranoid.so.0\n\nThese don't seem to be in any of the kde or x rpm's for SuSE 6.4 (i386).  Anyone know where these are hidden?"
    author: "Christopher Specker"
  - subject: "Re: KDE 2.1 Has Landed (mostly)"
    date: 2001-02-28
    body: "Hi,\nI had the same dependency problem on SuSE 7.0, and ignored it.  Until now KDE works fine without these dependencies on my system.\n\nKind regards, Rinse"
    author: "rinse"
  - subject: "Re: KDE 2.1 Has Landed (mostly)"
    date: 2001-02-28
    body: "try to install the packages icons and cdparanoia..."
    author: "Mathias"
  - subject: "Re: KDE 2.1 Has Landed (mostly)"
    date: 2001-02-28
    body: "I'll have to look for the cdparanoia package, but I can't find icons.  That package doesn't seem to be anywhere in my kde rpm's or my (x, xdevel, etc) rpm's either."
    author: "Christopher Specker"
  - subject: "Re: KDE 2.1 Has Landed (mostly)"
    date: 2001-02-28
    body: "icons is an old \"unresolved dep\", I had it in my old SuSE 6.4 with kde 2.0 install, too.\nYou can find an old icons.rpm on rpmfind.net, it should also exist somewhere on your CD's, \nSuSE series xwm \n(what rpm -q -i icons says me)\nSince SuSE has packed some icons into xf* packages, this can really be \"--nodeps\"-ed."
    author: "Adrian"
  - subject: "ANTIALIASING"
    date: 2001-02-28
    body: "Someone on Slashdot said that the Debian Packages , have antialiasing precompiled in(!) Does anybody know more about this? Do other packages have antialiasing precompiled? Or is this just plain false altogether?"
    author: "t0m_dR"
  - subject: "Re: ANTIALIASING - True in Debian Unstable"
    date: 2001-03-01
    body: "In case you haven't found out yet, it's true indeed!  Debian Unstable has AA compiled in.  Also, thw libqt.so file is 4.7MB leading me to believe that they compiled that properly too!  (There was a note on Slashdot about QT and KDE optimizations that the package managers should adhere to.)\n\nAlso, in case you try this, you can turn off AA by going to K->Preferences->Look 'N Feel -> Style and unselecting AA.  \n\nWhy would you do this?  Well, while AA looks great, I found that I lost access to most of my TTFonts (I'm sure there's a fix for this) and everything seemed noticeably slower.  My video card is a Voodoo3 2000m so it's no slouch (though not the best) in the graphics speed dept.  \n\nI also noticed that Konsole (and other apps) had problems with some of the fonts displaying screen garbage.  I'm not sure if that's an X, Qt, or KDE bug, but in any case it was annoying enough for me to turn AA off.\n\nPretty cool though, I can't wait for that to get a little more solid..\n\nSpeakings of solid, be sure to check out the new KDevelop!  WOW!\n\nCheers,\n\nBen"
    author: "benmhall"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-28
    body: "Has any one else had a problem with the kdemultimedia rpm package for RedHat <b>6.x</b>?\n<p>\nI get an error unpacking the archive when I try to install it."
    author: "Rob"
  - subject: "QT 2.2.4"
    date: 2001-02-28
    body: "Hi.\nDownloaded nearly all sources (except i18n). Cant find qt 2.2.4 sources in the mirrors (or I don't know where to look). Main ftp site of trolltech is too slow for me. Is there an http mirror available for the same?"
    author: "rv"
  - subject: "Re: QT 2.2.4"
    date: 2001-02-28
    body: "Go here:\nhttp://www.trolltech.com/products/download/freelicense/qtfree-dl.html\nThen click on \"ready-made search\" in \"TAR FILE\" chapter.\nYou will have a list of mirror sites."
    author: "Nicolas"
  - subject: "Re: QT 2.2.4"
    date: 2001-03-01
    body: "Got it. Thanks. Problem is that most sites on the search are ftp. Anyway speeds are improving :-)"
    author: "rv"
  - subject: "Hurry up...."
    date: 2001-02-28
    body: "This may explain the lack of KDE 2.1 RPMS for Mandrake.\n<p>\n<a href=\"mailto:molnarc@nebsllc.com\">Chris Molnar</a>:\n<p>\nHello, \n<p>\nI can no longer speak for Mandrake as I am no longer working for the company, \nbut here is my guess: There was probably a slight confusion over who is \nresponsible for making the new RPM's. I had been creating all RPM's for the \n7.2 KDE updates until about 2 weeks ago. It takes time to reassign \nresponsibilities. I have had some email with people who know what is going on \nand they are working on building rpm's for kde 2.1. You have not been \nforgotten about. \n<p>\n-Chris"
    author: "Chris Adams"
  - subject: "Re: Hurry up...."
    date: 2001-02-28
    body: "Finally, some clarification. Hopefully they will be out sometime this weekend."
    author: "John"
  - subject: "Re: Hurry up...."
    date: 2001-03-01
    body: "And hopefully the packages binary rpm packages are compiled against Mandrake 7.2 glibc and other libraries ;-)\n\n-Boozeman"
    author: "Timo Viinanen"
  - subject: "It's nice... or it would be if it would compile"
    date: 2001-02-28
    body: "In the absence of Slackware packages I decided to do what I normally do anyway and compile 2.1 from source. Will it compile? Will it hell. Undefined symbols all over the place at link time.<p>\n\nCan anyone say what versions of glibc and similar libraries KDE is supposed to work against?<p>\n\nNo doubt it's just my screwed up system... one day I'll get around to doing a proper upgrade to Slackware 7.1... or maybe when 7.2 comes out..."
    author: "Yrd"
  - subject: "Re: It's nice... or it would be if it would compile"
    date: 2001-02-28
    body: "Without knowing what symbol is missing, how can anyone know what library is the culprit?\n\nKDE doesn't require any particulat glibc. It doesn't even require glibc in particular."
    author: "Roberto Alsina"
  - subject: "Re: It's nice... or it would be if it would compil"
    date: 2001-02-28
    body: "It's not a problem anymore - in trying to fix the symbol problem I somehow arranged it so that configure doesn't like my libXext. I think I'll be getting binaries, and doing some serious maintenance on my distro."
    author: "Yrd"
  - subject: "Re: It's nice... or it would be if it would compile"
    date: 2001-03-01
    body: "Hi. I have Slak 7.1, running Kde 2.0.1 and QT 2.2.3 currently. None of the 2.1 releases (betas and final) have compiled. The problem is kdelibs, in particular kdecore/klocale.o. I hacked around that, then kdeui/kedit(?) then gave me problems, and I cannot hack around it. In fact, I cannot even find the problem line!! I searched all over the sources, the offending string just isn't there. I cannot remember the string off hand, I'm at work on a Win95 machine. Last night I compiled QT 2.2.4, but I haven't tried recompiling against it yet. Any suggestions?"
    author: "Capt Pungent"
  - subject: "Re: It's nice... or it would be if it would compile"
    date: 2001-03-01
    body: "I'm having trouble too. I'm running Slackware 7.1 and I had trouble compiling as well. I noticed there were some packages up there and so I installed them. howeve,r after removing my old configure files in my home dir and starting it up, I ran into two problems:\n\n1.) the dcopserver wasn't eruning. that was easy enough to start form the comand line, just unclear why I never hadthis problem with kde2.0????\n\n2.) after that it just gets to a dead grey screen. what is going on? I snagged the error messages, here they are, any ideas people? \n(II) Keyboard \"Keyboard1\" handled by legacy driver\n(II) XINPUT: Adding extended input device \"Mouse1\" (type: MOUSE)\nCould not init font path element /usr/X11R6/lib/X11/fonts/Speedo/,\nremoving from list!\nksplash: error while loading shared\nlibraries: /opt/kde/lib/libkdeui.so.3: undefined\nsymbol: focusNextPrevChild__14QMultiLineEditb\nkdeinit: error while loading shared\nlibraries: /opt/kde/lib/libkdeui.so.3: undefined\nsymbol: focusNextPrevChild__14QMultiLineEditb\nknotify: error while loading shared\nlibraries: /opt/kde/lib/libkdeui.so.3: undefined\nsymbol: focusNextPrevChild__14QMultiLineEditb\nktip: error while loading shared\nlibraries: /opt/kde/lib/libkdeui.so.3: undefined\nsymbol: focusNextPrevChild__14QMultiLineEditb\nDCOP: register 'anonymous-32592'\nksmserver: KSMServer: SetAProc_loc: conn 0, prot=local,\nfile=/tmp/.ICE-unix/32592\nQSocketNotifier: Multiple socket notifiers for same socket 6 and type read"
    author: "Will Stokes"
  - subject: "Re: It's nice... or it would be if it would compile"
    date: 2001-03-01
    body: "Yeah, thats the string!!! A problem with QMultiLineEdit::focusNextPrevChild.<br>\nI think I found the problem, well, with mine anyway. <i>****Side Note****</i> Are you using the Kde 2.0.1 SlakPaks? I couldn't use them, they are compiled against glibc 2.2, which apparantly is what the next Slak is going to use. I'm stubborn, and I don't like to update things like that without upgrading the whole distro. I'll wait until Slak 7.2(?)<br>I discovered that, when I compiled Kde 2.0.1 for my machine (I like to compile from source for my machine on <i>everything</i> to get it optimized), it failed. So, I recompiled QT 2.2.3, <i>with -no-g++-exceptions</i> and then tried KDE, again, it failed. Recompiled Qt 2.2.3 <i>with</i> g++ exceptions, ran my compile script (simple bash: <br>arg=$1 <br>./configure $arg && make && make install )<br> and it compiles. I had forgotten this when I compiled Qt 2.2.4 and used -no-g++-exceptions, and my results are above. Last night it hit me when I tried recompiling Kde 2.0.1 to restore the old libs (the new ones seriously conflict with 2.0.1) and I received the MultiLineEdit error. It then occurred to me what I did wrong and recompiled Qt with g++ exceptions, but it was late so I haven't tried recompiling Kde 2.1 yet but I expect it to work. <br>As for your errors, I can't help, since I didn't use the Slak Pak version of Kde 2.0.1 or their Qt, but I do suggest that you use the source Slak Paks (from their ftp or you should have gotten it with the distro) and recompile Qt and Kde, hell, maybe the entire distro if you want, for optimisations. I have been hearing people say here lately about such and such program is running slow, when they don't compile <i>anything</i> for their CPU's. I'm not saying you, I just noticed that a lot, especially with users of RPM systems. Really, how hard is it to use a script (like the one above) to install software? It is painless and you get a binary that is perfect for your CPU. And, you can rip \"features\" out of the code that you don't want, like programs that use sound that don't need it. I have a firewall program, really nice one to, but it plays sounds when the wall takes a hit. Some people may like this, I really don't care, and yes, I can turn it off, but it is better for me to rip the  code out. Result: smaller binary, faster load. And extremely easy (especially with sound). I'm not attacking you, that rant was about other posts I'm seen in the past few days. That actually had nothing to do with you.<br>ummm...yea. Try recompiling Qt without the -no-g++-exceptions flag during the configure. But, you can leave the Slak Pak installed, that way their is a record of it and you can remove your Qt compile by removepkg qt2. Just rename your current /usr/lib/qt2 to something, but DON'T delete it until you verify that the new compile works. Then, rm -R (insert old qt directory), rm /usr/lib/qt, and ln -s /usr/lib/qt-2.2.x /usr/lib/qt <br>Hope that helps<br><br>C Pungent"
    author: "Capt Pungent"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-28
    body: "Now that KDE is out I hope the majority of development will shift to koffice.\n\nCraig\n\n(just a wish)"
    author: "Craig black"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-28
    body: "Oh My,\n\nI have been using gnome for quite some time but I went ahead and installed 2.1 out of curiosity.  It's still here today :)\n\nIt is very impressive.  I'd say it's quite a step over kde2.0\n\nKonquerer is quite impressive.  \n\nThe extra bit of polish on everything speaks highly of it's professionalism.  \n\nI would still like some more configurability of the panel (like gnome) but I will live.\n\nNice work kde/qt developers - very nice"
    author: "PedroG"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-02-28
    body: "Will there be a bugfix release 2.1.1. <br>\nany time soon, the way it has been with 2.0? <br>\n<br>\nRegards, <br>\ncm.<br>"
    author: "cm"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "using redhat 7, i can't get kdebase to install.  it keeps giving me a \"libsensor.so.1\" dependency.  all the other rpms will install just fine.  i did get qt 2.2.4 to install ok, and have downloaded and installed the latest aRts rpm, but still no luck with the kdebase install.  any help would be greatly appreciated.  ..jack.."
    author: "jack"
  - subject: "me too .. and what about libmng ?"
    date: 2001-03-01
    body: "I eventually installed with --force because i got fed up :)\n\nSeems to work but id like to know what libsensors.so.1 does :)\n\nAnother thing:\nWasn't there supposed to be a libmng package in the redhat7 dir ?"
    author: "FlipFlop"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "using redhat 7, i can't get kdebase to install.  it keeps giving me a \"libsensor.so.1\" dependency.  all the other rpms will install just fine.  i did get qt 2.2.4 to install ok, and have downloaded and installed the latest aRts rpm, but still no luck with the kdebase install.  any help would be greatly appreciated.  ..jack.."
    author: "jack"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "Install lm_sensors rpm\n\n<a href=\"http://rpmfind.net/linux/RPM/redhat/7.0/i386////lm_sensors-2.5.2-3.i386.html\">http://rpmfind.net/linux/RPM/redhat/7.0/i386////lm_sensors-2.5.2-3.i386.html</a>\n\nThe RH-7 packager used those on his system to compile ksysguard.\n\nThe good part of this hassle is that you will be able to read info about your processor's temperature etc directly in your ksysguard."
    author: "Inorog"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "KDE 2.1 just kicks ass ! Konqueror is totally stable, except of some java script problems and ...... why the fuck am i wasting 2 gigs of my HD to windows ??????"
    author: "Jimbo"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "i *wonder* if i am the only one who has *always* problem getting kde to work . I just installed debain 2.1-final-1 packages , und whoops , konqueror sometimes does not use a new URL when i enter it.\n\nund beside that , source from the head branch does not compile completly , that sucks , i compiled about 10 times :-( , is there something like mozilla-tinerbox for kde ? a dedicted compile box that checks which parts of the codebase is ok , an presents it on a nice webpage , and if something is broken show's which user is the Bad Guy. This was very cool feature in mozilla development.\n\nbtw. is there a list of depended software packages beside QT , which are needed for *full* compilation of kde ??"
    author: "peter"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "yeah i forgot to complain about the smb support in konqeror. while i know that the smb protokoll is not that nice to implement , i would say hat it is NOT usable at this point , i cant browse throuh my entire windows-network looking at shares and playing divx or mp3 . it always wants to download the stuff to my computer .."
    author: "peter"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "yeah i forgot to complain about the smb support in konqeror. while i know that the smb protokoll is not that nice to implement , i would say hat it is NOT usable at this point , i cant browse throuh my entire windows-network looking at shares and playing divx or mp3 . it always wants to download the stuff to my computer .."
    author: "peter"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "Try komba:\nhttp://zeus.fh-brandenburg.de/~schwanz/php/komba.php3\n\ngreetings\n\n  Thorsten Schnebeck"
    author: "Thorsten Schnebeck"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "After upgrading (from 2.0) I noticed it was impossible to login at the kdm screen.\nAll went well passing from console and using startx.\n\nI noticed from the logs it seem to be a PAM problem (never upgraded, it's a RH6.2 box - it should be version 0.72) but I dont know PAM and fear to upgrade and remain closed outside my PC (smile - if you can...).\n\nNow:\n\na) never saw mentioned in the requirements that PAM is required. \n\nb) which PAM version is recommanded?\n\nc) how to upgrade PAM withou problems?"
    author: "Alessandro Magni"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "I experience the same on an old RH6.1. I've switch to using xdm, so I don't really have the log entry present, but it seemed that I was missing a file <tt>/lib/libpam_something.so</tt> or similar.\n\nThomas"
    author: "Thomas Olsen"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "Oops - sorry for repeating myself - I got a timeout on the server so I didn't think it got submitted."
    author: "Thomas Olsen"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "I experience the same on an old RH6.1. I've switch to using xdm, so I don't really have the log entry present, but it seemed that I was missing a file <tt>/lib/libpam_something.so</tt> or similar.\n\nThomas"
    author: "Thomas Olsen"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "I experience the same on an old RH6.1. I've switch to using xdm, so I don't really have the log entry present, but it seemed that I was missing a file <tt>/lib/libpam_something.so</tt> or similar.\n\nThomas"
    author: "Thomas Olsen"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-02
    body: "Last time I tried pam-0.72 rpm\nbut then my system wont let me login at login prompt ( telinit 3 ) at any login\nany suggestions how to avoid this problem?"
    author: "Yogeshwar"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-01
    body: "Are there any binary tar files available for RedHat-like systems?<p>\n\nI can't seem to install the RPM of kdebase. All the others install fine, but rpm exits with return code 1 and no error message. My RPM database is completely screwed up but I'm using --force and --nodeps along with --relocate /usr=/opt/kde2 which works for all the packages except kdebase.<p>\n\nI have looked at the binary tgzs for slackware, but they are still at 2.0.1 (despite what the announcement said!)<p>\n\nAny ideas?"
    author: "Owain Vaughan"
  - subject: "SuSE rpm's - bits missing?"
    date: 2001-03-01
    body: "First of all let me add my thanks to all the wonderful people at KDE for making being stuck in front of a computer all day a lot more fun than it used to be!\n\nWhen I installed the 2.1beta1 rpm's (SuSE 7.0 i386) I was really impressed with the customizable taskbar and the dock for WM applets, but these features disappeared when I installed the beta2 rpm's and are still missing in 2.1final. Is this a problem with the SuSE rpm's or have these features been left out for a reason?\n\nAlso, since installing 2.1final I've noticed that my swap file fills up really quickly, even with just Kmail, Star Office, and Konqueror open (I'm using a PentiumII with 96Mb ram). Has anyone else had this problem?\n\nThanks for any suggestions."
    author: "markes"
  - subject: "Re: SuSE rpm's - Swap file problems"
    date: 2001-03-01
    body: "Whoops! OK, I found the taskbar and WM dock. Still have problems with the swap file filling up though, which has now caused KDE to dump me back to the login a couple of times. Any ideas?"
    author: "markes"
  - subject: "Re: SuSE rpm's - Swap file problems"
    date: 2001-03-02
    body: "Press Ctrl-ESC and have a look which process is exceptionally large.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: SuSE rpm's - Swap file problems"
    date: 2001-03-02
    body: "Hi,\n\nIt seems to be a problem with Xfree86 (4.0.1). Even just swapping between desktops several times is enough to fill up the swap file and bring the computer to a halt. The last time it caused a total system lock out and I had to hard reboot. I couldn't even Alt-Ctrl-F1 to a text console to kill the process.\nThis problem has just started since I installed the 2.1final rpm's. I also notice that when I swap desktops there's a delay before the background colour catches up. Any ideas what I'm doing wrong, or if upgrading to Xfree 4.0.2 would help?\n\nThanks"
    author: "markes"
  - subject: "Swap file problems  Help!!"
    date: 2001-03-02
    body: "OK, Help! I logged out of KDE2.1 and restarted the X-server, and now when I start up KDE2.1 up pops Krash with a sig11, neither Kicker or Kpanel appear, and I can't even left-click on the desktop to access the applications menu. What's happening?"
    author: "markes"
  - subject: "suse's kdesupport misses libaudio"
    date: 2001-03-02
    body: "It looks like that kdesupport from suse-7.0 misses the library libaudio.  Is it only me that encounters this problem or is it a known problem?\n\nI downloaded de rpm's from:\nhttp://ftp.sourceforge.net/pub/mirrors/kde/stable/2.1/distribution/rpm/SuSE/7.0-i386/\n\nWhen installing kde-support using rpm -Uvh kdelibs-2.1.0-0.i386.rpm, rpm complains about\nlibaudio missing.  libaudio is part of kdesupport....\n\nLooking forward to a updated kdesupport rpm for suse-7.0"
    author: "RIchard Bos"
  - subject: "Re: KDE 2.1 Has Landed -- At a Server Near You"
    date: 2001-03-12
    body: "what's up with all the missing dependencies in RedHat 7 RPMs? the following is a list of packages i tried to install and the missing dependencies i got.\n\nkdebase: libsensors.so.1\nkde1-compat-1.x: libqt.so.1\nkdelibs-sound-devel: audiofile-devel\nkdepim: libpisock.so.3\n\nthere's a reply somewhere here that shows you where to download a package to fix the first dependency, so i fixed that, but i don't know about the others. i suppose i could either force install them or find the dependencies on RPM Find or something, but i suppose don't really need them anyway. still, these things should IMO be taken care of in the original RPMs."
    author: "Rick Kreikebaum"
---
KDE 2.1 is officially out!  This is a solid release with major improvements to <A HREF="http://www.konqueror.org/">Konqueror</A> and <A HREF="http://devel-home.kde.org/~kmail/index.html">KMail</A>, the addition of the excellent IDE <A HREF="http://www.kdevelop.org/">KDevelop</A>, as well as the modular new multimedia player <A HREF="http://noatun.kde.org/">noatun</A>.  It has a whole slew of improvements over 2.0; you can find the change log <A HREF="http://www.kde.org/announcements/changelog2_0to2_1.html">here</A>.  The full press release is attached.  Enjoy -- I already installed it and it rules!





<!--break-->
<P>DATELINE FEBRUARY 26, 2001</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">New KDE Desktop Ready for the Enterprise</H3>
<P><STRONG>KDE Ships Leading Desktop with Advanced Web Browser for Linux
and Other UNIXes</STRONG></P>
<P>February 26, 2001 (The INTERNET).
The <A HREF="http://www.kde.org/">KDE
Project</A> today announced the release of KDE 2.1,
a powerful and easy-to-use Internet-enabled desktop for Linux. KDE
features <A HREF="http://www.konqueror.org/">Konqueror</A>, a
state-of-the-art web browser, as an integrated
component of its user-friendly desktop environment, as well as
<A HREF="http://www.kdevelop.org/">KDevelop</A>,
an advanced IDE, as a central component of KDE's powerful
development environment.
This release marks a leap forward in Linux desktop stability, usability
and maturity and is suitable for enterprise deployment.
The KDE Project strongly encourages all users to upgrade to KDE 2.1.
</P>
<P>
KDE and all its components are available for free under
Open Source licenses from the KDE
<A HREF="http://ftp.kde.org/stable/2.1/distribution/">server</A>
and its <A HREF="http://www.kde.org/mirrors.html">mirrors</A> and can
also be obtained on <A HREF="http://www.kde.org/cdrom.html">CD-ROM</A>.
KDE 2.1 is available in
<A HREF="http://i18n.kde.org/teams/distributed.html">33 languages</A> and
ships with the core KDE
libraries, the core desktop environment (including Konqueror), developer
packages (including KDevelop), as well
as the over 100 applications from the other
standard base KDE packages (administration, games,
graphics, multimedia, network, PIM and utilities).
</P>
<P>
"This second major release of the KDE 2 series is a real improvement in
terms of stability, performance
and features," said David Faure, release manager for KDE 2.1 and
KDE Representative at
<A HREF="http://www.mandrakesoft.com/">Mandrakesoft</A>.
"KDE 2 has
now matured into a solid, intuitive and complete desktop for daily use.
Konqueror is a full-featured and robust web browser
and important applications like the mail client (KMail) have greatly
improved.
The multimedia architecture has made great strides and
this release inaugurates the new media player noatun,
which has a modular, plugin design for playing the latest audio and 
video formats.
For development, KDE 2.1 for the first time is bundled
with KDevelop, an outstanding IDE/RAD which will be comfortably familiar to
developers with Windows development backgrounds.
In short, KDE 2.1
is a state-of-the-art desktop and development environment,
and positions Linux/Unix to make significant inroads in the home and
enterprise."
</P>
<P>
"KDE 2.1 opens the door to widespread adoption of the Linux desktop
and will help provide the success on the desktop that Linux already
enjoys in the server space," added Dirk Hohndel, CTO of
<A HREF="http://www.suse.com/">Suse AG</A>.
"With its intuitive
interface, code maturity and excellent development tools and environment, I am
confident that enterprises and third party developers will realize
the enormous potential KDE offers and will migrate their workstations
and applications to Linux/KDE."
</P>
<P>
"KDE boasts an outstanding graphical design and robust functionality," said
Sheila Harnett, Senior Technical Staff Member for IBM's Linux Technology
Center.
"KDE 2.1 significantly raises the bar for Linux desktop
functionality, usability and quality in virtually every aspect of the
desktop."
</P>
<P>
<STRONG><EM>KDE 2:  The K Desktop Environment</EM></STRONG>.
<A NAME="Konqueror"></A><A HREF="http://www.konqueror.org/">Konqueror</A>
is KDE 2's next-generation web browser,
file manager and document viewer.
The standards-compliant
Konqueror has a component-based architecture which combines the features and
functionality of Internet Explorer/Netscape
Communicator and Windows Explorer.
Konqueror supports the full gamut of current Internet technologies,
including JavaScript, Java, XML, HTML 4.0, CSS-1 and -2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator plug-ins (for Flash, RealAudio, RealVideo
and similar technologies).
</P>
<P>
In addition, KDE offers seamless network transparency for accessing
or browsing files on Linux, NFS shares, MS Windows
SMB shares, HTTP pages, FTP directories, LDAP directories and audio CDs.
The modular,
plug-in nature of KDE's file architecture makes it simple to add additional
protocols (such as IPX, WebDAV or digital cameras) to KDE, which would
then automatically be available to all KDE applications.
</P>
<P>
KDE's <A NAME="customizability">configurability and customizability</A>
touches every aspect of the desktop.
KDE offers a unique cascading
customization feature where customization settings are inherited through
different layers, from global to per-user, permitting enterprise-wide
and group-based configurations.
KDE's sophisticated theme support
starts with Qt's style engine, which permits developers and artists to
create their own widget designs.
KDE 2.1 ships with over 14 of these styles,
some of which emulate the look of various operating systems.
Additionally
KDE includes a new theme manager and does an excellent job of
importing themes from GTK and GNOME.
Moreover, KDE 2 fully
supports Unicode and KHTML is the only free HTML rendering engine on
Linux/X11 that features nascent support for BiDi scripts
such as Arabic and Hebrew.
</P>
<P>
KDE 2 features an advanced, network-transparent multimedia architecture
based on <A NAME="arts">aRts</A>, the Analog Realtime Synthesizer.
ARts is a full-featured sound system which
includes a number of effects and filters, a modular analog synthesizer
and a mixer.
The aRts sound server provides network transparent sound support for
both input and output using MCOP, a CORBA-like network design, enabling
applications running on remote computers to output sound and receive
input from the local workstation.
This architecture provides a much-needed complement
to the network transparency provided by X and for the first time permits
users to run their applications remotely with sound enabled.
Moreover, aRts enables multiple applications (local or remote) to
output sound and/or video concurrently.
Video support is <a
href="http://mpeglib.sourceforge.net/">available</a> for MPEG versions
1, 2 and 4 (experimental), as well as the AVI and DivX formats.
Using the aRts component technology, it is very easy to develop
multimedia applications.
</P>
<P>
Besides the exceptional compliance with Internet and file-sharing standards
<A HREF="#Konqueror">mentioned above</A>, KDE 2 is a leader in
compliance with the available Linux desktop standards.
KWin, KDE's new
re-engineered window manager, complies to the new
<A HREF="http://www.freedesktop.org/standards/wm-spec.html">Window Manager
Specification</A>.
Konqueror and KDE comply with the <A
HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/kio/DESKTOP_ENTRY_STANDARD">Desktop
Entry Standard</A>.
Konqueror uses the  
         <A HREF="http://pyxml.sourceforge.net/topics/xbel/docs/html">XBEL</A>
standard for its bookmarks.
KDE 2 largely complies with the
<A HREF="http://www.newplanetsoftware.com/xdnd/">X Drag-and-Drop (XDND)
protocol</A> as well as with the
<A HREF="http://www.rzg.mpg.de/rzg/batch/NEC/sx4a_doc/g1ae04e/chap12.html">X11R6 session management protocol (XSMP)</A>.
</P>
<P>
<STRONG><EM>KDE 2:  The K Development Environment</EM></STRONG>.
KDE 2.1 offers developers a sophisticated IDE as well as a rich set
of major technological improvements over the critically acclaimed
KDE 1 series.
Chief among the technologies are
the <A HREF="#DCOP">Desktop COmmunication Protocol (DCOP)</A>, the
<A HREF="#KIO">I/O libraries (KIO)</A>, <A HREF="#KParts">the component
object model (KParts)</A>, an <A HREF="#XMLGUI">XML-based GUI class</A>, and
a <A HREF="#KHTML">standards-compliant HTML rendering engine (KHTML)</A>.
</P>
<P>
KDevelop is a leading Linux IDE
with numerous features for rapid application
development, including a GUI dialog builder, integrated debugging, project
management, documentation and translation facilities, built-in concurrent
development support, and much more.
</P>
<P>
<A NAME="KParts">KParts</A>, KDE 2's proven component object model, handles
all aspects of application embedding, such as positioning toolbars and insertingthe proper menus when the embedded component is activated or deactivated.
KParts can also interface with the KIO trader to locate available handlers for
specific mimetypes or services/protocols.
This technology is used extensively by the
<A HREF="http://www.koffice.org/">KOffice</A> suite and Konqueror.
</P>
<P>
<A NAME="KIO">KIO</A> implements application I/O in a separate
process to enable a
non-blocking GUI without the use of threads.
The class is network and protocol transparent
and hence can be used seamlessly to access HTTP, FTP, POP, IMAP,
NFS, SMB, LDAP and local files.
Moreover, its modular
and extensible design permits developers to "drop in" additional protocols,
such as WebDAV, which will then automatically be available to all KDE
applications.
KIO also implements a trader which can locate handlers
for specified mimetypes; these handlers can then be embedded within
the requesting application using the KParts technology.
</P>
<P>
The <A NAME="XMLGUI">XML GUI</A> employs XML to create and position
menus, toolbars and possibly 
     other aspects of the GUI.
This technology offers developers and users
the advantage of simplified configurability of these user interface elements
across applications and automatic compliance with the
<A HREF="http://developer.kde.org/documentation/standards/">KDE Standards
and Style Guide</A> irrespective of modifications to the standards.
</P>
<P>
<A NAME="DCOP">DCOP</A> is a client-to-client communications
protocol intermediated by a
server over the standard X11 ICE library.
The protocol supports both
message passing and remote procedure calls using an XML-RPC to DCOP "gateway".
Bindings for C, C++ and Python, as well as experimental Java bindings, are
available.
</P>
<P>
<A NAME="KHTML">KHTML</A> is an HTML 4.0 compliant rendering
and drawing engine.
The class
will support the full gamut of current Internet technologies, including
JavaScript, Java, HTML 4.0, CSS-2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator plugins (for
viewing Flash,
RealAudio, RealVideo and similar technologies).
The KHTML class can easily
be used by an application as either a widget (using normal window
parenting) or as a component (using the KParts technology).
KHTML, in turn, has the capacity to embed components within itself
using the KParts technology.
</P>
<H4>Downloading and Compiling KDE 2.1</H4>
<P>
The source packages for KDE 2.1 are available for free download at
<A HREF="http://ftp.kde.org/stable/2.1/distribution/tar/generic/src/">http://ftp.kde.org/stable/2.1/distribution/tar/generic/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>.
KDE 2.1 requires
qt-2.2.4, which is available in source code from Trolltech as
<A HREF="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.4.tar.gz">qt-x11-2.2.4.tar.gz</A>.
KDE 2.1 should work with Qt-2.2.3 but Qt-2.2.4 is recommended.
</P>
<P>
For further instructions on compiling and installing KDE 2.1, please consult
the <A HREF="http://www.kde.org/install-source.html">installation
instructions</A> and, if you encounter problems, the
<A HREF="http://www.kde.org/compilationfaq.html">compilation FAQ</A>.
</P>
<H4>Installing Binary Packages</H4>
<P>        
  Some distributors choose to provide binary packages of KDE for certain
versions of their distribution.
Some of these binary packages for KDE 2.1
will be available for free download under
<A HREF="http://ftp.kde.org/stable/2.1/distribution/">http://ftp.kde.org/stable/2.1/distribution/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>.
Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.
</P>
<P>KDE 2.1 requires qt-2.2.4, the free version of which is available
from the above locations usually under the name qt-x11-2.2.4.
KDE 2.1
should work with Qt-2.2.3 but Qt-2.2.4 is recommended.
<P>
At the time of this release, pre-compiled packages are available for:
</P>
<UL>
<LI>Caldera eDesktop 2.4: <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/Caldera/eDesktop24/RPMS/">i386</A></LI>
<LI>Debian GNU/Linux:
<UL>
<LI>Potato (2.2):  <A HREF="http://ftp.kde.org/stable/2.1/distribution/deb/dists/potato/main/binary-i386/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1/distribution/deb/dists/potato/main/binary-sparc/">Sparc</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/deb/dists/potato/main/binary-powerpc/">PPC</A>;
please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/deb/dists/potato/main/binary-all/">main</A> directory for common files</LI>
<LI>Stable (2.3):  <A HREF="http://ftp.kde.org/stable/2.1/distribution/deb/dists/stable/main/binary-i386/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1/distribution/deb/dists/stable/main/binary-sparc/">Sparc</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/deb/dists/stable/main/binary-powerpc/">PPC</A>;
please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/deb/dists/stable/main/binary-all/">main</A> directory for common files</LI>
</UL>
<LI>Linux-Mandrake 7.2:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/Mandrake/7.2/i586/">i586</A></LI>
<LI>RedHat Linux:
<UL>
<LI>Wolverine:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/wolverine/i386/">i386</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/wolverine/common/">common</A> directory for common files</LI>
<LI>7.0:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/alpha/">Alpha</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/common/">common</A> directory for common files</LI>  
  <LI>6.x:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/i386/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/alpha/">Alpha</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/sparc/">Sparc</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/common/">common</A> directory for common files</LI>
</UL>
<LI>SuSE Linux:
<UL>
<LI>7.1:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/7.1-i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/7.1-sparc/">Sparc</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/i18n/">i18n</A> or <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/noarch/">noarch</A> directory for common files</LI>
<LI>7.0:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/7.0-i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/7.0-ppc/">PPC</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/i18n/">i18n</A> or <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/noarch/">noarch</A> directory for common files</LI>
<LI>6.4:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/6.4-i386/">i386</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/i18n/">i18n</A> or <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/noarch/">noarch</A> directory for common files</LI>
<LI>6.3:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/6.3-i386/">i386</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/i18n/">i18n</A> or <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/noarch/">noarch</A> directory for common files</LI>
</LI>
</UL>
 
 
<LI><A HREF="http://ftp.kde.org/stable/2.1/distribution/tar/Tru64/">Tru64 Systems</A></LI>
<LI><A HREF="http://ftp.kde.org/stable/2.1/distribution/tar/FreeBSD/">FreeBSD</A></LI>
</UL>
<P>
Please check the servers periodically for pre-compiled packages for other
distributions.
More binary packages will become available over the
coming days and weeks.
</P>
<H4>What Others Are Saying</H4>
<P>KDE 2.1 has already earned accolades from industry leaders worldwide.
A sampling of comments follows.
</P> 
<P>
"We welcome the release of KDE 2.1," stated Dr. Markus Draeger, Senior Manager for Partner Relations at Fujitsu Siemens Computers. "The release introduces several important new components, like KDevelop and the media player noatun, and overall is a major step forward for this leading GUI on Linux."
</P>
  <P>
"We are very excited about the enhancements in KDE 2.1 and we are pleased
to be able to contribute to the project," said Rene Schmidt, Corel's
Executive Vice-President, Linux Products. "KDE continues to improve with
each release, and these enhancements will make our easy-to-use Linux
distribution for the desktop even better."
</P>
<P>
"A greater number and availability of Linux applications is an important
factor that will determine if Linux permeates the enterprise desktop,"
said Drew Spencer, Chief Technology Officer for Caldera Systems, Inc.
"KDE 2.1 addresses this issue with the integration of the Konqueror
browser and KDevelop, a tool that allows developers to create
applications in C++ for all kinds of environments.  Together with the
existing tools available for KDE, KDevelop is a one-stop solution for
developers."
</P>
<P>
"With the 2.1 release, KDE again demonstrates its capacity to offer rich
software and provide a complete and stable environment for everyday use",
added Ga&euml;l Duval, co-founder of Mandrakesoft. "This latest release
has paved the way for KDE on user's desktops in the enterprise as well
as at home.  From the full-featured web browser to the friendly
configuration center, it provides all the common facilities many
computers users need to abandon Windows entirely."
</P>
<H4>About KDE</H4>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environmentemploying a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.
</P>
<P>
For more information about KDE, please visit KDE's
<A HREF="http://www.kde.org/whatiskde/">web site</A>.
More information about KDE 2 is available in two
(<A HREF="http://devel-home.kde.org/~granroth/LWE2000/index.html">1</A>,
<A HREF="http://mandrakesoft.com/~david/OSDEM/">2</A>) slideshow
presentations and on
<A HREF="http://www.kde.org/">KDE's web site</A>, including an evolving
<A HREF="http://www.kde.org/info/2.1.html">FAQ</A> to answer questions about
migrating to KDE 2.1 from KDE 1.x, a number of
<A HREF="http://www.kde.org/screenshots/kde2shots.html">screenshots</A>, <A HREF="http://developer.kde.org/documentation/kde2arch.html">developer information</A> and
a developer's
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html">KDE 1 - KDE 2 porting guide</A>.
</P>
    <HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
KDE and K Desktop Environment are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
MS Windows, Internet Explorer and Windows Explorer are trademarks or registered
trademarks of Microsoft Corporation.
Netscape and Netscape Communicator are trademarks or registered trademarks of Netscape Communications Corporation in the United States and other countries and JavaScript is a trademark of Netscape Communications Corporation.
Java is a trademark of Sun Microsystems, Inc.
Flash is a trademark or registered trademark of Macromedia, Inc. in the United States and/or other countries.
RealAudio and RealVideo are trademarks or registered trademarks of RealNetworks, Inc.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
granroth@kde.org<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
pour@kde.org<BR>
(1) 718 456 1165
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
faure@kde.org<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
konold@kde.org<BR>
(49) 179 2252249
</TD></TR>
</TABLE>
<p>
<b>[Note: Posting and Thread Thresholds for this article were broken since the early morning of 27 Feb 2001 till the afternoon of the same day, due to a new caching strategy.  Our apologies for the inconvenience.]</b>






