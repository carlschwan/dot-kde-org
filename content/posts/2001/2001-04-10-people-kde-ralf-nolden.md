---
title: "People of KDE: Ralf Nolden"
date:    2001-04-10
authors:
  - "Inorog"
slug:    people-kde-ralf-nolden
comments:
  - subject: "Re: People of KDE: Ralf Nolden"
    date: 2001-04-10
    body: "He should write the KDevelop book. I will buy it, pronto."
    author: "reihal"
  - subject: "Re: People of KDE: Ralf Nolden"
    date: 2001-04-10
    body: "Hi, KandAlf, thank you for your all support you gave us on #kde-users. Kdevelop is a wonderfull tool, and I am using it every day :)"
    author: "Antialias"
  - subject: "(ot) Where is #kde-users"
    date: 2001-04-10
    body: "Hello all,\nI just have this question: where can I find the #kde-users channel, on which IRC network is it on? I am currently on Undernet, but I guess it's not there...\nThanks for your attention,\nHendric (Switzerland)"
    author: "Hendric Stattmann"
  - subject: "Re: (ot) Where is #kde-users"
    date: 2001-04-10
    body: "We're on Openprojects.net, try irc.kde.org or irc.openprojects.net."
    author: "Michael Driscoll"
  - subject: "Re: (ot) Where is #kde-users"
    date: 2001-04-10
    body: "Network: dalnet\nServer: irc.kde.org"
    author: "Antialias"
  - subject: "Re: (ot) Where is #kde-users"
    date: 2001-04-11
    body: "It's <b>not</b> DALnet; irc.kde.org is an alias to irc.openprojects.net"
    author: "Triskelios"
  - subject: "Re: People of KDE: Ralf Nolden"
    date: 2001-04-11
    body: "Well I don't use kdevelop because it doesn't have the features I need, that is xemacs behaviour/power.\nwould be cool to embed xemacs inside kdevelop!\nmay be it is possible to do that with the XPart technology. somebody has experience with this?\nI guess that the same with vi users !\nOn a side note I have converted ALL students of my group(at MIT) to KDevelop and they just love it !!\nthanks a LOT for this tool.\nKde is spreading slowly but steadily here. \nadvocating is slow process but when you have wonderful softwares to advertise it is not that difficult.\n\nbest regards."
    author: "jesunix"
  - subject: "kdevelop in a MIT research group"
    date: 2001-04-11
    body: "Well I don't use kdevelop because it doesn't have the features I need, that is xemacs behaviour/power.\nwould be cool to embed xemacs inside kdevelop!\nmay be it is possible to do that with the XPart technology. somebody has experience with this?\nI guess that the same with vi users !\nOn a side note I have converted ALL students of my group(at MIT) to KDevelop and they just love it !!\nthanks a LOT for this tool.\nKde is spreading slowly but steadily here. \nadvocating is slow process but when you have wonderful softwares to advertise it is not that difficult.\n\nbest regards."
    author: "jesunix"
---
<a href="http://www.kde.org/people/ralf.html">Ralf Nolden</a> is an energetic KDE advocate and one of the main developers of <a href="http://www.kdevelop.org/">KDevelop</a>, the highly praised integrated development platform based on KDE. While answering <a href="mailto:tink@kde.org">Tink</a>'s questions, Ralf confesses his strong desire to implement advanced coffee brewing in an upcoming KDE release. Find this and more, in <a href="http://www.kde.org/people/ralf.html">this week's edition</a> of <a href="http://www.kde.org/people/people.html">The People Behind KDE</a>.