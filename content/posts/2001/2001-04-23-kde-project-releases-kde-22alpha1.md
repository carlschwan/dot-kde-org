---
title: "KDE Project Releases KDE 2.2alpha1"
date:    2001-04-23
authors:
  - "Dre"
slug:    kde-project-releases-kde-22alpha1
comments:
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "what about k-installer?\n\nare there any plans to include it in kde 2.2?\n\nis there any progress on this module?"
    author: "steve_qui"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "I don't think it will be in KDE2.2 itself, however it could very well be ready for use by then. It is still in the kdenonbeta cvs module but is being actively developed and progressing nicely. You can check out the progress at Nick's site: http://rox0rs.net/kdeinstaller"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "we need krayon....\n\nis it in kde 2.2 final?\n\nwho knows something?"
    author: "steve_qui"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "It _might_ be in KOffice 1.1, which is a seperate release from KDE 2.2."
    author: "jd"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "There is something like koffice-1.1beta on the KDE-servers, too..."
    author: "AC"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: ">For the desktop, KDE 2.2 adds the new audiocd:/ ioslave, which, for example, permits you to drag'n'drop audio CD tracks onto your desktop using Konqueror\n\nwhat would be cool, is for people to be able to burn CD's from konqueror, I've heard M$ Explorer on XP will be able to  do it..."
    author: "t0m_dR"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "But this feature should be included into the kernel fs. It has nothing to do with KDE."
    author: "Christian Parpart"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "I'm not talking about CDRW support (treating cd's as floppies!) I'm talking about plain old Cd recording, and using KONQUEROR as a front end for cd burning programs"
    author: "t0m_dR"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "I've read once that this is one of the goals Konqueror is aiming for, but unfortunately don't remember the link or the details. I think it was said that Konqueror will eventually be able to burn a CD using KreateCD or something else."
    author: "Bojan"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "This would be good.\n\nI'd like it to be taken a step further though.  What about when encoding the CD's ID  into the comment field in the id3 tag?\n\nThen when burning mp3/oggs back onto audio CD, if all the tracks have the same ID, the ID is burnt back onto the correct place on the CD, so that anyone playing the CD back can query an online DB for the correct track names.\n\nEven better if the burner could burn the audio ID and place the tracks in the correct place by using the track number.\n\nHope i'm explaining this well. :-)"
    author: "Ack."
  - subject: "Desparately seeking Kate"
    date: 2001-04-23
    body: "Hi,\nid there someone nice outther who is willing to\nprovide a binary rpm from Kate for Mandrake8?\nThis is a  program which has undergone dramatic\ndevelopment since 2.1.1 and it would be really nice, if I wouldnt have to comlile everything.\n\nI think this could be possible"
    author: "compile lazy"
  - subject: "Re: Desparately seeking Kate"
    date: 2001-04-23
    body: "It's possible to build Kate from CVS sources using KDE2.1.1 libraries...\n\nGet the kdebase module, do make -f Makefile.cvs, ./configure --prefix=/usr, then go into the kate directory, and edit the Makefile's to remove -lkdeprint (or such), and replace all references to KPrint in the sources with QPrint..."
    author: "A Sad Person"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "With over 683 posts in the bugs database regarding the Konqueror 'process for the http://www.foo.bar protocol died unexpectedly' error (which I'm told arises from a problem with kio_http), can the people who are experiencing this problem right up to KDE 2.1.1 release (like ME) be reassured that it's fixed in this new version?\n\nThanks..."
    author: "Weevil"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "Yes, that bug is quite annoying. Anyone knows about this being fixed??"
    author: "Bojan"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "Yes, that bug is quite annoying. Anyone knows about this being fixed??"
    author: "Bojan"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "Hmm!\nI found 8 matches in the bugs database searching for\n\"protocol and  died and unexpectedly not done\".\n\nMost of these bugs can't be reproduced any more with the current CVS. One site crashes Netscape too (cyrilic encoding?).  \nferdinand"
    author: "ferdinand gassauer"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "Yes, this is supposed to be fixed in 2.2 as well as in KDE 2.1.2, which we hope to release soon.\n\nThe problem happened on relative slow connections\nwhen the webserver closed the connection while we\nwere trying to write to it.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "When is this kde 2.1.2 going to be coming out?? I havent heard anything about there being a 2.1.2 release befor reading this! Thats cool!"
    author: "Bakiller"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "2.1.2?\nI did not receive any comment on the\n10 bug reports I reported lately. So I do hope\nit will not be released to quickly!\n\nAC"
    author: "AC"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "I really hope so, this _very_ annoying bug prevents me using konqueror... However I don't really have a slow connection (cable modem) and this messages appears frequently (it works on the 4th or 5th try) and the message appears immediately after pressing Enter."
    author: "Henri"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "It happens at fast connections, too.\nIf you use ISDN with autoconnect-mode it happens always if isdn chanceld the connection."
    author: "Daniel Mandler"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-06-02
    body: "AFAIK, 2.1.2 has been released already\nThere's an announcement on \n<A HREF=http://www.kde.org/announcements/announce-2.1.2.html>www.kde.org<A>"
    author: "JPK"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "You are a troll, trolling helps no-one, if it's in the bug database people know about it."
    author: "ac"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "AFAIK, Kpilot (KDEPIM package) will now include the long-awaited adressbook conduit."
    author: "pasha"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "Wow, this is cool. Hopefully it got a import filter for vcards too. Few code with this intention was lying around for some time on the KDE server, but never get implemented I guess."
    author: "Andre"
  - subject: "Themes ?."
    date: 2001-04-23
    body: "Theming. Theming has gotten a big boost with support for both Windows themes from Tucows and pixmap-based IceWM themes and the addition of a Quartz Style (KCKDE Issue 4).\n\nI have compiled KDE 2.2alpha from source, but this theming thing do not show up in kcontroll, or anywhere else. Nor can i find any information on where to uncompress my icewm themes.\n\nWhere is it?. \nIs there something I have missed (configure options?)."
    author: "tjodleiv"
  - subject: "Re: Themes ?."
    date: 2001-04-23
    body: "Same here.\nWhere is the place you untar icewm files ?.\nHow do you enable them ?."
    author: "themelover"
  - subject: "Re: Themes ?."
    date: 2001-04-23
    body: "In Kcontrol > Look and feel > Window decorations."
    author: "Titlebar"
  - subject: "Re: Themes ?."
    date: 2001-04-23
    body: "yeah, I forgot to tell you :), you have to gunzip and untar IceWM themes to /.kde/share/apps/kwin/icewm-themes directory.\nIn Kcontrol > Look and feel > Window decorations you have to choose IceWM window decoration and then click on tab Configure and there you can choose your IceWM themes.\n Enjoy ;)"
    author: "Titlebar"
  - subject: "Re: Themes ?."
    date: 2001-04-23
    body: "Hi,\n\nI'm the author of the IceWM kwin client. It is unfortunate that the IceWM client did not make it into the kde 2.2 alpha1, but it will be in 2.2 beta 1. Its currently in the KDE CVS repository HEAD branch. There is also an associated kcontrol module to modify button positions, and set icewm themes. Quartz and modernsystem can now modify their button positions too via this new kcontrol module.\n\nSee http://gallium.n3.net/ for a preview of KWin-IceWM in action.\n\nEnjoy!"
    author: "gallium"
  - subject: "Re: Themes ?."
    date: 2001-04-23
    body: "is it possible to compile the icewm client with kde 2.1.1?"
    author: "Evandro"
  - subject: "Re: Themes ?."
    date: 2001-04-24
    body: "I have a HOW-TO I created about using the IceWM style in KDE 2.1.x at http://www.uninetsolutions.com/tbutler/linux/001.html .\nI hope you find this helpful.\n\n  HTH,\n    Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Themes ?."
    date: 2001-04-24
    body: "Yes it is, just get a resent snapshot of kdebase, then you just compile kcontroll and kwin. Start kde and go to kontrolsenter, under look and feel you will see it. Choose the icewm decorations and it will be enabled.\n\nIt works great."
    author: "ac"
  - subject: "Re: Themes ?."
    date: 2001-04-23
    body: "Actually, what I would like to see is all Gnome & KDE applications to look the same.  I know KDE will currently force Gnome apps to use the same color, but it still uses that butt ugly default gnome theme.\n\nWhat I am talking about is, that Gnome & KDE use the same color, theme, fonts, etc.\n\nHopefully when motif adds support for Gnome & KDE, atleast those apps will look the same.  And it would be cool if Qt apps would use KDE themes.\n\nSo I'm just saying I want some consistency!"
    author: "Henry Stanaland"
  - subject: "Re: Themes ?."
    date: 2001-04-24
    body: "Sharing a common theme engine was talked about during GUADEC.  Also ACS Motif can know use Gtk themes.  What would be cool is a common base like Xt, then a shared component model(plus shared themes), that way Gtk and Qt could be mixed eisly, and there could be KDE bindings to Gtk and GNOME bindings to Qt"
    author: "ac"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "What's with the import-tool for NetscapeMail-folders into KMail?"
    author: "Frank"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "\"K\" Menu => Utilities => KMail Import\n\nThe binary is called \"kmailcvt\"\n\nHTH,\n\nDaniel"
    author: "Daniel Molkentin"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "But it's NOT for NetscapeMail-Folders! Only for the addressbook, right?"
    author: "Frank"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-25
    body: "AFAIK Netscapemail uses exactly the same mailbox format of KMail... I don't think there is a need for an importing tool!"
    author: "Vajvarana"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-28
    body: "I'm surprised. It doesn't work here. How Do I import the Folders?"
    author: "Frank"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "Theming.\n\nThis is going in the wrong way. Why do you people need those themes so much. HiColor Default is very good all by itself. What would really be ok, is a title like this: \"All KDE apps start 40% faster!\" now __THIS__ would be ok. Screw themes, speed is what matters!"
    author: "ac"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "Some of us would like to use themes, if you dont want to us them no one will force you."
    author: "themelover"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "Hmm. I just wonder why I see more and more KDE coder using gprof. Must be about speed improvement, no? ;)\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "don't get me wrong. i don't have anything against themeing, i just think that developers are more concentrated on kde looking great, not performance. take m$ for example: (i hate them) windows looks ok, kde looks much much better, BUT, explorer takes half a second to start on my celeron 333 with 64 megs o' ram, konqueror take about 3-4 seconds, if not more. i think that kde has reached a high enough state of GUI look, and developers should concentrate more on performance"
    author: "ac"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "yeah well M$ loads Explorer into memory at start up so it is just a matter of drawing the window. and since explorer and IE are the same thing now both load quickly."
    author: "The Dude"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "Whatever M$ does to make Explorer go faster, I LIKE it!  I wish KDE had that ability too.  I really like Konqueror, but the fact that it takes 10-15X as long as IE/Explorer to start up is annoying.  Even opening a new window of an existing instance of Konqueror takes *much* longer than starting IE.\n\nIs there anything that could be done?  Preloading Konqueror would be a *good* thing, in my opinion, as long as it could be turned off in the Control panel for people who don't like that sort of thing.  Even if it used 5-10 more MB of memory, I'd still like the feature to be there."
    author: "not me"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "I don't know where I read it, but I believe that the reason Konqueror does not load automatically is that it wastes memory to keep everything loaded always in the background. This is something that users of KDE 1 protested strongly. Check old issues of the KC or browse the mailing list for discussions of the design of KDE 2 with these regards (I'm not saying that KDE 2 is lightweight, just that it's not as heavy as it might be).\n\nFor the most part, bits are only loaded when needed. Even konqueror for me seems to load the HTML renderer only when viewing a web page (start it as the file manager and type a URL to a web page to see what I mean). That's the price you pay for the modularity that has meant rapid development and clean/flexible design for KDE.\n\nThe most simple solution to the speed problem is for you to start Konqueror and not close it. It takes much less time to spawn a new window than to restart Konqueror from scratch. If it gets in your way, just send it to another virtual desktop."
    author: "Brent Cook"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "I know that pre-loading Konqueror takes more memory.  My point is, I am willing to give up 5-10 MB of memory to have a much faster-starting Konqueror.  I would like to have the option of pre-loading Konqueror and KHTML into memory so that whenever I need them, they will pop up immediately.  M$ knows the benefits of this and that is why they do it in Windows (despite popular opinion to the contrary, they are pretty smart).\n\nStarting Konqueror and leaving it somewhere helps a little, but as I said, even opening an new window of an existing instance of Konqueror takes noticably more time than \"starting\" IE in Windows.  I think this could be improved."
    author: "not me"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "You can always preload Konqueror by putting shortcut to Konqueror to Autostart folder (Konqueror will be automatically loaded every time you start kde), or not closing Konqueror when you log out and check Save session."
    author: "Antialias"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "Hmmm... 'by putting shortcut...' ...\nALLO KDE People ! open yuor eyes !\nI installed today the new 8.0 LinuxMandrake distribution, and then created 2 users, the first under a KDE session, the second over a GNOME session...:\n\nResults where Gnome desktop environement loads 3/4 times faster.\n\nI know the solution is to use Gnome, :) but I do love the Konqueror !"
    author: "Ddread"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "humm,\n\n1. altough faster starting applications maybe make you feel more comfortable, they don't enhance your productivity, and thats also what themes do no ?.\n2. I don't think apps can start much quicker than now because of the design of X and so (or am i wrong) ... X apps generally do start slow here, altough once started they work fast. (maybe zerocopy for unix sockets would help here, but now i am really talking about things i haven't a clue...)"
    author: "me"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-25
    body: "au contraire mon-swear...\n\nFaster starting of your browser (including the case of opening a new window) makes a big dent in productivity.  When bouncing back and forth between different reference sites, those 4 second delays add up, and more importantly, break your rhythm.  The interface doesn't have to be lighting fast, but needs to move as fast as you think to not put a drag on your flow."
    author: "chchchain"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-26
    body: "2. not really. i've written some apps which start up instantly (very small apps, though)."
    author: "Ev andro"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-26
    body: "X apps can start up almost instantly, one of the problems is that the apps themselves need work, but KDE has a bad habit of making apps take like 3 secs to load no matter what they are, other desktops don't do that."
    author: "ac"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "Don't get me wrong but if you know what is best for KDE why don't you take a more active, less talk more do, role? It's easy to sit on the sidelines and say what should be done but actually doing the coding and the planning is a much different game."
    author: "me"
  - subject: "2.2alpha performance"
    date: 2001-04-25
    body: "while its not mentioned in any release i've seen so far, in 2.2alpha, konq is much faster at opening new windows and loading pages from the cash. i've had some trouble with a few rendering bugs, but other then that, its been as stable as 2.1.1 for me (although i do not run a full kde desktop, just konq)\n\nform completion is nice too! \n\ncheck it out!"
    author: "jstultz"
  - subject: "Re: 2.2alpha performance"
    date: 2001-04-25
    body: ">in 2.2alpha, konq is much faster at opening new windows and loading pages from the cash\n\nNeato!  That sounds great!  I'm really going to have to fire up CVS here..."
    author: "not me"
  - subject: "2.2alpha performance"
    date: 2001-04-25
    body: "while its not mentioned in any release i've seen so far, in 2.2alpha, konq is much faster at opening new windows and loading pages from the cash. i've had some trouble with a few rendering bugs, but other then that, its been as stable as 2.1.1 for me (although i do not run a full kde desktop, just konq)\n\nform completion is nice too! \n\ncheck it out!"
    author: "jstultz"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "Take your trolls back to slashdot."
    author: "Scott"
  - subject: "Damn right!"
    date: 2001-04-23
    body: "Slashdot is the ideal place for trolls!\nIn fact: Slashdot is only intended for trolls!"
    author: "ac"
  - subject: "\"Stop trolling\" really means \"I disagree\""
    date: 2001-04-24
    body: "I think the original poster has a very valid point.  Just because you love themes doesn't mean he's trolling.\n\nAnd before someone asks me what I've done for KDE--i.e., attempts to dismiss my comment--I am packing up a nice, optimized build of KDE for my friends.  Speed does matter."
    author: "Chuck"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "ac has the point. Theming is cool, but not so important. It would be better to concentrate on KOffice and its import/export filters."
    author: "Bojan"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "I would be cool if KDE/Konqueror would run 'good' in 16 megs of memory, the KDE people would still be lagging behind QNX, but not far."
    author: "ac"
  - subject: "Trolls are dying!"
    date: 2001-04-23
    body: "You don't have to be Kreskin to predict trolls' future.\nThe hand writing is on the wall: trolls faces a bleak future.\nIn fact there won't be any future at all for trolls because trolls are dying.\nThings are looking very bad for trolls.\nAs many of us are already aware, trolls continues to reduce in numbers.\nRed ink flows like a river of blood.\n\nLet's keep to the facts and look at the numbers.\n\nTroll leader ac states that there are 2000 trolls. How many usenet trolls are there?\nLet's see. The number of Slashdot trolls versus usenet trolls posts is roughly in ratio of 5 to 1.\nTherefore there are about 5000/5 = 1000 usenet trolls post.\nUsenet trolls posts are about half of the volume of Slashdot trolls posts.\nTherefore there are about 500 usenet trolls.\nSo there are only 1500 Slashdot trolls, compared to the 3 bilion Internet users.\n\nAll major surveys show that trolls have steadily decreased in numbers.\nTrolls are very sick and its long term survival prospects are very dim.\nIf trolls want survive at all it will be among trolling hobbyists, Microsoft zealots,\nand people with sick minds.\nTrolls continues to decay. Nothing short of a miracle could save it at this point in time.\nFor all practical purposes, trolls are dead."
    author: "TrollKiller"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-26
    body: "kde core developers are working on that.\n\nthe thing is, Karol Szwed wanted to use icewm themes with kwin and implemented that (remember, kde is free software). there's no reason to complain."
    author: "Evandro"
  - subject: "Clipboard buffer too small"
    date: 2001-04-23
    body: "Why is it that every version of KDE has such a small clipboard buffer? This is one of the most annoying things about KDE for me.\nPlease fix it!"
    author: "Mark Swanson"
  - subject: "Re: Clipboard buffer too small"
    date: 2001-04-23
    body: "This is nothing to do for KDE. It is a Qt issue. Expect a better behavior in Qt 3.\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "I Really like KDE and i hope that those distro's keep that in mind and package it quick. It's their job cause i'm paying them money when i buy a linux suite in the shop (yes i'm a newbie kind of user!)"
    author: "jasja"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "I would recommend using a distro that actively supports KDE.  I've been a RedHat user for about 4 years, but just switched to SuSE becasue RedHat is leaning very much towards Gnome/GTK."
    author: "Scott"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-26
    body: "please, look at your linux box again.\n\nit doesn't say anywhere \"we'll create new kde packages when a new version is released\". you didn't pay for that."
    author: "Evandro"
  - subject: "What about the new smblib ?"
    date: 2001-04-23
    body: "Does anyone know if the new SMBLIB is in ?"
    author: "Biswa"
  - subject: "Re: What about the new smblib ?"
    date: 2001-04-23
    body: "Nope. It should be included in the new Samba Release (2.2.0). Get it and install it. If the kdebase's configure scripts detects smblib, the new smb io-slave will be built rather than the old read-only client based on the smbtools.\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: What about the new smblib ?"
    date: 2001-04-24
    body: "Does the new client allow browsing workgroups like the Windows Network Neighborhood?  I would really like that functionality in Konqueror."
    author: "not me"
  - subject: "Re: What about the new smblib ?"
    date: 2001-04-24
    body: "It's not in 2.2.0. You have to check out Samba from CVS and compile libsmbclient by hand. I did so, but the I/O slave is still pretty buggy: I can use it to browse hosts directly, but I can't see the network neighbourhood (which I thought was a supported feature). It all feels pretty shaky, though less so than the smbclient wrapper.\n\nIt definitely still needs some work, though perhaps some of the bugs are just because I use FreeBSD."
    author: "Theo van Klaveren"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "Would someone tell me what is up with the addressbook and kmail? Take a look at the included screenshot. In KDE2.1.1's full-fledged addressbook, I have all the information for names and so forth for these people. But why can't that be included in the address book's entry? Can't we have:\n\n<Last Name>, <First Name>, <Address>\n\n??"
    author: "EKH"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-23
    body: "Just go to Configuration -> Appearance -> Addressbook and choose a more powerful addressbook."
    author: "Daniel Naber"
  - subject: "Trolls are dying!"
    date: 2001-04-23
    body: "You don't have to be Kreskin to predict trolls' future.\nThe hand writing is on the wall: trolls faces a bleak future.\nIn fact there won't be any future at all for trolls because trolls are dying.\nThings are looking very bad for trolls.\nAs many of us are already aware, trolls continues to reduce in numbers.\nRed ink flows like a river of blood.\n\nLet's keep to the facts and look at the numbers.\n\nTroll leader ac states that there are 2000 trolls. How many usenet trolls are there?\nLet's see. The number of Slashdot trolls versus usenet trolls posts is roughly in ratio of 5 to 1.\nTherefore there are about 5000/5 = 1000 usenet trolls post.\nUsenet trolls posts are about half of the volume of Slashdot trolls posts.\nTherefore there are about 500 usenet trolls.\nSo there are only 1500 Slashdot trolls, compared to the 3 bilion Internet users.\n\nAll major surveys show that trolls have steadily decreased in numbers.\nTrolls are very sick and its long term survival prospects are very dim.\nIf trolls want survive at all it will be among trolling hobbyists, Microsoft zealots,\nand people with sick minds.\nTrolls continues to decay. Nothing short of a miracle could save it at this point in time.\nFor all practical purposes, trolls are dead."
    author: "TrollKiller"
  - subject: "Re: Trolls are dying!"
    date: 2001-04-24
    body: "KDE suXXors, GNOME roXXors."
    author: "ac"
  - subject: "Re: Trolls are dying!"
    date: 2001-06-01
    body: "dude speak is so damn gay,......matter of fact the use of the word \"gay\" to describe somthing stupid is quite dull. what 14 year old retarded girl invented this crap?\n\nit's about as cool a valley girl talk ANOTHER dopey way to express yourself."
    author: "end dood talk forever"
  - subject: "Any word on streaming audio?"
    date: 2001-04-23
    body: "> New applications. Kaboodle, a quick\n> multimedia viewer/player, has been added\n> to kdemultimedia (KCKDE Issue 5). It\n> complements noatun but is intended for use\n> as a KPart (e.g., to preview \n\nDoes anyone know if multimedia supports\nstreaming audio yet?  Noatun is really awesome,\nbut it's crippled by not supporting streaming\ninput."
    author: "George R. Welch"
  - subject: "Re: Any word on streaming audio?"
    date: 2001-04-24
    body: "Nikolas Zimmerman is working on this. Expect more for beta1.\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Is Noatun really what kde needs?"
    date: 2001-04-24
    body: "Why does kde keep getting new media players?  This seems to be some seriously treaded ground.  Everybody wants to make there own brand new mp3 player from scratch, or a SQL mp3 organizer.  It seems like more people could benefit from  a KDE enhanced version of XMMS.  Some common features between GNOME & KDE would be great, the player is already \"feature rich\", and it's finished too."
    author: "Eric Nicholson"
  - subject: "Re: Is Noatun really what kde needs?"
    date: 2001-04-24
    body: "Xmms may be feature rich, and ``finished''\nbut it's so seriously butt-ugly that I\nfor one cannot stand to have it running\non my desktop.  No amount of skinning can\ncure the fact that it puts *tiny* little\nbuttons on the *wrong place* on its ugly\nfakey title bar.\n \nWhatever Einstein came up with the idea of\ndoing away with title bars for audio players\nprobably also wears a nose ring.\n \nSo keep xmms if it rocks your world, but\nplease understand that for some people\nit's an abomination.  Give me noatun, with\nits clean, beautiful, simple, _excellent_\ninterface any day!"
    author: "George R. Welch"
  - subject: "Re: Is Noatun really what kde needs?"
    date: 2001-04-24
    body: "I'm using a theme for XMMS that make the buttons over 4 inches in circumfrence(there circular)(19 inch monitor).  XMMS themes can look like almost anything, STOP THE FUD."
    author: "AC"
  - subject: "Re: Is Noatun really what kde needs?"
    date: 2001-04-24
    body: "XMMS themes can't change the size of buttons.  The buttons are always the same size and in the same place (though they might appear to be bigger or smaller, the acutal clickable area never changes).  That's because its based on Winamp, which has the same limitation.  The titlebar buttons are _extremely_ small, especially at high resolutions.  You can make the window double-sized, but that makes it too big.\n\nThat said, I can't say I like Noatun's interface any bettter.  In fact, its worse (in KDE 2.1, but I'm sure its improving).  The GUI plugins waste too much space and don't provide enough options.  The playlist still needs some features like drag-n-drop-adding of directories, loading the ID3 tags and length of songs (and removing files that aren't acutally music/movies), icons for all the toolbar buttons, true shuffle play (I like to keep my mp3s organized but still hear them in random order), sorting, and other things.  I do like the global key shortcuts and the tray icon interface though (very nice! just fix the seeking and volume control from the keyboard).\n\nWinamp has the best interface, even though its titlebar buttons are small.  XMMS hasn't implemented all of winamp's features (like right-click menus on the buttons and one-click setting of all the equalizer sliders).  If winamp's titlebar buttons were bigger, and it didn't dump its playlist every time you opened a new file, it would be the perfect media player. (it even runs well under Linux with WINE!)\n\nOkay, I'm done nitpicking now :-)"
    author: "not me"
  - subject: "Re: Is Noatun really what kde needs?"
    date: 2001-04-24
    body: "What I'd like to see (to clarify my statements) in XMMS Is\n\n\tQt/KDE widget support\n\tA KDE extensible inerface (for your little \t\t\tbuttons)\n\nXMMS already has:\n\n\tStreaming audio (Ogg/Mp3/RM)\n\tAn aRts output plugin\n\tKJofol skin support (way better than noatun)\n\tVideo Plugs (Avi/DivX/MPEG)\n\tLarge user base (tried and true)\n\nIt's already full fledged, just make a version thats not dependent on GNOME libraries, and we can then stop reinventing the wheel, and maybe help the GNOME & X community along the way, assuming that's a good thing"
    author: "Eric Nicholson"
  - subject: "Re: Is Noatun really what kde needs?"
    date: 2001-04-24
    body: "\"and we can then stop reinventing the wheel, and maybe help the GNOME & X community along the way\"\n\nIt's obvious every time that someone makes a statement like this that they aren't a developer.  \n\nKDE and Gnome aren't written in the same language!\n\nC != C++\n\nYou can't just fit XMMS with Qt widgets and be done with it.  It's just not that easy."
    author: "Scott"
  - subject: "Re: Is Noatun really what kde needs?"
    date: 2001-04-24
    body: "\"It's obvious every time that someone makes a statement like this that they aren't a developer.\" \n\n     Don't be so quick to judge, Scott.  Although i'm not familiar with the source or XMMS, GNOME, or KDE/Qt,  but I don't think might statements are invalid.\n\n\"C != C++\"\n\n     true.  what could be said is that well written C is a proper subset, and anything which might not map 1:1 can easily be implemented in C++.  And I certainly hope your not going to say that GNOME has this amazing feature set that we couldn't mimic with KDE...\n\nA port of the xmms code should be feasible.  I'll start looking into it if people other than myself think it would be usefull.  It seems like a great system, and with a little (or maybe a lot of) work it could be a valuable asset.\n\nI really do apologize if I sound defensive or even agressive.  I love KDE, it's the most exciting software I've uncovered in years.  I really appreciate all the hardwork that's gone into Noatun/Kaiman/etc. aswell.  I really want to find a way to participate positively in this community and help make it a viable alternative to the closed, impenetrable mainstream desktop i've been using (weendoze not Gnome).  KDE, Linux, and all open software have one thing going for them, and that is the community behind it.  Much like distributed computing, the biggest issue is overcoming the inherit ineffeciency and redundancy of such a complex system."
    author: "Eric Nicholson"
  - subject: "Re: Is Noatun really what kde needs?"
    date: 2001-04-25
    body: "Sorry, this is just a rant that had been waiting to happen.  You hear the cry \"work together\" all of the time.\n\nMany of the people who proclaim this don't even know that KDE and Gnome are written in different languages.  Standing on the sidelines they yell \"work together\" at the people who are actually doing the work.  This is mostly directed towards them.\n\nI'm glad to see that you at least intend to implement what you would like to see.  This is why open source thrives.\n\nThe fact is that while a lot of C will compile with g++, given any large project, a lot of it won't.  Try compiling a Gnome app with it.  What's more, even what will compile in the end isn't Well Written C++ (tm).  KDE/Qt is very clean and goes to great lengths to use OOP features so that code is reuseable, modular and efficient.  You just don't get that in C.  \n\nThis is a lot of why I use KDE.  Well written code makes me happy."
    author: "Scott"
  - subject: "Re: Is Noatun really what kde needs?"
    date: 2001-04-24
    body: "> just fix the seeking and volume control from the keyboard).\n\nCan you elaborate a bit? Did you send a bugreport? I might fix it, if you tell me what's broken."
    author: "Carsten Pfeiffer"
  - subject: "Re: Is Noatun really what kde needs?"
    date: 2001-04-25
    body: "No, I didn't file a bug report.  Sorry!  I thought the problems were pretty obvious to anyone who used the plugin.  Seeking does technically work, it just sounds really bad and it doesn't go that much faster than just playing the song normally anyway.  It makes the keyboard pretty much useless for seeking.  Winamp (XMMS too?) skips 4 or 5 seconds of the song at a time, then plays a little, which sounds much better and allows you to seek faster.  I figured you'd get around to fixing it sooner or later, and it isn't a feature I use often, so I figured I wouldn't bother you with a bug report.\n\nThe volume control also doesn't change in large enough increments.  Usually the reason I want to use it is because a song has just come on that's really loud (you know how MP3s are all different volumes) and it isn't very fast.  If you do want the volume to change in small increments, you should take out the delay before the key starts repeating.  That way the volume control will seem faster and more responsive, even if you don't change anything else.\n\nI do like Noatun (more so now that I have aRts working correctly), it just needs a little more work to be as good as XMMS, which of course has been around for much longer.  Keep up the good work!"
    author: "not me"
  - subject: "Re: Is Noatun really what kde needs?"
    date: 2001-04-25
    body: "Now that I've tried seeking again, I notice that the first time you press the button, it does skip 3-4 seconds of music.  However, if you continue to press the button quickly or hold it down, it doesn't keep skipping 3-4 seconds of music each time.  Instead, it plays the same little bit of song over and over for a little bit, then skips to the next little bit of song.  Anyway, it sounds pretty bad, especially on loud songs.\n\nI'm sorry if it sounds like I don't like Noatun, I do!  Next time I'll submit a bug report.  In fact, I think I'll do that now about the mpeglib movie window."
    author: "not me"
  - subject: "Some XMMS problems, and why Qt/KDE is a good toolk"
    date: 2001-04-24
    body: "Qt as an underlying toolkit provides more than just a graphical interface, and KDE provides more than just a window manager.  I use XMMS now and then, but run into a major, show stopper (or should I say \"song stopper\") problem:\n\nAlthough every KDE app seems to be able to handle japanese text with no problem whatsoever, XMMS chokes and dies on any MP3 with a title/filename in anything but [A-Za-z] plus some basic punctuation.  Even simple accents on the letters (at least the accent on the e in Anime) seem to cause it to simply and silently skip over the song.\n\nNow, this is a fairly easy thing to fix, but the point is: Qt and KDE are extendable libraries, where one fix or one additional feature to the core code causes all applications to have the new feature or fix.  That's the advantage to KDE apps in my mind.  Look at the total network transparancy that exists in 99% of KDE apps without any effort on the part of their authors.  Now *that's* the way computers should work.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Binary Distribution of KDE"
    date: 2001-04-24
    body: "it feels weird why can't KDE release a generic binary KDE packages, like Adobes' acrobat, Netscape, StarOffice, Corel's wordperfect etc., when those companies can release their software in a generic way, irrespective of distribution, why can't WE???\n\nThis will benefit all Linux user's who love KDE more than other Unices :) \n\nlinux users of Redhat, Mandrake, SuSE, Caldera etc, will get latest builds on time. This can happen only when KDE installer comes in existence ;)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Binary Distribution of KDE"
    date: 2001-04-24
    body: "I'm not so sure as there are many difficulties with this approach. \nFirst, KDE is not just binaries, it comes with a boatload of configuration files, menustructures etc. that distributions modify quite heavily. It would be very painful to try to be compatible with each of them.\nSecond, distributions have different compiler and glibc versions in use, making them binary-incompatible with each other. This should be changed in a year or so, but it's still reality.\nThird, this blurs the domain of responsibility, as distributions are not quite sure, whether they should build their own binaries, or rely on KDE's builds. \nFourth, taking all of the above into consideration, this takes quite a lot of resources in the area where there are not too many volunteers, so it would probably have to be done by paid employees, and giving one company the brand of a \"Official KDE Company\" is bound to cause problems (See elsewhere for examples ;)"
    author: "nap"
  - subject: "Re: Binary Distribution of KDE"
    date: 2001-04-24
    body: "There was a big discussion of this issue when the story about KDE packaging policy (check it out, I think its still on the front page near the bottom).  Basically, a generic binary KDE would have to be bigger, slower, and take more memory - it would give a bad impression to people who were just trying KDE and it would annoy longtime KDE users.  The other programs you mention are much smaller than KDE (with the possible exception of StarOffice) so they can get away with increased memory usage and a little more slowness.  However, for KDE it would be bad, since KDE is already so big, and made up of many smaller programs.\n\nPlus, a generic KDE wouldn't integrate with your distribution - there would be no links to your distro's configuration utilities, and none of your distro's programs would appear in the KDE menus.  You would have to use the command line for all these things.\n\nBasically, the conclusion is that the distributions are supposed to provide packages which have none of these problems - and since they all do, there is no need for inferior generic KDE binaries.  Even if some distros' packages are a little late, its still better than installing generic binaries."
    author: "not me"
  - subject: "Here's why..."
    date: 2001-04-26
    body: "You know, I was about to re-hash that discussion you mentioned here, until I went to saw the reference to the KDE Installer.  \n\nhttp://www.rox0rs.net/kdeinstaller/\n\nApparently, someone is working on solving this issue already... so I guess we can chill and have a beer now.  ;-)\n\n--WorLord"
    author: "WorLord"
  - subject: "Kaboodle?"
    date: 2001-04-24
    body: "I wanted to check out kaboodle, but couldn't find it anywhere. I have the tagged CVS version (KDE_2_2_ALPHA_1) of all packages, but no trace of it anywhere. Where should it be, or is it not included anywhere?\n\nOtherwise everything works just fine! A big thanks to all involved."
    author: "Chakie"
  - subject: "Re: Kaboodle?"
    date: 2001-04-24
    body: "It is in 'kdenonbeta', and thus not part of the release. The announcement is incorrect."
    author: "Theo van Klaveren"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "I tryed KDE 2.0 , 2.0.1 , 2.1 , 2.1.1 (actual)\ngood!, the best desktop for linux,\nbut.. \n I hope that now keep in mind, \"STABILITY and SPEED\" for this newer version !"
    author: "samuele.c"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "what about divx???"
    author: "heytomet"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "eh...use aviplay? it even has a kde front end?"
    author: "AC"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "it would be great that it'was integrated into kdemultimedia libs and into kaiman !!!!!"
    author: "nico"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "You couldn't distribute the relevant codecs anyway as they are property of Microsoft."
    author: "nap"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-24
    body: "there's a noatun plugin at noatun.kde.org."
    author: "Evandro"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-05-02
    body: "the compilation of the plugin is too dificult.\nI had try it."
    author: "heytomet"
  - subject: "How coupled?"
    date: 2001-04-25
    body: "Would it be insane to think that I could get one app from the alpha to play with, without building the whole beast?  I've not yet had the privilege of building KDE from source, but I'm dying to check out how kmail's IMAP works, in hopes of finally pitching Netscape mail."
    author: "chchchain"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-25
    body: "Is KDE2.2 will include full support for arabic language.\nI knew that kde 2.1.1 support hebrew language and i think that hebrew language have same problem in support\ni mean right to left direction."
    author: "Ahmed  AlFaraj"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-25
    body: "I've installed KDE2.2alpha on Linux Mandrake , but I have some problems.\n\nI can't use Konqueror as a web browser, and there are 2 messages:\n\nkio_http: WARNING: found no definition PluralForm\n\n(in the consloe, where from I started KDE with startx)\n\nand\n\nThere was an error loading the module KHTML. The diagnostics is:\n\n(this is in a popup window; but there is not any diagnostics message).\n\nWhat is the problem? Can anyone help me? How can I solve this problem?\n\n(Animated/fade menus and combo boxes are great :) But I couldn't find Quartz Style)"
    author: "N\u00c9METH, Bal\u00e1zs"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-25
    body: "The could not load KHTML problem occurs because the packagers forgot to include libkhtml.la, a libtool library info file, in the kdelibs package. \nFortunately, it's easy to make one yourself.. Copy libkhtmlimage.la to libkhtml.la, open it up\nin a text editor, and change the .so files name from htmlimage to html whenever they occur..."
    author: "A Sad Person"
  - subject: "Re: KDE Project Releases KDE 2.2alpha1"
    date: 2001-04-26
    body: "hey, are there any plans in the works to develope an aplication installer/uninstaller that will installthe RPM and then run the /bin/setup script automaticly?\n\nand perhaps it could also compile and install tar.gz?\n\nthanks\n\nmodman"
    author: "ModMan"
---
For those of you who like the cutting edge or want to help the KDE developers continue their phenomenal development pace, but aren't yet ready to compile from CVS,
the <A HREF="http://www.kde.org/">KDE Project</A> has just released
KDE 2.2alpha1.  A list of some of the significant additions and improvements
versus the recent 2.1 release is below (please add any
unlisted significant changes in the comments section), as well as a list of currently available 
pre-compiled binaries (please read the <A HREF="/986933826/">KDE Binary
Packages Policy</A>).  Please bear in mind that this is an <EM>alpha</EM> release and those enjoying a stable desktop should use KDE 2.1.1.


<!--break-->
<P>&nbsp;</P>
<H3>ChangeLog:  KDE Alpha 2.2.1</H3>
<P>If you know of any significant changes not listed below, please add them
in a comment!</P>
<UL>
<LI><EM>kdelibs changes</EM>.</LI>
<UL>
<LI>One major change you will notice right away is from KDE's new modular printing subsystem based on the new KPrinter class (dot <A HREF="http://dot.kde.org/983389174/">story</A>; KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010406_5.html#6">5</A>, <A HREF="http://kt.zork.net/kde/kde20010331_4.html#2">4</A> and <A HREF="http://kt.zork.net/kde/kde20010310_1.html#1">1</A> ).  It is compatible with the existing QPrinter class and also features a new printer management tool.
 Its plugin design currently supports lpr, <A HREF="http://cups.sourceforge.net/">CUPS</A> and <A HREF="http://feynman.tam.uiuc.edu/pdq/">PDQ</A>.</LI>
<LI>Konqueror also benefits from the new HTML form completion added to KHTML, which allows Konqueror to supply possible completions to text fields in web forms via a drop down menu, very similar to how it does with URLs in the location bar (KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010316_2.html#2">2</A>).</LI>
<LI>Of interest to developers, KDE-DB, a modular database-connectivity class, has been added to kdelibs (KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010406_5.html#4">5</A>).  The <A HREF="http://www.mysql.com/">MySQL</A> plugin is the
most complete, but also the <A HREF="http://www.postgresql.org/index.html">PostgresSQL</A> one works.</LI>
<LI>KDE's font selection dialog has received a facelift:  it is now possible to
preview fonts in dialogs using the KFontAction class (KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010324_3.html#9">3</A>).</LI>
</UL>
<LI><EM>New applications</EM>.  Kaboodle, a quick multimedia viewer/player, has
been added to kdemultimedia (KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010406_5.html#5">5</A>).  It complements <A HREF="http://noatun.kde.org/">noatun</A> but is intended for use as a KPart (e.g., to preview a video in <A HREF="http://www.konqueror.org/">Konqueror</A>).  KScan, a library to access raster scanners using <A HREF="http://www.mostang.com/sane/">SANE</A>, as well as Kooka (dot
<A HREF="http://dot.kde.org/977380888/">story</A>), a front-end for KScan, have
been added to kdemultimedia (KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010316_2.html#9">2</A>).</LI>
<LI><EM>KMail</EM>.  The KMail developers have implemented the oft-requested IMAP support (KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010331_4.html#9">4</A>).  In addition, KMail features improved (non-blocking) sending (KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010406_5.html#11">5</A>).</LI>
<LI><EM>Theming</EM>.  Theming has gotten a big boost with support for both Windows themes from <A HREF="http://www.tucows.com/">Tucows</A> and pixmap-based <A
HREF="http://icewm.sourceforge.net/">IceWM</A> <A HREF="http://icewm.themes.org/">themes</A> and the addition of a Quartz Style (KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010331_4.html#10">4</A>).</LI>
<LI><EM>Networking</EM>.
<UL>
<LI><A HREF="http://www.caldera.com/">Caldera</A> has contributed a new smb kioslave which works with libsmbclient, released by the <A HREF="http://www.samba.org/samba/team.html">Samba team</A>.</LI>
<LI>KDE now supports SOCKS (a generic utility which re-implements a number of libc functions) (KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010324_3.html#5">3</A>).</LI>
<LI>KDE now supports IPv6 (tested on Linux, AIX, Tru64 4.x and Solaris 2.6 and 7) (KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010316_2.html#11">2</A>).</LI>
</UL>
<LI><EM>Desktop</EM>.  For the desktop, KDE 2.2 adds the new audiocd:/ ioslave,
which, for example, permits you to drag'n'drop audio CD tracks onto your desktop using Konqueror and have them automatically be encoded as .MP3 or Ogg Vorbis (dot <A HREF="http://dot.kde.org/984441100/">story</A>; KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010316_2.html#4">2</A>).</LI>
<LI><EM>Administrivia</EM>.  On the less ambitious side, Kant has been renamed as Kate (KCKDE Issue <A HREF="http://kt.zork.net/kde/kde20010406_5.html#7">5</A>).</LI>
</UL>
<P>&nbsp;</P>
<H3>Packages:  KDE Alpha 2.2.1</H3>
More packages may be released in the coming days (in particular Mandrake is said to still be working on their binaries), please read the <A HREF="/986933826/">KDE Binary Packages Policy</A>.
<UL>
<LI><A HREF="http://ftp.kde.org/unstable/2.2alpha1/distribution/tar/generic/">Source code</A></LI>
<LI>Tru64: <A HREF="http://ftp.kde.org/unstable/2.2alpha1/distribution/tar/Tru64/">4.0e, f, g or 5.x</A></LI>
<LI><A HREF="http://www.redhat.com/">RedHat</A>:  <A HREF="http://ftp.kde.org/unstable/2.2alpha1/distribution/rpm/RedHat/7.1/i386/">7.1 (i386)</A></LI>
<LI><A HREF="http://www.suse.com/">SuSE</A>:  <A HREF="http://ftp.kde.org/unstable/2.2alpha1/distribution/rpm/SuSE/i386/7.1/">7.1 (i386)</A></LI>
</UL>

