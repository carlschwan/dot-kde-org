---
title: "Cervisia 1.0 and 1.1 released"
date:    2001-02-12
authors:
  - "bgehrmann"
slug:    cervisia-10-and-11-released
comments:
  - subject: "Re: Cervisia 1.0 and 1.1 released"
    date: 2001-02-12
    body: "Will Cervisia be included in the KDE 2.1 distro?  I use cvs for the project Im on, but only use the CLI, Cervisia looks nice, hope it gets included :)"
    author: "Anonymous Coward"
  - subject: "Re: Cervisia 1.0 and 1.1 released"
    date: 2001-02-12
    body: "It's too late for 2.1, but you might well see it in KDE 2.2.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Cervisia 1.0 and 1.1 released"
    date: 2001-02-12
    body: "Eek.. One of the things that KDE needs to do is reduce the stuff in CVS and ship less apps as part of the various packages.. \n\nThis fits in nicely with Kdevelop and would be really nice if you could open a new project and use a cvs server to store it / open existing cvs tree.  This new app should be able to work in conjunction with kdevelop seemlessly but of course also remain standalone as it's a killer app on its own."
    author: "David"
  - subject: "Re: Cervisia 1.0 and 1.1 released"
    date: 2001-02-12
    body: "KDevelop 3.0 will be modularized so it would be no problem to replace the built-in cvs functions with acervisia module.\n\n--daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Cervisia 1.0 and 1.1 released"
    date: 2001-02-12
    body: "3.0 ? You guys are already thinking of 3.0 while 1.4 is merely out? Did I miss something? When is that supposed to happen?"
    author: "anonymous coward"
  - subject: "Re: Cervisia 1.0 and 1.1 released"
    date: 2001-02-12
    body: "According to the current roadmap it is planned to\nrelease KDevelop 2.0 (an improved 1.4) with KDE 2.2. \n\nKDevelop 3.0 will become a rewrite from scratch\n(this is what you get if you check out HEAD\nof the kdevelop module in the CVS). \n\nSo you basically missed nothing as KDE 3.0 is surely a long term issue and will be delayed as long as possible as it will break binary complatiblity (as the major number implies)\n\n--daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Cervisia 1.0 and 1.1 released"
    date: 2001-02-12
    body: "According to the current roadmap it is planned to\nrelease KDevelop 2.0 (an improved 1.4) with KDE 2.2. \n\nKDevelop 3.0 will become a rewrite from scratch\n(this is what you get if you check out HEAD\nof the kdevelop module in the CVS). \n\nSo you basically missed nothing as KDE 3.0 is surely a long term issue and will be delayed as long as possible as it will break binary compatibility (as the major number implies).\n\n--daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Cervisia 1.0 and 1.1 released"
    date: 2001-02-13
    body: "KEWL! this is good... for those who want to \"stay on the bleeding edge\" but who are essentially not-in-the-know :) Keep up! Esay tools for advanced things is A Major Factor(TM) when it comes to choosing what desktop (OS?) to use!!! <HTML><BR><BR></HTLM>/kidcat"
    author: "kidcat"
---
<a href="http://cervisia.sourceforge.net/">Cervisia</a>, an advanced graphical frontend for CVS, has finally been released as a 1.0 version. This release compiles either with KDE1 or KDE2 and contains complete documentation.   Also available now is the 1.1 release, which contains a <A href="http://sourceforge.net/project/shownotes.php?release_id=23289">pile</a> of new features and usability enhancements.  Read on for a few highlights.


<!--break-->
<p>
Cervisia features include:  
<ul>
<li>A file tree in which files are marked with     different colors depending on their status 
<li>A Bonsai-like blame-annotation view 
<li>A graphical revision tree 
<li>A graphical diff tool 
<li>A ChangeLog editor coupled with a dialog     for committing files 
<li>A dialog for resolving conflicts </ul>  
Screenshots, sources, documentation and a mailing list can be found on <a href="http://cervisia.sourceforge.net/index.html.new">Cervisia's homepage</a>.

