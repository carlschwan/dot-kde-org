---
title: "Slashdot: TheKompany's Shawn Gordon Responds In Full"
date:    2001-07-22
authors:
  - "numanee"
slug:    slashdot-thekompanys-shawn-gordon-responds-full
comments:
  - subject: "kamera"
    date: 2001-07-21
    body: "Anyone know if kamera was included in KDE 2.1 or 2.1.1?"
    author: "ac"
  - subject: "Re: kamera"
    date: 2001-07-22
    body: "Kamera appeared first in KDE 2.1.\n\nThe x.x.1/2 releases are mostly bug-fixes, and there's never any addition of big features like this. :)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Kivio"
    date: 2001-07-22
    body: "\"We are almost done with a new version that is written using Qt3 and is running on Linux and Windows and it shares the same stencils. What we are planning on is making a boxed set of this with all of our stencils for $99.95 with both Linux and Windows binaries.\"\n\nDoes it mean that KDE version in KDE cvs is no longer maintained by The Kompany?"
    author: "Hasso Tepper"
  - subject: "Re: Kivio"
    date: 2001-07-22
    body: "> Does it mean that KDE version in KDE cvs is no longer maintained by The Kompany?\n\nIt would appear that way, although Kivio originally started I believe on source forge as Queesio. TheKompany took it to completion and donated it to KDE. Shawn indicated the kivio model of a GPL program and selling add ons (stencils) was a trial. It doesn't seem it got tried for long. While it is possible to sell a GPL program it is more likely it would have been stripped of GPL'd code not by theKompany (parts re-written) and put under another license. Since you can't mingle GPL and and non GPL code it would seem rather illogical for them to contribute to the KDE version.\n\nIt would appear to indicate a fork in the code... even more odd is that it appears to be a fork with the same name, unless I'm missing something."
    author: "Eric Laffoon"
  - subject: "Re: Kivio"
    date: 2001-07-22
    body: "> While it is possible to sell a GPL program it is more likely it would have been stripped of GPL'd code not by theKompany (parts re-written) and put under another license.\n\nHow much effort does it take and what exactly does \"rewriting code\" mean so that it does not violate the copyright?\n\nI was under the impression that this is a difficult and time consuming task. I heard of \"clean rooms\" for reverse engeneering in order to avoid a simple copy of parts of the code.\n\nIf rewriting is so simple, what will prevent Microsoft from forking linux, KDE or whatever to a closed source version?\n\nI was hoping the GPL will forbid this and my code will live forever."
    author: "ac"
  - subject: "Re: Kivio"
    date: 2001-07-22
    body: "They don't have to rewrite almost anything. Copyright holder has right to relicence his code. Only parts not written by The Kompany or their emplyees have to be rewritten."
    author: "Hasso Tepper"
  - subject: "Re: Kivio"
    date: 2001-07-22
    body: "Yes, but what does rewrite mean?\n\nIs it a perl script which renames classes and methods sufficient, or does it have to be ...what?"
    author: "ac"
  - subject: "Re: Kivio"
    date: 2001-07-22
    body: "Dave Marotti is the original author of Queesio, which was a Qt based Visio type app, we worked with Dave, and continue to work with Dave to this day on the work on Kivio.  We jointly have the license, and in our qt based one we have jointly changed the license, and Dave continues to work on it with us.  We can change the license because it's our code and there isn't any other gpl code in it, we could dual license it if we wanted, or whatever else suited us.\n\nWe have decided how this dichotomy of having two Kivio's will play out, but they both can use the same stencil files, and the Qt Kivio application is also free, it's just not GPL."
    author: "Shawn Gordon"
  - subject: "Kivio and GPL"
    date: 2001-07-23
    body: "Shawn Gordon said :\n\n> We can change the license because it's our code and there isn't any other gpl code in it\n\nStrange... So a GPL code may become not GPL... For the one who wrote the code... I don't remember it is written in the GNU Public License...\n\n> We have decided how this dichotomy of having two Kivio's will play out\n\nIt is not clear while there is only one name. So I wish two names, for example Kivio K and Kivio Plus. And also who manages now the GPL version ?\n\nI feel that The Kompany has a change of strategy. The previous wore some equilibrism with GPL software. With some little explanations, it was easy to understand. \n\nI feel that the new equilibrism is less clear and I have difficulties to understand how the GPL and non-GPL code would be shared...\n\nI understand that you want to get an equilibrism a little similar to the Qt one (sure, it is a good thing to port Kivio, Kapital and other software on Windows and Mac...). But there is a big difference :\n- Qt was not GPL and it was possible to become a dual license GPL / not GPL\n- Kivio is GPL and GPL forbids it becomes not GPL, I think...\n\nNo ?"
    author: "Alain"
  - subject: "Re: Kivio and GPL"
    date: 2001-07-23
    body: "No.\nIt is perfectly legal for the copyright holder to change the license on something he has written. So  if Shawn and theKompany.com wrote or own the copyright on all the parts (by getting the authors to sign away copyright to them), then they are perfectly entitled to convert the license from GPL to whatever they want."
    author: "I"
  - subject: "Re: Kivio and GPL"
    date: 2001-07-23
    body: "> - Kivio is GPL and GPL forbids it becomes not GPL, I think...\n\nAs Shawn pointed out: they own the code of Kivio and can license it any way they want. Once a piece of code is released under GPL, that particular version remains under GPL and this license can never be revoked later on.\nHowever, if they make changes to that code, they can license that new version any way they want. \nIf *you* would change GPL'ed code of somebody else, you *have* to release this code again a GPL.\n\nTom"
    author: "Tom Verbeure"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-22
    body: "I'm excited about the fact that hes moving his products to Windows and Mac. This will only benefit KDE in the long run."
    author: "Craig"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-22
    body: "I am not quite sure I followoyu there Craig.\nHow will that be beneficial to KDE when some company ports their _QT_ based application to windows?\nI doubt they will port KDE right along so Kivio may use \"KDE\" extensions on Windows...but please enlighten me"
    author: "Darian Lanx"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds I"
    date: 2001-07-22
    body: "I think I opposite effect will benefit KDE:\n\nthe Kompany:\n\"Hey developers, you can use theKompany's applications in Windows/Mac/Linux.\" \n\nWindows Developer:\n\"Wow, neat, this QT stuff rocks, now that I know I can do this stuff in Windows, I can port my applications to Linux/Mac with relative ease. This is the perfect migration path, I've been wanting to get into Linux programming, this is neat. What did you say? An entire linux desktop built on QT? Cool, how do I get into that?\"  This could be a great way to get interested Windows programmers into the cross-platform world, and that's always a Good Thing.\n\nBack Ontopic:\nWindows apps will benefit KDE because if I learn to use QT apps in Windows, it makes the transition to other systems easier. \"Ever since I've been using Kapital for Windows, I've dumped Quicken, now I have no excuse to try Linux because I can use Kapital for KDE\". \n\nI think this will be difficult but doable, at least with enough critical mass to support theKompany, I wish them luck."
    author: "jorge o. castro"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds I"
    date: 2001-07-22
    body: "What makes you think Windows developers will know or notice that Kapital uses Qt?"
    author: "ac"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds I"
    date: 2001-07-23
    body: "If the users know that they can switch easily to Linux/Unwindows without having to convert all their files (e.g., From quicken to Kapital), then they're much more likely to switch to Unwindows.\n\nAnd 80% of those will use KDE likely."
    author: "Charles Samuels"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-22
    body: "It helps because it gives people an alternative to use that's available on Unix systems runing KDE as well.  So as OSes like Linux continue to become viable to the public at large, there's a greater chance that people will be able to use the applications that they're used to if they decide to switch."
    author: "Matt Perry"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-22
    body: "> Quanta Gold: This isn't on our web site yet, or available till about the end of summer, but the authors of the popular Quanta web design software came to us and wanted to work with us on making a multi-platform version of Quanta that theKompany would sell. There is a lack of high end tools available for PHP and web development and we are excited to be working with these members of the Quanta team.\n\nIn my conversations with Shawn I believe I made many of these points about the lack of high end development tools. I appreciate that he has worded this carefully... not ALL the memebers of the Quanta team are on board. Dmitry and Alex, the principle developers are. My role was primarily to specify much of what went into Quanta and to sponsor it's development.\n\nI wish my former associates success in all they do. I had intended to issue a carefully worded press release on this as soon as I could get an answer. After serveral weeks I have finally gotten a clear answer that this would indeed proceed. I have nothing against selling software and in fact believe that offering high end features to professional web developers makes sense. After all it is their livelyhood and they can afford it. What I strongly objected to were several key issues.\n\n1) Aborting GPL development. To my mind a \"free\" version is not the same thing.\n2) Walking away from an explicitly KDE centric application approach. I really like KDE.\n3) Freezing the feature set in the free program. Don't get me started.\n4) Offering features that are more applicable to new users and non professional site development retail. This is more bait and switch than free software to me and I believe there is a lot more to be made in true high end professional tools.\n\nUnfortunately any concerns I had regarding the over all vision of the project and my desire to take more than a day or two to research the offer were brushed aside.\n\nI appreciate all the Shawn has done for KDE and have long held him in high regard. However I feel taking an established project over to convert all further development to retail is not right, even if he did not initiate contact... he did set the terms.\n\nI will be posting a story to the dot in the next several days explaining the disposition of Quanta. With all due respect to Shawn and my former partners I intend to continue the GPL project. I am somewhat of a newbie C++ programmer but I've worked with computers and programming since TTL days (read late '70s). I plan on setting aside time to code but I have several businesses I'm just getting to positive cash flow right now and have to work on them... however they look to be ready to produce not only a nice income but any funding that might be needed for development. Since one of the businesses is a web development business looking to add web developers I am well prepared to invest in Quanta.\n\nI had previously not mentioned publicly that I was funding the development of Quanta. I had not wanted to hurt the pride of my friends. While I am eager to begin practicing the C++ coding I've been studying and I will be looking for volunteers I will also be ready to contract with the right developers. I would like the average person to enjoy high quality GPL'd web development software. I am willing to spend a lot more money than I have in the past to get the tools I want. \n\nI like knowing that theKompany develops retail software for KDE... I wish Dmitry and Alex well too, but I don't think the slated approach for Quanta is the way to go. I intend to see to it that what is freely available is much better than it is today. Obviously anything commercially available would need to be even better yet. I think that would be best for the rest of the people using KDE. Anyone who wants to be a part feel free to contact me. Anyone concerned about the future of Quanta, the uncertainty is now over. I only hope that you, the user, win.\n\nEric Laffoon\nsequitur@kde.org"
    author: "Eric Laffoon"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-22
    body: "Hmmm, bad.  I've heard the accusations about theKompany.com taking over and gutting free software projects before, but I've never heard it put so clearly or damning.\n\nAnyway, I'm sure we'll probably hear Shawn's side soon. :)\n\nAnd thanks for your contributions to KDE, Eric!  Quanta has amassed quite a fan base, although I still fiddle with the basic tools, myself.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-22
    body: "\"taking over and gutting free software\" \n\nNavin thats just trolling!! Shawns work will add much needed features to Quanta that I know i'll appreciate. If someone want so continue on a free version thats cool too. \n\nCraig"
    author: "Craig"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-22
    body: "Taking over and gutting?  give me a break Navin.  As even Eric said, these guys came to us, and they did it back in November, we are just now getting around to doing anything about it after repeated requests from the Quanta developers."
    author: "Shawn Gordon"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-22
    body: "I didn't really mean to say you *were* \"taking over and gutting\" anything, but I did say I had heard accusations that you had.\n\nAs for Quanta, I was going by the impression that the open source project was being shut down or that features were deliberately being removed or limited, in favour of a closed-source version.  \n\nSorry if I got it wrong in the moment. My bad.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-22
    body: "No wonder that Navindra and some others have bad feelings about this. There was already case with Magellan. Also, some guys are for sure disappointed that The Kompany is starting to drop native KDE support and will focus on multiplatvorm Qt apps. Nobody blames you, Shawn. Neither I. Most of people just doesn't have enough info whats going on. But they have their opinion."
    author: "Hasso Tepper"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-23
    body: "Just for the record so others don't jump on this with Kivio/Queesio.  Shawn did no such thing with Queesio/Kivio.  He has never made a change in license/code/concept/direction/anything with reguards to the program without contacting me first to see if I have any problems with it.  The few times I did have issues, I told him my feelings on the issues and he was very cooperative in figuring out a new approach.\n\nPersonally and professionally, Shawn has been great to work with as have the rest of the members of theKompany.  Queesio/Kivio would not have gotten this far had I still been working alone on it.  Working cooperatively with theKompany has brought alot of new functionality to a program which would probably still be pre 1.0.\n\n-dave marotti\n(author of the original queesio)"
    author: "Dave Marotti"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-22
    body: ">even if he did not initiate contact... he did set the terms.\n\nI think it's unfair to blame Shawn Gordon.\nUnfortunately I think that you have been naive about, how shall I put it, east-european ...hrmm...standards.\nAnyway, I wish everybody involved all the luck."
    author: "reihal"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-22
    body: "These guys need to pay the bills and theres nothing unethical about making money from there hardwork. I'm a little surprised by what looks little to be high brow elitistism. Some of us have families to feed and can't afford the idealism of the youth anymore. Unless your someone like Mr. Stallman thats making a living at preaching it. Its there work they should be able to do what they want with it. I don't think our Eastern friends will appreciate your regional chauvinism either.\n\nCraig"
    author: "Craig"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-23
    body: "Yes I agree with you. Maybe I meant the standard of living. \nI did mean that, didn't I?"
    author: "reihal"
  - subject: "Re: Slashdot: TheKompany's Shawn Gordon Responds In Full"
    date: 2001-07-24
    body: "It's interesting that TheKompany is going QT only. How will Microsoft's switching to C# and .Net affect the future of Trolltech and QT for Windows? Will QT on the Windows platform become obsolete?"
    author: "Sunil"
---
<a href="http://www.thekompany.com/">TheKompany.com</a> has contributed several apps to KDE such as <A href="http://www.thekompany.com/projects/kivio/">Kivio</a> and <a href="http://www.thekompany.com/projects/gphoto/">Kamera</a>. In this <a href="http://slashdot.org/article.pl?sid=01/07/20/1637220&mode=nocomment">Slashdot interview</a>,  Shawn Gordon gives us more insight and details on his company.  One of the things that has come out is that theKompany.com is moving products towards the Qt strategy, so that more platforms (including Windows and Mac) can be easily targeted. 

 Apparently theKompany.com is attempting to build infrastructure that will enable some level of KDE compatibility for Qt applications.  

I wonder what this means for Kivio in KDE CVS -- seems like there's already an implicit fork present here.

Another, much older, interview is also <a href="http://gui-lords.org/reviews.php?op=showcontent&id=1">available at GUI-Lords.org</a>.
<!--break-->
