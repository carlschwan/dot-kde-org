---
title: "Benjamin Meyer: A Tribute to KDE"
date:    2001-06-02
authors:
  - "nstevens"
slug:    benjamin-meyer-tribute-kde
comments:
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "I think this text needs more than two paragraphs :)"
    author: "will"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "Hehe see my previus two articals.  Didn't think it would go on the dot..."
    author: "Benjamin C Meyer"
  - subject: "The value of large modules"
    date: 2001-06-02
    body: "There's a perpetual argument as to whether KDE should break up its large modules into smaller pieces. Both sides usually focus on convenience: a smaller number of modules makes downloading, compiling and installation much easier while finer-grained packaging would enable a user to grab the new konqueror or kmail without having to download twenty other apps he doesn't want. There's no right answer, it's a tradeoff.\n\nThis piece points up what I think is an underappreciated advantage of large modules. It creates a situation where every developer not only has a copy of the source for every core app, but also a feeling of responsibility for and involvement with the entire project. That's a large part of why KDE feels so integrated and consistent, instead of coming across as a package of otherwise unrelated applications using the same toolkit.\n\nBTW, I agree that Benjamin needs to hit the Return key a bit more often...    ;-)"
    author: "Otter"
  - subject: "Re: The value of large modules"
    date: 2001-06-02
    body: "about the larger or smaller modules:\n\nsome further toughts about the upper mentioned advantages and disadvantages of large modules.\n\nkinstaller integrated with kconfigure and extended with some \"red-carpet\"-like apps.kde.com/ftp.suse.com/whatever.sourceforge.net features might allow to have smaller packages from the upper mentioned sources.\n\nadditional to this the large modules might stay the gooddy to motivate the k-developer to klean up the own project.\n\ni don't want to step to the level of knowing the formats of make and configure files but still want to try out cutting edge (not yet packaged) alpha applications.\n\nkconfigure helps the upper behaviour very much. kinstaller might help finally even cluelesser users. the kde-project might end up with a lot more bug reports (is that wanted anyway?)\n\nany comments?\n\n\ni agree about the return key :-)"
    author: "maxwest"
  - subject: "Re: The value of large modules"
    date: 2001-06-03
    body: "This is a very, very debatable point.\n\nFor instance, I recently did an update to KDE2, and now I've lost kmail.  There's no way I can reinstall it or compile it myself.  As such I have to use mozilla (s-l-o-w!) for mail.\n\nIt's too bad, because I really can't stand moz as it now stands.  It makes OS X actually seem fast, which is why I'm using it at this point."
    author: "Tony A. Emond"
  - subject: "Re: The value of large modules"
    date: 2001-06-04
    body: "Kmail is in KDE2. Of course you can reinstall or compile it yourself."
    author: "Erik"
  - subject: "Re: The value of large modules"
    date: 2001-06-04
    body: ">This is a very, very debatable point.\n\nWell, yes. That's what I meant by \"There's no right answer, it's a tradeoff.\"\n\n>For instance, I recently did an update to KDE2, and now I've lost kmail. \n>There's no way I can reinstall it or compile it myself.\n\nSay what? It's gone now or just broken? Why is there no way to reinstall it or compile it?"
    author: "Otter"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "Very cool article.\n\ncraig"
    author: "Craig"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "Another reason that KDE works so well (and is stable) is that it (almost totally) uses C++ rather than C. \n\n I've often thought that it's no coincidence that two projects which are having problems with stability (Mozilla and Gnome) are both C-based. Yes, its possible to create an unstable app in C++ as well as C , and there are examples of unstable C++ apps out there too.  However, it's hard to go past the advantages of templates etc that C++ provides.  Trying to imitate this behaviour in C is a bit like trying to bolt wings onto a car ..... :-)"
    author: "Fred123"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "Benjamin, all your peices seem to be very ojective and easy to read, interesting articles, thank you.\n\nI'd also like to ask something else: you learnt C++, QT and now KDE programming, how? At one point in time you were just a windows user, what was your road map to get from that point to writting a sucessful KDE aps?\n\nYes, I am seriously looking in to coding under Linux in C++ but I need a few pointers, I have only coded some TCL in the past and have little experience although I like to think I'm fairly technically minded.\n\nThere must be simple stuff on the web like \"hello world\" aimed at developing under Linux with GCC. Would using an IDE like KDevelop be a good move or console apps first? Pointers, links to Linux geared tutorials anyone? Recommened books for total beginners?"
    author: "Brian"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "I'm a relative C++ newbie and have found the book \"C++ from the ground up\" (by Herbert Schildt - ISBN 0-07-882405-2) to be very good. Gives a nice intro to the STL , too. \n  Also - \"KDE 2.0 Development\" (by David Sweet - ISBN 0-672-31891-1) is very good. I found this to be a great intro to both the Qt toolkit and KDE - understandable even to newbies like myself ...:-)"
    author: "Sagittarius"
  - subject: "Stick with Addison-Wesley"
    date: 2001-06-02
    body: "First of all, be wary of books by Herb Schildt.  I've heard only bad things about them.  I haven't read them myself, but I haven't read a good review of a Schildt book yet.  Here are some examples:\n\n<a>http://www.accu.org/cgi-bin/accu/rvout.cgi?from=0au_s&file=cp001508a</a>\n\nNow, on the constructive side, my advice is: stick with Addison-Wesley.  C++ is a big, complex language, and few are really qualified to teach it.  But it seems like the C++ \"inner circle\" all write for AW - Stroustrup, Lippmann, Koenig, Moo, Meyers.  Personally, I read Stanley Lippmann's \"C++ Primer\" cover-to-cover and did all the exercises on SuSE Linux using gcc and XEmacs.  Then I read Stroustrup, the Meyers books, and the C++ FAQs book (which really does add significant value compared to the online FAQ).\n\nHere's AW's new C++ series:\n\n<a>http://cseng.aw.com/catalog/series/0,3841,43,00.html</a>\n\nHere's a really good book to learn general Linux, including the programming environment:\n\n<a>http://www.sobell.com/LINUX/linux.html</a>\n\nHave fun with C++.  Learning new languages should expand your mind, and learning C++ expanded mine like no other.  Some of it may be hard, but stick with it; you'll be glad you did.\n\nFunny.  I invested so much in learning C++, and now I write Java for a living.  But I'm learning Qt and KDE in my \"spare\" time and can't wait to start contibuting to KDE.  See ya in the CVS logs!\n\nDein\nChris"
    author: "Chris"
  - subject: "Re: Stick with Addison-Wesley"
    date: 2001-06-02
    body: "I concur. Herb Schildt is notoriously bad, and Addison-Wesley series are a sure bet. Get Lippman's \"C++ Primer\", or, for a lighter start \"Essential C++\" :\n\nhttp://cseng.aw.com/book/0,3828,0201485184,00.html"
    author: "Guillaume Laurent"
  - subject: "Re: Stick with Addison-Wesley"
    date: 2001-08-20
    body: "For what it's worth, I like Herbert Schildt.  I remember studying Teach Yourself C along time ago, and it was (and is) a classic book.  \n\nTeach Yourself C++ was more challenging, but then, C++ is much more challenging, of course!  And, now that I have gone through Teach Yourself C++ a couple of times, I see that it too is a true classic.  \n\nNow I'm going to get the Windows 2000 from the Ground Up by Schildt, and it looks really great.  Addison-Wesley is a very good publishing house too; but, comparing author to publishers is like comparing apples and oranges!"
    author: "Patrick Kennedy"
  - subject: "Re: Stick with Addison-Wesley"
    date: 2001-08-20
    body: "Herb Schildt is reknown for writing very bad books filled with errors :\n\nhttp://www.accu.org/bookreviews/public/reviews/0au/s.htm\n\nAnd about what you call a \"true classic\" :\n\nhttp://www.accu.org/cgi-bin/accu/rvout.cgi?from=0au_s&file=t001453a"
    author: "Guillaume Laurent"
  - subject: "Re: Stick with Addison-Wesley"
    date: 2001-08-21
    body: "Herb Schildt is reknown for writing very bad books filled with errors :\n\nhttp://www.accu.org/bookreviews/public/reviews/0au/s.htm\n\nAnd about what you call a \"true classic\" :\n\nhttp://www.accu.org/cgi-bin/accu/rvout.cgi?from=0au_s&file=t001453a"
    author: "Guillaume Laurent"
  - subject: "Re: Stick with Addison-Wesley"
    date: 2001-06-02
    body: "Have a look at Thinking in C++ from Bruce Eckel\n(ISBN 0-13-979809-9).  It's downloadable from\nhttp://www.bruceckel.com"
    author: "Richard Bos"
  - subject: "Re: Stick with Addison-Wesley"
    date: 2001-06-03
    body: "The correct link is http://www.bruceeckel.com"
    author: "Anonymous"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "Yes, the easiest is to install KDevelop and start right off with creating a *Mini-KDE* app described in the tutorial manual included to work yourself through the 14 tutorial steps of the Qt docs. Show up on irc.kde.org on #kde if you?re running into problems :) That?s also the place where you can get some ideas to fix simple things on KDE by which you can learn C++ along doing it, it?s really easy once it gets ya.\n\nCheers,\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "Guys, thank you for the pointers. While running for a train today, instead of buying a magazine I bought my first c++ book, teach yourself C++ in 21 days. OK, I know, I always avoid books with titles like that also but it was the only thing they had. It starts with the real basics and I'm more than a little surprised to see how much overlap there is with other lanugages, the syntax is different etc but it's not a million miles away from perl or TCL, the design aspects, general strucutres seem to have far more in common that I would have thought. C++ is compiled of course and I'm used to runtime stuff but I had a real feeling that C++ was a million miles away, some kind of god like language that was far beyond my skills, that feeling has gone (however I'm on only day 4 of the book). I doubt I'll ever do coding for a living or be the next Mr. Faure / Mr. Bastian but given a bit of time there could be a few bug fixes here and there.  \n\nOne question though, is Kdevelop + QT examples / tutorials a good place for a beginner to C++, would it not be better to learn the console side first before diving in to KDE and QT? So for the ignorant question I ask because it made more sense to learn TCL and then add TK knowledge afterwards."
    author: "Brian"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-05
    body: "The best way to learn C++ (or any language for that matter) is to write a real application. Somehow, writing those \"toy\" programs doesn't help. With a real application you have real motivation. You ask yourself real questions and face real problems. You *know* where and when to use polymorphism (as an example) because you've seen where it works and doesn't work. You learn to use asserts and check for proper values, even though no example program in any how-to book does so. Etc.\n\nI would recommend learning the basics with a console application. Then progress on to straight Qt programming. And after you know what's going on, then move to KDE programming.\n\nAnd don't stop learning! No one C++ book will teach you everything you need to know. Get Meyers \"Effective C++\". Don't be content with the Qt collection classes, and learn the STL. Learn the common Design Patterns.\n\nI might be arguing against the crowd here, but I also suggest ignoring the example KDevelop and Qt programs at the *start*. The Qt samples are meant to illustrate one particular feature, but are not always good examples of program design. And the problem of the KDevelop templates is that you never learn how to start your own program from scratch."
    author: "David Johnson"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "To learn C++, start with small console programs. If you want to write KDE or Qt programs, KDevelop or KDE Studio are definitely a good choice. \n\nHave a look at STL, but don't become frustrated. It's all but intuitive. The more you will appreciate the nice Qt API ;-)"
    author: "Alexander Kuit"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "Mozilla is C++-based."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "That's rediculous. The language does not affect the stability.\nI've been using GNOME for a long time now, and it's absolutely stable.\n\nI've been doing GTK+/GNOME programming for a year.\nI also have experience on \"build-in-OOP\" languages such as Delphi.\nGTK+ is written in C, but it is OOP.\nMany C++ people consider that as a hack, but I don't.\nWhy would it be a hack? It works on all platforms, and it's stable.\nOther than the amount of code you have to write, doing C++ OOP is not any more efficient than GTK+ OOP.\nGTK+ OOP supports RTTI too. And soon it will also support Java-like interfaces.\n\nBesides, Mozilla is C++, not C.\nIt only uses GDK as an Xlib wrapper, that's all."
    author: "ac"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "> The language does not affect the stability.\n\nWhat's the color of the sky on your planet ?\n\n> Many C++ people consider that as a hack, but I don't.\n\nAnd you're right. But it's still not as efficient as an OO language.\n\n> Other than the amount of code you have to write, doing C++ OOP is not any more efficient than GTK+ OOP.\n\n*Precisely*. The \"amount of code\" is the key here. It's more than just sheer number or lines, it's that you also have to get them right. Hint : programmers are human.\n\nActually, there's a bit more than that. GTK+ OO framework also has a much larger overhead than C++.\n\nRead the quote from Stroustrup on\n\nhttp://gtkmm.sourceforge.net/philosophy.html\n\nI put it there myself back when I was still helping maintain Gtk--. It sums it up perfectly."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "> What's the color of the sky on your planet ?\n\nWhite.\n\nHello World in C:\n#include <stdio.h>\nint main()\n{\n    printf (\"Hello World!\\n\");\n    return 0;\n}\n\nHello World in C++:\n#include <iostream.h>\nint main()\n{\n    std::cout << \"Hello World!\\n\";\n    return 0;\n}\n\n\nDo you see difference in stability? I don't.\n\n\n> And you're right. But it's still not as efficient as an OO language.\n\nIt is less efficient when coding.\nBut the final result will be just as efficient as any code \"real\" OOP languages produce.\n\n\n> Precisely*. The \"amount of code\" is the key here.\n> It's more than just sheer number or lines, it's that\n> you also have to get them right. Hint : programmers are human.\n\nI don't mind writing a little more code.\nIn fact: writing small programs in GTK+ needs less code than QT.\n\nDoing \"normal\" things doesn't take much time.\nCreating GTK+ objects takes more time, but Gob can make it as simple as C++.\n\n\n> GTK+ OO framework also has a much larger overhead than C++.\n\nWhen you wrap things up in other languages, yes.\nBut who cares about those minor details?\nIf I write a program in GTK+ and Gtk-- I won't notice any different in speed on my old Pentium 166 MMX."
    author: "ac"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "> Do you see difference in stability? I don't.\n\nYou do know that people write programs slightly more complex than \"Hello World\", right ?\n\n> It is less efficient when coding.\n\nWhich is in itself a huge problem.\n\n> But the final result will be just as efficient as any code \"real\" OOP languages produce.\n\nFalse. An OO language can better represent concepts it knows about. Even more with C++ which has a very efficient OO model.\n\n> I don't mind writing a little more code.\n\nYes you do, just like everybody. You just don't realize it.\n\n> In fact: writing small programs in GTK+ needs less code than QT.\n\nAs I said, people write programs more complex than \"Hello World\".\n\n> Doing \"normal\" things doesn't take much time.\n> Creating GTK+ objects takes more time, but Gob can make it as simple as C++.\n\nAgain, you hit the nail right on : creating a class in C++ *is* a normal thing, and it is simple. With GTK+, it's a hard thing to do, so much than people felt the need for a preprocessor to help doing it. And though it does help, it's still far from being as simple as in any OO langage.\n\n> > GTK+ OO framework also has a much larger overhead than C++.\n\n> When you wrap things up in other languages, yes.\n\nNo. The GTK+ OO framework has inherently a much larger overhead than C++.\n\n> But who cares about those minor details?\n\nEverybody does. What you call \"minor details\" are actually essential. That's the old \"OO is syntactic sugar\" clich\u00e9. Some people buy it because they see OO as an entirely different paradigm than good ol' procedural programming, while in fact it's an evolution of proc. programming. It's the next abstraction step. And just like procedural programming, it has to be implemented at the language level to be really useable.\n\nYou could program without for(), while() and functions, using just if() and goto. Would you do it ? Would you claim than doing it is \"as efficient\" ? Would you accept needing something like GOB to write a for() loop ? Wouldn't you \"mind writing a little more code\" ?"
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-02
    body: "seems like a matter of taste to me :)"
    author: "ik"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "No, a matter of skills.\n\nIf you can't code faster in C++ than in C, you're doing something wrong. If you can't code faster in Java than in C++, you're doing something wrong.\n\nSome languages are simply better than others, because they're built upon the experience gained from previously existing ones. The \"it's all a matter of taste, use what you like\" mantra is bullshit."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "Aha, and just because C++ has some advantages over C, then I must use C++ even though I don't want to?\n\nLinux is written in C. Yet it is very stable."
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "> Aha, and just because C++ has some advantages over C, then I must use C++ even though I don't want to?\n\nNo. If you enjoy C, fine. It's just that someone working on the same task in C++ will probably go faster.\n\n> Linux is written in C. Yet it is very stable.\n\nYou can make stable code in any language. It just takes more effort in some than in others."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "well, then explain me why kde ain't written in java, if its a matter of skills.\n\nmaybe 'a matter of taste' is not totally correct either, but choosing the right language for the right thing to do is, whatever that language may be."
    author: "ik"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "> well, then explain me why kde ain't written in java, if its a matter of skills.\n\nBecause even now Java isn't a viable solution on the desktop (in particular on Unix). There is of course more than just the language, but also what implementations of it are available on your platform. No matter how good the language is in theory, if the compiler sucks you won't get far."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "You can pretty much say that g++ sucks (although it is good enough for most tasks).\nIt doesn't fully support the C++ standard."
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "> You can pretty much say that g++ sucks\n\nIn some regards it does, yes. :-) But it's improving, at that's the only free compiler we have.\n\n>(although it is good enough for most tasks).\n\nYes.\n\n> It doesn't fully support the C++ standard.\n\nNo compiler does, at this moment.\ng++ is among the best in that regard, though."
    author: "Guillaume Laurent"
  - subject: "Re: Java isn't a viable solution on the desktop?"
    date: 2001-06-03
    body: "KDE doesn't need to be written in Java to make it a killer Java development platform - it only needs a Java api. But KDE has a Java api in the kdebindings/qtjava and kdebindings/kdejava directories.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "> Which is in itself a huge problem.\n\nDelphi doesn't support class operators and multiple inheritance and other things C++ does.\nDoes that make Delphi a huge problem?\n\n\n> False. An OO language can better represent concepts\n> it knows about. Even more with C++ which has a very\n> efficient OO model.\n\nAn OO language provides abstraction for OO code.\nBut how's the final, lower-level result any better than OO in C?\n\nLet's say you write a class in C++.\nThen I disassemble that program and see what the asm instructions are.\nThen I write those same instructions in asm.\nAnd voila: the final result is the same without using an OO language.\n(this is only an example; nobody would do such crazy thing anyway)\n\nGTK+ OO is similar. It might look like a hack, but if you look at a lower level, it really isn't.\n\n\n> Yes you do, just like everybody. You just don't realize it.\n\nI said a *LITTLE* more code, not hundreds of lines.\nIf it was really that much then I wouldn't use GTK+.\n\n\n> As I said, people write programs more complex than \"Hello World\".\n\nAnd other people write programs less complex than Konqueror.\n\n\n> No. The GTK+ OO framework has inherently a much larger overhead than C++.\n\nWhere's the proof?\n\n\n> Everybody does. What you call \"minor details\" are\n> actually essential. That's the old \"OO is syntactic sugar\" clich\u00e9\n\nAnd why would they if the code is only 0.000000001 ms slower on a Pentium 166?"
    author: "ac"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "> Does that make Delphi a huge problem?\n\nIf you need to use multiple inheritance, then yes, it will become a problem. At least Delphi supports OO natively, not through syntactic conventions.\n\n> But how's the final, lower-level result any better than OO in C?\n\nAgain : because an OO compiler knows more about what you're trying to do. Do you think that if you manually replace all your for() and while() by if() and goto, you're going to get the exact same lower-result ?\n\nOn one hand you have a compiler which understands what things like a class or a virtual function are and can represent those directly in assembly. On the other you have a compiler which only knows structs and plain functions.\n\n> And voila: the final result is the same without using an OO language.\n> (this is only an example; nobody would do such crazy thing anyway)\n\nYou really have a gift for saying yourself exactly what I mean, but without understanding it. \n\nYes, nobody would do such a crazy thing, because it's incredibly hard to code all these abstractions yourself. In C it's a little bit easier. In an OO language it's even easier. That's the whole point, and why C++ is more efficient than OO in C. It's just plain *easier*.\n\nYou seem to misunderstand what I mean by \"more efficient\" : I mean more efficient for the programmer. You write less code, make less mistakes, and the code is more readable.\n\nIt's also true that the resulting binary code will be more efficient, for the reasons I state above, but the key here is programmer's efficiency. That's why KDE, as a programming platform, is light years above Gnome, and why KDE development goes so fast.\n\n> I said a *LITTLE* more code, not hundreds of lines.\n\nIt *is* hundreds of lines. When converting a GTK+ examples to Gtk--, on average we'd get between 2 to 3 times as less lines of code. That means a 1000 lines GTK+ program would shrink to 300-500 lines in Gtk--.\n\n> And other people write programs less complex than Konqueror.\n\nThe typical desktop application isn't much simpler than konqueror.\n\n> Where's the proof?\n\nI leave it to you as an exercise to compare the sizes of GtkObject and GtkObjectClass to the size of an empty C++ class. :-)\n\n> And why would they if the code is only 0.000000001 ms slower on a Pentium 166?\n\nAt this point I assume you've understood that I'm essentially talking about efficiency when writing code. Then again, with Gtk-- you'll also get several MB of mem overhead."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "> If you need to use multiple inheritance, then yes,\n> it will become a problem.\n\nEver heard if interfaces? Delphi supports them.\nGTK+ 2.0 as well.\n\nBut multiple inheritance is considered by many people as a flaw.\nI never use that feature anyway.\n\n\n> Again : because an OO compiler knows more about what\n> you're trying to do. Do you think that if you manually\n> replace all your for() and while() by if() and goto,\n> you're going to get the exact same lower-result ?\n\nI don't know whether it can be done in C, but if it can, and it can be wrapped up in a function, then I believe it would be just as efficient as a \"native\" one.\n\n\n> I leave it to you as an exercise to compare the sizes\n> of GtkObject and GtkObjectClass to the size of an empty C++ class. :-)\n\nHow?\n\n\n> Then again, with Gtk-- you'll also get several MB of mem overhead.\n\nWhich most of it is shared memory anyway.\n\n\n\nTrue, GTK+ emulates OOP. And it requires more code.\nBut it is not so inefficient that it makes GTK+ unuseable.\nMany things are wrapped up nicely in macros and functions.\n\nBesides, the GTK+ developers aren't crazy.\nIf GTK+ OOP is really that bad, then the Gimp/GTK+ developers would have created a C++ toolkit instead of a C one."
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "> Ever heard if interfaces? Delphi supports them.\n\nGood for them.\n\n> GTK+ 2.0 as well.\n\nNot \"as well\" I'm afraid. It's again a set of syntactic conventions and not a real language feature.\n\n> But multiple inheritance is considered by many people as a flaw.\n\nSo is every single C++ feature, except no two people can agree on which features are flaws and which aren't. :-)\n\n> I never use that feature anyway.\n\nHow could you, unless you use an OO language supporting it ?\n\n> I don't know whether it can be done in C, but if it can, and it can be wrapped up in a function\n\nDo you have any experience in programming, even in C ? Or do you totally misunderstand what I mean ?\n\nint i;\nfor(i = 0; i < 10; ++i) {\n/* do something */\n}\n\n====>\n\nint i = 0;\nbegin_loop:\nif(i < 10) {\n++i;\n/* do something */\ngoto begin_loop;\n}\n\n\nIf you think this looks ugly, error prone and unmaintainable, then you have an idea of how OO in C looks to any C++/Java/Python/... programmer.\n\n> How?\n\nEver used the sizeof() operator ?\n\n> Which most of it is shared memory anyway.\n\nThat's what we kept answering to our critics. Nevertheless, when the Eazel guys showed their very first prototype for Nautilus, using Gtk--, the beast was (according to Havoc) more than 30Mb large. That's one of the reasons why they were kindly invited to use C instead (although this was just before a vast improvement of mem usage in Gtk-- by Karl Nelson).\n\n> But it is not so inefficient that it makes GTK+ unuseable.\n\nCertainly not, nobody claims so. GTK+ is probably the best C toolkit around, way easier to use than Motif. But it is not and will never be as easy to use and powerful as Qt, and OO in C cannot be \"just as good\" as a real OO language.\n\n> If GTK+ OOP is really that bad, then the Gimp/GTK+ developers would have created a C++ toolkit instead of a C one.\n\nWhen the original developers of GTK+ started working on it, g++ was still at 2.7, e.g. a big pile of crap of a C++ compiler. And AFAIK none of them knew C++ anyway."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "> How could you, unless you use an OO language supporting it ?\n\nWhy would I need multiple inheritence if I just want a subclass?\n\n\n> If you think this looks ugly, error prone and\n> unmaintainable, then you have an idea of how OO\n> in C looks to any C++/Java/Python/... programmer.\n\nI disagree. GTK+ code still looks far better than that loop emulation.\n\n\n> Ever used the sizeof() operator ?\n\nYes, but it doesn't work correctly.\n\n\n> way easier to use than Motif.\n\nWhile Motif supporters/trolls always claims the opposite. ;-)"
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "> Why would I need multiple inheritence if I just want a subclass?\n\nWhy would you want more than a subclass if you don't have multiple inheritance ? The availability of a tool has a lot of influence on whether you feel like using it or not :-).\n\n> I disagree. GTK+ code still looks far better than that loop emulation.\n\nTo you may be. Ask a C++ or Java programmer :-).\n\n> Yes, but it doesn't work correctly.\n\n#include <gtk/gtkobject.h>\n\nint main()\n{\n    printf(\"sizeof gtkobject : %d\\nsizeof gtkobjectclass : %d\\n\",\n           sizeof(GtkObject), sizeof(GtkObjectClass));\n\n    return 0;\n}\n\nsizeof gtkobject : 16\nsizeof gtkobjectclass : 40\n\n> While Motif supporters/trolls always claims the opposite. ;-)\n\nI've seen only one so far, Andy Fountain, the guy who wrote the Motif books for O'Reilly. His interview was recently republished on /. even though it already was a year ago."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "sizeof gtkobject : 16\nsizeof gtkobjectclass : 40\n\nthats probably because of metadata overhead.\nanyway, you don't have a point there i think (for the rest, the discussion is interesting)\n\n#include <qt/qcombobox.h>\n#include <stdio.h>\nvoid main() {\n \nprintf(\"%i\\n\",sizeof(QObject));\n}\n\n36"
    author: "ik"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "oops, ignore this one (again), i didnot understand what you were trying to say."
    author: "ik"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "> Why would you want more than a subclass if you\n> don't have multiple inheritance ?\n\n???????????????\n\n\n> To you may be. Ask a C++ or Java programmer :-).\n\nBack in the days when I was still doing Winblow$ programming in Delphi, any C++ code looks far more messy and unmaintaiable to me.\nIt depends on one's experience.\nOnce you get used to GTK+, it won't look messy anymore.\n\n\n> sizeof(GtkObject), sizeof(GtkObjectClass));\n\no_0\nNever thought of that.\n\n\n> I've seen only one so far, Andy Fountain, the guy\n> who wrote the Motif books for O'Reilly.\n\nRead all the comments at Slashdot and you'll find milions of trolls, as usual."
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "> ???????????????\n\nWhat I mean is that if a feature is unavailable or very hard to implement, chances are that you won't think about using it.\n\n> Back in the days when I was still doing Winblow$ programming in Delphi, any C++ code looks far more messy and unmaintaiable to me.\n\nAnd OO in C doesn't look messy to you compared to Delphi ?\n\n> Once you get used to GTK+, it won't look messy anymore.\n\nOf course you can get used to it. You probably can get used to OO in assembly as well. But what's the point, when you have a simpler tool at hand ? Why use a less efficient and more complicated way of doing things ?"
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-04
    body: "> And OO in C doesn't look messy to you compared to Delphi ?\n\nNot anymore. It looks readable and maintainable to me.\nIt took me about a few weeks to get used to it.\nBut that applies to Delphi OO and C++ OO too.\n\n> But what's the point, when you have a simpler tool at hand ?\n\nYou're right, and that's why I use GOB for my GTK+ objects. :)\n\n\n> Why use a less efficient and more complicated way of doing things ?\n\nBecause I like C and GTK+."
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-04
    body: "> It took me about a few weeks to get used to it.\n\nMultiply that by the number of people involved in Gnome and you start to have an idea of why it's still catching up.\n\n> You're right, and that's why I use GOB for my GTK+ objects. :)\n\nSo you had to learn yet another tool just to do something which is trivial in an OO language, and this is why an OO language is more efficient than OO in C. Haven't we been through this already ?\n\n> Because I like C and GTK+.\n\nNo problem with that. The debated question is not what you like, but what is more efficient. You claim that OO in C is every bit as good as an OO language. You are wrong. That's all."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "> Multiply that by the number of people involved in\n> Gnome and you start to have an idea of why it's still catching up\n\nThat's not a valid argument.\nWhen I started doing C programming, I immediately started using GTK+.\nSo I had no C experience at all, yet it took me only a few weeks to learn it.\nA lot of other developers have C experience and thus can learn much faster.\n\n\n> So you had to learn yet another tool just to do something\n> which is trivial in an OO language, and this is why\n> an OO language is more efficient than OO in C.\n> Haven't we been through this already ?\n\nGOB is not difficult to learn. In fact, it took me less than half an hour.\n\n\n> You claim that OO in C is every bit as good as\n> an OO language. You are wrong. That's all.\n\nNo, I claimed that the final compiled code (the executeable) is just as efficient as a C++ one."
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "> A lot of other developers have C experience and thus can learn much faster.\n\nStill not as fast as Qt for a C++ programmer. And that's just a one-time handicap. The big difference comes in the long run, when actually using them.\n\n> GOB is not difficult to learn. In fact, it took me less than half an hour.\n\nAfter you had learn GTK+ itself, right ? Honestly, how many classes are defined in your current project ?\n\n> No, I claimed that the final compiled code (the executeable) is just as efficient as a C++ one.\n\nAnd you are still wrong, although that's less of an issue."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "> Still not as fast as Qt for a C++ programmer.\n> And that's just a one-time handicap. The big\n> difference comes in the long run, when actually\n> using them.\n\nTo be honest, it took me much longer to learn QT than GTK+ (and I'm still learning).\nAnd yes, I'm telling you the truth.\n\n> After you had learn GTK+ itself, right ?\n> Honestly, how many classes are defined in your\n> current project ?\n\n(???????????????????)\nAnd one has to learn C++ because he/she can use classes or QT.\nWhat's your point?\n\n\n> And you are still wrong, although that's less of an issue.\n\nWhat about Delphi classes then?\nA TComponent is large yet nobody complains about it and happily continues to do RAD."
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "you want multiple inheritance if you want a class to be a child of two parents. That situation happens a lot\nimagine you have an object you have to register in some list, and therefore it should inherit the foo class. but you also want it to have to be a bar (for similar or other reasons), so you also want to inherit from the bar class.\n. i am a beginning c++ programmer, just learned about mult. inh. (for dcop objects that also have to be widgets, otherwise you have to make two objects with almost the same interface.) , and i can say i encountered a lot of situations before where i could have used mult. inh. but i did not because i did not know it could be done in c++.\nmaybe you can do similar things with interfaces ..."
    author: "ik"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "At 66 to 75 characters C wins, although if you leave out \".h\" and \"std::\" C++ is only 68 (why does the word iostream have to be so dang long!?)."
    author: "Eric Nichlson"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "You can't leave out \"std::\" (well with g++ you currently can but that won't last), unless you add \"using namespace std\" before referring to cout :-)."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "Are you sure?\n\nWhy don't you check this comparative?\nand you will know which one use more code..\n\nRamiro"
    author: "Ramiro Tasquer"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-05
    body: "<em>It is less efficient when coding. But the final result will be just as efficient as any code \"real\" OOP languages produce.</em>\n\n<p>The ONLY efficiency in programming is the programmer's efficiency. And by \"programmer\" I mean all those people who write, modify, add to, and especially, maintain, the program. Who cares how many cycles a program shaves off of execution when few people can maintain it?\n\n<p>You know, it's possible to write object-oriented programs using assembler. So why isn't anyone doing it? Surely OOP in assembler must be extremely efficient..."
    author: "David Johnson"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-05
    body: "inheriting from a gtk widget:\n\n#ifndef __GTK_DIAL_H__\n#define __GTK_DIAL_H__ \n#include <gdk/gdk.h>\n#include <gtk/gtkadjustment.h>\n#include <gtk/gtkwidget.h> \n#ifdef __cplusplus\nextern \"C\" {\n#endif /* __cplusplus */ \n#define GTK_DIAL(obj)          GTK_CHECK_CAST obj, gtk_dial_get_type (), GtkDial)\n#define GTK_DIAL_CLASS(klass)  TK_CHECK_CLASS_CAST (klass, gtk_dial_get_type (), GtkDialClass)\n#define GTK_IS_DIAL(obj)       GTK_CHECK_TYPE obj, gtk_dial_get_type ()) \ntypedef struct _GtkDial        GtkDial;\ntypedef struct _GtkDialClass   GtkDialClass; struct _GtkDial\n{\n  GtkWidget widget;   /* update policy (GTK_UPDATE_[CONTINUOUS/DELAYED/DISCONTINUOUS]) */\n  guint policy : 2;   /* Button currently pressed or 0 if none */\n  guint8 button;   /* Dimensions of dial components */\n  gint radius;\n  gint pointer_width;   /* ID of update timer, or 0 if none */\n  guint32 timer;   /* Current angle */\n  gfloat angle;   /* Old values from adjustment stored so we know when something changes */\n  gfloat old_value;\n  gfloat old_lower;\n  gfloat old_upper;   /* The adjustment object that stores the data for this dial */\n  GtkAdjustment *adjustment;\n}; \n\nstruct _GtkDialClass\n{\n  GtkWidgetClass parent_class;\n}; \n\nGtkWidget*     gtk_dial_new(GtkAdjustment *adjustment);\nguint          gtk_dial_get_type(void);\nGtkAdjustment* gtk_dial_get_adjustment(GtkDial      *dial);\nvoid           gtk_dial_set_update_policy(GtkDial , dial,GtkUpdateType  policy); void           gtk_dial_set_adjustment         (GtkDial  dial, GtkAdjustment *adjustment);\n#ifdef __cplusplus\n}\n#endif /* __cplusplus */ \n#endif /* __GTK_DIAL_H__ */\n\n\n#include <math.h>\n#include <stdio.h>\n#include <gtk/gtkmain.h>\n#include <gtk/gtksignal.h> #include \"gtkdial.h\" #define SCROLL_DELAY_LENGTH  300\n#define DIAL_DEFAULT_SIZE 100 \n\n/* Local data */ \nstatic GtkWidgetClass *parent_class = NULL; guint\ngtk_dial_get_type ()\n{\n  static guint dial_type = 0;   if (!dial_type)\n    {\n      GtkTypeInfo dial_info =\n      {\n\"GtkDial\",\nsizeof (GtkDial),\nsizeof (GtkDialClass),\n(GtkClassInitFunc) gtk_dial_class_init,\n(GtkObjectInitFunc) gtk_dial_init,\n(GtkArgSetFunc) NULL,\n        (GtkArgGetFunc) NULL,\n      };       dial_type = gtk_type_unique (gtk_widget_get_type (), &dial_info);\n    }   return dial_type;\n} static void\n\ngtk_dial_class_init (GtkDialClass *class)\n{\n  GtkObjectClass *object_class;\n  GtkWidgetClass *widget_class;   object_class = (GtkObjectClass*) class;\n  widget_class = (GtkWidgetClass*) class;   parent_class = gtk_type_class (gtk_widget_get_type ());   \nobject_class->destroy = gtk_dial_destroy;   widget_class->realize = gtk_dial_realize;\n  widget_class->expose_event = gtk_dial_expose;\n  widget_class->size_request = gtk_dial_size_request;\n  widget_class->size_allocate = gtk_dial_size_allocate;\n  widget_class->button_press_event = gtk_dial_button_press;\n  widget_class->button_release_event = gtk_dial_button_release;\n  widget_class->motion_notify_event = gtk_dial_motion_notify;\n} \n\nstatic void gtk_dial_init (GtkDial *dial)\n{\n...\n\n\n--------------------------\n\nInheriting a widget with Qt:\n\nclass QDial: QWidget\n{\n   QDial(QWidget * parent): QWidget(parent)\n   {\n...\n\n=========================\n\nI see a difference!\n\n\nI do not invent this, this is pasted from the tutorial of gtk:\nhttp://www.gtk.org/tutorial/sec-creatingawidgetfromscratch.html\n\nWhen you see this amount of code necessary in gtk to handle the OOP, you understand that:\n- a gtk programmer spends lot of time on doing unuseful things (inheritance in C instead of just C++)\n- a gtk programmer needs to get this complicated stuff right\n- the C compiler can't check the OO things, you rely on the programmer\n- a lot of things that should be automatic are left to the programmer\n\n\n> In fact: writing small programs in GTK+ needs less code than QT.\nthis is not true.\nI am writing an article about this that should be ready in a few month to prove it.\n\n> Creating GTK+ objects takes more time, \nYes, two hours to write the code where 5 lines of C++ will do it. Are you going to tell me that usually, you don't create objects ?\n\n> but Gob can make it as simple as C++.\nWhat is Gob ?\nA gtk object generator ?\n\nThe only thing gtk is really good at is wrapping.\nBecause it is C, it is easy to wrap a gtk and program with a _good_ language (good in the sense of suited to the task)."
    author: "Philippe Fremy"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "> Are you going to tell me that usually, you don't create objects ?\n\nI doubt he would, but yet that's most probably the case. A typical GTK+ program has only a handful of huge classes. A C++ program can exercise the OO paradigm much better.\n\n> What is Gob ?\n\nGTK+ Object Builder\nhttp://www.5z.com/jirka/gob.html\n\n> Because it is C, it is easy to wrap a gtk and program with a _good_ language\n\nNot even :-). OK, it's probably true with scripting languages like Python."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "I just copy and paste things from a template and rename some things..."
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "> Yes, two hours to write the code where 5 lines of\n> C++ will do it. Are you going to tell me that usually,\n> you don't create objects ?\n\nI only create objects for large programs.\nBut GTK+ doesn't require you to inherid things.\nEver heard of copy and paste?\nRenaming things takes less than 3 minutes.\n\nIn fact, all the objects I write never look as messy as that code above.\nThey have newlines and identications.\nIf you write C++ code without newlines and identications it will look messy and unmaintainable too."
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "> I only create objects for large programs.\n\nAnd in C++ you can create some whenever you want. You have no idea how far tiny helper classes can get you.\n\n> But GTK+ doesn't require you to inherid things.\n\nYes it does, you have to inherit GtkObject.\n\n> Ever heard of copy and paste?\n> Renaming things takes less than 3 minutes. \n\nThis is becoming hilarious. :-)\n\n> In fact, all the objects I write never look as messy as that code above.\n> They have newlines and identications.\n\nRiiight.\n\nWhen it comes to this point of denial I think we can end this thread."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "> Yes it does, you have to inherit GtkObject.\n\nNo it doesn't.\n\n\nGtkWidget *win;\n\nwin = gtk_window_new (GTK_WINDOW_TOPLEVEL);\ngtk_window_show (win);\n\n\nAnd voila: a window without inheriting an object.\n\n\n\n> Riiight.\n> When it comes to this point of denial I think\n> we can end this thread.\n\nYou don't agree? Remove the idents and newlines from your C++ code and see what it will look like!"
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "> No it doesn't.\n\nHelloooo, we're talking about how to create a new class here. Not instantiate an object.\n\n> You don't agree? Remove the idents and newlines from your C++ code and see what it will look like!\n\nROTFL. Yes, absolutely, you're right, anything it takes to end this thread. Yes, you're using a great tool. There."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-07
    body: "> Helloooo, we're talking about how to create a\n> new class here. Not instantiate an object.\n\nAnd I was talking about instantiate an object, noy about how to derive a class. :)"
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-07
    body: "Now I *really* wish I could see the look on the face of someone who, having read the whole thread, stumbles on this final statement of yours."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-09
    body: "Something like this? o_0"
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "BTW, a lot of the above code is not neccesary.\n\n#include <gdk/gdk.h>\n#include <gtk/gtkadjustment.h>\n#include <gtk/gtkwidget.h> \n\nCompletely useless. Can be replaced with:\n#include <gtk/gtk.h>\nor\n#include <gnome.h>\n\n\n#ifdef __cplusplus\nextern \"C\" {\n#endif /* __cplusplus */\n\nOnly useful in C libraries. Useless in programs.\n\n\n\nguint policy : 2; /* Button currently pressed or 0 if none */\nguint8 button; /* Dimensions of dial components */\ngint radius;\ngint pointer_width; /* ID of update timer, or 0 if none */\nguint32 timer; /* Current angle */\ngfloat angle; /* Old values from adjustment stored so we know when something changes */\ngfloat old_value;\ngfloat old_lower;\ngfloat old_upper; /* The adjustment object that stores the data for this dial */\nGtkAdjustment *adjustment;\n\nYou forgot to include these fields in your C++ class.\nThese fields have nothing to do with inheritance, but the internals of the widget.\n\n\nGtkAdjustment* gtk_dial_get_adjustment(GtkDial *dial);\nvoid gtk_dial_set_update_policy(GtkDial , dial,GtkUpdateType policy); void gtk_dial_set_adjustment (GtkDial dial, GtkAdjustment *adjustment);\n\nIdem dito.\n\n\n#include <stdio.h>\n#include <gtk/gtkmain.h>\n#include <gtk/gtksignal.h>\n\nUseless.\n#include <gtk/gtk.h> does it all."
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "I do not pretend the above gtk code was optimised to be small, nor that the kde code is an exact equivalent. But it gives the general idea of what gtk vs kde programming is.\n\nThere were some problem of copy/paste, this is why the code is not properly indented. The original looks better, as you can check with the url.\n\nAbout copy/pasting code, don't you know that it is dangerous ? Especially in gtk where the compiler can't check anything. If you get one struct wrong, I guess the whole code is fucked-up. True ?\n\nAlso, you need to realise that what I have just copied in the previous message is _the official gtk tutorial_. This is what people are faced with when they want to learn gtk.\n\nIf you think the above code is not good, then correct the tutorial. You tell me about gob, but there is no mention of gob in the tutorial. If I want to program in gtk, I will produce code like that because this is what this tutorial told me.\n\nCompare with Qt: the tutorial can be done in 2 days. A new C++ programmer should have no problem producing a qt application in one week. Qt has only one new concept, signal and slots, which is easy learned. The remaining of Qt is so clean that it could be the basis of an OO lesson.\n\nThe same goes for the kde tutorial.\n\nThe tutorial of gtk is very long, you need probably more than one week to finish it. No C beginner can do it. There are lot of concepts in here to understand. You need to understand the OO abstraction, there are references to XWindow, there are many macros you are afraid to understand  how they work, you must supply by hand a lot of information and there are many things you just do but don't understand.\n\nI wonder why there are that many gtk application out there when I read this.\n\nAn intersting shot would be to take the Qt tutorial and examples, and to rewrite them in gtk. Then compare the results in terms of ease of read, ease of programming, source size, ...\n\nI am doing exactly this right now with gtk and I am having a lot of fun.\n\nNotice also that except for the gimp and perhaps nautilus, there are very few ambitious project written from scratch in gtk. \n\nThis is very different from kde where you have some very ambitious projects, most of them written from scratch, that can compete (and even sometimes beat) gnome projects. And they are done by very few developers (we didn't waste 13 million $ in a filemanager!).\n\nAsk yourself why!"
    author: "Philippe Fremy"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "> About copy/pasting code, don't you know that it\n> is dangerous ? Especially in gtk where the\n> compiler can't check anything. If you get one\n> struct wrong, I guess the whole code is fucked\n> up. True ?\n\nYes, I realize that. But I know exactly what to rename.\nEven though the compiler doesn't check anything, Gtk+ will.\nIt will give warnings like \"WARNING: class FooBar is smaller than it's parent\" and then you know you did something wrong.\n\n\n> Also, you need to realise that what I have just\n> copied in the previous message is _the official\n> gtk tutorial_. This is what people are faced\n> with when they want to learn gtk.\n\nI know that. And that's one of the reasons why idents and newlines are so important.\n\n\n> If you think the above code is not good, then\n> correct the tutorial.\n\nI never said that. I only said it looks messy because it lacks newlines and idents.\n\n> You tell me about gob,\n> but there is no mention of gob in the tutorial.\n\nOf course not. Gob is relatively new and the tutorial is outdated.\nAnd Gob is not part of GTK+.\n\n\n> Qt has only one new concept, signal and slots,\n> which is easy learned. The remaining of Qt is\n> so clean that it could be the basis of an OO\n> lesson.\n\nI learned GTK+' signal system quite easily you know.\nAnd I still don't see much difference between GTK+ signals, QT Signals and Delphi Events.\n\n\n> No C beginner can do it.\n\nI did.\n\n\n> I wonder why there are that many gtk\n> application out there when I read this.\n\nAnd I wonder why there are so many QT programs out there when I read the tutorial.\nIt took me days to find out that I need moc to be able to create QT classes.\nAnd I still have to find out how signal slots work.\n\n\n> This is very different from kde where you have\n> some very ambitious projects, most of them\n> written from scratch, that can compete (and\n> even sometimes beat) gnome projects.\n\nThe opposite is also true. Some Gnome projects can compete/beat KDE projects.\nAnd both can compete/beat Windows and MacOS software and FooBar 2.0 in l33t mode with the flying turtles.\n\n> And they\n> are done by very few developers (we didn't\n> waste 13 million $ in a filemanager!).\n\nEazel spend 13 million $ for the sake of Nautilus and GNOME.\nThat's a deed that shows their kindness and good faith.\nAnd now you're bitching about it? C'mon!"
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "Just for the record :\n\n> Completely useless. Can be replaced with:\n> #include <gtk/gtk.h>\n> or\n> #include <gnome.h>\n\nand drive up your compile times through the roof.\n\nThis is explicitly advised against (even in C++ :-)."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "We're waiting for gcc 3.0 with pre-compiled headers..."
    author: "dc"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-06
    body: "AFAIK they are scheduled for 3.1 or later, not 3.0."
    author: "Guillaume Laurent"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-07
    body: "AAAAAARRRGHHHH!!!!\n(*falls down a big, black hole*)"
    author: "dc"
  - subject: "Both C++ and C w/ OOP style are crappy and bloated"
    date: 2001-06-04
    body: "... compared to ObjC.\n\nObjectiveC = C with about 5 other things to learn it is a simple powerful language with way more flexible approach to OO than C++, no gigantic templates, and simple easy to maintain code. GNUStep is a framework built out of ObjC and it is very high quality.\n\nObjC is why basically 4 people working part time have been able to create GNUStep with **NO** corporate backing (no Trolltech or Redhat or foundation to underwrite things).  Before either KDE or GNOME people gloat too much they should go and READ some of the GNUStep code. Like the workspace manager - sure it's alpha and not that functional: **ONE** guy maintains it in his spare time. Please note how FEW lines of code are in it as well. Before you complain about lack of documentation READ the code. No C Java or C++ developper can honestly say that they can't understand ObjC after 5 minutes.\n\nOh and whipdeedoo on the cross platform stuff, GNUStep has run on a half dozen Unix platforms, Windows, MacOSX, and OpenStep for years now. It's also got a bridge to Java, and Guile and there's and ObjC<->Python project too ... This is all with fewer developpers than there are female senior citizens on the KDE mailing list."
    author: "TrueOO ObjectiveC"
  - subject: "Re: Both C++ and C w/ OOP style are crappy and bloated"
    date: 2001-06-04
    body: "You say because GNUStep can create a DE with fewer developers ObjectiveC is better than C++. But you ignore that KDE is much more than just a bare DE. It has a browser, mail klient, media players, music creating applications, network transparent sound server, many games, ...\n\n(By the way, shouldn't KDE use K or K++ instead? :-)"
    author: "Erik"
  - subject: "Re: Both C++ and C w/ OOP style are crappy and bloated"
    date: 2001-06-04
    body: "Yes, ObjC is a very nice language. I played with it a few years ago. I even added regexp support to GnuStep string class.\n\nUnfortunately, ObjC is also completely dead, which is quite a shame. And speaking of overhead, it also has quite a lot. But you're right, there's no question it beats C++ hands down as far as ease of use is concerned. The OO model is also quite powerful."
    author: "Guillaume Laurent"
  - subject: "KDE/GNUStep Foundation Kit/Objective-C integration"
    date: 2001-06-06
    body: "I'm working on Objective-C bindings for Qt/KDE, and I'll try and get them done in time for KDE 2.2. I think KDE and the GNUStep Foundation Kit should make a good combination. Quite a lot of the  really good stuff in GNUStep is in the Foundation kit, as opposed to the Appkit side. I don't think Objective-C is dead because it is now the native language of Mac OS X, and Apple have at least one full time maintainer, making sure it doesn't die on them :-).\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Benjamin Meyer: A Tribute to KDE"
    date: 2001-06-03
    body: "hmm... moz is C++. sorry, but bzzzt.\n\nSo is M$ stuff, for that matter (with MFC though, which hardly counts) :-).\n\nand the linux kernel is C and rock-solid."
    author: "Kevin Puetz"
  - subject: "microsoft stuff is C++"
    date: 2001-06-07
    body: "actually, most things at microsoft are written in C. only more recent projects have been done with C++. Windows is C."
    author: "Will Stokes"
  - subject: "Want to write an OS instead?"
    date: 2001-06-08
    body: "Do you want to write an OS instead in this well structured, coordinated, single-distributed form. Then you should run, not walk over to www.FreeBSD.org and start contributing."
    author: "Martin Nilsson"
  - subject: "Re: Want to write an OS instead?"
    date: 2002-12-16
    body: "Yeah I agree with you but what's the starting point for newbies.\nThere is a lot of OS design book but few of books how to implement that design."
    author: "ye wint soe htay"
  - subject: "Re: Want to write an OS instead?"
    date: 2003-05-29
    body: "\n\n        Really I agree with you, there are a lot of OS design books, which teach you lot about scheduling and interprocess communication and the like, but\nthey never get down to explaining how to really code one.\n\n        I am also deeply interested in making an OS. Will you and anybody reading this help me in any way you can. \n\n         By the way, if you want to get a list of interrupts for the PC, I think\none should visit \n           http://www.pobox.com/~ralf\n"
    author: "V Fredrick"
  - subject: "Re: Want to write an OS instead?"
    date: 2003-05-29
    body: "I suggest you read Tannenbaum's book, which not only talks about the details of implementing an OS (Minix) but actually does it and includes the source code. You should also not that Ralf Brown's interupt list is mainly about software interupts (ie. DOS system calls) and almost totally useless to anyone trying to write an OS of their own.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Want to write an OS instead?"
    date: 2005-07-26
    body: "send me sample code,books,tools,documets regading writing an OS"
    author: "yeswanth"
---
Why does KDE work so well?  <a href="http://www.csh.rit.edu/~benjamin/">Benjamin Meyer</a> thinks he knows why, and he explains his thoughts in this <a href="http://www.csh.rit.edu/~benjamin/log/e_kde.shtml">Tribute to KDE</a>. Benjamin is the lead developer of <a href="http://kaim.sourceforge.net/">Kaim</a> (<a href="http://kaim.sourceforge.net/screenshots.shtml">screenshots</a>), and has been working hard at porting this application from being Qt-only to a fully-fledged KDE application.  In doing so, he has come in contact with the KDE development community and processes. Benjamin's experiences have led him to conclude that KDE's <a href="http://lists.kde.org/?l=kde-cvs&r=1&w=2&b=200106">CVS culture</a>, release strategy, and focus on consistency lead to polished, mature applications.