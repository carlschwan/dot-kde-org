---
title: "Webpath chooses KDE for the SRB desktop"
date:    2001-01-10
authors:
  - "trobinson"
slug:    webpath-chooses-kde-srb-desktop
comments:
  - subject: "Re: Webpath chooses KDE for the SRB desktop"
    date: 2001-01-10
    body: "Kewl... sounds like I'll have to by one of those babies for my mum!<br><br>Exelent choise of HW (I have a BP6)... but I must say that Linux would have been the political correct answer on the OS side ;)<br><br>best regards<br>/kidcat"
    author: "kidcat"
---
<i>[Ed: This article was submitted about a month ago but got lost in the queue.]</i><br>
<a href="http://www.webpath.net/">Webpath</a> has officially chosen KDE for the desktop environment on its <a href="http://www.webpath.net/srb/">upcoming SRB systems</a>. It was a simple matter of needing an easy to use integrated desktop...we were pleasantly surprised by the progress of KDE, as well as the direction of the KDE project. KDE has made great strides with its office suite, addressed many stability issues, and made it much easier for software developers to write software with its choice of using Qt. Read our full announcement <a href="http://webpath.net/srb/kde-for-desktop.htm">here</a>.

<!--break-->
