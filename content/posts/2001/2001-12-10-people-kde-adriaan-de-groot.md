---
title: "People of KDE: Adriaan de Groot"
date:    2001-12-10
authors:
  - "Inorog"
slug:    people-kde-adriaan-de-groot
comments:
  - subject: "Tea Cooker"
    date: 2001-12-10
    body: "Does it count that I wrote a version of HeyU that can be controlled via dcop?  Using X10 and this dcop can do just about anything, including turn on my monitor and office lights in the morning.\n\nHeyU is no longer available and I think it is no longer developed, so I have been sitting on it until I can write my own serial wrapper.  I just finished one for the OziFox Pen Osciliscope, but again the bugger is prolly only Linux only :P\n\nIf anyone is a serial wizzkid and knows any portable way to read bits from the /dev/ttyS1 please contact me.  \n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Tea Cooker"
    date: 2001-12-10
    body: "Hmm, I have one of those serial-controlled IBM X10 interfaces, is that sort of thing what you're talking about? Mmm, I'd love to have kde support for that, but unfortunately, I don't know how you could read serial bits portably but to allow the user to pipe whatever serial device they want into STDIN (not being much of a C programmer, I'm not even sure if that would work)"
    author: "Carbon"
  - subject: "Re: Tea Cooker"
    date: 2001-12-10
    body: "Hmm, I have one of those serial-controlled IBM X10 interfaces, is that sort of thing what you're talking about? Mmm, I'd love to have kde support for that, but unfortunately, I don't know how you could read serial bits portably but to allow the user to pipe whatever serial device they want into STDIN (not being much of a C programmer, I'm not even sure if that would work)"
    author: "Carbon"
  - subject: "Re: Tea Cooker"
    date: 2001-12-10
    body: "Yes that is what I have been playing with, anything that uses the x10 protocal.\n\nI wrote a serial handler class in C++, but I have not clue if it is portable.\n\nAnyone wanna play?\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Tea Cooker"
    date: 2001-12-14
    body: "Heyu is still being deveoloped. Check http://groups.yahoo.com/group/heyu_users/\n\nJon"
    author: "Jon Lawrence"
  - subject: "Re: Tea Cooker"
    date: 2001-12-14
    body: "Yes there server has been haveing some problems.  Ill try again in a few weeks.  You never know, maby we can have an X10 demo at Linux World Expo in New York.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "What is missing badly in KDE?"
    date: 2001-12-10
    body: "1. Enough Testing! (e.g., cut,copy,paste, rename, move actions) has many bugs.\n2. Easy system & network administration tools\n3. KInstaller\n4. Indeed icon editor with the features of gimp\n5. KOffice 1.2/3.0 ;)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: What is missing badly in KDE?"
    date: 2001-12-10
    body: ">1. Enough Testing! (e.g., cut,copy,paste, rename, move actions) has many bugs.\n\nAnyone can test, getting people to submit good bug reports is the biggest problem.\n\n>2. Easy system & network administration tools\n\nAsk your distributor to make some.\n\n>3. KInstaller\n\nUse apt, you can get it for rpm as well as deb. Another thing for distributors to use.\n\n>4. Indeed icon editor with the features of gimp\n\nWell, I don't know about that one.\n\n> 5. KOffice 1.2/3.0 ;)\n\nIt's coming, but I don't think it will be released with KDE3."
    author: "ac"
  - subject: "Re: What is missing badly in KDE?"
    date: 2001-12-10
    body: "KDE needs Easy system & network administration tools & KInstaller to beat Windows one the desktop. I think KDE schoot join Ximian setup tools and make some kde frond to ther back ends"
    author: "Underground"
  - subject: "Re: What is missing badly in KDE?"
    date: 2001-12-10
    body: "Sorry, but that's an absolutlely horrible idea. As any developer will tell you if you ask on #kde on OpenProjects (go on, just ask), KDE provides only source! An installer is a distinctly binary-only thing, so it falls squarely in the range of packagers and distributions. This is one of the reasons that Debian's installer, apt-get, works so dang well : it's niftiness is not specific to a certain package or packages, it applies to everything in Debian.\n\nAs for admin tools, they would be nice, but they sure aren't a neccessity. They definetly aren't needed for network adminining (if you can't at least open up a console and use linuxconf, then you shouldn't be running a network), and for one-host admining, which basicaly gets down to turning on/off daemons and installing hardware, it would only be superflourous GUI icing on the perfectly good fudge-filled console cake. (Alright, the metaphor was a bit overboard, but you get the idea; in Linux, a GUI is just a frontend)"
    author: "Carbon"
  - subject: "Re: What is missing badly in KDE?"
    date: 2001-12-11
    body: "Carbon: \"As any developer will tell you if you ask on #kde on OpenProjects (go on, just ask), KDE provides only source! An installer is a distinctly binary-only thing, so it falls squarely in the range of packagers and distributions.\"\n\nWell, that's just plain stupid, IMHO. I'm well aware of the problems in creating an installer, but some graphical tool which installs KDE, koffice and other applications would be very, very nice."
    author: "Erik Hensema"
  - subject: "Re: What is missing badly in KDE?"
    date: 2001-12-11
    body: ">>\nWell, that's just plain stupid, IMHO. I'm well aware of the problems in creating an installer, but some graphical tool which installs KDE, koffice and other applications would be very, very nice.\n<<\n\nSorry, I just don't see the advantage. Here are the reasons why KDE isn't making a installer, at least, as far as I can see:\n\n-The developers have far more interesting and important things to do then work on installers, like fixing bugs, adding features, translating, etc.\n-It would be reinventing the wheel. Distros already have their own installers.\n-It would be a PITA to code. All of a sudden, every OS and distro of said OS that KDE runs on would need to be manually mentioned in the main KDE source tree. That's a definite no-no.\n-There would be almost no usability advantage, but a definite loss of efficiency. To take Debian for (a particularly good) example, how can it get simpler then typing:\n\napt-get install kde\n\nThis command will automatically download the latest version of KDE, including everything it needs to run, and install it. Moreso, a graphical installer would require that you already have X installed, whereas this command will install X (and, in fact, _any_ other dependencies of KDE) as needed.\n\nEven for a less automagical RPM or tarball based OS, the only thing that's needed is downloading the files, and running a few simple commands. Even compiling KDE from source is quite simple, just run 3 commands for each package you want.\n\nI just don't see the point, and the developers I've talked to don't either."
    author: "Carbon"
  - subject: "Re: What is missing badly in KDE?"
    date: 2001-12-10
    body: "That was already tried.  See http://www.derkarl.org/kdesysconfig-archives.phtml for the mailing list archives.  Note that the last post to the list was in august.  It isn't easy to make a system configuration tool.\n\nIf someone wants to revive the project, they're welcome."
    author: "not me"
  - subject: "Re: What is missing badly in KDE?"
    date: 2001-12-10
    body: "hey either fix them or stop posting the same message over and over.\nwe know about the bug reports, so either help or please go use what ever windows you can download and leave us alone. \n\n1) We have many levels of testing, but unless people fill out useful bug reports we cannot test everything.  When I get a bugreport for \"Unknown\" that says it crashed, I cannot fix that.\n2) That is not our job, that is the job of your dist.  This is a dead and over worked subject that can be dropped.\n3) Again a dists job... this too is a dead subject.\n4) ? Are you on crack?  Why do you need layers on a 16x16 pixel icon?  You obviously have no clue what you are doing in either application.\n5) I am using KWord right now in KDE 3.0 CVS as we speak.  \n\nPlease think harder before posting again, at least get createive like asking for something like DVD support, C# bindings or my personal favorite is a VB runtime for KDE.  Really all of these developers are working very hard, give them a break, or lend a hand.  \n\nCheers\n\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: What is missing badly in KDE?"
    date: 2001-12-10
    body: "Hey Ian - relax a bit, will u. It\u00b4s nice to have people interested in KDE, and if they don\u00b4t have the knowledge u do, pissing them off is not the way forward... Either share what u know in a friendly manner - which is a ground rule in the open source comm. - or don\u00b4t reply. That\u00b4s how I see it, anyway. \n\nWith the best regards\n\nJohan Abrahamsson"
    author: "ja"
  - subject: "Re: What is missing badly in KDE?"
    date: 2001-12-10
    body: "I am very relaxed this guy has been trolling for some time now and refuses to listen or help.  I figure if enough of us repeat the same message over and over he may get the point...\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Tired of Usability bugs!"
    date: 2001-12-10
    body: "Excuse me, I had no intention to offend the developers or the users! if you try do your daily work in KDE+Linux you may get annoyed by simple unexpected behaviors like loss of selection:\n\n1. Open Home Folder\n2. select a file/files by dragging around them\n3. click reload button or press F5 key\n4. your selection is gone.\n\nAnd this is annoying when You want to remember 2-3 files in many files, this loss of selection is very prominent either after preview of documents or rename etc.\n\nI have reported many bug reports regarding copy paste or basic actions. I feel that many applications do not get enough testing. I hope these bugs will be fixed but other users must report such annoyances (which makes the users' work a painful experience) at http://bugs.kde.org."
    author: "Asif Ali Rizwaan"
  - subject: "Tired of offtopic messages!"
    date: 2001-12-10
    body: "We appreciate your concern for KDE.  However, posting an extremely offtopic message to nearly every article on the Dot is NOT going to get these bugs fixed any faster.  It is great that you reported the bugs to the bug system.  Now let the bug system do its work.  Please stop posting useless offtopic messages in message boards where people are trying to have on-topic discussions.  Thank you."
    author: "not me"
  - subject: "Re: Tired of offtopic messages!"
    date: 2001-12-10
    body: ">Please stop posting useless offtopic messages in message boards where people \n>are trying to have on-topic discussions. Thank you.\n\nYup! thanks!\n\n>Now let the bug system do its work.\n\nBugs got closed and still lurks in KDE; one more just discovered:\n\nUnable to cut/copy/delete multiple files in konq.\n1. select 3 files\n2. right-click on any of the 3 files\n3. when the menu appears, the remaining 2 files get unselected\n4. annoying indeed, when you are doing some work!\n\nthis was reported (not by me) but got closed. I reported again. Actually, I am interested in the Success and Bugfree-ness of KDE. Anyway sorry for bothering you all!"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Tired of offtopic messages!"
    date: 2001-12-10
    body: ">Bugs got closed and still lurks in KDE; one more just discovered:\n> Unable to cut/copy/delete multiple files in konq.\n> 1. select 3 files\n> 2. right-click on any of the 3 files\n> 3. when the menu appears, the remaining 2 files get unselected\n> 4. annoying indeed, when you are doing some work!\n\n   5. working perfectly here (both CVS HEAD and KDE2.2.something)\n\n> this was reported (not by me) but got closed. I reported again. Actually, I am interested in the\n> Success and Bugfree-ness of KDE. Anyway sorry for bothering you all!\n\n If the bugreport got closed, it usually means it was fixed. It doesn't make sense to create another bugreport for the same thing, send additional info to the same bugreport. Also, when a bugreport is closed it doesn't mean it got somehow magically fixed also in your KDE. You have to actually use the fixed version, this sometimes means waiting for another release with higher minor version number, sometimes a bug can't be fixed in the stable branch. Flooding the bug database isn't a very good way of making KDE more stable and bugfree."
    author: "L.Lunak"
  - subject: "bug fixed!"
    date: 2001-12-11
    body: "Sorry! I will be careful to use the lastest stable release, and avoid dot for offtopic postings."
    author: "Asif Ali Rizwaan"
  - subject: "Re: bug fixed!"
    date: 2001-12-11
    body: "Dude your being a total jerk. Could i recommend posting on www.freekde.org You'll Find yourself in good company there. Every once in a while just make insane claims about responsible members of the KDE community and other wise make total ass of yourself and you'll fit right in. They may even ignore your annoying spamming activates.\n\nCraig"
    author: "Craig"
  - subject: "Go easy"
    date: 2001-12-11
    body: "I don't think this guy is being a problem.  If you want to see a problematic poster, go back to the articles where a character named Wiggle was posting.  Asif seems to be very concerned about KDE, and this is good.\n\nHowever, I think he does repeat himself too often.  KDE development has been going along just fine, and if he continues to only use the latest stable version then it is difficult for him to tell just exactly what has been worked on.  Even then, each developer can only do one thing at a time.  Everyone's needs, including his, will be addressed at some point (well, except for that silly taskbar image he always posts, something KDE could never implement with a straight face).\n\nTo Asif:  we hear you, and I'm sure the KDE developers do too.  No need to repeat yourself.  To anyone else:  go easy, anything KDE is on-topic in here.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Go easy"
    date: 2001-12-12
    body: "Come on did'nt you see i was just looking for a way to crack on the problem children over at freekde.org."
    author: "Craig"
  - subject: "Re: Tired of offtopic messages!"
    date: 2001-12-13
    body: "It was closed because it was fixed.\n\nNo need to submit again."
    author: "jz"
  - subject: "Re: Tired of Usability bugs!"
    date: 2001-12-13
    body: "I don't think that's a bug, personally. In fact, win explorer does this too., and pretty much every other file manager.\n\nso it's more of a feature request, and one most developers wouldn't want. in that case, submit a patch for enabling an option for this behavior! ;P"
    author: "jz"
  - subject: "Re: What is missing badly in KDE?"
    date: 2001-12-11
    body: "> 4) Are you on crack? Why do you need layers on a 16x16 pixel icon?\n\nThere are many cases where these would be quite comfortable - in 32x32-icons or 48x48 you can't live without them :-)\n\n> You obviously have no clue what you are doing in either application\n\n*cough* *cough*\n\nTackat"
    author: "Tackat"
  - subject: "Re: What is missing badly in KDE?"
    date: 2001-12-13
    body: "4. just use gimp (or krayon).\n\nwhat we really need is a kde frontend of gimp. hopefully this will be possible with the next major version of himp, whihc will be more interface portable."
    author: "jz"
  - subject: "Kruft?"
    date: 2001-12-11
    body: "For those who are wondering about this future project of Adriaan: it's Dutch Kfart. Parental advisory: this interview has a large lame-jokes content."
    author: "slierp"
---
Those of you interested in the interoperability of KDE with handheld computers will already be familiar with <a href="http://www.kde.org/people/adriaan.html">KDE developer Adriaan de Groot</a>, current maintainer of <a href="http://www.slac.com/pilone/kpilot_home/">KPilot</a>, and guest of this week's instalment of the <a href="http://www.kde.org/people/people.html">People Behind KDE</a> interviews. <a href="mailto:tink@kde.org">Tink</a> and Adriaan provide us with a funny and entertaining dialog, including a rather hilarious picture of Adriaan.  Well, it shouldn't surprise anybody that for Adriaan, as for any of us, speaking of KDE proves to be a cheerful and pleasant experience.
<!--break-->
