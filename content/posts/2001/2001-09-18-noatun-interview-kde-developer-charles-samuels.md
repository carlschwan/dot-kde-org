---
title: "Noatun-Interview with KDE Developer Charles Samuels"
date:    2001-09-18
authors:
  - "Dre"
slug:    noatun-interview-kde-developer-charles-samuels
comments:
  - subject: "Crucial Question Forgotten"
    date: 2001-09-18
    body: "Charles, what do you think about the Tabs versus Spaces for indentation situation in KDE?  I am never sure which to use and I am not happy about the state of Spaces and Tabs in KDE CVS!  We should do something about this.\n\nThanks for Noatun though. :)"
    author: "KDE User"
  - subject: "Nonsense"
    date: 2001-09-18
    body: "Everybody knows that the OTIS (One True Indenting Style) uses 4 spaces for indentation.\n\npffffffft..."
    author: "Simon Perreault"
  - subject: "Re: Nonsense"
    date: 2001-09-18
    body: "\"Everybody knows that the OTIS (One True Indenting Style) uses 4 spaces for indentation.\"\n\nBah!  No!  Use tabs.  If you like your indentation to be 4 spaces, set your tabstop to 4."
    author: "Matt Spong"
  - subject: "Re: Nonsense"
    date: 2001-09-18
    body: "What does 4 spaces mean ? 4 spaces.\nWhat do you intend when putting them ? Indent.\n\nJust as the newline character means a new line,\net the tab mean indentation (or tabulation).\nThat was it is for.\n\nThe first thing you learn while writing XML/SGML\ndocuments is that contents is not presentation.\nYou waste your time if you take care of presentation\n(ie : counting tabs, and why 4 tabs after all - it's\na presentation choice) while coding. Tabs acts as\na XML tag that says... indent !\n\nLet your editor handle the look of your code, and\nput only the core meaning in your files.\n\n(of course, that's my opinion... and no, I do not\nwant to troll ;-)"
    author: "Christian"
  - subject: "Re: Nonsense"
    date: 2001-09-18
    body: ": The first thing you learn while writing XML/SGML\n: documents is that contents is not presentation.\n\n    Damn it, I \ntook                         your\n           concept to heart\n,\n       and now my                 python\n                programs won't\n    run!\n\n--\n                                         Evan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Nonsense"
    date: 2001-09-18
    body: "You misunderstood me. In python, indentation is part\nof the semantic. Presentation has nothing to do with\nindentation, you can indent with tab (a language that\ndoesn't accept that is a broken one)."
    author: "Christian"
  - subject: "Re: Nonsense"
    date: 2001-09-19
    body: ":: You misunderstood me. In python, indentation is part\n:: of the semantic. Presentation has nothing to do with\n\n   In python, indentation is part of the semantic \ninformation used by the interpreter.  In C++, \nindentation is part of the semantic information used\nby the coder.\n\n   Otherwise, why comment code?  It does not carry any\nsemantic information that the compiler cares about...\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Crucial Question Forgotten"
    date: 2001-09-18
    body: "While reloading http://www.kde.org/ waiting for the KDE 2.2.1 release, so that I can mention it on freekde in a timely manner, I happened to notice this article... and it's a good thing I did.\n<br><br>\nThe definitive answer on code indentation is found at <a href=\"http://www.freekde.org/neil/why_to_tabs.html\">why-to-tabs.html</a>, mirrored on freekde as Charles lost his DSL.  A more complete style document is available at <A href=\"http://www.freekde.org/neil/s-and-s.html\">s-and-s.html</a>\n<br><br>\n\n(I still wish you'd use slash so I can turn off comment display, Navindra)"
    author: "Neil Stevens"
  - subject: "Re: Crucial Question Forgotten"
    date: 2001-09-18
    body: "And this from the person who actually used but subsequently abandoned Slash for some other wanabee LAMP solution? ;)\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Crucial Question Forgotten"
    date: 2001-09-18
    body: "Definitive my ass.  The fact that you prefer it hardly makes it definitive.  Plus it was written by Charles.  Not to downplay his significance in KDE, but he's hardly an authority on all of programming.  K&R say style is choice of the coder (what a shock!), and the only thing they recommend is sticking with the same style throughout your code, which is excellent advice.\n\nSo stop this \"definitive\" crap."
    author: "KDE User"
  - subject: "Re: Crucial Question Forgotten"
    date: 2001-09-18
    body: "> The definitive answer on code indentation is found at why-to-tabs.html\n\nI think it comes as no surprise that the person who wrote this (Charles) is still in high school. None of his arguments hold ground. This one in particular is a gem :\n\n\"Any editor that doesn't cope with tabs isn't an editor, but a bug (emacs comes to mind).\"\n\nBe assured that once he graduates, after a couple of years working as a professional programmer part of a team, he'll have quite a different opinion. At least I hope for him :-)."
    author: "Guillaume Laurent"
  - subject: "Re: Crucial Question Forgotten"
    date: 2001-09-19
    body: "Yeah... really definitive, anyone who uses tabs for identation will eventually use a space (mainly when you break statements into multiple lines) and... voil\u00e1, your code now visually suck everywhere but your editor (good luck trying to avoid it). Anyway tabs or spaces are a matter of personal choice, there's nothing definitive about that. I use spaces... I want my code to look the same everywhere... maybe I'm evil, but that's my choice."
    author: "Carlos Rodrigues"
  - subject: "Notaun site down"
    date: 2001-09-18
    body: "Notaun ( http://notaun.kde.org ) site seems to be down ..."
    author: "kde user"
  - subject: "Re: Notaun site down"
    date: 2001-09-18
    body: "Yeah, that's because it's running on Njaard's (er, i mean Njord's) personal network, and he's been having some ISP problems..."
    author: "Carbon"
  - subject: "Re: Notaun site down"
    date: 2001-09-18
    body: "Why Charles, it seems even the dot is more stable than your server! :-P"
    author: "Navindra Umanee"
  - subject: "Re: Notaun site down"
    date: 2001-09-18
    body: "Bleh! The server is still up!\n\n$ ping altair\nPING altair.littlenet (192.168.1.1): 56 data bytes\n64 bytes from 192.168.1.1: icmp_seq=0 ttl=255 time=0.9 ms\n \n--- altair.littlenet ping statistics ---\n1 packets transmitted, 1 packets received, 0% packet loss\nround-trip min/avg/max = 0.9/0.9/0.9 ms\n\nIt's just that it's not actually connected to the internet :)\n\nIt may also help to spell noatun.kde.org *correctly* in the future. btw :)"
    author: "Charles Samuels"
  - subject: "More codec via plugins..."
    date: 2001-09-18
    body: "Hm, it would be great if it where possible to use the \nquicktime .dll files as a plugin to play the Sorensen Codec movie files\nvia Noatun.\n\n\n\n\n(I know there is a crossover plugin that can play sorenson codec videos, but\nit is not free like free beer, and as long as i can dualboot and use quicktime under windows...)"
    author: "Catonga"
  - subject: "Re: More codec via plugins..."
    date: 2001-09-18
    body: "What about reaktivate?"
    author: "A Sad Person"
  - subject: "Re: More codec via plugins..."
    date: 2001-09-18
    body: "i'd LOVE to see a xine plugin! (xine.sourceforge.net). using it you'd get mpeg (ok that's duplicate but last time i checked i found rendering noticeably better than with mpeglib), mpeg2, DVD (that's the selling thing, and no MPAA risks, you install the DVD-unscrambling thing as a plugin if you want, xine does NOT include it :o) ), AVI, etc etc.\nand now xine is split out in a library and a UI module so it's possible... it's be great to have it integrated in noatun..\n\nemmanuel"
    author: "emmanuel"
  - subject: "Re: More codec via plugins..."
    date: 2001-09-19
    body: "I agree, and it doesn't have any evil dependencies either."
    author: "sashmit"
  - subject: "Real player theme for Noatun?"
    date: 2001-09-18
    body: "hi charles,\n\nI have some wishes for Noatun:\n\n1. It should be a media player instead of just audio player including VCD, DVD, AVI and especially real media's *.rm and *.ram files and other too.\n\n2. I wish Noatun should get a Real Player theme which looks very nice, and the URL you gave in the Help \"http://www.angelfire.com/mo/nequiem/tutorial.html\" is dead :(\n\n3. I wish Xmms plugins (which are too many) could be used with Noatun :)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Real player theme for Noatun?"
    date: 2001-09-19
    body: "Well, a direct port of xmms to qt and kde is in the works. It is almost done, and hopefully will be released in several weeks :)\n\n\nThe only downside is that gtk+ plugins have much trickery involved to work properly.\n\nBesides, that, the interface itself is natively drawn using qpainter instead of gdk."
    author: "AC"
  - subject: "Re: Real player theme for Noatun?"
    date: 2002-04-19
    body: "The new link for my tutorial is here:\n\nhttp://www.angelfire.com/mo/nequiem/kjofol/tutorial.html\n\n-nequiem"
    author: "nequiem"
  - subject: "Re: Real player theme for Noatun?"
    date: 2002-11-26
    body: "This is a question. Could you give me som info on the NOATUN media player? I need to know How it works, where the files are saved and how they are saved. Also if you could, what files it plays and what it does not, and any additional info you think is helpful. I appreciate it. Thanx, Dan Tenters"
    author: "Dan Tenters"
  - subject: "Re: Real player theme for Noatun?"
    date: 2002-11-26
    body: "Hi\n\nIn fact noatun is just a GUI for ARTS. ARTS is KDE's multimedia server. So, if ARTS can play a file, noatun can and vice-versa. IIRC, ARTS uses a plugin system to read different file formats. On my machine, ARTS can play WAV, OGG, MP3 as well as MPG, ASF, AVI, DIVX etc... If you want to read the latest video formats (DIVX, ASF etc...), you have to install the \"xine-arts\" plugin which enables ARTS to play every file that Xine (the video player) can read. So, if ARTS can play them, then Noatun (and kaboodle and any ARTS-enabled application) can read it.\n\nI hope that was clear enough."
    author: "Julien Olivier"
  - subject: "The Real Question..."
    date: 2001-09-18
    body: "...is where the hell is KDE 2.2.1? It was supposed to be released yesterday (9/17/2001)!\n\nBee-autch"
    author: "Some Poor Slob"
  - subject: "Re: The Real Question..."
    date: 2001-09-18
    body: "I've been using it since sometime last week. It's GREAT!!!"
    author: "eze"
  - subject: "Re: The Real Question..."
    date: 2001-09-18
    body: ":: ..is where the hell is KDE 2.2.1?\n\n    No, the real question is \"Were the tarballs released?\", your question would (probably) be \"Where are my OS/distro's binaries?\"\n\n    Incidently, someone on #kde just told me that the tarballs *have* shipped \"days ago\", and that means that it's in the hands of the packagers.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: The Real Question..."
    date: 2001-09-18
    body: "if you can find it you can propably get the sources via cvs.\n\nBram."
    author: "brambi"
  - subject: "Re: The Real Question... Here it is!"
    date: 2001-09-18
    body: "ftp://ftp.kde.org/pub/kde/stable/2.2.1/\n\nenjoy it"
    author: "yves"
  - subject: "Re: The Real Question... Here it is!"
    date: 2001-09-18
    body: "It seems at Mandrake everyone is too busy to make RPMS... maybe it's time to switch to SuSe."
    author: "mdkuser?"
  - subject: "Re: The Real Question... Here it is!"
    date: 2001-09-19
    body: "Yeap,if you like kde upgrades ASAP:)"
    author: "xmdkuser"
  - subject: "Re: The Real Question... Here it is!"
    date: 2001-09-20
    body: "Mandrake was too busy getting its release candidate for version 8.1 out the door.  Said release candidate does include KDE 2.2.1.   Switch to SuSE if you wish but there is no need to utter calumny against Mandrake's deservedly good name."
    author: "Frogger"
  - subject: "Hmm..."
    date: 2001-09-18
    body: "The Idea behind Noatun is quite nice. The problem is it just doesn't seem to work. Upto now I never had a version that didn't crash right away after starting to play a simple .wav or .mp3 file. I regurlarily build CVS versions and right now I'm running 2.2.0. Well, maybe I have to wait a while. Until it's ready I'll use xmms...\n\nCheers,\n\nRichard"
    author: "Richard"
  - subject: "Re: Hmm..."
    date: 2001-09-18
    body: "You should expect problems when running the CVS version, specially because of changes in arts.\n\nThe best thing you can do right now is try KDE 2.2.1 and if that doesn't fix your problem, report a bug."
    author: "Evandro"
  - subject: "Re: Hmm..."
    date: 2001-09-18
    body: "My first impressions of noatun where also not good. It was ugly and complex. But now in kde 2.2 with a nice skin I like it. \n\nI also use mostly xmms. The reason is simple: xmms doesn't use the soundserver and blocks the /dev/dsp device so I don't hear the embarrassing kde window manager sounds when listening music :-)))\n --Bram."
    author: "brambi"
  - subject: "Re: Hmm..."
    date: 2001-09-19
    body: "You know...you could just turn off the windomanager sounds....\n\nI use xmms too...mainly because I'm used to it......but I use the arts output plugin....."
    author: "Stuart Herring"
  - subject: "Re: Hmm..."
    date: 2001-09-19
    body: "I had the same problem until 2.2.0.  I didn't use it much when I ran 2.2.0, but I've been stressing it quite a bit since I pulled 2.2.1 from CVS early last week and it's been solid."
    author: "SubPar"
  - subject: "mplayer"
    date: 2001-09-19
    body: "Please have a look at the CVS-version of MPlayer! (http://mplayer.dev.hu)\nIt plays just about anything (including WMV8). They also have QuickTime and RealMovie support in the TODO-list... (or at least talk about it on the mailinglist). It is also the fastest DVD-player on Linux.\nA nice improved Windoze Media Player 6.4-alike KDE-GUI to this beast would make it the best Movie-player there is on any platform!\nAlso take the DVD-menus support from Ogle (http://www.dtek.chalmers.se/groups/dvd/) !"
    author: "Luser"
  - subject: "Re: mplayer"
    date: 2001-09-20
    body: "Why not just use aviplay. It is equivalent to mplayer (same libs), and has a nice qt gui instead of mplayer's ugly gui."
    author: "MegaBite"
  - subject: "Re: mplayer"
    date: 2001-09-20
    body: "Because aviplay's underlying code is inferior. MPlayer is *much* faster and is more configureable.\nI'd rather use a descent and full-featured player than some near-empty but good looking one."
    author: "Stof"
  - subject: "Re: mplayer"
    date: 2001-11-10
    body: "I WOULD LIKE TO JOIN"
    author: "RAVEN"
  - subject: "Re: mplayer"
    date: 2001-11-10
    body: "I LOVE MPLAYER"
    author: "RAVEN"
---
<a href="mailto:klaus.staerk@gmx.net">Klaus St&auml;rk</a> has
submitted an interview he recently conducted with
<a href="mailto:charles@kde.org">Charles Samuels</a> about
<a href="http://noatun.kde.org/">Noatun</A>.  Noatun is KDE's powerful multimedia (video/audio)
player.  Klaus tells us that Noatun will be the &quot;Application of the
Month October 2001&quot; on the <a href="http://www.kde.de/">German
KDE-webpage</a>, so the interview will be translated into German at that time.

<!--break-->
<blockquote>
<p>
<strong>Klaus (Q)</strong>:
<em>What does "Noatun" stand for? Is it an acronym for something?</em>
</p>
 
<p>
<strong>Charles (A)</strong>:
It's not an acronym at all.  It's related to my nickname on IRC (Njaard -- a
misspelling of the Norse god Njord).  Let's leave it at that.
</p>
 
<p>
<strong>Q</strong>:
<em>How did you start the Noatun-Project and who else is involved in
developing it</em>
</p>
 
<p>
<strong>A</strong>:
Noatun was started on August 26, 2000, well, that was the first commit.  I'd
been thinking about it already in March of that year, I decided that there
needed to be a media player that could play everything, and one that didn't
have silly skins.
</p>
<p>
It evolved quite a bit, and later that year, the core was pluginified, the
user interface was implemented (now known as Milk Chocolate), and the
playlist (now the Split Playlist) was created.
</p>
<p>
It developed quite rapidly, especially since I made a deal with myself: I
will not play a single bit of music unless it's with Noatun, and I listen to a
lot of music, so, within that week, I was playing music.  Not well, mind you,
the playlist hardly worked, and the UI didn't do much related to song length
and title.
</p>
<p>
<strong>Q</strong>:
<em>What other oss-projects are you involved in?</em>
</p>
 
<p>
<strong>A</strong>:
A couple of minor projects,
<a href="http://freshmeat.net/projects/itc/">ITC</a>, which, BTW, was
created to make it easy for me to write multithreaded codecs for
<a href="http://www.arts-project.org/">aRts</a> (kdenonbeta/arts/modplug
has now become a threaded one, with ITC)
</p>
<p>
<strong>Q</strong>:
<em>How was it decided to add Noatun to KDE?</em>
</p>
<p>
<strong>A</strong>:
From the very start, I created it in the hopes that it would be part of KDE.
Kaiman was . . . inadequate, to say the least. In addition, I have a strong
dislike for skins (which is strange, since I wrote what may be Noatun's best
skin loader, KJofol), and Kaiman was, at the time, unmaintained, and lacking
many features.
</p>
 
<p>
<strong>Q</strong>:
<em>You visited the LWE in SF this week where you had the joy of accepting the
"Best Open Source Project" award in the name of the KDE-Project.  What did
you think of that?</em>
</p>
 
<p>
<strong>A</strong>:
Well, it was nice!  I can't say I was too shocked, but I certainly was
pleased, and the large amount of KDE developers present made it even better.
</p>
 
<p>
<strong>Q</strong>:
<em>How do you see the future of multimedia apps for KDE, especially Noatun?
What can the KDE-user expect from these apps?</em>
</p>
 
<p>
<strong>A</strong>:
It will get better.  We're working on making Noatun itself much more powerful
and extensible via plugins.  It's designed bottom-up to make a huge amount of
features in the form of plugins, and KDE 3.0 will make this hold even more
true.  Multimedia-wise, support for more formats, more features, different
effects.
</p>
 
<p>
<strong>Q</strong>:
<em>Which is your favourite feature in Noatun?</em>
</p>
<p>
<strong>A</strong>:
Probably that the user-interface is a plugin.  I still in fact use the
original user-interface (Milk Chocolate), however, nobody else is forced to.
Although, Young Hickory is real nice.
</p>
 
<p>
<strong>Q</strong>:
<em>What do you do in your sparetime when you're not coding for the
KDE-project?</em>
</p>
 
<p>
<strong>A</strong>:
I don't understand the question .&nbsp;.&nbsp;. ;-) Seriously, I'll be
starting at a university in a couple of months, so until then, I get to
code to my heart's content.  Until I get a job *hint*.  There's also a
lot of non-KDE stuff I've written (which you get to hear about if you
find me on irc.kde.org)
</p>
 
<p>
<strong>Q</strong>:
<em>What's your opinion on the future of "Linux on the desktop"?</em>
</p>
 
<p>
<strong>A</strong>:
I think that Linux is a desktop OS, and it will slowly kick lesser operating
systems off the market.  And with Linux's popularity increasing, KDE's will,
along with FreeBSD's and the like.
</p>
</blockquote>
<p>

<strong>Links</strong>:</p>
<ul>
<li>Klaus St&auml;rk:               <a href="mailto:klaus.staerk@gmx.net">klaus.staerk@gmx.net</a>
</li>
<li>Charles Samuels:        <a href="mailto:charles@kde.org">charles@kde.org</a>
</li>
<li>Noatun:                 <a href="http://noatun.kde.org/">http://noatun.kde.org/</a>
</li>
</ul>
