---
title: "Qt 2.3 released: I can see clearly now"
date:    2001-03-10
authors:
  - "KDE.org"
slug:    qt-23-released-i-can-see-clearly-now
comments:
  - subject: "Can't run KDE2.1!"
    date: 2001-03-10
    body: "I'm having severe problems running KDE2.1 I have SuSE 7.0 PRofessionak with following modifications:\n\nKernel has been updated to 2.4.2\nXfree has been updated to 4.0.2\n\nI downloaded Suse 7.0 RPM's. I installed QT 2.2.4 correctly, and I installed KDE packages in correct order using Yast. But when I try to run KDE2.1 I get following error-messages:\n\nksplash: error in loading shared libraries: /opt/kde2/lib/libkdeui.so.3: undefined symbol: sizeHint__C7QDialog\n\nkdeinit: error in loading shared libraries: /opt/kde2/lib/libkdecore.so.3: undefined symbol: startsWith__C7QStringRC7QString\n\nknotify: error in loading shared libraries: /opt/kde2/lib/libkdeui.so.3: undefined symbol: sizeHint__C7QDialog\n\nktip: error in loading shared libraries: /opt/kde2/lib/libkdeui.so.3: undefined symbol: event__9QLineEditP6QEvent\n\nSession management error: networkIdsList argument is NULL\n\nksmserver: error in loading shared libraries: /opt/kde2/lib/libkdecore.so.3: undefined symbol: qt_set_locale_codec__FP10QTextCodec\n\nI have re-installed QT twice, but no help, I have installed KDE-packages several times with and without Yast. I upgraded to QT 2.3, but it doesn't help =(!\n\nI'm starting to get desperate!\n"
    author: "Janne Ojaniemi"
  - subject: "Re: Can't run KDE2.1!"
    date: 2001-03-11
    body: "I had a similar problem with RedHat 7.0, but I\nfound the fix.  This is RedHat, but there is \nprobably a very similar thing happening on your\nSuSE system.\n\nCause: installed KDE 2.x from different sources,\nI.E. kde.org and redhat.  They put the shared libs\nin slightly different places, and you are seeing\nsome of the older libs.\n.\nRedHat fix: Notice the lib names in your error.\nThe qt-2.2.4 stuff should be in /usr/lib/qt-2.2.4.\nRemove the kde libs in /usr/lib with the same names as /usr/lib/qt-2.2.4. \n\nOne way to see what shared libs kde is going to use is to run \"ldd $(which ksplash)\"\nSee if libkdecore.so.3 is coming from an unexpected place.\n\nGood luck!"
    author: "Randy Hron"
  - subject: "Re: Can't run KDE2.1!"
    date: 2001-03-14
    body: "It worked! I jut copied everything in /usr/local/qt/lib to /usr/lib and /usr/lib/qt. \n\nBut. I also noticed that I'm missing the AA'd fonts in Control Panel, even though I have installed & compiled QT 2.3 correctly. I downloaded latest RPM's from Suse's FTP-site. Is the AA-checkbox visible on KDE.org rpm's?"
    author: "Janne Ojaniemi"
  - subject: "Re: Can't run KDE2.1!"
    date: 2001-03-14
    body: "It worked! I jut copied everything in /usr/local/qt/lib to /usr/lib and /usr/lib/qt. \n\nBut. I also noticed that I'm missing the AA'd fonts in Control Panel, even though I have installed & compiled QT 2.3 correctly. I downloaded latest RPM's from Suse's FTP-site. Is the AA-checkbox visible on KDE.org rpm's?"
    author: "Janne Ojaniemi"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-10
    body: "Did you download and install liblcms.rpm and libmng.rpm?\nFor some reason they are not in the same directory as the other rpms for i386.\nYou have download them from SuSE."
    author: "reihal"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-13
    body: "Yes, I downloaded and installed liblcms and libmng, but it didn't help.\n\nI downloaded all KDE2.1 RPM from kde.org, so they are not from different sources. It seems that I'm just about the only one to have problems with SuSE rpm's. What am I doing wrong?\n\nI bet that there's some minor mistake somewhere. But to be honest, I'm at a loss :(!"
    author: "Janne Ojaniemi"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-10
    body: "I got QT to display anti-aliased fonts (Wow!), but I can't get KDE to startup displaying aa fonts.  During startup I see the following messages:<br>\n<br>\n<pre>\nkio (KLauncher): setLaunchEnv KDE_MULTIHEAD=false\nkdeinit: Got SETENV 'KDE_MULTIHEAD=false' from klauncher.\nkio (KLauncher): setLaunchEnv QT_XFT=0\nkdeinit: Got SETENV 'QT_XFT=0' from klauncher.\n</pre>\n<br>\nAnyone know how I can get KDE to turn QT_XFT to true?"
    author: "Kevin Pearson"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "KDE has an option to turn on AA in the Control Center.  Check Look & Feel under Style under Use Anti-Aliasing for fonts and icons."
    author: "KDE User"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "I've looked an I can't find an option for AA anywhere.  I'm using kde 2.1 under RedHat 7.0.\n<br>\nI've attached a screen shot of my control center."
    author: "Kevin Pearson"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "Very odd, I'm running KDE 2.1 compiled from sources and it does have the AA on/off option. See attached screenshot."
    author: "Maarten ter Huurne"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "That is a very nice theme, what is it called?"
    author: "KDE User"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-12
    body: "I'm glad you like it. It's a theme I've been working on for a while, but it's not released yet. People interested can send me an e-mail or check my KDE page once in a while (http://www.stack.nl/~mth/kde/)."
    author: "Maarten ter Huurne"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "I don't have it either i'm useing the mandrake 7.2 rpms. What are you useing?\n\nCraig"
    author: "Craig black"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "It looks like KDE determines at compile-time whether your Qt supports AA or not.  If it doesn't, then the code to handle AA is not compiled in.  The good news is that you can just get the source code for kcontrol and reconfigure then recompile/reinstall from the kdebase/kcontrol/display directory.  Or complain to the person who made your packages."
    author: "KDE User"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "Thanks....<br>\n\nI just looked at the code.  And it looks like the GUI to control this behavior is conditionally compiled.  But I also found that I can add the following to my kcmdisplayrc:<br>\n\n[KDE]<br>\nAntiAliasing=true<br>\n<br>\n\nBy adding this, I can now view AA with KDE."
    author: "Kevin Pearson"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "Add the following to your /etc/profile or /etc/profile.local or ~/.profile:<br>\n<pre>\nexport QT_XFT=true\n</pre>"
    author: "Nadeem Hasan"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "I added it to my /etc/profile.d/qt.sh file.  But KDE seems to be setting QT_XFT back to 0."
    author: "Kevin Pearson"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-10
    body: "I got it to work!  Yeah!  Anti-aliased fonts.  I'm running on a laptop (Toshiba 2805-S202, which, by the way, is a GREAT Linux laptop, and, no, I don't work for Toshiba) and I was wondering, how can one set the anti-aliasing to do sub-pixel rendering instead?  I've heard rumors that this was possible.  Has anyone tried it?\n\nIn the meanwhile, I copied all my Windows fonts (Windows has great fonts.  I knew it had to be useful for <I>something...</I>)\n\nErik"
    author: "Erik Hill"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "the guy who discovered that is just a few miles away from me, and if you've ever seen the example for Windows, it is truly stunning.  I asked about this in the kde-devel list and was told they already had the ability to do it (if I understood correctly), but I'll tell you what, if you can get that working with KDE and go to do a demo on a laptop, people will just be stunned."
    author: "Shawn Gordoon"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "hmm what is sub-pixel rendering?\n<p>\nThanks\n</p>"
    author: "Jason Katz-Brown"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "Add this to your ~/.Xdefaults:<br>\n<pre>\nXft.rgba: rgb\n</pre>"
    author: "Nadeem Hasan"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "It works GREAT!  Just beautiful.  Thank you.\n\nWow.  Sub-pixel rendering under Linux, with every single piece of text I see being rendered correctly.  Now all we need is:\n\n#1 Better hinting under FreeType\n#2 Better open fonts\n#3 A nice dialog to set this all up under the control panel, in the fonts panel.\n\nI don't know how to make fonts, but I could code the GUI, but I assume someone else has already thought of this and is coding it, or has coded it.  Meanwhile, I've been thinking about anti-aliased icons, which should be easier, because they can be just rendered statically, as in, not on the fly.\n\nErik"
    author: "Erik Hill"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-11
    body: "THE RELEASE SONG IS GREAT !!!!"
    author: "fgh"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-15
    body: "For 2.4 TrollTech should do a parody of the \"All Your Base Are Belong To Us\" song. Ah, I can see it now : \"All your widgets are open sourced by us\""
    author: "Carbon"
  - subject: "Has anyone got it to work with Nvidia 0-96 drivers"
    date: 2001-03-11
    body: "It seems, that Nvidias Drivers don't yet support AA. Any other experiences?\n\nGeorg"
    author: "Georg Oberwinkler"
  - subject: "Re: Has anyone got it to work with Nvidia 0-96 drivers"
    date: 2001-03-11
    body: "0.9-7 will (or at least the developer of the NVIDIA driver once claimed)\n\nIf you need AA right now, use XFree's 'nv'."
    author: "Flying Fish"
  - subject: "Re: Has anyone got it to work with Nvidia 0-96 drivers"
    date: 2001-03-12
    body: "9.7 should be out shortly. They told me last week that it would be out in a week or so. maybe we'll get the drivers this week I hope.\n\nCraig"
    author: "Craig black"
  - subject: "Re: Has anyone got it to work with Nvidia 0-96 drivers"
    date: 2001-03-13
    body: "Duh,\n\nthanks for this reply, I was just searching for alternatives to my Geforce2 MX, since <a href=\"http://trolls.troll.no/~lars/fonts/qt-fonts-HOWTO.html\">http://trolls.troll.no/ ~lars/fonts/qt-fonts-HOWTO.html</a> is writing that Nvidia doesn't support them yet.\n\nAnother week of waiting\n\nGeorg"
    author: "Georg Oberwinkler"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-12
    body: "Hi,\n\nI have a dualHead G450 with 2 monitors. Monitor 1 gives anti-alias fonts ok, but on screen 2 i don't see any fonts..\n\ncan somebody help me ?\n\ntanx"
    author: "Herwin Jan"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-12
    body: "The RENDER extension wasn't Xinerama-ified until recently. Grab the CVS or wait for 4.1.0"
    author: "Sam Rowe"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-13
    body: "I did yesterday, now the text is show, but with strange pixels around it, so it is not 100% working ;-(\n<BR>\nbut i will wait ;-)\n<BR>\ntanx"
    author: "Herwin Jan"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-12
    body: "Need KDE (which packages) to be recompiled in order for the antialiasing to work?"
    author: "Tomasz Jarzynka"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-12
    body: "Recompiling KDE is not necessary. Read the comments above to activate the \"use AA\" config option if you have KDE 2.1.\n\nYou do have to recompile Qt and XFree86. I found out today that just having the RENDER extension in your X server is not enough, /usr/X11R6/lib/libXft.so.1.0 must be linked to FreeType2 for anti-aliasing to work (use \"ldd\" to check). The XFree86 release notes tell you how to compile XFree86 so that Xft is linked to FreeType2."
    author: "Maarten ter Huurne"
  - subject: "Re: Qt 2.3 released: I can see clearly now"
    date: 2001-03-12
    body: "Need KDE (which packages) to be recompiled in order for the antialiasing to work?"
    author: "Tomasz Jarzynka"
  - subject: "NVIDIA driver, QT 2.3, XFree86 4.0.3 and KDE 2.1"
    date: 2001-03-26
    body: "I have absolutely know idea how I'm going to get this to work...\n\nI have the sources for QT 2.3, XF 4.0.3 and KDE 2.1 and I'm running NVidia release 0.9-769 (xdpyinfo shows RENDER).\n\nCompiled XFree86, QT (with Xft support) and KDE 2.1...\n\nNow, drkonqi gives me \"floating point exception\" and none of my KDE apps work!\n\nIf I ssh from a remote machine, KDE apps run fine!\n\nxterm -fa arial works fine!\n\nAAAARGH! Help!\n\nJan Gutter\n\n\"-and those damned stupid barbarians with their\ndamned stupid swords will win after all...\"\n-- Larry Niven (The Magic Goes Away)"
    author: "Jan Gutter"
  - subject: "prova da Suse  trx "
    date: 2003-01-08
    body: "\tciao dani  ti mando st\u00e0 prova di trx come sempre x testare \n    la nuova installazione d i susevrrmar"
    author: "danile"
---
<a href="http://www.trolltech.com/">Trolltech</a> has <a href="http://www.trolltech.com/company/announce/qt-230.html">announced</a> the release of Qt 2.3.0, the much-anticipated version of Qt that supports anti-aliased fonts (assuming your X server is up-to-date). <i>"KDE 2.1 automatically takes advantage of Qt's support of anti-aliased fonts. The result is one of the nicest looking user interfaces in the world."</i>  Along with anti-aliasing, Qt 2.3.0 offers True Type and Type 1 font support for printing, and a few other <a href="http://www.trolltech.com/developer/changes/2.3.0.html">improvements</a>.  The release was accompanied by an amusing <a href="http://www.trolltech.com/company/announce/qt-230audio.html">soundtrack</a>. Finally, of note to those of you struggling with fonts, is the <a href="http://trolls.troll.no/~lars/fonts/qt-fonts-HOWTO.html">Qt font HOWTO</a>, made available by <a href="http://lists.kde.org/?l=kde-devel&m=98416713716076&w=2">Lars Knoll</a>.





<!--break-->
