---
title: "City of Largo Adopts KDE 2.1.1"
date:    2001-07-24
authors:
  - "Dre"
slug:    city-largo-adopts-kde-211
comments:
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "As a Florida transplant currently living in Atlanta - I'm proud to see Florida taxpayer dollars put to such excellent work.\n\nMaybe we could fenagle him to write a how-to?  I'm sure his experiences would be invaluable to any corporate sysadmin looking to justify and show all the benefits of KDE+ / *NIX and the significant ROIs these types of installs have in the workplace."
    author: "Native Floridian"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-25
    body: "I think this is excellent.  If he would work up a how-to I'm sure it would be put to EXCELLENT use.  I've been pondering selling this idea to school districts and counties in my area as well, but have lacked the sufficient precedent and procedure.  Excellent work!!!  Its great to see the state government embrace superior technology."
    author: "Ohioan"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-08-14
    body: "It's easy to impress a group of users familiar with unixware on Xterms.  Good luck selling this to users addicted to Windows."
    author: "Dan Evenson"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "I'm not going anywhere near this place since that largo embargo."
    author: "Guybrush Threepwood"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "Look behind you !!\n\nA Three-headed monkey !!"
    author: "Sith"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-25
    body: "How much wood would a wood-chuck chuck if a wood-chuck could chuck wood?"
    author: "Wally"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-08-13
    body: "A wood chuck would chuck as much wood as a wood chuck could if a wood chuck could chuck wood."
    author: "Some Dude"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-08-14
    body: "I`m rubber, you are glue\n\n\n(sorry, couldn`t resist :)"
    author: "cHALiTO"
  - subject: "Office or day-care?"
    date: 2001-07-24
    body: "<<We would like the ability to turn off the feature that lets users pick their own wallpaper>>\n\nAre you serious? You really don't trust the employees enough to choose an appropriate background image? I have to wonder if you're creating an environment where people with integrity will not want to work."
    author: "kdeFan"
  - subject: "Re: Office or day-care?"
    date: 2001-07-24
    body: "Even people with integrity may not realize that their choice of decor is impacting other users via increased bandwidth usage (at least during first connection to the shared system). There are also the issues with 8-bit display that are mentioned in the article.\n\nMaybe the users should actually have windows open with actual work in them rather than staring at their pretty desktop all day."
    author: "DCMonkey"
  - subject: "Re: Office or day-care?"
    date: 2001-07-24
    body: "The reasons you mention may be legitimate, but he's talking about approving the content of all the images to make sure they're \"politically correct\". That's a different issue than what you're addressing."
    author: "kdeFan"
  - subject: "Doh!"
    date: 2001-07-24
    body: "You're right! That'll teach me to get lazy and just skim the end of the article. :)\n\nFWIW, we let our users pick their own wallpaper."
    author: "DCMonkey"
  - subject: "Re: Office or day-care?"
    date: 2001-10-27
    body: "You forget that these are images being offered by the CITY, and therefore must be acceptable to anyone. Thus the need to be \"politically correct\" is there not only because it is required by other employees who may not want to see a half naked man/women or some other ethnicly/politicly/religious/* charged image when they walk by your terminal... \n\nI know quite a few people would be upset if I were to have on my computer the picture by Andres Serrano \"Piss Christ\" as my background much less offer it so other people would see it when they were choosing their own back ground... \n\nbut it's a CYA effort as well.(with good reason)\n\nFYI - Serrano's Piss Christ is a 1987 photograph of a crucifix immersed in urine. In the late-1980s, the work prompted an intense national debate over government funding of the arts.\n\nYou can find a copy of it at:\n\nhttp://www.usc.edu/schools/annenberg/asc/projects/comm544/library/images/502.html\n\nIt's not really that bad if you don't know why the picture is yellowish...\n \n\nHyrcan\nthe happy godless heathen"
    author: "hyrcan"
  - subject: "Re: Office or day-care?"
    date: 2001-07-24
    body: "How does a wallpaper impact anything? Does it lean out from the screen and hit the user? Now, I can understand that it can have an <b>affect</b> ..."
    author: "Mike Richardson"
  - subject: "Re: Office or day-care?"
    date: 2001-08-13
    body: "> Now, I can understand that it can have an\n> affect\n\nITYM \"effect\".  HTH."
    author: "Pedant"
  - subject: "Re: Office or day-care?"
    date: 2001-08-14
    body: "I presume you meant *EFFECT*"
    author: "Kwantus"
  - subject: "Re: Office or day-care?"
    date: 2001-08-14
    body: "Gee, I thought that that was one of the best things about linux is that you can have windows (rxvt's?) open with actual work in them <strong>as well as</strong> stare at their pretty desktop all day (in their transparent background)."
    author: "Krishna Sethuraman"
  - subject: "Daycare by choice..."
    date: 2001-07-24
    body: "I agree, I would rather be able to choose my own wallpaper.  OTOH I really don't think that KDE (or any other software) should make moral decisions that piss my boss off... I usually do that myself anyway.\n\nI would guess that things like wallpaper freedoms (however trite) could be serious legal offenses that would automatically force potential markets to overlook KDE.\n\nAnd plus, lay off him, he's a sysadmin who has do deal with stupid questions like \"Can't we turn off their desktops?\" from the PHB and \"Why does everything I put in 'Trash' disappear?\" from co-workers. I say keep on rockin those admin skills all over Largo! \n\nNow if only I can get the sysadmin here at school would use KDE rather than it's \"common\" predecessor (yuck!)."
    author: "Eric Nichlson"
  - subject: "Re: Daycare by choice..."
    date: 2001-07-24
    body: "I really wasn't taking a position on whether the feature shold be implemented... I was merely surprised that his place of employment feels the need to check each background image for political correctness. And you're right, he's just the sysadmin not the policy maker. I didn't mean to come down on him."
    author: "kdeFan"
  - subject: "nothing to do with \"political correcntess\""
    date: 2001-07-24
    body: "It's not \"political correctness\", it's a support issue: background images run the server out of color map entries.  Apparently, even in this forum, many people are unaware of the problems that background images cause on 8bit screens."
    author: "Mike"
  - subject: "Re: nothing to do with \"political correcntess\""
    date: 2001-07-24
    body: "If you read the first point under the \"Configuration\" heading, you'll notice that political (in)correctness was his stated reason for wanting users to choose from an approved list. He does want to turn off wallpapers on 8-bit displays - that's a different issue. \n\n<<Apparently, even in this forum, many people are unaware of the problems that background images cause on 8bit screens.>>\nThat's not what I'm talking about, and there's no need to be pretentious.\n\nI think my first post was unclear and badly worded. I really didn't mean to suggest that the feature requested doesn't have legitimate uses; I was just surprised that his employer would have such a policy in place. Others have mentioned the possibility of litigation as being the cause of the policy, which is probably accurate. It seems odd to hold the company liable for individuals' actions  so long as management deals with infractions properly as soon as they are brought up, but I'm not a lawyer. And I prefer to treat people as adults until they prove that they can't handle it."
    author: "kdeFan"
  - subject: "Re: nothing to do with \"political correcntess\""
    date: 2001-07-24
    body: "The thing is that where I'm currently working there were people who showed that they could not handle it. The sad thing about this is that there are most likely othere places where this aplies too. So I support the feature to be able to turn of walpapers.\n\nNow I just have to find a way to swap the NT/Citrix servers with Linux/KDE/Citriux servers..."
    author: "Jo \u00d8iongen"
  - subject: "Re: nothing to do with \"political correcntess\""
    date: 2001-08-14
    body: "You touch the sore point.\nIn the US of A there is hardly privacy in the company computer you're using, just have a look at the snooping discussions.\nAnd this is another reason to declare the working environment in the computer private.\nThat way it becomes so much harder for a 'fiend of his/her own sex' to sue the company/city for what a single employe does on his computer.\nOnly the results of your working day/project should count."
    author: "Teun"
  - subject: "Re: Daycare by choice..."
    date: 2001-07-24
    body: "> I was merely surprised that his place of employment\n> feels the need to check each background image for\n> political correctness.\n\nFor better or worse, there are laws regarding this, and an employer (like the City of Largo) can get sued.  E.g., if someone is displaying a pornographic image, a woman visiting city offices may be offended.  Or someone may have a racist background.  The point is that, particularly when you are the government, you have to be careful that the government computers do not have images that do not discriminate on the basis of age, sex, race, national origin, religion, sexual preferences (in some states), etc.  Again, this is a legal requirement, so don't blame sysadmins :-)."
    author: "Dre"
  - subject: "Re: Daycare by choice..."
    date: 2001-08-14
    body: "You are 100% correct. The display of \"inappropriate\" images has cost both employer and employee tons of money. I have a friend in the HR department for the State of CA who has plenty of such horror stories. This stuff was popular in email and on screen savers was popular until a few folks lost their ($80k +) jobs over it. Now everyone seems happy to display Little League pix, Scenic Landscapes and Disney characters.\n\nI'd like to think that adults in the workforce have more sense than to exchange porno via e-mail and use obnoxious screen savers / desktops,but the evidence simply won't permit me to reach that conclusion without first resorting to the use of psychotropic substances.\n\nThere is no requirement that the City of Largo permit any sort of desktop customization. I think they should be commended for even permitting it ... and on Linux, at that!\n\nIt seems to me that asking users to submit proposed pix for approval is only a reasonable precaution and should inconvenience only dyed-in-the-wool jerks. My guess is that they end up rejecting very few pix since the employees will not want to submit the more offensive ones for approval to begin with."
    author: "Bill"
  - subject: "Re: Office or day-care?"
    date: 2001-07-24
    body: "The issue is running out of color map cells on 8bit displays.  If people pick a wallpaper, they'll run out and applications will start to malfunction.  That causes tech support headaches, since most users have no idea why things are falling apart.\n\nThese are business computers; it really isn't such an intrusion to expect people not to set a wallpaper, in particular if it causes support problems."
    author: "Mike"
  - subject: "Re: Office or day-care?"
    date: 2001-07-24
    body: "I think my first post must have been very unclear. I was attempting to address his statement that without the requested feature he would have to check each user's wallpaper for political correctness. His remedy for this was that people should be able to choose *only* wallpapers on an approved list. As far as color depth goes, he suggested that wallpapers be turned off on 8-bit displays, which is a separate issue.\n\nAs far as turning off wallpapers goes, I think that's a perfectly acceptable solution to a technical issue. For that matter, I think having an approved list of wallpapers isn't such a bad idea either. I was just surprised that he would he would have to check every user's background image(s) for PC compliance under the current scheme. It seems to me that showing so little faith in the judgement of the employees sends a bad message."
    author: "kdeFan"
  - subject: "Re: Office or day-care?"
    date: 2001-07-25
    body: "The problem is a legal one. In the new America,\nfreedom exists only if it does not offend and\ncompanies end up being the target of law suits.\n\nThis is a government agency. Trust me; if they\nhave been there more than 5 years, they are\nprobably too rooted to consider coming out into\nthe real marketplace. Then again...the DOD and\nLargo are using their money more wisely than\nmost businesses."
    author: "Charles Stepp"
  - subject: "Re: Office or day-care?"
    date: 2001-08-13
    body: "It's silly, but I can understand it being an issue.  It's probably easier for them to vette wallpapers than too deal with the occasional person getting <b>freaked</b> at the 'inappropriate' image on their computer. Remember that some of the terminals are <b>very public</b> and they follow a user around... \n \n Something that's find on the computer in my office could cause a firestorm if I logged in to a semi-public screen to check something for a minute -- with 15 visitors from the feminist action league having a cabal session 12 feet behind me.\n\nMy guess is that 95% of the pictures submitted would get about 2 seconds viewing before they were put into the public area, where they would then be available to everybody. Hmm... I wonder if they'd then be willing to make the collection publicly available? It'd be a wonderful resource."
    author: "Stephen Samuel"
  - subject: "Re: Office or day-care?"
    date: 2001-08-13
    body: "I have to agree with this one. When we first initiated the pr0n alarm at a company I was working, it was getting set off several times a day. I like to think people are smart enough not to do that sort of thing at work during business hours, but the fact remains that they do. I went back as a consultant 6 months later and the VPs son (who was working tech support) was in pr0n chat rooms while at work. Sad really."
    author: "jpostel"
  - subject: "Re: Office or day-care?"
    date: 2001-08-13
    body: "No kidding. Businesses should just make rules and give the employees the choice to break them or not. If you decide to break the rules, then disciplinary action is taken. You don't have to make your sysadmin into the desktop Nazi for something that is ultimately the responsibility of the individual."
    author: "gromm"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "Wow, that 11-12 MB per user surprised me.  I guess X and shared libs are what is taking all my computer's RAM.  How does that compare to the KDE 1 system that you guys had before?\n\nAlso, isn't it already possible to turn off things like the Quick Browser?  I'm sure I saw that somewhere..."
    author: "not me"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "Libraries and programs are shared between all users. 11MB is for users data. There shouldn't be much difference in memory usage if you keep everything on a single server."
    author: "Krame"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "Yeah, I know.  That's what I meant.  The actual data stored by KDE in RAM is less than I thought, all the other RAM that's being taken up on my computer is from X, shared libs, and program binaries.\n\nActually, now that I think of it, 11MB is kind of a lot of data.  That's roughly 11,000 screenfuls of text.  I wonder what makes up that ~11MB per user storage space?  Perhaps knowing what is in there could give some clues as to how to increase KDE's memory efficiency?"
    author: "not me"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-08-14
    body: "I wonder what makes up that ~11MB per user storage space?\n\nImported .doc and .xls files, what else?"
    author: "Slashgeek"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "Yes, it is most definetly possible to turn off the things in the menu you wanted turned off, except for \"Run Command\". It's in the kicker settings area in the Control Center, under the Menus tab.\n\nHmm, as for Kicker autorefresh, I have no idea why that should take so long. Doesn't kicker simply watch it's desktop file directory using the KDirWatcher class (sp?), which iirc uses the kernel to watch for processes. Could someone check this?"
    author: "Carbon"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "I think the problem with Kicker is that it check for changes periodically and with 200+ people accessing the network at once, it creates a huge bottleneck. Maybe if there is some small amount of randomness in the timing, it would be less of a load on the network."
    author: "foobar"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "For restarting kicker, what about\ndcop default kicker restart?"
    author: "georg"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-25
    body: "Not really randomness, but with the following lines in sharec/config/kdedrc you can tell it how often to poll.\n\nE.g: the following configures local fs once 0.5 seconds and non-local FS once each 5 seconds, which also happens to be the default.\n\n[General]\nPollInterval=500\nNFSPollInterval=5000\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-25
    body: "Yes, the autorefresh uses KDirWatch, so he should simply install libfam and reconfigure, to get rid of the polling.\n\nThe kernel support called dnotify is currently disabled again, 'cause not working properly yet."
    author: "David Faure"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-08-14
    body: "\"The kernel support called dnotify is currently disabled again, 'cause not working properly yet.\"\n\nActually, the preferred kernel support for fam is called imon, and can be obtained from\n<URL:http://oss.sgi.com/projects/fam/download/>.  There are others who are doing work with fam/dnotify rather than fam/imon since dnotify has made it into the standard kernel sources.\n\n-- \nMICHAEL WARDLE\nSGI FAM Maintainer"
    author: "Michael Wardle"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "> I had to put in code to redirect the sound to\n> the thin clients. By default, KDE tries to\n> send them to a sound card [..]\n\nbtw. the last time I checked out arts network transparency it did not work. I've a small network here running 5 clients with KDE2.1.1 (real PCs ... no X-terminals...) and I'd like to redirect the sound from the machines wo sound card to the only one with sound card."
    author: "Thomas"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "Just curious about your use of rsh..\n\nDo your users log into your servers using XDM,\nso the full X session is from the server, with\nonly the X server running on the client box?\n\nIf that is the case, why is rsh needed?\n\nCheers,\n\naid"
    author: "Adrian Bool"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "Maybe the applications are on another machine.\nThat would make sense."
    author: "Sheldon"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-25
    body: "Yeah, he explains that the apps are hosted all on different machines, thanks to the wonders of the X Window System.  The one big server runs just KDE, only KDE, all the time."
    author: "not me"
  - subject: "How about LTSP?"
    date: 2001-07-24
    body: "Does anyone have experience running KDE on the Linux Terminal Server? What about large-slace deployments? Looking at the text seems to suggest that this project does not use LTSP."
    author: "Janne"
  - subject: "Re: How about LTSP?"
    date: 2001-07-24
    body: "I've used KDE2 with the xterminals (solucorp) package.  This is similar to the LTSP.  I had only minor issues.  I have 30 terminals, two app servers, one file server, and one machine doing misc stuff (NFS, NIS, etc).  Based on the load, I could easily triple my number of clients, however I would need to be more creative with my network design to handle the traffic."
    author: "TJ"
  - subject: "Re: City of Largo Adopts KDE 2.1.1"
    date: 2001-07-24
    body: "To restart kicker:\n\ndcop kicker Panel \"restart()\" \n\nSheldon."
    author: "Sheldon"
  - subject: "rsh and sound problems."
    date: 2001-07-24
    body: "We run a similar (but much smaller) shop here: dumb X terminals running off a main application server with a KDE 2.X interface. It is working very well, but we use a different approach.\n<br><br>\nWe use Jacques Gelinas' (of Linuxconf fame) diskless XTerminal kit. The kit does not use a \"boot up a terminal and run applications in a server\" approach. It uses a \"boot up a terminal and transparently log in to the server\" approach.\n<br><br>\nSo starting konqueror from the command line works as expected. The only small hitch is that we have to use a VFS to give applications access to the terminal's disk drive. And some applications have a problem with that.\n<br><Br>\nBut overall, it is a very well-designed, simple solution. I recommend it to everyone who has the same needs.\n<br><br>\n<a href=\"http://www.solucorp.qc.ca/xterminals/\">http://www.solucorp.qc.ca/xterminals/</a>\n<br><br>\njerome at levinux dot org"
    author: "Jerome Loisel"
  - subject: "Re: rsh and sound problems."
    date: 2001-07-25
    body: "Yes, using 'rsh' is somehow strange. I suspect that they want to run some applications ( 'Kicker' and windowmanager ) locally, but some apps ( office ) remotely."
    author: "Ups"
  - subject: "Network traffic"
    date: 2001-07-25
    body: "How about the network traffic. Is it a normal 100Mbit ethernet connection? Can one nic handle so much traffic (X, nfs, sound, surfing,...) ?\n\nBram."
    author: "brambi"
  - subject: "Re: Network traffic"
    date: 2003-07-04
    body: "I have used Envisex terminals with Xwindows over 10Mb LAN links to hp-ux boxes with no probs. This was at a large international company about four years ago.  We used them for a number of server-based apps for specific tasks including a pretty chunky help-desk tool, about 200 users concurrently, with unrestricted surfing (Netscape Navigator), email (good ol' OpenMail) without any quotas, and all under CDE.  In my experience, you only need 100Mb in the office if you are doing a lot of large file transfers or have Windoze pushing out NetBUI and the like   ;)\n\nBizarrely, the IT department decided to put a Windoze desktop on each desk to allow us to use Office, and then replaced the help-desk tool with a suite of NT server-based tools.  Shortly after Openmail was replaced by Exchange   :( \n\nWhilst the new tools allowed us to do more, I shudder to think of the increase in cost.  The net traffic was still easily coped with over 10Mb, even with the massive increase due to the extra and bloated emails, file \"sharing\", and inevitable worms!\n\nThe final irony was the IT department was then outsourced to reduce costs!!!  Any IT admins out their might like to think about that the next time they are asked about the possibility of running a thin-client network as opposed to Windoze desktops  :D"
    author: "harmless"
  - subject: "wow"
    date: 2001-07-25
    body: "Thanks for your efforts Dave.  This is amazing.  It's impressive how one individual like you made a difference by deploying Linux/KDE.\n\nI hope the KDE project learns from your experience and makes the next version of KDE even more suitable for the enterprise.  \n\nA synergy has started here, let's keep it well and alive!"
    author: "KDE User"
  - subject: "Dave Richard's e-mail"
    date: 2001-07-25
    body: "Can anyone tell me Dave Richard's e-mail account so that I can ask him how did he accomplish such wonderful task?"
    author: "Fabricio Abreu"
  - subject: "Re: Dave Richard's e-mail"
    date: 2001-07-25
    body: "drichard@largo.com\n\nGood luck!"
    author: "KDE User"
  - subject: "Responses"
    date: 2001-07-25
    body: "Our announcement to kde-devel has certainly gotten a lot of emails and comments.  I appreciate the suggestions and thoughts.  I haven't seen any people doing things the same way, because more people set up departmental servers, but instead we are running application servers.  Each of our servers performs one function.\n<br><br>\nI read all of your comments, and will respond to the questions asked.\n<br><br>\n* Yes, I would love to help with a detailed how-to guide for this kind of rollout.  The cost savings are just amazing.  Anyone buy NT licenses yet for thin clients?  They have dropped all concurrent licenses, and it's going to cost anyone that wants to use NT a fortune.  Thankfully, we have always been mostly a Unix shop where concurrent prices are the norm.  Centralized NT only supports about 40-50 on the same hardware that we can get hundreds on with Linux/Unix.  \n<br><br>\n* Wallpapers.  Anyone that has worked for Government understands our concerns.  Not only can we get sued, but all it takes is one newspaper reporter coming in and seeing something, *anything* with which to write an article to sell papers.  \"City Employee Uses Taxpayer Dollars To Have Offensive Background\".  The 8 bit color thing is another issue, non-technical people have no idea what they are doing to their color cells.  Wallpapers come up *first*, so your 256 colors are gone before any software is started.  Also, people were picking wallpapers stored in NFS mounted drivers.  It is unknown what happens to KDE when it tries to get wallpaper from an NFS mount that is down for whatever reason (reboot, etc). It might just *hang* and wait.  Anyone ever try and do an ls in a mount that is down? Or 'df'?\n<br><br>\n* The 11MB of memory thing was good news for us.  Being that memory has dropped so much, we can certainly accomodate that per user.  Sticks of 1GB are about 1500 dollars, very cheap.  KDE 1 running on Unixware was only a partial deployment in order to get them used to KDE for a few years until we installed this version.  We only ran the kpanel only, window management came from the terminals.  In KDE2, we are using 100% of KDE, window management is being done on the host.\n<Br><Br>\n* NAS is the sound system being used.  NAS is client/server and works almost the same way that Xwindows does.  The guys at NCD did a great job with it.  It's available for free now.  I know that some people have looked at doing a Win32 port of it (another question asked).  I would suggest getting on the NAS list server and if you want to try and port it, that would be great.\n<br><Br>\n* Network performance is fine, we are hardly using any of our network.  The network really should be a major part of a white paper we make one.  Xwindows needs realtime access to the servers, and some switches do store-and-forward which is tested and designed for client/server only.  What it does is hold stuff for a good time to send them, which kind of doesn't work well when you are typing things and the keystrokes are going back and forth.  ;)  We have a Gig backbone, and fiber was run to all of the closets in the city, and then each closet has switched 3com 3900 devices.  We don't have any hubs in the whole city.  The switches automatically hide all of the broadcasts between switches.  NAS works in realtime. In fact, we run the Realplayer over the network, and the sound and video are in sync.\n<Br><Br>\n* We use rsh because the big server only does KDE.  One of the biggest things that people complain about with 'mainframes' is that if the mainframe is down, everything is down.  So they way that you get around that is that you build applications servers.  If one machine is down, or being rebooted, only one option from the Kpanel is inactive.  Everything else works.  Okay, so WordPerfect is down for 10 minutes, I'll read my email or check something on the web.  From the user perspective, it looks like one big machine, but each icon choice is calling a different machine.  The rsh sends the signals over to that machine to initiate a session and check to make sure that user has permissions to run that software.  NT with Citrix accepts rsh commands and can start up software that way in an Xwindow via the UIS.  This is really cool because it turns off the Windows Start bar and all options of NT.  Only the software requested via the rsh is started.\n<Br><br>\nMy very old presentation at SCO Forum 99 is still out there-->\n<br><Br>\n<a href=\"http://www.sco.com/skunkware/largo/\">http://www.sco.com/skunkware/largo/</a>\n<br><Br>\nWe have been running this design for about 7-8 years now, but the power of KDE has certainly given us a lot better front end for our users.\n<Br><br>\nRegards!\n<br><br>\nDave"
    author: "Dave Richards"
  - subject: "Re: Responses"
    date: 2001-07-26
    body: "I was wondering how you managed to get 230 users working from only one application server. Dividing the workload is indeed a very good idea.\n\nHad we had such a big client base going, we would probably still have used the XTerminal kit I referred to earlier. But we would still have divided it like so.\n\nNFS + NIS server\n... serving ...\nA few application servers\n... each serving ...\nA bunch of XTerminals\n\nWe tremendously enjoy the simplicity of the XTerminals kit. But your setup is definitely both tried-and-true and extremely cool.\n\nCheers and congratulations,"
    author: "Jerome Loisel"
  - subject: "PC script"
    date: 2001-07-26
    body: "\"What is happening is that people are installing their own wallpapers, and now I'm going to have to write a script to find all of them that are in use and check to make sure they are 'politically correct' in the workplace.\"\n\ncould you please post the PC-script, would be interesting to see ;-)\n\n*SCNR*\n\nanyways: great deal!\n\nRegards,\nDD"
    author: "zapalotta"
  - subject: "Why are you telling KDE users"
    date: 2001-08-16
    body: "This is a great story, and I'm just itching to try implementing some sort of similar system at my school as well, if the monks there are at all interested.\nbut that's beside the point. This article (or maybe a flower tabbed suit-readable version sould be posted all over the place (i.e. newsites, etc). Also that would give me and others like me more written material to be used for petitioning governments and schools to use this superiour technology.\nI'd write something myself, but I honestly don't have the time right now. I am currently using a  city library computer, and windows has to be resarted faithfully every fifteen minutes. This is just one of the many places that desperatly needs to get rid of the curse."
    author: "Gavin"
  - subject: "user-restrictions"
    date: 2001-11-04
    body: "The KDE Kiosk Mode Howto at http://www.brigadoon.de/peter/kde/\nhas some patches to block certain functionality for users"
    author: "Admin"
  - subject: "why thin client?"
    date: 2002-09-03
    body: "www.ltsp.org\n\nany old pc with minimum 16mb of ram, no hard disk, no cd, no floppy, just a bootable eprom from rom-o-matic.net or pxe support and u got dummy terminals with ALL the apps on the server running locally."
    author: "Mohammed Arafa"
---
Dave Richards, Systems Administrator for the
<A HREF="http://largo.com/">City of Largo</a>, Florida,
has just
<A HREF="http://www.kde.com/maillists/show.php?li=kde-devel&f=1&l=20&v=list&mq=-1&m=501676">announced</A>
that the City of Largo has switched to KDE 2.1.1 as their
production system.  According to Dave, <EM>"The City of Largo is a thin
client/X shop [which supports] 400 thin client
devices that support X, 800 total users, and run about 230 concurrently
during the heaviest part of the day."</EM>  Dave shares his experiences and
problems in getting the system working below, and happily concludes that <EM>"the cutover has gone <STRONG>really</STRONG> well"</EM>. <b>Update: 07/25 8:25 PM</b> by <a href="mailto:navindra@kde.org">N</a>: Dave answers your questions in <a href="http://dot.kde.org/995949998/996088741/">this article</a>.  You can also check out his <a href="http://www.sco.com/skunkware/largo/">old presentation</a> at SCO Forum 99.

<!--break-->
<P>&nbsp;</P>
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>
<TR ALIGN="top"><TD ALIGN="right"><STRONG>Date</STRONG>:&nbsp;&nbsp;</TD><TD>Mon, 23 Jul 2001 15:09:14 -0400</TD></TR>
<TR ALIGN="top"><TD ALIGN="right"><STRONG>From</STRONG>:&nbsp;&nbsp;</TD><TD>Dave Richards &lt;drichard@largo.com&gt;</TD></TR>
<TR ALIGN="top"><TD ALIGN="right"><STRONG>To</STRONG>:&nbsp;&nbsp;</TD><TD>kde-devel@kde.org</TD></TR>
<TR ALIGN="top"><TD ALIGN="right"><STRONG>Subject</STRONG>:&nbsp;&nbsp;</TD><TD>City of Largo Goes Live With KDE 2.1.1</TD></TR>
</TABLE>
<P>
Developers-
</P>
<P>I am not on the kde-devel group, this message is being sent from
the outside and if anyone wants to contact me, they should do so directly.
I posted a message a few months ago about the City of Largos intentions to
go live with the KDE desktop and as of Friday that transition is complete.
I thought the cutover, and some of the things that I had to do would be of
interest to all of you.  I also want to thank every person that worked on
this project because it has offered us a solution that has fit perfectly
into our technology and design goals.
</P>
<P>
<H4>Technology and Design Goals</H4>
</P>
<P>
The City of Largo is a thin client/X shop.  We have 400 thin client
devices that support X, 800 total users, and run about 230 concurrently
during the heaviest part of the day.  For the last 7 years, we have always
built one large 'desktop' system that everyone logs into and gets their
desktop.  All of the icons do a 'rsh' to other systems and then start up
the various software packages that we have in the City.  For instance, an
icon will say "WordPerfect" and the command executed is "rsh oa1
/usr/local/bin/sh_wordperfect $DISPLAY".  Previously, this function was
done by the IXI Desktop on SCO OpenServer, and then later we ran KDE 1 on
OpenServer, then Unixware.  The Friday cutover was moving all of these
users off of Unixware to RedHat Linux 7.1 and KDE 2.1.1   Because only KDE
is running on this machine, the performance is a good look at how KDE
scales.
</P>
<P>
<H4>Cutover &amp; Runtime Issues</H4>
</P>
<P>
The cutover has gone <EM>really</EM> well.  Most of the issues that we
have had have been people issues and not technology issues.   I have been
keeping notes and the issues that came up are noted below:
</P>
<UL>
<LI>The server we built is a dual-933 with 3GB of memory, and 18GB of disk,
this goes for about $9500 USD.</LI>
<LI>It seems that we settled into about 11-12MB of memory per user on the
server.  3GB of memory has allowed us to get the 230 concurrent users
logged in without using the swap space.</LI>
<LI>The dual-933 runs about 5-30% busy during the day with 200+ users on.
Most of the load seems to come from using the Pager and window management.
</LI>
<LI>When a large amount of users are cut over, there is a spike the first few
days while everyone customizes their desktops.  Everyone pokes around in
the wallpapers and colors and fonts until they are comfortable.  And then,
people tend to not change them often again.  It's best to move people over
in groups of about 20-30 at a time and then let them configure and then
bring over another group.</LI>
</UL>
<P>
<H4>Custom Changes</H4>
<P>
I had to make some changes to startkde to accomodate our thin
clients.
</P>
<UL>
<LI>I had to put in code to redirect the sound to the thin clients.  By
default, KDE tries to send them to a sound card in the server itself (which
is sitting in the computer room). ;)   Perhaps at some point, KDE could
allow a plugin system to be deployed that would allow the administrator to
select the desired sound system.  For those using NAS (Network Audio
System) the web page for that project and the redirection patch link is
<A HREF="http://radscan.com/nas.html">here</A>.</LI>
<LI>8 bit color really causes some problems.  Non-technical users cannot
understand color allocation and how once cells are used, applications begin
to not function correctly.  I installed a dialog box that tells them they
are on a low color terminal--&gt;
<BR>
<TT>
<PRE>
DEPTH=`/usr/bin/X11/xdpyinfo | grep "depth of root window:" \
      | awk -F " " '{print $5}'`
case $DEPTH in
        8)
                /usr/local/bin/sh_8bit &
        ;;
esac
</PRE></TT></LI>
<LI>I also turned off ksplash for low color users, to help cut down
on usage--&gt;<BR>
<TT>
<PRE>
# the splashscreen and progress indicator only in 16 bit depth
case $DEPTH in
        16)
            ksplash
        ;;
esac
</PRE></TT></LI>
<LI>What would be really nice is for KDE to allow me to force some
rules on 8 bit color users, for instance:</LI>
<UL>
<LI> Use 16 color icons on the kpanel and kmenu automatically.
 They won't know to pick a lo_lo_color option like this.</LI>
<LI> Allow me to turn off gradient colors when in 8 bit mode,
allow them to only pick solid colors that are in the pallette.</LI>
<LI> Allow me to turn off wallpapers for 8 bit devices.  The
option is grayed out, doesn't even work.</LI>
<LI> Wallpapers that are selected on a 16bit display, don't
display and just use a solid color wallpaper when the
                        user moves back and forth between 8 and 16 bit
color.</LI>
</UL>
</UL>
<P>
<H4>KDE Applications</H4>
</P>
<P>
I have some notes on certain features that would help with
deploying in a large shop:
</P>
<UL>
<LI><EM><STRONG>Kicker</STRONG></EM></LI>
<UL>
<LI>We would like to have the "Refresh" option back, when I install
new icons I'm sometimes on the phone with users and it would
be nice to have them be able to "Refresh" and see them
right away.</LI>
<LI>The autofresh feature of Kicker seems to really put a load on the
server when you have hundreds of people on the same machine.
                It seems to hit them all at the same time.</LI>
<LI>It would be nice to turn off some of the features in the lower
part of the kmenu.  We have some 'technique challenged' people and
I would like to hide certain things from them.  I would
turn off "Run Command", "Quick Browser", "Recent Documents",
"BookMarks".  All of these make the assumption that
documents and files are on the KDE machine, which isn't the case.  Instead,
what they are doing is opening up the GUI file managers and
deleting their configuration files because they think they are just
'unknown files'.</LI>
<LI>There seems to be an issue where users that change terminals off
have their DISPLAY variable get confused when running something like
                command=rsh oa1 /usr/local/bin/sh_whatever $DISPLAY
directly from the xxxx.desktop file.  There must be a pipe or something
that is
                still being held.  Instead, what we have to do is run
command=/usr/local/bin/sh_whatever and have the program in /usr/local/bin
                do the rsh and then it works.</LI>
</UL>
<LI><EM><STRONG>AutoStart</STRONG></EM></LI>
<UL>
<LI>I have had some requests that Autostart allow you to select the
room that the application start in.   When you shut down KDE currently,
                                and tell KDE to remember everything that is
running, it doesn't do so if the programs were initiated via 'rsh'.</LI>
</UL>
<LI><EM><STRONG>Configuration</STRONG></EM></LI>
<UL>
<LI>We would like the ability to turn off the feature that lets users
pick their own wallpaper instead of picking from our approved lists. What
                is happening is that people are installing their own
wallpapers, and now I'm going to have to write a script to find all of them
that
                are in use and check to make sure they are 'politically
correct' in the workplace.  We would rather they send them to us for
                approval and then install them into the main directory.</LI>
<LI>KDE seems to have some problems when the terminals are turned
off.  Certain processes stay on the server if they do not log off
correctly.
                Some of our users simply power off their temrinals at the
end of the day. :(    Could there be some kind of keepalive checks in the
                various programs to make sure the Xserver is still alive
and then terminate process if not?  I'm not sure how to fix this, but
                will keep experimenting.</LI>
</UL>
</UL>
<P>
<H4>Conclusion</H4>
</P>
<P>
I hope this information is interesting to you all, I hate to think I wasted
your time with it! ;)   If anyone has any questions or comments, feel free
to drop me a line, I'll be happy to answer your questions.   It's the least
that I can do considering how much this product has given us and our users.
</P>
<P>
Regards
</P>
Dave Richards<BR>
City of Largo, FL<BR>
Systems Administrator<BR>
