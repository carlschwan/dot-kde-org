---
title: "How to configure your Anti-Aliased desktop"
date:    2001-05-14
authors:
  - "Danny"
slug:    how-configure-your-anti-aliased-desktop
comments:
  - subject: "[Troll] Who cares?"
    date: 2001-05-14
    body: "[NU: <a href=\"http://dot.kde.org/989808269/#989828691\">Click here to skip this thread</a>]\n<br><br>\nWho cares? Ximian 1.4 is out and it is the Linux desktop standard. All the heavyweights in the Unix world have thrown their weight behind it. KDE no longer stands a chance. Besides, it sucks."
    author: "ac"
  - subject: "Re: Who cares?"
    date: 2001-05-14
    body: "Why do I get the feeling your email address matches your IQ? Go \"home\" flaming.\n\nBesides: _very_ cool tutorial. I was looking for something comprehensive like this for a very long time!\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: Who cares?"
    date: 2001-05-14
    body: "If you think so little of KDE why do you keep an eye on all of the latest info about it?"
    author: "ld"
  - subject: "Re: Who cares?"
    date: 2001-05-14
    body: "Whatever a \"Linux\" desktop standard is. Rumours have it KDE runs also on other *NIX systems.\nBut rather having the heavyweights throwing their weights around I prefer those slim guys and girls doing a fantastic job..... ;-}\n\nthanks for this tutorial, that was exactly what I needed"
    author: "Christoph"
  - subject: "Re: Who cares?"
    date: 2001-05-14
    body: "I don't agree that all of the Unix heavyweights are behind Ximian. Correct me if I am wrong, but the last time I wrote apps for gnome, I was writing straight C, since I switched to Qt/KDE I have been able to do everything in C++ and it is SO much cleaner, I will never look back. I have been using AA fonts on my desktop since qt-2.3 came out without any trouble at all, however I still found this HOWTO very helpful, my thanks go to the author.\nI care.."
    author: "Neil"
  - subject: "Re: Who cares?"
    date: 2001-05-14
    body: "I'm a GNOME supporter too, but I find KDE's design much cleaner...\n\nThe only thing that really bugs me about KDE is QT licensing :(\n\nI really hope the GNOME team will clean up GNOME and start to implement INTI (for C++)...\n\nI think the KDE team is doing a great job (I love everything that supports Linux) but I really hope that someday we will have a standard desktop!\n\nI hate it when people start flaming, it doesn't do anybody any good, we should all work together :)\n\nMarc S"
    author: "Marc S"
  - subject: "Re: Who cares?"
    date: 2001-05-14
    body: "> The only thing that really bugs me about KDE\n> is QT licensing :(\n??? I don't understand that - QT Free Edition is GPLed - is GPL not good enough for you? GNOME is GPLed, too... :-)\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: Who cares?"
    date: 2001-05-14
    body: "I think he refers to the fact that Qt is GPL whereas GTK and Gnome libs are LGPL"
    author: "ac"
  - subject: "Re: Who cares?"
    date: 2001-05-14
    body: "Hi!\n\nWell, the FSF itself recommends the GPL in favour of LGPL and there are quite a few libs in GNOME that are GPLed, too. IIRC there was a discussion once to GPL all major GNOME libs so GNOME apps would be forced to be GPLed, too. I don't know if they did it or if they cancelled it.\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: Who cares?"
    date: 2001-05-14
    body: "None of the essential Gnome libraries are GPL. And you probably remember the discussion backwards as there has been discussion that all essential gnome libs should be officially forced to be LGPLd or more liberally licensed. Not surprisingly RMS seems to have vetoed that."
    author: "ac"
  - subject: "Re: Who cares?"
    date: 2001-05-14
    body: "Although, I agree that it would be great if QT was LGPL'd, but if your a GNU fan, even Mr. Stallman has an article suggesting that all (well, most) libraries should also be GPL to further the movement.  If you're a GNU fan, a GPL'd QT should make you excited!!!"
    author: "Anonymous"
  - subject: "Re: Who cares?"
    date: 2001-05-14
    body: "> Ximian 1.4 is out and it is the Linux desktop standard.\n\nYou wish!"
    author: "Joe"
  - subject: "I care"
    date: 2001-05-14
    body: "Although I am a GNOME supporter and use GNOME instead of KDE, trolls like you make me sick.\nThe GNOME vs KDE war is over! Go home and get a life!"
    author: "dc"
  - subject: "Re: I care"
    date: 2001-05-14
    body: "I agree, it's good for us Linux have so more choices than Windows & MacOS give for a desktop !\n\nI hope there'll never be something like a \"standard desktop\", that's what M$ and Apple wanted to do since 20 years.\n\nThere's still a lot of work to do to have full font-AA everywhere (Gnome, Lesstif, whatever else...)."
    author: "Herv\u00e9 PARISSI"
  - subject: "Re: I care"
    date: 2001-05-14
    body: "Ever get the feeling ac and dc are one and the same ?"
    author: "Da Maine Man"
  - subject: "Re: I care"
    date: 2001-05-14
    body: "Yeah, it's funny how ac and dc always reply to each other on both KDE and Gnome news."
    author: "Henry Stanaland"
  - subject: "Re: I care"
    date: 2001-05-15
    body: "That is BECAUSE he is called ac.\n\"dc\" is some kind of parody on the name \"ac\", get it?\nI don't care whenether you believe me or not. It is the truth."
    author: "dc"
  - subject: "Re: I care"
    date: 2001-05-16
    body: "Ever get the feeling ac and dc are 12 years old ?"
    author: "Da Maine Man"
  - subject: "Re: I care"
    date: 2001-05-16
    body: "I think you're the one who's 12 year old here.\nI have done nothing wrong and yet you are accusing me!?"
    author: "dc"
  - subject: "Re: Who cares?"
    date: 2001-05-14
    body: "How about we just ignore this guy from now on?  He's always posting trying to start wars(on both KDE and Gnome news)...you shouldn't even bother replying.  Obviously, he's just an idiot with too much time on his hands...too bad that he could better spend his time doing something productive."
    author: "Henry Stanaland"
  - subject: "Re: [Troll] Who cares?"
    date: 2001-05-14
    body: "> Who cares? Ximian 1.4 is out and it is the\n> Linux desktop standard. All the heavyweights\n> in the Unix world have thrown their weight\n> behind it. KDE no longer stands a chance.\n\nThat's strange. On my computer it seems like KDE is the Linux desktop standard. And there is no sign of change."
    author: "Erik"
  - subject: "Re: [Troll] Who cares?"
    date: 2001-05-14
    body: "To you, the self-confessed troll:\n\nFact: KDE has it's own web browser, Gnome have to put up the shit Mozilla produces.\n\nFact: KDE has their own office suite. Gnome users have bits and pieces of shit applications.\n\nFact: KDE is designed by engineers. Gnome is hacked by kiddies.\n\nConclusion: who the fsck cares about Gnome anyway?"
    author: "Idiot-Buster"
  - subject: "Re: [Troll] Who cares?"
    date: 2001-05-15
    body: "Like I said, the GNOME vs KDE war is over.\nIf you want to respond on trolls, then flame the troll down.\nHe has nothing to do with GNOME.\n\n1) Mozilla is not crap. Galeon + Mozilla 0.9 runs incredibly fast on my Pentium 166.\n2) So it's not integrated yet. What's your point?\nAbiWord and Gnumeric works fine even without some kind of integration center.\n3) There are people who are involved with both KDE and GNOME. So by flaming like that you'll also be flaming at some KDE people.\n4) I care about GNOME.\n\nSo the next time, flame/spam/kill the troll, not GNOME."
    author: "dc"
  - subject: "Re: [Troll] Who cares?"
    date: 2001-05-14
    body: "ac needs a life.  It seems odd how quick he is to always be one of the first to respond to any accomplishment that enhances KDE, saying, with no reasons to back it up, that GNOME is better.  Impress me ac, tell me why.  And, tell me what relevance it has to this discussion, I'm more than interested to hear that as well."
    author: "dingodonkey"
  - subject: "Re: [Troll] Who cares?"
    date: 2001-05-15
    body: "I think it is great that someone put together this tutorial to help those that can have this now. It looks to be at least six months until Gnome users are so lucky.\n\nsigned,\nOne Very Happy Ximian Gnome User (who has to wait for AA fonts)"
    author: "Garrett Mickelson"
  - subject: "Re: [Troll] Who cares?"
    date: 2001-05-21
    body: "Actually, you are quite wrong. KDE is still the desktop standard. KDE is used on more Linux systems than any other D.E. in the world. \nKDE also does not depend on stuff from dead companies (Eazel), and is the default desktop of the most popular Linux distro in March (SuSE).\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "a lot of troll's here...\n\nwhat about ximian alias eazel?\n\ndead or dead?\n\nread this article... guys.\n\nwich wm becomes the standard?\n\nend of the road for eazel?\n\nhttp://linuxtoday.com/news_story.php3?ltsn=2001-05-12-001-20-NW-GN"
    author: "steve_qui"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "C'mon, just because there are a few so-called \"GNOME supporters\" (aka Trolls) doesn't mean you can just flame at Gnome & Friends.\nThere are lots of KDE trolls at Gnotices too."
    author: "dc"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "Gosh - I was impressed by the detail of this tutorial. Perhaps I'm being lazy, but first, my fonts all worked under a clean Mandrake 8.0 install. I then copied the win2000 AA fonts into one of the directories and restarted the XFS. \n\nEverything looks great. Sure, the smaller fonts are antialiased - but it actually looks good. And while my monitor is good at the current res (1600x1200 - Hitachi CM771) it's by no means some crystal-clear thing.\n\nOtherwise, I'm *so* glad I don't have to edit font resources and so forth such as I used to have to do with getting fonts working for the gimp... Again, kudos to all involved!\nK"
    author: "EKH"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "Nice tutorial. I just wish I could work out why my Xft won't serve TrueType fonts. If I use it, I get only Type1 and bitmap fonts available. If I don't use it, I get all the fonts on my system, but none of them are antialiased.\n\nIs there anything special I have to do to get it recognising TrueType fonts? I'm using XFree86-4.0.3 and Qt-2.3, both compiled from source, and my normal TrueType server (X's freetype engine) works fine, just (obviously) without AA."
    author: "MaW"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "Install mandrake 8.0. Problem will be solved.\n\nCraig"
    author: "Craig"
  - subject: "Problem"
    date: 2001-05-14
    body: "Might be caused by your video card drivers, I had the same problem with the older NVidia drivers, but with the latest drivers this is fixed."
    author: "Proton"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "add to your /usr/X11R6/lib/X11/XftConfig:\n\ndir \"/path/to/truetype/fonts\""
    author: "Evandro"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "I had the same problem\nI added\ndir \"/usr/X11R6/lib/X11/fonts/TrueType\"\nto the /usr/X11R6/lib/X11/XftConfig file \nand removed the Type1 and TrueType paths out\nof XF86Config file (I don't use xfs server)\nas explained in another tutorial on this same site"
    author: "wb"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "Nice tutorial. I just wish I could work out why my Xft won't serve TrueType fonts. If I use it, I get only Type1 and bitmap fonts available. If I don't use it, I get all the fonts on my system, but none of them are antialiased.\n\nIs there anything special I have to do to get it recognising TrueType fonts? I'm using XFree86-4.0.3 and Qt-2.3, both compiled from source, and my normal TrueType server (X's freetype engine) works fine, just (obviously) without AA."
    author: "MaW"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "Did you add your direcory with TT fonts to XftConfig,\nwith the 'dir' syntax?\n\n\nDanny"
    author: "Danny"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-15
    body: "Yes, I did. Didn't make a blind bit of difference - it's like my TrueType fonts just don't exist."
    author: "MaW"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-15
    body: "Funny that you say bitmap fonts work with Xft, because Xft does not support bitmapped fonts yet.\n\nDid you compile against freetype 2.0.2? And be\nsure to put XftConfig in the correct directory.\nYou can send me some debug info (see the tutorial on how to do that) and I will try to figure it out.\n\nDanny"
    author: "Danny"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-15
    body: "What I meant about the bitmap fonts was that they were there, in that X was serving them. Should've made that clearer really - although KDE seemed to only be able to see my Type 1 fonts, of which I have very few.\n\nI'll have to try again and experiment - I compiled against whichever version of freetype is included with XFree86 4.0.3's source."
    author: "MaW"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "Wow, thanks a lot !!\nI have always wanted to ONLY antialias fonts more than about 15 points in size.\n\nAnother thing that bugs me, is the clumsy way that I currently toggle anti-aliasing per application. \nSo instead of calling QT_XFT=0 every time b4 I run kwrite I did something like this:\n\ncd /opt/kde2/bin\nmv kwrite kwrite.bin\necho 'QT_XFT=0 ; kwrite.bin $*' > kwrite.bash\nchmod +x kwrite.bash\nln -s kwrite.bash kwrite\n\nNow, kwrite is no longer anti-aliased.\nI wonder if there is a section in some config file that allows inclusion/exclusion of applications in/from anti-aliasing.\n\nIf there isn't, maybe it could look a bit like this\n\nmatch\n     any app == konsole\n     any app == kwrite\n edit\n     antialias = false;\n \nOr maybe qtaa.allow and a qtaa.deny files or similar would make sense to have.\n-------"
    author: "Risto Treksler"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "A year have passed and my video card is still not supported by XFree86 4...\n\nHello XVideo. Hello anti-alias. Hello NewCoolFeatures(void).\nMay be see eachother in 50 years."
    author: "dc"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "Well, with a GeForce or 3Dfx card being cheap, maybe its time to support a Linux supporter.\n\nPricewatch.com has em dirt cheap!"
    author: "GreenLinux"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-15
    body: "No PCI cards these days are cheap enough for me..."
    author: "dc"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-15
    body: "I would sell my G200 for $50 :)"
    author: "Anonymous"
  - subject: "AA & the konsole part"
    date: 2001-05-14
    body: "I'm using antialiased fonts now since a couple of weeks and they almost work. One thing, though, still bothers me: The system doesn't accept the settings for the \"fixed font\". Although I can select a TrueType version of courier in kcontrol/look&feel/fonts and it is shown correctly in the example line, the system doesn't use it (in the konsole part of konqueror, kate or gideon).\nIt uses instead the TT font that comes first in the TT directory when sorted alphabetically. Of course all paths are set correctly in XftConfig and there's no obsolete \"kdefonts\" file around (like in dep's case). I also think, that my XFree (4.0.3 as of SuSE 7.1) is patched to work with fixed fonts. Has anyone had the same problem and knows how to solve it? (Note: I'm not talking of konsole itself, which works properly with AA. Just the konsole part doesn't!)"
    author: "Melchior FRANZ"
  - subject: "Re: AA & the konsole part"
    date: 2001-05-14
    body: "What if you try to select a custom font in\nkonsole itself? Are there any fonts shown?\n\nDanny"
    author: "Danny"
  - subject: "Re: AA & the konsole part"
    date: 2001-05-14
    body: "No, that doesn't help. As I already said, it's not a konsole problem. Konsole itself =does= work. Just the konsole kparts not, and there's not custom font setting facility in the kpart. Just the one from kcontrol. But that's exactly my problem. The system doesn't accept the kcontrol font settings (and =only= these!)."
    author: "Melchior FRANZ"
  - subject: "Re: AA & the konsole part"
    date: 2001-05-14
    body: "Ah, I see what you mean and think you have\nhit a bug in KDE. Workaround would be to\nalias the font that is selected automatically\nwith a font which is more to your liking.\n\nDanny"
    author: "Danny"
  - subject: "Re: AA & the konsole part"
    date: 2001-05-14
    body: "No, I had already tried that workaround, desperately. A few minutes ago I had another look at that issue, and now I know why it didn't work: Not the font with the alphabetically lowest font file name was taken, but that with the lowest =internal= name. So the link couldn't work. Now I have copied the Monotyope Courier New and named it \"aaaaaaa.ttf\" (for cosmetic reasons, see above), then I changed the internal font name from \"Courier New\" to \"AAAAAAA New\" and turned off AA for this font in ~/.xftconfig. This is an ugly hack, but it works. I still do not know whom to blame -- KDE? the X-server?"
    author: "Melchior FRANZ"
  - subject: "Re: AA & the konsole part"
    date: 2001-05-15
    body: "I suspect KDE, but I'm not sure. More people have reported this problem, but not on all setups.\nAnd the workaround you took is the only one I know about (see also the xfree render maillinglist).\n\nDanny"
    author: "Danny"
  - subject: "Re: AA & the konsole part"
    date: 2001-05-17
    body: "In CVS there is a Konsole Control Panel that allows you to set the system-wide default konsole font.  It solves the problem for Konsole.  However, there are a number of other places in KDE where this problem crops up (such as the numbers in KOffice rulers and occasionally in KHTML).  I sure wish there was a way to select which font to choose in the event that the one wanted is not found."
    author: "not me"
  - subject: "Re: AA & the konsole part"
    date: 2001-10-29
    body: "well...\nand finally what I have to do to get a \ngood looking konsole (even without AA fonts),\nand AA enabled for KDE globally.\nOf course using xterm solves this, but what about\npeople that prefer konsole?"
    author: "Ivan Petrov"
  - subject: "Where to find True Type fonts?"
    date: 2001-05-14
    body: "Anyone know of a good place to find a good collection of free true Type fonts?  Even better would be an RPM containing several of these.\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: Where to find True Type fonts?"
    date: 2001-05-14
    body: "On SuSE run \"fetchmsttfonts\" (as root). It downloads and installs the MS webfonts (Arial, Times, Courier...)\n\nSince most of us have at one time paid for a MS system, using these fonts is allowed."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Where to find True Type fonts?"
    date: 2001-10-28
    body: "I am searching for the TTF 'technical.' Can you offer a website address for this? I am told I have to pay $15.00 for the download which I am willing to do.\nI had the 'technical' font in Microsoft Excel. After an FDISK and reinstallation this font was lost. My documents now have to be changed whenever I used that font. Is there another name for this font? Do I have to convert it to another name? Any help you could offer would be greatly appreciated. [My Excel documents are just a part of my word processing home job ... I do not publish or do anything that would require legal rights to use the font.]"
    author: "Patrice Godey"
  - subject: "Re: Where to find True Type fonts?"
    date: 2001-05-14
    body: "there's a ttfonts package in the latest redhat version. you need rpm 4."
    author: "Evandro"
  - subject: "Re: Where to find True Type fonts?"
    date: 2001-05-15
    body: "FYI, to install/use the ttfonts package from redhat 7.1, you only really need rpm 3.0.5, not rpm4."
    author: "Rex Dieter"
  - subject: "Re: Where to find True Type fonts?"
    date: 2001-05-14
    body: "The best site by far is Microsoft's:\n\nThe biggest limitation is to not distribute the fonts commercially - - - \n\nhttp://www.microsoft.com/typography/fontpack/default.htm"
    author: "Nick Ryberg"
  - subject: "Re: Where to find True Type fonts?"
    date: 2001-05-21
    body: "x.themes.org\n\n'nuf said."
    author: "come on kids!"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "I put the following into my system-wide XftConfig, to try and fix the \"can't choose any fixed font after the first\" issue, and prettify small fonts.  It achieved the second result but not the first.\n\nFar worse was, when I restarted the X server, it died and retried about 5 times running and then bailed completely!  Any idea why, anyone?\n\nWhen I moved it to .xftconfig, KDM started and I could login normally.\n\nI'm using SuSE7.1, XFree4.03, KDE2.1.1, all SuSE binaries.\n\ncheers\n\nWill\n\n----------\n\nmatch\n\tany family == \"mono\"\nedit\n\tspacing = mono;\n\nmatch\n\tsize > 8\n\tsize < 12\nedit\n\tantialias = false;"
    author: "clickfurtle"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "> Far worse was, when I restarted the X server, it >died and retried about 5 times running and then >bailed completely! Any idea why, anyone?\nTrouble with most configurations is that X tries to respawn several times, upon failure, I never liked this:(\nAre there some errors logged somewhere? (/var/log/XFree86.log or similar?)\n\nThere are a few bugs in the 4.0.3 implementation of Xft (that's why I use a CVS version), which can cause segfaults. \n\nI'm not sure, but maybe it helps if you try this\n\nmatch\nany family == \"fixed\"\nedit\nspacing = mono;\n\nFixed should be aliased to mono for the other syntax to work... Maybe that solves the problem?\n\n\nDanny"
    author: "Danny"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "> Far worse was, when I restarted the X server, it >died and retried about 5 times running and then >bailed completely! Any idea why, anyone?\nTrouble with most configurations is that X tries to respawn several times, upon failure, I never liked this:(\nAre there some errors logged somewhere? (/var/log/XFree86.log or similar?)\n\nThere are a few bugs in the 4.0.3 implementation of Xft (that's why I use a CVS version), which can cause segfaults. \n\nI'm not sure, but maybe it helps if you try this\n\nmatch\nany family == \"fixed\"\nedit\nspacing = mono;\n\nFixed should be aliased to mono for the other syntax to work... Maybe that solves the problem?\n\n\nDanny"
    author: "Danny"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "Very nice tutorial :)\nUnfortunately, I had to switch back to 'normal' true type fonts, because for some reason antialiasing and unicode/iso8859-2 fonts totally break my system. Font server crashes and I can only cry. Nothing helps. Removing Xftconfig files, applying old XF86Config, rebooting etc., without luck. \"Fixed fonts\" are missing is all I get from console output. No way to log to X. I suspect that iso8859-2-100dpi fonts are reason for my trouble, but I am not sure."
    author: "Antialias"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "Funny, I do not think this should be causing problems. Especially not if you do not add these fonts to XftConfig with the 'dir' syntax.\n\nWith 'Font server crashing' I assume you mean xfs? What happens if you simply do not use xfs?\nYou should add some dirs with fonts to XF86Config:\n FontPath     \"/usr/X11R6/lib/X11/fonts/100dpi/\"\nand you won't need xfs anymore.\n\nBtw, xfs of X 4.0.2 had some problems with some truetype fonts, I think 4.0.3 fixed this.\n\n\nDanny"
    author: "Danny"
  - subject: "Very Nice!"
    date: 2001-05-14
    body: "Good job, Danny!  We really needed this.  Very well researched and very well done.  Thanks for your contribution!"
    author: "KDE User"
  - subject: "Fixed font problems with AA"
    date: 2001-05-14
    body: "I still can't get any fixed width fonts to work with any of the suggestions here.\n\nSee this bug:\n\nhttp://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=37494"
    author: "Will"
  - subject: "Re: Fixed font problems with AA"
    date: 2001-05-14
    body: "If you are refering to this:\n> The \"fixed width\" font in the KDE font \n>picker module is unable to find any \n>font it likes when in anti-aliased mode.  The >list of available fonts contains \n>only \"fixed\" which looks ok but if you choose >it, it will revert to the first \n>truetype font on the list every time you logon.\n\nIt _will_ be fixed by doing as I said (really;-P). Otherwise there is something strange about your setup/config. \nDoes it work if you use mine XftConfig instead of yours (and do put it in the right directory for your distro, since that can be one source of the problem).\n\nIf it still does not work mail me the debug output (export XFT_DEBUG=1024 ) when starting\nthe konsole.\n\n\nDanny"
    author: "Danny"
  - subject: "Re: Fixed font problems with AA"
    date: 2001-05-15
    body: "XFree86 4.1.0 will fix this, in the meantime you can use this patch from Linux Mandrake kdelibs:\n<tt>\n--- kdelibs/kdecore/kcharsets.cpp.qt_xft        Tue Mar 13 18:57:52 2001\n+++ kdelibs/kdecore/kcharsets.cpp       Tue Mar 13 19:04:04 2001\n@@ -481,6 +481,18 @@\n         return;\n     }\n\n+    // if QT_XFT is used, the font might be ok, even if it is not in the list\n+\n+    if(f.charSet() == charset) {\n+    //kdDebug() << \"KCharset::setQfont: font has correct charset\" << endl;\n+        return;\n+    }\n+\n+    if(f.charSet() == QFont::Unicode) {\n+    //kdDebug << \"KCharset::setQfont: font is Unicode\" << endl;\n+       return;\n+    }\n+\n     // ok... we don't have the charset in the specified family, let's\n     // try to find a replacement.\n \n--- kdelibs/kdeui/kfontdialog.cpp.qt_xft        Tue Mar 13 19:06:49 2001\n+++ kdelibs/kdeui/kfontdialog.cpp       Tue Mar 13 19:08:51 2001\n@@ -423,11 +423,9 @@\n     }\n \n     // Fallback.. if there are no fixed fonts found, it's probably a\n-    // bug in the font server or Qt.  In this case, just use 'fixed'\n-    if (lstFixed.count() == 0)\n-      lstFixed.append(\"fixed\");\n-\n-    lstSys = lstFixed;\n+    // bug in the font server or Qt.  In this case, show all fonts\n+    if (lstFixed.count() != 0)\n+       lstSys = lstFixed;\n   }\n \n   lstSys.sort();\n</tt>"
    author: "Arnd Bergmann"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "Thank you for the very informative article.\n\nOn a new Mandrake 8.0 machine, after \nI installed Microsoft ttf fonts and enabled AA\nall my default fonts changed to some italics font.\nI went to the fonts settings in KDe control center \nand custome specified the fonts and most of the \nitalics fonts went away. But still the default\nfont on Konquerror is italics, by default font \nI mean for an html page with a particular font\nwhich is not available on my system, gets\ndisplayed using the italics font.\n\nHas any one seen this ? How does KDE apps decide\nthe default font for an unmatched font request ?"
    author: "4not4"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "Konqueror uses his own font settings, which are\nkinda obscure:\nSettings->Configure Konqueror->Konqueror Browser->Appearence\nHere you can set a number of fonts that Konqi will use. But they are grouped per encoding, so you should select your preferred font for each of the encodings you are most likely to use (which probably include iso8859-1, iso8859-15 and iso10646-1).\n\nDoes this help?\n\nDanny"
    author: "Danny"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "Konqueror uses his own font settings, which are\nkinda obscure:\nSettings->Configure Konqueror->Konqueror Browser->Appearence\nHere you can set a number of fonts that Konqi will use. But they are grouped per encoding, so you should select your preferred font for each of the encodings you are most likely to use (which probably include iso8859-1, iso8859-15 and iso10646-1).\n\nDoes this help?\n\nDanny"
    author: "Danny"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-14
    body: "I have set the Konqi fonts only for the\ndefault encoding which I guess is iso8859-1, \nI will try setting it for all encodings and see \nif it helps.\n\nThanks a lot for your suggestion."
    author: "4not4"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-15
    body: "Sorry, it doesn't work.  This seems to be a Mandrake 8 or KDE 2 bug!"
    author: "Bart Simpson"
  - subject: "What a pain!"
    date: 2001-05-14
    body: "Why is there a tutorial for god's sake. The XFree installer should have set it up AA to begin with. How can anyone expect a normal person with no knowledge of how to recompile the kernel or xfree or qt or kde or some other god forsaken component. Yeah, I understand that it is complicated (have to detect versions of various components), but that's what an installer is for. I friend of mine with 10 years of DOS, OS/2 & Windows programming experience recently installed Linux and wanted to enable the sound card. Various responses on the newsgroup basically came down to recompiling & adding something to the kernel. This is nuts."
    author: "Robert Gelb"
  - subject: "Re: What a pain!"
    date: 2001-05-14
    body: "Wow, 10 years of experience and then recompiling the kernel, nuts. I use rh7.1 and I simply tell it to enable the soundcard, (mind you THE soundcard), but no kidding, most distro's come with a kernel with all modules allready compiled and they generally have some utility (on rh7.1 eg. you can run \"setup\" from the command line and then a liitle graphical install menu starts in which you you can , among others, configure your soundcard provided your soundcard is supported and most are) for conveniant soundcard configuration, but (I have a sb-live value) generally it is allready recognized during installation and configured.Look in /etc/modules.conf for a \"alias sound-slot-0\" entry."
    author: "wb"
  - subject: "Re: What a pain!"
    date: 2001-05-15
    body: "Besides my previous reply, I wanted to add that if y're new to linux, it might be a good idea to read the distro's manual for configuration options, simple things like sound support are generally well explained. As far as your remarks concerned on AA, if you compile xfree4.0.3 or install a distro that contains xfree4.0.3, render support is standard enabled, if applications want to use the render support for AA, they will have to built that in (qt-2.0.3 as distributed by the latest distro's has) and, lucky us, this means kde makes automagically use of that, you only have to check the appropriate box in the kde control center for that. As you see, buy the latest distro or download it from the internet, check use AA for fonts in the kde control center and the xfree86 installer automatically renders your fonts fuzzy. Peace of cookie."
    author: "wb"
  - subject: "Re: What a pain!"
    date: 2001-05-15
    body: "And please don't swear, god can't help it either if you don't read the manual."
    author: "wb"
  - subject: "Re: What a pain!"
    date: 2001-05-15
    body: "You are a troll, or you completly miss the point. But I'll bite:\nIt _does_ work out of the box, just by enabling AA in KDEs control panel. But, if you want to configure it so that it does all kind of strange things that are specific for your setup: then you need to edit a file, which is not very difficult to begin with, but in the near future someone will write a nice KDE application that can do this for you.\n\nAs for adding a soundcard: in most distros this is\neasy, if not that you either have an outdated distro, or you use a soundcard which is not supported due to lacking documentation of the manufacturer. Btw, this is offtopic on this list.\n\nDanny"
    author: "Danny"
  - subject: "Re: What a pain!"
    date: 2001-05-15
    body: "Come on.  If it were as simple as setting a checkbox (and believe me, it's been tried), we wouldn't see various & numerous articles explaining how to turn it on.\n\nAs far as the soundcard, it was motherboard-based and was supported by a commercial vendor that required that the kernel be recompiled.  \n\nBut you are right, time for a new distro."
    author: "Robert Gelb"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-15
    body: "I am having a strange experience with AA and KDE.\nWhen I run kde, the fonts are not anti-aliased. But if I run gnome1.4 and then run a kde app, say konqueror, the fonts are anti-aliased! Has anyone seen this strange behaviour? Any tips to fix this?"
    author: "Bharath"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-15
    body: "Yes, go to the Control Center->Look&Feel-Style and turn anti-aliasing on.\n\nIt's off by default since it causes crashes in some setups.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "what about xfs"
    date: 2001-05-15
    body: "Can I use AA with XFS (font server)?\nI'd like to use the single set of TT fonts on several machines."
    author: "svu"
  - subject: "Re: what about xfs"
    date: 2001-05-15
    body: "AA is controlled via the XftConfig file, which requires one to list the directories in which ttfonts live.\n\nSo if you want to use AA, the short answer is no, you can't use a single set of tt fonts to be distributed via xfs to several machines.  You can still use them this way in non-AA mode, however."
    author: "Rex Dieter"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-15
    body: "Hi All!!!\n\nI can't say that I have problems with AA but\nstrange feature is. I can't see any raster fonts\nin any font-choose dialogs in any kde apps.\nWhen some app try to use for example 'fixed'\nfont in gets first truetype font and use it.\nI wonder, if It is normal.\n\nBest Regards!!!!"
    author: "Sergey Holod"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-15
    body: "Well, Xft is not supporting bitmapped fonts yet...\n\nDanny"
    author: "Danny"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-05-16
    body: "This must be slightly more complicated!\nIt does not work here with KDE, it does work\nwith xterm's.\n\nI have X4.0.3. KDE2.1.2 with QT downloaded\nfrom the kde web site. RENDERER is shown\nfrom xdypinfo\n\nUnder preferences I have selected \n\"Use anti-aliasing...\" in the style section.\n\nI re-logged out, rebooted.\n\nXterm's are antialiased but not KDE.\n(for instance I can do\nxterm -fa courier -fs 10 and check with\nxmag)\nhmmmm."
    author: "augustmiles"
  - subject: "Re: How to configure your Anti-Aliased desktop"
    date: 2001-10-03
    body: "If xterm exhibits anti-aliasing then you can be sure that Xft is working. However, even if you have the anti-aliasing option set in the KDE control panel, you still need to be using a version of Qt which has been compiled/configured to use Xft.\n\nIt's quite possible that the Qt distribution you downloaded is not configured to use Xft; I solved this problem by recompiling the source, since I had the source anyway. (Can you even get the X11 version without source?) The supplied configure program should automatically detect Xft support, although you can force this option manually.\n\nKDE should use anti-aliasing once you've replaced those Qt libraries. (I stopped X while I replaced them to make absolutely sure that nothing would screw itself up in the process.) Just make sure you backup your old libraries, or put the new versions somewhere else (before the old versions) on your library path. Then, experience the fun of configuring fonts..."
    author: "Paul Boddie"
  - subject: "How not to anti-alias medium fonts in Konqueror"
    date: 2002-10-05
    body: "Open XftConfig and add the following\n#\n# Don't antialias Konqueror medium fonts \n#\nmatch\n\tany pixelsize < 18\n\tany pixelsize > 7\n\tedit antialias = false;\n\nnote: I have 10 as my default size and 8 as my min size so these values may have to be tweaked to your satisfaction.  "
    author: "straightjacket"
---
Various people have been asking how to configure fonts with the new Xft extension, which enables AA fonts, among other things.
This is a short tutorial on how to configure the new Xft extension.

<!--break-->
<h1>
Configuring the Xft extension (including antialiasing)</h1>
<i>Danny Tholen (<a href="mailto:obiwan@mailmij.org">obiwan@mailmij.org</a>)</i>
<p>Xft is an interface to the freetype rasterizer written by Keith Packard,
member of the XFree86 Project, Inc. It allows applications to use fonts
from the new X render extension using a unified font naming scheme. In
/etc/X11/XftConfig (or /usr/X11R6/lib/X11/XftConfig) you will find a configuration
file which can be adapted to suit your personal taste. In this document
I will explain the syntax and demonstrate some things you can do with this
file.
<h3>
Structure</h3>
The basic structure revolves around a 'pattern'. A pattern is a set of
name/valuelist pairs, each valuelist contains one or more typed values.
A certain application requests a font, for example:

<p style="font-family: courier"><tt>family: "Arial"</tt><br>
<tt>size: 12</tt><br>
<tt>encoding: "iso8859-1"</tt><br>

<p>A size 12 arial font in latin-1 encoding. The Xft extension will now
try to patch this pattern to all of the fonts available in the system.
And selecting the one with the best score. Before the matching is done
Xft looks in XftConfig. The requested pattern can here be extended before
use. An example is:
<p><tt>match any family == "Arial" edit antialias = true;</tt>
<p>This will enable antialiasing for all fonts of the family Arial.
<p>Also, the X server is queried to list all of its fonts; the XLFD contains
just enough information to match fonts roughly.
<p>Here's a list of attributes used in matching fonts (in priority order,
this may not be up to date anymore!):
<p><tt>foundry        font foundry (string,
like ¨monotype¨)</tt><br>
<tt>encoding       font encoding (string, like ¨iso8859-1¨)</tt><br>

<tt>spacing        font spacing (integers or proportional
               (0), mono (100), charcell (110))</tt><br>

<tt>bold           is the font bold? (boolean)</tt><br>

<tt>italic         is the font italic? (boolean)</tt><br>

<tt>antialias      is the font antialiased? (boolean)</tt><br>

<tt>family         font family (string)</tt><br>

<tt>size           font size (double)</tt><br>

<tt>style          font style (string, like "Bold Italic")</tt><br>

<tt>slant          font slant (roman, italic, oblique)</tt><br>

<tt>weight         font weight ( integers or light, medium
               (100),demibold, bold, black)</tt><br>

<tt>rasterizer     not yet used (probably "TrueType",
               "Type1", ...)</tt><br>

<tt>outline        outlines available? (boolean)</tt><br>

<h3>
Syntax</h3>
<b>dir</b>
<p>adds a directory to the list of places Xft will look for fonts. There
is no particular order implied by the list; Xft treats all fonts about
the same.
<p><b>include</b> and <b>includeif</b>
<p>cause Xft to load more configuration parameters from the indicated file.
"includeif" doesn't elicit a complaint if the file doesn't exist. If the
file name begins with a '~' character, it refers to a path relative to
the home directory of the user. This is usefull for user-specific configurations.
<p><b>match <pattern> edit <instructions>;</b>
<p>if a pattern from an application matches the pattern after match, it
is edited with the instructions in edit. The pattern match is done as follows:<tt></tt>
<p><tt><b>match </b>qual FIELD-NAME COMPARE CONSTANT</tt>
<p>where qual is either <b>any</b> (matches one specific font) or
<b>all
</b>(matches all fonts). An example:
<p><tt>match all foundry==¨monotype¨</tt>
<p>will match (and edit) all fonts belonging to the foundry monotype.
<p><tt>match any family==¨arial¨</tt>
<p>will match (and edit) one specific font with the family name arial.
<p>FIELD-NAME is one of the properties found above under
<b>Structure</b>.
<p>COMPARE is <b><,</b> <b>>,</b> or
<b>==</b>.
<p>CONSTANT is the value of the field-name in the appropiate type (see
under<b> Structure</b>).
<p>You can use multiple matches before you use the <b>edit</b> statement.<tt></tt>
<p><tt><b>edit</b> FIELD-NAME ASSIGN EXPR SEMI</tt>
<p>FIELD-NAME can be any of the above (see <b>Structure</b>) plus addionally:
<p><tt>pixelsize        font size in
pixels (integer)</tt>
<br><tt>charspace        characer space
(integer)</tt><br>
<tt>minspace        
minimal spacing (integer)</tt><br>

<tt>rgba             color hinting (string ¨rgb¨ or ¨bgr¨ and
                 vertical hinting ¨vrgb¨ ¨vbgr¨)</tt><br>

<tt>xlfd            
x server font (string, type xlsfonts to
                 see a list of your xlfd strings)</tt><br>

<tt>file             the font file (string)</tt><br>

<tt>core            
use X core fonts? (boolean)</tt><br>

<tt>render           use render fonts? (boolean)</tt><br>

<tt>index           
I have no idea what this does:)</tt><br>

<tt>scalable         is the font scalable (boolean)</tt><br>

<tt>scale           
scale the font (integer)</tt><br>
 
<tt>charwidth        character width (integer)</tt><br>

<tt>charheight      
character height (integer)</tt><br>

<tt>matrix           no idea (not really at least)</tt><br>

<p>ASSIGN can be one of <b>=</b>, <b>+=</b> or
<b>=+</b>. With <b>=</b>,
the matching value in the pattern will be replaced by the given expression.
<b>+=
</b>or<b> =+</b> will prepend/append a new value to the list of values
for the indicated field.
<p>EXPR sets the FIELD-NAME to a value.
<p>SEMI is a semicolon. You can use multiple instructions, separated by
a semicolon.
<h3>
Examples</h3>
And now I´ll try to list a few usefull configurations and explain
them. Note that it is configured for my system, and I may use different
fonts than you, so try to adapt the examples to your own needs.
<p><b>1) How do I make fonts available to Xft?</b>
<p>List your Type 1 and TrueType font directories with <b>dir</b>. On my
system (Mandrake 7.2) this becomes:
<p><tt>dir "/usr/X11R6/lib/X11/fonts/Type1"</tt>
<br><tt>dir "/usr/X11R6/lib/X11/fonts/drakfont"</tt>
<p><b>2) How do I use a user specific XftConfig file?</b>
<p>Put an .xftconfig file in your user directory and add
<p><tt>includeif "~/.xftconfig"</tt>
<p>to your standard XftConfig. This will enable a user specific configuration
file, but it will not complain if there is no such file.
<p><b>3) How do I make aliases for my fonts?</b>
<p>I noted that my KDE console asks for ´mono´ fonts when it
is looking for a fixed font. ´console´ is used when I select
´linux´ in the font menu of the KDE konsole. Therefore, I used
two aliases for fonts which are also named ´fixed´:
<p><tt>match any family == "fixed" edit family =+ "mono";</tt>
<br><tt>match any family == "console" edit family =+ "mono";</tt><tt></tt>
<p><b>4) Antialiasing my fonts makes me dizzy!</b>
<p>Although there is a big fuz around AA in X, good fonts actually look
better if they are not antialiased. The antialiasing blurs the fonts by
adding grey pixels to the edges, and this may strain your eyes if you looking
at them for a long time. (Your eyes will try to get the fonts sharper,
which ofcourse is not working because they are blurred;) However, for very
small fonts, antialiasing may increase the readability of the fonts, because
with sharp edges, there are to little pixels available for your mind to
figure out what it means. And for bigger fonts, the edges become very jagged
when not anitaliased, so here you also might want to have aliased fonts.
Ofcourse you can also turn off the antialiasing for specific fonts. In
other operating systems, most truetype fonts are not antialiased between 8 and 12
pixels, while only large Type1 fonts are antialiased.
<p>Use the following in your XftConfig to antialias only fonts of specific sizes:
<p><tt>match</tt><br>
<tt>    any size > 8</tt><br>

<tt>    any size < 15</tt><br>

<tt>edit</tt><br>

<tt>    antialias = false;</tt><br>

<p><b>5) My fixed fonts do not appear or look _very_ wrong in the KDE konsole
or similar programs!</b>
<p>I noted that somehow a lot of fixed font do not tell Xft that they are
fixed, and thus, mono spaced. Therefore only a part of the font is displayed.
We can manually set the spacing for these fonts (this assumes you have
fixed aliased with mono as in question 3):
<br><br>
<tt>match</tt><br>
<tt>    any family ==
"mono"</tt><br>

<tt>edit</tt><br>

<tt>    spacing = mono;</tt><br>

<p><b>6) My Symbol, Webdings, etc. fonts do not show up!</b>
<p>For some reason some (symbol) fonts are not correctly recognized, and Xft will
show your default font, or a font which has the closest match (which is
generally not what you mean at all). For Adobe Symbol and MS-webdings I
did the following to get them working:
<p><tt>match</tt><br>
<tt>    any family ==
"webdings"</tt><br>

<tt>edit</tt><br>

<tt>    antialias = false;</tt><br>

<tt>    encoding += "glyphs-fontspecific";</tt><br>
<br>
<br>
<tt>match</tt><br>
<tt>    any family ==
"symbol"</tt><br>

<tt>edit</tt><br>

<tt>    antialias = false;</tt><br>

<tt>    encoding += "glyphs-fontspecific";</tt><br>

<p>A usefull way of figuring out these things is to activate debuging
with
<p><tt>export XFT_DEBUG=1024</tt>
<p>This will generate <b>a lot</b> of output, especially if you have many fonts,
because it lists the properties and scores of every font available. You
can also use other values. For a nice summary of what happens (requested font, XftConfig substitutions,
x server additions and the finally matched font), you can use XFT_DEBUG=2.
<p><b>7) Why do my KDE programs start now soooo slow?</b>
<p>Currently, the Xft mechanism in XFree 4.0.3 has to parse the XftConfig file <i>each</i> time a program is started.
And the info of all these fonts has to be read. Newer versions (in CVS) will use a cache and are much faster.
Especially if you have many fonts this can be a problem, and the only real solution is to upgrade to a CVS version of XFree.
<p><b>8) I have a LCD screen, can I use subpixel hinting instead of normal antialiasing?</b>
<p>Yes you can. Subpixel hinting uses colors instead of grey pixels to do the AA.
I do not have a LCD screen so I do not have any idea of how it looks but you can play with the
<b>rgba</b> setting. Try
<p><tt>match edit rgba=bgr;</tt>
<p>or use rgb if you have a different type of monitor. For vertical AA you can try vbgr and vbgr.
<p><b>9) My fonts still look bad!</b>
<p>If you do not have some good truetype fonts, it is worth to go and look
for them on the internet. Other reasons why your fonts still look bad can
be because of your build of freetype2. Snapshots versions before 2.0.2 were
compiled with an option that had some patent issues. Therefore, the standard
2.0.2 compiles without this option. To fix this, download the freetype2 source rpm
and change in <br> include/freetype/config/ftoption.h line 314:<br><br>
<tt>#undef TT_CONFIG_OPTION_BYTECODE_INTERPRETER</tt><br><br>
to:<br><br>
<tt>#define TT_CONFIG_OPTION_BYTECODE_INTERPRETER</tt><br><br>
and rebuild with this modified source. See the freetype2 README file for details.
<br>
Adobe Courier looks terrible on my system, so I made an alias so that Lucida console
is displayed instead. If anyone can get it to display nicely I would appreciate it.
<br><br>

<p><b>10) Other resources on the web...</b>
<p>Here you can find more info about setting up the render and Xft extensions:<br>
<a href=http://www.nadmm.com/show.php?story=articles/aafonts-HOWTO.inc> SuSE HowTo </a><br>
<a href=http://trolls.troll.no/~lars/fonts/qt-fonts-HOWTO.html> Lars Knoll HowTo </a><br><br>
My own XftConfig can be found <a href=http://www.dexterslabs.com/danny/XftConfig>here</a>;
remember that it is specifically adapted to my system, and you will have to edit it.

<p>Feedback is appreciated!
