---
title: "KDE 2.2 Release Plan (Revised)"
date:    2001-03-30
authors:
  - "Dre"
slug:    kde-22-release-plan-revised
comments:
  - subject: "Whew!"
    date: 2001-03-30
    body: "I'm glad everything got pushed back a little bit. It seems like there has been tons of releases the last few months. This will give us users more time to digest 2.1.1 and give better feedback! Good job KDE guys!"
    author: "jorge o. castro"
  - subject: "Re: Whew!"
    date: 2001-03-30
    body: "Some still missing features in KDE\n\nThemes:\n- Add a configuration menu for title bar buttons (like in KDE 1.X)\n- Possibility to change the cursor of the pointer device (e.g. mouse)\n  - colorful pointers (*.PNG)\n  - animated pointers (*.MNG)\n- Enable animated pictures (*.MNG) in all picture (*.PNG) containing components\n\nKicker:\n\n- Add a popup menu when clicking at menu items with the right mouse button.\n  -> open\n  -> open in konsole\n  -> cut\n  -> copy\n  -> copy the link (stored in *.desktop)\n  -> paste\n  -> properties\n\nThe clipboard:\n\n- Deactivate the autocopy/paste behavoir of X:\n  -> Marking text with a pointer device (in X) owerwrites all information\n     cached in the clipboard.\n\n    |\n     > If you want to mark text to delete it, all clipboard information,\n       like filenames and text passages, are lost.\n\n\nSystem administration tools:\n\nThere is lack of administration tools in KDE.\n Many people prefer to edit configuration files.\n There are also inkompatibilities between different applications and UN*XES,\n but many users and admins don't have enough time and skills\n to fight through various types of docs and manuals.\n There is need of graphical system configuration tools in KDE.\n It's difficult for the masses(Win9X/Me and WinNT/2k users/admins) to migrate to\n Linux (or other UN*Xes), this has to be changed.\n\nKDM:\n - Toggle between different resolutions, color depth or even X configurations\n   -> Games often need different resolutions and color depth.\n\nKEYBOARD auto-repeat and RESOURCE management:\n Keyboard auto-repeat should be disabled while holding down CTRL, ALT or other special keys.\n If the system is out of resources you should be refused to start new apps\n until others are terminated. The user needs the possibility to logout at any time.\n\n Hold down CTRL and ESC in KDE2 for some seconds and the whole system gets messed up.\n This is caused by keyboard auto-repeat and missing resource management.\n Lots of KTOP instances are started until the system freezes and even the X server cannot be killed\n with CTRL+ALT+BACKSPACE.\n\n\nIf you have any further ideas about how to improve KDE post them or even better start coding."
    author: "gjw"
  - subject: "Re: Whew!"
    date: 2001-03-31
    body: "I believe mouse pointers are handled by X, not by the window manager, so you can't have colourful ones without rewriting the X server...\n\nI like X's clipboard system!\n\nBut MNGs where PNGs can be would be real cool."
    author: "Matt"
  - subject: "Wow ! Not so fast"
    date: 2001-03-31
    body: "The Kicker upgrade you proposed is a straight way to The Great Confusion. And the current (2.1)version isn't perfect too. When I want to start a program and click on the big K I often get a little plus, drag'n'drop starts, some icons landing on my desktop - God knows what happens, a few mouse moves and I have my desktop reconfigured. This horrible Drag'n'drop should be removed ! The interface design should be very simple and clean. If you want to change the menu start menu editor."
    author: "Felek"
  - subject: "Re: Whew!"
    date: 2001-03-31
    body: "Fancy coloring the mouse pointer is near impossible becuase of X."
    author: "robert"
  - subject: "coloring the mouse pointer"
    date: 2001-03-31
    body: "This was discussed in the kde community. It is of course possible, just not with the current X implementation.\n\nOne solution (apart from extending Xfree) would be do just have KDE manage the cursor icon. This can e.g. be seen when you drag a file. The icon is colored as well.\n\nI would not bet on thics being part of KDE-2.2, though, probably 2.3."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Whew!"
    date: 2001-03-31
    body: "Themes:\nControlling the placement of window buttons over all themes isn't really possible with the new KWin architecture.  Instead, what we need are more themes to choose from, because they decide themselves where the buttons are placed.\n\nAnimated MNGs everywhere:\nI'm not sure that's desireable, or even possible!  I wouldn't like it, it would be like Internet ad hell!  Flashing, animated thingees everywhere!  Besides, that's just asking for bloat, slowdown, and screen flickering.\n\nKicker menu:\nI'm not sure we should go the MS route and make everything in the menu draggable and right-clickable.  I think instead we should focus on making the menu editor better.  If you want a program opened in a Konsole, simply open a Konsole and type it's name.\n\nClipboard:\nI agree that the behavior of the X clipboard is annoying, however, that's a QT problem, not a KDE problem.  AFAIK, it will be fixed in QT3.  There will actually be 2 clipboards - a Windows style one and an X style one.  They will be completely independent, so you can use either one exclusively, or both if you want.  Choice is good!\n\nSystem Admin tools:\nI can only agree that there should be better admin tools.  However, it is very hard for the KDE team to do anything about this, because they can't single-handedly create and maintain 50 different versions of each admin tool, one for each OS and each distribution out there that KDE is used on. (and it really would require 50 different versions because each distro works differently).  The only solution is for distributions to do it themselves.\n\nResolution/color depth changer:\nThat's mostly an X problem.  A graphical X configurator would be nice, but that falls under the category of hard-to-make admin tools discussed above.\n\nKeyboard auto-repeat:\nAuto-repeat should NOT be disabled when Ctrl or Alt are pressed!  Think of the poor Emacs users!  Many other Linux programs would become much harder to use if KDE did this.\n\nResource management:\nA bad idea.  There is simply _no possible way_ to tell how starting another application will affect the responsiveness of the system (or to tell what level of responsiveness the particular user deems acceptable).  Any attempt to make something like this would simply serve to frustrate users who ran up against the limits.  The obvious solution to the bad situation you describe is to make KSysGuard a unique application:  that is, only one instance of it can be running at one time.  Running it again would simply bring up the already-running instance.\n\nAlso, anytime Ctrl-Alt-Backspace doesn't work is an X bug, not a KDE problem.  Badly behaved programs should never be able to crash your X session.\n\n>If you have any further ideas about how to improve KDE post them or even better start coding.\n\nNo, don't post them here!  Go to bugs.kde.org and file a wishlist item.  That's what the wishlist is there for!\n\nSorry for the long post."
    author: "not me"
  - subject: "X"
    date: 2001-05-16
    body: "There is a graphical X configurator, in X 4.01, called xf86cfg."
    author: "mike"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "Has arts been excluded from the key important issues ? I remember reading the output of a chat session wherer there was a whole discussion on the topic and how to give a *serious* face lift to the sound server.\n\nAny news ?"
    author: "Raphel Borg Ellul Vincenti"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "aRts is part of kdelibs"
    author: "Charles Samuels"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "i wish aRts was optional. i have a sblive that already supports playing multiple sounds through /dev/dsp so i don't need aRts. it just waste alot of resources for me. also some programs (like  Jezzball) don't have sound unless you are using aRts. i think this should be an option at compile time to use arts or not.."
    author: "jp"
  - subject: "Optional HW mixing?"
    date: 2001-03-30
    body: "Would it be possible to make the software mixing in aRts optional? \n\nI, too, have a soundcard that supports many (32 on the Trident 4Dwave I think) simultanuous output channels, so it would make good sense to use them. \n\nLower latency and lower overhead.\n\nWould this be possible, given aRts' structure?\n\nDaniel."
    author: "Daniel"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "I too have a sound blaster live, and would like to be able to not to use srts in anyway at all."
    author: "Mark Hillary"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-04-01
    body: "I'm not sure (for I haven't really programmed KDE2 yet), but I think it won't be possible. There had to be a \"stub\" interface in the KDElibs in place of sRts then, and after all the analog realtime synthesizer supports much more features to the programmers than just mixing into /dev/dsp"
    author: "CP"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "Shoot!  Since the release has been pushed back, I won't be able to download KDE 2.2 on my cool Ethernet connection here at college using apt-get.  I'll be stuck downloading it over a 28.8 modem, at home during the summer.  Yuck.  And I'll have to use Windows, because I have a winmodem.  Double yuck!  Oh well, it'll be worth it.\n\nI'll bet I'm not the only one with this problem, either :-)"
    author: "not me"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "<a href=\"http://www.kde.org/cdrom/\">http://www.kde.org/cdrom/</a>\n"
    author: "David Walser"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "If Your lucky it'll get pushed out a bit further and you can download it next semester ;-)"
    author: "E. Sorensen"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-31
    body: "If you run kernel 2.4.2, you wont have to use windows to download the new kde, because its supposed to support them."
    author: "James Hood"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-04-01
    body: "Kernel 2.4.2 supports winmodems?  I believe you are mistaken.  A few isolated types of winmodems do have drivers under Linux, but of course the one I have isn't supported.  The vast majority of winmodems aren't supported by Linux.  And it isn't possible to make a generic \"winmodem driver\" because each type of winmodem (each chipset) is different.  So it isn't really possible for kernel 2.4.2 to \"support winmodems\" because it would have to have lots and lots of drivers, one for each type.\n\nUnfortunately, until manufacturers start natively supporting Linux, there isn't much hope for winmodems in Linux, I'm afraid."
    author: "not me"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-04-01
    body: "Kernel 2.4.2 supports winmodems?  I believe you are mistaken.  A few isolated types of winmodems do have drivers under Linux, but of course the one I have isn't supported.  The vast majority of winmodems aren't supported by Linux.  And it isn't possible to make a generic \"winmodem driver\" because each type of winmodem (each chipset) is different.  So it isn't really possible for kernel 2.4.2 to \"support winmodems\" because it would have to have lots and lots of drivers, one for each type.\n\nUnfortunately, until manufacturers start natively supporting Linux, there isn't much hope for winmodems in Linux, I'm afraid."
    author: "not me"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-04-03
    body: "Well, Lucent didn't write the drivers for the LT chipset, but they did hire someone else to do it.  at least this is showing some interest in manufacturers providing support for Linux."
    author: "Michael Gaskins"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-04-05
    body: "Well, you should first use the summer to get rid of Bill Gates, then make your Winmodem running in Linux as I did it on Suse 7.0 on my Compaq notebook. After having done those things FIRST you should be able to download KDE 2.2 without any problems.\nMatthias"
    author: "Dipl.Ing.M.Schulze"
  - subject: "KDE 2.1.1"
    date: 2001-03-30
    body: "Does anyone know if in 2.1.1 it is possible to have an external (hideable) taskbar like in KDE 1.x?  I just upgraded to 2.1 and I thought that I read at one point that they would fix that, but I haven't read anywhere whether they have.  Just curious."
    author: "Tom Mutdosch"
  - subject: "Re: KDE 2.1.1"
    date: 2001-03-30
    body: "Well, you can add an external taskbar, but I am not sure whether it can be made\"hidable\". Right-click on the \"handles\" of any section in the main kicker - i.e. verical bars with raised dots - for instance near the clock - select Panel Menu -> Add -> Extension ->External Taskbar.."
    author: "Anonymous Coward"
  - subject: "Re: KDE 2.1.1"
    date: 2001-03-31
    body: "Well, I now have a task bar at the top of the screen, but I can NOT find any way to hide it.\n\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE 2.1.1"
    date: 2001-03-30
    body: "I use kicker with auto-hide option and the taskbar-applet only. The standard KDE stuff (clock, page, K-menu...) is on a solid child-panel.\nI *think* you need Aaron's patch from kde-devel mailing list to have a instant child-panel after starting (or you have to start kasbar)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: KDE 2.1.1"
    date: 2001-03-31
    body: "I fear it's impossible although it was the feature most anticipated by me for KDE 2.1. :-(\nKicker currently is far from beeing as cool and configurable as GNOMEs panel which is the Reason I'm using a mixture of KDE and GNOME. (They interoperate nicely, BTW.)\nWhat I's *REALLY* like to see in KDE 2.2 is\n* (auto)hideable child panels of an arbitrary\n.  size,\n* corner-aligned panels additionally to the\n.  default edge-aligned\n* _serious_ kwin extensions like configurable\n.  window borders on a per-app-basis, the ability\n.  to remember a window's state (like E does.),\n.  cooperationg with \"managed\" Wine-windows,\n.  animated desktop switching, animated shading\n.  of windows, ...\n* or, alternativly, if that is not possible,\n.  making kjava work with other WMs than kwin so\n.  I can continue to use my Enlightenment,\n.  GNOME-panel, kdestop, Konqueror-Environment\n.  with emmbedded Java applets...\n\nI'm really looking forward to the release of KDE 2.2, anyway! :-)\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "Yea this is kinda cool. It seems the releases have been pretty fast. I wonder if the application developers are having a hard time keeping up with all of it.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "Not really actually\nbecause the API is pretty stable, you usually\ndon't need to change anything in your app\neverything (like menu animation) is done by kdelibs unless you want to use a new feature like kprinter for example. \n\nNow coding in kde cvs is another matter\nyou have to keep up with the every day changes. \nBut it is not that hard since the technological choices were usually the good ones:) (like C++/Qt for example)"
    author: "jesunix"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "Thanks i've been wondering about how these people were able to keep up with all releases.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "Why dosen't KDE use the OpenOffice (Open2D?) printing API, since it's LGPL it could be change to look like the rest of Qt/KDE API very eisly, is there any technical reason this is not done?\n\nSorry bad english."
    author: "Bejin"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "If i'm not wrong, the change to the print framework is only about the printer management (adding a new printer, on the network, on the parrallel port, default page size,..).\nthis is done via CUPS, but the system supports plugins (like LPR, LPRing etc).\nKDE does not not (AFAIK) need a printing framework ala open2d (or whatever the name of it is ;o)), QT provides it already (QPainter).\n\nOf course it can also be that i'm misunderstood and that i say garbage. correct me then! ;o)\n\nemmanuel"
    author: "emmanuel"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-31
    body: "I meant the OpenOffice equivilient to QPainter, I'm also not sure why KDE dosen't use imlib and other stuff, it would be very eisy to make performance good more than now, if some stuff not TrollTech's was used."
    author: "Bejin"
  - subject: "Re: KDE 2.2 Release Plan - qcad?"
    date: 2001-03-31
    body: "While it's not really a kde app, I'd love to see qcad included with koffice.\n\nhttp://www.qcad.org"
    author: "Ross Campbell"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-31
    body: "Well, let me vote for the nice CUPS (www.cups.org, cups.sourceforge.net), too.\n\nI have done a package of qtcupts-2.0 and kups-1.0 for SuSE 7.1 and I can tell you that I have never seen a better printing system for *nix (M$, too :-) in my live.\n\nRegards,\n Dieter\n\n- \nDieter N\u00fctzel\nGraduate Student, Computer Science\n\nUniversity of Hamburg\nDepartment of Computer Science\nCognitive Systems Group\nVogt-K\u00f6lln-Stra\u00dfe 30\nD-22527 Hamburg, Germany\n\nemail: nuetzel@kogs.informatik.uni-hamburg.de\n@home: Dieter.Nuetzel@hamburg.de"
    author: "Dieter N\u00fctzel"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-04-04
    body: "For your information, the new KDE print framework _is_ QtCUPS and KUPS together (I'm the author of them), but completely integrated into KDE. This means that all KDE apps will use the QtCUPS print dialog, and there will be a KControl module for print management (like KUPS). The nice thing is that I switched to a plugin architecture allowing to support other print systems as well through the same interface. For example managing printtool printers (RedHat) with a KUPS interface."
    author: "Michael Goffioul"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-04-04
    body: "Yes and no.\nThe KDE print framework is not only a print managament tool, but more an intermediate layer between KDE applications and the underlying print system, which provides a nice user interface to control print job. The applications just send their drawing primitives to the system, and the framework takes care of sending the print job to the printer using the selected print system, print options, ...\nBut this is not a rewrite of a QPainter class. Qt does a good job, and the print framework uses it internally."
    author: "Michael Goffioul"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "The new SMB kioslave (by Caldera) will be ready in time for 2.2 as well, I suppose? To me that would be major news."
    author: "Matt"
  - subject: "Installation program - please!"
    date: 2001-03-30
    body: "It would be great if KDE had a standard intallation program (untar, unzip, make menu shortcut, resolve and install dependent libraries, etc) for itself or applications that are in apps.kde, for example."
    author: "Julio Alvarez"
  - subject: "Re: Installation program - please!"
    date: 2001-03-30
    body: "Search the Dot archives - there was an article about an installer that was being worked on.  In the meantime, I recommend that you try Debian.  You won't be sorry!  Here's how I install KDE:\n\napt-get install task-kde\n\nThat's it!  apt-get then goes out on the Internet, downloads all KDE packages _and_ dependencies automatically, then installs them and configures them in the right order, while I read a book :-)  Upgrading works much the same way, and I usually have updated KDE packages downloaded and installed even before the release has been announced!  I can hardly begin to express the complete and utter coolness of apt-get.\nAnd of course, this works for any other program that has a debian package.  \n\nSlightly OT:  A big thank-you goes out from me to all the Debian KDE packagers - THANK YOU!!!"
    author: "not me"
  - subject: "Re: Installation program - please!"
    date: 2001-03-31
    body: "There is no need to use debian for that...\n\nYou can get apt-get for Connective, and Mandrake Cooker/8.0\n\nThe latter can do the same thing through urpmi and   \n rpmdrake."
    author: "Anonymous Coward"
  - subject: "Installation program - please!"
    date: 2001-03-30
    body: "It would be great if KDE had a standard intallation program (untar, unzip, make menu shortcut, resolve and install dependent libraries, etc) for itself or applications that are in apps.kde, for example."
    author: "Julio Alvarez"
  - subject: "Sorry"
    date: 2001-03-30
    body: "Sorry for the 2 posts. Im am at work and i had a problem with IE5 :("
    author: "Julio Alvarez"
  - subject: "Re: Sorry"
    date: 2001-04-05
    body: "why don't you use Konqueror??? So its your fault!!!"
    author: "Dipl.Ing.M.Schulze"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "I have a question. What has happened to the ablity to hide the applet handles on kicker, like you where able to do in 2.0. Since upgrading to 2.1 and 2.1.1 I have not been able to find this option anywhere, it seems to have been dropped from kcontrol. I realy liked this feature. I now have to edit the config file by hand when I change to option.\n\nAnyway, I would realy like to see this added back in for 2.2. I did enter a bug report when I first noticed but I never got a reply."
    author: "Mark Hillary"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "In ~/.kde/share/config/kickerrc put FadeOutAppletHandles=true under [General]\n\nHopefully this gets put back in the configuration, because I like it like that :)"
    author: "Paul G"
  - subject: "Should be a 2.1.2"
    date: 2001-03-30
    body: "Since the 2.2 release has been pushed back, I think it would make more sense to release a 2.1.2 in about 6 weeks after 2.1.1. And even a 2.1.3 6 weeks after that. That way, we don't have to wait till July for new fixes. But then 2.2 will come with its own bug to be fixed in 2.2.1 :)\nWe need a stable version to go back to. Too much change too fast is bad too. KDE guys should stabilise a release more than they have been."
    author: "nadmm"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-30
    body: "Why not give the next release plenty of time instead of pushing back the schedule.  It's beginning to sound like MS and it disappoints many.\nKDE 2.1 is excellent and we can survive until the next one is technically superior and reliable. Need a better KOffice to handle MS Word docs and Kmail to handle existing email addresses.  \nProgramming is the best I've seen for any GUI. Kudus to all.\n\nThanks for a beautiful alternative to MS (Multiple\nSceloris?).\n\nEd"
    author: "Edward Rataj"
  - subject: "Wishlist for 2.2 and Great Thing KDE 2.1.1"
    date: 2001-03-30
    body: "Hay first off all \n\n        GREAT KDE 2.1.1\n\ni love to work under windows with the \"send to\" item acitaveted by right mouse click.\n\nCould you add something to the right.mouse.click.window ?\n\nnew feature for ark and the right.mouse.click.window (is there a name for that beast ?)\n\na right click on the file\n\"my.tar.zip\" \nshould bring the option extract to my in current folder (like under win98 with WinZip >= 7)\n\nis it possible to change the mime (?) type - what i want is to use Xemacs for opening tex-files instead of kwrite or ktexmaker \n\nMaybe a few of this features are already configurable  and it is me how just do not know that - sorry for that.\n\nGreat Work - although the start up speed is a little slow on my cleron 433 with old 4 MB PCI-Graphik-Card - i am using icewm less and less often (previous favorite windowmaker)\n\nOne last Question - what about kde 1.X i really loved that for older computers - is there any further devolpment ?\n\nAnyway really good work done.\ncu"
    author: "migis"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-31
    body: "KDE 2.1 is very great tu use. It works fine with Debian now. I'm quite happy about it!\n\nI'm looking forward to the nex release, and there's really no reason to hurry. More urgent would be KOffice to be more compatible with MS-Formats, to get really free from booting Windows."
    author: "Zeitblom"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-31
    body: "KDE2.1 is running great over here, but I do have one feature request (don't we all).  I don't know how many KDE developers have used BeOS, but the BeOS taskbar would group all windows of an application under one.  Clicking on \"konqueror\" would bring up a menu that showed all open instances of konqueror that are running at the time.  This is really useful with ~15 windows open, where a task bar with 15 \"konq...\" doesn't help much.  If it just grouped them all under \"konqueror\", navigation between windows would take one extra click, but usuability would be infinitely increased.  Thanks."
    author: "Adam"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-31
    body: "Good idea, ought to be optional though IMO. I'd only use it occasionally..."
    author: "Lars Janssen"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-31
    body: "Is KDE 1.x dead?  I'm have a slow computer that KDE 1 runs fine on but KDE 2 is unusable."
    author: "John"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-31
    body: "Tried KDE 2.1? It's much faster that 2.0 on my old machines."
    author: "Matt"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-04-01
    body: "Make sure you're compiling KDE correctly.\n\n* configure with --enable-final and --disable-debug\n\n* Use gcc 2.95.x, and compile it with optimizations, and tell it to use your cpu's instruction set (i.e. not just 386 code)\n\nAlso, make sure to kill any daemons you don't need, like http, ftp, sendmail, or whatnot."
    author: "Neil Stevens"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-04-01
    body: "I compiled KDE, Qt, X and my kernel all correctly and I run only the daemons that I need, the later KDE's don't help either.  The problem is that I have 4 computers with 24 megs of memory(I can't buy more memory, I live in a poor country) and KDE 2.x is useless, this makes KDE 2.x software unusable on my computers.  It would be great if KDE 1.x was continued or KDE 2.x was a little esier on resuorces that way people that can't afford nice computers can still get good software, that's one thing I though GNU was trying to do.  Anyways I found out that KDE 1.x is not being developed anymore so I'm asking the KDE developers to keep in mind that not everyone can run a PIII with 712 megs of memory."
    author: "John"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-04-10
    body: "Have you tried a different country?"
    author: "Punjab Bolinski"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-31
    body: "I would really like to see support added to Konqueror to send a page or a link like in Netscape (i.e. Location->Send Page and Location->Send Link).  Perhapse this would be better if it were included as part of Location->Save As...\n\nI also am hoping that Solaris will work better than it does for 2.1.  (I'm compiling 2.1.1 as I write this)."
    author: "Aaron Williams"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-03-31
    body: "Great work guys - keep it up.  This project keeps getting better and better."
    author: "Bruce"
  - subject: "Re: KDE 2.2 Release Plan (Revised)"
    date: 2001-04-02
    body: "Yeah it's nice that the release date has been pushed back,,, I hate having to donwload the new version.\n\nWould it be possible in 2.2 to like archive all rpms in like a tar or bz2 file which is compressed as it takes quite along time to download on my 56k.\n\nF.o.D"
    author: "F.o.D"
  - subject: "Start Menu"
    date: 2001-08-31
    body: "The start menu has way to many programs in it, it really needs cleaned up.  This makes it hard to navigate, and I don't like a desktop full of icons. We don't need five of everything on the start menu. Cut back to not much more then the sysetm basics and let the user add applications as they need form the install and the ext. CDs.It is not hard to add apps in Linux from the rpm's on the CD's. this would give user far more control of what apps to install on there system. I hate to say it but I love the new start menu of Windows XP."
    author: "Ken"
---
<A HREF="mailto:bastian@kde.org">Waldo Bastian</A>, the release manager
for KDE 2.2, has
<A HREF="http://developer.kde.org/development-versions/kde-2.2-release-plan.html">posted</A>
a revised schedule for the next major release of the KDE core applications
(<A HREF="http://www.koffice.org/">KOffice</A> is on a separate
<A HREF="http://developer.kde.org/development-versions/koffice-1.1-release-plan.html">release
cycle</A>).  The schedule has slipped somewhat to provide time to add
some major new features to the next release, including such popular
wish-list items as (1) a better printing framework; (2) IMAP support
in <A HREF="http://kmail.kde.org/">KMail</A>; and (3) improved database support with <A HREF="http://www.thekompany.com/projects/kdb/">KDE-DB</A>.  The schedule
now contemplates an alpha release on April 16, beta releases on
May 14 and June 18, and the final release on July 16 (109 days, but who's counting?).