---
title: "Konqueror Gets Activ(eX)ated"
date:    2001-07-10
authors:
  - "Dre"
slug:    konqueror-gets-activexated
comments:
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "That is really neat!\n\nIt's allmost like thumbing your nose at M$ and the companies that won't make plug-ins for Linux/Unix.\n\neven the ideal would be native support...\n\nI cannot wait to have a sorenson codec quicktime player for linux...."
    author: "L.D."
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "KDE developers have done it again.  Just when you thought Konqueror couldn't get any better it makes a quantum leap like this.  Shockwave is amazing, having this on the Linux platform is a real plus plus and I want my Quicktime!\n\nKDE Team:  Do you ever sleep?"
    author: "kde-fan"
  - subject: "The world should know"
    date: 2001-07-10
    body: "Konqueror is, all-round, the best browser in the world.  Nothing else even comes close to its truly staggering featureset, its configurability, and its excellent usability.\n\nWell done KDE team!  Lets keep the excellence (and releases) flowing and Konqueror will become truly world-changing."
    author: "Amazed of London"
  - subject: "Re: The world should know"
    date: 2001-07-10
    body: "Take it easy, there's still a long way to go. I for one would rather have a konqueror with full javascript support (stable !) instead of this ActiveX technology. Just to check out this LivePics stuff I went to the following URL and noticed the javascript wasn't working properly with my konqueror from CVS. Just a few seconds later it crashed, leaving me with a backtrace leading to, indeed, the javascript libraries. This is what happens a lot lately, now I don't know if the javascript problems are caused by something else, but anyway I wouldn't go so far to call konqueror the best browser in the world.\n\nA question for the javascript developers : are you interested in backtraces from crashes caused by javascript on certain sites ?\n\nJelmer"
    author: "Jelmer Feenstra"
  - subject: "Re: The world should know"
    date: 2001-07-10
    body: "> A question for the javascript developers : are you \n> interested in backtraces from crashes caused by \n> javascript on certain sites ?\n\nI think it's rather: \"a question for the developers: are you interested in helping with the development of KJS ?\"\n\nWhen Harri doesn't have time, KJS doesn't evolve much.\npmk fixes the JS<->DOM stuff, but it seems the crashes we get now (the buglist is full of them), rather come from KJS itself.\n\n> I for one would rather have a konqueror with full javascript > support (stable !) instead of this ActiveX technology\nDifferent people, different interests, different challenges. KDE isn't a company, but a bunch of volunteers, remember ;)"
    author: "David Faure"
  - subject: "Re: The world should know"
    date: 2001-07-10
    body: "Ok, well it's just that javascript kindof scares the hell out of me as I read certain mails on the lists talking about 20 different ways of implementing mouse overs etc etc :) I'll have a look at some of the code, it might even be understandable.\n\n>> I for one would rather have a konqueror with full javascript > support (stable !) instead of this ActiveX technology\n> Different people, different interests, different challenges. KDE isn't a company, but a bunch of volunteers, remember ;)\n\nOk, sorry David :) Let me just say that I'd rather see konqueror not crash all the time because of javascript issues instead of having ActiveX support. My friends keep nagging me about konqueror crashing all the time; now is that perfectly reasonable, were it not that I seem to be getting more reports of unstable konq's as the CVS revisions increase ! I know I should fix the bugs myself, but now I suddenly remember I originally replied to someone who said \"konqueror is the best browser in the world\".\n\nI'll have a look at some of those javascript crashes now, see if I can make something out of them."
    author: "Jelmer Feenstra"
  - subject: "Re: The world should know"
    date: 2001-07-11
    body: "A tip:\nDisable JS globally in Konqi. There are so many sites which have reasonable fall-back code.\nOne example is www.alternate.de.\nIf you enter this site of a German computer seller with JS on you cannot navigate their hardware list. If you try it with JS off, everything is fine. If you find a site where JS is a must have, enable it locally.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "ActiveX vs (GNU Mono and DotGNU)"
    date: 2001-07-10
    body: "It's amusing that while *other* projects talk about delivering Microsoft .NET *next year* and make a lot of noise about vaporware,  KDE delivers code for ActiveX support *today*...\n\nWhat a difference.  Congratulations all."
    author: "n"
  - subject: "Re: ActiveX vs (GNU Mono and DotGNU)"
    date: 2001-07-11
    body: "Hey smart guy: ActiveX and .NET have nothing to do with each other. 'Kay, buh-bye now."
    author: "reprazent"
  - subject: "Re: ActiveX vs (GNU Mono and DotGNU)"
    date: 2001-07-11
    body: "Don't think so and see the future. \nPERHAPS in 2005 there will have just some .NET programs, so be carefull insulting things which may save you from Micro$oft softs!!!"
    author: "XFly"
  - subject: "Re: ActiveX vs (GNU Mono and DotGNU)"
    date: 2001-07-11
    body: "I would like to point out a couple of things:\n\n  1) I love Linux - I am not here to bash it.\n\n  2) KDE may deliver ActiveX support today, but Microsoft delivered it years ago.\n\n  3) .Net, as you may or may not know, is in beta right now, and the framework, from what I can tell, is really quite stable. There isn't much to complain about. The Visual Studio IDE is still a little problematic, but I have been using it at work without much difficulty.\n\nAnyway, it seems strange to me that a Linux guy would talk smack about beta software. That's pretty far along in the development stage considering the usual \"something-0.0.0.0.0.1-pre-pre-alpha.tar.gz\" that we're used to seeing."
    author: "GuyWhoActuallyCodesWithDotNet"
  - subject: "Re: ActiveX vs (GNU Mono and DotGNU)"
    date: 2001-07-12
    body: "Look at the subject line."
    author: "n"
  - subject: "Re: ActiveX vs (GNU Mono and DotGNU)"
    date: 2001-07-12
    body: "ActiveX and .NET are two very different things.  There isn't some race between KDE's ActiveX support and GNU Mono.  AFAIK, KDE has *no* support for anything resembling .NET, and there are not plans for it.  This would put KDE behind GNOME in that sense."
    author: "RevAaron"
  - subject: "Re: ActiveX vs (GNU Mono and DotGNU)"
    date: 2001-07-12
    body: "Actually, AFAIK, Mono isn't just a UI, but a CLI Runtime Environment and C# compiler. Thus, both GNOME and KDE can take advantage of Mono.\nAs far as the ActiveX vs. Mono comparison goes, I think the poster was pointing out the KDE people just finished a major MS compatiblity project while the Mono people are saying they might be part way there [on an MS compatiblity project] by the end of the year.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "That's because..."
    date: 2001-07-13
    body: "...ActiveX support for Konqueror has never been mentioned before.\nI'm sure they were already working on it a few weeks ago.\nIf they announced that they are working on this a few weeks ago, you wouldn't say this.\nAnd this is based on Wine's existing work, which is a lot less work than Mono.\n\nBTW, there's already code available for Mono."
    author: "ac"
  - subject: "Re: ActiveX vs (GNU Mono and DotGNU)"
    date: 2004-02-05
    body: "ActiveX and .NET are very different things, so the comparison isn't all that accurate...\n\n...however, for those who seem to think Gnome is \"ahead\" and don't see how amazing this announcement is, I feel the need to point out that:\n\n1)  .NET was designed to be more platform-independent.  ActiveX is based on low-level MS API calls, many of which are un-documented or poorly documented.  So, the ActiveX stuff is harder to implement.\n\n2)  .NET is extremely new and has very few uses in current Windows versions.  ActiveX is everywhere, and every time a Linux product expands support for ActiveX, it is taking a major step in compatability, far more than any .NET project will offer."
    author: "JMoon5FTM"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "Wow, this would be really nice for streaming video plugins.  Windows Media Player works under WINE already, so maybe the browser plugin does too?  Can't wait to go home today and mess around!"
    author: "Justin"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "Geeze i'm becomeing more and more impressed with kde everyday. Much better than announceing what your going to do next year!!\n\nCraig"
    author: "craig"
  - subject: "grow up"
    date: 2001-07-12
    body: "this was a 20 minute hack on Konqueror's part, WINE did the real work of getting ActiveX working on linux.  so why stoke the stupid desktop war?"
    author: "Joe"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "<p>Andreas, Malte, Nikolas: Don't take this personally, but I just have to say this.<p>\n\n<p>I find this article insulting.</p>\n\n<ul>\n<li>(Assuming for a minute that this is a good thing to have) It insults our non-ia32 users.  Wine is not portable.  To say that this helps Konqueror, and makes Konqueror the best browser on any platform, when this will only run on a few select platforms of KDE, dismisses all those users.  Does Wine even run on all of the ia32 OSes that KDE runs on?</li>\n\n<li>It insults our memory and intelligence.  For the longest time we've been told over and over how wonderful KParts are.  From the <a href=\"http://www.kde.org/announcements/announce-2.0.html\">KDE 2.0 press release</a>:\n\n<blockquote>KDE 2: The K Desktop Environment. Konqueror is KDE 2's next-generation web browser, file manager and document viewer. Widely heralded as a technological break-through for the Linux desktop, the standards-compliant Konqueror has a component-based architecture which combines the features and functionality of Internet Explorer\u00ae/Netscape Communicator\u00ae and Windows Explorer\u00ae. Konqueror will support the full gamut of current Internet technologies, including JavaScript, Java\u00ae, HTML 4.0, CSS-1 and -2 (Cascading Style Sheets), SSL (Secure Socket Layer for secure communications) and Netscape Communicator\u00ae plug-ins (for playing FlashTM, RealAudioTM, RealVideoTM and similar technologies). The great bulk of this technology is already in place and functional for KDE 2.0.</blockquote>\nSo back in October, we were led to believe that KParts are the \"next generation,\" as they are what Konqueror is based on.  What changed?  Why is suddenly ActiveX a great \"obstacle\" removed, and a \"shot in the arm\" for the browser?  Is ActiveX superior to KParts?  What obstacles were in front of us before?  And why couldn't they be removed in the KDE way - using our libraries, following our UI standards, and using Free Software licenses?</li>\n<li>Lastly, this article attempts to speak for the reader in ways I think are unfounded.  How can things like the Shockwave player be \"popular,\" when there was no way to run them on Unix before now?  Or is it implied that all KDE users are Windows users, or ex-Windows users?</li></ul>\n\n<p>I see nothing wrong a developer for writing what he needs, and then sharing the results. I myself have an app in KDE that relies upon AOL servers.  What I do think is wrong is when an article under the name of the head of the KDE League, on a site in the kde.org domain, makes these kind of assertions and implications about KDE and its users.  Not everyone likes Windows, uses Windows, needs Windows, can run Windows, or even knows what runs on Windows these days.  To portray the KDE community, and KDE apps, as aspiring to be Windows, is insulting.</p>"
    author: "Neil Stevens"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "I wouldnt wory about that description of KDE2, kparts etc. It's a press release, and probably designed to be sent out to all sorts of journalists who might have no idea what KDE really it. It's standard practice to give background information about a company/product, inside a press release.\n\nAs for why they've chosen to use wine etc to implement activeX support, rather than with KDE libraries, UI standards etc. I suppose there's nothing (theoretically :P) stopping any one of us from doing so. Don't criticise their hard work since they're giving it away for free (even is it's not the ideal approach)."
    author: "Matt"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Actually WINE-based support is VERY important, AFAIK. Unless I've missed something, ActiveX is very platform specific, so implementing ActiveX API's using KDE libraries wouldn't do any good unless someone released an ActiveX control for Linux. And, if they did that, why not just release a KPart?\n\n -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "By \"any\" platform, I believe it was meant that regardless of platform, Konq is the best browser.  They did not mean Konq runs on all platforms...  I mean, heck, they were comparing Konq against Netscape and even IE.  It's not exactly fair playing ground since they all have various levels of portability.  But if you could pick one that would work on your platform (defying all laws of portability), they are saying you would want Konqueror.\n\nAlso, the majority of KDE users are Linux users. Many times you will see people speak of \"Linux is now better\" this-or-that because of something KDE-related.  No one is forgetting about non-x86 users (at least programmers aren't) so there is nothing to worry about.  WINE has always been x86-only.  Now Konq can use it for ActiveX on x86 platforms.  Could KDE come up with their own ActiveX type thing?  Sure, although portability would be difficult (delivery system could send source and compile on target machine?).  Does it mean having an x86-only alternative using WINE is a bad thing?  I don't think so.\n\nI sort of see your point, but I believe you overreacted a little.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: ">It insults our non-ia32 users. Wine is not portable.\n\nOkay.  First of all, you're taking this _way_ too seriously.  Nobody's insulting anyone here.  Now that you've cooled off a bit, think.  A feature that benefits some people makes Konqueror a better browser.  Unfortunately, that feature is not available to you; however, that doesn't make the feature bad or detrimental to Konqueror, or insulting to you.\n\nAlso, check out Bochs. (http://bochs.sourceforge.net)  There is hope!\n\n>Is ActiveX superior to KParts? \n\nActiveX is a different solution to a different problem.  No one is suggesting giving up KParts now that we have ActiveX!  The _only_ reason we want ActiveX is to be able to watch Shockwave movies and Sorensen/Windows Media encoded videos.  We would prefer a *nix native solution if one came along, but we do what we can with what we've got.\n\n>How can things like the Shockwave player be \"popular,\" when there was no way to run them on Unix before now?\n\nShockwave is popular *on the Net*!  Not, obviously, for KDE users, as they couldn't access it.  Duh!\n\n>To portray the KDE community, and KDE apps, as aspiring to be Windows, is insulting.\n\nWe don't aspire to be Windows.  We aspire to watch Shockwave/Sorensen/WMP web content as Microsoft goes down in flames!\n\nUnfortunately, I fear IHBT.  Oh well, it was fun actually :-)"
    author: "not me"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "> as Microsoft goes down in flames!\n\nThat kind of obsession with Windows would harm KDE, if the development community engaged in it.\n\nKDE is no more in competition with Windows than it is with GNOME.  Some organizations that package KDE may be, but KDE itself isn't.  KDE just ships code.  No more, no less."
    author: "Neil Stevens"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "Sorry, I was kind of kidding about that.  I certainly don't really believe Microsoft is going to go down in flames on KDE's part.  They've got billions of $ to burn, and if KDE really starts to threaten them, they will just spend their money on making Windows better until it's better than KDE.  They could do that for years without selling a single copy of Windows, with the cash hoard they've built up.  Heck, they could probably do it on the interest on Bill Gates' personal fortune alone.  So Microsoft isn't going down in flames anytime soon (unless the DOJ has a major change of opinion), and it's probably a good thing anyway."
    author: "not me"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Hey notme,\n  Thanks for pointing out Bochs! I certainly appreciate it!\n\n  -Tim (the Bochs web admin)"
    author: "Timothy R. Butler"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "> Andreas, Malte, Nikolas: Don't take this personally, but I just have\n> to say this.  I find this article insulting.\n \nI'm sorry to hear that.\n \n>            (Assuming for a minute that this is a good thing to have)\n> It insults our non-ia32 users. Wine is not portable. To say that this\n> helps Konqueror, and makes Konqueror the best browser on any platform,\n \nThe article does not claim that this\nsupport alone makes Konqueror the best browser, but it does fill a current\nvoid on the Linux/Unix desktop which made it difficult for Linux/Unix\nbrowsers to compete with browsers on other platforms.  This failure\nis not due to any lack of talent on the part of KDE developers but to the\nfact that certain very popular technologies on the Web are not available for\nLinux natively.\n \nAlso the intent clearly was not to insult anyone.  Though I respect your\nfeelings, I do not see how it insults anybody.  Also as a reality check\nthe fact is that KDE should not limit itself to things available for all\npossible platforms, particularly when something relies on a large Open Source\nproject that developers of other platforms are free to port over if they\ndeem it important enough.  Not to mention that clearly the vast majority\nof PC users are on Intel platforms.\n \n> when this will only run on a few select platforms of KDE, dismisses\n> all those users. Does Wine even run on all of the ia32 OSes that KDE\n> runs on?\n \nI don't know, but it runs at least on Linux, FreeBSD and Solaris.  If it\nruns on FreeBSD and Solaris I presume it would not\nbe that hard to port it to\nother BSDs and their derivatives, should someone be so inclined.  After all,\nWINE has an X-type license.\n\n>            It insults our memory and intelligence. For the longest\n> time we've been told over and over how wonderful KParts are. From the\n> KDE 2.0 press release:\n>\n \n [ ... ]\n \n>\n>            So back in October, we were led to believe that KParts\n> are the \"next generation,\" as they are what Konqueror is based on.\n> What changed? Why is suddenly ActiveX a great \"obstacle\" removed,\n> and a \"shot in the arm\" for the browser? Is ActiveX superior to KParts?\n \nOf course not.  ActiveX is a great feature for permitting Konqueror to\ngo mainstream and for permitting users to achieve greater enjoyment from\ntheir surfing.  Remember that \"it's the apps, stupid\".  Having a\ncompatability layer suddenly makes a lot of Web \"apps\" available to\nKDE users that were not available before.  I think our disagreement may\nbe that the article considers all PC users and you are considering\ncurrent KDE users?\n \n> What obstacles were in front of us before? And why couldn't they be\n> removed in the KDE way - using our libraries, following our UI\n> standards, and using Free Software licenses?\n \nIf an Open Source Shockwave Flash Player were available, or an Open Source\nRobinson codec, I would agree with you.  But they currently are not, and with \nreaktivate, KDE users will no longer be deprived of the web sites that feature\nthose technologies until they become available natively.\n\n>            Lastly, this article attempts to speak for the reader in\n> ways I think are unfounded. How can things like the Shockwave player\n> be \"popular,\" when there was no way to run them on Unix before now?\n \nPeople I have conversed with have universally praised Shockwave as a\ngreat technology.  According to Macromedia's website, over 200 million\nusers have Shockwave installed.  If that's not popular, I don't know\nwhat is!\n \nIn any case it is not speaking for the reader, it is speaking about the\nWeb -- i.e., Shockwave is popular on the Web.  I hope that clarifies\nthe point being made.\n \n[ ... ]\n \n> Not everyone likes Windows, uses Windows,\n> needs Windows, can run Windows, or even knows what runs on Windows\n> these days. To portray the KDE community, and KDE apps, as aspiring\n> to be Windows, is insulting.\n \nAgain, I am sorry you were upset by the article.  Please rest assured\nthat nothing of the sort was intended.  I think if you read the article\nwith the picture of Konqueror/KDE going into the mainstream in mind\nyou might view it differently.  In fact I'm not sure what the article\nhas to do with Windows, except to say that technologies previously\navailable only on Windows (and maybe the Mac) are now available for the\ngreat bulk of Linux/Unix users.  I think that's good news for KDE and\nOpen Source."
    author: "Dre"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "> Also as a reality check the fact is that\n> KDE should not limit itself to things\n> available for all possible platforms\n> particularly when something relies on a\n> large Open Source project that developers of\n> other platforms are free to port over if\n> they deem it important enough.\n\nOf course people are free to use KDE in platform-specific ways, or in proprietary ways.  That doesn't mean that the KDE news site, in the kde.org domain, has to act like they're a fundamental breakthrough for an open source project.\n\n> After all, WINE has an X-type license.\n\nIs Shockwave, or any of the other apps you suggest are useful with ActiveX, available under that license?\n\nYou say that ActiveX is a shot in the arm for Konqueror.  Obviously you mean that it's the use of these \"popular\" ActiveX components that is a great boon for Konqueror's users.  If these non-free components are what you are celebrating, then this is a break from normal KDE practice - to get things working with free software.\n\n> Remember that \"it's the apps, stupid\".\n\nParaphrasing the Clinton/Gore '92 campaign doesn't win any points with me. :-)\n\nBut let's look at what that expression means.  By saying \"It's the economy, stupid,\" the Clinton campaign meant that the economy was the number one, most important thing in the campaign, and in the country.  All other issues were beneath it, and could be compromised for it.\n\nSo, by saying \"It's the apps, stupid,\" you're saying that just having these apps is more important than any other aspect of KDE, and that any other aspect of KDE can be compromised for having more apps.\n\nAs I understand it, one of the fundamental aspects of KDE is that the apps are free.  By championing ActiveX, you're implicitly putting it over all existing free KParts, and implying that freedom is not a valuable attribute for users of software to have.  Is that what you mean to do, compromise freedom for more apps and users?\n\nWhere would we be if, instead of having our own open source html renderer, we instead added hooks to use some internet explorer DLL to do the rendering?  Or if aRts were as reliant upon non-free software as xanim?\n\nBe careful what you paraphrase.\n\n> If an Open Source Shockwave Flash Player were\n> available, or an Open Source Robinson codec,\n> I would agree with you. But they currently\n> are not, and with  reaktivate, KDE users will\n> no longer be deprived of the web sites that\n> feature those technologies until they become\n> available natively.\n\nWell, plenty will be.  Every KDE user not on ia32 will be.  Do you mean to say that Konqueror is not a superior browswer on any platform where Wine isn't supported, because people there lack the ability to run these non-free apps?\n\n> According to Macromedia's website, over 200\n> million users have Shockwave installed. If\n> that's not popular, I don't know what is!\n\nBut what is your audience here?  KDE Users and Developers, or \"the web\" in general?  My impression of dot.kde.org was the former.\n\nMicrosoft Office has a bunch of users, too.  Would you make a thrilled post to dot.kde.org if someone ported the latest MS Office run under KDE, via wine?  Would you call it the removal of a great obstacle, and dismissed  KOffice as you've dismissed KParts here?\n\n> I think if you read the article\n> with the picture of Konqueror/KDE going\n> into the mainstream in mind\n> you might view it differently.\n\nWhat do you mean by mainstream?  Mainstream Windows users?  Are these \"mainstream\" users more important to you than the principles that got KDE where it is today?\n\nYou know, at first, I thought the article was exaggerated in tone just in awe of Malte's and Niko's work.  Now you've made it clear to me that there was no exaggeration.\n\nYour goalfor KDE  seems to be that KDE should be used by as many people as possible, even if making that happen means running as much propreitary Windows software as possible.\n\n> In fact I'm not sure what the article\n> has to do with Windows, except to say that\n> technologies previously available only on\n> Windows (and maybe the Mac) are now available\n> for the great bulk of Linux/Unix users.\n\nEr, FYI, Wine is the \"Wine Is Not an Emulator\" project: a port of the Win32 API to unix, and a system for running Windows binaries under unix.  This has everything to do with bringing proprietary Windows software to KDE."
    author: "Neil Stevens"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "You seem to dislike the idea of any closed source programs running in Linux. Well, you're in the minority. \n\nYou said:\nMicrosoft Office has a bunch of users, too. Would you make a thrilled post to dot.kde.org if someone ported the latest MS Office run under KDE, via wine? Would you call it the removal of a great obstacle, and dismissed KOffice as you've dismissed KParts here?\n\nI say:\nI would love it if Microsoft Office (97 in my case) ran absolutely flawlessly under Linux (it runs off and on under Wine, but it would be lovely to have a native Linux port). Office is one of the jewels in the Microsoft crown, and my favorite word processor... although my favorite version was Word 2 (I've got the 4 floppies it came on sitting around somewhere :).\n\nHowever, I still find KOffice very exciting, not because it will replease Microsoft Office, but because it increases choice, variety and competitiveness. \n\nNo-one has made any attack, actual or implied, on KParts. After all your long and rambling posts, I'm still not sure why you think there has been. Supporting ActiveX is exactly the same (and implemented in a similar, if more complicated way), to supporting closed Netscape plugins -- do you think that *they* are a threat to KParts as well?"
    author: "Jon"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "> Are these \"mainstream\" users more important \n>to you than the principles that got KDE where it \n>is today?\n\nhahahaha...   see the flamewar that spawned gnome for some perspective on that statement.\n\nIt seems to me that KDE has always been about providing a Free desktop environment for *nix, even if that means using a few tools that aren't quite as \"free\" as some of the more rabid zealots would like them to be."
    author: "unterbear"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "> Of course people are free to use KDE in\n> platform-specific ways, or in proprietary\n> ways. That doesn't mean that the KDE news\n> site, in the kde.org domain, has to act like\n> they're a fundamental breakthrough for an\n> open  source project.\n\n  Okay - let me understand what you are saying.\nIf a program only works for 90%+ of all\ncomputers users, but forgets those that use\nsizably less popular architectures, it\nshouldn't be posted? C'mon! The place were a\nnon-IA architecture doesn't reign as the market\nleader is the embedded space. Does this mean\nsince the majority of PC users don't use KDE at\nall, that no KDE news should ever be posted\nanywhere?\n\n > > After all, WINE has an X-type license.\n > Is Shockwave, or any of the other apps you\n > suggest are useful with ActiveX, available\n > under that license?\n\n  You don't seem to understand the point. If\nthe ActiveX controls where open source this\nwouldn't be needed. However, if KDE stubbornly\nrefused the fact that most people want to be\nable to have Shockwave, et. al., we would have\na ticket to a failing platform. KDE developers\nunderstand that the key to making KDE a success\nis to support all major technologies. Like it\nor not, ActiveX is a major technology.\n  Tell me, most Netscape Plugins aren't open\nsource either, does that mean that Konqi\nshouldn't have Netscape Plugin support?\n\n> You say that ActiveX is a shot in the arm for\n> Konqueror. Obviously you mean that it's the\n> use of these \"popular\" ActiveX components\n> that  is a great boon for Konqueror's users.\n> If  these non-free components are what you\n> are  celebrating, then this is a break from\n> normal  KDE practice - to get things working\n> with free  software.\n\n  No it isn't. Everything is working with free\nsoftware. However, we can't get the Shockwave\nsource, so what do we do? The KDE Developers\nunderstand they can't stick their heads in the\nsand and pretend that the majority of PC users\nexpect and demand supprot for things like\nShockwave. Until Linux/KDE commands enough\nmarket share that major developers support it,\nit must be accepted that KDE needs to develop\n\"compatibility modules.\"\n\n> > Remember that \"it's the apps, stupid\".\n[clip]\n>  But let's look at what that expression\n> means.  By saying \"It's the economy, stupid,\"\n> the  Clinton campaign meant that the economy\n> was  the number one, most important thing in\n> the  campaign, and in the country. All other\n> issues  were beneath it, and could be\n> compromised for  it.\n\n  Exactly. Without apps, what exactly does KDE\ndo for us? Apps are everything in a computer.\n\n>  championing ActiveX, you're implicitly\n> putting  it over all existing free KParts,\n> and implying  that freedom is not a valuable\n> attribute for  users of software to have. Is\n> that what you  mean to do, compromise freedom\n> for more apps  and users?\n\n  You are blowing this way out of proportion.\nNo one said that KParts should take second\nfiddle to ActiveX. You seem to be interperating\nthis as they are being in ActiveX for replacing\nthe way of embedding things like KHTML.\nHowever, you seem to miss that ActiveX also\nserves a similar purpose to Netscape Plugins.\nKDE didn't push aside KParts by supporting\nNetscape Plugins, neither do they push it aside\nby supporting ActiveX. That's like saying\nsupporting a new type of printer is pushing\naside truetype font support.\n\n>  Where would we be if, instead of having our\n>  own open source html renderer, we instead\n>  added hooks to use some internet explorer\n> DLL  to do the rendering? Or if aRts were as\n>  reliant upon non-free software as xanim?\n\n  Now you seem to be comparing supporting\nproprietary technology with making it part of\nkey systems in KDE. ActiveX support isn't\nproprietary, the stuff that runs on it often\nis. The same goes for WINE itself. WINE isn't\nproprietary, but it supports many proprietary\napps. Their is a difference between being\nproprietary, and supporting such.\n\n\n> Well, plenty will be. Every KDE user not on\n> ia32 will be. Do you mean to say that\n\n  Okay, if you go by Linux sales statistics\n(and Linux is by far the most popular desktop\n*NIX), I'm pretty sure you would see that less\nthan 1 in 50 Linux users is a non-IA32 Linux\nuser. Let's face it, it doesn't make sense to\ncater to the 1, and ignore the 49.\n\n> Konqueror is not a superior browswer on any\n> platform where Wine isn't supported, because\n> people there lack the ability to run these\n> non-free apps?\n\n  No. But it makes KDE EVEN better because this\nis a major internet technology that can't just\nbe replaces with a OSS clone. If you don't like\nthat most ActiveX controls are non-free, go\nmake an OSS ActiveX control for yourself.\n\n>  But what is your audience here? KDE Users\n> and  Developers, or \"the web\" in general? My\n> impression of dot.kde.org was the former.\n\n  Well according to the article it simply said\n\"popular,\" which would mean \"overall,\" not just\nfor those that read the dot. Most KDE users\nprobably would like to use \"overall popular\"\ntechnologies simply because they run into sites\nthat require them.\n\n> Microsoft Office has a bunch of users, too.\n> Would you make a thrilled post to dot.kde.org\n> if someone ported the latest MS Office run\n> under KDE, via wine? Would you call it the\n> removal of a great obstacle, and dismissed\n> KOffice as you've dismissed KParts here?\n\n  Neil, you just don't seem to get the point of\nthe whole thing. What they are saying is stuff\nthat probably will never (at least for a long\ntime) be ported to a KPart can now be used.\nMacromedia IS NOT GOING to make a KPart\n(AFAIK), because there simply isn't the demand\nyet. Like I keep saying, it's just like\nnetscape plugins.\n  Now, you go back to apps again. ActiveX is a\ntechnology, KOffice is an application. ActiveX\nis not proprietary itself, MS Office is. You\ncan't replace all of the ActiveX controls,\nbecause you would get your pants sued off buy\nthe companies that own the patents on the\ntechnology. You can clone MS Office\nfunctionality because no one owns the rights to\nthe idea of an \"office suite.\"\n\n> What do you mean by mainstream? Mainstream\n> Windows users? Are these \"mainstream\" users\n> more important to you than the principles\n> that  got KDE where it is today?\n\n  Yes. If KDE doesn't care about mainstream\nusers, you can pretty much give up on KDE\nbecoming mainsteam itself. Do you want Linux to\nbecome another Amiga, or GEM, or GeoWorks, or\neven Apple? NO! We want Linux and KDE to be a\nsuccess - thus we shouldn't follow ill-fated or\nunsuccessful company's ideas.\n\n> You know, at first, I thought the article was\n> exaggerated in tone just in awe of Malte's\n> and Niko's work. Now you've made it clear to\n> me that there was no exaggeration.\n\n  Nor should it be, it is absolutely amazing,\nand great!\n\n> Your goalfor KDE seems to be that KDE should\n> be used by as many people as possible, even\n> if  making that happen means running as much\n> propreitary Windows software as possible.\n\n  Somethings may never be open source. Will\nMacromedia make Shockwave open? I don't think\nso. Will Apple make QuickTime open? Yeah right.\nStill users need support for this popular\ninternet standards. How do you expect to\naccomplish this otherwise?\n\nIN CONCLUSION: You seem to be only seeing this\nas that KDE might support some proprietary\nsoftware. Does this mean that KDE shouldn't\nsupport Netscape Plugins? Does this mean we\nshould make it difficult for Opera Software to\nmake the Opera browser? Does that mean all of\nKDE's libraries should be switched from BSD and\nLGPL licenses to the GPL so only open source\nsoftware should be written? NO!\nKDE will remain open, but that doesn't mean it\ncan't support non-open programs, because THEY\nARE NEEDED by MANY, if not MOST people.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "> Of course people are free to use KDE in\n> platform-specific ways, or in proprietary\n> ways. That doesn't mean that the KDE news\n> site, in the kde.org domain, has to act like\n> they're a fundamental breakthrough for an\n> open  source project.\n\n  Okay - let me understand what you are saying.\nIf a program only works for 90%+ of all\ncomputers users, but forgets those that use\nsizably less popular architectures, it\nshouldn't be posted? C'mon! The place were a\nnon-IA architecture doesn't reign as the market\nleader is the embedded space. Does this mean\nsince the majority of PC users don't use KDE at\nall, that no KDE news should ever be posted\nanywhere?\n\n > > After all, WINE has an X-type license.\n > Is Shockwave, or any of the other apps you\n > suggest are useful with ActiveX, available\n > under that license?\n\n  You don't seem to understand the point. If\nthe ActiveX controls where open source this\nwouldn't be needed. However, if KDE stubbornly\nrefused the fact that most people want to be\nable to have Shockwave, et. al., we would have\na ticket to a failing platform. KDE developers\nunderstand that the key to making KDE a success\nis to support all major technologies. Like it\nor not, ActiveX is a major technology.\n  Tell me, most Netscape Plugins aren't open\nsource either, does that mean that Konqi\nshouldn't have Netscape Plugin support?\n\n> You say that ActiveX is a shot in the arm for\n> Konqueror. Obviously you mean that it's the\n> use of these \"popular\" ActiveX components\n> that  is a great boon for Konqueror's users.\n> If  these non-free components are what you\n> are  celebrating, then this is a break from\n> normal  KDE practice - to get things working\n> with free  software.\n\n  No it isn't. Everything is working with free\nsoftware. However, we can't get the Shockwave\nsource, so what do we do? The KDE Developers\nunderstand they can't stick their heads in the\nsand and pretend that the majority of PC users\nexpect and demand supprot for things like\nShockwave. Until Linux/KDE commands enough\nmarket share that major developers support it,\nit must be accepted that KDE needs to develop\n\"compatibility modules.\"\n\n> > Remember that \"it's the apps, stupid\".\n[clip]\n>  But let's look at what that expression\n> means.  By saying \"It's the economy, stupid,\"\n> the  Clinton campaign meant that the economy\n> was  the number one, most important thing in\n> the  campaign, and in the country. All other\n> issues  were beneath it, and could be\n> compromised for  it.\n\n  Exactly. Without apps, what exactly does KDE\ndo for us? Apps are everything in a computer.\n\n>  championing ActiveX, you're implicitly\n> putting  it over all existing free KParts,\n> and implying  that freedom is not a valuable\n> attribute for  users of software to have. Is\n> that what you  mean to do, compromise freedom\n> for more apps  and users?\n\n  You are blowing this way out of proportion.\nNo one said that KParts should take second\nfiddle to ActiveX. You seem to be interperating\nthis as they are being in ActiveX for replacing\nthe way of embedding things like KHTML.\nHowever, you seem to miss that ActiveX also\nserves a similar purpose to Netscape Plugins.\nKDE didn't push aside KParts by supporting\nNetscape Plugins, neither do they push it aside\nby supporting ActiveX. That's like saying\nsupporting a new type of printer is pushing\naside truetype font support.\n\n>  Where would we be if, instead of having our\n>  own open source html renderer, we instead\n>  added hooks to use some internet explorer\n> DLL  to do the rendering? Or if aRts were as\n>  reliant upon non-free software as xanim?\n\n  Now you seem to be comparing supporting\nproprietary technology with making it part of\nkey systems in KDE. ActiveX support isn't\nproprietary, the stuff that runs on it often\nis. The same goes for WINE itself. WINE isn't\nproprietary, but it supports many proprietary\napps. Their is a difference between being\nproprietary, and supporting such.\n\n\n> Well, plenty will be. Every KDE user not on\n> ia32 will be. Do you mean to say that\n\n  Okay, if you go by Linux sales statistics\n(and Linux is by far the most popular desktop\n*NIX), I'm pretty sure you would see that less\nthan 1 in 50 Linux users is a non-IA32 Linux\nuser. Let's face it, it doesn't make sense to\ncater to the 1, and ignore the 49.\n\n> Konqueror is not a superior browswer on any\n> platform where Wine isn't supported, because\n> people there lack the ability to run these\n> non-free apps?\n\n  No. But it makes KDE EVEN better because this\nis a major internet technology that can't just\nbe replaces with a OSS clone. If you don't like\nthat most ActiveX controls are non-free, go\nmake an OSS ActiveX control for yourself.\n\n>  But what is your audience here? KDE Users\n> and  Developers, or \"the web\" in general? My\n> impression of dot.kde.org was the former.\n\n  Well according to the article it simply said\n\"popular,\" which would mean \"overall,\" not just\nfor those that read the dot. Most KDE users\nprobably would like to use \"overall popular\"\ntechnologies simply because they run into sites\nthat require them.\n\n> Microsoft Office has a bunch of users, too.\n> Would you make a thrilled post to dot.kde.org\n> if someone ported the latest MS Office run\n> under KDE, via wine? Would you call it the\n> removal of a great obstacle, and dismissed\n> KOffice as you've dismissed KParts here?\n\n  Neil, you just don't seem to get the point of\nthe whole thing. What they are saying is stuff\nthat probably will never (at least for a long\ntime) be ported to a KPart can now be used.\nMacromedia IS NOT GOING to make a KPart\n(AFAIK), because there simply isn't the demand\nyet. Like I keep saying, it's just like\nnetscape plugins.\n  Now, you go back to apps again. ActiveX is a\ntechnology, KOffice is an application. ActiveX\nis not proprietary itself, MS Office is. You\ncan't replace all of the ActiveX controls,\nbecause you would get your pants sued off buy\nthe companies that own the patents on the\ntechnology. You can clone MS Office\nfunctionality because no one owns the rights to\nthe idea of an \"office suite.\"\n\n> What do you mean by mainstream? Mainstream\n> Windows users? Are these \"mainstream\" users\n> more important to you than the principles\n> that  got KDE where it is today?\n\n  Yes. If KDE doesn't care about mainstream\nusers, you can pretty much give up on KDE\nbecoming mainsteam itself. Do you want Linux to\nbecome another Amiga, or GEM, or GeoWorks, or\neven Apple? NO! We want Linux and KDE to be a\nsuccess - thus we shouldn't follow ill-fated or\nunsuccessful company's ideas.\n\n> You know, at first, I thought the article was\n> exaggerated in tone just in awe of Malte's\n> and Niko's work. Now you've made it clear to\n> me that there was no exaggeration.\n\n  Nor should it be, it is absolutely amazing,\nand great!\n\n> Your goalfor KDE seems to be that KDE should\n> be used by as many people as possible, even\n> if  making that happen means running as much\n> propreitary Windows software as possible.\n\n  Somethings may never be open source. Will\nMacromedia make Shockwave open? I don't think\nso. Will Apple make QuickTime open? Yeah right.\nStill users need support for this popular\ninternet standards. How do you expect to\naccomplish this otherwise?\n\nIN CONCLUSION: You seem to be only seeing this\nas that KDE might support some proprietary\nsoftware. Does this mean that KDE shouldn't\nsupport Netscape Plugins? Does this mean we\nshould make it difficult for Opera Software to\nmake the Opera browser? Does that mean all of\nKDE's libraries should be switched from BSD and\nLGPL licenses to the GPL so only open source\nsoftware should be written? NO!\nKDE will remain open, but that doesn't mean it\ncan't support non-open programs, because THEY\nARE NEEDED by MANY, if not MOST people.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "Errmmm... just to point something out... KParts and ActiveX are two totally different animals.  In fact, I'd wager that the the ActiveX implementation in kde is a Kpart.  A Kpart is many times more powerful than activex and is used for embedding apps one within another regardless of what apps are being embedded where.  \n\nActiveX (afaik) is used principly for embedding web content plugins in MSIE.  Think of kde's implementation as a wrapper for activex controls to be used as kparts within konq.\n\nTRoy"
    author: "Troy Unrau"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "Then, instead of swooning over ActiveX plugins being available, why not just write KParts for the desired functionality?"
    author: "Neil Stevens"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "no. YOU do it.\ngood luck trying to implement opensource sorenson codecs, shockwave player, windows media player compatible stuff ....\nnow you ask those people who develop'd reaktivate to do just the same in 3 years more time (no, more) with legal risks because just ... you got 'insulted' by activex, not even because its bad.\nReaktivate is there for compatibility reasons. It makes technology available to more people (not to all people because it ain't possible - but konqueror is not there for blind users either)"
    author: "ik"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "That's just what they did. The wrote a kpart component which allows graphical embedding of COM components. The fact that the backend is actually implemented using wine is an implementation detail. An important one, though, of course, as it's the only possible way to get stuff like shockwave running. But still for KDE it just means that there is a component available to which you can pass a URL to a shockwave file and it does the job.\n\nHow performant and stable the whole thing is in the reality to the end-user still needs to be seen. But technically it's a very exciting thing and fun to implement/hack . (and fun is what counts when it comes to motivation in free software development) As it works, why not tell people about it? After all it's something completely new on linux (it's not that they just plugged in wine and it worked, they had to implement quite some stuff in wine itself and implement a good chunk of interfaces) .\n\nThe fact that it runs only in x86 is a pity, but it's not their fault. Instead it's a design limitation of the underlying technology that simply can't be resolved. And given the fact that x86 is a popular KDE platform it's just a good reason to tell other KDE developers about this project."
    author: "anonymous"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "man you're so dumb"
    author: "bg"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Maybe because the KDE developers don't have the time to rewrite Shockwave and Quicktime for KPart? Maybe because they can't afford the laywers when Apple comes down in one big swoop and sues the pants off of them for making anything even somewhat resembling the other \"QT\" without King Steven Jobs XIII for permission?\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "I find this post insulting.  It's exactly this kind of flame thrower silliness that builds up over time to cause some of the most talented developers to throw up their hands and just give up.  I further hate the fact that I'm getting sucked into replying to such a troll.\n\nTo your first point, the complaint you're getting at there should instead be directed to the Wine, not KDE folks.  Unix applications are supposed to build from other widely supported libraries, and Wine certainly fits that description.\n\nSecond point: KParts is for connecting information paths within KDE applications.  Where exactly do you get the notion that ActiveX is somehow to be used to fit that role?  I can only assume that you decided to make that up as you went rolling along.\n\nLast point: I don't believe that there are 2 billion Chinese in the world, as none of them are in my house at the moment.  That's the logical equivalent to your suggestion that Shockwave isn't a popular technology.\n\nIt's this kind of mindless venom that does far more harm to open source software than anything Microsoft's PR department can come up with.  Browser plug-in support is a huge hole for the *nix desktop, and these guys are well on the way of filling it.  On top of that, they did so in a way that both stays true to the GPL, and the proprietary licenses of the plug-in creators.  This is an accomplishment worthy of high praise, yet here I am instead responding to a troll."
    author: "Metrol"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "Well said !"
    author: "Jelmer Feenstra"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "<i>For the longest time we've been told over and over how wonderful KParts are.\nSo back in October, we were led to believe that KParts are the \"next generation,\" as they are what Konqueror is based on. What changed? Why is suddenly ActiveX a great \"obstacle\" removed, and a \"shot in the arm\" for the browser? Is ActiveX superior to KParts? What obstacles were in front of us before? And why couldn't they be removed in the KDE way - using our libraries, following our UI standards, and using Free Software licenses?</i>\n<p>\nThis is a complete misunderstanding. ActiveX does in no way replace KParts, those controls like Shockwave are not provided by reaktivate, they are <b>used</b> by it.\nYou obviously didn't bother to have a look at the code before assuming it's some kind of KParts \"replacement\". If you did, you would have noticed that reaktivate <b>is</b> a KPart."
    author: "Malte"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Your argument sounds as if the following are true - (1) Non-Intel platforms are the most popular, and (2) Linux/UNIX has more market share than Windows. However, neither of these are true, and so the reality is we must recognize that.\n\n1.) Supporting this as a big deal doesn't mean that no one cares about non-x86 users, it simply means that the x86 is the most popular platform, and thus if something works on x86, it works for most of us.\n\n2.) Supporting ActiveX doesn't mean that it is superior to KParts, but when was the last time any major software developer released a browser plugin in KPart format. The KDE team obviously recognizes that the important thing is to support as many technologies as possible to insure support of most of the technologies we could ever want (just like they have made Konqi compatible with \"broken\" [read: MSIE-friendly] pages).\n\n3.) Why do they call Shockwave popular? Because it is. Support for Unices doesn't make or break a product. Even support for Mac OS does very little. Support for Windows means everything right now. So, if a program is popular in Windows (i.e. 87% or so of the computer world), that translates as just plain popular.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "What a sour puss!  Well Done KDE!"
    date: 2001-10-24
    body: "Making Konqueror as feature packed as any M$ offering is what the public wants!  To suggest otherwise is to relegate the best chance Linux has to become accepted and USED to the ash heap of past failures.  \n\nI absolutely abhor Microsoft's marketing practices, their history of poorly cobbled together code and the poor value their products have represented.  The public wants and needs an alternative that does what M$ products promised and deliver so poorly.  And we want it without all the nonsense that comes from living with an agressive monopoly.  \n\nLike it or not, Windows has set the standard for the kinds of features users want.  If KDE and Konqueror are to succeed, they need to satisfy those needs the public now has and make the features accessible, affordable and an attractive alternative.\n\nWell done KDE!"
    author: "Pat Jones"
  - subject: "su nobody"
    date: 2001-07-10
    body: "To get rid of the security problems, why couldn't all ActiveX controls be run as a harmless user (such as the user nobody on most systems, who has no access to files)?  It would seem pretty easy to implement.  Does Shockwave really need disk access to function?  Even if it does, it could be totally restricted to a single directory.  You could even chroot it.  Linux's security system is meant to prevent security fiascoes like ActiveX - we should use it!\n\nP.S.  I hate to be a wet blanket, but this is just one less reason for companies to make Linux native versions of their plugins.  Oh well, if it helps Linux become more popular, I guess it'll be beneficial.  If Linux becomes dominant, more plugins will become native anyway :-)"
    author: "not me"
  - subject: "Re: su nobody"
    date: 2001-07-10
    body: "Brilliant idea!   Make your reaktivate binary setuid nobody and chrooted /dev/null!"
    author: "KDE User"
  - subject: "Re: su nobody"
    date: 2001-07-10
    body: "why not start reaktivate in a dedicated wine-system as a sandbox. best would be a template, from wich the actual wine-system will be copied for every activex control. this way the control could do whatever it wants with this system... just kill the wine instance and you have a clean system again... am i wrong ?"
    author: "cylab"
  - subject: "Re: su nobody"
    date: 2001-07-10
    body: "I'm not too sure if su nobody is easily possible given that usually nobody has no write access at all (apart from /tmp maybe). We need to install some files that we download and need to change WINE's registry. Running the whole thing chrooted is planned, though. However, it requires quite a few design changes. Stay tuned."
    author: "Malte"
  - subject: "Re: su nobody"
    date: 2001-07-16
    body: "I agree that it would be better if 'native' binaries were\nmade. But, think of the justice in running DLLs\nintended for a different  platform, but more securely.\nWould people consifer running Linux so they could\nrun MS DLLs with greater peace of mind?"
    author: "Roger Oberholtzer"
  - subject: "Re: su nobody"
    date: 2003-01-20
    body: "You might want to check Fred Mclains webpage on the continueing security hole that is ActiveX.  He has gotten to mimic a signed security certificate..... there are a lot of horrible things that can be done with that black magic...."
    author: "chris c"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "Wow you are taking this really too serious :)\n\nOur goal isn't the dropping of KParts etc..\nWe just want to help companys, which migrate to Linux (_from Windows_), to be able to use their self-made ActiveX controls for the time they have no money to rewrite everything.\nYou may know that it costs _really_ much if you migrate to Linux for the first time. Companys relied on Microsoft's technologies (like ActiveX) and we want to help them.\nOf course having everything native is the coolest thing.\n\nShockwave, Flash and Livepics are _JUST_ testing controls. I have no self-build activex control to test, that's why we take these ones.\n\nSo don't flame please, this is as always done for fun. (Imagine this cool stuff, we are mixing Qt and Windows stuff, throught winelib, in _one_ app). It's just cool stuff, not more."
    author: "Nikolas Zimmermann"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "I for one would like to thank you for your beautiful work!  Despite the flames, you have done some pioneering work and taken KDE/Konqi to a whole new level, even if it only shows what kinds of wonderful things can be done with KDE or if it only gets people like Neil off their collective rear-ends to implement native plugins for Konqueror.\n\nThanks!  Thanks! Thanks!  We are impressed!"
    author: "KDE User"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "My problem isn't with your code.  You wrote what you wanted, and shared it with the world.  That's a fine thing.\n\nMy problem is that Dre is describing the lack of support for propreitary plugins for Windows as a major lack of Konqueror until now, in contrast with the KDE community's general pleasure with Konqueror until now.\n\nAm I taking this seriously?  You'd better believe it.  By putting this site in the kde.org, domain, dot.kde.org represents KDE.  I've put a lot of time in to KDE, and I care about it.  I don't want it led astray."
    author: "Neil Stevens"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "well, for a _lot_ of folks not having ActiveX controls is a big thing. this wasn't a Konqueror weakness, it was a Linux/Unix weakness. what they have done is build as good a bridge as is possible today (which means x86 only right now) for technologies that were, until now, bound to Windows. without this bridge, many users would also remain bound to Windows. this is all about making lemon-aide out of lemons for the short term and opening the way for more people to enjoy the Freedom of KDE. it is one piece among many of Konqi's abilities and compatibilities that no other linux/unix web browser can measure up to.\n\nand that, if you calmly re-read the press release, is exactly what was said."
    author: "Aaron J. Seigo"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "> I've put a lot of time in to KDE, and I care about it.\n\nThen I suggest you take a vacation or something and return when the whole of your brain is back up, because right now you're not helping at all, to say the least.\n\nYour take on this is nothing less than appalling."
    author: "Guillaume Laurent"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Neil - tell me, if Konqi didn't support any Netscape plugins, only displayed pages that were W3C compliant to the letter, and wouldn't open pages served from non-open source web servers, would you use it?\nThe point with ActiveX isn't saying that KDE's native technologies aren't better, it's simply admitting the reality that KDE's technologies aren't being embraced by major companies yet, and for users to migrate to Linux, they need these technologies now.\nI might note that many programs that have major lacks are still enjoyed. I really like KMail, Kicker, XMMS, Konqueror, KNewsTicker, etc. but still there are thing I would love to see supported that aren't _yet_. That doesn't mean I am not pleased with them, I simply notice they lack certain things I would like. I might note Konqueror has JavaScript problems too, but if Dre announced that they had all been fixed, that wouldn't mean that the KDE Project was really saying \"Until now, Konqi really stinked like week old fish!\"\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-12
    body: "It's always like this. If any product is \"criticized\" by its own makers, you can bet there is a newer version of that product which solves those problems."
    author: "AB"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "Okay, here is my 'take' on this subject...\n\nTechnically, I like what has been done here. I think the guys have done a wonderful job in a short space of time.\n\nAnd I agree with WildFox's sentiments that it *could* help companies migrate over to KDE.\n\nIt's a shame that the implementation isn't portable, but with the absence of file formats and designs, it is going to be nearly impossible to implement this on a lot of operating systems :(\n\nAs a final point, I don't see it as a bad thing to embrace older/other technology with KDE. If work such as this allows (some) KDE users to access information that they couldn't before, surely that is a good thing? Sure, it would be better if *all* KDE users could use this technology, but 'some' has to be better than 'none'.\n\nJust my 0.02 euros\n\nTap"
    author: "Andy Fawcett"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "Excuse my offtopic, but i dont know where i can post my opinion about one thing in KDE.\n\nI really dont like the minimized KDE panel version. If we watch the Gnome panel or Windows panel minimized version (see the screenshots), we see that the KDE panel is less beautiful :-) than minimized gnome panel (or even Windows!).I think the problem are the small icons.\n\nI like the standar KDE panel size, but i like a lot the minimized version, because we have more space to work with the apps, but the look of actual panel (small version) is horrible! :-)\n\nAnd i want to say that KDE team are doing the best job i have ever seen! ...\n\nbyes! :-)\n\nI hope this post will be readen by a KDE look coder team."
    author: "Daniel"
  - subject: "Just make a Gnome icon theme"
    date: 2001-07-10
    body: "I think there even exists one. Personally I hate the blurred look of them, a real strain on the eyes as your brain tries to focus them.\n\nThe MS and the KDE icons are better, because they have a clear shape much more visible.\n\nKDE icons = symbols (cartoonish, like traffic signs)\nGnome icons = pictures (prettier, but harder to discern)\n\n\nApart from your dislike for the work of the KDE artists what else is the problem?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2001-07-10
    body: "Quote:>Apart from your dislike for the work of the KDE artists what else is the problem?\n\nI do not dislike the work of the artist team but I do have a what else problem. Why is the cvs server for such a long time unconnectable after a (beta or other)release?.\nNow I can't try out this new feature (reactivate). Is there a need for more mirrors??\nWould it help if there was created one more??"
    author: "wim bakker"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2001-07-10
    body: ">Why is the cvs server for such a long time unconnectable after a (beta or other)release?.\n\nI read somewhere that it's also the ftp-server. After releases it gets slashdotted so they try to decrease load by shutting down cvs."
    author: "Lenny"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2001-07-10
    body: "I think he meant that the panel ends up looking *very* cluttered. He kinda has a point. Although I would say, if you like the KPanel looking like as neat as the Windows one, you can remove the Logout/Lock panel and the pager and make the taskbar work for all desktops. Then you'd end up with something very similar. However if he want sstuff to disappear and reappear when you change size, I don't think anyones going to do it. It's illogical and unexpected and most users wouldn't be too pleased."
    author: "Bryan Feeney"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2001-07-10
    body: "Thats right - its too cluttered. It is almost strange how little need to be done to improve the appearence in many cases - just *remove* things (or economize and make consistent the stylistic elements - colors, textures, styles, lines etc as much as possible for better effect) \n\n- but at the same time it seems to be so hard get it done.\n\nBy the way: the KDE \"start\" icon should be changed. It\n\na. isn't simple enough to be an icon\nb. doesn't offer a clue (ie. a visual metaphor) for its function. Imagine asking a user \"What do you think clicking on this symbol would achieve?\"."
    author: "will"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2001-07-11
    body: "> b. doesn't offer a clue (ie. a visual\n> metaphor) for its function. Imagine asking a\n> user \"What do you think clicking on this\n> symbol would achieve?\".\n\nWhat would you suggest, a button that says \"Start\" that I must click to Stop my operating system.  That's intuitive.  C'mon, it is a desktop paradigm that has basically become a standard.  Anyone that has used a computer, or even one that hasn't, will learn in two seconds that to run a program, start help, etc. you click the K button--just like they would learn the same thing the first time they used Gnome(footprint button I think), Windows, or a Mac."
    author: "Kmax"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2001-07-11
    body: "Well, IMO usability is more important than what is politically correct, so - yes - \"start\" would be better. But there are probably many other ways to achieve this.\n\nGnome's footprint provides a visual metaphor and is in theory better - although I don't think it well enough drawn to make the point. Corel used a globe, which is also not entirely good, but is also at least a better attempt.\n\nYou are saying that everyone knows anyway so it isn't needed. A well executed icon would make the interface more inviting, intuitive and professional. It makes a difference and would be recognized as making a difference. To avoid a better solution just because you don't have to is amateurship and slacker mentality."
    author: "will"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2001-07-12
    body: ">Gnome's footprint provides a visual metaphor \n>Corel used a globe, which is also not entirely good, but is also at least a better attempt.\n\nI'm sorry, I don't see how a foot or a globe is a better metaphor than a gear with a \"K.\"  They are all the repsective logos of the organizations that produced the software.  None of them have anything to do with launching programs/logging out."
    author: "not me"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2001-07-13
    body: ">I'm sorry, I don't see how a foot or a globe is a better metaphor than a gear with a \"K.\" They are all the repsective logos of the organizations that produced the software. None of them have anything to do with launching programs/logging out.\n\n:)\n\nI take it that the footprint indicates \"where you want to go\" - you metaphorically \"walk a path\" when you navigate the menus to start a program. I guess this is what the Gnome people had in mind when they made it.\n\nThe globe is actually a similar metaphor as well - think of the MS-slogan \"where do you want to go today\". The globe indicates that you have the world at disposal (presumably by bookmarks etc) and intentionally blurring the distincticion between the internal workings of the computer and what you do out there on the net (after all \"the network is the computer\". A globe isn't a logo for Corel is it? \n\nStill, they both fail the test of asking the user (not having this information beforehand) \"what do you think pressing this icon would achive\"? But MS \"start\" does not - which is by no means a trivial achievement."
    author: "will"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2007-11-08
    body: "Not gonna validate this guy? This sort of design mentality is just what we need to make things more accessible."
    author: "Hyd"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2001-07-11
    body: "It would be nice if workspaces panel could be removed.\nMany people (like myself) do not use more than one\nworkspace and find the panel annoying. At some\npoint (maybe when I am done with my thesis) I was\nplanning to customize kwin to my liking (coding\na theme allowing for no title bar and just a\ntab on the side, kinda like flwm and beos combined),\nand finally banishing the workspaces panel. But\nmaybe someone has done or is doing those things?"
    author: "All_troll_no_tech"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2001-07-11
    body: "It's really easy to remove the desktop switcher - it's just like every other applet on the panel.  Right-click the applet handle (the little thing to the left of the applet that you can move it with) and click \"Remove.\"  You hadn't discovered this?"
    author: "not me"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2001-07-12
    body: "No, I hadn't. I was trying to tell kwin to\nonly have one workspace but it doesn't accept 1\nfor some reason."
    author: "All_troll_no_tech"
  - subject: "Re: Just make a Gnome icon theme"
    date: 2001-07-13
    body: "The screenshot he attached is horrible.  The Gnome icons do not look blurred like that on my system.  \n\nhttp://dolinux.dyn.dhs.org/screenshots/2001_07_09_175145_shot.png http://dolinux.dyn.dhs.org/screenshots/2001_07_11_212426_shot.png"
    author: "Jamin P. Gray"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "there are mailing lists for this sort of thing, both for users and for developers. visit kde.org to find them.\n\nas for your issues, what exactly don't you like about the small panel (you were rather vague)? you know that you can turn autohiding on, that you can remove the arrows, that you can apply themes that change the appearance of the panel, that you can rearrange/remove applets, turn on icon zooming, and more, right?\n\nalso, if you post with an email addy you might get some good responses in that format."
    author: "Aaron J. Seigo"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Sorry about my post if i could annoy somebody, but i was talking about the look&feel of the ICONS when you use the less size of the panel. If you see the screenshots, there is a big diference. I am not talking about to copy Windows or Gnome, only saying that both of them are a bit more beautiful than the KDE one (ONLY the small ICONS as you can see in the screenshots).\n\nThis is only thing i dont like in KDE, because i always work with this panel size (then, i have more space to work with apps).\n\nThis is the only thing i dont like. I like a lot KDE :-) ...\n\nBest regards"
    author: "Daniel"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Actually, I prefer the small KDE icons over the small GNOME icons shown there, I think they look much better.  I also like the \"icon zooming\" feature.  Looking at that comparison screenshot, (which BTW is very interesting :-) I notice that KDE spaces its icons farther apart than GNOME and Windows.  I'm not sure if this is good or bad, it is sort of a tradeoff.\n\nThe overall look of Kicker is rather cluttered, though.  Perhaps \"Fade out applet handles\" should be on by default, and the Logout and Lock buttons off?  The left hide button should _definitely_ be off by default (I think it is now in CVS). And that \"LCD\" clock has got to go.  What LCD has a brown and beige striped backround anyway?  I much prefer the Fuzzy clock, it rocks!"
    author: "not me"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2002-08-09
    body: "Icon zooming is a cool idea, but... there is no transition when the zooming happens.  It goes right from small to medium or medium to large icons and that looks pretty jerky.  Mac OSX does a much better job."
    author: "anonymous"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2002-08-09
    body: "Icon zooming is a cool idea, but... there is no transition when the zooming happens.  It goes right from small to medium or medium to large icons and that looks pretty jerky.  Mac OSX does a much better job."
    author: "anonymous"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "Erm, what's the problem?  To me, the top panel there is very poorly done.  Those lines in the background at the left and right are overly annoying, there are too many little, hard to make out icons.  The second one is nice, because you can see the icons, the background isn't half some wacky pixmap.  Plus it's very easy to move things around on it (ie, put the tray on the left, etc).  The bottom one is about like the second one, with way fewer features.  All in all, my preference is 2, 3, 1, in that order.  So please realize that what you find nice looking, others may not."
    author: "KDE User"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "> I think the problem are the small icons.\n\nActually it would be nice to have Alphablending for small icons and toolbar icons. This would improve the overall appearance of KDE quite a bit.\n\nAlso I agree that  the application icons need quite a bit of an overhaul.  I hope that we'll have some time for this task before KDE 2.2.\n\nPleasing the people who prefer a simple 2-D shape and something that looks sharp as well as those people who like a rather blurry 3-D shape with a rather photorealistic look is difficult as you might see from the replies.\n\nLooking at the most recent Nautilus-icons it seems to me that Gnome also heads into the 2D-ish direction now with rather clear shapes just like KDE already does. The reason is better usability - and usability is of course always more important than eyecandy in the eyes of the beholder. \n\nIf you want to help just send your improved icons to icons@kde.org or add alphablending to the kdelibs-code for small icons and toolbar-icons. \n\nAlso note that we'll get  RENDER-supported Alphablending \"for free\" as soon as we move to QT 3.0. \n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "doesn't look bad to me..."
    date: 2001-07-12
    body: "personally, I find kde2 to be a lot sleeker and polished than either windows or gnome, and the panel looks pretty good too, imho"
    author: "ralian"
  - subject: "Re: doesn't look bad to me..."
    date: 2001-07-12
    body: "Agreed. 2D icons are better than blurry 3D ones."
    author: "AB"
  - subject: "Konqueror sucks"
    date: 2001-07-10
    body: "...it seems to me Konqueror is fast becoming the best browser on any platform.\n\nKonqueror is too slow, too ugly, have too problems with strange html and definitively its not the best browser...\nI think it will be good to suport Mozilla QT port... Mozilla renders all pages without problems, and now it's engine it's fast enought (try moz 0.9.2 & galeon 0.11.1).\nThere's another project, dillo, what is the fastest browser i've seen, and its growing a lot, everyday... GTK-only :("
    author: "Anonymous Coward"
  - subject: "Re: Konqueror sucks"
    date: 2001-07-10
    body: "You _do_ realise that gtk+ is used by Mozilla only for drawing, right?\n\nIOW, there is no visible difference between gtk+ Mozilla and the Qt port."
    author: "Jeremy M. Jancsary"
  - subject: "Re: Konqueror sucks"
    date: 2001-07-10
    body: "This guys just trying to be a cleaver troll. Everyone knows Konqueror is faster than that beast mozilla.\n\nCraig"
    author: "craig"
  - subject: "Konqueror vs Mozilla, Gecko & Konqueror"
    date: 2001-07-10
    body: "not true, actually.. rendering wise that is. \n\nkonqueror *is* slow on slow computers which seems to be because it shows content the moment it comes in instead of waiting a little causing the layout of a page to refresh many times.. taking a great deal of cycles and making konqueror completely unresponsive.\n\nmozilla on the otherhand, renders pages usually in one go causing it to actually be less processor intensive than konqueror.\n\ndon't get me wrong, i love konqueror. i just won't use it because on my low end machines mozilla actually does a better job. throw away mozilla's achilles heel: the GUI and you have one hell of a browser (galeon).\n\nbtw, does anyone know where i can find information on using konqueror with gecko as it's rendering engine?"
    author: "William Leese"
  - subject: "Re: Konqueror vs Mozilla, Gecko & Konqueror"
    date: 2001-07-11
    body: "I guess it just needs to me smarter about it.  Internet Explorer displays content as soon as it can too and that's probably the best browser right now."
    author: "Nobody"
  - subject: "Re: Konqueror vs Mozilla, Gecko & Konqueror"
    date: 2001-07-11
    body: ">Internet Explorer displays content as soon as it can too \n\nNo it doesn't.  IE doesn't display tables until they are fully downloaded, which can be a real pain on a modem connection loading a big slashdot page or something, because most of the entire page is one big table and so it won't display until it's all downloaded.\n\nIt does this because it can't know what size the table's columns and rows will be until it gets every column and row downloaded.\n\nKonqueror displays tables as soon as they start downloading and adds new stuff as it comes in.  This is good for sites like Slashdot, but on some sites it can cause the page to shuffle around in an annoying way as table widths change.  Plus it takes more CPU time."
    author: "not me"
  - subject: "Re: Konqueror vs Mozilla, Gecko & Konqueror"
    date: 2001-07-12
    body: "\"Konqueror displays tables as soon as they start downloading and adds new stuff as it comes in. This is good for sites like Slashdot, but on some sites it can cause the page to shuffle around in an annoying way as table widths change. Plus it takes more CPU time.\"\n\nPlease tell me that this can be disabled! It has annoyed the crap out of me all the time."
    author: "Erik"
  - subject: "Re: Konqueror vs Mozilla, Gecko & Konqueror"
    date: 2003-11-09
    body: "You might be right, since IE is quite good. I use mozilla myself, but would \nuse opera if it would not be not free.\n\nTo sum it all together, there is no escape from the konquest of machines."
    author: "Lempi\u00e4l\u00e4"
  - subject: "Re: Konqueror vs Mozilla, Gecko & Konqueror"
    date: 2001-07-11
    body: "if you haven't tried 2.2 yet you might notice that this behaviour has been modified  recently so that it doesn't render as soon as it gets data but waits a bit to see if it gets more data shortly thereafter... basically it pauses... renders a bit... pauses... renders.. all the while geting data. i've noticed this cuts down on the rerendering quite a bit.\n\nanother cause of slowness in konqi is that it doesn't pipeline HTTP requests. work on this matter seems to be at the \"let's discuss how we should tackle this problem and start some early test development\" stage.. hopefully 2.3 will address the pipelining/multiple slave issues in a comprehensive manner. (kiotar also suffers from this)"
    author: "Aaron J. Seigo"
  - subject: "Re: Konqueror vs Mozilla, Gecko & Konqueror"
    date: 2001-07-11
    body: "I'm afraid i see no difference with konqueror 2.2-beta1 rendering behavior than previous versions.\n\nPerhaps it didn't make it for the beta release."
    author: "William Leese"
  - subject: "Re: Konqueror vs Mozilla, Gecko & Konqueror"
    date: 2001-07-13
    body: "Why cant this behavior be configurable. even on a per page basis. setting the pause intervals on a render pause render pause render pause render scheme sounds great also allowing me to say DONT render until the entire page is downloaded because i have a VERY slow machine. or the IE behavior wait for the full table to download ( though this is VERY bad because sometimes IE will not display tables if a </TABLE> tag is missing). this is open source. and personally i think people should be free to configure things down to the last minuta if that's what they want."
    author: "Adam Jacob Muller"
  - subject: "Re: Konqueror vs Mozilla, Gecko & Konqueror"
    date: 2004-12-15
    body: "whhooee:D\nI love yoooouuuuuuuuu:D:D:D"
    author: "sas"
  - subject: "Re: Konqueror sucks"
    date: 2001-07-13
    body: "But the Mozilla rendering engine is incredibly fast.\nEver tried the latest Galeon yet?"
    author: "dc"
  - subject: "Re: Konqueror sucks"
    date: 2003-11-09
    body: "Not really."
    author: "Lempi\u00e4l\u00e4"
  - subject: "Troll-Reply: Re: Konqueror sucks"
    date: 2001-07-10
    body: "Hi Troll,\n\n> have too problems with strange html and definitively its not the best browser...\n\nPersonally I think that it is the best browser. If you discover a bug you should fill out a bugreport at bugs.kde.org.\n\n> I think it will be good to suport Mozilla QT port\n\nNobody keeps you from supporting the Mozilla QT port and nobody keeps you from improving the Mozilla bindings fon konqueror (you'll find them in kdebindings)."
    author: "Tackat"
  - subject: "Re: Konqueror (does not) sucks"
    date: 2004-04-07
    body: "Konqueror is the fastest browser I know. \n\nIf you have a fast connection (like 5mbs download like mine), and you go to www.numion.com (http://www.numion.com/YourSpeed/Checkup.php?L=world&Duration=30&Repeat=600&Layout=1) and test each browser you will see which is the fastest. Konqueror goes ~2,700killobit/second wile Mozilla and Firefox goes ~400killobit/second. Internet Explorer went ~800killobit/second. I know the speed is less then my connections full speed, but it's only browsing not downloading big files. And I tested all of them sevrel times at different time of the year, and the result always came to be very similar."
    author: "Konqueror IS the fastest."
  - subject: "Re: Konqueror sucks"
    date: 2004-04-27
    body: "IE less useful than lynx - konqueror rules"
    author: "asdf"
  - subject: "Re: Konqueror sucks"
    date: 2004-08-04
    body: "Personally I can tell you I hate konqueror. But then by design the developers never set out to be the best browser, only the best overall application.  The thinking in linux is that as long as we follow the standards set by the standard makers and do not budge, people will start to realize that in order to use our application and see things as they SHOULD be seen.. then THEY have to conform to the standards as well.\n\nKonqueror Developers just like Linux developers could care less if you can see web pages properly or if something is outputting correctly.  All they care about is creating a product that is standardized.  Hello???  Do you really think I am going spend my time redesigning my websites so that they work in Konqueror for 1% of the population and no where else cause of some stupid Linux developer thinking.. Good luck!\n\nIf it was up to me I would toss konqueror to the dogs.. it rarely works properly.. it does not view sites created with ms fonts properly.. if you even make a single change to the font sizes your screwed for life, One page is fine the next is way off.  It does not understand half the commands you would normally use in every day life.  etc...etc..etc... \n\nDo yourself a favour people.. download Firefox for Linux.. you will be up and running in a few minutes.. and it works a heluva lot better then konq.\n\nI am in no way bad mouthing linux.. I love linux... I just hate konq..\n\nKind regards, \nredfrog"
    author: "redfrog"
  - subject: "Re: Konqueror sucks"
    date: 2004-08-04
    body: "> Konqueror Developers just like Linux developers could care less if you can see web pages properly or if something is outputting correctly\n\nthey obviously do care-- look at bugs.kde.org. \n\nPlease read http://weblogs.mozillazine.org/hyatt/archives/2003_11.html for a indication about how large the web is, and how hard supporting every webpage is. "
    author: "anon"
  - subject: "Re: Konqueror sucks"
    date: 2004-08-05
    body: "You may be right - you give me the distinct impression I couldn't give a shit if your websites work."
    author: "Richard Moore"
  - subject: "Strange..."
    date: 2005-12-30
    body: "You seem to denigrate Konqueror, but you, as a web designer should've used your brain to realize that every apple/mac OS/darwin BSD user is going to be a SAFARI user, which USES KONQUEROR as its base... where they merely rewrote the QT engine in C++ if my memory serves correctly.  This not meaning that they replaced any other parts of Konqueror.\n\nAs a double test, I believe the only browser other than Opera and Safari that passes the ACID2 test (google it) is Konqueror.  This means that it interprets the HTML properly.... hmmm, I wonder why Firefox and IE can't do it right?  Could it be they are using shortcuts to render things as may not be specified in the whitepapers at w3c?  (after all they DO set the standards for the web... even if microsoft rarely abides by them, the OSS community cares about this a bit more than you do)\n\n~Khye"
    author: "khyeron"
  - subject: "Not any more"
    date: 2006-02-09
    body: "Remember, this is an old discussion.\n\nKonqueror was pretty sucky back in those days, but how things have changed!\n\nNow it's my browser of choice. When I'm forced to use Windows, I can't stand using any browser other than Konqueror (3.5.*).\n\nAnd tabbed file management - the most fantastic feature ever. Once you've tried it, you'll never go back."
    author: "Flangitaire"
  - subject: "Re: Not any more"
    date: 2006-02-09
    body: "Just to clarify the above statement:\n\nWhen I'm forced to use Windows at work, I get frustrated by not being able to use Konqueror (yes, I know about cygwin etc.)\n\nIt's a fantastic file manager and browser."
    author: "Flangitaire"
  - subject: "Re: Konqueror sucks - use Firefox"
    date: 2006-07-03
    body: "I would like to download Firefox but I am using Konqueror and when I go to the Firefox page the download icon does not appear. After screwing around for ever i can find a page with a linux download icon. When I click the icon, Kongueror renames the file and downloads the file somewhere. Konqueror does not prompt me for a download location. When I finally find the file I can't unzip it. What am I doing wrong?\n\nAfter 2 days I am still not up and running with Firefox."
    author: "Michael H"
  - subject: "Re: Konqueror sucks - use Firefox"
    date: 2006-07-13
    body: "Ha ha ;)\n\nYou can always use ftp://ftp.mozilla.org to download mozilla/firefox.\n\nThe installation process is somewhat more complicated. The easiest thing to do is to use your particular brand of linux's package management system. For instance on fedora typing 'yum install firefox' (as root) would install firefox. Using this method you do not need to download firefox in advance.\n\nOtherwise you need to install whatever it is you downloaded. If it is a rpm, rpm -i <filename> might do something. Otherwise if it is a tar.gz file tar xvfz <filename> will uncompress the file. Then traverse the directory tree until you find the firefox binary... If it is a tar.bz, then try tar xvfj <filename>.\n\nIf it is a zip file, chances are you downloaded the windows version."
    author: "Generic User"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "Awesome! Now if Konq could actaully work with my online banking...... (Note: I don't believe its Konq's problem... My online banking has a Java applet where the classes are suppose to be loaded by https, and I don't think the Java plugin supports loading of classes via https.. I have the same problem w/ mozilla..)..."
    author: "garion911"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "That's cheese. The java you need works in the newest version of KDE."
    author: "henrik"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "I've been running KDE 2.2Beta2 w/ JRE 1.3.0_01, and it doesn't load classes via HTTPS.. The JRE can handle HTTPS within the applet, but it can't load it via HTTPS... Its something to do with the ClassLoader itself.\n\n--Garion"
    author: "garion911"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: ">The JRE can handle HTTPS within the applet, but it can't load it via HTTPS.\n\nNot sure if I'm confusing something but did you install Sun Microsystem's JSSE as mentioned on http://www.konqueror.org/konq-java.html ?"
    author: "someone"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-12
    body: "I have the same problem with both of\nmy banks, e.g.\nhttp://www.sparkasse-gnp.de\neven doesn't get rendered correctly.\nCompare the page with old netscape 4.72,\nyou'll realize, that the right menu is missing.\nToo sad.\n\nBest wishes,\nPeter\n\nP.S. Yes I did install JSSE,\non my other bank account on a VOlksbank\nI can start the applet, but after that I get a\nsytem error within the applet :("
    author: "Peter Schmitteckert"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "Well, I did think KDE's browser was a great thing..\nBut now I can kiss it good-by if its going to have Microsoft unsecurity to it and I won't use Gnome since it wants to copy the .NNNET crap too..\n\nIf you want to have Active X in the browser, go use windows.."
    author: "Harold"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "What the hell's your problem?  The Linux police aren't going to show up at your house tomorrow and make you install it."
    author: "Josh"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Then don't use this new tool, and don't use Mono when it comes out. BTW, Linux supports many Microsoft technologies... well I'm not going to even go there...\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "No one would ever force you to use it. We do not intend to enable it by default so whoever wants to use it will have to deliberately turn it on. Also, at least in the current state it takes even more action to install it because of the neccessary WINE patches. If you don't have a WINE version with our patches or no WINE at all, it won't be compiled at all.\nNo need to abandon Konqueror or KDE because of this.\nDon't like/want ActiveX? Don't turn it on.\nDon't like/want Java? Don't turn it on.\nDon't like/want JavaScript? Don't turn it on.\n..."
    author: "Malte"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-08-29
    body: "your loss"
    author: "MrGrim"
  - subject: "Embedding KParts"
    date: 2001-07-10
    body: "Does \"embedding kparts\" mean that it is possible to use the <object> tag to insert a KPart similar to a ActiveX control or a Netscape plugin (and if yes: where is this documented?) or does it only refer to the usual embedding of documents?\nIn the latter case the text is quite confusing. Embedding KParts in HTML could be very interresting though, especially for HTML-based administration tools and things like that."
    author: "Tim Jansen"
  - subject: "Re: Embedding KParts"
    date: 2001-07-11
    body: "You can embed KParts by using <embed> orp referably <object> tags. The KPart to be used will be determined by the MIME type of the data you want to embed. E.g. this will embed the KWord part into a web page showing foobar.kwd:\n<object width=\"400\" height=\"300\" data=\"foobar.kwd\" type=\"application/x-kword\">\n</object>"
    author: "Malte"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "There's a test of webbrowsers for linux in the German Linux-Magazin. Konqueror-2.1.2 comes off badly. It was tested with i-Bench 2.0 from Ziff Davis Media, Inc. Seven browsers (Beonex-0.6pre, Galeon-0.11.0, Konqueror-2.1.2, Mozilla-0.91, Netscape-4.77, Netscape-6, Opera-5.01) were tested, also IE-6.0Beta for comparison.\n\nResults in short: IE-6.0Beta is the fastest and the best in compatibility with standards. Konqueror is not very compatible to standards. Only Netscpae-4.77 is less compatible to HTML-4.0 than Konqueror. Konqueror is also slowly. Only Netscape-6 and Beonex are slower than konqueror.\n\nConclusion: It should be done more for speed, stability and compatibility than adding new features!"
    author: "Matthias"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-10
    body: "> Conclusion: It should be done more for speed, stability and compatibility than adding new features!\n\nI'm sorry, what are you saying?  That nobody should develop plugins and addons to Konqueror until the Konqueror developers improve KHTML speed and stability?  This is a volunteer project where people work on the itch they feel, not according to the schedule you would impose.\n\nAs to compatability, if you could \"closed\" web standards, this does add a lot, wouldn't you say?"
    author: "Confused"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "> I'm sorry, what are you saying? That nobody should develop plugins and addons to Konqueror until the Konqueror developers improve KHTML speed and stability?\n\nNo, of course not! There's also a way between that.\n\n> This is a volunteer project where people work on the itch they feel,..\n\nBut this is in my opinion a general problem of open source software. Nobody likes fixing bugs. It's cool to introduce new features. And so an advantage from linux gets lost: stability (ok, speed is not so important). But what can I do with a webbrowser with lots of cool features when I cannot use it.\nThat's surely overstated and I also use Konqueror even as my standard browser, but till now there was no KDE-release which does it for me (not because of features but because of stability). I would like to miss features if I can have more stability instead.\n\nAnd besides this, I was trying direct attention to this article maybe in the hope to stimulate somebody to improve konqueror referring to this.\n(And that's what with open source can done much easier.)\n\nBut for all that I love and use KDE. :-)\n\nPS: The difference between Konqueror and KHTML can't be found from an user point of view."
    author: "Matthias"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "> > This is a volunteer project where people work on the itch they feel,..\n>\n> But this is in my opinion a general problem of open source software.\n> Nobody likes fixing bugs. It's cool to introduce new features. And so\n> an advantage from linux gets lost: stability (ok, speed is not so\n> important).\n \nYou are missing the point.  The two developers are not Konqueror\ndevelopers.  It wasn't like they put aside Konqueror development to\ndo this project.  They got involved in Konqueror development through\nthis project.\n \n> But what can I do with a webbrowser with lots of cool\n> features when I cannot use it.  That's surely overstated and I also\n> use Konqueror even as my standard browser, but till now there was\n> no KDE-release which does it for me (not because of features but\n> because of stability). I would like to miss features if I can have\n> more stability instead.\n \n[ ... ]\n \n> PS: The difference between Konqueror and KHTML can't be found from an user point of view.\n \nWell you can tell b/c you start to see more apps using KTHM for online\nhelp browsing, documentation and now there is even a KControl module\nthat embeds KHTML to permit using browser-based config tools in the\nControl Center."
    author: "Confused"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Yes, and this article is widely known to be biased, misinformed and in several cases simply _wrong_. Better read the article, find the \"mistakes\" and complain to LinuxMagazin and the author."
    author: "ac"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "> Yes, and this article is widely known to be biased, misinformed and in several cases simply _wrong_.\n\nHmmm... I don't notice that there was already talk about it and I also don't read a comment about the article in KDE-news with a correction of this errors.\n\n> Better read the article, find the \"mistakes\" and complain to LinuxMagazin and the author.\n\nHow can I find mistakes when I don't know it better? I read the article to inform me about the topic. A person like you had to complain to the author, otherwise many people (like me) read it and think it's true.\n(I don't mean this personally but the logic was not right.)\n\nBy the way: What are the mistakes in the article? Are there links about a discussion to it?"
    author: "Matthias"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Well, since the article isn't available online, you're not going to find much discussion of it online.  I'd never even heard of it.  However, a post here says that they reported that Java doesn't work with Konqeror.  If they did report that, I can't possibly imagine that their review is much good.  That's almost certainly a distribution problem, nothing to do with Konqueror.\n\nI am interested in why Konqeror got such a low standards rating.  Did they do tests that indicated it had problems with HTML or CSS compatibility, or did they just complain about Java?"
    author: "not me"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "I have read this article and something made me confused about that: The article says that for example JAVA does not start with konqueror.\n\nOn my machine Java and konqueror is no problem. I think they had distribution based problems.\n\nThe other thing is that that the testing was compared to the newest IE beta from Windows XP;\nkonqueror was only tested in kde 2.1.x version. Why haven't they taken the newest konqueror version from CVS ???\n\nI know that konqueror could be faster and more stable, but i think that a test should be made on unique coditions.\n\nP.S.: I know the \"stable\" IE5 and often this one gets stuck without any chance to reproduce the bug."
    author: "Tom"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Java does work with Konqueror, but it is by default not enabled - you have to go to configuration section and select the checkbox to enable it (the same is true of Javascript).\n\nIf the reviewers didn't even bother looking through the configuration dialogs, I imagine they got a lot of other things wrong as well."
    author: "Jon"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "huh, well, standards and actual usage are two different things. I've found Konqi renders waaaay better than Netscape or Opera (any version), and is faster any day.\n\nBTW, ActiveX = compatibility.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Its a synthetic browser test. I don't know if you can say the author is biased. Tackat and I had some fun with this guy on www.linux-community.de cause he first did the tests with a Konqi 1.9.x ;-) You can make the test yourself:\n\nhttp://i-bench.zdnet.com/ibench/testlist/home_js.php\n\nYou will see, Konqi has some problems with this test. But reality it different, using the internet with Konqi is fun. Ok, there are some problems (most notably JaveScript), but if we give KDE developers the task: Konqi needs a 100% result in this benchmark, I think there would be not so much improvement in daily browsing.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: ">Konqueror has received another huge shot in the arm, \n\nFor implementing the security nightmare that  is ActiveX controls in a web brower, you deserve a shot in the head, not the arm."
    author: "Choadzilla"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "This does not decrease the security of the browser itself, only when the browser is using an ActiveX control. Read the discussion above about chrooting."
    author: "Carbon"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Although I'm reading this with Opera and will <BR> soon switch to a Mac. (wine won't work),<BR> \nI wan't congratulate the Konquerer developers.<BR> \nYou're doing a great job and keep up the work.<BR> \nYou are the guys that will prove that <BR> Linux-Desktop lives more than ever.<BR> \n<BR> <BR> \nTHANX"
    author: "matthias"
  - subject: "embedding X windows"
    date: 2001-07-11
    body: "Speaking of embedding X windows, any idea whether Konqueror will ever be able to embed java applets/windows within itself on window managers other than kwin?"
    author: "cosmo"
  - subject: "Re: embedding X windows"
    date: 2001-07-12
    body: "That's something I never understood,\nwhy we don't have X11 windows in todays\nbrowsers. We could serve a complete remote\nKDE desktop if the Browser infrastructure would allows this. \n\nBest wishes,\nPeter"
    author: "Peter Schmitteckert"
  - subject: "Microsoft.Net"
    date: 2001-07-11
    body: "Hey everybody!\n\nI can't understand why people are Kicking MS about their .NET services. It really is a solidly laid-out architecture. If you have been getting MSDN Cd's you would realize that it is a VERY powerful set of tools that run on a nicely integrated environment. I have been following XML for a while and have not seen ANY large scale support, I am glad to see that MS will push this standard into the mainstream.\n\nThanks\nChris"
    author: "Chris (HUPER) Hughes"
  - subject: "Re: Microsoft.Net"
    date: 2001-07-11
    body: "Microsoft lackey!"
    author: "S Lone"
  - subject: "Re: Microsoft.Net"
    date: 2001-07-11
    body: "Knee-jerk zealot!"
    author: "not me"
  - subject: "Re: Microsoft.Net"
    date: 2001-07-11
    body: "Oh yeah, I'm a turban!"
    author: "Carbon"
  - subject: "Re: Microsoft.Net"
    date: 2001-07-11
    body: "I'm not sure if you could say Microsoft is pushing out standards, only their twisted version of standards... but still I'm glad to see .net support in Linux. It will mean that Microsoft will not be able to dominate next generation computing."
    author: "Timothy R. Butler"
  - subject: "Re: Microsoft.Net"
    date: 2001-07-12
    body: ">I have been following XML for a while and have not seen ANY large scale support\n\nLook at java or beter yet j2ee, plus Micro$oft has it's own XML standart, plus what does that have to do with ActiveX except for letter X?"
    author: "Alex"
  - subject: "Java and Flash first"
    date: 2001-07-11
    body: "I am dubitative. Today Konqueror is installed without Flash and  Java (by Mandrake 8.0 for instance).\n\nYou may say that it is possible to add such plugins, but it is complex... So Konqui don't know Flash and Java for many users.\n\nAbout Shockwave I feel it will be more difficult, because there are Wine things and they are not easy to install.\n\nSo nothing will change. And it's not important, I don't worry, I don't like sites using Shockave or other ActiveX gadgets... \n\nYes, Shockwave is also unpopular..."
    author: "Alain"
  - subject: "Re: Java and Flash first"
    date: 2001-07-11
    body: "I see your point, but this is not exactly Konquerors problem but the problem of the distribution (and the legal situation of proprietary things like Flash). Write Mandrake that you want your Konqueror pre-installed with Flash and Java. If they cannot distribute Flash, tell them that they should write some script that automatically downloads and installs it for you."
    author: "Tim Jansen"
  - subject: "Re: Java and Flash first"
    date: 2001-07-11
    body: "Tim says :\n\n> this is not exactly Konquerors problem but the problem of the distribution \n\nSo, please don't say that Java and Flash run with Konqueror, it is false.\n\nToday Java and Flash work with Suze-Konqueror and don't work with Mandrake-Konqueror and many other distribs.\n\nI think that the Konqueror team has to help distribs (write some scripts...) so that they all will deliver Konqui with Flash and Java, and in the same way (so that we may easily change distrib...). It will be useful, also, for advanced users...\n\nIt's too easy to say it works when it don't work for many and many users. It's too easy to say the fault comes from distribs..."
    author: "Alain"
  - subject: "Re: Java and Flash first"
    date: 2001-07-11
    body: "I am using Konqueror on Debian and Flash works fine. I never tried Java though."
    author: "Tim Jansen"
  - subject: "Re: Java and Flash first"
    date: 2001-07-11
    body: "> So, please don't say that Java and Flash run \n> with Konqueror, it is false.\n\nNonsense.  I have been using Java and Flash with my Mandrake 8.0 since Day 1.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Java and Flash first"
    date: 2001-07-11
    body: "It depends on which are the installed rpms. Mandrake 8.0 includes this support in an RPM called kdensplugins, or something similar. I had to install it after initial system installation. And it works very nicely."
    author: "Juan"
  - subject: "Re: Java and Flash first"
    date: 2001-07-12
    body: "It seems impossible with the 2 Mandrake basic CD :\n- the rpm netscape-plugins is absent and it is necessary for Flash\n- there is no java machine\n\nI asked on a Mandrake mailing list and also on the Usenet Kde forum : there was no other solution than following the Konqueror site manipulations...\n\nMy questions - and then the questions of other users - where seen by many users and NO ONE said it was commonplace, as you seem to say...\n\nPerhaps you made an update with Mandrake while netscape-plugins and java machine were already installed ? It is a very particular and exceptional situation..."
    author: "Alain"
  - subject: "Re: Java and Flash first"
    date: 2001-07-12
    body: "KDE cannot control what Mandrake decides to distribute or not -- so you should complain to them as a paying customer and not blindly flame KDE.  \n\nHOWEVER I, too, am a paying customer and bought my Mandrake Linux Standard Edition.  I can tell you it comes with netscape-plugins, FlashPlayer, etc and all this works flawlessly.  It's also true that as I develop Java, I already had the latest Java RPM from my previous Mandrake 7.2 installation.  Mandrake probably distributes it with their Plus Pack.\n\nSo please be reasonable and find the right people to complain to instead of accusing us of  making false claims."
    author: "Navindra Umanee"
  - subject: "Re: Java and Flash first"
    date: 2001-07-12
    body: "Navindra explains that he is not a basic Mandrake user using the two basic CD.\n\n> So please be reasonable and find the right people to complain to instead of accusing us of making false claims.\n\nSorry, I was right: in the basic Mandrake distrib and many other distribs, Konqueror is installed without Flash and Java. It is true, it is not a \"nonsense\", but a great trouble to improve.\n\n(yes I say \"great\" because it was the biggest trouble of my Mandrake install and I have seen other users with the same difficulties, so I am perhaps a little disturbing, but I don't want to see such things minimized !)\n\nI also said that it is possible to add Flash and Java with complex (for a basic user) manipulations. So now it is only for advanced users. I wish it will change... And I think that it will be better done by the Konqueror team than by the many distrib teams... \n\n(And I worry that it can be seen something like a \"blindly flame\". It is a constructive wish)"
    author: "Alain"
  - subject: "Re: Java and Flash first"
    date: 2001-07-12
    body: "I see, so you did not buy the official Mandrake? Or else what is this \"basic\" Mandrake that you talk about?  I only see the Standard Edition and Deluxe Edition here in Montreal stores.  Perhaps in your country it is different.\n\nYou also seem to not understand that KDE has no control over what Mandrake decides to do.  *YOU* do.  So talk to Mandrake and get it fixed...\n\nAnyway, I think I already said everything that I wanted, and more, in this conversation, I'm out.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Java and Flash first"
    date: 2001-07-12
    body: "> what is this \"basic\" Mandrake that you talk about? I\n\nOf course it is the download version and also the magazines version. (and be quite, I bought a standard edition before you and -perhaps - before anybody else, the first day of the avalaibility of Mandrake 5.6, in Paris).\n\nI am very surprised by your reaction. I have seen in this site that the KDE team wants to create more configuration tools (as do distribs), so I thought that it was a common wish to hope a KDE whole Konqueror configuration...."
    author: "Alain"
  - subject: "Re: Java and Flash first"
    date: 2001-07-12
    body: ">I thought that it was a common wish to hope a KDE whole Konqueror configuration\n\nYou misunderstand.  Navindra wants Konqueror's Flash and Java to work too, but she is telling you that it is not *KDE's* fault that *Mandrake's* distribution doesn't install Java and Flash by default.  If you want Konqueror to have Java and Flash working in Mandrake, you must talk to Mandrake.  There is unfortunately nothing KDE can do about it, because Flash and Java are not KDE programs and can't be included with the KDE packages (legal difficulties).\n\nThis discussion should be taking place at Mandrakeforum.com, where you would be able to talk to other Mandrake users about this Mandrake-specific problem."
    author: "not me"
  - subject: "Re: Java and Flash first"
    date: 2001-07-12
    body: "> There is unfortunately nothing KDE can do about it, because Flash and Java are not KDE programs and can't be included with the KDE packages (legal difficulties).\n\nI don't say that Flash and Java are to be included in KDE packages, I know there are legal difficulties.\n\nI said that the Konqueror team may write some scripts so that any distribution and any advanced user may easily (and in the same way) add Flash and Java to Konqueror so that it will be very simple and transparent (nothing to do) for the end users of distribs.\n\nAnd I add now that it is perhaps possible that the Konqueror team create a separate package according to legal things so that it installs all the plugins of Konqueror. So if a distrib is only GPLed, the user has only to download and install a rpm (or something like).\n\nI am sure that today a majority of Konqui users don't use Java because it is too difficult to install. Why say\nthat Shocwave is coming if tomorrow only a minority will install it, for the same reasons ?...\n\nDo you want a Konqueror easy to install (with all things) and everywhere installed in the same way ? Now it is not...\n\nI think it is the problem of KDE, yes.\n\n(knowing that the KDE team wants to do some configuration tools, as distribs, I think that such tools would at first exists for the KDE programs installation, of course...)"
    author: "Alain"
  - subject: "Re: Java and Flash first"
    date: 2002-09-03
    body: "Hi! Have just installed Mandrake 8.2. Im a newbie in this. Got the same problem. Maybe it is a good idea to include at least some kind of popup to give nwebies easy way to install Java and Flash."
    author: "Digger"
  - subject: "Re: Java and Flash first"
    date: 2002-12-28
    body: "Ohhhhh!!!!... My new Mandrake 9.0 (update at this date dic-2002) does not work correctly: java and flash are not implement in this distribution in any browsers. I think Mandrake is making BIG mistakes to the newbie Linux users.\n\n(And There are other important bugs)\n\nBye, Manuel Perez\n"
    author: "MANUEL PEREZ LOPEZ"
  - subject: "Re: Java and Flash first"
    date: 2002-12-29
    body: "they aren't \"not implemented\", you just need to install them yourself.  Are you using the free download edition of mdk, i believe that the for pay version comes with these non-free things pre-installed...\n"
    author: "domi"
  - subject: "Re: Java and Flash first"
    date: 2002-12-29
    body: "Install Netscape. This will solve all the probblems."
    author: "digger"
  - subject: "Re: Java and Flash first"
    date: 2003-06-02
    body: "Allow me to rant for a moment. Whenever I do a search for any sort of help with Linux, all I find is a bunch of whining about how hard Linux is. If all you want to do is complain, reformat your hard drive and pony up some cash to Bill Gates. He'll be glad to sell you the latest incarnation of Windows!\n\nI'm the type of person who drives a manual shift transmission in my car because it gives me a certain measure of control - within certain parameters, of course. I'm trying out Linux because I believe that, despite its sharper learning curve, the end result will be ultimatly more satisfying.\n\nI'm only a lowly, undesirable newbie, and I, too, would like to install not only Java in my Mandrake 9.0 operating system, but Java as well. Unfortunatly, I have little or no idea of how to accomplish this remarkable feat. I've been around computers in general for a number of years, even built my own box on a number of occasions as well as tinkered with DOS commands here and there. If any of you Linux gods out there are willing to take a fledgling under their protective wing, so to speak, PLEASE let me know! As for the crybabies out there, a: go back to Microsoft\n       b: remember to pick up some baby food the next time you go grocery shopping. You obviously need it!\n\ntotallynotcool@hotmail.com"
    author: "totallynotcool"
  - subject: "Re: Java and Flash first"
    date: 2003-06-02
    body: "After re-reading, I meant to say Java and Flash. No need to install Java twice, now is there?"
    author: "totallynotcool"
  - subject: "Re: Java and Flash first"
    date: 2004-07-19
    body: "HI!!! \n\nI have SuSE 9.1 and java works fine with Konqueror. I cannot say the same about flash that works when it wants!!\n\nI see that: if I open my home page in a tab (my home page contains a flash animation) i cannot see the animation. If I open, in another tab, the home page.. there I can see the animation.\n\nAfter this I can see flash animation until I cose Konqueror.\n\nAnyone has a reply for this??\n\nExcuse me for my english... \nALEX"
    author: "Alex"
  - subject: "This is almost good, but..."
    date: 2001-07-11
    body: "This will really please the x86 users of Linux, however this will still not make ActiveX available on non-x86 architectures.  Great job, shame it is (and can) only satisfy a small percentage of users. \n\nIt would be really neat if there was a hack for Wine to run Windows applications compiled for x86 under non-x86 architectures without emulation.. of course that would be asking for a lot :)\n\n--\nEric Windisch"
    author: "Eric Windisch"
  - subject: "Re: This is almost good, but..."
    date: 2001-07-11
    body: ">It would be really neat if there was a hack for Wine to run Windows applications compiled for x86 under non-x86 architectures without emulation.. of course that would be asking for a lot :)\n\nIt would be asking the impossible.  Emulation is required between different CPU architectures.\n\nNow, if you were a l33t h4x0r, you might port Wine itself to a different CPU architecture, allowing the API to run natively, and using an emulation solution such as Bochs (http://bochs.sourceforge.net) to run the x86 binary code.  Not sure if it's really possible, or even worthwile to implement, but it sounds cool :-)"
    author: "not me"
  - subject: "Re: This is almost good, but..."
    date: 2002-02-07
    body: "Actually.. Wine was (being?) ported to PowerPC. \n\nWell, one of the reasons you need an emulator to run Amiga apps on your x86 box is because there is no Amiga API implimented for your operating system on the x86 box. If you have an implimentation of the API, you can emulate a CPU and run applications without having to have a complete VM.\n\nIt would actually be possible to run Windows applications on PowerPC via any platform Wine supports.. without having a complete VM. \n\nI never said it would be easy..."
    author: "Eric Windisch"
  - subject: "Re: This is almost good, but..."
    date: 2002-02-22
    body: "Oh, btw.. \n\nHow do you think that MacOS9 on a PowerPC machine runs applications written for MacOS7 compiled for a m68k processor? :)"
    author: "Eric windisch"
  - subject: "Re: This is almost good, but..."
    date: 2002-09-10
    body: "If I remember correctly the PowerPC evolved from the IBM POWER architecture and was also developed by Motorolla, it could very well be that the 68000 op-codes form a subset of the PPC op-codes. This is certainly true for the POWER/POWERPC machines, which allows AIX to run on different architectures (this is not QUITE true, as AIX enquires as to the platform its running on and uses architecture optimised routines in the kernel, using a branch table). But the op-codes that can be used by the IBM RS/6000 compiler used for user space programs has to ensure that it will run on both. So this sort of thing is not new. The x86->PPC conversion is totally different.\n\n\tAndy"
    author: "Andy Richardson"
  - subject: "Re: This is almost good, but..."
    date: 2003-06-04
    body: "No powerpc and 68k are completely different. The only way to run programs from one on the other is through emulation.\n"
    author: "Bijan Soleymani"
  - subject: "Re: This is almost good, but..."
    date: 2003-06-04
    body: "No powerpc and 68k are completely different. The only way to run programs from one on the other is through emulation.\n"
    author: "Bijan Soleymani"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Thank-you a ton Nikolas and Malte! This is really an exciting and and amazing accomplishment! Now I just have to try reaktivate.\n\nBTW, does anyone know if perhaps this will be integrated into the main CVS for KDE 2.3?\n\n  thanks,\n       Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "Followers, not leaders.  Arguments for this are now people with Linux can do Linux and winblowz, and it costs less when users switch etc....  However, to beat your enemies, one shouldn't marry their daughter/son or marry into their family and hope to inherit your way to success.  Therefore, do you think that people will stop using the supported activex junk?  Or, they'll keep using it more and allowing micros~1 to set the standards?  Looks like you are to follow micros~1 in standards, just like everyone else, not set them.\n\nWhy is it that micros~1 are able to charge for trash, while you can't even get enough users by giving away your work?\n\n---\nCall me a troll or what you wish, but you entitle my opinion, regardless."
    author: "Konqulator"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "what's next, to port the BSOD and GPF's ?"
    author: "<b><i>for a bloatless Desktop</i></b>"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-12
    body: "Dear l33t hax0r,\n\nIf you find any exploits, let me know too please.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-12
    body: "<aol>me too</aol>\nFeel free to mail them to my office address\nsecurity@bovenet.at or security@above.net"
    author: "Darian Lanx"
  - subject: "Reduce the waiting period"
    date: 2001-07-11
    body: "One question about Konq. \n\nDoes Konq have the ability to pre-cache links on the page that you are visiting so that if you click on one of the links, that site has been giving some time to load already and you wait less."
    author: "kde-user"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-11
    body: "It's fairly interesting. When something is done in sparetime and absolutely free others aren't allowed to critizise the work. Those who dare are said to be flamers and co. This is amusing. Where is the freedom you talk about all the time?!\n\n\n\nregards\n\nstephan"
    author: "stephan"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-12
    body: ">When something is done in sparetime and absolutely free others aren't allowed to critizise the work.\n\nOn the contrary, constructive criticism is welcomed.  People who criticize a project based on unfounded assumptions and poorly-thought-out arguments get what they deserve.\n\nAnd trolls are just dumb.\n\nThe people who are criticizing this work just for existing (example: what next, port the BSOD?) are being trolls.  You don't have to use this, it probably won't even be included in KDE proper because of its dependence on WINE.\n\nBesides, they *are* being allowed to criticize.  No one is censoring them, and their views are being heard.  The fact that they are being called trolls and flamers doesn't inhibit their freedom of speech."
    author: "not me"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-12
    body: "I see people criticise all the time (without flames back at 'em). The key things are:\n\n   1.) Does the criticism help?\n   2.) Is it really logical or is it a very narrow view point?\n   3.) Are the posters mean or insulting to those who worked hard on this project?\n\n  Now I might note that on one post who shall go nameless's posts last night he took a narrow view saying that if it didn't work everywhere, and just how *HE* wanted it, it shouldn't be on the dot. Thus he received flames.\n  The other poster, who notme mentioned to you said something about porting BSOD's next. Any logical person knows that the developers didn't port ActiveX merely  to open up problems, and so this was obviously insulting.\n\n  When someone works in their freetime to do something like this, they certainly don't deserve, mindless, narrow minded, or down right insulting posts, don't you agree?\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Konqueror Gets Activ(eX)ated"
    date: 2001-07-18
    body: "Thanks, this is great.\nWe have been waiting a long time for\nshockwave on linux. \n\nregards M.Seiler\n<A href=\"http://www.shocktime.de\">http://www.shocktime.de</A>"
    author: "Martin Seiler"
  - subject: "Program Shockwave plugin from scratch?"
    date: 2001-07-20
    body: "Which are the limitations which make that programmers didn't make linux-native shockwave plugin yet?\nAre there legal limitations?"
    author: "Dav"
  - subject: "Re: Program Shockwave plugin from scratch?"
    date: 2001-09-30
    body: "There is one native FLASH implementation for Linux\n\nCheck this out:\nhttp://www.swift-tools.com/Flash/\n\nJari"
    author: "jari soderholm"
  - subject: "Re: Program Shockwave plugin from scratch?"
    date: 2003-06-28
    body: "Flash its NOT a shockwave-plug in shockwave uses director, its used for applications, games, tutorials, but there is not a linux-native shockwave player (as i know the only one that support shockwave its crossover plugin) but it is really slow, because it uses emulation to use shockwave for windows on linux, any way,\n\ni think if a lot of ppl its really organized, can ask macromedia for some technical information to make a shockwave plugin for linux community, of course, it wouldn't be really suported by macromedia,\n\na lot, because i've sent some mails to macromedia, and there was not response, some ppl have had a response from macromedia saying that they have no plans to make a linux native shockwave plugin,\n\nunless the free community develop its own plugin it think there is no way to have a Director shockwave player plug in for linux"
    author: "Carlos Morales"
  - subject: "Quit whining"
    date: 2001-10-06
    body: "This is good. Most of the security holes in activeX aren't applicable for linux anyway plus it's a PATCH. No one is forcing you to apply it! If you don't like activeX don't use it. Don't yell at the KDE League about progress. this isn't going to do the impossible, it's just a nifty hack. Don't whine about being insulted, wine NEVER ran on non x86 archs. \nJeeze.. some people.. can't appreciate anything. Just because you don't like activeX doesn't mean everyone else doesn't."
    author: "Scott"
  - subject: "Compileing reAktivate"
    date: 2002-07-15
    body: "I'm a newbye on linux and i'm trying to install reAktivate, I can't undestand why Macromedia doesn't make a Shockwave player for linux, but that's not the problem, the problem is, how can i compile reAktivate? You will be probably laughting a lot now, but i think the documentation(if you call this documentation) of how to install reAktivate is horrible. I installed wine with the patch, but now i don't know how to compile the reAktivate source, can anyone help me please?\nI don't know if reAktivate works fine, but I think that the documentation have to be improved a lot, because not all the users that use linux are gurus.\n\nThank's a lot\n\n[royger] "
    author: "Roger"
  - subject: "Re: Compileing reAktivate"
    date: 2004-05-17
    body: "i dont even get how to get it from cvs. they could write a commandline, but no.. \"get it from cvs\"."
    author: "newbie"
  - subject: "Re: Compileing reAktivate"
    date: 2004-09-27
    body: "hey i figured out how 2 do it, u must download e toplevel kdenonbeta directory and the kde-common directory to do it\n\ne.g.\n\ncvs co -R kdenonbeta/reaktivate\ncvs co -l kdenonbeta\ncvs co -R kde-common\ncd  kdenonbeta\nln -s ../kde-common/admin\nmake -f Makefile.cvs"
    author: "wfyk"
  - subject: "Re: Compileing reAktivate"
    date: 2006-05-28
    body: "=( And how/where do you do that from?\n\nI am a total newbie to this. Lame-mans terms and links would be MUCH appreciated."
    author: "jennie"
  - subject: "Dead?"
    date: 2006-09-22
    body: "Hi \n\nis it dead, or is there a new project with the same goal?\n\nIf somebody hear me ... please tell me :=)\n\n Thx"
    author: "Gando"
---
<A HREF="http://www.konqueror.org/">Konqueror</A> has received another
huge shot in the arm, this time
by gaining the ability to embed MSIE ActiveX controls such as
the popular <A HREF="http://macromedia.com/software/shockwaveplayer/">Shockwave
Player</A>.  KDE developers
<A HREF="mailto:wildfox@kde.org">Nikolas Zimmermann</A> and
<A HREF="mailto:malte@kde.org">Malte Starostik</A> today announced
the initial release of <EM>reaktivate</EM>.
While not perfect yet, work is ongoing to support other controls
for which no native Linux/Unix solutions exist, such
as <A HREF="http://www.apple.com/quicktime/">Apple's QuickTime</A>.
Credit goes to the <A HREF="http://www.winehq.org/">WINE</A> developers
for providing the ActiveX support.  So now that Konqueror can embed
MSIE ActiveX controls, Netscape Communicator plugins (for Linux), any
<A HREF="http://www.kde.org/announcements/announce-2.0.html">X window</A>
(through X window parenting), Java applets and any KParts components, and does an excellent job at handling HTML, CSS and JavaScript natively, it seems to me Konqueror is fast becoming the best browser on <EM>any</EM> platform.  Sweet.  Read more for the full press release.

<!--break-->
<P>&nbsp;</P>
<P>DATELINE JULY 9, 2001</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">KDE Web Browser Konqueror Gets Activ(eX)ated</H3>
<P><STRONG>Konqueror Embraces ActiveX, Plays Shockwave Movies</STRONG></P>
<P>July 9, 2001 (The INTERNET).
<A HREF="mailto:wildfox@kde.org">Nikolas Zimmermann</A> and
<A HREF="mailto:malte@kde.org">Malte Starostik</A> today announced
the availability of reaktivate for
<A HREF="http://www.konqueror.org/">Konqueror</A>, KDE's web browser.
Reaktivate enables Konqueror to embed
<A HREF="http://www.microsoft.com/com/tech/ActiveX.asp">ActiveX</A> controls,
such as the popular
<A HREF="http://macromedia.com/software/shockwaveplayer/">Shockwave</A>
movies, for which no native Linux/Unix solution exists.  Reaktivate relies
on the
<A HREF="http://www.winehq.org/">WINE</A> libraries to load and run
ActiveX controls.
</P>
<P>
With this addition, Konqueror now enables KDE users to take optimal advantage
of sophisticated websites that make use of Microsoft Internet Explorer plugins,
Netscape Communicator
<A HREF="http://www.konqueror.org/faq.html#nsplugin">plugins</A> for Linux and Java applets,
as well as KDE plugins designed using KDE's
<A HREF="http://developer.kde.org/documentation/tutorials/kparts/">KParts</A>
technology.
</P>
<P>
According to Malte, the reason he and Nikolas implemented reaktivate
is rather simple: it broadens the spectrum of web sites accessible
to Konqueror, and it was possible.
</P>
<P>
<H3>Successes and Limitations</H3>
</P>
<P>Theoretically,
Reaktivate can eventually be used to embed any ActiveX control into Konqueror.
Currently, however, not all ActiveX controls are compatible with reaktivate.
In particular, the <A HREF="http://windowsmedia1.com/mg/home.asp">Microsoft
Windows Media Player</A> cannot be installed using reaktivate (though it is not known if a player which is already installed will work with
reaktivate).  Thus it is likely there exist other ActiveX controls which
will not yet work with reaktivate.
Work is ongoing to increase compatability with other ActiveX controls,
including the
<A HREF="http://www.apple.com/quicktime/">Apple QuickTime</A> plugin.
</P>
<P>
So far, however, reaktivate has been successfully tested with the
following ActiveX controls:
</P>
<P>
<TABLE BORDER=0 CELLSPACING=6 CELLPADDING=0>
 <TR><TH ALIGN="left">Control</TH><TH ALIGN="left">Status</TH>
     <TH ALIGN="left">Test-URL</TH><TH ALIGN="left">Screenshots</TH></TR>
 <TR VALIGN="top">
     <TD NOWRAP><A HREF="http://macromedia.com/software/shockwaveplayer/">Macromedia
Shockwave Flash 5</A></TD>
     <TD>No known problems.</TD>
     <TD><A HREF="http://static.kdenews.org/mirrors/malte.homeip.net/base.html">Click here</A></TD>
     <TD><A HREF="http://static.kdenews.org/content/reaktivate/flash1.png">[1]</A>, <A HREF="http://static.kdenews.org/content/reaktivate/flash2.png">[2]</A>, <A HREF="http://static.kdenews.org/content/reaktivate/flash3.png">[3]</A>, <A HREF="http://static.kdenews.org/content/reaktivate/flash4.png">[4]</A>, and <A HREF="http://static.kdenews.org/content/reaktivate/flash5.png">[5]</A></TD>
 </TR>
 <TR VALIGN="top">
     <TD NOWRAP>
<A HREF="http://macromedia.com/software/shockwaveplayer/">Macromedia
Shockwave Player 8</A></TD>
     <TD>Some files require the use of a native msvcrt.dll instead of the
         one provided by winelib. The post-installation dialog is functional
         but hard to decipher due to drawing problems.  Some movies do not
         display properly (only black stripes and rects are shown)</TD>
     <TD><A HREF="http://sdc.shockwave.com/shockwave/download/triggerpages_mmcom/default.html">Click here</A></TD>
     <TD><A HREF="http://static.kdenews.org/content/reaktivate/shockwave1.png">[1]</A></TD>
 </TR>
 <TR VALIGN="top">
     <TD NOWRAP><A HREF="http://www.livepicture.com/products/viewers/activex/info.html">LivePics</A></TD>
     <TD>Clicking the "info" button in the toolbar has no result, everything
         else works fine.</TD>
     <TD><A HREF="http://static.kdenews.org/mirrors/malte.homeip.net/livepics.html">Click here</A></TD>
     <TD><A HREF="http://static.kdenews.org/content/reaktivate/livepics1.png">[1]</A> and <A HREF="http://static.kdenews.org/content/reaktivate/livepics2.png">[2]</A></TD>
 </TR>
</TABLE>
</P>
<H3>Note on Security</H3>
<P>
<STRONG><EM>Install ActiveX controls only from sites that you
trust.</EM></STRONG>
Microsoft's ActiveX technology has often been criticized for weak security.
Those controls are dynamic libraries that are executed exactly like any
other piece of code installed on the user's system. This means they have
full access to the file system, the system registry etc.  As a means to
establish the users' trust in the controls a web site wishes to install,
every ActiveX control is cryptographically signed and carries a certificate
issued by an authority known to the web browser (like
<A HREF="http://www.verisign.com/">VeriSign</A>).  A control
that has no signature or no certificate or if they are invalid will not be
installed.
</P>
<P>
With reaktivate the situation is similar: the installed controls can call
every WinAPI function provided by the WINE libraries and therefore have
access to WINE's registry and all files visible to the WINE installation.
The current implementation of reaktivate will ask the user for
confirmation to install a new control, but it will not check the embedded
certificate and signature. This is due to technical reasons as well as
limited time.  Therefore we strongly advise to install controls only from
sites that you trust.  To save your files from malicious controls, you might
also consider using this feature only from a seperate user account that
has no access to your main user's files. Reaktivate will not run from the
root account.
</P>
<H3>Installing Reaktivate</H3>
<P>
Source code for reaktivate is freely available under a Free, Open Source
license from the
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/reaktivate">kdenonbeta
module</A> in
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi">KDE's CVS repository</A>
and its <A HREF="http://www.kde.org/cvsupmirrors.html">mirrors</A>.
See the <A HREF="http://www.kde.org/anoncvs.html">KDE website</A> for
information about how to get a module from CVS.  You only need
the toplevel, admin and reaktivate directories from kdenonbeta.  Before
compiling, get the latest <A HREF="http://www.winehq.org/download.shtml">CVS
version of WINE</A> (a snapshot will likely not be new enough). Next,
apply all patches from reaktivate/patches-for-wine/ against the WINE
sources and build/install WINE.  Finally, you can build and install
reaktivate.
</P>
<P>
<FONT SIZE="2">
<EM>Disclaimer</EM>:  reaktivate is not in any manner sponsored or endorsed
by, affiliated with, or otherwise related to,
<A HREF="http://www.microsoft.com/">Microsoft Corporation</A>.
</FONT>
</P>
<P>
<FONT SIZE="2">
Thanks to <A HREF="mailto:pour at kde.org">Andreas "Dre" Pour</A> and
<A HREF="mailto:navindra@kde.org">Navindra Umanee</A> for assisting in
drafting this release.
</FONT>
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
KDE, K Desktop Environment and Konqueror are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Microsoft, ActiveX, Microsoft Internet Explorer and Windows Media Player
are registered trademarks or trademarks of Microsoft Corporation.
Shockwave is a trademark or registered trademark of Macromedia, Inc. in
the United States and/or other countries.
Netscape and Netscape Communicator are trademarks or registered trademarks
of Netscape Communications Corporation in the United States and other
countries and JavaScript is a trademark of Netscape Communications Corporation.
Apple and Quicktime are trademarks of Apple Computer, Inc., registered in
the U.S. and other countries.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
