---
title: "theKompany.com Releases Kapital, a Personal Finance Manager for KDE"
date:    2001-02-19
authors:
  - "sgordon"
slug:    thekompanycom-releases-kapital-personal-finance-manager-kde
comments:
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "The screen shots look cool...\n\nPersonaly, though, if I was the Kompany, I'd be trying to convince Distro packagers why they need it in their comercial distros...  As its hard just to find a copy of linux at most stores, yet alone linux apps..."
    author: "greg"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Fi"
    date: 2001-02-19
    body: "That's a good idea I must say.  theKompany.com should strike such deals with distributions!  We know Red Hat is out, but there are other fish in the sea.\n\nThis is very much Slashdot-worthy news, btw."
    author: "KDE User"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Fi"
    date: 2001-02-19
    body: "My first reaction was a violent one to the idea of running anything but open source on my machine. But upon more consternation, I believe this is a very good thing. But lets see how good support for variants of Linux and Unix is with a commercial product from the Kompany, any commercial company is going to have to run hard to keep up with the platforms and quality of the KDE team. \n\nI will be the first in line to buy a copy of this for my Solaris on SPARC hardware."
    author: "mAIse"
  - subject: "Note to editors"
    date: 2001-02-19
    body: "Since all of thekompany's offerings have been commercial, maybe what the editor means to say is that this is a closed, or proprietary, product?"
    author: "Neil Stevens"
  - subject: "Re: Note to editors"
    date: 2001-02-19
    body: "This was simply meant to be a heads-up to KDE/Linux users expecting a free download.  I clarified the remark, although obviously more informed ones like you don't really need such information.\n\nMeanwhile, congrats to theKompany.com for filling in the void in the Linux market with a product of this sort!  I hope they manage to market their product to the rest of the Linux world.  This is the kind of software comp.os.linux.advocacy used to scream for all the time.  Even CmdrTaco has been salivating for such software, IIRC.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Note to editors"
    date: 2001-02-20
    body: "But, not everything they release is commercial...  They have released many good thing to the KDE tree, and ingeneral...  (Kamera, Kivio...)"
    author: "Nicholas Robbin"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "Once again Shawn, Great Work! but what I'm really waiting for is.... ReKall! when will we have a beta!?!? :)"
    author: "t0m_dR"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "Ok....what is REkall"
    author: "Kurt Palmer"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "Rekall is an M$Access like IDE for developing databases (as shawn said) for KDE!\n<br><br>\nhe also said that the first version would not have Python scripting [as opposed to VB for Access] but that the later versions would.\n<br><br>\ncheck for yourself  <a href= http://dot.kde.org/979768484/979777064/979778240/979781930/979783107/979801945/979803133/979810807/\n>  here </a>"
    author: "t0m_dR"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "I am itching to see reKall also. Any news on it would be great. Will it be a commercial package, and if so, how much will it cost?"
    author: "Jono Bacon"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "Right now we are on track to have a first beta of Rekall the beginning of March.  We are working on getting the web pages together right now, hopefully they will be up in another week.\n<br><br>\nThere will be two versions of Rekall, one will be free and have limited functionality, we haven't decided how it will be limited.  We want it to be useful for a certain category of use.  The commercial version, which can be seen as a database centric development environment like Access (but better :)), will cost somewhere between $30 and $50, we are still working it out.\n<br><br>\nI think you will be pleased when you see it :)."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Fi"
    date: 2001-02-19
    body: "WOW, if it is going to be as half as good as it sounds, with that price , it's going to be an instant hit!"
    author: "t0m_dR"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "What ReKall should have IMHO opinion, is a whole set of builders and Wizards, which will enable me to create a small/medium database within minutes after defining the tables. THAT is the advantage that Access has: Really QUICK database application  development. If ReKall can do that, you have my vote ( Then I might even convince the Greek L.U.G. to distribute the free edition inside Linux CDs that  will be given to IT students in a large number(thousands) of Computer Schools, later this year)"
    author: "t0m_dR"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "Well that is one of the objectives for Rekall."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "<b>Once again they have done it </b>,and i am really looking forward to buying this.<br>\nHowever I would love to see them include a version for my PDA (Palm) that i could use to syncronize and keep my info uptodate while i am\naway from my system <ul>(This mainly for the budget tracker/planner and payment reminders).</ul><br>\nI also hope that they do some intregration with Koffice (i can see it with kspread)and allow for html formatting/publishing ,it also goes without saying that it should intregrate with konqueror.<br>\nAlso I would love to also see it secure with the ability to lock and encrypt records on a per-user\nbasis.<br>\nOnce again let me say congrats and thanks<br>\n<br>\nP.s.\nAnd now this for the KDE developers out it would be cool if i could RMB click on a file and directory and encrypt them (new password or global session password independent of the users system passwd). <br>Also what happened to the idea of using compression on files in the recycle bin?, hope i didn't imagine it. It would be cool to make it smart and not compress files with extension *.bz2 *.zip *.tar....etc (i guess that this is better if user definable along with max trash size and purge threshold [oldest files first]).<br>\nHope i didn't waste your time with unnecessary info.<br>\n-kbee"
    author: "Kurt Palmer"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "See this is my real problem, I'm fine with paying for good software, but how is this going to play with Koffice, and so forth.  We have seen how The Kompany hasn't made much of an attempt to intergrate the Magellan with the rest of kde, I think that with KDE it is important for it's builtin modules to be used as much as possilbe, which the Kompany does do.  Since they are not GNU they have to remake all the modules that KDE already has made."
    author: "Jonna Hellia"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "theKompany are not responsible for Magellan - that's a seperate project driven by a seperate group of people.  theKompany's PIM is called \"Aethera\".\n\nHaving said that, I don't know how well that integrates with anything either :-)  Not tried it myself.\n\nMacka"
    author: "Macka"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "We are currently working on a full rewrite of the Aethera UI into KParts, which is almost done.  One of the disagreements we had with magellan originally is that we wanted it to be part of KOffice and they didn't.  \n\nThe sad truth is that I don't have unlimited resources to do everything that I want at the same time, so some of my dreams take a little longer to realize than others.  Good things come to those you wait :)."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "What is the relation (similarity/difference) between Empath and Aethera?. AFAIK Empath wanted to be a fully component based system. Making Aethera a parts based system sounds to me similar to Empath. Since both are opensource and KDE apps, shouldn't there be some exchange of code and ideas between the two?"
    author: "rv"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "I've spoken with Rik a number of times about this, but neither one of us has had a lot of time to really get into it.  We should probably have a conference between the various groups to see what might make sense.  Thanks for reminding me :)"
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "Whoa! fp! (fast post:-> 4 minutes flat!). Thanks."
    author: "rv"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "hi,\nvery interesting.\nis there any german version planned?\n\n-gunnar"
    author: "gunnar"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "Kapital supports a variety of monetary formats, so in that regard german will be supported.  We'll be testing various localizations once the code is tested a bit more.  Having programmers in a bunch of different countries makes that a lot easier to test :)."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "This is great news Shawn. One question I do have is: what theme are you guys using for the screenshots? It looks really slick and professional.\n\nAnother question I do have is how you are planning your distribution of your products. I am in the UK, and it would be great to order your products over here, or even see them in the shops. Any news on this?"
    author: "Jono Bacon"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "That was just the default color theme, the art work is ours.  I'm glad you like the look.\n<br><br>\nFunny you should ask about UK distribution.  I'm currently in discussions with a UK distributor right now.  They are likely to carry PowerPlant and BlackAdder at the moment, so I'm sure getting them to carry Kapital won't be a problem."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "Ahhh, I assume the screenshot was taken at a high resoloution as the menu's seem nice and compact. I like this look. The artwork is indeed very nice, and it puts a nice professional shine on the package.\n\nAs for the UK distribution, I hope something good comes out of it. I firmly believe you have a good business model, and although there are some flamers about close sourcing (which is inevitable in the business world), I hope you make some good money. The flamers will stop complaining when it brings more users to KDE."
    author: "Jono Bacon"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "Here's a tip for theKompany to become REALLY popular among most KDE users: Join efforts with Faure, Montel and the guys and work on KWord! Everybody needs a good word processor and KDE needs a good word processor to hit it big-time, which, in turn, means a broader user base for other theKompany products."
    author: "Matt"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "check this out: http://www.thekompany.com/press_media/full.php3?Id=3"
    author: "pieter bonne"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "So, who are the \"two programmers\" being sponsored?"
    author: "Matt"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "I don't think anyone is being sponsored. Yet."
    author: "ac"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "The deal was between us and Kawail.  They would find and house the programmers, and we would pay for them.  The last I heard from Kawail was that they finally found someone back in December, but only one.  We both looked at a lot of resumes, but no one fit the bill.\n\nAnother issue has been all the changes being made in Qt for the rich text widget.  One of the things we were going to work on was support for languages like Thai, and that appeared to be directly supported with the new widgets, so instead of doing unnecessary work, we needed to see if that was going to solve the problems or night.  I haven't gotten a status from Kawail in a bit, so I don't know the current state.  We've been very busy getting out our other products lately."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "Shawn, check out koffice-devel (mailing list) and you'll probably find guys suited for the job. Some of the talented guys there are working on KWord in their spare time, but could probably contribute a lot more if it payed the bills. Frankly, KWord is in desperate need of developers, check out how many cvs commits there have been during the last six months."
    author: "Matt"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "The idea was they would be Thai programmers, and would physically be at the Kawail facility.  As it turns out, it is hard to find Thai programmers.\n\nI know KWord needs help, and I have conversations about it at least once a week.  KSpread needs help too and so do a lot of things.  I already get accused of being spread too thin.  It's all important to do, but it can't all be done at once."
    author: "Shawn Gordon"
  - subject: "We will be offering Kapital under a closed-source"
    date: 2001-02-19
    body: "\"We will be offering Kapital under a closed-source license.\"\n\nThat statement is what makes me wonder why it even made it to dot.kde.org."
    author: "rg"
  - subject: "Re: We will be offering Kapital under a closed-source"
    date: 2001-02-19
    body: "From the \"about\" section:\n<p><blockquote><em>Here you can post or read about new developments, applications and breakthroughs, KDE announcements, feature articles, interviews, events and much more.</em></blockquote>\n<p>So this article seems to fit the bill nicely."
    author: "Me"
  - subject: "Re: We will be offering Kapital under a closed-source"
    date: 2001-02-19
    body: "They also consider giving the source to people who bought the package. I think that would be a good compromise.\n\nAnd who said that closed source apps have no room with KDE? I welcome thekompany's attempt to make a good product. They use free software as abase for their product and help improve it that way.\n\nGogogo!"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: We will be offering Kapital under a closed-source"
    date: 2001-02-19
    body: "I agree with the previous comment. I am OK with theKompany charging for their products, but I still feel that software should be released with sources included, IMHO."
    author: "Bojan"
  - subject: "Re: We will be offering Kapital under a closed-source"
    date: 2001-02-21
    body: "Freedom means choice. Open Source and Free Software are great, but they doesn't mean freedom. You have the freedom NOT to use closed source softare, while the rest of us have the freedom TO use it.\n\nAll things equal, I prefer Open Source. However, I am not aware of any Open Source equivalent to Kapital."
    author: "David Johnson"
  - subject: "Re: We will be offering Kapital under a closed-source"
    date: 2001-02-23
    body: "GnuCash"
    author: "Robert"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "Excellent work theKompany, it is not something I need but I'm sure there's a market for it.I for one support pay-ware/proprietatery on linux, as long as they're giving back to the community in other projects, and we all know TK does that. We need good end-user apps and we need them fast."
    author: "XPyre"
  - subject: "German version with online banking?"
    date: 2001-02-19
    body: "As soon as you ship a working German version that supports the HBCI standard for German online banking, I will buy two copies. \nIf you also support card readers you will make a fortune!\n\nhttp://www.hbci.de\n\n\nAt the moment I see no advantages over Gnucash from the announcement, but I hope that will change and you will do the boring work of supporting all the online banking standards."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: German version with online banking?"
    date: 2001-02-22
    body: "Thanks for the tip on HBCI, we've been getting a ton of requests for a native German version of Kapital as well.  I've just started to look at the HBCI site, and it's pretty terrible in terms of providing information since it is mixed between german and english, however we have some german folks on staff so we should be able to get it localized and tested with HBCI.  We'll announce it once we have the OFX, HBCI and MBANX support worked out."
    author: "Shawn Gordon"
  - subject: "Re: German version with online banking?"
    date: 2001-03-01
    body: "Just wanted to confirm that HBCI support will be a major factor for a German version of Kapital. It'd make me consider buying a copy, too (I'm still waiting for reviews of your product, though).\n\n> it's pretty terrible in terms of\n> providing information since it is\n> mixed between german and english\n\nThis is the standard lingo in German IT, sorry. We have indeed a terrible mix of German and English terms..."
    author: "Hanno M\u00fcller"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "It would be nice if you offered it in FreeBSD package format, or at least in a traball for execution in Linux compatability mode."
    author: "John Shannon"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "Like Jono Bacon I would very much like to know the name of the theme seen on the screen shots. It looks very cool."
    author: "Jonas"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "From the window decoration I would say that's System++"
    author: "ac"
  - subject: "theKompany releases commercial software again..."
    date: 2001-02-19
    body: "Does theKompany plan to release any open source software one of these days?\n\nIf I remember correctly, theKompany was supposed to be the KDE answer to the Gnome foundation. So far, I haven't heard of the Gnome foundation releasing any commercial software at all. Even the stuff developped by paid programmers like Evolution or OpenOffice is available under the GPL.\n\nMay be I'm just paranoid, but I have that nasty feeling that the KDE camp is developping killer apps but releases them as commercial packages (aerethra being the exception, due to the fact that is was based on the Magellan source code which was under an open source licence).\n\nOh well...I guess I'll keep using GnuCash under KDE 2.1. And I may even give gnome 1.4 a try, after all."
    author: "fltawa"
  - subject: "Re: theKompany releases commercial software again..."
    date: 2001-02-19
    body: "Excuse me, but your comment is a bit off putting.  All we've done for the last year is release free and open software, we are just finally getting to some commercial applications.  Please visit www.thekompany.com/projects for an extensive list of the free and open projects we've done.\n\nIn addition, who ever said we were the answer to the Gnome Foundation?  That doesn't even make sense, that would mean that we would be a governing board over what goes into KDE, which I'm sure none of you want, and neither do I.\n\nIf we give away our work, then how will we make money to do more work?  And let me clarify Aethera.  The license of Magellan specifically allowed us to close source it if we so chose.  We went the other way and put a GPL license on it.\n\nYou and I both know that your not going to run off and install gnome, so why do you make those comments?  simply to be inflammatory I imagine."
    author: "Shawn Gordon"
  - subject: "Re: theKompany releases commercial software again..."
    date: 2001-02-19
    body: "you can be sure that plenty 'killer apps' will continue to become available for kde under the gpl (there a lot of talented people that would see to that:) \ntheKompany is trying to make a buck. let 'em. you keep using and enjoying free software within kde. there is no danger."
    author: "ac"
  - subject: "Re: theKompany releases commercial software again..."
    date: 2001-02-19
    body: "you can be sure that plenty 'killer apps' will continue to become available for kde under the gpl (there a lot of talented people that would see to that:) \ntheKompany is trying to make a buck. let 'em. you keep using and enjoying free software within kde. there is no danger."
    author: "ac"
  - subject: "Re: theKompany releases commercial software again..."
    date: 2001-02-19
    body: "fltawa sounds like some little immature 16yo who likes to mouth off on things he knows nothing about..."
    author: "Chris Bordeman"
  - subject: "Re: theKompany releases commercial software again..."
    date: 2001-02-19
    body: "Have you heard of Kivio? Thank The Kompany and Shawn for that one too. The Kompany is awesome so check into it before you shot your mouth off. I say go Gorton go.\n\nCraig"
    author: "Craig black"
  - subject: "Re: theKompany releases commercial software again..."
    date: 2001-02-20
    body: "You're confused.. theKomany isn't KDE's answer to the Gnome Foundation... That's the KDE League..\n\n\nPlease check your facts (oh, and read shawn's posting about the aethera licence above..)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "There is already a review in LinuxPlanet for those insterested."
    author: "Cesar"
  - subject: "License problems?"
    date: 2001-02-19
    body: "Shawn,\nisn't Kugar released under GPL? If it's not dual-licensed, then Kapital would have to be GPL-ed too, right?\nI'm sure this is just an oversight, but you should probably change the Kugar pages to reflect the dual license.\nGood luck,\nJacek"
    author: "Jacek"
  - subject: "Re: License problems?"
    date: 2001-02-19
    body: "OH yea, we were changing the license to LGPL.  We'll get that all striaghtened out shortly."
    author: "Shawn Gordon"
  - subject: "Re: License problems?"
    date: 2001-02-21
    body: "As the authors of both Kugar and Kapital, they can do anything they want. Licenses are to impose restrictions upon the user, not on the author."
    author: "David Johnson"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Fi"
    date: 2001-02-19
    body: "I'm not dot-rich. Gnucash suits me just fine."
    author: "ephemeroot"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Fi"
    date: 2001-02-19
    body: "About $50 is cheap if you ask me. I like the idea that more and more commercial applications are developed for Linux and if its usable then $50 is more than OK with me."
    author: "J. Andersen"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Fi"
    date: 2001-02-19
    body: "It's not even $50!  Order it now and you can get it for $24.95 or $29.95.  I consider that to be a very good price for supported software."
    author: "Dave Marotti"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Fi"
    date: 2001-02-22
    body: "Personally I liked the price so much I've already placed an order :-) Now I hope it lives up to MickySoft Money ( which I've been using.. ) and releaves me of the pain of using M$.....<br>Oh... and commercial software is just as great as free software :-) ( I love Applix and QuakeI[I[I]] :)"
    author: "tosi"
  - subject: "Shawn and funky sales"
    date: 2001-02-19
    body: "Shawn, a little while ago I posted \"Sell me something funky!\".  Well now you have both my gratitude and my credit card number! keep up the good work.\n\nBy the way, it's a shame it's not possible to go to your ordering page with konquerer.  Netscape still has it's uses I suppose.\n\nRegards"
    author: "Mark Grant"
  - subject: "Re: Shawn and funky sales"
    date: 2001-02-19
    body: "Konqueror is working for everyone in my company, we just did a test after seeing your post.  Can you give us some specific information like the versions you are using and what is happening exactly?\n\nthanks"
    author: "Shawn Gordon"
  - subject: "Re: Shawn and funky sales"
    date: 2001-02-19
    body: "Well, I'm using konquerer from KDE2.1Beta2 which I compiled myself from the tar ball.  I get a \"The protocol https is not supported\" dialogue.  \n\nI don't know if it is a compile time option somewhere but that's what I get.\n\nRegards\n\n\n\nMark"
    author: "Mark Grant"
  - subject: "Re: Shawn and funky sales"
    date: 2001-02-19
    body: "yea, the problem is that you don't have https support compiled in.  We use ssl for secure ordering.  So you have to have an ssl capable browser."
    author: "Shawn Gordon"
  - subject: "Re: Shawn and funky sales"
    date: 2001-02-19
    body: "I guess this makes sense :)\n\nAnyway, you have my order for the CD, I'll look forward to receiving it and thanks for the chance to finally start paying you guys back for the truly frightening amount of quality work you keep putting out.\n\nRegards\n\n\nMark"
    author: "Mark Grant"
  - subject: "Re: Shawn and funky sales"
    date: 2001-02-20
    body: "I think you have to have openssl (not just the lib but also the haders)installed. Since you are probably using an rpm based system look for something like openssl-devel*.rpm"
    author: "Anon"
  - subject: "Why is this even on KDE Dot News?"
    date: 2001-02-19
    body: "This is disappointing. If we loved proprietary software, we would all be using Windows."
    author: "LN"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-19
    body: "Is we (i.e. users)loved only free software, we (i.e. programmers) would all be on welfare.<br>\nI like *good* software, regardless of whether it is open-source, free or not. Being open-source is no guarantee of high quality and being proprietary is no guarantee of shoddy quality either, both have a place in the scheme of things.<br>\nIf the Linux community will despise *all* non-free software, then software companies will not be interested in porting their software to the platform...much to Microsoft's happiness. Is that what you really want?"
    author: "Jacek"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-20
    body: "There is a place for proprietary software - for the more specialist applications. But for ubiquitous software, such as the operating system, office app's, accounting programs, etc, open source is the way to go. Besides, there are plenty of companies whose business plan involves making money from OS apps - Gnumatic (Gnucash), the MYSQL and PostgreSQL companies, Ximian, Eazel, etc. Programmers aren't going to starve."
    author: "LN"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-20
    body: "Which of these companies doesn't lose money?"
    author: "Bernd Gehrmann"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-20
    body: "MySQL for a starter"
    author: "rg"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-20
    body: "That is, 1 out of 5, and one which only made \ntheir application open source after several\nyears of selling licenses (PostgreSQL is a\nsimilarly bad example, since the company\ndidn't write the software, but started their\nbusiness on an existing and mature codebase).\n\nCertainly that's a convincing argument for\ngiving away desktop applications for free?"
    author: "Bernd Gehrmann"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-22
    body: "You have to remember that it is only recently that the free operating systems have gained a significant share of the market. It's hardly surprising there are few past successes to point to when the potential market was minute. These companies are relying on the continued growth of open source software (a not unreasonable assumption). What was uneconomical when the market share was a couple of percent, could be quite lucrative when it is significantly greater. We are now living in internet time - conditions are changing rapidly."
    author: "LN"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-23
    body: "<P><EM>What was uneconomical when the market share was a couple of percent, could be quite lucrative when it is significantly greater.</EM></P>\n<P>How do you figure that?  From what I learned, even 1,000,000,000,000,000 * $0 = $0.  In other words, if you give it away for free, you never make any money.  And as soon as you charge $1 for it, \"CheapDisks\" will sell it for $0.50 -- at cost, essentially, since they put not a single penny into developing it.\n</P>\n<P>Then you say, \"sell support\".  That might work for enterprise software, but you ain't selling any support to KDE users.  How many KDE users have bought a support contract for any of the software they use -- SuSE, Aethera, etc.?  And software like Kapital is targeted at the home user.\n</P>"
    author: "Dre"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-23
    body: "The ways to make money are from services, support ,training etc. It is an advantage if Cheapdisks sells thousands of disks. Cheapdisks is not going to be offering any support. People are going to install the software, transfer their financial records into it, and if the software is useful to them, they'll be willing to pay for support when they need it. Support doesn't have to be on a contractual basis, but can be on a per incident basis. I assume this is Gnumatics strategy. Look at how much Microsoft must make from support. If there are people around who are willing to fork out for software like Kapital, there must also be people who are willing to fork out for support/services on free software. Again, the size of the potential market is critical."
    author: "LN"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-23
    body: "I have a lot of history and experience with this model, and it really doesn't work.  It is too inconvenient to pull out a credit card and pay $10 to ask a question, people will go to the store and buy a book, or give up.  It's not cost effective to get the business or support the business and people aren't keen to do it.\n\nWe added 'for fee' support services a few months ago because we were just getting hammered with support questions for our open source projects.  Seemed fair, the software was free, and a very modest fee for support, you could do per incident and annual.  You know what happened?  Not one person has paid for support, they try to use our 'info' and 'sales' email addresses to get free support for the free software.  We answer what we can as time permits, but it proves to me that if people won't pay for the software, they won't pay for the support either."
    author: "Shawn Gordon"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-21
    body: "Do you actually believe what your saying our just mouth piecing Stallmen? The Kompany has the right business plan. Selling proprietary software allows them to contribute open source back into kde.\n\nCraig"
    author: "Craig black"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-22
    body: "Of course, theKompany is free to make and sell what software they like - no argument there. But should this be on KDE Dot News? The KDE project is about producing a fully free desktop. How does this further these aims. Shouldn't we be only reporting on open source software here? If this was produced by anyone else but theKompany, would it be even in here?"
    author: "LN"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-22
    body: "I don't think that even justifies my time.\n\nCraig"
    author: "Craig black"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-20
    body: "Choice, my friend, choice. You don't want to buy it, don't buy it. If you rather use something a student hacked together in his spare time, do that."
    author: "Matt"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-20
    body: "You mean like Linux? :)"
    author: "LN"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-21
    body: "Yes, like Linux. You see, SOMEONE got off their butt and made Linux. If you want an Open Source personal financial manager for KDE, go write one. Or did you think that GnuCash sprung fully formed from the brow of GNOME?"
    author: "David Johnson"
  - subject: "Re: Why is this even on KDE Dot News?"
    date: 2001-02-21
    body: "GnuCash runs just fine under KDE. Why is it necessary to rewrite the whole thing just because it uses a different toolkit?"
    author: "LN"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-19
    body: "Hmm, very cool, all it needs now is the bookkeeping features of QuickBooks!"
    author: "Carbon"
  - subject: "Yea! No more Windoze!"
    date: 2001-02-20
    body: "Woo-hoo!\n\nI for one will *definately* be buying this!!! Finally, I can stop booting into Windoze to take care of on-line banking!!\n\nWhile I would rather use open-source/GPL'd software, I have no problem paying for _good_ closed source software. I had no problem buying quicken. I wouldn't touch M$ Money (isn't that redundant?).\n\nBesides, theKompany's done a lot of good, open source stuff as well. I really don't begrudge them wanting to actually _sell_ something.\n\n--john\n\n\np.s. does anyone know if it actually _does_ online banking yet?"
    author: "john"
  - subject: "Re: Yea! No more Windoze!"
    date: 2001-02-20
    body: "It doesn't do online banking yet, but it will."
    author: "Shawn Gordon"
  - subject: "\"it will\" eh?"
    date: 2001-02-20
    body: "i sure hope so.\n\nthis is the one thing I really need, and the one feature lacking that is keeping me from buying it right now. does the closed source have anything to do with the online banking code?\n\non a different note: How is theKompany with upgrades? ie: There's quicken98, q99, quicken2000, etc, each one costing roughly $40. I still use quicken99 because that's all I need. Is the Kompany planning on doing the same thing? Or will yearly upgrades be more reasonable ($5-$10)? I'd probably upgrade to quicken2001 if I could do it for $10.\n\nTwo reasons for asking:\n1) I really don't know and am curious;\n2) I'd really hate to buy this thing now, and find out online banking is not available until Kapital2002, and have it cost another $40.\n\nthanks,\n-john"
    author: "john"
  - subject: "Re: \"it will\" eh?"
    date: 2001-02-20
    body: "Right now we are offering free updates through September 1st as an incentive.  going forward we haven't really thought about it.  I know how annoying the Quicken updates are, I usually only update every 3rd year myself.  With most of our products we also sell a subscription service to stay up to date, but those are programmer tools and this is an end user application.  I imagine we could do a half price electronic upgrade or something (don't hold me to that just yet).\n\nAs far as online banking, there are a number of formats for various countries.  For the most part everything supports QIF, so you can just import those files in the short term."
    author: "Shawn Gordon"
  - subject: "GnuCash importer?"
    date: 2001-02-20
    body: "I've been using GnuCash for quite some time now (I got rid of quicken as soon as GnuCash got good.  However there are a good number of shortcomings with GnuCash.  They are planning to address them, but they are slow to develop.  I would rather support a KDE product anyway, however, I need a GnuCash importer.  Is this planned?\n\nThanks for more great products.  All the people that say everything needs to be free need to ground one foot in reality.  I wouldn't have a job if everything was free.  And you guys have seemingly found a good mix of free (give back to the community) vs. proprietary (pay the bills!) and haven't gotten greedy yet.\n\nKeep up the good work.  And hopefully a GnuCash importer is on the plate.\n\nJim"
    author: "Jim Basilio"
  - subject: "Re: GnuCash importer?"
    date: 2001-02-20
    body: "A GNUCash importer should be pretty trivial since the format is available.  We'll certainly look at that, it hadn't occured to me yet.\n\nThanks for the support :)"
    author: "Shawn Gordon"
  - subject: "Re: GnuCash importer?"
    date: 2001-02-21
    body: "Now, will you be making your format available, so that the GNUCash people can make a Kapital importer trivially?"
    author: "Neil Stevens"
  - subject: "Good Question, I hope it gets answered"
    date: 2001-02-22
    body: "Title says it all."
    author: "Dave Rosky"
  - subject: "Desired features from a Quicken Lover"
    date: 2001-02-20
    body: "I am one of those who would put Quicken at the top of the list of why I don't dump Windows.  Since I've paid money for Quicken, I have no problems with paying money for something to replace it that is better.  I agree that Quicken is getting bloated, and it seems worse with every iteration.  In addition, their help service is getting even worse.  So here are some of the things I would love to see:\n<P>\n1) One thing I like about Quicken is that it's just not paying bills, but a central repository of all financial info.  They have a special section to store all of those useful Personal Records: Account Numbers, Policy Numbers, Contact Info, Medical Info, etc.  The concept is great, but it ends up being really annoying because:<BR>\na) there is no integration to the main program.  If I add an account, all of that information (account number, percentage rate if credit card, contact info, etc.) is not automatically added to Personal Records section. <BR>\nb) there is no way of customizing the reporting of the records, and their reports aren't very good.\n<P>\n2) Similar complaints can be leveled against their Home Inventory program.  Not enough integration with the main program.  If I buy something really expensive, I would like to have it pop up and ask if I want to add it to my inventory.\n<P>\n3) Related to a central information of all things financial, it would be useful to have special actions for specific type of purchases, especially cars, insurance, and home.  For example, if I have a car repair, I would like to have that information linked to a database where I can also add in the warranty period, contact information of the garage, etc.  Also remind me about car maintenance in addition to remind me to pay my bills (\"It's been 3 months since you've changed your oil.  Have you changed it yet?\")  \n<P>\n4)  Addresses.  The ability to import/export address from other source is very desirable (Can we import or share address info with Aethra?)\n<P>\n5) My bank allows me to download my monthly statements in QIF format.  However, there is no way for me to reconcile this with normal account.  All I can do is \"Import\" it, whereupon, I end up with duplicate actions.  A way to reconcile with generic QIF files would be useful.\n<P> \n6) Integration with other apps in KOffice (i.e., spreadsheet) would be useful.\n<P>\n7) The real must-need feature is online partnering with financial institutions, especially Fidelity.\n<P>\nI wish you the best of luck, and can't wait to try out the demo!"
    author: "SK"
  - subject: "Re: Desired features from a Quicken Lover"
    date: 2001-02-20
    body: "Thanks for the great ideas, some of these I had been planning on, but some had not occurred to me yet.  I agree totally with your point 5, it's a joke, if you could download and match transactions to clear them it would save a ton of time.\n\nThe whole address book thing is interesting, it's not a problem for us to import/export between Aethera and Kapital, but to what degree does this make sense?  I've got to think it about it some more.\n\nThanks again for the thoughts :)"
    author: "Shawn Gordon"
  - subject: "Re: Desired features from a Quicken Lover"
    date: 2001-02-20
    body: "Well, I'm a MS Money user and it handles importing QIF files very well.  There is a 'reconciliation' point where you match the things in the QIF with the ones you've entered (a NECESSITY) -- so no duplicates are added.  I've had some situations where for my student loans, I use two transactions in MS Money, but one check. So i reconcile that one check against two within the program.  And sometimes there are other things with the same amount where the name is not available, or it would be a guess at best."
    author: "Kevin"
  - subject: "Re: Desired features from a Quicken Lover"
    date: 2001-02-20
    body: "I agree that it would be tricky, but what I was thinking was something that would let you choose which ones to import.  This is similar what I do with my cell phone: I have a huge Outlook database of contacts, but I have a program that let's me choose which ones I actually upload to my cell phone.  What I have in mind for Kapital, is that I could have my bank info, insurance info, etc. also in Aethera so that I have access to all of those functions, but I can then have the info in Kapital as well.\n<p>\nPart of this is motivated by the fact that in Quicken at least, the way contacts are managed feels very awkward and not nearly as intuitive as say Outlook.  If Aethera's Contact manager is a KPart that Kapital imports, then this is perhaps moot, since the quality of the contact managers are the same."
    author: "SK"
  - subject: "Re: Desired features from a Quicken Lover"
    date: 2001-02-21
    body: "If you are only keeping windoze on your system for Quiken, and nothing else, then save some time and energy rebooting and invest in Executor, which last I heard comes with a Mac copy of Quiken. Executor, BTW, is a very neat (but commercial) Mac emulator. I use it to play old mac games. www.ardi.com"
    author: "Carbon"
  - subject: "I HOPE YOU MAKE A LOT OF MONEY!"
    date: 2001-02-20
    body: "You know, what i believe KDE/Linux needs is someone with the Mind of Shawn Gordon and the pockets of IBM(!). I mean IBM is supposed to give stuff to the community, but nothing is as REMOTELY  innovative as what The Kompany does. I'm sure the Kompany is working for peanuts (relatively with Money spent in even Medium sized companies). If the Kompany had some hard cash , think of the work they would do!, so I hope they make A LOT of money  and create REALLY GOOD software! KUDOS guys! \n:-)"
    author: "t0m_dR"
  - subject: "Re: I HOPE YOU MAKE A LOT OF MONEY!"
    date: 2001-02-20
    body: "god!! spare us the drama."
    author: "ac"
  - subject: "Re: I HOPE YOU MAKE A LOT OF MONEY!"
    date: 2001-02-21
    body: "OK OK maybe I overdid it just a bit :-P"
    author: "t0m_dR"
  - subject: "Re: I HOPE YOU MAKE A LOT OF MONEY!"
    date: 2001-02-23
    body: "IBM writes drivers and clustering support.  There more interested in enterprise support for linux, and having linux run on there hardware."
    author: "Robert"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "Am I the only one that has noticed that Slash Dot has become increasingly Gnome centric? The haven't even carried the the story on Kapital. I'm sure if Kapital was a Gnome based piece of software it would sure be up there. So join me and crank up the heat on em and everybody send them new posts telling them about Kapital. Maybe they'll get the point and relies that KDE is important!!! Kapital is much to important of software to ignore.\n\nCraig"
    author: "Craig black"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "maybe if thekompany created free software (gpl) like ximian and eazel, it would get slashdoted."
    author: "Evandro"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "The Kompany does create gpl but they need to make money so they can continue to give us more cool gpl software. How can they pay there programmers to create software like kivio and aethera if they can't make money. I appreachiate your idealism but this is the real world. The Kompany is just the kind of allias that KDE needs. The More money they make of cool quality software the more they can contribute to us. They given much to us and now its time we give back. Take alook at this list of what they contribute too.\n\n Aethera\n\nAethera is a pim application, i.e. it handles all kinds of personal information: email, contacts, notes, tasks, todos, journals. It has various communication features regarding: send/receive email, send/receive task requests and appointment requests via email and it can also send SMSs using web gateways.\n\nRead more about Aethera here. KDB\n\nKDB is a set of libraries that will ease the development of database applications for KDE. It leverages the power of KDE Plugins to access the wider range of available free and commercial DBMS. It also provides a set of built-in widgets and dialogs that will boost your productivity in developing database applications for KDE. \n\nRead more about KDB here. kamera\n\nkamera is an IO slave and a KControl panel module which allows you to access folders and images within any digital camera supported by the upcoming gPhoto2 libraries.\n\nRead more about kamera here. KDE Studio\n\nKDE Studio is moving to become the premiere development environment for Linux. theKompany.com is wholeheartedly supporting the further development and supply of this development suite, which will provide wide-ranging standardized developer tools and will be a strong reference set for development on Linux overall. Like other projects we sponsor, we will host the CVS development tree as well as other information about the project as it develops.\n\nRead more about KDE Studio here. Kivio\n\nKivio on the surface is your everyday flowcharting program. Underneath this skin, however, lies much more. Kivio will offer basic flowcharting abilities, but with a twist. Objects are scriptable through Python, and a backend plugin system will offer the ability to make objects do just about anything.\n\nRead more about Kivio here. Korelib\n\nKorelib is a cross-platform C++ library for developing plugin-based applications. It provides a uniform and consistent cross-platform API for developing modular applications.\n\nRead more about Korelib here. Krayon\n\nKrayon, formerly known as KImageShop, is a professional level bitmap image editor component of the KOffice project. Digital camera support is being added in cooperation with www.gphoto.org to provide a common API between GNOME and KDE for digital camera support.\n\nRead more about Krayon here (KImageShop site) Kugar\n\ntheKompany.com is pleased to support the open source project Kugar which is an XML based report generation and viewer tool for KDE. Applications generate data in XML and specify a template (also in XML) to be used to format the data. The resulting report may be viewed on screen or printed. Templates may be specified as a URL allowing businesses to establish a centralised template repository. Kugar's implementation as a KPart means that the production of business quality reports can be easily added to any KDE application, and that reports can be viewed using KDE's Konqueror browser.\n\nRead more about Kugar here. KWord\n\ntheKompany.com is pleased to support the open source project KWord, a powerful word processor for KDE and Linux and an important component of KOffice, a full-featured office package for Linux.\n\nRead more about KWord here. PyQt/PyKDE\n\ntheKompany.com is pleased to support the open source project PyQt/PyKDE which is the standard set of Python bindings for Qt and KDE. PyQt and PyKDE can be used to develop large scale Qt and KDE applications, or can be used to develop rapid prototypes in Python that are converted to C++ at a later stage of development. PyQt includes support for Qt Designer so that the same GUI design can be used to generate either C++ or Python code.\n\nRead more about PyQt/PyKDE here. VeePee\n\nVeePee is a framework that makes it easy for KDE and GNOME application developers to embed the Python scripting language in their applications, and provides users with a powerful and consistent scripting environment. theKompany.com sponsors VeePee and hosts the CVS development tree\n\nRead more about VeePee here."
    author: "Craig black"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "Why don't you wake up and smell the coffee! Kugar, Kivio, KDE Studio, and others."
    author: "David Johnson"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "<P>i'm up!</P>\n\n<P><I> Kugar, Kivio, KDE Studio, and others</I></P>\nplease read my previous posts."
    author: "Evandro"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "maybe if thekompany created free software (gpl) like ximian and eazel, it would get slashdoted.\n\ni don't think this kind of software should even be here..."
    author: "Evandro"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "when ibm donates something it gets posted in slashdot right? good guys those ibm folk.\nwell, theKompany has donated kivio, aethera and the gphoto plugin to konqueror that i know of. \nwhy don't they get the same thanks then?\nhell, if someone gave me apps like that for free, i would more than gladly give 'em $50 bucks. i gave at least that much to stallman when i bougth the 2 tomes of the emacs lisp guide. why is this different? \neven if they hadn't donated anything i will still buy kapital."
    author: "ac"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "please do a search on slashdot for theKompany; they did post messages about them.\n\ni guess you would buy CDE too? or windows? and do you like netscape not being free?\n\ncommercial software sucks! kapital is not different than the other commercial alternatives only because it runs on linux and uses kdelibs.\n\none final question: would you run internet explorer on linux?"
    author: "Evandro"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "i guess people care at different levels about free software. i feel a little guilty about it, but i am thankful there are people like you. me, i'm weak. i'm getting kapital :)"
    author: "ac"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "Come on how old are you? When did microsoft contribute to linux? There is a huge difference. The more success that The Kompany has the more gpl the can release. With all that xiamian and Nautilus talk I'm starting to wonder why your here? Steer up so trouble huh? Talk a little crap huh? I know your game. I'm sure your typing from your gnome desk top right now. Here we love kde and we love The Kompany.\n\nCraig"
    author: "Craig black"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "i like theKompany but i'm not so sure everyone's feels the same way. some people are really ticked off about the fact that non-free software is becoming important software in the kde desktop. i don't mind that much, but Evandro has a point. i don't think he's stirring up trouble. some people _do_ care about this issue a lot. i've always been a kde user because the Qt thing never bothered me much, but some of my friends switched to gnome becaue of it then switched back when Qt went gpl (yes, really). to each his own i say. good luck to free software, good luck to theKompnay."
    author: "ac"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "<i>Come on how old are you?</i><p>\n\ndid i ask how old you are? get a life.</p>\n\n<i>With all that xiamian and Nautilus talk I'm starting to wonder why your here?</i><p>\n\nno nautilus talk, i mentioned eazel and ximian. i never mentioned anything about gnome or gnome software.</p>\n\n<i>Steer up so trouble huh? Talk a little crap huh? I know your game. I'm sure your typing from your gnome desk top right now.</i><p>\n\ni'm typing this from konqueror, as i just finished compiling kde 2.1-final. btw (not that is is any of your bussiness), i don't have gnome-libs installed.</p>\n\n<i>Here we love kde and we love The Kompany.</i><p>\n\ndefine \"we\". i don't use ximian software and eazel software but i like these companies better because they make free software and they believe in free software.</p>\n\n\nhttp://www.gnu.org/philosophy/free-sw.html<br>\nThe freedom to run the program, for any purpose (freedom 0).<br>\nThe freedom to study how the program works, and adapt it to your needs (freedom 1).<br>\nAccess to the source code is a precondition for this. The freedom to redistribute copies so you can help your neighbor (freedom 2).<br>\nThe freedom to improve the program, and release your improvements to the public, so that the whole community benefits. (freedom 3).<br> Access to the source code is a precondition for this."
    author: "Evandro"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "Commercial software does NOT suck just becuase it's commercial.<br>\nSome of it does, some does not.<br>\nSame as open-source...grow up. Just wait till you are looking for a programming job and have bills to pay and no one wants to pay you a top-notch salary, 'cause all software has become a loss-leader..."
    author: "Jacek"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "many people make money writing free software. i won't go in to that, just go to www.gnu.org, www.redhat.com, www.slackware.com, www.kde.org, www.eazel.com, ...\n\n<i>Commercial software does NOT suck just becuase it's commercial.</i><p>\ni don't agree with you. if i can't view the code, how can i trust a software? if i can't trust it, it sucks (in my opinion).\n\nbtw, could you please show me some respect? i respect you for using commercial software so i hope to be respected the same way. people have called me a child here and told me to grow up; last time i heard this was when i was 10 (and from a 10-year-old)."
    author: "Evandro"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-20
    body: "I think its time to ignore this loser. If you don't appreciate all that The Kompany's done for kde thats fine just shut your suck. We're tired of reading what your little ungrateful punk ass has to say. When was ten a couple years ago? Go back to gnome punk.\n\nCraig"
    author: "Craig black"
  - subject: "Re: theKompany.com Releases Kapital, a Personal F"
    date: 2001-02-20
    body: "<i> If you don't appreciate all that The Kompany's done for kde</i><p>\n\ni appreciate kivio, aethera, support for kword and everything else theKompany.com lists in their \"projects\" page. however, i don't like commercial software so i'll keep my self away from their \"products\" page. i don't care what you do.</p>\n\n<p><i>loser, shut your suck, punk ass<i></p><p>\nyou think i'm 12? is this how you talk to kids? bah, i don't care about your words. i respect your view on how software should be distributed, i just don't agree with you. i guess you don't live in a democracy, do you?\n</p>\n\n<i>Go back to gnome punk</i><p>\n\ngo back to windows, lame.</p>"
    author: "Evandro"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "Yeah, they make money writing free software because THEY GET GRANTS from universities. Last time I read RMS has $500,000 lifetime grant from MIT sitting in his bank account.<br>\nIt's easy to say \"everything must be free\" when all your bills are paid.<br>\nBesides that, I'm sure he got nice stock options from many Linux companies.<br>\nGrants and handouts is not a viable long-term business model for a software industry, sorry.<br>\nEvandro, realize: not all commercial software is like Microsoft's. Not all of it is bloated, badly coded and full of insecurities.<br>\nIf open-source can produce better apps, so be it, no problem. But we've been waiting for a package like Kapital from open-source circles and it hasn't come yet. theKompany sensed an opportunity and met the demand. It's called capitalism. It works way better than any other system, trust me (and I know, I lived in a communist country for over 20 years)."
    author: "Jacek"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "Hey, just went to www.kde.org. I can't find anything about these guys making any money. In fact, I looked hard and could not find a way to BUY something from them. They say I can GIVE them money, though.\n\nYou are a strange person."
    author: "Roberto Alsina"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "what are you referring to?\nif i may, what is the position of kde developers (at least those you know) regarding theKompany?"
    author: "ac"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-22
    body: "I am referring to this nonsense about www.kde.org being proof of people making money off free software.\n\nI dare say that every big KDE developer would have made a heck of a lot more money if he had NOT been involved in KDE.\n\nI know *I* would have made more money if I had just started taking fulltime commercial jobs instead of staying in education, and one of the reasons I did that was because I wanted free time to work on KDE :-)"
    author: "Roberto Alsina"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-22
    body: "dare i say then that you're one of the good guys. thanks for everything you guys have done. :)\n\nbtw, are people still working on krn?"
    author: "ac"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-22
    body: "I'm afraid I'm a former good guy these days :-P\n\nNobody is working on KRN currently. I'd dare say KRN is dead, and that effort should be spent on making KNode better instead.\n\nThe bad side of it is that coding in KNode would be extremely unpleasant for me (not because of anything bad in KNode, just because I feel so lame about letting KRN rot)."
    author: "Roberto Alsina"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "check the People page.\n\n\"we're all strange in a very strange way\" (forgot who said that)"
    author: "Evandro"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-22
    body: "First of all: the \"strange\" stuff, I was just trying to be polite.\n\nAs for the \"People\" page, check the credits page, too. Now compare the sizes."
    author: "Roberto Alsina"
  - subject: "There's nothing wrong with commercial software.."
    date: 2001-02-20
    body: "By chance, what web browser did you use before Konqi/Mozilla ?  Do you use VMware?  Do you play any Windows games (be it WINE'ed or native)?  Better yet, do you play any console games?\n\nThere is nothing wrong with commercial software.  In some cases that is the only form certain software exists in.  Open Source software exists primarily because the author had \"an itch to scratch\".  This generally doesn't include Reference software, Educational software, Commercial-quality games, or loads of special-case programs.\n\nWhat do you do for the time being if there is no OSS program for the job?  And please, although I am a programmer, most people are not, so the answer \"make one yourself\" doesn't fly.  Instead, most people will just use what works.\n\nBesides, what does it matter to you that there is a commercial financial program available for Linux now? It's not like you are forced to use it, or it's existence somehow taints all of us.  We've had commercial software on Linux forever.  You don't have to use it if you don't want to, so what's the big deal?\n\n-Justin"
    author: "Justin"
  - subject: "Re: There's nothing wrong with commercial software.."
    date: 2001-02-21
    body: "<i>By chance, what web browser did you use before Konqi/Mozilla ? Do you use VMware? Do you play any Windows games (be it WINE'ed or native)? Better yet, do you play any console games?</i>\n<p>\ni used netscape navigator the same way i used dos/windows before GNU/Linux. i don't use vmware. i play games on playstation. i don't play console games, i play some games from kdegames and some other free games (KOF91, XLogical, Aleph One, XBlast, TuxRacer, 0verkill, GLTron, FlightGear, and others).\n</p>\n<i>There is nothing wrong with commercial software. In some cases that is the only form certain software exists in. Open Source software exists primarily because the author had \"an itch to scratch\".</i>\n<p>\nfree software exists because people have fun making it! do you think \"an itch to scratch\" is enough motivation to make KDE? Linux? all of GNU software?\n\nyou find nothing wrong with commercial software, that's your opinion and i respect it. however, i find a few things wrong with commercial software:\n\n- i can't trust it;\n- i can't view it's code;\n- some times i have to pay for it, and what if i don't have the money to? i use an older version? i should always be behind people that can afford to buy software?\n</p>\n<i>What do you do for the time being if there is no OSS program for the job? And please, although I am a programmer, most people are not, so the answer \"make one yourself\" doesn't fly. Instead, most people will just use what works.</i>\n<p>\nfortunetly, i don't need any anymore. and if i did, yes i would make it myself ;) (and i would have fun doing it)\n</p>\n<i>Besides, what does it matter to you that there is a commercial financial program available for Linux now? It's not like you are forced to use it, or it's existence somehow taints all of us. We've had commercial software on Linux forever. You don't have to use it if you don't want to, so what's the big deal?</i>\n<p>\ni didn't say \"oh no! commercial software!\". i just said i wouldn't use it, and like you said, there's nothing wrong with that.\n</p>\n\nwith all the respect,\n\nEvandro"
    author: "Evandro"
  - subject: "Re: There's nothing wrong with commercial software.."
    date: 2001-02-21
    body: "So you'll use there stuff on the projects page but not on there products page. Well my still living with your parents house friend. Where the hell do you think the the money came from for the projects? Your what's known as a free loader. In the \"real World\" thats not an MTV show, people need money. If you just want to enjoy the fruits of someone else's money and work don't get on here blah blah blahing about your high brow ideology. There's no such thing as a free lunch in the real world. \n        I love gpl i use it when ever i get a chance. However I'm not a coder so I give back in other ways that I can. One being the purchase of products by good companies so there able to give back to the community. Four in particular.\nThe Kompany\nLoki \nIBM\nTroll Tech\nThese four for profit companies have did wonderful things for open source. If they were not able to make a profit then they would not be able to give back gpl software. As for Xiamian and Eazel they'll be out of Business in 18 months. They've got a horrorable business model. If you could think of a way for someone to make money using the gpl for a finacial app like Kapital please share. Don't bother with any head in the clouds BS either.\n\nCraig"
    author: "Craig black"
  - subject: "Re: There's nothing wrong with commercial software.."
    date: 2001-02-21
    body: "when the Kompany licensed their software under the GPL license, they agreed with:\n\n``Free software'' is a matter of liberty, not price. To understand the concept, you should think of ``free speech'', not ``free beer.'' \n\n``Free software'' refers to the users' freedom to run, copy, distribute, study, change and improve the software. More precisely, it refers to four kinds of freedom, for the users of the software: \n\nThe freedom to run the program, for any purpose (freedom 0). The freedom to study how the program works, and adapt it to your needs (freedom 1). Access to the source code is a precondition for this. The freedom to redistribute copies so you can help your neighbor (freedom 2). The freedom to improve the program, and release your improvements to the public, so that the whole community benefits. (freedom 3). Access to the source code is a precondition for this.\n\ni believe in free software; i chose to support people that make free software. honestly, i don't think any of the companies you mentioned would make a difference in the success of KDE, GNU and free software in general. (but they help making linux more popular).\n\nto finish, i would like to say that you have every right to like commercial software and to use it. i can't argue with that.\n\nwhat i've been trying to do here is to show people how important being free is to a software. if you can't get my point, i'm sorry. maybe www.gnu.org can explain it better? or www.debian.org? anyway.\n\nit's been fun discussing this with you. may the source be with you all.\n\ncheers,\nEvandro."
    author: "Evandro"
  - subject: "Re: There's nothing wrong with commercial software.."
    date: 2001-02-21
    body: "Dude we all know this. How do you make money on a product like Kapital while using the gpl? If you have ideas share them, if not don't criticize it. Kapital is different that those Gnome apps you mentioned. I can't think of anyway that it could be licensed and still make money. If you can say so. We all know what \"free\" means it not the point. We're not talking ideology, we're talking real life. If there was a way to produce all gpl software and make good desktop apps while keeping a business alive, the Kompany would do it. The fact is there's not. We need company's like The Kompany doing what there doing. A mix of  proprietary software with gpl is a good business model. It allows the Kompany to give us cool apps we need while reinvesting some of there money back into kde. That way theres not a power struggle of who owns the desktop like there is in the gnome situation. If you use the Kompanies gpl contributions don't knock them for for there proprietary solutions. It's what keeps them alive and gives us cool software. If you don't like it don't buy it. Just don't preach about a utopian world where you can employee as many people as you want to produce software with out any consideration if you'll make any money off it. I know when your young ideology like Stallmens can make you dreamy eyed. Lets get our feet back on the ground and save it for your high school report.\nCraig"
    author: "Craig black"
  - subject: "Re: There's nothing wrong with commercial software.."
    date: 2001-02-21
    body: "i know a lot of companies that make money and they make free software only. it's not up to me to figure out a way of doing so with kapital. if the Kompany really wants to do that, they figure that out (the same way other companies did).\n\n\"Lets get our feet back on the ground and save it for your high school report.\"\n\nyou're a fun person."
    author: "Evandro"
  - subject: "Re: There's nothing wrong with commercial software.."
    date: 2001-02-21
    body: "what about a service based model. say, a tax service, financial advise, data storage, an online trading connection, strong integration with banks and other financial institutions, forecasting (retirement and such) all from the convinience of Kapital."
    author: "ac"
  - subject: "Re: There's nothing wrong with commercial software.."
    date: 2001-02-22
    body: "That would take allot more resources and infascructure than the kompany has available. I don't think you realize how small the Kompany is.\n\nCraig"
    author: "Craig black"
  - subject: "Re: There's nothing wrong with commercial software.."
    date: 2001-02-21
    body: "(honestly, i don't think any of the companies you mentioned would make a difference in the success of KDE, GNU and free software in general. (but they help making linux more popular).\n\nTroll Tech makes Qt the very liberties kde is build on.\nThe Kompany produces quality desktop apps that will go head to head with Microsoft. ( There totally lacking in Gnome)\n\nIBM gives us creditability advertising and publicity.\n\nThey all make a traminduce difference."
    author: "Craig black"
  - subject: "Re: There's nothing wrong with commercial software.."
    date: 2001-02-21
    body: "\"They all make a traminduce difference\"\n\nthey're not essential."
    author: "Evandro"
  - subject: "Re: There's nothing wrong with commercial software.."
    date: 2001-02-22
    body: "Not essential? KDE wouldn't even exist without qt.\nSounds like your just being anti business.\n\nCraig"
    author: "Craig black"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "<em>and do you like netscape not being free?</em>\n\n<p>But Netscape <b>is</b> free! It is free in both the free beer sense <b>and</b> the free speech sense. The developers at Netscape have the freedom of speech, which is guaranteed to them under the US Constitution for those of them residing in the US. And that freedom of speech says that they <b>do not</b> have to use an RMS or OSI approved license.\n\n<p>If you don't like closed source software, you have the <b>freedom</b> not to use any. But the rest of use <b>do</b> have the freedom to use it if we so wish. And apparently there are many of us that intend to use Kapital. So get used to it."
    author: "David Johnson"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "you mixed it all up! by \"free software\" i meant:\n<P>\n<B>\nThe freedom to run the program, for any purpose (freedom 0). <BR>\nThe freedom to study how the program works, and adapt it to your needs (freedom 1). <BR>\nAccess to the source code is a precondition for this. The freedom to redistribute copies so you can help your neighbor (freedom 2). <BR>\nThe freedom to improve the program, and release your improvements to the public, so that the whole community benefits. (freedom 3). <BR>\nAccess to the source code is a precondition for this.</P>\n<P> \"free speach\" is used to explain it because the word \"free\" has two meanings in english (free willy x free beer)."
    author: "Evandro"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "And I meant freedom according to Webster's Unabridged English Dictionary. I don't care what Richard Stallman's definition of \"free\" is. He certainly has the right to invent and use his own language, but he does not have the right to compell me to use it.\n\nActually, \"free\" has seventeen definitions in Webster's, not just two. Consider \"free speech\", \"free verse\", \"free electron\", \"free agent\", etc."
    author: "David Johnson"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "i can imagine then that it is probably impossible to talk to you. are you one of these people who requiere a context explanation before you begin a conversation.\n\nhey it rhymes :)"
    author: "ac"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-23
    body: "Actually, in my rather limited experience, talking (not literally) to David is simple. Talking (literally and electronically) with RMS is almost impossible.\n\nThe only remaining modes in RMS's discourse are sermon, public speech, pep talk, harangue and pontification."
    author: "Roberto Alsina"
  - subject: "What's the release date? Sept or March?"
    date: 2001-02-20
    body: "How long do I have to order Kapital at the lower price?\n\nUntil september?\n\nOr until the end of february?\n\nThe webpage mentions the initial release as this month, September as the date for the last free upgrade if you order now, and memtions the price is good until the Final Release Date.\n\nBut doesn't explicitly say what the final release date is.\n\nI just want to know if I have to hurry to buy this thing.  :)\n\n--john"
    author: "john"
  - subject: "Re: What's the release date? Sept or March?"
    date: 2001-02-20
    body: "The first beta release of Kapital is the first of March.  The final release is dependant on the number of requests we get for features.  The September 1 date is arbitrary and is how long you will be able to get free updates.\n\nThe lower price will stop when the final product is ready, and again, that depends on the requests.  I imagine that will be in April on a guess.\n\nshawn"
    author: "Shawn Gordon"
  - subject: "Re: What's the release date? Sept or March?"
    date: 2001-02-20
    body: "cool.\nso i have some time then. :)\n\nthanks for the response.\n\n--john"
    author: "john"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "Here's a cool idea -- what if you already have the \"versacheck\" paper and a laser printer? Why wouldn't you be able to print your own checks? essentially, it's just a sheet of paper divided into three parts. Quicken does this and I think it's definitely a great feature.\n\nI'd also like to see more Quickbooks-like capabilities. That said, nice work and I think I'll be picking up a copy when it gets a little more complete!! I especially love the Bill-minder feature like quicken!!"
    author: "Jamie Becker"
  - subject: "TheKompany needs to make money"
    date: 2001-02-21
    body: "Anyone who thinks that kde can live forever without having people making money from are wrong.\nMandrake Suse etc they all make money with kde there is nothing wrong with as long they don\u00b4t own it.\n\nWe must think of kde as base desktop not all you migth need but basic thinks \n\nCan you blame IBM for making money no you can't\n\nWe see the ethical side i don\u00b4t mind if Thekompany makes a lot of money Kde as long as they help building Kde basics \n\nYou guys have been doing a great jod an you should try to sell services likde ximian does\nbut for kde \n\nKeep up the good and feed your own families"
    author: "Jorge Costa"
  - subject: "Re: TheKompany needs to make money"
    date: 2001-02-21
    body: ">Mandrake Suse etc they all make money\n\n**Disclaimer**\n\nI am very dumb WRT how open source companies (like the ones mentioned above by you) make money, so I may be way off base in what I'm about to say here.\n\n**End Disclaimer**\n\nHaving said that, I feel it should be noted that the companies you used as examples - and many others - also allow all of their products to be downloaded free of charge.  And I think that this is where people's distaste for TheKompany's current tactic here springs from.  (The one difference is that theKompany has taken a slightly different tack: some of their products are free, some aren't.)\n\nI have yet to form an opinion on the matter, other then the idea that GNUCash works fine for me.\n\n--WorLord"
    author: "WorLord"
  - subject: "Re: TheKompany needs to make money"
    date: 2001-02-22
    body: "What a wonderful idea we should expect ford a intel and the others do give there stuff away for free as well. This is going to be great. Just drive on over to the grocery store and everything will be free. Of course will all work for free as well. It will be a workers paradise we'll call it the dictatership of the prolitariate!! A workers paradise. I can't believe no one has thought of this before. Workers of the world rise up. We will demand everything to be free free free. Who cares if it cost them thousands of dollars to produce there products. Who dare they expect something in return for there work!! Damn them they should have to give them away!! Glory to the workers!!!\n\nCraig"
    author: "Craig black"
  - subject: "Re: TheKompany needs to make money"
    date: 2001-02-22
    body: "Damn dude,\nYou really need to attend a basic course in English grammar and spelling. Although I'm surprised you know large words like prolitariate."
    author: "Me"
  - subject: "Re: TheKompany needs to make money"
    date: 2001-02-22
    body: "lol your right it was'nt in ispell. Acually i'm well read but I can't spell worth a damn.\n\nCraig"
    author: "Craig black"
  - subject: "Way to miss the point, guy."
    date: 2001-02-22
    body: "When you actually read and respond to something I wrote, instead of things I didn't even imply, maybe you and I can have an intelligent conversation about this matter.\n\n--WorLord"
    author: "WorLord"
  - subject: "Re: Way to miss the point, guy."
    date: 2001-02-22
    body: "Sorry that was'nt ment as a critizism of you but of the idea that some people have.\n\n (I think that this is where people's distaste for TheKompany's current tactic here springs from. (The one difference is that theKompany has taken a slightly different tack: some of their products are free, some aren't.)\n\nI have yet to form an opinion on the matter, other then the idea that GNUCash works fine for me.)\n\nCraig"
    author: "Craig black"
  - subject: "Re: TheKompany needs to make money"
    date: 2001-02-22
    body: "i think Suse is actually selling some administration Software linux based witch is not open source.\n\nI am sorry about my english but what i ment was companies need to make money i order to afford their staff. \nthere different ways to do it\n\t- software\n\t- distros\n\t- services\n\t- etc..\n\ni don't see anything wrong with it.\n\nwhat i believe is if they are building a financial manager for KDE (and please see this as a example only) they should share some of the code like the report making part so that they give a bit back to KDE team. \nIn that they are using the efforts of thousand of people but not for free it's more like a trade\n\nTheKompany<---> Kde Team\n\nI prefer to see this as a trade process\n\nOne thing the more commercial software you get on kde the better as long as kde itself is free\n\ni hope i made my point"
    author: "Jorge"
  - subject: "Re: TheKompany needs to make money"
    date: 2001-02-22
    body: "Let's look at some of the pieces in Kapital.  It makes use of VeePee for user scripting as well as charts and graphs, and will be the engine that will support the online banking.  We developed VeePee and gave that to the community.  It will also use Kugar for reporting, again something we've given to the community.  Both of those applications are going to go in core KDE from what I understand.\n\nHow much do we have to give away before it's enough?"
    author: "Shawn Gordon"
  - subject: "Re: TheKompany needs to make money"
    date: 2001-02-22
    body: "Shawn i wasn't sayng you guys don't give back anything to the comunity.\n\nI just wanted to make a point:\nthat's the to go\n\n\ni personaly thin~k you guys are doing it\n\n\nthank's for kugar\nveepee\nCrayon\nkvivio\netc ..."
    author: "jorge"
  - subject: "Clarification of a Point"
    date: 2001-02-22
    body: "First, to point out some things I sure didn't say, and didn't even imply:\n\n<li>I am not opposed to the Kompany's attempt to make money.\n<li>I do not feel the Kompany has given \"nothing\" back to the community.  Anyone with a modem shouldn't feel this way, either.\n<li>I do not believe closed-source/proprietary software is inherently evil. \n\nNow, to the one point I was trying to make:\n\n<b>No one with any significant knowledge of Linux - not here on this message board, or in the Kompany's headquarters, or anywhere else - should be at all surprised by people bitching about this commercial release.</b>\n\nPeriod, end of statement.  The Kompany is bucking a trend; the typical M.O. for the \"bigger names\" in the Linux community is to charge money for improved, boxed, documented, and supported sets of software that can *also* be downloaded (without the perks) free of charge.  The Kompany is not doing that, so people are bound to get confused and see this as some type of move to Make Linux Proprietary(C).  \n\nWhile that supposition may not be accurate, it is going to be mightily popular.  And I thought I should bring that up given the fact that the names Mandrake and SUSE were being dropped, supposedly as model examples of \"linux people making money\".  \n\nThat's it, and that's all.  Any statements suggesting that we should all \"work for free\" or that \"the Kompany is taking from the community to make money for itself\" will be seen as the rash, ignorant comments that they are, and duly ignored as a result.\n\nI'm out like the fat kid in dodgeball,\n--WorLord"
    author: "WorLord"
  - subject: "Re: Clarification of a Point"
    date: 2001-02-22
    body: "I don't think we should ignore any comments\n\nI believe that it's up to us people that understand the needs of companies like TheKompany and see their mission (money revenues included) as valid and good for the comunity, to help those who still believe that Kde is only for geeks. \n\nMake Kde easier but let it free as it is wrigth now\nDistros like Mandrake my favourite have been doing a very good job with Kde and i believe maybe not in the near future they could release some closed software (maybe network management who cares) but doesn't mean there not helping the comunity. \n\nI believe Thekompany is making good software and Shawn you guys have been doing a great job.\n\nIt's all a matter of give and take."
    author: "Jorge"
  - subject: "Re: Clarification of a Point"
    date: 2001-02-22
    body: "I don't think we should ignore any comments\n\nI believe that it's up to us people that understand the needs of companies like TheKompany and see their mission (money revenues included) as valid and good for the comunity, to help those who still believe that Kde is only for geeks. \n\nMake Kde easier but let it free as it is wrigth now\nDistros like Mandrake my favourite have been doing a very good job with Kde and i believe maybe not in the near future they could release some closed software (maybe network management who cares) but doesn't mean there not helping the comunity. \n\nI believe Thekompany is making good software and Shawn you guys have been doing a great job.\n\nIt's all a matter of give and take."
    author: "Jorge"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "Hi,\n\nthat all is very great of course. But I do not have any chance to purchase.\nWill there be a reduced version for free too?\nI do believe in KDE and theKompany if they would do so.\n\nRegards,\nChristian Parpart\nhttp://www.surakware.com."
    author: "Christian Parpart"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "Slash dot still has not run a story about Kapital, but you all will notice how quick they posted a story about Ximian today. Lets all rush over there and submit stories about Kapital and let them know how we feel about there biased reporting!!! They can't ignore KDE!!!\n\nCraig"
    author: "Craig black"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "They're not ignoring kde. They're ignoring theKompany. Different things, right? right?"
    author: "ac"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "No there gnome centric. While the majority of Linux users prefer KDE you wouldn't know it by reading Slash Dot. We need to put the pressure on them by posting as much KDE material as we can. Kapital just falling into the KDE category.\nSo lets keep up the pressure folks.\nCraig"
    author: "Craig black"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "i disagree. would you advertise and be so passionate about the upcoming borland ide? after all it's for kde. falls in the kde category."
    author: "ac"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-21
    body: "Borland needs our support as well. They are a member of the kde league. However I'm not a programmer. I'm a end user. So no i wouldn't be as passionate about a ide as i am about a quality professional desktop application. I'm not sure Borland contributes free applications to kde like the Komapany does. Troll tech is also a good company and needs our support.\n\nCraig"
    author: "Craig black"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Fi"
    date: 2001-02-22
    body: "programmers feel the same about protecting free software. we like to dig in, modify, share.... just as much as you like professional desktop applications for kde. so you see, you need to be a little more understanding of other peoples agendas."
    author: "ac"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Fi"
    date: 2001-02-22
    body: "Thats great I'm sure you liked The Kompany's kdestudio then. The better they do with there closed source stuff the more they'll be able to give us as far as there open source stuff.\n\nCraig"
    author: "Craig black"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-23
    body: "That's because slashdot is owned by VA Linux, VA wants to keep Linux focused on the server area.  GNOME is kind of a hacked together buggy mess that will never make a good desktop ever, the more people that associate linux with GNOME, the more people that will never use linux on the desktop."
    author: "John"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-22
    body: "I am glade that everyone is showing their ideas this is as important as coding or translanting for KDE.\n\nAgain thanks"
    author: "Jorge"
  - subject: "updates"
    date: 2001-02-23
    body: "How will updates be handled, considering that KDE and the open-source world in general moves so fast?\n\nSpecifically, suppose KDE 3.0 and Qt 2.5 and glibc 2.3 (or whatever) come out in a year.  Will a recompiled version of Kapital be available for those who have already bought it?\n\nI don't mind paying for new features in the future, but it drives me nuts to have 3 versions of every library installed just to make things run.  That's one of my biggest reservations about close-source software"
    author: "Charles"
  - subject: "Re: updates"
    date: 2001-02-23
    body: "yes they will."
    author: "Shawn Gordon"
  - subject: "stock options"
    date: 2001-02-23
    body: "I would hope Kapital can track and automatically vest stock options.  Quicken does.\n\nMake this thing good, and I'm jumping ship!  I won't miss the VMware/Windows/Quicken bloat."
    author: "Charles"
  - subject: "Re: theKompany.com Releases Kapital, a Personal Finance Manager "
    date: 2001-02-23
    body: "Until the KDE people make a <B>'free'</B> solution to GNU cash people will keep using Gtk applications, as well as GNOME, which will keep people away from Linux.  This dosen't help because the app still cost money.  If people realy wanted KDE/Linux to suceed they would be alot more concerned with GNOME than proprietary applications.  GNOME and Gtk apps are linux's only barier to the desktop, as long as there still being used KDE/Linux will never suceed."
    author: "John"
  - subject: "I value freedom more than I value cash"
    date: 2001-02-23
    body: "John, you've got a point.  Any time I introduce someone to Linux, they usually want to start with Gnome because there's an impression it's more free than KDE.  And then when they find bugs and inconsistencies in Gnome, they get turned off on Linux entirely.  So it hurts KDE.\n<p>\nSo freedom is important.  But as RMS will point out over and over, \"freedom\" doesn't necessarily mean price.  Your comment of \"This dosen't help because the app still cost money\" misses the point.  Freedom means not having your rights taken away.  The problem here (if there is one) is that Kapital is closed source, not that it costs money.\n<p>\n<em>Shawn, if you're reading this--</em> I will probably buy Kapital anyway, but if there was some way for it to <em>be open source yet still have you make money, I'd be so excited I'd buy a copy for myself, my sister, and my mother.</em>  I (and probably many other people) have no problem paying money, but after years of Microsoft's rule, not having the source makes us all feel pretty helpless and vulnerable and stranded.\n<p>\n(Of course, if you knew how to make money on open source, the rest of the open source world would be beating down your door and you'd be rich merely from consulting...)"
    author: "Charles"
  - subject: "Re: I value freedom more than I value cash"
    date: 2001-02-23
    body: "I've said this before, but I have no problem with people having the source, my problem is with people redistributing it.  Someone on the dot recently suggested creating a license that gave the source to the customer, but limited what they could do with it.  I've taken this suggestion very seriously and have been researching the possibilities ever since.  However, it's better to say it's closed, then provide a limited open source license later, than saying it's open and closing it later.  So that is where we are at right now."
    author: "Shawn Gordon"
---
<a href="http://www.thekompany.com/products/kapital/">Kapital</a> is our personal finance manager package for KDE and Linux. It is meant to be in the <a href="http://www.thekompany.com/products/kapital/screenshots.php3">spirit</a> of Intuit Quicken or Microsoft Money, but without the bloat associated with those packages from years of justifying upgrades. Kapital has everything you need for managing all your personal finances. Kapital has been tested with various other window managers including GNOME, and will work with them, however our area of focus and support is currently KDE. 
<i>[<b>Ed</b>: This is commercial software, see below for price info.]</i>


<!--break-->
<p>
Kapital features include:
<ul>
<li><b>Check register</b> for entering and clearing various types of transactions such as checks, deposits, ATM, EFT, etc.
<li><b>Calendar</b> for setting up scheduled payments, used in conjunction with a "Bill Tracker" alarm to let you know that bills are coming due.
<li><b>Check and report printing</b>, including graphics and logos. 
<li><b>Basic and advanced searching capabilities</b> and online reporting as well as charts and graphs. 
<li><b>Predefined categories for transactions.</b> Include sub-categories, and the ability for users to create and delete new categories. 
<li><b>New account wizard</b> that goes through various types of budget scenarios like Single, Married, Married with Children, Single with Children, etc. 
<li><b>Import/Export features</b> for Quicken QIF file format. 
<li><b>Budget Tracker.</b> The user sets up a budget for various items, and alarms can sound when you come within a threshold, or exceed it for defined time periods, typically a calendar month. This would usually happen when you are entering a new transaction into a category and the amount causes it to exceed the threshold. You can also do charts, graphs and reports that show where you are in the time period for your budget so you can see if you are ahead, behind, or spot on. 
<li><b>Check Designer.</b> This feature is very much in development, but you will be able to use a web page to design your own checks and then have them delivered to you as you would with any third party check printing company. 
<li><b>Create any type of account</b> to manage. This can be a checking account, savings account, stock, investment, retirement, etc. 
<li><b>Online Banking.</b> As we develop partnerships with financial institutions you will get to take more and more advantage of online banking -- with the ultimate goal of having the ability of paying your bills and sending money between various types of accounts all from your desktop. 
</ul>
<p>
What do you want to see? Let us know and we'll make your dreams a reality. 
<p>
The initial release of Kapital will be available as a download by the end of February 2001. As soon as the enhancement requests dwindle to a trickle, we will then burn the CD packages and send the final product to everyone. You will still be able to download free updates until September thus protecting your initial investment for some time. 
<p>
In consideration for early adopters of Kapital we will be extending a discount on all sales made prior to the actual availability date.  Pricing follows for the Standard Edition:
<p>
<ul>
<li>$39.95 -- $24.95 if purchased during pre-sales -- download only, all electronic doc.
<li>$49.95 -- $29.95 if purchased during pre-sales -- physical package with hard copy doc.
</ul>
<p>
Purchasers of Kapital will be entitled to free electronic updates through September 1, 2001.
<p>
A Kapital mailing list for customers will be available as well as support queues.  We will answer these support questions as time permits,
for priority response you should take a look at our support services.
<p>
You can order online and get more information <a href="http://www.thekompany.com/products/kapital/">here</a>.

