---
title: "People of KDE: George Staikos"
date:    2001-05-30
authors:
  - "Inorog"
slug:    people-kde-george-staikos
comments:
  - subject: "good interview"
    date: 2001-05-30
    body: "This interview is quite amusing.  :)\n\n> ~ Who would you hire to play in a KDE\n> commercial? And what would the scene be like?\n>\n> Arnold Swarzenegger, for sure! It doesn't even \n> have to have anything to do with KDE. Just \n> lots of blowing stuff up, and then a KDE logo \n> at the end."
    author: "KDE User"
  - subject: "Re: People of KDE: George Staikos"
    date: 2001-05-30
    body: "~ If you had to design a banner with a KDE slogan, what would it say?\n\nGot KDE? (and a picture of Konqi with a \"milk moustache\")\n\nlol!  sounds a lot like that patrick ewing banner at www.nba.com"
    author: "me"
  - subject: "Jumpman lives!"
    date: 2001-05-30
    body: "You'll be happy to know that someone is currently fixing up Jumpman for the PC (DOS/Windows).\n\nhttp://www.classicgaming.com/jmanproject/\n\nMaybe it will work under wine or dosemu??"
    author: "KDE User"
  - subject: "George knows his stuff!"
    date: 2001-06-01
    body: "I've always been very impressed with the KDE community.  I've been using KDE since the early 1.0 betas, but it's only recently that I've been submitting bug reports.  The interaction with the developers has really enlightened me to this great gift that the KDE developers are providing to Linux users.  Anyone who has used this growing set of applications and its underlying desktop should take the time to provide some feedback/bug reports to point the developers to some of the daily difficulties that we may run across.  I've found that it works _very_ well and see bugs that I've reported fixed in the next release or two.  Thanks to the developers who are doing all the HEAVY lifting!\n\nGeorge is one of the developers that I've had a chance to interact with in resolving a Konqueror/SSL difficulty.  Congratulations George on providing some great code and being very helpful to your community of KDE users.\n\nPhil -- Ottawa, Canada"
    author: "APW"
  - subject: "Re: People of KDE: George Staikos"
    date: 2001-06-03
    body: "i really enjoy the series \"People of KDE\". Just sometimes the questions are a little boring. Once a friend of mine started interviews on several of my other friends. He was diving into special themes and it worked very well! So my suggestion is: ask *some* background questions and then continue with a dynamic email-interview.\njust 2c..."
    author: "Dannu"
  - subject: "Re: People of KDE: George Staikos"
    date: 2001-06-03
    body: "What about Greece???"
    author: "yannis spanos"
---
The current interviewee of <a href="mailto:tink@kde.org">Tink</a>'s grand <a href="http://www.kde.org/people/people.html">series</a> is <a href="http://www.kde.org/people/george.html">George Staikos</a>. One of the members of the young KDE guard, George is very active in the parts of KDE that relate to cryptography. He helps with KMail and KOffice too, and he makes up brilliantly for the notorious lack of Canadian KDE developers :-). On a personal note, George is one of the very few KDE friends that I had the great chance to meet in person. Consider yourself invited to a joyful reading with this new colourful interview of the successful "People series".
<!--break-->
