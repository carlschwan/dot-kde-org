---
title: "KDEPrint Site Goes Live"
date:    2001-12-05
authors:
  - "numanee"
slug:    kdeprint-site-goes-live
comments:
  - subject: "Wonderful!"
    date: 2001-12-05
    body: "The printing system in KDE 2.2 and the further developments in KDE 3 are my favourite part of KDE: when combined with CUPS they finally bring printing on UNIX out of the dark ages! I'm still amazed how quickly it was developed once someone got an itch to do it. It often seems to be overlooked - do people not realise just how imporant decent printing support is, both for general quality of life *and* in getting KDE accepted by business?\n\nCongratulations to all involved."
    author: "Jon"
  - subject: "Re: Wonderful!"
    date: 2001-12-05
    body: "out of the dark ages! .. really?!?! .. what about printer support?"
    author: "Andreas"
  - subject: "Re: Wonderful!"
    date: 2001-12-05
    body: "Ever tried cups?"
    author: "Robi"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "what is cups really?"
    author: "Andreas"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "there's a good description on www.cups.org. As Jon already said cups together with the gimpprint drivers make a quite good printing system. I'm using it with a hp970cxi and I'm happy with the results... You should give it a try. What distribution do you use?"
    author: "Robi"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "yeah! but the printer world \"outside\" the domain of hp is still in the darkages and have always been"
    author: "Andreas"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "Even inside the HP domain as well. And CUPS can be fairly shaky with Samba in a mixed environment. Not happy with CUPS at all"
    author: "Geir"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "Not true at all.\n\nAt I mentioned in my previous reply, gimp-print is a collection of printer drivers for many inkjet and laser printers. Take a look at their supported printers list:\n\nhttp://gimp-print.sourceforge.net/p_Supported_Printers.php3\n\nI can personally vouch for the excellent quality of their Epson inkjet drivers."
    author: "Jon"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "I have a Samsung ML 6060N, network printer, apparently it supports linux, but the instructions for their including software are terrible, in fact, non-existent.  So basically, does anyone know how to make this work with linux?"
    author: "stuart_farnan"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "I have a mita 570 laserprinter ... no support! hardware support for linux is not good but it's getting better and better"
    author: "Andreas"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "One of the reason Epson inkjets and decent HP laserjets are well supported is that they speak a command language (ESC/P / PCL). Write a driver for that command language, and you support all the printers which understand it. Similar to having a modem which understands the AT command set.\n\nThe problem is that many cheap modern printers are 'winprinters'. Just like with 'winmodems', they have stripped out of all the complicated electronics, and make the computer do all the processing. This makes writing drivers for them much much more compilicated.\n\nDo a bit of research the next time you are buying a product, to check that it isn't a crippled 'winproduct'."
    author: "Jon"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "personly I never buy hardware without linux support ... the printer isnt mine"
    author: "Andreas"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "I don't want to answer all the posts. I'll just try to give some clues. For people having printing problem, some mailing lists exists and we'd be glad to help you if we can (http://cups.sourceforge.net, http://printing.kde.org).\n\nIf you're looking for printer support, the first thing you should do is to have a look at http://www.linuxprinting.org. This site has a huge DB about printing under Linux, and you can also download drivers from there. Now, about the printer support under Linux, let me state that the first reason about the poor support is mainly due to the printer manufacturers themselves. So I'm tempted to say that if you really want to complain, send an e-mail to your printer manufacturer. This situation is slowly changing, and some big manufacturer has begun to support Linux. But in most cases, the printer protocol is proprietary and drivers can only be made be reverse engineering, which takes a lot of time.\n\nBeing in the printing world for quite some time, I can tell you however that many printers *are* supported under Linux. All PS printers of course, PCL printers (3,4,5, at least), Epson printers, Lexmark printers, Canon printers, even so-called \"winprinters\" are partially supported (like the PPA printers from HP). In many cases, your printer *is* supported, the only thing is to know how, and that's where linuxprinting.org is useful. Of course not *all* printers are supported, especially some \"exotic\" ones, because they are not commonly used, especially by people that would be tempted to write a driver for them. In that case, the only possibility is to ask the manufacturer.\n\nI'd like also to add that KDEPrint is not a low level library, it's mainly intended to be a user interface between KDE applications and a print spooler. In no way it deals with supporting this or that printer, this is the job of the print spooler, and KDEPrint doesn't aim to help there.\n\nBye.\nMichael."
    author: "Michael Goffioul"
  - subject: "Re: Wonderful!"
    date: 2001-12-05
    body: "Postscript printers have (obviously) always been supported well.\n\nFor the newer printers, you have a lot of choice... particularly with tools like gimp-print developing decent support for modern inkjets and lazer printers.\n\nCUPS + gimp-print is already the default printing system in Mandrake. I hope it spreads through all the other Linux distos, and all the other UNIces as well."
    author: "Jon"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "You don't need gimp-print with cups.  I think most distro's ship the cupsomatic backend now, and linuxprinting.org gives you an enormous database of printers to choose from to generate printer specific PPD files that make use of it.  I've got this config working for an OfficeJet K60 and the quality is as good as Windows.  Didn't need gimp-print at all.  I did need the HP ptal driver kit to get the actual connection to the OfficeJet working, but that has nothing to do with KDE or the printing part per-se, just the connection over USB.\n\nJohn"
    author: "John"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "Some of the drivers from Foomatic are based on gimp-print. Actually, the stp driver for ghostscript IS gimp-print. So you may still use gimp-print without knowing it (gimp-print driver is usually compiled in ghostscript in recent Linux distros).\n\nMichael."
    author: "Michael Goffioul"
  - subject: "Re: Wonderful!"
    date: 2001-12-05
    body: "http://www.linuxprinting.org\n\nThere's support for hundreds of printers, usually at high quality, along with\ndetailed information on how to get your printer working under Linux. Gimp-driver\nis even known to give better print quality then windows drivers for some\nprinters.\nCoupled with Foomatic, you can get support for your printer under CUPS, LPR,\nLPRng, PDQ, and PPR.\n\nIf that's not printer support..."
    author: "Michael Goffioul"
  - subject: "gimp drivers for an idiot"
    date: 2005-01-10
    body: "Hi all,\n\nAlong the lines of the dark ages...  I speak of course only of my knowledge.  I'm having a bit of trouble finding info that I can understand on gimp drivers (I'm a geologist and I'm much better with rocks...).  I have several HPs that haven't worked well since an HP 120nr was introduced to them.  Now I have some font issues, like fonts printing too thick.  I've looked up the supported printers list for gimp and only one of my printers is on it.  Does this mean I shouldn't try gimp?  Is there another option at all?  If you answer my query, do please use the smallest words you can, this geek-speak is new to me.\n\nThanks,\nRebecca"
    author: "Rebecca Kraft"
  - subject: "Re: Wonderful!"
    date: 2001-12-05
    body: "I'll respond to this rather than the other respondents of this node.\n<P>\nAll of the other tools that already exist work fine, I can get network and local printers to work great.  But I should have to go to http://www.linuxprinting.org to get my printer to work.  There should be a straight forward, easy to find, consistant place to setup the printer."
    author: "Paul Seamons"
  - subject: "Re: Wonderful!"
    date: 2001-12-05
    body: "First post here (can you tell).  Forget the duplicate post and that \"should\" should be \"shouldn't\".\n\nSigh... (Bangs head against desk)..."
    author: "Paul Seamons"
  - subject: "Re: Wonderful!"
    date: 2001-12-05
    body: "Recent distributions include a package containing a bunch of drivers for\nmany printers (taken from linuxprinting.org). In that case, you don't need\nto go there, all available drivers are listed when installing a printer.\nYou have 2 ways around: either you install all possible drivers on your\nHD, but the major drawbacks are waste of space and new printers not supported,\nor you can simply download the driver from linuxprinting.org (not a very\nbig deal) and use the \"Other\" button when a driver is requested in kdeprint.\n\nMichael."
    author: "Michael Goffioul"
  - subject: "Re: Wonderful!"
    date: 2001-12-06
    body: "I can't understand what the fuzz is about. I've used linux for years, and printing\nhas never been a problem. Still, if it makes you happy, congrats...\n\n\nE."
    author: "E"
  - subject: "What about True Type Fonts??"
    date: 2001-12-05
    body: "I wonder whether other languages' true type fonts, like for hindi, telugu, etc., get printed with this! Or the user got to get the PCF font?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: What about True Type Fonts??"
    date: 2001-12-06
    body: "First, let me say that the KDEPrint site (and screenshots!!!) look fantastic.  Congratulations, developers!  Secondly, is anyone thinking of plugging in the libgnomeprint library to KDEPrint?  It seems that with CUPS, gimp-print, and gnome-print tied together, there will be even more printing resources under this nice interface.  I've heard that the gnome-print people are looking into a CUPS plug-in.  It would be a nice thing if the KDE people and the GNOME people worked together on this one. :-)"
    author: "andrew"
  - subject: "IOSlave"
    date: 2001-12-05
    body: "Wouldn't be fine to handle (view and delete) printer jobs through an konq. ioslave instead of through an extra application?"
    author: "Marc"
  - subject: "Re: IOSlave"
    date: 2001-12-05
    body: "You can. Load the ioslave 'print:/', and click \"Manager\"."
    author: "Chris Howells"
  - subject: "Re: IOSlave"
    date: 2001-12-05
    body: "Damn, now konqy gets really the \"all-in-one\" app :-)\n\nHow about a TV ioslave (which lists the channels) and an embeddable TV part ?\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: IOSlave"
    date: 2001-12-05
    body: "...maybe even with preview in icon mode :-)"
    author: "aleXXX"
  - subject: "Re: IOSlave"
    date: 2001-12-05
    body: "What we REALLY need is one kio_exchange and a kio_wince! I can imagine they are extremely difficult to code, but they'd add a LOT of value to KDE (more than $69 ;)!"
    author: "me"
  - subject: "Re: IOSlave"
    date: 2001-12-05
    body: "I use ppp to wince + an ftp server on the handheld + kio_ftp to do something quite like that.  I simply dock my casio, and click on the konqueror shortcut.  Wallah! All my files are a drag and drop away.\n\nMordy"
    author: "Ben Ploni"
  - subject: "Re: IOSlave"
    date: 2001-12-06
    body: "Not having a CE unit (okay... I have two CE v1.0 units), I can't do this - but I urge you to write it up in a HOWTO, or even better, as a DocBook format and contribute it to the PIM team.  I know several people off-hand that would find that solution very useful, and some of them couldn't get it to work without a bit of hand holding (which is what good documentation does).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: IOSlave"
    date: 2001-12-06
    body: "If I find the time, I'll contribute a kde-centric way to use wince handhelds easily with linux."
    author: "Ben Ploni"
  - subject: "Re: IOSlave"
    date: 2001-12-06
    body: "I know you can acces a wince's files. What I was talking about was not transferring files, but syncing addresses, calendars etc. with korganizer/aethera and others."
    author: "me"
  - subject: "Re: IOSlave"
    date: 2001-12-06
    body: "I agree. In fact, I have sent a bug/wishlist suggesting that IO should be more generalized. The KIO is a great first attempt at such a thing. This should actually go much further. KIO should be used for Input/Output even on Open/Save dialogs."
    author: "a.c."
  - subject: "Re: IOSlave"
    date: 2001-12-06
    body: "It does - even the first version of the file dialog which I wrote for KDE 1.x supported network transparency. The reason it doesn't do so in all cases is because the author of the application chooses between requesting a URL with getOpenFileURL() or a local file with getOpenFileName(). So, if you want things fixed you should identify the specific applications which don't support URLs and talk to their authors.\n\nRich."
    author: "Richard Moore"
  - subject: "Crap that looks good"
    date: 2001-12-05
    body: "This is the kind of thing that end users really need in order to use any environment. Congrats to the KDEPrint team!"
    author: "Jackson Dunstan"
  - subject: "ho hum"
    date: 2001-12-06
    body: "looks like windows, i use linux to get away from it...but kde wants to be the windows interface of linux. buggar."
    author: "buddy_love"
  - subject: "Re: ho hum"
    date: 2001-12-06
    body: "What are you talking about? The printing interface is completely different from Windows' for several reasons:\n\n1) It's Open Source. Windows' is not. Hello!\n2) It's not proprietary. CUPS uses plain old text files, whereas I still can't make heads or tails of the Windows driver packaging system.\n3) It's not annoying! The most annoying thing about Window's UI is that it often assumes the user is an idiot, and proceeds to make good on that assumption. In Linux and KDE (or any OSS Nix and any OSS UI) any administrative GUI is just a frontend to configuration files and console tools. If you don't want to use the GUI, ignore it.\n\nAnd besides, if you dislike KDE's UI (and basically KDE is a UI) then what in the world are you doing spending time on KDE's news site?\n</huff>"
    author: "Carbon"
  - subject: "Re: ho hum"
    date: 2001-12-06
    body: "This person is simply a TROLL! He or she must be jealous of KDE! :)"
    author: "tryme"
  - subject: "Re: ho hum"
    date: 2001-12-06
    body: "KDE wants to be user-friendly and usable, even for Linux newbies. And apart \nfrom the many bugs in Windows and the marketing policy of M$, I still think\nthat M$ is quite good at doing user interfaces that are easy to use. So what's\nthe point if you use some ideas from Windows world?\nBeyond that, M$ only used ideas that seems natural: a combobox to select a\nprinter, a listview to see jobs, ... It's not windows, it's natural. I admit\nthat the print dialog layout is inspired from Office, but changing the layout\njust to avoid resembling Windows doesn't make much sense to me.\nIf Linux wants to enter the enterprise, you need to provide a good UI to your\ncustomers. Most people are using M$, so it's much easier for them if you\nprovide an intuitive interface where they can find they way quickly and easily.\n\nWhen I switched to Linux, I also wanted to get away from Windows. Not\nespecially from its user interface, but more from its monopoly and marketing\npolicies.\n\nMichael."
    author: "Michael Goffioul"
  - subject: "Re: ho hum"
    date: 2001-12-06
    body: "So print from the command line if you want. Maximum Linux appeal for \"recovering Windows addicts\" will be achieved when KDE can look as familiar to them as they want it to, and works better than Windows in *every* way, including printing.  I'm all for it!"
    author: "David McClamrock"
  - subject: "KDEPrint fits even GNOME developers"
    date: 2001-12-06
    body: "I *luv* the FAQs there... I *hope* they will extend it even more. I like best this part:\n\nQ: Can I use KDEPrint together with GNOME?\nA: Yes. We even know some GNOME developers who use the \"kprinter\" utility on a day-to-day basis for their printing needs...\n\n[No flame-war intended here. Just to proof: KDE/GNOME inter-operation is starting to gain ground...  ;-)  ]\n\nVery usefull stuff, these FAQs!  Also the various print tutorials the printing.kde.org site. Printing sucks much less with CUPS/KDEprint. Big kudos to the whole kdeprint team. (Without real world print support, no real world desktop environment for Unix. KDEPrint was one of only a few \"missing links\". In place now....)"
    author: "CUPS-lover"
---
<A href="mailto:howells@kde.org">Chris Howells</a> wrote in to inform us of the new KDEPrint site at <a href="http://printing.kde.org/">printing.kde.org</a>.  The site is chockful of information including <a href="http://printing.kde.org/screenshots/">pretty screenshots</a>, a <a href="http://printing.kde.org/development/">description of new KDEPrint features</a> slated for KDE3, <a href="http://printing.kde.org/info/">information</a>, <a href="http://printing.kde.org/info/presentation/">presentation</a>, and <a href="http://printing.kde.org/documentation/">general documentation</a> including a <a href="http://printing.kde.org/faq/">faq</a>, <a href="http://printing.kde.org/documentation/handbook/">handbook</a>, and <a href="http://printing.kde.org/documentation/tutorials/">tutorials</a>.  But wait, there's more!  For no extra cost, you may subscribe to a brand new <A href="http://mail.kde.org/mailman/listinfo/kde-print">kde-print mailing list</a> where both users and developers are welcome.  So if you have trouble or issues with KDEPrint not already dealt with by the site, feel free to subscribe to that list.  Enjoy.
<!--break-->
