---
title: "OSDEM with David Faure Intervention"
date:    2001-01-13
authors:
  - "raphinou"
slug:    osdem-david-faure-intervention
comments:
  - subject: "Re: OSDEM with David Faure Intervention"
    date: 2001-01-12
    body: "Shouldn't that say \"...with David Faure <b>Presentation</b>\"?"
    author: "Gash T."
  - subject: "Re: OSDEM with David Faure Intervention"
    date: 2001-01-12
    body: "But \"intervention\" sounds wayyyy more c00ler!  ;)"
    author: "ac"
  - subject: "Re: OSDEM with David Faure Intervention"
    date: 2001-01-12
    body: "David, do you know if the presentation will take place saturday or sunday? I'm sure to attend saturday, but actually not sure for sunday, and KDE presentation was one of my best interest. \n\n Regards"
    author: "Nicolas"
  - subject: "Re: OSDEM with David Faure Intervention"
    date: 2001-01-13
    body: "Hi, the program isn't fixed yet (will be posted on osdem.org next week), but the KDE presentation was supposed to take place on sunday :("
    author: "raphinou"
  - subject: "Re: OSDEM with David Faure Intervention"
    date: 2001-01-14
    body: "Is this the same Phil Thomson who does PyQt/PyKDE?"
    author: "ac"
  - subject: "Re: OSDEM with David Faure Intervention"
    date: 2001-01-15
    body: "Yes but sadly Phil is going to have to back out.  He has just been too busy getting BlackAdder out to get a proper presentation put together.  Our humble apologies for the inconvenience to everyone."
    author: "Shawn Gordon"
  - subject: "Re: OSDEM with David Faure Intervention"
    date: 2001-01-17
    body: "Now this is something good :) Getting as much attention as possible is a good way to reach 'the lamers'. Allso.. OS has become very mature in the last few years, so now is a good time to start yelling: \"HEY!!... We are actually the very best programmers on this rock we call Earth!\"<br><br>/kidcat"
    author: "kidcat"
---
We are proud to <a href="http://www.osdem.org/comm.html">announce</a> the first <a href="http://www.osdem.org/">Open Source and Free Developers' European Meeting</a> featuring Fyodor, Jeremy Allison, Richard Stallman, Kevin Lawton, Phil Thomson, Rasmus Lerdorf and more. <i>David Faure</i> will be offering a KDE presentation.
<!--break-->
<p>
The meeting will take place in Brussels Saturday the 3rd and Sunday the 4th February 2001, and is organised in collaboration with <a href="http://www.valinux/">VA Linux</a> which is actively sponsoring the event.
<p>
The goal is to bring developers together so they can share ideas and learn about other projects. We hope this event will foster collaboration between different projects and perhaps give birth to new ones.
<p>
The website is available at <a href="http://www.osdem.org">www.osdem.org</a>, the initial announcement is also  <a href="http://www.osdem.org/comm.html">available</a> . A <a href="http://www.raphinou.com/cgi-bin/mailman/listinfo/osdem/">mailing list</a> has been set up, and it is possible to register so we that can evaluate the number of attendants (and print enough t-shirts :-)
<p>
We hope to welcome lots of you in Brussels!
<p>
Thank you.<br>
Raph.