---
title: "KDE Dot News: We've Moved."
date:    2001-06-01
authors:
  - "numanee"
slug:    kde-dot-news-weve-moved
comments:
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "Wonderful.\n\nThanks Navindra, thanks Bero."
    author: "Rob Kaper"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "Things are not all well yet though.  Your news.kde.org URL is not working (try it).  Only the dot.kde.org version of the URL is working :o("
    author: "David Walser"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "Fixed thanks.  Please report any more bugs like this."
    author: "Navindra Umanee"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "Eeek. A redhatter helping out a kde project! The world has ended! (hehe) Seriously, it's great to see bero doing this for us. This is greatly appreciated.\n\nKudos to you both."
    author: "Anonymous"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "I've been waiting for a proper moment to say my thanks to Bero. I guess that's it.\n\nYou've worked very hard on KDE, and I've seen you win and sometimes lose, but KDE support with RedHat has gone better and better. Maybe, just maybe there are some other issues involved, too, but I'm convinced that most of the credit goes to you. I was very happy, when installing RH 7.1 I could set KDE as my primary desktop and therefore log in using KDM as desktop manager.\n\nI don't want to list all the things that there is to better KDE support in RH, but I'm sure anyone who has used RedHat for some time knows what I'm talking about.\n\nI've also seen that you have done configuration utilities to KDE and also compiled rpm's of developing versions. Much of it on your own time. That's just very great!\n\nMany thanks to you, Bero! Keep up the good work!\n\nEleknader"
    author: "Eleknader"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "> we have had a lot of downtime lately brought \n> on by some severe server problems.\n\nSomehow this seems to be an understatement. \n\nI try to read www.kde.org and dot.kde.org a couple of times a week, and almost always one of those url's seems to be down. Very annoying since a lot of links on www.kde.org point to dot.kde.org and visa versa.\n\nOfcourse this could all be the fault of my ISP, but they have had enough complaints from yours truely for the next few weeks.\n\nkind regards to those who try to make these sites work,\n\npivo to all who like to drink beer,\n\nJohan V."
    author: "Johan Veenstra"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "I hope this server stays up more than the last one... I need my dot fix! :-)"
    author: "AC"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "Well this is very very good news for kde the old server being down all the time was becoming an embarrassment. Great work getting it moved.\n\nCraig"
    author: "Craig"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "Well this is very very good news for kde the old server being down all the time was becoming an embarrassment. Great work getting it moved.\n\nCraig"
    author: "Craig"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "Thanks Bero!"
    author: "Joergen Ramskov"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "apps.kde.com does not work for me either for some days now\n:-("
    author: "Andreas Zehender"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "Navindra, first of all: thanks for your excellent contribution to KDE. Don't get discouraged by people complaining. It's just that they love the site, like me. What I don't understand, is why you don't get any support from the KDE League. I thought their task is to promote KDE, and this wouldn't even cost lots of money (in fact, I haven't heard anything about them since their foundation)."
    author: "Alexander Kuit"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "Eh, just wanted to say I completely agree. Navindra, keep up the good work ! Thanks !"
    author: "Jelmer Feenstra"
  - subject: "Me too"
    date: 2001-06-01
    body: "I agree on all six points.\n\nNavindra, you're doing a great job on the editorial side. Hopefully the new hosting situation will do the same for availability."
    author: "Otter"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "As a member of the League, I totally agree with you, this has been my pet peeve since the kick off back in November.  I have been insisting that the web sites are the public face of KDE and do more for promotion than almost anything else that can be done, but no one else seems to agree with me."
    author: "Shawn Gordon"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-02
    body: "It's the truth that nobody heard nothing of KDE league since it's creation and that is very sad. It makes people think that KDE league was really founded just because of GNOME foundation and that it has no proper goals. KDE league could do many things, like:\n- take care of dot.kde.org, www.kde.org and similar sites,\n- take care of binary distributions of KDE, KOffice, etc.\n- sell goodies (T-shirts, etc), with KDE symbols,\nto make money and promote KDE.\n- take care of KDE documentation.\n- any other idea?\n\nShawn, is it possible to call some KDE league meeting, or something like that, maybe even on IRC? It is too bad, that an organization that claims that it was founded only to promote KDE, does so little. There is something wrong here. Or KDE League should change its attitude or we shall just forget it ever existed."
    author: "Bojan"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-02
    body: "It's possible that there has been some trouble in collecting dues from everyone which could be hampering the financial side of it.  I know that Trolltech and SuSE were extremely supportive at the formation, but there has been virtually no traffic on that mail list for a long time now.  I don't know what to do, I know Dre works his ass off on this stuff, but sometimes it's like herding cats.\n\nI think the reality is that none of the big companies like HP, IBM and Compaq really care about the details.  The lady that represented HP to the League is also the one that made the deal to bundle Gnome on the HP workstations (go figure).\n\nI might be talking more than I should, but I've found it frustrating."
    author: "Shawn Gordon"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-02
    body: "I think that many people find this frustrating and as a president of theKompany, who gained so much admiration (and rightfully so) among the KDE community you have all the rights to say it.\n\nI have read the bylaws at www.kdeleague.org. It says something about regular meetings, special meetings...\nWas there any meeting after the foundation? What did members discuss there? There should be an annual meeting... Who is the Chairperson of the Board of Directors? I mean, there is so much things written in the Bylaws, that I never read of anywhere else(dot.kde.org or linuxtoday.com or similar linux or kde related news). I think it's worse to have an organization that does nothing, than to have no organization at all."
    author: "Bojan"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-02
    body: "Not being on the executive committe, I don't really want to speak out of turn.  I think there were a couple of changes from the initial meeting but Dre has been acting as treasurer and taking care of all the business matters.  It's probably more appropriate for him to talk about it.  I think it would make a good dot story myself."
    author: "Shawn Gordon"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "Thank you Nav, thank you Bero, thank you Red Hat!\n\nImagine that, Red Hat helping out and they are not even in the KDE League."
    author: "reihal"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "See how our complaining proved useful. ;-))\nSeriously, this is great news, thanks to all of you who made this happen."
    author: "Bojan"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-01
    body: "When Ximian helps GNOME, people troll about GNOME being controlled by Ximian.\nNow when RedHat helps KDE, nobody trolls about KDE being controlled by RedHat.\nOr by TrollTech, or by TheKompany, or by Mandrake.\nWhile everybody trolls about GNOME..."
    author: "ac"
  - subject: "Thank you BERO and REDHAT :)"
    date: 2001-06-02
    body: "Bero Thanks a million for your work; I like your Configuration Tools, I would love them if you just polish their interface a bit! \n\nThank you RedHat for Not Messing Up, Default KDE Icons, Menus and Configuration. Please for goodness sake Join the KDE League (My personal request), so that KDE Users and Developers trust RedHat."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Thank you BERO and REDHAT :)"
    date: 2001-06-02
    body: "Joining or not joining is pretty pointless IMO..it's a matter of effectively contributing..all the rest (announcement of massive involvements of big companies in a project) are just void if in the end there's no effective work/code being done.\nTake HP/IBM/Compaq supporting GNOME..haven't seen a single mail from them on any GNOME ML. (Sun is the exception)...so trust the code, the commits, the sponsoring..but don't care about announcements.\n\ncheers"
    author: "Mik"
  - subject: "Re: KDE Dot News: We've Moved."
    date: 2001-06-02
    body: "isn't it great to fire up konqui and get the dot (even that apps.kde.com is down, miss that onw two.\n\ngreat work boys -- wouldn't have been so fast decided in any company environment (cause: politics :-)"
    author: "maxwest"
---
As many of you have noticed, we have had a lot of downtime lately brought on by some severe server problems.  With our regular server admin away on a well-deserved vacation, we have been forced to move to a new location, at least for now.  The new server, <a href="http://www.bero.org/">bero.org</a>, is owned and operated privately by our friend <a href="mailto:bero@redhat.com">Bernhard Rosenkraenzer</a>, but is hosted on the excellent network and resources of <A href="http://europe.redhat.com/">Red Hat Europe</a>.  I would like to extend a huge thanks both to Bero and to Red Hat for their support!
<!--break-->
