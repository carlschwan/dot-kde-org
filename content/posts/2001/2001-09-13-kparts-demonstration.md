---
title: "KParts Demonstration"
date:    2001-09-13
authors:
  - "pfremy"
slug:    kparts-demonstration
comments:
  - subject: "Cool stuff + some comments"
    date: 2001-09-13
    body: "Nice to see more resources about KParts coming up, this is cool.\nThanks for your continued work on promoting KDE technologies!\n\nHehe, I like the line count argument for koshell. What isn't known is that I had to add special hooks/code for KOShell in kofficecore ;-)\n\nAnother comment: assert()s are very dangerous, don't use them. Especially in such code, where nothing guarantees that the component will exist, will be openable, and will contain what will expect. Always prefer something like \"if (!condition) return 0\". Not longer, and avoids giving crashes to the unexpecting user ;)"
    author: "David Faure"
  - subject: "Qt3 Component Role"
    date: 2001-09-14
    body: "So where will Qt3's component architecture fit into KDE3's KPart framework? Will there be a bridge to connect the two, so for instance, one could use the Opera render in Konqi or embed a HancomSheet document in KWord? I know it may be early in the game to ask this, but I am quite interested in the possibilites....\n\nThanks,\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Qt3 Component Role"
    date: 2001-09-14
    body: "All good questions, but at the moment things like this are still undecided. There's a discussion about the Qt interface stuff discussion currently underway on the devel lists.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Qt3 Component Role"
    date: 2001-09-14
    body: "the QtCom stuff doesn't handle UI issues for a KPart/OLE interface for document embedding.  I don't see that happening for a while, but it will probably happen."
    author: "Shawn Gordon"
  - subject: "Re: Qt3 Component Role"
    date: 2001-09-15
    body: "Hello, Shawn! Speaking of frameworks, how is Korelib coming along? Are you still actively developing it? It seems like a nice lightweight component library for cross-platform stuff. I wish you guys would publish more technical papers about all the wonderful stuff you engineer... anyone know of a good technical writer these guys could hire ;-) ?\n\nRegards,\n\nEron"
    author: "Eron Lloyd"
  - subject: "which is better?"
    date: 2001-09-14
    body: "Is qt3's solution or kparts better? kparts seems very elegant but I'd think that it'd be better if it could be done within qt with as few dependencies as possible. It'd certainly make KDE more portable. \n\nDoes qt3's solution have something like dcop?"
    author: "foaog"
  - subject: "Hmm.. how does this connect to this article?"
    date: 2001-09-14
    body: "I'm referring to this article, which came op on /. just today:\n\nhttp://www.zdnet.com/zdnn/stories/comment/0,5859,2811809,00.html\n\nCould KParts/KOffice be a first step in this direction?\n\nShould we make KOffice even more \"Kparts\" (ie: split them up in more components, like KSpellcheck, etc.)?\n\nI'm not a programmer (yet!), but it seems like a very likable idea. You could even download (life) new components you might need, and you would deliver a very flexible solution.\n\nMaybe you could take it even further, and make all of K(DE/Office) write into one XML database (internet-based, of course!). That way you would have .Net (ie: write together into one database on the net with your co-workers), and a very flexible unix-tools-Office-like conglomerate of small tools you could use to edit the data in your central database of \"XML\" documents. Of course, the data would start by describing witch component should be used to edit/view/print etc it...\n\nKParts is, as I understand is, ideal for this kind of solution. (I dont know enough to say for sure if it would be possible to take it to the central XML-database idea...)\n\nWell... just dreaming away :-))"
    author: "Paul Wiegers"
  - subject: "Re: Hmm.. how does this connect to this article?"
    date: 2001-09-14
    body: "What you say is more or less the way KDE 2 works :-)\nWe have konsole and the embedded konsole -> the same binary (lib)\nWe have kate, kwrite, the embedded text viewer in konqy, and AFAIK the upcoming kdevelop will also use the kwrite part -> the same binary (lib)\nYou can download and install additional ioslaves to access completely new ressources and so on.\nI'm not sure right now, but I think even your example (KSpellCheck) already exists :-)\nYou can view kword texts inside konqy using the same binary as when writing kword documents.\nAnd you can download new parts and install them on the fly, but currently the most parts are in the base packages, so you don't notice it that much.\nBut this downloading and installing ain't that simple in binary form due to different lib dependencies, compiler versions, cpu's, paths and so on.\nAlso the author makes it appear a little bit to easy: \"use off-the-shelf algorithms\". These algorithms have to be glued together, they have to understand each other, they need to have a common GUI, they need to be implemented bug free. Although \"text-book\", it ain't that simple.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Mentioning kate"
    date: 2001-09-14
    body: "Mentioning kate: I had heard that kate should be the new advanced editor in KDE, replacing kwrite. As my current distribution, containing KDE 2.1.2, still starts kwrite as the advanced editor, I just wonder how the situation really is. Is kate part of the core KDE package?\n\nAlex"
    author: "Alexander Kuit"
  - subject: "Re: Mentioning kate"
    date: 2001-09-14
    body: "Yes, kdebase KDE 2.2.\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Hmm.. how does this connect to this article?"
    date: 2001-09-14
    body: "very beautiful dream indeed, multiple small components, called only when needed. But, sadly, I can't dream on using .Net-like technology. Connection cost is very high here (I can get full lunch with < US$ 0.5, here)\n\nAnyway, document centric and component approach is so nice."
    author: "Reza"
  - subject: "Re: Connection cost is very high here..."
    date: 2001-09-16
    body: "The beauty of open source .net-like technology is that everyone runs their own \"centralized database\" at home. That way\n\n- _You_ possess your data, not micro****.\n- When you travel, you can access your computer transparently, as if your home machine were there with you."
    author: "Daver"
  - subject: "KNOME vs GDE"
    date: 2001-09-14
    body: "Why mention GNOME in the article? What does it have to do with KParts?\n\nAnd if CORBA is so slow, then how come GNOME apps using ORBit aren't slow?\nHow come Bonobo is so fast?\n(hint: perhaps it's just that your CORBA implementation (MICO) is slow?)"
    author: "KNOME vs GDE"
  - subject: "Re: KNOME vs GDE"
    date: 2001-09-14
    body: "CORBA is for distributed systems, a desktop ain't distributed. The developers had all the problems of working with distributed systems without actually working on a distributed system. CORBA ain't easy to learn (I know from experience), it requires actually an almost entire new way of programming. CORBA has nothing to do with embedding GUI stuff, but it was used in relation to this. And CORBA (MICO) apps takes ages to compile and don't run that fast. The actual CORBA was replaced by DCOP which does now the IPC and is very lightweight, both in terms of speed and memory as in terms of learning how to use it.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: KNOME vs GDE"
    date: 2001-09-14
    body: "> CORBA ain't easy to learn (I know from experience), it\n> requires actually an almost entire new way of programming.\n\nThen how come so many GNOME developers learned it in such a short time?\n\n\n> And CORBA (MICO) apps takes ages to compile and don't run that fast.\n\nThat only proofs that MICO is slow. Try compile an ORBit app.\n\nAnd if CORBA is slow, how come Berlin, a new windowing system, use CORBA for IPC?"
    author: "KNOME vs GDE"
  - subject: "Re: KNOME vs GDE"
    date: 2001-09-14
    body: "> > CORBA ain't easy to learn (I know from experience), it\n> > requires actually an almost entire new way of programming.\n>\n> Then how come so many GNOME developers learned it in such a short time?\n\nDid you try to use CORBA ?\nThen you wouldn't ask, I'm sure. I don't think (but I don't know) that there are so many Gnome developers *really* understanding the CORBA stuff.\nI don't say they are stupid, I only say what I expect from my own experience with CORBA, and I don't consider myself especially stupid (except sometimes ;-)\n\n> > And CORBA (MICO) apps takes ages to compile and don't run that fast.\n>\n> That only proofs that MICO is slow. Try compile an ORBit app.\n \nYes, I didn't say more. \n\n> And if CORBA is slow, how come Berlin, a new windowing system, use CORBA for > > IPC?\n\nDid you ever see Berlin in action ?\nI did, it ran on a fast machine (PIII 800 or something like this) and it wasn't fast, it was very capable, but not even on this machine usable fast. \nProbably the reason is not CORBA but the fancy graphics stuff, but at least this is no \"look at Berlin to see that CORBA is fast\" proove.\nAnd of course Berlin needs some networking infrastructure to provide network transparency like nowadays X11 does. KDE doesn't need a network to embed kwrite into konqy.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: KNOME vs GDE"
    date: 2001-09-14
    body: "If you look at the Berlin FAQ, they do admit that CORBA is a lot slower than, for example, KParts. There approach is to use a very high-level API to minimise the communications between the client and the server (e.g. where X would give 20 odd commands to draw a button, Berlin would just give a single \"Draw Button\" command). They use CORBA to give them the network transparency of X. It's an interesting project, but frankly I think the Unix world really needs an X12 specification to get rid of all the dross that the great people in XFree86 are lumped with."
    author: "Bryan Feeney"
  - subject: "Re: KNOME vs GDE"
    date: 2001-09-15
    body: "> Did you ever see Berlin in action ?\n> I did, it ran on a fast machine (PIII 800 or something\n> like this) and it wasn't fast, it was very capable, but\n> not even on this machine usable fast.\n> Probably the reason is not CORBA but the fancy graphics\n> stuff, but at least this is no \"look at Berlin to see that\n> CORBA is fast\" proove.\n\nAccording to all those anti-X11 trolls at Slashdot, X is bloated and must die and Berlin is light much faster than X..."
    author: "KNOME vs GDE"
  - subject: "Re: KNOME vs GDE"
    date: 2001-09-14
    body: "I can confirm that CORBA gives programmers a hard time. The inofficial CORBA bible for C++ programmers has more than 1000 pages and it doesn't even cover every aspect. To really understand CORBA, you have to have a lot of time. Most developers rather spend this time on functionality and improvements. CORBA should be used where it is really useful: in highly distributed systems."
    author: "Alexander Kuit"
  - subject: "Re: KNOME vs GDE"
    date: 2001-09-14
    body: "In my experience of using CORBA I would rather say it simplifies things for programmers, but as with all techniques you have to learn how to use it first(which is non-trivial unfortunatly.) \n\nI have tried multiple ORB-implementations with different levels of success. But \nto say that the CORBA-technique is to blame is probably not correct.\n\nHave a look at TAO which includes a real-time orb.\nhttp://www.cs.wustl.edu/~schmidt/TAO.html\nOne problem with CORBA applications that has been brought up is that they take long time to compile, with this I have too agree. But on the other hand this\nis only a problem during the development phase."
    author: "Jonas Nordin"
  - subject: "Re: KNOME vs GDE"
    date: 2001-09-15
    body: ">But on the other hand this is only a problem during the development phase.\n\nIn case you hadn't noticed, KDE is constantly in a development phase...\n\nI think long compile times are a big downside.  Even if the end-user doesn't notice them directly, he/she might notice that development got slower."
    author: "not me"
  - subject: "Re: KNOME vs GDE"
    date: 2001-09-15
    body: ">In case you hadn't noticed, KDE is constantly in a development phase...\n\nYep, that's why it's moving forward so fast! :-)"
    author: "Carbon"
  - subject: "Re: KNOME vs GDE"
    date: 2001-09-18
    body: "> Then how come so many GNOME developers learned it in such a short time?\n\nDid they ?\n\nhttp://www.advogato.org/person/trow/diary.html?start=15\n\nNote: the man works for Ximian."
    author: "Guillaume Laurent"
  - subject: "Re: KNOME vs GDE"
    date: 2001-09-14
    body: "Hi Alexxx :)\n\nCORBA is great, this type of technology might be the future of computing.\nOr at least in certain areas of computing. Distributed Computing, Grid computing ...., and associated technologies (CORBA, Globus, .NET, MONO...)\nare a very active trends these days. Why? because they address lots of current issues. I am more the Grid Type of person (scientific computing) and I can tell you that this type of technology enabling distributed computing ---- but at the same time hiding the nasty details --- is important ,really important. \nIt raises the problems of \"hiding the details\" which is by no means easy: Analysis, Design are tremendously important there, you need to sit down and think about things in a different way than before. Speed is certainly an issue and GUI are certainly very demanding from this point of view. My view is that now it is difficult to create a good distributed framework due to hardware(computer and network) constraints and also software (slow implementation of protocols) constraints.\n\nHowever if the project fits in (little data to manipulate through the distributed computing  system) then you have a winner !\nYes CORBA is a \"difficult\" technology, partly because it needs good implementation, but also because it needs the programmer to rethink his Design\nand the way is developping things.\n\nMICO is not \"bad\", I use it all the time. It is very complete and that's what I like. I think (correct me if I am wrong) that it is the fullest CORBA/CORBA Services implementation available. \n\nFor my project, MICO is doing VERY well because the design and application fits perfectly in the big picture. Now MICO is going also multithread(see <a href=\"http://mico-mt.sourceforge.net\">http://mico-mt.sourceforge.net</a>) and I am sure that with this more and more people will use it.\n\nNow GUI are another concern and it seems that lightweight ORB are working (ssee GNOME/Bomobo technology). I don't think that Kde or Gnome are right or wrong here: I think that if they are doing their DESIGN well then having distributed components aren't or won't be a problem at all. I am convinced that Design matters a lot."
    author: "Jesunix"
  - subject: "find the mistake ;-)"
    date: 2001-09-14
    body: "Cite: \"they have gone through three stable release (KDE 2.0 and KDE 2.1) with almost...\"\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Bonobo != CORBA"
    date: 2001-09-14
    body: "[I must admit that I can only judge KParts by this article]\n \n From the article:\n \"In Gnome, the component technology is provided by Bonobo, which is\n built upon Corba.\"\n \n and\n \n \"In short, a good component technology has the following\n characteristics:\n \n     * it is easy to enable an application as a component\n     * it is easy to use a component in another application\n     * the component is activated quickly (almost instantaneously).\n \n Corba meets none of these requirements, while KParts meets all of them.\n Corba is a very good technology but it is definitely not suited for gui\n components. Read what follows to see how KPart is a success in this\n area.\"\n \n The author misrepresenting Bonobo here. [classical usenet style\n reasoning: A uses B, B is difficult, therefore A is difficult]\n \n While Bonobo is based upon CORBA, it provides default implementations\n for most stuff, so most of the time, you don't have to implement and\n interfaces at all. My example below shows that. However, you can if you\n need to.\n \n KPart seems like a glorified shared library loader; there's nothing\n wrong with that, in fact, it's a lot easier to implement, and has some\n performance advantages (but you can optimize shared library\n CORBA-servers to approach virtual function call speeds - in fact works\n being done in GNOME's ORB (ORBit) to achieve this).\n \n On the other hand, Bonobo's CORBA-base allows for flexibility which is\n not possible with KPart (as far as I can see):\n \n 1) out of process components \n 2) components on other machines (may not be suitable for GUI-components,\n but perfectly reasonable for non-GUI-components) \n \n note that 1) and 2) can be done transparently -- client doesn't have to\n know where components live.\n \n 3) interaction with other languages (e.g. you can use Bonobo from C++,\n Perl, Python, Guile,...). It would be hard to write a KParts-component\n in Perl, I guess...\n \n 4) Allows for independent implementations (e.g. there's a Java-based \n implementation of Bonobo, 'MonkeyBeans').\n \n\n For more info on Bonobo, please take a look at my Bonobo Controls tutorial:\n http://www.djcbsoftware.nl/projecten/bonobo_controls/\n \n It's extremely simple to create a Bonobo component (control) from a\n regular gtk-widget, in fact I show this in the tutorial (ipentry is a\n gtkwidget I've written as well:\n \n BonoboControl* control;\n GtkWidget*     ipentry;\n \n  ipentry = gtk_ip_entry_new ();\n  gtk_widget_show (ipentry);\n \n  /* create a BonoboControl from a widget */\n control = bonobo_control_new (ipentry);\n \nand on the container side we can start it with:\n \n  /* get a widget, containing the control */\n control = bonobo_widget_new_control(IPCTRL_OAFIID, \n \t\t\t\tbonobo_object_corba_objref(BONOBO_OBJECT(uic)));\n\n(IPCTRL_OAFIID contains the name  the component)\n\nnot too hard, right?\n\n--Dirk-Jan."
    author: "djcb"
  - subject: "Re: Bonobo != CORBA"
    date: 2001-09-14
    body: "Hi Dirk Jan, I have read your tutorial (very well written!) about Bonobo so I am not speaking ignorantely.\n\nI plan a more detailed comparison of Bonobo and KPart in another article (when I'll have time!) but I can at least say a few things:\n\n> On the other hand, Bonobo's CORBA-base allows for flexibility which is\n> not possible with KPart (as far as I can see):\n> 1) out of process components\n> 2) components on other machines (may not be suitable for GUI-components,\n> but perfectly reasonable for non-GUI-components)\n> 3) interaction with other languages (e.g. you can use Bonobo from C++,\n> Perl, Python, Guile,...). It would be hard to write a KParts-component\n> in Perl, I guess...\n> 4) Allows for independent implementations (e.g. there's a Java-based\n> implementation of Bonobo, 'MonkeyBeans').\n\nExcept for out-of-process components that can sometimes be very handy, the stuff you described will actually almost never be used.\n\nI would say that 90% of Gnome is written in C but it is probably more than that.  Remote components are a cool hack but even you are doubtful about the usefullness of this (quoting your tutorial \"there must be someone somewhere who needs this\").\n\nKde has chosen the technology that solved the problem: \"I want an application as component\". Gnome has chosen the technology that is theorically the top and can do a lot. But in practice 95% of the users won't use the things that made you choose Corba over Shared Libraries. You are forcing them to use a more complicated technology because you want the remaining theorical 5% to be able to use remote embedding.\n\nThe comparison that comes to my mind is this famous guy who designed an operating system that would not be unix although it would resemble it a lot. It would be cool, have micro-kernel, use message passing and provide more Freedom for the user. 10 years later, this OS is still struggling to get support and applications. Besides, there is this cool finnish student who wrote a cool hack on x86 that would solve exactely his problem. This cool hack is no more a cool hack but a widely used OS. The fact that it was originally specifically and only designed for x86 doesn't stop it to run on many architecture now.\n\nCorba vs Shared Libraries is exactely this: theorical good choice vs practical one.\n\nYou are right though that KPart doesn't provide the flexibility of Bonobo. But there is XPart () which provides exactely what you want. It uses X to embed out- of-process applications. So if you have a remote X server with applications written in a different language, on a different workstation/OS, you can embed it with XPart. Still no need for Corba.\n\nXPart is sufficentely generic to make a Bonobo-XPart bridge, or to be used outside KDE (it depends only on X and Qt).\n\nKDE made the good choice: shared libraries and ease of use for direct components. More exotic technology for more exotic needs.\n\nNote that nobody has used XPart yet, nor requested for remote components. IMHO, this is because KPart solves 99% of the needs for components and nobody needs remote components. I will use XPart for kvim because of its out-of-process property.\n\nI have absolutely nothing against Gnome. I simply think the Gnome project has made some wrong technical choices.\n\n> It's extremely simple to create a Bonobo component (control) from a\n> regular gtk-widget, in fact I show this in the tutorial (ipentry is a\n> gtkwidget I've written as well:\nIt is true that creating a bonobo component is as simple as creating a KPart.\nBut what the bonobo tutorial also told me is that, in comparison to KPart:\n- it didn't compile on my Mandrake :-)\n- your component doesn't add any menu entries and there is no dynamic activation, it is just a static widget\n- communicating with the component is painful. You must encapsulate your data in the property bag\n- using signal/slots is painful\n- you don't have remote scripting\n\nMoreover, I find the component accessing and embedding more difficult. The problem is that for every interaction with the component, you must go through Corba. You don't have this problem with in-process components."
    author: "Philippe Fremy"
  - subject: "Re: Bonobo != CORBA"
    date: 2001-09-14
    body: "Hi Philippe,\n\n> Hi Dirk Jan, I have read your tutorial (very well written!) about\n> Bonobo so I am not speaking ignorantely.\n\nOk. I'm equally well-educated in KParts, thanks to you article ;-)\n\n> I plan a more detailed comparison of Bonobo and KPart in another\n> article (when I'll have time!) but I can at least say a few things:\n\nThat would be interesting.\n \n> > On the other hand, Bonobo's CORBA-base allows for flexibility which\n> > is not possible with KPart (as far as I can see):\n> > 1) out of process components\n> > 2) components on other machines (may not be suitable for\n> >    GUI-components,\n> > but perfectly reasonable for non-GUI-components)\n> > 3) interaction with other languages (e.g. you can use Bonobo from\n> >    C++,\n> > Perl, Python, Guile,...). It would be hard to write a\n> > KParts-component in Perl, I guess...\n> > 4) Allows for independent implementations (e.g. there's a Java-based\n> > implementation of Bonobo, 'MonkeyBeans').\n> \n> Except for out-of-process components that can sometimes be very handy,\n> the stuff you described will actually almost never be used.\n> \n> I would say that 90% of Gnome is written in C but it is probably more\n> than that. Remote components are a cool hack but even you are doubtful\n> about the usefullness of this (quoting your tutorial \"there must be\n> someone somewhere who needs this\").\n\nSure, GNOME's mostly written in C, but that doesn't really say much\nabout third-party use. I don't have any numbers here, but I've seen\nquite some use of the Gnome / Python combination, which is very nice,\nfor example for database frontends. So, in the GNOME-world, non-C/C++\nlanguages are more important, and therefore should have Bonobo\nsupport.\n\n> Kde has chosen the technology that solved the problem: \"I want an\n> application as component\". Gnome has chosen the technology that is\n> theorically the top and can do a lot. But in practice 95% of the users\n> won't use the things that made you choose Corba over Shared\n> Libraries. You are forcing them to use a more complicated technology\n> because you want the remaining theorical 5% to be able to use remote\n> embedding.\n\nWell, the funny thing is that with Bonobo you don't need to deal with\nCORBA complexity if you don't want to, but it's there if you need it. \n\n> Corba vs Shared Libraries is exactely this: theorical good choice vs\n> practical one.\n\nHmmm.... a CORBA server can be a shared library, and it is possible to\ndo this without too much overhead (just a bit more than a c++ virtual\nfunction call, but not too much).\n\nWriting CORBA-code (esp. servers) in C is a bit inconvenient, but the\nbeauty of Bonobo is that you don't have to. Note that there's also a\nvery nice CORBA binding for Python, which makes writing CORBA servers\n/ clients as easy as writing an ActiveX client or server in Visual\nBasic.\n \n<snip: XParts>\n\nXParts sounds like reinventing CORBA, without the benefits of using\nwidely-supported standard.\n \n> XPart is sufficentely generic to make a Bonobo-XPart bridge, or to be\n> used outside KDE (it depends only on X and Qt).\n\nThe core Bonobo (cvs-versions) doesn't depend on either X or GTK+. But\na bridge with KParts could be an interesting exercise.\n\n> KDE made the good choice: shared libraries and ease of use for direct\n> components. More exotic technology for more exotic needs.\n> \n> Note that nobody has used XPart yet, nor requested for remote\n> components. IMHO, this is because KPart solves 99% of the needs for\n> components and nobody needs remote components. I will use XPart for\n> kvim because of its out-of-process property.\n\nGNOME uses out-of-proc servers a lot, as well as inproc servers. You\ncould even choose at runtime if you want the inproc version or the one\nrunning on the machine down the hall. Note that for example the Sun\npeople (which are actively contributing to GNOME) support Bonobo\nespecially because it uses CORBA, which goes nicely with their\nexisting infrastructure.\n\n> I have absolutely nothing against Gnome. I simply think the Gnome\n> project has made some wrong technical choices.\n\nGnome made some different choices.\n\n> It is true that creating a bonobo component is as simple as creating a\n> KPart.  But what the bonobo tutorial also told me is that, in\n> comparison to KPart:\n> - it didn't compile on my Mandrake :-)\n\nI didn't receive your bugreport. Please send me one.\n\n> - your component doesn't add any menu entries and there is no dynamic\n>   activation, it is just a static widget\n\nWhat is 'dynamic invocation'? And you're right I should add some stuff\nabout adding menu entries... it's quite simple!\n\n> - using signal/slots is painful\n\nYou could transport gtk-signals from server to client using a property\nbag. I plan to write some wrapper code to make this (almost) the same\nas using plain gtk-signals.\n\n> - you don't have remote scripting\n\nWhy not? You can use Perl/Python/Guile if you like.\n\n> Moreover, I find the component accessing and embedding more\n> difficult. The problem is that for every interaction with the\n> component, you must go through Corba. You don't have this problem with\n> in-process components.\n\nCORBA servers can be inproc if you like. The way you access them is\nexactly the same no matter if they're inproc or outproc.\n\nAnyway, it seems both technologies (Bonobo and KParts) are doing what\nthey're designed for, and it's only interesting to see what will\nhappen. I hope we will have some interoperatibility!\n\nCheers.\n--Dirk-Jan."
    author: "djcb"
  - subject: "Re: Bonobo != CORBA"
    date: 2001-09-15
    body: "It seems to me that making/using bonobo components are easier seeing that no 80 line could has to be used.  I understand that those 80 are very general and that certain ide will provide them but 80 lines it still 80 lines- the only time that is good is when one is talking about coke."
    author: "bondowine"
  - subject: "Re: Bonobo != CORBA"
    date: 2001-09-14
    body: "2) components on other machines (may not be suitable for GUI-components,\n but perfectly reasonable for non-GUI-components) \n\n    I've always been curious - how do you handle security issues with multi-system components like this?  How can you contain a malicious component which is designed to advertise all services and spread across a server pool or even an entire network?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Bonobo != CORBA"
    date: 2001-09-14
    body: "very good question. \nSecurity issues are very important in distributed computing(like standard computing) but it is more complicated/difficult.\n\nHere are some solution SSL(MICO implements a SSL layer on top of itself), Kerberos, ...\n\nWhat you need is an authentification layer+crypto on top of your framework\nwithout losing flexibility.\n\nAs regards the use of SSL+Mico for example it is very straightforward \nand it works relatively well but it is not perfect.\n\nI guess some new/modified technologies will/are arise(ing) to address this\ntype of issues."
    author: "Jesunix"
  - subject: "Re: Bonobo != CORBA"
    date: 2001-09-14
    body: ": What you need is an authentification layer+crypto on top of your framework\n\n   How does that prevent one compromised host from spreading through an entire network?  And I think that this thread should probably be taken to email, unless any KDEers are interested in theory for potential application to future Qt3/KParts issues.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "troll syndrome"
    date: 2001-09-14
    body: "gnome vs kde?"
    author: "Sotheby"
  - subject: "Re: troll syndrome"
    date: 2001-09-14
    body: "Yes, it's no coincidence that this Gnome/Bonobo-trashing article appears only a week after Gnotices published an introduction to Bonobo."
    author: "Anonymous Flamer"
  - subject: "Re: troll syndrome"
    date: 2001-09-14
    body: "What a conspiracy! And how awfully Gnome-bashing this article is! Evil, evil word!\n\nHey guys, the KDE developpers gave Corba a try and switched to KParts. I'm no developper, so I can't judge this decision, but for me as user there seem to be only advantages of this.\n\nHowever, isn't it obvious that a KDE developper in an article about KParts stresses the disadvantages of Corba *for the specific use within KDE* - BECAUSE this disadvantages were the reasons they dumped Corba and DEVELOPPED KParts?"
    author: "AC"
  - subject: "Re: troll syndrome"
    date: 2001-09-14
    body: "Please... what advantages do kparts give you as a user? The component based approach provides the advantages - not the particular implementation. Have you even tried any bonobo-based applications? Didn't think so. From a user point of view they work exactly the same as kparts-based ones - the only difference being look'n feel due to differences in widget sets.\n\nThe article is just plain inflammatory - instead of pointing out the merits of kparts it starts out by bashing Bonobo, making several inaccurate statements about the Bonobo technology. It then concludes that Gnome is far behind KDE. WTF? This is supposed to be an article on \"KDE components\", not \"KDE vs Gnome\".\n\nI'm left with the impression that the author has a small penis."
    author: "Anonymous Flamer"
  - subject: "Re: troll syndrome"
    date: 2001-09-14
    body: "The last sentence disqualifies youself, but nevertheless:\n\nThe first mentioning of KDE/Gnome/Corba I find in this article goes like this:\n\n\"In Gnome, the component technology is provided by Bonobo, which is built upon Corba. KDE also used Corba in the past, but eventually dumped it for an home-made technology: KPart. This choice has been very criticized although almost nobody understood the ground and the consequences of it. It was a good choice and I'll write an article one day, to explain why.\"\n\nNow... what's your problem with this sentence? The author states facts, he doesn't even state that Gnome is worse off with Bonobo, he just says that Corba was bad for KDE's needs and that it was good (for KDE) to replace it with KParts. Some sentences later he even states that \"Corba is a very good technology\" - just not fit for KDE's needs. He just discusses the technology and why the KDE developpers chose to develop a new approach instead of using the existing Corba technology.\n\nConsidering Gnome being behind KDE in terms of component stuff - Hey, open your eyes, it is! Bonobo is existing, but it isn't really an integral part of Gnome as a whole yet. Yes, I give Gnome a try once in a while, but as of 1.4 it's not yet as integrated as KDE. Maybe 2.0 will change this. But 2.0 isn't released yet.\n\nDiscussing technical merits and pointing out the advantages of the technology KDE has chosen is definitely not inflammatory. I'd like to know how much of the article you did indeed understand or even read besides the few lines where the auther mentions Gnome."
    author: "AC"
  - subject: "Re: troll syndrome"
    date: 2001-09-16
    body: "The first sentence disqualifies youself, but nevertheless:\n\nJust because CORBA isn't a good solution for KDE, the author of that article talk about the disadvantages of CORBA in the beginning of the article.\nHowever, nobody cares about that. People will just use the current solution because it works.\nWhat reason is there then to talk about why CORBA is bad?\nThere is none. So I can only conclude that the author writes that to bash GNOME technologies.\n\nThe author claims that CORBA is slow. And he mentions Bonobo.However, the author didn't say that they used MICO, one of the slowest CORBA implementations in the world.\nORBit happens to be the fastest CORBA implementation, but the author didn't mentions anything about that.\nThis will cause people to think that all CORBA implementations are slow.\nThey will think that GNOME and Bonobo are slow, since they use CORBA.\nThose people will tell other people that GNOME and Bonobo are slow, thus spreading the virus.\nThose people will blindly believe what they've been told, and thus not even bother to try out GNOME."
    author: "Morgoth"
  - subject: "Re: troll syndrome"
    date: 2001-09-17
    body: "I guess you're happy with your conspiracy theories, so I won't bother to reply too long. All KDE users and developers are bad and always trying to bash Gnome. Is it that what you wanted to hear?"
    author: "AC"
  - subject: "Re: troll syndrome"
    date: 2001-09-19
    body: "That article does not infect people with some magical virus that causes them to hate GNOME. No-one who understands OSS at all is stupid enough to use a single, semi-related sentance from an article that has nothing to do with GNOME as the basis for their entire KDE/GNOME decision. \n\nThe author does not bash GNOME technologies, he just says that KDE uses DCOP, not Corba, and explains why that decision was made by the KDE devleopers."
    author: "Carbon"
  - subject: "Re: troll syndrome"
    date: 2001-09-15
    body: "You mean the KDE developers gave MICO a try. ORBit is really *much* faster than MICO."
    author: "KNOME vs GDE"
  - subject: "Re: troll syndrome"
    date: 2001-09-17
    body: "The real question isn't really performances but ease of use."
    author: "Guillaume Laurent"
  - subject: "Re: troll syndrome"
    date: 2001-09-24
    body: "When KDE was developing its component technology (more than 2 years ago), they were using Mico. They had optimised it and reduced its size to get better performance but it wasn't enough and a lot of problems were not due to mico itself.\n\nAnyway, they couldn't have used Orbit, because at that time, Orbit was almost nothing. Even the C bindings were not stable. The C++ were ... planned! And KDE was needing components now.\n\nSo they moved to KParts and Shared Libraries. IMHO, this one of the best choices of KDE, after using Qt. KParts was developed very quickly and was very stable, as opposed to Orbit which took a long time to get developed. This has certainly had a negative impact on the development speed of Gnome component's technology."
    author: "Philippe Fremy"
  - subject: "KParts"
    date: 2001-09-14
    body: "Yes, good idea but I had problems with implementation of XML builders. Editing toolbars screw my 'Settings' menu and makes 'Help' menu disappear.  I could not figure out anything  from documentation - it consist of a few comments ( or I'm spoiled by detailed Qt docs ), so I had to copy a code from other \"official\" apps. It is just wonderful simple idea, but I'm lost in all those KXML.. classes and a few dozens methods. Implementation looks definitively too complicated."
    author: "Krame"
  - subject: "Re: KParts"
    date: 2001-09-17
    body: "Some time ago I improved the examples in the docu for kedittoolbar. Did you read this ?\nhttp://developer.kde.org/documentation/library/2.2-api/classref/kdeui/KEditToolbar.html"
    author: "David Faure"
  - subject: "Re: KParts"
    date: 2001-09-17
    body: "No, I didn't"
    author: "Krame"
---
I have written a small article that demonstrates the use of KParts. You can find the article <a href="http://klotski.berlios.de/kde/kpart-techno/kpart-techno.html">here</a>. The tutorial demonstrates the ease with which KParts can be embedded in applications, and discusses their use in KOffice. This article should also be a great way for developers to get up to speed with this powerful KDE technology.
<!--break-->
