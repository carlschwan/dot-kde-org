---
title: "New Development Mailing List for KDE Games"
date:    2001-03-14
authors:
  - "Inorog"
slug:    new-development-mailing-list-kde-games
comments:
  - subject: "Any \"General\" Linux Game Dev Lists?"
    date: 2001-03-13
    body: "Does anyone know of a mailing list that focuses on Linux games development without focusing on a particular enviroment or technology?\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: Any \"General\" Linux Game Dev Lists?"
    date: 2001-03-13
    body: "Try the LGDC at <b>http://sunsite.dk/lgdc/</b>\n\nThey have tons of information and mailinglists and should be exactly what you need."
    author: "Rob Kaper"
  - subject: "Re: Any \"General\" Linux Game Dev Lists?"
    date: 2001-03-14
    body: "Rather than linux specific you should look at anything OpenGL with GLUT."
    author: "robert"
---
<a href="mailto:martin@heni-online.de">Martin Heni</a>, the reigning master of <a href="http://games.kde.org/">games.kde.org</a>, sent this note for our readers interested in game development: <EM>"<a href="mailto:dr_maux@maux.de">Josef Spillner</a> has arranged the opening of a 
new KDE mailing list called <a href="http://master.kde.org/pipermail/kde-games-devel/">kde-games-devel@kde.org</a>
which has its mailman webpage
<a href="http://master.kde.org/mailman/listinfo/kde-games-devel">here</a>. It is intended for all discussions related to games development
under KDE/Qt."</EM>
They are looking for ideas and developers regarding current as well as new KDE games.  Artists are also needed. Further information is available at <a href="http://games.kde.org/">games.kde.org</a>.




<!--break-->
