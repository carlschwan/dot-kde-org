---
title: "Good news for KWord: Qt3 widget ported to Qt2"
date:    2001-02-13
authors:
  - "jloisel"
slug:    good-news-kword-qt3-widget-ported-qt2
comments:
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-13
    body: "Many kudos to our mighty KOffice/KDE/Qt hackers!\n\nCheers,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-13
    body: "Many thanks to David and all people who have made this possible. You guys rock :)\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-13
    body: "Nice work, David. Will this back-ported widget be part of an official QT 2.x release as well?"
    author: "Matt"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-15
    body: "No, I don't think Trolltech wants to support those hacks :)\n\nIt's at the moment in the koffice source tree, but it might be moved to a more common place (kdelibs) in case KWrite and/or e.g. ksirc want to use it too.\n\nBasically, if the next KOffice is ready before Qt 3, those classes will be part of it if nothing changes. But I doubt that will happen, I expect that Qt 3 will be out before KWord is done - unless you all give a hand :)"
    author: "David Faure"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-13
    body: "Yeeees!!! Stable and \"rich\" Kword :)"
    author: "Baracuda"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-13
    body: "Stable KWord?  I do think KWord has potential, but I can't imagine it being stable :)\n\nThis is good news, of course."
    author: "dingodonkey"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-13
    body: "What exactly does did mean, what is rich text, and how will this move improve the usefulness of KWORD ?"
    author: "Matts"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-13
    body: "Rich Text is text with additional attributes like color, different fonts etc. E.g. that what KWord (or any other word-processor) is all about.\n\nThe original rich text support in KWord (also written by Reginald) had its issues and this new version will be much better. For end-users that mostly means less crashes.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-15
    body: "... and many new features. Like, in the current KWord you can't even put some margin at the right of a paragraph (also called \"indent\", not sure which term is better for the right). The UI for it is there but disabled.\n\nAnd many other things, much better handled by the new widget."
    author: "David Faure"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-13
    body: "This is ,of course, Great!. Kudos to the Koffice team. BUT, IMHO if Kword doesnt have REALLY GOOD M$WORD import/export filters, it won't \"Catch up\",  even if it is (and I do think it is) technically superior from other products.."
    author: "t0m_dR"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "I always figured that since StarOffice went GPL someone from KOffice was busy...er um...looking at the filters it uses for other applications...Word being one."
    author: "Bob"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-15
    body: "Actually, porting the Abiword import filters might not be a bad idea. They are based on WV, and although the exporter isn't finished yet, the importer is very good."
    author: "Ash G"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-13
    body: "will kwrite be faster than it is at the moment?\n(opening a large file takes a lot of patients)\n\nwill tab-support get a better implementation?\n(got some problems with tabs)\n\ngood work\n- gnu"
    author: "gnu128"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "> (opening a large file takes a lot of patients)\n\nYes, I guess it takes a lot of ill people to open a large file. ;-)"
    author: "Rui"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "lol ;-)"
    author: "gnu128"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "A lot of distributions advertice their newest version with the \"cool\" KDE2 that includes a complete Office program. I don't thinkt that this is a good idea. Kword already can do a lot, but there is still a far way to go and I wouldn't do my daily work with it because it is not that stable as I would wish it. Don't get me wrong, I think it is(will be) a great application and I really have my respect from all these programmers that write this application, but I think that is not a good idea to call it \"stable\" yet.\nI am very happy about these news, that means that the development goes on (when I took look on the homepage of kword it didn't make the impression to me that the development was still alive). Maybe someone can put news/informations about kword on the homepage?"
    author: "Steffen"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "No offense to the KWord people but KWord kinda sucks(it's beta-ish), why not just work on OpenOffice, not everything has to be written in Qt.  OpenOffice has good MS inport, export filters, alot more features, and is more stable.  OpenWriter has allready been seperated from OpenOffice.  Not only that OpenOffice has alot more developers.  \n\n  This is not some kind of troll post, please don't respond that I'm stupid, or go away troll.  I'm really curious as to why not.\n"
    author: "robert"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "Don't know why thats bold!  Wasn't on purpose."
    author: "robert"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "Sure, but they've been in development for a lot longer than KWord has.  However, QT is good, GTK sucks, a lot.  What will happen is that we'll get this boost, and maybe, perhaps, a developer will start actually coding KWord, and then we'll get ahead of OpenOffice, just like everything else that is QT."
    author: "Charles Samuels"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "Just like Noatun and XMMS! Charles, what's the future of Noatun anyway? Something I noticed the other day, I can't play all media types with on program. Noatun plays mp3 and all normal sound files but video is still a little bit off. This isn't a complaint but I wonder if the following will happen after KDE 2.1:\n\nAvifile now has a beta arts plugin, will it get into kdemultimedia? Dispite this plugin noatun doesn't support .avi extentions. I guess that streaming support will also be added although it looks as if arts will need a bit of a redesign for streaming support and there are some threading issues with mpeglib.\n\nIMHO these two features would mean that I never had to bother with XMMS again.. Noatun would handle every important media type out there currently, this is something that I would really like in KDE... A good Multimedia framework.\n\nCharles (who posted the first in the thread), Simon and Martin who have done a lot of the kdemultimedia implimentation are some of the biggest stars of KDE and get very little press. These guys deserve a lot of credit and I'm sure we will be seeing a lot more info about kde's multimedia infrastructure after 2.1 as it's going to be a very big feature."
    author: "David"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "Mpeglib is funky.\n\nNoatun identifies a file by mime-type, then gets arts to create the proper type.  But you see, mpeglib identifies Divx files with video/x-avi , while noatun/kde identifies it with video/x-msvideo.  the latter being the correct one :)  Fix your $KDEDIR's .mcopclass files.  Same also goes with certain versions of mpeglib.\n\nAbout anything making it into kdemultimedia.  Not for 2.1, possibly for later versions.  For KDE 2.2, we're planning many changes into the arts framework, which will make lots of very cool things happen, like multitrack files, and Video frame embedding, and most importantly, true streaming.  However, Noatun already is 100% network-transparent."
    author: "Charles Samuels"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "That's correct, QT rules and GTK really looks amateurish. Just look at how inheritance is done in GTK, it makes me sick, to say the least. I think that the argument that GTK people use to justify why they use GTK instead of QT (that is, GTK is written in C, QT in C++ and there is no C++ compiler on every platform) is really obsolete."
    author: "Bojan"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "This is a pretty lame argument. All you're saying s that \"QT 7u13z!!\" For example, QT's signal-slot mechanism is dated and could be done a lot better, IMHO. GTKMM does signals very well and is significantly faster than QT's signal/slot mechanism. This \"design problem\" as I put it can be seen from the fact that QT requires a pre-processor (moc) before cpp (the C-Pre Processor, not not mention c++) gets to it. Plus your \"C++\" widget code ends up looking something like this:\n\nclass MyWidget {\nQ_OBJECT\nproteced slots:\nprotected signals:\n};\n\nWhat are things like Q_OBJECT and the \"keywords\" 'protected slots' and 'protected signals' doing in there? Did Bjarne add some new features to C++ that I'm not aware of? If you want to talk about bad inheritance design, I suggest that you look first in your own back yard, otherwise you'll be forced to say that GOB is actually kind-of nice, since it accomplishes exactly the same kinds of things in a very similar manner...\n\nOn the whole, QT is *very* nice from an API standpoint, esp. when compared to say MFC or some parts of GTK+. But then again, so is Inti (C++ wrapper around GTK+ and Gnome) and it has the benefit of utilizing an outstanding signaling mechanism (GTKMM). GTK+ isn't that bad of a library. GTK2.0 is *very* nice and does some things that won't be available until at least QT3.0\n\nDom"
    author: "mod"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "No, it couldn't be done better, and no, it's not dated.  C++ hasn't changed yet to allow signals and slots.  Qt still does it best.  And yes, Qt\nru13z.\n\nTrading moc in for slightly less messy code, that's also slower and less legible is not worth it in our opinion.  Go back to your gnome garden."
    author: "Charles Samuels"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-15
    body: "First of all, it doesn't matter how fast gtk--\nsignals are. They are used in areas which are\nnot time-critical at all. Also, most of the\nsignals in a gtk-whatever program are emitted\nin event handling, which _is_ time-critical,\nand in this area Qt's virtual methods are much\nfaster.\n\nAs for the class declaration, it makes headers\nvery nice to read and self-documenting. Tools\nlike kdoc and doxygen can use these tags to\nextract a nice class documentation. Where do\nyou look up the signal documentation in a gtk\nprogram? In the source code.BTW, moc is _not_\na preprocessor.\n\nAnd who cares about the things gtk 2.0 does when\nit isn't released? You better look at what is\nreleased, and find that gtk doesn't even support\nUnicode, which Qt does for about 2 years now."
    author: "Bernd Gehrmann"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-15
    body: "Gtk-- signals are more flexible than Qt's, that's true. That they are faster is, as it was already pointed out, unimportant because they are used to respond to user input.\n\nHowever it's also true that Qt's \"C++ extensions\", regardless of whether Bjarne Stroustrup sanctified them or not, are more readable, easier to use, and good enough in 99% of the cases. And the remaining 1% is easy enough to work around.\n\nIn short : yes, moc is a less-than-ideal solution. No, that doesn't make Gtk-- or Inti easier to work with than Qt. Just look at their respective documentations for a start.\n\nAnother thing : as of today, Inti does NOT wrap Gnome, and does NOT uses Gtk--'s signal library (libsig++), but a very simplified version of it.\n\nFinally, I expect Qt 3.0 to be released before GTK+ 2.0."
    author: "Guillaume Laurent"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-17
    body: "Just some minor additions, I mostly agree with Guillaume.\n\nlibsigc++' signals are not more flexible than Qt's, they are different. The system gtk+ uses is string-based, just like Qt's original implementation - and they know why.\n\nA template based solution is very appealing for static applications (runtime type checking, speed), but Qt's solution is way more flexible. A GUI designer like Qt Designer that queries dynamically loaded components for signals and slots or a library that constructs GUIs including signals and slots connections from XML is simply not possible with a template based solution like libsigc++.\n\nSome other features, like the available return type, may appear like flexibility, but all they indicate is bad design if you use them.\n\nAnother important factor is stability. What happens if you emit a signal to 5 receivers, and the receiver 4 is destroyed as a result of receiver 3 getting the signal. Can libsigc++ handle that without crashing? The situation is not as rare as it looks like, in fact, cross-dependencies like this happen a lot when using signals and slots extensivly. Those safety cost time.\n\nIf pure speed is what you were looking for (for example a network class that emits each single byte it receives), you may not want to use signals and slots in your Qt application. But why would you want to use a generic connection mechanism for this kind of task anyway if all classes know each other and there's only one single listener? You could simply use a standard C++ function call instead.\n\nLast but not least: nothing prevents you from using libsigc++ with a Qt application if you want to."
    author: "Matthias Ettrich"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-18
    body: "Never fails to amaze me how much cool information can be found here.  ;)\n\nYou all r3w1."
    author: "KDE User"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2002-09-24
    body: ">A template based solution is very appealing for static applications (runtime \n>type checking, speed), but Qt's solution is way more flexible. A GUI designer \n>like Qt Designer that queries dynamically loaded components for signals and \n>slots or a library that constructs GUIs including signals and slots connections \n>from XML is simply not possible with a template based solution like libsigc++.\n\nHow so?  You can do dynamic slots with sigc++ as simply as will Qt.  Simply\nmake a map from charactors to slots of some type.  Then you can look up slots\nby name.  Just because it does static checking doesn't make it restricted\nto purely compile time usage.  \n\n>What happens if you emit a signal to 5 receivers, and the receiver 4 is \n>destroyed as a result of receiver 3 getting the signal. Can libsigc++ handle \n>that without crashing? The situation is not as rare as it looks like, in fact,\n>cross-dependencies like this happen a lot when using signals and slots \n>extensivly. Those safety cost time.\n\nYes, it does that is its entire point.  If an connection is broken the \nsystem handles it cleanly by skipping the disconnected slot and then removing\nit from the list later.  \n\nPlease try it out.\n\n--Karl"
    author: "Karl Nelson"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-15
    body: "Here is a problem i've been noticing a lot on the dot: People complaining that so-and-so KDE app isn't as stable as it's GNOME counterpart, or doesn't have so-and-so feature that GNOME does, or whatever. This is great, because this is open source, and unlike some closed source companies (coughcoughmicro$hitcoughcoughaolcoughcough), such feature requests are valuble. Thus, they should be placed in bugs.kde.org (after checking to make sure they are not already there), where they will be more useful to the developers. Most developers are busy making KDE awesome to have time to read every dot comment, and bugs.kde.org allows the developers to easily organize bug reports and requests. Go OSS!"
    author: "Carbon"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "Seems to be one of the \"tricks\" included in structured text.  For example, an *I* in structured text becomes italics automatically.  I've removed the bold...  it hurts.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "Just a quick one:\n\nKOffice team never claimed that KOffice was anything more than Beta, in fact it wasn't long ago when the project was Apha-stage.  QT is way better than GTK - that the simple reason.\n\nBesides when KOffice is done it will be much more supperior than Open Office, the Koffice team aren't simple going to forfiet they project in faivour of Suns, the two group have defferent design goals, and different roadmaps - besides Linux is all about Choice!\n\nI'm 100% behind KDE/Koffice, in fact, I'm learning QT so I can't contribute! As for import/export filters, Its not as easy as you might think, Open office has a difernt \"toolkit\" than Qt, so it will take time to get all the filters in. Another problem is see is that, some features that M$ Office supports are not yet implemented in K Office so, theres no real way of translating that data to the new program. \n\nWith that said, I belive the KOffice team will have KOffice good and ready soon enough, but as for now, its still Beta.\n\ncheers just my ZW$ 0.02\n\n< -= iCEmAm =->"
    author: "iCEmAn"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-15
    body: "PLEASE READ.\n\nFirst of all OpenOffice dosen't use Gtk.  All the responses I got were Gtk flames(It's not my favorite GUI lib, but it's one of the better ones).  OpenOffice is written in very OO C++, as well as a little Java, you can write parts in almost any language though.  OpenOffice does use Corba, as well as UNO, and XPCOM.  This means any midleware midleware(it's not really slow) for those protocols that works for the KDE protocols will work.  OpenOffice will probably never use Gtk, theres allready too much code.  OpenOffice is really quite advanced, even if KWork had 15 developers, it would still take at least a year and a half to catch up to OpenWriter.  During that time OpenOffice is being developed.  OpenOffice also has a Windows port.  This is very important considering that 95% of desktops use windows.  This means that if OpenOffice ever get popular then switching people to Linux/UNIX is even esier.  KOffice will probably never have a windows port.  The only disadvantage to not using Qt, is that there are a more people that know Qt than the open office API though(allthough the APIs will be LGPL, and kept somewhat seprate), and that startup time will be a little slower."
    author: "robert"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-15
    body: "Still, openoffice is a HUGE pile of code - and of over-engineering. KWord now has a very clean and solid basis, and it won't take a year and a half (as you say).\n\nWindows ? Couldn't care less. The goal of KDE is to provide a free (and open) solution for Linux/BSD/Unix systems - so that people can get free from Windows, if they want to.\n\nThis sounds just like Konqueror vs Mozilla. Different goals (including target platforms), and different speed of development (did you notice how fast Konqueror became a first-class browser?). Sure, it doesn't work under Windows, but that's also why we didn't have to develop our own toolkit for it [like mozilla does].\n\nIn openoffice, it's everything that is redeveloped, from the toolkit to the component model and middleware. We already have all that working well with Qt/KDE, and that's why KOffice can evolve quickly on top of that.\n\nI have nothing against those other 2 projects. I just point out that the goals are different, and hence so are the technical solutions, and the speed of development."
    author: "David Faure"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-18
    body: "> did you notice how fast Konqueror became a > first-class browser? <p>\nI sure did! :-) <p>\nI had been using GNOME for about 1.5 years and only tried KDE out of curiosity because it was the default when I installed Linux-Mandrake. <p>\nKonqueror is Browser Heaven as far as I'm concerned - light footprint, fast, consistent UI, standards compliant. <p>\nM. <p>\n--<br>\nMichael O'Henly<br>\nTENZO Design"
    author: "Michael O'Henly"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-15
    body: "A year and a half? With David Faure working on it? Hahaha, it'll be ready next week. Even as I type the KDE scientists are  doing experiments to try and clone Mr. Faure (& Waldo Bastian), if sucessful the completed Koffice will be released by the end of Feb and roled out to all Microsoft locations by the end of March."
    author: "David"
  - subject: "Because KWord isn't a Word clone"
    date: 2001-04-02
    body: "KWord is designed more with Adobe FrameMaker in mind, which has better features for technical writers and those who need to do books and other large documents/multi-document sets."
    author: "Dennis"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-14
    body: "When will we get to see this  and the kde installer? When to the developers expect to have word support? I think that is also a major issue.\nWill it be kde 2.2 sometime this summer? I'm really getting excited about linux and kde specificly. Good jod koffice developers!!!\n\ncraig"
    author: "Craig black"
  - subject: "Re: Good news for KWord: Qt3 widget ported to Qt2"
    date: 2001-02-15
    body: "Have you guys ever thought of implementing some of the cool features Apache.org is offering ? I'm thinking of Xalan / FOP in particular, i.e. taking the KOffice Files (which are XML-Formats, right?) and then transform them _directly_ to PDF. This works really good, even if the version of FOP is 0.16. Better than Ghostscript & PDFWriter, IMHO. Think about it."
    author: "no time"
  - subject: "Time allocation can be of some help!!!"
    date: 2001-02-15
    body: "The frequent releases of KDE 2.x has taken all time of developers in KDE 2.x's development, and the KOFFice is not been given much attention, as I suspect.\n\nIf all (or say most of) KDE developers give time say, a month, or week, or fortnight developing KOFFice, then I believe KOffice would have been completed quite earlier.\n\nI allege that KDE is given the most time than Koffice. Am I wrong? perhaps :)"
    author: "Asif Ali Rizwaan"
---
Back in November, Reginald Stadlbauer <A HREF="http://lists.kde.org/?l=koffice-devel&m=97337143124867&w=2">announced</A> a new rich text widget for Qt. That widget could provide, he said, a very stable base for <a href="http://www.koffice.org/kword/">KWord</a>. So a rich text branch of KWord was started and became the focal point of KWord development... The problem was that the new widget was only slated to appear in Qt3. Recently, Thomas Zander <A HREF="http://lists.kde.org/?l=koffice-devel&m=98170739114893&w=2">reasoned</A> that a stable Qt3 was too far off and considering that current users needed more support right now, came to the conclusion that it might be best to drop the coolness-enhanced branch. Fortunately, David Faure valiantly <A HREF="http://lists.kde.org/?l=koffice-devel&m=98184264400340&w=2">tackled the task</A> of backporting the Qt3 rich text widget to Qt2 and succeeded!  This could be a big boost to rapid KWord development.

<!--break-->
