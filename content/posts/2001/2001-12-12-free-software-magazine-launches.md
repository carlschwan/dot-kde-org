---
title: "Free Software Magazine Launches"
date:    2001-12-12
authors:
  - "Dre"
slug:    free-software-magazine-launches
comments:
  - subject: "gnome bias"
    date: 2001-12-12
    body: "This magazine seems way too GNU/GNOME biased. The only mention of KDE in it is by RMS.  *shudder*  Let's hope we can change that."
    author: "ac"
  - subject: "Re: gnome bias"
    date: 2001-12-12
    body: "Didnt you know?  KDE is a dissapointment to the free software desktop.  \n\nAt least according to RMS....\n\nHetz, Juno, you two are very good at writing, send some articles there way!\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: gnome bias"
    date: 2001-12-12
    body: "I doubt it will lack KDE converage due to the fact it is released by KDE. They probably just don't want people thinking it is a KDE zine only."
    author: "RazzleDazzle"
  - subject: "Re: gnome bias"
    date: 2001-12-12
    body: "i think you may be thinking of another magazine effort started some time ago by some KDE people...\n\nthis one is managed by a Chinese group affiliated with the FSF"
    author: "Aaron J. Seigo"
  - subject: "Re: gnome bias"
    date: 2001-12-13
    body: "> This magazine seems way too GNU/GNOME biased. \n\nWell, that's a good reason to get some KDE content in there! Also, since the focus seems to be on political issues, something describing the great technology the KDE project is shipping today would be a great contrast.\n\nIf anyone working on a submission would like some assistance with editing or proofreading, please email me."
    author: "Otter"
  - subject: "Re: gnome bias"
    date: 2001-12-13
    body: "I agree, anyone planning to write an article?"
    author: "jz"
  - subject: "some words"
    date: 2001-12-13
    body: "Hi all,\n\nthanks for your interest to the magazine I am going to publish,\nI have read some posts here, and I think I shall say sth.\n\n1. The licenese issue.\n\nI hope authors could submit the articles under GNU FDL as the default.\n\n2. We are not KDE biased, instead, I welcome articles about KDE and Qt,  actually, my door to KDE authors is always open. I will be glad to read them.\n\nBut commerical sales pitches are not welcome, as the magazine is aimed to \"hackers write for hackers\". If someone wants to place sales pitches, pls pay us for advertisement fees (if you could meet our criteria on advertising.)\n\n3. We are not GNOME centered, instead, this is magazine for the \"whole\"\nfree software community globally. I am interested in publishing \"anything\" noteworthy about our free software community, not only the community in the States, but also in Asia, Europe, Australia, Africa.... So, if you KDE people think are belong to this community, then join with us! Don't guess my standpoint just by reading the unfinished columns listed.  You could issue your effect to me by your contribution of articles, but not the guessing which is not reliable.\n\nBTW, I am trying to control the printing costs of the magazine, so that the hackers in some poor countries could afford it too.\n\n4. Articles about philosophy discussions are also welcome,  as long as you are talking about free software, or free software related, but not the sales pitches for proprietary software (but you could critisize it by explosure its mistakes and weakness).  I maybe not agree with your philosophy, but I respect your rights to publish your points and views in our magazine (if I think yours sounds reasonable, or you have your logic behind it to support.)\n\nBest wishes,\nFrederic\n\n--\n------------------------------------------\nHong Feng\nPublisher, Free Software Magazine\nPresident & CEO, RON's Datacom Co., Ltd.\nfred@mail.rons.net.cn   hongfeng@gnu.org\nhttp://www.rons.net.cn/hongfeng.html\n------------------------------------------"
    author: "Hong Feng"
  - subject: "Re: some words"
    date: 2001-12-13
    body: "I can see the anti-kde prejudice in his statements already. Note how often he mentioned no sales pitches for proprietary programs. Its obvious thats what these FSF people think about kde. They think that kde is somehow not as open or free as Gnome. We should use this opportunity to get Kzine going again. The sun is setting on gnome and the FSF political folks and we should not artificially prolong there life by particpatring in there propaganda mechanisms.\n\nCraig"
    author: "Craig"
  - subject: "Re: some words"
    date: 2001-12-13
    body: "What about writing an article for this magazine instead of bitching how bad it might be? Don't mess up this forum with your brain-excrements."
    author: "Jake"
  - subject: "Re: some words"
    date: 2001-12-14
    body: "> I can see the anti-kde prejudice in his statements already.\n\nI don't. Craig, just drop this KDE versus GNOME thing. It's not us versus them. It's us together against Microsoft, et al."
    author: "jz"
  - subject: "Re: some words"
    date: 2001-12-14
    body: "Thats incredibly naive. The free software movements is a completely separate movement than the open source movement with totally different and conflicting aims. KDE i guarantee will play second fiddle to gnome in any FSF publication. If you don't see that then you haven't been following the movement or have let your idealism cloud your judgment. A open source magazine that expounds the superiority of linux and the kde desktop would serve the open source community in a more fruitful way then playing second fiddle in a rival movements publication and being portrayed as a second rate alternative to a Linux desktop that doesn't even have 50% the user base that KDE currently enjoys. Not to mention Richard Stallmans ego is squarely in the way of KDE being placed on a equal to or its deserved higher position.\n\nCraig"
    author: "Craig"
  - subject: "Re: some words"
    date: 2001-12-14
    body: "> playing second fiddle in a rival movements publication and being portrayed as a second rate alternative to a Linux desktop that doesn't even have 50% the user base that KDE currently enjoys.\n\nThat's debatable. But does it really matter?\n\n> Not to mention Richard Stallmans ego is squarely in the way of KDE being placed on a equal to or its deserved higher position.\n\nThe GNOME'rs are currently pissed at RMS too."
    author: "jz"
  - subject: "Re: some words"
    date: 2001-12-16
    body: "All he asks is no advertisement for commercial proprietary software. Now how hard is that?\nAll you have to do is write an article about KDE, and since doesn't contain any commercial proprietary software, what is the problem?\nHow in the world can that be anti-kde prejudice?"
    author: "Stof"
  - subject: "GNU/FREE?"
    date: 2001-12-14
    body: "Just out of interest, because I've always wondered, where does GNUSTEP fit into the the Free softwaware big picture? GNUStep was started long before GNOME, but somehow, GNOME is the flagship Free SOftware desktop.\nJust thought I'd go offtopic, seeing that any mention of FSF starts the KDEvsGNOME flames."
    author: "Ezz"
  - subject: "Re: GNU/FREE?"
    date: 2001-12-14
    body: "> GNOME is the flagship Free SOftware desktop.\n\nThat's not really true. KDE is also a desktop that is Free Software, and is, of course, a flagship (as evidenced by the inclusion of KDE into many Linux distros). However, I think that you mean, \"Part of the GNU Project\". \n\n> GNUStep was started long before GNOME\n\nYour're right. GNUStep was started long before GNOME and even before KDE. However, it simply wasn't complete enough until about a year ago. Emulating someone else's API, like GNUStep must do, simply takes more time to implement than making your own from scratch, like KDE/Qt and GNOME/gtk."
    author: "jz"
  - subject: "Re: GNU/FREE?"
    date: 2001-12-14
    body: "What he means is its the flagship GNU Desktop. Which means it is the flagship Desktop of the FSF. If you don't see that well i guess i don't know what to say.\n\nCraig"
    author: "Craig"
  - subject: "Anti-KDE?"
    date: 2001-12-15
    body: "I doubt that Hong Feng took time to write a reply to this forum in hopes of spreading secret anti-KDE propaganda. I think that any of us who scanned his reply and tried to see dragons where there are none need to step back a bit from the whole KDE vs. GNOME issue. They're both great products, and if we send him some articles he would include them. I doubt this is a false promise, as he took extra steps to tell us which license to place the article under, and asked that noone try to use the magazine as a launching pad for commercial advertising. I'm excited to see that a magazine will be published that is not controlled by its advertisers. I mean, Microsoft owns NBC. They literally own the media. In the issue before the last issue of Linux Magazine (US publication) Microsoft had placed an ad talking about how reliable their products were. The next issue had at least 3 letters from customers who were cancelling their subscription. Now THAT is a dirty tactic. I'm sure M$ meant to cause the publication to lose some subscribers. I'm sure that the Free Software Magazine will be scrupulous enough to exclude Microsoft and thus avoid their nasty games. I'll be looking forward to my first issue, and I'll support the magazine with my subscription. It's time for all of us to quit name-calling and finger-pointing and start to work together. Remember, if Vercingetorix had managed to keep the Gauls united, Caesar would not have been able to defeat them. Will ancient history repeat itself? In ten years, the whole KDE vs. GNOME issue could become resolved ... in a way that neither you nor I want to see.\nNathan Waddell"
    author: "Nathan Waddell"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-16
    body: "I commend your lofty aspirations and your cooperative attitude. However I'm afraid it's not as simple a KDE vs. Gnome issue. Whenever you deal with the FSF your dealing with a cult of personality of sorts. That of course being Richard Stallmen. Here's where the problem lies. He has a clear picture of the future of free software and believe me his vision does not include either Linux or KDE. \n\nIn Dick's little world The hurd kernel will replace Linux and Gnome will replace KDE and he'll have the GNU Operating system that he already pretends he has. He is driven by personnel ambition and Free software is his tools. Here's the scary part. The FSF folks idolize this guy and share his dream. He wants the glory that Linus now has for himself. So you see its not a KDE vs. Gnome issue at all,  but something  much weirder.\n\nYou venture into that world of the FSF folks and any sane person will get the willies. Lets stay on the side where common since and sanity rule and do a KDE/Linux magazine and let them keep the nut house.\n\nCraig"
    author: "Craig"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-16
    body: "> The FSF folks idolize this guy and share his dream. He wants the glory that Linus now has for himself. \n\nNot all FSF-affilated folks do. For example, take the GNOME foundation. There is quite a rift between them and RMS currently. This is because RMS wants to continously meddle in GNOME's affairs.\n\nAnyways, this magazine is about open sourced software. A large amount of this software is linked to the FSF on paper through the usage of the GNU Public License, including KDE.\n\nSo, Hong, if you are listening, the opinions of Craig are not shared by many in the KDE community anymore, although I am still ticked off at RMS for other things."
    author: "zzz"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-17
    body: "<em>A large amount of this software is linked to the FSF on paper through the usage of the GNU Public License, including KDE.</em>\n\nThe use of a particular license does not mean that one project belongs to another. There is nothing in the GPL (or any other OSS license) that attempts to place one organization over another. Just because some portions of KDE are under the GPL does not mean that KDE is linked to the FSF.\n\nParts of KDE are also under the BSD license. Is KDE now linked to *BSD? Other parts of KDE are under the Artistic license. Is KDE now linked to Perl?\n\nKDE is Free and Open Source Software. If the FSF wishes to associate itself with KDE, they have all the freedom in the world to do so. But that doesn't mean that KDE is associated with the FSF despite any official grants of forgiveness on the part of RMS."
    author: "David Johnson"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-18
    body: "You're right, but KDE is still linked to the FSF by using a FSF-copyrighted work (the GNU GPL). This certainly does not mean that FSF is affiliated with KDE at all. \n\nGNOME itself is linked to the FSF deeper. It is part of the GNU project. Remember that KDE is NOT, nor ever will be (hopefully, since then RMS would try to gain more control in KDE)."
    author: "zzz"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-16
    body: "Oh sure, then explain me why the members of the GNOME Foundation haven't voted on RMS if all FSF folks worship him."
    author: "Stof"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-16
    body: "To be perfectly honest, I can't see how on Earth Craig managed to read any GNOME-biased features out of Hong's words. He is aware of the fact that with the KDE culture, there comes a bunch of proprietary software as well. He doesn't even mention it as strongly as e.g. I do, and I can't see anything indicate that he's any more GNOME-biased than KDE. He's making a magazine for the open-source community, and so he only mentions... that he only wants the open-source culture! Saying anything else would kinda spoil the point, wouldn't it?\n\nIf saying \"only open-source, none proprietary\" means anti-KDE... then KDE obviously doesn't belong to the magazine, does it? :) Note the question mark, I'm not making a statement here. I just can't see how you read anti-KDE from his reply.\n\nAnd if you people have anything to say about GNOME and KDE, write it down, read it over a couple of time, contribute to Hong's magazine, and quit the whining. :)\n\nPeace."
    author: "Helgi Hrafn Gunnarsson"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-16
    body: "I just read his statement once again and sure enough he didn't mention Open Source once. I guess whats tripping you up is a confusion between free software and Open Source software. I know its sometimes confusing but they are not the samething. The Open Source community is a more open place that excepts many different contributers. The FSF community is a more political driven culture that is closed to commercial ventures. In the Open Source world some proprietary company's are a valued members of the community. The FSF folks want to take away the right of a programmer to choose his  or herlicenses. The fact is they have a much more close minded Stallmen driven perspective. For the most part the KDE community is a real community with many different contributers. They are a smaller more closed and intolerate bunch driven by a creepy love of Dick Stallmen. Its not uncommon to hear them claim that he's the \"real\" inventor of Linux. Its not the sane KDE community that we love. Its the difference between the Democrats and the Communits or the Republicans and the John Berchers. We are not them and they are not \"us\".\nI recommend co oreration but its importantant to remember that we are two different community's with different objectives."
    author: "Craig"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-16
    body: "Somehow you remind me of Morgoth, an ancient GNotices troll who keeps spreading FUD even though it has been proven that none of what he said is true...\nAnyway, I'll shut up now. :-X"
    author: "Stof"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-16
    body: "Gah, as much as many of us hate the FSF for certain comments that RMS made long ago, I think attributing them to Hong is simply unfair. No one else see's any \"Anti-KDE\" sentiment in his comment except you Craig. \n\nAnd yes, I do think that RMS's personality cult is weird. RMS is human. He has many faults and good sides, but all of us do.\n\nAlso, think about this craig. Included with every kde source package is the file COPYING. These mostly include the GPL. It was written and copyrighted by the FSF. How much code in KDE have you written, Craig? Yup, 0 bytes (If I'm not mistaken)."
    author: "zzz"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-16
    body: "There are many ways to contribute besides codeing."
    author: "Craig"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-16
    body: "> There are many ways to contribute besides codeing.\n\nYou mean like ranting about how evil the FSF and free software movements are? Who needs Microsoft when we have people like you."
    author: "kholmes"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-18
    body: "Long ago??? Just look at the crazy drivel that flows from his mouth on a daily basis. Lets take today as an example.\n\nhttp://www.linuxtoday.com/news_story.php3?ltsn=2001-12-17-015-20-OP-LL"
    author: "Craig"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-18
    body: "uh, that has nothing to do with KDE."
    author: "zzz"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-17
    body: "now could somebody explain this once in plain english: if one does not install any programs besides gpl and lgpl programs (and uses a free distribution, such as debian), will there really be a difference between gnome and kde?\nand further: i consider myself a communist and do therefor not believe that corparations will be able to program \"for free\" forever. i basically can't believe that companies such as red hat or the kompany will be able to get investors suport forever, since they will not be able to make much of a profitt. but does that mean that i am against these companies helping linux getting stronger? np, of course not! as long as they can get capitalist to pay for a concept which in the end will abolish capitalism, let them do it. let them tell all kinds of stories of how they are going to make money in some years, etc. if it helps them continue.\nand here comes my question:  now what exactly is the problem for political people like myself to support kde? i don't see it! and thats also why i don't get the  gnome kde controversy... let the gnome-programmers be more political... in the end (if we political people are right) it will not really matter whether the programmer was a communist or not, as long as the source is open, capitalism will not prevail in software business in the long run."
    author: "Johannes Wilm"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-17
    body: "See what i mean. Total fruit loops."
    author: "Craig"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-17
    body: "ok could you maybe explain a little more about that? do you intend to say that i am a \"fruitloop\" and that then what? if that is your intention, please consider that there are many different political backgrounds for why one can be for open source/free software. please don't expect everyone to have the same visions, isn't the entire movement more something of a coalision of people who have different backgrounds for why they come to the conclusion that open source/free software is right?\ni don't you to be communist either.\nbut if you meant: see this is a guy who doesn't know what he is talking about: please tell me what you mean. are there things i don't understand in this fight? or technically? i will be happy to learn."
    author: "Johannes Wilm"
  - subject: "Re: Anti-KDE?"
    date: 2001-12-17
    body: "Just ignore him. He's a troll."
    author: "zzz"
  - subject: "Craig"
    date: 2001-12-19
    body: "I think Craig is trying to say he doesn't want to buy the magazine.\nAnyone know when the first issue is coming out and how to subscribe?\nNathan"
    author: "Nathan Waddell"
  - subject: "Re: Craig"
    date: 2001-12-20
    body: "Buy huh? But I thought the FSF would be morally agaist selling anything."
    author: "Craig"
  - subject: "CSE"
    date: 2002-09-25
    body: "Hi \n>\n>I was wondering if you could help me with the following I am a student of\n>print with the following project to do::\n>\n>Task outline:\n>\n>A new magazine is planned for the year 2002 by a company called 'BLP'\n>(Buttler Lloyd Publications)\n>\n>Subject: Graphics, print and media industry\n>\n>Although there are competitive magazines in each area there does not seem\n>to be any single one that represents all within this continually merging\n>industry. Therefore initially the company PLP thinks there may be a market\n>for such a magazine to keep all concerned informed.\n>\n>The informative approach is one aspect being considered, both also\n>recruitment and employment, but not to the detriment of making the\n>magazine boring and without entertainment value. The magazine needs to be\n>high profile and have the image of being a 'must have' publication to make\n>it viable.\n>\n>My brief:\n>\n>To produce a report for the company, researching the magazines feasibility\n>and production. Look at:\n>Who would buy the magazine and what they would be interested in (articles)\n>where the magazine would be situated in the competitive market\n>Where the publication would be promoted (why and how)\n>Its possible distribution\n>What it would be called and why\n>What it would look like and why (consider quality of substrates and\n>production)\n>Price and name of magazine\n>\n>All aspects of the magazines image and placement must be researched and\n>illustrated in your report. Rough design ideas must also be developed and\n>included, illustrating your results.\n>\n>Can you help me with any of the above / can you point me in the right\n>direction of where I might get any information to make a start on this\n>\n>When you're launching and researching a new mag are there certain\n>procedures that you following that you would be prepared to share with me\n>\n>Can you give me some idea of you advertsing rates too?\n \n>Your assistance in this matter would be greatly appreciated.\n>\n>Kind regards Joanna\n\n\n\n\n\n\n\n\n\n\n\n\n"
    author: "Joanna Shields"
---
<a href="http://www.rons.net.cn/hongfeng.html">Hong Feng</a>, apparently a
former chief editor with O'Reilly & Associates in Beijing,
is preparing the first issue of the
<a href="http://www.rons.net.cn/english/FSM/issue01">Free Software
Magazine</a>, a periodical intended to be by and for free software hackers.  He is looking for some contributors
to the inaugural issue (slated for January 2002) who can write on
KDE development and/or
Qt/Embedded development (hmmm, an article about
<a href="http://www.konqueror.org/embedded/">Konqueror Embedded</a> or
<a href="http://www.kdevelop.org/">KDevelop</a> with its
<a href="http://dot.kde.org/1007435836/">new cross-compiling support</a>
sounds like a <em>sweet</em> fit).  A
<a href="http://www.rons.net.cn/english/FSM/issue01">number
of articles</a> appear to be in the queue already, and a
<a href="http://www.rons.net.cn/english/FSM/RMS_preface">preface</a> by
<a href="http://www.stallman.org/">RMS</a> is already online.
Guidelines are available
<a href="http://www.rons.net.cn/english/FSM/issue01">here</a>.  Noteworthy are that you can base a submission on something previously published, so long as you (or someone submitting with you) retain the copyright, and that the submission should be under some Open Publication license.  The deadline for submissions is December 20, so now's the time to get involved.

<!--break-->
