---
title: "Advanced Theming Tutorial:  Programming in *Style"
date:    2001-04-23
authors:
  - "Dre"
slug:    advanced-theming-tutorial-programming-style
comments:
  - subject: "Re: Advanced Theming Tutorial:  Programming in *Style"
    date: 2001-04-23
    body: "Please, give us a theme-editor! Its SOOO much work to create a theme, and it becomes even more when you need to search for all the specifications of the themes for yourself.\n\nI REALLY don't know about all that stuff, but would it be so hard to have a tree view with all necessary icons on the left, and for each one, an embedded krayon-part on the right? Something like that would make it so much easier!\n\nAlso, many other windowmanagers have nice themes. But I think the one who has the most is Windows. Well, not exactly windows, but Windowblinds, which is for windows. Its an add-on software, making windows completely themeable. It's really great and there are a lot of free (?!) themes available for download. Check it out on www.windowblinds.net. It uses .wba files for themes. Could someone extend the KDE1 Theme importer to use these .wba files?\n\nJust some ideas, hope they inspire someone ;)"
    author: "me"
  - subject: "Re: Advanced Theming Tutorial:  Programming in *Style"
    date: 2001-04-23
    body: "Yeah!  Mosfet, where are you man? ;)"
    author: "KDE User"
  - subject: "Re: Advanced Theming Tutorial:  Programming in *Style"
    date: 2001-04-23
    body: "I'm right here ;-) After spending about a year playing around with widget styles, KWin styles, and theme engines prior to the release of KDE2.0, I got rather bored doing theme oriented stuff. Lately I've been playing around with Pixie, and am toying around with a few new apps.\n\nThe problem has been it's been difficult to get new developers to take over the theme stuff. Luckily, there has been renewed interest and coding by people like Gallium, who has been doing some neat stuff like importing iceWM themes and some new KWin stuff.\n\nYou can expect some new widget styles from me, but that's about it as far as themes go. Now I want to hack apps :)"
    author: "Mosfet"
  - subject: "Re: Advanced Theming Tutorial:  Programming in *Style"
    date: 2001-04-23
    body: "What happened to the Widget Designer? I saw a screenshot of it, on your  site once.\nIs that one of the apps you are hacking? (I hope so ;-)"
    author: "reihal"
  - subject: "Re: Advanced Theming Tutorial:  Programming in *Style"
    date: 2001-04-24
    body: "That's too bad. Well, not for you, but for the look and style of KDE. The KDE project needs a Mosfet, someone who knows how to create eye-candy, not only for the icons (which Torsten does well) but to the desktop as a whole.\n\nActually, since you stopped working on this, GNOME has caught up quite a bit when it comes to style and elegance. If the KDE project is going to keep improving its looks, it needs a new Mosfet. Who? I don't know."
    author: "Matt"
  - subject: "Legal problems"
    date: 2001-04-24
    body: "I was thinking about WindowBlinds the other day, so I went over and downloaded one of their themes.  It had a prominent disclaimer to the effect that you can't use the theme with anything but WindowBlinds.  It's probably a standard-issue disclaimer that is in all the WindowBlinds themes.  So even if someone could make a WindowBlinds theme importer, it wouldn't be legal to use it.  While this probably wouldn't stop anyone from using it, it would prevent KDE from including it in the official KDE packages.  Those pesky lawyers always ruin our fun :-("
    author: "not me"
  - subject: "Can't you just import Windows 98 themes?"
    date: 2001-04-24
    body: "There are thousands of Windows 98 themes, and hundreds of them are quite nice and free, why can't KDE have an Windows-Theme-Importer for KDE.\n\nBy the way, KDE Themes simply lacks Fonts, Mouse Pointer/Cursor, and ScreenSavers! \n\nCan I expect it in KDE 3.0?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Can't you just import Windows 98 themes?"
    date: 2002-01-30
    body: "Import me some themes\n"
    author: "Michael Howard"
  - subject: "Re: Can't you just import Windows 98 themes?"
    date: 2003-05-24
    body: "yes, start working."
    author: "mark"
  - subject: "Re: Can't you just import Windows 98 themes?"
    date: 2003-09-01
    body: "where can i get them?"
    author: "frank"
  - subject: "Re: Can't you just import Windows 98 themes?"
    date: 2003-10-17
    body: "I WANT THEM, AND I WANT THEM NOW!"
    author: "MWiley"
  - subject: "Re: Can't you just import Windows 98 themes?"
    date: 2003-11-06
    body: "KDE is an open source application.\n\nYou can write anything for it if you want. But people who are already writing it, are focussing on more important things, like new user interface things, instead of copying Windows 98 looks.\n\nKDE 3.0 and up support icon and mouse pointers, altough the mouse pointers need XFree86 4.3 or up, Red Hat Linux 9 is a good example of a distribution that ships it.\n\nAnyways, you could always write a new KDE theme (code theme, not graphics), and make an option to import Windows 98 themes. Why not?\n\nGet to work and write one. That is how open source works. And don't blame other people that spend their spare free time for important things that they don't write tools to copy another ui."
    author: "Sijmen Mulder"
  - subject: "Re: Can't you just import Windows 98 themes?"
    date: 2007-07-01
    body: "I have windows 98 and Finding free themes is not as easy as you think"
    author: "vallentyne"
  - subject: "Re: Advanced Theming Tutorial:  Programming in *Style"
    date: 2001-04-24
    body: "> Can I expect it in KDE 3.0?\n\nNo, in KDE 2.2. Read the changelog again."
    author: "Matt"
---
Last week, we posted a <A HREF="/987013890/">story</A> on a <A HREF="http://www-105.ibm.com/developerworks/education.nsf/linux-onlinecourse-bytitle/B390A002F1F75BD186256A29005F94ED?Open&l=kde1,t=gr,p=KDE-Tutorisl">theme tutorial</A> sponsored by <A HREF="http://www.ibm.com/developerworks/">IBM developerWorks</A>. The tutorial was targeted at non-programmers who wish to theme window decorations, icons, sounds, colors, etc.  However, at the widget level, KDE/Qt also offers a more powerful type of "theming" referred to as <EM>widget styles</EM>, which must be programmed.  <A HREF="http://www.kde.org/people/rik.html">Rik Hemsley</A> recently authored a <A HREF="http://www.geoid.clara.net/rik/widget_style_tutorial.html">tutorial</A> for programming KDE styles (download the tutorial and sample code <A HREF="http://www.geoid.clara.net/rik/arch/widget_style_tutorial.tar.gz">here</A>).  The tutorial also points to the ability to create theme-based styles; these basically allow you to modify a coded style using pixmaps.  More information on theme-based coded styles is available <A HREF="http://www.mosfet.org/widgettheme-tutorial/contents.html">here</A> from <A HREF="http://www.mosfet.org/">mosfet.org</A>.
<!--break-->
