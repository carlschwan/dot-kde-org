---
title: "KDE Developer's Checklist"
date:    2001-03-24
authors:
  - "jtranter"
slug:    kde-developers-checklist
comments:
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-24
    body: "> make sure you aren't duplicating an existing application \n\nIMHO this shouldn't stop someone from developing an application if they want to.  The more the merrier."
    author: "KDE User"
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-24
    body: ">> make sure you aren't duplicating an existing application \n\n> IMHO this shouldn't stop someone from developing an application if they want to. The more the merrier.\n\n    I think that this is intended more as a guideline... I've only come across two projects that I felt I could finish (being involved in work and the local Rocky Horror eat my time), and finding that one was done (a weather applet) gave me a direction to go in.  Now I'm working on using low cost ( US$5 to $10 ) Dex Drives to store GPG keys to create a system by which a file (or later a directory tree) is encrypted unless the unlock media is inserted (a normal PlayStation memory cartridge, which is very inexpensive).  Unfortunantly, I can't find specs on the Dex drive, and now I'm slogging through trying to reverse engineer it.\n\nOkay... this reply got a bit off track; but you get the idea - keeping duplication of effort in mind at the start of a project will at *least* make you research what others have done.  If you don't like what has already been done, by all means go ahead.  Otherwise, you might have another \"itch\" to address first.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-24
    body: "This is because Linux users are socialist, why have competition when you can have a single unified force all working for a common goal(like Microsoft windows)."
    author: "Lee"
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-25
    body: "Only in the USA is socialism a four-letter word.\n(Sarcasm implied ;-)"
    author: "me"
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-24
    body: "> > make sure you aren't duplicating an existing application \n> IMHO this shouldn't stop someone from developing an application if they want to. The more the merrier.\n\nSo much for a well versed opinion. It is hardly ever so simple. One of the biggest problems in open source is not the projects that get started... it's the ones that never reach maturity. And the idea of checking has a mysterious connection with the concept of open source... you know the one. That you will build a better mouse trap and everyone will jump on your wagon. All too often we find that the simple little project is far more consuming.\n\nIf there is something that should be considered it's this. Before you build something see if someone else is building the same thing. Now... if after you check out their project and objectives if you cannot come up with anything that distinguishes what you want to do from them then it is plain foolish not to see if you can team up. OTOH if you have distinct difference in how you see things working it makes more sense to do your own.\n\nEven if you are doing your own project you may still want to share code or information. What bothers me is reading that someone is starting a project, I check out their web site and it's barely useful but does have a thumbnail description. Then I think of several other projects that are relatively identical except farther along. Let's be real people Think about this...\n\nClosed source takes a group of people and squeezes product out of them. So does open source more like a splinter of that group all alone, or a mass movement? Very few open source projects are at critical mass. That being that they would continue just fine if their authors walked away. Many more project would be more mature and more rewarding if people followed the advice here instead of just glibly launching another project that shadows several others with no distinctive purpose.\n\nAsk yourself this. If all the core KDE developers had set out to build their own desktops which one would you be using? Probobly none."
    author: "Eric Laffoon"
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-24
    body: "Time your self doing these things:\n\nRead 1K lines of code and understand it.\nWrite 1K lines of code and understand it.\n\nCompare the times.  Then ask yourself which was more fun, reading code or designing and writing it.  \n\n   The reason that KDE can't be made by one person is because it does ALOT of stuff.  People code peices of KDE, the peices get rewritten alot.  So even in the case of the KDE core people rewrite stuff.  The guy who wrote the checklist is probably just a socialist."
    author: "Lee"
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-24
    body: "I think it's still useful to point out that you might want to look if the application you wish to make already exists. KDE is a desktop environment so there are a huge number of different applications that can be written for it. People want to write their own program, but they also might wish to contribute in a useful way. It might just be possible to find something you would enjoy to make, and isn't already there.\n\nOf course people should make whatever they want to make, but it doesn't hurt to point out what's already there.\n\n-- \nMatthijs"
    author: "Matthijs"
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-26
    body: "> make sure you aren't unknowingly duplicating an existing application\n\nNOTE: unknowingly. You're of course free to develop what you want."
    author: "per"
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-24
    body: "What exactly is a Linux Software Map file?  What does it do?  How does it work?"
    author: "not me"
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-24
    body: "AFAIK the .lsm is useed to build a description of the project, and when you upload it somewhere it can create an anouncement. Like when you upload to ftp.kde.org it will post an announcement to kde-announce. I assume it was meant for the official Linux Software Map."
    author: "Jono Bacon"
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-24
    body: "put some links in there.  Most people are too lazy to go find the style guides, etc."
    author: "tim"
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-24
    body: "As a humble user, I'd like to add this suggestion:\n\nIf your application should only be run by root, make sure it brings up the root password dialog when it's launched by a normal user.\n\nThere's one very useful (non-core) KDE utility that doesn't do this. It's not harmful ultimately because the lack of appropriate permissions prevents it from completing its task -- but it is incovenient."
    author: "Michael O'Henly"
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-25
    body: "Care to tell us which it is so we can fix it? ;-)\n\nRich."
    author: "Richard Moore"
  - subject: "Re: KDE Developer's Checklist"
    date: 2001-03-25
    body: "KPackage used to do that, but I think it's mostly fixed now (I haven't needed KPackage much since switching to Debian, so I don't know anymore).  The KDE SysV init editor does it too.  When you start it as a non-root user, it brings up an informative dialog that tells you that you cannot change anything, only look.  This is fine, but it doesn't give you the option to run it as root.  You have to start up the menu editor, look to see what command starts it (ksysvinit), go into a Konsole, su, and then start it, just to use the program the way it was intended!\n\nIMHO, the way all KDE programs should handle a situation like this (a program has extended capabilities when run as root) is to use kdesu, because a user can always hit the \"ignore\" button in the (unlikely) case that he/she doesn't really want the program running as root.  Also, that \"ignore\" button in kdesu should be renamed to something that describes what it does, like \"run without root priveleges.\"  And, the text \"requires root priveleges\" should be modifiable by a command line option or something to say \"has extended capabilities when run as root\" or something.\n\nOf course, the best solution to a situation like this would be a way to change the priveleges of an already-running program, so you only have to enter the root password if you actually do something that requires root priveleges.  I don't know if such a thing is possible."
    author: "not me"
---
There are a number of things that are easy to overlook when working on KDE applications. I've put together a <a href="http://developer.kde.org/documentation/other/checklist.html">checklist</a> on <a href="http://developer.kde.org/">developer.kde.org</a> to help developers keep track of important items when creating new applications or when making changes to existing ones.  You can <a href="mailto:tranter@kde.org">send me</a> any additions, corrections, or comments.  Happy hacking.

<!--break-->
