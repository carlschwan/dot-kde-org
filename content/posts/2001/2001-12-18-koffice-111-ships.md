---
title: "KOffice 1.1.1 Ships"
date:    2001-12-18
authors:
  - "Dre"
slug:    koffice-111-ships
comments:
  - subject: "printing"
    date: 2001-12-18
    body: "The other day I was using KWord on a university machine (rh 7.1).  I thought it looked pretty impressive, and had little trouble typing up my document.  The document looked great on my monitor, but after picking up the hard copy from the printer I nearly fell over laughing.  The printout looked nothing like the print-preview (or whatever koffice calls it) on my screen.  The margins were way out of wack and the font size was probably double what I specified.  I really hope there has been some work fixing up these types of bugs, because other than that it appeared to be a solid product.\n\nNonetheless, thanks for all the hard work.  I look forward to trying out the latest KOffice.\n\nChristopher"
    author: "Christopher Martin"
  - subject: "Re: printing"
    date: 2001-12-18
    body: "Here is a little experience of mine regarding printing in kword\n\nSome time back I created a two page ad pamplate with kword. It was quite complex. Around a dozen frames in one page full of graphics and fancy windows fonts etc. It worked perfectly. \n\nLater I printed it to a .ps file. Converted it to pdf(because I don't have a printer. Need to go to a cyber cafe to get a printout. And they have only windows). Originally the job was scheduled for pagemaker but I asked my friend to spare 20 minutes and it was done.\n\nMay be people experiencing problem with printing, try to print to a .ps file. If gv shows it as intended I suppose there is little reason for not being as intended on paper. It worked for me. Try it out.\n\n Shridhar"
    author: "Shridhar Daithankar"
  - subject: "Re: printing"
    date: 2001-12-18
    body: "This kind of problem does not occur only in KWord, the KOffice suite or whatever. This is a common problem with all *nix printing and a very frustrating one at that.\n\nGoing from my experience I got out of my desktop publishing days way back in the late '80s/early '90s, one of the most probable causes of all of this is that there does not seem to be any integrated font-management between the display and the printing system. Another one is that all the renderers seem to be seperate and are not sharing some common engine.\n\nNot being an active coder (except for some desktop database scripting and the odd shell script or SQL script here and there), I could be wrong here. But judging from a cursory gander at how things seem to work, printing wise, under both GNOME, KDE and anything else (like, printing using the Motif-based NetScape Navigator), this is the most reasonable hypothesis I could come with.\n\nPrinting is the Achilles heel of the OSS movement and they will need to address it if they want to take over The Desktop (TM). If Joe Schmoe typical IT architect can't get his 40 page technology analysis report printed for the 14:00 meeting downtown, how do you want him to recommend a switch to OSS?"
    author: "Joseph B."
  - subject: "Re: printing"
    date: 2001-12-18
    body: "Try cups. It works very well, from my experience. \n\nI think the problems that the grandparent poster had was the lack of wysiwyg features in kword. AFAIK, this is planned for koffice 1.2?"
    author: "Sashmit Bhaduri"
  - subject: "Re: printing"
    date: 2001-12-18
    body: "CUPS does not fix the problem with the fact that printouts don't match what's on the screen (fonts/graphics bigger/smaller than expected, etc.). It is a part of the solution, not the solution itself.\n\nUnless proven wrong, I maintain it's the lack of integrated font management (and even renderers!) that makes printing under KDE, GNOME et al not compare well at all with the leading commercial, closed-source desktop environments (MacOS, Windows)."
    author: "Joseph B."
  - subject: "Re: printing"
    date: 2001-12-19
    body: "Qt3.0 supports embedding TT fonts in a Postscript output - so you will have an integrated font management. I think that having two renderers ( Postscript an X11 ) isn't a disadvantage - you can have a print server running without X ( or even networked postscript printer ), you can generate reports from your console only server, today even a single page often have about 10MB of pixel data ( *100 send by network, stored in a queue like in Windows ? ) and remote X server would have to send a rasterized output back to the main computer, we have command-line tools like Tex, frame buffer versions of programs with printing, support for high-quality printing hardware etc. The problem is a low quality of ghostscript, especially no driver support."
    author: "Me"
  - subject: "Re: printing"
    date: 2001-12-19
    body: "I do see a possible step in the right direction with Qt 3.0, judging from your description. Question is, will this embedding mecanism you allude to ensure that if I want a font \"about this big\" in my document, that it will come about the same size and not twice as big/smaller on paper?"
    author: "Joseph B."
  - subject: "Re: printing"
    date: 2001-12-18
    body: "I hope that KSpread has got (or is getting) a zoom function and can (or will be able to) scale the worksheet down when printing. Even playing with Ghostscript didn't seem to be able to help me in this matter - indeed, the PS viewer, when asked to view A2 pages (which I wanted to scale down to A4) just crashed.\n\nKSpread does show some promise, but it is slow and, without a zoom function, it is frustrating to use. Still, at least it compiled from source alright - I never managed to get Kivio compiled and running..."
    author: "Paul Boddie"
  - subject: "Re: printing"
    date: 2001-12-18
    body: "Oh well...you can always try Latex2e, convert it to postscript then to PDF. Latex2e is no longer your old brother's/sister's software system for typesetting technical documents."
    author: "Larry Stephens III"
  - subject: "Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "Kongratulations to all!, I have pondered about why KDE apps look a bit uglier to win2k, and mostly it's cosmetic. please see:\n\nhttp://dot.kde.org/1008567623/1008645287/\n\nI have been waiting for KOffice 1.1.1. Thanks David and KOffice Team :) we appreciate your work."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "while you are pondering why KDE apps don't look or perform as well as you wish they did, please see:\n\nhttp://developer.kde.org/documentation/other/developer-faq.html"
    author: "Aaron J. Seigo"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "Submit a patch!\nPlease!\n\nWe are in need of people to dig into the code and debugify it now, we are ready, everyone get out your gdb and lend us a hand!  Get the cvs, install it, crash it, and patch it.  \n\nif not, stop complaining and go read slushdot or something to shut you up for a while...\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "exactly... another point is that a lot of users say they aren't programmers and therefore can't help. but that's completely false: there is a LOT that non developers can do.\n\nand as i like to practice what i preach, i do submit patches on a rather regular basis ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "hehe ;)"
    author: "ac"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-19
    body: "I would tend to disagree with those observations, you have to remember many people think KDE's design looks nicer than Win2k (that includes myself). There are many things you can do to improve the style though, if you prefer the WinXP look or Win2k look... give dotNet (oh, what is it, it's dot something) or QTWin styles a try. Personally I really like Mosfet's liquid, or the QNiX style.\n\nThe point being there is a style that nearly everyone is going to think is \"pretty.\" You do have a point about the menu bars though...\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-19
    body: "I would tend to disagree with those observations, you have to remember many people think KDE's design looks nicer than Win2k (that includes myself). There are many things you can do to improve the style though, if you prefer the WinXP look or Win2k look... give dotNet (oh, what is it, it's dot something) or QTWin styles a try. Personally I really like Mosfet's liquid, or the QNiX style.\n\nThe point being there is a style that nearly everyone is going to think is \"pretty.\" You do have a point about the menu bars though...\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Special Thanks"
    date: 2001-12-18
    body: "Hey everyone, lets give a round of applause for David Faure, Laurent Montel, Igor Jannsen, Rob Buis, Werner Trobin, Lukas Tinkl and a countless cast of others.  Has anyone ever sat back and remarked how far KOffice has come since it was started?  I mean how long did it take M$ Office or Star Office to get this far?  I mean look how long Applixware flailed along, and that was $100 a copy.\n\nThese people trully do deserve a round of applause, or at least a drink at the local pub.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Special Thanks"
    date: 2001-12-18
    body: "agreed ... kudos to the developers! (and if you are ever up around Calgary, I'll gladly buy you a pint...)"
    author: "Aaron J. Seigo"
  - subject: "Re: Special Thanks"
    date: 2001-12-18
    body: "I can't agree more. KOffice does nearly everything I want, and seems to excel (pun intended) at everything it does. How can you beat it's beautiful interface, I might add...\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "who needs koffice?"
    date: 2001-12-18
    body: "i dunno about koffice, both abiword and gnumeric are shaping up quiet nicely,..even so, we still have open office. Sure you can say its bloated, slow etc etc.. but it works so much better."
    author: "joe99"
  - subject: "Re: who needs koffice?"
    date: 2001-12-18
    body: "Unix needs koffice. if you don't know about koffice (or anything) then please don't flame others."
    author: "Asif Ali Rizwaan"
  - subject: "Re: who needs koffice?"
    date: 2001-12-18
    body: "Uh, that can be extended. Who needs abiword and gnumeric when you have openoffice?\n\nWho needs openoffice when you can run Microsoft Office perfectly on wine?\n\nAnyways, I like koffice because of it's features. I only really use kword, but it's frames are inheriently much more powerful than abiword/starwriter/msword. This is why the pros use adobe framemaker ;)"
    author: "Sashmit Bhaduri"
  - subject: "Re: who needs koffice?"
    date: 2001-12-19
    body: "I look forward to the day abiword has equivalent functionality to at least wordpad, never mind a real word processor.  It starts faster than wordpad ever did, and is beautifully stable.  It just doesn't do very much unless you just want to write a business letter, which covers about 10% of what I need to do and I'm not exactly a big office document guy.\n\nI look forward to the day OpenOffice/StarOffice integrates nicely with both KDE and GNOME.  I use it an awful lot but for christ's sake I can't even cut from a konsole and paste into Starwriter.\n\nAnd I look forward to the day koffice is stable, prints well and imports MS Word documents.  Doing a document in Starwriter (or really, Word itself) feels really constricting to me, but the last time I tried to do a nice multi-frame layout in kword 1.1 (a six-column CD booklet) it cored on me.\n\nMy money's on OpenOffice because right now it seems to have the momentum and is actually usable by real people (who can't afford to say \"don't send me that DOC garbage, I only use free software\" to their paying clients.)  But I will happily switch to KWord when the time comes because I like the DTP model much better than the \"blank piece of paper\" method.  Bit snappier too.  I don't understand why they haven't pulled the document filter code out of Openoffice yet, but I suppose stability should come first."
    author: "raindog"
  - subject: "Re: who needs koffice?"
    date: 2001-12-19
    body: "I look forward to the day abiword has equivalent functionality to at least wordpad, never mind a real word processor.  It starts faster than wordpad ever did, and is beautifully stable.  It just doesn't do very much unless you just want to write a business letter, which covers about 10% of what I need to do and I'm not exactly a big office document guy.\n\nI look forward to the day OpenOffice/StarOffice integrates nicely with both KDE and GNOME.  I use it an awful lot but for christ's sake I can't even cut from a konsole and paste into Starwriter.\n\nAnd I look forward to the day koffice is stable, prints well and imports MS Word documents.  Doing a document in Starwriter (or really, Word itself) feels really constricting to me, but the last time I tried to do a nice multi-frame layout in kword 1.1 (a six-column CD booklet) it cored on me.\n\nMy money's on OpenOffice because right now it seems to have the momentum and is actually usable by real people (who can't afford to say \"don't send me that DOC garbage, I only use free software\" to their paying clients.)  But I will happily switch to KWord when the time comes because I like the DTP model much better than the \"blank piece of paper\" method.  Bit snappier too.  I don't understand why they haven't pulled the document filter code out of Openoffice yet, but I suppose stability should come first."
    author: "raindog"
  - subject: "Re: who needs koffice?"
    date: 2001-12-20
    body: "Of course you can't cut from a console. What do you expect to happen if you cut text from a vi session?\n\nOn the other hand, if you mean copy and paste from a konsole to StarOffice, it does work just fine.\n\nSelect what you want copied in the konsole (of course you can't copy with ctrl-c!), then paste on StarOffice with Ctrl-v. You can't paste in StarOffice with middle-button because StarOffice uses the button for something else.\n\nTested right now, using SO5.2 and KDE 2.2.2.\n\nAnd it really amazes me how people keep on complaining that existing features are not there!\nIf you don't know how to do it, ASK HOW IT IS DONE! Don't assume that it doesn't exist just because you don't know how to do it!"
    author: "Roberto Alsina"
  - subject: "Re: who needs koffice?"
    date: 2001-12-20
    body: "Actually, I was talking KDE 2.1.1 and StarOffice 6 beta.  Select text in the konsole, try pasting in SO6 with the middle button or C-V, and you get either nothing or what was on the SO clipboard.  It is a documented SO6 problem, not a KDE problem.  In case I didn't make it clear enough, it's also the one and only problem I've noticed with that otherwise excellent beta, which is far stabler and full of working features than most actual point releases I've used on the desktop.\n\nI hear now that the clipboard issue has been fixed in the latest builds of OpenOffice.  Next time I have time to download 75 megs I may just grab that (if I don't spend my bandwidth on moving to KDE 2.2.2 so I can assign a keyboard shortcut to \"show desktop.\")  As with Netscape and Mozilla, it may be that OpenOffice ends up being the one that shines."
    author: "raindog"
  - subject: "StarOffice vs. KOffice"
    date: 2001-12-18
    body: "I really like KOffice but IMHO is far away from apllications like StarOfice or Office XP. Is like notepad (for now). You can't really right a medium document on it :(. And the printing is really bad, i mean untill the WYSIWYG is not implemented i don't want to work with koffice."
    author: "Codre Adrian"
  - subject: "Re: StarOffice vs. KOffice"
    date: 2001-12-18
    body: "Let's see, notepad is a non-formatted text editor, Koffice contains a spreadsheet.\n\nNah, I'd just say you don't know what you are talking about."
    author: "Roberto Alsina"
  - subject: "StarOffice is not KOffice"
    date: 2001-12-18
    body: "Imagine How much SUN spent on Research and Development on StarOffice? KOffice is an Open Source effort. If someone could spend 1/4 of the Staroffice's spending on KOffice, you can see the difference. BTW KDE is not a Company, and KOffice is not a product (software meant to be sold) but a creation for everyone using Unix."
    author: "Asif Ali Rizwaan"
  - subject: "Re: StarOffice is not KOffice"
    date: 2001-12-18
    body: "I know and i apreciate the effort on developing koffice but what i want it to say is that koffice is now in a shape that can do almost nothing right: the first thing that should be done in this way is WYSIWYG !!! After is implemented the koficce should be pretty good."
    author: "Codre Adrian"
  - subject: "Re: StarOffice is not KOffice"
    date: 2001-12-18
    body: "*sigh* ok, after a while it gets really tiring to listen to users go on and on and on about the same things when they: A) aren't doing anything to make it better (e.g. coding, creating icons, designing dialogs or other UI elements with Designer, documentation writing, etc) and B) don't have the foggiest idea about what is cooking with regard to development at the moment.\n\ncodre, were you aware that probably the biggest development in KWord in CVS is WYSIWYG printing (thanks David!)?\n\nwith open source you don't have a bunch of marketers giving you ideas and phrases to parrot about: you are left to educate yourself. you can do this by reading mailing lists, community news pieces, or talking with people in the know. i know this is a difficult task to bear, but it's part of the price of Free software. but if you don't know, it might be a good idea not to open one's mouth (unless it's a question or bug report, of course =)"
    author: "Aaron J. Seigo"
  - subject: "open source vs proprblablabla"
    date: 2001-12-18
    body: "This is why I can't see why some \"fanatic\" open-source people feel *all* software *should* be open source. Of course you should be able to pay for finished software that \"just works\" if you want to. Open source should be an alternative to proprietary sw for people who are interested in computers and software. (And that's exactly what it is. ;))"
    author: "Johnny Andersson"
  - subject: "Re: open source vs proprblablabla"
    date: 2001-12-18
    body: "here's a \"humerous\" thought to go along with your reasoning: if the users of open source software paid for its development (or at least defrayed its costs), they would not only get away with a fraction of the cost of proprietary products but would probably get higher quality products in a smaller time frame. as an additional bonus, by virtue of their license those pieces of software would have a guarantee to the continued availability and freedom of that software.\n\nthis is, of course, exactly what happens when you buy a packaged distribution."
    author: "Aaron J. Seigo"
  - subject: "Re: open source vs proprblablabla"
    date: 2001-12-18
    body: "I can't see why that is humerous... perhaps somewhat ironic. ;)  But I can see that it's true.\n\nI guess it all boils down to developers thinking either \"If I do this, KDE will be better, and I will be a part of KDE konquering the world, and I'm even getting paid for it!\", versus \"If I do this, I won't get fired\". ;)"
    author: "Johnny Andersson"
  - subject: "Re: StarOffice is not KOffice"
    date: 2001-12-18
    body: "I doubt that you've used KOffice CVS. Needless to say, KOffice 1.2/2.0 is going to be great!\n\nRemember that staroffice has been around for over 10 years.\nKoffice is progressing very nicely. And the interface is a lot better :)"
    author: "zzz"
  - subject: "Re: StarOffice is not KOffice"
    date: 2001-12-18
    body: "Sun did _not_ develop StarOffice, StarDivision did. Sun _is_ updating it and _did_ Open it (most of it). This was a bright move from Sun, since it makes them more the white knight and provides a terrifying (to Microsoft) competitor to MS-Office.\n\nYes, StarOffice is competing with KOffice, and as a drop-in MS-Office replacement is at the moment much more complete. KWord really lacks polish compared to MS-Word or StarOffice and has (or at least had, in the case of the printing-crushed-together bug, thank goodness that's gone) some showstopper problems.\n\nOTOH, KWord has the right feature set (frames etc) to replace MS-Publisher as well (which StarOffice has as yet no direct answer to even in principle) wihtout a great deal of tweaking. A nice interactive K component for SQL would complete the suite by conceptually replacing MS-Access, and KOffice - large though it is getting - is lightness incarnate when compared with StarOffice.\n\nI know of no other window manager that allows you to drag a track from an *audio* CD and drop it into a sound editor. Making the sound editor a K component would complete the set. That kind of works-the-same-everywhere is Bill's wettest dream for Windows, and KDE is beating him to it. Letting KWord usefully import MS-Publisher files, and usefully export (ie with images etc) HTML would line it up as a competitor for three Microsoft products (Publisher, FrontPage, Word) in one application. KDE already has the kind of network transparency that applications like FrontPage lie awake longing for. (-:\n\nCheers; Leon"
    author: "Leon Brooks"
  - subject: "Re: StarOffice is not KOffice"
    date: 2001-12-18
    body: "staroffice (or is it openoffice) does frames on my machine."
    author: "ik"
  - subject: "Re: StarOffice is not KOffice"
    date: 2001-12-18
    body: "It's not the same kind of frame. He was talking about kword/FrameMaker/Publisher-like frames, which are much, much, much, more powerful. StarWriter could not handle this without a massive rewrite."
    author: "565"
  - subject: "Re: StarOffice is not KOffice"
    date: 2001-12-18
    body: "And now you thnink only because SO is done by a big company it must be much better than the free software project?? Give me a break. If this was true Linux wouldn't have been around after all. I actually like KOffice very much and I think it has a lot of potential, it just needs more time to mature. And I also think tons of features in SO and OfficeXP aren't even needed so KOffice can compete very well."
    author: "Stephan Oehlert"
  - subject: "Re: StarOffice is not KOffice"
    date: 2001-12-18
    body: "Sun did _not_ develop StarOffice, StarDivision did. Sun _is_ updating it and _did_ Open it (most of it). This was a bright move from Sun, since it makes them more the white knight and provides a terrifying (to Microsoft) competitor to MS-Office.\n\nYes, StarOffice is competing with KOffice, and as a drop-in MS-Office replacement is at the moment much more complete. KWord really lacks polish compared to MS-Word or StarOffice and has (or at least had, in the case of the printing-crushed-together bug, thank goodness that's gone) some showstopper problems.\n\nOTOH, KWord has the right feature set (frames etc) to replace MS-Publisher as well (which StarOffice has as yet no direct answer to even in principle) wihtout a great deal of tweaking. A nice interactive K component for SQL would complete the suite by conceptually replacing MS-Access, and KOffice - large though it is getting - is lightness incarnate when compared with StarOffice.\n\nI know of no other window manager that allows you to drag a track from an *audio* CD and drop it into a sound editor. Making the sound editor a K component would complete the set. That kind of works-the-same-everywhere is Bill's wettest dream for Windows, and KDE is beating him to it. Letting KWord usefully import MS-Publisher files, and usefully export (ie with images etc) HTML would line it up as a competitor for three Microsoft products (Publisher, FrontPage, Word) in one application. KDE already has the kind of network transparency that applications like FrontPage lie awake longing for. (-:\n\nCheers; Leon"
    author: "Leon Brooks"
  - subject: "Re: StarOffice is not KOffice"
    date: 2001-12-18
    body: "And now you thnink only because SO is done by a big company it must be much better than the free software project?? Give me a break. If this was true Linux wouldn't have been around after all. I actually like KOffice very much and I think it has a lot of potential, it just needs more time to mature. And I also think tons of features in SO and OfficeXP aren't even needed so KOffice can compete very well."
    author: "Stephan Oehlert"
  - subject: "Re: StarOffice is not KOffice"
    date: 2001-12-18
    body: "And now you thnink only because SO is done by a big company it must be much better than the free software project?? Give me a break. If this was true Linux wouldn't have been around after all. I actually like KOffice very much and I think it has a lot of potential, it just needs more time to mature. And I also think tons of features in SO and OfficeXP aren't even needed so KOffice can compete very well."
    author: "Stephan Oehlert"
  - subject: "Re: StarOffice is not KOffice"
    date: 2001-12-18
    body: "Sun did _not_ develop StarOffice, StarDivision did. Sun _is_ updating it and _did_ Open it (most of it). This was a bright move from Sun, since it makes them more the white knight and provides a terrifying (to Microsoft) competitor to MS-Office.\n\nYes, StarOffice is competing with KOffice, and as a drop-in MS-Office replacement is at the moment much more complete. KWord really lacks polish compared to MS-Word or StarOffice and has (or at least had, in the case of the printing-crushed-together bug, thank goodness that's gone) some showstopper problems.\n\nOTOH, KWord has the right feature set (frames etc) to replace MS-Publisher as well (which StarOffice has as yet no direct answer to even in principle) wihtout a great deal of tweaking. A nice interactive K component for SQL would complete the suite by conceptually replacing MS-Access, and KOffice - large though it is getting - is lightness incarnate when compared with StarOffice.\n\nI know of no other window manager that allows you to drag a track from an *audio* CD and drop it into a sound editor. Making the sound editor a K component would complete the set. That kind of works-the-same-everywhere is Bill's wettest dream for Windows, and KDE is beating him to it. Letting KWord usefully import MS-Publisher files, and usefully export (ie with images etc) HTML would line it up as a competitor for three Microsoft products (Publisher, FrontPage, Word) in one application. KDE already has the kind of network transparency that applications like FrontPage lie awake longing for. (-:\n\nCheers; Leon"
    author: "Leon Brooks"
  - subject: "Re: StarOffice vs. KOffice"
    date: 2001-12-18
    body: "koffice is more like an extended version of wordpad and old version of Works."
    author: "joe99"
  - subject: "Re: StarOffice vs. KOffice"
    date: 2001-12-18
    body: "You never really worked with KOffice and Wordpad, did you?"
    author: "Vajsravana"
  - subject: "Why not use Star Office Filters (OT)"
    date: 2001-12-18
    body: "Why don't we just focus on a filter to import and export SO docs.\nIf we had that we could just use the Star Office filters to export and import to any format that SO supports.\n\nImports would be easy:\nWord(or whatever SO supports) -> SO -> KOffice\n\nSo would exports\nKOffice -> SO -> Any file format that Star Office Exports.\n\nCould someone please explain why we have to have our own filters and keep \nreinventing the wheel, when Sun obviously has a large interest in having\nSO be completely compatible with MS Office? I'm confused"
    author: "ac"
  - subject: "Re: Why not use Star Office Filters (OT)"
    date: 2001-12-19
    body: "Because, then you'd have to pull up StarOffice to load your MS documents! That\nwould be a total PITA. Of course, KDE could also port SO's filters over, but, wait,\nTHAT'S ALREADY HAPPENING! (sorry for the caps folks, but this time it's needed).\n\nKDE isn't reinventing the wheel, the problem is that the MS file format is a changing,\nhighly proprietary, badly documented format. The big issue, afaics, is trying to figure\nthe damn thing out in the first place, rather then writing the code to convert it to \nanother format."
    author: "Carbon"
  - subject: "Re: Why not use Star Office Filters (OT)"
    date: 2001-12-19
    body: "> If we had that we could just use the Star Office filters\n< to export and import to any format that SO supports.\n\nSure sounds like a good idea.  Where's the code?(tm)\n\n> Could someone please explain why we have to have our own\n> filters and keep reinventing the wheel, when Sun obviously\n> has a large interest in having SO be completely compatible\n> with MS Office? I'm confused\n\nThere's really nothing to be confused about.\n\nIt goes like this.\n\nSomeone (apparently not you) is actually writing code for KOffice.  That person wants to write their own filters.  Who knows why?  It obviously is not someone being paid by you, so they can spend their freetime how they choose, right?\n\nSo please stop criticizing people that actually do something.  \n\nIf you want SO office filters to work, start a project to port them (probably not that hard, I think they are separate plugins, at least it looks that way in the filters subdir of the OO install) and submit them to the KOffice team.  In the (very unlikely) event they don't get accepted by the KOffice team, if they are good the distros will include them anyway."
    author: "Sage"
  - subject: "Re: Why not use Star Office Filters (OT)"
    date: 2001-12-19
    body: "Not a criticism at all, just a question based on a quick glance at the KOffice filter web page.\n\nBut I guess next time before I post I'll have to check out the CVS and subscribe to the koffice-devel..."
    author: "ac"
  - subject: "Re: Why not use Star Office Filters (OT)"
    date: 2001-12-19
    body: "Not a criticism at all, just a question based on a quick glance at the KOffice filter web page.\n\nBut I guess next time before I post I'll have to check out the CVS and subscribe to the koffice-devel..."
    author: "ac"
  - subject: "Re: Why not use Star Office Filters (OT)"
    date: 2001-12-20
    body: "Right now I'm developing a new filter architecture for KOffice, which allows to use more than one filter for a conversion (I called them \"filter chains\"). Due to that we will be able to use non-KOffice filters from KOffice. StarOffice, e.g. supports batchmode conversion, which is exactly what we need. However, this stuff is quite tricky to implement -- stay tuned :)"
    author: "Werner Trobin"
  - subject: "Re: StarOffice vs. KOffice"
    date: 2001-12-19
    body: "Printing is bad because your machine is not configured right and Qt-based applications have no hint where the fonts might be :-(. Screen based fonts are provided by the fonts server fine, but it cannot download them to the printer - it needs to know the path.\n\nDo it right - at least for me it works perfectly. This has nothing to do with KOffice, all Qt applications including konqueror printing will suffer more-or-less the same. Do you happen to use otherwise-great RedHat's distribution?"
    author: "Kuba Ober"
  - subject: "Where's Krayon??"
    date: 2001-12-18
    body: "According to all of the announcements I read, Krayon was supposed to be included in\nthis release.  However, I downloaded the source and did not see it??\n\nAnyone have any information??\n\nMuch appreciation."
    author: "O37"
  - subject: "Re: Where's Krayon??"
    date: 2001-12-20
    body: "Krayon is far too unstable and unfinished to be released.\nIt has been removed from the tarballs before making them public.\n\nIf you - or anyone listening here - feel like contributing to an image editing application for KOffice... please feel free to come and hack Krayon ! ;-)"
    author: "David Faure"
  - subject: "Andreas (or dot administrator) please..."
    date: 2001-12-18
    body: "answer is it possible auto remove duplicate or triplicate posts/replies from the dot? mostly clicking back and submit button once more time will post it (post/reply) again!"
    author: "Asif Ali Rizwaan"
  - subject: "Andreas (or dot administrator) please..."
    date: 2001-12-18
    body: "answer is it possible auto remove duplicate or triplicate posts/replies from the dot? mostly clicking back and add button once more time will post it (post/reply) again!"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Andreas (or dot administrator) please..."
    date: 2001-12-18
    body: "Actually, this is an annoying enough problem that I can probably just fix it myself, \nif I happen to know the language it's written in. Hey, site admin (sorry, forgot the name), \nwhere is CVS/non-CVS access to the site, and what lang is it in?"
    author: "Carbon"
  - subject: "Re: Andreas (or dot administrator) please..."
    date: 2001-12-18
    body: "The DOT is a Zope site written in Python. Actually, it is a Squishdot application written in Python built on top of Zope. Without knowing what version of Sqish they use, I can't say if they need to either patch the addPosting method or just upgrade to the most recnet version.\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Andreas (or dot administrator) please..."
    date: 2001-12-19
    body: "That's too bad, because Python is one of the language I don't know yet, sorry :-\\.\n\nHowever, if someone does know Python, it would just be a matter of putting\nthis in the article posting code.\n----\nforeach existing_article (already_posted_article_in_this_thread) {\n\tif (\n\t\t(existing_article.subject == new_article.subject) &&\n\t\t(existing_article.author == new_article.author) &&\n\t\t(existing_article.body == new_article.body)\n\t) dont_post_article;\n}\npost_article; #Only reaches here if no other article matched.\n----\n\nUnless Zope or Python is even weirder then I think it is, that shouldn't be too hard to\ncode up. There's really no legitamite reason why someone would want to\npost two identical messages to the same thread. There's also no reason to display\nan error message, as all the poster wanted was to post one message. If the message\nwas posted at least once, then it worked, and there is no problem."
    author: "Carbon"
  - subject: "Re: Andreas (or dot administrator) please..."
    date: 2001-12-19
    body: "Very true. Maybe if we find out from Dre what version of Squish he and Nav are using we can check for a patch. What Squish currently does is convert the time using \"str(int(ZopeTime))\" that returns an ID of the seconds since the Epoch. So you could theoretically post the same piece every second without it being checked for duplicates. The most recent version may fix this, however. \n\nEron"
    author: "Eron Lloyd"
  - subject: "How are the filters doing"
    date: 2001-12-18
    body: "it doesn't look like there are any new or updated export import filters for Koffice 1.1.1 Any information what to expect? Decent .rtf export/import and what about .xls?\n\nI always use Koffice for all my work I'm sure I never have to export to MS like letters etc. I'd like to use it for my specification documents that go to MS-Users as well (especially that is where SO's hunger for resources hurts most)\n\nConrad"
    author: "Conrad"
  - subject: "Why not use StarOffice format/filters"
    date: 2001-12-18
    body: "Why don't we just focus on a filter to import and export SO docs.\nIf we had that we could just use the Star Office filters to export and import to any format that SO supports.\n\nImports would be easy:\nWord(or whatever SO supports) -> SO -> KOffice\n\nSo would exports\nKOffice -> SO -> Any file format that Star Office Exports.\n\nCould someone please explain why we have to have our own filters and keep \nreinventing the wheel, when Sun obviously has a large interest in having\nSO be completely compatible with MS Office?  I'm confused"
    author: "ac"
  - subject: "Re: Why not use StarOffice format/filters"
    date: 2001-12-19
    body: "well - it should be easier. The problem is said to be the cryptic MS formats. Why not spy with SO (Openoffice) to write a decent rtf filter. Has anybody tried this.\n\nConrad"
    author: "Conrad"
  - subject: "Re: Why not use StarOffice format/filters"
    date: 2001-12-19
    body: "spy???\n\nWhat I'm saying is make sure you can import and export to SO.  Once you can do that you can just take advantage of their filters, which work quite well.\n\nAll of this would be transparent to the user.  If they try to open a .doc file, then KOffice would use the SO filter to import it as an SO document, then KOffice would convert that SO document to a kwd doc (all of this is transparent, behind the scenes).  When you go to save the document as a .doc file, KOffice converts it to SO format, and then to .doc using the SO filter (again behind the scenes).\n\nIt seems to me that if you just get the import/export SO capabilities you would be pretty much set.  Rather than SO, Abiword, and KOffice each doing their own attempt at .doc import/export filters, take advantage of the work that SO has done.  Why re-invent the wheel?  Plus the SO format is open, XML, anyone can read it rather than some binary closed format."
    author: "ac"
  - subject: "Re: Why not use StarOffice format/filters"
    date: 2001-12-19
    body: "Actually, this is being worked on right now. Some of the results are already in KO CVS/will be in KO 1.2. However, we are directly porting the SO filters. It'd not terribly trivial, but much easier than reversing Microsoft formats."
    author: "gt"
  - subject: "Re: Why not use StarOffice format/filters"
    date: 2001-12-19
    body: "sounds pretty cool!\nbut what happens when openoffice updates their filters? will the whoel thing then have to be done again, or is it just a few clicks and scripts that have to be run?\nin other words: will koffice's filters automatically improve as the openoffice-flters get better?"
    author: "Johannes Wilm"
  - subject: "Re: Why not use StarOffice format/filters"
    date: 2001-12-20
    body: "We plan to use the binary filters. If the OO filters get better we can use the new ones, even without a recompile needed. So much for the plans... ;)"
    author: "Werner Trobin"
  - subject: "Re: Why not use StarOffice format/filters"
    date: 2001-12-20
    body: "I think this is a great idea.\nStarOffice/OpenOffice has done a tremendous amount of work getting their in/out filters to a very strong point. I think having native filters is cool, but it means that there is going to be a while before we can get them as good as the filters in StarOffice/OpenOffice. The hard work is done. If KWord had a really good set of StarOffice/OpenOffice in/out filters, it could easily make use of the  EXSISTING filters created for StarOffice/OpenOffice. Then effort could be spent working on one really good strong set of filters (ANYTHING to/from StarOffice/OpenOffice) that would benefit users of both StarOffice/OpenOffice and KWord and anthing else that can read/write StarOffice/OpenOffice format. \n\nI guess the major advantage to going native, is that once it matures, some things may translate better from KWord to MSWord than KWord to StarOffice/OpenOffice to MSWord, but I don't know any of the products well enough to purpose what kind of things would fit this category."
    author: "porter235"
  - subject: "Templates in KWord"
    date: 2001-12-18
    body: "Does anyone know a site where we can share/get KWord-Templates? This would help a lot."
    author: "Frank"
  - subject: "Re: Templates in KWord"
    date: 2001-12-18
    body: "Try the templates at http://users.skynet.be/bk369046/template.htm under Ktemplate.I found I had to install in my home directory under .kde2/share/apps/kword/templates (I'm using SuSE 7.3)"
    author: "Craig Nelson"
  - subject: "Re: Templates in KWord"
    date: 2001-12-18
    body: "Great!!! Thank You."
    author: "Frank"
  - subject: "Re: Templates in KWord"
    date: 2001-12-18
    body: "Also see the sourceforge link = http://sourceforge.net/projects/ktemplate"
    author: "Craig Nelson"
  - subject: "Great Work -- screw the idiots"
    date: 2001-12-18
    body: "I recently tried out KWrite and was simply amazed at how far you folks have come.  Great work!!  Sure it's got some issues, but what software doesn't?  \n\nThere will always be those idiots who complain about this and that, without even giving things a fair shake.. I say fuck em.  They're not important."
    author: "Lally Singh"
  - subject: "Re: Great Work -- screw the idiots"
    date: 2001-12-18
    body: "you mean kword? ;o"
    author: "565"
  - subject: "congratulations all round"
    date: 2001-12-18
    body: "Kword is really coming on nicely. I haven't fully tested 1.1.1 yet but still the progress is gratifying. Kword is much more than wordpad, even though some features are not yet implemented.\n\nI can remember only a short time ago when kword was completely unusable, now it is good for many tasks. I'm really looking forward to 1.2\n\nStarOffice has alot of advatages at the moment but kword has nifty features that SO does not and even openoffice is quite demanding on system resources."
    author: "Craig Nelson"
  - subject: "Documentation for image-insertion?"
    date: 2001-12-18
    body: "Hi,\nWow, first of all, I'd like to thank you guyz for the work on KOffice. Of course it's KWord that got my attention. I fiddled around with it, and was really surprised by the absence of crashs. So, even I'm not a developer, sometimes I get hold of a crash and can fix it, but here I didn't get the opportunity! God bless...\n Is there some documentation around? I have a big question regarding the insertion of picture-files. I'd like to insert a .eps-file in a KWord-document, but the file is inserted as bitmap, and thus pretty nasty to look at! If you could point me to a documentation I will gladly look up the answer there. But I didn't find any.\n\ngreets and kudos (where does this word come from?)\n\nIneiti"
    author: "Linus Gasser"
  - subject: "Re: Documentation for image-insertion?"
    date: 2001-12-18
    body: "Usually, at least in other packages, the bitmap is used for display on screen, and the actual postscript is used during printing.  How does the document look when the device is printed?\n\nCould someone let me know if this isn't the way that kword works.\n\nI'll be looking forward to trying kword again sometime soon - I'm just being spoiled with frame at the moment, and I didn't want to risk a thesis in kword when I started...\n\nCheers\n\nWill\n  --"
    author: "Will"
  - subject: "Re: Documentation for image-insertion?"
    date: 2001-12-19
    body: "Hi,\nno, it doesn't show up nicely in the printed version. On the screen it looks quite nice (not with 400% though), but as soon as I print it, it's not readable anymore. And, yes, the original .eps is readable!\n\nGreets\n\nineiti"
    author: "Linus Gasser"
  - subject: "Re: Documentation for image-insertion?"
    date: 2001-12-18
    body: "kudos comes from the greek word for glory: kydos."
    author: "Aaron J. Seigo"
  - subject: "Re: Documentation for image-insertion?"
    date: 2001-12-19
    body: "Ok, I am Greek, but this is the first time I have ever heard of this word. Glory in greek is \"thoksa\" (tho- as in \"though\"). Could you please tell me where you got your information from? Maybe it is a long-forgotten word from ancient greek."
    author: "Georgios E. Kylafas"
  - subject: "Re: Documentation for image-insertion?"
    date: 2001-12-19
    body: "This is a bit off topic; or maybe it's the i18 thing!\n\nAnyway, I know nothing about modern Greek, but Koine Greek (the language of the New Testament) has the word 'doxa' for glory, which finds its way into English in words such as 'doxology'.\n\nOh, and KDE + KOffice in phenomenal, which is another English word from Greek, and it's mega, etc..."
    author: "cadmus"
  - subject: "Re: Documentation for image-insertion?"
    date: 2001-12-19
    body: "it isn't modern greek. i dug this link up for you on google:\n\nhttp://www.bartleby.com/61/27/K0112700.html"
    author: "Aaron J. Seigo"
  - subject: "KWord Templates"
    date: 2001-12-18
    body: "Hi all,\n\nA while ago Kernel Cousin KDE #13 - http://dot.kde.org/992464961/  mentioned that someone was working on Avery label templates for KWord.<p>\nOther than the screenshots listed there's no additional information about the templates.  I've looked around but have not been able to find any download or availability information.  Does anyone know when these templates are going to be released?"
    author: "SD"
  - subject: "Re: KWord Templates"
    date: 2001-12-19
    body: "perhaps an email to the fellow putting the templates together would be a good way to get an answer to your question?"
    author: "Aaron J. Seigo"
  - subject: "printing problem"
    date: 2001-12-19
    body: "Thanks for the awesome work in KOffice!\nHowever, there is a problem and I'd be glad if someone could give me a hint:\n\nMy KDE locale is iso8859-1, German. I want to use KOffice to edit ancient Hebrew texts, which is iso 8859-9 afaik. I can do that, but when I try to print I only see ??? characters, which probably indicates that there is some problem with Unicode at the printing. \n\nIs this my fault? Any help is appreciated."
    author: "Martin Gruner"
  - subject: "Re: printing problem"
    date: 2001-12-20
    body: "Select the Hebrew text, go to Format / Font, and\nin the font dialog, select the iso-8859-9 encoding,\nto apply it to the selected text. Although this changes\nnothing on screen, usually, it helps printing such text.\n\nSuch workarounds will not be needed anymore with Qt 3."
    author: "David Faure"
  - subject: "Diffs for source"
    date: 2001-12-20
    body: "A source diff can be found at http://www.hmallett.co.uk/diffs/\n\nHywel"
    author: "Hywel Mallett"
---
The KOffice project today announced the release of KOffice 1.1.1.
KOffice is a free, Open Source, integrated office
suite demonstrating the richness and power of the KDE development environment.
The release, out less than
three months after the long-anticipated KOffice 1.1 hit the KDE ftp
servers, mainly improves performance, printing (particularly in
<a href="http://www.koffice.org/kword/">KWord</a>), and stability.
The <a href="http://www.koffice.org/announcements/announce-1.1.1.phtml">announcement</a>
contains links to the source and a number of binary packages, as well as
a summary of the changes.  A more
<a href="http://www.koffice.org/announcements/changelog-1.1.1.phtml">detailed
changelog</a> is available at the
<a href="http://www.koffice.org/">KOffice website</a>.  The next release will
likely be released shortly after KDE 3.0, sometime in the first quarter of 2002.
<strong>Update Tuesday December 18, @11:42 pm</strong>:  <a href="tbutler@uninetsolutions.com">Timothy Butler</a> wrote in to tell us that
<em><a href="http://www.ofb.biz/">Open for Business</a> is running a <a href="http://www.ofb.biz/article.php?sid=50&mode=&order=0">mini-review</a> of KOffice, and it's readiness for enterprise deployment.</em>
<!--break-->
