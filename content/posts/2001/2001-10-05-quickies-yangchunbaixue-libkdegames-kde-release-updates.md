---
title: "Quickies: Yangchunbaixue, libkdegames, KDE Release Updates"
date:    2001-10-05
authors:
  - "numanee"
slug:    quickies-yangchunbaixue-libkdegames-kde-release-updates
comments:
  - subject: "ZDnet can't get their facts right"
    date: 2001-10-04
    body: "\"Trolltech licensed [Qt] under a different public license (BSD-type public license) than the Free Software Foundation's (GNU Project General Public License [GPL]) used for Linux.\"\n\nThis could cause some confusion for anyone reading about this for the first time on ZDnet.  QPL is not BSD-style.\n\nWhat?  GNOME folks are unhappy that people can develop closed-source apps with this \"BSD Qt\" so they create their own LGPL widget set?  LOL\n\nA bit of a clueless statement on the part of ZDnet."
    author: "Justin"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "> A bit of a clueless statement on the part of ZDnet.\n\nWell, of course! This is ZDNet, remember?\n\nUseless bashing apart, the article has many problems, some technical, some trivial, and most because they're biased towards GNOME. \"GNOME is rapidly catching up\" is one of those examples. (Note that I don't say it's false per se) No basis for their statements, no sources, no nothing. \n\nGNOME is catching up? Yes, of course it is. It suddenly has adopted arTs, has managed to cool down from its \"We're LGPL'ed and better than you\" position and has been told humility in the most direct way: It's been badly beaten in terms of quality, and has lost its licensing 'superiority'. Eazel has gone under, with its only product being criticized violently across the board. Ximian still shows no sign of anything save for going the Microsoft way and outputing a PIM(P).\n\nGNOME is catching up? Yes. Is there anything else it did? Well, it was probably the main reason behind GPL'ing Qt. Has GNOME outlived its usefulness? Yes.\n\nBut you won't see that in ZDNet."
    author: "Anonymous"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: ">GNOME is catching up?\n\nNo, GNOME is already ahead in everything but the final bit of polish on the desktop - something that GNOME 2.0 will bring.\n\nAs for aRTS... it's not part of GNOME, and probably never will be. Who wants something so complex, bloated and full of horrible latency? There is no official GNOME sound system, it's not considered an important thing (and before you start screaming about that, try thinking it through)."
    author: "Wiggle"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "* GNOME is already ahead in everything\n\nComgratulations, you substantiate your opinion even less than ZDNet."
    author: "Roberto Alsina"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: ">Comgratulations, you substantiate your opinion even less than ZDNet.\n\nNo, congratulations to you. You selectively edit extremely well..."
    author: "Wiggle"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "\"Selectively edit\" as opposed to what? accidental edition?"
    author: "Roberto Alsina"
  - subject: "Do not feed the trolls, please"
    date: 2001-10-05
    body: "For all of you who didn't read the huge \"discussion\" about QCom:\n\nWiggle is a troll.\n\nA troll is a person who dumps shit in debates for the only pupose of disrupting the discussion. The only thing we can do about such persons, is to ignore them. Then they will go away.\n\nTherefore, please ignore Wiggle and his so called \"opinions\"."
    author: "Niftie"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "Perhaps dot kde should implement a login system, similar to freekde.\n\nWe've got to do something about trolls before we get like Slashdot. Plus, as it stands, anyone can adopt anyone's name and email, which can seriously cause flamewars."
    author: "Carbon"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "A site needs a fairly large dedicated readership before it can start requiring memberships to post.  If not enough people register, the discussions die off and the site goes downhill.  I don't know if the Dot's readership is high enough to make a required membership system work.\n\nPlus, usually making an account is a matter of minutes and it won't stop the more determined trolls.  The only permanent solution is a Slashdot-like moderation system, which I would hate to see the Dot have to adopt.  I like the approach that's been taken so far."
    author: "not me"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "Yes! I would like to see a login system on \"The Dot\". I don't want to enter my name each time I want to post anything.\n\nIt would be cool if there was an option to NOT display anonymous posts and/or even more cool, be able to hide posts on a per user basis.\n\nAs an example, I should be able to hide/ignore all posts from user \"Wiggle\". That would make the page load 10 times as fast too :)"
    author: "Joergen Ramskov"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "You shouldn't need to enter your name each time you want to post -- cookies should take of that.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: ">Perhaps dot kde should implement a login system, similar to freekde.\n>We've got to do something about trolls before we get like Slashdot. \n\nYeah, and look how successful slashdot logins are at stopping trolls!"
    author: "Wiggle"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "They are not supposed to stop them, just make them invisible."
    author: "Flagg"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "You don't need a user system to moderate posts."
    author: "Wiggle"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "Translation : \"Yes, don't try this! It certainly won't stop me. There is no need to try this method at all!\""
    author: "Carbon"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "Stop trolling."
    author: "Evandro"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "Sorry, but I just found it kind of amusing that in a discussion about how to stop trolling, the guy who had trolled in the first place was talking about how a particular method wouldn't stop trolls. Kind of M.C. Esher-ish."
    author: "Graphite"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "I posted a suggestion about this on the GNOME website a while ago, after someone started posting comments under Miguel de Icaza's name talking about the wonders of Qt (a bit of an extreme case, I must say).\n\nIt would be best to have a partial login system. In this way, people could register and that name and email would become reserved, so people could not post under a false name. However, all other names and emails would be allowed to post in the current way.\n\nIn this way, registration could be limited only to developers, etc. and the post header could be a different colour to notify people that that comment was from a credible source.\n\nConstantine Karastamatis"
    author: "C. Evans"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "hey guys calm down ... apart from some articles, we dont really have a troll problem \non the dot. only once upon a time something bad happens, but it doesn't really disturb\nthe normal discussion. So the current policy of adding '[troll]' to the subject or remoiving the posts\n(with a 'troll removed' message) seems perfect to me ..."
    author: "ik"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "The QCom discussion is the QCom discussion and it's not in this thread.\n\nIf what Wiggle posted in this thread is trolling, then a guy trolled against GNOME before him. Is that allowed?"
    author: "Evandro"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "No, the guy before him made a point of being reasonable about the topic. A troll isn't just someone who posts about liking GNOME on dot.kde. It has to do with the fact that he's obviously not trying to contribute to the discussion, but from his lack of evidence and insults, is just making a point of trying to piss people off."
    author: "Carbon"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "Oh really? There was nothing reasonable in the first message, nor was there any evidence. It was rabidly pro-KDE and rabidly anti-GNOME - which is the \"done thing\" around here, apparently."
    author: "Wiggle"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "There was evidence. Mostly, it was not about being anti-GNOME, but rather talking about how ZDNet did not use any evidence. The only anti-GNOME thing I noticed was the part at the end, where he said it had outlived it's usefullness, which I would say is an opinion, not a fact. Somewhat opionated, but I would say it's not trolling. \n\nOn the other hand, saying things like GNOME will not use aRts because it's blah blah blah is attempting to pass of your opinion as fact. Not to mention being off-topic."
    author: "Graphite"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: ">The only anti-GNOME thing I noticed was the part at the end, \n>where he said it had outlived it's usefullness,\n\nOh, is that all... no, of course that's not trolling! Not to mention he supplied absolutely no evidence for that.\n\n>On the other hand, saying things like GNOME will not use aRts \n>because it's blah blah blah is attempting to pass of\n>your opinion as fact. Not to mention being off-topic.\n\nNo, I said that aRTS will not be used because it is bloated, slow and has horrible latency. \"bloated\" - an opinion, slow is not and neither is \"horrible latency\" - as you will find if you do a bit of checking.\n\nI also supplied information stating that GNOME has no standard sound system, since the developers happen to believe it is simply not necessary. Quite how this adds up to no evidence is a bit of a mystery, since it contains consierably more detail than most pro-KDE posts... and OFF-TOPIC? Excuse me, but the original post was about aRTS use in GNOME!\n\nYou know those peril-senstive sungless in The Hitchhikers Guide To The Galaxy? Do dot.kde.org users wear GNOME-fact sensitive ones... it sure seems like it."
    author: "Wiggle"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-05
    body: "For someone, who likes GNOME so much, you do spend a significant amount of time here? Do you also visit www.microsoft.com regularly?\n\nJust wondering..."
    author: "Marko Samastur"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-06
    body: "As a matter of fact, yes... microsoft.com contains a great deal of useful information.\n\nAnyone who does serious software development runs up against Microsoft software at some point. So if you are too ideologically pure to suffer any contact with their website, you have a lot of problems.\n\nWhy do you ask? Is it some bizarre notion of territory... or some completely idiotic idea like that?"
    author: "Wiggle"
  - subject: "Re: Do not feed the trolls, please"
    date: 2001-10-06
    body: "This may come as a shocking surprise to you, but some of us do have jobs that are completely unrelated to Microsoft's software and therefore NEVER get in contact with them. Your answer though suggests that you have misfortune of being forced to deal with KDE or was that comment just a smoke screen?\n\nBut it's very hard to take you seriously and regard you as anything but provocateur, if you can't answer a simple question without being offensive.\n\nAnd if you really want to know, I asked because I don't really get the idea of spending my time on community sites of software I dislike."
    author: "Marko Samastur"
  - subject: "Re: Do not feed the trolls, please"
    date: 2006-05-10
    body: "I WILL TROLL THIS SITE TO FUCK\n"
    author: "TROLL"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "Here is a nice article about the new gnome:\n\nhttp://www.theregister.co.uk/content/4/22025.html\n\nI particurarly like this quote:\n\n\"The software is accompanied by a note warning that this initial Alpha build \"does not include anything of use to end users,\" which at least makes it consistent with all the previous versions of GNOME Desktop we've used.\" \n\nThe world have found consensus."
    author: "reihal"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "I don't see any troll warnings attached to this post!\n\nCould it be... yes I think it could... that you are a bunch of hypocrites."
    author: "Wiggle"
  - subject: "fair warning"
    date: 2001-10-05
    body: "Wiggle, however funny some people may find you, dot.kde.org is not your platform for gratuitous flamefests.  Consider this fair warning before we take any actual measures.\n\nAs to the person imitating Miguel, please consider yourself warned too.\n\nLater,\n-N."
    author: "Navindra Umanee"
  - subject: "Re: fair warning (my arse)"
    date: 2001-10-05
    body: ">however funny some people may find you, dot.kde.org \n>is not your platform for gratuitous flamefests\n\nExcuse me? The first time I posted here I was called an idiot (and a number of times after that), simply for having an opinion your cronies didn't like. Roberto Alsina and friend also called me an asshole. Are you surprised that I reacted in a robust manner (silly question really, but I have to ask).\n\nWill you be taking action against those people? I doubt it somehow. So quite frankly, you attaching a warning to my post calling dot.kde.org a bunch of hypocrites is rather amusing."
    author: "Wiggle"
  - subject: "Re: fair warning (my arse)"
    date: 2001-10-05
    body: "> Roberto Alsina and friend also called me an asshole\nAFAICR, I was the only one who called you an asshle, kid.\n\n> Are you surprised that I reacted in a robust manner \nYou were already acting in a robustely annoying manner before I replied.\nThat is why I called you an asshole.\n\nBelieve it or not, I don't call people assholes if I don't believe their actions make them deserving of that label, asshole."
    author: "Roberto Alsina"
  - subject: "Re: fair warning (my arse)"
    date: 2001-10-06
    body: ">AFAICR, I was the only one who called you an asshle, kid.\n\nWrong, chum. Perhaps you'd like to go back a read it again... carefully this time.\n\n>You were already acting in a robustely annoying manner \n>before I replied. That is why I called you an asshole.\n\nBecause I was annoying you. Oh how precious... thank you for proving my point.\n\n>Believe it or not, I don't call people assholes if I don't\n>believe their actions make them deserving of that label,\n>asshole.\n\nOh I believe you. It's just that your definition of deserving is a little different from most people. Something along the lines of: \"Doesn't think the sun shines out of TrollTech headquaters.\""
    author: "Wiggle"
  - subject: "Re: fair warning (my arse)"
    date: 2001-10-06
    body: "> Wrong, chum. Perhaps you'd like to go back a read it again... carefully this \n> time.\n\nIn case you have not noticed, those posts are not there anymore. That is why\nI wrote \"AFAICR\".\n\n> Because I was annoying you. Oh how precious... thank you for proving my \n> point.\n\nWell, yes, indeed I tend to insult a larger percentage of those who annoy me, as compared to those who don't annoy me. Do you find that peculiar?\n\n> Oh I believe you. It's just that your definition of deserving is a little different \n> from most people.\n\nWell, you just said I was not the only one. Wanna make a poll?\nIf a man says you are a donkey, you are not a donkey.\nIf three men tell you you are a donkey, check the length of your ears.\n\nYou are one donkey away, pal."
    author: "Roberto Alsina"
  - subject: "Re: fair warning (my arse)"
    date: 2001-10-06
    body: ">In case you have not noticed, those posts are not there \n>anymore. That is why I wrote \"AFAICR\".\n\nNo, I hadn't noticed - and you seem to have a problem making yourself clear. \"AFAICR\" indeed.\n\n> Because I was annoying you. Oh how precious... thank you for proving my \n> point.\n\n>Well, yes, indeed I tend to insult a larger percentage of those\n>who annoy me, as compared to those who don't annoy me. Do you \n>find that peculiar?\n\nI find it precious that you behave in exactly the way you criticise others for. \nEvery message from you just further confirms that you lack the sense most people\nare born with.\n\n>Well, you just said I was not the only one. Wanna make a poll?\n\nWhat would that prove?\n\n>If a man says you are a donkey, you are not a donkey.\n>If three men tell you you are a donkey, check the length of your ears.\n\nYou are fond of getting all philosophical aintcha?\n\nIf I ask a thousands Christian fundamentalists whether God greated the Earth\nin six days, what would their answer be? \n\nYou now have you answer\n\n>You are one donkey away, pal.\n\nAnd you are a rampaging ignoramus - and you were told so by half a dozen different people (that I can directly recall) on Linux Today. So there. Nyah, nyah. You're it. And all that.\n\nBTW: the software used on this board is appalling."
    author: "Wiggle"
  - subject: "Re: fair warning (my arse)"
    date: 2001-10-06
    body: ">>In case you have not noticed, those posts are not there \n>>anymore. That is why I wrote \"AFAICR\".\n> No, I hadn't noticed - and you seem to have a problem making yourself \n> clear. \"AFAICR\" indeed.\n\nYou would have noticed, if you had followed your own advice. And sorry if you don't understand what AFAICR means. Everyone was an internet newbie once.\n\n> I find it precious that you behave in exactly the way you criticise others for. \n> Every message from you just further confirms that you lack the sense most \n> people are born with.\n\nThere is a big difference between insulting an annoyance, and being an annoyance. Your lack of discernment only shows, like Talleyrand said, that common sense is the least common of the senses.\n\n> >Well, you just said I was not the only one. Wanna make a poll?\n> What would that prove?\n\nWell, so far, it has proven that you are quite illogical. You claimed my threshold before calling someone an asshole was different from that of the majority. Perhaps you have a better way to guess what opinion is held by a majority than a poll? How antidemocratic. Or perhaps you were just saying stuff.\n\n> You are fond of getting all philosophical aintcha?\n\nThat is not philosopic. That is epigrammatic.\n\n> If I ask a thousands Christian fundamentalists whether God greated the Earth\n> in six days, what would their answer be? \n\nPretty much an accurate approximation to the opinion of the population of christian fundamentalists, of course, assuming they are a representative sample. That is just an example of how polls work. Or perhaps you wanted to make a different point?\n\n> You now have you answer\n\nMe now have me answer, indeed.\n\n> And you are a rampaging ignoramus - and you were told so by half a dozen \n> different people (that I can directly recall) on Linux Today. So there. Nyah, \n> nyah. You're it. And all that.\n\nIf that makes you happy, revel in your delusions, just as I revel on mine. \n\n> BTW: the software used on this board is appalling.\n\nIt is squishdot, same as used in news.gnome.org, IIRC.\n\nBTW: IIRC means \"If I Recall Correctly\". See, you take the initials and use them together in uppercase. Once many people do it for a long period of time, readers are expected to understand it. If they don't understand it, they should lurk for a while until they start getting the jargon. Or impatient newbies such as yourself can simply read the Jargon File. Try that at home."
    author: "Roberto Alsina"
  - subject: "Re: fair warning (my arse)"
    date: 2002-06-15
    body: "well, you are all nobs.  And so say I.  Please don't hurt me :'("
    author: "Idunno"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "That article was pure troll material. The author may prefer KDE (probably because he uses to, or still does, use Windos), but that's no reason to bash GNOME. Both are great environments, with different ways of going about things. What you choose is YOUR opinion. Free software is all about choice. People who criticise others for their desktop choices are pure bigots. This guy doesn't even try to substantiate his arguments (which he cannot), a sure sign of a troll.\n\nBesides, it's obvious that the author of this piece of flamebait masquerading as an article didn't properly read the press release. It is a release of the GNOME 2.,0 Alpha PLATFORM, not the desktop environment. Hence, it is of use only to developers, not to end users.\n\nAs for the ZDNet article, I'm not surprised at the result. ZDNet is a Windows site (everybody knows that the Mac and GNU/Linux sections are really just there for show) staffed with Windows users. In a desktop comparison, of course they will choose an environment that is similar to what they are used to, namely KDE. GNOME can be just as user-friendly once one gets used to it."
    author: "Yama"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "Hmm... from the discussion here and on LinuxToday.com, I think that you lot really aren't used to a British sense of humour... you need to take stuff like that on The Register a bit less seriously.\n\nWe all know it's a non-serious, opinionated and slightly factually incorrect piece. Relax a little."
    author: "Jon"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-06
    body: "I see British humour all the time (and I love it :), but I wouldn't classify this as \"humour\" at all. It is simply a piece of flamebait. Well I guess they've got my flames now. I normally like The Register, and I read it all the time. However, sometimes I feel that they go too far over the top."
    author: "Yama"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "\"... can be just as user-friendly once one gets used to it.\"\n\nI find this amusing :-) Don't you think this is a contradiction?"
    author: "Marko Samastur"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "It's not a contradiction. 'user-friendly' does not mean 'stupid-user-friendly'. Both Emacs and VI are very user friendly, they are just picky about whom their users are :)"
    author: "Jon"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-06
    body: "No, it isn't. The concept of \"user-friendliness\" is heavily based on past experiences. Most computer users are Windows users. KDE is deliberately targeting these people, and hence has a Windows-like interface. GNOME is, for the most part, unlike any other interface seen before (AFAIK). There is nothing people can migrate from that would make GNOME feel like 'home' to them. You really need to keep an open mind while using it, just as you should keep an open mind about using WMs like Enlightenment, Blackbox or FVWM. The same can go for editors (vi vs emacs), and even for religions. Would you criticise someone for their choice of religion?"
    author: "Yama"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-06
    body: "I wouldn't quite say that - Blackbox has a nice level of \"intuitive\" and \"easy\", but suffers from being simplistic (not bad if you don't want a full blown \"environment\".  I would call it userfriendly in general.  Enlightenment, otoh, has a decently hard learning curve (lots of subtle bits), but is powerful.  I'd call it very nice, but not very userfriendly.  FVWM is \"chunky\" and not-intuitive, despite similarlities to other Windowing interfaces, and doesn't seem to have a common feel in various parts.\n\n    KDE, otoh, has a nice simple level, and deep features that can be found.  A few bits (like the new sidebar in Konqueror) aren't quite intuitive, and others (like the new tearable frames) aren't quite userfriendly - but the former is powerful, and the latter is similar to things currently in use (tearable toolbars).  I would call it nicely positioned between powerful with a steep learning curve and simplistic with a low learning curve.\n\n    And the icons can use a bit of work, too - they confuse people.  I've gathered these observations from about 15 people who have sat down and used my computer (generally to poke through CDs, surf the web, or IM somebody).  Three of those fifteen are perfect - complete computer novices with no experience whatsoever (in their 40s-50s) who picked up KDE nearly instantly. Incidently, Kinkatta is judged to be \"really cool, I wish Windows had an AIM like that\".\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-06
    body: "Yes, intuitiveness of user interface is hardly anything but how much you are able to use past experiences to work in new environment.\n\nSo environment, which discards those experiences (and I don't particularly mean GNOME here because that would be a debate I'm not all that qualified for), can hardly be described as intuitive. Hence it's not user friendly, because it means a whole lot of learning and possibly grasping completely new concepts. That's just not friendly.\n\nHowever, it doesn't mean that it's useless. With some luck, it might be worthy to invest all that time in learning a new paradigm, but just because people can learn and get used to practically anything it doesn't mean everything is user friendly. Neither does it mean everything should or could be.\n\nAs a vim zealot I can certainly testify that some things are just powerful without being all that friendly."
    author: "Marko Samastur"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-06
    body: "Not at all. The concept of 'user-friendliness' hinges upon past experiences and teachings. When a Windows user (about 95% of the user population) sits at a computer, they expect a Windows GUI and they expect to be able to do things in a Windows-like manner. KDE is designed to lure these people by being similar to Windows, so it is only natural that they see KDE as more 'user-friendly'. They can see KDE as more of an 'upgrade' than a 'switch'.\n\nGNOME doesn't have that luxury, and neither do the other X environments/WMs. For a Windows user, using these would be more of a 'switch' than an 'upgrade'. Despite this, GNOME and the others have fairly strong support bases.\n\nThis is a testament to the idea that free software = freedom to choose. One needs to keep an open mind when trying something new, and not be influenced too much by past prejudices (\"Common sense is the collection of prejudices acquired by the age of eighteen.\" -- Albert Einstein). If people were not open minded, everybody would still be using Windows/MacOS instead of *nix."
    author: "Yama"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-06
    body: "No.\n\nThere's a quote about the nipple being the only instinctive interface, everything else is learned. I've probably butchered it, but the point is clear enough."
    author: "Wiggle"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-06
    body: "Just because you can quote it, doesn't make it true!\n\nReally though, in terms of User Friendliness, I suppose it all has to do with an interface making sense. However, different UI's have different audiences. Both Vim and KDE are very user friendly, because their UI's are fairly consistent and powerful, even though there are people who use Vim but don't use KDE (Console junkies) and people who use KDE but never use Vim (KSpaceDuel junkies :-) This is just my opinion though, I don't have any research to support it other then personal observations.\n\nInstinctiveness is just the effect of a well designed interface immediately making sense to its intended audience."
    author: "Carbon"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "GNOME 2.0 will have a wrapper to the user's preferred sound server. Applications will use a generic API, not getting tied with aRts or esound or any other sound system. This way, the user can choose the sound server."
    author: "Evandro"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "Well, GNOME will probably always be behing in polishing the desktop. That is because GNOME always leaps ahead in API, and continually forgets that their desktop isn't fancy like KDEs.\n\nI will say that esd needs a replacement - it is not very good, I can hardly ever get it to work.\n\nWhat I think we really need is to push towards GNOME/KDE (GDE??) integration. For example, it would be nice if they could both use the same configuration interface (like gconf), sound system (probably aRTS), etc, as well as possibly future integration of Gtk and Qt. It seems now that we have two DEs, one which has a better API, and continually is leaping ahead in new technologies, and another which looks very good and seems to be much more focused towards end-users (GNOME has ALWAYS lagged behind on this - only people like me use it.)\nIf we could combine these two DEs, we would have an innovative API (GNOME), a stable, end-user oriented system (KDE),  a much more developed system (neither), etc.. In this way, we would finally have a good DE to compete with Windows, and we would only need a good end-user distribution of Linux (right now there isn't one - please try installing Windows before commenting - it is MUCH easier to do for people who are not knowledgable. Besides, right now we have too many copies of the same thing in KDE and GNOME - Abiword and KOffice, Kfm/Konqueror (forgive me about this, I haven't used KDE very long) and Nautilus, KControl and Gnomecc, etc.\n\nConstantine Evans\n\nNote: I use both - I am not a troll for GNOME"
    author: "C. Evans"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "Random comments on your post:\n\nNote that some KDE developers have talked about moving back from DCOP to CORBA, or at least connecting the two, as soon as they feel that CORBA has become a usable system for them.\n\nBy the way, you sound a bit like a public relations guy using 'innovative' all the time... both KDE and Gnome are 'innovative'.\n\nI have tried installing Windows -- start from a computer without a formatted harddisk and then try installing Windows and Linux Mandrake -- Linux will win on ease of use (plus the Windows install crashed on me).\n\nIt always used to be said that Gnome was better because it looked better. Now people seem to be saying that KDE is the one with the looks and Gnome the one with the decent backend. This confuses me, particularly as Gnome hasn't changed in this time."
    author: "Jon"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-06
    body: "Oh - the innovative comes from my background in biotech - everything is \"innovative\"\n\nIn matters of Windows - I mean installation for the normal user - the normal user doesn't want to install 2 operating systems, or format their harddrive themself, they just want the OS to install - we need a dist like this for normal users (prob now with KDE - because KDE is easier to use and more stable in my opinion - maybe that is because my GNOME is from CVS).\n\nBut GNOME has changed - the backend at least. And it seems that they have forgotten about looking good - something that normal users care more about than a good backend. Yet KDE has changed how they look - and they look very good. But in the meantime, GNOME is developing C#, Bonobo, GConf, Bonobo-conf, gtk2, and many other backend changes that have no effect on looks. So GNOME has certainly been doing something, you just can not see it. Whereas in KDE it is very easy to see (I used KDE 1.x for a while and it was completely different)."
    author: "C. Evans"
  - subject: "You don't have a clue what your talking about"
    date: 2001-10-05
    body: "Do you have any evidence that Gnome has a better api than kde.  Name one single part of the gnome api that is better than kde's equivalent.  The gnome 2.0 api, while taking a slightly different approach in many areas, is just catching up to what kde has been using and has stabalized since the release of 2.0.  Qt and gtk will never merge, simply because qt uses c++, the natural way to do OOP. \n\nTell me, what part of gnome is so innovative?  Is it there rippoff of IOSlaves, their rippoff of COM, the fact that they are the first people to program a GUI in C, or is it because they are the first GUI to using AA fonts?  I see no innovation.\n\nThen you complain about duplication.  Well, try convincing the KDE programmers to stop using C++, and then convince the Gnome guys to stop using C.  That will be the day.\n\nAt the end of your post you say that you haven't used KDE for a very long time.  I assume by this comment that you haven't looked at the source either.  If you don't know about KDE, and don't know about KDE's API, then what gives you the right to comment on them?"
    author: "Matt Newell"
  - subject: "Re: You don't have a clue what your talking about"
    date: 2001-10-06
    body: "My point exactly - this is the main flaw in my comment. IT WILL NOT WORK. Because of people who say KDE is stupid, and people who say GNOME is stupid. They just have to continue preaching. Oh well."
    author: "C. Evans"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "Well, GNOME will probably always be behing in polishing the desktop. That is because GNOME always leaps ahead in API, and continually forgets that their desktop isn't fancy like KDEs.\n\nI will say that esd needs a replacement - it is not very good, I can hardly ever get it to work.\n\nWhat I think we really need is to push towards GNOME/KDE (GDE??) integration. For example, it would be nice if they could both use the same configuration interface (like gconf), sound system (probably aRTS), etc, as well as possibly future integration of Gtk and Qt. It seems now that we have two DEs, one which has a better API, and continually is leaping ahead in new technologies, and another which looks very good and seems to be much more focused towards end-users (GNOME has ALWAYS lagged behind on this - only people like me use it.)\nIf we could combine these two DEs, we would have an innovative API (GNOME), a stable, end-user oriented system (KDE),  a much more developed system (neither), etc.. In this way, we would finally have a good DE to compete with Windows, and we would only need a good end-user distribution of Linux (right now there isn't one - please try installing Windows before commenting - it is MUCH easier to do for people who are not knowledgable. Besides, right now we have too many copies of the same thing in KDE and GNOME - Abiword and KOffice, Kfm/Konqueror (forgive me about this, I haven't used KDE very long) and Nautilus, KControl and Gnomecc, two different websites that do nearly exactly the same thing, etc.\n\nConstantine Evans\n\nNote: I use both - I am not a troll for GNOME"
    author: "C. Evans"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-06
    body: "This sounds great, but it's very difficult to integrate technologies. Gtk and Qt have completley different philosophies regarding just about everything. A wrapper connecting the two would be insanely diffucult to code, and probably very very slow in usage.\n\nI don't think \"competing\" with Windows is even an issue. For one, I have installed windows and and several Linux distributions, and found that they are about the same in terms of ease of use, but Linux instllations have always given more control over the installed system. Abiword and KOffice are not copies, Konqueror and Nautilus, etc and etc, are not copies because they are not simply duplicates of each with :s/gtk/qt or :s/qt/gtk. They use different technologies in different ways."
    author: "Carbon"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "I would not be bashing GNOME too much. Fact is, the most valuable\npart of a desktop environment is apps, specifically an office suite.\nOpenOffice started out as a bloated pig but it looks like they are\ndoing all the right things to it. OpenOffice now looks like it will\nbe a serious competitor to KOffice. All the rest of GNOME and KDE is\nnot as essential, so despite a lag in technology, GNOME is quite\ncompetitive now."
    author: "All_troll_no_tech"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "And what exactly does Open Office have to do with GNOME?  Does it use GTK or Gnomelibs?  No.  Does it use Bonobo?  No. (Before you go disputing this, it does use an IDL, see http://udk.openoffice.org/, but a special bridge is required for Bonobo, see http://whiteboard.openoffice.org/bonobo/technical.html).\n\nOf course there is also a C++ bridge for the Open Office UNO -- see http://udk.openoffice.org/cpp/man/cpp_bridges.html -- so Open Office can integrate just as easily into KDE (as a KPart or whatnot).\n\nSo you can just as easily run OpenOffice on KDE as on GNOME -- well, better even :-)."
    author: "Sage"
  - subject: "OT: app compatibility was: Re: ZDnet can't..."
    date: 2001-10-06
    body: "Instead of discussing whether OOffice is a \"GNOME\" application or a package without special bindings to either project, how about a tool that automagically themes gtk like the current KDE theme? KDE already has something that imports pixmap-based GTK themes to use them, and an option to use KDE fonts and colours in Motif apps like Netscape. \n\nThat way, even users that think there is merit in true GNOME apps (like I do - sodipodi e.g. is really much more useful than kontour to me, same with abiword, although I do like the general concept of koffice) can use them without visual ugliness or having to adapt their gtk theme manually.\n\nOf course, this won't help *me* since I don't use pixmap-based themes (liquid), but especially for the default KDE look and feel it would be an useful option."
    author: "Arondylos"
  - subject: "Re: ZDnet can't get their facts right"
    date: 2001-10-05
    body: "a) GNOME has not adopted aRts.\n\nb) What LPGL thing? GNOME's platform libraries have always been and always will be LGPLed. If you saw people complaining against Qt's GPL, it's not the GNOME people. They could care less. KDE users complained about it.\n\nc) \"Badly beaten in terms of quality\"? What are you, a ZDNet writer?\n\nd) Eazel has gone under and the Nautilus software gets more and more contributions every day. The 1.0.5 release is coming and it's, once again, a vast improvement over the previous one.\n\ne) Ximian is doing pretty well actually. They're selling different products (Ximian GNOME, Ximian Desktop, Ximian Evolution) and services (see www.ximian.com). They will, in the future, use Mono to help them build their apps in a better way (for them). Follow the simple rule: don't like it, don't use it."
    author: "Evandro"
  - subject: "Big news!"
    date: 2001-10-05
    body: ">Jono Bacon wrote in to tell us about the first interview \n>on enterprise.kde.org; it's with Shawn Gordon of theKompany.\n\nWay to branch out and get a handle of the views of the enterprise!\n\nNext week: Jono's mum, and how she uses KDE for keeping track of addresses and phone numbers. Followed by yet another fawning interview with Shawn Gordon."
    author: "Wigle"
  - subject: "Warning:  Blatant Troll"
    date: 2001-10-05
    body: "Please, do not be alarmed.  This is only a troll.  Refrain from replying and ignore all posts in this thread.\n\nThis has been a public service announcement."
    author: "Troll Detector"
  - subject: "Re: Warning:  Blatant Troll"
    date: 2001-10-05
    body: "But it IS funny."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Warning:  Blatant Troll"
    date: 2001-10-06
    body: "It's funny, because it's true"
    author: "Dong"
  - subject: "Re: Warning:  Blatant Troll"
    date: 2001-10-06
    body: "Not quite. In order for it to be true, he'd have to actually interview his mom. Plus, I don't see what you're getting cynical about. theKompany is a great example of a KDE business, there's no reason for enterprise.kde.org not to interview them."
    author: "Carbon"
  - subject: "Still waiting for official KDE2.2.1 on Mandrake8"
    date: 2001-10-05
    body: "How funny it is,\n\nthey are discussing the future releases, while Mandrake8 users\nare still waiting on the wings to get their delayed packages for\ntheir OS. Instead of this, the excusing README in the Mandrake\nsubdirectory tells me to look into the cocker, \"or better\" buy\nthe new Mandrake 8.1 .\nDo you want us to buy software, to get the latest packages ?\nShall we better go back to other distributors, that seems to be\nbetter supportet ?"
    author: "Christian Groove"
  - subject: "Try RedHat 7.1"
    date: 2001-10-05
    body: ">Do you want us to buy software, to get the latest packages ?\n>Shall we better go back to other distributors, that seems to be\n>better supportet ?\n\nNo ;) RedHat user base is more than Mandrake's so you can see earlier RedHat packages were unavailable or broken, now the RedHat packages are quite impressive, no headaches."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Still waiting for official KDE2.2.1 on Mandrake8"
    date: 2001-10-05
    body: "You can upgrade to Mandrake 8.1 for free. In fact I did so just today.\nExcept that fonts in Konsole seem rather broken, it's not bad.\n\nAnd please don't look into the cocker, dogs don't like that.\n\nIn any case: KDE doesn't make Mandrake RPMS, or RedHat RPMS, or no RPM, no DEB, no binary at all.\n\nIf you think Mandrake is lagging on building RPMs, go to mandrakeforum, and complain there. It really does you no good at all to do it here."
    author: "Roberto Alsina"
  - subject: "Re: Still waiting for official KDE2.2.1 on Mandrake8"
    date: 2001-10-05
    body: "This is *not* intended to start a distro war thread, as I liked Mandrake when I used it, and still recommend people stay with Windows if they have a working environment that they like. But...\n\nI switched from Mandrake 7.2 to SuSE 7.1 Professional because of slow releases of KDE updates, and because of all the talk of a GTK-only Mandrake admin program.  Having used it for several months, I really think that SuSE has integrated KDE much more closely than Mandrake; the KDE Control Center has a bunch of extra SuSE admin choices, meaning I go to one place to admin all aspects of the machine (that's just an example, not the whole story... the KDE install in SuSE just \"feels\" better).\n\nSuSE 7.3 is due out October 22nd with KDE 2.2.1, ReiserFS, JFS or Ext3, Linux 2.4.10, and a whole slew of the latest nice bells and whistles.  I'm planning on testing the waters of installing it on a friend's computer (until now, I haven't felt that Linux/KDE offered a *better* experience).  \n\nI haven't played with debian or Red Hat recently, but played with *all* the major distros a year and a half ago, so I'm *not* saying that you choice is bad, just saying what I use, and why I switched from Mandrake to SuSE for my KDE desktop.  Incidently, I'm also moving my servers to SuSE in the next few months... I'm very comfortable with its \"feel\" via ssh/telnet as well (which could concievably have suffered due to an overemphasis on the GUI).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "KDE 3.0 will bring Linux/Unix to the desktop :)"
    date: 2001-10-05
    body: "The features of KDE 3.0 will be of User Friendly, easy to use, easy to upgrade and very functional desktop environment complimented with Feature rich KOffice 1.2> perhaps KOffice 3.0 :) But it may take a year :( I am just very eager to see that day ;)\n\nKeep up the nice work!!!"
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE 3.0 will bring Linux/Unix to the desktop :)"
    date: 2001-10-05
    body: "Anyone know anything about the next version of koffice (1.2)?  Release schedule?  New features?  Will there be a bug fix release (1.1.1)?  Just curious.  I'm a happy user of Koffice - MS Office is nowhere to be found on my machine :)"
    author: "Jano"
  - subject: "Re: KDE 3.0 will bring Linux/Unix to the desktop :"
    date: 2001-10-05
    body: "The next version of Koffice will probably be based on KDE 3, so expect it around the 1st quarter of 2002."
    author: "Evandro"
  - subject: "Re: KDE 3.0 will bring Linux/Unix to the desktop :"
    date: 2001-10-06
    body: "Current HEAD KWord has true WYSIWYG, iirc."
    author: "Carbon"
  - subject: "Re: KDE 3.0 will bring Linux/Unix to the desktop :)"
    date: 2001-10-05
    body: ">But it may take a year\n\nNot likely!  Right now the KDE 3.0 release is aimed at February 25 of next year.  I wonder how the developers are going to have time to add all of their cool features?"
    author: "not me"
  - subject: "Re: KDE 3.0 will bring Linux/Unix to the desktop :"
    date: 2001-10-06
    body: "Six syllables:\n\nKDE 3.1 :-)"
    author: "Carbon"
  - subject: "Re: KDE 3.0 will bring Linux/Unix to the desktop :"
    date: 2001-10-06
    body: "No, it won't - because no normal user is  going to be able to 1. figure out how to install linux, 2. figure out how to get X working, and 3. figure out how to get KDE working.\n\nWe need and end-user(ie. idiot) distribution before we can bring linux to the desktop."
    author: "C. Evans"
  - subject: "Re: KDE 3.0 will bring Linux/Unix to the desktop :"
    date: 2001-10-06
    body: "Ever tried Suse? I think they're well on the road for said Idiot's Distribution. Maybe in a few revisions..\n\nBut you don't just need this - what you really need for the end-user is games.\nMost of the dummy-users choose their system by availability of games, so they don't need an additional machine for their kids..."
    author: "Robyxx"
  - subject: "KGame?"
    date: 2001-10-05
    body: "Whatever happened to GGZ?"
    author: "Carbon"
  - subject: "Euro support and 2.2.2"
    date: 2001-10-06
    body: "Can I draw the attention of the KDE community more specifically to the frustrations Mandrake 8.1 users are experiencing with <A HREF=\"http://mandrakeforum.com/article.php?sid=1236&mode=&order=0&thold=0\">KDE and the euro symbol</A> - non-KDE apps are working fine.  There is some major keyboard problems"
    author: "Simon"
  - subject: "Wiggle - What to do?"
    date: 2001-10-06
    body: "Seriously - I'm pretty laid back about topic drift and metaconversation, but can't someone just ban his IP from posting, or do the equivelent of killfiling the guy?  He's almost singlehandedly turned the entire site into useless back and forth semantic and illogical arguements about inane and trivial points.  Just take a look at flatforty - almost all the new posts are him (a good third to 40%) or replies to him.\n\n    The flames and stupid threads on the dot have gotten to the point that it's not worth reading more than the story summaries - and I'm saying this as an avid Slashdot reader and poster.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Wiggle - What to do?"
    date: 2001-10-06
    body: "How do we know he has a static ip?? - or that he isn't behind an ipmasq? This is a  major problem for every Linux site with comments (KDE, GNOME, Slashdot, etc.). Ban the name - he changes it. There is hardly anything that can be done."
    author: "C. Evans"
  - subject: "Re: Wiggle - What to do?"
    date: 2001-10-06
    body: "What we can do, for now, is simply encourage everyone not to respond to trolls. This is hard, I know, because I've tried and failed to keep myself from chewing trolls out. But, if they get no response, then they won't come back."
    author: "Carbon"
  - subject: "Re: Wiggle - What to do?"
    date: 2001-10-06
    body: "There are only two things that can be done as I see it:\n\n1.  Navindra deletes all his troll posts\n\nThat's kind of a lot of work for poor Navindra, and he can't monitor the site 24 hours a day for trolls!\n\n2.  Readers do something about it\n\nThe best option would be for everyone to ignore him, and he would go away.  It's not likely though, there's always someone who just has to reply.  We could do \"Troll Detector\" posts all the time, but then we run the risk of getting legitimite posts marked as trolls and generally making dot.kde.org an unpleasant place.\n\nHopefully he will get bored soon and simply go away, because I don't really see a good way of getting rid of him."
    author: "not me"
  - subject: "Re: Wiggle - What to do?"
    date: 2001-10-08
    body: "I believe it would be better to implement a moderation system."
    author: "Evandro"
  - subject: "OT: Any 3alpha1 binaries around?"
    date: 2001-10-06
    body: "Subject says it all,\n\nfor SuSE 7.2 would be convenient!"
    author: "yves"
---
In a commendable flurry of submissions, Anonymous wrote in with a link to <a href="http://linuxpr.com/releases/4175.html">an announcement</a> from China-based <a href="http://www.dynasoft.com.cn">Dynasoft</a> on a <i>"sophisticated Chinese
KDE desktop environment, hybridly-licensed, and based on Red Hat Linux 7.1"</i>.  Looks promising (<a href="http://dyp.v-eden.com/english.html">English</a>, <a href="http://www.yangchunbaixue.com">Chinese</a>), although the <a href="http://dyp.v-eden.com/screenshots/indexe.html">screenshot section</a> is a bit wack.  <a href="mailto:b_mann@gmx.de">Andreas Beckermann</a> wrote in with <a href="http://lists.kde.org/?l=kde-games-devel&m=100132107307589">an announcement</a> for KGame -- the short is that KGame is part of libkdegames and provides <a href="http://www.heni-online.de/libkdegames/">a sweet API</a> to make the life of the game designer that much easier.  <a href="mailto:jono@kde.org">Jono Bacon</a> wrote in to tell us about <a href="http://enterprise.kde.org/interviews/thekompany/">the first interview</a> on <a href="http://enterprise.kde.org/">enterprise.kde.org</a>; it's with Shawn Gordon of <a href="http://thekompany.com/">theKompany</a>.  <a href="mailto:mmh@gmx.net">Moritz Moeller-Herrmann</a> pointed us to <a href="http://techupdate.zdnet.com/techupdate/stories/main/0,14179,2816006-1,00.html">an indepth perspective</a> on CDE, KDE, GNOME featured on <a href="http://www.zdnet.com/">ZDNet</a>.  Naturally, they like KDE.  Finally, <a href="mailto:george.russell@clara.net">George Russell</a> wrote in with the latest updates on all of two (2) upcoming KDE releases: <i>"The developer site has a preliminary release schedule for a 2.2.2 release - incremental bug fixes over 2.2.1 - details are <a href="http://developer.kde.org/development-versions/kde-2.2.2-release-plan.html">here on the KDE site</a>.  The <a href="http://developer.kde.org/development-versions/kde-3.0-release-plan.html">KDE 3 Alpha 1</a> release will be on Friday. Release coordinator spells out the reasons for the 1 week delay in this <a href="http://lists.kde.org/?l=kde-core-devel&m=100203620929122&w=2">email on the core development lists</a>."</i>  Thar you go, thanks to all.
<!--break-->
