---
title: "Mythical Creatures Behind KDE: Konqi"
date:    2001-05-01
authors:
  - "Inorog"
slug:    mythical-creatures-behind-kde-konqi
comments:
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "A dragon ??? Why not a door, a roof, or ... a window :-)"
    author: "Bebe"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "For F**K SAKE!\n\nAre we all Kiddies or what!\n\nWould it not be real to work on the real issues and problems with KDE2 rather that talk on and on\nabout silly subjects like this?\n\nLike Lack of programs like kmp3/kmidi as in KDE1!\n\ntracking down why YMF44b sound card that works fine on KDE 1.1.2 will lock up in KDE2 and require hitting reset button (Crash re-boot) to exit!\n\nKDE has a great name so please stop being total wankers and try being positive about all the bugs!\n\nRegards,\nJames"
    author: "James Alan Brown"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "Hey man, lighten up. Life in the KDE world ain't that bad.\nI have just enabled AA fonts on a desktop that leaves anthing in the commercial world for dead. The majority of the applications developed by the talented and dedicated people who have created this environment are in various stages of usability but I have seen amazing progress in the past two years in many areas. There is nothing wrong with injecting a bit of humour into the community and I for one enjoy the interviews and in this case the lightheartedness of the KDE team. The developers and support team are entitled to relax a bit and enjoy the use of their window on the world without grouchy individuals like you raining on their parade. I'll be looking for your contribution to the project in the near future!"
    author: "Tony Farrell"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "What a sad person!\n\nI don't believe Tink is a software developer and a little humour goes a long way."
    author: "CyberKat"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "Geez, guy!  You're sounding like a little fun is the spawn of satan!  How long could this have taken?  Like an hour?  Maybe less?  That amount of time is not what I would call \"Wasted Time.\"  And if you really want, you can use KDE 1.1.2, it isn't a bad desktop.  But the best thing you could do is develop some patches or fixes of which you spoke of before.  Just quit complaining :-)"
    author: "Steve Hunt"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "> Would it not be real to work on the real issues\n> and problems with KDE2 rather that talk on and\n> on about silly subjects like this?\n\nBut James,\n\nWhy are you wasting your time by posting this\nhere? Why aren't you busy tracking down why your\nYMF44b sound card doesn't work? Why haven't you\nwritten a MP3 player yet? Don't tell me that you\nare such a wanker that you can't even program!\nThey have books in the library about all that\nstuff you know. So stop wasting your time here \nand do something usefull.\n\n\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "I can program: (25 years of programming experiance)\n\ncheck out what I am currently doing with my version of KDE AbiWord.  \n\nIt will show you how nice KDE office programs can look and work.\n\nwww.jabcomp.force9.co.uk/download/abiword.jpg\n\nbest viewed full screen 800x600 and plase note dual themming with GTK the way I had hoped that KDE would have gone from KDE1.1.2.\n\nAn ugly woman in a new dress is still an ugly woman! This is my point about flash before stability.\n\nregards,\nJames"
    author: "James Alan Brown"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "> An ugly woman in a new dress is still an ugly\n> woman! This is my point about flash before\n> stability\n\nYour screenshot hurts my eyes. I doubt anyone could be productive staring at for a long time.\n\nIt probably even breaks labor laws in several countries, here in the Netherlands I believe that to prevent work related injuries such as RSI you have to read text black on white. My boss actually made a comment on the white on black Konsole's of mine. I wonder what he would do if I started using those colours!\n\nAnd using colours for checkboxes (green/red) is behaviour very dependant on the Western culture and not completely suitable for world-wide use. That is the reason they were removed from the old System theme of KDE2 pre-alpha (back in 1999)."
    author: "Rob Kaper"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "Wow, what theme is that?  I think I'm blind."
    author: "AC"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "This whole post has *got* to be a joke.\n\n> check out what I am currently doing with my version of KDE AbiWord. \n\nI've recently taken a look at the ABI Word code and found that it basically was programmed in C++ from 10 years ago. char* everywhere, no strings, no vectors. Yuck. No chance you'll get this thing more stable than KWord.\n\n> An ugly woman in a new dress is still an ugly woman! This is my point about flash before stability.\n\n... says he after posting the URL to one of the most eye-piercing screenshot ever been displayed."
    author: "Guillaume Laurent"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "***YAWN*** - you are so lame that even libmp3lame can't top you. You already posted this eye-hurting fake-screenshot months ago. If you were half as lame as you are you would have made sure that this \"screenshot\" wouldn't contain those obvious text alignment-\"bugs\" ..."
    author: "me"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-09
    body: "Yes there is something very fake about that isnt there. To call it his 'kde AbiWord version\" is practically fraud. It is so easy to put a title on something. Someone with 25 alleged yrs programming would know that a kde app has to actually have some kde code in it. Running a gtk/gnome app under kde and changing the title does not make it a kde app.\n\nBut never fear, I will use my 'kde xmag version\" to save the day: I need to after looking at that screenshot.\nDoes this geek really think that is what people want? The guy has absolutely no-conception of UI design nor artistic or graphic talent. That would have to be the most atrocious colour scheme I have ever seen out of hundeds of themes and could never be used in real work.\n\nAnd as for the original issue, these product identification icons are essential to building a product image and ensuring the long term proliferation and success of kde but the reasons are to much to go into here and besides I think James is on a different wavelength."
    author: "Andrew Kar"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "James:\n1) use civilised languange. No need to swear, it only shades your stature\n\n2) I know I will shock you, but my main attraction towards KDE is *the fun it provides me* constantly, be it that I code, I chat with peer developers, I read masterful code of others, I do conferencese, I give interviews, I write book chapters, I advocate or whatever.\n\n3) The minute the fun factor goes away, KDE  dies with a not more than 48 hours agony.\n\n4) We are all aware we don't have a 100% perfect desktop. But we know it's not that bad, not only for its own price, but also for the price of the competition (microsoft)\n\n5) We *will* improve it, but we need to have fun while doing it.\n\n6) The dragon has more sense of humor than some real people, it seems :-)"
    author: "Inorog"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2004-10-08
    body: "i need yamaha - ymf44b audi driver to toshiba satellite pro 4200 series\nPlease help me \n\nIt\u00b4s hard to find it"
    author: "Hugo"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "Congrats Tink on another interesting feature...some of these KDE people are really really cool arnt they. ;-P\n\nI think the whole Konqi thing is getting a little out of control, and my only worry is that it affects KDE's reputation in the enterprise. I mean...if you were a multi-millio pound company...would you put an environment on your machines that constantly shows a cartoon dragon...?\n\nThe other argument is that Linux has Tux, and it seems to be doing quite well. Well, a friend of mine (who is leading a fairly large and famous Linux company) said that he believes that the day Linux really hits the enterprise, the penguin will go. I can't really imagine anyone getting rid of the penguin though...\n\nAnyway...interesting article as ever Tinkywinky, but maybe we should discuss taking the dragon out of a KDE a little. :-P"
    author: "Jono"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "...if M$ can cope with a paper clip on a bike\nKDE should be to cope with a dragon ;-)"
    author: "Adrian Bool"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "Sorry, Jono. If enterprises measure a software product by the look of it's mascot, then i personally dont care if they adopt it or not. They are unworthy.\nMany people hate this paper clip, some love it. But does anyone say: \"i really like word, but i cant use it because of this childish paper clip\" ? _This_ would be the actual act of being childish."
    author: "Lenny"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "paper clips: actually i really do like MS word but that paper clip thing _completely_does_my_head_in_\n\ni don't just dislike it, i'm afraid i loathe it with a passion.\n\ni think things like that are actually out of place in such an environment- \"cute\" is HIGHLY subjective and speaking for myself if i disagree that something _is_ cute i will find it intensly irritating to be constantly subjected to it.\n\nno that i'm complaining at all about the kde dragon. how on _earth_ can you complain about you're not paying for? particuarily something as cool as kde! (and anyway, it dosen't get my goat like that hideous paper clip anyways because it dosen't keep popping up and attempting to take control!)\n\nanyway,,,, ermm- what was i saying?"
    author: "phil"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "I agree, that paperclip is out of control. However, I'm more disgusted by the MSN/IE butterfly. I can just see an animation of Konqi un-twisting that paperclip and whacking the butterfly several times with it. >:^)>"
    author: "Carbon"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-03
    body: "why didn't you install office without the assistants? It's your free choice to live with or without them and it just needs a click to get rid of them... At least MS offers you a choice..."
    author: "W.A."
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "<quote>Well, a friend of mine (who is leading a fairly large and famous Linux company) said that he believes that the day Linux really hits the enterprise, the penguin will go. I can't really imagine anyone getting rid of the penguin though...</quote>\n\nI assure you Tux isn't going anywhere. IBM has spray painted a bunch of them on the sidewalk near my office and they're not showing any signs of disappearing."
    author: "Otter"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "> my only worry is that it affects KDE's\n> reputation in the enterprise.\n\nI think their are still 3342 other things that\naffect KDEs reputation in the enterprise more.\n\nIf enterprises only want to use KDE if it is\ndeadly boring then I suggest they make room in\ntheir budget for those Windows licenses.\n\n> if you were a multi-millio pound company...would\n> you put an environment on your machines\n> that constantly shows a cartoon dragon...?\n\nNo, I would replace it with my company logo to\nmake my desktop fit in with my corporate look. \nYou seem to mix up KDE, the desktop, with KDE,\nthe webpages btw.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: ">> I mean...if you were a multi-millio pound company...would you put an environment on your machines that constantly shows a cartoon dragon...?\n\nWhere does Konqi \"constantly\" appear?  I *never* see him, even on the splash screen.  Maybe it's just the way SuSE packages the RPMs.  Hmm."
    author: "Tukla Ratte"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "The penguin will go in order to make Linux acceptable for serious users? Konqi must go because that's not business-like?\n\nMust be a world of very dull-minded people we live in. OK, if people can't cope with penguins and dragons, they should not use the respective products, probably not really a loss.\n\nI personally dislike people who take themselves too seriously. People (and projects) who don't take themselves too seriously all the time - but produce serious work instead - are much more appealing."
    author: "AC"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "Yes. Lets replace Tux and Konqui and Katie and everything else with splash screens containing pictures of dumb looking but mostly attractive women. I say this not as a sexist, but as an observer of such things (eg., Painshop Pro 7). After all, the advertising people must be right, musn't they, otherwise they wouldn't be in advertising."
    author: "Mike Richardson"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "This discussion is getting more interesting buy the minute. I suggest we replace the dumb looking but gorgeous women with the sleezy unattractive standard image of a nerd (that the 'general public' has) who works on KDE and see how much more popular KDE will be.\n\nJust a thought,\n--\nTink"
    author: "Tink"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "ermm... the SuSE-box in the picture doesn't colour-match. (Mine does, though.)"
    author: "reihal"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "To all at KDE and with respect,\n\nPlease read slowly  all of my points before blowing a fuse!\n\nLook guys, lets try this in plain English\u0085\n\nFirst read that Dragon post fully.\n\nIt would be really great as a bed-time story for most kiddies but come on where are we going?  Is this for real?  Do you not think KDE has the potential to put Linux on the map as a great OS?\n\nLook if Linux is to succeed then, like it or like it not, you have got to produce a working environment  that looks and works and performs better than MS Windows or Linux will just end up as a fun play-about program. Dragons don't really send out the right vibes to professional people nor do talking paper clips (even his Billness has at last got that point)!\n\nAny new Linux user, with even half a brain, is looking so desperately hard at being able to use a PC with stable software or they will just keep on using the same old blue screen crasher called MS Windows($$$).\n\nI am sorry but I really don't want to see Linux becoming one big joke.\n\nHaving spent many long hours sorting out other peoples MS Windows blue screen crashes I have found that certainly with KDE 1.1.2 you can actually run a small business and have total stability to boot. I would love KDE2 to be that way, I am sure it will be in time, but I am questioning KDE's current priorities.\n\nOk yes I can use KDE1.1.2 and you can all use KDE 2. whatever, but you know\nfor some time now, looking from the outside at KDE you see this  \"rush\" to compete with Gnome, to be more flash, to release far to soon because your maybe so pedantic about losing out as \"The Linux Desktop\"!\n\nI really like KDE,  but it must be clear to all I do not like the way it is now going.\n\nOk  its open source and yes people are giving their free time and total effort, but come on, what is more important?  Surely get the ruddy thing stable first, port over all the programs you can that worked well in KDE1.1.2 rather than what is now actually happening. Transparent Icons, Dragons and other silly gimmicks  are great but are not that good if the rest of KDE2's performance and stability is poor.\n\nKDE1.1.2 put in an attic and labelled up as \"No longer supported\"  before KDE2 is even half ready just so KDE can claim \"Look at how great we are\"!\n\nAnd yet Guys KDE1.1.2 is totally stable and totally usable in an office environment. \n\nSo why this big rush to realise a half finished product with silly gimmicks?\n\nSurely the flash and fun come way after getting the basics working correctly?\n\nYou really do need to stop and take a look at these points.\n\nTake a look at this URL:- \n\nhttp://www.jabcomp.force9.co.uk/download/abiword.jpg/\n\nThis is what I am doing with a KDE version of AbiWord then tell me I don't have a good point or two to make!\n\nOk I could get involved, and yes, I could try to sort out some of this  as a QA person for that part.  I am sorry to say what little QA you have \"stinks\" at the moment. \n\nIts no fun downloading 80 Megs of KDE2 source code, compiling it up, only then, find it is full of bugs and unstable although it looks flash with silly talking dragons and transparent icons! \n\nAt the end of the day the question is this:  Do you all want a flash toy or a stable working environment?\n\nRegards,\nJames"
    author: "James Alan Brown"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "<http://www.jabcomp.force9.co.uk/download/abiword.jpg/>\n\nNot Found\nThe requested URL /download/abiword.jpg/ was not found on this server.\n\nApache/1.3.12 Server at www.jabcomp.force9.co.uk Port 80\n------\n\nIf you want to make a point you better make sure that you have at least your urls working. Otherwise it will look ridiculous.\n\n--\nTink"
    author: "Tink"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "It is there: I up-loaded 1 hour ago and have just typed in the URL to check... try again please!\n\nRegards,\nJames"
    author: "James Alan Brown"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "She is right. You mistyped your URL up there. There should be no trailing \"/\". To those interested, the real URL is:\n\nhttp://www.jabcomp.force9.co.uk/download/abiword.jpg\n\nBut I must say, James, that I have seen more impressive screenshots. A preferences dialog does not a featureful word-processor make... And your color scheme looks god-awful.\n\nAbout your original post: I find your reaction surprising. No doubt I am not alone. If you have at all followed reactions to the releases of KDE 2.0 and 2.1, you should know that your experiences are not at all typical.\n\nKDE 2.X is a very large software suite. It can be difficult to compile and configure properly. Perhaps that is the source of your problems. Try pre-compiled binaries. Maybe you will change your mind.\n\nIf I was you, I would actually try it out before commenting further on this issue. You are so far off from what everyone else experiences that it makes you look like you do not know what you are talking about."
    author: "Jerome Loisel"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "as a point:\n\nI have used pre-compiled binaries for kde2.0, kde2.0.1 and 2.1.0 and again source bzip2's\n\nI dont think I can begin to tell you how many times/hours/phone/bill/costs I have downloaded 80 meg of files for each version the hours of compiling and 3 different versions of QT source. I am dam sure my system is A OK too. Other programs compile fine and run without errors. It has been my experiance to date that compiling from source code is always the best way to do it.\n\nI certainly found problems with SuSE's pri-compiled binaries that went when I compiled from source.\n\nCertaily the gold colour theme maybe subjective and I respect your view. Please look at the layout\nand themming that is my point! \n\nBy the way my KDE 1.1.2 was totally complied from \nKDE's bzip2 source files and boy oh boy its stable. I re-worked all the icons and kpanel with many changes. \n\nI hear you but not sure if I follow your thoughts?\nRegards,\n\nI have also used KDE1.1.2 and various KDE2 versions on many other PC's that I build with the same results: that is KDE2 looks great but still to date just to many bugs to use in the real world for business.\n\nregards,\nJames"
    author: "James Alan Brown"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "I currently work for the Levinux project (http://www.levinux.org/). The project is connecting ten community centres to the Internet. It is lead by Jacques Daignault, a university professor at the Universit\u00e9 du Qu\u00e9bec \u00e0 Rimouski.\n\nEach community centre gets one server and several (6-20) dumb X terminals. All machines run Linux (we promote the philosophy of free software). We have so far 50+ installed machines and use them every day. I use mine all day long. So I think that when you talk about \"use in the real world\", I have some first-hand experience.\n\nOur users do not for the most part know anything about Linux, so KDE 1.X was very helpful when we ran it. It made the project possible because it is powerful and, as you say, stable. Yet moving to KDE 2.X has been a no-brainer and we have never regretted it. \"Unstable\" is definitely not how I would describe KDE 2.\n\nI cannot even remember the last time I saw Konqueror crash. Application crashes in general happen with KDE 2 applications as they do with all software every now and then, but KDE 2 application almost never crash for me. And I have never seen a KDE 2 desktop crash or lock up because of something KDE 2 did. (And KDE 1.1.2 did have at least one desktop-locking bug.)\n\nDo yourself a favor. Grab a distribution CD that ships KDE 2.X (Mandrake 8.0 and RedHat 7.1 are both good bets) and make a fresh install on a clean machine. Then try it out. You might learn something. Or maybe we will realize that there really is something magical about where I work that makes software bugs go away, though I somehow doubt it."
    author: "Jerome Loisel"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "SuSE 7.1 is a godo choice for a KDE 2.X distro too. \n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "Thats true. I work with KDE 2.1.1 on Suse 7.1 nearly every day. Never had a crash :)"
    author: "Wolfgang Hockertz"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "I don't know why you would say \"KDE2 looks great but still to date just to many bugs to use in the real world for business.\" I've been using KDE 2.1.x for months, and it works like a dream - I think KDE 2.1.x is a BIG improvement over KDE 1.1.x or 2.x even.\n  Also about \"silly talking dragon\" - tell me where there is a silly talking dragon, and I'll be shocked. All I've seen is Konqi in the about boxes, certainly nothing better or worse than Clippet the Paper Clip (who I actually like, I like 3D stuff :-).\n  Finally about themeing: \"It will show you how nice KDE office programs can look and work.\"  No offense, but I personally think KDE and KOffice's high color icons look nicer than the ones you point out. I especially appreciate them after I try doing a little icon design. Do you realize how hard would be just to create an icon like the one for the Konqueror Web Browser button?\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "Just remove the trailing slash, please. The correct address is\n    http://www.jabcomp.force9.co.uk/download/abiword.jpg\n\nI was able to see the picture. But even if the link was broken, the concerns about current KDE priorities, stability and the future remained valid. I have these concerns too ;-(\nTima."
    author: "Tima Vaisburd"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "Image is there, but the URL was mistyped, there should be no slash at the end."
    author: "Carbon"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "neither,\n2.1.1 does me just fine..."
    author: "mr blunt"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "> Surely the flash and fun come way after getting the basics working correctly?\n\non this topic you complain about essentially two things, \n1- performance: I cannot comment on that since my computer is really cool and fast with lots of memory(1 Go). However after reading the different mailing lists, lots of effort are done to improve the performances. It is a long and nasty job and not fun at all most of the time. having good Performances is good from the user point of view, but, if you are not careful and good at it, you can end up with a code which is unreadable, difficult to maintain and share with many people. So there is always a tradeoff. \nI use 2.2alpha and konqueror is faster than ever to display web pages. it is just awesome. \n2- stability: well all my colleagues and students here are using kde 2.1.1 and they are experiencing no CRASH, it is highly useable and they are happy about it(they used 1.1.2 before and don't want to go back sorry ;)). Yes there might be some problems, with some web sites for example, but if you don't put the code for a public review you will never know about lots of problems, you know!\nthat's the power of Open Source development. \n\nKde is a great environment, stable and all.\nit has lots of app which help productivity(kdevelop), not just a toy environment as you describe it. I have a proof of that every day at work!\n\nAnd I tell you, if the environment is not stable\npeople would complain all the time saying that it was working better before. It didn't happen for me here. (btw the people I mentioned are really end users and not power users)\n\nMoreover since the sources are available, you could perhaps enhance what you think is not good enough and share your work with the community.\nThat would be great. (unfortunately for most of us we have only 24h in a day, that everybody knows that it is not enough ;))"
    author: "jesunix"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "I appreciate that you presented your argument clearly, but frankly I have a hard time seeing where you are coming from. KDE is quite stable and usable, although not as good as it could be. 2.1.1 is, IMHO, up to the level of KDE 1.1.2, for stability. \n\nYou're right, flashy gimmicks do not come before stability, and we are not as stable as we can be, and are currently getting towards. But, we are not an organized effort! If people develop a stable, but flashy, gimmick feature, it makes no sense for us to ban it and say \"No, don't add that until we've added more stability!\".\n\nKDE 1.x was abandoned too early, I'll agree with you there. It would have been much better if was ditched right about now, since at this point KDE 2.x is as stable."
    author: "Carbon"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "If I can summarize your points:\n\n  (1) KDE, and hence Linux, cannot succeed if KDE has a mascot that you don't find professional.\n\nLinux's success (tux the penguin and all) is not tied to KDE.  Moreover, most projects have a logo.  If you have a better logo, show us.  If your screenshot is any indication, I don't think anything interesting will come of it. \n\n  (2)  KDE 2 is being rushed out.\n\nI don't agree with that.  Take a look at KDE history (http://www.kde.org/whatiskde/proj.html#A bit of KDE History). You probably are using KDE 1.1.2, which was released 14 months after KDE 1.0.  As KDE 2.0 was released in October 2000, that means KDE 2.0 has until December of this year to reach the stability of KDE 1.1.2, assuming the same time frame (KDE is much bigger now so maybe it may take a while longer to stabilize).\n\nKDE 1 was never going to be \"better\" than MS Windows in terms of features; the architecture in KDE 2 has been much better thought out.  The jump to KDE 2 was necessary, and dropping KDE 1 development was necessary to develop KDE 2.  As KDE 1 is open source, anybody can continue to maintain it.  Why should the core KDE developers stop developing KDE 2 -- to get it as stable as you like -- to work on KDE 1?  What is there to do?  You yourself said it is very stable . . . . And in terms of architectural improvements and feature enhancements, that's what KDE 2 is!\n\n  (3)  KDE developers are not allowed to enjoy themselves or have fun until you have determined that KDE 2 is stable enough and the logo professional enough.\n\n'Nuff said.\n\nYou should also bear in mind that many people want the flashing stuff.  One of the biggest criticisms of KDE (valid or not) has been that it lacks the flashiness of GNOME.  If someone wants to take their spare time to make transparent icons, more power to them.  If as many people contribute as complain things would get stable very quickly."
    author: "Sage"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "so again we are back to this silly KDE V Gnome thing!\n\nCome on!\n\nregards, James"
    author: "James Alan Brown"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "You are a troll.  Where does anything in my post say anything about KDE v. GNOME?  Sure users will compare it and provide feedback; just like, if you would bother educating yourself, you will see that many users compare KDE to Windows or MacOS or AmigaOS.  The flashiness-compared-to-GNOME thing happens to be one of the bigger user feedback items.\n\nNow I guess you will say KDE developers shouldn't care about those users, right, as you have something you find more important for them to do?\n\nGet a clue, James."
    author: "Sage"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "\"KDE (valid or not) has been that it lacks the flashiness of GNOME\"\n\nsounds like KDE V GNOME to me!\n\nRegards,\nJames"
    author: "James Alan Brown"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "To spell it out:  users want, and have repeatedly asked/demanded, the flashiness."
    author: "Sage"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: ">Surely the flash and fun come way after getting the basics working correctly?\n\nI totally agree with this, but you seem to miss a thing. The person doing this doesn't have the knowledge and skill (IIRC) to fix the basics. This, hovewer, is an area where she can contribute. I believe most of the gimmicks are done by people who are better at other things than coding. Things like this don't decrease the effort to get the basics working correctly.\n\n/trynis"
    author: "trynis"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2003-06-06
    body: "I can see what you are saing, and it seems that the other people seem to not understand the simple concept you are tring to get across and all they do is criticise your little typos. \n\nMy God....if these people are leading KDE then they are certanly in trouble. Anyone with half a brain should realize that stability is definately more important than looks. \n\nMicrosoft's main focus in Windows XP was a new look, but they failed to give the deserved focus on stability, and as you all have seen by the huge size of SP1 obiously they forgot something.\n\nBells and wistles are nice, and without them you have no market. What I think he questions is does the same focus go on asking the question \"Does it actually work?\"\n\nMy idea of what a good focus should be is consider all aspects.... Have people that make your little dragons but have someone that's going behind the senes and makeing sure all the backend code is stable.\n\nRespectfully,\nJoey Pruitt"
    author: "Joey Pruitt"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: "KDE should really have a few different look & feels.\nMaybe we can have some conslusions here, and I feel that all discussion about kde beiing childish is not so bad as it looks.\nI remember that I was the first who protested on cooker mailing list when Mandrake replaced original kde splash screen (Mandrake 7.2) with its IMHO ugly and childish splash screen (colours & design were awfull). \nSome other users protested too, and now Mandrake 8.0\nships with kde original splash screen, better designed icons etc.\nJames is right when he says that Konqi and Katie and KandAlf are not good for business users, but he is not right when he says that they are ugly.\nWe should really save those (sorry James) cute characters and add some icons for children and make the first Unix based desktop environment with additional look and feel for our children. I am not kidding\n:). I wish Konqueror had a possibility to view 48x48 toolbar icons. It would be easier to create some icons for kids. And remember that small kids have difficulties pointing the mouse cursor to small areas on the screen."
    author: "antialias"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-01
    body: ">James is right when he says that Konqi and >Katie and KandAlf are not good for business >users, but he is not right when he says that >they are ugly.\n\nInteresting question. I suspect that the suit who is 'put off' by Konqui/Katie/Kandalf is going to go with M$oft whatever. I say stick with them. I think it is time to try to break the mould, and to promote content over flashyp resentation (but I _don't_ mean settle for any old scruffy stuff); maybe some 'cute' images actually emphasises the fact that that bit is light hearted, and not some overblown marketing fantasy.\n\n> We should really save those (sorry James) cute > characters and add some icons for children and > make the first Unix based desktop environment > with additional look and feel for our\n> children. I am not kidding\n> I wish Konqueror had a possibility to view\n> 48x48 toolbar icons. It would be easier to \n> create some icons for kids. And remember that \n> small kids have difficulties pointing the \n> mouse cursor to small areas on the screen.\n\nActually, this is a serious point; get people involved as early as possible ....."
    author: "Mike Richardson"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "I concur, KDE for kids is something that people should get involved in.\n\nAny thoughts about KoolPaint having a \"Kiddie\" mode, similar to kid progs such as KidPix?"
    author: "Carbon"
  - subject: "<B>Attn: James Allan Brown</b>"
    date: 2001-05-02
    body: "You should seek psychiatric aid.\n\nTink (the writer of the article) doesn't code afaik, so she contributes how she can.\n\nThe people who drew konqi and katie most likely don't code, or if they do, they also draw.  If I must say so myself, they draw pretty well.  Currently konqi is only in the kde about dialogs and on the websites.... no where else in kde.  In fact, you have to look for him to find him.\n\nAnd transparent icons are configurable.\n\nAnd last but not least, we generally don't give a fu_ck what the corperate people think -- we're doing this for ourselves.  If I want a feature, I implement it.  If the corperations want a feature, then they should implement it.\n\nIf you want a better handle on the attitude of the people working on kde, hang out on irc.kde.org/#kde for a few minutes.  We're not here to please the corperations, we're here to please each other... <B>individuals!</B>\n\n/me goes back to irc\n\nTroy Unrau\n--First there were monkeys on typewriters; \n  Then there was UNIX."
    author: "Troy Unrau"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "Linux has officially lost it's 1334ness."
    author: "AC"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "Lost it's ... what?\nQuestion: Did it ever wanted to be ... ?\n\nSome time ago it was so easy to be ___, you\njust had to install Linux on your PC :)\nAnd I'm sure, this was the reason for you to\ninstall Linux.\nMy suggestion: Try harder to be ____ \n(I guess you want to be). Maybe you find a OS\non the ROM of your Microwave, that suites your needs...\n\nWe use this Linux thing because of other reasons\n(One day you will understand that...)\n\nhehe"
    author: "Thomas"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "ha, i wait for you to flee to a more l337 operating system. when that happens, i'll port some uncool app to it, say a kony-screensaver and i will joyfully watch you run around in panic seeking desperately for a new l337 os. *enter recursion*"
    author: "Lenny"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "I don't know why everyone is bashing this article. I thought it was lighthearted and funny.\n\nAnd that Konqi plushie is cute. :-)\n\nLighten up people."
    author: "AC"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "Me neither, I thought the interview was simply great. Free software is about fun, not about enterprise (not to say that this is an exclusive or or enterprises cannot be fun).\n\nIn Norway there is a saying: \"a good laughter makes your life last longer\". Thanks a lot, Konq and Tink, for the extra minutes :)\n\n\nMatthias"
    author: "Matthias Ettrich"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-03
    body: "Huh, I am totally confused now. There is a guy on kde-user mailing list (Andrew, who BTW obviously rulez there), and who says that core kde-developers and people from zine and promo share his opinion. And his opinion is that: kde should konqer corporate market (and that gnomes Nautilus rocks - it is 'kde all in one', and that primary kde goal now is to think about marketing, etc. I was pissed off and I told him that his marketing philosophy is a crap & bulshit, but...following the tread on the kde-user mailing list I concluded that he must be a part of kde core team cause everybody there (I wont mention names)really share his opinion. So Matthias, I would really want to know what is kde's primary goal? Is it 'Free software is about fun, not about enterprise..' or 'marketing and business'? When James says on dot.kde.org that kde should take care about corporate users you guys call him a troll, and when I say on kde-user mailing list that kde corporate & business & marketing philosophy is bulshit and crap I have to shut up, and everyone is pleased by Andrew's words."
    author: "Antialias"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-03
    body: "Well, I'm not subscribed to kde-users, but tokde-devel and kde-core-devel, I don't know which Andrew you are talking about. Maybe he \"rulez\" on kde-users, but obviously not among the developers.\nWell, kde's goal are both: first it is fun to develop software and share them with others, and on the other hand we don't want to lose the competition with Gnome and Windows :-)\nAnd for this we have to make sure that KDE gets enough attention -> \"marketing\".\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "I like Konqui a lot an i like the interview :) AND i like to laugh :)))"
    author: "Wolfgang Hockertz"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-03
    body: "I think there are not really many people bashing the article, only very few, but very often.\n\nAnd I'd like konqi even as a business user. As a business user I'd dislike anything that hurts productivity, but as Konqi in a splash screen doesn't hurt producitvity - what the f**ck?"
    author: "AC"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "Is it true that Konqi is a SwampDragon and came from sunshinehome in Ankh-Morpok?"
    author: "Lobo"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "Hey, ... :)\nThe weather is fine today and your interview\nreally made me smile ...\n\nThanks for your work \nThomas"
    author: "Thomas"
  - subject: "James Alan Brown is a troll"
    date: 2001-05-02
    body: "In case you didn't realize yet: James Alan Brown is a troll\n\nHe already posted his eye-hurting fake-screenshot months ago in this discussion-forum without providing any code. He only wants to get some attention."
    author: "me"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "Do you know what Konqi and Katie is doing when you hear heavy breathing?\n\nThey are frying trolls!"
    author: "reihal"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "A Troll:\n\nDon't forget he has also got 3 of his customised Themes on kde.themes (Computers section KDE1.1.2) too. \n\nI respect your views but calling me a Troll is quite rich!\n\nI also feel it is important to express your view and findings even if those around you don't agree. That how things get changed in life!\n\nIn this real world I live in Linux is still thought of as a bit of a joke by far too many.\n\nTry reading all the Linux news groups and see what others are saying about KDE2 and GNOME bloatware before putting down people.\n\nIts easy to say good things on \"kde.org\" thats what most of you like to hear but it is also better point out not all are happy or if you wish just James.\n\nWhatever you may think I like KDE 1.1.2 use it, shout out virtues to one and all and am sure that given time KDE2 will be just as good. \n\nI question the number of gimmicks and GNOME look-a-likes only because its not the direction that I hoped KDE would go.\n\nAnd while most of you seem to say its really stable I have despite many downloads of various KDE2 bzip2 source and SuSE rpm files found it \"nice to look at but still unstable\"! So I am at fault for being open about this!\n\nso my systems at fault or I am at fault(oh how rich that is)!\n\nI can't help but wonder if  the rest of the open source community if asked may also have similar findings?\n\nThe bulk of you who replied up on this site are probably more inclined to give a bias view rather than hear what you don't wish too hear!\n\nAnd as per normal,  I am called a \"Troll\"  \nhow silly can we get?\n\nRegards,\n\nJames"
    author: "James Alan Brown"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-02
    body: "Okay, so since you keep on clogging dot.kde.org with complaints that others here don't have, I figure you have had time to actually submit a bug report? I looked at bugs.kde.org and nothing with your name is there, but your not including your email so I can't be sure."
    author: "Mosfet"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-03
    body: "Would you all prefer that I put up an article on some of the main Linux User groups and ask others to comment on KDE2?  \n\n1.  If they are happy with the way KDE2 is developing and to its usability/current  stability?\n\n2.  How many still continue to use KDE 1.1.2?\n\nWhile I can accept that many of you at kde.org seem to be happy with it I can't see any reason why it would not be a useful exercise to check how many others like it or dislike it or are getting the problems that I seem to be getting. \n\nregards,\nJames"
    author: "James Alan Brown"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-03
    body: "Okay, I guess that means you never submitted a bug report. Considering you take all this time complaining on the dot, and even are willing to take all the time to write an article, but aren't willing to do something as simple as submit a proper bug report, I think it's fair for people to consider you a troll."
    author: "Mosfet"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-03
    body: "Fine: OK I am a Troll (if that makes you feel better) \n\nWonderful and very grown up way to try to put down someone!\n\nYes I have submitted bug reports regarding Yamaha sound card problems also emails directly to one or two of the kde big wigs and you know what no one ever replied.\n\nNow then re: my last questions on News Groups?\n\nRegards,\nJames"
    author: "James Alan Brown"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-03
    body: "What email did you submit the bug report with? I can't find it... I see reports from other people about Yamaha, but nothing from you. Plus, a problem with your sound card is a long way from KDE being so unstable as to be unusable.\n\nIt doesn't make me feel better that your a Troll - you all waste other people's time. Going around and threatening about writing putdown articles also makes you look like a troll. But it's a free world, go ahead. All one has to do is point out that while you can take all the time in the world ranting about KDE2 you never even submitted a report on the bugs list. I could also link to this thread. How do you think this makes you look?\n\nAs far as a search for \"Yamaha\", I get:\n\nYamaha chips should work with the recent CVS versions of the 2.0.1/2.1 branch,\nwhere you can configure the sampling rate with kcontrol (set it to 48000 Hz).\n\nAs a workaround for 2.0 versions, you can start artsd manually, using something\nlike\n\nartsd -r 48000 -F 12 -S 4096\n\n   Cu... Stefan"
    author: "Mosfet"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-03
    body: "Look you need to look at past postings on kde.org  regarding KDE2 version releases\nand read their comments regarding stability and problems with the Yamaha SG sound cards from some of the users. Indeed I noted timings had been change to try to sort this problem out.\n\nYou need to look at my home page and see that for the last two years I have been promoting the virtues of KDE and please note the link back to kde.org\n\nhttp://www.jabcomp.force9.co.uk \n\nI do not dispute that some or even a very large number of users are happy with the way KDE2 is going.  I am concerned that KDE1.1.2 is marked up as \"obsolete\" and that its seems to me that KDE2 is full of to much glitter and lacks many of the nice packages contained within KDE1.1.2 and in my case seems no matter what I do runs \nquite badly and is still quite unstable.\n\nDragons are fun.. yes I agree but with respect many small business users that I am trying to promote the virtues of Linux running KDE tell me it's a bit too \"Kiddie\" \nfor them. \n\nMy personal view is this: until the day we can show a large number of Windows users that Linux is a great operating system, stable and usable, it will never be more that a passing fad. \n\nEveryone seems to jump on the bandwagon saying \"its your system try this distribution\" or that there maybe other reasons to this but never the less its factual in my case. \n\nI have 25 years of experience of PC building and 10 years of C++ programming and been using Linux now for some 4 years or so.  I am totally happy with SuSE Linux,\n(now using SuSE 7.1) and even compiling QT/KDE2 from kde.org source files still shows KDE2 as being unstable. Yet compiling KDE1.1.2 source files on the very same system work great and boy its stable.  So you tell me! (rather than calling me a \"Troll\")\n\nI respect and have 100% of gratitude for all of the many programmers working hard on KDE2.  That is not the issue!\n\nI do however feel that the release of KDE2 has more to do with trying to out run GNOME and that  KDE1.1.2 was dumped before KDE2 was stable and contained\nmore functional programs. (again a subjective view). \n\nRegards,\nJames"
    author: "James Alan Brown"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-03
    body: "Yeah, KFM was so much more functional than Konq, KPanel was so much more functional than Kicker, KEdit was much better than Kate, KMail in 1.x was so much more functional... And forget the new KParts model and all the other architecture improvements - that's all glitter! \n\nThis is pretty ridiculous. The feature set of KDE is so far beyond 1.x's that it would take pages to list them all.  \n\nAs far as stability, I haven't had KDE crash on me in months as far as I can recall without it being a bug in my own code..."
    author: "Mosfet"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-03
    body: "And I could say this:\n\n\"And how many pages to list all the Mosfet bugs too\"?\n\nbut it don't solve any of the problems!\n\nCome on now! Your getting totally silly with remarks like that!\n\nbe contructive dear chap\n\nregards,\nJames"
    author: "James Alan Brown"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-03
    body: "That's a pretty easy answer. Search for mosfet on the bugs list database.\n\nI find it ironic that your telling me to be constructive..."
    author: "Mosfet"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-04
    body: "\"you're\"\nIt's really really simple to understand."
    author: "me"
  - subject: "Re: Mythical Creatures Behind KDE: Konqi"
    date: 2001-05-04
    body: "Heh, my my do the trolls get upset..."
    author: "Mosfet"
---
Just over two years ago (end of April 1999), Konqi the dragon decided to join the brave crew of the KDE ship in an endeavor to <a href="http://www.kde.org/">conquer your desktop</a>. His help in promotional activities (as well as his intimidating flaming breath) has been invaluable to KDE's success. <a href="mailto:tink@kde.org">Tink</a> had the brilliant idea to <a href="http://www.kde.org/people/konqi.html">interview</A> the handsome dragon for the launch of a new series of the famous <a href="http://www.kde.org/people/people.html">KDE people interviews</a>. Tink made the site even more beautiful and the new series of questions even more interesting. And there are other goodies in preparation, as you can <a href="http://www.kde.org/people/people.html">check for yourself</a>. Thank you, Tink, for this refreshing story.
<!--break-->
