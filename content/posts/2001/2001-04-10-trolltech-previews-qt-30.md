---
title: "Trolltech Previews Qt 3.0"
date:    2001-04-10
authors:
  - "soehlert"
slug:    trolltech-previews-qt-30
comments:
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-09
    body: "Good stuff but still no Doc/View?"
    author: "AC"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "Huh?  What's a \"Doc/View?\""
    author: "not me"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "Document/View seperation, like if I want 15 views of the same text I can't do that with Qt."
    author: "AC"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "Sure you can. You just have to implement it yourself. But using Qt's signals/slots is very easy to do.\n\nFor an example, have a look at the ksysv (the KSYSV_2_BRANCH).\n\nregards,\nPeter"
    author: "Peter Putzer"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "Yes you can! You just won't have MS/MFC holding your hand while you do it.\n\nCreate a document class.\nCreate one or more view classes.\nCreate a controller class.\nConnect together with signals and slots.\n\nSee the KDevelop templates for implementation details."
    author: "David Johnson"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-09
    body: "I am VERY happy that they are developing this absolutely amazing toolkit for us! This lets the KDE guys concentrate on KDE instead of a toolkit, making them much more productive.\n\nThank you VERY much, trolltech, Qt rocks da house! I am impressed again and again by the pace of Qt's development!"
    author: "me"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-09
    body: "It seems that we already have TrollTech Linux 1.0. What's with those COM-like features ? Is it a some sort of binary standard ? Can they form alanguage-independent object model ?"
    author: "Hi"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: ">It seems that we already have TrollTech Linux 1.0.\n\nRemember, QT isn't just a graphics toolkit.  It's a true cross-platform development environment.  That means if TrollTech wants QT programs to be able to use a feature, they have to implement it all themselves inside QT so they can guarantee it works exactly the same way on every platform they support, with the same API. That's why they're inventing their own object model and stuff.  They have no ambition to create their own OS, they just want to improve their development environment."
    author: "not me"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "I though CORBA was the solution to this, CORBA has backing by over 500 companies and was made to solve just this problem and a few more."
    author: "Joe Blo"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "CORBA is hardly a component model (though it can be used as the foundation for one, as Bonobo has demonstrated).  My guess would be that the main motivation behind this new feature in Qt is to be able to use COM objects on Windows without sacrificing platform independance.  COM is after all still the only really widely used component model and a cross platform toolkit must have some support for it.  CORBA has got a good foothold in client-server applications, but is really only at an experimental stage in component architectures."
    author: "ac"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "You have absolutely no idea what your talking about."
    author: "AC"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "care to correct him then?"
    author: "none"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-13
    body: "COM is a windows only thing, DCOM is cross platform, but there exist almost no implimentations for it on anything but windows.  DCOM is also horribly complex, more-so than CORBA, it's easy to use in VB but it's heavily wrapped.  CORBA works fine as a component model, it just takes alot of extra code."
    author: "ac"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "It's called \"vendor lockin\"."
    author: "Hello"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "\"vendor lock in\""
    author: "ac"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "\"vendor lock-in\""
    author: "Eminem"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-09
    body: "Qt3 will have HTTP support, database access and even its own component model. KDE already has (or at least in CVS) all of these things, is there any concern about this duplication of efforts? Having a functionality twice seems like bloat to me :(\n\nHowever, we all appreciate the wizardry that's going on at Trolltech. I'm looking forward to Qt3 a lot!"
    author: "Haakon Nilsen"
  - subject: "Good question"
    date: 2001-04-09
    body: "I look forward to an answer from someone in the know. ;)\n\nI'm especially interested to hear about this component model and what it means for KDE.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "I'm interested in learning Trolltech component model as oppose to KParts.  And how will this new component model communicate ... is it via DCOP or CORBA.\n\nAnother question, will the new Qt designer replace KDevelop?  Or will it complement KDevelop?  From the features list I think Qt designer is going all out as a RAD which KDevelop has been doing for over 3 years.\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "TrollTech's main concern is money(at least I hope so), what they sell is an XP tool kit. There's X toolkits just as good that are free(please don't flame me for saying that, IMHO Fltk, Gtk and Motif are just as good) the reason that most people _buy_ Qt is because it's XP.  \n\n   Since most Linux users tend to smoke crack, I'm going to say that this is not a troll, so please be nice, it's just my opinion."
    author: "ac"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "<i>please don't flame me for saying that, IMHO Fltk, Gtk and Motif are just as good.</i>\n\nI won't flame you, but you're still just plain wrong :-)."
    author: "Guillaume Laurent"
  - subject: "You are clueless."
    date: 2001-04-11
    body: "You obviously have no clue what Qt and KDE are about.\n\nQt does not exist for KDE. KDE is built on top of Qt. \n\nTrolltech does not work on Qt according to whatever KDE or any other project built on Qt may have done.\n\nQt is a *toolkit* whose features are being extended and naturally that eventually catches up with functionality that may have been developed in higher application levels like KDE.\n\nQt is a totally independent development and if KDE stopped tomorrow Trolltech would be in no way affected.\n\nI hope that next time you whinge about \"bloat\" you actually give the topic more than 30 seconds' worth of thought."
    author: "Atilla"
  - subject: "Re: You are clueless."
    date: 2001-04-11
    body: "Too bad you couldn't actually answer the question. No one needs to hear garbage like what you just wrote."
    author: "Blsah"
  - subject: "Re: You are clueless."
    date: 2001-04-11
    body: "What I wrote in my reply was information that the original poster should have known before shooting his uninformed mouth off, using words like \"bloat\" and \"concern\".\n\nIf the guy had not postured like he had an opinion based on a clue, I would have been absolutely prepared to fill him in.\n\nYou apparently seem to have the same understanding of the issue as our friend and your intervention is just as sterile."
    author: "Atilla"
  - subject: "Re: You are clueless."
    date: 2001-04-12
    body: "Atilla, you chose your name wisely. No need to attack someone for this justified question. You're right that Trolltech is formally independent from KDE, but what would Qt be without KDE? Trolltech must be and surely is aware of KDE.\n\nBut quite some core KDE developers are employed by Trolltech, and I'm sure they all know what they are doing."
    author: "Alexander Kuit"
  - subject: "Qt 3.0 copy and paste behaviour"
    date: 2001-04-10
    body: "It says here that Qt 3.0 will be using two clipboards; one as a record of the last selection, and the other as a clipboard with standard Macintosh style behaviour:\n<br><br>\n<a href=\"http://www.freedesktop.org/standards/clipboards.txt\">freedesktop/clipboards.txt</a>\n<br><br>\n\"The correct behavior can be summarized as follows: CLIPBOARD works just like the clipboard on Mac or Windows; it only changes on explicit cut/copy. PRIMARY is an \"easter egg\" for expert users, regular users can just ignore it; it's normally pastable only via middle-mouse-click.\"\n<br><br>\nWill KDE 3.0 be adopting this behaviour (I personally find the standard X, 'copy to clipboard on select' unusable, and would like to just turn it off and forget it)?\n<br><br>\nThe article says nothing about the format of the clipboard - are there any inter-operability issues between, say the GNOME clipboard format, and the KDE one? I know that KDE 1.x had just 2 data types, but KDE 2.x can copy any mime type, but not much more. What format will the Qt 3.0 clipboard have?\n<br><br>\n-- Richard\n"
    author: "Richard Dale"
  - subject: "Re: Qt 3.0 copy and paste behaviour"
    date: 2001-04-10
    body: "I like the way as described from the paper Qt 3.0 ways from using cut and paste.  \n\nSelect should copy the selection to the PRIMARY and using \"Ctrl-X\" to put selection to clipboard is the proper way.\n\nComing from a windoze world, this is one of the most useful feature.  It took me over 4 months to relearn the \"cut and paste\" in the windoze world to accomodate for KDE lack of paste over selection feature.\n\nIn Konqueror, KDE team create an \"X\" button on the address bar to clear the line so you can paste URL if you intend to do \"paste over selection\".  I find that annoying after a while :)\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: Qt 3.0 copy and paste behaviour"
    date: 2001-04-10
    body: "A useful trick for your favorite X browser URL line: Ctrl+U erases the line. Works in Konqueror, Mozilla and Netscape, and pretty much everywhere else."
    author: "Toastie"
  - subject: "Re: Qt 3.0 copy and paste behaviour"
    date: 2001-04-10
    body: "Thanks a lot, it has been bugging me for quite a while now (since I moved to the desktop (KDE ofcourse)).\n\nI deleted the URL to be replaced first, then selected to to be pasted URL and copied with the middle mouse button. This trick make it a lot easier.\n\nBut this doesn't solve the problem for the general case, and this tells me the functionality is there but in the wrong place.\n\nI like the idea of two clipboards and at the same time it bugs me. \n\nThe windows kind of copy-paste seems to provide all the functionality. The Unix kind of select-paste seems to be quicker but less functional in other cases.\n\nJohan V."
    author: "Johan Veenstra"
  - subject: "Re: Qt 3.0 copy and paste behaviour"
    date: 2001-04-10
    body: ">Will KDE 3.0 be adopting this behaviour (I personally find the standard X, 'copy to clipboard on select' unusable, and would like to just turn it off and forget it)? \n\nYes, of course, since KDE is based on QT.  This is probably my favorite feature of the new QT.  Finally!\n\n>are there any inter-operability issues between, say the GNOME clipboard format, and the KDE one?   What format will the Qt 3.0 clipboard have? \n\nSupport for multiple clipboards is built into X itself.  Right now, everyone uses only one of the X clipboards by general convention.  The new QT will use more than one.  I believe the general effect will be that GNOME or other apps will only be able to \"see\" one of QT's 2 clipboards."
    author: "not me"
  - subject: "Re: Qt 3.0 copy and paste behaviour"
    date: 2001-04-10
    body: "please don't remove the 'copy to clipboard on select'.  I use it every day.  In my opinion it is much better than the \"windows-way\".  Maybe this could be made configurable...\n\nFilip"
    author: "Filip"
  - subject: "Re: Qt 3.0 copy and paste behaviour"
    date: 2001-04-10
    body: "Yes, I think the solution with 2 clipboards should  work well. It should be able please both types of users - 'normal' (like myself) and 'X power user' (like yourself). I assume some KDE code will need changing in moving from 2.x to 3.0, I don't think it will happen just by linking against Qt 3.0. \n\nCopy and paste in a modeless editor was invented by Larry Tesler at XEROX Parc for the Smalltalk-76 programming environment, in 1973-76. In Smalltalk, when you selected some text you had a large number of commands you could apply to the selection; 'again', 'copy', 'cut', 'paste', 'doit', 'compile', 'undo', 'cancel', 'align'. So it less likely that the command you would want to use 80% of the time would be 'copy'.\n\nIn the original world of X-Windows, you just had 4 xterms. If you selected some text, the *only* command you could do was 'copy'. Hence, the origination of this shortcut I believe.\n\nI would like to have a few of those Smalltalk commands back via a right click of the mouse - more convenient than cntrl+C etc. And what about an option to have scrollbars on the left again (the correct side for ragged right justified text)? The old ideas are still often the best!\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Qt 3.0 copy and paste behaviour"
    date: 2001-04-20
    body: "I've got an idea, how does this sound:\n\nThe primary clipboard is built in, always active, and in fact there IS no other clipboard.\n\nKlipper, and the QT clipboard, would have to be expanded to allow more then text to be added, and accomodate by, say, inserting \"<Unable to insert image>\", if attempting to do something stupid like copy a png from kword into kedit. \n\nKlipper displays a list of recently selected items, using perhaps konqy technology to provide image previews, and if an item is clicked, klipper copies that item to the real clipboard.  Middle mouse button becomes a shortcut for paste. \n\nKlipper would also have an option, \"Selected items automatically copied to clipboard\"\n\nThen, we could add a \"Klipmarks\" menu to klipper, to allow the user to permanently store any item in the clipboard. In addition to this, klipper would now also display the current clipboard contents.\n\nThis way, you would have only one clipboard, but still would have the power to do selection-to-clipboard, if you so chose.\n\nAny suggestions?"
    author: "Carbon"
  - subject: "Re: Qt 3.0 copy and paste behaviour\u00ba"
    date: 2003-07-28
    body: "Please don't remove that \"copy on select\"!!!\nI've found that new 'feature' removed in Opera 7, and now I'll stop using it.\nThat should be configurable... That limits the copy&paste to QT programs..."
    author: "Llu\u00eds Batlle i Rossell"
  - subject: "Re: Qt 3.0 copy and paste behaviour"
    date: 2001-04-21
    body: "Sounds like some good improvements coming.  \n\nAs for copying & pasting graphics, I think that's one of the most useful things with Mac or Windows, especially the Mac.  I can select part of a graphic (say, a screenshot) and then paste that directly into Word (or some other word processor). \n\nIt's a whole lot faster than creating the graphic, saving it, and then inserting it into the document."
    author: "Jeff Nelson"
  - subject: "Eye Candy?"
    date: 2001-04-10
    body: "Anyone got any screenshots up?\n\nErik"
    author: "Erik Severinghaus"
  - subject: "re: eye candy"
    date: 2001-04-10
    body: "www.themes.org/KDE\n\neye candy are only provided by THEMES. Or am I wrong ?"
    author: "AC"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "> Among its new capabilities are: database \n> support; data-aware widgets that provide \n> automotive synchronization between the GUI and \n> the underlying database\n\nWhat exactly does this mean?  Can someone describe an example where this feature might be used?"
    author: "Macka"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "I guess they are trying to emulate ActiveX style data binding.  The easiest example is you can have a GUI table widget that mirrors the information in a Database Table.  The only place this is widely used is in the Visual Basic world.  It's a nice gee whiz feature, but makes for poor program design in my book.  The GUI should never be so closely linked with the backend database."
    author: "David Clark"
  - subject: "ViaVoice & Qt 3.0"
    date: 2001-04-10
    body: "I think I could remember a headline a while ago, that Trolltech plans to add Speech-Input via IBM's ViaVoice.\n\nDoes anyone know if this is just postboned or completly canceled?"
    author: "Fabi"
  - subject: "Re: ViaVoice & Qt 3.0"
    date: 2001-04-10
    body: "I read somewhere, that the hold-up is IBM, still wondering, what license to slap on it."
    author: "nap"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "Wow this sounds great! Finnally Linux will have a RAD tools comparable to Delphi and Borland Builder. (Of course we allready have in the form of Kylix, but I prefer Qt and this will give us a RAD tool for free. As long as we do open source devel of course)."
    author: "Erik Engheim"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "Kylix is based on Qt and there will be an opensource version released real soon (May)"
    author: "me"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "That sounds nice. You mean they will make it free for open source devel or are they going to opensource the whole program? \n\nI know Kylix is based on Qt but I have tried Borland Builder and even though it was a great well integrated IDE I prefer Qt. I think the modularity and the design of the class hierarchy is one of the great strenghts of Qt. Even Borland Builder doesn't have that well designed class hierarchy.\n\nI really hope Kylix will be successfull on Linux though. It's important for Linux that companies like Borland make Linux products."
    author: "Erik Engheim"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "free for opensource devel"
    author: "me"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "Borland's compiler and IDE will be closed source, but there will be a free version that can be used to write GPL software(like Qt), the problem with Kylix though is that it's linux x86 and windows only."
    author: "AC"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "Borland will release their CLX-libs as OpenSource so Kylix application can compiled on every CPU-architecture."
    author: "Ravemax"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-10
    body: "Many modern windows applications (e.g. winamp I think) have different sections to their GUI that all move as one window but can be dettatched from each other, and then attached again.\n\nNormally I think this looks ugly but sometimes there is a need for this type of feature. Kppp's debug window doesn't seem right separated from kppp. Although I'm sure some would like the option to do that.\n\nDoes QT (or X) support doing this yet?"
    author: "Daniel"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-11
    body: "Try using xmms - same thing. \nIt is possible using X calls. \nDon't know if Qt abstracts this."
    author: "Rob"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-11
    body: "X supports this, but Qt dosen't(I think, I've never seen it) so you probably have to use 2 programs and something like CORBA, or use some X code in your app."
    author: "AC"
  - subject: "Re: Trolltech Previews Qt 3.0"
    date: 2001-04-11
    body: "Although it doesn't show the seperation (which\nxmms shows is possible) Noatun is written using\nQt and uses non rectangular borders for its \nKjofol plugin. \n\nIt might be an idea to look at that source...\n\nAlex"
    author: "Alexander Kellett"
---
<a href="http://www.trolltech.com/">Trolltech</a> is now previewing Qt 3.0: <i>"Among the new capabilities in Qt 3.0 are: database support; data-aware widgets that provide automotive synchronization between the GUI and the underlying database; a new version of Qt Designer that has become a full-function GUI builder; comprehensive internationalization; and a host of other improvements."</i>
Check out <a href="http://www.trolltech.com/company/announce/30pre.html">the announcement</a> for full details; Qt 3.0 snapshots can be found <a href="ftp://ftp.trolltech.com/qt/snapshots/">here</a>.





<!--break-->
