---
title: "KDE::Enterprise Launched"
date:    2001-09-25
authors:
  - "Dre"
slug:    kdeenterprise-launched
comments:
  - subject: "How many developers work for/with KDE? And Gnone?"
    date: 2001-09-25
    body: "Are we really conquering the linux desktops out there?\n\nAndrea"
    author: "I want to believe"
  - subject: "That's a double dosis of Enterprise this week!"
    date: 2001-09-25
    body: "Since the fifth installment of Star Trek will start on Wednesday if I am correct. Coincidence or timing?"
    author: "Rob Kaper"
  - subject: "Re: That's a double dosis of Enterprise this week!"
    date: 2001-09-25
    body: "nerd ;)"
    author: "Maarten Stolte/Datadevil"
  - subject: "WOW!"
    date: 2001-09-25
    body: "WOW! This is exciting news!  Does this mean that we'll see some Admin/deployment tools, that will make it easier to deploy and manage KDE in an organization. I can already see a profile editor coming up.\n\nThanks to the KDE TEAM for making *NIX Desktops a *reality*\n\niCEmAn\n~~~~~~"
    author: "iceman"
  - subject: "does this compete....."
    date: 2001-09-25
    body: "is the plan hear to compete with Ximian for the desktop? is there going to be a similar KDE set up tool like ximian door man and admin tools like red carpet?\n\nthis would realy make KDE a more useful environment than Ximina gnome since KDE is lightyeas ahead of Gnome.\n\n\nkick butt!!!!"
    author: "Jeremy Petzold"
  - subject: "Re: does this compete....."
    date: 2001-09-25
    body: "you are dreaming troll.............\n\n[X] i'm a gnome-troll and don't understand that the world choosen 'kde'\n     as the leading desktop on unix-plattforms\n\n[X] I'm a gnome-troll and mean gimp is a gnome-programm\n\n[X] I'm a gnome-troll and mean gtk+ means world-domination"
    author: "steve"
  - subject: "Re: does this compete....."
    date: 2001-09-26
    body: "You are a dreamer gnome :-)\n\nIf you mean that gnome is better, why are you loosing your time\nat this side of the modern world?\n\nIMHO, KDE is the Desktop with the biggest potential on the Unix world,\nwant it or not. And this kind of initiatives (enterprise, edukation, kdeleague)\nshows that this guys are meaning serious about their job."
    author: "Filma"
  - subject: "Perhaps best known for his work?"
    date: 2001-09-26
    body: "\"Jono Bacon, perhaps best known for his work on Kafka and the KDE Usability Study\"\n\nIf there was any relationship between the number of times I have seen Jono accredited with having done work on the supposedly important/impressive Kafka project and the amount of useful code he has contributed to it, Kafka would by now have overtaken DreamWeaver in terms of functionality, usability and market penetration.\n\nIn truth, this project to bring a WYSIWYG HTML editor to the KDE is around two years old now, and has yet to reach its first ever release (as in release early, release often). Jono has now left that project for which he still commands massive publicity, and has started an even more vapourware-like project to create something like Shockwave Flash in KDE. Of course the project website doesn't detail how he hopes to get the industry-wide support needed to get _anyone_ to create content for it, but it does at least admit that he is hoping that someone else will write most of the code.\n\nPerhaps Kafka's problem is that people who know how enough about HTML, CSS, JavaScript etc to make a good job of it would never use a WYSIWYG editor anyway, prefering to hand-code. Most people working on free software are scratching an it, which few suitable hackers have in this case.\n\nI can barely visit a free software news site or open a dead-tree Linux magazine, at least here in the UK, without seeing similar comments, often accompanied by his picture, which is enough to make everyone outside the KDE community think that KDE hackers all look like Marilyn Manson. He even has an opinion column in 'Linux Format', where he is presented as a great leader of the open source revolution. He is often also accredited as having founded 'Linux UK', which sounds like some sort of important, possibly Linus-blessed, national organisation, whilst it is infact just the domain name that he registered for his personal web site, presumably with the intention of catching the eye-balls of those looking for a site that really is in some way official.\n\nIn an early fit of self-importance and self-promotion, I remember the time that he came to a LUG meeting and announced that he was the official UK national co-ordinator for an upcoming world-wide installfest day of action. About ten of us spent two or more hours that night discussing how we could go ahead with a local installfest. We uncovered a lot of problems to overcome, and set about over a couple of months trying to solve them (venue, indemnity, ensuring a good supply of expertise, etc). After the initial meeting Jono didn't join in any more, and we eventually abandoned the installfest idea through lack of support. Lots of people all over the country did manage to put on installfests, with some success. And Jono got some of the credit for it.\n\nI was very pleased to see his useability study site. This indeed is a very worthwhile contribution, at last. I hope very much that many reports will come in and good things will come of it. He faces some difficulty with the project, as he has to get people who are not very expert with computers to use KDE and then write about it. I know he has used his media contacts to great effect in getting the word out to those users - so its all been for the good in the end. There can't be many that run Linux or a Unix with KDE but don't have much computing experience. Sun had to pay people off the street to use GNOME for their thorough study. Can the usability project find a way to do similar things?\n\nThis enterprise project is very interesting, and I hope that it accelerates KDE's penetration of the business world. These case studies provide great ammunition when trying to put forward a compelling business case for using KDE. I wish it could have come up with a different name though. Perhaps business.kde.org ? The problem is that unfortunately computer science students like Jono, and many other people that haven't yet had their first proper job in the IT world, don't grasp what the word 'enterprise' means in business computing. When articles in magazines and on web sites get titles like 'Is Linux Ready For The Enterprise?' they are not refering to the quality of the end-user experience. Enterprise computing is about the back end systems. Multi-terrabyte databases, enterprise resource planning, customer relationship management, n-tier web applications, application servers, these are what the term refers to. Products like Oracle 8i, IBM DB/2 and WebSphere, SAP R/3, BEA WebLogic. The articles explore whether the applications are in place, whether the tools exist for easy and cheap development of enterprise applications, and whether the Linux kernel has adequate support for the features needed to deliver these kind of apps, running on serious hardware, with good uptime. I have never heard any suggestion that the KDE project might move from desktop software to server software arena.\n\nI struggled all through my CS degree trying to get a handle on what enterprise computing was all about. It was this that brought me to Linux, as it was such a great way to turn my computer into a powerful system for server-type applications, which I wanted to work on after graduation. Its only since I have been out in industry that I have got a good understanding of how all the various enterprise products fit together, and what their advantages are to business. A project like this really needs some leadership and direction from people that actually use Linux in their businesses - CTOs, CEOs, MDs even. Jono would do very well to encourage the contributors to this project to get their managers involved too, to give the project some serious business credibility.\n\nThe danger of having the name of the project wrong in this way is that business people take a look at the site, and go away with the impression that the nearest Linux has progressed towards being enterprise ready is having a decent desktop environment, and subsequently pass straight over all the products I listed earlier. I hope that a good number of them at least go away thinking about the money they can save by migrating their company away from being part of the licensing and subscription revenue stream of their current suppliers.\n\nIn summary, I very much welcome this project and its goals. But if I hear one more time about the great work of the famous KDE hacker Jono Bacon and his amazing Kafka project I will scream ;-)"
    author: "Mark Roberts"
  - subject: "Re: Perhaps best known for his work?"
    date: 2001-09-26
    body: ">In summary, I very much welcome this project and its goals. But if I hear one more time about the great work of the famous KDE hacker Jono Bacon and his amazing Kafka project I will scream ;-)\n\nSince you can summarize all your post in that one sentence, why did you waste the time needed to type out the other part? :-)"
    author: "Carbon"
  - subject: "Re: Perhaps best known for his work?"
    date: 2001-09-26
    body: "The same thing applies to my mother and my girlfriend as well... They can talk for 20 minutes about something that would fit in one or two sentences, without losing content, if you just leave out the unimportant parts. :)"
    author: "Per Wigren"
  - subject: "Re: Perhaps best known for his work?"
    date: 2001-09-26
    body: "Hi,\n\nSorry to hear some of your grumbles. Let me clear some things up:\n\n (1) Kafka was started and I joined the team. I tried to bring Kafka out of the dead, but to no avail...help was thin on the ground (as Niko Zimmerman who maintains it now will confirm). I didnt want to flog a dead horse so I quit it.\n\n (2) Linux UK was a website run by an open source team that is now Think Open. I left that due to KDE interests.\n\n (3) I assume you are from northants LUG. I visited the LUG when I lived in Bedfordshire and left when I came to Uni. No self importance was assumed at all...I came to the LUG because I was interested in Linux. At the time I was helping the Installfest effort. I decided to leave when I realized I didnthave enough time to fullfil the project.\n\n (4) Thanks for your kind words on the usability study. I set the project up to develop a framework for developing usability for KDE. I am not professing at all to be experienced in usability research, but I am developing the site to accomodate it. People have joined the team who are experienced in usability to help the project.\n\n (5) As for the enterprise project, you make some valid points. I encourage everyone including managers, developers, marketing people etc to get involved. Again I am developing this project to provide the framework for demonstrating KDE in the business world. We have had some feedback from people and it is looking interesting.\n\nTo clear this up, I have an interest in Linux and KDE, and I like helping these projects. I am in no way inclined to publicize my work more than an announcement. If you look across KDE and Linux there are people *far* more capable and published than I. I try to use my skills (which are mainly web based) to develop frameworks so a project can be run by a team. I have done soem application development in the past, but I am stepping down from that as I dont feel I am particularly suited to it.\n\nLike any open source hacker, they get acknowledgement from people within their community when they do some work. The thing about some of the stuff I have done is that it has been announced in places other than the community which I work in, and hence gets more publicity than it needs.\n\nAs for the column in Linux Format...it is just that. It is a column and an opinion, and nothing more. I use it to discuss ideas floating in my head, and that is what the Linux Format team intended it for.\n\nIn the open source world we all have a chance to contribute, and I dont want to give off any impression that I have self importance at all. I think the fact that you feel that I am making KDE hackers look like metallers in rubbish...does that mean that mean that other people who are in the public eye every so often are representatives of their community...no it doesnt. I am not a representative of KDE or Linux, and I am merely someone who contributes and has an interest in it."
    author: "Jono"
  - subject: "Re: Perhaps best known for his work?"
    date: 2001-09-26
    body: "Hi Jono,\n\nThanks for the clear ups. I'm sure I keep seeing you attributed (in the press) as the leader of the Kafka project (with the suggestion that it is one of the most excellent applications in KDE), and the founder of 'Linux UK'. I expect this is the common problem journalists have when reporting specialist topics - they like to bump up the seriousness of their sources, and once they find a source they are reluctant to look for any others. Not that I am comparing you in any way to him (that would be terribly insulting, and I mean no offense), but you see this phenomenon a lot concerning John Vransevich - if you believed the major media, the guy is the guru of all security experts, and gets interviewed, quoted and attributed for everything.\n\nAs for your photo in Linux Format, I am a fine one to talk (take a look at the photo on the front (and only) page of my web site www.taurine.demon.co.uk). I'm really reacting to the photo the magazine has next to every contributor in the magazine. Its just that they all look like geek stereotypes (just like I do in fact), and I think it plays up to a stereotype that I know isn't true. I must know thirty or more Linux users personally, and myself and Jono are the only ones I know with long hair, aggressive t-shirts, funny beards, etc. I worry that when mainstream computer users, business IT people, etc, pick up Linux Format to find out what lies behind the hype of this Linux thing, they will go away with the stereotype instead of the reality. We all know that geeks are just like everyone else in most ways, but we also know that mainstream people would think 'weirdo' before they would think 'credible' if they saw a picture of RMS (as an extreme example). Its kind of the opposite of the thing about email making everyone equal. Come on, admit it, in the photo you are pulling your best heavy-metal face ;-) Is it from a promo shot for your band?\n\nI greatly admire your recent KDE web projects. You are acting as a catalyst to make something big happen, making good use of your web development skills. The style of leadership by example (I mean putting in a visible effort) is a good one. I would be using it in my current projects if I had enough time and effort. You are very lucky to be at uni at the moment - I wish I still was ;-) Of course it always seemed like I didn't have enough time, but now I look back I realise how relatively free I was...\n\nYou really need to find ways to promote the useability project to the people who are least likely to have heard of it because they are just computer users, not /. and .kde readers. I'm sure you have some plans with this regards already. Perhaps you could influence Linux Format to do an article on how to write usability and bug reports? There are an increasing number of places where KDE desktops are available in shared computing areas, particularly in universities and Internet cafes. Perhaps the people that have pioneered these projects could be persuaded to try to survey their users? Indeed, there is a strong synergy between your useability and enterprise projects. The companies that are reporting their case studies of KDE use in mainstream IT type could perhaps be persuaded to survey their employees?\n\nAlso, on the useability site, have you considered posting reports of the UI design changes that occur as a result of each usability report. By showing that something really does happen when you contribute to the project, you may inspire more people to take part. Just a thought.\n\nAnyway, the very best of luck with your projects, and I hope I will continue to see your name come up in future press releases!\n\nBest regards,\nMark."
    author: "Mark Roberts"
  - subject: "Re: Perhaps best known for his work?"
    date: 2001-09-26
    body: "Hi Mark,\n\nThanks for your kind reply.\n\nThe difficult thing in being part of the whole Linux effort is putting across the right impression. I agree completely with the stereotype's of Linux people. It is difficult to get a fair representation.\n\nThe last thing I would want to put across is that the stuff I am doing is particularly ground shakingly important. I just try to fill in the gaps, although I suppose a project which gets as much PR as KDE, there is bound to be publicity at all levels.\n\nAs for the usability project, we are branching out in a few areas. We have a guy called Matt Clark working on a research part of the project, and we will be posting usability case studies on the site. Some interesting things should be happening with this project over the next few weeks.\n\nCheers,\n\n           Jono"
    author: "Jono"
  - subject: "Yes, Enterprise is for the Server"
    date: 2001-09-27
    body: "I'm afraid Mark is right.  Enterprise software is something\ncompletely different.  business.kde.org would be more accurate."
    author: "steve"
  - subject: "SuSE is missing"
    date: 2001-09-26
    body: "About half of the business cases are currently running under \"other Linux\" because someone forgot the most popular KDE-distribution. \n(RedHat is Gnome-based and hates KDE)"
    author: "Roland"
  - subject: "Re: SuSE is missing"
    date: 2001-09-26
    body: "I assume you mean SuSE. I did not spot this...I will add this today."
    author: "Jono"
  - subject: "Re: SuSE is missing"
    date: 2001-09-26
    body: "Was it the bold \"SuSE is missing\" in the title that gave it away"
    author: "me"
  - subject: "Re: SuSE is missing"
    date: 2001-09-27
    body: "\"(RedHat is Gnome-based and hates KDE)\"\n\nWhat does GNOME-based mean to you?\n\nAnyway, Red Hat doesn't hate KDE. In their latest version, the user has to choose the default desktop and KDE is there as an option."
    author: "Evandro"
  - subject: "Gnome hates KDE"
    date: 2001-10-03
    body: "I have spend a lot of time to bring GNOME and KDE people to wrk with each other and to let the best of the two worlds work together. Both are going now the same way and are helping each other on their own. KDE is strong in the technical design and efficiency for big multi user systems, GNOME is strong in the graphical design and speed for small standalone systems. Both have their merits, we should use them in those situations where they are designed for. Both work now together and aren't developing two times the same thing. This reduces the development cycle and the usability for both desktops.\nDo not try to set up people against each other : that's is a BAD ATTITUDE!\nAND shamefull for a true Linuxlover.\n\nThe guy behind the scene..."
    author: "Patrick Op de Beeck"
  - subject: "Kafka plus Quanta"
    date: 2001-09-26
    body: "I want to know one thing: Why they didnt add Quanta Code C++ features into Kafka? if they do it, they will run faster with the Kafka coding. And other question: Will be Kafka similar to DreamWeaver ? i hope so. I love Dream :-)"
    author: "Daniel"
  - subject: "Interesting quote from news.com article"
    date: 2001-09-27
    body: "As many obviously have seen already, Mandrake 8.1 has been released with, among other things, KDE 2.2.1. \n\nThere is an article on News.com here: http://news.cnet.com/news/0-1003-200-7321032.html?tag=mn_hd\n\nI mention this article because it has an interesting quote:\n\" Mandrake version 8.1 includes several new features. Among them is the new version 2.2.1  of the KDE desktop interface, released Sept. 19. KDE 3.0 is scheduled to be released in beta version in December and in final form in February.\"\n\nCan you say World Domination :D \nJust that they report about Mandrake's latest release and mention KDE is cool, but that they mention KDE3.0 is IMHO really cool and surprised me :)"
    author: "Joergen Ramskov"
---
<a href="http://www.kde.org/people/jono.html">Jono Bacon</a>, perhaps
best known for his work on <a href="http://kafka.kde.org/">Kafka</a>
and the <a href="http://usability.kde.org/">KDE Usability
Study</a>, has announced the launch of his most recent endeavor:
<a href="http://enterprise.kde.org/">KDE::Enterprise</a>. Since KDE
is probably <em>best</em> suited for deployment in enterprises and
educational institutions at this juncture, it is at once wonderful
and very exciting to
see this useful initiative come to light shortly after the
<a href="http://edu.kde.org/">KDE Edutainment project</a>,
<a href="http://dot.kde.org/1001099410/">announced</a> a few days ago.
The press release follows.


<!--break-->
<p>&nbsp;</p>
<p>
DATELINE SEPTEMBER 24, 2001
</p>
<p>
FOR IMMEDIATE RELEASE
</p>
September 24, 2001 (Internet).   The
<a href="http://enterprise.kde.org/">KDE::Enterprise Initiative</a>
was launched today, with the goal of improving, integrating and customizing
the <a href="http://www.kde.org/">K Desktop Environment</a> (KDE),
for development and use by enterprises.   KDE is the
leading desktop for Linux and other Unix systems.
</p>
<p>
The aim of the project's website is to create an information repository
of successful business cases using KDE, to provide contact points
for KDE support, and to further the awareness of KDE as a business platform.
</p>
<p>
<a href="mailto:jono@kde.org">Jono Bacon</a>, KDE developer and founder of
the project, stated, "KDE is rapidly developing as a preferred
interface for Linux and UNIX based systems. I want to present KDE as a
suitable solution for the enterprise, and the KDE::Enterprise project is an
initiative to do this."
</p>
<p>
"This project is very exciting because it will provide businesses with
real cases and real awareness of how much money they can save by switching
to KDE/Linux for all or part of their enterprise network," added
<A HREF="mailto:pour at kde.org">Andreas Pour</A>, Chairman of the
<a href="http://www.kdeleague.org/">KDE League</a>.  "KDE is ready and
poised to make substantial inroads in the enterprise, schools, universities
and other institutions, and a forum was needed to catalog the successes,
together with any problems and their solutions."
</p>
<p>
The initial release of the KDE::Enterprise website
includes an interface for submitting successful usage
of KDE in a business case.
This service is intended to build into a substantial
information repository demonstrating KDE as an
enterprise aware platform.
</p>
<p>
Jono Bacon added, "Free software has demonstrated it's successful usage
and value in the enterprise with other notable projects.  KDE is
now ready to tackle the business desktop, and the KDE::Enterprise
initiative will seek to publish and further this cause".
</p>
<h3>About KDE</h3>
<p>
KDE is an independent, collaborative project by hundreds of developers
worldwide working over the Internet to create a sophisticated,
customizable and stable desktop environment employing a component-based,
network-transparent architecture.
</p>
<p>
Please visit the KDE family of web sites for the
<a href="http://www.kde.org/faq.html">KDE FAQ</a>,
<a href="http://www.kde.org/screenshots/kde2shots.html">screenshots</a>,
<a href="http://www.koffice.org/">KOffice information</a> and
<a href="http://developer.kde.org/documentation/kde2arch.html">developer
information</a>.
Much more <a href="http://www.kde.org/whatiskde/">information</a>
about KDE is available from KDE's
<a href="http://www.kde.org/family.html">family of web sites</a>.
</p>
<hr noshade size=1 width="95%" align="center">
<font size=2>
<em>Trademarks Notices.</em>
KDE, K Desktop Environment and KOffice are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
</br>
<hr noshade size=1 width="95%" align="center">
</font>
<table border=0 cellpadding=8 cellspacing=0>
<tr><th colspan=2 align="left">
Press Contacts:
</th></tr>
<tr valign="top"><td align="right" nowrap>
Europe:
</td><td nowrap>
Jono Bacon</br>
jono@kde.org</br>
(44) 797 414 7045</br>&nbsp;</br>
</td></tr>
<tr valign="top"><td align="right" nowrap>
United States:
</td><td nowrap>
Andreas Pour</br>
Chairman</br>
KDE League, Inc.</br>
pour@kde.org</br>
(1) 917 312 3122
</td></tr>
</table>
</body>
</html>

