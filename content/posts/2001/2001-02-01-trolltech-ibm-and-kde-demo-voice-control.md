---
title: "Trolltech, IBM and KDE to Demo Voice-Control"
date:    2001-02-01
authors:
  - "rmoore"
slug:    trolltech-ibm-and-kde-demo-voice-control
comments:
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Let me just say that this is beyond cool.  Once again, the TT/KDE guys, as well as IBM, show their sheer brilliance.  My hat's off to them all."
    author: "Joe KDE User"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Hmm, text to speech looks really cool. It would save a lot of reading. I'm guessing the speech to text would require a pretty beefy machine though."
    author: "Ashleigh G"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Do you really think it would save so much reading? I don't. Thing is, that (at least I) can read much faster than I can speak, meaning ViaVoice reading something to me will always be slower than me reading myself.\n\nReading is great. All you have to do is move your eyes to the position that you want to read. How will ViaVoice handle moving to a different text position (maybe because you're not interested in this paragraph)? Its like comparing a tape with a cd (access-wise). Imagine reading Slashdot! It would probably read out the leftmost column first (faq, code, osdn, awards, privacy etc.) Wow, great! Then, with some bad luck, you're not interested in the first new item and have to spend 30 seconds on listening to it?\n\nWell, speech recognition and text2speech is cool, but I cannot imagine how to make especially the latter very useful. I just hope they find a way!"
    author: "me"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "This is about VOICE RECOGNITION\n\nNOT \n\nTEXT SYNTHESIS!"
    author: "Anon"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "No its not!\n\nRead the darn article: \"In addition, however, ViaVoice on Qt supports: TTS (text to speech)\"\n\nThe someone said: \"That would save me a lot of reading.\" The above poster replied to that. period."
    author: "bah"
  - subject: "I can immagine uses."
    date: 2001-02-01
    body: "How about when you can have your computer tell you about something when you're not at your desk?\n*phones home.\n*computer answers (text2speech) \"You have 3 email messages on your work account. One is marked urgent. Would you like me to list the subject lines?\"\n(voice recognition) \"no. Tell me if the door is locked.\"\n*computer answers (yes jim, the house is secured.)\"\netc etc."
    author: "vod"
  - subject: "A more realistic conversation"
    date: 2001-02-01
    body: "<i>*computer answers (yes jim, the house is secured.)\" etc etc.</i>\n<P>\nA more realistic conversation:\n<p>\n<B>Dave:</B> \"Open the pod bay doors, HAL\"<P>\n<B>HAL:</B>  \"I'm sorry, Dave, I'm afraid I can't do that...\"<P>\nflounder"
    author: "flounder"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "There is the issue of \"accessibility\" which is the point IBM usually brings up(not only the deaf & blind, but also access from alternative methods, such as handheld devices...or as in the previous message, through the telephone."
    author: "Henry"
  - subject: "Synthesized speech faster then human speech"
    date: 2001-02-01
    body: "This is off topic, but here goes:\n\nSynthesized speech can be made a _lot_ faster then real human speech.\n\nI have taken part to a project in which we created a synthesizer for blind people. We were able to make the thing speak three times the normal speed without any deterioration of recognisability.\n\nContrary to what you may think, it is not possible to speak this fast normally, just record yourself speaking and then compare files in a wave editor.\n\nOTOH, if you are a fast reader, you are right - there is no way to make a synthesizer as fast as reading, but it can get pretty close.\n\nThe real problem is that browsing with read aloud text is really hard."
    author: "Pirkka Jokela"
  - subject: "Re: Synthesized speech faster then human speech"
    date: 2001-02-01
    body: "This is even more off topic... once years ago I won a contest reciting the names of all 66 books of the bible in 13 seconds. It's amazing how fast you can say a tongue twister with a few days practice.\n\nGenerally though you cannot speak that fast. As someone who has done public speaking and taught it I know that we can hear and process words something like 6 times faster than we can talk. For a speakder this means you must adjust vocoal inflections to hold audience interest. If you think about it you can look at something, think about something else and listen to someone talk so you can process information much faster than anyone can speak...\n\nHowever I imagine very fast speach may take some getting used to. Classically it is considered good form to speak slower to be able to digest what is being said. Our prejudice against speach rates are that slower rates are perceived as lacking mental ability and faster rates are perceived as crooked types. These are in relation to our rate of speach. 5% faster is considered intelligent... so it would be interesting also to see how people perceive fast synthesized speach and how it might impact us."
    author: "Eric Laffoon"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-02
    body: "text to speach is handy.\n\nI work for an ISP, use KDE all day and have quite a few screens - so logs of problems are missed.  recently got text to speach on my log files courtesy of Festival - works pretty well - no matter what screen I'm workign on  I know when things go wrong.\n\nNeed to admit  I woudl feel an idoit talkign to the computer in teh office though ;-)\n\nhave fun,\n\naid"
    author: "aid"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2003-06-24
    body: "A hammer is a hammer not a hoe!!!!\n\nThe point is not how a technology like a spread sheet will do the same old things in the same old way... Likker aps re-define the eway we get our job done... The succesfull ones are seldon incremental...\n\nE.g ask yourself HOW could I do WHAT that was not practical before this technology existed and you will likly see it's true impact on man...\n\n-tdh"
    author: "tdh"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2007-01-17
    body: "It would be good for those who are disabled and need a way to interact with a system other then typing . I cant wait to see this. I would also like to know th eplanned cost of doing this and where we can down load it...."
    author: "Richard"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Hmm,\n\nI was just thinking about license issues. \nAnybody who knows more?\n\n\tMax"
    author: "Max"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "IBM's product is commersial..."
    author: "Jo"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Does this mean we`ll have the whole liscens-issue all over again?"
    author: "coba"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Yes, exactly. All of the KDE developers have unanimously decided to distribute KDE only in binary form from now on, so that ViaVoice can be integrated into KDE. Some people raised concerns about this issue, but were quickly convinced by the clear advantages that a speech-driven desktop would bring them.\n\nThe Gnome Foundation immediately reacted to this revolutionary improvement by also closing all of their sources to be able to add AOL's Instant Messenger to their distribution.\n\nbah!"
    author: "anonymous"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "I'd like to know if IBM will support internationalization. Will IBM release some utils to make localized phonems and train Voice recognition for another languages than English, Spanish, French, German? Or will IBM localize ViaVoice technology itself? I don't think so."
    author: "Petr Husak"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "You might be surprised- while living in Japan I wanted to buy via-voice (English) for a friend... couldn't find it anywhere because the only version available was the *localised* one for the Japanese market.  I was told by several people it did very poorly at recognising English- reasonable proof to me that it had been well localised (in Japanese there are no solitary consonant sounds other than 'n'- everything is consonant+vowel.)"
    author: "Julian Rendell"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-05
    body: "well, Japanese market is really much bigger\nthen eg. Czech, Slovakian, Hungarian ,..."
    author: "Milan Svoboda"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Just one quick troll: \n\nGood work, boys!:) Only about a year and a half behind GTK this time. \n\nGVoice, a Wrapper library around ViaVoice that provides call-back gtk+ signals, was initially announced sometime around June of 1999. \n\nI normally wouldn't post trash such as this, but every article posted here is always accompanied with at least 3 comments to the effect of \"What do you have to say about this, GNOME?:P\" So I figured, if the KDE peeps troll on their own forum, I might as well too:) Happy replies:)"
    author: "James"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Oh no more postings like this one, please...\n\nSo just use your GVoice and your call-back signals and feel happy, o.k. ?"
    author: "Thomas"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "The comment was in jest..:)"
    author: "James"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "ah... what does 'in jest' mean ?\n\nIf you want to compare Gnome with KDE please use\nneutral ground (not dot.kde.org).\nAnd a little bit self praise is allowed on both sides I think :) (this includes statements like\n'what's your answer Gnome?'-justified or not..)"
    author: "Thomas"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-02
    body: "Is there such a thing such as a _neutral_ forum for talking about Gnome VS KDE ? :)"
    author: "Batard"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-02
    body: "Sure there is... Microsoft web communities =)"
    author: "Divine"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "really?\n\nHonestly, where can I find this, its not on sourceforge!\n\nAlso, can I use every GNOME/Gtk App with it, or do they have to be modified? If yes, how far along are they?\n\nI don't think a Speech-Controlled Desktop makes anyone work faster (except for dicatting maybe), but playing around with this feature surely is cool. Also, disabled^W differently abled people should get some advantages from this, I guess."
    author: "me"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "\"...should get some advantages from this, I guess.\"\n\nhmmm... depends if you consider being able to use a computer an advantage or not, I guess.\n\n\"I don't think a Speech-Controlled Desktop makes anyone work faster...\"\n\nit enables some of us to work, at all."
    author: "disabifferently abled"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "yeah, even for those people afflicted with something as common as carpal-tunnel syndrome, this would be a huge benefit."
    author: "will"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Freshmeat:)"
    author: "James"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "From Freshmeat:\n<a href=\"http://www.cse.ogi.edu/~omega/gnome/gvoice/\">http://www.cse.ogi.edu/~omega/gnome/gvoice/\n</a>\n\nFrom <a href=\"http://www.cse.ogi.edu/~omega/gnome/gvoice/\n\">http://www.cse.ogi.edu/~omega/gnome/gvoice/</a>:\nFile Not Found\n\n\nVapor?"
    author: "dimator"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-02
    body: "Try here:<p>\n\n<a href=\"http://www.linuxberg.com/x11html/preview/9417.html\">http://www.linuxberg.com/x11html/preview/9417.html</a>"
    author: "Tsujigiri"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "A simple wrapper library around the native and very specific ViaVoice API is nice to have, but far from being sufficient - unless you explictely want to bind GPL'd applications to a non-free component.\n\nI don't think you want that, James :)"
    author: "Matthias Ettrich"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Dunno, this is a good point.  I would be curious to understand the difference though, because in all honestly I don't.  Evidently I'm missing something important here (And admittedly I just skimmed the article).  But I really don't see the difference between GVoice and the QT integration:)\n\n- James"
    author: "James"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-02
    body: "Are you sugesting that By accesing VV throgh QT, KDE insulates itself from the license isue ?\n\nPleas clarify."
    author: "Forge"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-02
    body: "> Are you sugesting that By accesing VV throgh QT,\n> KDE insulates itself from the license isue ?\n\nNo, I'm not suggesting that. \n\nUnfortuntely, I can't speak openly about the technology as it's just a demo yet (I suggest you try to contact IBM at the LinuxWorld).\n\nWhat I can say is that all involved parties are aware of the licensing issues and that I personally won't do anything that might lock KDE into a non-free product (been there, done that)."
    author: "Matthias Ettrich"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "and <em>how on earth</em> is that ever supposed to work properly...<br>\nFacts:\n\n<li>KDE2 <em>needs</em> alsa sound drivers</li>\n<li>ViaVoice <em>needs</em> OSS drivers</li>\n<li><em>They just don't work together.</em></li><p>\n\n\nI just tried to use ViaVoice's cmdlinespeak example to say something to me, it hung after speaking, with strace I found a syscall that would not finish which I reported to the alsa bugs page, let's wait and see.<p>\nDon't tell me about soundcards, tried on different machines with different soundcards..."
    author: "Mathias"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "<em>KDE2 needs alsa sound drivers</em><br><br>\n\nHuh? How did you come to this statement???\n<br>\nLukas"
    author: "Lukas Tinkl"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "crystal ball?\ntea leaves?\nor just the fact that kde2 would not want to hoot at me when using the plain old kernel drivers, and anything KDE2 with sound is linked against the alsa libs???"
    author: "Mathias"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "<em>and anything KDE2 with sound is linked against the alsa libs</em><br><br>\nWell, what can I say? If you had linked it against Alsa, it's linked against Alsa. Otherwise you can still compile kdelibs and kdemultimedia --with-oss and/or --with-alsa. It's up to you! :)<br><br>\nCheers,<br>\nLukas Tinkl [lukas@kde.org]"
    author: "Lukas Tinkl"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "So maybe that's what's wrong, the goys @ suse (where I go my KDE 2.0.1 packages) just built against ALSA nut not OSS... but I just don't have the time to rebuild my KDE 2, sorry."
    author: "Mathias Homann"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-02
    body: "Actually, I'm running a kernel driver for my Sound Blaster 128 (the es1371 driver) and it works great under kde2. Although Alsa has support for OSS compatibility from what I understand. (it takes some extra entries in /etc/modules.conf) what kinda sound card are you working with?"
    author: "Mr.Kill-9"
  - subject: "Maybe this is what they fixed ?"
    date: 2001-02-01
    body: "Note that the anounce specificaly says they will be doing demoes.  Not just talking about this.  \n\nMore importantly since it's antitrust headaches, IBM tends not to anounce anything ontil it actualy works."
    author: "Forge"
  - subject: "Re: Maybe this is what they fixed ?"
    date: 2001-02-01
    body: "then they had that fixed within the last 8 days..."
    author: "Mathias"
  - subject: "Re: Maybe this is what they fixed ?"
    date: 2001-02-02
    body: "Posible.  Or maybe they just havn't released the working code yet :)"
    author: "Forge"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Hi!\n\nAt least some of your facts are..well...wrong.\n\nArts is the KDE sound daemon. If you look at <A HREF=\"http://www.kde.org/announcements/changelog2_0to2_1.html\">this page</A>, you'll find \"support for audio output via ALSA\" as a new feature for kde2.1beta2. So I guess, OSS was supported before. Or how else could someone have had sound before?"
    author: "ac"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "same to you... ever tried to use kmixer with the old OSS driver???\nno valid device found is what you get in KDE2..."
    author: "Mathias"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Yep, and it work!\nI don't use ALSA on my slackware but KDE2, Arts and sound playing work really fine.\n\n:-)"
    author: "Michele"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Since your statement is wrong (kde does accept OSS) I guess you never really tested this...."
    author: "rinse"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "yea, as if I was using linux only since yesterday...\nI had OSS drivers up and running since kernel 1.0, the ossfree from the kernel on one machine, the commercial oss on my laptop. Installed KDE2, no sound.\ninstalled alsa instead of oss drivers, sound. any further questions?"
    author: "Mathias"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "and for anyone who wants to flame me further,\nWhat I speak of is KDE 2.0.1., <em>not</em> kde 1.x and also <em>not</em> kde 2.1 beta something."
    author: "Mathias"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Well, the only guy flaming here is you. KDE 2.0.x works with OSS since the beginning. Yes, if you use SuSE-packages then you need to install ALSA but this has got reasons which are not related to KDE at all. And now go and grab your brown paperbag ... (PS: I didn't ever install ALSA and it's not installed on my system. Strange enough kaiman, noatun etc. work decently using aRts ;)"
    author: "Anon"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Well the fact it doesn't work for you doesn't automatically mean it's not possible... I'd suggest you to check your kernel and sound modules, there's no reason for KDE to barf on OSS! You'd have to believe me, but I *do* use KDE with OSS...<br>\n\nRegards,\nLukas Tinkl [lukas@kde.org]"
    author: "Lukas Tinkl"
  - subject: "Arts + Alsa = :-("
    date: 2001-02-01
    body: "Huh, I'm running the alsa drivers on my box and sound works great for everything EXCEPT arts (I've got a VIA integrated 686 chipset). arts makes my speakers sound like a truck is running over them - horrible.\n\nHow do you go about debugging arts anyway? I haven't been able to find any info on troubleshooting..."
    author: "JC"
  - subject: "Re: Arts + Alsa = :-("
    date: 2001-02-03
    body: "You can start artsd -l0 to get some debugging information. Other than that, I can tell you that KDE2.1 will support ALSA directly, so you will have the option to use aRts -> ALSA sound or aRts -> OSS sound."
    author: "Stefan Westerfeld"
  - subject: "Re: Arts + Alsa = :-("
    date: 2001-02-03
    body: "Hey, have you got the 2.4.x Linux kernel yet? Via686 soundcards (integrated into the motherboard) are supported. : )\n\nWhoa- I just realized something. Sound works under Linux. My Visor works under Linux. My cd-burner works under linux. My bleeding-edge-CVS KDE2 builds work under Linux. Wait - no, my printer doesn't work under Linux. Whew, thought I was dreaming or something... heh. Almost had to pinch myself...\n\nAnyway, yes - the sound support alone is worth the kernel upgrade."
    author: "Chris Lee"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Strange statement. I've been using OSS-drivers without problems on my Aureal 2 soundcard for a long time.\nIn kde2.1 beta, you can choose sound I/O-method Autodetect, OSS or ALSA in kcontrol. Artswrapper translates or something.... Are now running ALSA-drivers (new soundcard) and had to change output-method to get sound in UT and Quake3.  \n\nTry it out some more, wait to till the finished product is out before you screem up...."
    author: "Geir Kielland"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "OK OK OK\n\n\nShame on me!!!!\n\nI tried the OSS support with the kernel drivers for my old SB AWE64, which did not work in KDE2, but had ever worked before... Then I bought a SB Live, and after seeing no matching card found in the control center in kde2, installed alsa, which showed up in controlcenter...\nbut now I installed as a test the emu10k1 oss driver and kde 2.0.1 works fine with it, it just can't give me informations about it in controlcenter... feel free to flame me..."
    author: "Mathias Homann"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-02
    body: "You flaming idiot!"
    author: "ac"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-02
    body: "welcome, long lost brother..."
    author: "Mathias"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Since IBM ViaVoice is not Free Software (in the GNU sense), it's of no use!!! Again it's like building a car with it's bonet locked up!! Pity that people have not learned from the KDE mistake!!"
    author: "RK"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "umm... I doubt kde will ever *need* to have ViaVoice to work with a kbd and mouse. But the way I read this, there will be a nice API for such things to exist in KDE, and ViaVoice will be able to use it (if you wish to purchase a copy) and maybe someday someone will write an OSS one and those who want voice recognition won't have to buy ViaVoice.\n\nBut in the meantime, why not let them have the choice of buying functionality that isn't currently available in OSS form? I probably won't purchase it, but I can see how some people would, and it's no loss to the OSS core."
    author: "puetzk"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "If you don't like it don't use it. The problem is that voice recognition needs to be implemented on the toolkit-level to provide people who can't enter text via a keyboard with a reasonable input-method. The API being used for this needs to be in place as soon as possible (the later it will come the more difficult it would be). If you are not satisfied with Viavoice's license feel free to create an LGPLed speech-recognition. But be aware that this won't really be a simple project ..."
    author: "Anon"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "There is a BSD licensed speech recognition project.\nIt is called <a href=\"http://sourceforge.net/projects/cmusphinx/\">CMU Sphinx</a>, and is hosted at <a href=\"http://sourceforge.net/\">SourgeForge</a>."
    author: "Simon Hill"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Great!!\n\nWhat the hell do KDE or gnome need to bother about ViaVoice ...\n\nthis sphynx project should be used in Open Source Desktop...not a Devil Commercial Product...\n\nregards\ngb"
    author: "gberrido"
  - subject: "It don't have to be free to be good!"
    date: 2001-02-01
    body: "Since when does everything regarding Linux have to be free?\n\nI am sick of this juvenile attitude within the Linux community that everything has GOT to be free or it sucks.\n\nTo expound on your analogy, it's like you were given a car for free but:\n a) You want the gas/ petrol to be free, too.\n b) You may not have been given the tools to repair it for free BUT:\n    1) How many of us actually HAVE the time to repair our cars anyway?\n    2) Last time I BOUGHT a car, I wasn't given the tools to repair it.\n    3) Forget the fact that since the car is free, the mechanic's shop can charge me less to repair it since he didn't have to waste his hard earned money on the car to begin with.\n    4) Even if you WERE given the tools to repair your car (which you got for FREE) the population of this world (en masse) does not have the skills or more importantly the DESIRE to repair their own cars.)\n\nWhat about all the time, energy, and resources which went into designing and manufacturing the car? Did you think all the artisans who designed and built the car should not have been paid for their services? How do you expect them to survive?\n\nThe Linux community is sounding more and more communistic. \"Give me, give me, give me. You take care of me, I don't want to.\"\n\nGranted, on paper communism is ideal, but in practice and reality haven't we learned that it just doesn't work?\n\nFor goodness sake.\n\nBe thankful that we DO have speech recognition software. Closed or open, this is a proud day."
    author: "Cyber Czar"
  - subject: "Re: It don't have to be free to be good!"
    date: 2001-02-02
    body: "Bravo bravo I'm getting sick of the idealists as well!! Open source is very cool but software companies need to make money and since ibm is really doing cool stuff with linux get off there back!!!! Some people whine about everything. It always seems to be the gnome people whining whining grow up losers.\n\nCraig"
    author: "craig"
  - subject: "Re: It don't have to be free to be good!"
    date: 2001-02-02
    body: "I agree with your subject line. But maybe you ought to study a subject a bit before you attack somebody over it. Are you aware of such things as lisencing issues? Do you know that e.g. Debian wouldn't package KDE before QT became GPLd because it thought there was a lisencing conflict? Now I don't know anything about how the thing would integrate with QT but I think that it is possible that some kind of conflict with GPL would be born and I think this is the thing that people were commenting on. \n\nNote, that in the message RK talked about free in the GNU way. This means not the price at all, as you assumed but the freedom to get and re-distribute the source code. Their worry is a good one and ought to be consider well and good, and not juvenile attitude of wanting everything for free (gratis).\n\nIn essence this makes you a big mouthed idiot who doesn't know what he is talking about."
    author: "anonymous"
  - subject: "Re: It don't have to be free to be good!"
    date: 2001-02-02
    body: "Qt is available in several licences.\n\nCraig"
    author: "craig"
  - subject: "Re: It don't have to be free to be good!"
    date: 2001-02-02
    body: "I think that what is bad in integrating commercial soft, is that a commercial company have the exact opposite philosophy (making money),\nand a non profit project like KDE (programmers writting nice pieces of code FOR FUN).\nThe first one is in logic of Buziness,\nthe 2nd in a logic of offer.\n\nI mean this is the same difference between\ncommercial music, produced just to make money,\nand real artist which just care about what he plays, without asking himself if his song will make a hit or not...\n\nthe concern is not having stuff for free, it s the freedom of the project.\n\nregards\ngb"
    author: "gberrido"
  - subject: "Was: It don't have to be free to be good!"
    date: 2004-12-23
    body: "The word FREE was not MEANT to relate to money rather freedom to know what you run on your own CPU.  So i do agree with the author who says that the Linux community has gone bonkers over \"Free\" software thinking it should be given to them without renumerations.\n\nFor those who think they should get a FREE ride and not FREEDOM to view/modify their local cpu electrons please READ the GPL to understand the original FREE philosophy.\n\n;) This will cost you guys 2 cents but you are FREE to interpret it anywhich way you like."
    author: "Ahmed"
  - subject: "Cool Game/Simulator/Trainer idea"
    date: 2001-02-01
    body: "If this all happens (direct integration of KDE/ViaVoice/TTS), then this could make some really cool opportunities for games, simulators, and even training devices!\n\nImagine a \"game\", under KDE, that fully simulates an Air-Traffic Controller's job.  Said game would have a \"controller\" speaking his commands to the various aircraft in his airspace and those aircraft would respond (via speech) back to the controller.  A true to life trainer/simulator.\n\nBest part about such a game, we could honestly tell our Windows based friends that \"Sorry, but this technology is not supported under Windows!\"  I would just love to be able to tell some people that!"
    author: "Ron Gage"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Speech recognition is probably great :), but can we stop for a while and fix a simple keyboard layout recognition. I have spent last two weeks trying to fix croatian keyboard layout and when I did it I lost my danish keyboard layout. Switching keyboard layout is something essential for my work, and do not misunderstand me - it worked well in KDE 1.1.2.\nWhat I want to say is: I am not against \"big\" steps, but do not forget that little-big things make software great. \nBTW, Krayon should have more PR :). Progress that Krayons developers made is simply amazing."
    author: "Zeljko Vukman"
  - subject: "What about dictation?"
    date: 2001-02-01
    body: "This release says you can do command control, and text to speech, but what about the speech to text?  I know all of us can read and write faster than talking or hearing, but I'm thinking about for handicapped people, this could be a real godsend for some types of applicaitons."
    author: "Shawn Gordon"
  - subject: "Re: What about dictation?"
    date: 2001-02-01
    body: "It specifaclly mentions dictation"
    author: "carbon"
  - subject: "Re: What about dictation?"
    date: 2001-02-02
    body: "<I>I'm thinking about for handicapped people, this could be a real godsend for some types of applicaitons.</I>\n<P>\nSpeaking as one of said \"handicapped\" population, this would be more than a godsend.  I have ViaVoice for Linux right now and it's helped tremendiously with any document/WP I've needed to do but the vast majority of my work is either email or coding.  From years of experiance, voice recognition and coding just don't mix.  But email would save me hours of time and physical pain.\n<P>\nAs for licensing, free-as-in-speach is always best.  But anything that will work is what is needed.  Especially in situations like this.\n<BR>\n<BR>\n<TT>\n---<BR>\nIf I actually <B>could</B> spell I'd have spelled it right in the first place.</TT>"
    author: "X-Nc"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "Um, I have used Via Voice, and contrary to what is stated here, even after an hour of voice training i could get no better then 50 % accuracy on anything but numbers. Also, is this aRts integrated? If it is, then we could create a universal speech recognition library, and use whatever backend you want (viavoice or something else). I.E. the backend would simply analyze the text of the message, and decide what command to run, and leave the actual recognition to the engine that it is connected to."
    author: "carbon"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "I used it on Win98 with Lotus Wordpro and the accuracy was very high after just a few minutes of training, and this is with the budget version they bundled in the Millenium suite a couple years ago.  I have to think the technology has evolved even from that point."
    author: "Shawn Gordon"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-02
    body: "Via Voice is definatley a YMMV product.  For me I attained a very high hit rate within well less than an hour.  The more I used it the more accurate it got.  Even when I got a cold it was still pretty darn accurate.\n\nOthers I know had problems similar to what you describe.  Very similar to the problems some small percentage of the population still cant master pilot script."
    author: "mperino"
  - subject: "Cost/Availability?"
    date: 2001-02-01
    body: "There is nothing here about how to get it and use it or who to contact.  Does anyone else have more specific details?"
    author: "Shawn Gordon"
  - subject: "Re: Cost/Availability?"
    date: 2001-02-01
    body: "I looked at the Trollech site, and there is nothing.  I looked at the IBM site and the most current information for ViaVoice SDK for Linux is from February.\n\nAs someone who lives by press releases, we typically make it a point to update our web site before we send out announcements that way people can find out details immediatley."
    author: "Shawn Gordon"
  - subject: "Re: Cost/Availability?"
    date: 2001-02-01
    body: "> As someone who lives by press releases, we\n> typically make it a point to update our web site\n> before we send out announcements that way people\n> can find out details immediatley.\n\nYeah, which is one of the reasons everyone around here loves TheKompany so much (that and the kick-ass software). I assume your comment was tongue-in-cheek to some extent, since I think everyone here knows IBM doesn't exactly live by that policy. TrollTech on the other hand suprises me a bit... I couldn't find anything either tho, does anyone have a url??\n\nErik"
    author: "Erik Severinghaus"
  - subject: "Re: Cost/Availability?"
    date: 2001-02-03
    body: "The press release clearly says it's a demo. It's not vapoware (since it actually runs), but as of today, it's not a product but a demo. Not more, not less. I hope the feedback at LinuxWorld Expo will be encouraging enough to turn it into a product, though. \n\nAll I can say at this point is that you probably should talk to IBM."
    author: "Matthias Ettrich"
  - subject: "Re: Cost/Availability?"
    date: 2001-02-03
    body: "But Matthias, what is the point of it if no one else can do anything with it?  Saying \"talk to IBM\" is like saying \"talk to Pakistan\" - where do you start?  I'm keenly interested in this technology and making use of it.  I don't really understand the point of making a big announcement about something that may not be available for anyone to use.  If it's there, and it works, then it sounds like it's a product.  Saying it's a demo does not imply that it is unavailable, it means you are demonstrating the technology in practical terms.  I want to support you guys and this effort, I'm just trying to find out how to do that."
    author: "Shawn Gordon"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-01
    body: "I would be particularly impressed if this also worked with CMU's open-source Sphinx speech engine? Hopefully the API is reasonably flexible and you could plug a Sphinx back-end into it.\n\n- Daniel"
    author: "Daniel Franklin"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-05
    body: "NOT HAPPY :(<BR><BR>Is it *totaly* impossible to integrate ViaVoice without including any of the commercail code? I mean, if KDE and ViaVoice is integrated, what about us who refuse to buy software for homeuse? Ehh? If I don't want/need ViaVoice, then I don't need the code, that integrates it, thus I would like to compile without it, right? How am I gonna do that if the code is closed? There MUST me somthing to do here! I mean from my point of view, this is the first step towards a (non)-solution that gives IBM some sort of controle over KDE.. not? Will I ultimately have to buy ViaVoice (read: get an elegal copy)? I have ALWAYS been a Die Hard(TM) KDE fan, but if this keeps up, and there isn't found a solution to the problem so the code gets re-opened, I will be forced to switch to GNOME (please not God, pleeeeease not), since I am way to paranoid to use closed code. For all I care IBM can be as much a crook corp. as Micro$oft. How do I know that I can still trust the KDE-team if I can't read to code? Who knows, maby we will find NSA backdores in KDE someday! DON'T LET THIS HAPPEN!!!!<BR><BR>/me is begging on his knees: \"Please reopen the code and kick IBM until they figgure out a way to implement ViaVoice in a SAFE and OPEN way\"<BR><BR>/kidcat a Die Hard KDE fan."
    author: "kidcat"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-05
    body: "I totally agree, and what about the other platforms if we could not have access to the sources ? Currently I'm running Linux on a PPC Amiga.\n<BR>\nI'm very unhappy too seeing KDE turning into a non free-software open-sourced solution!!!"
    author: "Alx"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-05
    body: "See? I'm not the only one! HELP ME GUYS! KEEP POSTING! THIS IS NOT ACCEPTABLE! KDE IS TOO GOOD TO BECOME ANOTHER EXPLORER!!!<BR><BR>/kidcat, a worried Die Hard KDE fan!"
    author: "kidcat"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-13
    body: "Guys, you go a bit too fast. They only chose\nKDE to make a demo. KDE source code is GPL and\nis going to stay that way. It was never the\nquestion to link KDE to ViaVoice. No cigar, then.\nWhy such a panic ?"
    author: "titi"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-13
    body: "PHEEEEEEW!<br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: Trolltech, IBM and KDE to Demo Voice-Control"
    date: 2001-02-13
    body: "And remember guys... HTML is not html here :) <BR><BR>/kidcat"
    author: "kidcat"
  - subject: "Getting KDE back to OpenSource!!!"
    date: 2001-02-05
    body: "Hi again... 'cus I'm not giving up on this!<BR><BR>Daniel mentioned CMU's Sphinx. So maby it's not so good as ViaVoice... yet! If the KDE community in general are as pissed of about the closeing of the code, as those I have talked to, this might be the answer!<BR><BR> http://www.speech.cs.cmu.edu/sphinx/<BR><BR> Those who can, help. Those can't, keep screaming loudly!<BR><BR>/kidcat (the little annoing one who refuse to see KDE go NonOpenSource)<BR>-<BR>Render onto \"Ceasar what is Cearsar's; Render onto God what is God's\".... Also translates as: Render onto Micro$oft what is Micro$soft; Render onto KDE what is KDE's\"... get my point?<BR>-"
    author: "kidcat"
---
<A HREF="http://www.trolltech.com/">Trolltech</A>, <A HREF="http://www.ibm.com/">IBM</A> (NYSE:IBM - news), and <A HREF="http://www.kde.org/">KDE</A> have teamed up at LinuxWorld Expo in New York and are demonstrating IBM's ViaVoice speech-recognition technology running on Qt and KDE.  With ViaVoice integrated into Qt/KDE, it will be possible to control Qt/KDE desktop applications with speech input -- from launching applications to menu selections to text entry.  Developers can easily integrate this technology into existing applications; in fact, in many cases no changes have to be made.  The Trolltech press release follows.


<!--break-->
<P>&nbsp;</P>
<P>
Santa Clara, California -- Trolltech, IBM (NYSE:IBM - news), and KDE are teaming up at LinuxWorld to demonstrate IBM's ViaVoice speech-recognition technology running on Trolltech's Qt, a cross-platform C++ GUI framework in the K Desktop Environment.
<P>
The technology preview will be running during the entire show at Trolltech's Booth, No. 1557 at LinuxWorld, which will be held at the Jacob Javitz Convention
Center January 31 through February 2, 2001.
<P>
"This combination of technologies will greatly accelerate the creation and adoption of speech-enabled applications for the Linux desktop," says Patricia
McHugh, Director, New Business Development, IBM Voice Systems.
<P>
Matthias Ettrich, a senior software engineer at Trolltech and the founder of KDE, elaborates: "When ViaVoice is integrated with Qt, it will be possible to control
Qt-based Linux desktop applications with speech input that is as simple as -- if not more simple than -- keyboard input. Developers can build speech-capability
into the structure of their application from the beginning."
<P>
In other words, the two technologies running together eliminate several of the obstacles that have hampered widespread adoption of speech-recognition on the
desktop, including: inefficient resource-use; sub-optimum performance; and the difficulty of "bolting on" this functionality after a typical application has already
been written.
<P>
ViaVoice has already shown that it can handle the two typical speech-recognition tasks: command and control; and dictation. In addition, however, ViaVoice on
Qt supports: TTS (text to speech), in which the system can read any kind of text input and translate it into speech; and a function that allows programmers to
define a "grammar" in BNF format. The engine will then recognize phrases that match the grammar, e.g., special input modes for dates or numbers such as
"Monday, the first of June" or "two thousand one hundred and seventy five."
<P>
About Trolltech
<BR>
Trolltech develops, supports, and markets Qt, a C++ cross-platform toolkit and windowing system. Qt and Qt/Embedded let programmers rapidly build
state-of-the-art GUI applications for desktop and embedded environments using a "write once, compile anywhere" strategy. Qt has been used to develop
hundreds of successful commercial applications worldwide, and is the basis of the K Desktop Environment (KDE). Trolltech is headquartered in Oslo, Norway,
with offices in Santa Clara, California, and Brisbane, Australia. www.trolltech.com
<pre>
CONTACT:  Trolltech
          Aron Kozak, 408/219-6303
          aron@trolltech.com
 or
          Al Shugart International
          Jessica Damsen, 831/464-4746
          jdamsen@alshugart.com
</pre>

