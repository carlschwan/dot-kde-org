---
title: "Kernel Cousin KDE #18 is Out"
date:    2001-07-27
authors:
  - "numanee"
slug:    kernel-cousin-kde-18-out
comments:
  - subject: "Benefits of Qt3?"
    date: 2001-07-27
    body: "What are the benefits of moving to Qt3?"
    author: "Matt Perry"
  - subject: "Re: Benefits of Qt3?"
    date: 2001-07-27
    body: "- Support for Arabic and Hewbrew\n- RichText classes\n- Database support\n- Component model\n- No more cut/paste problems (but only between Qt3 apps)\n\nOne of the most complained about aspects of X is the darn clipboard, so getting KDE based on Qt3 will solve a lot of headaches.  But this is from a user perspective.\n\nFrom a developer perspective, KDE-DB is going to utilize Qt3's database support, and this can't happen until they make the switch.  KWord currently uses a backported richtext for use with Qt2.  So you can see that there is a drive/need in KDE to use the new Qt3 features."
    author: "Justin"
  - subject: "Re: Benefits of Qt3?"
    date: 2001-07-27
    body: "What is the purpose of database support in a *widget toolkit*? Isn't this just like placing TCP/IP support in /etc/passwd or another similarly unrelated place?"
    author: "Niftie"
  - subject: "Re: Benefits of Qt3?"
    date: 2001-07-27
    body: "there is often a need to access data from a database and display it in a GUI, or vice versa. in those cases having a db API that abstracts the details of the actual data access away (connecting, sending queries, retrieving results, details specific to a given db implementaiton, etc) that works nicely with your widgets (even so far as to make the widgets aware of the database) is very very nice.\n\nmaking such things simple and convenient opens the door to making more applications database aware (e.g. financial packages, email apps, contact information systems)"
    author: "Aaron J. Seigo"
  - subject: "Re: Benefits of Qt3?"
    date: 2001-07-27
    body: "Qt is more than a GUI/widget toolkit; it is a cross-platform toolkit.  What about QFile and QProcess?  These are not GUI related, but they sure make life easier!\n\nTrolltech put database support into Qt because there was a demand for it from their customers."
    author: "Justin"
  - subject: "Re: Benefits of Qt3?"
    date: 2001-07-27
    body: "Qt has been more than a GUI toolkit right from the start. KDE has been using it as the basis for a full application framework since day one\nand over time Troll Tech are filling the gaps in Qt to meet this application framework role. This is why so often there is an overlap between kdelibs and Qt functionality even in non-GUI-related areas (KConfig, KDB, i18n, KActions etc)"
    author: "taj"
  - subject: "Open Nap / Gnutella client"
    date: 2001-07-27
    body: "Why not intergrate a Open Nap / Gnutella client in the sidebar of konqueror"
    author: "underground"
  - subject: "Re: Open Nap / Gnutella client"
    date: 2001-08-01
    body: "Actually, I'd like to see a Freenet IOSlave...\n\nA nice GUI client would make Freenet more usable."
    author: "Rk"
---
<a href="mailto:aseigo@mountlinux.com">Aaron J. Seigo</a> is back with much appreciated KDE development news.  This week read about the app currently known as <a href="http://www.koffice.org/kontour/">Kontour</a>, <a href="http://www.kde.gr.jp/~toshitaka/Kde/KOffice/KWord/xim.png">XIM support</a> for KOffice, and more. <i>"The self-imposed lull in new features and the looming inevitability of starting work on the next version of KDE has given many developers some time to consider the future of KDE. As a result several large threads regarding which direction to take things in the near future have occurred on several of the KDE development lists, some of which are covered in this issue."</i> <a href="http://kt.zork.net/kde/kde20010720_18.html">Go get it here</a>.
<!--break-->
