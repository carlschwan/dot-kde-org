---
title: "OSNews: Interview with Harri Porten"
date:    2001-11-06
authors:
  - "juser"
slug:    osnews-interview-harri-porten
comments:
  - subject: "Cross platform component model"
    date: 2001-11-05
    body: "He forget to ask Harry Potter when we will be able to write cross-platform OLE servers/clients. KParts are great but they are not cross-platform."
    author: "Me"
  - subject: "Re: Cross platform component model"
    date: 2001-11-06
    body: "<Yell from the back of the room>\nTRY KoreLibs\n</Tell from the back of the room>\n\nCross plaform, python bindings, very light, works on OS X, WinXP and Linux...\nWhat else to you want out of it?  To dry your socks?\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "HancomOffice components"
    date: 2001-11-06
    body: "What I'M curious about is if theKompany & Hancom will use Korelib to do OLE in HancomOffice. As far as I could tell (in the demos), there isn't any way to say, embed a HancomSheet into a HancomWord document. I dunno if this was just disabled, or isn't planned at all for this initial version. Shawn, could you elaborate?"
    author: "Eron Lloyd"
  - subject: "Re: HancomOffice components"
    date: 2001-11-06
    body: "I would assume that if they where going to do embedding and extention they would use korelibs.  QTCOM is not an option and korelibs has over a year of development head start.  I have used korelibs for two projects now and have found it a little more complex to setup than KParts and KPlugins but that has to mostly with the fact that it is designed for portabilty in mind.  \n\nWhat would be nice to see now is if The Kompany or some kde developer would write a KoreLibs to KParts wrapper.  There is all ready a KParts to Korelibs wrapper that allows Aethera to load KParts, so I think going the other way should be very straitforward.  I really do not see anything more than a wrapper layer though, because it would break too much for either side to port their applications from one to the other.\n\nI guess the big issue is the KDE commnity has always been very cold to the comercial secter (See IBM ViaVoice 1 year later), so I would not hold my breath."
    author: "Ian Reinhart Geiser"
  - subject: "Re: HancomOffice components"
    date: 2001-11-06
    body: "Looks like there are a few things here that I can answer, so I'll take the opportunity to do just that.\n\n1.  Korelib is not KParts, they are similar, but complimentary technologies, Aethera is an excellent example of how they work together.  Korelib does not handle UI embedding.\n\n2.  We have a cross platform OLE type technology built to work with Korelib called TINO (This Is Not OLE or TINO Is Not OLE), you can think of this as being similar to KParts but cross platform.\n\n3.  OLE type support in HancomOffice:  We needed to updated Korelib and create TINO so that this would be available, as we are finishing these technologies in parralel to the HO development, it hasn't been put into anything just yet, but Aethera and Rekall will be using it shortly for their own needs, and then we will add document embedding functions into the rest of HO.\n\nI think that covers it.  I've asked Catalin (the Korelib developer) to do a write up for the Dot when he gets some free time so people actually understand what it is and how to use it, but at this rate maybe Ian will do it first :)"
    author: "Shawn Gordon"
  - subject: "Re: HancomOffice"
    date: 2001-11-06
    body: "Shawn, I love your other products, but when will HancomOffice work?  I'm running a pretty stock SuSE 7.1 box (stock kernel, stock XFree, etc), and I couldn't get any of the trial HancomOffice apps to run.  I've got a friend who's going in with me to try the finished product (we figure if only one of us uses it, they'll keep it.  If both of us use it, we'll buy another copy), but quite frankly, not being able to get the trial version to run is not a good sign.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: HancomOffice"
    date: 2001-11-06
    body: "What release are you trying?  They just put up a version the other day that is a 'beta' of the 'standard' (everything that isn't from theKompany), and other than Presenter, everything worked fine for me on my Caldera 3.1 system.  I know when they were making RPM's, they were doing it for RedHat 7.1 pretty much, but since it's Qt based I got them to start making tarballs instead.  You should be able to use the Envision and WebBuilder stuff as well without trouble, at least I am on various distributions.  Go ahead and email me privately if you need support, or use the talkback forums on their web site.\n\nShawn"
    author: "Shawn Gordon"
  - subject: "Re: Cross platform component model"
    date: 2001-11-06
    body: "> What else to you want out of it?\n\nRuby bindings.\n\n(I've lost all interest in Python since I discovered Ruby, as do most people who discover Ruby, actually :-)"
    author: "Guillaume Laurent"
  - subject: "Re: Cross platform component model"
    date: 2001-11-06
    body: "I figured you would say this ;)\nReally it should not be that hard, I have heard rumors of SIP also supporting ruby, but I have not been able to get Google to tell me where...\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Cross platform component model"
    date: 2001-11-06
    body: "Could you elaborate why?\n\nI love Python and I looked at Ruby, but I failed to see what should make it so appealing to me to want to switch.\n\nI'm not trying to provoke you, I'd really like to know :-)"
    author: "Marko Samastur"
  - subject: "Re: Cross platform component model"
    date: 2001-11-06
    body: "I've got to agree with this.  The first time I looked at Python I loved it, and the more I use it the more I like it.  I just can't warm to Ruby no matter how much I look at it :).\n\nShawn"
    author: "Shawn Gordon"
  - subject: "Re: Cross platform component model"
    date: 2001-11-06
    body: "Don't look at it, use it :-).\n\nReally, next time you have a something to write in Python, get Ruby instead. I'm not saying that this will necessarily change your mind, of course, but at least this way there's a chance to.\n\nThings I liked in Ruby are the very consistent and powerful object model, the closures/iterators, the really good mix between Perlish features (like '@' and '$' to prefix some variable names, except done the *right* way) and Pythonish ones (easyness of defining a class, cleanness of the syntax)...\n\nIt's really the most exciting language I've used in years."
    author: "Guillaume Laurent"
  - subject: "Re: Cross platform component model"
    date: 2001-11-06
    body: "Well i did have our BlackAdder product updated to work with Ruby because of all the excitement, but I imagine why I can't warm to it is because I just can't tolerate Perl at all.  I admit it is amazingly powerful at some things, but it just isn't for me.  Keep in mind my background as a long time COBOL developer and you will understand :)."
    author: "Shawn Gordon"
  - subject: "Re: Cross platform component model"
    date: 2001-11-07
    body: "Ruby is light years above Perl, and the features it borrows from it are the right ones, like regular expressions (with the added flexibility of storing the matching result in an object instead of global $&/$0/$1 etc...) and the possibility to invert the if/unless with the clause (print foobar if condition).\n\nAbout the '$' and '@' prefixes : in Perl '$' is for scalar, '@' is for array, and that quickly becomes unreadable. In Ruby, '$' is for global variables (which you rarely use - the main use is actually to ease the transition from Perl) and '@' is for member variables, which is exactly the same thing as the 'mMember' (or 'm_Member', or '_member') naming convention which is so often used in C++ or Java.\n\nSo Ruby still lets you write throw-away scripts like in Perl (something at which Python sucks), except you write them faster than with Perl, and the code is more readable."
    author: "Guillaume Laurent"
  - subject: "Re: Cross platform component model"
    date: 2001-11-07
    body: "but what about non-throw away scripts?\n\nI tried using Ruby too, but it seemed to still be too Perl-ish. I prefer Python a lot more."
    author: "off"
  - subject: "Re: Cross platform component model"
    date: 2001-11-07
    body: "> but what about non-throw away scripts?\n\nI don't have real experience to prove that Ruby scales better than Python, but I'd be *really* surprised it wouldn't, and my currently limited experience shows that one writes working code much faster in Ruby.\n\nIn particular, the object model is just so much cleaner and consistent than Python's."
    author: "Guillaume Laurent"
  - subject: "Re: Cross platform component model"
    date: 2001-11-06
    body: "I seriously doubt he is the guy that can fly on a broomstick :)\n\nHis name it Porten !!!"
    author: "Coward"
  - subject: "Dark and snowy"
    date: 2001-11-05
    body: "Well, that's what I always imagined Norway as looking like...\n\nOSNews is an excellent site. I've enjoyed all the articles I've read there, especially the interviews which are most more technically sophisticated than the \"And you give this software away? For free?\" you usually get."
    author: "Otter"
  - subject: "Double Buffering & Konqueror"
    date: 2001-11-06
    body: "The mention of double buffering in the article got me to wondering whether Konqueror makes use of this at all, as it does appear to be a bit jerky and flickery sometimes when updating pages or file views.\n\nGoing backwards to a previous page is noticably different from the competition too.  IE updates the screen once, returning to the same point where you jumped off.  Konqueror takes you first to the top of the page and then in a second visible operation, jumps down to where you were previously.  Sometimes this is so slow that I forget and try to manually intervene before the second step kicks in.\n\nJust wondering."
    author: "John"
  - subject: "Re: Double Buffering & Konqueror"
    date: 2001-11-06
    body: "> Konqueror takes you first to the top of the page and then in a second\n> visible operation, jumps down to where you were previously. Sometimes this\n> is so slow that I forget and try to manually intervene before the second\n> step kicks in.\n\nI agree. This has annoyed me all the time."
    author: "Erik"
  - subject: "Re: Double Buffering & Konqueror"
    date: 2001-11-06
    body: "Especially with slow machines! That's really annoying! Chris"
    author: "Chris Naeger"
  - subject: "Do not forget."
    date: 2001-11-07
    body: "Personally I would like to thank Harri Porton for: KPPP. The nice dial-up tool used by thousands of people everyday. Harri Porten has made it the nice app that it is today."
    author: "Matt"
  - subject: "Re: Do not forget."
    date: 2001-11-07
    body: "Please change my spelling mistake of mr. Porten's name above and delete this message."
    author: "Matt"
---
Harri Porten is one of the most active KDE developers (working, amongst other things, on the Javascript part in Konqueror) and is also a <a href="http://www.trolltech.com/">Trolltech</a> employee. In <a href="http://www.osnews.com/story.php?news_id=259">this interview</a>, Harri talks to <A HREF="http://www.osnews.com/">OSNews</A> covering topics such as Qt 3, licensing, KDE, and more. <i>"Vector graphics support is already present in Qt 3.0. The QPicture class is able to read and write static SVG files. Plug those pictures into QIconView and be done."</i>
<!--break-->
