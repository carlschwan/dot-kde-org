---
title: "Focus on Infusion"
date:    2001-06-16
authors:
  - "numanee"
slug:    focus-infusion
comments:
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "It's very nice.\nIs there a plan to integrate with kpalm or other palm support/interface?\nI'd like to make my  hotSync in Linux (at home) just as I do it on Outlook (at work)."
    author: "cristian"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "I could not understand why you don't merge with any of the moltitude of similar projects trying to do exactely the same. It seems that you know them, you even talk to them....\n\nDon't re-develop the wheel, please!\n\nAndrea"
    author: "Andrea"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "It can be read above that he started it as a `first try at writing a GUI-based application'. I think it's quite natural to start out on your own when you want to get experience in something and it's probably a more fun way of learning. There's no real obligation to help out the (free software/kde) community just because you can. People should enjoy what they do.\n\nIf the author is getting more involved and is having fun he might eventually even start contributing in a way you'd consider to be more useful as well.\n\n-- \nMatthijs"
    author: "Matthijs Sypkens Smit"
  - subject: "Amen!"
    date: 2001-06-16
    body: "100% agreed!\n\nI realize that everybody has a 'different way to scratch their itch', but it's going to take a lot of cooperation to produce competition for Outlook and Exchange.\n\nKDE is as successful as it is because thousands of ideas have been pulled together towards a common goal.  If each of the core developers forged ahead with their own DE, neither DE would likely EVER be as successful as KDE.\n\nConsider the current 'major players' w.r.t KDE:\nInfusion - Promising backend\nMagellen - Elegent interface\nAethera - KDE integration\nKMail - simple, functional, robust\n\nNow imagine what could be produced by combining the best parts of each offering!\n\nMaybe at that point we could start using the word 'competition' with some confidence..."
    author: "KDE User"
  - subject: "Re: Amen! - Not quite ..."
    date: 2001-06-16
    body: "Unfortunately you mixed up a lot of things:\n\nInfusion : It's not a backend, it is a frontend (GUI) to Citadel/UX, which is a quite powerful, if slightly strage (due to its BBS roots) office communication backend.\n\nMagellan : This is a try to combine all necessary means of office communication in one application instead of several specialized ones; idea: something in between MS Outlook and Lotus Notes Client.\nIt's developed by some developers lead by Teodor Mihai.\n\nAethera : This is a fork of Magellan by theKompany; they 'stole' the code and made it GPL (instead of LGPL) so that other companies cannot use it in their non-GPL apps anymore. The Magellan developers are pretty pissed off because of that, so cooperation is unlikely.\ntheKompany intends to make money with a proprietary server component, which would be a competitor to the free Citadel/UX, so they are unlikely to support that...\n\nKMail : Classic MUA tool focusing on the mail function. It tries (or should try) to use components ond KDE services wherever possible without the intend of creating an all-in-on application."
    author: "Corrector"
  - subject: "Re: Amen! - Not quite ..."
    date: 2001-06-16
    body: "Stold? Its freakin open source thats what your supposed to do. What a bunch of crap. Aethera has improved on what was a good idea but not quite there. To say they stold it is just FUD.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Amen! - Not quite ..."
    date: 2001-06-16
    body: "Stole?  Stole would be if we took it, closed it, rebranded it and pretended that we did it all ourselves.  Isn't one of the major points of open source that people use other peoples code for things?  Since they are both free and open source projects where does 'stole' possibly enter into the picture?  We made it GPL because all the trolls were running around insisting that we make all of our software GPL, since when has making something GPL been evil?  Geez, I don't get what you are trying to say at all."
    author: "Shawn Gordon"
  - subject: "Re: Amen! - Not quite ..."
    date: 2001-06-16
    body: "Don't worry, Shawn - most of us \"get it\" when it comes to Open Source.  I've taken dead projects myself and advanced them, and I'm aware of several projects that were taken over by a more enthusiastic person or someone with a different vision.\n\n    And to anybody who isn't aware, XEmacs, one of the \"great\" open source applications, was \"stolen\" by JWZ from RMS's original emacs.  RMS was upset for awhile, then (at least in public) realized that JWZ really produced some kickass code.  (This is the same JWZ who wrote big chunks of Netscape, was fundimental in opening up the Netscape source, and also wrote xscreensaver).\n\n    So, Shawn, you're in good company.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Amen! - Not quite ..."
    date: 2001-06-16
    body: "Nobody insisted that you change software with a\nBSD license to GPL. That's a plain lie."
    author: "me"
  - subject: "Re: Amen! - Not quite ..."
    date: 2001-06-16
    body: "And you would know that because you read all of my mail?  Perhaps you were somehow CC'd on the email I got from RMS as well?  Oh wait, that wouldn't be possible since you don't seem to have an email address according to your posting."
    author: "Shawn Gordon"
  - subject: "Re: Amen! - Not quite ..."
    date: 2001-06-16
    body: "Oh, now you do everything RMS tells you? So in\nthe future, you won't be selling proprietary\nsoftware? That's great news :-)\n\nBTW, I think your claim that lots of people have \nasked you not to release software under a BSD \nlicense is your free invention."
    author: "Bernd Gehrmann"
  - subject: "Re: Amen! - Not quite ..."
    date: 2001-06-19
    body: "... when one thinks it can be used by people like you.\nReally, as it is said in Italy: \"Gratitude is not of this world\"..."
    author: "How sad should be making free software..."
  - subject: "Re: Amen! - Not quite ..."
    date: 2001-06-19
    body: "Now, if only I could understand what you are\ntrying to say..."
    author: "Bernd Gehrmann"
  - subject: "Re: Amen! - Not quite ..."
    date: 2001-06-16
    body: "He wasn't attempting to cover the capabilites of each app in that listing, just the most important features of it. I.e, he was saying that a Magellan/Aethera/KMail/Infusion merge would have the best of all worlds, with a functional but elegant and KDE integrated interface, the simplicity and robustness of a simple email client if that's all that's needed, and a very powerful server-side backend for more complex PIM tasks.\n\nBesides, there's no reason that integration means only one huge, honkin binary loaded at once, StarOffice style. Just look at konqi, it's capable of accessing web sites, file drives, ftp, audio ripping, (minimal) email sending and recieving, koffice previewing, etc etc etc. It does this using a loaded-as-needed plugin style, ala KIO slaves. What's to keep us from using (long term view, that is) KPIM slaves/plugins or something like that?"
    author: "Carbon"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "It would be so freakin awesome it the kmail and infusion teams joined up with the Aethera team. Aethera's the application that shows the most promise at this point. They've spent quite a lot of energy cleaning up the Magellan code and have some very awesome things planned for it. We all can see what a good job Xamian has done with evolution but wait and see how Aethera turns out. Evolution will look boring. They basicly just Redid Outlook for gnome. Shawns guys are doing something better than that. Infusion is a little amateur project not even in the same league as Aethera.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "<i>Evolution will look boring. They basicly just Redid Outlook for gnome. Shawns guys are doing something better than that</i>\n\nAre you able to substantiate any of this, or is it just random spewage?\n\nFrom having used both, I think Evolution is looking like the better application.\n\nAlso, it's not very nice to diss someone's project with no proof. From the looks Infusion does almost as much as Aethera, and certainly looks better, so I would put Aethera and Infusion in exactly the same league.\n\nHype is great and all, but in the end of the day, you need to look at what it actually can do, rather than what the (marketeers|coders) hype it up to do."
    author: "no-control"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "<i>Evolution will look boring. They basicly just Redid Outlook for gnome. Shawns guys are doing something better than that</i>\n\n<i>Are you able to substantiate any of this, or is it just random spewage?<i>\n\nSubstantiate? Why? Just use evolution its just a redone outlook. Its so obvious theres no need to substantiate anything. \n\n\n\n<i>From having used both, I think Evolution is looking like the better application.<i>\nYour right at this piont evolution is better but it won't be.\n\n<i>From the looks Infusion does almost as much as Aethera, and certainly looks better,<i>\n\nWell i guess one mans trash is another mans treasure. Not to say infusion looks like trash but aethera looks professional and infusion looks like a hobbist program."
    author: "Craig Black"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "> Substantiate? Why? Just use evolution its just \n> a redone outlook. Its so obvious theres no need \n> to substantiate anything\n\nI wasn't actually talking about that claim that Evolution is a redone Outlook. I would agree with that claim.\n\nThere is, however, the need to substantiate the claims \"Shawn's guys are doing something better than that\" and \"...but it won't be\".\n\nIn what ways is Aethera going to be different from Outlook and Evolution and Infusion? At the moment, it's looking like a fairly poor clone of them. Please back up your claims."
    author: "no-control"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "Aethera looks just like an Outlook clone to me. Can you explain what will be so different about it? I'm currently using Evolution because Aethera doesn't work at all for me. I will say that Evolution does seem like pretty much a clone of Outlook, but at least it works."
    author: "anon"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "Well your right evolution works better at this point than aethera but the development is moving fast. I've got a good idea of where aetheras going and it sounds pretty exciting, but i'll let Shawn release those as they come. I don't think he would appreciate it if i posted his plans here before he did.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "I can't say I know what you know about where Aethera is going, but how do you know Evolution isn't going in the same direction? Or maybe even a better direction?\n\nFor all you know, it could very well be going in the same direction."
    author: "anon"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "Damn Craig, you're really informed, aren't you?\n\nCome on, throw us a bone, we want to be as smart as you are!\n\nOh, and yes, please be so kind as to \"let Shawn release those as they come\"! Shawn, you have Craig's permission!\n\nThank You, Craig!"
    author: "me"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "Aethera looks nothing like Outlook.  Evolution and Infusion look almost exactly like Outlook.  If you're using Evolution your the first person I've talked to that has been able to.  Did you send in any bug reports about your inability to use Aethera?  We have thousands of people using it, but we are always interested in fixing whatever problems might be around."
    author: "Shawn Gordon"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "Hello, \n\nI think comments like-\n \n If you're using Evolution your the first person I've talked to that has been able to.\n\nare what make the Kompany look so petty in \ncomparison to other open source companies. \nIts an obviously false claim; plainly \nevolution currently has a lot more\nusers than Aethera. Maybe this will change.\n\nBTW I am a recent convert to KDE, as it just \nprovides a better user experience than Gnome. \nHowever, I'm still looking for a decent IMAP \nclient. Till then I'm sticking with Evolution.."
    author: "Robert"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "This is a 100% honest response.  I've never used Evolution, but in talking with people about Aethera I ask them if they've tried Evolution and what they think, and without exception, everyone I've talked to has said that they can't get it to work.  I don't doubt that people are using it, but I've never talked to one of them.\n\nWRT to IMAP, our new Aethera release next week will have full IMAP support including folders and a few other nifty improvements."
    author: "Shawn Gordon"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "Thank you, thank you, thank you... at last IMAP on KDE..."
    author: "marioy"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-18
    body: "Even better, that'll be in the 2.2 Kmail also."
    author: "Carbon"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "The biggest reason people have problems with evolution is because it uses a version of Gnome that isn't yet released (just as Evolution isn't yet released).  There are a large number of dependancies that must be filled for it to work properly.  It'd be like trying to run a program that depends heavily the new features in KDE 2.2 on KDE 2.1 or 2.0.  It just wouldn't work.  It's not fair to bash Evolution for that reason.\n\nPerhaps instead of bashing them, you could try to work with them to come up with a common spec for writing support for new protocols.  No one will benefit from having to rewrite support for every groupware server out there for each and every mail client anyone cares to write.  Take a note of the Abiword/KWord/etc developers and reduce the duplication of work of the things that will help everyone."
    author: "Chad Kitching"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "Evolution has always worked for me, albeit to varrying levels of success.\n\nCurrently i use 0.10 as my default mail/scheduling client.\n\nIt's a very good quality app, and it looks like steady progress is being made on it.\n\nHowever saying that, the major problem with Evolution is that it is very slow (on my system at least).  Compare that with Aethera, which is lightning fast on my system.\n\nI also very much prefer the look of Aethera (i believe it's going to be changing though?).\n\nI firmly believe that Aethera will be a very good application. and i fully intend to switch to it full time as soon as it reaches the level of functionality that Evolution is currently at.  But it isn't there yet.\n\nAlso, with Evolution we can see what is happening with regards to development, so it feels like we're a part of it even if we've nothing to contribute at that time.  There's a development list, as well as a users one.  With Aethera we do not know what is happening so we feel more detached.  The only time we know what's happened is when a new release is made and we can try it out.\n\nIt's a lot harder to be enthusiastic about something when we don't know what is happening.  If there's anyway of letting us know about the progress on Aethera i think it would be a good idea.  It helps foster the community feelings aspect. :-)"
    author: "Ack"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "The CVS is public and open to anyone who wants to look at the code, we are trying to be considerate and at least package the code for as many distros as possible when there is what we consider a stable release.  In what way is it not publicaly available and how would you suggest we let people know what is happening outside of what we are currently doing."
    author: "Shawn Gordon"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "It's not on the sourceforge CVS, and i couldn't find anything on thekompany.com site about it.  Could you let me know the details?\n\nSomething that i think would raise interest is perhaps a paragraph or two on your site every week.\n\nSomething that states what has been done that week \"so and so fixed this annoying bug, this feature was added\", that sort of thing.\n\nIt would allow an \"at a glance\" look at what progress has been made, even for those not interested in pulling it from CVS."
    author: "Ack"
  - subject: "Use Kernel Couse KDE ?"
    date: 2001-06-18
    body: "How about a small update item from theKompany, posted to the mailing list each week so that it could be rolled into the latest Kernel Cousin KDE publication?  \n\nIt would add value to Kernel Cousin KDE and us readers wouldn't have to go looking elsewhere for it.  \n\nDon't know about you, but that would work nicely for me.\n\nJohn"
    author: "John"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "> However, I'm still looking for a decent IMAP\n> client.\n\nKMail has IMAP support in CVS, see\nhttp://kt.zork.net/kde/kde20010331_4.html#9\n\nSo while it isn't there yet, 2.2 will bring IMAP to your KDE desktop."
    author: "Rob Kaper"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "> Aethera looks nothing like Outlook. \n\nWell, I'd disagree with that. It still follows the same concepts and ideas. Bar down the side with large buttons for the categories, then a tree with folders, and finally the actual data view.\n\nNot that it's a bad thing, Outlook has a vry good interface.\n\nAethera does however have that really ugly pointless bar across the top that has a \"pretty\" picture and does nothing but take up screen space, but maybe it's going to do something else eventually."
    author: "no-control"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "The interface paradigm is not unique to Outlook, nor was it invented with Outlook and overall it makes sense in an application that has categories and functions.  However the \"look\" of Aethera is nothing like Outlook, whereas the \"look\" of Evolution and Infusion are almost identical clones of Outlook.\n\nIf you don't like our \"pretty\" picture, you can dismiss it by closing the dockwidget that it is part of :)."
    author: "Shawn Gordon"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "So what you're really meaning when you say 'look' is the artwork?\n\nWhat I'm talking about when I say 'look' is how the interfce is laid out, and I think (as you agreed) it is fair to say that Aethera's interface looks like the Outlook one (albeit with different pictures) - even if Outlook didn't invent this paradigm.\n\nhttp://www.thekompany.com/projects/aethera/images/aethera-mail.png\n\nTake away the bar thing with the mail gfx at the top, give it a normal windows like theme, I would suspect that it looks rather like Outlook.\n\nPlus I'm still not sure I can see the similarity with Evolution's graphics and Outlook's. Evolution's do look so much nicer. I do know that Evolution's menu layout were copied directly from Outlook's, but is this a bad thing? I don't really think so.\n\nAnd I am glad you can remove the ugl^h^h^hpretty pictures, it just annoys me when I have to, because I always think I shouldn't have to. But at least you can remove them, which is always something :)\n\nNot that any of this matters, in the long run. I don't care if it looks like Outlook or if it doesn't, just that it does what I need it to with the minimum of fuss."
    author: "no-control"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "Paradigm?  Are you in marketing, by any chance, Shawn?\n\nAside from segregating the contact list, notes and mail capabilities into separate sections, I fail to see any real distinction between Aethera, Infusion, Evolution, or Outlook.  And to be quite honest, I get a whole lot more from something like Magellan or Outlook that doesn't enforce the segregation of the different elements because it means I can turn off the left-side toolbar, free up screen space and work just as efficiently as someone who heavily uses that toolbar.\n\nSo, Shawn, how exactly is your \"look\" different from Outlook, whereas Evolution and Infusion are clones of Outlook?  From my perspective, and apparently from the perspective of a lot of other readers, Aethera is just as much a clone as all the rest."
    author: "Chad Kitching"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "I've been programming for 22 years, the word paradigm comes up a lot and is descriptive.\n\nLook at the flat panel layout of Outlook and Evolution.  Does Aethera look like that?  No, it does not.  The UI has been further modified but is pending our rewrite of the current UI code, which is about 6 weeks away."
    author: "Shawn Gordon"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "The word paradigm has been abused for years to mean just about anything, most people don't even realise what it really means.  I've come to loathe the word as it is thrown around with all the rest of the garbled technobabble that populates so many press releases and \"journalism\" today.\n\nEither you mean flat panel as in the graphics used, or flat as in the panels are unmovable in Outlook.  Either that, or I still don't know what you mean.  I know it's probably not real flattering to be called a clone of another interface, but from my perspective, arguing against this seems to be more of a distinction without a difference.  I could just as easily say Aethera is a clone of Netscape Mail, Outlook Express, Groupwise, etc.  The point is, it's not a drastic departure from the standard e-mail client look, and there are very few clients that are (Eudora Light 2.0 for Windows 3.1 springs to mind, and perhaps the UNIX command-line \"mail\").\n\nDon't get me wrong, I don't mean to belittle you or your project, but it just seems that maintaining that you're not a clone of Outlook while everyone else is, seems a little silly, when functionally your UI isn't all that different from theirs."
    author: "Chad Kitching"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-18
    body: "Who gives a rats ass anyway.  There's only so many ways you can present mail and a folder tree.  \n\nThere's very little difference in the look of most modern cars these days too.  And consequently the features list, customer experience ( drivability, power, economy, etc ) are more important that the overall look.\n\nSo long as it works well, is intuitive, stable and fast, and meets the needs of its targeted customer base, then it's in!\n\nJohn"
    author: "John"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "Since you can't even spell Ximian, I doubt you really keep up with Evolution developments.  Evolution is not an Outlook clone.  Check it out, sometime.  The Camel backend is very impressive.  Evolution offers powerful filtering methods and vFolders, which simply rock.  It offers importing for several file formats with more to come, supports multiple accounts.  The calendar is, IMO, the best free software Calendar program I've seen.  And it's all tightly integrated in the Evolution shell which is simply gorgeous.  Boring, it is not.  Here's some information on the backend stuff:\n\nhttp://ximian.com/tech/calendar.php3\nhttp://ximian.com/tech/camel.php3"
    author: "Jamin Philip Gray"
  - subject: "Troll"
    date: 2001-06-16
    body: "<i>[ADMIN: Please <a href=\"http://dot.kde.org/992627943/992647448/992660167/992666945/992668120/992672894/992684980/992696044/992753357/992766139/992767706/992769072/992775010/\">do not reply further</a> to this thread]</i>\n<p>\nOh come on i know evolutions nice but to say its not an outlook clone is being naive or stubborn. Its funny how you gnome guys get away with trolling on the dot. \n<p>\nCraig"
    author: "Craig Black"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "Craig, you've not replied to my question. Or were you just pulling stuff out of your ass?"
    author: "no-control"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "The interface looks much like Outlook, but it's functionality/architecture is quite different.  I'm sorry you thought I was trolling.  I'm a gnome developer and was trying to point out the architectural differences between Outlook and Evolution.  I also keep up with KDE developments and have a special interest in coordination between the two groups."
    author: "Jamin Philip Gray"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "It's functionality is pretty much the same, it sends emails, receives them, does some stuff with calendars, tasks and addresses. :) Thankfully all the functionality I could ask for in a mailer :)\n\nCamel is probably more modular than anything in Outlook, but it's just a clone of Javamail, or JMail or something like that.\n\nAnd I'd not be surprised if Outlook isn't internally quite similar in ideas to Evolution, the only difference is, we don't know how Outlook is done :)"
    author: "no-control"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "> you gnome guys get away with trolling \n> on the dot. \n\nSorry, but the only guy who is trolling here is you Craig Black. Actually I feel really sick everytime I see your immature rants. I wonder if you are only too young to realize that your \"KDE-advocation\" is doing the KDE-project a real disservice or if you are trying to harm KDE on purpose.\nEither-way: grow up!\n\nTackat"
    author: "Tackat"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "You hit the nail on the head.. \n\nMaybe Craig needs the same?"
    author: "KDE User"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "<i>[ADMIN: Please <a href=\"http://dot.kde.org/992627943/992647448/992660167/992666945/992668120/992672894/992684980/992696044/992753357/992766139/992767706/992769072/992775010/\">do not reply further</a> to this thread]</i>\n<p>\nThe guy was a gnome developer pushing evolution on the dot. I think calling him out was necessary. The free software folks have done a very good job of determing what is politalically correct in the open source circles. Being pro kde on public forums like Slash Dot is \"uncool\" and \"trolling\". While being pro gnome is the correct position. Sorry if i don't follow the rules and bend a few. One thing i know is once you no longer argue ideas but attack on a personnel level you just committed a \"immature rant\". \n<p>\nCraig"
    author: "Craig Black"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "Yet neither you nor Shawn can say anything other than Aethera is not an Outlook clone, and Evolution is an Outlook clone.  The Evolution developers for the most part have admitted that the user interface on Evolution was modeled after Outlook, but maintain that the underlying framework is nothing like it.  The people advocating Aethera have not really given any real reason why Aethera should not be considered an Outlook clone.\n\nNow, if you were a developer of a program that received negative comments on some site, wouldn't you feel the need to comment?  If you look, he didn't attack Aethera, or any other KDE program, he merely felt the need to correct what he felt was an error, although perhaps he was a little too enthusiastic.  On the other hand, we have you and Shawn that are just repeating the same line over and over again about cloning Outlook.  From my perspective, you're the troll and Jamin is not.\n\nAnd you want to know what?  Admitting that Microsoft might've actually done things right in \"uncool\" and \"trolling\" on Slashdot, too, so I'm going to break the rules, too.  I think Microsoft did a wonderful job with the interface on Microsoft Outlook 2000.  With a little configuration, it's a highly capable and efficient mail/news/calendaring/contacts/journal program.  Many of it's features, such as Auto-preview, save me a great deal of time in helping me sort through the mail I need to read and respond to, and the ones that can be deleted and/or ignored without a response.\n\nPerhaps one of these messages, one of you will say why you feel that Aethera is not a clone of Outlook, just as Jamin did."
    author: "Chad Kitching"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-18
    body: ">> Perhaps one of these messages, one of you will say why you feel that Aethera is not a clone of Outlook, just as Jamin did.\n\n    First off, let me say that I am going to try and answer in a very non-trollish way... but AFAIK, no one in the thread has attempted to answer this.  Let me also say that I think that Outlook, Lotus Notes, Aethera, Evolution, etc are pretty much interchangeable programs - they are all built to do pretty much the same things, and as such, pull from each other heavily.\n\n    Now, having said that, let me try and explore the two things that I am aware that Aethera has that kick it a notch above Outlook.  First off, it has a clearly defined, open API for plugins.  I developed Outlook objects for about a year, and while it has some functionality in this area, it is limited.  Without having really used Aethera, the documentation makes it look like this functionality was designed from the start, rather than being added on, which really is a fundimental difference: but *only* if Aethera plugins take off.\n\n    The thing that seems to really seperate it from the other PIMs is a nebulous thing that I freely admit that I don't quite understand.  I've asked Shawn to explain it further, because it is *very* tantalizing, but not very well described.  The concept is similar to DiBona's LookOut project - all data types are parsed into a common, linkable and indexed format.  Thus, all your data is accessable in a structured way, whether it be a note you jotted down, an email, or an image.  Each can annontate each other (I gather), and they can be structured into various thought grouping (which is done automatically by access, I believe).\n\n    Now, if that works, that's enough to set it ahead for my use.  If that makes it more than a \"clone\", that's up to you.  Really, Outlook is a good program, and I don't see any harm in calling something a clone of a good program.  I don't normally think \"clone\" implies that that is all that program will *ever* be... just that it drew heavily in inspiration from one (in this case, good) source.\n\n    PLEASE keep all replies to the topic of Aethera features... the \"is it or isn't it a clone\" is a matter of opinion, and will never be resolved.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Focus on Infusion"
    date: 2001-06-18
    body: "\"First off, it has a clearly defined, open API for plugins. [...] look like this functionality was designed from the start [...]\"\n\nNow this is interesting.  Indeed this could be a very powerful feature and \"selling point\".  I'd really be interested to know if they've defined a mail provider interface (much like Outlook uses MAPI providers), and if it's simple and generic enough to be [re]used by other mail applications.  There are obviously PIM/groupware servers in the work (such as the Citadel/UX mentioned in the article), and if a fairly common API can be defined, programs could use it as a sort of plugin for non-integrated mail services.  I have a feeling more and more servers will eventually pop-up that don't use IMAP or POP3 as their native protocol, or have features that can't be implemented in those protocols, and it'd be nice if the server creators could make a single shared library that could plug into all these different mail clients and just work independantly of toolkit, desktop or anything else.\n\n\"The thing that seems to really seperate it from the other PIMs is a nebulous thing that I freely admit that I don't quite understand.\"\n\nLooks like it's what their website calls PDR...  I don't quite get it, either.  It'll probably be one of those things that you don't understand what's so great about it until you start using and relying on it.\n\nMy problem in all of this was we had two people complaining that their program was not a clone and everyone else's was.  I think this is a dangerous attitude to have, especially when you're developing a program.  It can seriously blind you to limitations in your program that your \"competitions\" program doesn't suffer from.  Whether any of these programs are a clone of Outlook isn't as important as how well it will do the job it's been designed for.\n\nIt will certainly be interesting to see how all the projects evolve over the next year or so.  I'm glad someone finally can tell me what differentiates it from the competition other than \"look\".  At least they're all trying to break new ground, Aethera with those plugins and \"PDR\", Evolution with it's VFolders, and now Infusion with it's support of Citadel/UX.  Although, now I'm interested in what's unique about Magellan, despite the fact that they seem to be either way behind schedule, or too busy to work on it (or maybe still bitter that theKompany forked magellan to make Aethera?)."
    author: "Chad Kitching"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "No, actually it seems to me that he is trying to correct some statements you made in a public forum and he does it in a quite reasonable voice. This is certainly not what trolling is about. As long as he does that in a reasonable manner without flooding the forum with his opinion you have no right to call him out.\n<p>\nI think before you try to do any further \"advocacy\" for KDE, theKompany etc. you might want to read \n<p>\n<a href=\"http://www.ibiblio.org/pub/Linux/docs/HOWTO/mini/other-formats/html_single/Advocacy.html#s6\">Advocacy.html</a>\n<p>\nwhich is not only true for Linux but to a certain degree also for KDE.\nYou might especially be interested in \"6. Canons of Conduct\" ... which says: \n\"Avoid hyperbole and unsubstantiated claims at all costs. It's unprofessional and will result in unproductive discussions.\" e.g.\n<p>\nTackat"
    author: "Tackat"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-17
    body: "<i>[ADMIN: Please <a href=\"http://dot.kde.org/992627943/992647448/992660167/992666945/992668120/992672894/992684980/992696044/992753357/992766139/992767706/992769072/992775010/\">do not reply further</a> to this thread]</i>\n<p>\nWell the dot right now is crawling with closet and open gnome supporters that are trying direct discussion and spread miss information. I'm getting sick of it. Even anon of linux today fame for trolling openly and often against kde feels comfortable doing gnome advocacy here on the dot. Its getting hard to tell who's here to add something or push discussion and options away from what's best for kde. Kdes pulling so far ahead of gnome among user there resorting to less than ethical tactics. \n<p>\nCraig"
    author: "Craig Black"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-17
    body: "you see non-existant phantoms (don't know if this is right English). Maybe there are gnome users criticising kde on this forum, but why not ? kde ain't perfect yet ... and posts like \"this is bad, i don't like the way thats' done because ...\" help making kde better. if they spread misinformation, simply correct it."
    author: "ik"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-17
    body: "I disagree. There are definitely more trolls here now than there were a few months ago (I am not saying that these troll are Gnome supporters, to make that clear). But if you really do not care for the other environment, then you should stay away from their discussionboards."
    author: "jj"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-17
    body: "> Well the dot right now is crawling with closet > and open gnome supporters that are trying\n> direct discussion and spread miss\n> information.\n\nAnd how exactly is this claimed trolling different from what you were doing on Gnotices a week or two ago?"
    author: "advocates are boring"
  - subject: "Troll"
    date: 2001-06-17
    body: "<i>[ADMIN: Please <a href=\"http://dot.kde.org/992627943/992647448/992660167/992666945/992668120/992672894/992684980/992696044/992753357/992766139/992767706/992769072/992775010/\">do not reply further</a> to this thread]</i>\n<p>\nWell advocates are boring. I'm assumeing that is your real name.  I was promoting a desktop web poll on gnotices to make it fair. I pushed it here and on gnotices. In the end with over 4000 votes which were verified as good my the sites owner kde had a 9 to 1 margin over gnome. He finally had to drop the poll because someone tried to run a script to push up the gnome votes. It would have be fair if i just mentioned the poll here now would it? I wanted to know with as much accuracy as i could what the desktop user base was. I'm not saying kde has that big of lead though. There was another poll on www.freeos.com with over 1600 votes where KDE only got 57% and gnome came in at 19%. Which is a bit different. Thats was a bit more of a geek crowd however because the command line came in at 13% with 221. \n<p>\nCraig"
    author: "Craig Black"
  - subject: "Troll"
    date: 2001-06-17
    body: "<i>[ADMIN: Please <a href=\"http://dot.kde.org/992627943/992647448/992660167/992666945/992668120/992672894/992684980/992696044/992753357/992766139/992767706/992769072/992775010/\">do not reply further</a> to this thread]</i>\n<p>\nThat's all you were doing, eh? I'll just provide a few choice quotes and let the public decide.\n<p>\ncraig: \"Well i sent it here and the dot and linux today. It's not rigged i think the kde users are more\nenthusiastic than gnome users or there are more kde users. Thats the fact.\" (<a href=\"http://news.gnome.org/990590897/990642174/990642812/990644299/990644912/990646935/index_html\">gnome-news1</a>)\n<p>\ncraig: \"That was then this is now. Kde has been moving at a much faster clip than gnome has.\n Since xiamian took over most of the development and kde stayed a hacker desktop i think that\n there has been many defecters to kde including me.\"\n(<a href=\"http://news.gnome.org/990590897/990642174/990642812/990644299/990644912/990646935/990650744/990652119/index_html\">gnome-news2</a>)\n<p>\ncraig: \"Ah well this gnome defector will stay where the action is at kde. Were there is no\n xiamian or eazel just hackers\" (same thread)\n<p>\ncraig: \"Kde has pulled so far ahead of gnome and the fact that kde id still a hacker afair has\nmade all the difference. The eazel thing and xiamian deal has really screwed gnome over.\nMean while kde pulls way ahead in developement and user base.\nIts no longer a 50 50 affair.\"\n(<a href=\"http://news.gnome.org/990590897/990642174/990642812/990650159/index_html\">gnome-news3</a>)\n<p>\nThat all says TROLL to me."
    author: "advocates are boring"
  - subject: "ADMIN: Enough is Enough"
    date: 2001-06-17
    body: "<i>[Please do not reply to this message.  <A href=\"mailto:navindra@kde.org\">Mail me</a> directly for comments.]</i>\n<p>\nI think it is a given that Craig is what he is. This has been on enough forums for enough times. The public has to decide nothing, and certainly not here on dot.kde.org.\n<p>\nPlease stop this bullsh*t, we will use IP banning if forced to do so.  Feel free to use our free-for-all <a href=\"http://slashdot.org/article.pl?sid=dot.kde.org&threshold=-1\">trollfest forum meanwhile</a>."
    author: "Navindra Umanee"
  - subject: "Re: Troll"
    date: 2001-06-17
    body: "Well showing one side of a converation is not a fair account of the conversation. I'm always a advocate and was very impressed my the gnome folks persistance in defending there project. The kde folks could learn a thing or two. But i was not purposely trolling i was inviteing them to the poll which spured a lively debate. The difference is i was not pretending to be something that i was'nt. Here i fear there are those that are not open with there motives.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "C'mon, this is lame.  You don't understand that the fact that this is his personal project.  This isn't your project for you to criticize.  He stated it was his first GUI app.  Who cares what it looks like.  Maybe you have to build a wheel before you can make a good one huh?  Well basicly, you won't have to worry about 'rebuilding the wheel'.  Pretty soon all the good ideas will play out and there will either be a victor, a collaboration, or a totally new program that'll combine the features of all of 'em together that will take the cake.  Just like it sucks to keep re-inventing the car, i'd sure hate to be forced to drive a Ford because that's the only car (because they didnt want to re-invent the car).  Some diversity is good, and I'm sure he didn't re-invent the inner workings which makes a mail program work.  I'm very sure he just used the code from any standard mail program, which is what OSS is inteded for.  No the whole damn project, just a bit of code here and there :^)\nnexen@qwest.net"
    author: "Daniel Remsburg"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "\"No doubt the problematic compiler from Red Hat 7.x included with Mandrake 8.0 also had a hand in the matter.\"\n\nthe problem is in infusion itself, not in the compiler.\n\n\"Most of gcc 2.96's perceived \"bugs\" are actually broken code that older gccs accepted because they were not standards compliant - or, using an alternative term to express the same thing, buggy.\""
    author: "Evandro"
  - subject: "Compiler Bashing"
    date: 2001-06-16
    body: "Well, my personal grudge against Mandrake 8.0's compiler is that it pretty much breaks <a href=\"http://www.sablevm.org/\">SableVM</a>.  One of the compiler bugs can be worked around by turning off GCC optimization, but the other one related to native interfaces doesn't seem trivial to work around.\n<br><br>\nThis basically means that Mandrake is useless as a development platform to me, and for real work, I have to use some other platform or install a good compiler.  So I'm quite happy to bash this compiler decision whenever I get the chance, and it's also quite possible I might have jumped the gun here.  :)\n<br><br>\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Compiler Bashing"
    date: 2001-06-16
    body: "while you're at it, when will we be able to post html again? seems like you can *grin*"
    author: "me"
  - subject: "Re: Compiler Bashing"
    date: 2001-06-17
    body: "I guess gcc 3.0 will also be useless as a development platform for you since it behaves just like gcc 2.96 relating source level compatibility. If your code breaks gcc 2.96 it will break gcc 3.0."
    author: "Carlos Rodrigues"
  - subject: "Re: Compiler Bashing"
    date: 2001-06-18
    body: "Oh yeah?  And if I turn off optimization flags it will compile certain things as opposed to when I turn them on (stuff that works on the stable version)?  How comforting.\n\nSigh.  Why aren't they fixing this?  And how do you know they aren't going to fix this?\n"
    author: "Navindra Umanee"
  - subject: "Re: Compiler Bashing"
    date: 2001-06-18
    body: "2.96 probably does more optimizations to the code than the stable version.\nIf those problems are compiler bugs, I'm sure they will fix them (they should). But nonoptimized/optimized code problems do not always mean compiler bugs, optimizations could just show bugs in the code that otherwise go unnoticed. Don't just go ahead and say it's the compiler's fault.\nI'm not saying that 2.96 that comes with RedHat is the best thing since sliced bread but it's not as bad as people say it is, I think it is rather good (for a stabilized cvs snapshot anyway, but I would also say it's better than the 2.95.x series), after all, they compiled a whole distribution with it including the kernel (and the kernel is well known to break some compilers, mostly due to kernel bugs but sometimes due to compiler bugs)."
    author: "Carlos Rodrigues"
  - subject: "Re: Compiler Bashing"
    date: 2001-06-18
    body: "Instead of the hand-waving, I respectfully suggest you check out SableVM yourself and the compilation errors first hand -- try to fix them even. I gave you the link in the original comment. :-)\n\nI don't know about Red Hat though.  I'm using Mandrake 8.0 which I presume took the broken compiler from Red Hat.  Because that's what they do."
    author: "Navindra Umanee"
  - subject: "Re: Compiler Bashing"
    date: 2001-06-18
    body: "Well, in my experience LM8.0 gcc is better than LM7.2 one, which would crash fequently when compiling KDE.\nThis used to be quite frustrating - since building it takes quite long already, but of course the rewards are quite big - the KDE developers code very fast ;-)\n<p>\nNo such problem with LM8.0 gcc - BTW, have you tried the latest builds from Cooker? Those have  some additional bugfixes which may just end up relevant."
    author: "A Sad Person"
  - subject: "Re: Compiler Bashing"
    date: 2001-06-18
    body: "Hmmm, I didn't use 7.2 very long (2-3 weeks) before I upgraded to 8.0 but I didn't have any problems with the compiler, and at least it compiled SableVM so I could do my work.\n\nHaven't checked Cooker, thanks for the tip.  It's probably easier for me to go backwards to a stable platform though. :(\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: ">\"Most of gcc 2.96's perceived \"bugs\" are actually broken code that older gccs accepted because they were not standards compliant\"\n\nWhen something worked before, and now it's broken, I call that adding bugs, not removing them :-)\n\nSeriously though, although being standards compliant is good, especially for porting to other compilers or architectures, it's the compilers job to compile as well as possible, even if that means accepting code that is written uncleanly."
    author: "Carbon"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "GCC 2.96 in RedHat 7.x and Mandrake 8.0 in general is a Bad Thing.  Both of these platforms are commonly used as a development platform for commercial applications.  Because of the fact the C++ ABI in GCC 2.96 is different from GCC 2.95 and will be different from even GCC 3.0, this should make RH7.x unsuitable as a commercial development platform, because you end up locking yourself to that platform and that platform alone.  If you were developing GTK apps, this is unlikely to cause a problem, but for QT and other C++ apps, this can be the difference from your program working everywhere and it working nowhere but RH7.x...\nThose who focus on the inability of GCC 2.96 to compile their favorite app as a sign of it's bugginess are wrong to blame the compiler.  The C++ ABI changes are the reason why 2.96 should not be used (even a 2.95.4 pre-release version would've been a better idea)."
    author: "Chad Kitching"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "> The C++ ABI changes are the reason why 2.96 should not be used\n\nABI compatibility problems are just a fact of life in C++ development. If you release binary packages, you just release one for each kind of compiler your customer uses, there is no \"locking in\". All software vendors do that."
    author: "Guillaume Laurent"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: ">If you release binary packages, you just release one for each kind of compiler your customer uses\n>All software vendors do that.\n\nYes, and each new incompatible platform have a price !!\n\nOk Redhat 7.x (gcc 2.96) have binary incompatibility with GCC 2.95\nBut if there is another binary incompatibility between 2.96 and 3.0\nYou end with 3 incompatible version of redhat in 10 month => No professional\n\nToday I prefer the way Suse manage this problem."
    author: "thierry"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "> You end with 3 incompatible version of redhat in 10 month\n\nAssuming that gcc 3.0 is immediately used by RH as soon as it's out, which is quite unlikely to say the least.\n\nPlus at the time they took the decision to use gcc 2.96, there was no telling when 3.0 would come out."
    author: "Guillaume Laurent"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "Are there developers working one Magellan it looks like the project is dead !!"
    author: "Is Magellan dead !!"
  - subject: "Everyone should just use gnus"
    date: 2001-06-16
    body: "... it does news and mail and with little lisp hacking will make either a calendar or mailbox metaphor for all your ICAL standard appointments .... \n\n\nSwitch to gnus now"
    author: "zoneur fou"
  - subject: "Everyone should just use an abacus"
    date: 2001-06-18
    body: "... it does addition, subtraction, multiplication and division.  It also comes in a range of sizes an colours, and can even fit in your pocket.\n\nSwitch to an abacus now."
    author: "John"
  - subject: "Re: Everyone should just use an abacus"
    date: 2002-08-03
    body: "Hi John,\nso where can you buy one and instructions on how to use this abacus (for divisions and multiplications especially...)"
    author: "Julien"
  - subject: "I NEED HELP!"
    date: 2002-11-15
    body: "Please help me. I need to learn how to multiply on a chinese abacus. Please contact me ASAP (As Soon As Possible) !!!!!!!!!! Thank you for your time.\n\n                         - Nikki S. Jones"
    author: "Nikki S. Jones"
  - subject: "Re: I NEED HELP!"
    date: 2003-05-13
    body: "Funny that: Alas this is many months late: (I hope this helps someone else if not you).\n\nThe first teaches how to use Chinese abacus.  The second teaches multiple versions of the abacus (from a cursory glance at the site).  The third I chose as an additional resource.  (These are the best three of what I have found)\n\n1)Complete site on the Abacus - How to use, build, history, etc. (2/5 bead)\n(Good Resource)\nhttp://www.ee.ryerson.ca:8080/~elf/abacus/\n\n2) Math Mojo with an Abacus   (Good Resource)\nhttp://www.mathmojo.com/index.html3)\n\n3)Think Quest on line: Abacus\nhttp://www.thinkquest.org/library/lib/site_sum_outside.html?tname=C005843&url=C005843/english/html/lessons.html\n"
    author: "Dee"
  - subject: "More on Citadel"
    date: 2001-06-16
    body: "Art wrote me with more exciting details on the upcoming version(s) of Citadel. Quoted here with author's permission.\n<p>\n<i>Art J. Cancro writes:</i>\n</p>\n<p>\nHi.  I saw your review of the Infusion client and the Citadel/UX server,\nand was quite happy to read it ... because I happen to be the lead\ndeveloper for Citadel/UX.  :)\n</p>\n<p>\nOne thing I thought you might like to know -- the version of Citadel/UX\nwhich is about to be released has working IMAP support.  So you'll be\nable to point your \"mutt\" client at it using IMAP, and it'll work. Private and public folders, it's all there!\nIncidentally, Citadel also has a web-based front end called WebCit.\n It's very nice, and once Brian is finished with the calendaring stuff\nin Infusion, I intend to add web support for that as well.\n</p>\n<p>\nThen comes the really ambitious undertaking: we're planning to write a\nMAPI driver for Citadel.  This will make Citadel the first open source\ngroupware server to fully support the calendaring/scheduling features in\nMicrosoft Outlook to the same degree that Exchange does.  This is\nsignificant because on a Citadel network, non-Outlook users don't have\nto be second class citizens.  You'll have full access to the calendar\nfunctions (and everything else) regardless of whether you're using\nOutlook, Infusion, or a web browser.\n</p>\n<p>\nJust thought you'd like to hear more about what's going on with what I\nbelieve is an important project that has lots of potential.\n Incidentally, I am composing this e-mail to you using the SMTP/IMAP\nclient in Netscape 6.1PR1, connected to my Citadel server.\n</p>\n</i>"
    author: "Navindra Umanee"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-16
    body: "If you truly want to compete with Outlok and Outlook Express, Secure Password Authentication needs to be added. This is so vital to some of us, I can't understand why noone has done it yet."
    author: "Jason"
  - subject: "Re: Focus on Infusion"
    date: 2001-06-17
    body: "Evolution has already done it..."
    author: "anon"
  - subject: "Infusion & other multi-function messaging services"
    date: 2001-06-16
    body: "Let me mention another similar solution in the same problem/solution-space, Bynari's Insight [formerly TradeServer/TradeClient] at http://www.bynari.net\n\nHowever, if you look at SourceForge, the most popular mailserver seems to be CourierMail [ # 18 most popular download this week ]. Probably because it offers a web interface as an option. Things like global address books and calendaring are important, but without a web interface, it is only half-way there. There seems to be two threads here: 1. The 'Enterprise' Exchange type server that give the typical office worker a calendar and 'global' address book on their local server and maybe IMAP (ie stays on the server, not downloaded). 2. Internet mail, either through a mail client or the browser.\n\nA killer app will be the one that combines the two..."
    author: "Clay Daniels"
  - subject: "Re: Infusion & other multi-function messaging services"
    date: 2001-06-18
    body: "\"There seems to be two threads here: 1. The 'Enterprise' Exchange type server that give the typical office worker a calendar and 'global' address book on their local server and maybe IMAP (ie stays on the server, not downloaded). 2. Internet mail, either through a mail client or the browser.\"\n\nActually, Microsoft Exchange does both of those.  There is an Outlook Web Access component that gives you client that lacks many of the additional features of the full Outlook client, but is still reasonably usable, even on non-Microsoft platforms (although Konqueror has problems with the javascript on OWA for Exchange 5.5).  I think any program that wants to get well used will need to implement both, because accessing e-mail through a webbrowser when you're local seems rather inefficient, and using a mail client to read your mail when you're remote seems insecure and usually too slow.  Web-based mail systems seem to work the best when you're remote and just need to quickly check in, whereas local clients seem to work best when you're hardwired into the LAN, where you'd like nearly instant notification when someone mails you.\n\nWithout both these features, it'll be hard to compare to conventional proprietary systems like Lotus Notes, Microsoft Exchange and Novell Groupwise."
    author: "Chad Kitching"
  - subject: "Re: Infusion & other multi-function messaging services"
    date: 2001-06-18
    body: "Besides  'complete solutions' (i think citadel/UX is such one, but i may be wrong), i would like to have 'modular solutions'. Look at the existing unix system for mail: we have an incoming mailserver like sendmail/postfix/qmail/exim, we have unix accounts, we have procmail, we have things like pop3d/imapd ...\nI'd like a similar solution for all the other features we need in a pim solution, and maybe even we could reuse existing components (we don't need a new mta ...)"
    author: "ik"
---
A few days ago, <a
href="http://www.shadowcom.net/Software/infusion/">Infusion</a>
(<a
href="http://www.shadowcom.net/Software/infusion/screenshots.html">screenshots</a>)
was announced on <a
href="http://apps.kde.com/nf/2/info/vid/3290">apps.kde.com</a>. Along with <a href="http://uncensored.citadel.org/citadel/">Citadel/UX</a> serving as backend, Infusion aspires to compete with
the likes of <a
href="http://www.thekompany.com/projects/aethera/">Aethera</a>, <a
href="http://www.kalliance.org/">Magellan</a>, <a
href="http://www.ximian.com/apps/evolution.php3">Evolution</a>, and yes, Microsoft <a
href="http://www.microsoft.com/outlook/">Outlook</a>+<a
href="http://www.microsoft.com/exchange/">Exchange</a>.  Is
Infusion there yet?  Nope.  But from what I've seen, I've certainly
been impressed by <a
href="http://uncensored.citadel.org/citadel/">Citadel/UX</a>, and once
I managed to get Infusion compiled, I was able to enjoy some neat
functionality.  Coupled with the enthusiam of author <a
href="mailto:brian@shadowcom.net">Brian Ledbetter</a>, it would seem that Infusion is
going places.  Read on for further details of my Infusion experience
and for an interesting interview with the author. <b>Update: 06/16 03:30 AM</b> by <a href="mailto:navindra@kde.org">N</a>: Art wrote in with <a href="http://dot.kde.org/992627943/992676648/">some interesting comments</a> on the upcoming version(s) of Citadel.
<!--break-->
<br><br>

<h3>A Little Test Drive</h3>

Most of the packages I needed to
compile Citadel+Infusion were readily available with the Mandrake 8.0 Standard
Edition, with the exception of of libical which I obtained from <a
href="http://www.rpmfind.net/">rpmfind.net</a>.  Unfortunately, the
build experience itself left something to be desired with less than optimal
configure scripts and minor source bugs. No doubt the problematic
compiler from Red Hat 7.x included with Mandrake 8.0 also had a hand
in the matter.  After several source and makefile "adjustments"
however, I managed to get Citadel, libCxClient and Infusion all
installed and working.  Despite all this, I should note that my efforts were nothing compared to the pain and tragedy I've witnessed fellow colleagues go through while installing (or re-installing) Exchange -- things can only get better from here.

<br><br>

Citadel/UX's BBS roots are almost immediately apparent, although the
setup is fairly painless to a seasoned Unix user. Simply run the setup
script, create a sysop account, login and then create the basic
configuration.  A good idea at this point is to create a couple of
test users for Infusion.  

<br><br>

Citadel/UX is pretty impressive, including such things as SMTP and POP
servers in the package.  Citadel can send mail between local users,
send and receive internet mail, and make mail available to internet
users.  This brilliant arrangement means that, as far as mail is concerned, any
diehard <a href="http://www.mutt.org/">Mutt</a> user such as myself
can happily coexist on a Citadel/UX network with Infusion users.  Brian had a good idea when he chose to base
Infusion on Citadel/UX.  One might even ask why other Open Source
projects such as <a
href="http://www.thekompany.com/projects/aethera/">Aethera</a> and <a
href="http://www.ximian.com/apps/evolution.php3">Evolution</a> don't
consider improving and sharing this capable backend.

<br><br>

I was able to test basic Infusion functionality with 2 test users.  I could mail back and forth, I could send mail to an internet account, I could receive internet mail, and I could view shared areas.  Infusion also has chat capabilities and token organizer/notes functionality which I did not fully investigate. Infusion is evidently here both in concept and proof-of-concept, including the expected bugs.  The colourful interface will surely evolve to please many in the future. 

<br><br>

So should you rush out and download Infusion?  No. Set your expectations
too high, and you will be disappointed. Infusion is still at
the early stages, but if you enjoy tinkering, or you're a developer
who can help, or you or your company has an interest in collaboration
systems, then check this out.

<br><br>

Want more?  There's more.  Get the facts below from author Brian
Ledbetter who was kind enough to answer a few questions.

<br><br>

<h3>Talking To The Author</h3>

<b>How robust is Infusion/Citadel, and what kind of environment have
you used it it in?</b>

<br><br>

Infusion by itself is actually not a very capable application, and is
still in the early stages of development.  Right now, Infusion is tied
in to the Citadel/UX backend 100%, kinda like how most of the
components in Microsoft Outlook are only useful with an Exchange
server in the backend.  I envision the ability to add plug-ins for
SMTP/IMAP/etc. ala Outlook sometime in the future, however.

<br><br>

The Citadel/UX Communications Server is a promising platform.
While many of the newest features are still in the development stage,
it is currently a rather powerful communications platform.  It's had
over 10 years (actually, almost 25 years, if you count the old
non-UNIX variants) of development behind it.  There are sites
currently running Citadel/UX which have thousands of users, with a
large number (I'm probably not the best person to ask here) of
concurrent users accessing the system.

<br><br>

<b>How does Infusion compete with Microsoft Outlook/Exchange,
Aethera/Magellan/KMail, Evolution etc.</b>

<br><br>

Infusion is a direct competitor of any MUA (mail-user-agent), though
due to how early in development I am, it's not all that capable yet.
Specifically, Infusion is aimed at competing directly with other
group-scheduling packages, such as Microsoft Outlook and Evolution.

<br><br>

The Citadel/UX server is aimed at offering a powerful group-scheduling
or internet service server similar to Microsoft Exchange Server.  The
maintainer of this project is Art J. Cancro; see the <a
href="http://uncensored.citadel.org/citadel/">Citadel/UX webpage</a>
for more information.

<br><br>

<b>Do you see Infusion/Citadel being used to communicate
cross-platform and with other (possibly non-Citadel) clients?</b>

<br><br>

Any client can access a Citadel/UX server via a standardized library,
libCxClient, which will soon be a part of the official Citadel/UX
project (I'm maintaining it until then).  Also, end-users can access
Citadel/UX through standardized protocols -- It communicates via SMTP
and POP3, currently, with full IMAP support on the way.

<br><br>

Like I have said, I plan on adding 'hooks' to Infusion to allow it to
communicate with other backend servers.  This is still on the
drawing-board only, though.

<br><br>

<b>I've been given to understand that Infusion currently shares some
code with other KDE apps such as KOrganizer or KNotes.  What are your
plans for future integration with the rest of KDE?</b>

<br><br>

Infusion currently shares some code with <a
href="http://korganizer.kde.org/">KOrganizer</a>, but after
discussions with the head of the KOrganizer project, Cornelius
Schumacher, I will be stripping the code out and utilizing the KParts
API exported by KOrganizer instead.  This way, we have a standardized
KDE calendaring component, without having to maintain two seperate
portions of code.
 
<br><br>

As far as the notes go, Infusion has only native-code, with me referencing
the KNotes source for hints.  :)

<br><br>

<b>What previous development experience have you had?  Why did you
start this project?  Have you been doing this project in your spare
time?  What were your experiences while developing for KDE/Qt?</b>

<br><br>

Actually, Infusion is my first try at writing a GUI-based application.
The KDE libraries have made this much easier than I ever thought it
could've been, even without using tools like <a
href="http://www.kdevelop.org/">KDevelop</a>.
 
<br><br>

As far as past experience, I've not really done anything of note in
the open source world.  I've been working in the software industry for
about 5 years now (mostly web application development, which, after
writing <i>real</i> software, doesn't seem all that impressive ;).
I've worked for a few companies during this timeframe, though my
employment has generally gravitated towards web software development.
 
<br><br>

The Infusion project is currently done entirely in my spare time, what
little bit of it I have.  According to my CVS logs, which vaguely
resemble my memory, Infusion was first checked in on November 17,
2000.  It's gone through quite a few changes since then.  I hope that
before too long, it can prove to be a valuable addition to any
enterprise seeking an affordable groupware platform.  I certainly hope
to use this project to prove undoubtably that UNIX <i>can</i> be as
user-friendly as it is powerful.

<br><br>

<b>Anything else you'd like to add/comment on?</b>

<br><br>

KDE rocks!  If I were to try developing Infusion in <a
href="http://www.gtk.org/">GTK</a> (I did, actually, but scrapped it
almost immediately after beginning), it would've taken twice as long
as it has, and it would've been substantially larger.  I would like to
offer my sincerest and most gracious <i>thank you</i> to all of the
KDE developers out there, who have given us such a wonderful
development (and user) environment for UNIX!