---
title: "KDE 3.0Alpha1 Developer's Release Ships"
date:    2001-10-07
authors:
  - "Dre"
slug:    kde-30alpha1-developers-release-ships
comments:
  - subject: "Clipboard?"
    date: 2001-10-06
    body: "Does KDE3a1 also fix the clipboard problems, just by switching to QT3?"
    author: "Carbon"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: "yes, clipboard!=selection, finally."
    author: "anonymous"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: "... but I *liked* the clipboard being the selection... what does QT 3 have to offer me?"
    author: "Jon"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: "You can still use your X middle-mouse-button text buffer thingy, but the real clipboard is now independent of it.  So using Ctrl-C to copy uses a different clipboard than simply selecting with the mouse and hitting the middle mouse button.  It's the best of both worlds!"
    author: "not me"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: "Wow, we've been asking for this exact behaviour for so long, and finally, here it is!\n\nThis alone makes it worth the download ;) (for me, anyway)"
    author: "dingodonkey"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: "*devil's advocate*\n\nAnd gnome has had it, as have other X apps :-)\n\nThis has always been the ICCCM (the X11 document describing the way apps should interact) stance on how the clipboard should work. Nice to see KDE getting it right at last, this should be popular with Windows converts and X11 die-hards alike :-)"
    author: "Kevin Puetz"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: ": And gnome has had it, as have other X apps :-)\n\n   Yes, but I refuse to use Gnome because it's coded in perl... that's no way to write a GUI.  It's just too slow.  Plus, since Ximian owns the code, they can start charging for it anytime they want."
    author: "Matt Black"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: "I would like to correct some errors of yours:\n\n: Yes, but I refuse to use Gnome because it's coded in perl\nActually, it's written (mostly) in C.\n\n:Ximian owns the code, they can start charging for it anytime they want\nNo, all the code is GPL/LGPL, so - even if the code were Ximian's property -this could not happen.\n\nHaving said that, I would like to add:\nKDE rocks!!"
    author: "Federico Cozzi"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: ": I would like to correct some errors of yours:\n\n    Oh, hell... let the countertrolls do their work.  I'm tired of the BS about C++ being unstable, and Gnome's use of GTK better because it runs on Windows (as if Qt didn't and as if either Gnome or KDE ran on Windows in anything more than proof of concept works).  Logical arguements haven't worked, and the CounterTroll (and the other fauna due soon, the TrollKiller, the TrollSpotter, the Moderator, the GrammarNazi and others) is a sign that the ecology of this message board just went beyond normal users.  \n\n    Take heart - at least it's a sign that KDE has a high enough visibility so that people who don't even use it (and indeed, don't care about it) can flourish in the nooks and crannys of its online userbase.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Clipboard?"
    date: 2001-10-08
    body: "i think he was joking .."
    author: "ik"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: "Oh no, don't get me started on GNOME.  I'll keep my mouth shut this time, only to avoid an \"incident\""
    author: "dingodonkey"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: "Finally!  Woohoo!"
    author: "MP"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: "Well, to copy anything, you first must select it. Only thing is, now the middle-mouse-button selection buffer is seperate from the clipboard. Imagine this:\n\nIn KWord, you select and then cut the words \"The quick brown fox\"\nThen, you just select and then delete \"Jumped over the lazy dog\"\n\nNow, just Ctrl+V and MMB, and you get... well, you can figure it out :-)\n\nJust think of the clipboard as a second selection buffer, that's slightly tougher to get stuff into."
    author: "Carbon"
  - subject: "Great improvement from nothing! :)"
    date: 2001-10-07
    body: "I am happy and appreciate this feature. I was very annoyed with unexpected clipboard selection (both text and files). Thank god, KDE is somewhat more user friendly ;)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Great improvement from nothing! :)"
    date: 2001-10-07
    body: "Well, now the only thing I can think of that would be nice in the clipboard is easily copying and pasting KParts between apps. I.e. it would be nice if you could copy a Kontour part from within KWord, and then paste it into a presentation within KPresenter. Is something like this already working at this point?"
    author: "Graphite"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: "The only proble that I have with the current clipboard behavior is that if something is copied into the KDE (Klipper) clipboard if does not showup in the X-Clipboard. \n\nYou have to actually paste it into the X-Clipboard.\n\nOn the other hand, if something is copied into the X-Clipboard, it does showup in the KDE (Klipper) clipboard.\n\nIs this problem being fixed?\n\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Clipboard?"
    date: 2001-10-07
    body: "Yes, I fixed this a few days ago."
    author: "Carsten Pfeiffer"
  - subject: "The BUGS"
    date: 2001-10-07
    body: "I do hope that somewhere along the way that someone will fix the various minor bugs.  I realize that this is not as much fun as coding for new features.\n\nBUT, if KDE is going to become a comercial quality product, this must be done.\n\nAND, yes, I am willing to submit bug reports on these and to provide the developer that is working on it more detailed information, to try patches, discuss things in detail, etc.\n\nI am running the current CVS KDE_2_2_Branch.  Except that kdebase seems to be broken and I had to download the 2.2.1 tarball.\n\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: The BUGS"
    date: 2001-10-07
    body: ": I do hope that somewhere along the way that someone will fix the various minor bugs.\n\n    3.0 is pretty much a port of the existing code to Qt 3.0.  Along the way, a whole slew of new bugs will be picked up, so in a way, the only thing that 3.0 will be is bug fixes - pre-existing and new ones.\n\n    At the same time, there is a 2.2.2 branch that was announced that will just have some bug fixes in it, with no new features.\n\n    Here's your cake, and you can eat this cake over here, too.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: The BUGS"
    date: 2001-10-08
    body: "Of course hackers are constantly also bug fixing (e.g. I did nothing else the last weeks since I didn't have time to do something else), but look at the huge list on bugs.kde.org, many bugs are not always reproducible, some bugs are more like feature requests, some bugs are known but nobody knows how to solve them, well, it's not that simple to fix them all ;-)\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "regular expression class"
    date: 2001-10-07
    body: "Is the new regular expression class designed to be compatible with the one in boost? (http://www.boost.org)? If not, why? These guys have spent a lot of energy getting the interface correct in order for a feature like this to become a standard in c++ sooner or later. But having multiple incompatible interfaces for the same thing (implementations don't matter) are really going to cause a lot of trouble later. \n\nAnd if it was examined and found to \"not meet your needs\", was any attempt made to tell the boost developers what you needed?"
    author: "me"
  - subject: "Re: regular expression class"
    date: 2001-10-07
    body: "I guess the main concern is Unicode support.  Does the boost regex class support it?  Another question I've always had is: does std::string support Unicode?  A standard Unicode string interface is much more important than regex.\n\nOh well, the QString class is awesome."
    author: "Justin"
  - subject: "Re: regular expression class"
    date: 2001-10-07
    body: "Yes, std::string supports unicode. Actually std::string is just a typedef for\n\nstd::basic_string<char>. You might replace that char with everything, even your own personal humpty-dumpty Char-object. Unicode is there. Brought to you by the power of c++-templates.\n\nFrank"
    author: "Frank Foerster"
  - subject: "Re: regular expression class"
    date: 2001-10-07
    body: "well, this would be a question best put to Qt developers, not KDE developers, since the class (QRegExp) is in Qt, not kdelibs. the version put in qt3 is perl-compatible, like boost's regexp class. however, I think that the trolltech developers didn't use boost's regexp class for a number of reasons:\n\n1. qregexp uses QString. This is a fully unicode-compatible string class used everywhere in Qt (along with QCString). Boost's regexp class uses std::string. This is not used at all by Qt, afaik. Neither is much of the c++ system libraries or the stl. Qt provides adequate, if not better replacements for all of these. Why? Because Qt has existed long before these things were standardized among many implementations. \n\n2. qregexp has existed for a long time, even before boost's regexp class. qregexp is used to many Qt/KDE apps already, so this new version is mostly a dropin replacement for the old qregexp (with a _LOT_ more functionality, of couse)."
    author: "Jesus Christ"
  - subject: "Re: regular expression class"
    date: 2001-10-07
    body: "lol, we all knew along that jesus christ was a KDE developer. nowonder that KDE is so great ;-)"
    author: "jds"
  - subject: "Re: regular expression class"
    date: 2001-10-07
    body: "I wonder if he uses kde on jesux"
    author: "Gayr Harnard"
  - subject: "Re: regular expression class"
    date: 2001-10-08
    body: "AFAIK the new Qt regexp class is designed to be compatible as much as possible with Perl regexps.\n\nAlex"
    author: "aleXXX"
  - subject: "Screenshots?"
    date: 2001-10-07
    body: "Anyone care to post them?"
    author: "Robert Gelb"
  - subject: "Re: Screenshots?"
    date: 2001-10-07
    body: "There is not much new to see except a splash screen and an optional side image for K menu which both will be changed before KDE 3."
    author: "someone"
  - subject: "Re: Screenshots?"
    date: 2001-10-07
    body: "omg... Did they *really* have to copy that??\n\nNow there's really no point in defending KDE when to comes to a 'KDE looks like Windows' flame. They have won without a battle."
    author: "Richard Stellingwerff"
  - subject: "Re: Screenshots?"
    date: 2001-10-07
    body: "and an __optional__ side image for K menu"
    author: "Lubos Lunak"
  - subject: "Re: Screenshots?"
    date: 2001-10-07
    body: "It's optional in windows too..."
    author: "Richard Stellingwerff"
  - subject: "Re: Screenshots?"
    date: 2001-10-07
    body: "OK, so then let's make it mandatory... ;)"
    author: "just me"
  - subject: "Re: Screenshots?"
    date: 2001-10-07
    body: "I like the DOM tree viewer in konqueror: <a href=\"http://wojas.vvtp.nl/KDE3-konqueror-domtree.png\">KDE3-konqueror-domtree.png</a>"
    author: "Konrad Wojas"
  - subject: "Re: Screenshots?"
    date: 2001-10-09
    body: "I have the DOM-tree viewer too, but I use KDE 2.2.1. I think it is included in kdeaddons."
    author: "proc"
  - subject: "CVS Tag?"
    date: 2001-10-07
    body: "What is the CVS tag for the alpha1 release?"
    author: "Luser"
  - subject: "Re: CVS Tag?"
    date: 2001-10-07
    body: "KDE_3_0_ALPHA_1"
    author: "someone"
  - subject: "SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-07
    body: "There are some things that I'd really like to see in the next release of KDE.  A few of them are eye-candy, but the rest I feel are truly needed.  What do you think? \n\nSVG Icons: \n\nI've found this *very* neat screenshot of Gnome (rather nautilus) supporting SVG icons:   http://jimmac.musichall.cz/screenshots/e-sync.jpeg.\nThese are really nice.  Maybe it's the cartoony style of the icons, I don't know... but I really like them over the current KDE icons. \n\nTranslucency: \n\nAlso, I've heard that QT 3.0 will support xrender fully -- providing access to \"true\" transparency.  What does this mean?  Is it the kind of transparency provided by Mosfet's translucent menu style,  or would it be true *true* transparency, where graphics /underneath/ the translucent area change, and you can see the change through the translucent area?   I figure this full kind of transparency would be needed if KDE were to implement drop shadows for windows, menus and dialog boxes;  if the windows under the drop -shadow change their views, then the kind of transparency we see in Mosfet's style engine would not update the drop-shadow... just a thought...   \n\nBIDI support:\n\nhow long until KOffice shows real support for Hebrew?   Abiword is making (admirable) steps to support it, but they are hampered until gtk 2.0 comes out (with bidi & aaliasing support).  \n\nIntegrating CD mastering into Konqueror:\n\nI wish to develop an ioslave or kpart implementing cd mastering and arrangement-making capabilities into Konqueror (as a kpart or ioslave).  So far, whomever I find to help me drops out after a day.  Anyone want to see this capability in the next release of KDE?  contact me.  This could be very powerful.  Windows XP has this, albeit in a braindamaged way (you have to drag your files to a cd-burner 'trash-can' and hit 'burn';  my idea is more thought-out).  Again, contact me for details (katz@wam.umd.edu, IM: MnicHisteriaNARF )\n\nThanks!\nRoey"
    author: "Roey Katz"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-07
    body: "About transparency:\nI think it's really important, but not for fancy effects; transparency is the key to a full support of the PNG format. I would love a full alpha support of PNG images in Konqueror - that would make it an even better browser!"
    author: "Federico Cozzi"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-07
    body: "Roey says :\n\n> There are some things that I'd really like to see in the next release of KDE. A few of them are eye-candy, but the rest I feel are truly needed. What do you think? \n\nI think that eye-candy things are now uninteresting. It is not important for KDE to copy the MS Windows, Gnome or Mac look. KDE has its own look, I appreciate it, as many users, and the way is to continue, with some little improvements (example : coloured, perhaps animated, cursors)... There are so many things to do more useful that copying looks. But copying good functionnalities of others desktops is useful, of course. For instance, I recently discovered in KDE the ability to put in a window a menu of the K Menu (as in Gnome). Very good ! Thanks ! Also I now can use Kicker in a left vertical bar (without any damage). Very useful for the management of the space in a 19\" screen !\n\nAlas, all is not implemented in KDE 2.2.1, so I hope for KDE 3.... For example any program MUST memorize the location and status of the bars (I am tired of changing the properties of the Kate bar or the Quanta + bar...)."
    author: "Alain"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-07
    body: "> I think that eye-candy things are now uninteresting.\n\nI disagree. Some people choose other environments over KDE because of the look. That can be avoided only by offering as many alternative looks as possible. The G-team has guys like tigert and jimmac and that's the reason why some people choose it over KDE. Yes, KDE might offer better functionality, but that proves the importance of looks. It is NOT to be taken lightly.\n\n> There are so many things to do more useful that copying looks.\n\nI agree. Especially in the case of copying Windows. When I left Windows for Linux I wanted something drastically different. I had a hard time using KDE 1.1.2 because of that. It has gotten better over the years, but we can't stop evolving the KDE look.\n\nSo, people out there. If you're more of an artist than a coder, please serve the community by creating different kinds of themes and icons. Submit them to the kdeartwork module and who knows, one day a person might choose KDE over another DE because of that.\n\nPeace."
    author: "Matt"
  - subject: "KDEs icon style is too much like windows"
    date: 2001-11-13
    body: "They SHOULD make their own style of icons, look at what they talk about in mailing lists\ncopying XP.\n\nBut they have a point, before you can have your own style you must copy the style of what you know works.\n\nbetter to experiment later, first we need something to compete with windows."
    author: "Lucian"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-07
    body: "Yes, all those above and a drop shadow on every window ala Mac OS X would be nice ;-)."
    author: "LocoHijo"
  - subject: "This is what I'm talking about"
    date: 2001-10-07
    body: "What KDE 3 should look like ..."
    author: "LocoHijo"
  - subject: "Re: This is what I'm talking about"
    date: 2001-10-07
    body: "That's a REALLY beautiful mock-up!\nIf KDE had those dropshadows I think it would be easy to attract non-geeks!"
    author: "iuesyfntwe93268ew"
  - subject: "Look At Image Of Parent"
    date: 2001-10-08
    body: "Truly, truly beautiful. What a way to give differentiation of space. The only improvement I can think of adding is to brighten up the left and top of the window borders to differentiate two windows side by side where no drop shadow can determine depth."
    author: "Viracocha"
  - subject: "Re: This is what I'm talking about"
    date: 2001-10-08
    body: "That is really nice.  It would be nice to have a plugin archetecture for WM effects.  That way, it wouldn't have to be part of the theme, but you could add as many effects as you want.  Pie in the sky, I know, but it's an idea.  Stuff like window effects when minimized/maximized, effects when switching desktops, etc.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: This is what I'm talking about"
    date: 2001-10-16
    body: "He yes ... we could have a \"genie\" effect like in Mac OS X !!\n\nIt's beautiful ... and i think visuel apparence of a wm is very important"
    author: "Joe"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-07
    body: "Roey wrote:\n\n> I've found this *very* neat screenshot of Gnome (rather nautilus) supporting\n> SVG icons: http://jimmac.musichall.cz/screenshots/e-sync.jpeg.\n> These are really nice. Maybe it's the cartoony style of the icons, I don't\n> know... but I really like them over the current KDE icons. \n\nThe Gnome icons are indeed quite nice. Icons in KDE as nice as in Gnome would be a cool feature for KDE3. But I have no idea about creating icons, and I admire everybody who has. Someone out there should create some nice looking icons for KDE...\n\nHeiner\n\nP.S. I have to revise this a bit: Some of the icons of kde are quite nifty. For instance the K of the KMenu and the icons of konsole and konqi.\nBut konqi's stop button isn't as nice..."
    author: "Heiner"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-07
    body: "I think whether the icons in Gnome are \"nicer\" than those in KDE or not depends a lot on your taste. When I last tried gnome, my reaction was, looking at the panel \"Boy are these icons ugly compared to the KDE ones\"."
    author: "A Sad Person"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-08
    body: "GNOME icons are too serious for me.  If you want some good icons, talk to this guy, I just love the icons he's making here (especially the hardware ones):\n\nhttp://users.skynet.be/bk369046/icon.htm"
    author: "dingodonkey"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-07
    body: "Roey wrote:\n\n> I've found this *very* neat screenshot of Gnome (rather nautilus) supporting\n> SVG icons: http://jimmac.musichall.cz/screenshots/e-sync.jpeg.\n> These are really nice. Maybe it's the cartoony style of the icons, I don't\n> know... but I really like them over the current KDE icons. \n\nThe Gnome icons are indeed quite nice. Icons in KDE as nice as in Gnome would be a cool feature for KDE3. But I have no idea about creating icons, and I admire everybody who has. Someone out there should create some nice looking icons for KDE...\n\nHeiner\n\nP.S. I have to revise this a bit: Some of the icons of kde are quite nifty. For instance the K of the KMenu and the icons of konsole and konqi.\nBut konqi's stop button isn't as nice..."
    author: "Heiner"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-07
    body: "We need a well-structured way of contributing icons.  Someting a-la sourceforge that lets kde users vote on what gets included in the next version.\n\nA single icon  (for instance, a replacement for the K icon) is too small to distribute as a thing in and of itself, but needs to be packaged together w/other people's icons.....\n\nAlso, we need a good resource for finding where certain icons are stored or how we can tell kde to use a png or xpm other than the default.  I have been trying to replace the K icon and cant figure out how to do it, despite looking at other themes out there that do it, and adding these lines to my liquid.themesrc (in /usr/share/apps/kstyle/themes):\n\n[Icons]\nPanelGo=liquid/k.png:liquid/mini-k.png\n\n(this is something i copied from an iMac theme floating around the web, alas themes.org is down!!!!)\n\nattached, find the K icon that i wan to use instead of the default."
    author: "Pablo"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-08
    body: "> We need a well-structured way of contributing icons. \n\nIf you want to submit icons feel free to send them to icons@kde.org.\nIf you're an experienced artist you might also want to discuss/contribute stuff on kde-artists@kde.org or #kde-artist on IRC.\n\nFor more information on icons in KDE you might want to read artist.kde.org\n\n> Also, we need a good resource for finding where certain icons are stored \n> or how we can tell kde to use a png or xpm other than the default. \n\nftp://ftp.derkarl.org/pub/incoming/icnthm-doc.tar.gz\n\nmight save your day :-)\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-08
    body: "Thanks!"
    author: "Pablo Liska"
  - subject: "the attachment"
    date: 2001-10-07
    body: "attaching?"
    author: "Pablo"
  - subject: "Re: the attachment"
    date: 2001-10-08
    body: "Is it just me, or is that a 70's era K button? :-D"
    author: "Carbon"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-07
    body: "http://users.skynet.be/bk369046/icon.htm\n\nDid you see this icons ? Jimmac or Tigert icons arent better than this ones."
    author: "Daniel"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-08
    body: "I set those icons up for my parents and my friends, as well as myself, and we all love em.  Some need work (especially 32x32 icons on my 1600x1600 resolution), but all-in-all they are beautiful.\n\nThe hardware ones by far surpass those included with KDE..."
    author: "dingodonkey"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-08
    body: "I have to disappoint you :( These icons are icons form Microsoft Windows XP."
    author: "antialias"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-09
    body: "Not based on the screenshots of Windows XP, but then again I've not seen any for a while. Can you point out a screenshot that shows that these icons are a copy of the Windows XP ones?\n\n--\n\nA more general point: I think Nautilus had the right idea in having icons as overlaid collections. We already have a general 'page' for a document... so why not make that a file on it's own, and overlay the 'file type' indicator on top? Then you have the overlays for 'link', for 'read only', and so in. Particularly nice is that way that you can 'annotate' the icons with indicators of your own. It's one area that Nautilus does really well in (for 3 million dollars you'd hope that it did well :). Given a bit of work, Nautilus and KDE could have a common icon collection format, which would mean people would be able to create icons for both KDE and Gnome really easily..."
    author: "Jon"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-09
    body: "The first 3 releases of Kicons featured a lot of XP icons. The successor of the third release, called 'iKons 0.4' doesn't contain these XP icons. Some people argued that some of my icons looked to much like the WinXP icons. In order to avoid any legal problems, I decided to remove these icons or replace them by other icons. 'iKons' 0.4 is a first step in this direction: I redid most of the icons and added some new icons.\nBy 'iKons 0.5', there won't be any XP icon left. At this time, I'm replacing the folder icon with the two people on it, the pda icon and the scanner icon."
    author: "Kristof Borrey"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BI"
    date: 2001-10-07
    body: "CD mastering is already handled very well with KreateCD. It's a very nice app. I don't see particuarly any advantage in doing it in Konqi, other then the niftiness factor (\"Hey, i can view websites, manage files, rip and burn CDs, and preview Koffice documents all from the same app\")"
    author: "Carbon"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-07
    body: "BIDI support is built-in to QT3, so I think it will probably be supported in the next KOffice release.\nI don't think that there will be true transparency, I'm not sure its even possible with X.\nThose SVG icons do look cool, but unless you have a Cray or two, wouldn't they be unbearably slow?"
    author: "not me"
  - subject: "Re: SVG Icons for KDE 3.0, \"true\" translucency, BIDI.."
    date: 2001-10-08
    body: ">Integrating CD mastering into Konqueror\nI think integrated CD mastering in Konqueror would be nice, but I think support for RW CD's is more important. That means using of RW CD's just like a disk as it is possible under windows. But I think this is not a problem of KDE, this is a problem of Linux, because no writeable UFS filesystem is available."
    author: "Bernd"
  - subject: "Re: SVG Icons"
    date: 2001-10-09
    body: "The legal status of SGV is VERY dubious.\nIn fact, it is pretty much in the same shape GIF is, except that with SVG the patent is held by apple.\n\nIMHO, before someone spents a month doing SVG icons, this needs to be investigated.\n\nAfter all, it is not like much has to be changed on KDE to make it have SVG icons, once there is free code out there that reads it."
    author: "Roberto Alsina"
  - subject: "Re: SVG Icons"
    date: 2002-01-31
    body: "Huh? SVG is a W3C standard."
    author: "Josh"
  - subject: "Quanta"
    date: 2001-10-07
    body: "I see in the KDE 3.0 Planned Features that Eric Laffoon is working on Quanta. Is it a new version of Quanta + ?"
    author: "Alain"
  - subject: "Re: Quanta"
    date: 2001-10-07
    body: "Jeez... now my name is floating before my eyes while I read about KDE... what did I do? Oh yeah... Quanta ;)\n\nAs I have mentioned in talkbacks and in private emails my partners, the brilliant and witty Dmitry and Alex, decided to do a proprietary version with theKompany. While I have been supportive of theKompany and remain friends with Dima and Alex I strongly opposed this move. I don't want to get into all my reasons or any politics, but my opinion is that Quanta should remain a viable GPL'd program.\n\nWhile I have decades of experience in computer service and programming I am just a newbie at C++ but I'm getting better. I have had inquiries from a number of people interested in getting into a new development effort. I'm also in the process of putting together a new web development company and just about to close a very sweet deal. This will put me in a position shortly to be able to allocate a large portion of my time to coding and overseeing development of Quanta. I have a long list of things I want to do and I am adamant about this development not being half hearted or lagging. I did spec a lot of what went into Quanta although Dima and Alex are also very sharp and had a lot of good ideas.\n\nI won't go into a long list right now but I will mention this. Very shortly I will be branching Quanta in KDE CVS for the KDE 2 branch and the head branch will be KDE 3. I'm just working on my development environment now. Here are two very obvious things that will change with KDE 3.\n1) WYSIWYG - at least to a degree for basic HTML or XML with style sheets. Since there are plans to link to DTDs this will mean a lot of work enabling a visual design mode that honors the author's tagging intentions. There will be modes for text edit, visual edit and preview.\n2) Data access - because this is made easier in QT 3 it seems logical to incorporate data management for web work into your web design tool.\n\nI would like to state that I am a professional web developer so I approach Quanta from the standpoint that I need the most productive tool for my business and the best tool at any cost. I remain committed that cost shall stay free because I want web teams to be able to grow around Quanta. My vision is that it will be the best editor for web work. I am going to leave it at that and post the details after we assemble the team as soon as we can.\n\nAnyone interested in getting involved with development, documentation, support or other areas of help should feel free to contact me. If you have requests or questions please hold them until we have our on-line facilities up to manage them.\n\nThanks to all the people who have made Quanta the most popular web development tool on KDE!\n\nEric Laffoon\nMember of the Quanta team"
    author: "Eric Laffoon"
  - subject: "Re: Quanta"
    date: 2001-10-08
    body: "Very good news ! I hope a good success for all your wishes !"
    author: "Alain"
  - subject: "Re: Quanta"
    date: 2001-10-08
    body: "I wish you very good luck.\nI use Quanta at my job, and it rocks (but I still think it should be faster, use less resources).\nAll my work in php is made easier thanks to quanta, so I have to thank you and everbody that helps quanta being the best HTML editor for Linux (with some effort soon it can be the best editor for all systems beating homesite).\n\nI only wish someone develops a easy-to-use-as-quanta image editor for KDE. Krayon simply is pre-alpha and seems to not being developed, and gimp misses the point on easy to use, simply dosen't have some features (as line, elipse, etc and a good interface) that makes it hard to use.\nMaybe next year I can start learning C++ so I can help quanta and other projects."
    author: "Iuri Fiedoruk"
  - subject: "Re: Quanta"
    date: 2001-10-08
    body: "i wish u good luck,too!\na few weeked ago, i was teaching php/mysql in an 18 student class. most of them were using quanta+ for coding (myself too: with beamer and the very candy aqua theming ;-) )\n\nsome bugs are frustrating but its the best tool under linux! thanx.\n\nbtw: isnt there a initiative to make kdevelop be an very handy php coding tool?\ni have read an short note on this...\n\n-gunnar"
    author: "gunnar"
  - subject: "Re: Quanta"
    date: 2001-10-09
    body: "yeah, kdevelop HEAD aka 3.0 aka Gideon, will have support for all languages through plugins\n\nit won't be released with kde 3.0 probably tho, it isn't ready :/\nmaybe kde 3.0.1 or kde 3.0.2 or kde 3.1"
    author: "asasd"
  - subject: "Re: Quanta"
    date: 2001-10-10
    body: "As a web developer, I would have to say that Quanta+ has been a most useful tool.  I was originally an Emacs user, then I went to Nedit because I wanted more RAM.  After realizing that 30+ NEdit windows is out of the question (and that I needed project management) I went searching and found Quanta+.  Despite the number of times I might complain to my wife about it crashing on me I think it's the best tool I've ever used for web development.  (It doesn't crash that often at all really, but every now and then... :) ).  I would like to thank Eric for keeping it GPLed.  The only thing in my wish list for Quanta+ at this point is CVS integrated into the project interface.\n\nOther than that it's a great app, and all those web developers out there who are fond of PHP, this is the app for you.  :)"
    author: "Robert Charbonneau"
  - subject: "Re: Quanta"
    date: 2001-10-10
    body: "i have disabled the projectmanagment - it was (is) too slow.\nthe webpage which i maintain has arrount 2000 files. when adding a new file, it took about 25 sec (400k2, amd) to work again.\nstarting quanta took araouns 1,5 minutes.\n\nprobably its because the xml data.\n\n-> so quanta cant handle big projects this way.\n\nbut its very nice and still my default programming enviroment fpr php-\n-gunnar"
    author: "gunnar"
  - subject: "Re: Quanta"
    date: 2001-10-10
    body: "i have disabled the projectmanagment - it was (is) too slow.\nthe webpage which i maintain has arrount 2000 files. when adding a new file, it took about 25 sec (400k2, amd) to work again.\nstarting quanta took araouns 1,5 minutes.\n\nprobably its because the xml data.\n\n-> so quanta cant handle big projects this way.\n\nbut its very nice and still my default programming enviroment for php.\n-gunnar"
    author: "gunnar"
  - subject: "Small feature suggestion"
    date: 2001-10-07
    body: "Would it be possible to have a sort of 'theme server'? I dont know a great deal about KDE's internals, however if you start up an app as yourself, and then start an app as root, root will usually just have the default theme seeing as most sane people dont use KDE as root, which looks quite jarring. Couldnt one of the KDE session management daemons export some theme info (heck, why not just have an environment variable that points to a KDE theming directory, which all programs automatically load or or something?) so that after you start a session on a given X server, any Qt/KDE apps that attach to that X server will use your theme settings.\n\nAlso, one small thing that I notice a lot is that under KHTML 2.1, vBulletins (which I use a lot) display fine and yet in KHTML 2.2 all of a sudden the tables get mangled (that and a lot of textarea input fields with lots of HTML stuff in them by default have stopped working all of a sudden). Is it a problem with vB or KHTML?"
    author: "Somebody"
  - subject: "Re: Small feature suggestion"
    date: 2001-10-07
    body: "I'm not sure if I understood you. Do you want this theme server to \"export\" settings so if you logged in as a different user (for example root), they'd have the same look?\n\nThis is IMHO a bad idea. Different people have different taste and wishes and this way one would only force others on multiuser systems.\n\nI'm not sure I understand your comment about X server and sessions either (you do know that X server is machine that DISPLAYS text and X client is the one where application runs on), but the way X works, you'll get widget look that application uses on machine it runs and windows will look how they are set on machine where application user interface is displayed. I don't think there's much you can do about that."
    author: "Marko Samastur"
  - subject: "Re: Small feature suggestion"
    date: 2001-10-07
    body: "What he wants is a way to make all apps running under the same X server look the same.  So if he logs in as \"Joe\" and does \"su root\" and then starts Konqeror as root, he wants it to look the same as all the other Konqueror windows on his desktop.  This would not affect the way Konqueror would look when he was actually logged in as root.  Other people using other X servers would not be affected.\n\nPersonally, I _like_ the fact that all my root windows look different.  It helps me tell them apart.  In fact, I want a yellow/black \"Caution\" theme that I can put on all my root windows :-)"
    author: "not me"
  - subject: "Re: Small feature suggestion"
    date: 2001-10-08
    body: "The simplest way I see to do this (and I would like to see it done as well, as I often run KDE apps via remote-X and would like to force a leaner theme when doing so) would be to have an environment variable ($KDESTYLE?) that, if set, overrides the user's theme choices. startkde could then set this var to match the user's pref on startup, so that all kde apps started by this user would get their theme, and I could set it in my .login for remote access.\n\nMaybe this even already exists :-)"
    author: "Kevin Puetz"
  - subject: "Binaries?"
    date: 2001-10-07
    body: "Is there a chance to get some binaries of 3alpha1?"
    author: "yves"
  - subject: "Re: Binaries?"
    date: 2001-10-07
    body: "Basically, if you're not happy to be compiling this release, you probably shouldn't be using it. It's targeted toward people that want to check that they can port their code to KDE 3 easily, plus the bleeding edge adopters that don't mind if 90% of the desktop doesn't work :)"
    author: "Jon"
  - subject: "Re: Binaries?"
    date: 2001-10-07
    body: "Plus there aren't really any new features worth getting yet."
    author: "not me"
  - subject: "Re: Binaries?"
    date: 2001-10-08
    body: "There are quite some new features. It just seems that nobody wrote a detailed changelog yet."
    author: "Michael H\u00e4ckel"
  - subject: "KOffice"
    date: 2001-10-07
    body: "Will KOffice be released with KDE 3.0 or separetely like kde 2.2 ?"
    author: "Macolu"
  - subject: "Re: KOffice"
    date: 2001-10-07
    body: "No. KOffice will always be released independently of KDE."
    author: "Chris Howells"
  - subject: "KDE for Mac OS X"
    date: 2001-10-07
    body: "Will there be a version for Mac OS X?\nI love my system and I\u00b4d love to have KDE on it."
    author: "Bibel Biber"
  - subject: "Re: KDE for Mac OS X"
    date: 2002-01-23
    body: "KDE on OS X would be great."
    author: "Jason Zimdars"
  - subject: "Re: KDE for Mac OS X"
    date: 2004-08-10
    body: "Here is the link\n\nhttp://ranger.befunk.com/blog/archives/000072.html"
    author: "NoPCZone"
  - subject: "OT: New KDE-Themes Site"
    date: 2001-10-07
    body: "Until t.o is up again: www.kde-look.org has a number of native KDE themes + gtk themes, icewm styles and noatun skins."
    author: "Anonymous"
  - subject: "Re: OT: New KDE-Themes Site"
    date: 2001-10-07
    body: "Neat!  I like it much more than I ever liked themes.org (who thought up that old interface?  Deranged monkeys?)"
    author: "not me"
  - subject: "Re: OT: New KDE-Themes Site"
    date: 2001-10-07
    body: "Heh - an hour or so ago, I just submitted an article to the dot about the site - nice place.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: OT: New KDE-Themes Site"
    date: 2001-10-07
    body: "Thanks for informing about KDE-look.org, I have uploaded my Win2k icon theme."
    author: "Asif Ali Rizwaan"
  - subject: "KDE 3.x should not contain weird 2.x things"
    date: 2001-10-07
    body: "Here are the weird things KDE 2.x has:\n\n1. Theme Manager: Everything is scattered, It would have been nice to see all KDEGLOBALS can be used in kthememgr. Like fonts, widget styles, icons, sounds, mouse cursors, kicker size, number of kicker applets, etc., menu appearance, konqueror profiles, KWord profiles etc (to emulate corel wordperfect, msword, staroffice, etc). So Integrate everything themeable into the theme manager, much like KDE 1.1.2's.\n\n2. Help: KDE 2.x is the most helpless system I have ever thought of ;). KDE 1.1.2 was the most helpful. Most applications (even basic) do not have updated help. The \"What's this\" has no use/importance in KDE 2.2's many base applications :(\n\n3. System & network configuration: Even though SuSE has many nice System and Network configuration modules integrated with KControl but I wonder, why KDE can't have them or not try to get them in KDE? why can't KDE behave as it is the integrated part of the Linux/Unix OS and not just an optional component? I just can't set my host name using the KControl :( (what to say about Sound, X, and other configuration) and KDE touts as the best DE of Unix/Linux! Be responsible and control every aspect of the OS, and be in power KDE!!! Stop saying \"it's not my job, it's the distro's\"!\n\n4. Speed: I remember how I was proud of KDE 1.1.2 when it surpasses Windows 9x speed for applications etc., I used to see applications appear as soon as I release the mouse button after clicking on the icon/link. But now I can go make a coffee, drink it and come back to see that the application is still loading ;)\n\n5. Customizing: I just don't want/use 40% of KDE apps. I am forced to download many unwanted packages/apps since it is tightly integrated into KDE's _HUGE_ packages :( if it tries to break those huge package into individual apps then upgrading and installing customized KDE packages will be great. Have a look at http://dot.kde.org/1001216773/1001253025. \n\n\nI don't like to criticize KDE but... how about constructive criticism? I believe that KDE 3.x will be much better than I could imagine and/or suggest. \n\n\nGod bless KDE and America :)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE 3.x should not contain weird 2.x things"
    date: 2001-10-07
    body: "About the packaging...\n\nOne of the reasons KDE is easier to install than Gnome is because of the monolithic packages... it's certainly one of the things I like about how the KDE source is structured. That said, there is no reason someone can't come along and break the packages up and distribute them themselves.\n\nAbout system configuration...\n\nKDE is a *Unix* desktop, not a *Linux* desktop. Anything in the main source needs to be designed for more than just Linux on i386. I'd love to see a generic system configuration area, but again this is something that specific distributers can work on."
    author: "Jon"
  - subject: "Re: KDE 3.x should not contain weird 2.x things"
    date: 2001-10-08
    body: "KDE is a *Unix* desktop, not a *Linux* desktop. Anything in the main source needs to be designed for more than just Linux on i386. I'd love to see a generic system configuration area, but again this is something that specific distributers can work on.\n\nYes, but why not KDE team? I mean, it dosen't need to be those developers that are working on KDE base, but new ones.\nSee ximian-tools. It's going to be *nix based and will work both on command line and gnome, this can be ported to KDE (I think someone is already doing it).\nThis would be great. Interfaces should integrate this kind of tools to making *nix expercience similar on various system types.\nIMHO"
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE 3.x should not contain weird 2.x things"
    date: 2001-10-09
    body: "\"One of the reasons KDE is easier to install than Gnome is because of the monolithic packages... it's certainly one of the things I like about how the KDE source is structured. That said, there is no reason someone can't come along and break the packages up and distribute them themselves.\"\n\nI definitely second that, gnome has way too may packages. It seems that every .so and tiny app gets its own package, and it looks really daunting.\nHaving bigger packages is also useful for developers because they can get a general idea of what parts of KDE the user has installed (because related libs are in the same package) and avoid forcing him to a dependencies rollercoaster when installing some app. This gives for a better use of the available KDE API resources... which favours everybody.\n\nNow packagers already break the binary packages into smaller bits but they manage not to destroy what I mentioned in the previous paragraph, which is nice."
    author: "Carlos Rodrigues"
  - subject: "Documentation &  \"What's this?\""
    date: 2001-10-08
    body: "[...]\n\n> 2. Help: KDE 2.x is the most helpless system I have ever thought of ;). KDE \n> 1.1.2 was the most helpful. Most applications (even basic) do not have \n> updated help. The \"What's this\" has no use/importance in KDE 2.2's many base \n> applications :(\n\n[...]\n\nI agree completely. I think there are several reasons:\n\n1. The used DTD-XML is unknown to everyone, there is no usable editor for it.\nXML is for programmers not for writers. Adopting it was a mistake. HTML was better.\n\n2. Also the template for the documentation sucked for a long time. Installation as the first entry (It must have changed now...). No newbie could install the program and gett to see the xml documentation.\n\n3. Many apps are so simple that writing newbie documentation is impossible for an intelligent person. You just can't dumb yourself down enough.\n\nPlease:\n\nIf someone can write a tutorial or explain with an example how to add \"what's this\" information to a C++ file (or wherever) and if it does not require any advanced programming skills, I would be willing to add a lot of the missing \"what's this\" things. TZhey are more helpful than the complete documentation which is most often outdated anyways...."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Documentation &  \"What's this?\""
    date: 2001-10-08
    body: "...not really a tutorial:\n\nhttp://doc.trolltech.com/3.0/qwhatsthis.html\n\nseems to be mainly:\n\n//create some widget\nQPushButton *pb=new QPushButton(i18n(\"click me\"),parent);\n//add the what's this help\nQWhatsThis::add(pb,i18n(\"this is a <b>whatsthis</b> help \"));\n\nthat's more or less all ,correct me if I'm wrong\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: Documentation &  \"What's this?\""
    date: 2001-10-09
    body: "IMO, at least the KStdActions should predefine the what's this texts.\nAnd if a programmer don't need them, he or she can redefine them using\nKAction::setWhatsThis(text). If someone wrote all this messages, most of the available actions (i.e. Toolbar buttons and menubar entries) would have a nice description."
    author: "Heiner"
  - subject: "Re: Documentation &  \"What's this?\""
    date: 2001-10-09
    body: "if a project uses qt designer, just select a widget, and fill in 'whats this' in the property editor. happy whats-thissing."
    author: "ik"
  - subject: "KDE for MacOS X/Darwin ?"
    date: 2001-10-07
    body: "I'm a long time Linux user, but I've been using MacOS X for the last month and I must say I'm very happy with it, it's a quite impressive system. I can run XFree on it, and Gnome and most GTK+ apps on top of that, and even QT works well. Now, why not KDE?? QT works fine on XDarwin and so do most pure QT apps, and most of the GNU tools such as compilers and linkers. \n\nFink's (excelent UNIX Package manager for OS X) homepage says:\n\"KDE assumes it can do things with shared libraries that are only possible on ELF systems like Linux, *BSD and Solaris\".\n\nI'd like to ask KDE developers to work on whatever minor changes are required to make KDE apps run on XFree over MacOS X, so that I can use Konqui, KOffice and other tools without having to boot into LinuxPPC. Apple is now the world's largest supplier of Unix-based operating systems, and and by supporting those systems KDE could considerably expand it's market share."
    author: "Pedro Ziviani"
  - subject: "Re: KDE for MacOS X/Darwin ?"
    date: 2001-10-07
    body: "> Apple is now the world's largest supplier of Unix-based operating systems\n\nI highly doubt that, Linux's overall marketshare is as much as the classical MacOS's was (granted, most of Linux's share was in server, while MacOS was in educational, graphics, and prepress). Many MacOS users have not switched to MacOSX, so I highly doubt this.\n\nBut I agree, KDE should work in MacOSX :)"
    author: "asasd"
  - subject: "Re: KDE for MacOS X/Darwin ?"
    date: 2001-10-08
    body: "I also need KDE on Mac OS X, and now!!\nNo, I do not know the A & B of C ..."
    author: "kundan"
  - subject: "Qt/Mac?"
    date: 2001-10-08
    body: "Yep, it would be really interesting to have KDE on MacOS X. But is it possible to do this with Qt/Mac? How X dependent are KDE?"
    author: "Loranga"
  - subject: "Re: Qt/Mac?"
    date: 2001-12-04
    body: "Uhh.  KDE is a Window Manager, it NEEDS X11.  But that is not a problem,  I can run programs that require QT and X using XonX.  Why can't I compile KDE?"
    author: "hal97"
  - subject: "Re: KDE for MacOS X/Darwin ?"
    date: 2001-10-08
    body: "The problem is probably, that there are not that much KDE developers having Mac OS X available out there. If you have some basic C/C++ knowledge, don't hesitate to join :-)\nA good place is kde-devel@kde.org.\nIf Qt is already running, then the problems might be not that big.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: KDE for MacOS X/Darwin ?"
    date: 2002-01-30
    body: "Hear hear!  I echo this cry.\n\nI want to log in to my system as console, then startx and run KDE, just like a Linux box."
    author: "Leo"
  - subject: "Re: KDE for MacOS X/Darwin ?"
    date: 2003-07-31
    body: "Then use Darwin, not MacOS X ;)"
    author: "MacOS X User"
  - subject: "Re: KDE for MacOS X/Darwin ?"
    date: 2005-03-22
    body: "duh, that's what you do if you log in as console and start x....:-)"
    author: "root"
  - subject: "Re: KDE for MacOS X/Darwin ?"
    date: 2003-12-27
    body: "MacOS is fine so far, but the GUI is horrible! Its totaly user inefficent. I was going to install Mandrake PPC but KDE on Darwin is a much better (and potentialy more compatable) idea."
    author: "Delta"
  - subject: "Re: KDE for MacOS X/Darwin ?"
    date: 2004-02-20
    body: "Wow, that's the first time I've heard anyone say that."
    author: "mikeydood"
  - subject: "Re: KDE for MacOS X/Darwin ?"
    date: 2004-07-30
    body: "Yea, I can't say I agree with your opinion of OS X. Panther is probably one of the most effectively structured OS' I've used. I say this because of expose, Finder enhancements to quickly access commonly used directories, etc."
    author: "Adam"
  - subject: "Why KDE 3 for QT 3?"
    date: 2001-10-08
    body: "I've been running KDE2.2 on QT3Beta5 for quite some time... why is KDE3 necessary to run QT3?"
    author: "desau"
  - subject: "Re: Why KDE 3 for QT 3?"
    date: 2001-10-08
    body: "you probably have both qt3 and qt2.2 installed..\n\nI do (installed both using apt)"
    author: "asasd"
  - subject: "Re: Why KDE 3 for QT 3?"
    date: 2001-10-08
    body: "Nope.. just Qt 3."
    author: "desau"
  - subject: "Re: Why KDE 3 for QT 3?"
    date: 2001-10-12
    body: "You have?\n\nCould you say how you did this? I have tried a couple of times and whilst I am no stranger to Linux I never managed to get this combo working.\n\nThanks in advance."
    author: "John Bend"
  - subject: "Re: Why KDE 3 for QT 3?"
    date: 2002-12-27
    body: "So what is your full name ? perhaps we are related !"
    author: "D. DeSau"
  - subject: "Configuration consistency..."
    date: 2001-10-08
    body: "Hello all,\n\nfirst of all let me say that KDE is truly a heck of a DE. But I have a few gripes with the configuration. One example that really annoyed me was the size an image is allowed to have if you care for an icon preview of them. Some time in the past it was deemed to slow if the image was larger than 1 megabyte. This is ok as long as there was a way to change this (I really needed to have previews of larger images). But here comes the catch: This option is not available in the control center. Even worse: There are two places that enforce this limit on preview icon generation with different settings to change. One palce is somewhere in FMSettings of Konqueror and the other is buried somewhere deep in kdelibs (forgot really where, because I have patched the source to get rid of this annoying limit)...\n\nSo my plea to all developers is:\n\n- Look for consistency in the configuration. Don't simply add a new option, look if someone else has done this at another level already and reuse (as with relying on the base classes one should rely on base configuration whereever possible or suitable.\n- Make all UI-configuirations visible inside the control center and add apropiate searchable headings/topics so that you'll find the places to tweak when looking for something like \"image size\" or \"icon generation\". \n\n\nNot being able to change the default made me resort to grep'ing through the source to find the limits and disable them completely. This however is nothing a normal user (who get's his KDE rpm's from a distribution) will do or even could do. I almost reverted back to the previous KDE (2.2.0) in which the limit was limited to one place I found by reading the mailing lists, which is another thing normal users won't/can't resort to.\n\nI'll gladly go over the source and point out all configuration options that are available but don't have an control-center entry. \n\nregards\nKarl G\u00fcnter W\u00fcnsch"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Speed!"
    date: 2001-10-08
    body: "A few days a ago I was still a devoted KDE user. Accidentally my last apt-get upgrade (debian unstable...) screwed up something, making X quit without even an error message. Right now I really have no time to fix things, so I booted Win98 up to check my email. And wow - is it fast! For some time I've been thinking about buying a new computer, my K6 / 200Mhz is simply too slow. Too slow for KDE, and very much too slow for every Linux-available internet browser. As it turned out I guess I'll just switch to Windows - at least until I have the money to buy a *really* fast computer, right now my windows box is feeling more responsive than a brand new Duron running KDE. \n\nPleeaase consider revolutionary speedups! With a 200Mhz computer KDE is practically unuseable right now. To compete with windows my guess is that you have to make KDE in general twice as fast and make program start up four times quicker."
    author: "Renegade"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "The apt-upgrade problem: Comment out the last lines before exit 0 in /etc/X11/Xsessions and add a \"exec startkde\" line just before exit 0. I don't know if this is the correct way to solve the problem, but it works.\n\nFor speed. I'm very happy with the speed of my KDE2. Everything is responsive and looks great. I don't think you should blame the speed problem on the KDE developers. It's probably a X issue."
    author: "Mark N"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "Yeah, X is a serious problem.  What ever happened to those attempts to replace it with something a bit more light-weight?"
    author: "dingodonkey"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "I don't think KDE is so slow.\nOn the other hand, for the price of Win98 you can get a faster CPU and more RAM instead."
    author: "AB"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "I know this is cheezy... but it made a hell of a difference on my thinkpad (a21m, p3 @700mhz). Just lower the bpp for X to 16 or even 8 if you want it faster. X11 is a huge bottleneck in and of itself.\n\nOf course, this won't change how long it takes apps to start. But at least once they're running, the performance is quite a bit better."
    author: "Shamyl Zakariya"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "What computers do you guys have? On my PentiumMMX 233MHz 256MB RAM KDE 2.2.1 is 2 to 4 times slower than Windows ME! Not unusable, but very slow! I don't know if it is because of g++, X or KDE, but what I'm saying is entirely true, and that's the point here: KDE runs slow (on startup I can even go make a coffee and it stills loading when I come back)."
    author: "Helder Correia"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "It is is really, really, rediculously slow, it could well be that\nyour DNS settings are messed up.  This can cause each KDE application\nto timeout for several seconds whislt it tries to look up the name of your machine.\n\nBasically, make sure you have an entry in yoru /etc/hosts file\nfor any ip addresses on your machine - including the loopback address\n(127.0.0.1).\n\neg.  I have,\n\n127.0.0.1       localhost       localhost.noc.u-net.net\n195.102.252.65  ewe.noc.u-net.net\n\nwhere the second IP address is of my ethernet port.\n\nHave fun!\n\naid"
    author: "Adrian Bool"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "Mmm... You should have enough RAM - so that shouldn't be the bottle neck. Once the applications are loaded, can you flip from window to window in a reasonable time? (just press the buttons on the task bar). That should really take no time at all - and be as fast as Windows...\n\nIf the above is OK then it may well be your disk that is not set for optimum performance. Do you have dma turned on (it will be on in Windows if it is available)?\n\nhdparm -tT /dev/hda (if hda is your main disk drive) might be interesting..."
    author: "Corba the Geek"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "I just can't turn DMA on (applies to Linux AND Windows) for some reason. Everytime I try to enable it some of my hardware just stops working (like the IDE Iomega ZIP drive). This must be a bug in my BIOS (very old one) or even in my motherboard circuits, who knows. Anyway, my hardware is about 5 years old and I just don't care about bugs anymore. Hoping I can get money to buy a REAL computer soon.\nBut it's true that I get immediate responses from the Windows desktop, and it often happens getting stucked in KDE. No DMA for both!\n\nP.S.: I now have XFS (SGI filesystem) installed. Some time ago, when I had EXT2, the speed was even lower...I had just 64MB RAM a week ago, and I can't notice any big improvement in KDE now that I have 256MB but launching xterm instantly. By the opposite, my Windows desktop is significantly faster now."
    author: "Helder Correia"
  - subject: "Re: Speed!"
    date: 2001-10-10
    body: "You should notice a big improvement in KDE when upgrading from 64MB to 256MB. If not, something is wrong. Are you using Linux 2.2? What does \"cat /proc/meminfo\" say?"
    author: "Erik"
  - subject: "Re: Speed!"
    date: 2001-10-10
    body: "You should notice a big improvement in KDE when upgrading from 64MB to 256MB. If not, something is wrong. Are you using Linux 2.2? What does \"cat /proc/meminfo\" say?"
    author: "Erik"
  - subject: "Re: Speed!"
    date: 2001-10-10
    body: "You should notice a big improvement in KDE when upgrading from 64MB to 256MB. If not, something is wrong. Are you using Linux 2.2? What does \"cat /proc/meminfo\" say?"
    author: "Erik"
  - subject: "Re: Speed!"
    date: 2001-10-10
    body: "You should notice a big improvement in KDE when upgrading from 64MB to 256MB. If not, something is wrong. Are you using Linux 2.2? What does \"cat /proc/meminfo\" say?"
    author: "Erik"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "What computers do you guys have? On my PentiumMMX 233MHz 256MB RAM KDE 2.2.1 is 2 to 4 times slower than Windows ME! Not unusable, but very slow! I don't know if it is because of g++, X or KDE, but what I'm saying is entirely true, and that's the point here: KDE runs slow (on startup I can even go make a coffee and it stills loading when I come back)."
    author: "Helder Correia"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "what a fast coffee machine ! ;-)\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "ROTFLMAO"
    author: "me"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "Check out <a href=\u00a8http://www.mandrakeuser.org/docs/xwin/xtweak.html\u00a8>this</a> page to optimize your X server a bit.\n\nKDE's problem will only be solved when GCC 3.1 is released. Until then, I recommend you to run a lighter environment, such as IceWM or BlackBox.\n\nFor a web browser, you can run Konqueror, Galeon, Netscape 4 or Mozilla on top of it. But please note that KDE apps will be _slower_ unless you run kdeinit."
    author: "Evandro"
  - subject: "Re: Speed!"
    date: 2001-10-08
    body: "I have a k6-2 500 with 192 RAM, and I think KDE is sooooooooooo slow....\nAnd I am using prelink!\nI don't mean only startup times, but on use of the programs, for example konqueror simply stops responding (all windows of it) when loading a page, I can't even stop it loading. Why? Well, did you saw that kde opens a kio thread for each frame/image, and keeps it open? I think this is a bad way on doing things. Another thing is that it \"remould\" the tables, images, etc sizes - this is comproved to be slower than just doing as opera does.\nThis is two little examples that could be easily improved, and have nothing to do with compiler/etc. \nI think the wole KDE should same tons of things that are just a bad design - maybe developers should start taking a look at this NOOOOOOOOOOOOOOOOOOWWWWWWWWW! (sorry for that, I think it should really be made now on kde 3.0)."
    author: "Iuri Fiedoruk"
  - subject: "Re: Speed!"
    date: 2001-10-09
    body: "interesting. i have a k6-3 500 with 640mb of ram, kernel 2.4.10 (+kernel preempt patch), and kde 2.2.1 (from debian unstable) runs great for me. it takes a little bit (~30 seconds) from kdm login until the desktop (and my session) loads, but that's fine considering that i'm launching ~12 apps in my session.\n\nkde 2.1.x was really slow on that system. i switched back to gnome until 2.2.0, which was fast enough for me again.\n\ni also have kde 2.1.1 on a pentium 3 700 w/512mb of ram, kernel 2.2.19, running debian \"testing\". it's faster than my k6."
    author: "Blue"
  - subject: "Re:"
    date: 2001-10-08
    body: "I have myself a K6/200, not really flying, but fast enough.\nAbout the application startup speed: this is a known problem. E.g. for konsole linking to all libraries takes on my system approx. 1.8 seconds, i.e. 1.8 seconds which *we* can't do anything about. But there is help in sight, some ld.so hacker implemented a faster ld by using so called pre-linking, which will reduce this linking time *very* much ( the last figures I saw where from 0.8 sec to 0.05 sec or something like this). The patches are available for glibc 2.2, hopefully it will be included in glibc 2.3.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "MathML"
    date: 2001-10-08
    body: "MathML (http://www.w3.org/math/)in Konqueror would be awesome!\n\nUnfortunately the spec. is 500+ pages."
    author: "A Wacky Scientist"
  - subject: "Re: MathML"
    date: 2001-10-08
    body: "I remember when MathML first appeared -- I thought 'that sounds like an interesting thing to have'... but it's a bloated monster, unfortunately, and I don't imagine IE ever supporting it. I don't imagine many other browers supporting it either, given that most people who write mathematics will just use TeX.\n\nWhat would you rather type --\n\nx^2\n\nor\n\n<mscripts>x<mc><mc>2</mscripts>\n\n?"
    author: "Jon"
  - subject: "Re: MathML"
    date: 2001-10-08
    body: "1) Mozilla supports it today (if you compile it with the appropriate switch).\n2) There's already a TeX to MathML conversion tool available (search the web) so you can easily write x^2 and have <mrow>x<msup>2</msup></mrow> or whatever pop out the other end.\n\nI'd be surprised if IE isn't forced to support it fairly soon because Mozilla is pulling so far ahead feature-wise (MathML, SVG, tabbed browsing, gestures, <link> support, etc) that upcoming Netscape releases are going to start being serious competition.\n\nSay what you want about Mozilla being bloated and slow - if you have a computer that can support it (which most new computer buyers do these days) it's a kick-ass browser. Unless MS really starts \"innovating\", Mozilla will leave it in the dust by 1.0. That doesn't mean it will be successful user-wise, but even if it gets 10% of the market that will be enough to force MS to keep improving IE - and then everyone wins.\n\n(Sorry if this is offtopic but Mozilla works just as well under KDE as any other DE :) Konq's good too, but IMO still a way behind on standards compliance and features, and for me it felt even slower/bloateder than moz does. YMMV, and probably does.)"
    author: "Stuart Ballard"
  - subject: "Re: MathML"
    date: 2001-10-08
    body: "I just want to say that on my computer, Konq actually runs faster than Mozilla! \n\n(and I have a really old computer. It's so slow that people can get away with telling me that it is the computer that is slow - not KDE.)"
    author: "Renegade"
  - subject: "Konqueror speed and rendering"
    date: 2001-10-09
    body: "I am sorry to say this, but Netscape really is a lot faster than Konqueror. I remember when the first preview versions of KDE 2 started leaking out to the general public; people were boasting about how fast Konqueror was - faster than Netscape!\n\nIn reality, the only time Netscape is slower than Konqueror is when rendering tables. Netscape has to load the full contents of a table in order to render it, while Konqueror renders the parts of the table that it has loaded. In all other cases, Netscape is faster.\n\nFor pages that consist of one large table (such as Slashdot articles), Netscape is ridiculosly slow. It hangs for several seconds before anything shows up, while Konqueror immediately displays the page.\n\nKonqueror, on the other hand, renders pages in a whacky style. Text and images liteally jump around the screen until the page is fully loaded. This is incredibly irritating! Often when browsing on a slow site, the text appears at the top of the page and stays there long enough for me to start reading the text. And then, bang, a picture appears from out of nowhere and makes the text jump several rows down. This makes me totally lose track of the text. Again, incredibly irritating!\n\nI don't know which is worse, having to wait until the page is loaded before it is rendered, or having the page jump up and down before your eyes while it is loading. I am using Konqueror even though it is slower and whacky, mostly because it is so nicely integrated with the rest of KDE, plus it has other nice features that Netscape doesn't have."
    author: "Helbons Travis"
  - subject: "Re: Konqueror speed and rendering"
    date: 2001-10-09
    body: "Well I know which is worse -- having just a blank page... 'for a couple of seconds' you say - you must have broadband because it's 'for a couple of minutes' for me. I like being able to see the information as soon as Konqueror gets it, not have to wait. It doesn't render pages in a 'whacky style' -- it does the best with the partial page it has recieved. \n\nBelieve me, if you had the internet connection *I* had, you'd be thankful for Konqueror."
    author: "Jon"
  - subject: "Re: MathML"
    date: 2001-10-08
    body: "Did I say I didn't like Mozilla? No :)\n\nI prefer Konqueror though -- given that I'm running KDE, there is no point me using a whole new widget set and all the memory that implies, when Konqueror works perfectly well on any pages I use, plus it integrates perfectly with the rest of the desktop.\n\nTabbed browsing is lovely. I love it in Konsole -- I'd like it if it could be made possible in just about every KDE program. Kudos for Mozilla for implementing it, and implementing it well. Sadly, I didn't get to see the Links toolbar (it had got disabled by the time I downloaded a build). Is there a screenshot of it anywhere?\n\nI still don't think IE will support MathML anytime soon... it's far too technical for them -- they can't even get XHTML right :)"
    author: "Jon"
  - subject: "Re: MathML"
    date: 2001-10-09
    body: "MathML isn't meant to be typed. It wants an interface, be it textual, e.g. TeX, or graphical, e.g. MathType (not ported to Linux yet :^( ).\n\nThe fact that MathML is a XML implementations should make easy to start with. Qt now has XML classes. I don't know how useful they are, though. Also font support seems to have improved. This means any Unicode characters should be displayable. This enables an easier display of difficult symbols. I'll test this soon in a very simple GUI for LaTeX based on Qt3&beta; that replaces \\Alpha onscreen by a real alpha etc.\n\nThe recursive nature of the MathML spec allows for a recursive widget implementation. This might be slow, but easy and clear to program.\n\nBTW Is it true Konqueror will be able to display XML with stylesheets in the next version?"
    author: "Jos"
  - subject: "Re: MathML"
    date: 2001-10-08
    body: "people actually use this?"
    author: "dingodonkey"
  - subject: "wtf"
    date: 2001-10-09
    body: "is it just me, or is KDE 3 just as ugly as a 1280x1024 closeup of george bush senior having sex with a donkey?\n\nif it's not just me, well... good to see nothing's changed since KDE 2 then!"
    author: "Normal Adult Vision"
  - subject: "Re: wtf"
    date: 2001-10-09
    body: "kde 3 has no themes yet (maybe now, but not last time i checked) because the\nQT theme engine has been rewritten, and the kde style/theme engine will have to be rewritten/adapted too (and the existing themes will have to be ported to the new engine)\nanyway, i think thats not the highest priority. Lets make it useable first so that application developers can have a look at it ..."
    author: "ik"
  - subject: "qt3 and fonts?"
    date: 2001-10-09
    body: "does anyone know if the godawful font problems are fixed in qt3?\n\nin case you haven't run into these problems... you don't do enough graphic design work. :)\n\nproblem 1: with lots of fonts installed (~2000), kde slows to a crawl. everything is slow. launching apps is <i>slooooooww</i>. god help you if you try to change fonts - the font dialog windows take upwards of two minutes to appear, and often crash all of kde.\n\nproblem 2: no matter what fonts you pick, the wrong ones are used in some places. it seems to pick the first font in the list of available fonts, for e.g. lots of text on webpages. try installing e.g. \"101 puppies sw\" (ftp://ftp.linuxpower.org/fonts/serif-sans-serif/101pup.zip) - you'll get tons of text that's unreadable because of the font\n\nany clues?"
    author: "Blue"
  - subject: "Re: qt3 and fonts?"
    date: 2001-10-09
    body: "Problem 1:\ni've read many places, that the X font renderer is the bottleneck here. sure its qt's fault?\n\nproblem 2:\ni had this problem when i installed lots of (ttf)fonts. the problem was, that the default font was set to helvetica, which for some reason was not available any more. when the default is not available it takes the first font it finds, which obviously would be \"101 puppies sw\" (alphabetical order). before you installed the new font(s) \"Arial\" probably was your first font, which meant that you did not notice the difference. \n\nthis is a konfiguration problem of your KDE/XFT setup and not qt's fault.\nanybody know how to get helvetica to work again?"
    author: "Thilo Bangert"
  - subject: "Re: qt3 and fonts?"
    date: 2001-10-11
    body: "Helvetica is supposedly available only in Type 1.  It comes with Adobe Acrobat Reader 3.x and the AFM files are on Adobe's FTP.\n\nOr, the site: http://www.downloadheaven.co.uk/ appears to have Helvetica TTF.\n\nCommon wisdom would be that this is an illegal copy so I would use the Type 1 version.\n\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: qt3 and fonts?"
    date: 2001-10-09
    body: "ar you using qt3 with or without xaa fonts ?\n(if you do with try without and the other way around)"
    author: "ik"
  - subject: "Re: qt3 and fonts?"
    date: 2001-10-10
    body: "Problem 1: I used to have that problem, and unless I am mistaken, there is no lag time starting up my programs no matter how many fonts I have ~1500.  PIII 500, GeForce DDR w/latest NVidia Driver, XFree86 4.10, KDE CVS 05/10/2001, QT 3.0 beta 6."
    author: "Hesham Hassan"
  - subject: "Re: qt3 and fonts?"
    date: 2001-10-11
    body: "Yes, I think there was a problem with the X Render extension where it would re-parse its config file every time a new program was started.  The new XFree has a better version that caches this info so it doesn't have to be parsed again."
    author: "not me"
---
The KDE Project has just announced the release of KDE 3.0Alpha1,
the inaugural release of the
KDE 3 series.  This release is targeted at developers, though experimental
users might want to check it out (be sure to read the
<a href="http://www.kde.org/kde2-and-kde3.html">instructions</a> for
installing KDE 3 alongside your KDE 2 desktop).  The principal changes from the
<a href="http://dot.kde.org/1000875636/">recently-released</a> KDE 2.2.1 stem
from the switch to Qt 3.  However, that switch does bring with it an impressive
array of feature enhancements, including new database classes, new data-aware
widgets, improved RAD development with a much-enhanced Qt Designer, a new
powerful regular expression class (with full Unicode support),
improved internationalization support (including the ability to mix different
character sets in the same text), bi-directional language support (for languages such as Arabic and Hebrew),
multi-monitor (Xinerama and multi-screen) support, better integration of pure
Qt applications into KDE, and hardware-accelerated alpha blending.  With
the Qt port out of the way, the KDE developers can now focus on the
<a href="http://developer.kde.org/development-versions/kde-3.0-features.html">planned</a>
KDE improvements.  Read the full announcement
<a href="http://www.kde.org/announcements/announce-3.0alpha1.html">here</a>, or go straight to the
<a href="http://master.kde.org/pub/kde/unstable/kde-3.0-alpha1/src/">source</a>
(<a href="ftp://ftp1.sourceforge.net/pub/mirrors/kde//unstable/kde-3.0-alpha1/src/">alternative
link</a>).

<!--break-->
