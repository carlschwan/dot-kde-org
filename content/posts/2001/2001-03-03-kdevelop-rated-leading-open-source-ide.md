---
title: "KDevelop Rated Leading Open Source IDE"
date:    2001-03-03
authors:
  - "shutton"
slug:    kdevelop-rated-leading-open-source-ide
comments:
  - subject: "(X)Emacs and Kdevelop-type fancy stuff"
    date: 2001-03-02
    body: "Kdevelop is undoubtedly a very impressive tool. Still, being part of those guys who grew up with Emacs, I will never give it up for any \"standard paradigm\" IDE, however excellent this IDE may be. \n\nI don't think that I have to explain why, every folk out there who has got a chance to master only 0.5% of Emacs functionnalities, and to \"get it\", in the sense \"undestood what make the Emacs paradigm different from everything else\",  knows why I feel that way. Emacs is my desktop, and Emacs is my IDE.\n\nStill, many Kdevelop functionnalities rock, and I envy some of them. Meanwhile, XEmacs is now being ported to Gtk, which is only the tip of the \"Gui-XEmacs project\" iceberg, since XEmacs's architecture, more importantly, is now more and more adapted to the development of sophisticated graphics extensions. And a Qt port, thanks to the Gtk port, is much easier to do than it was one year ago. To put it another way: it is now possible to embed Kdevelop-type functionnalities, especially graphical ones, in a Qt-based XEmacs. Moreover, one can certainly imagine that this baby would have the capability to embed KDE components in its buffers, including Khtml, among others. For the first time, one would have the possibility to combine the unequaled Emacs Lisp environment paradigm with KDE/Konqueror/Kdevelop/whatever capabilities. A dream come true.\n\nBefore you ask, no, I wouldn't be able to do it myself, unfortunately."
    author: "kalifa"
  - subject: "Re: (X)Emacs and Kdevelop-type fancy stuff"
    date: 2001-03-02
    body: "Instead of this, I'd much rather a Qt prot of Emacs be done as a KPart, or emacs made into an XPart. This could also theoretically be done with VIM. That way, we can simply drop our editor of choice into KDevelop, and use it's cool features."
    author: "Aaron Traas"
  - subject: "Re: (X)Emacs and Kdevelop-type fancy stuff"
    date: 2001-03-02
    body: "This is of course another thing we may look forward to, but this is very different. You're thinking of Emacs the editor, I'm thinking of Emacs the environment. To put is another way, a day in front of my computer is not a KDE session or whatever, it's an Emacs session. Mail, news, development, Matlab session, document creation, web site creation, etc... all those things take place in my Emacs buffers. Still, (X)Emacs's currently limited graphics capability make it a poor or unusable tool for good web browsing, graphics creation and manipulation, graphical development (Kdevelop/Glade/ddd-type, for example), etc... What I was suggesting in my previous post is aimed at answering those limitations, by bringing KDE  capabilities within Emacs."
    author: "kalifa"
  - subject: "Re: (X)Emacs and Kdevelop-type fancy stuff"
    date: 2001-03-02
    body: "Afaik there is a kvim in kdenobeta on CVS"
    author: "msp"
  - subject: "Re: (X)Emacs and Kdevelop-type fancy stuff"
    date: 2001-03-10
    body: "There's also a very complete gvim that may get corba bindings soon."
    author: "Robert"
  - subject: "Re: KDevelop Rated Leading Open Source IDE"
    date: 2001-03-10
    body: "KDevelop is great except the editor blows.  If you could get CORBA, K/X-PARTS bindings around gvim or xemacs, I'm sure a ton of people would use it up then."
    author: "Robert"
  - subject: "Re: KDevelop Rated Leading Open Source IDE"
    date: 2001-03-11
    body: "I don't know that it \"blows\" - it bears a striking resemblance to the Visual Studio editor, but I do agree that it is a weak point. I very much like KDevelop, but found that I could not live without XEmacs.\n\n  Allowing XEmacs to be embedded would be excellent."
    author: "Mike Bedy"
  - subject: "Re: KDevelop Rated Leading Open Source IDE"
    date: 2001-03-11
    body: "Except visual studio does auto-indention, code-completion and that function param insight thing.  The thing is that it would not be hard to actauly write the code that does that, just have a queue that holds the lines(or marks that keep up with editor mutations, when a char is pressed just add 1 to everything greater than that point, or subtract for a delete and some other weird suff) get the C++ lines(from ';','{' or '}' to ';', '{' or '}') changed, and parse them as belonging to a scope object(in a seperate thread of course).  This is were OO programming gets cool, just make a class decleration or a namespace a scope subclass and you can just say something like scope_in->parseline(line), if it's a namespace, class, or anything it will figure it out itself, fill in the stucture.  I once wrote this exact thing in like 2 weeks for Gtk in c++ but deleted it(I use visual studio now on windows), and I've only been programming for like 8 months(I'm 18), so I'm sure someone who knew what they were doing could do it in like a day.  And if your keeping track of marks, then auto-indention is simple(like 2 hours to impliment).  The point is that it takes more code to find out were to draw the auto-completion windows than anything else.  KDE studio is trying to impliment auto-completion but ther doing some weird stuff with XML which seems like it would be alot harder.  Of course a CORBA editor binging would be cooler, then you could just add auto-completion to XEmacs or gvim and get that plus their functionality.  \n\nSorry for my convulated writing, it's like 1 am."
    author: "Robert"
  - subject: "Re: KDevelop Rated Leading Open Source IDE"
    date: 2001-03-11
    body: "KDE Studio has had auto-completion implemented for quite a long time.  It uses ctags for it currently.  In the first incarnation it was a rather convoluted process, but it works nicely now.  We are working on some new features to allow you to configure auto-formatting of your code to your particular style."
    author: "Shawn Gordon"
  - subject: "Re: KDevelop Rated Leading Open Source IDE"
    date: 2001-03-11
    body: "Try to use marks(things that keep up with text mutations), and just keep the indent level and if it's an open or close brace.  Then when a close brace is typed find the first unmatched open brace and insert the indent level of the open brace tabs from the beginning of the line.  I wish I still had the code it worked almost perfectly and with stuff like templates support and namspace auto-completion, and was far faster than KDE studio's implimentation, but I haven't used KDE studio in like a month so I don't know how it is now.  Also a nice easter egg for auto completion is support for glBegin and glEnd, which can be done eisly if auto-indention reads a file that lets it support diferent languages like pascal's begin-end."
    author: "robert"
  - subject: "Re: KDevelop Rated Leading Open Source IDE"
    date: 2001-03-12
    body: "For glBegin glEnd I meant auto-indention not auto-complerion."
    author: "Robert"
  - subject: "Re: KDevelop Rated Leading Open Source IDE"
    date: 2001-03-11
    body: "You don't need any advanced technologies to\ncommunicate with emacs. That's simple stuff.\nThe problem is that emacs is not written to\nbe embedabble, i.e. you don't get correct\nfocus handling without patching around in\nthe emacs sources, which are not exactly easy\nto understand."
    author: "Bernd Gehrmann"
  - subject: "Re: KDevelop Rated Leading Open Source IDE"
    date: 2005-12-12
    body: "Emacs is embedabble via an emacs server.  Thing is, it wants to present all its functionality, which includes its quirks.  Emacs sources are elisp, which isn't so much like C++ or other procedural languages.  People do seem to get hooked on it, who try it, and emacs becomes infinitely customizable for them.  What sets emacs apart for me are all the major modes that people have written for working in almmost any given enviornment.  These modes are also written in elisp, and hence can be tweaked by the elisp savy user.  It is much more than editing functions, and includes a sophisticated and powerful way of manipulating text based information.  The truely devoted never leave emacs because whatever you were going to do, there is a major mode for doing it emacs."
    author: "George Kamp"
---
<p>The illustrious <a href="http://www.drdobbs.com/cpp/"> C++ Users Journal</a> has a comprehensive comparison of Open Source IDEs in its March issue. Although not currently online, the article covers 18 IDEs and then concludes <em>"<a href="http://www.kdevelop.org/">KDevelop</a> is exemplary as an all around open source IDE and is suitable for both commercial and Open Source development..."</em>. The story also mentions <a href="http://www.thekompany.com/projects/kdestudio/">KDE Studio</a> along with VDK Builder, Source-Navigator and Code Crusader/Medic as products that are <em>"almost as good"</em> as KDevelop. The article's author is Ptr Sorfa, SCO's alternate member of the ANSI C++ Committee. Those of you just starting out in KDE/Qt development may be interested in this nice <a href="http://women.kde.org/docs/tut_kdevelop/">KDevelop and Qt Designer tutorial</a> by Anne-Marie Mahfouf. <a href="http://www.thekompany.com/products/ksg/">KDE Studio Gold</a> users may be interested to know that a new beta is out featuring improvements to the debugger as well as enhanced support for non-KDE projects.</p><!--break-->