---
title: "KDE & Companies: Ask Trolltech Anything"
date:    2001-07-08
authors:
  - "pfremy"
slug:    kde-companies-ask-trolltech-anything
comments:
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "For anyone interested in interviews alike; slashdot has an interview with Shawn Gordon from The Kompany. Check slashdot for some of the questions that were asked, and expect the answers there in about a week.\n\nI actually don't have any questions to ask mr Eirik Eng, as I think Trolltech is doing a _great_ job and I honestly don't know what they should be doing better. Well, perhaps they could use a kdockwidget-like solution for their current MDI implementation in the Qt Designer. But that's not really a question either :)"
    author: "Jelmer Feenstra"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "--I interviewed Shawn sometime ago for kdezine. However the progress of that project is going slower than i anticipated  so today i published the interview at http://GUI-Lords.org\n\n\nCraig Black\nICQ 103920729"
    author: "craig"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Yes, what about publishing that interview as the first of the \"KDE Companies\" sery, and letting the TT interview be the second one (since it's not done yet). Philippe ?"
    author: "David Faure"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Read the interview first before making such suggestions. Eek. THAT is real troll material. \n\nNot exacly wrong, or uninteresting. But certainly very,very inflamatory. Plus I would somehow doubt the actual authenticity, as it is somehow not quite Gordon's style of writing."
    author: "Hmmm"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "I have read the interview.\n\nIf you think this is not Shawn's own words, please ask him (I trust Craig on that, but feel free to prove me wrong)."
    author: "David Faure"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "I didn't realize my style of writing was so obvious that an imposter could so easily be found out.  Yes, I did this interview, but it was done around 6 months ago which is why Craig wanted to publish it, it was starting to get stale and the actual publish date of the 'zine was no where in site."
    author: "Shawn Gordon"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "I would think accusing someone of lieing would be a bit more \"inflammatory\" than someone expressing honest opinions like Shawn did. Plus I've seen Hmmm on this forum before and i don't think your him:)\n\n-- \nCraig Black\nICQ 103920729"
    author: "craig"
  - subject: "Craig is a troll"
    date: 2001-07-15
    body: "Got your attention? Good.\n\nYou want REAL troll material? Look up Craig Black's posts on http://www.mandrakeforum.com! For one article he posts the EXACT SAME thing about TWENTY TIMES (set the view level to -1 to see the trolling)!!!"
    author: "Shrek"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Err. Why is QT so good? :)\n\nI don't now of a real question to ask."
    author: "Bluewolf"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Do you have a corporate mission statement, even an unofficial one?  And how do you choose which areas to keep commerical, and which to release more freely?  The embedded aspects of Qt are particularly interesting.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Three questions really.. Thought i'd put a few in the same post, hope no one minds too much :)\n\n1)\ndo you Trolls have plans for any \"other\" products other than Qt or are you content to stay in the toolkit business? \n\n2)\nHow are things financially with the economy being in the state it is? Obviously i'm not looking for a detailed statement - just hoping things are good enough for you to stay and produce Qt (and give it away to us hobbyists!) indefinitely.\n\n3)\nWhat do your feelings towards the \"linux community\" after all the flak Trolltech has gotten over the Qt licensing issue? You were accused of everything from wanting to monopolize the linux desktop to being in bed with microsoft. Even with Qt GPLed, the licensing flames still pop up occationally.\n\nFinally, i'd like to express my thanks to all att Trolltech for releasing what is easilt he worlds toolkit for writing GUI apps. I tried a bit a GTK+ and MFC before discovering Qt - needless to say, i was less than impressed with the first two when compared to Qt."
    author: "Henrik"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "I like QT really. It's cool that there is a free edition for windows now. It is now possible to develop windows freeware with qt and port it to linux easily. What are the next genertion feature do you plan. An platform undependend object-model (like: kparts, bonobo, or OLE ;-)) a data-base access through all systems (MySQL run's under Windows too... and it would be great to access these database with the same applications from all platforms)\nHupps, i thin this is more a wishlist... ;-)\n\nSebastian"
    author: "Sebastian Werner"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "No need to wish.  The upcoming Qt3 has an object model called QCom as well as SQL database support!"
    author: "Justin"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "How does QCom compare to KParts?"
    author: "ac"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Like apples and oranges ;-)\n\nWell, not entirely. KParts is a set of well-defined C++ interfaces for graphical embedding, based on the Qt object model, combined with accessing components through shared libraries. \n\nQCom provides a new set of clean C++ interfaces as object model, independent from QObject, moc and friends. With different goals and different semantics (memory management is different, not MOC (->no signals/slots) and different introspection (and basically optional for components) . It has nothing to do with graphical embedding nor with shared libraries as-is (although QCom components (or plugins, as they call it) are usually contained in shared objects) ."
    author: "anonymous"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Hello Sir,\n\nHere is my question:\n\n1. Why is Trolltech's QT libraries for commercial companies costs so much? ($1500 for a single license according to TT page), while Windows 2000 Pro + Visual Studio costs about $400 less? of course - the price goes down with more licenses are bought, but when a company needs 3-4 licenses - it's still much higher then the Windows Equivalent...\n\nI find this pricing very problematic when I approach commercial companies to write/port applications from Windows to Linux. Most of them simply point me to those prices (and most of them have been looking at QT in Linux and they like it).\n\nThanks,\nHetz"
    author: "Hetz Ben Hamo"
  - subject: "me too"
    date: 2001-07-08
    body: "Good question Hetz!  I would like Trolltech to explain their thoughts and strategy on pricing as well. Just last night, I had trouble getting a small company to consider Qt for Windows+future/cross-platform devel because of the price...\n\n-N."
    author: "Navindra Umanee"
  - subject: "Good?"
    date: 2001-07-08
    body: "I cant se how this becomes a good question at all, $1500 is not much. A decent programmer/developer will cost you close to the same amount of money for a week."
    author: "Morty"
  - subject: "Re: Good?"
    date: 2001-07-08
    body: "Well, personally, I have heard everybody else's theories on this but I have never heard Trolltech's official position.  \n\nI'm no business man, so I don't know if this really adds up or not, and at the same time I really would like to see more people using Qt commercially whether by small companies or shareware authors.  \n\nSo I'd definitely like to hear Trolltech's take on this, and maybe what their actual customers think about the issue. If it's indeed working out for them (as it seems to be!), then great, I'm very happy!  Qt's commercial survival is a very good thing for KDE."
    author: "Navindra Umanee"
  - subject: "Re: Good?"
    date: 2001-07-08
    body: "You are of course right, it's a good question. Hearing Trolltech's official\nposition and resons behind the pricing policy are better than 3. party\ntheories.\n\nI'm just tired of the constant nagging about the horrible price every time there is a QT article on Linux Today , /. etc, when I fail to see the\nproblem :)\n\nIt's only $125/month for a year, with free upgrades, not bad. If ones buisness can't handle the cost, maybe it's time to take a closer look at the buisness plan.\n\nWhat the actual customers think about the pricing is the most important question, and what I would like hear. A good question for Shawn Gordon btw."
    author: "Morty"
  - subject: "Re: Good?"
    date: 2001-07-09
    body: "Here I get a 400$ a month ( fulltime job ). This is also a problem when you want to buy  development tools (Kylix, BlackAdder) and Qt itself you must pay for Qt license several times, a poblem when you writing simple administrative scripts or shareware programs etc. I think QT for Linux should be much cheaper than for Windows and commercial Unices. I know - there is a personal and enterprise version - but enterprise means that you have table and a few other widgets added ( ha, ha, ha !) and personal means 1000$ ! It is a great and cheap ( yes !) clossplatform development environment, but a very expensive GUI library for Linux.  So there is my questions:\n\nAre you plan to diverse your offer ? Will be cheap QT for Linux version released ( with tables, XML, and database access ) ? \n\nGreetings from Eastern Europe"
    author: "Ups"
  - subject: "Re: Good?"
    date: 2001-07-09
    body: "Here I get a 400$ a month ( fulltime job ). This is also a problem when you want to buy  development tools (Kylix, BlackAdder) and Qt itself you must pay for Qt license several times, a poblem when you writing simple administrative scripts or shareware programs etc. I think QT for Linux should be much cheaper than for Windows and commercial Unices. I know - there is a personal and enterprise version - but enterprise means that you have table and a few other widgets added ( ha, ha, ha !) and personal means 1000$ ! It is a great and cheap ( yes !) clossplatform development environment, but a very expensive GUI library for Linux.  So there is my questions:\n\nAre you plan to diverse your offer ? Will be cheap QT for Linux version released ( with tables, XML, and database access ) ? \n\nGreetings from Eastern Europe"
    author: "Ups"
  - subject: "Re: me too"
    date: 2001-07-08
    body: "I actually thought too that the price would be a big problem too. But my experience so far is that doesn't matter too much. However much companies would ask: \"Since we are paying so much more for this than Visual C++ what do we get extra? What is the benefits?\". Most GUI programmers can see the benefits but not Management. But if you manage to explain that it really gives great benefits I don't think the price matters much to them.\n\nI personally think the price is high because they don't have the enourmouse volume that Visual C++ and Builder has. Still the price isn't that bad if your look at the Enterpise versions of Visual C++ and Borland Builder.\n\nFrom working in a small software company my experience is that software that isn't sold in big volumes often cost far far more than Qt costs. And you can't argue that if you lower the price you will sell in larger volume because certain software doesn't have a large market. Most people don't need a professional CAD program, professional cross-platform GUI toolkit etc so no matter what the price is they can never sell in the volume that Office programs sell in."
    author: "Erik Engheim"
  - subject: "Re: me too"
    date: 2001-07-10
    body: "Hi there QT people,\n\nwhat about a reduced license fee for students or shareware programmers. I think there are some people that would like to write software with QT, but they also want to sell the program. As they are new to the market, they don't have any money and they can't calculate if they will have success with their small program.\n\nJust an idea."
    author: "neo"
  - subject: "Re: me too"
    date: 2006-10-21
    body: "The conversation with anyone from management who is the least bit clued in often goes something like:\n\nDev:Hi Mr.Manager I would like to buy buy a copy of QT...its bloody expensive, but its great !!\n\nManager: Yaa..Can you not find some shareware tools to do the same thing...\n\nDev: Yes, but QT is much nicer, looks better, it is more powerful then a locomotive...etc.etc....\n\nManager: Yaaa.. But this consultant says you can do exactly the same thing with   wxWidiget, TK, and GTK for FREE. How come you are not using any of those platforms ?\n\nDev: Welll...we like QT better !! \n\nManager: Yaaa.. All of our software is internal and not even for sale, so this still seems much too expensive.\n\nDev: But we think we can save money in the long run.\n\nManager: What ?!!! Its not even a floating license !!! For that much money !! You are nuts...forget it !!!"
    author: "MK"
  - subject: "Re: me too"
    date: 2006-10-21
    body: "So, if it's for internal, you don't need to buy the license."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-09-25
    body: "Good Question.\n\nI would like to add to it: why does the Microsoft Windows version of Qt state that it requires Microsoft Visual C++ 6? Is this a technological reason or a market driven reason? Perhaps am I missing something here?\n\nDo you have any plans to move toward other compilers for the windows platform? What about gcc and cygwin? I could convince my employer to pay $400 more for Qt instead of VC++/ATL/MFC but not on top of the price of Visual Studio. They say, \"If we have to buy VC anyways, just write it in MFC or ATL.\"\n\n-pos"
    author: "pos"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Hello, \n\nA few questions:\n\n1. What is Trolltechs position on the full C++ standard? Eg do you advocate the use of all aspects of the STL, and if so, are future versions of QT going to be based around the STL?\n\n2. Further to this, are there plans to get the signal/slot mechanism of moc into the C++ standard, or do you feel a preprocessor is a fully satisfactory solution? \n\n3. What are your plans for language bindings? \nATM, we have PyQT, a perl binding I think, and C bindings in progress. Do you have any plans to adopt an \"automatic\" binding approach, ala COMs IDispatch or Corba DII? Also what is your response to the claim that it is hard to wrap C++ in other languages? \n\n4. Is the KDE/Qt divide ever frustrating? When a good technology arrives in KDE, do you sometimes wish to deliver it to all your Qt customers, but find that you must reimplement it, due to licensing or technoloy issues? As an example, database access in Qt/KDE seems to be in a state of flux.\n\n5. QTE has raised some hackles in the X camp. Could you explain why you think it is necessary, given that embedded machines are getting more powerful every day, and it is really no harder to re-engineer an X app, say a QT-x11 or GTK app, for a small display than moving a QT-x11 app to QTE? \n\n6. Displays are getting bigger, and screen elements are getting lost - eg icons must now be produced in multiple sizes. People also want to use things on smaller screens - eg the ipaq. Are there any plans to make Qt pixel independent, like Fresco or Berlin?\n\n7. X11 has proven to be a long lasting solution for the desktop window system. What possible extensions (if any) would be useful to you in creating QT? Or do you feel that a new desktop window system would be a better solution?\n\n8. Given the plethora of toolkits for X11, what initiatives will Trolltech be taking in order to maintain a consistent desktop? Eg formulating a good common theme format, a common widget rendering layer or just trying to ignore other toolkits? \n\nPhew! Anyway, some of these questions may not reflect my actual position - but I tried to think of as many issues as I could ;-) \nSorry if the questions were not meant to be technical ..."
    author: "Robert"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "No need to apologize, these are some great questions! :)"
    author: "KDE User"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Regarding #6.  I have beeen wondering if it would make sence or even be possible to have screen elements be defined as vector graphics rather then raster (i.e. bitmaped).  Then icons and other elements could be scaled cleanly.\n\n\n--adam"
    author: "adam"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "On 3: QObject already allows a certain degree of introspection through QMetaObject (you can call slots, signals and set/query properties dynamically without any dependency on the actual interface) . QCom's QUInterfaceDescription interface is even more powerful by providing not only access to methods and properties but also detailed info for the types being used (for arguments, etc.) .\n\nOn 4: That's already been sorted out. It has been decided to drop kdedb in kdelibs in favour of Qt3's database system.\n\nOn 5: Why waste additional memory if it's not necessary? And besides, there are still devices like the Cassiopeia out there which do not have enough resources to run a X-server, a toolkit and a application. Bundling everything in Qt/E, providing high performance and a nifty interclient messaging system sounds like a good competition to X :) . Moving a Qt/X11 app to Qt/E is just a matter of recompilation. However what needs to be done in either way is to re-design the GUI to fit for small resolutions. Here the Qt designer comes in where you just exchange the .ui file and there you are. It's a round packet you get. No need to re-write all the Xt calls to fit for 320x240 ;)"
    author: "anonymous"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Why does Qt contain two completely different event systems (QEvent & signal/slot)?"
    author: "Amir Michail"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "I think QEvent is for window-system events only (like paint, mouse, keystroke, etc), while signal/slot is for anything under the sun."
    author: "Justin"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Yes, that's true, but why have two completely different mechanisms?  Why not have a more general mechanism that can support all kinds of tasks?"
    author: "Amir Michail"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "I'd say that signals/slots are probably slower than QEvent by a small amount, but every little bit counts when you're processing clicks and keypresses at the toolkit level."
    author: "not me"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "One obvious answer is performance reasons, the other is simply that event handling and signals are actually quite different in purpose.\n\nSignals are used to \"communicate\" between widgets, typically when a widget needs to report about a state change. Events are things which the widget may want to handle itself.\n\nTry figuring out how it would be if QEvents were replaced by signals or if signals were replaced by QEvent. It just won't work."
    author: "Guillaume Laurent"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Well, signals/slots don't have to be used for widgets.  That's the difference I pointed out above.\n\nTake, for example, my project called Psi (it's a Jabber client. see: http://www.affinix.com/~justin/programs/psi/).  The backend classes to handle the XML, networking, and communication to the main program all use signal/slots.  There is no widget in sight.  In fact, the first incarnations of Psi were all console based (requiring Qt of course, because of the signal/slots and QSocket/QXml/QString,etc).  QEvents are window system notifications.  Signals/slots are a really cool way for your classes to interact within your program.\n\n-Justin"
    author: "Justin"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Is it possible to have Trolltech produce a text file of all your classes, widgets of QT3 in such a form that when read from within ADA95 one could automatically make the relevant bindings and type definitions?\n\nregards\nguran"
    author: "guran"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Yes, please make Ada95 bindings for Qt/KDE!\n\ndrool"
    author: "Erik"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "How does one go about getting a job at TT?  What are the qualifications that you desire for programmers and do they need degrees in computer science/engineering?  Do you hire based on reviewing contributions to KDE?\n\nTroy Unrau\n(and no, I'm not looking for a job)"
    author: "Troy Unrau"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Why should a programmer choose Qt and not GTK.\n\nCraig"
    author: "craig"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "use both and you'll know ;)"
    author: "Richard Stevens"
  - subject: "They \"should\" not"
    date: 2001-07-09
    body: "Programmers should just choose whatever they like.\nDispite Borland C++ Builder's \"superiority\", many people still choose M$VC++, for example."
    author: "ac"
  - subject: "Re: They \"should\" not"
    date: 2001-09-28
    body: "maybe a better way to state the question would have been:\n\n\"What advantages does Qt have over GTK+?\"\n\nbut oh well...  in the world of opensource, it's hard not to hear questions asking for an objective answer when the only true answer has to be subjective (ie: which distro is the best?)..."
    author: "gLaNDix"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Its a question i'd like the TT people to answer. I'm sure it will be enlightening and interesting.\n\nCraig"
    author: "craig"
  - subject: "Re: Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-11
    body: "> I'm sure it will be enlightening and interesting\n\n*sniff* .. I smell smoke :)  \n\nHow on earth are they supposed to answer that without pinging someone's pride in the GTK camp and starting a flame war.   \n\nI hope they have the good sense to discretely bypass that question and leave those people who genuinely need to make the decision to work it out for themselves.\n\nJohn"
    author: "John"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "inflammatory, -1."
    author: "me"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "What about voice integration in Qt?"
    author: "annma"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Use both and you'll know ;)"
    author: "Richard Stevens"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "oops, sorry, pressed the wrong link, please ignore my posting..."
    author: "Richard Stevens"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "I'm really curious to know if Trolltech has struck any big deals. I mean with big companies planning to release handhelds, PDAs or \"multimedia terminals\". I heard Nokia were using QT and Opera, is that right? Any other major deals? I'd like to know because I think that's where the real money is, and not from selling commercial licenses to the occasional programmer. Actually I think Trolltech needs the \"big deals\" pretty soon, or 3.x is the last we hear from them. But I'd gladly be proven wrong."
    author: "Matt"
  - subject: "Ericsson"
    date: 2001-07-09
    body: "I think you are confusing Nokia with Ericsson. Ericsson will use Opera for their surfboard/cordless phone. Nokia is using a Mozilla platform for their media terminal."
    author: "claes"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "I have a few questions. I've been using Qt since the 0.9X days, so it's been fun to see it mature and still manage to keep that original \"Qt\" spirit.\n\n1. what inspired TrollTech to start building Qt, or put another way, what led to the forming of TrollTech?\n\n2. what are the ultimate goals for Qt? What do you want to achieve with Qt?"
    author: "Chakie"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Do the Kompany and Trolltech have contacts with each other?\nWhat about the financial issue (does the Kompany pay for using Qt?)\nDoes the Kompany ask for new features?\n\nannma"
    author: "annma"
  - subject: "QCOM questions"
    date: 2001-07-08
    body: "Hello Eirik,\n\nI'm really glad that you put QCOM (a COM \nworkalike) in Qt3. I think this will help\nQt and KDE developers a lot. Howewer, I have \nsome questions on this:\n\n1. By making QCOM part of Qt and putting it under\n   GPL/QPL, don't you think that QCOM will be\n   just one of several component systems, which\n   are all derived from COM and all incompatible? \n   Could you LGPL it and make it independent\n   from Qt such that QCOM can be used \n   by everybody, removing the need for other\n   component models?\n\n2. To make QCOM even more useful, it would be \n   great to have\n      - an IDL and an IDL compiler\n      - type libraries\n      - a scripting host a la WSH\n    Are there any plans in this direction?\n\nThanks"
    author: "SW"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "Qt is the best C++ GUI toolkit I have ever used. I am not sure it actually exist anything better. Apart from a number of lesser known I have tried MFC and Borland C++ Builder. Builder was okay but not nearly as good as Qt I think. MFC I really hated.\n\nBeing a very strong Qt advocate I have always suggested Qt to people wanting to do GUI programs and always suggested it in projects I have joined. However dispite Qt's excenlence it is often not choosen. In the last project we did MFC was choosen dispite most of my group said we hated MFC and loved Qt. Our employer dispite not knowing either toolkit very well choose MFC because they saw it as a industry standard and that not enough people knew Qt. \n\nWith this background here is my questions:\n1.What would you say to companies who choose MFC over Qt because most GUI programmer know MFC? Further the company does not care about portability.\n\n(I really like to know because I want to know what I should argue to persvuade them to don't be so stupid).\n\n2.Are you activly trying to make universities and colleges teach Qt GUI programming?\n\n3.When Qt comes to Mac will Linux and Windows users be able to use the Aqua theme?\n\n4.Will there be a non comercial version of Qt for Mac?\n\n5.Does Trolltech have any thoughts on Microsofts .NET and what it will do to Qt?"
    author: "Erik Engheim"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "For number 3, I noticed in the build process of Qt3.0 Beta2 the command line option to g++ \"-DQT_NO_AQUA\" or something to that extent.  I thought about maybe removing the flag, but when I checked the styles folder in the package there was no sign of the Aqua style source code.  So it probably would have ended up a broken compile.\n\nMy guess is Apple would have a fit if Aqua worked on X11 and Windows too."
    author: "Justin"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "I wouldn't be surprised if the aqua style is deliberately only available on Qt/Mac for licensing reasons ;)"
    author: "anonymous"
  - subject: "The name"
    date: 2001-07-08
    body: "Just curious, how did your company arrive at the name \"Trolltech\"?  Do you mind the resulting nickname of \"the trolls\" at all?\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: The name"
    date: 2001-07-11
    body: "I thought of asking this earlier too, so I'll add a vote to this question :)"
    author: "John"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "First of all I think QT is a very classy toolkit, and that the KDE desktop is a clean stylish desktop that is considerably ahead of anything out there so far.  However ...\n\n1) I believe QT and KDE will come in to conflict at some future point. As a commercial enterprise you may want to include stuff in QT, that is better suited at the KDE layer. You seem to wrapping up an awful lot of functionality within QT. A Component    model, XML, Database access. Functionality that I would rather see develop within KDE. Do you see any potential conflicts? \n\n2) If KDE becomes the defacto desktop of Linux and Linux becomes mainstream (as I think both will come to pass), you will effectively become a toll booth to a developers wishing to develop commerical KDE apps. Your terms are \"fairly\" reasonable now but whats to stop you asking for unreasonable commercial licensing terms in the    future?\n\nIf KDE becomes the default you will be able to extract quite punitive terms before development shops will develop with other toolkits. This isn't a problem right now but could it become one?\n\n3) You have several key KDE developers working for you. If KDE becomes the main source of future income for Trolltech will there not be conflicts of interest? Will Trolltech will be able to effectively steer KDE open source developers. Before you mention key kernel developers working for Red hat as a rebut, the development those developers do is not under a dual license that allow Red hat to extract licensing fees for their work. I understand that the dual KDE/QT developers develop stuff for TrollTech for QT but will they also not be keener to influence application frameworks within KDE that favour functionality appearing under QT (and hence the dual license) rather than within KDE.\n\n4) If KDE forked QT to keep the bare minimum of functionality KDE requires from it, what would your reaction be? Could you ask for different license terms from commercial developers who favoured using the KDE version of QT. If you could ask for different terms, would they be more severe to discourage commercial developers from using this branch? \n\nI find myself in a difficult position of admiring the quality of work Trolltech is doing and wishing you could be renumerated for it but also wishing it wasn't at such a key platform chokehold point. Could TrollTech become the effective Microsoft of Linux/KDE? Or is that overblown? If somebody said the same in the early 80's about Microsoft they would have laughed at you if you said Microsoft could unseat IBM. However a developer lock at the toolkit level today may just be as effective now as Microsoft lock on applications at the OS level in the 80's."
    author: "David O'Connell"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-08
    body: "actually, is was not the lock on the OS that is important, but the control of the API. These are actually very good questions"
    author: "ac"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "In regards to your number 4 question: KDE would not be able to charge commercial developers to use a forked Qt because it would be GPL.  The only reason Trolltech can charge is because it's their product."
    author: "Justin"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Justin, does this mean KDE can just copy the current version of QT, and claim it is as their fork? Maybe making minor changes at most?\n\nUsing this KDE fork of QT, would this mean that commercial developers could then build KDE/QT applications without paying Trolltech anything? I am not so sure that this is correct. \n\nI guess that although I believe TrollTech are fairly reasonable now, if KDE becomes as big as I think it might, Trolltech will grow with KDE and will probably become a very different organisation with very different goals. Will a free desktop KDE interests always be congruent with QT.\n\nNo matter how reasonable Trolltech is now, Does the dual licencing protect KDE from future conflicts of interest. It protects open source software but with a large body of commercial developers entering KDE, TrollTech will make decisions for QT which have an impact on KDE. Will these impacts always be favourable to KDE? \n\nIf KDE and QT interests diverge can KDE satisfy commercial developers without TrollTech always demanding a cut. If KDE wants to succeed it needs to be attactive to both commercial and open source software. \n\nFor commercial organisations to commit to a new platform, after the experiences of Microsoft, there need to be assurances that the platform can't be controlled by any one organization in the future. \n\nKDE is my default Linux desktop. I like KDE. I want it to succeed. Trolltech has been exemplary so far. But what if it isn't always so, what will KDEs options be then?"
    author: "David O'Connell"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "No one can fork Qt and suddenly be the owner of it.  Even if modifications are made, it would still be 99% Trolltech's code.  The whole point of the GPL is to prevent people/companies from \"stealing\" code like this.  There's nothing wrong with someone forking the code or taking a chunk, but it must stay GPL.  This means no closed-source development whatsoever.  If the GPL worked like the way you speak, it would be worthless.\n\nI can't say what the future holds.  Trolltech has good intentions, and they do respect the community (consider the Qt GPL move).  Also, some key KDE developers work for Trolltech, so KDE's interests will be present as long as they're around.\n\nFortunately, when KDE/Qt takes the Unix desktop completely over (and I'm sure it will), it will still be a good development platform.  For us open source developers, you can't beat the GPL.  Qt, arguably the world's best GUI toolkit, is just handed out like candy.  And, if you don't like the GPL (ie, you're a commercial developer), Trolltech provides an \"out\" :  Buy a commercial license.  Ask the GNU folks for a commercial license to any of their code and they'd laugh at you.  So Trolltech is open-source and commercial friendly.\n\nThere are only 2 things they could do to harm either side:\n\n1) New Qt versions not GPL\n2) Raise price of commercial license\n\nFor #1, see Section 3 of this KDE Free Qt Foundation document:\nhttp://www.trolltech.com/image/kdefreeqt1.png\n\nIf there is no release of the Qt Free edition for a year, the latest Free edition becomes BSD licensed.  If this ever happened, the KDE team could fork, improve, and even sell commercial licenses of Qt (or LGPL it like the kdelibs).\n\n#2 is a matter of marketing.  If they make the price too high or restrict the license somehow, they'll lose customers.  This is nothing new.\n\nEverybody's in good shape.\n\n-Justin"
    author: "Justin"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "I think there are some subtleties. In an ideal free market if somebody asks for too high a price they lose sales. However software of this nature does not operate in a pure classical free market. Software that forms a platorm has a what economists call a \"networking effect\", similar to the one the Microsoft has gained from in the past. The more people use your software,  the more difficult it is for people to choose an alternative. Once that effect kicks in, in TrollTechs favour, they can extact extract quite punitive terms and people still won't change, as the cost of switching becomes high. The effect hasn't quite kicked in yet for KDE/QT yet, so TrollTech are reasonable for now.\n\nAlso having KDE people work for Trolltech cuts both ways. Yes, they represent KDEs interests to Trolltech, then again, they can also influence KDE to favour Trolltech. For the moment I think these opposing tensions are well balanced. However when Trolltech derive a serious amount of revenue as a result of KDEs success, money might become an pressure that distorts these developers choices. Functionality that might be better placed within the LGPL'd kdelibs ends up in QT and under  the dual license where they can charge for it.\n\nI like Trolltech, I like QT but my real loyalty is with KDE. I don't have a problem with companies earning a buck, but TrollTech are in a similar position to Microsoft in the 80s, they are in a controlling postion to influence key developers APIs.\n\nDoes the dual licensing really protect KDEs interests once commercial developers use KDE as a platform? The fact is, I don't know for sure. I want to believe Trolltech will always be benign but \ndoes KDE really have the safeguards in place. Does the QT free edition guarantees work when commercial KDE development becomes the main influence for QT?\n\nTrollTech, Are my fears overblown?"
    author: "David O'Connell"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "Which revenue does Trolltech get from KDE?\nThere are about 10 licenses for theKompany, then there is a Korean closed source office suite which uses KDE.\nBut else ? \nTrolltech doesnt say who its customers are, but currently they are probably not developing for KDE but for Windows, in the future also for OSX, and of course Unix, but not for KDE.\n\nKDE brings them a huge PR, more than a small Norwegian company could afford.\n\nYour questions depends on many uncertainties, the main one being the end of Microsofts domination on the desktop, which currently is simply wishfull thinking.\n\nWhich of a QT decision are you thinking about that could hurt KDE and why should they do this?"
    author: "ac"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-11
    body: "I don't believe any TrollTech decision so far has been harmful to KDE. That is not my point.  I wonder about a future when Linux becomes mainstream and KDE becomes Linux default desktop. I think KDE should plan for success. I think the dependency of KDE on Trolltech may then become a problem. \n\nYou are right, KDE for the moment is publicity for Trolltech. However, it is a future jackpot if Linux/KDE become mainstream. That could be one very big pile of money for TrollTech as hordes of  commercial developers port to the new platform.\n\nI wonder is the first inklings of a problem starting to emerge. How will KDE-DB develop along with the DB functionality in QT?  How will the future QT component model and KParts develop. Will they be mutually exclusive or will they fit somehow together. \n\nIf KDE starts to alway choose QT functionality over their own,  That would indicate the  start of a problem. \n\nAll healthy open source projects (should?) have conflicts. If KDE and QT never seem to  have a conflict I would worry. I guess the ultimate test would be for KDE to fork QT. I wonder what TrollTechs reaction would be then?"
    author: "David O'Connell"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-11
    body: "If KDE forked Qt it would have to stay GPL.  This would mean no closed-source development on KDE whatsoever.  Definitely not a road KDE wants to take.  The only way KDE could \"take over\" Qt would be if Trolltech stops producing the GPL version for 1 year.  Didn't you read my post above?"
    author: "Justin"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-12
    body: "Justin, Yes I did read it. Its just suddenly clicked!  I have only now realized  why KDE cannot or won't fork QT. One of the strengths of open source is the right to fork and although KDE could do it, I can see now why it never would. KDE would have no commercial developers and its fork would wither. In effect the the right to fork is useless in this case. KDE could never break from Trolltech even if they wanted to. A fundamental requirement of open source has been subverted by its own GPL. I find that worrying. \n\nKDE forking QT was one of several points I made though. I don't think the other points have been effectively addressed by anyone. \n\nAll anybody seems to say is that Trolltech has been fine so far, so why worry. I am just saying I would feel more comfortable with guarantees. \n\nI know Trolltech have probably gone further than many a company to accomadate open source, but I don't think the guarantees will be effective if KDE takes off.\n\nA booming KDE will mean revenue for the Trolltech, which will mean constant development for QT (sounds good, I know) but will probably mean the BSD clause in their guarantee will never happen. A rapidly developing and well funded QT  will start to encroach into KDE eventually controlling it. My prediction is that it will, but that this is currently sometime off. Maybe up to 5 years away. KDE could face a very different Trolltech then and it would be too late to do anything about it. TrollTech will at some point become a toll gate for commercial developers and be able to dictate their terms. Just like Microsoft! I know its not obvious now, but neither was the extent of Microsoft hold on software back in the 1980s. I am not sure MS even realized at that time, and I don't think people can see the strength of Trolltech hand now."
    author: "David O'Connell"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-09-25
    body: "If it really became necessary (which probably won't happen, I trust TT not to become M$ 2.0), we could still write a mostly compatible replacement for Qt - it's actually been done before (remember Harmony?).\n\nLots of work, but possible."
    author: "bero"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-09-25
    body: "I am not so sure. Eirik Eng comment in his just released interview today mentioned they could \"outrun\" any fork or replacement. It would take the best part of a year (probably more) to rewrite a clone and a willingnes for KDE developers to refuse to use the latest extensions to TT version of QT in the meantime. As core KDE architects work for TT, I am not so sure that would happen anymore. Not now and certainly not a year or two from now when KDE becomes the defacto desktop of linux and maybe even the defacto desktop fullstop.\n\nI would never trust TT to not become MS version 2.0. They are for sure nice guys now but what happens when they are large concern or bought over by a large concern and have armies of business \"analysts \" and product managers making decisions. Those people won't give a damn about how QT came to prominance via KDE. They won't feel they own KDE anything. They will see KDE as an opportunity to \"leverage\" QT.\n\nQT may indeed be GPL but it will be subject to a TT commercial interests rather than open source communities interests. TT will take QT to where they need to go and may damage KDE or even end up controlling KDE as a side effect.\n\nThey may be nice now, indeed Microsoft looked nice guys and a way out from IBM back in the 80s."
    author: "David O'Connell"
  - subject: "Company info"
    date: 2001-07-08
    body: "Hi, \n\nWho owns TrollTech A/S?\n\nMats, a.k.a Loranga"
    author: "Loranga"
  - subject: "Re: Qt and Swing/Java"
    date: 2001-07-08
    body: "Hi,\n\nwill there be a Qt-style pluggable look and feel for Swing/Java?\n\nWe are programming Java applications for use under Linux and since KDE/Qt seems to become the standard look and feel for the user (at least in our part of the world :-), it would be great to have the same look and feel with our Java applications.\n\nI don't mean a Java binding for Qt. I'd like to continue to use normal Swing applications and just change their look and feel which is possible with Swing.\n\nBest wishes\n\nMichael"
    author: "Michael Gengenbach"
  - subject: "Re: Qt and Swing/Java"
    date: 2001-07-09
    body: "That question should probably be asked to Sun"
    author: "KI"
  - subject: "Re: Qt and Swing/Java"
    date: 2001-07-09
    body: "Actually, this is a good question since TrollTech recently announced a Qt port of the AWT for use in embedded devices with Qt/Embedded.\n\nDoing a Swing Look and Feel for Java is very tricky with the theming abilities of Qt.  I tried this(writing a qt look and feel for swing) once and without a Qt port of the awt, it isn't really feasible (at least I didn't find any good solutions to the problems I hit).\n\nWith this Qt AWT port, it should be very doable though.  It would be very interesting to know if Trolltech had any further plans for it on the desktop.  I doubt Sun would be interested in it, but something could be worked out with the Blackdown group though.  This would be great news for those that use Java applications with KDE if that ever got worked out."
    author: "Wynn Wilkes"
  - subject: "Re: Qt and Swing/Java"
    date: 2004-11-03
    body: "Try looking at http://javootoo.l2fprod.com/plaf/liquid/index.php.  It will not skin with KDE - you will need to use some other toolkit such as SWT for that to happen, but it is a look in parallel with at least one KDE style (Liquid)."
    author: "GenericProdigy"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Hi there\n\nIs there any chance of porting Qt to VMS?\n\nI'm a happy user of GNU/Linux/KDE at home, but i work as a VMS sysadmin, and i love VMS's amazing stability and ease of use - at least from a sysadmin's perspective. Now - VMS uses pretty much the same X-Windows/Motif/CDE combo that most commercial Unices do; if it was possible to port Qt to Mac and Windows, why can't it be done with VMS? Technically it seems even easier, and the OS licensing is not any worse than these two.\n\nThis port should be even more exciting considering the latest Compaq announcements of their further commitment to the VMS platform and the planned Itanium port. VMS's superior technical features are well-known but it lacks a broad appeal and marketing and exposure that Windows and Linux have; i'm not talking about it becoming a desktop platform, but i haven't ever seen a better server, and i've seen a lot of them (sorry, Unixoids :). Go figure, maybe Compaq will even get into a \"strategic partnership\" with you or something :)"
    author: "Amir E. Aharoni"
  - subject: "What about Win32 version of QT for gcc??"
    date: 2001-07-09
    body: "Why aren't yo planning to port noncommercial version of QT to gcc compiler.\n\nI think , that most pepole who program in Visual Studio uses MFC, and those who not are left to use either Borland/Inprise classes or some GTK+ bindings, and they lack of QT"
    author: "metalman"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "my question:\nin what way do you think .Net (and winXP) are going to affect KDE/Qt/Linux?\nDoes it offer new chances or new threats?"
    author: "farid elyahyaoui"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "> in what way do you think .Net (and winXP) are >going to affect KDE/Qt/Linux?\n\nEspecially in light of Ximian's announcement today, are there any plans at TT to target Qt in a .Net CLR framework (C# compiler, etc.)? Ximian's effort seems to be focused on gtk and gnome. Since their libraries will be LGPL and the compiler will most likely be GPL, I think a lot of their work could be reused. Can you briefly summarize the technical challenges involved in targeting Qt instead of Gtk for the cross platform CLR.\n\nAnand"
    author: "Anand Rangarajan"
  - subject: "What are your plans regarding .NET?"
    date: 2001-07-10
    body: "I would moderate this question up if I could..."
    author: "ac"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "When will typesafe signal/slot connections be implemented ?"
    author: "Ups"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Now, I only get runtime errors, when I type something wrong."
    author: "Ups"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "In which piece of software do you get run-time errors? What do you type wrong?\n\nIf you type bugs you'll get buggy programs. Qt won't help you with that..."
    author: "Dimitri"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Could you explain what you mean by \"typesafe signal/slot connections\"? Actually they are typesafe."
    author: "Dimitri"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "But of course they are not. Write something like that:\nconnect( this, SIGNAL(blablabla), this, SLOT(blebleble) );\nThe moc and gcc will not return any warnings or errors ! You will get only runtime error when this instruction is about to be executed:\n\nQObject::connect: Parentheses expected, signal MyObject::blablabla\nQObject::connect:  (sender name:   'mainwindow')\nQObject::connect:  (receiver name: 'mainwindow')"
    author: "Ups"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "That _is_ typesafety.  Qt does a runtime check to make sure signal / slot functions have matching argument types.  If it didn't perform such checks, the program would spiral into horrible flaming death.  Think about what you're saying.\n\nIt sounds like you want the check to be done at compile time.  Unfortunately, \"signals/slots\" is a concept not known by the compiler, so I'm not sure how it could do a check.  Currently the system uses the functions as strings, so that the Qt library can perform the check."
    author: "Justin"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Was there a lot of in-company controversy over the switch to unmodified GPL, or was it mostly unanimous?\n\nDo you have any relations with the distribution companies?\n\nWhat OS/distro/desktop do the QT developers use?\n\nHow many employees do you have at the moment, and how many in each department?"
    author: "Carbon"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Hello,\n\nI'm an undergraduate computer science student who will graduate in a year and start looking for work.  I'm very interested in KDE and open source software in general, but am uncertain of the job market for writing open source code.\n\nAs a member a company that has been successful in creating high quality, high profile open source software and staying in business, what do you think about the current and future prospects for software developers who want to making a career of writing open source code?"
    author: "Richard Moonick"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Maybe it would be also from interest to speak with KDE-non-friendly companies to hear why the are not using KDE."
    author: "Jo man"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Sure, but what does this have to do with Trolltech? Trolltech's product is Qt - *not* KDE."
    author: "Dimitri"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Why are you trying to make KDE a Ms window like desktop manager ???"
    author: "sebastien boyart"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "a desktop manager? what is a desktop manager?\nIs Ms window a desktop manager? Here nobody\ncares about wind blows, so why do you?\n\nno, not like Windows, better, _much_ better :)"
    author: "grrr.."
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-09-28
    body: "actually, the default desktop has more of a BeOS-ish feel than a Windows feel...  but it's really a great mix of BeOS, MacOS <= 9.x, NeXTSTEP and Windows...\n\nand like one of the other posts mentioned, TT does Qt, not KDE..."
    author: "gLaNDix"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Mmmmh... Trolltech's product is Qt - *not* KDE."
    author: "Dimitri"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "Hi....\nwill be available port of qt-emb for AgendaVR3 PDAs ?? (as target) an what about QPE ??\nthnx a lot"
    author: "Orpheus"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-09
    body: "How is it possible for TrollTech to survive while giving freely Qt to the GPL community ?"
    author: "Chucky / Outmax!"
  - subject: "QT Designer"
    date: 2001-07-09
    body: "(When) Are you going to make a complete, integrated visual RAD development environment out of the QT Designer?"
    author: "Johan"
  - subject: "Re: QT Designer"
    date: 2001-07-12
    body: "I believe they are allready on their way doing that. In Qt Designer for Qt 3.0 I believe you can both edit source code and compile."
    author: "Erik Engheim"
  - subject: "shareware license"
    date: 2001-07-09
    body: "Hi, first : Trolltech is a great company with a great business model. QT simply rocks.\n\nMy question : I`m a student and actually I REALLY need some money. I believe in free software but at the moment I`d\nlike to earn some money with writing software. But there's no shareware license anymore.\n\nWill there be a shareware license again ? What was it ?\nIs there any way how I can sell my qt-software ?\nI can`t pay $1500 for a license (or a car or whatever)\n\nthanks\ntoctoc"
    author: "toctoc"
  - subject: "Re: shareware license"
    date: 2001-07-18
    body: "naybe Trolltech would consider allowing Shareware, but that the proceeds (or a percent of) go towards paying for the license?  \n\nI know that there are companies that take care of the collection of money for shareware and deal with credit card and foreign currency issues.  \n\nJust a thought, plucked from the ether."
    author: "Anonymous"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "Do you have plans to make a library that dinamically load the graphical interface (like libglade for GTK+). It would be much faster to develop an Qt application and it would result in a much clearer code."
    author: "Benoit WALTER"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "Do you have plans to make a library that dinamically load the graphical interface (like libglade for GTK+). It would be much faster to develop an Qt application and it would result in a much clearer code."
    author: "Benoit WALTER"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "You can do this in the upcoming Qt3.  See the QWidgetFactory class."
    author: "Justin"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "Great ! Here is what I found in the trolltech web site :\n\n\"In addition to generating code via uic, Qt Designer now supports the dynamic creation of widgets directly from XML user interface description files (*.ui files) at runtime. This eliminates the need of recompiling your application when the GUI changes, and could be used to enable your customers to do their own customizations. Technically, the feature is provided by a new class, QWidgetFactory in the QResource library.\""
    author: "Benoit WALTER"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "Dear Sirs!\n\nSeveral traditional software companies are worried about Open Source licencing. Many of them feel, that you can't make business by giving the fruit of your work (source code) for free.\n\nFew days ago a KDE friendly software company revealed, that they are selling some part of an open source application (an add-on, to be more specific) and not giving that for free. It looked like few people on KDE mailing lists were unsatisfied for this solution.\n\nMy question is:\n\n1. How can you make profitable software sales with a product, that you have released as Open Source before?\n\n2. If you can't, what kind of a business there is to a company like you?\n\n3. Would you like to comment recent discussion about Open Source licencing and commercial software development.\n\nThank You,\n\nEleknader"
    author: "Eleknader"
  - subject: "When does Qt become too big?"
    date: 2001-07-10
    body: "Is there a point at which you would consider the size of Qt \"too big\"? I understand your reasonings why you don't want to split up the Qt library. But every release it gets bigger and bigger...\n\nThe latest 3.0 beta is almost FIVE times larger than qt-1.45."
    author: "David Johnson"
  - subject: "Re: When does Qt become too big?"
    date: 2001-09-24
    body: "I think this is a really important question, esp when\nyou think about all the new features which are being\nadded into QT (DB, XML, QCOM etc).\n\nAt one time QT may have been a cross platform GUI lib,\nbut I think it's gone way beyond that now. It's a\ncross platform development environment for all types\nof application: GUI client, middleware, anything else\nyou want.\n\nIMHO QT3 is a great time to break the libraries up into\na couple of separate libraries. It's already split up \ninto modules, so even splitting the libraries like that \nwould allow more compact apps.\n\nFor example, if I'm writing a server process, I don't \nneed (nor want) all the GUI stuff. I'll want/need all\nthe networking stuff, collections, strings, DB access,\nXML etc. \n\nSo here's the questions: \n\nAre there any plans to split the massive QT library into \nseveral smaller, more use-specific libraries,\nnow that QT is way more than just a GUI toolkit.\nIf not, can you explain what your thinking is behind this?\n\nQT3 has seen some major new enhancements with the QCom and\nDatabase modules. Are there any other functional areas which\nQT4 might support? I'm not looking for any commitments, \nperhaps just ideas which are being thrown around TT? I'm\nalways interested in hearing what interests the developers.\n\nAnyway, I'd just like to chime in with my thanks. I think\nQT is one of the best toolkits (cross platform or otherwise)\nI've used (and I've used a few!). I think that the price is\nVERY cheap for what you get! On UNIX, you're not surprised \n(but are unhappy!) when you have to pay $6000+ for a table \nand chart widget on their own, so QT is a bargin.\n\nKeep up the excellent work.\nRich"
    author: "Richard Sheldon"
  - subject: "Re: When does Qt become too big?"
    date: 2001-09-25
    body: "I think this is a really important question, esp when\nyou think about all the new features which are being\nadded into QT (DB, XML, QCOM etc).\n\nAt one time QT may have been a cross platform GUI lib,\nbut I think it's gone way beyond that now. It's a\ncross platform development environment for all types\nof application: GUI client, middleware, anything else\nyou want.\n\nIMHO QT3 is a great time to break the libraries up into\na couple of separate libraries. The code is already split\nup into modules, so even splitting the libraries like that \nwould allow more compact apps.\n\nFor example, if I'm writing a server process, I don't \nneed (nor want) all the GUI stuff. I'll want/need all\nthe networking stuff, collections, strings, DB access,\nXML etc. \n\nSo here's the questions: \n\nAre there any plans to split the massive QT library into \nseveral smaller, more use-specific libraries,\nnow that QT is way more than just a GUI toolkit.\nIf not, can you explain what your thinking is behind this?\n\nQT3 has seen some major new enhancements with the QCom and\nDatabase modules. Are there any other functional areas which\nQT4 might support? I'm not looking for any commitments, \nperhaps just ideas which are being thrown around TT? I'm\nalways interested in hearing what interests the developers.\n\nAnyway, I'd just like to chime in with my thanks. I think\nQT is one of the best toolkits (cross platform or otherwise)\nI've used (and I've used a few!). I think that the price is\nVERY cheap for what you get! On UNIX, you're not surprised \n(but are unhappy!) when you have to pay $6000+ for a table \nand chart widget on their own, so QT is a bargin.\n\nKeep up the excellent work.\nRich"
    author: "Richard Sheldon"
  - subject: "QT for the Mac"
    date: 2001-07-10
    body: "Does TrollTech see its role primarily as\n1. Providing the best GUI developement framework in the world.\n2. Providing a cross-platform GUI framework that happens to be a really nice framework too.\n\nI ask this mainly because I've seen mention of a MAC/OS port since I first heard of QT (with the relase of KDE 1.0).  At what point does it become reasonable to ask 'show me the money'.\n\nIn my opinion a good cross-platform toolkit that includes Windows, MacOS and Linux/Unix would do more for the acceptance of Linux desktops (and as a result, for the acceptance of QT-based Windows apps) than just about any other development.\n\nMuch as we like Linux and see it as the natural alternative to Windows, most commercial ISV's are more likely to be asked for Mac ports of their code than Linux ports.\nThat being the case, a QT port of a Windows app to MacOS is more likely to be bankrolled than a Linux port.  And you're likely to see less resistance to the QT professional license fee there too.  And if we get some great Linux apps in the process, well that's great too."
    author: "Rob"
  - subject: "Re: QT for the Mac"
    date: 2001-07-10
    body: "Have you seen this?\n\nhttp://www.trolltech.com/company/announce/mac.html\n\nQt3 will have a Mac version it appears."
    author: "Justin"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "With all the hype about .NET, do you consider porting Qt to C#? The Qt Socket/Slot mechanism is almost identical to the C# \"delegate\" mechanism. Since Qt is crossplatform, it could also benefit from a platform-independent binary format."
    author: "R\u00fcdiger Klaehn"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-08-27
    body: "Yes indeed, \n\n\"With all the hype about .NET, do you consider porting Qt to C#? The Qt Socket/Slot mechanism is almost identical to the C# \"delegate\" mechanism. Since Qt is crossplatform, it could also benefit from a platform-independent binary format.\"\n\n.NET is claiming to be cross-language, cross-platform. I've looked over the specs for C# and the only thing it lacks (when compared to C++) is generic programming, which they plan on implementing in the next version of C#. What is Trolltech's response to C# and .NET?"
    author: "Craig Black 2"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "I am rather seeing KDE turning into a very good Desktop.But at the same time ,it consumes huge amount of resources which makes older machines suck(On my 64mb Ram machine,it didnt go well)\nDo you have any plans to reduce the footprint and \nport kde to other emerging windowing systems like Berlin"
    author: "Rajeev Narayanan"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-12
    body: "It's seems that a popular misconception is that Trolltech makes KDE. It does NOT. It only makes the toolkit that KDE programmers use to make KDE.\n\nIf KDE should be ported to other windowing systems like Berlin Qt does need a rewrite. This shouldn't be too problematic or time consuming but I doubt Trolltech sees it as worht the money and time at the moment since practiacaly nobody use Berlin at the moment (maybe not so strange since from what I know it isn't even finished yet).\n\nIt is probably a higher likelyhood that we will get a KDE that bypasses the X-server and use the display buffer directly, since Qt can allready do this. Only problem with this at the moment is that KDE is dependent on ICE found in X for DCOP and KParts."
    author: "Erik Engheim"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-10
    body: "This is NOT a question for the excellent Trolltech, but instead a recommendation that you add a mailbox for questions to companies in general to help select those which people have questions for.\n\nI would like to ask IBM about their position on KDE vs GNOME and about the satus of the VIAVOICE/KDE integration ( which I am eager to buy )."
    author: "Ross Baker"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-12
    body: "I want to ask Trolltech about the future of ViaVoice integration into Qt. It was annonced and even demonstrated  early this year but seems to be retired now."
    author: "Marc Meier"
  - subject: "Why does the FAQ differ from the GPL?"
    date: 2001-07-14
    body: "In the Qt Free Edition FAQ, it says:\n\n(http://www.trolltech.com/developer/faq/free.html)\n\n>   Using the Free Edition, can I make software for \n>   internal use in my company/organization?\n\n>   The Qt Free Edition is not intended for such use;\n>   it is our policy that when you are using Qt for \n>   free, you should in return contribute to the free\n>   software community. If you cannot do that, you \n>   must get Professional/Enterprise Edition licenses\n>   instead.\n\nYet, the intent of the GPL is pretty clearly that\napplications that are developed in-house need\nnot be distributed externally.\n\nIs it your position that the GPL requires\ncode to be open sourced and distributed even\nif it is used for in-house purposes only?\nOr is the FAQ wrong?"
    author: "Mike"
  - subject: "Position on Free Qt Clones"
    date: 2001-07-14
    body: "Could you please clarify what Troll Tech's\nposition is on independent, third party\nimplementations of the Qt toolkit?\n\nAccording to you, may developers who have \nhad access to Troll Tech's documentation \nparticipate in such a project?  What about\ndevelopers that have had access to the GPL'ed \nsource code for Qt? Do you believe that there \nare \"contamination issues\" similar to what \nSun claims for JDK sources?"
    author: "Mike"
  - subject: "Re: KDE & Companies: Ask Trolltech Anything"
    date: 2001-07-15
    body: "Is Trolltech planning to go public, with their stock, anytime soon?"
    author: "B. Olson"
  - subject: "Qt for Windows"
    date: 2001-07-17
    body: "i was very glad to see Qt for windows made available under a less strict license.  \n\nHopefully it will encourage many people to use the toolkit and maybe some windows demos/ports of Linux programs will encourage people to use Linux.  And it also might allow me to use Linux apps on the universities computer networks which only provides MSWindows.  \n\nI was dissapointed however that Qt for Windows did not have more detailed instruction on how to get it working with gcc/g++, MSVC licenses are way too  expensive if you just want to make some simple freeware.  On a computer i have access to that has MSVC the demonstation code and examples compiled without a hitch, but i dont have a license of my own so im screwed unless i can get g++ working.  \n\nWhere can i get more information?  Please!  Ive thouroughly read throught the docs on the webpages and some of the linux docs in an attempt to get it working under cygwin.  \n\nthanks in advance."
    author: "Alan"
  - subject: "Re: Qt for Windows"
    date: 2001-11-22
    body: "hi ,\ncuold show me how about find and install Qt for window\n thank"
    author: "thanh"
  - subject: "helpyourself :)  [was Re: Qt for Windows]"
    date: 2001-11-23
    body: "http://www.trolltech.com/developer/download/\n\nread the instructions\nmake sure you understand the license\n\nenjoy!"
    author: "wicked monkey tribe"
  - subject: "QT/embedded"
    date: 2001-09-24
    body: "What are your plans about qt embedded.\nDo you try to get some distribution deals, so we could get an iPaq with qt/embedded in the store?"
    author: "anonymous"
  - subject: "Important Point: SOAP/Web Services?!!"
    date: 2001-09-28
    body: "Hi!\n\nMy questions:\n\nWhat is your opinion about .NET and SOAP/Webservices? How important is it for Linux to follow/integrate these ideas?\nDo you plan to add support for SOAP/Web services in future versions of QT?\nWhat do you think about Mono? What about dotGNU? Is there any possiblity for Trolltech to work together with Miguel de Icaza and/or the FSF in these fields?\nOr what *real* (at least equally good) alternative do you see?\n\nRegards\n\nh2o"
    author: "h2o"
  - subject: "Re: Important Point: SOAP/Web Services?!!"
    date: 2005-03-22
    body: "I think Yes.\n\n1. SOAP is language independent (java,perl,C#,php,C++...).\n2. with WSDL you can generate the stub interfaces in all language wich support this.\n3. XML-RPC deprecated\n4. You can attach attachement to the SOAP messages\n\nMono is great, and it's very useful now. (with GTK# extension)"
    author: "Boci"
  - subject: "Fiscal Situation"
    date: 2001-10-04
    body: "How many employees do you have? What is your revenue?"
    author: "g8oz"
---
I am starting a new monthly feature, tentatively dubbed <i>KDE &amp; Companies</i>, which will consist of a series of interviews with KDE-related or KDE-friendly companies. We will start with the company that, through <a href="http://www.trolltech.com/qt/">Qt</a>, is arguably at the root and foundation of KDE; <a href="http://www.trolltech.com/">Trolltech</a>'s CEO Eirik Eng has agreed to answer our questions. Please <a href="http://dot.kde.org/994553595/addPostingForm">submit any queries</a> you may have for Trolltech in the comments below. I leave you one week for this, after which I will choose the best questions and compile an interview for Eirik.  All considered, I expect that it will take a month at the most before we publish the answers.
<!--break-->
