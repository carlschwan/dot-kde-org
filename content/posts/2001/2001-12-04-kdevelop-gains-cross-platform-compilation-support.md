---
title: "KDevelop Gains Cross-Platform Compilation Support"
date:    2001-12-04
authors:
  - "Dre"
slug:    kdevelop-gains-cross-platform-compilation-support
comments:
  - subject: "Which CVS Tag to check it out?"
    date: 2001-12-04
    body: "Which CVS Tag is needed to download this version from CVS? The HEAD branch uses Qt3. But I think this ia a extension to KDE_2_2_2_RELEASE. \n\nIt's a great extension for KDevelop and I want to test it. I hope someone can tell me the correct tag to check it out from cvs."
    author: "Bernd Lachner"
  - subject: "Re: Which CVS Tag to check it out?"
    date: 2001-12-04
    body: "This is the current KDevelop of branch KDE_2_2_BRANCH heading towards to version number 2.1.\n\nAlthough it's on KDE's 2_2 branch KDevelop itself compiles with Qt3/KDE3 as well as Qt2/KDE2 without any necessary switches.\n\nNote that there are also some nice bugfixes, additionally. So it's like a version 2.0.3\n\nHave fun,\nF@lk"
    author: "F@lk"
  - subject: "platforms"
    date: 2001-12-05
    body: "Hi, \njust for information, \nis it planned to add more platforms, like SunOS, IRIX, Windows and HP-UX ?\nThis could really benefit to people developping cross platform tools.\nYou develop everything on one OS, then launch a build for each platform, and have the possibility to execute and debug the code on the target platform by automagically rlogin and exporting the display [OK, this works only on platforms featuring X, but for me it's 4/5 of the work done !]"
    author: "obi"
  - subject: "Re: platforms"
    date: 2001-12-05
    body: "And, how is KDevelop going to compile win32/X binaries in a UNIX environment?\n\nI can see it now: KDevelop grabs a warez copy of windows, creates a bochs partition, installs Cygwin on it, copies the code over, compiles, deletes everything, and hands back the binary. :-)\n\nSeriosuly though, you would at least need some sort of (possibly emulated) Windows installation to at least test the binaries. WINE isn't even close to suitable for that yet, it has a hard enough time running Windows apps you know work (at least, as much as any Windows app can work ;-)"
    author: "Carbon"
  - subject: "Re: platforms"
    date: 2002-01-09
    body: "AFAIK, Borland's free C++ windows compiler works just fine under Wine. With the ability to run windows command-line utils almost as easily as unix ones (that's what Wine gives), you're home, more-or-less. Still you need the header files and libraries, but these can be downloaded for free - get a windows machine, install SDK on it, and copy the directory over to your unix box."
    author: "Kuba Ober"
  - subject: "Re: platforms"
    date: 2002-01-21
    body: "Don't you know better ? \nThere's a GCC toolchain suite for Window$, and it exists too in the form of a cross-compiler toolchain.\nSee http://www.libsdl.org/Xmingw32/ http://www.mingw.org\nSo I hope kdevelop could be use to cross-compile at least SDL projects !"
    author: "Herv\u00e9 PARISSI"
  - subject: "Re: platforms"
    date: 2002-01-21
    body: ">Don't you know better ? \n\nNo, that's why I asked :-) Thanks for the info tho"
    author: "Carbon"
  - subject: "Re: platforms"
    date: 2001-12-05
    body: "The crosscompiling stuff is universal and you can extend target archs and os'es if your combination is missing (basically, add the crosscompiler commands). By default the setup covers irix5,6, solaris, *bsd, linux, hp-ux, aix, arm, sh, mips, ppc, i386, bla foo whatever :) You can develop *any* crossplaform stuff with that as long as you use the gnu-tools (which all kdevelop templates do)\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Debugger Support?"
    date: 2001-12-07
    body: "Does this allow the KDevelop debugger to run GDB in remote mode? That'd be really useful. I already use KDevelop to edit files for an embedded project (dual MIPS R3K, custom OS), but have to use Kdbg or DDD to debug it 'coz Kdevelop's built in debugger lacks support for remote binarys."
    author: "Craig Graham"
  - subject: "Arm"
    date: 2001-12-09
    body: "Gameboy Advance is also based on ARM7TDMI CPU, and is supported on KDevelop."
    author: "Mirza"
  - subject: "Program debugging for Sharp Zaurus applications"
    date: 2002-05-26
    body: "I want to know that in the updated version of KDevelop, is it possible to be used for debugging Sharp Zaurus(QTopia) applications, and even debugging them with using qvfb, the emulator for Sharp Zaurus(QTopia), within KDevelop?"
    author: "Kevin Tse"
---
<a href="http://www.kdevelop.org/">KDevelop</a>'s programmer
extraordinaire <a href="http://www.kde.org/people/ralf.html">Ralf Nolden</a>
has added cross-compilation support to KDevelop.  As his initial motivation
was to support development for the
<a href="http://developer.sharpsec.com/">Zaurus</a>
(as we <a href="http://dot.kde.org/1005086450/">reported</a> earlier this month, <a href="http://www.trolltech.com/">Trolltech</a> and
<a href="http://www.sharp-usa.com/">Sharp</a> have collaborated on this
Linux palmtop), KDevelop can now be used for developing applications
for <a href="http://www.arm.com/">ARM processors</a> (both the Zaurus
and <A HREF="http://www.compaq.com/">Compaq</A>'s <A HREF="http://www.compaq.com/products/iPAQ/">iPaq</A> are based on
the <a href="http://www.arm.com/armtech/StrongARM">StrongARM</a>).
Screenshots of the new KDevelop features are available
<a href="http://www.kde.com/devel/kdev-zaurus/index.php">here</a>, and information about using this new feature to prevail in <a href="http://www.trolltech.com/">TrollTech</a>'s and Sharp's <a href="http://contest.trolltech.com/">developers' contest</a> for the Zaurus, as well as the KDevelop announcement, are below.


<!--break-->
<p>
In addition to the exciting news about KDevelop,
<a href="http://www.trolltech.com/">TrollTech</a> has
<a href="http://www.trolltech.com/company/announce.html?Action=Show&AID=83">announced</a>
an <a href="http://contest.trolltech.com/">international developer contest</a>
for the Zaurus SL-5000D (in case you are wondering, <em>Qtopia</em> is the
new name for the Qt Palmtop Environment).  Submitted applications
can have any license,
and prizes include cash and electronic gadgets (including Zaurii/Zauruses).
To stoke interest in the
contest, TrollTech is providing free preview binary SDKs, which will permit
developers
to create both shareware and commercial applications for the Zaurus (simply
enter the contest for download instructions, the entry form should be available
on the <a href="http://contest.trolltech.com/">contest site</a> tomorrow).
</p>
<p>
<hr width="95%" size="1" noborder>
</p>
<p>DATELINE DECEMBER 3, 2001</p>
<p>FOR IMMEDIATE RELEASE</p>
<h3 align="center">Embedding Made Easy - KDevelop Supports
Cross-compilation</h3>
<p><strong>Leading Open Source C/C++ IDE Adds Cross-Compilation
Support</strong></p>
<p>
The <a href="http://www.kdevelop.org/">KDevelop</a> project today announced
that KDevelop, the leading Open Source C/C++ Integrated Development
Environment, has been enhanced with capabilities for easy cross-compilation
of applications (<a
href="http://www.kde.com/devel/kdev-zaurus/index.php">screenshots</a>).
</p>
<p>
The changes -- including cross-compiler configuration in the
setup dialog, a new <code>--enable-palmtop</code> configure switch, as well
as the ability to create multiple compile configurations --
enable developers to specify different options to compile the same project,
such as the target OS and target architecture. Developers can then dynamically
switch between each configuration and effortlessly compile the same project
code for these different platforms.
</p>
<p>
Given the cross-platform capabilities of KDE/Qt, developers employing these
toolkits will benefit especially from these new enhancements.  For example,
developers can now easily develop applications targeting both the desktop
and handheld devices such as <a href="http://www.sharp-usa.com/">Sharp</a>'s
new <a href="http://developer.sharpsec.com/">Zaurus</a> and 
<a href="http://www.compaq.com/">Compaq</a>'s
<a href="http://www.compaq.com/products/iPAQ/">iPaq</a> PDAs.
</p>
<p>
An updated version of KDevelop incorporating these enhancements will be
released as soon as the new features are regarded stable (probably within the
next two weeks).  In the meantime,
developers can test it by downloading KDevelop from KDE CVS.
</p>
<p>
<table border=0 cellpadding=8 cellspacing=0>
<tr><th colspan=2 align="left">
Press Contacts:
</th></tr>
<tr valign="top"><td align="right" nowrap>
United&nbsp;States:
</td><td nowrap>
Eunice Kim<br />
The Terpin Group<br />
pmckenna@terpin.com<br />
(1) 650 344 4944 ext. 105<br />&nbsp;<br />
Andreas Pour<br />
KDE League, Inc.<br />
pour@kde.org<br />
(1) 917 312 3122
</td></tr>
<tr valign="top"><td align="right" nowrap>
Europe (English and German):
</td><td nowrap>
Ralf Nolden<br />
nolden@kde.org<br />
(49) 2421 502758
</td></tr>
</table>
</p>

