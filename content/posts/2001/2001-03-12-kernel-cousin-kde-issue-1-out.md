---
title: "Kernel Cousin KDE Issue #1 is Out!"
date:    2001-03-12
authors:
  - "Dre"
slug:    kernel-cousin-kde-issue-1-out
comments:
  - subject: "Cool!"
    date: 2001-03-12
    body: "Kudos to Aaron and Zack!  We haven't had such good coverage of the KDE lists since the days of the \"other\" <a href=\"http://developer.kde.org/news/weekly/\">KDN</a>.\n<p>\nI greatly enjoyed this first edition and I'm really looking forward to future editions.\n<p>\nCheers,<br>\nNavin.\n<p>\nPS Sorry for getting the news up on the dot so late -- almost 9 hours after it was first submitted.  Our email notification is busted. :("
    author: "Navindra Umanee"
  - subject: "Re: Kernel Cousin KDE Issue #1 is Out!"
    date: 2001-03-12
    body: "Absolutely fantastic! This made my day!"
    author: "Matt"
  - subject: "Re: Kernel Cousin KDE Issue #1 is Out!"
    date: 2001-03-12
    body: "we love u stilborne!"
    author: "Jason Katz-Brown"
  - subject: "Re: Kernel Cousin KDE Issue #1 is Out!"
    date: 2001-03-12
    body: "That was good. \n\nDO you reacon that there is a chance of setting up a mailed version. or us lazy types that can be arse to check the web site?"
    author: "Mark Hillary"
  - subject: "Re: Kernel Cousin KDE Issue #1 is Out!"
    date: 2001-03-13
    body: "<p>The other kernel cousins have email lists that announce new issues via email and others that deliver the entire issue via email.</p>\n\n<p>Hopefully in the next week or two Zack will be able to set up similar lists for KC KDE.</p>"
    author: "Aaron J. Seigo"
  - subject: "Re: Kernel Cousin KDE Issue #1 is Out!"
    date: 2001-03-13
    body: "<p>OK, i take my last message back. Zack has already set up the mailing lists! (He is Good(tm))</p>\n\n<p>For all mailling lists, visit <A HREF=\"http://kt.zork.net/lists.html\">http://kt.zork.net/lists.html</A>. To sign up for the KDE email delivery list, visit\n<A HREF=\"http://zork.net/mailman/listinfo/kckdedistrib\">\nhttp://zork.net/mailman/listinfo/kckdedistrib</A></p>"
    author: "Aaron J. Seigo"
---
<A HREF="mailto:aseigo@mountlinux.com">Aaron J. Seigo</A> has started a <a href="http://kt.zork.net/kde/">KDE version</a> of the Kernel Cousin series at <A HREF="http://kt.zork.net/">Kernel Traffic</A>.    Given the number of Cousins already active, he asked, <i>"Why not for KDE?"</i>.  The result is one of the first projects to flow from the recently-announced <A HREF="http://master.kde.org/mailman/listinfo/kde-promo">kde-promo</a> mailing list.  Read the <a href="http://kt.zork.net/kde/kde20010310_1.html">first edition</a> of KC KDE for all of 11 topics covering the new KDE printing system, Caldera's work on Samba and kio_smb, AA, KWrite, KDE on HPUX and Solaris, and more. Our congratulations and thanks go to Aaron for this initiative that will greatly benefit the KDE community! For more details and to learn how to get involved, read below.





<!--break-->
<P>Aaron Seigo writes:</P>
<blockquote>
<A HREF="http://kt.zork.net/">Kernel Traffic</A>, headed by Zack Brown, has been publishing weekly summaries of the Linux kernel mailing list for over two years.  The program has been expanded over time with "cousin" publications, covering weekly development summaries for <A HREF="http://www.debian.org/">Debian</A>, <A HREF="http://www.gimp.org/">Gimp</A>, <A HREF="http://www.gnu.org/software/hurd/">GNU Hurd</A>, <A HREF="http://us1.samba.org/samba/samba.html">Samba</A> and <A HREF="http://www.winehq.org/">Wine</A>. These volunteer efforts have helped promote and
keep the developments of these projects in the public eye. </P>
<P>The latest Kernel Cousin, KC KDE, published its <A HREF="http://kt.zork.net/kde/latest.html">inaugural issue</A> today, covering the discussions and events in the KDE mailing lists between February 25 and March 6. A new issue will be published each week and
older issues will be archived on the <A HREF="http://kt.zork.net/kde/index.html">KC KDE</A> page. If you would like to help or have suggestions for making KC KDE better, please email <A HREF="mailto:aseigo@mountlinux.com">Aaron Seigo</A>, the current editor. Enjoy!</EM>.</P>
</blockquote>




