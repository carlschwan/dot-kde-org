---
title: "Development Quickies:  SMB, Java and Printing"
date:    2001-01-25
authors:
  - "Dre"
slug:    development-quickies-smb-java-and-printing
comments:
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-25
    body: "KDE 2.1 beta 2 seems to be out..."
    author: "1"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-25
    body: "Lets hope the RedHat-6.2 RPMS will be there soon:-)\n\n--\nAndreas Joseph Krogh <andreak@nettet.no>"
    author: "Andreas Joseph Krogh"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-25
    body: "Bero told me he was going to create rpms for kde 2.1.....but when I don't know, he usually post rpms a week before everyone does."
    author: "Iuri Fiedoruk"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-25
    body: "SMB slave .. great ... Now we can save from any KDE2 app onto SAMBA shares ...\n\nWill someone PLEASE write a WebDAV KIO Slave??!!\n\nAn experienced KDE developper could do this in half a day."
    author: "guy"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-25
    body: "And another Q... Will SMB shares have BOTH client AND server possibilleties in the Kontrole Center? This would be awfully nice 4 us not-so-kewl-hackers ;) <br><br>/kidcat"
    author: "kidcat"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-26
    body: "No, it won't have. You can use swat for this.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-27
    body: "If I can ever get back in a coding groove, there will be a SMB serving capability (through SAMBA, of course).\n\nMy current idea is to write a small proggie associated to he inode/directory mimetype that shares that directory. Or rather, marks it as shareable until someone runs as root a program to collate all shareables into a smb.conf)\n\nThe logistics of doing this and not breaking everything while remaining even slightly secure are complex, but I think I have most of the design in my head now.\n\nIt will be pretty paranoid (you can only share directories you own, you can not follow symlinks, and a dozen other things)."
    author: "Roberto Alsina"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-25
    body: "Calm down.  The SMB slave needed an upgrade since the old one was a bit flaky.  I'm sure someone will get to a WebDAV slave someday (not that I know how it works or if it's possible, but..)\n\nThey are doing this for free, so I'm grateful for getting anything.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-26
    body: "Well, the mentioned smb ioslave will probably be part of KDE 2.2.\nIn 2.1 there is also a new smb ioslave, which is more compatible and faster than the one from 2.0, it is a wrapper around smbclient. Unfortunately it is read-only, you can't write to the shares, it would have been unacceptable slow if I had implemented it.\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-25
    body: "Does anyone have the same problem of java applets actually open in another window (kjas) while it should actually be embedded in konqueror ? Is this normal ?"
    author: "Pyretic"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-25
    body: "Hmm, not really,\n\nI'm running CVS from Jan 24 - 16:00 CET and things look okay. I actually never had one single applet that was not intended to open seperately open in another window.\n\nCheers,\n\nRichard"
    author: "Richard Stevens"
  - subject: "Re: Development Quickies:  [slightly OT]"
    date: 2001-01-25
    body: "I ve the same problem with mozilla embeded in konq ?\nany hint where this can come from ?\nif i say internal browser, it s external\nif I click external , nothing comes ...\n\narthur"
    author: "arthur dent"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-26
    body: "That will happen under other window managers besides KWin.  Otherwise that should not happen if you\nare running any version after 2.0.1.\n\nIf you are already doing that and still have problems, send me an email with some info about your setup and we'll try to work it out.\n\nThere's also a new Konq + Java HOWTO at www.konqueror.org now.\n\nWynn"
    author: "Wynn Wilkes"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-25
    body: "Talking about developing and developers. Is mosfet still employed by MandrakeSoft to work on KDE? I haven't seen much from mosfet for the last 6-7 months. Only a couple of comments on the mailing lists, but not much code-wise. I really miss his work."
    author: "Joe"
  - subject: "Re: Development Quickies:  SMB, Java and Printing"
    date: 2001-01-25
    body: "Font embedding in Postscript is a VERY useful development. It is very difficult to get something printed nicely from under X in Russian, font metrics are usually spoiled and results are inconsistent.\nSo thanks for good work and keep it up."
    author: "Nikolai Kopanygin"
  - subject: "Don't use JPEG for screenshots"
    date: 2001-01-26
    body: "Screenshots and other non-photographic images aren't suitable for lossy JPEG compression. You can get much better quality at the same compression levels with lossless compressed formats like PNG."
    author: "mbrubeck"
  - subject: "Re: Don't use JPEG for screenshots"
    date: 2001-01-26
    body: "I like PNG, but Netscape 4.x doesn't. If I open a PNG-page it crashes so I prefer JPG."
    author: "Troels Nordfalk"
  - subject: "Re: Don't use JPEG for screenshots"
    date: 2001-01-26
    body: "That's what Konqueror is for, fool"
    author: "Arseholio"
  - subject: "Kword printing is not WYSIWYG"
    date: 2001-01-26
    body: "Hi,\n\nI did not fully understand Lars' report, but\nI hope he is addressing the problem where,\nin Koffice components, the printed output does\nnot match the displayed one very well. For\nexample, the fonts that are used for printing\nare quite different from that fonts that are chosen for the display. Furthermore, even though\nthe display always looks well-formatted, the\nprinted output looks chopped and blocky. The math\nfonts in particular render awfully on paper.\n\nMagnus."
    author: "Magnus Pym"
---
In the last week several KDE developers delivered short reports about the status of their projects.  The first was <A HREF="mailto:wynnw--at--calderasystems.com">Wynn Wilkes</A>, who gave a <A HREF="http://www.kde.com/maillists/show.php?m=380889">report</A> on the status of Java support in Konqueror.  The short version:  all applets which can be run the jdk appletviewer should now work, the security manager (sandbox) is in place, and applet loading via proxy and over SSL is now working (for SSL you need the JSSE (Java Secure Sockets Extension)).  Next came <A HREF="mailto:lars--at--trolltech.com">Lars Knoll</A>, who <A HREF="http://www.kde.com/maillists/show.php?m=381931">reported</A> on his progress on better font handling into the Qt PostScript<SUP>&reg;</SUP> driver.  The improvements concern X displays which have a resolution different from 75dpi, and support for embedded TrueType and Type 1 fonts.  Finally, <A HREF="mailto:wynnw--at--calderasystems.com">Wynn Wilkes</A> (again) <A HREF="http://www.kde.com/maillists/show.php?m=382488">reported</A> that the integration of smblib into the SMB KIO-slave was progressing well.


<!--break-->
