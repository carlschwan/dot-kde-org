---
title: "Antonio Larrosa: Online KDE Talk in Spanish"
date:    2001-07-06
authors:
  - "numanee"
slug:    antonio-larrosa-online-kde-talk-spanish
comments:
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-06
    body: "Does anybody could tell me the different between argentina and spain?\n\nthanks :)"
    author: "Mad"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-06
    body: "Argentina is in south america, and Spain in Europe. Them both uses spanish as main language but in spain the dance is flamenco and argentina is tango ;)\n\n  Now speaking seriously, Argentina and Spain are quite different meanwhile they have the hard link of the language.\n\n Greetings"
    author: "V\u00edctor Romero"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-06
    body: "Oh my god!\nJust a question. Where are you from?\nI guess you are from the USA. Do the American Schools teach about the rest of the World out of America?"
    author: "Juan"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-06
    body: "Ja ja ja!!!\nI've just seen in your email you are from Argentina. Sorry! You were just joking. I thought you were serious. Cachondos mentales estos Argentinos :-)"
    author: "Juan"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "mas cuidado con lo que se dice leches .. que luego pasa lo que pasa ... \n\nEso si, los argentinos son muy cachondos ... :)"
    author: "Daniel"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "Si :-)~~~~"
    author: "Mad"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "No.  Aqui en los Estados Unidos no aprendemos sobre los otro pa\u00edses.  Somos muy stupido.\n\nUhm.  Yeah--if that's not a gross generalization then I've never seen one.  Oh, wait, you must be one of those Europeans--I've heard you're all arrogant.\n\n;-)"
    author: "Scott"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "No quiero ser quisquilloso :-) pero la ortografia correta es \"Somos muy estupidos\".\n\n... Supongo que no has llegado a segundo de Spanglish todavia."
    author: "pirilon"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "Yo supe lo.  Solo fue un typo.  Pero, cierto, mi Espa\u00f1ol, o Spanglish, es malo.  Tambien, yo soy horrible con ortografias en todo lenguas. :-(\n\n-Scott"
    author: "Scott"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "La culpa de todo este galimatias la tiene el cachondo Mad :-)\nQue hable el para poner paz!\nTe aclamamos! juah juah! :-))))))"
    author: "Antonio Gutierrez"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "Off topic: Generally, people from the USA doesn't know very much about the rest of the world.. often, americans mistake Sweden for Switzerland! It's sad, really. ;)\n\n'Course, USA is the centre of the earth.. isn't it? ;)"
    author: "Johnny Andersson"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "hey!!! stop dissing people from the USA!!! they're not that bad... well at least i'm not. but then again, i'm really german but probably gonna live in the US for the rest of my life"
    author: "Rick Kreikebaum"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "Even more off topic:  Generally, people from places other than the US make gross generalizations about Americans based on stereotypes.  It's frustrating, really :-)\n\n'Course, seeing as I'm from the US, I'm just dumb, aren't I?\n\nSorry, I had to get that out of my system.  I'm just tired of being classified as an arrogant idiot just because I'm an American.\n\nSometimes I think Americans are held to a higher standard than the rest of the world when it comes to knowledge of foreign countries.  I mean, do you guys who laugh at us for not knowing Sweden from Switzerland really know all the names of the provinces in China (which probably has about the same population as most of Europe)?  Are we to laugh at you for not knowing?  Do the Chinese laugh at you?  I'm sure they laugh at American tourists who come over and don't know.\n\nSince everyone knows a lot about America, Americans are expected to know a lot about everyone.  It just doesn't work that way, though, Americans can't be experts on everyone.\n\nP.S.  Yes, I *do* know Sweden from Switzerland :-)"
    author: "not me"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-09
    body: "There's a difference between knowing countries and knowing provinces in China.\n\nAnyhow, the Chinese probably consider their provinces state secrets... ;)\n\n(btw, do you yanks manage to confuse Austria and Australia too? what about Ireland and Iceland? what about Chechoslovakia[sp?] and Chechnya?)\n\nAFAIK, the Americans aren't at fault, only the Ignorant Southerners are."
    author: "RK"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-10
    body: "How nice; the rant which I considered making was provided for me.  \n\nI think this can be summed up as this:  Idiots abound everywhere.  I really doubt that a European idiot is much better with the geography of the Americas than an idiot from the US is with European geography.  But actually, it doesn't matter!  No one likes being lumped into negative stereotypes; reguardless of their validity."
    author: "Scott"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-19
    body: "(From a SOUTHERN American. I'm not angry or venting...just bored I guess):\n\n\nI know foreigners don't like Americans, but if you are smart enough to be hanging out with KDE you should be smart enough not to be stereotypical.  I go to Mexico all the time and they say things like \"Americans have better gadgets, but in Mexico we have better theory.\"\n\nIf this is true how come most Mexicans I talk to don't know the names of all their states?  I know most of the states in the US, Canada, AND Mexico!  If you gave me an empty world map I could give you just about every country.  And I don't even have a good memory...and I got out of taking World History(which is REQUIRED in the US--although I didn't take it).  Even if I didn't know the difference between Sweden and Switzerland...what does it matter?  Does one make better cheese than the other?  I think one of them makes knives. (EUROPEANS: I'm being sarcastic)\n\nI'm not saying we are smarter, but I cerantly am saying that I don't think anyone else is.\n\nOh, and I'm not saying Mexicans aren't smart.  I just don't like it when that they say American's aren't."
    author: "Motley Krue"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2003-02-13
    body: "holla how are u? what up? im rebecca 19 white female black hair blueyes 4'8.\noctober 18, 1983. do not smoke drink or do no drugs. colors pink,yellow,lightblue. well so i have to go right to me at\n\nrebecca rager\n3737 brooklyn ave\nbalto md 21225"
    author: "rebecca"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2004-01-05
    body: "yes my school alway does like latin america and mexico! how old are you?"
    author: "screena"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2004-01-08
    body: "Sometimes teachers from the usa teach about different languages. Like in Spanish classes they teach spanish and a little of german. Now does that answer your question? If you email me back I need help with more things to teach the kids in spanish if you know how to. Thank you!\n\n                Amanda"
    author: "Amanda "
  - subject: "Re: What's the difference ?"
    date: 2001-07-07
    body: "I guess you are asking about the time difference between Argentina and Spain.\n\n20:00:00 of Spain is equal to 15:00:00 of Argentina\n\nGreetings ..."
    author: "Daniel Mario Vega"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "Hey.\ni was talking about the time differences..\nsorry for not been so explicit..\nmaybe i forgot to mention.. :/ (yeah..i do)\n\nand i know what argentina is and i know what spain is.. ( but.. wtf(what the f*ck) is USA? :PP )\n\nand if somebody ask you the different between argentina and spain, you could tell them the geographics, politics, religius and etc different between them :P\n\nand sorry for not reply this as soon as i can, but this is as soon as i can.. i was in the real world having great time :)\n\nthanks for the only guy who understand me :)\n\nsee you.\nPD : Bytheway... Tango is dead... Rockandroll is the answer.. (long live to Soda Stereo and Divididos)"
    author: "Mad"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-08
    body: "Me encanta Soda Estereo (y lo que est\u00e1 haciendo ahora cerati), pero los divididos no hacen mucho por mi."
    author: "Mario"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-08
    body: "Todo el mundo sabe que la principal diferencia es que en Espa\u00f1a \"todo va bien\"......"
    author: "infoMAN"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "?"
    author: "? (213.93.205.7)"
  - subject: "Troll"
    date: 2001-07-07
    body: "Oh no not again... this guy also shows up at Gnotices. :("
    author: "dc (213.93.205.7)"
  - subject: "don't be fooled"
    date: 2001-07-08
    body: "Hey Jerk,\n<p>\nFunny how you wrote the original troll and then replied to yourself.  Go play <a href=\"http://www.microsoft.com/billgates/\">elsewhere</a>.\n<p>\nThanks anyway,<br>\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "Just for the sake of argument, let us assume that the above post is not a troll, but in fact a highly composed, very reasonable message, without changing the viewpoint. If this were so, then here's my response :\n\n>its simple to use\nSure, but so is KDE. KDE has loads of documentation, is very configurable so that however you use your system, KDE can handle it gracefully, and finally, it has almost no marketing bull to get in the way of an effective desktop (for instance, app names that are more relevant)\n\n>installs itself without pain\nIt took me all of an hour to install a working Mandrake 7.2 system, and since then the installer has improved dramatically. Also, unlike Windows, much interactive help is included at each step of the install.\n\n>all your hardware works\nAll my hardware works in Linux too. The only hardware I can think of that doesn't work is WinModems and certain printers. Hardware support (except for estoric, obsolete, or ultra-specialized stuff) is much less of an issue now with Open Source Unices then it was a couple years ago.\n\n>you get one cool desktop\nBy cool i assume you mean \"looks cool\". Windows has no transparent menus, no themable window decorations, and no code-based theme support. Also, it doesn't allow you to set button backgrounds for the taskbar, doesn't let you set automatically rotating backgrounds, and also has no support for icon-sets.\n\n>all applications looking alike\nAll applications do mostly look alike in KDE, much work is going on to match the desktop consistency guide, and some work going on to improve the consistency guide itself. A good example is the newly revamped games package, which is much more consistent now then a year ago, thanks to the great new kde-games maintainer\n\n>and so on\nHow about (for example) Linux's \"and so on\". Windows does NOT come with a free web server, sql server, or any other server software. It comes with all of 5-6 games (including hover, which isn't even all that easy to find), whereas most Linux distros come with at least 50, and KDE itself has over 15, with more being added each minor (x.x) revision. Linux distros come with many different editors, and several shells, all of which are much much much more useful then the DOS shell, and (imho) the Windows Explorer. This is even more true when you use a shell embedded into the excellent Konqueror. Linux distros come with compilers and development tools, which alone cost $200+ on Windows systems.\n\nHowever, this isn't my response, since you are in fact a troll, noted by the fact that you don't have anything better to say then \"I'm right, you're wrong\".\n\nBottom line : Go fill up someone else's message board with ****, or at least make some sort of real arguments to defend your point."
    author: "Carbon"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "Well said!"
    author: "dc (213.93.205.7)"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: ">By cool i assume you mean \"looks cool\". Windows has no transparent menus, no themable window decorations, and no code-based theme support. Also, it doesn't allow you to set button backgrounds for the taskbar, doesn't let you set automatically rotating backgrounds, and also has no support for icon-sets.<\n\nWindows has transparent menus (better than KDE's Liquid style), and you can even set percentage of transparency.\nWindowsFX is name of the program which lets you do that.\n\nWindows has better themable window decorations (and many more better designed window decorations than for example KDE). Take a look at http://www.skinz.org and search for WindowsBlinds themes. And not only windows decorations. Themeability of menus is also much better with WindowsBlinds then in any window manager for Linux.\n\nSome apps for windows allow you to set button backgrounds for the taskbar.\n\nThere are some apps for windows which allow you to have rotating backgrounds.\n\nIt is very easy to change icons for Explorer toolbar etc. I can change icons for all windows programs on the fly. There is a program called Restorator which allows you to replace all windows icons from opriginal .exe or .dll file and save it with new icons as new .exe and .dll file. (not only icons, also all pictures). Restorator has another good thing: you can open .exe or .dll file and translate applications directly. Just save your translated .dll or .exe afterwords and you have for example Microsoft Word translated to Spanish.\n\nBut all these things are not free (although above mentioned apps for windows are really cheap). \n\nThere are also applications called shell replacements for windows which replace explorer as default windows shell\n(Talisman, LiteStep, DarkStep, NextStart, Reveal etc.)\n\nThere are also apps for windows (also free) which allow you to have virtual desktops alla virtual desktops for KDE (desktop pager) or even better - Talisman gives you possibility to have 999 virtual desktops.\n\nSo, believe me, there are many possibilities to theme MS Windows, but Windows is always Windows. You know what I mean. There is no real freedom. \n\nYou are consumer in Windows world, and you are person in Linux world.\n\nBest regards,"
    author: "Antialias"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "Precisely what I'm talking about. None of the features are part of Windows, they are produced by outside parties to make up for the fact that Windows has none of these features! KDE (at least 2.2) has these features built-in.\n\nPlus, LiteStep, DarkStep, and NextStart are all built to make Windows look like UNIX variants!"
    author: "Carbon"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "It's funny how you have Windows users trying to be like l33t *nixers by using LiteStep, yet you have KDE users running the Windows 2000 theme/decorations!\n\nHmm, on the subject of window decorations, are we going to see more that put the close button on the left?  I like the look of many of the decorations, but I choose laptop every time just because I want the close button on the left.  Maybe the window decorations should have a button layout configuration someplace?"
    author: "Justin"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-07-07
    body: "It does in KDE2.2beta1 (on Debian sid)"
    author: "robert"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2001-09-13
    body: "it is dumb to compare windows with KDE, all those features are  not part of unix either, they are a SEPARTE PRODUCT , windows is a OS (or tries to be), litestep or windowsblind are dedicated to the desktop look (as kde does), but windowsblinds does a much better work at its task than kde, linux or unix etc does not any of this work by itself, thinking of that then WINDOWS does better job at this because it has some of it integrated on it."
    author: "Arana"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2002-06-24
    body: "In response to comparing Windows to KDE, lets do something else, lets compare the package Windows comes in to the package you get when you buy the powerpack edition  of Mandrake.  Is this fair?  Sure it is.  Windows wants a whole whack of money for a CD with an operating sytem on it and basically one type of user interface, a few games (does hover still come with Windows?) a few simple apps, Notepad, Wordpad, and a bunch of Demos and avis for programs MS would like you to buy.  You pay much less for Mandrake which packs onto three CD's plus you get four other CD's of all kinds of free stuff, actual working apps like Star Office, enough games to keep most individuals amused for a few months minimum.  You also get a number of user interfaces and the entire thing has enough documentation and customization options to please even the fussiest user"
    author: "C_WIZ"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2004-04-22
    body: "yo tengo uno alomno"
    author: "Carmen"
  - subject: "Re: Antonio Larrosa: Online KDE Talk in Spanish"
    date: 2005-09-22
    body: "In fact, Windows comes with a free (in the sense that you don't pay extra for it) web server named IIS. But, if you don't like bullshit, you can download (for free) Apache for Windows. Comparing Windows to KDE is way out of the reasonable logic."
    author: "harcalion"
---
For those amongst you who gr0k spanish, KDE hacker <a href="http://perso.wanadoo.es/antlarr/">Antonio Larrosa Jiménez</a> will be presenting a KDE development talk on IRC this Saturday.  The talk will mostly cover material from Antonio's well-known <a href="http://perso.wanadoo.es/antlarr/tutorial.html">tutorial</a> (<a href="http://perso.wanadoo.es/antlarr/tutorial/">browse online</a>).  If interested, be sure to drop by server <i>irc.irc-hispano.org</i> on channel <i>#linux-prog</i>, Saturday 7th at 20:00 (spanish time). If you need any help with IRC, check the <a href="http://www.irchelp.org/">IRC help site</a> first.
<!--break-->
