---
title: "KDE on Windows?"
date:    2001-05-18
authors:
  - "Dre"
slug:    kde-windows
comments:
  - subject: "Trolltech?"
    date: 2001-05-17
    body: "I suppose that Trolltech would not like the X11 version of Qt ported to Windows, since they have a non-free Windows version already.\n\nThis could get rid of one of their ways to make $$$.\n\nIt would be nice to see KDE running on Windows though. I think Gtk already has a free Windows version, so..."
    author: "AC"
  - subject: "Re: Trolltech?"
    date: 2001-05-17
    body: "Gtk on Windows is hardly functional though."
    author: "ac"
  - subject: "Re: Gtk on Windows is hardly functional though."
    date: 2001-05-18
    body: "Ethereal and GIMP on Windows use the GTK+ library and they work quite well. I wouldn't call that \"hardly functional\"."
    author: "Toastie"
  - subject: "Re: Gtk on Windows is hardly functional though."
    date: 2001-05-19
    body: "Both programs work for me too, to some extent at least. But all GTK widgets just suck very bad on Win32. GTK crashes regularly and the widgets don't even paint themselves correctly most of the time. Edit control is just horrible, and tab control is not much better."
    author: "Kirill"
  - subject: "Re: Trolltech?"
    date: 2001-05-18
    body: "Gtk on windows works allright, DnD is flacky as well as non english, but Gtk 2.0 is beinng built to be fully XP."
    author: "ac"
  - subject: "Re: Trolltech?"
    date: 2001-05-17
    body: "I am not sure that they will mind actually,\nseeing that it would still be GPLd and limited,\ntherefore, to free apps. People working on freeware and OSS projects are probably not the biggest market for them anyhow, and it might just increase visibility of QT in the windows world..."
    author: "A Sad Person"
  - subject: "Re: Trolltech?"
    date: 2001-05-17
    body: "Well, the way I see it, if they wanted a GPL QT on Windows, they would have licensed one already.  They must have reasons for not wanting it, because they have not already done it."
    author: "not me"
  - subject: "Re: Trolltech?"
    date: 2001-05-18
    body: "My guess is that lots of companies buy Qt for in-house development.  If Qt/Windows went GPL, Trolltech might lose out on a lot of sales."
    author: "Justin"
  - subject: "Re: Trolltech?"
    date: 2006-05-13
    body: "The last.fm player is based on Qt AFAIK and they have a Windows version, don't know how they licence Qt for that particular app, but I can tell you that the player itself is released under a BSD licence so it could well be that Qt (the native Windows version, it is a native Win32 app) is GPL'd for Win32 native open source apps..."
    author: "GeoNorth"
  - subject: "Re: Trolltech?"
    date: 2006-05-13
    body: "This discussion is obsolete.  Qt4 is available under the GPL for Linux, Unix, Windows and Mac: \n\nhttp://www.trolltech.com/products/qt/licenses/licensing/opensource\n\n\n\nBTW: There's no such thing as \"being GPL'd for\" a certain group of apps.  Either something is under the GPL or not.  If you use it with the software you write you have to abide by its rules, though (release your stuff under a compatible license), and I suggest you read the GPL and the GPL FAQ before you start writing that code. \n\n"
    author: "cm"
  - subject: "Re: Trolltech?"
    date: 2001-05-18
    body: "Pls note this is not really a port of Qt to windows, it's a port of Qt to Cygwin which provides Unix-like APIs under Windows.\n\nit is not the same as the native version of Qt for Win32 which I'm sure is way more stable and optimized for that platform.\n\nI think TrollTech can relax."
    author: "Jacek"
  - subject: "Re: Trolltech?"
    date: 2001-05-19
    body: "I think it won't be a great idea providing a fully featured KDE system on Windows platforms. The main reason I am against it is that if it will really have a good impact, there are good chances to see many apps ported/using it JUST on Windows. I realize that it will be a GPL'd system, but... who prevents me to use Windows API's together with that product? Example: why isn't FlaskMPEG actually in development for unix platforms? Its source code is GPL'd."
    author: "Massimo Santoro"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-17
    body: "II believe it is generally a good idea to make Free Software attractive for daily use for a the largest possible number of people possible. KDE is doing so by making free Unix environments accessible for people whoes computer expriences are largely GUI/Windows based. Porting KDE to Windows is a logical step in that directon,\nenabling people to have a gentle transition from the platform they grew up with (and of which they are told, it is the only one in existance) to the better alternative. Also it is a great thing to make Konqueror available on this platform as an alternative browser that\n*does* enforce standarts!  So I think this is a good thing!\n\nHowever I believe it is important not to copy Windows on Linux or create an alternative Windows on Windows. There must be no doubt a\ncompletely Free system is the better system!"
    author: "Diedrich Vorberg"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-17
    body: "I'd been thinking for a while that this would be a Good Thing, but in particular having KDE on Windows so that KOffice could be there as well, because I think a lot of Windows users would be receptive to an office suite that doesn't cost a lot (MS, Corel) and doesn't suck (Staroffice).  This project (if brought more up to date, and marketed to some degree) could be used to ween Windows users from the interface they know, and gain KDE more attention.\n\nKudos to the project."
    author: "Joe Theriault"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-17
    body: ">> I'd been thinking for a while that this would be a Good Thing, but in particular having KDE on Windows so that KOffice could be there\n\nAnother aspect that makes *me* happy is the fact that I have been lobbying (sucessfully) to use Qt for commercial software development so that we can have Linux, Mac and Windows based tools.  If KDE, or just kdelibs is ported to OSX and Windows, then it suddenly becomes a fantastic tool for commerical development, which has to worry about such things (We have an even Mac and Windows mixed client base, due to our being in the publishing field, but our current products are all backend servers that run on *nix, and we need GUI admin tools).\n\nAnd of course, then the Linux version would be KDE, rather than just Qt, resulting in more KDE native apps.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE on Windows?"
    date: 2006-07-12
    body: "If the project was \"brought more up to date, and marketed to some degree\" it would cost the same as MS and Corel.....\n\nNothing is for free...."
    author: "Voice of Reason"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-17
    body: "Although a nice attempt, I think it is mainly a proof of concept. Which windows-user would mess around with installing this? One who is really interessted would give linux a try, and the others wouldn't mind."
    author: "Fabi"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-17
    body: ">> Which windows-user would mess around with installing this? \n\nOne who was interested in installing a KDE app.  Maybe a suite of KDE applications.  Maybe it will turn into a common library like VBRUNx00.dll.\n\nEven if you are a Linux or BSD fan, and you believe that your OS of choice will take over, someday the tables could be reversed, and the poor guy in the corner running Win2008 while the rest of the company is using KDE on a variety of OSes will need *some* way to read KWord 7.0 files (since they are the business standard).\n\nAnd if you doubt that, I've got a CP/M machine in the closet with Wordstar and Visicalc that was the \"business standard\" not *too* long ago.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE on Windows?"
    date: 2001-05-18
    body: "actually, the standards used by koffice are open, so it's very easy to add support for .kwd in other office applications (ms will do so in 2008, if they're still around)."
    author: "Evandro"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-19
    body: "Your comment is the first sane thing that I've seen in this thread.  This is nothing more than a proof-of-concept which hasn't been thought through to the end-product.  There might be a geek or two that actually implements KDE apps on Windows, but the global population will be in the 2 or 3 digits range -- hardly worth getting up from your chair.\n\n\"To a man with a hammer, everything looks like a nail\""
    author: "APW"
  - subject: "Konqueror/Embedded"
    date: 2001-05-17
    body: "btw konq/e is doing quite well. I really like this small browser. Go try the latest snapshot (you don't need anything special for it): http://devel-home.kde.org/~hausmann/snapshots/\n\nfaq is here:\nhttp://www.konqueror.org/embedded.html"
    author: "Pyretic"
  - subject: "The apps, not the enviroment"
    date: 2001-05-17
    body: "I doubt many Windows users have a desire to use KDE's window manager or the like, but might enjoy a few of the apps, like Kmail, Knode, and some of the games.\n\nAs much as I enjoy using Konqueror under Linux, I think there would be little motiviation to use it instead of IE on a Windows machine that has IE built-in.\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: The apps, not the enviroment"
    date: 2001-05-20
    body: "I tried out installing Linux on my machine, and with the crappy computer I have, I couldn't get it past 640x480 in Linux.  All the drivers on this computer are for Windoze only, so I'm stuck with it.\nWhat I want to say is that there seems to be little real motivation for the common Windows user who has been using Windows all his/her life, and now people want them to use Linux.  Sure, some of us realize that free=better, as well as stable, fast (though it did lag on my system, hardware garbage again), powerful and other things.  I found it a little difficult to get it running myself, I can just imagine the hell an inexperienced user would have installing even the simplest (I was installing Mandrake) distro.  Wait until they have to compile and such.  Linux is obviously superiour, but is certainly not for the masses yet, it seems."
    author: "DragonXero"
  - subject: "Re: The apps, not the enviroment"
    date: 2001-05-21
    body: "I tend now to favor using applications which can either be multiplatform, like Netscape, or StarOffice, or at least share their files with multiples applications.\n\nIn this extent I would use Konqueror on Windows, for two reasons: 1) I like it bettre 2) I could use it on more than one platform.\n\nIt is not irrational that MS understand this, Word files are already usable on Windows, and Apple machines, and maybe more.\nIt is very possible that eventually they will reconize Linux as a real fact of life, and port IE, MSOffice to Linux. For MS this is the lest of two evils, and they may have to give up their grandiose plans to dominate the all industry, and just make money like every one else.\n\nI am betting on a Linux version of MSOffice.\nBecause not doing it would be stupid!\nIBM learned the hard way that resisting to UNIX was a major mistake.\nAndre G-"
    author: "-Andre"
  - subject: "Re: The apps, not the enviroment"
    date: 2001-05-21
    body: "I tend now to favor using applications which can either be multiplatform, like Netscape, or StarOffice, or at least share their files with multiples applications.\n\nIn this extent I would use Konqueror on Windows, for two reasons: 1) I like it bettre 2) I could use it on more than one platform.\n\nIt is not irrational that MS understand this, Word files are already usable on Windows, and Apple machines, and maybe more.\nIt is very possible that eventually they will reconize Linux as a real fact of life, and port IE, MSOffice to Linux. For MS this is the lest of two evils, and they may have to give up their grandiose plans to dominate the all industry, and just make money like every one else.\n\nI am betting on a Linux version of MSOffice.\nBecause not doing it would be stupid!\nIBM learned the hard way that resisting to UNIX was a major mistake.\nAndre G-"
    author: "-Andre"
  - subject: "Re: The apps, not the enviroment"
    date: 2001-06-02
    body: "Hi Karl,\n\nI would have to disagree withyou on GUI users staying with IE.\n\nHave you tried to reinstall 5.5 or SP6A again after the NT4.0 system crashes? It\u00b4s a bag of ****\n\nThe problem with SP6A High Encyrption is there isn\u00b4t one! You have to rename a dll before reinsatlling a Service Pack, thats nuts!\n\nThis is the kind of functionalitiy and control that KDE will bring back to Admins. It will also ease the transition to Linux as this will be  a fact soon."
    author: "Michael Goedeker"
  - subject: "Re: The apps, not the enviroment"
    date: 2001-06-28
    body: "> I think there would be little motiviation to use it instead of IE on a Windows machine that has IE built-in\n\nNot if you've installed Win98 using the 98lite utility (which makes MSIE & a number of other \"embedded\" components uninstallable).  Sometimes you need to set up a Windows system for friends or family; sometimes Linux is not a good choice at the time, but you'd like to start moving them towards OSS."
    author: "James E. LaBarre"
  - subject: "Enviroment NOT APPS"
    date: 2002-01-10
    body: "I would love to see KDE on the win 2k platform because it would give every one and me the ability to run our faverite win 2k games while still have the best GUI."
    author: "!Q!"
  - subject: "Re: Enviroment NOT APPS"
    date: 2002-06-21
    body: "I agree! I love KDE's interface, especially the multilpe desktop feature. Such a great idea, I don't know why Micro$oft hasn't implemented this yet. I have to open up 20+ windows when I'm working (I have 23 open right now) and I would be nice to seperate them onto seperate desktops according to their function. Does anyone know of something like this out there for Windows right now?"
    author: "Mark"
  - subject: "Re: Enviroment NOT APPS"
    date: 2002-06-29
    body: "maybe Lindows will make us happy, that's if mS doesn't kill it first!"
    author: "Chris"
  - subject: "Re: Enviroment NOT APPS"
    date: 2003-12-23
    body: "i have an nVidia card... it can setup multiple desktops under Windows"
    author: "nonUSER"
  - subject: "Re: The apps, not the enviroment"
    date: 2002-10-14
    body: "When somebody found it available for windoze please let me know. B.t.w. did somebody checked those crystalyze XP Skinz they are in KDE look, very kewl ;)"
    author: "D@V\u0080"
  - subject: "Re: The apps, not the enviroment"
    date: 2006-03-28
    body: "This post was so long ago and still very little has happenned :-(\n\nKDE adds a lot to QT and having it availble for application development under windows would be great.  Otherwise WXwidgets and GTK are going to lead the way with progressively more support and simpler developer licensing.  GTK apps never give the right feel under windows - which gives users an impression of dubious quality.  Perhaps people think \"is everything this bad under linux - why would you go there?\"\n\nIf I develop using the best platform choice for linux (KDE) where can my code go right now?  Only to linux users.  But if KDE is the development platform of choice even under windows we potentially bring many more apps to linux.  This has got to be a good thing.\n\nDo not hold back for political reasons!  KDE is the platform (not win32 or linux)  Get it being used by the largest number of people\n"
    author: "Andrew"
  - subject: "Re: The apps, not the enviroment"
    date: 2006-03-28
    body: "Nobody reads these old forums.\nIf you are interested in useing kdelibs on win32 see the wiki:\nhttp://wiki.kde.org/tiki-index.php?page=KDElibs+for+win32"
    author: "Nobody"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-18
    body: "This is a good idea but it has to be done systematically, by creating some X-independent layer including system independent IPC and window manager layer. KDE for Windows will always be a toy, but I would like to see a framebuffer-based KDE."
    author: "Hi"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-18
    body: "There is an X-independant layer. Its called Qt.\n\nSpooq"
    author: "Spooq"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-18
    body: "But it's not X-independent unless you pay for the commercial version."
    author: "Rakko"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-18
    body: "The original poster was talking about a Unix framebuffer version.  In that case, Qt/Embedded would do just fine.  The license is the same as Qt/X11."
    author: "Justin"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-20
    body: "Actually Qt/X11 is licensed under two licenses, QPL and GPL, whereas Qt/Embedded is licensed only under GPL."
    author: "nitpicker"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-18
    body: "I think KDE should avoid windows, otherwise we'll just end up with a Mozilla-like \"port to all platform\"-hell... Spare us!"
    author: "Bj\u00f6rn Svensson"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-18
    body: "Well, let them port.\n\nBut there should be no windows compatibility overhead in kde itself in future. Not go back to stone age.\n\nMartin"
    author: "mg"
  - subject: "Almost useless"
    date: 2001-05-18
    body: "Seriously.  KDE should never focus on Windows, however it isn't quite so much of a problem if it is handled by someone else without a fork (similar to the Konq/Embedded project).\n\nStill, who would use this?  When people talk about OS strengths, it's usually:  Windows has applications, Unix is stable.  Unix applications on Windows then seems silly.  Really, the true problem with Windows is not its GUI.  It's the crap underneath.  KDE running on Windows will be no more stable since it's running on top of the same crap.\n\nAnd most users would not be interested in running a completely different desktop on Windows (anyone remember Litestep?).  If you're going to do that, you might as well run Linux anyway.\n\nI think a project like this would only help in very niche situations.  I can remember times when I've been stuck at a Windows box and I wished I had Konqueror.  Or perhaps Licq.  But really, they weren't *that* necessary.  Light is nice, but if I'm only using this computer for an hour then I'll deal with the bloat.\n\nMaybe this is one of those \"I'm bored, let's do something\" projects.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Almost useless"
    date: 2004-04-27
    body: "Hi you wonderful people!\n\nSorry if I seem a bit manic, but I've just had my first Linux experience, even if oddly enough, I'm back at XP at the moment posting through Opera. \n\nWhat I wanted to say regarding KDE on Windows, the only people you'll reach with that (in regards to the Windows masses), are the experimental types. However, those have dual boot anyway. The average MS user is much too scared to use anything Linux, the problem here is psychological. Mental block. \n\nPeople who do not consider themselves tekkies (Hello!;-) are scared shitless of Linux because they *think* that if they make any mistake, it will create a black hole and swallow the solar system. It really doesn't matter even how smoothly KDE would run on Windows, your average tekkiot would not install it simply because of the fear it would \"do something\" which was irreversible.\n\nSo unless there was a specific need by some prof, I doubt most dozers would even try it because it's all a bit irrational really, myself included. For several years I moved *very slowly* towards the fence, intimidated as hell, to get a whiff of Linux air and wanted to wait until 'I bought the next computer' so I 'can mess up the old one' with Linux. Like I said, irrational. \n\nEven dual boot was a threatning concept to me, the sheer thought brought on fantasies about penguins eating their way crisscross through partitions while grinning evily. \n\nThen, I found out about the Live CD thingy and I must say the concept is just brilliant. Pop in a CD, play with Linux, take out the CD, restart and you're \"back to normal\". Only that for me, normal suddenly didn't feel quite normal anymore. I'm so impressed. Sure, KDE still \"ported Linux to me\", but under Linux. While I'm willing to learn, there is just no way I would have been interested in any Unix based system without practical GUI. I think command prompts are scary. This might change with time and who knows where the road will lead, but I'm pretty confident that this Windows box here was the last one I bought. I actually start feeling tricked that most PC's are sold with Windows preslapped, if I had had (had had?) a choice when I bought my PC's with the preinstalled Linux being considerably cheaper, of course I would have opted for Linux. Who dosen't like to save a couple of bucks?\n\nSo anyway, sorry for the long post, but it was the 'supersave' 'totally reversible' LiveCD which did it for me (first SLAX then Knoppix). "
    author: "Your_average_MS_user"
  - subject: "Native Win32 would be rather easy..."
    date: 2001-05-18
    body: "If you look at the sources of Qt, *very* little of it is dependent on Xlib. Once you port QWidget, QPixmap, QFont, and QFontMetrics, virtually everything else is derived from that. 99% of the work will be in those 4 classes. Xlib is used in very little of Qt itself (for obvious reasons since Qt sells a native Windows lib). You can tell TrollTech was very careful to minimize the platform dependent code, because they want to sell the Windows version. Porting the GPL version to Win32 should not be a massive effort, since it's the same sources used in the professional version, just with the Win32 code missing.\n\nThere are other classes that would require some work like the sound and network I/O classes, but KDE doesn't use these AFAIK.\n\nKDE is also a lot less X dependent than you'd think (which is why we also have Konq/Embedded ;-) While KWin and KDesktop doesn't make sense on Windows, getting the rest of kdelibs running natively on Windows should also be straightforward with a good Posix compiler."
    author: "Mosfet"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2001-05-19
    body: "This would be an interesting exercise.  If someone were to port the free Qt to Windows, even if they did it badly, if it took hold then TrollTech would probably just release their official version using the same dual license scheme that they have for Unix.\n\nReally, they have nothing to lose, and plenty to gain.  Qt would become THE definitive way to write cross-platform apps.  The community of developers who write free software would have a no-brainer choice when they want to write apps for both the Linux and Windows communities.  This would of course cause an avalanche effect of commercial developers doing the same, all paying for their commercial Qt licenses.\n\nIt would also have the pleasant side effect of making KDE the de facto standard desktop for Linux, which would also mean more license revenue for TrollTech from ISV's who will finally start releasing Linux apps now that there's a standard set of desktop API's to write to.\n\nEveryone wins.  TrollTech, KDE, Linux, and of course the end user.  Are you listening, TrollTech?  This could be big."
    author: "Art Cancro"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2001-05-19
    body: "Like I said in a post earlier, my guess is that a lot of the Qt/Windows license sales are for in-house development.  Trolltech would probably lose a LOT by releasing it as GPL.\n\nHonestly, I can't name any Qt/Windows applications.  This may be because it isn't so obvious what toolkit is used in the Windows environment as it is in X.  Still, this leads me to theorize that:\n\n1) In-house Unix developers use free Qt/X11\n2) In-house Windows developers use nonfree Qt/Windows\n3) Commercial Unix developers use nonfree Qt/X11\n4) Commercial Windows developers use nonfree Qt/Windows\n\nSince the majority of software in this world is in-house, Trolltech's biggest customer base is probably #2.\n\nLet's say 80% of their sales are for in-house work.  Would the benefits of a free Qt on all platforms outweigh the obvious drop in license sales?  GPL is really not a problem for in-house developers.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2001-05-19
    body: "I'm not talking about TrollTech doing it. I don't work for them, I don't care about their license sales ;-) What I'm talking about is if someone else wanted to take the GPL QT/X11 and port it to Windows, it wouldn't be that hard to do since it's already designed to be run on multiple platforms. Looking at the code it's fairly straightforward."
    author: "Mosfet"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2001-05-19
    body: "He was replying to the comment above his more than your comment, Mosfet.\n\n\"I don't work for them, I don't care about their license sales ;-)\"\n\nI care about TrollTech's license sales!  TrollTech is a good company, they produce a good product which we all benefit from, and if they went out of business it would be a big loss.  TrollTech obviously doesn't want a GPL QT on Windows, or else they would have done it already.  I agree with Justin, a GPL QT/Win would be disasterous for TrollTech.  Do you want Open Source to be seen as poison to companies that might be considering open-sourcing their products?  We don't want to kill off companies that open-source their products, we want to encourage them!\n\nBesides, a GPL QT/Win would have questionable benefits.  KDE on Windows?  Great, all the stability of Windows with the broad application base of KDE!  There are really no GPL apps that Windows needs and QT/X has.  The only thing I can think of would be increased interest in QT development by open-source types, but I don't think it would be increased very much since most open-source developers use Linux or BSD anyway."
    author: "not me"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2001-05-20
    body: "I would like apps I write for KDE to be able to run on Windows (and Mac, and Beos, and everything else ;-). Windows is irrelevant to me because I can't hack it, but that's just me. All I know is I want as many people to be able to use cool KDE software as possible.\n\nI don't feel any need to discourage porting in order to protect TrollTech, either. If someone wants to port, it's totally within their rights. TrollTech would have to live with commercial application licensing, like everyone else that does GPL libraries. Saying \"Okay, we'll use the GPL but please don't use all the rights given in the license!\" is crap (which BTW they never said)."
    author: "Mosfet"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2001-05-22
    body: "I agree with your statemens Mosfet. It seems like porting QT-Free to Windows could also benefit KDE, since any QT-Free apps created in Windows could easily be ported back to KDE/Linux.\nAnother major benefit of Qt and KDE on Windows would be the porting of Konqueror to Windows. If Konqi was available on Linux and Windows, Konqi could potentially become a major competitor in the browser wars.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2002-04-24
    body: "It's a good thing you can't, or we'd have to worry about trash like Liquid coming to Windows."
    author: "Andrew Ymirth"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2001-09-07
    body: "Porting the GPL Qt/X11 into a GPL Qt/Win32 sounds like the right thing to do.  Where shall we host this project so that I may join?"
    author: "BLink"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2001-09-22
    body: "I'd like to see Qt ported as well. For two main reasons: 1) this would take 9/10ths of the BS out of porting apps from X11/Linux to Win32 MUCH easier 2) if the port is done correctly, it could open the door to a DOS based port. Think about it? FreeDOS is a decent little OS for the old DOS nut (all bet missing a bootmanager), but it lacks the user-friendly GUI environments that X11/Linux and MS products have. With a port of QT to Win32, the main task of porting to DOS becomes designing a few libraries instead of building an entire GUI toolkit, WM, etc.\n\nThese are only a few of my ideas.\n\nAnywho, later.\n\nGary Greene <rei_0000@yahoo.com>\nChief Software Architect, S4 Inc. - Operating Systems Development Division\nGrand Rapids, Michigan"
    author: "Gary Greene"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2001-09-22
    body: "I'd like to see Qt ported as well. For two main reasons: 1) this would take 9/10ths of the BS out of porting apps from X11/Linux to Win32 MUCH easier 2) if the port is done correctly, it could open the door to a DOS based port. Think about it? FreeDOS is a decent little OS for the old DOS nut (all bet missing a bootmanager), but it lacks the user-friendly GUI environments that X11/Linux and MS products have. With a port of QT to Win32, the main task of porting to DOS becomes designing a few libraries instead of building an entire GUI toolkit, WM, etc.\n\nThese are only a few of my ideas.\n\nAnywho, later.\n\nGary Greene <rei_0000@yahoo.com>\nChief Software Architect, S4 Inc. - Operating Systems Development Division\nGrand Rapids, Michigan"
    author: "Gary Greene"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2001-12-27
    body: "I have taken an alternative route to this project.  After stumbling upon the code from NTXlib.  I started a project to make a library, that is compatible with libX11.dll, but instead calls native Win32 API, instead of using an X server.\n\nI am using QT/X11 from the kde-cygwin page for a testing bed.  Numerous applications are running, some showing signs of displaying properly.  The project is still in the very early stages, but if anyone is interested, visit:\n     http://www.sf.net/projects/libw11"
    author: "Don Becker"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2001-12-28
    body: "quite interesting."
    author: "gt"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2003-01-08
    body: "Well... just do it! and do an official Win32 port of QT/KDE! not under cygwin, but under doze! juuuust to modularize windows a little bit... and then other things, up to the kernel... and BAM! we have a free version of windows!\n\nexcuse my enthusiasm..."
    author: "Anonymous Coward"
  - subject: "Re: Native Win32 would be rather easy..."
    date: 2003-03-29
    body: "I've actually been thinking about this for a while.  I personally really prefer the KDE interfac e to windows, and have been wondering if it was possible to port KDE in such a way that it could replace explorer.exe.  What do you guys think?"
    author: "Sam"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-19
    body: "I couldent care less, im not using windoze."
    author: "[Bad-Knees]"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-19
    body: "Somehow I get the idea that we are loosing sight of the forest.  If we have a killer GUI like KDE and a killer app like KOffice (needs improvement), people will flock to Linux, KDE, etc.  The hell with windows.  Let's make KDE, KOffice (Open Office?) the best, fastest and most elegant. We're almost there.  I for one, need an AutoCAD equivalent for Linux.  LinuxCAD is crap and Qcad is not there yet.   \nI love what's been done so far.  Thanks to everyone involved for Letting me use a great GUI.\n\nEd"
    author: "Edward Rataj"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-20
    body: "Seriously,  What I see the big problem is, that people are not using Linux (enough).\n\n  We do have killer apps: the GIMP.\n\n  I run Linux on *my* computer, but at work, I have to run Windows (2000).  It is not my computer, it is not my choice, the Business world is crowding around and basing itself off apps like Outlook, amoung others.\n\n  Should I be able to run Linux at work, I would save to company a lot of money, and have apps (like the GIMP) that I want.\n\n  It will be the new companies, those that realize that they just can't afford computers, and Windows too.  Those that choose to be to run Linux so that they can afford to pay their employees, those that don't need Qt/Windows until they have a product to sell.  These are the companies that will bring Linux to the business marketplace.  These are the companies that will make KDE famous.  These are the companies that will grow the Linux desktop use up, develop their products under Qt/GPL, and then storm the still to exist Windows market (probably at least 80%) with their (cheaply created) products, now throught Qt/Commercial.\n\n  As long as Linux becomes serious amoung the desktop (from 4-5% to 15-20%) (especialy popular amoung startups) and Windows keeps a strong base (30-80%), Qt/Comerical(Windows) will be a huge profit potential for TrollTech, and a GPL(Windows) version won't really matter -- assuming the above.\n\n  Similarly, if Linux, Windows, and MacOS all each hold a strong portion of the market (at least 15%+), then Qt/Commercial will be essential for cross platform development .\n\n  However, ultimately, we face a future with further and further general consumer education.  More and more great game MODS (like Counter-Ctrike for half-life) will of course become stardard, moreover, \"KWORD\" MODs will also increase (a.k.a. an increase in the number of serious, capable coders and kernel hackers).\n\n  We will see increased GNU/Linux use.  Predictably, it's happening from the outside in.  First came servers, currently we're seeing it happen to embedded, and adventualy (the hardest to \"Konquer\") the desktop.\n\n  The next Linux boom doesn't have to happen in the USA, France, or Finland.  It doesn't have to start in China, Japan, or Brazil.  It will happen, where it happens.  Many governments will help, including those that really hate Microsoft, those that really hate the USA and/or other foreign influences.  Linux is the silent \"killer.\"  It's existence (or that of an alternative) is unavoidable.\n\n  What is the immediate future?  I'd like to think single boxes.  Each marked \"WIN/LIN/MAC\".  That don't give any preference to either.  They'll contain a single DVD, which will install perfectly, just differing on your system.\n\n  What about today?  Today you don't have to care about Linux anymore.  Its just inevitable.  You can be like me, and do what you can, all while forgeting about Linux at your work PC, and then coming back in amazement of all that has happened in the Linux world, when you come back.\n\n-- Greg"
    author: "Greg Brubaker"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-24
    body: "Ed, Bentley System's has offered Microstation for Linux for at least 4 years now. i've used the windows versions from that of \"4\" back in 1993 through the Microstation-95 version. I've used Autocad (due to necessity (like windows), but thing it sucks big time) for a long time too. Look into Microstation,it is clearly the world best CAD application, and has been lightyears ahead of Autocad since 1992.Good luck! ;-)"
    author: "gaffo"
  - subject: "Re: KDE on Windows?"
    date: 2002-01-15
    body: "are you insane? i have used both autoCAD and microcrap, and autoCAD is clearly the industry standard, and there is a huge reason for that. Microcrap is slow and dumb, it hardly works.  AutoCAD is precise and very fast. You must be very ignorant to how both of the programs operate."
    author: "chad williamson"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-19
    body: "I'd *really love* to see this happen! As I see it, this was almost bound to happen sooner or later! And the more KDE there is out there (whatever the platform) , the better! Anyway - just my 2c worth ... :-) . To the KDE team - keep up the good work - KDE *rocks* !"
    author: "KiwiGuy"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-20
    body: "Better yet, implement qt in the hardware.  Windows GDI already enjoys that privelege -- so why not do the same with a much more powerful toolkit like QT?"
    author: "Vlad"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-20
    body: "Why on earth would you waste your time? Windows is windows because of IT'S windows. Rip the guts out of it, remove functionality and (dare I say it) stability and noone would want to install kde. The only people who would want it are those who use kde on linux, bsd etc. so you'd be aiming at an even smaller audience than now, which would consist of people already using the product anyway. Concentrate your efforts where it counts."
    author: "Con Kolivas"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-21
    body: "kdelibs would be nice to have on windows, because then you can code portable apps not only based on QT but also on the very smooth KDE interface. \nThis will especially help industry that want to port their apps to linux, but need to continue parallel development on windows. An even better thing would be a wrapper to the MFC event loop so that you can use both events loops in one program for porting big apps in small steps.\n\nI think there are a lot of developers out there in the industry who'd love to make a linux version or their products, but who cannot persuade their business people to throw away their code and start over from the beginning. If KDE helps the people to make an easy transition from MFC to QT/KDE this will result on a lot of applications being available for Linux/KDE!"
    author: "eva"
  - subject: "Re: KDE on Windows?"
    date: 2006-12-16
    body: "I'm not sure how good it would be to even bother writing an MFC wrapper, because Microsoft is essentially phasing MFC out. They don't plan on updating it much at all in the future, so it seems they're planning on abandoning support for it, in much the same way they abandon old OS's a short time after their newest version is released.\n"
    author: "Mike"
  - subject: "Re: KDE on Windows?"
    date: 2006-12-16
    body: "I'm not sure how good it would be to even bother writing an MFC wrapper, because Microsoft is essentially phasing MFC out. They don't plan on updating it much at all in the future, so it seems they're planning on abandoning support for it, in much the same way they abandon old OS's a short time after their newest version is released.\n\nSo essentially, any software using MFC will probably require recoding anyway. Why not move it all to Qt? It's more powerful and easier to use, anyway..."
    author: "Mike"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-21
    body: "How would you implement the blue screen of death in KDE. :)\nReally, with having to reboot that unstable opperating system all the time, why would you want to give KDE this reputation ??\n\nGary"
    author: "Gary"
  - subject: "Re: KDE on Windows?"
    date: 2001-09-01
    body: "I don't think that the instability of Windows would diminish the credibility of KDE. I know of many companies that would like to adopt Linux, but are limited because they can't get the software they need on it. If you believe as I do, that the Free Software model inevitable yields better software, then you won't be scared of proprietary software competing with free software over the long term. Porting KDE to Windows would allow free software to compete with proprietary software based on merit, and in the mean time create a larger selection of KDE applications. If we are confident in the long-term credibility of Free Software then we will not be scared to share an API on a proprietary system. Think of a KDE port to Windows as an onramp to Linux."
    author: "George Molson"
  - subject: "Re: KDE on Windows?"
    date: 2003-01-26
    body: "I don't know about you, but personally I haven't rebooted my Windows machine since the last security patch.  And before that, the previous one.  And before that, the previous one, And before that..........\n"
    author: "TX"
  - subject: "I want KDE on BeOS"
    date: 2001-05-21
    body: "I don't realy care about a Win32-version. But a BeOS version of some KDE apps (like KOffice) would be cool. According to this article (see the link below), there are already people working on it:\n\nhttp://www.benews.com/story/4007"
    author: "XRay"
  - subject: "KDE on MacOSX?"
    date: 2001-05-21
    body: "Check out this link. We will soon have Qt for Macintosh, and maybe we can have KDE for MacOSX then!\n\nhttp://www.trolltech.com/company/announce/mac.html"
    author: "Stentapp"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-22
    body: "I'll think this would be a great idea to put kde on Windows boxes !\nThen users under windows could personize the entire desktop.\n\nAnd this is for kde the great way to expanse and have more users."
    author: "DjCoke"
  - subject: "Re: KDE on Windows?"
    date: 2001-05-22
    body: "I think it should be done- Introduce Windows users\nto the glory of The Linux/Unix way of thinking. Expand their minds. Challenge assumptions. That way, when the Microsoft empire falls, There will\nbe users who are primed and ready to switch to Unix/Linux. Yes, I know, the Gui is not the OS,\nas in Windows (and should never be! Thank God) But, it's a step to more widepread acceptance without having to turn into Windows.\n\namadeus@asn.com"
    author: "Angel Mendez"
  - subject: "Re: KDE on Windows?"
    date: 2001-06-28
    body: "Some of the responses here show that some of the readers are rather unfamiliar with Windows.\n\nLet's explain the 3 main areas of concern here:\n  Kernal\n  Shell / Window manager\n  Browser\n\nPeople have this nasty habit of saying that Windows is a cancer by Billy that is unstable.  That is true of the 9x/ME kernal, which is a hack of DOS.  The NT kernal, used in NT/2000/XP is about as stable as Linux.  In some situations, yes, it is unstable, in others, it's more stable then Linux.  (Chances are, if it crashes a lot, it's a sign of bad hardware and not a buggy kernal.)\n\nWindows default \"window manager\" and browser are explorer.  IE (internet explorer) isn't integrated into windows, it's integrated into explorer.  In fact, it's just as integrated as KDE's web browser is integrated into KDE!\n\n(Thus, if you want to get rid of IE, you'll need to replace explorer alltogether)\n\nIf you replace explorer with KDE, what do you gain?  As far as stability, a lot, because you're replacing the weakest link of windows.  You also get a 100% replacement shell, which none of the current replacement shells can do.  They all still use explorer to browse files!  As far as usability, you don't gain much.  KDE has a steeper learning curve then explorer, IMO, thus novice users might be even more intimidated.  Expert users will probably like it better.\n\nWhat do you need to get KDE to replace explorer/IE?\n    - You need to get the port of XFree to \"mix\" its onscreen windows with the windows on windows's desktop.\n    - You need to get KDE to integrate windows from windows into its taskbar.\n    - You need to create replacements for the explorer executables that wrap into KDE.\n    - You will probably need cygwin or equivilent\n\nDoing so will allow for full KDE integration and will actually make WindowsNT even more stable."
    author: "Andy Rondeau"
  - subject: "Re: KDE on Windows?"
    date: 2006-12-16
    body: "I defy you to name *one* situation where the Windows kernel (it is kernel, by the way, not kernal) is more stable than a Linux kernel."
    author: "Mike"
  - subject: "Re: KDE on Windows?"
    date: 2007-07-09
    body: "Fundamental flaws in the Linux kernel can often not be fixed without totally recompiling the kernel or taking the time to install a new kernel entirely.  Patching the Linux kernel can be a time consuming task as well.  With windows, security flaws and other issues can be easily and often automatically addressed by patches, allowing businesses to maintain a lower total maintenance cost in many cases.  At IBM we use both open source operating systems like Linux and Windows.  We also use Solaris SPARC systems and AIX systems.  Don't be so quick to debunk a particular operating system for every case of use.  No one operating system can fit the needs of every user or situation.  Thats why it's important to consider OS choices when planning a network or system architecture.\n\n-- Joe Davy"
    author: "Joe"
  - subject: "Re: KDE on Windows?"
    date: 2007-07-10
    body: "> Fundamental flaws in the Linux kernel can often not be fixed without \n> totally recompiling the kernel or taking the time to install a new \n> kernel entirely.\n\nLinux kernel security flaws are relatively rare compared to flaws in other pieces of software that you might be running. Also, upgrading the kernel to fix a security bug, when managed correctly by your distribution, should be fairly painless - you just need to reboot afterwards, exactly like you do after applying most Windows updates.\n\nThe underlying message of your post is a good one, but if you're going to point out an issue in Linux at least pick one that is a real problem."
    author: "Paul Eggleton"
  - subject: "Re: KDE on Windows?"
    date: 2007-04-03
    body: "Windows sucks dude. The only thing I like about Windows is the Windows installer. I am still figuring out Linux so text based instalation is new to me. With Windows XP (that was on the family's computer, I only use it to play WoW and surf the web because I still havn't gotten the Netgear WG121 wireless adapter working on my Linux PC. I would get one that is compatable out of the box but I am broke.) I have to reboot 1 too 3 times a day. I upgraded the family computer to Vista the other day and that is a little better but still there are plenty of software incompatabilitys and the sound bugs out.\n\nThe only thing that is cool about vista is the sidebar. I am still trying to find one like that for KDE.\n\nBut to say what I wat trying to say before I got off track is that everything Microsoft does to improve brings its own problems"
    author: "Idog_is_1"
  - subject: "Re: KDE on Windows?"
    date: 2008-02-11
    body: "I really like to see fanatics at work. Windows has a lot of bugs? Yes it does, so has Linux. As I read earlier and agree, no Operating System can satisfy every need  or machine there is.\nAt home I still use Windows XP. It is now installed for almost two years, never had to reboot it because of some weird crash, never had to reformat it. And by the way, in all my years in using windows, all the times i reformated my hard-drive, i did it because of all the software i installed and uninstalled.\nAnd for you who stand up for Linux, I do love Linux, but it's a mess to install whatever you want in it. Sure, it is getting easier as time goes by, but still, we have to recompile stuff, it almost never works at first, etc.\nFor those of you who like to compile you OS from scratch, ok, it's awesome, but for those of us who prefer spending there time using the software instead of rewriting it, linux is kind of a turn-off.\nWhat I'm saying is, Windows has it's good points and it's bad points, but then again, so does Linux.\nI'm still trying to replace explorer with KDE for good. I prefer KDE, but won't trade it untill i find some easy way to install it."
    author: "Ficc"
  - subject: "Re: KDE on Windows?"
    date: 2008-03-04
    body: "Why bother with Windows?\n\nWell, until ReactOS finally (if ever) matures, there is simply no replacement for the functionality which has been extended to Windows through the MILLIONS of commercial and freeware offerings available to the platform.  The flexibility and diversity provided here cannot be matched by Linux or anything else, period.\n\nThere's more.\n\nA significant portion of the difficulties which prevent Linux from being widely adopted as a replacement for Windows is the ongoing unavailability of drivers for hardware (printers, anyone?); and the pervasive, entrenched unwillingness on the part of the development community to drop its arcane and problematic approach to software installation.\n\nThe final crippling blow for Linux really comes down to ego on the part of individual members of the greater development community.  For, without a central, unified SINGLE distro effort to forcefully address those several issues which prevent its widespread adoption, Linux will never find popular support in either the freeware or commercial sectors.  Get smart, get it together, or get out, Linux: Microsoft is laughing its head off at your narcissistic, disorganized idiocy.\n\nTower of Babel, anyone???\n "
    author: "Old Hand"
  - subject: "Re: KDE on Windows?"
    date: 2008-03-20
    body: "well i used Linux for 3 years and eventually the windows world beat me in the drivers section. No driver for my usb wifi adaptor (very low signal with ndiswrapper), my mp3 player wasn't working well in linux, etc. So KDE on windows would be a bless. For now i'm using FlyakiteOS to change the interface of windows.  I hope that eventually i'll find a way to replace Explorer with KDE "
    author: "dan"
  - subject: "would it be more stable then the current windows?"
    date: 2001-10-11
    body: "i'm a newbie when it come to linux, in fact i have little to none experiance with unix or linux.  i, like many other windows users, am fed up with the MS OS. it's unreliable, unstable, and uncooperative when it comes to changing it to your liking.  i also grew up on windows and am stuck on using thier OS for the time being, and i'm sick of not have the control and stability of my own computer.  the idea of having a desktop other then the MS desktop is a great idea.  it would allow normal user in the same predicament as me to warm up to linux or Unix software.  however, the idea of piggy backing on the MS OS rasies questions on the stability.  will it work better then the windows currently on the majority of computer that use MS?"
    author: "dan sawvel"
  - subject: "Re: would it be more stable then the current windows?"
    date: 2005-10-04
    body: "piggybacking ms may be varry unstable. If I where you i would try a linux live cd fedora core or suse to get the feel of linux and decide if you want to make a permanant change. I'm hoping you will go with linux"
    author: "Joey"
  - subject: "Going in for the test... heh"
    date: 2001-10-17
    body: "Well...\n<p>\nI have KDE running on Windows right now with cygwin and xfree86.\n</p>\n<a href=\"http://www.gr3p.com/crap/XWin.jpg\">XWin.jpg</a>\n<br>\n<A href=\"http://www.gr3p.com/crap/WindowsKDE.jpg\">WindowsKDE.jpg</a><br>\n<A href=\"http://www.gr3p.com/crap/WindowsKDE2.jpg\">WindowsKDE2.jpg</a><br>\n<a href=\"http://www.gr3p.com/crap/WindowsKDE3.jpg\">WindowsKDE3.jpg</a><br>\n<Br><br>\nWhat I'm going to attempt now, is to replace the  shell=explorer.exe  and see if I can just load up cygwin.\n<br><Br>\nWell, the reason, is because when I load it, and then load KDE, it's soooo slow.\n<br><Br>\nIt reminds me of a couple years back, when I was running Win98 on 8mb ram.  Heh.  Slow.  But now I have 228MB ram, 333mhz AMDk6-2  Lemme see if I can get this too work.  If not, oh well. :)  I'm running windows 2000 professional btw. :)"
    author: "Rich Kreider"
  - subject: "h3ll ripping explore.exe out and slipping KDE in.."
    date: 2002-01-10
    body: "as a shell would be nice.... i get sick of looking at the same old windows... and functions.\n\ni usually replace explore with LiteStep from LiteStep.net but the themes from there usually crash due to a step.rc error or missing dll that u cant find anywhere handy.\n\nit would be a good idea for KDE to come up with a shell replacement for windows machines or post a clear HOW-TO on the ways and means."
    author: "Dr_Blackross"
  - subject: "Re: h3ll ripping explore.exe out and slipping KDE in.."
    date: 2002-05-23
    body: "download"
    author: "maan"
  - subject: "Re: h3ll ripping explore.exe out and slipping KDE in.."
    date: 2002-07-15
    body: "I'm new at this computor stuff...from what i understand i don't need explore.exe then what do i need to download pictures from afriends e mail????"
    author: "christine labatt"
  - subject: "Re: h3ll ripping explore.exe out and slipping KDE in.."
    date: 2002-10-10
    body: "Ia gree completely.  I think a version of KDE that would be used like a shell and implement the best features of windows with the best features of KDE, and allow you to run certain KDE only apps such as ksirc and kvirc, and konq, and a few others on your winblows machine would be nice.  Even if it was just an overlay or an emulator would be an improvement, it would allow people to get used to KDE, and all the intracacies within without having to totally strip down their windows machine all at once and be lost in a world they know nothing about and have to limit productivity while they take th time to learn an entirely new O/S.  I know alot of you are thinking why can;t they jsut abandon it little babies, and it's people like that with attitudes like that, that will doom linux to a select few then to getting it on majority of computers like everyone wishes it was.  Problem is some people RELY knowing their computer inside and out, and scrapping it for something else could put them on the streets or drop them to another job or any number of things.  You can;t generalize like that.  Linux in general has finally started over the first few hurdles, but we still have a LONG ways to go before we can kick the %$#^ out of Gates.  To smooth the eventual transition though, desktop developers including KDE, would do well to at least make a windows shell, and even better to port some of their apps or all of their apps to run in that shell, maybe with an emulator or something.  Anyway I would say more about what linux needs to do but this is hardly the place."
    author: "Michael Dooley"
  - subject: "Re: h3ll ripping explore.exe out and slipping KDE in.."
    date: 2003-08-08
    body: "Is this Jellybean4u?"
    author: "Katieboo"
  - subject: "i need installer?"
    date: 2003-02-03
    body: "i'm looking for an installer for KDE and GNOME on Windows(CYGWIN) which is only one file so that when i will download it, it's only one file. i'm tired downloading many files. is there a site for KDE and GNOME that is already in zip, gzip, rar, ace, tar, bzip, or any archives(all files in compress). can anybody tell me where i can download it...\n\nthat's all, thanks!\n\ncygwin user, CamiloX"
    author: "Camilo Lozano III"
  - subject: "Re: i need installer?"
    date: 2004-10-24
    body: "I myself am looking for an installer.  So many people talking about this tool, nut I am exhousted of too many files on sourceforge and all other sites I looked for.\nAll tar bz2 and many other fiels are there which I downlaoded, but cant find Konqurer or any installer exe in them.\n\nIs there anyone who can direct us to the right files to downlaod to install\nkde for windows system. I read somewhere about downlaoding kexi-2005beta4-win32.exe which is simply set of 2 or 3 tools created with outlook of kde but does not include kde itself for windows.\n\nbtw: I am running win98.\n\nAny help is appreciated.\n\nregards\nAli Imran"
    author: "Ali"
  - subject: "Re: i need installer?"
    date: 2005-02-10
    body: "Try winik, http://www.winik.sourceforge.net.  I've installed it, but can't get kmymoney to compile.  It's still pretty cool."
    author: "pnutjam"
  - subject: "KDE as an explorer replacement"
    date: 2003-09-24
    body: "I can see why you wouldn't want to waste time make windows look like linux but look at it this way:\n\nI currently use a windows desktop simply to run programs like dreamweaver and photoshop for my work. There just simply isn't a way for me to switch completely to linux with these dependencies. I also use a linux computer that doubles as a home server. I love my kde desktop and I would love to have that environment on my windows machine. It wouldn't change the fact that I'm using windows but it would sure make life a little more interesting. I would like to agree with a previous comment that started out with \"Some of the responses here show that some of the readers are rather unfamiliar with Windows.\" Porting kde and it's applications to windows would make life better for us that have to use windows everyday until big software companies understand and create versions of their software for linux. It would be very interesting to know why Macromedia and Adobe are holding off, you would think that it would be very simple for them to take ther macosX versions and build them for linux?\n\nAs for the benefits of cygwin... my life is now complete... no more typing 'ls' and 'rm' in a windows command prompt with the response of \"comamnd not found\" chmod and chown even work! I set c:\\cygwin\\bin as a search path and my windows already feels unixy, now only if I had a unixy gui......"
    author: "Mat"
  - subject: "Re: KDE as an explorer replacement"
    date: 2004-04-18
    body: "Try Litestep on for size you'll love it"
    author: "Jon Sandell"
  - subject: "poo? YES"
    date: 2004-01-02
    body: "Apparently I have pooed my pants"
    author: "Mr_boots"
  - subject: "And not to forget..."
    date: 2004-01-29
    body: "We can not forget the uninterruptable sleep processes and those damn zombie processes...."
    author: "AcidHorse"
  - subject: "Konsole"
    date: 2004-04-24
    body: "Has anybody tried to use Kde's KONSOLE on windows. I'm dammned to use a win2k at my office and i do not even have the root for my desktop. \nI'd love to use this terminal on windows. will it work with cygwin or it must be recompiled????"
    author: "Steve"
  - subject: "Re: Konsole"
    date: 2004-04-24
    body: "It's part of the KDE Cygwin kdebase package. And use the latest KDE 3.1.4 release, not such ancient one from 2001 to whose story you're replying to. :-)"
    author: "Anonymous"
  - subject: "kde on windows, great business sense"
    date: 2004-07-20
    body: "the microslop world is based on greed and power, and the mergence of GNU over to that OS chops away at the financial power base that feeds the monster. Keep it up... the addition of each new program weakens the profit margin for MS!"
    author: "mtntoprebel"
  - subject: "Re: kde on windows, great business sense"
    date: 2006-09-06
    body: "We, need to break the standard idea, \"that you have to be a nerd if you use linux\" god i don't use linux out of nerdiness, I use it out of cheapness,, hmm that came out wrong, I like the interface of kde quite well and it was the first thing linux i tried, you can customize everything without dling all these fancy crap from several vendors that half the time screw up your computer... damn you alien ware, increase my boot time. Windows is unacomandating to anything, you ever try and copy paste a large amount of files, in windows if one fails, it cancels the rest no option to continue, at least in kde it asks you to retry or ignore and continue or abort, not just \"Ah screw you but i don't want to. I'm windows. I'm too good to work\" It plain sucks i only keep my windows around for games, and and damn you ATI, not support linux well. The general concensus about linux is either they too retarded and havn't heard of it, heard of it don't know what it is, know what it is havn't botherd, or they think its for nerds, mac and windows are the only two things that exsist and i don't like mac. Hell i like mac os i wish i could use it on my computer, but i'm too lazy to do it the hard way, maybe i should anyways, to me an os is worth paying for if it is good and works well, hmm windows doesn't and i would never / never have paid for a copy, I got them free on my computers, and another thing if windows wasn't the only thing stuck on any thing non mac out of the factory, maybe it wouldn't seem so bad, create a new company that sells pcs with a good linux on it, well i'm just ranting now, but SUCK IT MS."
    author: "Abic Shadar"
  - subject: "Great for apps"
    date: 2005-06-14
    body: "I think this is a great idea. It opens the door to KDE and GNU software for people who have used Windows all their life (which was me up until very recently). Also for me, I can now use the free KDE apps on my Windows machine. I hope they get the simple installer, though, because the long installation process is discouraging."
    author: "Harrison TS"
  - subject: "KDE under windows"
    date: 2005-12-09
    body: "Yesterday i had an insight, that i can install KDE under windows and run both win32 and posix applications under one desktop. It would be an easy installation \n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon]\n\"Shell\"=\"Explorer.exe\"\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon]\n\"Shell\"=\"cygwin.exe\"\n\nbut, it would be a very-long optimisation. For WIndows and for Cyrillic))) "
    author: "treehel"
  - subject: "One can hope"
    date: 2006-09-06
    body: "I personally agree, though at heart i like the world of free ware and share ware (for more then just bein chaeper) i've found several problems in coparate software not in others, and oddly enough the freeware seams to work better in most cases perhaps just luck there. But KDE for windows, if only you could get microsoft to endorse it as a standard, but you know MS, the most i can see it getting thought i hope i'm wrong is availiable, hard to find, and only those who habitually use linux will even seek it out, and thus no increase in apps for it. But to get kde working on unix and its children (including mac) and on windows, and some how make it a standard would excellent, though from a corprate stand point and competetion it would be impractical and less profitable for them to do. Becuase they thrive off of proprietary software, and i just now realized that the post i'm replying to is 5 years old, but I still don't see it happening thought i wish it would, cause kde is much better then the windows shell"
    author: "Abic Shadar"
  - subject: "KDE on Windows"
    date: 2007-09-25
    body: "I think this would be an excellent idea i go back and forth between windows and linux all of the time and i think kde is excellent and i would love to see it on windows it would beat looking at that dull start button and start bar all of the time"
    author: "Frank Green"
---
I was browsing <A HREF="http://www.sourceforge.net/">SourceForge</A> today and stumbled across the <A HREF="http://sourceforge.net/projects/kde-cygwin/">KDE on Cygwin</A> project.  Apparently, they have ported <A HREF="http://www.trolltech.com/products/index.html">Qt</A> 2.3.0 for X11, Qt 1.45 and KDE 1.1.2 to use the <A HREF="http://www.cygwin.com/">CygWin tools</A> and <a href="http://xfree86.cygwin.com/">CygWin/XFree86</a>, respectively ports of the popular <A HREF="http://www.gnu.org/">GNU</A> platform and <a href="http://www.xfree86.org/">XFree86</a> to Windows.  Although this is a big step towards making KDE applications useable on Windows machines, it might be interesting to get rid of the X server requirements as <A HREF="http://www.kde.org/people/tron.html">Simon Haussman</A> has done with <A HREF="http://apps.kde.com/uk/0/info/id/924">Konqueror/Embedded</A>.  It seems to me that a free desktop infrastructure of KDE's caliber that runs on both Windows and UNIX, and perhaps even embedded devices runing <A HREF="http://www.trolltech.com/products/qt/embedded/">Qt/Embedded</A>, would increase the number and quality of KDE programs, and open the door to KDE for many more users.  What do you think?

<!--break-->
