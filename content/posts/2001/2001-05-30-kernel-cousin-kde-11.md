---
title: "Kernel Cousin KDE #11"
date:    2001-05-30
authors:
  - "aseigo"
slug:    kernel-cousin-kde-11
comments:
  - subject: "Re: Kernel Cousin KDE #11"
    date: 2001-05-30
    body: "Glad to hear that there is a drive to port KDE 2.x to Qt3.  Qt3 will be everywhere soon anyway, and it isn't much different from Qt2 (can any of the developers from the 1.4 era say which is a bigger leap?), so this shouldn't be a difficult task.\n\nI imagine this is to avoid having multiple library versions on the same box?  I always thought it was strange how most distros came with KDE 1.x & Qt 1.x, yet Qt2 was included also to run things like Licq.\n\nIs there a possibility for Qt to remain binary compatible now from version 3 and on?  Really, what else could they possibly need to add after this?  And would it have to break anything if they did?\n\n-Justin"
    author: "Justin"
  - subject: "Re: Kernel Cousin KDE #11"
    date: 2001-05-30
    body: "Speaking of Licq, why or why is the Qt GUI interface so ugly on it?!  I'm using Licq from the Mandrake 8.0 packages.\n\nI'm an \"micq\" person anyway, but I couldn't help but notice that the Licq GUI doesn't look very good, and doesn't look anything like a typical Qt application."
    author: "Navindra Umanee"
  - subject: "Re: Kernel Cousin KDE #11"
    date: 2001-05-30
    body: "the main window of licq is, of course, themable. if you don't like the default, pick another theme. you can also change the icons. i find it very extensible that way.\n\nas for the dialogs, etc.. it seems to take after the windows ICQ client in regard to its layout. and compiling it with KDE support gives those widgets a nice KDE2 sheen =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Kernel Cousin KDE #11"
    date: 2001-05-30
    body: "Ahhh, I didn't know there was KDE support.  Mandrake should really include that.   \n\n<br><Br>\nI now realize that Licq is defaulting to the Qt Motif style.  I had only forgotten how butt-ugly motif was!  Or has Qt's Motif look deteriorated while I wasn't looking?  ;) \n<br><Br>\n<i>Btw, \"licq -style=windows\" doesn't seem to work.  Anybody know how to force the windows look?</i>\n"
    author: "Navindra Umanee"
  - subject: "Re: Kernel Cousin KDE #11"
    date: 2001-05-30
    body: "Mandrake didn't include it because (I believe) the KDE support has some problems:\n\nThe session management crashes\nI suspect that it has a fairly bad memory leak.\n(Whenever I run it for a long period of time X crashes)."
    author: "Sheldon"
  - subject: "Re: Kernel Cousin KDE #11"
    date: 2001-05-30
    body: "I wonder if kicq will be done before kde 2.2 is out? Does anyone know.\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #11"
    date: 2001-05-30
    body: "Hello,\n\ncould someone tell me or show me a link what the Licq is and how it look like and what the micq is and how it looks like ?\n\n\nThanks in advance\n\ncu\nmg"
    author: "mg"
  - subject: "Re: Kernel Cousin KDE #11"
    date: 2001-05-30
    body: "Check out www.licq.org"
    author: "belochitski"
  - subject: "Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "How about you post a story explaining why dot.kde.org has been down so long, and when you next expect it to die?\n\nThis would be a much more useful site if it worked more than 3 days a week."
    author: "Someone"
  - subject: "Laugh..."
    date: 2001-05-30
    body: "I forgot to mention this in my post below.\n\nIt's funny but one of the biggest problems with running KDE (versus GNOME) is the slow speed of sites such as dot.kde.org and apps.kde.com.  I'm guessing a lot of the problem is me living in Nevada and these sites being in Europe.\n\nMaybe there are some good US sites for KDE software and news?\n\nRimmer"
    author: "Rimmer"
  - subject: "Re: Laugh..."
    date: 2001-05-30
    body: ">>I'm guessing a lot of the problem is me living in Nevada and these sites being in Europe.<<\nNope. They aren't any faster over here in Germany :-( (Neither there is a mentionable differende in speed between European and US Sites as transatlantic links are quite fast nowadays...)\n\nConrad"
    author: "Conrad Beckert"
  - subject: "Re: Laugh..."
    date: 2001-05-30
    body: "Huh? FWIW, dot.kde.org speed is fine here.  I see nothing wrong with the speed.  What are the symptoms that you are experiencing?\n\nWe were a bit slower than usual when we were being Slashdotted the other day, but nothing that wasn't expected."
    author: "Navindra Umanee"
  - subject: "Re: Laugh..."
    date: 2001-05-30
    body: "The server is located somewhere south of Chicago.\nThere should be a mirror i Europe, it doesn't have to be updated in realtime, daily would be fine."
    author: "reihal"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "Our hosting server went down and the owner/admin is out of the country on vacation.  I finally got the idea to get in touch with his ISP myself, and the support guys over there were good enough to bring us back.\n\nIf I remember correctly, the server has a hardware/os crash every weekend, Sunday morning or so.  So you could expect a crash then."
    author: "Navindra Umanee"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "a crash every Sunday?\nwhat the hell , are they running Windoze ?"
    author: "Gnulin"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "Well, according to Netcraft, yes, they are running Windows.  Windows NT 4.0, to be exact.  \n\"The site www.sohosnet.com is running Microsoft-IIS/4.0 on NT4/Windows 98.\"\n(Don't worry, none of the kde.com sites are, though -- they're all Linux running either Apache or Zope)"
    author: "Chad Kitching"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "The problem is not sohosnet, it's our hardware."
    author: "Navindra Umanee"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "LOL!!! They're really running Winblow$ 98 ?!?!?!?!\n!!!!!!!!!!!!!!!"
    author: "dc"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "Can't the owner/admin (Dre I guess) get some help?\nThe KDE League should support these sites actively, they are very important for PR."
    author: "reihal"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "Don't tell me that hardware fails every Sunday morning??? What is this, kind of a joke or something?\nWhich hardware exactly fails and why don't anybody do something about this?\nI am really annoyed about dot.kde.org being down every couple of days. This is not a good for Linux community at all. ;-)"
    author: "Bojan"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "I wish people would be a little more understanding.  We are all, like, volunteers here, you know?  We're doing what we can.  We care too, believe it or not.\n\nWe *don't know* what exactly is wrong.  My *present* *theory* is that the RAID is unstable and bombs particularly when there is high disk activity.  This high disk activity happens to be scheduled on Sunday mornings."
    author: "Navindra Umanee"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "So, change the schedule to another day and see what happens."
    author: "reihal"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "> I wish people would be a little more\n> understanding. We are all, like, volunteers\n> here, you know? We're doing what we can. We\n> care too, believe it or not.\n\nIt is actually a complement when people complain when the Dot is not there, it means that they really love it when it's up and running! :-)\n\nWe have had some (software) RAID instabilities at work but that was a long time ago and was actually caused by a kernel bug in 2.2.9 or something like that.\n\nAnd as said before, there must be a reason for this activity on Sunday mornings. I doubt updatedb would cause a crash but I would indeed check crontab for scheduled events."
    author: "Rob Kaper"
  - subject: "turn left!  turn right!  press the gas!"
    date: 2001-05-30
    body: "one writes:\n>So, change the schedule to another day and\n>see what happens.\n\nanother writes:\n>And as said before, there must be a reason for\n>this activity on Sunday mornings. I doubt \n>updatedb would cause a crash but I would indeed\n>check crontab for scheduled events.\n ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n\nMy dear, but *everybody* wants to be a backseat driver these days! Next time I'll be sure to dole out less of this info/bait.  ;-)"
    author: "Navindra Umanee"
  - subject: "Re: turn left!  turn right!  press the gas!"
    date: 2001-05-30
    body: "Just trying to help you with.. watch out for that old lady!"
    author: "Rob Kaper"
  - subject: "((I've never think they'll mad a movie like this))"
    date: 2003-10-03
    body: "    When i frist time read the tittle of this movie (Trun Left Turn Right), i thought it was some kind of Rord storys. Were they tell the person to make a left then trun to the right and then...bla-bla-bla...\nthen i read the little story lines about it, i think, umm..it's ok!\nbut when i saw a little per-viwe, dang..this movie hock me right away.\nman..my husband Takeshi Kaneshiro is soo danm hot! woow! hahahaha...GiGi is pretty..i like her too, so, im GiGi! lol...\nwell..like i said, i've never seen a movie that are so realy listice like this moive before. \nI like how they set up the movie. where Takeshi and GiGi are living in the same apporment, but still looking all around the world thinking who is their sole mate. And not notice that the person who is next to them is the one.\nBut the bad piont is that..i havn't seen the whole movie. \nohh..runny out of time..so..take care ppl...\n\nbye,\n\nMiss. Takeshi Kaneshiro (in the future)..lol,\nmiyu yang\n\n"
    author: "Miss. Takeshi Kaneshiro"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "Understanding lol it goes down more than its up. I think people have been understanding. Its making kde look like crap. It time to get your act together. Someone else will start a lde site if you don;t then all your traffic will dry up.\n\nCraig"
    author: "Craig"
  - subject: "That would be a GOOD thing."
    date: 2001-05-30
    body: "Go ahead and start a new site, if it's any good, we will even announce it here for you!  We *need* more KDE websites, so why don't you go ahead and do it?  It would certainly be a much better and productive thing than all your bitching!\n\nIf you can generate more KDE traffic than us, than that would be quite a feat and quite amazing.  It would also be quite a good thing for KDE, which would make me VERY happy.\n\nAs for your criticism, we're not Microsoft with infinite resources.  We're a free software project and we work in our spare time.  But I know from experience you won't understand that...\n"
    author: "Navindra Umanee"
  - subject: "Re: That would be a GOOD thing."
    date: 2001-05-30
    body: "lol Well i have a dial up modem but i hope someone will. This really sucks to have the dot down 3 days a week every week. If it was so easy why aren't you up seven days a week? It makes it harder to criticize if someone says yes your right its a real problem and we'll try to fix it. Excuses especially lame ones just provoke jerks like me.\n\nCraig"
    author: "Craig"
  - subject: "clubies"
    date: 2001-05-30
    body: "So who said we weren't fixing it or that it didn't suck? I thought I made it pretty clear that we were doing our best to deal with the issues?  Didn't I also make it clear that the admin was away on vacation?  Isn't he allowed some rest from all your crap?  \n\nAnd the easy thing was sarcasm, just to get you to try it for yourself.  I'm on dialup too FWIW, doesn't seem to have stopped me from setting up this website.  So why not get off your feet and get something done instead of giving lame excuses.\n\nAnd finally, quit your damn exaggeration.  This is the first time in weeks dot.kde.org has been down for so long.  Get the facts straight or get a clue."
    author: "Navindra Umanee"
  - subject: "Re: clubies"
    date: 2001-05-30
    body: "And finally, quit your damn exaggeration\n\nlol Ok maybe i exagerated a bit. Your volunteer work is appreciated. I hope your efforts pay off and the site stabilizes.\n\nCraig\n\noh wait hold that the site just went down again lol."
    author: "Craig"
  - subject: "Re: clubies"
    date: 2001-05-31
    body: "Oops sorry for the multiple postings the site went down again when i was submitting this.\n\nCraig"
    author: "Craig"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "Damn right Bojan, its intolerable, its a rip off, we are not getting our money's worth out of dot.kde.org...\n\nOh wait a minute, we don't pay for it, and the guys that run it do it on their own time, out of love. Now what were you annoyed about again?"
    author: "Taurine"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "I apologize, I didn't mean to be rude. I know people are not paid for maintaining this site. I just don't understand why for example www.linuxtoday.com (ok, this might be commercial site, but look at www.gnu.org or www.linux.org, etc) or similar sites are never down, while dot.kde.org is down every couple of days. Do their servers run on so much better hardware? \n\nIf I were a system administrator of a site that is down so often I would definitely look at it and fix what is wrong (or ask somebody to do it, if I couldn't fix it myself). Otherwise I would be ashamed of myself."
    author: "Bojan"
  - subject: "Re: Offtopic: what's been happening to dot.kde.org?"
    date: 2001-05-30
    body: "I agree with Rob Kaper, dot.kde.org is my favourite site, I love it when it works and I wish it would always work, because here I can find the latest news about my favourite desktop. Seeing it down for couple of days makes me sad. :-("
    author: "Bojan"
  - subject: "Hmmm"
    date: 2001-05-30
    body: "Glad to hear I'm not the only one who has had problems with noatun.  The version with KDE 2.1 hung constantly for me.  The version with KDE 2.1.1 doesn't seem to hang but still has some really annoying bugs (like not allowing only one instance of the player)  I'm pretty happy playing my mp3's through arts using XMMS.  Still I would like a pure KDE player... I hope the problems get fixed.\n\nBtw... does anyone else think a small interface (with small buttons) would be nice for noatun?  There is one small skin currently, but the buttons are so large its very unattractive. \n\nAfter fixing noatun maybe they could make kppp stop hanging when pppd dies unexpectedly ehehe.\n\nRimmer\n\nRedhat 7.1 with kernel upgraded to 2.4.4"
    author: "Rimmer"
  - subject: "Re: Hmmm"
    date: 2001-05-30
    body: "> After fixing noatun maybe they could make kppp > stop hanging when pppd dies unexpectedly ehehe.\n\nA temporary solution if you have an one-user machine is to make /usr/sbin/kppp setuid root and then make /usr/bin/kppp a symlink to /usr/sbin/kppp. It is a major security gap though."
    author: "KE"
  - subject: "I would be happy if"
    date: 2001-05-30
    body: "I thought the problem would be fixed in the next KDE.  However, I'm not holding my breath.  The only response to my bug report has been another user with a similar problem."
    author: "Rimmer"
  - subject: "Re: Hmmm"
    date: 2001-05-30
    body: "Much as I admire the effort going into Noatun, and I am friends with the authors, I am afraid that I have some points to make:\n\n - It was said in the KDE Cousin that bug reports should be made about noatun. Well I have made bug reports to Charles and sometimes Neil and unfortunatly recieved a nagative response. I have either been told to stop complaing or write a patch. Well it is not a complaint but a bug report, and I am spending my time on other KDE related projects.\n\n - The impression I get on Noatun is to make lots of whizz bang plugins and effects. This is great and I admire it...but to be honest...if it keeps crashing I wont use it. The core functionality needs to be stabailised first.\n\n\n - I know aRts has its problems, but I dont believe it is the main problem with Noatun's issues. aRts needs work, but so does Noatun.\n\n - Noatun is a project with great promise, but I honestly think that until aRts and Noatun are stable enough for release either of them or both should go. At the moment...it seems Noatun should go.\n\n - Finally, I apprieciate what the Noatun and aRts developers are doing, and as a friend and a user I support them wholeheartedly, but unfortunatly we need to face facts.\n\nGood luck. :-)\n\n    Jono"
    author: "Jono Bacon"
  - subject: "Re: Hmmm"
    date: 2001-05-30
    body: "Well I never get able to play music with noAtum, it crashes every time I open a file. Neither kmidi with dosen't find my midi driver (notice xmms works fine, even for midis)\n\nAnyway the good news are:\nhttp://lists.kde.org/?t=99104529200001&w=2&r=1&n=3\n\nIf xmms gets support for KDE I will uninstall all kde-multimedia package ASAP. :))))"
    author: "Protoman"
  - subject: "Re: Hmmm"
    date: 2001-05-30
    body: "To add a tinge of fairness to this discussion, Noatun works perfectly fine for me on Mandrake 8.0.  So here's one happy user...  ;-)"
    author: "Navindra Umanee"
  - subject: "Re: Hmmm"
    date: 2001-05-30
    body: "I second this, using debian packages.\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Hmmm"
    date: 2001-06-03
    body: "I use it almost constantly to play mp3's and mpg videos (I grabbed my vhs video collection with a capture card) and I must say that It works great.\n\nI can count the number of times it has crashed on one finger!\n\nThis is with Mandrake8 and RH7.1 and Debian potato."
    author: "g0"
  - subject: "Re: Hmmm"
    date: 2001-06-04
    body: "I had problems with Noatun and Redhat for a good while.  I switched from Redhat 7.0 (where I had built KDE 2.1.1) to SuSE 7.1 with their 2.1.1. RPMs and my problems went away. Also on my work computer I still use Redhat.  Redhat 7.1 seems to work pretty well with Noatun.\n\n-Scott"
    author: "Scott"
  - subject: "konqueror supports vi keys... oh my god"
    date: 2001-05-30
    body: "After what seems like years of using Konqueror, I just now discovered that the vi keys hjkl can be used for moving up, down, left, right on a web page!\n\nOh my god!!  How come nobody told me this?  This is too cool..."
    author: "KDE User"
  - subject: "noatun may innovate"
    date: 2001-05-30
    body: "Yes, there are problems with noatun. It is less functionnal than Xmms. And I feel it is badly (ie too commonly) integrated to KDE.  So, I can't use it now... Hope someday it will change...\n\nI have a Html interface to call my mp3, either mp3 or m3u files. With Konqueror, when I put Xmms as the first reader, all is OK. When I put noatun as the first one :\n- when I click on a mp3 file, it goes in the noatun playlist, but the current song is stopped. Aaargghhh...\n- when I click on a m3u file, Xmms (the second reader) is called... Hmmm..... (instead of adding the content of the m3u file in the noatun playlist, under the current mp3 files)\n\nAlso, I think the button in the kicker bar is too light. It lacks parameters to choose what does a left click does, a double left click, a triple left click.. (see \"Notify CD player\", a windows freeware, very good). It lacks something as KNewsTicker to show the tags of the current mp3 in a child pannel extension. I feel KNewsTicker is very pretty... Something like its button-up may also be good to show the content of the playlist and jump to a file...\n\nI think it is possible to innovate with noatun. Its presence in Kicker is an advantage to explore... For instance remember some bookmarks as in a navigator... A program like Xmms is now closed in his functionnalities, but  noatun is young and may bring new ones..."
    author: "Alain"
  - subject: "KDE 2.1.1 rocks with noatun"
    date: 2001-05-30
    body: "I loved KDE 2.1.1 on RedHat 7.1; I love RH because it doesn't mess up KDE (Kicker Menus, etc)like Mandrake. \n\nArts has improved drastically, and my stupid 16 bit SB sound card rocks with KDE 2.1.1 :) Xmms without Arts plugin doesn't sound good.\n\nI really don't know how to convince David Faure for a feature called \"Overriding Default HTML colours\" like KDE 1.x kfm's \"Use Custom Colors\" (in HTML pages). He says 'Use a CSS' which I won't prefer. Why good old features are discarded?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE 2.1.1 rocks with noatun"
    date: 2001-05-31
    body: "You can disable the Mandrake menu from a pull-down menu in menudrake.\n\nI think \"override CSS\" should be a feature in KDE too, but the problem with Open and Free software projects is that everyone wants their special feature. \nkcontrol is now cluttered with options. The style/theme concept is confusing.  It is structured from a developer perspective instead of a user perspective. \nWhy is 'Allow Moving and Resizing of maximized windows' enabled by default? I can't see why anyone would want this feature at all.\n\nNotatun works fine with my AWE64 Gold in KDE 2.1.1 on both SuSE 7.0 and Mandrake 8.0. Despite functional simulartities between XMMS and Noatun, the extent of the XMMS application can't be compared with the aRts projekt ."
    author: "\u00d8ystein Heskestad"
  - subject: "i for one like this option on"
    date: 2001-05-31
    body: "why would anyone NOT want to be able to resize a maximized window???\n\n:)\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: i for one like this option on"
    date: 2001-05-31
    body: "lo jasonkb\n\nTry this one on for size...\n\nMake a konsole schema that is totally transparent.\nRun konsole full screen.\nUsing the RMB popup, diable all visible items of konsole.\nusing ALT-RMB, resize konsole to your desire.\nKill all the icons on the desktop.\nHide Kicker.\nChoose a really really nice desktop background.\n\nPresto... you now have a linux console with a kick ass background and AA fonts....\n\nGotta love being able to resize maximised windows :)\n\nTroy :P"
    author: "Troy Unrau"
  - subject: "Re: i for one like this option on"
    date: 2001-06-01
    body: "Maximize your window and open a (verically) long webpage like this one. Without using the mousewheel, scoll down the webpage without looking at the mouse. A lot of the time I miss the scrollbar and resize the window instead. :(\n\nThe outer edges are the most accessable part of the screen, this should be considered when the window is maximized. \nWhen I move the mouse to the top left corner of the screen, the window I have maximized should be closed. At least with the window decoration I am using.\nI don't think kwin's handleing of maximized windows is good. I know inferior operating systems which handles this better."
    author: "\u00d8ystein Heskestad"
  - subject: "DreamOS"
    date: 2001-05-31
    body: "Dream time ...\n\nI love KDE, BUT the BeOS is just sooooooooooo much better as a desktop and OS in general. The window manager is faster than windows 3.11 for crying out loud. I don't give a toss about the hundreds of useless gui features in KDE. I would remove themes to make KDE faster, etc. However, the BeOS is light years behind KDE as far as browser and office functionality is concerned.\n\nDream .. open-source the BeOS. Get the original developers to finish BONE (BeOS networking based on BSD) and OpenGL support (apparently faster than Windows). Port Konqueror and KOffice to Be. \n\nThis would be an unbelievable experience. \n\nWhy I want this. Leave the server stuff for servers. Give me a WORKSTATION OS, just for me, one user! I would run OpenBSD if I wanted a real server.\n\nLast night, I fired up 46 copies of a 28.2mb AVI file under BeOS back-to-back. I was still able to play two WAVE files, 70mb in total, while playing the CD player. The BeOS did not crash! How can linux ever be this good? It will never be. And that is frustrating. \n\nBeOS + Konq + OpenGL + BONE + KOffice = DreamOS"
    author: "KDEuser"
  - subject: "Re: DreamOS"
    date: 2001-06-01
    body: "Last night, I fired up 46 copies of a 28.2mb AVI file under BeOS back-to-back. I was still able to play two WAVE files, 70mb in total, while playing the CD player. The BeOS did not crash! How can linux ever be this good? It will never be. And that is frustrating. \n\nit truly is frustrating trying to figure out why you need 46 copies of the avi open at the same time."
    author: "BeOS user"
  - subject: "Re: DreamOS"
    date: 2001-06-01
    body: "it truly is frustrating trying to figure out why you need 46 copies of the avi open at the same time.\n\nBecause I was attempting to make the OS fall over. Why must I tip toe around the OS in an attempt to use it within the confines that it \"should\" be used?\n\nI'll keep using Winblows and Be and hope that KDE + Linux is better in about 6 months time."
    author: "KDEuser"
  - subject: "Re: DreamOS"
    date: 2001-06-02
    body: "troll\n\nouta sheer curiosity, I started 25 noatun's playing 25 different mp3s.. worked great - they played fine, 25 different songs at once sounded like shit tho.\n\nWas gonna try yer 46 avi thing, but i dont have any avi videos (A porn Video from Internet?)\n\n\"The BeOS\" is not better than kde unless you have a differing definition of \"better\" than most people. \"useless gui features\" - well they might be useless to a troll.\n\n\"I fired up 46 copies of a AVI\" snip \"How can linux ever be this good?\" - humm, errr, hrm well if your definition of \"good\" is - \"Can play 46 AVI's at once\" - then who cares!\n\nsweet dreams,\npleasant nightmares,\n\ng0"
    author: "g0"
  - subject: "Re: DreamOS"
    date: 2002-08-18
    body: "BeOS is an incredible OS- a better foundation and experience than ANY other modern OS.\nI LOVE linux- more for what it stands for than what/where it is developementally. Let's not be childish, we all need to unite against the evil giant of microsoft. There is plenty of room for all if the MSCancer can be controlled. KDE is slower and less responsive than BeOS, period. Konqueror is more advanced than any BeOS browser/filemanager. GET ALONG GUYS!!! \nI love you both, love eachother.\n-shaka"
    author: "shaka"
  - subject: "Re: DreamOS"
    date: 2003-01-21
    body: "Just loaded BeOS 5 PE Max Edition onto my now triple booting machine. I have to say it is by far the easiest OS to install and the boot manager is simple and direct. You programmer folks can be so picky about details and are so political and even religious about the whole thing. Use it as is or make it better already. As for me BeOS is interestingly useful for everyday computing and I don't have to play games on it for it to be valid. The part about computing and computing development in the hands of everyday users, even advanced users like yourselves should not be overlooked. BeOS is an opportunity to extend computing not a throw-away has-been recycled consumer product. Many M$guided products have flooded and captured the market. Linux as free and useful as it is has a very big backside that even KDE clothes can't cover. An OS that does everything, how complex that must be. Windows need constant caulking. BeOS until further notice is wonderful.  "
    author: "Arnold L Johnson"
---
This week's issue is momentous as it marks the growth of the editing team from one lonesome soul to a whopping two people! Rob Kaper joins the KC KDE effort this week with a summary of the kde-games list. He will also be following the konq-e list which tracks development of Konqueror for the frame buffer (non-X) and the kde-promo list. This brings the total number of lists summarized each week up to an even dozen. <a href="http://kt.zork.net/kde/kde20010525_11.html"> Check it out here...</a> (includes coverage on thesaurus support in KWord, fax support in KDE, and much more).