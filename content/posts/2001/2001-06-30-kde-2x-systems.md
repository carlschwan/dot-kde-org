---
title: "KDE 2.x Systems"
date:    2001-06-30
authors:
  - "Dre"
slug:    kde-2x-systems
comments:
  - subject: "Compiler for Sun Solaris?"
    date: 2001-06-30
    body: "Are KDE for Solaris compiled with GCC or the Solaris compilers?"
    author: "Loranga"
  - subject: "Re: Compiler for Sun Solaris?"
    date: 2001-07-03
    body: "I've built 2.1.x successfully with gcc 2.95.2 and 2.95.3 for Solaris 8 on x86.  You can read my notes at <a href=\"http://66.65.87.136/~ftp/pub/solaris/i86pc/kde/misc_notes/COMPILE.HOWTO\">solaris/i86pc/kde/misc_notes/COMPILE.HOWTO</a>"
    author: "Vladimir Annenkov"
  - subject: "Re: Compiler for Sun Solaris?"
    date: 2001-07-03
    body: "I have compiled everything up to the latest alpha\nusing gcc and encountered only minor problems\n(The most anoying one the need for -fpermissive)."
    author: "Peter Benschop"
  - subject: "Re: Compiler for Sun Solaris?"
    date: 2001-07-03
    body: "The KDE 2.1.2 packages are compiled with gcc-2.95.2 and thus 32bit."
    author: "eva"
  - subject: "Re: KDE 2.x Systems"
    date: 2001-07-01
    body: "That's a whole lotta Debian.  Go R. Krusty!"
    author: "KDE User"
  - subject: "Re: KDE 2.x Systems"
    date: 2001-07-02
    body: "They didn't include testing (Woody) into it. And thats the one that going to be interesting, specially with the freeze coming up. Hopefully KDE2.2 isn't delayed so it can enter Woody on time. KDE 2.2b1 or even a later CVS-version if I'm not mistaken has already entered Sid."
    author: "Hans Spaans"
  - subject: "Should run on more NetBSD architectures"
    date: 2001-07-01
    body: "Post this message to the NetBSD advocacy list. Od it now and you'll get probably 6 or 7 more entries. \n\n:-P"
    author: "joe_netbsd_lover"
  - subject: "Re: Should run on more NetBSD architectures"
    date: 2001-07-01
    body: "should it be linux-mandrake not mandrake linux? hrmmmmmm."
    author: "hmmm, privacy needed"
  - subject: "Re: KDE 2.x Systems"
    date: 2001-07-01
    body: "How can I install this on my Amiga 500? I have the 512k expansion card so memory shouldn't be a problem."
    author: "Guru"
  - subject: "Re: KDE 2.x Systems"
    date: 2001-07-01
    body: "You need the expansion memory to be chipmem, not fastmem. It is needed to support the transparency effects."
    author: "Per Wigren"
  - subject: "Re: KDE 2.x Systems"
    date: 2001-07-02
    body: "You will also really need an external disc drive. KDE 1.x works fine from a single 880k floppy, but I found 2.x requires a lot of disc swapping with just one drive. With Workbench 1.2 in the internal drive and KDE in the external it just flies!"
    author: "Dan"
  - subject: "Re: KDE 2.x Systems"
    date: 2001-07-03
    body: "Wow! I thought this KDE-on-Amiga was a joke until so many serious replies popped up to the first question!\n\nCould you please tell me how it is possible to run KDE in less than 1MB of memory (RAM)? And how can it be that it \"flies\" on such an old computer when KDE sometimes crawls on a rather new Pentium-calss system?\n\nIs this a special KDE-for-Amiga version or did I underestimate the old Amiga so badly?"
    author: "me"
  - subject: "Slackware 8.0"
    date: 2001-07-01
    body: "Slackware 8.0 is out, this should replace the 7.2 (current) entry in the list.\n\nIt comes with KDE 2.1.2 (ftp://ftp.slackware.com/pub/slackware/slackware-8.0/slakware/kde1)\nand it will compile current CVS (thus 2.2 beta1) just fine. Haven't installed it yet, but I can depend on Slackware and am willing to take bets it does."
    author: "Rob Kaper"
  - subject: "PowerPC'ers, unite!"
    date: 2001-07-02
    body: "KDE has always run beautifully on PowerPC. I submitted a report for LinuxPPC on the 604e and G3 but I know there's a lot of other people running Debian and SuSe for PowerPC out there. Let's hear from all you guys, as well as anyone running on a G4!\n\nBy the way, this is a great move on Andreas' part. KDE's portability is extremely underrated because of its lack of official links with commercial Unixes, but it actually runs far better out of the box on Solaris and HP-UX than does the new official desktop for those platforms. (I'll forgo commenting on why I'm not shocked that paying the project leader's company to manage cross-platform compatability results in less portability in the main source tree, not more. ;-) ) Making it prominent and clear what platforms can run KDE and how to do it is going to be a great PR boost."
    author: "Otter"
  - subject: "Re: KDE 2.x Systems"
    date: 2001-07-02
    body: "KDE 1.1.2 also runs on Windows, albeit in an alpha-quality form. Work on porting KDE 2.0 run seems to be making good progress.\n\nSee http://cygwin.kde.org\n\nI'm quite keen to see this happen. It would be no use to me personally, but it's a challenge and I'm sure people can think of reasons why (or why not) this should be done.\n\nCurrently I'm wondering whether KDE should be ported to Qt-NonCommercial ('natively') or whether using the cygwin layer is the best idea.\n\nUsing cygwin would make the port easier and also allow us to avoid having to maintain so much cross-platform code, however using Qt-NC would mean no compatibility layer (perhaps slightly faster - though I can't imagine cygwin adds much) and also no X server needed.\n\nPerhaps it might be possible to have XFree-Cygwin 'detach' windows ? That would allow KDE apps to run 'inside' the Windows desktop - just like apps using Wine can on Linux.\n\nRik\n\np.s. Major kudos to the talented hacker ('habacker') who has obviously been working very hard on this project."
    author: "Rik Hemsley"
  - subject: "Re: KDE 2.x Systems"
    date: 2001-07-02
    body: ">Currently I'm wondering whether KDE should be ported to Qt-NonCommercial ('natively') or whether using the cygwin layer is the best idea.\n\nWhy not both?  QT/Non-Commercial is obviously the better choice for display as it can co-exist w/ normal Windows apps on the same desktop, and it's probably faster too.  However, the Cygwin libraries  would provide a Linux-like environment for the things that KDE does that are unrelated to QT (Cygwin is _much_ more than an XFree port) and therefore reduce the porting needed.  Best of both worlds!\n\nIf someone ported KWin too, KDE could form a complete shell-replacement, like Litestep.  And if KDE started catching the attention of the Litestep-using crowd, it could only be a good thing.  For one, it would mean a HUGE increase in the number of available, good themes ;-)"
    author: "not me"
  - subject: "Re: KDE 2.x Systems"
    date: 2001-07-02
    body: "Gygwin gcc has one restriction: all program produced with it must be GPLed. So you can't distribute a binary version of konqueror that was compiled this way, with Qt-NC, because you can not change the license of konqi to add exception for Qt. We are back to license nightmares.\n\nHowever, mingw32 doesn't have this restriction. But you must first convince trolltech to release a QT-NC for mingw32 and cygwin. Currently, they only provide binary for msvc.\n\nI would love to use Konqi and KOffice at work, under windows."
    author: "Philippe Fremy"
  - subject: "Re: KDE 2.x Systems"
    date: 2001-07-03
    body: ">Gygwin gcc has one restriction: all program produced with it must be GPLed. So you can't \n>distribute a binary version of konqueror that was compiled this way, with Qt-NC, because you \n>can not change the license of konqi to add exception for Qt. We are back to license nightmares.\n\nNot true.  Cygwin is not licensed under the GPL per se, it is licensed under a modified GPL which specifically allows linking of libcygwin1.a to Open Source programs without requiring that those programs be licensed under the GPL.\n\nhttp://cygwin.com/licensing.html\n\nPlease don't spread such misinformation.  I love KDE, and I develop for Cygwin/XFree86, and I'd really hate to see KDE on Cygwin held up because people hadn't read the Cygwin license carefully :)\n\nHarold"
    author: "Harold Hunt"
  - subject: "off topic but still cool pls forgive"
    date: 2001-07-02
    body: "I came across this today. Looks like another Linux desktop site has popped up. Another place for us to hang out between dot stories. Its at http://GUI-Lords.org\n\nCraig\n\nplease forgive my off topic post. I know it would'nt warrent a full story."
    author: "craig"
---
Yesterday I started putting together
a <A HREF="http://promo.kde.org/kde_systems.php">list of operating
systems/distributions and architectures</a> on which KDE 2.x compiles and runs.  It is far from
complete but already lists three BSDs, eight Linuxes and four other
Unices, as well as nine architectures.  If you know of a system that is not listed, please help us complete
the list.  Instructions for contributing are
<A HREF="http://promo.kde.org/kde_systems.php">here</A>.


<!--break-->
