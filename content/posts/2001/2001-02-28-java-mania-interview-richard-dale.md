---
title: "Java Mania: An Interview With Richard Dale"
date:    2001-02-28
authors:
  - "rmoore"
slug:    java-mania-interview-richard-dale
comments:
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "Way too cool!\n\nCongratulations Richard! I knew that week I once spent on QtC would be useful somehow even if only as inspiration to a much better idea :-)\n\nActually, if you think about it, this work you did is most of the work to bind ANY language to Qt and KDE.\n\nOnce the C binding is good, any language can bind to it easily. \n\nAnd if your language supports binding to C++, then you can do a direct one.\n\nCome on Pascal/Ruby/Sather guys, have fun! :-)"
    author: "Roberto Alsina"
  - subject: "Re: bindings to other languages ..."
    date: 2001-02-28
    body: "Well, with Kylix on the very verge of actual distribution, that sounds pretty interesting ... because Linux will get an awfull large number of new programmers anxious to port their code and relate to the new environment"
    author: "bPoterie"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "Thanks Roberto - your QtC stuff was the start point - it would have been very difficult to get going without it,as I didn't know anything about Qt programming at first.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "Well, QtC was described by someone whose coding I respect as \"an incredibly ugly abortion that should never have happened\", so I hope you didn't take the code in it too seriously ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-02
    body: "Well - you could use similar language to describe my perl! As long as the api you generate is pretty, perhaps it doesn't matter if the 'scaffolding' is a bit scruffy.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "So as I understand it, QtC is completely dead now?  The last checkin, according to cvsweb, was 3 years ago...  so, i doubt it will even build?"
    author: "dimator"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "QtC pretty much was born dead :-)\n\nI am pretty sure that the current sources could almost build, as soon as you take care of all the QString strangeness (it's Qt 1.33 code ;-)\n\nI really wanted to keep it up to date, but noone seemed to actually want to CODE using it (I got perhaps 3 mails in 3 years :-)\n\nSo, I let it rot as a proof of concept.\n\nThen came Richard, and the rest is history, since a good QtC (the name is cool, please keep it ;-) should be now easier than ever."
    author: "Roberto Alsina"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-02
    body: "But 'Son of QtC' is going well, but isn't quite ready for the KDE cvs yet. I can send a version of C bindings for Qt 2.2 as a 800k email attachment if anyone is interested. I'll wait to see how much bug fixing etc needs to be done with the Java, then if that seems stable, I'll start on the C/Objective-C again.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "So when gcj matures, we'll be able to write natively compiled kde apps in java? VERY cool! While it may not be as capable as C++, java is a lot of fun to code (imho, of course). Funny thing is, I'm currently taking a java class and an intel assembly class (definitely two extremes), and I enjoy them both more than C++. I have my reasons, most of which amount to personal preference, so I won't start a language war ;) Besides, I would enjoy coding C++ if I had nothing else, and I certainly enjoy it more than C. I should shut up and put on my flamesuit :)\n<p>\nSeriously though, KDE2 + Qt + gcj would be amazing!"
    author: "kdeFan"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "JNI wrapper of Qt would be about 10 times cooler."
    author: "0"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "The bindings use lots of JNI, and JNI is good."
    author: "Richard Dale"
  - subject: "More info on the bindings"
    date: 2001-02-28
    body: "The developer site seems to having trouble (a result of the number of people downloading 2.1), but you can still see the docs I referenced via CVS. If you don't have CVS access then you can use the webcvs to view them.\n<p>\n<ul>\n<li><a href=\"http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/developer.kde.org/language-bindings/java/qtjava.html?rev=1.1&content-type=text/html\">KDE Java bindings info</a>\n</ul>\nYou can also use this interface to look at the files in the kdebindings module itself it you like."
    author: "Richard Moore"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "I'm sure Java is a great language and Qt and KDE are great libraries, but why would you want to write nonportabe KDE apps in java when you can write completely portable apps in swing.  This is a thing that java was meant to get rid of.  I can't think of the word that describes something like making non-portable java?"
    author: "John"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "Think of Java-the-language, not Java-the-platform. Admittedly, portability is one of Java's (language and platform) main reasons for existing, so it does sound silly to take away that advantage. But people, myself included, have found that they enjoy Java-the-language and would like to use it in situations where they would otherwise use C++. When the desire for a native KDE interface outweighs the desire for perfect portability, this development becomes very useful. I can do what I would with C++ in a language that I prefer.\n<p>\nIn my view, this would really only be attractive if you could compile Java to a native executable just like C++ (etc.). Fortunately, the good folks of the gcj project are working on a compiler that turns java source or bytecode into native machine code. That gives me the choice of using Java as a hardware-independent platform when I want, and as a nice high-level compiled language when I want. If the jvm platform was suitable for all situations this wouldn't be necessary, but sometimes native binaries are preferable... particularly when speed is an issue or a native gui is required (which is what this development addresses).\n<p>\nJust my $.02"
    author: "kdeFan"
  - subject: "Read my mind?"
    date: 2001-02-28
    body: "You must have read my mind! This is probably the best KDE news since KDE 2.0! Java language is great, but not the platform, but with gcj, this will not matter.\n\nKDE + Java + gcj = COOL!\n\nAnyone who knows the state of gcj?"
    author: "Stentapp"
  - subject: "Re: Read my mind? [GCJ]"
    date: 2001-03-01
    body: "I would be nice if we got this to work under <A href=\"http://gcc.gnu.org/java/\">GCJ</A> / CNI. I really like Microsoft's J++ (sorry) for the VB/KDevelop look and easy of use, a Qt/GCJ library could be the first viable competitor, IMHO. <A href=\"http://gnome-gcj.sourceforge.net/\">Gnome-GCJ</A> is a distant third."
    author: "george moudry"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "Then put a JNI wrapper around Qt"
    author: "0"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "A jni wrapper would be nice so that non-native apps could have a native interface, true. Personally, I'm not interested in writing for the jvm, but I do like what I've seen of java and would like to use it as a typical hll. For my purposes, the wrapper mentioned above is perfect. JNI isn't really what I want, because the jni is intended to interface Java with native code. I want the java I write to *be* native code, hence a wrapper around qt is all I need. (I'm probably going to offend a lot of java people by saying that ;)\n\n-kdeFan"
    author: "kdeFan"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "A jni wrapper would be nice so that non-native apps could have a native interface, true. Personally, I'm not interested in writing for the jvm, but I do like what I've seen of java and would like to use it as a typical hll. For my purposes, the wrapper mentioned above is perfect. JNI isn't really what I want, because the jni is intended to interface Java with native code. I want the java I write to *be* native code, hence a wrapper around qt is all I need. (I'm probably going to offend a lot of java people by saying that ;)\n<p>\n-kdeFan"
    author: "kdeFan"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "What he said + Motif sucks very much!  Much better to have themable Qt, and no GTK+ is not an answer.\n\nAnd also portable Java is very slow, but the language is very nice!  So you can get the best of both worlds.  Nice java language + Native performance.  Except that you'll need gcj to get full native performance, I think."
    author: "KDE User"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "If you have alot of money you can buy native compilers from IBM.  Native compiled swing is very fast.  Swing is themeable, it probably wouldn't be to hard to make it look like your KDE theme, I believe that GNOME 2 will have this, but I could be wrong.  Also Swing is a very good API, and  Qt, thin wrappers for Gtk and Motif are not the only toolkits for X."
    author: "John"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "I wonder what happens when The KDE GNOME-theme-wrapper is pointed to the GNOME KDE-theme-wrapper is pointed to the KDE GNOME-theme-wrapper is pointed to the GNOME KDE-theme-wrapper is pointed...."
    author: "Silly"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "I wonder what happens when The KDE GNOME-theme-wrapper is pointed to the GNOME KDE-theme-wrapper is pointed to the KDE GNOME-theme-wrapper is pointed to the GNOME KDE-theme-wrapper is pointed.... <BR>\n  <i>(Something like this)</i><br>\n  continued on parent, please read all responses to the parent as well, and follow all directions."
    author: "SomeOtherGuy"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "Qt and JNI are excellent cross platform technologies. So the Qt side of the api is cross platform."
    author: "Richard Dale"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "Richard, thanks for your kind contribution to the KDE project. I am currently in the process of learning Java, and your work has given me extra incetive to learn.\n\nIt is work like this that inspires others to contribute, and which makes KDE such a fantastic desktop and project to be a part of.\n\nCongratulations. :-)"
    author: "Jono Bacon"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "Isn't this illegial(as proved in MS vs SUN)."
    author: "Luke"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "No, but what's really ironic is that Sun is supporting that <i>other</i> project while KDE/Qt has already gotten fully fledged Java bindings, no thanks to Sun.  =)"
    author: "KDE User"
  - subject: "Re: That other project GNOME supports Java"
    date: 2001-02-28
    body: "Loot at this\nhttp://news.gnome.org/gnome-news/973860170/index_html"
    author: "Hajdi"
  - subject: "Re: That other project GNOME supports Java"
    date: 2001-02-28
    body: "..and also http://gnome-gcj.sourceforge.net/\n\n /mill\n"
    author: "mill"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "<p>One thing I would note is that Swing isn't as bad as you make out - there are three main problems - one is that there's a lot of very badly written code out there, thanks to \"Java applets for dummies\" books and the like, another is that people persist in using (definitely flawed) AWT components, and the third is that people are still using the 1.1 swingall.jar swing instead of 1.3 swing, thanks to MS and Netscape faffing.</p>\n\n<p>The Beans/Swing event handling model is very similar to signals and slots to program (although not in its actual implementation), and I assure you that _my_ swing code is only a little more verbose than Qt code for equivalent functionality,\nand that reflection-based at-runtime eventlistener connections are possible, and easy to do. There is stronger type-checking on java's events than Qt signals, though, so you have to explicitly deal with different exceptions when you do dodgy things.\n</p>\n<p>\nThat's not to say I don't prefer Qt programming though! - just that swing is actually not that bad a toolkit (better than programming Gtk in C, anyway, although Gtk--, again, isn't all that bad - I dislike Qt's moc, especially since new C++ compilers have sufficient template support to render it obsolete...), provided you learn it from a decent, i.e. O'Reilly or Sun published, book.\n</p>"
    author: "Silly"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "All the NeXTSTEP/OpenStep guys I know haven't been very impressed with Swing. I don't like to use toolkits whch are just 'not bad' - I think that's the only thing we might disagree on. No moc is needed for Java Qt of course."
    author: "Richard Dale"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "This is indeed excellent! I will try to make use of this in my university projects. It's also a great way to let new programmers write KDE applications without having to learn the (in comparison rather complicated) C++ Syntax."
    author: "gis"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "Hmm... Python syntax is, in many people's opinion, easier than either Java or C++, and Python bindings are already here."
    author: "Silly"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "I wanna Forth binding."
    author: "reihal"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "nice one"
    author: "ac"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "don't you mean 'I Forth binding wanna'?"
    author: "teferi"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-02-28
    body: "Too bad GNOME has JavaGtk <B>and</B> an implimentation under JNI.  But KDE people like to do stuff like integrate browsers into there desktops, use nonportable APIs and component technologies, push nonportable java and FUD the competition.  Of course they also do stuff like DoS attacks on google, even though Ximian took there adds down, but the addwords were bought by a free speach advocate."
    author: "Luke"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "Fsck!<br>\nWhat is it with all these GNOME trolls on this site recently???<br>\nNon-portable? Last time I checked KDE compiled under Linux, all *BSD, Solaris, Aix, HP, etc.<br>\nAll the components and protocols are open and non-proprietary.<br>\nAnd unlike Ximian they are free.<br>\nGo away.<br>"
    author: "Jacek"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "While I agree that the guy was a troll, Ximian sells only GPL/GPL compatible(BSD etc) liscensed software.  Also X11 apps have little market share for desktop software and probably never will, more likely embedded and also eventualy better protocols like D11 or Berlin(though probably not berlin), that TrollTech probably won't support until it's profitable, while java will run on windows, X11, BeOS, QNX, embedded and more, and you don't even have to recompile."
    author: "KDE-r0XoR$"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "yea but look at Microsoft there the only people with a better operating system than KDE and KDE is only about 3 years behind once you can stop worying about the competitionn you can start doing codeing.  and gnome is not the anwser it sucks because its ugly and other stuff it dosent have even a web browser.  plus Ximain tries to trick people to buy free software it proobably almost impossibe to get a free versiom of Ximain GNOME, the olny way to get them probably is by buying them.  But places like theKokpany have the software on there site, and trolltechs toolkit is truly free and it uses objcets,  Quit your postin g you retard."
    author: "KDEguy"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "<I>\"Quit your postin g you <B>retard</B>.\"</I> <BR>\nWow maybe you shouldn't think about baseball players while you post.  <BR> <BR>\nThis post dosen't help make KDE look any better, only worse."
    author: "Luke"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "<i>But KDE people like to do stuff like integrate browsers into there desktops, use nonportable APIs and component technologies, push nonportable java and FUD the competition.</i>\n<p>\nI'm sure you can remove the khtml kpart if you want and nobody at kde would say it was technically impossible or against their right to innovate. Those \"non-portable\" APIs and component technologies are free software. Feel free to implement them wherever you desire. The KDE coders build technology that suits their needs, that they (and I) consider to be a good solution to the problem and that's fun to work with. Why does it make you angry that they chose to write code the way they want to? You don't have to use it, you don't have to pay for it, you're free to completey ignore it, and I assure you that they didn't undertake this project with the aim of offending you. Please show them and the readers of this forum at least a basic level of respect."
    author: "kdeFan"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "After QtC will there be a lisp binding?"
    author: "KDE-r0XoR$"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "Yes and prolog.. But how solid is KDE's undo code - it could be hammered ;-)"
    author: "Richard Dale"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "Why did you do a thin wrap of Qt and not do a JNI wrap, would have made alot more sense and all those Swing apps would look cool, not to mention the portability."
    author: "0"
  - subject: "Swing callbacks?"
    date: 2001-03-01
    body: "One thing I would love to see this used for is a Swing callback to render the components for a custom look and feel.  This would be similar to the Mac OS X Aqua look and feel in which the Java code calls back to the native code to draw the user interface elements.\n\nThat would be very nice since all java swing applications (and there are some good ones!) would automatically be consistent with the native desktop theme."
    author: "Sean Reilly"
  - subject: "Why Java or Objective C Bindings are huge."
    date: 2001-03-01
    body: "Okay, you want to push a desktop environment and useful applications, time for an unholy alliance...  Apple and KDE.\n<P>\nApple is sitting, IIRC, at around 8% of the desktop, and Linux/Unix around 4%.  I figure that KDE has at least half the Linux/Unix desktop market, so combined that is 10% of the desktop.\n<P>\nThis kind of alliance is huge, because a MUCH higher percentage of Linux users are programmers than Apple users, while Apple brings a marketshare and a marketshare that will pay for commercial applications.\n<P>\nIf you can create a common language for developing MacOS X applications and KDE applications, you now have a competitive desktop standard.\n<P>\nJava has the potential, Mac is pushing Java, hard.  KDE pushing Java hard would also create this alliance.  GNOME will never push Java, but pushing a REAL KDE-Java solution (this is a start of course...) will let you combine those markets and create something interesting.\n<P>\nAlternatively, get Trolltech to port QT to Cocoa, and port the KDE Libs to Cocoa, so that you can build KDE/MacOS X applications in the KDE Library space that will run natively on both...\n<P>\nJust a thought...\n<P>\nAlex Hochberger<BR>\n<a href=\"http://www.feratech.com\">Feratech, Inc.</A>"
    author: "Alex Hochberger"
  - subject: "Re: Why Java or Objective C Bindings are huge."
    date: 2001-03-01
    body: "<I>GNOME will never push Java</I> <BR>\nGNOME has bindings and a JNI layer so that swing can use the native Gtk.  I personaly prefer KDE but Java support is one of those things that GNOME is way ahead in, and with SUN backing you can expect alot of GNOME Java support in the future.  If you want to push a portable toolkit use swing and do a JNI wrap thats what it was made for.  Also theres a library called wxWindows that looks kinda like MFC that can do Gtk, OSX, Win32, BeOS and theres a Qt port being worked on."
    author: "SomeOtherGuy"
  - subject: "Re: Why Java or Objective C Bindings are huge."
    date: 2001-03-01
    body: "Let me rephrase.  While GNOME has a JNI layer, GNOME programmers/users/FSF-types/slashdotters will NEVER push GNOME.  :)"
    author: "Alex Hochberger"
  - subject: "Re: Why Java or Objective C Bindings are huge."
    date: 2001-03-02
    body: "Your rephrasing made you make even less sense, I can't tell what you mean, all I can get out of this is that people will never push the GNOME platform, which dosen't realy make sense.  Sorry but can you please clarify?"
    author: "SomeOtherGuy"
  - subject: "Re: Why Java or Objective C Bindings are huge."
    date: 2001-03-02
    body: "Sorry, what I was trying to get at was that the people pushing GNOME (users, loudmouths, etc.) seem to have this hatred of Java.  Part of this is political (GNOME started because of a dispute over QT licensing; while the real reason was RMS's admirable goals of a GNU system require a fully GPL'd solution, preferably under FSF's copyright), part is technology (GNOME/GTK is based upon C, and part of it was complaints about C++ and Object Oriented programming in general, if you wanted to build OO Apps, you'd be using KDE/QT before GNOME/GTK.\n\nThe point I'm getting at is the anti-Java, anti-OO, pro-GNOME faction, which is a REALLY loud vocal minority, will never make Java for GNOME seem like a Kosher option.  They'll be major bitching and whining about anything significant being Java based, and how C was the epitomy of computer knowledge.\n\nDoes that make more sense or should I respond again?"
    author: "Alex Hochberger"
  - subject: "Re: Why Java or Objective C Bindings are huge."
    date: 2001-03-10
    body: "Thanks for clearing but, but I wouldn't say anti-OO, if you have ever seen Gtk."
    author: "Robert"
  - subject: "Re: Why Java or Objective C Bindings are huge."
    date: 2001-03-10
    body: "Thanks for clearing but, but I wouldn't say anti-OO, if you have ever seen Gtk."
    author: "Robert"
  - subject: "Re: Why Java or Objective C Bindings are huge."
    date: 2001-03-02
    body: "I'm sure Qt already runs on Mac OS X (because it's *BSD), but only under X-Windows. So it would be nice to have native Quartz widgets for Qt, as well as Aqua look alikes (if Apple allows it) for X. With X seamlessly integrated and running under Quartz. I'm looking forward to finishing the Objective-C bindings + KDevelop 2.0 support anyway, it should a nice combo.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Why Java or Objective C Bindings are huge."
    date: 2001-03-02
    body: "Oh absolutely.  The QT under X-Windows is a nice option, as every Unix geek will buy a commercial X Server or use the XonX from the XFree port.  However, in my ideal world, I'd like native Aqua widgets.\n\nMy reasoning is that having QT support on OS X through X Windows means that I can run my Unix apps on my GNU/MacOS box, but it doesn't fulfill the ideal.  The ideal approach is that as a software developer, I can target both platforms with one application, which means that the combined KDE/MacOS X desktop market may be a reasonable market to target, even if the KDE, of MacOS markets aren't.  If we could get GNOME to do the same (port GTK to MacOS X Aqua/Quartz), then MacOS becomes the peace-maker.  An API that can target GNOME, KDE, and MacOS X results in a real market to target, and a growing one at that.  Even if MacOS X doesn't bring Mac more marketshare, they will maintain their current 8%(?), which will be more than Unix/Unix-like desktops for 2-3 years minimum, and will be a significant market for at least 10...\n\nThink another way, as a programmer, I can build Free Software available for download without any support (including pretty GUI installers, etc.) and even make it available on my web site.  I can make a commercial package for MacOS (under GPL if I want) that I charge to download from my site.  MacOS users are much more likely to buy the package, so I can build free software and still earn a living.\n\nThat's why a KDE/GNOME/MacOS-X alliance is key.  It means revenue from Mac users can finance the development of good free software for KDE/GNOME/Unix geeks using MacOS-X."
    author: "Alex Hochberger"
  - subject: "Re: Why Java or Objective C Bindings are huge."
    date: 2001-03-11
    body: "<A HREF=\"http://www.wxWindows.org\"> http://www.wxWindows.org </A>"
    author: "Robert"
  - subject: "Re: Why Java or Objective C Bindings are huge."
    date: 2001-03-11
    body: "<A HREF=\"http://www.wxWindows.org\"> http://www.wxWindows.org </A>"
    author: "Robert"
  - subject: "Re: Why Java or Objective C Bindings are huge."
    date: 2001-03-11
    body: "Go to www.wxwindows.org"
    author: "robert"
  - subject: "Re: Why Java or Objective C Bindings are huge."
    date: 2003-09-01
    body: "Eh, QT already works natively under osX. "
    author: "Jack"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "This is way too cool. Is there going to be a site or something that <I>sample</I> Java code would be posted to show how to take advantage of this binding?"
    author: "Dave"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-01
    body: "Look at the ScribbleWindow.java file attachment to the top left of the main article."
    author: "ac"
  - subject: "Thanks Richard!"
    date: 2001-03-02
    body: "This is absolutely fabulous :-).\n\nJava has been my favourite language for a while but writing desktop apps that don't integrate well into desktop has been pointless. Now this is all changed.\n\n-- \n  jannek"
    author: "Janne Kylli\u00f6"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-02
    body: "Gee... a non-portable Desktop specific Java...\nWhere have I heard that before?\nOHHHH Bill strikes again!!!!"
    author: "Stephen McConnell"
  - subject: "Re: Java Mania: An Interview With Richard Dale"
    date: 2001-03-11
    body: "You forgot to embedded Konqueror, KPARTS and a proprietary API."
    author: "000"
---
One of the most fascinating aspects of KDE is the speed with which new developments 
  occur after each release. KDE 2.1 came out <A HREF="http://dot.kde.org/983227586/">yesterday</A>, and was celebrated in 
  the traditional manner by adding lots of new features to the KDE development tree 
  in CVS. For me, one of the most exciting things to arrive was a very large commit 
  to the <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/">kdebindings</a> 
  module (used for bindings between KDE's C++ code and other programing languages) 
  which added bindings for the KDE and Qt libraries to the well known <a href="http://java.sun.com/">Java</a> 
  language. The author of the code, Richard Dale, has kindly agreed to answer 
  some questions so that readers of the Dot can get up to speed with this new 
  addition to KDE.




<!--break-->
<h4>1. What do QtJava and KDEJava actually do?</h4>

<p>They wrap most of the classes in Qt and KDE so that they can be used to write 
  Java programs, which look and behave identically to a C++ version. The Qt date, 
  time, string and list classes are replaced with their Java equivalents.</p>

<h4>2. What would you say are the best features of the binding?</h4>

<p>It uses <A HREF="http://www.ph.unimelb.edu.au/~ssk/kde/kdoc/">KDOC</A> as the parser engine, so it has access to the documentation comments of
  the KDE methods. It translates these comments into <A HREF="http://java.sun.com/j2se/javadoc/index.html">Javadoc</A> format, and has a go 
  at translating the code snippets into 'cod java' - ie C++ with those funny symbols 
  like '&amp;', '*', '::' (that you never quite mastered ;-) ) replaced with java 
  style '.'s. </p>

<h4>3. What made you decide to write this, and how long did it take?</h4>

<p>I wrote some code to patch <A HREF="http://www.kdevelop.org/">KDevelop</A> and KDOC 1.0 to use Objective-C for gnustep 
  development in late 1999. After I'd done that, someone posted a question to 
  comp.lang.objective-c asking about whether there were any Objective-C bindings 
  for Qt. I thought that if I could do some bindings, I'd have a really nice complete 
  Objective-C programming environment. </p>

<p>I started using Roberto Alsina's QtC for the C bindings and shell scripts for 
  the Objective-C side (you can't call C++ directly from Objective-C, so you have 
  to do two sets of bindings). I completed a prototype version good enough to 
  run Qt Scribble in Objective-C in about 3 months. But the work was very labour 
  intensive and I couldn't see how I could manage to get the whole Qt/KDE api 
  done.</p>

<p>I wrote a patch to add Objective-C support to kdoc 2.0, so I got familiar with 
  the Perl code. Mathieu Chouinard emailed me asking 
  about how work on the Objective-C bindings was going. I told him I'd started 
  to hack Roberto's Python scripts, but it was still taking a lot of work to get 
  anything wrapped. Then it occured to me that it would be easier to make KDOC 
  generate the same code as the python stuff, than it would be to make the Python 
  parse as well as KDOC. After two months (less time than the prototype took), 
  we had two KDOC-based converters, for C and Objective-C, for Qt 2.2 wrapped and 
  working, and I was about to start on KDE 2.0.</p>

<p>But once I realised I could the same thing for Java/JNI at the end of October 
  or so, I just downed tools on Objective-C and ran for it. Er.. 24 by 7. I haven't 
  still been programming when the birds start singing since late 1988 or so! I 
  got Qt 2.2 done before Christmas, then had six weeks off starting again in February. 
  So KDE 2.0 has taken a bit less than a month to get this far.</p>

<p>Probably about a man year of 40 hour weeks for C/Objective-C/Java in total.</p>

<h4>4. Who do you think will find this code most useful?</h4>

<p>One group might be those people who would like to write apps with full Qt or 
  KDE look and feel, but prefer to code in Java over C++ (because it's quicker). 
  Another group might be those taking existing Java code and wrapping it to get 
  a fully functional KDE app. A third group might use the Qt side to write apps 
  which will run cross-platform, both on standard Wintel PC's (a port to Windows 
  should be straightforward) and Linux, *BSD, etc.</p>

<h4>5. How much of the Qt and KDE APIs have you covered?</h4>

<p>280 Qt classes and over 400 KDE classes - pretty complete.</p>

<h4>6. How hard do you think it will be to extend the binding to cover the new 
  features in KDE 2.1?</h4>

<p>The manual edits applied to get the bindings working are in the form of a 
  patch. So the KDE 2.1 headers need to be put through the converter, then the 
  patch applied. Finally, the diffs added to the KDE CVS since the initial check-in 
  must be applied, and any essential new 2.1 classes added to the project.</p>

<h4>7. How does the signal/slot mechanism of Qt translate into Java?</h4>

<p>Here is an example of how to connect a signal to a slot in QtJava:</p>

<pre>
 QObject.connect( _colormenu,
                  SIGNAL( &quot;activated( int )&quot; ),
                  this,
                  SLOT( &quot;slotColorMenu( int )&quot; ) );
</pre>

<p>If the underlying C++ instance of '_colormenu' has an 'activated(int)' signal, 
  the Java slot is connected to that. Otherwise, the runtime assumes that you 
  want to connect to a Java signal and it connects the slot to that instead.</p>

<p>Emitting a signal looks like this:</p>

<pre>
 emit(&quot;colorChanged&quot;, darkGreen());
</pre>

<h4>8. How does this compare with AWT and Swing?</h4>

<p>A very interesting question! I've never done any AWT or Swing programming (my 
  background is Objective-C with NeXTSTEP). It doesn't appeal to me because the 
  subclassing and managment of event listeners looks clunky and doesn't make use 
  of Java's dynamic runtime. Swing code appears to need 5 times as many classes 
  and twice the bulk, yet offers less functionality, compared to Apple's 
  Cocoa or Trolltech's Qt.</p>

<h4>9. How hard is it for developers to add Java support for their own Qt widgets?</h4>

<p>The KDOC converter is included in the Koala KDE bindings project, so they can 
  run their widget headers through that and convert them. One day per widget or 
  so would be my estimate</p>

<h4>10. Could this code be used to embed Java code in a C++ KDE application?</h4>

<p>Yes, you can mix C++ and Java. The objects don't always have to be instantiated 
  from within the Java environment. If you allocate an object instance on the 
  C++ side, and then you refer to it from within Java, a Java version is created 
  automatically. However, when the Java version is subsequently garbage collected, the 
  C++ instance isn't freed by the Java runtime. It would still exist over in the 
  C++ environment.</p>

<p>However, if you instantiate an instance on the Java side with a 'new' command 
  then the C++ is freed on Java garbarge collection.</p>

<h4>11. Where do you plan to go from here?</h4>

<p>I plan to finish the Objective-C and C bindings for KDE, finish adding Objective-C and 
  Java support to KDevelop 2.0 and contribute bug fixes/enhancements to the code 
  just released.</p>

<h4>12. Is there anything else you'd like to say?</h4>

 <p>I haven't given the KDOC author a namecheck - which I think he
 deserves.  So a big hand to Sirtaj Singh Kang <taj@kde.org> from me
 is deserved. Also I forgot to mention there is a patch to the <A HREF="http://www.trolltech.com/">Qt Designer</A>
 uic tool to generate Java from exactly the same .ui files as C++.
</p>

<p><i>I'd like to thank Richard for creating this cool new binding,
and for finding the time to answer my questions. You can get the code
by <a href="http://www.kde.org/anoncvs.html">checking out the
kdebindings module</a> from the CVS; it's not included in the packages
for KDE 2.1 because it's so new. I have added some information to the
<a href="http://developer.kde.org//language-bindings/java/qtjava.html">Java language bindings</a>
section of the developer site, including an <a href="http://dot.kde.org/983311036/ScribbleWindow.java">example</a> of using the
bindings for people to have a look at. People interested in using
Java with KDE might want to join the
<a href="mailto:kde-java-request@kde.org">kde-java mailing list</a>.
</i></p>



