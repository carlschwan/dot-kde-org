---
title: "Proposed Timetable for KDE 3"
date:    2001-07-24
authors:
  - "Dre"
slug:    proposed-timetable-kde-3
comments:
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-24
    body: "There is a saying ...\n\nVision without action is fiction\n\nThree cheers for KDE for keeping me in the NON-fiction part of the library :)\n\nOn a side note .. the funniest thing about the new KDE site www.freekde.org is the hyperlink which reads Humour. The link points to gnome.org. \n\nNow THAT was funny!"
    author: "kde-zealot"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-25
    body: "... and :wq links to vim.org"
    author: "philiKON"
  - subject: "i don't believe it"
    date: 2001-07-24
    body: "my god.\n\n6 months until KDE3.0?!\n\ndon't you people have lives?!?!\n\nsheesh.\n\n8*]\n\ncan't wait for it.\n\n--john"
    author: "john"
  - subject: "Migration"
    date: 2001-07-24
    body: "How about KDE2 programs? Will KDE3 be source comaptible with KDE2? Will I/O slaves written for KDE3 also work with the KDE2 programs or do you have to maintain a separate set of I/O slaves? \n\nI'm looking forward to KDE3, but I hope some attention will be given to backward compatibility with KDE2..."
    author: "Konrad Wojas"
  - subject: "Re: Migration"
    date: 2001-07-25
    body: "I don't expect the I/O slave system to change except for a very small cleanup.\n\nFor the rest it's hard to tell, the porting hasn't been done yet.\n\nThe plan is _not_ to re-engineer everything as happened during the 1.x->2.x switch, so the porting should be quite easy.\n\nDon't expect binary compatibility though, recompiling will be in order."
    author: "David Faure"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-24
    body: "what features will kde 3 have???"
    author: "M. Khawar Zia"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-24
    body: "If you want a lot of details you can read the thread at <a href=\"http://www.kde.com/maillists/view_thread.php?f=1&l=20&sb=rank&o=d&v=search&mq=35987&t=494946&m=494972\">http://www.kde.com/maillists/</a> \n<br><br>\nThe executive summary:  the intiial kde 3 release will just be a port of KDE 2.x to Qt-3, with the usual set of feature enhancements that accompanies a KDE major release (e.g., like KDE 2.0 to KDE 2.1).\n<br><br>\nQt-3 offers some significant advantages over Qt-2, including bi-directional text support (for Arabic and Hebrew), database-aware widgets, QTable, etc.  Of course there is also the rich text widget and the \"html\" widget, but at least the rich text widget has been backported to Qt-2 for KOffice already.\n<br><br>\nAlso, porting KDE 2 to qt-3 will be *a lot* easier than it was to port KDE 1 to qt-2.  The changes are far less drastic; some have reported porting complex and large applications in just a few hours.  Plus KDE itself is not getting rewritten, as was the case for KDE 2.\n<Br><br>\nThe thread also gives the reason for switching to KDE 3 so soon.  Most distributions will upgrade to gcc 3.0 and the accompanying libstdc++ in the near future.  This will break binary compatability for all KDE programs.  Also gcc-3.1 is again to break binary compatability, this time for libstdc++ only.  Thus the idea is to switch to KDE 3 right along with the gcc 3.1 release so that binary compatability is not broken a third time with KDE 3.  It does break source compatability, but as noted above it is not difficult to port to Qt-3 from Qt-2."
    author: "Dre"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-24
    body: "Wow, that thread makes for facinating reading.  A nice, reasoned exchange of ideas until a sensible plan is developed.  A great example of how the Open Source model is supposed to work.  \n\nProps to the guys for professional way they've attacked the problems involved.\n\nJohn."
    author: "John"
  - subject: "The new component model shipped with Qt3"
    date: 2001-07-24
    body: "How does the new component  model play a part in KDE 3?\n\nBye,\n Victor"
    author: "Victor R\u00f6der"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-25
    body: ">Thus the idea is to switch to KDE 3 right along >with the gcc 3.1 release\n\n???\nCannot be 3.1, KDE 3 was proposed to be ready in January 2002, gcc 3.1 is scheluded for April."
    author: "Eeli Kaikkonen"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-24
    body: "Some developers started to scribble down their plans, of course this is not complete:\nhttp://developer.kde.org/development-versions/kde-2.3-features.html"
    author: "someone"
  - subject: "KDEUpdate"
    date: 2001-07-24
    body: "I think it would be beneficial to everyone if we had an update system built into KDE.\n\nafter KDE2 came out, we had update after update after update, without really a way to upgrade kde2.0 to 2.0.1, then 2.0.2 then 2.1 then 2.2, and all the beta's in between that.\n\nIs there anybody working on a live update's package for KDE2 that can take binary packages, download them, and install them into the KDE directory for you. You would still use the system's package manager, but not for updates. KDE would take care of all that for you. Only use the distribution's package manager when doing a new install of kde, or when doing a major upgrade (ie, kde2x to kde3x)\n\nThis would also put KDE on a lot more desktops of willing beta testers. And can keep people from the kde2x series up to date, without having to re-do their whole KDE installation.\n\ni hope you like this suggestion\n\nbye"
    author: "dude who cares"
  - subject: "Re: KDEUpdate"
    date: 2001-07-24
    body: "kdeinstaller does (or at least aims to do) something similar already.\nIt's in the kdenonbeta directory of the main CVS repository.\n\nHaven't tested it myself, good luck.\n\nRegards,\nwizdom."
    author: "wizdom"
  - subject: "Re: KDEUpdate"
    date: 2001-07-25
    body: "The KDE Installer Project was halted by its fearless leader, Nick Betcher.  Their website is at http://kdeinstaller.rox0rs.net"
    author: "Steve Hunt"
  - subject: "Re: KDEUpdate"
    date: 2001-07-24
    body: "I dont think that this is within the scope of KDE. Also this is duplicating what decent distros already do. Try using Debian, \"apt-get update; apt-get dist-upgrade\". Plus, if you want a GUI to do it for you there is kpackage. I'm sure that sometime soon the other distros will catch up to what apt has been doing for ages."
    author: "Corey"
  - subject: "Re: KDEUpdate"
    date: 2001-07-24
    body: "As far as I am concerned, that should be up to the OS distro including KDE.  They mostly all have their own update programs anyway, best keep it simple.  And if you know enough to get KDE set up on a system that didn't come with it, you know enough to update yourself.  That would just bloat things more than they need be."
    author: "dingodonkey"
  - subject: "Re: KDEUpdate"
    date: 2001-07-24
    body: "> As far as I am concerned, that should be up to the OS distro including KDE. They mostly all have their own update programs anyway, \n\nNo, they have not any an easy way to install. Only a lot of rpm, with bad dependencies to manage.\n\nSo a kdeinstaller would be useful.\n\nGood thing this KDE 3 calendar, I am happy to see the dynamism of the KDE team ! Changing sooner is a sign of leadership. Good luck for many powerful improvements !"
    author: "Alain"
  - subject: "Re: KDEUpdate"
    date: 2001-07-24
    body: "> No, they have not any an easy way to install. Only a lot of rpm, with bad dependencies to manage.\n\nYeah, cos RPM based distro's suck. As said above, use Debian. Dependencies are dealt with automagically."
    author: "Psiren"
  - subject: "Re: KDEUpdate"
    date: 2001-07-24
    body: "> As said above, use Debian. Dependencies are dealt with automagically.\n\nSorry, I want something user friendly (and, about it, Debian sucks, as you said...) or I wait...\n\nNow I wait, I install KDE only with a distrib new version."
    author: "Alain"
  - subject: "Re: KDEUpdate"
    date: 2001-07-25
    body: "this might become a flame war... hehe...\n\nAnyway, KDE runs on everything...  not just linux...  and linux, as well as the other operating systems don't just run on intel processors...\n\nIt would take a large team to support this many binary update schemes...\n\nPersonaly, the operating system ( or in Linux's case, the maybe the distro maker ) should probably handle this...\n\nAfter all, just because KDE developers are on steroids (They take it in tea form), doesn't mean they don't (ever) sleep...\n------------------------------\nNew Thought:\n------------------------------\n\nMan did we all notice KDE spinning along last Fall/Winter.  Many of us didn't believe they would be able to release on schedule, with what they had planed, and do it all so well.  Now, we know it will be done, and can just stare in ammazement...   However, don't forget you too can join these cool guys, and help out however possible..."
    author: "Gregory W. Brubaker"
  - subject: "Re: KDEUpdate"
    date: 2001-07-26
    body: "Conectiva has APT and Mandrake has URPM to handle dependencies automagically. And they\u00b4re both rpm-based distributions."
    author: "Evandro"
  - subject: "Re: KDEUpdate"
    date: 2001-07-25
    body: "One possible solution to this would be to have an updater/installer GUI interface with multiple backends.  One backend for Debian, one for RedHat, one for SuSE, etc."
    author: "David Ludwig"
  - subject: "Re: KDEUpdate"
    date: 2001-07-26
    body: "or a common backend like the ximian setup tools or something that all the distros could later adopt"
    author: "Rick Kreikebaum"
  - subject: "Re: KDEUpdate"
    date: 2001-07-27
    body: "Debian's apt-get dist-upgrade seems to keep me in line of the newer KDE packages pretty quickly already :)\nIf anything, just post new .debs every day?"
    author: "Peaker"
  - subject: "Qrash3?"
    date: 2001-07-24
    body: "Aka Qrash3? Joke? :)\nWhen I pronounce Qrash it sounds like 'crash'.\nHehe..."
    author: "antialias"
  - subject: "Re: Qrash3?"
    date: 2001-07-24
    body: "KDE 1.89, the first 2.0 pre-release, was code-named \"Krash\". The intent was to let everybody know that this was a very early beta of a whole new KDE architecture and was therefore still unstable. Think \"technology preview\".\n\nThis time, of course, the new technology will mostly stem from the new Qt version... Hence the name \"Qrash\". But it's mostly for laughs. :-)"
    author: "Jerome Loisel"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-24
    body: "Great news. I have been hoping they would opt to switch to Qt 3 sooner rather than later."
    author: "AC"
  - subject: "What about Speed and OS independence!"
    date: 2001-07-24
    body: "KDE 2.1.1 is still very much slower than Windows or say KDE 1.1 which was very fast. Here is my observation:\n\n1. Explorer takes 2 seconds in windows 98\n2. Konqueror takes 3 seconds in KDE 2.1.1\n3. KFM file manager/broweser took 1/2 (half) second.\n\nKDE Updates should be Statically linked binaries like windows Internet Explorer, Windows Media Player, etc., upgrades. Other than that Opera Browser is a good example for Linux.\n\nAll I want is KDE 1.1's speed in KDE 2.2 or KDE 3.0. Have the developers Optimized the KDE code!?"
    author: "Asif Ali Rizwaan"
  - subject: "Making it clear :)"
    date: 2001-07-24
    body: "> 1. Explorer takes 2 seconds in windows 98\n> 2. Konqueror takes 3 seconds in KDE 2.1.1\n> 3. KFM file manager/broweser took 1/2 (half) second.\n\n1. Explorer takes 2 seconds in windows 98 _to appear_ after click.\n2. Konqueror takes 3 seconds in KDE 2.1.1 _to appear_ after click.\n3. KFM file manager/broweser took 1/2 (half) second _to appear_ after click on the associated icon.\n\n> Other than that Opera Browser is a good example for Linux.\n\nOther than that Opera Browser is a good example for Linux which gives statically linked application. I used that on RH 6.2."
    author: "Asif Ali Rizwaan"
  - subject: "Re: What about Speed and OS independence!"
    date: 2001-07-24
    body: "The speed penalty is in big parts due to the linker (ld). Waldo Bastian analyzed the problem and now somebody from the ld team is working on it. I read first numbers which said a first alpha version of the improved linker reduced the time needed for linking on his machine from 0.6 to 0.01 seconds. This sounds very promising :-)\nThe second thing is the slow icon loading, I will have a look at it.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "more ram is the answer for me"
    date: 2001-07-24
    body: "Is this at all possible?\n\n(I don't expect it to be done, but just from a wish-list point of view)\n\nI would rather buy another 128mb DIMM RAM so that the entire KDE could be run from RAM so that everything is already loaded.\n\nWhy can't we have this as a solution for people NOW that can afford it?\n\nI would love that as the solution for me :)"
    author: "kde-user"
  - subject: "Re: more ram is the answer for me"
    date: 2001-07-24
    body: "I don't think that RAM is the answer, since I've tried that solution myself. I doubled my RAM from 128 MB to 236 MB - I know that it isn't a huge amount of RAM - but it still ought to make a difference - and that's my point: I didn't for me... So I'll look forward to see Waldo's paper turned into code i KDE 2.2 /Fred"
    author: "Fred"
  - subject: "Re: more ram is the answer for me"
    date: 2001-07-24
    body: "Let me try again : ) 128 MB + 128 MB is 256 MB all those numbers..."
    author: "Fred"
  - subject: "Re: more ram is the answer for me"
    date: 2001-07-25
    body: "I recently upgraded my ram from a puny 64 megs to a total of 320.  Starting times of kde apps are somewhat better, but this is mainly due to the fact that I don't have to hit swap anymore.  \n\nI'd imagine there might also be some slight improvements just because previously accessed files (including binaries for the apps) get cached in ram, making the second load faster.  I don't really know enough about how Linux does this to accurately comment, though.\n\nHopefully the loader issues get sorted out soon, though; the effect of it currently is fairly noticable on a 350MHz cpu.\n\n--c."
    author: "Carl R"
  - subject: "Re: more ram is the answer for me"
    date: 2001-07-25
    body: "Hmm...I have 256MB and I hit swap all the time.  Maybe something's wrong.  Running Potato, X 4.1.0-4.0.1 \"hybrid\", Kernel 2.4.5, 57 MB Swap, and ran completely out once, had to kill X, then went down to 3 MB used.  What could the problem be?  Probably my celeron...maybe on my 1.0GHz Thunderbird things will change......."
    author: "Justin Hibbits"
  - subject: "Re: more ram is the answer for me"
    date: 2001-07-25
    body: "IIRC, Linux kernels in the 2.4 series prior to 2.4.6 had some memory management issues. I noticed these myself... After going from 2.2.18 to 2.4.2, memory use on my 128MB Athlon 600 system really seemed to *not* perform as well as with 2.2.18.\n\nI upgraded with each new 2.4.x, and now with 2.4.6 memory usage has really improved! Yay! =)\n\nIf you are still using 2.4.5, try upgrading to the latest kernel and see if that helps...\n\nIIRC, there was something about it (the kernel) not releasing allocated memory when the memory was no longer being used..? Can't say more because I don't know all the details.\n-- \nCheers,\nJeff"
    author: "Jeff N."
  - subject: "Re: more ram is the answer for me"
    date: 2001-07-25
    body: "I'm still running 2.2.19 until I either find a compelling reason to switch, or I decide to play with a new distro that has a 2.4.x kernel.\n\nI *think* the problem with the 2.4 kernels up to 2.4.5 was that they would allocate pages in swap, but not always *de*allocate them.  This would cause any machine to eventually run out of swap space.  I could be remembering this wrong though, so check Kernel Traffic for details."
    author: "Carl R"
  - subject: "Re: more ram is the answer for me"
    date: 2001-07-27
    body: "I would suggest all of you to switch to 2.4.7, as soon as possible.\n\nWith it most VM issues are fixed (some more optimizations are under development).\nLinus et. all. are very pleased with it.\n\nEven most of the outstanding ReiserFS fixes are in place, now (NFS).\n\nCheers,\n\tDieter\n\nBTW I have 640 MB but it is NOT the answer to all questions...;-)"
    author: "Dieter N\u00fctzel"
  - subject: "running out of RAM (was: more ram is the answer fo"
    date: 2001-07-26
    body: "Hi!\n\nAre you using imported GTK pixmap themes? I don't know if that has been fixed but in times of KDE 2.0.x there was a huge ressource leak in GTK pixmap theme handling.\nAdditionally I must say that I still have memory consumption problems although I dropped my favourite GTK theme quite fast. (at least for KDE...) I have 512 Mb RAM installed in a DualCeleron 466 machine running Linux 2.4.6, glibc 2.2.0 and X 4.0.3 / 4.1.0. X often uses more than 200 MB RAM, sometimes even causing my machine to page out memory... :-(\nKernel 2.4.6 seems to perform substantically better than the previous 2.4.xs.\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: running out of RAM (was: more ram is the answer fo"
    date: 2001-07-26
    body: "Nope, I never use Gtk+ themes, since there are KDE themes that are close enough, so I use those.  Now I use KDE 1, but parts still seem to be a little slow.  Oh, well.  Ever since I upgraded to kernel 2.4.7 yesterday, it hasn't gone to swap at all.  Also, it actually found my power button -- what would it do if it didn't, panic? (lol)\n\nI am still keeping KDE 2.1 on my computer, but just the libs and some programs like Konsole because they are better than the KDE 1.  I just need to find a patch for Qt 1.44 that supports the wheel.  Then I'm all set."
    author: "Justin Hibbits"
  - subject: "Linker issues"
    date: 2001-07-25
    body: "aleXXX wrote:\n> The speed penalty is in big parts due to the\n> linker (ld). Waldo Bastian analyzed the\n> problem and now somebody from the ld team is \n> working on it.\n\nDoes anyone have a link or other reference as to what's currently being done?  I don't know if I'd really have much to contribute, but being the curious type, I'm sure following the development of this would be about as much of a learning experience as reading Waldo's paper was!\n\n--c."
    author: "Carl R"
  - subject: "Re: Linker issues"
    date: 2001-07-25
    body: "Try:\n<br><br>\n<a href=\"http://lists.kde.org/?l=kde-core-devel&m=99134101312638&w=2\">http://lists.kde.org/?l=kde-core-devel&m=99134101312638&w=2</a>\n<br><br>\nand\n<br><br>\n<a href=\"http://sources.redhat.com/ml/binutils/2001-06/msg00360.html\">http://sources.redhat.com/ml/binutils/2001-06/msg00360.html</a>"
    author: "AC"
  - subject: "Re: Linker issues"
    date: 2001-07-25
    body: "Thanks, nice links. I wondered if anything came of this after Waldo's paper and actually I'm still wondering if anything will actually happen with this code?\n\nHas anyone actually tried this and if so what results did you see in the real world; could you tell the difference when using the pre-linker? \n\nAny ideas what timescales we are talking about till this goes mainstream and this distro's use it?"
    author: "DavidA"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-24
    body: "So when well we have Arabic or hebrow support?!\nit's about time .. :)\ni mean eaven if it was a beta ver. when do you think that KDE well support Arabic and Hebrow (BiDi)?\nKDE always rocks :)\n\nThanks and keep up the GOOD work ;o)"
    author: "JP"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-25
    body: "Arabic and Hebrew support is one of the primary reasons for switching to Qt 3. So expect this to be available starting from KDE 2.89.\nI'll make sure KWord supports it."
    author: "David Faure"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-25
    body: "David this is the best news i have heard in a long time..\n\njust keep up the good work!\nyou guys are great!\n\nthanks"
    author: "JP"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-26
    body: "Thanks a lot for the Arabic support.\nThis is one thing that keep me using MS OS and application on my desktop. with this support I will switch to only linux desktop.\n\nWill Arabic support be available for all KDE applications?\n\nAbdullah"
    author: "Abdullah"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-09-13
    body: "Hi,\n\nI am very happy to hear about Arabic support in KDE.\n\nbut please try to fix up the arabization faults M$ did in their Arabic Windows System.\n\nI hope you finish your work perfectly.\n\nPlease consider implementing some special Arabic typefaces issues in the KWord to correct the bad shapes of some combinations.\n\nPlease tell me if you need a description about the advanced shaping problems in Arabic typefaces.\n\nthank you very much."
    author: "Amer"
  - subject: "Too fast?"
    date: 2001-07-24
    body: "KDE is sure moving quickly. And that is in some ways good. But in this fast speed, will KDE ever be able to mature and become truly stable? I fear that there always will be serious bugs if there is so little time to maintain a stable release before it is time to release the next major version. Compare to the Linux kernel. Even though there are newer development kernels, the old ones are maintained a long time. I wonder if it should not be better to aim for the next release much further ahead in time."
    author: "Claes"
  - subject: "Re: Too fast?"
    date: 2001-07-24
    body: "I could not agree with you any less. I rather have KDE 3.0 between June-July next year rather than early next year.\n\nMore times on development means more stable, better product and less pressure on those KDE teams...\n\nAll the best ..\n\nyour biggest FAN"
    author: "Tom"
  - subject: "Re: Too fast?"
    date: 2001-07-25
    body: "I absolutely agree. Linux is known to be verry stable. So we should keep this advantage and not develop ANOTHER rich-featured, buggy System. \nKDE is about as stable as my NT-Workstation I have to use at work: it does not crash often but still TOO often...\n\nPlease don't sacrifice stability for features and new *.0-Versions  !"
    author: "Thomas"
  - subject: "Re: Too fast?"
    date: 2001-07-25
    body: ">Please don't sacrifice stability for features and new *.0-Versions !\n\nKDE is doing nothing of the kind!  KDE 3 is basically a port of the existing KDE2 framework to QT3.  It's nothing at all like the change from KDE 1 to KDE 2.  Much of the existing code will probably make it through the transition essentially unchanged.  So the stability of KDE 3 will probably be greater than KDE 2's stability (which is already better than Windows).\n\nThe reason for the big number change from 2 to 3 is an issue of compatibility.  The KDE team wants to make it clear that programs written and compiled for KDE 2 will not work \"out-of-the-box\" with the next KDE.  Making the version number 3 lets people know that it is not compatible.  It is not compatible for a variety of reasons, including:\n\n~QT 3 is slightly changed, breaking both source-code and binary compatibility.\n\n~GCC 3.0 is coming out, which will break binary compatibility (now I hear something about 3.1 breaking compatibility too?)\n\n~Now that the KDE libraries have seen real use, some minor API problems have been uncovered (of course).  These can't be changed without breaking binary (and sometimes source) compatibility.\n\nAfter KDE 3 comes out with an improved API, the KDE team hopes to standardize on it for a long time, allowing a large application base to grow, and allowing any remaining bugs to be fixed until KDE 3 is as stable as KDE 1.1.2!"
    author: "not me"
  - subject: "Re: Too fast?"
    date: 2001-07-26
    body: "I agree.\n\nThere should be more time focusing on stablising KDE2 and making KDE2's memory usage smaller."
    author: "James Pole"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-24
    body: "But when KDE3 comes out am I going to have ~/Desktop, ~/KDesktop, ~/Kdesktop3, ~/.kde, ~/.kde2, ~/.kde3, /opt/kde, /opt/kde2, /opt/kde3 ?\nThis is getting to be an awful mess - can/will the old stuff for KDE 1 be junked?\n- Alan"
    author: "Alan"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-24
    body: "KDE developers have long stopped working on KDE 1.X... But no, no-one will log on to your computer and junk the old versions for you. You will have to do that part yourself. :-)"
    author: "Jerome Loisel"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-24
    body: "This is your distribution's fault. If you compile KDE 2.x from source on a clean system, it goes into wherever you --prefix it, ~/.kde, and ~/Desktop.\n\nAre you running SuSE? Complain to them."
    author: "11223"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-31
    body: "KDE2 was a rewrite. Almost everything changed from KDE1.\nKDE3 is mostly going to be the same code recompiled to work with the improved QT3, glibc3 and libstdc++, hence the shorter time. It's (hopefully, 'cos I'll have to recompile KPooka ;> ) not going to be as major a change."
    author: "Mr. Bogus"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-25
    body: "This will probably become a flame really quickly, just please don't flame me.\n\nDidn't it take the team 2 years from the release of KDE 1.0 to get to KDE 2.0?  Now it's only taking them 1 year to go from KDE 2.0 to KDE 3.0.  I think it's a little fast....I, too, have begun to question the stability/(release speed) trade-off, since I can't bear to use KDE 2.1 any more.  Hey, I've actually found a site with KDE 1.1.2 source, the most stable KDE I've used.  Anyone want the url?  It's:\n\nftp://ftp.externet.hu/pub/linux/kde/stable/1.1.2/distribution/tar/generic/source/\n\nI am getting probably the worst connection possible, but I don't care.  It's only a one-time deal.\n\nEnough ranting...\n\nAnd KDE Team, SLOW DOWN BEFORE YOU HURT YOURSELVES!"
    author: "Justin Hibbits"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-25
    body: "Some people just don't understand the \"release often\" method, typical of opensource projects.\nIf we don't release often, then there is much _less_ polishing going on, developers would just go on and stack up new features. It's the _releases_ that 'force' people to think about quality.\n\nExpect many improvements in terms of speed and stability in KDE 2.2. It's definitely faster than 2.1, I felt that when switching between the two. As for stability, I didn't find 2.1 unstable so I can't really say :)"
    author: "David Faure"
  - subject: "Re: Proposed Timetable for KDE 3"
    date: 2001-07-25
    body: "IMHO, the \"release early, release often\" method is to allow everyone to see what you've done lately.  I would rather you make the \"final\" a beta, until you get positive feedback from at least <insert a bare minimum here> people, who have tested it in several ways.  Like, have people register to be \"official testers\" of certain areas, so that you will expect feedback from those people before you go ahead with the release.  That way, you have several people with similar results, and if the results look good, change the beta into a final.  That is probably a better solution than adding features at the last minute, or fixing bugs at the last minute, just to have a release.\n\nJust my opinion."
    author: "Justin Hibbits"
  - subject: "KDE3 on Darwin/MacOSX??"
    date: 2001-07-25
    body: "As Qt3 will be available for Darwin/MacOSX, will KDE3 ever run on that platform? I can run GNOME apps and WindowMaker on my Mac, but I miss KDE..."
    author: "Pedro Ziviani"
  - subject: "Re: KDE3 on Darwin/MacOSX??"
    date: 2001-07-25
    body: "Not unless QT3/Mac is GPL'd..."
    author: "not me"
  - subject: "I want it!"
    date: 2002-01-30
    body: "I want KDE on my Mac.\n\nFor crying out loud.  MacOS X was supposed to bring us all the great UNIX apps that are out there.  And the one biggest app of all, KDE, is still unavailable for some obscure technical reasons that I don't understand and shouldn't have to.\n\nEither QT or Apple or KDE or the Darwin open-source people are writing crappy code.  Whoever's fault it is, knock it off!  I want KDE on my Mac darn it - and NO I don't want to install any other OS to get it!!!"
    author: "Leo"
  - subject: "Re: KDE3 on Darwin/MacOSX??"
    date: 2002-06-04
    body: "RIGH ON!\n\nI love KDE and KDE3 is even more cooler!\n\nI have a G4 iMac and I have XonX running.. I like that.. (blackbox) But I want KDE3!\n\nPlease make it all work.\n\nBrian\n"
    author: "Brian"
---
While we anxiously await the release of KDE 2.2
<A HREF="http://dot.kde.org/995344211/">in two weeks</A>,
<A HREF="http://www.kde.org/people/waldo.html">Waldo Bastian</A>,
the current KDE release coordinator, has
posted the proposed schedule for the release of KDE 3.  The upshot:
KDE 2.2.1 is scheduled for September 2001 and KDE 3.0 for January
2002.  The full schedule is available below.
<!--break-->
<P>&nbsp;</P>
<HR SIZE=1 NOSHADE WIDTH="90%">
<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0>
<TR ALIGN="top"><TD ALIGN="right"><STRONG>
From:</STRONG>&nbsp;&nbsp;</TD><TD><A HREF="mailto:bastian at kde.org">Waldo Bastian &lt;bastian@kde.org&gt;</A>
</TD></TR>
<TR ALIGN="top"><TD ALIGN="right"><STRONG>
To:</STRONG>&nbsp;&nbsp;</TD><TD>kde-core-devel@kde.org
</TD></TR>
<TR ALIGN="top"><TD ALIGN="right"><STRONG>
Subject:</STRONG>&nbsp;&nbsp;</TD><TD>RFC: The Road Ahead
</TD></TR>
<TR ALIGN="top"><TD ALIGN="right"><STRONG>
Date:</STRONG>&nbsp;&nbsp;</TD><TD>Mon, 23 Jul 2001 14:07:01 -0700
</TD></TR>
</TABLE>
<P>
To wrap up the Qt3 discussion and to bring everyones expectations a bit in
line, here is a proposed time-table for the rest of this year:
</P>
<UL>
<LI>Aug 2001: Release of KDE 2.2</LI>
<LI>Sep 2001: Release of KDE 2.2.1</LI>
<LI>Oct 2001: Development release, KDE 2.89 aka Qrash3.</LI>
<LI>Nov 2001: KDE 3.0beta1</LI>
<LI>Dec 2001: KDE 3.0beta2</LI>
<LI>Jan 2002: KDE 3.0 final</LI>
</UL>
<P>
I suggest to switch KDE CVS to Qt3 after the release of 2.2.1.
</P>
<P>
If you would like to be the release dude for KDE 3.0, now is a good time to
step forward :-)
</P>
Cheers,<BR>
Waldo<BR>
-- <BR>
Andrei Sakharov, Exiled 1980-1986, USSR, <A HREF="http://www.aip.org/history/sakharov/">http://www.aip.org/history/sakharov/</A><BR>
Dmitry Sklyarov, Detained 2001-????, USA, <A HREF="http://www.aip.org/history/sakharov/">http://www.elcomsoft.com/</A>
<HR SIZE=1 NOSHADE WIDTH="90%">