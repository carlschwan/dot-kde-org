---
title: "KSpread 1.1 Beta 3 reviewed"
date:    2001-07-26
authors:
  - "numanee"
slug:    kspread-11-beta-3-reviewed
comments:
  - subject: "Tahoma"
    date: 2001-07-26
    body: "Wow, good fonts really make a big difference in the appearance of X apps.  Kent seems to use Tahoma, which is a Microsoft font included with Office, I believe.  X windows needs some *good* free fonts.  My favorite font for X right now is Nimbus Sans, which is in Debian, but it really doesn't hold a candle to Tahoma for readability at small size.  There seems to be a real shortage of free fonts.\n\nAlthough this issue isn't really a KDE issue, it does have a big effect on KDE.  Does anyone know of a place to get free fonts?\n\nOh, BTW, great review Kent.  Keep 'em coming!"
    author: "not me"
  - subject: "Re: Tahoma"
    date: 2001-07-26
    body: "You can look for free fonts at http://www.freewarefonts.com/ or http://www.freepcfonts.com/index.html as a starting point. However I really do not know if you'll find any font which is readable at very small point sizes...\nGood luck! :-)\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: Tahoma"
    date: 2002-07-02
    body: "well in debian there is a package called msttcorefonts. this actually downloads a prog that grabs the files from microsofts website (you cant repackage them) then extracts them and converts them to usable fonts. great stuff."
    author: "Andy Barnes"
  - subject: "KSpread is not ready"
    date: 2001-07-26
    body: "It is said :\n>  All the basic functionality that I've come to expect from a spreadsheet is presented in KSpread. \nand also :\n>  KSpread is already a good enough spreadsheet for probably 90% of users, with the basic functionalities I've described.\n\nNo, no, no, KSpread is not ready, it is unusable for 90% of users because there is a big lack.\n\nIn Excel or Gnumeric or any spread manager, how do you navigate between cells ? By using Tab and Shift Tab keys, of course. In a cell, how do you navigate betweeen words ? By using Ctrl Tab, Home, End keys.\n\nIn KSpread these elementary and natural things are impossible. These keys are useless. All your key reflexs are useless... So there is a big wasted time by trying to use other keys or by using the mouse...\n\nIn the other KOffice programs, there is not such desagreements. Only in KSpread, there is a lack of an engine for intercepting key actions. KSpread will have this functionnality only after the 1.1 version.\n\nSorry, I feel that this review is not serious by jumping such a lack (although it is OK about KChart truth). Of course, by importing a spread and modifiying some formulas and appearence, there is no need to type many things with the keyboard... But try to fill a table with some text, several columns and at least 10 lines, you will have some irritating difficulties...\n\nHappily, for me (and I think for many users having basic needs), KWord, Kontour, Kivio are ready. And we can wait a little about KSpread (Gnumeric is a good transitional program...). Sure, after updating this Key handicap (+ KChart), it will be a good program !"
    author: "Alain"
  - subject: "Re: KSpread is not ready"
    date: 2001-07-26
    body: "Hear, hear!!! It would also help if I could make use of the whole canvas and not only the 9999 first lines...\n\n(See bug number 11819, it is only 302 days old) :(\n\nThe day these bugs are removed I will (be able to) start promoting Koffice."
    author: "Jo \u00d8iongen"
  - subject: "Kivio lacks three things:"
    date: 2001-07-26
    body: "1. Arrows (not implemented)\n2. Cliparts (not implemented)\n3. Decent printing / Postscript output.\n\nATM you can print only in portrait mode, the fonts become bitmaps!! Also the print out put is NOT the same as the screen output (no WYSIWIG), there is no distance between the text and the stencil borders. So an M directly next to a line will look horrible! Looks unprofessional."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Kivio lacks three things:"
    date: 2001-07-27
    body: "Hello,\n\nI haven't had any time in months to work on Kivio - sorry.  I never got around to updating all the arrowheads.  I probably should have removed most of them from the combo box.\n\nAs for the printing thing.  I'm using Qt's printing system so it really should be WYSIWYG.  If it isn't, I don't know why.  I haven't printed from it for a while.\n\ntheKompany offers alot of stencils now - see their website for stencils.\n\n-dave"
    author: "Dave Marotti"
  - subject: "Re: KSpread is not ready"
    date: 2001-07-30
    body: "Hi!\n\nWith my every day use, I'm very happy with KSpread.  In fact people how buy Office product, only a small percentage of them use spreadsheet and when they do they only use it to do very little things like calculating income, business expense, and do lab report.  \n\nI don't use spreadsheet that often.  I probably use it twice a month.  There are a small population who use it extensively.  Those are the 10% people.\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Screenshot shows miscalculation between Excel/KSpr"
    date: 2001-07-26
    body: "Hi,\n<br><Br>\nTake a look at the following screenshots:\n<br><br>\n<a href=\"http://www.mslinux.com/reviews/kspread2.png\">http://www.mslinux.com/reviews/kspread2.png</a><br>\n<a href=\"http://www.mslinux.com/reviews/kspread3.png\">http://www.mslinux.com/reviews/kspread3.png</a>\n<br><br>\nTake a look at E12 (Direct Loans)\n<br><Br>\nWho said I didn't have a sensitive eye ;-)\n<br><br>\n/Samie"
    author: "Sam"
  - subject: "a bit overly optimistic"
    date: 2001-07-26
    body: "These reviews are not bad, but I consider them a bit too optimistic. OK, KDE also needs some PR-hype, I admit that. But by reading those reviews the average reader almost gets the impression that KOffice is a complete replacement of MS Office already. A claim probably no one of the programmers would make yet.\n\nKOffice is a relatively young product and definitely beta (it is released as beta, KDE releases tend to be very conservative in this respect, I like that!).\n\nI like KOffice as I like almost everything of KDE, but I wouldn't use it for my everyday serious work - yet."
    author: "AC"
  - subject: "Re: a bit overly optimistic"
    date: 2001-07-26
    body: "I agree that the reviews are \"too optimistic\". However, KWord is pretty stable and feature rich word processor.\n\n/Samie"
    author: "Sam"
  - subject: "Without footnotes?"
    date: 2001-07-26
    body: "Someone removed the footnote functionality from kword, how is anyone to work without footnotes?\nThat is nothing fancy, this is basic functionality missing.\n\nkword ATM is only nice for letters(It esxcels and is nicer than SO5.2 there), but for longer documents some features are still missing.\n\nHave a look at lyx for these it is better anyways."
    author: "Moritz Moeller-Herrmann"
  - subject: "Recalc"
    date: 2001-07-26
    body: "No, it's not ready. There's a serious flaw in the recalc procedure: I couldn't turn it off, every time I closed and reopened the options dialog the recalc was again switched on. The recalc made it extremely difficult to enter formulas into cells: just enter the \"=\" then wait 3 seconds and Kspread saves the cell thus effectively leaving edit-mode. Or try to auto-copy formulas to, say, a thousand rows. It takes far too long and almost locks your box as the cpu is working so hard that it almost melts.\n\nThing is, this problem is known to everyone who actually has read the note to the developers coming with the sources.\n\nI like the look and feel of KSpread but I just can't use it for complex sheets and, therefore, I still have to use StarOffice."
    author: "MarkD"
  - subject: "don't call them reviews"
    date: 2001-07-27
    body: "These texts are great, and could be turned into a useful introduction to the applications. But, as many has said, the author should really stop calling them \"reviews\". \n\nWe, who know the context can bear with this misnomer, but other people, and the intended audience in particular, expect a review to be critical and impartial. Imagine the response if microsoft employee wrote a similar \"review\" of office and put it out on the web...."
    author: "will"
  - subject: "Re: don't call them reviews"
    date: 2001-07-30
    body: "You can never have impartial reviews.  There will always be bias based on the \"things\" you test.  What I test makes very much sense to a lot of people and their use.  I'm an ordinary spreadsheet user.  \n\nPerhaps I will label it personal reviews to satisfy you?\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: don't call them reviews"
    date: 2001-08-08
    body: "Hi\n\nThanks again for your great work! Someone else suggested to call them \"guides\" which would be better. Although I have a feeling that there is an even better word out there...\n\nYes, reviews are supposed to be impartial - they should only serve the primary interest of giving an objective assessment of the product. Expressing personal preferences belongs to a review, but that it something else. You are a KDE-developer and the texts are to all appearances an attempt to present the applications in best possible light, serving the external interest of promoting KDE.  Nothing wrong with that - that can be the purpose of introductions or marketing material - but out of honesty, just give it the right name.\n\nThe specific value of a review consist in the trust you have that that the reviewer has done an impartial assesment for you. There was a scandal recently where it turned out that Sony had printed \"reviews\" of its own movies. The problem with the Mindcrafts-tests a couple of years ago was precisely that they were felt to be tailormade to put Microsoft in a good light while at the same time hiding this fact (in being a presented as a \"test\")."
    author: "will"
  - subject: "Re: don't call them reviews"
    date: 2001-08-05
    body: "I have only recently become interested in KDE.  I've been following the GNOME project for ages.  I am thinking of changing over to KDE because I like what I see and what I read about KDE suggests that it is a well designed and well implemented desktop environment.\n\tWith this in mind I am endevouring to learn more about KOffice.  A good word processor and a good spreadsheet are important to me.  I am in search of an impartial, independant review of KWord and/or KSpread.  It would be good to find a comprehensive comparison of KSpread vs Excel, or KSpread vs Gnumeric.  Similarly, it would be helpful to read a comparison of KWord vs MS Word.\n\tI started reading Kent's reviews with the idea that they would tell me whether or not KWord & KSpread ready for \"prime time\".  After reading his KWord & KSpread \"reviews\" I am impressed with what he showed me of the applications but not convinced that they will fulfill my word processing and spreadsheeting needs.\n\tAfter reading the postings in this thread I get the impression that KWord is a good word processor and fairly mature and that it's good enough to fulfill most peoples word processing requirements.  My impression of KSpread is that it's well on its way but still has a few serious shortcomings (like navigating cells with the tab & arrow keys).  \n\tTo sum up, if the intended audience of Kent's reviews are people like me, then the reviews aren't comprehensive enough or critical enough to convince me of the quality and 'readiness' of the applications.  That said, I still liked what I saw and I'll give them a go.  :-)"
    author: "Jeff Lasslett"
  - subject: "KSpread not ready (yet)."
    date: 2001-08-02
    body: "It is amazing to see with which speed the Koffice package is becoming a real and usable set of programs. Kspread was one of the first programs within the package that was usefull and stable enough to actually use it. \n\nLast month I had to travel around Europe and wanted to calculate my expenses back to one currency. At that point it became clear to me that Kspread is lacking some basic functionality. I could not find a vlookup, hlookup or whatever lookup function that could be used with a currency table.\n\nOnce that has been added I will try to use Kspread for the more serious (fincial) work."
    author: "Arnoud H."
---
In the second part of his <a href="http://www.mslinux.com/reviews/koffice.html">review</a> (<a href="http://static.kdenews.org/mirrors/www.mslinux.com/reviews/koffice.html">mirror</a>), Kent Nguyen walks us through the <a href="http://www.mslinux.com/reviews/koffice6.html">KSpread component</a> (<a href="http://static.kdenews.org/mirrors/www.mslinux.com/reviews/koffice6.html">mirror</a>) of <a href="http://www.koffice.org/">KOffice</a> 1.1 Beta 3. As usual, the review is complete with screenshots, and edits are courtesy Tina Gasperson of <a href="http://www.newsforge.com/">Newsforge</a>. 
<!--break-->
