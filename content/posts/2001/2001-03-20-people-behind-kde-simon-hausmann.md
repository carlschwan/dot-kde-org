---
title: "People behind KDE: Simon Hausmann"
date:    2001-03-20
authors:
  - "Inorog"
slug:    people-behind-kde-simon-hausmann
comments:
  - subject: "Re: People behind KDE: Simon Hausmann"
    date: 2001-03-20
    body: "That's funny.. I just started a project which will use Konqueror/Embedded. Since when can The Dot read my mind? :-)"
    author: "Rob Kaper"
  - subject: "Re: People behind KDE: Simon Hausmann"
    date: 2001-03-20
    body: "Magic has always been an important part of KDE, and I believe the mind-reading stuff has been part of The Dot for some weeks already. Just look at Ettrich's quote at the bottom of the page - I hope the patch will make it into KDE 2.2 ;-)\n\nRegards,\n Gaute Hvoslef Kvalnes\n Norwegian (Nynorsk) KDE translation: http://i18n.kde.org/teams/no_NY/"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Not far enough"
    date: 2001-03-21
    body: "Konqueror/Embedded is good, but it doesn't go far enough.\n\nWhat KDE needs is KDE/Embedded, built from the same source base as KDE.\n\nSo I can type 'startkde' to run KDE, then 'startkde --embedded' to run a stripped down, black and white version suitable for 320x200 displays.\n\nDesktop is on the way out, but we need a seamless binary-compatible transition path to palmtop devices.\n\nWriting an 'embedded' version of each app is out of the question.\n\nSo KDE needs an 'embedded' mode where eevrything is 'debloaded' and scaled down at run time."
    author: "Professional Software Developer"
  - subject: "Re: Not far enough"
    date: 2001-03-21
    body: "I think desktop is on the way in, the latest iPaq comes with 64 MB RAM + X MB ROM.\nForget black&white, grayscale maybe.\nSmaller and faster is always nice, though."
    author: "reihal"
  - subject: "Re: Not far enough"
    date: 2001-03-21
    body: "But I think he is right with having two modes of Kde/Kde-embedded. More and more people are\n getting used to mobile computing and wouldnt it be nice to transfer your contacts, notes\n whatsoever from your palm running Kde/embedded via infrared to your laptop running KDE(antialiased)using all the same applications ??"
    author: "Christoph"
  - subject: "Re: Not far enough"
    date: 2001-03-22
    body: "Well, that'll have to be integrated into kdenox (KDE-no-X), for running KDE in the framebuffer (in Linux). Palmtop devices will use this, this as they don't run X. I haven't bothered to check much of the details on the progress, though."
    author: "Triskelios"
  - subject: "Re: Not far enough"
    date: 2001-03-23
    body: "NANO-X is an implementation of X Windows for embedded devices.  There is also Tiny X.\n\nAs far as getting KDE/palm integration, I say on getting KPilot/Korganizer/Knotes/Kab to work together perfect is a start.  Stripping KDE to run in a 4 - 8 mb footprint might be difficult."
    author: "Nick Papadonis"
  - subject: "Re: Not far enough"
    date: 2001-03-23
    body: "KDE is very heavyweight it's made to do a rather lot.  A simple window manager only and Qt embedded is all you need.  Especially since the basic interface would probably have to be changed from the KDE one.  What I'd like to see is a simple enviornment based on the KDE window manager."
    author: "Lee"
  - subject: "I'm sorry..."
    date: 2001-03-21
    body: "J.R.R. Tolkien is dead."
    author: "J\u00f8rgen Adam Holen"
  - subject: "Re: People behind KDE: Simon Hausmann"
    date: 2001-03-21
    body: "Hi Simon,\n\nThanks for all your work and efforts throughout the last years and all the help you offered while being the humblest coder I've ever collaborated with.\n\nYou simply rock!\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: People behind KDE: Simon Hausmann"
    date: 2001-03-21
    body: "... and I can only second that :-)\n\nLukas"
    author: "Lukas Tinkl"
  - subject: "What about Mosfet?"
    date: 2001-03-22
    body: "Hey, Tink: What happened to our (former?) hero Mosfet? I notice he's no longer on the list of folks to be interviewed. His fans would really like to know what he's up to these days!\n\nSamawi"
    author: "Samawi"
  - subject: "Re: What about Mosfet?"
    date: 2001-03-23
    body: ">>What happened to our (former?) hero Mosfet?<<\n\nOur knight in shining armour has decided that he doesn't want to participate in an interview. I guess being too famous comes with a price.\n\n--\nTink"
    author: "Tink"
  - subject: "Re: What about Mosfet?"
    date: 2001-03-23
    body: "Mosfet just doesn't want you to ask him about a specific picture. But I have a copy! I will blackmail him!\n\nAnd I will force him to put my copyright on all his code!\n\nAnd I will win fame and fortune! Oh, wait, ermmm... hey, what a shame he doesn't because he's a very interesting guy :-)\n\nAnd he would have answered FTE (maybe even KFTE ;-) for the question about favourite editor! Dammit!"
    author: "Roberto Alsina"
---
The wonderful Konqueror/Embedded project, the DCOP bindings for the C language, contributions to Konqueror, KParts (and before that, OpenParts), KHTML and KOffice... all this work is courtesy of <a href="http://www.kde.org/people/tron.html">Simon Hausmann</a>.
<a href="mailto:tink@kde.org">Tink</a> needed all her persuasive powers to make Simon submit to this interview for the well known <a href="http://www.kde.org/people/people.html">weekly series</a>. Have a thankful thought for her work while you enjoy Simon's answers.
<!--break-->
