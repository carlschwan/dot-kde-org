---
title: "Torvalds Lauds KDE"
date:    2001-08-30
authors:
  - "Dre"
slug:    torvalds-lauds-kde
comments:
  - subject: "I must agree"
    date: 2001-08-30
    body: "From someone that can't run kernel 2.4 because it freezes when I mount my cdroom on redhat 7.1 I must say I agree, KDE is the most noticeable improvement on linux this year.\nCongrats KDE team and all KDE apps developers, coordinators, translators, and helpers!"
    author: "Iuri Fiedoruk"
  - subject: "Re: I must agree"
    date: 2001-09-01
    body: "hmh, i had similar problems with kernel 2.4,\njust try 2.4.9, all my problems have been\nsolved in 2.4.9."
    author: "hk"
  - subject: "Re: I must agree"
    date: 2001-09-07
    body: "This is usually due to Linux trying to use DMA disk access mode on a CD drive that does not truely support DMA.\n\nTo disable this, you can pass an option to the kernel something like \"ide=nodma\".  If you know which drive the problem is occuring on, you can use hdparm to disable DMA on that drive.\n\nAccording to Red Hat, the problem is due to some drives claiming to support DMA, despite not truely supporting it.  There is a blacklist of these drives in the Linux kernel, and naturally more recent kernels will have more up-to-date blacklists."
    author: "Michael Wardle"
  - subject: "Recruit Linus to KDE core team"
    date: 2001-08-30
    body: "Why not recruit Linus to the KDE core team? If Linus works for KDE, he would not be the only Linux kernel developer working for a DE..."
    author: "Loranga"
  - subject: "Re: Recruit Linus to KDE core team"
    date: 2001-08-30
    body: "Um, not quite, I believe that Alan Cox did/does a bit of work on G'NOME..."
    author: "David Johnson"
  - subject: "Re: Recruit Linus to KDE core team"
    date: 2001-08-30
    body: "he said \"would not be,\" I think you misread :)"
    author: "dingodonkey"
  - subject: "Re: Recruit Linus to KDE core team"
    date: 2001-08-31
    body: "Because Linus only knows C"
    author: "ac"
  - subject: "Re: Recruit Linus to KDE core team"
    date: 2001-08-31
    body: "Ah, he can see."
    author: "Dr No"
  - subject: "Re: Recruit Linus to KDE core team"
    date: 2001-09-01
    body: "Actually, some very early (unreleased?) version of the Linux kernel was written in C++. Then Linus rewrote it in plain C because g++ at that time was a load of crap."
    author: "joib"
  - subject: "Re: Recruit Linus to KDE core team"
    date: 2001-09-01
    body: "no, some earlier versions of the kernel were compiled in c++ mode, using some basic c++ features, but after much complaining about code-generations issues with g++ at the time the makefiles were switched back to c.  saying that it was written in c++ is incorrect.  however I do not doubt that Linus can write c++ code if he wants."
    author: "David"
  - subject: "Give GNOME it's due [Re: Recruit Linus to KDE]"
    date: 2001-09-04
    body: "For the record several Gnome Hackers also do Kernel work.  Most noteble is the project leader.\n\nThe folowing was extracted from '/usr/src/linux/CREDITS'\n\nN: Miguel de Icaza Amozurrutia\nE: miguel@nuclecu.unam.mx\nD: Linux/SPARC team, Midnight Commander maintainer\nS: Avenida Copilco 162, 22-1003\nS: Mexico, DF\nS: Mexico\n\nWhile I was browsing there I also stumbled accross. \n\nN: Raymond Chen\nE: raymondc@microsoft.com\nD: Author of Configure script\nS: 14509 NE 39th Street #1096\nS: Bellevue, Washington 98007\nS: USA\n\nGo figure :)\nPS : It is monumentaly rude to send trivial and frivulus Email to anyone.  Especialy free software contributer."
    author: "Forge"
  - subject: "This is not news!"
    date: 2001-08-30
    body: "\"No fake - I'm a big fan of konqueror, and I use it for everything.\" -- Linus Torvalds\n\n:-)"
    author: "Navindra Umanee"
  - subject: "Re: This is not news!"
    date: 2001-08-30
    body: "You got me there! :)\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: This is not news!"
    date: 2001-08-31
    body: "Navindra, you should give the link :-) :\n<br><br>\n<a href=\"http://bugs.kde.org//db/27/27340.html\">http://bugs.kde.org//db/27/27340.html</a>"
    author: "Philippe Fremy"
  - subject: "Re: This is not news!"
    date: 2001-08-31
    body: "So this is at least _one thing_ I have in common with Linus. Makes me proud. Made my day! :-)"
    author: "Hans Aschauer"
---
During a panel discussion at <A HREF="http://www.linuxworldexpo.com/">LinuxWorld Expo</A> here in San Francisco, CA, Linus Torvalds said that the biggest development in Linux this past year has been the desktop, as reported in this <A HREF="http://dailynews.yahoo.com/h/cn/20010829/tc/making_linux_usable_tops_tovalds_list_1.html">story</A>.   <EM>"Within the last year, it's progressed past the eye-candy stage," he said during a panel discussion at the LinuxWorld Conference and Expo, praising the KDE user interface and higher-level applications such as KOffice.</EM>  Another happy KDE user :-).