---
title: "People of KDE: Matthias \"Kalle\" Dalheimer"
date:    2001-11-26
authors:
  - "Inorog"
slug:    people-kde-matthias-kalle-dalheimer
comments:
  - subject: "great stuff"
    date: 2001-11-26
    body: "As a devoted KDE (&Linux) supporter I really enjoy the new interview series. ThanX to the commited KDE developers who are responsible for the desktop revolution."
    author: "Dominik"
  - subject: "Damn you KDE! :)"
    date: 2001-11-26
    body: "Damn you KDE, for you are turning me into a GUI user.\nI swear to god, I used to despise gui's.\n(That may not be entirely true, probably only the windows gui)\nBut anyway, all these nifty features make me so lazy, and they \"deconsole\" me.\nI just want to say you are the best of the best.\n(Gnome developers also, no, better anyone coding for linux).\nSo thank you so much for spending your time working very hard on KDE.\nIf I ever win the lotto, first thing I'll do is donate something to KDE :)"
    author: "the red in the sky is KDE's"
  - subject: "nice"
    date: 2001-11-26
    body: "Nice interview.. great that it was Kalle, he was very important in the early history of KDE, and his contributions are still very important to this day. \n\nand also, he was responsible for many of the current kde developers being kde developers. many of them read kalle's c't article and heard about kde for the first time."
    author: "hm"
  - subject: "Same old picture... :)"
    date: 2001-11-26
    body: "Isn't it time to have a new picture of Kalle? It must be at least four years old. But maybe he looks like the picture even today :)\nhttp://www.kde.org/people/images/kalle.jpg"
    author: "Loranga"
  - subject: "quality"
    date: 2001-11-26
    body: "Thank you, Tink! The questions have become a *LOT* better than those before (I can imagine its very hard to find good questions, but you really did a good job!)\n\nUnd herzliche - 13 Tage versp\u00e4tete - Gl\u00fcckw\u00fcnsche an den zweifachen Vater!!!"
    author: "me"
  - subject: "Kword and Kspread"
    date: 2001-11-26
    body: "I feel strange these two answers :\n\n> ~ What is missing badly in KDE?\n> A Publisher-like program that makes it very easy to create all kinds of stationery, labels, etc.\n\nI thought that KWord is both Word and Publisher-like. Of course all the functionnalities are not still here, but it seems very well started...\n\n> ~ Which section of KDE is underrated and could get more publicity?\n> KOffice, especially KSpread.\n\nIn Koffice, I think that KSpread is in late (for instance comparing with Kword or KPresenter). So I don't feel false that it is \"underrated\". But with a good KChart (thanks !) and a good keyboard navigation in words, it will change, of course..."
    author: "Alain"
  - subject: "Re: Kword and Kspread"
    date: 2001-11-26
    body: "M$ Publisher may be called a document publishing system, but in my experience, it's always being used for it's built-in greeting cards, calendars, small posters, and other such stuff. Those who want a real publishing system for things like newsletters tend to gravitate towards other, more expensive commercial software, or just use M$ Word. KWord is a nice word-processor, but it can't do greeting cards yet. :-)"
    author: "Carbon"
  - subject: "Re: Kword and Kspread"
    date: 2001-11-26
    body: "> M$ Publisher may be called a document publishing system, but in my experience, it's always being used for it's built-in greeting cards, calendars, small posters, and other such stuff. Those who want a real publishing system for things like newsletters tend to gravitate towards other, more expensive commercial software, \n\nHmmm, I disagree. I see my own experience (and some others I know)... I used at first Pagemaker and then Publisher (because it was less expensive and did most things I needed). Publisher is called Publisher because it is a small, honest, well done document publishing system. You are wrong when you say \"it's always being used for it's built-in greeting cards, calendar\". Also, it is used as a web editor, but, here, it is bad...\n\nI think that KWord, in future versions, is able to do what Publisher does (as document publishing...) but I don't think it can one day do what Pagemaker does (and I don't think it is necessary and useful). \n\nToday, the big lack of KWord as a document publishing system is that it does'nt have a desktop (or table) around the sheet (for moving some texts, images...). And also it lacks hyphenation. And also there is no \"forced justification\" (\"justification forc\u00e9e\" in french) (available in Pagemaker, not in Publisher).\n\nAbout \"built-in greeting cards, calendar\", yes, it is not in KWord now, but I think it will be able to do this in future versions, it is better that using another program, no ?"
    author: "Alain"
  - subject: "Re: Kword and Kspread"
    date: 2001-11-26
    body: "You're probably right. My experience with Publisher is limited to school-curriculm computing. They bought about 30 Publisher licenses at about $40 each or so w/ school discount, then proceeded to use it entirely for greeting cards and little one page \"newsletters\" based on templates, and they had apparently had someone come in and rip up the UI's defaults a bit to make it look specialized for that sort of thing. It doesn't surprise me they were missing the point of the program, now that I think about it."
    author: "Carbon"
  - subject: "Languages"
    date: 2001-12-01
    body: "Hmm... I have great respect for people who have taken the time to learn other languages, but how many of them does Kalle know anyhow? If he can read a menu and understand it in all of Europe, that's got to be a lot! I, at least, would be completely lost with a Hungarian menu, for instance. Even if I consider myself moderately civilized with six languages."
    author: "Language freak"
---
Let the drums roll, there is reason to rejoice. <a href="mailto:tink@kde.org">Tink</a> is back with her insightful interviews with the <a href="http://www.kde.org/people/people.html">People of KDE</a>. Tink has updated the set of questions, and notes that she has prepared an impressive list of future interviews. The launch of the new interview season features <a href="http://www.kde.org/people/kalle.html">Kalle</a> (yes, the one and only Matthias "Kalle" Dalheimer). Thanks to Tink for her great work. We hope you enjoy the new series.

<!--break-->
