---
title: "KDE Accessibility Mailing List Started"
date:    2001-01-12
authors:
  - "jschnapper-casteras"
slug:    kde-accessibility-mailing-list-started
comments:
  - subject: "Re: KDE Accessibility Mailing List Started"
    date: 2001-01-12
    body: "This is great!  :)\n\nThis isn't about getting KDE to more people, this is about getting the wonderfull world of computers and the internet out to more people, people that have different disabilities and therefore might find computing to be hard.\n\nGO KDE!  :)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: KDE Accessibility Mailing List Started"
    date: 2001-01-15
    body: "OOH! I want my cat to be able to use KDE!\n<p>\nGreat!\n<p>\nJason"
    author: "Jason Katz-Brown"
  - subject: "I am mostly init because my love is blind :)"
    date: 2001-01-15
    body: "6 years ago I was sent by my then employer to perform a minor repair job at UWI ( Jamaica's largest university ).  While there I was asked to help out a young lady who was having a little trouble with her speech synthesizer.  One thing led to another and I wound up involved the blind community.  Latter on I fell for her.\n\nA few things became apparent.  Number one is that a blind person with a masters degree can't hold a decent job if s/he doesn't have speech synthesis.  Number two is that this speech stuff is expensive, unreliable and temperamental.\n\nExpensive means $800 for a hardware synthesizer and another grand for the software to run it.  Lately the most popular such software ( JAWS - http://www.hj.com ) has been available as shareware, which means it works for 45 minutes then you must reboot to continue using your PC.  This is the version many people have been using.\n\nUnreliable comes from being wired into the most finicky windows subsystems.  the video driver, the desktop etc...  This stuff will fall down for no apparent reason and with little warning.\n\nBasically software synthesis and a proper interface on Linux would bring the cost of setting up a blind staff member near to that for a sighted one.  I.e. lower the specks on the monitor and video card to absolute minimum and add a scanner and better speakers + headphones and your set.\n\nThis assumes of course that you can set this person up with free software.  Free Software that DOSE NOT BREAK."
    author: "Forge"
---
A <a href="http://master.kde.org/mailman/listinfo/kde-accessibility">KDE accessibility mailing list</a> has been created. We hope the mailing list will be the starting point for a subproject of KDE, possibly named Access KDE, that will be dedicated to making the entire desktop environment accessible.  The mailing list will serve as a forum for those who want to further KDE's accessibility to disabled users (e.g., visually impaired or low-vision individuals). Those interested  are strongly encouraged to subscribe.
<!--break-->
<br>We aim to make KDE, applications included in the desktop package, and applications designed to run on KDE usable to all. Furthermore, we want to allow developers to discuss what changes need to be made to the KDE core, to its libraries, and to the toolkit on which it is based (Qt).&nbsp; We also want to encourage users to describe problems they are having accessing KDE so that developers can find solutions.&nbsp; Finally, we wish to use this list to collaborate with other free software projects, organizations, and individuals in order to help tie in our efforts at increasing KDE usability with the <a href="http://ocularis.sourceforge.net">larger
goal</a> of making Linux and the free software world accessible to all.
<br>
<br>Suggestions made and work done on the KDE-Accessibility mailing list will be reflected in the upcoming <a href="http://ocularis.sourceforge.net//story.php?storyid=2000/12/15/18/">Linux Accessibility Conference</a>.