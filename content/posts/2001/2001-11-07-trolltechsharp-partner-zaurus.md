---
title: "Trolltech/Sharp Partner on Zaurus"
date:    2001-11-07
authors:
  - "Dre"
slug:    trolltechsharp-partner-zaurus
comments:
  - subject: "Offline browsing"
    date: 2001-11-07
    body: "I want to browse downloaded websites (or parts of sites) offline on my PDA, if I buy one.\nReading mail and newsgroups offline would be nice too.\nBut it's only a deam, I guess, it will never happen."
    author: "reihal"
  - subject: "Why not?"
    date: 2001-11-07
    body: "If this thing can run wwwoffle, it should be possible."
    author: "Claes"
  - subject: "Re: Why not?"
    date: 2001-11-08
    body: "Sitescooper is cool too, it's one of my favorite small but ultra-handy perl apps.\n\nAnd of course, wget is always handy! :-)"
    author: "Carbon"
  - subject: "Looks very nice"
    date: 2001-11-07
    body: "This thing looks very nice! But as always with small devices, one problem: there are not enough keys for the extra characters in the swedish (and I guess many others) alphabet. I will never buy a PDA with keyboard unless it has got a reasonable keyboard layout."
    author: "Claes"
  - subject: "Availability and pricing?"
    date: 2001-11-07
    body: "When will this beauty be in stores? Where do I sign up for a development model? What's the price tag? I want one for Christmas!"
    author: "Rob Kaper"
  - subject: "This is difficult"
    date: 2001-11-07
    body: "If I got it right, Sharp sells the SL5000 with a preinstalled Lineo Embedix Linux and a Java Engine on top of it (in ROM). NO QT/Embedded.\n\nThe Trolls just fiddled around with this PDA and figured out how to get their\nPDA-Environment to run on this (nice lookin) PDA. That's all...\n\nBut me as stupid customer want to buy it with QT/Embedded as preinstalled system....\n\nAm I completely wrong? Did I misunderstood the (little) Sharp-SL5000 information I read on Sharps website?"
    author: "Thomas"
  - subject: "erm..."
    date: 2001-11-07
    body: "What I mean:\n\nO.k. there's the Qt AWT, so you can do your programming in Java, but it has\nnative look n' feel of Qt. \n\nThe preinstalled apps (like the organizer or the mailer): Are they written in Qt/C++ which is all known to us ...or in Java? Will you be able to still use C++ for development _without_ major modifications to the PDA's OS and Desktop Environment settings?\nAm I right, you can choose Konqui for browser (QT/C++)?\n\nOr do you have to get along with Java?\nJava seems to work out of the box, \nQT/C++ needs what? (a crosscompiler?)"
    author: "Thomas"
  - subject: "Re: This is difficult"
    date: 2001-11-07
    body: "Qt embedded is already installed (the device was shown at \"Systems\" fair in Munich in October, with Qt). The web pages seems to be not up to date.\nSharp has recognized the power of QPE and switched over in the last weeks.\nDeveloper releases should be available in November for around 399$, the target for the final devices is march (with a higher price?).\nAccording to there homepage http://www.lisa.de/ is also involved in application development."
    author: "Frank"
  - subject: "Re: This is difficult"
    date: 2001-11-08
    body: "Actually, this device ships with Qt/Embedded and Qt/Palmtop pre-installed and there is a development kit to develop Qt applications.  The Java engine runs on top of Qt/Embedded.\n\nMartin Jones,\nTrolltech."
    author: "Martin Jones"
  - subject: "KOffice/embedded"
    date: 2001-11-07
    body: "This is indeed a good platform for KOffice. I think that for using KOffice as main Word Processor on the desktop is lacks some major features. Star/Open Office does this job much better. Is there anybody working on a port of Koffice to qpe ?\n\nHancomoffice is also working on an office suite for Qpe. <http://www.hancom.com/en/product_service/embedded0905.html>\n\n  -b."
    author: "brambi"
  - subject: "Re: KOffice/embedded"
    date: 2001-11-11
    body: "Wouldn't it make more sense to make a KPE and have some sort of standardized flag that lets apps be aware that they are on a handheld format screen (change the menus and toolbars)?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "\"zaurus\" - isn't that out already?"
    date: 2001-11-07
    body: "I travelled to japan about half a year ago and I saw a PDA called \"Zaurus\" in stores everywhere. Is that the same? Didn't know it was running Linux..."
    author: "svara"
  - subject: "Re: \"zaurus\" - isn't that out already?"
    date: 2001-11-07
    body: "The japanise Zaurus in the same hardware but with Sharp OS, no Linux there."
    author: "Janne"
  - subject: "Re: \"zaurus\" - isn't that out already?"
    date: 2001-11-07
    body: "Actually I think its got an SH cpu instead of an ARM proccessor that the US version will have."
    author: "Anonymous Clippy"
  - subject: "I just got mine in"
    date: 2001-11-07
    body: "Arrived about an hour ago, we are working on porting Kivio mp to it right now.  It is a sweet unit, I hope they forget they sent it to me :).  It's much nicer than the iPaq and smaller, it's just a touch larger than my Handspring Visor.  There are some humorous misspellings in some of the current dialog boxes.\n\nShawn"
    author: "Shawn Gordon"
  - subject: "Re: I just got mine in"
    date: 2001-11-07
    body: "Wow, that's gotta be the best part of your job, getting to play with those gadgets! If it only included a cellphone, I'd have ordered mine by now...\n\nBut Shawn, to be honest, I think Kivio should be the very last thing to port to a handheld. I haven't done too much diagramming, but i found out that it takes quite a lot of screenspace to create bigger diagrams, since you tend to quickly lose overview.\n\nDon't you think that aethera would be a better idea?"
    author: "me"
  - subject: "Re: I just got mine in"
    date: 2001-11-07
    body: "Actually this is the first decent thing I've gotten in.\n\nAs far as applications, out of all the things we have, Kivio seems the most appropriate, if not particularly appropriate.  We're going to look at Aethera too when we finish the Qt port later this week, but I think it's too fat for this device.\n\nI'm open to suggestions for products that make sense though.\n\nshawn"
    author: "Shawn Gordon"
  - subject: "Re: I just got mine in"
    date: 2001-11-07
    body: "Kapital"
    author: "All_troll_no_tech"
  - subject: "Re: I just got mine in"
    date: 2001-11-07
    body: "kapital is too fat and needs KDE - on a palm you just want something lite weight to keep certain information and then sync up with."
    author: "Shawn Gordon"
  - subject: "Re: I just got mine in"
    date: 2001-11-07
    body: "how about making it able to sync with Aethera...\n\nthat would be sweet\n(oh, and i would REALLY like to see a finished Aethera....)\n\nKivio would be pretty cool, I installed a diagramming\nprogram on my ipaq called First and it wasn't as bad\nas you would expect it to be with the limited screen space.\n\nI did order one of these already(developers adddition) \nI hope they ship pretty soon!"
    author: "Brad C."
  - subject: "Re: I just got mine in"
    date: 2001-11-08
    body: "well I know you here this a lot, but early next week will have the new Aethera with the new UI and plug in architecture with a couple of commercial plug ins to follow a couple weeks later."
    author: "Shawn Gordon"
  - subject: "Re: I just got mine in"
    date: 2001-11-08
    body: "This is a bit OT, but why in the world did you guys decide to make custom toolbar icons for Aethera, even for standard functions like 'print'? Or, have those been removed?"
    author: "Carbon"
  - subject: "Re: I just got mine in"
    date: 2001-11-08
    body: "You think I've got time to check every feature and function when I'm so busy following these talkbacks? :)\n\nSeriously, I'm not sure what the original situation was, or what the current situation is with regard to the icons.  I know we made all our own icons because that's just what we do."
    author: "Shawn Gordon"
  - subject: "Re: I just got mine in"
    date: 2001-11-10
    body: "Would you please consider an option to make Aethera use KDE's icons? I'd love to see Aethera's look mirror my KDE desktop as *closely* as possible.\n\n  Thanks,\n       Tim"
    author: "Timothy R. Butler"
  - subject: "Re: I just got mine in"
    date: 2001-11-08
    body: "Since you are the first person I know that has the device, could you give us a mini-review :)?\n\nHow is the keyboard? Does it have a good feel to it (I guess Psion Revo has spoiled me when it coms to PDA-keyboards). How about the screen? Is it good? How fast is the device? Any slowdowns?"
    author: "Janne"
  - subject: "Re: I just got mine in"
    date: 2001-11-08
    body: "The device is slightly longer than my Visor.  Qpe feels like a cross between Palm OS and WinCE, I'm not totally sure I like it yet, I don't care for the handwriting recognition on it at all, but the keyboard is pretty nice.  You're going to be using your thumbs to type basically, and there is a enough room on there so it's not a problem.\n\nThere is a version of Opera on there, but I can't think of anything more painful than surfing the net on a screen this size, but at least there is a keyboard, trying to type a URL takes about 5 to 10 minutes (seriously, I tried it on a larger test unit some months ago).\n\nThe display is very nice and all the applications I brought up were very zippy, I was pleased with the performance.  So at this stage I would give it a positive review."
    author: "Shawn Gordon"
  - subject: "Um..."
    date: 2001-11-08
    body: "What the hell is this site, a forum for free advertising for TrollTech?"
    author: "Mr. Guy"
  - subject: "Re: Um..."
    date: 2001-11-08
    body: "Troll-detection... *bip bip*"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Why not a USB port? and video out??"
    date: 2001-11-10
    body: "I wanna plug in a keyboard and monitor and use this like a computer ;-) OK really I just wanna be able to use it for doing presentations instead of lugging a laptop around and using windbloze and F!@#$%@#$%#$ING PowerPoint"
    author: "SVG wanter"
  - subject: "Re: Why not a USB port? and video out??"
    date: 2001-11-10
    body: "you are a twit sir!\n\nEven if those things were available in hardware (and if they wer the think would be half the size of a laptop by that point) what uhh **APPLICATION** would you use for presentations? \n\nStaroffice -- nope\nHanoffice -- nope\nGobe -- nope\nKOffice -- nope \n\n.... nope nope nope"
    author: "Anti-Twit"
  - subject: "Re: Why not a USB port? and video out??"
    date: 2001-11-10
    body: "Hmm... maybe the port for syncing could have an add on for video out? If KOffice could (is being?) ported, and so is HancomOffice, such an idea would be kinda neat.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Why not a USB port? and video out??"
    date: 2004-05-03
    body: "http://www.shirtpocket.co.uk/cat/product_info.php?products_id=89"
    author: "Damion"
  - subject: "Re: Why not a USB port? and video out?? Look here"
    date: 2001-11-12
    body: "LOOK HERE:\n\nhttp://www.thinkgeek.com/stuff/gadgets/57a3.shtml\n\n... use of Qt is optional ;-)"
    author: "My other email address"
  - subject: "zaurus"
    date: 2001-11-11
    body: "As soon as I can help it, gutenbrowser is also going to run on it. This thing looks Kool! Never had a pda before- really never had a reason to have one. \nC'mon December!! :D\n\nljp"
    author: "ljp"
---
<A HREF="http://www.trolltech.com/">Trolltech</A> and
<A HREF="http://www.sharp-usa.com/">Sharp</A> have
<A HREF="http://www.trolltech.com/company/announce.html?Action=Show&AID=82">announced</A>
a really <A HREF="http://www.trolltech.com/products/palmtop/sharpimg.html">spiffy-looking</A>
Linux palmtop, named &quot;Zaurus&quot;.
The device itself <A HREF="http://developer.sharpsec.com/">features</A>
a sliding (retractable) keyboard, a color
display, a CF expansion slot (for memory or peripherals), an
SD expansion slot (for secure memory storage or other peripherals),
an IR port, a USB connector and a headset port.
On the software side, the Zaurus uses
<A HREF="http://www.lineo.com/">Lineo</A>'s
<A HREF="http://www.lineo.com/products/embedix/">Embedix Linux</A>;
Trolltech's <a href="http://www.trolltech.com/products/embedded/screenshots.html">Qt/Embedded</a>, <a href="http://www.trolltech.com/products/palmtop/screenshots.html">Qt/Palmtop</a> and Qt AWT GUI technologies;
<A HREF="http://www.insignia.com/noscript.htm">Insignia Solution</A>'s
<A HREF="http://www.insignia.com/products/jeode_1.7.asp">Jeode PDA Edition</A>;
and <A HREF="http://www.opera.com/">Opera Software</A>'s embedded web browser.
Sharp is accepting pre-orders from the developer community for the
SL-5000D developer unit (register
<A HREF="http://developer.sharpsec.com/join.cfm?Blue=RE">here</A>).
With the continuing additions to kdenox, this might just be
a great platform for KOffice/embedded.
<!--break-->
