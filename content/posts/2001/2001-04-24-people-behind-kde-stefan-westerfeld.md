---
title: "People Behind KDE:  Stefan Westerfeld"
date:    2001-04-24
authors:
  - "Dre"
slug:    people-behind-kde-stefan-westerfeld
comments:
  - subject: "Re: People Behind KDE:  Stefan Westerfeld"
    date: 2001-04-24
    body: "Now if only someone would tell me why artsd chokes on a stock 7.2 Mandrake after having compiled everything from source (bug reported ages ago) (kernel 2.4.3, again compiled locally):\n\nrubberducky.hall(15)> artsd\nartsd: Symbol `__vt_Q24Arts16SoundServer_base.Q24Arts22SimpleSoundServer_base' has different size in shared object, consider re-linking\nartsd: Symbol `__vt_Q24Arts7Factory' has different size in shared object, consider re-linking\nartsd: Symbol `__vt_Q24Arts22PlayObjectFactory_skel.Q24Arts11Object_base' has different size in shared object, consider re-linking\nartsd: Symbol `__vt_Q24Arts16SoundServer_base.Q24Arts11Object_base' has different size in shared object, consider re-linking\nartsd: error in loading shared libraries: artsd: undefined symbol: _t24__default_alloc_template2b1i0.free_list"
    author: "EKH"
  - subject: "Re: People Behind KDE:  Stefan Westerfeld"
    date: 2001-04-24
    body: "I apologize - that had the wrong airs to it as I just posted it quickly. A big thank you again to all developers! KDE has been the first and only thing to get me over from the CLI world!"
    author: "EKH"
  - subject: "Re: People Behind KDE:  Stefan Westerfeld"
    date: 2001-04-24
    body: "What? Free software is good, but free sex just sucks. We all realize thet, admit it or not. That's why we know terms like \"bitch\" and \"bastard\". Only, there isn't term \"bitch\" for male. It's unfair, isn't it."
    author: "My-My"
  - subject: "Re: CVS as of yesterday don't compile here"
    date: 2001-04-24
    body: "g++ -DHAVE_CONFIG_H -I. -I. -I../.. -I../../arts/mcop -I../../arts/flow -I../../arts/flow -I../../dcop -I../../libltdl -I../../kdecore -I../../kdeui -I../../kssl -I/usr/lib/qt-2.3.0/include -I/usr/X11R6/include -I/opt/kde2.1/include -D_REENTRANT -O2 -fno-exceptions -fno-check-new -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -Wno-long-long -Wnon-virtual-dtor -fno-builtin -frtti -DQT_NO_TRANSLATION -DQT_CLEAN_NAMESPACE -DQT_NO_COMPAT -DQT_NO_ASCII_CAST -c libkmedia2_la_closure.cc  -fPIC -DPIC -o .libs/libkmedia2_la_closure.o\ng++ -O2 -fno-exceptions -fno-check-new -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -Wno-long-long -Wnon-virtual-dtor -fno-builtin -frtti -DQT_NO_TRANSLATION -DQT_CLEAN_NAMESPACE -DQT_NO_COMPAT -DQT_NO_ASCII_CAST -o .libs/libkmedia2.la.closure .libs/libkmedia2_la_closure.o .libs/fileinputstream_impl.o .libs/stdoutwriter_impl.o  ./.libs/libkmedia2_idl.so /home/develop/cvsup/kde/kdelibs/arts/flow/.libs/libartsflow.so -L/usr/lib/gcc-lib/i386-redhat-linux/2.96 -L/usr/lib/gcc-lib/i386-redhat-linux/2.96/../../.. -L/usr/X11R6/lib -L/usr/lib/qt-2.3.0/lib -L/opt/kde2.1/lib ../../arts/flow/.libs/libartsflow.so /home/develop/cvsup/kde/kdelibs/arts/flow/.libs/libartsflow_idl.so /home/develop/cvsup/kde/kdelibs/arts/mcop/.libs/libmcop.so -lpthread -ldl /opt/kde2.1/lib/libaudiofile.so -lstdc++ -lm -lc -lgcc -Wl,--rpath -Wl,/opt/kde2.1/lib\n.libs/fileinputstream_impl.o: In function `Arts::FileInputStream_impl::seek(long)':\n.libs/fileinputstream_impl.o(.Arts::FileInputStream_impl::gnu.linkonce.t.seek(long)+0x113): undefined reference to `Arts::FileInputStream_impl::PACKET_SIZE'\n.libs/fileinputstream_impl.o: In function `Arts::FileInputStream_impl::request_outdata(Arts::DataPacket<unsigned char> *)':\n.libs/fileinputstream_impl.o(.Arts::FileInputStream_impl::gnu.linkonce.t.request_outdata(Arts::DataPacket<unsigned char> *)+0xc7): undefined reference to `Arts::FileInputStream_impl::PACKET_SIZE'\ncollect2: ld returned 1 exit status\nmake[3]: *** [libkmedia2.la.closure] Error 1\nmake[3]: Leaving directory `/home/develop/cvsup/kde/kdelibs/arts/soundserver'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/home/develop/cvsup/kde/kdelibs/arts'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/home/develop/cvsup/kde/kdelibs'\nmake: *** [all-recursive-am] Error 2"
    author: "mikka"
  - subject: "Re: CVS as of yesterday don't compile here"
    date: 2001-04-24
    body: "Can Stefan not have just one day? :)"
    author: "Charles Samuels"
  - subject: "Re: CVS as of yesterday don't compile here"
    date: 2001-04-24
    body: "i'm a bastard inside :-)"
    author: "mikka"
  - subject: "Re: CVS as of yesterday don't compile here"
    date: 2001-04-24
    body: "Fixed."
    author: "Guillaume Laurent"
  - subject: "Re: CVS as of yesterday don't compile here"
    date: 2001-05-02
    body: "That is the most beautiful thing anyone can say about any KDE bug :-)"
    author: "Carbon"
  - subject: "Re: People Behind KDE:  Stefan Westerfeld"
    date: 2001-04-24
    body: "aRts is excellent! Now all we need is more apps that use it since soundwrapping doesn't work for some of those naughty apps! Keep up the good work."
    author: "Con Kolivas"
  - subject: "What apps? It should ALWAYS work."
    date: 2001-04-24
    body: "It works with realplay, aviplay, xmms and quake.\n\nOf course nobody would play quake with a sound lag.\n\nYou just add e.g \"artsdsp realplay %u\" to the realplayer menue entry and it works as if it were created for arts. IMHO, this should even be the default."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: What apps? It should ALWAYS work."
    date: 2001-04-26
    body: "Try pmfax lite - it doesn't like artsdsp.\nYes I know it's a binary only commercial app. But I want an answering machine and kvoice has been deprecated and no longer works with kde2 anyway."
    author: "Con Kolivas"
  - subject: "Re: What apps? It should ALWAYS work."
    date: 2002-11-29
    body: "Hi!\n\nMy name is Alvaro and i would like to work with kvoice in my university. The problem is that i haven\u0092t any information about it. Do you know where could i take a manual? Do you have it?\n\nthanks from Spain\n"
    author: "Spain"
  - subject: "Re: People Behind KDE:  Stefan Westerfeld"
    date: 2001-04-24
    body: "aRts is excellent! Now all we need are more apps that use it since soundwrapping doesn't work for some of those naughty apps! Keep up the good work."
    author: "Con Kolivas"
  - subject: "Re: People Behind KDE:  Stefan Westerfeld"
    date: 2001-04-26
    body: "Why is the system bell so slow to react?"
    author: "Joe"
  - subject: "Artsd chokes no matter what"
    date: 2001-11-08
    body: "Is aRts ever going to be something stable.  I did a search on google for aRtsd and found nothing but it not working.  I sure do love open source software and Linux and UNIX but sound under KDE or GNome does not work on my stock Redhat 7.2 system.  It seemed to work ok under 7.1 but really tore into my cpu and now it just rips into it so much that it crashes.\n\nThese are the debug info that I get:\n(no debugging symbols found)...(no debugging symbols found)...\n0x40e29989 in __wait4 () from /lib/i686/libc.so.6\n#0  0x40e29989 in __wait4 () from /lib/i686/libc.so.6\n#1  0x40ea5534 in __DTOR_END__ () from /lib/i686/libc.so.6\n#2  0x405f8838 in KCrash::defaultCrashHandler () at eval.c:41\n#3  <signal handler called>\n#4  0x40d9dae1 in __kill () from /lib/i686/libc.so.6\n#5  0x40d9d8ba in raise (sig=6) at ../sysdeps/posix/raise.c:27\n#6  0x40d9f062 in abort () at ../sysdeps/generic/abort.c:88\n#7  0x406aa86b in FAMDebugLevel () at ../../gcc/libgcc2.c:3034\n#8  0x406aa88a in FAMDebugLevel () at ../../gcc/libgcc2.c:3034\n#9  0x4007542b in __pure_virtual () from /usr/lib/libkparts.so.1\n#10 0x41168c45 in Arts::Dispatcher::connectUrl () from /usr/lib/libmcop.so.0\n#11 0x41168973 in Arts::Dispatcher::connectObjectRemote ()\n   from /usr/lib/libmcop.so.0\n#12 0x410409f7 in Arts::SoundServerV2_base::_fromReference ()\n   from /usr/lib/libsoundserver_idl.so.0\n#13 0x410403ac in Arts::SoundServerV2_base::_fromString ()\n   from /usr/lib/libsoundserver_idl.so.0\n#14 0x41017234 in KNotify::connectSoundServer () from /usr/lib/knotify.so\n#15 0x410146c0 in KNotify::notifyBySound () from /usr/lib/knotify.so\n#16 0x41014491 in KNotify::notify () from /usr/lib/knotify.so\n#17 0x41018280 in KNotify::process () from /usr/lib/knotify.so\n#18 0x40032224 in DCOPClient::receive () at eval.c:41\n#19 0x40024570 in DCOPProcessInternal () at eval.c:41\n#20 0x4002411f in DCOPProcessMessage () at eval.c:41\n#21 0x4003d54a in KDE_IceProcessMessages () at eval.c:41\n#22 0x400386d3 in DCOPClient::processSocketData () at eval.c:41\n#23 0x40895c81 in QObject::activate_signal ()\n   from /usr/lib/qt-2.3.1/lib/libqt.so.2\n#24 0x408eaa76 in QSocketNotifier::activated ()\n   from /usr/lib/qt-2.3.1/lib/libqt.so.2\n#25 0x408c8f67 in QSocketNotifier::event ()\n   from /usr/lib/qt-2.3.1/lib/libqt.so.2\n\n#26 0x40845891 in QApplication::notify () from /usr/lib/qt-2.3.1/lib/libqt.so.2\n\n#27 0x4055fc89 in KApplication::notify () at eval.c:41\n#28 0x4081012b in sn_activate () from /usr/lib/qt-2.3.1/lib/libqt.so.2\n#29 0x40810999 in QApplication::processNextEvent ()\n   from /usr/lib/qt-2.3.1/lib/libqt.so.2\n#30 0x4084774c in QApplication::enter_loop ()\n   from /usr/lib/qt-2.3.1/lib/libqt.so.2\n#31 0x40810197 in QApplication::exec () from /usr/lib/qt-2.3.1/lib/libqt.so.2\n#32 0x41010d32 in main () from /usr/lib/knotify.so\n#33 0x0804d0c4 in strcpy ()\n#34 0x0804f462 in strcpy ()\n#35 0x40d8b627 in __libc_start_main (main=0x804ef70 <strcpy+16888>, argc=3,\n    ubp_av=0xbffffb04, init=0x804a4d0 <_init>, fini=0x804fc60 <_fini>,\n    rtld_fini=0x4000dcd4 <_dl_fini>, stack_end=0xbffffafc)\n    at ../sysdeps/generic/libc-start.c:129\n\nIs there anything that we can do to get it up and running atleast until we get the software fixed?"
    author: "Caleb"
---
This week's installment of <A HREF="mailto:tink@kde.org">Tink's</A> <A HREF="http://www.kde.org/people/people.html">People Behind KDE</A> features <A HREF="http://www.kde.org/people/stefanw.html">Stefan Westerfeld</A>, the creator of KDE's multimedia architecture based on <A HREF="http://www.arts-project.org/">aRts</A>.  Besides delving into the usual background, Stefan shares his favorite saying:  <EM>"Software is like sex, it's better when it's free"</EM>.  Amen.
<!--break-->
