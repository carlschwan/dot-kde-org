---
title: "Multihead Support in KDE 2.1"
date:    2001-02-07
authors:
  - "numanee"
slug:    multihead-support-kde-21
comments:
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "Brad, you're my new hero!<p>\nI can now use KFM (as opposed to my hacked BlackBox window manager), and have all the nice things like the desktop, Alt-F2 launch support, etc...<p>\nThankYouThankYouThankYouThankYouThankYou\nThankYouThankYouThankYouThankYouThankYou<p>\nGonna go do some compilin' today...<p>\n--<br>\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "<I>I can now use KFM (as opposed to my hacked BlackBox window manager)</I><br>\n<br>\nMisnamigs aside, isn't that a bit ironic..:)"
    author: "blob"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "Where can I see a screenshot?O a diagram?"
    author: "Soknet"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "Great :)"
    author: "Zeljko Vukman"
  - subject: "It is about time!!"
    date: 2001-02-07
    body: "This is a feature that kept my company from using KDE on our desktops. And now\nthat we are entrechec in SawFish and Gnome\nIt will probably stay that way.  I really like \nKDE and KDE2 but I never understood why no\nmultihead support."
    author: "Tim BRandt"
  - subject: "Re: It is about time!!"
    date: 2001-02-07
    body: "It's a pitty that gnome only ships as ROM-modules so you never ever are able to switch back to KDE without trashing your hardware."
    author: "Lenny"
  - subject: "Re: It is about time!!"
    date: 2001-02-07
    body: "what are you talking about???????????"
    author: "emmanuel"
  - subject: "Re: It is about time!!"
    date: 2001-02-07
    body: "He's either playing dumb or trying to flame/troll.\nThe latter is more likely, considering how many flamers and trolls are travelling around these days."
    author: "Anonymous"
  - subject: "Re: It is about time!!"
    date: 2001-02-07
    body: "Geez, he makes an obviously sarcastic comment about the original commenter's irrational fear of trying to install something different, and people somehow manage to misunderstand him. That doesn't make him a troll."
    author: "ac"
  - subject: "Re: It is about time!!"
    date: 2001-02-09
    body: "oooops!\nyou're obviously right.. my mistake :O(\nas was said, trolls are frequent so..\nsorry about that..\n\nEmmanuel"
    author: "emmanuel"
  - subject: "Re: It is about time!!"
    date: 2001-02-07
    body: "yea... umm... what the hell are you talking about?"
    author: "Roger"
  - subject: "Re: It is about time!!"
    date: 2001-02-07
    body: "Umm.. ditto that."
    author: "Darrell Esau"
  - subject: "Re: It is about time!!"
    date: 2001-02-07
    body: "Yeah, dont you know that once you are entrenched in GNOME you cannot change to another desktop?"
    author: "ac"
  - subject: "Re: It is about time!!"
    date: 2001-02-07
    body: "I was using Gnome, however when KDE 2.0 arrived, it became clear it was time to move to KDE."
    author: "Prefers KDE 2"
  - subject: "Re: It is about time!!"
    date: 2001-02-09
    body: "What the hell are you talking about? I have 3 virtual desktops in Gnome here and I can switch between them easily using the Gnome Pager!"
    author: "bd"
  - subject: "Re: It is about time!!"
    date: 2001-02-10
    body: "I was joking. Being a bit sarcastic. I was also implying desktop environment, not desktop screen."
    author: "ac"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "fine. this is indeed great news. my g400 is awaiting this feature for a long time now...\n\nbut, will it really ship with 2.1 final ? \n\n(read some confusing mails elsewhere about codefreeze)."
    author: "guenther"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "<P>it will ship in 2.1 final (as it is bugfix ;o) ). actually the mailing-list was quite funny to read there: yes! look at this! i found a bug-report for this!<BR>\nTherefore obviously it's a bug we *need* to correct, not a new feature ;o)</P>\nAnyway.. good news.. but does kde already have xinerama support?"
    author: "emmanuel"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "My G400 is so excited about this!\nGoodbye Windows!"
    author: "reihal"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "Does \"Multihead\" mean Xinerama or mirrored-display (same thing displayed on two monitors) or something else?"
    author: "Joe Dorita"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-09
    body: "Neither.\n\nMultihead is a general term to mean the support of several monitors. But there are two ways of doing that.\n\nThe simplest one (NO xinerama) is one X display per monitor. e.g. you can do\nxterm -display :0.0 -> goes on the first monitor\nxterm -display :0.1 -> goes on the 2nd\netc... but you can't move a window from a monitor to another.\n\nThe other one, xinerama, is all the monitors merged into one large X display. E.g. you can freely move one window from a monitor to another.\n\nXinerama is already working for KDE 2.1 (and every version of KDE) as it doesn't require anything special from the application. It just displays on something big.\n\nAs I understand this patch, it means the support of several displays, which is really nice to have as well. (somebody correct me if I'm wrong)."
    author: "Guillaume"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-09
    body: "KDE 2.1 is working with Xinerama, however\nonly on one monitor. You needed a second\nmultihead aware WM (like the awfull e) to\nbe able to use both screens.\n\nHowever, I have compiled everything, put\nenableMultihead=true in Kdeglobals, but I\nstill see no difference. (my xinerama\nworks with e)."
    author: "desperate"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-09
    body: "If it's working only on one monitor, then it means you're not using xinerama :-), just basic multihead.\n\nTry startx +xinerama"
    author: "Guillaume Laurent"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-09
    body: "Puh :) - now I realized, that I wasnt using Xinerama with e. It seems to have automagically\ndetected the second display and extended itself to it, opening a second window manager (or whatever).\n\nBut now I fear that my KDE compilation is broken, because it doesnt seem to be aware of xinerama at all. It opens applications between the two monitors and maximises across both screens. The panel is stretched to the second display, but doesnt show up there, so I cant use the left side, no edge resistance... \n\nIf its not my compilation: Is it possible to open a second KDE instance in the second monitor in basic multihead, not using xinerama?"
    author: "desperate"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-09
    body: "Puh :) - now I realized, that I wasnt using Xinerama with e. It seems to have automagically\ndetected the second display and extended itself to it, opening a second window manager (or whatever).\n\nBut now I fear that my KDE compilation is broken, because it doesnt seem to be aware of xinerama at all. It opens applications between the two monitors and maximises across both screens. The panel is stretched to the second display, but doesnt show up there, so I cant use the left side, no edge resistance... \n\nIf its not my compilation: Is it possible to open a second KDE instance in the second monitor in basic multihead, not using xinerama?"
    author: "desperate"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "Thank you!  My G400 will be pleased.  I've been trying to run Enlightenment w/ KDE 2.1 in order to get some multi-head awareness, but have run into numerous problems w/ virtual desktops working right between the two.  Bummer.  I liked the KDE/E combo."
    author: "Chardros"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "Awesome!!!!\n\nxinerama with KDE 2.0.1 is useable but this will be great!!!\n\nYou're the hero for the day Bradley!"
    author: "Darrell Esau"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "<p>Is it xinerama? Or is it just two seperate desktops?\n\n<p>Anyway, multihead support is great. Up until now, I have been running KDE2 on my primary screen and fvwm2 on my secondary screen. It works, but it isn't pretty, offcourse."
    author: "Erik Hensema"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-28
    body: "Xinerama."
    author: "Darrell Esau"
  - subject: "????"
    date: 2001-02-07
    body: "OK OK, multihead support is great.\n\nBut... what IS multihead?"
    author: "Anonymous"
  - subject: "Re: ????"
    date: 2001-02-09
    body: "some matrox cards (i don't think other manufacturors support that?) have two video outputs.\none for a normal monitor, some for a TFT/flat screen/TV. I hear recent one support two monitors as well.\nOf course this is way cool, and different from Xinerama, which is having two monitors with *two* video cards.\n\nHope this helps,\n\nEmmanuel"
    author: "emmanuel"
  - subject: "Re: ????"
    date: 2001-02-10
    body: "No, xinerama is a form of multihead merging several physical displays into a single logical one.\n\nTwo monitors on two videocards is basic multihead."
    author: "Guillaume Laurent"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "That's pretty cool. But can you make this work for only one monitor? I really can't afford a second one (+ a new graphic card) apart from not having enough space on my (physical) desktop anyway..."
    author: "Anonymous"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-07
    body: "Umm... I suppose it might be possible, but it would certainly be pointless. This isn't about virtual desktops or the like, it's solely about supporting multiple physical monitors correctly (not opening windows with odd overlaps across the boundary, etc). So, if you don't have a multihead setup, it means nothing at all to you."
    author: "Kevin Puetz"
  - subject: "Where to put enableMultihead?"
    date: 2001-02-09
    body: "Hi,\n\nI have compiled the CVS from 7/2. \nThe code is there. \nnow: where do I have to put enableMultihead?\n\nThank you very much for your answer."
    author: "desperate"
  - subject: "Re: Where to put enableMultihead?"
    date: 2001-02-11
    body: "look at .kde*/share/config/kdeglobals:\n\n[X11]\nenableMultihead=false"
    author: "KDE User"
  - subject: "Finally!"
    date: 2001-02-09
    body: "I assume this will fix the bug when running KDE apps remotely, the apps they spawn sometimes decide to appear on the local console instead of the remote host that should be getting them.  (For example, viewing an attachment in kmail)."
    author: "Gene Scott"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-10
    body: "who do i get anti-aliasing working , !?! , is this working with multihead too ???\n\ndo i have to recompile something or does it work out of the box"
    author: "x"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-12
    body: "I've got KDE 2.1 beta2 running with xinerama and 2 screens on a Matrox G450. Will this patch recognize the two screens of my desktop and maximize a window only on one screen? Can windows be aligned at both sides of each screen?\n\nThanks"
    author: "Frank Schr\u00f6der"
  - subject: "Re: Multihead Support in KDE 2.1"
    date: 2001-02-15
    body: "hmmm, multihead is great, i'm  running kde 2.1 in xinerama mode. i tried to patch the source rpms, for kdelibs it worked, but i couldn't get\nthe kdebase package to compile, did anyone make it ?\n\ndistrib.: redhat 6.2\n\nUli."
    author: "Uli Suppa"
  - subject: "Multihead lockup"
    date: 2002-09-25
    body: "I am currently running multihead on my linux box with 2 video cards; a radeon 32mb and an xpert98 16mb. It locks up KDE on initialization and works well until I run an app on Gnome. Any ideas what I might have configured wrong?"
    author: "Craig Buckley"
  - subject: "Dual Monitor Video card - Will it work with Dual H"
    date: 2004-02-17
    body: "Hi, I am fairly new to KDE, and I have a Geforce 4 ti 4400, and It has support for two monitors, and I was just wondering if there was a patch or a program so I can have dual monitor w/o having to buy a second video card? If anyone can help that would be greatly appreciated. Thx\n\nEric (taxidriver)"
    author: "taxidriver"
---
Bradley T Hughes has implemented <a href="http://lists.kde.org/?l=kde-core-devel&m=98139377617317&w=2">multihead support</a> for KDE 2.1, and is currently commiting the code. This is one fine feature masquerading as a bugfix but which should make a lot of <a href="http://lists.kde.org/?l=kde-devel&m=98154560621001&w=2">people</a> with multiple monitors quite happy.


<!--break-->
