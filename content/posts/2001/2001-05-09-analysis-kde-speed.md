---
title: "An Analysis of KDE Speed"
date:    2001-05-09
authors:
  - "Dre"
slug:    analysis-kde-speed
comments:
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-08
    body: "I'm so glad Waldo is working on KDE and not GNOME..."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Hmm... I surely am very glad to know Waldo and I'm honoured to call myself his friend. Yet, it should be a matter of pride for us to avoid the kind of remarks tou did. Waldo *did publish* his study, thus hoping *all* the Linux community will profit/help/contribute. If an inventive Gnome developer will take Waldo's notes, have a strike of genius and find a marvelous solution to our speed problem, I'm sure we *all* will profit. And it goes like this for every other thing in Free Code. And that's why we like it so much, even if we don't always realize this actual reason.\n\nI would like to thank to Waldo for his constant preocupation for KDE's performance aspects. He is one of the major contributors to KDE-2's at least equal to KDE-1 performance, despite of it (KDE-2) being at least 3 times more complex."
    author: "Inorog"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "\"Yet, it should be a matter of pride for us to avoid the kind of remarks tou did. Waldo *did publish* his study, thus hoping *all* the Linux community will profit/help/contribute.\"\n\nIt was a joke.. relax ;)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I see. I used to use KDE all the time. I don't at the moment, although it't not for any advocacy reasons. Comments like yours don't make me want to come running back (nor will similar comments from Gnome advocates make me proud)."
    author: "Ian A. Marsman"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I can't see how you could take this that way?\n\nFirst of, I AM glad that Waldo is working on KDE, as GNOME is not using C++ and had he been working on GNOME the article (if one had been made) would have had to do with C and not C++, and since KDE is a big part of my life, I am happy that a person with this much insight and intelligent is working on making it better.. This was not meant as a KDE-GNOME thing, I mentioned GNOME 'cause they use C and not C++...\n\nRelax.. Don't see wars where there aren't any.."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "What about Gnome-- and Gtk--?\nThey're C++ binding right?"
    author: "dc"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I didn't take this the wrong way at all - sounded to mee more like a \"I'm glad you're on the team\" comment..."
    author: "EKH"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-13
    body: "Actually, if you read the article, there are a number of things that would help Gnome as much as it would help KDE.  For one, the dirty pages that are created when libraries are relocated take up valuable memory, and increase the amount of I/O required to load something.  I/O tends to never be fast, so it's something everyone wants to avoid if possible.  A more optimized relocator in the ld.so would not only help KDE, but also Gnome as the memory footprint could very well shrink for both projects. \n\nIf you have 213 dirty pages per application, that means that every task (except one) is about 800k bigger than it could've been.  Not only that, that also means that you have to load another 800k of code from the disk, which adds to the start-up time if you were unlucky enough to have the content expire from the cache (and on small or low-memory systems, it's likely the case).  If you assume that the average consumer drive can do 5-10MB/sec, reading off the drive platters, that's already a tenth of a second wasted right there.  Having more free memory pages for cache never hurt anyone."
    author: "Chad Kitching"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Shouldn't that be \"I'm so glad Waldo is working for KDE not Winblow$\" ?\nPeople, Micro$oft is our enemy, not Gnome!"
    author: "dc"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "I like KDE, GNOME and Windows.  M$ is not the enemy, there is no enemy."
    author: "AC"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-11
    body: "I'm so glad you make KDE users look like complete dick heads, and not GNOME users."
    author: "ac"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-08
    body: "Many have said this on LinuxToday already:  This is one heck of a paper!"
    author: "KDE User"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Interesting esp. the part about classes. How is this done in windows ? does it have a better c++, less classes ?"
    author: "Pyretic"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Windoze is written in C.  C has no classes, so it doesn't have the same overhead."
    author: "Uhmmmm"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "No, C has the same problem, just less of it.  Just remember that kmail has about 60 000 relocations, and the (fun) GTK game freeciv has a mere 1172.\n\nBut Windows does fix it, so even (e.g.,) Qt/Professional with nearly the same amount of relocations will start fast on windows.  But this is only because Windows's dynamic linker isn't very versatile, while Unix's is very versatile (and therefore, slower)."
    author: "Charles Samuels"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "You just can decide between a fast program or a small-sized program. C++ is mostly small-sized, if it has a good style of code reuse. That means, you've to call more function, more time to spend in.\n-- But that doesn't mean, that you can't write fast code in c++.\n\nGreetings,\nChristian Parpart."
    author: "Christian Parpart"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "One question I have is:\nIsn't it possible to reduce the number of static functions in QT and KDE?\nFor me it looks like as static functions are \"bad\" functions."
    author: "Schwann"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "AFAIK, Windows DLLs have a \"preferred address\". If the dynamic linker finds that the preferred address is unallocated and there's enough room to take up the DLL at that address, it is loaded there, and no relocation is needed. Otherwise, it loads the DLL at a different address and relocates it.\n\nI think that this is the best thing to do: When a library is linked, it has to be decided in some way what the preferred address is. This must take into account the shared objects that the library depends on."
    author: "Johannes Sixt"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "DLLs do have a preferred address. However, as soon as a DLL has to be relocated, it can't be shared anymore, because windows has no concept of Posistion Independent Code (PIC): all function calls inside the DLL have to be changed when it is relocated.\n\nLinux (and all other Unices I know of) does support PIC: it is essential to the ELF binary format, AFAIK. This means relocation isn't expensive: the pages of the libary won't have to be touched, making them shareable.\n\nThe thing both the Windows and Unix dynlinkers have to do is to resolve the calls made by an application to a library (or lib -> lib): the app makes a call to a fixed address, inside the relocation table. The dynlinker generates a call on this fixed address to the dynamically linked function.\n\nThe problem with KDE is the sheer number of function calls exposes the inefficiency of the dynlinker.\n\nLoading a library on a preferred address may speed up this proces. It may break ELF though."
    author: "Erik Hensema"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "I didn't understand - all calls in PIC code, even inside a single DLL, are done indirectly using a PLT table ? Are there no relative call instructions in assembler !? But Windows simply modifies DLL code replacing all adresses ? - a text segment is writable ? Uff !"
    author: "Hi"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-12
    body: "If theres a PLT, why can't the dynlinker cache the plt and load the shared library the next time it's needed to the same address ?\nI could even think of saving this cache to disc."
    author: "remi"
  - subject: "Linking"
    date: 2001-05-09
    body: "Excellent paper.  I had always wondered about the performance of dynamic linking with C++, and library caching in general.\n\nWhile the \"kdeinit speed hack\" is called a hack, it actually sounds like the right way to do it.  What better way to keep the libraries loaded?  It's possible that certain libraries could even be preloaded, like the filemanager components (IMO, the only application that really requires instantaneous loading).\n\nOn a side note, the only real other speed problem in KDE would be Konqueror's re-rendering of content.  It takes a long time to load large pages, like for instance a huge message board.  However, the re-rendering is killer when you've finished reading a post and you're clicking \"back\".  Same goes for pages with many images (like Konq's thumbnail render).  Perhaps these final page renderings should be cached somehow.  Does anyone know how Netscape and IE go back and forth so fast between alread-visited views?\n\n-Justin"
    author: "Justin"
  - subject: "Re: Linking"
    date: 2001-05-09
    body: "I completely agree with what you said. The filemanager/browser is one of the applications that really should start as fast as possible. Also going back and forward in the history is at present somewhat slow. Dirk Mueller is doing some nice optimizations concerning khtml at the moment, so I have the feeling this will be solved/fixed somehow. For me the main problem is drawing the page on my screen. On my fast hardware (not being under load at that moment) I can very often actually see the page (slashdot for example) being 'sweeped' on my screen from top to bottom. I really don't have a clue what is causing this behaviour (Hardware ? Qt ? Khtml ?) :(\n\nWell, keep up the good work !\n\nJelmer"
    author: "Jelmer Feenstra"
  - subject: "Re: Linking and caches"
    date: 2001-05-09
    body: "I wasn't sure if you were serious here or not...\nSurely you are aware that konqueror has a cache just like other browsers? It has even been enhanced in 2.2 with auto-synching and offline-viewing mode.\nYou set it on the web-browsing proxy page. (Dont ask me! Its prob on the proxy page because a cache IS a proxy to all intents)\n\nAnd the slow rendering you are talking about has nothing to do with rendering. Konq is acknowledged as having one of the fastest renderers in the world beating even IE. The delay you are talking about is just your slow net connection downloading the hundreds of entries in a post-list, Once downloaded it renders that almost instantly because it is mainlt text.\n\nGod I wish people would read their manuals. There are so many badly set up linux systems out there and all blame kde rather than their own lack of intrest in setting up properly, Now that would give more speed increase than optimising the whole dynamic loader!"
    author: "Andrew Kar"
  - subject: "Re: Linking and caches"
    date: 2001-05-10
    body: "Whoa there.  You completely missed what I meant.\n\nWhile Konq may render a page faster than other browsers (and it certainly does), and it caches the content, it does not cache the \"render\".\n\nI was recently visiting a large SuSE forum that took almost a minute to load.  This is tolerable, but it immediately became a problem when I read a comment and then clicked \"back\".  I had to wait another minute as Konq re-rendered the forum.  My solution?  Create a split view and drag links to comments into the other view.  A better solution?  Konqueror should have a method for rapidly rendering previously viewed content.  Perhaps storing the final rendered canvas into memory for the current session.\n\nAnd don't worry, I read my manuals.  I'm a programmer, after all.  I might even want to contribute to solve this problem, but I'm hesitant since Dirk didn't accept one of my other patches.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Linking and caches"
    date: 2001-05-10
    body: "Depending on the color depth this could take a bit of memory.\n\nHow about a fast compression of the previous render stored in memory or disk?"
    author: "John"
  - subject: "Re: Linking and caches"
    date: 2001-05-10
    body: "Why the fsck can Windows do this instantaneously?\n\nIE loads in < 1 second. Rerenders pages instantaneously, and is viewable offline.\n\nIt just pisses me off the Konq can't do it yet. I love working in KDE, but once you have used KDE for a few days and you fire up Windows again (because my online banking does not work under Konq.) you then realise just how slow KDE is, in general, I am able to load IE, go to google and perform a search and be redirected to the first on the list before Konq. has even started.\n\nI wait patiently, or not so :) for KDE to become the only OS I use :)"
    author: "Wanting-not-to-be-pissed"
  - subject: "Re: Linking and caches"
    date: 2001-05-10
    body: "The reason Windows GUI's are, in general, faster is that most graphics cards implement GDI calls in the hardware.  Try disabling hardware acceleration in your graphics settings and you'll see exactly what I mean."
    author: "Vladimir Annenkov"
  - subject: "Re: Linking and caches"
    date: 2001-05-11
    body: "X-Free is using the hardware-acceleration al well (if possible). So this should not be the reason"
    author: "Schwann"
  - subject: "Re: Linking and caches"
    date: 2001-05-20
    body: "test, ignore"
    author: "test1"
  - subject: "Re: Linking and caches"
    date: 2001-05-20
    body: "test, ignore"
    author: "test1"
  - subject: "Re: Linking and caches"
    date: 2001-05-20
    body: "test, ignore"
    author: "test1"
  - subject: "Re: Linking and caches"
    date: 2001-05-20
    body: "test, ignre"
    author: "test1"
  - subject: "Re: Linking and caches"
    date: 2001-05-20
    body: "test, please ignore..."
    author: "test1"
  - subject: "Re: Linking and caches"
    date: 2001-05-20
    body: "test"
    author: "test1"
  - subject: "Re: Linking and caches"
    date: 2001-05-20
    body: "test"
    author: "test1"
  - subject: "Re: Linking and caches"
    date: 2001-05-20
    body: "test"
    author: "test1"
  - subject: "Re: Linking and caches"
    date: 2001-05-20
    body: "test\n\n(for the curious: I'm trying to see how \na weblog copes with indentation related \nto threading messages)"
    author: "test1"
  - subject: "Re: Linking and caches"
    date: 2001-05-20
    body: "test"
    author: "test1"
  - subject: "Like Plucker"
    date: 2001-05-10
    body: "PalmOS devices have been doing something like this for a while due to their limited memory and CPU horsepower. I believe iSilo (judging by its speed) does this. The open source Plucker for PalmOS documents a file format that is essentially a prerendered web page, and that saves both CPU and memory. Prerendering does not have to mean saving the entire bitmap. A compressed format that saves prerendered bitmaps, preparsed text and previously made formatting decision would save both space and time. It can be used in place of the raw html and jpg files."
    author: "Chris W"
  - subject: "It can be done"
    date: 2001-05-10
    body: "It can be done. It's not a Windows-specific feature either, as some have suggested to your post. Have you tried Opera? It's back/forward cache performance is lightning fast, way ahead of anything else on Linux. If you play with its settings, you will find a \"cache rendered images\" setting as well. So yes, something much like what you suggested can and has been done on Linux."
    author: "Chris W"
  - subject: "200+ ENOENT"
    date: 2001-05-19
    body: "A simple strace on konsole shows more than 200\nfailed opens (ENOENT). I.E. It fails to find the dynamic dependencies 200+ times. This transforms into so many open/access/stat calls which obviously slows down the startup. I just tried to adjust the links to SOs so that all required dependencies are resolved at first shot. Believe me, it speeds up things to a good extent. Can't anything be done to hardcode the paths in binaries at compile time? Also ld.so.preload of libc and libqt seems to be a good idea for \"KDE\nonly\" users.\n\n-- Parag."
    author: "parag"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I've been unable to turn up any links on the kdeinit speed hack (via Google) -- does anyone have information on this?  It sounds like something I'd like to check out..."
    author: "kendroid"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "kdelibs/kinit/*\n\nits described in this paper -- did you read it?\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "I read it and I can follow the link as well as you can. But there is nothing called \"kdeinit\" in this directory. So, the question stands: Where do you download kdeinit?"
    author: "Jim Philips"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "You already have kdeinit!\n\nif u use kde, u already use kdeinit....\n\nI was just noting u can read what kdeinit does by reading the .cpp files  in kdelibs/kinit/\n\nKood luck\n\nJason"
    author: "Jason"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Basically, \"kdeinit\" is a daemon, initialized with LD_BIND_NOW, which causes it to be loaded entirely, all at once (rather than as the parts are required), then we dlopen the program, fork() kdeinit, and enter the program.\n\nWith LD_BIND_NOW, the relocations of all the kde libraries (that's a lot of relocations!) are done only once.  And they won't be done again on that fork()"
    author: "Charles Samuels"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "You can find the kdeinit README at\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kinit/README?rev=1.7&content-type=text/x-cvsweb-markup"
    author: "Konrad Wojas"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-12
    body: "The kdeinit speed \"hack\" is mostly internal to KDE. However, you can use it with the 'kdeinit_wrapper' binary.\n\nRun 'kdeinit_wrapper <KDE program>', for instance: 'kdeinit_wrapper konqueror --profile webbrowsing'. Yeah, this is on the command-line."
    author: "David B. Harris"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "A question springs to mind ...\nWhy would the per process memory footprint drop by 800k?\nDoes this mean that under normal conditions, some libraries get loaded twice?"
    author: "Nobody in particular"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Pedantic: \"The memory footprint\" doesn't drop by 800Kb, the memory footprint remains the same, it's just that 800Kb more of the footprint is shared with other processes.\n\nThe reason for this is partly that the linker modifies data in pages of memory (the so called relocation) and by doing so such a memory page will no longer be shared. That is called \"copy-on-write\", normally the memory page is shared between different processes, but once you make a modification the kernel makes a copy of the page and lets you modify this copy so that your change only affects your process and not any other\nprocess.\n\nIt should be noted that about 400Kb of this 800Kb seems to be actually caused by the linker. The other 400Kb seems to go up on other things.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "hello,\n\nWould this also be true for non kde applications, like applications using just glibc (eg glibc also has to be relocated, and this also involves writing, and \ndoes this mean all used parts of glibc are no longer shared ? and does this involve a lot of memory ? \n(thus could linux memory usage in general benefit from such a kdeinit like hack)"
    author: "ik"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "To answer your questions in reverse order:\n\n> does this involve a lot of memory ?\nNo it does not involve a lot of memory.\n\n> does this mean all used parts of glibc are no\n> longer shared ?\nNo, only a small part of glibc (the .got and .data\nsections to be precies) is not shared. These\nsections are typically quite small in C libraries.\n2980 and 12804 bytes resp. for glibc.\n\n> Would this also be true for non kde \n> applications?\n\nYes, but too a much smaller degree. \n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "A very interesting paper. I suspect that using the -Bsymbolic flag to link the KDE libraries can make a significant difference. Most of the symbols that KDE applications use should be resolved within the KDE libraries (or the dependent libraries). \n\nGanesan"
    author: "Ganesan R"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "My impression about the poll: The first days (or were it hours?) \"KOffice\" and \"Konqueror\" were far ahead. Since then \"Speed\" has constantly catched up not only closing the gap but leading with over 170 votes!\n\nJust wondering if it is possible to vote hundred times via script from the same IP without delay?\n\nOf course speed is important, but I don't think that is SO slow when given enough memory that \"Speed\" gets most of the votes of all points."
    author: "kdefan"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "It means that people on the bleeding KDE edge are used to its speed and focus on extensions. Those that visit the site once in a while don't vote in the first days: I don't use KDE because it's too slow, but I voted nevertheless.\nIt remind me when I switched to Linux from another quick and dirty operating system, it took me weeks to adjust to the unresponsiveness of the system. Now I am used to it and don't expect windows to open instantaneously.\nPlease don't tell me to upgrade hardware, or send a me check with your comments.\nMinh."
    author: "Minh"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: ">It means that people on the bleeding KDE edge are used to its speed and focus on extensions. Those that visit the site once in a while don't vote in the first days: I don't use KDE because it's too slow, but I voted nevertheless.\n\nI would like to see a new poll which asks for the KDE version, MHz of CPU, RAM size and a rating for \"Speed\" between 1 and 5.\n\nUsers who don't use KDE but vote nevertheless will perhaps only know 2.0's speed. Users with low memory will perhaps vote for \"Speed\" not recognizing it's actually there memory shortage and they should vote for \"Memory footprint\".\n\nA _possible_ summary could be: More peoply are pleased with 2.1's than 2.0's speed. 80% are pleased with 2.1 which have at least 400Mhz and 128MB. 80% with less then 64MB are disappointed.\n\nThat would be interesting and perhaps show development progress."
    author: "kdefan"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "Kde 2.x needs about 48mb of ram to get decent performance, you can make it run ok on 32 if you diable all the eye candy, and klipper, but any machime with less than that is going to run into performance probs with any newer applications not just kde."
    author: "richie123"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-11
    body: "I switch back and forth between Mandrake Linux and Windows 98.  I recently upgraded my system from a 450MHz K6-2 with 128Meg RAM to a 900MHz Athlon with 256Meg RAM.  \n\nI find that KDE-2 runs slower (as measured by how long it takes to open a window to display my home directory, or to bring up a text editor) on the new system than Win98 runs on the old system with half the speed and half the RAM."
    author: "Steve Gorwood"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "On later days other sites picked up the poll and so people were voting who are not regular dot readers.\n\nAs for multiple voting, you can't do it (from the same IP address, anyway)."
    author: "Dre"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "> Since then \"Speed\" has constantly catched up\n> not only closing the gap but leading with over\n> 170 votes!\n\nPeople wanting more speed have slow computers so it took them more time to vote. ;-)"
    author: "Rob Kaper"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-12
    body: "Well... perceived speed of KDE is a little bit slower as compared to KDE 1.x, though its still a lot faster than Gnome and Windows. And I really think that its \"perception\" which matters. For example, when I open a link in Konquer in a new window, it takes a lot of time. But if I open it in the same window, it appears immediately (though it still takes noticeable time). I can imagine that it takes a lot of time opening a new window, unfortunately, that's what I've to do while I'm doing heavy surfing.  Other thing I've noticed is that I can see the page rendering from the top to the bottom from which I perceive that its slow. On the other hand, IE may not render the page faster than Konquerer, but its not noticeable."
    author: "Ghulam Sarwar"
  - subject: "Any news on the GNU malloc problem?"
    date: 2001-05-09
    body: "Some time ago someone posted a possible problem with GNU malloc wich causes memory fragmentation and, as a result of that, unneeded memory consumption. Using a alternative malloc gave him significant better result on memory usage. I can't find any reference on the GNU site. Is there somebody out there with a status update on this?"
    author: "pasha"
  - subject: "Re: Any news on the GNU malloc problem?"
    date: 2001-05-09
    body: "I think it was in KDE Kernel Cousins."
    author: "Nicolas DUPEUX"
  - subject: "Re: Any news on the GNU malloc problem?"
    date: 2001-05-09
    body: "Yes, it was in KC KDE:\n\nhttp://kt.zork.net/kde/kde20010331_4.html#7\n\nThe last sentence of the comment reads: \"It would indeed be interesting to see what the overall impact on the entirety of KDE would be if this new malloc() was used everywhere\".  I agree... anybody wanna recompile and see?\n\nBTW - anybody know why KC KDE hasn't been updated this past week?  It's already a regularly hit bookmark for me.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "KC KDE This Week"
    date: 2001-05-09
    body: "Hi...\n\nBrief answer: life imposed itself upon my schedule leaving pretty much no time to do anything on my \"want to\" list. This is actually my first chance to read the Dot since late last week! \n\nKC KDE will be back this coming week however as things are once again in a (more or less) normal pattern for me.\n\nApologies to all those who were looking forward to this week's KC KDE."
    author: "Aaron J. Seigo"
  - subject: "Re: Any news on the GNU malloc problem?"
    date: 2001-05-09
    body: "Does anybody around here know how PHK malloc (FreeBSD's default malloc function) compares to GNU malloc in this way? Does it have the same behaviour?"
    author: "Theo van Klaveren"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I find it a bit weird that when KDE starts up the CPU is between 15-30% idle (on SuSE 7.0) and a bit less on Mandrake 8. Any ideas?"
    author: "Laurence"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "CPU is not always the bottleneck, it is very well possible that a process is waiting for some bytes to be read from disk and thus cannot do anything but idle."
    author: "Rob Kaper"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "How did you measure this? With top?\n\nThe numbers (especially about CPU usage) the top (and similar tools using libproc) reports are not accurate AFAIK. Especially with multithreaded applications."
    author: "Martin Macok"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I think the kde window manager is also quite a bottleneck. It takes _much_ less time to start a kde application under Xfce or IceWM than under Kwm...\nWhich I find kind of strange...\n\nAC"
    author: "AC"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "even more strange is that kwm doesn't exist...\n\nanyway, i tried this here with kwin and icewm and can't duplicate it. the apps take the same time to start."
    author: "Evandro"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "Sigh, here we go again. KWin (KDE's window manager) relies on a couple of KDE libraries, including libkdecore and libkdeui.\n\nThose libraries have a certain size, but are present in memory whenever you run a single KDE application.\n\nNow, if you have a system with 8 MB of memory, and you do not run any KDE or Qt-based application  at all, you may see a performance decrease on launching applications compared to things like blackbox or icewm due to the increased memory usage (again: not due to the speed of kwin).\n\nIf you have 32 MB of memory and you run an entire KDE desktop (that is: kdesktop, konqueror, kicker, some tools like klipper or kscd plus all the service daemons) you will notive a speed difference when starting new applications compared to a simple window manager. \n\nBut again, this difference is _NOT_ due to kwin but due to the lack of free memory.\n\nIf you are concered that kwin isn't able to create enough window frames per second around new windows, run the following test program:\n\nwhile true; do sh -c \"xclock &\"; sleep 1; killall xclock; done\n\nThe time you see the clock flashing is the time required to destruct a window frame, start a shell, start xclock, make xclock communicate with the X-Server and having KWin create a decoration frame, doing the smart placement and inform the titlebar. You will hardly notice any CPU usage when running the program.\n\nIf that isn't enough of a proof, run kwin standalone, without the rest of KDE. \n\nWhen will people understand that all a window manager really is is a tiny little program that draws some stupid decoration frames around their application windows? I received bugreports that some programs had missbehaving scrollbars or weird menus and that most certainly must be because KWin does something wrong. \n\nThose things really bother me. A window manager is a frame around a window. Period. It has no influence at all on application behaviour or general look and feel (even if the S.u.S.E. manual tells a different story).\n\nWhen I read on lwn that fvwm is faster than KDE, I really do not understand what they compare.  Starting up a Konsole in KDE with fireing up rxvt from the fvwm menus? Starting lynx vs. starting Konqueror? Starting KMenuEdit vs. vi ~/.fvwmrc? I cannot come up with a single thing that actually has something to do with the window manager (i.e. fvwm). Anybody out there with a clue what they possibly could mean?"
    author: "Matthias Ettrich"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "At home I have an aging P200MMX with 64Mb of RAM. At the time, a nice machine with lots of RAM.\n\nUnder KDE2, the computer runs slow and sluggish. Under WindowMaker, it's fast and snappy.\n\nMy wife and I have come to the conclusion that Win9X runs just much, much better at this type of machines, although we both hate to admit that.\n\n\nReasons? Well, what I think is:\n\n1) The KDE2 apps seems to take a long time to start up, this makes everything feel sluggish right away.\n(I get the impression that the number of installed fonts makes a huge difference. When starting up the KDE2 desktop, the fontserver eats as much as 40%-50% of CPU for much of the time and if no fontserver runs the X server eats up that time. Starting up Gnome doesn't have the same effect. AA setting doesn't affect this.)\n\n2) Under WMaker I often run a different set of apps, which start faster. Not entirely true, because I do use Konqueror or Konsole from time to time when using WMaker, BlackBox or FLWM.\n\n3) WMaker is a simple windowmanager using little memory. KDE2 is a fullblown environment using lots of memory, running lots of little apps in fore- and background\n\nConclusions:\n\n1) We do false comparisons because we compare simple no-frills applications with applications that contains plenty nice features and therefore extra code\n\n2) The extra memory used by the KDE2 environment means more swapping, which vastly slows down everything - Netscape under KDE2 can be slower than under Wmaker, because more has to be swapped\n\nSo the overall computing experience while running KDE2 is that of a slow system, while the lightweight environments appear fast and snappy.\n\nMy subjective feeling is that the KDE2 environment *is* fast and snappy like FVWM, WMaker, what not -- once it's loaded. And that KDE2 applications *are* mostly fast and snappy like their non-KDE counterparts -- once they have started up.\n\nAnd as long as not too much swapping is required.\n\n\nSo in my opinion it is most important for KDE2 to:\n- Reduce memory usage\n- Improve app. startup speed\n- Improve overall speed, but not at the cost of vastly increased memory usage!\n- Fix bugs, become more stable\n\n- Keep improving Konqueror and KHtml - it's the greatest browser for *Nix, finally allows me to get rid of Netscape and beats the hell out of Mozilla until now\n- Keep improving everything else\n\n\nAnyone who wants to pay me for helping out with those things? ;-) (right now I don't even find time to play with my own pet projects, much less to start messing with KDE2 source code much as I would love to do it)\n\n\nWith regards and happy hacking,\n\n--Tim :-)"
    author: "Tim N. van der Leeuw"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "Yeah, right, sure, okay, fine. But that's not the KWin window manager. That's KDE as a whole. The window manager is really small and memory efficient. It is actually much smaller than windowmaker, believe it or not.\n\nI was once writing my own window manager just to learn how to do it. Looking over the sources to KWin, I was amazed at it's elegance and simplicity. The only \"bloat\" to it was the kdelibs, but then, if you're running KDE already, this is nothing.\n\nKWin by itself is extremely snappy and quick, rivaled only by Blackbox and other minimalistic window managers. Even icewm seemed sluggish alongside a bare KWin."
    author: "David Johnson"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-11
    body: "Indeed, that's KDE2 as a whole. Not the WM. That's why I went on to explain why, in my opinion, KDE2 as a whole feels sluggish :-)\n\nActually, Gnome doesn't feel sluggish as a whole (discounting Nautilus for the moment!). And while I prefer Gnome over KDE1.1, there is no way Gnome can compare with KDE2 - so either I put up with the sluggishnes or I revert to a bare-bones-just-wm setup. :-)\n\nBut when you run 'just a WM' the simple WM becomes in a way your desktop - it often has things like taskbar or program icons, launchers, little applets, pagers, etc. A WindowManager doesn't offer a whole desktop-suite with editor, calculator and filemanager but I don't think that those are what really makes up the 'desktop experience'. Filemanager does to some degree but old unixheads have gotten along without a filemanager for such a long time that perhaps it doesn't fully count as part of the desktop.\n\nSo when comparing KDE2 vs. WMaker, FVWM, FLWM, Blackbox etc you are comparing apples to oranges. But in a way, you are still comparing desktop experiences. Which makes the comparison unfair, but not totally invalid.\n\n\nWith regards & Happy Hacking,\n\n--Tim :-)"
    author: "Tim N. van der Leeuw"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-23
    body: "I agree.\nI've had mixed experiences running GNOME and KDE, and eventually went with just a bare WindowMaker. It gives me elaborate window management, multiple desktops, plugin dock applets, most of the stuff the start bars of GNOME and KDE provide in other words. And I can still run the loose apps from GNOME and KDE if I need them, without even getting much of a speed hit in comparison to a running KDE or GNOME.\nWindowMaker is up and running within 4 seconds of typing the enter after startx (on an old pentium II/233). And the only desktop feature it doesn't give me that I miss is abundant drag-and-drop. There is _some_ support, but it's not enough. Copy/paste though is complete, once you figure out to do it via the select/middle-mouse-click way, instead of via the keyboard shortcuts. And on top of all that , it even looks great. A lot better than I ever made kwin look anyway. WindowMaker is based on a principle I like called \"less is more\", meaning, only minimize and close in button form, and for all the rest there are the shortcuts and menu's.\n\nHonestly, I don't get what desktop environments are about. Most of the stuff they do seems to be hugging resources I'd rather use myself (like arts taking over /dev/dsp, so only KDE-approved apps can use it, unless you know the workarounds)."
    author: "Joeri Sebrechts"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I'd be willing to bet that you have very little memory, all of KDE is huge compared to IceWM, you probably are using virtual memory."
    author: "robert"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "From the article:\n[When I now define a class \"derive\" that inherits \"base\" and that overrides a single virtual function, I will get a second vtable. I now need 20 relocations.]\n\nI don't understand why twice the number of relocations is necessary. Can't the linker copy the vtable from the base class and then overwrite only the addresses of functions overridden by the derived class?\n\n(I'm not that familiar with linking; it's quite possible the answer is \"no\", but please explain why.)"
    author: "Maarten ter Huurne"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Because the linker doesn't know anything about C++."
    author: "Someone"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Sorry, that was too brief.  It couldn't be done by the linker alone.  The compiler would have to be modified to create partial vtables, containing only functions pointers which are overridden (the rest left blank), and generate special code to copy the inherited parts of the vtables. And then the loader would have to be modified to execute this code before the library or executable gets accessed (it could be a list of instructions to the loader instead of actual code, but that would be slower).\n\nYou'd have to somehow make sure the base class vtable always gets initialized before the derived class, which would be very tricky. Each vtable might need its own fragment of initialization code, or they could be grouped together based on which shared library contains their base class. The loader would have to be informed about the inheritance relationship of the vtables and would have to figure out what order to execute them in.\n\nThe extra overhead might cancel out any speed-up and the added complexity probably wouldn't be worth it."
    author: "Someone"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-16
    body: "I may be wrong, but I was under impression that all c++ features are taken care by compiler and when the compiled binary is produced, it's like an equivalent C binary, with only one difference. The class member function have an additional, unspecified (first/last) argument, *this, which points to proper data to be handled.\n\nWhere does relocation comes into picture? For derived objects, the pointers are totally different. \nIn a compiled object, there should be no relation between base class vtable and derived class vtable.\n\nWhy have dependency hierarchy in assmbled code? What compiler is supposed to do then?\n\nPlease enlighten me.\n \n Shridhar"
    author: "Shridhar Daithankar"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "That makes sense: the linker doesn't know about the base class / derived class relationship.\n\nHowever, if the linker can see that it's resolving the same symbols twice, the results could be cached. When resolving the base class symbols there is actual work being done, when resolving non-overridden derived class symbols there is only a lookup in the cache.\n\nSo the questions are: Is a non-overridden function indeed the same symbol as the original function? If so, can that be detected by the linker?"
    author: "Maarten ter Huurne"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "> So the questions are: Is a non-overridden\n> function indeed the same symbol as the original\n> function? If so, can that be detected by the\n> linker?\n\nYes and yes. John Polstra of BSD fame pointed \nthis out as well and managed to get quite an\nimprovement by caching symbol lookups for this\ncase.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "KDE should add Features instead of trying to improve performance. You can always buy a new faster computer. But you can't buy functionality... it has to be developered. Right now, KDE does not have enough features for end user. End user needs more configurability and options, which KDE developer should aim to deliver."
    author: "ac"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Ha, ha. Good joke!"
    author: "Marc Mutz"
  - subject: "Funny little guy"
    date: 2001-05-09
    body: "No comment !"
    author: "thil"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Yeah right, as if everybody has so much money.\nI have to wait at least another two years before I can afford a new motherboard and CPU!"
    author: "dc"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Could you please be more precise about which features YOU are missing in KDE?\nPersonnally I\u00b4m happy with it. I\u00b4m running KDE since almost the beginning and the last feature I WAS missing WAS an office suite ;-).\n\nSo what exactly are you missing within KDE?"
    author: "Sangohn Christian"
  - subject: "Donations are welcome!"
    date: 2001-05-10
    body: "Well, I'm having an 8 year old Tower case, 5 year old motherboard and graphics gard, very new AMD K6 processor and a 1 year old 10 gig hard disk. Obviously this is not anymore fast and big enough for operating systems these days, at least, when I read your comment. Please feel free to send donations in any currency and any amount to: J. Lermer, Fuerstenfeldbruck, Germany. Thank you."
    author: "Johann Lermer"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-11
    body: "Hi my name is Clippy.  You may have seen me in such applications as Microsoft Office 97 or 2000.  I'm currently job hunting and will work for RAM.  I believe that I am just the right extra functionality that you need.  Interested parties can contact me at clippy@microsoft.com"
    author: "Clippy"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I partly agree with that. Speed is a very important issue and should not be underestimated. However, you cannot expect complex applications that uses KParts and other technological advantages that KDE offers to run fast on 486. This is simply not possible. Flexibility and user friendliness are time consuming issues, unfortunately. I use 233Mhz Pentium II with 64MB of RAM and KDE 2.1 is slow on that kind of machine. But this is the price for all other benefits you get from using KDE. I really doubt that Windoze ME or 2K would run any faster (if they would run at all with that little RAM)."
    author: "Bojan"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Just some other issues to think about when dealing with the shared library/dll comparison.  I have spent most of my professional life working inside windows and some of the speed enhancements in NT are worth thinking about.\nMy understanding of the loader etc (from observation mostly goes like this):\nThe application does not call the DLL directly, they generate stubs that match the DLL entry table so that if you call a DLL only the relatively small relocation table in the executable needs relocation.  The code inside the DLL also has a jump table that uses relative code.  There is generally not a lot of relocation needed.\nThe next big thing is that if a DLL is relocated it is allocated in swap space and shared by all processes loading it.  This means that in most cases the reloc is only ever done once.\nIf a Dll is already in an unused memory area then nothing happens.  The .exe will use the link-time offsets and the dll is loaded into it's memory location using a memory mapped file.  Since this is how the O/S also tries to load apps the result is that if an app has pre-defined and non-conflicting addresses then the relocation time in 0 and the memory allocation is only the data needed.\n\nI can suggest one way to speed up the time to load a .so, and that is to generate a proxy object that the application calls, the proxy then is the only part of the application that needs to have entry point addresses relocated.  Should help?"
    author: "Peter Nunn"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-14
    body: "Or perhaps it would just be easier to 'fix' the dynamic loader/linker.  Generally when you install a new shared object (.so), you need to run ldconfig(8) utility to update the ld.so.cache file.  If I'm not mistaken, this process should also be extendable to automatically generate non-conflicting base addresses for all these libraries, much like how windows executables and dlls specify a preferred address to achieve the same result.  This wouldn't help libraries that aren't in the standard library directories, but since few of these are shared between multiple applications, I doubt there'd be much sharing between them.\n\nThe basic performance problem with KDE is basically the amount of RAM it consumes, more than anything else.  On my laptop, without X or KDM loaded, I have roughly 22MB free (0M swap used).   Starting kdm drops that figure to 6MB free (0M swap used), and after logging in, I end up down to 1MB free (2MB swap used).  The moment swap starts getting used, performance is ready long dead.  If I had 96MB or 128MB instead of 64MB, it'd probably seem much faster.  As it stands, KDE does not perform well in such 'low memory' situations.  RAM may be cheap right now, but that is no help to someone who may be trying to squeeze some extra life out of an old pentium machine (most of which can only cache up to 64MB of RAM anyway).\n\nThen again, I could be completely wrong...  I'm just rambling uneducated guesses about all of this."
    author: "Chad Kitching"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Why is not possible to run fast on a 486?  Wouldn't it be very nice if it did, especially if you live in an underdeveloped country where computers are very expensive?  Comparing KDE to Windows isn't the right view.  Instead, compare KDE/Linux to Be.  In that view, KDE/Linux has a long way to go and Waldo has done a great service to trying to go further down that road."
    author: "PG"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Speed enchancements are good, but there is a certain limit. A 486 is an extremely old piece of hardware, and expecting KDE to run quickly on it is simply asking too much."
    author: "Carbon"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "Actually I disagree. I almost exclusively use linux with kde2 on Mandrake8 at home on a reasonably fast PC and find that the few minutes I'm using windows everything is faster (simple loading and closing of programs, file manager etc..). Sad but true. Even on a fast PC, it is relatively slow. I've never had click->snap! kind of performance from kde1 or 2. I still love it though."
    author: "Con Kolivas"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "They run with 64MB.... and even with O2k installed."
    author: "BillyBoy"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-14
    body: "I disagree.  Firstly when it is obvious from an analysis like we see here that there are things that can be improved they should be - because there are sure to be other performance issues that will bite you anyway.  Secondly - its almost a frame of mind which says that hardware will improve so that lets ignore improving the software - that has us struggling with usability timings.  Add layer upon layer of that viewpoint in a complex application of today and that is precisely why the problems occur."
    author: "Alan Chandler"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I think functionality is important, obviously, but speed is also very important. \n\tI was using KDE 2.1, and some days ago, i return to start with Windows 98. Why?. Because is possible that Konqueror has the same functionality that Explorer or IExplorer, but when you open Konqueror first time, or close it and open again, it is very sloooow. You do the same operation on Explorer, and start is some slow, but open it again, is <1 seg. (I have Pentium II 233 Mhz, 128 MB ram). If you use Explorer to move or copy things from folders, operations are most quickly -flexible- on Windows that in KDE. Same things in Word or Excel: run one of these apliccations is very quickly, and reopen it again after close, is about 1 seg. You can compare similar programs on Linux, and imagine.. There are something that not work as it must work. Perhaps is ELF format executables, or is library linking, or must have libraries resident in memory, but there is something that not work as it must work. Perhaps it not was a bad idea to \"look\" how Windows do the work.\n\tI think that buy another machine is not a solution, because windows will be much faster, again. And Linux \"robust\" sistem is not a excuse; Windows NT is also robust, and it is fastest that Win98 to run programs.\n\tI don't want a war between Linux or Windows. I just remember the facts. There something that not works in Linux and in desktop environments."
    author: "Toni"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Look at it this way: \nwhen did the development of Windoze start? And when did KDE development start? Do you know, how many payed programmers Microshit has for Windoze development? And how many volunteers work for free on KDE? Even if you neglect this questions, you should answer this: let's say I give you few hundred bucks. You have a slow computer with no OS on it at the moment. You may buy a Windoze license or you may buy new hardware (only processor and RAM) that will run KDE faster. Which one will it be and which one will run faster now? OK, let's get a bit more serious. KDE offers great platform for application development, because it is written in pure C++ and it offers great technological advantages (like KParts, DCOP, etc.). It was written in C++ for that reason. Would the only reason be speed, it would probably be written C like GNOME. Speed is important, but KDE guys did what they could to speed it up as it is obvious from Waldo's article. If you care so much for speed, then maybe you should check back about KDE in few months."
    author: "Bojan"
  - subject: "C and C++.. a matter of speed?"
    date: 2001-05-09
    body: "Binaries from C-Code or from C++ are likely to be different in speed (maybe in size, plain C intends to result in bigger executables than C++). It's really the startup-time, that matters!\n\nWhat we should talk about instead:\nHow can we improve the dynamic linker?\n\np.s: It's just a feeling, but I think windoze has it's advantages in speed by putting up with technological disadvantages."
    author: "AC"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "nevertheless the shit is faster than \"the solid rock\"..."
    author: "BillyBoy"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-16
    body: "My 2 cents.\n\nWindows GUI is fast but it's speed varies depending upon the load. That happens *very* rarely with KDE.\n\nI use 256MB machine with win2K. When netscape is going thr. thousands of messages and I try starting IE or something like that, it takes more time than to start IE+netscape separately.\n\nWindows GUI has advantage of speed. But KDE is certainly more productive.\n\nBesides on Mandrake 8, KDE is at least as fast as w2k.\n\n Shridhar"
    author: "Shridhar Daithankar"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "Agreed, and I have a relatively fast PC and find the same things you do."
    author: "Con Kolivas"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-12
    body: "Yes, go ahead and go back to Windows.  I hope you enjoy digital content protection.\n\n(And yes, I posted this using Windows.  I don't feel like building a cross-compiler right now.)"
    author: "Chad Frick"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Speed is the number one reason I don't like KDE.  Gnome is and will remain my desktop on my KRUD (www.tummy.com/krud) boxes.  The only system I use KDE on is my laptop, and only because Ximian hasn't released a Mandrake 8.0 version yet.  I can't have more than three windows (two mozilla windows, an evolution window, and a terminal window stop the box completely) and this is a pIII-600 with 192M of RAM.  Win2k runs faster."
    author: "rev_matt"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "\"two mozilla windows, an evolution window, and a terminal window\"\n\nlol... I'm sure it's not the terminal, that causes your system to slow down ;)"
    author: "AC"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "this is rediculous.. what are you talking about? I have a Dell 5000e and at 400x1050, the memory used max goes till 70MB or so.. That means it should be very much possible to work even on a 64MB box. And yes, this is after i open tons of KDE applications."
    author: "sarang"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Then I have to ask: What company are you working for ? What's your relationship with M$ ?"
    author: "DC"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "Thats bollocks.\nI'm running a PIII700 196 meg memory, with 2 Xservers (one running on :0 and the second on :1). On the first theres Evolution, Galeon, many terminal windows, emacs, and numerous other windows. On the second Xserver theres wine running Starcraft.\n\nNow, if 2 mozilla windows, Evolution and a terminal (thats 4 windows BTW), \"stop the box completely\" I suggest it's probably not the software (either KDE or GNOME) at fault."
    author: "me"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "You perception of REALITY is far from the truth. In an attempt to make KDE crash, I loaded one Konq. with the root directory as the view. I then clicked on the Konq. spinning icon to respawn new Konq.s. After clicking as fast as I could until I got bored with it, I fired up a breakout game and played it as I watched all these windows being drawn to the screen. I eventually had 73 Konqs open!!!! I then closed all these windows as fast as I could, and KDE didn't crash.\n\nThis is on a P11 400 with 128mb RAM\n\nI love KDE, I would gladly buy a second 128mb DIMM and have the entire KDE run from ram if that was possible. Then the speed issue would be nul and void, for me at least."
    author: "Idiot-buster"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "that's not 73 konqs.  That's 73 konq windows, with 1 konq process."
    author: "Tick"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-11
    body: "Of course if you knew anything about how a computer works, you'd know that 1 konqueror and 1 mozilla has a better chance of breaking something than 73 konqs(this is not a joke about mozilla, assume mozilla takes the same resources as konq for this, and don't reply anything about mozilla)."
    author: "AC"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I voted for speed.\nBut the \"speed\" I was referring to is not only startup time, but also other things.\nShow a menu for example.\nI have a Pentium 166 MMX with 48 MB RAM.\nClicking on the menu button takes 1 - 2 seconds before the menu actually shows up.\nSame goes for the menus in KDevelop.\nThey are slower than other KDE/QT program menus for some reason."
    author: "dc"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Hi,\n\nTry to adjust the cache settings in kde so it don't cache so much in memory. Also try to stop so many services (/etc/rc.d/init.d) so you can free up as much memory as possible. Big fat background images is not for you either :("
    author: "Sam"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I already did that. Gnome is much faster though."
    author: "dc"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I agree 100%.  Start up times are annoying, but it is actual runtime speed that makes users really annoyed.  The simple act of using the UI feels sluggish.  This is without themes, backgrounds (I tend to use a single color or sometimes a gradient).  My machine also runs next to nothing other than X.  sshd is the only daemon likely to be active at any one point in time.\n\nI see people say that come on you can always buy a faster box.  Must be nice to have money to throw away.  Machines are still not free, nor can I just magically make my laptop 3 times faster.  But I should not need to either."
    author: "Shaleh"
  - subject: "Reaction time (was: An Analysis of KDE Speed)"
    date: 2001-05-09
    body: "Hi!\n\nAdditionally to freeing up memory as suggested in the other reply to your post you could try to play with the X servers setting. With 48 MB RAM you could try to disable double buffering for example and see if that *helps*! (Should give a quite a few bytes free RAM if you'r video RAM is relatively small.)\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: Reaction time (was: An Analysis of KDE Speed)"
    date: 2001-05-10
    body: "From personal experience, RAM doesn't make a difference. I have 128mb of RAM and the experience I have is still slow. In all honesty, running Win95 on a 486 with 32mb RAM (my old PC)is faster than KDE on a P11 400 with 128mb RAM . \n\nThis really is not acceptable."
    author: "KDEuser"
  - subject: "Re: Reaction time (was: An Analysis of KDE Speed)"
    date: 2001-05-11
    body: "RAM *DOES* make a difference! It's true Win95 uses much less of it, a linux system running X and KDE needs *tons* of RAM, 128 MB is still a bit few.\nYou could upgrade to 192 MB or 256 MB RAM which should improve the response time of your system and you can check the widget theme you are using. Never import GTK themes into KDE as this will *KILL* performance, there is an enormous ressource leak related to this \"feature\" of KDE."
    author: "Gunter Ohrner"
  - subject: "Offtopic: a Suggestion for the dot"
    date: 2001-05-09
    body: "I wish when posting comments, the subject field would default to blank unless one is replying to an individual's post.  This way, threads will likely have more useful subject lines, instead of the current situation where 90% of the posts for this topic have a subject line of \"Re: An Analysis of KDE Speed\"\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: Offtopic: a Suggestion for the dot"
    date: 2001-05-10
    body: "Good idea.  I'll look into it.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Offtopic: a Suggestion for the dot"
    date: 2001-05-11
    body: "I wish I had a million dollars."
    author: "ac"
  - subject: "How about a memory saving wizard?"
    date: 2001-05-09
    body: "Won't it be cool to have a wizard that shows step by step setting that could be changed to reduce memory footprint and increase speed? \n\nThe wizard could check the system's memory and give suggestions. For eg. the caching of wallpapers could be tunrned off for low memory machines etc..\n\nsarang"
    author: "sarang"
  - subject: "GCC 3.0"
    date: 2001-05-09
    body: "I heard that gcc 3.0 will improve C++. But I don't know what kind of improvement.\nDoes anybody of you know?"
    author: "Schwann"
  - subject: "Re: GCC 3.0"
    date: 2001-05-09
    body: "http://www.bero.org/gcc296.html"
    author: "Evandro"
  - subject: "Re: GCC 3.0"
    date: 2001-05-09
    body: "GCC 3.0 will be more standards complient, the problem is that linker, not the compiler."
    author: "robert"
  - subject: "Re: GCC 3.0"
    date: 2001-05-09
    body: "No, it it's not only the linker. If gcc would produce more relocatable code, using relative adresses for example, the linker has do do less relocation."
    author: "Herbert Graeber"
  - subject: "Re: GCC 3.0"
    date: 2001-05-09
    body: "interesting ... could someone with gnuc 3.0 compiled kde do those relocation statistics (LD_DEBUG=statistics and so) and compare with those in the paper ?"
    author: "ik"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I can't help thinking sometimes that KDE is a victim of its own sucess. Most of the biggest problems with KDE come from the X server (bloat) and the underlining system (slow loading of libraries).\n\nThe abilty to handle protocols like cdrom:// and kamera:// is a great feature but features like like belong in the underlying system.\n\nMaybe its time that KDE starts to think about contributing to these underlying systems?"
    author: "Daniel"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Please read the TOP documentation, X runs on IPAQs very smoothly.  There is no better known algorithm to relocate funtion pointers, performance can only be increased slightly, the only fix for this is to use less virtual functions, which may never hapen because Qt is not developed specificly for KDE.  Protocols like cdrom, kamera should not be bound to any underlying system, not only is that poor design, they are dependant on Qt.  The problems with KDE are KDE's fault(It's a very good desktop thought)."
    author: "AC"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-11
    body: "Please don't post anymore unless you want to look stupid again."
    author: "jed"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Would statically linking applications instead of using shared libraries improve the performance? I mean, would that avoid most, if not all, of the ld.so bottleneck?"
    author: "itsme"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "That would most likely make the situations worse.\nUnlike what many people think, statically linked apps are NOT the solution.\nStatically links apps don't share memory, thus making the footprint much higher.\nBecause of that, the swap must be accessed more often, causing major slowdowns."
    author: "dc"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "Now that there's a central way of starting applications, I'm wondering if someone could implement an object pooling/caching mechanism like EJB containers do... Imagine the increased speed if the various objects are already created, and just stored in memory and/or disk.. \n\nI'd also be curious about the various programming patterns that have been applied, if any.. Maybe some more effiecent patterns designed to reduce the number of ojbects could help..\n\nPlease note I'm not a KDE developer, nor have I looked at the code, but I am planning on it, as there's some bugs that are annoying the hell outta me right now...\n\n--Garion911"
    author: "gairon911"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I was talking to one of the KDE developers at the \"3rd Braunschweiger Linux Tage\" (I forgot his name). He told me that Qt is faster than gtk+.\nHowever I can not verify this statement since on my machine (amd 500MHz) gtk+ runs A LOT smoother than any Qt program (even without KDE).\n\nIMO, KDE is THE BEST/MOST STABLE and MOST FEATURErich desktop.\n\nHowever if I can look at the widgets drawing I will stick on enlightenment 0.16 and wait for 0.17 to introduce evas which draws 115 frames/s on my box.\n\nI think KDE Speed depends on how you compile the whole thing. All approaches I tried did not seem to make KDE any faster.\n\nI would appreciate if anyone of the KDE developers could make a KDE-Compile-HOWTO that discusses various problems...\n\nBye,\n   Carsten"
    author: "Carsten Schmidt"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "I simply don't understand how QT is faster.\nSure, the themes might draw a little faster, but what's a few miliseconds?\nEven on my old Pentium 166 I don't notice any difference."
    author: "dc"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-11
    body: "There's a noticable difference, on my dual P2/300, using the Legacy GTK-style importer, GTK themes are significantly faster in Qt, then they are in GTK."
    author: "Charles Samuels"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-11
    body: "Wow!  You got the GTK theme importer to work!?  How?  All it does for me is cause KDE to crash and print out lots of \"Unknown object GTKCurve\" messages or something like that."
    author: "not me"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-12
    body: "I just tried it on the GTK-default (the only theme I have for GTK ;), and it works.. just not very well.  Maybe it just can't make sense of the style you selected?"
    author: "Charles Samuels"
  - subject: "Port to QT embeded ?"
    date: 2001-05-09
    body: "Do you know if there is any plan\nto port KDE to QT embeded ?\n\nThis is not a troll.\n\nregards."
    author: "thil"
  - subject: "Re: Port to QT embeded ?"
    date: 2001-05-09
    body: "Heh, your question is a bit off-topic, but not a troll in any way (the word 'troll' seems a bit over-used lately anyway)...\n\nTo answer your question: I know of at least some work to port Konqueror to Qt-embedded, take a look at kde-nox in CVS."
    author: "Theo van Klaveren"
  - subject: "Re: Port to QT embeded ?"
    date: 2001-05-10
    body: "If you mean for speed enhancement on a PC, loosing hardware acceleration will slow things down."
    author: "AC"
  - subject: "Re: Port to QT embeded ?"
    date: 2004-12-12
    body: "Hello Sir ,\n\n           I am ganesh kumar  Doing B.Tech I.T In Sakthi mariamman Engg College\n\nChennai ( Tamilnadu , INDIA) . I need the List of Project Title & Thier Ideas .\n\n           \n                PLEASE Reply me..................\n           I "
    author: "R.Ganesh Kumar"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-09
    body: "In my honest opinion, KDE is very slow. KDE improved in startup from 1.x to 2.x series, but... the general use of it is lots and lots of hard disk and RAM usage.\nAnother negative aspect about KDE is the way you install it. Tarball and compilation time are the longest i've ever seen, it could take a week.\n(thanks RPM's).\nThe suite is very extreemely integrated, having endless dependencies/libraries split in several packages. I would prefer to have a kde-whole.tar.gz of 50 MB and ./configure --disable-koffice --disable-graphics --disable-toys --disable-junk etc etc etc......."
    author: "_linCE"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "That's just wrong..\n\nThe only real dependency is that you need to compile kdelibs first....\n\nevery other kde package will then compile in any order you wish..."
    author: "Stuart Herring"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "I might have slid away in my comment about dependecy. But my concern is mostly about SPEED. And KDE 2.x and OpenOffice are the most SLUGGISH things I ever seen. Something is wrong. Compilation takes a LOT of time. Or am I wrong too?"
    author: "_linCE"
  - subject: "GNU Rope (grope)???"
    date: 2001-05-10
    body: "Does anybody know what happened to GNU Rope (grope)? It is a GNU replacement for SGIs cord.\n\nThe trick is to rearrange functions in binaries and libraries in groups often used together or in order of first call. This makes disc cache more efficient and uses less memory pages. \n\nThey claim to save 30% memory and could double speed. Unfortunately I only found a description, but no code or binaries.\n\nHere is a description:\n\nhttp://lwn.net/1998/1029/als/rope.html\n\n\nIf you do not find grope keep in mind:\n\n- Functions used at program start should be grouped in order of their first call -> makes disc cache more efficient -> reduces load time\n\n- Functions often used together should be located close to each other in binaries. If they fit in the same page of memory \"load on demand\" needs to load less pages. -> Faster and less memory usage\n\n\nBye,\n Stefan Heimers"
    author: "Stefan Heimers"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "I found some more info about those tools:\n\nSCO has one called fur\n\nhttp://uw7doc.sco.com/SDK_cdebug/_Using_fur_to_Perform_Block_Prof.html\n\nThe Intel-Compiler also can use runtime profiling information to optimize linking.\n\nYou might also want to have a look at the following pages:\n\nhttp://lists.gnome.org/archives/gnome-hackers/2000-October/msg00116.html\n\nThe author of grope is called Nat Friedman. His email adress is nat@nat.org. \n\nThis seems to be his homepage:\nhttp://www.nat.org\nBut it doesn't look very useful for any code optimization ;-)\n\n\nAnd there are some magic point presentation slides:\nhttp://www.hungry.com/~shaver/grope/slides/\n\n\nhttp://www.tux.org/hypermail/linux-kernel/1999week30/0290.html\n\n\n\n\nI still did not find any code :-(\n\n\nBye,\n Stefan Heimers"
    author: "Stefan Heimers"
  - subject: "KDE and configure --enable-debug"
    date: 2001-05-10
    body: "When kde is compiled using --enable-debug, what exactly does this change as far as speed or memory usage?\n\n\tWhat I have always compared it to is the .dbg files generated in visual studio. I know relatively little about them but they enable you to pinpoint problems with a debugger, enabling you to view the call stack. These files are not part of the normal executable, but enable you to get the debug symbols without the extra memory usage. Is this a wrong anology, and what would prevent kde from compiling debugging information to a file? \n\tI believe this would be beneficial if implemented in kde so people would not have to compile from source code to be able to give backtraces when submitting a bug reports and then not have to deal with any extra memory usage that having them built in?"
    author: "Jeff Brybajer"
  - subject: "Re: KDE and configure --enable-debug"
    date: 2001-05-12
    body: "What I started to do recently is to compile everything with --enable-debug so that the binaries contain the symbols. Then I install it (to /usr/local/kde) and execute \"strip bin/* lib/*\". This saves a LOT of disk space. If I need to report a bug with backtrace I only have to reinstall the package, not recompile it."
    author: "Erik"
  - subject: "Avoiding relocating? Is this possible"
    date: 2001-05-10
    body: "I am not so sure about what i will suggest here, Comment are welcome:\n\n- It is possible to change ld algorithm so that:\n\n-- Any share library is loaded at boot time, relocated to a different space, and saved back to the disk using this default adressing (may be replacing the original library??).ld will reserve these adress spacea for ever, making it impossible for any library not in ld normal cache to used it.\n\n-- this does not need to be executed at each reboot, as probability is high that nothing as changed...\n\n-- Any furhter attempt to uses this library is done from the relocated version. There is not need to do the job anymore.\n\n-- any configuration changes (Add library, ...) is performed as usual... or as during boot phase to improve the mechanism.\n\nThe previous idea as the advantage to ensure:\n- maximum usage of share pages (All library are always at the same location)\n- slow a little bit he boot process, especially after new installation.\n- reduce loading time.\n\nAny comment??\n\nStephane"
    author: "stephane PETITHOMME"
  - subject: "Re: Avoiding relocating? Is this possible"
    date: 2001-05-10
    body: "Yes, something like that would work.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Avoiding relocating? Is this possible"
    date: 2001-05-10
    body: "I see a great deal of potential in this\n\nGive the libraries their own place in memory so:\n\n- nothing has to be relocated.\n- libraries can be precached.\n\nDoesn't solve the 800kb of duplicated vtables per kde program, but heh, it's a start.\n\nregards,\n\nohan Veenstra"
    author: "Johan Veenstra"
  - subject: "Re: Avoiding relocating? Is this possible"
    date: 2001-05-10
    body: "I see a great deal of potential in this\n\nGive the libraries their own place in memory so:\n\n- nothing has to be relocated.\n- libraries can be precached.\n\nDoesn't solve the 800kb of duplicated vtables per kde program, but heh, it's a start.\n\nregards,\n\nJohan Veenstra"
    author: "Johan Veenstra"
  - subject: "[Avoiding relocating?] Yes, like REBASE.EXE"
    date: 2001-05-10
    body: "The relocation problem exists on windows also.\nBut to avoid relocations on every app start, microsoft released a tool which pre-relocates each DLL (.so) to a different address, so they don't collide. Here's a link to MS Systems Journal: <A href=\"http://www.microsoft.com/msj/defaultframe.asp?page=/msj/0498/bugslayer0498.htm&nav=/msj/0498/newnav.htm\">bugslayer</A>\n((Hmmm sorry URL is funny - I only see Plain encoding in the submit form??))\n\nMaybe a similar utility could work for linux, and maybe it already exists? Then SuSe and other distros would run it before packaging their product, and we could save the half-second.\nHope this is correct,\n/george"
    author: "george moudry"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "I'm not really into the technical stuff of KDE so this may not make sence but\nisn't it an idea to get al the GUI and render stuff hardware accelerated? I believe Enlightenment is doing this and I suppose this will improve the speed dramatically."
    author: "Jan Hanstede"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "Hmm. In Waldo's tables not only linking is the performance killer, but also the application specific initialization, isn't it? I mean, how can a simple application like KEdit consume 0.48 seconds for it's initialisation? Or is KEdit more complicated than I always thought?"
    author: "Johann Lermer"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "Well, ok, probably the speed of KDE can be improved. However, from my own experience, I can say that I have used KDE 2.1.1 on everything from a K6-2 with 300 Mhz and 96 MB of RAM to an AMD Athlon at 1000 Mhz and 512 MB of RAM. On all of these machines it ran fast enough for my taste.\n\nIn the KDE poll, I voted for improvements in KOffice. Why? Well, konqueror has already replaced Netscape as my default browser since I find it really great. What I'm still missing is a KOffice that is able to replace StarOffice as my default office suite. That's what I believe is one of the most important things in the future of KDE. Of course, this does not mean that speed is not an issue that should be looked at, but I'd rather wait some time for a desktop environment with some really great features than to have some fast desktop that lacks some very useful things.\n\nBottom line is: It's probably a bad idea to work only on improving speed or only on improving KOffice. A broad range of improvements in all areas is probably what will make people happy in the end.\n\nGreetings,\nNils"
    author: "Nils Holland"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "Ohyeah, KDE needs to be alot faster !\n\nI mean, emacs is so much faster on my 386 DX40 with 8mb memory (though I am thinking of buying a color monitor....)\n\nIf you do not have the money to buy a new machine to match KDE performance, than run a window manager that was designed to run on slow hardware like blackbox !\n\nOr stop whining and start to help the KDE people so it will run on your anchient hardware !\n\nSorry about this but it is getting a bit anoying to see people complaining about something they get for free (and can help to improve)"
    author: "Robbin Bonthond"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-11
    body: "people are not complaining here. they answer to a poll from KDE."
    author: "oliv"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-13
    body: "i totally agree"
    author: "fsa"
  - subject: "Benchmarking of KDE Speed"
    date: 2001-05-10
    body: "Perhaps it is possible to make a program that benchmarks several parts of KDE. Also it could have the abilitie to post it to a database on the web, along with information about the hardware in the machine that KDE is running on, so we all can see how a certain KDE version reacts on different configurations ? At least it would help to get a better idea of the \"speed of KDE\""
    author: "Robbin Bonthond"
  - subject: "Benchmarking of KDE Speed"
    date: 2001-05-10
    body: "Perhaps it is possible to make a program that benchmarks several parts of KDE. Also it could have the abilitie to post it to a database on the web, along with information about the hardware in the machine that KDE is running on, so we all can see how a certain KDE version reacts on different configurations ? At least it would help to get a better idea of the \"speed of KDE\""
    author: "Robbin Bonthond"
  - subject: "Benchmarking of KDE Speed"
    date: 2001-05-10
    body: "Perhaps it is possible to make a program that benchmarks several parts of KDE. Also it could have the abilitie to post it to a database on the web, along with information about the hardware in the machine that KDE is running on, so we all can see how a certain KDE version reacts on different configurations ? At least it would help to get a better idea of the \"speed of KDE\""
    author: "Robbin Bonthond"
  - subject: "Benchmarking of KDE Speed"
    date: 2001-05-10
    body: "Perhaps it is possible to make a program that benchmarks several parts of KDE. Also it could have the abilitie to post it to a database on the web, along with information about the hardware in the machine that KDE is running on, so we all can see how a certain KDE version reacts on different configurations ? At least it would help to get a better idea of the \"speed of KDE\""
    author: "Robbin Bonthond"
  - subject: "Re: Benchmarking of KDE Speed"
    date: 2001-05-10
    body: "Perhaps you shouldn't post the same 4 times in a row.\n\nJelmer"
    author: "Jelmer Feenstra"
  - subject: "Re: Benchmarking of KDE Speed"
    date: 2001-05-15
    body: "Could you post that a few more times ?"
    author: "Charles McCormick"
  - subject: "Speed vs Memory usage"
    date: 2001-05-10
    body: "I would to \"debloat\" kde disabling all unnecessary daemons but I can't find any documentations that explains the services needed by kde.\nFor example, what is the daemons kwrited ?\n\nAfter using konqueror, there are many process in memory: kdeinit:kio_http ......\nWhy are these process still in memory even if a close konqueror?\n\n--\nNothingman"
    author: "Nothingman"
  - subject: "Other stuff to disable"
    date: 2001-05-10
    body: "I'm with you. How about disabling the sound stuff for systems without sound? How about disabling kxmlrpcd? What the heck are khotkeys, kded or kwrited? It would be nice to have a UI to disable some of that herd of daemons that KDE starts up. Even some non-programmer documentation would be helpful.\n\nChris"
    author: "Chris W"
  - subject: "Re: Other stuff to disable"
    date: 2001-05-10
    body: "\"How about disabling the sound stuff for systems without sound?\"\n\nIt is disabled.  If artsd is not running, the \"sound stuff\" is disabled.  If artsd doesn't find any sound hardware, it exits.\n\n\"What the heck are khotkeys, kded or kwrited?\"\n\nKHotKeys recognizes hotkeys, of course!  kded probably has something to do with Drag-and-Drop compatibility.  I don't know what kwrited is.  If you want a performance increase, though, you're looking in the wrong place.  Daemons like these use only a little piddling amount of memory (which gets swapped out to disk when they're idle) and next to no CPU time (on the order of seconds of CPU time per week they are running - unless they are used).  Try killing them and then see if you can tell the difference in speed.  I guarantee you won't be able to tell (unless you have 8 MB of RAM or something)."
    author: "not me"
  - subject: "Re: Other stuff to disable"
    date: 2001-05-11
    body: "It is not so much performance that I am thinking of, but complexity. In any case, kxmlrpcd should be disabled, yet there is no obvious way to do so. Why should the ability to execute remote procedure calls on your machine be automatically enabled in a desktop environment? It's a security hazard and a generally unnecessary feature."
    author: "Chris W`"
  - subject: "Re: Other stuff to disable"
    date: 2001-05-11
    body: "I would like not to use arts because I have a SB Live!, but that's impossible with KDE."
    author: "AC"
  - subject: "Re: Other stuff to disable"
    date: 2001-05-13
    body: "uhh? that makes no sense\n\narts works with all sound cards.\n\nit doesn't access the hardware directly, but rather through your snd driver :p"
    author: "fsa"
  - subject: "Re: Speed vs Memory usage"
    date: 2001-05-11
    body: "Exactly!\nWhat are all these daemons and processes good for, if there is no difference wether they are killed or not? These number of processes is already dangerously close to what can be seen in windoze an its stability. Related with this is memory footprint in RAM.\nSo what is needed is:\n1. Less memory usage.\n2. Better control of which processes shall be started.\n3. Possibility to use KDE2 applications from other environments, ie. gnome.\n4. Improvements to Koffice, especially MS filters."
    author: "P.Braun"
  - subject: "Re: Speed vs Memory usage"
    date: 2001-05-13
    body: "kwrited has nothing to do with kde. it is a kernel daemon"
    author: "fsa"
  - subject: "kwrited is part of KDE"
    date: 2001-05-18
    body: "Do a \"ps ax\" some time. Here is what I got:\n\n17924 ?        S      0:00 kdeinit: kwrited\n\nThe kernel would not start its modules with kdeinit, would it?"
    author: "Chris W"
  - subject: "Re: kwrited is part of KDE"
    date: 2003-09-29
    body: "Hi i had same problem, i dont know what its for but i found out how to turn autostart off.\ndo  nano /opt/kde/share/config/kwritedrc  \n\n[General]\nAutostart=true\n\nchange it to\n\n[General]\nAutostart=false"
    author: "Ivan"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-10
    body: "The same problem appears probably when loading StarOffice and many other apps. So system-level solution would be much better."
    author: "Hi"
  - subject: "Re:KDE Speed analysis, we need so_locations file?"
    date: 2001-05-11
    body: "Excellent paper.  As I have experienced it, this\nis a problem that spans many applications.  If\na shared is located in the 'next available' vm\naddress then we are begging for trouble because\neach application has unique offsets and library\nload orders.  Staroffice is a prime example.  It\ntakes way too long and beats the disk to death\njust to get the logo up...\n\nCompaq Tru-64 (aka DEC OSF/1) solved this problem\nby loading shared objects at fixed VM addresses\nand storing the address maps in a 'so_locations'\nfile.  The linker had access to this file at\nlink time and the dynamic loader used it to\nprime the mmap.  The end result was that reloc\nwas already done.  There is also the option\nto reloc on the fly if there is an address space\nconfict at the expense of lots of loader work.\nThis was done because 8 byte address fixups are\ntwice as bad as 4 byte ones.\n\nSorry, I don't have access to an alpha at this\ntime and I don't know enough about the dirty\ndetails of the GNU tool chain to say more at this\ntime other than I have not seen anything \nindicating such.  If it is not there, it should\nbe.  Anyone with KDE on Tru-64 to prove/disprove\nmy assumptions?"
    author: "Jim Lieb"
  - subject: "Re:KDE Speed analysis, we need so_locations file?"
    date: 2001-05-11
    body: "Windows does this as well, kind of, DLLs have a prefered address on windows."
    author: "AC"
  - subject: "Re:KDE Speed analysis, we need so_locations file?"
    date: 2001-05-23
    body: "Yeah, but they have a 64-bit address space, so you have less chance of hitting address space twice when loading stuff.\n32-bits systems give you 4 gigs of ram, so you could predefine all these libs to load in the 2 to 4 gig area, but soon all systems will be carrying 4 gigs of ram, and then you'll notice you've only filled a hole by digging another one because your apps will want to load where your libraries are set to preload."
    author: "Joeri Sebrechts"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-11
    body: "I don't really understand everything that's going on, so excuse me if this sounds absurd.  Would it be possible to make ld.so have the same hack that kdeinit has so that every program gets the \"magic\" speedup!?"
    author: "Martin Fick"
  - subject: "How Win32 does it, AFAIK"
    date: 2001-05-11
    body: "As I remember, Win32 DLLs define a base address that it pops into.  Microsoft defines different base addresses for its DLLs such that the standard DLLs don't overlap and thus no address fixups required.  If an application uses the default base addresses in its DLLs, Windows fixes up the addresses while loading the DLLs, making the process slower, especially in huge programs.\n\nAre Win32 DLL address fixups similar in spirit to address relocations?  Is there any way to predefine base addresses in the libraries so that the relocations are not necessary, or am I missing the point?"
    author: "Steve Lawrance"
  - subject: "Re: How Win32 does it, AFAIK"
    date: 2001-05-11
    body: "<i>Every library that is needed by an application gets loaded to an address that \nis unique within that process. This address may vary each time the library is\nloaded. <ul>Code that references addresses in the library must be adjusted for the\naddress the library is loaded to, this is called relocation.</ul> ...</i>\n\nHmm.. It looks like Win32 base addresses in DLLs might be exactly the same thing.  Does the GNU toolchain allow you to specify a predefined base address in shared libraries so that the dynamic linker can simply pop in the shared objects and not worry about relocations until a conflict occurs, as in Win32?\n\nLoading up Notepad, Word, Excel, Access, and PowerPoint in WinNT on this PIII 500 with 256MB of RAM comes up instantly, as they almost did when I used to run NT4 on my dual P1-233 with 64MB (now it runs Linux 2.4.2, KDE 2.1, and has 192MB, and KDE programs take a little while to load)."
    author: "Steve Lawrance"
  - subject: "KDE is slow but faster on some Unices than others"
    date: 2001-05-11
    body: "kde 1.0 seemed faster and actually timed out to be about 10-15% faster on NetBSD and FreeBSD than it was on Linux 2.2 - this could've just been defaiult kernel and optimization settings being different or other factors.\n\nIn any case a large part of the problme seems to derive from ld.so being designed before graphics, GUI, multimedia and point and click existed\n\nThere's a lot more launching of apps going on in a GUI than in c-line environment: libs and memory are used differently.\n\nHow do other or commercial Unices (like Irix, Solaris, AIX etc) deal with this??"
    author: "guy"
  - subject: "KDE is slow but faster on some Unices than others"
    date: 2001-05-11
    body: "kde 1.0 seemed faster and actually timed out to be about 10-15% faster on NetBSD and FreeBSD than it was on Linux 2.2 - this could've just been defaiult kernel and optimization settings being different or other factors.\n\nIn any case a large part of the problme seems to derive from ld.so being designed before graphics, GUI, multimedia and point and click existed\n\nThere's a lot more launching of apps going on in a GUI than in c-line environment: libs and memory are used differently.\n\nHow do other or commercial Unices (like Irix, Solaris, AIX etc) deal with this??"
    author: "guy"
  - subject: "KDE is slow but faster on some Unices than others"
    date: 2001-05-11
    body: "kde 1.0 seemed faster and actually timed out to be about 10-15% faster on NetBSD and FreeBSD than it was on Linux 2.2 - this could've just been defaiult kernel and optimization settings being different or other factors.\n\nIn any case a large part of the problme seems to derive from ld.so being designed before graphics, GUI, multimedia and point and click existed\n\nThere's a lot more launching of apps going on in a GUI than in c-line environment: libs and memory are used differently.\n\nHow do other or commercial Unices (like Irix, Solaris, AIX etc) deal with this??"
    author: "guy"
  - subject: "KDE is slow but faster on some Unices than others"
    date: 2001-05-11
    body: "kde 1.0 seemed faster and actually timed out to be about 10-15% faster on NetBSD and FreeBSD than it was on Linux 2.2 - this could've just been defaiult kernel and optimization settings being different or other factors.\n\nIn any case a large part of the problme seems to derive from ld.so being designed before graphics, GUI, multimedia and point and click existed\n\nThere's a lot more launching of apps going on in a GUI than in c-line environment: libs and memory are used differently.\n\nHow do other or commercial Unices (like Irix, Solaris, AIX etc) deal with this??"
    author: "guy"
  - subject: "KDE is slow but faster on some Unices than others"
    date: 2001-05-11
    body: "kde 1.0 seemed faster and actually timed out to be about 10-15% faster on NetBSD and FreeBSD than it was on Linux 2.2 - this could've just been defaiult kernel and optimization settings being different or other factors.\n\nIn any case a large part of the problme seems to derive from ld.so being designed before graphics, GUI, multimedia and point and click existed\n\nThere's a lot more launching of apps going on in a GUI than in c-line environment: libs and memory are used differently.\n\nHow do other or commercial Unices (like Irix, Solaris, AIX etc) deal with this??"
    author: "guy"
  - subject: "KDE is slow but faster on some Unices than others"
    date: 2001-05-11
    body: "kde 1.0 seemed faster and actually timed out to be about 10-15% faster on NetBSD and FreeBSD than it was on Linux 2.2 - this could've just been defaiult kernel and optimization settings being different or other factors.\n\nIn any case a large part of the problme seems to derive from ld.so being designed before graphics, GUI, multimedia and point and click existed\n\nThere's a lot more launching of apps going on in a GUI than in c-line environment: libs and memory are used differently.\n\nHow do other or commercial Unices (like Irix, Solaris, AIX etc) deal with this??"
    author: "guy"
  - subject: "KDE is slow but faster on some Unices than others"
    date: 2001-05-11
    body: "kde 1.0 seemed faster and actually timed out to be about 10-15% faster on NetBSD and FreeBSD than it was on Linux 2.2 - this could've just been defaiult kernel and optimization settings being different or other factors.\n\nIn any case a large part of the problme seems to derive from ld.so being designed before graphics, GUI, multimedia and point and click existed.\n\nThere's a lot more launching of apps going on in a GUI than in c-line environment: libs and memory are used differently.\n\nHow do other or commercial Unices (like Irix, Solaris, AIX etc) deal with this??"
    author: "guy"
  - subject: "Re: KDE is slow but faster on some Unices than others"
    date: 2001-05-11
    body: "This multiple posting is really becoming a problem.  If a Dot admin is reading this, PLEASE put some BIG RED TEXT next to the \"add\" button noting that even if your browser times out on the connection and doesn't display the confirmation page, the post was STILL POSTED!  It's happened to me more than once.\n\nAs a better solution, you might look into why the Dot responds only sporadically to requests for pages.  I have a 75 ms ping time to the Dot, but pages sometimes take minutes to request.  Sometimes my browser gives up and times out.  Once the page starts to download, though, it downloads very fast.  Something is not quite right."
    author: "not me"
  - subject: "Re: KDE is slow but faster on some Unices than others"
    date: 2001-05-14
    body: "I think that slashcode prevents posting messages with identical text. Doing that here as well looks much better to me than writing a warning to the user.\nThe connection will also time out if a comment could not be posted successfully!"
    author: "Arnd Bergmann"
  - subject: "Compile Times"
    date: 2001-05-11
    body: "Well, sure compilation takes forever, kdelibs is somewhere in the hundreds of thousands of Lines of Code, and gcc has always been very inneficient when it comes to compiling C++.  But then again, people only compile things once normally, and C++ is faster than C for these things anyway, at run-time."
    author: "Charles Samuels"
  - subject: "solutions for faster invocation"
    date: 2001-05-11
    body: "My project http://www.BitWagon.com/elfvector.html speeds up application start by providing transfer vectors.  PLT relocation goes from 1 per symbol to 1 per library.  The symbols covered cannot be overridden, but it is easy to exclude an arbitrary list (such as malloc,calloc,realloc,free,memalign) by regular expression matching on the symbol name.  You can even retrofit elfvector into existing shared libraries and existing apps; no recompilation and no relinking are required.  What is required is maintenance: the ordered list of symbols is now very important, and it must be versioned, etc.\n\nThe instructions generated for calling a virtual function could be changed to delay relocation of the vtable until the first call through it, by adding one more level of indirection.  After the first call, the ongoing cost would be 2 cycles per call.\n\nMore generally, a shared library linked as an ET_EXEC file loaded at a fixed address could be handled by _dl_map_object_from_fd() in glibc-2.2/elf/dl-load.c. Currently anything that is not ET_DYN is rejected, but there is essentially no difference in file format between ET_EXEC and ET_DYN.  The only real problem is detecting address conflicts at runtime, in order to diagnose errors.  The runtime linker could use a bitmap of 1 bit per page (32-bit address space with pages of 4KB requries 128KBytes).  To handle conflicts between dynamic linking and malloc(), it would be nice to have a binary interface to /proc/self/maps, perhaps implemented via ioctl().  sbrk() should be deprecated in favor of a user-space page manager which provides services to both malloc, mmap, dlopen, etc.\n\nAnd while we're here, there is a missing piece in <link.h>, namely\n        ElfW(Ehdr) *l_elfehdr;\nin the \"public protocol\" section of struct link_map:\n-----\nstruct link_map\n  {\n    /* These first few members are part of the protocol with the debugger.\n       This is the same format used in SVR4.  */\n\n    ElfW(Addr) l_addr;          /* Base address shared object is loaded at.  */\n    char *l_name;               /* Absolute file name object was found in.  */\n    ElfW(Dyn) *l_ld;            /* Dynamic section of the shared object.  */\n    struct link_map *l_next, *l_prev; /* Chain of loaded objects.  */\n    ElfW(Ehdr) *l_elfhdr;       /* Public, so everyone can find it */\n-----\n\nYes, this partially duplicates \n    const ElfW(Phdr) *l_phdr;   /* Pointer to program header table in core.  */\n    ElfW(Half) l_phnum;         /* Number of program header entries.  */\nlater in the structure, but these are \"private\" to the implementation.\n\n[Neither Netscape nor Opera could select anything other than Plain Text for Encoding this reply!]"
    author: "John Reiser"
  - subject: "Re: solutions for faster invocation"
    date: 2001-05-11
    body: "[Neither Netscape nor Opera could select anything other than Plain Text for Encoding this reply!]\n\nYes, HTML posting was disabled some time ago after someone posted some destructive Javascript.  I can't understand why they haven't re-enabled it yet, though, especially since the malicious poster used the name and title fields for his javascript and not the comment field.  The comment field itself is already protected from malicious javascript, it has always been that way.  Please give us HTML posting back!  There is no reason not to!"
    author: "not me"
  - subject: "Re: solutions for faster invocation"
    date: 2001-05-11
    body: "Oh yeah, I wanted to comment on your post as well.  Looks very cool, but very technical.  What must a user do to take advantage of this now?  Can KDE use this even if other components of the system don't and still be compatible and portable?  What kind of speed increase and size decrease can be expected for libraries the size of KDE?"
    author: "not me"
  - subject: "Re: solutions for faster invocation - right place"
    date: 2001-05-12
    body: "perhaps , if possible , a better place for the solution would be in the linker because *ALL* apps qould benefit , not only the one's done with the speed increase programm you discribe.\n\n\nif it cant be resolved (i dont know as much about resolving symbols as you do) your solution would be nice..."
    author: "chris"
  - subject: "It's UNIX people, the solution is easy!"
    date: 2001-05-12
    body: "1) start machine\n2) start all necessary apps (could take time)\n3) leave apps and machine running until next kernel upgrade (every year or so for me).\n\nLaptops have battery back up and deep sleep you never have to turn them off ;-)"
    author: "Koochicoo"
  - subject: "Re: It's UNIX people, the solution is easy!"
    date: 2001-05-13
    body: "4) Buy earplug for those who happens to have the computer in the bedroom.\n\n:-)"
    author: "reno"
  - subject: "Re: It's UNIX people, the solution is easy!"
    date: 2001-05-16
    body: "Yeah, sure. And increase my electricity bill with a factor 10. \n\nAnd run the risk of finding my house burned-down if a short-circuit happens in the powersupply while I'm not at home..."
    author: "Alex Wulms"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-12
    body: "Could vtable relocations be made lazy also through the use of virtual memory somehow?  Have all pages with vtables not accessible, and when access is attempted, then you do the relocations.  I guess this might want to be in the kernel.\n\n-Yonik"
    author: "Yonik Seeley"
  - subject: "fast starting konqueror if already open windows"
    date: 2001-05-12
    body: "this script opens a new konqueror window with an url really fast if there are already konqueror windows open:\n\n\n#!/bin/sh\nif ps auxwwwwww | grep konqueror | grep -v kio_http | grep -v grep >/dev/null;\ndcop konqueror KonquerorIface openBrowserWindow $*\nelse\nkonqueror $*\nfi"
    author: "ik"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-13
    body: "In my experience, KDE tends to be algorithmically pretty fast... What slows it down is the page faults, when it has to swap stuff in. I find KDE more than fast enough on a K6-233 w/64+ Mb of ram... But when it does slow down, it is because of ram utilisation. (And 32mb is painful)\nReducing the memory footprint of KDE, then, would actually result in a massive speed boost on a lot of lower end machines (which are what we're worrying about in the first place...) <p>"
    author: "C. D. Thompson-Walsh"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-14
    body: "i find kde REALLY slow on my 1400mhz 512 ram machine , it never swaps , but still it is soooo slow !!!\n\nyou see , slowlyness is relative...\n\n\nchris"
    author: "chris"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-14
    body: "About lower end machines, what would you say about  porting Qt + KDE without X-window into desktop PCs, too. That would be a huge improvement in speed and memory usage.\nNow the Qt/Embedded is meant only for PDAs etc.\n\nJuha Manninen"
    author: "Juha Manninen"
  - subject: "Fixed base address implemeted in ldconfig?"
    date: 2001-05-13
    body: "The solution of having shared libraries load at fixed addresses seems like a great solution to me. Windows and Tru64 apparently do this too.\n\nIt could be implemented in a rather straightforward manner in ldconfig. After searching the library path for shared libs, it should assign a unique address to each one. This value can be stored in either a special ELF section or in a system wide file (maybe a system wide file is better). At runtime, ld.so uses this information to load the library at the appropriate address. If it sees that it must load libraries that are not in this \"base address cache\", it relocates the shared libraries in the usual way.\n\nI never looked at the source of ldconfig or ld.so but the above change seems easy enough."
    author: "Geert Jansen"
  - subject: "Re: Fixed base address implemeted in ldconfig?"
    date: 2001-05-14
    body: "Apologies if I'm far off the mark -- I'm not versed in the details of how the dynamic linker works. \n\nI think at first glance, it would seem that the problem would be solved by giving every shared library a unique base address.  This would presumably mean that libraries are slurped into preferred spots in the address space, and symbols end up at predictable addresses.\n  \nBut it is not so much the base address which matters, but the offsets of each symbol in the library.  Symbols change in size and may be ordered differently between versions of the library.  Knowing a base address would no longer help.\n\nI would guess that this is why shared libraries contain huge hash tables of strings against symbol offsets:  other than a string name, the program has no reliable way of naming a symbol it is interested in.\n\nSo, it seems that this approach would require more than caching the base address:  it would also require that libraries and programs be linked symbolically at build time, linked again at install time with known base addresses in mind, then linked again at runtime if symbols move underfoot.\n\nI think the install-time link would be prohibitively slow, especially for people who keep a constant churn of upgrades on their systems."
    author: "Ian Chiew"
  - subject: "Re: Fixed base address implemeted in ldconfig?"
    date: 2001-05-14
    body: "Apologies if I'm far off the mark -- I'm not versed in the details of how the dynamic linker works. \n\nI think at first glance, it would seem that the problem would be solved by giving every shared library a unique base address.  This would presumably mean that libraries are slurped into preferred spots in the address space, and symbols end up at predictable addresses.\n  \nBut it is not so much the base address which matters, but the offsets of each symbol in the library.  Symbols change in size and may be ordered differently between versions of the library.  Knowing a base address would no longer help.\n\nI would guess that this is why shared libraries contain huge hash tables of strings against symbol offsets:  other than a string name, the program has no reliable way of naming a symbol it is interested in.\n\nSo, it seems that this approach would require more than caching the base address:  it would also require that libraries and programs be linked symbolically at build time, linked again at install time with known base addresses in mind, then linked again at runtime if symbols move underfoot.\n\nI think the install-time link would be prohibitively slow, especially for people who keep a constant churn of upgrades on their systems."
    author: "Ian Chiew"
  - subject: "Re: Fixed base address implemeted in ldconfig?"
    date: 2001-05-14
    body: "Apologies if I'm far off the mark -- I'm not versed in the details of how the dynamic linker works. \n\nI think at first glance, it would seem that the problem would be solved by giving every shared library a unique base address.  This would presumably mean that libraries are slurped into preferred spots in the address space, and symbols end up at predictable addresses.\n  \nBut it is not so much the base address which matters, but the offsets of each symbol in the library.  Symbols change in size and may be ordered differently between versions of the library.  Knowing a base address would no longer help.\n\nI would guess that this is why shared libraries contain huge hash tables of strings against symbol offsets:  other than a string name, the program has no reliable way of naming a symbol it is interested in.\n\nSo, it seems that this approach would require more than caching the base address:  it would also require that libraries and programs be linked symbolically at build time, linked again at install time with known base addresses in mind, then linked again at runtime if symbols move underfoot.\n\nI think the install-time link would be prohibitively slow, especially for people who keep a constant churn of upgrades on their systems."
    author: "Ian Chiew"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-13
    body: "What does `speed' mean?  \n\nIt seems that some have taken it to mean application startup time, and others the general performance of KDE."
    author: "Ian Chiew"
  - subject: "What does speed mean - use dymamic loading more?"
    date: 2001-05-13
    body: "A good way to speed up application startup time is to not load all the code at startup, but only when it is needed. So you could speed up an app by splitting it into a small core, along with a number of dynamically loaded KParts. The KParts can be loaded lazily on demand, and you don't need to always load code for the areas of the UI that the user only very occasionally might want to touch - eg preferences or help.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: What does speed mean - use dymamic loading more?"
    date: 2001-05-14
    body: "From what I can understand of Waldo's paper, it seems that the startup time problem would then have two parts.  There's the time it takes for the base KDE libraries to load, and there is the incremental cost of introducing more functionality in the form of libraries.\n\nIt appears that both are particularly bad.  \n\nI agree that KParts and the general practice of breaking up applications into large blocks of demand-loaded functionality is a great way to tackle the second problem.  \n\nThat, though, still leaves the first problem.  Even small applications like KEdit (which use a minimal subset of the KDE framework) seem to take many times longer (and a great deal more processor time) than their GNOME or Windows counterparts to start up."
    author: "Ian Chiew"
  - subject: "Re: An Analysis of KDE Speed"
    date: 2001-05-14
    body: "When I took my computer home, where I have no Internet connection, all Qt based applications took like 1 minute to start (otherwise they took like 1 second). So it seems to me, that each Qt application looks for something on the Internet. Maybe this is the reason for slow startup of the Qt based applications or am I just wrong?"
    author: "Bojan"
  - subject: "solution that works _today_"
    date: 2001-05-14
    body: "Here's how to get rid of nearly all relocations, by assigning fixed virtual addresses to shared libraries.  You can run it and measure its performance today.  Assign the addresses from the space below 0x08048000; leave a gap between adjacent libraries for expansion, etc.\n\n# demo: main() calls foo(); foo() printf()s a message\ngcc -c main.c foo.c  # compile only\n\n# Create a \"shared library\" at a fixed address using foo.o\n# elf_i386.x is a copy of /usr/lib/ldscripts/elf_i386.x,\n# EXCEPT that `0x08048000' has been changed, such as to `0x07ff0000'.\ngcc -Wl,-E -nostartfiles  -o foo.so -T elf_i386.x  foo.o\n\ncat >dyn4exec.c <<EOF\n#include <elf.h>\n#include <sys/mman.h>\n#include <fcntl.h>\n\nmain(int argc, char const *const *const argv)\n{\n        int const fd = open(argv[1], O_RDWR, 0);\n        Elf32_Ehdr *const ehdr = (Elf32_Ehdr *)mmap(0, 4096,\n                PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);\n        if (ehdr->e_type!=ET_EXEC) {\n                return 1;\n        }\n        ehdr->e_type = ET_DYN;\n        return 0;\n}\nEOF\n\ngcc -o dyn4exec dyn4exec.c\n./dyn4exec foo.so  # sets Elf32_Ehdr.e_type = ET_DYN\n\ngcc -o main main.o foo.so  # link main() to new shared lib\n\nexport LD_LIBRARY_PATH=`pwd`:$LD_LIBRARY_PATH  \n./main  # test run"
    author: "John Reiser"
  - subject: "KDE is faster under BSD: here's why"
    date: 2001-05-15
    body: "I've found this article digging into mozilla's\nperformance newsgroup. It may be of interest:\n\nFrom the FreeBSD mailing lists:\n\n\njdp         2001/05/05 16:21:05 PDT\n\n  Modified files:\n    libexec/rtld-elf     rtld.c rtld.h \n    libexec/rtld-elf/alpha reloc.c \n    libexec/rtld-elf/i386 reloc.c \n  Log:\n  Performance improvements for the ELF dynamic linker.  These particularly help programs which load many shared libraries with a lot of relocations.  Large C++ programs such as are found in KDE are a prime example.\n  \nWhile relocating a shared object, maintain a vector of symbols which have already been looked up, directly indexed by symbol number.  \nTypically, symbols which are referenced by a relocation entry are referenced by many of them.  This is the same optimization I made to the a.out dynamic linker in 1995 (rtld.c revision 1.30).\n  \nAlso, compare the first character of a sought-after symbol with its symbol table entry before calling strcmp().\n  \nOn a PII/400 these changes reduce the start-up time of a typical KDE program from 833 msec (elapsed) to 370 msec.\n  \n  MFC after:    5 days\n  \n  Revision  Changes    Path\n  1.52      +22 -6     src/libexec/rtld-elf/rtld.c\n  1.22      +11 -2     src/libexec/rtld-elf/rtld.h\n  1.12      +12 -5     src/libexec/rtld-elf/alpha/reloc.c\n  1.7       +10 -5     src/libexec/rtld-elf/i386/reloc.c"
    author: "Andrea"
---
Our recent <A HREF="http://www.kde.com/polls/vote.php?poll=5">poll</A> (courtesy <a href="http://www.kde.com/">KDE.com</a>) on the upcoming KDE 2.2 <a href="http://www.kde.com/polls/view.php?poll=5">suggests</a> that the area of
greatest concern for KDE users is speed -- at this time, out of 3,463 votes, over 24% consider speed as most important for developers to address.  <A HREF="http://www.kde.org/people/waldo.html">Waldo Bastian</A>, who developed the <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kinit/">kdeinit</a> speed hack among other things, has <A HREF="http://www.suse.de/~bastian/Export/linking.txt">written a paper</A> entitled <EM>"Making C++ ready for the desktop"</EM>, in which he analyzes the various startup phases of a C++ program.  Noting that one component of linking -- namely, library relocations -- is currently slow, he offers some suggestions for optimizations.  An interesting read.
<!--break-->
