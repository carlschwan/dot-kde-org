---
title: "ZDNet reviews KDE 2.2.1"
date:    2001-09-21
authors:
  - "Anonymous"
slug:    zdnet-reviews-kde-221
comments:
  - subject: "Funny"
    date: 2001-09-21
    body: "I'm so glad that when I clicked on it to read it, the (big, ugly) embedded ad was for MS Office XP. Specifically, touting their \"Auto Suggest\" feature for form completion in the \"To:\" field of the mail program.\n\nIs that really a feature worth advertising?\n\n-pos"
    author: "pos"
  - subject: "Re: Funny"
    date: 2001-09-21
    body: "Yes, the integration between the XP programs themself and also the system is worth advertising.\nThe To: field thing is more than just advertising.\nI have heard, that typing something which looks like an e-mail adress in word or excel leads  to automatical take - over of this adress into the adress book\nof outlook etc. ( this could also be annoying ).\n\nKDE3 will hopefully bring a unified adress/contacts/meeting/mail/? database, with an interface accessible from all applications."
    author: "ac"
  - subject: "Microsoft Office"
    date: 2001-09-21
    body: "\"\"Naturally, the primary downfall of deploying KDE is a lack of mature, enterprise-level Linux productivity apps, especially those that can properly import documents in MS Office format.\"\"\n\nWhen did Microsoft Office become a subset of 'productivity'?\n\nWhenever I use Windows these days, I notice the lack of *my* productivity applications, including KDevelop, LyX and Gnuplot.  (KOffice isn't one of them -- why has KWord followed the Microsoft office practise and made equations a seperate object, with all the horrible memory requirements that implies?  Why can't I find a simple replacement for XFig with a modern graphical interface?)\n\nIt seems that \"\"enterprise-level\"\" means 'does very little, but costs enough to make the middle managers happy'. That being the case, I will quite happily provide the source code to KOffice for the one time, never to be repeated, special offer site license price of \u00a3100,000."
    author: "Jon"
  - subject: "Re: Microsoft Office"
    date: 2001-09-21
    body: "\"Why can't I find a simple replacement for XFig with a modern graphical interface?\"\n\nWhy don't you try VeKtor 0.7.3\n\nIt is the fancy vector drawing app for KDE. It runs smooth and replaces XFig in all functions."
    author: "Dr No"
  - subject: "Re: Microsoft Office"
    date: 2001-09-21
    body: "What is this \"Vektor\" application? I couldn't find any info for it. Is it a KDE2 application?\n\nThanks,\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Microsoft Office"
    date: 2001-09-21
    body: "Hmm, I can't find it either.\n\nPerhaps they meant Kontour, the vector drawing program that's part of KOffice?  I think VeKtor was one of the suggested names for it when it had to change from KIllustrator.\n\nAnyway, they're certainly right about it being good - it's a pretty damn good vector illustration package, and it suits me quite well.  If you haven't tried it yet, see what you think of it.  If you've ever used Corel Draw, Adobe Illustrator or Macromedia Freehand, then you'll have some idea of what it's like.  It's not yet as feature-rich as the big 3 commercial drawing programs, but it's getting there.  The user interface could do with a bit of polish too, but it's far far better than Xfig.\n\nActually, it's my favourite KOffice component :)"
    author: "Amazed of London"
  - subject: "Re: Microsoft Office"
    date: 2001-09-22
    body: "Ahh, that's what I figured. I do use it, yes. However, it doesn't seem to exploit some of the features available before it merged with Karbon (see http://kt.zork.net/kde/kde20010615_14.html). Most of the bezier and spline editing functionality doesn't seem to be exposed. Otherwise, it seems to hold alot of promise. In a radio interview, I heard Shawn Gorden say that Kivio is slowly becoming a drawing program, too. Mmm... python scripting...\n\nCheers!\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Microsoft Office"
    date: 2003-03-09
    body: "\"Anyway, they're certainly right about it being good - it's a pretty damn good vector illustration package, and it suits me quite well. If you haven't tried it yet, see what you think of it. If you've ever used Corel Draw, Adobe Illustrator or Macromedia Freehand, then you'll have some idea of what it's like. It's not yet as feature-rich as the big 3 commercial drawing programs, but it's getting there. The user interface could do with a bit of polish too, but it's far far better than Xfig.\"\n\nIt's good as far as it goes, and XFig indeed has a terminally funky interface, but XFig also has essential features that Kontour lacks, as far as I can see from a quick try-out.  The first thing that comes to mind is the ability to copy properties between objects, so you can say 'take the line properties from this object and apply them to this selected group of objects'.  It's quite painful to work without that.  \n\nI have no doubt that many nice features are on the way, however, I worry that some of the really good features of XFig are going to be missed because the funky interface keeps developers from really using it and finding out what's good about it.  There are reasons Xfig has survived and developed so long."
    author: "Daniel Phillips"
  - subject: "Re: Microsoft Office"
    date: 2001-09-21
    body: "\"KWord followed the Microsoft office practise and made equations a seperate object, with all the horrible memory requirements that implies?\"\n\nso perhaps other applications besides just the word processor can include complex equations? as for the amount of memory required by not having it directly a part of kword, i highly doubt it is bad as you seem to think it must be. =) also, another way of looking at it is that most people never use equations in their documents, so having that functionality directly in kword would merely be bloat for most of its users."
    author: "Aaron J. Seigo"
  - subject: "Equations!"
    date: 2001-09-22
    body: "Let's not forget our roots. Unix was and is an operating system aimed to satisfy the needs of scientists and techies. Belonging to both of these groups (MSc applied physics student and part time computer geek), equation editing ability is one of the very most important features to me. I've tried KWord, but it's lack of mathematical tools makes it really unusable to me. I guess I'll stick with the in some ways horrible LyX until there is a new KLyx. :(\n\nIn my opinion, the way to go is to work very close to the LyX team and produce a new KLyX that can do all the good things from both worlds: Layout engine and equation editor from LyX, add KParts to make it a real member of the KOffice family. If you can just convert KPart objects to postscript, this should be absolutely possible to do without changing anything in LaTeX.\n\nAlso, I would be very glad to see a scientific plotting program like xmgr in the KOffice suite!\n\nMany people now code their documents by hand in LaTeX. With the backing of the well known KDE project, such a \"scientific publishing environment\" could be a real killer app! (many of the people I've talked with don't know that LyX exists, but I'd bet that they know that KDE exists!)"
    author: "Johan"
  - subject: "Re: Equations!"
    date: 2001-09-22
    body: "You might have a look at gnu texmacs ( http://www.texmacs.org/ )"
    author: "Anonymous"
  - subject: "Re: Equations!"
    date: 2001-09-23
    body: "Wow!  I just downloaded this program and I've got to say that it seems pretty darn impressive!  (A little slow, but it seems to be doing a lot of rendering work all of the time, so that can be forgiven..)\n\nThanks for pointing this out, I'd somehow never heard of it before...\n\nBen"
    author: "Ben"
  - subject: "Processor: Intel or Alpha"
    date: 2001-09-24
    body: "When it's only runing on Intel or Alpha, do I have to delete KDE 2.2.1 on my PowerBook?\nOk, Noatun dosen't work and some info-modules are crashing, but everything else works very good."
    author: "Manfred Tremmel"
---
ZDNet reviews KDE 2.2.1, saying it provides a seamless migration from Microsoft platforms for business users and a comprehensive set of productivity applications, internet software and user management programs. They also mention, that "since KDE is merely running on top of the X Window System, you can perform remote administration of any KDE-enabled system by redirecting application output to another X server on the network."

<a href="http://www.zdnet.com/techupdate/stories/main/0,14179,2813695,00.html">Read the full review here</a>.