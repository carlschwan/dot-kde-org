---
title: "Konqueror and Other Version 5 Browsers"
date:    2001-09-24
authors:
  - "Dre"
slug:    konqueror-and-other-version-5-browsers
comments:
  - subject: "Konqueror truly has the best CSS support"
    date: 2001-09-24
    body: "Look at http://www.w3.org/Style/CSS/ That page is 100% html and css compliant and so far konqueror is the only browser I can find that actually properly renders that page. Mozilla 0.9.4, opera 5, netscape 4.77, ie 6, etc all fail. Mozilla gets it closest behind konqueror but I think I could read war and peace in the time it takes to scroll the page. Opera scrolls quickly and has the floating menu but it doesn't look even close to correct. IE doesn't have the menu float and has lots of other rendering issues. Netscape 4 .... well let's just not discuss that one.\n\nFrom doing a lot of web development and extensively using CSS I have found nothing that is even close to konqueror in terms of CSS support. Too many times I find browsers that just can't get stuff right. I would rate Mozilla and Opera and pretty good CSS support. Not excellent but still pretty good and mozilla  has been showing a lot of improvements. I don't doubt it will be excellent in another 6 months or so. IE6 I find to be pretty poor in CSS support.\n\nOverall I find konqueror to be my favorite browser to use and love using it for website devel. I can make stuff 100% spec and verify that it works well. Then I just make the changes needed for each of the various browsers needed to make them vew it properly also and stick it in a browser specific version server side and register them with the right browsers. That way all the browsers get the right page and it is far less painful to develop the stuff."
    author: "kosh"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2001-09-24
    body: "I can't see the floating menu at all (KDE 2.2.1 RHJ 7.1 Ben's RPMs) - Mozilla and Opera bot show it correctly (Opera is pretty fast in scrolling, too). Besides, the layout in Konq. is probably a bit incorrect as well ('CSS Browsers' and 'Authoring Tools' are on the same line - should be slightly offset IMO).\n\n- Biswa."
    author: "Biswapesh"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2001-09-24
    body: "I have been looking at it with KDE 2.2.1 and I see a nice transparent blue menu that floats and scrolls quickly down the page. It is fully transparent through the entire page. With Opera here it is just completely transparent while it should be this blueish alpha blended menu. Opera does scroll quickly but it doesn't look very good. \n\nI am not sure what the alignment of those two items should be. I was looking at the CSS for the page and it looks fairly complex. I couldn't say exactly where those two items should be. On my system they don't look like they are on the same line but they are fairly close.\n\nIf I flick the scroll wheel down on Mozilla 0.9.4 to scroll the page on my k6-2 500 it will take about 10 minuts to reach the bottom and that is not an exaggeration. I have no idea why that page is so slow on mozilla on this box. Every other page I have hit in mozilla seems to be pretty fast. 768M of ram, 2.4.10 kernel and a 64MB DDR Radeon card. \n\nThis is all a debian sid system using the kde packages that are in sid."
    author: "kosh"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2001-09-24
    body: "> can't see the floating menu at all (KDE 2.2.1 RHJ 7.1 Ben's RPMs) \n> ('CSS Browsers' and 'Authoring Tools' are on the same \n> line - should be slightly offset IMO).\n\nNo such problems here (KDE 2.2 on SuSE 7.2).\n\nGreetings, \nTackat"
    author: "Tackat"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2001-09-25
    body: "> I can't see the floating menu at all (KDE 2.2.1 RHJ 7.1 Ben's RPMs) - \n> Mozilla and Opera bot show it correctly (Opera is pretty fast in scrolling, > too). \n\nNo problem with KDE 2.2.1 (kdelibs-2.2-22mdk) on Linux-Mandrake 8.0.\nMenu is present, transparent! :-)\n\n> Besides, the layout in Konq. is probably a bit incorrect as well ('CSS \n> Browsers' and 'Authoring Tools' are on the same line - should be slightly \n> offset IMO).\n\nTry to enlarge browser's window to full-screen.\nI am on 1024x768, and still it's not enough (small overlap of 'CSS Browsers' and 'Authoring Tools' text)... Probably W3C designed this page for 1280x1024 resolution.\n\nAs about CSS support in Konqueror - pls check\nhttp://www.konqueror.org/content/khtml_css2.html\nIt has a lot of very useful info.\n\nCheers,\n\nVadim"
    author: "Vadim Plessky"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2001-09-26
    body: "On my KDE2.2.1 the menu is there, semi-transparent. Mozilla is unbearably slow on this page (as kosh wrote, it really takes minutes to scroll down the page). And CSS Browsers and Authoring tools are offset by about 1/4 of letter height.\n\nHey, I just love konqueror. :-))))))"
    author: "Lada"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2001-09-24
    body: "Yes, Konqueror has great basic and semi-advanced CSS support. Using Konqueror as my primary test environment, I've developed a few web pages with heavy CSS formatting. I've never had to do much tweaking to make it look nice in other browsers.\n\nHowever, Konqueror fails on the really advanced CSS aspects. Check out the CSS tests at http://www.bath.ac.uk/~py8ieh/internet/home.html (especially the 'Evil test suite')."
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Overall Mozilla is probably better"
    date: 2001-09-24
    body: "Admittedly, Mozilla sucks at the W3C site (hopefully they fix this soon). However, overall CSS support is still probably better in Mozilla than Konqueror.\n\nFor example, check this cool CSS demo site:\n\nhttp://www.meyerweb.com/eric/css/edge/complexspiral/demo.html\n\nThere are some cosmetic problems with Konqueror, such as with the menu on the left. (Also note that IE can't handle the page correctly either.)"
    author: "Joni K."
  - subject: "Re: Overall Mozilla is probably better"
    date: 2001-09-24
    body: "This page looks fine to me using the Konqueror in the HEAD branch. What problems are you refering too?\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Overall Mozilla is probably better"
    date: 2001-09-25
    body: "The only cosmetic issue with this page under KDE 2.2.1 is that the mouse-overs for the menu on the left don't work 100% correctly - sometimes there are horizontal bands of the previous colour left there.\n\nI haven't got KDE HEAD up and running on my machine yet, so I can't confirm whether it's gone away or not."
    author: "Jon"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2001-09-25
    body: "Can Konqueror style form elements now? I'm using 2.1.1 now, and all the widgets shows up as they are specified in KDE theme, not as in CSS rules. IMO it's a long way to claim CSS compliance if buttons and text boxes cannot be styled using CSS. Along with that, KHTML 2.1.1 has tons of rendering bugs concerning CSS. Has situation been improved?"
    author: "Danny"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2001-09-25
    body: "Color etc information are suggestions to the browser and not a requirement. It is within spec to ignore that stuff. Actually I would like it to be configurable. Personally I hate websites that try and change the look and color of buttons, textareas etc since I have kde setup to look like how I want. So what we need is a checkbox in kde that is something along the lines of allow all site/ sites in this list/ no sites to override the look and feel of the standard controls.\n\nEvery point release of kde since 1.0 has offered major improvements each time in html/css rendering capabilities. 2.2.1 is far superior to 2.1.1 and fixed lots of things."
    author: "kosh"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2001-09-25
    body: "Yes, those \"color etc information\" are suggestions, and browsers are under no obligation to render as specified. But ignoring them does not lead to the claim of CSS compliance.\n\nCertainly some people pefer the form widgets to be consistent with their desktop, but how is a web author to know how the user desktops are set up (or can the user change their desktop settings at all)? Normally the web author would write different style sheets, one for regular viewing, and the other for visually-impaired (high-contrast, large font). Without the ability to style form widgets, the high-contrast style sheet may not be very useful. (side question: does Konqueror 2.2 allow users to select alternate style sheet, like in Mozilla?)\n\nI've seen a lot of improvements in KHTML since the original 2.0 release, but for advanced features like CSS 2, DOM 2, etc., it still seems very lacking compared to Mozilla and IE. Nevertheless, I'd say Konqueror surpasses Opera and certainly Netscape 4, so congratulations to the KDE team."
    author: "Danny"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2001-09-25
    body: ">side question: does Konqueror 2.2 allow users to select alternate style sheet, like in Mozilla?\n\nYes.  It has a nice wizard that makes you a custom stylesheet."
    author: "not me"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2001-09-28
    body: "You're refering to user-defined style sheet which applies to every web the user visits. This is different from \"alternate stylesheet\" which specify an alternative representation of a web page.\n\nIt's specified in the HTML by:\n  <link rel=\"alternate stylesheet\" href=\"foo.css\" title=\"foo\">\nUnder Mozilla, you can select such style sheet by View->Use Stylesheet->foo. I can't find anyway to do this in other major browsers."
    author: "Danny"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2001-09-25
    body: "Konqueror has been able to style form elements for some time, but only if you are using a KDE widget style that adapts to specified colours - the default highcolor style does for instance, as do all the Qt styles, and Mosfet's Liquid style.\n\nIf you're using a pixmap-based theme (maybe you're using the Acqua or Acqua-Graphite styles from themes.org, or the Marble or System styles, or one of Vadim Plessky's styles), then KStyle cannot reliably change the colours of widgets, as they are all pre-drawn by a human rather than created by code that can adapt to different colours.\n\nSwitch back to the default highcolour style and you will see Konqueror does indeed style form elements."
    author: "Amazed of London"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2004-12-06
    body: "Yeah, sure, here is Konqueror reading a simple xml file:\n\nConnection HealthClient connection information.1Connection Usage1Connection Usage01001000[Threads_connected] [max_connections]Usage10001Traffic2Traffic01201024001^[Bytes_sent] 102400Usage10001Number of SQL Queries3Number of SQL Queries0110101^[Com_select] 1010001Memory HealthGives an overview of critical memory health issues.2Query  Cache Hitrate1Query Cache Hitrate01001000 (^[Qcache_hits]/(^[Qcache_hits]+^[QCache_inserts] +^[QCache_not_cached]))*100100Hitrate10001Key  Efficiency2Key buffer size02301000[Key_blocks_used]*[key_cache_block_size] [key_buffer_size]Key buffer usageKey buffer10001Key buffer  usage01001000100-(^[Key_reads]/^[Key_read_requests])*100100Hitrate10002\n\nWhy? No default on style sheet, so garbage comes out.  The page is mysqladmin_health.xml from MySQL, which also seems to have this \"simple answers\" problem spreading throughout the Internet and programming world.\n\nKDE and Konqueror are suffering from the same synaptic collapse; it won't work and you can't fix it with a tool that requires it to be working in the first place!\n\nKong could use a searchable help file, and content.  MySQL could use some cyanide to slow it down a little in its overdevelopment.  If you can't start at the beginning, \"Why doesn't xml render in Konqueror?\" then there is no sense in going into a long lecture of how great it is.\n\nI think both programs are great, but they could both use some professional manual writers and editors, not programmers, real manual writers.\n\nNot even css has the best css support; there is no css support, period.\n\nEverything is on \"runaway\" development.  Would anyone care to tell how to make Konqueror render an xml file?  Without it being embedded in an html file!  What's the point of having files inside of files inside of files in an object oriented system?  If Konqueror can't find a style sheet, it should ask what to do and suggest a style sheet with proper instructions on how to get the job done.\n\nAnd this is exactly what's lacking in all the patting themselves on the back development that is going too fast.  Too fast causes crashes; witness the most popular OS and its millions, literally, of problems.\n\nSend me an email.\n\n\n"
    author: "CyberSongs"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2004-12-06
    body: "How on earth would you render \"an xml file\"? XML is meta-container format and specifies nothing about rendering or even format. \nKonqueror doesn't have problems with XHTML though."
    author: "Carewolf"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2004-12-08
    body: "Render it by processing it.  Either as PHP, which is what most xml's are currently, or perl, perhaps.  All basic php's are seen as xml in supplied mysql program provided scripts.  They cannot be displayed without being converted to html of some sort.  The files in phpMyAdmin, for example, begin with \"<?php\" and that should be a flag for at least two alternatives, one if mysql and/or php is installed to render by either, or two, if neither is installed and/or have problems, to alternatively display the code with a message that php/mysql did not handle the page.\n\nBut if the browser thinks that \"<?php\" file type xml, then it should do a better job and not ask for a plugin, or worse, display a blank page, then the browser is wrong in its definitions.\n\nPhrases like \"meta container format\" are long grammatical statements equivalent to \"it's an eXtended Markup Language file.\"  Regardless, it should not result in a blank page.\n\nFor example, having a simple file called packets01.xml, Konqueror will ask for a plugin while IE will display the page in colored code.  This file was created by Ethereal.  It can be exported as various file types.  The idea is to make the file viewable in a tabular format such as php, but the Ethereal authors used type .xml\n\nThere are programs that can open such files, but Konqueror will ask for them and selection of the wrong file will produce useless rendering.\n\nSaved as a PostScript document, the file can be read using GhostView, but it would be preferable to see it in tabular form using php.\n\nI believe you thought I had made a simple statement.  There was all of this and more in that statement.  Nothing comes down to such a simple aside as it's this kind of file or that kind of file that can't be rendered.  All files can be rendered, it doesn't matter what kind they are, and there are choices in how to have them rendered.  Rendering means making it human friendly viewable, nothing more.  Hard definitions of words like \"render\" do not exist in programming as all definitions are variable in programming.\n\nSuccesful development is always based on what the end user wants, not what the developer thinks he should give them.\n\nI'm currently debugging the MySQL source code line 1830, etc., for socket failures inherent in MySQL.  Care to comment?\n\nCiao\n"
    author: "CyberSongs"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2004-12-08
    body: "If all you want is to see the tag soup, open it in kate.\n\nIt will highlight all the brackets.\n\nHow that is supposed to be useful, I have no idea.\n\nXML contains nop presentation info, unless it's a known XML variety.\n\nFor example: \n\n* fwbuilder saves firewall configs as XML.\n* kword saves documents as XML\n* I save arbitrary python object trees as XML\n\nMost of those can't be shown \"in tabular form\". And all are XML.\n\nIn short: I think you are very confused.\n\nAnd about the MySql stuff.. are you somehow mislead into believing this is a MySql support forum?\n"
    author: "Roberto Alsina"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2004-12-08
    body: "Where does this attitude come from?  Why is it that whenever anyone posts a comment, there's always someone out there waiting to pounce on them?  Roberto, I really don't think you're old enough to decide whether or not someone is confused.  Pretty sure of that.\n\nWhy personal?  Why can't you simply discuss the subject?\n\nI'll bet I was working in Unix before you went to school.\n\nDon't flame me; I have no time for such nonsense.\n\nBy the way, I was creating style sheets when html came out.\n\nI had suggested a windows type interface for unix and linux probably before you knew what the Internet was.\n\nTherefore, since I was associated with KDE's beginnings, back when Dragonlord was in Dragonsrealm on dal.net, I think I know a little about kde's development.\n\nWhere were you when I was working on some original GeOS windows, and before that on graphics for the pdp11 and 8088?\n\nWhere were you when I was authoring \"relocate\" in machine and assembly code?\n\nAnd MySQL is increasingly becoming part and parcel of an html based database; where were you when I wrote papers about the proposed html database structure, or suggested it to Tim Berners-Lee?\n\nAnd for that matter, what are the cloud chamber photographs of pi-mesons and other subatomic particles?\n\nWhere were you when I was working with and studying particle physics?\n\nDon't \"aside\" me again with such childish flaming techniques.\n"
    author: "CyberSongs"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2004-12-08
    body: "He is not trying to flame you, but just explain to you that you are seriously confused. For instance PHP is a server-side script, that cannot be processed by a browser. If a webserver delivers PHP or MySQL to the browser it has been misconfigured.\n\nThe only issue where you might be right is the XML. I think we put in code that treats all code delivered as XML as XHTML which will mean an empty page if it is not. We could propably do some more fine-grained detection here."
    author: "Carewolf"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2004-12-08
    body: "Well, if what you say makes no sense, you are probably confused, and age has nothing to do with it.\n\nI seriously doubt you were hacking UNIX before I went to school, unless you worked on AT&T labs, since I went to school in 1974.\n\nAs for where I was when you were writing relocate in assembly, well, I was probably old enough to know that writing it in C was a better idea (although I have no idea what relocate is suppsed to be).\n\nDude, I *have* programmed Fortran on a PDP-11. I have spent two years studying differentiation theory, and all that has nohing whatsoever to do with this. You can be a hotshot physicist, and still have no idea what XML is. XML doesn\u00b4t imply presentation, unless you know what variant it is, and that\u00b4s it. End of argument.\n\nOn the other hand, if you believe that your personal qualifications (or mine) have anything to do with the argument, you are, let\u00b4s say, stupid."
    author: "Roberto Alsina"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2004-12-20
    body: "I'm as stupid as Albert Einstein.\n\nRelocate is the machine code that everyone now uses to make all modules dynamic, that is, they are loaded to a relative address, the relocate routine adjusts the base of the execution stack for the code to the new relative address.\n\nAll \"jumps\" and any uncondtional goto or branch is replaced by a conditional relative branch.\n\nXML is problematic.  KDE and X are problematic.  Konqueror is problematic.  They all seem to be reflective of a Microsoft gui style of programming.  They don't know how to manage either the screen refresh or memory.\n\nMaybe I was stupid in asking a question about XML.  I didn't think so.  Apparently, however, it left an open door for someone to call me stupid.  Being able to see through transparent people, I knew instinctively that I'd be flamed for asking any question in public.  However, I was more brave than stupid, so I asked.  I felt like taking the position of the average person that comes along and asks a simple question.  How many of them are made to feel bad by so-called IT professionals by being monikered with \"stupid?\"  It is just not right to call someone who asks a question \"stupid.\"\n\nSorry, it is just not right.\n\nIf you wouldn't do it to a 5 year old, don't do it to an adult.  And if you do that to a 5 year old, you need professional help.  In fact, I think if you do it to an adult you need professional help.\n\nI find it hard to believe that a question about XML's validity would raise hecklers from the dead.  I don't much care about XML.  We all use it, but it's not all that life-sustaining to defend it to the death.  Sometimes it works, and sometimes it doesn't.  CSS and style sheets are going to come and go; to be replaced, eventually, by something not so dependant upon dependancies.  And Konqueror will only improve; as will KDE and Linux.  None of them are important enough to offend other people by calling their questions and opinions \"stupid.\"  That's just not right, and, it's mean.\n\nFor KDE, Konqueror, and the rest of the so-called super-gui's:\n\nI wrote another routine, years ago, that uses one execution clock to change a screen.  I recently suggested KDE look into how it's done.  I had an old Commodore refreshing the screens faster than any current OS.\n\nIf people would pay attention, they might learn something.\n\nYou don't transfer the 3 meg or so of bits to the monitor [televsion, regardless of its current name], you change the pointer to the display area.  Doing that, you can change the screen, call it refresh if you like, in one microprocessor execution clock.\n\nGiven a sufficient area in memory, you can have as many screens as you like available instantly.  Currently, it's my bet that all of these gui programs stream the new screen to the display area; that is inefficient in a world of pointers, object oriented code, and Vector Operators, which are all really Indirect Reference Pointers and calls for arrays, binary tables, etc..\n\nIf the L1 or L2 cache has the necessary depth, of course, if not, it has to come from external memory or disk memory.  Even with the external RAM onboard though, it is much faster than gooeying the refresh from a stream.  If the display chipset, on the other hand, had known this technique, there would be no hashing for local memory or disk memory.\n\nThe X, KDE, and Konqueror I'm using right now are eating up to 30% cpu time doing absolutely nothing.  And because it shows up as a delay between screens, as in using ALT+TAB, like Windows does, I know it is the display code mechanisms.  In fact, it looks so much like a Microsoft programmer wrote it, I begin to wonder.\n\nI just happened to have noticed that newer programs are slowing down in performance.  I've seen this many times before on mainframes, years ago.\n\nAnd if I said I was not more than ten feet from J.Presper Eckert when Unix was intially discussed, I meant it, even if said in passing.  Unix and Aix were oldies, but goodies.  Both ended in the Roman Numeral IX because of the 9 companies involved; not just Bell Labs!\n\nBell Labs takes far too much credit, especially when the people I worked with were supplying them and AT&T with the machines and the network on which to test UNI6 and Unix.\n\nBut then again, let's not forget that when Alexander Graham Bell's future father-in-law was going to patent the telephone on the morrow, Bell and his fiancee beat daddy to it by going to the patent office with daddy's drawings on that day!\n\nI suppose you weren't aware that Alex Bell had stolen the plans and design of the telephone from his future father-in-law.\n\nIt's in the history books, if you're looking for a reference.\n\nSometimes some people do know a little more and at least, as Solomon once said, you would be wiser to listen first.\n\nKDE and Konqueror are great, and I like them, but they definitely still need work.\n\nIf they implement the pointer screen change as stated here, no gui or browser will be faster.  I'm surprised the video chipset manufacturers haven't standardized this yet.  Perhaps they should.  It would give them something to work on with high profit potential.\n\nOh, btw, I and my colleague back in the year you stated at Bell Labs, built a radio and fried a resistor.  The interesting thing was that it turned into a diode.  So, we figured, that's how Wang discovered it too.  By accident, the first solid state diode, and hence junction, is discovered, and somebody gets the Nobel Prize for it.  Then goes on to burn two resistors and put them opposite polarity end-to-end and discover, further, that they can control the current throught the thing at the junction.  Hence, the first transistor.  This is meant to be funny.  I remember laughing quite good when we did it and saying \"So that's how he discovered solid state electronics!\"\n\nLastly, most things at Bell Labs do happen by accident, as in \n\"Watson, come here, I need you\"  Alexander Graham Bell while producing the first working telephone, er, by accident.\n\nEverything is not so high-fallutin as the world would make us believe.  The greatest inventions never came from those with 4.0 averages and all the honors and accolades; they came from average men.  Einstein's GPA was 2.0 and he failed Algebra.  He was turned down for admission to the University of Berlin, and went instead to the Unviversity of Switzerland at Zurich.  Thank God he was a dummy!  Otherwise, Adolf Hitler would have had the atomic bomb first and we'd all be saying \"Sieg Heil!\"  If Albert Einstein were here today, and 18 years old, applying for college in the United States, he would not get in; at least not to one of the hotshot or bigshot universities.  GPA and cum are asses.  Consider just how many true geniuses our schools have turned away with their current policies of a stupid test based solely on memory, the GPA and cum, and no test for real intelligence exists.\n\nThus, the favorite saying of people who can memorize is \"you're stupid.\"\n\nAnd my reply is, \"Yes, but at least I can think.\"\n\nAnd they just don't get it.\n\n\n:)\n\n\n\n"
    author: "CyberSongs"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2004-12-20
    body: "Not everyone with a 2.0 GPA is Albert Einstein."
    author: "Roberto Alsina"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2008-12-01
    body: "The point is that not ever 2.0 is stupid.  And that higher requirements don't necessarily equal better/smarter people. "
    author: "KT"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2004-12-20
    body: "You write too much."
    author: "ac"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2004-12-08
    body: "You are talking about server side processing stuff. KHTML/Konqueror, like any other internet browser, is only responsible for rendering client side stuff so what you are asked for can at most be found in web managing software like Quanta, but not in a customer's browsing client."
    author: "Datschge"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2005-04-02
    body: "Visit this page in Firefox, and then in Konqueror.  The only difference is that Firefox correctly applies the CSS stylesheet to the XML, producing a page formatted for viewing, while Konqueror does not.  At least, not with version 3.3.2.\n\n    http://www.ser1.net/Files/Reviews/Sager4750.xml\n\n--- SER"
    author: "Sean Russell"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2005-04-02
    body: "Odd. Try pressing back and then forward, and please open a bug-report at bugs.kde.org"
    author: "Carewolf"
  - subject: "Already fixed"
    date: 2001-09-27
    body: "They've already fixed the W3C page bug in Mozilla.\nCheck this build (for Linux):\n\nftp://ftp.mozilla.org/pub/mozilla/nightly/2001-09-27-06-trunk/"
    author: "scb"
  - subject: "Re: Already fixed"
    date: 2001-09-28
    body: "Not only it is fixed, but now it is easier to see (it was too slow to notice before) that Konqy has some rendering problems with regard to the CSS...\n\nMozilla is not perfect either (it has some problems with the outer box in the \"Learning CSS\" part), but it renders the rounded edges beautifully (Konqy fails at that, and so does Opera).\n\nAlso, the page has a big collection of alternate style sheets (that can be accessed via View -> Use Stylesheet in Mozilla). I haven't seen a way to choose them in Konqueror. Am I missing something?"
    author: "Marcelo"
  - subject: "I don't agree"
    date: 2001-10-10
    body: "try this:\nwww.uniserv.krakow.pl\nthere is problem with centering email logo, and some texts in left menu frame. And most visible: Positioning background image: it should be bottom if page, but isn't.\nYou can view this in Explorer, Netscape and Mozilla, everything ok."
    author: "Majki"
  - subject: "Re: I don't agree"
    date: 2001-10-10
    body: "Sorry but I just validated the page and it failed. As such you are expecting the browser to work on a broken page. Sorry get the page fixed and 100% compliant then check it. Also I can at least thank a few different deities that frames are dead. xhtml 1.0 is the last version to support frames :)\n\nI do professional web devel and I know the problems using non standard stuff causes. Even one error on the page changes the way a browser will render the page. That page is riddled with errors."
    author: "kosh"
  - subject: "Re: Konqueror truly has the best CSS support"
    date: 2004-12-08
    body: "I don't want to go on about this too much, but here is the start of the MySQL code that is a problem for every Unix browser and OS:\n\n01828 \n01829 MYSQL * STDCALL\n01830 mysql_real_connect(MYSQL *mysql,const char *host, const char *user,\n01831                    const char *passwd, const char *db,\n01832                    uint port, const char *unix_socket,ulong client_flag)\n01833 {\n01834   char          buff[NAME_LEN+USERNAME_LENGTH+100],charset_name_buff[16];\n01835   char          *end,*host_info,*charset_name;\n01836   char          password_hash[SCRAMBLE41_LENGTH]; /* tmp storage stage1 hash */\n01837   my_socket     sock;\n01838   uint32        ip_addr;\n01839   struct        sockaddr_in sock_addr;\n01840   ulong         pkt_length;\n01841   NET           *net= &mysql->net;\n01842 #ifdef __WIN__\n01843   HANDLE        hPipe=INVALID_HANDLE_VALUE;\n01844 #endif\n01845 #ifdef HAVE_SYS_UN_H\n01846   struct        sockaddr_un UNIXaddr;\n01847 #endif\n01848   init_sigpipe_variables\n01849   DBUG_ENTER(\"mysql_real_connect\");\n01850   LINT_INIT(host_info);\n01851 \n01852   DBUG_PRINT(\"enter\",(\"host: %s  db: %s  user: %s\",\n01853                       host ? host : \"(Null)\",\n01854                       db ? db : \"(Null)\",\n01855                       user ? user : \"(Null)\"));\n01856 \n\nThat is directly from the MySQL source.  Going further in the code, when compiled it will result in can't connect to mysql socket.  Find the source and tell me what you think is wrong with this code.\n\nPosted just to back up the point about debugging MySQL source code.\n\ncss sort of becomes useless if you can't use it.\n"
    author: "CyberSongs"
  - subject: "scrolling problem of Mozilla: Mozilla too good?"
    date: 2001-09-24
    body: "The scrolling problem of Mozilla comes from the fact that the floating menu is transparent. Knowing Konqui's failure with PNG alpha channel, I think it does not render the floating menu as transparent, so he does not encounters the transparency problem. Without hardware acceleration, belnding a floating transparent menu is a CPU hog.\n\nNot so long ago, this CSS page had a reddish opaque floating menu (instead of the blueish transparent it has now). Mozilla was fast to render it, as it is for every page on the web. The slowlyness of Mozilla and other Gecko based browsers is just due to this transparency issue IMHO. Any browser that does not implement this feature will of course avoid the problem. But it cannot be said \"better\".\n\nI can't test this page with Konqui, as my linux machine is not connected to the net, but I would be really surprised if the floating menu was rendered transparent in Konqui."
    author: "oliv"
  - subject: "Re: scrolling problem of Mozilla: Mozilla too good?"
    date: 2001-09-24
    body: "It is transparent in Konqueror."
    author: "Uwe Thiem"
  - subject: "Re: scrolling problem of Mozilla: Mozilla too good?"
    date: 2001-09-24
    body: "Nice to know this.\nI willl dowload the CSS and try it locally.\nGecko is quite 'old' now, but this problem with transparency is the only one I've seen so far. NS 4 and IE don't have the problem, as they don't display the floating menu, and I don't understand seeing some pages (as the one in the news) where IE is classified as excellent, when only the Mac version is trully excellent. My comment was not Mozilla/vs Konqui, but more Mozilla/ vs others (esp. the reference IE 5 and 6)."
    author: "oliv"
  - subject: "Re: scrolling problem of Mozilla: Mozilla too good?"
    date: 2001-09-24
    body: "It is transparent, yes, but because the background png doesn't use\nthe alpha channel transparency. It just uses the \"make-one-color-\ncompletely-transparent\"-thing. That's not real transparency. \n\nBy the way, does anybody know how to convince Konqueror to do some document.write()? It simply doesn't do anything when I use this method from my Javascript code."
    author: "Benno Dielmann"
  - subject: "document.write, JavaScript, etc."
    date: 2001-09-25
    body: "> By the way, does anybody know how to convince Konqueror to do some \n> document.write()? It simply doesn't do anything when I use this method from \n> my Javascript code.\n\nYou should probably ask me about this. :-)\nSee script attached, let me know if you have more questions.\n\nVadim"
    author: "Vadim Plessky"
  - subject: "Re: document.write, JavaScript, etc."
    date: 2002-06-10
    body: "The problem seems to be that document.write doesnt work when its inside a function, any suggestions?"
    author: "Juuso Ketonen"
  - subject: "Re: document.write, JavaScript, etc."
    date: 2002-10-02
    body: "use 'blablabla' instead of \"blablabla\" that worked for me"
    author: "Tyler"
  - subject: "Re: scrolling problem of Mozilla: Mozilla too good?"
    date: 2001-10-02
    body: "Try a nightly build of Mozilla, or - better still - compile one yourself from CVS. The difference between 0.9.4 and the current builds is enormous in speed on this page: with a current build I can zip through the page, transparent menu and all, many times a second using the scrollbar.\n\nMozilla is much better in rendering this page than Konqueror (at least here, KDE 2.2.1 vs. Mozilla CVS build from a couple of days ago), as Mozilla does the transparency thing right, shows the rounded corners for the blocks, is *faster* (yes, faster, and a lot faster... That's Mozilla, mind you, I'm not even talking about Galeon of Kmeleon...), etc. Konqueror is surely getting better, but it is not on par with Gecko/Mozilla. IMnsHO..."
    author: "Frank"
  - subject: "Re: scrolling problem of Mozilla: Mozilla too good?"
    date: 2001-10-02
    body: "Try a nightly build of Mozilla, or - better still - compile one yourself from CVS. The difference between 0.9.4 and the current builds is enormous in speed on this page: with a current build I can zip through the page, transparent menu and all, many times a second using the scrollbar.\n\nMozilla is much better in rendering this page than Konqueror (at least here, KDE 2.2.1 vs. Mozilla CVS build from a couple of days ago), as Mozilla does the transparency thing right, shows the rounded corners for the blocks, is *faster* (yes, faster, and a lot faster... That's Mozilla, mind you, I'm not even talking about Galeon of Kmeleon...), etc. Konqueror is surely getting better, but it is not on par with Gecko/Mozilla. IMnsHO..."
    author: "Frank"
  - subject: "Re: scrolling problem of Mozilla: Mozilla too good?"
    date: 2001-09-24
    body: "well... it is transparent in konq, and it IS very fast to scroll that page.. at least on my PII 300."
    author: "ealm"
  - subject: "Re: scrolling problem of Mozilla: Mozilla too good?"
    date: 2001-09-25
    body: "It works fast on my Cyrix M II 200Mhz processor"
    author: "Luke"
  - subject: "Re: scrolling problem of Mozilla: Mozilla too good"
    date: 2001-09-24
    body: "seems this issue has just been reported and discussed on bugzilla\nhttp://bugzilla.mozilla.org/show_bug.cgi?id=98252"
    author: "oliv"
  - subject: "Re: scrolling problem of Mozilla: Mozilla too good?"
    date: 2001-09-25
    body: "Wtf are you talking about? Konqueror _DOES_ have transparent PNG support. YOu just cannot use transparent pngs made by Photoshop 5.0 through 5.0.3 on Windows. They are buggy. GIMP and Moz have workarounds to these buggy PNGs, but Konqueror doesn't."
    author: "AS"
  - subject: "Re: scrolling problem of Mozilla: Mozilla too good"
    date: 2001-09-25
    body: "I'm talking about good PNG images, not buggy ones. I'm talking about the png test pages you can find on the w3c page and on libpng homepage. Most of the people I've asked on several places (as well as libpng site) do confirm that Konqueror (actually, it's more KHTML) does not properly handle PNG alpha channel.\n\nSomeone told me that it will be the case with QT 3, and when the render extension will be implemented in the video drivers (for linux and al.)."
    author: "oliv"
  - subject: "Re: scrolling problem of Mozilla: Mozilla too good"
    date: 2001-10-02
    body: "I am using konqueror HEAD (kde3), and it definately does the alpha channel on the png images on libpng.org."
    author: "tick"
  - subject: "webreview.com?"
    date: 2001-09-24
    body: "How come www.webreview.com still ignores Konqueror? It is not listed in their browser compatibility chart.\nPerhaps some more users should point them to the fact that Konqueror is actually pretty cool and wide-spread among Linux users"
    author: "Anonymous"
  - subject: "Re: webreview.com?"
    date: 2001-09-24
    body: "I love Konqueror, however i believe that as long that we don't have a windows (and maybe mac) port, it will not be popular nor stable... (it's stable but could use more work)"
    author: "A"
  - subject: "Re: webreview.com?"
    date: 2001-09-24
    body: "Right, so how is porting it to other platforms going to make it more stable?"
    author: "Anonymous"
  - subject: "Re: webreview.com?"
    date: 2001-09-24
    body: "porting often uncovers obscure bugs that only rear their heads under certain ugly situations."
    author: "anonymous code junkie"
  - subject: "Re: webreview.com?"
    date: 2001-09-26
    body: "If there will be more users there will be more developers & more feedback, and when there will be a big user base nobody will ibnore it."
    author: ""
  - subject: "Re: webreview.com?"
    date: 2001-09-25
    body: "Two statements from my side:\na) \n> I love Konqueror, however i believe that as long that we don't have a windows (and maybe mac) port.\n\nIndeed, Windows port for Konq (or Konq/Embedded) is nice to have.\nMy wife has to work with Windows in office, so adding option of running Konq would be very useful.\n\nb) Re: webreview.com\nIt's apity that webreview.com and many other so-called \"web-development\"-centric sites ignore Konq and some other new browsers (Galeon and KMeleon come to mind, as well as iCab)\nI personally tried to write to 2 web metrics agencies here, in Russia (http://top.list.ru and http://www.spylog.ru) in March-April 2001, but to no results.\nThey place Konq to \"Other browsers\" category, and sometimes add to \"Netscape\" in their reports :-((\n\nVadim"
    author: "Vadim Plessky"
  - subject: "Re: webreview.com?"
    date: 2003-05-19
    body: "There IS a mac port called Safari. Check out apple's site. I would like to see a windows port, though. (I only have access to windows boxes, and would like to try konqueror). Opera's great, but I want to try alternatives (Mozilla, on the other hand...)"
    author: "Anonymous Coward (What else?)"
  - subject: "www.hsbc.com.au"
    date: 2001-09-24
    body: "How about the website above? Who to blame? Konqueror or the website incompatibility with the standard? This has been a problem for me for a long time since I can't access that website's functionality because of the missing menus which are not loaded by Konqueror (compared with Mozilla and the difference will be noticeable). I hope this is not a bug in Konqueror and only the website to be blamed but if it's a bug, I can only hope this will be fixed soon as said in the posting by Dre above."
    author: "Alimin Bijosono Oei"
  - subject: "Re: www.hsbc.com.au"
    date: 2001-09-24
    body: "Hmm... nice to see that web-page has a good fallback if you're not running Javascript (it just doesn't bother displaying anything at all...).\nIn fact they seem to do everything with Javascript, which makes it very hard to find a decent page with content to try and validate...\n\nI did notice that they don't include DOCTYPEs, which means that all their pages are not valid HTML. Very badly designed as well.\n\nEven just checking the framesets with http://validator.w3.org shows loads of problems...\n\n\n--\n\nAs an aside -- has anyone seen a bug with http://validator.w3.org and Konqueror in KDE 2.2.1 where the floating menu is *under* the title?"
    author: "Jon"
  - subject: "Konqueror even clones MSIE deficiencies"
    date: 2001-09-24
    body: "Konqueror is so far in reaching complete MSIE compatibility, that it even clones the content negotiation deficiencies from (old versions of) MSIE.\n\nWhen you surf, Konqueror claims that the only language you are able to read is the one your user interface uses at the moment. If you visit web sites, which use content negotiation for selecting the appropriate language for the reader, this deficiency will reduce the usability dramatically.\n\n/Jacob"
    author: "Jacob Sparre Andersen"
  - subject: "Re: Konqueror even clones MSIE deficiencies"
    date: 2001-09-24
    body: "Please feel free to check before making such statements!  Starting with 2.2.1\nyou can change the language information that is sent to remote machines to whatever you want.  There is just not GUI config option for it yet!  In fact you can change this setting for per domain if you like!  Anyways, if you want\nto do this simply add\n\nLanguages= comma-separated-list-of-language-entries-in-order\n\nto $KDEHOME/share/config/kio_httprc, where $KDEHOME is you own local KDE config directory which usually is \".kde\".  If you add the setting under a group with the hostname, then it would only apply to that specific domain.  For example,\n\n[somehost.com]\nLanguages= comma-separated-list-of-language-entries-in-order\n\nAnyways, I probably will write a quick how-to since there are many more options that are configurable like this.\n\nRegards,\nDawit A."
    author: "Dawit Alemayehu"
  - subject: "Re: Konqueror even clones MSIE deficiencies"
    date: 2001-09-25
    body: "> Please feel free to check before making such statements!\n\nI DID check it out first. I just made the mistake of assuming that a graphical user interface can be configured through a graphical user interface.\n\nI have edited the configuration file as you suggest, but it does not seem to have any effect. Is it necessary to log out to make it take effect?\n\n/Jacob"
    author: "Jacob Sparre Andersen"
  - subject: "Re: Konqueror even clones MSIE deficiencies"
    date: 2001-09-25
    body: "Well,  it will enventually have a graphical configuration.  What I meant by my statement was that you should ask before stating what you said as a fact.  I did not mean for it to sound rude:) Anyways, for the changes to take effect you can either restart KDE or use the following command which will spare you that action:\n\ndcop konqueror KIO::Scheduler reparseSlaveConfiguration \"\"\n\nHope that helps.\n\nRegards,\nDawit A."
    author: "Dawit Alemayehu"
  - subject: "Konqueror vs. Mozilla"
    date: 2001-09-24
    body: "I have to say here that Konq started rendering my KDE page (http://kde2.newmail.ru) correctly around 8 months before Mozilla was able to do it.\nI refer here to backgrounds in A and DIV tags (menu on left side).\nYes, Mozilla 0.9.2 which I have now installed can render page ok. But Mozilla M18, 0.8.x and all versions between *couldn't* do it.\nUnfortunately my HTMLtests page was killed by provider, but *fortunately* \nMax Moritz Sievers did backup copy of it (and even added several improvements)\nSo please go to http://htmltests.enddeluxe.de\nand see how Konq 2.1beta was running against Mozilla and MS IE (in February 2001!). You can try Konqueror 2.2.1 from recently released KDE 2.2.1 and see improvement, and also compare to *old* screenshots.\nI am now working on updated set of tests, stay tuned...\nAfter all:\nKonq is great, isn't it?\n\nCheers,\n\nVadim Plessky"
    author: "Vadim Plessky"
  - subject: "Re: Konqueror vs. Mozilla"
    date: 2001-09-24
    body: "I'm not sure to understand your test pages. Sometimes, you have links that are pointing to pages using valid CSS styles (w3c validated), and others pointing to pages that make Mozilla fail, but are not valid CSS.\nDon't you think that if the CSS is not valid, it's the CSS that is responsible for the bad display, not the browser?"
    author: "oliv"
  - subject: "Re: Konqueror vs. Mozilla"
    date: 2001-09-25
    body: "> I'm not sure to understand your test pages. Sometimes, you have links that \n> are pointing to pages using valid CSS styles (w3c validated), and others \n> pointing to pages that make Mozilla fail, but are not valid CSS.\n\nTests were prepared in Jan.-Feb. 2001, in process of bug-catching in Konq for KDE 2.1 release.\n\"pages that make Mozilla fail, but are not valid CSS\" are taken from real-world sites or from examples in several books.\nIt's not a secret that many people use books to study HTML/CSS/JavaScript.\nSo you should, probably, blaim here authors of those books, not me!  :-)\n\nWhat I did after all (finding that Konq and/or Mozilla fail on some pages) - I rewrote them, making W3C compliant.\nIt helped in many cases, while A/DIV background in Mozilla still was a problem.\n\nIn short, Mozilla has good CSS support, but Konq is better, IMO.\n\nCheers,\n\nVadim\n\nP.S. As I have mentioned, I am working now on new, updated suite of tests."
    author: "Vadim Plessky"
  - subject: "https through proxy"
    date: 2001-09-24
    body: "Konqueror is good and fast browser, but there are some things that i dont like. For example since KDE2.2 https through proxy does not work. Also some javascript is missing, so i have to use sometimes Mozilla."
    author: "IP"
  - subject: "Re: https through proxy"
    date: 2001-09-24
    body: "Please feel free to open a bug report on the https proxy stuff.\nI can only attempt to fix any bugs I receive report on.  Thanks"
    author: "Dawit Alemayehu"
  - subject: "Re: https through proxy"
    date: 2001-09-24
    body: "Are you using Redhat RPMs?  Some other people using Redhat RPMs were complaining about this.  It's not a KDE problem because it works fine for many people, I think it is a problem with your KDE installation."
    author: "not me"
  - subject: "Re: https through proxy"
    date: 2001-09-24
    body: "Have you tried 2.2.1?  The https thing was a known bug in 2.2 and has been fixed (at least, it Works For Me(TM))."
    author: "Ranger Rick"
  - subject: "Konqy and memory management"
    date: 2001-09-28
    body: "I just want to comment on this, since I saw someone talking about KDE 2.1, and would like to say that konqy 2.1.0, if left on for long enough, usually a couple days, will eat up RAM and swap space like there's no tomorrow.  I don't have or need a clue on if this is fixed, since I moved over to primarily console(18) and in X, I use windowmaker and mozilla from cvs, which is sweet as hell.\n\nAlso, what would be really cool is, if someone doesn't have openssl on his machine, and installs kde, and then later decides to install openssl, has to rebuild kde just to allow the use of ssl.  What would be better is, if ssl isn't found on konqy startup, don't use it...simple test....\n\n\nJust another of my $.02"
    author: "gandalf"
  - subject: "Re: Konqy and memory management"
    date: 2001-09-28
    body: "It's quite hard to build SSL support into your program if the SSL libraries aren't on the system -- you would have to incorporate an awful lot of information about the library which would probably no longer be true the next time the SSL library is updated.\n\nBut you don't really care about that, you just want to troll."
    author: "Jon"
  - subject: "Re: Konqy and memory management"
    date: 2001-09-29
    body: "Now, why would you have to do that, if you just build an api and use ssl as an ioslave....that shouldn't be to hard...well, I haven't tried it, but I'm gonna look it over soon....\n\nhere's what I was trying to describe: \n\nhave an ioslave for the ssl, and set it up so you can just d/l that source tree when you need it, and also have it in the kdebase with konqy.\nthen, wrap all your ssl calls around this ioslave, so that konqy doesn't have to really know much about the ssl itself, it just asks the ioslave for help.\n\njust an idea....sorry if I sound like a troll...I just find it rather difficult to use if I need to have ssl already on in order to let konqy support it....\nmaybe you already do this...I haven't checked the source tree much, but I think it might be nice for those of us like me who build lfs, and just want to get the DE up, so he can go on the web and get the ssl support.\n\ngandalf"
    author: "gandalf"
  - subject: "Re: Konqy and memory management"
    date: 2001-09-30
    body: "You really should just use Debian if you want this kind of automation.\nThen it is just \n\napt-get install kdelibs3-crypto\n\nto upgrade to openssl support. If you are a compiloholic, you are asking for extreme boredom, \nand should not be complaining about it."
    author: "rob"
  - subject: "Gecko renders the layers with round edges"
    date: 2001-10-10
    body: "Which looks quite cool. Konqueror doesn't\n\nBut yes scroll speed is bad under gecko."
    author: "Danie Roux"
---
<a href="http://www.xs4all.nl/~ppk/cv.html">Peter-Paul Koch</a>,
maintainer of a JavaScript/DOM/CSS testing site, recently updated
his <a href="http://www.xs4all.nl/~ppk/js/browsers5.html">browser
section</a>.  In it he reviews
<a href="http://www.konqueror.org/">Konqueror</a>, together with
the other Version 5 browsers, <a href="http://www.mozilla.org/">Mozilla</a>
and <a href="http://www.microsoft.com/windows/ie/default.asp">IE</a>.
He concludes, &quot;<em>In short, the few remaining bugs in Konqueror are
details that no doubt will be solved soon. The development team has
succeeded in building an excellent, standards compliant browser from
scratch. Therefore I expect Konqueror to become a real competitor for
Netscape 6 on Linux: it has made an excellent start and can only
become better</em>.&quot;

<!--break-->
