---
title: "Help give a voice to KDE"
date:    2001-02-18
authors:
  - "Inorog"
slug:    help-give-voice-kde
comments:
  - subject: "Strange title for a news ?"
    date: 2001-02-17
    body: "When I saw the title of the news, I believed that KDE team wanted to make KDE speak :)\n\nOf course, I already have \"festival\" on my \"babasse\" (French Word to speak about own computer) and it is very cool. It can read files (mail for examples). The only problem is that I haven't succeeded in making it speak with a French pronounciation :(\n\nIt's out of topic,... I know ;)\n\nBut what about integration of plugins to interface with apps like festival in kde apps like kbiff ?"
    author: "Shift"
  - subject: "Re: Strange title for a news ?"
    date: 2001-02-17
    body: "I don't know if you know or not but it might be interesting info: There was an article here a wile ago regarding the fact the QT & IBM had demo'ed a voice enabled QT so real voice enabled KDE might not be that far away? There is the licensing issue to sort out so I guess a voice enabled QT might actually be non-GLP'ed.\n\nAny tips on festival? I played with it for 30 mins or so but it wasn't something I'd use to read to me, is there anyway to make it sound a bit more human? I've never played with the MS stuff either so I don't know how festival compares to anything else."
    author: "David"
  - subject: "Re: Strange title for a news ?"
    date: 2001-02-17
    body: "In fact, I have seen this news about QT&IBM.\n\nAbout festival, I haven't tested it a lot. I haven't got time to do it 'cause I have some works for my school :(\nIf I succeed in using french voice, I will look at other parameters but not before that ;)\n\nI also try the duo kvoice/festival but it can work only with full-duplex soundcards but my SB Vibra16X can't do that under \"Nunux\" (even with alsa). It will be funny to speak to my computer and that he answer me :))\n\n\"I love the off-topic\""
    author: "Shift"
  - subject: "Re: Strange title for a news ?"
    date: 2001-02-19
    body: "Well, i played with mbrola, and there is quite cool french voices.. There is a perl script also, for read a french text. Very cool, and easy to hack..."
    author: "Nicolas Roard"
  - subject: "Festival Speech"
    date: 2001-02-18
    body: "A while ago, while mosfet.org was still active, I saw a link to someone who had patched kedit to interface with festival. Take that, simpletext!<br><br>\nThat IBM stuff is ridiculous. IBM obviously plans to sue the pants off of anyone who starts any new KDE speech projects, even though there is no chance that KDE developers will make commercial libraries a central part of their progs."
    author: "Carbon"
  - subject: "Re: Festival Speech"
    date: 2001-02-18
    body: "Wow you are clueless. IBM was nice enough to get this working on Linux. It is people like you who will guarentee that MS will always have a home.\n\nFor your information IBM is working to make QT/Speech integration better. They spent a VERY large sum of money on it.  Now I understand you have no concept of the cost of things but some of us do and are thankful.\n\nAs a side note, festival is to slow to do real time speech, but there are some open source solutions in the works."
    author: "ian reinhart geiser"
  - subject: "Re: Festival Speech"
    date: 2001-02-18
    body: "I got good results with mbrola/hadifix, but this is only for german."
    author: "Rolf"
  - subject: "Re: Festival Speech"
    date: 2001-02-19
    body: "Yes, they were nice enough to suggest ever so beneficially to the developers that they should integrate a library into their desktops that effectively makes it non-open-source. If major KDE progs start using this, and making it a seriously important part of their architecture, then said progs will cease to be free source code. That's all there is to it, and I frankly don't give a politician's ass how much money they spent. I could understand it if they were attempting to put this out to encourage the development of niche kde apps, apps that specifically involve voice detection (such as a kde based telephone remote admin system or some such thing), but this basically makes very little sense anyways, since a specialist will want to alter their systems manually, and then they'll just use GVoice, which works quite well in situations which use custom dictionaries (the airplane scheduler is very hip). Also, that crack about having no concept of the cost of things, doesn't that seem pretty inappropriate on a news site about a DE that is almost completely developed by people working in their spare time for free?"
    author: "David Simon"
  - subject: "Re: Festival Speech"
    date: 2001-02-19
    body: "Qt has several lincencing agreements. I think its great if some people will develope commercial apps for kde. Of course open is awesome but commercial is ok to."
    author: "Craig black"
  - subject: "Re: Festival Speech"
    date: 2001-02-19
    body: "I totally agree. Especially, I think that theKompany has the right idea : help KDE by doing open source devel so that potential customers will use it, and then make commercial accessories for it. Unless I've completely misunderstood their post to kde news, it sounded as though IBM wanted to integrate closed source voice control integrally into core KDE components."
    author: "David Simon"
  - subject: "Re: Strange title for a news ?"
    date: 2001-02-21
    body: "festival is IMO the only real thing we could use - it has a somewhere near usable API, can be turned into a shared library with just a couple of lines of patching (I've actually done this a while ago, if anyone wants the patch just drop me a message), and most importantly it's open source.\n\nMost of the alternatives aren't (e.g. mbrola has the \"no commercial redistribution\" restriction which shuts it out from all major Linux distributions)."
    author: "bero"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-17
    body: "Don't wanna just \"throw\" this out there, but shouldn't be a bigger effort to make KDE a little more response on older CPUs first? I mean, I have an AMD-350 w/ 256Mb of RAM and I have major performance issues when using Konquerer as my primary web browser.\n\nI REALLY love KDE 2.1 and everything that is happening, but I really believe that we are leaving ALOT of people behind by not focussing on performance for a while.\n\nI hate to bring this up, but my wife runs another OS on a P60 and I.E. renders and displays faster than my system.  A Pentium 60!\n\nI think before any more glitz gets added, two things need to happen:\n\nComplete code release focussed on making KDE more responsive and perform better on smaller systems (I really believe that KDE should function OK on a Pentium 166 and above)\n\nComplete code release focussed on better, more end-user friendly default values for all application and environment settings.  Better default values with improve usability more than anything else!\n\nAgain, this is just my 2 cents, but I felt that it needed to be said.  Let's focus on the important things and not get into glitz too much (that has GNOME's mistake for a long time, let's not relive it))\n\nChris"
    author: "Christopher Young"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-17
    body: "Really? For a while I had KDE2.0 running on a K2-450 w/ 64 MBytes of memory. And while Konquerer wasn't as snappy as IE or Netscape, it was completely usable."
    author: "JC"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-17
    body: "I have a very similar setup, with a AMD K6-2 400 and 64 MB of RAM. Konqueror seems to be a little faster than IE. Of course, I have IE 5.5, and that is said to be slower than previous versions.\nHowever, I think the video card makes a difference. My TNT2 performs much better than the old S3 Trio3d/2x card I used to have."
    author: "Josh Liechty"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "And dont forget X server, you get a huge performance boost going from xfree 3.3.6 to 4.0.x in many cases."
    author: "asdfuweyyewru"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "Here p2-266/64Mb, rage pro, x3.3.6. The machine is perfect to use... Maybe with 128Mb of RAM it would be better (like now 2konquys+emacs is just just), but the proc is perfectly OK.\n\n(And somehow the windows move more smoothly than on my p3-550/128Mb/r128/x4.0.2, grrrrrr)\n\nEmmanuel"
    author: "emmanuel"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "Hmm, I use a P166 with 64 MB Ram. KDE and Konqueror are very usable, and fast enough. I regulary have about 10 or more Konqi windows opened, without suffering performance loss.....\n\nKind regards, Rinse"
    author: "rinse"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "Hi!\nGuys,\nI 'don''t know what you mean\nby \"perfomance\" and \"fast\",\nbut I''m running KDE2.1beta2/XFree-4..0.2\non PIII-800/133FSB with 192Mb RAM,\nand I think it could be really\nmore responsive.\nAlso, we have 70 workstations (PII-266/64RAM), running KDE 1.2\nand already started switching to KDE 2.x\nmaking our users not really happy with\nit's perfomance..\n\nI'm not confused with that for now,\nbecause KDE is a really GREAT project and it is in the process of development.\nBut I suppose there could\nbe some things to do to make it\na bit faster :)."
    author: "Much Foster"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "i guess try to find out what the problem is.. the problem is def. not with KDE.. its really fast.. look at my post.. after kde comes up, i do a free and i get memory usage as just 31MB.. so its cool! it shld be perfectly usable on a 64MB machine.. check if there are any backgroud processes running.. arts in KDE 2.1beta seemed to be broken a bit.."
    author: "sarang"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-19
    body: "KDE 2.0 being slow? God no! I even dumped wicked2k\njust because KDE was:\n\na-More reliable\n\nb-Way quicker when with XFree 4.0.1\n\nc-Updated NVidia drivers\n\nd-And gosh, extremely more nice to work with.\n\nGuess it all depends on your config, i have an AMD Athlon 900 with 256Mb RAM and GeForce2 MX :)\n\n\u00c9tienne"
    author: "\u00c9tienne Brouillard"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-17
    body: "I have a 450MHz AMD with 128MB memory and KDE 2.x simply rocks.. infact, KDE 1.2 after booting showed 51Mb of usage.. now when i boot 2.x, the initial memory usage is just 31MB!\n\nSo I guess there is some problem with u're machine's configuration.. please don't blame KDE.. they are find on lower end machines."
    author: "sarang"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-17
    body: "Agreed.  Konqueror is very good as it is, but it is a tad slow compared to IE.  As a web browser, they both take about the same time to load, but as a file manager IE starts up almost instantly.\n\nSince Konqueror treats folders and websites as one in the same (IE does too, but has a more obvious mode-switch), optimizing the code would probably make both types of browsing go faster.  Even though Konqueror loads up at a \"bearable\" rate compared to Netscape / IE, I think it should (and could) be optimized to just blow everything out of the water.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "<blockquote><i>Agreed. Konqueror is very good as it is, but it is a tad slow compared to IE. As a web browser, they both take about the same time to load, but as a file manager IE starts up almost instantly.\n</i></blockquote><p>\nOf course IE starts faster, major parts of it is preloaded when you boot up your machine and there are no way to prevent that. If you want exactly the same speed you would probably have to stick parts of konqueror into the kernel. (which i don't think Linus would approve of :)"
    author: "Troels"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "Well I think loading into the kernel is a bit extreme.  Maybe preloading when you start KDE?  There is a performance boost when you have Konqueror already open.  Since Konqueror is a very core portion of KDE, I think it ought to be preloaded as much as possible.\n\nOf course, not everybody would want this, which is why it should be just an option. =)\n\n-Justin"
    author: "Justin"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "I was being sarcastic about the kernel part ;) I generally find preloading a bad thing. Load on demand is a more clean way of doing things. This of course results in longer loading times, but then again if your machine is slow enough for you to notice a big difference then you should be happy that it doesn't take up extra resources by preloading stuff you might not need. I guess making it an option might not be too bad an idea but then what should be preloaded? Would be nice if kdevelop would start instantly too you know. Nah, i think it is fine the way it is. Bloating the system might gain you a few seconds of loading time but i doubt it is worth it."
    author: "Troels"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-17
    body: "<P>What you describe does not correspond with my experience. I run KDE 2.1beta2 on a K6-2 266mhz with 64mbs of RAM. So my machine is definitely not as performant as yours. My primary OS is a RedHat 6.2 install, slightly modified.</P>\n<P>The only times when I get \"major performance issues\" is when I am running low on memory. Since I develop websites, I sometimes run Netscape 4.X, Mozilla 0.8 and Konqueror all at the same time. THEN things get slow. But if I had half the RAM you have, I would still be able to get away with it.</P>\n<P>I even dual-boot on this machine, so I can compare. Konqueror exhibits no major performance issues as far as I am concerned when compared with Mozilla, Netscape and IE. The only slowish part of Konquy is start-up, especially since the new, very beautiful greeter page was added (beta2). But it is still better than Mozilla with regards to that.</P>\n<P>Don't get me wrong: I agree with you that optimizing KDE and Konqueror are very good things. But judging from what I see here, if you do experience \"major performance issues\" with Konqueror, there is probably a problem with your setup. (Or maybe we just have different definitions of what is a \"major\" performance problem.)</P>\n\n<P>Jerome</P>"
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "HOWTO make konqueror startup fast"
    date: 2001-02-18
    body: "Type \n\nabout:blanc\n\ninto your location-bar and choose \"Window -> Save View Profile 'Web Browsing'\"\n\nThen start Konqueror using the Konqueror-icon"
    author: "a@b.c"
  - subject: "Re: HOWTO make konqueror startup fast"
    date: 2001-02-18
    body: "mistake -- it should read:\n\n\nabout:blank"
    author: "a@b.c"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-19
    body: "How do we turn the greeter page off? I would rather it went to my home page!"
    author: "Stuart"
  - subject: "Re: Help give a voice to KDE"
    date: 2003-03-30
    body: "I'd be prepared to dissagree with that, if you use Konqueror to copy files over samba networks, ALL konqueror windows respond _painfully_ slowly, sometimes you have to click on them 10 or 20 times to get them to close. etc.\n\nThis is on:\n\nSuSE 8.1\nAMD K6-2 500MHz\n384MB RAM\nDual 60GB IDEs & 10K SCSI-160\n100Mbit net."
    author: "Martyn"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "What kernel are you running???\n<br><br>\nYou might want to get a newer one in right away. Starting about 2.2.17 there were some noticable increases in performance on my system. I have an Athlon 700 with 512 MB and my wife's system is a K6 III 400 with 128 MB. They are both very crisp.\n<br><br>\nIf you are running anything like 2.2.15 or earlier you will probably see performace seem to be about double.<br><br>\n--<br>\nMember of the Quanta Team<br>\n<a href=\"http://quanta.sourceforge.net/\">quanta.sourceforge.net</a>"
    author: "Eric Laffoon"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "That's wacky, I have a 200mhz AMD k6 with 64 mb of ram, and konqueror runs fine. It's certainly zippier then Mozilla! I think the problem is people forgetting that almost every Linux distro comes with daemons turned on that people either don't know about, or are afraid to turn off (to a newbie, they might think that 'sendmail' actually handles outgoing mail through their ISP, and thus waste a pretty considerable portion of CPU cycles). Besides, you can just turn off the sounds, all this means is that people who use sounds will have more ear-candy."
    author: "Carbon"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "If you knew the first thing about UNIX, you'd be able to sit there with top(1) and watch all the processes running on your system. You'd then be aware that most of those background daemons don't take up anything in terms of CPU time. Just RAM."
    author: "J"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "Oops, I meant system resources when I said CPU time. Thanks for pointing that out, but there's no reason to get a \"Hackier then thou\" attitude. :)"
    author: "Carbon"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-19
    body: "what kind of a stupid jerk response is that?"
    author: "Craig black"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "KDE 2 is extremely snappy on my Celeron 450 with 64M.  There is definitely something wrong with your setup.  Don't blame KDE until you fix it."
    author: "KDE User"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "Right. I can't complain about performance on my Celeron 300A with 96MB. KDE 2.1 Beta 2 runs fast enough. It's a fine click-and-point thing, comfortable and good looking. And there was nothing to fix, I just downloaded and installed it."
    author: "Ralph Miguel"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "I'd like to throw in my support for the optimization push.<p><p>\nTurning off useless daemons is certainly a good OS-level start.  We ought to apply this philosophy to KDE as well.  All over KDE, it should be easy to disable features you don't, especially ear and eye candy type features (I like em plenty, but sometimes I want em off).<p>IMHO, one of KDE's greatest advantages over Windows is that it's preserved much more true modularity than Windows - let's use this to make modules easily disabled and re-enabled.  I'm thinking of an augmented module manager, integrated with KControlCenter (even cooler would be one that worked with KTop) that helps you to disable processes, with, of course a Restore All Defaults Button if you screw up your config really bad and want to go back to scratch.  This will make KDE \"wicked fast\".<p><p>\nAnother direction is making compilation (and hence compiler optimization) easier.  Would it be possible to make a GUI installer that compiles tarballs, and if that bombs, helps the user go get binaries?\n\nCheers,\n\nEric\n\n\nPS - KDE from binaries runs fine on my RH6.1 machine, P3500 - 256MB RAM - Konqi is at least twice as fast as Mozilla, and doesn't have aggravatingly slow rendering.  Nice work Konqi!"
    author: "Electronic Eric"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "You're probably comparing a fresh Windows 95/98 machine to a Linux distro default install.\n\nDid you turn off the unrequired services on your Linux machine? inetd? atd? crond? Are you actually running in a \"server-like setup\" on a desktop machine with limited RAM?\n\nAlso, upgrading to XFree86 4.x and tuning up performance (32bit disk access etc.) might be a good idea."
    author: "Toastie"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-19
    body: "One of the things that can slow down KDE2 is an improperly configured QT2.  If QT2 is compiled with no exceptions (use -no-g++-exceptions along with all of the other ./configure options you use), it'll use a lot less memory and run noticeably faster.  Get the latest version from <a href=\"http://www.troll.no/\">Trolltech</a> and install it (Even if you're not the compiling type, don't worry.  It's not that hard).  Heck, using 2.2.4 over any previous version gets you better handling of poorly-created GIF and PNG images (example: benchmark graphs in Anandtech reviews), so it might be worth it for that."
    author: "SubPar"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-20
    body: "<p>Will programs that have been already installed (i.e. Konqueror) use the new Qt? If not, how can I force them to do so?</p>"
    author: "Josh Liechty"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-21
    body: "They will.  Make sure you delete the old version."
    author: "jon"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-20
    body: "Ok, because I have gotten so many (what I consider to be) newbie-oriented answers, let me clairfy a few things...\n\nI am running RedHat 7 w/ all updates w/ Xfree86 4.02.  My video card is a 3DFx VooDoo 3 AGP (16Mb).  I have been running Linux for about 5 years now.\n\nAll uneeded services have been disabled w/ chkconfig.  All uneeded services have been removed/disable from xinetd.  I have even lowered the number of virtual terminals (mingetty processes) in /etc/inittab in order to keep everything nice and tidy.  I consider myself to be way beyond newbie level on these things.\n\nJust wanted to clarify that since most of the answers that I have received tend to lean towards things other than KDE.  I KNOW that it is KDE because I can backtrack into KDE 1.x and everything is super snappy.  My point wasn't to try to go WAY off course (sorry for interrupting a good thread) but merely to point out that the KDE community needs to go back to its roots for a minute and cover the basics that make running Linux so great\n\nStability should always be number one.  No one disagrees on this.  I truly believe that to make the best desktop for UNIX, you need to consider the impact that the desktop environment will have on performance and put a real effort into it.\n\nI'm NOT saying that the KDE people don't do this!  I love KDE and everything that it offers.  The KDE developers have my gratitude forever as thier environment makes me very productive in Linux.  I am just suggesting that we backtrack a little and try to cover the basics before throwing in every feature we can think of.\n\nAll I know is that I watch my CPU pop up to 100% when rendering pages and things are really slow.  On my other system (a laptop) which is a PIII 700/ 256 Mg of RAM everything is wonderful, which is why I'm concerned.\n\nOk, I'll stop babbling, but I can guarantee that it _IS_ KDE (or some aspect associated with it) that is slowing things down heavily.  I have made certain that the binaries have been stripped and have done all of the usual system tuning.\n\nThat's all I have to say.\n\nChris"
    author: "Christopher Young"
  - subject: "KDE can be very slow"
    date: 2001-02-20
    body: "on machines where it shouldn't be (i.e. 600mhz pIII) due to strange combinations of cpu-cach main board characteristics, buses, disks, controllers, etc. It can be very very difficult and frustrating to debug.\n\nKDE2 and Staroffice 5.2 run fine on my Pentium 133 (!!!) but both are sluggish as molasses on my p3-450. In fact on that machine staroffice 5.2 is *unuseably* slow - it simply will NOT work at a useful speed. So I use KDE 1.2 and StarOffice 5.1 on my 450 and KDE2 and SO 5.2 on my Pentium 133 ... go figure."
    author: "oogabooga"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-21
    body: "are you installing kde in package form? if you compiled from the sources with the right optimizations and useless stuff left out it might help."
    author: "Thom Lawrence"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-20
    body: "As far as speed goes, compilation otions are really important, and a lot of distros did a piss-poor job (*cough*redhat*cought*) with kde2.0 of optimising the builds.  Particularly, if you don't turn exceptions off in the qt build, it increases the size of EVERY qt-linked binary by a huge amout (we're talking 1meg+ here).  That, and most of KDE doesn't use exceptions either - AFAIK, only khtml needs them.  Plus, the default build scripts optomize for a 386!!! Who tries to use kde2 on one of those?  If this hasn't already been fixed in the build process, it should be top priority.  Those extra megs in every binary are enough to push a 64meg system over into swap when you're not doing much at all.  And that explains the speed arguments between people here - my start times were devided by 4!!!! when I recompiled form source with sane options."
    author: "Wayne Vinson"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-17
    body: "Wow, i agree that kde needs new sounds. But, how does one make them? i don't know :P\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "Just make a wave file that sounds neat. One way to start would be to use a midi composer, hit 3 or 4 notes that sound sort of neat, and have sox screw with it. I've had a lot of fun with the sox reversal filter! !looc tub elpmiS"
    author: "Carbon"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "I think the bigger q is what good software for doing this is.\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "Everyone has been posting stuff that they would like to see happen on KDE, and I have a few. It is for KWrite, I would like for it be able to save in *.doc format or some format that MS Word can. Also, in KWrite a print preview would be nice. I would like for KMail to beable to open graphics in email. Of course anything to make KDE faster would be great, too.  Thanks for make the best!"
    author: "Stuart"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "KWrite is a text editor and will probably never same someting else than simple text. And MS Word can read simple text. If you meant KWord, it's being worked on, and it has a print preview.\nKMail is able to open graphics in email.It shows them inline or can open them with a program, and you can d'n'd them just anywhere. What are you missing?"
    author: "Rolf"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "About KWord, I like to see MS Word improve their import/export filters so you can use KWord files in MS Word en vise versa. \n:)\nAbout Kmail, it can show attached grafics, but not embedded ones. If de sender creates a HTML email containing graphics, Kmail does not show them emmedded in the mail body (or at least not on my system, but it displays them as attachements\nBut enough about Kde wishes, KDE has a fine bug system where you can post your wishes for future releases.\n\nKind regards, Rinse"
    author: "rinse"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "Following on from the comments about priorities and stablity:\n\nI think KDE is a superb system and has plenty of features, so adding more isn't urgent IMHO.\n\nThere is, however, one seemingly small thing that presents a huge barrier to Linux (or Unix) and KDE (or GNOME) on the desktop: copy & paste.\n\nI realise this isn't a new revelation, and that a lot of good work has been done. But, again IMHO, it has not been resolved.\n\nExample: I cannot type this message in KWrite, select Copy from the menu and then Paste it via the Edit menu of Mozilla. For new users this is a disaster.\n\nAfter years of using computers, including Amiga, Mac and Windows, I found it really difficult at first (eventually you learn about the middle mouse button etc. and then it's only somewhat inconvenient). My girlfriend, less experienced but nevertheless intelligent [:-)] found it frustrating too.\n\nLuckily we both stuck with it, but many users won't. I really think this issue is a huge barrier to Linux on the desktop - it's a little niggle that will annoy almost anyone who comes to Linux from Windows or Mac; it'll put them off, and they'll tell people that Linux is 'fiddly' or 'hard' to use. [I know, KDE != Linux, but there's a close relationship there ;-)]\n\nI realise this isn't KDE's fault. It's also not the fault of Mozilla, GNOME, XFree86, Linux, Bill Gates, or Saddam Hussain. But it's a problem nevertheless, and if there is *anything* KDE can do to contribute to its resolution (through tweaking KDE or working with others), then that would be an extremely worthwhile enhancement. (Of course, the same goes for the good people behind GNOME, Mozilla etc.)\n\nIMO: Stability, Usability, Features - in that order.\n\nTo the KDE team/community: you guys are doing a fantastic job, and I hope you don't mind me drawing attention (back) to this issue.\n\nKind regards,\n\nLars."
    author: "Lars Janssen"
  - subject: "Potential Copy/Paste Solution"
    date: 2001-02-18
    body: "I believe the root of the problem is the way all apps use copy/paste.  Also, isn't the middle-mouse and selection autocopy a feature of X?  I don't know if this is possible, but maybe disabling that feature would solve the whole problem.  Sure it would kill all copy/paste support in non-Qt apps, but it might solve any frustration with copy/paste in KDE.  In fact, by merely disabling that feature in X, KDE would probably operate exactly like Windows does in this regard.\n\nWhile selection-autocopy is handy (and sometimes the only way, like in Konsole), it really can be annoying when writing text or programming.  Fortunately, Kwrite actually has a feature to disable it.  Now I can copy blocks of code and paste over blocks of code without worry of autocopying the destination region.  It would be cool if this was a global feature in KDE.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Potential Copy/Paste Solution"
    date: 2001-02-18
    body: "No! Don't disable this. It's something I really like.\nLook about how nedit does this. It supports both. But Ctrl-C/Ctrl-V uses it's own buffer, so that you can select a part of your text and paste someting over it with ctrl-v. And the normal paste with middle button still works."
    author: "Rolf"
  - subject: "Selection-AutoCopy"
    date: 2001-02-18
    body: "Having an extra buffer for keyboard copy/paste is quite strange.  What if you want to do a keyboard copy to paste into another application?  Or vice versa.  The clipboard should be universal.\n\nThe concern is the automatic copy to clipboard that X does.  While faster for doing quick grab-and-paste actions, it can get in the way when dealing with selections.\n\nTwo easy ways to nuke your clipboard:\n1) Make a selection to paste over.\n2) Make a selection to delete\n\nBoth are very annoying.  It is reasonable that you should be able to highlight and delete a paragraph of text without destroying your clipboard, isn't it?  The X clipboard is like a timebomb.  Hurry and put it somewhere, *anywhere*, even in a new text editor window.  Just don't keep it in the clipboard for too long, or you'll kill it by accident.\n\nIt's clear that both methods of copy/paste have their ups and downs (Windows style Ctrl-C/V = more work for the user, more control, X11 style = quick and easy).  I think having an extra option to disable it would be a good thing.  I absolutely love the feature in Kwrite.  If only all of the KDE apps had such a feature (or a KControl global setting).\n\n-Justin"
    author: "Justin"
  - subject: "Please use klipper."
    date: 2001-02-18
    body: "Thank you. I like cutnpaste the way it is."
    author: "Moritz Moeller-Herrmann"
  - subject: "The Solution"
    date: 2001-02-18
    body: "Well that's why if an alternate method is devised, it should be an option.  Everyone likes to do things differently.  It's all about choice.  There's nothing wrong with it being an option, is there?\n\nIMO I think it's a bit strange to lose your current clipboard while doing selection delete, and then turn around and retrieve the original content with klipper.\n\nAnyway I gave some thought to this problem and I came up with what I think is a simple solution.  Why not just have autocopy only work when using the middle mousebutton to highlight?  Highlighting with the left mousebutton would not copy.  This would totally solve the problem.  Best of both worlds.\n\n-Justin"
    author: "Justin"
  - subject: "Re: The Solution"
    date: 2001-02-18
    body: "The middle button DOES NOT HIGHTLIGHT. It pastes. That is X standard. This is KDE standard.\n\nIf you prefer the windows method which is equivalent, but different, stick with windows.\n\nYou can't have an option for everything. Too many options work against usability.\n\n\nYou are just not used to the X way. Why do you have to delete BEFORE you paste? Paste, then delete. Viola, no klipper needed."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: The Solution"
    date: 2001-02-19
    body: "> If you prefer the windows method which is equivalent, but different, stick with windows\n\nThat is a very unfair thing to say; and the \"X way\" is not so holy that it cannot be challenged if a more user-friendly alternative is available.\n\nSee my reply to your previous post for more of my thoughts on this matter.\n\nMacka"
    author: "Macka"
  - subject: "Re: The Solution"
    date: 2004-03-12
    body: "Yes, the people that say \"stick with windows\" are elitists and have no clue about what a good user interface is.  The X method is indeed annoying for anybody except poeple that copy/paste to and from a shell window.  Desktop users of Linux could do without the X method.  I love linux and have been using it exclusively for years, but the whole cut and paste issue still drives me nuts.  This is one case where the windows way to do it is A LOT more intuitive.  I will welcome the day when they finally get rid of the X windows cut/paste method.\n\n"
    author: "Anonymous Coward"
  - subject: "Re: Please use klipper."
    date: 2001-02-19
    body: "I've been in the computer industry for about 15 years now.  Starting out with VT100's, then working up to X-terminals and VMS workstations, then Unix (for the last 10 years).  I've only had to use MS windows (and therefore the RMB cut-n-paste) for the last two years.  Eventually I got forced into it through work.\n\nYou'd think that having all those previous years experience with X Windows that I'd be a die hard MMB man and pretty much set in my ways.  Not the case.  I prefer the MS style cut-n-paste and given the choice would use that everywhere.  It's one of the (very) few things about MS Windows that I do advocate (that and ^Z undo in text widgets .. what a life saver that one is ;-)\n\nI agree with Justin that this is quite an obstacle to people migrating from MS Windows to an X Windows based environment.  That and the fragility of the X clipboard and the ease with which you can loose what you've selected en-route to the next application.\n\nJust pointing out that not everyone thinks as you do!\n\nMacka"
    author: "Macka"
  - subject: "Re: Please use klipper."
    date: 2001-02-20
    body: "Hello,\n\nI have read all the messages on this cut and pat topic.  I an replying to this message of Macka as I believe it best summarizes the essence of the matter.  I to was forced to Windows NT both personally (now migrating to Linux :<)) ) and professionally (missed the whole 3.x/Win9x experience personally :<)))), but had limited time in professionally).\n\nI have to admit the degree of seemless and ease of how the cut and past works in windows is truely a wonderful feature.\n\nI know Linux is about choices, and I have seen some window manageers provide a close, but not quite complete cut and paste functionality as Windows has.  I really like using KDE, and really want the same, perhaps we can even improve a few samll aspects, of the cut and paste that exists in windows.\n\nI believe the option to have a Windows Like\" cut and paste like behaviour should exit for KDE.  It will need to be seamless across all applications and wigets.  Simply put it allows users to keep their preference of choice on this matter as it seems to be so important for those that have voiced an opinion here, but also for those that would if they were aware of this forum.  I would hope the implementation of the option could be like the other KDE options that can be set via the KDE options.\n\nLinux is not only about choices, but also in striving to be the \"best of the breed\".  That does mean some aspects of Linux will have features or applications that existed on other OS's, but either implemented better, cleaner and/or with less bloat.  That is what makes Linux and all of what brings the Linux experience together such a growing and welcome experience.\n\nSo please do bring the ease of Windows cut and paste functionality to KDE.  Think of the many more users that would be so happy to have this and the many untapped users that would consider Linux as an alternative to the OS's or wannabe OS's that users stay with just because of the value, power and ease this feature brings to users.\n\n\nJohn L. Males\nSoftware I.Q. Consulting\nToronto, Ontario\n20 Feb 2001 14:22"
    author: "John L. Males"
  - subject: "Re: Please use klipper. [ off topic ]"
    date: 2003-12-21
    body: "IMO, the MMB for paste is better than for scroll anchoring in windows, not only is that what the cursor keys are for but also you have sufficent widgets to move the window anyway, if you want a device to scroll you have the wheel, buttons 4 and 5. I know most mice don't incorporate this device. And anyway shift selections can be pasted over without klipper replacement.\n"
    author: "William coleman"
  - subject: "Re: Please use klipper."
    date: 2004-03-12
    body: "Totally agree with you.  This is one area where the windows method of copy/paste is just intuitive and makes sense.  The x windows method is meant for people that need to copy/paste between x apps and shell window... that is all.\n\n"
    author: "Anonymous Coward"
  - subject: "Re: Selection-AutoCopy"
    date: 2001-02-18
    body: "Are there any applications to monitor or manage both X and kde's clipboard ?\n\nit would be nice that a third party application could handle cross clipboard transfert with every app. that can handle text selection."
    author: "Massimo"
  - subject: "Re: Selection-AutoCopy"
    date: 2007-05-10
    body: "Interesting set of views. I like both methods as much as each other.\nIt all depends on the tasks at hand.\nEveryone will, at some point, perform repetitive tasks, tasks which are too small and infrequent to automate.\nI'd be much happier in that circumstance using '[DLClick|highlight] [alt-tab|LClick] RClick' method and skip the extra 'ctrl-c' - as long as switching between methods didnt take more effort than the extra 'ctrl-c'.\n\nregs, "
    author: "sedman"
  - subject: "Re: Potential Copy/Paste Solution"
    date: 2001-02-21
    body: "If memory serves X actually has several cut buffers available: selection-autocopy and middle button only use the primary selection, the most ephemeral of them.  Emacs has a notion of a secondary selection which I believe corresponds to one of the other X cut buffers, and you could use that for C-c and C-v."
    author: "Graham"
  - subject: "Re: Potential Copy/Paste Solution"
    date: 2001-02-23
    body: "Yes, as far as I can make out there are two buffers commonly used by X, and maybe a third one too that is rarely used.\n\nI read something about it here a while back:\nhttp://www.freedesktop.org/standards/clipboards.txt\n\nFrom what I can make out, there is a PRIMARY buffer, a SECONDARY (rarely used) and the Clipboard, and the author of the above page seems to be suggesting something like this:\n\n1. If you highlight text with your mouse, it goes into the PRIMARY buffer. If you press the middle mouse button, the contents of the PRIMARY buffer are pasted.\n\n2. If you highlight text and select 'copy' (menu or keyboard shortcut) it goes into the Clipboard. When you 'paste' (menu or keyboard shortcut), the contents of the clipboard are pasted.\n\nThus, the old X way and the Mac/Windows/Amiga(almost)/others way can co-exist more or less happily; new users will not need to learn about the middle mouse button trick unless they want to.\n\nI hesitated to say this when I originally posted, because I don't know *for sure*, and I don't want to suggest KDE are doing it 'wrong' (if there is such a thing) - but the impression I get is that when you select 'copy' (menu or shortcut) in a KDE app, your text goes into the Secondary buffer, or somewhere else specific to KDE maybe? - rather than the clipboard. I'm not sure, but unfortunately it doesn't seem to go into a place where non-KDE apps can find it, such as GNOME apps (in fact I think even between KDE apps it's tricky, whereas with GNOME it works a bit better for me).\n\nAs to whether KDE, GNOME or both should change, I really don't know, and I think that's something they should discuss with each other. (Where 'should' == 'if you want to remove a huge barrier to new users that harms KDE and GNOME alike')\n\nIn short, we need to see standardisation; this is a real shortcoming."
    author: "Lars Janssen"
  - subject: "Re: Potential Copy/Paste Solution"
    date: 2006-06-17
    body: "Sound's exactly like what I want. So, how do I disable middle-click-pastes? Whether in X or KDE, whatever, yet still allow middle click for other stuff. It's too easy with the wheel mouse to insert random text all over the place!!\n\nOtherwise, if this is a place for wish lists have an option to disable it and/or allow it to be qualified with, say, shift-click (the console uses shift-insert) for those with sensitive mice..."
    author: "Dan"
  - subject: "Re: Potential Copy/Paste Solution"
    date: 2006-11-17
    body: "Well, I'm not sure if many of you know this, but if you download the source code you can make your own modifications to have the functions you want.\n:)\n\n\nAs for the Middle Mouse click, I think it should be on by default (and just work) and if you want it off, you can turn it off. In fact, I have used many distro's and KDE seems to have the most difficult time with the feature. Forcing us to build workarounds so that KDE functions work correctly with our custom applications.\n\nDeleting the feature all-together would piss off a lot of people, trust me. But allowing for some customization global or application based would work fine with me.\n"
    author: "Tom"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-19
    body: "Please see <a href=\"http://www.freedesktop.org/standards/clipboards.txt\">http://www.freedesktop.org/standards/clipboards.txt</a> . In short, it says how to use the clipboard the same way in all toolkits, and it also says that Qt2.x uses different way, and that in Qt3.x it will work the standard way ( it's actually not a formal standard, it's just the most logical way ).<br>So ... wait for first Qt3.x based version of KDE ( I think KDE2.2 already will  be Qt3.x based )."
    author: "L.Lunak"
  - subject: "Clipboard specs"
    date: 2003-07-30
    body: "The clipboard specs file was moved to this URL:\nhttp://www.freedesktop.org/standards/clipboards-spec/clipboards.txt"
    author: "mcbrain"
  - subject: "Re: Klipper solves"
    date: 2004-03-12
    body: "I had same problems but surprisingly, KDE solved it. Klipper can manage X clipboard (selection) and KDE clipboard (^X ^C) separately or synchronize them. it has some history (not only) for the case of accidental overwriting. in short, i'm DOS/windows grown so a had some hard time with X clippboard style but klipper solved that all. only issue i can remember is opera which does not support X style clipboard (you see i get used to X style after some time and combined with reasonably long klipper history i think it is superior)\n"
    author: "Hynek \"rADOn\" Fabian"
  - subject: "Re: Help give a voice to KDE"
    date: 2008-01-25
    body: "Fuck KDE, how about that? Got me new asus eee pc. Decided to watch movies from my external hard drive. For some stupid fucking reason KDE renamed permissions on the hard drive to root and now i cant fucking watch shit on it becuase of this dumbshit program. Now I am fucking stuck reading fucking manuals on how to reverse this shit so this piece of shit OS  can allow me to fucking watch my god damn movies. Fuck this shit WINDOWS XP rocks this bitch ass os. When i need ot use the hard drive between windows machines no problem FUCK KDE!"
    author: "Nick"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "well, speaking of sound (I hope an arts developer is looking at this) There has got to be some way to make the arts daemon let go of the /dev/dsp device faster. None of my non-arts compliant apps will play sound when artsd has recently played a kde system sound. This is quite annoying."
    author: "Muythaibxr"
  - subject: "Try a soundwrapper"
    date: 2001-02-19
    body: "You can, for example, wrap the non arTs compliant sound app with artsdsp...\n\nie. artsdsp xmms\n\nWorks for me for most things...\n\n-Robert Dowden"
    author: "Mandrias"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "Sounds and little elegant polish is nice, but reliability and functionality are nicer.  KDE cannot succeed without superior performance and RELIABILITY to Windows 98/2000.\n\nKDE is now an outstanding GUI and the programming involved is the best anywhere.  Please concentrate on KOffice to be better than MS Office so we can finally switch to Linux without regrets or loss of our .docs and .xls!"
    author: "Ed Rataj"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "IF you can make beautiful sounds and you are not a programmer, you can't help koffice team.\n\nSo your post is ridiculous.\n\nIf there are too much persons in a restaurant to be served, I prefer waiting than having food made by the person responsable of clening the restaurant (I don't remenber the word for this)\n\nFrench guy ;)"
    author: "Shift"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-19
    body: "what are you doing here ???\ngo back immediatly to linuxfr ;)))"
    author: "hihimr"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "People, this is kinda the wrong article for the things you write. Please stay on topic!\n\nI'd like to know too, how sounds can be 'made', because i haven't ever done it nor heard about how they're done. Even assuming you have a nice sound editor, how do you start on this? Record something, and edit it with some effects until it sounds nice? How does it work?"
    author: "me"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "I WANT TO KNOW!! :)))\n\nSomebody respond please :)\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-18
    body: "There are two different approaches: either you synthesize the sound from scratch using a hardware or software synthesizer (like aRts  or CSound for example) or you record something with your soundcard. Then you use a soundeditor to give it the right \"shape\", by applying effects, filters, changing dynamics, i.e. making it sound as you want it to be.<br><br>\nFinally, when you have a collection of sounds, you should make them fit together by making sure they all have the same \"loudness\", removing unnecessary silence in the beginning/end and maybe downsample them to make the files smaller. Here's the tradeoff between quality and filesize, you should try to make them as small as possible while keeping them sound well enough.\n<br><br>\nThere are a lot of sound editing tools available for Windows (that's where I last did audio-related stuff a few years ago) -- unfortunately I haven't ever done anything like that on Linux, so I can't recommend any programs for that."
    author: "gis"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-19
    body: "go to http://www.bright.net/~dlphilp/linuxsound/ and see what's available ..."
    author: "AC"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-21
    body: "You shoudn't have given him this link, now the kid will be lost in soundspace forever ;-)"
    author: "reihal"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-21
    body: "<i>You shoudn't have given him this link, now the kid will be lost in soundspace forever ;-)</i><br><br>\n\nI haven't checked the link yet, but now I'm warned at least :)"
    author: "gis"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-27
    body: "It would be a good thing if someone could convince the developers of the software listed in that page, to port to KDE2."
    author: "reihal"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-22
    body: "hmmm\n\nI had been there looking, but none of the software i compile works or seems to be what I want.\n\noh well.. /me codes krpn some more"
    author: "Jason Katz-Brown"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-19
    body: "I would be glad to create some sounds for KDE, but unlike some of you, i am more a sound specialist than a computer specialist. A friend gave me this url because he think i can do something for you.\n\nI have a ton of ideas but I don't know where to put the sounds or themes, once finished. Anyone could explain, or give an url where i can find some info?"
    author: "Nolv"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-20
    body: "Have a look at the kde-multimedia mailing list at <a href=\"http://master.kde.org/mailman/listinfo/kde-multimedia\">http://master.kde.org/mailman/listinfo/kde-multimedia</a> where the multimedia developers should be able to help you out with what to do with sounds or themes you've created."
    author: "marm"
  - subject: "Packaging issue"
    date: 2001-02-19
    body: "Sound events are a great idea, but I'm not big on sounds and usually have them turned off.  Will these be tied in w/ their given pacakages or will they be packaged as kdesounds?  I think thought should be given to the later so that people like me stuck on a 56k (or slower) won't have to download anything that we will not use.  \n-Adam"
    author: "Adam Black"
  - subject: "Sound scheme support?"
    date: 2001-02-20
    body: "I'm all for adding a little bit of extra sound to KDE - now artsd is solid and reliable and doing its job.  But the interface for adding sounds to events is less than top-notch, and, more importantly, there is no way of saving your system notifications as a scheme, like there is in the Colours kcontrol module.\n\nAny chance of getting this fixed and getting the System Notifications kcontrol module polished up a bit?\n\nAlso, there are a few system events that could do with entries in the System Notifications module - for instance, the equivalent of IE's 'Start Navigation' sound, but in Konqueror.  This is a subtle but important addition to IE which Konqueror could use - it really does help the browser's perceived speed, as the sound plays and notifies you that the browser is doing stuff even before the page has started loading."
    author: "marm"
  - subject: "Re: Sound scheme support?"
    date: 2001-02-20
    body: "About the internet explorer sound, I completely agree with you. That's why I hacked it in myself. It really works great IMHO. After 2.1 I'll see if I can get it into a patch so I can perhaps have it commited to the cvs."
    author: "spark"
  - subject: "Re: Sound scheme support?"
    date: 2001-02-21
    body: "<i>But the interface for adding sounds to events is less than top-notch, and, more importantly, there is no way of saving your system notifications as a scheme, like there is in the Colours kcontrol module.</i><br><br>\n\nYou're right. I'll improve the System Notifications control module for KDE 2.2, including adding Sound Theme support."
    author: "gis"
  - subject: "Re: Help give a voice to KDE"
    date: 2001-02-20
    body: "Isn't that (I mean giving voice to KDE) something KDE league should do??"
    author: "Bojan"
---
With the  KDE 2.1 release nearing, major code changes are prohibited. Despite this restriction, we started thinking about how to make KDE 2.1 (or future releases) even more appealing. <a href="mailto:pfeiffer@kde.org">Carsten Pfeiffer</a>, devoted KDE developer, remarked that while people are hard at work polishing the visual aspects,  KDE suffers from a marked lack of sounds and sound effects. 
[<b>Update: 02/18 23:25 PM</b> by <a href="mailto:navindra@kde.org">N</a>:  See also <a href="http://lists.kde.org/?l=kde-multimedia&m=98254474424798&w=2">this mail</a> for more details.]


<!--break-->
<p>
Window manager events, application events, general actions, even games, are in a need for high quality, appealing sound bits. So, we hereby invite all those willing unsung sound artists to contribute a bit of their talent to KDE. If you are interested, please go and subscribe to the <a href="http://master.kde.org/mailman/listinfo/kde-multimedia">KDE Multimedia</a> mailing list, and help get the snowball rolling.
<p>The only criteria  for the sounds you submit are: aesthetic elegance, originality and copyright rightness. Please, make sure your sound bits aren't proprietary; open licenses or public domain are acceptable, as for all things KDE.
<p>Well, come in large numbers, we need your help before the KDE-2.1 release, if at all possible.



