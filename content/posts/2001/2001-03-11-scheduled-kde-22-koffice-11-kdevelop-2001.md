---
title: "Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
date:    2001-03-11
authors:
  - "numanee"
slug:    scheduled-kde-22-koffice-11-kdevelop-2001
comments:
  - subject: "It's been said before, but I'll say it again"
    date: 2001-03-11
    body: "DO YOU GUYS EVER SLEEP?!?!? \n\nSeriously, you are incredible. This reminds me of a quote from somebody, but I don't remember who.\n\nA windows user reinstalls software every few weeks.\nA linux user reinstalls software every few weeks.\nThe difference is with Linux the version numbers change. :)\n\nErik"
    author: "Erik"
  - subject: "Re: It's been said before, but I'll say it again"
    date: 2001-03-11
    body: "No, a windows user reinstalls its windows every few weeks.\nA linux user updates its application to more robust and more functionnal ones every few weeks."
    author: "Christian"
  - subject: "2001?"
    date: 2001-03-11
    body: "Can we PLEASE not call KDevelop 3.0 KDevelop 2001?  This is a Microsoftism.  There is no way to gauge progression between versions with using years."
    author: "Kevin Breit"
  - subject: "Re: 2001?"
    date: 2001-03-11
    body: "Don't worry, that's kind of misleading in the title.  KDevelop 3.0 will not be named KDevelop 2001, the news article refers to the 2001 road map for KDevelop."
    author: "not me"
  - subject: "Re: 2001?"
    date: 2001-03-11
    body: "They never said they would call it 2001. The article show a roadmap for kdevelop in the year 2001!!!"
    author: "RockHound"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "wow i'm very impressed about this work. \n\ni'm so exited about the progress and improvements of koffice. \n\nit could be fact in the feature that it rulez over openoffice/ star office.\n\nbye\n\nsteve qui"
    author: "steve qui"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "C'mon how about XLST in Konqueror for 2.2, some demonstration Kparts -how about a flash player. While your at it HTML 4.01 support, lots of work on CSS 1,2 DOM 1,2, java bindings....and anything else you can fit in would be great."
    author: "Richard Stallman"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "<p>Hmmm? I'm sure you're busy but why not have a look in cvs? I'm not an expert on all of this... I'd just like to see Quanta Plus make some progress on our list and get into one of these distributions officially. Let me take a stab at it.</p>\n<b>XSLT:</b>I'd have to check, but since konq now does support XML directly it seems pretty likely if it's not there it most likely will be soon,<br>\n<b>demonstration Kparts:</b> Gee there are several tutorials linked from the kde developers pages, kdesdk, an open license programming book on KDE 2x at Andamooka, mailing lists and all the code to look at... although perhaps some demos a la QT might be nice.<br>\n<b>flash player:</b> Seriously? KDE has people skilled in flash but since we can already use Netscape plugins we in effect have one... lacking only that it was invented here and open source.<br>\n<b>HTML 4.01 support:</b> No comment... as a web developer I have not even looked at the spec yet to know if I should get excited.<br>\n<b>lots of work on CSS 1,2 DOM 1,2:</b> here is where I really recommend you peruse the cvs on kdelibs. Lots of work <b>is</b> being done here and I might add with excellent results! I am running KDE 2.1 cvs and konq has become the only browser I use and the first on Linux I actually like, not endure.<br>\n<b>java bindings:</b> it's been done. Here's a couple of links regarding it.<br>\n<a href=\"http://dot.kde.org/971418407/\">DCOP bindings for Java</a><br>\n<a href=\"http://dot.kde.org/983311036/\">Interview With Richard Dale</a> on the new (Jan 27) Java bindings.<br>\n<b>anything else you can fit in would be great:</b> Yes, it will. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "I will be happy to have flash support in Konqueror in this release of KDE. At this time, it's annoying to use only Netscape to view pages with flash items."
    author: "M"
  - subject: "Konq has supported flash since 2.0"
    date: 2001-03-12
    body: "see subject"
    author: "steve"
  - subject: "Re: Konq has supported flash since 2.0"
    date: 2001-03-12
    body: "Which plugin?? The macromedia's plugin does not work."
    author: "M"
  - subject: "Re: Konq has supported flash since 2.0"
    date: 2001-03-12
    body: "If it doesn't you're doing something wrong. You need lesstif or openmotif to make it work."
    author: "a@b.c"
  - subject: "Re: Konq has supported flash since 2.0"
    date: 2001-03-12
    body: "In Konqueror 2.0 the netscape plugin worked \nfor me but in 2.1 it doesn't any more :-( \n\nI tried the precompiled binaries for SuSE 7.0 \nand I compiled it myself, but to no avail.\n\nHas anybody experienced the same and solved \nthis problem?\n\nRegards, \ncm.\n"
    author: "cm"
  - subject: "Check your LessTif libs"
    date: 2001-03-12
    body: "Check for nspluginscan.  It should be in /opt/kde/bin or wherever you installed KDE to.  If it's not there, then likely netscape plugin support was not compiled in.\n\nHave you modified anything related to X recently?  I upgraded to XFree86 4.0.2 by removing my old 3.3.6 packages and then blowing out my X11R6 folders.  4.0.2 then installed cleanly, but I had the unfortunate problem of wiping out my LessTif installation which happened to be in /usr/X11R6 somewhere.  Since the kdelibs \"configure\" script didn't find the LessTif libraries, it didn't build in netscape plugin support.\n\nI restored my LessTif libs and then it all worked out.  Watch closely at your configure script.  I have Flash running fine here with KDE 2.1.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Check your LessTif libs"
    date: 2001-03-15
    body: "Yes, I guess that's it.  Thanks.\n  \nThe libs are there on my system at home\n(the one I compiled it on myself), but \nthe header files are missing.  \nSo the nspluginscan won't compile.  \n\nAt work (lucky me, I can use Linux + KDE 2.1 \nthere too :-) )\nthe libs are missisng altogether..."
    author: "cm"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "I believe that there is an open source flash player available, but I am not sure as to the level of quality. Check out <a href=www.swift-tools.com/Flash/>this site</a>"
    author: "Carbon"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "Oops, sorry, accidentally set that link as relative. Its actually <a href=http://www.swift-tools.com/Flash/>this</a>"
    author: "Carbon"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "I would LOVE to see a freeware version of Flash available through KDE.  I use Solaris 7 x86 and there is no Flash plugin available for it.  It's only available for SPARC.  That would rock if it were ever completed!"
    author: "Steve Nakhla"
  - subject: "TROLL"
    date: 2001-04-26
    body: "Was the last post a troll ?"
    author: "Bryan Hunt"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "Don't know whether it would be appropriate to include it, but how about some good Instant Messengers integrated into KDE?\n\nKICQ is pretty good, but development doesn't seem to be progressing very much at the moment due, i expect, to it not being very well known.\n\nA good ICQ clone, that is a full featured as Mirablis' Windows version (but lacking the ads *g*) is something that's missing from Linux at the moment."
    author: "R,H"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "I think the most popular and advanced ICQ client is licq. Perfect integration into KDE2. \n\n<p>Was one of the first programs using QT2 I know (before KDE!).\n\n<p>Check <a href=\"http://www.licq.org\"> http://www.licq.org</a>. A new release has been released last week (1.03)"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "I would second the licq comment, it's better than the Windows version in my opinion because it doesn't include all the rubbish that I don't use.  Most of the Windows ICQ client is dead weight."
    author: "Mark Dickie"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "Modulise it?\n\nJust build a core system for sending messages, and have plugins for things like SMS, file exchange, chat sessions, etc.\n\nAs long as the functionality of the Windows version *can* be there if you want it i don't mind."
    author: "R,H"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "I've used Licq, but i can't say i ever really liked it very much and it lacked the features of Windows ICQ.\n\nCall me weird, but i like programs to have a certain feel to them, and Licq never felt quite right compared to ICQ on Windows.\n\nKICQ does have pretty much the same feel to it, and also integrates well into KDE2 (even down to the little flower icon in the system tray).  So an integration of this (or Licq) into KDE with *all* the features available on Windows would be great.\n\nWhat i'd ideally like is to be able to say \"right, no more Windows at all\", and then import everything over from there into programs that look and feel the same as their Windows conterparts with no loss of functionality.\n\nAt the moment i'm still having to use Windows for some things because i haven't found a comparable product for Linux.\n\nEudora for email (how i wish they made a Linux version), because while KMail is good, it's not got the same feel or functionality to it (Aethera will though once it's working properly).\n\nGravity for Usenet, because there simply isn't a graphical client in the same league on Linux.  PAN shows promise, but i've started thinking it wont ever be up to the same standard.\n\nAnd of course a good quality WYSIWYG web tool.  The direct source editing ones available for Linux at the moment are great, but i change my mind so often it's easier to just drop bits and pieces where i want to see the look of it.\n\nI've no doubt that by the end of the year i will not be tied to Windows in anyway, but at the moment i'm stuck."
    author: "R,H"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "Though some Windows programs do have nice and functional interfaces, you ought not to switch to Linux and expect to find exactly the same interfaces and software.\n\nDevelopment of the same software for completely different platform (since that's what Linux is compared to Windows) is absurd at best. That would be pointless and \"reinventing the wheel\", while we could have all those developers introduce new, and probably better concepts into desktop area.\n\nJust because you're not ready to learn some new ways of working with a PC doesn't mean that your ways are the best. It's all about choice. So, take Windows, or don't. Do not try to take Linux and hope of it to become Windows. That's not the goal.\n\nAnd it shouldn't be.\n\nAnd by the way, in Linux world, if you feel some application lacks some features, go ahead and write them yourself. That's what GPL philosophy is all about. It's not stability of Linux that made it a success (I found NT4 quite as much stable as Linux is).\n\nAnd last (but also the least), you may disagree with some or all of my points. But I'll still hold to it. (Note: Linux should be made easier to use for some people, but that doesn't neccessarily mean it should be made \"Windowsish\").\n\nThat's all folks."
    author: "Danilo Segan"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "You misunderstand me.\n\nI don't want to make Linux into another Windows, i would like to see the best that Windows (applications) has to offer available under Linux.  And i'm very open to learning new ways of working with a PC.\n\nI agree that development of the same software for different platforms isn't always a good idea, but somethings are universal concepts.\n\nTake for instance email clients.  While there are a lot of graphical clients on Linux that can handle the basics, an email client needs more than that.  Enhanced features like multiple identities/accounts, powerful filters, a multitude of formats, etc.\n\nIMO Eudora does this best on Windows, so while not reinventing the wheel, what's wrong with taking the best features from it and integrating them into a Linux client?\n\nI do take your point about developing things myself if i find something lacking.  Trouble is i lack all creative skills.  I'm more an idea person.  I have tons of ideas but lack the ability to make them a reality.  If i had the ability believe me i would do it."
    author: "R,H"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "Maybe it would be worth looking at Wine? It's runs some Eudora versions perfectly. I think that Wine is the middle ground that would help a lot of people that feel the same way you do.\n\nOf course once people switch to Linux on mass, companies can re-compile their apps using winelib and hey presto a linux version."
    author: "David"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "I don't think that's a very fair response. RH's views are very valid from his point of view and from where he's coming from.  Basicly, telling him that his views suck .. which is what you just did in \"pretty\" language is not very positive.  \n\nIt takes more courage to listen to someone elses conflicting views and to accept they are valid in that persons context, than it does to attack them!\n\nYou are in fact both right, because you (most likely) come from different learning experiences and have different expectations.  I would guess that you're both here on The Dot because you see something in KDE that appeals to your own needs.  RH, is quite in scope in expressing and asking for what he has. \n\nI hope that KDE continues to be as appealing to both of you.  \n\nBtw, before brandishing the GPL like that you need to remember one thing - that those who have the skills to code their own apps are in the minority.   Someone could drop a private pilots license in my lap tomorrow giving me full freedom to take to the sky on my own.  While I may feel empowered, it would be as much use as a chocolate coffeepot - because I can't fly.  Nor do I have the time or incentive to learn."
    author: "Macka"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "I think your confused, Linux is just an OS, KDE is just a desktop, Qt an API.  There's no reason that the software should be difficult to use just because it's linux/KDE software.  I think he has a valid point that until Linux gets a good chunk of software that is up to par with what people are used to using on windows it realy won't ever become popular on the desktop, and you can't just tell people to write a full featured mail reader or news reader, it's not even possible if they tried.  Also <I>\"Development of the same software for completely different platform (since that's what Linux is compared to Windows) is absurd at best\"</I>, you must be on crack, that's like saying you don't even need a mail reader then, and theres tons of cross platform APIs, people just don't use them.  And finaly the UNIX philosophy is to wrap stuff(or flexibility in depth), make something flexible then put a easy to use interface around it(like a GUI)."
    author: "Robert"
  - subject: "Live a little.."
    date: 2001-03-12
    body: "I think he actually meant that it's okay for KDE (or Linux/Unix) to be different.  MacOS does not operate exactly the same as Windows, nor do their applications.  There's nothing wrong with being different.  Sometimes it's a good idea to rethink a design and make it better.  However, there is a classic example of people stuck in their ways:  The kwin title decoration.\n\nThe early betas of KDE2 (remember all those silly names like Kleopatra? hehehe) had a default decoration which placed the 'Close' button at the top left.  Presumably, this confused the daylights out of users who were used to other window managers and probably any \"Windows Converts.\"  The original debate was that the Close button made more sense to be far away and alone, rather than a pixel away from other useful, but non-destructive buttons.\n\nIt was a little strange at first for me, but I understood the programmers' intent so I figured I'd get used to it.  Well I ended up really liking it.  It became second nature to go to the top left when I want to close an application.  But then suddenly with the release of 2.0, the button was back on the top right!  What was this?  Too much pressure from people unwilling to switch their ways.\n\nFortunately, KDE2 offers many decorations to choose from, and I soon found out that my decoration of choice was hidden in the list as \"Laptop\".  In the end, it's really all about choice, and I am happy that it was still included.  So everyone can do it their own way and be happy.  I'm kind of disappointed that it's not the default though, as I think people should be exposed to new ways of doing things.\n\nThere's no reason why featureful easy-to-use programs cannot be created with KDE/Qt.  In fact, for the most part they are.  I just don't think they necessarily need to be exactly the same as their \"Windows counterparts\".  People really get hooked on certain applications (in this case, Eudora).  It is my hunch that these types of people won't be satisfied without an actual unix port of of the application.  I say take the plunge and try some new things for a change.  That is the whole reason for switching to Linux, isn't it?  Just trying to escape Microsoft should not be reason enough.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "Jabber IM is much beter than icq KDE cane beter denfelop a good KDE jabber client"
    author: "Gerrit"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "I agree with this, we should be supporting jabber and not just because it is better, but because it is an open standard. Also it allows messages to be sent to all the other networks."
    author: "Mark Hillary"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "I was thinking the same when I read the comments about ICQ... A Jabber client is the only IM that should come as standard with KDENetwork. It makes no sense to have 5 different apps for each network."
    author: "David"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "I like kicq the best but thats just my opion.\n\nCraig"
    author: "Craig black"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "How about konverse the KDE jabber client, it still pretty basic but the ICQ transport works, Jabber is the wave of the futere dont you think?"
    author: "coba"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-11
    body: "I hope anti aliasing for icons makes it into kde 2.2 that would be real sweet. \n\nCraig"
    author: "Craig black"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-17
    body: "it made it into 2.1, actually - turn it on in kcontrol (in the icons app, turn on the alpha channel or something similar).\n\nSorry i don't recall the exact option - I also thought it was on by default now, but who knows."
    author: "Kevin Puetz"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "Well... yeah this is all great, but. If you look at the facts, you don't have many apps to do your work completly. On the other hand there are thousands unfinished or half finished apps, even some in KDE.\nMany apps are doing everything but the thing that they are supposed to do.\n\nOn Linux you don't have any design or multimedia tools that could be compared to those from SGI, Mac or Win.\nI'm a realy diehard Linux user, I'm a designer, author of several Linux sites, coordinator of KDE translations to Serbian lang. but I was so dissapointed when I've discovered a program called Rhinoceros for Win... wich is comercial, and it's size is 12Mb. It is a perfect 3D modeling program, it's small, very fast and functional. I can bet that it was developed by a very few people, but who knew exactly what they wanted, and they got it.\n\nI like KDE, and this is all good thing, but I think there are some realy bacic parts missing.\n\nP.S. One of the programs I realy admire is Kbabel, it is realy one of the best apps for Linux. It does everything it is supposed to do :)\n\nP.P.S. Critics are always more productive than boasts."
    author: "Marko Rosic"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "Rhino has been in development for some 5 years or something, is the pet product for some major company, and frankly (and I've used it) is useless for doing any kind of advanced modelling at all. No IK, no decent scripting, no plugins. No nuthin'. Next!<br>\n\nSeriously, take a look at blender. It may not be open source, and it may not be a KDE app, but it works quite well. At least it's free as in beer, and uses Mesa3D, an OSS library. Finally, advanced multimedia and 3d modelling is not really a basic part, its an ultra-specialized system.<br>\n\nAlso, constructive critisicm is great! OSS is awesome because it's users make it that way by telling the developers what they want, and making sure it gets in there. However, it's more useful to the developers if it's organized and placed correctly. Please, find some half-finished kde modelling app, and add some messages to their bug database telling them exactly what you want!"
    author: "Carbon"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-12
    body: "<p>My post is rather general. I can write a very huge post or whatever with exactly described issues that are missin, or not very good in many programs for Linux (or Win).\nThis is not a list for that kind of posts, on the other hand I don't have enough time to deal with everything :((\n\n<p>I have tried Blender, and ... I'm not very impressed by it. No real documentation, no useful tutorials (yes I know there are some). Rhino is very good modeler, it can do some very good stuff, but it's very bad for rendering, no anim etc. I have learned to use it from it's three tutorials.\n\n<p>Other missing things:\n<ul>\n<li>No Photoshop (Don't even try to mention Gimp)\n<li>No FreeHand or Illustrator\n<li>No Quark of InDesign\n<li>Houndreds of media players but none that can compare to WIn media player, or QuickTime, or some other players.\n</ul>\n\n<p>As I said I like KDE, I wouldn't even write this or work in Linux if I think that Linux has no future, and I realy think that KDE has potentials to make things different.<br>\nWe have to try to make things better and faster, to look at the priorities, and I have offered my help to some developers in some parts that I'm expirienced."
    author: "Marko Rosic"
  - subject: "Photoshop?"
    date: 2001-03-13
    body: "You know, everytime someone brings up Photoshop vs. GIMP, I have to wonder:  Does everyone on the planet have a legal copy of Photoshop?  You'd think so by the way people talk.\n\nGIMP is completely free, but that's almost never part of the argument.  Is Photoshop really _that_ obtainable?\n\nThis is mostly a sarcastic observation =)\n\n-Justin"
    author: "Justin"
  - subject: "Re: Photoshop?"
    date: 2001-03-14
    body: "Quality and features are issue.\nIf you make money out of something, and most people who use PS does. Money isn't an issue, because you can earn $600 per cheapest web site.\n\nI don't think Linux would be this popular or even some serious competition to others if it isn't a very good OS, even if it's free.\n\nKDE is also much beter in many issues than gnome, and both of them are free. They both have some better fetures than Windows or maybe others.\n\nSo some software can be good or bad, weather it is free of commercial."
    author: "Marko Rosic"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-14
    body: "Did you check K-3D out? Despite the name,\nit's not a KDE app, but seems to be very\ngood... Haven't tried it yet myself, but\nfrom what I see it appears to work \"today\"."
    author: "LodeRunner"
  - subject: "This site does not render in Konqueror , but in IE"
    date: 2001-03-12
    body: "www.n0sle3p.de"
    author: "web"
  - subject: "Re: This site does not render in Konqueror , but in IE"
    date: 2001-03-12
    body: "Please report this as a bug. This is not the right place to post bug reports to. Please use \n<a href=\"http://bugs.kde.org\">bugs.kde.org</a> instead.\n\nDaniel"
    author: "Daniel"
  - subject: "Code completion for KDevelop"
    date: 2001-03-12
    body: "As far as I understand, KDevelop people are going\nto use the code completion parts from KDEStudio.\nIMHO that is not the best approach. KDEStudio\nuses some pretty clean but outdated C++ parser\nwritten in C (I don't think that this parser was \nwritten by KDEStudio authors due to many reasons\nlike that it's code is much cleaner than\nthe remaining KDEStudio code). This parser\nrequires preprocessed C++ code as input, and\nKDEStudio obtains it by\ngcc -S -save-temp -o /dev/null (!!!). As\nresult, the completion is *VERY* *VERY* *VERY* slow.\nMaybe it would be better to use the existing\nKDevelop class parser, and borrow only the code\nthat determines when and how to initiate\ncompletion mechanism. BTW, though KDevelop's\nclass parser (1.4) seems to be OK, the class browser\nis broken. I can't see new member functions\nimmediatelly after adding them using \"Add\nmember\", but after saving the header file\ncontaining class definition, sometimes I get\ntwo items for each new member, one of which\nlooks pretty strange (e.g. \"void myfunc()\" and\n\"void myfunc()( )\"). Also sometimes a removed\nmember function doesn't disappear at all before\nI reload my project. Another reason why I\nstill prefer Emacs over KDevelop is lack of\ngood indentation mechanism (like Emacs' one)."
    author: "Ivan Shvedunov"
  - subject: "Re: Code completion for KDevelop"
    date: 2001-03-15
    body: "KDEstudio uses ctags."
    author: "KDEguy"
  - subject: "Re: Code completion for KDevelop"
    date: 2001-03-24
    body: "I recently downloaded KDE Studio to review to code completion - it doesn't use KCompletionObject which would seem to be the obvious way to do it - perhaps a combination of ctags and KCompletion would be best. The KDevelop class parser has bugs, but I find it well written and straightforward to follow.\n\nThe class parser refresh problem is tricky to solve.\n \nThe KDE Studio class parser didn't look as though it would be as easy to work on - eg written in C, no flex grammar. The version of kwrite that they use could also use some patches - it automatically copies text to the pasteboard when it is selected. I would need to fix that and make it optional, as in the KDevelop one, before I use it.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Scheduled: KDE 2.2, KOffice 1.1, KDevelop 2001"
    date: 2001-03-13
    body: "Anyone know of plans for solaris packages?\n\nI mean, I like GNOME and all, but I prefer KDE, *especially* after 2.1!\n\nWhat about experiences building from source? Using CVS? \n\nThanks,\n\nnate"
    author: "nolla"
---
Various release schedules have been announced over the past week.  First, Waldo Bastian, the release dude for KDE 2.2, <a href="http://lists.kde.org/?l=kde-devel&m=98412533218670&w=2">announced</a> the <a href="http://developer.kde.org/development-versions/kde-2.2-release-plan.html">KDE 2.2 release plan</a>. Incidentally, many more improvements than those mentioned in the schedule have already gone into <a href="http://lists.kde.org/?l=kde-cvs&r=1&w=2&b=200103">CVS</a>. David Faure, the release dude for KOffice 1.1, <a href="http://lists.kde.org/?l=koffice-devel&m=98391291407370&w=2">followed</a> with a <a href="http://developer.kde.org/development-versions/koffice-1.1-release-plan.html">KOffice release plan</a>.  Finally, Ralf Nolden announced the <a href="http://lists.kde.org/?l=kdevelop&m=98352993506471&w=2">KDevelop Roadmap</a> for 2001.  Included are plans for versions 2.0 and 3.0.  Those interested in helping with KDevelop 3.0 should also read this <a href="http://lists.kde.org/?l=kdevelop&m=98379930132535&w=2">note</a> by Omid Givi.



<!--break-->
