---
title: "KDE 2.2.1/3.0 Release Schedules Updated/Posted"
date:    2001-09-09
authors:
  - "Dre"
slug:    kde-22130-release-schedules-updatedposted
comments:
  - subject: "Fast pace"
    date: 2001-09-09
    body: "I think I'm in love with this quick one-release-a-month way of doing things.\n\nKeep up the good work!!!"
    author: "Simon Perreault"
  - subject: "Re: Fast pace"
    date: 2001-09-09
    body: "It is too fast...\nI even haven't have chance to upgrade to KDE2.2 and planning to do it this week until I read this news of KDE 2.2.1 release.\nQuestion: If I Install the KDE 2.2 now, is it easy to upgrade it KDE 2.2.1 or Do I need to download again all the libraries (20 packages or more)? Should I wait for KDE 2.2.1 instead?"
    author: "jamal"
  - subject: "Re: Fast pace"
    date: 2001-09-09
    body: "> Question: If I Install the KDE 2.2 now, is it easy to upgrade it KDE 2.2.1 or Do I need\n> to download again all the libraries (20 packages or more)? Should I wait for KDE 2.2.1\n> instead?\n\nBetter wait until 2.2.1 is out. It fixes several bugs. For several reasons discussed >1000 times we do only provide the tarballs as a whole and most packagers create completely new RPMs, so there won't be a smaller \"upgrade version\".\n\nCheers,\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: Fast pace"
    date: 2001-09-09
    body: "<P>Hmmm...this is only a rant/flame but if installing new updates is such a complicated matter with your distro, you can always switch to Debian and have everything done for you.\n<P>P.S. Daniel, \"we do only provide tarballs\" is not very grammatical.  I believe that should read \"we only provide\" and the do is implied.  [Major flame/rant]"
    author: "Robert Tilley"
  - subject: "Re: Fast pace"
    date: 2001-09-09
    body: "I tried Progeny Debian last week, and the \"latest\" release of KDE for Progeny was 2.0. I found a site where I could get KDE 2.1, but it didn't have instructions on how to install it from within Debian.\n\nI've been running SuSE Linux 7.2 for a while, and have KDE2.2 installed from several very nicely packaged RPMs, available from SuSE's LinuKS (Linux KDE Service) site.\n\nDebian itself seems to be fine, but in order to get the latest and greatest, you almost always have to compile from tarballs.. so why even bother with Debian in the first place?\n\nThis is a serious question, not meant to be a flame or a cutdown on Debian.. I simply want to know.. with Debian having this great package management system.. how come the latest and greatest software isn't available on Debian when it is for other Distros, like SuSE, RedHat or Mandrake?\n\nThanks,\nSteven"
    author: "Steven"
  - subject: "Re: Fast pace"
    date: 2001-09-09
    body: "It's not?\n\nmerlin:/home/spark# apt-cache show kdebase\nPackage: kdebase\nPriority: optional\nSection: x11\nInstalled-Size: 16456\nMaintainer: Ivan E. Moore II <rkrusty@debian.org>\nArchitecture: i386\nVersion: 4:2.2.0.20010822-1\n\nYou just have to use the \"unstable\" branch, easy as that.\nAnd no, you system won't become unstable because you do that. ;)\nIt's just that you will NEVER find anything in stable that isn't tested out for months and you'll never find anything in \"testing\", that is just packaged (A few days after usually).\nSo use unstable but don't do a dist-upgrade, just fetch all the sotware you need.\nYou'll get all the latest and greatest packages, nicely packaged by over 500 dedicated maintainers, mostly a few days (or even faster) after the source is released.\nKDE and Gnome packages are usually available at the day of their release.\nI never have to compile from source, unless I want to try out a CVS version or a 0.0.1 app in development."
    author: "Spark"
  - subject: "Re: Fast pace"
    date: 2001-09-09
    body: "It's not?\n\nmerlin:/home/spark# apt-cache show kdebase\nPackage: kdebase\nPriority: optional\nSection: x11\nInstalled-Size: 16456\nMaintainer: Ivan E. Moore II <rkrusty@debian.org>\nArchitecture: i386\nVersion: 4:2.2.0.20010822-1\n\nYou just have to use the \"unstable\" branch, easy as that.\nAnd no, you system won't become unstable because you do that. ;)\nIt's just that you will NEVER find anything in stable that isn't tested out for months and you'll never find anything in \"testing\", that is just packaged (A few days after usually).\nSo use unstable but don't do a dist-upgrade, just fetch all the sotware you need.\nYou'll get all the latest and greatest packages, nicely packaged by over 500 dedicated maintainers, mostly a few days (or even faster) after the source is released.\nKDE and Gnome packages are usually available at the day of their release.\nI never have to compile from source, unless I want to try out a CVS version or a 0.0.1 app in development."
    author: "Spark"
  - subject: "Re: Fast pace"
    date: 2001-09-09
    body: "Jep, Debian is just GREAT for KDE. :)\nNever had such a nice KDE installation and it was never that easy.\nThat was the first time I did with Debian back in the good old days of KDE 2.0 and that was the reason why I fell in love with Debian. :)\nBut this doesn't change the problem that upgrading everything KDE will be a pain for modem users. :) Especially if they have to pay for their onlinetime. \nFortunatly, that shouldn't be many anymore. :)"
    author: "Spark"
  - subject: "Re: Fast pace"
    date: 2001-09-10
    body: "The main problem I have with Debian's KDE packages is that by default they install Debian's menu items in the same menus as the KDE-only stuff. I prefer to separate KDE apps from non-KDE ones. Similarly, I somewhat dislike how everything installs in /usr/share instead of like /usr/kde or something like that, but I suppose it's in the spirit of the FSSTND.\n\nMinor gripes though. Anyway, keep up the good work, KDE and Debian!"
    author: "Rakko"
  - subject: "Re: Fast pace"
    date: 2001-09-10
    body: "Actually, it's in the spirit of the FHS :)\nIt's been a pain to complete the transition, lets not go back again :P"
    author: "Oskuro"
  - subject: "Re: Fast pace"
    date: 2001-09-10
    body: "Hmm, for me Debian puts it's own menus in submenus, like \nUtilities -> Debian -> Application\nThat's pretty handy.\nDon't know why though, maybe it's a different \"menu\" version.\nAnd if you don't like this at all, you can just do a \napt-get remove menu\nThis won't hurt you much.\n\nAbout /usr, that's a matter of taste. I for one HATE seperate directorys for every desktop. I mean, usually 90% of my applications are from one desktop, so this is not ONE part, it's THE part of the system, that's why it should be in /usr and not /usr/kde or even worse /opt/kde! This is just causing incompatibilities."
    author: "Spark"
  - subject: "Re: Fast pace"
    date: 2001-09-23
    body: "Oh yeah, that's what I meant: Debian icons go in submenus of the KDE menus. I prefer to have a \"Debian\" submenu off the K menu and put all that stuff in there. Mostly just because most non-KDE, non-GNOME apps don't have icons (and the ones that do don't look nice like KDE ones), and I hate the look of a menu full of nice KDE icons mixed with \"unknown\" icons :)\nYes, just a matter of taste.\n\n[And yeah, I meant FHS, not FSSTND. I knew it had a different name but was too tired to think of it :)]"
    author: "Rakko"
  - subject: "Re: Fast pace"
    date: 2001-09-10
    body: "Actually, I don't like it and I have several reasons why.\n\nFirst:  Stability.  I like a cutting edge system, but I also want it to work.  With only a month for programmers to find and fix bugs, releases get out that are not exactly stable.  Also, it doesn't give a lot of time to optimize code.  You can get it to work, but will it work fast and efficient?\n\nSecond:  Upgrading.  I would rather wait until a program has been fully implemented than to see little bits and pieces of improvement or fixes as releases go by.  Its progress, but it doesn't seem worth going through an upgrade.\n\nThird:  Time.  For nine out of 12 months I live on a college campus and have a blistering fast T3 connection.  For the other 3 I live off campus and have a 56k dialup modem.  Pretty soon I plan to live off campus 12 months a year (all year) and will have a 56k modem until broadband prices fall and it becomes available in whatever area I will live in.  That said, it will take me quite some time to download the tarballs for a release.  Compiling, too takes some time (even on a 1 gig Athlon).  So I would want to actually spend more time in the windowmanager than getting it and compiling it.  My grandmother is quoted by saying \"I don't want to spend the rest of my life in front of a computer when there really isn't much left.\"  She's pushing 70 years now and is moderately healthy, but you can understand her argument.\n\nFourth:  Time.  This is different in that it relates to the developers.  This is free software and so any of the developers develop as volunteers.  Consequently, I don't wish to rush them or crowd their lives anymore than they might already be because this college kid wants pretty fonts on a free OS.  Also, I soon will be among the list of developers around the world, and intend to contribute to the open source community; still, I would consider it a secondary activity (if not a hobby), presumably as do many of the developers of KDE and its app family.  Hobby should not take precedence over making a living.  That may sound overly capitalistic, but \"whoever said money is the root of all evil obviously never went hungry.\"  I forgot who said that, maybe it was a rapper, maybe it was Murphy.\n\nSo thats my opinion, not meant to be inflammatory.  Maybe I will be one of the KDE developers one day, or will develop a really cool KDE app.  Only time will tell."
    author: "Greg"
  - subject: "KDE.com"
    date: 2001-09-09
    body: "Is Dre colour-blind? Or am I?"
    author: "reihal"
  - subject: "Re: KDE.com"
    date: 2001-09-10
    body: "The jury's still out on that one ;-)"
    author: "Steve Hunt"
  - subject: "New features in KDE 3"
    date: 2001-09-09
    body: "\u00bfWhere can I suggest new features for KDE 3? I have read the announce and it seems not have many changes ...\n\u00bfWhat about a little redesign of the user interface (kpanel, i.e.), like XP (XP is beautiful, and MacOS X is more beautiful ...)"
    author: "Gabriel"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-09
    body: "It's Kicker, not kpanel, and beauty is only skin-deep."
    author: "reihal"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-09
    body: "But in a GUI, is it not that skin-deep beauty that is vital?"
    author: "dingodonkey"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-10
    body: "That's like saying:\n\n\"Look a my Ferrari, it doesn't have an engine, but it looks great!\""
    author: "Vindi"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-10
    body: "but you buy a Ferrari for its appearance, not (only) its engine..."
    author: "john"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-10
    body: "but you shouldn't have one without the other, else both are pointless!"
    author: "Vindi"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-09
    body: "They're called themes.\n\nI wouldn't like KDE to look like XP's Luna, or MacOS's Aqua. It needs to at least be original in some way. And might I use this chance to be on-topic to say that I think it's childish how MS made a badly-done copy of the Aqua gui? You should check out XP's gui before they released Luna, and after. Completely different styling. It's not at all that surprising that Luna (just the name is a rip-off) showed up a bit after Aqua. \n\nBesides, I don't even like Luna. I think it's ugly (it doesn't have sheens, which are a core element of the liquid effect)."
    author: "Joeri Sebrechts"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-10
    body: "But you *can* make KDE look like Luna or Aqua.  Well, I don't know about Luna, but Aqua for sure, which is available in several incarnations on kde.themes.org.  :)"
    author: "surak"
  - subject: "Mosfet's Liquid engine"
    date: 2001-09-10
    body: "... or even better than those slow pixmap-based renderings of Aqua at themes.org, go to mosfet.org and download his Liquid style engine :) Luna-style window decoration coupled with Aqua.\n\nMichel"
    author: "Michel"
  - subject: "Re: Mosfet's Liquid engine"
    date: 2001-09-11
    body: "I always hated all available KDE styles and themes, but I immidiatly fall in love with Liquid. :)\nIt does not even compare to every other Aqua or Luna clone I've ever seen. It's so much better! It feels like a \"real\" and \"native\" style, not a clone or something.\nAnd it's unbelievable fast.\n\nHere is my desktop:\nhttp://home.wtal.de/borgmann/konqueror.png\n\nNote that the menu on the top is for some weird reason shown BEHIND the menu bar in this shot. Usually it's not so it looks better in reallife. ;)"
    author: "Spark"
  - subject: "Re: Mosfet's Liquid engine"
    date: 2001-09-14
    body: "Is Mosfet's Liquid Engine like a window manager? Is it like Window Maker (or Sawfish, etc), or a copy of KDE with a new look?"
    author: "Sarah Gould"
  - subject: "Re: Mosfet's Liquid engine"
    date: 2001-09-14
    body: "It's a style engine for KDE, Sarah. That means it changes the appearance of Qt and KDE apps for you - just like you can choose to have your KDE desktop look like Mac, Windows or Unix currently.\n\nBasically it consists of:\n1. a set of window decoration for KWin, the KDE window manager\n2. a style engine to change the appearance of the widgets (buttons, scrollbar etc) of Qt/KDE apps\n3. a colour scheme to switch the colours\n\nPretty amazing stuff! GNOME won't have easily-switchable colour schemes until GNOME 2 next year.\n\nRegards,\n\nMichel"
    author: "Michel"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-09
    body: "You should try Mosfet's High Performance Liquid theme."
    author: "Greg"
  - subject: "Eye candy =)"
    date: 2001-09-09
    body: "Yes, that's pretty damn cool (I use it currently), but I think he didn't speak about the style that is themable, but about fixed elements like the panel or the startmenu. Or even the login menu... \n\nI would also suggest to work on the desktop/filemanager part.\nSome ideas of Nautilus are pretty good. For example the transparent and colored select box when dragging the mouse while mousebutton is held. That's not just very nice to look at, but also easier to the eyes than a thin black line. :)\nI also like how Nautilus renders Text below items. Very nice. KDE needs some improvement in this area.\n\nAlso missing is transparent moving od icons. I thought that was already implemente? I remember something like that, but can't find it in KDE 2.2. This does not just look nice, but it is VERY usefull to exactly see where you are placing your icon. :)\n\nSomething I do NOT like about the current KDE optic is, that it always and everywhere renders thin black lines! What's up with that? For example: I select an icon and click on the desktop to deselect it. Now their is a small black border around the icon... why? To show me that I clicked this last? It just looks ugly.\nThis also happens to buttons and many other widgets. \nPlease remove that! :) There is also a lot of weirdness when clicking and moving things. For example, when dragging an item, there are boxes drawn around the item and the text... why?  It even lacks behind the actual rendering of the item... very ugly. :) Selected items do not look nice as well, that could be improved.\n\nYou see, there is a lot more about eyecandy than GUI-style and window decorations...\n\nSome other ideas:  When drawing the menu on top of the screen, can't you (optionally) place a shadow below it like Mac OS X?\nOr even better, implement those shadows (optional) for every window and probably even icons?\nThat would just rock. I know that it would be quite a performance hit, but maybe this would force the lame XFree developers to come out with this alpha blending extension soon!\nIt would also be great to have real transparent windows (while moving or for backgrounds of some widgets), but I think that would be way to slow without native support by Xfree. :(\n\nIt would be great if someone could port Qt and KDE to DirectFB.\nI know that people don't want to abandon Xfree, so it would be great if KDE would only depend on Qt, so it could run on every plattform that Qt supports and you could run this either on Xfree or on Linux framebuffer.\nQt/Embedded looks so great... why do our embedded devices have more eye candy than our desktop computers?? :) That's not fair."
    author: "Spark"
  - subject: "Re: Eye candy =)"
    date: 2001-09-10
    body: "XFree already has alpha, with the xrender extension. Or am I mistaken?"
    author: "Carbon"
  - subject: "Re: Eye candy =)"
    date: 2001-09-10
    body: "Has it? I don't think so.\nAFAIK transparency tricks like the Liquid menus or the transparent windows while moving in Enlightenment still have to be done by copying the screen to a buffer and using this as the background. :(\nThat's why this Enlightenment windows always stop for a second before you can move them transparently. This sucks.\nAlpha blending could be used for so many things...\nX is not really the greatest thing for a desktop.\nI would gladly switch to DirectFB for desktop rendering and user X only for 3D games anymore.\necho \"exec quake3\" > .xinitrc\nstartx\n:)"
    author: "Spark"
  - subject: "Re: Eye candy =)"
    date: 2001-09-10
    body: "Hmm, lack of one eye-candy oriented feature (that's already currently in devel) is hardly reason to switch entirely to FB. FB is good, but X has quite a bit going for it, like network windowing support, and acclerated 2d."
    author: "Carbon"
  - subject: "Re: Eye candy =)"
    date: 2001-09-10
    body: "Not really.  Xfree really doesn't have that much 3d support, nor hardware 2d.  I know this is a complicated thing, you have to implement OpenGL or Mesa to even have and hardware acceleration in Xfree.  Still, my video card, an S3 Savage4, that is has hardware accelerated 2d and 3d cannot do hardware acceleration because it is only implemented for 3dFX, Matrox, and NVidia chipsets.  What about the rest of us?  If I had full hardware accel. support for my board, X and KDE would no doubt run so much faster, but as I don't they are noticeably slower compared to win 98 with hardware accel. support from the manufacturer.  Absolutely no 3d games for me until Mesa can fully support my card's chipset."
    author: "Greg"
  - subject: "Re: Eye candy =)"
    date: 2001-09-11
    body: "Accelerated 2D is MUCH better with DirectFB! They even have a DirectFB X-Server, which outperformces XFree in benchmarks. Couldn't try it yet.\nXFree has more drivers and better 3D support. Drivers will come and 3D support is not important for the desktop (I could still launch XFree from within the DirectFB desktop for the game).\nThe network support is something, that many users don't really need. :)\nIt would be great, if KDE could just run on \"Qt\" (not Xlibs) and Qt would run natively on DirectFB, so you could choose your environment.\nX is really not everybody's taste.\nOr their could be something like a \"layer\" between Qt, the hardware/operating system and KDE, so you just had to port this layer (along Qt) to other plattforms if you want to run KDE on them.\nKDE could become a kernel independent operating system, like Qube or Athene. :) That would really rock."
    author: "Spark"
  - subject: "Re: Eye candy =)"
    date: 2001-09-11
    body: ">KDE could become a kernel independent operating system, like Qube or Athene. :) That would really rock.\n\nEh, I dunno about that. There's a whole lot more code and work in between a DE and a full OS, and the KDE developers are working hard enough as it is. On the other hand, perhaps a Linux distro dedicated to nothing but KDE (and not a crappy commercial one either, but an open source distro) would be very cool."
    author: "Carbon"
  - subject: "Re: Eye candy =)"
    date: 2001-09-10
    body: ">For example: I select an icon and click on the desktop to deselect it. Now their is a small black border around the icon... why? To show me that I clicked this last?\n\nNo, it's for people who like to use the keyboard.  That line indicates where the current focus is so that you can move it around with the keyboard.  Win2K has a very nice option that turns off these keyboard hints until you press an arrow key.  Yet another option for KDE's overcrowded configuration panels.\n\n>maybe this would force the lame XFree developers to come out with this alpha blending extension soon!\n\nThe render extension is already out.  That's what gives us the wonderful anti-aliased text.  Alpha-blending is already used in various places in KDE.  I think with QT3 support for alpha-blending will become more universal, but I'm not sure.\n\n>Qt/Embedded looks so great...\n\nWhat you aren't seeing in the screenshots is the fact that QT/Embedded is not hardware accelerated, which makes it very slow.  QT/Embedded was never meant to replace X for normal usage."
    author: "not me"
  - subject: "Re: Eye candy =)"
    date: 2001-09-11
    body: "> No, it's for people who like to use the keyboard. That line indicates where the current focus is so that you can move it around with the keyboard. Win2K has a very nice option that turns off these keyboard hints until you press an arrow key. Yet another option for KDE's overcrowded configuration panels.\n\n1) They should look nicer. :)\n2) That's a nice idea. Why not just do it by default?\n\n> The render extension is already out. That's what gives us the wonderful anti-aliased text. Alpha-blending is already used in various places in KDE. I think with QT3 support for alpha-blending will become more universal, but I'm not sure.\n\nI know that it's used for Antia Aliasing, but I never saw it used for actual alpha blending of windows or widgets. I don't think this part is out yet.\nIf it is, why isn't is used? Maybe somebody know?\n\n> What you aren't seeing in the screenshots is the fact that QT/Embedded is not hardware accelerated, which makes it very slow. QT/Embedded was never meant to replace X for normal usage.\n\nFramebuffer IS hardware accelerated (look at DirectFB). And it's very fast.\nOf course no 3D yet. :) But I don't need 3D on my desktop (yet). And it wouldn't be a problem to launch X to play Quake or so."
    author: "Spark"
  - subject: "Re: Eye candy =)"
    date: 2001-09-11
    body: "The framebuffer is *NOT* hardware accelerated.  DirectFB is not the framebuffer.  DirectFB is hardware accelerated.  DirectFB is a layer on top of the framebuffer that provides an interface to hardware acceleration.  DirectFB needs lots of drivers to catch up with XFree.\n\nDirectFB does look like a cool project.  Those screenshots are amazing!  Especially this one:\nhttp://directfb.org/news/count/videoshot.png\nI dearly hope someone ports QT to DirectFB (which is not the same as QT/Embedded running on the framebuffer) so we can enjoy that DirectFB goodness.  However, even if KDE was ported to DirectFB, it probably couldn't use any DirectFB specific features because DirectFB only works on Linux and KDE is cross-platform."
    author: "not me"
  - subject: "Re: Eye candy =)"
    date: 2001-09-11
    body: "In regards to QT embedded, the graphical layers bypasses X and talks directly to the frame buffer (in the kernel), hence the reason the importance of frame buffers in the kernel, thus removing the need to have a top heavy X + QT etc etc."
    author: "Matthew Gardiner"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-10
    body: "I think kicker should use some xml skin system like noatun, sonique or winamp3.\nThis would simply rock! Imagine that you could choose a background for kicker (a png with any format) or even some images for the background, then each item on kicker could be a png in any format...\nIt's hard to explain it well.\nLater I'll post some screenshots and some xml to show what I mean."
    author: "Iuri Fiedoruk"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-10
    body: "Ok, here's the idea:\nhttp://www.protoman.f2s.com/pic01.png (144k, png)\nNow let me explain how this theme would be build (I'll supose the background is only one image called blue.png) - this is only a idea, so I'll kick values for posx and posy (position x and position y) for the items. So here's the xml:\n<kickertheme>\n    <kmenu>\n        <posx>20</posx><posy>112</posy>\n        <icon>kmenu.png</icon>\n    </kmenu>\n    <openapps>\n        <posx>62</posx><posy>96</posy>\n        <cols>2</cols><rows>4</rows>\n    </openapps>\n\nand so goes on for dock, clock, other applets, etc.....\nthis can be more generic, as instead of a tag for each applet, a generic <applet> tag that receives a applet name.\n</kickertheme>\n\nAnyway, this is my idea, what do you think?"
    author: "Iuri Fiedoruk"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-10
    body: "I think it is already possible.  Kicker can have a backround image already.  Kicker's buttons can have \"backround tiles\" that show up behind the icons.  You can change Kicker's icons with an icon theme, and you can change the appearence of the applet handles, buttons, etc. with a regular theme.  You wouldn't want to specify the positions of buttons and applets in the theme itself, that is for the user to decide.  The only thing that is missing is the XML to tie it together.  Currently KDE's themeing capabilities are somewhat widely distributed in different places.  You're welcome to invent an XML theme parser for KDE, since no one else has shown a desire to do so (although unified theme support is an often-requested feature)."
    author: "not me"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-11
    body: "Yes, the only think that seems to be missing is enabling other formats of kicker other than a rectangle.\nThis could even be extended to it's applets. Imagine how cool a circle clock or elipsed buttons for open apps.\nWith this and hability to use kicker as any format, things should be very easy, but I really would like to extend it more to something like Object Desktop."
    author: "Iuri Fiedoruk"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-11
    body: "I like your ideas.\nIt would be really need to be able to skin kicker like say... k-jofol. :)\nSomeone could do a sience fiction kicker, etc...\nLook at Athene (www.rocklyte.com) to know what I'm talking about (you can download and install it, it's pretty easy and a nice demo of a fully skinnable desktop! Very impressive).\nOf course that's very hard to do, cause you have to provide places for applets, the menu entry's, etc. You can't hardcode them, cause the user could decide to load an unknown applet or something like that.\nI don't expect this very soon, but would probably be a nice idea for KDE 4.0 ;)"
    author: "Spark"
  - subject: "Re: New features in KDE 3"
    date: 2001-09-14
    body: "Try kde.themes.org for themes and help with making your own. That's the great thing about Free software: If you don't like it, you can fix it! I eagerly await your beautiful new KDE skins!"
    author: "Wulfhere"
  - subject: "missing features of 3.0"
    date: 2001-09-09
    body: "I think a lot of users will be surprised reading that there is no plant to make a unique theme handler, merging things like window decoration / theme / style into one single menu point under kcontrol, and even more important, integrate a theme creator which enables the user to simply choose style / wallpaper / colours etc, save everything into 1 file, and upload it to the one-day-probably-working-and-updated-again-kde.themes.org."
    author: "ElveOfLight"
  - subject: "Re: missing features of 3.0"
    date: 2001-09-09
    body: "I think its SO important. I would like to start to improve my C++ to build a theme creator. I think this is not very difficult. Only a wizard that asks you what color or pixmap do you want for the different widgets."
    author: "Daniel"
  - subject: "Re: missing features of 3.0"
    date: 2001-09-09
    body: "Yeah, we'd all appreciate your efforts!\n\nI took again a look at the list of planned features and it makes me think that kde-developers are not really talking about all their plans. I guess we ll be surprised by the things that are upcoming and not mentioned in the list by our kde-wizard heroes. \n\nDont forget about improving your c++ abilities and write the theme-manager ;-)"
    author: "ElveOfLight"
  - subject: "Re: missing features of 3.0"
    date: 2001-09-09
    body: "I think its SO important. I would like to start to improve my C++ to build a theme creator. I think this is not very difficult. Only a wizard that asks you what color or pixmap do you want for the different widgets."
    author: "Daniel"
  - subject: "Re: missing features of 3.0"
    date: 2001-09-10
    body: "Right said. That merging would be _badly_ needed."
    author: "AB"
  - subject: "2.2-2.2.1.patch"
    date: 2001-09-09
    body: "i really really hope will see something like that on kde ftp.... \nwill it come ??"
    author: "jamal"
  - subject: "Re: 2.2-2.2.1.patch"
    date: 2001-09-09
    body: "With the very very large size of KDE code, such a patch would be huge. Those yo whom such a patch would be addressed would need to know how to compile KDE from source, hence it would be expected from them to use CVS directly. With CVS, going from KDE-2.2 to KDE-2.2.1. is a simple \"cvs upd -dPA\" away."
    author: "Inorog"
  - subject: "Re: 2.2-2.2.1.patch"
    date: 2001-09-09
    body: "owh... that's nice :) thanks !!"
    author: "jamal"
  - subject: "Re: 2.2-2.2.1.patch"
    date: 2001-09-10
    body: "In fact, sorry, it's not \"cvs upd -dPA\" but it's rather \"cvs upd -dP -r  KDE_2_2_1_RELEASE\"\n\nHope I didn't cause you much trouble."
    author: "Inorog"
  - subject: "Re: 2.2-2.2.1.patch"
    date: 2001-09-11
    body: "yes , you make me in trouble !! ..\nhehehe .. nope , it's all right :) , yes.. i'm in trouble in a few minutes .. then i realize it , go to kde web , and found about that -r thing . \nthx ."
    author: "jamal"
  - subject: "Re: 2.2-2.2.1.patch"
    date: 2001-09-10
    body: "Going from 2.1 - 2.1.1 the KDE team released a patch (or at least it was on their ftp server) at ftp://ftp.kde.org/pub/kde/stable/2.1.1/distribution/tar/generic/src/diffs/.  And although the diffs would be large, they wouldn't be as large as downloading the full source distribution (especially going from 2.1 -2.1.1 which are bugfixes).  Not everyone using KDE will have access tot the web from the UNIX machine (thanks to winmodems), so a patch would be of big help to those who like to compile from source, but may not have access to the web from their machine.  I just hope that they do provide them becuase sometimes even CVS isn't always the best option, especially if you get a slow server.\n\n--Reggie"
    author: "Reggie"
  - subject: "Clipboards support"
    date: 2001-09-09
    body: "I have just one question: Will Qt3 fix the clipboard?\nAtm it is that mouse selections always overwrite my clipboard.\nI think it's much neater to store the \"Strg+C, Strg+V\" clipboard in a different X-Clipboard than the the \"Mouse selection, MMB\" clipboard.\nAFAIK Qt3 WILL handle it this way (like Gtk does already), but I want to be sure cause it is REALLY hard for me to work without it. :) And will it be compatible to the Gtk clipboards?"
    author: "Spark"
  - subject: "Re: Clipboards support"
    date: 2001-09-09
    body: "Yes it will, and I'm totally gutted as a result, as I really liked that drag and click method of copying and pasting. Other OS's aren't always better you know..."
    author: "Bryan Feeney"
  - subject: "Explaination"
    date: 2001-09-09
    body: "Maybe you misunderstood me... using two clipboards does not take away any functionality.\n\nSee, in Gtk it works like this:\n\nThere are two clipboards. I call them CA and CB. \n\nNow if you select text with the mouse, it's always copied to CA and everything there is overwritten.\nIf you press the middle mouse button, the text is pasted from CA.\n\nIf you press Ctrl+C (or use the menu, toolbar) while text is selected, it's copied to CB.\nIf you press Ctrl+V (or menu, toolbar), it's pasted from CB.\n\nSo you can use CA for quick copy and paste (I do most of the time), but if you need something stored longer (i.e. it shouldn't be overwritten if you select something else), then you can press Ctrl+V to store it in CB. Now you can still use the mouse for quick copy and paste with CA, but you can always paste the selection in CB by pressing Ctrl+V cause it never gets overwritten, unless you press Ctrl+C again.\n\nI hope that Qt 3 will have the same functionality!\nEverything else is cheap."
    author: "Spark"
  - subject: "Re: Explaination"
    date: 2001-09-09
    body: "Yes, Qt3 fixes this. This was a long overlooked bug in Qt."
    author: "Gary Storla"
  - subject: "Great. :) n.T."
    date: 2001-09-09
    body: "n.T."
    author: "Spark"
  - subject: "Re: Explaination"
    date: 2001-09-10
    body: "Phew!!!"
    author: "Bryan Feeney"
  - subject: "Re: Explaination"
    date: 2001-09-10
    body: "... and while you're at it: would be nice to have the clipboard content available for motif and java based apps. If I want to paste some text into\nJBuilder I always have to middle-mouse-btn-paste it into nedit and then copy it with ctrl-c. Would be nice if there could be a \"direct\" way ;o)\n\nkeep up the good work\n Michael"
    author: "mike.s"
  - subject: "Re: Explaination"
    date: 2001-09-10
    body: "I don't know about others, but for me klipper works as a good work-around for the multiple clipboard problem.  No matter how I enter text into the clipboard, I can always get it out later.  And the copy history can be set to almost any size.  This is definitely one of the most useful applications under KDE!"
    author: "Peter"
  - subject: "Re: Explaination"
    date: 2001-09-10
    body: "Hmm yes. Klipper is a workaround, but nothing more. \nIt's not very comfortable and I don't like to use it. :)\nYou have to click it, than search the text, etc, instead of just pressing Ctrl+V to enter the text."
    author: "Spark"
  - subject: "How about updating help and context sensitive help"
    date: 2001-09-09
    body: "Right from KDE 2.0, the Help seems to be ages old and not quite helpful. And the \"What's This?\" context sensitive help  is just missing in most of the KDE applications. Can we expect updated help in KDE 3.0?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: How about updating help and context sensitive"
    date: 2001-09-09
    body: "You seem to know what should be in the help and in the context sensitive help. Maybe you could help the KDE team (which mostly consists of volunteers) by improving the help yourself. I'm sure it's not at all that difficult to do (as in, you don't need to know how to program to author the help)."
    author: "Joeri Sebrechts"
  - subject: "Re: How about updating help and context sensitive"
    date: 2001-09-09
    body: "All the KDE apps I have tried to use have poor (generally sparse) help. (I might say, not as bad as man pages which overall should take the prize for \"worst technical writing of all time\")\n\nBeing realistic, it is way too difficult to write up-to-date help after the code has been done. For the developers it becomes a choice between new features/bug fixes OR documentation. No choice really.\n\nProbably the only way that you have a chance is for the help to be embedded in the source code, and co-written with it. \n\nMatlab takes the initial comment lines of the file as being help for that function, and I use this as the default help page for the gui screen created in that file. This is a simple and very effective system, that has resulted in almost all matlab functions written here, having documentation.\nBy comparison I just did a quick count of some in house VB and C: 11 of 74 documented main fn's, and only 1 of 5 apps with online help at all.\n\nI think the help source needs to be in the files, right with the functions that respond to UI events, or in the UI builder as properties of the widgets. This context sensitive, widget bound help is the front line help of a gui app. By co-writing, it can become more useful. Written with the code it is likely to tell the use about known \"gotcha's\" and twists associated with using a feature. This is what the user is in the help for. \nWhen it is written after the event, you get useless help that states the self evident: \"The START button starts programs\" \n\nIn delphi I use all the tooltips for documentation as these are so easy to edit as you go along. I would love Borland to put the context help editor in with the gui property editors, so you write the context help as you add a new widget.\n\n-------------------------------------------------------------------------\nImproving the overall standard of help is a job for the development tool writers. \nIf X hours of time is spent on an easy to use \"help with code\" system at the tool end, the results are multiplied by the number of application developers."
    author: "simon"
  - subject: "Re: How about updating help and context sensitive"
    date: 2001-09-10
    body: "Documentation is already integrated with the code, but it is documentation for programmers.  User documentation on top of that would just clutter up the source files, I think, since user help isn't necessarily directly related to the various functions that are in the source.  The underlying design of the program would start to leak into the user help and just confuse the user.\n\nWhat is really needed is an easy way for non-programmers to create help files.  Something on the order of KBabel (KDE's translator app) for help files.  That way the programmers can concentrate on coding, ordinary people can have a nice way to contribute, and KDE gets up-to-date and useful help files!"
    author: "not me"
  - subject: "Some K-apps needs facelifts in KDE 3.0"
    date: 2001-09-09
    body: "I thank KDE team for including Kinstaller in KDE 3.0. I would love to see improvements in Kicker, KOffice, and KWin. And overall redesign of KControl modules, the KStyle module going out of my desktop (screen area) not just at 800x600 but also at 1024x768 resolution."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Some K-apps needs facelifts in KDE 3.0"
    date: 2001-09-09
    body: "Is Kinstalle a app that provide a easy way to install and remove aps one KDE and will there be a update app zo that we can download update and new software out of a data base one the internet sort a way as windows updater and windows installer. \n\nIs this http://www.mslinux.com/software/kinstaller.html the web site of kinstaller ore is this a other project."
    author: "Underground"
  - subject: "Re: Some K-apps needs facelifts in KDE 3.0"
    date: 2001-09-09
    body: "That's the project.\n\nThe idea is to create an XML file from the RPM and DEB packages.  Think of it like an index file.  It will have a KTicker feature where you can specify the interval of time to check for update.  \n\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: Some K-apps needs facelifts in KDE 3.0"
    date: 2001-09-10
    body: "nice!\nI think the major thing that the KDE world doesn't have and GNOME has is a good installer. It kept me from running KDE for a long time. Thanks!"
    author: "Wolfgang"
  - subject: "Wishlist"
    date: 2001-09-09
    body: "I saw this on kde-devel a few weeks ago, anyone care to comment on any of them?\n\nHi, after not using kde since 1.1, I've switched back\nto kde with 2.2. It is great! It is 95% of what I want\nmy desktop to act/feel/look like. However, there are a\nfew features that I mess from other kde-like software,\nwether it be Windows, GNOME, or MacOS. So, here is\nwhat I'd look foward to seeing in kde 3.0 (or even kde\n2.2.1, but I guess that is only a bug fix release).\nKeep in mind that I am not a programmer, so I don't\nknow what is viable or not.\n \n \n1. drag and drop in more things that now, for example,\nit would be neat to drag and drop text from konqueror\nto kword, or maybe perhaps\ndragging a paragraph from konqueror and having it\nstyled in kword, but would appear as simple text in\nsomething like kwrite.\n \n2. downloads windows have checkbox to close when done,\nand a button to show the directory in which it is\ndownloading (very useful feature from internet\nexplorer).\n \n3. with plugins enabled in konqueror, and when a\nplugin for a type is not found, only popup a window\nonce for it.. for example if you do not have flash\ninstalled, and you goto a flash website, you may see 5\nor 6 windows popup trying to show you that the plugin\nis not installed. annoying. maybe there should be\noption for turning off these altogether.\n \n4. <nobr> support in khtml.. this is probably the most\nwidely used non-standard tag.. it still prevents soem\npages from being rendered\ncorrectly.\n\n5. in konqueror, add an action that represents the\nfunctionality of the find window, this could be placed\nin a toolbar and quick searchign of webpages could be\ndone without the need of opening a find window\neverytime. opera has something like this afaik.\n \n6. fix the user style sheet option in konqueror, I\ncould not get this to work with stylesheets that work\nin both IE and opera. This is really a must for people\nwith special accessibility needs.\n \n7. maybe some kind of mouse trails for the mouse\narrow? I'm not sure if kde could do this, but judging\n>from some of the funky animations\nKDE does with the mouse pointer, maybe it is. I hope\n:)\n \n8. Fix applets when the panel is vertical. some\napplets, such as the Application Launcher, have a\nhabit of dissapearing when the applet is\nvertical. Other applets, such as the clock sometimes\ndo not display well with less space available (and oth\ner times it works perfectly).\n \n9. Kde app wide alpha channel blending (if that's\npossible).\n \n10. Make selecting text in khtml faster. It is\nnoticbly more sluggish compared to Mozilla. rendering\nit self is pretty fast.\n \n11. Make the kmenu editable without need for any otehr\napps. For example, you could drag \"Graphics\" under\n\"Internet\" to make it go under Internet. Or maybe you\ncould hold down middle mouse button for a\nfew seconds, and the text of a name could be editable.\n\n12. kmail imap support is very cool, but it is really\nunstable/doesn't work properly with large IMAP\nfolders. This needs to be changed\n \n13. in konqueror's file browsing mode, if double click\nis enabled to open up folders, if a icon is selected,\nand the mouse pointer is on the label of the icon for\na few seconds, it should go into rename mode (like in\nmacos).\n \n14. have more meaningful names in the kmenu. I really\nlike names like \"konqueror- web browser\". that shows\nto the user that whenever they\nsee that icon, it is konqueror the web browser.\nHowever, if the user sees something like \"kit\", they\ndon't know what it is without chosing\nit. Maybe it should be renamed \"kit - AOL Internet\nMessanger client\" or more simply, \"AOL Internet\nMessanger Client\". Maybe theer should be\na policy for this.\n \n15. a \"reboot\" command in kmenu, or maybe logout has a\nreboot checkbox\n \n16. Maybe have a \"send bug report\" button in the crash\ndialog\n \n16. show kde version in splash screen\n \n17. tooltips in stuff in kmenu\n \n18. in the pager applet, have a row and columns\noption, for example, the user, if he/she has 4\ndesktops, might want to have the desktops arranged\nhoriztonally  \n\n19. a \"floating\" type of panel should be introduced\nthat can be put anywhere in the desktop\n \n20. in panels in kicker which don't take 100% of an\nedge, they should allow panel extentions to use up\npart of the rest space. for example, i could have the\nmain kicker panel starting from te northwest corner\ndown, and a kasbar starting from southwest corner up.\n \n21. Have a \"drawer\" button in the kicker, a drawer\nslides out, and can hold applets, other application\nbuttons, other drawers, or stuff liek\nmore kmenus\n \n22. make button positions in kwin-clients modifibable\nlike in kde 1.1\n \n23. in kde apps, Make menubar movable, like toolbars\n \n24. in kde apps, add the ability for menubars and\ntoolbars to appear in new windows (like a palette). I\nsaw this in the qt3 designer app,\nso I think qt3 has this ability.\n \n25. in konsole, add ability to change cursor color,\nalso, when you open up konsole, the tab name is\n\"konsole\", but when you click on new\nit is \"shell\". imho, it should start with shell, and\nnot konsole.\n \n26. ability of kicker to \"swallow\" other apps, like\nswallowing gkrellm/ksim into a vertical panel\n \napps that I'd like to see kde3 equivs of (that are not\nany kde2 equivs):\n1. lyx (there was klyx for kde 1.1? what happened to\nthat?)\n2. ethereal\n3. gimp (well, I don't like krayon's interface that\nmuch, if krayon's feature set was expanded a bit, and\nit's interface was tweaked for some kind of MDI\nsetting, I'd prefer it over GIMP any day. (GIMP's\nInterface isn't that great either.. I guess I am\nlooking for Photoshop for X11 :))\n4. xmms/winamp - noatun is a great MS media player\nreplacement, but it *feels* too heavy for a mp3/ogg\nplayer. but maybe that's just me.\n5. aviplay or mplayer (aviplay would be easy to port\nto kde since its in qt.)\n6. xmame\n7. zsnes/snes9x- there are several gtk/gnome\nfrontends, but no kde ones afaik\n8. xchat - well, kvirc and ksirc are great... but I\ndon't like kvirc's interface much, and xchat is a bit\nmore customizable than ksirc.. so what I'm trying to\nsay is that ksric would be great if more\ncustomizablity wwere added.\n9. nethack/gnomehack\n10. xqf (a online game finder for gtk)\n11. adaptec directcd- packet burning app for windows..\nit'd be prolly easily done through konqueror. just\nselect a group of files/dirs, drag to a special cd\nburner icon, and then after you are done, right click\non the special icon and chose \"burn\".\n12. xine or oms-gtk - somethign th at can play dvds\n13. a wysiwyg html editor-- something like mozilla's\ncomposer mode\n14. gcolorsel/xcolorsel\n15. xmag\n16. an app like gtk-theme-switch to switch kde or qt\nwidget themes on the fly without kcontrol\n\n\nKeep in mind that that these are just my wishlist, you\nguys are doing a great job already. just putting in my\n2cents. And sorry, for any speling, grammer. and other\ndumb errors, I'm tired. :)"
    author: "lildawg"
  - subject: "Re: Wishlist"
    date: 2001-09-09
    body: "Wow!\n\nI had never seen a list so complete of things that KDE is missing. I don't really care about the apps (as long as they exist for linux, I'm not bothered about if it's gtk or qt), but I agree that most of the things in the list are expected from users that come from Windows and I hope that the KDE programmers read this message.\n\nSalva"
    author: "Salva"
  - subject: "Re: Wishlist"
    date: 2001-09-09
    body: "Very intersting list ! I don't understand all things, but very few seem useless.\nAnd I recognize some improvements I strongly wish :\n\n> 1. drag and drop\n-- Yes, and also between two full screen apps, I think by moving thru the\ntaskbar\n\n>  4. <nobr> support in khtml.. this is probably the most\n widely used non-standard tag.. it still prevents soem\n pages from being rendered  correctly.\n-- Of course !\n\n> 5. in konqueror, add an action that represents the\n functionality of the find window\n-- Perhaps, but at first enable the F3 key to be used for \"Find Next\". And also\n(very important !) use the \"Back\" Key for going on the previous page\n\n> 8. Fix applets when the panel is vertical. some\n applets, such as the Application Launcher, have a\n habit of dissapearing\n-- Yes and make the clock more readable when the panel is vertical.\n\n>  11. Make the kmenu editable without need for any otehr\n apps.\n -- Yes (already said last week)\n\n And I add :\n\n 27 In all apps, memorize the positions and properties of all the tasks bars,\nso that they will be the same when opening the program. I first think about\nKate...\n\n28 Add icons in the right mouse menus of many apps. For example in Konqueror\nfor refreshing the frame (the icon already exists...) (and about the refreshing\nbutton of Konqui, display the page at the same place it was before the\nrefresh...)"
    author: "Alain"
  - subject: "Re: Wishlist"
    date: 2001-09-09
    body: "I agree too, here are my additions:\n\n29  when you hold down the mouse button in konqueror on a directory icon, it should popup a new window with the listing of the directory, which could be further traversed. This would be useful if you dragged a file from /home/foo/test/ to /usr/foo/test/blah/heh/sdsd. you would not have had to open the second directory. This is like MacOS's spring folders.\n\n30  a theme creator is needed!\n\n31  the kmix applet should have a mode in which it appears as an icon. when yourrmb ont he icon, a little popup menu shows with sliders for controlling master/pcm/etc..\n\n32  the pager applet should have a mode to remove the labels of the desktops (like 1 2 3 4). I frankly don't care what those desktops are named or what number they are. Other people might, but I should still be given a choice. \n\n33  a way to simplify all menus, sorta like in Microsoft Office 2000's menus. a small subset of the menu items are shown, and there is an arrow is shown at the bottom of the menu. When you get your mouse over that arrow,the rest of the items of the menu show up. If you select one of these items, it'll be added to the small subset, and remembered next time when you load that menu or app. Right now, imho, kde menus are really cluttered compared to GNOME's, and I think this would be a good way to simplify it somewhat.\n\n34  menus should have flexible locations, like GNOME's or Window's. THey really should just be toolbars with text being the default. I'd even be nice to turn some of the menu names into Icons like in MacOS. \n\n35 keyboard navigation should be improved. There should be a way to do through keyboard. To start, I think tabbing in Konqueror needs to be fixed.  It should work like it does in IE. \t\n\n36 tabbing in konqueror. the only thing I miss from galeon or opera.\n\n37 a smart bookmark feature in konqueror like in galeon\n\nI'd like to see these apps ported to KDE3 (not really that important, but still):\n\nxqf\na mathml widget/kpart (like mozilla's or gtkmathmlwidget)\nxmms\n\nalso, I think kinkatta should replace kit and kvirc should replace ksirc (wll, I dunno about that, is kvirc a true KDE app?). it'd still be nice to remove the perl dependencies in ksirc."
    author: "Nitro"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: ">33 a way to simplify all menus, sorta like in Microsoft Office 2000's menus. a\n>small subset of the menu items are shown, and there is an arrow is shown at the\n>bottom of the menu. When you get your mouse over that arrow,the rest of the\n>items of the menu show up. If you select one of these items, it'll be added to\n>the small subset, and remembered next time when you load that menu or app.\n>Right now, imho, kde menus are really cluttered compared to GNOME's, and I\n>think this would be a good way to simplify it somewhat.\n\nIn addition, apply this logic to the file type picker.  When saving a graphic, show only the most recent -- with JPG, GIF, PNG, TIF and BMP being the defaults.  It's wonderful some programs can save in dozens of formats, but how many are really used by the average user?\n\nAlso, give the file type picker extended text -- like a description of the file type.  I've had novices repeatedly ask me what file type to save images in for the net and then wonder why their BMP files take so long to download.\n\nSort of like:\n.JPEG  -- Photographs and pictures for the web.\n.PNG -- Icons and an alternative to GIF\n.GIF -- Animated icons, banners and web buttons.\n.TIFF -- Full fidelity images for pro printing."
    author: "Charles Hill"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: ">  33 a way to simplify all menus, sorta like in Microsoft Office 2000's menus. a small subset of the menu items are shown, and there is an arrow is shown at the bottom of the menu. \n\nI desagree. It is very irritating to 1) search, 2) not find and then 3) make a supplementary click for having the good command.\n\nFor me it is a very bad option and MS Windows is very irratating with such things where the system try (badly of course) to think for the user. So please, if such a thing has to exist one day, make it only in option and not the default option."
    author: "Alain"
  - subject: "I Agree 100% ( with you that is :) )"
    date: 2001-09-11
    body: "I currently find this one of the most annoying and irritating \u0093features\u0094 of MS software.  It drives me nuts not being able to quickly find a menu item because the program/OS got a brainwave and decided to hide it from me because it knew I didn\u0092t want to use it.  IMHO it is much faster to find an item in a menu if the menu remains the same as apposed to if it keeps changing which items are immediately visible to the user.\n\nJust my 2 Oz cents plus a rant :)"
    author: "Cyber Rider"
  - subject: "Re: Wishlist"
    date: 2001-09-12
    body: "Count me in agreement, too.\n\nI *HATE* \"Intelli\"menus.  And it makes support a nightmare, because you don't really know what will show up on a users monitor when you're talking to them.\n\nKeep them out of KDE."
    author: "Electroniceric"
  - subject: "Re: Wishlist"
    date: 2001-09-15
    body: "I guess it can't be underlined often enough!!!\n\nIf there really are lots of users who think they need\n'intelligent' menu make it optional and never default.\n\nI would have something else on the wishlist:\n\nI would like to use a filefilter on the url-field.\n\nIf I give it a URL like f.i.\n file:/path/to/somewhere/*.jpg\nkonqui should show me only jpegs simiulary to an\n ls /path/to/somewhere/*.jpg\non the console. Even my really very old Atari had\na file-filter-option, allthough a little bit hidden."
    author: "Andreas Silberstorff"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: "Mathml support (and maybe SVG) would be great. Love KDE by the way, keep up the good work."
    author: "Thijs"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: "> > 1. drag and drop\n> -- Yes, and also between two full screen apps, I think by moving thru the\n> taskbar\n\nThis is really one of the few things bugging me: When a window is hidden it is not possible to drag something into it. Windows is better in this respect: Drag to the taskbar and the appropriate window will open."
    author: "kdefriend"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: "WHY does everyone think this doesn't work?!?  Of course it works:  drag something to the taskbar, HOLD it there for 1-2 seconds, and the corresponding window will pop up!  Seems easy enough.  In fact, it works in *exactly* the same way as it works in Windows!\n\nMaybe it was a recent KDE addition or something, but I'm here to tell you that it works great in KDE 2.2."
    author: "not me"
  - subject: "Re: Wishlist"
    date: 2001-09-12
    body: "You're right, it does work. But what aur you doing with \"Group similar tasks\" switched on? I allways get the wrong window..."
    author: "Michael"
  - subject: "Re: Wishlist"
    date: 2001-09-12
    body: "Do you need do do anything in the control center for this to be activated?  I've held over a button in the taskbar for 10 seconds and jack all happened.  A little square popped up to the right of the task buttons (shrinking the buttons) indicating that I could drop on the panel, but no windows magically popped up."
    author: "Me"
  - subject: "Re: Wishlist"
    date: 2001-09-09
    body: ">an app like gtk-theme-switch to switch kde or qt\nwidget themes on the fly without kcontrol\n\nHmm, couldnt you just use a console dcop command to accomplish the same thing?"
    author: "Carbon"
  - subject: "Re: Wishlist"
    date: 2001-09-09
    body: "Hi! How do I do this?"
    author: "Wolfgang"
  - subject: "Re: Wishlist"
    date: 2001-09-11
    body: "Run \"kdcop\" to get a nice tree-view of all the DCOP-callable functions registered on your system at the moment. (which will of course vary depending on which programs you have open)  If you can do it with DCOP, it is listed there.  Then if you run \"dcop\" from the command line you can call any of those functions."
    author: "not me"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: "One thing I miss is KMail support for authenticated SMTP.\nAs it is, I can read my POP3 mailbox just fine with KMail,\nbut need to use Netscape to send mail, as my ISP's SMTP\nserver requires authentication."
    author: "Anonymous Coward"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: "Me and other people requested this feature more than a year ago, but they seem to ignore it for now, well thats OS, nobody likes to do things that are neither interesting nor simple. I guess thats OK, the guys already to a good job. In the 3.0 feature list there's a point about switching smtp to io slave - does a smtp io slave support authentication ?\n\nCheers,\n-Lev"
    author: "Lev"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: "Well and we also ignore the other 300 outstanding wishlist requests for KMail since ages :-)\nIt's just, that nobody of the KMail core developers uses an SMTP server, that requires authenitcation.\n\nAlso in this case there were unfortunately also some other non technical problems that caused a delay. A few different people promising to work on the issue and not delivering anything for weeks or months, some different opinions of how things should be done and a licencing issue.\n\nAnyway, please note, that we do now finally have SMTP authentication in CVS. Check it out and test it!"
    author: "Michael H\u00e4ckel"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: ":) Well its just as I thought :) Anyway thanks much for new features! Keep up the good work! \n\nCheers,\n-Lev"
    author: "Lev"
  - subject: "Re: Wishlist"
    date: 2001-11-29
    body: "I downloaded just the Kmail stuff from CVS but con't seem to be able to build it.  I'm missing the configure.in file (probably others?).  Do I have to download all of KDE to build the Kmail with autehnticated smtp send support?\n\nAny helpful advice?"
    author: "Bob Riddle"
  - subject: "Re: Wishlist"
    date: 2001-11-30
    body: "Do you have kdelibs and qt3 already installed? make -f Makefile.cvs already?\n\nYou might just want to wait for kde 3 beta1 to be made (next week, likely). Many distros will package it."
    author: "Sashmit Bhaduri"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: "wow....quite the list - I've got a few things to add.....\n\nafter using gnome/sawfish for the last 2 years, I switched to kde 2.2 - lovin it! except for 2 little things:\n1) I can't seem to find an option for edge-switching of my virtual desktops (eg move the pointer to the far right of the screen and keep \"pushing\" for a few milliseconds to move to the desktop to the right)\n2) also with the window manager - I'd really like to see an option to remove certain applications from the taskbar - I've got a few apps (licq, gkrellm, xmms) that are on all my desktops, always on top - and they just clutter up the taskber list."
    author: "duber_ertw"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: "1. This should be re-enabled in version 3.0 I think. Check out the kde-core mailing list archive (search for electric borders). I guess it is already in the cvs version.\n\n2. Use the kstart command to start your application. Type kstart --help to see to options. For example I start gkrellm with:\n\nkstart --alldesktops --skiptaskbar --skippager '/usr/bin/gkrellm'"
    author: "Meir Kriheli"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: "> 1. drag and drop in more things that now, for example,\n> it would be neat to drag and drop text from konqueror\n> to kword, or maybe perhaps\n> dragging a paragraph from konqueror and having it\n> styled in kword, but would appear as simple text in\n> something like kwrite.\n\nis already being worked on ... (but i don't know if it will be ready by 3.0)\n\n> 10. Make selecting text in khtml faster. It is\n> noticbly more sluggish compared to Mozilla. rendering\n> it self is pretty fast.\n\nThis is known (it bugs me too btw) but there is no easy fix for this...\ncurrently konqueror traverses the whole dom tree for every event \n(as someone pointed out here). \n(try waving with your mouse above freshmeat.net and watch your\ncpu load)\n\n> 13. a wysiwyg html editor-- something like mozilla's\n> composer mode\n\nkafka is really promising, but not yet finished."
    author: "ik"
  - subject: "Re: Wishlist -- Lyx"
    date: 2001-09-10
    body: "Re: your request for a KDE-Lyx, you might like to know \nthat the Lyx folks are working on GUI-independence so that\nthere can be multiple front-ends to the back-end engine.\n\nYou can track the (slow) work on the lyx-developers site;\nthere is progress on both gnome and kde front-ends."
    author: "ProfDumb"
  - subject: "Re: Wishlist"
    date: 2001-09-10
    body: "I would like to add to this a very important app which still is not available in linux.\n\nKde Download Accelerator.\n\nyou can understand that it must have support for segmented downloading and ofcourse must have http, ftp proxy support. there are many download manager available but noone have support for segmented downloading.\nif anyone know about such an app, please let me know."
    author: "vk"
  - subject: "Re: Wishlist"
    date: 2001-09-19
    body: "Well what's segmented downloading?\nIf it's the ability to continue downloading, konqueror can already do this\n;-&\n\nbtw, please go to\nhttp://povtek.tripod.com/\n\nso it adds a few more on to the counter :-)"
    author: "Luke"
  - subject: "Re: Wishlist"
    date: 2002-09-23
    body: "URR LUKE YOU ARE POVERYT STRICKEN\n\n<body background=\"http://www.stupid.com/images/mouthy-ani.gif\">\n<EMBED SRC=\"http://www.newgrounds.com/portal/uploads/18000/18702_hyakugo.swf\" PLUGINSPAGE=\"http://www.macromedia.com/shockwave/download/\" HEIGHT=\"400\" WIDTH=\"550\" TYPE=\"application/x-shockwave-flash\" quality=\"best\"></EMBED>\n</body>\n        "
    author: "Zena the Grey from Zeta Reticuli"
  - subject: "Re: Wishlist"
    date: 2002-09-23
    body: "URR LUKE YOU ARE POVERYT STRICKEN\n\n"
    author: "Zena the Grey from Zeta Reticuli"
  - subject: "Re: Wishlist"
    date: 2001-12-26
    body: "I also have a few small things to add, mainly to Konqueror file browser:\n\n1. Ignore dropping an icon on itself - currently you can't\n   move a desktop icon or a group of them just a few pixels\n\n2. Tooltip with the full filename over a file whose name is\n   truncated in konqueror. The full name is currently shown\n   in the statusbar, but you have to move your eyes down to\n   see it, which slightly disturbs concentration.\n\n3. A \"Shell here\" option in the popup menu when you right-click\n   a directory in Konqueror (this is a neat feature from 4NT)\n\n4. GPG \"encrypt file\" and \"decrypt file\" extension for the\n   righ-click-popup-menu in konqueror\n\n\n\nThese are all quite small tasks indeed and I could probably fix\nthem myself for KDE 2.2 if I knew:\n\n + how to check that the feature doesn't already exist\n + how to make sure that the change will be accepted\n   (and by whom)\n + where to look in the code\n + how to submit a patch\n\nCould someone give some pointers?"
    author: "Jarno"
  - subject: "Re: Wishlist"
    date: 2001-12-26
    body: "I also have a few small things to add, mainly to Konqueror file browser:\n\n1. Ignore dropping an icon on itself - currently you can't\n   move a desktop icon or a group of them just a few pixels\n\n2. Tooltip with the full filename over a file whose name is\n   truncated in konqueror. The full name is currently shown\n   in the statusbar, but you have to move your eyes down to\n   see it, which slightly disturbs concentration.\n\n3. A \"Shell here\" option in the popup menu when you right-click\n   a directory in Konqueror (this is a neat feature from 4NT)\n\n4. GPG \"encrypt file\" and \"decrypt file\" extension for the\n   righ-click-popup-menu in konqueror\n\n\n\nThese are all quite small tasks indeed and I could probably fix\nthem myself for KDE 2.2 if I knew:\n\n + how to check that the feature doesn't already exist\n + how to make sure that the change will be accepted\n   (and by whom)\n + where to look in the code\n + how to submit a patch\n\nCould someone give some pointers?"
    author: "Jarno"
  - subject: "Konq 2.2.1 Fantastic!!!"
    date: 2001-09-09
    body: "I just pulled down KDE 2.2.1 (I couldn't find the tar files so I grabbed the CVS) and I just wanted to say that Konqueror seems to render pages much faster, and an annoying bug that made my domain take _forever_ to load in 2.2 has been fixed!\n\nIt was the weirdest thing.. every version of Konqi prior to 2.2 loaded my site super-fast, but with 2.2 it would just sit there for about 30 seconds before loading.  The really odd part was that when I put the site on my local machine and loaded it it was the normal speed..  It was going out to my domain and back that took the time..  I logged a bug about it, but never got a reply..  well, it's been fixed!\n\n\nThanks a lot Konqi team, that was really bothering me!\n\nIn fact, for whoever is reading this, I'd just like to mention that at least on my machine) Konqi's rendering seems a lot faster.  I'd like to know what they did to speed things up.. Anyway, I've only been running 2.2.1 for a few minutes yet, but I haven't seen any bad things happen yet, so I'm happy!\n\nCheers,\n\nBen"
    author: "Ben"
  - subject: "This is only interesting if KNote and KBabel speed"
    date: 2001-09-09
    body: "On ppc machines KNote and KBabel are incredible slow. I mean, aspestus dust fall faster than letters apear on the screen after typing.\nIn periods i can type compleat sentenses before things starts to happen on the screen.\n\nRegards\nIsak"
    author: "Isak Lyberth"
  - subject: "Re: This is only interesting if KNote and KBabel s"
    date: 2001-09-10
    body: "1) What version of X are you using?\n2) What distro are you using?\n3) Have you reported this to bugs.kde.org? Otherwise, very few developers will see it."
    author: "Carbon"
  - subject: "Re: This is only interesting if KNote and KBabel s"
    date: 2001-09-16
    body: "im using Yellowdog Linux 2.0, kernel 2.4, XFree86, QT 2.3.1, on a iMac, Rev A \nKDE have been notified, yes\n\nRegards Isak"
    author: "Isak Lyberth"
  - subject: "Will Red Hat RPMs be all messed up again?"
    date: 2001-09-09
    body: "Last August I've downloaded the Red Hat \"7.x\" packages that broke my RPM system and introduced several bugs to my system, **eventually leading to a system-wide crash with data loss**. The \"7.x\" label is DECEPTIVE since the RPMs were built on a beta system and there WAS ABSOLUTELY NO INDICATION of this fact.\n\nI understand that those problems were because Red Hat chose to build the RPMs against a beta setup (RawHide). This time, Red Hat folks, please build a STABLE release against a STABLE distro.\n\nThanks, Eduardo\n(registered RH user: sombragris)"
    author: "Eduardo Sanchez"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-09
    body: "I found the right KDE2.2 rpms for RedHat 7.1 at http://www.opennms.org/~ben/kde2.2/. They're build by someone called ranger_rick on this forum.\n\nPerhaps it's time for some other people besides bero@redhat.com to step up and build rpm's for RedHat-stable."
    author: "bdr"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-10
    body: "Thanks for your comment. Of course, after crashing my system and reformatting my Linux partition and reinstalling RedHat I found out a series of independent, stable RPMs. But that's not the point. The point is that the Red Hat \"7.x\" RPMs that are stored in the ftp.kde.org site are all messed up and are a real nuisance, and the KDE project would do well to delete them from their server and tell Red Hat users about some stable RPMs. We can produce (after a day and a half) our own binary, stable RPMs, but it would amount to nothing if the main ftp site still points to those unstable and dangerous RPMs.\n\n--Eduardo"
    author: "Eduardo Sanchez"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-10
    body: "I had no problem installing the RPMS.  What do you mean \"messed up\"?"
    author: "Neal Becker"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-10
    body: "With \"messed up\" I mean that the RPMs required dependecies on UNSTABLE software and in one case (the kdeadmin package) the dependecies (unnecessary) on rpm 4.0.3 broke my system badly.\n\nthanks, eduardo"
    author: "Eduardo Sanchez"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-14
    body: "I installed everything (including the \"Unstable\" deps) on my RH 7.1 system and everything works fine (KDE 2.2 is *GREAT*!).  So I don't know what to tell you."
    author: "Ian Schmidt"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-18
    body: "They broke Xminian Red Carpet as it will not know read the database."
    author: "Colin"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-10
    body: "The actual problem, which no one seems to understand, is that some of the packages needed for full support of KDE 2.2 are only available on RawHide -- they are not available in RedHat 7.1.\n\nSo, what should be done?\n\nClearly, if the RPMs are for the RawHide release, they should be labeled as such.\n\nBut, this does not solve the problem of what users with even totally up to date RedHat 7.x systems should do.\n\nSpecifically, it appears that to install on RedHat 7.1 that a \"kdesupport\" package is needed."
    author: "James Richard Tyrer"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-10
    body: "Well, I understand, and take note: even your idea of \"kdesupport\" would not work because of the dreaded RPM dependencies on the kde.org's Red Hat kdeadmin rpms. Only after your post in kde-linux in which you pointed out to an independent package of kdeadmin was I able to get an usable setup for KDE 2.2.\n\nThe labels on the ftp.kde.org site are DECEPTIVE. This is a RawHide setup and there is ABSOLUTELY NO INDICATION of that fact.\n\nThanks, Eduardo"
    author: "Eduardo Sanchez"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-10
    body: "The thing is that KDE libs would be (and on my system is) perfectly happy with RPM-4.02-8 as long as that is what it was built with.  So, the solution to that porblem, at least, should be very simple for the builder of the RPMs."
    author: "James Richard Tyrer"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-10
    body: "rpm's are too commonly dependant on some strange other package that takes too long to find and is dependant on some other strange package... it's always easier just typeing :\n\n./configure\nmake\nmake install\n\nsure, it's a little work with all the 20-30 separate kde files you gotta install to get it working :), but it's well worth it.\nwhile you're at it, build qt, and x11, and, and and.  \n\nreally folks it's not _THAT_ hard.  sometimes easier than the lousy rpms."
    author: "Mark"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-11
    body: "Now tell me: Have you ever had used a Red Hat KDE RPM installation? Many of us are left with Red Hat RPMs because we first used the KDE 2.1.2 environment that came with RH 7.1 and we have some KDE apps installed on top of them, some from source and some from RPMs. If you are acquainted with Red Hat RPMs you will recall that the Red Hat RPMs place the KDE files in a nonstandard placement. That's why you see RH users bitching about the broken RPMs. If they used their previous KDE 2.1.2 setup, then they're stuck with RH RPMs."
    author: "Eduardo Sanchez"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-11
    body: "how is it you say you're stuck with the redhat rpm setup?  it's perfectly possible to have the rh kde rpm's installed in where ever they put them, and to have your built kde system put into /usr/local/kde2.2.2/, and have qt into /usr/local/qtxx/\n\nyou'll need to set your kde path and qt path in the shell configuration files appropriately, and maybe create or edit a .Xdefaults (or something like that) file."
    author: "Mark"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-11
    body: "> build qt, and x11, and, and and. \n\nThat scares me.  Because that's what I do :)"
    author: "Yu Song"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-10
    body: "I use beros rpm's, and it does work. I also had a \nlittle bit of problems, but after a few days, he had \nput enough rpms in the non-kde directory, and it does actually\nwork, at least for me. But I agree it was a problem in the\nbeginning chasing dependencies.\n\nErik"
    author: "Erik Kj\u00e6r Pedersen"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-10
    body: "well.. i have no problems, but it's been a while since my RH7.0 looks more like a RH7.1/RawHide than a 7.0. i've constantly upgraded everything (except kernel), because there was always something not working.\ni had to recompile packages (not from kde though, but others that were needed), though, because sometimes the packagers at RH do some hard check of versions (version =, not >= or so), which should NEVER BEEN DONE UNLESS U HAVE A VERY VERY GOOD REASON. i recompiled and it worked, so they have no good reason.\n\nanyway, my brother is using Mandrake, and has no problems with it and KDE, any version: i think RedHat is simply not the good Linux distro for workstation. I'll switch soon."
    author: "loopkin"
  - subject: "Re: Will Red Hat RPMs be all messed up again?"
    date: 2001-09-13
    body: "Man, you too?  I thought it was just me.\n\nGlad to know I wasn't insane when my system got the colly-wobbles after installing \"official\" RH7.1 RPMs...\n\n:-)\n\nCheers,\n\nBullfrog\n\nPS, I would have built it myself from CVS, but I couldn't afford the length of time that takes on my system...\n\nPPS, How about a 2.2 to 2.2.1 patch?  C'mon guys, not all of us have broadband."
    author: "Bullfrog"
  - subject: "There is no RH packager"
    date: 2001-09-13
    body: "I agree with you, but AFAIK Red Hat isn't really interested in updating KDE for the stable release. One reason they are only building against a beta is that they want to update KDE for their NEXT release of Red Hat Linux. Users of the current release are out of luck unless they upgrade. IMHO, KDE really needs a volunteer to package RPMs for Red Hat. As long as no one steps forward (hey, don't look at me :-)), the RPM problems will probably continue.\n\nChris"
    author: "Chris Wong"
  - subject: "CodeWeavers Crossover Plugin + Quicktime"
    date: 2001-09-09
    body: "I hope that 2.2.1 addresses the problems that i have with embedding the Quicktime plugin using the nsplugin viewer in konqueror.I have not been able to use CW crossover plugin with anything but Netscape :-( ."
    author: "Kurt"
  - subject: "Wishlist: GNOME applets in KDE panel"
    date: 2001-09-09
    body: "It would be cool to be able to add GNOME applets into the KDE panel. It seems that GNOME has many more applets than KDE that I find useful like Weather applet, Gaim applet etc. Any news if that is doable ?"
    author: "Vladimir"
  - subject: "Re: Wishlist: GNOME applets in KDE panel"
    date: 2001-09-10
    body: "there is a kweatherapplet"
    author: "dikro"
  - subject: "Re: Wishlist: GNOME applets in KDE panel"
    date: 2001-09-10
    body: ">there is a kweatherapplet\n\nThat's a KDE 1 app. It still hasn't been officially updated for KDE 2, much less for KDE 2.2. Also, the beta version for KDE 2 isn't quite the same as a GNOME weather applet - in GNOME, the entire applet sits in the \"system tray\" - that is, there is no entry on the task bar. \n\nI personally would DEARLY love a decent weather applet."
    author: "Michael Leone"
  - subject: "Re: Wishlist: GNOME applets in KDE panel"
    date: 2001-09-10
    body: "LICQ (with KDE support compiled in) supports use of the kicker tray"
    author: "Carbon"
  - subject: "Re: Wishlist: GNOME applets in KDE panel"
    date: 2003-01-09
    body: "I think that the cool would be to be able to have *any* gnome applet in the system tray, not only a case-to-case answer..."
    author: "davux"
  - subject: "Re: Wishlist: GNOME applets in KDE panel"
    date: 2003-08-23
    body: "Hey Guy,\n\njust use gnome! with gnome you are able to use all applets and all kde things too.\n\nregards, Sven"
    author: "Sven"
  - subject: "$0.02 wishlist for KDE 2.x/3.x"
    date: 2001-09-09
    body: "These are a few suggestions from a very happy KDE user, \nwho would like to see KDE getting even better with each release.\n\n1. A graphical front end to XF86Config file. which will restart X \n   with the new settings and will retract to the old config if \n   the new settings fail or is not OK.\n\n2. Improved nsplugin support for Konqueror, expecially \n   if it could work with the codeweaver crossover plugins.\n\n3. Konqueror font size +/- option does not apply to all \n   frames in a page. I dont know if its intentional, if so please\n   provide an option for font size changes to apply to all frames \n   in the page.\n\n4. Provide multiple sets of icon themes, which might suit the taste \n   of diffrent users. ( kde classic, kde modern, kde gnomish , kde xp\n   kde mac-style etc )\n\n5. Consolidate kmail korganiser and other k pim apps under one suit\n   may be named K Lookout \n\n6. Take the most popular QT/KDE CD burning software ( koncd ? ) and\n   adopt it as the default KDE cd burner with a default easy inerface,\n   with an option to switch to expert mode."
    author: "hackorama"
  - subject: "Re: $0.02 wishlist for KDE 2.x/3.x"
    date: 2001-09-10
    body: "> 5. Consolidate kmail korganiser and other k pim apps under one suit\n> may be named K Lookout\n\nNo, dont. I really like to be able to read my email without starting a big program with a lot of integrated stuff that I don't use anyway. Why should I have to start the Palm-synching utility to read my email? I don't even own a PDA!\n\nBasse"
    author: "Basse"
  - subject: "Re: $0.02 wishlist for KDE 2.x/3.x"
    date: 2001-09-10
    body: "Well, that's what the famous component architecture of KDE is for. :)\nAll those app should be included into one shell, but all parts should only be started when needed. And they should work very well together.\nI think that could work. Somehow like the KOffice workspace."
    author: "Spark"
  - subject: "Re: $0.02 wishlist for KDE 2.x/3.x"
    date: 2001-09-10
    body: "Yeah, an app like koshell would be awesome. Knode and kmail need to look more similiar first tho."
    author: "Milo"
  - subject: "Re: $0.02 wishlist for KDE 2.x/3.x"
    date: 2001-09-10
    body: "> 2. Improved nsplugin support for Konqueror, expecially \nif it could work with the codeweaver crossover plugins.\n\nYeah, that would be awesome. \n\n\n\n> 4. Provide multiple sets of icon themes, which might suit the taste \nof diffrent users. ( kde classic, kde modern, kde gnomish , kde xp\n kde mac-style etc )\n\nYeah, there needs to be more icon themes.\n\n> 5. Consolidate kmail korganiser and other k pim apps under one suit\n may be named K Lookout \n\nYeah, I think kmail+knode+karm+korganizer+kaddressbook should be integrated more. Like it or not, most people are used to Outlook. I think something like konqueror should be done for these apps (like konqueror in that it'd be a \"shell\" program, which loads different parts). Also, some of these apps are really similiar (kmail and knode, although I like knode's interface before), so common code would make everything smaller. Also, it'd stop all the GNOME trolls in saying the KDE has no outlook-like app (Evolution). KDE in fact does, just in seperate apps :) .\n\n> 6. Take the most popular QT/KDE CD burning software ( koncd ? ) and\n adopt it as the default KDE cd burner with a default easy inerface,\n with an option to switch to expert mode.\n\nYeah, integrating a cd creation app would be awesome. I'd rather personally integrate k3b (http://k3b.sourceforge.net/). \n\nAlso, adding burning abilities through konqueror would be awesome too. Dragging and droping to make a cd in konqi, yum."
    author: "Milo"
  - subject: "Re: $0.02 wishlist for KDE 2.x/3.x"
    date: 2001-09-10
    body: ">Yeah, integrating a cd creation app would be awesome. I'd rather personally integrate k3b (http://k3b.sourceforge.net/). \n\nNever tried k3b, screenshots looks promising, its download time :)"
    author: "hackorama"
  - subject: "Re: $0.02 wishlist for KDE 2.x/3.x"
    date: 2001-09-10
    body: "> Also, it'd stop all the GNOME trolls in saying the KDE has no outlook-like app (Evolution). KDE in fact does, just in seperate apps :) .\n\nYou may want to check out Infusion at http://www.shadowcom.net/Software/infusion/"
    author: "Per"
  - subject: "Re: $0.02 wishlist for KDE 2.x/3.x"
    date: 2002-06-26
    body: "I would like to see a feature by wich we can specify the vertical and horizontal distance betweent the icons, as well as a custom size of the icons on the Desktop. Currently only icon sizes of 16x16 - 32x32 - 48x48 sizes are supported."
    author: "Krishna"
  - subject: "Other Apps"
    date: 2001-09-10
    body: "Hmm since kmail is in kdenetwork, will I have to wait untill januari if there's a new feature/better imap support  ?"
    author: "frido"
  - subject: "Re: Other Apps"
    date: 2001-09-10
    body: "Well, if you can't wait, you can always get the latest sources via anoncvs or have at least a look at the betas."
    author: "Michael H\u00e4ckel"
  - subject: "My Wishlist"
    date: 2001-09-10
    body: "Also putting my 2cents.\nMy changes would be more in konqueror or khtml\n\n\n1. tabbed browsing option\n2. khtml should not \"blank\" the page when going into a new page. it \"seems\" bit slower when going in a new page after you click on a link than mozilla. I noticed that mozilla shows the page after it has loaded at least the first screenful, and it does NOT blank the page to the bgcolor before displaying anything.\n3. another reason I think that khtml selections are slow are because it applies selection color based upon the color of the selected text. Mozilla does not do this (and has a default blue selection color, depending on the theme). \n4. It'd be REALLY nice to drag text from konqueror, and not just links\n5. It'd be really really nice to drag images from konqueror\n6. Make he tab key lets you navigate around things in konqueror."
    author: "c4"
  - subject: "Re: My Wishlist"
    date: 2001-09-10
    body: "1. I guess this is becoming a FREQ item. :)\nHopefully it will be implemented.\n\n2+3: I doubt that it's that easy. :)\n\n4: Oh yes. A must have.\n\n5: Yeah, I would also like to have a \"use image as background\" option or something like that. \n\n6: Works for me. Just not when I'm typing something into fields like this, but I think that's ok. :)"
    author: "Spark"
  - subject: "KMail Wish List"
    date: 2001-09-10
    body: "The two things I would like to see added to Kmail.\n\n1. That you maintain the tool bar when you open a message to full screen. This is the view I prefer for reading messages. Then I have to close it to read the next message. I would prefer to have the previous/next arrows.\n\n2. I frequently get messages sent as attachments, sometimes 4 or 5 deep. I would like to be able to display the attachments as one big message ( like Hotmail and Eudora).\n\nAlso as a low priority wish, I would like to see KPacman ported to KDE 2.x & 3.x. and added to the games. I really like this game, but can only use it with KDE 1.1.2.\n\nI would like to say thanks to all the KDE developers for all their hard work. You've produced a great product that just keeps getting better.\n\nBrian"
    author: "Brian Murphy"
  - subject: "Re: KMail Wish List"
    date: 2001-09-10
    body: "Also it will be great if kmail will use SOCKS settings...\nFor now socks support is working only in konqueror.\nOf course, I can modify the startkde script and run kmail with runsocks but it is not so clear... Especially startkde modification..."
    author: "Alexei Dets"
  - subject: "My Only Wish.."
    date: 2001-09-10
    body: "Hey, I'd like to see all kwin themes have the ability to control button positions/hiding buttons.\n\nAnd props to all KDE developers. I'm looking forward to KDE 3 :)."
    author: "MegaBite"
  - subject: "Re: My Only Wish.."
    date: 2001-09-10
    body: "I've made B2 do that now, with the rest to follow shortly.\n\nCheers,\ngallium"
    author: "gallium"
  - subject: "Re: My Only Wish.."
    date: 2001-09-11
    body: "I always wondered why the \"Buttons\" tab wasn't grayed out like the \"settings\" tab when you couldn't change the buttons.  It was kind of confusing  I guess if all the themes support it, though, it won't be a problem anymore.  (what about the IceWM theme support though?)"
    author: "not me"
  - subject: "Re: My Only Wish.."
    date: 2001-09-14
    body: "The buttons tab does get greyed out if you don't select the custom button positions checkbox. Which settings tab are you talking about? The configure tab? \n\nWith the IceWM theme support in KDE 2.2, the buttons are customisable, as are the themes. I don't understand your post, please clarify what you mean."
    author: "gallium"
  - subject: "own wish"
    date: 2001-09-10
    body: "I think that KDE Menu is too crowded. How about moving most of the top level subcategories to a \"Program\" subcategory. It'd appear a LOT less crowded then. Right now, I think that there is a plethora of items in the Kmenu, which sucks for usability and low resolutions. Fix it :p.\n\nI also look forward to KDE 3.0."
    author: "Locke"
  - subject: "Re: own wish"
    date: 2001-09-10
    body: "No, I don't think so.\nHaving to go one submenu deeper as in windows is less userfriendly.\nIf you don't like it, it's easy to change the menu on your harddisk, it's only a bunch of .desktop files which have to be moved around.\n\nAlex"
    author: "aleXXX"
  - subject: "Re: own wish"
    date: 2001-09-10
    body: "> Having to go one submenu deeper as in windows is less userfriendly.\n\nI agree with you. However I think it would be good to gather in one menu item \"Parameters and  help\" the entries Configure, Control Center, Kprinter, Help...\n\n> If you don't like it, it's easy to change the menu on your harddisk,\n\nI think it is not really easy. It is heavy. A dynamic change of the KMenu, like in MS Windows, would be useful..."
    author: "Alain"
  - subject: "Re: own wish"
    date: 2001-09-10
    body: "> I agree with you. However I think it would be good to gather in one menu item \"Parameters and help\" the entries Configure, Control Center, Kprinter, Help...\n\nI would prefer one submenu for \"Preferences\", which shows all the KControl modules and KControl itself. ATM there is a \"Preferences\" submenu, but it's somewhere in the middle of the applications, not a good idea. :) Should be seperated.\n\nPreferences are preferences, apps are apps...\n\n> I think it is not really easy. It is heavy. A dynamic change of the KMenu, like in MS Windows, would be useful...\n\nI agree, although I don't know how it works in MS Windows. It would be nice to be able to move menu items around. That's not that easy, so there should also be a \"Menu Editor\". This should only open a window with all submenus as folders, so you can move those items around like usual files. I think that would be the most easy way. BTW: Same could be done for Bookmarks.\nIf you treat every file, menu entry, bookmark as an object, you can get a very consistens user interface.\nYou don't have to use the \"everything is a file\" metapher, much better is the \"everything is an object\" metapher IMO, although there is not much of a difference. :)"
    author: "Spark"
  - subject: "Re: own wish"
    date: 2001-09-11
    body: "There is a menu editor, and it works sort of like you describe.  It should appear in your K menu but if it doesn't try right-clicking the K menu and select Preferences.  There is also a bookmark editor which is pretty nice.  How did you miss those? :-)"
    author: "not me"
  - subject: "Re: own wish"
    date: 2001-09-11
    body: "Hmm no, I would prefer them to show up in a Konqueror window, so I don't have to learn a new app. :)\nJust a suggestion thought. Maybe it's not the best idea. :) \nBeOS does it this way and it works very good. Even mails are files and you can move, sort, search, whatever them just like other files.\nBut this does only work with an enhanced filesystem... too bad, that KDE can't expect a special filesystem."
    author: "Spark"
  - subject: "Re: own wish"
    date: 2002-10-20
    body: "I've always played with the thought of implementing a menu-KIO-slave so that a file-view could be accomplished.\n\nDragging files to this slave would create .desktop-files etc....\n\nis this already done????\n\n"
    author: "Jens Nordenbro"
  - subject: "another one"
    date: 2001-09-10
    body: "Tooltips should be displayed in the default monospaced font. Not proportional."
    author: "Monor"
  - subject: "Re: another one"
    date: 2001-09-10
    body: "No. Can't think why you would want this!\n\nBut a seperate definable font for tooltips is what you really want, perhaps. If there isn't one all ready (there isn't in 2.1 - not looked at 2.2)."
    author: "Corba the Geek"
  - subject: "kmail: easy to use spam to trash"
    date: 2001-09-10
    body: "hi,\n\nkde2 is great!!!\na. would be good to have a rightclick menu to add the sender to a spam list.\nb. to set a rule how to handle it (-> move to trash)\nc. --> so the filter list will be clearer because there is an extra spamlist\n\nthanx to all developers\n-gunnar"
    author: "gunnar"
  - subject: "Re: kmail: easy to use spam to trash"
    date: 2001-09-10
    body: "I would also like an option to \"ask before downloading\" a mail with attachments.\nOr something like that.\nThere is a nasty worm going around here that sends a lot of mails with attachments and can really make checking for email a nasty thing. :(\nIt always has different titles and different attachments, only the body is the same. \nSometimes it would be nice to delete those mails without downloading them. Also usefull if some kid thinks it's fun to send mailbombs."
    author: "Spark"
  - subject: "just a wish!"
    date: 2001-09-10
    body: "Must say that KDE has improved enourmosly since I first tried it in 1996. But there is one thing that has irritated me ever since.. \n\nWhy must the \"K\" menu look and work like \"Start\" in windows, it even works worse and is uglier? The \"Start\" menu has never worked very well in windows either from a usability point of view... it just confuses the user and makes it hard to find an application. As everybody now, home users has tons of applications installed and the \"Programs\"-folder in windows grows several pages long.. not very easy to find The applications you are looking for... and KDE suffers from the same. Maybe ever worse because often an application doesent connect to the name of the folder it is found in. \n\nA complete new application access theory is needed! Something that the world has never seen before!\n\nKDE could lead the way of a complete new navigation theory, must say that I havent figured how yet, it could really accomplish something instead of just copying bad ideas from other os's. This would be a BIG step ahead in competion. As easy access to applications is one of the most important ideas of a modern GUI.\n\n-daniel-"
    author: "daniel"
  - subject: "Re: just a wish!"
    date: 2001-09-10
    body: "I agree although you must give credit to KMenu for putting commonly used apps in the root.  It has always amazed me that MS don't do this and their equivalent, Personalised Menus, is pathetic and simply doesn't work.\n\nWe should strive to produce a unique way to launch applications rather than stick with the START menu idea which has never worked IMHO.\n\nI use ALT-F2 - much better than any menu system!!"
    author: "Paul Drummond"
  - subject: "Re: just a wish!"
    date: 2001-09-10
    body: "Looking forward to hear your ideas  :-)\n(seriously)\n\nAlex"
    author: "aleXXX"
  - subject: "Re: just a wish!"
    date: 2001-09-10
    body: "Probably the best solution would be: customizable \"drawers\" for Kicker.\nAnyway, Alt+F2 rules."
    author: "AB"
  - subject: "Why not K button in the Mac-like global menu?"
    date: 2001-09-10
    body: "I personally like the Macintosh style global menu at the top, but I tend to not use it because it doesn't have the K menu... I would have to also use the taskbar at the bottom which does have it, and I don't like using 2 bars.\n\nAny chance the Mac-like global menu will eventually have a K button over on the left similar to how MacOS has that \"Apple logo\" menu?\n\n(pardon me if it does and I overlooked it. I'm still using 2.1 though)"
    author: "PRR"
  - subject: "Re: Why not K button in the Mac-like global menu?"
    date: 2001-09-10
    body: "Seems that Mosfet is your man. :)\nAccording to his website, he is currently working on this:\nhttp://mosfet.org/macmenu2.jpg"
    author: "Spark"
  - subject: "Re: just a wish!"
    date: 2001-09-11
    body: "Ever since I can remember, I've been using desktop icons to launch frequently used applications. I've done this while using Windows and MacOS.\n\nHowever, while using KDE, I don't ever recall using desktop icons. I just don't know why. I usually either click on buttons in kicker or launch from a konsole instead.\n\nAnyone else notice this (not using desktop icons in KDE). Curiously enough, I *did* use a lot of desktop icons in KDE 1.x. I don't really know why, but I still like the *feel* of kfm, even though I know that konqi is a much better application."
    author: "Larry Konzac"
  - subject: "Re: just a wish!"
    date: 2001-09-11
    body: "people like the Ideas of Icons, the see them as objects, menuse are harder for people to understand. if oyu set up a directory with a special Icon and call it applications, when they open it, they see the applications broken down into groups, internet, office, development, and the misc can just be at the top level. that way, people can just look for the application, it is very much like the mac system. BTW I also like how mac staticly links its applications so you can move the application around with out breaking it or relinking it to the correct library. I think they use Hexbin files. why can't Linux use this meathod of program distrobution?"
    author: "Jeremy Petzold"
  - subject: "since 1996?"
    date: 2001-09-12
    body: "http://www.kde.org/whatiskde/proj.html#A bit of KDE History\n\nYou probably also used Win95 in 1992 and Linux back in 1989 eh?"
    author: "Danny"
  - subject: "Re: since 1996?"
    date: 2001-09-12
    body: "Since 1997.. thanks for correcting me.\n\n-daniel-"
    author: "daniel"
  - subject: "Maybe Like THE BRAIN"
    date: 2001-09-18
    body: "I have always liked the idea of THE BRAIN. It might be changed enough to be a new and effecient file system explorer/desktop for KDE. Just a thought. E"
    author: "Mr Frog"
  - subject: "Re: Maybe Like THE BRAIN"
    date: 2001-09-19
    body: "What's BRAIN? do you mean the stuff inside my head or just a name of something else?\n\nANYWAY,\n\nI was thinking, what about using hi-tech stuff, such as voice recognition?\nWell I guess that sort of stuff is either slow or expensive right now (open source would be GREAT!)\n\nAlso, how about using GESTURES, like in Blender?\nToday I was watching some guys using this expensive software package for designing printed circuit boards, and they were hardly TOUCHING the keyboard! They use gestures for everything, from zooming in and out to closing a window to bringing up a help window! Well I think maybe there could be some sort of alternate menu, if anyone can think of one, but also use user-defined gestures for commonly used stuff...?\n\njust a thought."
    author: "Luke"
  - subject: "RPM broken"
    date: 2001-09-10
    body: "I have both KDE 2.2 and Ximian GNOME 1.4.\nSome libraries from kde-pim rpms have same name of Ximian Evolution's libraries."
    author: "Dratomix"
  - subject: "Re: RPM broken"
    date: 2001-09-10
    body: "What version of Evolution do you have?"
    author: "Evandro"
  - subject: "couple of missing things."
    date: 2001-09-10
    body: "A few things are missing in kde:\n\n1) Find in ftp directories (ever tried to search a specific rpm on a ftp mirror?)\n\n2) _configurable_ right click menus in, for example, konqueror. So we can finally:\n      - add to zip, tgz, bzip2\n      - send to kate, kword, kmail\n      - etc\n\n3) Change screen resolution & virtual screen size. (I know, I know, this is an XFree thingy, but RandR extension is progressing terribly slowly, Maybe a dirty\nQT hack can solve this by just telling the apps screen size is now 800x600 even if\nthe virtual screen is 1024x768??)\n\n4) Copy/paste of Objects like spreadsheat cells, pictures, etc. Or is QT3 already doing this?\n\n5) Control panel for system resources. I know we have a lot of system information in the control panel, but loading/unloading modules + setting\nparameters like irq, dma io etc. would be nice. Also nice for turning off/on\nDMA, etc on you HDD.\n\n\nDanny"
    author: "Danny"
  - subject: "Re: couple of missing things."
    date: 2001-09-10
    body: "Actually, you could have \"send to zip file\" in the RMB since before KDE 1.0, and it is not even hard to do :-)\n\nStep a: find a simple tool that does what you want. If what you want is for it to ask what zip file the current file/dir should be added to and just add it there using zip -a (or whatever zip uses), a simple ten line shell script should be enough, a nice GUI can be added if you want, should be VERY easy.\n\nStep b: Save that script/program/whatever somewhere in your PATH withj the name \"Send to ZIP File\"\n\nStep c: associate \"Send to ZIP File\" with the default filetype in konqueror.\n\nVoil\u00e1.\n\nI should really write an article with this kind of things :-)"
    author: "Roberto Alsina"
  - subject: "Re: couple of missing things."
    date: 2001-09-11
    body: ">I should really write an article with this kind of things :-)\n\nWhat you *should* do is create the script and send it to the kde-devel mailing list so it can be added to KDE.  That way all users of KDE can benefit from your little additions, with no effort.\n\nKDE is sorely lacking nice right-click associations such as this one.  If you created a bunch and mailed them to the developers, I'll bet they would be added."
    author: "not me"
  - subject: "Re: couple of missing things."
    date: 2001-09-11
    body: "Hell, I could add them myself if I was so inclined!\n\nBut really, something explaining how to do these things means users can create them too. And as you know, there's plenty more of them, and they think they know what they want ;-)"
    author: "Roberto Alsina"
  - subject: "ftp"
    date: 2001-09-10
    body: "Sorry for my poor english... :)\n\nWith KDE 2.2, we can create a ftp link in konqueror ( ftp://login:pwd@server if i remember) using 'http link', but.\n\nUnfortunately:\n1) It's hard to remember. It's would be cool to add \"ftp link\" in the right mouse list, with the differents options like an ftp client (ftp server, login, pwd, passive mode,...).\n\n2) Actualy, we can't edit file from ftp like 'UltraEdit' on Windows. On Kate for example, it edit the file \"ftp link\" itself, but don't browse the server to select a file like an harddrive. It's unless..."
    author: "Johann Ollivier-Lapeyre"
  - subject: "Re: ftp"
    date: 2001-09-10
    body: "2) Actualy, we can't edit file from ftp like 'UltraEdit' on Windows. On Kate for example, it edit the file \"ftp link\" itself, but don't browse the server to\nselect a file like an harddrive. It's unless...\n\n\nI suppose you mean \"it's useless\"?\n\nYou are doing it backwards.\n\nYou don't open the link with kate, you open the link, then use kate to edit whatever is in inside the FTP server."
    author: "Roberto Alsina"
  - subject: "Re: ftp"
    date: 2001-09-10
    body: ">> You don't open the link with kate, you open the link, then use kate to edit whatever is in inside the FTP server.\n\nI just load up Kate, and hit \"Open File\", and enter ftp://user:pass@ftp.server.com/ (add it to the bookmarks the first time).  Browse the remote filesystem, and load and save files.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "SuSE/Pre-linking bugfixes?"
    date: 2001-09-10
    body: "A couple of issues with SuSE distributions that I'm wondering if they have been fixed or will be fixed in this or future releases: \n\n1) objPrelinking (found under the experimental i686 rpms). Supposedly makes things run faster, I guess, however, having these features set up causes Konqueror (greatest Linux program EVER, btw) to crash on the simplest Javascript functions. This has been documented and a solution found at the konq faq said, \"Don't use the i686 rpms\". WTF? That's not a solution.\n\n2) Not sure if this is a Kde issue or not (but I think it is). After installing X4.1.0, kdm doesn't load anymore. It bails out with\n\n\"/opt/kde2/bin/kdm: error while loading shared libraries: /opt/kde2/bin/kdm: __\nundefined symbol: _XdmcpWrapperToOddParity\"\n\nAgain, this has been documented (somewhere I forget). It is also features a non-solution reading something on the order of \"this version of kdm is incompatible.\" This causes alot of problems for a newbie idiot like me (such as how do I switch WMs now?)\n\nSorry to bitch, because you K developers have got to be the creme da le creme if I've ever seen it. Hope to join you some day.\n\nRegards,\n~Z"
    author: "Zaq Rizer"
  - subject: "Re: SuSE/Pre-linking bugfixes?"
    date: 2001-09-10
    body: "2) Not sure if this is a Kde issue or not (but I think it is). After installing X4.1.0, kdm doesn't load anymore. It bails out with\n\n\"/opt/kde2/bin/kdm: error while loading shared libraries: /opt/kde2/bin/kdm: __\nundefined symbol: _XdmcpWrapperToOddParity\"\n\nThis is not a problem with KDE, it's a problem with your binary KDE packages. I use KDE 2.2, compiled from source and X 4.1.0 and there is no problem with kdm."
    author: "Christian Hengstebeck"
  - subject: "My one Wish"
    date: 2001-09-10
    body: "I have a real problem with the idea that All data is saved to files. Likewise that the file to go to the Printer is somehow unique. It should be apparent that the idea of Export/Import suggests that the File Menu is incorrect. It is simply a hang-over from days gone by. \n\nInstead, there should be three possible types of Streams being outputed. \nGeneral, most likely a local format.\nRecord-Oriented (perhaps xml based).\nPage-Oriented (postscript) . \nAt that point, then should have a dialog from which to send the Stream, that is, record-oriented can go to Files, Databases, E-Mail, IM, etc.\nPage-oriented can be sent to the Printer, but could also be converted to images, or perhaps Fax (tiff vs postscript).\nOffer filters for converting from one format to another to be used across all apps e.g. portscript -> jpeg, postscript -> tiff, My application specific General Stream -> html.\nFinally, change the menu to reflect it.\n\nLocation\n\tNew\n\t--------\n\t(Defaults)\n\t--------\n\tSend To...\n\tGet From...\n\t--------\n\tClose\n\tExit\n\n\n(Default) should be a user modifable area where they can place the common ones, such as File, or Print.\n\n\nThe Send To should bring up a Dialog for sending the Streams to various areas. General Stream available => File System, Http, Ftp, E-mail, another Application (think pipe).\nRecord Stream available => same as General Streams, Database.\nPage Stream available => same as General Streams, Printer, Fax."
    author: "G. Richard Raab"
  - subject: "Just one tiny wish"
    date: 2001-09-10
    body: "Please separate out the Linux-only software into their own package. Since KDE is more than just a Linux-only desktop, it makes no sense to make non Linux users download software they will never use. Most of these tools (which are fine Linux tools, by the way) are in the kdeadmin package, but a few are not. Even stuff like the Tux icons are inappropriate outside of Linux.\n\nI know most of you guys develop on Linux, and that some of you don't even realize the existance of other operating systems, but there are a few of us out here using other Unix systems.\n\nBy placing these programs into their own package, such as kdelinux, they can still be considered \"default\" programs by Linux distros and users."
    author: "David Johnson"
  - subject: "Re: Just one tiny wish"
    date: 2001-09-11
    body: "I disagree about the Tux thing. Tux is not Linux and causes no imcompatibility under other operating systems."
    author: "Carg"
  - subject: "Re: Just one tiny wish"
    date: 2001-09-12
    body: "Tux, might not be Linux but it symbolizes Linux.  Those of us that prefer not to  run Linux do not want to be subjected to the Linux mascot. Why should people who run BSD, Solaris, Tru64, etc have to view the image of Linux's mascot on our systems?  We prefer not too, and therefore ask the developers nicely if they can do something about that."
    author: "Joey Garcia"
  - subject: "Re: Just one tiny wish"
    date: 2001-09-12
    body: "I second this motion.  I totally agree with you here.  I think it's pointless for me to have those Linux applications which will do me no good.  Although, sometimes you might build from source and it builds everything.  Perhaps there should be a way to check the OS and build applications that ware suitable for that OS in order to keep the needless applications down to a minimum.\n\nAlso, in the Kontrol Center application where you can adjust the settings for various things there is an option to view some of the system's status which doesn't seem to work outside of Linux.  I'm thinking these things should be conditionalized to the point that if it's not going to be built for Linux, then don't build those parts of it.\n\nAnyawys, that's just my opinion."
    author: "Joey Garcia"
  - subject: "Wish: thumbnails, previews and konqueror"
    date: 2001-09-10
    body: "I think that alongside with the option to show thumbnails of files as their icon in konqueror, there should be possible to preview them in the right-click menu, just like picaview does in windows (but for more than just images), perhaps with an option to just show a square reading \"preview\" until the user clicks it. This would give the users some of the advantages of thumbnais without having to process an entire directory along with some of the advantages of the embedded image viewer, without having to constantly go back and forth."
    author: "Carlos Rodrigues"
  - subject: "Dark widget issues"
    date: 2001-09-10
    body: "I have a really cool CDE-ish green color theme, and earlier I had a nice blue one.  The blue one I had to get rid of because on the virtual desktop, the numbers wouldn't display.  My new white on dark green one, which is shown in the attatched screenshot, has a white window background and white text for buttons and menus.\n\nThere is a problem with the calendar widgets, and I've seen it one other time and I don't remember when.  The white toolbar, status, etc. text gets put on the white calendar background.  This is shown in the attatchment.\n\nSo I want this improved.  I'd like the white on white problem fixed, as well as the nearly invisible desktop numbering when it's dark blue.\n\nAlso, it would be nice if the dark-buttoned konqueror widgets had white text as well.\n\nKDE would then be the best supporter of the dark-buttoned look."
    author: "Benjamin Atkin"
  - subject: "Re: Dark widget issues"
    date: 2001-09-11
    body: "looks very nice.. yeah, I agree that more dark-friendly changes need to be made or KDE will be very racist. heh :)"
    author: "anonymous ab00ser"
  - subject: "kcontrol issues"
    date: 2001-09-10
    body: "I think that the KDE control panel needs to be reworked. Right now, it is *way* too cluttered, and is thus hard to find things. I think a better approach would be to have it like MS-Windows or MacOS, in which the top level control panel shows large icons (the current things in the tree. Each of these, when clicked, would open a new window with the options.\n\nAs for KDE applications, I think that a little bit more consistance is good, especially in menu items. For example, decide if you want to call the preferences/options/settings menu item in an app one or the other. It varies quite a bit in KDE applications.\n\nIt'd also be nice to configure the Kate editor part's options in kcontrol, as you can already configure Konqueror and kwin options there. \n\nIf you fix these in KDE 3.0, it'll make my day. If you don't, that's ok too :)./"
    author: "purity"
  - subject: "Re: kcontrol issues"
    date: 2001-09-10
    body: "> I think that the KDE control panel needs to be reworked\n\nWe are just about to do that :)\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: kcontrol issues"
    date: 2001-09-11
    body: "Nice! What are you guys planning?"
    author: "purity"
  - subject: "Wishes"
    date: 2001-09-11
    body: "Hi, this is what I'd like to see in KDE 3, in addition to most of the other wishes already:\n\n1. Make all menus tearoffable\n2. Menus should be tearoffable by simply 1). clicking on a menu while still holding the mouse down 2). move the mouse down under the menu 3). a shadow of the menu would then popup, and it can be moved wherever\n\n3. move the tearoff handle in menus to the TOP, instead of the bottom, like where it is now. This is especially more better in terms of usability for long menus.\n\nThank You!"
    author: "theref"
  - subject: "Re: Wishes"
    date: 2001-09-11
    body: "Tearoff menus are nice, no more running thru the same menus to do the same task repeatedly, but... the tearoff item looks ugly in some menus, like the K menu, so there should be an option to disable it (for the K menu), better yet... two options, \"Enable tearoff menus\" and \"Enable tearoff K menu\".\n\nAnd while at it, make an option to disable the fade in taskbar buttons, I really prefer that \"...\" instead.\n\nBottom line: options are nice."
    author: "Carlos Rodrigues"
  - subject: "My Wish"
    date: 2001-09-11
    body: "Hi, I want the KDE splash screen to have a new image and for every version after that!\n\nI think a simpler white background with KDE in big black helvetica letters and the cog motif somewhere there."
    author: "kc"
  - subject: "More wishes"
    date: 2001-09-12
    body: "Most of the wishes I've seen here sound like good ideas.\n\nMy huge wish:\n\nA new session management setup.\n\nSince many of us now have access to *nix on several machines, a really useful thing the KDE could broker would be keeping running apps inside a session, and manage those apps.  When you log out, you would have the option of killing all your running apps, putting them to sleep, lowering their priority, suspending them to disk, or leaving them as they are.  Further advancements would be a graphical way to detach processes and leave them running (like compilers, for example)\n\nThen when you return to login via KDM, you could return to the session you left alive, or start a new session (on a new display, if you're starting a new session on the same machine).  All of this would be run over SSL, meaning that we wouldn't have to worry in the future about security complaints (or at least they'd be easy to fix).  And one could easily add VPN support.\n\nI don't know how hard it would be , and I would welcome comments on where the difficulties might lie.\n\nUses:\n\nI leave my box running at home when I go to work.  My roommate could come and login and use apps that run out of her own home directory, without me killing all my own apps.  Since I have a 1GHz box, this shouldn't be all this big a deal.\n\nI leave apps running at work, and could pick up a session right where I left it, either from home or work, just by logging back into that session.\n\n\nThoughts about this.  I think it would be excellent.\n\nCheers,\n\nEric"
    author: "Electroniceric"
  - subject: "Re: More wishes"
    date: 2001-09-17
    body: "Hi Eric,\n\nthis sort of session management sounds like a real good idea.\nBut it sounds like a heck of a job because of all needed interaction. I would be a lot of work _and_ it is not really required.\n\nIf you have a lot of MHz and RAM you can reach your goal (in this special prob. with your roommate) in this way:\n\nLock your running box with a blank screensaver (passwd activated). Then change to a new basic console (e.g. ctrl+alt+F2).\nAfter this your roommate has to log in with his/her username and passwd. Then he or her will be able to start a second X-session with:\n\nstartx -- :1\n\n(BTW: There is a space between \"startx\" and \"--\" _and_ there is a space between \"--\" and \":1\"  [The most common failure ;-)] )\n\nVoila. A independent X-session will open with the standard windowmanager!\n\nAt least when you use SuSE 7.2 or other systems with support the WINDOWMANAGER variable then also those versions should also work:\n\nWINDOWMANAGER=\"insert_here_your_favorite_windowmanager\" startx -- :\"Number\"\n\nE.g.\nWINDOWMANAGER=kde startx -- :2\nor\nWINDOWMANAGER=gnome startx -- :1\ne.t.c.\n\nThis works very fine for me ;-)\nNow you can change between those sessions with e.g. alt+F8 and alt+F7 or whatever your standard for X is.\n(Do not use :0. This is normally the default X-session.)\n\n(Warning: It should work, but if you log in twice as the same user you maybe will have some probs. I have also observed strange effects when i use two different windowmanager at the same time in two independent X-sessions. I dont know why. :-(  IMHO it should work. But maybe thats only a effect on my custom build box with its strange configuration. So my fault. ;-)  )\n\nHave fun\nIngmar"
    author: "Ingmar"
  - subject: "The Question: WHY ARTS ?"
    date: 2001-09-13
    body: "I've never seen any satisfactory answer to that:\nwhy does KDE use aRts ???\n1- this is the worse piece of software included in KDE, it leaks memory like hell (seems worse in kde 2.2 than before, btw), or it crashes without any reason...\n2- everybody outta here use ESD (look at flash, realplayer, etc.)\n\ncan't we modify ESD in order to give him an aRts interface for backward compatibility, and go on that way ? or is MCOP compatibility too hard to implement ? in this case, why not implementing ESD compatibility ?\n\nanyway, maybe i'm tough, but it's very frustratring that it crashes all the time, and not to be able to use esd-compliant progs (artsdsp doesn't work well at all). or maybe i'm the only one still using an old sound card with only one audio device ?\nbtw, KDE 2.2 has decreased memory consumption, but processes are more keen to have memory leaks. after a quick look at bugs.kde.org, seems they're all known... waiting for 2.2.1... maybe they will be a 2.2.2 ? ;-)"
    author: "Loopkin"
  - subject: "Re: The Question: WHY ARTS ?"
    date: 2001-09-13
    body: "A 2.2.2 release wouldn't be such a bad idea since 3.0 is only due to late February."
    author: "Carlos Rodrigues"
  - subject: "One more wish (unicode fonts)"
    date: 2001-09-13
    body: "I wish they'd fix the problem with utf-8 charset webpages, I've made some changes to ttmkfdir2 to make my truetype fonts available as unicode and that fixed most of the problems, but for pages that rely on the default font it still looks really ugly (take a look at http://www.kernel.org). I would like if they just defaulted to using the default charset fonts if the page doesn't specify the fonts itself, like all other browsers do."
    author: "Carlos Rodrigues"
  - subject: "Wish"
    date: 2001-09-14
    body: "Hello !!\n\nYou should implement full support for the smb ioslave (read and write).\nIt's appears being read-only in 2.2....\n\nThanks"
    author: "Macolu"
  - subject: "Re: Wish"
    date: 2001-09-14
    body: "We already have a new SMB slave that supports read-write access that has been developed by Caldera. Unfortunately it needs the new SAMBA library which is not yet included in any of the stable SAMBA releases.\n\nRich."
    author: "Richard Moore"
  - subject: "A wharf for KDE"
    date: 2001-09-15
    body: "I've attached a screenshot of stuff I've been doing while my internet has been out.\n\na) It's obvious the k-menu is copying from microsoft's start button\nb) wharfs are easily more usuable than that\nc) wharfs (and NeXT's user interface), at least in my opinion, is the pinnacle of UI design, it does everything right, at least with their \"panel\" : It puts it on the right-top, it makes the icons larger\nd) starting an app with kicker takes very much siginificantly longer than a wharf: (kicker) move mouse, click, move mouse, move mouse, move mouse, click; (wharf) move mouse, click, move mouse, click\ne) Users have difficulty identifying apps for the first time by icon, all wharf buttons can have captions."
    author: "Charles Samuels"
  - subject: "KDE 2.2.1"
    date: 2001-09-18
    body: "Where is it? It's the 17th and it's nowhere to be found"
    author: "Tannhaus"
  - subject: "Re: KDE 2.2.1"
    date: 2001-09-18
    body: "\"Did the day ended ?\" \"It's still 17th.\" \"it's still September\n17th for many more hours in California, which is where the\nrelease guy lives.\"\n\nThese are some answers from #kde and #kde-users channels on\nirc.openprojects.net I got when asked the very same question.\n\nSo far there is an empty directory /pub/kde/stable/2.2.1 at\nftp.kde.org"
    author: "monarch"
  - subject: "Re: KDE 2.2.1"
    date: 2001-09-18
    body: "Got mine at SuSE ftp site. Been there since the 16th. They are good with keeping KDE updates - up-to-date."
    author: "SuppCast"
  - subject: "Re: KDE 2.2.1"
    date: 2001-09-18
    body: "The ReHat Roswell ones are well and truly screwed up the directories are all files about 4 to 6 mb insize??"
    author: "Colin"
  - subject: "So where is it?"
    date: 2001-09-18
    body: "Today is Tuesday September the 18th 2001 and it still hasn't been a Offcial release for KDE 2.2.1 on www.kde.org, and so far there is no sign of the release except the directory. Don't feel rushed, take your time and do a good job.\n\nThanks"
    author: "MattB"
  - subject: "Things that make you go KKKKKKKKKKKK..."
    date: 2001-09-19
    body: "Well, here's my input.\n1. Perhaps the k-menu could be turned into an applet, which has many reincarnations and the user can choose whichever suits them\n\n2. Is it hard to implement animated cursors? how about colour cursors? well it's just that I go to use windoze and it has this fancy cursor, much more configurable and better looking than the X one. maybe KDE can implement this?\n\n3. I second the idea for a theme/style creater.\n\n4. Maybe some fancy animations? like a button which flips over when the mouse is over it? or like the superflous option in WindowMaker the window explodes?"
    author: "Luke"
---
<A HREF="http://www.kde.org/people/waldo.html">Waldo Bastian</A>, the KDE 2.x release coordinator, has <A HREF="http://www.kde.com/maillists/show.php?li=kde-core-devel&f=1&l=20&sb=mid&o=d&v=list&mq=-1&m=528736">announced</A> that KDE
2.2.1 was packaged as a tarball yesterday and will be officially released on September 17, 2001.  Those
of you expecting the release this Monday will be happy to know that the extra week was used to squash bugs.  At about the same time, <A HREF="http://www.kde.org/people/dirk.html">Dirk Mueller</A>, the new KDE 3.x release coordinator (thanks, Dirk!), <A HREF="http://developer.kde.org/development-versions/kde-3.0-release-plan.html">posted</A> the <EM>projected</EM> KDE 3.0 release schedule.  The short and sweet:  first beta on December 3, 2001, first release candidate on January 7,
2002, and KDE 3.0 on February 25, 2002.  Unlike the KDE 1 to KDE 2 change, this one
should be much smoother:  Qt has changed far less, and very few (if any) core applications will be completely rewritten, as many were for KDE 2.


<!--break-->
