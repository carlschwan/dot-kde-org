---
title: "Review: theKompany's Universal Documentation Tool"
date:    2001-07-27
authors:
  - "kdeFan"
slug:    review-thekompanys-universal-documentation-tool
comments:
  - subject: "Re: Review: theKompany's Universal Documentation T"
    date: 2001-07-27
    body: "When Ximian creates open source software under the GPL and LGPL, everybody complains about how bad that is and how they try to control GNOME, but if TheKompany or any other KDE company creates closed source software, nobody complains about it.\nHow pathetic."
    author: "dc"
  - subject: "Re: Review: theKompany's Universal Documentation T"
    date: 2001-07-30
    body: "I know you are just a trolling, but there is a whole website for freeing KDE.  So you are quite wrong.  To some of us it is obvious that theKompany is not going to control KDE with its docu tool."
    author: "ac"
  - subject: "Re: Review: theKompany's Universal Documentation T"
    date: 2001-07-30
    body: "Yea you should be able to dictate what people do with there own money right? We should make the kompany give away all there software!!  No your the one thats pathetic. Get a life. If you don't want it don't buy it. When Ximian gos under in the next 12 months from a lack of revenue  you probably still won't get it.\n\nCraig"
    author: "Craig"
---
<i>"If you've ever wanted to get into programming for either KDE or Qt but weren't sure where to begin, <a href="http://www.thekompany.com/products/docbrowser/">theKompany's DocBrowser</a> is a pretty good place to start. It features a truly hyper-linked set of documentation for KDE and Qt's API, flow diagrams for how code relates, and easy to understand documentation of the calls you want to use."</i> Get the rest of <a href="http://GUI-Lords.org/article.php?sid=65&mode=&order=0">the review here</a>. [<b>Ed</b>: This is closed-source commercial software.]
<!--break-->
