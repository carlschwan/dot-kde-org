---
title: "Telsa Gwynne LCA Report: Why KDE Works"
date:    2001-03-30
authors:
  - "numanee"
slug:    telsa-gwynne-lca-report-why-kde-works
comments:
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "\"We could write better than CORBA. Hic. In a day!\"\nHeheheh, hilarious!\nReally, meetings are very important, if the meetings are fun, then better!\nKDE people is always nice and good people, this absolutely helps development."
    author: "Iuri Fiedoruk"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "It's clear that the responsible thing for KDE sponsors to do, would be to enable the core KDE developers to work together in the same location :-) Revolutionary changes happen to KDE every time these people meet, just imagine what KDE would be like if they shared an office floor permanently! :-)"
    author: "Haakon Nilsen"
  - subject: "I agree"
    date: 2001-03-30
    body: "The virtual office works, but magic happens when two or more people sit at a table and start scribbling on the leftover napkin of your pizza.\n\nWe are human after all, and inspiration happens best when physical contact is present. I would also add a barrel of beer... but that is a personal side note.\n\nI have worked on international working groups in AIESEC and you just get so much more productive when you meet at an international conference. You start putting faces to peoples emails, and get grips of their character. Its so much fun.\n\nCheers"
    author: "RE: How about speed"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "I'm unofficially pondering over a KDE hackfest in San Francisco this August, corresponding to LinuxWorldExpo then.\n\nI mean, I have nothing concrete, or even for a fact that I'll really do it.\n\nI also want to this because: we've never had a meeting in the states,  I live near S.F., and many many more developers live near S.F.  I can think of four within 45 minutes* of driving.\n\n*In the US, 45 minutes of driving is not a lot :)"
    author: "Charles Samuels"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: ">*In the US, 45 minutes of driving is not a lot :)\n\nAnd in California, especially, 45 minutes is short ride. Most people seem to keep themselves occupied while on the road by reading, applying makeup, talking on cell phones, etc (sometimes all at the same time:) \n\nA note for those from elsewhere: If you plan on visiting and driving, bring a healthy dose of patience and be prepared to drive defensively! Avoid the freeways and urban centers during commute hours (these days that's getting to be 5:30am-10:00am and 4:00pm to 7:00pm, depending where in the Bay Area you are). Oh, and don't forget to check the state's driving laws... you may be shocked, for example, to see people making high-g u-turns everywhere, and it's usually legal."
    author: "kdeFan"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "Oh, yes, and in californias can identify \"foreigners,\" because they always signal.\n\nWow, this is off topic ;)"
    author: "Charles Samuels"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: ">Wow, this is off topic ;)\n\nYeah, I guess it is :)\n\nTo bring this back on topic, I would humbly suggest that if you do organize a hackfest that you encourage all the developers to stay at the same hotel. If possible, the meetings could be held within the hotel... just a thought. Anyway, good luck with that."
    author: "kdeFan"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "No average KDE developer can afford a San Francisco hotel!"
    author: "Charles Samuels"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "Mozilla has been frozen for months, KDE would suck just as much if it ran on all platforms and it's GUI was written using XML and JavaScript."
    author: "AC"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "Actually, KPart GUIs (used by Konqueror, KOffice, etc.) are defined in XML.  No Javascript, though, thank God!"
    author: "not me"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-04-01
    body: "I felt a bit bad making that dig about Mozilla at the time, certainly I didn't expect it to make the Dot and LT when I was speaking. But in hindsight I remember them ignoring\nthe excellent qtscape hack and not using Qt for their project, so as far as I'm concerned there's open season on Moziila jokes. ;)"
    author: "Taj"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-04-02
    body: "The reason not to use Qt was a simple one, they didn't want Mozilla to be GPL, but don't want to have people pay 2K to work on it."
    author: "robert"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-04-02
    body: "Mostly because Qt on windows is not free, windows is netscapes target platform, not only that but relying on a third party toolkit that is not based on an open standard reduces portability(It may not now but the Mozilla people where thinking ahead).  Did mozill make the right choice to use JavaScript and XML rather than an abstraction layer in their code, maybe, Mozilla is meant to eventualy become a 100% portable free platform that poeple can develop for.  Using Qt would be like saying, why write KDE, you can allready buy CDE.  Remember that the majority of poeple use Windows on the desktop and that the purpose of Mozilla is to keep MS from basicaly owning the web by standards extensions, so the Windows port is the most important of all.  So please don't bash Mozilla, fist of all it's not even a 1.0, 1.0 will be released when it's usable, this would be like bashing the KDE 2 betas.  Mozilla is a piece of free software trying to achieve a very good purpose and because of it's purpose(to keep the web free) and the fact that Qt is 2000$ on windows it dosen't make much sense to use Qt.  Of course I'm sure your just bashing Mozilla because GNOME users use it, and not using Qt dosen't give KDE users another peice of KDE looking software, and that's just a stupid immature thing to do.  If things don't go your way don't be mad at people that didn't give you stuff for free, you'll have a very unhappy life."
    author: "KDEfan"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-04-02
    body: "I'm not unhappy about it at all (it has no effect on my life whatsoever), I just think they made a few mistakes along the way. 1. writing the unix browser without Qt (they are not using GTK for their windows browser are they?), 2. biting off more than they could chew for their 1.0 release checklist. What GNOME users use is of no concern to me.\n\nThat said, I'm sorry I offended anybody, that was not my intention."
    author: "ac"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-04-03
    body: "1. Mozilla is not using Gtk for UNIX, Mozilla uses it's own XP toolkit, the toolkit is very beta itself, that's why development is taking so long. One of the goals of mozilla is to create a XP development platform.\n\n2. \"Biting off more than they can chew\", Mozilla's 1.0 goal is not not crash much and be standards compliant, everything else is side projects.  \n\nIf you are going to insult open source projects at least learn a little about them(what they do, what platforms they run on) that takes 45 seconds to look up."
    author: "KDEfan"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-04-03
    body: "Please have a look at <a href=\"http://www.mozilla.org/ports/gtk/\">http://www.mozilla.org/ports/gtk/</a>. Describing a UI with XML is not enough, the conversion of the DOM tree into a user interface requires a platform-dependent toolkit, and they are using GTK+ for this.\n<br><br>\nIncidentally this is a similar approach to what we use in KDE, except that we do not do the UI-building from XML on the fly but during compilation.[1]\n<br><br>\nThe difference is that KDE tried to make a usable desktop in KDE 1 without specifying (IMHO) overly ambitious goals - these goals were left till KDE 2 and Qt 2.x. This was the point I made in the talk and in my comment above.\n<br><br>\n[1] This is not true for the kparts stuff, where the UI embedding is done on the fly.\n"
    author: "Taj"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-04-03
    body: "Please read the first line of http://www.mozilla.org/ports/gtk, it says\n\"It seems that the development on the \"Mozilla Classic\" GTK+ frontend has halted. Development efforts are now turning to the new layout engine and cross platform front end code, together code named Seamonkey.\".  \n\nIf Mozilla was to only run on UNIX and to use Gtk it would be done a long time ago(there are quite a few Gtk/GNOME browsers, there's also GtkHtml which is extremey fast, and almost complete in very little time).  If you ever port anything from Gtk to Qt, you will realize that the 2 API's are almost exactly the same, most stuff has a 1 to 1 relationship, the reason that Mozilla is taking so long is that they are using a beta XP toolkit, alot of the bugs are in XUL.   \n\nAnd again your confusing the Mozilla project and the Mozilla browser, Standards complience and not very buggy browser is not overly ambitious goals, yes konqueror was released without good JavaScript support or good CSS support because poeple could fall back on netscape 4.x, but that's realy required if you don't want people to get frusturated and fall back on IE(remember the idea is to keep MS from 'owning' the web), or if it's going on something like an embedded system that Mozilla is the only browser on."
    author: "KDEfan"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-04-06
    body: "No wonder GtkHtml was \"almost complete in very little time\". It's a port of KDE's old HTML code!"
    author: "Roberto Alsina"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "Good stuff.\n<br><br>\nBefore I realized that KMail could do everything I actually needed, I used mutt.\n<br><br>\nAnd learning mutt was infinitely easier thanks to Telsa Gwynne's copiously-commented .muttrc (<a href=\"http://www.linux.org.uk/~telsa/BitsAndPieces/muttrc-1.0\">http://www.linux.org.uk/~telsa/BitsAndPieces/muttrc-1.0</a>)\n<br><br>\nHer point that advances don't mean much unless they're communicated is spot on. Good communicators are as essential to open source as good developers (IMHO, of course).\n"
    author: "Michael O'Henly"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "uh? Telsa is a man's name :)"
    author: "ac"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-31
    body: "uh, I'm sorry, but you are so clueless :)"
    author: "aac"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "I have always been interested in these, but I live in the states.  I am currently living in the new england area and have no clue at to who else lives in this area.  Anyone interested?"
    author: "Ben Meyer"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "<I>>was looking after their newbies. </I>\n<P>\nYes, I think they do very well, e.g. the KDevelop mailing list treats newbies really well. \n<P>\nStill, with so many lists I'm still struggeling to know where to aks some specific questions. \nWhat if I got a question regarding KDE App programming which tends to be a bit more a general programming question and is not related to the KDE core development?\n"
    author: "Meinhard Ritscher"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "That would be for kde-devel@kde.org"
    author: "Roberto Alsina"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "Thankyou!\n\nThe only bad thing is:\nI recently subscripbed to kde-user and I could happyly subscribe to it but posting anything from my GMX account gets surpressed.\n\nSo I subscribed again, this time using my work account and what shall I say? I can post things but for some reason don't receive any postings. In some way that's good as I don't want my work e-mail stuffed with lots of e-mails. (My postmaster wouldn't be too happy about it.) \nBut it does mean replying to mails is a bit awkward. (Having to move mails from one account to another now.) I know, there are solutions to it, get a propper mail account.... \n\nMeinhard"
    author: "Meinhard Ritscher"
  - subject: "Re: Telsa Gwynne LCA Report: Why KDE Works"
    date: 2001-03-30
    body: "Get a yahoo account and use fetchmail to get it through pop into your box.\n\nThen use a mailer that supports multiple identities and reply using the yahoo account's address."
    author: "Roberto Alsina"
  - subject: "KDE Mailing lists"
    date: 2001-03-30
    body: "Actually I'm quite happy with gmx though. And I'm rather not using yahoo. (privacy policy).\n\nUsing my laptop for all my e-mails and this one not supporting X (IBM Thinkpad 365 - some weird graphic chip - not even beeing sucessful with framebuffer) I'm happyly using PMMail under OS/2. It does support multiple accounts but unlike KMail doesn't support different identities for one account though.\n\nBut how come the list.kde.org doesn't reject mails from yahoo (and maybe not even from h*tmail) but from GMX accounts?\n\nFor some reason I would be even happier if the mailing list were news groups. But I guess there is a good reason for them not beeing.\n\nMeinhard"
    author: "Meinhard Ritscher"
---
The other day, I found something rather interesting in the referrer logs: <a href="http://www.linux.org.uk/~telsa/">Telsa Gwynne</a>, well-known in Linux and GNOME circles, wrote up a <a href="http://www.linux.org.uk/~telsa/Trips/Talks/lca-kde.html">pretty neat report</a> of <a href="http://www.kde.org/gallery/index.html#kang">Sirtaj Singh Kang</a>'s talk at <a href="http://www.linux.org.uk/~telsa/Trips/lca.html">Linux.Conf.Au 2001</a>. <i>"One huge thing that helped KDE and would probably benefit other very big projects was that KDE had the KDE Summit which was sponsored by Trolltech. When you get all the people in same place, then magic happens. At the KDE one, Matthias Ettrich and Preston Brown got drunk to the stage of claiming "We could write better than CORBA. Hic. In a day!" The next day, of course, everyone reminded them about this claim, so they had to do something about it. 36 hours later, DCOP emerged. There was a similar effect with KOffice, which came on by leaps and bounds as a result of the summit. So, if you're big enough to get sponsors, get your people all in one place and watch the results pour out."</i> Go read it for some  pretty entertaining <a href="http://www.linux.org.uk/~telsa/Trips/Talks/lca-kde.html">KDE history and insight</a>.