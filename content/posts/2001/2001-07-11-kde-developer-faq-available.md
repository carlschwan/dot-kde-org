---
title: "KDE Developer FAQ Available"
date:    2001-07-11
authors:
  - "pfremy"
slug:    kde-developer-faq-available
comments:
  - subject: "addition"
    date: 2001-07-11
    body: "Nice faq, thanks for your work. I thought this might make an interesting addition:\n\nHow does one switch between using a branch and KDE HEAD in CVS?  For example, say I want to work temporarily with the KDE 2.1 branch for a bugfix and then switch back to KDE HEAD...  \n\nI've seen this explained on the lists before, but I thought it might be useful to have the answer in this faq."
    author: "Navindra Umanee"
  - subject: "Re: KDE Developer FAQ Available"
    date: 2001-07-11
    body: "Had a look at this FAQ and in my opinion it is an extremely good idea.\n\nPlease make sure to have it listed on http://developer.kde.org/documentation/ as soon as possible."
    author: "Karl-Heinz Zimmer"
  - subject: "generate makefile.am from qmake projectfile"
    date: 2001-07-11
    body: "Is there a simple way to genereate a makefile.am\nfrom a qmake projectfile?\n\nI was reading a little bit into the automake and autoconf documentation and .... :-(\n\nIs there a simpler way? I want to switch from\nhand making everything to kdevelop, but I understood that I need a makefile.am for my project for this.\n\nDont know if this is a FAQ."
    author: "ac"
  - subject: "Re: generate makefile.am from qmake projectfile"
    date: 2001-07-11
    body: "[tq]make only generates makefile but is very easy to use.\n\nAn good advice: don't ever try to understand autoconf/automake/acinclude and so on. This is very complicated and you will probably waste a lot  of time for a bad results. Kde has some configuration gods that have taken this time for you.\n\nIf you want to move your app to kdevelop, create a new project with kdevelop and then add your source files. KDevelop will generate the Makefile.am stuff.\n\nElse, use kapptemplate to generate an app template and modify the Makefile.am to get your sources integrated.\n\nOr look for the Makefile.am of an existing kde applications. It is usually quite simple.\n\nI'll try to integrate your question into the faq."
    author: "Philippe Fremy"
  - subject: "Re: generate makefile.am from qmake projectfile"
    date: 2001-07-12
    body: "As a quick way to still work with your current qmake setup in KDevelop, just open KDevelop and choose Project -> Generate Projectfile and select the base directory of your project. It will then import your project and you can work on it like before, though without the automake/autoconf stuff yet, for that you need to create a new application with the wizard and add your projects files with project -> add existing files.\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: generate makefile.am from qmake projectfile"
    date: 2002-08-27
    body: "You could also wait for KDE3.1 where kdevelop (gideon) will ship with qmake project support."
    author: "Jakob Simon-Gaarde"
  - subject: "how to install qmake"
    date: 2004-04-12
    body: "a new user of mandrake 9.1, when trying to use qmake ,get the error qmake command not found.\ntried installing it according to the manual but in vain\na detailed explaination of installin thru terminal would be really appreciated.\nThanks"
    author: "ccittgrp"
  - subject: "Re: KDE Developer FAQ Available"
    date: 2001-07-11
    body: "There is not that much work to be done for developer.kde.org, because it is quite good already. But it can be improved. Here is a more detailed list of what needs to be done:\n\n- rewrite the debugging howto, so that it integrates all the debug questions/answer of the faq and mention every possible tool used to debug. Debugging a program is a key activity for a developer, so we should help as much as we can. This document should then become the reference to debug a kde program and every trick (debug a ioslave, debug a kpart, ...) should be mentionned there.\n\n- move the developer howto hidden in the tutorial section to the \"faq and howto\" page. It is not exactly up-to-date, so it should also be slightly updated.\n\n- the kde books has some interesting chapters about using cvs and packaging that should be linked or added here, for people not yet knowing cvs.\n\nThe best would probably be to update the kde book, by integrating an updated developer howto in it.\nThen we could simply link to the relevant chapters.\n\n\n- there is probably something to be done to unify more the compilation faq (on www.kde.org) and the compilation howto"
    author: "Philippe Fremy"
  - subject: "Re: KDE Developer FAQ Available"
    date: 2001-07-11
    body: "It's a bit of a shame that you need to know C++ to develop KDE. I would really like to contribute, but I don't feel up to learning C++ right now: I would happily learn Python but I can't compile Sip/PyQT/PyKDE on my box, and PyKDE is only compatible with KDE 1 :-(\n\nDavid"
    author: "dave"
  - subject: "Re: KDE Developer FAQ Available"
    date: 2001-07-11
    body: "If you have problems compiling PyQt/PyKde, ask on their mailing list, people here are very responsive.\n\nSomeone is working on PyKde for kde 2, but there is no release date. I agree that it would be really cool to have the power of kde with python.\n\nBut with PyQt, you can do already develop an application. Turning it into a kde application can be done later."
    author: "Philippe Fremy"
  - subject: "Re: KDE Developer FAQ Available"
    date: 2001-07-12
    body: "Hi,\n\nwell, getting into C++ is not that hard, especially with Qt and KDE. You should just give it a try blindly to see how easy it is. Theres tons of simple introductions that last already for understanding the Qt tutorials and the KDevelop IDE makes it easy for you to create a KDE application quickly without knowing anything about C++, then guides you with a tutorial towards following the Qt examples/tutorials with your created app.\n\nI think writing a short C++ introduction for beginners is a good idea actually and could be done easily. If you would like to help with that while learning, please contact me :)\n\nRalf"
    author: "Ralf Nolden"
  - subject: "OT: Killustrator & Adobe"
    date: 2001-07-11
    body: "Hi, I know it's off topic but nervertheless important:\nAccording to heise ( http://www.heise.de/newsticker/data/odi-11.07.01-000/ )\nAdobe wants to talk with Dr. Kai-Uwe Sattler about the naming problem.\nSeems like they regret what their lawyers did."
    author: "anonymous"
  - subject: "Re: KDE Developer FAQ Available"
    date: 2001-07-11
    body: "Great.\n\n How about having a package kde-devel-doc (or\nsomething) with all the development documentation (APIs, tutorials, the KDE development book etc). \n\n It would be easy then to get all the develop-\nment documentation one needs, instead of searching\nfor it here and there (on the DOT, on develop.kde\n.org, on mosfet's site -well, previously at least).\n\n Distributions would include it also. It would\nbe a GOOD THING. \n\n P.S Excuse me if such a thing already excists. \nI never found it in Madrake, SuSE or Debian, though. Instead there only was the usual end-\nuser documentation and stuff on /usr/share/doc."
    author: "Count Zero"
  - subject: "Re: KDE Developer FAQ Available"
    date: 2001-07-12
    body: "I agree with you that it would be cool to have a tar archive of \"all necessary kde docs\".\n\nBut, already, you can use cvs to get developer.kde.org . It is quite big but you have everything you need."
    author: "Philippe Fremy"
  - subject: "Re: KDE Developer FAQ Available"
    date: 2001-07-12
    body: "Hi,\n\nThis might be off-topic. After reading the FAQ, I went to developer.kde.org. My interest is in making new widget that looks like Windows XP.. I follow the widget-theme tutorial ( at mosfet.org ) but it's broken.. So anybody else have any tutorial on widget.. maybe the mirror of the original one or any intersting site for me to start learn and make theme widget.\n\nThanks."
    author: "deman"
  - subject: "Re: KDE Developer FAQ Available"
    date: 2001-07-13
    body: "This is great, I looked at it for about 5 seconds and found an answer to a question that has been bugging me. Thanks A Lot!!"
    author: "John Christopher"
---
With the help of <a href="http://www.kde.org/people/david.html">David Faure</a>, I have just made available a <a href="http://developer.kde.org/documentation/other/developer-faq.html">KDE Developer FAQ</a> on <a href="http://developer.kde.org">developer.kde.org</a>. Here is your chance to see answered that annoying little question about development which you never dared ask.  You are strongly encouraged to <a href="mailto:pfremy@chez.com">submit</a> any other such questions about development for which you don't yet have a clear answer -- don't be shy, if something is a problem for you, it is probably a problem for ten other developers! I also take this opportunity to highlight the fact that we need volunteers to rewrite, complete or update the various documents on developer.kde.org.  If you can help, <a href="mailto:webmaster@kde.org,pfremy@chez.com">please apply</a>!
<!--break-->
