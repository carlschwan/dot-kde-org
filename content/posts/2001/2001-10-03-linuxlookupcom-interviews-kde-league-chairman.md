---
title: "Linuxlookup.com Interviews KDE League Chairman"
date:    2001-10-03
authors:
  - "ostrutynski"
slug:    linuxlookupcom-interviews-kde-league-chairman
comments:
  - subject: "Mr"
    date: 2001-10-04
    body: "I don't understand how they can run MS Office from their terminals if they are running linux/kde?  Surely, not through Wine???"
    author: "Robert Gelb"
  - subject: "Re: Mr"
    date: 2001-10-04
    body: "I'm not sure exactly what the program they use is, but the MS Office components run on a server running Windows, with the interface displayed on the users terminal."
    author: "Daniel"
  - subject: "Maybe..."
    date: 2001-10-04
    body: "they are using Rdesktop <http://www.rdesktop.org>. Works great when you have to log on to a NT4 Terminal Server Edition, any W2K server or Windows XP."
    author: "Martin Dahlberg"
  - subject: "Re: Maybe..."
    date: 2001-10-04
    body: "Do you use Rdesktop? Would you care to tell us how it is done?\nIt  seems very interesting."
    author: "reihal"
  - subject: "Re: Maybe..."
    date: 2001-10-06
    body: "rdesktop is very, very good.\n\nThe home site is at http://www.rdesktop.org\n\nSearch www.rpmfind.net for a suiteable RPM. The PLD (Polish(ed) Linux Distribution) have made some. I took the source RPM and rebuilt it with no problems on my, basically, redhat 6.2 server.\n\nThis server provides application support for a network of \"Thin Client\" X terminals.\n\nI've created a \"session\" for KDM so that uses can choose to select a full screen \"Windows\" session at login - hard to distinguish from the real thing! After logging in to KDE rdesktop can also be run - so that the windows session appears in a window...\n\nOf course, although rdesktop doesn't actually need to check for licenses, you still need a boat load of MS Windows NT licenses... but at least you don't actually have to run the software.\n\nWe use it against Windows NT Terminal Server 4.0 - mainly to run applications that we need to support clients with a windows environment. Performance is very good - certainly for light usage.\n\nThe command line for \"windows in a Window\" is:\n\nrdesktop -l -4 -k uk <hostnameofterminalserver>\n\nFor full screen\n\nrdesktop -l -F -4 -k uk <hostnameofterminalserver>\n\nIs that enough info for you?"
    author: "Corba the Geek"
  - subject: "Re: Maybe..."
    date: 2001-10-06
    body: "Thanks, I must try this.\nIt will be interesting to see what speed I will get on a small, switched, 100Mbs home-network."
    author: "reihal"
  - subject: "Re: Mr"
    date: 2001-10-04
    body: "They could be using vnc. VNC clients everywhere, few windows workstation with VNC servers and that's it. What windows is missing now is probably the ability to have multiple users logged at the same time on the same station."
    author: "Philippe Fremy"
  - subject: "Re: Mr"
    date: 2001-10-04
    body: "reminds me of a funny project i saw in a large european telco, but the final architecture was different: first a client to a Win TSE, and then from the TSE a commercial equivalent of VNC to a farm of workstations."
    author: "Guest"
  - subject: "watch your links"
    date: 2001-10-04
    body: "please, never link to the same thing twice in your article, its just annoying.\n\nthanks."
    author: "me"
  - subject: "Good interview, but..."
    date: 2001-10-04
    body: "...since it was an interview with \"Chairman of the KDE League\", wouldn't it have been even more interesting to hear some news about the KDE League? Does anyone have any news about the KDE League?"
    author: "Loranga"
---
<a href="http://www.linuxlookup.com/">Linuxlookup.com</a> is running a nice <a href="http://www.linuxlookup.com/html/interviews/kde-interview-2001.html">interview</a> with <a href="mailto:pour at kde.org">Andreas Pour</a>, Chairman of the KDE League, Dot editor ("Dre") and KDE evangelist, about the latest KDE release, the KDE developers' attitude towards Open Source and business use of KDE and KOffice. 
Andreas points out some reasons why businesses might consider switching to Linux/KDE instead of migrating to Windows XP and reveals some interesting facts about the <a href="http://www.largo.com/">City of Largo's</a> KDE installation: apparently it only requires half a person to provide user and technical support for all 800 KDE Users in an office environment. TCO - no problem for KDE. You can read the full interview <a href="http://www.linuxlookup.com/html/interviews/kde-interview-2001.html">here</a>.

<!--break-->
