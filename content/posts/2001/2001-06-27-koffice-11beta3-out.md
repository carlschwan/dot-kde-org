---
title: "KOffice 1.1beta3 is Out!"
date:    2001-06-27
authors:
  - "numanee"
slug:    koffice-11beta3-out
comments:
  - subject: "Reviews"
    date: 2001-06-27
    body: "It's great to see many reviews about this new release.\n\nIt will help people who are having a difficult time with Office XP see that there is actually an alternative office suite.\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "What about Krayon?\nThere is no activity on the mailing-list."
    author: "reihal"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "> What about Krayon?\n\n    Download and try it.  It's missing several icons, has a few misfeatures (especially how the magnifying glass works), but seems to be workable except for one major, major detail:  It can't save as anything but .kra files (side note: KDE runs on *nix... why use a three letter extension?).  AFAIK, nothing else reads them, so they can't be converted to/from any other format.\n\n    Oh, and Undo dosen't work.  It's not ready, IMO.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: three letter extensions"
    date: 2001-06-27
    body: "<a href=\"http://kt.zork.net/kde/kde20010608_13.html#1\">http://kt.zork.net/kde/kde20010608_13.html#1</a>"
    author: "Aaron J. seigo"
  - subject: "Re: three letter extensions"
    date: 2001-06-27
    body: "Does this mean that KOffice/KDE does not default to saving files with proper extensions?  I would like to appeal to David and the KDE developers to reconsider this decision.  Forcing too much choice on the user is not a good thing, but helping them by suggesting a default extension is a good idea and simplifies people's lives.  Especially people from the Windows world.\n\n> It was, after some discussion, decided that the best idea was to provide a checkbox in the Save File dialog box that allows the user to set wether or not to automatically add file extensions\n\nAgain, I hope there is a proper default.  Don't force these kinds of decisions on users!"
    author: "KDE User"
  - subject: "Re: three letter extensions"
    date: 2001-06-27
    body: "I strongly agree with this...\n\n The simple fact that we are in *nix and we have magic cookies and stuff doesn't mean we should \nnot use extensions.\n\n We are in an era of 30GB hard disks. Everything that can make files *easier* to locate and manipulate should be appreciated.\n\n Extensions make visual seperation of filetypes\neasier. They make shell scripting and general shell usage easier. Like:\n\nmv *.kwd ~/Documents\n\nThere is not a single practical reason that extensions should not be used. The \"they look ugly\" is not a practical argument. It's merely \na personal preference."
    author: "Count Zero"
  - subject: "Re: three letter extensions"
    date: 2001-06-28
    body: "> I would like to appeal to David and the KDE developers to > reconsider this decision.\n\nI have already reconsidered, the extensions are appended automatically again :)"
    author: "David Faure"
  - subject: "Re: three letter extensions"
    date: 2001-06-28
    body: "Hurray!  We all win. :)"
    author: "KDE User"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-28
    body: "Image -> Export Image. Yes, it should be under \"File\"."
    author: "Carsten Pfeiffer"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "Hey, it starts to look really nice. I'm just working on a presentation with Staroffice (urgent), bur for the next one I definetly will try Kpresenter.\n\nBtw, am I stupid or is it not possible to zoom in and out in Kpresenter. Should be somewhere, I dont believe that nobody implemented this....\n\nBest regards,\nSri"
    author: "Sri"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "You're unfortunately right, it's not possible :( The current design and pseudo \"page\" concept is just too limiting in this matter."
    author: "Lukas Tinkl"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-29
    body: "I've used KPresenter (KOffice 1.0) a bit, and it's been very stable. In KOffice 1.1, it should be even better.\n\nThe other parts of KOffice I've tried have not been stable enough for me. KWord crashes too easily (but that's perhaps just me trying out all the advanced layout functions), and KIllustrator is unstable as well. I've found StarOffice's drawing program a nice alternative, and for word processing I mainly use LyX and Psion Word (on my PDA)."
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "it looks great .... I hope their will be some export-filters for the MS-DOC-format soon ... but this Office-suite definitly is maturing !!\n\nbtw I had to uninstall the rpms from alpha2 before I could install KOffice 1.1beta3 -- some version-numbering wasn't correct here\n\nKDE rocks .. thank you guys !!\n\n\nfab"
    author: "fab"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "save your document as RTF."
    author: "Evandro"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "Why KWord can't emmbed Qt metafiles, but only Windows ones ? Isn't it strange ? My program is running Qt and can only generate Qt metafiles. I suspect that those guys are paid by Microsoft this is why KWord is dependent on their proprietary formats."
    author: "Doubtful"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "Hmm... with Qt metafiles you probably mean a saved QPicture? It wouldn't be a problem to import them (as Qt would do *everything* for us), but I doubt that a lot of users would need that feature. Anyway, this should be quite easy to implement."
    author: "Werner Trobin"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-28
    body: "So you doubt that users need to embed their graphics into KOffice, because there is no other way to do it than QT metafiles ( so what is WMF for ? ) You can't generate WMF on Linux. ( GNOME programs can't generate QT metafiles but QT3 will have *.SVG format support, it just may be too bloated for everyday work, so two-format support would be best  ). It is a natural complement to pixmap embedding. I personally think that users need more QTmeta/SVG than WMF."
    author: "Ups"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: ">I suspect that those guys are paid by Microsoft this is why KWord is dependent on their proprietary formats.<\n\nThat makes not the slightest bit of sense."
    author: "Carbon"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-28
    body: "Just kidding ..."
    author: "Ups"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-28
    body: "Just kidding ..."
    author: "Ups"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "screenshots ?"
    author: "mr jobs"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "I agree, it would be nice to have some screenshots of all the apps in their new suits.\n\n--\nAndreas"
    author: "Andreas Joseph Krogh"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "http://www.koffice.org and then select an application whose screenshots you want to view."
    author: "Lukas Tinkl"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-28
    body: "Those screenshots are from the last millenium."
    author: "Robert Gelb"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "KChart sucks as it did two years ago."
    author: "Ups"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "So much hard work has gone into KOffice.  How about setting a new standard for posting online and make a little rule that when posting something negative, find something else positive to go along with it.  \n\nYour life Karma would improve no end.  You might even find yourself smiling ... wow!! ;-)"
    author: "Macka"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "What about applying to help out ? That would do much better than just saying it sucks :) and finally, with your help and time investment, it won't suck anymore in a little time you have been working on it and hence, you have solved 3 problems at once:\n\na) you don't have to post KChart sucks again :)\nb) you take other users the possibility to post it sucks\nc) you are proud of what you did and a happy KChart user.\n\nAh, and most importantly, you're a part of this *great* community who invests most of their free time into the KDE project and making it so much fun to work with people around the world. It is a friendly place where we are, so feel free to join.\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "Okay, I give up.  Where is there information about koscript?  Where is the language even mentioned?  What can it do?  What *does* it do?  Google and guesses have not helped... anybody know where information about this application in this release can be found?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "KOScript is a stripped down version of KScript and both of them reside in koffice/lib/koscript and koffice/lib/kscript. In these directories you can also find a lot of examples and some documentation. Basically it looks like a mixture of Python, Perl, and C++ (no static types, object oriented, including multiple inheritence, natice support for signals and slots,...). Please check koffice/lib/kscript/Manual.<br>\n\nHowever, if you just want to script your KOffice applications I'd rather use the DCOP interface. Within KOffice koscript is used in KSpread only."
    author: "Werner Trobin"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "Why create yet another programming language, rather than just standardising on one particular pre-existing one (such as Python)? We don't need to copy Microsoft all the time."
    author: "Jon"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "KOScript was introduced when DCOP was not yet present. Now that we have DCOP, KOffice apps (and all other DCOP-enabled KDE applications btw) can be scripted with every programming and scripting language with a DCOP binding. \n\nThese currently include Java, Perl (?) and Python. Please correct me if I am missing some."
    author: "Daniel Molkentin"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "This release of kword is MUCH better than ever before. Finally, I can layout more than one frame on the page, connect them and acutally run text through them and around them without it crashing on me. While, I'm sure it will still crash (it's a beta), it is already much better than before!\n\nIf it gets really stable I may have to use it all the time\n(provided the interface get's cleaned up sum and it gets more features - comes with time.)"
    author: "Sheldon Lee Wen"
  - subject: "Give these developers a hand"
    date: 2001-06-27
    body: "The KOffice 1.1 and KDE 2.2 teams have both been working at an amazing rate and have done great things with these two projects.  Remember most of the people doing this work don't see any material compensation for their efforts.  Give all of these people a hand, they truly deserve it for the spectacular work that's been accomplished.  I for one can't say enough good things to truly show my gratitude.  If you get a chance, drop them a line and say thanks.  KDE is poised to amaze the desktop world in how great Open Source projects can develop just like Apache was for the web server.  Thanks guys.\n\nCheers.\n\nNate"
    author: "Nate Murphy"
  - subject: "Re: Give these developers a hand"
    date: 2001-06-27
    body: "Very much so! In addition, kudos to the other Open Source teams that KDE wouldn't be possible or at it's current incredible state without : XFree86, Ghostscript, the GNU C++ compiler and tools, the Linux and BSD kernel developers, the distro and theKompany people, and everybody else who's lended a hand.\n\nAh, the smell of OS revoluion is in the air."
    author: "Carbon"
  - subject: "Re: Give these developers a hand"
    date: 2001-06-27
    body: "Heya guys, why don't you just join ? :) Thanks for the thanks though, but feel invited to participate as well. Please come and join us on irc.kde.org on #kde and step right in, there's still a lot to do and many ideas only come with new people :) Admit it, you always wanted to be a KDE developer ! :))\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: Give these developers a hand"
    date: 2001-06-30
    body: "Admit it, you always wanted to be a KDE developer ! :))\n\nSure.  But you don't want me to be one - you want GOOD software!  :-P\nCY"
    author: "CY"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "Well ! I only begin to test and I find some good new features...\n\nAt first Kivio is avalaible. It seems performing, however I have some difficulties to find some features (anchor a link between two shapes... (P.-S. : yes, I succeed...)), because I am accustomed to ABC Flowcharter and because there is no documentation.\n\nIn Kword, I am very glad to see that it is possible to easily insert a picture, move it, change the size... The usage of the right mouse is only available in the edges of the frame and it would be better to be avalaible too inside. And in the \"around text\" of the frame property there are 3 options, and the 4th (text all around) would be pretty...\n\nIn KSpread, I see that the sum is now showed in the status bar. Alas - for me it is very essential - the usage of the tab key is bad : it has to go from a cell to the following (like in Excel or Gnumeric). Also important, it is not possible to have an auto-size of the hight of a line when text goes to line automatically in some cells of the line (it is also a lack of my version of Gnumeric).\n\nAnd, of course, import and export are important, and it is better than the beta2...\n\nI hope that the essential will be done for soon, good work !"
    author: "Alain"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "just a question: is it possible to connect two lines together in kivio? like when you move one, it stays connected with the other?\n\nit's necessary to have stuff like:\n\n         |\n   -------------\n   |     |     |\n\ni could connect lines and boxes (sorry for my crude vocabulary :o( ) but not lines and lines.."
    author: "emmanuel"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-28
    body: "> The usage of the right mouse is only available in the edges of the frame and it would be better to be avalaible too inside. \n\nIt is now, for image frames.\n\n> And in the \"around text\" of the frame property there are 3 options, and the 4th (text all around) would be pretty...\n\nWhat's \"text all around\" ?"
    author: "David Faure"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-28
    body: "> > And in the \"around text\" of the frame property there are 3 options, and the 4th (text all around) would be pretty...\n\n> What's \"text all around\" ?\n\nFor example a page full of text and in the middle a little picture (without text on it, only around).\n\nThere are 3 possibilities :\n1 text on the picture and around\n2 text around the picture excepted on the left (or the right if picture is on the left)\n3 text above and below the picture (no text in right and left)\n\nAnd 4 would be ;\ntext around the picture (but not inside, as 1)\n\nOf course, some other managements are possible :\n- 2 --> 2a and 2b (no text on left, no text on right)\n- choose the width of the blank zone (between the picture and the around text), on the top, bottom, left, right\n\nBut, for a first version, only the dot 4) seems necessary for me...\n\nThanks."
    author: "Alain"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-27
    body: "Just a question... What about Klyx?? will it be part of koffice at any point???\n\nFor what I see, its been mantained and it actually looks god (much much better than lyx)"
    author: "Mario"
  - subject: "KOffice vs Klyx"
    date: 2001-06-27
    body: "I'm currently working on a presentation on plasma simulation. As we have KDE2 installed at the University I thought that I'd give KOffice a try when making my slides using KIllustrator / KPresenter. \n\nTrying to import my plots (made with xmgr) I was very surprised to see that the only vector file format supported is WINDOWS WMF! This would not be a big problem for a WINDOWS program, but most other UNIX program tend to export EPS. Not even my favourite drawing/sketching/flowcharting program \"Dia\" exports to WMF!\n\n(Note: I'm not using the latest version of KOffice - maybe EPS support is already included?)\n\nI know that neither xmgr nor Dia are KDE programs, but I really think that it is more important for the KDE project to keep compatibility with the rest of the Unix world than trying to be smooth and easy for ex-Windows users!\n\nIn the end, I used good old Klyx to combine a few plots. Not the best program for presentations. But  - in my opinion the very technical-report-writing-program for any platform ever. It's really easy to use when writing a lot of math, and it produces beatiful documents that look *the right way* (thanks to LaTeX).\n\nSo will there ever be a KDE2 Klyx version? \n\nOr will KOffice become as good as KLyx/LaTeX when it comes to math and layout intelligence?\n\nThanks for reading."
    author: "Johan"
  - subject: "Re: KOffice vs Klyx"
    date: 2001-06-28
    body: "EPS is supported (and has always been), but as an image (pixmap), not as a vectorial format. (Try Insert Picture).\n\nApparently the EPS module for KImageIO uses \"gs\" to render the EPS into an image.... this doesn't help getting it in vector format :("
    author: "David Faure"
  - subject: "lyx GUII"
    date: 2001-06-28
    body: "I guess sometime into the future (maybe even this year??) lyx TNG  aka GUII (GUI independent) will be released upon the unsuspecting word processor  population.\n\nThis rewrite of lyx is based on either QT2/KDE,  GTK or xforms, your choice during compilation. Then lyx will  have a modern GUI finally. Xforms sucks badly.\n\nRead all about it and its status here:\nhttp://www.devel.lyx.org/guii.php3\n\nklyx is an old port  of an old version of lyx to qt/KDE. lyx is much better today than klyx."
    author: "Moritz Moeller-Herrmann"
  - subject: "Can't read KOffice native formats anymore!"
    date: 2001-06-28
    body: "KOffice Beta 3 is looking fine and the filters work very well for foreign formats, but to my shock, after upgrading I could not read my native files anymore! I have lots of KWord and KSpread files saved on KOffice Beta2 that I can not open anymore. I'm using KDE 2.2alpha2 on Madrake8. Can someone help?\n\nPedro"
    author: "Pedro Ziviani"
  - subject: "Re: Can't read KOffice native formats anymore!"
    date: 2001-06-28
    body: "What are the messages in the konsole where you start kword or kspread ?\nThis is really strange !"
    author: "David Faure"
  - subject: "Re: Can't read KOffice native formats anymore!"
    date: 2001-06-28
    body: "Yes, that's pretty strange and very frustrating. I'm sending, in a text file attached to this message, a copy of the console messages generated when opening Kword and trying to read a native format file saved by my previous KOffice installation."
    author: "Pedro Ziviani"
  - subject: "Re: Can't read KOffice native formats anymore!"
    date: 2001-06-28
    body: "kword: WARNING: KFilterBase::findFilterByExtension : not filter found for application/x-kword\n\nHmm, this bug again. You are using kdelibs-2.2-alpha2, right ? I think I have fixed it afterwards. Please try updating to kdelibs-2.2-beta1."
    author: "David Faure"
  - subject: "Krayon and more"
    date: 2001-06-28
    body: "I feel difficult to use Krayon, because it seems impossible to do quickly something very simple :\n1. load a jpg (or png, or...)\n2. modify it\n3. save it\n\n1. About loading, the (little) difficulty is only for the first time to find the command import in the menu Image, so not use the command \"Open\". I do not try to associate .jpg and Krayon, so that the .jpg will be open in Krayon by clicking on it, I hope it's good...\n\n2. About modifying, the functions are today limited, but it is going to grow later...\n\n3. The difficulty, for me, is here : it seems now impossible to save quickly (using a command/button/shortcut) the .jpg file. By using \"save\" or \"export\", a dialog box is opened, and it is too tiring for this beginning of summer, and later too...\n\nMore generally, I think that for any KOffice program, it would be fine to save quickly the file in its original format, excepted a structural modification, like adding a layer...\n\nAnd if the \"structural modification\" is not easy to manage, perhaps only add a quick advertising needing to click on OK, something like : \"You are going to save in a format that may impoverish the content of your work. Do you want it ?\", so that it is saved with only a double click..."
    author: "Alain"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-28
    body: "Wow, a big problem :\nAfter an import and some modications, if you close Krayon, there is no question such as \"Do you want to save ?\""
    author: "Alain"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-28
    body: "Well, it seems that developement of Krayon has stopped, I wonder why?"
    author: "reihal"
  - subject: "Re: KOffice 1.1beta3 is Out!"
    date: 2001-06-29
    body: "Maybe because there is no active developer on it at the moment ?\nIf you want to have fun with C++, Qt, KDE and KOffice, become a krayon developer ! :)"
    author: "David Faure"
---
KOffice 1.1beta3 <a href="http://www.koffice.org/announcements/announce-1.1-beta3.phtml">has been announced</a>.  Although only a beta, this version should be more stable and feature-complete than previous versions of <a href="http://www.koffice.org/">KOffice</a>.  A <a href="http://www.koffice.org/announcements/changelog-1.1.phtml">changelog versus KOffice 1.0</a> is available as well as an <a href="http://www.koffice.org/announcements/announce-1.1-beta3.phtml#changelog">incremental changelog</a> since the last beta. Changes include DCOP scripting enhancements, KGhostview integration, many KWord improvements, and various tweaks and polish. Binary and source packages are <a href="http://ftp.sourceforge.net/pub/mirrors/kde/unstable/koffice-1.1-beta3/">available here</a>. (As usual, kudos go to <a href="mailto:pour at kde.org">Dre</a> for putting together <a href="http://www.koffice.org/announcements/announce-1.1-beta3.phtml">our nice announcement</a>.)
<!--break-->
