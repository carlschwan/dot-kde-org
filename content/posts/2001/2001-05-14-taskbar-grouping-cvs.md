---
title: "Taskbar Grouping In CVS"
date:    2001-05-14
authors:
  - "Dre"
slug:    taskbar-grouping-cvs
comments:
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-14
    body: "> Here, 17 Gimp windows can be seen grouped in\n> the same taskbar button\n\nYet the main Gimp window is not grouped together with those.\n\nMakes me curious.. *enables code*\n\nHm, looks as if only the picture windows get grouped together, \"New image\" and the layers window are all still seperate. Odd."
    author: "Rob Kaper"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-14
    body: "It must be matching based on the name or a window property.  The first few characters of the name matches, or the window property is the same, so it is grouped together.  I guess this may change (maybe it is already configurable?) as this has recently gone into CVS as part of Taskbar TNG."
    author: "KDE User"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-14
    body: "I've checked the code, and...\n\nQString Task::className()\n{\n    XClassHint hint;\n    if(XGetClassHint(qt_xdisplay(), _win, &hint))\n        return QString(hint.res_name);\n    return QString::null;\n}\n\n... it's based on classname which is a feature of X. So I presume any application could actually choose seperate (or the same) classes for its various windows itself."
    author: "Rob Kaper"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-14
    body: "This sounds pretty cool, but I hope there's a way to turn this on application/by application. I'd hate all my terminals to show up on 1 task bar button!"
    author: "Richard Sheldon"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-14
    body: "Yeah, especially with Konsole, where they manage their terminals on the bottom of the window.  Anybody with multiple Konsoles open clearly intends to want to shift between them.  I hope it is easy (or will be) for app developers to disable this feature.  I can see where it could really come in handy, especially when driven with some \"ai\" behind it...  Something to think about."
    author: "dingodonkey"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "I only use one single Konsole.\nIn my mind, I view Konsole as a KUniqueApp.\n\nLet the program do its job and open new sessions\nfrom within Konsole."
    author: "yoowhoo"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "Right.  I, on the other hand, am in the habit of having 3-4 konsoles open (no idea why), the grouping would just seem...  pointless, since grouping is available in a single Konsole should you wish to use it.\n\nThis is a feature that will be more accepted by users after Windows XP is released, and seem as foreign as virtual consoles to any current Windows users.  We should be devoting the most time now to making features like this more advanced, with more \"smart\" grouping, to ease it on new users.  After all, haven't we all wanted a GUI that did what we want all the time, without telling it what to do?\n\nFor example, in that image, all of the GIMP images that were grouped had extremely similar filenames, yet were grouped so annoyingling...  Perhaps listing them as:\n\n(...source_y.xcf-1.0...)\n(...source_py.xcf-3.0...)\n<snip>\n...midi.xcf-17.0...\n<snip to button>\n[hi64-mime-...(RGB) 100%]\n\nIn other words: include only the areas of change, the rest is ...ed out, perhaps using transparency, so the outer dot is lighter than the inner dot.  Whatever.  If the icons for each are different, then just show either the most used or some default.  In the button itself, include everything, ...ing out the parts listed in the menu.   Multiple ...s can be used.  Note the way I used ()s only in the menu around the menu items that are minimized.  No place else.\n\nThis is just my own random babbling of how a feature like this (which i do like, and look foward to) should be.  In addition, you should be able to configure how many similar items are needed to form a group (i think 4-6... although it could also vary based on how crowded the taskbar is...  if there is plenty of room, why bother?)"
    author: "dingodonkey"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-14
    body: "Hm, I think, kasbar is better, and it comes with preview windows and icons, so I can easily find what I am looking for. Too much text, especially with full path with all directories is confusing and not easy for fast navigating. When I work in Gimp I put Gimp main window, layers window, tool window and brushes window on one virtual desktop and a picture (or group of pictures if they are smaller) on another virtual desktop, and keep them opened all the time. And kasbar appears on all virtual desktops, so I use it as desktop pager."
    author: "Antialias"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-14
    body: "This is a useful feature. Good work KDE team. :-)"
    author: "AC"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-14
    body: "I know I am beating a dead horse here, but when will the taskbar not have to bo the full length of the screen?"
    author: "Ben Meyer"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-14
    body: "In CVS, it doesn't have to be - you can specify a percentage, and whether you want it to autoexpand beyond that..\n\nThe nice things about running KDE from CVS is that you get to see all those new cool features :-)"
    author: "A Sad Person"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-14
    body: "Another step in the right direction!"
    author: "yg"
  - subject: "Special behaviour for Konqueror ? [Taskbar Group]"
    date: 2001-05-14
    body: "What about programs with multi configurations and icons ?\n\nI think about Konqueror.\n\nIn the screenshot, I see that icons are in the beginning of each line, and I guess they may be different as now in the task bar (favicons).  Well.\n\nBut for Konqueror it needs more, I think. It needs to have several buttons in the taskbar, one per profile (navigator, file manager, help...), a task-grouping for each-one..."
    author: "Alain"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "First of all, we need to see the big piture.\nDon't implement something just because it is in winXP or it is in Gnome. And if you really need to implement it make sure to implement it correctly and in a smart way. I am not sure about the details of KDE implementation, but here are my obvious observations:\n- Don't need taskbar grouping if there are only two konqueror windows are open. It just slows you down. You need two clicks instead of one click to activate windows. So put a limit (4-5) on open windows before grouping them.\n\n- Only group windows that really can show some differentiating text to identify which is which. An example is the url of the konqueror window. A bad example is gnome xterm windows. I have three xterms open. Gnome groups them and I am really frustrated since there is no text identifying which one is which. If all of them were on the taskbar, at least mentally I know their locations on the taskbar which makes it easy to switch.\n\n- Use the taskbar fully before switching to grouping. There is no point of grouping windows when the rest of the taskbar is empty. It just slows down the productivity."
    author: "Solhell"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "I agree with the points you've made."
    author: "foobar"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "That is the exact KDE mentality that makes it the best desktop enviroment. I hope the developers listen to the piece of wisdom above."
    author: "David"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "I'd definitely like to have the option to turn this feature off.  I have yet to run into a situation in my use of KDE where this feature would provide an \"ease of use\" advantage.  I'm sure that there may be other KDE users who will have occasion to use a grouping feature.\n\nMy opinion comes from use of konsole.  The multiple sessions in a konsole instance was a nice feature to try out, but in practice since, I never use it.  I want to have 4-6 konsoles open on a single desktop where I can view the output of each konsole.  Similarly, on the taskbar, I never have more than 4-6 applications open in any single desktop."
    author: "APW"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-18
    body: "I use the multiple session feature of konsole, it's fine\n\n;-)\n\nTim"
    author: "Tim"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "> Don't implement something just because it is in winXP or it is in Gnome. \n\nActually this was on the TODO-list of Matthias since long. If you look back into the archives of kde-look in december 1999 you'll notice that I have suggested this feature way before MS or Gnome implemented it.\nI think the very first OS which came up with taskbar-grouping to solve the issue of SDI cluttering up the interface was BeOS.\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Finally Tackat"
    date: 2001-05-15
    body: "So when do we get icons of the macos X quality in Kde?\n\nDont get me wrong. the current ones are good. But they are certainly not very eye pleasing.\n\nKeep up the good work.\n\nnalo"
    author: "naloquin"
  - subject: "Re: Finally Tackat"
    date: 2001-05-15
    body: "> So when do we get icons of the macos X quality in Kde?\n\nOh, I don't think that Icons in KDE look much worse than those in MacOSX- especially if you switch icon-size in MacOS X down to 32x32.\n\nIf you switch on Alphablending in kcontrol -> Look & Feel -> Icons and switch your icon-size to 48x48 things might look much better already to you:\n\nhttp://master.kde.org/~tackat/screenie1505.png\n\nIn current CVS we even already offer 64x64-icons which looks like this:\n\nhttp://master.kde.org/~tackat/greatkonq.png\n\nUsually I get several e-mails per day of people who tell me that they really like the KDE-icons and are very pleased with the work the kde-artist-team did.\n\nWhile I'm quite happy with the current state as well  there are still some icons KDE which I don't like yet (especially when it comes to application-icons) but this isn't something one could solve within a few days. KDE makes very much use of several different icons. In fact kdelibs has got around 800 icon-pixmaps.\n\nThe only solutions I can offer to you is: \n\n- wait until things get better (I'm working on those icons daily and they are improving that way). You should be aware though that KDE is not meant to look Geeky/Cool by default. We try to create icons which please most people and are good icons in terms of usability as well.\n- submit some better icons made by yourself to icons@kde.org which might get into KDE-CVS then.\n- Create yourself a MacOS X-icontheme. After all KDE can technically do 95% of the icon-stuff MacOS X can do (except for the bouncing icons on startup .. )\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: Finally Tackat"
    date: 2001-05-15
    body: "I would like to ask you a question regarding some MacOS X related eye-candy. Is there anything in KDE CVS that can emulate the minimise and maximise animations that exist in OSX ? Those animations are way too cool.\n\nCheers,\nRaphael"
    author: "Raphael Borg Ellul Vincenti"
  - subject: "Re: Finally Tackat"
    date: 2001-05-17
    body: "Before something like that were to happen, X12 (if that's what it is called) with a new rendering engine will have to be completed.  The current X is far too slow for a modern desktop.  Hopefully X12 will include the ability to dynamically change desktop sizes without restarting the X server, the way MS Windows does now."
    author: "Chris Bordeman"
  - subject: "Re: Finally Tackat"
    date: 2001-05-15
    body: "Well I think the icons are great!  Everyone that I show KDE to makes the comment that it looks so 'rich' and 'sharp'.  The beauty of the icons plays the most important role in making that impression.\n\nPlus the icons are designed as icons should be designed - clean, symbolic, and easy to interpret.  None of that silly 'photo realistic' stuff.. \n\nI have watched Windows users sit at my desktop and look around and they are quickly able to figure out what the icons represent.  \n\nExcellent work!  Thanks for a beautiful desktop!"
    author: "KDE Fan"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "Hitting the middle button on a grouped taskbar icon toggles between the different tasks. So, if you have two Konsole windows open, you can still toggle with a single click.\n\nI love it."
    author: "Brent Cook"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "Hmmm, interesting points.  In sawfish when toggling between windows, there is an option to warp the mouse pointer to different windows as they are selected.  This is very useful for telling apart all of my terminals.  \n\nPerhaps the mouse pointer could similarly be warped to windows as the mouse traverses the list of windows?"
    author: "Rob"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "Actually on the Xterm statement, it is fairly easy to title an Xterm and have that title show up in your taskbar.  I generally retitle my Xterm based on my current task.   I have a little alias called xtitle and I just type xtitle [title] and I am off to the races. I also rewrote the code for konsole to allow the title to show up in the task bar for the current session active in any konsole window.  But that patch never seems to get applied so it is sort of a private feature ;-)."
    author: "Anthony Moulen"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-16
    body: "I add the folowing to my .cshrc to set the window title to the current directory.\n\n# Set the console title to CWD for xterm\nif ( $TERM == xterm ) then\n        alias cwdcmd 'wtitle'\n        setenv WNAME `uname -n`\n        alias wtitle 'title ${WNAME}:${cwd}'\n        alias title  'echo -n \"\\033]0;\"\\!*\"\\007\"'\nendif\n\n\nWorks for xterm and konsole!"
    author: "JPK"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-17
    body: "That is great!"
    author: "Chris Bordeman"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "Interesting... This will unclutter the task bar a lot :-)\n\nAnd I'd like another wish: add a new command to tile the open windows (in the same location that \"Cascade Windows\" command).\n\nSuperb KDE team :-)"
    author: "J. Pongilioni"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "Good Job KDE Team! It looks like you implemented yet another one of Microsoft's good ideas (one of the few I've heard about in XP) into KDE!\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "Didn't MS steal it from Gnome though?"
    author: "David"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "The fact that GNOME had it first doesn't make it stealing.  Windows had a taskbar before KDE did, regardless of what each calls theirs.  Does that mean KDE \"stole\" it from Windows?\n\nSomething to ponder..."
    author: "dingodonkey"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "You do know that taskbar have existed in one form or another way before Windows did it?"
    author: "ac"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "like Acorn's RiscOS back in 1990???"
    author: "Adrian Bool"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "And didn't BeOS have it before Gnome?"
    author: "ac"
  - subject: "Mr"
    date: 2001-05-15
    body: "Actually I think the taskbar started with Mac."
    author: "Adam Black"
  - subject: "NextStep?"
    date: 2001-05-15
    body: "It really depends on what you call a \"Taskbar\".  The earliest example of the precise modern taskbar (\"Start\", \"K\" or \"Foot\" button with a foldout menu of programs, and tiled buttons containing each running program... all along a bar that is on the edge of the screen) is on the NeXTStep platform, first released (v0.8) October 1988.\n\nIt may well have a direct ancestor; it certainly has related UI interfaces that predate it.  But it's the first taskbar that really looks like the modern taskbar with familiar \"buttonized\" tasks, and a program launch button that flys out a menu.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: NextStep?"
    date: 2002-07-09
    body: "have you noticed the simalirtys of XP and NeXT.\n\nits creepy..."
    author: "Media left."
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-19
    body: "And besides, OS/2 Warp also had it in V3."
    author: "Daniel Pisano"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "Actually, I used this on my amiga, many years ago. MacOS has also let you select programs to activate via its task menu (I used that in os7.1, and it was probably there before that)."
    author: "Rikard Anglerud"
  - subject: "Taskbar Grouping In CVS: Other interesting option"
    date: 2001-05-15
    body: "What about this other way of configuring:\n\n- There is no default grouping limits. By default, the option is not activated.\n- By grading a \"group\" icon from the taskbar to the application I need to group, I activate the option. Other way can be :Adding the group option on the application popup menu.\n- The same can apply to a \"ungroup\" notion.\n\nThe whole Idea is that I can dynamically control it based on my actual requirement.\n\nEven better, Can it remimber my last setting for my next session?\n\nWhatever, that already a nice feature.\nRegards,\nStephane"
    author: "stephane PETITHOMME"
  - subject: "Re: Taskbar Grouping In CVS: Other interesting opt"
    date: 2001-05-15
    body: "> Adding the group option on the application popup menu\n\nNow this is certainly a good idea."
    author: "me"
  - subject: "Put it on taskbar button menu"
    date: 2001-05-16
    body: "Great work, this is a really nice thing to have available, but I think it should be off by default. I think it's better to have to turn it on once for apps like Konqueror and GIMP than to have to do two clicks every time.\n\nI think it should be on the taskbar button menu like always on top. There is already a feature (with a menu item on the window, though not on the taskbar) to save settings like always on top, could it be saved like that?\n\nAlso I think there should be a menu item for \"skip taskbar\". Currently the only way to turn that on is with kstart and it's very useful for e.g. GKrellM, XMMS, etc..."
    author: "Chris Boyle"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "You are missing the point here. \n\nThe point is not just so that you are saving on taskbar screen real estate, or just to group them so that you have a single point where you can swap between them. The real usability issue that hasn't been included is to kill all these open windows. Say, you have 10 konq. windows open. To close all 10 would simply be two clicks, one to click on the group listing and two to click on the option \"Close all active windows\" or something similar.\n\nThere is no point in grouping windows if you don't add this functionality."
    author: "KDEuser"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "I think this is a very good idea and I hope it is pursued. \nClosing multiple windows has been a pain so far in KDE.\n\nI haven't used the latest CVS so I don't know if this is there already, but here is another option I would like to see.\n\nSuppose you have the taskbar set to NOT group  windows together by default, but on occasion you want to group all your konqueror windows together !!\n\nRight click on ANY Konqueror entry in the taskbar\n --restore\n --shade\n -- ...\n --group with similar !!!\n -- ...\n --close\n\nOTOH, Suppose you have the taskbar SET TO group  windows together by default, but on occasion you want to UNGROUP the konqueror windows \n\nRight click on THE Konqueror entry in the taskbar\n --restore\n --shade\n -- ...\n --UNgroup windows !!!\n -- ...\n --close\n --close all :))"
    author: "Risto Treksler"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "Risto said :\n\n> I think this is a very good idea and I hope it is pursued\n\nMe too !\n\n> Suppose you have the taskbar set to NOT group windows together by default, but on occasion you want to group all your konqueror windows together !!\n> Right click on ANY Konqueror entry in the taskbar\n> --restore\n> --shade\n> -- ...\n> --group with similar !!!\n> -- ...\n> --close\n\nYes ! (or after an ungroup with parameter on group windows)\n\n> OTOH, Suppose you have the taskbar SET TO group windows together by default, but on occasion you want to UNGROUP the konqueror windows \n\nOr after a group with parameter on not group windows...\n\n> Right click on THE Konqueror entry in the taskbar\n> --restore\n\n-- restore all, I think\n\n> --shade\n> -- ...\n> --UNgroup windows !!!\n> -- ...\n> --close\n\nNo, not \"close\", only \"close all\", because the close function is above, for each window\n\n> --close all :))\n\nYes, good ideas !! I hope it is easy to improve..."
    author: "Alain"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "This is great, uncluttering achieved. Now I just need to figure out how to remove window decorations for my biff client and asclock. The FAQ only tells how to do this on KDE 1.x.\nThanks to all the great KDE developers."
    author: "Hyperchaotic"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "Try something like kstart --type Tool xbiff for your biff client. For asclock, run the 'Dock Application Bar' from the Kicker extensions menu then it will be docked to the screen edge.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-16
    body: "Thanks, Rich. I just ran out of Needful Things.\nBest regards, Hyperchaotic."
    author: "Hyperchaotic"
  - subject: "Kicker vs GNOME Panel"
    date: 2001-05-15
    body: "I am a fan of both KDE and GNOME, so don't think I'm trying to spark a war here. As an integrated environment, I prefer KDE. Konqueror is my favourite file manager and browser (Nautilus, IMHO, has been dumbed-down for newbies too much), and I find QT to be far faster than GTK.\n\nNevertheless, I am a GNOME user. Why, you ask? Mainly because of one thing: the GNOME Panel. This has got to be one of the most configurable and useful apps on the planet, sporting a multitude of applets (far more in number and sophistication than KDE's) and features. I love how I can have as many panels as I want, each with as much configurability as the next. I appreciate how I can make the panels any size I want, and place them anywhere on the screen (either along an edge or free-floating). The \"swallowed app\" feature, where you can embed any normal app in the taskbar (yes, even KDE apps), is incredibly useful. The GNOME panel even has a status dock that is fully compatible with KDE's.\n\nNow my point here is that GNOME's main strength (along with the very flexible Sawfish WM, but that's another story) is its panel. If KDE developed a similar panel, GNOME would have lost it's main advantage. GNOME users (except for the real die-hard ones) would all flock to KDE. KDE has about six months before GNOME 2.0 is due to be released - that gives some time for KDE to develop a top-notch panel application. In the meantime, however, I'll stick with GNOME."
    author: "Sridhar Dhanapalan"
  - subject: "Re: Kicker vs GNOME Panel"
    date: 2001-05-15
    body: "I wouldn't flock to KDE even if it did have a decent panel. Window manager's not very good.\n\nNow give KDE a good window manager and then I'd consider it... depending of course on how good GNOME 2 is."
    author: "MaW"
  - subject: "Re: Kicker vs GNOME Panel"
    date: 2001-05-15
    body: "You have a good point there. I did mention in passing that GNOME's other main strength was it's default window manager (Sawfish), which is highly configurable. Also interesting is the fact that GNOME doesn't try to tie you to one window manager as KDE does. In GNOME, you can easily switch to use another WM like Enlightenment, IceWM or WindowMaker if you so wish, whereas in KDE there is no easy way to achieve this.\n\nAnyway, I'll stop there, since this is most definitely off-topic :-)"
    author: "Sridhar Dhanapalan"
  - subject: "Re: Kicker vs GNOME Panel"
    date: 2001-05-15
    body: "That's not true anymore. In KDE 2.0, you can use any window manager that implements the new NETWM window manager spec. The spec is made together by KDE, Gnome and other guys. Currently compliant windowmanagers that I know of are KWin and Sawfish."
    author: "nap"
  - subject: "Re: Kicker vs GNOME Panel"
    date: 2001-05-16
    body: "There is also a project that makes FVWM compliant with the new spec\n\nhttp://fvwm-ewmh.sourceforge.net"
    author: "Grant"
  - subject: "Re: Kicker vs GNOME Panel (drifting off topic)"
    date: 2001-05-16
    body: "Just curious, but what do you think are the strengths of Sawfish?"
    author: "Doug Welzel"
  - subject: "Re: Kicker vs GNOME Panel (drifting off topic)"
    date: 2001-05-16
    body: "I love Sawfish because it is both light and *very* configurable. Like KWM, it is designed primarily to be used along with an environment like KDE or GNOME. Unlike other WMs like Enlightenment or WindowMaker, which are also designed to run stand-alone, Sawfish leaves almost everything (even simple things like configuring the desktop background) to the desktop environment. This allows it to remain lightweight, by avoiding unnecessary duplication of feature sets, and it minimises the risk of clashes with the environment.\n\nAt the same time, however, Sawfish is incredibly configurable. All window decorations are configurable (far more so than in KWM) and the entire user-interface policy is controlled through a Lisp-based scripting language, which can be manually edited to a user's desire. If one prefers to set up their WM graphically, Sawfish's graphical configuration tools are also very powerful (again, much more so than KWM). It is even possible (and very easy) to give each *window* its own theme (I don't know why someone would want to do this, but it's a good demonstration of Sawfish's power).\n\nMore information cab be found at the Sawfish website: http://sawmill.sourceforge.net/"
    author: "Sridhar Dhanapalan"
  - subject: "Re: Kicker vs GNOME Panel"
    date: 2001-05-16
    body: "I've never tried GNOME, but many years ago I used OS/2 with a 3rd party app called Object Desktop. OD had a panel alike, I think it was called the Control Center.\n\nIt was a desktop object the could contain a number of modules/objects, like virtual desktop, application launch buttons, status windows, etc (like Kicker). But you could open any number of \"control centers\", containing one or more objects/modules and they [CC's] could have any size and placement you wanted.\n\nI would prefer putting a Kicker vertically in upper right corner and configure it with a few application buttons, the kicker clock and some biff (like *Step - not spanning the height of the screen), then put another Kicker horizontally in the lower left corner with minimized or running app icons, that expands as they are put there.\n\nKicker can almost do this, only configurable width and multible instances are needed."
    author: "Hyperchaotic"
  - subject: "Re: Kicker vs GNOME Panel"
    date: 2001-05-20
    body: "Object desktop is still in existance, now on windows, though. One of the few ways to make windows usable..."
    author: "nobody"
  - subject: "Re: Kicker vs GNOME Panel"
    date: 2003-03-05
    body: "My favorite part of Gnome is the Menu Panel.  This panel creates a Mac like menu bar at the top of the screen (but without swallowing the menu bars from applications like OS X does).  Like the other panels, you can add applets and launchers.  I don't know why, but to me it always seemed natural to be able to drop down my menus instead of the whole push up thing.\n\nAnyway, I've used many desktops on many OS's from VAX to Irix to Mac to Windoze to KDE to Gnome, and Gnome's Panel is my killer app.  That plus GDM, but that's off topic.\n\nThis is a screenshot of my work laptop.\n\nOh, and before you flame me for using RedHat, my desktop PC is running SuSE, so yes, I have used a real KDE enviromnent!  :-)"
    author: "Nomad"
  - subject: "Re: Kicker vs GNOME Panel"
    date: 2003-03-05
    body: "You're replying to a nearly two year discussion, where kicker has pretty much caught up to the gnome-panel in many respects.. (except for floating (non-edge) panels, and gnome-panel drawers)... you can easily do menu panels in kicker with the quick browser special button :) "
    author: "fault"
  - subject: "Re: Kicker vs GNOME Panel"
    date: 2008-05-15
    body: "I do like the KDE not only because the stability and the significant feeling when look at it, but also as I am a embedded software developer, KDE and QT provide the primary GUI solutions for embedded devices. But now in the recent KDE4 of Fedora 9, I found the panel can't be hidden just as KDE3 did. This troubles me a lot, because I use the notebook with only 1280x800 resolutions, and the vertical resolution 800 is very important for me. Hope there could be a way to hide the panel as other OS does.\n"
    author: "Li Qi"
  - subject: "Re: Kicker vs GNOME Panel"
    date: 2008-05-15
    body: "It's on the list of planned features:\n\nhttp://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan\n\n(\"panel hiding\")\n\nbut unfortunately it looks like it will be punted to 4.2 (all features still marked as \"TODO\" on the 19th May will be postponed to 4.2)."
    author: "Anon"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "I wonder if this has made it into the the External Taskbar _Extensilon_, which IMHO is better than the Taskbar _Applet_ (shown in the screenshot)\n\nSpeaking of the External Taskbar Extension,\nwouldn't it be nice if it also had a \"windows list button\", like the one in the Taskbar Applet.\n \nIts absence almost qualifies as a bug!!\nI mean there is an option for \"show windows list in taskbar\" in the kcontrol module, but the button itself is nonetheless absent from the External Taskbar Extension. :((\n\nIt'd also be nice to have an option to set the min/max height of the External Taskbar Extension.\n(I think I'd like it at \"minimum: 2 rows\")"
    author: "Risto Treksler"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "Done"
    author: "Matthias"
  - subject: "What about enhanced Grouping?"
    date: 2001-05-15
    body: "There have been suggestions on how to group windows efficiently. I'd like to add another scheme for konqueror: Windows could be grouped on a host-by host basis - all pages on dot.kde.org in one group, all on apps.kde.com on another and so on.\n\nIt's a great feature, go on guys!"
    author: "Sebastian Kuhnert"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "It is very good !\nBut where is the icon floppy in kicker when a file change ?\nAnd where is the animation when a soft starts ?\nInthe CVS the animation in ON the icon. It is not beautiful !\n\nBut news features are very good ideas !\n\n\nThanks\n@+\nJP"
    author: "Martin"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-15
    body: "I've always known, that a taskbar is a bad thing,\nand this effort shows up, that there are only limits extended.\nSo please investigate more to a tool currently\nreachable by ALT-Tab which could show up the\nhirachical list / tree of the opend XAplications.\n\nSo please consider, that <b>the taskbar desktop metaphor is wrong</b>. Nobody would nowadays would \nthink of files only in a flat way, today always\nas a tree.\n\nAnyhow thanks, <b>many</b> thanks for your efforts.\nKay"
    author: "Kay Winkler"
  - subject: "Re: Taskbar Grouping In CVS"
    date: 2001-05-16
    body: "Take a look at the attached image, it shows the UI I planning for nested groups of tasks in Kasbar. I think this shows that you can handle it nicely within the taskbar metaphor.\n\nRich."
    author: "Richard Moore"
  - subject: "Grouping coming soon in Kasbar"
    date: 2001-05-16
    body: "Now that Matthias has done all the hard work(thanks!), I'll be adding grouping to Kasbar too. You can see a screenshot of the grouping ui for Kasbar at:\nhttp://www.ipso-facto.demon.co.uk/development/kasbar/crop-medium-grouping-ui.png\nThe items in the popup group bar will be have the same feature set as items in the main bar (eg. they'll still have thumbnails).\n\nThere are some other images in this directory which show other features of the program - everything there except the grouping is in the latest CVS versions.\n\nRich."
    author: "Richard Moore"
---
Sure to put an end to countless discussions on the ever-lively <A HREF="http://www.kde.com/maillists/list.php?li=kde-look">kde-look</A> mailing list, and to cut off a stream of <A HREF="http://bugs.kde.org/">"wishlist" bug requests</A>, KDE <A HREF="http://www.kde.org/anoncvs.html">CVS</a> (look for it in the upcoming <A HREF="http://developer.kde.org/development-versions/kde-2.2-release-plan.html">KDE 2.2-beta1 release</A>) now features task-grouping in Kicker, the KDE panel.  As is shown in this <A HREF="http://ftp.kde.com/dot/img/taskgroup_screenie2.jpg">screenshot</A>, task grouping optionally groups all windows opened by the same application in the same task-bar button. Here, 17 Gimp windows can be seen grouped in the same taskbar button, and the same can be seen for Konqueror and Konsole windows.  With no more overflowing task bars, I am a happy camper :-).