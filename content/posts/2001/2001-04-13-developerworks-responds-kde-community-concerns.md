---
title: "developerWorks Responds to KDE Community Concerns"
date:    2001-04-13
authors:
  - "Dre"
slug:    developerworks-responds-kde-community-concerns
comments:
  - subject: "deadline"
    date: 2001-04-13
    body: "Great! Seems like IBM is getting it - accept feedback and  work with the community....\n\nAnother thing they should probably change is the deadline. I imagine that a really good theme needs\nmore time than 2 weeks to make. This way they will limit the number of possible participants (who\ndoesn't have the time right now to do this) and make do with an assemblage of relatively hurried work."
    author: "Wilhelm Schlegel"
  - subject: "Re: developerWorks Responds to KDE Community Concerns"
    date: 2001-04-13
    body: "Good, I am going to donate my 2000 $ to KDE :)."
    author: "Antialias"
  - subject: "Re: developerWorks Responds to KDE Community Concerns"
    date: 2001-04-13
    body: "Man thats cool!! Go IBM!!\n\nCraig"
    author: "Craig Black"
  - subject: "Re: developerWorks Responds to KDE Community Concerns"
    date: 2001-04-13
    body: "Yeah !!!\n\nIBM really tries to express this \"Love\" thing they're advertising with :))\n\nThanks IBM !!"
    author: "Jelmer Feenstra"
  - subject: "Re: developerWorks Responds to KDE Community Concerns"
    date: 2001-04-13
    body: "Impressive...most impressive.\n\nThis is the way companies interested in \nOpen Source/Free Software are supposed to\nbehave.\n\nYou make money thanks to community's efforts,\nthen give something in reward and respect the\ncommunity itself\n\ncool =)"
    author: "msx"
  - subject: "Re: developerWorks Responds to KDE Community Concerns"
    date: 2001-04-13
    body: "> we'll be sacrificing a lawyer to the OSS gods for that rule tonight at midnight\n\nNow that's what I call commitment... exercising the \"death clause\". :-O Will this be webcast? ;-)"
    author: "Eric Laffoon"
  - subject: "Re: developerWorks Responds to KDE Community Concerns"
    date: 2001-04-14
    body: "I hope so. I wonder if they can set up a poll so that we can chose the method of sacrifice."
    author: "Mark Hillary"
  - subject: "IBM - the goooood guys!? (was: developerWorks...)"
    date: 2001-04-14
    body: "[Caution: lots of exclamation marks following]\n\nYou are right: It is nice to see this reaction of IBM and sacrificing one of their thousands of lawyers surely is a very good idea!\n\nEven more nice would be to let us choose *whom* to sacrifice: I would like to select one of their top-patent-layers!\n\nHonest, cari amici, of course it is nice (and wise) that they have reacted that way this time but while 'praising' them do NOT forget that IBM is constantly filing lots (and I mean LOTS) of patents worldwide!\n\nThey are the main fighter for software-patents in the european community - this does *not* look like a main friend of free software would act - a themes contest doesn't change this at all!\n\nI once had a boss who told us \"Just put money on it!\" when being asked how to solve a problem.\n\nThe funny \"We love Linux.\" thing presented to us by IBM recently reminds me to that former boss: IBM seems to think that putting money on Linux is the way to salvation.  Perhaps it is a good thing for IBM to do this but does that mean it is also a good thing for the Linux Community?\n\nIn my (absolutely unimportant) opinion the patents-issue should be addressed aggressively next time the wannabe nice people from IBM are offering their so-called help!\n\nMoney from people who (at the very same time!) are die hard fighting against fundamental open source ways of sharing ideas and sharing code is kind of bad money.\n\n(just my 2 pence, feel free to ignore it - but a \"Non olet pecunia!\" won't change my mind.)"
    author: "Karl-Heinz Zimmer"
  - subject: "Donate to an organisation that is against patents?"
    date: 2001-04-18
    body: "You could ask your prize money to be donated to an organisation that lobbies against software patents. At least that may leave a mark with management.\n\nDoes anyone know of such organisations?"
    author: "Daniel"
  - subject: "Re: IBM - the goooood guys!? (was: developerWorks...)"
    date: 2001-04-18
    body: "How about this: <daydream> IBM tries to get as many software patents as possible, only to let the  open source community use them freely! Patents are of no danger if the owner of the patent is your friend </daydream>. It's just a silly dream, I know :-("
    author: "Alexander Kuit"
  - subject: "Talk about timely correction and care..."
    date: 2001-04-13
    body: "Seriously, I didn't fully mind the contest as it was, but I did feel very queasy about it...\n\nTalk about Timely correction, and care!\n\nNow when's IBM going to start selling linux desktop computers???\n\n\nAnyway, good job IBM.\n\nAll of us KDE user's aplaud your corrections!"
    author: "Greg Brubaker"
  - subject: "Re: Now when's IBM going to start selling ...."
    date: 2001-04-17
    body: "IMHO IBM sells Netfinity Servers with SuSE Linux."
    author: "Stephan Boeni"
  - subject: "Re: Now when's IBM going to start selling ...."
    date: 2001-04-22
    body: "IBM sells Linux systems, but mostly business oriented(Suse Servers, and Caldera Notebooks).  Tell me when they start pre-installing Loki game packs on Linux boxes...  Not until then are they selling CONSUMER desktops."
    author: "Greg Brubaker"
  - subject: "Re: developerWorks Responds to KDE Community Concerns"
    date: 2001-04-14
    body: "They have my respect.\n\nPeace Man\n:-)"
    author: "Mark Hillary"
  - subject: "Why was the dot (and apps) down?"
    date: 2001-04-17
    body: "Can we have a news story as to why apps.kde.com and the dot were down?  Or was it just me and everybody I talked to?\n\nInquiring minds want to know...\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Why was the dot (and apps) down?"
    date: 2001-04-18
    body: "This has been a problem a couple of times, I don't know why the KDE people are still having the same people host the sites or have not switched to a non Zope based message board."
    author: "AC"
  - subject: "Re: Why was the dot (and apps) down?"
    date: 2001-04-18
    body: "IIRC, last time it was ISP trouble - not zope trouble."
    author: "kdeFan"
  - subject: "Re: Why was the dot (and apps) down?"
    date: 2001-04-18
    body: "Maybe they should rename this site to http://down.kde.org.\n(Sorry about that, I hope irony works)"
    author: "reihal"
  - subject: "Re: Why was the dot (and apps) down?"
    date: 2001-04-18
    body: "It's not that Zope is broke, it's that Zope is hard to move if the server goes down."
    author: "AC"
  - subject: "Dot Editor Conserving Neural Energy"
    date: 2001-04-18
    body: "> It's not that Zope is broke, it's that Zope is \n> hard to move if the server goes down.\n\nIt's only a matter of installing the software and importing the zexp.  I try to keep a backup zexp available generally.  And FWIW, we had hardware problems.\n\nOn this note, I'm taking a break as editor of this site for personal reasons.  Fortunately, we have several other hardworking ones.\n\nSome stories I'm hoping will be covered in one form or another in the future but that I won't be taking care of myself:\n\n- KDE presence at GUADEC II, Interoperability\n\n- KC KDE #6\n\n- Rich Moore interview, and other interviews\n\n- LinuxTests review\n\n- New devel articles by Rich, Rik and co.\n\n- Qt Mozilla\n\n- etc.\n\nSee you in May or so.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Dot Editor Conserving Neural Energy"
    date: 2001-04-18
    body: "Thanks Navindra, you did a great job :-)"
    author: "kdeFan"
  - subject: "Thanks IBM :)"
    date: 2001-04-18
    body: "Your support to Open Source is very much appreciated by us (the users). Thanks a lot.\n\nIndeed if I win I would donate it to KDE and Gnome 1/2, 1/2 :)"
    author: "Asif Ali Rizwaan"
---
Yesterday we posted a <A HREF="http://dot.kde.org/987013890/">story</A>
about the <a href="http://www.ibm.com/developerworks/">IBM developerWorks</a>'
<a href="http://www-106.ibm.com/developerworks/linux/library/l-kde-c/?open&l=1974,t=gr,p=kcst">KDE
Theme Contest</A>.  It quickly became apparent that the contest had a few
shortcomings, which were noted in another update and in the comments from
our readership.  It turns out that the people working on the contest were
new to the Open Source community and KDE and did not realize the mistakes
that were made.  I drew <A HREF="http://www.ibm.com/">IBM</A>'s attention
to these problems and when the Open Source leadership at IBM noted them
they immediately recognized and corrected them.
An explanation from <a href="mailto:ssdhanoa@us.ibm.com">Shailendra</A>
follows.  Kudos to IBM for being so receptive and responsive to our
community!  (Also check out the <A HREF="http://www.newsforge.com/">NewsForge</A> <A HREF="http://www.newsforge.com/article.pl?sid=01/04/12/229234">story</A> on this.)




<!--break-->
<br><br>
<TT>
<A HREF="mailto:ssdhanoa@us.ibm.com">Shailendra</A> from developWorks writes:
</TT>
<blockquote>
<P>
Due to an overwhelming response from the community, calling us on a
few not-so-subtle gaffs, we have changed our KDE theme contest. First and
foremost, entering the contest does not mean you are giving us ownership of
your code or exclusive rights to it (we'll be sacrificing a lawyer to the OSS
gods for that rule tonight at midnight). All your entries (see revised rule 7
on the
<A HREF="http://www-106.ibm.com/developerworks/linux/library/l-kde-c/?open&l=1974,t=gr,p=kcst">contest
page</A>) must be licensed under an OSI-approved public software license. Yes,
we screwed that one up the first time. Apologies!
</P><P>
Secondly, so that we can make the contest open for contestants worldwide,
winners will be able to choose a non-profit, Open Source organization to which
dW will make a donation on the winner's behalf. Unfortunately, giving away
cash worldwide is another legal quagmire we were trying to keep our foot
out of. Turns out we put it in our mouths instead :)
</P><P>
Finally, themes eligible for the contest are to be compliant with KDE 2.x or
KDE 1.x, not versions 1.1.x and 1.2 as we accidently wrote in our previous
rules! Whew!
</P>
<P>
Shailendra<BR>
ssdhanoa@us.ibm.com
</P>
</blockquote>


