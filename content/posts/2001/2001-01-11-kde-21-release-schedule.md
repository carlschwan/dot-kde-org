---
title: "KDE 2.1 Release Schedule"
date:    2001-01-11
authors:
  - "Dre"
slug:    kde-21-release-schedule
comments:
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-10
    body: "Can we get RedHat binaries this time?  They'd be awfully nice."
    author: "Justin Lee"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-10
    body: "for a beta release it is better that most people try to compile the code before using it to detect compile-time bug specific to a particular system.\nBut i believe that binary packages of KDE2.1 final will be avaible just after the official release."
    author: "renaud"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-10
    body: "http://www.linux-easy.com/daily/"
    author: "j nordstrand"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-12
    body: "take a look at http://www.linux-easy.com/daily/"
    author: "Matthias Braun"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-10
    body: "For the final 2.1 release can we get mandrake 7.2\npackages this time instead of cooker packages that are compiled against a different version of glib\n(which has *many* dependencies)????"
    author: "Sheldon"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-10
    body: "I'm sure someone at Mandrake will make Mdk7.2 RPMs out of 2.1final, and even of 2.1beta2. Look for them at www.mandrakeforum.com! I also suggest you install their 2.1beta1 RPMs; they're pretty stable plus you can do your share of bugtesting :)"
    author: "Haakon Nilsen"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-10
    body: "Just point your mandrake update agent the develope part at this ftp site they have recent mandrake rpms for 2.1 beta.\nftp://ftp.sunet.se/pub/Linux/distributions/mandrake-devel/unsupported/i586\n\nCraig"
    author: "craig"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-11
    body: "It's a shame to be dependent on a company - Mandrake - for RPM upgrades. Isn't this a bit like being a Windows user?\n\nSure, if you have time on your hands, you can legally hack it, that's good. But, you can't just go to the KDE site and download the latest version - you have to have a Mandrake format, or a Red Hat format or...\n\nGNOME/KDE/Linux - still a long way behind Windows for the average user.\n\nI'm only saying this because I believe it's true, and that it needs to change."
    author: "Lars Janssen"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-11
    body: "We're going somewhat off-topic here, but your message is a non-sequitor.  If you're the `average user', you can wait until your OS vendor releases official binaries of KDE 2.1, just as you would do with commercial software.  Meanwhile, if you're so inclined, you also have the choice of downloading the latest Beta and compiling it yourself with very little effort (it only requires a couple of minutes of your time, though your computer will be busy for a few hours).  You also have the choice of using binary packages compiled by a third party (such as most of the ones on the KDE ftp site), if someone happens to have put them together.  With Windows, only the first option is open to you.  KDE/*nix may lag behind Windows in many ways, but this ain't one of them."
    author: "Stephen Cornell"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-11
    body: "Thanks for some good reasoning there.\n\nI do think there's some space between people who are reluctant to mess with their system at all, and those who are happy to do a lot of tinkering and compiling, resolving dependencies etc.\n\nI find the 'fragmentation' (diversity!) of distros and packages a little worrying, but I accept your points - we can't have everything! :-)"
    author: "Lars Janssen"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-14
    body: "of course <insert your favorite package format here> is going to lag behind source distributions ;-)\n\nInstalling QT and/or KDE from source is extremely simple in comparison to a number of other software offerings.  \n\nThe various package formats are nice for figuring out dependencies... and giving you 'sorry... you need X to install Y' type feedback - but there are also sane methods for add/remove on installs from source.\n\nIt's a tough call - a particular vendor has visions about making things 'easier' - but it's probably better to keep help/troubleshooting as generic as possible... and leave it to the users of distribution-specific tools to read between the lines and adapt to their systems.\n\n- Jason"
    author: "Jason Byrne"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-10
    body: "Is there a CHANGELOG somewhere we can read?\n\nWhat will be new?"
    author: "Joe Dorita"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-10
    body: "Some changes are mentioned in the <A HREF=\"http://www.kde.org/announcements/announce-2.1-beta1.html\">beta1 release notes</A>."
    author: "Joshua"
  - subject: "Re: KDE 2.1 Release Schedule [SUSE 6.4 ]"
    date: 2001-01-11
    body: "Will SuSE make a 6.4 RPM of KDE 2.1\nwhen it is released to the world?\n\nDont forget US SuSE 6.4 users who have\nnot upgraded bother to 7.0, because\nit we think it isn't worth it (yet). \nWe' will wait until 7.1 or 7.2\n\nPlease dont forget us in your package RPMS\nor tell us what incompatible with 6.4\nat least"
    author: "Peter Pilgrim"
  - subject: "Re: KDE 2.1 Release Schedule [SUSE 6.4 ]"
    date: 2001-01-11
    body: "I second this - nice if 2.1 is made for 6.4, I'm waiting for 7.1 too.  Well done everyone at KDE, fantastic job."
    author: "Fergus Wilde"
  - subject: "Re: KDE 2.1 Release Schedule [SUSE 6.4 ]"
    date: 2001-01-17
    body: "some to me. waiting for 7.1\n--> 6.4 rpm's would be great"
    author: "Gunnar Johannesmeyer"
  - subject: "KOffice"
    date: 2001-01-11
    body: "What's happening with KOffice apps these days?   Seems very quiet!  Anything new for KOffice in 2.1?  Is anyone still working on them?\n\nMacka"
    author: "Macka"
  - subject: "Re: KOffice"
    date: 2001-01-11
    body: "The Beta 1 release notes linked to further up the page mention specifically that KOffice is NOT included in the beta!!!  What's up with that?  IMHO, KOffice is an essential part of KDE, but it seems like it's getting very little attention.  The majority of the KOffice website hasn't been updated in months.  It's still got those same old \"More Information Soon!\" messages.  Well, \"Soon\" has come and gone, and there is still no information there.  Has such an important project been left to die?  Or is it just that the developers haven't bothered to tell anyone about their work? (I'm hoping it's the latter)"
    author: "not me"
  - subject: "Re: KOffice"
    date: 2001-01-11
    body: "Sometimes open source can be a bitch. We have been looking to do more with Quanta but you know when you have a life and projects you work on into the night...\n\nWhat I was curious about is how your open source project is going? Well? Not a programmer? Did you know the programmer on kpresenter learned to program to develop that app? The thing you have to ask yourself is this. How does all this open source software get here anyway? it is sad how few people actually make a difference but amazing what one motivated person can do. You sound motivated.\n\nBTW you can look at cvs and see what has been done by who and when. There is a web cvs to see all of that and it's at kde.org. Koffice is not dead... although kword has not seen much in recent weeks kspread is moving along, kivio has and krayon is seeing heavy develpment and is very encouraging. Graphite is seeing some work too.\n\nUnlinking koffice from KDE is also not good conspiriacy theroy material. Putting krayon in a feature freeze this month is ludicrous... same for Quanta now that I think about it. It allows koffice to release seperately and manage it's development by it's needs and standards. Frankly koffice might be more feature rich today if it had been removed from kde 2. Hmmm?\n\nAll of koffice could use not only developers but documentation, input, encouragement... you could make a difference."
    author: "Eric Laffoon"
  - subject: "Re: KOffice"
    date: 2001-01-11
    body: "Wasn't there something in a press release a while back about theKompany employing 2 people to work on Koffice?\n\nMacka"
    author: "John"
  - subject: "Re: KOffice"
    date: 2001-01-14
    body: "Sorry my last post sounded kinda negative, I don't want to make anyone mad.  I'll have to take a look at CVS, and maybe look into making a KWord filter or something.  BTW, I do have a little open source project, not related to Linux (actually a small game for the TI-89 graphing calculator).  It's about done, and I've been wanting to get into KDE/QT programming, so I'll give it a try.  Anyway, good luck to all KOffice developers, and I hope they can get their websites updated soon, even if they only say \"We're still here!  More next month!\""
    author: "not me"
  - subject: "Re: KOffice"
    date: 2001-01-11
    body: "AFAIK KWord went for such major changes right after KDE 2.0 that it did not make sense to fit it into the schedule of KDE 2.1, so KOffice will probably have a separate release later."
    author: "ac"
  - subject: "Re: KOffice"
    date: 2001-01-12
    body: "KOffice is still beta software. A newer snapshot *might* be included in 2.1, but it would still be beta. In my opinion, one of the big mistakes of KDE 2.0 was including KOffice without clearly mentioning its beta status."
    author: "David Johnson"
  - subject: "KDE 2.1 and Anti-Alias"
    date: 2001-01-11
    body: "Does anyone know if we can we hope for a fully working Anti-Aliasing in 2.1 (or is this only  depending on the xf86 guys)?"
    author: "Joshua"
  - subject: "Re: KDE 2.1 and Anti-Alias"
    date: 2001-01-11
    body: "The Mandrake Cooker rpms of kde2.1 post beta 1 and xfree86-4.0.2 contain anti-alias fonts.  Of course you need to upgrade your glibc libs to ver 2.2 and download compat-glibc for backwards compalibility (also from the Cooker), but be beware you can seriously damage your system with this packages. (my 3d h/w accel hasn't work since I upgradeed to xfree86-4.0.2 and I get locale errors messages on perl and gtk apps since I upgraded glibc :("
    author: "Jose"
  - subject: "Re: KDE 2.1 and Anti-Alias"
    date: 2001-01-11
    body: "For anti-aliased fonts you need:\n- XFree86 4.0.2 compiled with anti-aliasing enabled\n  (the binaries from xfree86.org have anti-aliasing disabled)\n- Qt compiled with anti-aliasing patch applied\n- FreeType\nAny KDE 2.X version will do, I'm using 2.0.1 myself. I didn't have to recompile KDE, only XFree86 and Qt."
    author: "Maarten ter Huurne"
  - subject: "Re: KDE 2.1 and Anti-Alias"
    date: 2001-01-11
    body: "Where can i find the anti-alias patch for qt?"
    author: "Andre"
  - subject: "Re: KDE 2.1 and Anti-Alias"
    date: 2001-01-11
    body: "just get qt-copy from CVS, do your configure with\n-xft or something in the way (./configure --help will tell you). Works as a dream, and is DAMN BEAUTIFUL :o)"
    author: "emmanuel"
  - subject: "Re: KDE 2.1 and Anti-Alias"
    date: 2001-01-11
    body: "Would anybody like to post some screenshots w/ AA enabled?  I think I saw some a few weeks ago, but I think I also recall that not all of the desktop was stable/functional at that time either.  Does it work perfectly w/ AA enabled?  If so I'll bother to do the CVS thingy..."
    author: "Hans"
  - subject: "Re: KDE 2.1 and Anti-Alias"
    date: 2001-01-12
    body: "http://www.xfree86.org/~keithp/render/"
    author: "matthias Wieser"
  - subject: "Re: KDE 2.1 and Anti-Alias"
    date: 2001-01-12
    body: "feast your eyes.....\n\nposted with fully AA-KDE"
    author: "ac"
  - subject: "Re: KDE 2.1 and Anti-Alias"
    date: 2001-03-26
    body: "you only had to recompile Xfree86 ?\nsure, it'll only take a few days on my PII 500 :-)"
    author: "fred"
  - subject: "Re: KDE 2.1 and Anti-Alias"
    date: 2001-11-28
    body: "Well, all I had to do was download the latest X-Windows from xfree.org or wherever, install it using the script, hack around getting my login screen to work again, and voila, Anti-Aliased text everywhere!  Really bloody good when almost every KDE program I run starts doing some run-away scroll bar flickering, and konquerer screws up badly when displaying it's own farging welcome page... see a screenshot: http://www.kyhm.com/~mbevan/images/kde/this-bruiser.png\n\nIt's so bad, I have to type this text either in a text-based bruiser, or Opera (opera good, but doesn't support a-aliasing.)\n\nhttp://www.kyhm.com/~mbevan/images/kde/this-bruiser.png\nSee this page under konquerer!\n\nhttp://www.kyhm.com/~mbevan/images/kde/aa-sweet.png\nSome things do look kick-ass sweet. (shameless plug!)"
    author: "Matthew Bevan"
  - subject: "Can We Get Installer Script for KDE 2.1?"
    date: 2001-01-11
    body: "HelixCode Build a Installer Script for Gnome 1.2\nIt's is very good script, the installation take few second's.\nbut to install Kde 2.0 you need to Install some Rpm, that every one of them had dependences problam(Like Xfree dependence and more)\nso if you will make a Installer that Install Kde 2.1, and solve the dependences probalm(by useing the internet, and website's like ftp.kde.org)\nso if there is dependence problam it will be download by the installer script.\n\nthanks\nOr Fialckove"
    author: "Or Fialckove"
  - subject: "Re: Can We Get Installer Script for KDE 2.1?"
    date: 2001-01-11
    body: "Thanks for volunteering. Tho I'm not a C++ programmer myself, I can help you with the Tedium(C) bits."
    author: "ne..."
  - subject: "Re: Can We Get Installer Script for KDE 2.1?"
    date: 2001-01-11
    body: "What? \ni want that KDE team will make the Installer\nNot ME!"
    author: "Or Fialckove"
  - subject: "Re: Can We Get Installer Script for KDE 2.1?"
    date: 2001-01-11
    body: "I think his point was instead of whining about the lack of something, get of your butt and do it yourself!"
    author: "Anon"
  - subject: "Re: Can We Get Installer Script for KDE 2.1?"
    date: 2001-01-11
    body: "1. i dont know C++\n2. i dont work for KDE\n3. this is KDE Team Work, not me.\n4. i only asked for thing that make Kde Install easiest"
    author: "Or Fialckove"
  - subject: "Re: Can We Get Installer Script for KDE 2.1?"
    date: 2001-01-12
    body: "Noone can _work_ for KDE, everyone can _contribute_ to KDE!"
    author: "Datschge"
  - subject: "Re: Can We Get Installer Script for KDE 2.1?"
    date: 2001-01-12
    body: "Hey loser,<BR>Why not run to your local bookstore and buy a book about kde and c++ programming.<BR><BR>If you can't do that or don't have time to do it, how do you believe that other people will do it when it is already really easy to install packages...<BR><BR>gee, get a life."
    author: "Sam"
  - subject: "Re: Can We Get Installer Script for KDE 2.1?"
    date: 2001-01-11
    body: "I think his point was instead of whining about the lack of something, get of your butt and do it yourself!"
    author: "Anon"
  - subject: "Re: Can We Get Installer Script for KDE 2.1?"
    date: 2001-01-12
    body: "This is one of the points I was looking at and I sent a detailed proposal mail to Waldo but never got a reply. The KDE team thinks that its up to distro makers to package KDE and this is a fair point. My idea also means changing CVS around to a more package friendly manner so maybe this is why I got no responce? The mail is copied below.\n\nMail to Waldo:\n\n[Snip printing blah]\nI'm studying for my CCIE (high level Cisco thing) right now, my exam is in \nFebuary once that's out of the way, my life is a lot easier; I can then \ndevote a lot of time to KDE.\n\nOn to packaging, I'd like your ideas on this: One of the \"problems\" I can see \nwith KDE is its packaging. The packages are too large and although they cover \na specific area i.e. networking, admin etc I don't want to install all the \nbits of each package. For example say I wanted to manage my ftpserver via a \nnice gui, I then also install a user manager, a cron job manager a dat drive \ntool etc. Say I want an email client (kmail) as most people do, I also get an \narchcie client, a mail reader, a PPP daemon config util, a AOL instant \nmessanger etc etc... Most users install KDE from distributions and this is \nexactly what they see, a heap of apllications that they don't need and for \nthe most part don't even understand.\n\nI think Linux has too many applications (well, to many similar apps anyway) \nand too many options. Bizare as it might seem this holds it back from the \naverage user.  I'd say with the current satus of Linux mainly power users and \ntechies use it, these users are the kind that only want to install what they \nneed. Of course this also can be blamed on the distribution makers but we \nneed to make it easier for them as well, the current way KDE make releases \nisn't that effective.\n\nSolution:\n\n1. KDE core should be maintained as it currently is the 3 main packages:\nkdesupport\nkdelibs\nkdebase\n\nIMHO I think kdesuppport should be phased out if possible but this will take \ntime? Possible solution to getting rid of kdesupport would lie with the \ndistribution makers. I *think* KDE currently needs mimelib and libaudio from \nkdesupport on my system even though it contains other packages, these aren't \ncompiled by default as I guess I already have them. Surely we can just make \nthese few libs a dependancy for KDE base?\nLook at it this way, we need openssl for crypto support in konqy, this is an \nimportant feature, why's it not in kdesupport? What about Lesstif for \nnetscape plug ins or a java implimentation? Of course mimelib and libaudio \nare version critical to the correct operation of core fuctionality of KDE \nwhere as the other libs mentioned above provide \"nice to have\" functionality. \nEven so, I can see this only from two sides, which are:\n\nA) Techie compiling KDE from source. If he/she has the knowledge to compile \nKDE from source they should have the knowledge to get the dependant packages. \nIf he/she doesn't have the ability  to read figure out why configure \ncomplained about a missing package and read the README what are they doing \ncompiling KDE themselves.\n\nB) Average Linux dude that installs from a distribution. The distribution \njust has to package KDE correctly with the correct libs as a dependancy on \nthe KDE RPMs / APTS / slackwware packages. This can't be that hard, the \ndistro people should get this right (I'd hope or else their in the wrong \ngame), apart from that David Faure / Mosfet are at mandrakesoft, Bero is at \nredhat, surely there are employees of the other major distros involved in the \ncore team that could advise their colleauges.\n\n2. Seperate all the apps out of their current groups so that for example \nkdenetwork, utils, multimedia etc are no more. A solution to this would be to \nsort CVS out in to two groups:\n\nA) released: These are apps that are currently maintained, are actively \ndeveloped and considered up to a high level. Basically 99% of the apps that \nnow make up the various parts if the distribution. New work is commited in to \nHEAD as it is currently and releases are taged when the author(s) feels the \napp has new fuctionality or a large bug sqashed. Of course releases should be \ncompatable with the current stable core release.  When a new release is made \nthe tag should be packaged, for now this is a source only package in tar.gz \nformat (bare with me, works with the installer..). It must contain a file \ncalled KDE_RELEASE, this file has a strict layout or is in XML and contains \nat least all of the following information:\n\nname\ndescription\nversion\nchanges\nimportance\n\nOnce the tag has been placed and the KDE_RELEASE file added a script can be \nrun against CVS to automatically package it, copy it to the KDE FTP site and \nremove the old version, it then gets mirrored from the KDE main site. The ftp \nsite would be layed out the same way CVS is:\n\nftp.kde.org:     |\n                 |\n\n                 - stable\n                         |\n\n                         -kmail-1.9.99.tar.gz\n                         -ark-1.2.3.tar.gz\n                         -konsole-1.0.1.tar.gz\n                         -kuser-1.9.8.tar.gz\n                         -....\n                         -....\n                         -....\n\nOnce every hour a script can be run on the ftp server to extract the \nKDE_RELEASE file from any new packages if they exist (based on time created I \nguess). This would update a file called KDE_RELEASES that lives in the root \nof stable, obviously this file contains up to date information on all \nreleased packages.\n\nIn it's self this is a far better way to release KDE for the power users out \nthere who want a stable release but can compile what they need from source \nand I'm sure it will help the distros to package KDE better. This is not the \nend goal, the installer is.\n\nThe Installer: The installer is a simple gui app that acts with two \nfunctions, firstly it can install KDE once the base packages are in place and \nsecondly it can keep KDE up to date. In the KDE root directory on the local \nmachine lives a file called KDE_INSTALLED, this is a DB (XML / plain text) of \nall packages installed on the machine and is created when kdebase is \ninstalled. Its updated by the \"make install\" script when a package is \ninstalled or as a post install script from RPM /apt etc. When the installer \nis run, it loads the KDE_INSTALLED file and asks where it should check for \nnew packages, either locally or the Internet. Locally brings up a location \nselector, so that for example magazine CDROM can contain packaged updates (a \nlot of people are still on dial up) or to install 3rd party software \n(something not released by KDE e.g. kcpuload). The Internet option connects \nto the ftp.kde.org and downloads a list of current mirrors, the user selects \na mirror and the KDE_RELEASES file is download. The installer compares this \nwith KDE_INSTALLED and shows updates to installed packages + info such as \nimportance, description etc. If the user wants to install a selected package \nthey select download & install, the installer then gets the package and \nperforms a configure, make, make install. This exact commands should be \nconfigurable so that --enable-final, install-strip or lib paths could be \ncould be configured for example.\n\nAs for installing KDE, the installer should have two radio buttons one for \nupdate that works as described above and only checks for installed packages \nand the second lists all packages where multiple packages can be selected and \ninstalled. Further options are for the installer to live in the system tray \nand check at regular intervals for updates from their favourite mirror, when \nan update is availible for an installed package it alerts the user.\n\nThe future: I think up to now, this gives KDE a solid base to expand this \nidea, sorts out CVS and it certainly lets the users decide what they want to \ninstall instead of the ever growing kdenetwork, kdeadmin etc packages. It \nhelps the distributions and is atleast platform indipendant. The big problem \nwith this is that packages need to be compiled on the users machine so its \nnot perfect for everyone especially joe average. However, if the installer \ndoes some checks on the development enviroment first it would satify a lot of \nKDE users out there as I'm sure most have compilers installed?. The other \nflaw is that it doesn't allow uninstalls to be performed which is I think an \nimportant feature, this could be handled by having the KDE_INSTALLED DB \nkeeping a list of files that \"make install\" installed and having the app \nupdate the DB if it updates a global config or adds files. This is  not \nreally an elegant solution and duplicates functions of other software (RPM \n/APT)\n\nI haven't tought the next bit through 100% but the idea is to have the first \ngeneration installer to get the source as above but to build RPMs and install \nthem using KDE's own branch under the RPM DB. This way the packages are able \nto uninstall via RPM, this can be expanded later to use the installer to \ncheck for updates to bin packages but bin packages create a new set of \nproblems like install paths etc. I personally like RPM but some distros don't \nso there are politcal issues I guess, I do remember some distro writting an \napt to RPM layer so that any packages could be used\n\nB) going back to the CVS stuff, keep the nonbeta part of CVS, might be better \nto call it unreleased and move it to its own branch (if it's not already). \nOnce a package in unreleased proves it stuff it can be moved to released and \na first release can be made. Because everything would be a seperate package \nit means that moving a package from nonbeta doesn't have to bloat up a \npackage group as is the current situation.\n\nI think the installer app should be fairly easy to do and I hope there is \nsomeone with the time to impliment it but getting CVS in the shape where this \nwill work might be a little hard?\n\nAnyway, the stuff above is just a few ideas that I can see from the outside, \nthere may be very many reasons why this can't be done, but as I'm not that \ninvolved with KDE, I of course don't know about them. If this is some use, \nplease feel free to forward it to the kde developers / mailing lists.\n\nRegards,\n\nDavid."
    author: "david"
  - subject: "Re: Can We Get Installer Script for KDE 2.1?"
    date: 2001-01-12
    body: "You might want to send your request to more than one core developer. I would suggest including David Faure on your list."
    author: "ne..."
  - subject: "Re: Can We Get Installer Script for KDE 2.1?"
    date: 2001-01-23
    body: "I'm new to linux but think an installer would be a great idea.\n\nI am an experienced windoze programmer and would be interested in writing an installer for the kde environment.\n\nWho would I contact to see if the KDE team would wish this to occur?\n\nWhat should it be written in? I was thinking C/C++ and the X libs as these should be installed on everyones computers.\n\nAlso for the downloading of files should a program already written to download files that will resume be used to save re-inventing the wheel?\ndoes wget do this?\n\ne-mail me the answers to this question if you would like an installer!!!"
    author: "Paul Hensgen"
  - subject: "Use Debian!"
    date: 2001-01-11
    body: "Use debian, apt and dselect instead of continiously mess around with RPM-based systems. Then you don't have to use such a windowish thing like an installer.\n\nPersonally, I think it's up to the distromakers to decide distro-specific things like how they want to install the software..., and that goes for all software in the distro, not just KDE (and Gnome).\n\nInstead of using the Helix installer for my Debian Potato system, I just point my /etc/apt/sources.list at the HelixCode server. And for KDE, just use kde.tdyc.com.\n\nWorks very well, much better than a RPM based system has ever worked for me!\n\nRegards\nMats"
    author: "Stentapp"
  - subject: "Re: Use Debian!"
    date: 2001-01-12
    body: "you can get a RPM-based and APT-enabled distribution at www.conectiva.com"
    author: "Evandro"
  - subject: "Use FreeBSD!"
    date: 2001-01-12
    body: "As my Debian colleague pointed out, the installer should be distro/os based. KDE runs on all versions of Linux, GNU/Linux, and BSD, as well as Solaris, AIX, HPUX..., and they all have different packaging systems.\n\nUnder FreeBSD, simply-->\ncd /usr/ports/x11/kde2\nmake install\n\nIf you don't want to actually *build* KDE (an eight hour procedure on my machine), then use sysinstall."
    author: "David Johnson"
  - subject: "Hebrew Probalms"
    date: 2001-01-11
    body: "when i install the i18n[he] of hebrew package\nthere is some probalms:\n1. in the login i see only question marks (???)\ni solve it by useing the contrl panel.\n2. in the headline of windows in Kde 2.0 i see question marks even when i change fonts\n3. in every program of kde 2.0 i can see hebrew characters(Iso-8859-8) it's all question marks\n\ncan the KDE them solve this probalms?"
    author: "Or Fialckove"
  - subject: "KDE is incomplete, still..."
    date: 2001-01-11
    body: "Any Desktop environment should be able to manipulate or configure the Whole system whether its linux or solaris or hpux, etc. KDE indeed has many utilities and applications which serves most of a desktop user's need. But still it lacks these. Some of these are built but not integrated as a KDE packaged application:\n\n0. KSoundRecorder\n\n    Strangely enough a KDE user can't record sounds and encode mp3! though there are many such KDE application scattered on the Net.\n\n1. Kfontinst. (Font installation)\n\n    Mandrake has drakfont which installs truetype fonts. Mandrake which is a big supporter of KDE uses a Gnome application for installing fonts! other distros don't even have such Graphical utility.\n\n2. ZZplayer ( A VCD player )\n\n3. KDVD ( a Dvd player )\n\n    The above ones are a must desktop environment's application. Still KDE only provides aKtion which uses xanim, again which limps to run an mpg file. It can't play a VCD or DVD!\n\nKDE in no way tries to go beyond applications and become a Responsible desktop environment by trying to configure every aspect of a Linux (or Unix) system.\n\n4. XKonfigurator ( XFree 86 Configurator )\n\n    RedHat and Mandrake has XConfigurator, SuSE has SAX. KDE can bridge this incompatibility by creating an X configuration system utility (as present in Corel Linux). I think you won't like extra work like Corel's!\n\n5. KHardware! (KDE's Hardware adding and removing tool)\n\nNetwork related stuff still not there is KDE!\n\n6. Tcp/Ip (or Network Configuration)\n\n    KDE has no utility or tool to configure networking!\n\n7. Samba.\n\n8. Apache.\n\n9. FTP (server/client)\n\n10. Anonymous FTP (we have kwuftpd)\n\n12. etc.\n\n\nIf KDE really want's to be the best desktop environment, it has to make life easy of its user and the Super User (Administrator's) life easy. My point is \"Why isn't KDE including the MOST FREQUENTLY REQUIRED applications and tools, and not creating which are missing and there since a long time in other Desktop Environments, like Windows, Apple MacOS. How about QNX and BeOS? Do you like the ugly looking black mouse pointers?\n\nPlease don't mind but KDE needs some thinking and absorbing Good Features from Other Desktop Envrionment! Thank you!\n\n\nYours truly,\n\nRizwaan."
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE is incomplete, still..."
    date: 2001-01-12
    body: "You forgot to mention that there also should be CD recording tool:)"
    author: "Franky"
  - subject: "Re: KDE is incomplete, still..."
    date: 2001-01-12
    body: "The NOATUN project includes some wishes. It's a jukebox for playing different sound formats (like mp3 and others), creating sound files from CD-ROM to a mp3 archive with looking on CDDB for titel information and burns audio tracks on CD-R. Perhaps is NOATUN included in KDE 2.1."
    author: "Stephan B\u00f6ni"
  - subject: "Re: KDE is incomplete, still..."
    date: 2001-01-12
    body: "I don't think most of these things should be a part of KDE. They belong to the operating system. Remember that KDE is a *desktop*, not an operating environment. Throw all your Windows and Mac concepts in the trash. Keep the components separate."
    author: "David Johnson"
  - subject: "Re: KDE is incomplete, still..."
    date: 2001-01-12
    body: "dont need to copy windows ! LEARN !!! (fauler sack)"
    author: "ch"
  - subject: "Re: KDE is incomplete, still..."
    date: 2001-01-12
    body: "<p>&gt; <i>My point is \"Why isn't KDE including the MOST FREQUENTLY REQUIRED applications and tools, and not creating which are missing ?\"</i><br>\nBecause KDE is a collective effort, made by benevols,not by people all working for the same company and payed to create particular application. People in the KDE communauty, as in any [Open Source|Free Software] communauty, work on what they found interesting, usefull, and in their skills.\n\nGNOME, however, has a steering board which can, for what I have understand of it, can direct the effort of coders. KDE totally reject such a thing.\n\nRemember that most people working on KDE apps are doing it for fun. If noone find a \"XKonf\" tool fun to do, noone will do it. And all you'll get will be a \"If you need it, do it\"."
    author: "GeZ"
  - subject: "Re: KDE is incomplete, still..."
    date: 2001-01-12
    body: "I still will not use it until it has \"Full Geometry Control\" as is the case with KDE1.1.2.\n\nIts sad to keep on about this but I feel until someone wakes up to that fact I can't get my customers to want to use it as it is!\n\n(-geometry x y ) is quite basic; dont you all think!\n\nRegards,\nJames"
    author: "James Alan Brown"
  - subject: "Re: KDE is incomplete, still..."
    date: 2001-01-15
    body: "I completely agree with what you have said. In my eyes, BeOS is the most visually well thought out desktop I have come across. The GUI developers for KDE c(sh)ould take their pointers from other OS's as far as creating utilities instead of relying on copying windows, for a start. Then, completing the missing applications is essential. I have one rant about Linux in general to make here: BeOS installed on my system from inside Windows in under 10 minutes, my system rebooted and I was faced with a completely installed system, right down to my internal 56k modem; The QNX demo disk (OS on a stiffy) picked up my modem and I was able to surf the web!! Linux has NEVER been able to pick up my modem. That is shocking. Now that I have that out of my system ... :)\n\nCheers for now"
    author: "Kde-Needs-Work"
  - subject: "CSS Issues??"
    date: 2001-01-12
    body: "I love the 2.1 releases that I've been running from Woody, it's as dramatic an improvement as teh move from 1.0 to 1.1x.  Polish, simply fantastic.\n\nHaving said all of that, I'd like to point out that the CSS remdering has become somewhat broken between 2.01 and 2.1.\n\nI have a nicely formatted page at:\n\nhttp://www.moses.cx/school/214/214_Project/\n\nit renedered perfectly in both Konqueror 2.0 and 2.01 (as well as IE, Netscape 4.x, 6.x and mozilla) but hasn't worked in any of the kde2.1 builds.\n\nCan anyone suggest what might be the cause of this??\n\nCheers,\n\nBen"
    author: "Ben Hall"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-13
    body: "GO FURTHER!!!!!!!!!"
    author: "Manuel Rom\u00e1n"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-13
    body: "KDE shouldn't configure every aspect of the OS, its not KDE's job.\n\nIt is the job of the end user to configure the machine, tools ship with each and every Unix and Linux OS to configure it.\n\nIf you tried to let it configure each and every aspect of each unix os it compiles on it would become so bloated it would be unusable.\n\nIf you want a nice gui configuration for your os try windows, that is what it is for."
    author: "Jason"
  - subject: "Re: KDE 2.1 Release Schedule"
    date: 2001-01-14
    body: "Why should KDE not configure every aspect?\n\nI would have thought that the more that KDE2 can do the better and in turn more fed up Windows95/98/2000/NT users would want to use Linux!\n\nIt still seems to me that at the moment with KDE2: it is a little more important to copy/try to out do Gnome rather than to create a real alternative Desktop for Linux With the great name of KDE. \n\nKDE1.1.2 with full Geometry control and with some very tidy well working programs has already got very close to that. \n\nI am hoping that the KDE team will sort out the Geometry flag and become the best desktop alternative. Your have a very long way to go to bloat KDE2 like Microsoft have done with Windows 2000!\n\nCertainly no one with half a brain using Linux wants to go back over to Microsoft's expensive (no source code OS) and silly long folder names like \"Microsofts Temporary Internet Folder Grooving in a Cave With a Pict\" do they?\n\n\nRegards James"
    author: "James Alan Brown"
  - subject: "Re: Why should KDE not configure every aspect?"
    date: 2001-01-14
    body: "The problem, as I see it, is distributions.  You've got Mandrake, Red Hat, Debian, Slackware, Caldera, and so on and so forth.  Each one of these distros is configured in a different, proprietary way, with configuration scripts and files scattered all over.  To write one tool that could handle all the formats of all the configuration files of all the distributions out there, and still have it be small and easy to use, is flat-out impossible.\n\nAnd then, what if someone makes a new distro?  Or changes an old one?  Imagine if every time Red Hat came out with a new version, KDE developers would have to scramble to update their configuration tool to handle all the changes.  That just wouldn't work.  \n\nWhat I think needs to be done is there needs to be a KDE-centered distro that includes only KDE and a few other apps, and has configuration tools that integrate with KDE (inside the Control Center).  Of course, I'm not going to be the one to start such a project, at least not until I get a LOT more programming experience.  Well, we can always hope :-)"
    author: "not me"
  - subject: "Re: Multi-head support"
    date: 2001-01-17
    body: "Will there be multi-head support in the final 2.1?\n\nRight now (beta1), the situation is far from satisfactory. There doesn't seem to be a way to configure KDE for multiple physical screens and the documentation doesn't mention anything either."
    author: "Knut"
  - subject: "Re: Multi-head support"
    date: 2003-03-04
    body: "Hi ,\n   I need to configure Multiple monitors on a linux system.\nSystem configuration is\ncpu -p3\nram 256 MB\ncard1 S3 PCI\ncard2 S3  PCI\ncard3 cirrus pci\ncard4 nvidia agp\nOS RedHat linux 8\n    Please kindly let me know how to go about with the configuration\nThanks\nSudhir"
    author: "Sudhir mahale"
---
<A  HREF="mailto:faure at kde.org">David Faure</A>, the KDE release coordinator, has <A HREF="http://www.kde.com/maillists/show.php?li=kde-core-devel&f=1&l=20&sb=mid&o=d&v=list&mq=-1&m=376037">posted</A> the KDE 2.1 release schedule.  The summary:  KDE 2.1beta2 is scheduled for release on January 29, and KDE 2.1 is scheduled for release on February 12, 2001.  Read the post below.


<!--break-->
<BR>
<PRE>
<STRONG>From</STRONG>: David Faure &lt;david@mandrakesoft.com&gt;
<STRONG>To</STRONG>: kde-core-devel@kde.org, kde-devel@kde.org, kde-i18n-doc@kde.org
<STRONG>Subject</STRONG>: Release schedule for KDE 2.1 beta 2 and KDE 2.1 final
<STRONG>Date</STRONG>: Wed, 10 Jan 2001 13:00:15 +0000
<STRONG>Sender</STRONG>: kde-devel-admin@max.tat.physik.uni-tuebingen.de
 
Here is a proposal for the release schedule for the next two releases.
 
This schedule gives the 3 weeks of message freeze that the translators asked for,
and gives us a feature freeze of 3 weeks too - hopefully enough time to close
everything on http://bugs.kde.org :-))
 
An alternative schedule would be to add one week between the two releases,
i.e. delay 2.1 final by one week. I suggest that we don't plan to do that, but we
keep this option at hand in case beta 2 turns out to have too many bugs.
 
Comments welcome.
 
KDE 2.1 Beta 2
=============
Friday 12/01/2001
The HEAD branch is frozen again : no new features, bugfixes
commits only, no commit that modifies an application's behaviour,
and **message freeze**.
 
Monday 22/01
The HEAD branch of CVS is tagged KDE_2_1_BETA2, and tarballs are made.
They are made public immediately, and given to packagers for creation
of binary packages.
 
Friday 26/01
The binary packages are uploaded to ftp.kde.org to give some time for the mirrors
to get them.
 
Monday 29/01
Announce KDE 2.1 Beta 2
 
KDE 2.1 final
=============
The HEAD branch remains frozen since Friday 12/01. Only bugfixing,
there is enough of that to do :)
 
Monday 05/02
Last day for translations, documentation, icons and critical bugfixes
commits.
 
Tuesday 06/02
The HEAD branch of CVS is tagged KDE_2_1_RELEASE, and tarballs are released
to the packagers.
 
The rest of the week is spent testing the tarballs, packaging and writing the
announcement and changelog.
 
Friday 09/02
The source and binary packages are uploaded to ftp.kde.org to give some time
for the mirrors to get them.
 
Monday 12/02
Announce KDE 2.1.
 
--
David FAURE, david@mandrakesoft.com, faure@kde.org
http://www.mandrakesoft.com/~david/, http://www.konqueror.org/
KDE, Making The Future of Computing Available Today
</PRE>

