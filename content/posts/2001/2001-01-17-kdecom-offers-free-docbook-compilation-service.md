---
title: "KDE.com Offers Free Docbook Compilation Service"
date:    2001-01-17
authors:
  - "Dre"
slug:    kdecom-offers-free-docbook-compilation-service
comments:
  - subject: "Re: KDE.com Offers Free Docbook Compilation Servic"
    date: 2001-01-17
    body: "Ahhhh...  not the pain of creating Yet Another Website Account.  ;)\n\nHopefully \"Pain Of Creating Account\" << \"Pain Of Installing Working DocBook\""
    author: "ac"
  - subject: "Re: KDE.com Offers Free Docbook Compilation Service"
    date: 2001-01-18
    body: "<P>\nYes, the pain is far less.\n<P></P>\nThought about not requiring an account, but it takes quite a bit of CPU power to compile .docbooks -- jade appears to be quite inefficient (a 60K docbook file can take up to 15 seconds of actual CPU usage on a PII-550 to compile).</P>"
    author: "Dre"
  - subject: "Great idea !"
    date: 2001-01-18
    body: "Wow, amazing idea. Andreas, you amaze me every day. \nThanks for the service (it's worth to note that I use a machine of Andreas to generate the documentation for the official tarballs, too !)."
    author: "David Faure"
  - subject: "Re: KDE.com Offers Free Docbook Compilation Service"
    date: 2001-01-18
    body: "DocBook can be a pain in the ass to install and set up because there are different pieces of software, and different ``languages'' and concepts, but in the end it is probably beneficial to developers to understand how it works.\n<BR><BR>\nA note to those who do want to try to do their own docbook compiling: don't use the cvs version of openjade, I don't think it's been touched in months, and I couldn't for the life of me get it working."
    author: "Caleb Land"
  - subject: "Re: KDE.com Offers Free Docbook Compilation Service"
    date: 2001-01-19
    body: "AT LAST !!!"
    author: "anonymous coward"
  - subject: "Re: KDE.com Offers Free Docbook Compilation Service"
    date: 2001-01-21
    body: "Just an idea: What about compiling the docs of the CVS tree every night and making a link to download the whole doc-package?\n\nWhile the Code allmost ever compiles, the docs just won't."
    author: "Docs of current CVS"
---
<A HREF="http://www.kde.com/">KDE.com</A> has announced it will provide a convenient
service to all KDE developers who, for whatever reason, have not been able
to compile KDE documentation properly or who do not want to go through the
hassle of installing all of the necessary packages to compile KDE .dockbook
files.  Simply visit the KDE.com
<A HREF="http://www.kde.com/devel/docbook.php">DocBook Documentation
Generator</A>, create an account (if you don't already have one for KDE.com or <A HREF="http://apps.kde.com/">APPS.KDE.com</A>), upload your
index.docbook file (requires a browser such as Konqueror supporting the
'file' input method) and in a few minutes the generated HTML will be
available for download.  Enjoy!