---
title: "First review of KDE 2.2"
date:    2001-08-17
authors:
  - "mmoeller-herrmann"
slug:    first-review-kde-22
comments:
  - subject: "I say KDE has surpassed Windows."
    date: 2001-08-17
    body: "It is so good that every time I use Windows I marvel at how primitive and limited it feels compared to KDE."
    author: "ac"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-17
    body: "Man, ac!  You are so full of yourself it is pathetic!  I hope that KDE does get as good as Windows, but I don't see that with 2.2."
    author: "Jiffy Jones"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-17
    body: "Try installing the Fast-Liquid theme from Mosfet's site at http://www.mosfet.org/liquid.html ! I was stunned how good my desktop looks after installing it!! This should be the default theme in KDE 3! It's fast, slick and big and small at the same time! It is much slicker than the REAL MacOS-X Aqua!"
    author: "Per Wigren"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-17
    body: "I made an RPM for it at <a href=\"http://www.opennms.org/~ben/kde2.2/\">http://www.opennms.org/~ben/kde2.2/</a> if you want to try it out.  That whole directory is built for RedHat 7.1 (since the official RPMs have issues on 7.1), but I put the source RPM for liquid up there too.  Have fun...\n"
    author: "Ranger Rick"
  - subject: "Filename?"
    date: 2001-08-17
    body: "Is the Fast-Liquid theme from Mosfet's site == kdeliquid?"
    author: "tweet"
  - subject: "Re: Filename?"
    date: 2001-08-18
    body: "yup"
    author: "Ranger Rick"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-17
    body: "Liquid can't be part of KDE. Look at Liquid license."
    author: "Hasso Tepper"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-18
    body: "yup, fork out the last BSD-license code and develope it with or without mosfet. why did he take away pixie anyway? just because a few ppl disagree with him? (not, i use few ppl, i'm sure the rest might have supported him)"
    author: "Rajan Rishyakaran"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-18
    body: "This type of attitude is exactly why I moved my code to the QPL. People fork my projects just for the hell of it, and don't add anything of value. Yeah, take version 0.1, with a ton of bugs that have been fixed and a ton of features missing, and add a fork to KDE CVS just because I want to develop it independently. Good idea, there :P It's not like people who are forking my code actually added anything significant, the KDE CVS forks are much more buggy than my recent independent work. Stupid. This is why I switched to the QPL. I want people to use the most stable and bug-free versions of my software, not buggy versions KDE includes just to be in a pissing contest with me because I decided not to develop in CVS.\n\nAnd if you had a clue, you'd know I originally tried to get it into KDE CVS (and even offered to have it disabled if it wasn't ready for KDE2.2). The offer was rejected, even tho I maintained all the style code.\n\nAnd the last place you lack a clue is I was asked to remove Pixie from CVS by the KDE release maintainer after deciding I was going to code styles outside of CVS. I originally was going to leave it in."
    author: "Mosfet"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "Mosfet, I love what you have done for KDE, but your positions really seem childish and uncooperative.  The 'cluelessness' and bureacracy you complain about are standard in a project of KDE's size, but you stay in there and work things out.  You don't take you dice and withdraw into a shell, that just forces other people to fork your code."
    author: "Chris Bordeman"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "Your welcome to your opinion, that doesn't really matter to me. But I haven't \"taken my dice and withdrawn in a shell\". As a matter of fact, the software I write and have been releasing has been getting even better. I simply want to be able to maintain and write my code as I see fit. As I said over and over, I don't do development via \"bureacracy\" anymore. This doesn't seem childish to me at all, and has greatly improved my productivity. I do this for fun, not for headaches. After arguing with bureacracy for a year, I've had enough. If the \"bureacracy and cluelessness\" I've had to deal with are standard in a project of KDE's size when dealing with code pertaining to UI issues, then I want nothing to do with it. Call me names if you want, but I'm the one doing the coding."
    author: "Mosfet"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "Great for you, but who i going to use your crap now that it's not even part of KDE?"
    author: "noone"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "Judging from the downloads, emails, and comments, quite a few people are using my \"crap\" ;-)"
    author: "Mosfet"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-22
    body: "Be careful with words like that, even if you don't like the software. BTW a lot of people like it."
    author: "AB"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "I really didn't mean to call you names, Mosfet, I think you are one heck of a programmer and I admire your work and I apologize for the condescending tone.  I just wish you had a higher threshold for these committee decisions.  There are many core developers I'm sure that had similar frustrations, but you seem to think either your situation was special or you're just too smart for those other opinions.  \n\nI am glad your code is moving along so well now, but now considerably fewer people will have the pleasure of using your additions.  Ah well I guess."
    author: "Chris Bordeman"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "Okay, this is my last post on this topic. If people don't like it or agree it doesn't affect my life one way or another :P\n\nUser interface code definitely is a different situation than most other programming. It's always a more controversial topic, with many different opinions and everyone feels their opinion is right (I know I do! ;-). Doing such work by committee is virtually impossible, or you end up with crap. You should of seen the fighting over the default styles I wrote... this had been going on privately for a year. What I was in was a situation where people who never contributed telling me what I can and cannot do, how I should make things look, etc... You don't see this in many other fields of KDE development. Part of it is because I'm so blunt and feel strongly about many issues, but a larger part is UI work doesn't lend itself to committee design. IMO it requires someone with a specific vision willing to implement it. This is what I will do now, without the fighting. I'm of course open to opinions, but I make the final decision about UI code I write (if you don't like it come up with something original yourself).\n\nAs for fewer people having access to my code, that's nonsense. Remember, it was the \"committee\", who said I couldn't include Liquid in CVS (even if I disabled it by KDE2.2 if it wasn't ready). I wanted to include it, but was outvoted by people concerned about having a Liquid style UI in CVS, so would of had to make it a separate package anyways.\n\nReally doesn't matter either way. If users like the code they'll tell their distributors to package it, and they'll include it along with their other KDE related stuff (at least this is the way it should be). There are plenty of applications and developers doing lots of things outside KDE CVS... I'm still getting bug reports and suggestions, and that's all I really care about."
    author: "Mosfet"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-22
    body: "For a couple of years now I have been seeing great things being added to KDE. And I have also seen KDE get bigger and bigger as a result. It's now time for KDE to limit what is the \"core\" and spin off the rest into sibling projects.\n\nI want my desktop to be just a desktop. I don't want it to be a development environment, office suite, image processing shell, or anything of the sort. KOffice is a separate project and that is a Good Thing(tm). Keeping the KDE components separated into packages is the way to go. It shouldn't be just kdelibs and kdebase, but not every program written for KDE should be included with KDE. Mosfet's widget themes are simply superb. But let's limit kdelibs to just a few themes, with a few more in kdeaddons/kdeartwork, but keep the rest as separate projects."
    author: "David Johnson"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-18
    body: "Dude, get realistic.  Fast Liquid has a nice look\nbut there's no way it's half as attractive as\nMac Aqua.  Don't get me wrong, I wish it did\ncompare favorably, but the gradients are grainy,\nthe color scheme is inconsistent and the widget\ngraphics don't have the same quality."
    author: "Freddy Fred Fred"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-18
    body: "But I like it being more squary than the (IMHO) too-round Aqua! :)"
    author: "Per Wigren"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-18
    body: "I'd say it's least half as attractive ;-) Some widgets look different, but this is supposed to be a fast and efficent style usable by people like me who are also compiling all day, etc... You have to balance features vs. performance. People are even reporting success using it on remote X terminals and are happy about the speed.\n\nLiquid doesn't use gradients, so I don't know what you mean there...\n\nI just released version 0.5 as well, which further increases the quality without sacrificing performance."
    author: "Mosfet"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-19
    body: "Hmm,\n\nok, I got it with \nwget http://www.mosfet.org/mosfet-liquid0.5.tar.gz\nBut it's nowhere announced on your site yet :-)\nAnd please bring it back to the main KDE distribution. It's just too good to be not part of KDE."
    author: "Thomas"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-19
    body: "a) It has been announced on Freshmeat and will be announced on apps.kde.com, b) I don't do work in KDE CVS and even had my account removed because I wanted to code my styles outside the core distribution, and c) even when I did do CVS development I was not allowed to put Liquid in CVS (see my earlier post). Not for any technical reasons, mind you, but because some people outvoted me even tho I supposedly maintained the widget styles. Stupid reason, but with a lot of the style stuff I was forced to do things I didn't want because people who never even contributed voted on it. I'll never develop in KDE CVS again, I don't do design by committee."
    author: "Mosfet"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-19
    body: "a) the reasons were largely legal and timing concerns. it wasn't a conspiracy against mosfet or anything, just a few people who were worried about the legal liability of a theme that looked so much like Aqua in the screenshots he provided and the remaining limited time to 2.2.\n\nb) there are developers who can not or will not work with larger teams of people (i know several people like this personally) and that is cool if not occasionally unfortunate. what would be nice is if the more important contributions made by such people could be easily made available to the wider numbers of the kde user base.\n\nc) i don't have a c point. i wish i did though."
    author: "Aaron J. Seigo"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-19
    body: "As for a) Liquid doesn't look like Aqua, none of the graphics are the same and it doesn't even operate like Aqua. Thus there are no legal issues (as is being proved every day now). Usually I'd ignore such cluelessness, but since everything was developed by committee I couldn't do that. *This* is why there is no Liquid in CVS. This also partially addresses b). I also had people telling me if I could or could not include testing styles (something I need), setting maximum limits on the amount of code I could include, etc... Totally not acceptable. The maintainer makes such choices, not people who never contributed. All these put together is what made me decide it's better to develop solo. Since making that decision, I've been a hell of a lot more productive since I don't have to fight tooth and nail with other people in order to do anything ;-)\n\nAs or b) I don't really care what your opinion of me is... or if you feel I can or cannot work with the KDE team. I spent well over a year defending KDE and helping it any way I could, but we have different visions of what KDE should be. Most people get along fine, but when your doing code that concerns UI issues people are much more likely to flame, think their opinion is right, and just generally argue. I'm was sick of it. I'm much happier now :)"
    author: "Mosfet"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-26
    body: "> I'd say it's least half as attractive ;-) \n\nAt one point you state:\n\n> Some widgets look different, \n\n.. and at another point of time you state:\n\n> Liquid doesn't look like Aqua, none of the graphics are the same\n\nSo you admit that some widgets do look like Aqua. This was exactly what some people were concerned about."
    author: "Anonymous"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-27
    body: "WTF are you talking about? I never said any of the widgets were copies of Aqua. *None* of the graphics were stolen from MacOS. Since it aims to provide the same style of UI ie: one that looks like a liquid with rounded widgets, of course some are similiar. But then again, Motif and Windows have a lot of similarities, both have rectangular buttons. This means nothing. I don't very well think Apple can copyright how water looks. \n\nIf you actually used Liquid or looked at it's graphics, you'd know this (everyone who uses it states it looks or operates nothing like Apple's), but evidently you haven't, like most people making such comments.\n\nEither way it doesn't matter. Just one less thing KDE packages won't get, but of course I'll make sure the users can."
    author: "Mosfet"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-18
    body: "Hy, can someone help me. I cant get liquid running. When i type make -f Makefile.csv i get an errormessage:\n--------------------\n./aclocal.m4:2695: error: m4_defn: undefined macro: _m4_divert_diversion acfunctions.m4:1108: AM_FUNC_OBSTACK is expanded from... ./aclocal.m4:2695: the top level autoconf: tracing failed make[1]: *** [cvs] Error 1 make: *** [all] Error 2\n--------------------\n\nI run Suse 7.0 with KDE2.2\n\nthanks"
    author: "steve"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-22
    body: "You may need to downgrade your version of autoconf.  I think you'll find that you can't compile any KDE projects, not just Liquid.  I've heard from a few SuSE users of my KDE project with similar error messages.  They all had autoconf 2.5.  Downgrading to ac2.13 or so clears up the problems.\n\nI think the problem is that ac2.5 isn't backward-compatible with the autoconf scripts in most KDE projects.\n\nhope that helps,\nJason"
    author: "LMCBoy"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-27
    body: "I'm also having a problem installing the theme... Does it work with 2.2-release or just 2.2-beta? I don't think it's autoconf, since I only have version 2.13_1.\nAnyway here's what happens:\n\nbash-2.05$ make -f Makefile.cvs\nThis Makefile is only for the CVS repository\nThis will be deleted before making the distribution\n \n*** Concatenating configure tests into acinclude.m4\nmake: don't know how to make acinclude.m4. Stop\n*** Error code 2\n \nStop in /home/erin/themes/kde/mosfet-liquid0.5.\n*** Error code 1\n \nStop in /home/erin/themes/kde/mosfet-liquid0.5.\n\nI probably won't get any responses, but I figured I'd try anyway."
    author: "Genghis_Conn"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-17
    body: "I guess it's a matter of opinion, but I have to agree with ac that KDE is much nicer to work with than Windows' UI. Of course, KDE is a desktop environment whereas Windows is an operating system, so it's possible to believe KDE is better than the Windows equivalent but that Windows systems are better than systems running KDE (I don't, but it's possible). \n\nAs much as I hate trying to install software on *nix (yes, I've tried Debian;) or upgrade the systems or make my distro work with my unusual hardware or watch movies or play games etc., I can't switch back to Windows because I can't stand to work without KDE! I've used quite a few UIs and I've never grown as attached to one as I have to KDE.\n\nThanks KDE team!"
    author: "kdeFan"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-20
    body: "I don't know how many people have the misconception that Windows is an OS, but The only\nWindows that are an OS are ones derived from WinNT. (NT, 2K, XP?) The other 'Windows's are just Desktop environments like KDE or Midnight Commander for DOS :) They Just sit on top of DOS which is the OS."
    author: "Tom Fjellstrom"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "I don't know how many people have the misconception that Windows is not an OS and sits on top of DOS... are you drawing your conclusion from the fact that you can type \"win\" on the DOS commandline to bring up Windows ? So a Linux started with loadlin also sits on top of DOS, eeh ? And DOS can't be so bad, because it supports preemtive multitasking from windows 95 (it have to, if it is the underlying OS...). the fact, that DOS is still there does not mean it is used for anything else than an osloader."
    author: "cylab"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "With windows you still can run dos programs right? but you also get most of dos's limitations like the 8.3 filenames, and insuficient memory protection. Your point about multitasking doesnt' make much sence considering its the hardware that 'supports' it, not DOS.\n Loadlin on the otherhand overwrites dos with the linux kernel."
    author: "Tom Fjellstrom"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "sure you can still run dos-programs in windows... in a dos-box. you can run dos-programs under linux to in a dos-box called dosemu. the dos-api-calls are indeed supported by windows, but that does not lead to the conclusion, that dos is the underlying os of windows (i dont talk about win311 of course). if you dont use the dos-api calls you are in a (mostly) pure windows (there are exceptions in win95 like formatting of disks) environment with no dos at all. the 8.3 filename problem is only true for 16bit win-apps which are the same as in win311 so not counting in this discussion. and there is no such thing like hardware multitasking. you have memory management units and context-switching opcodes in some cpus to make implementation of multitasking easier and faster, but you cant ask a cpu to \"run windows-programm Excel with priority normal\". thats clearly an os issue. to end this discussion: yes, dos is still there and can be used with all disadvantages, but you dont have to use it (most of the time... there are exceptions as i said). to compare dos as the win9x-kernel with a linux-kernel and the windows-shell with the kde-desktop does not reflect the facts. and some more: not everything microsoft does is bad. they have done an incredibly good work in some areas, dont judge this company only by their marketing behaviour..."
    author: "cylab"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "First things first, I have never actually said Microsoft Sucks, In fact they have made some fairly nice software but I wouldn't count Windows 3.i or Win9x into that category. Sure its an dosemu is an emulator but Win9x neven included one, the closest thing in win9x that I've see to a dos emulator is the crappy DPMI server that it forces you to use. And where did you get the Idea that without hardware supoort that TRUE multitasking is even possible... when was the last time you saw a OS for a chip like the 286 that could multitask? YOU NEED to have those 'extra' opcodes, without them all your apps would share one memory space, one stack, etc...."
    author: "Tom Fjellstrom"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-22
    body: ">And where did you get the Idea that without\n>hardware supoort that TRUE multitasking is even\n>possible...\n\nthat was not my statement. i refered to the following statement in a earlier post:\n\n>Your point about multitasking doesnt' make much\n>sence considering its the hardware\n>that 'supports' it, not DOS.\n\nwhich was an answer to my ironic argument, that DOS seems to support multitasking, if it is the kernel of the windows shell, because windows does support multitasking.\n\n>Sure its an dosemu is an emulator but Win9x\n>neven included one, the closest thing in win9x\n>that I've see to a dos emulator is the crappy\n>DPMI server that it forces you to use.\n\ni dont really know what you mean here. as far as i know DPMI is a memory managemant api like VRMI (or similar ;) that allows you to code dos-programs for the 32-bit mode instead of the native 16-bit environment and the DPMI-Server is a protected mode framework that provides the needed functionality. windows only provides a DPMI-server to allow this dos-programs to be executed in win9x, so i dont see the correlation to dosemu (despite the fact, that dosemu also provides a DPMI-server)\n\ni noted dosemu only to show that running dosprograms in a dosbox is no evidence for an os that sits on top of dos.\n\nbut back to _true_ multitasking: it isnt possible at all with a single machine as you surely know, so lets talk about _faked_ preemptive multitasking. you can of course implement multitasking on a 286 cpu or on a 68000 and if you want to even on a Z80. if you lack an MMU you might (possibly will) run into trouble with unwanted memory-access, but that does not prevent you from implementing multitasking. the registers of a cpu (the stack-pointer mostly is one of them) and process relevant information like filepointers can be saved manually, so you need no extra opcodes for context switching.\n\nafter all iam tired of this thread and everyone else probably too. but i have to have the last word =P\n\nseveral other _last_ words: i personally count win9x as one of the great pieces of software by microsoft. it might be unstable, but it changed a lot in my world and its gui had a great impact on every other gui for any other operating-system, kde included. yes, i know finder was before and others(xerox?) were first, but win9x had the greatest impact in my opinion."
    author: "cylab"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-22
    body: "win9x a great piece of software? nah. not even for Microsoft. WinNT/2k on the other hand is."
    author: "Tom Fjellstrom"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-22
    body: "looks like you have to have the last word, too =)"
    author: "cylab"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-22
    body: "Of course not. what do you take me for? ;)"
    author: "Tom Fjellstrom"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-22
    body: "I do think that Win95 changed the world of personal computers. People like my mother surely appreciated the changes M$ made to the face of the computer. W9x surely _is_ crap, but quality isn't as important as the sheer amount of installations in this context - M$ managed to introduce personal computers to a market where no Unix derivative would have had any chance of survival.\n\nDo you need a perfect OS architecture for playing solitaire?\n\n\n---\nVisit http://www.bluephod.net if you're interested in a german newssite."
    author: "Zaphod"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "I'm far from a Windows-lover, but I feel I have to respond to this:\n\n - You can run DOS programs under Linux as well, through DOSemu.  In fact, DOSemu under Linux will even run some DOS programs that won't run in a DOS box under Windows 95, 98, or ME.\n\n - Windows/DOS hasn't limited users to 8.3 filenames for a while now.  DOS still uses 8.3 filenames internally, but it has ways to show and allow use of the long names in commands.\n\nPersonally, I'd consider Windows to be an extension of DOS.  You still have the option of running \"just DOS\", but Windows adds/extends many things that are generally considered to be OS services, such as process management, memory management, and device management.  Win9x does not call DOS for most memory management and filesystem tasks -- it uses its own services for those, and provides them to DOS boxes running under it.\n\nInteresting note -- boot a Win95 boot disk up in \"DOS mode\", and you can't work with long filenames.  You can quote filenames to put in spaces and such, but anything beyond 8.3 just gets ignored.  It's Win95 that provides the long filename services, and it goes around DOS to do so.  A DOS box running under Win95 can work with long filenames, though -- because it's calling Win95 to do the actual work, rather than doing it directly.\n\nFor some more information, see:\n\nhttp://www.cuenet.com/archive/wordstar/1997/97-08/msg00284.html\n\nFor a *lot* more information, read Andrew Schulman's \"Undocumented DOS\" and \"Undocumented Windows\".\n\nPerhaps the best way to view it would be to say that Windows is not a complete OS, but is an OS extension.  Thus, DOS+Windows is a different OS than just DOS, providing different services."
    author: "Travis Casey"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "This is by far the most inteligent post so far :)\nBut you still feel the need to bring up dosemu? Why? It has absolutly nothing to do with the fact that Win9x literally sits ontop of dos and now and then has a few hacks to get around some of dos's shortcomings. Also DOS programs don't have any real access to windows services, accessing window's longfilname API through dos is a headache have you ever tried? Running in a DOS box while in windows is a bit different because windows replaced some of the DOS INT codes, so yes I'd have to agree that Win9x is more of an extention to the OS its still just a really complicated shell. (Bash can run its own kind of program too ;)"
    author: "Tom Fjellstrom"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "actually i tought windows uses the same technique for running dos programs as linux does with dosemu: vm86. it only hacks around having to restart dos every time by starting (a part of) dos at boottime, and keeping that 'image' around. \ncorrect me if i'm wrong"
    author: "ik"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-21
    body: "I think thats a part of it. But Windows 9x NEEDS dos.\nSo when it keeps dos arround its not doing it just to speed up the startup of dos programs..."
    author: "Tom Fjellstrom"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2007-03-12
    body: "Well, plain and simple, fruitless arguments over past procedures never did anyone any good. So I think we need to look past our egos here and examine this in a logical and _unexcited_ manor."
    author: "Zach Meyer"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-17
    body: "Jiffy, I think you missunderstood what ac said. He/She clearly states that in their experience doze is the one with the problem."
    author: "tonyl"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-17
    body: "I say not yet.\nI just used win2k a little on my machine (that happens from time to time...), and I must\nsay that there is still one major problem with\nkde2.2.\nIt doesn't feel so snappy (or is it snippy?) as win2k.\nWhn I compare Opera win/Opera Linux and \nIE5.5/Konqueror:\nOpera win renders a little faster than opera linux\nKonq renders ok, but when you switch from one window to another, or switch between desktops, it feels heavier, there is more cpu load compared to similar action in win.\n\nWell, despite alll this, I'm a convert since KDE2.0 and I'm, looking forward to the future of KDE.\n\nyves"
    author: "Yves Glodt"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-18
    body: "> I just used win2k a little on my machine (that happens from time to time...), and I must\n say that there is still one major problem with\n kde2.2.  It doesn't feel so snappy (or is it snippy?) as win2k.\n\nYes, but it is only a part of the comparison. Globally KDE has surpassed Windows, I agree. In terms of functionnalities :\n- Konqueror is better than explorer/IE\n- KNode/Kmail is better than Outlook Express\n- Kicker is better than the windows taskbars\n- KDE games are better than Windows games\n- Virtual Destops don't exist in Windows\n- Korganizer don't exist in Windows, and so, and so... (KMid, Kate...)\n\nWindows still has some advantages :\n- it is quicker (snappy, yes)\n- it has easier management of start menu\n- all functionnalities are ready to run (no need to search for having Flash or for displaying the local network...)\n- it is better translated\n\nAnd KDE has big advantages in terms of price and philosophy (technical philosophy with many choices, parameters...)\n\nAnd I don't speak about KOffice...\n\nBut Microsoft has a big advantage in terms of marketing and base of users..."
    author: "Alain"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-18
    body: "I agree with what you say.\n\nI think that there is one point which could be easily improved: the ready-to-run point (an important one IMHO).\n\nI upgraded to KDE2.2 and I like it very much, still when I want to view a website with Flash, RealAudio or things like that, I'm using Mozilla not Konqueror because I could find the instruction easily to install these component on Mozilla not on Konqueror.\n\nI found nothing in the FAQ, HOW-TO of KDE/Konqueror, if it's there it is well hidden.."
    author: "renoX"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-18
    body: "Hey,\n\nI'm new in Linux and in English. Hehe...\nand sorry my poor vocabulary.\n\nWell, I didn't download KDE 2.2 'coz I\nthink I wouldn't know how to install it.\nAnd 'coz I have a dial up Internet\nconnection. I'd take forever to get\nhere... hehe..\n\nSo I'd like to ask for you a screenshot of\nthe new KDE. I'd like to see how it looks\nlike. If you don't mind. Or anybody else\nthat reads that could send me some screen\nshots too. I'd thank you so much. My e-mail\naddress is 'cartmanep@bol.com.br'\n\nWell, thank you.\nFelipe"
    author: "Felipe"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-18
    body: "You want some screenshots sent to you and you have a slow internet connexion??\n\nBe cautious if too many people send you, your connexion may cry mercy.\n\nWhy don't you look here:\nhttp://static.kdenews.org/content/kde2.2/"
    author: "renoX"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-19
    body: "Here are very nice screenshots of the now famous fast liquid theme :)\n\nhttp://www.mosfet.org/liquid.html"
    author: "khalid"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-19
    body: ">- it is better translated\n\nPlease could you explain this. Is better translated in what language ?\nWhat are the KDE translation problems ?\nDid you report any i18n bug to bugs.kde.org ?"
    author: "Andrea"
  - subject: "Translation, documentation"
    date: 2001-08-19
    body: ">  Please could you explain this. Is better translated in what language ?\n\nMany things are not plainly translated in french. One example, here in Konqueror ? The CSS configuration window is in english.\n\nOf course, there a big work of translation, of course it is in the last functionnalities that most of things are not translated and I am optimistic that it will come. But never in the next french Windows XP you will see a not translated window. So it is a little real advantage of Windows.\n\n> Did you report any i18n bug to bugs.kde.org ?\n\nNo, because I think it is some lateness for improving new things. I understand (even I find normal) such a lateness, but I do not hide it...\n\nI did not spoke of the documentation. Here I feel irritated by the latenesses or the lacks. also about updated functionnalities. \n\nExample : KMid don't work (although KMidi works). Why ? It seems I have some Soundfont to find ? \nWhere I load a \"soundfont\" ? \nI think it Is it to put as a midi mapper on the page \"Sound - midi\" of KDE Panel Control. In the Help page, it is said that it is very easy, you only have to select a mapper  (\"That is all there is to it !\", Cf. http://www.kde.gr.jp/help/doc/kdebase/doc/kcontrol/HTML/midi.html). But in the selected directory, $HOME, there is no such files !! \n\nHere, I may post a bug (and if somebody knows how to fix such a thing... In the Mdk 8.0 control Center, my soundcard is seen as a sb ESS1869)\n\nOn the other side, I feel that the Windows documentation is bad, not better than the KDE one, although better on last improvements... So I think it is not a significant difference..."
    author: "Alain"
  - subject: "Re: Translation, documentation"
    date: 2001-08-19
    body: ">Of course, there a big work of translation, of >course it is in the last functionnalities that >most of things are not translated and I am >optimistic that it will come. But never in the >next french Windows XP you will see a not >translated window. So it is a little real >advantage of Windows.\nAs you know we provide the version 2.x.1 of KDE to complete translation. You may also know that windows has different release dates for different country so here in italy you cannot find english and italian version the same day.\nSame thing with KDE, the i18n complete version come one month later.\n\nAnyway, if windows has more translated string (you say all the interface and all the documentation) that does not mean the translations are *better*, better means higer quality and I think our translation, at least for italian, are often better than windows one.\n\nThe KDE i18n architecture is also better than the windows one, as an example I always find in windoze dialog of english application that asks somthing in english and the 2 \"reply\" buttons are in italian instead of english. KDE do not use the translation of \"Yes\" and \"No\" provided in kdelibs.po if the application is not translated so there is no language mixing that, I think, is worse than untranslated.\n\nThere also are a lot of minor language that Micro$oft do not consider because of the little market of those languages (I think one of the 2 official languages of Norway is not considered by MicroZozz while KDE translate it).\n\nSo, you can say Winzozz is *more* (and this is not 100% true) translated but not *better* translated. When we translate something we understand what the message means and than we translate, while I have many example of DOS/Windows translations in docus that mean nothing in italian and the only way to understand their meaning is to translate it back to english \"word by word\" then try to understand in english, of course people that do not understand english cannot do it."
    author: "Andrea"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-19
    body: "- Konqueror is better than explorer/IE\nAre you sure? There is a plenitude of sites which are still rendered incorrectly / not rendered at all with Konqueror.\nNetscape & IE have no problems with those sites.\n- KNode/Kmail is better than Outlook Express\nRecently, I've tried to post some message to comp.windows.x.kde using the KNode coming with KDE 2.2. No problem to view the content of the newsgroup, but \"No valid newsgroups in message\", when trying to post.\n- Kicker is better than the windows taskbars\nWhat about Drag&Drop to invisible windows? In Windows, you just drag the icon to the taskbar button, hold the icon over the button for some seconds and the window activates and goes upmost. This feature HAS EXISTED in KDE up to 2.1.1, and disappeared in KDE 2.2. Wouldn't I like to know why?\n- KDE games are better than Windows games\nLittle games, yes. Big serious games - still nothing to talk about. All Linux games go out two years behind the Windows counterparts.\n- Virtual Destops don't exist in Windows\nThey do exist, even in Windows 95. Using some add-on software, though.\n- Korganizer don't exist in Windows, and so, and so... (KMid, Kate...)\n What about Outlook? Virus prone, buggy, not always convenient, yes. But it does its work.\nKMid? What about Windows Media Player?\nI wonder when will Linux community start LEARNING both GOOD sides and bad sides of Windows, Mac OS,whatever - and not just see those systems as overall bad. Windows has GOOD sides - usability-wise. See the DND-related phrase above."
    author: "Alex"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-19
    body: "- Konqueror vs. Explorer/IE\n\nyes, konqi has a ways to go in the web browser dpt (mostly w/javascript) but it is very usable. where i think it goes beyond WE/IE is in things like thumbnails, the sidebar, the services, kparts (in-browser viewing), profiles, konqi-plugins..\n\n- KNode\n\nwas your expereince the result of a bug knode or the knode user ;-)\n\n- Kicker\n\ni can still drag things on to my taskbar buttons and the window pops up, so i'm not sure what version you are running or how it is configured. i do wish it would also expand the menu on buttons that consist of several items due to grouping. that said, does windows support the number of extensions and flexibility that kicker does? the ms win panel absolutely sucked the last time i had to use it (winME.. maybe XP is better who knows.)\n\n- Virtual desktops\n\nif you are refering to that program that minimizes/maximizes apps to simulate virt desktops, it isn't nearly what you get with real virt desktops. its a poor imitation, at best.\n\nnow, this isn't to say that MS Windows sucks 100%. nor is it to say that there are parts of MS Windows that aren't better than their counterparts in KDE. the question, i believe, is which desktop scores the most wins. this is, of course, somewhat subjective so don't expect everyone to agree on it. but i think your reaction to the (over-?)exuberant post you replied to was a little on the knee-jerk side of reactions."
    author: "Aaron J. Seigo"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-20
    body: ">- Kicker\n\n>i can still drag things on to my taskbar buttons and the >window pops up, so i'm not sure what version you are running >or how it is configured. \nI run the 2.2 version for Mandrake 8.0. Can you tell me how it should be configured? Is that documented somewhere?\nWindow doesn't pop up neither for internal nor for external taskbar. When I drop the icon, it gets dropped into the panel.\n>i do wish it would also expand the \n>menu on buttons that consist of several items due to >grouping. \nI wish that too.\n\n>if you are refering to that program that minimizes/maximizes >apps to simulate virt desktops, it isn't nearly what you get \n>with real virt desktops. its a poor imitation, at best.\nNo, those were real virtual desktops, even with separate set of icons per desktop. Once more, it's not within Windows, it's add-on software -  Norton Desktop, for example.\n\n\n>that said, does windows support the number of >extensions and flexibility that kicker does? the ms win >panel absolutely sucked the last time i had to use it \n>(winME.. maybe XP is better who knows.)"
    author: "Alex"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-22
    body: "\"Window doesn't pop up neither for internal nor for external taskbar. When I drop the icon, it gets dropped into the panel.\"\n\n\nYou don't drop the icon on the taskbar.  You have to wait over the taskbar without moving the mouse for some time (seems longer than necessary).  Drag to the taskbar, and while still holding the button, leave the mouse over one of the buttons.  If you wait a while, the window will pop up, allowing you to drag the thing you are holding into the window."
    author: "not me"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-22
    body: ">You don't drop the icon on the taskbar. You have to wait \n>over the taskbar without moving the mouse for some time \n>(seems longer than necessary). Drag to the taskbar, and \nI've tried to wait for around <B>20 seconds</B>. Seems <B>more than enough</B>.\n>while still holding the button, leave the mouse over one of > the buttons. If you wait a while, the window will pop up, \n>allowing you to drag the thing you are holding into the \n>window."
    author: "Alex"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-20
    body: "Little addition to my previous reply :-)\n>- Konqueror vs. Explorer/IE\n\n>yes, konqi has a ways to go in the web browser dpt (mostly >w/javascript) but it is very usable. where i think it goes >beyond WE/IE is in things like thumbnails, the sidebar, the >services, kparts (in-browser viewing), profiles, konqi->plugins..\nWE has services since W95. Win2K or even earlier has thumbnails. \nOne big problem in Konqueror up to \nKDE 2.2 ( maybe ) is that there is a plenty of services, but they are not always shown in the sidebar, so you should learn the exact syntax of the URL before you may use them. Good for day-to-day KDE developer, outright bad for an average user.\nWindows shows its services in \"My computer\", so  you know of their existence once they are installed.\n\n>- KNode\n\n>was your expereince the result of a bug knode or the knode >user ;-)\nIt was my fault, actually :-)\n\n\n\n>now, this isn't to say that MS Windows sucks 100%. nor is it >to say that there are parts of MS Windows that aren't better >than their counterparts in KDE. the question, i believe, is >which desktop scores the most wins. this is, of course, >somewhat subjective so don't expect everyone to agree on it. >but i think your reaction to the (over-?)exuberant post you >replied to was a little on the knee-jerk side of reactions.\n\nMaybe. But who knows... When the community easily develops really BIG and GREAT things, but nobody seems to pay attention to those little-little-little annoyances, and those annoyances continue for years - there are some HTML rendering problems I've experienced since KDE 1.0 - you easily may overreact.\nI wish Linux to combine the advantages of both Windows as Linux as for today - to be good both for newbie and advanced user. I think that it still is not there partly because of those little things that are not paid attention to."
    author: "Alex"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-19
    body: "> > Konqueror is better than explorer/IE\n> Are you sure?\nFor me and many users, yes. Of course some flashy sites are \"not / incorrectly rendered\", as you say, but they are not my usual sites. It is rare that I meet such sites. When it happens, I ignore the site when I feel it is bad (flashy sites are often bad), or I keep Netscape 4.7. I did not use it from two months...\nUsing favicons, button for increase font size, \"up\" button, cookie management, stop animations command and several others useful functionnalities (enhanced browsing by keywords, translation...) is more important than seeing some flashy sites...\n\nIt is strange you have problems with KNode about KDE newsgroups, for me it is always OK, perhaps a problem of your FAI...\n\n> What about Drag&Drop to invisible windows? \nYes, you are right, here it is an advantage of Windows. However, there is a workaround by moving the windows. I hope it will be a purpouse in KDE 3... (I thought it didn't exist in 2.2.1...) And Kicker has so many other advantages...\n\n> > KDE games are better than Windows games\n>  Little games, yes. Big serious games\nAnd you, are you serious ?? Many \"little games\", as you say, are great games !\nI spoke about the games of the Windows OS,  here is the comparison, not about commercial DirectX games...\nAnd many people prefer cards, Reversi, Mahjong or chess than DirectX game...\n\n> > Virtual Destops don't exist in Windows\n> They do exist, even in Windows 95. Using some add-on software, though.\nThey are add-ons so they don't exist in Windows OS. And I have tried some of these add-ons, they were not as improved as in KDE...\n\n> >  - Korganizer don't exist in Windows, and so, and so... (KMid, Kate...)\n> What about Outlook? \nOutlook is in MS Office, not in Windows OS. How much money ?\n\n>  KMid? What about Windows Media Player?\nIs there some karaoke in WMA ?\n\n>  I wonder when will Linux community start LEARNING both GOOD sides and bad sides of Windows, Mac OS,whatever - and not just see those systems as overall bad. \n\nBla bla bla : please read again my post, you will see that I say that Windows has good sizes.\n\n> Windows has GOOD sides - usability-wise. \n\nI said around the same thing : it is more snappy.\n\nI remember one year ago when I said in a linux newsgroup that Windows was better than Linux (KDE or Gnome)... Now things have changed.\n\nYeah, times are changing ! Bravo KDE team !"
    author: "Alain"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-20
    body: ">> > Konqueror is better than explorer/IE\n>> Are you sure?\n>For me and many users, yes. Of course some flashy sites >are \"not / incorrectly rendered\", as you say, but they are \nwww.anekdot.ru - this site has voting radio buttons after each joke/picture. When starting to show pictures, Konqueror shows the voting buttons in the middle of the picture and repositions correctly only when you start scrolling and scroll the buttons out and then back in.\nwww.anekdotov.net - the alignment of the calendar is severely broken.\nThese sites are NOT flashy  - just Russian joke sites.\nNo problems in Netscape/IE.\nI agree that those are little annoyances, but they exist since version 2.0, or even 1.2, and nobody seems to pay attention.\nWhen you configure Konqueror for some different behavior, using its own menu, the changes don't take any effect, till you restart Konqueror or even the whole KDE. \n>not my usual sites. It is rare that I meet such sites. When >it happens, I ignore the site when I feel it is bad (flashy >sites are often bad), or I keep Netscape 4.7. I did not use >it from two months...\n>Using favicons, button for increase font size, \"up\" button, >cookie management, stop animations command and several >others useful functionnalities (enhanced browsing by >keywords, translation...) is more important than seeing some >flashy sites...\nButton for increase font size doesn't work more than often. And the same button exists in IE.\n\n\n>It is strange you have problems with KNode about KDE >newsgroups, for me it is always OK, perhaps a problem of >your FAI...\nYes, it was clearly my own problem :-)\n>> What about Drag&Drop to invisible windows? \n>Yes, you are right, here it is an advantage of Windows. >However, there is a workaround by moving the windows. I hope \nHow do you spell \"inconvenient\" :-)?\n>it will be a purpouse in KDE 3... (I thought it didn't exist >in 2.2.1...) And Kicker has so many other advantages...\nIt existed even in 2.0. And Windows has that since Windows 95.\nTo your advantage, there are not many users of Windows who know about this drag&drop feature - it is not documented so much.\n\n>> > KDE games are better than Windows games\n>> Little games, yes. Big serious games\n>And you, are you serious ?? Many \"little games\", as you say, >are great games !\n>I spoke about the games of the Windows OS, here is the >comparison, not about commercial DirectX games...\n>And many people prefer cards, Reversi, Mahjong or chess than >DirectX game...\nI prefer little games, too. But I know many people who use their PC to surf the Internet and play games - DirectX games.\n> > Virtual Destops don't exist in Windows\n> They do exist, even in Windows 95. Using some add-on >software, though.\n>They are add-ons so they don't exist in Windows OS. And I \nDo you define KDE as part of Linux OS?\n>have tried some of these add-ons, they were not as improved >as in KDE...\nSeparate icon set for each desktop - how is that to you?\n> > - Korganizer don't exist in Windows, and so, and so... (KMid, Kate...)\n>> What about Outlook? \n>Outlook is in MS Office, not in Windows OS. How much money ?\n>> KMid? What about Windows Media Player?\n>>Is there some karaoke in WMA ?\nThere are plenty of karaoke players as for Windows as for Linux. \n>> I wonder when will Linux community start LEARNING both >>GOOD sides and bad sides of Windows, Mac OS,whatever - and >>not just see those systems as overall bad. \n\n>Bla bla bla : please read again my post, you will see that >I say that Windows has good sizes.\n\n>> Windows has GOOD sides - usability-wise. \n\n> I said around the same thing : it is more snappy.\n\n>I remember one year ago when I said in a linux newsgroup >that Windows was better than Linux (KDE or Gnome)... Now >things have changed.\n\n>Yeah, times are changing ! Bravo KDE team !\nI agree with you, but let's do the work and not rest on the laurels."
    author: "Alex"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-20
    body: "Well, again, Alain Terior is responding to Alex Terior ;-)...\n\n> I agree that those are little annoyances, but they exist since version 2.0, or even 1.2, and nobody seems to pay attention.\n-- I posted 3 bugs about Konqueror/KHtml, 2 are fixed.\n\n> When you configure Konqueror for some different behavior, using its own menu, the changes don't take any effect, till you restart Konqueror or even the whole KDE. \n-- ?? Very strange, for me it take effect when I push OK or Apply button...\n\n> Button for increase font size doesn't work more than often. \n-- I have no problem... Ah perhaps, I remember 1 or 2 pages...\n\n> And the same button exists in IE.\n-- It is not a button, but a menu command. 3 clicks against 1, it is very different, yes : I didn't use it on IE, I use it in Konqui.\n\n> > What about Drag&Drop to invisible windows? \n>Yes, you are right, here it is an advantage of Windows. >However, there is a workaround by moving the windows. I hope \n> How do you spell \"inconvenient\" :-)?\n-- Yes. But it is many more convenient that the big bug I have seen (as I say on previous page). I lost 2 important files (happily I retrieved a backup for the second...). You may try :\n - create on the destop a file Text1.txt, containing the line \"This is my text\". Save and exit the editor.\n - on the desktop change the name Text1.txt to Text2.txt\n - See the content of Text2.txt, now it is :\n [Desktop Entry]\n Name[fr]=Text2.txt\nIs it specific for my installation ? Is something to do for quickly correct such a thing ?... \nSo nothing is white, nothing is black, of course...\n\n[Drag and drop on tool bar]\n>  To your advantage, there are not many users of Windows who know about this drag&drop feature - it is not documented so much.\n-- I used it very much, so I agree with you, it is irritating...\n\n>  I prefer little games, too. But I know many people who use their PC to surf the Internet and play games - DirectX games.\n-- Often it is a period. Linux is an occasion to be desintoxicated ;-)...\n\n > > Virtual Destops don't exist in Windows\n > They do exist, even in Windows 95. Using some add-on >software, though.\n >They are add-ons so they don't exist in Windows OS. And I \nDo you define KDE as part of Linux OS?\n-- I feel KDE like Windows 3.1 was for Dos.\n\n > have tried some of these add-ons, they were not as improved >as in KDE...\n Separate icon set for each desktop - how is that to you?\n-- Yes, it would be interesting. However, I felt it was a lack when I began KDE, and now I think I would turn off this ability if it exists (hmm... but it would be very good to decide if each destop is similar to the first or specific : 3 similar desktops and 1 specific would be very good for me...)\n\n> >Is there some karaoke in WMA ?\n> There are plenty of karaoke players as for Windows as for Linux. \n-- Not in the MS Windows package. But these windows freeware are often good and easy to install. More than KMid, I made many tries, without success... (and I have a common souncart)\n\n > >Yeah, times are changing ! Bravo KDE team !\n> I agree with you, but let's do the work and not rest on the laurels.\n-- Oh yes !\nI think that a big difficulty for maintening KDE is the number of parameters and the many ways KDE is used (and installed). So it needs a great attention."
    author: "Alain"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-20
    body: "I think we can pretty much all agree that the speed issue with KDE is the biggest hurdle, and, I'm impressed with the progress made so far, but it still isn't enough.  Personally, I don't fully blame KDE, but much of the underlying system.  I guess it's only a matter of time."
    author: "dingodonkey"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-23
    body: "It certainly does; using SuSE Linux 7.2 Pro\nI have done totally with W@#$**.\nThe integration into linux has surpassed anything\nI've seen sofar.\nMy God what a JOY TO USE!!!"
    author: "henk lutgerink"
  - subject: "Re: I say KDE has surpassed Windows."
    date: 2001-08-24
    body: "I favor that! Having used suse and suse7.2pro for a month i say their distribution is really good. And the KDE service they deliver is awsome. And KDE 2.2 is as all KDE releases a \"great leap forward\". Thanks developers!! Now looking forward to Koffice and a new release of Aethera ...\n\nThis is like Christmas all the time:)"
    author: "DiCkEII"
  - subject: "Enough already!"
    date: 2001-08-17
    body: "I stopped reading after the first couple of paragraphs because I'm sick of pointless gnome bashing.  Sure, it was a \"NB: I'm not going to comment on GNOME...\" and then \"X and Y are good but GNOME?  Let's just leave it at that\"...um, what do you think you just did?  Is it too much to ask for a bit of maturity in reviews?  Why does a KDE review full of opinion masquerading as fact (\"the fact there is no Linux desktop that can yet match the ease of use, and comfortable usage of Microsoft Windows or Mac OS\"???) require an obligatory GNOME jibe?  Doesn't the reviewer realize a large number of KDE users also use GNOME [apps] and vice-versa?  I used GNOME for years before switching to KDE due to dist'n support reasons (RH->Debian).\n\nTo me, the review lost all credibility in the first two paragraphs, and I'm still waiting for a professional and objective review of KDE 2.2, one that can restrict itself to the subject on hand, or at least back up opinions with objective and well-formed arguments.  FWIW, I have been using 2.2 since it came out and enjoy it significantly.  But let's not allow our regard for KDE favour this kind of amateur elitism posing as a real review."
    author: "wct"
  - subject: "Re: Enough already!"
    date: 2001-08-17
    body: "Yuck get a clue real Linux users don't touch that sellout crap."
    author: "Kraig"
  - subject: "Re: Enough already!"
    date: 2001-08-18
    body: "Do you ever <i>not</i> troll, Craig?"
    author: "ScumRemover Pro"
  - subject: "Re: Enough already!"
    date: 2001-08-17
    body: "As much as I understand your anger, you should note that the reviewer is no KDE developer or participant on KDE; therefore KDE is not responsible for the contents of the review. Our opinion is that GNOME bashing has no place here nor does KDE bashing have a place on gnotices. Please try to keep comments about the quality of GNOME software out of KDE reports, comments or reviews."
    author: "Ralf Nolden"
  - subject: "Re: Enough already!"
    date: 2001-08-20
    body: "completely agreed"
    author: "giantux"
  - subject: "Re: Enough already - people need to relax"
    date: 2001-08-25
    body: "Cripes! I agree completely. Admittedly, I use GNOME most of the time, but I think KDE is a great project too. Every time I read about a new KDE release, I load it up and I'm always impressed.\n\nI know there are zealots on both sides, but for some reason, posters on this story just seem to be particularly venomous. I mean, there's a really cool new KDE release out and a good 40% of the comments here on the KDE website are about how much GNOME sucks. Come on people, lighten up a little bit. I don't mean to be harsh here, but both projects would benefit if some people would stop investing so much of their self-worth in which desktop they run on their computer."
    author: "bluetea"
  - subject: "\"view doc source\" not working?"
    date: 2001-08-17
    body: "seems like \"view document source\" does not work in konqueror anymore. has anybody else experienced this problem (could not find a bug report)?\n\nthanks!"
    author: "anon_"
  - subject: "Re: \"view doc source\" not working?"
    date: 2001-08-17
    body: "Works fine on this page with my KDE 2.2 final. At least it's not a consistent error then. Good luck!"
    author: "Anders"
  - subject: "Re: \"view doc source\" not working?"
    date: 2001-08-18
    body: "It works for me too ... it brings-up whatever you have setup as your default text editor.  Maybe the problem is launching that app?\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: \"view doc source\" not working?"
    date: 2001-08-18
    body: "that was it: after removing \"kant\" and creating a symlink kant->kate everything worked fine. don't know however how to _set_ the default editor for kde. could not find an appropriate option in kconfig.\n\nthanks anyway."
    author: "anon_"
  - subject: "Re: \"view doc source\" not working?"
    date: 2001-08-18
    body: "To display the source, konqueror uses the primary edtior defined to handle text/plain mimetypes. So you can configure it using via 'File Associations' of konqueror.\n\nAndreas"
    author: "Andreas Schlapbach"
  - subject: "koutlook = aethera"
    date: 2001-08-17
    body: "this really needs to check out aethera.\n\nvery nice program. yeah, it still has a few (major) bugs, but it's getting there.\n\n-john"
    author: "JOHN"
  - subject: "Re: koutlook = aethera"
    date: 2001-08-17
    body: "Yeap your right on that one. Aethera is the answer. I'd love to see Aethera as the default pim for 3.0.\n\nCraig"
    author: "Craig"
  - subject: "Re: koutlook = aethera"
    date: 2001-08-17
    body: "Yes, aethera looks promising. But at least in combination with KDE 2.2 it crashes always when I click on new mail... I think I have to wait for the next version. Anyway, the old xfmail is still unreached, but it really gets time for something competitive."
    author: "Sri"
  - subject: "Re: koutlook = aethera"
    date: 2001-08-18
    body: "Can the framework being developed for Gideon be used for an app similar to Outlook/Notes, using existing apps as parts? Or maybe a quanta type app? My reasoning is that if something as complex as an IDE can use it, a groupware or Web deelopment app can too."
    author: "Ignorant newbie"
  - subject: "RedHat 7.1 KDE 2.2 installation Scripts (simple)"
    date: 2001-08-17
    body: "Hi RedHat 7.1 Users,\n\nThis is the *first* I won't complain about RedHat RPMS. This is real cool no troubles except those big dependencies files glibc-common, etc.,\n\nI have written few scripts to automate rpm based install of KDE 2.2 on a Standard RedHat KDE workstation. These scripts works fine for me, and hence I am providing these just to help you and so I disclaim any responsibility for any harm caused to your computer.\n\nJust get All rpms from ftp.sourceforge.net and from rawhide RH 7.1 update site at\n(http://rpmfind.net/linux/rawhide/1.0/i386/RedHat/RPMS).\n\n1.KDE2.2rpm-requirement.txt: contains the RPMS you need to get these script working properly.\n\n2.install-KDE-2.2: will uninstall KDE 2.1.1 from your workstation and install KDE 2.2 and recheck for any missing packages\n\n3. reinstall-KDE-2.2: will check for missing packages which should be installed.\n\n4. uninstall-KDE-2.1.1: this will uninstall (almost) all KDE 2.1.1 packages.\n\nTo sum up:\n\n1. copy these 3 script files in the same folder in which you save the required rpms as listed in KDE2.2rpm-requirement.txt \n\n2. run ./install-KDE-2.2\n\nThat's all...\n\n---\nI wish this installation headache get solved soon by the stable release of\nKDE-Installer, which would download all the required packages. But when?\n---"
    author: "Asif Ali Rizwaan"
  - subject: "Re: RedHat 7.1 KDE 2.2 installation Scripts (simple)"
    date: 2001-08-17
    body: "Just grab my KDE 2.2 RPMs for RH71 instead: http://www.opennms.org/~ben/kde2.2/\n\nI rebuilt everything on RH71, should be no weird dependency issues."
    author: "Ranger Rick"
  - subject: "Re: RedHat 7.1 KDE 2.2 installation Scripts (simple)"
    date: 2001-08-18
    body: "Rick,\n\nIs there any way to FTP the files from your site instead of HTTP downloading it one by one?\n\nRegards,\n\nJake"
    author: "JJacob"
  - subject: "Re: RedHat 7.1 KDE 2.2 installation Scripts (simple)"
    date: 2001-08-18
    body: "you can use wget to grab them all"
    author: "Ranger Rick"
  - subject: "Help ! It doesn't work for me..."
    date: 2001-08-19
    body: "Everything except 'ksplash' and 'dcopserver' dumps core:\nIllegal instruction (core duped).\nSo I've downgraded back to KDE 2.1. Any help ?"
    author: "Krame"
  - subject: "Re: Help ! It doesn't work for me..."
    date: 2001-08-19
    body: "Uhhh... not sure what to tell you.  Sounds like you've got other problems, or you're not really running RedHat 7.1...\n\nBut it's hard to say without more info."
    author: "Ranger Rick"
  - subject: "Re: Help ! It doesn't work for me..."
    date: 2001-08-20
    body: "I have AMD K6-2 processor, it is i586 architecture, so I suspect that  'illegal instruction' comes from architecture incompability ( i686 instruction ). Oh, but maybe somebody with AMD K6 have got it to work ?"
    author: "Krame"
  - subject: "Re: Help ! It doesn't work for me..."
    date: 2001-08-20
    body: "Well, I haven't changed the compilation options from what RedHat uses, which is -march=i386 -mcpu=i686, which targets i686 but is backwards compatible to i386.  If you can run anything else in redhat, you should be able to run these...\n\nBut I'm no compiler guru (not even a programmer), so this is beyond me as far as being able to fix it.  =)"
    author: "Ranger Rick"
  - subject: "Re: Help ! It doesn't work for me..."
    date: 2001-08-23
    body: "I also could not successfully install using Ranger Rick's RPMs on a K6-2 machine.  Hopefully the original RedHat RPMs with Asif's script will do the trick.  In any event, the problem illustrates why writing a KDE installer is not an easy undertaking.  Thanks to both Asif and Ranger Rick for their efforts."
    author: "another K6-2 user"
  - subject: "Re: Help ! It doesn't work for me..."
    date: 2001-08-24
    body: "How funny ! After I downgraded back to KDE 2.1.1 I noticed that liquid theme is still on the theme list. So I activated it and it worked ! On KDE 2.1.1 and AMD K6-2."
    author: "Krame"
  - subject: "Re: RedHat 7.1 KDE 2.2 installation Scripts (simple)"
    date: 2001-08-17
    body: "I am looking for people to work with and try and get a program together that would work like GNOME's red carpet.\n\nAnyone interested ???"
    author: "Mark Webb"
  - subject: "Using Ximian Red-Carpet for our own needs"
    date: 2001-08-18
    body: "We can use Ximian's existing Red Carpet code and use their already developed framework for multi-distro installation and dependency resolving, while providing our own XMLs for packages. I'm not sure Ximian, being GNOME centric and selling GNOME apps, would agree to provide an official KDE channel _within_ Red Carpet, but we could simply provide an alternative source from where Red Carpet would fetch the package lists and downloads.\n\nRed Carpet's source can be downloaded at ftp://ftp.ximian.com:/pub/ximian-source/debian-potato-i386/\n\nHack (develop?) away!"
    author: "Toastie"
  - subject: "Re: Using Ximian Red-Carpet for our own needs"
    date: 2001-08-18
    body: "uSE SuSE."
    author: "reihal"
  - subject: "Re: RedHat 7.1 KDE 2.2 installation Scripts (simple)"
    date: 2001-08-18
    body: "i think it can be a great idea share the red carpet's backend, in a library , so the two projects just have to do the gui and leave more time to work in the shared backend."
    author: "jodi"
  - subject: "Re: RedHat 7.1 KDE 2.2 installation Scripts (simple)"
    date: 2001-08-17
    body: "The KDE-installer you speak of (the one that would install KDE itself) has been discontinued.  Work is not proceeding.  I don't know how far the project got before stopping.  I think they have the easy part done, the GUI, but not the hard part, the package backends for each and every distro out there.\n\nIf anyone wants to pick it up, the source is out there in CVS.\n\nNow, there is also another KDE installer project, but that one is supposed to install KDE apps over the Internet by downloading them.  It doesn't seem to be aiming to install KDE itself.  Work on that is still proceeding, as far as I know (though there is only one guy working on it)."
    author: "not me"
  - subject: "Re: RedHat 7.1 KDE 2.2 installation Scripts (simple)"
    date: 2001-08-22
    body: "Okay, Nick Betcher has just said on KDE-Devel that he will pick up the project again, although he is still disappointed by the lack of contributions from other developers.  If you want there to be a KDE-Installer, now is the time to help out. (and by help out I mean \"start coding right now\" and not \"join the mailing list and make suggestions but never actually contribute anything\")"
    author: "not me"
  - subject: "KDE 2.2"
    date: 2001-08-17
    body: "KDE will never compite with Windows because Windows is an operating system while KDE is only a Desktop. I mean ... Hardware detection-configuration and system configuration for example. Its part of distro work, but you know, the distros are so far of KDE-Gnome desktop facilities.\n\nIf KDE can add into Kcontrol some modules for hardware detection-configuration and system configuration (with scripts maintance by the distro down the kcontrol module for example), will compite with windows. i hope .. :)\n\nI think KDE 3.high-number.x even KDE 4 will compite completly with windows .. i hope"
    author: "Daniel"
  - subject: "Re: KDE 2.2"
    date: 2001-08-18
    body: "> If KDE can add into Kcontrol some modules for \n> hardware detection-configuration and system \n> configuration (with scripts maintance by the \n> distro down the kcontrol module for example), \n> will compite with windows. i hope .. :)\n\nWell SuSE has added yast2 into kcontrol, so I guess we can comp_e_te right now.\n\nActually I don't go around detecting hardware all day ( I would like to have to do it more often), but rather work on my PC, and KDE works a lot better and faster than Win 98-SE.\n\nWith faster I don't mean startup times, but blocking guis, nonmoving windows, because the program is waiting, stuff like that makes me scream."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: KDE 2.2"
    date: 2001-08-19
    body: "What version of windows are you talking about ?\nDOS is still the basis of 95/98/ME, so those Windows are no better or worse than KDE from that point of view. Windows NT or 2000 could be called an Operating System (by some)."
    author: "Steve"
  - subject: "Re: KDE 2.2"
    date: 2001-08-20
    body: "I entirely agree with this.  You cannot compare apples and oranges, just like you cannot compare operating systems and window managers.  To be controversial, I will say that windows still has a performance advantage (I compare windows 98 and linux 2.4.7 w/ kde), quite possibly an unfair one.  I would like to see Microsoft sell its operating system and leave window managers to open sourcing, reminicent of the DOS days.  Only then will people be able to compare linux to windows correctly."
    author: "Greg"
  - subject: "Re: KDE 2.2"
    date: 2001-08-20
    body: "Why is everyone talking about \"competing with Windows\"?\nI have yet to see the Windows GUI touch the flexibility, configurability or aesthetics appeal that are common in even older versions of KDE or GNOME."
    author: "cerebraldebris"
  - subject: "Re: KDE 2.2"
    date: 2001-08-23
    body: "Finaly someone hit the nail.\n\nKDE/Linux enables me to use my computer more productively that Microsoft Windows will EVER allow me to do.\n\n/Jocke!"
    author: "Joachim Holst"
  - subject: "w00t!"
    date: 2001-08-18
    body: "First of all I want to congradulate the KDE team on their great products. It's definately something to be proud of. =)\n\nNice to see that xinerama works properly now. (I hate single monitor even with virtual desktops)\n\nI am relatively new to KDE and even *nix for that matter. I've always been a windows user but lately I've been trying out different flavours of *nix, firstly to learn more, and secondly to move away from the Microsoft monopoly.\n\nI keep seeing some *nix users making adament statements that they don't want everything integrated or that a certain desktop looks too much like windows, and it troubles me. I understand the diversity they want, but if you contemplate the future of *nix as a desktop OS, it is obvious that everything needs to be closely integrated and userfriendly if you want to compete with the Redmond Machine. The average Joe/Jane off the street can't handle something that is going to take them days or even weeks to learn/install/configure.\n\nKDE is doing a great job catching up, and hopefully in version 3, they can compete side by side with windows. \n\nIt's obvious that no other *nix desktop development is in the race anymore. I won't mention the \"G\" word *wink*\n\nKeep up the great work all,\nCalDude"
    author: "CalDude"
  - subject: "Re: w00t!"
    date: 2001-08-18
    body: "Not really, if you work in a tech support line, you would see that Windows \"nice-little-look\" is not so easy to use for people <b>who have not use computers before</b>. KDE is relatively easy to use. I think its because you have been using Windows way to long, its gets into your head (the UI)"
    author: "Rajan Rishyakaran"
  - subject: "Re: w00t!"
    date: 2001-08-18
    body: "Why not to mention Gnome? They're going\na good job too...\n\nThey have the Red Carpet. It's pretty\neasyer to install the Gnome then KDE.\nI prefer KDE, but I like very much\nGnome too..\n\nAnother one that is VERY, VERY good\ntoo is WindowMaker. YES, there are\nothers in the race. I still use these\nthree window managers.\n\nSorry about my poor English. I'm still\nlearning English. My native language\nis Portuguese.\n\nWell, I hope KDE 3.0 and higher get\neven better to start competing with\nM$ Ruindow$  (Ruim = bad in portuguese.\nIs sounds something like Hoo in, so we\nuse that Ruim + Window$ to refer to it)\n:)\n\nThank you for reading me."
    author: "Felipe"
  - subject: "Gecko not Mozilla"
    date: 2001-08-18
    body: "KMozilla, i think, is just a Mozilla clone built for KDE. Athera no KOutlook (same case with KIllustator is KOutlook is use - courtroom) Gecko I think is quite fast, but its mozilla that slows everything down."
    author: "Rajan Rishyakaran"
  - subject: "Re: Gecko not Mozilla"
    date: 2001-08-18
    body: "No, kmozilla is gecko used as replacement for khtml in konqueror / KDE. Install kdebindings.\n\nMozilla can be compiled with QT for those people who like antialiassing(among other advantages over GTK-1.x). Maybe you thought this was meant?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Gecko not Mozilla"
    date: 2001-08-18
    body: "I've never been able to successfully compile mozilla source --with-qt and have it work. At latest attempt with 0.9.3 source and qt 2.3.1 it compiled ok but refused to start with some major error. I'd love to see a qt-mozilla or k-mozilla but until someone compiles it and gets it working and makes it available for download then it still doesn't exist. Any takers? This is the big one I've been looking for in kde."
    author: "ck"
  - subject: "Re: Gecko not Mozilla"
    date: 2001-08-18
    body: "Open Konqueror, click View->View Mode->KMOZZILA.  You now have the Konqueror interface using the \"kmozzila\" widget instead of the \"khtml\" widget to render pages (ie, you now have Konqueror w/ Gecko engine)."
    author: "optikSmoke"
  - subject: "Re: Gecko not Mozilla"
    date: 2001-08-19
    body: "But what version of the gecko engine?\n\nIt wouldn't be fair if they bundled some ancient version of gecko with it."
    author: "j"
  - subject: "Re: Gecko not Mozilla"
    date: 2001-08-20
    body: "It uses whatever version of it you have installed on your system (the same way Galeon and SkipStone)."
    author: "Evandro"
  - subject: "Re: Gecko not Mozilla"
    date: 2003-01-08
    body: "but which version of konqueror is able to do so? i'm using 2.2.2 (kde 2.2.2) and i cannot find anything. :("
    author: "Manuel"
  - subject: "Re: Gecko not Mozilla"
    date: 2001-08-18
    body: "\u00ee\u00c5 \u00ce\u00c1\u00c4\u00cf \u00d0\u00c5\u00d2\u00c5\u00c8\u00cf\u00c4\u00c9\u00d4\u00d8 \u00ce\u00c1 Gecko - Konquerror \u00cc\u00d5\u00de\u00db\u00c5 !\n\nNot gecko - konqueror best"
    author: "Andrew"
  - subject: "iconarranging"
    date: 2001-08-18
    body: "Only compiled kdebase and kdelibs at the moment, but it runs wonderfull already.\n\nOne thing however: Icons seem to autoarrange, how to disable this? There is no entry for turning it off when I right (or middle) click on the background....\n\nDanny"
    author: "Danny"
  - subject: "Re: iconarranging"
    date: 2001-08-18
    body: "There is no auto-arrange. When KDE starts all icons vertically aligned - at least they were with me. When I switched off vertical alignment in DEsktop configuration (or whatevr in English), all icons were horizontally aligned, but were free to place them anywhere on the desktop.\n\nKDE 2.2 is great, cograts to the development team for this feat.\nonly the default sounds in KDE are annoying, they're blocking my Realplayer, so I had to switch off aRts soundsserver in the config panel"
    author: "Jan"
  - subject: "Re: iconarranging"
    date: 2001-08-18
    body: "Yes I had the same problem, it was irritating... I tried several things without success but then it was again fine. After 1 or 2 reboots... Strange... And I see that now the vertical alignment is checked. I uncheck it... Yaaaarghhh all my icons are now horizontal on the top of the desktop !... I try to rearrange... Yes, I can... It seems now good..."
    author: "Alain"
  - subject: "Re: iconarranging"
    date: 2001-08-19
    body: "... you can read 'bout whats happening in 'startkde' script... :)"
    author: "Zaufi"
  - subject: "KDE 2.2 & Xinerama"
    date: 2001-08-18
    body: "KDE 2.2 looks like a big step forward.  The reason I was willing to give it another shot was the fact that it claimed to have Xinerama support.  I switched to Gnome + E a long time ago because I couldn't stand KDE's lack of support, even though I loved the desktop environment itself.  However, I've had no luck w/ KDE 2.2's xinerama support.  I'm running it on RedHat and after some reading I pulled down the kdebase src rpm and recompiled it w/ the --with-xinerama flag, thinking the rpm perhaps wasn't compiled with it (looks like it wasn't).  NOW - I see the \"Xinerama\" section in \"Window Behavior\" / Advanced tab in kcontrol.  However, it's all grayed out - I'm unable to modify the settings (click the boxes to enable it).  I manually edited the kwinrc file to turn the options on, but still no dice.  Has anyone else had such problems?  I'd really like to give KDE 2.2 a fair shot, but I can't stand this issue."
    author: "chardros"
  - subject: "Re: KDE 2.2 & Xinerama"
    date: 2001-08-18
    body: "I belive there is also a --with-xinerama flag in kdelibs????\n\nWorth a look.\n\nRoyce"
    author: "RoyceyBaby"
  - subject: "I stopped using KDE for few reasons!"
    date: 2001-08-18
    body: "I stopped using KDE for few reasons, (no applications, no applications, no applications based on QT!).  Yes, KDE seems to be moving faster than GNOME, but... it seems to me, it's lacking application support!  All my applications are based on GTK+, for example (GNUMERIC, GNUCASH, GTKGRAPH, OREGANO, GIMP.... you name it!).  No one seems to be writing good, useful applications based on QT.  Why run KDE using QT when all your applications are bases on GTK+?  Even downloading netscape requires GTK+!  Also, KOFFICE, so far is useless on my mind.  If it can't export, import Microsoft Office files, it's useless!  Star Office  is slow, but, hey, it works.  Open Office seems to be using GTK+ also.... GNUMERIC is good a year & half ahead of KSPREAD.  I use XFCE with GNOME.  XFCE look as my desk and running GNOME in the background.  I tried running KDE applications under XFCE, but... it takes long time to open.  GNOME maybe useless, too slow (with all that crappy nautilus and so on), but running GNOME in the background and using XFCE as a desktop makes a complete DESKTOP system that's fast, stable and application rich!  GNOME+XFCE is like having better CDE.  KDE is doing great, but... it's QT based applications are limited; and, that's the whole reason I'm using GNOME + XFCE.  Also, I would like to have an option like CDE PANEL look in KDE.\n\nP.S.  My Redhat 7.1 also seem to make use of GTK+ tool kits for it's system managements, for example, up2date....etc.\n\nCordially,\nSung N. Cho,\nSaturday, August 17, 2001.\n\nDept. of Physics,\nVirginia Polytechnic Institute & State University."
    author: "Sung N. Cho"
  - subject: "speed on XFCE"
    date: 2001-08-18
    body: "Make sure you use kdeinit_wrapper when you launch KDE applications.  It's much faster.  You can set up symlinks to automatically use kdeinit."
    author: "ac"
  - subject: "http://apps.kde.com"
    date: 2001-08-18
    body: "Have a look at the application data base, you will find many programs for QT/KDE there.\n\nI do not agree with you about kword, it is already very useful, which version did you test? Try out kword-1.1 next week, you will like it. Also look at kspread again, will ya?\n\ngimp is king, but there are alternatives, wait and see.... \n\ngnucash, well gnucash, there are commercial QT/KDE alternatives: kapital. QT/KDE based and nice, but it costs money. On the other hand it is said to be better than gnucash.\nhttp://www.thekompany.com/products/kapital/?dhtml_ok=0\n\n\nI can't say a lot about  GTKGRAPH or OREGANO, because I am not into that kind of thing, but I searched for you.\n\nkpl looks like a good alternative to gtkgraph to me:\nhttp://frsl06.physik.uni-freiburg.de/privat/stille/kpl/\n\nviPEc looks like a good alternative to oregano:\nhttp://vipec.sourceforge.net/\n\nMaybe check them out and report back?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: http://apps.kde.com"
    date: 2001-08-19
    body: "also have a look at kmatplot ... it seems to be _really_ nice (and it supports octave), but the build system seems to be a bit broken.\nthere are also free KDE finance programs, have a look at apps.kde.com."
    author: "ik"
  - subject: "Re: I stopped using KDE for few reasons!"
    date: 2001-08-19
    body: "there are less apps for kde than gnome because:\n\n1) many people find c simpler to learn than c++\n\n2) commercial companies (like redhat, mandrake) prefer to stick to an LGPL gui library, so in case they need to develop a commercial app they can do it without paying. instead with QT you have to pay money to trolltech.\n\n3) the development of GTK+ is mainly driven by open source people, so it's a real product of the open source community. instead qt is mainly developed by trolltech, so it is essentially a commercial product, even is there is a free GPL version."
    author: "ultraman"
  - subject: "Re: I stopped using KDE for few reasons!"
    date: 2001-08-19
    body: "(i'm not trying to be a troll, just responding :) )\n\n>> there are less apps for kde than gnome because:\n\nhow do you get that statistic? I would agree that there are probably more gtk+ apps than kde/qt apps, but I highly doubt that there are more gnome-only apps than kde apps. If you count the number of gtk+/gnome apps and the number of qt/kde1/kde2 apps, you'll probably find it about the same. \n\n>> 1) many people find c simpler to learn than c++\n\nThere are a LOT more people doing c++ development in the world today than c development. I'd venture to say that more software in the world is written in c++ than in c (by a long shot).  \n\nIn the UNIX and UNIX-like world, C has had a tradition of usage for over 30 years. Still, I'd say that the C-C++-Java ratio would be something like 4-2-1. It is something like that in sourceforge.\n\n>> 2) commercial companies (like redhat, mandrake) prefer to stick to an LGPL gui library, so in case they need to develop a commercial app they can do it without paying. instead with QT you have to pay money to trolltech.\n\nYou can sell gpl'd software without paying trolltech (on X11), or generally, you can sell any gpl'd software.\n\n> 3) the development of GTK+ is mainly driven by open source people, so it's a real product of the open source community. instead qt is mainly developed by trolltech, so it is essentially a commercial product, even is there is a free GPL version.\n\nThe development of Qt was done by TrollTech, a company devoted to open sourced software. I'd agree that the FreeQt license was probably not a OSI license, but the QPL was clearly. You can use and sell any software for X11 that links with Qt. The only restriction is that you must keep it open. \n\nFor Windows, you can use the non-commercial version, and develop open and free (as in beer) software that is under less restrictive licenses than the GPL. Or, you can chose to buy the commercial Qt and receive lots of support (have you ever tried to use Gtk+ in windows? it does not work that well)\n\nIf you bring commercialization to this, notice how many gnome-hackers are employed by Ximian :)"
    author: "fault"
  - subject: "Re: I stopped using KDE for few reasons!"
    date: 2001-08-20
    body: "> >> 2) commercial companies (like redhat, mandrake) prefer to stick to an LGPL gui library, so in case they need to develop a commercial app they can do it without paying. instead with QT you have to pay money to trolltech.\n\n>         You can sell gpl'd software without paying trolltech (on X11), or generally, you can sell any gpl'd software.\n\nI think he meant if they want to produce a *closed* source application like Yast e.g. (I think it is, I don't use SuSE). I personally don't see this as something that would turn companies away though. People in the business world like to have somebody to complain to if things doesn't work the way their supposed to and if they buy qt, they also get a company to blame for it's errors. Some companies might however like the fact that they can develop free of charge using GTK+, but I (in my perfect world) hope that they rater judge them on their technical merits and not the cost. If I ran a company I sure would, price is merly a part of the eqation. Well, enough ranting...\n\n\nRegards,\n\nJ\u00f6rgen Lundberg (jorgen@asd.campus.luth.se)"
    author: "J\u00f6rgen Lundberg"
  - subject: "Re: I stopped using KDE for few reasons!"
    date: 2001-08-20
    body: "> 1) many people find c simpler to learn than c++\n\nC/GTK+ is much harder to learn and use than C++/Qt. Do you recommed Gtk++ for newbies ? Gtk+ uses its own non-standard, object-oriented language build on top of simple C language.\n\n> commercial companies (like redhat, mandrake) prefer to \n> stick to an LGPL gui library, so in case they need to \n> develop a commercial app they can do it without paying. \n> instead with QT you have to pay money to trolltech.\n\nCompanies which write commercial programs choose rather Qt ( Opera, Corel, Borland ). I don't know any serious commercial apps using Gtk++. Price is important for in-house development, especially because Qt is a very expensive product, so some _distributors_ choose Gtk+ over Qt. In software development a quality is most important and Gtk hasn't a production quality ( only Microsoft can sell a MFC-shit ). \n \n> the development of GTK+ is mainly driven by open source \n> people, so it's a real product of the open source \n> community. instead qt is mainly developed by trolltech, so it \n> is essentially a commercial product, even is there is a free\n> GPL version.\n\nYes, that's true. People don't want to support a commercial company, when they give away their work for free.\n\nAnd a few other reasons:\n\nC++ compiler is very slow (  for each *.cpp file the second one *.moc.cpp is generated which slows everything ever more ). Elf linker is slow.\n\nBuild system with moc and uic is very complicated, with some conflics between KDE and Qt, with instructions like that  :\nuic -tr i18n -i dialoginterf.h ./dialoginterf.ui | sed -e \"s,i18n( \\\"\\\" ),QString::null,g\" >> dialoginterf.cpp || rm -f dialoginterf.cpp\necho '#include \"dialoginterf.moc\"' >> dialoginterf.cpp\n\nBut I see no counterpars to Qt. Maybe Gtk++ wrappers, but I prefer to use only a native interface - there are always so many problems with wrappers.  If GNOME had been more programmer friendly it would have beat KDE. But they are sinking - have you seen its COM-based component model (Bonobo). You can tell that C is easier than C++ forever, this means nothing. They have forgotten that they are not Microsoft."
    author: "Krame"
  - subject: "Re: I stopped using KDE for few reasons!"
    date: 2001-08-20
    body: "Gtk+ or QT.. I don't give a rats *ss as long the app is good. Which widget set my apps are don't decide alone which desktop/UI is better and which I want to use. But I like to see better compatibility between KDE and GNOME like drag'n drop. Sooo.. here's *THE* question to KDE and Gnome developers: when can I drag and drop between GNOME and KDE apps???"
    author: "Anton V."
  - subject: "Re: I stopped using KDE for few reasons!"
    date: 2001-08-20
    body: "Gtk+ or QT.. I don't give a rats *ss as long the app is good. Which widget set my apps are don't decide alone which desktop/UI is better and which I want to use. But I like to see better compatibility between KDE and GNOME like drag'n drop. Sooo.. here's *THE* question to KDE and Gnome developers: when can I drag and drop between GNOME and KDE apps???"
    author: "Anton V."
  - subject: "Upgraded to x4.1 & kdm doesnt start & no keyboard"
    date: 2001-08-18
    body: "On debian, I upgraded kde to 2.2 then x to 4.1. I was running kdm, but after the x upgrade, booting starts me to a weird gnome desktop (I primarily use gnome) and does not start kdm or any *dm. The desktop is blank (like the x background) and I am only able to click to pop-up a menu.  I can use the mouse, but the keyboard does not work.\n\nXF86Config-4 looks fine, it is the same one which worked under x4.0.3.  Also, XF86Config-4 includes keyboard settings. I ran kdm-config (after cutting and pasting my root password from a document) and checked kdm's configuration --- it looked fine.\n\nI decided to try gdm. But, the gdm package will not install until kdm is removed. I tried to remove kdm, but removal fails.\n\nAny suggestions?"
    author: "gecko"
  - subject: "Re: Upgraded to x4.1 & kdm doesnt start & no keybo"
    date: 2001-08-19
    body: "XF86Config-4 has nothing to do with which apps get launched, it's just a hardware configuration script that indicates the details about what hardware you want X to use. The software side is done with various scripts in the /etc/X11 directory.\n\nYour problem is most likely with overwriting of the xserverrc and xinitrc (or just plain removal of these two).\nX probably asked you whether you wanted to replace these or keep them, and you probably answered you wanted them replaced.\n\nI suggest you make your own .xinitrc file in your home directory that contains the instruction to start kde (which I believe to be \"startkde\", but I'm not sure, it's been a while.\n\nThe other option is to force a configure of kdm, by running \"dpkg-reconfigure kdm\". That might overwrite your files with the correct ones.\n\nOr even better, just look through the documentation of kdm (man kdm, info kdm, files in /usr/share/doc/kdm, and the site of kdm)"
    author: "j"
  - subject: "Re: Upgraded to x4.1 & kdm doesnt start & no keyboard"
    date: 2001-08-20
    body: "<P>\nI've had problems after upgrading to KDE2.2 too. As I observed, it's because kdm configuration files move from <TT>/usr/share/config/kdmrc</TT> to <TT>/usr/share/config/kdm/kdmrc</TT>. Moreover, the kdmrc file from the KDE2.2Beta1 is not compatible with the one from KDE2.2 final. So I had to modify <TT>/usr/share/config/kdm/kdmrc</TT> again. And, I' had to <STRONG>create</STRONG> file <TT>/etc/sysconfig/desktop</TT> (I use Mandrake 8.0, which has RH-based sysconfig dir) with contents of:\n</P>\n<P><TT>\nDESKTOP=KDE.\n</TT></P>\n<P>\nBe careful with session types list, desktop types are case sensitive, so correct values are (so far I know)\n</P>\n<P>\n<TT>KDE, Gnome, IceWM, BlackBox...</TT>\n</P>\n<P>\nAfter trying to run kdm with new configuration nothing helped but reboot (yes, Windows users laugh now..), but \nfrom then it works perfectly.\n</P>\nI hope this will help."
    author: "Ondrej"
  - subject: "[slightly OT] kmozilla impressions"
    date: 2001-08-19
    body: "Hello,\ni toyed a bit with kmozilla and altough it seems to be a bit early for real browsing (frames are broken, and apparently posting too) its a nice project ...\nI noticed the following:\n- selecting text is way faster in kmozilla than it was in khtml (it seems selecting text in khtml is cpu hogging and slow on my system ... is this a known issue or did i misconfigure something ?)\n- scrolling seems faster, but i discovered thats just because the mousewheel seems to be configured differently in mozilla. is there a way to reconfigure kde mousewheel support ?\n- it also seems (don't know if its true) mozilla uses some screen-buffering to avoid flicker. Altough it does not make it faster, it gives that impression, and it's nice.\n- unrelated: sometimes the html form widgets in khtml are slow here (but its only sometimes, the cpu stays idle, and hitting refresh fixes it)\ni saw someone mention this here too ...\n\ngreetings"
    author: "ik"
  - subject: "Re: [slightly OT] kmozilla impressions"
    date: 2001-08-19
    body: "Regarding the mouse wheel, you should be able to configure it in kcontrol->Peripherals->Mouse->Advanced, \"Mouse Wheel Scrolls By:\""
    author: "mikecd"
  - subject: "Re: [slightly OT] kmozilla impressions"
    date: 2001-08-19
    body: "What you mentioned is exactly the point where mozilla, opera, IE and possibly still even netscape outperform khtml quite a bit. The problem seems to be that every time something has to be highlighted, underlined, selected (mouse activities) etc the whole DOM (or rendering ?) tree has to be traversed to look up the specific element(s). This process takes so much cpu that it results in a lagging effect. I know several speed ups were done in the past, but IMO it's still rather sluggish. Does anyone know how these things are done differently in the mentioned browsers ? \n\nAnyway, I do think khtml is _really_ good, except for a few little problems :)"
    author: "Jelmer Feenstra"
  - subject: "Is 2.2 still ugly as sin?"
    date: 2001-08-19
    body: "Just wondering when I'll actually _want_ to switch from GNOME."
    author: "Sniggly fox"
  - subject: "Re: Is 2.2 still ugly as sin?"
    date: 2001-08-19
    body: "heh! gnome is a bloated fugly POS, with almost no developmental momentum\n\nmaybe it's time to revisit kde again (i've used kde 1.x, then used gnome 1.2.x, then used kde 2.1.x for a while before starting to use blackbox 0.61, and now I'm compiling kde 2.2.x, it looks nice :)\n\ngood work kde developers"
    author: "aaron leahman"
  - subject: "Maybe a GNOME theme for KDE?"
    date: 2001-08-19
    body: "I also think GNOME looks better than KDE, but KDE works better. Then how about a GNOME theme (icons and all) for KDE? Anyone wants to give it a try? I might as well, when I get the time."
    author: "Joe"
  - subject: "Re: Maybe a GNOME theme for KDE?"
    date: 2001-08-19
    body: "Well an icon theme is available here:\nftp://derkarl.org/pub/incoming/theme-gnome-0.4.ikn\n\nIt is not complete, but works fine. (There are also some other icon themes to be found there...)\nMaybe you can complete it? \n\n\nRegarding the gnome style: You can import gtk-themes into KDE without a problem and kwin uses icewm themes if you tell it to."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Maybe a GNOME theme for KDE?"
    date: 2001-08-20
    body: "I tried to import the Gradient theme from GNOME ( it's a pixmap theme), but klegacyimport can't get it right."
    author: "dfs"
  - subject: "Re: Is 2.2 still ugly as sin?"
    date: 2001-08-21
    body: "Hmm, interesting that dot trolls such as this appear in most profusion whenever kde makes a major release, or the next few articles after that.\n\nPerhaps someone should get a sociologist to examine trolls in action (heh, perhaps the Crocodile Hunter)  and see if there is a behavioral pattern..."
    author: "Carbon"
  - subject: "Re: Is 2.2 still ugly as sin?"
    date: 2001-08-21
    body: "Eat me, retard.\n\nI asked if KDE 2.2 was still damn ugly. Nobody's answered! Is it because you disagree that KDE used to be ugly? Or is it because it still is ugly? What?\n\nSeriously dude, I want performance and reliability out of my hardware, kernel, filesystem and C compiler. I could care less if there were a ton of quirks or ugly hacks in the user interface, and indeed, I'd much rather that then the alternative: KDE, a well-architected, decently-coded steaming pile of sh!t that at best is an eyesore, and at worst is distracting in the extreme.\n\nDon't get me wrong - I *want* to switch from GNOME, but I won't as long as KDE is fsck%g ugly. So I ask if it's still fsck%g ugly. Is that too much to ask?"
    author: "Sniggly fox"
  - subject: "Re: Is 2.2 still ugly as sin?"
    date: 2001-08-22
    body: ">So I ask if it's still fsck%g ugly. Is that too much to ask?\n\nNo, It isn't ugly. IMHO :-).\n\n1. Mimetype icons are awesome (some app icons could be better though).\n\n2. Really nice widget sets available. You can also find the 'liquid' one at www.mosfet.org.\n\n3. Kwin is completely themable. It can import Icewm themes, which are nice."
    author: "ac"
  - subject: "KDE User"
    date: 2001-08-19
    body: "Thank you KDE developers!  \n\nKDE has come extremely far in such a short time. Don't listen to anyone's criticism for now.  Enjoy your accomplishment.  I have been using KDE as my only desktop for about a year and it's very usable.  Usable.  Just like software should be.  The rest of the desired bells and whistles people are mentioning are just icing on the cake.  I can wait for those.\n\nThanks again!"
    author: "Kovalid"
  - subject: "Re: KDE User"
    date: 2001-08-27
    body: "I totally Agree :\nForget about all the people that compare kde to windows , please if you dont like this great job use windows !!\nI cant believe the incredible work that this people do for us for a price of nothing.\nReally guys , why you dont create a monthly cds with themes , new applications from the Open source, etc to help your group working with some adittional money in your pockets ?\n\nCarlos Arana\nLima-Peru"
    author: "Carlos Arana"
  - subject: "Re: KDE User"
    date: 2001-08-28
    body: "> Really guys , why you dont create\n> a monthly cds with themes , new\n> applications from the Open source,\n> etc to help your group working\n> with some adittional money in your\n> pockets ?\n\nOr even an own Linux distribution suitable for daily home and office work?\n\nMaybe, you could even create an \"online distribution\", so you just download a short install routine (one boot floppy), which loggs onto the internet in order to download and install the desired packages.\n\nNethertheless, keep up your excellent work, I deeply admire you guys.\n\nBest regards,\n\nFlorian Gr\u00e4tz"
    author: "Florian Gr\u00e4tz"
  - subject: "Re: KDE User"
    date: 2001-08-28
    body: "> Really guys , why you dont create\n> a monthly cds with themes , new\n> applications from the Open source,\n> etc to help your group working\n> with some adittional money in your\n> pockets ?\n\nOr even an own Linux distribution suitable for daily home and office work?\n\nMaybe, you could even create an \"online distribution\", so you just download a short install routine (one boot floppy), which loggs onto the internet in order to download and install the desired packages.\n\nNethertheless, keep up your excellent work, I deeply admire you guys.\n\nBest regards,\n\nFlorian Gr\u00e4tz"
    author: "Florian Gr\u00e4tz"
  - subject: "code set in konqueror (1265)"
    date: 2001-08-19
    body: "i can't view arabic pages in konqueror when i brows an arabic site and set my encoding to Windows(1256) ... \nin KDE 2.1.2 it was fine and working but in KDE 2.2 is not viewing!\n\ni use SuSE 7.2 Pro\n\nthanks..\nSami"
    author: "sami"
  - subject: "Re: code set in konqueror (1265)"
    date: 2001-08-21
    body: "I can view arabic pages on KDE 2.2 RH7.1.\nCheck the fonts setting for konqueror (as a browser) and if that is still pointing to arabic fonts and codecs."
    author: "Kefah Issa"
  - subject: "Mosfets Liquid Theme"
    date: 2001-08-19
    body: "How do you install in KDE 2.2 final?\nI compiled it and did the make install, everything when OK , but I can't see any new stuff in the KDE control panel.\nNothing on the newsgroups either.\nafter the make install there is a little message that say dont forget to add /usr/local/kde/bin to your path, but there is not a bin dir in /usr/local/kde/ ?\n\nT"
    author: "Tony Caduto"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-19
    body: "I'm having the same trouble. Any tips for those of us less technical would be most appreciated!\n\nThanks. . . Scott"
    author: "Scott"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-19
    body: "I figured it out, you need to edit the paths to the KDE directories in the configure file before doing the make install.  Mofset has them set to his KDE install which is different than my Suse 7.2.   Anyway just change the paths and it will install perfect.  I really like it, it looks alot better than windoze.\n\nthe default paths are set to /usr/local/kde, and my actual kde2 instal was in /opt/kde2, the paths are near the top of the configure file.\nBasicly follow the install instructions, then after doing the ./configure make the path changes, then make install :-)\n\n\nTony Caduto"
    author: "Tony Caduto"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-19
    body: "or do \n./configure --prefix=/opt/kde2"
    author: "nq"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-19
    body: "Yep, this is correct, or set KDEDIR. AFAIK this is required for compiling any KDE software."
    author: "Mosfet"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-19
    body: "Desperatly need help compiling v0.5 on slack8:\n\n/bin/sh ../libtool --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I.. -I/opt/kde22/include -I/usr/src/qt-copy/include -I/usr/X11R6/include   @CPPFLAGS@\n@CXXFLAGS@  -c main.cpp\ng++ -DHAVE_CONFIG_H -I. -I. -I.. -I/opt/kde22/include -I/usr/src/qt-copy/include -I/usr/X11R6/include @CPPFLAGS@ @CXXFLAGS@ -Wp,-MD,.deps/main.pp -c main.cpp  -fPIC -DPIC -o .libs/main.o\ng++: cannot specify -o with -c or -S and multiple compilations\nmake: *** [main.lo] Error 1"
    author: "Bob"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2002-01-10
    body: "Slack8's versions of automake and autoconf don't play nice with the mosfet configure scripts...I dunno who is at fault but I just downgraded to the automake and autoconf packages from Slack7."
    author: "Chris"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-19
    body: "Oh, and BTW, Liquid 0.5 isn't automasking HTML widgets properly... I was using a khtml checkout done after KDE2.2, and it busted some of the code (automasking is pretty buggy in KHTML widgets to begin with). Thus the background around rounded edges shows through. I'm checking out the KDE_2_2_BRANCH (the official KDE2.2 release), and will fix this then release a 0.5.1."
    author: "Mosfet"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-19
    body: "Yes, I noticed that, but it still looks real good :-).  Even  with the background showing through it looks alot better than mozilla.\nI think liquid is the best looking theme for KDE2. It just has a elegant look and feel to it.\nKeep up the good work.\n\nTony Caduto"
    author: "Tony Caduto"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-21
    body: "Liquid is nice and slick but I find the background too noisy, and the normal kde background settings are ignored. Will there be better integration and fine grained settings for liquid in the coming versions?\nAlso would it not be nice to have the effects from liquid available to all themes so that not only C++ programmers is able to do cool effects (like being able to add transparency for menus in a themes editor).\nAlso I had to set --prefix=/usr for my Mandrake 8.0(using texstar prelink 2.2 RPM\u0092s) to pick up the theme, it would be nice if the readme informed about this.\nThanks!"
    author: "Fredrik Corneliusson"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-21
    body: "RTFM, background settings are not ignored ;-) If you read mosfet.org you'll see that this issue is noted, that the background wallpaper gets messed up when the style is first applied but you can reset it in KControl or it will fix itself the next time you log in. If you see a rendering error I suggest looking at the website first, since this *is* beta software. I'm working on fixing this for 0.5.1, but I want to release this today or tommorow because it has important fixes (HTML, KSCD, etc..). I *do* appreciate all new rendering error reports, tho! :) I usually take care of these pretty quickly.\n\nThere is already fine grained configuration for Liquid, at least as much as there is under regular KDE: colors, wallpaper, fonts are all done in the normal way. It's quite integrated, it's exactly the same! ;-)\n\nAs for configure, your going to have to set that when compiling any KDE software, AFAIK. If not let me know. I think Liquid is many user's first time compiling KDE software... Hopefully distributors will start making an RPM (I believe there is already an RH one). If they do and send it to me I'd be glad to put it on my site.\n\nAs for menu effects, all the styles I maintain and support will have them."
    author: "Mosfet"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-21
    body: "I made an RPM for RH71, you can grab the source RPM at http://www.opennms.org/~ben/kde2.2/kdeliquid-0.4-1.src.rpm if you want it.  Looking forward to the new release, I plan on packaging it up if you want RPMs for \"official\" release...  =)"
    author: "Ranger Rick"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-21
    body: "Sure! RPM's or other package formats would be much appreciated, both by the users so they don't have to compile it if they don't want to, and by me so I don't get the email ;-) I'd make RPM's for RH myself, but my system is significantly different (paths, glibc, compiler, kernel, X11, KDE, etc...).\n\nAnyone who wants to contribute packages please give me a mail and I'll put them up! :)"
    author: "Mosfet"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-21
    body: "Hi mosfet,\n\nI want to thank you for this wonderful style.\n\nThere's just one thing that bugs me: \nwhenever the button color is the same as the background color, the style automagicly chooses a default (other) color. Could this be modified to just accept the button color of the colorstyle (even if it's the same as the background color)\n\nThanx"
    author: "caatje"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-21
    body: "No, because it looks like crap ;-) Having a separate button color is important to the style since that is what draws all the \"blue\" components, but most color schemes don't have it set. Even more importantly, it's also used in mouse hover. I *may* change it to a color based off the background color, tho."
    author: "Mosfet"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-09-22
    body: "Hi, I used the Liquid Theme on an old box with KDE 2.2beta1 a while back. I recently built a new box, installed KDE 2.2.1 today and then remembered the \"missing\" (and the best) theme. Anyway, I managed to download and compile 0.5.1. It looks great :). Unfortunately, as soon as my mouse goes over the panel, it disappears. Then it comes back. But as soon as my mouse goes near it, it disappears again, permanently. Arse :).\n\nAny ideas? \n\nPS. Good job Mosfet - such a cool theme (when it works :) )."
    author: "Spoon Merchant"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-09-23
    body: "Posted a little too soon :). \n\nIt turned out to be the panel background picture was missing... Turned it off and my panel stayed put :).\n\nOne other thing - if I had animate menus selected in the style dialog, the translucent menus were all weird. Turned that off and now all is well :)..."
    author: "Spoon Merchant"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2002-09-19
    body: "I cant seem to be able to download his theme *anywhere*. Does anyone have the source code who can email it to me. His site is down and his tarball is no where else."
    author: "MozFet Site Down"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2002-07-09
    body: "What would be the correct prefix for Red Hat 7.3 with KDE 3.0?"
    author: "fern"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-20
    body: "Hello. I am running SuSE 7.1 KDE 2.2. When I run ./configure, it generates the following error \"Qt (>= Qt 2.2.2) (libraries) not found.\" I have installed QT 2.3.1. Any suggestions would be appriciated. Thanks."
    author: "Eb"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-21
    body: "Include apriopriate fragment of ./config.log file ( ./configure messages tells nothing, so nobody will tell you what is the reason ) and post it to the newest header on KDE.dot. I think that few people read this old messages."
    author: "Krame"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-22
    body: "Ups ! It looks like it is the newest header."
    author: "Krame"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-23
    body: "You need to issue ./configure --with-qt-libraries=/whatever/your/qt/path/is\n\nFor me it was /usr/lib/qt2/lib\n\nNow that I have that figured out, does anyone know why make install tells me:\n\nmake[2]: Entering directory `/usr/src/packages/SOURCES/mosfet-liquid0.5.1/kcmtransmenu'\n/bin/sh ../libtool --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I.. -I/opt/kde2/include -I/usr/lib/qt2/include -I/usr/X11R6/include     -O2 -fno-exceptions -fno-check-new -DQT_CLEAN_NAMESPACE -DQT_NO_COMPAT -DQT_NO_ASCII_CAST  -c transmenu.cpp\ng++ -DHAVE_CONFIG_H -I. -I. -I.. -I/opt/kde2/include -I/usr/lib/qt2/include -I/usr/X11R6/include -O2 -fno-exceptions -fno-check-new -DQT_CLEAN_NAMESPACE -DQT_NO_COMPAT -DQT_NO_ASCII_CAST -Wp,-MD,.deps/transmenu.pp -c transmenu.cpp  -fPIC -DPIC -o .libs/transmenu.o\nIn file included from transmenu.cpp:129:\ntransmenu.moc:45: new declaration `static void TransMenuConfig::staticMetaObject()'\ntransmenu.h:19: ambiguates old declaration `static class QMetaObject * TransMenuConfig::staticMetaObject()'\ntransmenu.moc: In function `void __static_initialization_and_destruction_0(int, int)':\ntransmenu.moc:29: no matching function for call to `QMetaObjectInit::QMetaObjectInit (QMetaObject * (*)())'\n/usr/lib/qt2/include/qmetaobject.h:259: candidates are: QMetaObjectInit::QMetaObjectInit(void (*)())\n/usr/lib/qt2/include/qmetaobject.h:261:                 QMetaObjectInit::QMetaObjectInit(const QMetaObjectInit &)\nmake[2]: *** [transmenu.lo] Error 1\nmake[2]: Leaving directory `/usr/src/packages/SOURCES/mosfet-liquid0.5.1/kcmtransmenu'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/usr/src/packages/SOURCES/mosfet-liquid0.5.1'\nmake: *** [all-recursive-am] Error 2"
    author: "Jon Tillman"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2001-08-23
    body: "Just a wild guess (I'm at work on a w2k machine and can't check it...):\n\nIf you have to specify the qt-lib-path for it to work (normally, you don't have to if your $QTDIR path is set correctly) maybe your \"moc-path\" isn't correct either? It could be that you pick up the wrong version of \"moc\" during compilation, and that is sure to mess things up... \n\nI have $QTDIR, $KDEDIR and $MOCPATH (not sure if this is the correct name, but can't check it right now) all set correctly and the only thing I normally  add to ./configure is --enable-final (which sometimes works and some times doesn't...)"
    author: "jd"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2002-05-22
    body: "hi, i'm having trouble getting the theme to work too.  i did ./configure; make install; just fine.  then i added /usr/local/kde/bin to my path just fine.  rebooted, and looked in the Look and Feel | Colors and nothing.  I can't find a trace of liquid in any of the KDE control screens.\n\nalso, one more thing, after i finally do get liquid working what do i do with the Liquid-0.1.kcsrc file??\n\nthanks in advance,\njonathan"
    author: "jonathan"
  - subject: "Re: Mosfets Liquid Theme"
    date: 2002-05-24
    body: "oops.  i guess it pays to watch the dates of previous posts and not just reply to them because they seem relevant.  sorry for the screw up."
    author: "jonathan"
  - subject: "No feedback in KHTML"
    date: 2001-08-19
    body: "Konqueror is great and getting better all the time, but it seems to be lacking feedback when you click on a link or open a new page.\nIf you click on a link that is slow, you don't know if anything is happening, it would be nice to have the cursor change to a hour glass or something.\nIs there something that I missed and just don't have it enabled?\nComments anyone?\n\nTony Caduto"
    author: "Tony Caduto"
  - subject: "Re: No feedback in KHTML"
    date: 2001-08-20
    body: "Just look for the spinning Konqueror logo.  (Much more reliable than the IE globe, which doesn't spin sometimes)"
    author: "not me"
  - subject: "Re: No feedback in KHTML"
    date: 2001-08-20
    body: "Sometimes the konqueror logo isn't spinning either although a link was pressed (and a new page comes up in a few seconds). Haven't seen any concistency in this behaviour yet so I'm not sure where the problem could be. Personally I'd like to some more feedback - it adds a lot to perceived speed as well (take a look at opera, it seems very fast because of the timers starting off every time etc etc)."
    author: "Jelmer Feenstra"
  - subject: "Desktop Icons"
    date: 2001-08-20
    body: "My problem probably has an easy solution but I am frustrated. I want to organize my desktop icons any way I want. But, as soon as I move an icon, kde reorganizes it. I've tried setting the desktop  icons in the control center organized as vertically, horizontal and I've kept both unchecked -- no luck. Suggestions? Thanks"
    author: "tweet"
  - subject: "Re: Desktop Icons"
    date: 2001-08-20
    body: "log out and log in again. Second time I logged in I could reorganize them myself.\n\nDanny"
    author: "Danny"
  - subject: "Re: Desktop Icons"
    date: 2001-08-20
    body: "Known bug, it is being worked on.\n\nAlex"
    author: "aleXXX"
  - subject: "Soooo maaannny crashes!"
    date: 2001-08-20
    body: "After upgrading to KDE 2.2 on Mandrake 8.0.\nI faced 15 crashes within an hour! Ugh!\nHitting refresh in Konqueror crashes,\nenabling java in the browser crashes,\nscreen savers crash, kdeinit dumps core\nregularly, and so many apps... the list goes\non. Arrgghh! I'd better use Windows (it will\ncrash 4/5 times, but in a day, not in a single\nhour).\n\n- Anonymous coward"
    author: "Anonymous Coward"
  - subject: "Works for me....."
    date: 2001-08-20
    body: "Strange that you should get so many crashes.\nIn general I think that KDE is very stable, and that a crash like you have is caused by configuration problems (wrong glibc, compiler, etc...)\n\nDid you update KDE from the official KDE RPMS for Mandrake 8.0, or did you use some other ones (for example the ones from cooker or RedHat?)\n\nI have upgraded to KDE 2.2 on mandrake 8.0 (using the \"official\" rpms for mandrake 8.0 on my \"clean\" 8.0 system), and I have not experienced any problems"
    author: "Erlend B\u00f8e"
  - subject: "Re: Works for me....."
    date: 2001-08-20
    body: "Yes, it is so unfortunate.\n\nI updated the fresh Mdk 8.0 installation with the \nlatest updates from Mandrake (including the glibc\nupdate).\n\nThen I used the official KDE RPMs for Mdk 8.0.\n\nI have a iPIII 1GHz system on i810E mboard,\n512 MB RAM, 20 GB HDD, running Mdk 8.0 system\non ReieserFS 4GB partition, 512MB swap.\n\nKDE first refused to run and the kernel\n(2.4.3mdk-up) spit out an 'oops' during the\nKDE startup.\nI had to then upgrade the kernel to 2.4.8\n(recompiled from sources and installed).\n\nDo I need to rebuild KDE 2.2 from sources\non my system?\n\n- Anonymous Coward"
    author: "Anonymous Coward"
  - subject: "Re: Works for me....."
    date: 2001-08-20
    body: "Do you install the RPM with --nodeps option ?\nIf yes you can :\nreinstall the RPM with\nrpm -i --force kde*\nand try to resolv the dependencies problem \n\nSome dependencies are not important,\nwhile others can cause big instability.\n\nregards"
    author: "thierry"
  - subject: "Re: Works for me....."
    date: 2001-08-20
    body: "Do you install the RPM with --nodeps option ?\nIf yes you can :\nreinstall the RPM with\nrpm -i --force kde*\nand try to resolv the dependencies problem \n\nSome dependencies are not important,\nwhile others can cause big instability.\n\nregards"
    author: "thierry"
  - subject: "Re: Works for me....."
    date: 2001-08-20
    body: "unrelated but due to a bug in 2.4.8 and 2.4.9\nyou will see bad performance (and this affects kde a lot). you can use 2.4.8-ac7, or wait \nuntil the bug will be fixed."
    author: "ik"
  - subject: "Hardware problems?"
    date: 2001-08-26
    body: "With crashes like that, I'd suspect faulty hardware, possibly RAM?  I suspect hardware because crashes occur in both operating systems."
    author: "WereCat"
  - subject: "Re: Hardware problems?"
    date: 2002-09-25
    body: "Well it could be your ram. How much ram do you have? Or it could be hardware. But if you have 128 megs of ram or above its probably hardware. \n\nHave you ever had pc troubles with software ect... Come to my site and post for free and you dont even have to become a member. SO stop on by. This url is. http://denim.bbboy.net/pcdr"
    author: "Jay"
  - subject: "No Keyboard When Boot and KDM Starts"
    date: 2001-08-20
    body: "but if I restart X server under KDM in the pull down menu and KDM starts up again, the keyboard works. Any suggestions on getting the keyboard to work the first time? thanks"
    author: "flyher"
  - subject: "Any Suggestions?"
    date: 2001-08-23
    body: "I remain stumped."
    author: "flyher"
  - subject: "Re: Any Suggestions?"
    date: 2001-08-24
    body: "I've found that with some Microsoft mice, you need to stop GPM from running before you launch X otherwise you get the same problem that you've mentioned.\n\nHope this helps."
    author: "Si"
  - subject: "Re: No Keyboard When Boot and KDM Starts"
    date: 2004-12-15
    body: "I have the same problem with the difference that a X server restart doesnt help, but choosing console login in the pull down menu does. The keyboard works in the console and also starts to work in KDM after the return from the console.\n\nSuggestions ?\n\nRegards\n_______\nBansaie"
    author: "Bansaie"
  - subject: "Re: No Keyboard When Boot and KDM Starts"
    date: 2005-02-11
    body: "I have a similar problem that started after I upgraded to KDE 3.3.2.  Just like this, I have to login to the console then let it timeout back to KDM, then the keyboard works.\n\nLet me know if you find of solution.  It is kind of annoying."
    author: "Chris Pierson"
  - subject: "Re: No Keyboard When Boot and KDM Starts"
    date: 2005-02-19
    body: "Problem solved!\n\nUnder [General] in /etc/kde3/kdm/kdmrc add the line:\n ServerVTs=-7\n\nOr run \"apt-get -u --reinstall install kdm\" and choose to replace the current configuration file with the one in the package.\n\nHope this is of any use for others too.\n\nRegards\nBansaie"
    author: "Bansaie"
  - subject: "Re: No Keyboard When Boot and KDM Starts"
    date: 2006-05-07
    body: "The keyboard can also appear unresponsive, to the impatient, when slow keys is turned on.  KDE Control Center >> REgional & Accessibility >> Keyboard Filters >> Slow Keys."
    author: "Henry Kroll"
  - subject: "Re: No Keyboard When Boot and KDM Starts"
    date: 2005-01-10
    body: "Solution:\nAdd \nServerVTs=-7\nto\n/etc/kde3/kdm/kdmrc\n\nA longer description can be found at:\nhttp://lists.debian.org/debian-qt-kde/2004/10/msg00486.html\n\nGreetings, Philipp"
    author: "Philipp Spitzer"
  - subject: "Usability must be priority in kde 3"
    date: 2001-08-21
    body: "Great works by kde team. Kde 2.2 has came with all its promises. Now for kde 3, put usability as one of the priorities. \n\n-deman"
    author: "deman"
  - subject: "My experiences with KDE 2.2"
    date: 2001-08-21
    body: "I've used GNOME for a year or two now and decided to try out KDE since the screenshots look nice and the KDE website promised a lot in this new release.  I had tried KDE 2.0 but never got Konqueror working properly so I deleted the installation.  Here's my first impression of 2.2:\n\n1. Upon installing the binary packages and before rebooting, the kwm window manager ran super slow.  It'd take several seconds before clicking a window's title and before the window actually moved.  I then changed the theme of my desktop-- the window border style changed but oddly the background didn't.  So I install libasound and reboot.\n\n2. Once rebooted kwm worked fine-- probably some library/ldconfig problem.  Now the background was the one specified in the new theme, but I didn't like it.  Ok, so everything seems to be working fine now.  The menu fade in stuff was annoying, especially since it was flickery and not smooth like Win2k.  Alright, so there's probably a way to turn it off.\n\n3. I tried changing my background image.  The first time I clicked the \"Browse...\" button, it popped up a dialog box with an image preview and filenames were sorted in case-sensitive order.  A much nicer looking file browse dialog box compared to GNOME's.  I changed directories and selected an image and hit Ok.  It turned out I didn't like the background I selected, so I hit the \"Browse...\" button again.  The dialog box popped up but it was pointing to the old directory, so I had to renavigate back to the background directory.  That was annoying.  Secondly, this time the dialog box didn't have an image preview and filenames were sorted in case-insensitive order (wtf?).  I don't know if I want to trust KDE for my data if it can't pop up a dialog box in a predictable way.\n\n4. aRts builder was disappointing.  I tried following the tutorial for creating a network, but artsbuilder crashed when I tried to run it.  Sigh.  So much for thinking it would replace Buzz.\n\nWell KDE has a lot of promise, but even though the version is a .2, it seems like a .0"
    author: "Hypothalamus Cow"
  - subject: "Start Application Menu doesnt go away"
    date: 2001-08-21
    body: "OK.   First things first.  I _love_ KDE 2.2.  I compiled all the source for it on my nifty Sony Picturebook and it works great, except for 1 little annoying bug.\n\nAfter I click on the Start Menu, there are only (2) ways to get it to dissapear.\n\n1)  Click on a link contained in the start menu\n2)  Click on the \"K\" icon at the base of the menu.\n\nI cant just click outside the menu to make it go away.  Is there some way I can fix this?  I've heard a rumor that it is cause by a bug in qtlibs.\n\nThanks,\n\nThinker"
    author: "Thinker"
  - subject: "Re: Start Application Menu doesnt go away"
    date: 2001-08-22
    body: "The rumor is true.  QT has a bug.  qt-copy from KDE cvs has the fix, but I don't think any Trolltech-released version does yet.  You might try changing widget styles, I have heard a different rumor that says it depends on what style you are using."
    author: "not me"
  - subject: "KDE configurator"
    date: 2001-08-21
    body: "We only need a hardware configuration-instalation KDE app into Kcontrol and a system adm (like ximian setup tools). I hope things like this will make KDE compite with windows, that is an operating system. I hope. Could be it posible? with scritps under the app that could be maintain by the distro."
    author: "Daniel"
  - subject: "Re: KDE configurator"
    date: 2001-08-22
    body: "There was a project to make a KDE frontend to the Ximian setup tools (the backend is completely independent of GNOME).  However, the last post on the mailing list was July 15.  Despite the valiant efforts of Charles Samuels, I don't think the project is still alive.  (I guess that person from Ximian who didn't want to move to joint CVS was right, huh?)"
    author: "not me"
  - subject: "A little disappointment"
    date: 2001-08-22
    body: "Guys (and gals ;),\n\nI'm a little disappointed in the 2.2 release. The ones before were better. But maybe I messed things up - you tell me. (I'm using the Debian unstable builds) Let me explain.\n\na) Konqueror does not do Javascript if called via kfmclient (ie Alt-F2, click on your home icon, etc). If I call it via the konqueror binary it works. How in the world could such a blatant show stopper bug be missed? It wasn't there in 2.2beta1.\n\nb) The objprelink process may be a nice thing in theory but here it hasn't improved speed at all. (The Debian binaries are supposed to be built with objprelink.) \nI have a K6-2 500MHz and each Konqueror window still needs about 4-5 seconds to appear, during which the CPU meter is fixed at 100%. When a popup, well, pops up, then 2-3 seconds pass with half-redrawn konqueror windows. \n\nc) Neither has the startup time increased. (about 30 seconds) I already removed the Netscape plugin search. The whole GUI also still seems a little sluggish.\n\nd) The printing system is very nice, and I really LOVE the configuration utility - but it cannot cope with password-asking CUPS servers yet (I get an endless number of password prompts, and then it often crashes). This is also a bug that I would call obvious.\n\n\nThese are just some things that I noticed after the upgrade. Most of the time it's the speed, but I simply don't see why a 500MHz machine with 448MB of RAM should be too slow for desktop use. I'm not claiming Gnome is faster or whatever, there are just my current feelings about KDE.\n\nPlease enlighten me, what am I doing wrong?"
    author: "Jens"
  - subject: "Re: A little disappointment"
    date: 2001-08-22
    body: "about the objprelinking and speed stuff:\n1)delete your old .kde directory, you probably will see an improvement in speed.\n2) The bottleneck _on your specific system_ may very well be not the relocations of symbols (done by the CPU) but the loading of apps + data from disk. This is way YMMV with objprelinking.\n\nD."
    author: "Danny"
  - subject: "Re: A little disappointment"
    date: 2001-08-24
    body: "Hi,\n\nthanks. to 1) this might be important. When I updated to 2.0 final from the beta builds my .kde dir was about 200 MB (!!!) from old cache files, .nfs* files, etc etc etc \n\nbtw: my $HOME is on NFS but that doesn't make a difference, I already checked.\n\nto 2) I have a 4G IBM UW-SCSI disk that does about 13MB/sec according to hdparm, and between 1 and 8MB/s according to bonnie++. This disk is about 4 years old, and just about the fastest that would work on my motherboard - it only does DMA-33 for IDE disks. \n\nBut I would think the disk I/O performance should be enough.. ?"
    author: "Jens"
  - subject: "Re: A little disappointment"
    date: 2001-08-23
    body: "One problem is that you have more than 128MB memory. Rest of memory won't be cached because your motherboard doesn't have enough cache  (512KB?). In LKML someone reported that kernel compilation time increased from 9min->13min when he increased memory from 128->256!!"
    author: "Jaana Kunttu"
  - subject: "hdparm?"
    date: 2001-08-24
    body: "Have you installed and setup hdparm?\n\nmake a huge difference on my computer and i'm running the same binaries as you.\n\nI have a k6-500 128MB and konq. takes around a second to load."
    author: "Paul Hensgen"
  - subject: "Re: hdparm?"
    date: 2001-08-24
    body: "Actually, I use SCSI harddisks. :-)\n\nBut thanks for the info. I use hdparm whenever it makes sense (ie. with IDE)."
    author: "Jens"
  - subject: "Re: A little disappointment"
    date: 2001-08-24
    body: "Yeah, like the other people said, check in your mainboard's manual how large the \"Cacheable area\" of your mainboard is. On most K6-2 boards, it's just 64 MBs, and adding more memory doesn't make it faster. Newer Socket-7 boards might have a larger Cacheable area (128 MB?) but still, the amount of RAM you have is more suited to Slot-1 or better chipsets, which usually have a cacheable area of >=512 MB. \n\nHope this helps. (KDE 2.2 is *still* not exactly fast. Especially initial startup. I am forced to use icewm on an old PC (K6-2 333, *cough*, \"old\") just because it's as fast as Win95, while KDE2 just takes too long (although it has way more features). On my desktop box (Duron-800, 256 Megs RAM), it's definitely usable though.\n\nThe hdparm trick (enabling DMA or better Ultra-DMA) is a good one, too. Also, try both with 2.2 and later 2.4 kernels. The 2.4 series had (still has!) some issues on the Virtual Memory balancing side AFAIK. \n\nI'll try copying $HOME/.kde to a ramdisk on system startup to improve system speed, that's my last real idea for making initial startup faster - dunno if it'll work. \n\nAfter that, it's optimizing the startup source code itself.\n\nHave fun,\n\nYours Arondylos #8-)"
    author: "Arondylos"
  - subject: "Re: A little disappointment"
    date: 2001-08-24
    body: "Yes, On my machine it takes more time to start KDE ( after login ), than to start Linux/X/kdm. \nI have some old K6 motherboard, but I can't find any \"Cacheable area\" in manual."
    author: "Krame"
  - subject: "Re: A little disappointment"
    date: 2001-08-24
    body: "ok, have you already tried the other stuff (except the weird ramdisk idea)? \n\nanyway, do you know the board vendor or what kind of chipset is on the chip? (you might find that out by looking on the board), maybe an Intel 430 TX or something? \n\nhttp://www.wimsbios.com/ might also be interesting to find out your Board vendor/Chipset.\n\nStill, KDE *is* rather slow, _especially_ on initial startup. Usually, I startup KDE just once per day - since I don't usually have to reboot. That is rather wasteful energy-wise, but the computer has to run anyway here so it doesn't matter. It's just an ugly workaround though :)"
    author: "Arondylos"
  - subject: "Re: A little disappointment"
    date: 2001-08-25
    body: "I've tried:\n- wiping .kde\n- moving .kde to local disk (before #1 :)\n- a different SCSI host adapter\n- building KDE from source (OK, that was 2.1)\n\nNot much change. My mainboard chipset is an ALI M1533 or something like that. \n\n\nThanks a lot for all the insight! :)"
    author: "Jens"
  - subject: "Re: A little disappointment"
    date: 2001-08-25
    body: "I suppose you have an Ali-5 chipset, unfortunately Ali doesn't really provide generic chipset information (or I couldn't find it). A vendor which uses this chipset is Asus, and their board supports 128 MB with 512 kB Cache; if you have just 256 kB second Level cache, your cacheable area will likely be at 64 MB. \n\nSo, verify that you have 512 kB cache and try ripping out RAM so you only have 128 MB. This *might* not improve startup time too much (since at startup, memory fills up from below AFAIK, and even with KDE fully loaded you might not yet be at >128 MB) but interactive speed, hopefully.\n\nAlso, what kind of SCSI adapter do you have?\n\nDo note that I'm just another non-developer, I haven't even read the KDE startup source code yet...\n\n-Arondylos"
    author: "Arondylos"
  - subject: "TaskBar... 2 states?  There should be 3..."
    date: 2001-08-22
    body: "There are always 2 states on the Taskbar: Normal, and darkened...  Things that are the active view are darkened, while everything else looks normal...\n\nHowever, there are three states:\n\nActive, Open, and _Minimized_\n\nI suggest that we highlight the active state, and darken the minimized states...\n\n(I do realize that the \"darkened\" are really depressed... However, still rest my case)...  as with would more properly follow the scheme, and help you now just what windows arent currently open, when you have alot of different things open...\n\nHowever, I do find interesting the colapsing of items by type that currently exists...\n\nBut still, find this feature an easy and good one...\n\nThanks,\nGregory W. Brubaker"
    author: "Gregory W. Brubaker"
  - subject: "Why"
    date: 2001-08-22
    body: "don KDE uses components from OpenOffice ? There are lot of great components 100% profesional code. It will do KDE going faster .. we want an \"excel\" ? well .. we take the OpenOffice component, we make a port and we have the apps. Its is faster than code it from down. Why reinvent the weel ? OpenSource is based on it, i mean, use the code of other coders."
    author: "Daniel"
  - subject: "Re: Why"
    date: 2001-08-22
    body: "Have you ever looked at the OpenOffice code ?\nIt would be much more work to clean & fix the code, understand the OpenOffice libs and port it to QT than enhance our current KOffice project."
    author: "Cullmann Christoph"
  - subject: "Re: Why"
    date: 2001-08-22
    body: "I am talking about pieces of OpenOffice. Not the whole of it. Only some important components like the print support for example."
    author: "Daniel"
  - subject: "Re: Why"
    date: 2001-08-24
    body: "This is completely rediculous. Mainly for 2 reasons:\n\n1) For the printing, we do have equal or even better code in place alreasy\n\n2) For the visual stuff: Using many widgetsets is a very bad idea. You want to have not more than one (and that is Qt) at one time.\n\n3) Qt provides an API that makes stuff easier to code than using native C++. \n\nSo what you basically want is having a look into the other Office Suites code and copy wherever it make sense. \n\nbtw: We already do this kind of code sharing with the ABI Word authors (dunno about OO)."
    author: "Daniel Molkentin"
  - subject: "What about Star/OpenOffice's MS Office filters?"
    date: 2001-12-13
    body: "I keep hearing, and am confident in my own right, that the single biggest obstacle to end-user / desktop / workstation *nix adoption these days is the lack of confidence in ability to retain old data, i.e., Microsoft Office files, and to exchange files with MS Office users.\n\nThe word just doesn't seem to have gotten out there that StarOffice/OpenOffice has good filters.  Perhaps if KOffice imported SO/OO's MS Office filters this would change.  Would that have similar techinical obstacles the ones you described above, or is that code more portable?\n\nBest of all worlds would be if KDE, GNOME, and others cooperated with Sun and the OO developers on common, portable MS Office filter code, or at least frequently and closely worked together on this issue, sharing info, etc.\n\nReally, a near-maniacal focus needs to happen on MS Office filter development.  That is far more important than adding KOffice feature #185, or even development of KDE 3.0.  There are many nice-to-haves; this is a must-have.\n\nThe single best thing you could possibly do for *nix is to have *nix-wide high-quality, confidence-worthy MS Office file filters and import/export capability as soon as possible.\n\nDataViz Corporation has accomplished this on a commercial basis for MacOS with MacLinkPlus.  Thanks to it, Mac users can just use AppleWorks or other apps instead of MS Office for Mac, and can save files in a dizzying array of formats from virtually any application.\n\nIf a commercial company can do usable and commercially viable MS Office filters, I think open source developers need to rise to that challenge.   After all, isn't the whole point of open source that you guys can do better than commercial developers?  And, what more important feature could there possibly be for the future of *nix than MS Office file filters?"
    author: "Leo"
  - subject: "What about Star/OpenOffice's MS Office filters?"
    date: 2001-12-13
    body: "I keep hearing, and am confident in my own right, that the single biggest obstacle to end-user / desktop / workstation *nix adoption these days is the lack of confidence in ability to retain old data, i.e., Microsoft Office files, and to exchange files with MS Office users.\n\nThe word just doesn't seem to have gotten out there that StarOffice/OpenOffice has good filters.  Perhaps if KOffice imported SO/OO's MS Office filters this would change.  Would that have similar techinical obstacles the ones you described above, or is that code more portable?\n\nBest of all worlds would be if KDE, GNOME, and others cooperated with Sun and the OO developers on common, portable MS Office filter code, or at least frequently and closely worked together on this issue, sharing info, etc.\n\nReally, a near-maniacal focus needs to happen on MS Office filter development.  That is far more important than adding KOffice feature #185, or even development of KDE 3.0.  There are many nice-to-haves; this is a must-have.\n\nThe single best thing you could possibly do for *nix is to have *nix-wide high-quality, confidence-worthy MS Office file filters and import/export capability as soon as possible.\n\nDataViz Corporation has accomplished this on a commercial basis for MacOS with MacLinkPlus.  Thanks to it, Mac users can just use AppleWorks or other apps instead of MS Office for Mac, and can save files in a dizzying array of formats from virtually any application.\n\nIf a commercial company can do usable and commercially viable MS Office filters, I think open source developers need to rise to that challenge.   After all, isn't the whole point of open source that you guys can do better than commercial developers?  And, what more important feature could there possibly be for the future of *nix than MS Office file filters?"
    author: "Leo"
  - subject: "All well and good, but..."
    date: 2001-08-22
    body: "Look, I a a newbie. I am so new I still have the tags hanging offa me. So new I still haven't gotten the clearcoating. I'll admit it. There, I said it. Good to get that out of the way. Send the smarmy remarks to any device you want, except one of mine.\n\nHaving said that, I have tried to install 2.2. Ghod knows I have tried. Keerist, peeps, can there not be an easier way to install something?\n\nI am not without \"certain technical skills\", or a certain degree of modest intelligence and capability to learn new concepts, but installing 2.2, for me, is a non-starter. I have tried 2 different distros, to get it isntalled on Mandrake 8.0, and both times it wanted stuff that was presumably not here and I was unable to find. Gah. Not fun, considering the DL times at 50.7 connect speeds(I live very much in the broccoli...).\n\nSo, my question is: All this geek stuff is great for whiling away the empty hours of the day, and certainly better for you than hanging out in some dark and sleazy barroom, but can there not be a better way for the less geekoidal amongst us to Make Things Work?\n\nJust asking..."
    author: "Skal Loret"
  - subject: "Sure"
    date: 2001-08-22
    body: "First: Wait a few days unti\u00f6l the rough edges of packages the worst bugs and so have gone. Then download all .rpms for your KDE _supporting_ distribution (SuSE, Debian, Mandrake,...).\n\nrpm -Uvh *.rpm\n\nIf some dependencies are not met, use rpmfind.net to find out which package might be missing and install it. NEVER install packages not for your distribution.\n\nNever use --nodeps unless you have heard from a very knowledgable person that it is okay.\n\nAfter that ask on newsgroups for help, if you face problems."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Sure"
    date: 2001-08-24
    body: "I think the point the new user was trying to convey is that fact that why the heck does installing things on Linux have to be so darn complex.  I've been using Linux off and on for about 5 years, I'm no expert, but I can feel my way around pretty well.  I can't even get KDE 2.2 to run on my Mandrake 8.0 box either. I've done everything you said to do, and still nothing, I've uninstalled 2.1.2, and tried to reinstall...nothing.  Nothing but conflicts, and dependancies.  I personally don't have all day to search for the libraries needed, it's a waste of my time and other users as well.  Suffice to say, that's one of the benefits of a Winblows box, you just click setup and away you go, and in less than 2-3 minutes your using your new software.  Until installing programs is THAT easy, newbies like the author, and even non-newbies like myself will only play with Linux and never adopt it.  BTW, those that think urpm saves the day, think again...it has it's limitations.  One file, one download, one install, that's what is needed to make Linux work for everyone.  There are great products out there, too bad half the Linux users will never get to see them due to their pain in the butt installation dependancies and conflicts.\n\nSage"
    author: "Sage"
  - subject: "Re: Sure"
    date: 2001-08-25
    body: "Yes, it could be easier, but \n\n>One file, one download, one install, that's what is needed to make Linux work for everyone.\n\nIf you think so, you have to wait kde2.2 to be in the official package of your distribution.\n\n>that's one of the benefits of a Winblows box\n\nwindows never allow's you to download fifty percent\nof the system (kde+libraries) and install it on the old \nfifty percent (kernel+libraries...)\n\n>half the Linux users will never get to see them...\n\nJust wait 4 month and you have it in the next \nrelease of your distribution.\n\nregards"
    author: "thierry"
  - subject: "Re: Sure"
    date: 2001-08-25
    body: "I can understand the author's frustration, and I completely agree with Sage. While a lot of Geeks continue to insist on the virtues of linux, the rest of us simply don't want to spend all day tyring to set up something that takes 5 minutes to do on Windows or Mac. We have families, or other interests (like reading or writing), or simply like to spend time outside on a nice day.\n\nI just hope that linux can became easier to use for the average user. Let me just add that I really do support the concept of linux. It just doesn't seem practical for people. Not yet, anyway.\n\n(Even as I type this, I have problems. My delete key is not working.)"
    author: "Paul"
  - subject: "Still disappointed in klipper"
    date: 2001-08-22
    body: "Klipper is a great tool - but it could be so much better. It should:\n\n1) Purge duplicate strings: if I highlight \"qwerty\", hl \"foobar\" and hl \"qwerty\" again I get two copies of \"qwerty\" in the list taking up two slots. Just have unique strings in the list.\n\n2) Promote most recent string to front. It is logical that if I choose a string, I'm more likely to use it yet again. Move it to the front to reflect this.\n\n3) Let user choose history size. Maybe some of us want to keep 20 or 25 strings around.\n\nThese would make klipper REALLY COOL."
    author: "G. Brannon Smith"
  - subject: "Re: Still disappointed in klipper"
    date: 2001-08-23
    body: "You didn't test the one from KDE 2.2 yet, did you?"
    author: "Carsten Pfeiffer"
  - subject: "kde, icons, etc"
    date: 2001-08-22
    body: "KDE 2.2 just rules. It's clean, elegant, sophisticated, and easy to use.\n\nOnly one problem - my icons will not stay WHERE I PUT THEM - every time I log out and log back in, KDE rearranges my icons for me. :P *I* want to decide where my icons should be when I login!\n\nAs soon as I figure out the solution to that irritation, however, I would say KDE 2.2 has no flaws apparent to me. :)"
    author: "J Pruitt"
  - subject: "Re: kde, icons, etc"
    date: 2001-08-22
    body: "I updated my suse 7.2 with the rpms from ftp.suse.com and I don't have that problem you speak of.\nUse the command line version of Yast and get KDE 2.2 that way.\n\nTony"
    author: "Tony Caduto"
  - subject: "icon arrangement fix"
    date: 2001-08-23
    body: "Hello!\n\nI've heard this will do what you are looking for:\n\nUncheck kcontrol->Desktop->\"Align Icons Vertically\".  Icons will then be laid out horizontally, but you can put them where you want and they will stay."
    author: "mikecd"
  - subject: "Re: icon arrangement fix"
    date: 2003-01-25
    body: "Where is this kcontrol? Where do you find this exactly? Can you give more details on this? Right click or something?\nTHanks\n\n"
    author: "Justin"
  - subject: "Re: kde, icons, etc"
    date: 2001-08-23
    body: "I have no idea if this always helps, but it did it for me:\n\nopen konsole.\n\nKill the kdesktop process:\n#kill -HUP <kdesktop-pid>\n\nRestart the kdesktop process:\n#kdesktop\n\nAfter this, no more annoying automatic rearranging of icons for me. Very strange, I must admit, because before I did this, restarting KDE didn't help. But it doesn't come back, even after restarting KDE... :)"
    author: "jd"
  - subject: "Re: kde, icons, etc"
    date: 2001-08-23
    body: "Thanks for the tips. :)\n\nUnfortunately, neither disabling \"vertical alignment\" in the control panel, nor killing and restarting the kdesktop process, works for me.\n\nOh well. I guess I'll wait for 2.2.1. :P\n\nFWIW, I'm running FreeBSD-STABLE (built KDE from ports)."
    author: "J Pruitt"
  - subject: "Re: kde, icons, etc"
    date: 2001-08-26
    body: "I loged out, went to a Textconsole via STRG-ALT-F1 and deleted anything belonging to me in /tmp. When I loged into kde next time my desktop looked as I wanted it."
    author: "Andreas Silberstorff"
  - subject: "Window placement problem"
    date: 2001-08-22
    body: "I like KDE a lot, but there's a really annoying problem in this latest version.\n\nI'm using SuSE 7.2.\nI downloaded all the files from the sourceforge.net and installed them with\nrpm -Uvh --nodeps --force *\n\nThe problem shows when i start an app maximised. The position of the window doesn't align with the screen. KDE2 apps are a bit wider than the screen. Netscape (old one) appears at an offset of about 10x10 pixels. This is _extremely_ annoying.\n\nWhat's the problem?"
    author: "Cihl"
  - subject: "Re: Window placement problem"
    date: 2001-08-24
    body: "the problem can be solved this way :\nmaximize the window, or place it right, and then right click on the title bar , and click on 'store settings'.\nenjoy !"
    author: "ik"
  - subject: "System recommendation for KDE 2.2 ?"
    date: 2001-08-23
    body: "Could someone post a recomended system requirement for a stable, full-featured and fast KDE 2.2 (kernel, glibc, compiler, other libraries, tools etc.)\n\nI've got full-featured but not quite stable KDE 2.2, the speed is fine with PIII 750@825MHz 192MB and Athlon 900@945MHz 128MB, but horrible with celeron 266MHz 64MB (please don't say hardware upgrade).\nI think it's more stable when I'm using Redhat 6.2+KDE 2.1.x, but now I'm using Mandrake 8.0.\n\nSo if someone could post a recommended a stable system, I'll upgrade right away.\n\nOh yeah, does Mandrake 8.0 RPM relocatable ? I usually install KDE on a different partition.\n\nTIA"
    author: "2WhyNo"
  - subject: "Re: System recommendation for KDE 2.2 ?"
    date: 2001-08-24
    body: "Recommended fast and stable system for KDE:\nSlackware 7.0 on K6/200 128 MB :-)\n\nAlex"
    author: "aleXXX"
  - subject: "Re: System recommendation for KDE 2.2 ?"
    date: 2002-09-24
    body: "hmm......so cld i get away with running x on slackware with 16 ram ??........and if i do change the config ltr, will i need to reinstall slack??"
    author: "day"
  - subject: "Re: System recommendation for KDE 2.2 ?"
    date: 2001-09-01
    body: "I don't think your hardware power has anything to do with the stability. Look above for all sorts of horror stories about Mandrake RPMs. I would reccomend that you compile from source, shouldn't take long on either of the first two systems! Just make sure you compile qt from source as well."
    author: "Carbon"
  - subject: "Minor KDE annoyance"
    date: 2001-08-23
    body: "After many hours of DL'ing across my ancient modem out here in the sticks, I installed the SuSE specific RPMs of KDE2.2, and for the most part, it rocks. However, there is one little problem that is threatening to make the whole thing an also-ran for me. Every 30 seconds or so, the screen pauses for about a second. No mouse or keyboard input, though it does get buffered and drawn when the pause is over. Perhaps this is a known bug? Need a little help here.\n\nVital Stats: \nKernel 2.2.14\nKDE 2.2\nX-Server 3320 11.0"
    author: "Jon Tillman"
  - subject: "Re: Minor KDE annoyance"
    date: 2001-08-24
    body: "Perhaps you have the screen saver feature enable?? If so, you will find that it doesn't blank the screen. The cursors hide and everything is suspended but you still have a \"normal\" screen otherwise. This is an error and there is a fix.  Check out the updates for 7.2.  I see you have a modem so using Y.O.U. may or may not be a favorable idea. I have a cable modem and the Y.O.U works great.\nTake care,\nChuck"
    author: "Chuck Taylor"
  - subject: "kde 2.2 not running in my red hat 7.1"
    date: 2001-08-24
    body: "I run kde2.1 using red hat 7.1.\ndownload the new kde2.2 (all rpm)\nand then instal it using\nrpm -ivh --force --nodeps\n\nthe kde 2.2 not run, the messages are\nwhen I run kdevelop (from gnome):\n\nkdevelop: error while loading shared libraries: libfam.so.0: cannot load shared object file: No such file or directory\n\nwhen I reinstal the kde2.2 using\nrpm -Uvh *.rpm\nthe messages are the lib problens:\nlibSDL-1.2.so.0\nlibfam.so.0\nefax \u00e9 necess\u00e1rio pelo kdeutils-2.2-2\nlibxml2.so.2   \u00e9 necess\u00e1rio pelo kdelibs-2.2-5"
    author: "andre bueno"
  - subject: "Re: kde 2.2 not running in my red hat 7.1"
    date: 2001-08-25
    body: "It never ceases to amaze me how people install RPMS --force --nodeps and are surprised when things don't work."
    author: "ac"
  - subject: "www.rpmfind.net - Use it!"
    date: 2001-08-26
    body: "Go to www.rpmfind.net and find the libraries that are missing. You just search with the engine for the files and they will tell you which one that contains that file. Download the files for RedHat 7.1 and install them with rpm -ivh <filename>. Are they already installed and you only need to upgrade use rpm -Uvh <filename>.\n\nAn advise for you and other \"--nodeps --force\" people. The messages given by rpm that files are missing and that it is not possible to install is because files ARE missing. Solve the dependencies and your system will work well.\n\nwww.rpmfind.net is a very good tool/engine to find missinng files. Use it!\n\nGood luck!\n\nMartin"
    author: "Martin Karlsson"
  - subject: "Anti Alias in KDE ?"
    date: 2001-08-25
    body: "I don't know if anyone else has this problem... but when I enable AA support, all non-TrueType fonts dissapear, also apps can't acces them anymore. \nSo I had to manualy setup every font where I've used Helvetica or Times. \n \nIf this is not me... than it is realy big bug. \n \nAny clues?"
    author: "Marko Rosic"
  - subject: "Re: Anti Alias in KDE ?"
    date: 2001-11-09
    body: "I have the same bug it is not you, but it is annoying as there is no use using TT fonts without AA as they look crappy then and i need them in gimp (aliased)...\n\nAh well, prob takes lot of time to catch up with win in this (very important) field, as this means you customers stay or leave..."
    author: "Bas Burger"
  - subject: "Complaints"
    date: 2001-08-25
    body: "KDE 2.2 seems really good, except for some bugs introduced in Konqueror since 2.1.2 (non-working form buttons, some javascript.submit()s popping up new windows instead of doing what they are supposed to do) which are driving me crazy.\nAnother thing that isn't so good is the fact that it is impossible to disable the fadeout of text in the taskbar (replacing it with \"...\") and the tearoff in the K menus (it's kind of ugly albeit useful).\n\nI hope these issues will be fixed in 2.2.1, it would make it kick serious ass!"
    author: "Carlos Rodrigues"
  - subject: "KDE, back to basics for God's sake"
    date: 2001-08-26
    body: "i am a new Linux user with all my respect. i love the OS, but things are making me crazy in both gnome and KDE. \n\nWhy, tell me why KDE is trying to be a microsoft?.  While there are very good applications develpoed by other companies or groups, KDE wants all by himself. First Konqueror. There are powerful two internet browser, Mozilla (lets say NS 6.1)and  Opera, both working fine and posibly Mozilla will take over all Linux world in one year, why are you putting all your efforts in an internet browser and e-mail client?.\nThen KOffice, it simply lacks. Yes Star Ofiice is a slow beast but there is no match with KOfiice. it is a waste using all those power on this product. Gnome is a better choice when it comes to applications because they are aware of the situation they are not trying to be best in everything which is virtually impossible. But unfortunately Gnome lacks in speed and usabilty :(. Dont try to make a media player nobody uses, dont try to make a messaging client while there are thousands. \n\nWhat KDE has to do is to focus basics, What a new linux user wants? \n- A better desktop use, full drag and drop, fast execution, better memory usage (it is killing me to hear my hard drive go crazy for a minute in KDE)\n- A better understanding of icons (i am still trying to find where is the star office icon is, how to change?), short cuts, easy aceess to start menu, process winows, easy help system which helps exactly in place. \n- A better package system. provide better tools for install/uninstall i install packages, then i dont know where the hell they are.. in usr/bin? in a hiden file in home? i am not an advanced user face with the reality %90 of the wold will ever dumber than me how will they uninstall a package they installed day before?\n\nSo go for user interface business KDE, you are better than Gnome in this."
    author: "Ahmet"
  - subject: "Re: KDE, back to basics for God's sake"
    date: 2001-08-26
    body: "Not everyone has to use Linux. Linux shouldn't take over Microsofts peace of Desktop market.\nTo play simple games like cards, chess... you don't even need computer, to play better games, use PlayStation.\nFor most common office surroundings KOffice is and will be enough. One linux server and many diskles workstation, and one real admin. Nobody else has to know anything about system.\n\nIf you want to use Linux at home anyway, then you'lll have to learn it's ways.\n\nI prefer KDE's way of thinking... recently I've installed Slackware 8.0 on 300MB of space to my friend who don't want to know anything about how computer works. He has little space, bad machine, bad monitor etc. As I sait I have installed only necesary packages, XFree and KDE (not all parts) with KOffice.\nAfter that he can easily delete Windows because he has everything he needs for daily work:\n\nKwrite - For writing text\nKMail - For Mail\nKonqueror - For browsing\n\nAll these apps are among the best on the market, and if not, then quite enough for every common user.\nYou can't install Windows with all these apps on 300MB, can you?\n\nYes KDE could be alot faster, and yes Nautilus has some nice very features :)"
    author: "Marko Rosic"
  - subject: "KDE 2.2"
    date: 2001-08-29
    body: "Why do people care so much about the author's reviews? It would be more reasonable if we all just checked the new version out and made our own comments and reviews. We mostly have different points of views and likes/dislikes, trying to \njudge the others commentaries is stupid.\n\nMeanwhile i think KDE has gotten a lot better\nafter the past updates, but in my opinion there's\na lot of lacking compatibility still, especially concerning other documents such as Winword and homepages. I liked konqueror, but i still gotta use netscape since konqueror fucks up the charset and table sizes.\n\nLong life to KDE :)"
    author: "A. Arruda"
  - subject: "Re: KDE 2.2"
    date: 2001-08-29
    body: "i need review, i need to know whether it is worth my time installing this new software, is it worth sacrificing my system stability?"
    author: "azrul"
  - subject: "Re: KDE 2.2"
    date: 2001-08-30
    body: "There's something called ChangeLog\n\nIf i havent used most of the softwares i use based on bad reviews i would have missed many good programs only because some people have a different point of view."
    author: "A. Arruda"
---
There is a nice and favorable review of KDE 2.2 <a href="http://www.thelinuxreview.com/entry.lxp?lxpe=104">available here</a>.

The author does make some mistakes and some propositions that do not seem too clueful (Gecko instead of KHTML? Try KMozilla..), but it's still a nice read.

I do agree about the need for better integration between the kdepim programs and kmail, however.
<!--break-->
