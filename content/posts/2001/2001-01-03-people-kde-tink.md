---
title: "People of KDE: Tink"
date:    2001-01-03
authors:
  - "Inorog"
slug:    people-kde-tink
comments:
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-03
    body: "gag she admires hillary clinton."
    author: "craig"
  - subject: "Ug I know"
    date: 2001-01-03
    body: "How can anyone admire that totally fake, selfish, power hungry, habitual liar and opportunist simply because she is both successful and a woman, I will never know."
    author: "Anonymous Coward"
  - subject: "Re: Ug I know"
    date: 2001-01-04
    body: "Don't forget that Hilary is also one of the biggest supporters of censorship. She is also a heavy supporter of our most blatant violation of civil rights and waste of resources: the so-called \"war on drugs\". Get real, Hilary.\n\n<a href=\"http://lp.org\">Support the Libertarian party</a>, and help rid our country of this asenine mindset."
    author: "Qwerty"
  - subject: "Re: Ug I know"
    date: 2001-01-04
    body: "I would agree.  I am conservative on most issues except the drug war, which for 'lesser' drugs I believe would be better handled through strict regulation.  What better way is there to stifle an industry?  But I do not agree with some of the more extreme Libertarian positions, so I will stick with the Republicans."
    author: "Anonymous Coward"
  - subject: "Re: Ug I know"
    date: 2001-01-04
    body: "You don't have to \"agree\" to be \"liberal\". You can be liberal, conservative, or both at the same time. What matters is that you <b>don't enforce your thinking on anyone else</b>. Censorship, for example, is nothing but enforced thinking. Instead of holding parents responsible for educating their children about pornography or drugs, the Republicans and Democrats want to take control altogether, and make everyone else pay for it. Hey, if Joe Pervert wants to trade child pornography with Bill Dildo, he should have every damn right in the world. <b>You</b> might not like it, but that's the price you pay for freedom. You have to accept the \"bad\" (what you don't agree with) along with the \"good\" (what you do agree with).\n\n<p>The Libertarian party is not anti-conservative, but pro-freedom. People are happiest when they are free to live their lives in whatever way they choose. If you didn't know, our very own US constitution is <b>distinctly</b> Libertarian in philosophy."
    author: "Qwerty"
  - subject: "Re: Ug I know"
    date: 2001-01-04
    body: "But who's right is more important.\nJoe Pervert and Bill Dildo's to trade child porn,\nor the child's right not to appear in the child porn until he/she understands what it is doing (ie at age 16/18 or whatever the age of consent in your country is...US it's probably 45 or something stupid).\n\nThat is the problem I have with libertarians who say everything should be permissible. example #2: Is my right to keep my TV greater than some petty thiefs right to get my TV?"
    author: "Me"
  - subject: "Re: Ug I know"
    date: 2001-01-04
    body: "You are putting words in my mouth. Look at how many <b>different</b> legal concepts are involved with child pornography: Possession, trading or private distribution, public distribution, and of course, creation (i.e. the act of taking the actual photos or videos). To criminalize possession or private distribution is nothing short of asenine, and that's what I was getting at. Joe Pervert hasn't taken any pictures himself. He hasn't even put the on his website. He is guilty of nothing more than having \"lower\" moral standards than you, or in other words, <b>thinking different</b>. Thus, to make him a criminal is to deprive him of his constitutional right to make his own decisions.\n\n<p>BTW, I won't even comment on the thing about the thief and the TV. Talk about apples and oranges."
    author: "Qwerty"
  - subject: "Re: Ug I know"
    date: 2001-01-04
    body: "You obviously believe all rights are absolute.  This is just not practical.  Rights are given relative strength, and sometimes lesser rights such as the right to distribute are trumped by the rights of children not to be sexually violated.\n\nIt is this moral simplism which turns me off about Libertarianism.  I value individual rights enormously, but recognize that taken to an _extreme_ they can be self-defeating."
    author: "Chris Bordeman"
  - subject: "Re: Ug I know"
    date: 2001-01-05
    body: "<i>sometimes lesser rights such as the right to distribute are trumped by the rights of children not to be sexually violated.</i>\n\n<p>You are trying to make a connection between unconnected concepts. Let me explain this a different way. Yes, it's unfortunate that Exhibit A (child porn) was created in the first place. And, it's unfortunate that Exhibit A was distributed such that it fell into the hands of Defendant X. However, Defendant X neither participated in the creation or distribution of Exhibit A. He is guilty of nothing more than possession, or in other words, being a pervert. Does that make him a criminal? In my opinion, absolutely not. If anything, Exhibit A should be destroyed and that's the end of it. To try to \"teach Defendant X a lesson\" by sending him to jail or fining him is nothing short of asenine, and reeks of the typical conformist attitude that Republicans and Democrats try to force upon us. \"Think like us, or suffer the consequences.\" Frankly, that makes me sick.\n\n<p>Look at the war on drugs. 90% of people that are in jail for drug-related crimes are guilty of possession, and nothing more. Now, just who's paying for all that? It ain't the politicians who created these laws. It's you and me, whether we like it or not.\n\n<p><i>It is this moral simplism which turns me off about Libertarianism.</i>\n\n<p>Funny, moral simplism is exactly the thing that turns me off about the Republican and Democratic parties. Talk about not thinking out the whole problem before implementing a solution.\nIf you have not researched <a href=\"http://lp.org\">Libertarian philosophy</a> for yourself, I urge you to do so. Don't judge a book by its cover.\n\n<p>I would also like to apoligize to the KDE people for taking up all this space on the news board, and being completely off-topic for this article. What can I say, besides that I am very spirited in my beliefs. As a final note, I <b>am not</b> a creator, distributor, or user of child pornography, and I <b>don't</b> support it in any way. I think it's rather disgusting, untasteful, and unethical, but then again, who am I to make that decision for everybody else?\n\n<p>BTW, KDE 2 rocks on Debian unstable."
    author: "Qwerty"
  - subject: "Re: Ug I know"
    date: 2001-01-07
    body: "But he _did_ participate in the distribution, he recieved the dirty pics!  Also, in this case, simply owning and viewing the child porn and viewing it is a violation of the spirit of the law, something Libertarianism seems to think is secondary to a hardline, tunnel-vision-oriented philosophy."
    author: "Chris Bordeman"
  - subject: "Re: Ug I know"
    date: 2001-01-08
    body: "<i>But he did participate in the distribution, he recieved the dirty pics!</i>\n\n<p>Absolutely not. You're stretching the truth to fit your own agenda.\n\n<p><i>Also, in this case, simply owning and viewing the child porn and viewing it is a violation of the spirit of the law</i>\n\n<p>Violation of the spirit of the law? <b>Whose spirit?</b> Do you really want to live in a state of enforced morality? There's a term for that, and I believe it's called \"communism\". I can't imagine living in a place where simply viewing a photograph can put you in jail.\n\n<p>Read your history, and learn from the mistakes of others. You cannot enforce morality. And I'm not talking about obvious crimes like murder, rape, or theft. I'm talking about <b>victimless crimes</b> like drug use and possession of child pornography, which, on a more general level, are nothing more than crimes of thinking different.\n\n<p>Let me reiterate. It's very unfortunate that certain people have disgustingly low moral standards. But, that's the price you pay for freedom. You have to accept the fact that some people are different and don't necessarily live their lives by the same moral guidelines as you. Also, you have to realize that this issue, like many others, is not black and white. There is plenty of grey, and you can't just ignore it and hope it goes away. That's not how a government should operate, and it's not how the US was intended to operate."
    author: "Qwerty"
  - subject: "Re: Ug I know"
    date: 2001-01-09
    body: "But _all_ laws are intended to enforce morality, and are derived from the values of the people.  Name one that is not."
    author: "Chris Bordeman"
  - subject: "Re: Ug I know"
    date: 2001-01-10
    body: "<i>But _all_ laws are intended to enforce morality, and are derived from the values of the people.</i>\n\n<p>Then please explain why US laws can be purchased like material items.\n\n<p>You put too much trust in government. If you really think that US government is here to protect and serve the people, then why has the national crime rate shot so high since the criminalization of recreational drug use? And why don't they care or even admit it? If the government was doing what it's supposed to be doing, they would re-think this \"solution\" with an open mind.\n\n<p>What I'm saying is that enforced morality is a trap. A false sense of security. An impossible dream. Wake up and take responsibility for your own actions. The government isn't here to babysit us, or teach us morality, or protect us from ourselves. That's your job, not mine. If you didn't realize, I am the one paying for the government to babysit you, and I don't like it. Because, as a citizen of the US, half of my earnings are stolen each year to support your enforced morality, and <b>that's</b> immoral, in my book.\n\n<p>Conformity is like a black hole, and I think you've already been sucked in. Let the people make their own decisions. After all, isn't Free Will the entire point of the US Constitution?"
    author: "Qwerty"
  - subject: "Re: Ug I know"
    date: 2001-01-10
    body: "<I>Then please explain why US laws can be purchased like material items.</I>\n<BR><BR>\nClearly, some laws are 'bought,' but even those enforce a behavior.\n<BR><BR>\n<I>What I'm saying is that enforced morality is a trap.</I>\n<BR><BR>\n<U>How</U>?  Sure, it doesn't always work, but the vast majority of our laws do work quite effectively."
    author: "Chris Bordeman"
  - subject: "Re: Ug I know"
    date: 2001-01-04
    body: "This the sort of extreme position I was talking about.  This is why I am a conservative  Republican, not a Libertarian.\n\nIt is 18 in the U.S. ;)"
    author: "Anonymous Coward"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-04
    body: "Hillary Clinton stands for everything that is wrong in the US (and mankind?).  Why did the people of New York elect her as their Senator?  Does she represent New York? No."
    author: "secret_agent_man"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-04
    body: "She only ran in New York because it is one of the very few states (only?) liberal enough to ignore her utter lack of sincerity and power-lust.  Take comfort in that in 4 years, Bush will have a better grasp of the issues than anyone and lots of experience to boot, reversing the only successful argument against him onto her."
    author: "Anonymous Coward"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-05
    body: "Powerful women are a scary thought aren't they? ;-0\n\n--\nTink, who finds all the comments very amusing, thanx guys!"
    author: "Tink"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-05
    body: "No, powerful women are sexy, it is the divisive, fake, heartless, opportunistic, ultra-selfish, win-at-all-costs get-in-my-way-and-you're-dead types like Hillary Clinton that scare people."
    author: "Chris Bordeman"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-04
    body: "I think it's a shame that all the comments that have come up here have been stupid, or negative.  It's time to change that.\n\nTink: Thanks for the great interviews.  They've illustrated very well that people do KDE because they want to, so KDE will go on as long as there are people who want it.\n\nOh yes, and they've been entertaining, too. :-)"
    author: "Neil Stevens"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-04
    body: "Yey, so you'll change it by putting an asskissing comment. Thanks for that."
    author: "Me"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-04
    body: "Unlike you, both Neil and Tink are doing something to help KDE. Comments like yours make me think we should consider moderating posts.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-04
    body: "Thath I would agree on.<BR>\nThank you Tink for helping kde."
    author: "KDE USER"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-04
    body: "Sounds good to me... the \"Does interoperability (GTK widgets in QT/KDE) help KDE?\" thread seemed to develop into a boring flame war with KDE fans and GNOME supporters having a go at each other. Until that thread, this site has been a fairly enjoyable read...\n<p>\nWho cares if Tink admires Hilary Clinton? Ever heard of \"freedom of choice\" and \"freedom of expression\"? People are entitled to admire Hilary, just as others are entitled to admire that Libertarian guy and GW Bush. It's about time that right wing Republicans stopped hating the Clintons for winning in '92 and '96; remember, you don't have an absolute right to power! (Thank god)."
    author: "zaaz"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-04
    body: "Nobody hates them for winning, that is an oversimplification.  It is the corruption and scorched-earth methods they use to destroy anyone in their way that sickens us."
    author: "Chris Bordeman"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-05
    body: "And the GOP is entirely innocent of ever having been corrupt, underhand, illegal or whatever, have they? Ever heard of Richard Nixon? He lied like Clinton. And Clinton's \"crime\" wasn't that important - having an affair with some intern is a matter for him, his family and Monica. Republicans and right-wingers in general seem to take a great interest in a person's private morality, lifestyle choices and so on, when it's really nothing to do with them!\n<p>\nOf course, if the GOP was as marvelous as you seem to be implying they are, then the Democratic presidential candidate wouldn't have lost a state that he should have [and did] win, despite it being run by the brother of the Republican candidate, where the father of the Republican, was himself President eight years earlier and was defeated by the Democrat's running partner of eight and four years earlier, where the result was certified by a Republican Secretary of State (Cruella De Vil), where the Republican-dominated state legislature got involved and started making noises about overriding any decisions of a senior court within that state, and where the entire process was ended by the decision of a Republican-dominated Supreme Court. Corruption? No. Slightly worrying? Yep. I bought Tom Paine's Common Sense and the Rights of Man today; I'm sure that it will apply 100% to the current situation in modern western countries like the US, UK and so on."
    author: "zaaz"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-07
    body: "After promising \"the most moral administration in history,\" the Clintons have destroyed more good people and lowered political standards more than anyone in the history of the presidency.  Sure, the Repubs have done many evil things, but that pales in comparison to the scorched-earth tactics the Clintons have used.  The Democratic Party has become the party of the status quo, opposing with scare tactics all reform whenever they can, minimizing then taking credit for what they can't, e.g. welfare reform, social security reform, education reform, tort reform, and medicare reform."
    author: "Chris Bordeman"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-08
    body: "Scorched earth tactics were used by Hitler, Stalin and Slobodan Milosevic when they embarked on campaigns of ethnic cleansing, holocaust and purges. Are you comparing Bubba and Hilary to them? I suggest you start using a little bit of rationale and perspective."
    author: "jpmfan"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-09
    body: "No, brainiac, 'political' scorched-earch tactics."
    author: "Chris Bordeman"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-04
    body: "Granted Tink,\nbut Neil's post\nwas just asskissing.\nAnd didn't help KDE at all.\nJust because a comment isn't negative\ndoesn't mean it's a good comment."
    author: "Me"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-04
    body: "Well, do you know who Neil Stevens is?\n\nI must confess I don't, but that means nothing considering my memory :-)\n\nMaybe he has contributed to KDE, and that was what the other person was referring to."
    author: "Roberto Alsina"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-05
    body: "He's the author of Kit, the AIM client in kdenetwork IIRC.\n<p>\nRich."
    author: "Richard Moore"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-05
    body: "Do you know who I am?\nNo.\nSo the person was referring to the act of posting a message here.\nNeil Stevens didn't do anything for KDE by kissing ass, just as I didn't do anything for flaming him for kissing ass\n\nIt's really not that complicated to work out."
    author: "Me"
  - subject: "I *do* know who you are."
    date: 2001-01-05
    body: "You are someone who chooses to be anonymous.\nOf course, as soon as you choose anonymity, you declare you don't want to be associated with anything you do or did.\n\nSo, since you are anonymous, for all practical effects and purposes, you are just someone completely unrelated to KDE who passed by the site and started insulting someone who actually does help KDE.\n\nTherefore, I stand by my previous post, and declare whatever Neil Stevens or Tink have to say in a KDE related site more important than anything you might say, unless I am swept off my feet by the sheer brilliance of such a post.\n\nHaving my feet still firmly planted on the ground (I am pretty hard to sweep), I consider your original comment useless, rude, obnoxious, insulting and out of place.\n\nHave fun!"
    author: "Roberto Alsina"
  - subject: "Re: I *do* know who you are."
    date: 2001-01-06
    body: "I never claimed my post did help KDE.\nIn fact, I'm fairly certain it said it didn't do anything.\n\nIt is inert. It is not good for KDE, it is not bad for KDE.\n\nI was commenting that Neil's original kiss ass comment, did not help KDE either.\n\nYes, it was useless, rude, obnoxious, insulting and out of place.\n\nBut kissing ass doesn't help much either.\n\nYou are putting too much prejudice into this.\nNeil may one day decide to flame someone with a wild unnecessary flame, just like I did.\n\nWhat would happen if Tink were to post \"Hello, I like hot grits down my pants\"? Is that more important, just because Tink posted it? I'd hope not, but that's what you're claiming."
    author: "Me"
  - subject: "Re: I *do* know who you are."
    date: 2001-01-07
    body: "Ok, so you admit you are rude obnoxious and out of place. So, why should anyone care about your posts?\n\nBTW: for some reason, it\u00b4s always people hiding like chickens under the blanket of anonimity (say, you) who post stuff about grits and natalie portman. So, I would guess there\u00b4s a much higher chance of you doing it than her.\n\nSo, don\u00b4t believe that waving your own reputation would harm that of others."
    author: "Roberto Alsina"
  - subject: "Re: I *do* know who you are."
    date: 2001-01-09
    body: "Who's reputation was I trying to harm?\nNot tinks. There is a higher chance of an anonymous person doing it (I wouldn't. I have standards. I only post things that are relevant to some train of thought, and yes, I maintain that telling someone that \"ass kissing comments don't help KDE\" is a relevant comment), yes, but it is still possible. So your view is prejudiced.\n\nAnd no-one should care about my posts. They were a statement. however you obviously care enough about my posts that 3 days after the first was made, you perpetuate it. Strange that."
    author: "Me"
  - subject: "Re: People of KDE: Tink"
    date: 2001-01-05
    body: "I won't repeat what's in the comments that were deleted, but how was it a \"good comment\" to complain about Hillary Clinton?\n\nGranted, I personally would have voted for Lazio, and I find some of the beliefs Clinton propounds in her writings scary, but all that stuff is irrelevant to KDE, and to my opinion of Tink.\n\nA person is more than just a set of beliefs, you know."
    author: "Neil Stevens"
  - subject: "I thought this was a KDE site..."
    date: 2001-01-05
    body: "Sorry for being boring, but...\n\nPosts about Hilary/Libertarians/etc: about 27 or 28.\nPosts about KDE: 1 (the original one).\n\nHave the people who run this site considered adopting a proper forum like at Anandtech (<a href=\"http://forums.anandtech.com\">http://forums.anandtech.com</a>) or Atlas F1 (<a href=\"http://www.atlasf1.com/bb/index.php\">http://www.atlasf1.com/bb/index.php</a>)? This way, people would have to sign up by leaving their email addresses and threads and people could easily be deleted and controlled. Obviously, if you are all more interested in improving KDE (I would be), then don't bother, but it might be something to bear in mind..."
    author: "jpmfan"
  - subject: "Ha Ha Ha"
    date: 2001-01-08
    body: "<p><i>Posts about Hilary/Libertarians/etc: about 27 or 28. Posts about KDE: 1</i>\n\n<p><i>This way, people would have to sign up by leaving their email addresses and threads and people could easily be deleted and controlled.</i>\n\n<p>How ironic! Nothing like a good dose of republican conformity to solve the problem! Rules and penalties, just like home! Of course, KDE has the right to do whatever they want with their own website, but I just found that sort of amusing."
    author: "Anonymous"
  - subject: "Re: Ha Ha Ha"
    date: 2001-01-08
    body: "Actually, I voted for Gore... I guess I was taking a leaf out of Clinton's book, copying the nuts in the GOP."
    author: "jpmfan"
  - subject: "Where is Waldo?"
    date: 2002-02-04
    body: "Hmmm... no comment from Waldo :-) Has he read this?"
    author: "Andy \"Storm\" Goossens"
---
To start this new year in a joyous mood, I kindly asked Tink to grant the <a href="http://www.kde.org/people/tink.html">first interview of the year</a> for our popular <a href="http://www.kde.org/people/people.html">People behind KDE</a> section, and she graciously accepted. At this time of celebration we would like to thank and pay tribute to those that, with their patience, support and understanding shown toward their dear ones, KDE contributors, make the KDE project an enjoyable activity and even a family :-). And thank you Tink for all the great work. A Happy New Year to you all!

<!--break-->
